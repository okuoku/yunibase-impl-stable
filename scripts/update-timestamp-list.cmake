#
# INPUTs:
#  DIR: Dirname
#

cmake_policy(SET CMP0009 NEW)

if(NOT DIR)
    message(FATAL_ERROR "Huh?")
endif()

set(me ${CMAKE_CURRENT_LIST_DIR}/..)
set(out ${CMAKE_CURRENT_LIST_DIR}/../_timestamps/${DIR}.tsv)

file(GLOB_RECURSE lis RELATIVE ${me} "${me}/${DIR}/*")

set(times)

foreach(e ${lis})
    file(TIMESTAMP ${me}/${e} t "%Y%m%d%H%M.%S" UTC)
    set(x "${t}\t${e}")
    list(APPEND times ${x})
endforeach()

list(SORT times)

string(REGEX REPLACE ";" "\n" times "${times}")
file(WRITE ${out} "${times}")

