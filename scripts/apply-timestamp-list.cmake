#
# INPUTs:
#  DIR: Directory
#

if(NOT DIR)
    message(FATAL_ERROR "Huh?")
endif()

set(me ${CMAKE_CURRENT_LIST_DIR}/..)
set(fil ${CMAKE_CURRENT_LIST_DIR}/../_timestamps/${DIR}.tsv)

file(STRINGS ${fil} lis)

set(ENV{TZ} "UTC+0")

message(STATUS "Apply timestamp file(${DIR})...")

# Pass1: Collect times
foreach(e ${lis})
    if(${e} MATCHES "([^\t]*)\t(.*)")
        set(tim ${CMAKE_MATCH_1})
        set(pth ${CMAKE_MATCH_2})
        if(NOT times_${tim}_files)
            list(APPEND times ${tim})
        endif()
        list(APPEND times_${tim}_files ${me}/${pth})
        # message(STATUS "${pth}: ${tim}")
    else()
        message(FATAL_ERROR "Unknown timestamp ${e}")
    endif()
endforeach()

# Pass2: Apply timestamp
foreach(t ${times})
    #message(STATUS "${t}: ${times_${t}_files}")
    execute_process(COMMAND touch -c -t ${t} ${times_${t}_files})
endforeach()
