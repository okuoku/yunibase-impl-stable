function(check_status st msg)
    if(${st} EQUAL 0)
        message(STATUS "... OK ${st} ${msg}")
    else()
        message(FATAL_ERROR "... Err ${st} ${msg}")
    endif()
endfunction()

set(TOP ${CMAKE_CURRENT_LIST_DIR}/..)
file(DOWNLOAD https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/latest-version.txt ${TOP}/latest-version.txt SHOW_PROGRESS)
file(STRINGS latest-version.txt ver)
string(STRIP "${ver}" ver)

message(STATUS "Version = ${ver}")
set(url "https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/sagittarius-${ver}.tar.gz")
message(STATUS "Url = ${url}")

file(DOWNLOAD 
    ${url}
    ${TOP}/sagittarius.tar.gz SHOW_PROGRESS
    STATUS st)

check_status(${st})

message(STATUS "Removing previous version...")

execute_process(COMMAND git rm -rf ${TOP}/sagittarius)

message(STATUS "Extract ...")

execute_process(COMMAND cmake -E tar zxvf ${TOP}/sagittarius.tar.gz)

message(STATUS "Generate commit ...")

file(RENAME ${TOP}/sagittarius-${ver} ${TOP}/sagittarius)

execute_process(COMMAND git add -f ${TOP}/sagittarius)
execute_process(COMMAND git commit -m "Sagittarius ${ver}\n\nfrom:\n${url}\n")

