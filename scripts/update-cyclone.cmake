set(CYCLONE ${CMAKE_CURRENT_LIST_DIR}/../cyclone)
set(TARFILE ${CMAKE_CURRENT_LIST_DIR}/../cyclone.tar)
set(CYCLONE_BOOTSTRAP ${CMAKE_CURRENT_LIST_DIR}/../_submodules/cyclone-bootstrap)

execute_process(COMMAND
    git archive --format=tar HEAD
    OUTPUT_FILE ${TARFILE}
    WORKING_DIRECTORY ${CYCLONE_BOOTSTRAP}
    RESULT_VARIABLE rr)

if(rr)
    message(FATAL_ERROR "Err: Fetch ${rr}")
endif()

execute_process(COMMAND
    git rm -rf ${CYCLONE}
    )

file(MAKE_DIRECTORY ${CYCLONE})

execute_process(COMMAND
    tar xvf ${TARFILE} -C ${CYCLONE}
    )

