/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         number_yyparse
#define yylex           number_yylex
#define yyerror         number_yyerror
#define yydebug         number_yydebug
#define yynerrs         number_yynerrs

/* First part of user prologue.  */
#line 32 "src/NumberReader.y"

#include <stdio.h>
#include <stdlib.h>
#include "scheme.h"
#include "Object.h"
#include "Object-inl.h"
#include "Pair.h"
#include "Pair-inl.h"
#include "SString.h"
#include "StringProcedures.h"
#include "EqHashTable.h"
#include "NumberScanner.h"
#include "TextualInputPort.h"
#include "TextualOutputPort.h"
#include "Arithmetic.h"
#include "Reader.h"
#include "NumberReader.h"
#include "ScannerHelper.h"
#include "Scanner.h"
#include "Ratnum.h"
#include "Flonum.h"
#include "ProcedureMacro.h"
#include "MultiVMProcedures.h"

using namespace scheme;
extern int number_yylex(YYSTYPE* yylval);
extern int number_yyerror(const char *);
//#define YYDEBUG 1
// yydebug = 1

/* static int ucs4stringToInt(const ucs4string& text) */
/* { */
/*     int ret = strtol(text.ascii_c_str(), NULL, 10); */
/*     if (errno == ERANGE) { */
/*         MOSH_FATAL("reader suffix"); */
/*     } */
/*     return ret; */
/* } */

// e1000 => 1000 e+100 => 100, e-100 =>-100
/* static int suffix(const ucs4string& text) */
/* { */
/*     MOSH_ASSERT(text.size() > 0); */
/*     const char* p = text.ascii_c_str(); */
/*     int ret = strtol(p + 1, NULL, 10); */
/*     if (errno == ERANGE) { */
/*         MOSH_FATAL("reader suffix"); */
/*     } */
/*     return ret; */
/* } */

// text => "e100", "e+100" or "e-100" style
static Object suffixToNumberOld(const ucs4string& text)
{
    int sign = 1;
    ucs4string decimal10(UC(""));
    if (text[1] == '-') {
        sign = -1;
        decimal10 = text.substr(2, text.size() - 2);
    } else if (text[1] == '+') {
        decimal10 = text.substr(2, text.size() - 2);
    } else {
        decimal10 = text.substr(1, text.size() - 1);
    }
    Object exponent = Bignum::makeInteger(decimal10);
    if (sign == -1) {
        exponent = Arithmetic::negate(exponent);
    }
    MOSH_ASSERT(!exponent.isBignum());
    return Arithmetic::expt(Object::makeFixnum(10), exponent);
}

static Object suffixToNumber(const ucs4string& text)
{
    int sign = 1;
    ucs4string decimal10(UC(""));
    if (text[1] == '-') {
        sign = -1;
        decimal10 = text.substr(2, text.size() - 2);
    } else if (text[1] == '+') {
        decimal10 = text.substr(2, text.size() - 2);
    } else {
        decimal10 = text.substr(1, text.size() - 1);
    }
    Object exponent = Bignum::makeInteger(decimal10);
    if (sign == -1) {
        exponent = Arithmetic::negate(exponent);
    }
    return exponent;
}


#line 168 "src/NumberReader.tab.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_NUMBER_YY_SRC_NUMBERREADER_TAB_HPP_INCLUDED
# define YY_NUMBER_YY_SRC_NUMBERREADER_TAB_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int number_yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    END_OF_FILE = 258,
    PLUS = 259,
    MINUS = 260,
    SLASH = 261,
    DOT = 262,
    AT = 263,
    MY_NAN = 264,
    MY_INF = 265,
    IMAG = 266,
    RADIX_2 = 267,
    RADIX_8 = 268,
    RADIX_10 = 269,
    RADIX_16 = 270,
    EXACT = 271,
    INEXACT = 272,
    DIGIT_2 = 273,
    DIGIT_8 = 274,
    DIGIT_10 = 275,
    DIGIT_16 = 276,
    EXPONENT_MARKER = 277
  };
#endif

/* Value type.  */



int number_yyparse (void);

#endif /* !YY_NUMBER_YY_SRC_NUMBERREADER_TAB_HPP_INCLUDED  */


/* Unqualified %code blocks.  */
#line 125 "src/NumberReader.y"

  bool hasExplicitExact;

#line 253 "src/NumberReader.tab.cpp"

#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  40
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   383

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  41
/* YYNRULES -- Number of rules.  */
#define YYNRULES  140
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  226

#define YYUNDEFTOK  2
#define YYMAXUTOK   277


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   151,   151,   152,   154,   154,   154,   154,   156,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   173,   174,   175,   176,   179,   180,   190,   191,
     194,   195,   200,   202,   204,   205,   206,   207,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   220,   221,
     222,   223,   226,   227,   237,   238,   241,   242,   247,   248,
     251,   253,   254,   255,   256,   257,   258,   259,   260,   261,
     262,   263,   264,   265,   266,   269,   270,   271,   272,   275,
     276,   286,   287,   290,   291,   296,   297,   300,   301,   306,
     309,   316,   323,   330,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   355,   356,
     357,   358,   361,   362,   372,   373,   376,   396,   407,   431,
     445,   447,   452,   459,   460,   463,   464,   465,   468,   469,
     476,   477,   480,   481,   482,   485,   486,   489,   490,   493,
     494
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "END_OF_FILE", "PLUS", "MINUS", "SLASH",
  "DOT", "AT", "MY_NAN", "MY_INF", "IMAG", "RADIX_2", "RADIX_8",
  "RADIX_10", "RADIX_16", "EXACT", "INEXACT", "DIGIT_2", "DIGIT_8",
  "DIGIT_10", "DIGIT_16", "EXPONENT_MARKER", "$accept", "top_level",
  "datum", "num2", "complex2", "real2", "ureal2", "sreal2", "uinteger2",
  "digit2", "num8", "complex8", "real8", "ureal8", "sreal8", "uinteger8",
  "digit8", "num16", "complex16", "real16", "ureal16", "sreal16",
  "uinteger16", "digit16", "num10", "inexact_complex10", "complex10",
  "real10", "ureal10", "sreal10", "decimal10", "uinteger10",
  "uinteger10String", "digit10", "exactness", "suffix", "prefix2",
  "prefix10", "prefix8", "prefix16", "naninf", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277
};
# endif

#define YYPACT_NINF (-67)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-121)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     194,   -67,    90,    90,    90,    90,   -67,   164,     8,   -67,
     -67,   -67,   -67,   -67,   -67,   359,   100,   181,    59,   139,
     -67,   -67,   -67,   -67,   -67,   243,   243,   103,   -67,   -67,
     -67,   -67,   -67,   134,   -67,   -67,   -67,     6,    17,   -67,
     -67,   -67,   -67,   -67,   -67,    78,   327,   -67,   141,   -67,
       4,     7,   -67,   205,   210,   -67,   355,   297,   308,   -67,
     357,   -67,   -67,   248,   -67,   259,   263,   -67,   -67,   362,
     -67,   -67,    36,   -67,   -67,   -67,   -67,    27,   -67,    34,
     -67,   334,   238,   238,   103,   334,   -67,   -67,   -67,   -67,
     -67,    37,   -67,   -67,    41,   330,   340,   111,   -67,     9,
     -67,   -67,    47,    50,   -67,    54,    61,   224,   229,   185,
     -67,    70,    75,   -67,    79,    87,   311,   314,   108,   146,
     -67,   -67,   106,   145,   -67,   150,   159,   276,   280,   158,
     358,   -67,   -67,   -67,   -67,   163,   169,   -67,   103,   334,
     -67,   -67,   -67,   -67,   170,   176,   -67,   180,   184,     0,
       0,   -67,   -67,     9,   -67,   -67,   -67,   -67,   -67,   187,
     191,   -67,   202,   207,   243,   243,   -67,   -67,   -67,   -67,
     -67,   -67,   211,   215,   -67,   216,   221,   325,   325,   -67,
     146,   -67,   -67,   -67,   -67,   -67,   226,   230,   -67,   235,
     240,   293,   293,   -67,   358,   -67,   -67,   -67,   -67,   -67,
     -67,   -67,   -67,   -67,   -67,   -67,   -67,   -67,   -67,   -67,
     -67,   -67,   -67,   -67,   -67,   -67,   -67,   -67,   -67,   -67,
     -67,   -67,   -67,   -67,   -67,   -67
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
     125,     3,   125,   125,   125,   125,   126,   127,     0,     2,
       4,     5,     7,     6,    87,   134,     0,     0,     0,     0,
     127,   130,   135,   132,   137,     0,     0,     0,    32,    59,
     124,    58,   123,    89,   108,   109,   112,     0,   128,   121,
       1,   131,   136,   133,   138,     0,     0,     8,     9,    22,
      23,    26,    30,     0,     0,    88,    94,     0,     0,    33,
      34,    48,    49,    52,    56,     0,     0,    86,    60,    61,
      75,    76,    79,    83,    85,   139,   140,   114,   110,   115,
     111,   128,     0,     0,     0,   128,   129,   122,   116,    16,
      28,    24,    17,    29,    25,     0,     0,     0,    11,     0,
      31,   102,   114,   110,   103,   115,   111,     0,     0,     0,
      42,    54,    50,    43,    55,    51,     0,     0,     0,     0,
      57,    69,    81,    77,    70,    82,    78,     0,     0,     0,
       0,    84,    92,    93,   117,     0,     0,   113,   120,   128,
     119,    20,    21,    14,     0,     0,    15,     0,     0,     0,
       0,    10,    23,    27,   100,   106,   101,   107,    98,     0,
       0,    99,     0,     0,     0,     0,    95,    40,    46,    41,
      47,    38,     0,     0,    39,     0,     0,     0,     0,    35,
      53,    67,    73,    68,    74,    65,     0,     0,    66,     0,
       0,     0,     0,    62,    80,    90,    91,   118,    12,    18,
      13,    19,    24,    25,    96,   104,    97,   105,   114,   115,
      36,    44,    37,    45,    54,    50,    55,    51,    63,    71,
      64,    72,    81,    77,    82,    78
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -67,   -67,   -67,   -67,   -67,    53,     5,   162,    15,    -2,
     -67,   -67,   137,   -41,   -67,   152,    16,   -67,   -67,   131,
     -61,   -67,   135,   -66,   -67,   -67,   -67,   -10,   -23,   -67,
     -67,   192,   -16,   -19,   378,   -59,   -67,   -67,   -67,   -67,
     -25
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     8,     9,    10,    47,    48,    49,    50,    51,    31,
      11,    59,    60,    61,    62,    63,    32,    12,    68,    69,
      70,    71,    72,    73,    13,    14,    55,    33,    34,    35,
      36,    37,    38,    39,    15,    88,    16,    17,    18,    19,
      78
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      74,    80,    77,    79,   122,   125,   131,    56,    40,    75,
      76,    81,    84,    99,    52,    98,   111,   114,    28,    87,
      91,    94,   134,  -120,    85,    28,   140,    28,   103,   106,
     102,   105,   112,   115,    64,    28,    29,    30,   132,    86,
     123,   126,   130,    52,    52,   133,    74,    74,   141,   100,
      90,    93,   142,    74,    28,    29,    30,    67,   154,   135,
     136,   155,    87,    57,    58,   156,   186,   189,   138,   139,
     145,   148,   157,    64,    64,   172,   175,    28,    29,   120,
     197,   167,   160,   163,   159,   162,   168,    75,    76,    89,
     169,   173,   176,    52,    52,    52,    28,    52,   170,   166,
     144,   147,   187,   190,    45,    46,     6,    20,    74,    74,
      74,    74,   177,   178,   153,   149,   150,   181,    28,    87,
      87,    28,    29,    30,   202,   203,    28,    29,   131,    28,
     222,   224,    64,    64,    64,    64,   214,   216,    82,    83,
      80,   208,   209,    65,    66,    95,    96,    52,    52,    97,
     151,   100,   215,   217,    90,    93,   182,    28,    29,    30,
      67,   183,   191,   192,    28,    29,   223,   225,    25,    26,
     184,    27,    74,    74,   195,    74,    28,    29,    30,    67,
     196,   198,    28,    29,    30,    53,    54,   199,    27,   164,
     165,   200,    27,    64,    64,   201,   120,     1,   204,    28,
      29,    30,   205,    28,    29,    30,     2,     3,     4,     5,
       6,     7,    27,   206,    75,    76,   101,    27,   207,    75,
      76,   104,   210,    28,    29,    30,   211,   212,    28,    29,
      30,    27,   213,    75,    76,   158,    27,   218,    75,    76,
     161,   219,    28,    29,    30,    27,   220,    28,    29,    30,
      27,   221,    75,    76,   119,   179,    28,    29,    30,   152,
     193,    28,    29,    30,     0,   194,    28,    29,    75,    76,
     121,   180,    75,    76,   124,     0,   137,    28,    29,    30,
      67,    28,    29,    30,    67,    75,    76,   185,     0,    75,
      76,   188,     0,     0,    28,    29,    30,    67,    28,    29,
      30,    67,    75,    76,     0,     0,    75,    76,   110,     0,
       0,    28,    29,    30,    67,    28,    29,    75,    76,   113,
      75,    76,   171,    75,    76,   174,    28,    29,     0,    28,
      29,     0,    28,    29,    75,    76,    75,    76,    92,    75,
      76,   143,     0,    28,    29,    28,     0,     0,    28,    75,
      76,   146,    28,    29,    30,     0,    86,     0,    28,   107,
     108,   116,   117,   109,     0,   118,   127,   128,     0,     0,
     129,    41,    42,    43,    44,     0,    28,    29,    30,    67,
      21,    22,    23,    24
};

static const yytype_int16 yycheck[] =
{
      19,    26,    25,    26,    65,    66,    72,    17,     0,     9,
      10,    27,     6,     6,    16,    11,    57,    58,    18,    38,
      45,    46,    81,     6,     7,    18,    85,    18,    53,    54,
      53,    54,    57,    58,    18,    18,    19,    20,    11,    22,
      65,    66,     6,    45,    46,    11,    65,    66,    11,    51,
      45,    46,    11,    72,    18,    19,    20,    21,    11,    82,
      83,    11,    81,     4,     5,    11,   127,   128,    84,    85,
      95,    96,    11,    57,    58,   116,   117,    18,    19,    63,
     139,    11,   107,   108,   107,   108,    11,     9,    10,    11,
      11,   116,   117,    95,    96,    97,    18,    99,    11,   109,
      95,    96,   127,   128,     4,     5,    16,    17,   127,   128,
     129,   130,     4,     5,    99,     4,     5,    11,    18,   138,
     139,    18,    19,    20,   149,   150,    18,    19,   194,    18,
     191,   192,   116,   117,   118,   119,   177,   178,     4,     5,
     165,   164,   165,     4,     5,     4,     5,   149,   150,     8,
      97,   153,   177,   178,   149,   150,    11,    18,    19,    20,
      21,    11,     4,     5,    18,    19,   191,   192,     4,     5,
      11,     7,   191,   192,    11,   194,    18,    19,    20,    21,
      11,    11,    18,    19,    20,     4,     5,    11,     7,     4,
       5,    11,     7,   177,   178,    11,   180,     3,    11,    18,
      19,    20,    11,    18,    19,    20,    12,    13,    14,    15,
      16,    17,     7,    11,     9,    10,    11,     7,    11,     9,
      10,    11,    11,    18,    19,    20,    11,    11,    18,    19,
      20,     7,    11,     9,    10,    11,     7,    11,     9,    10,
      11,    11,    18,    19,    20,     7,    11,    18,    19,    20,
       7,    11,     9,    10,     6,   118,    18,    19,    20,    97,
     129,    18,    19,    20,    -1,   130,    18,    19,     9,    10,
      11,   119,     9,    10,    11,    -1,    84,    18,    19,    20,
      21,    18,    19,    20,    21,     9,    10,    11,    -1,     9,
      10,    11,    -1,    -1,    18,    19,    20,    21,    18,    19,
      20,    21,     9,    10,    -1,    -1,     9,    10,    11,    -1,
      -1,    18,    19,    20,    21,    18,    19,     9,    10,    11,
       9,    10,    11,     9,    10,    11,    18,    19,    -1,    18,
      19,    -1,    18,    19,     9,    10,     9,    10,    11,     9,
      10,    11,    -1,    18,    19,    18,    -1,    -1,    18,     9,
      10,    11,    18,    19,    20,    -1,    22,    -1,    18,     4,
       5,     4,     5,     8,    -1,     8,     4,     5,    -1,    -1,
       8,    12,    13,    14,    15,    -1,    18,    19,    20,    21,
       2,     3,     4,     5
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,    12,    13,    14,    15,    16,    17,    24,    25,
      26,    33,    40,    47,    48,    57,    59,    60,    61,    62,
      17,    57,    57,    57,    57,     4,     5,     7,    18,    19,
      20,    32,    39,    50,    51,    52,    53,    54,    55,    56,
       0,    12,    13,    14,    15,     4,     5,    27,    28,    29,
      30,    31,    32,     4,     5,    49,    50,     4,     5,    34,
      35,    36,    37,    38,    39,     4,     5,    21,    41,    42,
      43,    44,    45,    46,    56,     9,    10,    51,    63,    51,
      63,    55,     4,     5,     6,     7,    22,    56,    58,    11,
      29,    63,    11,    29,    63,     4,     5,     8,    11,     6,
      32,    11,    51,    63,    11,    51,    63,     4,     5,     8,
      11,    36,    63,    11,    36,    63,     4,     5,     8,     6,
      39,    11,    43,    63,    11,    43,    63,     4,     5,     8,
       6,    46,    11,    11,    58,    51,    51,    54,    55,    55,
      58,    11,    11,    11,    29,    63,    11,    29,    63,     4,
       5,    28,    30,    31,    11,    11,    11,    11,    11,    51,
      63,    11,    51,    63,     4,     5,    50,    11,    11,    11,
      11,    11,    36,    63,    11,    36,    63,     4,     5,    35,
      38,    11,    11,    11,    11,    11,    43,    63,    11,    43,
      63,     4,     5,    42,    45,    11,    11,    58,    11,    11,
      11,    11,    63,    63,    11,    11,    11,    11,    51,    51,
      11,    11,    11,    11,    36,    63,    36,    63,    11,    11,
      11,    11,    43,    63,    43,    63
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    23,    24,    24,    25,    25,    25,    25,    26,    27,
      27,    27,    27,    27,    27,    27,    27,    27,    27,    27,
      27,    27,    28,    28,    28,    28,    29,    29,    30,    30,
      31,    31,    32,    33,    34,    34,    34,    34,    34,    34,
      34,    34,    34,    34,    34,    34,    34,    34,    35,    35,
      35,    35,    36,    36,    37,    37,    38,    38,    39,    39,
      40,    41,    41,    41,    41,    41,    41,    41,    41,    41,
      41,    41,    41,    41,    41,    42,    42,    42,    42,    43,
      43,    44,    44,    45,    45,    46,    46,    47,    47,    48,
      48,    48,    48,    48,    49,    49,    49,    49,    49,    49,
      49,    49,    49,    49,    49,    49,    49,    49,    50,    50,
      50,    50,    51,    51,    52,    52,    53,    53,    53,    53,
      54,    55,    55,    56,    56,    57,    57,    57,    58,    58,
      59,    59,    60,    60,    60,    61,    61,    62,    62,    63,
      63
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     2,     1,
       3,     2,     4,     4,     3,     3,     2,     2,     4,     4,
       3,     3,     1,     1,     2,     2,     1,     3,     2,     2,
       1,     2,     1,     2,     1,     3,     4,     4,     3,     3,
       3,     3,     2,     2,     4,     4,     3,     3,     1,     1,
       2,     2,     1,     3,     2,     2,     1,     2,     1,     1,
       2,     1,     3,     4,     4,     3,     3,     3,     3,     2,
       2,     4,     4,     3,     3,     1,     1,     2,     2,     1,
       3,     2,     2,     1,     2,     1,     1,     1,     2,     2,
       5,     5,     4,     4,     1,     3,     4,     4,     3,     3,
       3,     3,     2,     2,     4,     4,     3,     3,     1,     1,
       2,     2,     1,     3,     2,     2,     2,     3,     4,     3,
       1,     1,     2,     1,     1,     0,     1,     1,     0,     1,
       2,     2,     2,     2,     1,     2,     2,     2,     2,     1,
       1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

/* User initialization code.  */
#line 129 "src/NumberReader.y"
{
  hasExplicitExact = false;
}

#line 1428 "src/NumberReader.tab.cpp"

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 151 "src/NumberReader.y"
                  { currentVM()->numberReaderContext()->setParsed((yyval.object)); YYACCEPT; }
#line 1618 "src/NumberReader.tab.cpp"
    break;

  case 3:
#line 152 "src/NumberReader.y"
              { currentVM()->numberReaderContext()->setParsed(Object::Eof); YYACCEPT; }
#line 1624 "src/NumberReader.tab.cpp"
    break;

  case 8:
#line 156 "src/NumberReader.y"
                             { (yyval.object) = ScannerHelper::applyExactness((yyvsp[-1].exactValue), (yyvsp[0].object)); }
#line 1630 "src/NumberReader.tab.cpp"
    break;

  case 10:
#line 160 "src/NumberReader.y"
                                    { (yyval.object) = Arithmetic::makePolar((yyvsp[-2].object), (yyvsp[0].object)); }
#line 1636 "src/NumberReader.tab.cpp"
    break;

  case 11:
#line 161 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 1642 "src/NumberReader.tab.cpp"
    break;

  case 12:
#line 162 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 1648 "src/NumberReader.tab.cpp"
    break;

  case 13:
#line 163 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1654 "src/NumberReader.tab.cpp"
    break;

  case 14:
#line 164 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(1)); }
#line 1660 "src/NumberReader.tab.cpp"
    break;

  case 15:
#line 165 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(-1)); }
#line 1666 "src/NumberReader.tab.cpp"
    break;

  case 16:
#line 166 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(1)); }
#line 1672 "src/NumberReader.tab.cpp"
    break;

  case 17:
#line 167 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(-1)); }
#line 1678 "src/NumberReader.tab.cpp"
    break;

  case 18:
#line 168 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 1684 "src/NumberReader.tab.cpp"
    break;

  case 19:
#line 169 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1690 "src/NumberReader.tab.cpp"
    break;

  case 20:
#line 170 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 1696 "src/NumberReader.tab.cpp"
    break;

  case 21:
#line 171 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1702 "src/NumberReader.tab.cpp"
    break;

  case 24:
#line 175 "src/NumberReader.y"
                         { (yyval.object) = (yyvsp[0].object); }
#line 1708 "src/NumberReader.tab.cpp"
    break;

  case 25:
#line 176 "src/NumberReader.y"
                         { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 1714 "src/NumberReader.tab.cpp"
    break;

  case 27:
#line 180 "src/NumberReader.y"
                                      {
               bool isDiv0Error = false;
               (yyval.object) = Arithmetic::div((yyvsp[-2].object), (yyvsp[0].object), isDiv0Error);
               if (isDiv0Error) {
                   yyerror("division by zero");
                   YYERROR;
               }
          }
#line 1727 "src/NumberReader.tab.cpp"
    break;

  case 28:
#line 190 "src/NumberReader.y"
                         { (yyval.object) = (yyvsp[0].object); }
#line 1733 "src/NumberReader.tab.cpp"
    break;

  case 29:
#line 191 "src/NumberReader.y"
                         { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 1739 "src/NumberReader.tab.cpp"
    break;

  case 30:
#line 194 "src/NumberReader.y"
                   { (yyval.object) = Object::makeFixnum((yyvsp[0].intValue)); }
#line 1745 "src/NumberReader.tab.cpp"
    break;

  case 31:
#line 195 "src/NumberReader.y"
                              {
               (yyval.object) = Arithmetic::add(Arithmetic::mul(2, (yyvsp[-1].object)), Object::makeFixnum((yyvsp[0].intValue)));
          }
#line 1753 "src/NumberReader.tab.cpp"
    break;

  case 33:
#line 202 "src/NumberReader.y"
                            { (yyval.object) = ScannerHelper::applyExactness((yyvsp[-1].exactValue), (yyvsp[0].object)); }
#line 1759 "src/NumberReader.tab.cpp"
    break;

  case 35:
#line 205 "src/NumberReader.y"
                                    { (yyval.object) = Arithmetic::makePolar((yyvsp[-2].object), (yyvsp[0].object)); }
#line 1765 "src/NumberReader.tab.cpp"
    break;

  case 36:
#line 206 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 1771 "src/NumberReader.tab.cpp"
    break;

  case 37:
#line 207 "src/NumberReader.y"
                                    { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1777 "src/NumberReader.tab.cpp"
    break;

  case 38:
#line 208 "src/NumberReader.y"
                                     { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(1)); }
#line 1783 "src/NumberReader.tab.cpp"
    break;

  case 39:
#line 209 "src/NumberReader.y"
                                     { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(-1)); }
#line 1789 "src/NumberReader.tab.cpp"
    break;

  case 40:
#line 210 "src/NumberReader.y"
                                     { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 1795 "src/NumberReader.tab.cpp"
    break;

  case 41:
#line 211 "src/NumberReader.y"
                                     { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1801 "src/NumberReader.tab.cpp"
    break;

  case 42:
#line 212 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(1)); }
#line 1807 "src/NumberReader.tab.cpp"
    break;

  case 43:
#line 213 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(-1)); }
#line 1813 "src/NumberReader.tab.cpp"
    break;

  case 44:
#line 214 "src/NumberReader.y"
                                     { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 1819 "src/NumberReader.tab.cpp"
    break;

  case 45:
#line 215 "src/NumberReader.y"
                                     { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1825 "src/NumberReader.tab.cpp"
    break;

  case 46:
#line 216 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 1831 "src/NumberReader.tab.cpp"
    break;

  case 47:
#line 217 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1837 "src/NumberReader.tab.cpp"
    break;

  case 50:
#line 222 "src/NumberReader.y"
                         { (yyval.object) = (yyvsp[0].object); }
#line 1843 "src/NumberReader.tab.cpp"
    break;

  case 51:
#line 223 "src/NumberReader.y"
                         { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 1849 "src/NumberReader.tab.cpp"
    break;

  case 53:
#line 227 "src/NumberReader.y"
                                      {
               bool isDiv0Error = false;
               (yyval.object) = Arithmetic::div((yyvsp[-2].object), (yyvsp[0].object), isDiv0Error);
               if (isDiv0Error) {
                   yyerror("division by zero");
                   YYERROR;
               }
          }
#line 1862 "src/NumberReader.tab.cpp"
    break;

  case 54:
#line 237 "src/NumberReader.y"
                         { (yyval.object) = (yyvsp[0].object); }
#line 1868 "src/NumberReader.tab.cpp"
    break;

  case 55:
#line 238 "src/NumberReader.y"
                         { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 1874 "src/NumberReader.tab.cpp"
    break;

  case 56:
#line 241 "src/NumberReader.y"
                   { (yyval.object) = Object::makeFixnum((yyvsp[0].intValue)); }
#line 1880 "src/NumberReader.tab.cpp"
    break;

  case 57:
#line 242 "src/NumberReader.y"
                              {
                (yyval.object) = Arithmetic::add(Arithmetic::mul(8, (yyvsp[-1].object)), Object::makeFixnum((yyvsp[0].intValue)));
          }
#line 1888 "src/NumberReader.tab.cpp"
    break;

  case 59:
#line 248 "src/NumberReader.y"
                    { (yyval.intValue) = (yyvsp[0].intValue); }
#line 1894 "src/NumberReader.tab.cpp"
    break;

  case 60:
#line 251 "src/NumberReader.y"
                               { (yyval.object) = ScannerHelper::applyExactness((yyvsp[-1].exactValue), (yyvsp[0].object)); }
#line 1900 "src/NumberReader.tab.cpp"
    break;

  case 62:
#line 254 "src/NumberReader.y"
                                      { (yyval.object) = Arithmetic::makePolar((yyvsp[-2].object), (yyvsp[0].object)); }
#line 1906 "src/NumberReader.tab.cpp"
    break;

  case 63:
#line 255 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 1912 "src/NumberReader.tab.cpp"
    break;

  case 64:
#line 256 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1918 "src/NumberReader.tab.cpp"
    break;

  case 65:
#line 257 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(1)); }
#line 1924 "src/NumberReader.tab.cpp"
    break;

  case 66:
#line 258 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(-1)); }
#line 1930 "src/NumberReader.tab.cpp"
    break;

  case 67:
#line 259 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 1936 "src/NumberReader.tab.cpp"
    break;

  case 68:
#line 260 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1942 "src/NumberReader.tab.cpp"
    break;

  case 69:
#line 261 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(1)); }
#line 1948 "src/NumberReader.tab.cpp"
    break;

  case 70:
#line 262 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(-1)); }
#line 1954 "src/NumberReader.tab.cpp"
    break;

  case 71:
#line 263 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 1960 "src/NumberReader.tab.cpp"
    break;

  case 72:
#line 264 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1966 "src/NumberReader.tab.cpp"
    break;

  case 73:
#line 265 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 1972 "src/NumberReader.tab.cpp"
    break;

  case 74:
#line 266 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 1978 "src/NumberReader.tab.cpp"
    break;

  case 77:
#line 271 "src/NumberReader.y"
                         { (yyval.object) = (yyvsp[0].object); }
#line 1984 "src/NumberReader.tab.cpp"
    break;

  case 78:
#line 272 "src/NumberReader.y"
                         { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 1990 "src/NumberReader.tab.cpp"
    break;

  case 80:
#line 276 "src/NumberReader.y"
                                        {
               bool isDiv0Error = false;
               (yyval.object) = Arithmetic::div((yyvsp[-2].object), (yyvsp[0].object), isDiv0Error);
               if (isDiv0Error) {
                   yyerror("division by zero");
                   YYERROR;
               }
          }
#line 2003 "src/NumberReader.tab.cpp"
    break;

  case 81:
#line 286 "src/NumberReader.y"
                          { (yyval.object) = (yyvsp[0].object); }
#line 2009 "src/NumberReader.tab.cpp"
    break;

  case 82:
#line 287 "src/NumberReader.y"
                          { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 2015 "src/NumberReader.tab.cpp"
    break;

  case 83:
#line 290 "src/NumberReader.y"
                     { (yyval.object) = Object::makeFixnum((yyvsp[0].intValue)); }
#line 2021 "src/NumberReader.tab.cpp"
    break;

  case 84:
#line 291 "src/NumberReader.y"
                                {
                (yyval.object) = Arithmetic::add(Arithmetic::mul(16, (yyvsp[-1].object)), Object::makeFixnum((yyvsp[0].intValue)));
          }
#line 2029 "src/NumberReader.tab.cpp"
    break;

  case 86:
#line 297 "src/NumberReader.y"
                     { (yyval.intValue) = (yyvsp[0].intValue); }
#line 2035 "src/NumberReader.tab.cpp"
    break;

  case 88:
#line 301 "src/NumberReader.y"
                               { (yyval.object) = ScannerHelper::applyExactness((yyvsp[-1].exactValue), (yyvsp[0].object)); }
#line 2041 "src/NumberReader.tab.cpp"
    break;

  case 89:
#line 306 "src/NumberReader.y"
                                   {
            (yyval.object) = ScannerHelper::applyExactness(-1, (yyvsp[0].object));
          }
#line 2049 "src/NumberReader.tab.cpp"
    break;

  case 90:
#line 309 "src/NumberReader.y"
                                             {
            if (Arithmetic::isExactZero((yyvsp[-1].object))) {
              (yyval.object) = Object::makeCompnum(ScannerHelper::applyExactness(-1, (yyvsp[-3].object)), Object::makeFlonum(0.0));
            } else {
              (yyval.object) = ScannerHelper::applyExactness(-1, Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)));
            }
          }
#line 2061 "src/NumberReader.tab.cpp"
    break;

  case 91:
#line 316 "src/NumberReader.y"
                                              {
            if (Arithmetic::isExactZero((yyvsp[-1].object))) {
              (yyval.object) = Object::makeCompnum(ScannerHelper::applyExactness(-1, (yyvsp[-3].object)), Object::makeFlonum(0.0));
            } else {
              (yyval.object) = ScannerHelper::applyExactness(-1, Arithmetic::mul(-1, Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object))));
            }
          }
#line 2073 "src/NumberReader.tab.cpp"
    break;

  case 92:
#line 323 "src/NumberReader.y"
                                       {
            if (Arithmetic::isExactZero((yyvsp[-1].object))) {
              (yyval.object) = Object::makeCompnum(Object::makeFlonum(0.0), Object::makeFlonum(0.0));
            } else {
              (yyval.object) = Object::makeCompnum(Object::makeFlonum(0.0), ScannerHelper::applyExactness(-1, (yyvsp[-1].object)));
            }
          }
#line 2085 "src/NumberReader.tab.cpp"
    break;

  case 93:
#line 330 "src/NumberReader.y"
                                       {
            if (Arithmetic::isExactZero((yyvsp[-1].object))) {
              (yyval.object) = Object::makeCompnum(Object::makeFlonum(0.0), Object::makeFlonum(0.0));
            } else {
              (yyval.object) = Object::makeCompnum(Object::makeFlonum(0.0), ScannerHelper::applyExactness(-1, Arithmetic::mul(-1, (yyvsp[-1].object))));
            }
          }
#line 2097 "src/NumberReader.tab.cpp"
    break;

  case 95:
#line 340 "src/NumberReader.y"
                                      { (yyval.object) = Arithmetic::makePolar((yyvsp[-2].object), (yyvsp[0].object)); }
#line 2103 "src/NumberReader.tab.cpp"
    break;

  case 96:
#line 341 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 2109 "src/NumberReader.tab.cpp"
    break;

  case 97:
#line 342 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 2115 "src/NumberReader.tab.cpp"
    break;

  case 98:
#line 343 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(1)); }
#line 2121 "src/NumberReader.tab.cpp"
    break;

  case 99:
#line 344 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-2].object), Object::makeFixnum(-1)); }
#line 2127 "src/NumberReader.tab.cpp"
    break;

  case 100:
#line 345 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 2133 "src/NumberReader.tab.cpp"
    break;

  case 101:
#line 346 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 2139 "src/NumberReader.tab.cpp"
    break;

  case 102:
#line 347 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(1)); }
#line 2145 "src/NumberReader.tab.cpp"
    break;

  case 103:
#line 348 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Object::makeFixnum(-1)); }
#line 2151 "src/NumberReader.tab.cpp"
    break;

  case 104:
#line 349 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), (yyvsp[-1].object)); }
#line 2157 "src/NumberReader.tab.cpp"
    break;

  case 105:
#line 350 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum((yyvsp[-3].object), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 2163 "src/NumberReader.tab.cpp"
    break;

  case 106:
#line 351 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), (yyvsp[-1].object)); }
#line 2169 "src/NumberReader.tab.cpp"
    break;

  case 107:
#line 352 "src/NumberReader.y"
                                      { (yyval.object) = Object::makeCompnum(Object::makeFixnum(0), Arithmetic::mul(-1, (yyvsp[-1].object))); }
#line 2175 "src/NumberReader.tab.cpp"
    break;

  case 110:
#line 357 "src/NumberReader.y"
                         { (yyval.object) = (yyvsp[0].object); }
#line 2181 "src/NumberReader.tab.cpp"
    break;

  case 111:
#line 358 "src/NumberReader.y"
                         { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 2187 "src/NumberReader.tab.cpp"
    break;

  case 113:
#line 362 "src/NumberReader.y"
                                        {
               bool isDiv0Error = false;
               (yyval.object) = Arithmetic::div((yyvsp[-2].object), (yyvsp[0].object), isDiv0Error);
               if (isDiv0Error) {
                   yyerror("division by zero");
                   YYERROR;
               }
          }
#line 2200 "src/NumberReader.tab.cpp"
    break;

  case 114:
#line 372 "src/NumberReader.y"
                          { (yyval.object) = (yyvsp[0].object); }
#line 2206 "src/NumberReader.tab.cpp"
    break;

  case 115:
#line 373 "src/NumberReader.y"
                          { (yyval.object) = Arithmetic::mul(-1, (yyvsp[0].object)); }
#line 2212 "src/NumberReader.tab.cpp"
    break;

  case 116:
#line 376 "src/NumberReader.y"
                                    {
              // This should be exact because suffix is not present.
              if ((yyvsp[0].stringValue).empty()) {
                  (yyval.object) = Bignum::makeInteger((yyvsp[-1].stringValue));
              } else {
                  if (hasExplicitExact) {
                    // #e-1e1000
                    (yyval.object) = Arithmetic::mul(Bignum::makeInteger((yyvsp[-1].stringValue)), suffixToNumberOld((yyvsp[0].stringValue)));
                  } else {
                    // 1e10000
                    (yyval.object) = ScannerHelper::applyExactness(-1, Arithmetic::mul(Bignum::makeInteger((yyvsp[-1].stringValue)), suffixToNumberOld((yyvsp[0].stringValue))));
                  }
// todo ("#e-1e-1000" (- (expt 10 -1000)))
//                   int suffixNum = suffix($2);
//                   Object z0 = Arithmetic::mul(Bignum::makeInteger($1),
//                                               Arithmetic::expt(Object::makeFixnum(10), Object::makeFixnum(suffixNum)));
//                   z0 = Arithmetic::inexact(z0);
//                   $$ = Object::makeFlonum(FlonumUtil::algorithmR(Bignum::makeInteger($1), suffixNum, z0.toFlonum()->value()));
              }
          }
#line 2237 "src/NumberReader.tab.cpp"
    break;

  case 117:
#line 396 "src/NumberReader.y"
                                        {
              ucs4string ret(UC("."));
              ret += (yyvsp[-1].stringValue);
              if (!(yyvsp[0].stringValue).empty()) {
                  (yyval.object) = Arithmetic::mul(Flonum::fromString(ret), suffixToNumberOld((yyvsp[0].stringValue)));
              } else {

                  (yyval.object) = Flonum::fromString(ret);
              }

          }
#line 2253 "src/NumberReader.tab.cpp"
    break;

  case 118:
#line 407 "src/NumberReader.y"
                                                         {
              ucs4string uinteger10 = (yyvsp[-3].stringValue);
              uinteger10 += (yyvsp[-1].stringValue);
              Object f = Bignum::makeInteger(uinteger10);
              if (!(yyvsp[0].stringValue).empty()) {
                Object e = suffixToNumber((yyvsp[0].stringValue));
                ucs4string fstring = (yyvsp[-3].stringValue);
                fstring += UC(".");
                fstring += (yyvsp[-1].stringValue);
                double z0 = Arithmetic::mul(Flonum::fromString(fstring), suffixToNumberOld((yyvsp[0].stringValue))).toFlonum()->value();
                if (!e.isFixnum()) {
                  yyerror("invalid flonum expression: suffix");
                }
                if (!f.isFixnum()) {
                  yyerror("invalid flonum expression: too large significand");
                }
                const int digit = static_cast<int>((yyvsp[-1].stringValue).size());
                (yyval.object) = Object::makeFlonum(FlonumUtil::algorithmR(f, e.toFixnum() - digit, z0));
              } else {
                ucs4string ret = (yyvsp[-3].stringValue);
                ret += UC(".") + (yyvsp[-1].stringValue);
                (yyval.object) = Flonum::fromString(ret);
              }
          }
#line 2282 "src/NumberReader.tab.cpp"
    break;

  case 119:
#line 431 "src/NumberReader.y"
                                        {
              ucs4string ret = (yyvsp[-2].stringValue);
              ret += UC(".0");
              if (!(yyvsp[0].stringValue).empty()) {
//                  printf("%s %s:%d\n", __func__, __FILE__, __LINE__);fflush(stdout);// debug
                (yyval.object) = Arithmetic::mul(Flonum::fromString(ret), suffixToNumberOld((yyvsp[0].stringValue)));
              } else {
//                  printf("%s %s:%d\n", __func__, __FILE__, __LINE__);fflush(stdout);// debug
                  (yyval.object) = Flonum::fromString(ret);
              }
          }
#line 2298 "src/NumberReader.tab.cpp"
    break;

  case 120:
#line 445 "src/NumberReader.y"
                              { (yyval.object) = Bignum::makeInteger((yyvsp[0].stringValue)); }
#line 2304 "src/NumberReader.tab.cpp"
    break;

  case 121:
#line 447 "src/NumberReader.y"
                            {
                const ucs4char ch = '0' + (yyvsp[0].intValue);
                (yyval.stringValue) = ucs4string(UC(""));
                (yyval.stringValue) += ch;
           }
#line 2314 "src/NumberReader.tab.cpp"
    break;

  case 122:
#line 452 "src/NumberReader.y"
                                       {
               const ucs4char ch = '0' + (yyvsp[0].intValue);
               (yyval.stringValue) = (yyvsp[-1].stringValue);
               (yyval.stringValue) += ch;
          }
#line 2324 "src/NumberReader.tab.cpp"
    break;

  case 124:
#line 460 "src/NumberReader.y"
                             { (yyval.intValue) = (yyvsp[0].intValue); }
#line 2330 "src/NumberReader.tab.cpp"
    break;

  case 125:
#line 463 "src/NumberReader.y"
                            { (yyval.exactValue) = 0; }
#line 2336 "src/NumberReader.tab.cpp"
    break;

  case 126:
#line 464 "src/NumberReader.y"
                            { (yyval.exactValue) = 1; hasExplicitExact = true; }
#line 2342 "src/NumberReader.tab.cpp"
    break;

  case 127:
#line 465 "src/NumberReader.y"
                            { (yyval.exactValue) = -1; }
#line 2348 "src/NumberReader.tab.cpp"
    break;

  case 128:
#line 468 "src/NumberReader.y"
                            { (yyval.stringValue) = ucs4string(UC("")); }
#line 2354 "src/NumberReader.tab.cpp"
    break;

  case 129:
#line 469 "src/NumberReader.y"
                            {
              ucs4string ret(UC("e"));
              ret += (yyvsp[0].stringValue).substr(1, (yyvsp[0].stringValue).size() - 1);
              (yyval.stringValue) = ret;
          }
#line 2364 "src/NumberReader.tab.cpp"
    break;

  case 130:
#line 476 "src/NumberReader.y"
                              { (yyval.exactValue) = (yyvsp[0].exactValue); }
#line 2370 "src/NumberReader.tab.cpp"
    break;

  case 131:
#line 477 "src/NumberReader.y"
                              { (yyval.exactValue) = (yyvsp[-1].exactValue); }
#line 2376 "src/NumberReader.tab.cpp"
    break;

  case 132:
#line 480 "src/NumberReader.y"
                                { (yyval.exactValue) = (yyvsp[0].exactValue);}
#line 2382 "src/NumberReader.tab.cpp"
    break;

  case 133:
#line 481 "src/NumberReader.y"
                                { (yyval.exactValue) = (yyvsp[-1].exactValue);}
#line 2388 "src/NumberReader.tab.cpp"
    break;

  case 135:
#line 485 "src/NumberReader.y"
                               { (yyval.exactValue) = (yyvsp[0].exactValue);}
#line 2394 "src/NumberReader.tab.cpp"
    break;

  case 136:
#line 486 "src/NumberReader.y"
                               { (yyval.exactValue) = (yyvsp[-1].exactValue);}
#line 2400 "src/NumberReader.tab.cpp"
    break;

  case 137:
#line 489 "src/NumberReader.y"
                                { (yyval.exactValue) = (yyvsp[0].exactValue);}
#line 2406 "src/NumberReader.tab.cpp"
    break;

  case 138:
#line 490 "src/NumberReader.y"
                                { (yyval.exactValue) = (yyvsp[-1].exactValue);}
#line 2412 "src/NumberReader.tab.cpp"
    break;

  case 139:
#line 493 "src/NumberReader.y"
                   { (yyval.object) = Flonum::NOT_A_NUMBER; }
#line 2418 "src/NumberReader.tab.cpp"
    break;

  case 140:
#line 494 "src/NumberReader.y"
                   { (yyval.object) = Flonum::POSITIVE_INF; }
#line 2424 "src/NumberReader.tab.cpp"
    break;


#line 2428 "src/NumberReader.tab.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 495 "src/NumberReader.y"


extern ucs4char* token;
int number_yyerror(char const *str)
{
  TextualInputPort* const port = currentVM()->numberReaderContext()->port();
  port->setError(format(nullptr, UC("~a near [~a] at ~a:~d. "),
                        Pair::list4(Object(str), Object::makeString(port->scanner()->currentToken()), Object(port->toString()), Object::makeFixnum(port->getLineNo()))));
  return 0;
}
