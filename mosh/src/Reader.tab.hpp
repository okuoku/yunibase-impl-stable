/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_SRC_READER_TAB_HPP_INCLUDED
# define YY_YY_SRC_READER_TAB_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENTIFIER = 258,
    SCHEME_BOOLEAN = 259,
    STRING = 260,
    CHARACTER = 261,
    CHARACTER_NAME = 262,
    DEFINING_SHARED = 263,
    DEFINED_SHARED = 264,
    REGEXP = 265,
    NUMBER = 266,
    NUMBER2 = 267,
    NUMBER8 = 268,
    NUMBER10 = 269,
    NUMBER16 = 270,
    LEFT_PAREN = 271,
    RIGHT_PAREN = 272,
    END_OF_FILE = 273,
    VECTOR_START = 274,
    BYTE_VECTOR_START = 275,
    DOT = 276,
    DATUM_COMMENT = 277,
    ABBV_QUASIQUOTE = 278,
    ABBV_QUOTE = 279,
    ABBV_UNQUOTESPLICING = 280,
    ABBV_UNQUOTE = 281,
    ABBV_SYNTAX = 282,
    ABBV_QUASISYNTAX = 283,
    ABBV_UNSYNTAXSPLICING = 284,
    ABBV_UNSYNTAX = 285
  };
#endif

/* Value type.  */



int yyparse (void);

#endif /* !YY_YY_SRC_READER_TAB_HPP_INCLUDED  */
