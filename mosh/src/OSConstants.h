// Do not edit, this file is generated from os-constants.scm
#ifdef AF_INET
osConstants->set(Symbol::intern(UC("AF_INET")), Bignum::makeInteger((long int)AF_INET));
#endif
#ifdef AF_INET6
osConstants->set(Symbol::intern(UC("AF_INET6")), Bignum::makeInteger((long int)AF_INET6));
#endif
#ifdef AF_UNSPEC
osConstants->set(Symbol::intern(UC("AF_UNSPEC")), Bignum::makeInteger((long int)AF_UNSPEC));
#endif
#ifdef SOCK_STREAM
osConstants->set(Symbol::intern(UC("SOCK_STREAM")), Bignum::makeInteger((long int)SOCK_STREAM));
#endif
#ifdef SOCK_DGRAM
osConstants->set(Symbol::intern(UC("SOCK_DGRAM")), Bignum::makeInteger((long int)SOCK_DGRAM));
#endif
#ifdef SOCK_RAW
osConstants->set(Symbol::intern(UC("SOCK_RAW")), Bignum::makeInteger((long int)SOCK_RAW));
#endif
#ifdef AI_ADDRCONFIG
osConstants->set(Symbol::intern(UC("AI_ADDRCONFIG")), Bignum::makeInteger((long int)AI_ADDRCONFIG));
#endif
#ifdef AI_ALL
osConstants->set(Symbol::intern(UC("AI_ALL")), Bignum::makeInteger((long int)AI_ALL));
#endif
#ifdef AI_CANONNAME
osConstants->set(Symbol::intern(UC("AI_CANONNAME")), Bignum::makeInteger((long int)AI_CANONNAME));
#endif
#ifdef AI_NUMERICHOST
osConstants->set(Symbol::intern(UC("AI_NUMERICHOST")), Bignum::makeInteger((long int)AI_NUMERICHOST));
#endif
#ifdef AI_NUMERICSERV
osConstants->set(Symbol::intern(UC("AI_NUMERICSERV")), Bignum::makeInteger((long int)AI_NUMERICSERV));
#endif
#ifdef AI_PASSIVE
osConstants->set(Symbol::intern(UC("AI_PASSIVE")), Bignum::makeInteger((long int)AI_PASSIVE));
#endif
#ifdef AI_V4MAPPED
osConstants->set(Symbol::intern(UC("AI_V4MAPPED")), Bignum::makeInteger((long int)AI_V4MAPPED));
#endif
#ifdef IPPROTO_TCP
osConstants->set(Symbol::intern(UC("IPPROTO_TCP")), Bignum::makeInteger((long int)IPPROTO_TCP));
#endif
#ifdef IPPROTO_UDP
osConstants->set(Symbol::intern(UC("IPPROTO_UDP")), Bignum::makeInteger((long int)IPPROTO_UDP));
#endif
#ifdef IPPROTO_RAW
osConstants->set(Symbol::intern(UC("IPPROTO_RAW")), Bignum::makeInteger((long int)IPPROTO_RAW));
#endif
#ifdef SHUT_RD
osConstants->set(Symbol::intern(UC("SHUT_RD")), Bignum::makeInteger((long int)SHUT_RD));
#endif
#ifdef SHUT_WR
osConstants->set(Symbol::intern(UC("SHUT_WR")), Bignum::makeInteger((long int)SHUT_WR));
#endif
#ifdef SHUT_RDWR
osConstants->set(Symbol::intern(UC("SHUT_RDWR")), Bignum::makeInteger((long int)SHUT_RDWR));
#endif
#ifdef SIGHUP
osConstants->set(Symbol::intern(UC("SIGHUP")), Bignum::makeInteger((long int)SIGHUP));
#endif
#ifdef SIGINT
osConstants->set(Symbol::intern(UC("SIGINT")), Bignum::makeInteger((long int)SIGINT));
#endif
#ifdef SIGQUIT
osConstants->set(Symbol::intern(UC("SIGQUIT")), Bignum::makeInteger((long int)SIGQUIT));
#endif
#ifdef SIGILL
osConstants->set(Symbol::intern(UC("SIGILL")), Bignum::makeInteger((long int)SIGILL));
#endif
#ifdef SIGTRAP
osConstants->set(Symbol::intern(UC("SIGTRAP")), Bignum::makeInteger((long int)SIGTRAP));
#endif
#ifdef SIGABRT
osConstants->set(Symbol::intern(UC("SIGABRT")), Bignum::makeInteger((long int)SIGABRT));
#endif
#ifdef SIGEMT
osConstants->set(Symbol::intern(UC("SIGEMT")), Bignum::makeInteger((long int)SIGEMT));
#endif
#ifdef SIGFPE
osConstants->set(Symbol::intern(UC("SIGFPE")), Bignum::makeInteger((long int)SIGFPE));
#endif
#ifdef SIGKILL
osConstants->set(Symbol::intern(UC("SIGKILL")), Bignum::makeInteger((long int)SIGKILL));
#endif
#ifdef SIGBUS
osConstants->set(Symbol::intern(UC("SIGBUS")), Bignum::makeInteger((long int)SIGBUS));
#endif
#ifdef SIGSEGV
osConstants->set(Symbol::intern(UC("SIGSEGV")), Bignum::makeInteger((long int)SIGSEGV));
#endif
#ifdef SIGSYS
osConstants->set(Symbol::intern(UC("SIGSYS")), Bignum::makeInteger((long int)SIGSYS));
#endif
#ifdef SIGPIPE
osConstants->set(Symbol::intern(UC("SIGPIPE")), Bignum::makeInteger((long int)SIGPIPE));
#endif
#ifdef SIGALRM
osConstants->set(Symbol::intern(UC("SIGALRM")), Bignum::makeInteger((long int)SIGALRM));
#endif
#ifdef SIGTERM
osConstants->set(Symbol::intern(UC("SIGTERM")), Bignum::makeInteger((long int)SIGTERM));
#endif
#ifdef SIGURG
osConstants->set(Symbol::intern(UC("SIGURG")), Bignum::makeInteger((long int)SIGURG));
#endif
#ifdef SIGTHR
osConstants->set(Symbol::intern(UC("SIGTHR")), Bignum::makeInteger((long int)SIGTHR));
#endif
#ifdef SIGUSR1
osConstants->set(Symbol::intern(UC("SIGUSR1")), Bignum::makeInteger((long int)SIGUSR1));
#endif
#ifdef SIGUSR2
osConstants->set(Symbol::intern(UC("SIGUSR2")), Bignum::makeInteger((long int)SIGUSR2));
#endif
#ifdef SIGCHLD
osConstants->set(Symbol::intern(UC("SIGCHLD")), Bignum::makeInteger((long int)SIGCHLD));
#endif
#ifdef SIGCONT
osConstants->set(Symbol::intern(UC("SIGCONT")), Bignum::makeInteger((long int)SIGCONT));
#endif
#ifdef SIGSTOP
osConstants->set(Symbol::intern(UC("SIGSTOP")), Bignum::makeInteger((long int)SIGSTOP));
#endif
#ifdef SIGTSTP
osConstants->set(Symbol::intern(UC("SIGTSTP")), Bignum::makeInteger((long int)SIGTSTP));
#endif
#ifdef SIGTTIN
osConstants->set(Symbol::intern(UC("SIGTTIN")), Bignum::makeInteger((long int)SIGTTIN));
#endif
#ifdef SIGTTOU
osConstants->set(Symbol::intern(UC("SIGTTOU")), Bignum::makeInteger((long int)SIGTTOU));
#endif
#ifdef SIGPOLL
osConstants->set(Symbol::intern(UC("SIGPOLL")), Bignum::makeInteger((long int)SIGPOLL));
#endif
#ifdef SIGPROF
osConstants->set(Symbol::intern(UC("SIGPROF")), Bignum::makeInteger((long int)SIGPROF));
#endif
#ifdef SIGSYS
osConstants->set(Symbol::intern(UC("SIGSYS")), Bignum::makeInteger((long int)SIGSYS));
#endif
#ifdef SIGVTALRM
osConstants->set(Symbol::intern(UC("SIGVTALRM")), Bignum::makeInteger((long int)SIGVTALRM));
#endif
#ifdef SIGXCPU
osConstants->set(Symbol::intern(UC("SIGXCPU")), Bignum::makeInteger((long int)SIGXCPU));
#endif
#ifdef SIGXFSZ
osConstants->set(Symbol::intern(UC("SIGXFSZ")), Bignum::makeInteger((long int)SIGXFSZ));
#endif
#ifdef SIGIO
osConstants->set(Symbol::intern(UC("SIGIO")), Bignum::makeInteger((long int)SIGIO));
#endif
#ifdef SIGPWR
osConstants->set(Symbol::intern(UC("SIGPWR")), Bignum::makeInteger((long int)SIGPWR));
#endif
#ifdef SIGINFO
osConstants->set(Symbol::intern(UC("SIGINFO")), Bignum::makeInteger((long int)SIGINFO));
#endif
#ifdef SIGLOST
osConstants->set(Symbol::intern(UC("SIGLOST")), Bignum::makeInteger((long int)SIGLOST));
#endif
#ifdef SIGWINCH
osConstants->set(Symbol::intern(UC("SIGWINCH")), Bignum::makeInteger((long int)SIGWINCH));
#endif
#ifdef MSG_OK
osConstants->set(Symbol::intern(UC("MSG_OK")), Bignum::makeIntegerFromU32((uint32_t)MSG_OK));
#endif
#ifdef MSG_STARTED
osConstants->set(Symbol::intern(UC("MSG_STARTED")), Bignum::makeIntegerFromU32((uint32_t)MSG_STARTED));
#endif
#ifdef MSG_INTERRUPTED
osConstants->set(Symbol::intern(UC("MSG_INTERRUPTED")), Bignum::makeIntegerFromU32((uint32_t)MSG_INTERRUPTED));
#endif
#ifdef MSG_TIMER
osConstants->set(Symbol::intern(UC("MSG_TIMER")), Bignum::makeIntegerFromU32((uint32_t)MSG_TIMER));
#endif
#ifdef MSG_READ_READY
osConstants->set(Symbol::intern(UC("MSG_READ_READY")), Bignum::makeIntegerFromU32((uint32_t)MSG_READ_READY));
#endif
#ifdef MSG_WRITE_READY
osConstants->set(Symbol::intern(UC("MSG_WRITE_READY")), Bignum::makeIntegerFromU32((uint32_t)MSG_WRITE_READY));
#endif
#ifdef MSG_TEXT
osConstants->set(Symbol::intern(UC("MSG_TEXT")), Bignum::makeIntegerFromU32((uint32_t)MSG_TEXT));
#endif
#ifdef MSG_NAME
osConstants->set(Symbol::intern(UC("MSG_NAME")), Bignum::makeIntegerFromU32((uint32_t)MSG_NAME));
#endif
osConstants->set(Symbol::intern(UC("size-of-char")), Bignum::makeInteger(sizeof(char)));
osConstants->set(Symbol::intern(UC("size-of-bool")), Bignum::makeInteger(sizeof(bool)));
osConstants->set(Symbol::intern(UC("size-of-short")), Bignum::makeInteger(sizeof(short)));
osConstants->set(Symbol::intern(UC("size-of-unsigned-short")), Bignum::makeInteger(sizeof(unsigned short)));
osConstants->set(Symbol::intern(UC("size-of-int")), Bignum::makeInteger(sizeof(int)));
osConstants->set(Symbol::intern(UC("size-of-unsigned-int")), Bignum::makeInteger(sizeof(unsigned int)));
osConstants->set(Symbol::intern(UC("size-of-long")), Bignum::makeInteger(sizeof(long)));
osConstants->set(Symbol::intern(UC("size-of-unsigned-long")), Bignum::makeInteger(sizeof(unsigned long)));
osConstants->set(Symbol::intern(UC("size-of-unsigned-long-long")), Bignum::makeInteger(sizeof(unsigned long long)));
osConstants->set(Symbol::intern(UC("size-of-long-long")), Bignum::makeInteger(sizeof(long long)));
osConstants->set(Symbol::intern(UC("size-of-void*")), Bignum::makeInteger(sizeof(void*)));
osConstants->set(Symbol::intern(UC("size-of-size_t")), Bignum::makeInteger(sizeof(size_t)));
osConstants->set(Symbol::intern(UC("size-of-float")), Bignum::makeInteger(sizeof(float)));
osConstants->set(Symbol::intern(UC("size-of-double")), Bignum::makeInteger(sizeof(double)));
{
    struct x { char y; char z; };
    osConstants->set(Symbol::intern(UC("align-of-char")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; bool z; };
    osConstants->set(Symbol::intern(UC("align-of-bool")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; short z; };
    osConstants->set(Symbol::intern(UC("align-of-short")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; unsigned short z; };
    osConstants->set(Symbol::intern(UC("align-of-unsigned-short")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; int z; };
    osConstants->set(Symbol::intern(UC("align-of-int")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; unsigned int z; };
    osConstants->set(Symbol::intern(UC("align-of-unsigned-int")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; long z; };
    osConstants->set(Symbol::intern(UC("align-of-long")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; unsigned long z; };
    osConstants->set(Symbol::intern(UC("align-of-unsigned-long")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; unsigned long long z; };
    osConstants->set(Symbol::intern(UC("align-of-unsigned-long-long")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; long long z; };
    osConstants->set(Symbol::intern(UC("align-of-long-long")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; void* z; };
    osConstants->set(Symbol::intern(UC("align-of-void*")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; size_t z; };
    osConstants->set(Symbol::intern(UC("align-of-size_t")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; float z; };
    osConstants->set(Symbol::intern(UC("align-of-float")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; double z; };
    osConstants->set(Symbol::intern(UC("align-of-double")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; int8_t z; };
    osConstants->set(Symbol::intern(UC("align-of-int8_t")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; int16_t z; };
    osConstants->set(Symbol::intern(UC("align-of-int16_t")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; int32_t z; };
    osConstants->set(Symbol::intern(UC("align-of-int32_t")), Object::makeFixnum(offsetof(x, z)));
}
{
    struct x { char y; int64_t z; };
    osConstants->set(Symbol::intern(UC("align-of-int64_t")), Object::makeFixnum(offsetof(x, z)));
}
#ifdef E2BIG
osConstants->set(Symbol::intern(UC("E2BIG")), Bignum::makeInteger((long int)E2BIG));
#endif
#ifdef EACCES
osConstants->set(Symbol::intern(UC("EACCES")), Bignum::makeInteger((long int)EACCES));
#endif
#ifdef EADDRINUSE
osConstants->set(Symbol::intern(UC("EADDRINUSE")), Bignum::makeInteger((long int)EADDRINUSE));
#endif
#ifdef EADDRNOTAVAIL
osConstants->set(Symbol::intern(UC("EADDRNOTAVAIL")), Bignum::makeInteger((long int)EADDRNOTAVAIL));
#endif
#ifdef EAFNOSUPPORT
osConstants->set(Symbol::intern(UC("EAFNOSUPPORT")), Bignum::makeInteger((long int)EAFNOSUPPORT));
#endif
#ifdef EAGAIN
osConstants->set(Symbol::intern(UC("EAGAIN")), Bignum::makeInteger((long int)EAGAIN));
#endif
#ifdef EALREADY
osConstants->set(Symbol::intern(UC("EALREADY")), Bignum::makeInteger((long int)EALREADY));
#endif
#ifdef EBADF
osConstants->set(Symbol::intern(UC("EBADF")), Bignum::makeInteger((long int)EBADF));
#endif
#ifdef EBADMSG
osConstants->set(Symbol::intern(UC("EBADMSG")), Bignum::makeInteger((long int)EBADMSG));
#endif
#ifdef EBUSY
osConstants->set(Symbol::intern(UC("EBUSY")), Bignum::makeInteger((long int)EBUSY));
#endif
#ifdef ECANCELED
osConstants->set(Symbol::intern(UC("ECANCELED")), Bignum::makeInteger((long int)ECANCELED));
#endif
#ifdef ECHILD
osConstants->set(Symbol::intern(UC("ECHILD")), Bignum::makeInteger((long int)ECHILD));
#endif
#ifdef ECONNABORTED
osConstants->set(Symbol::intern(UC("ECONNABORTED")), Bignum::makeInteger((long int)ECONNABORTED));
#endif
#ifdef ECONNREFUSED
osConstants->set(Symbol::intern(UC("ECONNREFUSED")), Bignum::makeInteger((long int)ECONNREFUSED));
#endif
#ifdef ECONNRESET
osConstants->set(Symbol::intern(UC("ECONNRESET")), Bignum::makeInteger((long int)ECONNRESET));
#endif
#ifdef EDEADLK
osConstants->set(Symbol::intern(UC("EDEADLK")), Bignum::makeInteger((long int)EDEADLK));
#endif
#ifdef EDESTADDRREQ
osConstants->set(Symbol::intern(UC("EDESTADDRREQ")), Bignum::makeInteger((long int)EDESTADDRREQ));
#endif
#ifdef EDOM
osConstants->set(Symbol::intern(UC("EDOM")), Bignum::makeInteger((long int)EDOM));
#endif
#ifdef EDQUOT
osConstants->set(Symbol::intern(UC("EDQUOT")), Bignum::makeInteger((long int)EDQUOT));
#endif
#ifdef EEXIST
osConstants->set(Symbol::intern(UC("EEXIST")), Bignum::makeInteger((long int)EEXIST));
#endif
#ifdef EFAULT
osConstants->set(Symbol::intern(UC("EFAULT")), Bignum::makeInteger((long int)EFAULT));
#endif
#ifdef EFBIG
osConstants->set(Symbol::intern(UC("EFBIG")), Bignum::makeInteger((long int)EFBIG));
#endif
#ifdef EHOSTUNREACH
osConstants->set(Symbol::intern(UC("EHOSTUNREACH")), Bignum::makeInteger((long int)EHOSTUNREACH));
#endif
#ifdef EIDRM
osConstants->set(Symbol::intern(UC("EIDRM")), Bignum::makeInteger((long int)EIDRM));
#endif
#ifdef EILSEQ
osConstants->set(Symbol::intern(UC("EILSEQ")), Bignum::makeInteger((long int)EILSEQ));
#endif
#ifdef EINPROGRESS
osConstants->set(Symbol::intern(UC("EINPROGRESS")), Bignum::makeInteger((long int)EINPROGRESS));
#endif
#ifdef EINTR
osConstants->set(Symbol::intern(UC("EINTR")), Bignum::makeInteger((long int)EINTR));
#endif
#ifdef EINVAL
osConstants->set(Symbol::intern(UC("EINVAL")), Bignum::makeInteger((long int)EINVAL));
#endif
#ifdef EIO
osConstants->set(Symbol::intern(UC("EIO")), Bignum::makeInteger((long int)EIO));
#endif
#ifdef EISCONN
osConstants->set(Symbol::intern(UC("EISCONN")), Bignum::makeInteger((long int)EISCONN));
#endif
#ifdef EISDIR
osConstants->set(Symbol::intern(UC("EISDIR")), Bignum::makeInteger((long int)EISDIR));
#endif
#ifdef ELOOP
osConstants->set(Symbol::intern(UC("ELOOP")), Bignum::makeInteger((long int)ELOOP));
#endif
#ifdef EMFILE
osConstants->set(Symbol::intern(UC("EMFILE")), Bignum::makeInteger((long int)EMFILE));
#endif
#ifdef EMLINK
osConstants->set(Symbol::intern(UC("EMLINK")), Bignum::makeInteger((long int)EMLINK));
#endif
#ifdef EMSGSIZE
osConstants->set(Symbol::intern(UC("EMSGSIZE")), Bignum::makeInteger((long int)EMSGSIZE));
#endif
#ifdef EMULTIHOP
osConstants->set(Symbol::intern(UC("EMULTIHOP")), Bignum::makeInteger((long int)EMULTIHOP));
#endif
#ifdef ENAMETOOLONG
osConstants->set(Symbol::intern(UC("ENAMETOOLONG")), Bignum::makeInteger((long int)ENAMETOOLONG));
#endif
#ifdef ENETDOWN
osConstants->set(Symbol::intern(UC("ENETDOWN")), Bignum::makeInteger((long int)ENETDOWN));
#endif
#ifdef ENETRESET
osConstants->set(Symbol::intern(UC("ENETRESET")), Bignum::makeInteger((long int)ENETRESET));
#endif
#ifdef ENETUNREACH
osConstants->set(Symbol::intern(UC("ENETUNREACH")), Bignum::makeInteger((long int)ENETUNREACH));
#endif
#ifdef ENFILE
osConstants->set(Symbol::intern(UC("ENFILE")), Bignum::makeInteger((long int)ENFILE));
#endif
#ifdef ENOBUFS
osConstants->set(Symbol::intern(UC("ENOBUFS")), Bignum::makeInteger((long int)ENOBUFS));
#endif
#ifdef ENODEV
osConstants->set(Symbol::intern(UC("ENODEV")), Bignum::makeInteger((long int)ENODEV));
#endif
#ifdef ENOENT
osConstants->set(Symbol::intern(UC("ENOENT")), Bignum::makeInteger((long int)ENOENT));
#endif
#ifdef ENOEXEC
osConstants->set(Symbol::intern(UC("ENOEXEC")), Bignum::makeInteger((long int)ENOEXEC));
#endif
#ifdef ENOLCK
osConstants->set(Symbol::intern(UC("ENOLCK")), Bignum::makeInteger((long int)ENOLCK));
#endif
#ifdef ENOLINK
osConstants->set(Symbol::intern(UC("ENOLINK")), Bignum::makeInteger((long int)ENOLINK));
#endif
#ifdef ENOMEM
osConstants->set(Symbol::intern(UC("ENOMEM")), Bignum::makeInteger((long int)ENOMEM));
#endif
#ifdef ENOMSG
osConstants->set(Symbol::intern(UC("ENOMSG")), Bignum::makeInteger((long int)ENOMSG));
#endif
#ifdef ENOPROTOOPT
osConstants->set(Symbol::intern(UC("ENOPROTOOPT")), Bignum::makeInteger((long int)ENOPROTOOPT));
#endif
#ifdef ENOSPC
osConstants->set(Symbol::intern(UC("ENOSPC")), Bignum::makeInteger((long int)ENOSPC));
#endif
#ifdef ENOSR
osConstants->set(Symbol::intern(UC("ENOSR")), Bignum::makeInteger((long int)ENOSR));
#endif
#ifdef ENOSTR
osConstants->set(Symbol::intern(UC("ENOSTR")), Bignum::makeInteger((long int)ENOSTR));
#endif
#ifdef ENOSYS
osConstants->set(Symbol::intern(UC("ENOSYS")), Bignum::makeInteger((long int)ENOSYS));
#endif
#ifdef ENOTCONN
osConstants->set(Symbol::intern(UC("ENOTCONN")), Bignum::makeInteger((long int)ENOTCONN));
#endif
#ifdef ENOTDIR
osConstants->set(Symbol::intern(UC("ENOTDIR")), Bignum::makeInteger((long int)ENOTDIR));
#endif
#ifdef ENOTEMPTY
osConstants->set(Symbol::intern(UC("ENOTEMPTY")), Bignum::makeInteger((long int)ENOTEMPTY));
#endif
#ifdef ENOTRECOVERABLE
osConstants->set(Symbol::intern(UC("ENOTRECOVERABLE")), Bignum::makeInteger((long int)ENOTRECOVERABLE));
#endif
#ifdef ENOTSOCK
osConstants->set(Symbol::intern(UC("ENOTSOCK")), Bignum::makeInteger((long int)ENOTSOCK));
#endif
#ifdef ENOTSUP
osConstants->set(Symbol::intern(UC("ENOTSUP")), Bignum::makeInteger((long int)ENOTSUP));
#endif
#ifdef ENOTTY
osConstants->set(Symbol::intern(UC("ENOTTY")), Bignum::makeInteger((long int)ENOTTY));
#endif
#ifdef ENXIO
osConstants->set(Symbol::intern(UC("ENXIO")), Bignum::makeInteger((long int)ENXIO));
#endif
#ifdef EOPNOTSUPP
osConstants->set(Symbol::intern(UC("EOPNOTSUPP")), Bignum::makeInteger((long int)EOPNOTSUPP));
#endif
#ifdef EOVERFLOW
osConstants->set(Symbol::intern(UC("EOVERFLOW")), Bignum::makeInteger((long int)EOVERFLOW));
#endif
#ifdef EOWNERDEAD
osConstants->set(Symbol::intern(UC("EOWNERDEAD")), Bignum::makeInteger((long int)EOWNERDEAD));
#endif
#ifdef EPERM
osConstants->set(Symbol::intern(UC("EPERM")), Bignum::makeInteger((long int)EPERM));
#endif
#ifdef EPIPE
osConstants->set(Symbol::intern(UC("EPIPE")), Bignum::makeInteger((long int)EPIPE));
#endif
#ifdef EPROTO
osConstants->set(Symbol::intern(UC("EPROTO")), Bignum::makeInteger((long int)EPROTO));
#endif
#ifdef EPROTONOSUPPORT
osConstants->set(Symbol::intern(UC("EPROTONOSUPPORT")), Bignum::makeInteger((long int)EPROTONOSUPPORT));
#endif
#ifdef EPROTOTYPE
osConstants->set(Symbol::intern(UC("EPROTOTYPE")), Bignum::makeInteger((long int)EPROTOTYPE));
#endif
#ifdef ERANGE
osConstants->set(Symbol::intern(UC("ERANGE")), Bignum::makeInteger((long int)ERANGE));
#endif
#ifdef EROFS
osConstants->set(Symbol::intern(UC("EROFS")), Bignum::makeInteger((long int)EROFS));
#endif
#ifdef ESPIPE
osConstants->set(Symbol::intern(UC("ESPIPE")), Bignum::makeInteger((long int)ESPIPE));
#endif
#ifdef ESRCH
osConstants->set(Symbol::intern(UC("ESRCH")), Bignum::makeInteger((long int)ESRCH));
#endif
#ifdef ESTALE
osConstants->set(Symbol::intern(UC("ESTALE")), Bignum::makeInteger((long int)ESTALE));
#endif
#ifdef ETIME
osConstants->set(Symbol::intern(UC("ETIME")), Bignum::makeInteger((long int)ETIME));
#endif
#ifdef ETIMEDOUT
osConstants->set(Symbol::intern(UC("ETIMEDOUT")), Bignum::makeInteger((long int)ETIMEDOUT));
#endif
#ifdef ETXTBSY
osConstants->set(Symbol::intern(UC("ETXTBSY")), Bignum::makeInteger((long int)ETXTBSY));
#endif
#ifdef EWOULDBLOCK
osConstants->set(Symbol::intern(UC("EWOULDBLOCK")), Bignum::makeInteger((long int)EWOULDBLOCK));
#endif
#ifdef EXDEV
osConstants->set(Symbol::intern(UC("EXDEV")), Bignum::makeInteger((long int)EXDEV));
#endif
#ifdef _CS_PATH
osConstants->set(Symbol::intern(UC("_CS_PATH")), Bignum::makeInteger((long int)_CS_PATH));
#endif
#ifdef _CS_POSIX_V7_ILP32_OFF32_CFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_ILP32_OFF32_CFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_ILP32_OFF32_CFLAGS));
#endif
#ifdef _CS_POSIX_V7_ILP32_OFF32_LDFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_ILP32_OFF32_LDFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_ILP32_OFF32_LDFLAGS));
#endif
#ifdef _CS_POSIX_V7_ILP32_OFF32_LIBS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_ILP32_OFF32_LIBS")), Bignum::makeInteger((long int)_CS_POSIX_V7_ILP32_OFF32_LIBS));
#endif
#ifdef _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS));
#endif
#ifdef _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS));
#endif
#ifdef _CS_POSIX_V7_ILP32_OFFBIG_LIBS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_ILP32_OFFBIG_LIBS")), Bignum::makeInteger((long int)_CS_POSIX_V7_ILP32_OFFBIG_LIBS));
#endif
#ifdef _CS_POSIX_V7_LP64_OFF64_CFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_LP64_OFF64_CFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_LP64_OFF64_CFLAGS));
#endif
#ifdef _CS_POSIX_V7_LP64_OFF64_LDFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_LP64_OFF64_LDFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_LP64_OFF64_LDFLAGS));
#endif
#ifdef _CS_POSIX_V7_LP64_OFF64_LIBS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_LP64_OFF64_LIBS")), Bignum::makeInteger((long int)_CS_POSIX_V7_LP64_OFF64_LIBS));
#endif
#ifdef _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS));
#endif
#ifdef _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS));
#endif
#ifdef _CS_POSIX_V7_LPBIG_OFFBIG_LIBS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_LPBIG_OFFBIG_LIBS")), Bignum::makeInteger((long int)_CS_POSIX_V7_LPBIG_OFFBIG_LIBS));
#endif
#ifdef _CS_POSIX_V7_THREADS_CFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_THREADS_CFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_THREADS_CFLAGS));
#endif
#ifdef _CS_POSIX_V7_THREADS_LDFLAGS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_THREADS_LDFLAGS")), Bignum::makeInteger((long int)_CS_POSIX_V7_THREADS_LDFLAGS));
#endif
#ifdef _CS_POSIX_V7_WIDTH_RESTRICTED_ENVS
osConstants->set(Symbol::intern(UC("_CS_POSIX_V7_WIDTH_RESTRICTED_ENVS")), Bignum::makeInteger((long int)_CS_POSIX_V7_WIDTH_RESTRICTED_ENVS));
#endif
#ifdef _CS_V7_ENV
osConstants->set(Symbol::intern(UC("_CS_V7_ENV")), Bignum::makeInteger((long int)_CS_V7_ENV));
#endif
