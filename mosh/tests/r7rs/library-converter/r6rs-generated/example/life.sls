(library
  (example life)
  (export life)
  (import
    (except (scheme base) set!)
    (scheme write)
    (example grid))
  (begin (define
           (life-count grid i j)
           (define (count i j) (if (ref grid i j) 1 0))
           (+ (count (- i 1) (- j 1))
              (count (- i 1) j)
              (count (- i 1) (+ j 1))
              (count i (- j 1))
              (count i (+ j 1))
              (count (+ i 1) (- j 1))
              (count (+ i 1) j)
              (count (+ i 1) (+ j 1))))
         (define
           (life-alive? grid i j)
           (case (life-count grid i j)
                 ((3) #t)
                 ((2) (ref grid i j))
                 (else #f)))
         (define
           (life-print grid)
           (display "[1H[J")
           (each grid
                 (lambda
                   (i j v)
                   (display (if v "*" " "))
                   (when (= j (- (cols grid) 1)) (newline)))))
         (define
           (life grid iterations)
           (do ((i 0 (+ i 1))
                (grid0 grid grid1)
                (grid1 (make (rows grid) (cols grid)) grid0))
               ((= i iterations))
               (each grid0
                     (lambda
                       (j k v)
                       (let ((a (life-alive? grid0 j k)))
                            (set! grid1 j k a))))
               (life-print grid1)))))
