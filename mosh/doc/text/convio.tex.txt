Title: Simple I/O

The (rnrs io simple (6)) library provides a somewhat more convenient interface for performing textual I/O on ports.

library: (rnrs io simple (6))

Function: eof-object

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_636>

Prototype:
> (eof-object)


Function: eof-object?

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_638>

Prototype:
> (eof-object? obj)


Function: call-with-input-file

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_776>

Prototype:
> (call-with-input-file filename proc)


Function: call-with-output-file

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_778>

Prototype:
> (call-with-output-file filename proc)


Function: input-port?

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_664>

Prototype:
> (input-port? obj)


Function: output-port?

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_710>

Prototype:
> (output-port? obj)


Function: current-input-port

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_678>

Prototype:
> (current-input-port)


Function: current-output-port

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_734>

Prototype:
> (current-output-port)


Function: current-error-port

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_736>

Prototype:
> (current-error-port)


Function: with-input-from-file

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_796>

Prototype:
> (with-input-from-file filename thunk)


Function: with-output-to-file

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_798>

Prototype:
> (with-output-to-file filename thunk)


Function: open-input-file

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_800>

Prototype:
> (open-input-file filename)


Function: open-output-file

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_802>

Prototype:
> (open-output-file filename)


Function: close-input-port

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_804>

Prototype:
> (close-input-port input-port)


Function: close-output-port

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_806>

Prototype:
> (close-output-port output-port)


Function: read-char

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_808>

Prototype:
> (read-char)


Function: read-char

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_808>

Prototype:
> (read-char textual-input-port)


Function: peek-char

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_812>

Prototype:
> (peek-char)


Function: peek-char

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_812>

Prototype:
> (peek-char textual-input-port)


Function: read

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_816>

Prototype:
> (read)


Function: read

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_816>

Prototype:
> (read textual-input-port)


Function: write-char

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_820>

Prototype:
> (write-char char)


Function: write-char

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_820>

Prototype:
> (write-char char textual-output-port)


Function: newline

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_824>

Prototype:
> (newline)


Function: newline

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_824>

Prototype:
> (newline textual-output-port)


Function: display

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_828>

Prototype:
> (display obj)


Function: display

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_828>

Prototype:
> (display obj textual-output-port)


Function: write

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_832>

Prototype:
> (write obj)


Function: write

See <http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-9.html#node_idx_832>

Prototype:
> (write obj textual-output-port)


