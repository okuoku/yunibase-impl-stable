/*===========================================================================*/
/*   (Lalr/lalr.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Lalr/lalr.scm -indent -o objs/obj_u/Lalr/lalr.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___LALR_EXPAND_TYPE_DEFINITIONS
#define BGL___LALR_EXPAND_TYPE_DEFINITIONS
#endif													// BGL___LALR_EXPAND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_setzd2reductionzd2tablez00zz__lalr_expandz00(void);
	extern obj_t BGl_firstzd2reductionzd2zz__lalr_globalz00;
	extern obj_t BGl_za2symvza2z00zz__lalr_rewritez00;
	static obj_t BGl_symbol2484z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_consistentz00zz__lalr_globalz00;
	extern obj_t BGl_nvarsz00zz__lalr_globalz00;
	static obj_t BGl_list2455z00zz__lalr_expandz00 = BUNSPEC;
	extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	extern obj_t BGl_2minz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_initializa7ezd2allz75zz__lalr_globalz00(void);
	extern obj_t BGl_ntermsz00zz__lalr_globalz00;
	static bool_t BGl_loop2ze70ze7zz__lalr_expandz00(long, long, long, obj_t,
		obj_t, obj_t, obj_t, long, bool_t);
	extern obj_t BGl_ngotosz00zz__lalr_globalz00;
	static bool_t BGl_loop2ze71ze7zz__lalr_expandz00(long, obj_t, obj_t, obj_t,
		obj_t, obj_t, long);
	extern obj_t BGl_nshiftsz00zz__lalr_globalz00;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_list2466z00zz__lalr_expandz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_rewritezd2grammarz12zc0zz__lalr_rewritez00(obj_t);
	static obj_t BGl_closurez00zz__lalr_expandz00(obj_t);
	extern obj_t BGl_ritemz00zz__lalr_globalz00;
	extern obj_t BGl_reductionzd2tablezd2zz__lalr_globalz00;
	static bool_t BGl_generatezd2stateszd2zz__lalr_expandz00(void);
	extern obj_t BGl_LAz00zz__lalr_globalz00;
	static obj_t BGl_z62zc3z04anonymousza31363ze3ze5zz__lalr_expandz00(obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_nsymsz00zz__lalr_globalz00;
	static bool_t BGl_loop3ze70ze7zz__lalr_expandz00(obj_t, obj_t, long, obj_t,
		long, obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__lalr_expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_rewritez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_globalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_genz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_utilz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_rprecz00zz__lalr_globalz00;
	extern obj_t BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00;
	static bool_t BGl_initializa7ezd2Fz75zz__lalr_expandz00(void);
	static obj_t BGl_getzd2statezd2zz__lalr_expandz00(obj_t);
	extern obj_t BGl_maxrhsz00zz__lalr_globalz00;
	extern obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	extern obj_t BGl_genzd2lalrzd2codez00zz__lalr_genz00(void);
	extern obj_t BGl_lookaheadsz00zz__lalr_globalz00;
	extern obj_t BGl_shiftzd2symbolzd2zz__lalr_globalz00;
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__lalr_expandz00(void);
	static obj_t BGl_savezd2shiftszd2zz__lalr_expandz00(obj_t);
	static obj_t BGl_buildzd2relationszd2zz__lalr_expandz00(void);
	static obj_t BGl_appendzd2stateszd2zz__lalr_expandz00(void);
	extern obj_t BGl_nrulesz00zz__lalr_globalz00;
	static bool_t BGl_setzd2gotozd2mapz00zz__lalr_expandz00(void);
	static long BGl_mapzd2gotozd2zz__lalr_expandz00(obj_t, obj_t);
	static bool_t BGl_computezd2lookaheadszd2zz__lalr_expandz00(void);
	static obj_t BGl_allocatezd2storagezd2zz__lalr_expandz00(void);
	static bool_t BGl_initializa7ezd2LAz75zz__lalr_expandz00(void);
	static obj_t BGl_allocatezd2itemzd2setsz00zz__lalr_expandz00(void);
	extern obj_t BGl_shiftzd2tablezd2zz__lalr_globalz00;
	static obj_t BGl_cnstzd2initzd2zz__lalr_expandz00(void);
	static obj_t BGl_za2defaultza2z00zz__lalr_expandz00 = BUNSPEC;
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_savezd2reductionszd2zz__lalr_expandz00(obj_t, obj_t);
	extern obj_t bgl_reverse(obj_t);
	extern obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__lalr_expandz00(void);
	extern long bgl_list_length(obj_t);
	extern obj_t BGl_acceszd2symbolzd2zz__lalr_globalz00;
	extern obj_t BGl_fromzd2statezd2zz__lalr_globalz00;
	extern obj_t BGl_nstatesz00zz__lalr_globalz00;
	extern obj_t BGl_gotozd2mapzd2zz__lalr_globalz00;
	static obj_t BGl_importedzd2moduleszd2initz00zz__lalr_expandz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2lalrzd2grammarz00zz__lalr_expandz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__lalr_expandz00(void);
	static obj_t BGl_objectzd2initzd2zz__lalr_expandz00(void);
	extern obj_t BGl_kernelzd2basezd2zz__lalr_globalz00;
	extern bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_nitemsz00zz__lalr_globalz00;
	extern obj_t BGl_derivesz00zz__lalr_globalz00;
	extern obj_t BGl_redzd2setzd2zz__lalr_globalz00;
	extern obj_t BGl_shiftzd2setzd2zz__lalr_globalz00;
	static obj_t BGl_buildzd2tableszd2zz__lalr_expandz00(void);
	extern obj_t BGl_lastzd2shiftzd2zz__lalr_globalz00;
	static bool_t BGl_setzd2nullablezd2zz__lalr_expandz00(void);
	static bool_t BGl_setzd2fderiveszd2zz__lalr_expandz00(void);
	static obj_t BGl_initializa7ezd2statesz75zz__lalr_expandz00(void);
	static obj_t BGl_keyword2456z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_includesz00zz__lalr_globalz00;
	static obj_t BGl_keyword2458z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_sunionz00zz__lalr_utilz00(obj_t, obj_t);
	extern obj_t BGl_cleanzd2plistzd2zz__lalr_rewritez00(void);
	extern obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t BGl_exptz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_newzd2itemsetszd2zz__lalr_expandz00(obj_t);
	extern obj_t BGl_nullablez00zz__lalr_globalz00;
	static obj_t BGl_keyword2460z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_firstzd2shiftzd2zz__lalr_globalz00;
	static bool_t BGl_setzd2firstszd2zz__lalr_expandz00(void);
	extern obj_t BGl_statezd2tablezd2zz__lalr_globalz00;
	extern obj_t BGl_fderivesz00zz__lalr_globalz00;
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	extern obj_t BGl_lastzd2statezd2zz__lalr_globalz00;
	extern obj_t BGl_lookbackz00zz__lalr_globalz00;
	static bool_t BGl_setzd2accessingzd2symbolz00zz__lalr_expandz00(void);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_STATEzd2TABLEzd2SIZEz00zz__lalr_globalz00;
	static bool_t BGl_digraphz00zz__lalr_expandz00(obj_t);
	static obj_t BGl_transposez00zz__lalr_expandz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__lalr_expandz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static bool_t BGl_lalrz00zz__lalr_expandz00(void);
	static bool_t BGl_packzd2grammarzd2zz__lalr_expandz00(void);
	extern obj_t BGl_firstzd2statezd2zz__lalr_globalz00;
	static bool_t BGl_traverseze70ze7zz__lalr_expandz00(long, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rrhsz00zz__lalr_globalz00;
	static obj_t BGl_symbol2444z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_grammarz00zz__lalr_globalz00;
	static obj_t BGl_loopze70ze7zz__lalr_expandz00(obj_t);
	static obj_t BGl_symbol2449z00zz__lalr_expandz00 = BUNSPEC;
	static obj_t BGl_loopze71ze7zz__lalr_expandz00(obj_t);
	static obj_t BGl_loopze72ze7zz__lalr_expandz00(obj_t);
	static bool_t BGl_loopze73ze7zz__lalr_expandz00(obj_t, obj_t, obj_t, obj_t,
		long, long, long);
	static bool_t BGl_loopze74ze7zz__lalr_expandz00(obj_t, obj_t, obj_t, obj_t,
		long, long);
	extern obj_t BGl_finalzd2statezd2zz__lalr_globalz00;
	static obj_t BGl_setzd2maxzd2rhsz00zz__lalr_expandz00(void);
	extern obj_t BGl_firstsz00zz__lalr_globalz00;
	static obj_t BGl_addzd2lookbackzd2edgez00zz__lalr_expandz00(obj_t, obj_t,
		long);
	static obj_t BGl_symbol2451z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_LArulenoz00zz__lalr_globalz00;
	static bool_t BGl_compactzd2actionzd2tablez00zz__lalr_expandz00(void);
	static bool_t BGl_setzd2deriveszd2zz__lalr_expandz00(void);
	static obj_t BGl_newzd2statezd2zz__lalr_expandz00(obj_t);
	static obj_t BGl_symbol2462z00zz__lalr_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2463z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_kernelzd2endzd2zz__lalr_globalz00;
	static obj_t BGl_symbol2469z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_lastzd2reductionzd2zz__lalr_globalz00;
	extern obj_t BGl_tozd2statezd2zz__lalr_globalz00;
	static obj_t BGl_z62expandzd2lalrzd2grammarz62zz__lalr_expandz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_buildzd2rulezd2zz__lalr_expandz00(long);
	static obj_t BGl_TAGzd2594ze70z35zz__lalr_expandz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_quotientz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	static obj_t BGl_addzd2actionze70z35zz__lalr_expandz00(obj_t, long, obj_t);
	static obj_t BGl_symbol2471z00zz__lalr_expandz00 = BUNSPEC;
	static obj_t BGl_symbol2473z00zz__lalr_expandz00 = BUNSPEC;
	extern obj_t BGl_Fz00zz__lalr_globalz00;
	extern obj_t BGl_sinsertz00zz__lalr_utilz00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2479z00zz__lalr_expandz00 = BUNSPEC;
	static bool_t BGl_setzd2shiftzd2tablez00zz__lalr_expandz00(void);
	static bool_t BGl_checkzd2lalrzd2rulesz00zz__lalr_expandz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_actionzd2tablezd2zz__lalr_globalz00;
	extern obj_t BGl_rlhsz00zz__lalr_globalz00;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2lalrzd2grammarzd2envzd2zz__lalr_expandz00,
		BgL_bgl_za762expandza7d2lalr2487z00,
		BGl_z62expandzd2lalrzd2grammarz62zz__lalr_expandz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2445z00zz__lalr_expandz00,
		BgL_bgl_string2445za700za7za7_2488za7, "default", 7);
	      DEFINE_STRING(BGl_string2446z00zz__lalr_expandz00,
		BgL_bgl_string2446za700za7za7_2489za7, "lalr-grammar", 12);
	      DEFINE_STRING(BGl_string2447z00zz__lalr_expandz00,
		BgL_bgl_string2447za700za7za7_2490za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2450z00zz__lalr_expandz00,
		BgL_bgl_string2450za700za7za7_2491za7, "sym-no", 6);
	      DEFINE_STRING(BGl_string2452z00zz__lalr_expandz00,
		BgL_bgl_string2452za700za7za7_2492za7, "prec", 4);
	      DEFINE_STRING(BGl_string2453z00zz__lalr_expandz00,
		BgL_bgl_string2453za700za7za7_2493za7, "Illegal form ", 13);
	      DEFINE_STRING(BGl_string2454z00zz__lalr_expandz00,
		BgL_bgl_string2454za700za7za7_2494za7,
		"Illegal form (Illegal left hand side)", 37);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2448z00zz__lalr_expandz00,
		BgL_bgl_za762za7c3za704anonymo2495za7,
		BGl_z62zc3z04anonymousza31363ze3ze5zz__lalr_expandz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2457z00zz__lalr_expandz00,
		BgL_bgl_string2457za700za7za7_2496za7, "left", 4);
	      DEFINE_STRING(BGl_string2459z00zz__lalr_expandz00,
		BgL_bgl_string2459za700za7za7_2497za7, "right", 5);
	      DEFINE_STRING(BGl_string2461z00zz__lalr_expandz00,
		BgL_bgl_string2461za700za7za7_2498za7, "none", 4);
	      DEFINE_STRING(BGl_string2464z00zz__lalr_expandz00,
		BgL_bgl_string2464za700za7za7_2499za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2465z00zz__lalr_expandz00,
		BgL_bgl_string2465za700za7za7_2500za7, "Illegal token", 13);
	      DEFINE_STRING(BGl_string2467z00zz__lalr_expandz00,
		BgL_bgl_string2467za700za7za7_2501za7, "Error in map-goto", 17);
	      DEFINE_STRING(BGl_string2468z00zz__lalr_expandz00,
		BgL_bgl_string2468za700za7za7_2502za7, "Error in add-lookback-edge : ", 29);
	      DEFINE_STRING(BGl_string2470z00zz__lalr_expandz00,
		BgL_bgl_string2470za700za7za7_2503za7, "bidon", 5);
	      DEFINE_STRING(BGl_string2472z00zz__lalr_expandz00,
		BgL_bgl_string2472za700za7za7_2504za7, "-->", 3);
	      DEFINE_STRING(BGl_string2474z00zz__lalr_expandz00,
		BgL_bgl_string2474za700za7za7_2505za7, "accept", 6);
	      DEFINE_STRING(BGl_string2475z00zz__lalr_expandz00,
		BgL_bgl_string2475za700za7za7_2506za7, "'", 1);
	      DEFINE_STRING(BGl_string2476z00zz__lalr_expandz00,
		BgL_bgl_string2476za700za7za7_2507za7, "\non token `", 11);
	      DEFINE_STRING(BGl_string2477z00zz__lalr_expandz00,
		BgL_bgl_string2477za700za7za7_2508za7, "\n - reduce by rule ", 19);
	      DEFINE_STRING(BGl_string2478z00zz__lalr_expandz00,
		BgL_bgl_string2478za700za7za7_2509za7, "** Reduce/Reduce conflict: ", 27);
	      DEFINE_STRING(BGl_string2480z00zz__lalr_expandz00,
		BgL_bgl_string2480za700za7za7_2510za7, "*error*", 7);
	      DEFINE_STRING(BGl_string2481z00zz__lalr_expandz00,
		BgL_bgl_string2481za700za7za7_2511za7, "\n - reduce rule ", 16);
	      DEFINE_STRING(BGl_string2482z00zz__lalr_expandz00,
		BgL_bgl_string2482za700za7za7_2512za7, "\n - shift to state ", 19);
	      DEFINE_STRING(BGl_string2483z00zz__lalr_expandz00,
		BgL_bgl_string2483za700za7za7_2513za7, "** Shift/Reduce conflict: ", 26);
	      DEFINE_STRING(BGl_string2485z00zz__lalr_expandz00,
		BgL_bgl_string2485za700za7za7_2514za7, "error", 5);
	      DEFINE_STRING(BGl_string2486z00zz__lalr_expandz00,
		BgL_bgl_string2486za700za7za7_2515za7, "__lalr_expand", 13);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2484z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_list2455z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_list2466z00zz__lalr_expandz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_za2defaultza2z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_keyword2456z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_keyword2458z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_keyword2460z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2444z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2449z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2451z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2462z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2463z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2469z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2471z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2473z00zz__lalr_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol2479z00zz__lalr_expandz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__lalr_expandz00(long
		BgL_checksumz00_4490, char *BgL_fromz00_4491)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__lalr_expandz00))
				{
					BGl_requirezd2initializa7ationz75zz__lalr_expandz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__lalr_expandz00();
					BGl_cnstzd2initzd2zz__lalr_expandz00();
					BGl_importedzd2moduleszd2initz00zz__lalr_expandz00();
					return BGl_toplevelzd2initzd2zz__lalr_expandz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			BGl_symbol2444z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2445z00zz__lalr_expandz00);
			BGl_symbol2449z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2450z00zz__lalr_expandz00);
			BGl_symbol2451z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2452z00zz__lalr_expandz00);
			BGl_keyword2456z00zz__lalr_expandz00 =
				bstring_to_keyword(BGl_string2457z00zz__lalr_expandz00);
			BGl_keyword2458z00zz__lalr_expandz00 =
				bstring_to_keyword(BGl_string2459z00zz__lalr_expandz00);
			BGl_keyword2460z00zz__lalr_expandz00 =
				bstring_to_keyword(BGl_string2461z00zz__lalr_expandz00);
			BGl_list2455z00zz__lalr_expandz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2456z00zz__lalr_expandz00,
				MAKE_YOUNG_PAIR(BGl_keyword2458z00zz__lalr_expandz00,
					MAKE_YOUNG_PAIR(BGl_keyword2460z00zz__lalr_expandz00, BNIL)));
			BGl_symbol2462z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2446z00zz__lalr_expandz00);
			BGl_symbol2463z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2464z00zz__lalr_expandz00);
			BGl_list2466z00zz__lalr_expandz00 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
			BGl_symbol2469z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2470z00zz__lalr_expandz00);
			BGl_symbol2471z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2472z00zz__lalr_expandz00);
			BGl_symbol2473z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2474z00zz__lalr_expandz00);
			BGl_symbol2479z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2480z00zz__lalr_expandz00);
			return (BGl_symbol2484z00zz__lalr_expandz00 =
				bstring_to_symbol(BGl_string2485z00zz__lalr_expandz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			return (BGl_za2defaultza2z00zz__lalr_expandz00 =
				BGl_gensymz00zz__r4_symbols_6_4z00(BGl_symbol2444z00zz__lalr_expandz00),
				BUNSPEC);
		}

	}



/* expand-lalr-grammar */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2lalrzd2grammarz00zz__lalr_expandz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Lalr/lalr.scm 62 */
			if (PAIRP(BgL_xz00_3))
				{	/* Lalr/lalr.scm 63 */
					obj_t BgL_arg1359z00_1215;

					BgL_arg1359z00_1215 = CDR(((obj_t) BgL_xz00_3));
					return
						BGl_TAGzd2594ze70z35zz__lalr_expandz00(BgL_ez00_4, BgL_xz00_3,
						BgL_arg1359z00_1215);
				}
			else
				{	/* Lalr/lalr.scm 63 */
					return
						BGl_errorz00zz__errorz00(BGl_string2446z00zz__lalr_expandz00,
						BGl_string2447z00zz__lalr_expandz00, BgL_xz00_3);
				}
		}

	}



/* TAG-594~0 */
	obj_t BGl_TAGzd2594ze70z35zz__lalr_expandz00(obj_t BgL_ez00_4476,
		obj_t BgL_xz00_4475, obj_t BgL_rulesz00_1210)
	{
		{	/* Lalr/lalr.scm 63 */
			if (PAIRP(BgL_rulesz00_1210))
				{	/* Lalr/lalr.scm 65 */
					obj_t BgL_carzd2608zd2_1221;
					obj_t BgL_cdrzd2609zd2_1222;

					BgL_carzd2608zd2_1221 = CAR(((obj_t) BgL_rulesz00_1210));
					BgL_cdrzd2609zd2_1222 = CDR(((obj_t) BgL_rulesz00_1210));
					if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
						(BgL_carzd2608zd2_1221))
						{	/* Lalr/lalr.scm 65 */
							if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
								(BgL_cdrzd2609zd2_1222))
								{	/* Lalr/lalr.scm 65 */
									BGl_checkzd2lalrzd2rulesz00zz__lalr_expandz00(BgL_xz00_4475,
										BgL_carzd2608zd2_1221, BgL_cdrzd2609zd2_1222);
								}
							else
								{	/* Lalr/lalr.scm 65 */
									((bool_t) 0);
								}
						}
					else
						{	/* Lalr/lalr.scm 65 */
							((bool_t) 0);
						}
				}
			else
				{	/* Lalr/lalr.scm 65 */
					((bool_t) 0);
				}
			{	/* Lalr/lalr.scm 68 */
				obj_t BgL_codez00_1225;

				{	/* Lalr/lalr.scm 68 */
					obj_t BgL_exitd1127z00_1226;

					BgL_exitd1127z00_1226 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Lalr/lalr.scm 68 */
						obj_t BgL_arg2429z00_2932;

						{	/* Lalr/lalr.scm 68 */
							obj_t BgL_arg2430z00_2933;

							BgL_arg2430z00_2933 = BGL_EXITD_PROTECT(BgL_exitd1127z00_1226);
							BgL_arg2429z00_2932 =
								MAKE_YOUNG_PAIR(BGl_proc2448z00zz__lalr_expandz00,
								BgL_arg2430z00_2933);
						}
						BGL_EXITD_PROTECT_SET(BgL_exitd1127z00_1226, BgL_arg2429z00_2932);
						BUNSPEC;
					}
					{	/* Lalr/lalr.scm 70 */
						obj_t BgL_tmp1129z00_1228;

						BGl_initializa7ezd2allz75zz__lalr_globalz00();
						BGl_rewritezd2grammarz12zc0zz__lalr_rewritez00(BgL_rulesz00_1210);
						BGl_packzd2grammarzd2zz__lalr_expandz00();
						BGl_setzd2deriveszd2zz__lalr_expandz00();
						BGl_setzd2nullablezd2zz__lalr_expandz00();
						BGl_generatezd2stateszd2zz__lalr_expandz00();
						BGl_lalrz00zz__lalr_expandz00();
						BGl_buildzd2tableszd2zz__lalr_expandz00();
						BGl_compactzd2actionzd2tablez00zz__lalr_expandz00();
						BgL_tmp1129z00_1228 = BGl_genzd2lalrzd2codez00zz__lalr_genz00();
						{	/* Lalr/lalr.scm 68 */
							bool_t BgL_test2521z00_4550;

							{	/* Lalr/lalr.scm 68 */
								obj_t BgL_arg2428z00_2935;

								BgL_arg2428z00_2935 = BGL_EXITD_PROTECT(BgL_exitd1127z00_1226);
								BgL_test2521z00_4550 = PAIRP(BgL_arg2428z00_2935);
							}
							if (BgL_test2521z00_4550)
								{	/* Lalr/lalr.scm 68 */
									obj_t BgL_arg2425z00_2936;

									{	/* Lalr/lalr.scm 68 */
										obj_t BgL_arg2426z00_2937;

										BgL_arg2426z00_2937 =
											BGL_EXITD_PROTECT(BgL_exitd1127z00_1226);
										BgL_arg2425z00_2936 = CDR(((obj_t) BgL_arg2426z00_2937));
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1127z00_1226,
										BgL_arg2425z00_2936);
									BUNSPEC;
								}
							else
								{	/* Lalr/lalr.scm 68 */
									BFALSE;
								}
						}
						BGl_cleanzd2plistzd2zz__lalr_rewritez00();
						BgL_codez00_1225 = BgL_tmp1129z00_1228;
					}
				}
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4476, BgL_codez00_1225, BgL_ez00_4476);
			}
		}

	}



/* &expand-lalr-grammar */
	obj_t BGl_z62expandzd2lalrzd2grammarz62zz__lalr_expandz00(obj_t
		BgL_envz00_4436, obj_t BgL_xz00_4437, obj_t BgL_ez00_4438)
	{
		{	/* Lalr/lalr.scm 62 */
			return
				BGl_expandzd2lalrzd2grammarz00zz__lalr_expandz00(BgL_xz00_4437,
				BgL_ez00_4438);
		}

	}



/* &<@anonymous:1363> */
	obj_t BGl_z62zc3z04anonymousza31363ze3ze5zz__lalr_expandz00(obj_t
		BgL_envz00_4439)
	{
		{	/* Lalr/lalr.scm 68 */
			return BGl_cleanzd2plistzd2zz__lalr_rewritez00();
		}

	}



/* pack-grammar */
	bool_t BGl_packzd2grammarzd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 88 */
			BGl_rlhsz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nrulesz00zz__lalr_globalz00), BFALSE);
			BGl_rrhsz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nrulesz00zz__lalr_globalz00), BFALSE);
			BGl_ritemz00zz__lalr_globalz00 =
				make_vector(
				(1L + (long) CINT(BGl_nitemsz00zz__lalr_globalz00)), BFALSE);
			BGl_rprecz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nrulesz00zz__lalr_globalz00), BFALSE);
			{
				obj_t BgL_pz00_1233;
				long BgL_itemzd2nozd2_1234;
				long BgL_rulezd2nozd2_1235;

				BgL_pz00_1233 = BGl_grammarz00zz__lalr_globalz00;
				BgL_itemzd2nozd2_1234 = 0L;
				BgL_rulezd2nozd2_1235 = 1L;
			BgL_zc3z04anonymousza31365ze3z87_1236:
				if (NULLP(BgL_pz00_1233))
					{	/* Lalr/lalr.scm 95 */
						return ((bool_t) 0);
					}
				else
					{	/* Lalr/lalr.scm 96 */
						obj_t BgL_ntz00_1238;

						{	/* Lalr/lalr.scm 96 */
							obj_t BgL_auxz00_4576;

							{	/* Lalr/lalr.scm 96 */
								obj_t BgL_pairz00_2944;

								BgL_pairz00_2944 = CAR(((obj_t) BgL_pz00_1233));
								BgL_auxz00_4576 = CAR(BgL_pairz00_2944);
							}
							BgL_ntz00_1238 =
								BGl_getpropz00zz__r4_symbols_6_4z00(BgL_auxz00_4576,
								BGl_symbol2449z00zz__lalr_expandz00);
						}
						{	/* Lalr/lalr.scm 97 */
							obj_t BgL_g1131z00_1239;

							{	/* Lalr/lalr.scm 97 */
								obj_t BgL_pairz00_2948;

								BgL_pairz00_2948 = CAR(((obj_t) BgL_pz00_1233));
								BgL_g1131z00_1239 = CDR(BgL_pairz00_2948);
							}
							{
								obj_t BgL_prodsz00_1241;
								long BgL_itzd2no2zd2_1242;
								long BgL_rlzd2no2zd2_1243;

								BgL_prodsz00_1241 = BgL_g1131z00_1239;
								BgL_itzd2no2zd2_1242 = BgL_itemzd2nozd2_1234;
								BgL_rlzd2no2zd2_1243 = BgL_rulezd2nozd2_1235;
							BgL_zc3z04anonymousza31367ze3z87_1244:
								if (NULLP(BgL_prodsz00_1241))
									{	/* Lalr/lalr.scm 99 */
										obj_t BgL_arg1369z00_1246;

										BgL_arg1369z00_1246 = CDR(((obj_t) BgL_pz00_1233));
										{
											long BgL_rulezd2nozd2_4590;
											long BgL_itemzd2nozd2_4589;
											obj_t BgL_pz00_4588;

											BgL_pz00_4588 = BgL_arg1369z00_1246;
											BgL_itemzd2nozd2_4589 = BgL_itzd2no2zd2_1242;
											BgL_rulezd2nozd2_4590 = BgL_rlzd2no2zd2_1243;
											BgL_rulezd2nozd2_1235 = BgL_rulezd2nozd2_4590;
											BgL_itemzd2nozd2_1234 = BgL_itemzd2nozd2_4589;
											BgL_pz00_1233 = BgL_pz00_4588;
											goto BgL_zc3z04anonymousza31365ze3z87_1236;
										}
									}
								else
									{	/* Lalr/lalr.scm 98 */
										VECTOR_SET(BGl_rlhsz00zz__lalr_globalz00,
											BgL_rlzd2no2zd2_1243, BgL_ntz00_1238);
										VECTOR_SET(BGl_rrhsz00zz__lalr_globalz00,
											BgL_rlzd2no2zd2_1243, BINT(BgL_itzd2no2zd2_1242));
										{	/* Lalr/lalr.scm 103 */
											obj_t BgL_g1132z00_1247;

											{	/* Lalr/lalr.scm 103 */
												obj_t BgL_pairz00_2957;

												BgL_pairz00_2957 = CAR(((obj_t) BgL_prodsz00_1241));
												BgL_g1132z00_1247 = CAR(BgL_pairz00_2957);
											}
											{
												obj_t BgL_rhsz00_1249;
												long BgL_itzd2no3zd2_1250;

												BgL_rhsz00_1249 = BgL_g1132z00_1247;
												BgL_itzd2no3zd2_1250 = BgL_itzd2no2zd2_1242;
											BgL_zc3z04anonymousza31370ze3z87_1251:
												if (NULLP(BgL_rhsz00_1249))
													{	/* Lalr/lalr.scm 104 */
														{	/* Lalr/lalr.scm 106 */
															long BgL_arg1372z00_1253;

															BgL_arg1372z00_1253 = NEG(BgL_rlzd2no2zd2_1243);
															VECTOR_SET(BGl_ritemz00zz__lalr_globalz00,
																BgL_itzd2no3zd2_1250,
																BINT(BgL_arg1372z00_1253));
														}
														{	/* Lalr/lalr.scm 107 */
															obj_t BgL_arg1373z00_1254;
															long BgL_arg1375z00_1255;
															long BgL_arg1376z00_1256;

															BgL_arg1373z00_1254 =
																CDR(((obj_t) BgL_prodsz00_1241));
															BgL_arg1375z00_1255 = (BgL_itzd2no3zd2_1250 + 1L);
															BgL_arg1376z00_1256 = (BgL_rlzd2no2zd2_1243 + 1L);
															{
																long BgL_rlzd2no2zd2_4608;
																long BgL_itzd2no2zd2_4607;
																obj_t BgL_prodsz00_4606;

																BgL_prodsz00_4606 = BgL_arg1373z00_1254;
																BgL_itzd2no2zd2_4607 = BgL_arg1375z00_1255;
																BgL_rlzd2no2zd2_4608 = BgL_arg1376z00_1256;
																BgL_rlzd2no2zd2_1243 = BgL_rlzd2no2zd2_4608;
																BgL_itzd2no2zd2_1242 = BgL_itzd2no2zd2_4607;
																BgL_prodsz00_1241 = BgL_prodsz00_4606;
																goto BgL_zc3z04anonymousza31367ze3z87_1244;
															}
														}
													}
												else
													{	/* Lalr/lalr.scm 108 */
														obj_t BgL_symz00_1257;

														BgL_symz00_1257 = CAR(((obj_t) BgL_rhsz00_1249));
														{	/* Lalr/lalr.scm 108 */
															obj_t BgL_asymz00_1258;

															if (PAIRP(BgL_symz00_1257))
																{	/* Lalr/lalr.scm 109 */
																	BgL_asymz00_1258 = CAR(BgL_symz00_1257);
																}
															else
																{	/* Lalr/lalr.scm 109 */
																	BgL_asymz00_1258 = BgL_symz00_1257;
																}
															{	/* Lalr/lalr.scm 109 */
																obj_t BgL_symzd2nozd2_1259;

																BgL_symzd2nozd2_1259 =
																	BGl_getpropz00zz__r4_symbols_6_4z00
																	(BgL_asymz00_1258,
																	BGl_symbol2449z00zz__lalr_expandz00);
																{	/* Lalr/lalr.scm 112 */

																	if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00
																			(BgL_asymz00_1258,
																				BGl_symbol2451z00zz__lalr_expandz00)))
																		{	/* Lalr/lalr.scm 114 */
																			VECTOR_SET(BGl_rprecz00zz__lalr_globalz00,
																				BgL_rlzd2no2zd2_1243,
																				BGl_getpropz00zz__r4_symbols_6_4z00
																				(BgL_asymz00_1258,
																					BGl_symbol2451z00zz__lalr_expandz00));
																		}
																	else
																		{	/* Lalr/lalr.scm 114 */
																			BFALSE;
																		}
																	VECTOR_SET(BGl_ritemz00zz__lalr_globalz00,
																		BgL_itzd2no3zd2_1250, BgL_symzd2nozd2_1259);
																	{	/* Lalr/lalr.scm 117 */
																		obj_t BgL_arg1379z00_1262;
																		long BgL_arg1380z00_1263;

																		BgL_arg1379z00_1262 =
																			CDR(((obj_t) BgL_rhsz00_1249));
																		BgL_arg1380z00_1263 =
																			(BgL_itzd2no3zd2_1250 + 1L);
																		{
																			long BgL_itzd2no3zd2_4625;
																			obj_t BgL_rhsz00_4624;

																			BgL_rhsz00_4624 = BgL_arg1379z00_1262;
																			BgL_itzd2no3zd2_4625 =
																				BgL_arg1380z00_1263;
																			BgL_itzd2no3zd2_1250 =
																				BgL_itzd2no3zd2_4625;
																			BgL_rhsz00_1249 = BgL_rhsz00_4624;
																			goto
																				BgL_zc3z04anonymousza31370ze3z87_1251;
																		}
																	}
																}
															}
														}
													}
											}
										}
									}
							}
						}
					}
			}
		}

	}



/* check-lalr-rules */
	bool_t BGl_checkzd2lalrzd2rulesz00zz__lalr_expandz00(obj_t BgL_xz00_5,
		obj_t BgL_tokensz00_6, obj_t BgL_rulesz00_7)
	{
		{	/* Lalr/lalr.scm 122 */
			{
				obj_t BgL_rulez00_1299;

				{
					obj_t BgL_l1233z00_1271;

					BgL_l1233z00_1271 = BgL_tokensz00_6;
				BgL_zc3z04anonymousza31383ze3z87_1272:
					if (PAIRP(BgL_l1233z00_1271))
						{	/* Lalr/lalr.scm 142 */
							{	/* Lalr/lalr.scm 144 */
								obj_t BgL_tz00_1274;

								BgL_tz00_1274 = CAR(BgL_l1233z00_1271);
								if (SYMBOLP(BgL_tz00_1274))
									{	/* Lalr/lalr.scm 144 */
										BTRUE;
									}
								else
									{	/* Lalr/lalr.scm 146 */
										bool_t BgL_test2529z00_4631;

										if (PAIRP(BgL_tz00_1274))
											{	/* Lalr/lalr.scm 146 */
												BgL_test2529z00_4631 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
														(BgL_tz00_1274),
														BGl_list2455z00zz__lalr_expandz00));
											}
										else
											{	/* Lalr/lalr.scm 146 */
												BgL_test2529z00_4631 = ((bool_t) 0);
											}
										if (BgL_test2529z00_4631)
											{	/* Lalr/lalr.scm 147 */
												obj_t BgL_g1232z00_1279;

												BgL_g1232z00_1279 = CDR(BgL_tz00_1274);
												{
													obj_t BgL_l1230z00_1281;

													{	/* Lalr/lalr.scm 152 */
														bool_t BgL_tmpz00_4638;

														BgL_l1230z00_1281 = BgL_g1232z00_1279;
													BgL_zc3z04anonymousza31389ze3z87_1282:
														if (PAIRP(BgL_l1230z00_1281))
															{	/* Lalr/lalr.scm 152 */
																{	/* Lalr/lalr.scm 148 */
																	obj_t BgL_tz00_1284;

																	BgL_tz00_1284 = CAR(BgL_l1230z00_1281);
																	if (SYMBOLP(BgL_tz00_1284))
																		{	/* Lalr/lalr.scm 148 */
																			BFALSE;
																		}
																	else
																		{	/* Lalr/lalr.scm 148 */
																			BGl_bigloozd2typezd2errorz00zz__errorz00
																				(BGl_symbol2462z00zz__lalr_expandz00,
																				BGl_symbol2463z00zz__lalr_expandz00,
																				BgL_tz00_1284);
																		}
																}
																{
																	obj_t BgL_l1230z00_4645;

																	BgL_l1230z00_4645 = CDR(BgL_l1230z00_1281);
																	BgL_l1230z00_1281 = BgL_l1230z00_4645;
																	goto BgL_zc3z04anonymousza31389ze3z87_1282;
																}
															}
														else
															{	/* Lalr/lalr.scm 152 */
																BgL_tmpz00_4638 = ((bool_t) 1);
															}
														BBOOL(BgL_tmpz00_4638);
													}
												}
											}
										else
											{	/* Lalr/lalr.scm 146 */
												BGl_errorz00zz__errorz00
													(BGl_string2446z00zz__lalr_expandz00,
													BGl_string2465z00zz__lalr_expandz00, BgL_tz00_1274);
											}
									}
							}
							{
								obj_t BgL_l1233z00_4649;

								BgL_l1233z00_4649 = CDR(BgL_l1233z00_1271);
								BgL_l1233z00_1271 = BgL_l1233z00_4649;
								goto BgL_zc3z04anonymousza31383ze3z87_1272;
							}
						}
					else
						{	/* Lalr/lalr.scm 142 */
							((bool_t) 1);
						}
				}
				{
					obj_t BgL_l1235z00_2996;

					BgL_l1235z00_2996 = BgL_rulesz00_7;
				BgL_zc3z04anonymousza31395ze3z87_2995:
					if (PAIRP(BgL_l1235z00_2996))
						{	/* Lalr/lalr.scm 156 */
							BgL_rulez00_1299 = CAR(BgL_l1235z00_2996);
							{
								obj_t BgL_subrulesz00_1301;

								if (PAIRP(BgL_rulez00_1299))
									{	/* Lalr/lalr.scm 141 */
										obj_t BgL_cdrzd2622zd2_1306;

										BgL_cdrzd2622zd2_1306 = CDR(((obj_t) BgL_rulez00_1299));
										{	/* Lalr/lalr.scm 141 */
											bool_t BgL_test2535z00_4657;

											{	/* Lalr/lalr.scm 141 */
												obj_t BgL_tmpz00_4658;

												BgL_tmpz00_4658 = CAR(((obj_t) BgL_rulez00_1299));
												BgL_test2535z00_4657 = SYMBOLP(BgL_tmpz00_4658);
											}
											if (BgL_test2535z00_4657)
												{	/* Lalr/lalr.scm 141 */
													if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
														(BgL_cdrzd2622zd2_1306))
														{	/* Lalr/lalr.scm 141 */
															bool_t BgL_tmpz00_4664;

															BgL_subrulesz00_1301 = BgL_cdrzd2622zd2_1306;
															{
																obj_t BgL_l1228z00_1312;

																BgL_l1228z00_1312 = BgL_subrulesz00_1301;
															BgL_zc3z04anonymousza31406ze3z87_1313:
																if (PAIRP(BgL_l1228z00_1312))
																	{	/* Lalr/lalr.scm 126 */
																		{	/* Lalr/lalr.scm 127 */
																			obj_t BgL_srz00_1315;

																			BgL_srz00_1315 = CAR(BgL_l1228z00_1312);
																			{
																				obj_t BgL_srz00_1316;

																				if (PAIRP(BgL_srz00_1315))
																					{	/* Lalr/lalr.scm 127 */
																						obj_t BgL_carzd2631zd2_1321;

																						BgL_carzd2631zd2_1321 =
																							CAR(((obj_t) BgL_srz00_1315));
																						if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2631zd2_1321))
																							{	/* Lalr/lalr.scm 127 */
																								bool_t BgL_tmpz00_4674;

																								BgL_srz00_1316 =
																									BgL_carzd2631zd2_1321;
																								{
																									obj_t BgL_l1226z00_1324;

																									BgL_l1226z00_1324 =
																										BgL_srz00_1316;
																								BgL_zc3z04anonymousza31410ze3z87_1325:
																									if (PAIRP
																										(BgL_l1226z00_1324))
																										{	/* Lalr/lalr.scm 129 */
																											{	/* Lalr/lalr.scm 130 */
																												obj_t BgL_symz00_1327;

																												BgL_symz00_1327 =
																													CAR
																													(BgL_l1226z00_1324);
																												if (SYMBOLP
																													(BgL_symz00_1327))
																													{	/* Lalr/lalr.scm 130 */
																														BFALSE;
																													}
																												else
																													{	/* Lalr/lalr.scm 130 */
																														BGl_errorz00zz__errorz00
																															(BGl_string2446z00zz__lalr_expandz00,
																															BGl_string2453z00zz__lalr_expandz00,
																															BgL_rulez00_1299);
																													}
																											}
																											{
																												obj_t BgL_l1226z00_4681;

																												BgL_l1226z00_4681 =
																													CDR
																													(BgL_l1226z00_1324);
																												BgL_l1226z00_1324 =
																													BgL_l1226z00_4681;
																												goto
																													BgL_zc3z04anonymousza31410ze3z87_1325;
																											}
																										}
																									else
																										{	/* Lalr/lalr.scm 129 */
																											BgL_tmpz00_4674 =
																												((bool_t) 1);
																										}
																								}
																								BBOOL(BgL_tmpz00_4674);
																							}
																						else
																							{	/* Lalr/lalr.scm 127 */
																								BGl_errorz00zz__errorz00
																									(BGl_string2446z00zz__lalr_expandz00,
																									BGl_string2454z00zz__lalr_expandz00,
																									BgL_rulez00_1299);
																							}
																					}
																				else
																					{	/* Lalr/lalr.scm 127 */
																						BGl_errorz00zz__errorz00
																							(BGl_string2446z00zz__lalr_expandz00,
																							BGl_string2454z00zz__lalr_expandz00,
																							BgL_rulez00_1299);
																					}
																			}
																		}
																		{
																			obj_t BgL_l1228z00_4686;

																			BgL_l1228z00_4686 =
																				CDR(BgL_l1228z00_1312);
																			BgL_l1228z00_1312 = BgL_l1228z00_4686;
																			goto
																				BgL_zc3z04anonymousza31406ze3z87_1313;
																		}
																	}
																else
																	{	/* Lalr/lalr.scm 126 */
																		BgL_tmpz00_4664 = ((bool_t) 1);
																	}
															}
															BBOOL(BgL_tmpz00_4664);
														}
													else
														{	/* Lalr/lalr.scm 141 */
															BGl_errorz00zz__errorz00
																(BGl_string2446z00zz__lalr_expandz00,
																BGl_string2447z00zz__lalr_expandz00,
																BgL_xz00_5);
														}
												}
											else
												{	/* Lalr/lalr.scm 141 */
													BGl_errorz00zz__errorz00
														(BGl_string2446z00zz__lalr_expandz00,
														BGl_string2447z00zz__lalr_expandz00, BgL_xz00_5);
												}
										}
									}
								else
									{	/* Lalr/lalr.scm 141 */
										BGl_errorz00zz__errorz00
											(BGl_string2446z00zz__lalr_expandz00,
											BGl_string2447z00zz__lalr_expandz00, BgL_xz00_5);
									}
							}
							{
								obj_t BgL_l1235z00_4693;

								BgL_l1235z00_4693 = CDR(BgL_l1235z00_2996);
								BgL_l1235z00_2996 = BgL_l1235z00_4693;
								goto BgL_zc3z04anonymousza31395ze3z87_2995;
							}
						}
					else
						{	/* Lalr/lalr.scm 156 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* set-derives */
	bool_t BGl_setzd2deriveszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 165 */
			{	/* Lalr/lalr.scm 166 */
				obj_t BgL_deltsz00_1334;

				BgL_deltsz00_1334 =
					make_vector(
					((long) CINT(BGl_nrulesz00zz__lalr_globalz00) + 1L), BINT(0L));
				{	/* Lalr/lalr.scm 167 */
					obj_t BgL_dsetz00_1335;

					BgL_dsetz00_1335 =
						make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BINT(-1L));
					{
						long BgL_iz00_1337;
						long BgL_jz00_1338;

						BgL_iz00_1337 = 1L;
						BgL_jz00_1338 = 0L;
					BgL_zc3z04anonymousza31415ze3z87_1339:
						if ((BgL_iz00_1337 < (long) CINT(BGl_nrulesz00zz__lalr_globalz00)))
							{	/* Lalr/lalr.scm 171 */
								obj_t BgL_lhsz00_1341;

								BgL_lhsz00_1341 =
									VECTOR_REF(BGl_rlhsz00zz__lalr_globalz00, BgL_iz00_1337);
								if (((long) CINT(BgL_lhsz00_1341) >= 0L))
									{	/* Lalr/lalr.scm 172 */
										{	/* Lalr/lalr.scm 174 */
											obj_t BgL_arg1418z00_1343;

											{	/* Lalr/lalr.scm 174 */
												long BgL_arg1419z00_1344;

												{	/* Lalr/lalr.scm 174 */
													long BgL_kz00_3012;

													BgL_kz00_3012 = (long) CINT(BgL_lhsz00_1341);
													BgL_arg1419z00_1344 =
														(long) CINT(VECTOR_REF(BgL_dsetz00_1335,
															BgL_kz00_3012));
												}
												BgL_arg1418z00_1343 =
													MAKE_YOUNG_PAIR(BINT(BgL_iz00_1337),
													BINT(BgL_arg1419z00_1344));
											}
											VECTOR_SET(BgL_deltsz00_1334, BgL_jz00_1338,
												BgL_arg1418z00_1343);
										}
										VECTOR_SET(BgL_dsetz00_1335,
											(long) CINT(BgL_lhsz00_1341), BINT(BgL_jz00_1338));
										{
											long BgL_jz00_4721;
											long BgL_iz00_4719;

											BgL_iz00_4719 = (BgL_iz00_1337 + 1L);
											BgL_jz00_4721 = (BgL_jz00_1338 + 1L);
											BgL_jz00_1338 = BgL_jz00_4721;
											BgL_iz00_1337 = BgL_iz00_4719;
											goto BgL_zc3z04anonymousza31415ze3z87_1339;
										}
									}
								else
									{
										long BgL_iz00_4723;

										BgL_iz00_4723 = (BgL_iz00_1337 + 1L);
										BgL_iz00_1337 = BgL_iz00_4723;
										goto BgL_zc3z04anonymousza31415ze3z87_1339;
									}
							}
						else
							{	/* Lalr/lalr.scm 170 */
								((bool_t) 0);
							}
					}
					BGl_derivesz00zz__lalr_globalz00 =
						make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BINT(0L));
					{
						long BgL_iz00_1350;

						BgL_iz00_1350 = 0L;
					BgL_zc3z04anonymousza31423ze3z87_1351:
						if ((BgL_iz00_1350 < (long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
							{	/* Lalr/lalr.scm 183 */
								obj_t BgL_qz00_1353;

								{	/* Lalr/lalr.scm 183 */
									long BgL_g1133z00_1355;

									BgL_g1133z00_1355 =
										(long) CINT(VECTOR_REF(BgL_dsetz00_1335, BgL_iz00_1350));
									{
										obj_t BgL_jz00_3045;
										obj_t BgL_sz00_3046;

										BgL_jz00_3045 = BINT(BgL_g1133z00_1355);
										BgL_sz00_3046 = BNIL;
									BgL_loop2z00_3044:
										if (((long) CINT(BgL_jz00_3045) < 0L))
											{	/* Lalr/lalr.scm 184 */
												BgL_qz00_1353 = BgL_sz00_3046;
											}
										else
											{	/* Lalr/lalr.scm 186 */
												obj_t BgL_xz00_3053;

												BgL_xz00_3053 =
													VECTOR_REF(BgL_deltsz00_1334,
													(long) CINT(BgL_jz00_3045));
												{	/* Lalr/lalr.scm 187 */
													obj_t BgL_arg1428z00_3054;
													obj_t BgL_arg1429z00_3055;

													BgL_arg1428z00_3054 = CDR(((obj_t) BgL_xz00_3053));
													{	/* Lalr/lalr.scm 187 */
														obj_t BgL_arg1430z00_3056;

														BgL_arg1430z00_3056 = CAR(((obj_t) BgL_xz00_3053));
														BgL_arg1429z00_3055 =
															MAKE_YOUNG_PAIR(BgL_arg1430z00_3056,
															BgL_sz00_3046);
													}
													{
														obj_t BgL_sz00_4744;
														obj_t BgL_jz00_4743;

														BgL_jz00_4743 = BgL_arg1428z00_3054;
														BgL_sz00_4744 = BgL_arg1429z00_3055;
														BgL_sz00_3046 = BgL_sz00_4744;
														BgL_jz00_3045 = BgL_jz00_4743;
														goto BgL_loop2z00_3044;
													}
												}
											}
									}
								}
								VECTOR_SET(BGl_derivesz00zz__lalr_globalz00, BgL_iz00_1350,
									BgL_qz00_1353);
								{
									long BgL_iz00_4747;

									BgL_iz00_4747 = (BgL_iz00_1350 + 1L);
									BgL_iz00_1350 = BgL_iz00_4747;
									goto BgL_zc3z04anonymousza31423ze3z87_1351;
								}
							}
						else
							{	/* Lalr/lalr.scm 182 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* set-nullable */
	bool_t BGl_setzd2nullablezd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 193 */
			BGl_nullablez00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BFALSE);
			{	/* Lalr/lalr.scm 195 */
				obj_t BgL_squeuez00_1369;
				obj_t BgL_rcountz00_1370;
				obj_t BgL_rsetsz00_1371;
				obj_t BgL_reltsz00_1372;

				BgL_squeuez00_1369 =
					make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BINT(0L));
				BgL_rcountz00_1370 =
					make_vector(
					((long) CINT(BGl_nrulesz00zz__lalr_globalz00) + 1L), BINT(0L));
				BgL_rsetsz00_1371 =
					make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BFALSE);
				{	/* Lalr/lalr.scm 198 */
					long BgL_arg1469z00_1446;

					{	/* Lalr/lalr.scm 198 */
						long BgL_za71za7_3067;

						BgL_za71za7_3067 = (long) CINT(BGl_nitemsz00zz__lalr_globalz00);
						BgL_arg1469z00_1446 =
							(BgL_za71za7_3067 +
							((long) CINT(BGl_nvarsz00zz__lalr_globalz00) + 1L));
					}
					BgL_reltsz00_1372 = make_vector(BgL_arg1469z00_1446, BFALSE);
				}
				return
					BGl_loopze73ze7zz__lalr_expandz00(BgL_squeuez00_1369,
					BgL_rsetsz00_1371, BgL_rcountz00_1370, BgL_reltsz00_1372, 0L, 0L, 0L);
			}
		}

	}



/* loop3~0 */
	bool_t BGl_loop3ze70ze7zz__lalr_expandz00(obj_t BgL_reltsz00_4454,
		obj_t BgL_rsetsz00_4453, long BgL_rulenoz00_4452, obj_t BgL_rcountz00_4451,
		long BgL_s2z00_4450, obj_t BgL_squeuez00_4449, long BgL_r2z00_1401,
		long BgL_p2z00_1402)
	{
		{	/* Lalr/lalr.scm 216 */
		BGl_loop3ze70ze7zz__lalr_expandz00:
			{	/* Lalr/lalr.scm 217 */
				obj_t BgL_symbolz00_1404;

				BgL_symbolz00_1404 =
					VECTOR_REF(BGl_ritemz00zz__lalr_globalz00, BgL_r2z00_1401);
				if (((long) CINT(BgL_symbolz00_1404) > 0L))
					{	/* Lalr/lalr.scm 218 */
						{	/* Lalr/lalr.scm 221 */
							long BgL_arg1447z00_1406;

							BgL_arg1447z00_1406 =
								(
								(long) CINT(VECTOR_REF(BgL_rcountz00_4451,
										BgL_rulenoz00_4452)) + 1L);
							VECTOR_SET(BgL_rcountz00_4451, BgL_rulenoz00_4452,
								BINT(BgL_arg1447z00_1406));
						}
						{	/* Lalr/lalr.scm 223 */
							obj_t BgL_arg1449z00_1408;

							BgL_arg1449z00_1408 =
								MAKE_YOUNG_PAIR(VECTOR_REF(BgL_rsetsz00_4453,
									(long) CINT(BgL_symbolz00_1404)), BINT(BgL_rulenoz00_4452));
							VECTOR_SET(BgL_reltsz00_4454, BgL_p2z00_1402,
								BgL_arg1449z00_1408);
						}
						VECTOR_SET(BgL_rsetsz00_4453,
							(long) CINT(BgL_symbolz00_1404), BINT(BgL_p2z00_1402));
						{
							long BgL_p2z00_4785;
							long BgL_r2z00_4783;

							BgL_r2z00_4783 = (BgL_r2z00_1401 + 1L);
							BgL_p2z00_4785 = (BgL_p2z00_1402 + 1L);
							BgL_p2z00_1402 = BgL_p2z00_4785;
							BgL_r2z00_1401 = BgL_r2z00_4783;
							goto BGl_loop3ze70ze7zz__lalr_expandz00;
						}
					}
				else
					{	/* Lalr/lalr.scm 218 */
						return
							BGl_loopze73ze7zz__lalr_expandz00(BgL_squeuez00_4449,
							BgL_rsetsz00_4453, BgL_rcountz00_4451, BgL_reltsz00_4454,
							(BgL_r2z00_1401 + 1L), BgL_s2z00_4450, BgL_p2z00_1402);
					}
			}
		}

	}



/* loop2~0 */
	bool_t BGl_loop2ze70ze7zz__lalr_expandz00(long BgL_s2z00_4461,
		long BgL_pz00_4460, long BgL_rz00_4459, obj_t BgL_squeuez00_4458,
		obj_t BgL_rsetsz00_4457, obj_t BgL_rcountz00_4456, obj_t BgL_reltsz00_4455,
		long BgL_r1z00_1390, bool_t BgL_anyzd2tokenszd2_1391)
	{
		{	/* Lalr/lalr.scm 210 */
		BGl_loop2ze70ze7zz__lalr_expandz00:
			{	/* Lalr/lalr.scm 211 */
				obj_t BgL_symbolz00_1393;

				BgL_symbolz00_1393 =
					VECTOR_REF(BGl_ritemz00zz__lalr_globalz00, BgL_r1z00_1390);
				{	/* Lalr/lalr.scm 211 */

					if (((long) CINT(BgL_symbolz00_1393) > 0L))
						{	/* Lalr/lalr.scm 213 */
							long BgL_arg1442z00_1395;
							bool_t BgL_arg1443z00_1396;

							BgL_arg1442z00_1395 = (BgL_r1z00_1390 + 1L);
							if (BgL_anyzd2tokenszd2_1391)
								{	/* Lalr/lalr.scm 213 */
									BgL_arg1443z00_1396 = BgL_anyzd2tokenszd2_1391;
								}
							else
								{	/* Lalr/lalr.scm 213 */
									BgL_arg1443z00_1396 =
										(
										(long) CINT(BgL_symbolz00_1393) >=
										(long) CINT(BGl_nvarsz00zz__lalr_globalz00));
								}
							{
								bool_t BgL_anyzd2tokenszd2_4799;
								long BgL_r1z00_4798;

								BgL_r1z00_4798 = BgL_arg1442z00_1395;
								BgL_anyzd2tokenszd2_4799 = BgL_arg1443z00_1396;
								BgL_anyzd2tokenszd2_1391 = BgL_anyzd2tokenszd2_4799;
								BgL_r1z00_1390 = BgL_r1z00_4798;
								goto BGl_loop2ze70ze7zz__lalr_expandz00;
							}
						}
					else
						{	/* Lalr/lalr.scm 212 */
							if (BgL_anyzd2tokenszd2_1391)
								{	/* Lalr/lalr.scm 214 */
									return
										BGl_loopze73ze7zz__lalr_expandz00(BgL_squeuez00_4458,
										BgL_rsetsz00_4457, BgL_rcountz00_4456, BgL_reltsz00_4455,
										(BgL_r1z00_1390 + 1L), BgL_s2z00_4461, BgL_pz00_4460);
								}
							else
								{	/* Lalr/lalr.scm 214 */
									return
										BGl_loop3ze70ze7zz__lalr_expandz00(BgL_reltsz00_4455,
										BgL_rsetsz00_4457, NEG((long) CINT(BgL_symbolz00_1393)),
										BgL_rcountz00_4456, BgL_s2z00_4461, BgL_squeuez00_4458,
										BgL_rz00_4459, BgL_pz00_4460);
		}}}}}

	}



/* loop~4 */
	bool_t BGl_loopze74ze7zz__lalr_expandz00(obj_t BgL_rsetsz00_4465,
		obj_t BgL_squeuez00_4464, obj_t BgL_rcountz00_4463, obj_t BgL_reltsz00_4462,
		long BgL_s1z00_1416, long BgL_s3z00_1417)
	{
		{	/* Lalr/lalr.scm 229 */
			if ((BgL_s1z00_1416 < BgL_s3z00_1417))
				{	/* Lalr/lalr.scm 231 */
					obj_t BgL_g1136z00_1420;

					{	/* Lalr/lalr.scm 231 */
						obj_t BgL_arg1467z00_1442;

						BgL_arg1467z00_1442 =
							VECTOR_REF(BgL_squeuez00_4464, BgL_s1z00_1416);
						BgL_g1136z00_1420 =
							VECTOR_REF(BgL_rsetsz00_4465, (long) CINT(BgL_arg1467z00_1442));
					}
					return
						BGl_loop2ze71ze7zz__lalr_expandz00(BgL_s1z00_1416,
						BgL_squeuez00_4464, BgL_rcountz00_4463, BgL_reltsz00_4462,
						BgL_rsetsz00_4465, BgL_g1136z00_1420, BgL_s3z00_1417);
				}
			else
				{	/* Lalr/lalr.scm 230 */
					return ((bool_t) 0);
				}
		}

	}



/* loop~3 */
	bool_t BGl_loopze73ze7zz__lalr_expandz00(obj_t BgL_squeuez00_4469,
		obj_t BgL_rsetsz00_4468, obj_t BgL_rcountz00_4467, obj_t BgL_reltsz00_4466,
		long BgL_rz00_1374, long BgL_s2z00_1375, long BgL_pz00_1376)
	{
		{	/* Lalr/lalr.scm 199 */
		BGl_loopze73ze7zz__lalr_expandz00:
			{	/* Lalr/lalr.scm 200 */
				obj_t BgL_za2rza2_1378;

				BgL_za2rza2_1378 =
					VECTOR_REF(BGl_ritemz00zz__lalr_globalz00, BgL_rz00_1374);
				if (CBOOL(BgL_za2rza2_1378))
					{	/* Lalr/lalr.scm 201 */
						if (((long) CINT(BgL_za2rza2_1378) < 0L))
							{	/* Lalr/lalr.scm 203 */
								obj_t BgL_symbolz00_1380;

								BgL_symbolz00_1380 =
									VECTOR_REF(BGl_rlhsz00zz__lalr_globalz00,
									NEG((long) CINT(BgL_za2rza2_1378)));
								{	/* Lalr/lalr.scm 204 */
									bool_t BgL_test2553z00_4821;

									if (((long) CINT(BgL_symbolz00_1380) >= 0L))
										{	/* Lalr/lalr.scm 205 */
											bool_t BgL_test2555z00_4825;

											{	/* Lalr/lalr.scm 205 */
												obj_t BgL_vectorz00_3076;
												long BgL_kz00_3077;

												BgL_vectorz00_3076 = BGl_nullablez00zz__lalr_globalz00;
												BgL_kz00_3077 = (long) CINT(BgL_symbolz00_1380);
												BgL_test2555z00_4825 =
													CBOOL(VECTOR_REF(BgL_vectorz00_3076, BgL_kz00_3077));
											}
											if (BgL_test2555z00_4825)
												{	/* Lalr/lalr.scm 205 */
													BgL_test2553z00_4821 = ((bool_t) 0);
												}
											else
												{	/* Lalr/lalr.scm 205 */
													BgL_test2553z00_4821 = ((bool_t) 1);
												}
										}
									else
										{	/* Lalr/lalr.scm 204 */
											BgL_test2553z00_4821 = ((bool_t) 0);
										}
									if (BgL_test2553z00_4821)
										{	/* Lalr/lalr.scm 204 */
											VECTOR_SET(BGl_nullablez00zz__lalr_globalz00,
												(long) CINT(BgL_symbolz00_1380), BTRUE);
											VECTOR_SET(BgL_squeuez00_4469, BgL_s2z00_1375,
												BgL_symbolz00_1380);
											{
												long BgL_s2z00_4834;
												long BgL_rz00_4832;

												BgL_rz00_4832 = (BgL_rz00_1374 + 1L);
												BgL_s2z00_4834 = (BgL_s2z00_1375 + 1L);
												BgL_s2z00_1375 = BgL_s2z00_4834;
												BgL_rz00_1374 = BgL_rz00_4832;
												goto BGl_loopze73ze7zz__lalr_expandz00;
											}
										}
									else
										{	/* Lalr/lalr.scm 204 */
											return ((bool_t) 0);
										}
								}
							}
						else
							{	/* Lalr/lalr.scm 202 */
								return
									BGl_loop2ze70ze7zz__lalr_expandz00(BgL_s2z00_1375,
									BgL_pz00_1376, BgL_rz00_1374, BgL_squeuez00_4469,
									BgL_rsetsz00_4468, BgL_rcountz00_4467, BgL_reltsz00_4466,
									BgL_rz00_1374, ((bool_t) 0));
							}
					}
				else
					{	/* Lalr/lalr.scm 201 */
						return
							BGl_loopze74ze7zz__lalr_expandz00(BgL_rsetsz00_4468,
							BgL_squeuez00_4469, BgL_rcountz00_4467, BgL_reltsz00_4466, 0L,
							BgL_s2z00_1375);
					}
			}
		}

	}



/* loop2~1 */
	bool_t BGl_loop2ze71ze7zz__lalr_expandz00(long BgL_s1z00_4474,
		obj_t BgL_squeuez00_4473, obj_t BgL_rcountz00_4472, obj_t BgL_reltsz00_4471,
		obj_t BgL_rsetsz00_4470, obj_t BgL_pz00_1422, long BgL_s4z00_1423)
	{
		{	/* Lalr/lalr.scm 231 */
			if (CBOOL(BgL_pz00_1422))
				{	/* Lalr/lalr.scm 233 */
					obj_t BgL_xz00_1425;

					BgL_xz00_1425 =
						VECTOR_REF(BgL_reltsz00_4471, (long) CINT(BgL_pz00_1422));
					{	/* Lalr/lalr.scm 233 */
						obj_t BgL_rulenoz00_1426;

						BgL_rulenoz00_1426 = CDR(((obj_t) BgL_xz00_1425));
						{	/* Lalr/lalr.scm 234 */
							long BgL_yz00_1427;

							{	/* Lalr/lalr.scm 235 */
								long BgL_arg1465z00_1439;

								{	/* Lalr/lalr.scm 235 */
									long BgL_kz00_3119;

									BgL_kz00_3119 = (long) CINT(BgL_rulenoz00_1426);
									BgL_arg1465z00_1439 =
										(long) CINT(VECTOR_REF(BgL_rcountz00_4472, BgL_kz00_3119));
								}
								BgL_yz00_1427 = (BgL_arg1465z00_1439 - 1L);
							}
							{	/* Lalr/lalr.scm 235 */

								VECTOR_SET(BgL_rcountz00_4472,
									(long) CINT(BgL_rulenoz00_1426), BINT(BgL_yz00_1427));
								if ((BgL_yz00_1427 == 0L))
									{	/* Lalr/lalr.scm 238 */
										obj_t BgL_symbolz00_1429;

										BgL_symbolz00_1429 =
											VECTOR_REF(BGl_rlhsz00zz__lalr_globalz00,
											(long) CINT(BgL_rulenoz00_1426));
										{	/* Lalr/lalr.scm 239 */
											bool_t BgL_test2558z00_4855;

											if (((long) CINT(BgL_symbolz00_1429) >= 0L))
												{	/* Lalr/lalr.scm 240 */
													bool_t BgL_test2560z00_4859;

													{	/* Lalr/lalr.scm 240 */
														obj_t BgL_vectorz00_3127;
														long BgL_kz00_3128;

														BgL_vectorz00_3127 =
															BGl_nullablez00zz__lalr_globalz00;
														BgL_kz00_3128 = (long) CINT(BgL_symbolz00_1429);
														BgL_test2560z00_4859 =
															CBOOL(VECTOR_REF(BgL_vectorz00_3127,
																BgL_kz00_3128));
													}
													if (BgL_test2560z00_4859)
														{	/* Lalr/lalr.scm 240 */
															BgL_test2558z00_4855 = ((bool_t) 0);
														}
													else
														{	/* Lalr/lalr.scm 240 */
															BgL_test2558z00_4855 = ((bool_t) 1);
														}
												}
											else
												{	/* Lalr/lalr.scm 239 */
													BgL_test2558z00_4855 = ((bool_t) 0);
												}
											if (BgL_test2558z00_4855)
												{	/* Lalr/lalr.scm 239 */
													VECTOR_SET(BGl_nullablez00zz__lalr_globalz00,
														(long) CINT(BgL_symbolz00_1429), BTRUE);
													VECTOR_SET(BgL_squeuez00_4473, BgL_s4z00_1423,
														BgL_symbolz00_1429);
													{	/* Lalr/lalr.scm 244 */
														obj_t BgL_arg1461z00_1433;
														long BgL_arg1462z00_1434;

														BgL_arg1461z00_1433 = CAR(((obj_t) BgL_xz00_1425));
														BgL_arg1462z00_1434 = (BgL_s4z00_1423 + 1L);
														BGl_loop2ze71ze7zz__lalr_expandz00(BgL_s1z00_4474,
															BgL_squeuez00_4473, BgL_rcountz00_4472,
															BgL_reltsz00_4471, BgL_rsetsz00_4470,
															BgL_arg1461z00_1433, BgL_arg1462z00_1434);
												}}
											else
												{	/* Lalr/lalr.scm 245 */
													obj_t BgL_arg1463z00_1435;

													BgL_arg1463z00_1435 = CAR(((obj_t) BgL_xz00_1425));
													BGl_loop2ze71ze7zz__lalr_expandz00(BgL_s1z00_4474,
														BgL_squeuez00_4473, BgL_rcountz00_4472,
														BgL_reltsz00_4471, BgL_rsetsz00_4470,
														BgL_arg1463z00_1435, BgL_s4z00_1423);
												}
										}
									}
								else
									{	/* Lalr/lalr.scm 246 */
										obj_t BgL_arg1464z00_1438;

										BgL_arg1464z00_1438 = CAR(((obj_t) BgL_xz00_1425));
										BGl_loop2ze71ze7zz__lalr_expandz00(BgL_s1z00_4474,
											BgL_squeuez00_4473, BgL_rcountz00_4472, BgL_reltsz00_4471,
											BgL_rsetsz00_4470, BgL_arg1464z00_1438, BgL_s4z00_1423);
									}
							}
						}
					}
				}
			else
				{	/* Lalr/lalr.scm 232 */
					((bool_t) 0);
				}
			return
				BGl_loopze74ze7zz__lalr_expandz00(BgL_rsetsz00_4470, BgL_squeuez00_4473,
				BgL_rcountz00_4472, BgL_reltsz00_4471, (BgL_s1z00_4474 + 1L),
				BgL_s4z00_1423);
		}

	}



/* set-firsts */
	bool_t BGl_setzd2firstszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 256 */
			BGl_firstsz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BNIL);
			{
				long BgL_iz00_1449;

				BgL_iz00_1449 = 0L;
			BgL_zc3z04anonymousza31473ze3z87_1450:
				if ((BgL_iz00_1449 < (long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
					{	/* Lalr/lalr.scm 262 */
						obj_t BgL_g1137z00_1452;

						BgL_g1137z00_1452 =
							VECTOR_REF(BGl_derivesz00zz__lalr_globalz00, BgL_iz00_1449);
						{
							obj_t BgL_spz00_1454;

							BgL_spz00_1454 = BgL_g1137z00_1452;
						BgL_zc3z04anonymousza31475ze3z87_1455:
							if (NULLP(BgL_spz00_1454))
								{
									long BgL_iz00_4886;

									BgL_iz00_4886 = (BgL_iz00_1449 + 1L);
									BgL_iz00_1449 = BgL_iz00_4886;
									goto BgL_zc3z04anonymousza31473ze3z87_1450;
								}
							else
								{	/* Lalr/lalr.scm 265 */
									obj_t BgL_symz00_1458;

									{	/* Lalr/lalr.scm 265 */
										obj_t BgL_arg1483z00_1467;

										{	/* Lalr/lalr.scm 265 */
											obj_t BgL_arg1484z00_1468;

											BgL_arg1484z00_1468 = CAR(((obj_t) BgL_spz00_1454));
											BgL_arg1483z00_1467 =
												VECTOR_REF(BGl_rrhsz00zz__lalr_globalz00,
												(long) CINT(BgL_arg1484z00_1468));
										}
										BgL_symz00_1458 =
											VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
											(long) CINT(BgL_arg1483z00_1467));
									}
									{	/* Lalr/lalr.scm 266 */
										bool_t BgL_test2563z00_4894;

										if (BGl_2zc3zc3zz__r4_numbers_6_5z00(BINT(-1L),
												BgL_symz00_1458))
											{	/* Lalr/lalr.scm 266 */
												bool_t BgL_test2565z00_4898;

												if (INTEGERP(BgL_symz00_1458))
													{	/* Lalr/lalr.scm 266 */
														BgL_test2565z00_4898 =
															INTEGERP(BGl_nvarsz00zz__lalr_globalz00);
													}
												else
													{	/* Lalr/lalr.scm 266 */
														BgL_test2565z00_4898 = ((bool_t) 0);
													}
												if (BgL_test2565z00_4898)
													{	/* Lalr/lalr.scm 266 */
														BgL_test2563z00_4894 =
															(
															(long) CINT(BgL_symz00_1458) <
															(long) CINT(BGl_nvarsz00zz__lalr_globalz00));
													}
												else
													{	/* Lalr/lalr.scm 266 */
														BgL_test2563z00_4894 =
															BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_symz00_1458,
															BGl_nvarsz00zz__lalr_globalz00);
													}
											}
										else
											{	/* Lalr/lalr.scm 266 */
												BgL_test2563z00_4894 = ((bool_t) 0);
											}
										if (BgL_test2563z00_4894)
											{	/* Lalr/lalr.scm 266 */
												VECTOR_SET(BGl_firstsz00zz__lalr_globalz00,
													BgL_iz00_1449,
													BGl_sinsertz00zz__lalr_utilz00(BgL_symz00_1458,
														VECTOR_REF(BGl_firstsz00zz__lalr_globalz00,
															BgL_iz00_1449)));
											}
										else
											{	/* Lalr/lalr.scm 266 */
												BFALSE;
											}
									}
									{	/* Lalr/lalr.scm 268 */
										obj_t BgL_arg1482z00_1466;

										BgL_arg1482z00_1466 = CDR(((obj_t) BgL_spz00_1454));
										{
											obj_t BgL_spz00_4911;

											BgL_spz00_4911 = BgL_arg1482z00_1466;
											BgL_spz00_1454 = BgL_spz00_4911;
											goto BgL_zc3z04anonymousza31475ze3z87_1455;
										}
									}
								}
						}
					}
				else
					{	/* Lalr/lalr.scm 261 */
						((bool_t) 0);
					}
			}
			{
				bool_t BgL_continuez00_1472;

				BgL_continuez00_1472 = ((bool_t) 1);
			BgL_zc3z04anonymousza31485ze3z87_1473:
				if (BgL_continuez00_1472)
					{
						long BgL_iz00_1475;
						bool_t BgL_contz00_1476;

						BgL_iz00_1475 = 0L;
						BgL_contz00_1476 = ((bool_t) 0);
					BgL_zc3z04anonymousza31486ze3z87_1477:
						if ((BgL_iz00_1475 >= (long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
							{
								bool_t BgL_continuez00_4916;

								BgL_continuez00_4916 = BgL_contz00_1476;
								BgL_continuez00_1472 = BgL_continuez00_4916;
								goto BgL_zc3z04anonymousza31485ze3z87_1473;
							}
						else
							{	/* Lalr/lalr.scm 276 */
								obj_t BgL_xz00_1479;

								BgL_xz00_1479 =
									VECTOR_REF(BGl_firstsz00zz__lalr_globalz00, BgL_iz00_1475);
								{	/* Lalr/lalr.scm 276 */
									obj_t BgL_yz00_1480;

									{
										obj_t BgL_lz00_3178;
										obj_t BgL_za7za7_3179;

										BgL_lz00_3178 = BgL_xz00_1479;
										BgL_za7za7_3179 = BgL_xz00_1479;
									BgL_loop3z00_3177:
										if (NULLP(BgL_lz00_3178))
											{	/* Lalr/lalr.scm 278 */
												BgL_yz00_1480 = BgL_za7za7_3179;
											}
										else
											{	/* Lalr/lalr.scm 280 */
												obj_t BgL_arg1494z00_3186;
												obj_t BgL_arg1495z00_3187;

												BgL_arg1494z00_3186 = CDR(((obj_t) BgL_lz00_3178));
												{	/* Lalr/lalr.scm 281 */
													obj_t BgL_arg1497z00_3188;

													{	/* Lalr/lalr.scm 281 */
														obj_t BgL_arg1498z00_3189;

														BgL_arg1498z00_3189 = CAR(((obj_t) BgL_lz00_3178));
														BgL_arg1497z00_3188 =
															VECTOR_REF(BGl_firstsz00zz__lalr_globalz00,
															(long) CINT(BgL_arg1498z00_3189));
													}
													BgL_arg1495z00_3187 =
														BGl_sunionz00zz__lalr_utilz00(BgL_arg1497z00_3188,
														BgL_za7za7_3179);
												}
												{
													obj_t BgL_za7za7_4928;
													obj_t BgL_lz00_4927;

													BgL_lz00_4927 = BgL_arg1494z00_3186;
													BgL_za7za7_4928 = BgL_arg1495z00_3187;
													BgL_za7za7_3179 = BgL_za7za7_4928;
													BgL_lz00_3178 = BgL_lz00_4927;
													goto BgL_loop3z00_3177;
												}
											}
									}
									{	/* Lalr/lalr.scm 277 */

										if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_xz00_1479,
												BgL_yz00_1480))
											{
												long BgL_iz00_4931;

												BgL_iz00_4931 = (BgL_iz00_1475 + 1L);
												BgL_iz00_1475 = BgL_iz00_4931;
												goto BgL_zc3z04anonymousza31486ze3z87_1477;
											}
										else
											{	/* Lalr/lalr.scm 282 */
												VECTOR_SET(BGl_firstsz00zz__lalr_globalz00,
													BgL_iz00_1475, BgL_yz00_1480);
												{
													bool_t BgL_contz00_4936;
													long BgL_iz00_4934;

													BgL_iz00_4934 = (BgL_iz00_1475 + 1L);
													BgL_contz00_4936 = ((bool_t) 1);
													BgL_contz00_1476 = BgL_contz00_4936;
													BgL_iz00_1475 = BgL_iz00_4934;
													goto BgL_zc3z04anonymousza31486ze3z87_1477;
												}
											}
									}
								}
							}
					}
				else
					{	/* Lalr/lalr.scm 272 */
						((bool_t) 0);
					}
			}
			{
				long BgL_iz00_3206;

				BgL_iz00_3206 = 0L;
			BgL_loopz00_3205:
				if ((BgL_iz00_3206 < (long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
					{	/* Lalr/lalr.scm 289 */
						VECTOR_SET(BGl_firstsz00zz__lalr_globalz00, BgL_iz00_3206,
							BGl_sinsertz00zz__lalr_utilz00(BINT(BgL_iz00_3206),
								VECTOR_REF(BGl_firstsz00zz__lalr_globalz00, BgL_iz00_3206)));
						{
							long BgL_iz00_4944;

							BgL_iz00_4944 = (BgL_iz00_3206 + 1L);
							BgL_iz00_3206 = BgL_iz00_4944;
							goto BgL_loopz00_3205;
						}
					}
				else
					{	/* Lalr/lalr.scm 289 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* set-fderives */
	bool_t BGl_setzd2fderiveszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 301 */
			BGl_fderivesz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nvarsz00zz__lalr_globalz00), BFALSE);
			BGl_setzd2firstszd2zz__lalr_expandz00();
			{
				long BgL_iz00_1505;

				BgL_iz00_1505 = 0L;
			BgL_zc3z04anonymousza31504ze3z87_1506:
				if ((BgL_iz00_1505 < (long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
					{	/* Lalr/lalr.scm 308 */
						obj_t BgL_xz00_1508;

						{	/* Lalr/lalr.scm 308 */
							obj_t BgL_g1138z00_1510;

							BgL_g1138z00_1510 =
								VECTOR_REF(BGl_firstsz00zz__lalr_globalz00, BgL_iz00_1505);
							{
								obj_t BgL_lz00_3241;
								obj_t BgL_fdz00_3242;

								BgL_lz00_3241 = BgL_g1138z00_1510;
								BgL_fdz00_3242 = BNIL;
							BgL_loop2z00_3240:
								if (NULLP(BgL_lz00_3241))
									{	/* Lalr/lalr.scm 309 */
										BgL_xz00_1508 = BgL_fdz00_3242;
									}
								else
									{	/* Lalr/lalr.scm 311 */
										obj_t BgL_arg1509z00_3249;
										obj_t BgL_arg1510z00_3250;

										BgL_arg1509z00_3249 = CDR(((obj_t) BgL_lz00_3241));
										{	/* Lalr/lalr.scm 312 */
											obj_t BgL_arg1511z00_3251;

											{	/* Lalr/lalr.scm 312 */
												obj_t BgL_arg1513z00_3252;

												BgL_arg1513z00_3252 = CAR(((obj_t) BgL_lz00_3241));
												BgL_arg1511z00_3251 =
													VECTOR_REF(BGl_derivesz00zz__lalr_globalz00,
													(long) CINT(BgL_arg1513z00_3252));
											}
											BgL_arg1510z00_3250 =
												BGl_sunionz00zz__lalr_utilz00(BgL_arg1511z00_3251,
												BgL_fdz00_3242);
										}
										{
											obj_t BgL_fdz00_4963;
											obj_t BgL_lz00_4962;

											BgL_lz00_4962 = BgL_arg1509z00_3249;
											BgL_fdz00_4963 = BgL_arg1510z00_3250;
											BgL_fdz00_3242 = BgL_fdz00_4963;
											BgL_lz00_3241 = BgL_lz00_4962;
											goto BgL_loop2z00_3240;
										}
									}
							}
						}
						VECTOR_SET(BGl_fderivesz00zz__lalr_globalz00, BgL_iz00_1505,
							BgL_xz00_1508);
						{
							long BgL_iz00_4965;

							BgL_iz00_4965 = (BgL_iz00_1505 + 1L);
							BgL_iz00_1505 = BgL_iz00_4965;
							goto BgL_zc3z04anonymousza31504ze3z87_1506;
						}
					}
				else
					{	/* Lalr/lalr.scm 307 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* closure */
	obj_t BGl_closurez00zz__lalr_expandz00(obj_t BgL_corez00_8)
	{
		{	/* Lalr/lalr.scm 320 */
			{	/* Lalr/lalr.scm 320 */
				obj_t BgL_rulesetz00_1523;

				BgL_rulesetz00_1523 =
					make_vector((long) CINT(BGl_nrulesz00zz__lalr_globalz00), BFALSE);
				{
					obj_t BgL_cspz00_1525;

					BgL_cspz00_1525 = BgL_corez00_8;
				BgL_zc3z04anonymousza31514ze3z87_1526:
					if (NULLP(BgL_cspz00_1525))
						{	/* Lalr/lalr.scm 325 */
							((bool_t) 0);
						}
					else
						{	/* Lalr/lalr.scm 326 */
							obj_t BgL_symz00_1528;

							{	/* Lalr/lalr.scm 326 */
								obj_t BgL_arg1524z00_1543;

								BgL_arg1524z00_1543 = CAR(((obj_t) BgL_cspz00_1525));
								BgL_symz00_1528 =
									VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
									(long) CINT(BgL_arg1524z00_1543));
							}
							{	/* Lalr/lalr.scm 327 */
								bool_t BgL_test2575z00_4975;

								if (BGl_2zc3zc3zz__r4_numbers_6_5z00(BINT(-1L),
										BgL_symz00_1528))
									{	/* Lalr/lalr.scm 327 */
										bool_t BgL_test2577z00_4979;

										if (INTEGERP(BgL_symz00_1528))
											{	/* Lalr/lalr.scm 327 */
												BgL_test2577z00_4979 =
													INTEGERP(BGl_nvarsz00zz__lalr_globalz00);
											}
										else
											{	/* Lalr/lalr.scm 327 */
												BgL_test2577z00_4979 = ((bool_t) 0);
											}
										if (BgL_test2577z00_4979)
											{	/* Lalr/lalr.scm 327 */
												BgL_test2575z00_4975 =
													(
													(long) CINT(BgL_symz00_1528) <
													(long) CINT(BGl_nvarsz00zz__lalr_globalz00));
											}
										else
											{	/* Lalr/lalr.scm 327 */
												BgL_test2575z00_4975 =
													BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_symz00_1528,
													BGl_nvarsz00zz__lalr_globalz00);
											}
									}
								else
									{	/* Lalr/lalr.scm 327 */
										BgL_test2575z00_4975 = ((bool_t) 0);
									}
								if (BgL_test2575z00_4975)
									{	/* Lalr/lalr.scm 328 */
										obj_t BgL_g1140z00_1532;

										BgL_g1140z00_1532 =
											VECTOR_REF(BGl_fderivesz00zz__lalr_globalz00,
											(long) CINT(BgL_symz00_1528));
										{
											obj_t BgL_dspz00_3282;

											BgL_dspz00_3282 = BgL_g1140z00_1532;
										BgL_loop2z00_3281:
											if (NULLP(BgL_dspz00_3282))
												{	/* Lalr/lalr.scm 329 */
													((bool_t) 0);
												}
											else
												{	/* Lalr/lalr.scm 329 */
													{	/* Lalr/lalr.scm 331 */
														obj_t BgL_arg1521z00_3287;

														BgL_arg1521z00_3287 =
															CAR(((obj_t) BgL_dspz00_3282));
														VECTOR_SET(BgL_rulesetz00_1523,
															(long) CINT(BgL_arg1521z00_3287), BTRUE);
													}
													{	/* Lalr/lalr.scm 332 */
														obj_t BgL_arg1522z00_3288;

														BgL_arg1522z00_3288 =
															CDR(((obj_t) BgL_dspz00_3282));
														{
															obj_t BgL_dspz00_4997;

															BgL_dspz00_4997 = BgL_arg1522z00_3288;
															BgL_dspz00_3282 = BgL_dspz00_4997;
															goto BgL_loop2z00_3281;
														}
													}
												}
										}
									}
								else
									{	/* Lalr/lalr.scm 327 */
										((bool_t) 0);
									}
							}
							{	/* Lalr/lalr.scm 333 */
								obj_t BgL_arg1523z00_1542;

								BgL_arg1523z00_1542 = CDR(((obj_t) BgL_cspz00_1525));
								{
									obj_t BgL_cspz00_5000;

									BgL_cspz00_5000 = BgL_arg1523z00_1542;
									BgL_cspz00_1525 = BgL_cspz00_5000;
									goto BgL_zc3z04anonymousza31514ze3z87_1526;
								}
							}
						}
				}
				{
					long BgL_rulenoz00_1547;
					obj_t BgL_cspz00_1548;
					obj_t BgL_itemsetvz00_1549;

					BgL_rulenoz00_1547 = 1L;
					BgL_cspz00_1548 = BgL_corez00_8;
					BgL_itemsetvz00_1549 = BNIL;
				BgL_zc3z04anonymousza31525ze3z87_1550:
					if (
						(BgL_rulenoz00_1547 < (long) CINT(BGl_nrulesz00zz__lalr_globalz00)))
						{	/* Lalr/lalr.scm 336 */
							if (CBOOL(VECTOR_REF(BgL_rulesetz00_1523, BgL_rulenoz00_1547)))
								{	/* Lalr/lalr.scm 338 */
									obj_t BgL_itemnoz00_1553;

									BgL_itemnoz00_1553 =
										VECTOR_REF(BGl_rrhsz00zz__lalr_globalz00,
										BgL_rulenoz00_1547);
									{
										obj_t BgL_cz00_1555;
										obj_t BgL_itemsetv2z00_1556;

										BgL_cz00_1555 = BgL_cspz00_1548;
										BgL_itemsetv2z00_1556 = BgL_itemsetvz00_1549;
									BgL_zc3z04anonymousza31528ze3z87_1557:
										{	/* Lalr/lalr.scm 340 */
											bool_t BgL_test2582z00_5008;

											if (PAIRP(BgL_cz00_1555))
												{	/* Lalr/lalr.scm 340 */
													BgL_test2582z00_5008 =
														(
														(long) CINT(CAR(BgL_cz00_1555)) <
														(long) CINT(BgL_itemnoz00_1553));
												}
											else
												{	/* Lalr/lalr.scm 340 */
													BgL_test2582z00_5008 = ((bool_t) 0);
												}
											if (BgL_test2582z00_5008)
												{	/* Lalr/lalr.scm 342 */
													obj_t BgL_arg1535z00_1561;
													obj_t BgL_arg1536z00_1562;

													BgL_arg1535z00_1561 = CDR(BgL_cz00_1555);
													BgL_arg1536z00_1562 =
														MAKE_YOUNG_PAIR(CAR(BgL_cz00_1555),
														BgL_itemsetv2z00_1556);
													{
														obj_t BgL_itemsetv2z00_5019;
														obj_t BgL_cz00_5018;

														BgL_cz00_5018 = BgL_arg1535z00_1561;
														BgL_itemsetv2z00_5019 = BgL_arg1536z00_1562;
														BgL_itemsetv2z00_1556 = BgL_itemsetv2z00_5019;
														BgL_cz00_1555 = BgL_cz00_5018;
														goto BgL_zc3z04anonymousza31528ze3z87_1557;
													}
												}
											else
												{	/* Lalr/lalr.scm 343 */
													long BgL_arg1539z00_1564;
													obj_t BgL_arg1540z00_1565;

													BgL_arg1539z00_1564 = (BgL_rulenoz00_1547 + 1L);
													BgL_arg1540z00_1565 =
														MAKE_YOUNG_PAIR(BgL_itemnoz00_1553,
														BgL_itemsetv2z00_1556);
													{
														obj_t BgL_itemsetvz00_5024;
														obj_t BgL_cspz00_5023;
														long BgL_rulenoz00_5022;

														BgL_rulenoz00_5022 = BgL_arg1539z00_1564;
														BgL_cspz00_5023 = BgL_cz00_1555;
														BgL_itemsetvz00_5024 = BgL_arg1540z00_1565;
														BgL_itemsetvz00_1549 = BgL_itemsetvz00_5024;
														BgL_cspz00_1548 = BgL_cspz00_5023;
														BgL_rulenoz00_1547 = BgL_rulenoz00_5022;
														goto BgL_zc3z04anonymousza31525ze3z87_1550;
													}
												}
										}
									}
								}
							else
								{
									long BgL_rulenoz00_5025;

									BgL_rulenoz00_5025 = (BgL_rulenoz00_1547 + 1L);
									BgL_rulenoz00_1547 = BgL_rulenoz00_5025;
									goto BgL_zc3z04anonymousza31525ze3z87_1550;
								}
						}
					else
						{
							obj_t BgL_cz00_3320;
							obj_t BgL_itemsetv2z00_3321;

							BgL_cz00_3320 = BgL_cspz00_1548;
							BgL_itemsetv2z00_3321 = BgL_itemsetvz00_1549;
						BgL_loop2z00_3319:
							if (PAIRP(BgL_cz00_3320))
								{	/* Lalr/lalr.scm 347 */
									obj_t BgL_arg1547z00_3327;
									obj_t BgL_arg1549z00_3328;

									BgL_arg1547z00_3327 = CDR(BgL_cz00_3320);
									BgL_arg1549z00_3328 =
										MAKE_YOUNG_PAIR(CAR(BgL_cz00_3320), BgL_itemsetv2z00_3321);
									{
										obj_t BgL_itemsetv2z00_5033;
										obj_t BgL_cz00_5032;

										BgL_cz00_5032 = BgL_arg1547z00_3327;
										BgL_itemsetv2z00_5033 = BgL_arg1549z00_3328;
										BgL_itemsetv2z00_3321 = BgL_itemsetv2z00_5033;
										BgL_cz00_3320 = BgL_cz00_5032;
										goto BgL_loop2z00_3319;
									}
								}
							else
								{	/* Lalr/lalr.scm 346 */
									return bgl_reverse(BgL_itemsetv2z00_3321);
								}
						}
				}
			}
		}

	}



/* allocate-item-sets */
	obj_t BGl_allocatezd2itemzd2setsz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 352 */
			BGl_kernelzd2basezd2zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nsymsz00zz__lalr_globalz00), BINT(0L));
			return (BGl_kernelzd2endzd2zz__lalr_globalz00 =
				make_vector(
					(long) CINT(BGl_nsymsz00zz__lalr_globalz00), BFALSE), BUNSPEC);
		}

	}



/* allocate-storage */
	obj_t BGl_allocatezd2storagezd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 357 */
			BGl_allocatezd2itemzd2setsz00zz__lalr_expandz00();
			return (BGl_redzd2setzd2zz__lalr_globalz00 =
				make_vector(
					((long) CINT(BGl_nrulesz00zz__lalr_globalz00) + 1L),
					BINT(0L)), BUNSPEC);
		}

	}



/* initialize-states */
	obj_t BGl_initializa7ezd2statesz75zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 364 */
			{	/* Lalr/lalr.scm 365 */
				obj_t BgL_pz00_1581;

				BgL_pz00_1581 = make_vector(4L, BINT(0L));
				VECTOR_SET(BgL_pz00_1581, 0L, BINT(0L));
				VECTOR_SET(BgL_pz00_1581, 1L, BFALSE);
				VECTOR_SET(BgL_pz00_1581, 2L, BINT(1L));
				VECTOR_SET(BgL_pz00_1581, 3L, BGl_list2466z00zz__lalr_expandz00);
				{	/* Lalr/lalr.scm 371 */
					obj_t BgL_list1554z00_1582;

					BgL_list1554z00_1582 = MAKE_YOUNG_PAIR(BgL_pz00_1581, BNIL);
					BGl_firstzd2statezd2zz__lalr_globalz00 = BgL_list1554z00_1582;
				}
				BGl_lastzd2statezd2zz__lalr_globalz00 =
					BGl_firstzd2statezd2zz__lalr_globalz00;
				return (BGl_nstatesz00zz__lalr_globalz00 = BINT(1L), BUNSPEC);
			}
		}

	}



/* generate-states */
	bool_t BGl_generatezd2stateszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 377 */
			BGl_allocatezd2storagezd2zz__lalr_expandz00();
			BGl_setzd2fderiveszd2zz__lalr_expandz00();
			BGl_initializa7ezd2statesz75zz__lalr_expandz00();
			{
				obj_t BgL_thiszd2statezd2_1584;

				BgL_thiszd2statezd2_1584 = BGl_firstzd2statezd2zz__lalr_globalz00;
			BgL_zc3z04anonymousza31555ze3z87_1585:
				if (PAIRP(BgL_thiszd2statezd2_1584))
					{	/* Lalr/lalr.scm 383 */
						obj_t BgL_xz00_1587;

						BgL_xz00_1587 = CAR(BgL_thiszd2statezd2_1584);
						{	/* Lalr/lalr.scm 383 */
							obj_t BgL_isz00_1588;

							{	/* Lalr/lalr.scm 384 */
								obj_t BgL_arg1559z00_1591;

								BgL_arg1559z00_1591 = VECTOR_REF(((obj_t) BgL_xz00_1587), 3L);
								BgL_isz00_1588 =
									BGl_closurez00zz__lalr_expandz00(BgL_arg1559z00_1591);
							}
							{	/* Lalr/lalr.scm 384 */

								BGl_savezd2reductionszd2zz__lalr_expandz00(BgL_xz00_1587,
									BgL_isz00_1588);
								BGl_newzd2itemsetszd2zz__lalr_expandz00(BgL_isz00_1588);
								BGl_appendzd2stateszd2zz__lalr_expandz00();
								if (((long) CINT(BGl_nshiftsz00zz__lalr_globalz00) > 0L))
									{	/* Lalr/lalr.scm 388 */
										BGl_savezd2shiftszd2zz__lalr_expandz00(BgL_xz00_1587);
									}
								else
									{	/* Lalr/lalr.scm 388 */
										BFALSE;
									}
								{
									obj_t BgL_thiszd2statezd2_5071;

									BgL_thiszd2statezd2_5071 = CDR(BgL_thiszd2statezd2_1584);
									BgL_thiszd2statezd2_1584 = BgL_thiszd2statezd2_5071;
									goto BgL_zc3z04anonymousza31555ze3z87_1585;
								}
							}
						}
					}
				else
					{	/* Lalr/lalr.scm 382 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* new-itemsets */
	obj_t BGl_newzd2itemsetszd2zz__lalr_expandz00(obj_t BgL_itemsetz00_9)
	{
		{	/* Lalr/lalr.scm 396 */
			BGl_shiftzd2symbolzd2zz__lalr_globalz00 = BNIL;
			{
				long BgL_iz00_3358;

				BgL_iz00_3358 = 0L;
			BgL_loopz00_3357:
				if ((BgL_iz00_3358 < (long) CINT(BGl_nsymsz00zz__lalr_globalz00)))
					{	/* Lalr/lalr.scm 400 */
						VECTOR_SET(BGl_kernelzd2endzd2zz__lalr_globalz00, BgL_iz00_3358,
							BNIL);
						{
							long BgL_iz00_5077;

							BgL_iz00_5077 = (BgL_iz00_3358 + 1L);
							BgL_iz00_3358 = BgL_iz00_5077;
							goto BgL_loopz00_3357;
						}
					}
				else
					{	/* Lalr/lalr.scm 400 */
						((bool_t) 0);
					}
			}
			{
				obj_t BgL_ispz00_1600;

				BgL_ispz00_1600 = BgL_itemsetz00_9;
			BgL_zc3z04anonymousza31563ze3z87_1601:
				if (PAIRP(BgL_ispz00_1600))
					{	/* Lalr/lalr.scm 407 */
						obj_t BgL_iz00_1603;

						BgL_iz00_1603 = CAR(BgL_ispz00_1600);
						{	/* Lalr/lalr.scm 407 */
							obj_t BgL_symz00_1604;

							BgL_symz00_1604 =
								VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
								(long) CINT(BgL_iz00_1603));
							{	/* Lalr/lalr.scm 408 */

								if (((long) CINT(BgL_symz00_1604) >= 0L))
									{	/* Lalr/lalr.scm 409 */
										BGl_shiftzd2symbolzd2zz__lalr_globalz00 =
											BGl_sinsertz00zz__lalr_utilz00(BgL_symz00_1604,
											BGl_shiftzd2symbolzd2zz__lalr_globalz00);
										{	/* Lalr/lalr.scm 412 */
											obj_t BgL_xz00_1606;

											BgL_xz00_1606 =
												VECTOR_REF(BGl_kernelzd2endzd2zz__lalr_globalz00,
												(long) CINT(BgL_symz00_1604));
											if (NULLP(BgL_xz00_1606))
												{	/* Lalr/lalr.scm 413 */
													{	/* Lalr/lalr.scm 415 */
														obj_t BgL_arg1567z00_1608;

														{	/* Lalr/lalr.scm 415 */
															long BgL_arg1571z00_1609;

															BgL_arg1571z00_1609 =
																((long) CINT(BgL_iz00_1603) + 1L);
															BgL_arg1567z00_1608 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1571z00_1609),
																BgL_xz00_1606);
														}
														VECTOR_SET(BGl_kernelzd2basezd2zz__lalr_globalz00,
															(long) CINT(BgL_symz00_1604),
															BgL_arg1567z00_1608);
													}
													VECTOR_SET(BGl_kernelzd2endzd2zz__lalr_globalz00,
														(long) CINT(BgL_symz00_1604),
														VECTOR_REF(BGl_kernelzd2basezd2zz__lalr_globalz00,
															(long) CINT(BgL_symz00_1604)));
												}
											else
												{	/* Lalr/lalr.scm 413 */
													{	/* Lalr/lalr.scm 418 */
														obj_t BgL_arg1575z00_1611;

														{	/* Lalr/lalr.scm 418 */
															long BgL_arg1576z00_1612;

															BgL_arg1576z00_1612 =
																((long) CINT(BgL_iz00_1603) + 1L);
															{	/* Lalr/lalr.scm 418 */
																obj_t BgL_list1577z00_1613;

																BgL_list1577z00_1613 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg1576z00_1612),
																	BNIL);
																BgL_arg1575z00_1611 = BgL_list1577z00_1613;
														}}
														{	/* Lalr/lalr.scm 418 */
															obj_t BgL_tmpz00_5106;

															BgL_tmpz00_5106 = ((obj_t) BgL_xz00_1606);
															SET_CDR(BgL_tmpz00_5106, BgL_arg1575z00_1611);
													}}
													{	/* Lalr/lalr.scm 419 */
														obj_t BgL_arg1578z00_1614;

														BgL_arg1578z00_1614 = CDR(((obj_t) BgL_xz00_1606));
														VECTOR_SET(BGl_kernelzd2endzd2zz__lalr_globalz00,
															(long) CINT(BgL_symz00_1604),
															BgL_arg1578z00_1614);
									}}}}
								else
									{	/* Lalr/lalr.scm 409 */
										BFALSE;
									}
								{
									obj_t BgL_ispz00_5113;

									BgL_ispz00_5113 = CDR(BgL_ispz00_1600);
									BgL_ispz00_1600 = BgL_ispz00_5113;
									goto BgL_zc3z04anonymousza31563ze3z87_1601;
								}
							}
						}
					}
				else
					{	/* Lalr/lalr.scm 406 */
						((bool_t) 0);
					}
			}
			return (BGl_nshiftsz00zz__lalr_globalz00 =
				BINT(bgl_list_length(BGl_shiftzd2symbolzd2zz__lalr_globalz00)),
				BUNSPEC);
		}

	}



/* get-state */
	obj_t BGl_getzd2statezd2zz__lalr_expandz00(obj_t BgL_symz00_10)
	{
		{	/* Lalr/lalr.scm 426 */
			{	/* Lalr/lalr.scm 427 */
				obj_t BgL_ispz00_1617;

				BgL_ispz00_1617 =
					VECTOR_REF(BGl_kernelzd2basezd2zz__lalr_globalz00,
					(long) CINT(BgL_symz00_10));
				{	/* Lalr/lalr.scm 427 */
					long BgL_nz00_1618;

					BgL_nz00_1618 = bgl_list_length(BgL_ispz00_1617);
					{	/* Lalr/lalr.scm 428 */
						long BgL_keyz00_1619;

						{
							obj_t BgL_isp1z00_3407;
							long BgL_kz00_3408;

							BgL_isp1z00_3407 = BgL_ispz00_1617;
							BgL_kz00_3408 = 0L;
						BgL_loopz00_3406:
							if (NULLP(BgL_isp1z00_3407))
								{	/* Lalr/lalr.scm 430 */
									BgL_keyz00_1619 =
										BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(BgL_kz00_3408,
										(long) CINT(BGl_STATEzd2TABLEzd2SIZEz00zz__lalr_globalz00));
								}
							else
								{	/* Lalr/lalr.scm 432 */
									obj_t BgL_arg1616z00_3414;
									long BgL_arg1617z00_3415;

									BgL_arg1616z00_3414 = CDR(((obj_t) BgL_isp1z00_3407));
									BgL_arg1617z00_3415 =
										(BgL_kz00_3408 +
										(long) CINT(CAR(((obj_t) BgL_isp1z00_3407))));
									{
										long BgL_kz00_5131;
										obj_t BgL_isp1z00_5130;

										BgL_isp1z00_5130 = BgL_arg1616z00_3414;
										BgL_kz00_5131 = BgL_arg1617z00_3415;
										BgL_kz00_3408 = BgL_kz00_5131;
										BgL_isp1z00_3407 = BgL_isp1z00_5130;
										goto BgL_loopz00_3406;
									}
								}
						}
						{	/* Lalr/lalr.scm 429 */
							obj_t BgL_spz00_1620;

							BgL_spz00_1620 =
								VECTOR_REF(BGl_statezd2tablezd2zz__lalr_globalz00,
								BgL_keyz00_1619);
							{	/* Lalr/lalr.scm 433 */

								if (NULLP(BgL_spz00_1620))
									{	/* Lalr/lalr.scm 435 */
										obj_t BgL_xz00_1622;

										BgL_xz00_1622 =
											BGl_newzd2statezd2zz__lalr_expandz00(BgL_symz00_10);
										{	/* Lalr/lalr.scm 436 */
											obj_t BgL_arg1582z00_1623;

											{	/* Lalr/lalr.scm 436 */
												obj_t BgL_list1583z00_1624;

												BgL_list1583z00_1624 =
													MAKE_YOUNG_PAIR(BgL_xz00_1622, BNIL);
												BgL_arg1582z00_1623 = BgL_list1583z00_1624;
											}
											VECTOR_SET(BGl_statezd2tablezd2zz__lalr_globalz00,
												BgL_keyz00_1619, BgL_arg1582z00_1623);
										}
										return VECTOR_REF(BgL_xz00_1622, 0L);
									}
								else
									{
										obj_t BgL_sp1z00_1626;

										BgL_sp1z00_1626 = BgL_spz00_1620;
									BgL_zc3z04anonymousza31584ze3z87_1627:
										{	/* Lalr/lalr.scm 439 */
											bool_t BgL_test2593z00_5139;

											{	/* Lalr/lalr.scm 439 */
												bool_t BgL_test2594z00_5140;

												{	/* Lalr/lalr.scm 439 */
													obj_t BgL_b1239z00_1677;

													{	/* Lalr/lalr.scm 439 */
														obj_t BgL_arg1613z00_1679;

														BgL_arg1613z00_1679 =
															CAR(((obj_t) BgL_sp1z00_1626));
														BgL_b1239z00_1677 =
															VECTOR_REF(((obj_t) BgL_arg1613z00_1679), 2L);
													}
													if (INTEGERP(BgL_b1239z00_1677))
														{	/* Lalr/lalr.scm 439 */
															BgL_test2594z00_5140 =
																(BgL_nz00_1618 ==
																(long) CINT(BgL_b1239z00_1677));
														}
													else
														{	/* Lalr/lalr.scm 439 */
															BgL_test2594z00_5140 =
																BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT
																(BgL_nz00_1618), BgL_b1239z00_1677);
														}
												}
												if (BgL_test2594z00_5140)
													{	/* Lalr/lalr.scm 440 */
														obj_t BgL_g1142z00_1663;

														{	/* Lalr/lalr.scm 440 */
															obj_t BgL_arg1612z00_1676;

															BgL_arg1612z00_1676 =
																CAR(((obj_t) BgL_sp1z00_1626));
															BgL_g1142z00_1663 =
																VECTOR_REF(((obj_t) BgL_arg1612z00_1676), 3L);
														}
														{
															obj_t BgL_i1z00_1665;
															obj_t BgL_tz00_1666;

															BgL_i1z00_1665 = BgL_ispz00_1617;
															BgL_tz00_1666 = BgL_g1142z00_1663;
														BgL_zc3z04anonymousza31609ze3z87_1667:
															{	/* Lalr/lalr.scm 441 */
																bool_t BgL_test2596z00_5155;

																if (PAIRP(BgL_i1z00_1665))
																	{	/* Lalr/lalr.scm 442 */
																		obj_t BgL_a1240z00_1672;

																		BgL_a1240z00_1672 = CAR(BgL_i1z00_1665);
																		{	/* Lalr/lalr.scm 443 */
																			obj_t BgL_b1241z00_1673;

																			BgL_b1241z00_1673 =
																				CAR(((obj_t) BgL_tz00_1666));
																			{	/* Lalr/lalr.scm 442 */

																				{	/* Lalr/lalr.scm 442 */
																					bool_t BgL_test2598z00_5161;

																					if (INTEGERP(BgL_a1240z00_1672))
																						{	/* Lalr/lalr.scm 442 */
																							BgL_test2598z00_5161 =
																								INTEGERP(BgL_b1241z00_1673);
																						}
																					else
																						{	/* Lalr/lalr.scm 442 */
																							BgL_test2598z00_5161 =
																								((bool_t) 0);
																						}
																					if (BgL_test2598z00_5161)
																						{	/* Lalr/lalr.scm 442 */
																							BgL_test2596z00_5155 =
																								(
																								(long) CINT(BgL_a1240z00_1672)
																								==
																								(long) CINT(BgL_b1241z00_1673));
																						}
																					else
																						{	/* Lalr/lalr.scm 442 */
																							BgL_test2596z00_5155 =
																								BGl_2zd3zd3zz__r4_numbers_6_5z00
																								(BgL_a1240z00_1672,
																								BgL_b1241z00_1673);
																						}
																				}
																			}
																		}
																	}
																else
																	{	/* Lalr/lalr.scm 441 */
																		BgL_test2596z00_5155 = ((bool_t) 0);
																	}
																if (BgL_test2596z00_5155)
																	{
																		obj_t BgL_tz00_5171;
																		obj_t BgL_i1z00_5169;

																		BgL_i1z00_5169 = CDR(BgL_i1z00_1665);
																		BgL_tz00_5171 =
																			CDR(((obj_t) BgL_tz00_1666));
																		BgL_tz00_1666 = BgL_tz00_5171;
																		BgL_i1z00_1665 = BgL_i1z00_5169;
																		goto BgL_zc3z04anonymousza31609ze3z87_1667;
																	}
																else
																	{	/* Lalr/lalr.scm 441 */
																		BgL_test2593z00_5139 =
																			NULLP(BgL_i1z00_1665);
																	}
															}
														}
													}
												else
													{	/* Lalr/lalr.scm 439 */
														BgL_test2593z00_5139 = ((bool_t) 0);
													}
											}
											if (BgL_test2593z00_5139)
												{	/* Lalr/lalr.scm 446 */
													obj_t BgL_arg1601z00_1654;

													BgL_arg1601z00_1654 = CAR(((obj_t) BgL_sp1z00_1626));
													return VECTOR_REF(((obj_t) BgL_arg1601z00_1654), 0L);
												}
											else
												{	/* Lalr/lalr.scm 439 */
													if (NULLP(CDR(((obj_t) BgL_sp1z00_1626))))
														{	/* Lalr/lalr.scm 448 */
															obj_t BgL_xz00_1657;

															BgL_xz00_1657 =
																BGl_newzd2statezd2zz__lalr_expandz00
																(BgL_symz00_10);
															{	/* Lalr/lalr.scm 449 */
																obj_t BgL_arg1605z00_1658;

																{	/* Lalr/lalr.scm 449 */
																	obj_t BgL_list1606z00_1659;

																	BgL_list1606z00_1659 =
																		MAKE_YOUNG_PAIR(BgL_xz00_1657, BNIL);
																	BgL_arg1605z00_1658 = BgL_list1606z00_1659;
																}
																{	/* Lalr/lalr.scm 449 */
																	obj_t BgL_tmpz00_5185;

																	BgL_tmpz00_5185 = ((obj_t) BgL_sp1z00_1626);
																	SET_CDR(BgL_tmpz00_5185, BgL_arg1605z00_1658);
																}
															}
															return VECTOR_REF(BgL_xz00_1657, 0L);
														}
													else
														{	/* Lalr/lalr.scm 451 */
															obj_t BgL_arg1607z00_1660;

															BgL_arg1607z00_1660 =
																CDR(((obj_t) BgL_sp1z00_1626));
															{
																obj_t BgL_sp1z00_5191;

																BgL_sp1z00_5191 = BgL_arg1607z00_1660;
																BgL_sp1z00_1626 = BgL_sp1z00_5191;
																goto BgL_zc3z04anonymousza31584ze3z87_1627;
															}
														}
												}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* new-state */
	obj_t BGl_newzd2statezd2zz__lalr_expandz00(obj_t BgL_symz00_11)
	{
		{	/* Lalr/lalr.scm 454 */
			{	/* Lalr/lalr.scm 455 */
				obj_t BgL_ispz00_1690;

				BgL_ispz00_1690 =
					VECTOR_REF(BGl_kernelzd2basezd2zz__lalr_globalz00,
					(long) CINT(BgL_symz00_11));
				{	/* Lalr/lalr.scm 455 */
					long BgL_nz00_1691;

					BgL_nz00_1691 = bgl_list_length(BgL_ispz00_1690);
					{	/* Lalr/lalr.scm 456 */
						obj_t BgL_pz00_1692;

						BgL_pz00_1692 = make_vector(4L, BINT(0L));
						{	/* Lalr/lalr.scm 457 */

							VECTOR_SET(BgL_pz00_1692, 0L, BGl_nstatesz00zz__lalr_globalz00);
							VECTOR_SET(BgL_pz00_1692, 1L, BgL_symz00_11);
							{	/* Lalr/lalr.scm 460 */
								bool_t BgL_test2601z00_5199;

								{	/* Lalr/lalr.scm 460 */
									bool_t BgL_test2602z00_5200;

									if (INTEGERP(BgL_symz00_11))
										{	/* Lalr/lalr.scm 460 */
											BgL_test2602z00_5200 =
												INTEGERP(BGl_nvarsz00zz__lalr_globalz00);
										}
									else
										{	/* Lalr/lalr.scm 460 */
											BgL_test2602z00_5200 = ((bool_t) 0);
										}
									if (BgL_test2602z00_5200)
										{	/* Lalr/lalr.scm 460 */
											BgL_test2601z00_5199 =
												(
												(long) CINT(BgL_symz00_11) ==
												(long) CINT(BGl_nvarsz00zz__lalr_globalz00));
										}
									else
										{	/* Lalr/lalr.scm 460 */
											BgL_test2601z00_5199 =
												BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_symz00_11,
												BGl_nvarsz00zz__lalr_globalz00);
										}
								}
								if (BgL_test2601z00_5199)
									{	/* Lalr/lalr.scm 460 */
										BGl_finalzd2statezd2zz__lalr_globalz00 =
											BGl_nstatesz00zz__lalr_globalz00;
									}
								else
									{	/* Lalr/lalr.scm 460 */
										BFALSE;
									}
							}
							VECTOR_SET(BgL_pz00_1692, 2L, BINT(BgL_nz00_1691));
							VECTOR_SET(BgL_pz00_1692, 3L, BgL_ispz00_1690);
							{	/* Lalr/lalr.scm 463 */
								obj_t BgL_arg1621z00_1696;

								{	/* Lalr/lalr.scm 463 */
									obj_t BgL_list1622z00_1697;

									BgL_list1622z00_1697 = MAKE_YOUNG_PAIR(BgL_pz00_1692, BNIL);
									BgL_arg1621z00_1696 = BgL_list1622z00_1697;
								}
								SET_CDR(BGl_lastzd2statezd2zz__lalr_globalz00,
									BgL_arg1621z00_1696);
							}
							BGl_lastzd2statezd2zz__lalr_globalz00 =
								CDR(BGl_lastzd2statezd2zz__lalr_globalz00);
							BGl_nstatesz00zz__lalr_globalz00 =
								ADDFX(BGl_nstatesz00zz__lalr_globalz00, BINT(1L));
							return BgL_pz00_1692;
						}
					}
				}
			}
		}

	}



/* append-states */
	obj_t BGl_appendzd2stateszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 471 */
			return (BGl_shiftzd2setzd2zz__lalr_globalz00 =
				BGl_loopze72ze7zz__lalr_expandz00
				(BGl_shiftzd2symbolzd2zz__lalr_globalz00), BUNSPEC);
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__lalr_expandz00(obj_t BgL_lz00_1699)
	{
		{	/* Lalr/lalr.scm 473 */
			if (NULLP(BgL_lz00_1699))
				{	/* Lalr/lalr.scm 474 */
					return BNIL;
				}
			else
				{	/* Lalr/lalr.scm 476 */
					obj_t BgL_arg1625z00_1702;
					obj_t BgL_arg1626z00_1703;

					{	/* Lalr/lalr.scm 476 */
						obj_t BgL_arg1627z00_1704;

						BgL_arg1627z00_1704 = CAR(((obj_t) BgL_lz00_1699));
						BgL_arg1625z00_1702 =
							BGl_getzd2statezd2zz__lalr_expandz00(BgL_arg1627z00_1704);
					}
					{	/* Lalr/lalr.scm 476 */
						obj_t BgL_arg1628z00_1705;

						BgL_arg1628z00_1705 = CDR(((obj_t) BgL_lz00_1699));
						BgL_arg1626z00_1703 =
							BGl_loopze72ze7zz__lalr_expandz00(BgL_arg1628z00_1705);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1625z00_1702, BgL_arg1626z00_1703);
				}
		}

	}



/* save-shifts */
	obj_t BGl_savezd2shiftszd2zz__lalr_expandz00(obj_t BgL_corez00_12)
	{
		{	/* Lalr/lalr.scm 480 */
			{	/* Lalr/lalr.scm 481 */
				obj_t BgL_pz00_1707;

				BgL_pz00_1707 = make_vector(3L, BINT(0L));
				{	/* Lalr/lalr.scm 482 */
					obj_t BgL_arg1629z00_1708;

					BgL_arg1629z00_1708 = VECTOR_REF(((obj_t) BgL_corez00_12), 0L);
					VECTOR_SET(BgL_pz00_1707, 0L, BgL_arg1629z00_1708);
				}
				VECTOR_SET(BgL_pz00_1707, 1L, BGl_nshiftsz00zz__lalr_globalz00);
				VECTOR_SET(BgL_pz00_1707, 2L, BGl_shiftzd2setzd2zz__lalr_globalz00);
				if (CBOOL(BGl_lastzd2shiftzd2zz__lalr_globalz00))
					{	/* Lalr/lalr.scm 485 */
						{	/* Lalr/lalr.scm 487 */
							obj_t BgL_arg1630z00_1709;

							{	/* Lalr/lalr.scm 487 */
								obj_t BgL_list1631z00_1710;

								BgL_list1631z00_1710 = MAKE_YOUNG_PAIR(BgL_pz00_1707, BNIL);
								BgL_arg1630z00_1709 = BgL_list1631z00_1710;
							}
							SET_CDR(BGl_lastzd2shiftzd2zz__lalr_globalz00,
								BgL_arg1630z00_1709);
						}
						return (BGl_lastzd2shiftzd2zz__lalr_globalz00 =
							CDR(BGl_lastzd2shiftzd2zz__lalr_globalz00), BUNSPEC);
					}
				else
					{	/* Lalr/lalr.scm 485 */
						{	/* Lalr/lalr.scm 490 */
							obj_t BgL_list1632z00_1711;

							BgL_list1632z00_1711 = MAKE_YOUNG_PAIR(BgL_pz00_1707, BNIL);
							BGl_firstzd2shiftzd2zz__lalr_globalz00 = BgL_list1632z00_1711;
						}
						return (BGl_lastzd2shiftzd2zz__lalr_globalz00 =
							BGl_firstzd2shiftzd2zz__lalr_globalz00, BUNSPEC);
					}
			}
		}

	}



/* save-reductions */
	obj_t BGl_savezd2reductionszd2zz__lalr_expandz00(obj_t BgL_corez00_13,
		obj_t BgL_itemsetz00_14)
	{
		{	/* Lalr/lalr.scm 493 */
			{	/* Lalr/lalr.scm 494 */
				obj_t BgL_rsz00_1712;

				BgL_rsz00_1712 = BGl_loopze71ze7zz__lalr_expandz00(BgL_itemsetz00_14);
				if (NULLP(BgL_rsz00_1712))
					{	/* Lalr/lalr.scm 501 */
						return BFALSE;
					}
				else
					{	/* Lalr/lalr.scm 502 */
						obj_t BgL_pz00_1714;

						BgL_pz00_1714 = make_vector(3L, BINT(0L));
						{	/* Lalr/lalr.scm 503 */
							obj_t BgL_arg1634z00_1715;

							BgL_arg1634z00_1715 = VECTOR_REF(((obj_t) BgL_corez00_13), 0L);
							VECTOR_SET(BgL_pz00_1714, 0L, BgL_arg1634z00_1715);
						}
						{	/* Lalr/lalr.scm 504 */
							long BgL_arg1636z00_1716;

							BgL_arg1636z00_1716 = bgl_list_length(BgL_rsz00_1712);
							VECTOR_SET(BgL_pz00_1714, 1L, BINT(BgL_arg1636z00_1716));
						}
						VECTOR_SET(BgL_pz00_1714, 2L, BgL_rsz00_1712);
						if (CBOOL(BGl_lastzd2reductionzd2zz__lalr_globalz00))
							{	/* Lalr/lalr.scm 506 */
								{	/* Lalr/lalr.scm 508 */
									obj_t BgL_arg1637z00_1717;

									{	/* Lalr/lalr.scm 508 */
										obj_t BgL_list1638z00_1718;

										BgL_list1638z00_1718 = MAKE_YOUNG_PAIR(BgL_pz00_1714, BNIL);
										BgL_arg1637z00_1717 = BgL_list1638z00_1718;
									}
									SET_CDR(BGl_lastzd2reductionzd2zz__lalr_globalz00,
										BgL_arg1637z00_1717);
								}
								return (BGl_lastzd2reductionzd2zz__lalr_globalz00 =
									CDR(BGl_lastzd2reductionzd2zz__lalr_globalz00), BUNSPEC);
							}
						else
							{	/* Lalr/lalr.scm 506 */
								{	/* Lalr/lalr.scm 511 */
									obj_t BgL_list1639z00_1719;

									BgL_list1639z00_1719 = MAKE_YOUNG_PAIR(BgL_pz00_1714, BNIL);
									BGl_firstzd2reductionzd2zz__lalr_globalz00 =
										BgL_list1639z00_1719;
								}
								return (BGl_lastzd2reductionzd2zz__lalr_globalz00 =
									BGl_firstzd2reductionzd2zz__lalr_globalz00, BUNSPEC);
							}
					}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__lalr_expandz00(obj_t BgL_lz00_1721)
	{
		{	/* Lalr/lalr.scm 494 */
		BGl_loopze71ze7zz__lalr_expandz00:
			if (NULLP(BgL_lz00_1721))
				{	/* Lalr/lalr.scm 495 */
					return BNIL;
				}
			else
				{	/* Lalr/lalr.scm 497 */
					obj_t BgL_itemz00_1724;

					{	/* Lalr/lalr.scm 497 */
						obj_t BgL_arg1648z00_1730;

						BgL_arg1648z00_1730 = CAR(((obj_t) BgL_lz00_1721));
						BgL_itemz00_1724 =
							VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
							(long) CINT(BgL_arg1648z00_1730));
					}
					if (((long) CINT(BgL_itemz00_1724) < 0L))
						{	/* Lalr/lalr.scm 499 */
							long BgL_arg1643z00_1726;
							obj_t BgL_arg1644z00_1727;

							BgL_arg1643z00_1726 = NEG((long) CINT(BgL_itemz00_1724));
							{	/* Lalr/lalr.scm 499 */
								obj_t BgL_arg1645z00_1728;

								BgL_arg1645z00_1728 = CDR(((obj_t) BgL_lz00_1721));
								BgL_arg1644z00_1727 =
									BGl_loopze71ze7zz__lalr_expandz00(BgL_arg1645z00_1728);
							}
							return
								MAKE_YOUNG_PAIR(BINT(BgL_arg1643z00_1726), BgL_arg1644z00_1727);
						}
					else
						{	/* Lalr/lalr.scm 500 */
							obj_t BgL_arg1646z00_1729;

							BgL_arg1646z00_1729 = CDR(((obj_t) BgL_lz00_1721));
							{
								obj_t BgL_lz00_5275;

								BgL_lz00_5275 = BgL_arg1646z00_1729;
								BgL_lz00_1721 = BgL_lz00_5275;
								goto BGl_loopze71ze7zz__lalr_expandz00;
							}
						}
				}
		}

	}



/* lalr */
	bool_t BGl_lalrz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 517 */
			{	/* Lalr/lalr.scm 518 */
				long BgL_arg1649z00_1732;

				{	/* Lalr/lalr.scm 518 */
					long BgL_tmpz00_5276;

					BgL_tmpz00_5276 = (long) CINT(BGl_ntermsz00zz__lalr_globalz00);
					BgL_arg1649z00_1732 = (BgL_tmpz00_5276 / 28L);
				}
				BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00 =
					BINT((1L + BgL_arg1649z00_1732));
			}
			BGl_setzd2accessingzd2symbolz00zz__lalr_expandz00();
			BGl_setzd2shiftzd2tablez00zz__lalr_expandz00();
			BGl_setzd2reductionzd2tablez00zz__lalr_expandz00();
			BGl_setzd2maxzd2rhsz00zz__lalr_expandz00();
			BGl_initializa7ezd2LAz75zz__lalr_expandz00();
			BGl_setzd2gotozd2mapz00zz__lalr_expandz00();
			BGl_initializa7ezd2Fz75zz__lalr_expandz00();
			BGl_buildzd2relationszd2zz__lalr_expandz00();
			BGl_digraphz00zz__lalr_expandz00(BGl_includesz00zz__lalr_globalz00);
			BGL_TAIL return BGl_computezd2lookaheadszd2zz__lalr_expandz00();
		}

	}



/* set-accessing-symbol */
	bool_t BGl_setzd2accessingzd2symbolz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 530 */
			BGl_acceszd2symbolzd2zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nstatesz00zz__lalr_globalz00), BFALSE);
			{
				obj_t BgL_lz00_1734;

				BgL_lz00_1734 = BGl_firstzd2statezd2zz__lalr_globalz00;
			BgL_zc3z04anonymousza31650ze3z87_1735:
				if (PAIRP(BgL_lz00_1734))
					{	/* Lalr/lalr.scm 534 */
						obj_t BgL_xz00_1737;

						BgL_xz00_1737 = CAR(BgL_lz00_1734);
						{	/* Lalr/lalr.scm 535 */
							obj_t BgL_arg1652z00_1738;
							obj_t BgL_arg1653z00_1739;

							BgL_arg1652z00_1738 = VECTOR_REF(((obj_t) BgL_xz00_1737), 0L);
							BgL_arg1653z00_1739 = VECTOR_REF(((obj_t) BgL_xz00_1737), 1L);
							VECTOR_SET(BGl_acceszd2symbolzd2zz__lalr_globalz00,
								(long) CINT(BgL_arg1652z00_1738), BgL_arg1653z00_1739);
						}
						{
							obj_t BgL_lz00_5302;

							BgL_lz00_5302 = CDR(BgL_lz00_1734);
							BgL_lz00_1734 = BgL_lz00_5302;
							goto BgL_zc3z04anonymousza31650ze3z87_1735;
						}
					}
				else
					{	/* Lalr/lalr.scm 533 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* set-shift-table */
	bool_t BGl_setzd2shiftzd2tablez00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 538 */
			BGl_shiftzd2tablezd2zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nstatesz00zz__lalr_globalz00), BFALSE);
			{
				obj_t BgL_lz00_1743;

				BgL_lz00_1743 = BGl_firstzd2shiftzd2zz__lalr_globalz00;
			BgL_zc3z04anonymousza31655ze3z87_1744:
				if (PAIRP(BgL_lz00_1743))
					{	/* Lalr/lalr.scm 542 */
						obj_t BgL_xz00_1746;

						BgL_xz00_1746 = CAR(BgL_lz00_1743);
						{	/* Lalr/lalr.scm 543 */
							obj_t BgL_arg1657z00_1747;

							BgL_arg1657z00_1747 = VECTOR_REF(((obj_t) BgL_xz00_1746), 0L);
							VECTOR_SET(BGl_shiftzd2tablezd2zz__lalr_globalz00,
								(long) CINT(BgL_arg1657z00_1747), BgL_xz00_1746);
						}
						{
							obj_t BgL_lz00_5313;

							BgL_lz00_5313 = CDR(BgL_lz00_1743);
							BgL_lz00_1743 = BgL_lz00_5313;
							goto BgL_zc3z04anonymousza31655ze3z87_1744;
						}
					}
				else
					{	/* Lalr/lalr.scm 541 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* set-reduction-table */
	bool_t BGl_setzd2reductionzd2tablez00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 546 */
			BGl_reductionzd2tablezd2zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nstatesz00zz__lalr_globalz00), BFALSE);
			{
				obj_t BgL_lz00_1751;

				BgL_lz00_1751 = BGl_firstzd2reductionzd2zz__lalr_globalz00;
			BgL_zc3z04anonymousza31659ze3z87_1752:
				if (PAIRP(BgL_lz00_1751))
					{	/* Lalr/lalr.scm 550 */
						obj_t BgL_xz00_1754;

						BgL_xz00_1754 = CAR(BgL_lz00_1751);
						{	/* Lalr/lalr.scm 551 */
							obj_t BgL_arg1661z00_1755;

							BgL_arg1661z00_1755 = VECTOR_REF(((obj_t) BgL_xz00_1754), 0L);
							VECTOR_SET(BGl_reductionzd2tablezd2zz__lalr_globalz00,
								(long) CINT(BgL_arg1661z00_1755), BgL_xz00_1754);
						}
						{
							obj_t BgL_lz00_5324;

							BgL_lz00_5324 = CDR(BgL_lz00_1751);
							BgL_lz00_1751 = BgL_lz00_5324;
							goto BgL_zc3z04anonymousza31659ze3z87_1752;
						}
					}
				else
					{	/* Lalr/lalr.scm 549 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* set-max-rhs */
	obj_t BGl_setzd2maxzd2rhsz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 554 */
			{
				long BgL_pz00_3525;
				obj_t BgL_curmaxz00_3526;
				long BgL_lengthz00_3527;

				BgL_pz00_3525 = 0L;
				BgL_curmaxz00_3526 = BINT(0L);
				BgL_lengthz00_3527 = 0L;
			BgL_loopz00_3524:
				{	/* Lalr/lalr.scm 556 */
					obj_t BgL_xz00_3528;

					BgL_xz00_3528 =
						VECTOR_REF(BGl_ritemz00zz__lalr_globalz00, BgL_pz00_3525);
					if (CBOOL(BgL_xz00_3528))
						{	/* Lalr/lalr.scm 557 */
							if (((long) CINT(BgL_xz00_3528) >= 0L))
								{
									long BgL_lengthz00_5334;
									long BgL_pz00_5332;

									BgL_pz00_5332 = (BgL_pz00_3525 + 1L);
									BgL_lengthz00_5334 = (BgL_lengthz00_3527 + 1L);
									BgL_lengthz00_3527 = BgL_lengthz00_5334;
									BgL_pz00_3525 = BgL_pz00_5332;
									goto BgL_loopz00_3524;
								}
							else
								{	/* Lalr/lalr.scm 560 */
									long BgL_arg1669z00_3537;
									obj_t BgL_arg1670z00_3538;

									BgL_arg1669z00_3537 = (BgL_pz00_3525 + 1L);
									BgL_arg1670z00_3538 =
										BGl_2maxz00zz__r4_numbers_6_5z00(BgL_curmaxz00_3526,
										BINT(BgL_lengthz00_3527));
									{	/* Lalr/lalr.scm 556 */
										obj_t BgL_xz00_3542;

										BgL_xz00_3542 =
											VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
											BgL_arg1669z00_3537);
										if (CBOOL(BgL_xz00_3542))
											{	/* Lalr/lalr.scm 557 */
												if (((long) CINT(BgL_xz00_3542) >= 0L))
													{
														long BgL_lengthz00_5348;
														obj_t BgL_curmaxz00_5347;
														long BgL_pz00_5345;

														BgL_pz00_5345 = (BgL_arg1669z00_3537 + 1L);
														BgL_curmaxz00_5347 = BgL_arg1670z00_3538;
														BgL_lengthz00_5348 = (0L + 1L);
														BgL_lengthz00_3527 = BgL_lengthz00_5348;
														BgL_curmaxz00_3526 = BgL_curmaxz00_5347;
														BgL_pz00_3525 = BgL_pz00_5345;
														goto BgL_loopz00_3524;
													}
												else
													{
														long BgL_lengthz00_5355;
														obj_t BgL_curmaxz00_5352;
														long BgL_pz00_5350;

														BgL_pz00_5350 = (BgL_arg1669z00_3537 + 1L);
														BgL_curmaxz00_5352 =
															BGl_2maxz00zz__r4_numbers_6_5z00
															(BgL_arg1670z00_3538, BINT(0L));
														BgL_lengthz00_5355 = 0L;
														BgL_lengthz00_3527 = BgL_lengthz00_5355;
														BgL_curmaxz00_3526 = BgL_curmaxz00_5352;
														BgL_pz00_3525 = BgL_pz00_5350;
														goto BgL_loopz00_3524;
													}
											}
										else
											{	/* Lalr/lalr.scm 557 */
												return (BGl_maxrhsz00zz__lalr_globalz00 =
													BgL_arg1670z00_3538, BUNSPEC);
											}
									}
								}
						}
					else
						{	/* Lalr/lalr.scm 557 */
							return (BGl_maxrhsz00zz__lalr_globalz00 =
								BgL_curmaxz00_3526, BUNSPEC);
						}
				}
			}
		}

	}



/* initialize-LA */
	bool_t BGl_initializa7ezd2LAz75zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 563 */
			BGl_consistentz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_nstatesz00zz__lalr_globalz00), BFALSE);
			BGl_lookaheadsz00zz__lalr_globalz00 =
				make_vector(
				((long) CINT(BGl_nstatesz00zz__lalr_globalz00) + 1L), BFALSE);
			{
				long BgL_countz00_1775;
				long BgL_iz00_1776;

				BgL_countz00_1775 = 0L;
				BgL_iz00_1776 = 0L;
			BgL_zc3z04anonymousza31676ze3z87_1777:
				if ((BgL_iz00_1776 < (long) CINT(BGl_nstatesz00zz__lalr_globalz00)))
					{	/* Lalr/lalr.scm 573 */
						VECTOR_SET(BGl_lookaheadsz00zz__lalr_globalz00, BgL_iz00_1776,
							BINT(BgL_countz00_1775));
						{	/* Lalr/lalr.scm 576 */
							obj_t BgL_rpz00_1779;
							obj_t BgL_spz00_1780;

							BgL_rpz00_1779 =
								VECTOR_REF(BGl_reductionzd2tablezd2zz__lalr_globalz00,
								BgL_iz00_1776);
							BgL_spz00_1780 =
								VECTOR_REF(BGl_shiftzd2tablezd2zz__lalr_globalz00,
								BgL_iz00_1776);
							{	/* Lalr/lalr.scm 578 */
								bool_t BgL_test2618z00_5369;

								if (CBOOL(BgL_rpz00_1779))
									{	/* Lalr/lalr.scm 579 */
										bool_t BgL__ortest_1143z00_1796;

										{	/* Lalr/lalr.scm 579 */
											obj_t BgL_arg1706z00_1802;

											BgL_arg1706z00_1802 =
												VECTOR_REF(((obj_t) BgL_rpz00_1779), 1L);
											BgL__ortest_1143z00_1796 =
												((long) CINT(BgL_arg1706z00_1802) > 1L);
										}
										if (BgL__ortest_1143z00_1796)
											{	/* Lalr/lalr.scm 579 */
												BgL_test2618z00_5369 = BgL__ortest_1143z00_1796;
											}
										else
											{	/* Lalr/lalr.scm 579 */
												if (CBOOL(BgL_spz00_1780))
													{	/* Lalr/lalr.scm 581 */
														bool_t BgL_test2622z00_5379;

														{	/* Lalr/lalr.scm 583 */
															obj_t BgL_arg1703z00_1799;

															{	/* Lalr/lalr.scm 583 */
																obj_t BgL_arg1704z00_1800;

																{	/* Lalr/lalr.scm 583 */
																	obj_t BgL_arg1705z00_1801;

																	BgL_arg1705z00_1801 =
																		VECTOR_REF(((obj_t) BgL_spz00_1780), 2L);
																	{
																		obj_t BgL_lz00_3581;

																		BgL_lz00_3581 = BgL_arg1705z00_1801;
																	BgL_lastz00_3580:
																		if (NULLP(CDR(((obj_t) BgL_lz00_3581))))
																			{	/* Lalr/lalr.scm 565 */
																				BgL_arg1704z00_1800 =
																					CAR(((obj_t) BgL_lz00_3581));
																			}
																		else
																			{
																				obj_t BgL_lz00_5388;

																				BgL_lz00_5388 =
																					CDR(((obj_t) BgL_lz00_3581));
																				BgL_lz00_3581 = BgL_lz00_5388;
																				goto BgL_lastz00_3580;
																			}
																	}
																}
																BgL_arg1703z00_1799 =
																	VECTOR_REF
																	(BGl_acceszd2symbolzd2zz__lalr_globalz00,
																	(long) CINT(BgL_arg1704z00_1800));
															}
															BgL_test2622z00_5379 =
																(
																(long) CINT(BgL_arg1703z00_1799) <
																(long) CINT(BGl_nvarsz00zz__lalr_globalz00));
														}
														if (BgL_test2622z00_5379)
															{	/* Lalr/lalr.scm 581 */
																BgL_test2618z00_5369 = ((bool_t) 0);
															}
														else
															{	/* Lalr/lalr.scm 581 */
																BgL_test2618z00_5369 = ((bool_t) 1);
															}
													}
												else
													{	/* Lalr/lalr.scm 580 */
														BgL_test2618z00_5369 = ((bool_t) 0);
													}
											}
									}
								else
									{	/* Lalr/lalr.scm 578 */
										BgL_test2618z00_5369 = ((bool_t) 0);
									}
								if (BgL_test2618z00_5369)
									{	/* Lalr/lalr.scm 585 */
										long BgL_arg1699z00_1792;
										long BgL_arg1700z00_1793;

										{	/* Lalr/lalr.scm 585 */
											obj_t BgL_arg1701z00_1794;

											BgL_arg1701z00_1794 =
												VECTOR_REF(((obj_t) BgL_rpz00_1779), 1L);
											BgL_arg1699z00_1792 =
												(BgL_countz00_1775 + (long) CINT(BgL_arg1701z00_1794));
										}
										BgL_arg1700z00_1793 = (BgL_iz00_1776 + 1L);
										{
											long BgL_iz00_5402;
											long BgL_countz00_5401;

											BgL_countz00_5401 = BgL_arg1699z00_1792;
											BgL_iz00_5402 = BgL_arg1700z00_1793;
											BgL_iz00_1776 = BgL_iz00_5402;
											BgL_countz00_1775 = BgL_countz00_5401;
											goto BgL_zc3z04anonymousza31676ze3z87_1777;
										}
									}
								else
									{	/* Lalr/lalr.scm 578 */
										VECTOR_SET(BGl_consistentz00zz__lalr_globalz00,
											BgL_iz00_1776, BTRUE);
										{
											long BgL_iz00_5404;

											BgL_iz00_5404 = (BgL_iz00_1776 + 1L);
											BgL_iz00_1776 = BgL_iz00_5404;
											goto BgL_zc3z04anonymousza31676ze3z87_1777;
										}
									}
							}
						}
					}
				else
					{	/* Lalr/lalr.scm 573 */
						VECTOR_SET(BGl_lookaheadsz00zz__lalr_globalz00,
							(long) CINT(BGl_nstatesz00zz__lalr_globalz00),
							BINT(BgL_countz00_1775));
						{	/* Lalr/lalr.scm 592 */
							obj_t BgL_cz00_1803;

							BgL_cz00_1803 =
								BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_countz00_1775),
								BINT(1L));
							BGl_LAz00zz__lalr_globalz00 =
								make_vector((long) CINT(BgL_cz00_1803), BFALSE);
							{
								long BgL_jz00_1805;

								BgL_jz00_1805 = 0L;
							BgL_zc3z04anonymousza31707ze3z87_1806:
								{	/* Lalr/lalr.scm 594 */
									bool_t BgL_test2624z00_5414;

									if (INTEGERP(BgL_cz00_1803))
										{	/* Lalr/lalr.scm 594 */
											BgL_test2624z00_5414 =
												(BgL_jz00_1805 == (long) CINT(BgL_cz00_1803));
										}
									else
										{	/* Lalr/lalr.scm 594 */
											BgL_test2624z00_5414 =
												BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_jz00_1805),
												BgL_cz00_1803);
										}
									if (BgL_test2624z00_5414)
										{	/* Lalr/lalr.scm 594 */
											((bool_t) 0);
										}
									else
										{	/* Lalr/lalr.scm 594 */
											VECTOR_SET(BGl_LAz00zz__lalr_globalz00, BgL_jz00_1805,
												make_vector(
													(long)
													CINT(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00),
													BINT(0L)));
											{
												long BgL_jz00_5425;

												BgL_jz00_5425 = (BgL_jz00_1805 + 1L);
												BgL_jz00_1805 = BgL_jz00_5425;
												goto BgL_zc3z04anonymousza31707ze3z87_1806;
											}
										}
								}
							}
							BGl_LArulenoz00zz__lalr_globalz00 =
								make_vector((long) CINT(BgL_cz00_1803), BINT(-1L));
							BGl_lookbackz00zz__lalr_globalz00 =
								make_vector((long) CINT(BgL_cz00_1803), BFALSE);
						}
						{
							long BgL_iz00_1816;
							long BgL_npz00_1817;

							BgL_iz00_1816 = 0L;
							BgL_npz00_1817 = 0L;
						BgL_zc3z04anonymousza31712ze3z87_1818:
							if (
								(BgL_iz00_1816 < (long) CINT(BGl_nstatesz00zz__lalr_globalz00)))
								{	/* Lalr/lalr.scm 599 */
									bool_t BgL_test2627z00_5435;

									{	/* Lalr/lalr.scm 599 */
										obj_t BgL_vectorz00_3611;

										BgL_vectorz00_3611 = BGl_consistentz00zz__lalr_globalz00;
										BgL_test2627z00_5435 =
											CBOOL(VECTOR_REF(BgL_vectorz00_3611, BgL_iz00_1816));
									}
									if (BgL_test2627z00_5435)
										{
											long BgL_iz00_5438;

											BgL_iz00_5438 = (BgL_iz00_1816 + 1L);
											BgL_iz00_1816 = BgL_iz00_5438;
											goto BgL_zc3z04anonymousza31712ze3z87_1818;
										}
									else
										{	/* Lalr/lalr.scm 601 */
											obj_t BgL_rpz00_1822;

											BgL_rpz00_1822 =
												VECTOR_REF(BGl_reductionzd2tablezd2zz__lalr_globalz00,
												BgL_iz00_1816);
											if (CBOOL(BgL_rpz00_1822))
												{	/* Lalr/lalr.scm 603 */
													obj_t BgL_g1146z00_1823;

													BgL_g1146z00_1823 =
														VECTOR_REF(((obj_t) BgL_rpz00_1822), 2L);
													{
														obj_t BgL_jz00_1825;
														long BgL_np2z00_1826;

														BgL_jz00_1825 = BgL_g1146z00_1823;
														BgL_np2z00_1826 = BgL_npz00_1817;
													BgL_zc3z04anonymousza31716ze3z87_1827:
														if (NULLP(BgL_jz00_1825))
															{
																long BgL_npz00_5449;
																long BgL_iz00_5447;

																BgL_iz00_5447 = (BgL_iz00_1816 + 1L);
																BgL_npz00_5449 = BgL_np2z00_1826;
																BgL_npz00_1817 = BgL_npz00_5449;
																BgL_iz00_1816 = BgL_iz00_5447;
																goto BgL_zc3z04anonymousza31712ze3z87_1818;
															}
														else
															{	/* Lalr/lalr.scm 604 */
																{	/* Lalr/lalr.scm 607 */
																	obj_t BgL_arg1720z00_1830;

																	BgL_arg1720z00_1830 =
																		CAR(((obj_t) BgL_jz00_1825));
																	VECTOR_SET(BGl_LArulenoz00zz__lalr_globalz00,
																		BgL_np2z00_1826, BgL_arg1720z00_1830);
																}
																{	/* Lalr/lalr.scm 608 */
																	obj_t BgL_arg1722z00_1831;
																	long BgL_arg1723z00_1832;

																	BgL_arg1722z00_1831 =
																		CDR(((obj_t) BgL_jz00_1825));
																	BgL_arg1723z00_1832 = (BgL_np2z00_1826 + 1L);
																	{
																		long BgL_np2z00_5457;
																		obj_t BgL_jz00_5456;

																		BgL_jz00_5456 = BgL_arg1722z00_1831;
																		BgL_np2z00_5457 = BgL_arg1723z00_1832;
																		BgL_np2z00_1826 = BgL_np2z00_5457;
																		BgL_jz00_1825 = BgL_jz00_5456;
																		goto BgL_zc3z04anonymousza31716ze3z87_1827;
																	}
																}
															}
													}
												}
											else
												{
													long BgL_iz00_5458;

													BgL_iz00_5458 = (BgL_iz00_1816 + 1L);
													BgL_iz00_1816 = BgL_iz00_5458;
													goto BgL_zc3z04anonymousza31712ze3z87_1818;
												}
										}
								}
							else
								{	/* Lalr/lalr.scm 598 */
									return ((bool_t) 0);
								}
						}
					}
			}
		}

	}



/* set-goto-map */
	bool_t BGl_setzd2gotozd2mapz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 612 */
			BGl_gotozd2mapzd2zz__lalr_globalz00 =
				make_vector(
				((long) CINT(BGl_nvarsz00zz__lalr_globalz00) + 1L), BINT(0L));
			{	/* Lalr/lalr.scm 614 */
				obj_t BgL_tempzd2mapzd2_1845;

				BgL_tempzd2mapzd2_1845 =
					make_vector(
					((long) CINT(BGl_nvarsz00zz__lalr_globalz00) + 1L), BINT(0L));
				{
					long BgL_ngz00_1847;
					obj_t BgL_spz00_1848;

					BgL_ngz00_1847 = 0L;
					BgL_spz00_1848 = BGl_firstzd2shiftzd2zz__lalr_globalz00;
				BgL_zc3z04anonymousza31731ze3z87_1849:
					if (PAIRP(BgL_spz00_1848))
						{	/* Lalr/lalr.scm 617 */
							obj_t BgL_g1147z00_1851;

							{	/* Lalr/lalr.scm 617 */
								obj_t BgL_arg1744z00_1867;

								{	/* Lalr/lalr.scm 617 */
									obj_t BgL_arg1745z00_1868;

									BgL_arg1745z00_1868 = CAR(BgL_spz00_1848);
									BgL_arg1744z00_1867 =
										VECTOR_REF(((obj_t) BgL_arg1745z00_1868), 2L);
								}
								BgL_g1147z00_1851 = bgl_reverse(BgL_arg1744z00_1867);
							}
							{
								obj_t BgL_iz00_1853;
								long BgL_ng2z00_1854;

								BgL_iz00_1853 = BgL_g1147z00_1851;
								BgL_ng2z00_1854 = BgL_ngz00_1847;
							BgL_zc3z04anonymousza31733ze3z87_1855:
								if (PAIRP(BgL_iz00_1853))
									{	/* Lalr/lalr.scm 619 */
										obj_t BgL_symbolz00_1857;

										{	/* Lalr/lalr.scm 619 */
											obj_t BgL_arg1741z00_1864;

											BgL_arg1741z00_1864 = CAR(BgL_iz00_1853);
											BgL_symbolz00_1857 =
												VECTOR_REF(BGl_acceszd2symbolzd2zz__lalr_globalz00,
												(long) CINT(BgL_arg1741z00_1864));
										}
										if (
											((long) CINT(BgL_symbolz00_1857) <
												(long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
											{	/* Lalr/lalr.scm 620 */
												{	/* Lalr/lalr.scm 623 */
													long BgL_arg1736z00_1859;

													{	/* Lalr/lalr.scm 623 */
														obj_t BgL_arg1737z00_1860;

														BgL_arg1737z00_1860 =
															VECTOR_REF(BGl_gotozd2mapzd2zz__lalr_globalz00,
															(long) CINT(BgL_symbolz00_1857));
														BgL_arg1736z00_1859 =
															(1L + (long) CINT(BgL_arg1737z00_1860));
													}
													VECTOR_SET(BGl_gotozd2mapzd2zz__lalr_globalz00,
														(long) CINT(BgL_symbolz00_1857),
														BINT(BgL_arg1736z00_1859));
												}
												{
													long BgL_ng2z00_5492;
													obj_t BgL_iz00_5490;

													BgL_iz00_5490 = CDR(BgL_iz00_1853);
													BgL_ng2z00_5492 = (BgL_ng2z00_1854 + 1L);
													BgL_ng2z00_1854 = BgL_ng2z00_5492;
													BgL_iz00_1853 = BgL_iz00_5490;
													goto BgL_zc3z04anonymousza31733ze3z87_1855;
												}
											}
										else
											{
												obj_t BgL_iz00_5494;

												BgL_iz00_5494 = CDR(BgL_iz00_1853);
												BgL_iz00_1853 = BgL_iz00_5494;
												goto BgL_zc3z04anonymousza31733ze3z87_1855;
											}
									}
								else
									{	/* Lalr/lalr.scm 626 */
										obj_t BgL_arg1743z00_1865;

										BgL_arg1743z00_1865 = CDR(((obj_t) BgL_spz00_1848));
										{
											obj_t BgL_spz00_5499;
											long BgL_ngz00_5498;

											BgL_ngz00_5498 = BgL_ng2z00_1854;
											BgL_spz00_5499 = BgL_arg1743z00_1865;
											BgL_spz00_1848 = BgL_spz00_5499;
											BgL_ngz00_1847 = BgL_ngz00_5498;
											goto BgL_zc3z04anonymousza31731ze3z87_1849;
										}
									}
							}
						}
					else
						{
							long BgL_kz00_1870;
							long BgL_iz00_1871;

							BgL_kz00_1870 = 0L;
							BgL_iz00_1871 = 0L;
						BgL_zc3z04anonymousza31746ze3z87_1872:
							if ((BgL_iz00_1871 < (long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
								{	/* Lalr/lalr.scm 629 */
									VECTOR_SET(BgL_tempzd2mapzd2_1845, BgL_iz00_1871,
										BINT(BgL_kz00_1870));
									{	/* Lalr/lalr.scm 632 */
										long BgL_arg1748z00_1874;
										long BgL_arg1749z00_1875;

										{	/* Lalr/lalr.scm 632 */
											obj_t BgL_arg1750z00_1876;

											BgL_arg1750z00_1876 =
												VECTOR_REF(BGl_gotozd2mapzd2zz__lalr_globalz00,
												BgL_iz00_1871);
											BgL_arg1748z00_1874 =
												(BgL_kz00_1870 + (long) CINT(BgL_arg1750z00_1876));
										}
										BgL_arg1749z00_1875 = (BgL_iz00_1871 + 1L);
										{
											long BgL_iz00_5510;
											long BgL_kz00_5509;

											BgL_kz00_5509 = BgL_arg1748z00_1874;
											BgL_iz00_5510 = BgL_arg1749z00_1875;
											BgL_iz00_1871 = BgL_iz00_5510;
											BgL_kz00_1870 = BgL_kz00_5509;
											goto BgL_zc3z04anonymousza31746ze3z87_1872;
										}
									}
								}
							else
								{	/* Lalr/lalr.scm 629 */
									{
										long BgL_iz00_3659;

										BgL_iz00_3659 = 0L;
									BgL_dozd2loopzd2zd21148zd2_3658:
										if (
											(BgL_iz00_3659 >=
												(long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
											{	/* Lalr/lalr.scm 635 */
												((bool_t) 0);
											}
										else
											{	/* Lalr/lalr.scm 635 */
												VECTOR_SET(BGl_gotozd2mapzd2zz__lalr_globalz00,
													BgL_iz00_3659, VECTOR_REF(BgL_tempzd2mapzd2_1845,
														BgL_iz00_3659));
												{
													long BgL_iz00_5516;

													BgL_iz00_5516 = (BgL_iz00_3659 + 1L);
													BgL_iz00_3659 = BgL_iz00_5516;
													goto BgL_dozd2loopzd2zd21148zd2_3658;
												}
											}
									}
									BGl_ngotosz00zz__lalr_globalz00 = BINT(BgL_ngz00_1847);
									VECTOR_SET(BGl_gotozd2mapzd2zz__lalr_globalz00,
										(long) CINT(BGl_nvarsz00zz__lalr_globalz00),
										BGl_ngotosz00zz__lalr_globalz00);
									VECTOR_SET(BgL_tempzd2mapzd2_1845,
										(long) CINT(BGl_nvarsz00zz__lalr_globalz00),
										BGl_ngotosz00zz__lalr_globalz00);
									BGl_fromzd2statezd2zz__lalr_globalz00 =
										make_vector((long) CINT(BGl_ngotosz00zz__lalr_globalz00),
										BFALSE);
									BGl_tozd2statezd2zz__lalr_globalz00 =
										make_vector((long) CINT(BGl_ngotosz00zz__lalr_globalz00),
										BFALSE);
									{
										obj_t BgL_spz00_1885;

										BgL_spz00_1885 = BGl_firstzd2shiftzd2zz__lalr_globalz00;
									BgL_zc3z04anonymousza31755ze3z87_1886:
										if (NULLP(BgL_spz00_1885))
											{	/* Lalr/lalr.scm 645 */
												return ((bool_t) 0);
											}
										else
											{	/* Lalr/lalr.scm 645 */
												{	/* Lalr/lalr.scm 647 */
													obj_t BgL_xz00_1888;

													BgL_xz00_1888 = CAR(((obj_t) BgL_spz00_1885));
													{	/* Lalr/lalr.scm 647 */
														obj_t BgL_state1z00_1889;

														BgL_state1z00_1889 =
															VECTOR_REF(((obj_t) BgL_xz00_1888), 0L);
														{	/* Lalr/lalr.scm 648 */

															{
																obj_t BgL_iz00_1892;

																{	/* Lalr/lalr.scm 649 */
																	obj_t BgL_arg1757z00_1891;

																	BgL_arg1757z00_1891 =
																		VECTOR_REF(((obj_t) BgL_xz00_1888), 2L);
																	BgL_iz00_1892 = BgL_arg1757z00_1891;
																BgL_zc3z04anonymousza31758ze3z87_1893:
																	if (NULLP(BgL_iz00_1892))
																		{	/* Lalr/lalr.scm 649 */
																			((bool_t) 0);
																		}
																	else
																		{	/* Lalr/lalr.scm 649 */
																			{	/* Lalr/lalr.scm 651 */
																				obj_t BgL_state2z00_1895;

																				BgL_state2z00_1895 =
																					CAR(((obj_t) BgL_iz00_1892));
																				{	/* Lalr/lalr.scm 651 */
																					obj_t BgL_symbolz00_1896;

																					BgL_symbolz00_1896 =
																						VECTOR_REF
																						(BGl_acceszd2symbolzd2zz__lalr_globalz00,
																						(long) CINT(BgL_state2z00_1895));
																					{	/* Lalr/lalr.scm 652 */

																						if (
																							((long) CINT(BgL_symbolz00_1896) <
																								(long)
																								CINT
																								(BGl_nvarsz00zz__lalr_globalz00)))
																							{	/* Lalr/lalr.scm 654 */
																								obj_t BgL_kz00_1898;

																								BgL_kz00_1898 =
																									VECTOR_REF
																									(BgL_tempzd2mapzd2_1845,
																									(long)
																									CINT(BgL_symbolz00_1896));
																								{	/* Lalr/lalr.scm 655 */
																									long BgL_arg1761z00_1899;

																									BgL_arg1761z00_1899 =
																										(
																										(long) CINT(BgL_kz00_1898) +
																										1L);
																									VECTOR_SET
																										(BgL_tempzd2mapzd2_1845,
																										(long)
																										CINT(BgL_symbolz00_1896),
																										BINT(BgL_arg1761z00_1899));
																								}
																								VECTOR_SET
																									(BGl_fromzd2statezd2zz__lalr_globalz00,
																									(long) CINT(BgL_kz00_1898),
																									BgL_state1z00_1889);
																								VECTOR_SET
																									(BGl_tozd2statezd2zz__lalr_globalz00,
																									(long) CINT(BgL_kz00_1898),
																									BgL_state2z00_1895);
																							}
																						else
																							{	/* Lalr/lalr.scm 653 */
																								BFALSE;
																							}
																					}
																				}
																			}
																			{	/* Lalr/lalr.scm 649 */
																				obj_t BgL_arg1762z00_1900;

																				BgL_arg1762z00_1900 =
																					CDR(((obj_t) BgL_iz00_1892));
																				{
																					obj_t BgL_iz00_5558;

																					BgL_iz00_5558 = BgL_arg1762z00_1900;
																					BgL_iz00_1892 = BgL_iz00_5558;
																					goto
																						BgL_zc3z04anonymousza31758ze3z87_1893;
																				}
																			}
																		}
																}
															}
														}
													}
												}
												{	/* Lalr/lalr.scm 645 */
													obj_t BgL_arg1763z00_1902;

													BgL_arg1763z00_1902 = CDR(((obj_t) BgL_spz00_1885));
													{
														obj_t BgL_spz00_5561;

														BgL_spz00_5561 = BgL_arg1763z00_1902;
														BgL_spz00_1885 = BgL_spz00_5561;
														goto BgL_zc3z04anonymousza31755ze3z87_1886;
													}
												}
											}
									}
								}
						}
				}
			}
		}

	}



/* map-goto */
	long BGl_mapzd2gotozd2zz__lalr_expandz00(obj_t BgL_statez00_15,
		obj_t BgL_symbolz00_16)
	{
		{	/* Lalr/lalr.scm 660 */
			{	/* Lalr/lalr.scm 661 */
				obj_t BgL_g1151z00_1907;
				long BgL_g1152z00_1908;

				BgL_g1151z00_1907 =
					VECTOR_REF(BGl_gotozd2mapzd2zz__lalr_globalz00,
					(long) CINT(BgL_symbolz00_16));
				{	/* Lalr/lalr.scm 662 */
					obj_t BgL_arg1782z00_1930;

					BgL_arg1782z00_1930 =
						VECTOR_REF(BGl_gotozd2mapzd2zz__lalr_globalz00,
						((long) CINT(BgL_symbolz00_16) + 1L));
					BgL_g1152z00_1908 = ((long) CINT(BgL_arg1782z00_1930) - 1L);
				}
				{
					obj_t BgL_lowz00_1910;
					long BgL_highz00_1911;

					BgL_lowz00_1910 = BgL_g1151z00_1907;
					BgL_highz00_1911 = BgL_g1152z00_1908;
				BgL_zc3z04anonymousza31765ze3z87_1912:
					if (((long) CINT(BgL_lowz00_1910) > BgL_highz00_1911))
						{	/* Lalr/lalr.scm 663 */
							{	/* Lalr/lalr.scm 665 */
								obj_t BgL_arg1767z00_1914;
								obj_t BgL_arg1768z00_1915;

								{	/* Lalr/lalr.scm 665 */
									obj_t BgL_list1769z00_1916;

									{	/* Lalr/lalr.scm 665 */
										obj_t BgL_arg1770z00_1917;

										{	/* Lalr/lalr.scm 665 */
											obj_t BgL_arg1771z00_1918;

											BgL_arg1771z00_1918 =
												MAKE_YOUNG_PAIR(BgL_symbolz00_16, BNIL);
											BgL_arg1770z00_1917 =
												MAKE_YOUNG_PAIR(BgL_statez00_15, BgL_arg1771z00_1918);
										}
										BgL_list1769z00_1916 =
											MAKE_YOUNG_PAIR(BGl_string2467z00zz__lalr_expandz00,
											BgL_arg1770z00_1917);
									}
									BgL_arg1767z00_1914 = BgL_list1769z00_1916;
								}
								{	/* Lalr/lalr.scm 665 */
									obj_t BgL_tmpz00_5575;

									BgL_tmpz00_5575 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1768z00_1915 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5575);
								}
								bgl_display_obj(BgL_arg1767z00_1914, BgL_arg1768z00_1915);
							}
							{	/* Lalr/lalr.scm 665 */
								obj_t BgL_arg1772z00_1919;

								{	/* Lalr/lalr.scm 665 */
									obj_t BgL_tmpz00_5579;

									BgL_tmpz00_5579 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1772z00_1919 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5579);
								}
								bgl_display_char(((unsigned char) 10), BgL_arg1772z00_1919);
							}
							return 0L;
						}
					else
						{	/* Lalr/lalr.scm 667 */
							long BgL_middlez00_1920;

							{	/* Lalr/lalr.scm 667 */
								long BgL_tmpz00_5583;

								BgL_tmpz00_5583 =
									((long) CINT(BgL_lowz00_1910) + BgL_highz00_1911);
								BgL_middlez00_1920 = (BgL_tmpz00_5583 / 2L);
							}
							{	/* Lalr/lalr.scm 667 */
								obj_t BgL_sz00_1921;

								BgL_sz00_1921 =
									VECTOR_REF(BGl_fromzd2statezd2zz__lalr_globalz00,
									BgL_middlez00_1920);
								{	/* Lalr/lalr.scm 668 */

									{	/* Lalr/lalr.scm 670 */
										bool_t BgL_test2639z00_5588;

										{	/* Lalr/lalr.scm 670 */
											bool_t BgL_test2640z00_5589;

											if (INTEGERP(BgL_sz00_1921))
												{	/* Lalr/lalr.scm 670 */
													BgL_test2640z00_5589 = INTEGERP(BgL_statez00_15);
												}
											else
												{	/* Lalr/lalr.scm 670 */
													BgL_test2640z00_5589 = ((bool_t) 0);
												}
											if (BgL_test2640z00_5589)
												{	/* Lalr/lalr.scm 670 */
													BgL_test2639z00_5588 =
														(
														(long) CINT(BgL_sz00_1921) ==
														(long) CINT(BgL_statez00_15));
												}
											else
												{	/* Lalr/lalr.scm 670 */
													BgL_test2639z00_5588 =
														BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_sz00_1921,
														BgL_statez00_15);
												}
										}
										if (BgL_test2639z00_5588)
											{	/* Lalr/lalr.scm 670 */
												return BgL_middlez00_1920;
											}
										else
											{	/* Lalr/lalr.scm 670 */
												if (
													((long) CINT(BgL_sz00_1921) <
														(long) CINT(BgL_statez00_15)))
													{	/* Lalr/lalr.scm 673 */
														long BgL_arg1777z00_1925;

														BgL_arg1777z00_1925 = (BgL_middlez00_1920 + 1L);
														{
															obj_t BgL_lowz00_5602;

															BgL_lowz00_5602 = BINT(BgL_arg1777z00_1925);
															BgL_lowz00_1910 = BgL_lowz00_5602;
															goto BgL_zc3z04anonymousza31765ze3z87_1912;
														}
													}
												else
													{
														long BgL_highz00_5604;

														BgL_highz00_5604 = (BgL_middlez00_1920 - 1L);
														BgL_highz00_1911 = BgL_highz00_5604;
														goto BgL_zc3z04anonymousza31765ze3z87_1912;
													}
											}
									}
								}
							}
						}
				}
			}
		}

	}



/* initialize-F */
	bool_t BGl_initializa7ezd2Fz75zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 678 */
			BGl_Fz00zz__lalr_globalz00 =
				make_vector((long) CINT(BGl_ngotosz00zz__lalr_globalz00), BFALSE);
			{
				long BgL_iz00_1933;

				BgL_iz00_1933 = 0L;
			BgL_zc3z04anonymousza31784ze3z87_1934:
				{	/* Lalr/lalr.scm 680 */
					bool_t BgL_test2643z00_5608;

					if (INTEGERP(BGl_ngotosz00zz__lalr_globalz00))
						{	/* Lalr/lalr.scm 680 */
							BgL_test2643z00_5608 =
								(BgL_iz00_1933 == (long) CINT(BGl_ngotosz00zz__lalr_globalz00));
						}
					else
						{	/* Lalr/lalr.scm 680 */
							BgL_test2643z00_5608 =
								BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_1933),
								BGl_ngotosz00zz__lalr_globalz00);
						}
					if (BgL_test2643z00_5608)
						{	/* Lalr/lalr.scm 680 */
							((bool_t) 0);
						}
					else
						{	/* Lalr/lalr.scm 680 */
							VECTOR_SET(BGl_Fz00zz__lalr_globalz00, BgL_iz00_1933,
								make_vector(
									(long) CINT(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00),
									BINT(0L)));
							{
								long BgL_iz00_5619;

								BgL_iz00_5619 = (BgL_iz00_1933 + 1L);
								BgL_iz00_1933 = BgL_iz00_5619;
								goto BgL_zc3z04anonymousza31784ze3z87_1934;
							}
						}
				}
			}
			{	/* Lalr/lalr.scm 682 */
				obj_t BgL_readsz00_1941;

				BgL_readsz00_1941 =
					make_vector((long) CINT(BGl_ngotosz00zz__lalr_globalz00), BFALSE);
				{
					long BgL_iz00_1943;
					long BgL_rowpz00_1944;

					BgL_iz00_1943 = 0L;
					BgL_rowpz00_1944 = 0L;
				BgL_zc3z04anonymousza31789ze3z87_1945:
					if ((BgL_iz00_1943 < (long) CINT(BGl_ngotosz00zz__lalr_globalz00)))
						{	/* Lalr/lalr.scm 686 */
							obj_t BgL_rowfz00_1947;

							BgL_rowfz00_1947 =
								VECTOR_REF(BGl_Fz00zz__lalr_globalz00, BgL_rowpz00_1944);
							{	/* Lalr/lalr.scm 686 */
								obj_t BgL_statenoz00_1948;

								BgL_statenoz00_1948 =
									VECTOR_REF(BGl_tozd2statezd2zz__lalr_globalz00,
									BgL_iz00_1943);
								{	/* Lalr/lalr.scm 687 */
									obj_t BgL_spz00_1949;

									BgL_spz00_1949 =
										VECTOR_REF(BGl_shiftzd2tablezd2zz__lalr_globalz00,
										(long) CINT(BgL_statenoz00_1948));
									{	/* Lalr/lalr.scm 688 */

										if (CBOOL(BgL_spz00_1949))
											{	/* Lalr/lalr.scm 690 */
												obj_t BgL_g1154z00_1950;

												BgL_g1154z00_1950 =
													VECTOR_REF(((obj_t) BgL_spz00_1949), 2L);
												{
													obj_t BgL_jz00_1953;
													obj_t BgL_edgesz00_1954;

													BgL_jz00_1953 = BgL_g1154z00_1950;
													BgL_edgesz00_1954 = BNIL;
												BgL_zc3z04anonymousza31791ze3z87_1955:
													if (PAIRP(BgL_jz00_1953))
														{	/* Lalr/lalr.scm 692 */
															obj_t BgL_symbolz00_1957;

															{	/* Lalr/lalr.scm 692 */
																obj_t BgL_arg1805z00_1972;

																BgL_arg1805z00_1972 = CAR(BgL_jz00_1953);
																BgL_symbolz00_1957 =
																	VECTOR_REF
																	(BGl_acceszd2symbolzd2zz__lalr_globalz00,
																	(long) CINT(BgL_arg1805z00_1972));
															}
															if (
																((long) CINT(BgL_symbolz00_1957) <
																	(long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
																{	/* Lalr/lalr.scm 694 */
																	bool_t BgL_test2649z00_5643;

																	{	/* Lalr/lalr.scm 694 */
																		obj_t BgL_vectorz00_3737;
																		long BgL_kz00_3738;

																		BgL_vectorz00_3737 =
																			BGl_nullablez00zz__lalr_globalz00;
																		BgL_kz00_3738 =
																			(long) CINT(BgL_symbolz00_1957);
																		BgL_test2649z00_5643 =
																			CBOOL(VECTOR_REF(BgL_vectorz00_3737,
																				BgL_kz00_3738));
																	}
																	if (BgL_test2649z00_5643)
																		{	/* Lalr/lalr.scm 695 */
																			obj_t BgL_arg1795z00_1960;
																			obj_t BgL_arg1796z00_1961;

																			BgL_arg1795z00_1960 = CDR(BgL_jz00_1953);
																			{	/* Lalr/lalr.scm 695 */
																				long BgL_arg1797z00_1962;

																				BgL_arg1797z00_1962 =
																					BGl_mapzd2gotozd2zz__lalr_expandz00
																					(BgL_statenoz00_1948,
																					BgL_symbolz00_1957);
																				BgL_arg1796z00_1961 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1797z00_1962),
																					BgL_edgesz00_1954);
																			}
																			{
																				obj_t BgL_edgesz00_5652;
																				obj_t BgL_jz00_5651;

																				BgL_jz00_5651 = BgL_arg1795z00_1960;
																				BgL_edgesz00_5652 = BgL_arg1796z00_1961;
																				BgL_edgesz00_1954 = BgL_edgesz00_5652;
																				BgL_jz00_1953 = BgL_jz00_5651;
																				goto
																					BgL_zc3z04anonymousza31791ze3z87_1955;
																			}
																		}
																	else
																		{
																			obj_t BgL_jz00_5653;

																			BgL_jz00_5653 = CDR(BgL_jz00_1953);
																			BgL_jz00_1953 = BgL_jz00_5653;
																			goto
																				BgL_zc3z04anonymousza31791ze3z87_1955;
																		}
																}
															else
																{	/* Lalr/lalr.scm 693 */
																	{	/* Lalr/lalr.scm 699 */
																		obj_t BgL_xz00_1964;
																		obj_t BgL_yz00_1965;

																		{	/* Lalr/lalr.scm 699 */
																			long BgL_arg1801z00_1968;

																			BgL_arg1801z00_1968 =
																				(
																				(long) CINT(BgL_symbolz00_1957) -
																				(long)
																				CINT(BGl_nvarsz00zz__lalr_globalz00));
																			BgL_xz00_1964 =
																				BGl_quotientz00zz__r4_numbers_6_5_fixnumz00
																				(BINT(BgL_arg1801z00_1968), BINT(28L));
																		}
																		{	/* Lalr/lalr.scm 699 */
																			obj_t BgL_arg1802z00_1969;

																			{	/* Lalr/lalr.scm 699 */
																				long BgL_arg1803z00_1970;

																				BgL_arg1803z00_1970 =
																					(
																					(long) CINT(BgL_symbolz00_1957) -
																					(long)
																					CINT(BGl_nvarsz00zz__lalr_globalz00));
																				BgL_arg1802z00_1969 =
																					BGl_remainderz00zz__r4_numbers_6_5_fixnumz00
																					(BINT(BgL_arg1803z00_1970),
																					BINT(28L));
																			}
																			BgL_yz00_1965 =
																				BGl_exptz00zz__r4_numbers_6_5z00(BINT
																				(2L), BgL_arg1802z00_1969);
																		}
																		{	/* Lalr/lalr.scm 699 */
																			long BgL_arg1799z00_1966;

																			{	/* Lalr/lalr.scm 699 */
																				obj_t BgL_arg1800z00_1967;

																				{	/* Lalr/lalr.scm 699 */
																					long BgL_kz00_3746;

																					BgL_kz00_3746 =
																						(long) CINT(BgL_xz00_1964);
																					BgL_arg1800z00_1967 =
																						VECTOR_REF(
																						((obj_t) BgL_rowfz00_1947),
																						BgL_kz00_3746);
																				}
																				BgL_arg1799z00_1966 =
																					(
																					(long) CINT(BgL_arg1800z00_1967) |
																					(long) CINT(BgL_yz00_1965));
																			}
																			{	/* Lalr/lalr.scm 699 */
																				long BgL_kz00_3750;

																				BgL_kz00_3750 =
																					(long) CINT(BgL_xz00_1964);
																				VECTOR_SET(
																					((obj_t) BgL_rowfz00_1947),
																					BgL_kz00_3750,
																					BINT(BgL_arg1799z00_1966));
																	}}}
																	{
																		obj_t BgL_jz00_5679;

																		BgL_jz00_5679 = CDR(BgL_jz00_1953);
																		BgL_jz00_1953 = BgL_jz00_5679;
																		goto BgL_zc3z04anonymousza31791ze3z87_1955;
																	}
																}
														}
													else
														{	/* Lalr/lalr.scm 691 */
															if (NULLP(BgL_edgesz00_1954))
																{	/* Lalr/lalr.scm 701 */
																	BFALSE;
																}
															else
																{	/* Lalr/lalr.scm 701 */
																	VECTOR_SET(BgL_readsz00_1941, BgL_iz00_1943,
																		bgl_reverse(BgL_edgesz00_1954));
																}
														}
												}
											}
										else
											{	/* Lalr/lalr.scm 689 */
												BFALSE;
											}
										{
											long BgL_rowpz00_5687;
											long BgL_iz00_5685;

											BgL_iz00_5685 = (BgL_iz00_1943 + 1L);
											BgL_rowpz00_5687 = (BgL_rowpz00_1944 + 1L);
											BgL_rowpz00_1944 = BgL_rowpz00_5687;
											BgL_iz00_1943 = BgL_iz00_5685;
											goto BgL_zc3z04anonymousza31789ze3z87_1945;
										}
									}
								}
							}
						}
					else
						{	/* Lalr/lalr.scm 685 */
							((bool_t) 0);
						}
				}
				return BGl_digraphz00zz__lalr_expandz00(BgL_readsz00_1941);
			}
		}

	}



/* add-lookback-edge */
	obj_t BGl_addzd2lookbackzd2edgez00zz__lalr_expandz00(obj_t BgL_statenoz00_17,
		obj_t BgL_rulenoz00_18, long BgL_gotonoz00_19)
	{
		{	/* Lalr/lalr.scm 706 */
			{	/* Lalr/lalr.scm 707 */
				obj_t BgL_kz00_1979;

				BgL_kz00_1979 =
					VECTOR_REF(BGl_lookaheadsz00zz__lalr_globalz00,
					((long) CINT(BgL_statenoz00_17) + 1L));
				{	/* Lalr/lalr.scm 708 */
					obj_t BgL_g1156z00_1980;

					BgL_g1156z00_1980 =
						VECTOR_REF(BGl_lookaheadsz00zz__lalr_globalz00,
						(long) CINT(BgL_statenoz00_17));
					{
						bool_t BgL_foundz00_1982;
						obj_t BgL_iz00_1983;

						BgL_foundz00_1982 = ((bool_t) 0);
						BgL_iz00_1983 = BgL_g1156z00_1980;
					BgL_zc3z04anonymousza31810ze3z87_1984:
						{	/* Lalr/lalr.scm 709 */
							bool_t BgL_test2651z00_5695;

							if (BgL_foundz00_1982)
								{	/* Lalr/lalr.scm 709 */
									BgL_test2651z00_5695 = ((bool_t) 0);
								}
							else
								{	/* Lalr/lalr.scm 709 */
									BgL_test2651z00_5695 =
										((long) CINT(BgL_iz00_1983) < (long) CINT(BgL_kz00_1979));
								}
							if (BgL_test2651z00_5695)
								{	/* Lalr/lalr.scm 710 */
									bool_t BgL_test2653z00_5700;

									{	/* Lalr/lalr.scm 710 */
										obj_t BgL_a1242z00_1990;

										BgL_a1242z00_1990 =
											VECTOR_REF(BGl_LArulenoz00zz__lalr_globalz00,
											(long) CINT(BgL_iz00_1983));
										{	/* Lalr/lalr.scm 710 */
											bool_t BgL_test2654z00_5703;

											if (INTEGERP(BgL_a1242z00_1990))
												{	/* Lalr/lalr.scm 710 */
													BgL_test2654z00_5703 = INTEGERP(BgL_rulenoz00_18);
												}
											else
												{	/* Lalr/lalr.scm 710 */
													BgL_test2654z00_5703 = ((bool_t) 0);
												}
											if (BgL_test2654z00_5703)
												{	/* Lalr/lalr.scm 710 */
													BgL_test2653z00_5700 =
														(
														(long) CINT(BgL_a1242z00_1990) ==
														(long) CINT(BgL_rulenoz00_18));
												}
											else
												{	/* Lalr/lalr.scm 710 */
													BgL_test2653z00_5700 =
														BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_a1242z00_1990,
														BgL_rulenoz00_18);
												}
										}
									}
									if (BgL_test2653z00_5700)
										{
											bool_t BgL_foundz00_5711;

											BgL_foundz00_5711 = ((bool_t) 1);
											BgL_foundz00_1982 = BgL_foundz00_5711;
											goto BgL_zc3z04anonymousza31810ze3z87_1984;
										}
									else
										{	/* Lalr/lalr.scm 712 */
											long BgL_arg1814z00_1989;

											BgL_arg1814z00_1989 = ((long) CINT(BgL_iz00_1983) + 1L);
											{
												obj_t BgL_iz00_5714;

												BgL_iz00_5714 = BINT(BgL_arg1814z00_1989);
												BgL_iz00_1983 = BgL_iz00_5714;
												goto BgL_zc3z04anonymousza31810ze3z87_1984;
											}
										}
								}
							else
								{	/* Lalr/lalr.scm 709 */
									if (BgL_foundz00_1982)
										{	/* Lalr/lalr.scm 718 */
											obj_t BgL_arg1815z00_1992;

											BgL_arg1815z00_1992 =
												MAKE_YOUNG_PAIR(BINT(BgL_gotonoz00_19),
												VECTOR_REF(BGl_lookbackz00zz__lalr_globalz00,
													(long) CINT(BgL_iz00_1983)));
											return
												VECTOR_SET(BGl_lookbackz00zz__lalr_globalz00,
												(long) CINT(BgL_iz00_1983), BgL_arg1815z00_1992);
										}
									else
										{	/* Lalr/lalr.scm 714 */
											{	/* Lalr/lalr.scm 715 */
												obj_t BgL_arg1817z00_1994;

												{	/* Lalr/lalr.scm 715 */
													obj_t BgL_tmpz00_5723;

													BgL_tmpz00_5723 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1817z00_1994 =
														BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5723);
												}
												bgl_display_string(BGl_string2468z00zz__lalr_expandz00,
													BgL_arg1817z00_1994);
											}
											{	/* Lalr/lalr.scm 716 */
												obj_t BgL_arg1818z00_1995;
												obj_t BgL_arg1819z00_1996;

												{	/* Lalr/lalr.scm 716 */
													obj_t BgL_list1820z00_1997;

													{	/* Lalr/lalr.scm 716 */
														obj_t BgL_arg1822z00_1998;

														{	/* Lalr/lalr.scm 716 */
															obj_t BgL_arg1823z00_1999;

															BgL_arg1823z00_1999 =
																MAKE_YOUNG_PAIR(BINT(BgL_gotonoz00_19), BNIL);
															BgL_arg1822z00_1998 =
																MAKE_YOUNG_PAIR(BgL_rulenoz00_18,
																BgL_arg1823z00_1999);
														}
														BgL_list1820z00_1997 =
															MAKE_YOUNG_PAIR(BgL_statenoz00_17,
															BgL_arg1822z00_1998);
													}
													BgL_arg1818z00_1995 = BgL_list1820z00_1997;
												}
												{	/* Lalr/lalr.scm 716 */
													obj_t BgL_tmpz00_5731;

													BgL_tmpz00_5731 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1819z00_1996 =
														BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5731);
												}
												bgl_display_obj(BgL_arg1818z00_1995,
													BgL_arg1819z00_1996);
											}
											{	/* Lalr/lalr.scm 716 */
												obj_t BgL_arg1826z00_2000;

												{	/* Lalr/lalr.scm 716 */
													obj_t BgL_tmpz00_5735;

													BgL_tmpz00_5735 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1826z00_2000 =
														BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5735);
												}
												return
													bgl_display_char(((unsigned char) 10),
													BgL_arg1826z00_2000);
		}}}}}}}}

	}



/* transpose */
	obj_t BGl_transposez00zz__lalr_expandz00(obj_t BgL_rzd2argzd2_20,
		obj_t BgL_nz00_21)
	{
		{	/* Lalr/lalr.scm 721 */
			{	/* Lalr/lalr.scm 722 */
				obj_t BgL_newzd2endzd2_2003;
				obj_t BgL_newzd2rzd2_2004;

				BgL_newzd2endzd2_2003 = make_vector((long) CINT(BgL_nz00_21), BFALSE);
				BgL_newzd2rzd2_2004 = make_vector((long) CINT(BgL_nz00_21), BFALSE);
				{
					long BgL_iz00_2006;

					BgL_iz00_2006 = 0L;
				BgL_zc3z04anonymousza31828ze3z87_2007:
					{	/* Lalr/lalr.scm 724 */
						bool_t BgL_test2657z00_5743;

						if (INTEGERP(BgL_nz00_21))
							{	/* Lalr/lalr.scm 725 */
								BgL_test2657z00_5743 =
									(BgL_iz00_2006 == (long) CINT(BgL_nz00_21));
							}
						else
							{	/* Lalr/lalr.scm 725 */
								BgL_test2657z00_5743 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_2006),
									BgL_nz00_21);
							}
						if (BgL_test2657z00_5743)
							{	/* Lalr/lalr.scm 724 */
								((bool_t) 0);
							}
						else
							{	/* Lalr/lalr.scm 724 */
								{	/* Lalr/lalr.scm 726 */
									obj_t BgL_xz00_2010;

									{	/* Lalr/lalr.scm 726 */
										obj_t BgL_list1831z00_2011;

										BgL_list1831z00_2011 =
											MAKE_YOUNG_PAIR(BGl_symbol2469z00zz__lalr_expandz00,
											BNIL);
										BgL_xz00_2010 = BgL_list1831z00_2011;
									}
									VECTOR_SET(BgL_newzd2rzd2_2004, BgL_iz00_2006, BgL_xz00_2010);
									VECTOR_SET(BgL_newzd2endzd2_2003, BgL_iz00_2006,
										BgL_xz00_2010);
								}
								{
									long BgL_iz00_5753;

									BgL_iz00_5753 = (BgL_iz00_2006 + 1L);
									BgL_iz00_2006 = BgL_iz00_5753;
									goto BgL_zc3z04anonymousza31828ze3z87_2007;
								}
							}
					}
				}
				{
					long BgL_iz00_2016;

					BgL_iz00_2016 = 0L;
				BgL_zc3z04anonymousza31833ze3z87_2017:
					{	/* Lalr/lalr.scm 729 */
						bool_t BgL_test2659z00_5755;

						if (INTEGERP(BgL_nz00_21))
							{	/* Lalr/lalr.scm 730 */
								BgL_test2659z00_5755 =
									(BgL_iz00_2016 == (long) CINT(BgL_nz00_21));
							}
						else
							{	/* Lalr/lalr.scm 730 */
								BgL_test2659z00_5755 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_2016),
									BgL_nz00_21);
							}
						if (BgL_test2659z00_5755)
							{	/* Lalr/lalr.scm 729 */
								((bool_t) 0);
							}
						else
							{	/* Lalr/lalr.scm 729 */
								{	/* Lalr/lalr.scm 731 */
									obj_t BgL_spz00_2020;

									BgL_spz00_2020 =
										VECTOR_REF(((obj_t) BgL_rzd2argzd2_20), BgL_iz00_2016);
									if (PAIRP(BgL_spz00_2020))
										{
											obj_t BgL_sp2z00_2023;

											BgL_sp2z00_2023 = BgL_spz00_2020;
										BgL_zc3z04anonymousza31837ze3z87_2024:
											if (PAIRP(BgL_sp2z00_2023))
												{	/* Lalr/lalr.scm 735 */
													obj_t BgL_xz00_2026;

													BgL_xz00_2026 = CAR(BgL_sp2z00_2023);
													{	/* Lalr/lalr.scm 735 */
														obj_t BgL_yz00_2027;

														BgL_yz00_2027 =
															VECTOR_REF(BgL_newzd2endzd2_2003,
															(long) CINT(BgL_xz00_2026));
														{	/* Lalr/lalr.scm 736 */

															{	/* Lalr/lalr.scm 737 */
																obj_t BgL_arg1839z00_2028;

																{	/* Lalr/lalr.scm 737 */
																	obj_t BgL_arg1840z00_2029;

																	BgL_arg1840z00_2029 =
																		CDR(((obj_t) BgL_yz00_2027));
																	BgL_arg1839z00_2028 =
																		MAKE_YOUNG_PAIR(BINT(BgL_iz00_2016),
																		BgL_arg1840z00_2029);
																}
																{	/* Lalr/lalr.scm 737 */
																	obj_t BgL_tmpz00_5775;

																	BgL_tmpz00_5775 = ((obj_t) BgL_yz00_2027);
																	SET_CDR(BgL_tmpz00_5775, BgL_arg1839z00_2028);
															}}
															{	/* Lalr/lalr.scm 738 */
																obj_t BgL_arg1842z00_2030;

																BgL_arg1842z00_2030 =
																	CDR(((obj_t) BgL_yz00_2027));
																VECTOR_SET(BgL_newzd2endzd2_2003,
																	(long) CINT(BgL_xz00_2026),
																	BgL_arg1842z00_2030);
															}
															{
																obj_t BgL_sp2z00_5782;

																BgL_sp2z00_5782 = CDR(BgL_sp2z00_2023);
																BgL_sp2z00_2023 = BgL_sp2z00_5782;
																goto BgL_zc3z04anonymousza31837ze3z87_2024;
															}
														}
													}
												}
											else
												{	/* Lalr/lalr.scm 734 */
													((bool_t) 0);
												}
										}
									else
										{	/* Lalr/lalr.scm 732 */
											((bool_t) 0);
										}
								}
								{
									long BgL_iz00_5784;

									BgL_iz00_5784 = (BgL_iz00_2016 + 1L);
									BgL_iz00_2016 = BgL_iz00_5784;
									goto BgL_zc3z04anonymousza31833ze3z87_2017;
								}
							}
					}
				}
				{
					long BgL_iz00_2037;

					BgL_iz00_2037 = 0L;
				BgL_zc3z04anonymousza31845ze3z87_2038:
					{	/* Lalr/lalr.scm 740 */
						bool_t BgL_test2663z00_5786;

						if (INTEGERP(BgL_nz00_21))
							{	/* Lalr/lalr.scm 741 */
								BgL_test2663z00_5786 =
									(BgL_iz00_2037 == (long) CINT(BgL_nz00_21));
							}
						else
							{	/* Lalr/lalr.scm 741 */
								BgL_test2663z00_5786 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_2037),
									BgL_nz00_21);
							}
						if (BgL_test2663z00_5786)
							{	/* Lalr/lalr.scm 740 */
								((bool_t) 0);
							}
						else
							{	/* Lalr/lalr.scm 740 */
								{	/* Lalr/lalr.scm 742 */
									obj_t BgL_arg1848z00_2041;

									{	/* Lalr/lalr.scm 742 */
										obj_t BgL_arg1849z00_2042;

										BgL_arg1849z00_2042 =
											VECTOR_REF(BgL_newzd2rzd2_2004, BgL_iz00_2037);
										BgL_arg1848z00_2041 = CDR(((obj_t) BgL_arg1849z00_2042));
									}
									VECTOR_SET(BgL_newzd2rzd2_2004, BgL_iz00_2037,
										BgL_arg1848z00_2041);
								}
								{
									long BgL_iz00_5797;

									BgL_iz00_5797 = (BgL_iz00_2037 + 1L);
									BgL_iz00_2037 = BgL_iz00_5797;
									goto BgL_zc3z04anonymousza31845ze3z87_2038;
								}
							}
					}
				}
				return BgL_newzd2rzd2_2004;
			}
		}

	}



/* build-relations */
	obj_t BGl_buildzd2relationszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 748 */
			{
				obj_t BgL_statenoz00_2105;
				obj_t BgL_symbolz00_2106;

				BGl_includesz00zz__lalr_globalz00 =
					make_vector((long) CINT(BGl_ngotosz00zz__lalr_globalz00), BFALSE);
				{
					long BgL_iz00_2048;

					BgL_iz00_2048 = 0L;
				BgL_zc3z04anonymousza31851ze3z87_2049:
					{	/* Lalr/lalr.scm 761 */
						bool_t BgL_test2665z00_5801;

						if (INTEGERP(BGl_ngotosz00zz__lalr_globalz00))
							{	/* Lalr/lalr.scm 762 */
								BgL_test2665z00_5801 =
									(BgL_iz00_2048 ==
									(long) CINT(BGl_ngotosz00zz__lalr_globalz00));
							}
						else
							{	/* Lalr/lalr.scm 762 */
								BgL_test2665z00_5801 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_2048),
									BGl_ngotosz00zz__lalr_globalz00);
							}
						if (BgL_test2665z00_5801)
							{	/* Lalr/lalr.scm 761 */
								((bool_t) 0);
							}
						else
							{	/* Lalr/lalr.scm 761 */
								{	/* Lalr/lalr.scm 763 */
									obj_t BgL_state1z00_2052;
									obj_t BgL_symbol1z00_2053;

									BgL_state1z00_2052 =
										VECTOR_REF(BGl_fromzd2statezd2zz__lalr_globalz00,
										BgL_iz00_2048);
									{	/* Lalr/lalr.scm 764 */
										obj_t BgL_arg1875z00_2101;

										BgL_arg1875z00_2101 =
											VECTOR_REF(BGl_tozd2statezd2zz__lalr_globalz00,
											BgL_iz00_2048);
										BgL_symbol1z00_2053 =
											VECTOR_REF(BGl_acceszd2symbolzd2zz__lalr_globalz00,
											(long) CINT(BgL_arg1875z00_2101));
									}
									{	/* Lalr/lalr.scm 765 */
										obj_t BgL_g1162z00_2054;

										BgL_g1162z00_2054 =
											VECTOR_REF(BGl_derivesz00zz__lalr_globalz00,
											(long) CINT(BgL_symbol1z00_2053));
										{
											obj_t BgL_rulepz00_2057;
											obj_t BgL_edgesz00_2058;

											BgL_rulepz00_2057 = BgL_g1162z00_2054;
											BgL_edgesz00_2058 = BNIL;
										BgL_zc3z04anonymousza31854ze3z87_2059:
											if (PAIRP(BgL_rulepz00_2057))
												{	/* Lalr/lalr.scm 768 */
													obj_t BgL_za2rulepza2_2061;

													BgL_za2rulepza2_2061 = CAR(BgL_rulepz00_2057);
													{	/* Lalr/lalr.scm 769 */
														obj_t BgL_g1164z00_2062;
														obj_t BgL_g1165z00_2063;

														BgL_g1164z00_2062 =
															VECTOR_REF(BGl_rrhsz00zz__lalr_globalz00,
															(long) CINT(BgL_za2rulepza2_2061));
														{	/* Lalr/lalr.scm 771 */
															obj_t BgL_list1874z00_2099;

															BgL_list1874z00_2099 =
																MAKE_YOUNG_PAIR(BgL_state1z00_2052, BNIL);
															BgL_g1165z00_2063 = BgL_list1874z00_2099;
														}
														{
															obj_t BgL_rpz00_2065;
															obj_t BgL_statenoz00_2066;
															obj_t BgL_statesz00_2067;

															BgL_rpz00_2065 = BgL_g1164z00_2062;
															BgL_statenoz00_2066 = BgL_state1z00_2052;
															BgL_statesz00_2067 = BgL_g1165z00_2063;
														BgL_zc3z04anonymousza31856ze3z87_2068:
															{	/* Lalr/lalr.scm 772 */
																obj_t BgL_za2rpza2_2069;

																BgL_za2rpza2_2069 =
																	VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
																	(long) CINT(BgL_rpz00_2065));
																if (((long) CINT(BgL_za2rpza2_2069) > 0L))
																	{	/* Lalr/lalr.scm 774 */
																		obj_t BgL_stz00_2071;

																		BgL_statenoz00_2105 = BgL_statenoz00_2066;
																		BgL_symbolz00_2106 = BgL_za2rpza2_2069;
																		{	/* Lalr/lalr.scm 751 */
																			obj_t BgL_g1160z00_2108;

																			{	/* Lalr/lalr.scm 751 */
																				obj_t BgL_arg1883z00_2122;

																				BgL_arg1883z00_2122 =
																					VECTOR_REF
																					(BGl_shiftzd2tablezd2zz__lalr_globalz00,
																					(long) CINT(BgL_statenoz00_2105));
																				BgL_g1160z00_2108 =
																					VECTOR_REF(((obj_t)
																						BgL_arg1883z00_2122), 2L);
																			}
																			{
																				obj_t BgL_jz00_2110;
																				obj_t BgL_stnoz00_2111;

																				BgL_jz00_2110 = BgL_g1160z00_2108;
																				BgL_stnoz00_2111 = BgL_statenoz00_2105;
																			BgL_zc3z04anonymousza31878ze3z87_2112:
																				if (NULLP(BgL_jz00_2110))
																					{	/* Lalr/lalr.scm 753 */
																						BgL_stz00_2071 = BgL_stnoz00_2111;
																					}
																				else
																					{	/* Lalr/lalr.scm 755 */
																						obj_t BgL_st2z00_2114;

																						BgL_st2z00_2114 =
																							CAR(((obj_t) BgL_jz00_2110));
																						{	/* Lalr/lalr.scm 756 */
																							bool_t BgL_test2670z00_5833;

																							{	/* Lalr/lalr.scm 756 */
																								obj_t BgL_a1243z00_2119;

																								BgL_a1243z00_2119 =
																									VECTOR_REF
																									(BGl_acceszd2symbolzd2zz__lalr_globalz00,
																									(long) CINT(BgL_st2z00_2114));
																								{	/* Lalr/lalr.scm 756 */
																									bool_t BgL_test2671z00_5836;

																									if (INTEGERP
																										(BgL_a1243z00_2119))
																										{	/* Lalr/lalr.scm 756 */
																											BgL_test2671z00_5836 =
																												INTEGERP
																												(BgL_symbolz00_2106);
																										}
																									else
																										{	/* Lalr/lalr.scm 756 */
																											BgL_test2671z00_5836 =
																												((bool_t) 0);
																										}
																									if (BgL_test2671z00_5836)
																										{	/* Lalr/lalr.scm 756 */
																											BgL_test2670z00_5833 =
																												(
																												(long)
																												CINT(BgL_a1243z00_2119)
																												==
																												(long)
																												CINT
																												(BgL_symbolz00_2106));
																										}
																									else
																										{	/* Lalr/lalr.scm 756 */
																											BgL_test2670z00_5833 =
																												BGl_2zd3zd3zz__r4_numbers_6_5z00
																												(BgL_a1243z00_2119,
																												BgL_symbolz00_2106);
																										}
																								}
																							}
																							if (BgL_test2670z00_5833)
																								{	/* Lalr/lalr.scm 756 */
																									BgL_stz00_2071 =
																										BgL_st2z00_2114;
																								}
																							else
																								{
																									obj_t BgL_stnoz00_5847;
																									obj_t BgL_jz00_5844;

																									BgL_jz00_5844 =
																										CDR(
																										((obj_t) BgL_jz00_2110));
																									BgL_stnoz00_5847 =
																										BgL_st2z00_2114;
																									BgL_stnoz00_2111 =
																										BgL_stnoz00_5847;
																									BgL_jz00_2110 = BgL_jz00_5844;
																									goto
																										BgL_zc3z04anonymousza31878ze3z87_2112;
																								}
																						}
																					}
																			}
																		}
																		{	/* Lalr/lalr.scm 775 */
																			long BgL_arg1858z00_2072;
																			obj_t BgL_arg1859z00_2073;

																			BgL_arg1858z00_2072 =
																				((long) CINT(BgL_rpz00_2065) + 1L);
																			BgL_arg1859z00_2073 =
																				MAKE_YOUNG_PAIR(BgL_stz00_2071,
																				BgL_statesz00_2067);
																			{
																				obj_t BgL_statesz00_5854;
																				obj_t BgL_statenoz00_5853;
																				obj_t BgL_rpz00_5851;

																				BgL_rpz00_5851 =
																					BINT(BgL_arg1858z00_2072);
																				BgL_statenoz00_5853 = BgL_stz00_2071;
																				BgL_statesz00_5854 =
																					BgL_arg1859z00_2073;
																				BgL_statesz00_2067 = BgL_statesz00_5854;
																				BgL_statenoz00_2066 =
																					BgL_statenoz00_5853;
																				BgL_rpz00_2065 = BgL_rpz00_5851;
																				goto
																					BgL_zc3z04anonymousza31856ze3z87_2068;
																			}
																		}
																	}
																else
																	{	/* Lalr/lalr.scm 773 */
																		{	/* Lalr/lalr.scm 778 */
																			bool_t BgL_test2673z00_5855;

																			{	/* Lalr/lalr.scm 778 */
																				obj_t BgL_vectorz00_3836;
																				long BgL_kz00_3837;

																				BgL_vectorz00_3836 =
																					BGl_consistentz00zz__lalr_globalz00;
																				BgL_kz00_3837 =
																					(long) CINT(BgL_statenoz00_2066);
																				BgL_test2673z00_5855 =
																					CBOOL(VECTOR_REF(BgL_vectorz00_3836,
																						BgL_kz00_3837));
																			}
																			if (BgL_test2673z00_5855)
																				{	/* Lalr/lalr.scm 778 */
																					BFALSE;
																				}
																			else
																				{	/* Lalr/lalr.scm 778 */
																					BGl_addzd2lookbackzd2edgez00zz__lalr_expandz00
																						(BgL_statenoz00_2066,
																						BgL_za2rulepza2_2061,
																						BgL_iz00_2048);
																				}
																		}
																		{	/* Lalr/lalr.scm 781 */
																			obj_t BgL_g1166z00_2075;
																			long BgL_g1167z00_2076;

																			BgL_g1166z00_2075 =
																				CDR(((obj_t) BgL_statesz00_2067));
																			BgL_g1167z00_2076 =
																				((long) CINT(BgL_rpz00_2065) - 1L);
																			{
																				bool_t BgL_donez00_2078;
																				obj_t BgL_stpz00_2079;
																				long BgL_rp2z00_2080;
																				obj_t BgL_edgpz00_2081;

																				BgL_donez00_2078 = ((bool_t) 0);
																				BgL_stpz00_2079 = BgL_g1166z00_2075;
																				BgL_rp2z00_2080 = BgL_g1167z00_2076;
																				BgL_edgpz00_2081 = BgL_edgesz00_2058;
																			BgL_zc3z04anonymousza31861ze3z87_2082:
																				if (BgL_donez00_2078)
																					{	/* Lalr/lalr.scm 794 */
																						obj_t BgL_arg1862z00_2083;

																						BgL_arg1862z00_2083 =
																							CDR(((obj_t) BgL_rulepz00_2057));
																						{
																							obj_t BgL_edgesz00_5868;
																							obj_t BgL_rulepz00_5867;

																							BgL_rulepz00_5867 =
																								BgL_arg1862z00_2083;
																							BgL_edgesz00_5868 =
																								BgL_edgpz00_2081;
																							BgL_edgesz00_2058 =
																								BgL_edgesz00_5868;
																							BgL_rulepz00_2057 =
																								BgL_rulepz00_5867;
																							goto
																								BgL_zc3z04anonymousza31854ze3z87_2059;
																						}
																					}
																				else
																					{	/* Lalr/lalr.scm 786 */
																						obj_t BgL_za2rpza2_2084;

																						BgL_za2rpza2_2084 =
																							VECTOR_REF
																							(BGl_ritemz00zz__lalr_globalz00,
																							BgL_rp2z00_2080);
																						{	/* Lalr/lalr.scm 787 */
																							bool_t BgL_test2675z00_5870;

																							if (BGl_2zc3zc3zz__r4_numbers_6_5z00(BINT(-1L), BgL_za2rpza2_2084))
																								{	/* Lalr/lalr.scm 787 */
																									bool_t BgL_test2677z00_5874;

																									if (INTEGERP
																										(BgL_za2rpza2_2084))
																										{	/* Lalr/lalr.scm 787 */
																											BgL_test2677z00_5874 =
																												INTEGERP
																												(BGl_nvarsz00zz__lalr_globalz00);
																										}
																									else
																										{	/* Lalr/lalr.scm 787 */
																											BgL_test2677z00_5874 =
																												((bool_t) 0);
																										}
																									if (BgL_test2677z00_5874)
																										{	/* Lalr/lalr.scm 787 */
																											BgL_test2675z00_5870 =
																												(
																												(long)
																												CINT(BgL_za2rpza2_2084)
																												<
																												(long)
																												CINT
																												(BGl_nvarsz00zz__lalr_globalz00));
																										}
																									else
																										{	/* Lalr/lalr.scm 787 */
																											BgL_test2675z00_5870 =
																												BGl_2zc3zc3zz__r4_numbers_6_5z00
																												(BgL_za2rpza2_2084,
																												BGl_nvarsz00zz__lalr_globalz00);
																										}
																								}
																							else
																								{	/* Lalr/lalr.scm 787 */
																									BgL_test2675z00_5870 =
																										((bool_t) 0);
																								}
																							if (BgL_test2675z00_5870)
																								{	/* Lalr/lalr.scm 788 */
																									bool_t BgL_arg1866z00_2088;
																									obj_t BgL_arg1868z00_2089;
																									long BgL_arg1869z00_2090;
																									obj_t BgL_arg1870z00_2091;

																									{	/* Lalr/lalr.scm 788 */
																										bool_t BgL_test2679z00_5882;

																										{	/* Lalr/lalr.scm 788 */
																											obj_t BgL_vectorz00_3845;
																											long BgL_kz00_3846;

																											BgL_vectorz00_3845 =
																												BGl_nullablez00zz__lalr_globalz00;
																											BgL_kz00_3846 =
																												(long)
																												CINT(BgL_za2rpza2_2084);
																											BgL_test2679z00_5882 =
																												CBOOL(VECTOR_REF
																												(BgL_vectorz00_3845,
																													BgL_kz00_3846));
																										}
																										if (BgL_test2679z00_5882)
																											{	/* Lalr/lalr.scm 788 */
																												BgL_arg1866z00_2088 =
																													((bool_t) 0);
																											}
																										else
																											{	/* Lalr/lalr.scm 788 */
																												BgL_arg1866z00_2088 =
																													((bool_t) 1);
																											}
																									}
																									BgL_arg1868z00_2089 =
																										CDR(
																										((obj_t) BgL_stpz00_2079));
																									BgL_arg1869z00_2090 =
																										(BgL_rp2z00_2080 - 1L);
																									{	/* Lalr/lalr.scm 791 */
																										long BgL_arg1872z00_2093;

																										{	/* Lalr/lalr.scm 791 */
																											obj_t BgL_arg1873z00_2094;

																											BgL_arg1873z00_2094 =
																												CAR(
																												((obj_t)
																													BgL_stpz00_2079));
																											BgL_arg1872z00_2093 =
																												BGl_mapzd2gotozd2zz__lalr_expandz00
																												(BgL_arg1873z00_2094,
																												BgL_za2rpza2_2084);
																										}
																										BgL_arg1870z00_2091 =
																											MAKE_YOUNG_PAIR(BINT
																											(BgL_arg1872z00_2093),
																											BgL_edgpz00_2081);
																									}
																									{
																										obj_t BgL_edgpz00_5897;
																										long BgL_rp2z00_5896;
																										obj_t BgL_stpz00_5895;
																										bool_t BgL_donez00_5894;

																										BgL_donez00_5894 =
																											BgL_arg1866z00_2088;
																										BgL_stpz00_5895 =
																											BgL_arg1868z00_2089;
																										BgL_rp2z00_5896 =
																											BgL_arg1869z00_2090;
																										BgL_edgpz00_5897 =
																											BgL_arg1870z00_2091;
																										BgL_edgpz00_2081 =
																											BgL_edgpz00_5897;
																										BgL_rp2z00_2080 =
																											BgL_rp2z00_5896;
																										BgL_stpz00_2079 =
																											BgL_stpz00_5895;
																										BgL_donez00_2078 =
																											BgL_donez00_5894;
																										goto
																											BgL_zc3z04anonymousza31861ze3z87_2082;
																									}
																								}
																							else
																								{
																									bool_t BgL_donez00_5898;

																									BgL_donez00_5898 =
																										((bool_t) 1);
																									BgL_donez00_2078 =
																										BgL_donez00_5898;
																									goto
																										BgL_zc3z04anonymousza31861ze3z87_2082;
																								}
																						}
																					}
																			}
																		}
																	}
															}
														}
													}
												}
											else
												{	/* Lalr/lalr.scm 767 */
													VECTOR_SET(BGl_includesz00zz__lalr_globalz00,
														BgL_iz00_2048, BgL_edgesz00_2058);
												}
										}
									}
								}
								{
									long BgL_iz00_5900;

									BgL_iz00_5900 = (BgL_iz00_2048 + 1L);
									BgL_iz00_2048 = BgL_iz00_5900;
									goto BgL_zc3z04anonymousza31851ze3z87_2049;
								}
							}
					}
				}
				return (BGl_includesz00zz__lalr_globalz00 =
					BGl_transposez00zz__lalr_expandz00(BGl_includesz00zz__lalr_globalz00,
						BGl_ngotosz00zz__lalr_globalz00), BUNSPEC);
			}
		}

	}



/* compute-lookaheads */
	bool_t BGl_computezd2lookaheadszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 800 */
			{	/* Lalr/lalr.scm 801 */
				obj_t BgL_nz00_2124;

				BgL_nz00_2124 =
					VECTOR_REF(BGl_lookaheadsz00zz__lalr_globalz00,
					(long) CINT(BGl_nstatesz00zz__lalr_globalz00));
				{
					long BgL_iz00_2126;

					BgL_iz00_2126 = 0L;
				BgL_zc3z04anonymousza31884ze3z87_2127:
					if ((BgL_iz00_2126 < (long) CINT(BgL_nz00_2124)))
						{	/* Lalr/lalr.scm 804 */
							obj_t BgL_g1168z00_2129;

							BgL_g1168z00_2129 =
								VECTOR_REF(BGl_lookbackz00zz__lalr_globalz00, BgL_iz00_2126);
							{
								obj_t BgL_spz00_2131;

								BgL_spz00_2131 = BgL_g1168z00_2129;
							BgL_zc3z04anonymousza31886ze3z87_2132:
								if (PAIRP(BgL_spz00_2131))
									{	/* Lalr/lalr.scm 806 */
										obj_t BgL_lazd2izd2_2134;
										obj_t BgL_fzd2jzd2_2135;

										BgL_lazd2izd2_2134 =
											VECTOR_REF(BGl_LAz00zz__lalr_globalz00, BgL_iz00_2126);
										{	/* Lalr/lalr.scm 807 */
											obj_t BgL_arg1897z00_2148;

											BgL_arg1897z00_2148 = CAR(BgL_spz00_2131);
											BgL_fzd2jzd2_2135 =
												VECTOR_REF(BGl_Fz00zz__lalr_globalz00,
												(long) CINT(BgL_arg1897z00_2148));
										}
										{
											long BgL_iz00_2137;

											BgL_iz00_2137 = 0L;
										BgL_zc3z04anonymousza31888ze3z87_2138:
											{	/* Lalr/lalr.scm 808 */
												bool_t BgL_test2682z00_5915;

												if (INTEGERP
													(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00))
													{	/* Lalr/lalr.scm 808 */
														BgL_test2682z00_5915 =
															(BgL_iz00_2137 ==
															(long)
															CINT
															(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00));
													}
												else
													{	/* Lalr/lalr.scm 808 */
														BgL_test2682z00_5915 =
															BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT
															(BgL_iz00_2137),
															BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00);
													}
												if (BgL_test2682z00_5915)
													{	/* Lalr/lalr.scm 808 */
														((bool_t) 0);
													}
												else
													{	/* Lalr/lalr.scm 808 */
														{	/* Lalr/lalr.scm 808 */
															long BgL_arg1891z00_2141;

															{	/* Lalr/lalr.scm 808 */
																obj_t BgL_arg1892z00_2142;
																obj_t BgL_arg1893z00_2143;

																BgL_arg1892z00_2142 =
																	VECTOR_REF(
																	((obj_t) BgL_lazd2izd2_2134), BgL_iz00_2137);
																BgL_arg1893z00_2143 =
																	VECTOR_REF(
																	((obj_t) BgL_fzd2jzd2_2135), BgL_iz00_2137);
																BgL_arg1891z00_2141 =
																	(
																	(long) CINT(BgL_arg1892z00_2142) |
																	(long) CINT(BgL_arg1893z00_2143));
															}
															VECTOR_SET(
																((obj_t) BgL_lazd2izd2_2134), BgL_iz00_2137,
																BINT(BgL_arg1891z00_2141));
														}
														{
															long BgL_iz00_5932;

															BgL_iz00_5932 = (BgL_iz00_2137 + 1L);
															BgL_iz00_2137 = BgL_iz00_5932;
															goto BgL_zc3z04anonymousza31888ze3z87_2138;
														}
													}
											}
										}
										{
											obj_t BgL_spz00_5934;

											BgL_spz00_5934 = CDR(BgL_spz00_2131);
											BgL_spz00_2131 = BgL_spz00_5934;
											goto BgL_zc3z04anonymousza31886ze3z87_2132;
										}
									}
								else
									{
										long BgL_iz00_5936;

										BgL_iz00_5936 = (BgL_iz00_2126 + 1L);
										BgL_iz00_2126 = BgL_iz00_5936;
										goto BgL_zc3z04anonymousza31884ze3z87_2127;
									}
							}
						}
					else
						{	/* Lalr/lalr.scm 803 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* digraph */
	bool_t BGl_digraphz00zz__lalr_expandz00(obj_t BgL_relationz00_22)
	{
		{	/* Lalr/lalr.scm 814 */
			{	/* Lalr/lalr.scm 814 */
				struct bgl_cell BgL_box2684_4447z00;
				obj_t BgL_topz00_4447;

				BgL_topz00_4447 = MAKE_CELL_STACK(BINT(0L), BgL_box2684_4447z00);
				{	/* Lalr/lalr.scm 815 */
					long BgL_infinityz00_2153;

					BgL_infinityz00_2153 =
						((long) CINT(BGl_ngotosz00zz__lalr_globalz00) + 2L);
					{	/* Lalr/lalr.scm 816 */
						obj_t BgL_indexz00_2154;

						BgL_indexz00_2154 =
							make_vector(
							((long) CINT(BGl_ngotosz00zz__lalr_globalz00) + 1L), BINT(0L));
						{	/* Lalr/lalr.scm 817 */
							obj_t BgL_verticesz00_2155;

							BgL_verticesz00_2155 =
								make_vector(
								((long) CINT(BGl_ngotosz00zz__lalr_globalz00) + 1L), BINT(0L));
							{
								long BgL_iz00_2159;

								BgL_iz00_2159 = 0L;
							BgL_zc3z04anonymousza31899ze3z87_2160:
								if (
									(BgL_iz00_2159 <
										(long) CINT(BGl_ngotosz00zz__lalr_globalz00)))
									{	/* Lalr/lalr.scm 853 */
										{	/* Lalr/lalr.scm 855 */
											bool_t BgL_test2686z00_5952;

											{	/* Lalr/lalr.scm 855 */
												bool_t BgL_test2687z00_5953;

												{	/* Lalr/lalr.scm 855 */
													long BgL_b1249z00_2174;

													BgL_b1249z00_2174 =
														(long) CINT(VECTOR_REF(BgL_indexz00_2154,
															BgL_iz00_2159));
													{	/* Lalr/lalr.scm 855 */

														BgL_test2687z00_5953 = (0L == BgL_b1249z00_2174);
												}}
												if (BgL_test2687z00_5953)
													{	/* Lalr/lalr.scm 856 */
														obj_t BgL_arg1906z00_2172;

														BgL_arg1906z00_2172 =
															VECTOR_REF(
															((obj_t) BgL_relationz00_22), BgL_iz00_2159);
														BgL_test2686z00_5952 = PAIRP(BgL_arg1906z00_2172);
													}
												else
													{	/* Lalr/lalr.scm 855 */
														BgL_test2686z00_5952 = ((bool_t) 0);
													}
											}
											if (BgL_test2686z00_5952)
												{	/* Lalr/lalr.scm 855 */
													BGl_traverseze70ze7zz__lalr_expandz00
														(BgL_infinityz00_2153, BgL_relationz00_22,
														BgL_indexz00_2154, BgL_verticesz00_2155,
														BgL_topz00_4447, BINT(BgL_iz00_2159));
												}
											else
												{	/* Lalr/lalr.scm 855 */
													((bool_t) 0);
												}
										}
										{
											long BgL_iz00_5962;

											BgL_iz00_5962 = (BgL_iz00_2159 + 1L);
											BgL_iz00_2159 = BgL_iz00_5962;
											goto BgL_zc3z04anonymousza31899ze3z87_2160;
										}
									}
								else
									{	/* Lalr/lalr.scm 853 */
										return ((bool_t) 0);
									}
							}
						}
					}
				}
			}
		}

	}



/* traverse~0 */
	bool_t BGl_traverseze70ze7zz__lalr_expandz00(long BgL_infinityz00_4444,
		obj_t BgL_rz00_4443, obj_t BgL_indexz00_4442, obj_t BgL_verticesz00_4441,
		obj_t BgL_topz00_4440, obj_t BgL_iz00_2178)
	{
		{	/* Lalr/lalr.scm 822 */
			{	/* Lalr/lalr.scm 822 */
				obj_t BgL_auxz00_4445;

				BgL_auxz00_4445 =
					ADDFX(BINT(1L), ((obj_t) ((obj_t) CELL_REF(BgL_topz00_4440))));
				CELL_SET(BgL_topz00_4440, BgL_auxz00_4445);
			}
			{	/* Lalr/lalr.scm 823 */
				long BgL_kz00_3882;

				BgL_kz00_3882 =
					(long) CINT(((obj_t) ((obj_t) CELL_REF(BgL_topz00_4440))));
				VECTOR_SET(BgL_verticesz00_4441, BgL_kz00_3882, BgL_iz00_2178);
			}
			{	/* Lalr/lalr.scm 824 */
				long BgL_heightz00_2180;

				BgL_heightz00_2180 =
					(long) CINT(((obj_t) ((obj_t) CELL_REF(BgL_topz00_4440))));
				VECTOR_SET(BgL_indexz00_4442,
					(long) CINT(BgL_iz00_2178), BINT(BgL_heightz00_2180));
				{	/* Lalr/lalr.scm 826 */
					obj_t BgL_rpz00_2181;

					{	/* Lalr/lalr.scm 826 */
						long BgL_kz00_3886;

						BgL_kz00_3886 = (long) CINT(BgL_iz00_2178);
						BgL_rpz00_2181 = VECTOR_REF(((obj_t) BgL_rz00_4443), BgL_kz00_3886);
					}
					if (PAIRP(BgL_rpz00_2181))
						{
							obj_t BgL_rp2z00_2184;

							BgL_rp2z00_2184 = BgL_rpz00_2181;
						BgL_zc3z04anonymousza31913ze3z87_2185:
							if (PAIRP(BgL_rp2z00_2184))
								{	/* Lalr/lalr.scm 830 */
									obj_t BgL_jz00_2187;

									BgL_jz00_2187 = CAR(BgL_rp2z00_2184);
									{	/* Lalr/lalr.scm 831 */
										bool_t BgL_test2690z00_5983;

										{	/* Lalr/lalr.scm 831 */
											long BgL_b1246z00_2193;

											{	/* Lalr/lalr.scm 831 */
												long BgL_kz00_3889;

												BgL_kz00_3889 = (long) CINT(BgL_jz00_2187);
												BgL_b1246z00_2193 =
													(long) CINT(VECTOR_REF(BgL_indexz00_4442,
														BgL_kz00_3889));
											}
											{	/* Lalr/lalr.scm 831 */

												BgL_test2690z00_5983 = (0L == BgL_b1246z00_2193);
										}}
										if (BgL_test2690z00_5983)
											{	/* Lalr/lalr.scm 831 */
												BGl_traverseze70ze7zz__lalr_expandz00
													(BgL_infinityz00_4444, BgL_rz00_4443,
													BgL_indexz00_4442, BgL_verticesz00_4441,
													BgL_topz00_4440, BgL_jz00_2187);
											}
										else
											{	/* Lalr/lalr.scm 831 */
												((bool_t) 0);
											}
									}
									{	/* Lalr/lalr.scm 833 */
										bool_t BgL_test2691z00_5989;

										{	/* Lalr/lalr.scm 833 */
											long BgL_arg1923z00_2199;
											long BgL_arg1924z00_2200;

											{	/* Lalr/lalr.scm 833 */
												long BgL_kz00_3893;

												BgL_kz00_3893 = (long) CINT(BgL_iz00_2178);
												BgL_arg1923z00_2199 =
													(long) CINT(VECTOR_REF(BgL_indexz00_4442,
														BgL_kz00_3893));
											}
											{	/* Lalr/lalr.scm 834 */
												long BgL_kz00_3895;

												BgL_kz00_3895 = (long) CINT(BgL_jz00_2187);
												BgL_arg1924z00_2200 =
													(long) CINT(VECTOR_REF(BgL_indexz00_4442,
														BgL_kz00_3895));
											}
											BgL_test2691z00_5989 =
												(BgL_arg1923z00_2199 > BgL_arg1924z00_2200);
										}
										if (BgL_test2691z00_5989)
											{	/* Lalr/lalr.scm 835 */
												long BgL_arg1920z00_2198;

												{	/* Lalr/lalr.scm 835 */
													long BgL_kz00_3899;

													BgL_kz00_3899 = (long) CINT(BgL_jz00_2187);
													BgL_arg1920z00_2198 =
														(long) CINT(VECTOR_REF(BgL_indexz00_4442,
															BgL_kz00_3899));
												}
												VECTOR_SET(BgL_indexz00_4442,
													(long) CINT(BgL_iz00_2178),
													BINT(BgL_arg1920z00_2198));
											}
										else
											{	/* Lalr/lalr.scm 833 */
												BFALSE;
											}
									}
									{	/* Lalr/lalr.scm 836 */
										obj_t BgL_fzd2izd2_2201;
										obj_t BgL_fzd2jzd2_2202;

										BgL_fzd2izd2_2201 =
											VECTOR_REF(BGl_Fz00zz__lalr_globalz00,
											(long) CINT(BgL_iz00_2178));
										BgL_fzd2jzd2_2202 =
											VECTOR_REF(BGl_Fz00zz__lalr_globalz00,
											(long) CINT(BgL_jz00_2187));
										{
											long BgL_iz00_2204;

											BgL_iz00_2204 = 0L;
										BgL_zc3z04anonymousza31925ze3z87_2205:
											{	/* Lalr/lalr.scm 838 */
												bool_t BgL_test2692z00_6007;

												if (INTEGERP
													(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00))
													{	/* Lalr/lalr.scm 838 */
														BgL_test2692z00_6007 =
															(BgL_iz00_2204 ==
															(long)
															CINT
															(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00));
													}
												else
													{	/* Lalr/lalr.scm 838 */
														BgL_test2692z00_6007 =
															BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT
															(BgL_iz00_2204),
															BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00);
													}
												if (BgL_test2692z00_6007)
													{	/* Lalr/lalr.scm 838 */
														((bool_t) 0);
													}
												else
													{	/* Lalr/lalr.scm 838 */
														{	/* Lalr/lalr.scm 838 */
															long BgL_arg1928z00_2208;

															{	/* Lalr/lalr.scm 838 */
																obj_t BgL_arg1929z00_2209;
																obj_t BgL_arg1930z00_2210;

																BgL_arg1929z00_2209 =
																	VECTOR_REF(
																	((obj_t) BgL_fzd2izd2_2201), BgL_iz00_2204);
																BgL_arg1930z00_2210 =
																	VECTOR_REF(
																	((obj_t) BgL_fzd2jzd2_2202), BgL_iz00_2204);
																BgL_arg1928z00_2208 =
																	(
																	(long) CINT(BgL_arg1929z00_2209) |
																	(long) CINT(BgL_arg1930z00_2210));
															}
															VECTOR_SET(
																((obj_t) BgL_fzd2izd2_2201), BgL_iz00_2204,
																BINT(BgL_arg1928z00_2208));
														}
														{
															long BgL_iz00_6024;

															BgL_iz00_6024 = (BgL_iz00_2204 + 1L);
															BgL_iz00_2204 = BgL_iz00_6024;
															goto BgL_zc3z04anonymousza31925ze3z87_2205;
														}
													}
											}
										}
									}
									{
										obj_t BgL_rp2z00_6026;

										BgL_rp2z00_6026 = CDR(BgL_rp2z00_2184);
										BgL_rp2z00_2184 = BgL_rp2z00_6026;
										goto BgL_zc3z04anonymousza31913ze3z87_2185;
									}
								}
							else
								{	/* Lalr/lalr.scm 829 */
									((bool_t) 0);
								}
						}
					else
						{	/* Lalr/lalr.scm 827 */
							((bool_t) 0);
						}
					{	/* Lalr/lalr.scm 840 */
						bool_t BgL_test2694z00_6028;

						{	/* Lalr/lalr.scm 840 */
							long BgL_a1247z00_2240;

							{	/* Lalr/lalr.scm 840 */
								long BgL_kz00_3919;

								BgL_kz00_3919 = (long) CINT(BgL_iz00_2178);
								BgL_a1247z00_2240 =
									(long) CINT(VECTOR_REF(BgL_indexz00_4442, BgL_kz00_3919));
							}
							BgL_test2694z00_6028 = (BgL_a1247z00_2240 == BgL_heightz00_2180);
						}
						if (BgL_test2694z00_6028)
							{

							BgL_zc3z04anonymousza31935ze3z87_2220:
								{	/* Lalr/lalr.scm 842 */
									obj_t BgL_jz00_2221;

									{	/* Lalr/lalr.scm 842 */
										long BgL_kz00_3923;

										BgL_kz00_3923 =
											(long) CINT(
											((obj_t) ((obj_t) CELL_REF(BgL_topz00_4440))));
										BgL_jz00_2221 =
											VECTOR_REF(BgL_verticesz00_4441, BgL_kz00_3923);
									}
									{	/* Lalr/lalr.scm 843 */
										obj_t BgL_auxz00_4446;

										BgL_auxz00_4446 =
											SUBFX(
											((obj_t) ((obj_t) CELL_REF(BgL_topz00_4440))), BINT(1L));
										CELL_SET(BgL_topz00_4440, BgL_auxz00_4446);
									}
									VECTOR_SET(BgL_indexz00_4442,
										(long) CINT(BgL_jz00_2221), BINT(BgL_infinityz00_4444));
									{	/* Lalr/lalr.scm 845 */
										bool_t BgL_test2695z00_6042;

										{	/* Lalr/lalr.scm 845 */
											bool_t BgL_test2696z00_6043;

											if (INTEGERP(BgL_iz00_2178))
												{	/* Lalr/lalr.scm 845 */
													BgL_test2696z00_6043 = INTEGERP(BgL_jz00_2221);
												}
											else
												{	/* Lalr/lalr.scm 845 */
													BgL_test2696z00_6043 = ((bool_t) 0);
												}
											if (BgL_test2696z00_6043)
												{	/* Lalr/lalr.scm 845 */
													BgL_test2695z00_6042 =
														(
														(long) CINT(BgL_iz00_2178) ==
														(long) CINT(BgL_jz00_2221));
												}
											else
												{	/* Lalr/lalr.scm 845 */
													BgL_test2695z00_6042 =
														BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_iz00_2178,
														BgL_jz00_2221);
												}
										}
										if (BgL_test2695z00_6042)
											{	/* Lalr/lalr.scm 845 */
												return ((bool_t) 0);
											}
										else
											{	/* Lalr/lalr.scm 845 */
												{
													long BgL_iz00_2225;

													BgL_iz00_2225 = 0L;
												BgL_zc3z04anonymousza31938ze3z87_2226:
													{	/* Lalr/lalr.scm 847 */
														bool_t BgL_test2698z00_6051;

														if (INTEGERP
															(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00))
															{	/* Lalr/lalr.scm 847 */
																BgL_test2698z00_6051 =
																	(BgL_iz00_2225 ==
																	(long)
																	CINT
																	(BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00));
															}
														else
															{	/* Lalr/lalr.scm 847 */
																BgL_test2698z00_6051 =
																	BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT
																	(BgL_iz00_2225),
																	BGl_tokenzd2setzd2siza7eza7zz__lalr_globalz00);
															}
														if (BgL_test2698z00_6051)
															{	/* Lalr/lalr.scm 847 */
																((bool_t) 0);
															}
														else
															{	/* Lalr/lalr.scm 847 */
																{	/* Lalr/lalr.scm 847 */
																	obj_t BgL_arg1941z00_2229;
																	long BgL_arg1942z00_2230;

																	BgL_arg1941z00_2229 =
																		VECTOR_REF(BGl_Fz00zz__lalr_globalz00,
																		BgL_iz00_2225);
																	{	/* Lalr/lalr.scm 847 */
																		obj_t BgL_arg1943z00_2231;
																		obj_t BgL_arg1944z00_2232;

																		{	/* Lalr/lalr.scm 847 */
																			obj_t BgL_arg1945z00_2233;

																			BgL_arg1945z00_2233 =
																				VECTOR_REF(BGl_Fz00zz__lalr_globalz00,
																				BgL_iz00_2225);
																			BgL_arg1943z00_2231 =
																				VECTOR_REF(((obj_t)
																					BgL_arg1945z00_2233), BgL_iz00_2225);
																		}
																		{	/* Lalr/lalr.scm 848 */
																			obj_t BgL_arg1946z00_2234;

																			BgL_arg1946z00_2234 =
																				VECTOR_REF(BGl_Fz00zz__lalr_globalz00,
																				(long) CINT(BgL_jz00_2221));
																			BgL_arg1944z00_2232 =
																				VECTOR_REF(
																				((obj_t) BgL_arg1946z00_2234),
																				BgL_iz00_2225);
																		}
																		BgL_arg1942z00_2230 =
																			(
																			(long) CINT(BgL_arg1943z00_2231) |
																			(long) CINT(BgL_arg1944z00_2232));
																	}
																	VECTOR_SET(
																		((obj_t) BgL_arg1941z00_2229),
																		BgL_iz00_2225, BINT(BgL_arg1942z00_2230));
																}
																{
																	long BgL_iz00_6072;

																	BgL_iz00_6072 = (BgL_iz00_2225 + 1L);
																	BgL_iz00_2225 = BgL_iz00_6072;
																	goto BgL_zc3z04anonymousza31938ze3z87_2226;
																}
															}
													}
												}
												goto BgL_zc3z04anonymousza31935ze3z87_2220;
											}
									}
								}
							}
						else
							{	/* Lalr/lalr.scm 840 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* build-rule */
	obj_t BGl_buildzd2rulezd2zz__lalr_expandz00(long BgL_nz00_23)
	{
		{	/* Lalr/lalr.scm 863 */
			{	/* Lalr/lalr.scm 865 */
				obj_t BgL_arg1950z00_2245;
				obj_t BgL_arg1951z00_2246;

				{	/* Lalr/lalr.scm 865 */
					obj_t BgL_arg1952z00_2247;

					BgL_arg1952z00_2247 =
						VECTOR_REF(BGl_rlhsz00zz__lalr_globalz00, BgL_nz00_23);
					BgL_arg1950z00_2245 =
						VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
						(long) CINT(BgL_arg1952z00_2247));
				}
				{	/* Lalr/lalr.scm 868 */
					obj_t BgL_arg1953z00_2248;

					BgL_arg1953z00_2248 =
						BGl_loopze70ze7zz__lalr_expandz00(VECTOR_REF
						(BGl_rrhsz00zz__lalr_globalz00, BgL_nz00_23));
					BgL_arg1951z00_2246 =
						MAKE_YOUNG_PAIR(BGl_symbol2471z00zz__lalr_expandz00,
						BgL_arg1953z00_2248);
				}
				return MAKE_YOUNG_PAIR(BgL_arg1950z00_2245, BgL_arg1951z00_2246);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__lalr_expandz00(obj_t BgL_posz00_2251)
	{
		{	/* Lalr/lalr.scm 868 */
			{	/* Lalr/lalr.scm 869 */
				obj_t BgL_xz00_2253;

				BgL_xz00_2253 =
					VECTOR_REF(BGl_ritemz00zz__lalr_globalz00,
					(long) CINT(BgL_posz00_2251));
				if (((long) CINT(BgL_xz00_2253) < 0L))
					{	/* Lalr/lalr.scm 870 */
						return BNIL;
					}
				else
					{	/* Lalr/lalr.scm 872 */
						obj_t BgL_arg1956z00_2255;
						obj_t BgL_arg1957z00_2256;

						BgL_arg1956z00_2255 =
							VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
							(long) CINT(BgL_xz00_2253));
						{	/* Lalr/lalr.scm 873 */
							long BgL_arg1958z00_2257;

							BgL_arg1958z00_2257 = ((long) CINT(BgL_posz00_2251) + 1L);
							BgL_arg1957z00_2256 =
								BGl_loopze70ze7zz__lalr_expandz00(BINT(BgL_arg1958z00_2257));
						}
						return MAKE_YOUNG_PAIR(BgL_arg1956z00_2255, BgL_arg1957z00_2256);
					}
			}
		}

	}



/* build-tables */
	obj_t BGl_buildzd2tableszd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 880 */
			{
				long BgL_statez00_2410;
				obj_t BgL_actionz00_2411;

				BGl_actionzd2tablezd2zz__lalr_globalz00 =
					make_vector((long) CINT(BGl_nstatesz00zz__lalr_globalz00), BNIL);
				{
					long BgL_iz00_2266;

					BgL_iz00_2266 = 0L;
				BgL_zc3z04anonymousza31963ze3z87_2267:
					{	/* Lalr/lalr.scm 956 */
						bool_t BgL_test2701z00_6095;

						if (INTEGERP(BGl_nstatesz00zz__lalr_globalz00))
							{	/* Lalr/lalr.scm 957 */
								BgL_test2701z00_6095 =
									(BgL_iz00_2266 ==
									(long) CINT(BGl_nstatesz00zz__lalr_globalz00));
							}
						else
							{	/* Lalr/lalr.scm 957 */
								BgL_test2701z00_6095 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_2266),
									BGl_nstatesz00zz__lalr_globalz00);
							}
						if (BgL_test2701z00_6095)
							{	/* Lalr/lalr.scm 956 */
								((bool_t) 0);
							}
						else
							{	/* Lalr/lalr.scm 956 */
								{	/* Lalr/lalr.scm 958 */
									obj_t BgL_redz00_2270;

									BgL_redz00_2270 =
										VECTOR_REF(BGl_reductionzd2tablezd2zz__lalr_globalz00,
										BgL_iz00_2266);
									{	/* Lalr/lalr.scm 959 */
										bool_t BgL_test2703z00_6103;

										if (CBOOL(BgL_redz00_2270))
											{	/* Lalr/lalr.scm 959 */
												obj_t BgL_arg1991z00_2320;

												BgL_arg1991z00_2320 =
													VECTOR_REF(((obj_t) BgL_redz00_2270), 1L);
												BgL_test2703z00_6103 =
													((long) CINT(BgL_arg1991z00_2320) >= 1L);
											}
										else
											{	/* Lalr/lalr.scm 959 */
												BgL_test2703z00_6103 = ((bool_t) 0);
											}
										if (BgL_test2703z00_6103)
											{	/* Lalr/lalr.scm 960 */
												bool_t BgL_test2705z00_6110;

												{	/* Lalr/lalr.scm 960 */
													bool_t BgL_test2706z00_6111;

													{	/* Lalr/lalr.scm 960 */
														obj_t BgL_a1250z00_2317;

														BgL_a1250z00_2317 =
															VECTOR_REF(((obj_t) BgL_redz00_2270), 1L);
														{	/* Lalr/lalr.scm 960 */

															if (INTEGERP(BgL_a1250z00_2317))
																{	/* Lalr/lalr.scm 960 */
																	BgL_test2706z00_6111 =
																		((long) CINT(BgL_a1250z00_2317) == 1L);
																}
															else
																{	/* Lalr/lalr.scm 960 */
																	BgL_test2706z00_6111 =
																		BGl_2zd3zd3zz__r4_numbers_6_5z00
																		(BgL_a1250z00_2317, BINT(1L));
																}
														}
													}
													if (BgL_test2706z00_6111)
														{	/* Lalr/lalr.scm 960 */
															obj_t BgL_vectorz00_4062;

															BgL_vectorz00_4062 =
																BGl_consistentz00zz__lalr_globalz00;
															BgL_test2705z00_6110 =
																CBOOL(VECTOR_REF(BgL_vectorz00_4062,
																	BgL_iz00_2266));
														}
													else
														{	/* Lalr/lalr.scm 960 */
															BgL_test2705z00_6110 = ((bool_t) 0);
														}
												}
												if (BgL_test2705z00_6110)
													{	/* Lalr/lalr.scm 965 */
														obj_t BgL_arg1971z00_2281;

														{	/* Lalr/lalr.scm 965 */
															obj_t BgL_arg1972z00_2282;

															{	/* Lalr/lalr.scm 965 */
																obj_t BgL_arg1974z00_2284;

																BgL_arg1974z00_2284 =
																	VECTOR_REF(((obj_t) BgL_redz00_2270), 2L);
																BgL_arg1972z00_2282 =
																	CAR(((obj_t) BgL_arg1974z00_2284));
															}
															BgL_arg1971z00_2281 =
																BGl_zd2zd2zz__r4_numbers_6_5z00
																(BgL_arg1972z00_2282, BNIL);
														}
														BgL_statez00_2410 = BgL_iz00_2266;
														BgL_actionz00_2411 = BgL_arg1971z00_2281;
														{
															long BgL_iz00_2414;

															BgL_iz00_2414 = 1L;
														BgL_zc3z04anonymousza32061ze3z87_2415:
															{	/* Lalr/lalr.scm 950 */
																bool_t BgL_test2708z00_6127;

																if (INTEGERP(BGl_ntermsz00zz__lalr_globalz00))
																	{	/* Lalr/lalr.scm 951 */
																		BgL_test2708z00_6127 =
																			(BgL_iz00_2414 ==
																			(long)
																			CINT(BGl_ntermsz00zz__lalr_globalz00));
																	}
																else
																	{	/* Lalr/lalr.scm 951 */
																		BgL_test2708z00_6127 =
																			BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT
																			(BgL_iz00_2414),
																			BGl_ntermsz00zz__lalr_globalz00);
																	}
																if (BgL_test2708z00_6127)
																	{	/* Lalr/lalr.scm 950 */
																		((bool_t) 0);
																	}
																else
																	{	/* Lalr/lalr.scm 950 */
																		BGl_addzd2actionze70z35zz__lalr_expandz00
																			(BINT(BgL_statez00_2410), BgL_iz00_2414,
																			BgL_actionz00_2411);
																		{
																			long BgL_iz00_6136;

																			BgL_iz00_6136 = (BgL_iz00_2414 + 1L);
																			BgL_iz00_2414 = BgL_iz00_6136;
																			goto
																				BgL_zc3z04anonymousza32061ze3z87_2415;
																		}
																	}
															}
														}
													}
												else
													{	/* Lalr/lalr.scm 966 */
														obj_t BgL_kz00_2285;

														BgL_kz00_2285 =
															VECTOR_REF(BGl_lookaheadsz00zz__lalr_globalz00,
															(BgL_iz00_2266 + 1L));
														{	/* Lalr/lalr.scm 967 */
															obj_t BgL_g1175z00_2286;

															BgL_g1175z00_2286 =
																VECTOR_REF(BGl_lookaheadsz00zz__lalr_globalz00,
																BgL_iz00_2266);
															{
																obj_t BgL_jz00_2288;

																BgL_jz00_2288 = BgL_g1175z00_2286;
															BgL_zc3z04anonymousza31975ze3z87_2289:
																if (
																	((long) CINT(BgL_jz00_2288) <
																		(long) CINT(BgL_kz00_2285)))
																	{	/* Lalr/lalr.scm 969 */
																		long BgL_rulez00_2291;
																		obj_t BgL_lavz00_2292;

																		{	/* Lalr/lalr.scm 969 */
																			obj_t BgL_arg1989z00_2313;

																			BgL_arg1989z00_2313 =
																				VECTOR_REF
																				(BGl_LArulenoz00zz__lalr_globalz00,
																				(long) CINT(BgL_jz00_2288));
																			BgL_rulez00_2291 =
																				NEG((long) CINT(BgL_arg1989z00_2313));
																		}
																		BgL_lavz00_2292 =
																			VECTOR_REF(BGl_LAz00zz__lalr_globalz00,
																			(long) CINT(BgL_jz00_2288));
																		{	/* Lalr/lalr.scm 971 */
																			obj_t BgL_g1176z00_2293;

																			BgL_g1176z00_2293 =
																				VECTOR_REF(
																				((obj_t) BgL_lavz00_2292), 0L);
																			{
																				long BgL_tokenz00_2295;
																				obj_t BgL_xz00_2296;
																				long BgL_yz00_2297;
																				long BgL_za7za7_2298;

																				BgL_tokenz00_2295 = 0L;
																				BgL_xz00_2296 = BgL_g1176z00_2293;
																				BgL_yz00_2297 = 1L;
																				BgL_za7za7_2298 = 0L;
																			BgL_zc3z04anonymousza31977ze3z87_2299:
																				if (
																					(BgL_tokenz00_2295 <
																						(long)
																						CINT
																						(BGl_ntermsz00zz__lalr_globalz00)))
																					{	/* Lalr/lalr.scm 972 */
																						{	/* Lalr/lalr.scm 974 */
																							long BgL_inzd2lazd2setzf3zf3_2301;

																							BgL_inzd2lazd2setzf3zf3_2301 =
																								BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00
																								((long) CINT(BgL_xz00_2296),
																								2L);
																							if ((BgL_inzd2lazd2setzf3zf3_2301
																									== 1L))
																								{	/* Lalr/lalr.scm 975 */
																									BGl_addzd2actionze70z35zz__lalr_expandz00
																										(BINT(BgL_iz00_2266),
																										BgL_tokenz00_2295,
																										BINT(BgL_rulez00_2291));
																								}
																							else
																								{	/* Lalr/lalr.scm 975 */
																									BFALSE;
																								}
																						}
																						if ((BgL_yz00_2297 == 28L))
																							{	/* Lalr/lalr.scm 978 */
																								long BgL_arg1981z00_2304;
																								obj_t BgL_arg1982z00_2305;
																								long BgL_arg1983z00_2306;

																								BgL_arg1981z00_2304 =
																									(BgL_tokenz00_2295 + 1L);
																								{	/* Lalr/lalr.scm 979 */
																									long BgL_arg1984z00_2307;

																									BgL_arg1984z00_2307 =
																										(BgL_za7za7_2298 + 1L);
																									BgL_arg1982z00_2305 =
																										VECTOR_REF(
																										((obj_t) BgL_lavz00_2292),
																										BgL_arg1984z00_2307);
																								}
																								BgL_arg1983z00_2306 =
																									(BgL_za7za7_2298 + 1L);
																								{
																									long BgL_za7za7_6173;
																									long BgL_yz00_6172;
																									obj_t BgL_xz00_6171;
																									long BgL_tokenz00_6170;

																									BgL_tokenz00_6170 =
																										BgL_arg1981z00_2304;
																									BgL_xz00_6171 =
																										BgL_arg1982z00_2305;
																									BgL_yz00_6172 = 1L;
																									BgL_za7za7_6173 =
																										BgL_arg1983z00_2306;
																									BgL_za7za7_2298 =
																										BgL_za7za7_6173;
																									BgL_yz00_2297 = BgL_yz00_6172;
																									BgL_xz00_2296 = BgL_xz00_6171;
																									BgL_tokenz00_2295 =
																										BgL_tokenz00_6170;
																									goto
																										BgL_zc3z04anonymousza31977ze3z87_2299;
																								}
																							}
																						else
																							{	/* Lalr/lalr.scm 982 */
																								long BgL_arg1985z00_2308;
																								long BgL_arg1986z00_2309;
																								long BgL_arg1987z00_2310;

																								BgL_arg1985z00_2308 =
																									(BgL_tokenz00_2295 + 1L);
																								{	/* Lalr/lalr.scm 982 */
																									long BgL_tmpz00_6175;

																									BgL_tmpz00_6175 =
																										(long) CINT(BgL_xz00_2296);
																									BgL_arg1986z00_2309 =
																										(BgL_tmpz00_6175 / 2L);
																								}
																								BgL_arg1987z00_2310 =
																									(BgL_yz00_2297 + 1L);
																								{
																									long BgL_yz00_6182;
																									obj_t BgL_xz00_6180;
																									long BgL_tokenz00_6179;

																									BgL_tokenz00_6179 =
																										BgL_arg1985z00_2308;
																									BgL_xz00_6180 =
																										BINT(BgL_arg1986z00_2309);
																									BgL_yz00_6182 =
																										BgL_arg1987z00_2310;
																									BgL_yz00_2297 = BgL_yz00_6182;
																									BgL_xz00_2296 = BgL_xz00_6180;
																									BgL_tokenz00_2295 =
																										BgL_tokenz00_6179;
																									goto
																										BgL_zc3z04anonymousza31977ze3z87_2299;
																								}
																							}
																					}
																				else
																					{	/* Lalr/lalr.scm 972 */
																						((bool_t) 0);
																					}
																			}
																		}
																		{	/* Lalr/lalr.scm 983 */
																			long BgL_arg1988z00_2312;

																			BgL_arg1988z00_2312 =
																				((long) CINT(BgL_jz00_2288) + 1L);
																			{
																				obj_t BgL_jz00_6185;

																				BgL_jz00_6185 =
																					BINT(BgL_arg1988z00_2312);
																				BgL_jz00_2288 = BgL_jz00_6185;
																				goto
																					BgL_zc3z04anonymousza31975ze3z87_2289;
																			}
																		}
																	}
																else
																	{	/* Lalr/lalr.scm 968 */
																		((bool_t) 0);
																	}
															}
														}
													}
											}
										else
											{	/* Lalr/lalr.scm 959 */
												((bool_t) 0);
											}
									}
								}
								{	/* Lalr/lalr.scm 985 */
									obj_t BgL_shiftpz00_2321;

									BgL_shiftpz00_2321 =
										VECTOR_REF(BGl_shiftzd2tablezd2zz__lalr_globalz00,
										BgL_iz00_2266);
									if (CBOOL(BgL_shiftpz00_2321))
										{	/* Lalr/lalr.scm 987 */
											obj_t BgL_g1177z00_2322;

											BgL_g1177z00_2322 =
												VECTOR_REF(((obj_t) BgL_shiftpz00_2321), 2L);
											{
												obj_t BgL_kz00_2324;

												BgL_kz00_2324 = BgL_g1177z00_2322;
											BgL_zc3z04anonymousza31992ze3z87_2325:
												if (PAIRP(BgL_kz00_2324))
													{	/* Lalr/lalr.scm 989 */
														obj_t BgL_statez00_2327;

														BgL_statez00_2327 = CAR(BgL_kz00_2324);
														{	/* Lalr/lalr.scm 989 */
															obj_t BgL_symbolz00_2328;

															BgL_symbolz00_2328 =
																VECTOR_REF
																(BGl_acceszd2symbolzd2zz__lalr_globalz00,
																(long) CINT(BgL_statez00_2327));
															{	/* Lalr/lalr.scm 990 */

																if (
																	((long) CINT(BgL_symbolz00_2328) >=
																		(long)
																		CINT(BGl_nvarsz00zz__lalr_globalz00)))
																	{	/* Lalr/lalr.scm 991 */
																		BGl_addzd2actionze70z35zz__lalr_expandz00
																			(BINT(BgL_iz00_2266),
																			((long) CINT(BgL_symbolz00_2328) -
																				(long)
																				CINT(BGl_nvarsz00zz__lalr_globalz00)),
																			BgL_statez00_2327);
																	}
																else
																	{	/* Lalr/lalr.scm 991 */
																		BFALSE;
																	}
																{
																	obj_t BgL_kz00_6206;

																	BgL_kz00_6206 = CDR(BgL_kz00_2324);
																	BgL_kz00_2324 = BgL_kz00_6206;
																	goto BgL_zc3z04anonymousza31992ze3z87_2325;
																}
															}
														}
													}
												else
													{	/* Lalr/lalr.scm 988 */
														((bool_t) 0);
													}
											}
										}
									else
										{	/* Lalr/lalr.scm 986 */
											((bool_t) 0);
										}
								}
								{
									long BgL_iz00_6208;

									BgL_iz00_6208 = (BgL_iz00_2266 + 1L);
									BgL_iz00_2266 = BgL_iz00_6208;
									goto BgL_zc3z04anonymousza31963ze3z87_2267;
								}
							}
					}
				}
				return
					BGl_addzd2actionze70z35zz__lalr_expandz00
					(BGl_finalzd2statezd2zz__lalr_globalz00, 0L,
					BGl_symbol2473z00zz__lalr_expandz00);
			}
		}

	}



/* add-action~0 */
	obj_t BGl_addzd2actionze70z35zz__lalr_expandz00(obj_t BgL_stz00_2336,
		long BgL_symz00_2337, obj_t BgL_actz00_2338)
	{
		{	/* Lalr/lalr.scm 947 */
			{	/* Lalr/lalr.scm 883 */
				obj_t BgL_xz00_2340;

				BgL_xz00_2340 =
					VECTOR_REF(BGl_actionzd2tablezd2zz__lalr_globalz00,
					(long) CINT(BgL_stz00_2336));
				{	/* Lalr/lalr.scm 883 */
					obj_t BgL_yz00_2341;

					BgL_yz00_2341 =
						BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_symz00_2337),
						BgL_xz00_2340);
					{	/* Lalr/lalr.scm 884 */

						if (CBOOL(BgL_yz00_2341))
							{	/* Lalr/lalr.scm 886 */
								obj_t BgL_curzd2preczd2_2342;
								obj_t BgL_newzd2preczd2_2343;

								{	/* Lalr/lalr.scm 886 */
									obj_t BgL_arg2057z00_2407;

									BgL_arg2057z00_2407 = CDR(((obj_t) BgL_yz00_2341));
									if (((long) CINT(BgL_arg2057z00_2407) <= 0L))
										{	/* Lalr/lalr.scm 876 */
											BgL_curzd2preczd2_2342 =
												VECTOR_REF(BGl_rprecz00zz__lalr_globalz00,
												NEG((long) CINT(BgL_arg2057z00_2407)));
										}
									else
										{	/* Lalr/lalr.scm 878 */
											obj_t BgL_arg1961z00_3984;

											BgL_arg1961z00_3984 =
												VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
												(BgL_symz00_2337 +
													(long) CINT(BGl_nvarsz00zz__lalr_globalz00)));
											BgL_curzd2preczd2_2342 =
												BGl_getpropz00zz__r4_symbols_6_4z00(BgL_arg1961z00_3984,
												BGl_symbol2451z00zz__lalr_expandz00);
								}}
								if (((long) CINT(BgL_actz00_2338) <= 0L))
									{	/* Lalr/lalr.scm 876 */
										BgL_newzd2preczd2_2343 =
											VECTOR_REF(BGl_rprecz00zz__lalr_globalz00,
											NEG((long) CINT(BgL_actz00_2338)));
									}
								else
									{	/* Lalr/lalr.scm 878 */
										obj_t BgL_arg1961z00_3996;

										BgL_arg1961z00_3996 =
											VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
											(BgL_symz00_2337 +
												(long) CINT(BGl_nvarsz00zz__lalr_globalz00)));
										BgL_newzd2preczd2_2343 =
											BGl_getpropz00zz__r4_symbols_6_4z00(BgL_arg1961z00_3996,
											BGl_symbol2451z00zz__lalr_expandz00);
									}
								if (
									((long) CINT(BgL_actz00_2338) ==
										(long) CINT(CDR(((obj_t) BgL_yz00_2341)))))
									{	/* Lalr/lalr.scm 889 */
										return BTRUE;
									}
								else
									{	/* Lalr/lalr.scm 891 */
										bool_t BgL_test2721z00_6245;

										if (((long) CINT(CDR(((obj_t) BgL_yz00_2341))) <= 0L))
											{	/* Lalr/lalr.scm 891 */
												BgL_test2721z00_6245 =
													((long) CINT(BgL_actz00_2338) <= 0L);
											}
										else
											{	/* Lalr/lalr.scm 891 */
												BgL_test2721z00_6245 = ((bool_t) 0);
											}
										if (BgL_test2721z00_6245)
											{	/* Lalr/lalr.scm 891 */
												{	/* Lalr/lalr.scm 895 */
													obj_t BgL_arg2006z00_2350;
													obj_t BgL_arg2007z00_2351;
													obj_t BgL_arg2008z00_2352;

													BgL_arg2006z00_2350 =
														BGl_buildzd2rulezd2zz__lalr_expandz00(NEG(
															(long) CINT(BgL_actz00_2338)));
													{	/* Lalr/lalr.scm 896 */
														long BgL_arg2019z00_2363;

														BgL_arg2019z00_2363 =
															NEG((long) CINT(CDR(((obj_t) BgL_yz00_2341))));
														BgL_arg2007z00_2351 =
															BGl_buildzd2rulezd2zz__lalr_expandz00
															(BgL_arg2019z00_2363);
													}
													BgL_arg2008z00_2352 =
														VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
														((long) CINT(BGl_nvarsz00zz__lalr_globalz00) +
															BgL_symz00_2337));
													{	/* Lalr/lalr.scm 893 */
														obj_t BgL_list2009z00_2353;

														{	/* Lalr/lalr.scm 893 */
															obj_t BgL_arg2010z00_2354;

															{	/* Lalr/lalr.scm 893 */
																obj_t BgL_arg2011z00_2355;

																{	/* Lalr/lalr.scm 893 */
																	obj_t BgL_arg2012z00_2356;

																	{	/* Lalr/lalr.scm 893 */
																		obj_t BgL_arg2013z00_2357;

																		{	/* Lalr/lalr.scm 893 */
																			obj_t BgL_arg2014z00_2358;

																			{	/* Lalr/lalr.scm 893 */
																				obj_t BgL_arg2015z00_2359;

																				{	/* Lalr/lalr.scm 893 */
																					obj_t BgL_arg2016z00_2360;

																					{	/* Lalr/lalr.scm 893 */
																						obj_t BgL_arg2017z00_2361;

																						BgL_arg2017z00_2361 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2475z00zz__lalr_expandz00,
																							BNIL);
																						BgL_arg2016z00_2360 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2008z00_2352,
																							BgL_arg2017z00_2361);
																					}
																					BgL_arg2015z00_2359 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2476z00zz__lalr_expandz00,
																						BgL_arg2016z00_2360);
																				}
																				BgL_arg2014z00_2358 =
																					MAKE_YOUNG_PAIR(BgL_arg2007z00_2351,
																					BgL_arg2015z00_2359);
																			}
																			BgL_arg2013z00_2357 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2477z00zz__lalr_expandz00,
																				BgL_arg2014z00_2358);
																		}
																		BgL_arg2012z00_2356 =
																			MAKE_YOUNG_PAIR(BgL_arg2006z00_2350,
																			BgL_arg2013z00_2357);
																	}
																	BgL_arg2011z00_2355 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2477z00zz__lalr_expandz00,
																		BgL_arg2012z00_2356);
																}
																BgL_arg2010z00_2354 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2478z00zz__lalr_expandz00,
																	BgL_arg2011z00_2355);
															}
															BgL_list2009z00_2353 =
																MAKE_YOUNG_PAIR
																(BGl_string2446z00zz__lalr_expandz00,
																BgL_arg2010z00_2354);
														}
														BGl_warningz00zz__errorz00(BgL_list2009z00_2353);
												}}
												{	/* Lalr/lalr.scm 899 */
													obj_t BgL_arg2022z00_2366;

													BgL_arg2022z00_2366 =
														BGl_2maxz00zz__r4_numbers_6_5z00(CDR(
															((obj_t) BgL_yz00_2341)), BgL_actz00_2338);
													{	/* Lalr/lalr.scm 899 */
														obj_t BgL_tmpz00_6277;

														BgL_tmpz00_6277 = ((obj_t) BgL_yz00_2341);
														return
															SET_CDR(BgL_tmpz00_6277, BgL_arg2022z00_2366);
													}
												}
											}
										else
											{	/* Lalr/lalr.scm 900 */
												bool_t BgL_test2723z00_6280;

												if (CBOOL(BgL_curzd2preczd2_2342))
													{	/* Lalr/lalr.scm 900 */
														BgL_test2723z00_6280 = ((bool_t) 1);
													}
												else
													{	/* Lalr/lalr.scm 900 */
														BgL_test2723z00_6280 =
															CBOOL(BgL_newzd2preczd2_2343);
													}
												if (BgL_test2723z00_6280)
													{	/* Lalr/lalr.scm 900 */
														if (CBOOL(BgL_newzd2preczd2_2343))
															{	/* Lalr/lalr.scm 902 */
																if (CBOOL(BgL_curzd2preczd2_2342))
																	{	/* Lalr/lalr.scm 904 */
																		if (
																			((long) CINT(CDR(
																						((obj_t) BgL_curzd2preczd2_2342)))
																				==
																				(long) CINT(CDR(((obj_t)
																							BgL_newzd2preczd2_2343)))))
																			{	/* Lalr/lalr.scm 908 */
																				obj_t BgL_shiftz00_2373;
																				obj_t BgL_reducez00_2374;

																				BgL_shiftz00_2373 =
																					BGl_2maxz00zz__r4_numbers_6_5z00(CDR(
																						((obj_t) BgL_yz00_2341)),
																					BgL_actz00_2338);
																				BgL_reducez00_2374 =
																					BGl_2minz00zz__r4_numbers_6_5z00(CDR((
																							(obj_t) BgL_yz00_2341)),
																					BgL_actz00_2338);
																				{	/* Lalr/lalr.scm 910 */
																					obj_t BgL_casezd2valuezd2_2375;

																					BgL_casezd2valuezd2_2375 =
																						CAR(
																						((obj_t) BgL_curzd2preczd2_2342));
																					if (
																						(BgL_casezd2valuezd2_2375 ==
																							BGl_keyword2456z00zz__lalr_expandz00))
																						{	/* Lalr/lalr.scm 912 */
																							obj_t BgL_tmpz00_6306;

																							BgL_tmpz00_6306 =
																								((obj_t) BgL_yz00_2341);
																							return
																								SET_CDR(BgL_tmpz00_6306,
																								BgL_reducez00_2374);
																						}
																					else
																						{	/* Lalr/lalr.scm 910 */
																							if (
																								(BgL_casezd2valuezd2_2375 ==
																									BGl_keyword2458z00zz__lalr_expandz00))
																								{	/* Lalr/lalr.scm 914 */
																									obj_t BgL_tmpz00_6311;

																									BgL_tmpz00_6311 =
																										((obj_t) BgL_yz00_2341);
																									return
																										SET_CDR(BgL_tmpz00_6311,
																										BgL_shiftz00_2373);
																								}
																							else
																								{	/* Lalr/lalr.scm 910 */
																									if (
																										(BgL_casezd2valuezd2_2375 ==
																											BGl_keyword2460z00zz__lalr_expandz00))
																										{	/* Lalr/lalr.scm 918 */
																											obj_t BgL_objz00_4034;

																											BgL_objz00_4034 =
																												BGl_symbol2479z00zz__lalr_expandz00;
																											{	/* Lalr/lalr.scm 918 */
																												obj_t BgL_tmpz00_6316;

																												BgL_tmpz00_6316 =
																													((obj_t)
																													BgL_yz00_2341);
																												return
																													SET_CDR
																													(BgL_tmpz00_6316,
																													BgL_objz00_4034);
																											}
																										}
																									else
																										{	/* Lalr/lalr.scm 910 */
																											return BUNSPEC;
																										}
																								}
																						}
																				}
																			}
																		else
																			{	/* Lalr/lalr.scm 906 */
																				if (
																					((long) CINT(CDR(
																								((obj_t)
																									BgL_curzd2preczd2_2342))) >
																						(long) CINT(CDR(((obj_t)
																									BgL_newzd2preczd2_2343)))))
																					{	/* Lalr/lalr.scm 935 */
																						obj_t BgL_tmpz00_6327;

																						BgL_tmpz00_6327 =
																							((obj_t) BgL_yz00_2341);
																						return
																							SET_CDR(BgL_tmpz00_6327,
																							BgL_actz00_2338);
																					}
																				else
																					{	/* Lalr/lalr.scm 933 */
																						return BTRUE;
																					}
																			}
																	}
																else
																	{	/* Lalr/lalr.scm 905 */
																		obj_t BgL_tmpz00_6330;

																		BgL_tmpz00_6330 = ((obj_t) BgL_yz00_2341);
																		return
																			SET_CDR(BgL_tmpz00_6330, BgL_actz00_2338);
																	}
															}
														else
															{	/* Lalr/lalr.scm 902 */
																return BTRUE;
															}
													}
												else
													{	/* Lalr/lalr.scm 900 */
														{	/* Lalr/lalr.scm 943 */
															obj_t BgL_arg2038z00_2390;
															obj_t BgL_arg2039z00_2391;

															{	/* Lalr/lalr.scm 943 */
																long BgL_arg2050z00_2401;

																BgL_arg2050z00_2401 =
																	NEG(
																	(long) CINT(CDR(((obj_t) BgL_yz00_2341))));
																BgL_arg2038z00_2390 =
																	BGl_buildzd2rulezd2zz__lalr_expandz00
																	(BgL_arg2050z00_2401);
															}
															BgL_arg2039z00_2391 =
																VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
																((long) CINT(BGl_nvarsz00zz__lalr_globalz00) +
																	BgL_symz00_2337));
															{	/* Lalr/lalr.scm 940 */
																obj_t BgL_list2040z00_2392;

																{	/* Lalr/lalr.scm 940 */
																	obj_t BgL_arg2041z00_2393;

																	{	/* Lalr/lalr.scm 940 */
																		obj_t BgL_arg2042z00_2394;

																		{	/* Lalr/lalr.scm 940 */
																			obj_t BgL_arg2044z00_2395;

																			{	/* Lalr/lalr.scm 940 */
																				obj_t BgL_arg2045z00_2396;

																				{	/* Lalr/lalr.scm 940 */
																					obj_t BgL_arg2046z00_2397;

																					{	/* Lalr/lalr.scm 940 */
																						obj_t BgL_arg2047z00_2398;

																						{	/* Lalr/lalr.scm 940 */
																							obj_t BgL_arg2048z00_2399;

																							{	/* Lalr/lalr.scm 940 */
																								obj_t BgL_arg2049z00_2400;

																								BgL_arg2049z00_2400 =
																									MAKE_YOUNG_PAIR
																									(BGl_string2475z00zz__lalr_expandz00,
																									BNIL);
																								BgL_arg2048z00_2399 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2039z00_2391,
																									BgL_arg2049z00_2400);
																							}
																							BgL_arg2047z00_2398 =
																								MAKE_YOUNG_PAIR
																								(BGl_string2476z00zz__lalr_expandz00,
																								BgL_arg2048z00_2399);
																						}
																						BgL_arg2046z00_2397 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2038z00_2390,
																							BgL_arg2047z00_2398);
																					}
																					BgL_arg2045z00_2396 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2481z00zz__lalr_expandz00,
																						BgL_arg2046z00_2397);
																				}
																				BgL_arg2044z00_2395 =
																					MAKE_YOUNG_PAIR(BgL_actz00_2338,
																					BgL_arg2045z00_2396);
																			}
																			BgL_arg2042z00_2394 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2482z00zz__lalr_expandz00,
																				BgL_arg2044z00_2395);
																		}
																		BgL_arg2041z00_2393 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2483z00zz__lalr_expandz00,
																			BgL_arg2042z00_2394);
																	}
																	BgL_list2040z00_2392 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2446z00zz__lalr_expandz00,
																		BgL_arg2041z00_2393);
																}
																BGl_warningz00zz__errorz00
																	(BgL_list2040z00_2392);
														}}
														{	/* Lalr/lalr.scm 946 */
															obj_t BgL_tmpz00_6351;

															BgL_tmpz00_6351 = ((obj_t) BgL_yz00_2341);
															return SET_CDR(BgL_tmpz00_6351, BgL_actz00_2338);
														}
													}
											}
									}
							}
						else
							{	/* Lalr/lalr.scm 947 */
								obj_t BgL_arg2058z00_2408;

								{	/* Lalr/lalr.scm 947 */
									obj_t BgL_arg2059z00_2409;

									BgL_arg2059z00_2409 =
										MAKE_YOUNG_PAIR(BINT(BgL_symz00_2337), BgL_actz00_2338);
									BgL_arg2058z00_2408 =
										MAKE_YOUNG_PAIR(BgL_arg2059z00_2409, BgL_xz00_2340);
								}
								return
									VECTOR_SET(BGl_actionzd2tablezd2zz__lalr_globalz00,
									(long) CINT(BgL_stz00_2336), BgL_arg2058z00_2408);
		}}}}}

	}



/* compact-action-table */
	bool_t BGl_compactzd2actionzd2tablez00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 997 */
			{
				obj_t BgL_actsz00_2462;

				{
					long BgL_iz00_2425;

					BgL_iz00_2425 = 0L;
				BgL_zc3z04anonymousza32065ze3z87_2426:
					{	/* Lalr/lalr.scm 1018 */
						bool_t BgL_test2732z00_6359;

						if (INTEGERP(BGl_nstatesz00zz__lalr_globalz00))
							{	/* Lalr/lalr.scm 1019 */
								BgL_test2732z00_6359 =
									(BgL_iz00_2425 ==
									(long) CINT(BGl_nstatesz00zz__lalr_globalz00));
							}
						else
							{	/* Lalr/lalr.scm 1019 */
								BgL_test2732z00_6359 =
									BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_2425),
									BGl_nstatesz00zz__lalr_globalz00);
							}
						if (BgL_test2732z00_6359)
							{	/* Lalr/lalr.scm 1018 */
								return ((bool_t) 0);
							}
						else
							{	/* Lalr/lalr.scm 1018 */
								{	/* Lalr/lalr.scm 1020 */
									obj_t BgL_actsz00_2429;

									BgL_actsz00_2429 =
										VECTOR_REF(BGl_actionzd2tablezd2zz__lalr_globalz00,
										BgL_iz00_2425);
									{	/* Lalr/lalr.scm 1021 */
										bool_t BgL_test2734z00_6367;

										{	/* Lalr/lalr.scm 1021 */
											obj_t BgL_tmpz00_6368;

											BgL_tmpz00_6368 =
												VECTOR_REF(BGl_reductionzd2tablezd2zz__lalr_globalz00,
												BgL_iz00_2425);
											BgL_test2734z00_6367 = VECTORP(BgL_tmpz00_6368);
										}
										if (BgL_test2734z00_6367)
											{	/* Lalr/lalr.scm 1022 */
												obj_t BgL_actz00_2432;

												BgL_actsz00_2462 = BgL_actsz00_2429;
												{	/* Lalr/lalr.scm 999 */
													obj_t BgL_accumsz00_2464;

													BgL_accumsz00_2464 = BNIL;
													{
														obj_t BgL_lz00_2466;

														BgL_lz00_2466 = BgL_actsz00_2462;
													BgL_zc3z04anonymousza32092ze3z87_2467:
														if (PAIRP(BgL_lz00_2466))
															{	/* Lalr/lalr.scm 1002 */
																obj_t BgL_xz00_2469;

																BgL_xz00_2469 = CDR(CAR(BgL_lz00_2466));
																{	/* Lalr/lalr.scm 1002 */
																	obj_t BgL_yz00_2470;

																	BgL_yz00_2470 =
																		BGl_assvz00zz__r4_pairs_and_lists_6_3z00
																		(BgL_xz00_2469, BgL_accumsz00_2464);
																	{	/* Lalr/lalr.scm 1003 */

																		{	/* Lalr/lalr.scm 1004 */
																			bool_t BgL_test2736z00_6376;

																			if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
																				(BgL_xz00_2469))
																				{	/* Lalr/lalr.scm 1004 */
																					BgL_test2736z00_6376 =
																						((long) CINT(BgL_xz00_2469) < 0L);
																				}
																			else
																				{	/* Lalr/lalr.scm 1004 */
																					BgL_test2736z00_6376 = ((bool_t) 0);
																				}
																			if (BgL_test2736z00_6376)
																				{	/* Lalr/lalr.scm 1004 */
																					if (CBOOL(BgL_yz00_2470))
																						{	/* Lalr/lalr.scm 1006 */
																							long BgL_arg2096z00_2473;

																							BgL_arg2096z00_2473 =
																								(1L +
																								(long) CINT(CDR(
																										((obj_t) BgL_yz00_2470))));
																							{	/* Lalr/lalr.scm 1006 */
																								obj_t BgL_auxz00_6389;
																								obj_t BgL_tmpz00_6387;

																								BgL_auxz00_6389 =
																									BINT(BgL_arg2096z00_2473);
																								BgL_tmpz00_6387 =
																									((obj_t) BgL_yz00_2470);
																								SET_CDR(BgL_tmpz00_6387,
																									BgL_auxz00_6389);
																						}}
																					else
																						{	/* Lalr/lalr.scm 1007 */
																							obj_t BgL_arg2098z00_2475;

																							BgL_arg2098z00_2475 =
																								MAKE_YOUNG_PAIR(BgL_xz00_2469,
																								BINT(1L));
																							BgL_accumsz00_2464 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2098z00_2475,
																								BgL_accumsz00_2464);
																						}
																				}
																			else
																				{	/* Lalr/lalr.scm 1004 */
																					BFALSE;
																				}
																		}
																		{
																			obj_t BgL_lz00_6395;

																			BgL_lz00_6395 = CDR(BgL_lz00_2466);
																			BgL_lz00_2466 = BgL_lz00_6395;
																			goto
																				BgL_zc3z04anonymousza32092ze3z87_2467;
																		}
																	}
																}
															}
														else
															{	/* Lalr/lalr.scm 1001 */
																((bool_t) 0);
															}
													}
													{
														obj_t BgL_lz00_4162;
														obj_t BgL_maxz00_4163;
														obj_t BgL_symz00_4164;

														BgL_lz00_4162 = BgL_accumsz00_2464;
														BgL_maxz00_4163 = BINT(0L);
														BgL_symz00_4164 = BFALSE;
													BgL_loopz00_4161:
														if (NULLP(BgL_lz00_4162))
															{	/* Lalr/lalr.scm 1011 */
																BgL_actz00_2432 = BgL_symz00_4164;
															}
														else
															{	/* Lalr/lalr.scm 1013 */
																obj_t BgL_xz00_4174;

																BgL_xz00_4174 = CAR(((obj_t) BgL_lz00_4162));
																if (
																	((long) CINT(CDR(
																				((obj_t) BgL_xz00_4174))) >
																		(long) CINT(BgL_maxz00_4163)))
																	{
																		obj_t BgL_symz00_6413;
																		obj_t BgL_maxz00_6410;
																		obj_t BgL_lz00_6407;

																		BgL_lz00_6407 =
																			CDR(((obj_t) BgL_lz00_4162));
																		BgL_maxz00_6410 =
																			CDR(((obj_t) BgL_xz00_4174));
																		BgL_symz00_6413 =
																			CAR(((obj_t) BgL_xz00_4174));
																		BgL_symz00_4164 = BgL_symz00_6413;
																		BgL_maxz00_4163 = BgL_maxz00_6410;
																		BgL_lz00_4162 = BgL_lz00_6407;
																		goto BgL_loopz00_4161;
																	}
																else
																	{
																		obj_t BgL_lz00_6416;

																		BgL_lz00_6416 =
																			CDR(((obj_t) BgL_lz00_4162));
																		BgL_lz00_4162 = BgL_lz00_6416;
																		goto BgL_loopz00_4161;
																	}
															}
													}
												}
												{	/* Lalr/lalr.scm 1024 */
													obj_t BgL_arg2070z00_2433;

													{	/* Lalr/lalr.scm 1024 */
														obj_t BgL_arg2072z00_2434;
														obj_t BgL_arg2074z00_2435;

														{	/* Lalr/lalr.scm 1024 */
															obj_t BgL_arg2075z00_2436;

															if (CBOOL(BgL_actz00_2432))
																{	/* Lalr/lalr.scm 1024 */
																	BgL_arg2075z00_2436 = BgL_actz00_2432;
																}
															else
																{	/* Lalr/lalr.scm 1024 */
																	BgL_arg2075z00_2436 =
																		BGl_symbol2484z00zz__lalr_expandz00;
																}
															BgL_arg2072z00_2434 =
																MAKE_YOUNG_PAIR
																(BGl_za2defaultza2z00zz__lalr_expandz00,
																BgL_arg2075z00_2436);
														}
														{	/* Lalr/lalr.scm 1025 */
															obj_t BgL_hook1256z00_2437;

															BgL_hook1256z00_2437 =
																MAKE_YOUNG_PAIR(BFALSE, BNIL);
															{
																obj_t BgL_l1253z00_2439;
																obj_t BgL_h1254z00_2440;

																BgL_l1253z00_2439 = BgL_actsz00_2429;
																BgL_h1254z00_2440 = BgL_hook1256z00_2437;
															BgL_zc3z04anonymousza32076ze3z87_2441:
																if (NULLP(BgL_l1253z00_2439))
																	{	/* Lalr/lalr.scm 1025 */
																		BgL_arg2074z00_2435 =
																			CDR(BgL_hook1256z00_2437);
																	}
																else
																	{	/* Lalr/lalr.scm 1025 */
																		bool_t BgL_test2743z00_6427;

																		{	/* Lalr/lalr.scm 1026 */
																			bool_t BgL_test2744z00_6428;

																			{	/* Lalr/lalr.scm 1026 */
																				obj_t BgL_tmpz00_6429;

																				{	/* Lalr/lalr.scm 1026 */
																					obj_t BgL_pairz00_4197;

																					BgL_pairz00_4197 =
																						CAR(((obj_t) BgL_l1253z00_2439));
																					BgL_tmpz00_6429 =
																						CDR(BgL_pairz00_4197);
																				}
																				BgL_test2744z00_6428 =
																					(BgL_tmpz00_6429 == BgL_actz00_2432);
																			}
																			if (BgL_test2744z00_6428)
																				{	/* Lalr/lalr.scm 1026 */
																					BgL_test2743z00_6427 = ((bool_t) 0);
																				}
																			else
																				{	/* Lalr/lalr.scm 1026 */
																					BgL_test2743z00_6427 = ((bool_t) 1);
																				}
																		}
																		if (BgL_test2743z00_6427)
																			{	/* Lalr/lalr.scm 1025 */
																				obj_t BgL_nh1255z00_2448;

																				{	/* Lalr/lalr.scm 1025 */
																					obj_t BgL_arg2083z00_2450;

																					BgL_arg2083z00_2450 =
																						CAR(((obj_t) BgL_l1253z00_2439));
																					BgL_nh1255z00_2448 =
																						MAKE_YOUNG_PAIR(BgL_arg2083z00_2450,
																						BNIL);
																				}
																				SET_CDR(BgL_h1254z00_2440,
																					BgL_nh1255z00_2448);
																				{	/* Lalr/lalr.scm 1025 */
																					obj_t BgL_arg2082z00_2449;

																					BgL_arg2082z00_2449 =
																						CDR(((obj_t) BgL_l1253z00_2439));
																					{
																						obj_t BgL_h1254z00_6441;
																						obj_t BgL_l1253z00_6440;

																						BgL_l1253z00_6440 =
																							BgL_arg2082z00_2449;
																						BgL_h1254z00_6441 =
																							BgL_nh1255z00_2448;
																						BgL_h1254z00_2440 =
																							BgL_h1254z00_6441;
																						BgL_l1253z00_2439 =
																							BgL_l1253z00_6440;
																						goto
																							BgL_zc3z04anonymousza32076ze3z87_2441;
																					}
																				}
																			}
																		else
																			{	/* Lalr/lalr.scm 1025 */
																				obj_t BgL_arg2084z00_2451;

																				BgL_arg2084z00_2451 =
																					CDR(((obj_t) BgL_l1253z00_2439));
																				{
																					obj_t BgL_l1253z00_6444;

																					BgL_l1253z00_6444 =
																						BgL_arg2084z00_2451;
																					BgL_l1253z00_2439 = BgL_l1253z00_6444;
																					goto
																						BgL_zc3z04anonymousza32076ze3z87_2441;
																				}
																			}
																	}
															}
														}
														BgL_arg2070z00_2433 =
															MAKE_YOUNG_PAIR(BgL_arg2072z00_2434,
															BgL_arg2074z00_2435);
													}
													VECTOR_SET(BGl_actionzd2tablezd2zz__lalr_globalz00,
														BgL_iz00_2425, BgL_arg2070z00_2433);
												}
											}
										else
											{	/* Lalr/lalr.scm 1029 */
												obj_t BgL_arg2087z00_2456;

												{	/* Lalr/lalr.scm 1029 */
													obj_t BgL_arg2088z00_2457;

													BgL_arg2088z00_2457 =
														MAKE_YOUNG_PAIR
														(BGl_za2defaultza2z00zz__lalr_expandz00,
														BGl_symbol2479z00zz__lalr_expandz00);
													BgL_arg2087z00_2456 =
														MAKE_YOUNG_PAIR(BgL_arg2088z00_2457,
														BgL_actsz00_2429);
												}
												VECTOR_SET(BGl_actionzd2tablezd2zz__lalr_globalz00,
													BgL_iz00_2425, BgL_arg2087z00_2456);
											}
									}
								}
								{
									long BgL_iz00_6450;

									BgL_iz00_6450 = (BgL_iz00_2425 + 1L);
									BgL_iz00_2425 = BgL_iz00_6450;
									goto BgL_zc3z04anonymousza32065ze3z87_2426;
								}
							}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__lalr_expandz00(void)
	{
		{	/* Lalr/lalr.scm 11 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__lalr_utilz00(165615091L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__lalr_genz00(181573618L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__lalr_globalz00(39277513L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__lalr_rewritez00(80638262L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
			return BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string2486z00zz__lalr_expandz00));
		}

	}

#ifdef __cplusplus
}
#endif
