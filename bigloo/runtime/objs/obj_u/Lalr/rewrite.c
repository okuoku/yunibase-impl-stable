/*===========================================================================*/
/*   (Lalr/rewrite.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Lalr/rewrite.scm -indent -o objs/obj_u/Lalr/rewrite.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___LALR_REWRITE_TYPE_DEFINITIONS
#define BGL___LALR_REWRITE_TYPE_DEFINITIONS
#endif													// BGL___LALR_REWRITE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static long BGl_za2maxzd2termza2zd2zz__lalr_rewritez00 = 0L;
	static obj_t BGl_za2plistza2z00zz__lalr_rewritez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2symvza2z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_symbol1671z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_symbol1673z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_symbol1675z00zz__lalr_rewritez00 = BUNSPEC;
	extern obj_t BGl_nvarsz00zz__lalr_globalz00;
	static obj_t BGl_symbol1679z00zz__lalr_rewritez00 = BUNSPEC;
	extern obj_t BGl_ntermsz00zz__lalr_globalz00;
	static obj_t BGl_z62cleanzd2plistzb0zz__lalr_rewritez00(obj_t);
	static obj_t BGl_symbol1681z00zz__lalr_rewritez00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__lalr_rewritez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_rewritezd2grammarz12zc0zz__lalr_rewritez00(obj_t);
	static obj_t BGl_symbolzd2ze3symbolzf2bindingzc3zz__lalr_rewritez00(obj_t);
	extern obj_t BGl_nsymsz00zz__lalr_globalz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__lalr_rewritez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_globalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_list1678z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__lalr_rewritez00(void);
	static obj_t BGl_list1687z00zz__lalr_rewritez00 = BUNSPEC;
	extern obj_t BGl_nrulesz00zz__lalr_globalz00;
	static long BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00 = 0L;
	static obj_t BGl_list1696z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__lalr_rewritez00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__lalr_rewritez00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__lalr_rewritez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__lalr_rewritez00(void);
	static obj_t BGl_objectzd2initzd2zz__lalr_rewritez00(void);
	static bool_t BGl_makezd2symzd2tablez00zz__lalr_rewritez00(void);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_nitemsz00zz__lalr_globalz00;
	static obj_t BGl_z62rewritezd2grammarz12za2zz__lalr_rewritez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cleanzd2plistzd2zz__lalr_rewritez00(void);
	extern obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_setzd2symzd2noz12z12zz__lalr_rewritez00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	extern obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__lalr_rewritez00(void);
	static long BGl_setzd2ntzd2noz12z12zz__lalr_rewritez00(obj_t);
	extern obj_t BGl_grammarz00zz__lalr_globalz00;
	static obj_t BGl_keyword1688z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_keyword1690z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t BGl_keyword1692z00zz__lalr_rewritez00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1669z00zz__lalr_rewritez00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1700z00zz__lalr_rewritez00,
		BgL_bgl_string1700za700za7za7_1701za7, "__lalr_rewrite", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cleanzd2plistzd2envz00zz__lalr_rewritez00,
		BgL_bgl_za762cleanza7d2plist1702z00,
		BGl_z62cleanzd2plistzb0zz__lalr_rewritez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1670z00zz__lalr_rewritez00,
		BgL_bgl_string1670za700za7za7_1703za7, "nt?", 3);
	      DEFINE_STRING(BGl_string1672z00zz__lalr_rewritez00,
		BgL_bgl_string1672za700za7za7_1704za7, "prec", 4);
	      DEFINE_STRING(BGl_string1674z00zz__lalr_rewritez00,
		BgL_bgl_string1674za700za7za7_1705za7, "sym-no", 6);
	      DEFINE_STRING(BGl_string1676z00zz__lalr_rewritez00,
		BgL_bgl_string1676za700za7za7_1706za7, "lalr-grammar", 12);
	      DEFINE_STRING(BGl_string1677z00zz__lalr_rewritez00,
		BgL_bgl_string1677za700za7za7_1707za7, "Grammar symbol already defined",
		30);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rewritezd2grammarz12zd2envz12zz__lalr_rewritez00,
		BgL_bgl_za762rewriteza7d2gra1708z00,
		BGl_z62rewritezd2grammarz12za2zz__lalr_rewritez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1680z00zz__lalr_rewritez00,
		BgL_bgl_string1680za700za7za7_1709za7, "*eoi*", 5);
	      DEFINE_STRING(BGl_string1682z00zz__lalr_rewritez00,
		BgL_bgl_string1682za700za7za7_1710za7, "*start*", 7);
	      DEFINE_STRING(BGl_string1683z00zz__lalr_rewritez00,
		BgL_bgl_string1683za700za7za7_1711za7, "Non-terminal defined twice", 26);
	      DEFINE_STRING(BGl_string1684z00zz__lalr_rewritez00,
		BgL_bgl_string1684za700za7za7_1712za7, "LHS must be a symbol", 20);
	      DEFINE_STRING(BGl_string1685z00zz__lalr_rewritez00,
		BgL_bgl_string1685za700za7za7_1713za7, "Bad rule specification", 22);
	      DEFINE_STRING(BGl_string1686z00zz__lalr_rewritez00,
		BgL_bgl_string1686za700za7za7_1714za7, "Ill-formed grammar", 18);
	      DEFINE_STRING(BGl_string1689z00zz__lalr_rewritez00,
		BgL_bgl_string1689za700za7za7_1715za7, "left", 4);
	      DEFINE_STRING(BGl_string1691z00zz__lalr_rewritez00,
		BgL_bgl_string1691za700za7za7_1716za7, "right", 5);
	      DEFINE_STRING(BGl_string1693z00zz__lalr_rewritez00,
		BgL_bgl_string1693za700za7za7_1717za7, "none", 4);
	      DEFINE_STRING(BGl_string1694z00zz__lalr_rewritez00,
		BgL_bgl_string1694za700za7za7_1718za7, "Bad terminal", 12);
	      DEFINE_STRING(BGl_string1695z00zz__lalr_rewritez00,
		BgL_bgl_string1695za700za7za7_1719za7, "Invalid semantic action", 23);
	      DEFINE_STRING(BGl_string1697z00zz__lalr_rewritez00,
		BgL_bgl_string1697za700za7za7_1720za7, "Undefined symbol", 16);
	      DEFINE_STRING(BGl_string1698z00zz__lalr_rewritez00,
		BgL_bgl_string1698za700za7za7_1721za7, "Invalid symbol in right-hand side",
		33);
	      DEFINE_STRING(BGl_string1699z00zz__lalr_rewritez00,
		BgL_bgl_string1699za700za7za7_1722za7, "Bad right-hand side", 19);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2plistza2z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_za2symvza2z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_symbol1671z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_symbol1673z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_symbol1675z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_symbol1679z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_symbol1681z00zz__lalr_rewritez00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_list1678z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_list1687z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_list1696z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_keyword1688z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_keyword1690z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_keyword1692z00zz__lalr_rewritez00));
		     ADD_ROOT((void *) (&BGl_symbol1669z00zz__lalr_rewritez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__lalr_rewritez00(long
		BgL_checksumz00_2046, char *BgL_fromz00_2047)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__lalr_rewritez00))
				{
					BGl_requirezd2initializa7ationz75zz__lalr_rewritez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__lalr_rewritez00();
					BGl_cnstzd2initzd2zz__lalr_rewritez00();
					BGl_importedzd2moduleszd2initz00zz__lalr_rewritez00();
					return BGl_toplevelzd2initzd2zz__lalr_rewritez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			BGl_symbol1669z00zz__lalr_rewritez00 =
				bstring_to_symbol(BGl_string1670z00zz__lalr_rewritez00);
			BGl_symbol1671z00zz__lalr_rewritez00 =
				bstring_to_symbol(BGl_string1672z00zz__lalr_rewritez00);
			BGl_symbol1673z00zz__lalr_rewritez00 =
				bstring_to_symbol(BGl_string1674z00zz__lalr_rewritez00);
			BGl_symbol1675z00zz__lalr_rewritez00 =
				bstring_to_symbol(BGl_string1676z00zz__lalr_rewritez00);
			BGl_symbol1679z00zz__lalr_rewritez00 =
				bstring_to_symbol(BGl_string1680z00zz__lalr_rewritez00);
			BGl_symbol1681z00zz__lalr_rewritez00 =
				bstring_to_symbol(BGl_string1682z00zz__lalr_rewritez00);
			BGl_list1678z00zz__lalr_rewritez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1679z00zz__lalr_rewritez00,
				MAKE_YOUNG_PAIR(BGl_symbol1681z00zz__lalr_rewritez00, BNIL));
			BGl_keyword1688z00zz__lalr_rewritez00 =
				bstring_to_keyword(BGl_string1689z00zz__lalr_rewritez00);
			BGl_keyword1690z00zz__lalr_rewritez00 =
				bstring_to_keyword(BGl_string1691z00zz__lalr_rewritez00);
			BGl_keyword1692z00zz__lalr_rewritez00 =
				bstring_to_keyword(BGl_string1693z00zz__lalr_rewritez00);
			BGl_list1687z00zz__lalr_rewritez00 =
				MAKE_YOUNG_PAIR(BGl_keyword1688z00zz__lalr_rewritez00,
				MAKE_YOUNG_PAIR(BGl_keyword1690z00zz__lalr_rewritez00,
					MAKE_YOUNG_PAIR(BGl_keyword1692z00zz__lalr_rewritez00, BNIL)));
			return (BGl_list1696z00zz__lalr_rewritez00 =
				MAKE_YOUNG_PAIR(BBOOL(((bool_t) 0)), BNIL), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			BGl_za2plistza2z00zz__lalr_rewritez00 = BFALSE;
			BGl_za2symvza2z00zz__lalr_rewritez00 = BFALSE;
			BGl_za2maxzd2termza2zd2zz__lalr_rewritez00 = 0L;
			return (BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00 = 1L, BUNSPEC);
		}

	}



/* clean-plist */
	BGL_EXPORTED_DEF obj_t BGl_cleanzd2plistzd2zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 56 */
			{
				obj_t BgL_lz00_1193;

				{	/* Lalr/rewrite.scm 57 */
					bool_t BgL_tmpz00_2072;

					BgL_lz00_1193 = BGl_za2plistza2z00zz__lalr_rewritez00;
				BgL_zc3z04anonymousza31198ze3z87_1194:
					if (PAIRP(BgL_lz00_1193))
						{	/* Lalr/rewrite.scm 59 */
							obj_t BgL_symz00_1196;

							BgL_symz00_1196 = CAR(BgL_lz00_1193);
							if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symz00_1196,
										BGl_symbol1669z00zz__lalr_rewritez00)))
								{	/* Lalr/rewrite.scm 60 */
									BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_symz00_1196,
										BGl_symbol1669z00zz__lalr_rewritez00);
								}
							else
								{	/* Lalr/rewrite.scm 60 */
									BFALSE;
								}
							if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symz00_1196,
										BGl_symbol1671z00zz__lalr_rewritez00)))
								{	/* Lalr/rewrite.scm 62 */
									BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_symz00_1196,
										BGl_symbol1671z00zz__lalr_rewritez00);
								}
							else
								{	/* Lalr/rewrite.scm 62 */
									BFALSE;
								}
							BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_symz00_1196,
								BGl_symbol1673z00zz__lalr_rewritez00);
							{
								obj_t BgL_lz00_2085;

								BgL_lz00_2085 = CDR(BgL_lz00_1193);
								BgL_lz00_1193 = BgL_lz00_2085;
								goto BgL_zc3z04anonymousza31198ze3z87_1194;
							}
						}
					else
						{	/* Lalr/rewrite.scm 58 */
							BgL_tmpz00_2072 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_2072);
				}
			}
		}

	}



/* &clean-plist */
	obj_t BGl_z62cleanzd2plistzb0zz__lalr_rewritez00(obj_t BgL_envz00_2034)
	{
		{	/* Lalr/rewrite.scm 56 */
			return BGl_cleanzd2plistzd2zz__lalr_rewritez00();
		}

	}



/* make-sym-table */
	bool_t BGl_makezd2symzd2tablez00zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 70 */
			BGl_za2symvza2z00zz__lalr_rewritez00 =
				make_vector(BGl_za2maxzd2termza2zd2zz__lalr_rewritez00, BFALSE);
			{
				obj_t BgL_lz00_1202;

				BgL_lz00_1202 = BGl_za2plistza2z00zz__lalr_rewritez00;
			BgL_zc3z04anonymousza31203ze3z87_1203:
				if (PAIRP(BgL_lz00_1202))
					{	/* Lalr/rewrite.scm 74 */
						obj_t BgL_symz00_1205;

						BgL_symz00_1205 = CAR(BgL_lz00_1202);
						{	/* Lalr/rewrite.scm 75 */
							obj_t BgL_arg1206z00_1206;

							BgL_arg1206z00_1206 =
								BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symz00_1205,
								BGl_symbol1673z00zz__lalr_rewritez00);
							VECTOR_SET(BGl_za2symvza2z00zz__lalr_rewritez00,
								(long) CINT(BgL_arg1206z00_1206), BgL_symz00_1205);
						}
						{
							obj_t BgL_lz00_2096;

							BgL_lz00_2096 = CDR(BgL_lz00_1202);
							BgL_lz00_1202 = BgL_lz00_2096;
							goto BgL_zc3z04anonymousza31203ze3z87_1203;
						}
					}
				else
					{	/* Lalr/rewrite.scm 73 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* set-nt-no! */
	long BGl_setzd2ntzd2noz12z12zz__lalr_rewritez00(obj_t BgL_symz00_3)
	{
		{	/* Lalr/rewrite.scm 85 */
			{	/* Lalr/rewrite.scm 86 */
				long BgL_xz00_1209;

				BgL_xz00_1209 = BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00;
				BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symz00_3,
					BGl_symbol1669z00zz__lalr_rewritez00, BTRUE);
				BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symz00_3,
					BGl_symbol1673z00zz__lalr_rewritez00,
					BINT(BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00));
				BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00 =
					(BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00 + 1L);
				BGl_za2plistza2z00zz__lalr_rewritez00 =
					MAKE_YOUNG_PAIR(BgL_symz00_3, BGl_za2plistza2z00zz__lalr_rewritez00);
				return BgL_xz00_1209;
			}
		}

	}



/* set-sym-no! */
	obj_t BGl_setzd2symzd2noz12z12zz__lalr_rewritez00(obj_t BgL_symz00_4)
	{
		{	/* Lalr/rewrite.scm 93 */
			{	/* Lalr/rewrite.scm 94 */
				obj_t BgL_xz00_1210;

				BgL_xz00_1210 =
					BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symz00_4,
					BGl_symbol1673z00zz__lalr_rewritez00);
				if (CBOOL(BgL_xz00_1210))
					{	/* Lalr/rewrite.scm 95 */
						return
							BGl_errorz00zz__errorz00(BGl_symbol1675z00zz__lalr_rewritez00,
							BGl_string1677z00zz__lalr_rewritez00, BgL_symz00_4);
					}
				else
					{	/* Lalr/rewrite.scm 97 */
						long BgL_yz00_1211;

						BgL_yz00_1211 = BGl_za2maxzd2termza2zd2zz__lalr_rewritez00;
						BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symz00_4,
							BGl_symbol1673z00zz__lalr_rewritez00,
							BINT(BGl_za2maxzd2termza2zd2zz__lalr_rewritez00));
						BGl_za2plistza2z00zz__lalr_rewritez00 =
							MAKE_YOUNG_PAIR(BgL_symz00_4,
							BGl_za2plistza2z00zz__lalr_rewritez00);
						BGl_za2maxzd2termza2zd2zz__lalr_rewritez00 =
							(BGl_za2maxzd2termza2zd2zz__lalr_rewritez00 + 1L);
						return BINT(BgL_yz00_1211);
					}
			}
		}

	}



/* symbol->symbol/binding */
	obj_t BGl_symbolzd2ze3symbolzf2bindingzc3zz__lalr_rewritez00(obj_t
		BgL_symz00_6)
	{
		{	/* Lalr/rewrite.scm 107 */
			{	/* Lalr/rewrite.scm 108 */
				obj_t BgL_g1039z00_1213;

				{	/* Lalr/rewrite.scm 108 */
					obj_t BgL_arg1223z00_1231;

					{	/* Lalr/rewrite.scm 108 */
						obj_t BgL_arg1454z00_1766;

						BgL_arg1454z00_1766 = SYMBOL_TO_STRING(BgL_symz00_6);
						BgL_arg1223z00_1231 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1454z00_1766);
					}
					BgL_g1039z00_1213 =
						BGl_stringzd2ze3listz31zz__r4_strings_6_7z00(BgL_arg1223z00_1231);
				}
				{
					obj_t BgL_lz00_1216;
					obj_t BgL_prefixz00_1217;

					BgL_lz00_1216 = BgL_g1039z00_1213;
					BgL_prefixz00_1217 = BNIL;
				BgL_zc3z04anonymousza31210ze3z87_1218:
					if (NULLP(BgL_lz00_1216))
						{	/* Lalr/rewrite.scm 110 */
							return BgL_symz00_6;
						}
					else
						{	/* Lalr/rewrite.scm 112 */
							obj_t BgL_cz00_1220;
							obj_t BgL_rz00_1221;

							BgL_cz00_1220 = CAR(((obj_t) BgL_lz00_1216));
							BgL_rz00_1221 = CDR(((obj_t) BgL_lz00_1216));
							if ((CCHAR(BgL_cz00_1220) == ((unsigned char) '@')))
								{	/* Lalr/rewrite.scm 114 */
									if (NULLP(BgL_rz00_1221))
										{	/* Lalr/rewrite.scm 115 */
											return BgL_symz00_6;
										}
									else
										{	/* Lalr/rewrite.scm 115 */
											return
												MAKE_YOUNG_PAIR(bstring_to_symbol
												(BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
													(bgl_reverse(BgL_prefixz00_1217))),
												bstring_to_symbol
												(BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
													(BgL_rz00_1221)));
										}
								}
							else
								{	/* Lalr/rewrite.scm 119 */
									obj_t BgL_arg1221z00_1229;

									BgL_arg1221z00_1229 =
										MAKE_YOUNG_PAIR(BgL_cz00_1220, BgL_prefixz00_1217);
									{
										obj_t BgL_prefixz00_2134;
										obj_t BgL_lz00_2133;

										BgL_lz00_2133 = BgL_rz00_1221;
										BgL_prefixz00_2134 = BgL_arg1221z00_1229;
										BgL_prefixz00_1217 = BgL_prefixz00_2134;
										BgL_lz00_1216 = BgL_lz00_2133;
										goto BgL_zc3z04anonymousza31210ze3z87_1218;
									}
								}
						}
				}
			}
		}

	}



/* rewrite-grammar! */
	BGL_EXPORTED_DEF obj_t BGl_rewritezd2grammarz12zc0zz__lalr_rewritez00(obj_t
		BgL_gramz00_7)
	{
		{	/* Lalr/rewrite.scm 125 */
			{	/* Lalr/rewrite.scm 125 */
				long BgL_nozd2itemszd2_1232;
				long BgL_nozd2ruleszd2_1233;
				long BgL_preczd2levelzd2_1234;

				BgL_nozd2itemszd2_1232 = 0L;
				BgL_nozd2ruleszd2_1233 = 0L;
				BgL_preczd2levelzd2_1234 = 0L;
				{	/* Lalr/rewrite.scm 126 */
					obj_t BgL_thezd2ruleszd2_1235;

					BgL_thezd2ruleszd2_1235 = CDR(((obj_t) BgL_gramz00_7));
					{	/* Lalr/rewrite.scm 127 */
						obj_t BgL_terminalsz00_1236;

						BgL_terminalsz00_1236 = CAR(((obj_t) BgL_gramz00_7));
						BGl_za2plistza2z00zz__lalr_rewritez00 =
							BGl_list1678z00zz__lalr_rewritez00;
						BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00 = 1L;
						{
							obj_t BgL_lz00_1238;

							BgL_lz00_1238 = BgL_thezd2ruleszd2_1235;
						BgL_zc3z04anonymousza31224ze3z87_1239:
							if (PAIRP(BgL_lz00_1238))
								{	/* Lalr/rewrite.scm 139 */
									obj_t BgL_prodsz00_1241;

									BgL_prodsz00_1241 = CAR(BgL_lz00_1238);
									if (PAIRP(BgL_prodsz00_1241))
										{	/* Lalr/rewrite.scm 141 */
											obj_t BgL_lhsz00_1243;

											BgL_lhsz00_1243 = CAR(BgL_prodsz00_1241);
											if (SYMBOLP(BgL_lhsz00_1243))
												{	/* Lalr/rewrite.scm 142 */
													if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00
															(BgL_lhsz00_1243,
																BGl_symbol1669z00zz__lalr_rewritez00)))
														{	/* Lalr/rewrite.scm 143 */
															BGl_errorz00zz__errorz00
																(BGl_symbol1675z00zz__lalr_rewritez00,
																BGl_string1683z00zz__lalr_rewritez00,
																BgL_lhsz00_1243);
														}
													else
														{	/* Lalr/rewrite.scm 143 */
															BGl_setzd2ntzd2noz12z12zz__lalr_rewritez00
																(BgL_lhsz00_1243);
															{
																obj_t BgL_lz00_2152;

																BgL_lz00_2152 = CDR(BgL_lz00_1238);
																BgL_lz00_1238 = BgL_lz00_2152;
																goto BgL_zc3z04anonymousza31224ze3z87_1239;
															}
														}
												}
											else
												{	/* Lalr/rewrite.scm 142 */
													BGl_errorz00zz__errorz00
														(BGl_symbol1675z00zz__lalr_rewritez00,
														BGl_string1684z00zz__lalr_rewritez00,
														BgL_lhsz00_1243);
												}
										}
									else
										{	/* Lalr/rewrite.scm 140 */
											BGl_errorz00zz__errorz00
												(BGl_symbol1675z00zz__lalr_rewritez00,
												BGl_string1685z00zz__lalr_rewritez00,
												BgL_prodsz00_1241);
										}
								}
							else
								{	/* Lalr/rewrite.scm 138 */
									if (NULLP(BgL_lz00_1238))
										{	/* Lalr/rewrite.scm 156 */
											BFALSE;
										}
									else
										{	/* Lalr/rewrite.scm 156 */
											BGl_errorz00zz__errorz00
												(BGl_symbol1675z00zz__lalr_rewritez00,
												BGl_string1686z00zz__lalr_rewritez00, BgL_lz00_1238);
										}
								}
						}
						BGl_za2maxzd2termza2zd2zz__lalr_rewritez00 =
							(BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00 + 1L);
						BGl_putpropz12z12zz__r4_symbols_6_4z00
							(BGl_symbol1679z00zz__lalr_rewritez00,
							BGl_symbol1673z00zz__lalr_rewritez00,
							BINT(BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00));
						BGl_putpropz12z12zz__r4_symbols_6_4z00
							(BGl_symbol1681z00zz__lalr_rewritez00,
							BGl_symbol1673z00zz__lalr_rewritez00, BINT(0L));
						BGl_putpropz12z12zz__r4_symbols_6_4z00
							(BGl_symbol1681z00zz__lalr_rewritez00,
							BGl_symbol1669z00zz__lalr_rewritez00, BTRUE);
						{
							obj_t BgL_l1093z00_1250;

							BgL_l1093z00_1250 = BgL_terminalsz00_1236;
						BgL_zc3z04anonymousza31231ze3z87_1251:
							if (PAIRP(BgL_l1093z00_1250))
								{	/* Lalr/rewrite.scm 168 */
									{	/* Lalr/rewrite.scm 171 */
										obj_t BgL_tz00_1253;

										BgL_tz00_1253 = CAR(BgL_l1093z00_1250);
										if (SYMBOLP(BgL_tz00_1253))
											{	/* Lalr/rewrite.scm 171 */
												BGl_setzd2symzd2noz12z12zz__lalr_rewritez00
													(BgL_tz00_1253);
											}
										else
											{	/* Lalr/rewrite.scm 173 */
												bool_t BgL_test1739z00_2171;

												if (PAIRP(BgL_tz00_1253))
													{	/* Lalr/rewrite.scm 173 */
														BgL_test1739z00_2171 =
															CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
																(BgL_tz00_1253),
																BGl_list1687z00zz__lalr_rewritez00));
													}
												else
													{	/* Lalr/rewrite.scm 173 */
														BgL_test1739z00_2171 = ((bool_t) 0);
													}
												if (BgL_test1739z00_2171)
													{	/* Lalr/rewrite.scm 173 */
														BgL_preczd2levelzd2_1234 =
															(BgL_preczd2levelzd2_1234 + 1L);
														{	/* Lalr/rewrite.scm 175 */
															obj_t BgL_g1092z00_1258;

															BgL_g1092z00_1258 = CDR(BgL_tz00_1253);
															{
																obj_t BgL_l1090z00_1260;

																{	/* Lalr/rewrite.scm 181 */
																	bool_t BgL_tmpz00_2179;

																	BgL_l1090z00_1260 = BgL_g1092z00_1258;
																BgL_zc3z04anonymousza31238ze3z87_1261:
																	if (PAIRP(BgL_l1090z00_1260))
																		{	/* Lalr/rewrite.scm 181 */
																			{	/* Lalr/rewrite.scm 178 */
																				obj_t BgL_innerzd2tzd2_1263;

																				BgL_innerzd2tzd2_1263 =
																					CAR(BgL_l1090z00_1260);
																				if (SYMBOLP(BgL_innerzd2tzd2_1263))
																					{	/* Lalr/rewrite.scm 177 */
																						BFALSE;
																					}
																				else
																					{	/* Lalr/rewrite.scm 177 */
																						BGl_errorz00zz__errorz00
																							(BGl_symbol1675z00zz__lalr_rewritez00,
																							BGl_string1694z00zz__lalr_rewritez00,
																							BgL_innerzd2tzd2_1263);
																					}
																				BGl_setzd2symzd2noz12z12zz__lalr_rewritez00
																					(BgL_innerzd2tzd2_1263);
																				{	/* Lalr/rewrite.scm 180 */
																					obj_t BgL_arg1242z00_1265;

																					{	/* Lalr/rewrite.scm 180 */
																						obj_t BgL_arg1244z00_1266;

																						BgL_arg1244z00_1266 =
																							CAR(((obj_t) BgL_tz00_1253));
																						BgL_arg1242z00_1265 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1244z00_1266,
																							BINT(BgL_preczd2levelzd2_1234));
																					}
																					BGl_putpropz12z12zz__r4_symbols_6_4z00
																						(BgL_innerzd2tzd2_1263,
																						BGl_symbol1671z00zz__lalr_rewritez00,
																						BgL_arg1242z00_1265);
																				}
																			}
																			{
																				obj_t BgL_l1090z00_2192;

																				BgL_l1090z00_2192 =
																					CDR(BgL_l1090z00_1260);
																				BgL_l1090z00_1260 = BgL_l1090z00_2192;
																				goto
																					BgL_zc3z04anonymousza31238ze3z87_1261;
																			}
																		}
																	else
																		{	/* Lalr/rewrite.scm 181 */
																			BgL_tmpz00_2179 = ((bool_t) 1);
																		}
																	BBOOL(BgL_tmpz00_2179);
																}
															}
														}
													}
												else
													{	/* Lalr/rewrite.scm 173 */
														BGl_errorz00zz__errorz00
															(BGl_symbol1675z00zz__lalr_rewritez00,
															BGl_string1694z00zz__lalr_rewritez00,
															BgL_tz00_1253);
													}
											}
									}
									{
										obj_t BgL_l1093z00_2196;

										BgL_l1093z00_2196 = CDR(BgL_l1093z00_1250);
										BgL_l1093z00_1250 = BgL_l1093z00_2196;
										goto BgL_zc3z04anonymousza31231ze3z87_1251;
									}
								}
							else
								{	/* Lalr/rewrite.scm 168 */
									((bool_t) 1);
								}
						}
						{
							obj_t BgL_lz00_1274;

							BgL_lz00_1274 = BgL_thezd2ruleszd2_1235;
						BgL_zc3z04anonymousza31253ze3z87_1275:
							if (PAIRP(BgL_lz00_1274))
								{	/* Lalr/rewrite.scm 190 */
									obj_t BgL_rulesz00_1277;

									BgL_rulesz00_1277 = CAR(BgL_lz00_1274);
									{	/* Lalr/rewrite.scm 191 */

										{	/* Lalr/rewrite.scm 192 */
											obj_t BgL_g1041z00_1279;

											BgL_g1041z00_1279 = CDR(((obj_t) BgL_rulesz00_1277));
											{
												obj_t BgL_rhszd2lzd2_1281;

												BgL_rhszd2lzd2_1281 = BgL_g1041z00_1279;
											BgL_zc3z04anonymousza31255ze3z87_1282:
												if (NULLP(BgL_rhszd2lzd2_1281))
													{	/* Lalr/rewrite.scm 195 */
														obj_t BgL_arg1268z00_1284;

														BgL_arg1268z00_1284 = CDR(((obj_t) BgL_lz00_1274));
														{
															obj_t BgL_lz00_2207;

															BgL_lz00_2207 = BgL_arg1268z00_1284;
															BgL_lz00_1274 = BgL_lz00_2207;
															goto BgL_zc3z04anonymousza31253ze3z87_1275;
														}
													}
												else
													{	/* Lalr/rewrite.scm 194 */
														if (PAIRP(BgL_rhszd2lzd2_1281))
															{	/* Lalr/rewrite.scm 197 */
																obj_t BgL_rhszf2actionzf2_1286;

																BgL_rhszf2actionzf2_1286 =
																	CAR(BgL_rhszd2lzd2_1281);
																BgL_nozd2ruleszd2_1233 =
																	(BgL_nozd2ruleszd2_1233 + 1L);
																if (PAIRP(BgL_rhszf2actionzf2_1286))
																	{	/* Lalr/rewrite.scm 200 */
																		obj_t BgL_actionz00_1288;

																		BgL_actionz00_1288 =
																			CDR(BgL_rhszf2actionzf2_1286);
																		if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_actionz00_1288))
																			{	/* Lalr/rewrite.scm 201 */
																				BFALSE;
																			}
																		else
																			{	/* Lalr/rewrite.scm 201 */
																				BGl_errorz00zz__errorz00
																					(BGl_symbol1675z00zz__lalr_rewritez00,
																					BGl_string1695z00zz__lalr_rewritez00,
																					BgL_rhszf2actionzf2_1286);
																			}
																		if (NULLP(BgL_actionz00_1288))
																			{	/* Lalr/rewrite.scm 203 */
																				SET_CDR(BgL_rhszf2actionzf2_1286,
																					BGl_list1696z00zz__lalr_rewritez00);
																			}
																		else
																			{	/* Lalr/rewrite.scm 203 */
																				BFALSE;
																			}
																		{	/* Lalr/rewrite.scm 205 */
																			obj_t BgL_g1042z00_1291;

																			BgL_g1042z00_1291 =
																				CAR(BgL_rhszf2actionzf2_1286);
																			{
																				obj_t BgL_rhsz00_1293;

																				BgL_rhsz00_1293 = BgL_g1042z00_1291;
																			BgL_zc3z04anonymousza31273ze3z87_1294:
																				if (NULLP(BgL_rhsz00_1293))
																					{	/* Lalr/rewrite.scm 207 */
																						{	/* Lalr/rewrite.scm 209 */
																							long BgL_b1097z00_1296;

																							{	/* Lalr/rewrite.scm 209 */

																								BgL_b1097z00_1296 =
																									(bgl_list_length(CAR(
																											((obj_t)
																												BgL_rhszf2actionzf2_1286)))
																									+ 1L);
																							}
																							BgL_nozd2itemszd2_1232 =
																								(BgL_nozd2itemszd2_1232 +
																								BgL_b1097z00_1296);
																						}
																						{	/* Lalr/rewrite.scm 210 */
																							obj_t BgL_arg1304z00_1302;

																							BgL_arg1304z00_1302 =
																								CDR(
																								((obj_t) BgL_rhszd2lzd2_1281));
																							{
																								obj_t BgL_rhszd2lzd2_2231;

																								BgL_rhszd2lzd2_2231 =
																									BgL_arg1304z00_1302;
																								BgL_rhszd2lzd2_1281 =
																									BgL_rhszd2lzd2_2231;
																								goto
																									BgL_zc3z04anonymousza31255ze3z87_1282;
																							}
																						}
																					}
																				else
																					{	/* Lalr/rewrite.scm 207 */
																						if (PAIRP(BgL_rhsz00_1293))
																							{	/* Lalr/rewrite.scm 212 */
																								obj_t BgL_symz00_1304;

																								BgL_symz00_1304 =
																									CAR(BgL_rhsz00_1293);
																								if (SYMBOLP(BgL_symz00_1304))
																									{	/* Lalr/rewrite.scm 214 */
																										obj_t BgL_symzf2varzf2_1306;

																										BgL_symzf2varzf2_1306 =
																											BGl_symbolzd2ze3symbolzf2bindingzc3zz__lalr_rewritez00
																											(BgL_symz00_1304);
																										{	/* Lalr/rewrite.scm 214 */
																											obj_t
																												BgL_thezd2symzd2_1307;
																											if (SYMBOLP
																												(BgL_symzf2varzf2_1306))
																												{	/* Lalr/rewrite.scm 215 */
																													BgL_thezd2symzd2_1307
																														=
																														BgL_symzf2varzf2_1306;
																												}
																											else
																												{	/* Lalr/rewrite.scm 215 */
																													BgL_thezd2symzd2_1307
																														=
																														CAR(((obj_t)
																															BgL_symzf2varzf2_1306));
																												}
																											{	/* Lalr/rewrite.scm 215 */

																												SET_CAR(BgL_rhsz00_1293,
																													BgL_symzf2varzf2_1306);
																												if (CBOOL
																													(BGl_getpropz00zz__r4_symbols_6_4z00
																														(BgL_thezd2symzd2_1307,
																															BGl_symbol1673z00zz__lalr_rewritez00)))
																													{	/* Lalr/rewrite.scm 104 */
																														BFALSE;
																													}
																												else
																													{	/* Lalr/rewrite.scm 104 */
																														BGl_errorz00zz__errorz00
																															(BGl_symbol1675z00zz__lalr_rewritez00,
																															BGl_string1697z00zz__lalr_rewritez00,
																															BgL_thezd2symzd2_1307);
																													}
																												{
																													obj_t BgL_rhsz00_2247;

																													BgL_rhsz00_2247 =
																														CDR
																														(BgL_rhsz00_1293);
																													BgL_rhsz00_1293 =
																														BgL_rhsz00_2247;
																													goto
																														BgL_zc3z04anonymousza31273ze3z87_1294;
																												}
																											}
																										}
																									}
																								else
																									{	/* Lalr/rewrite.scm 213 */
																										BGl_errorz00zz__errorz00
																											(BGl_symbol1675z00zz__lalr_rewritez00,
																											BGl_string1698z00zz__lalr_rewritez00,
																											BgL_rhsz00_1293);
																									}
																							}
																						else
																							{	/* Lalr/rewrite.scm 211 */
																								BGl_errorz00zz__errorz00
																									(BGl_symbol1675z00zz__lalr_rewritez00,
																									BGl_string1699z00zz__lalr_rewritez00,
																									BgL_rhsz00_1293);
																							}
																					}
																			}
																		}
																	}
																else
																	{	/* Lalr/rewrite.scm 199 */
																		BFALSE;
																	}
															}
														else
															{	/* Lalr/rewrite.scm 196 */
																BGl_errorz00zz__errorz00
																	(BGl_symbol1675z00zz__lalr_rewritez00,
																	BGl_string1685z00zz__lalr_rewritez00,
																	BgL_lz00_1274);
															}
													}
											}
										}
									}
								}
							else
								{	/* Lalr/rewrite.scm 189 */
									BFALSE;
								}
						}
						BGl_makezd2symzd2tablez00zz__lalr_rewritez00();
						{	/* Lalr/rewrite.scm 234 */
							obj_t BgL_startzd2symzd2_1313;

							{	/* Lalr/rewrite.scm 234 */
								obj_t BgL_pairz00_1811;

								BgL_pairz00_1811 = CAR(((obj_t) BgL_thezd2ruleszd2_1235));
								BgL_startzd2symzd2_1313 = CAR(BgL_pairz00_1811);
							}
							{	/* Lalr/rewrite.scm 236 */
								obj_t BgL_arg1309z00_1314;

								{	/* Lalr/rewrite.scm 236 */
									obj_t BgL_arg1310z00_1315;

									{	/* Lalr/rewrite.scm 236 */
										obj_t BgL_arg1311z00_1316;

										{	/* Lalr/rewrite.scm 236 */
											obj_t BgL_arg1312z00_1317;
											obj_t BgL_arg1314z00_1318;

											{	/* Lalr/rewrite.scm 236 */
												obj_t BgL_arg1315z00_1319;

												BgL_arg1315z00_1319 =
													MAKE_YOUNG_PAIR(BGl_symbol1679z00zz__lalr_rewritez00,
													BNIL);
												BgL_arg1312z00_1317 =
													MAKE_YOUNG_PAIR(BgL_startzd2symzd2_1313,
													BgL_arg1315z00_1319);
											}
											BgL_arg1314z00_1318 =
												MAKE_YOUNG_PAIR(BgL_startzd2symzd2_1313, BNIL);
											BgL_arg1311z00_1316 =
												MAKE_YOUNG_PAIR(BgL_arg1312z00_1317,
												BgL_arg1314z00_1318);
										}
										BgL_arg1310z00_1315 =
											MAKE_YOUNG_PAIR(BgL_arg1311z00_1316, BNIL);
									}
									BgL_arg1309z00_1314 =
										MAKE_YOUNG_PAIR(BGl_symbol1681z00zz__lalr_rewritez00,
										BgL_arg1310z00_1315);
								}
								BGl_grammarz00zz__lalr_globalz00 =
									MAKE_YOUNG_PAIR(BgL_arg1309z00_1314, BgL_thezd2ruleszd2_1235);
							}
						}
						{	/* Lalr/rewrite.scm 238 */
							long BgL_za71za7_1812;

							BgL_za71za7_1812 = BgL_nozd2ruleszd2_1233;
							BGl_nrulesz00zz__lalr_globalz00 = BINT((BgL_za71za7_1812 + 2L));
						}
						{	/* Lalr/rewrite.scm 239 */
							long BgL_za71za7_1813;

							BgL_za71za7_1813 = BgL_nozd2itemszd2_1232;
							BGl_nitemsz00zz__lalr_globalz00 = BINT((BgL_za71za7_1813 + 3L));
						}
						BGl_ntermsz00zz__lalr_globalz00 =
							BINT(BGl_za2maxzd2termza2zd2zz__lalr_rewritez00);
						BGl_nvarsz00zz__lalr_globalz00 =
							BINT(BGl_za2maxzd2ntza2zd2zz__lalr_rewritez00);
						{	/* Lalr/rewrite.scm 242 */
							bool_t BgL_test1754z00_2269;

							if (INTEGERP(BGl_ntermsz00zz__lalr_globalz00))
								{	/* Lalr/rewrite.scm 242 */
									BgL_test1754z00_2269 =
										INTEGERP(BGl_nvarsz00zz__lalr_globalz00);
								}
							else
								{	/* Lalr/rewrite.scm 242 */
									BgL_test1754z00_2269 = ((bool_t) 0);
								}
							if (BgL_test1754z00_2269)
								{	/* Lalr/rewrite.scm 242 */
									return (BGl_nsymsz00zz__lalr_globalz00 =
										ADDFX(BGl_ntermsz00zz__lalr_globalz00,
											BGl_nvarsz00zz__lalr_globalz00), BUNSPEC);
								}
							else
								{	/* Lalr/rewrite.scm 242 */
									return (BGl_nsymsz00zz__lalr_globalz00 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00
										(BGl_ntermsz00zz__lalr_globalz00,
											BGl_nvarsz00zz__lalr_globalz00), BUNSPEC);
								}
						}
					}
				}
			}
		}

	}



/* &rewrite-grammar! */
	obj_t BGl_z62rewritezd2grammarz12za2zz__lalr_rewritez00(obj_t BgL_envz00_2035,
		obj_t BgL_gramz00_2036)
	{
		{	/* Lalr/rewrite.scm 125 */
			return BGl_rewritezd2grammarz12zc0zz__lalr_rewritez00(BgL_gramz00_2036);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__lalr_rewritez00(void)
	{
		{	/* Lalr/rewrite.scm 11 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1700z00zz__lalr_rewritez00));
			BGl_modulezd2initializa7ationz75zz__lalr_globalz00(39277513L,
				BSTRING_TO_STRING(BGl_string1700z00zz__lalr_rewritez00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1700z00zz__lalr_rewritez00));
		}

	}

#ifdef __cplusplus
}
#endif
