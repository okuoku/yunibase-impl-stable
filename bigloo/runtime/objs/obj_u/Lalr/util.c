/*===========================================================================*/
/*   (Lalr/util.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Lalr/util.scm -indent -o objs/obj_u/Lalr/util.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___LALR_UTIL_TYPE_DEFINITIONS
#define BGL___LALR_UTIL_TYPE_DEFINITIONS
#endif													// BGL___LALR_UTIL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__lalr_utilz00 = BUNSPEC;
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__lalr_utilz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62sunionz62zz__lalr_utilz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_poszd2inzd2listz00zz__lalr_utilz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__lalr_utilz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__lalr_utilz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__lalr_utilz00(void);
	static obj_t BGl_objectzd2initzd2zz__lalr_utilz00(void);
	static obj_t BGl_z62filterz62zz__lalr_utilz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sunionz00zz__lalr_utilz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__lalr_utilz00(void);
	static obj_t BGl_z62poszd2inzd2listz62zz__lalr_utilz00(obj_t, obj_t, obj_t);
	static obj_t BGl_loopze70ze7zz__lalr_utilz00(obj_t, obj_t);
	static obj_t BGl_loopze71ze7zz__lalr_utilz00(obj_t, obj_t);
	static obj_t BGl_loopze72ze7zz__lalr_utilz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_filterz00zz__lalr_utilz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sinsertz00zz__lalr_utilz00(obj_t, obj_t);
	static obj_t BGl_z62sinsertz62zz__lalr_utilz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_filterzd2envzd2zz__lalr_utilz00,
		BgL_bgl_za762filterza762za7za7__1626z00, BGl_z62filterz62zz__lalr_utilz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1625z00zz__lalr_utilz00,
		BgL_bgl_string1625za700za7za7_1627za7, "__lalr_util", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sunionzd2envzd2zz__lalr_utilz00,
		BgL_bgl_za762sunionza762za7za7__1628z00, BGl_z62sunionz62zz__lalr_utilz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sinsertzd2envzd2zz__lalr_utilz00,
		BgL_bgl_za762sinsertza762za7za7_1629z00, BGl_z62sinsertz62zz__lalr_utilz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_poszd2inzd2listzd2envzd2zz__lalr_utilz00,
		BgL_bgl_za762posza7d2inza7d2li1630za7,
		BGl_z62poszd2inzd2listz62zz__lalr_utilz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__lalr_utilz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__lalr_utilz00(long
		BgL_checksumz00_1953, char *BgL_fromz00_1954)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__lalr_utilz00))
				{
					BGl_requirezd2initializa7ationz75zz__lalr_utilz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__lalr_utilz00();
					BGl_importedzd2moduleszd2initz00zz__lalr_utilz00();
					return BGl_methodzd2initzd2zz__lalr_utilz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__lalr_utilz00(void)
	{
		{	/* Lalr/util.scm 11 */
			return bgl_gc_roots_register();
		}

	}



/* pos-in-list */
	BGL_EXPORTED_DEF obj_t BGl_poszd2inzd2listz00zz__lalr_utilz00(obj_t
		BgL_xz00_3, obj_t BgL_lstz00_4)
	{
		{	/* Lalr/util.scm 51 */
			{
				obj_t BgL_lstz00_1693;
				long BgL_iz00_1694;

				BgL_lstz00_1693 = BgL_lstz00_4;
				BgL_iz00_1694 = 0L;
			BgL_loopz00_1692:
				if (PAIRP(BgL_lstz00_1693))
					{	/* Lalr/util.scm 53 */
						if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CAR(BgL_lstz00_1693),
								BgL_xz00_3))
							{	/* Lalr/util.scm 54 */
								return BINT(BgL_iz00_1694);
							}
						else
							{
								long BgL_iz00_1970;
								obj_t BgL_lstz00_1968;

								BgL_lstz00_1968 = CDR(BgL_lstz00_1693);
								BgL_iz00_1970 = (BgL_iz00_1694 + 1L);
								BgL_iz00_1694 = BgL_iz00_1970;
								BgL_lstz00_1693 = BgL_lstz00_1968;
								goto BgL_loopz00_1692;
							}
					}
				else
					{	/* Lalr/util.scm 53 */
						return BFALSE;
					}
			}
		}

	}



/* &pos-in-list */
	obj_t BGl_z62poszd2inzd2listz62zz__lalr_utilz00(obj_t BgL_envz00_1939,
		obj_t BgL_xz00_1940, obj_t BgL_lstz00_1941)
	{
		{	/* Lalr/util.scm 51 */
			return
				BGl_poszd2inzd2listz00zz__lalr_utilz00(BgL_xz00_1940, BgL_lstz00_1941);
		}

	}



/* sunion */
	BGL_EXPORTED_DEF obj_t BGl_sunionz00zz__lalr_utilz00(obj_t BgL_lst1z00_5,
		obj_t BgL_lst2z00_6)
	{
		{	/* Lalr/util.scm 57 */
			BGL_TAIL return
				BGl_loopze72ze7zz__lalr_utilz00(BgL_lst1z00_5, BgL_lst2z00_6);
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__lalr_utilz00(obj_t BgL_l1z00_1207,
		obj_t BgL_l2z00_1208)
	{
		{	/* Lalr/util.scm 58 */
		BGl_loopze72ze7zz__lalr_utilz00:
			if (NULLP(BgL_l1z00_1207))
				{	/* Lalr/util.scm 60 */
					return BgL_l2z00_1208;
				}
			else
				{	/* Lalr/util.scm 60 */
					if (NULLP(BgL_l2z00_1208))
						{	/* Lalr/util.scm 61 */
							return BgL_l1z00_1207;
						}
					else
						{	/* Lalr/util.scm 63 */
							obj_t BgL_xz00_1212;
							obj_t BgL_yz00_1213;

							BgL_xz00_1212 = CAR(((obj_t) BgL_l1z00_1207));
							BgL_yz00_1213 = CAR(((obj_t) BgL_l2z00_1208));
							if (((long) CINT(BgL_xz00_1212) > (long) CINT(BgL_yz00_1213)))
								{	/* Lalr/util.scm 66 */
									obj_t BgL_arg1201z00_1215;

									{	/* Lalr/util.scm 66 */
										obj_t BgL_arg1202z00_1216;

										BgL_arg1202z00_1216 = CDR(((obj_t) BgL_l2z00_1208));
										BgL_arg1201z00_1215 =
											BGl_loopze72ze7zz__lalr_utilz00(BgL_l1z00_1207,
											BgL_arg1202z00_1216);
									}
									return MAKE_YOUNG_PAIR(BgL_yz00_1213, BgL_arg1201z00_1215);
								}
							else
								{	/* Lalr/util.scm 65 */
									if (((long) CINT(BgL_xz00_1212) < (long) CINT(BgL_yz00_1213)))
										{	/* Lalr/util.scm 68 */
											obj_t BgL_arg1206z00_1218;

											{	/* Lalr/util.scm 68 */
												obj_t BgL_arg1208z00_1219;

												BgL_arg1208z00_1219 = CDR(((obj_t) BgL_l1z00_1207));
												BgL_arg1206z00_1218 =
													BGl_loopze72ze7zz__lalr_utilz00(BgL_arg1208z00_1219,
													BgL_l2z00_1208);
											}
											return
												MAKE_YOUNG_PAIR(BgL_xz00_1212, BgL_arg1206z00_1218);
										}
									else
										{	/* Lalr/util.scm 70 */
											obj_t BgL_arg1209z00_1220;

											BgL_arg1209z00_1220 = CDR(((obj_t) BgL_l1z00_1207));
											{
												obj_t BgL_l1z00_2000;

												BgL_l1z00_2000 = BgL_arg1209z00_1220;
												BgL_l1z00_1207 = BgL_l1z00_2000;
												goto BGl_loopze72ze7zz__lalr_utilz00;
											}
										}
								}
						}
				}
		}

	}



/* &sunion */
	obj_t BGl_z62sunionz62zz__lalr_utilz00(obj_t BgL_envz00_1942,
		obj_t BgL_lst1z00_1943, obj_t BgL_lst2z00_1944)
	{
		{	/* Lalr/util.scm 57 */
			return BGl_sunionz00zz__lalr_utilz00(BgL_lst1z00_1943, BgL_lst2z00_1944);
		}

	}



/* sinsert */
	BGL_EXPORTED_DEF obj_t BGl_sinsertz00zz__lalr_utilz00(obj_t BgL_elemz00_7,
		obj_t BgL_lstz00_8)
	{
		{	/* Lalr/util.scm 73 */
			BGL_TAIL return
				BGl_loopze71ze7zz__lalr_utilz00(BgL_elemz00_7, BgL_lstz00_8);
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__lalr_utilz00(obj_t BgL_elemz00_1952,
		obj_t BgL_l1z00_1223)
	{
		{	/* Lalr/util.scm 74 */
			if (NULLP(BgL_l1z00_1223))
				{	/* Lalr/util.scm 75 */
					return MAKE_YOUNG_PAIR(BgL_elemz00_1952, BgL_l1z00_1223);
				}
			else
				{	/* Lalr/util.scm 77 */
					obj_t BgL_xz00_1226;

					BgL_xz00_1226 = CAR(((obj_t) BgL_l1z00_1223));
					if (((long) CINT(BgL_elemz00_1952) < (long) CINT(BgL_xz00_1226)))
						{	/* Lalr/util.scm 78 */
							return MAKE_YOUNG_PAIR(BgL_elemz00_1952, BgL_l1z00_1223);
						}
					else
						{	/* Lalr/util.scm 78 */
							if (((long) CINT(BgL_elemz00_1952) > (long) CINT(BgL_xz00_1226)))
								{	/* Lalr/util.scm 81 */
									obj_t BgL_arg1215z00_1229;

									{	/* Lalr/util.scm 81 */
										obj_t BgL_arg1216z00_1230;

										BgL_arg1216z00_1230 = CDR(((obj_t) BgL_l1z00_1223));
										BgL_arg1215z00_1229 =
											BGl_loopze71ze7zz__lalr_utilz00(BgL_elemz00_1952,
											BgL_arg1216z00_1230);
									}
									return MAKE_YOUNG_PAIR(BgL_xz00_1226, BgL_arg1215z00_1229);
								}
							else
								{	/* Lalr/util.scm 80 */
									return BgL_l1z00_1223;
								}
						}
				}
		}

	}



/* &sinsert */
	obj_t BGl_z62sinsertz62zz__lalr_utilz00(obj_t BgL_envz00_1945,
		obj_t BgL_elemz00_1946, obj_t BgL_lstz00_1947)
	{
		{	/* Lalr/util.scm 73 */
			return BGl_sinsertz00zz__lalr_utilz00(BgL_elemz00_1946, BgL_lstz00_1947);
		}

	}



/* filter */
	BGL_EXPORTED_DEF obj_t BGl_filterz00zz__lalr_utilz00(obj_t BgL_pz00_9,
		obj_t BgL_lstz00_10)
	{
		{	/* Lalr/util.scm 85 */
			BGL_TAIL return
				BGl_loopze70ze7zz__lalr_utilz00(BgL_pz00_9, BgL_lstz00_10);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__lalr_utilz00(obj_t BgL_pz00_1951,
		obj_t BgL_lz00_1233)
	{
		{	/* Lalr/util.scm 86 */
		BGl_loopze70ze7zz__lalr_utilz00:
			if (NULLP(BgL_lz00_1233))
				{	/* Lalr/util.scm 87 */
					return BNIL;
				}
			else
				{	/* Lalr/util.scm 89 */
					obj_t BgL_xz00_1236;
					obj_t BgL_yz00_1237;

					BgL_xz00_1236 = CAR(((obj_t) BgL_lz00_1233));
					BgL_yz00_1237 = CDR(((obj_t) BgL_lz00_1233));
					if (CBOOL(BGL_PROCEDURE_CALL1(BgL_pz00_1951, BgL_xz00_1236)))
						{	/* Lalr/util.scm 90 */
							return
								MAKE_YOUNG_PAIR(BgL_xz00_1236,
								BGl_loopze70ze7zz__lalr_utilz00(BgL_pz00_1951, BgL_yz00_1237));
						}
					else
						{
							obj_t BgL_lz00_2037;

							BgL_lz00_2037 = BgL_yz00_1237;
							BgL_lz00_1233 = BgL_lz00_2037;
							goto BGl_loopze70ze7zz__lalr_utilz00;
						}
				}
		}

	}



/* &filter */
	obj_t BGl_z62filterz62zz__lalr_utilz00(obj_t BgL_envz00_1948,
		obj_t BgL_pz00_1949, obj_t BgL_lstz00_1950)
	{
		{	/* Lalr/util.scm 85 */
			return BGl_filterz00zz__lalr_utilz00(BgL_pz00_1949, BgL_lstz00_1950);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__lalr_utilz00(void)
	{
		{	/* Lalr/util.scm 11 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__lalr_utilz00(void)
	{
		{	/* Lalr/util.scm 11 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__lalr_utilz00(void)
	{
		{	/* Lalr/util.scm 11 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__lalr_utilz00(void)
	{
		{	/* Lalr/util.scm 11 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1625z00zz__lalr_utilz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1625z00zz__lalr_utilz00));
		}

	}

#ifdef __cplusplus
}
#endif
