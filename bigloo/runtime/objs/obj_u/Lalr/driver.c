/*===========================================================================*/
/*   (Lalr/driver.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Lalr/driver.scm -indent -o objs/obj_u/Lalr/driver.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___LALR_DRIVER_TYPE_DEFINITIONS
#define BGL___LALR_DRIVER_TYPE_DEFINITIONS
#endif													// BGL___LALR_DRIVER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static long BGl_za2maxzd2stackzd2siza7eza2za7zz__lalr_driverz00 = 0L;
	static obj_t BGl_requirezd2initializa7ationz75zz__lalr_driverz00 = BUNSPEC;
	static obj_t BGl_growzd2stackz12zc0zz__lalr_driverz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__lalr_driverz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t bgl_system_failure(int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl___makezd2parserzd2zz__lalr_driverz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__lalr_driverz00(void);
	extern int bgl_debug(void);
	static obj_t BGl_z62zc3z04anonymousza31195ze3ze5zz__lalr_driverz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__lalr_driverz00(void);
	static obj_t BGl_genericzd2initzd2zz__lalr_driverz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__lalr_driverz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__lalr_driverz00(void);
	static obj_t BGl_objectzd2initzd2zz__lalr_driverz00(void);
	static obj_t BGl_z62__makezd2parserzb0zz__lalr_driverz00(obj_t, obj_t, obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static long BGl_za2stackzd2siza7ezd2incrementza2za7zz__lalr_driverz00 = 0L;
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	extern obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__lalr_driverz00(void);
	static obj_t BGl_symbol1632z00zz__lalr_driverz00 = BUNSPEC;
	static obj_t BGl_symbol1635z00zz__lalr_driverz00 = BUNSPEC;
	static obj_t BGl_symbol1640z00zz__lalr_driverz00 = BUNSPEC;
	static obj_t BGl_symbol1642z00zz__lalr_driverz00 = BUNSPEC;
	static obj_t BGl_symbol1644z00zz__lalr_driverz00 = BUNSPEC;
	extern obj_t make_string(long, unsigned char);
	extern obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bgl_close_output_port(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1633z00zz__lalr_driverz00,
		BgL_bgl_string1633za700za7za7_1649za7, "parser", 6);
	      DEFINE_STRING(BGl_string1634z00zz__lalr_driverz00,
		BgL_bgl_string1634za700za7za7_1650za7, "Illegal `#f' token", 18);
	      DEFINE_STRING(BGl_string1636z00zz__lalr_driverz00,
		BgL_bgl_string1636za700za7za7_1651za7, "*eoi*", 5);
	      DEFINE_STRING(BGl_string1637z00zz__lalr_driverz00,
		BgL_bgl_string1637za700za7za7_1652za7, "LALR TRACE: input=", 18);
	      DEFINE_STRING(BGl_string1638z00zz__lalr_driverz00,
		BgL_bgl_string1638za700za7za7_1653za7, " state=", 7);
	      DEFINE_STRING(BGl_string1639z00zz__lalr_driverz00,
		BgL_bgl_string1639za700za7za7_1654za7, " sp=", 4);
	      DEFINE_STRING(BGl_string1641z00zz__lalr_driverz00,
		BgL_bgl_string1641za700za7za7_1655za7, "accept", 6);
	      DEFINE_STRING(BGl_string1643z00zz__lalr_driverz00,
		BgL_bgl_string1643za700za7za7_1656za7, "*error*", 7);
	      DEFINE_STRING(BGl_string1645z00zz__lalr_driverz00,
		BgL_bgl_string1645za700za7za7_1657za7, "error", 5);
	      DEFINE_STRING(BGl_string1646z00zz__lalr_driverz00,
		BgL_bgl_string1646za700za7za7_1658za7, "parse error (unexpected token `",
		31);
	      DEFINE_STRING(BGl_string1647z00zz__lalr_driverz00,
		BgL_bgl_string1647za700za7za7_1659za7, "')", 2);
	      DEFINE_STRING(BGl_string1648z00zz__lalr_driverz00,
		BgL_bgl_string1648za700za7za7_1660za7, "__lalr_driver", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl___makezd2parserzd2envz00zz__lalr_driverz00,
		BgL_bgl_za762__makeza7d2pars1661z00,
		BGl_z62__makezd2parserzb0zz__lalr_driverz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__lalr_driverz00));
		     ADD_ROOT((void *) (&BGl_symbol1632z00zz__lalr_driverz00));
		     ADD_ROOT((void *) (&BGl_symbol1635z00zz__lalr_driverz00));
		     ADD_ROOT((void *) (&BGl_symbol1640z00zz__lalr_driverz00));
		     ADD_ROOT((void *) (&BGl_symbol1642z00zz__lalr_driverz00));
		     ADD_ROOT((void *) (&BGl_symbol1644z00zz__lalr_driverz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__lalr_driverz00(long
		BgL_checksumz00_2055, char *BgL_fromz00_2056)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__lalr_driverz00))
				{
					BGl_requirezd2initializa7ationz75zz__lalr_driverz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__lalr_driverz00();
					BGl_cnstzd2initzd2zz__lalr_driverz00();
					BGl_importedzd2moduleszd2initz00zz__lalr_driverz00();
					return BGl_toplevelzd2initzd2zz__lalr_driverz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			BGl_symbol1632z00zz__lalr_driverz00 =
				bstring_to_symbol(BGl_string1633z00zz__lalr_driverz00);
			BGl_symbol1635z00zz__lalr_driverz00 =
				bstring_to_symbol(BGl_string1636z00zz__lalr_driverz00);
			BGl_symbol1640z00zz__lalr_driverz00 =
				bstring_to_symbol(BGl_string1641z00zz__lalr_driverz00);
			BGl_symbol1642z00zz__lalr_driverz00 =
				bstring_to_symbol(BGl_string1643z00zz__lalr_driverz00);
			return (BGl_symbol1644z00zz__lalr_driverz00 =
				bstring_to_symbol(BGl_string1645z00zz__lalr_driverz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			BGl_za2maxzd2stackzd2siza7eza2za7zz__lalr_driverz00 = 500L;
			return (BGl_za2stackzd2siza7ezd2incrementza2za7zz__lalr_driverz00 =
				200L, BUNSPEC);
		}

	}



/* grow-stack! */
	obj_t BGl_growzd2stackz12zc0zz__lalr_driverz00(obj_t BgL_vz00_3)
	{
		{	/* Lalr/driver.scm 52 */
			{	/* Lalr/driver.scm 53 */
				obj_t BgL_v2z00_1191;

				{	/* Lalr/driver.scm 54 */
					long BgL_arg1194z00_1199;

					BgL_arg1194z00_1199 = (VECTOR_LENGTH(BgL_vz00_3) + 200L);
					BgL_v2z00_1191 = make_vector(BgL_arg1194z00_1199, BINT(0L));
				}
				{	/* Lalr/driver.scm 54 */

					{
						long BgL_iz00_1704;

						BgL_iz00_1704 = 0L;
					BgL_loopz00_1703:
						if ((BgL_iz00_1704 < VECTOR_LENGTH(BgL_vz00_3)))
							{	/* Lalr/driver.scm 56 */
								VECTOR_SET(BgL_v2z00_1191, BgL_iz00_1704,
									VECTOR_REF(BgL_vz00_3, BgL_iz00_1704));
								{
									long BgL_iz00_2079;

									BgL_iz00_2079 = (BgL_iz00_1704 + 1L);
									BgL_iz00_1704 = BgL_iz00_2079;
									goto BgL_loopz00_1703;
								}
							}
						else
							{	/* Lalr/driver.scm 56 */
								return BgL_v2z00_1191;
							}
					}
				}
			}
		}

	}



/* __make-parser */
	BGL_EXPORTED_DEF obj_t BGl___makezd2parserzd2zz__lalr_driverz00(obj_t
		BgL_actionzd2tablezd2_4, obj_t BgL_reductionzd2functionzd2_5)
	{
		{	/* Lalr/driver.scm 62 */
			{	/* Lalr/driver.scm 64 */
				obj_t BgL_zc3z04anonymousza31195ze3z87_1998;

				BgL_zc3z04anonymousza31195ze3z87_1998 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31195ze3ze5zz__lalr_driverz00, (int) (3L),
					(int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31195ze3z87_1998, (int) (0L),
					BgL_actionzd2tablezd2_4);
				PROCEDURE_SET(BgL_zc3z04anonymousza31195ze3z87_1998, (int) (1L),
					BgL_reductionzd2functionzd2_5);
				return BgL_zc3z04anonymousza31195ze3z87_1998;
			}
		}

	}



/* &__make-parser */
	obj_t BGl_z62__makezd2parserzb0zz__lalr_driverz00(obj_t BgL_envz00_1999,
		obj_t BgL_actionzd2tablezd2_2000, obj_t BgL_reductionzd2functionzd2_2001)
	{
		{	/* Lalr/driver.scm 62 */
			return
				BGl___makezd2parserzd2zz__lalr_driverz00(BgL_actionzd2tablezd2_2000,
				BgL_reductionzd2functionzd2_2001);
		}

	}



/* &<@anonymous:1195> */
	obj_t BGl_z62zc3z04anonymousza31195ze3ze5zz__lalr_driverz00(obj_t
		BgL_envz00_2002, obj_t BgL_rgcz00_2005, obj_t BgL_inputzd2portzd2_2006,
		obj_t BgL_iszd2eofzf3z21_2007)
	{
		{	/* Lalr/driver.scm 64 */
			{	/* Lalr/driver.scm 72 */
				obj_t BgL_actionzd2tablezd2_2003;
				obj_t BgL_reductionzd2functionzd2_2004;

				BgL_actionzd2tablezd2_2003 = PROCEDURE_REF(BgL_envz00_2002, (int) (0L));
				BgL_reductionzd2functionzd2_2004 =
					PROCEDURE_REF(BgL_envz00_2002, (int) (1L));
				{	/* Lalr/driver.scm 72 */
					obj_t BgL_stackz00_2013;
					obj_t BgL_statez00_2014;
					obj_t BgL_inputz00_2015;
					obj_t BgL_inz00_2016;
					obj_t BgL_attrz00_2017;
					obj_t BgL_actsz00_2018;
					obj_t BgL_actz00_2019;
					bool_t BgL_eofzf3zf3_2020;
					bool_t BgL_debugz00_2021;

					BgL_stackz00_2013 = make_vector(500L, BINT(0L));
					BgL_statez00_2014 = BFALSE;
					BgL_inputz00_2015 = BFALSE;
					BgL_inz00_2016 = BFALSE;
					BgL_attrz00_2017 = BFALSE;
					BgL_actsz00_2018 = BFALSE;
					BgL_actz00_2019 = BFALSE;
					BgL_eofzf3zf3_2020 = ((bool_t) 0);
					{	/* Lalr/driver.scm 80 */
						int BgL_arg1234z00_2022;

						BgL_arg1234z00_2022 = bgl_debug();
						BgL_debugz00_2021 = ((long) (BgL_arg1234z00_2022) >= 100L);
					}
					{
						obj_t BgL_spz00_2024;

						BgL_spz00_2024 = BINT(0L);
					BgL_loopz00_2023:
						BgL_statez00_2014 =
							VECTOR_REF(BgL_stackz00_2013, (long) CINT(BgL_spz00_2024));
						{	/* Lalr/driver.scm 84 */
							long BgL_kz00_2025;

							BgL_kz00_2025 = (long) CINT(BgL_statez00_2014);
							BgL_actsz00_2018 =
								VECTOR_REF(((obj_t) BgL_actionzd2tablezd2_2003), BgL_kz00_2025);
						}
						if (NULLP(CDR(BgL_actsz00_2018)))
							{	/* Lalr/driver.scm 87 */
								obj_t BgL_pairz00_2026;

								BgL_pairz00_2026 = BgL_actsz00_2018;
								BgL_actz00_2019 = CDR(CAR(BgL_pairz00_2026));
							}
						else
							{	/* Lalr/driver.scm 86 */
								if (CBOOL(BgL_inputz00_2015))
									{	/* Lalr/driver.scm 89 */
										BFALSE;
									}
								else
									{	/* Lalr/driver.scm 89 */
										BgL_inputz00_2015 =
											BGL_PROCEDURE_CALL1(BgL_rgcz00_2005,
											BgL_inputzd2portzd2_2006);
									}
								if (CBOOL(BgL_inputz00_2015))
									{	/* Lalr/driver.scm 91 */
										((bool_t) 0);
									}
								else
									{	/* Lalr/driver.scm 91 */
										bgl_system_failure(BGL_IO_PARSE_ERROR,
											BGl_symbol1632z00zz__lalr_driverz00,
											BGl_string1634z00zz__lalr_driverz00, BFALSE);
									}
								if (CBOOL(BGL_PROCEDURE_CALL1(BgL_iszd2eofzf3z21_2007,
											BgL_inputz00_2015)))
									{	/* Lalr/driver.scm 97 */
										BgL_inz00_2016 = BGl_symbol1635z00zz__lalr_driverz00;
										BgL_attrz00_2017 = BFALSE;
										BgL_eofzf3zf3_2020 = ((bool_t) 1);
									}
								else
									{	/* Lalr/driver.scm 97 */
										if (PAIRP(BgL_inputz00_2015))
											{	/* Lalr/driver.scm 101 */
												BgL_inz00_2016 = CAR(BgL_inputz00_2015);
												BgL_attrz00_2017 = CDR(BgL_inputz00_2015);
											}
										else
											{	/* Lalr/driver.scm 101 */
												BgL_inz00_2016 = BgL_inputz00_2015;
												BgL_attrz00_2017 = BFALSE;
											}
									}
								{	/* Lalr/driver.scm 108 */
									obj_t BgL_xz00_2027;
									obj_t BgL_lz00_2028;

									BgL_xz00_2027 = BgL_inz00_2016;
									BgL_lz00_2028 = BgL_actsz00_2018;
									{	/* Lalr/driver.scm 67 */
										obj_t BgL_yz00_2029;

										BgL_yz00_2029 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_xz00_2027,
											BgL_lz00_2028);
										if (CBOOL(BgL_yz00_2029))
											{	/* Lalr/driver.scm 68 */
												BgL_actz00_2019 = CDR(((obj_t) BgL_yz00_2029));
											}
										else
											{	/* Lalr/driver.scm 70 */
												obj_t BgL_pairz00_2030;

												BgL_pairz00_2030 = CAR(((obj_t) BgL_lz00_2028));
												BgL_actz00_2019 = CDR(BgL_pairz00_2030);
											}
									}
								}
							}
						if (BgL_debugz00_2021)
							{	/* Lalr/driver.scm 110 */
								{	/* Lalr/driver.scm 111 */
									obj_t BgL_arg1202z00_2031;

									{	/* Lalr/driver.scm 111 */
										obj_t BgL_tmpz00_2136;

										BgL_tmpz00_2136 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1202z00_2031 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2136);
									}
									bgl_display_string(BGl_string1637z00zz__lalr_driverz00,
										BgL_arg1202z00_2031);
								}
								{	/* Lalr/driver.scm 112 */
									obj_t BgL_arg1203z00_2032;

									{	/* Lalr/driver.scm 112 */
										obj_t BgL_tmpz00_2140;

										BgL_tmpz00_2140 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1203z00_2032 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2140);
									}
									{	/* Lalr/driver.scm 112 */
										obj_t BgL_list1204z00_2033;

										BgL_list1204z00_2033 =
											MAKE_YOUNG_PAIR(BgL_arg1203z00_2032, BNIL);
										BGl_writez00zz__r4_output_6_10_3z00(BgL_inz00_2016,
											BgL_list1204z00_2033);
									}
								}
								{	/* Lalr/driver.scm 113 */
									obj_t BgL_arg1206z00_2034;

									{	/* Lalr/driver.scm 113 */
										obj_t BgL_tmpz00_2145;

										BgL_tmpz00_2145 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1206z00_2034 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2145);
									}
									bgl_display_string(BGl_string1638z00zz__lalr_driverz00,
										BgL_arg1206z00_2034);
								}
								{	/* Lalr/driver.scm 114 */
									obj_t BgL_arg1208z00_2035;

									{	/* Lalr/driver.scm 114 */
										obj_t BgL_tmpz00_2149;

										BgL_tmpz00_2149 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1208z00_2035 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2149);
									}
									{	/* Lalr/driver.scm 114 */
										obj_t BgL_list1209z00_2036;

										BgL_list1209z00_2036 =
											MAKE_YOUNG_PAIR(BgL_arg1208z00_2035, BNIL);
										BGl_writez00zz__r4_output_6_10_3z00(BgL_statez00_2014,
											BgL_list1209z00_2036);
									}
								}
								{	/* Lalr/driver.scm 115 */
									obj_t BgL_arg1210z00_2037;

									{	/* Lalr/driver.scm 115 */
										obj_t BgL_tmpz00_2154;

										BgL_tmpz00_2154 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1210z00_2037 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2154);
									}
									bgl_display_string(BGl_string1639z00zz__lalr_driverz00,
										BgL_arg1210z00_2037);
								}
								{	/* Lalr/driver.scm 116 */
									obj_t BgL_arg1212z00_2038;

									{	/* Lalr/driver.scm 116 */
										obj_t BgL_tmpz00_2158;

										BgL_tmpz00_2158 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1212z00_2038 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2158);
									}
									{	/* Lalr/driver.scm 116 */
										obj_t BgL_list1213z00_2039;

										BgL_list1213z00_2039 =
											MAKE_YOUNG_PAIR(BgL_arg1212z00_2038, BNIL);
										BGl_writez00zz__r4_output_6_10_3z00(BgL_spz00_2024,
											BgL_list1213z00_2039);
									}
								}
								{	/* Lalr/driver.scm 117 */
									obj_t BgL_arg1215z00_2040;

									{	/* Lalr/driver.scm 117 */
										obj_t BgL_tmpz00_2163;

										BgL_tmpz00_2163 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1215z00_2040 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2163);
									}
									bgl_display_char(((unsigned char) 10), BgL_arg1215z00_2040);
							}}
						else
							{	/* Lalr/driver.scm 110 */
								BFALSE;
							}
						if ((BgL_actz00_2019 == BGl_symbol1640z00zz__lalr_driverz00))
							{	/* Lalr/driver.scm 122 */
								return VECTOR_REF(BgL_stackz00_2013, 1L);
							}
						else
							{	/* Lalr/driver.scm 126 */
								bool_t BgL_test1672z00_2170;

								if ((BgL_actz00_2019 == BGl_symbol1642z00zz__lalr_driverz00))
									{	/* Lalr/driver.scm 126 */
										BgL_test1672z00_2170 = ((bool_t) 1);
									}
								else
									{	/* Lalr/driver.scm 126 */
										BgL_test1672z00_2170 =
											(BgL_actz00_2019 == BGl_symbol1644z00zz__lalr_driverz00);
									}
								if (BgL_test1672z00_2170)
									{	/* Lalr/driver.scm 127 */
										obj_t BgL_msgz00_2041;

										{	/* Lalr/driver.scm 130 */
											obj_t BgL_arg1218z00_2042;

											if (SYMBOLP(BgL_inz00_2016))
												{	/* Lalr/driver.scm 131 */
													obj_t BgL_symbolz00_2043;

													BgL_symbolz00_2043 = BgL_inz00_2016;
													{	/* Lalr/driver.scm 131 */
														obj_t BgL_arg1425z00_2044;

														BgL_arg1425z00_2044 =
															SYMBOL_TO_STRING(BgL_symbolz00_2043);
														BgL_arg1218z00_2042 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1425z00_2044);
													}
												}
											else
												{	/* Lalr/driver.scm 130 */
													if (CHARP(BgL_inz00_2016))
														{	/* Lalr/driver.scm 133 */
															obj_t BgL_charz00_2045;

															BgL_charz00_2045 = BgL_inz00_2016;
															BgL_arg1218z00_2042 =
																make_string(1L, CCHAR(BgL_charz00_2045));
														}
													else
														{	/* Lalr/driver.scm 135 */
															obj_t BgL_portz00_2046;

															{	/* Lalr/driver.scm 135 */

																{	/* Lalr/driver.scm 135 */

																	BgL_portz00_2046 =
																		BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00
																		(BTRUE);
																}
															}
															{	/* Lalr/driver.scm 136 */
																obj_t BgL_list1221z00_2047;

																BgL_list1221z00_2047 =
																	MAKE_YOUNG_PAIR(BgL_portz00_2046, BNIL);
																BGl_writez00zz__r4_output_6_10_3z00
																	(BgL_inz00_2016, BgL_list1221z00_2047);
															}
															BgL_arg1218z00_2042 =
																bgl_close_output_port(BgL_portz00_2046);
														}
												}
											BgL_msgz00_2041 =
												string_append_3(BGl_string1646z00zz__lalr_driverz00,
												BgL_arg1218z00_2042,
												BGl_string1647z00zz__lalr_driverz00);
										}
										return
											bgl_system_failure(BGL_IO_PARSE_ERROR,
											BGl_string1633z00zz__lalr_driverz00, BgL_msgz00_2041,
											BgL_inputz00_2015);
									}
								else
									{	/* Lalr/driver.scm 126 */
										if (((long) CINT(BgL_actz00_2019) >= 0L))
											{	/* Lalr/driver.scm 142 */
												{	/* Lalr/driver.scm 143 */
													bool_t BgL_test1677z00_2191;

													{	/* Lalr/driver.scm 143 */
														long BgL_arg1227z00_2048;

														{	/* Lalr/driver.scm 143 */
															long BgL_arg1228z00_2049;

															BgL_arg1228z00_2049 =
																VECTOR_LENGTH(BgL_stackz00_2013);
															BgL_arg1227z00_2048 = (BgL_arg1228z00_2049 - 4L);
														}
														BgL_test1677z00_2191 =
															(
															(long) CINT(BgL_spz00_2024) >=
															BgL_arg1227z00_2048);
													}
													if (BgL_test1677z00_2191)
														{	/* Lalr/driver.scm 143 */
															BgL_stackz00_2013 =
																BGl_growzd2stackz12zc0zz__lalr_driverz00
																(BgL_stackz00_2013);
														}
													else
														{	/* Lalr/driver.scm 143 */
															BFALSE;
														}
												}
												VECTOR_SET(BgL_stackz00_2013,
													((long) CINT(BgL_spz00_2024) + 1L), BgL_attrz00_2017);
												VECTOR_SET(BgL_stackz00_2013,
													((long) CINT(BgL_spz00_2024) + 2L), BgL_actz00_2019);
												if (BgL_eofzf3zf3_2020)
													{	/* Lalr/driver.scm 147 */
														BFALSE;
													}
												else
													{	/* Lalr/driver.scm 147 */
														BgL_inputz00_2015 = BFALSE;
													}
												{	/* Lalr/driver.scm 149 */
													long BgL_arg1231z00_2052;

													BgL_arg1231z00_2052 =
														((long) CINT(BgL_spz00_2024) + 2L);
													{
														obj_t BgL_spz00_2206;

														BgL_spz00_2206 = BINT(BgL_arg1231z00_2052);
														BgL_spz00_2024 = BgL_spz00_2206;
														goto BgL_loopz00_2023;
													}
												}
											}
										else
											{	/* Lalr/driver.scm 153 */
												obj_t BgL_arg1232z00_2053;

												{	/* Lalr/driver.scm 153 */
													long BgL_arg1233z00_2054;

													BgL_arg1233z00_2054 =
														NEG((long) CINT(BgL_actz00_2019));
													BgL_arg1232z00_2053 =
														BGL_PROCEDURE_CALL3
														(BgL_reductionzd2functionzd2_2004,
														BINT(BgL_arg1233z00_2054), BgL_stackz00_2013,
														BgL_spz00_2024);
												}
												{
													obj_t BgL_spz00_2217;

													BgL_spz00_2217 = BgL_arg1232z00_2053;
													BgL_spz00_2024 = BgL_spz00_2217;
													goto BgL_loopz00_2023;
												}
											}
									}
							}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__lalr_driverz00(void)
	{
		{	/* Lalr/driver.scm 11 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1648z00zz__lalr_driverz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1648z00zz__lalr_driverz00));
		}

	}

#ifdef __cplusplus
}
#endif
