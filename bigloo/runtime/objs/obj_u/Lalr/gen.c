/*===========================================================================*/
/*   (Lalr/gen.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Lalr/gen.scm -indent -o objs/obj_u/Lalr/gen.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___LALR_GEN_TYPE_DEFINITIONS
#define BGL___LALR_GEN_TYPE_DEFINITIONS
#endif													// BGL___LALR_GEN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_genzd2actionzd2tablez00zz__lalr_genz00(void);
	extern obj_t BGl_za2symvza2z00zz__lalr_rewritez00;
	static obj_t BGl_symbol1832z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1835z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1838z00zz__lalr_genz00 = BUNSPEC;
	extern obj_t BGl_nvarsz00zz__lalr_globalz00;
	static obj_t BGl_list1807z00zz__lalr_genz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1841z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1810z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1845z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1813z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1848z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1851z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1855z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1857z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1824z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1859z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1827z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1828z00zz__lalr_genz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__lalr_genz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_rewritez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_globalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol1861z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1863z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1831z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1865z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1867z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1834z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1869z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_reductionsze70ze7zz__lalr_genz00(void);
	static obj_t BGl_list1837z00zz__lalr_genz00 = BUNSPEC;
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_genzd2lalrzd2codez00zz__lalr_genz00(void);
	static obj_t BGl_loopzd2gze70z35zz__lalr_genz00(long);
	static obj_t BGl_symbol1871z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1873z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1840z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1875z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1843z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1877z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1844z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1879z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1847z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1850z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1853z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_list1854z00zz__lalr_genz00 = BUNSPEC;
	extern obj_t BGl_shiftzd2tablezd2zz__lalr_globalz00;
	static obj_t BGl_cnstzd2initzd2zz__lalr_genz00(void);
	static obj_t BGl_genericzd2initzd2zz__lalr_genz00(void);
	extern long bgl_list_length(obj_t);
	extern obj_t BGl_acceszd2symbolzd2zz__lalr_globalz00;
	extern obj_t BGl_nstatesz00zz__lalr_globalz00;
	static obj_t BGl_loopzd2pze70z35zz__lalr_genz00(obj_t, obj_t, obj_t, long);
	static obj_t BGl_importedzd2moduleszd2initz00zz__lalr_genz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__lalr_genz00(void);
	static obj_t BGl_objectzd2initzd2zz__lalr_genz00(void);
	static obj_t BGl_genzd2reductionzd2tablez00zz__lalr_genz00(void);
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_loopzd2aze70z35zz__lalr_genz00(long);
	static obj_t BGl_gotosze70ze7zz__lalr_genz00(void);
	static obj_t BGl_z62genzd2lalrzd2codez62zz__lalr_genz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_methodzd2initzd2zz__lalr_genz00(void);
	extern obj_t BGl_grammarz00zz__lalr_globalz00;
	static obj_t BGl_loopze70ze7zz__lalr_genz00(long, obj_t);
	static obj_t BGl_loopze71ze7zz__lalr_genz00(obj_t);
	static obj_t BGl_loopze72ze7zz__lalr_genz00(obj_t);
	static obj_t BGl_genzd2gotozd2tablez00zz__lalr_genz00(void);
	static obj_t BGl_symbol1808z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_actionsze70ze7zz__lalr_genz00(void);
	static obj_t BGl_symbol1811z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_loopzd2lze70z35zz__lalr_genz00(obj_t, long);
	static obj_t BGl_symbol1814z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1816z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1818z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1820z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1822z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1825z00zz__lalr_genz00 = BUNSPEC;
	static obj_t BGl_symbol1829z00zz__lalr_genz00 = BUNSPEC;
	extern obj_t BGl_actionzd2tablezd2zz__lalr_globalz00;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1870z00zz__lalr_genz00,
		BgL_bgl_string1870za700za7za7_1882za7, "__stack", 7);
	      DEFINE_STRING(BGl_string1872z00zz__lalr_genz00,
		BgL_bgl_string1872za700za7za7_1883za7, "n", 1);
	      DEFINE_STRING(BGl_string1874z00zz__lalr_genz00,
		BgL_bgl_string1874za700za7za7_1884za7, "case", 4);
	      DEFINE_STRING(BGl_string1876z00zz__lalr_genz00,
		BgL_bgl_string1876za700za7za7_1885za7, "vector-ref-ur", 13);
	      DEFINE_STRING(BGl_string1878z00zz__lalr_genz00,
		BgL_bgl_string1878za700za7za7_1886za7, "*start*", 7);
	      DEFINE_STRING(BGl_string1880z00zz__lalr_genz00,
		BgL_bgl_string1880za700za7za7_1887za7, "let", 3);
	      DEFINE_STRING(BGl_string1881z00zz__lalr_genz00,
		BgL_bgl_string1881za700za7za7_1888za7, "__lalr_gen", 10);
	      DEFINE_STRING(BGl_string1809z00zz__lalr_genz00,
		BgL_bgl_string1809za700za7za7_1889za7, "__push", 6);
	      DEFINE_STRING(BGl_string1812z00zz__lalr_genz00,
		BgL_bgl_string1812za700za7za7_1890za7, "lambda", 6);
	      DEFINE_STRING(BGl_string1815z00zz__lalr_genz00,
		BgL_bgl_string1815za700za7za7_1891za7, "stack", 5);
	      DEFINE_STRING(BGl_string1817z00zz__lalr_genz00,
		BgL_bgl_string1817za700za7za7_1892za7, "sp", 2);
	      DEFINE_STRING(BGl_string1819z00zz__lalr_genz00,
		BgL_bgl_string1819za700za7za7_1893za7, "new-cat", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_genzd2lalrzd2codezd2envzd2zz__lalr_genz00,
		BgL_bgl_za762genza7d2lalrza7d21894za7,
		BGl_z62genzd2lalrzd2codez62zz__lalr_genz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1821z00zz__lalr_genz00,
		BgL_bgl_string1821za700za7za7_1895za7, "goto-table", 10);
	      DEFINE_STRING(BGl_string1823z00zz__lalr_genz00,
		BgL_bgl_string1823za700za7za7_1896za7, "lval", 4);
	      DEFINE_STRING(BGl_string1826z00zz__lalr_genz00,
		BgL_bgl_string1826za700za7za7_1897za7, "let*", 4);
	      DEFINE_STRING(BGl_string1830z00zz__lalr_genz00,
		BgL_bgl_string1830za700za7za7_1898za7, "state", 5);
	      DEFINE_STRING(BGl_string1833z00zz__lalr_genz00,
		BgL_bgl_string1833za700za7za7_1899za7, "vector-ref", 10);
	      DEFINE_STRING(BGl_string1836z00zz__lalr_genz00,
		BgL_bgl_string1836za700za7za7_1900za7, "new-state", 9);
	      DEFINE_STRING(BGl_string1839z00zz__lalr_genz00,
		BgL_bgl_string1839za700za7za7_1901za7, "cdr", 3);
	      DEFINE_STRING(BGl_string1842z00zz__lalr_genz00,
		BgL_bgl_string1842za700za7za7_1902za7, "assq", 4);
	      DEFINE_STRING(BGl_string1846z00zz__lalr_genz00,
		BgL_bgl_string1846za700za7za7_1903za7, "new-sp", 6);
	      DEFINE_STRING(BGl_string1849z00zz__lalr_genz00,
		BgL_bgl_string1849za700za7za7_1904za7, "+fx", 3);
	      DEFINE_STRING(BGl_string1852z00zz__lalr_genz00,
		BgL_bgl_string1852za700za7za7_1905za7, "vector-set!", 11);
	      DEFINE_STRING(BGl_string1856z00zz__lalr_genz00,
		BgL_bgl_string1856za700za7za7_1906za7, "-fx", 3);
	      DEFINE_STRING(BGl_string1858z00zz__lalr_genz00,
		BgL_bgl_string1858za700za7za7_1907za7, "__reduce", 8);
	      DEFINE_STRING(BGl_string1860z00zz__lalr_genz00,
		BgL_bgl_string1860za700za7za7_1908za7, "__action-table", 14);
	      DEFINE_STRING(BGl_string1862z00zz__lalr_genz00,
		BgL_bgl_string1862za700za7za7_1909za7, "__make-parser", 13);
	      DEFINE_STRING(BGl_string1864z00zz__lalr_genz00,
		BgL_bgl_string1864za700za7za7_1910za7, "quote", 5);
	      DEFINE_STRING(BGl_string1866z00zz__lalr_genz00,
		BgL_bgl_string1866za700za7za7_1911za7, "__goto-table", 12);
	      DEFINE_STRING(BGl_string1868z00zz__lalr_genz00,
		BgL_bgl_string1868za700za7za7_1912za7, "__sp", 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1832z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1835z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1838z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1807z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1841z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1810z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1845z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1813z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1848z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1851z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1855z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1857z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1824z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1859z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1827z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1828z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1861z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1863z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1831z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1865z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1867z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1834z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1869z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1837z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1871z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1873z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1840z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1875z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1843z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1877z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1844z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1879z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1847z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1850z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1853z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_list1854z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1808z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1811z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1814z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1816z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1818z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1820z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1822z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1825z00zz__lalr_genz00));
		     ADD_ROOT((void *) (&BGl_symbol1829z00zz__lalr_genz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__lalr_genz00(long
		BgL_checksumz00_2103, char *BgL_fromz00_2104)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__lalr_genz00))
				{
					BGl_requirezd2initializa7ationz75zz__lalr_genz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__lalr_genz00();
					BGl_cnstzd2initzd2zz__lalr_genz00();
					BGl_importedzd2moduleszd2initz00zz__lalr_genz00();
					return BGl_methodzd2initzd2zz__lalr_genz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 11 */
			BGl_symbol1808z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1809z00zz__lalr_genz00);
			BGl_symbol1811z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1812z00zz__lalr_genz00);
			BGl_symbol1814z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1815z00zz__lalr_genz00);
			BGl_symbol1816z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1817z00zz__lalr_genz00);
			BGl_symbol1818z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1819z00zz__lalr_genz00);
			BGl_symbol1820z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1821z00zz__lalr_genz00);
			BGl_symbol1822z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1823z00zz__lalr_genz00);
			BGl_list1813z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1814z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1816z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_symbol1818z00zz__lalr_genz00,
						MAKE_YOUNG_PAIR(BGl_symbol1820z00zz__lalr_genz00,
							MAKE_YOUNG_PAIR(BGl_symbol1822z00zz__lalr_genz00, BNIL)))));
			BGl_symbol1825z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1826z00zz__lalr_genz00);
			BGl_symbol1829z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1830z00zz__lalr_genz00);
			BGl_symbol1832z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1833z00zz__lalr_genz00);
			BGl_list1831z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1832z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1814z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_symbol1816z00zz__lalr_genz00, BNIL)));
			BGl_list1828z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1829z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1831z00zz__lalr_genz00, BNIL));
			BGl_symbol1835z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1836z00zz__lalr_genz00);
			BGl_symbol1838z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1839z00zz__lalr_genz00);
			BGl_symbol1841z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1842z00zz__lalr_genz00);
			BGl_list1843z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1832z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1820z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_symbol1829z00zz__lalr_genz00, BNIL)));
			BGl_list1840z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1841z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1818z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_list1843z00zz__lalr_genz00, BNIL)));
			BGl_list1837z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1838z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1840z00zz__lalr_genz00, BNIL));
			BGl_list1834z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1837z00zz__lalr_genz00, BNIL));
			BGl_symbol1845z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1846z00zz__lalr_genz00);
			BGl_symbol1848z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1849z00zz__lalr_genz00);
			BGl_list1847z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1848z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1816z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BINT(2L), BNIL)));
			BGl_list1844z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1847z00zz__lalr_genz00, BNIL));
			BGl_list1827z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_list1828z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1834z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_list1844z00zz__lalr_genz00, BNIL)));
			BGl_symbol1851z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1852z00zz__lalr_genz00);
			BGl_list1850z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1851z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1814z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__lalr_genz00,
						MAKE_YOUNG_PAIR(BGl_symbol1835z00zz__lalr_genz00, BNIL))));
			BGl_symbol1855z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1856z00zz__lalr_genz00);
			BGl_list1854z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1855z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BINT(1L), BNIL)));
			BGl_list1853z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1851z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_symbol1814z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_list1854z00zz__lalr_genz00,
						MAKE_YOUNG_PAIR(BGl_symbol1822z00zz__lalr_genz00, BNIL))));
			BGl_list1824z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1825z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1827z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_list1850z00zz__lalr_genz00,
						MAKE_YOUNG_PAIR(BGl_list1853z00zz__lalr_genz00,
							MAKE_YOUNG_PAIR(BGl_symbol1845z00zz__lalr_genz00, BNIL)))));
			BGl_list1810z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1811z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1813z00zz__lalr_genz00,
					MAKE_YOUNG_PAIR(BGl_list1824z00zz__lalr_genz00, BNIL)));
			BGl_list1807z00zz__lalr_genz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1808z00zz__lalr_genz00,
				MAKE_YOUNG_PAIR(BGl_list1810z00zz__lalr_genz00, BNIL));
			BGl_symbol1857z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1858z00zz__lalr_genz00);
			BGl_symbol1859z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1860z00zz__lalr_genz00);
			BGl_symbol1861z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1862z00zz__lalr_genz00);
			BGl_symbol1863z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1864z00zz__lalr_genz00);
			BGl_symbol1865z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1866z00zz__lalr_genz00);
			BGl_symbol1867z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1868z00zz__lalr_genz00);
			BGl_symbol1869z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1870z00zz__lalr_genz00);
			BGl_symbol1871z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1872z00zz__lalr_genz00);
			BGl_symbol1873z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1874z00zz__lalr_genz00);
			BGl_symbol1875z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1876z00zz__lalr_genz00);
			BGl_symbol1877z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1878z00zz__lalr_genz00);
			return (BGl_symbol1879z00zz__lalr_genz00 =
				bstring_to_symbol(BGl_string1880z00zz__lalr_genz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 11 */
			return bgl_gc_roots_register();
		}

	}



/* gen-lalr-code */
	BGL_EXPORTED_DEF obj_t BGl_genzd2lalrzd2codez00zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 51 */
			{	/* Lalr/gen.scm 52 */
				obj_t BgL_arg1305z00_1187;

				{	/* Lalr/gen.scm 52 */
					obj_t BgL_arg1306z00_1188;
					obj_t BgL_arg1307z00_1189;

					{	/* Lalr/gen.scm 52 */
						obj_t BgL_arg1308z00_1190;
						obj_t BgL_arg1309z00_1191;

						BgL_arg1308z00_1190 = BGl_genzd2actionzd2tablez00zz__lalr_genz00();
						{	/* Lalr/gen.scm 53 */
							obj_t BgL_arg1310z00_1192;
							obj_t BgL_arg1311z00_1193;

							BgL_arg1310z00_1192 = BGl_genzd2gotozd2tablez00zz__lalr_genz00();
							{	/* Lalr/gen.scm 54 */
								obj_t BgL_arg1312z00_1194;
								obj_t BgL_arg1314z00_1195;

								BgL_arg1312z00_1194 = BGl_list1807z00zz__lalr_genz00;
								BgL_arg1314z00_1195 =
									MAKE_YOUNG_PAIR(BGl_genzd2reductionzd2tablez00zz__lalr_genz00
									(), BNIL);
								BgL_arg1311z00_1193 =
									MAKE_YOUNG_PAIR(BgL_arg1312z00_1194, BgL_arg1314z00_1195);
							}
							BgL_arg1309z00_1191 =
								MAKE_YOUNG_PAIR(BgL_arg1310z00_1192, BgL_arg1311z00_1193);
						}
						BgL_arg1306z00_1188 =
							MAKE_YOUNG_PAIR(BgL_arg1308z00_1190, BgL_arg1309z00_1191);
					}
					{	/* Lalr/gen.scm 56 */
						obj_t BgL_arg1316z00_1197;

						{	/* Lalr/gen.scm 56 */
							obj_t BgL_arg1317z00_1198;

							{	/* Lalr/gen.scm 56 */
								obj_t BgL_arg1318z00_1199;

								BgL_arg1318z00_1199 =
									MAKE_YOUNG_PAIR(BGl_symbol1857z00zz__lalr_genz00, BNIL);
								BgL_arg1317z00_1198 =
									MAKE_YOUNG_PAIR(BGl_symbol1859z00zz__lalr_genz00,
									BgL_arg1318z00_1199);
							}
							BgL_arg1316z00_1197 =
								MAKE_YOUNG_PAIR(BGl_symbol1861z00zz__lalr_genz00,
								BgL_arg1317z00_1198);
						}
						BgL_arg1307z00_1189 = MAKE_YOUNG_PAIR(BgL_arg1316z00_1197, BNIL);
					}
					BgL_arg1305z00_1187 =
						MAKE_YOUNG_PAIR(BgL_arg1306z00_1188, BgL_arg1307z00_1189);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1825z00zz__lalr_genz00,
					BgL_arg1305z00_1187);
			}
		}

	}



/* &gen-lalr-code */
	obj_t BGl_z62genzd2lalrzd2codez62zz__lalr_genz00(obj_t BgL_envz00_2070)
	{
		{	/* Lalr/gen.scm 51 */
			return BGl_genzd2lalrzd2codez00zz__lalr_genz00();
		}

	}



/* gen-action-table */
	obj_t BGl_genzd2actionzd2tablez00zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 69 */
			{	/* Lalr/gen.scm 86 */
				obj_t BgL_arg1319z00_1201;

				{	/* Lalr/gen.scm 86 */
					obj_t BgL_arg1320z00_1202;

					{	/* Lalr/gen.scm 86 */
						obj_t BgL_arg1321z00_1203;

						BgL_arg1321z00_1203 =
							MAKE_YOUNG_PAIR(BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
							(BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BGl_actionsze70ze7zz__lalr_genz00(), BNIL)), BNIL);
						BgL_arg1320z00_1202 =
							MAKE_YOUNG_PAIR(BGl_symbol1863z00zz__lalr_genz00,
							BgL_arg1321z00_1203);
					}
					BgL_arg1319z00_1201 = MAKE_YOUNG_PAIR(BgL_arg1320z00_1202, BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1859z00zz__lalr_genz00,
					BgL_arg1319z00_1201);
			}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__lalr_genz00(obj_t BgL_lz00_1217)
	{
		{	/* Lalr/gen.scm 75 */
			if (NULLP(BgL_lz00_1217))
				{	/* Lalr/gen.scm 76 */
					return BNIL;
				}
			else
				{	/* Lalr/gen.scm 78 */
					obj_t BgL_pz00_1220;

					BgL_pz00_1220 = CAR(((obj_t) BgL_lz00_1217));
					{	/* Lalr/gen.scm 78 */
						obj_t BgL_xz00_1221;

						BgL_xz00_1221 = CAR(((obj_t) BgL_pz00_1220));
						{	/* Lalr/gen.scm 78 */
							obj_t BgL_yz00_1222;

							BgL_yz00_1222 = CDR(((obj_t) BgL_pz00_1220));
							{	/* Lalr/gen.scm 78 */

								{	/* Lalr/gen.scm 81 */
									obj_t BgL_arg1335z00_1223;
									obj_t BgL_arg1336z00_1224;

									{	/* Lalr/gen.scm 81 */
										obj_t BgL_arg1337z00_1225;

										if (INTEGERP(BgL_xz00_1221))
											{	/* Lalr/gen.scm 81 */
												BgL_arg1337z00_1225 =
													VECTOR_REF(BGl_za2symvza2z00zz__lalr_rewritez00,
													((long) CINT(BGl_nvarsz00zz__lalr_globalz00) +
														(long) CINT(BgL_xz00_1221)));
											}
										else
											{	/* Lalr/gen.scm 81 */
												BgL_arg1337z00_1225 = BgL_xz00_1221;
											}
										BgL_arg1335z00_1223 =
											MAKE_YOUNG_PAIR(BgL_arg1337z00_1225, BgL_yz00_1222);
									}
									{	/* Lalr/gen.scm 83 */
										obj_t BgL_arg1340z00_1228;

										BgL_arg1340z00_1228 = CDR(((obj_t) BgL_lz00_1217));
										BgL_arg1336z00_1224 =
											BGl_loopze72ze7zz__lalr_genz00(BgL_arg1340z00_1228);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg1335z00_1223, BgL_arg1336z00_1224);
								}
							}
						}
					}
				}
		}

	}



/* loop-a~0 */
	obj_t BGl_loopzd2aze70z35zz__lalr_genz00(long BgL_iz00_1209)
	{
		{	/* Lalr/gen.scm 71 */
			{	/* Lalr/gen.scm 72 */
				bool_t BgL_test1916z00_2233;

				if (INTEGERP(BGl_nstatesz00zz__lalr_globalz00))
					{	/* Lalr/gen.scm 72 */
						BgL_test1916z00_2233 =
							(BgL_iz00_1209 == (long) CINT(BGl_nstatesz00zz__lalr_globalz00));
					}
				else
					{	/* Lalr/gen.scm 72 */
						BgL_test1916z00_2233 =
							BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_1209),
							BGl_nstatesz00zz__lalr_globalz00);
					}
				if (BgL_test1916z00_2233)
					{	/* Lalr/gen.scm 72 */
						return BNIL;
					}
				else
					{	/* Lalr/gen.scm 72 */
						return
							MAKE_YOUNG_PAIR(BGl_loopze72ze7zz__lalr_genz00(VECTOR_REF
								(BGl_actionzd2tablezd2zz__lalr_globalz00, BgL_iz00_1209)),
							BGl_loopzd2aze70z35zz__lalr_genz00((BgL_iz00_1209 + 1L)));
					}
			}
		}

	}



/* actions~0 */
	obj_t BGl_actionsze70ze7zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 84 */
			return BGl_loopzd2aze70z35zz__lalr_genz00(0L);
		}

	}



/* gen-goto-table */
	obj_t BGl_genzd2gotozd2tablez00zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 88 */
			{	/* Lalr/gen.scm 109 */
				obj_t BgL_arg1342z00_1235;

				{	/* Lalr/gen.scm 109 */
					obj_t BgL_arg1343z00_1236;

					{	/* Lalr/gen.scm 109 */
						obj_t BgL_arg1344z00_1237;

						BgL_arg1344z00_1237 =
							MAKE_YOUNG_PAIR(BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
							(BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BGl_gotosze70ze7zz__lalr_genz00(), BNIL)), BNIL);
						BgL_arg1343z00_1236 =
							MAKE_YOUNG_PAIR(BGl_symbol1863z00zz__lalr_genz00,
							BgL_arg1344z00_1237);
					}
					BgL_arg1342z00_1235 = MAKE_YOUNG_PAIR(BgL_arg1343z00_1236, BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1865z00zz__lalr_genz00,
					BgL_arg1342z00_1235);
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__lalr_genz00(obj_t BgL_lz00_1252)
	{
		{	/* Lalr/gen.scm 96 */
		BGl_loopze71ze7zz__lalr_genz00:
			if (NULLP(BgL_lz00_1252))
				{	/* Lalr/gen.scm 97 */
					return BNIL;
				}
			else
				{	/* Lalr/gen.scm 99 */
					obj_t BgL_statez00_1255;

					BgL_statez00_1255 = CAR(((obj_t) BgL_lz00_1252));
					{	/* Lalr/gen.scm 99 */
						obj_t BgL_symbolz00_1256;

						BgL_symbolz00_1256 =
							VECTOR_REF(BGl_acceszd2symbolzd2zz__lalr_globalz00,
							(long) CINT(BgL_statez00_1255));
						{	/* Lalr/gen.scm 100 */

							if (
								((long) CINT(BgL_symbolz00_1256) <
									(long) CINT(BGl_nvarsz00zz__lalr_globalz00)))
								{	/* Lalr/gen.scm 103 */
									obj_t BgL_arg1360z00_1258;
									obj_t BgL_arg1361z00_1259;

									BgL_arg1360z00_1258 =
										MAKE_YOUNG_PAIR(VECTOR_REF
										(BGl_za2symvza2z00zz__lalr_rewritez00,
											(long) CINT(BgL_symbolz00_1256)), BgL_statez00_1255);
									{	/* Lalr/gen.scm 104 */
										obj_t BgL_arg1363z00_1261;

										BgL_arg1363z00_1261 = CDR(((obj_t) BgL_lz00_1252));
										BgL_arg1361z00_1259 =
											BGl_loopze71ze7zz__lalr_genz00(BgL_arg1363z00_1261);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg1360z00_1258, BgL_arg1361z00_1259);
								}
							else
								{	/* Lalr/gen.scm 105 */
									obj_t BgL_arg1364z00_1262;

									BgL_arg1364z00_1262 = CDR(((obj_t) BgL_lz00_1252));
									{
										obj_t BgL_lz00_2272;

										BgL_lz00_2272 = BgL_arg1364z00_1262;
										BgL_lz00_1252 = BgL_lz00_2272;
										goto BGl_loopze71ze7zz__lalr_genz00;
									}
								}
						}
					}
				}
		}

	}



/* loop-g~0 */
	obj_t BGl_loopzd2gze70z35zz__lalr_genz00(long BgL_iz00_1243)
	{
		{	/* Lalr/gen.scm 90 */
			{	/* Lalr/gen.scm 91 */
				bool_t BgL_test1920z00_2273;

				if (INTEGERP(BGl_nstatesz00zz__lalr_globalz00))
					{	/* Lalr/gen.scm 91 */
						BgL_test1920z00_2273 =
							(BgL_iz00_1243 == (long) CINT(BGl_nstatesz00zz__lalr_globalz00));
					}
				else
					{	/* Lalr/gen.scm 91 */
						BgL_test1920z00_2273 =
							BGl_2zd3zd3zz__r4_numbers_6_5z00(BINT(BgL_iz00_1243),
							BGl_nstatesz00zz__lalr_globalz00);
					}
				if (BgL_test1920z00_2273)
					{	/* Lalr/gen.scm 91 */
						return BNIL;
					}
				else
					{	/* Lalr/gen.scm 94 */
						obj_t BgL_arg1354z00_1247;
						obj_t BgL_arg1356z00_1248;

						{	/* Lalr/gen.scm 94 */
							obj_t BgL_shiftsz00_1249;

							BgL_shiftsz00_1249 =
								VECTOR_REF(BGl_shiftzd2tablezd2zz__lalr_globalz00,
								BgL_iz00_1243);
							if (CBOOL(BgL_shiftsz00_1249))
								{	/* Lalr/gen.scm 96 */
									obj_t BgL_g1128z00_1250;

									BgL_g1128z00_1250 =
										VECTOR_REF(((obj_t) BgL_shiftsz00_1249), 2L);
									BgL_arg1354z00_1247 =
										BGl_loopze71ze7zz__lalr_genz00(BgL_g1128z00_1250);
								}
							else
								{	/* Lalr/gen.scm 95 */
									BgL_arg1354z00_1247 = BNIL;
								}
						}
						BgL_arg1356z00_1248 =
							BGl_loopzd2gze70z35zz__lalr_genz00((BgL_iz00_1243 + 1L));
						return MAKE_YOUNG_PAIR(BgL_arg1354z00_1247, BgL_arg1356z00_1248);
					}
			}
		}

	}



/* gotos~0 */
	obj_t BGl_gotosze70ze7zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 107 */
			return BGl_loopzd2gze70z35zz__lalr_genz00(0L);
		}

	}



/* gen-reduction-table */
	obj_t BGl_genzd2reductionzd2tablez00zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 111 */
			{	/* Lalr/gen.scm 148 */
				obj_t BgL_arg1366z00_1271;

				{	/* Lalr/gen.scm 148 */
					obj_t BgL_arg1367z00_1272;

					{	/* Lalr/gen.scm 148 */
						obj_t BgL_arg1368z00_1273;

						{	/* Lalr/gen.scm 148 */
							obj_t BgL_arg1369z00_1274;
							obj_t BgL_arg1370z00_1275;

							{	/* Lalr/gen.scm 148 */
								obj_t BgL_arg1371z00_1276;

								{	/* Lalr/gen.scm 148 */
									obj_t BgL_arg1372z00_1277;

									BgL_arg1372z00_1277 =
										MAKE_YOUNG_PAIR(BGl_symbol1867z00zz__lalr_genz00, BNIL);
									BgL_arg1371z00_1276 =
										MAKE_YOUNG_PAIR(BGl_symbol1869z00zz__lalr_genz00,
										BgL_arg1372z00_1277);
								}
								BgL_arg1369z00_1274 =
									MAKE_YOUNG_PAIR(BGl_symbol1871z00zz__lalr_genz00,
									BgL_arg1371z00_1276);
							}
							{	/* Lalr/gen.scm 150 */
								obj_t BgL_arg1373z00_1278;

								{	/* Lalr/gen.scm 150 */
									obj_t BgL_arg1375z00_1279;

									{	/* Lalr/gen.scm 150 */
										obj_t BgL_arg1376z00_1280;

										BgL_arg1376z00_1280 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BGl_reductionsze70ze7zz__lalr_genz00(), BNIL);
										BgL_arg1375z00_1279 =
											MAKE_YOUNG_PAIR(BGl_symbol1871z00zz__lalr_genz00,
											BgL_arg1376z00_1280);
									}
									BgL_arg1373z00_1278 =
										MAKE_YOUNG_PAIR(BGl_symbol1873z00zz__lalr_genz00,
										BgL_arg1375z00_1279);
								}
								BgL_arg1370z00_1275 =
									MAKE_YOUNG_PAIR(BgL_arg1373z00_1278, BNIL);
							}
							BgL_arg1368z00_1273 =
								MAKE_YOUNG_PAIR(BgL_arg1369z00_1274, BgL_arg1370z00_1275);
						}
						BgL_arg1367z00_1272 =
							MAKE_YOUNG_PAIR(BGl_symbol1811z00zz__lalr_genz00,
							BgL_arg1368z00_1273);
					}
					BgL_arg1366z00_1271 = MAKE_YOUNG_PAIR(BgL_arg1367z00_1272, BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1857z00zz__lalr_genz00,
					BgL_arg1366z00_1271);
			}
		}

	}



/* loop-l~0 */
	obj_t BGl_loopzd2lze70z35zz__lalr_genz00(obj_t BgL_lz00_1328,
		long BgL_noz00_1329)
	{
		{	/* Lalr/gen.scm 131 */
			if (NULLP(BgL_lz00_1328))
				{	/* Lalr/gen.scm 132 */
					return BNIL;
				}
			else
				{	/* Lalr/gen.scm 134 */
					obj_t BgL_defz00_1332;

					BgL_defz00_1332 = CAR(((obj_t) BgL_lz00_1328));
					{	/* Lalr/gen.scm 134 */
						obj_t BgL_ntz00_1333;

						BgL_ntz00_1333 = CAR(((obj_t) BgL_defz00_1332));
						{	/* Lalr/gen.scm 135 */

							{	/* Lalr/gen.scm 136 */
								obj_t BgL_g1129z00_1334;

								BgL_g1129z00_1334 = CDR(((obj_t) BgL_defz00_1332));
								return
									BGl_loopzd2pze70z35zz__lalr_genz00(BgL_lz00_1328,
									BgL_ntz00_1333, BgL_g1129z00_1334, BgL_noz00_1329);
							}
						}
					}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__lalr_genz00(long BgL_iz00_1287, obj_t BgL_lz00_1288)
	{
		{	/* Lalr/gen.scm 113 */
			if (NULLP(BgL_lz00_1288))
				{	/* Lalr/gen.scm 114 */
					return BNIL;
				}
			else
				{	/* Lalr/gen.scm 116 */
					obj_t BgL_symz00_1291;

					BgL_symz00_1291 = CAR(((obj_t) BgL_lz00_1288));
					{	/* Lalr/gen.scm 118 */
						obj_t BgL_arg1382z00_1292;
						obj_t BgL_arg1383z00_1293;

						{	/* Lalr/gen.scm 118 */
							obj_t BgL_arg1384z00_1294;
							obj_t BgL_arg1387z00_1295;

							if (PAIRP(BgL_symz00_1291))
								{	/* Lalr/gen.scm 118 */
									BgL_arg1384z00_1294 = CDR(BgL_symz00_1291);
								}
							else
								{	/* Lalr/gen.scm 118 */
									BgL_arg1384z00_1294 = BgL_symz00_1291;
								}
							{	/* Lalr/gen.scm 119 */
								obj_t BgL_arg1389z00_1297;

								{	/* Lalr/gen.scm 119 */
									obj_t BgL_arg1390z00_1298;

									{	/* Lalr/gen.scm 119 */
										obj_t BgL_arg1391z00_1299;

										{	/* Lalr/gen.scm 119 */
											obj_t BgL_arg1392z00_1300;

											{	/* Lalr/gen.scm 119 */
												obj_t BgL_arg1393z00_1301;

												{	/* Lalr/gen.scm 119 */
													obj_t BgL_arg1394z00_1302;

													{	/* Lalr/gen.scm 119 */
														long BgL_arg1395z00_1303;

														BgL_arg1395z00_1303 = ((BgL_iz00_1287 * 2L) - 1L);
														BgL_arg1394z00_1302 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1395z00_1303), BNIL);
													}
													BgL_arg1393z00_1301 =
														MAKE_YOUNG_PAIR(BGl_symbol1867z00zz__lalr_genz00,
														BgL_arg1394z00_1302);
												}
												BgL_arg1392z00_1300 =
													MAKE_YOUNG_PAIR(BGl_symbol1855z00zz__lalr_genz00,
													BgL_arg1393z00_1301);
											}
											BgL_arg1391z00_1299 =
												MAKE_YOUNG_PAIR(BgL_arg1392z00_1300, BNIL);
										}
										BgL_arg1390z00_1298 =
											MAKE_YOUNG_PAIR(BGl_symbol1869z00zz__lalr_genz00,
											BgL_arg1391z00_1299);
									}
									BgL_arg1389z00_1297 =
										MAKE_YOUNG_PAIR(BGl_symbol1875z00zz__lalr_genz00,
										BgL_arg1390z00_1298);
								}
								BgL_arg1387z00_1295 =
									MAKE_YOUNG_PAIR(BgL_arg1389z00_1297, BNIL);
							}
							BgL_arg1382z00_1292 =
								MAKE_YOUNG_PAIR(BgL_arg1384z00_1294, BgL_arg1387z00_1295);
						}
						{	/* Lalr/gen.scm 120 */
							long BgL_arg1397z00_1305;
							obj_t BgL_arg1399z00_1306;

							BgL_arg1397z00_1305 = (BgL_iz00_1287 - 1L);
							BgL_arg1399z00_1306 = CDR(((obj_t) BgL_lz00_1288));
							BgL_arg1383z00_1293 =
								BGl_loopze70ze7zz__lalr_genz00(BgL_arg1397z00_1305,
								BgL_arg1399z00_1306);
						}
						return MAKE_YOUNG_PAIR(BgL_arg1382z00_1292, BgL_arg1383z00_1293);
					}
				}
		}

	}



/* loop-p~0 */
	obj_t BGl_loopzd2pze70z35zz__lalr_genz00(obj_t BgL_lz00_2072,
		obj_t BgL_ntz00_2071, obj_t BgL_prodsz00_1336, long BgL_noz00_1337)
	{
		{	/* Lalr/gen.scm 136 */
			{
				obj_t BgL_ntz00_1308;
				long BgL_nz00_1309;
				obj_t BgL_actz00_1310;

				if (NULLP(BgL_prodsz00_1336))
					{	/* Lalr/gen.scm 138 */
						obj_t BgL_arg1421z00_1340;

						BgL_arg1421z00_1340 = CDR(((obj_t) BgL_lz00_2072));
						return
							BGl_loopzd2lze70z35zz__lalr_genz00(BgL_arg1421z00_1340,
							BgL_noz00_1337);
					}
				else
					{	/* Lalr/gen.scm 139 */
						obj_t BgL_rhsz00_1341;

						{	/* Lalr/gen.scm 139 */
							obj_t BgL_pairz00_1841;

							BgL_pairz00_1841 = CAR(((obj_t) BgL_prodsz00_1336));
							BgL_rhsz00_1341 = CAR(BgL_pairz00_1841);
						}
						{	/* Lalr/gen.scm 139 */
							obj_t BgL_actz00_1342;

							{	/* Lalr/gen.scm 139 */
								obj_t BgL_pairz00_1845;

								BgL_pairz00_1845 = CAR(((obj_t) BgL_prodsz00_1336));
								BgL_actz00_1342 = CDR(BgL_pairz00_1845);
							}
							{	/* Lalr/gen.scm 139 */
								long BgL_nz00_1343;

								BgL_nz00_1343 = bgl_list_length(BgL_rhsz00_1341);
								{	/* Lalr/gen.scm 139 */

									{	/* Lalr/gen.scm 142 */
										obj_t BgL_arg1422z00_1344;
										obj_t BgL_arg1423z00_1345;

										{	/* Lalr/gen.scm 142 */
											obj_t BgL_arg1424z00_1346;
											obj_t BgL_arg1425z00_1347;

											{	/* Lalr/gen.scm 142 */
												obj_t BgL_list1428z00_1350;

												BgL_list1428z00_1350 =
													MAKE_YOUNG_PAIR(BINT(BgL_noz00_1337), BNIL);
												BgL_arg1424z00_1346 = BgL_list1428z00_1350;
											}
											{	/* Lalr/gen.scm 143 */
												obj_t BgL_arg1429z00_1351;

												{	/* Lalr/gen.scm 143 */
													obj_t BgL_arg1430z00_1352;
													obj_t BgL_arg1431z00_1353;

													BgL_arg1430z00_1352 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BGl_loopze70ze7zz__lalr_genz00(BgL_nz00_1343,
															BgL_rhsz00_1341), BNIL);
													{	/* Lalr/gen.scm 143 */
														obj_t BgL_tmpz00_2350;

														BgL_ntz00_1308 = BgL_ntz00_2071;
														BgL_nz00_1309 = BgL_nz00_1343;
														BgL_actz00_1310 = BgL_actz00_1342;
														if (
															(BgL_ntz00_1308 ==
																BGl_symbol1877z00zz__lalr_genz00))
															{	/* Lalr/gen.scm 123 */
																BgL_tmpz00_2350 =
																	VECTOR_REF
																	(BGl_za2symvza2z00zz__lalr_rewritez00, 1L);
															}
														else
															{	/* Lalr/gen.scm 125 */
																obj_t BgL_arg1401z00_1312;

																{	/* Lalr/gen.scm 125 */
																	obj_t BgL_arg1402z00_1313;

																	{	/* Lalr/gen.scm 125 */
																		obj_t BgL_arg1403z00_1314;
																		obj_t BgL_arg1404z00_1315;

																		{	/* Lalr/gen.scm 125 */
																			obj_t BgL_arg1405z00_1316;

																			{	/* Lalr/gen.scm 125 */
																				obj_t BgL_arg1406z00_1317;

																				{	/* Lalr/gen.scm 125 */
																					long BgL_arg1407z00_1318;

																					BgL_arg1407z00_1318 =
																						(2L * BgL_nz00_1309);
																					BgL_arg1406z00_1317 =
																						MAKE_YOUNG_PAIR(BINT
																						(BgL_arg1407z00_1318), BNIL);
																				}
																				BgL_arg1405z00_1316 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1867z00zz__lalr_genz00,
																					BgL_arg1406z00_1317);
																			}
																			BgL_arg1403z00_1314 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1855z00zz__lalr_genz00,
																				BgL_arg1405z00_1316);
																		}
																		{	/* Lalr/gen.scm 126 */
																			obj_t BgL_arg1408z00_1319;
																			obj_t BgL_arg1410z00_1320;

																			{	/* Lalr/gen.scm 126 */
																				obj_t BgL_arg1411z00_1321;

																				BgL_arg1411z00_1321 =
																					MAKE_YOUNG_PAIR(BgL_ntz00_1308, BNIL);
																				BgL_arg1408z00_1319 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1863z00zz__lalr_genz00,
																					BgL_arg1411z00_1321);
																			}
																			{	/* Lalr/gen.scm 128 */
																				obj_t BgL_arg1412z00_1322;

																				{	/* Lalr/gen.scm 128 */
																					obj_t BgL_arg1413z00_1323;

																					{	/* Lalr/gen.scm 128 */
																						obj_t BgL_arg1414z00_1324;

																						BgL_arg1414z00_1324 =
																							MAKE_YOUNG_PAIR(BNIL,
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_actz00_1310, BNIL));
																						BgL_arg1413z00_1323 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1879z00zz__lalr_genz00,
																							BgL_arg1414z00_1324);
																					}
																					BgL_arg1412z00_1322 =
																						MAKE_YOUNG_PAIR(BgL_arg1413z00_1323,
																						BNIL);
																				}
																				BgL_arg1410z00_1320 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1865z00zz__lalr_genz00,
																					BgL_arg1412z00_1322);
																			}
																			BgL_arg1404z00_1315 =
																				MAKE_YOUNG_PAIR(BgL_arg1408z00_1319,
																				BgL_arg1410z00_1320);
																		}
																		BgL_arg1402z00_1313 =
																			MAKE_YOUNG_PAIR(BgL_arg1403z00_1314,
																			BgL_arg1404z00_1315);
																	}
																	BgL_arg1401z00_1312 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol1869z00zz__lalr_genz00,
																		BgL_arg1402z00_1313);
																}
																BgL_tmpz00_2350 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1808z00zz__lalr_genz00,
																	BgL_arg1401z00_1312);
															}
														BgL_arg1431z00_1353 =
															MAKE_YOUNG_PAIR(BgL_tmpz00_2350, BNIL);
													}
													BgL_arg1429z00_1351 =
														MAKE_YOUNG_PAIR(BgL_arg1430z00_1352,
														BgL_arg1431z00_1353);
												}
												BgL_arg1425z00_1347 =
													MAKE_YOUNG_PAIR(BGl_symbol1879z00zz__lalr_genz00,
													BgL_arg1429z00_1351);
											}
											{	/* Lalr/gen.scm 141 */
												obj_t BgL_list1426z00_1348;

												{	/* Lalr/gen.scm 141 */
													obj_t BgL_arg1427z00_1349;

													BgL_arg1427z00_1349 =
														MAKE_YOUNG_PAIR(BgL_arg1425z00_1347, BNIL);
													BgL_list1426z00_1348 =
														MAKE_YOUNG_PAIR(BgL_arg1424z00_1346,
														BgL_arg1427z00_1349);
												}
												BgL_arg1422z00_1344 = BgL_list1426z00_1348;
										}}
										{	/* Lalr/gen.scm 145 */
											obj_t BgL_arg1436z00_1356;
											long BgL_arg1437z00_1357;

											BgL_arg1436z00_1356 = CDR(((obj_t) BgL_prodsz00_1336));
											BgL_arg1437z00_1357 = (BgL_noz00_1337 + 1L);
											BgL_arg1423z00_1345 =
												BGl_loopzd2pze70z35zz__lalr_genz00(BgL_lz00_2072,
												BgL_ntz00_2071, BgL_arg1436z00_1356,
												BgL_arg1437z00_1357);
										}
										return
											MAKE_YOUNG_PAIR(BgL_arg1422z00_1344, BgL_arg1423z00_1345);
									}
								}
							}
						}
					}
			}
		}

	}



/* reductions~0 */
	obj_t BGl_reductionsze70ze7zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 145 */
			return
				BGl_loopzd2lze70z35zz__lalr_genz00(BGl_grammarz00zz__lalr_globalz00,
				1L);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 11 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 11 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 11 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__lalr_genz00(void)
	{
		{	/* Lalr/gen.scm 11 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__lalr_globalz00(39277513L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__lalr_rewritez00(80638262L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
			return BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string1881z00zz__lalr_genz00));
		}

	}

#ifdef __cplusplus
}
#endif
