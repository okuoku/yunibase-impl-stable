/*===========================================================================*/
/*   (Match/mexpand.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Match/mexpand.scm -indent -o objs/obj_u/Match/mexpand.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MATCH_EXPAND_TYPE_DEFINITIONS
#define BGL___MATCH_EXPAND_TYPE_DEFINITIONS
#endif													// BGL___MATCH_EXPAND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2matchzd2lambdaz00zz__match_expandz00(obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2matchzd2casez00zz__match_expandz00(obj_t);
	static obj_t BGl_list1731z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__match_expandz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__match_expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_descriptionsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_compilerz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza31194ze3ze5zz__match_expandz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__match_expandz00(void);
	extern obj_t BGl_jimzd2gensymzd2zz__match_s2cfunz00;
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__match_expandz00(void);
	static obj_t BGl_genericzd2initzd2zz__match_expandz00(void);
	static obj_t BGl_za2thezd2emptyzd2envza2z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__match_expandz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__match_expandz00(void);
	static obj_t BGl_objectzd2initzd2zz__match_expandz00(void);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31232ze3ze5zz__match_expandz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62expandzd2matchzd2lambdaz62zz__match_expandz00(obj_t,
		obj_t);
	extern obj_t BGl_patternzd2variableszd2zz__match_descriptionsz00(obj_t);
	extern obj_t BGl_pcompilez00zz__match_compilerz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_fetchzd2prototypeszd2zz__match_expandz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__match_expandz00(void);
	static obj_t BGl_symbol1716z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_symbol1719z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_symbol1722z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_symbol1724z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_symbol1726z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_symbol1729z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2matchzd2casez62zz__match_expandz00(obj_t, obj_t);
	static obj_t BGl_symbol1732z00zz__match_expandz00 = BUNSPEC;
	static obj_t BGl_symbol1734z00zz__match_expandz00 = BUNSPEC;
	extern obj_t BGl_normaliza7ezd2patternz75zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_list1715z00zz__match_expandz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_list1718z00zz__match_expandz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1717z00zz__match_expandz00,
		BgL_bgl_string1717za700za7za7_1737za7, "not", 3);
	      DEFINE_STRING(BGl_string1720z00zz__match_expandz00,
		BgL_bgl_string1720za700za7za7_1738za7, "any", 3);
	      DEFINE_STRING(BGl_string1721z00zz__match_expandz00,
		BgL_bgl_string1721za700za7za7_1739za7, "TAG-", 4);
	      DEFINE_STRING(BGl_string1723z00zz__match_expandz00,
		BgL_bgl_string1723za700za7za7_1740za7, "else", 4);
	      DEFINE_STRING(BGl_string1725z00zz__match_expandz00,
		BgL_bgl_string1725za700za7za7_1741za7, "tagged-or", 9);
	      DEFINE_STRING(BGl_string1727z00zz__match_expandz00,
		BgL_bgl_string1727za700za7za7_1742za7, "match-case", 10);
	      DEFINE_STRING(BGl_string1728z00zz__match_expandz00,
		BgL_bgl_string1728za700za7za7_1743za7, "Illegal expression", 18);
	      DEFINE_STRING(BGl_string1730z00zz__match_expandz00,
		BgL_bgl_string1730za700za7za7_1744za7, "labels", 6);
	      DEFINE_STRING(BGl_string1733z00zz__match_expandz00,
		BgL_bgl_string1733za700za7za7_1745za7, "t-or", 4);
	      DEFINE_STRING(BGl_string1735z00zz__match_expandz00,
		BgL_bgl_string1735za700za7za7_1746za7, "match-lambda", 12);
	      DEFINE_STRING(BGl_string1736z00zz__match_expandz00,
		BgL_bgl_string1736za700za7za7_1747za7, "__match_expand", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2matchzd2casezd2envzd2zz__match_expandz00,
		BgL_bgl_za762expandza7d2matc1748z00,
		BGl_z62expandzd2matchzd2casez62zz__match_expandz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2matchzd2lambdazd2envzd2zz__match_expandz00,
		BgL_bgl_za762expandza7d2matc1749z00,
		BGl_z62expandzd2matchzd2lambdaz62zz__match_expandz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list1731z00zz__match_expandz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__match_expandz00));
		     ADD_ROOT((void
				*) (&BGl_za2thezd2emptyzd2envza2z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1716z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1719z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1722z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1724z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1726z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1729z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1732z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1734z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_list1715z00zz__match_expandz00));
		     ADD_ROOT((void *) (&BGl_list1718z00zz__match_expandz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__match_expandz00(long
		BgL_checksumz00_2131, char *BgL_fromz00_2132)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__match_expandz00))
				{
					BGl_requirezd2initializa7ationz75zz__match_expandz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__match_expandz00();
					BGl_cnstzd2initzd2zz__match_expandz00();
					BGl_importedzd2moduleszd2initz00zz__match_expandz00();
					return BGl_toplevelzd2initzd2zz__match_expandz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			BGl_symbol1716z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1717z00zz__match_expandz00);
			BGl_symbol1719z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1720z00zz__match_expandz00);
			BGl_list1718z00zz__match_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1719z00zz__match_expandz00, BNIL);
			BGl_list1715z00zz__match_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1716z00zz__match_expandz00,
				MAKE_YOUNG_PAIR(BGl_list1718z00zz__match_expandz00, BNIL));
			BGl_symbol1722z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1723z00zz__match_expandz00);
			BGl_symbol1724z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1725z00zz__match_expandz00);
			BGl_symbol1726z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1727z00zz__match_expandz00);
			BGl_symbol1729z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1730z00zz__match_expandz00);
			BGl_symbol1732z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1733z00zz__match_expandz00);
			BGl_list1731z00zz__match_expandz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1732z00zz__match_expandz00,
				MAKE_YOUNG_PAIR(BGl_symbol1724z00zz__match_expandz00, BNIL));
			return (BGl_symbol1734z00zz__match_expandz00 =
				bstring_to_symbol(BGl_string1735z00zz__match_expandz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			return (BGl_za2thezd2emptyzd2envza2z00zz__match_expandz00 =
				BNIL, BUNSPEC);
		}

	}



/* expand-match-lambda */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2matchzd2lambdaz00zz__match_expandz00(obj_t
		BgL_expz00_3)
	{
		{	/* Match/mexpand.scm 74 */
			{
				obj_t BgL_clausesz00_1217;
				obj_t BgL_kz00_1218;

				{	/* Match/mexpand.scm 98 */
					obj_t BgL_arg1191z00_1220;

					BgL_arg1191z00_1220 = CDR(((obj_t) BgL_expz00_3));
					{	/* Match/mexpand.scm 100 */
						obj_t BgL_zc3z04anonymousza31194ze3z87_2075;

						{
							int BgL_tmpz00_2156;

							BgL_tmpz00_2156 = (int) (1L);
							BgL_zc3z04anonymousza31194ze3z87_2075 =
								MAKE_L_PROCEDURE
								(BGl_z62zc3z04anonymousza31194ze3ze5zz__match_expandz00,
								BgL_tmpz00_2156);
						}
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31194ze3z87_2075,
							(int) (0L), BgL_expz00_3);
						BgL_clausesz00_1217 = BgL_arg1191z00_1220;
						BgL_kz00_1218 = BgL_zc3z04anonymousza31194ze3z87_2075;
					BgL_clauseszd2ze3patternz31_1219:
						if (NULLP(BgL_clausesz00_1217))
							{	/* Match/mexpand.scm 78 */
								return
									((obj_t(*)(obj_t, obj_t,
											obj_t)) PROCEDURE_L_ENTRY(BgL_kz00_1218)) (BgL_kz00_1218,
									BGl_list1715z00zz__match_expandz00, BNIL);
							}
						else
							{	/* Match/mexpand.scm 80 */
								bool_t BgL_test1752z00_2168;

								{	/* Match/mexpand.scm 80 */
									obj_t BgL_tmpz00_2169;

									BgL_tmpz00_2169 = CAR(((obj_t) BgL_clausesz00_1217));
									BgL_test1752z00_2168 = PAIRP(BgL_tmpz00_2169);
								}
								if (BgL_test1752z00_2168)
									{	/* Match/mexpand.scm 83 */
										obj_t BgL_patternz00_1255;
										obj_t BgL_actionsz00_1256;
										obj_t BgL_restz00_1257;

										{	/* Match/mexpand.scm 83 */
											obj_t BgL_pairz00_1766;

											BgL_pairz00_1766 = CAR(((obj_t) BgL_clausesz00_1217));
											BgL_patternz00_1255 = CAR(BgL_pairz00_1766);
										}
										{	/* Match/mexpand.scm 84 */
											obj_t BgL_pairz00_1770;

											BgL_pairz00_1770 = CAR(((obj_t) BgL_clausesz00_1217));
											BgL_actionsz00_1256 = CDR(BgL_pairz00_1770);
										}
										BgL_restz00_1257 = CDR(((obj_t) BgL_clausesz00_1217));
										{	/* Match/mexpand.scm 86 */
											obj_t BgL_tagz00_1258;

											BgL_tagz00_1258 =
												BGL_PROCEDURE_CALL1
												(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
												BGl_string1721z00zz__match_expandz00);
											if ((BgL_patternz00_1255 ==
													BGl_symbol1722z00zz__match_expandz00))
												{	/* Match/mexpand.scm 88 */
													obj_t BgL_arg1220z00_1259;
													obj_t BgL_arg1221z00_1260;

													{	/* Match/mexpand.scm 88 */
														obj_t BgL_arg1223z00_1261;

														{	/* Match/mexpand.scm 88 */
															obj_t BgL_arg1225z00_1262;
															obj_t BgL_arg1226z00_1263;

															BgL_arg1225z00_1262 =
																MAKE_YOUNG_PAIR
																(BGl_symbol1719z00zz__match_expandz00, BNIL);
															{	/* Match/mexpand.scm 88 */
																obj_t BgL_arg1227z00_1264;

																{	/* Match/mexpand.scm 88 */
																	obj_t BgL_arg1228z00_1265;

																	{	/* Match/mexpand.scm 88 */
																		obj_t BgL_arg1229z00_1266;

																		{	/* Match/mexpand.scm 88 */
																			obj_t BgL_arg1230z00_1267;

																			BgL_arg1230z00_1267 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1719z00zz__match_expandz00,
																				BNIL);
																			BgL_arg1229z00_1266 =
																				MAKE_YOUNG_PAIR(BgL_arg1230z00_1267,
																				BNIL);
																		}
																		BgL_arg1228z00_1265 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1716z00zz__match_expandz00,
																			BgL_arg1229z00_1266);
																	}
																	BgL_arg1227z00_1264 =
																		MAKE_YOUNG_PAIR(BgL_arg1228z00_1265, BNIL);
																}
																BgL_arg1226z00_1263 =
																	MAKE_YOUNG_PAIR(BgL_tagz00_1258,
																	BgL_arg1227z00_1264);
															}
															BgL_arg1223z00_1261 =
																MAKE_YOUNG_PAIR(BgL_arg1225z00_1262,
																BgL_arg1226z00_1263);
														}
														BgL_arg1220z00_1259 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1724z00zz__match_expandz00,
															BgL_arg1223z00_1261);
													}
													{	/* Match/mexpand.scm 131 */
														obj_t BgL_arg1320z00_1772;

														BgL_arg1320z00_1772 =
															MAKE_YOUNG_PAIR(BgL_tagz00_1258,
															BgL_actionsz00_1256);
														BgL_arg1221z00_1260 =
															MAKE_YOUNG_PAIR(BgL_arg1320z00_1772, BNIL);
													}
													return
														((obj_t(*)(obj_t, obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_kz00_1218)) (BgL_kz00_1218,
														BgL_arg1220z00_1259, BgL_arg1221z00_1260);
												}
											else
												{	/* Match/mexpand.scm 93 */
													obj_t BgL_zc3z04anonymousza31232ze3z87_2076;

													{
														int BgL_tmpz00_2202;

														BgL_tmpz00_2202 = (int) (4L);
														BgL_zc3z04anonymousza31232ze3z87_2076 =
															MAKE_L_PROCEDURE
															(BGl_z62zc3z04anonymousza31232ze3ze5zz__match_expandz00,
															BgL_tmpz00_2202);
													}
													PROCEDURE_L_SET(BgL_zc3z04anonymousza31232ze3z87_2076,
														(int) (0L), BgL_patternz00_1255);
													PROCEDURE_L_SET(BgL_zc3z04anonymousza31232ze3z87_2076,
														(int) (1L), BgL_tagz00_1258);
													PROCEDURE_L_SET(BgL_zc3z04anonymousza31232ze3z87_2076,
														(int) (2L), BgL_actionsz00_1256);
													PROCEDURE_L_SET(BgL_zc3z04anonymousza31232ze3z87_2076,
														(int) (3L), BgL_kz00_1218);
													{
														obj_t BgL_kz00_2214;
														obj_t BgL_clausesz00_2213;

														BgL_clausesz00_2213 = BgL_restz00_1257;
														BgL_kz00_2214 =
															BgL_zc3z04anonymousza31232ze3z87_2076;
														BgL_kz00_1218 = BgL_kz00_2214;
														BgL_clausesz00_1217 = BgL_clausesz00_2213;
														goto BgL_clauseszd2ze3patternz31_1219;
													}
												}
										}
									}
								else
									{	/* Match/mexpand.scm 80 */
										return
											BGl_errorz00zz__errorz00
											(BGl_symbol1726z00zz__match_expandz00,
											BGl_string1728z00zz__match_expandz00, BgL_expz00_3);
									}
							}
					}
				}
			}
		}

	}



/* &expand-match-lambda */
	obj_t BGl_z62expandzd2matchzd2lambdaz62zz__match_expandz00(obj_t
		BgL_envz00_2077, obj_t BgL_expz00_2078)
	{
		{	/* Match/mexpand.scm 74 */
			return BGl_expandzd2matchzd2lambdaz00zz__match_expandz00(BgL_expz00_2078);
		}

	}



/* &<@anonymous:1194> */
	obj_t BGl_z62zc3z04anonymousza31194ze3ze5zz__match_expandz00(obj_t
		BgL_envz00_2079, obj_t BgL_patz00_2081, obj_t BgL_envz00_2082)
	{
		{	/* Match/mexpand.scm 99 */
			{	/* Match/mexpand.scm 100 */
				obj_t BgL_expz00_2080;

				BgL_expz00_2080 = PROCEDURE_L_REF(BgL_envz00_2079, (int) (0L));
				{	/* Match/mexpand.scm 100 */
					obj_t BgL_compiledzd2patzd2_2102;
					obj_t BgL_prototypesz00_2103;

					BgL_compiledzd2patzd2_2102 =
						BGl_pcompilez00zz__match_compilerz00(BgL_patz00_2081);
					BgL_prototypesz00_2103 =
						BGl_fetchzd2prototypeszd2zz__match_expandz00(BgL_patz00_2081);
					{	/* Match/mexpand.scm 105 */
						obj_t BgL_arg1196z00_2104;

						{	/* Match/mexpand.scm 105 */
							obj_t BgL_arg1197z00_2105;
							obj_t BgL_arg1198z00_2106;

							{	/* Match/mexpand.scm 105 */
								obj_t BgL_arg1199z00_2107;

								if (NULLP(BgL_prototypesz00_2103))
									{	/* Match/mexpand.scm 105 */
										BgL_arg1199z00_2107 = BNIL;
									}
								else
									{	/* Match/mexpand.scm 105 */
										obj_t BgL_head1088z00_2108;

										BgL_head1088z00_2108 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1086z00_2110;
											obj_t BgL_tail1089z00_2111;

											BgL_l1086z00_2110 = BgL_prototypesz00_2103;
											BgL_tail1089z00_2111 = BgL_head1088z00_2108;
										BgL_zc3z04anonymousza31201ze3z87_2109:
											if (NULLP(BgL_l1086z00_2110))
												{	/* Match/mexpand.scm 105 */
													BgL_arg1199z00_2107 = CDR(BgL_head1088z00_2108);
												}
											else
												{	/* Match/mexpand.scm 105 */
													obj_t BgL_newtail1090z00_2112;

													{	/* Match/mexpand.scm 105 */
														obj_t BgL_arg1206z00_2113;

														{	/* Match/mexpand.scm 105 */
															obj_t BgL_prototypez00_2114;

															BgL_prototypez00_2114 =
																CAR(((obj_t) BgL_l1086z00_2110));
															{	/* Match/mexpand.scm 107 */
																obj_t BgL_bodyz00_2115;

																{	/* Match/mexpand.scm 107 */
																	obj_t BgL_pairz00_2116;

																	BgL_pairz00_2116 =
																		BGl_assqz00zz__r4_pairs_and_lists_6_3z00(CAR
																		(((obj_t) BgL_prototypez00_2114)),
																		BgL_envz00_2082);
																	BgL_bodyz00_2115 = CDR(BgL_pairz00_2116);
																}
																if (NULLP(BgL_bodyz00_2115))
																	{	/* Match/mexpand.scm 108 */
																		BgL_arg1206z00_2113 =
																			BGl_errorz00zz__errorz00
																			(BGl_symbol1726z00zz__match_expandz00,
																			BGl_string1728z00zz__match_expandz00,
																			BgL_expz00_2080);
																	}
																else
																	{	/* Match/mexpand.scm 110 */
																		obj_t BgL_arg1208z00_2117;
																		obj_t BgL_arg1209z00_2118;

																		BgL_arg1208z00_2117 =
																			CAR(((obj_t) BgL_prototypez00_2114));
																		{	/* Match/mexpand.scm 111 */
																			obj_t BgL_arg1210z00_2119;

																			{	/* Match/mexpand.scm 111 */
																				obj_t BgL_pairz00_2120;

																				BgL_pairz00_2120 =
																					CDR(((obj_t) BgL_prototypez00_2114));
																				BgL_arg1210z00_2119 =
																					CAR(BgL_pairz00_2120);
																			}
																			BgL_arg1209z00_2118 =
																				MAKE_YOUNG_PAIR(BgL_arg1210z00_2119,
																				BgL_bodyz00_2115);
																		}
																		BgL_arg1206z00_2113 =
																			MAKE_YOUNG_PAIR(BgL_arg1208z00_2117,
																			BgL_arg1209z00_2118);
																	}
															}
														}
														BgL_newtail1090z00_2112 =
															MAKE_YOUNG_PAIR(BgL_arg1206z00_2113, BNIL);
													}
													SET_CDR(BgL_tail1089z00_2111,
														BgL_newtail1090z00_2112);
													{	/* Match/mexpand.scm 105 */
														obj_t BgL_arg1203z00_2121;

														BgL_arg1203z00_2121 =
															CDR(((obj_t) BgL_l1086z00_2110));
														{
															obj_t BgL_tail1089z00_2248;
															obj_t BgL_l1086z00_2247;

															BgL_l1086z00_2247 = BgL_arg1203z00_2121;
															BgL_tail1089z00_2248 = BgL_newtail1090z00_2112;
															BgL_tail1089z00_2111 = BgL_tail1089z00_2248;
															BgL_l1086z00_2110 = BgL_l1086z00_2247;
															goto BgL_zc3z04anonymousza31201ze3z87_2109;
														}
													}
												}
										}
									}
								BgL_arg1197z00_2105 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1199z00_2107, BNIL);
							}
							BgL_arg1198z00_2106 =
								MAKE_YOUNG_PAIR(BgL_compiledzd2patzd2_2102, BNIL);
							BgL_arg1196z00_2104 =
								MAKE_YOUNG_PAIR(BgL_arg1197z00_2105, BgL_arg1198z00_2106);
						}
						return
							MAKE_YOUNG_PAIR(BGl_symbol1729z00zz__match_expandz00,
							BgL_arg1196z00_2104);
					}
				}
			}
		}

	}



/* &<@anonymous:1232> */
	obj_t BGl_z62zc3z04anonymousza31232ze3ze5zz__match_expandz00(obj_t
		BgL_envz00_2083, obj_t BgL_patz00_2088, obj_t BgL_envz00_2089)
	{
		{	/* Match/mexpand.scm 92 */
			{	/* Match/mexpand.scm 93 */
				obj_t BgL_patternz00_2084;
				obj_t BgL_tagz00_2085;
				obj_t BgL_actionsz00_2086;
				obj_t BgL_kz00_2087;

				BgL_patternz00_2084 = PROCEDURE_L_REF(BgL_envz00_2083, (int) (0L));
				BgL_tagz00_2085 = PROCEDURE_L_REF(BgL_envz00_2083, (int) (1L));
				BgL_actionsz00_2086 = PROCEDURE_L_REF(BgL_envz00_2083, (int) (2L));
				BgL_kz00_2087 = ((obj_t) PROCEDURE_L_REF(BgL_envz00_2083, (int) (3L)));
				{	/* Match/mexpand.scm 93 */
					obj_t BgL_arg1233z00_2124;
					obj_t BgL_arg1234z00_2125;

					{	/* Match/mexpand.scm 93 */
						obj_t BgL_arg1236z00_2126;

						{	/* Match/mexpand.scm 93 */
							obj_t BgL_arg1238z00_2127;
							obj_t BgL_arg1239z00_2128;

							BgL_arg1238z00_2127 =
								BGl_normaliza7ezd2patternz75zz__match_normaliza7eza7
								(BgL_patternz00_2084);
							{	/* Match/mexpand.scm 93 */
								obj_t BgL_arg1242z00_2129;

								BgL_arg1242z00_2129 = MAKE_YOUNG_PAIR(BgL_patz00_2088, BNIL);
								BgL_arg1239z00_2128 =
									MAKE_YOUNG_PAIR(BgL_tagz00_2085, BgL_arg1242z00_2129);
							}
							BgL_arg1236z00_2126 =
								MAKE_YOUNG_PAIR(BgL_arg1238z00_2127, BgL_arg1239z00_2128);
						}
						BgL_arg1233z00_2124 =
							MAKE_YOUNG_PAIR(BGl_symbol1724z00zz__match_expandz00,
							BgL_arg1236z00_2126);
					}
					{	/* Match/mexpand.scm 131 */
						obj_t BgL_arg1320z00_2130;

						BgL_arg1320z00_2130 =
							MAKE_YOUNG_PAIR(BgL_tagz00_2085, BgL_actionsz00_2086);
						BgL_arg1234z00_2125 =
							MAKE_YOUNG_PAIR(BgL_arg1320z00_2130, BgL_envz00_2089);
					}
					return
						((obj_t(*)(obj_t, obj_t,
								obj_t)) PROCEDURE_L_ENTRY(BgL_kz00_2087)) (BgL_kz00_2087,
						BgL_arg1233z00_2124, BgL_arg1234z00_2125);
				}
			}
		}

	}



/* fetch-prototypes */
	obj_t BGl_fetchzd2prototypeszd2zz__match_expandz00(obj_t BgL_patz00_4)
	{
		{	/* Match/mexpand.scm 115 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
							((obj_t) BgL_patz00_4)), BGl_list1731z00zz__match_expandz00)))
				{	/* Match/mexpand.scm 117 */
					obj_t BgL_arg1252z00_1282;
					obj_t BgL_arg1268z00_1283;

					{	/* Match/mexpand.scm 117 */
						obj_t BgL_arg1272z00_1284;
						obj_t BgL_arg1284z00_1285;

						{	/* Match/mexpand.scm 117 */
							obj_t BgL_pairz00_1798;

							{	/* Match/mexpand.scm 117 */
								obj_t BgL_pairz00_1797;

								BgL_pairz00_1797 = CDR(((obj_t) BgL_patz00_4));
								BgL_pairz00_1798 = CDR(BgL_pairz00_1797);
							}
							BgL_arg1272z00_1284 = CAR(BgL_pairz00_1798);
						}
						{	/* Match/mexpand.scm 117 */
							obj_t BgL_arg1304z00_1286;

							{	/* Match/mexpand.scm 117 */
								obj_t BgL_arg1305z00_1287;

								{	/* Match/mexpand.scm 117 */
									obj_t BgL_pairz00_1802;

									BgL_pairz00_1802 = CDR(((obj_t) BgL_patz00_4));
									BgL_arg1305z00_1287 = CAR(BgL_pairz00_1802);
								}
								BgL_arg1304z00_1286 =
									BGl_patternzd2variableszd2zz__match_descriptionsz00
									(BgL_arg1305z00_1287);
							}
							BgL_arg1284z00_1285 = MAKE_YOUNG_PAIR(BgL_arg1304z00_1286, BNIL);
						}
						BgL_arg1252z00_1282 =
							MAKE_YOUNG_PAIR(BgL_arg1272z00_1284, BgL_arg1284z00_1285);
					}
					{	/* Match/mexpand.scm 118 */
						obj_t BgL_arg1306z00_1288;

						{	/* Match/mexpand.scm 118 */
							obj_t BgL_pairz00_1810;

							{	/* Match/mexpand.scm 118 */
								obj_t BgL_pairz00_1809;

								{	/* Match/mexpand.scm 118 */
									obj_t BgL_pairz00_1808;

									BgL_pairz00_1808 = CDR(((obj_t) BgL_patz00_4));
									BgL_pairz00_1809 = CDR(BgL_pairz00_1808);
								}
								BgL_pairz00_1810 = CDR(BgL_pairz00_1809);
							}
							BgL_arg1306z00_1288 = CAR(BgL_pairz00_1810);
						}
						BgL_arg1268z00_1283 =
							BGl_fetchzd2prototypeszd2zz__match_expandz00(BgL_arg1306z00_1288);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1252z00_1282, BgL_arg1268z00_1283);
				}
			else
				{	/* Match/mexpand.scm 116 */
					return BNIL;
				}
		}

	}



/* expand-match-case */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2matchzd2casez00zz__match_expandz00(obj_t
		BgL_expz00_7)
	{
		{	/* Match/mexpand.scm 126 */
			{	/* Match/mexpand.scm 127 */
				obj_t BgL_arg1312z00_1294;
				obj_t BgL_arg1314z00_1295;

				{	/* Match/mexpand.scm 127 */
					obj_t BgL_arg1317z00_1298;

					{	/* Match/mexpand.scm 127 */
						obj_t BgL_arg1318z00_1299;

						{	/* Match/mexpand.scm 127 */
							obj_t BgL_arg1319z00_1300;

							{	/* Match/mexpand.scm 127 */
								obj_t BgL_pairz00_1822;

								BgL_pairz00_1822 = CDR(((obj_t) BgL_expz00_7));
								BgL_arg1319z00_1300 = CDR(BgL_pairz00_1822);
							}
							BgL_arg1318z00_1299 =
								MAKE_YOUNG_PAIR(BGl_symbol1734z00zz__match_expandz00,
								BgL_arg1319z00_1300);
						}
						if (EPAIRP(BgL_expz00_7))
							{	/* Match/mexpand.scm 123 */
								obj_t BgL_arg1309z00_1824;
								obj_t BgL_arg1310z00_1825;
								obj_t BgL_arg1311z00_1826;

								BgL_arg1309z00_1824 = CAR(BgL_arg1318z00_1299);
								BgL_arg1310z00_1825 = CDR(BgL_arg1318z00_1299);
								BgL_arg1311z00_1826 = CER(((obj_t) BgL_expz00_7));
								{	/* Match/mexpand.scm 123 */
									obj_t BgL_res1713z00_1830;

									BgL_res1713z00_1830 =
										MAKE_YOUNG_EPAIR(BgL_arg1309z00_1824, BgL_arg1310z00_1825,
										BgL_arg1311z00_1826);
									BgL_arg1317z00_1298 = BgL_res1713z00_1830;
								}
							}
						else
							{	/* Match/mexpand.scm 122 */
								BgL_arg1317z00_1298 = BgL_arg1318z00_1299;
							}
					}
					BgL_arg1312z00_1294 =
						BGl_expandzd2matchzd2lambdaz00zz__match_expandz00
						(BgL_arg1317z00_1298);
				}
				{	/* Match/mexpand.scm 128 */
					obj_t BgL_pairz00_1834;

					BgL_pairz00_1834 = CDR(((obj_t) BgL_expz00_7));
					BgL_arg1314z00_1295 = CAR(BgL_pairz00_1834);
				}
				{	/* Match/mexpand.scm 127 */
					obj_t BgL_list1315z00_1296;

					{	/* Match/mexpand.scm 127 */
						obj_t BgL_arg1316z00_1297;

						BgL_arg1316z00_1297 = MAKE_YOUNG_PAIR(BgL_arg1314z00_1295, BNIL);
						BgL_list1315z00_1296 =
							MAKE_YOUNG_PAIR(BgL_arg1312z00_1294, BgL_arg1316z00_1297);
					}
					return BgL_list1315z00_1296;
				}
			}
		}

	}



/* &expand-match-case */
	obj_t BGl_z62expandzd2matchzd2casez62zz__match_expandz00(obj_t
		BgL_envz00_2090, obj_t BgL_expz00_2091)
	{
		{	/* Match/mexpand.scm 126 */
			return BGl_expandzd2matchzd2casez00zz__match_expandz00(BgL_expz00_2091);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__match_expandz00(void)
	{
		{	/* Match/mexpand.scm 26 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__match_compilerz00(108809663L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__match_descriptionsz00(232414635L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(515155867L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(509060701L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1736z00zz__match_expandz00));
		}

	}

#ifdef __cplusplus
}
#endif
