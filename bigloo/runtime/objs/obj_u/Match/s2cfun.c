/*===========================================================================*/
/*   (Match/s2cfun.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Match/s2cfun.scm -indent -o objs/obj_u/Match/s2cfun.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MATCH_S2CFUN_TYPE_DEFINITIONS
#define BGL___MATCH_S2CFUN_TYPE_DEFINITIONS
#endif													// BGL___MATCH_S2CFUN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62concatz62zz__match_s2cfunz00(obj_t, obj_t);
	extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__match_s2cfunz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62ormapz62zz__match_s2cfunz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__match_s2cfunz00(void);
	BGL_EXPORTED_DEF obj_t BGl_jimzd2gensymzd2zz__match_s2cfunz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_concatz00zz__match_s2cfunz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__match_s2cfunz00(void);
	static obj_t BGl_genericzd2initzd2zz__match_s2cfunz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__match_s2cfunz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__match_s2cfunz00(void);
	static obj_t BGl_objectzd2initzd2zz__match_s2cfunz00(void);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62andmapz62zz__match_s2cfunz00(obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31226ze3ze5zz__match_s2cfunz00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__match_s2cfunz00(void);
	BGL_EXPORTED_DECL obj_t BGl_atomzf3zf3zz__match_s2cfunz00(obj_t);
	static obj_t BGl_symbol1727z00zz__match_s2cfunz00 = BUNSPEC;
	static obj_t BGl_symbol1729z00zz__match_s2cfunz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_andmapz00zz__match_s2cfunz00(obj_t, obj_t);
	static obj_t BGl_symbol1731z00zz__match_s2cfunz00 = BUNSPEC;
	extern obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ormapz00zz__match_s2cfunz00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62atomzf3z91zz__match_s2cfunz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_andmapzd2envzd2zz__match_s2cfunz00,
		BgL_bgl_za762andmapza762za7za7__1735z00, va_generic_entry,
		BGl_z62andmapz62zz__match_s2cfunz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_concatzd2envzd2zz__match_s2cfunz00,
		BgL_bgl_za762concatza762za7za7__1736z00, va_generic_entry,
		BGl_z62concatz62zz__match_s2cfunz00, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string1728z00zz__match_s2cfunz00,
		BgL_bgl_string1728za700za7za7_1737za7, "G", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ormapzd2envzd2zz__match_s2cfunz00,
		BgL_bgl_za762ormapza762za7za7__m1738z00, va_generic_entry,
		BGl_z62ormapz62zz__match_s2cfunz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1730z00zz__match_s2cfunz00,
		BgL_bgl_string1730za700za7za7_1739za7, "non-user", 8);
	      DEFINE_STRING(BGl_string1732z00zz__match_s2cfunz00,
		BgL_bgl_string1732za700za7za7_1740za7, "concat", 6);
	      DEFINE_STRING(BGl_string1733z00zz__match_s2cfunz00,
		BgL_bgl_string1733za700za7za7_1741za7, "", 0);
	      DEFINE_STRING(BGl_string1734z00zz__match_s2cfunz00,
		BgL_bgl_string1734za700za7za7_1742za7, "__match_s2cfun", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_atomzf3zd2envz21zz__match_s2cfunz00,
		BgL_bgl_za762atomza7f3za791za7za7_1743za7,
		BGl_z62atomzf3z91zz__match_s2cfunz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__match_s2cfunz00));
		     ADD_ROOT((void *) (&BGl_jimzd2gensymzd2zz__match_s2cfunz00));
		     ADD_ROOT((void *) (&BGl_symbol1727z00zz__match_s2cfunz00));
		     ADD_ROOT((void *) (&BGl_symbol1729z00zz__match_s2cfunz00));
		     ADD_ROOT((void *) (&BGl_symbol1731z00zz__match_s2cfunz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(long
		BgL_checksumz00_2244, char *BgL_fromz00_2245)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__match_s2cfunz00))
				{
					BGl_requirezd2initializa7ationz75zz__match_s2cfunz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__match_s2cfunz00();
					BGl_cnstzd2initzd2zz__match_s2cfunz00();
					BGl_importedzd2moduleszd2initz00zz__match_s2cfunz00();
					return BGl_toplevelzd2initzd2zz__match_s2cfunz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			BGl_symbol1727z00zz__match_s2cfunz00 =
				bstring_to_symbol(BGl_string1728z00zz__match_s2cfunz00);
			BGl_symbol1729z00zz__match_s2cfunz00 =
				bstring_to_symbol(BGl_string1730z00zz__match_s2cfunz00);
			return (BGl_symbol1731z00zz__match_s2cfunz00 =
				bstring_to_symbol(BGl_string1732z00zz__match_s2cfunz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			{	/* Match/s2cfun.scm 66 */
				obj_t BgL_counterz00_2224;

				BgL_counterz00_2224 = MAKE_CELL(BINT(100L));
				{	/* Match/s2cfun.scm 67 */
					obj_t BgL_zc3z04anonymousza31226ze3z87_2219;

					BgL_zc3z04anonymousza31226ze3z87_2219 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31226ze3ze5zz__match_s2cfunz00,
						(int) (-1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31226ze3z87_2219, (int) (0L),
						((obj_t) BgL_counterz00_2224));
					return (BGl_jimzd2gensymzd2zz__match_s2cfunz00 =
						BgL_zc3z04anonymousza31226ze3z87_2219, BUNSPEC);
				}
			}
		}

	}



/* &<@anonymous:1226> */
	obj_t BGl_z62zc3z04anonymousza31226ze3ze5zz__match_s2cfunz00(obj_t
		BgL_envz00_2220, obj_t BgL_argsz00_2222)
	{
		{	/* Match/s2cfun.scm 67 */
			{	/* Match/s2cfun.scm 68 */
				obj_t BgL_counterz00_2221;

				BgL_counterz00_2221 = PROCEDURE_REF(BgL_envz00_2220, (int) (0L));
				{	/* Match/s2cfun.scm 68 */
					obj_t BgL_auxz00_2239;

					if (INTEGERP(CELL_REF(BgL_counterz00_2221)))
						{	/* Match/s2cfun.scm 68 */
							BgL_auxz00_2239 = ADDFX(CELL_REF(BgL_counterz00_2221), BINT(1L));
						}
					else
						{	/* Match/s2cfun.scm 68 */
							BgL_auxz00_2239 =
								BGl_2zb2zb2zz__r4_numbers_6_5z00(CELL_REF(BgL_counterz00_2221),
								BINT(1L));
						}
					CELL_SET(BgL_counterz00_2221, BgL_auxz00_2239);
				}
				{	/* Match/s2cfun.scm 69 */
					obj_t BgL_symbolz00_2240;

					{	/* Match/s2cfun.scm 69 */
						obj_t BgL_arg1227z00_2241;

						if (PAIRP(BgL_argsz00_2222))
							{	/* Match/s2cfun.scm 69 */
								BgL_arg1227z00_2241 = CAR(BgL_argsz00_2222);
							}
						else
							{	/* Match/s2cfun.scm 69 */
								BgL_arg1227z00_2241 = BGl_symbol1727z00zz__match_s2cfunz00;
							}
						{	/* Match/s2cfun.scm 69 */
							obj_t BgL_list1228z00_2242;

							{	/* Match/s2cfun.scm 69 */
								obj_t BgL_arg1229z00_2243;

								BgL_arg1229z00_2243 =
									MAKE_YOUNG_PAIR(CELL_REF(BgL_counterz00_2221), BNIL);
								BgL_list1228z00_2242 =
									MAKE_YOUNG_PAIR(BgL_arg1227z00_2241, BgL_arg1229z00_2243);
							}
							BgL_symbolz00_2240 =
								BGl_concatz00zz__match_s2cfunz00(BgL_list1228z00_2242);
						}
					}
					BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symbolz00_2240,
						BGl_symbol1729z00zz__match_s2cfunz00, BTRUE);
					return BgL_symbolz00_2240;
				}
			}
		}

	}



/* atom? */
	BGL_EXPORTED_DEF obj_t BGl_atomzf3zf3zz__match_s2cfunz00(obj_t BgL_ez00_3)
	{
		{	/* Match/s2cfun.scm 52 */
			if (PAIRP(BgL_ez00_3))
				{	/* Match/s2cfun.scm 53 */
					return BFALSE;
				}
			else
				{	/* Match/s2cfun.scm 53 */
					return BTRUE;
				}
		}

	}



/* &atom? */
	obj_t BGl_z62atomzf3z91zz__match_s2cfunz00(obj_t BgL_envz00_2226,
		obj_t BgL_ez00_2227)
	{
		{	/* Match/s2cfun.scm 52 */
			return BGl_atomzf3zf3zz__match_s2cfunz00(BgL_ez00_2227);
		}

	}



/* concat */
	BGL_EXPORTED_DEF obj_t BGl_concatz00zz__match_s2cfunz00(obj_t BgL_argsz00_4)
	{
		{	/* Match/s2cfun.scm 55 */
			{	/* Match/s2cfun.scm 57 */
				obj_t BgL_arg1236z00_1202;

				{	/* Match/s2cfun.scm 57 */
					obj_t BgL_runner1248z00_1221;

					if (NULLP(BgL_argsz00_4))
						{	/* Match/s2cfun.scm 58 */
							BgL_runner1248z00_1221 = BNIL;
						}
					else
						{	/* Match/s2cfun.scm 58 */
							obj_t BgL_head1094z00_1205;

							BgL_head1094z00_1205 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1092z00_1207;
								obj_t BgL_tail1095z00_1208;

								BgL_l1092z00_1207 = BgL_argsz00_4;
								BgL_tail1095z00_1208 = BgL_head1094z00_1205;
							BgL_zc3z04anonymousza31238ze3z87_1209:
								if (NULLP(BgL_l1092z00_1207))
									{	/* Match/s2cfun.scm 58 */
										BgL_runner1248z00_1221 = CDR(BgL_head1094z00_1205);
									}
								else
									{	/* Match/s2cfun.scm 58 */
										obj_t BgL_newtail1096z00_1211;

										{	/* Match/s2cfun.scm 58 */
											obj_t BgL_arg1244z00_1213;

											{	/* Match/s2cfun.scm 58 */
												obj_t BgL_sz00_1214;

												BgL_sz00_1214 = CAR(((obj_t) BgL_l1092z00_1207));
												if (STRINGP(BgL_sz00_1214))
													{	/* Match/s2cfun.scm 59 */
														BgL_arg1244z00_1213 = BgL_sz00_1214;
													}
												else
													{	/* Match/s2cfun.scm 59 */
														if (SYMBOLP(BgL_sz00_1214))
															{	/* Match/s2cfun.scm 60 */
																obj_t BgL_arg1500z00_1807;

																BgL_arg1500z00_1807 =
																	SYMBOL_TO_STRING(BgL_sz00_1214);
																BgL_arg1244z00_1213 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1500z00_1807);
															}
														else
															{	/* Match/s2cfun.scm 60 */
																if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
																	(BgL_sz00_1214))
																	{	/* Ieee/number.scm 174 */

																		BgL_arg1244z00_1213 =
																			BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																			(BgL_sz00_1214, BINT(10L));
																	}
																else
																	{	/* Match/s2cfun.scm 61 */
																		BgL_arg1244z00_1213 =
																			BGl_errorz00zz__errorz00
																			(BGl_symbol1731z00zz__match_s2cfunz00,
																			BGl_string1733z00zz__match_s2cfunz00,
																			BgL_argsz00_4);
																	}
															}
													}
											}
											BgL_newtail1096z00_1211 =
												MAKE_YOUNG_PAIR(BgL_arg1244z00_1213, BNIL);
										}
										SET_CDR(BgL_tail1095z00_1208, BgL_newtail1096z00_1211);
										{	/* Match/s2cfun.scm 58 */
											obj_t BgL_arg1242z00_1212;

											BgL_arg1242z00_1212 = CDR(((obj_t) BgL_l1092z00_1207));
											{
												obj_t BgL_tail1095z00_2306;
												obj_t BgL_l1092z00_2305;

												BgL_l1092z00_2305 = BgL_arg1242z00_1212;
												BgL_tail1095z00_2306 = BgL_newtail1096z00_1211;
												BgL_tail1095z00_1208 = BgL_tail1095z00_2306;
												BgL_l1092z00_1207 = BgL_l1092z00_2305;
												goto BgL_zc3z04anonymousza31238ze3z87_1209;
											}
										}
									}
							}
						}
					BgL_arg1236z00_1202 =
						BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_runner1248z00_1221);
				}
				return bstring_to_symbol(BgL_arg1236z00_1202);
			}
		}

	}



/* &concat */
	obj_t BGl_z62concatz62zz__match_s2cfunz00(obj_t BgL_envz00_2228,
		obj_t BgL_argsz00_2229)
	{
		{	/* Match/s2cfun.scm 55 */
			return BGl_concatz00zz__match_s2cfunz00(BgL_argsz00_2229);
		}

	}



/* andmap */
	BGL_EXPORTED_DEF obj_t BGl_andmapz00zz__match_s2cfunz00(obj_t BgL_pz00_5,
		obj_t BgL_argsz00_6)
	{
		{	/* Match/s2cfun.scm 74 */
			{
				obj_t BgL_argsz00_1223;
				obj_t BgL_valuez00_1224;

				BgL_argsz00_1223 = BgL_argsz00_6;
				BgL_valuez00_1224 = BTRUE;
			BgL_zc3z04anonymousza31249ze3z87_1225:
				{	/* Match/s2cfun.scm 77 */
					bool_t BgL_test1753z00_2310;

					{
						obj_t BgL_lsz00_1273;

						BgL_lsz00_1273 = BgL_argsz00_1223;
					BgL_zc3z04anonymousza31318ze3z87_1274:
						if (PAIRP(BgL_lsz00_1273))
							{	/* Match/s2cfun.scm 79 */
								bool_t BgL__ortest_1040z00_1276;

								{	/* Match/s2cfun.scm 79 */
									bool_t BgL_test1755z00_2313;

									{	/* Match/s2cfun.scm 79 */
										obj_t BgL_tmpz00_2314;

										BgL_tmpz00_2314 = CAR(BgL_lsz00_1273);
										BgL_test1755z00_2313 = PAIRP(BgL_tmpz00_2314);
									}
									if (BgL_test1755z00_2313)
										{	/* Match/s2cfun.scm 79 */
											BgL__ortest_1040z00_1276 = ((bool_t) 0);
										}
									else
										{	/* Match/s2cfun.scm 79 */
											BgL__ortest_1040z00_1276 = ((bool_t) 1);
										}
								}
								if (BgL__ortest_1040z00_1276)
									{	/* Match/s2cfun.scm 79 */
										BgL_test1753z00_2310 = BgL__ortest_1040z00_1276;
									}
								else
									{
										obj_t BgL_lsz00_2318;

										BgL_lsz00_2318 = CDR(BgL_lsz00_1273);
										BgL_lsz00_1273 = BgL_lsz00_2318;
										goto BgL_zc3z04anonymousza31318ze3z87_1274;
									}
							}
						else
							{	/* Match/s2cfun.scm 78 */
								BgL_test1753z00_2310 = ((bool_t) 0);
							}
					}
					if (BgL_test1753z00_2310)
						{	/* Match/s2cfun.scm 77 */
							return BgL_valuez00_1224;
						}
					else
						{	/* Match/s2cfun.scm 82 */
							obj_t BgL_valuez00_1237;

							{	/* Match/s2cfun.scm 82 */
								obj_t BgL_auxz00_2320;

								if (NULLP(BgL_argsz00_1223))
									{	/* Match/s2cfun.scm 82 */
										BgL_auxz00_2320 = BNIL;
									}
								else
									{	/* Match/s2cfun.scm 82 */
										obj_t BgL_head1099z00_1258;

										{	/* Match/s2cfun.scm 82 */
											obj_t BgL_arg1316z00_1270;

											{	/* Match/s2cfun.scm 82 */
												obj_t BgL_pairz00_1814;

												BgL_pairz00_1814 = CAR(((obj_t) BgL_argsz00_1223));
												BgL_arg1316z00_1270 = CAR(BgL_pairz00_1814);
											}
											BgL_head1099z00_1258 =
												MAKE_YOUNG_PAIR(BgL_arg1316z00_1270, BNIL);
										}
										{	/* Match/s2cfun.scm 82 */
											obj_t BgL_g1102z00_1259;

											BgL_g1102z00_1259 = CDR(((obj_t) BgL_argsz00_1223));
											{
												obj_t BgL_l1097z00_1835;
												obj_t BgL_tail1100z00_1836;

												BgL_l1097z00_1835 = BgL_g1102z00_1259;
												BgL_tail1100z00_1836 = BgL_head1099z00_1258;
											BgL_zc3z04anonymousza31310ze3z87_1834:
												if (NULLP(BgL_l1097z00_1835))
													{	/* Match/s2cfun.scm 82 */
														BgL_auxz00_2320 = BgL_head1099z00_1258;
													}
												else
													{	/* Match/s2cfun.scm 82 */
														obj_t BgL_newtail1101z00_1843;

														{	/* Match/s2cfun.scm 82 */
															obj_t BgL_arg1314z00_1844;

															{	/* Match/s2cfun.scm 82 */
																obj_t BgL_pairz00_1848;

																BgL_pairz00_1848 =
																	CAR(((obj_t) BgL_l1097z00_1835));
																BgL_arg1314z00_1844 = CAR(BgL_pairz00_1848);
															}
															BgL_newtail1101z00_1843 =
																MAKE_YOUNG_PAIR(BgL_arg1314z00_1844, BNIL);
														}
														SET_CDR(BgL_tail1100z00_1836,
															BgL_newtail1101z00_1843);
														{	/* Match/s2cfun.scm 82 */
															obj_t BgL_arg1312z00_1846;

															BgL_arg1312z00_1846 =
																CDR(((obj_t) BgL_l1097z00_1835));
															{
																obj_t BgL_tail1100z00_2339;
																obj_t BgL_l1097z00_2338;

																BgL_l1097z00_2338 = BgL_arg1312z00_1846;
																BgL_tail1100z00_2339 = BgL_newtail1101z00_1843;
																BgL_tail1100z00_1836 = BgL_tail1100z00_2339;
																BgL_l1097z00_1835 = BgL_l1097z00_2338;
																goto BgL_zc3z04anonymousza31310ze3z87_1834;
															}
														}
													}
											}
										}
									}
								BgL_valuez00_1237 = apply(BgL_pz00_5, BgL_auxz00_2320);
							}
							if (CBOOL(BgL_valuez00_1237))
								{	/* Match/s2cfun.scm 83 */
									obj_t BgL_arg1284z00_1239;

									if (NULLP(BgL_argsz00_1223))
										{	/* Match/s2cfun.scm 83 */
											BgL_arg1284z00_1239 = BNIL;
										}
									else
										{	/* Match/s2cfun.scm 83 */
											obj_t BgL_head1105z00_1242;

											{	/* Match/s2cfun.scm 83 */
												obj_t BgL_arg1307z00_1254;

												{	/* Match/s2cfun.scm 83 */
													obj_t BgL_pairz00_1852;

													BgL_pairz00_1852 = CAR(((obj_t) BgL_argsz00_1223));
													BgL_arg1307z00_1254 = CDR(BgL_pairz00_1852);
												}
												BgL_head1105z00_1242 =
													MAKE_YOUNG_PAIR(BgL_arg1307z00_1254, BNIL);
											}
											{	/* Match/s2cfun.scm 83 */
												obj_t BgL_g1108z00_1243;

												BgL_g1108z00_1243 = CDR(((obj_t) BgL_argsz00_1223));
												{
													obj_t BgL_l1103z00_1873;
													obj_t BgL_tail1106z00_1874;

													BgL_l1103z00_1873 = BgL_g1108z00_1243;
													BgL_tail1106z00_1874 = BgL_head1105z00_1242;
												BgL_zc3z04anonymousza31286ze3z87_1872:
													if (NULLP(BgL_l1103z00_1873))
														{	/* Match/s2cfun.scm 83 */
															BgL_arg1284z00_1239 = BgL_head1105z00_1242;
														}
													else
														{	/* Match/s2cfun.scm 83 */
															obj_t BgL_newtail1107z00_1881;

															{	/* Match/s2cfun.scm 83 */
																obj_t BgL_arg1305z00_1882;

																{	/* Match/s2cfun.scm 83 */
																	obj_t BgL_pairz00_1886;

																	BgL_pairz00_1886 =
																		CAR(((obj_t) BgL_l1103z00_1873));
																	BgL_arg1305z00_1882 = CDR(BgL_pairz00_1886);
																}
																BgL_newtail1107z00_1881 =
																	MAKE_YOUNG_PAIR(BgL_arg1305z00_1882, BNIL);
															}
															SET_CDR(BgL_tail1106z00_1874,
																BgL_newtail1107z00_1881);
															{	/* Match/s2cfun.scm 83 */
																obj_t BgL_arg1304z00_1884;

																BgL_arg1304z00_1884 =
																	CDR(((obj_t) BgL_l1103z00_1873));
																{
																	obj_t BgL_tail1106z00_2361;
																	obj_t BgL_l1103z00_2360;

																	BgL_l1103z00_2360 = BgL_arg1304z00_1884;
																	BgL_tail1106z00_2361 =
																		BgL_newtail1107z00_1881;
																	BgL_tail1106z00_1874 = BgL_tail1106z00_2361;
																	BgL_l1103z00_1873 = BgL_l1103z00_2360;
																	goto BgL_zc3z04anonymousza31286ze3z87_1872;
																}
															}
														}
												}
											}
										}
									{
										obj_t BgL_valuez00_2363;
										obj_t BgL_argsz00_2362;

										BgL_argsz00_2362 = BgL_arg1284z00_1239;
										BgL_valuez00_2363 = BgL_valuez00_1237;
										BgL_valuez00_1224 = BgL_valuez00_2363;
										BgL_argsz00_1223 = BgL_argsz00_2362;
										goto BgL_zc3z04anonymousza31249ze3z87_1225;
									}
								}
							else
								{	/* Match/s2cfun.scm 83 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &andmap */
	obj_t BGl_z62andmapz62zz__match_s2cfunz00(obj_t BgL_envz00_2230,
		obj_t BgL_pz00_2231, obj_t BgL_argsz00_2232)
	{
		{	/* Match/s2cfun.scm 74 */
			return BGl_andmapz00zz__match_s2cfunz00(BgL_pz00_2231, BgL_argsz00_2232);
		}

	}



/* ormap */
	BGL_EXPORTED_DEF obj_t BGl_ormapz00zz__match_s2cfunz00(obj_t BgL_pz00_7,
		obj_t BgL_argsz00_8)
	{
		{	/* Match/s2cfun.scm 86 */
			{	/* Match/s2cfun.scm 88 */
				bool_t BgL_test1762z00_2365;

				{	/* Match/s2cfun.scm 88 */

					BgL_test1762z00_2365 = (bgl_list_length(BgL_argsz00_8) == 1L);
				}
				if (BgL_test1762z00_2365)
					{	/* Match/s2cfun.scm 89 */
						obj_t BgL_arg1323z00_1286;

						{	/* Match/s2cfun.scm 89 */
							obj_t BgL_l1111z00_1287;

							BgL_l1111z00_1287 = CAR(((obj_t) BgL_argsz00_8));
							if (NULLP(BgL_l1111z00_1287))
								{	/* Match/s2cfun.scm 89 */
									BgL_arg1323z00_1286 = BNIL;
								}
							else
								{	/* Match/s2cfun.scm 89 */
									obj_t BgL_head1113z00_1289;

									{	/* Match/s2cfun.scm 89 */
										obj_t BgL_arg1331z00_1301;

										{	/* Match/s2cfun.scm 89 */
											obj_t BgL_arg1332z00_1302;

											BgL_arg1332z00_1302 = CAR(((obj_t) BgL_l1111z00_1287));
											BgL_arg1331z00_1301 =
												BGL_PROCEDURE_CALL1(BgL_pz00_7, BgL_arg1332z00_1302);
										}
										BgL_head1113z00_1289 =
											MAKE_YOUNG_PAIR(BgL_arg1331z00_1301, BNIL);
									}
									{	/* Match/s2cfun.scm 89 */
										obj_t BgL_g1116z00_1290;

										BgL_g1116z00_1290 = CDR(((obj_t) BgL_l1111z00_1287));
										{
											obj_t BgL_l1111z00_1911;
											obj_t BgL_tail1114z00_1912;

											BgL_l1111z00_1911 = BgL_g1116z00_1290;
											BgL_tail1114z00_1912 = BgL_head1113z00_1289;
										BgL_zc3z04anonymousza31325ze3z87_1910:
											if (NULLP(BgL_l1111z00_1911))
												{	/* Match/s2cfun.scm 89 */
													BgL_arg1323z00_1286 = BgL_head1113z00_1289;
												}
											else
												{	/* Match/s2cfun.scm 89 */
													obj_t BgL_newtail1115z00_1919;

													{	/* Match/s2cfun.scm 89 */
														obj_t BgL_arg1328z00_1920;

														{	/* Match/s2cfun.scm 89 */
															obj_t BgL_arg1329z00_1921;

															BgL_arg1329z00_1921 =
																CAR(((obj_t) BgL_l1111z00_1911));
															BgL_arg1328z00_1920 =
																BGL_PROCEDURE_CALL1(BgL_pz00_7,
																BgL_arg1329z00_1921);
														}
														BgL_newtail1115z00_1919 =
															MAKE_YOUNG_PAIR(BgL_arg1328z00_1920, BNIL);
													}
													SET_CDR(BgL_tail1114z00_1912,
														BgL_newtail1115z00_1919);
													{	/* Match/s2cfun.scm 89 */
														obj_t BgL_arg1327z00_1922;

														BgL_arg1327z00_1922 =
															CDR(((obj_t) BgL_l1111z00_1911));
														{
															obj_t BgL_tail1114z00_2394;
															obj_t BgL_l1111z00_2393;

															BgL_l1111z00_2393 = BgL_arg1327z00_1922;
															BgL_tail1114z00_2394 = BgL_newtail1115z00_1919;
															BgL_tail1114z00_1912 = BgL_tail1114z00_2394;
															BgL_l1111z00_1911 = BgL_l1111z00_2393;
															goto BgL_zc3z04anonymousza31325ze3z87_1910;
														}
													}
												}
										}
									}
								}
						}
						return
							BGl_memberz00zz__r4_pairs_and_lists_6_3z00(BTRUE,
							BgL_arg1323z00_1286);
					}
				else
					{
						obj_t BgL_argsz00_1304;
						obj_t BgL_valuez00_1305;

						BgL_argsz00_1304 = BgL_argsz00_8;
						BgL_valuez00_1305 = BFALSE;
					BgL_zc3z04anonymousza31333ze3z87_1306:
						{	/* Match/s2cfun.scm 91 */
							bool_t BgL_test1765z00_2396;

							{
								obj_t BgL_lsz00_1354;

								BgL_lsz00_1354 = BgL_argsz00_1304;
							BgL_zc3z04anonymousza31360ze3z87_1355:
								if (PAIRP(BgL_lsz00_1354))
									{	/* Match/s2cfun.scm 93 */
										bool_t BgL__ortest_1043z00_1357;

										{	/* Match/s2cfun.scm 93 */
											bool_t BgL_test1767z00_2399;

											{	/* Match/s2cfun.scm 93 */
												obj_t BgL_tmpz00_2400;

												BgL_tmpz00_2400 = CAR(BgL_lsz00_1354);
												BgL_test1767z00_2399 = PAIRP(BgL_tmpz00_2400);
											}
											if (BgL_test1767z00_2399)
												{	/* Match/s2cfun.scm 93 */
													BgL__ortest_1043z00_1357 = ((bool_t) 0);
												}
											else
												{	/* Match/s2cfun.scm 93 */
													BgL__ortest_1043z00_1357 = ((bool_t) 1);
												}
										}
										if (BgL__ortest_1043z00_1357)
											{	/* Match/s2cfun.scm 93 */
												BgL_test1765z00_2396 = BgL__ortest_1043z00_1357;
											}
										else
											{
												obj_t BgL_lsz00_2404;

												BgL_lsz00_2404 = CDR(BgL_lsz00_1354);
												BgL_lsz00_1354 = BgL_lsz00_2404;
												goto BgL_zc3z04anonymousza31360ze3z87_1355;
											}
									}
								else
									{	/* Match/s2cfun.scm 92 */
										BgL_test1765z00_2396 = ((bool_t) 0);
									}
							}
							if (BgL_test1765z00_2396)
								{	/* Match/s2cfun.scm 91 */
									return BgL_valuez00_1305;
								}
							else
								{	/* Match/s2cfun.scm 96 */
									obj_t BgL_valuez00_1318;

									{	/* Match/s2cfun.scm 96 */
										obj_t BgL_auxz00_2406;

										if (NULLP(BgL_argsz00_1304))
											{	/* Match/s2cfun.scm 96 */
												BgL_auxz00_2406 = BNIL;
											}
										else
											{	/* Match/s2cfun.scm 96 */
												obj_t BgL_head1119z00_1339;

												{	/* Match/s2cfun.scm 96 */
													obj_t BgL_arg1358z00_1351;

													{	/* Match/s2cfun.scm 96 */
														obj_t BgL_pairz00_1929;

														BgL_pairz00_1929 = CAR(((obj_t) BgL_argsz00_1304));
														BgL_arg1358z00_1351 = CAR(BgL_pairz00_1929);
													}
													BgL_head1119z00_1339 =
														MAKE_YOUNG_PAIR(BgL_arg1358z00_1351, BNIL);
												}
												{	/* Match/s2cfun.scm 96 */
													obj_t BgL_g1122z00_1340;

													BgL_g1122z00_1340 = CDR(((obj_t) BgL_argsz00_1304));
													{
														obj_t BgL_l1117z00_1950;
														obj_t BgL_tail1120z00_1951;

														BgL_l1117z00_1950 = BgL_g1122z00_1340;
														BgL_tail1120z00_1951 = BgL_head1119z00_1339;
													BgL_zc3z04anonymousza31351ze3z87_1949:
														if (NULLP(BgL_l1117z00_1950))
															{	/* Match/s2cfun.scm 96 */
																BgL_auxz00_2406 = BgL_head1119z00_1339;
															}
														else
															{	/* Match/s2cfun.scm 96 */
																obj_t BgL_newtail1121z00_1958;

																{	/* Match/s2cfun.scm 96 */
																	obj_t BgL_arg1356z00_1959;

																	{	/* Match/s2cfun.scm 96 */
																		obj_t BgL_pairz00_1963;

																		BgL_pairz00_1963 =
																			CAR(((obj_t) BgL_l1117z00_1950));
																		BgL_arg1356z00_1959 = CAR(BgL_pairz00_1963);
																	}
																	BgL_newtail1121z00_1958 =
																		MAKE_YOUNG_PAIR(BgL_arg1356z00_1959, BNIL);
																}
																SET_CDR(BgL_tail1120z00_1951,
																	BgL_newtail1121z00_1958);
																{	/* Match/s2cfun.scm 96 */
																	obj_t BgL_arg1354z00_1961;

																	BgL_arg1354z00_1961 =
																		CDR(((obj_t) BgL_l1117z00_1950));
																	{
																		obj_t BgL_tail1120z00_2425;
																		obj_t BgL_l1117z00_2424;

																		BgL_l1117z00_2424 = BgL_arg1354z00_1961;
																		BgL_tail1120z00_2425 =
																			BgL_newtail1121z00_1958;
																		BgL_tail1120z00_1951 = BgL_tail1120z00_2425;
																		BgL_l1117z00_1950 = BgL_l1117z00_2424;
																		goto BgL_zc3z04anonymousza31351ze3z87_1949;
																	}
																}
															}
													}
												}
											}
										BgL_valuez00_1318 = apply(BgL_pz00_7, BgL_auxz00_2406);
									}
									if (CBOOL(BgL_valuez00_1318))
										{	/* Match/s2cfun.scm 97 */
											return BgL_valuez00_1318;
										}
									else
										{	/* Match/s2cfun.scm 97 */
											obj_t BgL_arg1340z00_1320;

											if (NULLP(BgL_argsz00_1304))
												{	/* Match/s2cfun.scm 97 */
													BgL_arg1340z00_1320 = BNIL;
												}
											else
												{	/* Match/s2cfun.scm 97 */
													obj_t BgL_head1125z00_1323;

													{	/* Match/s2cfun.scm 97 */
														obj_t BgL_arg1348z00_1335;

														{	/* Match/s2cfun.scm 97 */
															obj_t BgL_pairz00_1967;

															BgL_pairz00_1967 =
																CAR(((obj_t) BgL_argsz00_1304));
															BgL_arg1348z00_1335 = CDR(BgL_pairz00_1967);
														}
														BgL_head1125z00_1323 =
															MAKE_YOUNG_PAIR(BgL_arg1348z00_1335, BNIL);
													}
													{	/* Match/s2cfun.scm 97 */
														obj_t BgL_g1128z00_1324;

														BgL_g1128z00_1324 = CDR(((obj_t) BgL_argsz00_1304));
														{
															obj_t BgL_l1123z00_1988;
															obj_t BgL_tail1126z00_1989;

															BgL_l1123z00_1988 = BgL_g1128z00_1324;
															BgL_tail1126z00_1989 = BgL_head1125z00_1323;
														BgL_zc3z04anonymousza31342ze3z87_1987:
															if (NULLP(BgL_l1123z00_1988))
																{	/* Match/s2cfun.scm 97 */
																	BgL_arg1340z00_1320 = BgL_head1125z00_1323;
																}
															else
																{	/* Match/s2cfun.scm 97 */
																	obj_t BgL_newtail1127z00_1996;

																	{	/* Match/s2cfun.scm 97 */
																		obj_t BgL_arg1346z00_1997;

																		{	/* Match/s2cfun.scm 97 */
																			obj_t BgL_pairz00_2001;

																			BgL_pairz00_2001 =
																				CAR(((obj_t) BgL_l1123z00_1988));
																			BgL_arg1346z00_1997 =
																				CDR(BgL_pairz00_2001);
																		}
																		BgL_newtail1127z00_1996 =
																			MAKE_YOUNG_PAIR(BgL_arg1346z00_1997,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1126z00_1989,
																		BgL_newtail1127z00_1996);
																	{	/* Match/s2cfun.scm 97 */
																		obj_t BgL_arg1344z00_1999;

																		BgL_arg1344z00_1999 =
																			CDR(((obj_t) BgL_l1123z00_1988));
																		{
																			obj_t BgL_tail1126z00_2447;
																			obj_t BgL_l1123z00_2446;

																			BgL_l1123z00_2446 = BgL_arg1344z00_1999;
																			BgL_tail1126z00_2447 =
																				BgL_newtail1127z00_1996;
																			BgL_tail1126z00_1989 =
																				BgL_tail1126z00_2447;
																			BgL_l1123z00_1988 = BgL_l1123z00_2446;
																			goto
																				BgL_zc3z04anonymousza31342ze3z87_1987;
																		}
																	}
																}
														}
													}
												}
											{
												obj_t BgL_valuez00_2449;
												obj_t BgL_argsz00_2448;

												BgL_argsz00_2448 = BgL_arg1340z00_1320;
												BgL_valuez00_2449 = BgL_valuez00_1318;
												BgL_valuez00_1305 = BgL_valuez00_2449;
												BgL_argsz00_1304 = BgL_argsz00_2448;
												goto BgL_zc3z04anonymousza31333ze3z87_1306;
											}
										}
								}
						}
					}
			}
		}

	}



/* &ormap */
	obj_t BGl_z62ormapz62zz__match_s2cfunz00(obj_t BgL_envz00_2233,
		obj_t BgL_pz00_2234, obj_t BgL_argsz00_2235)
	{
		{	/* Match/s2cfun.scm 86 */
			return BGl_ormapz00zz__match_s2cfunz00(BgL_pz00_2234, BgL_argsz00_2235);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__match_s2cfunz00(void)
	{
		{	/* Match/s2cfun.scm 11 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1734z00zz__match_s2cfunz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1734z00zz__match_s2cfunz00));
		}

	}

#ifdef __cplusplus
}
#endif
