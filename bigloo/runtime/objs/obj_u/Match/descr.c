/*===========================================================================*/
/*   (Match/descr.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Match/descr.scm -indent -o objs/obj_u/Match/descr.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MATCH_DESCRIPTIONS_TYPE_DEFINITIONS
#define BGL___MATCH_DESCRIPTIONS_TYPE_DEFINITIONS
#endif													// BGL___MATCH_DESCRIPTIONS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62patternzd2cdrzb0zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2minuszd2zz__match_descriptionsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_alphazd2convertzd2zz__match_descriptionsz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31498ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isOrzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__match_descriptionsz00 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_morezd2precisezf3z21zz__match_descriptionsz00(obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_normz00zz__match_descriptionsz00(obj_t, obj_t);
	extern obj_t BGl_jimzd2gensymzd2zz__match_s2cfunz00;
	static obj_t BGl_z62patternzd2variableszb0zz__match_descriptionsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isAConszf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isTopzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isVectorzd2endzf3z43zz__match_descriptionsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isTimeszf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__match_descriptionsz00(void);
	static obj_t BGl_objectzd2initzd2zz__match_descriptionsz00(void);
	static obj_t BGl_list2201z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_isVarzf3zf3zz__match_descriptionsz00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isCheckzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_list2216z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_extendzd2vectorzd2zz__match_descriptionsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_list2217z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_isSuccesszf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isBottomzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isAnyzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62patternzd2minuszb0zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_z62isTzd2Orzf3z43zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isTreezf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isSuccesszf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__match_descriptionsz00(void);
	static obj_t BGl_list2168z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_list2173z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_isBottomzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isTaggedzd2Orzf3z43zz__match_descriptionsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isNotzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isConszf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62patternzd2pluszb0zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31505ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_list2196z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31319ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62vectorzd2minuszb0zz__match_descriptionsz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62isHolezf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isXConszf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isVectorzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_compatiblezf3zf3zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t BGl_unionz00zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isVarzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isVectorzd2endzf3z21zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62g1050z62zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62g1051z62zz__match_descriptionsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__match_descriptionsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static bool_t BGl_mayzd2bezd2azd2conszd2zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isAnyzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62containsHolezf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_matchz00zz__match_descriptionsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isAndzf3zf3zz__match_descriptionsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isVectorzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31623ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isQuotezf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_symbol2202z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2204z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2209z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31616ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_symbol2211z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2214z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_patternzd2carzd2zz__match_descriptionsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2pluszd2zz__match_descriptionsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_patternzd2minuszd2zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__match_descriptionsz00(void);
	static obj_t BGl_symbol2221z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2223z00zz__match_descriptionsz00 = BUNSPEC;
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__match_descriptionsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__match_descriptionsz00(void);
	static obj_t BGl_z62morezd2precisezf3z43zz__match_descriptionsz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31715ze3ze5zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_comparez00zz__match_descriptionsz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31651ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isNotzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_symbol2162z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2164z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_patternzd2cdrzd2zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62compatiblezf3z91zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2166z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2169z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31482ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62loopz62zz__match_descriptionsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2171z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_patternzd2variableszd2zz__match_descriptionsz00(obj_t);
	static obj_t BGl_symbol2174z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_isXConszf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_symbol2176z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2178z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31483ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31645ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isTzd2Orzf3z21zz__match_descriptionsz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32154ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32155ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32156ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t BGl_rewritezd2notzd2zz__match_descriptionsz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32157ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32158ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2180z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32159ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2182z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2184z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2186z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2188z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_z62isTreezf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31484ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31719ze3ze5zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32160ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze32161ze5zz__match_descriptionsz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2190z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2192z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2194z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2197z00zz__match_descriptionsz00 = BUNSPEC;
	static obj_t BGl_symbol2199z00zz__match_descriptionsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_isTopzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isAConszf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isTimeszf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62extendzd2vectorzb0zz__match_descriptionsz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isOrzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62patternzd2carzb0zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isQuotezf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62vectorzd2pluszb0zz__match_descriptionsz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isConszf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t BGl_z62isAndzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isCheckzf3z91zz__match_descriptionsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isVectorzd2beginzf3z21zz__match_descriptionsz00(obj_t);
	static obj_t BGl_rewritezd2andzd2zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62isVectorzd2beginzf3z43zz__match_descriptionsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isTaggedzd2Orzf3z21zz__match_descriptionsz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31658ze3ze5zz__match_descriptionsz00(obj_t, obj_t);
	extern obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isHolezf3zf3zz__match_descriptionsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_containsHolezf3zf3zz__match_descriptionsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_patternzd2pluszd2zz__match_descriptionsz00(obj_t,
		obj_t);
	static bool_t BGl_isNegationzf3zf3zz__match_descriptionsz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2200z00zz__match_descriptionsz00,
		BgL_bgl_string2200za700za7za7_2226za7, "quote", 5);
	      DEFINE_STRING(BGl_string2203z00zz__match_descriptionsz00,
		BgL_bgl_string2203za700za7za7_2227za7, "acons", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isQuotezf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isquoteza7f3za7912228za7,
		BGl_z62isQuotezf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2205z00zz__match_descriptionsz00,
		BgL_bgl_string2205za700za7za7_2229za7, "xcons", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isAConszf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isaconsza7f3za7912230za7,
		BGl_z62isAConszf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2210z00zz__match_descriptionsz00,
		BgL_bgl_string2210za700za7za7_2231za7, "unbound", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2206z00zz__match_descriptionsz00,
		BgL_bgl_za762za7c3za704anonymo2232za7,
		BGl_z62zc3z04anonymousza31482ze3ze5zz__match_descriptionsz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2212z00zz__match_descriptionsz00,
		BgL_bgl_string2212za700za7za7_2233za7, "tagged-or", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2207z00zz__match_descriptionsz00,
		BgL_bgl_za762za7c3za704anonymo2234za7,
		BGl_z62zc3z04anonymousza31483ze3ze5zz__match_descriptionsz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2213z00zz__match_descriptionsz00,
		BgL_bgl_string2213za700za7za7_2235za7, "VAR-", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2208z00zz__match_descriptionsz00,
		BgL_bgl_za762za7c3za704anonymo2236za7,
		BGl_z62zc3z04anonymousza31484ze3ze5zz__match_descriptionsz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2215z00zz__match_descriptionsz00,
		BgL_bgl_string2215za700za7za7_2237za7, "vector", 6);
	      DEFINE_STRING(BGl_string2220z00zz__match_descriptionsz00,
		BgL_bgl_string2220za700za7za7_2238za7, "ALPHA-", 6);
	      DEFINE_STRING(BGl_string2222z00zz__match_descriptionsz00,
		BgL_bgl_string2222za700za7za7_2239za7, "top", 3);
	      DEFINE_STRING(BGl_string2224z00zz__match_descriptionsz00,
		BgL_bgl_string2224za700za7za7_2240za7, "hole", 4);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2218z00zz__match_descriptionsz00,
		BgL_bgl_za762g1051za762za7za7__m2241z00,
		BGl_z62g1051z62zz__match_descriptionsz00);
	      DEFINE_STRING(BGl_string2225z00zz__match_descriptionsz00,
		BgL_bgl_string2225za700za7za7_2242za7, "__match_descriptions", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2219z00zz__match_descriptionsz00,
		BgL_bgl_za762g1050za762za7za7__m2243z00,
		BGl_z62g1050z62zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2pluszd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762vectorza7d2plus2244z00,
		BGl_z62vectorzd2pluszb0zz__match_descriptionsz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2163z00zz__match_descriptionsz00,
		BgL_bgl_string2163za700za7za7_2245za7, "or", 2);
	      DEFINE_STRING(BGl_string2165z00zz__match_descriptionsz00,
		BgL_bgl_string2165za700za7za7_2246za7, "t-or", 4);
	      DEFINE_STRING(BGl_string2167z00zz__match_descriptionsz00,
		BgL_bgl_string2167za700za7za7_2247za7, "and", 3);
	      DEFINE_STRING(BGl_string2170z00zz__match_descriptionsz00,
		BgL_bgl_string2170za700za7za7_2248za7, "cons", 4);
	      DEFINE_STRING(BGl_string2172z00zz__match_descriptionsz00,
		BgL_bgl_string2172za700za7za7_2249za7, "vector-cons", 11);
	      DEFINE_STRING(BGl_string2175z00zz__match_descriptionsz00,
		BgL_bgl_string2175za700za7za7_2250za7, "tree", 4);
	      DEFINE_STRING(BGl_string2177z00zz__match_descriptionsz00,
		BgL_bgl_string2177za700za7za7_2251za7, "times", 5);
	      DEFINE_STRING(BGl_string2179z00zz__match_descriptionsz00,
		BgL_bgl_string2179za700za7za7_2252za7, "var", 3);
	      DEFINE_STRING(BGl_string2181z00zz__match_descriptionsz00,
		BgL_bgl_string2181za700za7za7_2253za7, "vector-begin", 12);
	      DEFINE_STRING(BGl_string2183z00zz__match_descriptionsz00,
		BgL_bgl_string2183za700za7za7_2254za7, "vector-end", 10);
	      DEFINE_STRING(BGl_string2185z00zz__match_descriptionsz00,
		BgL_bgl_string2185za700za7za7_2255za7, "struct-pat", 10);
	      DEFINE_STRING(BGl_string2187z00zz__match_descriptionsz00,
		BgL_bgl_string2187za700za7za7_2256za7, "any", 3);
	      DEFINE_STRING(BGl_string2189z00zz__match_descriptionsz00,
		BgL_bgl_string2189za700za7za7_2257za7, "check", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patternzd2cdrzd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762patternza7d2cdr2258z00,
		BGl_z62patternzd2cdrzb0zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extendzd2vectorzd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762extendza7d2vect2259z00,
		BGl_z62extendzd2vectorzb0zz__match_descriptionsz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2191z00zz__match_descriptionsz00,
		BgL_bgl_string2191za700za7za7_2260za7, "foo", 3);
	      DEFINE_STRING(BGl_string2193z00zz__match_descriptionsz00,
		BgL_bgl_string2193za700za7za7_2261za7, "bottom", 6);
	      DEFINE_STRING(BGl_string2195z00zz__match_descriptionsz00,
		BgL_bgl_string2195za700za7za7_2262za7, "not", 3);
	      DEFINE_STRING(BGl_string2198z00zz__match_descriptionsz00,
		BgL_bgl_string2198za700za7za7_2263za7, "success", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patternzd2variableszd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762patternza7d2var2264z00,
		BGl_z62patternzd2variableszb0zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_morezd2precisezf3zd2envzf3zz__match_descriptionsz00,
		BgL_bgl_za762moreza7d2precis2265z00,
		BGl_z62morezd2precisezf3z43zz__match_descriptionsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isHolezf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isholeza7f3za791za72266z00,
		BGl_z62isHolezf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isAndzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isandza7f3za791za7za72267za7,
		BGl_z62isAndzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isVectorzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isvectorza7f3za792268za7,
		BGl_z62isVectorzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isCheckzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762ischeckza7f3za7912269za7,
		BGl_z62isCheckzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_compatiblezf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762compatibleza7f32270z00,
		BGl_z62compatiblezf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isBottomzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isbottomza7f3za792271za7,
		BGl_z62isBottomzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isOrzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isorza7f3za791za7za7_2272za7,
		BGl_z62isOrzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isTzd2Orzf3zd2envzf3zz__match_descriptionsz00,
		BgL_bgl_za762istza7d2orza7f3za742273z00,
		BGl_z62isTzd2Orzf3z43zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2minuszd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762vectorza7d2minu2274z00,
		BGl_z62vectorzd2minuszb0zz__match_descriptionsz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isXConszf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isxconsza7f3za7912275za7,
		BGl_z62isXConszf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patternzd2minuszd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762patternza7d2min2276z00,
		BGl_z62patternzd2minuszb0zz__match_descriptionsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isTaggedzd2Orzf3zd2envzf3zz__match_descriptionsz00,
		BgL_bgl_za762istaggedza7d2or2277z00,
		BGl_z62isTaggedzd2Orzf3z43zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isTopzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762istopza7f3za791za7za72278za7,
		BGl_z62isTopzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isAnyzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isanyza7f3za791za7za72279za7,
		BGl_z62isAnyzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isConszf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isconsza7f3za791za72280z00,
		BGl_z62isConszf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isTreezf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762istreeza7f3za791za72281z00,
		BGl_z62isTreezf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isVarzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isvarza7f3za791za7za72282za7,
		BGl_z62isVarzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_containsHolezf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762containsholeza72283z00,
		BGl_z62containsHolezf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isSuccesszf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762issuccessza7f3za72284za7,
		BGl_z62isSuccesszf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patternzd2carzd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762patternza7d2car2285z00,
		BGl_z62patternzd2carzb0zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isVectorzd2endzf3zd2envzf3zz__match_descriptionsz00,
		BgL_bgl_za762isvectorza7d2en2286z00,
		BGl_z62isVectorzd2endzf3z43zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isTimeszf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762istimesza7f3za7912287za7,
		BGl_z62isTimeszf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patternzd2pluszd2envz00zz__match_descriptionsz00,
		BgL_bgl_za762patternza7d2plu2288z00,
		BGl_z62patternzd2pluszb0zz__match_descriptionsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isNotzf3zd2envz21zz__match_descriptionsz00,
		BgL_bgl_za762isnotza7f3za791za7za72289za7,
		BGl_z62isNotzf3z91zz__match_descriptionsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isVectorzd2beginzf3zd2envzf3zz__match_descriptionsz00,
		BgL_bgl_za762isvectorza7d2be2290z00,
		BGl_z62isVectorzd2beginzf3z43zz__match_descriptionsz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_list2201z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_list2216z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_list2217z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_list2168z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_list2173z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_list2196z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2202z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2204z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2209z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2211z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2214z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2221z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2223z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2162z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2164z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2166z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2169z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2171z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2174z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2176z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2178z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2180z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2182z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2184z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2186z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2188z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2190z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2192z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2194z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2197z00zz__match_descriptionsz00));
		     ADD_ROOT((void *) (&BGl_symbol2199z00zz__match_descriptionsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__match_descriptionsz00(long
		BgL_checksumz00_3620, char *BgL_fromz00_3621)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__match_descriptionsz00))
				{
					BGl_requirezd2initializa7ationz75zz__match_descriptionsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__match_descriptionsz00();
					BGl_cnstzd2initzd2zz__match_descriptionsz00();
					BGl_importedzd2moduleszd2initz00zz__match_descriptionsz00();
					return BGl_methodzd2initzd2zz__match_descriptionsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__match_descriptionsz00(void)
	{
		{	/* Match/descr.scm 12 */
			BGl_symbol2162z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2163z00zz__match_descriptionsz00);
			BGl_symbol2164z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2165z00zz__match_descriptionsz00);
			BGl_symbol2166z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2167z00zz__match_descriptionsz00);
			BGl_symbol2169z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2170z00zz__match_descriptionsz00);
			BGl_symbol2171z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2172z00zz__match_descriptionsz00);
			BGl_list2168z00zz__match_descriptionsz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2169z00zz__match_descriptionsz00,
				MAKE_YOUNG_PAIR(BGl_symbol2171z00zz__match_descriptionsz00, BNIL));
			BGl_symbol2174z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2175z00zz__match_descriptionsz00);
			BGl_symbol2176z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2177z00zz__match_descriptionsz00);
			BGl_list2173z00zz__match_descriptionsz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2174z00zz__match_descriptionsz00,
				MAKE_YOUNG_PAIR(BGl_symbol2176z00zz__match_descriptionsz00, BNIL));
			BGl_symbol2178z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2179z00zz__match_descriptionsz00);
			BGl_symbol2180z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2181z00zz__match_descriptionsz00);
			BGl_symbol2182z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2183z00zz__match_descriptionsz00);
			BGl_symbol2184z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2185z00zz__match_descriptionsz00);
			BGl_symbol2186z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2187z00zz__match_descriptionsz00);
			BGl_symbol2188z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2189z00zz__match_descriptionsz00);
			BGl_symbol2190z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2191z00zz__match_descriptionsz00);
			BGl_symbol2192z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2193z00zz__match_descriptionsz00);
			BGl_symbol2194z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2195z00zz__match_descriptionsz00);
			BGl_list2196z00zz__match_descriptionsz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2186z00zz__match_descriptionsz00, BNIL);
			BGl_symbol2197z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2198z00zz__match_descriptionsz00);
			BGl_symbol2199z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2200z00zz__match_descriptionsz00);
			BGl_symbol2202z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2203z00zz__match_descriptionsz00);
			BGl_symbol2204z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2205z00zz__match_descriptionsz00);
			BGl_list2201z00zz__match_descriptionsz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2169z00zz__match_descriptionsz00,
				MAKE_YOUNG_PAIR(BGl_symbol2202z00zz__match_descriptionsz00,
					MAKE_YOUNG_PAIR(BGl_symbol2204z00zz__match_descriptionsz00, BNIL)));
			BGl_symbol2209z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2210z00zz__match_descriptionsz00);
			BGl_symbol2211z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2212z00zz__match_descriptionsz00);
			BGl_symbol2214z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2215z00zz__match_descriptionsz00);
			BGl_list2217z00zz__match_descriptionsz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2169z00zz__match_descriptionsz00,
				MAKE_YOUNG_PAIR(BGl_list2196z00zz__match_descriptionsz00,
					MAKE_YOUNG_PAIR(BGl_list2196z00zz__match_descriptionsz00, BNIL)));
			BGl_list2216z00zz__match_descriptionsz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2194z00zz__match_descriptionsz00,
				MAKE_YOUNG_PAIR(BGl_list2217z00zz__match_descriptionsz00, BNIL));
			BGl_symbol2221z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2222z00zz__match_descriptionsz00);
			return (BGl_symbol2223z00zz__match_descriptionsz00 =
				bstring_to_symbol(BGl_string2224z00zz__match_descriptionsz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__match_descriptionsz00(void)
	{
		{	/* Match/descr.scm 12 */
			return bgl_gc_roots_register();
		}

	}



/* pattern-variables */
	BGL_EXPORTED_DEF obj_t
		BGl_patternzd2variableszd2zz__match_descriptionsz00(obj_t BgL_fz00_3)
	{
		{	/* Match/descr.scm 83 */
		BGl_patternzd2variableszd2zz__match_descriptionsz00:
			if (
				(CAR(
						((obj_t) BgL_fz00_3)) ==
					BGl_symbol2162z00zz__match_descriptionsz00))
				{	/* Match/descr.scm 86 */
					obj_t BgL_arg1201z00_1260;

					{	/* Match/descr.scm 86 */
						obj_t BgL_pairz00_2247;

						BgL_pairz00_2247 = CDR(((obj_t) BgL_fz00_3));
						BgL_arg1201z00_1260 = CAR(BgL_pairz00_2247);
					}
					{
						obj_t BgL_fz00_3675;

						BgL_fz00_3675 = BgL_arg1201z00_1260;
						BgL_fz00_3 = BgL_fz00_3675;
						goto BGl_patternzd2variableszd2zz__match_descriptionsz00;
					}
				}
			else
				{	/* Match/descr.scm 85 */
					if (
						(CAR(
								((obj_t) BgL_fz00_3)) ==
							BGl_symbol2164z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 88 */
							obj_t BgL_arg1206z00_1263;

							{	/* Match/descr.scm 88 */
								obj_t BgL_pairz00_2252;

								BgL_pairz00_2252 = CDR(((obj_t) BgL_fz00_3));
								BgL_arg1206z00_1263 = CAR(BgL_pairz00_2252);
							}
							{
								obj_t BgL_fz00_3683;

								BgL_fz00_3683 = BgL_arg1206z00_1263;
								BgL_fz00_3 = BgL_fz00_3683;
								goto BGl_patternzd2variableszd2zz__match_descriptionsz00;
							}
						}
					else
						{	/* Match/descr.scm 87 */
							if (
								(CAR(
										((obj_t) BgL_fz00_3)) ==
									BGl_symbol2166z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 90 */
									obj_t BgL_arg1210z00_1266;
									obj_t BgL_arg1212z00_1267;

									{	/* Match/descr.scm 90 */
										obj_t BgL_arg1215z00_1268;

										{	/* Match/descr.scm 90 */
											obj_t BgL_pairz00_2257;

											BgL_pairz00_2257 = CDR(((obj_t) BgL_fz00_3));
											BgL_arg1215z00_1268 = CAR(BgL_pairz00_2257);
										}
										BgL_arg1210z00_1266 =
											BGl_patternzd2variableszd2zz__match_descriptionsz00
											(BgL_arg1215z00_1268);
									}
									{	/* Match/descr.scm 90 */
										obj_t BgL_arg1216z00_1269;

										{	/* Match/descr.scm 90 */
											obj_t BgL_pairz00_2263;

											{	/* Match/descr.scm 90 */
												obj_t BgL_pairz00_2262;

												BgL_pairz00_2262 = CDR(((obj_t) BgL_fz00_3));
												BgL_pairz00_2263 = CDR(BgL_pairz00_2262);
											}
											BgL_arg1216z00_1269 = CAR(BgL_pairz00_2263);
										}
										BgL_arg1212z00_1267 =
											BGl_patternzd2variableszd2zz__match_descriptionsz00
											(BgL_arg1216z00_1269);
									}
									return
										BGl_unionz00zz__match_descriptionsz00(BgL_arg1210z00_1266,
										BgL_arg1212z00_1267);
								}
							else
								{	/* Match/descr.scm 89 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
													((obj_t) BgL_fz00_3)),
												BGl_list2168z00zz__match_descriptionsz00)))
										{	/* Match/descr.scm 92 */
											obj_t BgL_arg1220z00_1272;
											obj_t BgL_arg1221z00_1273;

											{	/* Match/descr.scm 92 */
												obj_t BgL_arg1223z00_1274;

												{	/* Match/descr.scm 92 */
													obj_t BgL_pairz00_2268;

													BgL_pairz00_2268 = CDR(((obj_t) BgL_fz00_3));
													BgL_arg1223z00_1274 = CAR(BgL_pairz00_2268);
												}
												BgL_arg1220z00_1272 =
													BGl_patternzd2variableszd2zz__match_descriptionsz00
													(BgL_arg1223z00_1274);
											}
											{	/* Match/descr.scm 92 */
												obj_t BgL_arg1225z00_1275;

												{	/* Match/descr.scm 92 */
													obj_t BgL_pairz00_2274;

													{	/* Match/descr.scm 92 */
														obj_t BgL_pairz00_2273;

														BgL_pairz00_2273 = CDR(((obj_t) BgL_fz00_3));
														BgL_pairz00_2274 = CDR(BgL_pairz00_2273);
													}
													BgL_arg1225z00_1275 = CAR(BgL_pairz00_2274);
												}
												BgL_arg1221z00_1273 =
													BGl_patternzd2variableszd2zz__match_descriptionsz00
													(BgL_arg1225z00_1275);
											}
											return
												BGl_unionz00zz__match_descriptionsz00
												(BgL_arg1220z00_1272, BgL_arg1221z00_1273);
										}
									else
										{	/* Match/descr.scm 91 */
											if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
															((obj_t) BgL_fz00_3)),
														BGl_list2173z00zz__match_descriptionsz00)))
												{	/* Match/descr.scm 94 */
													obj_t BgL_arg1228z00_1278;
													obj_t BgL_arg1229z00_1279;

													{	/* Match/descr.scm 94 */
														obj_t BgL_arg1230z00_1280;

														{	/* Match/descr.scm 94 */
															obj_t BgL_pairz00_2281;

															{	/* Match/descr.scm 94 */
																obj_t BgL_pairz00_2280;

																BgL_pairz00_2280 = CDR(((obj_t) BgL_fz00_3));
																BgL_pairz00_2281 = CDR(BgL_pairz00_2280);
															}
															BgL_arg1230z00_1280 = CAR(BgL_pairz00_2281);
														}
														BgL_arg1228z00_1278 =
															BGl_patternzd2variableszd2zz__match_descriptionsz00
															(BgL_arg1230z00_1280);
													}
													{	/* Match/descr.scm 94 */
														obj_t BgL_arg1231z00_1281;

														{	/* Match/descr.scm 94 */
															obj_t BgL_pairz00_2289;

															{	/* Match/descr.scm 94 */
																obj_t BgL_pairz00_2288;

																{	/* Match/descr.scm 94 */
																	obj_t BgL_pairz00_2287;

																	BgL_pairz00_2287 = CDR(((obj_t) BgL_fz00_3));
																	BgL_pairz00_2288 = CDR(BgL_pairz00_2287);
																}
																BgL_pairz00_2289 = CDR(BgL_pairz00_2288);
															}
															BgL_arg1231z00_1281 = CAR(BgL_pairz00_2289);
														}
														BgL_arg1229z00_1279 =
															BGl_patternzd2variableszd2zz__match_descriptionsz00
															(BgL_arg1231z00_1281);
													}
													return
														BGl_unionz00zz__match_descriptionsz00
														(BgL_arg1228z00_1278, BgL_arg1229z00_1279);
												}
											else
												{	/* Match/descr.scm 93 */
													if (
														(CAR(
																((obj_t) BgL_fz00_3)) ==
															BGl_symbol2178z00zz__match_descriptionsz00))
														{	/* Match/descr.scm 95 */
															return CDR(((obj_t) BgL_fz00_3));
														}
													else
														{	/* Match/descr.scm 95 */
															if (
																(CAR(
																		((obj_t) BgL_fz00_3)) ==
																	BGl_symbol2180z00zz__match_descriptionsz00))
																{	/* Match/descr.scm 98 */
																	obj_t BgL_arg1236z00_1286;

																	{	/* Match/descr.scm 98 */
																		obj_t BgL_pairz00_2298;

																		{	/* Match/descr.scm 98 */
																			obj_t BgL_pairz00_2297;

																			BgL_pairz00_2297 =
																				CDR(((obj_t) BgL_fz00_3));
																			BgL_pairz00_2298 = CDR(BgL_pairz00_2297);
																		}
																		BgL_arg1236z00_1286 = CAR(BgL_pairz00_2298);
																	}
																	{
																		obj_t BgL_fz00_3744;

																		BgL_fz00_3744 = BgL_arg1236z00_1286;
																		BgL_fz00_3 = BgL_fz00_3744;
																		goto
																			BGl_patternzd2variableszd2zz__match_descriptionsz00;
																	}
																}
															else
																{	/* Match/descr.scm 97 */
																	if (
																		(CAR(
																				((obj_t) BgL_fz00_3)) ==
																			BGl_symbol2182z00zz__match_descriptionsz00))
																		{	/* Match/descr.scm 99 */
																			return BNIL;
																		}
																	else
																		{	/* Match/descr.scm 99 */
																			if (
																				(CAR(
																						((obj_t) BgL_fz00_3)) ==
																					BGl_symbol2184z00zz__match_descriptionsz00))
																				{	/* Match/descr.scm 109 */
																					obj_t BgL_g1039z00_1291;

																					{	/* Match/descr.scm 109 */
																						obj_t BgL_pairz00_2306;

																						{	/* Match/descr.scm 109 */
																							obj_t BgL_pairz00_2305;

																							BgL_pairz00_2305 =
																								CDR(((obj_t) BgL_fz00_3));
																							BgL_pairz00_2306 =
																								CDR(BgL_pairz00_2305);
																						}
																						BgL_g1039z00_1291 =
																							CDR(BgL_pairz00_2306);
																					}
																					BGL_TAIL return
																						BGl_loopze70ze7zz__match_descriptionsz00
																						(BgL_g1039z00_1291);
																				}
																			else
																				{	/* Match/descr.scm 101 */
																					return BNIL;
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__match_descriptionsz00(obj_t BgL_pza2za2_1293)
	{
		{	/* Match/descr.scm 109 */
			if (NULLP(BgL_pza2za2_1293))
				{	/* Match/descr.scm 110 */
					return BNIL;
				}
			else
				{	/* Match/descr.scm 112 */
					obj_t BgL_arg1248z00_1296;
					obj_t BgL_arg1249z00_1297;

					{	/* Match/descr.scm 112 */
						obj_t BgL_arg1252z00_1298;

						BgL_arg1252z00_1298 = CAR(((obj_t) BgL_pza2za2_1293));
						BgL_arg1248z00_1296 =
							BGl_patternzd2variableszd2zz__match_descriptionsz00
							(BgL_arg1252z00_1298);
					}
					{	/* Match/descr.scm 113 */
						obj_t BgL_arg1268z00_1299;

						BgL_arg1268z00_1299 = CDR(((obj_t) BgL_pza2za2_1293));
						BgL_arg1249z00_1297 =
							BGl_loopze70ze7zz__match_descriptionsz00(BgL_arg1268z00_1299);
					}
					return
						BGl_unionz00zz__match_descriptionsz00(BgL_arg1248z00_1296,
						BgL_arg1249z00_1297);
				}
		}

	}



/* &pattern-variables */
	obj_t BGl_z62patternzd2variableszb0zz__match_descriptionsz00(obj_t
		BgL_envz00_3357, obj_t BgL_fz00_3358)
	{
		{	/* Match/descr.scm 83 */
			return BGl_patternzd2variableszd2zz__match_descriptionsz00(BgL_fz00_3358);
		}

	}



/* union */
	obj_t BGl_unionz00zz__match_descriptionsz00(obj_t BgL_l1z00_4,
		obj_t BgL_l2z00_5)
	{
		{	/* Match/descr.scm 117 */
		BGl_unionz00zz__match_descriptionsz00:
			if (NULLP(BgL_l1z00_4))
				{	/* Match/descr.scm 118 */
					return BgL_l2z00_5;
				}
			else
				{	/* Match/descr.scm 118 */
					if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00(CAR(
									((obj_t) BgL_l1z00_4)), BgL_l2z00_5)))
						{	/* Match/descr.scm 120 */
							obj_t BgL_arg1314z00_1313;

							BgL_arg1314z00_1313 = CDR(((obj_t) BgL_l1z00_4));
							{
								obj_t BgL_l1z00_3777;

								BgL_l1z00_3777 = BgL_arg1314z00_1313;
								BgL_l1z00_4 = BgL_l1z00_3777;
								goto BGl_unionz00zz__match_descriptionsz00;
							}
						}
					else
						{	/* Match/descr.scm 121 */
							obj_t BgL_arg1315z00_1314;
							obj_t BgL_arg1316z00_1315;

							BgL_arg1315z00_1314 = CAR(((obj_t) BgL_l1z00_4));
							{	/* Match/descr.scm 121 */
								obj_t BgL_arg1317z00_1316;

								BgL_arg1317z00_1316 = CDR(((obj_t) BgL_l1z00_4));
								BgL_arg1316z00_1315 =
									BGl_unionz00zz__match_descriptionsz00(BgL_arg1317z00_1316,
									BgL_l2z00_5);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1315z00_1314, BgL_arg1316z00_1315);
						}
				}
		}

	}



/* pattern-plus */
	BGL_EXPORTED_DEF obj_t BGl_patternzd2pluszd2zz__match_descriptionsz00(obj_t
		BgL_oldz00_9, obj_t BgL_newz00_10)
	{
		{	/* Match/descr.scm 143 */
			{	/* Match/descr.scm 144 */
				bool_t BgL_test2304z00_3784;

				if (
					(CAR(
							((obj_t) BgL_newz00_10)) ==
						BGl_symbol2174z00zz__match_descriptionsz00))
					{	/* Match/descr.scm 144 */
						BgL_test2304z00_3784 = ((bool_t) 1);
					}
				else
					{	/* Match/descr.scm 144 */
						BgL_test2304z00_3784 =
							(CAR(
								((obj_t) BgL_newz00_10)) ==
							BGl_symbol2176z00zz__match_descriptionsz00);
					}
				if (BgL_test2304z00_3784)
					{	/* Match/descr.scm 144 */
						return BgL_oldz00_9;
					}
				else
					{	/* Match/descr.scm 147 */
						bool_t BgL_test2306z00_3792;

						if (
							(CAR(
									((obj_t) BgL_oldz00_9)) ==
								BGl_symbol2186z00zz__match_descriptionsz00))
							{	/* Match/descr.scm 502 */
								BgL_test2306z00_3792 = ((bool_t) 1);
							}
						else
							{	/* Match/descr.scm 502 */
								BgL_test2306z00_3792 =
									(CAR(
										((obj_t) BgL_oldz00_9)) ==
									BGl_symbol2188z00zz__match_descriptionsz00);
							}
						if (BgL_test2306z00_3792)
							{	/* Match/descr.scm 147 */
								return BgL_newz00_10;
							}
						else
							{	/* Match/descr.scm 149 */
								bool_t BgL_test2308z00_3800;

								if (
									(CAR(
											((obj_t) BgL_newz00_10)) ==
										BGl_symbol2186z00zz__match_descriptionsz00))
									{	/* Match/descr.scm 502 */
										BgL_test2308z00_3800 = ((bool_t) 1);
									}
								else
									{	/* Match/descr.scm 502 */
										BgL_test2308z00_3800 =
											(CAR(
												((obj_t) BgL_newz00_10)) ==
											BGl_symbol2188z00zz__match_descriptionsz00);
									}
								if (BgL_test2308z00_3800)
									{	/* Match/descr.scm 149 */
										return BgL_oldz00_9;
									}
								else
									{	/* Match/descr.scm 149 */
										if (BGl_isNegationzf3zf3zz__match_descriptionsz00
											(BgL_oldz00_9))
											{	/* Match/descr.scm 151 */
												BGL_TAIL return
													BGl_normz00zz__match_descriptionsz00(BgL_newz00_10,
													BGl_symbol2190z00zz__match_descriptionsz00);
											}
										else
											{	/* Match/descr.scm 153 */
												obj_t BgL_arg1325z00_1325;

												if (
													(CAR(
															((obj_t) BgL_newz00_10)) ==
														BGl_symbol2178z00zz__match_descriptionsz00))
													{	/* Match/descr.scm 154 */
														obj_t BgL_list1327z00_1327;

														{	/* Match/descr.scm 154 */
															obj_t BgL_arg1328z00_1328;

															{	/* Match/descr.scm 154 */
																obj_t BgL_arg1329z00_1329;

																BgL_arg1329z00_1329 =
																	MAKE_YOUNG_PAIR(BgL_oldz00_9, BNIL);
																BgL_arg1328z00_1328 =
																	MAKE_YOUNG_PAIR(BgL_newz00_10,
																	BgL_arg1329z00_1329);
															}
															BgL_list1327z00_1327 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2166z00zz__match_descriptionsz00,
																BgL_arg1328z00_1328);
														}
														BgL_arg1325z00_1325 = BgL_list1327z00_1327;
													}
												else
													{	/* Match/descr.scm 155 */
														obj_t BgL_list1330z00_1330;

														{	/* Match/descr.scm 155 */
															obj_t BgL_arg1331z00_1331;

															{	/* Match/descr.scm 155 */
																obj_t BgL_arg1332z00_1332;

																BgL_arg1332z00_1332 =
																	MAKE_YOUNG_PAIR(BgL_newz00_10, BNIL);
																BgL_arg1331z00_1331 =
																	MAKE_YOUNG_PAIR(BgL_oldz00_9,
																	BgL_arg1332z00_1332);
															}
															BgL_list1330z00_1330 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2166z00zz__match_descriptionsz00,
																BgL_arg1331z00_1331);
														}
														BgL_arg1325z00_1325 = BgL_list1330z00_1330;
													}
												BGL_TAIL return
													BGl_normz00zz__match_descriptionsz00
													(BgL_arg1325z00_1325,
													BGl_symbol2190z00zz__match_descriptionsz00);
											}
									}
							}
					}
			}
		}

	}



/* &pattern-plus */
	obj_t BGl_z62patternzd2pluszb0zz__match_descriptionsz00(obj_t BgL_envz00_3359,
		obj_t BgL_oldz00_3360, obj_t BgL_newz00_3361)
	{
		{	/* Match/descr.scm 143 */
			return
				BGl_patternzd2pluszd2zz__match_descriptionsz00(BgL_oldz00_3360,
				BgL_newz00_3361);
		}

	}



/* pattern-minus */
	BGL_EXPORTED_DEF obj_t BGl_patternzd2minuszd2zz__match_descriptionsz00(obj_t
		BgL_p1z00_11, obj_t BgL_p2z00_12)
	{
		{	/* Match/descr.scm 157 */
			{	/* Match/descr.scm 158 */
				bool_t BgL_test2312z00_3823;

				{	/* Match/descr.scm 158 */
					bool_t BgL_test2313z00_3824;

					if (BGl_isNegationzf3zf3zz__match_descriptionsz00(BgL_p1z00_11))
						{	/* Match/descr.scm 158 */
							BgL_test2313z00_3824 = ((bool_t) 1);
						}
					else
						{	/* Match/descr.scm 159 */
							bool_t BgL_test2315z00_3827;

							if (
								(CAR(
										((obj_t) BgL_p1z00_11)) ==
									BGl_symbol2186z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 502 */
									BgL_test2315z00_3827 = ((bool_t) 1);
								}
							else
								{	/* Match/descr.scm 502 */
									BgL_test2315z00_3827 =
										(CAR(
											((obj_t) BgL_p1z00_11)) ==
										BGl_symbol2188z00zz__match_descriptionsz00);
								}
							if (BgL_test2315z00_3827)
								{	/* Match/descr.scm 159 */
									BgL_test2313z00_3824 = ((bool_t) 1);
								}
							else
								{	/* Match/descr.scm 159 */
									BgL_test2313z00_3824 =
										(CAR(
											((obj_t) BgL_p1z00_11)) ==
										BGl_symbol2192z00zz__match_descriptionsz00);
								}
						}
					if (BgL_test2313z00_3824)
						{	/* Match/descr.scm 158 */
							if (
								(CAR(
										((obj_t) BgL_p2z00_12)) ==
									BGl_symbol2176z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 161 */
									BgL_test2312z00_3823 = ((bool_t) 1);
								}
							else
								{	/* Match/descr.scm 161 */
									BgL_test2312z00_3823 =
										(CAR(
											((obj_t) BgL_p2z00_12)) ==
										BGl_symbol2174z00zz__match_descriptionsz00);
								}
						}
					else
						{	/* Match/descr.scm 158 */
							BgL_test2312z00_3823 = ((bool_t) 1);
						}
				}
				if (BgL_test2312z00_3823)
					{	/* Match/descr.scm 158 */
						return BgL_p1z00_11;
					}
				else
					{	/* Match/descr.scm 164 */
						bool_t BgL_test2318z00_3845;

						if (
							(CAR(
									((obj_t) BgL_p1z00_11)) ==
								BGl_symbol2186z00zz__match_descriptionsz00))
							{	/* Match/descr.scm 502 */
								BgL_test2318z00_3845 = ((bool_t) 1);
							}
						else
							{	/* Match/descr.scm 502 */
								BgL_test2318z00_3845 =
									(CAR(
										((obj_t) BgL_p1z00_11)) ==
									BGl_symbol2188z00zz__match_descriptionsz00);
							}
						if (BgL_test2318z00_3845)
							{	/* Match/descr.scm 165 */
								obj_t BgL_list1339z00_1342;

								{	/* Match/descr.scm 165 */
									obj_t BgL_arg1340z00_1343;

									BgL_arg1340z00_1343 = MAKE_YOUNG_PAIR(BgL_p2z00_12, BNIL);
									BgL_list1339z00_1342 =
										MAKE_YOUNG_PAIR(BGl_symbol2194z00zz__match_descriptionsz00,
										BgL_arg1340z00_1343);
								}
								return BgL_list1339z00_1342;
							}
						else
							{	/* Match/descr.scm 166 */
								obj_t BgL_arg1341z00_1344;

								{	/* Match/descr.scm 166 */
									obj_t BgL_arg1342z00_1345;

									{	/* Match/descr.scm 166 */
										obj_t BgL_list1347z00_1349;

										{	/* Match/descr.scm 166 */
											obj_t BgL_arg1348z00_1350;

											BgL_arg1348z00_1350 = MAKE_YOUNG_PAIR(BgL_p2z00_12, BNIL);
											BgL_list1347z00_1349 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2194z00zz__match_descriptionsz00,
												BgL_arg1348z00_1350);
										}
										BgL_arg1342z00_1345 = BgL_list1347z00_1349;
									}
									{	/* Match/descr.scm 166 */
										obj_t BgL_list1343z00_1346;

										{	/* Match/descr.scm 166 */
											obj_t BgL_arg1344z00_1347;

											{	/* Match/descr.scm 166 */
												obj_t BgL_arg1346z00_1348;

												BgL_arg1346z00_1348 =
													MAKE_YOUNG_PAIR(BgL_arg1342z00_1345, BNIL);
												BgL_arg1344z00_1347 =
													MAKE_YOUNG_PAIR(BgL_p1z00_11, BgL_arg1346z00_1348);
											}
											BgL_list1343z00_1346 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2166z00zz__match_descriptionsz00,
												BgL_arg1344z00_1347);
										}
										BgL_arg1341z00_1344 = BgL_list1343z00_1346;
									}
								}
								BGL_TAIL return
									BGl_normz00zz__match_descriptionsz00(BgL_arg1341z00_1344,
									BGl_symbol2190z00zz__match_descriptionsz00);
							}
					}
			}
		}

	}



/* &pattern-minus */
	obj_t BGl_z62patternzd2minuszb0zz__match_descriptionsz00(obj_t
		BgL_envz00_3362, obj_t BgL_p1z00_3363, obj_t BgL_p2z00_3364)
	{
		{	/* Match/descr.scm 157 */
			return
				BGl_patternzd2minuszd2zz__match_descriptionsz00(BgL_p1z00_3363,
				BgL_p2z00_3364);
		}

	}



/* isNegation? */
	bool_t BGl_isNegationzf3zf3zz__match_descriptionsz00(obj_t BgL_cz00_13)
	{
		{	/* Match/descr.scm 172 */
			{	/* Match/descr.scm 173 */
				bool_t BgL__ortest_1040z00_1355;

				if (
					(CAR(
							((obj_t) BgL_cz00_13)) ==
						BGl_symbol2166z00zz__match_descriptionsz00))
					{	/* Match/descr.scm 174 */
						obj_t BgL_arg1350z00_1358;

						{	/* Match/descr.scm 174 */
							obj_t BgL_pairz00_2356;

							BgL_pairz00_2356 = CDR(((obj_t) BgL_cz00_13));
							BgL_arg1350z00_1358 = CAR(BgL_pairz00_2356);
						}
						BgL__ortest_1040z00_1355 =
							BGl_isNegationzf3zf3zz__match_descriptionsz00
							(BgL_arg1350z00_1358);
					}
				else
					{	/* Match/descr.scm 173 */
						BgL__ortest_1040z00_1355 = ((bool_t) 0);
					}
				if (BgL__ortest_1040z00_1355)
					{	/* Match/descr.scm 173 */
						return BgL__ortest_1040z00_1355;
					}
				else
					{	/* Match/descr.scm 173 */
						return
							(CAR(
								((obj_t) BgL_cz00_13)) ==
							BGl_symbol2194z00zz__match_descriptionsz00);
					}
			}
		}

	}



/* pattern-car */
	BGL_EXPORTED_DEF obj_t BGl_patternzd2carzd2zz__match_descriptionsz00(obj_t
		BgL_cz00_14)
	{
		{	/* Match/descr.scm 177 */
			if (
				(CAR(
						((obj_t) BgL_cz00_14)) ==
					BGl_symbol2169z00zz__match_descriptionsz00))
				{	/* Match/descr.scm 179 */
					obj_t BgL_pairz00_2364;

					BgL_pairz00_2364 = CDR(((obj_t) BgL_cz00_14));
					return CAR(BgL_pairz00_2364);
				}
			else
				{	/* Match/descr.scm 178 */
					return BGl_list2196z00zz__match_descriptionsz00;
				}
		}

	}



/* &pattern-car */
	obj_t BGl_z62patternzd2carzb0zz__match_descriptionsz00(obj_t BgL_envz00_3365,
		obj_t BgL_cz00_3366)
	{
		{	/* Match/descr.scm 177 */
			return BGl_patternzd2carzd2zz__match_descriptionsz00(BgL_cz00_3366);
		}

	}



/* pattern-cdr */
	BGL_EXPORTED_DEF obj_t BGl_patternzd2cdrzd2zz__match_descriptionsz00(obj_t
		BgL_cz00_15)
	{
		{	/* Match/descr.scm 182 */
			if (
				(CAR(
						((obj_t) BgL_cz00_15)) ==
					BGl_symbol2169z00zz__match_descriptionsz00))
				{	/* Match/descr.scm 184 */
					obj_t BgL_pairz00_2373;

					{	/* Match/descr.scm 184 */
						obj_t BgL_pairz00_2372;

						BgL_pairz00_2372 = CDR(((obj_t) BgL_cz00_15));
						BgL_pairz00_2373 = CDR(BgL_pairz00_2372);
					}
					return CAR(BgL_pairz00_2373);
				}
			else
				{	/* Match/descr.scm 183 */
					return BGl_list2196z00zz__match_descriptionsz00;
				}
		}

	}



/* &pattern-cdr */
	obj_t BGl_z62patternzd2cdrzb0zz__match_descriptionsz00(obj_t BgL_envz00_3367,
		obj_t BgL_cz00_3368)
	{
		{	/* Match/descr.scm 182 */
			return BGl_patternzd2cdrzd2zz__match_descriptionsz00(BgL_cz00_3368);
		}

	}



/* norm */
	obj_t BGl_normz00zz__match_descriptionsz00(obj_t BgL_cz00_17,
		obj_t BgL_ancz00_18)
	{
		{	/* Match/descr.scm 212 */
		BGl_normz00zz__match_descriptionsz00:
			if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_ancz00_18, BgL_cz00_17))
				{	/* Match/descr.scm 215 */
					return BgL_ancz00_18;
				}
			else
				{	/* Match/descr.scm 215 */
					if (
						(CAR(
								((obj_t) BgL_cz00_17)) ==
							BGl_symbol2194z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 216 */
							obj_t BgL_arg1362z00_1369;

							{	/* Match/descr.scm 216 */
								obj_t BgL_pairz00_2378;

								BgL_pairz00_2378 = CDR(((obj_t) BgL_cz00_17));
								BgL_arg1362z00_1369 = CAR(BgL_pairz00_2378);
							}
							{	/* Match/descr.scm 222 */
								obj_t BgL_arg1375z00_2379;
								obj_t BgL_arg1376z00_2380;

								BgL_arg1375z00_2379 =
									BGl_rewritezd2notzd2zz__match_descriptionsz00
									(BgL_arg1362z00_1369);
								{	/* Match/descr.scm 222 */
									obj_t BgL_list1377z00_2381;

									{	/* Match/descr.scm 222 */
										obj_t BgL_arg1378z00_2382;

										BgL_arg1378z00_2382 =
											MAKE_YOUNG_PAIR(BgL_arg1362z00_1369, BNIL);
										BgL_list1377z00_2381 =
											MAKE_YOUNG_PAIR
											(BGl_symbol2194z00zz__match_descriptionsz00,
											BgL_arg1378z00_2382);
									}
									BgL_arg1376z00_2380 = BgL_list1377z00_2381;
								}
								{
									obj_t BgL_ancz00_3904;
									obj_t BgL_cz00_3903;

									BgL_cz00_3903 = BgL_arg1375z00_2379;
									BgL_ancz00_3904 = BgL_arg1376z00_2380;
									BgL_ancz00_18 = BgL_ancz00_3904;
									BgL_cz00_17 = BgL_cz00_3903;
									goto BGl_normz00zz__match_descriptionsz00;
								}
							}
						}
					else
						{	/* Match/descr.scm 216 */
							if (
								(CAR(
										((obj_t) BgL_cz00_17)) ==
									BGl_symbol2166z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 217 */
									obj_t BgL_arg1365z00_1372;
									obj_t BgL_arg1366z00_1373;

									{	/* Match/descr.scm 217 */
										obj_t BgL_pairz00_2388;

										BgL_pairz00_2388 = CDR(((obj_t) BgL_cz00_17));
										BgL_arg1365z00_1372 = CAR(BgL_pairz00_2388);
									}
									{	/* Match/descr.scm 217 */
										obj_t BgL_pairz00_2394;

										{	/* Match/descr.scm 217 */
											obj_t BgL_pairz00_2393;

											BgL_pairz00_2393 = CDR(((obj_t) BgL_cz00_17));
											BgL_pairz00_2394 = CDR(BgL_pairz00_2393);
										}
										BgL_arg1366z00_1373 = CAR(BgL_pairz00_2394);
									}
									{	/* Match/descr.scm 232 */
										obj_t BgL_arg1388z00_2395;
										obj_t BgL_arg1389z00_2396;

										BgL_arg1388z00_2395 =
											BGl_rewritezd2andzd2zz__match_descriptionsz00
											(BgL_arg1365z00_1372, BgL_arg1366z00_1373);
										{	/* Match/descr.scm 232 */
											obj_t BgL_list1390z00_2397;

											{	/* Match/descr.scm 232 */
												obj_t BgL_arg1391z00_2398;

												{	/* Match/descr.scm 232 */
													obj_t BgL_arg1392z00_2399;

													BgL_arg1392z00_2399 =
														MAKE_YOUNG_PAIR(BgL_arg1366z00_1373, BNIL);
													BgL_arg1391z00_2398 =
														MAKE_YOUNG_PAIR(BgL_arg1365z00_1372,
														BgL_arg1392z00_2399);
												}
												BgL_list1390z00_2397 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2166z00zz__match_descriptionsz00,
													BgL_arg1391z00_2398);
											}
											BgL_arg1389z00_2396 = BgL_list1390z00_2397;
										}
										{
											obj_t BgL_ancz00_3921;
											obj_t BgL_cz00_3920;

											BgL_cz00_3920 = BgL_arg1388z00_2395;
											BgL_ancz00_3921 = BgL_arg1389z00_2396;
											BgL_ancz00_18 = BgL_ancz00_3921;
											BgL_cz00_17 = BgL_cz00_3920;
											goto BGl_normz00zz__match_descriptionsz00;
										}
									}
								}
							else
								{	/* Match/descr.scm 217 */
									if (
										(CAR(
												((obj_t) BgL_cz00_17)) ==
											BGl_symbol2169z00zz__match_descriptionsz00))
										{	/* Match/descr.scm 218 */
											obj_t BgL_arg1369z00_1376;
											obj_t BgL_arg1370z00_1377;

											{	/* Match/descr.scm 218 */
												obj_t BgL_pairz00_2405;

												BgL_pairz00_2405 = CDR(((obj_t) BgL_cz00_17));
												BgL_arg1369z00_1376 = CAR(BgL_pairz00_2405);
											}
											{	/* Match/descr.scm 218 */
												obj_t BgL_pairz00_2411;

												{	/* Match/descr.scm 218 */
													obj_t BgL_pairz00_2410;

													BgL_pairz00_2410 = CDR(((obj_t) BgL_cz00_17));
													BgL_pairz00_2411 = CDR(BgL_pairz00_2410);
												}
												BgL_arg1370z00_1377 = CAR(BgL_pairz00_2411);
											}
											{	/* Match/descr.scm 251 */
												obj_t BgL_arg1431z00_2412;
												obj_t BgL_arg1434z00_2413;

												BgL_arg1431z00_2412 =
													BGl_normz00zz__match_descriptionsz00
													(BgL_arg1369z00_1376,
													BGl_symbol2190z00zz__match_descriptionsz00);
												BgL_arg1434z00_2413 =
													BGl_normz00zz__match_descriptionsz00
													(BgL_arg1370z00_1377,
													BGl_symbol2190z00zz__match_descriptionsz00);
												{	/* Match/descr.scm 251 */
													obj_t BgL_list1435z00_2414;

													{	/* Match/descr.scm 251 */
														obj_t BgL_arg1436z00_2415;

														{	/* Match/descr.scm 251 */
															obj_t BgL_arg1437z00_2416;

															BgL_arg1437z00_2416 =
																MAKE_YOUNG_PAIR(BgL_arg1434z00_2413, BNIL);
															BgL_arg1436z00_2415 =
																MAKE_YOUNG_PAIR(BgL_arg1431z00_2412,
																BgL_arg1437z00_2416);
														}
														BgL_list1435z00_2414 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2169z00zz__match_descriptionsz00,
															BgL_arg1436z00_2415);
													}
													return BgL_list1435z00_2414;
												}
											}
										}
									else
										{	/* Match/descr.scm 218 */
											return BgL_cz00_17;
										}
								}
						}
				}
		}

	}



/* rewrite-not */
	obj_t BGl_rewritezd2notzd2zz__match_descriptionsz00(obj_t BgL_cz00_20)
	{
		{	/* Match/descr.scm 224 */
			if (
				(CAR(
						((obj_t) BgL_cz00_20)) ==
					BGl_symbol2194z00zz__match_descriptionsz00))
				{	/* Match/descr.scm 227 */
					obj_t BgL_pairz00_2427;

					BgL_pairz00_2427 = CDR(((obj_t) BgL_cz00_20));
					return CAR(BgL_pairz00_2427);
				}
			else
				{	/* Match/descr.scm 228 */
					obj_t BgL_arg1382z00_1387;

					BgL_arg1382z00_1387 =
						BGl_normz00zz__match_descriptionsz00(BgL_cz00_20,
						BGl_symbol2190z00zz__match_descriptionsz00);
					{	/* Match/descr.scm 228 */
						obj_t BgL_list1383z00_1388;

						{	/* Match/descr.scm 228 */
							obj_t BgL_arg1384z00_1389;

							BgL_arg1384z00_1389 = MAKE_YOUNG_PAIR(BgL_arg1382z00_1387, BNIL);
							BgL_list1383z00_1388 =
								MAKE_YOUNG_PAIR(BGl_symbol2194z00zz__match_descriptionsz00,
								BgL_arg1384z00_1389);
						}
						return BgL_list1383z00_1388;
					}
				}
		}

	}



/* rewrite-and */
	obj_t BGl_rewritezd2andzd2zz__match_descriptionsz00(obj_t BgL_c1z00_23,
		obj_t BgL_c2z00_24)
	{
		{	/* Match/descr.scm 234 */
			if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_c1z00_23, BgL_c2z00_24))
				{	/* Match/descr.scm 236 */
					return BgL_c1z00_23;
				}
			else
				{	/* Match/descr.scm 236 */
					if (
						(CAR(
								((obj_t) BgL_c1z00_23)) ==
							BGl_symbol2166z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 239 */
							obj_t BgL_arg1396z00_1399;
							obj_t BgL_arg1397z00_1400;

							{	/* Match/descr.scm 239 */
								obj_t BgL_pairz00_2439;

								BgL_pairz00_2439 = CDR(((obj_t) BgL_c1z00_23));
								BgL_arg1396z00_1399 = CAR(BgL_pairz00_2439);
							}
							{	/* Match/descr.scm 241 */
								obj_t BgL_arg1401z00_1404;

								{	/* Match/descr.scm 241 */
									obj_t BgL_pairz00_2445;

									{	/* Match/descr.scm 241 */
										obj_t BgL_pairz00_2444;

										BgL_pairz00_2444 = CDR(((obj_t) BgL_c1z00_23));
										BgL_pairz00_2445 = CDR(BgL_pairz00_2444);
									}
									BgL_arg1401z00_1404 = CAR(BgL_pairz00_2445);
								}
								{	/* Match/descr.scm 240 */
									obj_t BgL_list1402z00_1405;

									{	/* Match/descr.scm 240 */
										obj_t BgL_arg1403z00_1406;

										{	/* Match/descr.scm 240 */
											obj_t BgL_arg1404z00_1407;

											BgL_arg1404z00_1407 = MAKE_YOUNG_PAIR(BgL_c2z00_24, BNIL);
											BgL_arg1403z00_1406 =
												MAKE_YOUNG_PAIR(BgL_arg1401z00_1404,
												BgL_arg1404z00_1407);
										}
										BgL_list1402z00_1405 =
											MAKE_YOUNG_PAIR
											(BGl_symbol2166z00zz__match_descriptionsz00,
											BgL_arg1403z00_1406);
									}
									BgL_arg1397z00_1400 = BgL_list1402z00_1405;
								}
							}
							{	/* Match/descr.scm 238 */
								obj_t BgL_list1398z00_1401;

								{	/* Match/descr.scm 238 */
									obj_t BgL_arg1399z00_1402;

									{	/* Match/descr.scm 238 */
										obj_t BgL_arg1400z00_1403;

										BgL_arg1400z00_1403 =
											MAKE_YOUNG_PAIR(BgL_arg1397z00_1400, BNIL);
										BgL_arg1399z00_1402 =
											MAKE_YOUNG_PAIR(BgL_arg1396z00_1399, BgL_arg1400z00_1403);
									}
									BgL_list1398z00_1401 =
										MAKE_YOUNG_PAIR(BGl_symbol2166z00zz__match_descriptionsz00,
										BgL_arg1399z00_1402);
								}
								return BgL_list1398z00_1401;
							}
						}
					else
						{	/* Match/descr.scm 243 */
							bool_t BgL_test2331z00_3967;

							if (
								(CAR(
										((obj_t) BgL_c1z00_23)) ==
									BGl_symbol2169z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 243 */
									BgL_test2331z00_3967 =
										(CAR(
											((obj_t) BgL_c2z00_24)) ==
										BGl_symbol2169z00zz__match_descriptionsz00);
								}
							else
								{	/* Match/descr.scm 243 */
									BgL_test2331z00_3967 = ((bool_t) 0);
								}
							if (BgL_test2331z00_3967)
								{	/* Match/descr.scm 246 */
									obj_t BgL_arg1410z00_1413;
									obj_t BgL_arg1411z00_1414;

									{	/* Match/descr.scm 246 */
										obj_t BgL_arg1415z00_1418;
										obj_t BgL_arg1416z00_1419;

										{	/* Match/descr.scm 246 */
											obj_t BgL_pairz00_2453;

											BgL_pairz00_2453 = CDR(((obj_t) BgL_c1z00_23));
											BgL_arg1415z00_1418 = CAR(BgL_pairz00_2453);
										}
										{	/* Match/descr.scm 246 */
											obj_t BgL_pairz00_2457;

											BgL_pairz00_2457 = CDR(((obj_t) BgL_c2z00_24));
											BgL_arg1416z00_1419 = CAR(BgL_pairz00_2457);
										}
										{	/* Match/descr.scm 246 */
											obj_t BgL_list1417z00_1420;

											{	/* Match/descr.scm 246 */
												obj_t BgL_arg1418z00_1421;

												{	/* Match/descr.scm 246 */
													obj_t BgL_arg1419z00_1422;

													BgL_arg1419z00_1422 =
														MAKE_YOUNG_PAIR(BgL_arg1416z00_1419, BNIL);
													BgL_arg1418z00_1421 =
														MAKE_YOUNG_PAIR(BgL_arg1415z00_1418,
														BgL_arg1419z00_1422);
												}
												BgL_list1417z00_1420 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2166z00zz__match_descriptionsz00,
													BgL_arg1418z00_1421);
											}
											BgL_arg1410z00_1413 = BgL_list1417z00_1420;
										}
									}
									{	/* Match/descr.scm 247 */
										obj_t BgL_arg1420z00_1423;
										obj_t BgL_arg1421z00_1424;

										{	/* Match/descr.scm 247 */
											obj_t BgL_pairz00_2464;

											{	/* Match/descr.scm 247 */
												obj_t BgL_pairz00_2463;

												BgL_pairz00_2463 = CDR(((obj_t) BgL_c1z00_23));
												BgL_pairz00_2464 = CDR(BgL_pairz00_2463);
											}
											BgL_arg1420z00_1423 = CAR(BgL_pairz00_2464);
										}
										{	/* Match/descr.scm 247 */
											obj_t BgL_pairz00_2470;

											{	/* Match/descr.scm 247 */
												obj_t BgL_pairz00_2469;

												BgL_pairz00_2469 = CDR(((obj_t) BgL_c2z00_24));
												BgL_pairz00_2470 = CDR(BgL_pairz00_2469);
											}
											BgL_arg1421z00_1424 = CAR(BgL_pairz00_2470);
										}
										{	/* Match/descr.scm 247 */
											obj_t BgL_list1422z00_1425;

											{	/* Match/descr.scm 247 */
												obj_t BgL_arg1423z00_1426;

												{	/* Match/descr.scm 247 */
													obj_t BgL_arg1424z00_1427;

													BgL_arg1424z00_1427 =
														MAKE_YOUNG_PAIR(BgL_arg1421z00_1424, BNIL);
													BgL_arg1423z00_1426 =
														MAKE_YOUNG_PAIR(BgL_arg1420z00_1423,
														BgL_arg1424z00_1427);
												}
												BgL_list1422z00_1425 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2166z00zz__match_descriptionsz00,
													BgL_arg1423z00_1426);
											}
											BgL_arg1411z00_1414 = BgL_list1422z00_1425;
										}
									}
									{	/* Match/descr.scm 245 */
										obj_t BgL_list1412z00_1415;

										{	/* Match/descr.scm 245 */
											obj_t BgL_arg1413z00_1416;

											{	/* Match/descr.scm 245 */
												obj_t BgL_arg1414z00_1417;

												BgL_arg1414z00_1417 =
													MAKE_YOUNG_PAIR(BgL_arg1411z00_1414, BNIL);
												BgL_arg1413z00_1416 =
													MAKE_YOUNG_PAIR(BgL_arg1410z00_1413,
													BgL_arg1414z00_1417);
											}
											BgL_list1412z00_1415 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2169z00zz__match_descriptionsz00,
												BgL_arg1413z00_1416);
										}
										return BgL_list1412z00_1415;
									}
								}
							else
								{	/* Match/descr.scm 248 */
									obj_t BgL_list1425z00_1428;

									{	/* Match/descr.scm 248 */
										obj_t BgL_arg1426z00_1429;

										{	/* Match/descr.scm 248 */
											obj_t BgL_arg1427z00_1430;

											BgL_arg1427z00_1430 = MAKE_YOUNG_PAIR(BgL_c2z00_24, BNIL);
											BgL_arg1426z00_1429 =
												MAKE_YOUNG_PAIR(BgL_c1z00_23, BgL_arg1427z00_1430);
										}
										BgL_list1425z00_1428 =
											MAKE_YOUNG_PAIR
											(BGl_symbol2166z00zz__match_descriptionsz00,
											BgL_arg1426z00_1429);
									}
									return BgL_list1425z00_1428;
								}
						}
				}
		}

	}



/* more-precise? */
	BGL_EXPORTED_DEF obj_t BGl_morezd2precisezf3z21zz__match_descriptionsz00(obj_t
		BgL_descrz00_27, obj_t BgL_fz00_28)
	{
		{	/* Match/descr.scm 263 */
		BGl_morezd2precisezf3z21zz__match_descriptionsz00:
			{	/* Match/descr.scm 265 */
				bool_t BgL_test2333z00_4001;

				if (
					(CAR(
							((obj_t) BgL_descrz00_27)) ==
						BGl_symbol2186z00zz__match_descriptionsz00))
					{	/* Match/descr.scm 502 */
						BgL_test2333z00_4001 = ((bool_t) 1);
					}
				else
					{	/* Match/descr.scm 502 */
						BgL_test2333z00_4001 =
							(CAR(
								((obj_t) BgL_descrz00_27)) ==
							BGl_symbol2188z00zz__match_descriptionsz00);
					}
				if (BgL_test2333z00_4001)
					{	/* Match/descr.scm 265 */
						return BFALSE;
					}
				else
					{	/* Match/descr.scm 265 */
						if (
							(CAR(
									((obj_t) BgL_fz00_28)) ==
								BGl_symbol2186z00zz__match_descriptionsz00))
							{	/* Match/descr.scm 267 */
								return BTRUE;
							}
						else
							{	/* Match/descr.scm 267 */
								if (
									(CAR(
											((obj_t) BgL_fz00_28)) ==
										BGl_symbol2197z00zz__match_descriptionsz00))
									{	/* Match/descr.scm 270 */
										return BFALSE;
									}
								else
									{	/* Match/descr.scm 270 */
										if (
											(CAR(
													((obj_t) BgL_fz00_28)) ==
												BGl_symbol2199z00zz__match_descriptionsz00))
											{	/* Match/descr.scm 273 */
												if (
													(CAR(
															((obj_t) BgL_descrz00_27)) ==
														BGl_symbol2199z00zz__match_descriptionsz00))
													{	/* Match/descr.scm 275 */
														bool_t BgL_tmpz00_4025;

														{	/* Match/descr.scm 275 */
															obj_t BgL_auxz00_4030;
															obj_t BgL_auxz00_4026;

															{	/* Match/descr.scm 275 */
																obj_t BgL_pairz00_2500;

																BgL_pairz00_2500 = CDR(((obj_t) BgL_fz00_28));
																BgL_auxz00_4030 = CAR(BgL_pairz00_2500);
															}
															{	/* Match/descr.scm 275 */
																obj_t BgL_pairz00_2496;

																BgL_pairz00_2496 =
																	CDR(((obj_t) BgL_descrz00_27));
																BgL_auxz00_4026 = CAR(BgL_pairz00_2496);
															}
															BgL_tmpz00_4025 =
																BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																(BgL_auxz00_4026, BgL_auxz00_4030);
														}
														return BBOOL(BgL_tmpz00_4025);
													}
												else
													{	/* Match/descr.scm 274 */
														return BFALSE;
													}
											}
										else
											{	/* Match/descr.scm 273 */
												if (
													(CAR(
															((obj_t) BgL_fz00_28)) ==
														BGl_symbol2166z00zz__match_descriptionsz00))
													{	/* Match/descr.scm 278 */
														obj_t BgL__andtest_1043z00_1452;

														{	/* Match/descr.scm 278 */
															obj_t BgL_arg1450z00_1454;

															{	/* Match/descr.scm 278 */
																obj_t BgL_pairz00_2506;

																BgL_pairz00_2506 = CDR(((obj_t) BgL_fz00_28));
																BgL_arg1450z00_1454 = CAR(BgL_pairz00_2506);
															}
															BgL__andtest_1043z00_1452 =
																BGl_morezd2precisezf3z21zz__match_descriptionsz00
																(BgL_descrz00_27, BgL_arg1450z00_1454);
														}
														if (CBOOL(BgL__andtest_1043z00_1452))
															{	/* Match/descr.scm 279 */
																obj_t BgL_arg1449z00_1453;

																{	/* Match/descr.scm 279 */
																	obj_t BgL_pairz00_2512;

																	{	/* Match/descr.scm 279 */
																		obj_t BgL_pairz00_2511;

																		BgL_pairz00_2511 =
																			CDR(((obj_t) BgL_fz00_28));
																		BgL_pairz00_2512 = CDR(BgL_pairz00_2511);
																	}
																	BgL_arg1449z00_1453 = CAR(BgL_pairz00_2512);
																}
																{
																	obj_t BgL_fz00_4050;

																	BgL_fz00_4050 = BgL_arg1449z00_1453;
																	BgL_fz00_28 = BgL_fz00_4050;
																	goto
																		BGl_morezd2precisezf3z21zz__match_descriptionsz00;
																}
															}
														else
															{	/* Match/descr.scm 278 */
																return BFALSE;
															}
													}
												else
													{	/* Match/descr.scm 277 */
														if (
															(CAR(
																	((obj_t) BgL_fz00_28)) ==
																BGl_symbol2162z00zz__match_descriptionsz00))
															{	/* Match/descr.scm 282 */
																obj_t BgL__ortest_1044z00_1457;

																{	/* Match/descr.scm 282 */
																	obj_t BgL_arg1454z00_1459;

																	{	/* Match/descr.scm 282 */
																		obj_t BgL_pairz00_2518;

																		BgL_pairz00_2518 =
																			CDR(((obj_t) BgL_fz00_28));
																		BgL_arg1454z00_1459 = CAR(BgL_pairz00_2518);
																	}
																	BgL__ortest_1044z00_1457 =
																		BGl_morezd2precisezf3z21zz__match_descriptionsz00
																		(BgL_descrz00_27, BgL_arg1454z00_1459);
																}
																if (CBOOL(BgL__ortest_1044z00_1457))
																	{	/* Match/descr.scm 282 */
																		return BgL__ortest_1044z00_1457;
																	}
																else
																	{	/* Match/descr.scm 283 */
																		obj_t BgL_arg1453z00_1458;

																		{	/* Match/descr.scm 283 */
																			obj_t BgL_pairz00_2524;

																			{	/* Match/descr.scm 283 */
																				obj_t BgL_pairz00_2523;

																				BgL_pairz00_2523 =
																					CDR(((obj_t) BgL_fz00_28));
																				BgL_pairz00_2524 =
																					CDR(BgL_pairz00_2523);
																			}
																			BgL_arg1453z00_1458 =
																				CAR(BgL_pairz00_2524);
																		}
																		{
																			obj_t BgL_fz00_4065;

																			BgL_fz00_4065 = BgL_arg1453z00_1458;
																			BgL_fz00_28 = BgL_fz00_4065;
																			goto
																				BGl_morezd2precisezf3z21zz__match_descriptionsz00;
																		}
																	}
															}
														else
															{	/* Match/descr.scm 281 */
																if (
																	(CAR(
																			((obj_t) BgL_fz00_28)) ==
																		BGl_symbol2164z00zz__match_descriptionsz00))
																	{	/* Match/descr.scm 285 */
																		return BFALSE;
																	}
																else
																	{	/* Match/descr.scm 285 */
																		if (CBOOL
																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(CAR(((obj_t) BgL_fz00_28)),
																					BGl_list2201z00zz__match_descriptionsz00)))
																			{	/* Match/descr.scm 288 */
																				if (
																					(CAR(
																							((obj_t) BgL_descrz00_27)) ==
																						BGl_symbol2169z00zz__match_descriptionsz00))
																					{	/* Match/descr.scm 290 */
																						obj_t BgL__andtest_1046z00_1465;

																						{	/* Match/descr.scm 290 */
																							obj_t BgL_arg1461z00_1468;
																							obj_t BgL_arg1462z00_1469;

																							{	/* Match/descr.scm 290 */
																								obj_t BgL_pairz00_2533;

																								BgL_pairz00_2533 =
																									CDR(
																									((obj_t) BgL_descrz00_27));
																								BgL_arg1461z00_1468 =
																									CAR(BgL_pairz00_2533);
																							}
																							{	/* Match/descr.scm 290 */
																								obj_t BgL_pairz00_2537;

																								BgL_pairz00_2537 =
																									CDR(((obj_t) BgL_fz00_28));
																								BgL_arg1462z00_1469 =
																									CAR(BgL_pairz00_2537);
																							}
																							BgL__andtest_1046z00_1465 =
																								BGl_morezd2precisezf3z21zz__match_descriptionsz00
																								(BgL_arg1461z00_1468,
																								BgL_arg1462z00_1469);
																						}
																						if (CBOOL
																							(BgL__andtest_1046z00_1465))
																							{	/* Match/descr.scm 291 */
																								obj_t BgL_arg1459z00_1466;
																								obj_t BgL_arg1460z00_1467;

																								{	/* Match/descr.scm 291 */
																									obj_t BgL_pairz00_2543;

																									{	/* Match/descr.scm 291 */
																										obj_t BgL_pairz00_2542;

																										BgL_pairz00_2542 =
																											CDR(
																											((obj_t)
																												BgL_descrz00_27));
																										BgL_pairz00_2543 =
																											CDR(BgL_pairz00_2542);
																									}
																									BgL_arg1459z00_1466 =
																										CAR(BgL_pairz00_2543);
																								}
																								{	/* Match/descr.scm 291 */
																									obj_t BgL_pairz00_2549;

																									{	/* Match/descr.scm 291 */
																										obj_t BgL_pairz00_2548;

																										BgL_pairz00_2548 =
																											CDR(
																											((obj_t) BgL_fz00_28));
																										BgL_pairz00_2549 =
																											CDR(BgL_pairz00_2548);
																									}
																									BgL_arg1460z00_1467 =
																										CAR(BgL_pairz00_2549);
																								}
																								{
																									obj_t BgL_fz00_4097;
																									obj_t BgL_descrz00_4096;

																									BgL_descrz00_4096 =
																										BgL_arg1459z00_1466;
																									BgL_fz00_4097 =
																										BgL_arg1460z00_1467;
																									BgL_fz00_28 = BgL_fz00_4097;
																									BgL_descrz00_27 =
																										BgL_descrz00_4096;
																									goto
																										BGl_morezd2precisezf3z21zz__match_descriptionsz00;
																								}
																							}
																						else
																							{	/* Match/descr.scm 290 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Match/descr.scm 289 */
																						return BFALSE;
																					}
																			}
																		else
																			{	/* Match/descr.scm 288 */
																				return BFALSE;
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &more-precise? */
	obj_t BGl_z62morezd2precisezf3z43zz__match_descriptionsz00(obj_t
		BgL_envz00_3369, obj_t BgL_descrz00_3370, obj_t BgL_fz00_3371)
	{
		{	/* Match/descr.scm 263 */
			return
				BGl_morezd2precisezf3z21zz__match_descriptionsz00(BgL_descrz00_3370,
				BgL_fz00_3371);
		}

	}



/* compatible? */
	BGL_EXPORTED_DEF obj_t BGl_compatiblezf3zf3zz__match_descriptionsz00(obj_t
		BgL_descrz00_29, obj_t BgL_patternz00_30)
	{
		{	/* Match/descr.scm 315 */
			{	/* Match/descr.scm 316 */
				obj_t BgL_resz00_1480;

				if (
					(CAR(
							((obj_t) BgL_patternz00_30)) ==
						BGl_symbol2166z00zz__match_descriptionsz00))
					{	/* Match/descr.scm 319 */
						obj_t BgL__andtest_1047z00_1482;

						{	/* Match/descr.scm 319 */
							obj_t BgL_arg1477z00_1484;

							{	/* Match/descr.scm 319 */
								obj_t BgL_pairz00_2555;

								BgL_pairz00_2555 = CDR(((obj_t) BgL_patternz00_30));
								BgL_arg1477z00_1484 = CAR(BgL_pairz00_2555);
							}
							BgL__andtest_1047z00_1482 =
								BGl_compatiblezf3zf3zz__match_descriptionsz00(BgL_descrz00_29,
								BgL_arg1477z00_1484);
						}
						if (CBOOL(BgL__andtest_1047z00_1482))
							{	/* Match/descr.scm 320 */
								obj_t BgL_arg1476z00_1483;

								{	/* Match/descr.scm 320 */
									obj_t BgL_pairz00_2561;

									{	/* Match/descr.scm 320 */
										obj_t BgL_pairz00_2560;

										BgL_pairz00_2560 = CDR(((obj_t) BgL_patternz00_30));
										BgL_pairz00_2561 = CDR(BgL_pairz00_2560);
									}
									BgL_arg1476z00_1483 = CAR(BgL_pairz00_2561);
								}
								BgL_resz00_1480 =
									BGl_compatiblezf3zf3zz__match_descriptionsz00(BgL_descrz00_29,
									BgL_arg1476z00_1483);
							}
						else
							{	/* Match/descr.scm 319 */
								BgL_resz00_1480 = BFALSE;
							}
					}
				else
					{	/* Match/descr.scm 318 */
						BgL_resz00_1480 =
							BGl_comparez00zz__match_descriptionsz00(BgL_descrz00_29,
							BGl_alphazd2convertzd2zz__match_descriptionsz00
							(BgL_patternz00_30), BGl_proc2206z00zz__match_descriptionsz00,
							BGl_proc2207z00zz__match_descriptionsz00,
							BGl_proc2208z00zz__match_descriptionsz00);
					}
				return BgL_resz00_1480;
			}
		}

	}



/* &compatible? */
	obj_t BGl_z62compatiblezf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3375,
		obj_t BgL_descrz00_3376, obj_t BgL_patternz00_3377)
	{
		{	/* Match/descr.scm 315 */
			return
				BGl_compatiblezf3zf3zz__match_descriptionsz00(BgL_descrz00_3376,
				BgL_patternz00_3377);
		}

	}



/* &<@anonymous:1484> */
	obj_t BGl_z62zc3z04anonymousza31484ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3378, obj_t BgL_xz00_3379)
	{
		{	/* Match/descr.scm 326 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1483> */
	obj_t BGl_z62zc3z04anonymousza31483ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3380, obj_t BgL_xz00_3381)
	{
		{	/* Match/descr.scm 325 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &<@anonymous:1482> */
	obj_t BGl_z62zc3z04anonymousza31482ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3382, obj_t BgL_xz00_3383)
	{
		{	/* Match/descr.scm 324 */
			return BGl_symbol2209z00zz__match_descriptionsz00;
		}

	}



/* compare */
	obj_t BGl_comparez00zz__match_descriptionsz00(obj_t BgL_descrz00_31,
		obj_t BgL_patz00_32, obj_t BgL_envz00_33, obj_t BgL_kz00_34,
		obj_t BgL_za7za7_35)
	{
		{	/* Match/descr.scm 343 */
		BGl_comparez00zz__match_descriptionsz00:
			{	/* Match/descr.scm 346 */
				bool_t BgL_test2349z00_4119;

				{	/* Match/descr.scm 346 */
					bool_t BgL_test2350z00_4120;

					if (
						(CAR(
								((obj_t) BgL_descrz00_31)) ==
							BGl_symbol2186z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 502 */
							BgL_test2350z00_4120 = ((bool_t) 1);
						}
					else
						{	/* Match/descr.scm 502 */
							BgL_test2350z00_4120 =
								(CAR(
									((obj_t) BgL_descrz00_31)) ==
								BGl_symbol2188z00zz__match_descriptionsz00);
						}
					if (BgL_test2350z00_4120)
						{	/* Match/descr.scm 346 */
							BgL_test2349z00_4119 = ((bool_t) 1);
						}
					else
						{	/* Match/descr.scm 347 */
							bool_t BgL_test2352z00_4128;

							if (
								(CAR(
										((obj_t) BgL_patz00_32)) ==
									BGl_symbol2186z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 502 */
									BgL_test2352z00_4128 = ((bool_t) 1);
								}
							else
								{	/* Match/descr.scm 502 */
									BgL_test2352z00_4128 =
										(CAR(
											((obj_t) BgL_patz00_32)) ==
										BGl_symbol2188z00zz__match_descriptionsz00);
								}
							if (BgL_test2352z00_4128)
								{	/* Match/descr.scm 347 */
									BgL_test2349z00_4119 = ((bool_t) 1);
								}
							else
								{	/* Match/descr.scm 347 */
									if (
										(CAR(
												((obj_t) BgL_patz00_32)) ==
											BGl_symbol2162z00zz__match_descriptionsz00))
										{	/* Match/descr.scm 348 */
											BgL_test2349z00_4119 = ((bool_t) 1);
										}
									else
										{	/* Match/descr.scm 348 */
											if (
												(CAR(
														((obj_t) BgL_patz00_32)) ==
													BGl_symbol2164z00zz__match_descriptionsz00))
												{	/* Match/descr.scm 348 */
													BgL_test2349z00_4119 = ((bool_t) 1);
												}
											else
												{	/* Match/descr.scm 348 */
													if (
														(CAR(
																((obj_t) BgL_patz00_32)) ==
															BGl_symbol2211z00zz__match_descriptionsz00))
														{	/* Match/descr.scm 348 */
															BgL_test2349z00_4119 = ((bool_t) 1);
														}
													else
														{	/* Match/descr.scm 348 */
															if (
																(CAR(
																		((obj_t) BgL_patz00_32)) ==
																	BGl_symbol2197z00zz__match_descriptionsz00))
																{	/* Match/descr.scm 349 */
																	BgL_test2349z00_4119 = ((bool_t) 1);
																}
															else
																{	/* Match/descr.scm 349 */
																	if (
																		(CAR(
																				((obj_t) BgL_patz00_32)) ==
																			BGl_symbol2188z00zz__match_descriptionsz00))
																		{	/* Match/descr.scm 350 */
																			BgL_test2349z00_4119 = ((bool_t) 1);
																		}
																	else
																		{	/* Match/descr.scm 350 */
																			if (
																				(CAR(
																						((obj_t) BgL_patz00_32)) ==
																					BGl_symbol2176z00zz__match_descriptionsz00))
																				{	/* Match/descr.scm 351 */
																					BgL_test2349z00_4119 = ((bool_t) 1);
																				}
																			else
																				{	/* Match/descr.scm 351 */
																					BgL_test2349z00_4119 =
																						(CAR(
																							((obj_t) BgL_patz00_32)) ==
																						BGl_symbol2174z00zz__match_descriptionsz00);
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
				if (BgL_test2349z00_4119)
					{	/* Match/descr.scm 346 */
						return BGL_PROCEDURE_CALL1(BgL_kz00_34, BgL_envz00_33);
					}
				else
					{	/* Match/descr.scm 346 */
						if (
							(CAR(
									((obj_t) BgL_patz00_32)) ==
								BGl_symbol2166z00zz__match_descriptionsz00))
							{	/* Match/descr.scm 356 */
								obj_t BgL_arg1495z00_1508;

								{	/* Match/descr.scm 356 */
									obj_t BgL_pairz00_2591;

									BgL_pairz00_2591 = CDR(((obj_t) BgL_patz00_32));
									BgL_arg1495z00_1508 = CAR(BgL_pairz00_2591);
								}
								{	/* Match/descr.scm 357 */
									obj_t BgL_zc3z04anonymousza31498ze3z87_3384;

									BgL_zc3z04anonymousza31498ze3z87_3384 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31498ze3ze5zz__match_descriptionsz00,
										(int) (1L), (int) (4L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31498ze3z87_3384,
										(int) (0L), BgL_patz00_32);
									PROCEDURE_SET(BgL_zc3z04anonymousza31498ze3z87_3384,
										(int) (1L), BgL_descrz00_31);
									PROCEDURE_SET(BgL_zc3z04anonymousza31498ze3z87_3384,
										(int) (2L), BgL_kz00_34);
									PROCEDURE_SET(BgL_zc3z04anonymousza31498ze3z87_3384,
										(int) (3L), BgL_za7za7_35);
									{
										obj_t BgL_kz00_4186;
										obj_t BgL_patz00_4185;

										BgL_patz00_4185 = BgL_arg1495z00_1508;
										BgL_kz00_4186 = BgL_zc3z04anonymousza31498ze3z87_3384;
										BgL_kz00_34 = BgL_kz00_4186;
										BgL_patz00_32 = BgL_patz00_4185;
										goto BGl_comparez00zz__match_descriptionsz00;
									}
								}
							}
						else
							{	/* Match/descr.scm 355 */
								if (
									(CAR(
											((obj_t) BgL_patz00_32)) ==
										BGl_symbol2169z00zz__match_descriptionsz00))
									{	/* Match/descr.scm 360 */
										if (BGl_mayzd2bezd2azd2conszd2zz__match_descriptionsz00
											(BgL_descrz00_31))
											{	/* Match/descr.scm 362 */
												obj_t BgL_arg1502z00_1516;
												obj_t BgL_arg1503z00_1517;

												if (
													(CAR(
															((obj_t) BgL_descrz00_31)) ==
														BGl_symbol2169z00zz__match_descriptionsz00))
													{	/* Match/descr.scm 179 */
														obj_t BgL_pairz00_2607;

														BgL_pairz00_2607 = CDR(((obj_t) BgL_descrz00_31));
														BgL_arg1502z00_1516 = CAR(BgL_pairz00_2607);
													}
												else
													{	/* Match/descr.scm 178 */
														BgL_arg1502z00_1516 =
															BGl_list2196z00zz__match_descriptionsz00;
													}
												{	/* Match/descr.scm 362 */
													obj_t BgL_pairz00_2611;

													BgL_pairz00_2611 = CDR(((obj_t) BgL_patz00_32));
													BgL_arg1503z00_1517 = CAR(BgL_pairz00_2611);
												}
												{	/* Match/descr.scm 364 */
													obj_t BgL_zc3z04anonymousza31505ze3z87_3385;

													BgL_zc3z04anonymousza31505ze3z87_3385 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31505ze3ze5zz__match_descriptionsz00,
														(int) (1L), (int) (4L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31505ze3z87_3385,
														(int) (0L), BgL_descrz00_31);
													PROCEDURE_SET(BgL_zc3z04anonymousza31505ze3z87_3385,
														(int) (1L), BgL_patz00_32);
													PROCEDURE_SET(BgL_zc3z04anonymousza31505ze3z87_3385,
														(int) (2L), BgL_kz00_34);
													PROCEDURE_SET(BgL_zc3z04anonymousza31505ze3z87_3385,
														(int) (3L), BgL_za7za7_35);
													{
														obj_t BgL_kz00_4216;
														obj_t BgL_patz00_4215;
														obj_t BgL_descrz00_4214;

														BgL_descrz00_4214 = BgL_arg1502z00_1516;
														BgL_patz00_4215 = BgL_arg1503z00_1517;
														BgL_kz00_4216 =
															BgL_zc3z04anonymousza31505ze3z87_3385;
														BgL_kz00_34 = BgL_kz00_4216;
														BgL_patz00_32 = BgL_patz00_4215;
														BgL_descrz00_31 = BgL_descrz00_4214;
														goto BGl_comparez00zz__match_descriptionsz00;
													}
												}
											}
										else
											{	/* Match/descr.scm 361 */
												return
													BGL_PROCEDURE_CALL1(BgL_za7za7_35, BgL_envz00_33);
											}
									}
								else
									{	/* Match/descr.scm 360 */
										if (
											(CAR(
													((obj_t) BgL_patz00_32)) ==
												BGl_symbol2199z00zz__match_descriptionsz00))
											{	/* Match/descr.scm 372 */
												obj_t BgL_arg1509z00_1525;

												{	/* Match/descr.scm 372 */
													obj_t BgL_pairz00_2634;

													BgL_pairz00_2634 = CDR(((obj_t) BgL_patz00_32));
													BgL_arg1509z00_1525 = CAR(BgL_pairz00_2634);
												}
												BGL_TAIL return
													BGl_matchz00zz__match_descriptionsz00(BgL_descrz00_31,
													BgL_arg1509z00_1525, BgL_envz00_33, BgL_kz00_34,
													BgL_za7za7_35);
											}
										else
											{	/* Match/descr.scm 374 */
												bool_t BgL_test2365z00_4229;

												if (
													(CAR(
															((obj_t) BgL_descrz00_31)) ==
														BGl_symbol2178z00zz__match_descriptionsz00))
													{	/* Match/descr.scm 374 */
														BgL_test2365z00_4229 =
															(CAR(
																((obj_t) BgL_patz00_32)) ==
															BGl_symbol2178z00zz__match_descriptionsz00);
													}
												else
													{	/* Match/descr.scm 374 */
														BgL_test2365z00_4229 = ((bool_t) 0);
													}
												if (BgL_test2365z00_4229)
													{	/* Match/descr.scm 375 */
														bool_t BgL_test2367z00_4237;

														{	/* Match/descr.scm 375 */
															obj_t BgL_arg1557z00_1561;

															{	/* Match/descr.scm 375 */
																obj_t BgL_arg1558z00_1562;

																{	/* Match/descr.scm 375 */
																	obj_t BgL_pairz00_2642;

																	BgL_pairz00_2642 =
																		CDR(((obj_t) BgL_descrz00_31));
																	BgL_arg1558z00_1562 = CAR(BgL_pairz00_2642);
																}
																BgL_arg1557z00_1561 =
																	BGL_PROCEDURE_CALL1(BgL_envz00_33,
																	BgL_arg1558z00_1562);
															}
															BgL_test2367z00_4237 =
																(BgL_arg1557z00_1561 ==
																BGl_symbol2209z00zz__match_descriptionsz00);
														}
														if (BgL_test2367z00_4237)
															{	/* Match/descr.scm 376 */
																bool_t BgL_test2368z00_4246;

																{	/* Match/descr.scm 376 */
																	obj_t BgL_arg1535z00_1546;

																	{	/* Match/descr.scm 376 */
																		obj_t BgL_arg1536z00_1547;

																		{	/* Match/descr.scm 376 */
																			obj_t BgL_pairz00_2646;

																			BgL_pairz00_2646 =
																				CDR(((obj_t) BgL_patz00_32));
																			BgL_arg1536z00_1547 =
																				CAR(BgL_pairz00_2646);
																		}
																		BgL_arg1535z00_1546 =
																			BGL_PROCEDURE_CALL1(BgL_envz00_33,
																			BgL_arg1536z00_1547);
																	}
																	BgL_test2368z00_4246 =
																		(BgL_arg1535z00_1546 ==
																		BGl_symbol2209z00zz__match_descriptionsz00);
																}
																if (BgL_test2368z00_4246)
																	{	/* Match/descr.scm 377 */
																		obj_t BgL_sz00_1534;

																		{	/* Match/descr.scm 377 */
																			obj_t BgL_arg1525z00_1539;

																			BgL_arg1525z00_1539 =
																				BGL_PROCEDURE_CALL1
																				(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
																				BGl_string2213z00zz__match_descriptionsz00);
																			{	/* Match/descr.scm 377 */
																				obj_t BgL_list1526z00_1540;

																				{	/* Match/descr.scm 377 */
																					obj_t BgL_arg1527z00_1541;

																					BgL_arg1527z00_1541 =
																						MAKE_YOUNG_PAIR(BgL_arg1525z00_1539,
																						BNIL);
																					BgL_list1526z00_1540 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2178z00zz__match_descriptionsz00,
																						BgL_arg1527z00_1541);
																				}
																				BgL_sz00_1534 = BgL_list1526z00_1540;
																			}
																		}
																		{	/* Match/descr.scm 378 */
																			obj_t BgL_arg1521z00_1535;

																			{	/* Match/descr.scm 378 */
																				obj_t BgL_arg1522z00_1536;
																				obj_t BgL_arg1523z00_1537;

																				{	/* Match/descr.scm 378 */
																					obj_t BgL_arg1524z00_1538;

																					{	/* Match/descr.scm 378 */
																						obj_t BgL_pairz00_2651;

																						BgL_pairz00_2651 =
																							CDR(((obj_t) BgL_descrz00_31));
																						BgL_arg1524z00_1538 =
																							CAR(BgL_pairz00_2651);
																					}
																					{	/* Match/descr.scm 124 */
																						obj_t
																							BgL_zc3z04anonymousza31319ze3z87_3387;
																						{
																							int BgL_tmpz00_4264;

																							BgL_tmpz00_4264 = (int) (3L);
																							BgL_zc3z04anonymousza31319ze3z87_3387
																								=
																								MAKE_EL_PROCEDURE
																								(BgL_tmpz00_4264);
																						}
																						PROCEDURE_EL_SET
																							(BgL_zc3z04anonymousza31319ze3z87_3387,
																							(int) (0L), BgL_envz00_33);
																						PROCEDURE_EL_SET
																							(BgL_zc3z04anonymousza31319ze3z87_3387,
																							(int) (1L), BgL_sz00_1534);
																						PROCEDURE_EL_SET
																							(BgL_zc3z04anonymousza31319ze3z87_3387,
																							(int) (2L), BgL_arg1524z00_1538);
																						BgL_arg1522z00_1536 =
																							BgL_zc3z04anonymousza31319ze3z87_3387;
																				}}
																				{	/* Match/descr.scm 379 */
																					obj_t BgL_pairz00_2657;

																					BgL_pairz00_2657 =
																						CDR(((obj_t) BgL_patz00_32));
																					BgL_arg1523z00_1537 =
																						CAR(BgL_pairz00_2657);
																				}
																				{	/* Match/descr.scm 124 */
																					obj_t
																						BgL_zc3z04anonymousza31319ze3z87_3386;
																					BgL_zc3z04anonymousza31319ze3z87_3386
																						=
																						MAKE_FX_PROCEDURE
																						(BGl_z62zc3z04anonymousza31319ze3ze5zz__match_descriptionsz00,
																						(int) (1L), (int) (3L));
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31319ze3z87_3386,
																						(int) (0L), BgL_arg1522z00_1536);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31319ze3z87_3386,
																						(int) (1L), BgL_sz00_1534);
																					PROCEDURE_SET
																						(BgL_zc3z04anonymousza31319ze3z87_3386,
																						(int) (2L), BgL_arg1523z00_1537);
																					BgL_arg1521z00_1535 =
																						BgL_zc3z04anonymousza31319ze3z87_3386;
																			}}
																			return
																				BGL_PROCEDURE_CALL1(BgL_kz00_34,
																				BgL_arg1521z00_1535);
																		}
																	}
																else
																	{	/* Match/descr.scm 381 */
																		obj_t BgL_arg1528z00_1542;

																		{	/* Match/descr.scm 381 */
																			obj_t BgL_arg1529z00_1543;
																			obj_t BgL_arg1530z00_1544;

																			{	/* Match/descr.scm 381 */
																				obj_t BgL_pairz00_2663;

																				BgL_pairz00_2663 =
																					CDR(((obj_t) BgL_descrz00_31));
																				BgL_arg1529z00_1543 =
																					CAR(BgL_pairz00_2663);
																			}
																			{	/* Match/descr.scm 381 */
																				obj_t BgL_arg1531z00_1545;

																				{	/* Match/descr.scm 381 */
																					obj_t BgL_pairz00_2667;

																					BgL_pairz00_2667 =
																						CDR(((obj_t) BgL_patz00_32));
																					BgL_arg1531z00_1545 =
																						CAR(BgL_pairz00_2667);
																				}
																				BgL_arg1530z00_1544 =
																					BGL_PROCEDURE_CALL1(BgL_envz00_33,
																					BgL_arg1531z00_1545);
																			}
																			{	/* Match/descr.scm 124 */
																				obj_t
																					BgL_zc3z04anonymousza31319ze3z87_3388;
																				BgL_zc3z04anonymousza31319ze3z87_3388 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza31319ze32155ze5zz__match_descriptionsz00,
																					(int) (1L), (int) (3L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3388,
																					(int) (0L), BgL_envz00_33);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3388,
																					(int) (1L), BgL_arg1530z00_1544);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3388,
																					(int) (2L), BgL_arg1529z00_1543);
																				BgL_arg1528z00_1542 =
																					BgL_zc3z04anonymousza31319ze3z87_3388;
																		}}
																		return
																			BGL_PROCEDURE_CALL1(BgL_kz00_34,
																			BgL_arg1528z00_1542);
																	}
															}
														else
															{	/* Match/descr.scm 382 */
																bool_t BgL_test2369z00_4312;

																{	/* Match/descr.scm 382 */
																	obj_t BgL_arg1555z00_1559;

																	{	/* Match/descr.scm 382 */
																		obj_t BgL_arg1556z00_1560;

																		{	/* Match/descr.scm 382 */
																			obj_t BgL_pairz00_2673;

																			BgL_pairz00_2673 =
																				CDR(((obj_t) BgL_patz00_32));
																			BgL_arg1556z00_1560 =
																				CAR(BgL_pairz00_2673);
																		}
																		BgL_arg1555z00_1559 =
																			BGL_PROCEDURE_CALL1(BgL_envz00_33,
																			BgL_arg1556z00_1560);
																	}
																	BgL_test2369z00_4312 =
																		(BgL_arg1555z00_1559 ==
																		BGl_symbol2209z00zz__match_descriptionsz00);
																}
																if (BgL_test2369z00_4312)
																	{	/* Match/descr.scm 383 */
																		obj_t BgL_arg1543z00_1551;

																		{	/* Match/descr.scm 383 */
																			obj_t BgL_arg1544z00_1552;
																			obj_t BgL_arg1546z00_1553;

																			{	/* Match/descr.scm 383 */
																				obj_t BgL_pairz00_2677;

																				BgL_pairz00_2677 =
																					CDR(((obj_t) BgL_patz00_32));
																				BgL_arg1544z00_1552 =
																					CAR(BgL_pairz00_2677);
																			}
																			{	/* Match/descr.scm 383 */
																				obj_t BgL_arg1547z00_1554;

																				{	/* Match/descr.scm 383 */
																					obj_t BgL_pairz00_2681;

																					BgL_pairz00_2681 =
																						CDR(((obj_t) BgL_descrz00_31));
																					BgL_arg1547z00_1554 =
																						CAR(BgL_pairz00_2681);
																				}
																				BgL_arg1546z00_1553 =
																					BGL_PROCEDURE_CALL1(BgL_envz00_33,
																					BgL_arg1547z00_1554);
																			}
																			{	/* Match/descr.scm 124 */
																				obj_t
																					BgL_zc3z04anonymousza31319ze3z87_3389;
																				BgL_zc3z04anonymousza31319ze3z87_3389 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza31319ze32156ze5zz__match_descriptionsz00,
																					(int) (1L), (int) (3L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3389,
																					(int) (0L), BgL_envz00_33);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3389,
																					(int) (1L), BgL_arg1546z00_1553);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3389,
																					(int) (2L), BgL_arg1544z00_1552);
																				BgL_arg1543z00_1551 =
																					BgL_zc3z04anonymousza31319ze3z87_3389;
																		}}
																		return
																			BGL_PROCEDURE_CALL1(BgL_kz00_34,
																			BgL_arg1543z00_1551);
																	}
																else
																	{	/* Match/descr.scm 384 */
																		obj_t BgL_arg1549z00_1555;
																		obj_t BgL_arg1552z00_1556;

																		{	/* Match/descr.scm 384 */
																			obj_t BgL_arg1553z00_1557;

																			{	/* Match/descr.scm 384 */
																				obj_t BgL_pairz00_2687;

																				BgL_pairz00_2687 =
																					CDR(((obj_t) BgL_descrz00_31));
																				BgL_arg1553z00_1557 =
																					CAR(BgL_pairz00_2687);
																			}
																			BgL_arg1549z00_1555 =
																				BGL_PROCEDURE_CALL1(BgL_envz00_33,
																				BgL_arg1553z00_1557);
																		}
																		{	/* Match/descr.scm 384 */
																			obj_t BgL_arg1554z00_1558;

																			{	/* Match/descr.scm 384 */
																				obj_t BgL_pairz00_2691;

																				BgL_pairz00_2691 =
																					CDR(((obj_t) BgL_patz00_32));
																				BgL_arg1554z00_1558 =
																					CAR(BgL_pairz00_2691);
																			}
																			BgL_arg1552z00_1556 =
																				BGL_PROCEDURE_CALL1(BgL_envz00_33,
																				BgL_arg1554z00_1558);
																		}
																		{
																			obj_t BgL_patz00_4359;
																			obj_t BgL_descrz00_4358;

																			BgL_descrz00_4358 = BgL_arg1549z00_1555;
																			BgL_patz00_4359 = BgL_arg1552z00_1556;
																			BgL_patz00_32 = BgL_patz00_4359;
																			BgL_descrz00_31 = BgL_descrz00_4358;
																			goto
																				BGl_comparez00zz__match_descriptionsz00;
																		}
																	}
															}
													}
												else
													{	/* Match/descr.scm 374 */
														if (
															(CAR(
																	((obj_t) BgL_patz00_32)) ==
																BGl_symbol2178z00zz__match_descriptionsz00))
															{	/* Match/descr.scm 387 */
																bool_t BgL_test2371z00_4364;

																{	/* Match/descr.scm 387 */
																	obj_t BgL_arg1573z00_1571;

																	{	/* Match/descr.scm 387 */
																		obj_t BgL_arg1575z00_1572;

																		{	/* Match/descr.scm 387 */
																			obj_t BgL_pairz00_2697;

																			BgL_pairz00_2697 =
																				CDR(((obj_t) BgL_patz00_32));
																			BgL_arg1575z00_1572 =
																				CAR(BgL_pairz00_2697);
																		}
																		BgL_arg1573z00_1571 =
																			BGL_PROCEDURE_CALL1(BgL_envz00_33,
																			BgL_arg1575z00_1572);
																	}
																	BgL_test2371z00_4364 =
																		(BgL_arg1573z00_1571 ==
																		BGl_symbol2209z00zz__match_descriptionsz00);
																}
																if (BgL_test2371z00_4364)
																	{	/* Match/descr.scm 388 */
																		obj_t BgL_arg1564z00_1567;

																		{	/* Match/descr.scm 388 */
																			obj_t BgL_arg1565z00_1568;

																			{	/* Match/descr.scm 388 */
																				obj_t BgL_pairz00_2701;

																				BgL_pairz00_2701 =
																					CDR(((obj_t) BgL_patz00_32));
																				BgL_arg1565z00_1568 =
																					CAR(BgL_pairz00_2701);
																			}
																			{	/* Match/descr.scm 124 */
																				obj_t
																					BgL_zc3z04anonymousza31319ze3z87_3390;
																				BgL_zc3z04anonymousza31319ze3z87_3390 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza31319ze32157ze5zz__match_descriptionsz00,
																					(int) (1L), (int) (3L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3390,
																					(int) (0L), BgL_envz00_33);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3390,
																					(int) (1L), BgL_descrz00_31);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31319ze3z87_3390,
																					(int) (2L), BgL_arg1565z00_1568);
																				BgL_arg1564z00_1567 =
																					BgL_zc3z04anonymousza31319ze3z87_3390;
																		}}
																		return
																			BGL_PROCEDURE_CALL1(BgL_kz00_34,
																			BgL_arg1564z00_1567);
																	}
																else
																	{	/* Match/descr.scm 389 */
																		obj_t BgL_arg1567z00_1569;

																		{	/* Match/descr.scm 389 */
																			obj_t BgL_arg1571z00_1570;

																			{	/* Match/descr.scm 389 */
																				obj_t BgL_pairz00_2707;

																				BgL_pairz00_2707 =
																					CDR(((obj_t) BgL_patz00_32));
																				BgL_arg1571z00_1570 =
																					CAR(BgL_pairz00_2707);
																			}
																			BgL_arg1567z00_1569 =
																				BGL_PROCEDURE_CALL1(BgL_envz00_33,
																				BgL_arg1571z00_1570);
																		}
																		{
																			obj_t BgL_patz00_4396;

																			BgL_patz00_4396 = BgL_arg1567z00_1569;
																			BgL_patz00_32 = BgL_patz00_4396;
																			goto
																				BGl_comparez00zz__match_descriptionsz00;
																		}
																	}
															}
														else
															{	/* Match/descr.scm 386 */
																if (
																	(CAR(
																			((obj_t) BgL_descrz00_31)) ==
																		BGl_symbol2178z00zz__match_descriptionsz00))
																	{	/* Match/descr.scm 392 */
																		bool_t BgL_test2373z00_4401;

																		{	/* Match/descr.scm 392 */
																			obj_t BgL_arg1586z00_1581;

																			{	/* Match/descr.scm 392 */
																				obj_t BgL_arg1587z00_1582;

																				{	/* Match/descr.scm 392 */
																					obj_t BgL_pairz00_2713;

																					BgL_pairz00_2713 =
																						CDR(((obj_t) BgL_descrz00_31));
																					BgL_arg1587z00_1582 =
																						CAR(BgL_pairz00_2713);
																				}
																				BgL_arg1586z00_1581 =
																					BGL_PROCEDURE_CALL1(BgL_envz00_33,
																					BgL_arg1587z00_1582);
																			}
																			BgL_test2373z00_4401 =
																				(BgL_arg1586z00_1581 ==
																				BGl_symbol2209z00zz__match_descriptionsz00);
																		}
																		if (BgL_test2373z00_4401)
																			{	/* Match/descr.scm 393 */
																				obj_t BgL_arg1582z00_1577;

																				{	/* Match/descr.scm 393 */
																					obj_t BgL_arg1583z00_1578;

																					{	/* Match/descr.scm 393 */
																						obj_t BgL_pairz00_2717;

																						BgL_pairz00_2717 =
																							CDR(((obj_t) BgL_descrz00_31));
																						BgL_arg1583z00_1578 =
																							CAR(BgL_pairz00_2717);
																					}
																					{	/* Match/descr.scm 124 */
																						obj_t
																							BgL_zc3z04anonymousza31319ze3z87_3391;
																						BgL_zc3z04anonymousza31319ze3z87_3391
																							=
																							MAKE_FX_PROCEDURE
																							(BGl_z62zc3z04anonymousza31319ze32158ze5zz__match_descriptionsz00,
																							(int) (1L), (int) (3L));
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31319ze3z87_3391,
																							(int) (0L), BgL_envz00_33);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31319ze3z87_3391,
																							(int) (1L), BgL_patz00_32);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31319ze3z87_3391,
																							(int) (2L), BgL_arg1583z00_1578);
																						BgL_arg1582z00_1577 =
																							BgL_zc3z04anonymousza31319ze3z87_3391;
																				}}
																				return
																					BGL_PROCEDURE_CALL1(BgL_kz00_34,
																					BgL_arg1582z00_1577);
																			}
																		else
																			{	/* Match/descr.scm 394 */
																				obj_t BgL_arg1584z00_1579;

																				{	/* Match/descr.scm 394 */
																					obj_t BgL_arg1585z00_1580;

																					{	/* Match/descr.scm 394 */
																						obj_t BgL_pairz00_2723;

																						BgL_pairz00_2723 =
																							CDR(((obj_t) BgL_descrz00_31));
																						BgL_arg1585z00_1580 =
																							CAR(BgL_pairz00_2723);
																					}
																					BgL_arg1584z00_1579 =
																						BGL_PROCEDURE_CALL1(BgL_envz00_33,
																						BgL_arg1585z00_1580);
																				}
																				{
																					obj_t BgL_patz00_4433;

																					BgL_patz00_4433 = BgL_arg1584z00_1579;
																					BgL_patz00_32 = BgL_patz00_4433;
																					goto
																						BGl_comparez00zz__match_descriptionsz00;
																				}
																			}
																	}
																else
																	{	/* Match/descr.scm 391 */
																		if (
																			(CAR(
																					((obj_t) BgL_patz00_32)) ==
																				BGl_symbol2194z00zz__match_descriptionsz00))
																			{	/* Match/descr.scm 396 */
																				bool_t BgL_test2375z00_4438;

																				{	/* Match/descr.scm 396 */
																					obj_t BgL_arg1593z00_1586;

																					{	/* Match/descr.scm 396 */
																						obj_t BgL_pairz00_2729;

																						BgL_pairz00_2729 =
																							CDR(((obj_t) BgL_patz00_32));
																						BgL_arg1593z00_1586 =
																							CAR(BgL_pairz00_2729);
																					}
																					BgL_test2375z00_4438 =
																						CBOOL
																						(BGl_morezd2precisezf3z21zz__match_descriptionsz00
																						(BgL_arg1593z00_1586,
																							BgL_descrz00_31));
																				}
																				if (BgL_test2375z00_4438)
																					{	/* Match/descr.scm 396 */
																						return
																							BGL_PROCEDURE_CALL1(BgL_za7za7_35,
																							BgL_envz00_33);
																					}
																				else
																					{	/* Match/descr.scm 396 */
																						return
																							BGL_PROCEDURE_CALL1(BgL_kz00_34,
																							BgL_envz00_33);
																					}
																			}
																		else
																			{	/* Match/descr.scm 396 */
																				if (
																					(CAR(
																							((obj_t) BgL_patz00_32)) ==
																						BGl_symbol2180z00zz__match_descriptionsz00))
																					{	/* Match/descr.scm 401 */
																						bool_t BgL_test2377z00_4456;

																						if (
																							(CAR(
																									((obj_t) BgL_descrz00_31)) ==
																								BGl_symbol2186z00zz__match_descriptionsz00))
																							{	/* Match/descr.scm 502 */
																								BgL_test2377z00_4456 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Match/descr.scm 502 */
																								BgL_test2377z00_4456 =
																									(CAR(
																										((obj_t) BgL_descrz00_31))
																									==
																									BGl_symbol2188z00zz__match_descriptionsz00);
																							}
																						if (BgL_test2377z00_4456)
																							{	/* Match/descr.scm 401 */
																								return BTRUE;
																							}
																						else
																							{	/* Match/descr.scm 401 */
																								if (
																									(CAR(
																											((obj_t) BgL_descrz00_31))
																										==
																										BGl_symbol2214z00zz__match_descriptionsz00))
																									{	/* Match/descr.scm 403 */
																										BGL_TAIL return
																											BGl_matchz00zz__match_descriptionsz00
																											(BgL_patz00_32,
																											BgL_descrz00_31,
																											BgL_envz00_33,
																											BgL_kz00_34,
																											BgL_za7za7_35);
																									}
																								else
																									{	/* Match/descr.scm 403 */
																										return BFALSE;
																									}
																							}
																					}
																				else
																					{	/* Match/descr.scm 400 */
																						return
																							BGL_PROCEDURE_CALL1(BgL_kz00_34,
																							BgL_envz00_33);
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &<@anonymous:1498> */
	obj_t BGl_z62zc3z04anonymousza31498ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3392, obj_t BgL_envz00_3397)
	{
		{	/* Match/descr.scm 357 */
			{	/* Match/descr.scm 357 */
				obj_t BgL_patz00_3393;
				obj_t BgL_descrz00_3394;
				obj_t BgL_kz00_3395;
				obj_t BgL_za7za7_3396;

				BgL_patz00_3393 = PROCEDURE_REF(BgL_envz00_3392, (int) (0L));
				BgL_descrz00_3394 = PROCEDURE_REF(BgL_envz00_3392, (int) (1L));
				BgL_kz00_3395 = PROCEDURE_REF(BgL_envz00_3392, (int) (2L));
				BgL_za7za7_3396 = PROCEDURE_REF(BgL_envz00_3392, (int) (3L));
				{	/* Match/descr.scm 357 */
					obj_t BgL_arg1499z00_3588;

					{	/* Match/descr.scm 357 */
						obj_t BgL_pairz00_3589;

						{	/* Match/descr.scm 357 */
							obj_t BgL_pairz00_3590;

							BgL_pairz00_3590 = CDR(((obj_t) BgL_patz00_3393));
							BgL_pairz00_3589 = CDR(BgL_pairz00_3590);
						}
						BgL_arg1499z00_3588 = CAR(BgL_pairz00_3589);
					}
					return
						BGl_comparez00zz__match_descriptionsz00(BgL_descrz00_3394,
						BgL_arg1499z00_3588, BgL_envz00_3397, BgL_kz00_3395,
						BgL_za7za7_3396);
				}
			}
		}

	}



/* &<@anonymous:1505> */
	obj_t BGl_z62zc3z04anonymousza31505ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3398, obj_t BgL_envz00_3403)
	{
		{	/* Match/descr.scm 363 */
			{	/* Match/descr.scm 364 */
				obj_t BgL_descrz00_3399;
				obj_t BgL_patz00_3400;
				obj_t BgL_kz00_3401;
				obj_t BgL_za7za7_3402;

				BgL_descrz00_3399 = PROCEDURE_REF(BgL_envz00_3398, (int) (0L));
				BgL_patz00_3400 = PROCEDURE_REF(BgL_envz00_3398, (int) (1L));
				BgL_kz00_3401 = PROCEDURE_REF(BgL_envz00_3398, (int) (2L));
				BgL_za7za7_3402 = PROCEDURE_REF(BgL_envz00_3398, (int) (3L));
				{	/* Match/descr.scm 364 */
					obj_t BgL_arg1506z00_3591;
					obj_t BgL_arg1507z00_3592;

					if (
						(CAR(
								((obj_t) BgL_descrz00_3399)) ==
							BGl_symbol2169z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 184 */
							obj_t BgL_pairz00_3593;

							{	/* Match/descr.scm 184 */
								obj_t BgL_pairz00_3594;

								BgL_pairz00_3594 = CDR(((obj_t) BgL_descrz00_3399));
								BgL_pairz00_3593 = CDR(BgL_pairz00_3594);
							}
							BgL_arg1506z00_3591 = CAR(BgL_pairz00_3593);
						}
					else
						{	/* Match/descr.scm 183 */
							BgL_arg1506z00_3591 = BGl_list2196z00zz__match_descriptionsz00;
						}
					{	/* Match/descr.scm 365 */
						obj_t BgL_pairz00_3595;

						{	/* Match/descr.scm 365 */
							obj_t BgL_pairz00_3596;

							BgL_pairz00_3596 = CDR(((obj_t) BgL_patz00_3400));
							BgL_pairz00_3595 = CDR(BgL_pairz00_3596);
						}
						BgL_arg1507z00_3592 = CAR(BgL_pairz00_3595);
					}
					return
						BGl_comparez00zz__match_descriptionsz00(BgL_arg1506z00_3591,
						BgL_arg1507z00_3592, BgL_envz00_3403, BgL_kz00_3401,
						BgL_za7za7_3402);
				}
			}
		}

	}



/* &<@anonymous:1319> */
	obj_t BGl_z62zc3z04anonymousza31319ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3404, obj_t BgL_xz00_3408)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_arg1522z00_3405;
				obj_t BgL_sz00_3406;
				obj_t BgL_arg1523z00_3407;

				BgL_arg1522z00_3405 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3404, (int) (0L)));
				BgL_sz00_3406 = ((obj_t) PROCEDURE_REF(BgL_envz00_3404, (int) (1L)));
				BgL_arg1523z00_3407 = PROCEDURE_REF(BgL_envz00_3404, (int) (2L));
				if ((BgL_xz00_3408 == BgL_arg1523z00_3407))
					{	/* Match/descr.scm 124 */
						return BgL_sz00_3406;
					}
				else
					{	/* Match/descr.scm 124 */
						return
							BGl_z62zc3z04anonymousza31319ze32154ze5zz__match_descriptionsz00
							(BgL_arg1522z00_3405, BgL_xz00_3408);
					}
			}
		}

	}



/* &<@anonymous:1319>2154 */
	obj_t BGl_z62zc3z04anonymousza31319ze32154ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3409, obj_t BgL_xz00_3413)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3410;
				obj_t BgL_sz00_3411;
				obj_t BgL_arg1524z00_3412;

				BgL_envz00_3410 = PROCEDURE_EL_REF(BgL_envz00_3409, (int) (0L));
				BgL_sz00_3411 = ((obj_t) PROCEDURE_EL_REF(BgL_envz00_3409, (int) (1L)));
				BgL_arg1524z00_3412 = PROCEDURE_EL_REF(BgL_envz00_3409, (int) (2L));
				if ((BgL_xz00_3413 == BgL_arg1524z00_3412))
					{	/* Match/descr.scm 124 */
						return BgL_sz00_3411;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3410, BgL_xz00_3413);
					}
			}
		}

	}



/* &<@anonymous:1319>2155 */
	obj_t BGl_z62zc3z04anonymousza31319ze32155ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3414, obj_t BgL_xz00_3418)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3415;
				obj_t BgL_arg1530z00_3416;
				obj_t BgL_arg1529z00_3417;

				BgL_envz00_3415 = PROCEDURE_REF(BgL_envz00_3414, (int) (0L));
				BgL_arg1530z00_3416 = PROCEDURE_REF(BgL_envz00_3414, (int) (1L));
				BgL_arg1529z00_3417 = PROCEDURE_REF(BgL_envz00_3414, (int) (2L));
				if ((BgL_xz00_3418 == BgL_arg1529z00_3417))
					{	/* Match/descr.scm 124 */
						return BgL_arg1530z00_3416;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3415, BgL_xz00_3418);
					}
			}
		}

	}



/* &<@anonymous:1319>2156 */
	obj_t BGl_z62zc3z04anonymousza31319ze32156ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3419, obj_t BgL_xz00_3423)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3420;
				obj_t BgL_arg1546z00_3421;
				obj_t BgL_arg1544z00_3422;

				BgL_envz00_3420 = PROCEDURE_REF(BgL_envz00_3419, (int) (0L));
				BgL_arg1546z00_3421 = PROCEDURE_REF(BgL_envz00_3419, (int) (1L));
				BgL_arg1544z00_3422 = PROCEDURE_REF(BgL_envz00_3419, (int) (2L));
				if ((BgL_xz00_3423 == BgL_arg1544z00_3422))
					{	/* Match/descr.scm 124 */
						return BgL_arg1546z00_3421;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3420, BgL_xz00_3423);
					}
			}
		}

	}



/* &<@anonymous:1319>2157 */
	obj_t BGl_z62zc3z04anonymousza31319ze32157ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3424, obj_t BgL_xz00_3428)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3425;
				obj_t BgL_descrz00_3426;
				obj_t BgL_arg1565z00_3427;

				BgL_envz00_3425 = PROCEDURE_REF(BgL_envz00_3424, (int) (0L));
				BgL_descrz00_3426 = PROCEDURE_REF(BgL_envz00_3424, (int) (1L));
				BgL_arg1565z00_3427 = PROCEDURE_REF(BgL_envz00_3424, (int) (2L));
				if ((BgL_xz00_3428 == BgL_arg1565z00_3427))
					{	/* Match/descr.scm 124 */
						return BgL_descrz00_3426;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3425, BgL_xz00_3428);
					}
			}
		}

	}



/* &<@anonymous:1319>2158 */
	obj_t BGl_z62zc3z04anonymousza31319ze32158ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3429, obj_t BgL_xz00_3433)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3430;
				obj_t BgL_patz00_3431;
				obj_t BgL_arg1583z00_3432;

				BgL_envz00_3430 = PROCEDURE_REF(BgL_envz00_3429, (int) (0L));
				BgL_patz00_3431 = PROCEDURE_REF(BgL_envz00_3429, (int) (1L));
				BgL_arg1583z00_3432 = PROCEDURE_REF(BgL_envz00_3429, (int) (2L));
				if ((BgL_xz00_3433 == BgL_arg1583z00_3432))
					{	/* Match/descr.scm 124 */
						return BgL_patz00_3431;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3430, BgL_xz00_3433);
					}
			}
		}

	}



/* may-be-a-cons */
	bool_t BGl_mayzd2bezd2azd2conszd2zz__match_descriptionsz00(obj_t
		BgL_descrz00_36)
	{
		{	/* Match/descr.scm 411 */
		BGl_mayzd2bezd2azd2conszd2zz__match_descriptionsz00:
			if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_descrz00_36,
					BGl_list2216z00zz__match_descriptionsz00))
				{	/* Match/descr.scm 412 */
					return ((bool_t) 0);
				}
			else
				{	/* Match/descr.scm 412 */
					if (
						(CAR(
								((obj_t) BgL_descrz00_36)) ==
							BGl_symbol2166z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 415 */
							bool_t BgL_test2389z00_4588;

							{	/* Match/descr.scm 415 */
								obj_t BgL_arg1602z00_1603;

								{	/* Match/descr.scm 415 */
									obj_t BgL_pairz00_2744;

									BgL_pairz00_2744 = CDR(((obj_t) BgL_descrz00_36));
									BgL_arg1602z00_1603 = CAR(BgL_pairz00_2744);
								}
								BgL_test2389z00_4588 =
									BGl_mayzd2bezd2azd2conszd2zz__match_descriptionsz00
									(BgL_arg1602z00_1603);
							}
							if (BgL_test2389z00_4588)
								{	/* Match/descr.scm 416 */
									obj_t BgL_arg1601z00_1602;

									{	/* Match/descr.scm 416 */
										obj_t BgL_pairz00_2750;

										{	/* Match/descr.scm 416 */
											obj_t BgL_pairz00_2749;

											BgL_pairz00_2749 = CDR(((obj_t) BgL_descrz00_36));
											BgL_pairz00_2750 = CDR(BgL_pairz00_2749);
										}
										BgL_arg1601z00_1602 = CAR(BgL_pairz00_2750);
									}
									{
										obj_t BgL_descrz00_4597;

										BgL_descrz00_4597 = BgL_arg1601z00_1602;
										BgL_descrz00_36 = BgL_descrz00_4597;
										goto BGl_mayzd2bezd2azd2conszd2zz__match_descriptionsz00;
									}
								}
							else
								{	/* Match/descr.scm 415 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Match/descr.scm 414 */
							return ((bool_t) 1);
						}
				}
		}

	}



/* match */
	obj_t BGl_matchz00zz__match_descriptionsz00(obj_t BgL_dz00_37,
		obj_t BgL_ez00_38, obj_t BgL_envz00_39, obj_t BgL_kz00_40,
		obj_t BgL_za7za7_41)
	{
		{	/* Match/descr.scm 423 */
		BGl_matchz00zz__match_descriptionsz00:
			{	/* Match/descr.scm 424 */
				obj_t BgL_casezd2valuezd2_1604;

				BgL_casezd2valuezd2_1604 = CAR(((obj_t) BgL_dz00_37));
				if (
					(BgL_casezd2valuezd2_1604 ==
						BGl_symbol2186z00zz__match_descriptionsz00))
					{	/* Match/descr.scm 424 */
						return BGL_PROCEDURE_CALL1(BgL_kz00_40, BgL_envz00_39);
					}
				else
					{	/* Match/descr.scm 424 */
						if (
							(BgL_casezd2valuezd2_1604 ==
								BGl_symbol2199z00zz__match_descriptionsz00))
							{	/* Match/descr.scm 427 */
								bool_t BgL_test2392z00_4608;

								{	/* Match/descr.scm 427 */
									bool_t BgL_test2393z00_4609;

									{	/* Match/descr.scm 427 */
										obj_t BgL_tmpz00_4610;

										{	/* Match/descr.scm 427 */
											obj_t BgL_pairz00_2757;

											BgL_pairz00_2757 = CDR(((obj_t) BgL_dz00_37));
											BgL_tmpz00_4610 = CAR(BgL_pairz00_2757);
										}
										BgL_test2393z00_4609 = (BgL_ez00_38 == BgL_tmpz00_4610);
									}
									if (BgL_test2393z00_4609)
										{	/* Match/descr.scm 427 */
											BgL_test2392z00_4608 = ((bool_t) 1);
										}
									else
										{	/* Match/descr.scm 427 */
											if (STRINGP(BgL_ez00_38))
												{	/* Match/descr.scm 427 */
													obj_t BgL_arg1610z00_1615;

													{	/* Match/descr.scm 427 */
														obj_t BgL_pairz00_2761;

														BgL_pairz00_2761 = CDR(((obj_t) BgL_dz00_37));
														BgL_arg1610z00_1615 = CAR(BgL_pairz00_2761);
													}
													{	/* Match/descr.scm 427 */
														long BgL_l1z00_2764;

														BgL_l1z00_2764 = STRING_LENGTH(BgL_ez00_38);
														if (
															(BgL_l1z00_2764 ==
																STRING_LENGTH(((obj_t) BgL_arg1610z00_1615))))
															{	/* Match/descr.scm 427 */
																int BgL_arg1940z00_2767;

																{	/* Match/descr.scm 427 */
																	char *BgL_auxz00_4627;
																	char *BgL_tmpz00_4625;

																	BgL_auxz00_4627 =
																		BSTRING_TO_STRING(
																		((obj_t) BgL_arg1610z00_1615));
																	BgL_tmpz00_4625 =
																		BSTRING_TO_STRING(BgL_ez00_38);
																	BgL_arg1940z00_2767 =
																		memcmp(BgL_tmpz00_4625, BgL_auxz00_4627,
																		BgL_l1z00_2764);
																}
																BgL_test2392z00_4608 =
																	((long) (BgL_arg1940z00_2767) == 0L);
															}
														else
															{	/* Match/descr.scm 427 */
																BgL_test2392z00_4608 = ((bool_t) 0);
															}
													}
												}
											else
												{	/* Match/descr.scm 427 */
													BgL_test2392z00_4608 = ((bool_t) 0);
												}
										}
								}
								if (BgL_test2392z00_4608)
									{	/* Match/descr.scm 427 */
										return BGL_PROCEDURE_CALL1(BgL_kz00_40, BgL_envz00_39);
									}
								else
									{	/* Match/descr.scm 427 */
										return BGL_PROCEDURE_CALL1(BgL_za7za7_41, BgL_envz00_39);
									}
							}
						else
							{	/* Match/descr.scm 424 */
								if (
									(BgL_casezd2valuezd2_1604 ==
										BGl_symbol2166z00zz__match_descriptionsz00))
									{	/* Match/descr.scm 430 */
										obj_t BgL_arg1613z00_1618;

										{	/* Match/descr.scm 430 */
											obj_t BgL_pairz00_2777;

											BgL_pairz00_2777 = CDR(((obj_t) BgL_dz00_37));
											BgL_arg1613z00_1618 = CAR(BgL_pairz00_2777);
										}
										{	/* Match/descr.scm 432 */
											obj_t BgL_zc3z04anonymousza31616ze3z87_3434;

											BgL_zc3z04anonymousza31616ze3z87_3434 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31616ze3ze5zz__match_descriptionsz00,
												(int) (1L), (int) (4L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31616ze3z87_3434,
												(int) (0L), BgL_dz00_37);
											PROCEDURE_SET(BgL_zc3z04anonymousza31616ze3z87_3434,
												(int) (1L), BgL_ez00_38);
											PROCEDURE_SET(BgL_zc3z04anonymousza31616ze3z87_3434,
												(int) (2L), BgL_kz00_40);
											PROCEDURE_SET(BgL_zc3z04anonymousza31616ze3z87_3434,
												(int) (3L), BgL_za7za7_41);
											{
												obj_t BgL_kz00_4658;
												obj_t BgL_dz00_4657;

												BgL_dz00_4657 = BgL_arg1613z00_1618;
												BgL_kz00_4658 = BgL_zc3z04anonymousza31616ze3z87_3434;
												BgL_kz00_40 = BgL_kz00_4658;
												BgL_dz00_37 = BgL_dz00_4657;
												goto BGl_matchz00zz__match_descriptionsz00;
											}
										}
									}
								else
									{	/* Match/descr.scm 424 */
										if (
											(BgL_casezd2valuezd2_1604 ==
												BGl_symbol2169z00zz__match_descriptionsz00))
											{	/* Match/descr.scm 424 */
												if (PAIRP(BgL_ez00_38))
													{	/* Match/descr.scm 436 */
														obj_t BgL_arg1620z00_1626;
														obj_t BgL_arg1621z00_1627;

														{	/* Match/descr.scm 436 */
															obj_t BgL_pairz00_2789;

															BgL_pairz00_2789 = CDR(((obj_t) BgL_dz00_37));
															BgL_arg1620z00_1626 = CAR(BgL_pairz00_2789);
														}
														BgL_arg1621z00_1627 = CAR(BgL_ez00_38);
														{	/* Match/descr.scm 438 */
															obj_t BgL_zc3z04anonymousza31623ze3z87_3435;

															BgL_zc3z04anonymousza31623ze3z87_3435 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31623ze3ze5zz__match_descriptionsz00,
																(int) (1L), (int) (4L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31623ze3z87_3435,
																(int) (0L), BgL_dz00_37);
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31623ze3z87_3435,
																(int) (1L), BgL_ez00_38);
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31623ze3z87_3435,
																(int) (2L), BgL_kz00_40);
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31623ze3z87_3435,
																(int) (3L), BgL_za7za7_41);
															{
																obj_t BgL_kz00_4680;
																obj_t BgL_ez00_4679;
																obj_t BgL_dz00_4678;

																BgL_dz00_4678 = BgL_arg1620z00_1626;
																BgL_ez00_4679 = BgL_arg1621z00_1627;
																BgL_kz00_4680 =
																	BgL_zc3z04anonymousza31623ze3z87_3435;
																BgL_kz00_40 = BgL_kz00_4680;
																BgL_ez00_38 = BgL_ez00_4679;
																BgL_dz00_37 = BgL_dz00_4678;
																goto BGl_matchz00zz__match_descriptionsz00;
															}
														}
													}
												else
													{	/* Match/descr.scm 435 */
														return
															BGL_PROCEDURE_CALL1(BgL_za7za7_41, BgL_envz00_39);
													}
											}
										else
											{	/* Match/descr.scm 424 */
												if (
													(BgL_casezd2valuezd2_1604 ==
														BGl_symbol2194z00zz__match_descriptionsz00))
													{	/* Match/descr.scm 445 */
														bool_t BgL_test2400z00_4687;

														{	/* Match/descr.scm 516 */
															obj_t BgL_tmpz00_4688;

															{	/* Match/descr.scm 516 */
																obj_t BgL_pairz00_2806;

																{	/* Match/descr.scm 445 */
																	obj_t BgL_pairz00_2804;

																	BgL_pairz00_2804 = CDR(((obj_t) BgL_dz00_37));
																	BgL_pairz00_2806 = CAR(BgL_pairz00_2804);
																}
																BgL_tmpz00_4688 = CAR(BgL_pairz00_2806);
															}
															BgL_test2400z00_4687 =
																(BgL_tmpz00_4688 ==
																BGl_symbol2178z00zz__match_descriptionsz00);
														}
														if (BgL_test2400z00_4687)
															{	/* Match/descr.scm 446 */
																obj_t BgL_sz00_1637;

																BgL_sz00_1637 =
																	BGL_PROCEDURE_CALL1
																	(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
																	BGl_string2213z00zz__match_descriptionsz00);
																{	/* Match/descr.scm 447 */
																	obj_t BgL_arg1629z00_1638;

																	{	/* Match/descr.scm 447 */
																		obj_t BgL_arg1630z00_1639;
																		obj_t BgL_arg1631z00_1640;

																		{	/* Match/descr.scm 447 */
																			obj_t BgL_pairz00_2814;

																			{	/* Match/descr.scm 447 */
																				obj_t BgL_pairz00_2813;

																				{	/* Match/descr.scm 447 */
																					obj_t BgL_pairz00_2812;

																					BgL_pairz00_2812 =
																						CDR(((obj_t) BgL_dz00_37));
																					BgL_pairz00_2813 =
																						CAR(BgL_pairz00_2812);
																				}
																				BgL_pairz00_2814 =
																					CDR(BgL_pairz00_2813);
																			}
																			BgL_arg1630z00_1639 =
																				CAR(BgL_pairz00_2814);
																		}
																		{	/* Match/descr.scm 447 */
																			obj_t BgL_arg1634z00_1641;

																			{	/* Match/descr.scm 447 */
																				obj_t BgL_arg1636z00_1642;

																				{	/* Match/descr.scm 447 */
																					obj_t BgL_arg1637z00_1643;

																					BgL_arg1637z00_1643 =
																						MAKE_YOUNG_PAIR(BgL_sz00_1637,
																						BNIL);
																					BgL_arg1636z00_1642 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2199z00zz__match_descriptionsz00,
																						BgL_arg1637z00_1643);
																				}
																				BgL_arg1634z00_1641 =
																					MAKE_YOUNG_PAIR(BgL_arg1636z00_1642,
																					BNIL);
																			}
																			BgL_arg1631z00_1640 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2194z00zz__match_descriptionsz00,
																				BgL_arg1634z00_1641);
																		}
																		{	/* Match/descr.scm 124 */
																			obj_t
																				BgL_zc3z04anonymousza31319ze3z87_3436;
																			BgL_zc3z04anonymousza31319ze3z87_3436 =
																				MAKE_FX_PROCEDURE
																				(BGl_z62zc3z04anonymousza31319ze32159ze5zz__match_descriptionsz00,
																				(int) (1L), (int) (3L));
																			PROCEDURE_SET
																				(BgL_zc3z04anonymousza31319ze3z87_3436,
																				(int) (0L), BgL_envz00_39);
																			PROCEDURE_SET
																				(BgL_zc3z04anonymousza31319ze3z87_3436,
																				(int) (1L), BgL_arg1631z00_1640);
																			PROCEDURE_SET
																				(BgL_zc3z04anonymousza31319ze3z87_3436,
																				(int) (2L), BgL_arg1630z00_1639);
																			BgL_arg1629z00_1638 =
																				BgL_zc3z04anonymousza31319ze3z87_3436;
																	}}
																	return
																		BGL_PROCEDURE_CALL1(BgL_kz00_40,
																		BgL_arg1629z00_1638);
																}
															}
														else
															{	/* Match/descr.scm 448 */
																obj_t BgL_arg1638z00_1644;

																{	/* Match/descr.scm 448 */
																	obj_t BgL_pairz00_2820;

																	BgL_pairz00_2820 = CDR(((obj_t) BgL_dz00_37));
																	BgL_arg1638z00_1644 = CAR(BgL_pairz00_2820);
																}
																{
																	obj_t BgL_za7za7_4725;
																	obj_t BgL_kz00_4724;
																	obj_t BgL_dz00_4723;

																	BgL_dz00_4723 = BgL_arg1638z00_1644;
																	BgL_kz00_4724 = BgL_za7za7_41;
																	BgL_za7za7_4725 = BgL_kz00_40;
																	BgL_za7za7_41 = BgL_za7za7_4725;
																	BgL_kz00_40 = BgL_kz00_4724;
																	BgL_dz00_37 = BgL_dz00_4723;
																	goto BGl_matchz00zz__match_descriptionsz00;
																}
															}
													}
												else
													{	/* Match/descr.scm 424 */
														if (
															(BgL_casezd2valuezd2_1604 ==
																BGl_symbol2180z00zz__match_descriptionsz00))
															{	/* Match/descr.scm 453 */
																obj_t BgL_fun1643z00_1649;

																{	/* Match/descr.scm 453 */
																	obj_t BgL_arg1641z00_1647;
																	obj_t BgL_arg1642z00_1648;

																	{	/* Match/descr.scm 453 */
																		obj_t BgL_pairz00_2827;

																		{	/* Match/descr.scm 453 */
																			obj_t BgL_pairz00_2826;

																			BgL_pairz00_2826 =
																				CDR(((obj_t) BgL_dz00_37));
																			BgL_pairz00_2827 = CDR(BgL_pairz00_2826);
																		}
																		BgL_arg1641z00_1647 = CAR(BgL_pairz00_2827);
																	}
																	{	/* Match/descr.scm 453 */
																		obj_t BgL_pairz00_2833;

																		{	/* Match/descr.scm 453 */
																			obj_t BgL_pairz00_2832;

																			BgL_pairz00_2832 =
																				CDR(((obj_t) BgL_ez00_38));
																			BgL_pairz00_2833 = CDR(BgL_pairz00_2832);
																		}
																		BgL_arg1642z00_1648 = CAR(BgL_pairz00_2833);
																	}
																	BgL_fun1643z00_1649 =
																		BGl_matchz00zz__match_descriptionsz00
																		(BgL_arg1641z00_1647, BgL_arg1642z00_1648,
																		BgL_envz00_39, BgL_kz00_40, BgL_za7za7_41);
																}
																return
																	BGL_PROCEDURE_CALL1(BgL_fun1643z00_1649,
																	BINT(0L));
															}
														else
															{	/* Match/descr.scm 424 */
																if (
																	(BgL_casezd2valuezd2_1604 ==
																		BGl_symbol2171z00zz__match_descriptionsz00))
																	{	/* Match/descr.scm 456 */
																		obj_t BgL_zc3z04anonymousza31645ze3z87_3438;

																		BgL_zc3z04anonymousza31645ze3z87_3438 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31645ze3ze5zz__match_descriptionsz00,
																			(int) (1L), (int) (5L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31645ze3z87_3438,
																			(int) (0L), BgL_dz00_37);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31645ze3z87_3438,
																			(int) (1L), BgL_ez00_38);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31645ze3z87_3438,
																			(int) (2L), BgL_kz00_40);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31645ze3z87_3438,
																			(int) (3L), BgL_za7za7_41);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31645ze3z87_3438,
																			(int) (4L), BgL_envz00_39);
																		return
																			BgL_zc3z04anonymousza31645ze3z87_3438;
																	}
																else
																	{	/* Match/descr.scm 424 */
																		if (
																			(BgL_casezd2valuezd2_1604 ==
																				BGl_symbol2182z00zz__match_descriptionsz00))
																			{	/* Match/descr.scm 467 */
																				obj_t
																					BgL_zc3z04anonymousza31658ze3z87_3439;
																				BgL_zc3z04anonymousza31658ze3z87_3439 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza31658ze3ze5zz__match_descriptionsz00,
																					(int) (1L), (int) (2L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31658ze3z87_3439,
																					(int) (0L), BgL_kz00_40);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31658ze3z87_3439,
																					(int) (1L), BgL_envz00_39);
																				return
																					BgL_zc3z04anonymousza31658ze3z87_3439;
																			}
																		else
																			{	/* Match/descr.scm 424 */
																				if (
																					(BgL_casezd2valuezd2_1604 ==
																						BGl_symbol2178z00zz__match_descriptionsz00))
																					{	/* Match/descr.scm 469 */
																						bool_t BgL_test2405z00_4768;

																						{	/* Match/descr.scm 469 */
																							obj_t BgL_arg1678z00_1679;

																							{	/* Match/descr.scm 469 */
																								obj_t BgL_arg1681z00_1680;

																								{	/* Match/descr.scm 469 */
																									obj_t BgL_pairz00_2859;

																									BgL_pairz00_2859 =
																										CDR(((obj_t) BgL_dz00_37));
																									BgL_arg1681z00_1680 =
																										CAR(BgL_pairz00_2859);
																								}
																								BgL_arg1678z00_1679 =
																									BGL_PROCEDURE_CALL1
																									(BgL_envz00_39,
																									BgL_arg1681z00_1680);
																							}
																							BgL_test2405z00_4768 =
																								(BgL_arg1678z00_1679 ==
																								BGl_symbol2209z00zz__match_descriptionsz00);
																						}
																						if (BgL_test2405z00_4768)
																							{	/* Match/descr.scm 470 */
																								obj_t BgL_arg1667z00_1672;

																								{	/* Match/descr.scm 470 */
																									obj_t BgL_arg1668z00_1673;

																									{	/* Match/descr.scm 470 */
																										obj_t BgL_pairz00_2863;

																										BgL_pairz00_2863 =
																											CDR(
																											((obj_t) BgL_dz00_37));
																										BgL_arg1668z00_1673 =
																											CAR(BgL_pairz00_2863);
																									}
																									{	/* Match/descr.scm 124 */
																										obj_t
																											BgL_zc3z04anonymousza31319ze3z87_3440;
																										BgL_zc3z04anonymousza31319ze3z87_3440
																											=
																											MAKE_FX_PROCEDURE
																											(BGl_z62zc3z04anonymousza31319ze32160ze5zz__match_descriptionsz00,
																											(int) (1L), (int) (3L));
																										PROCEDURE_SET
																											(BgL_zc3z04anonymousza31319ze3z87_3440,
																											(int) (0L),
																											BgL_envz00_39);
																										PROCEDURE_SET
																											(BgL_zc3z04anonymousza31319ze3z87_3440,
																											(int) (1L), BgL_ez00_38);
																										PROCEDURE_SET
																											(BgL_zc3z04anonymousza31319ze3z87_3440,
																											(int) (2L),
																											BgL_arg1668z00_1673);
																										BgL_arg1667z00_1672 =
																											BgL_zc3z04anonymousza31319ze3z87_3440;
																								}}
																								return
																									BGL_PROCEDURE_CALL1
																									(BgL_kz00_40,
																									BgL_arg1667z00_1672);
																							}
																						else
																							{	/* Match/descr.scm 471 */
																								bool_t BgL_test2406z00_4793;

																								{	/* Match/descr.scm 471 */
																									obj_t BgL_arg1675z00_1677;

																									{	/* Match/descr.scm 471 */
																										obj_t BgL_arg1676z00_1678;

																										{	/* Match/descr.scm 471 */
																											obj_t BgL_pairz00_2869;

																											BgL_pairz00_2869 =
																												CDR(
																												((obj_t) BgL_dz00_37));
																											BgL_arg1676z00_1678 =
																												CAR(BgL_pairz00_2869);
																										}
																										BgL_arg1675z00_1677 =
																											BGL_PROCEDURE_CALL1
																											(BgL_envz00_39,
																											BgL_arg1676z00_1678);
																									}
																									BgL_test2406z00_4793 =
																										(BgL_arg1675z00_1677 ==
																										BgL_ez00_38);
																								}
																								if (BgL_test2406z00_4793)
																									{	/* Match/descr.scm 471 */
																										return
																											BGL_PROCEDURE_CALL1
																											(BgL_kz00_40,
																											BgL_envz00_39);
																									}
																								else
																									{	/* Match/descr.scm 471 */
																										return
																											BGL_PROCEDURE_CALL1
																											(BgL_za7za7_41,
																											BgL_envz00_39);
																									}
																							}
																					}
																				else
																					{	/* Match/descr.scm 424 */
																						return BUNSPEC;
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &<@anonymous:1616> */
	obj_t BGl_z62zc3z04anonymousza31616ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3441, obj_t BgL_envz00_3446)
	{
		{	/* Match/descr.scm 431 */
			{	/* Match/descr.scm 432 */
				obj_t BgL_dz00_3442;
				obj_t BgL_ez00_3443;
				obj_t BgL_kz00_3444;
				obj_t BgL_za7za7_3445;

				BgL_dz00_3442 = PROCEDURE_REF(BgL_envz00_3441, (int) (0L));
				BgL_ez00_3443 = PROCEDURE_REF(BgL_envz00_3441, (int) (1L));
				BgL_kz00_3444 = PROCEDURE_REF(BgL_envz00_3441, (int) (2L));
				BgL_za7za7_3445 = PROCEDURE_REF(BgL_envz00_3441, (int) (3L));
				{	/* Match/descr.scm 432 */
					obj_t BgL_arg1617z00_3597;

					{	/* Match/descr.scm 432 */
						obj_t BgL_pairz00_3598;

						{	/* Match/descr.scm 432 */
							obj_t BgL_pairz00_3599;

							BgL_pairz00_3599 = CDR(((obj_t) BgL_dz00_3442));
							BgL_pairz00_3598 = CDR(BgL_pairz00_3599);
						}
						BgL_arg1617z00_3597 = CAR(BgL_pairz00_3598);
					}
					return
						BGl_matchz00zz__match_descriptionsz00(BgL_arg1617z00_3597,
						BgL_ez00_3443, BgL_envz00_3446, BgL_kz00_3444, BgL_za7za7_3445);
				}
			}
		}

	}



/* &<@anonymous:1623> */
	obj_t BGl_z62zc3z04anonymousza31623ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3447, obj_t BgL_envz00_3452)
	{
		{	/* Match/descr.scm 437 */
			{	/* Match/descr.scm 438 */
				obj_t BgL_dz00_3448;
				obj_t BgL_ez00_3449;
				obj_t BgL_kz00_3450;
				obj_t BgL_za7za7_3451;

				BgL_dz00_3448 = PROCEDURE_REF(BgL_envz00_3447, (int) (0L));
				BgL_ez00_3449 = PROCEDURE_REF(BgL_envz00_3447, (int) (1L));
				BgL_kz00_3450 = PROCEDURE_REF(BgL_envz00_3447, (int) (2L));
				BgL_za7za7_3451 = PROCEDURE_REF(BgL_envz00_3447, (int) (3L));
				{	/* Match/descr.scm 438 */
					obj_t BgL_arg1624z00_3600;
					obj_t BgL_arg1625z00_3601;

					{	/* Match/descr.scm 438 */
						obj_t BgL_pairz00_3602;

						{	/* Match/descr.scm 438 */
							obj_t BgL_pairz00_3603;

							BgL_pairz00_3603 = CDR(((obj_t) BgL_dz00_3448));
							BgL_pairz00_3602 = CDR(BgL_pairz00_3603);
						}
						BgL_arg1624z00_3600 = CAR(BgL_pairz00_3602);
					}
					BgL_arg1625z00_3601 = CDR(((obj_t) BgL_ez00_3449));
					return
						BGl_matchz00zz__match_descriptionsz00(BgL_arg1624z00_3600,
						BgL_arg1625z00_3601, BgL_envz00_3452, BgL_kz00_3450,
						BgL_za7za7_3451);
				}
			}
		}

	}



/* &<@anonymous:1319>2159 */
	obj_t BGl_z62zc3z04anonymousza31319ze32159ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3453, obj_t BgL_xz00_3457)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3454;
				obj_t BgL_arg1631z00_3455;
				obj_t BgL_arg1630z00_3456;

				BgL_envz00_3454 = PROCEDURE_REF(BgL_envz00_3453, (int) (0L));
				BgL_arg1631z00_3455 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3453, (int) (1L)));
				BgL_arg1630z00_3456 = PROCEDURE_REF(BgL_envz00_3453, (int) (2L));
				if ((BgL_xz00_3457 == BgL_arg1630z00_3456))
					{	/* Match/descr.scm 124 */
						return BgL_arg1631z00_3455;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3454, BgL_xz00_3457);
					}
			}
		}

	}



/* &<@anonymous:1645> */
	obj_t BGl_z62zc3z04anonymousza31645ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3458, obj_t BgL_iz00_3464)
	{
		{	/* Match/descr.scm 456 */
			{	/* Match/descr.scm 457 */
				obj_t BgL_dz00_3459;
				obj_t BgL_ez00_3460;
				obj_t BgL_kz00_3461;
				obj_t BgL_za7za7_3462;
				obj_t BgL_envz00_3463;

				BgL_dz00_3459 = PROCEDURE_REF(BgL_envz00_3458, (int) (0L));
				BgL_ez00_3460 = PROCEDURE_REF(BgL_envz00_3458, (int) (1L));
				BgL_kz00_3461 = PROCEDURE_REF(BgL_envz00_3458, (int) (2L));
				BgL_za7za7_3462 = PROCEDURE_REF(BgL_envz00_3458, (int) (3L));
				BgL_envz00_3463 = PROCEDURE_REF(BgL_envz00_3458, (int) (4L));
				if (
					((long) CINT(BgL_iz00_3464) >=
						VECTOR_LENGTH(((obj_t) BgL_ez00_3460))))
					{	/* Match/descr.scm 457 */
						return BGL_PROCEDURE_CALL1(BgL_kz00_3461, BgL_envz00_3463);
					}
				else
					{	/* Match/descr.scm 459 */
						obj_t BgL_arg1648z00_3604;
						obj_t BgL_arg1649z00_3605;

						{	/* Match/descr.scm 459 */
							obj_t BgL_pairz00_3606;

							BgL_pairz00_3606 = CDR(((obj_t) BgL_dz00_3459));
							BgL_arg1648z00_3604 = CAR(BgL_pairz00_3606);
						}
						{	/* Match/descr.scm 459 */
							long BgL_kz00_3607;

							BgL_kz00_3607 = (long) CINT(BgL_iz00_3464);
							BgL_arg1649z00_3605 =
								VECTOR_REF(((obj_t) BgL_ez00_3460), BgL_kz00_3607);
						}
						{	/* Match/descr.scm 462 */
							obj_t BgL_zc3z04anonymousza31651ze3z87_3608;

							BgL_zc3z04anonymousza31651ze3z87_3608 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31651ze3ze5zz__match_descriptionsz00,
								(int) (1L), (int) (5L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31651ze3z87_3608, (int) (0L),
								BgL_dz00_3459);
							PROCEDURE_SET(BgL_zc3z04anonymousza31651ze3z87_3608, (int) (1L),
								BgL_ez00_3460);
							PROCEDURE_SET(BgL_zc3z04anonymousza31651ze3z87_3608, (int) (2L),
								BgL_kz00_3461);
							PROCEDURE_SET(BgL_zc3z04anonymousza31651ze3z87_3608, (int) (3L),
								BgL_za7za7_3462);
							PROCEDURE_SET(BgL_zc3z04anonymousza31651ze3z87_3608, (int) (4L),
								BgL_iz00_3464);
							return
								BGl_comparez00zz__match_descriptionsz00(BgL_arg1648z00_3604,
								BgL_arg1649z00_3605, BgL_envz00_3463,
								BgL_zc3z04anonymousza31651ze3z87_3608, BgL_za7za7_3462);
						}
					}
			}
		}

	}



/* &<@anonymous:1651> */
	obj_t BGl_z62zc3z04anonymousza31651ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3465, obj_t BgL_envz00_3471)
	{
		{	/* Match/descr.scm 461 */
			{	/* Match/descr.scm 462 */
				obj_t BgL_dz00_3466;
				obj_t BgL_ez00_3467;
				obj_t BgL_kz00_3468;
				obj_t BgL_za7za7_3469;
				obj_t BgL_iz00_3470;

				BgL_dz00_3466 = PROCEDURE_REF(BgL_envz00_3465, (int) (0L));
				BgL_ez00_3467 = PROCEDURE_REF(BgL_envz00_3465, (int) (1L));
				BgL_kz00_3468 = PROCEDURE_REF(BgL_envz00_3465, (int) (2L));
				BgL_za7za7_3469 = PROCEDURE_REF(BgL_envz00_3465, (int) (3L));
				BgL_iz00_3470 = PROCEDURE_REF(BgL_envz00_3465, (int) (4L));
				{	/* Match/descr.scm 462 */
					obj_t BgL_fun1654z00_3609;

					{	/* Match/descr.scm 462 */
						obj_t BgL_arg1652z00_3610;

						{	/* Match/descr.scm 462 */
							obj_t BgL_pairz00_3611;

							{	/* Match/descr.scm 462 */
								obj_t BgL_pairz00_3612;

								BgL_pairz00_3612 = CDR(((obj_t) BgL_dz00_3466));
								BgL_pairz00_3611 = CDR(BgL_pairz00_3612);
							}
							BgL_arg1652z00_3610 = CAR(BgL_pairz00_3611);
						}
						BgL_fun1654z00_3609 =
							BGl_matchz00zz__match_descriptionsz00(BgL_arg1652z00_3610,
							BgL_ez00_3467, BgL_envz00_3471, BgL_kz00_3468, BgL_za7za7_3469);
					}
					{	/* Match/descr.scm 463 */
						long BgL_arg1653z00_3613;

						BgL_arg1653z00_3613 = ((long) CINT(BgL_iz00_3470) + 1L);
						return
							BGL_PROCEDURE_CALL1(BgL_fun1654z00_3609,
							BINT(BgL_arg1653z00_3613));
					}
				}
			}
		}

	}



/* &<@anonymous:1658> */
	obj_t BGl_z62zc3z04anonymousza31658ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3472, obj_t BgL_iz00_3475)
	{
		{	/* Match/descr.scm 467 */
			{	/* Match/descr.scm 467 */
				obj_t BgL_kz00_3473;
				obj_t BgL_envz00_3474;

				BgL_kz00_3473 = PROCEDURE_REF(BgL_envz00_3472, (int) (0L));
				BgL_envz00_3474 = PROCEDURE_REF(BgL_envz00_3472, (int) (1L));
				return BGL_PROCEDURE_CALL1(BgL_kz00_3473, BgL_envz00_3474);
			}
		}

	}



/* &<@anonymous:1319>2160 */
	obj_t BGl_z62zc3z04anonymousza31319ze32160ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3476, obj_t BgL_xz00_3480)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3477;
				obj_t BgL_ez00_3478;
				obj_t BgL_arg1668z00_3479;

				BgL_envz00_3477 = PROCEDURE_REF(BgL_envz00_3476, (int) (0L));
				BgL_ez00_3478 = PROCEDURE_REF(BgL_envz00_3476, (int) (1L));
				BgL_arg1668z00_3479 = PROCEDURE_REF(BgL_envz00_3476, (int) (2L));
				if ((BgL_xz00_3480 == BgL_arg1668z00_3479))
					{	/* Match/descr.scm 124 */
						return BgL_ez00_3478;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3477, BgL_xz00_3480);
					}
			}
		}

	}



/* alpha-convert */
	obj_t BGl_alphazd2convertzd2zz__match_descriptionsz00(obj_t BgL_fz00_42)
	{
		{	/* Match/descr.scm 479 */
			return
				BGl_z62loopz62zz__match_descriptionsz00(BgL_fz00_42,
				BGl_proc2219z00zz__match_descriptionsz00,
				BGl_proc2218z00zz__match_descriptionsz00);
		}

	}



/* &loop */
	obj_t BGl_z62loopz62zz__match_descriptionsz00(obj_t BgL_fz00_1684,
		obj_t BgL_envz00_1685, obj_t BgL_kz00_1686)
	{
		{	/* Match/descr.scm 480 */
		BGl_z62loopz62zz__match_descriptionsz00:
			if (PAIRP(BgL_fz00_1684))
				{	/* Match/descr.scm 484 */
					if (
						(CAR(BgL_fz00_1684) == BGl_symbol2199z00zz__match_descriptionsz00))
						{	/* Match/descr.scm 486 */
							return
								((obj_t(*)(obj_t, obj_t,
										obj_t)) PROCEDURE_L_ENTRY(BgL_kz00_1686)) (BgL_kz00_1686,
								BgL_fz00_1684, BgL_envz00_1685);
						}
					else
						{	/* Match/descr.scm 486 */
							if (
								(CAR(BgL_fz00_1684) ==
									BGl_symbol2178z00zz__match_descriptionsz00))
								{	/* Match/descr.scm 489 */
									bool_t BgL_test2413z00_4946;

									{	/* Match/descr.scm 489 */
										obj_t BgL_arg1709z00_1707;

										{	/* Match/descr.scm 489 */
											obj_t BgL_arg1710z00_1708;

											BgL_arg1710z00_1708 = CAR(CDR(BgL_fz00_1684));
											BgL_arg1709z00_1707 =
												BGL_PROCEDURE_CALL1(BgL_envz00_1685,
												BgL_arg1710z00_1708);
										}
										BgL_test2413z00_4946 =
											(BgL_arg1709z00_1707 ==
											BGl_symbol2209z00zz__match_descriptionsz00);
									}
									if (BgL_test2413z00_4946)
										{	/* Match/descr.scm 490 */
											obj_t BgL_sz00_1696;

											BgL_sz00_1696 =
												BGL_PROCEDURE_CALL1
												(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
												BGl_string2220z00zz__match_descriptionsz00);
											{	/* Match/descr.scm 491 */
												obj_t BgL_arg1699z00_1697;
												obj_t BgL_arg1700z00_1698;

												{	/* Match/descr.scm 491 */
													obj_t BgL_list1701z00_1699;

													{	/* Match/descr.scm 491 */
														obj_t BgL_arg1702z00_1700;

														BgL_arg1702z00_1700 =
															MAKE_YOUNG_PAIR(BgL_sz00_1696, BNIL);
														BgL_list1701z00_1699 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2178z00zz__match_descriptionsz00,
															BgL_arg1702z00_1700);
													}
													BgL_arg1699z00_1697 = BgL_list1701z00_1699;
												}
												{	/* Match/descr.scm 491 */
													obj_t BgL_arg1703z00_1701;

													BgL_arg1703z00_1701 = CAR(CDR(BgL_fz00_1684));
													{	/* Match/descr.scm 124 */
														obj_t BgL_zc3z04anonymousza31319ze3z87_3481;

														BgL_zc3z04anonymousza31319ze3z87_3481 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31319ze32161ze5zz__match_descriptionsz00,
															(int) (1L), (int) (3L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3481,
															(int) (0L), BgL_envz00_1685);
														PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3481,
															(int) (1L), BgL_sz00_1696);
														PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3481,
															(int) (2L), BgL_arg1703z00_1701);
														BgL_arg1700z00_1698 =
															BgL_zc3z04anonymousza31319ze3z87_3481;
												}}
												return
													((obj_t(*)(obj_t, obj_t,
															obj_t))
													PROCEDURE_L_ENTRY(BgL_kz00_1686)) (BgL_kz00_1686,
													BgL_arg1699z00_1697, BgL_arg1700z00_1698);
											}
										}
									else
										{	/* Match/descr.scm 492 */
											obj_t BgL_arg1704z00_1702;

											{	/* Match/descr.scm 492 */
												obj_t BgL_arg1705z00_1703;

												{	/* Match/descr.scm 492 */
													obj_t BgL_arg1708z00_1706;

													BgL_arg1708z00_1706 = CAR(CDR(BgL_fz00_1684));
													BgL_arg1705z00_1703 =
														BGL_PROCEDURE_CALL1(BgL_envz00_1685,
														BgL_arg1708z00_1706);
												}
												{	/* Match/descr.scm 492 */
													obj_t BgL_list1706z00_1704;

													{	/* Match/descr.scm 492 */
														obj_t BgL_arg1707z00_1705;

														BgL_arg1707z00_1705 =
															MAKE_YOUNG_PAIR(BgL_arg1705z00_1703, BNIL);
														BgL_list1706z00_1704 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2178z00zz__match_descriptionsz00,
															BgL_arg1707z00_1705);
													}
													BgL_arg1704z00_1702 = BgL_list1706z00_1704;
												}
											}
											return
												((obj_t(*)(obj_t, obj_t,
														obj_t))
												PROCEDURE_L_ENTRY(BgL_kz00_1686)) (BgL_kz00_1686,
												BgL_arg1704z00_1702, BgL_envz00_1685);
										}
								}
							else
								{	/* Match/descr.scm 493 */
									obj_t BgL_arg1711z00_1709;

									BgL_arg1711z00_1709 = CAR(BgL_fz00_1684);
									{	/* Match/descr.scm 495 */
										obj_t BgL_zc3z04anonymousza31715ze3z87_3483;

										{
											int BgL_tmpz00_4990;

											BgL_tmpz00_4990 = (int) (2L);
											BgL_zc3z04anonymousza31715ze3z87_3483 =
												MAKE_L_PROCEDURE
												(BGl_z62zc3z04anonymousza31715ze3ze5zz__match_descriptionsz00,
												BgL_tmpz00_4990);
										}
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31715ze3z87_3483,
											(int) (0L), BgL_fz00_1684);
										PROCEDURE_L_SET(BgL_zc3z04anonymousza31715ze3z87_3483,
											(int) (1L), BgL_kz00_1686);
										{
											obj_t BgL_kz00_4998;
											obj_t BgL_fz00_4997;

											BgL_fz00_4997 = BgL_arg1711z00_1709;
											BgL_kz00_4998 = BgL_zc3z04anonymousza31715ze3z87_3483;
											BgL_kz00_1686 = BgL_kz00_4998;
											BgL_fz00_1684 = BgL_fz00_4997;
											goto BGl_z62loopz62zz__match_descriptionsz00;
										}
									}
								}
						}
				}
			else
				{	/* Match/descr.scm 484 */
					return
						((obj_t(*)(obj_t, obj_t,
								obj_t)) PROCEDURE_L_ENTRY(BgL_kz00_1686)) (BgL_kz00_1686,
						BgL_fz00_1684, BgL_envz00_1685);
				}
		}

	}



/* &g1051 */
	obj_t BGl_z62g1051z62zz__match_descriptionsz00(obj_t BgL_envz00_3487,
		obj_t BgL_fz00_3488, obj_t BgL_ez00_3489)
	{
		{	/* Match/descr.scm 482 */
			return BgL_fz00_3488;
		}

	}



/* &g1050 */
	obj_t BGl_z62g1050z62zz__match_descriptionsz00(obj_t BgL_envz00_3490,
		obj_t BgL_xz00_3491)
	{
		{	/* Match/descr.scm 481 */
			return BGl_symbol2209z00zz__match_descriptionsz00;
		}

	}



/* &<@anonymous:1319>2161 */
	obj_t BGl_z62zc3z04anonymousza31319ze32161ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3492, obj_t BgL_xz00_3496)
	{
		{	/* Match/descr.scm 124 */
			{	/* Match/descr.scm 124 */
				obj_t BgL_envz00_3493;
				obj_t BgL_sz00_3494;
				obj_t BgL_arg1703z00_3495;

				BgL_envz00_3493 = ((obj_t) PROCEDURE_REF(BgL_envz00_3492, (int) (0L)));
				BgL_sz00_3494 = PROCEDURE_REF(BgL_envz00_3492, (int) (1L));
				BgL_arg1703z00_3495 = PROCEDURE_REF(BgL_envz00_3492, (int) (2L));
				if ((BgL_xz00_3496 == BgL_arg1703z00_3495))
					{	/* Match/descr.scm 124 */
						return BgL_sz00_3494;
					}
				else
					{	/* Match/descr.scm 124 */
						return BGL_PROCEDURE_CALL1(BgL_envz00_3493, BgL_xz00_3496);
					}
			}
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3497, obj_t BgL_fcarz00_3500, obj_t BgL_ez00_3501)
	{
		{	/* Match/descr.scm 494 */
			{	/* Match/descr.scm 495 */
				obj_t BgL_fz00_3498;
				obj_t BgL_kz00_3499;

				BgL_fz00_3498 = PROCEDURE_L_REF(BgL_envz00_3497, (int) (0L));
				BgL_kz00_3499 = ((obj_t) PROCEDURE_L_REF(BgL_envz00_3497, (int) (1L)));
				{	/* Match/descr.scm 495 */
					obj_t BgL_arg1717z00_3616;

					BgL_arg1717z00_3616 = CDR(((obj_t) BgL_fz00_3498));
					{	/* Match/descr.scm 497 */
						obj_t BgL_zc3z04anonymousza31719ze3z87_3617;

						{	/* Match/descr.scm 495 */
							int BgL_tmpz00_5024;

							BgL_tmpz00_5024 = (int) (2L);
							BgL_zc3z04anonymousza31719ze3z87_3617 =
								MAKE_L_PROCEDURE
								(BGl_z62zc3z04anonymousza31719ze3ze5zz__match_descriptionsz00,
								BgL_tmpz00_5024);
						}
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31719ze3z87_3617,
							(int) (0L), BgL_fcarz00_3500);
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31719ze3z87_3617,
							(int) (1L), BgL_kz00_3499);
						return
							BGl_z62loopz62zz__match_descriptionsz00(BgL_arg1717z00_3616,
							BgL_ez00_3501, BgL_zc3z04anonymousza31719ze3z87_3617);
					}
				}
			}
		}

	}



/* &<@anonymous:1719> */
	obj_t BGl_z62zc3z04anonymousza31719ze3ze5zz__match_descriptionsz00(obj_t
		BgL_envz00_3502, obj_t BgL_fcdrz00_3505, obj_t BgL_ez00_3506)
	{
		{	/* Match/descr.scm 496 */
			{	/* Match/descr.scm 497 */
				obj_t BgL_fcarz00_3503;
				obj_t BgL_kz00_3504;

				BgL_fcarz00_3503 = PROCEDURE_L_REF(BgL_envz00_3502, (int) (0L));
				BgL_kz00_3504 = ((obj_t) PROCEDURE_L_REF(BgL_envz00_3502, (int) (1L)));
				{	/* Match/descr.scm 497 */
					obj_t BgL_arg1720z00_3619;

					BgL_arg1720z00_3619 =
						MAKE_YOUNG_PAIR(BgL_fcarz00_3503, BgL_fcdrz00_3505);
					return
						((obj_t(*)(obj_t, obj_t,
								obj_t)) PROCEDURE_L_ENTRY(BgL_kz00_3504)) (BgL_kz00_3504,
						BgL_arg1720z00_3619, BgL_ez00_3506);
				}
			}
		}

	}



/* isAny? */
	BGL_EXPORTED_DEF obj_t BGl_isAnyzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_43)
	{
		{	/* Match/descr.scm 502 */
			if (
				(CAR(
						((obj_t) BgL_cz00_43)) ==
					BGl_symbol2186z00zz__match_descriptionsz00))
				{	/* Match/descr.scm 502 */
					return BTRUE;
				}
			else
				{	/* Match/descr.scm 502 */
					return
						BBOOL(
						(CAR(
								((obj_t) BgL_cz00_43)) ==
							BGl_symbol2188z00zz__match_descriptionsz00));
				}
		}

	}



/* &isAny? */
	obj_t BGl_z62isAnyzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3507,
		obj_t BgL_cz00_3508)
	{
		{	/* Match/descr.scm 502 */
			return BGl_isAnyzf3zf3zz__match_descriptionsz00(BgL_cz00_3508);
		}

	}



/* isCheck? */
	BGL_EXPORTED_DEF obj_t BGl_isCheckzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_44)
	{
		{	/* Match/descr.scm 506 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_44)) ==
					BGl_symbol2188z00zz__match_descriptionsz00));
		}

	}



/* &isCheck? */
	obj_t BGl_z62isCheckzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3509,
		obj_t BgL_cz00_3510)
	{
		{	/* Match/descr.scm 506 */
			return BGl_isCheckzf3zf3zz__match_descriptionsz00(BgL_cz00_3510);
		}

	}



/* isSuccess? */
	BGL_EXPORTED_DEF obj_t BGl_isSuccesszf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_45)
	{
		{	/* Match/descr.scm 508 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_45)) ==
					BGl_symbol2197z00zz__match_descriptionsz00));
		}

	}



/* &isSuccess? */
	obj_t BGl_z62isSuccesszf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3511,
		obj_t BgL_cz00_3512)
	{
		{	/* Match/descr.scm 508 */
			return BGl_isSuccesszf3zf3zz__match_descriptionsz00(BgL_cz00_3512);
		}

	}



/* isTop? */
	BGL_EXPORTED_DEF obj_t BGl_isTopzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_46)
	{
		{	/* Match/descr.scm 510 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_46)) ==
					BGl_symbol2221z00zz__match_descriptionsz00));
		}

	}



/* &isTop? */
	obj_t BGl_z62isTopzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3513,
		obj_t BgL_cz00_3514)
	{
		{	/* Match/descr.scm 510 */
			return BGl_isTopzf3zf3zz__match_descriptionsz00(BgL_cz00_3514);
		}

	}



/* isBottom? */
	BGL_EXPORTED_DEF obj_t BGl_isBottomzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_47)
	{
		{	/* Match/descr.scm 512 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_47)) ==
					BGl_symbol2192z00zz__match_descriptionsz00));
		}

	}



/* &isBottom? */
	obj_t BGl_z62isBottomzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3515,
		obj_t BgL_cz00_3516)
	{
		{	/* Match/descr.scm 512 */
			return BGl_isBottomzf3zf3zz__match_descriptionsz00(BgL_cz00_3516);
		}

	}



/* isQuote? */
	BGL_EXPORTED_DEF obj_t BGl_isQuotezf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_48)
	{
		{	/* Match/descr.scm 514 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_48)) ==
					BGl_symbol2199z00zz__match_descriptionsz00));
		}

	}



/* &isQuote? */
	obj_t BGl_z62isQuotezf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3517,
		obj_t BgL_cz00_3518)
	{
		{	/* Match/descr.scm 514 */
			return BGl_isQuotezf3zf3zz__match_descriptionsz00(BgL_cz00_3518);
		}

	}



/* isVar? */
	BGL_EXPORTED_DEF obj_t BGl_isVarzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_49)
	{
		{	/* Match/descr.scm 516 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_49)) ==
					BGl_symbol2178z00zz__match_descriptionsz00));
		}

	}



/* &isVar? */
	obj_t BGl_z62isVarzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3519,
		obj_t BgL_cz00_3520)
	{
		{	/* Match/descr.scm 516 */
			return BGl_isVarzf3zf3zz__match_descriptionsz00(BgL_cz00_3520);
		}

	}



/* isNot? */
	BGL_EXPORTED_DEF obj_t BGl_isNotzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_50)
	{
		{	/* Match/descr.scm 518 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_50)) ==
					BGl_symbol2194z00zz__match_descriptionsz00));
		}

	}



/* &isNot? */
	obj_t BGl_z62isNotzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3521,
		obj_t BgL_cz00_3522)
	{
		{	/* Match/descr.scm 518 */
			return BGl_isNotzf3zf3zz__match_descriptionsz00(BgL_cz00_3522);
		}

	}



/* isAnd? */
	BGL_EXPORTED_DEF obj_t BGl_isAndzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_51)
	{
		{	/* Match/descr.scm 520 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_51)) ==
					BGl_symbol2166z00zz__match_descriptionsz00));
		}

	}



/* &isAnd? */
	obj_t BGl_z62isAndzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3523,
		obj_t BgL_cz00_3524)
	{
		{	/* Match/descr.scm 520 */
			return BGl_isAndzf3zf3zz__match_descriptionsz00(BgL_cz00_3524);
		}

	}



/* isOr? */
	BGL_EXPORTED_DEF obj_t BGl_isOrzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_52)
	{
		{	/* Match/descr.scm 522 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_52)) ==
					BGl_symbol2162z00zz__match_descriptionsz00));
		}

	}



/* &isOr? */
	obj_t BGl_z62isOrzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3525,
		obj_t BgL_cz00_3526)
	{
		{	/* Match/descr.scm 522 */
			return BGl_isOrzf3zf3zz__match_descriptionsz00(BgL_cz00_3526);
		}

	}



/* isT-Or? */
	BGL_EXPORTED_DEF obj_t BGl_isTzd2Orzf3z21zz__match_descriptionsz00(obj_t
		BgL_cz00_53)
	{
		{	/* Match/descr.scm 524 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_53)) ==
					BGl_symbol2164z00zz__match_descriptionsz00));
		}

	}



/* &isT-Or? */
	obj_t BGl_z62isTzd2Orzf3z43zz__match_descriptionsz00(obj_t BgL_envz00_3527,
		obj_t BgL_cz00_3528)
	{
		{	/* Match/descr.scm 524 */
			return BGl_isTzd2Orzf3z21zz__match_descriptionsz00(BgL_cz00_3528);
		}

	}



/* isTagged-Or? */
	BGL_EXPORTED_DEF obj_t BGl_isTaggedzd2Orzf3z21zz__match_descriptionsz00(obj_t
		BgL_cz00_54)
	{
		{	/* Match/descr.scm 526 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_54)) ==
					BGl_symbol2211z00zz__match_descriptionsz00));
		}

	}



/* &isTagged-Or? */
	obj_t BGl_z62isTaggedzd2Orzf3z43zz__match_descriptionsz00(obj_t
		BgL_envz00_3529, obj_t BgL_cz00_3530)
	{
		{	/* Match/descr.scm 526 */
			return BGl_isTaggedzd2Orzf3z21zz__match_descriptionsz00(BgL_cz00_3530);
		}

	}



/* isCons? */
	BGL_EXPORTED_DEF obj_t BGl_isConszf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_55)
	{
		{	/* Match/descr.scm 528 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_55)) ==
					BGl_symbol2169z00zz__match_descriptionsz00));
		}

	}



/* &isCons? */
	obj_t BGl_z62isConszf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3531,
		obj_t BgL_cz00_3532)
	{
		{	/* Match/descr.scm 528 */
			return BGl_isConszf3zf3zz__match_descriptionsz00(BgL_cz00_3532);
		}

	}



/* isACons? */
	BGL_EXPORTED_DEF obj_t BGl_isAConszf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_56)
	{
		{	/* Match/descr.scm 530 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_56)) ==
					BGl_symbol2202z00zz__match_descriptionsz00));
		}

	}



/* &isACons? */
	obj_t BGl_z62isAConszf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3533,
		obj_t BgL_cz00_3534)
	{
		{	/* Match/descr.scm 530 */
			return BGl_isAConszf3zf3zz__match_descriptionsz00(BgL_cz00_3534);
		}

	}



/* isXCons? */
	BGL_EXPORTED_DEF obj_t BGl_isXConszf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_57)
	{
		{	/* Match/descr.scm 532 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_57)) ==
					BGl_symbol2204z00zz__match_descriptionsz00));
		}

	}



/* &isXCons? */
	obj_t BGl_z62isXConszf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3535,
		obj_t BgL_cz00_3536)
	{
		{	/* Match/descr.scm 532 */
			return BGl_isXConszf3zf3zz__match_descriptionsz00(BgL_cz00_3536);
		}

	}



/* isTimes? */
	BGL_EXPORTED_DEF obj_t BGl_isTimeszf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_58)
	{
		{	/* Match/descr.scm 534 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_58)) ==
					BGl_symbol2176z00zz__match_descriptionsz00));
		}

	}



/* &isTimes? */
	obj_t BGl_z62isTimeszf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3537,
		obj_t BgL_cz00_3538)
	{
		{	/* Match/descr.scm 534 */
			return BGl_isTimeszf3zf3zz__match_descriptionsz00(BgL_cz00_3538);
		}

	}



/* containsHole? */
	BGL_EXPORTED_DEF obj_t BGl_containsHolezf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_59)
	{
		{	/* Match/descr.scm 536 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_59)) ==
					BGl_symbol2223z00zz__match_descriptionsz00));
		}

	}



/* &containsHole? */
	obj_t BGl_z62containsHolezf3z91zz__match_descriptionsz00(obj_t
		BgL_envz00_3539, obj_t BgL_cz00_3540)
	{
		{	/* Match/descr.scm 536 */
			return BGl_containsHolezf3zf3zz__match_descriptionsz00(BgL_cz00_3540);
		}

	}



/* isHole? */
	BGL_EXPORTED_DEF obj_t BGl_isHolezf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_60)
	{
		{	/* Match/descr.scm 538 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_60)) ==
					BGl_symbol2223z00zz__match_descriptionsz00));
		}

	}



/* &isHole? */
	obj_t BGl_z62isHolezf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3541,
		obj_t BgL_cz00_3542)
	{
		{	/* Match/descr.scm 538 */
			return BGl_isHolezf3zf3zz__match_descriptionsz00(BgL_cz00_3542);
		}

	}



/* isTree? */
	BGL_EXPORTED_DEF obj_t BGl_isTreezf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_61)
	{
		{	/* Match/descr.scm 540 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_61)) ==
					BGl_symbol2174z00zz__match_descriptionsz00));
		}

	}



/* &isTree? */
	obj_t BGl_z62isTreezf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3543,
		obj_t BgL_cz00_3544)
	{
		{	/* Match/descr.scm 540 */
			return BGl_isTreezf3zf3zz__match_descriptionsz00(BgL_cz00_3544);
		}

	}



/* isVector? */
	BGL_EXPORTED_DEF obj_t BGl_isVectorzf3zf3zz__match_descriptionsz00(obj_t
		BgL_cz00_62)
	{
		{	/* Match/descr.scm 542 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_62)) ==
					BGl_symbol2214z00zz__match_descriptionsz00));
		}

	}



/* &isVector? */
	obj_t BGl_z62isVectorzf3z91zz__match_descriptionsz00(obj_t BgL_envz00_3545,
		obj_t BgL_cz00_3546)
	{
		{	/* Match/descr.scm 542 */
			return BGl_isVectorzf3zf3zz__match_descriptionsz00(BgL_cz00_3546);
		}

	}



/* isVector-begin? */
	BGL_EXPORTED_DEF obj_t
		BGl_isVectorzd2beginzf3z21zz__match_descriptionsz00(obj_t BgL_cz00_63)
	{
		{	/* Match/descr.scm 544 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_63)) ==
					BGl_symbol2180z00zz__match_descriptionsz00));
		}

	}



/* &isVector-begin? */
	obj_t BGl_z62isVectorzd2beginzf3z43zz__match_descriptionsz00(obj_t
		BgL_envz00_3547, obj_t BgL_cz00_3548)
	{
		{	/* Match/descr.scm 544 */
			return BGl_isVectorzd2beginzf3z21zz__match_descriptionsz00(BgL_cz00_3548);
		}

	}



/* isVector-end? */
	BGL_EXPORTED_DEF obj_t BGl_isVectorzd2endzf3z21zz__match_descriptionsz00(obj_t
		BgL_cz00_64)
	{
		{	/* Match/descr.scm 546 */
			return
				BBOOL(
				(CAR(
						((obj_t) BgL_cz00_64)) ==
					BGl_symbol2182z00zz__match_descriptionsz00));
		}

	}



/* &isVector-end? */
	obj_t BGl_z62isVectorzd2endzf3z43zz__match_descriptionsz00(obj_t
		BgL_envz00_3549, obj_t BgL_cz00_3550)
	{
		{	/* Match/descr.scm 546 */
			return BGl_isVectorzd2endzf3z21zz__match_descriptionsz00(BgL_cz00_3550);
		}

	}



/* vector-plus */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2pluszd2zz__match_descriptionsz00(obj_t
		BgL_vz00_65, obj_t BgL_iz00_66, obj_t BgL_dz00_67)
	{
		{	/* Match/descr.scm 555 */
			{	/* Match/descr.scm 556 */
				bool_t BgL_test2416z00_5157;

				{	/* Match/descr.scm 556 */
					long BgL_arg1759z00_1763;

					{	/* Match/descr.scm 556 */
						obj_t BgL_arg1760z00_1764;

						{	/* Match/descr.scm 556 */
							obj_t BgL_pairz00_2948;

							{	/* Match/descr.scm 556 */
								obj_t BgL_pairz00_2947;

								BgL_pairz00_2947 = CDR(((obj_t) BgL_vz00_65));
								BgL_pairz00_2948 = CDR(BgL_pairz00_2947);
							}
							BgL_arg1760z00_1764 = CAR(BgL_pairz00_2948);
						}
						BgL_arg1759z00_1763 = VECTOR_LENGTH(((obj_t) BgL_arg1760z00_1764));
					}
					BgL_test2416z00_5157 =
						((long) CINT(BgL_iz00_66) >= BgL_arg1759z00_1763);
				}
				if (BgL_test2416z00_5157)
					{	/* Match/descr.scm 557 */
						obj_t BgL_arg1756z00_1760;
						obj_t BgL_arg1757z00_1761;

						{	/* Match/descr.scm 557 */
							obj_t BgL_pairz00_2955;

							BgL_pairz00_2955 = CDR(((obj_t) BgL_vz00_65));
							BgL_arg1756z00_1760 = CDR(BgL_pairz00_2955);
						}
						{	/* Match/descr.scm 558 */
							obj_t BgL_arg1758z00_1762;

							{	/* Match/descr.scm 558 */
								obj_t BgL_pairz00_2961;

								{	/* Match/descr.scm 558 */
									obj_t BgL_pairz00_2960;

									BgL_pairz00_2960 = CDR(((obj_t) BgL_vz00_65));
									BgL_pairz00_2961 = CDR(BgL_pairz00_2960);
								}
								BgL_arg1758z00_1762 = CAR(BgL_pairz00_2961);
							}
							{	/* Match/descr.scm 558 */
								obj_t BgL_fillz00_2962;

								BgL_fillz00_2962 = BGl_list2196z00zz__match_descriptionsz00;
								{	/* Match/descr.scm 582 */
									obj_t BgL_resz00_2963;

									{	/* Match/descr.scm 583 */
										obj_t BgL_newzd2vectorzd2_2964;

										BgL_newzd2vectorzd2_2964 =
											make_vector((long) CINT(BgL_iz00_66), BgL_fillz00_2962);
										{
											long BgL_iz00_2966;

											BgL_iz00_2966 = 0L;
										BgL_loopz00_2965:
											if (
												(BgL_iz00_2966 ==
													VECTOR_LENGTH(((obj_t) BgL_arg1758z00_1762))))
												{	/* Match/descr.scm 585 */
													BgL_resz00_2963 = BgL_newzd2vectorzd2_2964;
												}
											else
												{	/* Match/descr.scm 585 */
													{	/* Match/descr.scm 588 */
														obj_t BgL_arg1798z00_2969;

														BgL_arg1798z00_2969 =
															VECTOR_REF(
															((obj_t) BgL_arg1758z00_1762), BgL_iz00_2966);
														VECTOR_SET(BgL_newzd2vectorzd2_2964, BgL_iz00_2966,
															BgL_arg1798z00_2969);
													}
													{
														long BgL_iz00_5182;

														BgL_iz00_5182 = (BgL_iz00_2966 + 1L);
														BgL_iz00_2966 = BgL_iz00_5182;
														goto BgL_loopz00_2965;
													}
												}
										}
									}
									BgL_arg1757z00_1761 = BgL_resz00_2963;
								}
							}
						}
						{	/* Match/descr.scm 557 */
							obj_t BgL_tmpz00_5184;

							BgL_tmpz00_5184 = ((obj_t) BgL_arg1756z00_1760);
							SET_CAR(BgL_tmpz00_5184, BgL_arg1757z00_1761);
						}
					}
				else
					{	/* Match/descr.scm 556 */
						BTRUE;
					}
			}
			{	/* Match/descr.scm 560 */
				obj_t BgL_resz00_1765;

				{	/* Match/descr.scm 560 */
					obj_t BgL_arg1765z00_1770;

					{	/* Match/descr.scm 560 */
						long BgL_arg1766z00_1771;
						obj_t BgL_arg1767z00_1772;

						{	/* Match/descr.scm 560 */
							obj_t BgL_arg1768z00_1773;

							{	/* Match/descr.scm 560 */
								obj_t BgL_pairz00_2985;

								{	/* Match/descr.scm 560 */
									obj_t BgL_pairz00_2984;

									BgL_pairz00_2984 = CDR(((obj_t) BgL_vz00_65));
									BgL_pairz00_2985 = CDR(BgL_pairz00_2984);
								}
								BgL_arg1768z00_1773 = CAR(BgL_pairz00_2985);
							}
							BgL_arg1766z00_1771 =
								VECTOR_LENGTH(((obj_t) BgL_arg1768z00_1773));
						}
						{	/* Match/descr.scm 561 */
							obj_t BgL_arg1769z00_1774;

							{	/* Match/descr.scm 561 */
								obj_t BgL_arg1770z00_1775;

								{	/* Match/descr.scm 561 */
									obj_t BgL_arg1771z00_1776;

									{	/* Match/descr.scm 561 */
										obj_t BgL_pairz00_2992;

										{	/* Match/descr.scm 561 */
											obj_t BgL_pairz00_2991;

											BgL_pairz00_2991 = CDR(((obj_t) BgL_vz00_65));
											BgL_pairz00_2992 = CDR(BgL_pairz00_2991);
										}
										BgL_arg1771z00_1776 = CAR(BgL_pairz00_2992);
									}
									BgL_arg1770z00_1775 =
										BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
										(BgL_arg1771z00_1776);
								}
								BgL_arg1769z00_1774 =
									BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
									(BgL_arg1770z00_1775);
							}
							BgL_arg1767z00_1772 = MAKE_YOUNG_PAIR(BgL_arg1769z00_1774, BNIL);
						}
						BgL_arg1765z00_1770 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1766z00_1771), BgL_arg1767z00_1772);
					}
					BgL_resz00_1765 =
						MAKE_YOUNG_PAIR(BGl_symbol2214z00zz__match_descriptionsz00,
						BgL_arg1765z00_1770);
				}
				{	/* Match/descr.scm 562 */
					obj_t BgL_arg1761z00_1766;
					obj_t BgL_arg1762z00_1767;

					BgL_arg1761z00_1766 = CAR(CDR(CDR(BgL_resz00_1765)));
					{	/* Match/descr.scm 563 */
						obj_t BgL_arg1763z00_1768;

						{	/* Match/descr.scm 563 */
							obj_t BgL_arg1764z00_1769;

							{	/* Match/descr.scm 563 */
								obj_t BgL_pairz00_3004;

								{	/* Match/descr.scm 563 */
									obj_t BgL_pairz00_3003;

									BgL_pairz00_3003 = CDR(((obj_t) BgL_vz00_65));
									BgL_pairz00_3004 = CDR(BgL_pairz00_3003);
								}
								BgL_arg1764z00_1769 = CAR(BgL_pairz00_3004);
							}
							{	/* Match/descr.scm 563 */
								long BgL_kz00_3006;

								BgL_kz00_3006 = (long) CINT(BgL_iz00_66);
								BgL_arg1763z00_1768 =
									VECTOR_REF(((obj_t) BgL_arg1764z00_1769), BgL_kz00_3006);
						}}
						BgL_arg1762z00_1767 =
							BGl_patternzd2pluszd2zz__match_descriptionsz00
							(BgL_arg1763z00_1768, BgL_dz00_67);
					}
					{	/* Match/descr.scm 562 */
						long BgL_kz00_3008;

						BgL_kz00_3008 = (long) CINT(BgL_iz00_66);
						VECTOR_SET(
							((obj_t) BgL_arg1761z00_1766), BgL_kz00_3008,
							BgL_arg1762z00_1767);
				}}
				return BgL_resz00_1765;
			}
		}

	}



/* &vector-plus */
	obj_t BGl_z62vectorzd2pluszb0zz__match_descriptionsz00(obj_t BgL_envz00_3551,
		obj_t BgL_vz00_3552, obj_t BgL_iz00_3553, obj_t BgL_dz00_3554)
	{
		{	/* Match/descr.scm 555 */
			return
				BGl_vectorzd2pluszd2zz__match_descriptionsz00(BgL_vz00_3552,
				BgL_iz00_3553, BgL_dz00_3554);
		}

	}



/* vector-minus */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2minuszd2zz__match_descriptionsz00(obj_t
		BgL_vz00_68, obj_t BgL_iz00_69, obj_t BgL_dz00_70)
	{
		{	/* Match/descr.scm 566 */
			{	/* Match/descr.scm 567 */
				bool_t BgL_test2418z00_5218;

				{	/* Match/descr.scm 567 */
					long BgL_arg1781z00_1783;

					{	/* Match/descr.scm 567 */
						obj_t BgL_arg1782z00_1784;

						{	/* Match/descr.scm 567 */
							obj_t BgL_pairz00_3014;

							{	/* Match/descr.scm 567 */
								obj_t BgL_pairz00_3013;

								BgL_pairz00_3013 = CDR(((obj_t) BgL_vz00_68));
								BgL_pairz00_3014 = CDR(BgL_pairz00_3013);
							}
							BgL_arg1782z00_1784 = CAR(BgL_pairz00_3014);
						}
						BgL_arg1781z00_1783 = VECTOR_LENGTH(((obj_t) BgL_arg1782z00_1784));
					}
					BgL_test2418z00_5218 =
						((long) CINT(BgL_iz00_69) >= BgL_arg1781z00_1783);
				}
				if (BgL_test2418z00_5218)
					{	/* Match/descr.scm 568 */
						obj_t BgL_arg1775z00_1780;
						obj_t BgL_arg1777z00_1781;

						{	/* Match/descr.scm 568 */
							obj_t BgL_pairz00_3021;

							BgL_pairz00_3021 = CDR(((obj_t) BgL_vz00_68));
							BgL_arg1775z00_1780 = CDR(BgL_pairz00_3021);
						}
						{	/* Match/descr.scm 569 */
							obj_t BgL_arg1779z00_1782;

							{	/* Match/descr.scm 569 */
								obj_t BgL_pairz00_3027;

								{	/* Match/descr.scm 569 */
									obj_t BgL_pairz00_3026;

									BgL_pairz00_3026 = CDR(((obj_t) BgL_vz00_68));
									BgL_pairz00_3027 = CDR(BgL_pairz00_3026);
								}
								BgL_arg1779z00_1782 = CAR(BgL_pairz00_3027);
							}
							{	/* Match/descr.scm 569 */
								obj_t BgL_fillz00_3028;

								BgL_fillz00_3028 = BGl_list2196z00zz__match_descriptionsz00;
								{	/* Match/descr.scm 582 */
									obj_t BgL_resz00_3029;

									{	/* Match/descr.scm 583 */
										obj_t BgL_newzd2vectorzd2_3030;

										BgL_newzd2vectorzd2_3030 =
											make_vector((long) CINT(BgL_iz00_69), BgL_fillz00_3028);
										{
											long BgL_iz00_3032;

											BgL_iz00_3032 = 0L;
										BgL_loopz00_3031:
											if (
												(BgL_iz00_3032 ==
													VECTOR_LENGTH(((obj_t) BgL_arg1779z00_1782))))
												{	/* Match/descr.scm 585 */
													BgL_resz00_3029 = BgL_newzd2vectorzd2_3030;
												}
											else
												{	/* Match/descr.scm 585 */
													{	/* Match/descr.scm 588 */
														obj_t BgL_arg1798z00_3035;

														BgL_arg1798z00_3035 =
															VECTOR_REF(
															((obj_t) BgL_arg1779z00_1782), BgL_iz00_3032);
														VECTOR_SET(BgL_newzd2vectorzd2_3030, BgL_iz00_3032,
															BgL_arg1798z00_3035);
													}
													{
														long BgL_iz00_5243;

														BgL_iz00_5243 = (BgL_iz00_3032 + 1L);
														BgL_iz00_3032 = BgL_iz00_5243;
														goto BgL_loopz00_3031;
													}
												}
										}
									}
									BgL_arg1777z00_1781 = BgL_resz00_3029;
								}
							}
						}
						{	/* Match/descr.scm 568 */
							obj_t BgL_tmpz00_5245;

							BgL_tmpz00_5245 = ((obj_t) BgL_arg1775z00_1780);
							SET_CAR(BgL_tmpz00_5245, BgL_arg1777z00_1781);
						}
					}
				else
					{	/* Match/descr.scm 567 */
						BTRUE;
					}
			}
			{	/* Match/descr.scm 571 */
				obj_t BgL_resz00_1785;

				{	/* Match/descr.scm 571 */
					obj_t BgL_arg1788z00_1790;

					{	/* Match/descr.scm 571 */
						long BgL_arg1789z00_1791;
						obj_t BgL_arg1790z00_1792;

						{	/* Match/descr.scm 571 */
							obj_t BgL_auxz00_5248;

							{	/* Match/descr.scm 571 */
								obj_t BgL_pairz00_3051;

								{	/* Match/descr.scm 571 */
									obj_t BgL_pairz00_3050;

									BgL_pairz00_3050 = CDR(((obj_t) BgL_vz00_68));
									BgL_pairz00_3051 = CDR(BgL_pairz00_3050);
								}
								BgL_auxz00_5248 = CAR(BgL_pairz00_3051);
							}
							BgL_arg1789z00_1791 = bgl_list_length(BgL_auxz00_5248);
						}
						{	/* Match/descr.scm 572 */
							obj_t BgL_arg1792z00_1794;

							{	/* Match/descr.scm 572 */
								obj_t BgL_arg1793z00_1795;

								{	/* Match/descr.scm 572 */
									obj_t BgL_arg1794z00_1796;

									{	/* Match/descr.scm 572 */
										obj_t BgL_pairz00_3057;

										{	/* Match/descr.scm 572 */
											obj_t BgL_pairz00_3056;

											BgL_pairz00_3056 = CDR(((obj_t) BgL_vz00_68));
											BgL_pairz00_3057 = CDR(BgL_pairz00_3056);
										}
										BgL_arg1794z00_1796 = CAR(BgL_pairz00_3057);
									}
									BgL_arg1793z00_1795 =
										BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
										(BgL_arg1794z00_1796);
								}
								BgL_arg1792z00_1794 =
									BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
									(BgL_arg1793z00_1795);
							}
							BgL_arg1790z00_1792 = MAKE_YOUNG_PAIR(BgL_arg1792z00_1794, BNIL);
						}
						BgL_arg1788z00_1790 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg1789z00_1791), BgL_arg1790z00_1792);
					}
					BgL_resz00_1785 =
						MAKE_YOUNG_PAIR(BGl_symbol2214z00zz__match_descriptionsz00,
						BgL_arg1788z00_1790);
				}
				{	/* Match/descr.scm 573 */
					obj_t BgL_arg1783z00_1786;
					obj_t BgL_arg1785z00_1787;

					BgL_arg1783z00_1786 = CAR(CDR(CDR(BgL_resz00_1785)));
					{	/* Match/descr.scm 574 */
						obj_t BgL_arg1786z00_1788;

						{	/* Match/descr.scm 574 */
							obj_t BgL_arg1787z00_1789;

							{	/* Match/descr.scm 574 */
								obj_t BgL_pairz00_3069;

								{	/* Match/descr.scm 574 */
									obj_t BgL_pairz00_3068;

									BgL_pairz00_3068 = CDR(((obj_t) BgL_vz00_68));
									BgL_pairz00_3069 = CDR(BgL_pairz00_3068);
								}
								BgL_arg1787z00_1789 = CAR(BgL_pairz00_3069);
							}
							{	/* Match/descr.scm 574 */
								long BgL_kz00_3071;

								BgL_kz00_3071 = (long) CINT(BgL_iz00_69);
								BgL_arg1786z00_1788 =
									VECTOR_REF(((obj_t) BgL_arg1787z00_1789), BgL_kz00_3071);
						}}
						BgL_arg1785z00_1787 =
							BGl_patternzd2minuszd2zz__match_descriptionsz00
							(BgL_arg1786z00_1788, BgL_dz00_70);
					}
					{	/* Match/descr.scm 573 */
						long BgL_kz00_3073;

						BgL_kz00_3073 = (long) CINT(BgL_iz00_69);
						VECTOR_SET(
							((obj_t) BgL_arg1783z00_1786), BgL_kz00_3073,
							BgL_arg1785z00_1787);
				}}
				return BgL_resz00_1785;
			}
		}

	}



/* &vector-minus */
	obj_t BGl_z62vectorzd2minuszb0zz__match_descriptionsz00(obj_t BgL_envz00_3555,
		obj_t BgL_vz00_3556, obj_t BgL_iz00_3557, obj_t BgL_dz00_3558)
	{
		{	/* Match/descr.scm 566 */
			return
				BGl_vectorzd2minuszd2zz__match_descriptionsz00(BgL_vz00_3556,
				BgL_iz00_3557, BgL_dz00_3558);
		}

	}



/* extend-vector */
	BGL_EXPORTED_DEF obj_t BGl_extendzd2vectorzd2zz__match_descriptionsz00(obj_t
		BgL_vz00_71, obj_t BgL_lgz00_72, obj_t BgL_fillz00_73)
	{
		{	/* Match/descr.scm 581 */
			{	/* Match/descr.scm 582 */
				obj_t BgL_resz00_3074;

				{	/* Match/descr.scm 583 */
					obj_t BgL_newzd2vectorzd2_3075;

					BgL_newzd2vectorzd2_3075 =
						make_vector((long) CINT(BgL_lgz00_72), BgL_fillz00_73);
					{
						long BgL_iz00_3091;

						BgL_iz00_3091 = 0L;
					BgL_loopz00_3090:
						if ((BgL_iz00_3091 == VECTOR_LENGTH(((obj_t) BgL_vz00_71))))
							{	/* Match/descr.scm 585 */
								BgL_resz00_3074 = BgL_newzd2vectorzd2_3075;
							}
						else
							{	/* Match/descr.scm 585 */
								{	/* Match/descr.scm 588 */
									obj_t BgL_arg1798z00_3097;

									BgL_arg1798z00_3097 =
										VECTOR_REF(((obj_t) BgL_vz00_71), BgL_iz00_3091);
									VECTOR_SET(BgL_newzd2vectorzd2_3075, BgL_iz00_3091,
										BgL_arg1798z00_3097);
								}
								{
									long BgL_iz00_5288;

									BgL_iz00_5288 = (BgL_iz00_3091 + 1L);
									BgL_iz00_3091 = BgL_iz00_5288;
									goto BgL_loopz00_3090;
								}
							}
					}
				}
				return BgL_resz00_3074;
			}
		}

	}



/* &extend-vector */
	obj_t BGl_z62extendzd2vectorzb0zz__match_descriptionsz00(obj_t
		BgL_envz00_3559, obj_t BgL_vz00_3560, obj_t BgL_lgz00_3561,
		obj_t BgL_fillz00_3562)
	{
		{	/* Match/descr.scm 581 */
			return
				BGl_extendzd2vectorzd2zz__match_descriptionsz00(BgL_vz00_3560,
				BgL_lgz00_3561, BgL_fillz00_3562);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__match_descriptionsz00(void)
	{
		{	/* Match/descr.scm 12 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__match_descriptionsz00(void)
	{
		{	/* Match/descr.scm 12 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__match_descriptionsz00(void)
	{
		{	/* Match/descr.scm 12 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__match_descriptionsz00(void)
	{
		{	/* Match/descr.scm 12 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2225z00zz__match_descriptionsz00));
			BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(509060701L,
				BSTRING_TO_STRING(BGl_string2225z00zz__match_descriptionsz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2225z00zz__match_descriptionsz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2225z00zz__match_descriptionsz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2225z00zz__match_descriptionsz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2225z00zz__match_descriptionsz00));
		}

	}

#ifdef __cplusplus
}
#endif
