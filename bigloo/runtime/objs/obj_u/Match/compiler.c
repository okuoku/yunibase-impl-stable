/*===========================================================================*/
/*   (Match/compiler.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Match/compiler.scm -indent -o objs/obj_u/Match/compiler.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MATCH_COMPILER_TYPE_DEFINITIONS
#define BGL___MATCH_COMPILER_TYPE_DEFINITIONS
#endif													// BGL___MATCH_COMPILER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62pcompilez62zz__match_compilerz00(obj_t, obj_t);
	extern obj_t BGl_compatiblezf3zf3zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_dzd2initzd2zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_succeszd2conszd2zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_buildzd2ifzd2zz__match_compilerz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31523ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_compilez00zz__match_compilerz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31419ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31677ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_morezd2precisezf3z21zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31711ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static bool_t BGl_isDirectCallzf3zf3zz__match_compilerz00(obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_compilezd2timeszd2zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__match_compilerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_descriptionsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza31704ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2201z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2203z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2205z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2207z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2209z00zz__match_compilerz00 = BUNSPEC;
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__match_compilerz00(void);
	static obj_t BGl_z62zc3z04anonymousza31632ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2211z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2213z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t BGl_jimzd2gensymzd2zz__match_s2cfunz00;
	static obj_t BGl_symbol2216z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t BGl_patternzd2carzd2zz__match_descriptionsz00(obj_t);
	extern obj_t BGl_vectorzd2pluszd2zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31641ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31617ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_extendza2za2zz__match_compilerz00(obj_t, obj_t);
	extern obj_t BGl_patternzd2minuszd2zz__match_descriptionsz00(obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__match_compilerz00(void);
	static obj_t BGl_symbol2220z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2222z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zz__match_compilerz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_symbol2226z00zz__match_compilerz00 = BUNSPEC;
	extern long bgl_list_length(obj_t);
	static obj_t BGl_symbol2147z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_z62instanciatezd2tryzb0zz__match_compilerz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__match_compilerz00(void);
	static obj_t BGl_z62zc3z04anonymousza31650ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__match_compilerz00(void);
	static obj_t BGl_z62zc3z04anonymousza31383ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_rzd2initzd2zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__match_compilerz00(void);
	static obj_t BGl_z62compileza2zc0zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2230z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_symbol2150z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2232z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2152z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2234z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_integersz00zz__match_compilerz00(long, long);
	static obj_t BGl_symbol2154z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2156z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2238z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2158z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_unfoldz00zz__match_compilerz00(obj_t, obj_t, obj_t);
	static obj_t BGl_compilezd2vectorzd2consz00zz__match_compilerz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31481ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31384ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_z62mzd2initzb0zz__match_compilerz00(obj_t, obj_t);
	static obj_t BGl_symbol2240z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2160z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2242z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2162z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2245z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2164z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t BGl_patternzd2cdrzd2zz__match_descriptionsz00(obj_t);
	static obj_t BGl_symbol2247z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2166z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2168z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t BGl_extendzd2vectorzd2zz__match_descriptionsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31490ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_compilezd2varzd2zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2170z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_z62za7zd2initz17zz__match_compilerz00(obj_t, obj_t);
	static obj_t BGl_symbol2172z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2174z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t BGl_patternzd2variableszd2zz__match_descriptionsz00(obj_t);
	static obj_t BGl_symbol2179z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_list2146z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31411ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31548ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pcompilez00zz__match_compilerz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2181z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_buildzd2letzd2zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_symbol2183z00zz__match_compilerz00 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_symbol2185z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2187z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_list2236z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_list2237z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2189z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31638ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31395ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__match_compilerz00(void);
	static obj_t BGl_symbol2192z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2195z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2197z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_symbol2199z00zz__match_compilerz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31421ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	extern obj_t BGl_atomzf3zf3zz__match_s2cfunz00(obj_t);
	static obj_t BGl_countzd2occurrenceszd2zz__match_compilerz00(obj_t, obj_t,
		long);
	static obj_t BGl_z62zc3z04anonymousza31485ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31396ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_compilezd2orzd2zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_compilezd2structzd2patz00zz__match_compilerz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31407ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62kzd2initzb0zz__match_compilerz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31398ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_compilezd2vectorzd2beginz00zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31424ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31408ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_buildzd2atomzd2equalityzd2testzd2zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31496ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31522ze3ze5zz__match_compilerz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_patternzd2pluszd2zz__match_descriptionsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31659ze3ze5zz__match_compilerz00(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2200z00zz__match_compilerz00,
		BgL_bgl_string2200za700za7za7_2250za7, "null?", 5);
	      DEFINE_STRING(BGl_string2202z00zz__match_compilerz00,
		BgL_bgl_string2202za700za7za7_2251za7, "flonum?", 7);
	      DEFINE_STRING(BGl_string2204z00zz__match_compilerz00,
		BgL_bgl_string2204za700za7za7_2252za7, "=fl", 3);
	      DEFINE_STRING(BGl_string2206z00zz__match_compilerz00,
		BgL_bgl_string2206za700za7za7_2253za7, "number?", 7);
	      DEFINE_STRING(BGl_string2208z00zz__match_compilerz00,
		BgL_bgl_string2208za700za7za7_2254za7, "=", 1);
	      DEFINE_STRING(BGl_string2210z00zz__match_compilerz00,
		BgL_bgl_string2210za700za7za7_2255za7, "string?", 7);
	      DEFINE_STRING(BGl_string2212z00zz__match_compilerz00,
		BgL_bgl_string2212za700za7za7_2256za7, "string=?", 8);
	      DEFINE_STRING(BGl_string2214z00zz__match_compilerz00,
		BgL_bgl_string2214za700za7za7_2257za7, "equal?", 6);
	      DEFINE_STRING(BGl_string2215z00zz__match_compilerz00,
		BgL_bgl_string2215za700za7za7_2258za7, "KAP-", 4);
	      DEFINE_STRING(BGl_string2217z00zz__match_compilerz00,
		BgL_bgl_string2217za700za7za7_2259za7, "labels", 6);
	      DEFINE_STRING(BGl_string2218z00zz__match_compilerz00,
		BgL_bgl_string2218za700za7za7_2260za7, "KAP", 3);
	      DEFINE_STRING(BGl_string2219z00zz__match_compilerz00,
		BgL_bgl_string2219za700za7za7_2261za7, "TAG", 3);
	      DEFINE_STRING(BGl_string2221z00zz__match_compilerz00,
		BgL_bgl_string2221za700za7za7_2262za7, "unbound", 7);
	      DEFINE_STRING(BGl_string2223z00zz__match_compilerz00,
		BgL_bgl_string2223za700za7za7_2263za7, "let", 3);
	      DEFINE_STRING(BGl_string2224z00zz__match_compilerz00,
		BgL_bgl_string2224za700za7za7_2264za7, "CAR-", 4);
	      DEFINE_STRING(BGl_string2225z00zz__match_compilerz00,
		BgL_bgl_string2225za700za7za7_2265za7, "CDR-", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_kzd2initzd2envz00zz__match_compilerz00,
		BgL_bgl_za762kza7d2initza7b0za7za72266za7,
		BGl_z62kzd2initzb0zz__match_compilerz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2227z00zz__match_compilerz00,
		BgL_bgl_string2227za700za7za7_2267za7, "letrec", 6);
	      DEFINE_STRING(BGl_string2228z00zz__match_compilerz00,
		BgL_bgl_string2228za700za7za7_2268za7, "G-", 2);
	      DEFINE_STRING(BGl_string2229z00zz__match_compilerz00,
		BgL_bgl_string2229za700za7za7_2269za7, "TRY-", 4);
	      DEFINE_STRING(BGl_string2148z00zz__match_compilerz00,
		BgL_bgl_string2148za700za7za7_2270za7, "any", 3);
	      DEFINE_STRING(BGl_string2149z00zz__match_compilerz00,
		BgL_bgl_string2149za700za7za7_2271za7, "E-", 2);
	      DEFINE_STRING(BGl_string2231z00zz__match_compilerz00,
		BgL_bgl_string2231za700za7za7_2272za7, "vector", 6);
	      DEFINE_STRING(BGl_string2151z00zz__match_compilerz00,
		BgL_bgl_string2151za700za7za7_2273za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2233z00zz__match_compilerz00,
		BgL_bgl_string2233za700za7za7_2274za7, ">=", 2);
	      DEFINE_STRING(BGl_string2153z00zz__match_compilerz00,
		BgL_bgl_string2153za700za7za7_2275za7, "check", 5);
	      DEFINE_STRING(BGl_string2235z00zz__match_compilerz00,
		BgL_bgl_string2235za700za7za7_2276za7, "vector?", 7);
	      DEFINE_STRING(BGl_string2155z00zz__match_compilerz00,
		BgL_bgl_string2155za700za7za7_2277za7, "if", 2);
	      DEFINE_STRING(BGl_string2157z00zz__match_compilerz00,
		BgL_bgl_string2157za700za7za7_2278za7, "quote", 5);
	      DEFINE_STRING(BGl_string2239z00zz__match_compilerz00,
		BgL_bgl_string2239za700za7za7_2279za7, "vector-ref", 10);
	      DEFINE_STRING(BGl_string2159z00zz__match_compilerz00,
		BgL_bgl_string2159za700za7za7_2280za7, "var", 3);
	      DEFINE_STRING(BGl_string2241z00zz__match_compilerz00,
		BgL_bgl_string2241za700za7za7_2281za7, "struct-ref", 10);
	      DEFINE_STRING(BGl_string2161z00zz__match_compilerz00,
		BgL_bgl_string2161za700za7za7_2282za7, "not", 3);
	      DEFINE_STRING(BGl_string2243z00zz__match_compilerz00,
		BgL_bgl_string2243za700za7za7_2283za7, "dummy", 5);
	      DEFINE_STRING(BGl_string2244z00zz__match_compilerz00,
		BgL_bgl_string2244za700za7za7_2284za7, "No current repetition named", 27);
	      DEFINE_STRING(BGl_string2163z00zz__match_compilerz00,
		BgL_bgl_string2163za700za7za7_2285za7, "or", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_mzd2initzd2envz00zz__match_compilerz00,
		BgL_bgl_za762mza7d2initza7b0za7za72286za7,
		BGl_z62mzd2initzb0zz__match_compilerz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2246z00zz__match_compilerz00,
		BgL_bgl_string2246za700za7za7_2287za7, "car", 3);
	      DEFINE_STRING(BGl_string2165z00zz__match_compilerz00,
		BgL_bgl_string2165za700za7za7_2288za7, "tagged-or", 9);
	      DEFINE_STRING(BGl_string2248z00zz__match_compilerz00,
		BgL_bgl_string2248za700za7za7_2289za7, "cdr", 3);
	      DEFINE_STRING(BGl_string2167z00zz__match_compilerz00,
		BgL_bgl_string2167za700za7za7_2290za7, "and", 3);
	      DEFINE_STRING(BGl_string2249z00zz__match_compilerz00,
		BgL_bgl_string2249za700za7za7_2291za7, "__match_compiler", 16);
	      DEFINE_STRING(BGl_string2169z00zz__match_compilerz00,
		BgL_bgl_string2169za700za7za7_2292za7, "cons", 4);
	      DEFINE_STRING(BGl_string2171z00zz__match_compilerz00,
		BgL_bgl_string2171za700za7za7_2293za7, "pair?", 5);
	      DEFINE_STRING(BGl_string2173z00zz__match_compilerz00,
		BgL_bgl_string2173za700za7za7_2294za7, "times", 5);
	      DEFINE_STRING(BGl_string2175z00zz__match_compilerz00,
		BgL_bgl_string2175za700za7za7_2295za7, "tree", 4);
	      DEFINE_STRING(BGl_string2176z00zz__match_compilerz00,
		BgL_bgl_string2176za700za7za7_2296za7, "Tree not yet allowed", 20);
	      DEFINE_STRING(BGl_string2177z00zz__match_compilerz00,
		BgL_bgl_string2177za700za7za7_2297za7, "Incorrect pattern: ", 19);
	      DEFINE_STRING(BGl_string2178z00zz__match_compilerz00,
		BgL_bgl_string2178za700za7za7_2298za7, " *** ", 5);
	      DEFINE_STRING(BGl_string2180z00zz__match_compilerz00,
		BgL_bgl_string2180za700za7za7_2299za7, "hole", 4);
	      DEFINE_STRING(BGl_string2182z00zz__match_compilerz00,
		BgL_bgl_string2182za700za7za7_2300za7, "vector-begin", 12);
	      DEFINE_STRING(BGl_string2184z00zz__match_compilerz00,
		BgL_bgl_string2184za700za7za7_2301za7, "vector-end", 10);
	      DEFINE_STRING(BGl_string2186z00zz__match_compilerz00,
		BgL_bgl_string2186za700za7za7_2302za7, "vector-any", 10);
	      DEFINE_STRING(BGl_string2188z00zz__match_compilerz00,
		BgL_bgl_string2188za700za7za7_2303za7, "vector-cons", 11);
	      DEFINE_STRING(BGl_string2190z00zz__match_compilerz00,
		BgL_bgl_string2190za700za7za7_2304za7, "vector-times", 12);
	      DEFINE_STRING(BGl_string2191z00zz__match_compilerz00,
		BgL_bgl_string2191za700za7za7_2305za7, "Not yet allowed", 15);
	      DEFINE_STRING(BGl_string2193z00zz__match_compilerz00,
		BgL_bgl_string2193za700za7za7_2306za7, "struct-pat", 10);
	      DEFINE_STRING(BGl_string2194z00zz__match_compilerz00,
		BgL_bgl_string2194za700za7za7_2307za7, "Unrecognized pattern", 20);
	      DEFINE_STRING(BGl_string2196z00zz__match_compilerz00,
		BgL_bgl_string2196za700za7za7_2308za7, "vector-length", 13);
	      DEFINE_STRING(BGl_string2198z00zz__match_compilerz00,
		BgL_bgl_string2198za700za7za7_2309za7, "eq?", 3);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_za7zd2initzd2envza7zz__match_compilerz00,
		BgL_bgl_za762za7a7za7d2initza7172310z00,
		BGl_z62za7zd2initz17zz__match_compilerz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pcompilezd2envzd2zz__match_compilerz00,
		BgL_bgl_za762pcompileza762za7za72311z00,
		BGl_z62pcompilez62zz__match_compilerz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_dzd2initzd2zz__match_compilerz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2201z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2203z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2205z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2207z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2209z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2211z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2213z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2216z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2220z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2222z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2226z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2147z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_rzd2initzd2zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2230z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2150z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2232z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2152z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2234z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2154z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2156z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2238z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2158z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2240z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2160z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2242z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2162z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2245z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2164z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2247z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2166z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2168z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2170z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2172z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2174z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2179z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_list2146z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2181z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2183z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2185z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2187z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_list2236z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_list2237z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2189z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2192z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2195z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2197z00zz__match_compilerz00));
		     ADD_ROOT((void *) (&BGl_symbol2199z00zz__match_compilerz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__match_compilerz00(long
		BgL_checksumz00_3770, char *BgL_fromz00_3771)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__match_compilerz00))
				{
					BGl_requirezd2initializa7ationz75zz__match_compilerz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__match_compilerz00();
					BGl_cnstzd2initzd2zz__match_compilerz00();
					BGl_importedzd2moduleszd2initz00zz__match_compilerz00();
					return BGl_toplevelzd2initzd2zz__match_compilerz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			BGl_symbol2147z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2148z00zz__match_compilerz00);
			BGl_list2146z00zz__match_compilerz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2147z00zz__match_compilerz00, BNIL);
			BGl_symbol2150z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2151z00zz__match_compilerz00);
			BGl_symbol2152z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2153z00zz__match_compilerz00);
			BGl_symbol2154z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2155z00zz__match_compilerz00);
			BGl_symbol2156z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2157z00zz__match_compilerz00);
			BGl_symbol2158z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2159z00zz__match_compilerz00);
			BGl_symbol2160z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2161z00zz__match_compilerz00);
			BGl_symbol2162z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2163z00zz__match_compilerz00);
			BGl_symbol2164z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2165z00zz__match_compilerz00);
			BGl_symbol2166z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2167z00zz__match_compilerz00);
			BGl_symbol2168z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2169z00zz__match_compilerz00);
			BGl_symbol2170z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2171z00zz__match_compilerz00);
			BGl_symbol2172z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2173z00zz__match_compilerz00);
			BGl_symbol2174z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2175z00zz__match_compilerz00);
			BGl_symbol2179z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2180z00zz__match_compilerz00);
			BGl_symbol2181z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2182z00zz__match_compilerz00);
			BGl_symbol2183z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2184z00zz__match_compilerz00);
			BGl_symbol2185z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2186z00zz__match_compilerz00);
			BGl_symbol2187z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2188z00zz__match_compilerz00);
			BGl_symbol2189z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2190z00zz__match_compilerz00);
			BGl_symbol2192z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2193z00zz__match_compilerz00);
			BGl_symbol2195z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2196z00zz__match_compilerz00);
			BGl_symbol2197z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2198z00zz__match_compilerz00);
			BGl_symbol2199z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2200z00zz__match_compilerz00);
			BGl_symbol2201z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2202z00zz__match_compilerz00);
			BGl_symbol2203z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2204z00zz__match_compilerz00);
			BGl_symbol2205z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2206z00zz__match_compilerz00);
			BGl_symbol2207z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2208z00zz__match_compilerz00);
			BGl_symbol2209z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2210z00zz__match_compilerz00);
			BGl_symbol2211z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2212z00zz__match_compilerz00);
			BGl_symbol2213z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2214z00zz__match_compilerz00);
			BGl_symbol2216z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2217z00zz__match_compilerz00);
			BGl_symbol2220z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2221z00zz__match_compilerz00);
			BGl_symbol2222z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2223z00zz__match_compilerz00);
			BGl_symbol2226z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2227z00zz__match_compilerz00);
			BGl_symbol2230z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2231z00zz__match_compilerz00);
			BGl_symbol2232z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2233z00zz__match_compilerz00);
			BGl_symbol2234z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2235z00zz__match_compilerz00);
			BGl_list2237z00zz__match_compilerz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2230z00zz__match_compilerz00, BNIL);
			BGl_list2236z00zz__match_compilerz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2160z00zz__match_compilerz00,
				MAKE_YOUNG_PAIR(BGl_list2237z00zz__match_compilerz00, BNIL));
			BGl_symbol2238z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2239z00zz__match_compilerz00);
			BGl_symbol2240z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2241z00zz__match_compilerz00);
			BGl_symbol2242z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2243z00zz__match_compilerz00);
			BGl_symbol2245z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2246z00zz__match_compilerz00);
			return (BGl_symbol2247z00zz__match_compilerz00 =
				bstring_to_symbol(BGl_string2248z00zz__match_compilerz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			BGl_dzd2initzd2zz__match_compilerz00 =
				BGl_list2146z00zz__match_compilerz00;
			return (BGl_rzd2initzd2zz__match_compilerz00 = BNIL, BUNSPEC);
		}

	}



/* pcompile */
	BGL_EXPORTED_DEF obj_t BGl_pcompilez00zz__match_compilerz00(obj_t BgL_fz00_3)
	{
		{	/* Match/compiler.scm 64 */
			{	/* Match/compiler.scm 65 */
				obj_t BgL_sz00_1409;

				BgL_sz00_1409 =
					BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
					BGl_string2149z00zz__match_compilerz00);
				{	/* Match/compiler.scm 66 */
					obj_t BgL_arg1210z00_1410;

					{	/* Match/compiler.scm 66 */
						obj_t BgL_arg1212z00_1411;
						obj_t BgL_arg1215z00_1412;

						BgL_arg1212z00_1411 = MAKE_YOUNG_PAIR(BgL_sz00_1409, BNIL);
						BgL_arg1215z00_1412 =
							MAKE_YOUNG_PAIR(BGl_compilez00zz__match_compilerz00(BgL_fz00_3,
								BgL_sz00_1409, BNIL, BGl_mzd2initzd2envz00zz__match_compilerz00,
								BGl_kzd2initzd2envz00zz__match_compilerz00,
								BGl_za7zd2initzd2envza7zz__match_compilerz00,
								BGl_list2146z00zz__match_compilerz00), BNIL);
						BgL_arg1210z00_1410 =
							MAKE_YOUNG_PAIR(BgL_arg1212z00_1411, BgL_arg1215z00_1412);
					}
					return
						MAKE_YOUNG_PAIR(BGl_symbol2150z00zz__match_compilerz00,
						BgL_arg1210z00_1410);
				}
			}
		}

	}



/* &pcompile */
	obj_t BGl_z62pcompilez62zz__match_compilerz00(obj_t BgL_envz00_3399,
		obj_t BgL_fz00_3400)
	{
		{	/* Match/compiler.scm 64 */
			return BGl_pcompilez00zz__match_compilerz00(BgL_fz00_3400);
		}

	}



/* compile */
	obj_t BGl_compilez00zz__match_compilerz00(obj_t BgL_fz00_4, obj_t BgL_ez00_5,
		obj_t BgL_rz00_6, obj_t BgL_mz00_7, obj_t BgL_kz00_8, obj_t BgL_za7za7_9,
		obj_t BgL_dz00_10)
	{
		{	/* Match/compiler.scm 70 */
		BGl_compilez00zz__match_compilerz00:
			if (CBOOL(BGl_morezd2precisezf3z21zz__match_descriptionsz00(BgL_dz00_10,
						BgL_fz00_4)))
				{	/* Match/compiler.scm 72 */
					return
						BGL_PROCEDURE_CALL3(BgL_kz00_8, BgL_rz00_6, BgL_za7za7_9,
						BgL_dz00_10);
				}
			else
				{	/* Match/compiler.scm 72 */
					if (CBOOL(BGl_compatiblezf3zf3zz__match_descriptionsz00(BgL_dz00_10,
								BgL_fz00_4)))
						{	/* Match/compiler.scm 74 */
							obj_t BgL_casezd2valuezd2_1416;

							BgL_casezd2valuezd2_1416 = CAR(((obj_t) BgL_fz00_4));
							if (
								(BgL_casezd2valuezd2_1416 ==
									BGl_symbol2147z00zz__match_compilerz00))
								{	/* Match/compiler.scm 74 */
									return
										BGL_PROCEDURE_CALL3(BgL_kz00_8, BgL_rz00_6, BgL_za7za7_9,
										BgL_dz00_10);
								}
							else
								{	/* Match/compiler.scm 74 */
									if (
										(BgL_casezd2valuezd2_1416 ==
											BGl_symbol2152z00zz__match_compilerz00))
										{	/* Match/compiler.scm 76 */
											obj_t BgL_arg1221z00_1419;

											{	/* Match/compiler.scm 76 */
												obj_t BgL_pairz00_2480;

												BgL_pairz00_2480 = CDR(((obj_t) BgL_fz00_4));
												BgL_arg1221z00_1419 = CAR(BgL_pairz00_2480);
											}
											{	/* Match/compiler.scm 112 */
												obj_t BgL_arg1320z00_2481;

												{	/* Match/compiler.scm 112 */
													obj_t BgL_arg1321z00_2482;
													obj_t BgL_arg1322z00_2483;

													{	/* Match/compiler.scm 112 */
														obj_t BgL_arg1323z00_2484;

														BgL_arg1323z00_2484 =
															MAKE_YOUNG_PAIR(BgL_ez00_5, BNIL);
														BgL_arg1321z00_2482 =
															MAKE_YOUNG_PAIR(BgL_arg1221z00_1419,
															BgL_arg1323z00_2484);
													}
													{	/* Match/compiler.scm 113 */
														obj_t BgL_arg1325z00_2485;
														obj_t BgL_arg1326z00_2486;

														BgL_arg1325z00_2485 =
															BGL_PROCEDURE_CALL3(BgL_kz00_8, BgL_rz00_6,
															BgL_za7za7_9, BgL_dz00_10);
														{	/* Match/compiler.scm 114 */
															obj_t BgL_arg1327z00_2487;

															BgL_arg1327z00_2487 =
																BGL_PROCEDURE_CALL1(BgL_za7za7_9, BgL_dz00_10);
															BgL_arg1326z00_2486 =
																MAKE_YOUNG_PAIR(BgL_arg1327z00_2487, BNIL);
														}
														BgL_arg1322z00_2483 =
															MAKE_YOUNG_PAIR(BgL_arg1325z00_2485,
															BgL_arg1326z00_2486);
													}
													BgL_arg1320z00_2481 =
														MAKE_YOUNG_PAIR(BgL_arg1321z00_2482,
														BgL_arg1322z00_2483);
												}
												return
													MAKE_YOUNG_PAIR
													(BGl_symbol2154z00zz__match_compilerz00,
													BgL_arg1320z00_2481);
											}
										}
									else
										{	/* Match/compiler.scm 74 */
											if (
												(BgL_casezd2valuezd2_1416 ==
													BGl_symbol2156z00zz__match_compilerz00))
												{	/* Match/compiler.scm 138 */
													obj_t BgL_arg1371z00_2489;
													obj_t BgL_arg1372z00_2490;
													obj_t BgL_arg1373z00_2491;

													{	/* Match/compiler.scm 138 */
														obj_t BgL_arg1375z00_2492;

														{	/* Match/compiler.scm 138 */
															obj_t BgL_pairz00_2500;

															BgL_pairz00_2500 = CDR(((obj_t) BgL_fz00_4));
															BgL_arg1375z00_2492 = CAR(BgL_pairz00_2500);
														}
														BgL_arg1371z00_2489 =
															BGl_buildzd2atomzd2equalityzd2testzd2zz__match_compilerz00
															(BgL_ez00_5, BgL_arg1375z00_2492);
													}
													{	/* Match/compiler.scm 139 */
														obj_t BgL_arg1376z00_2493;

														{	/* Match/compiler.scm 139 */
															obj_t BgL_arg1377z00_2494;

															{	/* Match/compiler.scm 139 */
																obj_t BgL_arg1378z00_2495;

																{	/* Match/compiler.scm 139 */
																	obj_t BgL_pairz00_2504;

																	BgL_pairz00_2504 = CDR(((obj_t) BgL_fz00_4));
																	BgL_arg1378z00_2495 = CAR(BgL_pairz00_2504);
																}
																BgL_arg1377z00_2494 =
																	MAKE_YOUNG_PAIR(BgL_arg1378z00_2495, BNIL);
															}
															BgL_arg1376z00_2493 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2156z00zz__match_compilerz00,
																BgL_arg1377z00_2494);
														}
														BgL_arg1372z00_2490 =
															BGL_PROCEDURE_CALL3(BgL_kz00_8, BgL_rz00_6,
															BgL_za7za7_9, BgL_arg1376z00_2493);
													}
													{	/* Match/compiler.scm 140 */
														obj_t BgL_arg1379z00_2496;

														BgL_arg1379z00_2496 =
															BGl_patternzd2minuszd2zz__match_descriptionsz00
															(BgL_dz00_10, BgL_fz00_4);
														BgL_arg1373z00_2491 =
															BGL_PROCEDURE_CALL1(BgL_za7za7_9,
															BgL_arg1379z00_2496);
													}
													return
														BGl_buildzd2ifzd2zz__match_compilerz00
														(BgL_arg1371z00_2489, BgL_arg1372z00_2490,
														BgL_arg1373z00_2491);
												}
											else
												{	/* Match/compiler.scm 74 */
													if (
														(BgL_casezd2valuezd2_1416 ==
															BGl_symbol2158z00zz__match_compilerz00))
														{	/* Match/compiler.scm 78 */
															obj_t BgL_arg1225z00_1422;

															{	/* Match/compiler.scm 78 */
																obj_t BgL_pairz00_2509;

																BgL_pairz00_2509 = CDR(((obj_t) BgL_fz00_4));
																BgL_arg1225z00_1422 = CAR(BgL_pairz00_2509);
															}
															BGL_TAIL return
																BGl_compilezd2varzd2zz__match_compilerz00
																(BgL_arg1225z00_1422, BgL_ez00_5, BgL_rz00_6,
																BgL_mz00_7, BgL_kz00_8, BgL_za7za7_9,
																BgL_dz00_10);
														}
													else
														{	/* Match/compiler.scm 74 */
															if (
																(BgL_casezd2valuezd2_1416 ==
																	BGl_symbol2160z00zz__match_compilerz00))
																{	/* Match/compiler.scm 79 */
																	obj_t BgL_arg1227z00_1424;

																	{	/* Match/compiler.scm 79 */
																		obj_t BgL_pairz00_2514;

																		BgL_pairz00_2514 =
																			CDR(((obj_t) BgL_fz00_4));
																		BgL_arg1227z00_1424 = CAR(BgL_pairz00_2514);
																	}
																	{	/* Match/compiler.scm 143 */
																		obj_t BgL_zc3z04anonymousza31384ze3z87_3409;
																		obj_t BgL_zc3z04anonymousza31383ze3z87_3410;

																		BgL_zc3z04anonymousza31384ze3z87_3409 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31384ze3ze5zz__match_compilerz00,
																			(int) (1L), (int) (3L));
																		BgL_zc3z04anonymousza31383ze3z87_3410 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31383ze3ze5zz__match_compilerz00,
																			(int) (3L), (int) (1L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31384ze3z87_3409,
																			(int) (0L), BgL_kz00_8);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31384ze3z87_3409,
																			(int) (1L), BgL_rz00_6);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31384ze3z87_3409,
																			(int) (2L), BgL_za7za7_9);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31383ze3z87_3410,
																			(int) (0L), BgL_za7za7_9);
																		{
																			obj_t BgL_za7za7_3930;
																			obj_t BgL_kz00_3929;
																			obj_t BgL_fz00_3928;

																			BgL_fz00_3928 = BgL_arg1227z00_1424;
																			BgL_kz00_3929 =
																				BgL_zc3z04anonymousza31383ze3z87_3410;
																			BgL_za7za7_3930 =
																				BgL_zc3z04anonymousza31384ze3z87_3409;
																			BgL_za7za7_9 = BgL_za7za7_3930;
																			BgL_kz00_8 = BgL_kz00_3929;
																			BgL_fz00_4 = BgL_fz00_3928;
																			goto BGl_compilez00zz__match_compilerz00;
																		}
																	}
																}
															else
																{	/* Match/compiler.scm 74 */
																	if (
																		(BgL_casezd2valuezd2_1416 ==
																			BGl_symbol2162z00zz__match_compilerz00))
																		{	/* Match/compiler.scm 80 */
																			obj_t BgL_arg1229z00_1426;
																			obj_t BgL_arg1230z00_1427;

																			{	/* Match/compiler.scm 80 */
																				obj_t BgL_pairz00_2525;

																				BgL_pairz00_2525 =
																					CDR(((obj_t) BgL_fz00_4));
																				BgL_arg1229z00_1426 =
																					CAR(BgL_pairz00_2525);
																			}
																			{	/* Match/compiler.scm 80 */
																				obj_t BgL_pairz00_2531;

																				{	/* Match/compiler.scm 80 */
																					obj_t BgL_pairz00_2530;

																					BgL_pairz00_2530 =
																						CDR(((obj_t) BgL_fz00_4));
																					BgL_pairz00_2531 =
																						CDR(BgL_pairz00_2530);
																				}
																				BgL_arg1230z00_1427 =
																					CAR(BgL_pairz00_2531);
																			}
																			return
																				BGl_compilezd2orzd2zz__match_compilerz00
																				(BgL_arg1229z00_1426,
																				BgL_arg1230z00_1427, BgL_ez00_5,
																				BgL_rz00_6, BgL_mz00_7, BgL_kz00_8,
																				BgL_za7za7_9, BgL_dz00_10);
																		}
																	else
																		{	/* Match/compiler.scm 74 */
																			if (
																				(BgL_casezd2valuezd2_1416 ==
																					BGl_symbol2164z00zz__match_compilerz00))
																				{	/* Match/compiler.scm 81 */
																					obj_t BgL_arg1232z00_1429;
																					obj_t BgL_arg1233z00_1430;
																					obj_t BgL_arg1234z00_1431;

																					{	/* Match/compiler.scm 81 */
																						obj_t BgL_pairz00_2536;

																						BgL_pairz00_2536 =
																							CDR(((obj_t) BgL_fz00_4));
																						BgL_arg1232z00_1429 =
																							CAR(BgL_pairz00_2536);
																					}
																					{	/* Match/compiler.scm 81 */
																						obj_t BgL_pairz00_2542;

																						{	/* Match/compiler.scm 81 */
																							obj_t BgL_pairz00_2541;

																							BgL_pairz00_2541 =
																								CDR(((obj_t) BgL_fz00_4));
																							BgL_pairz00_2542 =
																								CDR(BgL_pairz00_2541);
																						}
																						BgL_arg1233z00_1430 =
																							CAR(BgL_pairz00_2542);
																					}
																					{	/* Match/compiler.scm 81 */
																						obj_t BgL_pairz00_2550;

																						{	/* Match/compiler.scm 81 */
																							obj_t BgL_pairz00_2549;

																							{	/* Match/compiler.scm 81 */
																								obj_t BgL_pairz00_2548;

																								BgL_pairz00_2548 =
																									CDR(((obj_t) BgL_fz00_4));
																								BgL_pairz00_2549 =
																									CDR(BgL_pairz00_2548);
																							}
																							BgL_pairz00_2550 =
																								CDR(BgL_pairz00_2549);
																						}
																						BgL_arg1234z00_1431 =
																							CAR(BgL_pairz00_2550);
																					}
																					{	/* Match/compiler.scm 196 */
																						obj_t BgL_za2varsza2z00_2551;

																						BgL_za2varsza2z00_2551 =
																							BGl_patternzd2variableszd2zz__match_descriptionsz00
																							(BgL_arg1232z00_1429);
																						{	/* Match/compiler.scm 199 */
																							obj_t
																								BgL_zc3z04anonymousza31421ze3z87_3411;
																							obj_t
																								BgL_zc3z04anonymousza31419ze3z87_3412;
																							BgL_zc3z04anonymousza31421ze3z87_3411
																								=
																								MAKE_FX_PROCEDURE
																								(BGl_z62zc3z04anonymousza31421ze3ze5zz__match_compilerz00,
																								(int) (1L), (int) (6L));
																							BgL_zc3z04anonymousza31419ze3z87_3412
																								=
																								MAKE_FX_PROCEDURE
																								(BGl_z62zc3z04anonymousza31419ze3ze5zz__match_compilerz00,
																								(int) (3L), (int) (2L));
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31421ze3z87_3411,
																								(int) (0L),
																								BgL_arg1234z00_1431);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31421ze3z87_3411,
																								(int) (1L), BgL_ez00_5);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31421ze3z87_3411,
																								(int) (2L), BgL_rz00_6);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31421ze3z87_3411,
																								(int) (3L), BgL_mz00_7);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31421ze3z87_3411,
																								(int) (4L), BgL_kz00_8);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31421ze3z87_3411,
																								(int) (5L), BgL_za7za7_9);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31419ze3z87_3412,
																								(int) (0L),
																								BgL_za2varsza2z00_2551);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31419ze3z87_3412,
																								(int) (1L),
																								BgL_arg1233z00_1430);
																							{
																								obj_t BgL_za7za7_3980;
																								obj_t BgL_kz00_3979;
																								obj_t BgL_fz00_3978;

																								BgL_fz00_3978 =
																									BgL_arg1232z00_1429;
																								BgL_kz00_3979 =
																									BgL_zc3z04anonymousza31419ze3z87_3412;
																								BgL_za7za7_3980 =
																									BgL_zc3z04anonymousza31421ze3z87_3411;
																								BgL_za7za7_9 = BgL_za7za7_3980;
																								BgL_kz00_8 = BgL_kz00_3979;
																								BgL_fz00_4 = BgL_fz00_3978;
																								goto
																									BGl_compilez00zz__match_compilerz00;
																							}
																						}
																					}
																				}
																			else
																				{	/* Match/compiler.scm 74 */
																					if (
																						(BgL_casezd2valuezd2_1416 ==
																							BGl_symbol2166z00zz__match_compilerz00))
																						{	/* Match/compiler.scm 83 */
																							obj_t BgL_arg1236z00_1433;
																							obj_t BgL_arg1238z00_1434;

																							{	/* Match/compiler.scm 83 */
																								obj_t BgL_pairz00_2564;

																								BgL_pairz00_2564 =
																									CDR(((obj_t) BgL_fz00_4));
																								BgL_arg1236z00_1433 =
																									CAR(BgL_pairz00_2564);
																							}
																							{	/* Match/compiler.scm 83 */
																								obj_t BgL_pairz00_2570;

																								{	/* Match/compiler.scm 83 */
																									obj_t BgL_pairz00_2569;

																									BgL_pairz00_2569 =
																										CDR(((obj_t) BgL_fz00_4));
																									BgL_pairz00_2570 =
																										CDR(BgL_pairz00_2569);
																								}
																								BgL_arg1238z00_1434 =
																									CAR(BgL_pairz00_2570);
																							}
																							if (CBOOL
																								(BGl_compatiblezf3zf3zz__match_descriptionsz00
																									(BgL_arg1236z00_1433,
																										BgL_arg1238z00_1434)))
																								{	/* Match/compiler.scm 208 */
																									obj_t
																										BgL_zc3z04anonymousza31424ze3z87_3413;
																									BgL_zc3z04anonymousza31424ze3z87_3413
																										=
																										MAKE_FX_PROCEDURE
																										(BGl_z62zc3z04anonymousza31424ze3ze5zz__match_compilerz00,
																										(int) (3L), (int) (4L));
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31424ze3z87_3413,
																										(int) (0L),
																										BgL_arg1238z00_1434);
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31424ze3z87_3413,
																										(int) (1L), BgL_ez00_5);
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31424ze3z87_3413,
																										(int) (2L), BgL_mz00_7);
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31424ze3z87_3413,
																										(int) (3L), BgL_kz00_8);
																									{
																										obj_t BgL_kz00_4005;
																										obj_t BgL_fz00_4004;

																										BgL_fz00_4004 =
																											BgL_arg1236z00_1433;
																										BgL_kz00_4005 =
																											BgL_zc3z04anonymousza31424ze3z87_3413;
																										BgL_kz00_8 = BgL_kz00_4005;
																										BgL_fz00_4 = BgL_fz00_4004;
																										goto
																											BGl_compilez00zz__match_compilerz00;
																									}
																								}
																							else
																								{	/* Match/compiler.scm 205 */
																									return
																										BGL_PROCEDURE_CALL1
																										(BgL_za7za7_9, BgL_dz00_10);
																								}
																						}
																					else
																						{	/* Match/compiler.scm 74 */
																							if (
																								(BgL_casezd2valuezd2_1416 ==
																									BGl_symbol2168z00zz__match_compilerz00))
																								{	/* Match/compiler.scm 84 */
																									obj_t BgL_arg1242z00_1436;
																									obj_t BgL_arg1244z00_1437;

																									{	/* Match/compiler.scm 84 */
																										obj_t BgL_pairz00_2580;

																										BgL_pairz00_2580 =
																											CDR(((obj_t) BgL_fz00_4));
																										BgL_arg1242z00_1436 =
																											CAR(BgL_pairz00_2580);
																									}
																									{	/* Match/compiler.scm 84 */
																										obj_t BgL_pairz00_2586;

																										{	/* Match/compiler.scm 84 */
																											obj_t BgL_pairz00_2585;

																											BgL_pairz00_2585 =
																												CDR(
																												((obj_t) BgL_fz00_4));
																											BgL_pairz00_2586 =
																												CDR(BgL_pairz00_2585);
																										}
																										BgL_arg1244z00_1437 =
																											CAR(BgL_pairz00_2586);
																									}
																									if (
																										(CAR(
																												((obj_t) BgL_dz00_10))
																											==
																											BGl_symbol2168z00zz__match_compilerz00))
																										{	/* Match/compiler.scm 250 */
																											return
																												BGl_succeszd2conszd2zz__match_compilerz00
																												(BgL_arg1242z00_1436,
																												BgL_arg1244z00_1437,
																												BgL_ez00_5, BgL_rz00_6,
																												BgL_mz00_7, BgL_kz00_8,
																												BgL_za7za7_9,
																												BgL_dz00_10);
																										}
																									else
																										{	/* Match/compiler.scm 252 */
																											obj_t BgL_arg1464z00_2588;

																											{	/* Match/compiler.scm 252 */
																												obj_t
																													BgL_arg1465z00_2589;
																												obj_t
																													BgL_arg1466z00_2590;
																												{	/* Match/compiler.scm 252 */
																													obj_t
																														BgL_arg1467z00_2591;
																													BgL_arg1467z00_2591 =
																														MAKE_YOUNG_PAIR
																														(BgL_ez00_5, BNIL);
																													BgL_arg1465z00_2589 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2170z00zz__match_compilerz00,
																														BgL_arg1467z00_2591);
																												}
																												{	/* Match/compiler.scm 253 */
																													obj_t
																														BgL_arg1468z00_2592;
																													obj_t
																														BgL_arg1469z00_2593;
																													BgL_arg1468z00_2592 =
																														BGl_succeszd2conszd2zz__match_compilerz00
																														(BgL_arg1242z00_1436,
																														BgL_arg1244z00_1437,
																														BgL_ez00_5,
																														BgL_rz00_6,
																														BgL_mz00_7,
																														BgL_kz00_8,
																														BgL_za7za7_9,
																														BgL_dz00_10);
																													{	/* Match/compiler.scm 254 */
																														obj_t
																															BgL_arg1472z00_2594;
																														{	/* Match/compiler.scm 254 */
																															obj_t
																																BgL_arg1473z00_2595;
																															{	/* Match/compiler.scm 254 */
																																obj_t
																																	BgL_arg1474z00_2596;
																																{	/* Match/compiler.scm 254 */
																																	obj_t
																																		BgL_list1475z00_2597;
																																	{	/* Match/compiler.scm 254 */
																																		obj_t
																																			BgL_arg1476z00_2598;
																																		{	/* Match/compiler.scm 254 */
																																			obj_t
																																				BgL_arg1477z00_2599;
																																			BgL_arg1477z00_2599
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_list2146z00zz__match_compilerz00,
																																				BNIL);
																																			BgL_arg1476z00_2598
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_list2146z00zz__match_compilerz00,
																																				BgL_arg1477z00_2599);
																																		}
																																		BgL_list1475z00_2597
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2168z00zz__match_compilerz00,
																																			BgL_arg1476z00_2598);
																																	}
																																	BgL_arg1474z00_2596
																																		=
																																		BgL_list1475z00_2597;
																																}
																																BgL_arg1473z00_2595
																																	=
																																	BGl_patternzd2minuszd2zz__match_descriptionsz00
																																	(BgL_dz00_10,
																																	BgL_arg1474z00_2596);
																															}
																															BgL_arg1472z00_2594
																																=
																																BGL_PROCEDURE_CALL1
																																(BgL_za7za7_9,
																																BgL_arg1473z00_2595);
																														}
																														BgL_arg1469z00_2593
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1472z00_2594,
																															BNIL);
																													}
																													BgL_arg1466z00_2590 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1468z00_2592,
																														BgL_arg1469z00_2593);
																												}
																												BgL_arg1464z00_2588 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1465z00_2589,
																													BgL_arg1466z00_2590);
																											}
																											return
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2154z00zz__match_compilerz00,
																												BgL_arg1464z00_2588);
																										}
																								}
																							else
																								{	/* Match/compiler.scm 74 */
																									if (
																										(BgL_casezd2valuezd2_1416 ==
																											BGl_symbol2172z00zz__match_compilerz00))
																										{	/* Match/compiler.scm 86 */
																											obj_t BgL_arg1248z00_1439;
																											obj_t BgL_arg1249z00_1440;
																											obj_t BgL_arg1252z00_1441;

																											{	/* Match/compiler.scm 86 */
																												obj_t BgL_pairz00_2607;

																												BgL_pairz00_2607 =
																													CDR(
																													((obj_t) BgL_fz00_4));
																												BgL_arg1248z00_1439 =
																													CAR(BgL_pairz00_2607);
																											}
																											{	/* Match/compiler.scm 86 */
																												obj_t BgL_pairz00_2613;

																												{	/* Match/compiler.scm 86 */
																													obj_t
																														BgL_pairz00_2612;
																													BgL_pairz00_2612 =
																														CDR(((obj_t)
																															BgL_fz00_4));
																													BgL_pairz00_2613 =
																														CDR
																														(BgL_pairz00_2612);
																												}
																												BgL_arg1249z00_1440 =
																													CAR(BgL_pairz00_2613);
																											}
																											{	/* Match/compiler.scm 86 */
																												obj_t BgL_pairz00_2621;

																												{	/* Match/compiler.scm 86 */
																													obj_t
																														BgL_pairz00_2620;
																													{	/* Match/compiler.scm 86 */
																														obj_t
																															BgL_pairz00_2619;
																														BgL_pairz00_2619 =
																															CDR(((obj_t)
																																BgL_fz00_4));
																														BgL_pairz00_2620 =
																															CDR
																															(BgL_pairz00_2619);
																													}
																													BgL_pairz00_2621 =
																														CDR
																														(BgL_pairz00_2620);
																												}
																												BgL_arg1252z00_1441 =
																													CAR(BgL_pairz00_2621);
																											}
																											return
																												BGl_compilezd2timeszd2zz__match_compilerz00
																												(BgL_arg1248z00_1439,
																												BgL_arg1249z00_1440,
																												BgL_arg1252z00_1441,
																												BgL_ez00_5, BgL_rz00_6,
																												BgL_mz00_7, BgL_kz00_8,
																												BgL_za7za7_9,
																												BgL_dz00_10);
																										}
																									else
																										{	/* Match/compiler.scm 74 */
																											if (
																												(BgL_casezd2valuezd2_1416
																													==
																													BGl_symbol2174z00zz__match_compilerz00))
																												{	/* Match/compiler.scm 325 */
																													obj_t
																														BgL_list1556z00_2641;
																													BgL_list1556z00_2641 =
																														MAKE_YOUNG_PAIR
																														(BGl_string2176z00zz__match_compilerz00,
																														BNIL);
																													return
																														BGl_errorz00zz__errorz00
																														(BGl_string2177z00zz__match_compilerz00,
																														BgL_list1556z00_2641,
																														BGl_string2178z00zz__match_compilerz00);
																												}
																											else
																												{	/* Match/compiler.scm 74 */
																													if (
																														(BgL_casezd2valuezd2_1416
																															==
																															BGl_symbol2179z00zz__match_compilerz00))
																														{	/* Match/compiler.scm 89 */
																															obj_t
																																BgL_arg1304z00_1447;
																															{	/* Match/compiler.scm 89 */
																																obj_t
																																	BgL_pairz00_2646;
																																BgL_pairz00_2646
																																	=
																																	CDR(((obj_t)
																																		BgL_fz00_4));
																																BgL_arg1304z00_1447
																																	=
																																	CAR
																																	(BgL_pairz00_2646);
																															}
																															{	/* Match/compiler.scm 322 */
																																obj_t
																																	BgL_arg1552z00_2647;
																																obj_t
																																	BgL_arg1553z00_2648;
																																{	/* Match/compiler.scm 322 */
																																	obj_t
																																		BgL_fun1555z00_2649;
																																	BgL_fun1555z00_2649
																																		=
																																		BGL_PROCEDURE_CALL1
																																		(BgL_mz00_7,
																																		BgL_arg1304z00_1447);
																																	BgL_arg1552z00_2647
																																		=
																																		BGL_PROCEDURE_CALL5
																																		(BgL_fun1555z00_2649,
																																		BgL_rz00_6,
																																		BgL_mz00_7,
																																		BgL_kz00_8,
																																		BgL_za7za7_9,
																																		BgL_dz00_10);
																																}
																																BgL_arg1553z00_2648
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_ez00_5,
																																	BNIL);
																																return
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1552z00_2647,
																																	BgL_arg1553z00_2648);
																															}
																														}
																													else
																														{	/* Match/compiler.scm 74 */
																															if (
																																(BgL_casezd2valuezd2_1416
																																	==
																																	BGl_symbol2181z00zz__match_compilerz00))
																																{	/* Match/compiler.scm 91 */
																																	obj_t
																																		BgL_arg1306z00_1449;
																																	obj_t
																																		BgL_arg1307z00_1450;
																																	{	/* Match/compiler.scm 91 */
																																		obj_t
																																			BgL_pairz00_2654;
																																		BgL_pairz00_2654
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_fz00_4));
																																		BgL_arg1306z00_1449
																																			=
																																			CAR
																																			(BgL_pairz00_2654);
																																	}
																																	{	/* Match/compiler.scm 91 */
																																		obj_t
																																			BgL_pairz00_2660;
																																		{	/* Match/compiler.scm 91 */
																																			obj_t
																																				BgL_pairz00_2659;
																																			BgL_pairz00_2659
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_fz00_4));
																																			BgL_pairz00_2660
																																				=
																																				CDR
																																				(BgL_pairz00_2659);
																																		}
																																		BgL_arg1307z00_1450
																																			=
																																			CAR
																																			(BgL_pairz00_2660);
																																	}
																																	return
																																		BGl_compilezd2vectorzd2beginz00zz__match_compilerz00
																																		(BgL_arg1306z00_1449,
																																		BgL_arg1307z00_1450,
																																		BgL_ez00_5,
																																		BgL_rz00_6,
																																		BgL_mz00_7,
																																		BgL_kz00_8,
																																		BgL_za7za7_9,
																																		BgL_dz00_10);
																																}
																															else
																																{	/* Match/compiler.scm 74 */
																																	if (
																																		(BgL_casezd2valuezd2_1416
																																			==
																																			BGl_symbol2183z00zz__match_compilerz00))
																																		{	/* Match/compiler.scm 372 */
																																			obj_t
																																				BgL_zc3z04anonymousza31641ze3z87_3414;
																																			BgL_zc3z04anonymousza31641ze3z87_3414
																																				=
																																				MAKE_FX_PROCEDURE
																																				(BGl_z62zc3z04anonymousza31641ze3ze5zz__match_compilerz00,
																																				(int)
																																				(1L),
																																				(int)
																																				(5L));
																																			PROCEDURE_SET
																																				(BgL_zc3z04anonymousza31641ze3z87_3414,
																																				(int)
																																				(0L),
																																				BgL_ez00_5);
																																			PROCEDURE_SET
																																				(BgL_zc3z04anonymousza31641ze3z87_3414,
																																				(int)
																																				(1L),
																																				BgL_kz00_8);
																																			PROCEDURE_SET
																																				(BgL_zc3z04anonymousza31641ze3z87_3414,
																																				(int)
																																				(2L),
																																				BgL_rz00_6);
																																			PROCEDURE_SET
																																				(BgL_zc3z04anonymousza31641ze3z87_3414,
																																				(int)
																																				(3L),
																																				BgL_za7za7_9);
																																			PROCEDURE_SET
																																				(BgL_zc3z04anonymousza31641ze3z87_3414,
																																				(int)
																																				(4L),
																																				BgL_dz00_10);
																																			return
																																				BgL_zc3z04anonymousza31641ze3z87_3414;
																																		}
																																	else
																																		{	/* Match/compiler.scm 74 */
																																			if (
																																				(BgL_casezd2valuezd2_1416
																																					==
																																					BGl_symbol2185z00zz__match_compilerz00))
																																				{	/* Match/compiler.scm 378 */
																																					obj_t
																																						BgL_zc3z04anonymousza31650ze3z87_3415;
																																					BgL_zc3z04anonymousza31650ze3z87_3415
																																						=
																																						MAKE_FX_PROCEDURE
																																						(BGl_z62zc3z04anonymousza31650ze3ze5zz__match_compilerz00,
																																						(int)
																																						(1L),
																																						(int)
																																						(4L));
																																					PROCEDURE_SET
																																						(BgL_zc3z04anonymousza31650ze3z87_3415,
																																						(int)
																																						(0L),
																																						BgL_kz00_8);
																																					PROCEDURE_SET
																																						(BgL_zc3z04anonymousza31650ze3z87_3415,
																																						(int)
																																						(1L),
																																						BgL_rz00_6);
																																					PROCEDURE_SET
																																						(BgL_zc3z04anonymousza31650ze3z87_3415,
																																						(int)
																																						(2L),
																																						BgL_za7za7_9);
																																					PROCEDURE_SET
																																						(BgL_zc3z04anonymousza31650ze3z87_3415,
																																						(int)
																																						(3L),
																																						BgL_dz00_10);
																																					return
																																						BgL_zc3z04anonymousza31650ze3z87_3415;
																																				}
																																			else
																																				{	/* Match/compiler.scm 74 */
																																					if (
																																						(BgL_casezd2valuezd2_1416
																																							==
																																							BGl_symbol2187z00zz__match_compilerz00))
																																						{	/* Match/compiler.scm 95 */
																																							obj_t
																																								BgL_auxz00_4121;
																																							obj_t
																																								BgL_auxz00_4117;
																																							{	/* Match/compiler.scm 95 */
																																								obj_t
																																									BgL_pairz00_2684;
																																								{	/* Match/compiler.scm 95 */
																																									obj_t
																																										BgL_pairz00_2683;
																																									BgL_pairz00_2683
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_fz00_4));
																																									BgL_pairz00_2684
																																										=
																																										CDR
																																										(BgL_pairz00_2683);
																																								}
																																								BgL_auxz00_4121
																																									=
																																									CAR
																																									(BgL_pairz00_2684);
																																							}
																																							{	/* Match/compiler.scm 95 */
																																								obj_t
																																									BgL_pairz00_2678;
																																								BgL_pairz00_2678
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_fz00_4));
																																								BgL_auxz00_4117
																																									=
																																									CAR
																																									(BgL_pairz00_2678);
																																							}
																																							return
																																								BGl_compilezd2vectorzd2consz00zz__match_compilerz00
																																								(BgL_auxz00_4117,
																																								BgL_auxz00_4121,
																																								BgL_ez00_5,
																																								BgL_rz00_6,
																																								BgL_mz00_7,
																																								BgL_kz00_8,
																																								BgL_za7za7_9,
																																								BgL_dz00_10);
																																						}
																																					else
																																						{	/* Match/compiler.scm 74 */
																																							if ((BgL_casezd2valuezd2_1416 == BGl_symbol2189z00zz__match_compilerz00))
																																								{	/* Match/compiler.scm 382 */
																																									obj_t
																																										BgL_list1651z00_2704;
																																									BgL_list1651z00_2704
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_string2191z00zz__match_compilerz00,
																																										BNIL);
																																									return
																																										BGl_errorz00zz__errorz00
																																										(BGl_string2177z00zz__match_compilerz00,
																																										BgL_list1651z00_2704,
																																										BGl_string2178z00zz__match_compilerz00);
																																								}
																																							else
																																								{	/* Match/compiler.scm 74 */
																																									if ((BgL_casezd2valuezd2_1416 == BGl_symbol2192z00zz__match_compilerz00))
																																										{	/* Match/compiler.scm 74 */
																																											BGL_TAIL
																																												return
																																												BGl_compilezd2structzd2patz00zz__match_compilerz00
																																												(BgL_fz00_4,
																																												BgL_ez00_5,
																																												BgL_rz00_6,
																																												BgL_mz00_7,
																																												BgL_kz00_8,
																																												BgL_za7za7_9,
																																												BgL_dz00_10);
																																										}
																																									else
																																										{	/* Match/compiler.scm 100 */
																																											obj_t
																																												BgL_list1318z00_1461;
																																											{	/* Match/compiler.scm 100 */
																																												obj_t
																																													BgL_arg1319z00_1462;
																																												BgL_arg1319z00_1462
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_fz00_4,
																																													BNIL);
																																												BgL_list1318z00_1461
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_string2194z00zz__match_compilerz00,
																																													BgL_arg1319z00_1462);
																																											}
																																											return
																																												BGl_errorz00zz__errorz00
																																												(BGl_string2177z00zz__match_compilerz00,
																																												BgL_list1318z00_1461,
																																												BGl_string2178z00zz__match_compilerz00);
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
					else
						{	/* Match/compiler.scm 73 */
							return BGL_PROCEDURE_CALL1(BgL_za7za7_9, BgL_dz00_10);
						}
				}
		}

	}



/* &<@anonymous:1384> */
	obj_t BGl_z62zc3z04anonymousza31384ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3416, obj_t BgL_d2z00_3420)
	{
		{	/* Match/compiler.scm 144 */
			{	/* Match/compiler.scm 144 */
				obj_t BgL_kz00_3417;
				obj_t BgL_rz00_3418;
				obj_t BgL_za7za7_3419;

				BgL_kz00_3417 = PROCEDURE_REF(BgL_envz00_3416, (int) (0L));
				BgL_rz00_3418 = PROCEDURE_REF(BgL_envz00_3416, (int) (1L));
				BgL_za7za7_3419 = PROCEDURE_REF(BgL_envz00_3416, (int) (2L));
				return
					BGL_PROCEDURE_CALL3(BgL_kz00_3417, BgL_rz00_3418, BgL_za7za7_3419,
					BgL_d2z00_3420);
			}
		}

	}



/* &<@anonymous:1383> */
	obj_t BGl_z62zc3z04anonymousza31383ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3421, obj_t BgL_r2z00_3423, obj_t BgL_za72za7_3424,
		obj_t BgL_d2z00_3425)
	{
		{	/* Match/compiler.scm 143 */
			{	/* Match/compiler.scm 143 */
				obj_t BgL_za7za7_3422;

				BgL_za7za7_3422 = PROCEDURE_REF(BgL_envz00_3421, (int) (0L));
				return BGL_PROCEDURE_CALL1(BgL_za7za7_3422, BgL_d2z00_3425);
			}
		}

	}



/* &<@anonymous:1421> */
	obj_t BGl_z62zc3z04anonymousza31421ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3426, obj_t BgL_dz00_3433)
	{
		{	/* Match/compiler.scm 200 */
			return
				BGl_compilez00zz__match_compilerz00(PROCEDURE_REF(BgL_envz00_3426,
					(int) (0L)),
				PROCEDURE_REF(BgL_envz00_3426,
					(int) (1L)),
				PROCEDURE_REF(BgL_envz00_3426,
					(int) (2L)),
				PROCEDURE_REF(BgL_envz00_3426,
					(int) (3L)),
				PROCEDURE_REF(BgL_envz00_3426,
					(int) (4L)),
				PROCEDURE_REF(BgL_envz00_3426, (int) (5L)), BgL_dz00_3433);
		}

	}



/* &<@anonymous:1419> */
	obj_t BGl_z62zc3z04anonymousza31419ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3434, obj_t BgL_rz00_3437, obj_t BgL_za7za7_3438,
		obj_t BgL_cz00_3439)
	{
		{	/* Match/compiler.scm 198 */
			{	/* Match/compiler.scm 199 */
				obj_t BgL_za2varsza2z00_3435;
				obj_t BgL_arg1233z00_3436;

				BgL_za2varsza2z00_3435 = PROCEDURE_REF(BgL_envz00_3434, (int) (0L));
				BgL_arg1233z00_3436 = PROCEDURE_REF(BgL_envz00_3434, (int) (1L));
				return
					MAKE_YOUNG_PAIR(BgL_arg1233z00_3436,
					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
					(BgL_za2varsza2z00_3435, BNIL));
			}
		}

	}



/* &<@anonymous:1424> */
	obj_t BGl_z62zc3z04anonymousza31424ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3440, obj_t BgL_r2z00_3445, obj_t BgL_za72za7_3446,
		obj_t BgL_c2z00_3447)
	{
		{	/* Match/compiler.scm 207 */
			return
				BGl_compilez00zz__match_compilerz00(PROCEDURE_REF(BgL_envz00_3440,
					(int) (0L)),
				PROCEDURE_REF(BgL_envz00_3440,
					(int) (1L)), BgL_r2z00_3445,
				PROCEDURE_REF(BgL_envz00_3440,
					(int) (2L)),
				PROCEDURE_REF(BgL_envz00_3440,
					(int) (3L)), BgL_za72za7_3446, BgL_c2z00_3447);
		}

	}



/* &<@anonymous:1641> */
	obj_t BGl_z62zc3z04anonymousza31641ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3448, obj_t BgL_iz00_3454)
	{
		{	/* Match/compiler.scm 372 */
			{	/* Match/compiler.scm 373 */
				obj_t BgL_ez00_3449;
				obj_t BgL_kz00_3450;
				obj_t BgL_rz00_3451;
				obj_t BgL_za7za7_3452;
				obj_t BgL_dz00_3453;

				BgL_ez00_3449 = PROCEDURE_REF(BgL_envz00_3448, (int) (0L));
				BgL_kz00_3450 = PROCEDURE_REF(BgL_envz00_3448, (int) (1L));
				BgL_rz00_3451 = PROCEDURE_REF(BgL_envz00_3448, (int) (2L));
				BgL_za7za7_3452 = PROCEDURE_REF(BgL_envz00_3448, (int) (3L));
				BgL_dz00_3453 = PROCEDURE_REF(BgL_envz00_3448, (int) (4L));
				{	/* Match/compiler.scm 373 */
					obj_t BgL_arg1642z00_3687;
					obj_t BgL_arg1643z00_3688;
					obj_t BgL_arg1644z00_3689;

					{	/* Match/compiler.scm 373 */
						obj_t BgL_arg1645z00_3690;

						{	/* Match/compiler.scm 373 */
							obj_t BgL_arg1646z00_3691;

							{	/* Match/compiler.scm 373 */
								obj_t BgL_arg1648z00_3692;

								{	/* Match/compiler.scm 373 */
									obj_t BgL_arg1649z00_3693;

									BgL_arg1649z00_3693 = MAKE_YOUNG_PAIR(BgL_ez00_3449, BNIL);
									BgL_arg1648z00_3692 =
										MAKE_YOUNG_PAIR(BGl_symbol2195z00zz__match_compilerz00,
										BgL_arg1649z00_3693);
								}
								BgL_arg1646z00_3691 =
									MAKE_YOUNG_PAIR(BgL_arg1648z00_3692, BNIL);
							}
							BgL_arg1645z00_3690 =
								MAKE_YOUNG_PAIR(BgL_iz00_3454, BgL_arg1646z00_3691);
						}
						BgL_arg1642z00_3687 =
							MAKE_YOUNG_PAIR(BGl_symbol2197z00zz__match_compilerz00,
							BgL_arg1645z00_3690);
					}
					BgL_arg1643z00_3688 =
						BGL_PROCEDURE_CALL3(BgL_kz00_3450, BgL_rz00_3451, BgL_za7za7_3452,
						BgL_dz00_3453);
					BgL_arg1644z00_3689 =
						BGL_PROCEDURE_CALL1(BgL_za7za7_3452, BgL_dz00_3453);
					return BGl_buildzd2ifzd2zz__match_compilerz00(BgL_arg1642z00_3687,
						BgL_arg1643z00_3688, BgL_arg1644z00_3689);
				}
			}
		}

	}



/* &<@anonymous:1650> */
	obj_t BGl_z62zc3z04anonymousza31650ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3455, obj_t BgL_iz00_3460)
	{
		{	/* Match/compiler.scm 378 */
			{	/* Match/compiler.scm 379 */
				obj_t BgL_kz00_3456;
				obj_t BgL_rz00_3457;
				obj_t BgL_za7za7_3458;
				obj_t BgL_dz00_3459;

				BgL_kz00_3456 = PROCEDURE_REF(BgL_envz00_3455, (int) (0L));
				BgL_rz00_3457 = PROCEDURE_REF(BgL_envz00_3455, (int) (1L));
				BgL_za7za7_3458 = PROCEDURE_REF(BgL_envz00_3455, (int) (2L));
				BgL_dz00_3459 = PROCEDURE_REF(BgL_envz00_3455, (int) (3L));
				return
					BGL_PROCEDURE_CALL3(BgL_kz00_3456, BgL_rz00_3457, BgL_za7za7_3458,
					BgL_dz00_3459);
			}
		}

	}



/* build-atom-equality-test */
	obj_t BGl_buildzd2atomzd2equalityzd2testzd2zz__match_compilerz00(obj_t
		BgL_ez00_24, obj_t BgL_constz00_25)
	{
		{	/* Match/compiler.scm 116 */
			if (NULLP(BgL_constz00_25))
				{	/* Match/compiler.scm 120 */
					obj_t BgL_arg1329z00_1471;

					BgL_arg1329z00_1471 = MAKE_YOUNG_PAIR(BgL_ez00_24, BNIL);
					return
						MAKE_YOUNG_PAIR(BGl_symbol2199z00zz__match_compilerz00,
						BgL_arg1329z00_1471);
				}
			else
				{	/* Match/compiler.scm 121 */
					bool_t BgL_test2336z00_4231;

					if (INTEGERP(BgL_constz00_25))
						{	/* Match/compiler.scm 121 */
							BgL_test2336z00_4231 = ((bool_t) 1);
						}
					else
						{	/* Match/compiler.scm 121 */
							if (CHARP(BgL_constz00_25))
								{	/* Match/compiler.scm 122 */
									BgL_test2336z00_4231 = ((bool_t) 1);
								}
							else
								{	/* Match/compiler.scm 122 */
									if (BOOLEANP(BgL_constz00_25))
										{	/* Match/compiler.scm 123 */
											BgL_test2336z00_4231 = ((bool_t) 1);
										}
									else
										{	/* Match/compiler.scm 123 */
											BgL_test2336z00_4231 = SYMBOLP(BgL_constz00_25);
										}
								}
						}
					if (BgL_test2336z00_4231)
						{	/* Match/compiler.scm 125 */
							obj_t BgL_arg1334z00_1476;

							{	/* Match/compiler.scm 125 */
								obj_t BgL_arg1335z00_1477;

								{	/* Match/compiler.scm 125 */
									obj_t BgL_arg1336z00_1478;

									{	/* Match/compiler.scm 125 */
										obj_t BgL_arg1337z00_1479;

										BgL_arg1337z00_1479 =
											MAKE_YOUNG_PAIR(BgL_constz00_25, BNIL);
										BgL_arg1336z00_1478 =
											MAKE_YOUNG_PAIR(BGl_symbol2156z00zz__match_compilerz00,
											BgL_arg1337z00_1479);
									}
									BgL_arg1335z00_1477 =
										MAKE_YOUNG_PAIR(BgL_arg1336z00_1478, BNIL);
								}
								BgL_arg1334z00_1476 =
									MAKE_YOUNG_PAIR(BgL_ez00_24, BgL_arg1335z00_1477);
							}
							return
								MAKE_YOUNG_PAIR(BGl_symbol2197z00zz__match_compilerz00,
								BgL_arg1334z00_1476);
						}
					else
						{	/* Match/compiler.scm 121 */
							if (REALP(BgL_constz00_25))
								{	/* Match/compiler.scm 127 */
									obj_t BgL_arg1339z00_1481;

									{	/* Match/compiler.scm 127 */
										obj_t BgL_arg1340z00_1482;
										obj_t BgL_arg1341z00_1483;

										{	/* Match/compiler.scm 127 */
											obj_t BgL_arg1342z00_1484;

											BgL_arg1342z00_1484 = MAKE_YOUNG_PAIR(BgL_ez00_24, BNIL);
											BgL_arg1340z00_1482 =
												MAKE_YOUNG_PAIR(BGl_symbol2201z00zz__match_compilerz00,
												BgL_arg1342z00_1484);
										}
										{	/* Match/compiler.scm 127 */
											obj_t BgL_arg1343z00_1485;

											{	/* Match/compiler.scm 127 */
												obj_t BgL_arg1344z00_1486;

												{	/* Match/compiler.scm 127 */
													obj_t BgL_arg1346z00_1487;

													BgL_arg1346z00_1487 =
														MAKE_YOUNG_PAIR(BgL_constz00_25, BNIL);
													BgL_arg1344z00_1486 =
														MAKE_YOUNG_PAIR(BgL_ez00_24, BgL_arg1346z00_1487);
												}
												BgL_arg1343z00_1485 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2203z00zz__match_compilerz00,
													BgL_arg1344z00_1486);
											}
											BgL_arg1341z00_1483 =
												MAKE_YOUNG_PAIR(BgL_arg1343z00_1485, BNIL);
										}
										BgL_arg1339z00_1481 =
											MAKE_YOUNG_PAIR(BgL_arg1340z00_1482, BgL_arg1341z00_1483);
									}
									return
										MAKE_YOUNG_PAIR(BGl_symbol2166z00zz__match_compilerz00,
										BgL_arg1339z00_1481);
								}
							else
								{	/* Match/compiler.scm 126 */
									if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_constz00_25))
										{	/* Match/compiler.scm 129 */
											obj_t BgL_arg1348z00_1489;

											{	/* Match/compiler.scm 129 */
												obj_t BgL_arg1349z00_1490;
												obj_t BgL_arg1350z00_1491;

												{	/* Match/compiler.scm 129 */
													obj_t BgL_arg1351z00_1492;

													BgL_arg1351z00_1492 =
														MAKE_YOUNG_PAIR(BgL_ez00_24, BNIL);
													BgL_arg1349z00_1490 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2205z00zz__match_compilerz00,
														BgL_arg1351z00_1492);
												}
												{	/* Match/compiler.scm 129 */
													obj_t BgL_arg1352z00_1493;

													{	/* Match/compiler.scm 129 */
														obj_t BgL_arg1354z00_1494;

														{	/* Match/compiler.scm 129 */
															obj_t BgL_arg1356z00_1495;

															BgL_arg1356z00_1495 =
																MAKE_YOUNG_PAIR(BgL_constz00_25, BNIL);
															BgL_arg1354z00_1494 =
																MAKE_YOUNG_PAIR(BgL_ez00_24,
																BgL_arg1356z00_1495);
														}
														BgL_arg1352z00_1493 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2207z00zz__match_compilerz00,
															BgL_arg1354z00_1494);
													}
													BgL_arg1350z00_1491 =
														MAKE_YOUNG_PAIR(BgL_arg1352z00_1493, BNIL);
												}
												BgL_arg1348z00_1489 =
													MAKE_YOUNG_PAIR(BgL_arg1349z00_1490,
													BgL_arg1350z00_1491);
											}
											return
												MAKE_YOUNG_PAIR(BGl_symbol2166z00zz__match_compilerz00,
												BgL_arg1348z00_1489);
										}
									else
										{	/* Match/compiler.scm 128 */
											if (STRINGP(BgL_constz00_25))
												{	/* Match/compiler.scm 131 */
													obj_t BgL_arg1358z00_1497;

													{	/* Match/compiler.scm 131 */
														obj_t BgL_arg1359z00_1498;
														obj_t BgL_arg1360z00_1499;

														{	/* Match/compiler.scm 131 */
															obj_t BgL_arg1361z00_1500;

															BgL_arg1361z00_1500 =
																MAKE_YOUNG_PAIR(BgL_ez00_24, BNIL);
															BgL_arg1359z00_1498 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2209z00zz__match_compilerz00,
																BgL_arg1361z00_1500);
														}
														{	/* Match/compiler.scm 131 */
															obj_t BgL_arg1362z00_1501;

															{	/* Match/compiler.scm 131 */
																obj_t BgL_arg1363z00_1502;

																{	/* Match/compiler.scm 131 */
																	obj_t BgL_arg1364z00_1503;

																	BgL_arg1364z00_1503 =
																		MAKE_YOUNG_PAIR(BgL_constz00_25, BNIL);
																	BgL_arg1363z00_1502 =
																		MAKE_YOUNG_PAIR(BgL_ez00_24,
																		BgL_arg1364z00_1503);
																}
																BgL_arg1362z00_1501 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2211z00zz__match_compilerz00,
																	BgL_arg1363z00_1502);
															}
															BgL_arg1360z00_1499 =
																MAKE_YOUNG_PAIR(BgL_arg1362z00_1501, BNIL);
														}
														BgL_arg1358z00_1497 =
															MAKE_YOUNG_PAIR(BgL_arg1359z00_1498,
															BgL_arg1360z00_1499);
													}
													return
														MAKE_YOUNG_PAIR
														(BGl_symbol2166z00zz__match_compilerz00,
														BgL_arg1358z00_1497);
												}
											else
												{	/* Match/compiler.scm 130 */
													if (NULLP(BgL_constz00_25))
														{	/* Match/compiler.scm 133 */
															obj_t BgL_arg1366z00_1505;

															BgL_arg1366z00_1505 =
																MAKE_YOUNG_PAIR(BgL_ez00_24, BNIL);
															return
																MAKE_YOUNG_PAIR
																(BGl_symbol2199z00zz__match_compilerz00,
																BgL_arg1366z00_1505);
														}
													else
														{	/* Match/compiler.scm 135 */
															obj_t BgL_arg1367z00_1506;

															{	/* Match/compiler.scm 135 */
																obj_t BgL_arg1368z00_1507;

																{	/* Match/compiler.scm 135 */
																	obj_t BgL_arg1369z00_1508;

																	{	/* Match/compiler.scm 135 */
																		obj_t BgL_arg1370z00_1509;

																		BgL_arg1370z00_1509 =
																			MAKE_YOUNG_PAIR(BgL_constz00_25, BNIL);
																		BgL_arg1369z00_1508 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2156z00zz__match_compilerz00,
																			BgL_arg1370z00_1509);
																	}
																	BgL_arg1368z00_1507 =
																		MAKE_YOUNG_PAIR(BgL_arg1369z00_1508, BNIL);
																}
																BgL_arg1367z00_1506 =
																	MAKE_YOUNG_PAIR(BgL_ez00_24,
																	BgL_arg1368z00_1507);
															}
															return
																MAKE_YOUNG_PAIR
																(BGl_symbol2213z00zz__match_compilerz00,
																BgL_arg1367z00_1506);
														}
												}
										}
								}
						}
				}
		}

	}



/* compile-or */
	obj_t BGl_compilezd2orzd2zz__match_compilerz00(obj_t BgL_f1z00_40,
		obj_t BgL_f2z00_41, obj_t BgL_ez00_42, obj_t BgL_rz00_43, obj_t BgL_mz00_44,
		obj_t BgL_kz00_45, obj_t BgL_za7za7_46, obj_t BgL_dz00_47)
	{
		{	/* Match/compiler.scm 151 */
			{	/* Match/compiler.scm 152 */
				obj_t BgL_za2kza2z00_1531;

				BgL_za2kza2z00_1531 =
					BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
					BGl_string2215z00zz__match_compilerz00);
				{	/* Match/compiler.scm 152 */
					obj_t BgL_za2varsza2z00_1532;

					BgL_za2varsza2z00_1532 =
						BGl_patternzd2variableszd2zz__match_descriptionsz00(BgL_f1z00_40);
					{	/* Match/compiler.scm 153 */
						obj_t BgL_za2callza2z00_1533;

						BgL_za2callza2z00_1533 =
							MAKE_YOUNG_PAIR(BgL_za2kza2z00_1531,
							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
							(BgL_za2varsza2z00_1532, BNIL));
						{	/* Match/compiler.scm 154 */
							obj_t BgL_successzd2formzd2_1534;

							{	/* Match/compiler.scm 155 */
								obj_t BgL_arg1414z00_1583;

								BgL_arg1414z00_1583 =
									BGl_extendza2za2zz__match_compilerz00(BgL_rz00_43,
									BgL_za2varsza2z00_1532);
								BgL_successzd2formzd2_1534 =
									BGL_PROCEDURE_CALL3(BgL_kz00_45, BgL_arg1414z00_1583,
									BgL_za7za7_46, BgL_dz00_47);
							}
							{	/* Match/compiler.scm 155 */

								{	/* Match/compiler.scm 157 */
									bool_t BgL_test2344z00_4297;

									if ((BgL_successzd2formzd2_1534 == BFALSE))
										{	/* Match/compiler.scm 157 */
											BgL_test2344z00_4297 = ((bool_t) 1);
										}
									else
										{	/* Match/compiler.scm 157 */
											if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(CDR
													(BgL_za2callza2z00_1533),
													CDR(((obj_t) BgL_successzd2formzd2_1534))))
												{	/* Match/compiler.scm 158 */
													BgL_test2344z00_4297 = ((bool_t) 1);
												}
											else
												{	/* Match/compiler.scm 158 */
													BgL_test2344z00_4297 =
														BGl_isDirectCallzf3zf3zz__match_compilerz00
														(BgL_successzd2formzd2_1534);
												}
										}
									if (BgL_test2344z00_4297)
										{	/* Match/compiler.scm 161 */
											obj_t BgL_zc3z04anonymousza31396ze3z87_3462;
											obj_t BgL_zc3z04anonymousza31395ze3z87_3463;

											BgL_zc3z04anonymousza31396ze3z87_3462 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31396ze3ze5zz__match_compilerz00,
												(int) (1L), (int) (6L));
											BgL_zc3z04anonymousza31395ze3z87_3463 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31395ze3ze5zz__match_compilerz00,
												(int) (3L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3462,
												(int) (0L), BgL_successzd2formzd2_1534);
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3462,
												(int) (1L), BgL_f2z00_41);
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3462,
												(int) (2L), BgL_ez00_42);
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3462,
												(int) (3L), BgL_rz00_43);
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3462,
												(int) (4L), BgL_mz00_44);
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3462,
												(int) (5L), BgL_za7za7_46);
											PROCEDURE_SET(BgL_zc3z04anonymousza31395ze3z87_3463,
												(int) (0L), BgL_successzd2formzd2_1534);
											return BGl_compilez00zz__match_compilerz00(BgL_f1z00_40,
												BgL_ez00_42, BgL_rz00_43, BgL_mz00_44,
												BgL_zc3z04anonymousza31395ze3z87_3463,
												BgL_zc3z04anonymousza31396ze3z87_3462, BgL_dz00_47);
										}
									else
										{	/* Match/compiler.scm 167 */
											obj_t BgL_resz00_1557;

											{	/* Match/compiler.scm 168 */
												obj_t BgL_zc3z04anonymousza31408ze3z87_3465;
												obj_t BgL_zc3z04anonymousza31407ze3z87_3466;

												BgL_zc3z04anonymousza31408ze3z87_3465 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31408ze3ze5zz__match_compilerz00,
													(int) (1L), (int) (6L));
												BgL_zc3z04anonymousza31407ze3z87_3466 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31407ze3ze5zz__match_compilerz00,
													(int) (3L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_3465,
													(int) (0L), BgL_za2callza2z00_1533);
												PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_3465,
													(int) (1L), BgL_f2z00_41);
												PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_3465,
													(int) (2L), BgL_ez00_42);
												PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_3465,
													(int) (3L), BgL_rz00_43);
												PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_3465,
													(int) (4L), BgL_mz00_44);
												PROCEDURE_SET(BgL_zc3z04anonymousza31408ze3z87_3465,
													(int) (5L), BgL_za7za7_46);
												PROCEDURE_SET(BgL_zc3z04anonymousza31407ze3z87_3466,
													(int) (0L), BgL_za2callza2z00_1533);
												BgL_resz00_1557 =
													BGl_compilez00zz__match_compilerz00(BgL_f1z00_40,
													BgL_ez00_42, BgL_rz00_43, BgL_mz00_44,
													BgL_zc3z04anonymousza31407ze3z87_3466,
													BgL_zc3z04anonymousza31408ze3z87_3465, BgL_dz00_47);
											}
											if (CBOOL(BgL_resz00_1557))
												{	/* Match/compiler.scm 175 */
													obj_t BgL_arg1399z00_1558;

													{	/* Match/compiler.scm 175 */
														obj_t BgL_arg1400z00_1559;
														obj_t BgL_arg1401z00_1560;

														{	/* Match/compiler.scm 175 */
															obj_t BgL_arg1402z00_1561;

															{	/* Match/compiler.scm 175 */
																obj_t BgL_arg1403z00_1562;

																{	/* Match/compiler.scm 175 */
																	obj_t BgL_arg1404z00_1563;

																	BgL_arg1404z00_1563 =
																		MAKE_YOUNG_PAIR(BgL_successzd2formzd2_1534,
																		BNIL);
																	BgL_arg1403z00_1562 =
																		MAKE_YOUNG_PAIR(BgL_za2varsza2z00_1532,
																		BgL_arg1404z00_1563);
																}
																BgL_arg1402z00_1561 =
																	MAKE_YOUNG_PAIR(BgL_za2kza2z00_1531,
																	BgL_arg1403z00_1562);
															}
															BgL_arg1400z00_1559 =
																MAKE_YOUNG_PAIR(BgL_arg1402z00_1561, BNIL);
														}
														BgL_arg1401z00_1560 =
															MAKE_YOUNG_PAIR(BgL_resz00_1557, BNIL);
														BgL_arg1399z00_1558 =
															MAKE_YOUNG_PAIR(BgL_arg1400z00_1559,
															BgL_arg1401z00_1560);
													}
													return
														MAKE_YOUNG_PAIR
														(BGl_symbol2216z00zz__match_compilerz00,
														BgL_arg1399z00_1558);
												}
											else
												{	/* Match/compiler.scm 174 */
													return BgL_resz00_1557;
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1396> */
	obj_t BGl_z62zc3z04anonymousza31396ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3467, obj_t BgL_dz00_3474)
	{
		{	/* Match/compiler.scm 162 */
			{	/* Match/compiler.scm 164 */
				obj_t BgL_successzd2formzd2_3468;
				obj_t BgL_f2z00_3469;
				obj_t BgL_ez00_3470;
				obj_t BgL_rz00_3471;
				obj_t BgL_mz00_3472;
				obj_t BgL_za7za7_3473;

				BgL_successzd2formzd2_3468 = PROCEDURE_REF(BgL_envz00_3467, (int) (0L));
				BgL_f2z00_3469 = PROCEDURE_REF(BgL_envz00_3467, (int) (1L));
				BgL_ez00_3470 = PROCEDURE_REF(BgL_envz00_3467, (int) (2L));
				BgL_rz00_3471 = PROCEDURE_REF(BgL_envz00_3467, (int) (3L));
				BgL_mz00_3472 = PROCEDURE_REF(BgL_envz00_3467, (int) (4L));
				BgL_za7za7_3473 = PROCEDURE_REF(BgL_envz00_3467, (int) (5L));
				{	/* Match/compiler.scm 164 */
					obj_t BgL_zc3z04anonymousza31398ze3z87_3694;

					BgL_zc3z04anonymousza31398ze3z87_3694 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31398ze3ze5zz__match_compilerz00,
						(int) (3L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31398ze3z87_3694, (int) (0L),
						BgL_successzd2formzd2_3468);
					return BGl_compilez00zz__match_compilerz00(BgL_f2z00_3469,
						BgL_ez00_3470, BgL_rz00_3471, BgL_mz00_3472,
						BgL_zc3z04anonymousza31398ze3z87_3694, BgL_za7za7_3473,
						BgL_dz00_3474);
				}
			}
		}

	}



/* &<@anonymous:1395> */
	obj_t BGl_z62zc3z04anonymousza31395ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3475, obj_t BgL_rz00_3477, obj_t BgL_za7za7_3478,
		obj_t BgL_dz00_3479)
	{
		{	/* Match/compiler.scm 161 */
			return PROCEDURE_REF(BgL_envz00_3475, (int) (0L));
		}

	}



/* &<@anonymous:1398> */
	obj_t BGl_z62zc3z04anonymousza31398ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3480, obj_t BgL_rz00_3482, obj_t BgL_za7za7_3483,
		obj_t BgL_dz00_3484)
	{
		{	/* Match/compiler.scm 164 */
			return PROCEDURE_REF(BgL_envz00_3480, (int) (0L));
		}

	}



/* &<@anonymous:1408> */
	obj_t BGl_z62zc3z04anonymousza31408ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3485, obj_t BgL_dz00_3492)
	{
		{	/* Match/compiler.scm 169 */
			{	/* Match/compiler.scm 171 */
				obj_t BgL_za2callza2z00_3486;
				obj_t BgL_f2z00_3487;
				obj_t BgL_ez00_3488;
				obj_t BgL_rz00_3489;
				obj_t BgL_mz00_3490;
				obj_t BgL_za7za7_3491;

				BgL_za2callza2z00_3486 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3485, (int) (0L)));
				BgL_f2z00_3487 = PROCEDURE_REF(BgL_envz00_3485, (int) (1L));
				BgL_ez00_3488 = PROCEDURE_REF(BgL_envz00_3485, (int) (2L));
				BgL_rz00_3489 = PROCEDURE_REF(BgL_envz00_3485, (int) (3L));
				BgL_mz00_3490 = PROCEDURE_REF(BgL_envz00_3485, (int) (4L));
				BgL_za7za7_3491 = PROCEDURE_REF(BgL_envz00_3485, (int) (5L));
				{	/* Match/compiler.scm 171 */
					obj_t BgL_zc3z04anonymousza31411ze3z87_3695;

					BgL_zc3z04anonymousza31411ze3z87_3695 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31411ze3ze5zz__match_compilerz00,
						(int) (3L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31411ze3z87_3695, (int) (0L),
						BgL_za2callza2z00_3486);
					return BGl_compilez00zz__match_compilerz00(BgL_f2z00_3487,
						BgL_ez00_3488, BgL_rz00_3489, BgL_mz00_3490,
						BgL_zc3z04anonymousza31411ze3z87_3695, BgL_za7za7_3491,
						BgL_dz00_3492);
				}
			}
		}

	}



/* &<@anonymous:1407> */
	obj_t BGl_z62zc3z04anonymousza31407ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3493, obj_t BgL_rz00_3495, obj_t BgL_za7za7_3496,
		obj_t BgL_dz00_3497)
	{
		{	/* Match/compiler.scm 168 */
			return ((obj_t) PROCEDURE_REF(BgL_envz00_3493, (int) (0L)));
		}

	}



/* &<@anonymous:1411> */
	obj_t BGl_z62zc3z04anonymousza31411ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3498, obj_t BgL_rz00_3500, obj_t BgL_za7za7_3501,
		obj_t BgL_dz00_3502)
	{
		{	/* Match/compiler.scm 171 */
			return ((obj_t) PROCEDURE_REF(BgL_envz00_3498, (int) (0L)));
		}

	}



/* isDirectCall? */
	bool_t BGl_isDirectCallzf3zf3zz__match_compilerz00(obj_t BgL_ez00_48)
	{
		{	/* Match/compiler.scm 182 */
			if (PAIRP(BgL_ez00_48))
				{	/* Match/compiler.scm 184 */
					obj_t BgL_prz00_1586;

					BgL_prz00_1586 = CAR(BgL_ez00_48);
					if (SYMBOLP(BgL_prz00_1586))
						{	/* Match/compiler.scm 186 */
							obj_t BgL_sz00_1588;

							{	/* Match/compiler.scm 186 */
								obj_t BgL_arg1926z00_2747;

								BgL_arg1926z00_2747 = SYMBOL_TO_STRING(BgL_prz00_1586);
								BgL_sz00_1588 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1926z00_2747);
							}
							if ((STRING_LENGTH(BgL_sz00_1588) > 3L))
								{	/* Match/compiler.scm 188 */
									obj_t BgL_sz00_1590;

									BgL_sz00_1590 = c_substring(BgL_sz00_1588, 0L, 3L);
									{	/* Match/compiler.scm 189 */
										bool_t BgL__ortest_1042z00_1591;

										{	/* Match/compiler.scm 189 */
											long BgL_l1z00_2753;

											BgL_l1z00_2753 = STRING_LENGTH(BgL_sz00_1590);
											if ((BgL_l1z00_2753 == 3L))
												{	/* Match/compiler.scm 189 */
													int BgL_arg1918z00_2756;

													{	/* Match/compiler.scm 189 */
														char *BgL_auxz00_4420;
														char *BgL_tmpz00_4418;

														BgL_auxz00_4420 =
															BSTRING_TO_STRING
															(BGl_string2218z00zz__match_compilerz00);
														BgL_tmpz00_4418 = BSTRING_TO_STRING(BgL_sz00_1590);
														BgL_arg1918z00_2756 =
															memcmp(BgL_tmpz00_4418, BgL_auxz00_4420,
															BgL_l1z00_2753);
													}
													BgL__ortest_1042z00_1591 =
														((long) (BgL_arg1918z00_2756) == 0L);
												}
											else
												{	/* Match/compiler.scm 189 */
													BgL__ortest_1042z00_1591 = ((bool_t) 0);
												}
										}
										if (BgL__ortest_1042z00_1591)
											{	/* Match/compiler.scm 189 */
												return BgL__ortest_1042z00_1591;
											}
										else
											{	/* Match/compiler.scm 190 */
												long BgL_l1z00_2764;

												BgL_l1z00_2764 = STRING_LENGTH(BgL_sz00_1590);
												if ((BgL_l1z00_2764 == 3L))
													{	/* Match/compiler.scm 190 */
														int BgL_arg1918z00_2767;

														{	/* Match/compiler.scm 190 */
															char *BgL_auxz00_4431;
															char *BgL_tmpz00_4429;

															BgL_auxz00_4431 =
																BSTRING_TO_STRING
																(BGl_string2219z00zz__match_compilerz00);
															BgL_tmpz00_4429 =
																BSTRING_TO_STRING(BgL_sz00_1590);
															BgL_arg1918z00_2767 =
																memcmp(BgL_tmpz00_4429, BgL_auxz00_4431,
																BgL_l1z00_2764);
														}
														return ((long) (BgL_arg1918z00_2767) == 0L);
													}
												else
													{	/* Match/compiler.scm 190 */
														return ((bool_t) 0);
													}
											}
									}
								}
							else
								{	/* Match/compiler.scm 187 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Match/compiler.scm 185 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/compiler.scm 183 */
					return ((bool_t) 0);
				}
		}

	}



/* compile-var */
	obj_t BGl_compilezd2varzd2zz__match_compilerz00(obj_t BgL_nz00_66,
		obj_t BgL_ez00_67, obj_t BgL_rz00_68, obj_t BgL_mz00_69, obj_t BgL_kz00_70,
		obj_t BgL_za7za7_71, obj_t BgL_cz00_72)
	{
		{	/* Match/compiler.scm 215 */
			{	/* Match/compiler.scm 216 */
				bool_t BgL_test2354z00_4436;

				{	/* Match/compiler.scm 455 */
					bool_t BgL_test2355z00_4437;

					{	/* Match/compiler.scm 455 */
						obj_t BgL_tmpz00_4438;

						if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_nz00_66,
									BgL_rz00_68)))
							{	/* Match/compiler.scm 438 */
								BgL_tmpz00_4438 =
									CDR(BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_nz00_66,
										BgL_rz00_68));
							}
						else
							{	/* Match/compiler.scm 438 */
								BgL_tmpz00_4438 = BGl_symbol2220z00zz__match_compilerz00;
							}
						BgL_test2355z00_4437 =
							(BgL_tmpz00_4438 == BGl_symbol2220z00zz__match_compilerz00);
					}
					if (BgL_test2355z00_4437)
						{	/* Match/compiler.scm 455 */
							BgL_test2354z00_4436 = ((bool_t) 0);
						}
					else
						{	/* Match/compiler.scm 455 */
							BgL_test2354z00_4436 = ((bool_t) 1);
						}
				}
				if (BgL_test2354z00_4436)
					{	/* Match/compiler.scm 217 */
						obj_t BgL_arg1426z00_1613;
						obj_t BgL_arg1427z00_1614;
						obj_t BgL_arg1428z00_1615;

						{	/* Match/compiler.scm 217 */
							obj_t BgL_arg1429z00_1616;

							{	/* Match/compiler.scm 217 */
								obj_t BgL_arg1430z00_1617;

								BgL_arg1430z00_1617 = MAKE_YOUNG_PAIR(BgL_ez00_67, BNIL);
								BgL_arg1429z00_1616 =
									MAKE_YOUNG_PAIR(BgL_nz00_66, BgL_arg1430z00_1617);
							}
							BgL_arg1426z00_1613 =
								MAKE_YOUNG_PAIR(BGl_symbol2197z00zz__match_compilerz00,
								BgL_arg1429z00_1616);
						}
						{	/* Match/compiler.scm 218 */
							obj_t BgL_arg1431z00_1618;

							{	/* Match/compiler.scm 218 */
								obj_t BgL_arg1434z00_1619;

								{	/* Match/compiler.scm 218 */
									obj_t BgL_list1435z00_1620;

									{	/* Match/compiler.scm 218 */
										obj_t BgL_arg1436z00_1621;

										BgL_arg1436z00_1621 = MAKE_YOUNG_PAIR(BgL_nz00_66, BNIL);
										BgL_list1435z00_1620 =
											MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__match_compilerz00,
											BgL_arg1436z00_1621);
									}
									BgL_arg1434z00_1619 = BgL_list1435z00_1620;
								}
								BgL_arg1431z00_1618 =
									BGl_patternzd2pluszd2zz__match_descriptionsz00(BgL_cz00_72,
									BgL_arg1434z00_1619);
							}
							BgL_arg1427z00_1614 =
								BGL_PROCEDURE_CALL3(BgL_kz00_70, BgL_rz00_68, BgL_za7za7_71,
								BgL_arg1431z00_1618);
						}
						{	/* Match/compiler.scm 219 */
							obj_t BgL_arg1437z00_1622;

							{	/* Match/compiler.scm 219 */
								obj_t BgL_arg1438z00_1623;

								{	/* Match/compiler.scm 219 */
									obj_t BgL_list1439z00_1624;

									{	/* Match/compiler.scm 219 */
										obj_t BgL_arg1440z00_1625;

										BgL_arg1440z00_1625 = MAKE_YOUNG_PAIR(BgL_nz00_66, BNIL);
										BgL_list1439z00_1624 =
											MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__match_compilerz00,
											BgL_arg1440z00_1625);
									}
									BgL_arg1438z00_1623 = BgL_list1439z00_1624;
								}
								BgL_arg1437z00_1622 =
									BGl_patternzd2minuszd2zz__match_descriptionsz00(BgL_cz00_72,
									BgL_arg1438z00_1623);
							}
							BgL_arg1428z00_1615 =
								BGL_PROCEDURE_CALL1(BgL_za7za7_71, BgL_arg1437z00_1622);
						}
						return
							BGl_buildzd2ifzd2zz__match_compilerz00(BgL_arg1426z00_1613,
							BgL_arg1427z00_1614, BgL_arg1428z00_1615);
					}
				else
					{	/* Match/compiler.scm 220 */
						obj_t BgL_bodyz00_1626;

						{	/* Match/compiler.scm 220 */
							obj_t BgL_arg1448z00_1639;
							obj_t BgL_arg1449z00_1640;

							{	/* Match/compiler.scm 435 */
								obj_t BgL_arg1691z00_2795;
								obj_t BgL_arg1692z00_2796;

								BgL_arg1691z00_2795 = MAKE_YOUNG_PAIR(BgL_nz00_66, BgL_ez00_67);
								BgL_arg1692z00_2796 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_rz00_68,
									BNIL);
								BgL_arg1448z00_1639 =
									MAKE_YOUNG_PAIR(BgL_arg1691z00_2795, BgL_arg1692z00_2796);
							}
							{	/* Match/compiler.scm 221 */
								obj_t BgL_arg1450z00_1641;

								{	/* Match/compiler.scm 221 */
									obj_t BgL_list1451z00_1642;

									{	/* Match/compiler.scm 221 */
										obj_t BgL_arg1452z00_1643;

										BgL_arg1452z00_1643 = MAKE_YOUNG_PAIR(BgL_nz00_66, BNIL);
										BgL_list1451z00_1642 =
											MAKE_YOUNG_PAIR(BGl_symbol2158z00zz__match_compilerz00,
											BgL_arg1452z00_1643);
									}
									BgL_arg1450z00_1641 = BgL_list1451z00_1642;
								}
								BgL_arg1449z00_1640 =
									BGl_patternzd2pluszd2zz__match_descriptionsz00(BgL_cz00_72,
									BgL_arg1450z00_1641);
							}
							BgL_bodyz00_1626 =
								BGL_PROCEDURE_CALL3(BgL_kz00_70, BgL_arg1448z00_1639,
								BgL_za7za7_71, BgL_arg1449z00_1640);
						}
						{	/* Match/compiler.scm 222 */
							bool_t BgL_test2357z00_4477;

							{	/* Match/compiler.scm 222 */
								obj_t BgL_a1090z00_1636;

								BgL_a1090z00_1636 =
									BGl_countzd2occurrenceszd2zz__match_compilerz00(BgL_nz00_66,
									BgL_bodyz00_1626, 0L);
								{	/* Match/compiler.scm 222 */

									if (INTEGERP(BgL_a1090z00_1636))
										{	/* Match/compiler.scm 222 */
											BgL_test2357z00_4477 =
												((long) CINT(BgL_a1090z00_1636) > 1L);
										}
									else
										{	/* Match/compiler.scm 222 */
											BgL_test2357z00_4477 =
												BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_a1090z00_1636,
												BINT(1L));
										}
								}
							}
							if (BgL_test2357z00_4477)
								{	/* Match/compiler.scm 223 */
									obj_t BgL_arg1443z00_1631;

									{	/* Match/compiler.scm 223 */
										obj_t BgL_arg1444z00_1632;
										obj_t BgL_arg1445z00_1633;

										{	/* Match/compiler.scm 223 */
											obj_t BgL_arg1446z00_1634;

											{	/* Match/compiler.scm 223 */
												obj_t BgL_arg1447z00_1635;

												BgL_arg1447z00_1635 =
													MAKE_YOUNG_PAIR(BgL_ez00_67, BNIL);
												BgL_arg1446z00_1634 =
													MAKE_YOUNG_PAIR(BgL_nz00_66, BgL_arg1447z00_1635);
											}
											BgL_arg1444z00_1632 =
												MAKE_YOUNG_PAIR(BgL_arg1446z00_1634, BNIL);
										}
										BgL_arg1445z00_1633 =
											MAKE_YOUNG_PAIR(BgL_bodyz00_1626, BNIL);
										BgL_arg1443z00_1631 =
											MAKE_YOUNG_PAIR(BgL_arg1444z00_1632, BgL_arg1445z00_1633);
									}
									return
										MAKE_YOUNG_PAIR(BGl_symbol2222z00zz__match_compilerz00,
										BgL_arg1443z00_1631);
								}
							else
								{	/* Match/compiler.scm 222 */
									return
										BGl_unfoldz00zz__match_compilerz00(BgL_nz00_66, BgL_ez00_67,
										BgL_bodyz00_1626);
								}
						}
					}
			}
		}

	}



/* count-occurrences */
	obj_t BGl_countzd2occurrenceszd2zz__match_compilerz00(obj_t BgL_sz00_73,
		obj_t BgL_ez00_74, long BgL_accz00_75)
	{
		{	/* Match/compiler.scm 227 */
			if (NULLP(BgL_ez00_74))
				{	/* Match/compiler.scm 228 */
					return BINT(BgL_accz00_75);
				}
			else
				{	/* Match/compiler.scm 228 */
					if (CBOOL(BGl_atomzf3zf3zz__match_s2cfunz00(BgL_ez00_74)))
						{	/* Match/compiler.scm 230 */
							if ((BgL_sz00_73 == BgL_ez00_74))
								{	/* Match/compiler.scm 231 */
									return BINT((BgL_accz00_75 + 1L));
								}
							else
								{	/* Match/compiler.scm 231 */
									return BINT(BgL_accz00_75);
								}
						}
					else
						{	/* Match/compiler.scm 230 */
							if (PAIRP(BgL_ez00_74))
								{	/* Match/compiler.scm 234 */
									if (
										(CAR(BgL_ez00_74) ==
											BGl_symbol2156z00zz__match_compilerz00))
										{	/* Match/compiler.scm 235 */
											return BINT(BgL_accz00_75);
										}
									else
										{	/* Match/compiler.scm 237 */
											obj_t BgL_a1092z00_1649;

											BgL_a1092z00_1649 =
												BGl_countzd2occurrenceszd2zz__match_compilerz00
												(BgL_sz00_73, CAR(BgL_ez00_74), BgL_accz00_75);
											{	/* Match/compiler.scm 238 */
												obj_t BgL_b1093z00_1650;

												BgL_b1093z00_1650 =
													BGl_countzd2occurrenceszd2zz__match_compilerz00
													(BgL_sz00_73, CDR(BgL_ez00_74), BgL_accz00_75);
												{	/* Match/compiler.scm 237 */

													{	/* Match/compiler.scm 237 */
														bool_t BgL_test2364z00_4513;

														if (INTEGERP(BgL_a1092z00_1649))
															{	/* Match/compiler.scm 237 */
																BgL_test2364z00_4513 =
																	INTEGERP(BgL_b1093z00_1650);
															}
														else
															{	/* Match/compiler.scm 237 */
																BgL_test2364z00_4513 = ((bool_t) 0);
															}
														if (BgL_test2364z00_4513)
															{	/* Match/compiler.scm 237 */
																return
																	ADDFX(BgL_a1092z00_1649, BgL_b1093z00_1650);
															}
														else
															{	/* Match/compiler.scm 237 */
																return
																	BGl_2zb2zb2zz__r4_numbers_6_5z00
																	(BgL_a1092z00_1649, BgL_b1093z00_1650);
															}
													}
												}
											}
										}
								}
							else
								{	/* Match/compiler.scm 234 */
									return BFALSE;
								}
						}
				}
		}

	}



/* succes-cons */
	obj_t BGl_succeszd2conszd2zz__match_compilerz00(obj_t BgL_f1z00_85,
		obj_t BgL_f2z00_86, obj_t BgL_ez00_87, obj_t BgL_rz00_88, obj_t BgL_mz00_89,
		obj_t BgL_kz00_90, obj_t BgL_za7za7_91, obj_t BgL_cz00_92)
	{
		{	/* Match/compiler.scm 256 */
			{	/* Match/compiler.scm 257 */
				obj_t BgL_za2carza2z00_1669;

				BgL_za2carza2z00_1669 =
					BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
					BGl_string2224z00zz__match_compilerz00);
				{	/* Match/compiler.scm 257 */
					obj_t BgL_za2cdrza2z00_1670;

					BgL_za2cdrza2z00_1670 =
						BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
						BGl_string2225z00zz__match_compilerz00);
					{	/* Match/compiler.scm 258 */
						obj_t BgL_bodyz00_1671;

						{	/* Match/compiler.scm 261 */
							obj_t BgL_arg1480z00_1674;

							BgL_arg1480z00_1674 =
								BGl_patternzd2carzd2zz__match_descriptionsz00(BgL_cz00_92);
							{	/* Match/compiler.scm 263 */
								obj_t BgL_zc3z04anonymousza31496ze3z87_3503;
								obj_t BgL_zc3z04anonymousza31481ze3z87_3506;

								BgL_zc3z04anonymousza31496ze3z87_3503 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31496ze3ze5zz__match_compilerz00,
									(int) (1L), (int) (2L));
								BgL_zc3z04anonymousza31481ze3z87_3506 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31481ze3ze5zz__match_compilerz00,
									(int) (3L), (int) (6L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31496ze3z87_3503, (int) (0L),
									BgL_cz00_92);
								PROCEDURE_SET(BgL_zc3z04anonymousza31496ze3z87_3503, (int) (1L),
									BgL_za7za7_91);
								PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_3506, (int) (0L),
									BgL_cz00_92);
								PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_3506, (int) (1L),
									BgL_kz00_90);
								PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_3506, (int) (2L),
									BgL_za7za7_91);
								PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_3506, (int) (3L),
									BgL_f2z00_86);
								PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_3506, (int) (4L),
									BgL_za2cdrza2z00_1670);
								PROCEDURE_SET(BgL_zc3z04anonymousza31481ze3z87_3506, (int) (5L),
									BgL_mz00_89);
								BgL_bodyz00_1671 =
									BGl_compilez00zz__match_compilerz00(BgL_f1z00_85,
									BgL_za2carza2z00_1669, BgL_rz00_88, BgL_mz00_89,
									BgL_zc3z04anonymousza31481ze3z87_3506,
									BgL_zc3z04anonymousza31496ze3z87_3503, BgL_arg1480z00_1674);
						}}
						{	/* Match/compiler.scm 259 */

							return
								BGl_buildzd2letzd2zz__match_compilerz00(BgL_za2carza2z00_1669,
								BgL_za2cdrza2z00_1670, BgL_ez00_87, BgL_bodyz00_1671);
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1496> */
	obj_t BGl_z62zc3z04anonymousza31496ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3507, obj_t BgL_c2z00_3510)
	{
		{	/* Match/compiler.scm 267 */
			{	/* Match/compiler.scm 267 */
				obj_t BgL_cz00_3508;
				obj_t BgL_za7za7_3509;

				BgL_cz00_3508 = PROCEDURE_REF(BgL_envz00_3507, (int) (0L));
				BgL_za7za7_3509 = PROCEDURE_REF(BgL_envz00_3507, (int) (1L));
				{	/* Match/compiler.scm 267 */
					obj_t BgL_arg1497z00_3696;

					{	/* Match/compiler.scm 267 */
						obj_t BgL_arg1498z00_3697;

						BgL_arg1498z00_3697 =
							BGl_patternzd2cdrzd2zz__match_descriptionsz00(BgL_cz00_3508);
						{	/* Match/compiler.scm 267 */
							obj_t BgL_list1499z00_3698;

							{	/* Match/compiler.scm 267 */
								obj_t BgL_arg1500z00_3699;

								{	/* Match/compiler.scm 267 */
									obj_t BgL_arg1501z00_3700;

									BgL_arg1501z00_3700 =
										MAKE_YOUNG_PAIR(BgL_arg1498z00_3697, BNIL);
									BgL_arg1500z00_3699 =
										MAKE_YOUNG_PAIR(BgL_c2z00_3510, BgL_arg1501z00_3700);
								}
								BgL_list1499z00_3698 =
									MAKE_YOUNG_PAIR(BGl_symbol2168z00zz__match_compilerz00,
									BgL_arg1500z00_3699);
							}
							BgL_arg1497z00_3696 = BgL_list1499z00_3698;
					}}
					return BGL_PROCEDURE_CALL1(BgL_za7za7_3509, BgL_arg1497z00_3696);
				}
			}
		}

	}



/* &<@anonymous:1481> */
	obj_t BGl_z62zc3z04anonymousza31481ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3511, obj_t BgL_r2z00_3518, obj_t BgL_za72za7_3519,
		obj_t BgL_c2z00_3520)
	{
		{	/* Match/compiler.scm 261 */
			{	/* Match/compiler.scm 263 */
				obj_t BgL_cz00_3512;
				obj_t BgL_kz00_3513;
				obj_t BgL_za7za7_3514;
				obj_t BgL_f2z00_3515;
				obj_t BgL_za2cdrza2z00_3516;
				obj_t BgL_mz00_3517;

				BgL_cz00_3512 = PROCEDURE_REF(BgL_envz00_3511, (int) (0L));
				BgL_kz00_3513 = PROCEDURE_REF(BgL_envz00_3511, (int) (1L));
				BgL_za7za7_3514 = PROCEDURE_REF(BgL_envz00_3511, (int) (2L));
				BgL_f2z00_3515 = PROCEDURE_REF(BgL_envz00_3511, (int) (3L));
				BgL_za2cdrza2z00_3516 = PROCEDURE_REF(BgL_envz00_3511, (int) (4L));
				BgL_mz00_3517 = PROCEDURE_REF(BgL_envz00_3511, (int) (5L));
				{	/* Match/compiler.scm 263 */
					obj_t BgL_arg1484z00_3701;

					BgL_arg1484z00_3701 =
						BGl_patternzd2cdrzd2zz__match_descriptionsz00(BgL_cz00_3512);
					{	/* Match/compiler.scm 264 */
						obj_t BgL_zc3z04anonymousza31485ze3z87_3702;
						obj_t BgL_zc3z04anonymousza31490ze3z87_3703;

						BgL_zc3z04anonymousza31485ze3z87_3702 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31485ze3ze5zz__match_compilerz00,
							(int) (3L), (int) (2L));
						BgL_zc3z04anonymousza31490ze3z87_3703 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31490ze3ze5zz__match_compilerz00,
							(int) (1L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31485ze3z87_3702, (int) (0L),
							BgL_c2z00_3520);
						PROCEDURE_SET(BgL_zc3z04anonymousza31485ze3z87_3702, (int) (1L),
							BgL_kz00_3513);
						PROCEDURE_SET(BgL_zc3z04anonymousza31490ze3z87_3703, (int) (0L),
							BgL_c2z00_3520);
						PROCEDURE_SET(BgL_zc3z04anonymousza31490ze3z87_3703, (int) (1L),
							BgL_za7za7_3514);
						return BGl_compilez00zz__match_compilerz00(BgL_f2z00_3515,
							BgL_za2cdrza2z00_3516, BgL_r2z00_3518, BgL_mz00_3517,
							BgL_zc3z04anonymousza31485ze3z87_3702,
							BgL_zc3z04anonymousza31490ze3z87_3703, BgL_arg1484z00_3701);
					}
				}
			}
		}

	}



/* &<@anonymous:1490> */
	obj_t BGl_z62zc3z04anonymousza31490ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3521, obj_t BgL_c3z00_3524)
	{
		{	/* Match/compiler.scm 265 */
			{	/* Match/compiler.scm 265 */
				obj_t BgL_c2z00_3522;
				obj_t BgL_za7za7_3523;

				BgL_c2z00_3522 = PROCEDURE_REF(BgL_envz00_3521, (int) (0L));
				BgL_za7za7_3523 = PROCEDURE_REF(BgL_envz00_3521, (int) (1L));
				{	/* Match/compiler.scm 265 */
					obj_t BgL_arg1492z00_3704;

					{	/* Match/compiler.scm 265 */
						obj_t BgL_list1493z00_3705;

						{	/* Match/compiler.scm 265 */
							obj_t BgL_arg1494z00_3706;

							{	/* Match/compiler.scm 265 */
								obj_t BgL_arg1495z00_3707;

								BgL_arg1495z00_3707 = MAKE_YOUNG_PAIR(BgL_c3z00_3524, BNIL);
								BgL_arg1494z00_3706 =
									MAKE_YOUNG_PAIR(BgL_c2z00_3522, BgL_arg1495z00_3707);
							}
							BgL_list1493z00_3705 =
								MAKE_YOUNG_PAIR(BGl_symbol2168z00zz__match_compilerz00,
								BgL_arg1494z00_3706);
						}
						BgL_arg1492z00_3704 = BgL_list1493z00_3705;
					}
					return BGL_PROCEDURE_CALL1(BgL_za7za7_3523, BgL_arg1492z00_3704);
				}
			}
		}

	}



/* &<@anonymous:1485> */
	obj_t BGl_z62zc3z04anonymousza31485ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3525, obj_t BgL_r3z00_3528, obj_t BgL_za73za7_3529,
		obj_t BgL_c3z00_3530)
	{
		{	/* Match/compiler.scm 263 */
			{	/* Match/compiler.scm 264 */
				obj_t BgL_c2z00_3526;
				obj_t BgL_kz00_3527;

				BgL_c2z00_3526 = PROCEDURE_REF(BgL_envz00_3525, (int) (0L));
				BgL_kz00_3527 = PROCEDURE_REF(BgL_envz00_3525, (int) (1L));
				{	/* Match/compiler.scm 264 */
					obj_t BgL_arg1486z00_3708;

					{	/* Match/compiler.scm 264 */
						obj_t BgL_list1487z00_3709;

						{	/* Match/compiler.scm 264 */
							obj_t BgL_arg1488z00_3710;

							{	/* Match/compiler.scm 264 */
								obj_t BgL_arg1489z00_3711;

								BgL_arg1489z00_3711 = MAKE_YOUNG_PAIR(BgL_c3z00_3530, BNIL);
								BgL_arg1488z00_3710 =
									MAKE_YOUNG_PAIR(BgL_c2z00_3526, BgL_arg1489z00_3711);
							}
							BgL_list1487z00_3709 =
								MAKE_YOUNG_PAIR(BGl_symbol2168z00zz__match_compilerz00,
								BgL_arg1488z00_3710);
						}
						BgL_arg1486z00_3708 = BgL_list1487z00_3709;
					}
					return
						BGL_PROCEDURE_CALL3(BgL_kz00_3527, BgL_r3z00_3528, BgL_za73za7_3529,
						BgL_arg1486z00_3708);
				}
			}
		}

	}



/* compile-times */
	obj_t BGl_compilezd2timeszd2zz__match_compilerz00(obj_t BgL_nz00_93,
		obj_t BgL_f1z00_94, obj_t BgL_f2z00_95, obj_t BgL_ez00_96,
		obj_t BgL_rz00_97, obj_t BgL_mz00_98, obj_t BgL_k0z00_99,
		obj_t BgL_za70za7_100, obj_t BgL_d0z00_101)
	{
		{	/* Match/compiler.scm 284 */
			{	/* Match/compiler.scm 286 */
				obj_t BgL_fzd2envzd2_3581;
				obj_t BgL_dzd2envzd2_3582;

				BgL_fzd2envzd2_3581 = MAKE_CELL(BNIL);
				BgL_dzd2envzd2_3582 = MAKE_CELL(BNIL);
				{	/* Match/compiler.scm 290 */
					obj_t BgL_instanciatezd2tryzd2_3533;

					BgL_instanciatezd2tryzd2_3533 =
						MAKE_FX_PROCEDURE(BGl_z62instanciatezd2tryzb0zz__match_compilerz00,
						(int) (5L), (int) (5L));
					PROCEDURE_SET(BgL_instanciatezd2tryzd2_3533,
						(int) (0L), ((obj_t) BgL_dzd2envzd2_3582));
					PROCEDURE_SET(BgL_instanciatezd2tryzd2_3533, (int) (1L), BgL_nz00_93);
					PROCEDURE_SET(BgL_instanciatezd2tryzd2_3533,
						(int) (2L), BgL_f1z00_94);
					PROCEDURE_SET(BgL_instanciatezd2tryzd2_3533,
						(int) (3L), BgL_f2z00_95);
					PROCEDURE_SET(BgL_instanciatezd2tryzd2_3533,
						(int) (4L), ((obj_t) BgL_fzd2envzd2_3581));
					{	/* Match/compiler.scm 313 */
						obj_t BgL_reszd2bodyzd2_1710;

						{	/* Match/compiler.scm 315 */
							obj_t BgL_zc3z04anonymousza31523ze3z87_3531;
							obj_t BgL_zc3z04anonymousza31522ze3z87_3532;

							BgL_zc3z04anonymousza31523ze3z87_3531 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31523ze3ze5zz__match_compilerz00,
								(int) (1L), (int) (2L));
							BgL_zc3z04anonymousza31522ze3z87_3532 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31522ze3ze5zz__match_compilerz00,
								(int) (3L), (int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31523ze3z87_3531, (int) (0L),
								BgL_za70za7_100);
							PROCEDURE_SET(BgL_zc3z04anonymousza31523ze3z87_3531, (int) (1L),
								BgL_d0z00_101);
							PROCEDURE_SET(BgL_zc3z04anonymousza31522ze3z87_3532, (int) (0L),
								BgL_k0z00_99);
							PROCEDURE_SET(BgL_zc3z04anonymousza31522ze3z87_3532, (int) (1L),
								BgL_d0z00_101);
							BgL_reszd2bodyzd2_1710 =
								BGl_z62instanciatezd2tryzb0zz__match_compilerz00
								(BgL_instanciatezd2tryzd2_3533, BgL_rz00_97, BgL_mz00_98,
								BgL_zc3z04anonymousza31522ze3z87_3532,
								BgL_zc3z04anonymousza31523ze3z87_3531, BgL_d0z00_101);
						}
						{	/* Match/compiler.scm 318 */
							obj_t BgL_arg1502z00_1711;

							{	/* Match/compiler.scm 318 */
								obj_t BgL_arg1503z00_1712;
								obj_t BgL_arg1504z00_1713;

								{	/* Match/compiler.scm 318 */
									obj_t BgL_l1094z00_1714;

									BgL_l1094z00_1714 = CELL_REF(BgL_fzd2envzd2_3581);
									if (NULLP(BgL_l1094z00_1714))
										{	/* Match/compiler.scm 318 */
											BgL_arg1503z00_1712 = BNIL;
										}
									else
										{	/* Match/compiler.scm 318 */
											obj_t BgL_head1096z00_1716;

											{	/* Match/compiler.scm 318 */
												obj_t BgL_arg1511z00_1728;

												{	/* Match/compiler.scm 463 */
													obj_t BgL_pairz00_2864;

													{	/* Match/compiler.scm 463 */
														obj_t BgL_pairz00_2856;

														BgL_pairz00_2856 = CAR(((obj_t) BgL_l1094z00_1714));
														BgL_pairz00_2864 =
															CAR(CDR(CAR(CDR(BgL_pairz00_2856))));
													}
													BgL_arg1511z00_1728 = CAR(BgL_pairz00_2864);
												}
												BgL_head1096z00_1716 =
													MAKE_YOUNG_PAIR(BgL_arg1511z00_1728, BNIL);
											}
											{	/* Match/compiler.scm 318 */
												obj_t BgL_g1099z00_1717;

												BgL_g1099z00_1717 = CDR(((obj_t) BgL_l1094z00_1714));
												{
													obj_t BgL_l1094z00_2903;
													obj_t BgL_tail1097z00_2904;

													BgL_l1094z00_2903 = BgL_g1099z00_1717;
													BgL_tail1097z00_2904 = BgL_head1096z00_1716;
												BgL_zc3z04anonymousza31506ze3z87_2902:
													if (NULLP(BgL_l1094z00_2903))
														{	/* Match/compiler.scm 318 */
															BgL_arg1503z00_1712 = BgL_head1096z00_1716;
														}
													else
														{	/* Match/compiler.scm 318 */
															obj_t BgL_newtail1098z00_2911;

															{	/* Match/compiler.scm 318 */
																obj_t BgL_arg1509z00_2912;

																{	/* Match/compiler.scm 463 */
																	obj_t BgL_pairz00_2925;

																	{	/* Match/compiler.scm 463 */
																		obj_t BgL_pairz00_2917;

																		BgL_pairz00_2917 =
																			CAR(((obj_t) BgL_l1094z00_2903));
																		BgL_pairz00_2925 =
																			CAR(CDR(CAR(CDR(BgL_pairz00_2917))));
																	}
																	BgL_arg1509z00_2912 = CAR(BgL_pairz00_2925);
																}
																BgL_newtail1098z00_2911 =
																	MAKE_YOUNG_PAIR(BgL_arg1509z00_2912, BNIL);
															}
															SET_CDR(BgL_tail1097z00_2904,
																BgL_newtail1098z00_2911);
															{	/* Match/compiler.scm 318 */
																obj_t BgL_arg1508z00_2914;

																BgL_arg1508z00_2914 =
																	CDR(((obj_t) BgL_l1094z00_2903));
																{
																	obj_t BgL_tail1097z00_4672;
																	obj_t BgL_l1094z00_4671;

																	BgL_l1094z00_4671 = BgL_arg1508z00_2914;
																	BgL_tail1097z00_4672 =
																		BgL_newtail1098z00_2911;
																	BgL_tail1097z00_2904 = BgL_tail1097z00_4672;
																	BgL_l1094z00_2903 = BgL_l1094z00_4671;
																	goto BgL_zc3z04anonymousza31506ze3z87_2902;
																}
															}
														}
												}
											}
										}
								}
								{	/* Match/compiler.scm 318 */
									obj_t BgL_arg1514z00_1730;

									{	/* Match/compiler.scm 318 */
										obj_t BgL_arg1516z00_1731;

										BgL_arg1516z00_1731 = MAKE_YOUNG_PAIR(BgL_ez00_96, BNIL);
										BgL_arg1514z00_1730 =
											MAKE_YOUNG_PAIR(BgL_reszd2bodyzd2_1710,
											BgL_arg1516z00_1731);
									}
									BgL_arg1504z00_1713 =
										MAKE_YOUNG_PAIR(BgL_arg1514z00_1730, BNIL);
								}
								BgL_arg1502z00_1711 =
									MAKE_YOUNG_PAIR(BgL_arg1503z00_1712, BgL_arg1504z00_1713);
							}
							return
								MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_compilerz00,
								BgL_arg1502z00_1711);
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1523> */
	obj_t BGl_z62zc3z04anonymousza31523ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3536, obj_t BgL_dz00_3539)
	{
		{	/* Match/compiler.scm 316 */
			{	/* Match/compiler.scm 316 */
				obj_t BgL_za70za7_3537;
				obj_t BgL_d0z00_3538;

				BgL_za70za7_3537 = PROCEDURE_REF(BgL_envz00_3536, (int) (0L));
				BgL_d0z00_3538 = PROCEDURE_REF(BgL_envz00_3536, (int) (1L));
				return BGL_PROCEDURE_CALL1(BgL_za70za7_3537, BgL_d0z00_3538);
			}
		}

	}



/* &<@anonymous:1522> */
	obj_t BGl_z62zc3z04anonymousza31522ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3540, obj_t BgL_rz00_3543, obj_t BgL_za7za7_3544,
		obj_t BgL_dz00_3545)
	{
		{	/* Match/compiler.scm 315 */
			{	/* Match/compiler.scm 315 */
				obj_t BgL_k0z00_3541;
				obj_t BgL_d0z00_3542;

				BgL_k0z00_3541 = PROCEDURE_REF(BgL_envz00_3540, (int) (0L));
				BgL_d0z00_3542 = PROCEDURE_REF(BgL_envz00_3540, (int) (1L));
				return
					BGL_PROCEDURE_CALL3(BgL_k0z00_3541, BgL_rz00_3543, BgL_za7za7_3544,
					BgL_d0z00_3542);
			}
		}

	}



/* &<@anonymous:1548> */
	obj_t BGl_z62zc3z04anonymousza31548ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3546, obj_t BgL_d2z00_3558)
	{
		{	/* Match/compiler.scm 302 */
			{	/* Match/compiler.scm 304 */
				obj_t BgL_dzd2envzd2_3547;
				obj_t BgL_f2z00_3548;
				obj_t BgL_fzd2envzd2_3549;
				obj_t BgL_mz00_3550;
				obj_t BgL_instanciatezd2tryzd2_3551;
				obj_t BgL_nz00_3552;
				obj_t BgL_f1z00_3553;
				obj_t BgL_gz00_3554;
				obj_t BgL_rz00_3555;
				obj_t BgL_kz00_3556;
				obj_t BgL_za7za7_3557;

				BgL_dzd2envzd2_3547 = PROCEDURE_REF(BgL_envz00_3546, (int) (0L));
				BgL_f2z00_3548 = PROCEDURE_REF(BgL_envz00_3546, (int) (1L));
				BgL_fzd2envzd2_3549 = PROCEDURE_REF(BgL_envz00_3546, (int) (2L));
				BgL_mz00_3550 = PROCEDURE_REF(BgL_envz00_3546, (int) (3L));
				BgL_instanciatezd2tryzd2_3551 =
					PROCEDURE_REF(BgL_envz00_3546, (int) (4L));
				BgL_nz00_3552 = PROCEDURE_REF(BgL_envz00_3546, (int) (5L));
				BgL_f1z00_3553 = PROCEDURE_REF(BgL_envz00_3546, (int) (6L));
				BgL_gz00_3554 = PROCEDURE_REF(BgL_envz00_3546, (int) (7L));
				BgL_rz00_3555 = PROCEDURE_REF(BgL_envz00_3546, (int) (8L));
				BgL_kz00_3556 = PROCEDURE_REF(BgL_envz00_3546, (int) (9L));
				BgL_za7za7_3557 = PROCEDURE_REF(BgL_envz00_3546, (int) (10L));
				{	/* Match/compiler.scm 304 */
					obj_t BgL_arg1549z00_3712;

					{	/* Match/compiler.scm 467 */
						obj_t BgL_zc3z04anonymousza31711ze3z87_3713;

						BgL_zc3z04anonymousza31711ze3z87_3713 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31711ze3ze5zz__match_compilerz00,
							(int) (1L), (int) (7L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (0L),
							BgL_dzd2envzd2_3547);
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (1L),
							BgL_f1z00_3553);
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (2L),
							BgL_f2z00_3548);
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (3L),
							BgL_fzd2envzd2_3549);
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (4L),
							BgL_mz00_3550);
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (5L),
							BgL_instanciatezd2tryzd2_3551);
						PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3713, (int) (6L),
							BgL_nz00_3552);
						BgL_arg1549z00_3712 = BgL_zc3z04anonymousza31711ze3z87_3713;
					}
					return
						BGl_compilez00zz__match_compilerz00(BgL_f1z00_3553, BgL_gz00_3554,
						BgL_rz00_3555, BgL_arg1549z00_3712, BgL_kz00_3556, BgL_za7za7_3557,
						BgL_d2z00_3558);
				}
			}
		}

	}



/* &<@anonymous:1711> */
	obj_t BGl_z62zc3z04anonymousza31711ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3559, obj_t BgL_xz00_3567)
	{
		{	/* Match/compiler.scm 467 */
			{	/* Match/compiler.scm 468 */
				obj_t BgL_mz00_3564;
				obj_t BgL_instanciatezd2tryzd2_3565;
				obj_t BgL_nz00_3566;

				BgL_mz00_3564 = PROCEDURE_REF(BgL_envz00_3559, (int) (4L));
				BgL_instanciatezd2tryzd2_3565 =
					PROCEDURE_REF(BgL_envz00_3559, (int) (5L));
				BgL_nz00_3566 = PROCEDURE_REF(BgL_envz00_3559, (int) (6L));
				if ((BgL_xz00_3567 == BgL_nz00_3566))
					{	/* Match/compiler.scm 468 */
						return BgL_instanciatezd2tryzd2_3565;
					}
				else
					{	/* Match/compiler.scm 468 */
						return BGL_PROCEDURE_CALL1(BgL_mz00_3564, BgL_xz00_3567);
					}
			}
		}

	}



/* &instanciate-try */
	obj_t BGl_z62instanciatezd2tryzb0zz__match_compilerz00(obj_t BgL_envz00_3568,
		obj_t BgL_rz00_3574, obj_t BgL_mz00_3575, obj_t BgL_kz00_3576,
		obj_t BgL_za7za7_3577, obj_t BgL_dz00_3578)
	{
		{	/* Match/compiler.scm 289 */
			{	/* Match/compiler.scm 290 */
				obj_t BgL_dzd2envzd2_3569;
				obj_t BgL_nz00_3570;
				obj_t BgL_f1z00_3571;
				obj_t BgL_f2z00_3572;
				obj_t BgL_fzd2envzd2_3573;

				BgL_dzd2envzd2_3569 = PROCEDURE_REF(BgL_envz00_3568, (int) (0L));
				BgL_nz00_3570 = PROCEDURE_REF(BgL_envz00_3568, (int) (1L));
				BgL_f1z00_3571 = PROCEDURE_REF(BgL_envz00_3568, (int) (2L));
				BgL_f2z00_3572 = PROCEDURE_REF(BgL_envz00_3568, (int) (3L));
				BgL_fzd2envzd2_3573 = PROCEDURE_REF(BgL_envz00_3568, (int) (4L));
				{	/* Match/compiler.scm 290 */
					obj_t BgL_tmpz00_3714;

					{
						obj_t BgL_dzd2envzd2_3716;

						BgL_dzd2envzd2_3716 = CELL_REF(BgL_dzd2envzd2_3569);
					BgL_lookzd2forzd2descrz00_3715:
						if (NULLP(BgL_dzd2envzd2_3716))
							{	/* Match/compiler.scm 416 */
								BgL_tmpz00_3714 = BFALSE;
							}
						else
							{	/* Match/compiler.scm 418 */
								bool_t BgL_test2370z00_4760;

								{	/* Match/compiler.scm 418 */
									obj_t BgL_auxz00_4761;

									{	/* Match/compiler.scm 418 */
										obj_t BgL_pairz00_3717;

										BgL_pairz00_3717 = CAR(((obj_t) BgL_dzd2envzd2_3716));
										BgL_auxz00_4761 = CAR(BgL_pairz00_3717);
									}
									BgL_test2370z00_4760 =
										BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_auxz00_4761,
										BgL_dz00_3578);
								}
								if (BgL_test2370z00_4760)
									{	/* Match/compiler.scm 418 */
										BgL_tmpz00_3714 = CAR(((obj_t) BgL_dzd2envzd2_3716));
									}
								else
									{	/* Match/compiler.scm 420 */
										obj_t BgL_arg1688z00_3718;

										BgL_arg1688z00_3718 = CDR(((obj_t) BgL_dzd2envzd2_3716));
										{
											obj_t BgL_dzd2envzd2_4770;

											BgL_dzd2envzd2_4770 = BgL_arg1688z00_3718;
											BgL_dzd2envzd2_3716 = BgL_dzd2envzd2_4770;
											goto BgL_lookzd2forzd2descrz00_3715;
										}
									}
							}
					}
					if (CBOOL(BgL_tmpz00_3714))
						{	/* Match/compiler.scm 292 */
							obj_t BgL_pairz00_3719;

							BgL_pairz00_3719 = CDR(((obj_t) BgL_tmpz00_3714));
							return CAR(BgL_pairz00_3719);
						}
					else
						{	/* Match/compiler.scm 293 */
							obj_t BgL_gz00_3720;
							obj_t BgL_tryz00_3721;

							BgL_gz00_3720 =
								BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
								BGl_string2228z00zz__match_compilerz00);
							BgL_tryz00_3721 =
								BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
								BGl_string2229z00zz__match_compilerz00);
							{	/* Match/compiler.scm 295 */
								obj_t BgL_auxz00_3722;

								{	/* Match/compiler.scm 295 */
									obj_t BgL_arg1525z00_3723;

									{	/* Match/compiler.scm 295 */
										obj_t BgL_list1526z00_3724;

										{	/* Match/compiler.scm 295 */
											obj_t BgL_arg1527z00_3725;

											BgL_arg1527z00_3725 =
												MAKE_YOUNG_PAIR(BgL_tryz00_3721, BNIL);
											BgL_list1526z00_3724 =
												MAKE_YOUNG_PAIR(BgL_dz00_3578, BgL_arg1527z00_3725);
										}
										BgL_arg1525z00_3723 = BgL_list1526z00_3724;
									}
									BgL_auxz00_3722 =
										MAKE_YOUNG_PAIR(BgL_arg1525z00_3723,
										CELL_REF(BgL_dzd2envzd2_3569));
								}
								CELL_SET(BgL_dzd2envzd2_3569, BgL_auxz00_3722);
							}
							{	/* Match/compiler.scm 296 */
								obj_t BgL_newzd2defzd2_3726;

								{	/* Match/compiler.scm 299 */
									obj_t BgL_arg1528z00_3727;

									{	/* Match/compiler.scm 299 */
										obj_t BgL_arg1531z00_3728;

										{	/* Match/compiler.scm 299 */
											obj_t BgL_arg1535z00_3729;

											{	/* Match/compiler.scm 299 */
												obj_t BgL_arg1536z00_3730;

												{	/* Match/compiler.scm 299 */
													obj_t BgL_arg1537z00_3731;

													{	/* Match/compiler.scm 299 */
														obj_t BgL_arg1539z00_3732;

														{	/* Match/compiler.scm 299 */
															obj_t BgL_arg1540z00_3733;

															{	/* Match/compiler.scm 299 */
																obj_t BgL_arg1543z00_3734;
																obj_t BgL_arg1544z00_3735;

																BgL_arg1543z00_3734 =
																	MAKE_YOUNG_PAIR(BgL_gz00_3720, BNIL);
																{	/* Match/compiler.scm 304 */
																	obj_t BgL_arg1546z00_3736;

																	{	/* Match/compiler.scm 304 */
																		obj_t BgL_zc3z04anonymousza31548ze3z87_3737;

																		BgL_zc3z04anonymousza31548ze3z87_3737 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31548ze3ze5zz__match_compilerz00,
																			(int) (1L), (int) (11L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (0L), BgL_dzd2envzd2_3569);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (1L), BgL_f2z00_3572);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (2L), BgL_fzd2envzd2_3573);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (3L), BgL_mz00_3575);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (4L), BgL_envz00_3568);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (5L), BgL_nz00_3570);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (6L), BgL_f1z00_3571);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (7L), BgL_gz00_3720);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (8L), BgL_rz00_3574);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (9L), BgL_kz00_3576);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31548ze3z87_3737,
																			(int) (10L), BgL_za7za7_3577);
																		BgL_arg1546z00_3736 =
																			BGl_compilez00zz__match_compilerz00
																			(BgL_f2z00_3572, BgL_gz00_3720,
																			BgL_rz00_3574, BgL_mz00_3575,
																			BgL_kz00_3576,
																			BgL_zc3z04anonymousza31548ze3z87_3737,
																			BgL_dz00_3578);
																	}
																	BgL_arg1544z00_3735 =
																		MAKE_YOUNG_PAIR(BgL_arg1546z00_3736, BNIL);
																}
																BgL_arg1540z00_3733 =
																	MAKE_YOUNG_PAIR(BgL_arg1543z00_3734,
																	BgL_arg1544z00_3735);
															}
															BgL_arg1539z00_3732 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2150z00zz__match_compilerz00,
																BgL_arg1540z00_3733);
														}
														BgL_arg1537z00_3731 =
															MAKE_YOUNG_PAIR(BgL_arg1539z00_3732, BNIL);
													}
													BgL_arg1536z00_3730 =
														MAKE_YOUNG_PAIR(BgL_tryz00_3721,
														BgL_arg1537z00_3731);
												}
												BgL_arg1535z00_3729 =
													MAKE_YOUNG_PAIR(BgL_arg1536z00_3730, BNIL);
											}
											BgL_arg1531z00_3728 =
												MAKE_YOUNG_PAIR(BgL_arg1535z00_3729, BNIL);
										}
										BgL_arg1528z00_3727 =
											MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_compilerz00,
											BgL_arg1531z00_3728);
									}
									{	/* Match/compiler.scm 296 */
										obj_t BgL_list1529z00_3738;

										{	/* Match/compiler.scm 296 */
											obj_t BgL_arg1530z00_3739;

											BgL_arg1530z00_3739 =
												MAKE_YOUNG_PAIR(BgL_arg1528z00_3727, BNIL);
											BgL_list1529z00_3738 =
												MAKE_YOUNG_PAIR(BgL_tryz00_3721, BgL_arg1530z00_3739);
										}
										BgL_newzd2defzd2_3726 = BgL_list1529z00_3738;
								}}
								{	/* Match/compiler.scm 309 */
									obj_t BgL_auxz00_3740;

									BgL_auxz00_3740 =
										MAKE_YOUNG_PAIR(BgL_newzd2defzd2_3726,
										CELL_REF(BgL_fzd2envzd2_3573));
									CELL_SET(BgL_fzd2envzd2_3573, BgL_auxz00_3740);
								}
								return BgL_tryz00_3721;
							}
						}
				}
			}
		}

	}



/* compile-vector-begin */
	obj_t BGl_compilezd2vectorzd2beginz00zz__match_compilerz00(obj_t
		BgL_lgminz00_118, obj_t BgL_fz00_119, obj_t BgL_ez00_120,
		obj_t BgL_rz00_121, obj_t BgL_mz00_122, obj_t BgL_kz00_123,
		obj_t BgL_za7za7_124, obj_t BgL_dz00_125)
	{
		{	/* Match/compiler.scm 334 */
			if (
				(CAR(((obj_t) BgL_dz00_125)) == BGl_symbol2230z00zz__match_compilerz00))
				{	/* Match/compiler.scm 336 */
					bool_t BgL_test2373z00_4829;

					{	/* Match/compiler.scm 336 */
						obj_t BgL_a1100z00_1793;

						{	/* Match/compiler.scm 336 */
							obj_t BgL_pairz00_2937;

							BgL_pairz00_2937 = CDR(((obj_t) BgL_dz00_125));
							BgL_a1100z00_1793 = CAR(BgL_pairz00_2937);
						}
						{	/* Match/compiler.scm 336 */
							bool_t BgL_test2374z00_4833;

							if (INTEGERP(BgL_a1100z00_1793))
								{	/* Match/compiler.scm 336 */
									BgL_test2374z00_4833 = INTEGERP(BgL_lgminz00_118);
								}
							else
								{	/* Match/compiler.scm 336 */
									BgL_test2374z00_4833 = ((bool_t) 0);
								}
							if (BgL_test2374z00_4833)
								{	/* Match/compiler.scm 336 */
									BgL_test2373z00_4829 =
										(
										(long) CINT(BgL_a1100z00_1793) >=
										(long) CINT(BgL_lgminz00_118));
								}
							else
								{	/* Match/compiler.scm 336 */
									BgL_test2373z00_4829 =
										BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_a1100z00_1793,
										BgL_lgminz00_118);
								}
						}
					}
					if (BgL_test2373z00_4829)
						{	/* Match/compiler.scm 337 */
							obj_t BgL_fun1560z00_1781;

							BgL_fun1560z00_1781 =
								BGl_compilez00zz__match_compilerz00(BgL_fz00_119, BgL_ez00_120,
								BgL_rz00_121, BgL_mz00_122, BgL_kz00_123, BgL_za7za7_124,
								BgL_dz00_125);
							return BGL_PROCEDURE_CALL1(BgL_fun1560z00_1781, BINT(0L));
						}
					else
						{	/* Match/compiler.scm 339 */
							obj_t BgL_arg1561z00_1782;

							{	/* Match/compiler.scm 339 */
								obj_t BgL_arg1562z00_1783;
								obj_t BgL_arg1564z00_1784;

								{	/* Match/compiler.scm 339 */
									obj_t BgL_arg1565z00_1785;

									{	/* Match/compiler.scm 339 */
										obj_t BgL_arg1567z00_1786;
										obj_t BgL_arg1571z00_1787;

										{	/* Match/compiler.scm 339 */
											obj_t BgL_arg1573z00_1788;

											BgL_arg1573z00_1788 = MAKE_YOUNG_PAIR(BgL_ez00_120, BNIL);
											BgL_arg1567z00_1786 =
												MAKE_YOUNG_PAIR(BGl_symbol2195z00zz__match_compilerz00,
												BgL_arg1573z00_1788);
										}
										BgL_arg1571z00_1787 =
											MAKE_YOUNG_PAIR(BgL_lgminz00_118, BNIL);
										BgL_arg1565z00_1785 =
											MAKE_YOUNG_PAIR(BgL_arg1567z00_1786, BgL_arg1571z00_1787);
									}
									BgL_arg1562z00_1783 =
										MAKE_YOUNG_PAIR(BGl_symbol2232z00zz__match_compilerz00,
										BgL_arg1565z00_1785);
								}
								{	/* Match/compiler.scm 340 */
									obj_t BgL_arg1575z00_1789;
									obj_t BgL_arg1576z00_1790;

									{	/* Match/compiler.scm 340 */
										obj_t BgL_fun1577z00_1791;

										BgL_fun1577z00_1791 =
											BGl_compilez00zz__match_compilerz00(BgL_fz00_119,
											BgL_ez00_120, BgL_rz00_121, BgL_mz00_122, BgL_kz00_123,
											BgL_za7za7_124, BgL_dz00_125);
										BgL_arg1575z00_1789 =
											BGL_PROCEDURE_CALL1(BgL_fun1577z00_1791, BINT(0L));
									}
									{	/* Match/compiler.scm 342 */
										obj_t BgL_arg1578z00_1792;

										BgL_arg1578z00_1792 =
											BGL_PROCEDURE_CALL1(BgL_za7za7_124, BgL_dz00_125);
										BgL_arg1576z00_1790 =
											MAKE_YOUNG_PAIR(BgL_arg1578z00_1792, BNIL);
									}
									BgL_arg1564z00_1784 =
										MAKE_YOUNG_PAIR(BgL_arg1575z00_1789, BgL_arg1576z00_1790);
								}
								BgL_arg1561z00_1782 =
									MAKE_YOUNG_PAIR(BgL_arg1562z00_1783, BgL_arg1564z00_1784);
							}
							return
								MAKE_YOUNG_PAIR(BGl_symbol2154z00zz__match_compilerz00,
								BgL_arg1561z00_1782);
						}
				}
			else
				{	/* Match/compiler.scm 343 */
					obj_t BgL_arg1579z00_1795;

					{	/* Match/compiler.scm 343 */
						obj_t BgL_arg1580z00_1796;
						obj_t BgL_arg1582z00_1797;

						{	/* Match/compiler.scm 343 */
							obj_t BgL_arg1583z00_1798;

							BgL_arg1583z00_1798 = MAKE_YOUNG_PAIR(BgL_ez00_120, BNIL);
							BgL_arg1580z00_1796 =
								MAKE_YOUNG_PAIR(BGl_symbol2234z00zz__match_compilerz00,
								BgL_arg1583z00_1798);
						}
						{	/* Match/compiler.scm 344 */
							obj_t BgL_arg1584z00_1799;
							obj_t BgL_arg1585z00_1800;

							{	/* Match/compiler.scm 344 */
								obj_t BgL_arg1586z00_1801;

								{	/* Match/compiler.scm 344 */
									obj_t BgL_arg1587z00_1802;
									obj_t BgL_arg1589z00_1803;

									{	/* Match/compiler.scm 344 */
										obj_t BgL_arg1591z00_1804;

										{	/* Match/compiler.scm 344 */
											obj_t BgL_arg1593z00_1805;
											obj_t BgL_arg1594z00_1806;

											{	/* Match/compiler.scm 344 */
												obj_t BgL_arg1595z00_1807;

												BgL_arg1595z00_1807 =
													MAKE_YOUNG_PAIR(BgL_ez00_120, BNIL);
												BgL_arg1593z00_1805 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2195z00zz__match_compilerz00,
													BgL_arg1595z00_1807);
											}
											BgL_arg1594z00_1806 =
												MAKE_YOUNG_PAIR(BgL_lgminz00_118, BNIL);
											BgL_arg1591z00_1804 =
												MAKE_YOUNG_PAIR(BgL_arg1593z00_1805,
												BgL_arg1594z00_1806);
										}
										BgL_arg1587z00_1802 =
											MAKE_YOUNG_PAIR(BGl_symbol2232z00zz__match_compilerz00,
											BgL_arg1591z00_1804);
									}
									{	/* Match/compiler.scm 348 */
										obj_t BgL_arg1598z00_1808;
										obj_t BgL_arg1601z00_1809;

										{	/* Match/compiler.scm 348 */
											obj_t BgL_fun1608z00_1814;

											{	/* Match/compiler.scm 348 */
												obj_t BgL_arg1602z00_1810;

												{	/* Match/compiler.scm 348 */
													obj_t BgL_arg1603z00_1811;

													{	/* Match/compiler.scm 348 */
														obj_t BgL_arg1605z00_1812;

														BgL_arg1605z00_1812 =
															MAKE_YOUNG_PAIR(make_vector(
																(long) CINT(BgL_lgminz00_118),
																BGl_list2146z00zz__match_compilerz00), BNIL);
														BgL_arg1603z00_1811 =
															MAKE_YOUNG_PAIR(BgL_lgminz00_118,
															BgL_arg1605z00_1812);
													}
													BgL_arg1602z00_1810 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2230z00zz__match_compilerz00,
														BgL_arg1603z00_1811);
												}
												BgL_fun1608z00_1814 =
													BGl_compilez00zz__match_compilerz00(BgL_fz00_119,
													BgL_ez00_120, BgL_rz00_121, BgL_mz00_122,
													BgL_kz00_123, BgL_za7za7_124, BgL_arg1602z00_1810);
											}
											BgL_arg1598z00_1808 =
												BGL_PROCEDURE_CALL1(BgL_fun1608z00_1814, BINT(0L));
										}
										{	/* Match/compiler.scm 350 */
											obj_t BgL_arg1609z00_1815;

											{	/* Match/compiler.scm 350 */
												obj_t BgL_arg1610z00_1816;

												{	/* Match/compiler.scm 350 */
													obj_t BgL_arg1611z00_1817;

													{	/* Match/compiler.scm 350 */
														obj_t BgL_arg1612z00_1818;

														BgL_arg1612z00_1818 =
															MAKE_YOUNG_PAIR(make_vector(0L,
																BGl_list2146z00zz__match_compilerz00), BNIL);
														BgL_arg1611z00_1817 =
															MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1612z00_1818);
													}
													BgL_arg1610z00_1816 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2230z00zz__match_compilerz00,
														BgL_arg1611z00_1817);
												}
												BgL_arg1609z00_1815 =
													BGL_PROCEDURE_CALL1(BgL_za7za7_124,
													BgL_arg1610z00_1816);
											}
											BgL_arg1601z00_1809 =
												MAKE_YOUNG_PAIR(BgL_arg1609z00_1815, BNIL);
										}
										BgL_arg1589z00_1803 =
											MAKE_YOUNG_PAIR(BgL_arg1598z00_1808, BgL_arg1601z00_1809);
									}
									BgL_arg1586z00_1801 =
										MAKE_YOUNG_PAIR(BgL_arg1587z00_1802, BgL_arg1589z00_1803);
								}
								BgL_arg1584z00_1799 =
									MAKE_YOUNG_PAIR(BGl_symbol2154z00zz__match_compilerz00,
									BgL_arg1586z00_1801);
							}
							{	/* Match/compiler.scm 351 */
								obj_t BgL_arg1615z00_1820;

								{	/* Match/compiler.scm 351 */
									obj_t BgL_arg1616z00_1821;

									BgL_arg1616z00_1821 =
										BGl_patternzd2pluszd2zz__match_descriptionsz00(BgL_dz00_125,
										BGl_list2236z00zz__match_compilerz00);
									BgL_arg1615z00_1820 =
										BGL_PROCEDURE_CALL1(BgL_za7za7_124, BgL_arg1616z00_1821);
								}
								BgL_arg1585z00_1800 =
									MAKE_YOUNG_PAIR(BgL_arg1615z00_1820, BNIL);
							}
							BgL_arg1582z00_1797 =
								MAKE_YOUNG_PAIR(BgL_arg1584z00_1799, BgL_arg1585z00_1800);
						}
						BgL_arg1579z00_1795 =
							MAKE_YOUNG_PAIR(BgL_arg1580z00_1796, BgL_arg1582z00_1797);
					}
					return
						MAKE_YOUNG_PAIR(BGl_symbol2154z00zz__match_compilerz00,
						BgL_arg1579z00_1795);
				}
		}

	}



/* compile-vector-cons */
	obj_t BGl_compilezd2vectorzd2consz00zz__match_compilerz00(obj_t BgL_f1z00_126,
		obj_t BgL_f2z00_127, obj_t BgL_ez00_128, obj_t BgL_rz00_129,
		obj_t BgL_mz00_130, obj_t BgL_kz00_131, obj_t BgL_za7za7_132,
		obj_t BgL_dz00_133)
	{
		{	/* Match/compiler.scm 357 */
			{	/* Match/compiler.scm 358 */
				obj_t BgL_zc3z04anonymousza31617ze3z87_3587;

				BgL_zc3z04anonymousza31617ze3z87_3587 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31617ze3ze5zz__match_compilerz00, (int) (1L),
					(int) (8L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (0L),
					BgL_dz00_133);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (1L),
					BgL_ez00_128);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (2L),
					BgL_f2z00_127);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (3L),
					BgL_mz00_130);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (4L),
					BgL_kz00_131);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (5L),
					BgL_za7za7_132);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (6L),
					BgL_f1z00_126);
				PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3587, (int) (7L),
					BgL_rz00_129);
				return BgL_zc3z04anonymousza31617ze3z87_3587;
			}
		}

	}



/* &<@anonymous:1617> */
	obj_t BGl_z62zc3z04anonymousza31617ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3588, obj_t BgL_iz00_3597)
	{
		{	/* Match/compiler.scm 358 */
			{	/* Match/compiler.scm 359 */
				obj_t BgL_dz00_3589;
				obj_t BgL_ez00_3590;
				obj_t BgL_f2z00_3591;
				obj_t BgL_mz00_3592;
				obj_t BgL_kz00_3593;
				obj_t BgL_za7za7_3594;
				obj_t BgL_f1z00_3595;
				obj_t BgL_rz00_3596;

				BgL_dz00_3589 = PROCEDURE_REF(BgL_envz00_3588, (int) (0L));
				BgL_ez00_3590 = PROCEDURE_REF(BgL_envz00_3588, (int) (1L));
				BgL_f2z00_3591 = PROCEDURE_REF(BgL_envz00_3588, (int) (2L));
				BgL_mz00_3592 = PROCEDURE_REF(BgL_envz00_3588, (int) (3L));
				BgL_kz00_3593 = PROCEDURE_REF(BgL_envz00_3588, (int) (4L));
				BgL_za7za7_3594 = PROCEDURE_REF(BgL_envz00_3588, (int) (5L));
				BgL_f1z00_3595 = PROCEDURE_REF(BgL_envz00_3588, (int) (6L));
				BgL_rz00_3596 = PROCEDURE_REF(BgL_envz00_3588, (int) (7L));
				{	/* Match/compiler.scm 359 */
					bool_t BgL_test2376z00_4941;

					{	/* Match/compiler.scm 359 */
						long BgL_b1101z00_3741;

						{	/* Match/compiler.scm 359 */
							obj_t BgL_arg1625z00_3742;

							{	/* Match/compiler.scm 359 */
								obj_t BgL_pairz00_3743;

								{	/* Match/compiler.scm 359 */
									obj_t BgL_pairz00_3744;

									BgL_pairz00_3744 = CDR(((obj_t) BgL_dz00_3589));
									BgL_pairz00_3743 = CDR(BgL_pairz00_3744);
								}
								BgL_arg1625z00_3742 = CAR(BgL_pairz00_3743);
							}
							BgL_b1101z00_3741 = VECTOR_LENGTH(((obj_t) BgL_arg1625z00_3742));
						}
						if (INTEGERP(BgL_iz00_3597))
							{	/* Match/compiler.scm 359 */
								BgL_test2376z00_4941 =
									((long) CINT(BgL_iz00_3597) >= BgL_b1101z00_3741);
							}
						else
							{	/* Match/compiler.scm 359 */
								BgL_test2376z00_4941 =
									BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_iz00_3597,
									BINT(BgL_b1101z00_3741));
							}
					}
					if (BgL_test2376z00_4941)
						{	/* Match/compiler.scm 360 */
							obj_t BgL_arg1621z00_3745;
							obj_t BgL_arg1622z00_3746;

							{	/* Match/compiler.scm 360 */
								obj_t BgL_pairz00_3747;

								BgL_pairz00_3747 = CDR(((obj_t) BgL_dz00_3589));
								BgL_arg1621z00_3745 = CDR(BgL_pairz00_3747);
							}
							{	/* Match/compiler.scm 361 */
								obj_t BgL_arg1623z00_3748;
								obj_t BgL_arg1624z00_3749;

								{	/* Match/compiler.scm 361 */
									obj_t BgL_pairz00_3750;

									{	/* Match/compiler.scm 361 */
										obj_t BgL_pairz00_3751;

										BgL_pairz00_3751 = CDR(((obj_t) BgL_dz00_3589));
										BgL_pairz00_3750 = CDR(BgL_pairz00_3751);
									}
									BgL_arg1623z00_3748 = CAR(BgL_pairz00_3750);
								}
								if (INTEGERP(BgL_iz00_3597))
									{	/* Match/compiler.scm 361 */
										BgL_arg1624z00_3749 = ADDFX(BgL_iz00_3597, BINT(1L));
									}
								else
									{	/* Match/compiler.scm 361 */
										BgL_arg1624z00_3749 =
											BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_3597, BINT(1L));
									}
								BgL_arg1622z00_3746 =
									BGl_extendzd2vectorzd2zz__match_descriptionsz00
									(BgL_arg1623z00_3748, BgL_arg1624z00_3749,
									BGl_list2146z00zz__match_compilerz00);
							}
							{	/* Match/compiler.scm 360 */
								obj_t BgL_tmpz00_4968;

								BgL_tmpz00_4968 = ((obj_t) BgL_arg1621z00_3745);
								SET_CAR(BgL_tmpz00_4968, BgL_arg1622z00_3746);
							}
						}
					else
						{	/* Match/compiler.scm 359 */
							BTRUE;
						}
				}
				{	/* Match/compiler.scm 363 */
					obj_t BgL_arg1626z00_3752;
					obj_t BgL_arg1629z00_3753;

					{	/* Match/compiler.scm 363 */
						obj_t BgL_arg1630z00_3754;

						{	/* Match/compiler.scm 363 */
							obj_t BgL_arg1631z00_3755;

							BgL_arg1631z00_3755 = MAKE_YOUNG_PAIR(BgL_iz00_3597, BNIL);
							BgL_arg1630z00_3754 =
								MAKE_YOUNG_PAIR(BgL_ez00_3590, BgL_arg1631z00_3755);
						}
						BgL_arg1626z00_3752 =
							MAKE_YOUNG_PAIR(BGl_symbol2238z00zz__match_compilerz00,
							BgL_arg1630z00_3754);
					}
					{	/* Match/compiler.scm 369 */
						obj_t BgL_arg1640z00_3756;

						{	/* Match/compiler.scm 369 */
							obj_t BgL_pairz00_3757;

							{	/* Match/compiler.scm 369 */
								obj_t BgL_pairz00_3758;

								BgL_pairz00_3758 = CDR(((obj_t) BgL_dz00_3589));
								BgL_pairz00_3757 = CDR(BgL_pairz00_3758);
							}
							BgL_arg1640z00_3756 = CAR(BgL_pairz00_3757);
						}
						{	/* Match/compiler.scm 369 */
							long BgL_kz00_3759;

							BgL_kz00_3759 = (long) CINT(BgL_iz00_3597);
							BgL_arg1629z00_3753 =
								VECTOR_REF(((obj_t) BgL_arg1640z00_3756), BgL_kz00_3759);
					}}
					{	/* Match/compiler.scm 365 */
						obj_t BgL_zc3z04anonymousza31632ze3z87_3760;
						obj_t BgL_zc3z04anonymousza31638ze3z87_3761;

						BgL_zc3z04anonymousza31632ze3z87_3760 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31632ze3ze5zz__match_compilerz00,
							(int) (3L), (int) (7L));
						BgL_zc3z04anonymousza31638ze3z87_3761 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31638ze3ze5zz__match_compilerz00,
							(int) (1L), (int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (0L),
							BgL_dz00_3589);
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (1L),
							BgL_iz00_3597);
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (2L),
							BgL_f2z00_3591);
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (3L),
							BgL_ez00_3590);
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (4L),
							BgL_mz00_3592);
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (5L),
							BgL_kz00_3593);
						PROCEDURE_SET(BgL_zc3z04anonymousza31632ze3z87_3760, (int) (6L),
							BgL_za7za7_3594);
						PROCEDURE_SET(BgL_zc3z04anonymousza31638ze3z87_3761, (int) (0L),
							BgL_dz00_3589);
						PROCEDURE_SET(BgL_zc3z04anonymousza31638ze3z87_3761, (int) (1L),
							BgL_iz00_3597);
						PROCEDURE_SET(BgL_zc3z04anonymousza31638ze3z87_3761, (int) (2L),
							BgL_za7za7_3594);
						return BGl_compilez00zz__match_compilerz00(BgL_f1z00_3595,
							BgL_arg1626z00_3752, BgL_rz00_3596, BgL_mz00_3592,
							BgL_zc3z04anonymousza31632ze3z87_3760,
							BgL_zc3z04anonymousza31638ze3z87_3761, BgL_arg1629z00_3753);
					}
				}
			}
		}

	}



/* &<@anonymous:1638> */
	obj_t BGl_z62zc3z04anonymousza31638ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3598, obj_t BgL_d1z00_3602)
	{
		{	/* Match/compiler.scm 367 */
			{	/* Match/compiler.scm 368 */
				obj_t BgL_dz00_3599;
				obj_t BgL_iz00_3600;
				obj_t BgL_za7za7_3601;

				BgL_dz00_3599 = PROCEDURE_REF(BgL_envz00_3598, (int) (0L));
				BgL_iz00_3600 = PROCEDURE_REF(BgL_envz00_3598, (int) (1L));
				BgL_za7za7_3601 = PROCEDURE_REF(BgL_envz00_3598, (int) (2L));
				{	/* Match/compiler.scm 368 */
					obj_t BgL_arg1639z00_3762;

					BgL_arg1639z00_3762 =
						BGl_vectorzd2pluszd2zz__match_descriptionsz00(BgL_dz00_3599,
						BgL_iz00_3600, BgL_d1z00_3602);
					return BGL_PROCEDURE_CALL1(BgL_za7za7_3601, BgL_arg1639z00_3762);
				}
			}
		}

	}



/* &<@anonymous:1632> */
	obj_t BGl_z62zc3z04anonymousza31632ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3603, obj_t BgL_rz00_3611, obj_t BgL_za71za7_3612,
		obj_t BgL_d1z00_3613)
	{
		{	/* Match/compiler.scm 364 */
			{	/* Match/compiler.scm 365 */
				obj_t BgL_dz00_3604;
				obj_t BgL_iz00_3605;
				obj_t BgL_f2z00_3606;
				obj_t BgL_ez00_3607;
				obj_t BgL_mz00_3608;
				obj_t BgL_kz00_3609;
				obj_t BgL_za7za7_3610;

				BgL_dz00_3604 = PROCEDURE_REF(BgL_envz00_3603, (int) (0L));
				BgL_iz00_3605 = PROCEDURE_REF(BgL_envz00_3603, (int) (1L));
				BgL_f2z00_3606 = PROCEDURE_REF(BgL_envz00_3603, (int) (2L));
				BgL_ez00_3607 = PROCEDURE_REF(BgL_envz00_3603, (int) (3L));
				BgL_mz00_3608 = PROCEDURE_REF(BgL_envz00_3603, (int) (4L));
				BgL_kz00_3609 = PROCEDURE_REF(BgL_envz00_3603, (int) (5L));
				BgL_za7za7_3610 = PROCEDURE_REF(BgL_envz00_3603, (int) (6L));
				{	/* Match/compiler.scm 365 */
					obj_t BgL_fun1637z00_3763;

					BgL_fun1637z00_3763 =
						BGl_compilez00zz__match_compilerz00(BgL_f2z00_3606, BgL_ez00_3607,
						BgL_rz00_3611, BgL_mz00_3608, BgL_kz00_3609, BgL_za7za7_3610,
						BGl_vectorzd2pluszd2zz__match_descriptionsz00(BgL_dz00_3604,
							BgL_iz00_3605, BgL_d1z00_3613));
					{	/* Match/compiler.scm 366 */
						obj_t BgL_arg1636z00_3764;

						if (INTEGERP(BgL_iz00_3605))
							{	/* Match/compiler.scm 366 */
								BgL_arg1636z00_3764 = ADDFX(BgL_iz00_3605, BINT(1L));
							}
						else
							{	/* Match/compiler.scm 366 */
								BgL_arg1636z00_3764 =
									BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_3605, BINT(1L));
							}
						return
							BGL_PROCEDURE_CALL1(BgL_fun1637z00_3763, BgL_arg1636z00_3764);
					}
				}
			}
		}

	}



/* compile-struct-pat */
	obj_t BGl_compilezd2structzd2patz00zz__match_compilerz00(obj_t BgL_fz00_155,
		obj_t BgL_ez00_156, obj_t BgL_rz00_157, obj_t BgL_mz00_158,
		obj_t BgL_kz00_159, obj_t BgL_za7za7_160, obj_t BgL_dz00_161)
	{
		{	/* Match/compiler.scm 387 */
			{	/* Match/compiler.scm 388 */
				obj_t BgL_predz00_1867;

				{	/* Match/compiler.scm 389 */
					obj_t BgL_pairz00_2995;

					{	/* Match/compiler.scm 389 */
						obj_t BgL_pairz00_2994;

						BgL_pairz00_2994 = CDR(((obj_t) BgL_fz00_155));
						BgL_pairz00_2995 = CDR(BgL_pairz00_2994);
					}
					BgL_predz00_1867 = CAR(BgL_pairz00_2995);
				}
				{	/* Match/compiler.scm 389 */
					obj_t BgL_pza2za2_1868;

					{	/* Match/compiler.scm 390 */
						obj_t BgL_pairz00_3001;

						{	/* Match/compiler.scm 390 */
							obj_t BgL_pairz00_3000;

							BgL_pairz00_3000 = CDR(((obj_t) BgL_fz00_155));
							BgL_pairz00_3001 = CDR(BgL_pairz00_3000);
						}
						BgL_pza2za2_1868 = CDR(BgL_pairz00_3001);
					}
					{	/* Match/compiler.scm 390 */
						obj_t BgL_za2kza2z00_1869;

						BgL_za2kza2z00_1869 =
							BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
							BGl_string2215z00zz__match_compilerz00);
						{	/* Match/compiler.scm 391 */
							obj_t BgL_za2varsza2z00_1870;

							BgL_za2varsza2z00_1870 =
								BGl_patternzd2variableszd2zz__match_descriptionsz00
								(BgL_fz00_155);
							{	/* Match/compiler.scm 392 */
								obj_t BgL_za2callza2z00_1871;

								BgL_za2callza2z00_1871 =
									MAKE_YOUNG_PAIR(BgL_za2kza2z00_1869,
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_za2varsza2z00_1870, BNIL));
								{	/* Match/compiler.scm 393 */
									obj_t BgL_successzd2formzd2_1872;

									{	/* Match/compiler.scm 394 */
										obj_t BgL_arg1663z00_1892;

										BgL_arg1663z00_1892 =
											BGl_extendza2za2zz__match_compilerz00(BgL_rz00_157,
											BgL_za2varsza2z00_1870);
										BgL_successzd2formzd2_1872 =
											BGL_PROCEDURE_CALL3(BgL_kz00_159, BgL_arg1663z00_1892,
											BgL_za7za7_160, BgL_dz00_161);
									}
									{	/* Match/compiler.scm 394 */
										obj_t BgL_failurezd2formzd2_1873;

										BgL_failurezd2formzd2_1873 =
											BGL_PROCEDURE_CALL1(BgL_za7za7_160, BgL_dz00_161);
										{	/* Match/compiler.scm 395 */
											obj_t BgL_indexesz00_1874;

											{	/* Match/compiler.scm 396 */
												long BgL_arg1661z00_1888;

												{	/* Match/compiler.scm 396 */

													BgL_arg1661z00_1888 =
														(bgl_list_length(BgL_pza2za2_1868) - 1L);
												}
												BgL_indexesz00_1874 =
													BGl_integersz00zz__match_compilerz00(0L,
													BgL_arg1661z00_1888);
											}
											{	/* Match/compiler.scm 396 */

												{	/* Match/compiler.scm 397 */
													obj_t BgL_arg1652z00_1875;
													obj_t BgL_arg1653z00_1876;

													{	/* Match/compiler.scm 397 */
														obj_t BgL_arg1654z00_1877;

														BgL_arg1654z00_1877 =
															MAKE_YOUNG_PAIR(BgL_ez00_156, BNIL);
														BgL_arg1652z00_1875 =
															MAKE_YOUNG_PAIR(BgL_predz00_1867,
															BgL_arg1654z00_1877);
													}
													{	/* Match/compiler.scm 400 */
														obj_t BgL_zc3z04anonymousza31659ze3z87_3614;

														BgL_zc3z04anonymousza31659ze3z87_3614 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31659ze3ze5zz__match_compilerz00,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31659ze3z87_3614,
															(int) (0L), BgL_failurezd2formzd2_1873);
														BgL_arg1653z00_1876 =
															BGl_z62compileza2zc0zz__match_compilerz00
															(BgL_successzd2formzd2_1872,
															BgL_zc3z04anonymousza31659ze3z87_3614,
															BgL_mz00_158, BgL_ez00_156,
															BgL_failurezd2formzd2_1873, BgL_pza2za2_1868,
															BgL_indexesz00_1874, BgL_rz00_157,
															BGl_list2146z00zz__match_compilerz00);
													}
													return
														BGl_buildzd2ifzd2zz__match_compilerz00
														(BgL_arg1652z00_1875, BgL_arg1653z00_1876,
														BgL_failurezd2formzd2_1873);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &compile* */
	obj_t BGl_z62compileza2zc0zz__match_compilerz00(obj_t
		BgL_successzd2formzd2_3621, obj_t BgL_zc3z04anonymousza31659ze3z87_3620,
		obj_t BgL_mz00_3619, obj_t BgL_ez00_3618, obj_t BgL_failurezd2formzd2_3617,
		obj_t BgL_pza2za2_3005, obj_t BgL_iza2za2_3006, obj_t BgL_rz00_3007,
		obj_t BgL_dz00_3008)
	{
		{	/* Match/compiler.scm 405 */
			if (NULLP(BgL_pza2za2_3005))
				{	/* Match/compiler.scm 406 */
					return BgL_successzd2formzd2_3621;
				}
			else
				{	/* Match/compiler.scm 408 */
					obj_t BgL_arg1667z00_3010;
					obj_t BgL_arg1668z00_3011;

					BgL_arg1667z00_3010 = CAR(((obj_t) BgL_pza2za2_3005));
					{	/* Match/compiler.scm 408 */
						obj_t BgL_arg1670z00_3012;

						{	/* Match/compiler.scm 408 */
							obj_t BgL_arg1675z00_3013;

							{	/* Match/compiler.scm 408 */
								obj_t BgL_arg1676z00_3014;

								BgL_arg1676z00_3014 = CAR(((obj_t) BgL_iza2za2_3006));
								BgL_arg1675z00_3013 =
									MAKE_YOUNG_PAIR(BgL_arg1676z00_3014, BNIL);
							}
							BgL_arg1670z00_3012 =
								MAKE_YOUNG_PAIR(BgL_ez00_3618, BgL_arg1675z00_3013);
						}
						BgL_arg1668z00_3011 =
							MAKE_YOUNG_PAIR(BGl_symbol2240z00zz__match_compilerz00,
							BgL_arg1670z00_3012);
					}
					{	/* Match/compiler.scm 410 */
						obj_t BgL_zc3z04anonymousza31677ze3z87_3615;

						BgL_zc3z04anonymousza31677ze3z87_3615 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31677ze3ze5zz__match_compilerz00,
							(int) (3L), (int) (7L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (0L),
							BgL_failurezd2formzd2_3617);
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (1L),
							BgL_ez00_3618);
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (2L),
							BgL_mz00_3619);
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (3L),
							BgL_zc3z04anonymousza31659ze3z87_3620);
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (4L),
							BgL_successzd2formzd2_3621);
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (5L),
							BgL_pza2za2_3005);
						PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3615, (int) (6L),
							BgL_iza2za2_3006);
						return BGl_compilez00zz__match_compilerz00(BgL_arg1667z00_3010,
							BgL_arg1668z00_3011, BgL_rz00_3007, BgL_mz00_3619,
							BgL_zc3z04anonymousza31677ze3z87_3615,
							BgL_zc3z04anonymousza31659ze3z87_3620,
							BGl_list2146z00zz__match_compilerz00);
					}
				}
		}

	}



/* &<@anonymous:1659> */
	obj_t BGl_z62zc3z04anonymousza31659ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3622, obj_t BgL_dz00_3624)
	{
		{	/* Match/compiler.scm 401 */
			return PROCEDURE_REF(BgL_envz00_3622, (int) (0L));
		}

	}



/* &<@anonymous:1677> */
	obj_t BGl_z62zc3z04anonymousza31677ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3625, obj_t BgL_rrz00_3633, obj_t BgL_za7za7z00_3634,
		obj_t BgL_ddz00_3635)
	{
		{	/* Match/compiler.scm 409 */
			{	/* Match/compiler.scm 410 */
				obj_t BgL_failurezd2formzd2_3626;
				obj_t BgL_ez00_3627;
				obj_t BgL_mz00_3628;
				obj_t BgL_zc3z04anonymousza31659ze3z87_3629;
				obj_t BgL_successzd2formzd2_3630;
				obj_t BgL_pza2za2_3631;
				obj_t BgL_iza2za2_3632;

				BgL_failurezd2formzd2_3626 = PROCEDURE_REF(BgL_envz00_3625, (int) (0L));
				BgL_ez00_3627 = PROCEDURE_REF(BgL_envz00_3625, (int) (1L));
				BgL_mz00_3628 = PROCEDURE_REF(BgL_envz00_3625, (int) (2L));
				BgL_zc3z04anonymousza31659ze3z87_3629 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3625, (int) (3L)));
				BgL_successzd2formzd2_3630 = PROCEDURE_REF(BgL_envz00_3625, (int) (4L));
				BgL_pza2za2_3631 = PROCEDURE_REF(BgL_envz00_3625, (int) (5L));
				BgL_iza2za2_3632 = PROCEDURE_REF(BgL_envz00_3625, (int) (6L));
				{	/* Match/compiler.scm 410 */
					obj_t BgL_arg1678z00_3765;
					obj_t BgL_arg1681z00_3766;

					BgL_arg1678z00_3765 = CDR(((obj_t) BgL_pza2za2_3631));
					BgL_arg1681z00_3766 = CDR(((obj_t) BgL_iza2za2_3632));
					return
						BGl_z62compileza2zc0zz__match_compilerz00
						(BgL_successzd2formzd2_3630, BgL_zc3z04anonymousza31659ze3z87_3629,
						BgL_mz00_3628, BgL_ez00_3627, BgL_failurezd2formzd2_3626,
						BgL_arg1678z00_3765, BgL_arg1681z00_3766, BgL_rrz00_3633,
						BGl_list2146z00zz__match_compilerz00);
				}
			}
		}

	}



/* &k-init */
	obj_t BGl_z62kzd2initzb0zz__match_compilerz00(obj_t BgL_envz00_3403,
		obj_t BgL_rz00_3404, obj_t BgL_za7za7_3405, obj_t BgL_dz00_3406)
	{
		{	/* Match/compiler.scm 423 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &z-init */
	obj_t BGl_z62za7zd2initz17zz__match_compilerz00(obj_t BgL_envz00_3407,
		obj_t BgL_dz00_3408)
	{
		{	/* Match/compiler.scm 425 */
			return BBOOL(((bool_t) 0));
		}

	}



/* extend* */
	obj_t BGl_extendza2za2zz__match_compilerz00(obj_t BgL_rz00_181,
		obj_t BgL_vza2za2_182)
	{
		{	/* Match/compiler.scm 442 */
			if (NULLP(BgL_vza2za2_182))
				{	/* Match/compiler.scm 443 */
					return BgL_rz00_181;
				}
			else
				{	/* Match/compiler.scm 445 */
					obj_t BgL_arg1701z00_3090;
					obj_t BgL_arg1702z00_3091;

					{	/* Match/compiler.scm 445 */
						obj_t BgL_arg1703z00_3092;

						BgL_arg1703z00_3092 = CDR(((obj_t) BgL_vza2za2_182));
						BgL_arg1701z00_3090 =
							BGl_extendza2za2zz__match_compilerz00(BgL_rz00_181,
							BgL_arg1703z00_3092);
					}
					BgL_arg1702z00_3091 = CAR(((obj_t) BgL_vza2za2_182));
					{	/* Match/compiler.scm 445 */
						obj_t BgL_imz00_3095;

						BgL_imz00_3095 = BGl_symbol2242z00zz__match_compilerz00;
						{	/* Match/compiler.scm 435 */
							obj_t BgL_arg1691z00_3096;
							obj_t BgL_arg1692z00_3097;

							BgL_arg1691z00_3096 =
								MAKE_YOUNG_PAIR(BgL_arg1702z00_3091, BgL_imz00_3095);
							BgL_arg1692z00_3097 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg1701z00_3090, BNIL);
							return MAKE_YOUNG_PAIR(BgL_arg1691z00_3096, BgL_arg1692z00_3097);
						}
					}
				}
		}

	}



/* &m-init */
	obj_t BGl_z62mzd2initzb0zz__match_compilerz00(obj_t BgL_envz00_3401,
		obj_t BgL_nz00_3402)
	{
		{	/* Match/compiler.scm 450 */
			{	/* Match/compiler.scm 451 */
				obj_t BgL_zc3z04anonymousza31704ze3z87_3767;

				BgL_zc3z04anonymousza31704ze3z87_3767 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31704ze3ze5zz__match_compilerz00, (int) (5L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31704ze3z87_3767, (int) (0L),
					BgL_nz00_3402);
				return BgL_zc3z04anonymousza31704ze3z87_3767;
			}
		}

	}



/* &<@anonymous:1704> */
	obj_t BGl_z62zc3z04anonymousza31704ze3ze5zz__match_compilerz00(obj_t
		BgL_envz00_3637, obj_t BgL_ez00_3639, obj_t BgL_rz00_3640,
		obj_t BgL_kz00_3641, obj_t BgL_za7za7_3642, obj_t BgL_cz00_3643)
	{
		{	/* Match/compiler.scm 451 */
			{	/* Match/compiler.scm 452 */
				obj_t BgL_nz00_3638;

				BgL_nz00_3638 = PROCEDURE_REF(BgL_envz00_3637, (int) (0L));
				{	/* Match/compiler.scm 452 */
					obj_t BgL_list1705z00_3768;

					{	/* Match/compiler.scm 452 */
						obj_t BgL_arg1706z00_3769;

						BgL_arg1706z00_3769 = MAKE_YOUNG_PAIR(BgL_nz00_3638, BNIL);
						BgL_list1705z00_3768 =
							MAKE_YOUNG_PAIR(BGl_string2244z00zz__match_compilerz00,
							BgL_arg1706z00_3769);
					}
					return
						BGl_errorz00zz__errorz00(BGl_string2177z00zz__match_compilerz00,
						BgL_list1705z00_3768, BGl_string2178z00zz__match_compilerz00);
				}
			}
		}

	}



/* build-if */
	obj_t BGl_buildzd2ifzd2zz__match_compilerz00(obj_t BgL_tstz00_191,
		obj_t BgL_thenz00_192, obj_t BgL_elsez00_193)
	{
		{	/* Match/compiler.scm 470 */
			if ((BgL_tstz00_191 == BTRUE))
				{	/* Match/compiler.scm 472 */
					return BgL_thenz00_192;
				}
			else
				{	/* Match/compiler.scm 472 */
					if ((BgL_tstz00_191 == BFALSE))
						{	/* Match/compiler.scm 473 */
							return BgL_elsez00_193;
						}
					else
						{	/* Match/compiler.scm 474 */
							bool_t BgL_test2384z00_5158;

							if ((BgL_thenz00_192 == BTRUE))
								{	/* Match/compiler.scm 474 */
									BgL_test2384z00_5158 = (BgL_elsez00_193 == BFALSE);
								}
							else
								{	/* Match/compiler.scm 474 */
									BgL_test2384z00_5158 = ((bool_t) 0);
								}
							if (BgL_test2384z00_5158)
								{	/* Match/compiler.scm 474 */
									return BgL_tstz00_191;
								}
							else
								{	/* Match/compiler.scm 477 */
									bool_t BgL_test2386z00_5162;

									if ((BgL_thenz00_192 == BFALSE))
										{	/* Match/compiler.scm 477 */
											BgL_test2386z00_5162 = (BgL_elsez00_193 == BTRUE);
										}
									else
										{	/* Match/compiler.scm 477 */
											BgL_test2386z00_5162 = ((bool_t) 0);
										}
									if (BgL_test2386z00_5162)
										{	/* Match/compiler.scm 479 */
											obj_t BgL_arg1714z00_1937;

											BgL_arg1714z00_1937 =
												MAKE_YOUNG_PAIR(BgL_tstz00_191, BNIL);
											return
												MAKE_YOUNG_PAIR(BGl_symbol2160z00zz__match_compilerz00,
												BgL_arg1714z00_1937);
										}
									else
										{	/* Match/compiler.scm 480 */
											obj_t BgL_arg1715z00_1938;

											{	/* Match/compiler.scm 480 */
												obj_t BgL_arg1717z00_1939;

												{	/* Match/compiler.scm 480 */
													obj_t BgL_arg1718z00_1940;

													BgL_arg1718z00_1940 =
														MAKE_YOUNG_PAIR(BgL_elsez00_193, BNIL);
													BgL_arg1717z00_1939 =
														MAKE_YOUNG_PAIR(BgL_thenz00_192,
														BgL_arg1718z00_1940);
												}
												BgL_arg1715z00_1938 =
													MAKE_YOUNG_PAIR(BgL_tstz00_191, BgL_arg1717z00_1939);
											}
											return
												MAKE_YOUNG_PAIR(BGl_symbol2154z00zz__match_compilerz00,
												BgL_arg1715z00_1938);
										}
								}
						}
				}
		}

	}



/* build-let */
	obj_t BGl_buildzd2letzd2zz__match_compilerz00(obj_t BgL_za2carza2z00_194,
		obj_t BgL_za2cdrza2z00_195, obj_t BgL_ez00_196, obj_t BgL_bodyz00_197)
	{
		{	/* Match/compiler.scm 482 */
			{	/* Match/compiler.scm 483 */
				bool_t BgL_test2388z00_5172;

				{	/* Match/compiler.scm 483 */
					obj_t BgL_a1104z00_1996;

					BgL_a1104z00_1996 =
						BGl_countzd2occurrenceszd2zz__match_compilerz00
						(BgL_za2carza2z00_194, BgL_bodyz00_197, 0L);
					{	/* Match/compiler.scm 483 */

						if (INTEGERP(BgL_a1104z00_1996))
							{	/* Match/compiler.scm 483 */
								BgL_test2388z00_5172 = ((long) CINT(BgL_a1104z00_1996) > 1L);
							}
						else
							{	/* Match/compiler.scm 483 */
								BgL_test2388z00_5172 =
									BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_a1104z00_1996, BINT(1L));
							}
					}
				}
				if (BgL_test2388z00_5172)
					{	/* Match/compiler.scm 484 */
						bool_t BgL_test2390z00_5180;

						{	/* Match/compiler.scm 484 */
							obj_t BgL_a1106z00_1971;

							BgL_a1106z00_1971 =
								BGl_countzd2occurrenceszd2zz__match_compilerz00
								(BgL_za2cdrza2z00_195, BgL_bodyz00_197, 0L);
							{	/* Match/compiler.scm 484 */

								if (INTEGERP(BgL_a1106z00_1971))
									{	/* Match/compiler.scm 484 */
										BgL_test2390z00_5180 =
											((long) CINT(BgL_a1106z00_1971) > 1L);
									}
								else
									{	/* Match/compiler.scm 484 */
										BgL_test2390z00_5180 =
											BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_a1106z00_1971,
											BINT(1L));
									}
							}
						}
						if (BgL_test2390z00_5180)
							{	/* Match/compiler.scm 485 */
								obj_t BgL_arg1723z00_1949;

								{	/* Match/compiler.scm 485 */
									obj_t BgL_arg1724z00_1950;
									obj_t BgL_arg1725z00_1951;

									{	/* Match/compiler.scm 485 */
										obj_t BgL_arg1726z00_1952;
										obj_t BgL_arg1727z00_1953;

										{	/* Match/compiler.scm 485 */
											obj_t BgL_arg1728z00_1954;

											{	/* Match/compiler.scm 485 */
												obj_t BgL_arg1729z00_1955;

												{	/* Match/compiler.scm 485 */
													obj_t BgL_arg1730z00_1956;

													BgL_arg1730z00_1956 =
														MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
													BgL_arg1729z00_1955 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2245z00zz__match_compilerz00,
														BgL_arg1730z00_1956);
												}
												BgL_arg1728z00_1954 =
													MAKE_YOUNG_PAIR(BgL_arg1729z00_1955, BNIL);
											}
											BgL_arg1726z00_1952 =
												MAKE_YOUNG_PAIR(BgL_za2carza2z00_194,
												BgL_arg1728z00_1954);
										}
										{	/* Match/compiler.scm 485 */
											obj_t BgL_arg1731z00_1957;

											{	/* Match/compiler.scm 485 */
												obj_t BgL_arg1733z00_1958;

												{	/* Match/compiler.scm 485 */
													obj_t BgL_arg1734z00_1959;

													{	/* Match/compiler.scm 485 */
														obj_t BgL_arg1735z00_1960;

														BgL_arg1735z00_1960 =
															MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
														BgL_arg1734z00_1959 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2247z00zz__match_compilerz00,
															BgL_arg1735z00_1960);
													}
													BgL_arg1733z00_1958 =
														MAKE_YOUNG_PAIR(BgL_arg1734z00_1959, BNIL);
												}
												BgL_arg1731z00_1957 =
													MAKE_YOUNG_PAIR(BgL_za2cdrza2z00_195,
													BgL_arg1733z00_1958);
											}
											BgL_arg1727z00_1953 =
												MAKE_YOUNG_PAIR(BgL_arg1731z00_1957, BNIL);
										}
										BgL_arg1724z00_1950 =
											MAKE_YOUNG_PAIR(BgL_arg1726z00_1952, BgL_arg1727z00_1953);
									}
									BgL_arg1725z00_1951 = MAKE_YOUNG_PAIR(BgL_bodyz00_197, BNIL);
									BgL_arg1723z00_1949 =
										MAKE_YOUNG_PAIR(BgL_arg1724z00_1950, BgL_arg1725z00_1951);
								}
								return
									MAKE_YOUNG_PAIR(BGl_symbol2222z00zz__match_compilerz00,
									BgL_arg1723z00_1949);
							}
						else
							{	/* Match/compiler.scm 487 */
								obj_t BgL_arg1736z00_1961;

								{	/* Match/compiler.scm 487 */
									obj_t BgL_arg1737z00_1962;
									obj_t BgL_arg1738z00_1963;

									{	/* Match/compiler.scm 487 */
										obj_t BgL_arg1739z00_1964;

										{	/* Match/compiler.scm 487 */
											obj_t BgL_arg1740z00_1965;

											{	/* Match/compiler.scm 487 */
												obj_t BgL_arg1741z00_1966;

												{	/* Match/compiler.scm 487 */
													obj_t BgL_arg1743z00_1967;

													BgL_arg1743z00_1967 =
														MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
													BgL_arg1741z00_1966 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2245z00zz__match_compilerz00,
														BgL_arg1743z00_1967);
												}
												BgL_arg1740z00_1965 =
													MAKE_YOUNG_PAIR(BgL_arg1741z00_1966, BNIL);
											}
											BgL_arg1739z00_1964 =
												MAKE_YOUNG_PAIR(BgL_za2carza2z00_194,
												BgL_arg1740z00_1965);
										}
										BgL_arg1737z00_1962 =
											MAKE_YOUNG_PAIR(BgL_arg1739z00_1964, BNIL);
									}
									{	/* Match/compiler.scm 488 */
										obj_t BgL_arg1744z00_1968;

										{	/* Match/compiler.scm 488 */
											obj_t BgL_arg1745z00_1969;

											{	/* Match/compiler.scm 488 */
												obj_t BgL_arg1746z00_1970;

												BgL_arg1746z00_1970 =
													MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
												BgL_arg1745z00_1969 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2247z00zz__match_compilerz00,
													BgL_arg1746z00_1970);
											}
											BgL_arg1744z00_1968 =
												BGl_unfoldz00zz__match_compilerz00(BgL_za2cdrza2z00_195,
												BgL_arg1745z00_1969, BgL_bodyz00_197);
										}
										BgL_arg1738z00_1963 =
											MAKE_YOUNG_PAIR(BgL_arg1744z00_1968, BNIL);
									}
									BgL_arg1736z00_1961 =
										MAKE_YOUNG_PAIR(BgL_arg1737z00_1962, BgL_arg1738z00_1963);
								}
								return
									MAKE_YOUNG_PAIR(BGl_symbol2222z00zz__match_compilerz00,
									BgL_arg1736z00_1961);
							}
					}
				else
					{	/* Match/compiler.scm 489 */
						bool_t BgL_test2392z00_5212;

						{	/* Match/compiler.scm 489 */
							obj_t BgL_a1108z00_1993;

							BgL_a1108z00_1993 =
								BGl_countzd2occurrenceszd2zz__match_compilerz00
								(BgL_za2cdrza2z00_195, BgL_bodyz00_197, 0L);
							{	/* Match/compiler.scm 489 */

								if (INTEGERP(BgL_a1108z00_1993))
									{	/* Match/compiler.scm 489 */
										BgL_test2392z00_5212 =
											((long) CINT(BgL_a1108z00_1993) > 1L);
									}
								else
									{	/* Match/compiler.scm 489 */
										BgL_test2392z00_5212 =
											BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_a1108z00_1993,
											BINT(1L));
									}
							}
						}
						if (BgL_test2392z00_5212)
							{	/* Match/compiler.scm 490 */
								obj_t BgL_arg1749z00_1978;

								{	/* Match/compiler.scm 490 */
									obj_t BgL_arg1750z00_1979;
									obj_t BgL_arg1751z00_1980;

									{	/* Match/compiler.scm 490 */
										obj_t BgL_arg1752z00_1981;

										{	/* Match/compiler.scm 490 */
											obj_t BgL_arg1753z00_1982;

											{	/* Match/compiler.scm 490 */
												obj_t BgL_arg1754z00_1983;

												{	/* Match/compiler.scm 490 */
													obj_t BgL_arg1755z00_1984;

													BgL_arg1755z00_1984 =
														MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
													BgL_arg1754z00_1983 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2247z00zz__match_compilerz00,
														BgL_arg1755z00_1984);
												}
												BgL_arg1753z00_1982 =
													MAKE_YOUNG_PAIR(BgL_arg1754z00_1983, BNIL);
											}
											BgL_arg1752z00_1981 =
												MAKE_YOUNG_PAIR(BgL_za2cdrza2z00_195,
												BgL_arg1753z00_1982);
										}
										BgL_arg1750z00_1979 =
											MAKE_YOUNG_PAIR(BgL_arg1752z00_1981, BNIL);
									}
									{	/* Match/compiler.scm 491 */
										obj_t BgL_arg1756z00_1985;

										{	/* Match/compiler.scm 491 */
											obj_t BgL_arg1757z00_1986;

											{	/* Match/compiler.scm 491 */
												obj_t BgL_arg1758z00_1987;

												BgL_arg1758z00_1987 =
													MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
												BgL_arg1757z00_1986 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2245z00zz__match_compilerz00,
													BgL_arg1758z00_1987);
											}
											BgL_arg1756z00_1985 =
												BGl_unfoldz00zz__match_compilerz00(BgL_za2carza2z00_194,
												BgL_arg1757z00_1986, BgL_bodyz00_197);
										}
										BgL_arg1751z00_1980 =
											MAKE_YOUNG_PAIR(BgL_arg1756z00_1985, BNIL);
									}
									BgL_arg1749z00_1978 =
										MAKE_YOUNG_PAIR(BgL_arg1750z00_1979, BgL_arg1751z00_1980);
								}
								return
									MAKE_YOUNG_PAIR(BGl_symbol2222z00zz__match_compilerz00,
									BgL_arg1749z00_1978);
							}
						else
							{	/* Match/compiler.scm 492 */
								obj_t BgL_arg1759z00_1988;
								obj_t BgL_arg1760z00_1989;

								{	/* Match/compiler.scm 492 */
									obj_t BgL_arg1761z00_1990;

									BgL_arg1761z00_1990 = MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
									BgL_arg1759z00_1988 =
										MAKE_YOUNG_PAIR(BGl_symbol2247z00zz__match_compilerz00,
										BgL_arg1761z00_1990);
								}
								{	/* Match/compiler.scm 493 */
									obj_t BgL_arg1762z00_1991;

									{	/* Match/compiler.scm 493 */
										obj_t BgL_arg1763z00_1992;

										BgL_arg1763z00_1992 = MAKE_YOUNG_PAIR(BgL_ez00_196, BNIL);
										BgL_arg1762z00_1991 =
											MAKE_YOUNG_PAIR(BGl_symbol2245z00zz__match_compilerz00,
											BgL_arg1763z00_1992);
									}
									BgL_arg1760z00_1989 =
										BGl_unfoldz00zz__match_compilerz00(BgL_za2carza2z00_194,
										BgL_arg1762z00_1991, BgL_bodyz00_197);
								}
								return
									BGl_unfoldz00zz__match_compilerz00(BgL_za2cdrza2z00_195,
									BgL_arg1759z00_1988, BgL_arg1760z00_1989);
							}
					}
			}
		}

	}



/* unfold */
	obj_t BGl_unfoldz00zz__match_compilerz00(obj_t BgL_sz00_198,
		obj_t BgL_vz00_199, obj_t BgL_ez00_200)
	{
		{	/* Match/compiler.scm 498 */
			if (NULLP(BgL_ez00_200))
				{	/* Match/compiler.scm 499 */
					return BNIL;
				}
			else
				{	/* Match/compiler.scm 499 */
					if (CBOOL(BGl_atomzf3zf3zz__match_s2cfunz00(BgL_ez00_200)))
						{	/* Match/compiler.scm 501 */
							if ((BgL_ez00_200 == BgL_sz00_198))
								{	/* Match/compiler.scm 502 */
									return BgL_vz00_199;
								}
							else
								{	/* Match/compiler.scm 502 */
									return BgL_ez00_200;
								}
						}
					else
						{	/* Match/compiler.scm 501 */
							if (PAIRP(BgL_ez00_200))
								{	/* Match/compiler.scm 503 */
									if (
										(CAR(BgL_ez00_200) ==
											BGl_symbol2156z00zz__match_compilerz00))
										{	/* Match/compiler.scm 504 */
											return BgL_ez00_200;
										}
									else
										{	/* Match/compiler.scm 506 */
											obj_t BgL_fz00_2004;
											obj_t BgL_argsz00_2005;

											BgL_fz00_2004 = CAR(BgL_ez00_200);
											BgL_argsz00_2005 = CDR(BgL_ez00_200);
											return
												MAKE_YOUNG_PAIR(BGl_unfoldz00zz__match_compilerz00
												(BgL_sz00_198, BgL_vz00_199, BgL_fz00_2004),
												BGl_unfoldz00zz__match_compilerz00(BgL_sz00_198,
													BgL_vz00_199, BgL_argsz00_2005));
										}
								}
							else
								{	/* Match/compiler.scm 503 */
									return BFALSE;
								}
						}
				}
		}

	}



/* integers */
	obj_t BGl_integersz00zz__match_compilerz00(long BgL_fromz00_201,
		long BgL_toz00_202)
	{
		{	/* Match/compiler.scm 509 */
			if ((BgL_fromz00_201 > BgL_toz00_202))
				{	/* Match/compiler.scm 510 */
					return BNIL;
				}
			else
				{	/* Match/compiler.scm 510 */
					return
						MAKE_YOUNG_PAIR(BINT(BgL_fromz00_201),
						BGl_integersz00zz__match_compilerz00(
							(BgL_fromz00_201 + 1L), BgL_toz00_202));
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__match_compilerz00(void)
	{
		{	/* Match/compiler.scm 26 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2249z00zz__match_compilerz00));
			BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(509060701L,
				BSTRING_TO_STRING(BGl_string2249z00zz__match_compilerz00));
			BGl_modulezd2initializa7ationz75zz__match_descriptionsz00(232414635L,
				BSTRING_TO_STRING(BGl_string2249z00zz__match_compilerz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2249z00zz__match_compilerz00));
		}

	}

#ifdef __cplusplus
}
#endif
