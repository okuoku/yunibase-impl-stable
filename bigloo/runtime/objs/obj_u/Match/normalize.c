/*===========================================================================*/
/*   (Match/normalize.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Match/normalize.scm -indent -o objs/obj_u/Match/normalize.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MATCH_NORMALIZE_TYPE_DEFINITIONS
#define BGL___MATCH_NORMALIZE_TYPE_DEFINITIONS
#endif													// BGL___MATCH_NORMALIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7 =
		BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__match_normaliza7eza7 =
		BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31677ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static bool_t BGl_segmentzd2variablezf3z21zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_standardiza7ezd2treezd2variableza7zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static bool_t BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__match_normaliza7eza7(void);
	static obj_t BGl_makezd2togglezd2zz__match_normaliza7eza7(void);
	static long BGl_patternzd2lengthzd2zz__match_normaliza7eza7(obj_t);
	extern obj_t BGl_jimzd2gensymzd2zz__match_s2cfunz00;
	static obj_t BGl_standardiza7ezd2structz75zz__match_normaliza7eza7(obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__match_normaliza7eza7(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62extendzd2rzd2macrozd2envzb0zz__match_normaliza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__match_normaliza7eza7(void);
	static obj_t
		BGl_z62zc3z04anonymousza31572ze32214ze5zz__match_normaliza7eza7(obj_t,
		obj_t, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31313ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t BGl_list2305z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_list2306z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_standardiza7ezd2repetitionz75zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31500ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31403ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_standardiza7ezd2segmentzd2variableza7zz__match_normaliza7eza7(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static bool_t BGl_treezd2variablezf3z21zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_list2311z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_list2312z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_list2315z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_list2316z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_vectorifyze70ze7zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_list2319z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31250ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31404ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31323ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31315ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_standardiza7ezd2consz75zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__match_normaliza7eza7(void);
	static obj_t BGl_z62normaliza7ezd2patternz17zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t BGl_list2320z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31332ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31503ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31252ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static bool_t BGl_termzd2variablezf3z21zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31342ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31334ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 = BUNSPEC;
	extern obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7(obj_t);
	extern obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31351ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31270ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static bool_t
		BGl_lispishzd2segmentzd2variablezf3zf3zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31319ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31506ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62matchzd2definezd2recordzd2typez12za2zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31531ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31523ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31353ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2298z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31613ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31362ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31338ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31541ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t
		BGl_z62zc3z04anonymousza31380ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31623ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31372ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_extendzd2rzd2macrozd2envzd2zz__match_normaliza7eza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_matchzd2definezd2recordzd2typez12zc0zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31551ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31462ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31535ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_standardiza7ezd2vectorz75zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_symbol2217z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_unboundzd2patternzd2zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31722ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31358ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_standardiza7ezd2lispishzd2anyza7zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__match_normaliza7eza7(void);
	static obj_t BGl_symbol2220z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_rzd2macrozd2patternzd2initzd2zz__match_normaliza7eza7 =
		BUNSPEC;
	static obj_t BGl_symbol2303z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2223z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2307z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2226z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2309z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2229z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_oczd2countzd2zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__match_normaliza7eza7(void);
	static obj_t BGl_gczd2rootszd2initz00zz__match_normaliza7eza7(void);
	static obj_t
		BGl_z62zc3z04anonymousza31626ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31375ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static bool_t BGl_holezd2variablezf3z21zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_symbol2313z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2232z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_standardiza7ezd2holezd2variableza7zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_symbol2235z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2317z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2237z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_standardiza7ezd2anyz75zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31562ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31392ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31554ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31384ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31538ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31619ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31368ze3ze5zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t BGl_symbol2321z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2240z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2324z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2243z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2326z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2246z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2329z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2249z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31547ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_standardiza7ezd2realzd2xconsza7zz__match_normaliza7eza7(obj_t, obj_t);
	static obj_t BGl_symbol2258z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_standardiza7ezd2realzd2consza7zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31572ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31734ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31462ze32213ze5zz__match_normaliza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31462ze32215ze5zz__match_normaliza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2260z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2263z00zz__match_normaliza7eza7 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_symbol2265z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2267z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7(obj_t);
	static obj_t BGl_symbol2271z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2273z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2276z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2278z00zz__match_normaliza7eza7 = BUNSPEC;
	extern obj_t BGl_atomzf3zf3zz__match_s2cfunz00(obj_t);
	static obj_t BGl_za2preferzd2xconsza2zd2zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31396ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31558ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2280z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2282z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2284z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2286z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2288z00zz__match_normaliza7eza7 = BUNSPEC;
	extern obj_t BGl_structzd2ze3listz31zz__structurez00(obj_t);
	static obj_t
		BGl_standardiza7ezd2lispishzd2segmentzd2variablez75zz__match_normaliza7eza7
		(obj_t, obj_t);
	static obj_t BGl_symbol2290z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2292z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2294z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2296z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2299z00zz__match_normaliza7eza7 = BUNSPEC;
	static obj_t
		BGl_standardiza7ezd2termzd2variableza7zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31681ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31584ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31576ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62matchzd2definezd2structurez12z70zz__match_normaliza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31836ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31658ze3ze5zz__match_normaliza7eza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_normaliza7ezd2patternz75zz__match_normaliza7eza7(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2218z00zz__match_normaliza7eza7,
		BgL_bgl_string2218za700za7za7_2333za7, "atom", 4);
	      DEFINE_STRING(BGl_string2300z00zz__match_normaliza7eza7,
		BgL_bgl_string2300za700za7za7_2334za7, "tagged-or", 9);
	      DEFINE_STRING(BGl_string2221z00zz__match_normaliza7eza7,
		BgL_bgl_string2221za700za7za7_2335za7, "or", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2216z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2336za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31250ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2304z00zz__match_normaliza7eza7,
		BgL_bgl_string2304za700za7za7_2337za7, "vector-begin", 12);
	      DEFINE_STRING(BGl_string2224z00zz__match_normaliza7eza7,
		BgL_bgl_string2224za700za7za7_2338za7, "t-or", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2219z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2339za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31313ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2308z00zz__match_normaliza7eza7,
		BgL_bgl_string2308za700za7za7_2340za7, "vector-any", 10);
	      DEFINE_STRING(BGl_string2227z00zz__match_normaliza7eza7,
		BgL_bgl_string2227za700za7za7_2341za7, "and", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extendzd2rzd2macrozd2envzd2envz00zz__match_normaliza7eza7,
		BgL_bgl_za762extendza7d2rza7d22342za7,
		BGl_z62extendzd2rzd2macrozd2envzb0zz__match_normaliza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2343za7,
		BGl_z62zc3z04anonymousza31722ze3ze5zz__match_normaliza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2344za7,
		BGl_z62zc3z04anonymousza31462ze32215ze5zz__match_normaliza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2222z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2345za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31332ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2310z00zz__match_normaliza7eza7,
		BgL_bgl_string2310za700za7za7_2346za7, "vector-cons", 11);
	      DEFINE_STRING(BGl_string2230z00zz__match_normaliza7eza7,
		BgL_bgl_string2230za700za7za7_2347za7, "not", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2225z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2348za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31351ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2314z00zz__match_normaliza7eza7,
		BgL_bgl_string2314za700za7za7_2349za7, "vector-end", 10);
	      DEFINE_STRING(BGl_string2233z00zz__match_normaliza7eza7,
		BgL_bgl_string2233za700za7za7_2350za7, "?", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2228z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2351za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31368ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2236z00zz__match_normaliza7eza7,
		BgL_bgl_string2236za700za7za7_2352za7, "kwote", 5);
	      DEFINE_STRING(BGl_string2318z00zz__match_normaliza7eza7,
		BgL_bgl_string2318za700za7za7_2353za7, "vector-times", 12);
	      DEFINE_STRING(BGl_string2238z00zz__match_normaliza7eza7,
		BgL_bgl_string2238za700za7za7_2354za7, "**Bad-Luck096561123523452**", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2231z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2355za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31380ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2234z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2356za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31392ze3ze5zz__match_normaliza7eza7, BUNSPEC, -1);
	      DEFINE_STRING(BGl_string2322z00zz__match_normaliza7eza7,
		BgL_bgl_string2322za700za7za7_2357za7, "define-struct", 13);
	      DEFINE_STRING(BGl_string2241z00zz__match_normaliza7eza7,
		BgL_bgl_string2241za700za7za7_2358za7, "struct-pat", 10);
	      DEFINE_STRING(BGl_string2323z00zz__match_normaliza7eza7,
		BgL_bgl_string2323za700za7za7_2359za7, "Incorrect declaration: ", 23);
	      DEFINE_STRING(BGl_string2325z00zz__match_normaliza7eza7,
		BgL_bgl_string2325za700za7za7_2360za7, "Aborted", 7);
	      DEFINE_STRING(BGl_string2244z00zz__match_normaliza7eza7,
		BgL_bgl_string2244za700za7za7_2361za7, "Pattern-Matching", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2239z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2362za7, va_generic_entry,
		BGl_z62zc3z04anonymousza31403ze3ze5zz__match_normaliza7eza7, BUNSPEC, -3);
	      DEFINE_STRING(BGl_string2245z00zz__match_normaliza7eza7,
		BgL_bgl_string2245za700za7za7_2363za7, "Illegal `kwote' form", 20);
	      DEFINE_STRING(BGl_string2327z00zz__match_normaliza7eza7,
		BgL_bgl_string2327za700za7za7_2364za7, "define-record-type", 18);
	      DEFINE_STRING(BGl_string2328z00zz__match_normaliza7eza7,
		BgL_bgl_string2328za700za7za7_2365za7, "No such structure: ", 19);
	      DEFINE_STRING(BGl_string2247z00zz__match_normaliza7eza7,
		BgL_bgl_string2247za700za7za7_2366za7, "quote", 5);
	      DEFINE_STRING(BGl_string2248z00zz__match_normaliza7eza7,
		BgL_bgl_string2248za700za7za7_2367za7, "Illegal `?' form", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2242z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2368za7,
		BGl_z62zc3z04anonymousza31462ze3ze5zz__match_normaliza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2330z00zz__match_normaliza7eza7,
		BgL_bgl_string2330za700za7za7_2369za7, "match-case", 10);
	      DEFINE_STRING(BGl_string2331z00zz__match_normaliza7eza7,
		BgL_bgl_string2331za700za7za7_2370za7, "No such structure ", 18);
	      DEFINE_STRING(BGl_string2250z00zz__match_normaliza7eza7,
		BgL_bgl_string2250za700za7za7_2371za7, "check", 5);
	      DEFINE_STRING(BGl_string2332z00zz__match_normaliza7eza7,
		BgL_bgl_string2332za700za7za7_2372za7, "__match_normalize", 17);
	      DEFINE_STRING(BGl_string2251z00zz__match_normaliza7eza7,
		BgL_bgl_string2251za700za7za7_2373za7, "Illegal `not'", 13);
	      DEFINE_STRING(BGl_string2252z00zz__match_normaliza7eza7,
		BgL_bgl_string2252za700za7za7_2374za7, "Illegal `and' form", 18);
	      DEFINE_STRING(BGl_string2253z00zz__match_normaliza7eza7,
		BgL_bgl_string2253za700za7za7_2375za7, "Illegal `t-or' form", 19);
	      DEFINE_STRING(BGl_string2254z00zz__match_normaliza7eza7,
		BgL_bgl_string2254za700za7za7_2376za7, "Incompatible alternative", 24);
	      DEFINE_STRING(BGl_string2255z00zz__match_normaliza7eza7,
		BgL_bgl_string2255za700za7za7_2377za7, "Illegal `or' form", 17);
	      DEFINE_STRING(BGl_string2256z00zz__match_normaliza7eza7,
		BgL_bgl_string2256za700za7za7_2378za7, "Illegal `atom' form", 19);
	      DEFINE_STRING(BGl_string2257z00zz__match_normaliza7eza7,
		BgL_bgl_string2257za700za7za7_2379za7,
		"Too many patterns provided for atom", 35);
	      DEFINE_STRING(BGl_string2259z00zz__match_normaliza7eza7,
		BgL_bgl_string2259za700za7za7_2380za7, "any", 3);
	      DEFINE_STRING(BGl_string2261z00zz__match_normaliza7eza7,
		BgL_bgl_string2261za700za7za7_2381za7, "cons", 4);
	      DEFINE_STRING(BGl_string2264z00zz__match_normaliza7eza7,
		BgL_bgl_string2264za700za7za7_2382za7, "?-", 2);
	      DEFINE_STRING(BGl_string2266z00zz__match_normaliza7eza7,
		BgL_bgl_string2266za700za7za7_2383za7, "\077\077-", 3);
	      DEFINE_STRING(BGl_string2268z00zz__match_normaliza7eza7,
		BgL_bgl_string2268za700za7za7_2384za7, "\077\077?-", 4);
	      DEFINE_STRING(BGl_string2269z00zz__match_normaliza7eza7,
		BgL_bgl_string2269za700za7za7_2385za7, "g", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2262z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2386za7,
		BGl_z62zc3z04anonymousza31462ze32213ze5zz__match_normaliza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2270z00zz__match_normaliza7eza7,
		BgL_bgl_string2270za700za7za7_2387za7, "HOLE-", 5);
	      DEFINE_STRING(BGl_string2272z00zz__match_normaliza7eza7,
		BgL_bgl_string2272za700za7za7_2388za7, "hole", 4);
	      DEFINE_STRING(BGl_string2274z00zz__match_normaliza7eza7,
		BgL_bgl_string2274za700za7za7_2389za7, "times", 5);
	      DEFINE_STRING(BGl_string2277z00zz__match_normaliza7eza7,
		BgL_bgl_string2277za700za7za7_2390za7, "...", 3);
	      DEFINE_STRING(BGl_string2279z00zz__match_normaliza7eza7,
		BgL_bgl_string2279za700za7za7_2391za7, "value", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2275z00zz__match_normaliza7eza7,
		BgL_bgl_za762za7c3za704anonymo2392za7,
		BGl_z62zc3z04anonymousza31523ze3ze5zz__match_normaliza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2281z00zz__match_normaliza7eza7,
		BgL_bgl_string2281za700za7za7_2393za7, "on", 2);
	      DEFINE_STRING(BGl_string2283z00zz__match_normaliza7eza7,
		BgL_bgl_string2283za700za7za7_2394za7, "off", 3);
	      DEFINE_STRING(BGl_string2285z00zz__match_normaliza7eza7,
		BgL_bgl_string2285za700za7za7_2395za7, "xcons", 5);
	      DEFINE_STRING(BGl_string2287z00zz__match_normaliza7eza7,
		BgL_bgl_string2287za700za7za7_2396za7, "var", 3);
	      DEFINE_STRING(BGl_string2289z00zz__match_normaliza7eza7,
		BgL_bgl_string2289za700za7za7_2397za7, "segment", 7);
	      DEFINE_STRING(BGl_string2291z00zz__match_normaliza7eza7,
		BgL_bgl_string2291za700za7za7_2398za7, "end-ssetq", 9);
	      DEFINE_STRING(BGl_string2293z00zz__match_normaliza7eza7,
		BgL_bgl_string2293za700za7za7_2399za7, "tree", 4);
	      DEFINE_STRING(BGl_string2295z00zz__match_normaliza7eza7,
		BgL_bgl_string2295za700za7za7_2400za7, "ssetq-append", 12);
	      DEFINE_STRING(BGl_string2297z00zz__match_normaliza7eza7,
		BgL_bgl_string2297za700za7za7_2401za7, "eval-append", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_normaliza7ezd2patternzd2envza7zz__match_normaliza7eza7,
		BgL_bgl_za762normaliza7a7eza7d2402za7,
		BGl_z62normaliza7ezd2patternz17zz__match_normaliza7eza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_matchzd2definezd2recordzd2typez12zd2envz12zz__match_normaliza7eza7,
		BgL_bgl_za762matchza7d2defin2403z00,
		BGl_z62matchzd2definezd2recordzd2typez12za2zz__match_normaliza7eza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_matchzd2definezd2structurez12zd2envzc0zz__match_normaliza7eza7,
		BgL_bgl_za762matchza7d2defin2404z00,
		BGl_z62matchzd2definezd2structurez12z70zz__match_normaliza7eza7, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7));
		     ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2305z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2306z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2311z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2312z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2315z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2316z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2319z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2320z00zz__match_normaliza7eza7));
		   
			 ADD_ROOT((void *) (&BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_list2298z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2217z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_unboundzd2patternzd2zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2220z00zz__match_normaliza7eza7));
		   
			 ADD_ROOT((void
				*) (&BGl_rzd2macrozd2patternzd2initzd2zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2303z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2223z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2307z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2226z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2309z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2229z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2313z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2232z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2235z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2317z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2237z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2321z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2240z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2324z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2243z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2326z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2246z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2329z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2249z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2258z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2260z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2263z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2265z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2267z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2271z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2273z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2276z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2278z00zz__match_normaliza7eza7));
		   
			 ADD_ROOT((void
				*) (&BGl_za2preferzd2xconsza2zd2zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2280z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2282z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2284z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2286z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2288z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2290z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2292z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2294z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2296z00zz__match_normaliza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2299z00zz__match_normaliza7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long
		BgL_checksumz00_3980, char *BgL_fromz00_3981)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__match_normaliza7eza7))
				{
					BGl_requirezd2initializa7ationz75zz__match_normaliza7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__match_normaliza7eza7();
					BGl_cnstzd2initzd2zz__match_normaliza7eza7();
					BGl_importedzd2moduleszd2initz00zz__match_normaliza7eza7();
					return BGl_toplevelzd2initzd2zz__match_normaliza7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			BGl_symbol2217z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2218z00zz__match_normaliza7eza7);
			BGl_symbol2220z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2221z00zz__match_normaliza7eza7);
			BGl_symbol2223z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2224z00zz__match_normaliza7eza7);
			BGl_symbol2226z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2227z00zz__match_normaliza7eza7);
			BGl_symbol2229z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2230z00zz__match_normaliza7eza7);
			BGl_symbol2232z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2233z00zz__match_normaliza7eza7);
			BGl_symbol2235z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2236z00zz__match_normaliza7eza7);
			BGl_symbol2237z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2238z00zz__match_normaliza7eza7);
			BGl_symbol2240z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2241z00zz__match_normaliza7eza7);
			BGl_symbol2243z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2244z00zz__match_normaliza7eza7);
			BGl_symbol2246z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2247z00zz__match_normaliza7eza7);
			BGl_symbol2249z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2250z00zz__match_normaliza7eza7);
			BGl_symbol2258z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2259z00zz__match_normaliza7eza7);
			BGl_symbol2260z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2261z00zz__match_normaliza7eza7);
			BGl_symbol2263z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2264z00zz__match_normaliza7eza7);
			BGl_symbol2265z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2266z00zz__match_normaliza7eza7);
			BGl_symbol2267z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2268z00zz__match_normaliza7eza7);
			BGl_symbol2271z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2272z00zz__match_normaliza7eza7);
			BGl_symbol2273z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2274z00zz__match_normaliza7eza7);
			BGl_symbol2276z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2277z00zz__match_normaliza7eza7);
			BGl_symbol2278z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2279z00zz__match_normaliza7eza7);
			BGl_symbol2280z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2281z00zz__match_normaliza7eza7);
			BGl_symbol2282z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2283z00zz__match_normaliza7eza7);
			BGl_symbol2284z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2285z00zz__match_normaliza7eza7);
			BGl_symbol2286z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2287z00zz__match_normaliza7eza7);
			BGl_symbol2288z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2289z00zz__match_normaliza7eza7);
			BGl_symbol2290z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2291z00zz__match_normaliza7eza7);
			BGl_symbol2292z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2293z00zz__match_normaliza7eza7);
			BGl_symbol2294z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2295z00zz__match_normaliza7eza7);
			BGl_symbol2296z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2297z00zz__match_normaliza7eza7);
			BGl_symbol2299z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2300z00zz__match_normaliza7eza7);
			BGl_list2298z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2220z00zz__match_normaliza7eza7,
				MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_normaliza7eza7,
					MAKE_YOUNG_PAIR(BGl_symbol2223z00zz__match_normaliza7eza7,
						MAKE_YOUNG_PAIR(BGl_symbol2299z00zz__match_normaliza7eza7,
							MAKE_YOUNG_PAIR(BGl_symbol2260z00zz__match_normaliza7eza7,
								MAKE_YOUNG_PAIR(BGl_symbol2229z00zz__match_normaliza7eza7,
									BNIL))))));
			BGl_symbol2303z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2304z00zz__match_normaliza7eza7);
			BGl_list2305z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
			BGl_symbol2307z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2308z00zz__match_normaliza7eza7);
			BGl_list2306z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2307z00zz__match_normaliza7eza7, BNIL);
			BGl_symbol2309z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2310z00zz__match_normaliza7eza7);
			BGl_list2311z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2246z00zz__match_normaliza7eza7,
				MAKE_YOUNG_PAIR(BNIL, BNIL));
			BGl_symbol2313z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2314z00zz__match_normaliza7eza7);
			BGl_list2312z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2313z00zz__match_normaliza7eza7, BNIL);
			BGl_list2315z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_normaliza7eza7,
				MAKE_YOUNG_PAIR(BGl_symbol2220z00zz__match_normaliza7eza7,
					MAKE_YOUNG_PAIR(BGl_symbol2229z00zz__match_normaliza7eza7, BNIL)));
			BGl_list2316z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2273z00zz__match_normaliza7eza7,
				MAKE_YOUNG_PAIR(BGl_symbol2292z00zz__match_normaliza7eza7, BNIL));
			BGl_symbol2317z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2318z00zz__match_normaliza7eza7);
			BGl_list2319z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2265z00zz__match_normaliza7eza7,
				MAKE_YOUNG_PAIR(BGl_symbol2267z00zz__match_normaliza7eza7, BNIL));
			BGl_list2320z00zz__match_normaliza7eza7 =
				MAKE_YOUNG_PAIR(BGl_symbol2220z00zz__match_normaliza7eza7,
				MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_normaliza7eza7,
					MAKE_YOUNG_PAIR(BGl_symbol2223z00zz__match_normaliza7eza7,
						MAKE_YOUNG_PAIR(BGl_symbol2299z00zz__match_normaliza7eza7, BNIL))));
			BGl_symbol2321z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2322z00zz__match_normaliza7eza7);
			BGl_symbol2324z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2325z00zz__match_normaliza7eza7);
			BGl_symbol2326z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2327z00zz__match_normaliza7eza7);
			return (BGl_symbol2329z00zz__match_normaliza7eza7 =
				bstring_to_symbol(BGl_string2330z00zz__match_normaliza7eza7), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			BGl_za2preferzd2xconsza2zd2zz__match_normaliza7eza7 =
				BGl_makezd2togglezd2zz__match_normaliza7eza7();
			BGl_rzd2macrozd2patternzd2initzd2zz__match_normaliza7eza7 = BNIL;
			BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 = BNIL;
			{	/* Match/normalize.scm 360 */
				obj_t BgL_namez00_2465;

				BgL_namez00_2465 = BGl_symbol2217z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2466;

					BgL_fnz00_2466 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2467;

						BgL_arg1724z00_2467 =
							MAKE_YOUNG_PAIR(BgL_namez00_2465,
							BGl_proc2216z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2467, BgL_fnz00_2466);
			}}} BGl_symbol2217z00zz__match_normaliza7eza7;
			{	/* Match/normalize.scm 375 */
				obj_t BgL_namez00_2470;

				BgL_namez00_2470 = BGl_symbol2220z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2471;

					BgL_fnz00_2471 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2472;

						BgL_arg1724z00_2472 =
							MAKE_YOUNG_PAIR(BgL_namez00_2470,
							BGl_proc2219z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2472, BgL_fnz00_2471);
			}}} BGl_symbol2220z00zz__match_normaliza7eza7;
			{	/* Match/normalize.scm 409 */
				obj_t BgL_namez00_2475;

				BgL_namez00_2475 = BGl_symbol2223z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2476;

					BgL_fnz00_2476 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2477;

						BgL_arg1724z00_2477 =
							MAKE_YOUNG_PAIR(BgL_namez00_2475,
							BGl_proc2222z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2477, BgL_fnz00_2476);
			}}} BGl_symbol2223z00zz__match_normaliza7eza7;
			{	/* Match/normalize.scm 428 */
				obj_t BgL_namez00_2483;

				BgL_namez00_2483 = BGl_symbol2226z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2484;

					BgL_fnz00_2484 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2485;

						BgL_arg1724z00_2485 =
							MAKE_YOUNG_PAIR(BgL_namez00_2483,
							BGl_proc2225z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2485, BgL_fnz00_2484);
			}}} BGl_symbol2226z00zz__match_normaliza7eza7;
			{	/* Match/normalize.scm 444 */
				obj_t BgL_namez00_2496;

				BgL_namez00_2496 = BGl_symbol2229z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2497;

					BgL_fnz00_2497 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2498;

						BgL_arg1724z00_2498 =
							MAKE_YOUNG_PAIR(BgL_namez00_2496,
							BGl_proc2228z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2498, BgL_fnz00_2497);
			}}} BGl_symbol2229z00zz__match_normaliza7eza7;
			{	/* Match/normalize.scm 454 */
				obj_t BgL_namez00_2504;

				BgL_namez00_2504 = BGl_symbol2232z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2505;

					BgL_fnz00_2505 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2506;

						BgL_arg1724z00_2506 =
							MAKE_YOUNG_PAIR(BgL_namez00_2504,
							BGl_proc2231z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2506, BgL_fnz00_2505);
			}}} BGl_symbol2232z00zz__match_normaliza7eza7;
			{	/* Match/normalize.scm 460 */
				obj_t BgL_namez00_2512;

				BgL_namez00_2512 = BGl_symbol2235z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2513;

					BgL_fnz00_2513 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2514;

						BgL_arg1724z00_2514 =
							MAKE_YOUNG_PAIR(BgL_namez00_2512,
							BGl_proc2234z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2514, BgL_fnz00_2513);
			}}} BGl_symbol2235z00zz__match_normaliza7eza7;
			BGl_unboundzd2patternzd2zz__match_normaliza7eza7 =
				BGl_symbol2237z00zz__match_normaliza7eza7;
			BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7 = BNIL;
			{	/* Match/normalize.scm 563 */
				obj_t BgL_namez00_2557;

				BgL_namez00_2557 = BGl_symbol2240z00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 343 */
					obj_t BgL_fnz00_2558;

					BgL_fnz00_2558 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
					{	/* Match/normalize.scm 334 */
						obj_t BgL_arg1724z00_2559;

						BgL_arg1724z00_2559 =
							MAKE_YOUNG_PAIR(BgL_namez00_2557,
							BGl_proc2239z00zz__match_normaliza7eza7);
						BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
							MAKE_YOUNG_PAIR(BgL_arg1724z00_2559, BgL_fnz00_2558);
			}}}
			return BGl_symbol2240z00zz__match_normaliza7eza7;
		}

	}



/* &<@anonymous:1403> */
	obj_t BGl_z62zc3z04anonymousza31403ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3386, obj_t BgL_namez00_3387, obj_t BgL_predz00_3388,
		obj_t BgL_eza2za2_3389)
	{
		{	/* Match/normalize.scm 563 */
			{	/* Match/normalize.scm 564 */
				obj_t BgL_zc3z04anonymousza31404ze3z87_3706;

				BgL_zc3z04anonymousza31404ze3z87_3706 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31404ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (3L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31404ze3z87_3706, (int) (0L),
					BgL_eza2za2_3389);
				PROCEDURE_SET(BgL_zc3z04anonymousza31404ze3z87_3706, (int) (1L),
					BgL_predz00_3388);
				PROCEDURE_SET(BgL_zc3z04anonymousza31404ze3z87_3706, (int) (2L),
					BgL_namez00_3387);
				return BgL_zc3z04anonymousza31404ze3z87_3706;
			}
		}

	}



/* &<@anonymous:1404> */
	obj_t BGl_z62zc3z04anonymousza31404ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3390, obj_t BgL_rz00_3394, obj_t BgL_cz00_3395)
	{
		{	/* Match/normalize.scm 564 */
			{	/* Match/normalize.scm 565 */
				obj_t BgL_eza2za2_3391;
				obj_t BgL_predz00_3392;
				obj_t BgL_namez00_3393;

				BgL_eza2za2_3391 = PROCEDURE_REF(BgL_envz00_3390, (int) (0L));
				BgL_predz00_3392 = PROCEDURE_REF(BgL_envz00_3390, (int) (1L));
				BgL_namez00_3393 = PROCEDURE_REF(BgL_envz00_3390, (int) (2L));
				{	/* Match/normalize.scm 565 */
					obj_t BgL_arg1405z00_3707;

					{	/* Match/normalize.scm 565 */
						obj_t BgL_arg1406z00_3708;

						{	/* Match/normalize.scm 565 */
							obj_t BgL_arg1407z00_3709;

							{	/* Match/normalize.scm 565 */
								obj_t BgL_arg1408z00_3710;

								{	/* Match/normalize.scm 565 */
									obj_t BgL_arg1410z00_3711;

									if (NULLP(BgL_eza2za2_3391))
										{	/* Match/normalize.scm 565 */
											BgL_arg1410z00_3711 = BNIL;
										}
									else
										{	/* Match/normalize.scm 565 */
											obj_t BgL_head1137z00_3712;

											{	/* Match/normalize.scm 565 */
												obj_t BgL_arg1417z00_3713;

												{	/* Match/normalize.scm 565 */
													obj_t BgL_arg1418z00_3714;

													BgL_arg1418z00_3714 = CAR(((obj_t) BgL_eza2za2_3391));
													{	/* Match/normalize.scm 124 */
														obj_t BgL_fun1461z00_3715;

														BgL_fun1461z00_3715 =
															BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
															(BgL_arg1418z00_3714);
														BgL_arg1417z00_3713 =
															BGL_PROCEDURE_CALL2(BgL_fun1461z00_3715,
															BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7,
															BGl_proc2242z00zz__match_normaliza7eza7);
													}
												}
												BgL_head1137z00_3712 =
													MAKE_YOUNG_PAIR(BgL_arg1417z00_3713, BNIL);
											}
											{	/* Match/normalize.scm 565 */
												obj_t BgL_g1140z00_3717;

												BgL_g1140z00_3717 = CDR(((obj_t) BgL_eza2za2_3391));
												{
													obj_t BgL_l1135z00_3719;
													obj_t BgL_tail1138z00_3720;

													BgL_l1135z00_3719 = BgL_g1140z00_3717;
													BgL_tail1138z00_3720 = BgL_head1137z00_3712;
												BgL_zc3z04anonymousza31412ze3z87_3718:
													if (NULLP(BgL_l1135z00_3719))
														{	/* Match/normalize.scm 565 */
															BgL_arg1410z00_3711 = BgL_head1137z00_3712;
														}
													else
														{	/* Match/normalize.scm 565 */
															obj_t BgL_newtail1139z00_3721;

															{	/* Match/normalize.scm 565 */
																obj_t BgL_arg1415z00_3722;

																{	/* Match/normalize.scm 565 */
																	obj_t BgL_arg1416z00_3723;

																	BgL_arg1416z00_3723 =
																		CAR(((obj_t) BgL_l1135z00_3719));
																	BgL_arg1415z00_3722 =
																		BGl_normaliza7ezd2patternz75zz__match_normaliza7eza7
																		(BgL_arg1416z00_3723);
																}
																BgL_newtail1139z00_3721 =
																	MAKE_YOUNG_PAIR(BgL_arg1415z00_3722, BNIL);
															}
															SET_CDR(BgL_tail1138z00_3720,
																BgL_newtail1139z00_3721);
															{	/* Match/normalize.scm 565 */
																obj_t BgL_arg1414z00_3724;

																BgL_arg1414z00_3724 =
																	CDR(((obj_t) BgL_l1135z00_3719));
																{
																	obj_t BgL_tail1138z00_4107;
																	obj_t BgL_l1135z00_4106;

																	BgL_l1135z00_4106 = BgL_arg1414z00_3724;
																	BgL_tail1138z00_4107 =
																		BgL_newtail1139z00_3721;
																	BgL_tail1138z00_3720 = BgL_tail1138z00_4107;
																	BgL_l1135z00_3719 = BgL_l1135z00_4106;
																	goto BgL_zc3z04anonymousza31412ze3z87_3718;
																}
															}
														}
												}
											}
										}
									BgL_arg1408z00_3710 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1410z00_3711, BNIL);
								}
								BgL_arg1407z00_3709 =
									MAKE_YOUNG_PAIR(BgL_predz00_3392, BgL_arg1408z00_3710);
							}
							BgL_arg1406z00_3708 =
								MAKE_YOUNG_PAIR(BgL_namez00_3393, BgL_arg1407z00_3709);
						}
						BgL_arg1405z00_3707 =
							MAKE_YOUNG_PAIR(BGl_symbol2240z00zz__match_normaliza7eza7,
							BgL_arg1406z00_3708);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3395, BgL_arg1405z00_3707,
						BgL_rz00_3394);
				}
			}
		}

	}



/* &<@anonymous:1462> */
	obj_t BGl_z62zc3z04anonymousza31462ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3396, obj_t BgL_patternz00_3397, obj_t BgL_rrz00_3398)
	{
		{	/* Match/normalize.scm 126 */
			return BgL_patternz00_3397;
		}

	}



/* &<@anonymous:1392> */
	obj_t BGl_z62zc3z04anonymousza31392ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3399, obj_t BgL_lz00_3400)
	{
		{	/* Match/normalize.scm 460 */
			{	/* Match/normalize.scm 461 */
				bool_t BgL_test2408z00_4117;

				if (NULLP(BgL_lz00_3400))
					{	/* Match/normalize.scm 461 */
						BgL_test2408z00_4117 = ((bool_t) 1);
					}
				else
					{	/* Match/normalize.scm 461 */
						obj_t BgL_tmpz00_4120;

						BgL_tmpz00_4120 = CDR(((obj_t) BgL_lz00_3400));
						BgL_test2408z00_4117 = PAIRP(BgL_tmpz00_4120);
					}
				if (BgL_test2408z00_4117)
					{	/* Match/normalize.scm 461 */
						return
							BGl_errorz00zz__errorz00
							(BGl_symbol2243z00zz__match_normaliza7eza7,
							BGl_string2245z00zz__match_normaliza7eza7, BgL_lz00_3400);
					}
				else
					{	/* Match/normalize.scm 463 */
						obj_t BgL_zc3z04anonymousza31396ze3z87_3725;

						BgL_zc3z04anonymousza31396ze3z87_3725 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31396ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_3725, (int) (0L),
							BgL_lz00_3400);
						return BgL_zc3z04anonymousza31396ze3z87_3725;
					}
			}
		}

	}



/* &<@anonymous:1396> */
	obj_t BGl_z62zc3z04anonymousza31396ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3401, obj_t BgL_rz00_3403, obj_t BgL_cz00_3404)
	{
		{	/* Match/normalize.scm 463 */
			{	/* Match/normalize.scm 464 */
				obj_t BgL_lz00_3402;

				BgL_lz00_3402 = PROCEDURE_REF(BgL_envz00_3401, (int) (0L));
				{	/* Match/normalize.scm 464 */
					obj_t BgL_arg1397z00_3726;

					{	/* Match/normalize.scm 464 */
						obj_t BgL_arg1399z00_3727;

						{	/* Match/normalize.scm 464 */
							obj_t BgL_arg1400z00_3728;

							BgL_arg1400z00_3728 = CAR(((obj_t) BgL_lz00_3402));
							BgL_arg1399z00_3727 = MAKE_YOUNG_PAIR(BgL_arg1400z00_3728, BNIL);
						}
						BgL_arg1397z00_3726 =
							MAKE_YOUNG_PAIR(BGl_symbol2246z00zz__match_normaliza7eza7,
							BgL_arg1399z00_3727);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3404, BgL_arg1397z00_3726,
						BgL_rz00_3403);
				}
			}
		}

	}



/* &<@anonymous:1380> */
	obj_t BGl_z62zc3z04anonymousza31380ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3405, obj_t BgL_lz00_3406)
	{
		{	/* Match/normalize.scm 454 */
			{	/* Match/normalize.scm 455 */
				bool_t BgL_test2410z00_4141;

				if (NULLP(BgL_lz00_3406))
					{	/* Match/normalize.scm 455 */
						BgL_test2410z00_4141 = ((bool_t) 1);
					}
				else
					{	/* Match/normalize.scm 455 */
						obj_t BgL_tmpz00_4144;

						BgL_tmpz00_4144 = CDR(((obj_t) BgL_lz00_3406));
						BgL_test2410z00_4141 = PAIRP(BgL_tmpz00_4144);
					}
				if (BgL_test2410z00_4141)
					{	/* Match/normalize.scm 455 */
						return
							BGl_errorz00zz__errorz00
							(BGl_symbol2243z00zz__match_normaliza7eza7,
							BGl_string2248z00zz__match_normaliza7eza7, BgL_lz00_3406);
					}
				else
					{	/* Match/normalize.scm 457 */
						obj_t BgL_zc3z04anonymousza31384ze3z87_3729;

						BgL_zc3z04anonymousza31384ze3z87_3729 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31384ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31384ze3z87_3729, (int) (0L),
							BgL_lz00_3406);
						return BgL_zc3z04anonymousza31384ze3z87_3729;
					}
			}
		}

	}



/* &<@anonymous:1384> */
	obj_t BGl_z62zc3z04anonymousza31384ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3407, obj_t BgL_rz00_3409, obj_t BgL_cz00_3410)
	{
		{	/* Match/normalize.scm 457 */
			{	/* Match/normalize.scm 458 */
				obj_t BgL_lz00_3408;

				BgL_lz00_3408 = PROCEDURE_REF(BgL_envz00_3407, (int) (0L));
				{	/* Match/normalize.scm 458 */
					obj_t BgL_arg1387z00_3730;

					{	/* Match/normalize.scm 458 */
						obj_t BgL_arg1388z00_3731;

						{	/* Match/normalize.scm 458 */
							obj_t BgL_arg1389z00_3732;

							BgL_arg1389z00_3732 = CAR(((obj_t) BgL_lz00_3408));
							BgL_arg1388z00_3731 = MAKE_YOUNG_PAIR(BgL_arg1389z00_3732, BNIL);
						}
						BgL_arg1387z00_3730 =
							MAKE_YOUNG_PAIR(BGl_symbol2249z00zz__match_normaliza7eza7,
							BgL_arg1388z00_3731);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3410, BgL_arg1387z00_3730,
						BgL_rz00_3409);
				}
			}
		}

	}



/* &<@anonymous:1368> */
	obj_t BGl_z62zc3z04anonymousza31368ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3411, obj_t BgL_lz00_3412)
	{
		{	/* Match/normalize.scm 444 */
			{	/* Match/normalize.scm 445 */
				bool_t BgL_test2412z00_4165;

				if (NULLP(BgL_lz00_3412))
					{	/* Match/normalize.scm 445 */
						BgL_test2412z00_4165 = ((bool_t) 1);
					}
				else
					{	/* Match/normalize.scm 445 */
						obj_t BgL_tmpz00_4168;

						BgL_tmpz00_4168 = CDR(((obj_t) BgL_lz00_3412));
						BgL_test2412z00_4165 = PAIRP(BgL_tmpz00_4168);
					}
				if (BgL_test2412z00_4165)
					{	/* Match/normalize.scm 445 */
						return
							BGl_errorz00zz__errorz00
							(BGl_symbol2243z00zz__match_normaliza7eza7,
							BGl_string2251z00zz__match_normaliza7eza7, BgL_lz00_3412);
					}
				else
					{	/* Match/normalize.scm 447 */
						obj_t BgL_ez00_3733;

						BgL_ez00_3733 = CAR(((obj_t) BgL_lz00_3412));
						{	/* Match/normalize.scm 448 */
							obj_t BgL_zc3z04anonymousza31372ze3z87_3734;

							BgL_zc3z04anonymousza31372ze3z87_3734 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31372ze3ze5zz__match_normaliza7eza7,
								(int) (2L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31372ze3z87_3734, (int) (0L),
								BgL_ez00_3733);
							return BgL_zc3z04anonymousza31372ze3z87_3734;
						}
					}
			}
		}

	}



/* &<@anonymous:1372> */
	obj_t BGl_z62zc3z04anonymousza31372ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3413, obj_t BgL_rz00_3415, obj_t BgL_cz00_3416)
	{
		{	/* Match/normalize.scm 448 */
			{	/* Match/normalize.scm 449 */
				obj_t BgL_ez00_3414;

				BgL_ez00_3414 = PROCEDURE_REF(BgL_envz00_3413, (int) (0L));
				{	/* Match/normalize.scm 449 */
					obj_t BgL_fun1374z00_3735;

					BgL_fun1374z00_3735 =
						BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
						(BgL_ez00_3414);
					{	/* Match/normalize.scm 452 */
						obj_t BgL_zc3z04anonymousza31375ze3z87_3736;

						BgL_zc3z04anonymousza31375ze3z87_3736 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31375ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31375ze3z87_3736, (int) (0L),
							BgL_cz00_3416);
						PROCEDURE_SET(BgL_zc3z04anonymousza31375ze3z87_3736, (int) (1L),
							BgL_rz00_3415);
						return BGL_PROCEDURE_CALL2(BgL_fun1374z00_3735, BgL_rz00_3415,
							BgL_zc3z04anonymousza31375ze3z87_3736);
					}
				}
			}
		}

	}



/* &<@anonymous:1375> */
	obj_t BGl_z62zc3z04anonymousza31375ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3417, obj_t BgL_patternz00_3420, obj_t BgL_rrz00_3421)
	{
		{	/* Match/normalize.scm 451 */
			{	/* Match/normalize.scm 452 */
				obj_t BgL_cz00_3418;
				obj_t BgL_rz00_3419;

				BgL_cz00_3418 = PROCEDURE_REF(BgL_envz00_3417, (int) (0L));
				BgL_rz00_3419 = PROCEDURE_REF(BgL_envz00_3417, (int) (1L));
				{	/* Match/normalize.scm 452 */
					obj_t BgL_arg1376z00_3737;

					{	/* Match/normalize.scm 452 */
						obj_t BgL_arg1377z00_3738;

						BgL_arg1377z00_3738 = MAKE_YOUNG_PAIR(BgL_patternz00_3420, BNIL);
						BgL_arg1376z00_3737 =
							MAKE_YOUNG_PAIR(BGl_symbol2229z00zz__match_normaliza7eza7,
							BgL_arg1377z00_3738);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3418, BgL_arg1376z00_3737,
						BgL_rz00_3419);
				}
			}
		}

	}



/* &<@anonymous:1351> */
	obj_t BGl_z62zc3z04anonymousza31351ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3422, obj_t BgL_lz00_3423)
	{
		{	/* Match/normalize.scm 428 */
			if (NULLP(BgL_lz00_3423))
				{	/* Match/normalize.scm 429 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2243z00zz__match_normaliza7eza7,
						BGl_string2252z00zz__match_normaliza7eza7, BgL_lz00_3423);
				}
			else
				{	/* Match/normalize.scm 431 */
					obj_t BgL_ez00_3739;
					obj_t BgL_eza2za2_3740;

					BgL_ez00_3739 = CAR(((obj_t) BgL_lz00_3423));
					BgL_eza2za2_3740 = CDR(((obj_t) BgL_lz00_3423));
					{	/* Match/normalize.scm 433 */
						obj_t BgL_zc3z04anonymousza31353ze3z87_3741;

						BgL_zc3z04anonymousza31353ze3z87_3741 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31353ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31353ze3z87_3741, (int) (0L),
							BgL_ez00_3739);
						PROCEDURE_SET(BgL_zc3z04anonymousza31353ze3z87_3741, (int) (1L),
							BgL_eza2za2_3740);
						return BgL_zc3z04anonymousza31353ze3z87_3741;
					}
				}
		}

	}



/* &<@anonymous:1353> */
	obj_t BGl_z62zc3z04anonymousza31353ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3424, obj_t BgL_rz00_3427, obj_t BgL_cz00_3428)
	{
		{	/* Match/normalize.scm 433 */
			{	/* Match/normalize.scm 434 */
				obj_t BgL_ez00_3425;
				obj_t BgL_eza2za2_3426;

				BgL_ez00_3425 = PROCEDURE_REF(BgL_envz00_3424, (int) (0L));
				BgL_eza2za2_3426 = PROCEDURE_REF(BgL_envz00_3424, (int) (1L));
				if (PAIRP(BgL_eza2za2_3426))
					{	/* Match/normalize.scm 435 */
						obj_t BgL_fun1357z00_3742;

						BgL_fun1357z00_3742 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3425);
						{	/* Match/normalize.scm 438 */
							obj_t BgL_zc3z04anonymousza31358ze3z87_3743;

							BgL_zc3z04anonymousza31358ze3z87_3743 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31358ze3ze5zz__match_normaliza7eza7,
								(int) (2L), (int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31358ze3z87_3743, (int) (0L),
								BgL_eza2za2_3426);
							PROCEDURE_SET(BgL_zc3z04anonymousza31358ze3z87_3743, (int) (1L),
								BgL_cz00_3428);
							return BGL_PROCEDURE_CALL2(BgL_fun1357z00_3742, BgL_rz00_3427,
								BgL_zc3z04anonymousza31358ze3z87_3743);
						}
					}
				else
					{	/* Match/normalize.scm 442 */
						obj_t BgL_fun1366z00_3744;

						BgL_fun1366z00_3744 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3425);
						return BGL_PROCEDURE_CALL2(BgL_fun1366z00_3744, BgL_rz00_3427,
							BgL_cz00_3428);
					}
			}
		}

	}



/* &<@anonymous:1358> */
	obj_t BGl_z62zc3z04anonymousza31358ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3429, obj_t BgL_pattern1z00_3432, obj_t BgL_rrz00_3433)
	{
		{	/* Match/normalize.scm 437 */
			{	/* Match/normalize.scm 438 */
				obj_t BgL_eza2za2_3430;
				obj_t BgL_cz00_3431;

				BgL_eza2za2_3430 = PROCEDURE_REF(BgL_envz00_3429, (int) (0L));
				BgL_cz00_3431 = PROCEDURE_REF(BgL_envz00_3429, (int) (1L));
				{	/* Match/normalize.scm 438 */
					obj_t BgL_fun1361z00_3745;

					{	/* Match/normalize.scm 438 */
						obj_t BgL_arg1359z00_3746;

						BgL_arg1359z00_3746 =
							MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_normaliza7eza7,
							BgL_eza2za2_3430);
						BgL_fun1361z00_3745 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_arg1359z00_3746);
					}
					{	/* Match/normalize.scm 441 */
						obj_t BgL_zc3z04anonymousza31362ze3z87_3747;

						BgL_zc3z04anonymousza31362ze3z87_3747 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31362ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31362ze3z87_3747, (int) (0L),
							BgL_pattern1z00_3432);
						PROCEDURE_SET(BgL_zc3z04anonymousza31362ze3z87_3747, (int) (1L),
							BgL_cz00_3431);
						return BGL_PROCEDURE_CALL2(BgL_fun1361z00_3745, BgL_rrz00_3433,
							BgL_zc3z04anonymousza31362ze3z87_3747);
					}
				}
			}
		}

	}



/* &<@anonymous:1362> */
	obj_t BGl_z62zc3z04anonymousza31362ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3434, obj_t BgL_pattern2z00_3437, obj_t BgL_rrrz00_3438)
	{
		{	/* Match/normalize.scm 440 */
			{	/* Match/normalize.scm 441 */
				obj_t BgL_pattern1z00_3435;
				obj_t BgL_cz00_3436;

				BgL_pattern1z00_3435 = PROCEDURE_REF(BgL_envz00_3434, (int) (0L));
				BgL_cz00_3436 = PROCEDURE_REF(BgL_envz00_3434, (int) (1L));
				{	/* Match/normalize.scm 441 */
					obj_t BgL_arg1363z00_3748;

					{	/* Match/normalize.scm 441 */
						obj_t BgL_arg1364z00_3749;

						{	/* Match/normalize.scm 441 */
							obj_t BgL_arg1365z00_3750;

							BgL_arg1365z00_3750 = MAKE_YOUNG_PAIR(BgL_pattern2z00_3437, BNIL);
							BgL_arg1364z00_3749 =
								MAKE_YOUNG_PAIR(BgL_pattern1z00_3435, BgL_arg1365z00_3750);
						}
						BgL_arg1363z00_3748 =
							MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_normaliza7eza7,
							BgL_arg1364z00_3749);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3436, BgL_arg1363z00_3748,
						BgL_rrrz00_3438);
				}
			}
		}

	}



/* &<@anonymous:1332> */
	obj_t BGl_z62zc3z04anonymousza31332ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3439, obj_t BgL_lz00_3440)
	{
		{	/* Match/normalize.scm 409 */
			if (NULLP(BgL_lz00_3440))
				{	/* Match/normalize.scm 410 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2243z00zz__match_normaliza7eza7,
						BGl_string2253z00zz__match_normaliza7eza7, BgL_lz00_3440);
				}
			else
				{	/* Match/normalize.scm 412 */
					obj_t BgL_ez00_3751;
					obj_t BgL_eza2za2_3752;

					BgL_ez00_3751 = CAR(((obj_t) BgL_lz00_3440));
					BgL_eza2za2_3752 = CDR(((obj_t) BgL_lz00_3440));
					{	/* Match/normalize.scm 414 */
						obj_t BgL_zc3z04anonymousza31334ze3z87_3753;

						BgL_zc3z04anonymousza31334ze3z87_3753 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31334ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31334ze3z87_3753, (int) (0L),
							BgL_ez00_3751);
						PROCEDURE_SET(BgL_zc3z04anonymousza31334ze3z87_3753, (int) (1L),
							BgL_eza2za2_3752);
						PROCEDURE_SET(BgL_zc3z04anonymousza31334ze3z87_3753, (int) (2L),
							BgL_lz00_3440);
						return BgL_zc3z04anonymousza31334ze3z87_3753;
					}
				}
		}

	}



/* &<@anonymous:1334> */
	obj_t BGl_z62zc3z04anonymousza31334ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3441, obj_t BgL_rz00_3445, obj_t BgL_cz00_3446)
	{
		{	/* Match/normalize.scm 414 */
			{	/* Match/normalize.scm 415 */
				obj_t BgL_ez00_3442;
				obj_t BgL_eza2za2_3443;
				obj_t BgL_lz00_3444;

				BgL_ez00_3442 = PROCEDURE_REF(BgL_envz00_3441, (int) (0L));
				BgL_eza2za2_3443 = PROCEDURE_REF(BgL_envz00_3441, (int) (1L));
				BgL_lz00_3444 = PROCEDURE_REF(BgL_envz00_3441, (int) (2L));
				if (PAIRP(BgL_eza2za2_3443))
					{	/* Match/normalize.scm 416 */
						obj_t BgL_fun1337z00_3754;

						BgL_fun1337z00_3754 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3442);
						{	/* Match/normalize.scm 419 */
							obj_t BgL_zc3z04anonymousza31338ze3z87_3755;

							BgL_zc3z04anonymousza31338ze3z87_3755 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31338ze3ze5zz__match_normaliza7eza7,
								(int) (2L), (int) (4L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_3755, (int) (0L),
								BgL_eza2za2_3443);
							PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_3755, (int) (1L),
								BgL_lz00_3444);
							PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_3755, (int) (2L),
								BgL_cz00_3446);
							PROCEDURE_SET(BgL_zc3z04anonymousza31338ze3z87_3755, (int) (3L),
								BgL_rz00_3445);
							return BGL_PROCEDURE_CALL2(BgL_fun1337z00_3754, BgL_rz00_3445,
								BgL_zc3z04anonymousza31338ze3z87_3755);
						}
					}
				else
					{	/* Match/normalize.scm 426 */
						obj_t BgL_fun1349z00_3756;

						BgL_fun1349z00_3756 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3442);
						return BGL_PROCEDURE_CALL2(BgL_fun1349z00_3756, BgL_rz00_3445,
							BgL_cz00_3446);
					}
			}
		}

	}



/* &<@anonymous:1338> */
	obj_t BGl_z62zc3z04anonymousza31338ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3447, obj_t BgL_pattern1z00_3452, obj_t BgL_rrz00_3453)
	{
		{	/* Match/normalize.scm 418 */
			{	/* Match/normalize.scm 419 */
				obj_t BgL_eza2za2_3448;
				obj_t BgL_lz00_3449;
				obj_t BgL_cz00_3450;
				obj_t BgL_rz00_3451;

				BgL_eza2za2_3448 = PROCEDURE_REF(BgL_envz00_3447, (int) (0L));
				BgL_lz00_3449 = PROCEDURE_REF(BgL_envz00_3447, (int) (1L));
				BgL_cz00_3450 = PROCEDURE_REF(BgL_envz00_3447, (int) (2L));
				BgL_rz00_3451 = PROCEDURE_REF(BgL_envz00_3447, (int) (3L));
				{	/* Match/normalize.scm 419 */
					obj_t BgL_fun1341z00_3757;

					{	/* Match/normalize.scm 419 */
						obj_t BgL_arg1339z00_3758;

						BgL_arg1339z00_3758 =
							MAKE_YOUNG_PAIR(BGl_symbol2223z00zz__match_normaliza7eza7,
							BgL_eza2za2_3448);
						BgL_fun1341z00_3757 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_arg1339z00_3758);
					}
					{	/* Match/normalize.scm 422 */
						obj_t BgL_zc3z04anonymousza31342ze3z87_3759;

						BgL_zc3z04anonymousza31342ze3z87_3759 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31342ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (4L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31342ze3z87_3759, (int) (0L),
							BgL_lz00_3449);
						PROCEDURE_SET(BgL_zc3z04anonymousza31342ze3z87_3759, (int) (1L),
							BgL_pattern1z00_3452);
						PROCEDURE_SET(BgL_zc3z04anonymousza31342ze3z87_3759, (int) (2L),
							BgL_cz00_3450);
						PROCEDURE_SET(BgL_zc3z04anonymousza31342ze3z87_3759, (int) (3L),
							BgL_rrz00_3453);
						return BGL_PROCEDURE_CALL2(BgL_fun1341z00_3757, BgL_rz00_3451,
							BgL_zc3z04anonymousza31342ze3z87_3759);
					}
				}
			}
		}

	}



/* &<@anonymous:1342> */
	obj_t BGl_z62zc3z04anonymousza31342ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3454, obj_t BgL_pattern2z00_3459, obj_t BgL_rrrz00_3460)
	{
		{	/* Match/normalize.scm 421 */
			{	/* Match/normalize.scm 422 */
				obj_t BgL_lz00_3455;
				obj_t BgL_pattern1z00_3456;
				obj_t BgL_cz00_3457;
				obj_t BgL_rrz00_3458;

				BgL_lz00_3455 = PROCEDURE_REF(BgL_envz00_3454, (int) (0L));
				BgL_pattern1z00_3456 = PROCEDURE_REF(BgL_envz00_3454, (int) (1L));
				BgL_cz00_3457 = PROCEDURE_REF(BgL_envz00_3454, (int) (2L));
				BgL_rrz00_3458 = PROCEDURE_REF(BgL_envz00_3454, (int) (3L));
				{	/* Match/normalize.scm 422 */
					bool_t BgL_test2418z00_4356;

					if (BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7
						(BgL_rrz00_3458, BgL_rrrz00_3460))
						{	/* Match/normalize.scm 422 */
							BgL_test2418z00_4356 =
								BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7
								(BgL_rrrz00_3460, BgL_rrz00_3458);
						}
					else
						{	/* Match/normalize.scm 422 */
							BgL_test2418z00_4356 = ((bool_t) 0);
						}
					if (BgL_test2418z00_4356)
						{	/* Match/normalize.scm 424 */
							obj_t BgL_arg1346z00_3760;

							{	/* Match/normalize.scm 424 */
								obj_t BgL_arg1347z00_3761;

								{	/* Match/normalize.scm 424 */
									obj_t BgL_arg1348z00_3762;

									BgL_arg1348z00_3762 =
										MAKE_YOUNG_PAIR(BgL_pattern2z00_3459, BNIL);
									BgL_arg1347z00_3761 =
										MAKE_YOUNG_PAIR(BgL_pattern1z00_3456, BgL_arg1348z00_3762);
								}
								BgL_arg1346z00_3760 =
									MAKE_YOUNG_PAIR(BGl_symbol2223z00zz__match_normaliza7eza7,
									BgL_arg1347z00_3761);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_cz00_3457, BgL_arg1346z00_3760,
								BgL_rrrz00_3460);
						}
					else
						{	/* Match/normalize.scm 422 */
							return
								BGl_errorz00zz__errorz00
								(BGl_symbol2243z00zz__match_normaliza7eza7,
								BGl_string2254z00zz__match_normaliza7eza7, BgL_lz00_3455);
						}
				}
			}
		}

	}



/* &<@anonymous:1313> */
	obj_t BGl_z62zc3z04anonymousza31313ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3461, obj_t BgL_lz00_3462)
	{
		{	/* Match/normalize.scm 375 */
			if (NULLP(BgL_lz00_3462))
				{	/* Match/normalize.scm 376 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2243z00zz__match_normaliza7eza7,
						BGl_string2255z00zz__match_normaliza7eza7, BgL_lz00_3462);
				}
			else
				{	/* Match/normalize.scm 378 */
					obj_t BgL_ez00_3763;
					obj_t BgL_eza2za2_3764;

					BgL_ez00_3763 = CAR(((obj_t) BgL_lz00_3462));
					BgL_eza2za2_3764 = CDR(((obj_t) BgL_lz00_3462));
					{	/* Match/normalize.scm 380 */
						obj_t BgL_zc3z04anonymousza31315ze3z87_3765;

						BgL_zc3z04anonymousza31315ze3z87_3765 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31315ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31315ze3z87_3765, (int) (0L),
							BgL_ez00_3763);
						PROCEDURE_SET(BgL_zc3z04anonymousza31315ze3z87_3765, (int) (1L),
							BgL_eza2za2_3764);
						PROCEDURE_SET(BgL_zc3z04anonymousza31315ze3z87_3765, (int) (2L),
							BgL_lz00_3462);
						return BgL_zc3z04anonymousza31315ze3z87_3765;
					}
				}
		}

	}



/* &<@anonymous:1315> */
	obj_t BGl_z62zc3z04anonymousza31315ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3463, obj_t BgL_rz00_3467, obj_t BgL_cz00_3468)
	{
		{	/* Match/normalize.scm 380 */
			{	/* Match/normalize.scm 381 */
				obj_t BgL_ez00_3464;
				obj_t BgL_eza2za2_3465;
				obj_t BgL_lz00_3466;

				BgL_ez00_3464 = PROCEDURE_REF(BgL_envz00_3463, (int) (0L));
				BgL_eza2za2_3465 = PROCEDURE_REF(BgL_envz00_3463, (int) (1L));
				BgL_lz00_3466 = PROCEDURE_REF(BgL_envz00_3463, (int) (2L));
				if (PAIRP(BgL_eza2za2_3465))
					{	/* Match/normalize.scm 382 */
						obj_t BgL_fun1318z00_3766;

						BgL_fun1318z00_3766 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3464);
						{	/* Match/normalize.scm 385 */
							obj_t BgL_zc3z04anonymousza31319ze3z87_3767;

							BgL_zc3z04anonymousza31319ze3z87_3767 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31319ze3ze5zz__match_normaliza7eza7,
								(int) (2L), (int) (4L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3767, (int) (0L),
								BgL_eza2za2_3465);
							PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3767, (int) (1L),
								BgL_lz00_3466);
							PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3767, (int) (2L),
								BgL_cz00_3468);
							PROCEDURE_SET(BgL_zc3z04anonymousza31319ze3z87_3767, (int) (3L),
								BgL_rz00_3467);
							return BGL_PROCEDURE_CALL2(BgL_fun1318z00_3766, BgL_rz00_3467,
								BgL_zc3z04anonymousza31319ze3z87_3767);
						}
					}
				else
					{	/* Match/normalize.scm 392 */
						obj_t BgL_fun1329z00_3768;

						BgL_fun1329z00_3768 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3464);
						return BGL_PROCEDURE_CALL2(BgL_fun1329z00_3768, BgL_rz00_3467,
							BgL_cz00_3468);
					}
			}
		}

	}



/* &<@anonymous:1319> */
	obj_t BGl_z62zc3z04anonymousza31319ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3469, obj_t BgL_pattern1z00_3474, obj_t BgL_rrz00_3475)
	{
		{	/* Match/normalize.scm 384 */
			{	/* Match/normalize.scm 385 */
				obj_t BgL_eza2za2_3470;
				obj_t BgL_lz00_3471;
				obj_t BgL_cz00_3472;
				obj_t BgL_rz00_3473;

				BgL_eza2za2_3470 = PROCEDURE_REF(BgL_envz00_3469, (int) (0L));
				BgL_lz00_3471 = PROCEDURE_REF(BgL_envz00_3469, (int) (1L));
				BgL_cz00_3472 = PROCEDURE_REF(BgL_envz00_3469, (int) (2L));
				BgL_rz00_3473 = PROCEDURE_REF(BgL_envz00_3469, (int) (3L));
				{	/* Match/normalize.scm 385 */
					obj_t BgL_fun1322z00_3769;

					{	/* Match/normalize.scm 385 */
						obj_t BgL_arg1320z00_3770;

						BgL_arg1320z00_3770 =
							MAKE_YOUNG_PAIR(BGl_symbol2220z00zz__match_normaliza7eza7,
							BgL_eza2za2_3470);
						BgL_fun1322z00_3769 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_arg1320z00_3770);
					}
					{	/* Match/normalize.scm 388 */
						obj_t BgL_zc3z04anonymousza31323ze3z87_3771;

						BgL_zc3z04anonymousza31323ze3z87_3771 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31323ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (4L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31323ze3z87_3771, (int) (0L),
							BgL_lz00_3471);
						PROCEDURE_SET(BgL_zc3z04anonymousza31323ze3z87_3771, (int) (1L),
							BgL_pattern1z00_3474);
						PROCEDURE_SET(BgL_zc3z04anonymousza31323ze3z87_3771, (int) (2L),
							BgL_cz00_3472);
						PROCEDURE_SET(BgL_zc3z04anonymousza31323ze3z87_3771, (int) (3L),
							BgL_rrz00_3475);
						return BGL_PROCEDURE_CALL2(BgL_fun1322z00_3769, BgL_rz00_3473,
							BgL_zc3z04anonymousza31323ze3z87_3771);
					}
				}
			}
		}

	}



/* &<@anonymous:1323> */
	obj_t BGl_z62zc3z04anonymousza31323ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3476, obj_t BgL_pattern2z00_3481, obj_t BgL_rrrz00_3482)
	{
		{	/* Match/normalize.scm 387 */
			{	/* Match/normalize.scm 388 */
				obj_t BgL_lz00_3477;
				obj_t BgL_pattern1z00_3478;
				obj_t BgL_cz00_3479;
				obj_t BgL_rrz00_3480;

				BgL_lz00_3477 = PROCEDURE_REF(BgL_envz00_3476, (int) (0L));
				BgL_pattern1z00_3478 = PROCEDURE_REF(BgL_envz00_3476, (int) (1L));
				BgL_cz00_3479 = PROCEDURE_REF(BgL_envz00_3476, (int) (2L));
				BgL_rrz00_3480 = PROCEDURE_REF(BgL_envz00_3476, (int) (3L));
				{	/* Match/normalize.scm 388 */
					bool_t BgL_test2422z00_4450;

					if (BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7
						(BgL_rrz00_3480, BgL_rrrz00_3482))
						{	/* Match/normalize.scm 388 */
							BgL_test2422z00_4450 =
								BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7
								(BgL_rrrz00_3482, BgL_rrz00_3480);
						}
					else
						{	/* Match/normalize.scm 388 */
							BgL_test2422z00_4450 = ((bool_t) 0);
						}
					if (BgL_test2422z00_4450)
						{	/* Match/normalize.scm 390 */
							obj_t BgL_arg1326z00_3772;

							{	/* Match/normalize.scm 390 */
								obj_t BgL_arg1327z00_3773;

								{	/* Match/normalize.scm 390 */
									obj_t BgL_arg1328z00_3774;

									BgL_arg1328z00_3774 =
										MAKE_YOUNG_PAIR(BgL_pattern2z00_3481, BNIL);
									BgL_arg1327z00_3773 =
										MAKE_YOUNG_PAIR(BgL_pattern1z00_3478, BgL_arg1328z00_3774);
								}
								BgL_arg1326z00_3772 =
									MAKE_YOUNG_PAIR(BGl_symbol2220z00zz__match_normaliza7eza7,
									BgL_arg1327z00_3773);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_cz00_3479, BgL_arg1326z00_3772,
								BgL_rrrz00_3482);
						}
					else
						{	/* Match/normalize.scm 388 */
							return
								BGl_errorz00zz__errorz00
								(BGl_symbol2243z00zz__match_normaliza7eza7,
								BGl_string2254z00zz__match_normaliza7eza7, BgL_lz00_3477);
						}
				}
			}
		}

	}



/* &<@anonymous:1250> */
	obj_t BGl_z62zc3z04anonymousza31250ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3483, obj_t BgL_lz00_3484)
	{
		{	/* Match/normalize.scm 360 */
			if (NULLP(BgL_lz00_3484))
				{	/* Match/normalize.scm 361 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2243z00zz__match_normaliza7eza7,
						BGl_string2256z00zz__match_normaliza7eza7, BgL_lz00_3484);
				}
			else
				{	/* Match/normalize.scm 363 */
					obj_t BgL_ez00_3775;
					obj_t BgL_eza2za2_3776;

					BgL_ez00_3775 = CAR(((obj_t) BgL_lz00_3484));
					BgL_eza2za2_3776 = CDR(((obj_t) BgL_lz00_3484));
					{	/* Match/normalize.scm 365 */
						obj_t BgL_zc3z04anonymousza31252ze3z87_3777;

						BgL_zc3z04anonymousza31252ze3z87_3777 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31252ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31252ze3z87_3777, (int) (0L),
							BgL_ez00_3775);
						PROCEDURE_SET(BgL_zc3z04anonymousza31252ze3z87_3777, (int) (1L),
							BgL_eza2za2_3776);
						return BgL_zc3z04anonymousza31252ze3z87_3777;
					}
				}
		}

	}



/* &<@anonymous:1252> */
	obj_t BGl_z62zc3z04anonymousza31252ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3485, obj_t BgL_rz00_3488, obj_t BgL_cz00_3489)
	{
		{	/* Match/normalize.scm 365 */
			{	/* Match/normalize.scm 366 */
				obj_t BgL_ez00_3486;
				obj_t BgL_eza2za2_3487;

				BgL_ez00_3486 = PROCEDURE_REF(BgL_envz00_3485, (int) (0L));
				BgL_eza2za2_3487 = PROCEDURE_REF(BgL_envz00_3485, (int) (1L));
				if (PAIRP(BgL_eza2za2_3487))
					{	/* Match/normalize.scm 366 */
						return
							BGl_errorz00zz__errorz00
							(BGl_symbol2243z00zz__match_normaliza7eza7,
							BGl_string2257z00zz__match_normaliza7eza7, BgL_eza2za2_3487);
					}
				else
					{	/* Match/normalize.scm 368 */
						obj_t BgL_fun1269z00_3778;

						BgL_fun1269z00_3778 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_ez00_3486);
						{	/* Match/normalize.scm 371 */
							obj_t BgL_zc3z04anonymousza31270ze3z87_3779;

							BgL_zc3z04anonymousza31270ze3z87_3779 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31270ze3ze5zz__match_normaliza7eza7,
								(int) (2L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31270ze3z87_3779, (int) (0L),
								BgL_cz00_3489);
							return BGL_PROCEDURE_CALL2(BgL_fun1269z00_3778, BgL_rz00_3488,
								BgL_zc3z04anonymousza31270ze3z87_3779);
						}
					}
			}
		}

	}



/* &<@anonymous:1270> */
	obj_t BGl_z62zc3z04anonymousza31270ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3490, obj_t BgL_patternz00_3492, obj_t BgL_rrz00_3493)
	{
		{	/* Match/normalize.scm 370 */
			{	/* Match/normalize.scm 371 */
				obj_t BgL_cz00_3491;

				BgL_cz00_3491 = PROCEDURE_REF(BgL_envz00_3490, (int) (0L));
				{	/* Match/normalize.scm 371 */
					obj_t BgL_arg1272z00_3780;

					{	/* Match/normalize.scm 371 */
						obj_t BgL_arg1284z00_3781;

						{	/* Match/normalize.scm 371 */
							obj_t BgL_arg1304z00_3782;
							obj_t BgL_arg1305z00_3783;

							{	/* Match/normalize.scm 371 */
								obj_t BgL_arg1306z00_3784;

								{	/* Match/normalize.scm 371 */
									obj_t BgL_arg1307z00_3785;

									{	/* Match/normalize.scm 371 */
										obj_t BgL_arg1308z00_3786;

										{	/* Match/normalize.scm 371 */
											obj_t BgL_arg1309z00_3787;
											obj_t BgL_arg1310z00_3788;

											BgL_arg1309z00_3787 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
											{	/* Match/normalize.scm 371 */
												obj_t BgL_arg1311z00_3789;

												BgL_arg1311z00_3789 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
												BgL_arg1310z00_3788 =
													MAKE_YOUNG_PAIR(BgL_arg1311z00_3789, BNIL);
											}
											BgL_arg1308z00_3786 =
												MAKE_YOUNG_PAIR(BgL_arg1309z00_3787,
												BgL_arg1310z00_3788);
										}
										BgL_arg1307z00_3785 =
											MAKE_YOUNG_PAIR(BGl_symbol2260z00zz__match_normaliza7eza7,
											BgL_arg1308z00_3786);
									}
									BgL_arg1306z00_3784 =
										MAKE_YOUNG_PAIR(BgL_arg1307z00_3785, BNIL);
								}
								BgL_arg1304z00_3782 =
									MAKE_YOUNG_PAIR(BGl_symbol2229z00zz__match_normaliza7eza7,
									BgL_arg1306z00_3784);
							}
							BgL_arg1305z00_3783 = MAKE_YOUNG_PAIR(BgL_patternz00_3492, BNIL);
							BgL_arg1284z00_3781 =
								MAKE_YOUNG_PAIR(BgL_arg1304z00_3782, BgL_arg1305z00_3783);
						}
						BgL_arg1272z00_3780 =
							MAKE_YOUNG_PAIR(BGl_symbol2226z00zz__match_normaliza7eza7,
							BgL_arg1284z00_3781);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3491, BgL_arg1272z00_3780,
						BgL_rrz00_3493);
				}
			}
		}

	}



/* term-variable? */
	bool_t BGl_termzd2variablezf3z21zz__match_normaliza7eza7(obj_t BgL_ez00_3)
	{
		{	/* Match/normalize.scm 72 */
			if (SYMBOLP(BgL_ez00_3))
				{	/* Match/normalize.scm 74 */
					bool_t BgL_test2427z00_4514;

					{	/* Match/normalize.scm 74 */
						long BgL_a1110z00_1445;

						{	/* Match/normalize.scm 74 */
							obj_t BgL_arg1425z00_1448;

							BgL_arg1425z00_1448 = SYMBOL_TO_STRING(BgL_ez00_3);
							BgL_a1110z00_1445 = STRING_LENGTH(BgL_arg1425z00_1448);
						}
						{	/* Match/normalize.scm 74 */

							BgL_test2427z00_4514 = (BgL_a1110z00_1445 > 1L);
					}}
					if (BgL_test2427z00_4514)
						{	/* Match/normalize.scm 75 */
							unsigned char BgL_arg1422z00_1443;

							{	/* Match/normalize.scm 75 */
								obj_t BgL_arg1423z00_1444;

								BgL_arg1423z00_1444 = SYMBOL_TO_STRING(BgL_ez00_3);
								BgL_arg1422z00_1443 = STRING_REF(BgL_arg1423z00_1444, 0L);
							}
							return (BgL_arg1422z00_1443 == ((unsigned char) '?'));
						}
					else
						{	/* Match/normalize.scm 74 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/normalize.scm 73 */
					return ((bool_t) 0);
				}
		}

	}



/* segment-variable? */
	bool_t BGl_segmentzd2variablezf3z21zz__match_normaliza7eza7(obj_t BgL_ez00_4)
	{
		{	/* Match/normalize.scm 77 */
			if (SYMBOLP(BgL_ez00_4))
				{	/* Match/normalize.scm 79 */
					bool_t BgL_test2429z00_4523;

					{	/* Match/normalize.scm 79 */
						long BgL_a1112z00_1456;

						{	/* Match/normalize.scm 79 */
							obj_t BgL_arg1431z00_1459;

							BgL_arg1431z00_1459 = SYMBOL_TO_STRING(BgL_ez00_4);
							BgL_a1112z00_1456 = STRING_LENGTH(BgL_arg1431z00_1459);
						}
						{	/* Match/normalize.scm 79 */

							BgL_test2429z00_4523 = (BgL_a1112z00_1456 > 2L);
					}}
					if (BgL_test2429z00_4523)
						{	/* Match/normalize.scm 80 */
							bool_t BgL_test2430z00_4527;

							{	/* Match/normalize.scm 80 */
								unsigned char BgL_arg1428z00_1454;

								{	/* Match/normalize.scm 80 */
									obj_t BgL_arg1429z00_1455;

									BgL_arg1429z00_1455 = SYMBOL_TO_STRING(BgL_ez00_4);
									BgL_arg1428z00_1454 = STRING_REF(BgL_arg1429z00_1455, 0L);
								}
								BgL_test2430z00_4527 =
									(BgL_arg1428z00_1454 == ((unsigned char) '?'));
							}
							if (BgL_test2430z00_4527)
								{	/* Match/normalize.scm 81 */
									unsigned char BgL_arg1426z00_1452;

									{	/* Match/normalize.scm 81 */
										obj_t BgL_arg1427z00_1453;

										BgL_arg1427z00_1453 = SYMBOL_TO_STRING(BgL_ez00_4);
										BgL_arg1426z00_1452 = STRING_REF(BgL_arg1427z00_1453, 1L);
									}
									return (BgL_arg1426z00_1452 == ((unsigned char) '?'));
								}
							else
								{	/* Match/normalize.scm 80 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Match/normalize.scm 79 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/normalize.scm 78 */
					return ((bool_t) 0);
				}
		}

	}



/* lispish-segment-variable? */
	bool_t BGl_lispishzd2segmentzd2variablezf3zf3zz__match_normaliza7eza7(obj_t
		BgL_ez00_5)
	{
		{	/* Match/normalize.scm 83 */
			if (SYMBOLP(BgL_ez00_5))
				{	/* Match/normalize.scm 85 */
					bool_t BgL_test2432z00_4536;

					{	/* Match/normalize.scm 85 */
						long BgL_a1114z00_1470;

						{	/* Match/normalize.scm 85 */
							obj_t BgL_arg1441z00_1473;

							BgL_arg1441z00_1473 = SYMBOL_TO_STRING(BgL_ez00_5);
							BgL_a1114z00_1470 = STRING_LENGTH(BgL_arg1441z00_1473);
						}
						{	/* Match/normalize.scm 85 */

							BgL_test2432z00_4536 = (BgL_a1114z00_1470 > 3L);
					}}
					if (BgL_test2432z00_4536)
						{	/* Match/normalize.scm 86 */
							bool_t BgL_test2433z00_4540;

							{	/* Match/normalize.scm 86 */
								unsigned char BgL_arg1438z00_1468;

								{	/* Match/normalize.scm 86 */
									obj_t BgL_arg1439z00_1469;

									BgL_arg1439z00_1469 = SYMBOL_TO_STRING(BgL_ez00_5);
									BgL_arg1438z00_1468 = STRING_REF(BgL_arg1439z00_1469, 0L);
								}
								BgL_test2433z00_4540 =
									(BgL_arg1438z00_1468 == ((unsigned char) '?'));
							}
							if (BgL_test2433z00_4540)
								{	/* Match/normalize.scm 87 */
									bool_t BgL_test2434z00_4544;

									{	/* Match/normalize.scm 87 */
										unsigned char BgL_arg1436z00_1466;

										{	/* Match/normalize.scm 87 */
											obj_t BgL_arg1437z00_1467;

											BgL_arg1437z00_1467 = SYMBOL_TO_STRING(BgL_ez00_5);
											BgL_arg1436z00_1466 = STRING_REF(BgL_arg1437z00_1467, 1L);
										}
										BgL_test2434z00_4544 =
											(BgL_arg1436z00_1466 == ((unsigned char) '?'));
									}
									if (BgL_test2434z00_4544)
										{	/* Match/normalize.scm 88 */
											unsigned char BgL_arg1434z00_1464;

											{	/* Match/normalize.scm 88 */
												obj_t BgL_arg1435z00_1465;

												BgL_arg1435z00_1465 = SYMBOL_TO_STRING(BgL_ez00_5);
												BgL_arg1434z00_1464 =
													STRING_REF(BgL_arg1435z00_1465, 2L);
											}
											return (BgL_arg1434z00_1464 == ((unsigned char) '?'));
										}
									else
										{	/* Match/normalize.scm 87 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Match/normalize.scm 86 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Match/normalize.scm 85 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/normalize.scm 84 */
					return ((bool_t) 0);
				}
		}

	}



/* tree-variable? */
	bool_t BGl_treezd2variablezf3z21zz__match_normaliza7eza7(obj_t BgL_ez00_6)
	{
		{	/* Match/normalize.scm 90 */
			if (SYMBOLP(BgL_ez00_6))
				{	/* Match/normalize.scm 92 */
					bool_t BgL_test2436z00_4553;

					{	/* Match/normalize.scm 92 */
						long BgL_a1116z00_1478;

						{	/* Match/normalize.scm 92 */
							obj_t BgL_arg1445z00_1481;

							BgL_arg1445z00_1481 = SYMBOL_TO_STRING(BgL_ez00_6);
							BgL_a1116z00_1478 = STRING_LENGTH(BgL_arg1445z00_1481);
						}
						{	/* Match/normalize.scm 92 */

							BgL_test2436z00_4553 = (BgL_a1116z00_1478 > 1L);
					}}
					if (BgL_test2436z00_4553)
						{	/* Match/normalize.scm 93 */
							unsigned char BgL_arg1442z00_1476;

							{	/* Match/normalize.scm 93 */
								obj_t BgL_arg1443z00_1477;

								BgL_arg1443z00_1477 = SYMBOL_TO_STRING(BgL_ez00_6);
								BgL_arg1442z00_1476 = STRING_REF(BgL_arg1443z00_1477, 0L);
							}
							return (BgL_arg1442z00_1476 == ((unsigned char) '!'));
						}
					else
						{	/* Match/normalize.scm 92 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/normalize.scm 91 */
					return ((bool_t) 0);
				}
		}

	}



/* hole-variable? */
	bool_t BGl_holezd2variablezf3z21zz__match_normaliza7eza7(obj_t BgL_ez00_7)
	{
		{	/* Match/normalize.scm 95 */
			if (SYMBOLP(BgL_ez00_7))
				{	/* Match/normalize.scm 97 */
					bool_t BgL_test2438z00_4562;

					{	/* Match/normalize.scm 97 */
						long BgL_a1118z00_1486;

						{	/* Match/normalize.scm 97 */
							obj_t BgL_arg1449z00_1489;

							BgL_arg1449z00_1489 = SYMBOL_TO_STRING(BgL_ez00_7);
							BgL_a1118z00_1486 = STRING_LENGTH(BgL_arg1449z00_1489);
						}
						{	/* Match/normalize.scm 97 */

							BgL_test2438z00_4562 = (BgL_a1118z00_1486 > 1L);
					}}
					if (BgL_test2438z00_4562)
						{	/* Match/normalize.scm 98 */
							unsigned char BgL_arg1446z00_1484;

							{	/* Match/normalize.scm 98 */
								obj_t BgL_arg1447z00_1485;

								BgL_arg1447z00_1485 = SYMBOL_TO_STRING(BgL_ez00_7);
								BgL_arg1446z00_1484 = STRING_REF(BgL_arg1447z00_1485, 0L);
							}
							return (BgL_arg1446z00_1484 == ((unsigned char) '^'));
						}
					else
						{	/* Match/normalize.scm 97 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/normalize.scm 96 */
					return ((bool_t) 0);
				}
		}

	}



/* normalize-pattern */
	BGL_EXPORTED_DEF obj_t
		BGl_normaliza7ezd2patternz75zz__match_normaliza7eza7(obj_t BgL_ez00_13)
	{
		{	/* Match/normalize.scm 123 */
			{	/* Match/normalize.scm 124 */
				obj_t BgL_fun1461z00_2652;

				BgL_fun1461z00_2652 =
					BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7(BgL_ez00_13);
				return
					BGL_PROCEDURE_CALL2(BgL_fun1461z00_2652,
					BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7,
					BGl_proc2262z00zz__match_normaliza7eza7);
			}
		}

	}



/* &normalize-pattern */
	obj_t BGl_z62normaliza7ezd2patternz17zz__match_normaliza7eza7(obj_t
		BgL_envz00_3495, obj_t BgL_ez00_3496)
	{
		{	/* Match/normalize.scm 123 */
			return
				BGl_normaliza7ezd2patternz75zz__match_normaliza7eza7(BgL_ez00_3496);
		}

	}



/* &<@anonymous:1462>2213 */
	obj_t BGl_z62zc3z04anonymousza31462ze32213ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3497, obj_t BgL_patternz00_3498, obj_t BgL_rrz00_3499)
	{
		{	/* Match/normalize.scm 126 */
			return BgL_patternz00_3498;
		}

	}



/* standardize-pattern */
	obj_t BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7(obj_t
		BgL_ez00_14)
	{
		{	/* Match/normalize.scm 136 */
			{	/* Match/normalize.scm 137 */
				obj_t BgL_g1052z00_1511;

				if (PAIRP(BgL_ez00_14))
					{	/* Match/normalize.scm 358 */
						obj_t BgL_arg1727z00_2657;

						BgL_arg1727z00_2657 = CAR(BgL_ez00_14);
						{	/* Match/normalize.scm 358 */
							obj_t BgL_rz00_2659;

							BgL_rz00_2659 =
								BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
							if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1727z00_2657, BgL_rz00_2659)))
								{	/* Match/normalize.scm 337 */
									BgL_g1052z00_1511 =
										CDR(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1727z00_2657, BgL_rz00_2659));
								}
							else
								{	/* Match/normalize.scm 337 */
									BgL_g1052z00_1511 = BFALSE;
								}
						}
					}
				else
					{	/* Match/normalize.scm 357 */
						BgL_g1052z00_1511 = BFALSE;
					}
				if (CBOOL(BgL_g1052z00_1511))
					{	/* Match/normalize.scm 137 */
						return apply(BgL_g1052z00_1511, CDR(((obj_t) BgL_ez00_14)));
					}
				else
					{	/* Match/normalize.scm 137 */
						if ((BgL_ez00_14 == BGl_symbol2263z00zz__match_normaliza7eza7))
							{	/* Match/normalize.scm 138 */
								return BGl_proc2275z00zz__match_normaliza7eza7;
							}
						else
							{	/* Match/normalize.scm 138 */
								if (BGl_termzd2variablezf3z21zz__match_normaliza7eza7
									(BgL_ez00_14))
									{	/* Match/normalize.scm 139 */
										BGL_TAIL return
											BGl_standardiza7ezd2termzd2variableza7zz__match_normaliza7eza7
											(BgL_ez00_14);
									}
								else
									{	/* Match/normalize.scm 139 */
										if (BGl_holezd2variablezf3z21zz__match_normaliza7eza7
											(BgL_ez00_14))
											{	/* Match/normalize.scm 140 */
												BGL_TAIL return
													BGl_standardiza7ezd2holezd2variableza7zz__match_normaliza7eza7
													(BgL_ez00_14);
											}
										else
											{	/* Match/normalize.scm 140 */
												if (VECTORP(BgL_ez00_14))
													{	/* Match/normalize.scm 141 */
														return
															BGl_standardiza7ezd2vectorz75zz__match_normaliza7eza7
															(BgL_ez00_14);
													}
												else
													{	/* Match/normalize.scm 141 */
														if (STRUCTP(BgL_ez00_14))
															{	/* Match/normalize.scm 142 */
																return
																	BGl_standardiza7ezd2structz75zz__match_normaliza7eza7
																	(BgL_ez00_14);
															}
														else
															{	/* Match/normalize.scm 142 */
																if (CBOOL(BGl_atomzf3zf3zz__match_s2cfunz00
																		(BgL_ez00_14)))
																	{	/* Match/normalize.scm 243 */
																		obj_t BgL_zc3z04anonymousza31572ze3z87_3500;

																		BgL_zc3z04anonymousza31572ze3z87_3500 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31572ze3ze5zz__match_normaliza7eza7,
																			(int) (2L), (int) (1L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31572ze3z87_3500,
																			(int) (0L), BgL_ez00_14);
																		return
																			BgL_zc3z04anonymousza31572ze3z87_3500;
																	}
																else
																	{	/* Match/normalize.scm 143 */
																		BGL_TAIL return
																			BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7
																			(BgL_ez00_14);
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &<@anonymous:1572> */
	obj_t BGl_z62zc3z04anonymousza31572ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3501, obj_t BgL_rz00_3503, obj_t BgL_cz00_3504)
	{
		{	/* Match/normalize.scm 243 */
			{	/* Match/normalize.scm 244 */
				obj_t BgL_ez00_3502;

				BgL_ez00_3502 = PROCEDURE_REF(BgL_envz00_3501, (int) (0L));
				{	/* Match/normalize.scm 244 */
					obj_t BgL_arg1573z00_3790;

					{	/* Match/normalize.scm 244 */
						obj_t BgL_arg1575z00_3791;

						BgL_arg1575z00_3791 = MAKE_YOUNG_PAIR(BgL_ez00_3502, BNIL);
						BgL_arg1573z00_3790 =
							MAKE_YOUNG_PAIR(BGl_symbol2246z00zz__match_normaliza7eza7,
							BgL_arg1575z00_3791);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3504, BgL_arg1573z00_3790,
						BgL_rz00_3503);
				}
			}
		}

	}



/* standardize-patterns */
	obj_t BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7(obj_t
		BgL_eza2za2_15)
	{
		{	/* Match/normalize.scm 154 */
			if (PAIRP(BgL_eza2za2_15))
				{	/* Match/normalize.scm 156 */
					obj_t BgL_g1054z00_1520;

					{	/* Match/normalize.scm 358 */
						obj_t BgL_arg1727z00_2672;

						BgL_arg1727z00_2672 = CAR(BgL_eza2za2_15);
						{	/* Match/normalize.scm 358 */
							obj_t BgL_rz00_2674;

							BgL_rz00_2674 =
								BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
							if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1727z00_2672, BgL_rz00_2674)))
								{	/* Match/normalize.scm 337 */
									BgL_g1054z00_1520 =
										CDR(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1727z00_2672, BgL_rz00_2674));
								}
							else
								{	/* Match/normalize.scm 337 */
									BgL_g1054z00_1520 = BFALSE;
								}
						}
					}
					if (CBOOL(BgL_g1054z00_1520))
						{	/* Match/normalize.scm 156 */
							return apply(BgL_g1054z00_1520, CDR(BgL_eza2za2_15));
						}
					else
						{	/* Match/normalize.scm 156 */
							if (
								(CAR(BgL_eza2za2_15) ==
									BGl_symbol2265z00zz__match_normaliza7eza7))
								{	/* Match/normalize.scm 157 */
									BGL_TAIL return
										BGl_standardiza7ezd2anyz75zz__match_normaliza7eza7(CDR
										(BgL_eza2za2_15));
								}
							else
								{	/* Match/normalize.scm 157 */
									if (
										(CAR(BgL_eza2za2_15) ==
											BGl_symbol2267z00zz__match_normaliza7eza7))
										{	/* Match/normalize.scm 158 */
											BGL_TAIL return
												BGl_standardiza7ezd2lispishzd2anyza7zz__match_normaliza7eza7
												(CDR(BgL_eza2za2_15));
										}
									else
										{	/* Match/normalize.scm 158 */
											if (BGl_lispishzd2segmentzd2variablezf3zf3zz__match_normaliza7eza7(CAR(BgL_eza2za2_15)))
												{	/* Match/normalize.scm 159 */
													return
														BGl_standardiza7ezd2lispishzd2segmentzd2variablez75zz__match_normaliza7eza7
														(CAR(BgL_eza2za2_15), CDR(BgL_eza2za2_15));
												}
											else
												{	/* Match/normalize.scm 159 */
													if (BGl_segmentzd2variablezf3z21zz__match_normaliza7eza7(CAR(BgL_eza2za2_15)))
														{	/* Match/normalize.scm 161 */
															return
																BGl_standardiza7ezd2segmentzd2variableza7zz__match_normaliza7eza7
																(CAR(BgL_eza2za2_15), CDR(BgL_eza2za2_15));
														}
													else
														{	/* Match/normalize.scm 161 */
															if (BGl_treezd2variablezf3z21zz__match_normaliza7eza7(CAR(BgL_eza2za2_15)))
																{	/* Match/normalize.scm 163 */
																	return
																		BGl_standardiza7ezd2treezd2variableza7zz__match_normaliza7eza7
																		(CAR(BgL_eza2za2_15),
																		CAR(CDR(BgL_eza2za2_15)),
																		CAR(CDR(CDR(BgL_eza2za2_15))));
																}
															else
																{	/* Match/normalize.scm 163 */
																	return
																		BGl_standardiza7ezd2consz75zz__match_normaliza7eza7
																		(CAR(BgL_eza2za2_15), CDR(BgL_eza2za2_15));
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Match/normalize.scm 243 */
					obj_t BgL_zc3z04anonymousza31572ze3z87_3505;

					BgL_zc3z04anonymousza31572ze3z87_3505 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31572ze32214ze5zz__match_normaliza7eza7,
						(int) (2L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31572ze3z87_3505, (int) (0L),
						BgL_eza2za2_15);
					return BgL_zc3z04anonymousza31572ze3z87_3505;
				}
		}

	}



/* &<@anonymous:1572>2214 */
	obj_t BGl_z62zc3z04anonymousza31572ze32214ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3506, obj_t BgL_rz00_3508, obj_t BgL_cz00_3509)
	{
		{	/* Match/normalize.scm 243 */
			{	/* Match/normalize.scm 244 */
				obj_t BgL_eza2za2_3507;

				BgL_eza2za2_3507 = PROCEDURE_REF(BgL_envz00_3506, (int) (0L));
				{	/* Match/normalize.scm 244 */
					obj_t BgL_arg1573z00_3792;

					{	/* Match/normalize.scm 244 */
						obj_t BgL_arg1575z00_3793;

						BgL_arg1575z00_3793 = MAKE_YOUNG_PAIR(BgL_eza2za2_3507, BNIL);
						BgL_arg1573z00_3792 =
							MAKE_YOUNG_PAIR(BGl_symbol2246z00zz__match_normaliza7eza7,
							BgL_arg1575z00_3793);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3509, BgL_arg1573z00_3792,
						BgL_rz00_3508);
				}
			}
		}

	}



/* standardize-repetition */
	obj_t BGl_standardiza7ezd2repetitionz75zz__match_normaliza7eza7(obj_t
		BgL_ez00_16, obj_t BgL_eza2za2_17)
	{
		{	/* Match/normalize.scm 168 */
			{	/* Match/normalize.scm 169 */
				obj_t BgL_zc3z04anonymousza31500ze3z87_3512;

				BgL_zc3z04anonymousza31500ze3z87_3512 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31500ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31500ze3z87_3512, (int) (0L),
					BgL_ez00_16);
				PROCEDURE_SET(BgL_zc3z04anonymousza31500ze3z87_3512, (int) (1L),
					BgL_eza2za2_17);
				return BgL_zc3z04anonymousza31500ze3z87_3512;
			}
		}

	}



/* &<@anonymous:1500> */
	obj_t BGl_z62zc3z04anonymousza31500ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3513, obj_t BgL_rz00_3516, obj_t BgL_cz00_3517)
	{
		{	/* Match/normalize.scm 169 */
			{	/* Match/normalize.scm 170 */
				obj_t BgL_ez00_3514;
				obj_t BgL_eza2za2_3515;

				BgL_ez00_3514 = PROCEDURE_REF(BgL_envz00_3513, (int) (0L));
				BgL_eza2za2_3515 = PROCEDURE_REF(BgL_envz00_3513, (int) (1L));
				{	/* Match/normalize.scm 170 */
					obj_t BgL_fun1502z00_3794;

					BgL_fun1502z00_3794 =
						BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
						(BgL_ez00_3514);
					{	/* Match/normalize.scm 173 */
						obj_t BgL_zc3z04anonymousza31503ze3z87_3795;

						BgL_zc3z04anonymousza31503ze3z87_3795 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31503ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31503ze3z87_3795, (int) (0L),
							BgL_eza2za2_3515);
						PROCEDURE_SET(BgL_zc3z04anonymousza31503ze3z87_3795, (int) (1L),
							BgL_cz00_3517);
						return BGL_PROCEDURE_CALL2(BgL_fun1502z00_3794, BgL_rz00_3516,
							BgL_zc3z04anonymousza31503ze3z87_3795);
					}
				}
			}
		}

	}



/* &<@anonymous:1503> */
	obj_t BGl_z62zc3z04anonymousza31503ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3518, obj_t BgL_fz00_3521, obj_t BgL_rrz00_3522)
	{
		{	/* Match/normalize.scm 172 */
			{	/* Match/normalize.scm 173 */
				obj_t BgL_eza2za2_3519;
				obj_t BgL_cz00_3520;

				BgL_eza2za2_3519 = PROCEDURE_REF(BgL_envz00_3518, (int) (0L));
				BgL_cz00_3520 = PROCEDURE_REF(BgL_envz00_3518, (int) (1L));
				{	/* Match/normalize.scm 173 */
					obj_t BgL_fun1505z00_3796;

					BgL_fun1505z00_3796 =
						BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7
						(BgL_eza2za2_3519);
					{	/* Match/normalize.scm 176 */
						obj_t BgL_zc3z04anonymousza31506ze3z87_3797;

						BgL_zc3z04anonymousza31506ze3z87_3797 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31506ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31506ze3z87_3797, (int) (0L),
							BgL_fz00_3521);
						PROCEDURE_SET(BgL_zc3z04anonymousza31506ze3z87_3797, (int) (1L),
							BgL_cz00_3520);
						return BGL_PROCEDURE_CALL2(BgL_fun1505z00_3796, BgL_rrz00_3522,
							BgL_zc3z04anonymousza31506ze3z87_3797);
					}
				}
			}
		}

	}



/* &<@anonymous:1506> */
	obj_t BGl_z62zc3z04anonymousza31506ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3523, obj_t BgL_fza2za2_3526, obj_t BgL_rrrz00_3527)
	{
		{	/* Match/normalize.scm 175 */
			{	/* Match/normalize.scm 176 */
				obj_t BgL_fz00_3524;
				obj_t BgL_cz00_3525;

				BgL_fz00_3524 = PROCEDURE_REF(BgL_envz00_3523, (int) (0L));
				BgL_cz00_3525 = PROCEDURE_REF(BgL_envz00_3523, (int) (1L));
				{	/* Match/normalize.scm 176 */
					obj_t BgL_labelz00_3798;

					BgL_labelz00_3798 =
						BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
						BGl_string2269z00zz__match_normaliza7eza7);
					{	/* Match/normalize.scm 178 */
						obj_t BgL_arg1507z00_3799;

						{	/* Match/normalize.scm 178 */
							obj_t BgL_arg1508z00_3800;

							{	/* Match/normalize.scm 178 */
								obj_t BgL_arg1509z00_3801;

								{	/* Match/normalize.scm 178 */
									obj_t BgL_arg1510z00_3802;
									obj_t BgL_arg1511z00_3803;

									{	/* Match/normalize.scm 178 */
										obj_t BgL_arg1513z00_3804;

										{	/* Match/normalize.scm 178 */
											obj_t BgL_arg1514z00_3805;

											{	/* Match/normalize.scm 178 */
												obj_t BgL_arg1516z00_3806;

												{	/* Match/normalize.scm 178 */
													obj_t BgL_arg1517z00_3807;

													{	/* Match/normalize.scm 178 */
														obj_t BgL_arg1521z00_3808;

														{	/* Match/normalize.scm 178 */
															obj_t BgL_arg1522z00_3809;

															BgL_arg1522z00_3809 =
																BGL_PROCEDURE_CALL1
																(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
																BGl_string2270z00zz__match_normaliza7eza7);
															BgL_arg1521z00_3808 =
																MAKE_YOUNG_PAIR(BgL_arg1522z00_3809, BNIL);
														}
														BgL_arg1517z00_3807 =
															MAKE_YOUNG_PAIR(BgL_labelz00_3798,
															BgL_arg1521z00_3808);
													}
													BgL_arg1516z00_3806 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2271z00zz__match_normaliza7eza7,
														BgL_arg1517z00_3807);
												}
												BgL_arg1514z00_3805 =
													MAKE_YOUNG_PAIR(BgL_arg1516z00_3806, BNIL);
											}
											BgL_arg1513z00_3804 =
												MAKE_YOUNG_PAIR(BgL_fz00_3524, BgL_arg1514z00_3805);
										}
										BgL_arg1510z00_3802 =
											MAKE_YOUNG_PAIR(BGl_symbol2260z00zz__match_normaliza7eza7,
											BgL_arg1513z00_3804);
									}
									BgL_arg1511z00_3803 = MAKE_YOUNG_PAIR(BgL_fza2za2_3526, BNIL);
									BgL_arg1509z00_3801 =
										MAKE_YOUNG_PAIR(BgL_arg1510z00_3802, BgL_arg1511z00_3803);
								}
								BgL_arg1508z00_3800 =
									MAKE_YOUNG_PAIR(BgL_labelz00_3798, BgL_arg1509z00_3801);
							}
							BgL_arg1507z00_3799 =
								MAKE_YOUNG_PAIR(BGl_symbol2273z00zz__match_normaliza7eza7,
								BgL_arg1508z00_3800);
						}
						return
							BGL_PROCEDURE_CALL2(BgL_cz00_3525, BgL_arg1507z00_3799,
							BgL_rrrz00_3527);
					}
				}
			}
		}

	}



/* &<@anonymous:1523> */
	obj_t BGl_z62zc3z04anonymousza31523ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3529, obj_t BgL_rz00_3530, obj_t BgL_cz00_3531)
	{
		{	/* Match/normalize.scm 183 */
			{	/* Match/normalize.scm 184 */
				obj_t BgL_arg1524z00_3810;

				BgL_arg1524z00_3810 =
					MAKE_YOUNG_PAIR(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
				return
					BGL_PROCEDURE_CALL2(BgL_cz00_3531, BgL_arg1524z00_3810,
					BgL_rz00_3530);
			}
		}

	}



/* standardize-cons */
	obj_t BGl_standardiza7ezd2consz75zz__match_normaliza7eza7(obj_t BgL_fz00_18,
		obj_t BgL_fza2za2_19)
	{
		{	/* Match/normalize.scm 186 */
			{	/* Match/normalize.scm 187 */
				bool_t BgL_test2456z00_4758;

				if (PAIRP(BgL_fza2za2_19))
					{	/* Match/normalize.scm 187 */
						BgL_test2456z00_4758 =
							(CAR(BgL_fza2za2_19) ==
							BGl_symbol2276z00zz__match_normaliza7eza7);
					}
				else
					{	/* Match/normalize.scm 187 */
						BgL_test2456z00_4758 = ((bool_t) 0);
					}
				if (BgL_test2456z00_4758)
					{	/* Match/normalize.scm 187 */
						BGL_TAIL return
							BGl_standardiza7ezd2repetitionz75zz__match_normaliza7eza7
							(BgL_fz00_18, CDR(BgL_fza2za2_19));
					}
				else
					{	/* Match/normalize.scm 187 */
						if (CBOOL
							(BGl_z62zc3z04anonymousza31531ze3ze5zz__match_normaliza7eza7
								(BGl_za2preferzd2xconsza2zd2zz__match_normaliza7eza7,
									BGl_symbol2278z00zz__match_normaliza7eza7)))
							{	/* Match/normalize.scm 189 */
								BGL_TAIL return
									BGl_standardiza7ezd2realzd2xconsza7zz__match_normaliza7eza7
									(BgL_fz00_18, BgL_fza2za2_19);
							}
						else
							{	/* Match/normalize.scm 189 */
								BGL_TAIL return
									BGl_standardiza7ezd2realzd2consza7zz__match_normaliza7eza7
									(BgL_fz00_18, BgL_fza2za2_19);
							}
					}
			}
		}

	}



/* make-toggle */
	obj_t BGl_makezd2togglezd2zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 193 */
			{	/* Match/normalize.scm 194 */
				obj_t BgL_valuez00_3538;

				BgL_valuez00_3538 = MAKE_CELL(BFALSE);
				{	/* Match/normalize.scm 195 */
					obj_t BgL_zc3z04anonymousza31531ze3z87_3532;

					{
						int BgL_tmpz00_4773;

						BgL_tmpz00_4773 = (int) (1L);
						BgL_zc3z04anonymousza31531ze3z87_3532 =
							MAKE_EL_PROCEDURE(BgL_tmpz00_4773);
					}
					PROCEDURE_EL_SET(BgL_zc3z04anonymousza31531ze3z87_3532,
						(int) (0L), ((obj_t) BgL_valuez00_3538));
					return BgL_zc3z04anonymousza31531ze3z87_3532;
				}
			}
		}

	}



/* &<@anonymous:1531> */
	obj_t BGl_z62zc3z04anonymousza31531ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3533, obj_t BgL_msgz00_3535)
	{
		{	/* Match/normalize.scm 195 */
			{	/* Match/normalize.scm 196 */
				obj_t BgL_valuez00_3534;

				BgL_valuez00_3534 = PROCEDURE_EL_REF(BgL_envz00_3533, (int) (0L));
				if ((BgL_msgz00_3535 == BGl_symbol2278z00zz__match_normaliza7eza7))
					{	/* Match/normalize.scm 196 */
						return CELL_REF(BgL_valuez00_3534);
					}
				else
					{	/* Match/normalize.scm 196 */
						if ((BgL_msgz00_3535 == BGl_symbol2280z00zz__match_normaliza7eza7))
							{	/* Match/normalize.scm 198 */
								obj_t BgL_auxz00_3812;

								BgL_auxz00_3812 = BTRUE;
								return CELL_SET(BgL_valuez00_3534, BgL_auxz00_3812);
							}
						else
							{	/* Match/normalize.scm 196 */
								if (
									(BgL_msgz00_3535 ==
										BGl_symbol2282z00zz__match_normaliza7eza7))
									{	/* Match/normalize.scm 199 */
										obj_t BgL_auxz00_3813;

										BgL_auxz00_3813 = BFALSE;
										return CELL_SET(BgL_valuez00_3534, BgL_auxz00_3813);
									}
								else
									{	/* Match/normalize.scm 196 */
										return BUNSPEC;
									}
							}
					}
			}
		}

	}



/* standardize-real-cons */
	obj_t BGl_standardiza7ezd2realzd2consza7zz__match_normaliza7eza7(obj_t
		BgL_fz00_20, obj_t BgL_fza2za2_21)
	{
		{	/* Match/normalize.scm 203 */
			{	/* Match/normalize.scm 204 */
				obj_t BgL_zc3z04anonymousza31535ze3z87_3542;

				BgL_zc3z04anonymousza31535ze3z87_3542 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31535ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31535ze3z87_3542, (int) (0L),
					BgL_fz00_20);
				PROCEDURE_SET(BgL_zc3z04anonymousza31535ze3z87_3542, (int) (1L),
					BgL_fza2za2_21);
				return BgL_zc3z04anonymousza31535ze3z87_3542;
			}
		}

	}



/* &<@anonymous:1535> */
	obj_t BGl_z62zc3z04anonymousza31535ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3543, obj_t BgL_rz00_3546, obj_t BgL_cz00_3547)
	{
		{	/* Match/normalize.scm 204 */
			{	/* Match/normalize.scm 205 */
				obj_t BgL_fz00_3544;
				obj_t BgL_fza2za2_3545;

				BgL_fz00_3544 = PROCEDURE_REF(BgL_envz00_3543, (int) (0L));
				BgL_fza2za2_3545 = PROCEDURE_REF(BgL_envz00_3543, (int) (1L));
				{	/* Match/normalize.scm 205 */
					obj_t BgL_fun1537z00_3814;

					BgL_fun1537z00_3814 =
						BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
						(BgL_fz00_3544);
					{	/* Match/normalize.scm 209 */
						obj_t BgL_zc3z04anonymousza31538ze3z87_3815;

						BgL_zc3z04anonymousza31538ze3z87_3815 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31538ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31538ze3z87_3815, (int) (0L),
							BgL_fza2za2_3545);
						PROCEDURE_SET(BgL_zc3z04anonymousza31538ze3z87_3815, (int) (1L),
							BgL_cz00_3547);
						return BGL_PROCEDURE_CALL2(BgL_fun1537z00_3814, BgL_rz00_3546,
							BgL_zc3z04anonymousza31538ze3z87_3815);
					}
				}
			}
		}

	}



/* &<@anonymous:1538> */
	obj_t BGl_z62zc3z04anonymousza31538ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3548, obj_t BgL_pattern1z00_3551, obj_t BgL_rrz00_3552)
	{
		{	/* Match/normalize.scm 207 */
			{	/* Match/normalize.scm 209 */
				obj_t BgL_fza2za2_3549;
				obj_t BgL_cz00_3550;

				BgL_fza2za2_3549 = PROCEDURE_REF(BgL_envz00_3548, (int) (0L));
				BgL_cz00_3550 = PROCEDURE_REF(BgL_envz00_3548, (int) (1L));
				{	/* Match/normalize.scm 209 */
					obj_t BgL_fun1540z00_3816;

					BgL_fun1540z00_3816 =
						BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
						(BgL_fza2za2_3549);
					{	/* Match/normalize.scm 212 */
						obj_t BgL_zc3z04anonymousza31541ze3z87_3817;

						BgL_zc3z04anonymousza31541ze3z87_3817 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31541ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31541ze3z87_3817, (int) (0L),
							BgL_pattern1z00_3551);
						PROCEDURE_SET(BgL_zc3z04anonymousza31541ze3z87_3817, (int) (1L),
							BgL_cz00_3550);
						return BGL_PROCEDURE_CALL2(BgL_fun1540z00_3816, BgL_rrz00_3552,
							BgL_zc3z04anonymousza31541ze3z87_3817);
					}
				}
			}
		}

	}



/* &<@anonymous:1541> */
	obj_t BGl_z62zc3z04anonymousza31541ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3553, obj_t BgL_pattern2z00_3556, obj_t BgL_rrrz00_3557)
	{
		{	/* Match/normalize.scm 211 */
			{	/* Match/normalize.scm 212 */
				obj_t BgL_pattern1z00_3554;
				obj_t BgL_cz00_3555;

				BgL_pattern1z00_3554 = PROCEDURE_REF(BgL_envz00_3553, (int) (0L));
				BgL_cz00_3555 = PROCEDURE_REF(BgL_envz00_3553, (int) (1L));
				{	/* Match/normalize.scm 212 */
					obj_t BgL_arg1543z00_3818;

					{	/* Match/normalize.scm 212 */
						obj_t BgL_arg1544z00_3819;

						{	/* Match/normalize.scm 212 */
							obj_t BgL_arg1546z00_3820;

							BgL_arg1546z00_3820 = MAKE_YOUNG_PAIR(BgL_pattern2z00_3556, BNIL);
							BgL_arg1544z00_3819 =
								MAKE_YOUNG_PAIR(BgL_pattern1z00_3554, BgL_arg1546z00_3820);
						}
						BgL_arg1543z00_3818 =
							MAKE_YOUNG_PAIR(BGl_symbol2260z00zz__match_normaliza7eza7,
							BgL_arg1544z00_3819);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3555, BgL_arg1543z00_3818,
						BgL_rrrz00_3557);
				}
			}
		}

	}



/* standardize-real-xcons */
	obj_t BGl_standardiza7ezd2realzd2xconsza7zz__match_normaliza7eza7(obj_t
		BgL_fz00_22, obj_t BgL_fza2za2_23)
	{
		{	/* Match/normalize.scm 214 */
			{	/* Match/normalize.scm 215 */
				obj_t BgL_zc3z04anonymousza31547ze3z87_3560;

				BgL_zc3z04anonymousza31547ze3z87_3560 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31547ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31547ze3z87_3560, (int) (0L),
					BgL_fza2za2_23);
				PROCEDURE_SET(BgL_zc3z04anonymousza31547ze3z87_3560, (int) (1L),
					BgL_fz00_22);
				return BgL_zc3z04anonymousza31547ze3z87_3560;
			}
		}

	}



/* &<@anonymous:1547> */
	obj_t BGl_z62zc3z04anonymousza31547ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3561, obj_t BgL_rz00_3564, obj_t BgL_cz00_3565)
	{
		{	/* Match/normalize.scm 215 */
			{	/* Match/normalize.scm 216 */
				obj_t BgL_fza2za2_3562;
				obj_t BgL_fz00_3563;

				BgL_fza2za2_3562 = PROCEDURE_REF(BgL_envz00_3561, (int) (0L));
				BgL_fz00_3563 = PROCEDURE_REF(BgL_envz00_3561, (int) (1L));
				{	/* Match/normalize.scm 216 */
					obj_t BgL_fun1550z00_3821;

					BgL_fun1550z00_3821 =
						BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7
						(BgL_fza2za2_3562);
					{	/* Match/normalize.scm 219 */
						obj_t BgL_zc3z04anonymousza31551ze3z87_3822;

						BgL_zc3z04anonymousza31551ze3z87_3822 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31551ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31551ze3z87_3822, (int) (0L),
							BgL_fz00_3563);
						PROCEDURE_SET(BgL_zc3z04anonymousza31551ze3z87_3822, (int) (1L),
							BgL_cz00_3565);
						return BGL_PROCEDURE_CALL2(BgL_fun1550z00_3821, BgL_rz00_3564,
							BgL_zc3z04anonymousza31551ze3z87_3822);
					}
				}
			}
		}

	}



/* &<@anonymous:1551> */
	obj_t BGl_z62zc3z04anonymousza31551ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3566, obj_t BgL_pattern1z00_3569, obj_t BgL_rrz00_3570)
	{
		{	/* Match/normalize.scm 218 */
			{	/* Match/normalize.scm 219 */
				obj_t BgL_fz00_3567;
				obj_t BgL_cz00_3568;

				BgL_fz00_3567 = PROCEDURE_REF(BgL_envz00_3566, (int) (0L));
				BgL_cz00_3568 = PROCEDURE_REF(BgL_envz00_3566, (int) (1L));
				{	/* Match/normalize.scm 219 */
					obj_t BgL_fun1553z00_3823;

					BgL_fun1553z00_3823 =
						BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
						(BgL_fz00_3567);
					{	/* Match/normalize.scm 222 */
						obj_t BgL_zc3z04anonymousza31554ze3z87_3824;

						BgL_zc3z04anonymousza31554ze3z87_3824 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31554ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31554ze3z87_3824, (int) (0L),
							BgL_pattern1z00_3569);
						PROCEDURE_SET(BgL_zc3z04anonymousza31554ze3z87_3824, (int) (1L),
							BgL_cz00_3568);
						return BGL_PROCEDURE_CALL2(BgL_fun1553z00_3823, BgL_rrz00_3570,
							BgL_zc3z04anonymousza31554ze3z87_3824);
					}
				}
			}
		}

	}



/* &<@anonymous:1554> */
	obj_t BGl_z62zc3z04anonymousza31554ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3571, obj_t BgL_pattern2z00_3574, obj_t BgL_rrrz00_3575)
	{
		{	/* Match/normalize.scm 221 */
			{	/* Match/normalize.scm 222 */
				obj_t BgL_pattern1z00_3572;
				obj_t BgL_cz00_3573;

				BgL_pattern1z00_3572 = PROCEDURE_REF(BgL_envz00_3571, (int) (0L));
				BgL_cz00_3573 = PROCEDURE_REF(BgL_envz00_3571, (int) (1L));
				{	/* Match/normalize.scm 222 */
					obj_t BgL_arg1555z00_3825;

					{	/* Match/normalize.scm 222 */
						obj_t BgL_arg1556z00_3826;

						{	/* Match/normalize.scm 222 */
							obj_t BgL_arg1557z00_3827;

							BgL_arg1557z00_3827 = MAKE_YOUNG_PAIR(BgL_pattern1z00_3572, BNIL);
							BgL_arg1556z00_3826 =
								MAKE_YOUNG_PAIR(BgL_pattern2z00_3574, BgL_arg1557z00_3827);
						}
						BgL_arg1555z00_3825 =
							MAKE_YOUNG_PAIR(BGl_symbol2284z00zz__match_normaliza7eza7,
							BgL_arg1556z00_3826);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3573, BgL_arg1555z00_3825,
						BgL_rrrz00_3575);
				}
			}
		}

	}



/* standardize-term-variable */
	obj_t BGl_standardiza7ezd2termzd2variableza7zz__match_normaliza7eza7(obj_t
		BgL_ez00_24)
	{
		{	/* Match/normalize.scm 224 */
			{	/* Match/normalize.scm 225 */
				obj_t BgL_zc3z04anonymousza31558ze3z87_3576;

				BgL_zc3z04anonymousza31558ze3z87_3576 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31558ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31558ze3z87_3576, (int) (0L),
					BgL_ez00_24);
				return BgL_zc3z04anonymousza31558ze3z87_3576;
			}
		}

	}



/* &<@anonymous:1558> */
	obj_t BGl_z62zc3z04anonymousza31558ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3577, obj_t BgL_rz00_3579, obj_t BgL_cz00_3580)
	{
		{	/* Match/normalize.scm 225 */
			{	/* Match/normalize.scm 226 */
				obj_t BgL_ez00_3578;

				BgL_ez00_3578 = PROCEDURE_REF(BgL_envz00_3577, (int) (0L));
				{	/* Match/normalize.scm 226 */
					obj_t BgL_namez00_3828;

					{	/* Match/normalize.scm 101 */
						obj_t BgL_sz00_3829;

						BgL_sz00_3829 = SYMBOL_TO_STRING(((obj_t) BgL_ez00_3578));
						BgL_namez00_3828 =
							bstring_to_symbol(c_substring(BgL_sz00_3829, 1L,
								STRING_LENGTH(BgL_sz00_3829)));
					}
					{	/* Match/normalize.scm 227 */
						obj_t BgL_arg1559z00_3830;

						{	/* Match/normalize.scm 227 */
							obj_t BgL_arg1561z00_3831;

							BgL_arg1561z00_3831 = MAKE_YOUNG_PAIR(BgL_namez00_3828, BNIL);
							BgL_arg1559z00_3830 =
								MAKE_YOUNG_PAIR(BGl_symbol2286z00zz__match_normaliza7eza7,
								BgL_arg1561z00_3831);
						}
						return
							BGL_PROCEDURE_CALL2(BgL_cz00_3580, BgL_arg1559z00_3830,
							BgL_rz00_3579);
					}
				}
			}
		}

	}



/* standardize-hole-variable */
	obj_t BGl_standardiza7ezd2holezd2variableza7zz__match_normaliza7eza7(obj_t
		BgL_ez00_25)
	{
		{	/* Match/normalize.scm 237 */
			{	/* Match/normalize.scm 238 */
				obj_t BgL_zc3z04anonymousza31562ze3z87_3581;

				BgL_zc3z04anonymousza31562ze3z87_3581 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31562ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31562ze3z87_3581, (int) (0L),
					BgL_ez00_25);
				return BgL_zc3z04anonymousza31562ze3z87_3581;
			}
		}

	}



/* &<@anonymous:1562> */
	obj_t BGl_z62zc3z04anonymousza31562ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3582, obj_t BgL_rz00_3584, obj_t BgL_cz00_3585)
	{
		{	/* Match/normalize.scm 238 */
			{	/* Match/normalize.scm 239 */
				obj_t BgL_ez00_3583;

				BgL_ez00_3583 = PROCEDURE_REF(BgL_envz00_3582, (int) (0L));
				{	/* Match/normalize.scm 239 */
					obj_t BgL_namez00_3832;

					{	/* Match/normalize.scm 113 */
						obj_t BgL_sz00_3833;

						BgL_sz00_3833 = SYMBOL_TO_STRING(((obj_t) BgL_ez00_3583));
						BgL_namez00_3832 =
							bstring_to_symbol(c_substring(BgL_sz00_3833, 1L,
								STRING_LENGTH(BgL_sz00_3833)));
					}
					{	/* Match/normalize.scm 240 */
						obj_t BgL_arg1564z00_3834;

						{	/* Match/normalize.scm 240 */
							obj_t BgL_arg1565z00_3835;

							{	/* Match/normalize.scm 240 */
								obj_t BgL_arg1567z00_3836;

								{	/* Match/normalize.scm 240 */
									obj_t BgL_arg1571z00_3837;

									BgL_arg1571z00_3837 =
										BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
										BGl_string2270z00zz__match_normaliza7eza7);
									BgL_arg1567z00_3836 =
										MAKE_YOUNG_PAIR(BgL_arg1571z00_3837, BNIL);
								}
								BgL_arg1565z00_3835 =
									MAKE_YOUNG_PAIR(BgL_namez00_3832, BgL_arg1567z00_3836);
							}
							BgL_arg1564z00_3834 =
								MAKE_YOUNG_PAIR(BGl_symbol2271z00zz__match_normaliza7eza7,
								BgL_arg1565z00_3835);
						}
						return
							BGL_PROCEDURE_CALL2(BgL_cz00_3585, BgL_arg1564z00_3834,
							BgL_rz00_3584);
					}
				}
			}
		}

	}



/* standardize-segment-variable */
	obj_t BGl_standardiza7ezd2segmentzd2variableza7zz__match_normaliza7eza7(obj_t
		BgL_ez00_27, obj_t BgL_fza2za2_28)
	{
		{	/* Match/normalize.scm 246 */
			{	/* Match/normalize.scm 247 */
				obj_t BgL_zc3z04anonymousza31576ze3z87_3588;

				BgL_zc3z04anonymousza31576ze3z87_3588 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31576ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31576ze3z87_3588, (int) (0L),
					BgL_ez00_27);
				PROCEDURE_SET(BgL_zc3z04anonymousza31576ze3z87_3588, (int) (1L),
					BgL_fza2za2_28);
				return BgL_zc3z04anonymousza31576ze3z87_3588;
			}
		}

	}



/* &<@anonymous:1576> */
	obj_t BGl_z62zc3z04anonymousza31576ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3589, obj_t BgL_rz00_3592, obj_t BgL_cz00_3593)
	{
		{	/* Match/normalize.scm 247 */
			{	/* Match/normalize.scm 248 */
				obj_t BgL_ez00_3590;
				obj_t BgL_fza2za2_3591;

				BgL_ez00_3590 = PROCEDURE_REF(BgL_envz00_3589, (int) (0L));
				BgL_fza2za2_3591 = PROCEDURE_REF(BgL_envz00_3589, (int) (1L));
				{	/* Match/normalize.scm 248 */
					obj_t BgL_namez00_3838;

					{	/* Match/normalize.scm 105 */
						obj_t BgL_sz00_3839;

						BgL_sz00_3839 = SYMBOL_TO_STRING(((obj_t) BgL_ez00_3590));
						BgL_namez00_3838 =
							bstring_to_symbol(c_substring(BgL_sz00_3839, 2L,
								STRING_LENGTH(BgL_sz00_3839)));
					}
					{	/* Match/normalize.scm 249 */
						bool_t BgL_test2462z00_4952;

						{	/* Match/normalize.scm 249 */
							obj_t BgL_arg1618z00_3840;

							if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_namez00_3838, BgL_rz00_3592)))
								{	/* Match/normalize.scm 337 */
									BgL_arg1618z00_3840 =
										CDR(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_namez00_3838, BgL_rz00_3592));
								}
							else
								{	/* Match/normalize.scm 337 */
									BgL_arg1618z00_3840 = BFALSE;
								}
							BgL_test2462z00_4952 =
								(BgL_arg1618z00_3840 ==
								BGl_unboundzd2patternzd2zz__match_normaliza7eza7);
						}
						if (BgL_test2462z00_4952)
							{	/* Match/normalize.scm 250 */
								obj_t BgL_fun1583z00_3841;

								BgL_fun1583z00_3841 =
									BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7
									(BgL_fza2za2_3591);
								{	/* Match/normalize.scm 251 */
									obj_t BgL_arg1580z00_3842;

									{	/* Match/normalize.scm 251 */
										obj_t BgL_imz00_3843;

										BgL_imz00_3843 = BGl_symbol2288z00zz__match_normaliza7eza7;
										{	/* Match/normalize.scm 334 */
											obj_t BgL_arg1724z00_3844;

											BgL_arg1724z00_3844 =
												MAKE_YOUNG_PAIR(BgL_namez00_3838, BgL_imz00_3843);
											BgL_arg1580z00_3842 =
												MAKE_YOUNG_PAIR(BgL_arg1724z00_3844, BgL_rz00_3592);
										}
									}
									{	/* Match/normalize.scm 253 */
										obj_t BgL_zc3z04anonymousza31584ze3z87_3845;

										BgL_zc3z04anonymousza31584ze3z87_3845 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31584ze3ze5zz__match_normaliza7eza7,
											(int) (2L), (int) (2L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31584ze3z87_3845,
											(int) (0L), BgL_namez00_3838);
										PROCEDURE_SET(BgL_zc3z04anonymousza31584ze3z87_3845,
											(int) (1L), BgL_cz00_3593);
										return BGL_PROCEDURE_CALL2(BgL_fun1583z00_3841,
											BgL_arg1580z00_3842,
											BgL_zc3z04anonymousza31584ze3z87_3845);
									}
								}
							}
						else
							{	/* Match/normalize.scm 261 */
								obj_t BgL_fun1612z00_3846;

								BgL_fun1612z00_3846 =
									BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7
									(BgL_fza2za2_3591);
								{	/* Match/normalize.scm 264 */
									obj_t BgL_zc3z04anonymousza31613ze3z87_3847;

									BgL_zc3z04anonymousza31613ze3z87_3847 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31613ze3ze5zz__match_normaliza7eza7,
										(int) (2L), (int) (2L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31613ze3z87_3847,
										(int) (0L), BgL_namez00_3838);
									PROCEDURE_SET(BgL_zc3z04anonymousza31613ze3z87_3847,
										(int) (1L), BgL_cz00_3593);
									return BGL_PROCEDURE_CALL2(BgL_fun1612z00_3846, BgL_rz00_3592,
										BgL_zc3z04anonymousza31613ze3z87_3847);
								}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1584> */
	obj_t BGl_z62zc3z04anonymousza31584ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3594, obj_t BgL_patternz00_3597, obj_t BgL_rrz00_3598)
	{
		{	/* Match/normalize.scm 252 */
			{	/* Match/normalize.scm 253 */
				obj_t BgL_namez00_3595;
				obj_t BgL_cz00_3596;

				BgL_namez00_3595 = ((obj_t) PROCEDURE_REF(BgL_envz00_3594, (int) (0L)));
				BgL_cz00_3596 = PROCEDURE_REF(BgL_envz00_3594, (int) (1L));
				{	/* Match/normalize.scm 253 */
					obj_t BgL_labelz00_3848;

					BgL_labelz00_3848 =
						BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
						BGl_string2269z00zz__match_normaliza7eza7);
					{	/* Match/normalize.scm 257 */
						obj_t BgL_arg1585z00_3849;

						{	/* Match/normalize.scm 257 */
							obj_t BgL_arg1586z00_3850;

							{	/* Match/normalize.scm 257 */
								obj_t BgL_arg1587z00_3851;

								{	/* Match/normalize.scm 257 */
									obj_t BgL_arg1589z00_3852;
									obj_t BgL_arg1591z00_3853;

									{	/* Match/normalize.scm 257 */
										obj_t BgL_arg1593z00_3854;

										{	/* Match/normalize.scm 257 */
											obj_t BgL_arg1594z00_3855;

											{	/* Match/normalize.scm 257 */
												obj_t BgL_arg1595z00_3856;
												obj_t BgL_arg1598z00_3857;

												{	/* Match/normalize.scm 257 */
													obj_t BgL_arg1601z00_3858;

													{	/* Match/normalize.scm 257 */
														obj_t BgL_arg1602z00_3859;
														obj_t BgL_arg1603z00_3860;

														BgL_arg1602z00_3859 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
														{	/* Match/normalize.scm 257 */
															obj_t BgL_arg1605z00_3861;

															{	/* Match/normalize.scm 257 */
																obj_t BgL_arg1606z00_3862;

																{	/* Match/normalize.scm 257 */
																	obj_t BgL_arg1607z00_3863;

																	{	/* Match/normalize.scm 257 */
																		obj_t BgL_arg1608z00_3864;

																		BgL_arg1608z00_3864 =
																			BGL_PROCEDURE_CALL1
																			(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
																			BGl_string2270z00zz__match_normaliza7eza7);
																		BgL_arg1607z00_3863 =
																			MAKE_YOUNG_PAIR(BgL_arg1608z00_3864,
																			BNIL);
																	}
																	BgL_arg1606z00_3862 =
																		MAKE_YOUNG_PAIR(BgL_labelz00_3848,
																		BgL_arg1607z00_3863);
																}
																BgL_arg1605z00_3861 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2271z00zz__match_normaliza7eza7,
																	BgL_arg1606z00_3862);
															}
															BgL_arg1603z00_3860 =
																MAKE_YOUNG_PAIR(BgL_arg1605z00_3861, BNIL);
														}
														BgL_arg1601z00_3858 =
															MAKE_YOUNG_PAIR(BgL_arg1602z00_3859,
															BgL_arg1603z00_3860);
													}
													BgL_arg1595z00_3856 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2260z00zz__match_normaliza7eza7,
														BgL_arg1601z00_3858);
												}
												{	/* Match/normalize.scm 258 */
													obj_t BgL_arg1609z00_3865;

													{	/* Match/normalize.scm 258 */
														obj_t BgL_arg1610z00_3866;

														BgL_arg1610z00_3866 =
															MAKE_YOUNG_PAIR(BgL_namez00_3595, BNIL);
														BgL_arg1609z00_3865 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2290z00zz__match_normaliza7eza7,
															BgL_arg1610z00_3866);
													}
													BgL_arg1598z00_3857 =
														MAKE_YOUNG_PAIR(BgL_arg1609z00_3865, BNIL);
												}
												BgL_arg1594z00_3855 =
													MAKE_YOUNG_PAIR(BgL_arg1595z00_3856,
													BgL_arg1598z00_3857);
											}
											BgL_arg1593z00_3854 =
												MAKE_YOUNG_PAIR(BgL_labelz00_3848, BgL_arg1594z00_3855);
										}
										BgL_arg1589z00_3852 =
											MAKE_YOUNG_PAIR(BGl_symbol2292z00zz__match_normaliza7eza7,
											BgL_arg1593z00_3854);
									}
									BgL_arg1591z00_3853 =
										MAKE_YOUNG_PAIR(BgL_patternz00_3597, BNIL);
									BgL_arg1587z00_3851 =
										MAKE_YOUNG_PAIR(BgL_arg1589z00_3852, BgL_arg1591z00_3853);
								}
								BgL_arg1586z00_3850 =
									MAKE_YOUNG_PAIR(BgL_namez00_3595, BgL_arg1587z00_3851);
							}
							BgL_arg1585z00_3849 =
								MAKE_YOUNG_PAIR(BGl_symbol2294z00zz__match_normaliza7eza7,
								BgL_arg1586z00_3850);
						}
						return
							BGL_PROCEDURE_CALL2(BgL_cz00_3596, BgL_arg1585z00_3849,
							BgL_rrz00_3598);
					}
				}
			}
		}

	}



/* &<@anonymous:1613> */
	obj_t BGl_z62zc3z04anonymousza31613ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3599, obj_t BgL_patternz00_3602, obj_t BgL_rrz00_3603)
	{
		{	/* Match/normalize.scm 263 */
			{	/* Match/normalize.scm 264 */
				obj_t BgL_namez00_3600;
				obj_t BgL_cz00_3601;

				BgL_namez00_3600 = ((obj_t) PROCEDURE_REF(BgL_envz00_3599, (int) (0L)));
				BgL_cz00_3601 = PROCEDURE_REF(BgL_envz00_3599, (int) (1L));
				{	/* Match/normalize.scm 264 */
					obj_t BgL_arg1615z00_3867;

					{	/* Match/normalize.scm 264 */
						obj_t BgL_arg1616z00_3868;

						{	/* Match/normalize.scm 264 */
							obj_t BgL_arg1617z00_3869;

							BgL_arg1617z00_3869 = MAKE_YOUNG_PAIR(BgL_patternz00_3602, BNIL);
							BgL_arg1616z00_3868 =
								MAKE_YOUNG_PAIR(BgL_namez00_3600, BgL_arg1617z00_3869);
						}
						BgL_arg1615z00_3867 =
							MAKE_YOUNG_PAIR(BGl_symbol2296z00zz__match_normaliza7eza7,
							BgL_arg1616z00_3868);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3601, BgL_arg1615z00_3867,
						BgL_rrz00_3603);
				}
			}
		}

	}



/* standardize-tree-variable */
	obj_t BGl_standardiza7ezd2treezd2variableza7zz__match_normaliza7eza7(obj_t
		BgL_ez00_29, obj_t BgL_f1z00_30, obj_t BgL_f2z00_31)
	{
		{	/* Match/normalize.scm 266 */
			{	/* Match/normalize.scm 267 */
				obj_t BgL_zc3z04anonymousza31619ze3z87_3606;

				BgL_zc3z04anonymousza31619ze3z87_3606 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31619ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (3L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31619ze3z87_3606, (int) (0L),
					BgL_ez00_29);
				PROCEDURE_SET(BgL_zc3z04anonymousza31619ze3z87_3606, (int) (1L),
					BgL_f1z00_30);
				PROCEDURE_SET(BgL_zc3z04anonymousza31619ze3z87_3606, (int) (2L),
					BgL_f2z00_31);
				return BgL_zc3z04anonymousza31619ze3z87_3606;
			}
		}

	}



/* &<@anonymous:1619> */
	obj_t BGl_z62zc3z04anonymousza31619ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3607, obj_t BgL_rz00_3611, obj_t BgL_cz00_3612)
	{
		{	/* Match/normalize.scm 267 */
			{	/* Match/normalize.scm 268 */
				obj_t BgL_ez00_3608;
				obj_t BgL_f1z00_3609;
				obj_t BgL_f2z00_3610;

				BgL_ez00_3608 = PROCEDURE_REF(BgL_envz00_3607, (int) (0L));
				BgL_f1z00_3609 = PROCEDURE_REF(BgL_envz00_3607, (int) (1L));
				BgL_f2z00_3610 = PROCEDURE_REF(BgL_envz00_3607, (int) (2L));
				{	/* Match/normalize.scm 268 */
					obj_t BgL_namez00_3870;

					{	/* Match/normalize.scm 109 */
						obj_t BgL_sz00_3871;

						BgL_sz00_3871 = SYMBOL_TO_STRING(((obj_t) BgL_ez00_3608));
						BgL_namez00_3870 =
							bstring_to_symbol(c_substring(BgL_sz00_3871, 1L,
								STRING_LENGTH(BgL_sz00_3871)));
					}
					{	/* Match/normalize.scm 269 */
						obj_t BgL_fun1622z00_3872;

						BgL_fun1622z00_3872 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_f1z00_3609);
						{	/* Match/normalize.scm 270 */
							obj_t BgL_arg1620z00_3873;

							{	/* Match/normalize.scm 270 */
								obj_t BgL_imz00_3874;

								BgL_imz00_3874 = BGl_symbol2292z00zz__match_normaliza7eza7;
								{	/* Match/normalize.scm 334 */
									obj_t BgL_arg1724z00_3875;

									BgL_arg1724z00_3875 =
										MAKE_YOUNG_PAIR(BgL_namez00_3870, BgL_imz00_3874);
									BgL_arg1620z00_3873 =
										MAKE_YOUNG_PAIR(BgL_arg1724z00_3875, BgL_rz00_3611);
							}}
							{	/* Match/normalize.scm 272 */
								obj_t BgL_zc3z04anonymousza31623ze3z87_3876;

								BgL_zc3z04anonymousza31623ze3z87_3876 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31623ze3ze5zz__match_normaliza7eza7,
									(int) (2L), (int) (3L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31623ze3z87_3876, (int) (0L),
									BgL_f2z00_3610);
								PROCEDURE_SET(BgL_zc3z04anonymousza31623ze3z87_3876, (int) (1L),
									BgL_namez00_3870);
								PROCEDURE_SET(BgL_zc3z04anonymousza31623ze3z87_3876, (int) (2L),
									BgL_cz00_3612);
								return BGL_PROCEDURE_CALL2(BgL_fun1622z00_3872,
									BgL_arg1620z00_3873, BgL_zc3z04anonymousza31623ze3z87_3876);
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1623> */
	obj_t BGl_z62zc3z04anonymousza31623ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3613, obj_t BgL_holezd2patternzd2_3617, obj_t BgL_rrz00_3618)
	{
		{	/* Match/normalize.scm 271 */
			{	/* Match/normalize.scm 272 */
				obj_t BgL_f2z00_3614;
				obj_t BgL_namez00_3615;
				obj_t BgL_cz00_3616;

				BgL_f2z00_3614 = PROCEDURE_REF(BgL_envz00_3613, (int) (0L));
				BgL_namez00_3615 = ((obj_t) PROCEDURE_REF(BgL_envz00_3613, (int) (1L)));
				BgL_cz00_3616 = PROCEDURE_REF(BgL_envz00_3613, (int) (2L));
				{	/* Match/normalize.scm 272 */
					obj_t BgL_fun1625z00_3877;

					BgL_fun1625z00_3877 =
						BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
						(BgL_f2z00_3614);
					{	/* Match/normalize.scm 275 */
						obj_t BgL_zc3z04anonymousza31626ze3z87_3878;

						BgL_zc3z04anonymousza31626ze3z87_3878 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31626ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31626ze3z87_3878, (int) (0L),
							BgL_holezd2patternzd2_3617);
						PROCEDURE_SET(BgL_zc3z04anonymousza31626ze3z87_3878, (int) (1L),
							BgL_namez00_3615);
						PROCEDURE_SET(BgL_zc3z04anonymousza31626ze3z87_3878, (int) (2L),
							BgL_cz00_3616);
						return BGL_PROCEDURE_CALL2(BgL_fun1625z00_3877, BgL_rrz00_3618,
							BgL_zc3z04anonymousza31626ze3z87_3878);
					}
				}
			}
		}

	}



/* &<@anonymous:1626> */
	obj_t BGl_z62zc3z04anonymousza31626ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3619, obj_t BgL_patternsz00_3623, obj_t BgL_rrrz00_3624)
	{
		{	/* Match/normalize.scm 274 */
			{	/* Match/normalize.scm 275 */
				obj_t BgL_holezd2patternzd2_3620;
				obj_t BgL_namez00_3621;
				obj_t BgL_cz00_3622;

				BgL_holezd2patternzd2_3620 = PROCEDURE_REF(BgL_envz00_3619, (int) (0L));
				BgL_namez00_3621 = ((obj_t) PROCEDURE_REF(BgL_envz00_3619, (int) (1L)));
				BgL_cz00_3622 = PROCEDURE_REF(BgL_envz00_3619, (int) (2L));
				{	/* Match/normalize.scm 275 */
					bool_t BgL_test2464z00_5101;

					{	/* Match/normalize.scm 275 */
						obj_t BgL_a1120z00_3879;

						BgL_a1120z00_3879 =
							BGl_oczd2countzd2zz__match_normaliza7eza7(BgL_namez00_3621,
							BgL_patternsz00_3623);
						{	/* Match/normalize.scm 275 */

							if (INTEGERP(BgL_a1120z00_3879))
								{	/* Match/normalize.scm 275 */
									BgL_test2464z00_5101 = ((long) CINT(BgL_a1120z00_3879) > 1L);
								}
							else
								{	/* Match/normalize.scm 275 */
									BgL_test2464z00_5101 =
										BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_a1120z00_3879,
										BINT(1L));
								}
						}
					}
					if (BgL_test2464z00_5101)
						{	/* Match/normalize.scm 276 */
							obj_t BgL_arg1629z00_3880;

							{	/* Match/normalize.scm 276 */
								obj_t BgL_arg1630z00_3881;

								{	/* Match/normalize.scm 276 */
									obj_t BgL_arg1631z00_3882;

									{	/* Match/normalize.scm 276 */
										obj_t BgL_arg1634z00_3883;

										BgL_arg1634z00_3883 =
											MAKE_YOUNG_PAIR(BgL_holezd2patternzd2_3620, BNIL);
										BgL_arg1631z00_3882 =
											MAKE_YOUNG_PAIR(BgL_patternsz00_3623,
											BgL_arg1634z00_3883);
									}
									BgL_arg1630z00_3881 =
										MAKE_YOUNG_PAIR(BgL_namez00_3621, BgL_arg1631z00_3882);
								}
								BgL_arg1629z00_3880 =
									MAKE_YOUNG_PAIR(BGl_symbol2292z00zz__match_normaliza7eza7,
									BgL_arg1630z00_3881);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_cz00_3622, BgL_arg1629z00_3880,
								BgL_rrrz00_3624);
						}
					else
						{	/* Match/normalize.scm 277 */
							obj_t BgL_arg1636z00_3884;

							{	/* Match/normalize.scm 277 */
								obj_t BgL_arg1637z00_3885;

								{	/* Match/normalize.scm 277 */
									obj_t BgL_arg1638z00_3886;

									{	/* Match/normalize.scm 277 */
										obj_t BgL_arg1639z00_3887;

										BgL_arg1639z00_3887 =
											MAKE_YOUNG_PAIR(BgL_holezd2patternzd2_3620, BNIL);
										BgL_arg1638z00_3886 =
											MAKE_YOUNG_PAIR(BgL_patternsz00_3623,
											BgL_arg1639z00_3887);
									}
									BgL_arg1637z00_3885 =
										MAKE_YOUNG_PAIR(BgL_namez00_3621, BgL_arg1638z00_3886);
								}
								BgL_arg1636z00_3884 =
									MAKE_YOUNG_PAIR(BGl_symbol2273z00zz__match_normaliza7eza7,
									BgL_arg1637z00_3885);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_cz00_3622, BgL_arg1636z00_3884,
								BgL_rrrz00_3624);
						}
				}
			}
		}

	}



/* oc-count */
	obj_t BGl_oczd2countzd2zz__match_normaliza7eza7(obj_t BgL_namez00_32,
		obj_t BgL_patternz00_33)
	{
		{	/* Match/normalize.scm 279 */
			if (NULLP(BgL_patternz00_33))
				{	/* Match/normalize.scm 281 */
					return BINT(0L);
				}
			else
				{	/* Match/normalize.scm 281 */
					if (
						(CAR(
								((obj_t) BgL_patternz00_33)) ==
							BGl_symbol2271z00zz__match_normaliza7eza7))
						{	/* Match/normalize.scm 283 */
							bool_t BgL_test2468z00_5134;

							{	/* Match/normalize.scm 283 */
								obj_t BgL_tmpz00_5135;

								{	/* Match/normalize.scm 283 */
									obj_t BgL_pairz00_2805;

									BgL_pairz00_2805 = CDR(((obj_t) BgL_patternz00_33));
									BgL_tmpz00_5135 = CAR(BgL_pairz00_2805);
								}
								BgL_test2468z00_5134 = (BgL_tmpz00_5135 == BgL_namez00_32);
							}
							if (BgL_test2468z00_5134)
								{	/* Match/normalize.scm 283 */
									return BINT(1L);
								}
							else
								{	/* Match/normalize.scm 283 */
									return BINT(0L);
								}
						}
					else
						{	/* Match/normalize.scm 282 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
											((obj_t) BgL_patternz00_33)),
										BGl_list2298z00zz__match_normaliza7eza7)))
								{	/* Match/normalize.scm 287 */
									obj_t BgL_runner1653z00_1744;

									{	/* Match/normalize.scm 287 */
										obj_t BgL_l1122z00_1731;

										BgL_l1122z00_1731 = CDR(((obj_t) BgL_patternz00_33));
										if (NULLP(BgL_l1122z00_1731))
											{	/* Match/normalize.scm 287 */
												BgL_runner1653z00_1744 = BNIL;
											}
										else
											{	/* Match/normalize.scm 287 */
												obj_t BgL_head1124z00_1733;

												BgL_head1124z00_1733 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1122z00_2827;
													obj_t BgL_tail1125z00_2828;

													BgL_l1122z00_2827 = BgL_l1122z00_1731;
													BgL_tail1125z00_2828 = BgL_head1124z00_1733;
												BgL_zc3z04anonymousza31649ze3z87_2826:
													if (NULLP(BgL_l1122z00_2827))
														{	/* Match/normalize.scm 287 */
															BgL_runner1653z00_1744 =
																CDR(BgL_head1124z00_1733);
														}
													else
														{	/* Match/normalize.scm 287 */
															obj_t BgL_newtail1126z00_2835;

															{	/* Match/normalize.scm 287 */
																obj_t BgL_arg1652z00_2836;

																{	/* Match/normalize.scm 287 */
																	obj_t BgL_patz00_2837;

																	BgL_patz00_2837 =
																		CAR(((obj_t) BgL_l1122z00_2827));
																	BgL_arg1652z00_2836 =
																		BGl_oczd2countzd2zz__match_normaliza7eza7
																		(BgL_namez00_32, BgL_patz00_2837);
																}
																BgL_newtail1126z00_2835 =
																	MAKE_YOUNG_PAIR(BgL_arg1652z00_2836, BNIL);
															}
															SET_CDR(BgL_tail1125z00_2828,
																BgL_newtail1126z00_2835);
															{	/* Match/normalize.scm 287 */
																obj_t BgL_arg1651z00_2838;

																BgL_arg1651z00_2838 =
																	CDR(((obj_t) BgL_l1122z00_2827));
																{
																	obj_t BgL_tail1125z00_5163;
																	obj_t BgL_l1122z00_5162;

																	BgL_l1122z00_5162 = BgL_arg1651z00_2838;
																	BgL_tail1125z00_5163 =
																		BgL_newtail1126z00_2835;
																	BgL_tail1125z00_2828 = BgL_tail1125z00_5163;
																	BgL_l1122z00_2827 = BgL_l1122z00_5162;
																	goto BgL_zc3z04anonymousza31649ze3z87_2826;
																}
															}
														}
												}
											}
									}
									return
										BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner1653z00_1744);
								}
							else
								{	/* Match/normalize.scm 286 */
									return BINT(0L);
								}
						}
				}
		}

	}



/* standardize-lispish-segment-variable */
	obj_t
		BGl_standardiza7ezd2lispishzd2segmentzd2variablez75zz__match_normaliza7eza7
		(obj_t BgL_ez00_34, obj_t BgL_fza2za2_35)
	{
		{	/* Match/normalize.scm 290 */
			if (NULLP(BgL_fza2za2_35))
				{	/* Match/normalize.scm 292 */
					obj_t BgL_zc3z04anonymousza31658ze3z87_3625;

					BgL_zc3z04anonymousza31658ze3z87_3625 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31658ze3ze5zz__match_normaliza7eza7,
						(int) (2L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31658ze3z87_3625, (int) (0L),
						BgL_ez00_34);
					return BgL_zc3z04anonymousza31658ze3z87_3625;
				}
			else
				{	/* Match/normalize.scm 291 */
					BGL_TAIL return
						BGl_standardiza7ezd2segmentzd2variableza7zz__match_normaliza7eza7
						(BgL_ez00_34, BgL_fza2za2_35);
				}
		}

	}



/* &<@anonymous:1658> */
	obj_t BGl_z62zc3z04anonymousza31658ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3626, obj_t BgL_rz00_3628, obj_t BgL_cz00_3629)
	{
		{	/* Match/normalize.scm 292 */
			{	/* Match/normalize.scm 293 */
				obj_t BgL_ez00_3627;

				BgL_ez00_3627 = PROCEDURE_REF(BgL_envz00_3626, (int) (0L));
				{	/* Match/normalize.scm 293 */
					obj_t BgL_namez00_3888;

					{	/* Match/normalize.scm 117 */
						obj_t BgL_sz00_3889;

						BgL_sz00_3889 = SYMBOL_TO_STRING(((obj_t) BgL_ez00_3627));
						BgL_namez00_3888 =
							bstring_to_symbol(c_substring(BgL_sz00_3889, 3L,
								STRING_LENGTH(BgL_sz00_3889)));
					}
					{	/* Match/normalize.scm 294 */
						bool_t BgL_test2473z00_5181;

						{	/* Match/normalize.scm 294 */
							obj_t BgL_arg1676z00_3890;

							if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_namez00_3888, BgL_rz00_3628)))
								{	/* Match/normalize.scm 337 */
									BgL_arg1676z00_3890 =
										CDR(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_namez00_3888, BgL_rz00_3628));
								}
							else
								{	/* Match/normalize.scm 337 */
									BgL_arg1676z00_3890 = BFALSE;
								}
							BgL_test2473z00_5181 =
								(BgL_arg1676z00_3890 ==
								BGl_unboundzd2patternzd2zz__match_normaliza7eza7);
						}
						if (BgL_test2473z00_5181)
							{	/* Match/normalize.scm 295 */
								obj_t BgL_arg1663z00_3891;
								obj_t BgL_arg1664z00_3892;

								{	/* Match/normalize.scm 295 */
									obj_t BgL_arg1667z00_3893;

									{	/* Match/normalize.scm 295 */
										obj_t BgL_arg1668z00_3894;

										{	/* Match/normalize.scm 295 */
											obj_t BgL_arg1669z00_3895;

											BgL_arg1669z00_3895 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
											BgL_arg1668z00_3894 =
												MAKE_YOUNG_PAIR(BgL_arg1669z00_3895, BNIL);
										}
										BgL_arg1667z00_3893 =
											MAKE_YOUNG_PAIR(BgL_namez00_3888, BgL_arg1668z00_3894);
									}
									BgL_arg1663z00_3891 =
										MAKE_YOUNG_PAIR(BGl_symbol2286z00zz__match_normaliza7eza7,
										BgL_arg1667z00_3893);
								}
								{	/* Match/normalize.scm 296 */
									obj_t BgL_imz00_3896;

									BgL_imz00_3896 = BGl_symbol2288z00zz__match_normaliza7eza7;
									{	/* Match/normalize.scm 334 */
										obj_t BgL_arg1724z00_3897;

										BgL_arg1724z00_3897 =
											MAKE_YOUNG_PAIR(BgL_namez00_3888, BgL_imz00_3896);
										BgL_arg1664z00_3892 =
											MAKE_YOUNG_PAIR(BgL_arg1724z00_3897, BgL_rz00_3628);
									}
								}
								return
									BGL_PROCEDURE_CALL2(BgL_cz00_3629, BgL_arg1663z00_3891,
									BgL_arg1664z00_3892);
							}
						else
							{	/* Match/normalize.scm 297 */
								obj_t BgL_arg1670z00_3898;

								{	/* Match/normalize.scm 297 */
									obj_t BgL_arg1675z00_3899;

									BgL_arg1675z00_3899 = MAKE_YOUNG_PAIR(BgL_namez00_3888, BNIL);
									BgL_arg1670z00_3898 =
										MAKE_YOUNG_PAIR(BGl_symbol2286z00zz__match_normaliza7eza7,
										BgL_arg1675z00_3899);
								}
								return
									BGL_PROCEDURE_CALL2(BgL_cz00_3629, BgL_arg1670z00_3898,
									BgL_rz00_3628);
							}
					}
				}
			}
		}

	}



/* standardize-any */
	obj_t BGl_standardiza7ezd2anyz75zz__match_normaliza7eza7(obj_t BgL_fza2za2_36)
	{
		{	/* Match/normalize.scm 300 */
			{	/* Match/normalize.scm 301 */
				obj_t BgL_zc3z04anonymousza31677ze3z87_3631;

				BgL_zc3z04anonymousza31677ze3z87_3631 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31677ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31677ze3z87_3631, (int) (0L),
					BgL_fza2za2_36);
				return BgL_zc3z04anonymousza31677ze3z87_3631;
			}
		}

	}



/* &<@anonymous:1677> */
	obj_t BGl_z62zc3z04anonymousza31677ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3632, obj_t BgL_rz00_3634, obj_t BgL_cz00_3635)
	{
		{	/* Match/normalize.scm 301 */
			{	/* Match/normalize.scm 302 */
				obj_t BgL_fza2za2_3633;

				BgL_fza2za2_3633 = PROCEDURE_REF(BgL_envz00_3632, (int) (0L));
				{	/* Match/normalize.scm 302 */
					obj_t BgL_fun1680z00_3900;

					BgL_fun1680z00_3900 =
						BGl_standardiza7ezd2patternsz75zz__match_normaliza7eza7
						(BgL_fza2za2_3633);
					{	/* Match/normalize.scm 305 */
						obj_t BgL_zc3z04anonymousza31681ze3z87_3901;

						BgL_zc3z04anonymousza31681ze3z87_3901 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31681ze3ze5zz__match_normaliza7eza7,
							(int) (2L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31681ze3z87_3901, (int) (0L),
							BgL_cz00_3635);
						return BGL_PROCEDURE_CALL2(BgL_fun1680z00_3900, BgL_rz00_3634,
							BgL_zc3z04anonymousza31681ze3z87_3901);
					}
				}
			}
		}

	}



/* &<@anonymous:1681> */
	obj_t BGl_z62zc3z04anonymousza31681ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3636, obj_t BgL_patternz00_3638, obj_t BgL_rrz00_3639)
	{
		{	/* Match/normalize.scm 304 */
			{	/* Match/normalize.scm 305 */
				obj_t BgL_cz00_3637;

				BgL_cz00_3637 = PROCEDURE_REF(BgL_envz00_3636, (int) (0L));
				{	/* Match/normalize.scm 305 */
					obj_t BgL_labelz00_3902;

					BgL_labelz00_3902 =
						BGL_PROCEDURE_CALL1(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
						BGl_string2269z00zz__match_normaliza7eza7);
					if (CBOOL(BGl_z62zc3z04anonymousza31531ze3ze5zz__match_normaliza7eza7
							(BGl_za2preferzd2xconsza2zd2zz__match_normaliza7eza7,
								BGl_symbol2278z00zz__match_normaliza7eza7)))
						{	/* Match/normalize.scm 308 */
							obj_t BgL_arg1684z00_3903;

							{	/* Match/normalize.scm 308 */
								obj_t BgL_arg1685z00_3904;

								{	/* Match/normalize.scm 308 */
									obj_t BgL_arg1688z00_3905;

									{	/* Match/normalize.scm 308 */
										obj_t BgL_arg1689z00_3906;
										obj_t BgL_arg1691z00_3907;

										{	/* Match/normalize.scm 308 */
											obj_t BgL_arg1692z00_3908;

											{	/* Match/normalize.scm 308 */
												obj_t BgL_arg1699z00_3909;
												obj_t BgL_arg1700z00_3910;

												BgL_arg1699z00_3909 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
												{	/* Match/normalize.scm 308 */
													obj_t BgL_arg1701z00_3911;

													{	/* Match/normalize.scm 308 */
														obj_t BgL_arg1702z00_3912;

														{	/* Match/normalize.scm 308 */
															obj_t BgL_arg1703z00_3913;

															{	/* Match/normalize.scm 308 */
																obj_t BgL_arg1704z00_3914;

																BgL_arg1704z00_3914 =
																	BGL_PROCEDURE_CALL1
																	(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
																	BGl_string2270z00zz__match_normaliza7eza7);
																BgL_arg1703z00_3913 =
																	MAKE_YOUNG_PAIR(BgL_arg1704z00_3914, BNIL);
															}
															BgL_arg1702z00_3912 =
																MAKE_YOUNG_PAIR(BgL_labelz00_3902,
																BgL_arg1703z00_3913);
														}
														BgL_arg1701z00_3911 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2271z00zz__match_normaliza7eza7,
															BgL_arg1702z00_3912);
													}
													BgL_arg1700z00_3910 =
														MAKE_YOUNG_PAIR(BgL_arg1701z00_3911, BNIL);
												}
												BgL_arg1692z00_3908 =
													MAKE_YOUNG_PAIR(BgL_arg1699z00_3909,
													BgL_arg1700z00_3910);
											}
											BgL_arg1689z00_3906 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2284z00zz__match_normaliza7eza7,
												BgL_arg1692z00_3908);
										}
										BgL_arg1691z00_3907 =
											MAKE_YOUNG_PAIR(BgL_patternz00_3638, BNIL);
										BgL_arg1688z00_3905 =
											MAKE_YOUNG_PAIR(BgL_arg1689z00_3906, BgL_arg1691z00_3907);
									}
									BgL_arg1685z00_3904 =
										MAKE_YOUNG_PAIR(BgL_labelz00_3902, BgL_arg1688z00_3905);
								}
								BgL_arg1684z00_3903 =
									MAKE_YOUNG_PAIR(BGl_symbol2273z00zz__match_normaliza7eza7,
									BgL_arg1685z00_3904);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_cz00_3637, BgL_arg1684z00_3903,
								BgL_rrz00_3639);
						}
					else
						{	/* Match/normalize.scm 312 */
							obj_t BgL_arg1705z00_3915;

							{	/* Match/normalize.scm 312 */
								obj_t BgL_arg1706z00_3916;

								{	/* Match/normalize.scm 312 */
									obj_t BgL_arg1707z00_3917;

									{	/* Match/normalize.scm 312 */
										obj_t BgL_arg1708z00_3918;
										obj_t BgL_arg1709z00_3919;

										{	/* Match/normalize.scm 312 */
											obj_t BgL_arg1710z00_3920;

											{	/* Match/normalize.scm 312 */
												obj_t BgL_arg1711z00_3921;
												obj_t BgL_arg1714z00_3922;

												BgL_arg1711z00_3921 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
												{	/* Match/normalize.scm 312 */
													obj_t BgL_arg1715z00_3923;

													{	/* Match/normalize.scm 312 */
														obj_t BgL_arg1717z00_3924;

														{	/* Match/normalize.scm 312 */
															obj_t BgL_arg1718z00_3925;

															{	/* Match/normalize.scm 312 */
																obj_t BgL_arg1720z00_3926;

																BgL_arg1720z00_3926 =
																	BGL_PROCEDURE_CALL1
																	(BGl_jimzd2gensymzd2zz__match_s2cfunz00,
																	BGl_string2270z00zz__match_normaliza7eza7);
																BgL_arg1718z00_3925 =
																	MAKE_YOUNG_PAIR(BgL_arg1720z00_3926, BNIL);
															}
															BgL_arg1717z00_3924 =
																MAKE_YOUNG_PAIR(BgL_labelz00_3902,
																BgL_arg1718z00_3925);
														}
														BgL_arg1715z00_3923 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2271z00zz__match_normaliza7eza7,
															BgL_arg1717z00_3924);
													}
													BgL_arg1714z00_3922 =
														MAKE_YOUNG_PAIR(BgL_arg1715z00_3923, BNIL);
												}
												BgL_arg1710z00_3920 =
													MAKE_YOUNG_PAIR(BgL_arg1711z00_3921,
													BgL_arg1714z00_3922);
											}
											BgL_arg1708z00_3918 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2260z00zz__match_normaliza7eza7,
												BgL_arg1710z00_3920);
										}
										BgL_arg1709z00_3919 =
											MAKE_YOUNG_PAIR(BgL_patternz00_3638, BNIL);
										BgL_arg1707z00_3917 =
											MAKE_YOUNG_PAIR(BgL_arg1708z00_3918, BgL_arg1709z00_3919);
									}
									BgL_arg1706z00_3916 =
										MAKE_YOUNG_PAIR(BgL_labelz00_3902, BgL_arg1707z00_3917);
								}
								BgL_arg1705z00_3915 =
									MAKE_YOUNG_PAIR(BGl_symbol2273z00zz__match_normaliza7eza7,
									BgL_arg1706z00_3916);
							}
							return
								BGL_PROCEDURE_CALL2(BgL_cz00_3637, BgL_arg1705z00_3915,
								BgL_rrz00_3639);
						}
				}
			}
		}

	}



/* standardize-lispish-any */
	obj_t BGl_standardiza7ezd2lispishzd2anyza7zz__match_normaliza7eza7(obj_t
		BgL_fza2za2_37)
	{
		{	/* Match/normalize.scm 316 */
			if (NULLP(BgL_fza2za2_37))
				{	/* Match/normalize.scm 317 */
					return BGl_proc2301z00zz__match_normaliza7eza7;
				}
			else
				{	/* Match/normalize.scm 317 */
					BGL_TAIL return
						BGl_standardiza7ezd2anyz75zz__match_normaliza7eza7(BgL_fza2za2_37);
				}
		}

	}



/* &<@anonymous:1722> */
	obj_t BGl_z62zc3z04anonymousza31722ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3641, obj_t BgL_rz00_3642, obj_t BgL_cz00_3643)
	{
		{	/* Match/normalize.scm 318 */
			{	/* Match/normalize.scm 318 */
				obj_t BgL_arg1723z00_3927;

				BgL_arg1723z00_3927 =
					MAKE_YOUNG_PAIR(BGl_symbol2258z00zz__match_normaliza7eza7, BNIL);
				return
					BGL_PROCEDURE_CALL2(BgL_cz00_3643, BgL_arg1723z00_3927,
					BgL_rz00_3642);
			}
		}

	}



/* extend-r-macro-env */
	BGL_EXPORTED_DEF obj_t
		BGl_extendzd2rzd2macrozd2envzd2zz__match_normaliza7eza7(obj_t
		BgL_namez00_45, obj_t BgL_funz00_46)
	{
		{	/* Match/normalize.scm 341 */
			{	/* Match/normalize.scm 343 */
				obj_t BgL_fnz00_2862;

				BgL_fnz00_2862 = BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7;
				{	/* Match/normalize.scm 334 */
					obj_t BgL_arg1724z00_2863;

					BgL_arg1724z00_2863 = MAKE_YOUNG_PAIR(BgL_namez00_45, BgL_funz00_46);
					return (BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7 =
						MAKE_YOUNG_PAIR(BgL_arg1724z00_2863, BgL_fnz00_2862), BUNSPEC);
				}
			}
		}

	}



/* &extend-r-macro-env */
	obj_t BGl_z62extendzd2rzd2macrozd2envzb0zz__match_normaliza7eza7(obj_t
		BgL_envz00_3644, obj_t BgL_namez00_3645, obj_t BgL_funz00_3646)
	{
		{	/* Match/normalize.scm 341 */
			return
				BGl_extendzd2rzd2macrozd2envzd2zz__match_normaliza7eza7
				(BgL_namez00_3645, BgL_funz00_3646);
		}

	}



/* coherent-environment? */
	bool_t BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7(obj_t
		BgL_rz00_48, obj_t BgL_rrz00_49)
	{
		{	/* Match/normalize.scm 469 */
		BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7:
			if (PAIRP(BgL_rz00_48))
				{	/* Match/normalize.scm 475 */
					bool_t BgL_test2478z00_5290;

					{	/* Match/normalize.scm 475 */
						obj_t BgL_arg1730z00_1813;

						BgL_arg1730z00_1813 = CAR(CAR(BgL_rz00_48));
						{
							obj_t BgL_rz00_2894;

							BgL_rz00_2894 = BgL_rrz00_49;
						BgL_lookz00_2893:
							if (PAIRP(BgL_rz00_2894))
								{	/* Match/normalize.scm 472 */
									bool_t BgL__ortest_1061z00_2896;

									BgL__ortest_1061z00_2896 =
										(CAR(CAR(BgL_rz00_2894)) == BgL_arg1730z00_1813);
									if (BgL__ortest_1061z00_2896)
										{	/* Match/normalize.scm 472 */
											BgL_test2478z00_5290 = BgL__ortest_1061z00_2896;
										}
									else
										{
											obj_t BgL_rz00_5299;

											BgL_rz00_5299 = CDR(BgL_rz00_2894);
											BgL_rz00_2894 = BgL_rz00_5299;
											goto BgL_lookz00_2893;
										}
								}
							else
								{	/* Match/normalize.scm 471 */
									BgL_test2478z00_5290 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2478z00_5290)
						{
							obj_t BgL_rz00_5301;

							BgL_rz00_5301 = CDR(BgL_rz00_48);
							BgL_rz00_48 = BgL_rz00_5301;
							goto BGl_coherentzd2environmentzf3z21zz__match_normaliza7eza7;
						}
					else
						{	/* Match/normalize.scm 475 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Match/normalize.scm 474 */
					return ((bool_t) 1);
				}
		}

	}



/* standardize-vector */
	obj_t BGl_standardiza7ezd2vectorz75zz__match_normaliza7eza7(obj_t BgL_ez00_54)
	{
		{	/* Match/normalize.scm 496 */
			{	/* Match/normalize.scm 498 */
				obj_t BgL_tmpz00_1818;

				{	/* Match/normalize.scm 498 */
					obj_t BgL_arg1777z00_1865;

					BgL_arg1777z00_1865 =
						BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_ez00_54);
					{	/* Match/normalize.scm 124 */
						obj_t BgL_fun1461z00_2905;

						BgL_fun1461z00_2905 =
							BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
							(BgL_arg1777z00_1865);
						BgL_tmpz00_1818 =
							BGL_PROCEDURE_CALL2(BgL_fun1461z00_2905,
							BGl_rzd2macrozd2patternz00zz__match_normaliza7eza7,
							BGl_proc2302z00zz__match_normaliza7eza7);
					}
				}
				{	/* Match/normalize.scm 518 */
					obj_t BgL_zc3z04anonymousza31734ze3z87_3647;

					BgL_zc3z04anonymousza31734ze3z87_3647 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31734ze3ze5zz__match_normaliza7eza7,
						(int) (2L), (int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31734ze3z87_3647, (int) (0L),
						BgL_ez00_54);
					PROCEDURE_SET(BgL_zc3z04anonymousza31734ze3z87_3647, (int) (1L),
						BgL_tmpz00_1818);
					return BgL_zc3z04anonymousza31734ze3z87_3647;
				}
			}
		}

	}



/* &<@anonymous:1734> */
	obj_t BGl_z62zc3z04anonymousza31734ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3649, obj_t BgL_rz00_3652, obj_t BgL_cz00_3653)
	{
		{	/* Match/normalize.scm 518 */
			{	/* Match/normalize.scm 519 */
				obj_t BgL_ez00_3650;
				obj_t BgL_tmpz00_3651;

				BgL_ez00_3650 = ((obj_t) PROCEDURE_REF(BgL_envz00_3649, (int) (0L)));
				BgL_tmpz00_3651 = PROCEDURE_REF(BgL_envz00_3649, (int) (1L));
				{	/* Match/normalize.scm 519 */
					obj_t BgL_arg1735z00_3928;

					{	/* Match/normalize.scm 519 */
						obj_t BgL_arg1736z00_3929;

						{	/* Match/normalize.scm 519 */
							long BgL_arg1737z00_3930;
							obj_t BgL_arg1738z00_3931;

							BgL_arg1737z00_3930 =
								BGl_patternzd2lengthzd2zz__match_normaliza7eza7
								(BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_ez00_3650));
							BgL_arg1738z00_3931 =
								MAKE_YOUNG_PAIR(BGl_vectorifyze70ze7zz__match_normaliza7eza7
								(BgL_tmpz00_3651), BNIL);
							BgL_arg1736z00_3929 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg1737z00_3930), BgL_arg1738z00_3931);
						}
						BgL_arg1735z00_3928 =
							MAKE_YOUNG_PAIR(BGl_symbol2303z00zz__match_normaliza7eza7,
							BgL_arg1736z00_3929);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_cz00_3653, BgL_arg1735z00_3928,
						BgL_rz00_3652);
				}
			}
		}

	}



/* vectorify~0 */
	obj_t BGl_vectorifyze70ze7zz__match_normaliza7eza7(obj_t BgL_pz00_1819)
	{
		{	/* Match/normalize.scm 499 */
			if (
				(CAR(
						((obj_t) BgL_pz00_1819)) ==
					BGl_symbol2260z00zz__match_normaliza7eza7))
				{	/* Match/normalize.scm 503 */
					obj_t BgL_arg1743z00_1832;

					{	/* Match/normalize.scm 503 */
						obj_t BgL_arg1744z00_1833;
						obj_t BgL_arg1745z00_1834;

						{	/* Match/normalize.scm 503 */
							obj_t BgL_pairz00_2913;

							BgL_pairz00_2913 = CDR(((obj_t) BgL_pz00_1819));
							BgL_arg1744z00_1833 = CAR(BgL_pairz00_2913);
						}
						{	/* Match/normalize.scm 504 */
							obj_t BgL_arg1746z00_1835;

							{	/* Match/normalize.scm 504 */
								bool_t BgL_test2482z00_5341;

								{	/* Match/normalize.scm 504 */
									obj_t BgL_auxz00_5342;

									{	/* Match/normalize.scm 504 */
										obj_t BgL_pairz00_2919;

										{	/* Match/normalize.scm 504 */
											obj_t BgL_pairz00_2918;

											BgL_pairz00_2918 = CDR(((obj_t) BgL_pz00_1819));
											BgL_pairz00_2919 = CDR(BgL_pairz00_2918);
										}
										BgL_auxz00_5342 = CAR(BgL_pairz00_2919);
									}
									BgL_test2482z00_5341 =
										BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_auxz00_5342,
										BGl_list2305z00zz__match_normaliza7eza7);
								}
								if (BgL_test2482z00_5341)
									{	/* Match/normalize.scm 504 */
										BgL_arg1746z00_1835 =
											BGl_list2306z00zz__match_normaliza7eza7;
									}
								else
									{	/* Match/normalize.scm 506 */
										obj_t BgL_arg1749z00_1838;

										{	/* Match/normalize.scm 506 */
											obj_t BgL_pairz00_2925;

											{	/* Match/normalize.scm 506 */
												obj_t BgL_pairz00_2924;

												BgL_pairz00_2924 = CDR(((obj_t) BgL_pz00_1819));
												BgL_pairz00_2925 = CDR(BgL_pairz00_2924);
											}
											BgL_arg1749z00_1838 = CAR(BgL_pairz00_2925);
										}
										BgL_arg1746z00_1835 =
											BGl_vectorifyze70ze7zz__match_normaliza7eza7
											(BgL_arg1749z00_1838);
									}
							}
							BgL_arg1745z00_1834 = MAKE_YOUNG_PAIR(BgL_arg1746z00_1835, BNIL);
						}
						BgL_arg1743z00_1832 =
							MAKE_YOUNG_PAIR(BgL_arg1744z00_1833, BgL_arg1745z00_1834);
					}
					return
						MAKE_YOUNG_PAIR(BGl_symbol2309z00zz__match_normaliza7eza7,
						BgL_arg1743z00_1832);
				}
			else
				{	/* Match/normalize.scm 502 */
					if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_pz00_1819,
							BGl_list2311z00zz__match_normaliza7eza7))
						{	/* Match/normalize.scm 507 */
							return BGl_list2312z00zz__match_normaliza7eza7;
						}
					else
						{	/* Match/normalize.scm 507 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
											((obj_t) BgL_pz00_1819)),
										BGl_list2315z00zz__match_normaliza7eza7)))
								{	/* Match/normalize.scm 510 */
									obj_t BgL_arg1754z00_1843;
									obj_t BgL_arg1755z00_1844;
									obj_t BgL_arg1756z00_1845;

									BgL_arg1754z00_1843 = CAR(((obj_t) BgL_pz00_1819));
									{	/* Match/normalize.scm 511 */
										obj_t BgL_arg1760z00_1849;

										{	/* Match/normalize.scm 511 */
											obj_t BgL_pairz00_2931;

											BgL_pairz00_2931 = CDR(((obj_t) BgL_pz00_1819));
											BgL_arg1760z00_1849 = CAR(BgL_pairz00_2931);
										}
										BgL_arg1755z00_1844 =
											BGl_vectorifyze70ze7zz__match_normaliza7eza7
											(BgL_arg1760z00_1849);
									}
									{	/* Match/normalize.scm 512 */
										obj_t BgL_arg1761z00_1850;

										{	/* Match/normalize.scm 512 */
											obj_t BgL_pairz00_2937;

											{	/* Match/normalize.scm 512 */
												obj_t BgL_pairz00_2936;

												BgL_pairz00_2936 = CDR(((obj_t) BgL_pz00_1819));
												BgL_pairz00_2937 = CDR(BgL_pairz00_2936);
											}
											BgL_arg1761z00_1850 = CAR(BgL_pairz00_2937);
										}
										BgL_arg1756z00_1845 =
											BGl_vectorifyze70ze7zz__match_normaliza7eza7
											(BgL_arg1761z00_1850);
									}
									{	/* Match/normalize.scm 510 */
										obj_t BgL_list1757z00_1846;

										{	/* Match/normalize.scm 510 */
											obj_t BgL_arg1758z00_1847;

											{	/* Match/normalize.scm 510 */
												obj_t BgL_arg1759z00_1848;

												BgL_arg1759z00_1848 =
													MAKE_YOUNG_PAIR(BgL_arg1756z00_1845, BNIL);
												BgL_arg1758z00_1847 =
													MAKE_YOUNG_PAIR(BgL_arg1755z00_1844,
													BgL_arg1759z00_1848);
											}
											BgL_list1757z00_1846 =
												MAKE_YOUNG_PAIR(BgL_arg1754z00_1843,
												BgL_arg1758z00_1847);
										}
										return BgL_list1757z00_1846;
									}
								}
							else
								{	/* Match/normalize.scm 509 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
													((obj_t) BgL_pz00_1819)),
												BGl_list2316z00zz__match_normaliza7eza7)))
										{	/* Match/normalize.scm 514 */
											obj_t BgL_arg1764z00_1853;
											obj_t BgL_arg1765z00_1854;
											obj_t BgL_arg1766z00_1855;

											{	/* Match/normalize.scm 514 */
												obj_t BgL_pairz00_2943;

												BgL_pairz00_2943 = CDR(((obj_t) BgL_pz00_1819));
												BgL_arg1764z00_1853 = CAR(BgL_pairz00_2943);
											}
											{	/* Match/normalize.scm 515 */
												obj_t BgL_arg1771z00_1860;

												{	/* Match/normalize.scm 515 */
													obj_t BgL_pairz00_2949;

													{	/* Match/normalize.scm 515 */
														obj_t BgL_pairz00_2948;

														BgL_pairz00_2948 = CDR(((obj_t) BgL_pz00_1819));
														BgL_pairz00_2949 = CDR(BgL_pairz00_2948);
													}
													BgL_arg1771z00_1860 = CAR(BgL_pairz00_2949);
												}
												BgL_arg1765z00_1854 =
													BGl_vectorifyze70ze7zz__match_normaliza7eza7
													(BgL_arg1771z00_1860);
											}
											{	/* Match/normalize.scm 516 */
												obj_t BgL_arg1772z00_1861;

												{	/* Match/normalize.scm 516 */
													obj_t BgL_pairz00_2957;

													{	/* Match/normalize.scm 516 */
														obj_t BgL_pairz00_2956;

														{	/* Match/normalize.scm 516 */
															obj_t BgL_pairz00_2955;

															BgL_pairz00_2955 = CDR(((obj_t) BgL_pz00_1819));
															BgL_pairz00_2956 = CDR(BgL_pairz00_2955);
														}
														BgL_pairz00_2957 = CDR(BgL_pairz00_2956);
													}
													BgL_arg1772z00_1861 = CAR(BgL_pairz00_2957);
												}
												BgL_arg1766z00_1855 =
													BGl_vectorifyze70ze7zz__match_normaliza7eza7
													(BgL_arg1772z00_1861);
											}
											{	/* Match/normalize.scm 514 */
												obj_t BgL_list1767z00_1856;

												{	/* Match/normalize.scm 514 */
													obj_t BgL_arg1768z00_1857;

													{	/* Match/normalize.scm 514 */
														obj_t BgL_arg1769z00_1858;

														{	/* Match/normalize.scm 514 */
															obj_t BgL_arg1770z00_1859;

															BgL_arg1770z00_1859 =
																MAKE_YOUNG_PAIR(BgL_arg1766z00_1855, BNIL);
															BgL_arg1769z00_1858 =
																MAKE_YOUNG_PAIR(BgL_arg1765z00_1854,
																BgL_arg1770z00_1859);
														}
														BgL_arg1768z00_1857 =
															MAKE_YOUNG_PAIR(BgL_arg1764z00_1853,
															BgL_arg1769z00_1858);
													}
													BgL_list1767z00_1856 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2317z00zz__match_normaliza7eza7,
														BgL_arg1768z00_1857);
												}
												return BgL_list1767z00_1856;
											}
										}
									else
										{	/* Match/normalize.scm 513 */
											return BgL_pz00_1819;
										}
								}
						}
				}
		}

	}



/* &<@anonymous:1462>2215 */
	obj_t BGl_z62zc3z04anonymousza31462ze32215ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3654, obj_t BgL_patternz00_3655, obj_t BgL_rrz00_3656)
	{
		{	/* Match/normalize.scm 126 */
			return BgL_patternz00_3655;
		}

	}



/* pattern-length */
	long BGl_patternzd2lengthzd2zz__match_normaliza7eza7(obj_t BgL_pz00_55)
	{
		{	/* Match/normalize.scm 522 */
		BGl_patternzd2lengthzd2zz__match_normaliza7eza7:
			if (CBOOL(BGl_atomzf3zf3zz__match_s2cfunz00(BgL_pz00_55)))
				{	/* Match/normalize.scm 524 */
					return 0L;
				}
			else
				{	/* Match/normalize.scm 524 */
					if (NULLP(BgL_pz00_55))
						{	/* Match/normalize.scm 525 */
							return 0L;
						}
					else
						{	/* Match/normalize.scm 525 */
							if (
								(CAR(
										((obj_t) BgL_pz00_55)) ==
									BGl_symbol2229z00zz__match_normaliza7eza7))
								{	/* Match/normalize.scm 526 */
									return 1L;
								}
							else
								{	/* Match/normalize.scm 527 */
									bool_t BgL_test2489z00_5409;

									{	/* Match/normalize.scm 527 */
										obj_t BgL_arg1794z00_1883;

										BgL_arg1794z00_1883 = CAR(((obj_t) BgL_pz00_55));
										BgL_test2489z00_5409 =
											BGl_treezd2variablezf3z21zz__match_normaliza7eza7
											(BgL_arg1794z00_1883);
									}
									if (BgL_test2489z00_5409)
										{	/* Match/normalize.scm 527 */
											return 0L;
										}
									else
										{	/* Match/normalize.scm 527 */
											if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
															((obj_t) BgL_pz00_55)),
														BGl_list2319z00zz__match_normaliza7eza7)))
												{	/* Match/normalize.scm 528 */
													return 0L;
												}
											else
												{	/* Match/normalize.scm 528 */
													if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
																(((obj_t) BgL_pz00_55)),
																BGl_list2320z00zz__match_normaliza7eza7)))
														{	/* Match/normalize.scm 529 */
															obj_t BgL_arg1789z00_1876;

															{	/* Match/normalize.scm 529 */
																obj_t BgL_pairz00_2972;

																BgL_pairz00_2972 = CDR(((obj_t) BgL_pz00_55));
																BgL_arg1789z00_1876 = CAR(BgL_pairz00_2972);
															}
															{
																obj_t BgL_pz00_5426;

																BgL_pz00_5426 = BgL_arg1789z00_1876;
																BgL_pz00_55 = BgL_pz00_5426;
																goto
																	BGl_patternzd2lengthzd2zz__match_normaliza7eza7;
															}
														}
													else
														{	/* Match/normalize.scm 530 */
															long BgL_b1128z00_1878;

															{	/* Match/normalize.scm 530 */
																obj_t BgL_arg1791z00_1880;

																BgL_arg1791z00_1880 =
																	CDR(((obj_t) BgL_pz00_55));
																BgL_b1128z00_1878 =
																	BGl_patternzd2lengthzd2zz__match_normaliza7eza7
																	(BgL_arg1791z00_1880);
															}
															{	/* Match/normalize.scm 530 */

																return (1L + BgL_b1128z00_1878);
															}
														}
												}
										}
								}
						}
				}
		}

	}



/* match-define-structure! */
	BGL_EXPORTED_DEF obj_t
		BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7(obj_t
		BgL_expz00_56)
	{
		{	/* Match/normalize.scm 546 */
			if (PAIRP(BgL_expz00_56))
				{	/* Match/normalize.scm 547 */
					obj_t BgL_cdrzd2126zd2_1891;

					BgL_cdrzd2126zd2_1891 = CDR(((obj_t) BgL_expz00_56));
					if (
						(CAR(
								((obj_t) BgL_expz00_56)) ==
							BGl_symbol2321z00zz__match_normaliza7eza7))
						{	/* Match/normalize.scm 547 */
							if (PAIRP(BgL_cdrzd2126zd2_1891))
								{	/* Match/normalize.scm 547 */
									obj_t BgL_arg1800z00_1895;
									obj_t BgL_arg1801z00_1896;

									BgL_arg1800z00_1895 = CAR(BgL_cdrzd2126zd2_1891);
									BgL_arg1801z00_1896 = CDR(BgL_cdrzd2126zd2_1891);
									{	/* Match/normalize.scm 550 */
										obj_t BgL_arg1803z00_2992;

										{	/* Match/normalize.scm 550 */
											obj_t BgL_arg1804z00_2993;

											{	/* Match/normalize.scm 550 */
												obj_t BgL_arg1805z00_2994;
												obj_t BgL_arg1806z00_2995;

												{	/* Match/normalize.scm 550 */
													obj_t BgL_arg1807z00_2996;

													{	/* Match/normalize.scm 550 */
														obj_t BgL_arg1808z00_2997;
														obj_t BgL_arg1809z00_2998;

														{	/* Match/normalize.scm 550 */
															obj_t BgL_arg2024z00_3000;

															BgL_arg2024z00_3000 =
																SYMBOL_TO_STRING(((obj_t) BgL_arg1800z00_1895));
															BgL_arg1808z00_2997 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg2024z00_3000);
														}
														{	/* Match/normalize.scm 550 */
															obj_t BgL_symbolz00_3001;

															BgL_symbolz00_3001 =
																BGl_symbol2232z00zz__match_normaliza7eza7;
															{	/* Match/normalize.scm 550 */
																obj_t BgL_arg2024z00_3002;

																BgL_arg2024z00_3002 =
																	SYMBOL_TO_STRING(BgL_symbolz00_3001);
																BgL_arg1809z00_2998 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg2024z00_3002);
															}
														}
														BgL_arg1807z00_2996 =
															string_append(BgL_arg1808z00_2997,
															BgL_arg1809z00_2998);
													}
													BgL_arg1805z00_2994 =
														bstring_to_symbol(BgL_arg1807z00_2996);
												}
												BgL_arg1806z00_2995 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1801z00_1896, BNIL);
												BgL_arg1804z00_2993 =
													MAKE_YOUNG_PAIR(BgL_arg1805z00_2994,
													BgL_arg1806z00_2995);
											}
											BgL_arg1803z00_2992 =
												MAKE_YOUNG_PAIR(BgL_arg1800z00_1895,
												BgL_arg1804z00_2993);
										}
										return
											(BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7 =
											MAKE_YOUNG_PAIR(BgL_arg1803z00_2992,
												BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7),
											BUNSPEC);
									}
								}
							else
								{	/* Match/normalize.scm 547 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2323z00zz__match_normaliza7eza7, BgL_expz00_56,
										BGl_symbol2324z00zz__match_normaliza7eza7);
								}
						}
					else
						{	/* Match/normalize.scm 547 */
							return
								BGl_errorz00zz__errorz00
								(BGl_string2323z00zz__match_normaliza7eza7, BgL_expz00_56,
								BGl_symbol2324z00zz__match_normaliza7eza7);
						}
				}
			else
				{	/* Match/normalize.scm 547 */
					return
						BGl_errorz00zz__errorz00(BGl_string2323z00zz__match_normaliza7eza7,
						BgL_expz00_56, BGl_symbol2324z00zz__match_normaliza7eza7);
				}
		}

	}



/* &match-define-structure! */
	obj_t BGl_z62matchzd2definezd2structurez12z70zz__match_normaliza7eza7(obj_t
		BgL_envz00_3657, obj_t BgL_expz00_3658)
	{
		{	/* Match/normalize.scm 546 */
			return
				BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7
				(BgL_expz00_3658);
		}

	}



/* match-define-record-type! */
	BGL_EXPORTED_DEF obj_t
		BGl_matchzd2definezd2recordzd2typez12zc0zz__match_normaliza7eza7(obj_t
		BgL_expz00_57)
	{
		{	/* Match/normalize.scm 554 */
			{
				obj_t BgL_namez00_1905;
				obj_t BgL_constrz00_1906;
				obj_t BgL_predz00_1907;
				obj_t BgL_fieldsz00_1908;

				if (PAIRP(BgL_expz00_57))
					{	/* Match/normalize.scm 555 */
						obj_t BgL_cdrzd2145zd2_1913;

						BgL_cdrzd2145zd2_1913 = CDR(((obj_t) BgL_expz00_57));
						if (
							(CAR(
									((obj_t) BgL_expz00_57)) ==
								BGl_symbol2326z00zz__match_normaliza7eza7))
							{	/* Match/normalize.scm 555 */
								if (PAIRP(BgL_cdrzd2145zd2_1913))
									{	/* Match/normalize.scm 555 */
										obj_t BgL_cdrzd2151zd2_1917;

										BgL_cdrzd2151zd2_1917 = CDR(BgL_cdrzd2145zd2_1913);
										if (PAIRP(BgL_cdrzd2151zd2_1917))
											{	/* Match/normalize.scm 555 */
												obj_t BgL_cdrzd2157zd2_1919;

												BgL_cdrzd2157zd2_1919 = CDR(BgL_cdrzd2151zd2_1917);
												if (PAIRP(BgL_cdrzd2157zd2_1919))
													{	/* Match/normalize.scm 555 */
														BgL_namez00_1905 = CAR(BgL_cdrzd2145zd2_1913);
														BgL_constrz00_1906 = CAR(BgL_cdrzd2151zd2_1917);
														BgL_predz00_1907 = CAR(BgL_cdrzd2157zd2_1919);
														BgL_fieldsz00_1908 = CDR(BgL_cdrzd2157zd2_1919);
														{	/* Match/normalize.scm 557 */
															obj_t BgL_reallyzd2fieldszd2_1926;

															if (NULLP(BgL_fieldsz00_1908))
																{	/* Match/normalize.scm 557 */
																	BgL_reallyzd2fieldszd2_1926 = BNIL;
																}
															else
																{	/* Match/normalize.scm 557 */
																	obj_t BgL_head1131z00_1932;

																	{	/* Match/normalize.scm 557 */
																		obj_t BgL_arg1834z00_1944;

																		{	/* Match/normalize.scm 557 */
																			obj_t BgL_pairz00_3005;

																			BgL_pairz00_3005 =
																				CAR(((obj_t) BgL_fieldsz00_1908));
																			BgL_arg1834z00_1944 =
																				CAR(BgL_pairz00_3005);
																		}
																		BgL_head1131z00_1932 =
																			MAKE_YOUNG_PAIR(BgL_arg1834z00_1944,
																			BNIL);
																	}
																	{	/* Match/normalize.scm 557 */
																		obj_t BgL_g1134z00_1933;

																		BgL_g1134z00_1933 =
																			CDR(((obj_t) BgL_fieldsz00_1908));
																		{
																			obj_t BgL_l1129z00_3026;
																			obj_t BgL_tail1132z00_3027;

																			BgL_l1129z00_3026 = BgL_g1134z00_1933;
																			BgL_tail1132z00_3027 =
																				BgL_head1131z00_1932;
																		BgL_zc3z04anonymousza31828ze3z87_3025:
																			if (NULLP(BgL_l1129z00_3026))
																				{	/* Match/normalize.scm 557 */
																					BgL_reallyzd2fieldszd2_1926 =
																						BgL_head1131z00_1932;
																				}
																			else
																				{	/* Match/normalize.scm 557 */
																					obj_t BgL_newtail1133z00_3034;

																					{	/* Match/normalize.scm 557 */
																						obj_t BgL_arg1832z00_3035;

																						{	/* Match/normalize.scm 557 */
																							obj_t BgL_pairz00_3039;

																							BgL_pairz00_3039 =
																								CAR(
																								((obj_t) BgL_l1129z00_3026));
																							BgL_arg1832z00_3035 =
																								CAR(BgL_pairz00_3039);
																						}
																						BgL_newtail1133z00_3034 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1832z00_3035, BNIL);
																					}
																					SET_CDR(BgL_tail1132z00_3027,
																						BgL_newtail1133z00_3034);
																					{	/* Match/normalize.scm 557 */
																						obj_t BgL_arg1831z00_3037;

																						BgL_arg1831z00_3037 =
																							CDR(((obj_t) BgL_l1129z00_3026));
																						{
																							obj_t BgL_tail1132z00_5492;
																							obj_t BgL_l1129z00_5491;

																							BgL_l1129z00_5491 =
																								BgL_arg1831z00_3037;
																							BgL_tail1132z00_5492 =
																								BgL_newtail1133z00_3034;
																							BgL_tail1132z00_3027 =
																								BgL_tail1132z00_5492;
																							BgL_l1129z00_3026 =
																								BgL_l1129z00_5491;
																							goto
																								BgL_zc3z04anonymousza31828ze3z87_3025;
																						}
																					}
																				}
																		}
																	}
																}
															{	/* Match/normalize.scm 559 */
																obj_t BgL_arg1822z00_1927;

																{	/* Match/normalize.scm 559 */
																	obj_t BgL_arg1823z00_1928;

																	BgL_arg1823z00_1928 =
																		MAKE_YOUNG_PAIR(BgL_predz00_1907,
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_fieldsz00_1908, BNIL));
																	BgL_arg1822z00_1927 =
																		MAKE_YOUNG_PAIR(BgL_namez00_1905,
																		BgL_arg1823z00_1928);
																}
																return
																	(BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7
																	=
																	MAKE_YOUNG_PAIR(BgL_arg1822z00_1927,
																		BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7),
																	BUNSPEC);
															}
														}
													}
												else
													{	/* Match/normalize.scm 555 */
														return
															BGl_errorz00zz__errorz00
															(BGl_string2323z00zz__match_normaliza7eza7,
															BgL_expz00_57,
															BGl_symbol2324z00zz__match_normaliza7eza7);
													}
											}
										else
											{	/* Match/normalize.scm 555 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2323z00zz__match_normaliza7eza7,
													BgL_expz00_57,
													BGl_symbol2324z00zz__match_normaliza7eza7);
											}
									}
								else
									{	/* Match/normalize.scm 555 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string2323z00zz__match_normaliza7eza7, BgL_expz00_57,
											BGl_symbol2324z00zz__match_normaliza7eza7);
									}
							}
						else
							{	/* Match/normalize.scm 555 */
								return
									BGl_errorz00zz__errorz00
									(BGl_string2323z00zz__match_normaliza7eza7, BgL_expz00_57,
									BGl_symbol2324z00zz__match_normaliza7eza7);
							}
					}
				else
					{	/* Match/normalize.scm 555 */
						return
							BGl_errorz00zz__errorz00
							(BGl_string2323z00zz__match_normaliza7eza7, BgL_expz00_57,
							BGl_symbol2324z00zz__match_normaliza7eza7);
					}
			}
		}

	}



/* &match-define-record-type! */
	obj_t
		BGl_z62matchzd2definezd2recordzd2typez12za2zz__match_normaliza7eza7(obj_t
		BgL_envz00_3659, obj_t BgL_expz00_3660)
	{
		{	/* Match/normalize.scm 554 */
			return
				BGl_matchzd2definezd2recordzd2typez12zc0zz__match_normaliza7eza7
				(BgL_expz00_3660);
		}

	}



/* standardize-struct */
	obj_t BGl_standardiza7ezd2structz75zz__match_normaliza7eza7(obj_t BgL_ez00_58)
	{
		{	/* Match/normalize.scm 567 */
			{	/* Match/normalize.scm 568 */
				obj_t BgL_zc3z04anonymousza31836ze3z87_3661;

				BgL_zc3z04anonymousza31836ze3z87_3661 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31836ze3ze5zz__match_normaliza7eza7,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31836ze3z87_3661, (int) (0L),
					BgL_ez00_58);
				return BgL_zc3z04anonymousza31836ze3z87_3661;
			}
		}

	}



/* &<@anonymous:1836> */
	obj_t BGl_z62zc3z04anonymousza31836ze3ze5zz__match_normaliza7eza7(obj_t
		BgL_envz00_3662, obj_t BgL_rz00_3664, obj_t BgL_cz00_3665)
	{
		{	/* Match/normalize.scm 568 */
			{	/* Match/normalize.scm 572 */
				obj_t BgL_ez00_3663;

				BgL_ez00_3663 = ((obj_t) PROCEDURE_REF(BgL_envz00_3662, (int) (0L)));
				{
					obj_t BgL_providedzd2fieldszd2_3933;

					{	/* Match/normalize.scm 579 */
						obj_t BgL_fz00_3941;

						BgL_fz00_3941 =
							BGl_structzd2ze3listz31zz__structurez00(BgL_ez00_3663);
						{	/* Match/normalize.scm 581 */
							obj_t BgL_structurez00_3942;

							{	/* Match/normalize.scm 582 */
								bool_t BgL_test2502z00_5516;

								{	/* Match/normalize.scm 582 */
									obj_t BgL_tmpz00_5517;

									BgL_tmpz00_5517 = CAR(BgL_fz00_3941);
									BgL_test2502z00_5516 = PAIRP(BgL_tmpz00_5517);
								}
								if (BgL_test2502z00_5516)
									{	/* Match/normalize.scm 583 */
										obj_t BgL_arg1859z00_3943;

										{	/* Match/normalize.scm 583 */
											obj_t BgL_l1141z00_3944;

											BgL_l1141z00_3944 = CDR(BgL_fz00_3941);
											if (NULLP(BgL_l1141z00_3944))
												{	/* Match/normalize.scm 583 */
													BgL_arg1859z00_3943 = BNIL;
												}
											else
												{	/* Match/normalize.scm 583 */
													obj_t BgL_head1143z00_3945;

													{	/* Match/normalize.scm 583 */
														obj_t BgL_arg1868z00_3946;

														{	/* Match/normalize.scm 583 */
															obj_t BgL_pairz00_3947;

															BgL_pairz00_3947 =
																CAR(((obj_t) BgL_l1141z00_3944));
															BgL_arg1868z00_3946 = CAR(BgL_pairz00_3947);
														}
														BgL_head1143z00_3945 =
															MAKE_YOUNG_PAIR(BgL_arg1868z00_3946, BNIL);
													}
													{	/* Match/normalize.scm 583 */
														obj_t BgL_g1146z00_3948;

														BgL_g1146z00_3948 =
															CDR(((obj_t) BgL_l1141z00_3944));
														{
															obj_t BgL_l1141z00_3950;
															obj_t BgL_tail1144z00_3951;

															BgL_l1141z00_3950 = BgL_g1146z00_3948;
															BgL_tail1144z00_3951 = BgL_head1143z00_3945;
														BgL_zc3z04anonymousza31861ze3z87_3949:
															if (NULLP(BgL_l1141z00_3950))
																{	/* Match/normalize.scm 583 */
																	BgL_arg1859z00_3943 = BgL_head1143z00_3945;
																}
															else
																{	/* Match/normalize.scm 583 */
																	obj_t BgL_newtail1145z00_3952;

																	{	/* Match/normalize.scm 583 */
																		obj_t BgL_arg1864z00_3953;

																		{	/* Match/normalize.scm 583 */
																			obj_t BgL_pairz00_3954;

																			BgL_pairz00_3954 =
																				CAR(((obj_t) BgL_l1141z00_3950));
																			BgL_arg1864z00_3953 =
																				CAR(BgL_pairz00_3954);
																		}
																		BgL_newtail1145z00_3952 =
																			MAKE_YOUNG_PAIR(BgL_arg1864z00_3953,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1144z00_3951,
																		BgL_newtail1145z00_3952);
																	{	/* Match/normalize.scm 583 */
																		obj_t BgL_arg1863z00_3955;

																		BgL_arg1863z00_3955 =
																			CDR(((obj_t) BgL_l1141z00_3950));
																		{
																			obj_t BgL_tail1144z00_5539;
																			obj_t BgL_l1141z00_5538;

																			BgL_l1141z00_5538 = BgL_arg1863z00_3955;
																			BgL_tail1144z00_5539 =
																				BgL_newtail1145z00_3952;
																			BgL_tail1144z00_3951 =
																				BgL_tail1144z00_5539;
																			BgL_l1141z00_3950 = BgL_l1141z00_5538;
																			goto
																				BgL_zc3z04anonymousza31861ze3z87_3949;
																		}
																	}
																}
														}
													}
												}
										}
										BgL_providedzd2fieldszd2_3933 = BgL_arg1859z00_3943;
										{
											obj_t BgL_sz00_3935;

											BgL_sz00_3935 =
												BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7;
										BgL_loop1z00_3934:
											{
												obj_t BgL_pzd2fzd2_3937;

												BgL_pzd2fzd2_3937 = BgL_providedzd2fieldszd2_3933;
											BgL_loop2z00_3936:
												if (NULLP(BgL_sz00_3935))
													{	/* Match/normalize.scm 572 */
														BgL_structurez00_3942 =
															BGl_errorz00zz__errorz00
															(BGl_string2328z00zz__match_normaliza7eza7,
															BgL_providedzd2fieldszd2_3933, BNIL);
													}
												else
													{	/* Match/normalize.scm 572 */
														if (NULLP(BgL_pzd2fzd2_3937))
															{	/* Match/normalize.scm 574 */
																BgL_structurez00_3942 =
																	CAR(((obj_t) BgL_sz00_3935));
															}
														else
															{	/* Match/normalize.scm 576 */
																bool_t BgL_test2507z00_5547;

																{	/* Match/normalize.scm 576 */
																	obj_t BgL_tmpz00_5548;

																	{	/* Match/normalize.scm 576 */
																		obj_t BgL_auxz00_5549;

																		{	/* Match/normalize.scm 576 */
																			obj_t BgL_pairz00_3938;

																			BgL_pairz00_3938 =
																				CAR(((obj_t) BgL_sz00_3935));
																			BgL_auxz00_5549 = CDR(BgL_pairz00_3938);
																		}
																		BgL_tmpz00_5548 =
																			BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																			(CAR(((obj_t) BgL_pzd2fzd2_3937)),
																			BgL_auxz00_5549);
																	}
																	BgL_test2507z00_5547 = CBOOL(BgL_tmpz00_5548);
																}
																if (BgL_test2507z00_5547)
																	{	/* Match/normalize.scm 577 */
																		obj_t BgL_arg1885z00_3939;

																		BgL_arg1885z00_3939 =
																			CDR(((obj_t) BgL_pzd2fzd2_3937));
																		{
																			obj_t BgL_pzd2fzd2_5559;

																			BgL_pzd2fzd2_5559 = BgL_arg1885z00_3939;
																			BgL_pzd2fzd2_3937 = BgL_pzd2fzd2_5559;
																			goto BgL_loop2z00_3936;
																		}
																	}
																else
																	{	/* Match/normalize.scm 578 */
																		obj_t BgL_arg1887z00_3940;

																		BgL_arg1887z00_3940 =
																			CDR(((obj_t) BgL_sz00_3935));
																		{
																			obj_t BgL_sz00_5562;

																			BgL_sz00_5562 = BgL_arg1887z00_3940;
																			BgL_sz00_3935 = BgL_sz00_5562;
																			goto BgL_loop1z00_3934;
																		}
																	}
															}
													}
											}
										}
									}
								else
									{	/* Match/normalize.scm 584 */
										bool_t BgL_test2508z00_5563;

										{	/* Match/normalize.scm 584 */
											obj_t BgL_arg1874z00_3956;

											BgL_arg1874z00_3956 = CAR(BgL_fz00_3941);
											BgL_test2508z00_5563 =
												CBOOL(BGl_assocz00zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1874z00_3956,
													BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7));
										}
										if (BgL_test2508z00_5563)
											{	/* Match/normalize.scm 585 */
												obj_t BgL_arg1872z00_3957;

												BgL_arg1872z00_3957 = CAR(BgL_fz00_3941);
												BgL_structurez00_3942 =
													BGl_assocz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1872z00_3957,
													BGl_za2Matchzd2Structuresza2zd2zz__match_normaliza7eza7);
											}
										else
											{	/* Match/normalize.scm 588 */
												obj_t BgL_arg1873z00_3958;

												BgL_arg1873z00_3958 = CAR(BgL_fz00_3941);
												BgL_structurez00_3942 =
													BGl_errorz00zz__errorz00
													(BGl_symbol2329z00zz__match_normaliza7eza7,
													BGl_string2331z00zz__match_normaliza7eza7,
													BgL_arg1873z00_3958);
											}
									}
							}
							{	/* Match/normalize.scm 582 */
								obj_t BgL_namez00_3959;

								BgL_namez00_3959 = CAR(((obj_t) BgL_structurez00_3942));
								{	/* Match/normalize.scm 589 */
									obj_t BgL_predz00_3960;

									{	/* Match/normalize.scm 590 */
										obj_t BgL_pairz00_3961;

										BgL_pairz00_3961 = CDR(((obj_t) BgL_structurez00_3942));
										BgL_predz00_3960 = CAR(BgL_pairz00_3961);
									}
									{	/* Match/normalize.scm 590 */
										obj_t BgL_fieldsz00_3962;

										{	/* Match/normalize.scm 591 */
											obj_t BgL_pairz00_3963;

											BgL_pairz00_3963 = CDR(((obj_t) BgL_structurez00_3942));
											BgL_fieldsz00_3962 = CDR(BgL_pairz00_3963);
										}
										{	/* Match/normalize.scm 591 */
											obj_t BgL_providedzd2fieldszd2_3964;

											{	/* Match/normalize.scm 592 */
												bool_t BgL_test2509z00_5579;

												{	/* Match/normalize.scm 592 */
													obj_t BgL_tmpz00_5580;

													BgL_tmpz00_5580 = CAR(BgL_fz00_3941);
													BgL_test2509z00_5579 = PAIRP(BgL_tmpz00_5580);
												}
												if (BgL_test2509z00_5579)
													{	/* Match/normalize.scm 592 */
														BgL_providedzd2fieldszd2_3964 = BgL_fz00_3941;
													}
												else
													{	/* Match/normalize.scm 592 */
														BgL_providedzd2fieldszd2_3964 = CDR(BgL_fz00_3941);
													}
											}
											{	/* Match/normalize.scm 592 */
												obj_t BgL_patternz00_3965;

												{	/* Match/normalize.scm 604 */
													obj_t BgL_arg1838z00_3966;

													{	/* Match/normalize.scm 604 */
														obj_t BgL_arg1839z00_3967;

														{	/* Match/normalize.scm 604 */
															obj_t BgL_arg1840z00_3968;

															{	/* Match/normalize.scm 604 */
																obj_t BgL_arg1842z00_3969;

																{	/* Match/normalize.scm 604 */
																	bool_t BgL_test2510z00_5584;

																	{	/* Match/normalize.scm 604 */
																		obj_t BgL_tmpz00_5585;

																		BgL_tmpz00_5585 = CAR(BgL_fz00_3941);
																		BgL_test2510z00_5584 =
																			PAIRP(BgL_tmpz00_5585);
																	}
																	if (BgL_test2510z00_5584)
																		{	/* Match/normalize.scm 604 */
																			if (NULLP(BgL_fieldsz00_3962))
																				{	/* Match/normalize.scm 605 */
																					BgL_arg1842z00_3969 = BNIL;
																				}
																			else
																				{	/* Match/normalize.scm 605 */
																					obj_t BgL_head1149z00_3970;

																					BgL_head1149z00_3970 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1147z00_3972;
																						obj_t BgL_tail1150z00_3973;

																						BgL_l1147z00_3972 =
																							BgL_fieldsz00_3962;
																						BgL_tail1150z00_3973 =
																							BgL_head1149z00_3970;
																					BgL_zc3z04anonymousza31846ze3z87_3971:
																						if (NULLP
																							(BgL_l1147z00_3972))
																							{	/* Match/normalize.scm 605 */
																								BgL_arg1842z00_3969 =
																									CDR(BgL_head1149z00_3970);
																							}
																						else
																							{	/* Match/normalize.scm 605 */
																								obj_t BgL_newtail1151z00_3974;

																								{	/* Match/normalize.scm 605 */
																									obj_t BgL_arg1849z00_3975;

																									{	/* Match/normalize.scm 605 */
																										obj_t BgL_fieldz00_3976;

																										BgL_fieldz00_3976 =
																											CAR(
																											((obj_t)
																												BgL_l1147z00_3972));
																										if (CBOOL
																											(BGl_assocz00zz__r4_pairs_and_lists_6_3z00
																												(BgL_fieldz00_3976,
																													BgL_providedzd2fieldszd2_3964)))
																											{	/* Match/normalize.scm 607 */
																												obj_t BgL_pairz00_3977;

																												BgL_pairz00_3977 =
																													BGl_assocz00zz__r4_pairs_and_lists_6_3z00
																													(BgL_fieldz00_3976,
																													BgL_providedzd2fieldszd2_3964);
																												BgL_arg1849z00_3975 =
																													CAR(CDR
																													(BgL_pairz00_3977));
																											}
																										else
																											{	/* Match/normalize.scm 606 */
																												BgL_arg1849z00_3975 =
																													BGl_symbol2263z00zz__match_normaliza7eza7;
																											}
																									}
																									BgL_newtail1151z00_3974 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1849z00_3975, BNIL);
																								}
																								SET_CDR(BgL_tail1150z00_3973,
																									BgL_newtail1151z00_3974);
																								{	/* Match/normalize.scm 605 */
																									obj_t BgL_arg1848z00_3978;

																									BgL_arg1848z00_3978 =
																										CDR(
																										((obj_t)
																											BgL_l1147z00_3972));
																									{
																										obj_t BgL_tail1150z00_5607;
																										obj_t BgL_l1147z00_5606;

																										BgL_l1147z00_5606 =
																											BgL_arg1848z00_3978;
																										BgL_tail1150z00_5607 =
																											BgL_newtail1151z00_3974;
																										BgL_tail1150z00_3973 =
																											BgL_tail1150z00_5607;
																										BgL_l1147z00_3972 =
																											BgL_l1147z00_5606;
																										goto
																											BgL_zc3z04anonymousza31846ze3z87_3971;
																									}
																								}
																							}
																					}
																				}
																		}
																	else
																		{	/* Match/normalize.scm 604 */
																			BgL_arg1842z00_3969 = CDR(BgL_fz00_3941);
																		}
																}
																BgL_arg1840z00_3968 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_arg1842z00_3969, BNIL);
															}
															BgL_arg1839z00_3967 =
																MAKE_YOUNG_PAIR(BgL_predz00_3960,
																BgL_arg1840z00_3968);
														}
														BgL_arg1838z00_3966 =
															MAKE_YOUNG_PAIR(BgL_namez00_3959,
															BgL_arg1839z00_3967);
													}
													BgL_patternz00_3965 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2240z00zz__match_normaliza7eza7,
														BgL_arg1838z00_3966);
												}
												{	/* Match/normalize.scm 600 */

													{	/* Match/normalize.scm 611 */
														obj_t BgL_fun1837z00_3979;

														BgL_fun1837z00_3979 =
															BGl_standardiza7ezd2patternz75zz__match_normaliza7eza7
															(BgL_patternz00_3965);
														return BGL_PROCEDURE_CALL2(BgL_fun1837z00_3979,
															BgL_rz00_3664, BgL_cz00_3665);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__match_normaliza7eza7(void)
	{
		{	/* Match/normalize.scm 2 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2332z00zz__match_normaliza7eza7));
			BGl_modulezd2initializa7ationz75zz__match_s2cfunz00(509060701L,
				BSTRING_TO_STRING(BGl_string2332z00zz__match_normaliza7eza7));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2332z00zz__match_normaliza7eza7));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2332z00zz__match_normaliza7eza7));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2332z00zz__match_normaliza7eza7));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2332z00zz__match_normaliza7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
