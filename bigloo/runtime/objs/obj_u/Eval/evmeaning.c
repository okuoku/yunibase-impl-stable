/*===========================================================================*/
/*   (Eval/evmeaning.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evmeaning.scm -indent -o objs/obj_u/Eval/evmeaning.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVMEANING_TYPE_DEFINITIONS
#define BGL___EVMEANING_TYPE_DEFINITIONS
#endif													// BGL___EVMEANING_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_initzd2thezd2globalzd2environmentz12zc0zz__evenvz00(void);
	static obj_t BGl_z62zc3z04anonymousza31701ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2bouncezd2175z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd2176z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2tailcallzd24zd2stackzd2zz__evmeaningz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31935ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31676ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31710ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31621ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evmeaningz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31944ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd214z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd217z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd218z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd225z00zz__evmeaningz00(obj_t);
	static obj_t BGl_evmeaningzd2bouncezd226z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd227z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd228z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd229z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evmeaningz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_evalzd2modulezd2zz__evmodulez00(void);
	static obj_t BGl_evmeaningzd2uninitializa7edz75zz__evmeaningz00(obj_t, obj_t);
	static obj_t BGl_evmeaningzd2funcallzd20z00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2funcallzd21z00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2funcallzd22z00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2funcallzd23z00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2funcallzd24z00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2makezd24procedurez00zz__evmeaningz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2bouncezd230z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd236z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd237z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd238z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd239z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__evmeaningz00(void);
	static obj_t BGl_z62zc3z04anonymousza31640ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evmeaningzd2tailcallzd22zd2stackzd2zz__evmeaningz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_evmeaningzd2bouncezd240z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd241z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd242z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd243z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd244z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd245z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd246z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd247z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd248z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd249z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t eval_funcall_0(obj_t);
	extern obj_t eval_funcall_1(obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t eval_funcall_2(obj_t, obj_t, obj_t);
	extern obj_t eval_funcall_3(obj_t, obj_t, obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	extern obj_t eval_funcall_4(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31706ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd250z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd251z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd252z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd253z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evmeaningz00(void);
	static obj_t BGl_evmeaningzd2bouncezd254z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31689ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_evarityzd2errorzd2zz__everrorz00(obj_t, obj_t, int, int);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__evmeaningz00(void);
	static obj_t BGl_z62evmeaningz62zz__evmeaningz00(obj_t, obj_t, obj_t, obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evmeaningz00(void);
	static obj_t BGl_z62zc3z04anonymousza31650ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_evmeaningzd2makezd2tracedzd24procedurezd2zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__evmeaningz00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zz__evmeaningz00(obj_t);
	extern obj_t BGl_evwarningz00zz__everrorz00(obj_t, obj_t);
	extern obj_t BGl_evtypezd2errorzd2zz__everrorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__evmeaningz00(void);
	static obj_t BGl_evmeaningzd2bouncezd264z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_evmeaningzd2bouncezd267z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_evmeaningzd2bouncezd268z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_evmeaningzd2unboundzd2zz__evmeaningz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_evmodulezd2findzd2globalz00zz__evmodulez00(obj_t, obj_t);
	extern bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31587ze3ze70z60zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31716ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31635ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31627ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31619ze3ze5zz__evmeaningz00(obj_t);
	static obj_t BGl_evmeaningzd2bouncezd270z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd271z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_symbol2321z00zz__evmeaningz00 = BUNSPEC;
	static obj_t BGl_symbol2325z00zz__evmeaningz00 = BUNSPEC;
	static obj_t BGl_symbol2327z00zz__evmeaningz00 = BUNSPEC;
	static obj_t BGl_symbol2329z00zz__evmeaningz00 = BUNSPEC;
	extern obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	static obj_t BGl_evmeaningzd2tailcallzd20zd2stackzd2zz__evmeaningz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31725ze3ze5zz__evmeaningz00(obj_t);
	static obj_t BGl_evmeaningzd2pushzd2fxargsz00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31645ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmeaningz00zz__evmeaningz00(obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_evmeaningzd2bouncezd27z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmeaningzd2bouncezd29z00zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evmodulezd2namezd2zz__evmodulez00(obj_t);
	static obj_t BGl_evmeaningzd2tailcallzd23zd2stackzd2zz__evmeaningz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31654ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31719ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zz__evmeaningz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__evmeaningz00(void);
	extern obj_t BGl_everrorz00zz__everrorz00(obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31754ze3ze70z60zz__evmeaningz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62__dummy__z62zz__evmeaningz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31664ze3ze5zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_evmeaningzd2pushzd2vaargsz00zz__evmeaningz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl__loop_ze70ze7zz__evmeaningz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl__loop_ze71ze7zz__evmeaningz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00(obj_t, obj_t,
		obj_t, obj_t);
	extern bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_evmeaningzd2tailcallzd21zd2stackzd2zz__evmeaningz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31933ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31658ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	extern obj_t eval_apply(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31942ze3ze5zz__evmeaningz00(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evmeaningzd2envzd2zz__evmeaningz00,
		BgL_bgl_za762evmeaningza762za72345za7, BGl_z62evmeaningz62zz__evmeaningz00,
		0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2316z00zz__evmeaningz00,
		BgL_bgl_string2316za700za7za7_2346za7,
		"Unbound variable (from module `~a')", 35);
	      DEFINE_STRING(BGl_string2317z00zz__evmeaningz00,
		BgL_bgl_string2317za700za7za7_2347za7, "Unbound variable (from top-level)",
		33);
	      DEFINE_STRING(BGl_string2318z00zz__evmeaningz00,
		BgL_bgl_string2318za700za7za7_2348za7, "eval", 4);
	      DEFINE_STRING(BGl_string2319z00zz__evmeaningz00,
		BgL_bgl_string2319za700za7za7_2349za7, "Uninitialized variable", 22);
	      DEFINE_STRING(BGl_string2320z00zz__evmeaningz00,
		BgL_bgl_string2320za700za7za7_2350za7, "unknown byte-code", 17);
	      DEFINE_STRING(BGl_string2322z00zz__evmeaningz00,
		BgL_bgl_string2322za700za7za7_2351za7, "evprocedure", 11);
	      DEFINE_STRING(BGl_string2323z00zz__evmeaningz00,
		BgL_bgl_string2323za700za7za7_2352za7, "apply", 5);
	      DEFINE_STRING(BGl_string2324z00zz__evmeaningz00,
		BgL_bgl_string2324za700za7za7_2353za7, "Not a procedure", 15);
	      DEFINE_STRING(BGl_string2326z00zz__evmeaningz00,
		BgL_bgl_string2326za700za7za7_2354za7, "number", 6);
	      DEFINE_STRING(BGl_string2328z00zz__evmeaningz00,
		BgL_bgl_string2328za700za7za7_2355za7, "fixnum", 6);
	      DEFINE_STRING(BGl_string2330z00zz__evmeaningz00,
		BgL_bgl_string2330za700za7za7_2356za7, "pair", 4);
	      DEFINE_STRING(BGl_string2331z00zz__evmeaningz00,
		BgL_bgl_string2331za700za7za7_2357za7,
		"/tmp/bigloo/runtime/Eval/evmeaning.scm", 38);
	      DEFINE_STRING(BGl_string2332z00zz__evmeaningz00,
		BgL_bgl_string2332za700za7za7_2358za7, "&evmeaning", 10);
	      DEFINE_STRING(BGl_string2333z00zz__evmeaningz00,
		BgL_bgl_string2333za700za7za7_2359za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2334z00zz__evmeaningz00,
		BgL_bgl_string2334za700za7za7_2360za7, "dynamic-env", 11);
	      DEFINE_STRING(BGl_string2335z00zz__evmeaningz00,
		BgL_bgl_string2335za700za7za7_2361za7, "synchronize", 11);
	      DEFINE_STRING(BGl_string2336z00zz__evmeaningz00,
		BgL_bgl_string2336za700za7za7_2362za7, "mutex", 5);
	      DEFINE_STRING(BGl_string2337z00zz__evmeaningz00,
		BgL_bgl_string2337za700za7za7_2363za7, "with-handler", 12);
	      DEFINE_STRING(BGl_string2338z00zz__evmeaningz00,
		BgL_bgl_string2338za700za7za7_2364za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2339z00zz__evmeaningz00,
		BgL_bgl_string2339za700za7za7_2365za7,
		"Read-only variable cannot be redefined", 38);
	      DEFINE_STRING(BGl_string2340z00zz__evmeaningz00,
		BgL_bgl_string2340za700za7za7_2366za7,
		"Compiled read-only variable cannot be redefined", 47);
	      DEFINE_STRING(BGl_string2341z00zz__evmeaningz00,
		BgL_bgl_string2341za700za7za7_2367za7,
		"\nRedefinition of compiled variable -- ", 38);
	      DEFINE_STRING(BGl_string2342z00zz__evmeaningz00,
		BgL_bgl_string2342za700za7za7_2368za7, "set!", 4);
	      DEFINE_STRING(BGl_string2343z00zz__evmeaningz00,
		BgL_bgl_string2343za700za7za7_2369za7, "read-only variable", 18);
	      DEFINE_STRING(BGl_string2344z00zz__evmeaningz00,
		BgL_bgl_string2344za700za7za7_2370za7, "__evmeaning", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evmeaningz00));
		     ADD_ROOT((void *) (&BGl_symbol2321z00zz__evmeaningz00));
		     ADD_ROOT((void *) (&BGl_symbol2325z00zz__evmeaningz00));
		     ADD_ROOT((void *) (&BGl_symbol2327z00zz__evmeaningz00));
		     ADD_ROOT((void *) (&BGl_symbol2329z00zz__evmeaningz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evmeaningz00(long
		BgL_checksumz00_4208, char *BgL_fromz00_4209)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evmeaningz00))
				{
					BGl_requirezd2initializa7ationz75zz__evmeaningz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evmeaningz00();
					BGl_cnstzd2initzd2zz__evmeaningz00();
					BGl_importedzd2moduleszd2initz00zz__evmeaningz00();
					return BGl_toplevelzd2initzd2zz__evmeaningz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			BGl_symbol2321z00zz__evmeaningz00 =
				bstring_to_symbol(BGl_string2322z00zz__evmeaningz00);
			BGl_symbol2325z00zz__evmeaningz00 =
				bstring_to_symbol(BGl_string2326z00zz__evmeaningz00);
			BGl_symbol2327z00zz__evmeaningz00 =
				bstring_to_symbol(BGl_string2328z00zz__evmeaningz00);
			return (BGl_symbol2329z00zz__evmeaningz00 =
				bstring_to_symbol(BGl_string2330z00zz__evmeaningz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			BGL_TAIL return BGl_initzd2thezd2globalzd2environmentz12zc0zz__evenvz00();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__evmeaningz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1391;

				BgL_headz00_1391 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1392;
					obj_t BgL_tailz00_1393;

					BgL_prevz00_1392 = BgL_headz00_1391;
					BgL_tailz00_1393 = BgL_l1z00_1;
				BgL_loopz00_1394:
					if (PAIRP(BgL_tailz00_1393))
						{
							obj_t BgL_newzd2prevzd2_1396;

							BgL_newzd2prevzd2_1396 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1393), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1392, BgL_newzd2prevzd2_1396);
							{
								obj_t BgL_tailz00_4230;
								obj_t BgL_prevz00_4229;

								BgL_prevz00_4229 = BgL_newzd2prevzd2_1396;
								BgL_tailz00_4230 = CDR(BgL_tailz00_1393);
								BgL_tailz00_1393 = BgL_tailz00_4230;
								BgL_prevz00_1392 = BgL_prevz00_4229;
								goto BgL_loopz00_1394;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1391);
				}
			}
		}

	}



/* evmeaning-unbound */
	obj_t BGl_evmeaningzd2unboundzd2zz__evmeaningz00(obj_t BgL_locz00_3,
		obj_t BgL_namez00_4, obj_t BgL_modz00_5)
	{
		{	/* Eval/evmeaning.scm 165 */
			{	/* Eval/evmeaning.scm 168 */
				obj_t BgL_arg1305z00_1399;

				if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_modz00_5))
					{	/* Eval/evmeaning.scm 170 */
						obj_t BgL_arg1307z00_1401;

						BgL_arg1307z00_1401 =
							BGl_evmodulezd2namezd2zz__evmodulez00(BgL_modz00_5);
						{	/* Eval/evmeaning.scm 169 */
							obj_t BgL_list1308z00_1402;

							BgL_list1308z00_1402 = MAKE_YOUNG_PAIR(BgL_arg1307z00_1401, BNIL);
							BgL_arg1305z00_1399 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2316z00zz__evmeaningz00, BgL_list1308z00_1402);
						}
					}
				else
					{	/* Eval/evmeaning.scm 168 */
						BgL_arg1305z00_1399 = BGl_string2317z00zz__evmeaningz00;
					}
				return
					BGl_everrorz00zz__everrorz00(BgL_locz00_3,
					BGl_string2318z00zz__evmeaningz00, BgL_arg1305z00_1399,
					BgL_namez00_4);
			}
		}

	}



/* evmeaning-uninitialized */
	obj_t BGl_evmeaningzd2uninitializa7edz75zz__evmeaningz00(obj_t BgL_locz00_6,
		obj_t BgL_namez00_7)
	{
		{	/* Eval/evmeaning.scm 177 */
			return
				BGl_everrorz00zz__everrorz00(BgL_locz00_6,
				BGl_string2318z00zz__evmeaningz00, BGl_string2319z00zz__evmeaningz00,
				BgL_namez00_7);
		}

	}



/* evmeaning */
	BGL_EXPORTED_DEF obj_t BGl_evmeaningz00zz__evmeaningz00(obj_t BgL_codez00_8,
		obj_t BgL_stackz00_9, obj_t BgL_denvz00_10)
	{
		{	/* Eval/evmeaning.scm 183 */
		BGl_evmeaningz00zz__evmeaningz00:
			if (VECTORP(BgL_codez00_8))
				{

					{	/* Eval/evmeaning.scm 186 */
						obj_t BgL_aux1079z00_1407;

						BgL_aux1079z00_1407 = VECTOR_REF(BgL_codez00_8, 0L);
						if (INTEGERP(BgL_aux1079z00_1407))
							{	/* Eval/evmeaning.scm 186 */
								switch ((long) CINT(BgL_aux1079z00_1407))
									{
									case -2L:

										{	/* Eval/evmeaning.scm 189 */
											obj_t BgL_runner1317z00_1415;

											{	/* Eval/evmeaning.scm 189 */
												obj_t BgL_arg1310z00_1408;
												obj_t BgL_arg1311z00_1409;

												BgL_arg1310z00_1408 = VECTOR_REF(BgL_codez00_8, 1L);
												BgL_arg1311z00_1409 = VECTOR_REF(BgL_codez00_8, 2L);
												{	/* Eval/evmeaning.scm 189 */
													obj_t BgL_list1312z00_1410;

													BgL_list1312z00_1410 =
														MAKE_YOUNG_PAIR(BgL_arg1311z00_1409, BNIL);
													BgL_runner1317z00_1415 =
														BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1310z00_1408, BgL_list1312z00_1410);
												}
											}
											{	/* Eval/evmeaning.scm 189 */
												obj_t BgL_aux1313z00_1411;

												BgL_aux1313z00_1411 = CAR(BgL_runner1317z00_1415);
												BgL_runner1317z00_1415 = CDR(BgL_runner1317z00_1415);
												{	/* Eval/evmeaning.scm 189 */
													obj_t BgL_aux1314z00_1412;

													BgL_aux1314z00_1412 = CAR(BgL_runner1317z00_1415);
													BgL_runner1317z00_1415 = CDR(BgL_runner1317z00_1415);
													{	/* Eval/evmeaning.scm 189 */
														obj_t BgL_aux1315z00_1413;

														BgL_aux1315z00_1413 = CAR(BgL_runner1317z00_1415);
														BgL_runner1317z00_1415 =
															CDR(BgL_runner1317z00_1415);
														return
															BGl_everrorz00zz__everrorz00(BgL_aux1313z00_1411,
															BgL_aux1314z00_1412, BgL_aux1315z00_1413,
															CAR(BgL_runner1317z00_1415));
													}
												}
											}
										}
										break;
									case -1L:

										return VECTOR_REF(BgL_codez00_8, 2L);
										break;
									case 0L:

										return CAR(((obj_t) BgL_stackz00_9));
										break;
									case 1L:

										{	/* Eval/evmeaning.scm 198 */
											obj_t BgL_pairz00_2883;

											BgL_pairz00_2883 = CDR(((obj_t) BgL_stackz00_9));
											return CAR(BgL_pairz00_2883);
										}
										break;
									case 2L:

										{	/* Eval/evmeaning.scm 201 */
											obj_t BgL_pairz00_2889;

											{	/* Eval/evmeaning.scm 201 */
												obj_t BgL_pairz00_2888;

												BgL_pairz00_2888 = CDR(((obj_t) BgL_stackz00_9));
												BgL_pairz00_2889 = CDR(BgL_pairz00_2888);
											}
											return CAR(BgL_pairz00_2889);
										}
										break;
									case 3L:

										{	/* Eval/evmeaning.scm 204 */
											obj_t BgL_pairz00_2897;

											{	/* Eval/evmeaning.scm 204 */
												obj_t BgL_pairz00_2896;

												{	/* Eval/evmeaning.scm 204 */
													obj_t BgL_pairz00_2895;

													BgL_pairz00_2895 = CDR(((obj_t) BgL_stackz00_9));
													BgL_pairz00_2896 = CDR(BgL_pairz00_2895);
												}
												BgL_pairz00_2897 = CDR(BgL_pairz00_2896);
											}
											return CAR(BgL_pairz00_2897);
										}
										break;
									case 4L:

										{	/* Eval/evmeaning.scm 207 */
											obj_t BgL_offsetz00_1416;

											BgL_offsetz00_1416 = VECTOR_REF(BgL_codez00_8, 2L);
											{
												long BgL_iz00_1419;
												obj_t BgL_envz00_1420;

												BgL_iz00_1419 = 4L;
												{	/* Eval/evmeaning.scm 209 */
													obj_t BgL_pairz00_2911;

													{	/* Eval/evmeaning.scm 209 */
														obj_t BgL_pairz00_2910;

														{	/* Eval/evmeaning.scm 209 */
															obj_t BgL_pairz00_2909;

															BgL_pairz00_2909 = CDR(((obj_t) BgL_stackz00_9));
															BgL_pairz00_2910 = CDR(BgL_pairz00_2909);
														}
														BgL_pairz00_2911 = CDR(BgL_pairz00_2910);
													}
													BgL_envz00_1420 = CDR(BgL_pairz00_2911);
												}
											BgL_zc3z04anonymousza31319ze3z87_1421:
												if ((BgL_iz00_1419 == (long) CINT(BgL_offsetz00_1416)))
													{	/* Eval/evmeaning.scm 208 */
														return CAR(((obj_t) BgL_envz00_1420));
													}
												else
													{
														obj_t BgL_envz00_4280;
														long BgL_iz00_4278;

														BgL_iz00_4278 = (BgL_iz00_1419 + 1L);
														BgL_envz00_4280 = CDR(((obj_t) BgL_envz00_1420));
														BgL_envz00_1420 = BgL_envz00_4280;
														BgL_iz00_1419 = BgL_iz00_4278;
														goto BgL_zc3z04anonymousza31319ze3z87_1421;
													}
											}
										}
										break;
									case 5L:

										{	/* Eval/evmeaning.scm 214 */
											obj_t BgL_arg1323z00_1426;

											{	/* Eval/evmeaning.scm 214 */
												obj_t BgL_arg1325z00_1427;

												BgL_arg1325z00_1427 = VECTOR_REF(BgL_codez00_8, 2L);
												BgL_arg1323z00_1426 =
													VECTOR_REF(((obj_t) BgL_arg1325z00_1427), 2L);
											}
											return __EVMEANING_ADDRESS_REF(BgL_arg1323z00_1426);
										}
										break;
									case 6L:

										{	/* Eval/evmeaning.scm 217 */
											obj_t BgL_gz00_1428;

											BgL_gz00_1428 = VECTOR_REF(BgL_codez00_8, 2L);
											{	/* Eval/evmeaning.scm 217 */
												obj_t BgL_valz00_1429;

												BgL_valz00_1429 =
													VECTOR_REF(((obj_t) BgL_gz00_1428), 2L);
												{	/* Eval/evmeaning.scm 218 */

													if ((BgL_valz00_1429 == BUNSPEC))
														{	/* Eval/evmeaning.scm 220 */
															int BgL_tagz00_1430;

															{	/* Eval/evmeaning.scm 220 */
																int BgL_res2285z00_2920;

																BgL_res2285z00_2920 =
																	CINT(VECTOR_REF(((obj_t) BgL_gz00_1428), 0L));
																BgL_tagz00_1430 = BgL_res2285z00_2920;
															}
															{	/* Eval/evmeaning.scm 221 */
																bool_t BgL_test2378z00_4300;

																if (((long) (BgL_tagz00_1430) == 3L))
																	{	/* Eval/evmeaning.scm 221 */
																		BgL_test2378z00_4300 = ((bool_t) 1);
																	}
																else
																	{	/* Eval/evmeaning.scm 221 */
																		BgL_test2378z00_4300 =
																			((long) (BgL_tagz00_1430) == 4L);
																	}
																if (BgL_test2378z00_4300)
																	{	/* Eval/evmeaning.scm 223 */
																		obj_t BgL_arg1328z00_1433;
																		obj_t BgL_arg1329z00_1434;

																		BgL_arg1328z00_1433 =
																			VECTOR_REF(BgL_codez00_8, 1L);
																		BgL_arg1329z00_1434 =
																			VECTOR_REF(((obj_t) BgL_gz00_1428), 1L);
																		return
																			BGl_evmeaningzd2uninitializa7edz75zz__evmeaningz00
																			(BgL_arg1328z00_1433,
																			BgL_arg1329z00_1434);
																	}
																else
																	{	/* Eval/evmeaning.scm 221 */
																		return BgL_valz00_1429;
																	}
															}
														}
													else
														{	/* Eval/evmeaning.scm 219 */
															return BgL_valz00_1429;
														}
												}
											}
										}
										break;
									case 7L:

										return
											BGl_evmeaningzd2bouncezd27z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 8L:

										{	/* Eval/evmeaning.scm 241 */
											obj_t BgL_varz00_1436;
											obj_t BgL_valz00_1437;

											BgL_varz00_1436 = VECTOR_REF(BgL_codez00_8, 2L);
											BgL_valz00_1437 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
											BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00
												(BgL_codez00_8, BgL_varz00_1436, BgL_valz00_1437);
											return BUNSPEC;
										}
										break;
									case 9L:

										return
											BGl_evmeaningzd2bouncezd29z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 10L:

										{	/* Eval/evmeaning.scm 260 */
											obj_t BgL_arg1332z00_1439;

											BgL_arg1332z00_1439 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 2L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 260 */
												obj_t BgL_tmpz00_4318;

												BgL_tmpz00_4318 = ((obj_t) BgL_stackz00_9);
												SET_CAR(BgL_tmpz00_4318, BgL_arg1332z00_1439);
											}
										}
										return BUNSPEC;
										break;
									case 11L:

										{	/* Eval/evmeaning.scm 264 */
											obj_t BgL_arg1334z00_1441;
											obj_t BgL_arg1335z00_1442;

											BgL_arg1334z00_1441 = CDR(((obj_t) BgL_stackz00_9));
											BgL_arg1335z00_1442 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 2L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 264 */
												obj_t BgL_tmpz00_4325;

												BgL_tmpz00_4325 = ((obj_t) BgL_arg1334z00_1441);
												SET_CAR(BgL_tmpz00_4325, BgL_arg1335z00_1442);
											}
										}
										return BUNSPEC;
										break;
									case 12L:

										{	/* Eval/evmeaning.scm 268 */
											obj_t BgL_arg1337z00_1444;
											obj_t BgL_arg1338z00_1445;

											{	/* Eval/evmeaning.scm 268 */
												obj_t BgL_pairz00_2939;

												BgL_pairz00_2939 = CDR(((obj_t) BgL_stackz00_9));
												BgL_arg1337z00_1444 = CDR(BgL_pairz00_2939);
											}
											BgL_arg1338z00_1445 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 2L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 268 */
												obj_t BgL_tmpz00_4333;

												BgL_tmpz00_4333 = ((obj_t) BgL_arg1337z00_1444);
												SET_CAR(BgL_tmpz00_4333, BgL_arg1338z00_1445);
											}
										}
										return BUNSPEC;
										break;
									case 13L:

										{	/* Eval/evmeaning.scm 272 */
											obj_t BgL_arg1340z00_1447;
											obj_t BgL_arg1341z00_1448;

											{	/* Eval/evmeaning.scm 272 */
												obj_t BgL_pairz00_2948;

												{	/* Eval/evmeaning.scm 272 */
													obj_t BgL_pairz00_2947;

													BgL_pairz00_2947 = CDR(((obj_t) BgL_stackz00_9));
													BgL_pairz00_2948 = CDR(BgL_pairz00_2947);
												}
												BgL_arg1340z00_1447 = CDR(BgL_pairz00_2948);
											}
											BgL_arg1341z00_1448 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 2L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 272 */
												obj_t BgL_tmpz00_4342;

												BgL_tmpz00_4342 = ((obj_t) BgL_arg1340z00_1447);
												SET_CAR(BgL_tmpz00_4342, BgL_arg1341z00_1448);
											}
										}
										return BUNSPEC;
										break;
									case 14L:

										return
											BGl_evmeaningzd2bouncezd214z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 15L:

										{	/* Eval/evmeaning.scm 284 */
											bool_t BgL_test2380z00_4346;

											{	/* Eval/evmeaning.scm 284 */
												obj_t BgL_arg1348z00_1454;

												BgL_arg1348z00_1454 = VECTOR_REF(BgL_codez00_8, 2L);
												BgL_test2380z00_4346 =
													CBOOL(BGl_evmeaningz00zz__evmeaningz00
													(BgL_arg1348z00_1454, BgL_stackz00_9,
														BgL_denvz00_10));
											}
											if (BgL_test2380z00_4346)
												{
													obj_t BgL_codez00_4350;

													BgL_codez00_4350 = VECTOR_REF(BgL_codez00_8, 3L);
													BgL_codez00_8 = BgL_codez00_4350;
													goto BGl_evmeaningz00zz__evmeaningz00;
												}
											else
												{
													obj_t BgL_codez00_4352;

													BgL_codez00_4352 = VECTOR_REF(BgL_codez00_8, 4L);
													BgL_codez00_8 = BgL_codez00_4352;
													goto BGl_evmeaningz00zz__evmeaningz00;
												}
										}
										break;
									case 16L:

										{	/* Eval/evmeaning.scm 289 */
											long BgL_lenz00_1455;

											BgL_lenz00_1455 =
												((VECTOR_LENGTH(BgL_codez00_8) - 2L) - 1L);
											{
												long BgL_iz00_1457;

												BgL_iz00_1457 = 0L;
											BgL_zc3z04anonymousza31349ze3z87_1458:
												if ((BgL_iz00_1457 == BgL_lenz00_1455))
													{	/* Eval/evmeaning.scm 292 */
														obj_t BgL_arg1351z00_1460;

														{	/* Eval/evmeaning.scm 292 */
															long BgL_arg1352z00_1461;

															BgL_arg1352z00_1461 = (BgL_iz00_1457 + 2L);
															BgL_arg1351z00_1460 =
																VECTOR_REF(
																((obj_t) BgL_codez00_8), BgL_arg1352z00_1461);
														}
														{
															obj_t BgL_codez00_4362;

															BgL_codez00_4362 = BgL_arg1351z00_1460;
															BgL_codez00_8 = BgL_codez00_4362;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 291 */
														{	/* Eval/evmeaning.scm 294 */
															obj_t BgL_arg1354z00_1462;

															{	/* Eval/evmeaning.scm 294 */
																long BgL_arg1356z00_1463;

																BgL_arg1356z00_1463 = (BgL_iz00_1457 + 2L);
																BgL_arg1354z00_1462 =
																	VECTOR_REF(
																	((obj_t) BgL_codez00_8), BgL_arg1356z00_1463);
															}
															BGl_evmeaningz00zz__evmeaningz00
																(BgL_arg1354z00_1462, BgL_stackz00_9,
																BgL_denvz00_10);
														}
														{
															long BgL_iz00_4367;

															BgL_iz00_4367 = (BgL_iz00_1457 + 1L);
															BgL_iz00_1457 = BgL_iz00_4367;
															goto BgL_zc3z04anonymousza31349ze3z87_1458;
														}
													}
											}
										}
										break;
									case 17L:

										return
											BGl_evmeaningzd2bouncezd217z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 18L:

										return
											BGl_evmeaningzd2bouncezd218z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 25L:

										return
											BGl_evmeaningzd2bouncezd225z00zz__evmeaningz00
											(BgL_codez00_8);
										break;
									case 26L:

										return
											BGl_evmeaningzd2bouncezd226z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 27L:

										return
											BGl_evmeaningzd2bouncezd227z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 28L:

										return
											BGl_evmeaningzd2bouncezd228z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 29L:

										return
											BGl_evmeaningzd2bouncezd229z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 30L:

										return
											BGl_evmeaningzd2bouncezd230z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 31L:

										return
											BGl_evmeaningzd2funcallzd20z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_8,
													3L), BgL_stackz00_9, BgL_denvz00_10));
										break;
									case 32L:

										return
											BGl_evmeaningzd2funcallzd21z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_8,
													3L), BgL_stackz00_9, BgL_denvz00_10));
										break;
									case 33L:

										return
											BGl_evmeaningzd2funcallzd22z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_8,
													3L), BgL_stackz00_9, BgL_denvz00_10));
										break;
									case 34L:

										return
											BGl_evmeaningzd2funcallzd23z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_8,
													3L), BgL_stackz00_9, BgL_denvz00_10));
										break;
									case 35L:

										return
											BGl_evmeaningzd2funcallzd24z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_8,
													3L), BgL_stackz00_9, BgL_denvz00_10));
										break;
									case 36L:

										return
											BGl_evmeaningzd2bouncezd236z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 37L:

										return
											BGl_evmeaningzd2bouncezd237z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 42L:

										return
											BGl_evmeaningzd2bouncezd242z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 38L:

										return
											BGl_evmeaningzd2bouncezd238z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 43L:

										return
											BGl_evmeaningzd2bouncezd243z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 39L:

										return
											BGl_evmeaningzd2bouncezd239z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 44L:

										return
											BGl_evmeaningzd2bouncezd244z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 40L:

										return
											BGl_evmeaningzd2bouncezd240z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 45L:

										return
											BGl_evmeaningzd2bouncezd245z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 41L:

										return
											BGl_evmeaningzd2bouncezd241z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 46L:

										return
											BGl_evmeaningzd2bouncezd246z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 47L:

										return
											BGl_evmeaningzd2bouncezd247z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 51L:

										return
											BGl_evmeaningzd2bouncezd251z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 48L:

										return
											BGl_evmeaningzd2bouncezd248z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 52L:

										return
											BGl_evmeaningzd2bouncezd252z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 49L:

										return
											BGl_evmeaningzd2bouncezd249z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 53L:

										return
											BGl_evmeaningzd2bouncezd253z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 50L:

										return
											BGl_evmeaningzd2bouncezd250z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 54L:

										return
											BGl_evmeaningzd2bouncezd254z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 55L:

										return
											BGl_evmeaningzd2makezd2tracedzd24procedurezd2zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 56L:

										return
											BGl_evmeaningzd2makezd24procedurez00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 64L:

										return
											BGl_evmeaningzd2bouncezd264z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 65L:

										{	/* Eval/evmeaning.scm 706 */
											obj_t BgL_g1081z00_1478;

											BgL_g1081z00_1478 = VECTOR_REF(BgL_codez00_8, 3L);
											{
												obj_t BgL_valsz00_1480;
												obj_t BgL_envz00_1481;

												BgL_valsz00_1480 = BgL_g1081z00_1478;
												BgL_envz00_1481 = BgL_stackz00_9;
											BgL_zc3z04anonymousza31365ze3z87_1482:
												if (NULLP(BgL_valsz00_1480))
													{	/* Eval/evmeaning.scm 709 */
														obj_t BgL_arg1367z00_1484;

														BgL_arg1367z00_1484 =
															VECTOR_REF(((obj_t) BgL_codez00_8), 2L);
														{
															obj_t BgL_stackz00_4420;
															obj_t BgL_codez00_4419;

															BgL_codez00_4419 = BgL_arg1367z00_1484;
															BgL_stackz00_4420 = BgL_envz00_1481;
															BgL_stackz00_9 = BgL_stackz00_4420;
															BgL_codez00_8 = BgL_codez00_4419;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 710 */
														obj_t BgL_vz00_1485;

														{	/* Eval/evmeaning.scm 710 */
															obj_t BgL_arg1370z00_1488;

															BgL_arg1370z00_1488 =
																CAR(((obj_t) BgL_valsz00_1480));
															BgL_vz00_1485 =
																BGl_evmeaningz00zz__evmeaningz00
																(BgL_arg1370z00_1488, BgL_stackz00_9,
																BgL_denvz00_10);
														}
														{	/* Eval/evmeaning.scm 711 */
															obj_t BgL_arg1368z00_1486;
															obj_t BgL_arg1369z00_1487;

															BgL_arg1368z00_1486 =
																CDR(((obj_t) BgL_valsz00_1480));
															BgL_arg1369z00_1487 =
																MAKE_YOUNG_PAIR(BgL_vz00_1485, BgL_envz00_1481);
															{
																obj_t BgL_envz00_4428;
																obj_t BgL_valsz00_4427;

																BgL_valsz00_4427 = BgL_arg1368z00_1486;
																BgL_envz00_4428 = BgL_arg1369z00_1487;
																BgL_envz00_1481 = BgL_envz00_4428;
																BgL_valsz00_1480 = BgL_valsz00_4427;
																goto BgL_zc3z04anonymousza31365ze3z87_1482;
															}
														}
													}
											}
										}
										break;
									case 66L:

										{	/* Eval/evmeaning.scm 714 */
											obj_t BgL_g1082z00_1490;

											BgL_g1082z00_1490 = VECTOR_REF(BgL_codez00_8, 3L);
											{
												obj_t BgL_valsz00_1492;
												obj_t BgL_stackz00_1493;

												BgL_valsz00_1492 = BgL_g1082z00_1490;
												BgL_stackz00_1493 = BgL_stackz00_9;
											BgL_zc3z04anonymousza31371ze3z87_1494:
												if (NULLP(BgL_valsz00_1492))
													{	/* Eval/evmeaning.scm 717 */
														obj_t BgL_arg1373z00_1496;

														BgL_arg1373z00_1496 =
															VECTOR_REF(((obj_t) BgL_codez00_8), 2L);
														{
															obj_t BgL_stackz00_4435;
															obj_t BgL_codez00_4434;

															BgL_codez00_4434 = BgL_arg1373z00_1496;
															BgL_stackz00_4435 = BgL_stackz00_1493;
															BgL_stackz00_9 = BgL_stackz00_4435;
															BgL_codez00_8 = BgL_codez00_4434;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 718 */
														obj_t BgL_arg1375z00_1497;
														obj_t BgL_arg1376z00_1498;

														BgL_arg1375z00_1497 =
															CDR(((obj_t) BgL_valsz00_1492));
														{	/* Eval/evmeaning.scm 719 */
															obj_t BgL_arg1377z00_1499;

															{	/* Eval/evmeaning.scm 719 */
																obj_t BgL_arg1378z00_1500;

																BgL_arg1378z00_1500 =
																	CAR(((obj_t) BgL_valsz00_1492));
																BgL_arg1377z00_1499 =
																	BGl_evmeaningz00zz__evmeaningz00
																	(BgL_arg1378z00_1500, BgL_stackz00_1493,
																	BgL_denvz00_10);
															}
															BgL_arg1376z00_1498 =
																MAKE_YOUNG_PAIR(BgL_arg1377z00_1499,
																BgL_stackz00_1493);
														}
														{
															obj_t BgL_stackz00_4443;
															obj_t BgL_valsz00_4442;

															BgL_valsz00_4442 = BgL_arg1375z00_1497;
															BgL_stackz00_4443 = BgL_arg1376z00_1498;
															BgL_stackz00_1493 = BgL_stackz00_4443;
															BgL_valsz00_1492 = BgL_valsz00_4442;
															goto BgL_zc3z04anonymousza31371ze3z87_1494;
														}
													}
											}
										}
										break;
									case 67L:

										return
											BGl_evmeaningzd2bouncezd267z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 68L:

										return
											BGl_evmeaningzd2bouncezd268z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 70L:

										return
											BGl_evmeaningzd2bouncezd270z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 71L:

										return
											BGl_evmeaningzd2bouncezd271z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 175L:

										return
											BGl_evmeaningzd2bouncezd2175z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 176L:

										return
											BGl_evmeaningzd2bouncezd2176z00zz__evmeaningz00
											(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10);
										break;
									case 131L:

										{	/* Eval/evmeaning.scm 783 */
											obj_t BgL_funz00_1502;

											BgL_funz00_1502 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 784 */
												bool_t BgL_test2384z00_4452;

												if (PROCEDUREP(BgL_funz00_1502))
													{	/* Eval/evmeaning.scm 933 */
														obj_t BgL_arg1791z00_2982;

														BgL_arg1791z00_2982 =
															PROCEDURE_ATTR(BgL_funz00_1502);
														if (STRUCTP(BgL_arg1791z00_2982))
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2384z00_4452 =
																	(STRUCT_KEY(BgL_arg1791z00_2982) ==
																	BGl_symbol2321z00zz__evmeaningz00);
															}
														else
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2384z00_4452 = ((bool_t) 0);
															}
													}
												else
													{	/* Eval/evmeaning.scm 933 */
														BgL_test2384z00_4452 = ((bool_t) 0);
													}
												if (BgL_test2384z00_4452)
													{	/* Eval/evmeaning.scm 785 */
														obj_t BgL_arg1380z00_1504;
														obj_t BgL_arg1382z00_1505;

														{	/* Eval/evmeaning.scm 946 */
															obj_t BgL_arg1793z00_2988;

															BgL_arg1793z00_2988 =
																PROCEDURE_ATTR(BgL_funz00_1502);
															BgL_arg1380z00_1504 =
																STRUCT_REF(
																((obj_t) BgL_arg1793z00_2988), (int) (1L));
														}
														BgL_arg1382z00_1505 =
															BGl_evmeaningzd2tailcallzd20zd2stackzd2zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1502);
														{
															obj_t BgL_stackz00_4466;
															obj_t BgL_codez00_4465;

															BgL_codez00_4465 = BgL_arg1380z00_1504;
															BgL_stackz00_4466 = BgL_arg1382z00_1505;
															BgL_stackz00_9 = BgL_stackz00_4466;
															BgL_codez00_8 = BgL_codez00_4465;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 784 */
														return
															BGl_evmeaningzd2funcallzd20z00zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1502);
													}
											}
										}
										break;
									case 132L:

										{	/* Eval/evmeaning.scm 791 */
											obj_t BgL_funz00_1507;

											BgL_funz00_1507 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 792 */
												bool_t BgL_test2387z00_4470;

												if (PROCEDUREP(BgL_funz00_1507))
													{	/* Eval/evmeaning.scm 933 */
														obj_t BgL_arg1791z00_2993;

														BgL_arg1791z00_2993 =
															PROCEDURE_ATTR(BgL_funz00_1507);
														if (STRUCTP(BgL_arg1791z00_2993))
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2387z00_4470 =
																	(STRUCT_KEY(BgL_arg1791z00_2993) ==
																	BGl_symbol2321z00zz__evmeaningz00);
															}
														else
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2387z00_4470 = ((bool_t) 0);
															}
													}
												else
													{	/* Eval/evmeaning.scm 933 */
														BgL_test2387z00_4470 = ((bool_t) 0);
													}
												if (BgL_test2387z00_4470)
													{	/* Eval/evmeaning.scm 793 */
														obj_t BgL_arg1387z00_1509;
														obj_t BgL_arg1388z00_1510;

														{	/* Eval/evmeaning.scm 946 */
															obj_t BgL_arg1793z00_2999;

															BgL_arg1793z00_2999 =
																PROCEDURE_ATTR(BgL_funz00_1507);
															BgL_arg1387z00_1509 =
																STRUCT_REF(
																((obj_t) BgL_arg1793z00_2999), (int) (1L));
														}
														BgL_arg1388z00_1510 =
															BGl_evmeaningzd2tailcallzd21zd2stackzd2zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1507);
														{
															obj_t BgL_stackz00_4484;
															obj_t BgL_codez00_4483;

															BgL_codez00_4483 = BgL_arg1387z00_1509;
															BgL_stackz00_4484 = BgL_arg1388z00_1510;
															BgL_stackz00_9 = BgL_stackz00_4484;
															BgL_codez00_8 = BgL_codez00_4483;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 792 */
														return
															BGl_evmeaningzd2funcallzd21z00zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1507);
													}
											}
										}
										break;
									case 133L:

										{	/* Eval/evmeaning.scm 799 */
											obj_t BgL_funz00_1512;

											BgL_funz00_1512 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 800 */
												bool_t BgL_test2390z00_4488;

												if (PROCEDUREP(BgL_funz00_1512))
													{	/* Eval/evmeaning.scm 933 */
														obj_t BgL_arg1791z00_3004;

														BgL_arg1791z00_3004 =
															PROCEDURE_ATTR(BgL_funz00_1512);
														if (STRUCTP(BgL_arg1791z00_3004))
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2390z00_4488 =
																	(STRUCT_KEY(BgL_arg1791z00_3004) ==
																	BGl_symbol2321z00zz__evmeaningz00);
															}
														else
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2390z00_4488 = ((bool_t) 0);
															}
													}
												else
													{	/* Eval/evmeaning.scm 933 */
														BgL_test2390z00_4488 = ((bool_t) 0);
													}
												if (BgL_test2390z00_4488)
													{	/* Eval/evmeaning.scm 801 */
														obj_t BgL_arg1391z00_1514;
														obj_t BgL_arg1392z00_1515;

														{	/* Eval/evmeaning.scm 946 */
															obj_t BgL_arg1793z00_3010;

															BgL_arg1793z00_3010 =
																PROCEDURE_ATTR(BgL_funz00_1512);
															BgL_arg1391z00_1514 =
																STRUCT_REF(
																((obj_t) BgL_arg1793z00_3010), (int) (1L));
														}
														BgL_arg1392z00_1515 =
															BGl_evmeaningzd2tailcallzd22zd2stackzd2zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1512);
														{
															obj_t BgL_stackz00_4502;
															obj_t BgL_codez00_4501;

															BgL_codez00_4501 = BgL_arg1391z00_1514;
															BgL_stackz00_4502 = BgL_arg1392z00_1515;
															BgL_stackz00_9 = BgL_stackz00_4502;
															BgL_codez00_8 = BgL_codez00_4501;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 800 */
														return
															BGl_evmeaningzd2funcallzd22z00zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1512);
													}
											}
										}
										break;
									case 134L:

										{	/* Eval/evmeaning.scm 807 */
											obj_t BgL_funz00_1517;

											BgL_funz00_1517 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 808 */
												bool_t BgL_test2393z00_4506;

												if (PROCEDUREP(BgL_funz00_1517))
													{	/* Eval/evmeaning.scm 933 */
														obj_t BgL_arg1791z00_3015;

														BgL_arg1791z00_3015 =
															PROCEDURE_ATTR(BgL_funz00_1517);
														if (STRUCTP(BgL_arg1791z00_3015))
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2393z00_4506 =
																	(STRUCT_KEY(BgL_arg1791z00_3015) ==
																	BGl_symbol2321z00zz__evmeaningz00);
															}
														else
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2393z00_4506 = ((bool_t) 0);
															}
													}
												else
													{	/* Eval/evmeaning.scm 933 */
														BgL_test2393z00_4506 = ((bool_t) 0);
													}
												if (BgL_test2393z00_4506)
													{	/* Eval/evmeaning.scm 809 */
														obj_t BgL_arg1395z00_1519;
														obj_t BgL_arg1396z00_1520;

														{	/* Eval/evmeaning.scm 946 */
															obj_t BgL_arg1793z00_3021;

															BgL_arg1793z00_3021 =
																PROCEDURE_ATTR(BgL_funz00_1517);
															BgL_arg1395z00_1519 =
																STRUCT_REF(
																((obj_t) BgL_arg1793z00_3021), (int) (1L));
														}
														BgL_arg1396z00_1520 =
															BGl_evmeaningzd2tailcallzd23zd2stackzd2zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1517);
														{
															obj_t BgL_stackz00_4520;
															obj_t BgL_codez00_4519;

															BgL_codez00_4519 = BgL_arg1395z00_1519;
															BgL_stackz00_4520 = BgL_arg1396z00_1520;
															BgL_stackz00_9 = BgL_stackz00_4520;
															BgL_codez00_8 = BgL_codez00_4519;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 808 */
														return
															BGl_evmeaningzd2funcallzd23z00zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1517);
													}
											}
										}
										break;
									case 135L:

										{	/* Eval/evmeaning.scm 815 */
											obj_t BgL_funz00_1522;

											BgL_funz00_1522 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 816 */
												bool_t BgL_test2396z00_4524;

												if (PROCEDUREP(BgL_funz00_1522))
													{	/* Eval/evmeaning.scm 933 */
														obj_t BgL_arg1791z00_3026;

														BgL_arg1791z00_3026 =
															PROCEDURE_ATTR(BgL_funz00_1522);
														if (STRUCTP(BgL_arg1791z00_3026))
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2396z00_4524 =
																	(STRUCT_KEY(BgL_arg1791z00_3026) ==
																	BGl_symbol2321z00zz__evmeaningz00);
															}
														else
															{	/* Eval/evmeaning.scm 927 */
																BgL_test2396z00_4524 = ((bool_t) 0);
															}
													}
												else
													{	/* Eval/evmeaning.scm 933 */
														BgL_test2396z00_4524 = ((bool_t) 0);
													}
												if (BgL_test2396z00_4524)
													{	/* Eval/evmeaning.scm 817 */
														obj_t BgL_arg1399z00_1524;
														obj_t BgL_arg1400z00_1525;

														{	/* Eval/evmeaning.scm 946 */
															obj_t BgL_arg1793z00_3032;

															BgL_arg1793z00_3032 =
																PROCEDURE_ATTR(BgL_funz00_1522);
															BgL_arg1399z00_1524 =
																STRUCT_REF(
																((obj_t) BgL_arg1793z00_3032), (int) (1L));
														}
														BgL_arg1400z00_1525 =
															BGl_evmeaningzd2tailcallzd24zd2stackzd2zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1522);
														{
															obj_t BgL_stackz00_4538;
															obj_t BgL_codez00_4537;

															BgL_codez00_4537 = BgL_arg1399z00_1524;
															BgL_stackz00_4538 = BgL_arg1400z00_1525;
															BgL_stackz00_9 = BgL_stackz00_4538;
															BgL_codez00_8 = BgL_codez00_4537;
															goto BGl_evmeaningz00zz__evmeaningz00;
														}
													}
												else
													{	/* Eval/evmeaning.scm 816 */
														return
															BGl_evmeaningzd2funcallzd24z00zz__evmeaningz00
															(BgL_codez00_8, BgL_stackz00_9, BgL_denvz00_10,
															BgL_funz00_1522);
													}
											}
										}
										break;
									case 136L:

										{	/* Eval/evmeaning.scm 823 */
											obj_t BgL_namez00_1527;

											BgL_namez00_1527 = VECTOR_REF(BgL_codez00_8, 2L);
											{	/* Eval/evmeaning.scm 823 */
												obj_t BgL_funz00_1528;

												BgL_funz00_1528 =
													BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
													(BgL_codez00_8, 3L), BgL_stackz00_9, BgL_denvz00_10);
												{	/* Eval/evmeaning.scm 824 */
													obj_t BgL_locz00_1529;

													BgL_locz00_1529 = VECTOR_REF(BgL_codez00_8, 1L);
													{	/* Eval/evmeaning.scm 825 */

														{	/* Eval/evmeaning.scm 826 */
															obj_t BgL_g1083z00_1530;

															BgL_g1083z00_1530 = VECTOR_REF(BgL_codez00_8, 4L);
															{
																obj_t BgL_argsz00_1533;
																obj_t BgL_newz00_1534;
																long BgL_lenz00_1535;

																BgL_argsz00_1533 = BgL_g1083z00_1530;
																BgL_newz00_1534 = BNIL;
																BgL_lenz00_1535 = 0L;
															BgL_zc3z04anonymousza31402ze3z87_1536:
																if (NULLP(BgL_argsz00_1533))
																	{	/* Eval/evmeaning.scm 830 */
																		bool_t BgL_test2400z00_4547;

																		if (PROCEDUREP(BgL_funz00_1528))
																			{	/* Eval/evmeaning.scm 933 */
																				obj_t BgL_arg1791z00_3040;

																				BgL_arg1791z00_3040 =
																					PROCEDURE_ATTR(BgL_funz00_1528);
																				if (STRUCTP(BgL_arg1791z00_3040))
																					{	/* Eval/evmeaning.scm 927 */
																						BgL_test2400z00_4547 =
																							(STRUCT_KEY(BgL_arg1791z00_3040)
																							==
																							BGl_symbol2321z00zz__evmeaningz00);
																					}
																				else
																					{	/* Eval/evmeaning.scm 927 */
																						BgL_test2400z00_4547 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Eval/evmeaning.scm 933 */
																				BgL_test2400z00_4547 = ((bool_t) 0);
																			}
																		if (BgL_test2400z00_4547)
																			{	/* Eval/evmeaning.scm 831 */
																				obj_t BgL_fmlsz00_1539;

																				{	/* Eval/evmeaning.scm 958 */
																					obj_t BgL_arg1795z00_3046;

																					BgL_arg1795z00_3046 =
																						PROCEDURE_ATTR(BgL_funz00_1528);
																					BgL_fmlsz00_1539 =
																						STRUCT_REF(
																						((obj_t) BgL_arg1795z00_3046),
																						(int) (0L));
																				}
																				{	/* Eval/evmeaning.scm 831 */
																					obj_t BgL_stackz00_1540;

																					{	/* Eval/evmeaning.scm 952 */
																						obj_t BgL_arg1794z00_3049;

																						BgL_arg1794z00_3049 =
																							PROCEDURE_ATTR(BgL_funz00_1528);
																						BgL_stackz00_1540 =
																							STRUCT_REF(
																							((obj_t) BgL_arg1794z00_3049),
																							(int) (2L));
																					}
																					{	/* Eval/evmeaning.scm 832 */
																						obj_t BgL_wenz00_1541;

																						BgL_wenz00_1541 =
																							bgl_reverse_bang(BgL_newz00_1534);
																						{	/* Eval/evmeaning.scm 833 */
																							obj_t BgL_e2z00_1542;

																							if (
																								((long) CINT(BgL_fmlsz00_1539)
																									>= 0L))
																								{	/* Eval/evmeaning.scm 834 */
																									BgL_e2z00_1542 =
																										BGl_evmeaningzd2pushzd2fxargsz00zz__evmeaningz00
																										(BgL_namez00_1527,
																										BgL_locz00_1529,
																										BgL_wenz00_1541,
																										BgL_fmlsz00_1539,
																										BgL_stackz00_1540);
																								}
																							else
																								{	/* Eval/evmeaning.scm 834 */
																									BgL_e2z00_1542 =
																										BGl_evmeaningzd2pushzd2vaargsz00zz__evmeaningz00
																										(BgL_namez00_1527,
																										BgL_locz00_1529,
																										BgL_wenz00_1541,
																										BgL_fmlsz00_1539,
																										BgL_stackz00_1540);
																								}
																							{	/* Eval/evmeaning.scm 834 */

																								{	/* Eval/evmeaning.scm 845 */
																									obj_t BgL_arg1405z00_1543;

																									{	/* Eval/evmeaning.scm 946 */
																										obj_t BgL_arg1793z00_3053;

																										BgL_arg1793z00_3053 =
																											PROCEDURE_ATTR
																											(BgL_funz00_1528);
																										BgL_arg1405z00_1543 =
																											STRUCT_REF(((obj_t)
																												BgL_arg1793z00_3053),
																											(int) (1L));
																									}
																									{
																										obj_t BgL_stackz00_4574;
																										obj_t BgL_codez00_4573;

																										BgL_codez00_4573 =
																											BgL_arg1405z00_1543;
																										BgL_stackz00_4574 =
																											BgL_e2z00_1542;
																										BgL_stackz00_9 =
																											BgL_stackz00_4574;
																										BgL_codez00_8 =
																											BgL_codez00_4573;
																										goto
																											BGl_evmeaningz00zz__evmeaningz00;
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		else
																			{	/* Eval/evmeaning.scm 848 */
																				obj_t BgL_arg1407z00_1545;

																				BgL_arg1407z00_1545 =
																					bgl_reverse_bang(BgL_newz00_1534);
																				if (PROCEDUREP(BgL_funz00_1528))
																					{	/* Eval/evmeaning.scm 1188 */
																						if (PROCEDURE_CORRECT_ARITYP
																							(BgL_funz00_1528,
																								(int) (BgL_lenz00_1535)))
																							{	/* Eval/evmeaning.scm 1190 */
																								return
																									eval_apply(BgL_funz00_1528,
																									BgL_arg1407z00_1545);
																							}
																						else
																							{	/* Eval/evmeaning.scm 1191 */
																								obj_t BgL_arg1928z00_3057;
																								int BgL_arg1929z00_3058;

																								BgL_arg1928z00_3057 =
																									VECTOR_REF(
																									((obj_t) BgL_codez00_8), 1L);
																								BgL_arg1929z00_3058 =
																									PROCEDURE_ARITY
																									(BgL_funz00_1528);
																								return
																									BGl_evarityzd2errorzd2zz__everrorz00
																									(BgL_arg1928z00_3057,
																									BgL_namez00_1527,
																									(int) (BgL_lenz00_1535),
																									BgL_arg1929z00_3058);
																					}}
																				else
																					{	/* Eval/evmeaning.scm 1189 */
																						obj_t BgL_arg1930z00_3059;

																						BgL_arg1930z00_3059 =
																							VECTOR_REF(
																							((obj_t) BgL_codez00_8), 1L);
																						return
																							BGl_everrorz00zz__everrorz00
																							(BgL_arg1930z00_3059,
																							BGl_string2323z00zz__evmeaningz00,
																							BGl_string2324z00zz__evmeaningz00,
																							BgL_namez00_1527);
																					}
																			}
																	}
																else
																	{	/* Eval/evmeaning.scm 849 */
																		obj_t BgL_arg1408z00_1546;
																		obj_t BgL_arg1410z00_1547;
																		long BgL_arg1411z00_1548;

																		BgL_arg1408z00_1546 =
																			CDR(((obj_t) BgL_argsz00_1533));
																		{	/* Eval/evmeaning.scm 850 */
																			obj_t BgL_arg1412z00_1549;

																			{	/* Eval/evmeaning.scm 850 */
																				obj_t BgL_arg1413z00_1550;

																				BgL_arg1413z00_1550 =
																					CAR(((obj_t) BgL_argsz00_1533));
																				BgL_arg1412z00_1549 =
																					BGl_evmeaningz00zz__evmeaningz00
																					(BgL_arg1413z00_1550, BgL_stackz00_9,
																					BgL_denvz00_10);
																			}
																			BgL_arg1410z00_1547 =
																				MAKE_YOUNG_PAIR(BgL_arg1412z00_1549,
																				BgL_newz00_1534);
																		}
																		BgL_arg1411z00_1548 =
																			(1L + BgL_lenz00_1535);
																		{
																			long BgL_lenz00_4599;
																			obj_t BgL_newz00_4598;
																			obj_t BgL_argsz00_4597;

																			BgL_argsz00_4597 = BgL_arg1408z00_1546;
																			BgL_newz00_4598 = BgL_arg1410z00_1547;
																			BgL_lenz00_4599 = BgL_arg1411z00_1548;
																			BgL_lenz00_1535 = BgL_lenz00_4599;
																			BgL_newz00_1534 = BgL_newz00_4598;
																			BgL_argsz00_1533 = BgL_argsz00_4597;
																			goto
																				BgL_zc3z04anonymousza31402ze3z87_1536;
																		}
																	}
															}
														}
													}
												}
											}
										}
										break;
									case 145L:
									case 146L:

										{	/* Eval/evmeaning.scm 854 */
											long BgL_arg1415z00_1553;

											{	/* Eval/evmeaning.scm 854 */
												obj_t BgL_arg1416z00_1554;

												BgL_arg1416z00_1554 = VECTOR_REF(BgL_codez00_8, 0L);
												BgL_arg1415z00_1553 =
													((long) CINT(BgL_arg1416z00_1554) - 140L);
											}
											VECTOR_SET(BgL_codez00_8, 0L, BINT(BgL_arg1415z00_1553));
										}
										VECTOR_SET(BgL_codez00_8, 2L,
											BGl_evmodulezd2findzd2globalz00zz__evmodulez00(VECTOR_REF
												(BgL_codez00_8, 3L), VECTOR_REF(BgL_codez00_8, 2L)));
										{

											goto BGl_evmeaningz00zz__evmeaningz00;
										}
										break;
									case 147L:

										{	/* Eval/evmeaning.scm 861 */
											obj_t BgL_a0z00_1558;
											obj_t BgL_a1z00_1559;

											BgL_a0z00_1558 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1559 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1558))
												{	/* Eval/evmeaning.scm 861 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1559))
														{	/* Eval/evmeaning.scm 861 */
															bool_t BgL_test2408z00_4617;

															if (INTEGERP(BgL_a0z00_1558))
																{	/* Eval/evmeaning.scm 861 */
																	BgL_test2408z00_4617 =
																		INTEGERP(BgL_a1z00_1559);
																}
															else
																{	/* Eval/evmeaning.scm 861 */
																	BgL_test2408z00_4617 = ((bool_t) 0);
																}
															if (BgL_test2408z00_4617)
																{	/* Eval/evmeaning.scm 861 */
																	return ADDFX(BgL_a0z00_1558, BgL_a1z00_1559);
																}
															else
																{	/* Eval/evmeaning.scm 861 */
																	return
																		BGl_2zb2zb2zz__r4_numbers_6_5z00
																		(BgL_a0z00_1558, BgL_a1z00_1559);
																}
														}
													else
														{	/* Eval/evmeaning.scm 861 */
															obj_t BgL_arg1423z00_1563;

															BgL_arg1423z00_1563 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1423z00_1563,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1559);
														}
												}
											else
												{	/* Eval/evmeaning.scm 861 */
													obj_t BgL_arg1424z00_1564;

													BgL_arg1424z00_1564 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1424z00_1564,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1558);
												}
										}
										break;
									case 148L:

										{	/* Eval/evmeaning.scm 863 */
											obj_t BgL_a0z00_1567;
											obj_t BgL_a1z00_1568;

											BgL_a0z00_1567 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1568 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1567))
												{	/* Eval/evmeaning.scm 863 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1568))
														{	/* Eval/evmeaning.scm 863 */
															bool_t BgL_test2412z00_4635;

															if (INTEGERP(BgL_a0z00_1567))
																{	/* Eval/evmeaning.scm 863 */
																	BgL_test2412z00_4635 =
																		INTEGERP(BgL_a1z00_1568);
																}
															else
																{	/* Eval/evmeaning.scm 863 */
																	BgL_test2412z00_4635 = ((bool_t) 0);
																}
															if (BgL_test2412z00_4635)
																{	/* Eval/evmeaning.scm 863 */
																	return SUBFX(BgL_a0z00_1567, BgL_a1z00_1568);
																}
															else
																{	/* Eval/evmeaning.scm 863 */
																	return
																		BGl_2zd2zd2zz__r4_numbers_6_5z00
																		(BgL_a0z00_1567, BgL_a1z00_1568);
																}
														}
													else
														{	/* Eval/evmeaning.scm 863 */
															obj_t BgL_arg1430z00_1572;

															BgL_arg1430z00_1572 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1430z00_1572,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1568);
														}
												}
											else
												{	/* Eval/evmeaning.scm 863 */
													obj_t BgL_arg1431z00_1573;

													BgL_arg1431z00_1573 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1431z00_1573,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1567);
												}
										}
										break;
									case 149L:

										{	/* Eval/evmeaning.scm 865 */
											obj_t BgL_a0z00_1576;
											obj_t BgL_a1z00_1577;

											BgL_a0z00_1576 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1577 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1576))
												{	/* Eval/evmeaning.scm 865 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1577))
														{	/* Eval/evmeaning.scm 865 */
															bool_t BgL_test2416z00_4653;

															if (INTEGERP(BgL_a0z00_1576))
																{	/* Eval/evmeaning.scm 865 */
																	BgL_test2416z00_4653 =
																		INTEGERP(BgL_a1z00_1577);
																}
															else
																{	/* Eval/evmeaning.scm 865 */
																	BgL_test2416z00_4653 = ((bool_t) 0);
																}
															if (BgL_test2416z00_4653)
																{	/* Eval/evmeaning.scm 865 */
																	return
																		BINT(
																		((long) CINT(BgL_a0z00_1576) *
																			(long) CINT(BgL_a1z00_1577)));
																}
															else
																{	/* Eval/evmeaning.scm 865 */
																	return
																		BGl_2za2za2zz__r4_numbers_6_5z00
																		(BgL_a0z00_1576, BgL_a1z00_1577);
																}
														}
													else
														{	/* Eval/evmeaning.scm 865 */
															obj_t BgL_arg1439z00_1581;

															BgL_arg1439z00_1581 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1439z00_1581,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1577);
														}
												}
											else
												{	/* Eval/evmeaning.scm 865 */
													obj_t BgL_arg1440z00_1582;

													BgL_arg1440z00_1582 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1440z00_1582,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1576);
												}
										}
										break;
									case 150L:

										{	/* Eval/evmeaning.scm 867 */
											obj_t BgL_a0z00_1585;
											obj_t BgL_a1z00_1586;

											BgL_a0z00_1585 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1586 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1585))
												{	/* Eval/evmeaning.scm 867 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1586))
														{	/* Eval/evmeaning.scm 867 */
															return
																BGl_2zf2zf2zz__r4_numbers_6_5z00(BgL_a0z00_1585,
																BgL_a1z00_1586);
														}
													else
														{	/* Eval/evmeaning.scm 867 */
															obj_t BgL_arg1445z00_1589;

															BgL_arg1445z00_1589 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1445z00_1589,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1586);
														}
												}
											else
												{	/* Eval/evmeaning.scm 867 */
													obj_t BgL_arg1446z00_1590;

													BgL_arg1446z00_1590 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1446z00_1590,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1585);
												}
										}
										break;
									case 151L:

										{	/* Eval/evmeaning.scm 869 */
											obj_t BgL_a0z00_1593;
											obj_t BgL_a1z00_1594;

											BgL_a0z00_1593 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1594 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1593))
												{	/* Eval/evmeaning.scm 869 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1594))
														{	/* Eval/evmeaning.scm 869 */
															bool_t BgL_test2422z00_4687;

															if (INTEGERP(BgL_a0z00_1593))
																{	/* Eval/evmeaning.scm 869 */
																	BgL_test2422z00_4687 =
																		INTEGERP(BgL_a1z00_1594);
																}
															else
																{	/* Eval/evmeaning.scm 869 */
																	BgL_test2422z00_4687 = ((bool_t) 0);
																}
															if (BgL_test2422z00_4687)
																{	/* Eval/evmeaning.scm 869 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a0z00_1593) <
																			(long) CINT(BgL_a1z00_1594)));
																}
															else
																{	/* Eval/evmeaning.scm 869 */
																	return
																		BBOOL(BGl_2zc3zc3zz__r4_numbers_6_5z00
																		(BgL_a0z00_1593, BgL_a1z00_1594));
																}
														}
													else
														{	/* Eval/evmeaning.scm 869 */
															obj_t BgL_arg1452z00_1598;

															BgL_arg1452z00_1598 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1452z00_1598,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1594);
														}
												}
											else
												{	/* Eval/evmeaning.scm 869 */
													obj_t BgL_arg1453z00_1599;

													BgL_arg1453z00_1599 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1453z00_1599,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1593);
												}
										}
										break;
									case 152L:

										{	/* Eval/evmeaning.scm 871 */
											obj_t BgL_a0z00_1602;
											obj_t BgL_a1z00_1603;

											BgL_a0z00_1602 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1603 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1602))
												{	/* Eval/evmeaning.scm 871 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1603))
														{	/* Eval/evmeaning.scm 871 */
															bool_t BgL_test2426z00_4709;

															if (INTEGERP(BgL_a0z00_1602))
																{	/* Eval/evmeaning.scm 871 */
																	BgL_test2426z00_4709 =
																		INTEGERP(BgL_a1z00_1603);
																}
															else
																{	/* Eval/evmeaning.scm 871 */
																	BgL_test2426z00_4709 = ((bool_t) 0);
																}
															if (BgL_test2426z00_4709)
																{	/* Eval/evmeaning.scm 871 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a0z00_1602) >
																			(long) CINT(BgL_a1z00_1603)));
																}
															else
																{	/* Eval/evmeaning.scm 871 */
																	return
																		BBOOL(BGl_2ze3ze3zz__r4_numbers_6_5z00
																		(BgL_a0z00_1602, BgL_a1z00_1603));
																}
														}
													else
														{	/* Eval/evmeaning.scm 871 */
															obj_t BgL_arg1459z00_1607;

															BgL_arg1459z00_1607 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1459z00_1607,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1603);
														}
												}
											else
												{	/* Eval/evmeaning.scm 871 */
													obj_t BgL_arg1460z00_1608;

													BgL_arg1460z00_1608 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1460z00_1608,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1602);
												}
										}
										break;
									case 153L:

										{	/* Eval/evmeaning.scm 873 */
											obj_t BgL_a0z00_1611;
											obj_t BgL_a1z00_1612;

											BgL_a0z00_1611 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1612 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1611))
												{	/* Eval/evmeaning.scm 873 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1612))
														{	/* Eval/evmeaning.scm 873 */
															bool_t BgL_test2430z00_4731;

															if (INTEGERP(BgL_a0z00_1611))
																{	/* Eval/evmeaning.scm 873 */
																	BgL_test2430z00_4731 =
																		INTEGERP(BgL_a1z00_1612);
																}
															else
																{	/* Eval/evmeaning.scm 873 */
																	BgL_test2430z00_4731 = ((bool_t) 0);
																}
															if (BgL_test2430z00_4731)
																{	/* Eval/evmeaning.scm 873 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a0z00_1611) <=
																			(long) CINT(BgL_a1z00_1612)));
																}
															else
																{	/* Eval/evmeaning.scm 873 */
																	return
																		BBOOL(BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																		(BgL_a0z00_1611, BgL_a1z00_1612));
																}
														}
													else
														{	/* Eval/evmeaning.scm 873 */
															obj_t BgL_arg1466z00_1616;

															BgL_arg1466z00_1616 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1466z00_1616,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1612);
														}
												}
											else
												{	/* Eval/evmeaning.scm 873 */
													obj_t BgL_arg1467z00_1617;

													BgL_arg1467z00_1617 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1467z00_1617,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1611);
												}
										}
										break;
									case 154L:

										{	/* Eval/evmeaning.scm 875 */
											obj_t BgL_a0z00_1620;
											obj_t BgL_a1z00_1621;

											BgL_a0z00_1620 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1621 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1620))
												{	/* Eval/evmeaning.scm 875 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1621))
														{	/* Eval/evmeaning.scm 875 */
															bool_t BgL_test2434z00_4753;

															if (INTEGERP(BgL_a0z00_1620))
																{	/* Eval/evmeaning.scm 875 */
																	BgL_test2434z00_4753 =
																		INTEGERP(BgL_a1z00_1621);
																}
															else
																{	/* Eval/evmeaning.scm 875 */
																	BgL_test2434z00_4753 = ((bool_t) 0);
																}
															if (BgL_test2434z00_4753)
																{	/* Eval/evmeaning.scm 875 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a0z00_1620) >=
																			(long) CINT(BgL_a1z00_1621)));
																}
															else
																{	/* Eval/evmeaning.scm 875 */
																	return
																		BBOOL(BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																		(BgL_a0z00_1620, BgL_a1z00_1621));
																}
														}
													else
														{	/* Eval/evmeaning.scm 875 */
															obj_t BgL_arg1473z00_1625;

															BgL_arg1473z00_1625 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1473z00_1625,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1621);
														}
												}
											else
												{	/* Eval/evmeaning.scm 875 */
													obj_t BgL_arg1474z00_1626;

													BgL_arg1474z00_1626 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1474z00_1626,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1620);
												}
										}
										break;
									case 155L:

										{	/* Eval/evmeaning.scm 877 */
											obj_t BgL_a0z00_1629;
											obj_t BgL_a1z00_1630;

											BgL_a0z00_1629 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1630 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_a0z00_1629))
												{	/* Eval/evmeaning.scm 877 */
													if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
														(BgL_a1z00_1630))
														{	/* Eval/evmeaning.scm 877 */
															bool_t BgL_test2438z00_4775;

															if (INTEGERP(BgL_a0z00_1629))
																{	/* Eval/evmeaning.scm 877 */
																	BgL_test2438z00_4775 =
																		INTEGERP(BgL_a1z00_1630);
																}
															else
																{	/* Eval/evmeaning.scm 877 */
																	BgL_test2438z00_4775 = ((bool_t) 0);
																}
															if (BgL_test2438z00_4775)
																{	/* Eval/evmeaning.scm 877 */
																	return
																		BBOOL(
																		((long) CINT(BgL_a0z00_1629) ==
																			(long) CINT(BgL_a1z00_1630)));
																}
															else
																{	/* Eval/evmeaning.scm 877 */
																	return
																		BBOOL(BGl_2zd3zd3zz__r4_numbers_6_5z00
																		(BgL_a0z00_1629, BgL_a1z00_1630));
																}
														}
													else
														{	/* Eval/evmeaning.scm 877 */
															obj_t BgL_arg1481z00_1634;

															BgL_arg1481z00_1634 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1481z00_1634,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2325z00zz__evmeaningz00,
																BgL_a1z00_1630);
														}
												}
											else
												{	/* Eval/evmeaning.scm 877 */
													obj_t BgL_arg1482z00_1635;

													BgL_arg1482z00_1635 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1482z00_1635,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2325z00zz__evmeaningz00, BgL_a0z00_1629);
												}
										}
										break;
									case 166L:

										{	/* Eval/evmeaning.scm 879 */
											obj_t BgL_a0z00_1638;
											obj_t BgL_a1z00_1639;

											BgL_a0z00_1638 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1639 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1638))
												{	/* Eval/evmeaning.scm 879 */
													if (INTEGERP(BgL_a1z00_1639))
														{	/* Eval/evmeaning.scm 879 */
															return ADDFX(BgL_a0z00_1638, BgL_a1z00_1639);
														}
													else
														{	/* Eval/evmeaning.scm 879 */
															obj_t BgL_arg1487z00_1642;

															BgL_arg1487z00_1642 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1487z00_1642,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1639);
														}
												}
											else
												{	/* Eval/evmeaning.scm 879 */
													obj_t BgL_arg1488z00_1643;

													BgL_arg1488z00_1643 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1488z00_1643,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1638);
												}
										}
										break;
									case 167L:

										{	/* Eval/evmeaning.scm 881 */
											obj_t BgL_a0z00_1646;
											obj_t BgL_a1z00_1647;

											BgL_a0z00_1646 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1647 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1646))
												{	/* Eval/evmeaning.scm 881 */
													if (INTEGERP(BgL_a1z00_1647))
														{	/* Eval/evmeaning.scm 881 */
															return SUBFX(BgL_a0z00_1646, BgL_a1z00_1647);
														}
													else
														{	/* Eval/evmeaning.scm 881 */
															obj_t BgL_arg1494z00_1650;

															BgL_arg1494z00_1650 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1494z00_1650,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1647);
														}
												}
											else
												{	/* Eval/evmeaning.scm 881 */
													obj_t BgL_arg1495z00_1651;

													BgL_arg1495z00_1651 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1495z00_1651,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1646);
												}
										}
										break;
									case 168L:

										{	/* Eval/evmeaning.scm 883 */
											obj_t BgL_a0z00_1654;
											obj_t BgL_a1z00_1655;

											BgL_a0z00_1654 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1655 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1654))
												{	/* Eval/evmeaning.scm 883 */
													if (INTEGERP(BgL_a1z00_1655))
														{	/* Eval/evmeaning.scm 883 */
															return
																BINT(
																((long) CINT(BgL_a0z00_1654) *
																	(long) CINT(BgL_a1z00_1655)));
														}
													else
														{	/* Eval/evmeaning.scm 883 */
															obj_t BgL_arg1501z00_1658;

															BgL_arg1501z00_1658 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1501z00_1658,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1655);
														}
												}
											else
												{	/* Eval/evmeaning.scm 883 */
													obj_t BgL_arg1502z00_1659;

													BgL_arg1502z00_1659 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1502z00_1659,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1654);
												}
										}
										break;
									case 169L:

										{	/* Eval/evmeaning.scm 885 */
											obj_t BgL_a0z00_1662;
											obj_t BgL_a1z00_1663;

											BgL_a0z00_1662 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1663 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1662))
												{	/* Eval/evmeaning.scm 885 */
													if (INTEGERP(BgL_a1z00_1663))
														{	/* Eval/evmeaning.scm 885 */
															return
																BINT(
																((long) CINT(BgL_a0z00_1662) /
																	(long) CINT(BgL_a1z00_1663)));
														}
													else
														{	/* Eval/evmeaning.scm 885 */
															obj_t BgL_arg1507z00_1666;

															BgL_arg1507z00_1666 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1507z00_1666,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1663);
														}
												}
											else
												{	/* Eval/evmeaning.scm 885 */
													obj_t BgL_arg1508z00_1667;

													BgL_arg1508z00_1667 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1508z00_1667,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1662);
												}
										}
										break;
									case 170L:

										{	/* Eval/evmeaning.scm 887 */
											obj_t BgL_a0z00_1670;
											obj_t BgL_a1z00_1671;

											BgL_a0z00_1670 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1671 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1670))
												{	/* Eval/evmeaning.scm 887 */
													if (INTEGERP(BgL_a1z00_1671))
														{	/* Eval/evmeaning.scm 887 */
															return
																BBOOL(
																((long) CINT(BgL_a0z00_1670) <
																	(long) CINT(BgL_a1z00_1671)));
														}
													else
														{	/* Eval/evmeaning.scm 887 */
															obj_t BgL_arg1513z00_1674;

															BgL_arg1513z00_1674 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1513z00_1674,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1671);
														}
												}
											else
												{	/* Eval/evmeaning.scm 887 */
													obj_t BgL_arg1514z00_1675;

													BgL_arg1514z00_1675 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1514z00_1675,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1670);
												}
										}
										break;
									case 171L:

										{	/* Eval/evmeaning.scm 889 */
											obj_t BgL_a0z00_1678;
											obj_t BgL_a1z00_1679;

											BgL_a0z00_1678 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1679 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1678))
												{	/* Eval/evmeaning.scm 889 */
													if (INTEGERP(BgL_a1z00_1679))
														{	/* Eval/evmeaning.scm 889 */
															return
																BBOOL(
																((long) CINT(BgL_a0z00_1678) >
																	(long) CINT(BgL_a1z00_1679)));
														}
													else
														{	/* Eval/evmeaning.scm 889 */
															obj_t BgL_arg1521z00_1682;

															BgL_arg1521z00_1682 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1521z00_1682,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1679);
														}
												}
											else
												{	/* Eval/evmeaning.scm 889 */
													obj_t BgL_arg1522z00_1683;

													BgL_arg1522z00_1683 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1522z00_1683,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1678);
												}
										}
										break;
									case 172L:

										{	/* Eval/evmeaning.scm 891 */
											obj_t BgL_a0z00_1686;
											obj_t BgL_a1z00_1687;

											BgL_a0z00_1686 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1687 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1686))
												{	/* Eval/evmeaning.scm 891 */
													if (INTEGERP(BgL_a1z00_1687))
														{	/* Eval/evmeaning.scm 891 */
															return
																BBOOL(
																((long) CINT(BgL_a0z00_1686) <=
																	(long) CINT(BgL_a1z00_1687)));
														}
													else
														{	/* Eval/evmeaning.scm 891 */
															obj_t BgL_arg1527z00_1690;

															BgL_arg1527z00_1690 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1527z00_1690,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1687);
														}
												}
											else
												{	/* Eval/evmeaning.scm 891 */
													obj_t BgL_arg1528z00_1691;

													BgL_arg1528z00_1691 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1528z00_1691,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1686);
												}
										}
										break;
									case 173L:

										{	/* Eval/evmeaning.scm 893 */
											obj_t BgL_a0z00_1694;
											obj_t BgL_a1z00_1695;

											BgL_a0z00_1694 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1695 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1694))
												{	/* Eval/evmeaning.scm 893 */
													if (INTEGERP(BgL_a1z00_1695))
														{	/* Eval/evmeaning.scm 893 */
															return
																BBOOL(
																((long) CINT(BgL_a0z00_1694) >=
																	(long) CINT(BgL_a1z00_1695)));
														}
													else
														{	/* Eval/evmeaning.scm 893 */
															obj_t BgL_arg1535z00_1698;

															BgL_arg1535z00_1698 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1535z00_1698,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1695);
														}
												}
											else
												{	/* Eval/evmeaning.scm 893 */
													obj_t BgL_arg1536z00_1699;

													BgL_arg1536z00_1699 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1536z00_1699,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1694);
												}
										}
										break;
									case 174L:

										{	/* Eval/evmeaning.scm 895 */
											obj_t BgL_a0z00_1702;
											obj_t BgL_a1z00_1703;

											BgL_a0z00_1702 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1703 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											if (INTEGERP(BgL_a0z00_1702))
												{	/* Eval/evmeaning.scm 895 */
													if (INTEGERP(BgL_a1z00_1703))
														{	/* Eval/evmeaning.scm 895 */
															return
																BBOOL(
																((long) CINT(BgL_a0z00_1702) ==
																	(long) CINT(BgL_a1z00_1703)));
														}
													else
														{	/* Eval/evmeaning.scm 895 */
															obj_t BgL_arg1543z00_1706;

															BgL_arg1543z00_1706 =
																VECTOR_REF(BgL_codez00_8, 1L);
															return
																BGl_evtypezd2errorzd2zz__everrorz00
																(BgL_arg1543z00_1706,
																BGl_string2318z00zz__evmeaningz00,
																BGl_symbol2327z00zz__evmeaningz00,
																BgL_a1z00_1703);
														}
												}
											else
												{	/* Eval/evmeaning.scm 895 */
													obj_t BgL_arg1544z00_1707;

													BgL_arg1544z00_1707 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1544z00_1707,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2327z00zz__evmeaningz00, BgL_a0z00_1702);
												}
										}
										break;
									case 156L:

										{	/* Eval/evmeaning.scm 897 */
											obj_t BgL_a0z00_1710;
											obj_t BgL_a1z00_1711;

											BgL_a0z00_1710 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											BgL_a1z00_1711 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 5L), BgL_stackz00_9, BgL_denvz00_10);
											return BBOOL((BgL_a0z00_1710 == BgL_a1z00_1711));
										}
										break;
									case 157L:

										return
											MAKE_YOUNG_PAIR(BGl_evmeaningz00zz__evmeaningz00
											(VECTOR_REF(BgL_codez00_8, 4L), BgL_stackz00_9,
												BgL_denvz00_10),
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_8,
													5L), BgL_stackz00_9, BgL_denvz00_10));
										break;
									case 158L:

										{	/* Eval/evmeaning.scm 901 */
											obj_t BgL_a0z00_1718;

											BgL_a0z00_1718 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											if (PAIRP(BgL_a0z00_1718))
												{	/* Eval/evmeaning.scm 901 */
													return CAR(BgL_a0z00_1718);
												}
											else
												{	/* Eval/evmeaning.scm 901 */
													obj_t BgL_arg1556z00_1720;

													BgL_arg1556z00_1720 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1556z00_1720,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2329z00zz__evmeaningz00, BgL_a0z00_1718);
												}
										}
										break;
									case 159L:

										{	/* Eval/evmeaning.scm 903 */
											obj_t BgL_a0z00_1722;

											BgL_a0z00_1722 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											if (PAIRP(BgL_a0z00_1722))
												{	/* Eval/evmeaning.scm 903 */
													return CDR(BgL_a0z00_1722);
												}
											else
												{	/* Eval/evmeaning.scm 903 */
													obj_t BgL_arg1559z00_1724;

													BgL_arg1559z00_1724 = VECTOR_REF(BgL_codez00_8, 1L);
													return
														BGl_evtypezd2errorzd2zz__everrorz00
														(BgL_arg1559z00_1724,
														BGl_string2318z00zz__evmeaningz00,
														BGl_symbol2329z00zz__evmeaningz00, BgL_a0z00_1722);
												}
										}
										break;
									case 160L:

										{	/* Eval/evmeaning.scm 905 */
											obj_t BgL_a0z00_1726;

											BgL_a0z00_1726 =
												BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_8, 4L), BgL_stackz00_9, BgL_denvz00_10);
											{	/* Eval/evmeaning.scm 907 */
												bool_t BgL_test2460z00_4954;

												if (PAIRP(BgL_a0z00_1726))
													{	/* Eval/evmeaning.scm 907 */
														obj_t BgL_tmpz00_4957;

														BgL_tmpz00_4957 = CDR(BgL_a0z00_1726);
														BgL_test2460z00_4954 = PAIRP(BgL_tmpz00_4957);
													}
												else
													{	/* Eval/evmeaning.scm 907 */
														BgL_test2460z00_4954 = ((bool_t) 0);
													}
												if (BgL_test2460z00_4954)
													{	/* Eval/evmeaning.scm 907 */
														return CAR(CDR(BgL_a0z00_1726));
													}
												else
													{	/* Eval/evmeaning.scm 907 */
														if (PAIRP(BgL_a0z00_1726))
															{	/* Eval/evmeaning.scm 909 */
																return
																	BGl_evtypezd2errorzd2zz__everrorz00(VECTOR_REF
																	(BgL_codez00_8, 1L), VECTOR_REF(BgL_codez00_8,
																		2L), BGl_string2330z00zz__evmeaningz00,
																	CDR(BgL_a0z00_1726));
															}
														else
															{	/* Eval/evmeaning.scm 909 */
																return
																	BGl_evtypezd2errorzd2zz__everrorz00(VECTOR_REF
																	(BgL_codez00_8, 1L), VECTOR_REF(BgL_codez00_8,
																		2L), BGl_string2330z00zz__evmeaningz00,
																	BgL_a0z00_1726);
															}
													}
											}
										}
										break;
									default:
									BgL_case_else1078z00_1406:
										{	/* Eval/evmeaning.scm 919 */
											obj_t BgL_arg1580z00_1739;

											BgL_arg1580z00_1739 =
												VECTOR_REF(((obj_t) BgL_codez00_8), 1L);
											return
												BGl_everrorz00zz__everrorz00(BgL_arg1580z00_1739,
												BGl_string2318z00zz__evmeaningz00,
												BGl_string2320z00zz__evmeaningz00, BgL_codez00_8);
										}
									}
							}
						else
							{	/* Eval/evmeaning.scm 186 */
								goto BgL_case_else1078z00_1406;
							}
					}
				}
			else
				{	/* Eval/evmeaning.scm 184 */
					return BgL_codez00_8;
				}
		}

	}



/* &evmeaning */
	obj_t BGl_z62evmeaningz62zz__evmeaningz00(obj_t BgL_envz00_3924,
		obj_t BgL_codez00_3925, obj_t BgL_stackz00_3926, obj_t BgL_denvz00_3927)
	{
		{	/* Eval/evmeaning.scm 183 */
			{	/* Eval/evmeaning.scm 184 */
				obj_t BgL_auxz00_4983;
				obj_t BgL_auxz00_4976;

				if (BGL_DYNAMIC_ENVP(BgL_denvz00_3927))
					{	/* Eval/evmeaning.scm 184 */
						BgL_auxz00_4983 = BgL_denvz00_3927;
					}
				else
					{
						obj_t BgL_auxz00_4986;

						BgL_auxz00_4986 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2331z00zz__evmeaningz00,
							BINT(6923L), BGl_string2332z00zz__evmeaningz00,
							BGl_string2334z00zz__evmeaningz00, BgL_denvz00_3927);
						FAILURE(BgL_auxz00_4986, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_stackz00_3926))
					{	/* Eval/evmeaning.scm 184 */
						BgL_auxz00_4976 = BgL_stackz00_3926;
					}
				else
					{
						obj_t BgL_auxz00_4979;

						BgL_auxz00_4979 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2331z00zz__evmeaningz00,
							BINT(6923L), BGl_string2332z00zz__evmeaningz00,
							BGl_string2333z00zz__evmeaningz00, BgL_stackz00_3926);
						FAILURE(BgL_auxz00_4979, BFALSE, BFALSE);
					}
				return
					BGl_evmeaningz00zz__evmeaningz00(BgL_codez00_3925, BgL_auxz00_4976,
					BgL_auxz00_4983);
			}
		}

	}



/* evmeaning-bounce-176 */
	obj_t BGl_evmeaningzd2bouncezd2176z00zz__evmeaningz00(obj_t BgL_codez00_11,
		obj_t BgL_stackz00_12, obj_t BgL_denvz00_13)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 772 */
				obj_t BgL_mutexz00_1740;

				BgL_mutexz00_1740 = VECTOR_REF(BgL_codez00_11, 2L);
				{	/* Eval/evmeaning.scm 772 */
					obj_t BgL_prelockz00_1741;

					BgL_prelockz00_1741 = VECTOR_REF(BgL_codez00_11, 3L);
					{	/* Eval/evmeaning.scm 773 */
						obj_t BgL_bodyz00_1742;

						BgL_bodyz00_1742 = VECTOR_REF(BgL_codez00_11, 4L);
						{	/* Eval/evmeaning.scm 774 */
							obj_t BgL_mz00_1743;

							BgL_mz00_1743 =
								BGl_evmeaningz00zz__evmeaningz00(BgL_mutexz00_1740,
								BgL_stackz00_12, BgL_denvz00_13);
							{	/* Eval/evmeaning.scm 775 */

								if (BGL_MUTEXP(BgL_mz00_1743))
									{	/* Eval/evmeaning.scm 777 */
										obj_t BgL_top2467z00_4998;

										BgL_top2467z00_4998 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Eval/evmeaning.scm 777 */
											obj_t BgL_tmpz00_5000;

											BgL_tmpz00_5000 =
												BGl_evmeaningz00zz__evmeaningz00(BgL_prelockz00_1741,
												BgL_stackz00_12, BgL_denvz00_13);
											BGL_MUTEX_LOCK_PRELOCK(BgL_mz00_1743, BgL_tmpz00_5000);
										}
										BGL_EXITD_PUSH_PROTECT(BgL_top2467z00_4998, BgL_mz00_1743);
										BUNSPEC;
										{	/* Eval/evmeaning.scm 777 */
											obj_t BgL_tmp2466z00_4997;

											BgL_tmp2466z00_4997 =
												BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_1742,
												BgL_stackz00_12, BgL_denvz00_13);
											BGL_EXITD_POP_PROTECT(BgL_top2467z00_4998);
											BUNSPEC;
											BGL_MUTEX_UNLOCK(BgL_mz00_1743);
											return BgL_tmp2466z00_4997;
										}
									}
								else
									{	/* Eval/evmeaning.scm 776 */
										return
											BGl_evtypezd2errorzd2zz__everrorz00(VECTOR_REF
											(BgL_codez00_11, 1L), BGl_string2335z00zz__evmeaningz00,
											BGl_string2336z00zz__evmeaningz00, BgL_mz00_1743);
									}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-175 */
	obj_t BGl_evmeaningzd2bouncezd2175z00zz__evmeaningz00(obj_t BgL_codez00_14,
		obj_t BgL_stackz00_15, obj_t BgL_denvz00_16)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 763 */
				obj_t BgL_mutexz00_1746;

				BgL_mutexz00_1746 = VECTOR_REF(BgL_codez00_14, 2L);
				{	/* Eval/evmeaning.scm 763 */
					obj_t BgL_bodyz00_1747;

					BgL_bodyz00_1747 = VECTOR_REF(BgL_codez00_14, 3L);
					{	/* Eval/evmeaning.scm 764 */
						obj_t BgL_mz00_1748;

						BgL_mz00_1748 =
							BGl_evmeaningz00zz__evmeaningz00(BgL_mutexz00_1746,
							BgL_stackz00_15, BgL_denvz00_16);
						{	/* Eval/evmeaning.scm 765 */

							if (BGL_MUTEXP(BgL_mz00_1748))
								{	/* Eval/evmeaning.scm 767 */
									obj_t BgL_top2470z00_5015;

									BgL_top2470z00_5015 = BGL_EXITD_TOP_AS_OBJ();
									BGL_MUTEX_LOCK(BgL_mz00_1748);
									BGL_EXITD_PUSH_PROTECT(BgL_top2470z00_5015, BgL_mz00_1748);
									BUNSPEC;
									{	/* Eval/evmeaning.scm 767 */
										obj_t BgL_tmp2469z00_5014;

										BgL_tmp2469z00_5014 =
											BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_1747,
											BgL_stackz00_15, BgL_denvz00_16);
										BGL_EXITD_POP_PROTECT(BgL_top2470z00_5015);
										BUNSPEC;
										BGL_MUTEX_UNLOCK(BgL_mz00_1748);
										return BgL_tmp2469z00_5014;
									}
								}
							else
								{	/* Eval/evmeaning.scm 766 */
									return
										BGl_evtypezd2errorzd2zz__everrorz00(VECTOR_REF
										(BgL_codez00_14, 1L), BGl_string2335z00zz__evmeaningz00,
										BGl_string2336z00zz__evmeaningz00, BgL_mz00_1748);
								}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-71 */
	obj_t BGl_evmeaningzd2bouncezd271z00zz__evmeaningz00(obj_t BgL_codez00_17,
		obj_t BgL_stackz00_18, obj_t BgL_denvz00_19)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 750 */
				obj_t BgL_handlerz00_1751;

				BgL_handlerz00_1751 = VECTOR_REF(BgL_codez00_17, 2L);
				{	/* Eval/evmeaning.scm 750 */
					obj_t BgL_bodyz00_1752;

					BgL_bodyz00_1752 = VECTOR_REF(BgL_codez00_17, 3L);
					{	/* Eval/evmeaning.scm 751 */
						obj_t BgL_ehandlerz00_1753;

						BgL_ehandlerz00_1753 =
							BGl_evmeaningz00zz__evmeaningz00(BgL_handlerz00_1751,
							BgL_stackz00_18, BgL_denvz00_19);
						{	/* Eval/evmeaning.scm 752 */
							obj_t BgL_locz00_1754;

							BgL_locz00_1754 = VECTOR_REF(BgL_codez00_17, 1L);
							{	/* Eval/evmeaning.scm 753 */

								if (PROCEDUREP(BgL_ehandlerz00_1753))
									{	/* Eval/evmeaning.scm 755 */
										if (PROCEDURE_CORRECT_ARITYP(BgL_ehandlerz00_1753,
												(int) (1L)))
											{	/* Eval/evmeaning.scm 758 */
												obj_t BgL_handler1090z00_1756;
												obj_t BgL_env1093z00_1757;

												BgL_handler1090z00_1756 = BgL_ehandlerz00_1753;
												BgL_env1093z00_1757 = BGL_CURRENT_DYNAMIC_ENV();
												{	/* Eval/evmeaning.scm 758 */
													obj_t BgL_cell1088z00_1758;

													BgL_cell1088z00_1758 = MAKE_STACK_CELL(BUNSPEC);
													{	/* Eval/evmeaning.scm 758 */
														obj_t BgL_val1092z00_1759;

														BgL_val1092z00_1759 =
															BGl_zc3z04exitza31587ze3ze70z60zz__evmeaningz00
															(BgL_denvz00_19, BgL_stackz00_18,
															BgL_bodyz00_1752, BgL_cell1088z00_1758,
															BgL_env1093z00_1757);
														if ((BgL_val1092z00_1759 == BgL_cell1088z00_1758))
															{	/* Eval/evmeaning.scm 758 */
																{	/* Eval/evmeaning.scm 758 */
																	int BgL_tmpz00_5038;

																	BgL_tmpz00_5038 = (int) (0L);
																	BGL_SIGSETMASK(BgL_tmpz00_5038);
																}
																{	/* Eval/evmeaning.scm 758 */
																	obj_t BgL_arg1586z00_1760;

																	BgL_arg1586z00_1760 =
																		CELL_REF(((obj_t) BgL_val1092z00_1759));
																	return
																		BGL_PROCEDURE_CALL1(BgL_handler1090z00_1756,
																		BgL_arg1586z00_1760);
																}
															}
														else
															{	/* Eval/evmeaning.scm 758 */
																return BgL_val1092z00_1759;
															}
													}
												}
											}
										else
											{	/* Eval/evmeaning.scm 757 */
												return
													BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_1754,
													BGl_string2337z00zz__evmeaningz00, (int) (1L),
													PROCEDURE_ARITY(BgL_ehandlerz00_1753));
									}}
								else
									{	/* Eval/evmeaning.scm 755 */
										return
											BGl_evtypezd2errorzd2zz__everrorz00(BgL_locz00_1754,
											BGl_string2318z00zz__evmeaningz00,
											BGl_string2338z00zz__evmeaningz00, BgL_ehandlerz00_1753);
									}
							}
						}
					}
				}
			}
		}

	}



/* <@exit:1587>~0 */
	obj_t BGl_zc3z04exitza31587ze3ze70z60zz__evmeaningz00(obj_t BgL_denvz00_4127,
		obj_t BgL_stackz00_4126, obj_t BgL_bodyz00_4125, obj_t BgL_cell1088z00_4124,
		obj_t BgL_env1093z00_4123)
	{
		{	/* Eval/evmeaning.scm 758 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1097z00_1762;

			if (SET_EXIT(BgL_an_exit1097z00_1762))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1097z00_1762 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1093z00_4123, BgL_an_exit1097z00_1762, 1L);
					{	/* Eval/evmeaning.scm 758 */
						obj_t BgL_escape1089z00_1763;

						BgL_escape1089z00_1763 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1093z00_4123);
						{	/* Eval/evmeaning.scm 758 */
							obj_t BgL_res1100z00_1764;

							{	/* Eval/evmeaning.scm 758 */
								obj_t BgL_ohs1085z00_1765;

								BgL_ohs1085z00_1765 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1093z00_4123);
								{	/* Eval/evmeaning.scm 758 */
									obj_t BgL_hds1086z00_1766;

									BgL_hds1086z00_1766 =
										MAKE_STACK_PAIR(BgL_escape1089z00_1763,
										BgL_cell1088z00_4124);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1093z00_4123,
										BgL_hds1086z00_1766);
									BUNSPEC;
									{	/* Eval/evmeaning.scm 758 */
										obj_t BgL_exitd1094z00_1767;

										BgL_exitd1094z00_1767 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1093z00_4123);
										{	/* Eval/evmeaning.scm 758 */
											obj_t BgL_tmp1096z00_1768;

											{	/* Eval/evmeaning.scm 758 */
												obj_t BgL_arg1589z00_1770;

												BgL_arg1589z00_1770 =
													BGL_EXITD_PROTECT(BgL_exitd1094z00_1767);
												BgL_tmp1096z00_1768 =
													MAKE_YOUNG_PAIR(BgL_ohs1085z00_1765,
													BgL_arg1589z00_1770);
											}
											{	/* Eval/evmeaning.scm 758 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1094z00_1767,
													BgL_tmp1096z00_1768);
												BUNSPEC;
												{	/* Eval/evmeaning.scm 758 */
													obj_t BgL_tmp1095z00_1769;

													BgL_tmp1095z00_1769 =
														BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4125,
														BgL_stackz00_4126, BgL_denvz00_4127);
													{	/* Eval/evmeaning.scm 758 */
														bool_t BgL_test2474z00_5063;

														{	/* Eval/evmeaning.scm 758 */
															obj_t BgL_arg2281z00_3209;

															BgL_arg2281z00_3209 =
																BGL_EXITD_PROTECT(BgL_exitd1094z00_1767);
															BgL_test2474z00_5063 = PAIRP(BgL_arg2281z00_3209);
														}
														if (BgL_test2474z00_5063)
															{	/* Eval/evmeaning.scm 758 */
																obj_t BgL_arg2279z00_3210;

																{	/* Eval/evmeaning.scm 758 */
																	obj_t BgL_arg2280z00_3211;

																	BgL_arg2280z00_3211 =
																		BGL_EXITD_PROTECT(BgL_exitd1094z00_1767);
																	BgL_arg2279z00_3210 =
																		CDR(((obj_t) BgL_arg2280z00_3211));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1094z00_1767,
																	BgL_arg2279z00_3210);
																BUNSPEC;
															}
														else
															{	/* Eval/evmeaning.scm 758 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1093z00_4123,
														BgL_ohs1085z00_1765);
													BUNSPEC;
													BgL_res1100z00_1764 = BgL_tmp1095z00_1769;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1093z00_4123);
							return BgL_res1100z00_1764;
						}
					}
				}
		}

	}



/* evmeaning-bounce-70 */
	obj_t BGl_evmeaningzd2bouncezd270z00zz__evmeaningz00(obj_t BgL_codez00_20,
		obj_t BgL_stackz00_21, obj_t BgL_denvz00_22)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 739 */
				obj_t BgL_valsz00_1772;

				BgL_valsz00_1772 = VECTOR_REF(BgL_codez00_20, 3L);
				{	/* Eval/evmeaning.scm 739 */
					obj_t BgL_stack2z00_1773;

					{	/* Eval/evmeaning.scm 740 */
						obj_t BgL_arg1603z00_1785;

						{	/* Eval/evmeaning.scm 740 */
							long BgL_arg1605z00_1786;

							BgL_arg1605z00_1786 = bgl_list_length(BgL_valsz00_1772);
							BgL_arg1603z00_1785 =
								BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
								(int) (BgL_arg1605z00_1786), BNIL);
						}
						BgL_stack2z00_1773 =
							BGl_appendzd221011zd2zz__evmeaningz00(BgL_arg1603z00_1785,
							BgL_stackz00_21);
					}
					{	/* Eval/evmeaning.scm 740 */

						{
							obj_t BgL_valsz00_1775;
							obj_t BgL_stack3z00_1776;

							BgL_valsz00_1775 = BgL_valsz00_1772;
							BgL_stack3z00_1776 = BgL_stack2z00_1773;
						BgL_zc3z04anonymousza31592ze3z87_1777:
							if (NULLP(BgL_valsz00_1775))
								{	/* Eval/evmeaning.scm 743 */
									return
										BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_20,
											2L), BgL_stack2z00_1773, BgL_denvz00_22);
								}
							else
								{	/* Eval/evmeaning.scm 743 */
									{	/* Eval/evmeaning.scm 746 */
										obj_t BgL_arg1595z00_1780;

										{	/* Eval/evmeaning.scm 746 */
											obj_t BgL_arg1598z00_1781;

											BgL_arg1598z00_1781 = CAR(((obj_t) BgL_valsz00_1775));
											BgL_arg1595z00_1780 =
												BGl_evmeaningz00zz__evmeaningz00(BgL_arg1598z00_1781,
												BgL_stack2z00_1773, BgL_denvz00_22);
										}
										{	/* Eval/evmeaning.scm 746 */
											obj_t BgL_tmpz00_5084;

											BgL_tmpz00_5084 = ((obj_t) BgL_stack3z00_1776);
											SET_CAR(BgL_tmpz00_5084, BgL_arg1595z00_1780);
										}
									}
									{	/* Eval/evmeaning.scm 747 */
										obj_t BgL_arg1601z00_1782;
										obj_t BgL_arg1602z00_1783;

										BgL_arg1601z00_1782 = CDR(((obj_t) BgL_valsz00_1775));
										BgL_arg1602z00_1783 = CDR(((obj_t) BgL_stack3z00_1776));
										{
											obj_t BgL_stack3z00_5092;
											obj_t BgL_valsz00_5091;

											BgL_valsz00_5091 = BgL_arg1601z00_1782;
											BgL_stack3z00_5092 = BgL_arg1602z00_1783;
											BgL_stack3z00_1776 = BgL_stack3z00_5092;
											BgL_valsz00_1775 = BgL_valsz00_5091;
											goto BgL_zc3z04anonymousza31592ze3z87_1777;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-68 */
	obj_t BGl_evmeaningzd2bouncezd268z00zz__evmeaningz00(obj_t BgL_codez00_23,
		obj_t BgL_stackz00_24, obj_t BgL_denvz00_25)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 730 */
				long BgL_lenz00_1788;

				BgL_lenz00_1788 = (VECTOR_LENGTH(BgL_codez00_23) - 2L);
				{
					long BgL_iz00_1790;
					obj_t BgL_lz00_1791;

					BgL_iz00_1790 = 0L;
					BgL_lz00_1791 = BTRUE;
				BgL_zc3z04anonymousza31607ze3z87_1792:
					if ((BgL_iz00_1790 < BgL_lenz00_1788))
						{	/* Eval/evmeaning.scm 734 */
							obj_t BgL_lz00_1794;

							BgL_lz00_1794 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_23,
									(BgL_iz00_1790 + 2L)), BgL_stackz00_24, BgL_denvz00_25);
							if (CBOOL(BgL_lz00_1794))
								{
									obj_t BgL_lz00_5104;
									long BgL_iz00_5102;

									BgL_iz00_5102 = (BgL_iz00_1790 + 1L);
									BgL_lz00_5104 = BgL_lz00_1794;
									BgL_lz00_1791 = BgL_lz00_5104;
									BgL_iz00_1790 = BgL_iz00_5102;
									goto BgL_zc3z04anonymousza31607ze3z87_1792;
								}
							else
								{	/* Eval/evmeaning.scm 735 */
									return BFALSE;
								}
						}
					else
						{	/* Eval/evmeaning.scm 733 */
							return BgL_lz00_1791;
						}
				}
			}
		}

	}



/* evmeaning-bounce-67 */
	obj_t BGl_evmeaningzd2bouncezd267z00zz__evmeaningz00(obj_t BgL_codez00_26,
		obj_t BgL_stackz00_27, obj_t BgL_denvz00_28)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 722 */
				long BgL_lenz00_1801;

				BgL_lenz00_1801 = (VECTOR_LENGTH(BgL_codez00_26) - 2L);
				{
					long BgL_iz00_1803;

					BgL_iz00_1803 = 0L;
				BgL_zc3z04anonymousza31613ze3z87_1804:
					if ((BgL_iz00_1803 < BgL_lenz00_1801))
						{	/* Eval/evmeaning.scm 725 */
							obj_t BgL__ortest_1103z00_1806;

							BgL__ortest_1103z00_1806 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_26,
									(BgL_iz00_1803 + 2L)), BgL_stackz00_27, BgL_denvz00_28);
							if (CBOOL(BgL__ortest_1103z00_1806))
								{	/* Eval/evmeaning.scm 725 */
									return BgL__ortest_1103z00_1806;
								}
							else
								{
									long BgL_iz00_5114;

									BgL_iz00_5114 = (BgL_iz00_1803 + 1L);
									BgL_iz00_1803 = BgL_iz00_5114;
									goto BgL_zc3z04anonymousza31613ze3z87_1804;
								}
						}
					else
						{	/* Eval/evmeaning.scm 724 */
							return BFALSE;
						}
				}
			}
		}

	}



/* evmeaning-bounce-64 */
	obj_t BGl_evmeaningzd2bouncezd264z00zz__evmeaningz00(obj_t BgL_codez00_29,
		obj_t BgL_stackz00_30, obj_t BgL_denvz00_31)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 700 */
				obj_t BgL_bodyz00_1812;
				obj_t BgL_protectz00_1813;

				BgL_bodyz00_1812 = VECTOR_REF(BgL_codez00_29, 2L);
				BgL_protectz00_1813 = VECTOR_REF(BgL_codez00_29, 3L);
				{	/* Eval/evmeaning.scm 702 */
					obj_t BgL_exitd1104z00_1814;

					BgL_exitd1104z00_1814 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Eval/evmeaning.scm 703 */
						obj_t BgL_zc3z04anonymousza31619ze3z87_3928;

						BgL_zc3z04anonymousza31619ze3z87_3928 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31619ze3ze5zz__evmeaningz00, (int) (0L),
							(int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31619ze3z87_3928, (int) (0L),
							BgL_protectz00_1813);
						PROCEDURE_SET(BgL_zc3z04anonymousza31619ze3z87_3928, (int) (1L),
							BgL_stackz00_30);
						PROCEDURE_SET(BgL_zc3z04anonymousza31619ze3z87_3928, (int) (2L),
							BgL_denvz00_31);
						{	/* Eval/evmeaning.scm 702 */
							obj_t BgL_arg2282z00_3238;

							{	/* Eval/evmeaning.scm 702 */
								obj_t BgL_arg2283z00_3239;

								BgL_arg2283z00_3239 = BGL_EXITD_PROTECT(BgL_exitd1104z00_1814);
								BgL_arg2282z00_3238 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31619ze3z87_3928,
									BgL_arg2283z00_3239);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1104z00_1814, BgL_arg2282z00_3238);
							BUNSPEC;
						}
						{	/* Eval/evmeaning.scm 702 */
							obj_t BgL_tmp1106z00_1816;

							BgL_tmp1106z00_1816 =
								BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_1812,
								BgL_stackz00_30, BgL_denvz00_31);
							{	/* Eval/evmeaning.scm 702 */
								bool_t BgL_test2480z00_5132;

								{	/* Eval/evmeaning.scm 702 */
									obj_t BgL_arg2281z00_3241;

									BgL_arg2281z00_3241 =
										BGL_EXITD_PROTECT(BgL_exitd1104z00_1814);
									BgL_test2480z00_5132 = PAIRP(BgL_arg2281z00_3241);
								}
								if (BgL_test2480z00_5132)
									{	/* Eval/evmeaning.scm 702 */
										obj_t BgL_arg2279z00_3242;

										{	/* Eval/evmeaning.scm 702 */
											obj_t BgL_arg2280z00_3243;

											BgL_arg2280z00_3243 =
												BGL_EXITD_PROTECT(BgL_exitd1104z00_1814);
											BgL_arg2279z00_3242 = CDR(((obj_t) BgL_arg2280z00_3243));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1104z00_1814,
											BgL_arg2279z00_3242);
										BUNSPEC;
									}
								else
									{	/* Eval/evmeaning.scm 702 */
										BFALSE;
									}
							}
							BGl_z62zc3z04anonymousza31619ze3ze5zz__evmeaningz00
								(BgL_zc3z04anonymousza31619ze3z87_3928);
							return BgL_tmp1106z00_1816;
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1619> */
	obj_t BGl_z62zc3z04anonymousza31619ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3929)
	{
		{	/* Eval/evmeaning.scm 702 */
			{	/* Eval/evmeaning.scm 703 */
				obj_t BgL_protectz00_3930;
				obj_t BgL_stackz00_3931;
				obj_t BgL_denvz00_3932;

				BgL_protectz00_3930 = PROCEDURE_REF(BgL_envz00_3929, (int) (0L));
				BgL_stackz00_3931 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3929, (int) (1L)));
				BgL_denvz00_3932 = ((obj_t) PROCEDURE_REF(BgL_envz00_3929, (int) (2L)));
				return
					BGl_evmeaningz00zz__evmeaningz00(BgL_protectz00_3930,
					BgL_stackz00_3931, BgL_denvz00_3932);
			}
		}

	}



/* evmeaning-bounce-54 */
	obj_t BGl_evmeaningzd2bouncezd254z00zz__evmeaningz00(obj_t BgL_codez00_32,
		obj_t BgL_stackz00_33, obj_t BgL_denvz00_34)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 681 */
				obj_t BgL_bodyz00_1819;

				BgL_bodyz00_1819 = VECTOR_REF(BgL_codez00_32, 2L);
				{	/* Eval/evmeaning.scm 684 */
					obj_t BgL_zc3z04anonymousza31621ze3z87_3933;

					BgL_zc3z04anonymousza31621ze3z87_3933 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31621ze3ze5zz__evmeaningz00, (int) (-4L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31621ze3z87_3933, (int) (0L),
						BgL_stackz00_33);
					PROCEDURE_SET(BgL_zc3z04anonymousza31621ze3z87_3933, (int) (1L),
						BgL_bodyz00_1819);
					PROCEDURE_SET(BgL_zc3z04anonymousza31621ze3z87_3933, (int) (2L),
						BgL_denvz00_34);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3246;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3247;

							BgL_newz00_3247 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5161;

								BgL_tmpz00_5161 = (int) (2L);
								STRUCT_SET(BgL_newz00_3247, BgL_tmpz00_5161, BgL_stackz00_33);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5164;

								BgL_tmpz00_5164 = (int) (1L);
								STRUCT_SET(BgL_newz00_3247, BgL_tmpz00_5164, BgL_bodyz00_1819);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5169;
								int BgL_tmpz00_5167;

								BgL_auxz00_5169 = BINT(-4L);
								BgL_tmpz00_5167 = (int) (0L);
								STRUCT_SET(BgL_newz00_3247, BgL_tmpz00_5167, BgL_auxz00_5169);
							}
							BgL_arg1792z00_3246 = BgL_newz00_3247;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31621ze3z87_3933,
							BgL_arg1792z00_3246);
						BgL_arg1792z00_3246;
					}
					return BgL_zc3z04anonymousza31621ze3z87_3933;
				}
			}
		}

	}



/* &<@anonymous:1621> */
	obj_t BGl_z62zc3z04anonymousza31621ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3934, obj_t BgL_xz00_3938, obj_t BgL_yz00_3939,
		obj_t BgL_za7za7_3940, obj_t BgL_tz00_3941)
	{
		{	/* Eval/evmeaning.scm 683 */
			{	/* Eval/evmeaning.scm 687 */
				obj_t BgL_stackz00_3935;
				obj_t BgL_bodyz00_3936;
				obj_t BgL_denvz00_3937;

				BgL_stackz00_3935 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3934, (int) (0L)));
				BgL_bodyz00_3936 = PROCEDURE_REF(BgL_envz00_3934, (int) (1L));
				BgL_denvz00_3937 = ((obj_t) PROCEDURE_REF(BgL_envz00_3934, (int) (2L)));
				{	/* Eval/evmeaning.scm 687 */
					obj_t BgL_arg1622z00_4144;

					{	/* Eval/evmeaning.scm 687 */
						obj_t BgL_arg1623z00_4145;

						{	/* Eval/evmeaning.scm 687 */
							obj_t BgL_arg1624z00_4146;

							{	/* Eval/evmeaning.scm 687 */
								obj_t BgL_arg1625z00_4147;

								BgL_arg1625z00_4147 =
									MAKE_YOUNG_PAIR(BgL_tz00_3941, BgL_stackz00_3935);
								BgL_arg1624z00_4146 =
									MAKE_YOUNG_PAIR(BgL_za7za7_3940, BgL_arg1625z00_4147);
							}
							BgL_arg1623z00_4145 =
								MAKE_YOUNG_PAIR(BgL_yz00_3939, BgL_arg1624z00_4146);
						}
						BgL_arg1622z00_4144 =
							MAKE_YOUNG_PAIR(BgL_xz00_3938, BgL_arg1623z00_4145);
					}
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3936,
						BgL_arg1622z00_4144, BgL_denvz00_3937);
				}
			}
		}

	}



/* evmeaning-bounce-50 */
	obj_t BGl_evmeaningzd2bouncezd250z00zz__evmeaningz00(obj_t BgL_codez00_35,
		obj_t BgL_stackz00_36, obj_t BgL_denvz00_37)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 656 */
				obj_t BgL_bodyz00_1832;
				obj_t BgL_wherez00_1833;
				obj_t BgL_locz00_1834;

				BgL_bodyz00_1832 = VECTOR_REF(BgL_codez00_35, 2L);
				BgL_wherez00_1833 = VECTOR_REF(BgL_codez00_35, 3L);
				BgL_locz00_1834 = VECTOR_REF(BgL_codez00_35, 1L);
				{	/* Eval/evmeaning.scm 661 */
					obj_t BgL_zc3z04anonymousza31627ze3z87_3942;

					BgL_zc3z04anonymousza31627ze3z87_3942 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31627ze3ze5zz__evmeaningz00, (int) (-4L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31627ze3z87_3942, (int) (0L),
						BgL_wherez00_1833);
					PROCEDURE_SET(BgL_zc3z04anonymousza31627ze3z87_3942, (int) (1L),
						BgL_locz00_1834);
					PROCEDURE_SET(BgL_zc3z04anonymousza31627ze3z87_3942, (int) (2L),
						BgL_stackz00_36);
					PROCEDURE_SET(BgL_zc3z04anonymousza31627ze3z87_3942, (int) (3L),
						BgL_bodyz00_1832);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3254;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3255;

							BgL_newz00_3255 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5202;

								BgL_tmpz00_5202 = (int) (2L);
								STRUCT_SET(BgL_newz00_3255, BgL_tmpz00_5202, BgL_stackz00_36);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5205;

								BgL_tmpz00_5205 = (int) (1L);
								STRUCT_SET(BgL_newz00_3255, BgL_tmpz00_5205, BgL_bodyz00_1832);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5210;
								int BgL_tmpz00_5208;

								BgL_auxz00_5210 = BINT(-4L);
								BgL_tmpz00_5208 = (int) (0L);
								STRUCT_SET(BgL_newz00_3255, BgL_tmpz00_5208, BgL_auxz00_5210);
							}
							BgL_arg1792z00_3254 = BgL_newz00_3255;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31627ze3z87_3942,
							BgL_arg1792z00_3254);
						BgL_arg1792z00_3254;
					}
					return BgL_zc3z04anonymousza31627ze3z87_3942;
				}
			}
		}

	}



/* &<@anonymous:1627> */
	obj_t BGl_z62zc3z04anonymousza31627ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3943, obj_t BgL_xz00_3948, obj_t BgL_yz00_3949,
		obj_t BgL_za7za7_3950, obj_t BgL_tz00_3951)
	{
		{	/* Eval/evmeaning.scm 660 */
			{	/* Eval/evmeaning.scm 661 */
				obj_t BgL_wherez00_3944;
				obj_t BgL_locz00_3945;
				obj_t BgL_stackz00_3946;
				obj_t BgL_bodyz00_3947;

				BgL_wherez00_3944 = PROCEDURE_REF(BgL_envz00_3943, (int) (0L));
				BgL_locz00_3945 = PROCEDURE_REF(BgL_envz00_3943, (int) (1L));
				BgL_stackz00_3946 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3943, (int) (2L)));
				BgL_bodyz00_3947 = PROCEDURE_REF(BgL_envz00_3943, (int) (3L));
				{	/* Eval/evmeaning.scm 661 */
					obj_t BgL_z12denvz12_4148;

					BgL_z12denvz12_4148 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 664 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4148, BgL_wherez00_3944,
							BgL_locz00_3945);
						{	/* Eval/evmeaning.scm 666 */
							obj_t BgL_resz00_4149;

							{	/* Eval/evmeaning.scm 671 */
								obj_t BgL_arg1628z00_4150;

								{	/* Eval/evmeaning.scm 671 */
									obj_t BgL_arg1629z00_4151;

									{	/* Eval/evmeaning.scm 671 */
										obj_t BgL_arg1630z00_4152;

										{	/* Eval/evmeaning.scm 671 */
											obj_t BgL_arg1631z00_4153;

											BgL_arg1631z00_4153 =
												MAKE_YOUNG_PAIR(BgL_tz00_3951, BgL_stackz00_3946);
											BgL_arg1630z00_4152 =
												MAKE_YOUNG_PAIR(BgL_za7za7_3950, BgL_arg1631z00_4153);
										}
										BgL_arg1629z00_4151 =
											MAKE_YOUNG_PAIR(BgL_yz00_3949, BgL_arg1630z00_4152);
									}
									BgL_arg1628z00_4150 =
										MAKE_YOUNG_PAIR(BgL_xz00_3948, BgL_arg1629z00_4151);
								}
								BgL_resz00_4149 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3947,
									BgL_arg1628z00_4150, BgL_z12denvz12_4148);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4148);
							return BgL_resz00_4149;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-53 */
	obj_t BGl_evmeaningzd2bouncezd253z00zz__evmeaningz00(obj_t BgL_codez00_38,
		obj_t BgL_stackz00_39, obj_t BgL_denvz00_40)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 644 */
				obj_t BgL_bodyz00_1850;

				BgL_bodyz00_1850 = VECTOR_REF(BgL_codez00_38, 2L);
				{	/* Eval/evmeaning.scm 647 */
					obj_t BgL_zc3z04anonymousza31635ze3z87_3952;

					BgL_zc3z04anonymousza31635ze3z87_3952 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31635ze3ze5zz__evmeaningz00, (int) (-3L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31635ze3z87_3952, (int) (0L),
						BgL_stackz00_39);
					PROCEDURE_SET(BgL_zc3z04anonymousza31635ze3z87_3952, (int) (1L),
						BgL_bodyz00_1850);
					PROCEDURE_SET(BgL_zc3z04anonymousza31635ze3z87_3952, (int) (2L),
						BgL_denvz00_40);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3260;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3261;

							BgL_newz00_3261 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5243;

								BgL_tmpz00_5243 = (int) (2L);
								STRUCT_SET(BgL_newz00_3261, BgL_tmpz00_5243, BgL_stackz00_39);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5246;

								BgL_tmpz00_5246 = (int) (1L);
								STRUCT_SET(BgL_newz00_3261, BgL_tmpz00_5246, BgL_bodyz00_1850);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5251;
								int BgL_tmpz00_5249;

								BgL_auxz00_5251 = BINT(-3L);
								BgL_tmpz00_5249 = (int) (0L);
								STRUCT_SET(BgL_newz00_3261, BgL_tmpz00_5249, BgL_auxz00_5251);
							}
							BgL_arg1792z00_3260 = BgL_newz00_3261;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31635ze3z87_3952,
							BgL_arg1792z00_3260);
						BgL_arg1792z00_3260;
					}
					return BgL_zc3z04anonymousza31635ze3z87_3952;
				}
			}
		}

	}



/* &<@anonymous:1635> */
	obj_t BGl_z62zc3z04anonymousza31635ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3953, obj_t BgL_xz00_3957, obj_t BgL_yz00_3958,
		obj_t BgL_za7za7_3959)
	{
		{	/* Eval/evmeaning.scm 646 */
			{	/* Eval/evmeaning.scm 649 */
				obj_t BgL_stackz00_3954;
				obj_t BgL_bodyz00_3955;
				obj_t BgL_denvz00_3956;

				BgL_stackz00_3954 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3953, (int) (0L)));
				BgL_bodyz00_3955 = PROCEDURE_REF(BgL_envz00_3953, (int) (1L));
				BgL_denvz00_3956 = ((obj_t) PROCEDURE_REF(BgL_envz00_3953, (int) (2L)));
				{	/* Eval/evmeaning.scm 649 */
					obj_t BgL_arg1636z00_4154;

					{	/* Eval/evmeaning.scm 649 */
						obj_t BgL_arg1637z00_4155;

						{	/* Eval/evmeaning.scm 649 */
							obj_t BgL_arg1638z00_4156;

							BgL_arg1638z00_4156 =
								MAKE_YOUNG_PAIR(BgL_za7za7_3959, BgL_stackz00_3954);
							BgL_arg1637z00_4155 =
								MAKE_YOUNG_PAIR(BgL_yz00_3958, BgL_arg1638z00_4156);
						}
						BgL_arg1636z00_4154 =
							MAKE_YOUNG_PAIR(BgL_xz00_3957, BgL_arg1637z00_4155);
					}
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3955,
						BgL_arg1636z00_4154, BgL_denvz00_3956);
				}
			}
		}

	}



/* evmeaning-bounce-49 */
	obj_t BGl_evmeaningzd2bouncezd249z00zz__evmeaningz00(obj_t BgL_codez00_41,
		obj_t BgL_stackz00_42, obj_t BgL_denvz00_43)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 622 */
				obj_t BgL_bodyz00_1861;
				obj_t BgL_wherez00_1862;
				obj_t BgL_locz00_1863;

				BgL_bodyz00_1861 = VECTOR_REF(BgL_codez00_41, 2L);
				BgL_wherez00_1862 = VECTOR_REF(BgL_codez00_41, 3L);
				BgL_locz00_1863 = VECTOR_REF(BgL_codez00_41, 1L);
				{	/* Eval/evmeaning.scm 627 */
					obj_t BgL_zc3z04anonymousza31640ze3z87_3960;

					BgL_zc3z04anonymousza31640ze3z87_3960 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31640ze3ze5zz__evmeaningz00, (int) (-3L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31640ze3z87_3960, (int) (0L),
						BgL_wherez00_1862);
					PROCEDURE_SET(BgL_zc3z04anonymousza31640ze3z87_3960, (int) (1L),
						BgL_locz00_1863);
					PROCEDURE_SET(BgL_zc3z04anonymousza31640ze3z87_3960, (int) (2L),
						BgL_stackz00_42);
					PROCEDURE_SET(BgL_zc3z04anonymousza31640ze3z87_3960, (int) (3L),
						BgL_bodyz00_1861);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3268;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3269;

							BgL_newz00_3269 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5283;

								BgL_tmpz00_5283 = (int) (2L);
								STRUCT_SET(BgL_newz00_3269, BgL_tmpz00_5283, BgL_stackz00_42);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5286;

								BgL_tmpz00_5286 = (int) (1L);
								STRUCT_SET(BgL_newz00_3269, BgL_tmpz00_5286, BgL_bodyz00_1861);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5291;
								int BgL_tmpz00_5289;

								BgL_auxz00_5291 = BINT(-3L);
								BgL_tmpz00_5289 = (int) (0L);
								STRUCT_SET(BgL_newz00_3269, BgL_tmpz00_5289, BgL_auxz00_5291);
							}
							BgL_arg1792z00_3268 = BgL_newz00_3269;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31640ze3z87_3960,
							BgL_arg1792z00_3268);
						BgL_arg1792z00_3268;
					}
					return BgL_zc3z04anonymousza31640ze3z87_3960;
				}
			}
		}

	}



/* &<@anonymous:1640> */
	obj_t BGl_z62zc3z04anonymousza31640ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3961, obj_t BgL_xz00_3966, obj_t BgL_yz00_3967,
		obj_t BgL_za7za7_3968)
	{
		{	/* Eval/evmeaning.scm 626 */
			{	/* Eval/evmeaning.scm 627 */
				obj_t BgL_wherez00_3962;
				obj_t BgL_locz00_3963;
				obj_t BgL_stackz00_3964;
				obj_t BgL_bodyz00_3965;

				BgL_wherez00_3962 = PROCEDURE_REF(BgL_envz00_3961, (int) (0L));
				BgL_locz00_3963 = PROCEDURE_REF(BgL_envz00_3961, (int) (1L));
				BgL_stackz00_3964 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3961, (int) (2L)));
				BgL_bodyz00_3965 = PROCEDURE_REF(BgL_envz00_3961, (int) (3L));
				{	/* Eval/evmeaning.scm 627 */
					obj_t BgL_z12denvz12_4157;

					BgL_z12denvz12_4157 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 630 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4157, BgL_wherez00_3962,
							BgL_locz00_3963);
						{	/* Eval/evmeaning.scm 632 */
							obj_t BgL_resz00_4158;

							{	/* Eval/evmeaning.scm 635 */
								obj_t BgL_arg1641z00_4159;

								{	/* Eval/evmeaning.scm 635 */
									obj_t BgL_arg1642z00_4160;

									{	/* Eval/evmeaning.scm 635 */
										obj_t BgL_arg1643z00_4161;

										BgL_arg1643z00_4161 =
											MAKE_YOUNG_PAIR(BgL_za7za7_3968, BgL_stackz00_3964);
										BgL_arg1642z00_4160 =
											MAKE_YOUNG_PAIR(BgL_yz00_3967, BgL_arg1643z00_4161);
									}
									BgL_arg1641z00_4159 =
										MAKE_YOUNG_PAIR(BgL_xz00_3966, BgL_arg1642z00_4160);
								}
								BgL_resz00_4158 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3965,
									BgL_arg1641z00_4159, BgL_z12denvz12_4157);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4157);
							return BgL_resz00_4158;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-52 */
	obj_t BGl_evmeaningzd2bouncezd252z00zz__evmeaningz00(obj_t BgL_codez00_44,
		obj_t BgL_stackz00_45, obj_t BgL_denvz00_46)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 612 */
				obj_t BgL_bodyz00_1877;

				BgL_bodyz00_1877 = VECTOR_REF(BgL_codez00_44, 2L);
				{	/* Eval/evmeaning.scm 615 */
					obj_t BgL_zc3z04anonymousza31645ze3z87_3969;

					BgL_zc3z04anonymousza31645ze3z87_3969 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31645ze3ze5zz__evmeaningz00, (int) (-2L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31645ze3z87_3969, (int) (0L),
						BgL_stackz00_45);
					PROCEDURE_SET(BgL_zc3z04anonymousza31645ze3z87_3969, (int) (1L),
						BgL_bodyz00_1877);
					PROCEDURE_SET(BgL_zc3z04anonymousza31645ze3z87_3969, (int) (2L),
						BgL_denvz00_46);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3274;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3275;

							BgL_newz00_3275 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5323;

								BgL_tmpz00_5323 = (int) (2L);
								STRUCT_SET(BgL_newz00_3275, BgL_tmpz00_5323, BgL_stackz00_45);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5326;

								BgL_tmpz00_5326 = (int) (1L);
								STRUCT_SET(BgL_newz00_3275, BgL_tmpz00_5326, BgL_bodyz00_1877);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5331;
								int BgL_tmpz00_5329;

								BgL_auxz00_5331 = BINT(-2L);
								BgL_tmpz00_5329 = (int) (0L);
								STRUCT_SET(BgL_newz00_3275, BgL_tmpz00_5329, BgL_auxz00_5331);
							}
							BgL_arg1792z00_3274 = BgL_newz00_3275;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31645ze3z87_3969,
							BgL_arg1792z00_3274);
						BgL_arg1792z00_3274;
					}
					return BgL_zc3z04anonymousza31645ze3z87_3969;
				}
			}
		}

	}



/* &<@anonymous:1645> */
	obj_t BGl_z62zc3z04anonymousza31645ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3970, obj_t BgL_xz00_3974, obj_t BgL_yz00_3975)
	{
		{	/* Eval/evmeaning.scm 614 */
			{	/* Eval/evmeaning.scm 616 */
				obj_t BgL_stackz00_3971;
				obj_t BgL_bodyz00_3972;
				obj_t BgL_denvz00_3973;

				BgL_stackz00_3971 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3970, (int) (0L)));
				BgL_bodyz00_3972 = PROCEDURE_REF(BgL_envz00_3970, (int) (1L));
				BgL_denvz00_3973 = ((obj_t) PROCEDURE_REF(BgL_envz00_3970, (int) (2L)));
				{	/* Eval/evmeaning.scm 616 */
					obj_t BgL_arg1646z00_4162;

					{	/* Eval/evmeaning.scm 616 */
						obj_t BgL_arg1648z00_4163;

						BgL_arg1648z00_4163 =
							MAKE_YOUNG_PAIR(BgL_yz00_3975, BgL_stackz00_3971);
						BgL_arg1646z00_4162 =
							MAKE_YOUNG_PAIR(BgL_xz00_3974, BgL_arg1648z00_4163);
					}
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3972,
						BgL_arg1646z00_4162, BgL_denvz00_3973);
				}
			}
		}

	}



/* evmeaning-bounce-48 */
	obj_t BGl_evmeaningzd2bouncezd248z00zz__evmeaningz00(obj_t BgL_codez00_47,
		obj_t BgL_stackz00_48, obj_t BgL_denvz00_49)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 594 */
				obj_t BgL_bodyz00_1886;
				obj_t BgL_wherez00_1887;
				obj_t BgL_locz00_1888;

				BgL_bodyz00_1886 = VECTOR_REF(BgL_codez00_47, 2L);
				BgL_wherez00_1887 = VECTOR_REF(BgL_codez00_47, 3L);
				BgL_locz00_1888 = VECTOR_REF(BgL_codez00_47, 1L);
				{	/* Eval/evmeaning.scm 599 */
					obj_t BgL_zc3z04anonymousza31650ze3z87_3976;

					BgL_zc3z04anonymousza31650ze3z87_3976 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31650ze3ze5zz__evmeaningz00, (int) (-2L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31650ze3z87_3976, (int) (0L),
						BgL_wherez00_1887);
					PROCEDURE_SET(BgL_zc3z04anonymousza31650ze3z87_3976, (int) (1L),
						BgL_locz00_1888);
					PROCEDURE_SET(BgL_zc3z04anonymousza31650ze3z87_3976, (int) (2L),
						BgL_stackz00_48);
					PROCEDURE_SET(BgL_zc3z04anonymousza31650ze3z87_3976, (int) (3L),
						BgL_bodyz00_1886);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3282;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3283;

							BgL_newz00_3283 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5362;

								BgL_tmpz00_5362 = (int) (2L);
								STRUCT_SET(BgL_newz00_3283, BgL_tmpz00_5362, BgL_stackz00_48);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5365;

								BgL_tmpz00_5365 = (int) (1L);
								STRUCT_SET(BgL_newz00_3283, BgL_tmpz00_5365, BgL_bodyz00_1886);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5370;
								int BgL_tmpz00_5368;

								BgL_auxz00_5370 = BINT(-2L);
								BgL_tmpz00_5368 = (int) (0L);
								STRUCT_SET(BgL_newz00_3283, BgL_tmpz00_5368, BgL_auxz00_5370);
							}
							BgL_arg1792z00_3282 = BgL_newz00_3283;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31650ze3z87_3976,
							BgL_arg1792z00_3282);
						BgL_arg1792z00_3282;
					}
					return BgL_zc3z04anonymousza31650ze3z87_3976;
				}
			}
		}

	}



/* &<@anonymous:1650> */
	obj_t BGl_z62zc3z04anonymousza31650ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3977, obj_t BgL_xz00_3982, obj_t BgL_yz00_3983)
	{
		{	/* Eval/evmeaning.scm 598 */
			{	/* Eval/evmeaning.scm 599 */
				obj_t BgL_wherez00_3978;
				obj_t BgL_locz00_3979;
				obj_t BgL_stackz00_3980;
				obj_t BgL_bodyz00_3981;

				BgL_wherez00_3978 = PROCEDURE_REF(BgL_envz00_3977, (int) (0L));
				BgL_locz00_3979 = PROCEDURE_REF(BgL_envz00_3977, (int) (1L));
				BgL_stackz00_3980 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3977, (int) (2L)));
				BgL_bodyz00_3981 = PROCEDURE_REF(BgL_envz00_3977, (int) (3L));
				{	/* Eval/evmeaning.scm 599 */
					obj_t BgL_z12denvz12_4164;

					BgL_z12denvz12_4164 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 602 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4164, BgL_wherez00_3978,
							BgL_locz00_3979);
						{	/* Eval/evmeaning.scm 604 */
							obj_t BgL_rz00_4165;

							{	/* Eval/evmeaning.scm 604 */
								obj_t BgL_arg1651z00_4166;

								{	/* Eval/evmeaning.scm 604 */
									obj_t BgL_arg1652z00_4167;

									BgL_arg1652z00_4167 =
										MAKE_YOUNG_PAIR(BgL_yz00_3983, BgL_stackz00_3980);
									BgL_arg1651z00_4166 =
										MAKE_YOUNG_PAIR(BgL_xz00_3982, BgL_arg1652z00_4167);
								}
								BgL_rz00_4165 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3981,
									BgL_arg1651z00_4166, BgL_z12denvz12_4164);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4164);
							return BgL_rz00_4165;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-51 */
	obj_t BGl_evmeaningzd2bouncezd251z00zz__evmeaningz00(obj_t BgL_codez00_50,
		obj_t BgL_stackz00_51, obj_t BgL_denvz00_52)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 584 */
				obj_t BgL_bodyz00_1900;

				BgL_bodyz00_1900 = VECTOR_REF(BgL_codez00_50, 2L);
				{	/* Eval/evmeaning.scm 587 */
					obj_t BgL_zc3z04anonymousza31654ze3z87_3984;

					BgL_zc3z04anonymousza31654ze3z87_3984 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31654ze3ze5zz__evmeaningz00, (int) (-1L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31654ze3z87_3984, (int) (0L),
						BgL_stackz00_51);
					PROCEDURE_SET(BgL_zc3z04anonymousza31654ze3z87_3984, (int) (1L),
						BgL_bodyz00_1900);
					PROCEDURE_SET(BgL_zc3z04anonymousza31654ze3z87_3984, (int) (2L),
						BgL_denvz00_52);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3288;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3289;

							BgL_newz00_3289 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5401;

								BgL_tmpz00_5401 = (int) (2L);
								STRUCT_SET(BgL_newz00_3289, BgL_tmpz00_5401, BgL_stackz00_51);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5404;

								BgL_tmpz00_5404 = (int) (1L);
								STRUCT_SET(BgL_newz00_3289, BgL_tmpz00_5404, BgL_bodyz00_1900);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5409;
								int BgL_tmpz00_5407;

								BgL_auxz00_5409 = BINT(-1L);
								BgL_tmpz00_5407 = (int) (0L);
								STRUCT_SET(BgL_newz00_3289, BgL_tmpz00_5407, BgL_auxz00_5409);
							}
							BgL_arg1792z00_3288 = BgL_newz00_3289;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31654ze3z87_3984,
							BgL_arg1792z00_3288);
						BgL_arg1792z00_3288;
					}
					return BgL_zc3z04anonymousza31654ze3z87_3984;
				}
			}
		}

	}



/* &<@anonymous:1654> */
	obj_t BGl_z62zc3z04anonymousza31654ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3985, obj_t BgL_xz00_3989)
	{
		{	/* Eval/evmeaning.scm 586 */
			{	/* Eval/evmeaning.scm 588 */
				obj_t BgL_stackz00_3986;
				obj_t BgL_bodyz00_3987;
				obj_t BgL_denvz00_3988;

				BgL_stackz00_3986 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3985, (int) (0L)));
				BgL_bodyz00_3987 = PROCEDURE_REF(BgL_envz00_3985, (int) (1L));
				BgL_denvz00_3988 = ((obj_t) PROCEDURE_REF(BgL_envz00_3985, (int) (2L)));
				{	/* Eval/evmeaning.scm 588 */
					obj_t BgL_arg1656z00_4168;

					BgL_arg1656z00_4168 =
						MAKE_YOUNG_PAIR(BgL_xz00_3989, BgL_stackz00_3986);
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3987,
						BgL_arg1656z00_4168, BgL_denvz00_3988);
				}
			}
		}

	}



/* evmeaning-bounce-47 */
	obj_t BGl_evmeaningzd2bouncezd247z00zz__evmeaningz00(obj_t BgL_codez00_53,
		obj_t BgL_stackz00_54, obj_t BgL_denvz00_55)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 566 */
				obj_t BgL_bodyz00_1907;
				obj_t BgL_wherez00_1908;
				obj_t BgL_locz00_1909;

				BgL_bodyz00_1907 = VECTOR_REF(BgL_codez00_53, 2L);
				BgL_wherez00_1908 = VECTOR_REF(BgL_codez00_53, 3L);
				BgL_locz00_1909 = VECTOR_REF(BgL_codez00_53, 1L);
				{	/* Eval/evmeaning.scm 571 */
					obj_t BgL_zc3z04anonymousza31658ze3z87_3990;

					BgL_zc3z04anonymousza31658ze3z87_3990 =
						MAKE_VA_PROCEDURE
						(BGl_z62zc3z04anonymousza31658ze3ze5zz__evmeaningz00, (int) (-1L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31658ze3z87_3990, (int) (0L),
						BgL_wherez00_1908);
					PROCEDURE_SET(BgL_zc3z04anonymousza31658ze3z87_3990, (int) (1L),
						BgL_locz00_1909);
					PROCEDURE_SET(BgL_zc3z04anonymousza31658ze3z87_3990, (int) (2L),
						BgL_stackz00_54);
					PROCEDURE_SET(BgL_zc3z04anonymousza31658ze3z87_3990, (int) (3L),
						BgL_bodyz00_1907);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3296;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3297;

							BgL_newz00_3297 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5439;

								BgL_tmpz00_5439 = (int) (2L);
								STRUCT_SET(BgL_newz00_3297, BgL_tmpz00_5439, BgL_stackz00_54);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5442;

								BgL_tmpz00_5442 = (int) (1L);
								STRUCT_SET(BgL_newz00_3297, BgL_tmpz00_5442, BgL_bodyz00_1907);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5447;
								int BgL_tmpz00_5445;

								BgL_auxz00_5447 = BINT(-1L);
								BgL_tmpz00_5445 = (int) (0L);
								STRUCT_SET(BgL_newz00_3297, BgL_tmpz00_5445, BgL_auxz00_5447);
							}
							BgL_arg1792z00_3296 = BgL_newz00_3297;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31658ze3z87_3990,
							BgL_arg1792z00_3296);
						BgL_arg1792z00_3296;
					}
					return BgL_zc3z04anonymousza31658ze3z87_3990;
				}
			}
		}

	}



/* &<@anonymous:1658> */
	obj_t BGl_z62zc3z04anonymousza31658ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3991, obj_t BgL_xz00_3996)
	{
		{	/* Eval/evmeaning.scm 570 */
			{	/* Eval/evmeaning.scm 571 */
				obj_t BgL_wherez00_3992;
				obj_t BgL_locz00_3993;
				obj_t BgL_stackz00_3994;
				obj_t BgL_bodyz00_3995;

				BgL_wherez00_3992 = PROCEDURE_REF(BgL_envz00_3991, (int) (0L));
				BgL_locz00_3993 = PROCEDURE_REF(BgL_envz00_3991, (int) (1L));
				BgL_stackz00_3994 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3991, (int) (2L)));
				BgL_bodyz00_3995 = PROCEDURE_REF(BgL_envz00_3991, (int) (3L));
				{	/* Eval/evmeaning.scm 571 */
					obj_t BgL_z12denvz12_4169;

					BgL_z12denvz12_4169 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 574 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4169, BgL_wherez00_3992,
							BgL_locz00_3993);
						{	/* Eval/evmeaning.scm 576 */
							obj_t BgL_resz00_4170;

							{	/* Eval/evmeaning.scm 576 */
								obj_t BgL_arg1661z00_4171;

								BgL_arg1661z00_4171 =
									MAKE_YOUNG_PAIR(BgL_xz00_3996, BgL_stackz00_3994);
								BgL_resz00_4170 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_3995,
									BgL_arg1661z00_4171, BgL_z12denvz12_4169);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4169);
							return BgL_resz00_4170;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-46 */
	obj_t BGl_evmeaningzd2bouncezd246z00zz__evmeaningz00(obj_t BgL_codez00_56,
		obj_t BgL_stackz00_57, obj_t BgL_denvz00_58)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 554 */
				obj_t BgL_bodyz00_1919;

				BgL_bodyz00_1919 = VECTOR_REF(BgL_codez00_56, 2L);
				{	/* Eval/evmeaning.scm 557 */
					obj_t BgL_zc3z04anonymousza31664ze3z87_3997;

					BgL_zc3z04anonymousza31664ze3z87_3997 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31664ze3ze5zz__evmeaningz00, (int) (4L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31664ze3z87_3997, (int) (0L),
						BgL_stackz00_57);
					PROCEDURE_SET(BgL_zc3z04anonymousza31664ze3z87_3997, (int) (1L),
						BgL_bodyz00_1919);
					PROCEDURE_SET(BgL_zc3z04anonymousza31664ze3z87_3997, (int) (2L),
						BgL_denvz00_58);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3302;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3303;

							BgL_newz00_3303 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5477;

								BgL_tmpz00_5477 = (int) (2L);
								STRUCT_SET(BgL_newz00_3303, BgL_tmpz00_5477, BgL_stackz00_57);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5480;

								BgL_tmpz00_5480 = (int) (1L);
								STRUCT_SET(BgL_newz00_3303, BgL_tmpz00_5480, BgL_bodyz00_1919);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5485;
								int BgL_tmpz00_5483;

								BgL_auxz00_5485 = BINT(4L);
								BgL_tmpz00_5483 = (int) (0L);
								STRUCT_SET(BgL_newz00_3303, BgL_tmpz00_5483, BgL_auxz00_5485);
							}
							BgL_arg1792z00_3302 = BgL_newz00_3303;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31664ze3z87_3997,
							BgL_arg1792z00_3302);
						BgL_arg1792z00_3302;
					}
					return BgL_zc3z04anonymousza31664ze3z87_3997;
				}
			}
		}

	}



/* &<@anonymous:1664> */
	obj_t BGl_z62zc3z04anonymousza31664ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_3998, obj_t BgL_xz00_4002, obj_t BgL_yz00_4003,
		obj_t BgL_za7za7_4004, obj_t BgL_tz00_4005)
	{
		{	/* Eval/evmeaning.scm 556 */
			{	/* Eval/evmeaning.scm 559 */
				obj_t BgL_stackz00_3999;
				obj_t BgL_bodyz00_4000;
				obj_t BgL_denvz00_4001;

				BgL_stackz00_3999 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3998, (int) (0L)));
				BgL_bodyz00_4000 = PROCEDURE_REF(BgL_envz00_3998, (int) (1L));
				BgL_denvz00_4001 = ((obj_t) PROCEDURE_REF(BgL_envz00_3998, (int) (2L)));
				{	/* Eval/evmeaning.scm 559 */
					obj_t BgL_arg1667z00_4172;

					{	/* Eval/evmeaning.scm 559 */
						obj_t BgL_arg1668z00_4173;

						{	/* Eval/evmeaning.scm 559 */
							obj_t BgL_arg1669z00_4174;

							{	/* Eval/evmeaning.scm 559 */
								obj_t BgL_arg1670z00_4175;

								BgL_arg1670z00_4175 =
									MAKE_YOUNG_PAIR(BgL_tz00_4005, BgL_stackz00_3999);
								BgL_arg1669z00_4174 =
									MAKE_YOUNG_PAIR(BgL_za7za7_4004, BgL_arg1670z00_4175);
							}
							BgL_arg1668z00_4173 =
								MAKE_YOUNG_PAIR(BgL_yz00_4003, BgL_arg1669z00_4174);
						}
						BgL_arg1667z00_4172 =
							MAKE_YOUNG_PAIR(BgL_xz00_4002, BgL_arg1668z00_4173);
					}
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4000,
						BgL_arg1667z00_4172, BgL_denvz00_4001);
				}
			}
		}

	}



/* evmeaning-bounce-41 */
	obj_t BGl_evmeaningzd2bouncezd241z00zz__evmeaningz00(obj_t BgL_codez00_59,
		obj_t BgL_stackz00_60, obj_t BgL_denvz00_61)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 530 */
				obj_t BgL_bodyz00_1932;
				obj_t BgL_wherez00_1933;
				obj_t BgL_locz00_1934;

				BgL_bodyz00_1932 = VECTOR_REF(BgL_codez00_59, 2L);
				BgL_wherez00_1933 = VECTOR_REF(BgL_codez00_59, 3L);
				BgL_locz00_1934 = VECTOR_REF(BgL_codez00_59, 1L);
				{	/* Eval/evmeaning.scm 535 */
					obj_t BgL_zc3z04anonymousza31676ze3z87_4006;

					BgL_zc3z04anonymousza31676ze3z87_4006 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31676ze3ze5zz__evmeaningz00, (int) (4L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31676ze3z87_4006, (int) (0L),
						BgL_wherez00_1933);
					PROCEDURE_SET(BgL_zc3z04anonymousza31676ze3z87_4006, (int) (1L),
						BgL_locz00_1934);
					PROCEDURE_SET(BgL_zc3z04anonymousza31676ze3z87_4006, (int) (2L),
						BgL_stackz00_60);
					PROCEDURE_SET(BgL_zc3z04anonymousza31676ze3z87_4006, (int) (3L),
						BgL_bodyz00_1932);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3310;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3311;

							BgL_newz00_3311 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5518;

								BgL_tmpz00_5518 = (int) (2L);
								STRUCT_SET(BgL_newz00_3311, BgL_tmpz00_5518, BgL_stackz00_60);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5521;

								BgL_tmpz00_5521 = (int) (1L);
								STRUCT_SET(BgL_newz00_3311, BgL_tmpz00_5521, BgL_bodyz00_1932);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5526;
								int BgL_tmpz00_5524;

								BgL_auxz00_5526 = BINT(4L);
								BgL_tmpz00_5524 = (int) (0L);
								STRUCT_SET(BgL_newz00_3311, BgL_tmpz00_5524, BgL_auxz00_5526);
							}
							BgL_arg1792z00_3310 = BgL_newz00_3311;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31676ze3z87_4006,
							BgL_arg1792z00_3310);
						BgL_arg1792z00_3310;
					}
					return BgL_zc3z04anonymousza31676ze3z87_4006;
				}
			}
		}

	}



/* &<@anonymous:1676> */
	obj_t BGl_z62zc3z04anonymousza31676ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4007, obj_t BgL_xz00_4012, obj_t BgL_yz00_4013,
		obj_t BgL_za7za7_4014, obj_t BgL_tz00_4015)
	{
		{	/* Eval/evmeaning.scm 534 */
			{	/* Eval/evmeaning.scm 535 */
				obj_t BgL_wherez00_4008;
				obj_t BgL_locz00_4009;
				obj_t BgL_stackz00_4010;
				obj_t BgL_bodyz00_4011;

				BgL_wherez00_4008 = PROCEDURE_REF(BgL_envz00_4007, (int) (0L));
				BgL_locz00_4009 = PROCEDURE_REF(BgL_envz00_4007, (int) (1L));
				BgL_stackz00_4010 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4007, (int) (2L)));
				BgL_bodyz00_4011 = PROCEDURE_REF(BgL_envz00_4007, (int) (3L));
				{	/* Eval/evmeaning.scm 535 */
					obj_t BgL_z12denvz12_4176;

					BgL_z12denvz12_4176 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 538 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4176, BgL_wherez00_4008,
							BgL_locz00_4009);
						{	/* Eval/evmeaning.scm 540 */
							obj_t BgL_resz00_4177;

							{	/* Eval/evmeaning.scm 545 */
								obj_t BgL_arg1678z00_4178;

								{	/* Eval/evmeaning.scm 545 */
									obj_t BgL_arg1681z00_4179;

									{	/* Eval/evmeaning.scm 545 */
										obj_t BgL_arg1684z00_4180;

										{	/* Eval/evmeaning.scm 545 */
											obj_t BgL_arg1685z00_4181;

											BgL_arg1685z00_4181 =
												MAKE_YOUNG_PAIR(BgL_tz00_4015, BgL_stackz00_4010);
											BgL_arg1684z00_4180 =
												MAKE_YOUNG_PAIR(BgL_za7za7_4014, BgL_arg1685z00_4181);
										}
										BgL_arg1681z00_4179 =
											MAKE_YOUNG_PAIR(BgL_yz00_4013, BgL_arg1684z00_4180);
									}
									BgL_arg1678z00_4178 =
										MAKE_YOUNG_PAIR(BgL_xz00_4012, BgL_arg1681z00_4179);
								}
								BgL_resz00_4177 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4011,
									BgL_arg1678z00_4178, BgL_z12denvz12_4176);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4176);
							return BgL_resz00_4177;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-45 */
	obj_t BGl_evmeaningzd2bouncezd245z00zz__evmeaningz00(obj_t BgL_codez00_62,
		obj_t BgL_stackz00_63, obj_t BgL_denvz00_64)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 520 */
				obj_t BgL_bodyz00_1950;

				BgL_bodyz00_1950 = VECTOR_REF(BgL_codez00_62, 2L);
				{	/* Eval/evmeaning.scm 523 */
					obj_t BgL_zc3z04anonymousza31689ze3z87_4016;

					BgL_zc3z04anonymousza31689ze3z87_4016 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31689ze3ze5zz__evmeaningz00, (int) (3L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31689ze3z87_4016, (int) (0L),
						BgL_stackz00_63);
					PROCEDURE_SET(BgL_zc3z04anonymousza31689ze3z87_4016, (int) (1L),
						BgL_bodyz00_1950);
					PROCEDURE_SET(BgL_zc3z04anonymousza31689ze3z87_4016, (int) (2L),
						BgL_denvz00_64);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3316;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3317;

							BgL_newz00_3317 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5559;

								BgL_tmpz00_5559 = (int) (2L);
								STRUCT_SET(BgL_newz00_3317, BgL_tmpz00_5559, BgL_stackz00_63);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5562;

								BgL_tmpz00_5562 = (int) (1L);
								STRUCT_SET(BgL_newz00_3317, BgL_tmpz00_5562, BgL_bodyz00_1950);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5567;
								int BgL_tmpz00_5565;

								BgL_auxz00_5567 = BINT(3L);
								BgL_tmpz00_5565 = (int) (0L);
								STRUCT_SET(BgL_newz00_3317, BgL_tmpz00_5565, BgL_auxz00_5567);
							}
							BgL_arg1792z00_3316 = BgL_newz00_3317;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31689ze3z87_4016,
							BgL_arg1792z00_3316);
						BgL_arg1792z00_3316;
					}
					return BgL_zc3z04anonymousza31689ze3z87_4016;
				}
			}
		}

	}



/* &<@anonymous:1689> */
	obj_t BGl_z62zc3z04anonymousza31689ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4017, obj_t BgL_xz00_4021, obj_t BgL_yz00_4022,
		obj_t BgL_za7za7_4023)
	{
		{	/* Eval/evmeaning.scm 522 */
			{	/* Eval/evmeaning.scm 524 */
				obj_t BgL_stackz00_4018;
				obj_t BgL_bodyz00_4019;
				obj_t BgL_denvz00_4020;

				BgL_stackz00_4018 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4017, (int) (0L)));
				BgL_bodyz00_4019 = PROCEDURE_REF(BgL_envz00_4017, (int) (1L));
				BgL_denvz00_4020 = ((obj_t) PROCEDURE_REF(BgL_envz00_4017, (int) (2L)));
				{	/* Eval/evmeaning.scm 524 */
					obj_t BgL_arg1691z00_4182;

					{	/* Eval/evmeaning.scm 524 */
						obj_t BgL_arg1692z00_4183;

						{	/* Eval/evmeaning.scm 524 */
							obj_t BgL_arg1699z00_4184;

							BgL_arg1699z00_4184 =
								MAKE_YOUNG_PAIR(BgL_za7za7_4023, BgL_stackz00_4018);
							BgL_arg1692z00_4183 =
								MAKE_YOUNG_PAIR(BgL_yz00_4022, BgL_arg1699z00_4184);
						}
						BgL_arg1691z00_4182 =
							MAKE_YOUNG_PAIR(BgL_xz00_4021, BgL_arg1692z00_4183);
					}
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4019,
						BgL_arg1691z00_4182, BgL_denvz00_4020);
				}
			}
		}

	}



/* evmeaning-bounce-40 */
	obj_t BGl_evmeaningzd2bouncezd240z00zz__evmeaningz00(obj_t BgL_codez00_65,
		obj_t BgL_stackz00_66, obj_t BgL_denvz00_67)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 498 */
				obj_t BgL_bodyz00_1961;
				obj_t BgL_wherez00_1962;
				obj_t BgL_locz00_1963;

				BgL_bodyz00_1961 = VECTOR_REF(BgL_codez00_65, 2L);
				BgL_wherez00_1962 = VECTOR_REF(BgL_codez00_65, 3L);
				BgL_locz00_1963 = VECTOR_REF(BgL_codez00_65, 1L);
				{	/* Eval/evmeaning.scm 503 */
					obj_t BgL_zc3z04anonymousza31701ze3z87_4024;

					BgL_zc3z04anonymousza31701ze3z87_4024 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31701ze3ze5zz__evmeaningz00, (int) (3L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31701ze3z87_4024, (int) (0L),
						BgL_wherez00_1962);
					PROCEDURE_SET(BgL_zc3z04anonymousza31701ze3z87_4024, (int) (1L),
						BgL_locz00_1963);
					PROCEDURE_SET(BgL_zc3z04anonymousza31701ze3z87_4024, (int) (2L),
						BgL_stackz00_66);
					PROCEDURE_SET(BgL_zc3z04anonymousza31701ze3z87_4024, (int) (3L),
						BgL_bodyz00_1961);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3324;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3325;

							BgL_newz00_3325 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5599;

								BgL_tmpz00_5599 = (int) (2L);
								STRUCT_SET(BgL_newz00_3325, BgL_tmpz00_5599, BgL_stackz00_66);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5602;

								BgL_tmpz00_5602 = (int) (1L);
								STRUCT_SET(BgL_newz00_3325, BgL_tmpz00_5602, BgL_bodyz00_1961);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5607;
								int BgL_tmpz00_5605;

								BgL_auxz00_5607 = BINT(3L);
								BgL_tmpz00_5605 = (int) (0L);
								STRUCT_SET(BgL_newz00_3325, BgL_tmpz00_5605, BgL_auxz00_5607);
							}
							BgL_arg1792z00_3324 = BgL_newz00_3325;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31701ze3z87_4024,
							BgL_arg1792z00_3324);
						BgL_arg1792z00_3324;
					}
					return BgL_zc3z04anonymousza31701ze3z87_4024;
				}
			}
		}

	}



/* &<@anonymous:1701> */
	obj_t BGl_z62zc3z04anonymousza31701ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4025, obj_t BgL_xz00_4030, obj_t BgL_yz00_4031,
		obj_t BgL_za7za7_4032)
	{
		{	/* Eval/evmeaning.scm 502 */
			{	/* Eval/evmeaning.scm 503 */
				obj_t BgL_wherez00_4026;
				obj_t BgL_locz00_4027;
				obj_t BgL_stackz00_4028;
				obj_t BgL_bodyz00_4029;

				BgL_wherez00_4026 = PROCEDURE_REF(BgL_envz00_4025, (int) (0L));
				BgL_locz00_4027 = PROCEDURE_REF(BgL_envz00_4025, (int) (1L));
				BgL_stackz00_4028 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4025, (int) (2L)));
				BgL_bodyz00_4029 = PROCEDURE_REF(BgL_envz00_4025, (int) (3L));
				{	/* Eval/evmeaning.scm 503 */
					obj_t BgL_z12denvz12_4185;

					BgL_z12denvz12_4185 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 506 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4185, BgL_wherez00_4026,
							BgL_locz00_4027);
						{	/* Eval/evmeaning.scm 508 */
							obj_t BgL_resz00_4186;

							{	/* Eval/evmeaning.scm 511 */
								obj_t BgL_arg1702z00_4187;

								{	/* Eval/evmeaning.scm 511 */
									obj_t BgL_arg1703z00_4188;

									{	/* Eval/evmeaning.scm 511 */
										obj_t BgL_arg1704z00_4189;

										BgL_arg1704z00_4189 =
											MAKE_YOUNG_PAIR(BgL_za7za7_4032, BgL_stackz00_4028);
										BgL_arg1703z00_4188 =
											MAKE_YOUNG_PAIR(BgL_yz00_4031, BgL_arg1704z00_4189);
									}
									BgL_arg1702z00_4187 =
										MAKE_YOUNG_PAIR(BgL_xz00_4030, BgL_arg1703z00_4188);
								}
								BgL_resz00_4186 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4029,
									BgL_arg1702z00_4187, BgL_z12denvz12_4185);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4185);
							return BgL_resz00_4186;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-44 */
	obj_t BGl_evmeaningzd2bouncezd244z00zz__evmeaningz00(obj_t BgL_codez00_68,
		obj_t BgL_stackz00_69, obj_t BgL_denvz00_70)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 488 */
				obj_t BgL_bodyz00_1977;

				BgL_bodyz00_1977 = VECTOR_REF(BgL_codez00_68, 2L);
				{	/* Eval/evmeaning.scm 491 */
					obj_t BgL_zc3z04anonymousza31706ze3z87_4033;

					BgL_zc3z04anonymousza31706ze3z87_4033 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31706ze3ze5zz__evmeaningz00, (int) (2L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31706ze3z87_4033, (int) (0L),
						BgL_stackz00_69);
					PROCEDURE_SET(BgL_zc3z04anonymousza31706ze3z87_4033, (int) (1L),
						BgL_bodyz00_1977);
					PROCEDURE_SET(BgL_zc3z04anonymousza31706ze3z87_4033, (int) (2L),
						BgL_denvz00_70);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3330;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3331;

							BgL_newz00_3331 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5639;

								BgL_tmpz00_5639 = (int) (2L);
								STRUCT_SET(BgL_newz00_3331, BgL_tmpz00_5639, BgL_stackz00_69);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5642;

								BgL_tmpz00_5642 = (int) (1L);
								STRUCT_SET(BgL_newz00_3331, BgL_tmpz00_5642, BgL_bodyz00_1977);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5647;
								int BgL_tmpz00_5645;

								BgL_auxz00_5647 = BINT(2L);
								BgL_tmpz00_5645 = (int) (0L);
								STRUCT_SET(BgL_newz00_3331, BgL_tmpz00_5645, BgL_auxz00_5647);
							}
							BgL_arg1792z00_3330 = BgL_newz00_3331;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31706ze3z87_4033,
							BgL_arg1792z00_3330);
						BgL_arg1792z00_3330;
					}
					return BgL_zc3z04anonymousza31706ze3z87_4033;
				}
			}
		}

	}



/* &<@anonymous:1706> */
	obj_t BGl_z62zc3z04anonymousza31706ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4034, obj_t BgL_xz00_4038, obj_t BgL_yz00_4039)
	{
		{	/* Eval/evmeaning.scm 490 */
			{	/* Eval/evmeaning.scm 492 */
				obj_t BgL_stackz00_4035;
				obj_t BgL_bodyz00_4036;
				obj_t BgL_denvz00_4037;

				BgL_stackz00_4035 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4034, (int) (0L)));
				BgL_bodyz00_4036 = PROCEDURE_REF(BgL_envz00_4034, (int) (1L));
				BgL_denvz00_4037 = ((obj_t) PROCEDURE_REF(BgL_envz00_4034, (int) (2L)));
				{	/* Eval/evmeaning.scm 492 */
					obj_t BgL_arg1707z00_4190;

					{	/* Eval/evmeaning.scm 492 */
						obj_t BgL_arg1708z00_4191;

						BgL_arg1708z00_4191 =
							MAKE_YOUNG_PAIR(BgL_yz00_4039, BgL_stackz00_4035);
						BgL_arg1707z00_4190 =
							MAKE_YOUNG_PAIR(BgL_xz00_4038, BgL_arg1708z00_4191);
					}
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4036,
						BgL_arg1707z00_4190, BgL_denvz00_4037);
				}
			}
		}

	}



/* evmeaning-bounce-39 */
	obj_t BGl_evmeaningzd2bouncezd239z00zz__evmeaningz00(obj_t BgL_codez00_71,
		obj_t BgL_stackz00_72, obj_t BgL_denvz00_73)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 470 */
				obj_t BgL_bodyz00_1986;
				obj_t BgL_wherez00_1987;
				obj_t BgL_locz00_1988;

				BgL_bodyz00_1986 = VECTOR_REF(BgL_codez00_71, 2L);
				BgL_wherez00_1987 = VECTOR_REF(BgL_codez00_71, 3L);
				BgL_locz00_1988 = VECTOR_REF(BgL_codez00_71, 1L);
				{	/* Eval/evmeaning.scm 475 */
					obj_t BgL_zc3z04anonymousza31710ze3z87_4040;

					BgL_zc3z04anonymousza31710ze3z87_4040 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31710ze3ze5zz__evmeaningz00, (int) (2L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31710ze3z87_4040, (int) (0L),
						BgL_wherez00_1987);
					PROCEDURE_SET(BgL_zc3z04anonymousza31710ze3z87_4040, (int) (1L),
						BgL_locz00_1988);
					PROCEDURE_SET(BgL_zc3z04anonymousza31710ze3z87_4040, (int) (2L),
						BgL_stackz00_72);
					PROCEDURE_SET(BgL_zc3z04anonymousza31710ze3z87_4040, (int) (3L),
						BgL_bodyz00_1986);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3338;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3339;

							BgL_newz00_3339 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5678;

								BgL_tmpz00_5678 = (int) (2L);
								STRUCT_SET(BgL_newz00_3339, BgL_tmpz00_5678, BgL_stackz00_72);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5681;

								BgL_tmpz00_5681 = (int) (1L);
								STRUCT_SET(BgL_newz00_3339, BgL_tmpz00_5681, BgL_bodyz00_1986);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5686;
								int BgL_tmpz00_5684;

								BgL_auxz00_5686 = BINT(2L);
								BgL_tmpz00_5684 = (int) (0L);
								STRUCT_SET(BgL_newz00_3339, BgL_tmpz00_5684, BgL_auxz00_5686);
							}
							BgL_arg1792z00_3338 = BgL_newz00_3339;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31710ze3z87_4040,
							BgL_arg1792z00_3338);
						BgL_arg1792z00_3338;
					}
					return BgL_zc3z04anonymousza31710ze3z87_4040;
				}
			}
		}

	}



/* &<@anonymous:1710> */
	obj_t BGl_z62zc3z04anonymousza31710ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4041, obj_t BgL_xz00_4046, obj_t BgL_yz00_4047)
	{
		{	/* Eval/evmeaning.scm 474 */
			{	/* Eval/evmeaning.scm 475 */
				obj_t BgL_wherez00_4042;
				obj_t BgL_locz00_4043;
				obj_t BgL_stackz00_4044;
				obj_t BgL_bodyz00_4045;

				BgL_wherez00_4042 = PROCEDURE_REF(BgL_envz00_4041, (int) (0L));
				BgL_locz00_4043 = PROCEDURE_REF(BgL_envz00_4041, (int) (1L));
				BgL_stackz00_4044 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4041, (int) (2L)));
				BgL_bodyz00_4045 = PROCEDURE_REF(BgL_envz00_4041, (int) (3L));
				{	/* Eval/evmeaning.scm 475 */
					obj_t BgL_z12denvz12_4192;

					BgL_z12denvz12_4192 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 478 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4192, BgL_wherez00_4042,
							BgL_locz00_4043);
						{	/* Eval/evmeaning.scm 480 */
							obj_t BgL_rz00_4193;

							{	/* Eval/evmeaning.scm 480 */
								obj_t BgL_arg1711z00_4194;

								{	/* Eval/evmeaning.scm 480 */
									obj_t BgL_arg1714z00_4195;

									BgL_arg1714z00_4195 =
										MAKE_YOUNG_PAIR(BgL_yz00_4047, BgL_stackz00_4044);
									BgL_arg1711z00_4194 =
										MAKE_YOUNG_PAIR(BgL_xz00_4046, BgL_arg1714z00_4195);
								}
								BgL_rz00_4193 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4045,
									BgL_arg1711z00_4194, BgL_z12denvz12_4192);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4192);
							return BgL_rz00_4193;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-43 */
	obj_t BGl_evmeaningzd2bouncezd243z00zz__evmeaningz00(obj_t BgL_codez00_74,
		obj_t BgL_stackz00_75, obj_t BgL_denvz00_76)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 460 */
				obj_t BgL_bodyz00_2000;

				BgL_bodyz00_2000 = VECTOR_REF(BgL_codez00_74, 2L);
				{	/* Eval/evmeaning.scm 463 */
					obj_t BgL_zc3z04anonymousza31716ze3z87_4048;

					BgL_zc3z04anonymousza31716ze3z87_4048 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31716ze3ze5zz__evmeaningz00, (int) (1L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31716ze3z87_4048, (int) (0L),
						BgL_stackz00_75);
					PROCEDURE_SET(BgL_zc3z04anonymousza31716ze3z87_4048, (int) (1L),
						BgL_bodyz00_2000);
					PROCEDURE_SET(BgL_zc3z04anonymousza31716ze3z87_4048, (int) (2L),
						BgL_denvz00_76);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3344;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3345;

							BgL_newz00_3345 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5717;

								BgL_tmpz00_5717 = (int) (2L);
								STRUCT_SET(BgL_newz00_3345, BgL_tmpz00_5717, BgL_stackz00_75);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5720;

								BgL_tmpz00_5720 = (int) (1L);
								STRUCT_SET(BgL_newz00_3345, BgL_tmpz00_5720, BgL_bodyz00_2000);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5725;
								int BgL_tmpz00_5723;

								BgL_auxz00_5725 = BINT(1L);
								BgL_tmpz00_5723 = (int) (0L);
								STRUCT_SET(BgL_newz00_3345, BgL_tmpz00_5723, BgL_auxz00_5725);
							}
							BgL_arg1792z00_3344 = BgL_newz00_3345;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31716ze3z87_4048,
							BgL_arg1792z00_3344);
						BgL_arg1792z00_3344;
					}
					return BgL_zc3z04anonymousza31716ze3z87_4048;
				}
			}
		}

	}



/* &<@anonymous:1716> */
	obj_t BGl_z62zc3z04anonymousza31716ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4049, obj_t BgL_xz00_4053)
	{
		{	/* Eval/evmeaning.scm 462 */
			{	/* Eval/evmeaning.scm 464 */
				obj_t BgL_stackz00_4050;
				obj_t BgL_bodyz00_4051;
				obj_t BgL_denvz00_4052;

				BgL_stackz00_4050 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4049, (int) (0L)));
				BgL_bodyz00_4051 = PROCEDURE_REF(BgL_envz00_4049, (int) (1L));
				BgL_denvz00_4052 = ((obj_t) PROCEDURE_REF(BgL_envz00_4049, (int) (2L)));
				{	/* Eval/evmeaning.scm 464 */
					obj_t BgL_arg1717z00_4196;

					BgL_arg1717z00_4196 =
						MAKE_YOUNG_PAIR(BgL_xz00_4053, BgL_stackz00_4050);
					return
						BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4051,
						BgL_arg1717z00_4196, BgL_denvz00_4052);
				}
			}
		}

	}



/* evmeaning-bounce-38 */
	obj_t BGl_evmeaningzd2bouncezd238z00zz__evmeaningz00(obj_t BgL_codez00_77,
		obj_t BgL_stackz00_78, obj_t BgL_denvz00_79)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 442 */
				obj_t BgL_bodyz00_2007;
				obj_t BgL_wherez00_2008;
				obj_t BgL_locz00_2009;

				BgL_bodyz00_2007 = VECTOR_REF(BgL_codez00_77, 2L);
				BgL_wherez00_2008 = VECTOR_REF(BgL_codez00_77, 3L);
				BgL_locz00_2009 = VECTOR_REF(BgL_codez00_77, 1L);
				{	/* Eval/evmeaning.scm 447 */
					obj_t BgL_zc3z04anonymousza31719ze3z87_4054;

					BgL_zc3z04anonymousza31719ze3z87_4054 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31719ze3ze5zz__evmeaningz00, (int) (1L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31719ze3z87_4054, (int) (0L),
						BgL_wherez00_2008);
					PROCEDURE_SET(BgL_zc3z04anonymousza31719ze3z87_4054, (int) (1L),
						BgL_locz00_2009);
					PROCEDURE_SET(BgL_zc3z04anonymousza31719ze3z87_4054, (int) (2L),
						BgL_stackz00_78);
					PROCEDURE_SET(BgL_zc3z04anonymousza31719ze3z87_4054, (int) (3L),
						BgL_bodyz00_2007);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3352;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3353;

							BgL_newz00_3353 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5755;

								BgL_tmpz00_5755 = (int) (2L);
								STRUCT_SET(BgL_newz00_3353, BgL_tmpz00_5755, BgL_stackz00_78);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5758;

								BgL_tmpz00_5758 = (int) (1L);
								STRUCT_SET(BgL_newz00_3353, BgL_tmpz00_5758, BgL_bodyz00_2007);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5763;
								int BgL_tmpz00_5761;

								BgL_auxz00_5763 = BINT(1L);
								BgL_tmpz00_5761 = (int) (0L);
								STRUCT_SET(BgL_newz00_3353, BgL_tmpz00_5761, BgL_auxz00_5763);
							}
							BgL_arg1792z00_3352 = BgL_newz00_3353;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31719ze3z87_4054,
							BgL_arg1792z00_3352);
						BgL_arg1792z00_3352;
					}
					return BgL_zc3z04anonymousza31719ze3z87_4054;
				}
			}
		}

	}



/* &<@anonymous:1719> */
	obj_t BGl_z62zc3z04anonymousza31719ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4055, obj_t BgL_xz00_4060)
	{
		{	/* Eval/evmeaning.scm 446 */
			{	/* Eval/evmeaning.scm 447 */
				obj_t BgL_wherez00_4056;
				obj_t BgL_locz00_4057;
				obj_t BgL_stackz00_4058;
				obj_t BgL_bodyz00_4059;

				BgL_wherez00_4056 = PROCEDURE_REF(BgL_envz00_4055, (int) (0L));
				BgL_locz00_4057 = PROCEDURE_REF(BgL_envz00_4055, (int) (1L));
				BgL_stackz00_4058 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4055, (int) (2L)));
				BgL_bodyz00_4059 = PROCEDURE_REF(BgL_envz00_4055, (int) (3L));
				{	/* Eval/evmeaning.scm 447 */
					obj_t BgL_z12denvz12_4197;

					BgL_z12denvz12_4197 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 450 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4197, BgL_wherez00_4056,
							BgL_locz00_4057);
						{	/* Eval/evmeaning.scm 452 */
							obj_t BgL_resz00_4198;

							{	/* Eval/evmeaning.scm 452 */
								obj_t BgL_arg1720z00_4199;

								BgL_arg1720z00_4199 =
									MAKE_YOUNG_PAIR(BgL_xz00_4060, BgL_stackz00_4058);
								BgL_resz00_4198 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4059,
									BgL_arg1720z00_4199, BgL_z12denvz12_4197);
							}
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4197);
							return BgL_resz00_4198;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-42 */
	obj_t BGl_evmeaningzd2bouncezd242z00zz__evmeaningz00(obj_t BgL_codez00_80,
		obj_t BgL_stackz00_81, obj_t BgL_denvz00_82)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 432 */
				obj_t BgL_bodyz00_2019;

				BgL_bodyz00_2019 = VECTOR_REF(BgL_codez00_80, 2L);
				{	/* Eval/evmeaning.scm 435 */
					obj_t BgL_zc3z04anonymousza31723ze3z87_4061;

					BgL_zc3z04anonymousza31723ze3z87_4061 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31723ze3ze5zz__evmeaningz00, (int) (0L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4061, (int) (0L),
						BgL_bodyz00_2019);
					PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4061, (int) (1L),
						BgL_stackz00_81);
					PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4061, (int) (2L),
						BgL_denvz00_82);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3358;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3359;

							BgL_newz00_3359 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5793;

								BgL_tmpz00_5793 = (int) (2L);
								STRUCT_SET(BgL_newz00_3359, BgL_tmpz00_5793, BgL_stackz00_81);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5796;

								BgL_tmpz00_5796 = (int) (1L);
								STRUCT_SET(BgL_newz00_3359, BgL_tmpz00_5796, BgL_bodyz00_2019);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5801;
								int BgL_tmpz00_5799;

								BgL_auxz00_5801 = BINT(0L);
								BgL_tmpz00_5799 = (int) (0L);
								STRUCT_SET(BgL_newz00_3359, BgL_tmpz00_5799, BgL_auxz00_5801);
							}
							BgL_arg1792z00_3358 = BgL_newz00_3359;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31723ze3z87_4061,
							BgL_arg1792z00_3358);
						BgL_arg1792z00_3358;
					}
					return BgL_zc3z04anonymousza31723ze3z87_4061;
				}
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4062)
	{
		{	/* Eval/evmeaning.scm 434 */
			{	/* Eval/evmeaning.scm 436 */
				obj_t BgL_bodyz00_4063;
				obj_t BgL_stackz00_4064;
				obj_t BgL_denvz00_4065;

				BgL_bodyz00_4063 = PROCEDURE_REF(BgL_envz00_4062, (int) (0L));
				BgL_stackz00_4064 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4062, (int) (1L)));
				BgL_denvz00_4065 = ((obj_t) PROCEDURE_REF(BgL_envz00_4062, (int) (2L)));
				return
					BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4063, BgL_stackz00_4064,
					BgL_denvz00_4065);
			}
		}

	}



/* evmeaning-bounce-37 */
	obj_t BGl_evmeaningzd2bouncezd237z00zz__evmeaningz00(obj_t BgL_codez00_83,
		obj_t BgL_stackz00_84, obj_t BgL_denvz00_85)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 413 */
				obj_t BgL_bodyz00_2024;
				obj_t BgL_wherez00_2025;
				obj_t BgL_locz00_2026;

				BgL_bodyz00_2024 = VECTOR_REF(BgL_codez00_83, 2L);
				BgL_wherez00_2025 = VECTOR_REF(BgL_codez00_83, 3L);
				BgL_locz00_2026 = VECTOR_REF(BgL_codez00_83, 1L);
				{	/* Eval/evmeaning.scm 418 */
					obj_t BgL_zc3z04anonymousza31725ze3z87_4066;

					BgL_zc3z04anonymousza31725ze3z87_4066 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31725ze3ze5zz__evmeaningz00, (int) (0L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31725ze3z87_4066, (int) (0L),
						BgL_wherez00_2025);
					PROCEDURE_SET(BgL_zc3z04anonymousza31725ze3z87_4066, (int) (1L),
						BgL_locz00_2026);
					PROCEDURE_SET(BgL_zc3z04anonymousza31725ze3z87_4066, (int) (2L),
						BgL_bodyz00_2024);
					PROCEDURE_SET(BgL_zc3z04anonymousza31725ze3z87_4066, (int) (3L),
						BgL_stackz00_84);
					{	/* Eval/evmeaning.scm 939 */
						obj_t BgL_arg1792z00_3366;

						{	/* Eval/evmeaning.scm 927 */
							obj_t BgL_newz00_3367;

							BgL_newz00_3367 =
								create_struct(BGl_symbol2321z00zz__evmeaningz00, (int) (3L));
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5830;

								BgL_tmpz00_5830 = (int) (2L);
								STRUCT_SET(BgL_newz00_3367, BgL_tmpz00_5830, BgL_stackz00_84);
							}
							{	/* Eval/evmeaning.scm 927 */
								int BgL_tmpz00_5833;

								BgL_tmpz00_5833 = (int) (1L);
								STRUCT_SET(BgL_newz00_3367, BgL_tmpz00_5833, BgL_bodyz00_2024);
							}
							{	/* Eval/evmeaning.scm 927 */
								obj_t BgL_auxz00_5838;
								int BgL_tmpz00_5836;

								BgL_auxz00_5838 = BINT(0L);
								BgL_tmpz00_5836 = (int) (0L);
								STRUCT_SET(BgL_newz00_3367, BgL_tmpz00_5836, BgL_auxz00_5838);
							}
							BgL_arg1792z00_3366 = BgL_newz00_3367;
						}
						PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31725ze3z87_4066,
							BgL_arg1792z00_3366);
						BgL_arg1792z00_3366;
					}
					return BgL_zc3z04anonymousza31725ze3z87_4066;
				}
			}
		}

	}



/* &<@anonymous:1725> */
	obj_t BGl_z62zc3z04anonymousza31725ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4067)
	{
		{	/* Eval/evmeaning.scm 417 */
			{	/* Eval/evmeaning.scm 418 */
				obj_t BgL_wherez00_4068;
				obj_t BgL_locz00_4069;
				obj_t BgL_bodyz00_4070;
				obj_t BgL_stackz00_4071;

				BgL_wherez00_4068 = PROCEDURE_REF(BgL_envz00_4067, (int) (0L));
				BgL_locz00_4069 = PROCEDURE_REF(BgL_envz00_4067, (int) (1L));
				BgL_bodyz00_4070 = PROCEDURE_REF(BgL_envz00_4067, (int) (2L));
				BgL_stackz00_4071 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4067, (int) (3L)));
				{	/* Eval/evmeaning.scm 418 */
					obj_t BgL_z12denvz12_4200;

					BgL_z12denvz12_4200 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 421 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4200, BgL_wherez00_4068,
							BgL_locz00_4069);
						{	/* Eval/evmeaning.scm 423 */
							obj_t BgL_resz00_4201;

							BgL_resz00_4201 =
								BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4070,
								BgL_stackz00_4071, BgL_z12denvz12_4200);
							BGL_ENV_POP_TRACE(BgL_z12denvz12_4200);
							return BgL_resz00_4201;
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-36 */
	obj_t BGl_evmeaningzd2bouncezd236z00zz__evmeaningz00(obj_t BgL_codez00_86,
		obj_t BgL_stackz00_87, obj_t BgL_denvz00_88)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 394 */
				obj_t BgL_namez00_2034;

				BgL_namez00_2034 = VECTOR_REF(BgL_codez00_86, 2L);
				{	/* Eval/evmeaning.scm 394 */
					obj_t BgL_funz00_2035;

					BgL_funz00_2035 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_86, 3L),
						BgL_stackz00_87, BgL_denvz00_88);
					{	/* Eval/evmeaning.scm 395 */

						{	/* Eval/evmeaning.scm 396 */
							obj_t BgL_g1108z00_2036;

							BgL_g1108z00_2036 = VECTOR_REF(BgL_codez00_86, 4L);
							{
								obj_t BgL_argsz00_2039;
								obj_t BgL_newz00_2040;
								long BgL_lenz00_2041;

								BgL_argsz00_2039 = BgL_g1108z00_2036;
								BgL_newz00_2040 = BNIL;
								BgL_lenz00_2041 = 0L;
							BgL_zc3z04anonymousza31726ze3z87_2042:
								if (NULLP(BgL_argsz00_2039))
									{	/* Eval/evmeaning.scm 399 */
										{	/* Eval/evmeaning.scm 401 */
											obj_t BgL_tmpz00_5861;

											BgL_tmpz00_5861 = VECTOR_REF(BgL_codez00_86, 1L);
											BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_88,
												BgL_tmpz00_5861);
										}
										{	/* Eval/evmeaning.scm 402 */
											obj_t BgL_arg1729z00_2045;

											BgL_arg1729z00_2045 = bgl_reverse_bang(BgL_newz00_2040);
											if (PROCEDUREP(BgL_funz00_2035))
												{	/* Eval/evmeaning.scm 1188 */
													if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_2035,
															(int) (BgL_lenz00_2041)))
														{	/* Eval/evmeaning.scm 1190 */
															return
																eval_apply(BgL_funz00_2035,
																BgL_arg1729z00_2045);
														}
													else
														{	/* Eval/evmeaning.scm 1190 */
															return
																BGl_evarityzd2errorzd2zz__everrorz00(VECTOR_REF
																(BgL_codez00_86, 1L), BgL_namez00_2034,
																(int) (BgL_lenz00_2041),
																PROCEDURE_ARITY(BgL_funz00_2035));
												}}
											else
												{	/* Eval/evmeaning.scm 1188 */
													return
														BGl_everrorz00zz__everrorz00(VECTOR_REF
														(BgL_codez00_86, 1L),
														BGl_string2323z00zz__evmeaningz00,
														BGl_string2324z00zz__evmeaningz00,
														BgL_namez00_2034);
												}
										}
									}
								else
									{	/* Eval/evmeaning.scm 403 */
										obj_t BgL_arg1730z00_2046;
										obj_t BgL_arg1731z00_2047;
										long BgL_arg1733z00_2048;

										BgL_arg1730z00_2046 = CDR(((obj_t) BgL_argsz00_2039));
										{	/* Eval/evmeaning.scm 404 */
											obj_t BgL_arg1734z00_2049;

											{	/* Eval/evmeaning.scm 404 */
												obj_t BgL_arg1735z00_2050;

												BgL_arg1735z00_2050 = CAR(((obj_t) BgL_argsz00_2039));
												BgL_arg1734z00_2049 =
													BGl_evmeaningz00zz__evmeaningz00(BgL_arg1735z00_2050,
													BgL_stackz00_87, BgL_denvz00_88);
											}
											BgL_arg1731z00_2047 =
												MAKE_YOUNG_PAIR(BgL_arg1734z00_2049, BgL_newz00_2040);
										}
										BgL_arg1733z00_2048 = (1L + BgL_lenz00_2041);
										{
											long BgL_lenz00_5886;
											obj_t BgL_newz00_5885;
											obj_t BgL_argsz00_5884;

											BgL_argsz00_5884 = BgL_arg1730z00_2046;
											BgL_newz00_5885 = BgL_arg1731z00_2047;
											BgL_lenz00_5886 = BgL_arg1733z00_2048;
											BgL_lenz00_2041 = BgL_lenz00_5886;
											BgL_newz00_2040 = BgL_newz00_5885;
											BgL_argsz00_2039 = BgL_argsz00_5884;
											goto BgL_zc3z04anonymousza31726ze3z87_2042;
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-30 */
	obj_t BGl_evmeaningzd2bouncezd230z00zz__evmeaningz00(obj_t BgL_codez00_89,
		obj_t BgL_stackz00_90, obj_t BgL_denvz00_91)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 368 */
				obj_t BgL_eargsz00_2053;

				{	/* Eval/evmeaning.scm 368 */
					obj_t BgL_l1179z00_2054;

					BgL_l1179z00_2054 = VECTOR_REF(BgL_codez00_89, 3L);
					if (NULLP(BgL_l1179z00_2054))
						{	/* Eval/evmeaning.scm 368 */
							BgL_eargsz00_2053 = BNIL;
						}
					else
						{	/* Eval/evmeaning.scm 368 */
							obj_t BgL_head1181z00_2056;

							BgL_head1181z00_2056 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1179z00_2058;
								obj_t BgL_tail1182z00_2059;

								BgL_l1179z00_2058 = BgL_l1179z00_2054;
								BgL_tail1182z00_2059 = BgL_head1181z00_2056;
							BgL_zc3z04anonymousza31738ze3z87_2060:
								if (NULLP(BgL_l1179z00_2058))
									{	/* Eval/evmeaning.scm 368 */
										BgL_eargsz00_2053 = CDR(BgL_head1181z00_2056);
									}
								else
									{	/* Eval/evmeaning.scm 368 */
										obj_t BgL_newtail1183z00_2062;

										{	/* Eval/evmeaning.scm 368 */
											obj_t BgL_arg1741z00_2064;

											{	/* Eval/evmeaning.scm 368 */
												obj_t BgL_xz00_2065;

												BgL_xz00_2065 = CAR(((obj_t) BgL_l1179z00_2058));
												BgL_arg1741z00_2064 =
													BGl_evmeaningz00zz__evmeaningz00(BgL_xz00_2065,
													BgL_stackz00_90, BgL_denvz00_91);
											}
											BgL_newtail1183z00_2062 =
												MAKE_YOUNG_PAIR(BgL_arg1741z00_2064, BNIL);
										}
										SET_CDR(BgL_tail1182z00_2059, BgL_newtail1183z00_2062);
										{	/* Eval/evmeaning.scm 368 */
											obj_t BgL_arg1740z00_2063;

											BgL_arg1740z00_2063 = CDR(((obj_t) BgL_l1179z00_2058));
											{
												obj_t BgL_tail1182z00_5902;
												obj_t BgL_l1179z00_5901;

												BgL_l1179z00_5901 = BgL_arg1740z00_2063;
												BgL_tail1182z00_5902 = BgL_newtail1183z00_2062;
												BgL_tail1182z00_2059 = BgL_tail1182z00_5902;
												BgL_l1179z00_2058 = BgL_l1179z00_5901;
												goto BgL_zc3z04anonymousza31738ze3z87_2060;
											}
										}
									}
							}
						}
				}
				{	/* Eval/evmeaning.scm 371 */
					obj_t BgL_funz00_4138;

					BgL_funz00_4138 = VECTOR_REF(BgL_codez00_89, 2L);
					return apply(BgL_funz00_4138, BgL_eargsz00_2053);
				}
			}
		}

	}



/* evmeaning-bounce-29 */
	obj_t BGl_evmeaningzd2bouncezd229z00zz__evmeaningz00(obj_t BgL_codez00_92,
		obj_t BgL_stackz00_93, obj_t BgL_denvz00_94)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 360 */
				obj_t BgL_funz00_2067;

				BgL_funz00_2067 = VECTOR_REF(BgL_codez00_92, 2L);
				{	/* Eval/evmeaning.scm 360 */
					obj_t BgL_a0z00_2068;

					BgL_a0z00_2068 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_92, 3L),
						BgL_stackz00_93, BgL_denvz00_94);
					{	/* Eval/evmeaning.scm 361 */
						obj_t BgL_a1z00_2069;

						BgL_a1z00_2069 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_92, 4L),
							BgL_stackz00_93, BgL_denvz00_94);
						{	/* Eval/evmeaning.scm 362 */
							obj_t BgL_a2z00_2070;

							BgL_a2z00_2070 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_92, 5L),
								BgL_stackz00_93, BgL_denvz00_94);
							{	/* Eval/evmeaning.scm 363 */
								obj_t BgL_a3z00_2071;

								BgL_a3z00_2071 =
									BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_92,
										6L), BgL_stackz00_93, BgL_denvz00_94);
								{	/* Eval/evmeaning.scm 364 */

									return
										BGL_PROCEDURE_CALL4(BgL_funz00_2067, BgL_a0z00_2068,
										BgL_a1z00_2069, BgL_a2z00_2070, BgL_a3z00_2071);
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-28 */
	obj_t BGl_evmeaningzd2bouncezd228z00zz__evmeaningz00(obj_t BgL_codez00_95,
		obj_t BgL_stackz00_96, obj_t BgL_denvz00_97)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 353 */
				obj_t BgL_funz00_2076;

				BgL_funz00_2076 = VECTOR_REF(BgL_codez00_95, 2L);
				{	/* Eval/evmeaning.scm 353 */
					obj_t BgL_a0z00_2077;

					BgL_a0z00_2077 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_95, 3L),
						BgL_stackz00_96, BgL_denvz00_97);
					{	/* Eval/evmeaning.scm 354 */
						obj_t BgL_a1z00_2078;

						BgL_a1z00_2078 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_95, 4L),
							BgL_stackz00_96, BgL_denvz00_97);
						{	/* Eval/evmeaning.scm 355 */
							obj_t BgL_a2z00_2079;

							BgL_a2z00_2079 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_95, 5L),
								BgL_stackz00_96, BgL_denvz00_97);
							{	/* Eval/evmeaning.scm 356 */

								return
									BGL_PROCEDURE_CALL3(BgL_funz00_2076, BgL_a0z00_2077,
									BgL_a1z00_2078, BgL_a2z00_2079);
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-27 */
	obj_t BGl_evmeaningzd2bouncezd227z00zz__evmeaningz00(obj_t BgL_codez00_98,
		obj_t BgL_stackz00_99, obj_t BgL_denvz00_100)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 347 */
				obj_t BgL_funz00_2083;

				BgL_funz00_2083 = VECTOR_REF(BgL_codez00_98, 2L);
				{	/* Eval/evmeaning.scm 347 */
					obj_t BgL_a0z00_2084;

					BgL_a0z00_2084 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_98, 3L),
						BgL_stackz00_99, BgL_denvz00_100);
					{	/* Eval/evmeaning.scm 348 */
						obj_t BgL_a1z00_2085;

						BgL_a1z00_2085 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_98, 4L),
							BgL_stackz00_99, BgL_denvz00_100);
						{	/* Eval/evmeaning.scm 349 */

							return
								BGL_PROCEDURE_CALL2(BgL_funz00_2083, BgL_a0z00_2084,
								BgL_a1z00_2085);
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-26 */
	obj_t BGl_evmeaningzd2bouncezd226z00zz__evmeaningz00(obj_t BgL_codez00_101,
		obj_t BgL_stackz00_102, obj_t BgL_denvz00_103)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 342 */
				obj_t BgL_funz00_2088;

				BgL_funz00_2088 = VECTOR_REF(BgL_codez00_101, 2L);
				{	/* Eval/evmeaning.scm 342 */
					obj_t BgL_a0z00_2089;

					BgL_a0z00_2089 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_101, 3L),
						BgL_stackz00_102, BgL_denvz00_103);
					{	/* Eval/evmeaning.scm 343 */

						return BGL_PROCEDURE_CALL1(BgL_funz00_2088, BgL_a0z00_2089);
					}
				}
			}
		}

	}



/* evmeaning-bounce-25 */
	obj_t BGl_evmeaningzd2bouncezd225z00zz__evmeaningz00(obj_t BgL_codez00_104)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 339 */
				obj_t BgL_fun1753z00_2091;

				BgL_fun1753z00_2091 = VECTOR_REF(BgL_codez00_104, 2L);
				return BGL_PROCEDURE_CALL0(BgL_fun1753z00_2091);
			}
		}

	}



/* evmeaning-bounce-18 */
	obj_t BGl_evmeaningzd2bouncezd218z00zz__evmeaningz00(obj_t BgL_codez00_105,
		obj_t BgL_stackz00_106, obj_t BgL_denvz00_107)
	{
		{	/* Eval/evmeaning.scm 922 */
			return
				BGl_zc3z04exitza31754ze3ze70z60zz__evmeaningz00(BgL_denvz00_107,
				BgL_stackz00_106, BgL_codez00_105);
		}

	}



/* <@exit:1754>~0 */
	obj_t BGl_zc3z04exitza31754ze3ze70z60zz__evmeaningz00(obj_t BgL_denvz00_4122,
		obj_t BgL_stackz00_4121, obj_t BgL_codez00_4120)
	{
		{	/* Eval/evmeaning.scm 335 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1110z00_2093;

			if (SET_EXIT(BgL_an_exit1110z00_2093))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1110z00_2093 = (void *) jmpbuf;
					{	/* Eval/evmeaning.scm 335 */
						obj_t BgL_env1114z00_2094;

						BgL_env1114z00_2094 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1114z00_2094, BgL_an_exit1110z00_2093, 1L);
						{	/* Eval/evmeaning.scm 335 */
							obj_t BgL_an_exitd1111z00_2095;

							BgL_an_exitd1111z00_2095 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1114z00_2094);
							{	/* Eval/evmeaning.scm 335 */
								obj_t BgL___dummy__z00_4072;

								BgL___dummy__z00_4072 =
									MAKE_FX_PROCEDURE(BGl_z62__dummy__z62zz__evmeaningz00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL___dummy__z00_4072,
									(int) (0L), BgL_an_exitd1111z00_2095);
								{	/* Eval/evmeaning.scm 335 */
									obj_t BgL_res1113z00_2098;

									{	/* Eval/evmeaning.scm 336 */
										obj_t BgL_fun1756z00_2100;

										BgL_fun1756z00_2100 =
											BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF
											(BgL_codez00_4120, 2L), BgL_stackz00_4121,
											BgL_denvz00_4122);
										BgL_res1113z00_2098 =
											BGL_PROCEDURE_CALL1(BgL_fun1756z00_2100,
											BgL___dummy__z00_4072);
									}
									POP_ENV_EXIT(BgL_env1114z00_2094);
									return BgL_res1113z00_2098;
								}
							}
						}
					}
				}
		}

	}



/* &__dummy__ */
	obj_t BGl_z62__dummy__z62zz__evmeaningz00(obj_t BgL_envz00_4073,
		obj_t BgL_val1112z00_4075)
	{
		{	/* Eval/evmeaning.scm 335 */
			return
				unwind_stack_until(PROCEDURE_REF(BgL_envz00_4073,
					(int) (0L)), BFALSE, BgL_val1112z00_4075, BFALSE, BFALSE);
		}

	}



/* evmeaning-bounce-17 */
	obj_t BGl_evmeaningzd2bouncezd217z00zz__evmeaningz00(obj_t BgL_codez00_108,
		obj_t BgL_stackz00_109, obj_t BgL_denvz00_110)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 298 */
				obj_t BgL_varz00_2101;
				obj_t BgL_valz00_2102;
				obj_t BgL_modz00_2103;

				BgL_varz00_2101 = VECTOR_REF(BgL_codez00_108, 2L);
				BgL_valz00_2102 = VECTOR_REF(BgL_codez00_108, 3L);
				BgL_modz00_2103 = VECTOR_REF(BgL_codez00_108, 4L);
				{	/* Eval/evmeaning.scm 301 */
					obj_t BgL_vz00_2104;

					BgL_vz00_2104 =
						BGl_evmodulezd2findzd2globalz00zz__evmodulez00(BgL_modz00_2103,
						BgL_varz00_2101);
					{	/* Eval/evmeaning.scm 302 */
						bool_t BgL_test2486z00_5981;

						if (VECTORP(BgL_vz00_2104))
							{	/* Eval/evmeaning.scm 302 */
								BgL_test2486z00_5981 = (VECTOR_LENGTH(BgL_vz00_2104) == 5L);
							}
						else
							{	/* Eval/evmeaning.scm 302 */
								BgL_test2486z00_5981 = ((bool_t) 0);
							}
						if (BgL_test2486z00_5981)
							{

								{	/* Eval/evmeaning.scm 303 */
									int BgL_aux1117z00_2107;

									BgL_aux1117z00_2107 = CINT(VECTOR_REF(BgL_vz00_2104, 0L));
									switch ((long) (BgL_aux1117z00_2107))
										{
										case 0L:

											BGl_everrorz00zz__everrorz00(VECTOR_REF(BgL_codez00_108,
													1L), BGl_string2318z00zz__evmeaningz00,
												BGl_string2340z00zz__evmeaningz00, BgL_varz00_2101);
											break;
										case 1L:

											{	/* Eval/evmeaning.scm 309 */
												obj_t BgL_arg1759z00_2109;

												BgL_arg1759z00_2109 = VECTOR_REF(BgL_codez00_108, 1L);
												{	/* Eval/evmeaning.scm 309 */
													obj_t BgL_list1760z00_2110;

													{	/* Eval/evmeaning.scm 309 */
														obj_t BgL_arg1761z00_2111;

														{	/* Eval/evmeaning.scm 309 */
															obj_t BgL_arg1762z00_2112;

															BgL_arg1762z00_2112 =
																MAKE_YOUNG_PAIR(BgL_varz00_2101, BNIL);
															BgL_arg1761z00_2111 =
																MAKE_YOUNG_PAIR
																(BGl_string2341z00zz__evmeaningz00,
																BgL_arg1762z00_2112);
														}
														BgL_list1760z00_2110 =
															MAKE_YOUNG_PAIR(BGl_string2318z00zz__evmeaningz00,
															BgL_arg1761z00_2111);
													}
													BGl_evwarningz00zz__everrorz00(BgL_arg1759z00_2109,
														BgL_list1760z00_2110);
												}
											}
											BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00
												(BgL_codez00_108, BgL_vz00_2104,
												BGl_evmeaningz00zz__evmeaningz00(BgL_valz00_2102, BNIL,
													BgL_denvz00_110));
											break;
										case 2L:

											BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00
												(BgL_codez00_108, BgL_vz00_2104,
												BGl_evmeaningz00zz__evmeaningz00(BgL_valz00_2102, BNIL,
													BgL_denvz00_110));
											break;
										case 3L:

											BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00
												(BgL_codez00_108, BgL_vz00_2104,
												BGl_evmeaningz00zz__evmeaningz00(BgL_valz00_2102, BNIL,
													BgL_denvz00_110));
											VECTOR_SET(BgL_vz00_2104, 0L, BINT(2L));
											break;
										case 4L:

											BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00
												(BgL_codez00_108, BgL_vz00_2104,
												BGl_evmeaningz00zz__evmeaningz00(BgL_valz00_2102, BNIL,
													BgL_denvz00_110));
											VECTOR_SET(BgL_vz00_2104, 0L, BINT(5L));
											break;
										default:
											BGl_everrorz00zz__everrorz00(VECTOR_REF(BgL_codez00_108,
													1L), BGl_string2318z00zz__evmeaningz00,
												BGl_string2339z00zz__evmeaningz00, BgL_varz00_2101);
										}
								}
							}
						else
							{	/* Eval/evmeaning.scm 325 */
								obj_t BgL_locz00_2118;

								BgL_locz00_2118 = VECTOR_REF(BgL_codez00_108, 1L);
								{	/* Eval/evmeaning.scm 325 */
									obj_t BgL_gz00_2119;

									{	/* Eval/evmeaning.scm 326 */
										obj_t BgL_arg1768z00_2121;

										BgL_arg1768z00_2121 = BGl_evalzd2modulezd2zz__evmodulez00();
										{	/* Eval/evmeaning.scm 326 */
											obj_t BgL_v1184z00_3425;

											BgL_v1184z00_3425 = create_vector(5L);
											VECTOR_SET(BgL_v1184z00_3425, 0L, BINT(2L));
											VECTOR_SET(BgL_v1184z00_3425, 1L,
												((obj_t) BgL_varz00_2101));
											VECTOR_SET(BgL_v1184z00_3425, 2L, BUNSPEC);
											VECTOR_SET(BgL_v1184z00_3425, 3L, BgL_arg1768z00_2121);
											VECTOR_SET(BgL_v1184z00_3425, 4L, BgL_locz00_2118);
											BgL_gz00_2119 = BgL_v1184z00_3425;
										}
									}
									{	/* Eval/evmeaning.scm 326 */

										BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
											(BgL_modz00_2103, BgL_varz00_2101, BgL_gz00_2119,
											BgL_locz00_2118);
										VECTOR_SET(BgL_gz00_2119, 2L,
											BGl_evmeaningz00zz__evmeaningz00(BgL_valz00_2102, BNIL,
												BgL_denvz00_110));
									}
								}
							}
					}
					return BgL_varz00_2101;
				}
			}
		}

	}



/* evmeaning-bounce-14 */
	obj_t BGl_evmeaningzd2bouncezd214z00zz__evmeaningz00(obj_t BgL_codez00_111,
		obj_t BgL_stackz00_112, obj_t BgL_denvz00_113)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 276 */
				obj_t BgL_offsetz00_2122;
				obj_t BgL_valuez00_2123;

				BgL_offsetz00_2122 = VECTOR_REF(BgL_codez00_111, 2L);
				BgL_valuez00_2123 =
					BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_111, 3L),
					BgL_stackz00_112, BgL_denvz00_113);
				{
					long BgL_iz00_2126;
					obj_t BgL_envz00_2127;

					{	/* Eval/evmeaning.scm 279 */
						obj_t BgL_arg1769z00_2125;

						{	/* Eval/evmeaning.scm 279 */
							obj_t BgL_pairz00_3447;

							{	/* Eval/evmeaning.scm 279 */
								obj_t BgL_pairz00_3446;

								{	/* Eval/evmeaning.scm 279 */
									obj_t BgL_pairz00_3445;

									BgL_pairz00_3445 = CDR(((obj_t) BgL_stackz00_112));
									BgL_pairz00_3446 = CDR(BgL_pairz00_3445);
								}
								BgL_pairz00_3447 = CDR(BgL_pairz00_3446);
							}
							BgL_arg1769z00_2125 = CDR(BgL_pairz00_3447);
						}
						BgL_iz00_2126 = 4L;
						BgL_envz00_2127 = BgL_arg1769z00_2125;
					BgL_zc3z04anonymousza31770ze3z87_2128:
						if ((BgL_iz00_2126 == (long) CINT(BgL_offsetz00_2122)))
							{	/* Eval/evmeaning.scm 280 */
								obj_t BgL_tmpz00_6035;

								BgL_tmpz00_6035 = ((obj_t) BgL_envz00_2127);
								SET_CAR(BgL_tmpz00_6035, BgL_valuez00_2123);
							}
						else
							{	/* Eval/evmeaning.scm 278 */
								long BgL_arg1772z00_2130;
								obj_t BgL_arg1773z00_2131;

								BgL_arg1772z00_2130 = (BgL_iz00_2126 + 1L);
								BgL_arg1773z00_2131 = CDR(((obj_t) BgL_envz00_2127));
								{
									obj_t BgL_envz00_6042;
									long BgL_iz00_6041;

									BgL_iz00_6041 = BgL_arg1772z00_2130;
									BgL_envz00_6042 = BgL_arg1773z00_2131;
									BgL_envz00_2127 = BgL_envz00_6042;
									BgL_iz00_2126 = BgL_iz00_6041;
									goto BgL_zc3z04anonymousza31770ze3z87_2128;
								}
							}
					}
				}
				return BUNSPEC;
			}
		}

	}



/* evmeaning-bounce-9 */
	obj_t BGl_evmeaningzd2bouncezd29z00zz__evmeaningz00(obj_t BgL_codez00_114,
		obj_t BgL_stackz00_115, obj_t BgL_denvz00_116)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 247 */
				obj_t BgL_namez00_2134;

				BgL_namez00_2134 = VECTOR_REF(BgL_codez00_114, 2L);
				{	/* Eval/evmeaning.scm 247 */
					obj_t BgL_valuez00_2135;

					BgL_valuez00_2135 = VECTOR_REF(BgL_codez00_114, 3L);
					{	/* Eval/evmeaning.scm 248 */
						obj_t BgL_modz00_2136;

						BgL_modz00_2136 = VECTOR_REF(BgL_codez00_114, 4L);
						{	/* Eval/evmeaning.scm 249 */
							obj_t BgL_globalz00_2137;

							BgL_globalz00_2137 =
								BGl_evmodulezd2findzd2globalz00zz__evmodulez00(BgL_modz00_2136,
								BgL_namez00_2134);
							{	/* Eval/evmeaning.scm 250 */

								{	/* Eval/evmeaning.scm 251 */
									bool_t BgL_test2489z00_6047;

									if (VECTORP(BgL_globalz00_2137))
										{	/* Eval/evmeaning.scm 251 */
											BgL_test2489z00_6047 =
												(VECTOR_LENGTH(BgL_globalz00_2137) == 5L);
										}
									else
										{	/* Eval/evmeaning.scm 251 */
											BgL_test2489z00_6047 = ((bool_t) 0);
										}
									if (BgL_test2489z00_6047)
										{	/* Eval/evmeaning.scm 251 */
											VECTOR_SET(BgL_codez00_114, 0L, BINT(8L));
											VECTOR_SET(BgL_codez00_114, 2L, BgL_globalz00_2137);
											VECTOR_SET(BgL_codez00_114, 3L, BgL_valuez00_2135);
											return
												BGl_evmeaningz00zz__evmeaningz00(BgL_codez00_114,
												BgL_stackz00_115, BgL_denvz00_116);
										}
									else
										{	/* Eval/evmeaning.scm 251 */
											return
												BGl_evmeaningzd2unboundzd2zz__evmeaningz00(VECTOR_REF
												(BgL_codez00_114, 1L), BgL_namez00_2134,
												BgL_modz00_2136);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-bounce-7 */
	obj_t BGl_evmeaningzd2bouncezd27z00zz__evmeaningz00(obj_t BgL_codez00_117,
		obj_t BgL_stackz00_118, obj_t BgL_denvz00_119)
	{
		{	/* Eval/evmeaning.scm 922 */
			{	/* Eval/evmeaning.scm 228 */
				obj_t BgL_namez00_2140;

				BgL_namez00_2140 = VECTOR_REF(BgL_codez00_117, 2L);
				{	/* Eval/evmeaning.scm 228 */
					obj_t BgL_modz00_2141;

					BgL_modz00_2141 = VECTOR_REF(BgL_codez00_117, 3L);
					{	/* Eval/evmeaning.scm 229 */
						obj_t BgL_globalz00_2142;

						BgL_globalz00_2142 =
							BGl_evmodulezd2findzd2globalz00zz__evmodulez00(BgL_modz00_2141,
							BgL_namez00_2140);
						{	/* Eval/evmeaning.scm 230 */

							{	/* Eval/evmeaning.scm 231 */
								bool_t BgL_test2491z00_6062;

								if (VECTORP(BgL_globalz00_2142))
									{	/* Eval/evmeaning.scm 231 */
										BgL_test2491z00_6062 =
											(VECTOR_LENGTH(BgL_globalz00_2142) == 5L);
									}
								else
									{	/* Eval/evmeaning.scm 231 */
										BgL_test2491z00_6062 = ((bool_t) 0);
									}
								if (BgL_test2491z00_6062)
									{	/* Eval/evmeaning.scm 231 */
										VECTOR_SET(BgL_codez00_117, 0L, BINT(6L));
										VECTOR_SET(BgL_codez00_117, 2L, BgL_globalz00_2142);
										return VECTOR_REF(BgL_globalz00_2142, 2L);
									}
								else
									{	/* Eval/evmeaning.scm 231 */
										return
											BGl_evmeaningzd2unboundzd2zz__evmeaningz00(VECTOR_REF
											(BgL_codez00_117, 1L), BgL_namez00_2140, BgL_modz00_2141);
									}
							}
						}
					}
				}
			}
		}

	}



/* update-eval-global! */
	obj_t BGl_updatezd2evalzd2globalz12z12zz__evmeaningz00(obj_t BgL_codez00_142,
		obj_t BgL_varz00_143, obj_t BgL_valz00_144)
	{
		{	/* Eval/evmeaning.scm 963 */
			{	/* Eval/evmeaning.scm 964 */
				int BgL_aux1121z00_2164;

				{	/* Eval/evmeaning.scm 964 */
					int BgL_res2301z00_3509;

					BgL_res2301z00_3509 = CINT(VECTOR_REF(((obj_t) BgL_varz00_143), 0L));
					BgL_aux1121z00_2164 = BgL_res2301z00_3509;
				}
				switch ((long) (BgL_aux1121z00_2164))
					{
					case 0L:

						{	/* Eval/evmeaning.scm 966 */
							obj_t BgL_arg1796z00_2165;
							obj_t BgL_arg1797z00_2166;

							BgL_arg1796z00_2165 = VECTOR_REF(BgL_codez00_142, 1L);
							BgL_arg1797z00_2166 = VECTOR_REF(((obj_t) BgL_varz00_143), 1L);
							return
								BGl_everrorz00zz__everrorz00(BgL_arg1796z00_2165,
								BGl_string2342z00zz__evmeaningz00,
								BGl_string2343z00zz__evmeaningz00, BgL_arg1797z00_2166);
						}
						break;
					case 1L:

						{	/* Eval/evmeaning.scm 968 */
							obj_t BgL_arg1798z00_2167;

							BgL_arg1798z00_2167 = VECTOR_REF(((obj_t) BgL_varz00_143), 2L);
							return
								__EVMEANING_ADDRESS_SET(BgL_arg1798z00_2167, BgL_valz00_144);
						}
						break;
					case 2L:

						return VECTOR_SET(((obj_t) BgL_varz00_143), 2L, BgL_valz00_144);
						break;
					case 3L:

						return VECTOR_SET(((obj_t) BgL_varz00_143), 2L, BgL_valz00_144);
						break;
					case 4L:

						return VECTOR_SET(((obj_t) BgL_varz00_143), 2L, BgL_valz00_144);
						break;
					case 5L:

						{	/* Eval/evmeaning.scm 976 */
							obj_t BgL_arg1799z00_2168;
							obj_t BgL_arg1800z00_2169;

							BgL_arg1799z00_2168 = VECTOR_REF(BgL_codez00_142, 1L);
							BgL_arg1800z00_2169 = VECTOR_REF(((obj_t) BgL_varz00_143), 1L);
							return
								BGl_everrorz00zz__everrorz00(BgL_arg1799z00_2168,
								BGl_string2342z00zz__evmeaningz00,
								BGl_string2343z00zz__evmeaningz00, BgL_arg1800z00_2169);
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* evmeaning-funcall-0 */
	obj_t BGl_evmeaningzd2funcallzd20z00zz__evmeaningz00(obj_t BgL_codez00_145,
		obj_t BgL_stackz00_146, obj_t BgL_denvz00_147, obj_t BgL_funz00_148)
	{
		{	/* Eval/evmeaning.scm 981 */
			{	/* Eval/evmeaning.scm 982 */
				obj_t BgL_namez00_2170;
				obj_t BgL_locz00_2171;

				BgL_namez00_2170 = VECTOR_REF(BgL_codez00_145, 2L);
				BgL_locz00_2171 = VECTOR_REF(BgL_codez00_145, 1L);
				BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_147, BgL_locz00_2171);
				if (PROCEDUREP(BgL_funz00_148))
					{	/* Eval/evmeaning.scm 986 */
						if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_148, (int) (0L)))
							{	/* Eval/evmeaning.scm 988 */
								return eval_funcall_0(BgL_funz00_148);
							}
						else
							{	/* Eval/evmeaning.scm 988 */
								return
									BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_2171,
									BgL_namez00_2170, (int) (0L),
									PROCEDURE_ARITY(BgL_funz00_148));
					}}
				else
					{	/* Eval/evmeaning.scm 986 */
						return
							BGl_everrorz00zz__everrorz00(BgL_locz00_2171,
							BGl_string2318z00zz__evmeaningz00, BgL_namez00_2170,
							BGl_string2324z00zz__evmeaningz00);
					}
			}
		}

	}



/* evmeaning-funcall-1 */
	obj_t BGl_evmeaningzd2funcallzd21z00zz__evmeaningz00(obj_t BgL_codez00_149,
		obj_t BgL_stackz00_150, obj_t BgL_denvz00_151, obj_t BgL_funz00_152)
	{
		{	/* Eval/evmeaning.scm 996 */
			{	/* Eval/evmeaning.scm 997 */
				obj_t BgL_namez00_2174;

				BgL_namez00_2174 = VECTOR_REF(BgL_codez00_149, 2L);
				{	/* Eval/evmeaning.scm 997 */
					obj_t BgL_locz00_2175;

					BgL_locz00_2175 = VECTOR_REF(BgL_codez00_149, 1L);
					{	/* Eval/evmeaning.scm 998 */
						obj_t BgL_a0z00_2176;

						BgL_a0z00_2176 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_149, 4L),
							BgL_stackz00_150, BgL_denvz00_151);
						{	/* Eval/evmeaning.scm 999 */

							BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_151, BgL_locz00_2175);
							if (PROCEDUREP(BgL_funz00_152))
								{	/* Eval/evmeaning.scm 1002 */
									if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_152, (int) (1L)))
										{	/* Eval/evmeaning.scm 1004 */
											return eval_funcall_1(BgL_funz00_152, BgL_a0z00_2176);
										}
									else
										{	/* Eval/evmeaning.scm 1004 */
											return
												BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_2175,
												BgL_namez00_2174, (int) (1L),
												PROCEDURE_ARITY(BgL_funz00_152));
								}}
							else
								{	/* Eval/evmeaning.scm 1002 */
									return
										BGl_everrorz00zz__everrorz00(BgL_locz00_2175,
										BGl_string2318z00zz__evmeaningz00,
										BGl_string2324z00zz__evmeaningz00, BgL_namez00_2174);
								}
						}
					}
				}
			}
		}

	}



/* evmeaning-funcall-2 */
	obj_t BGl_evmeaningzd2funcallzd22z00zz__evmeaningz00(obj_t BgL_codez00_153,
		obj_t BgL_stackz00_154, obj_t BgL_denvz00_155, obj_t BgL_funz00_156)
	{
		{	/* Eval/evmeaning.scm 1012 */
			{	/* Eval/evmeaning.scm 1013 */
				obj_t BgL_namez00_2180;

				BgL_namez00_2180 = VECTOR_REF(BgL_codez00_153, 2L);
				{	/* Eval/evmeaning.scm 1013 */
					obj_t BgL_locz00_2181;

					BgL_locz00_2181 = VECTOR_REF(BgL_codez00_153, 1L);
					{	/* Eval/evmeaning.scm 1014 */
						obj_t BgL_a0z00_2182;

						BgL_a0z00_2182 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_153, 4L),
							BgL_stackz00_154, BgL_denvz00_155);
						{	/* Eval/evmeaning.scm 1015 */
							obj_t BgL_a1z00_2183;

							BgL_a1z00_2183 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_153,
									5L), BgL_stackz00_154, BgL_denvz00_155);
							{	/* Eval/evmeaning.scm 1016 */

								BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_155, BgL_locz00_2181);
								if (PROCEDUREP(BgL_funz00_156))
									{	/* Eval/evmeaning.scm 1019 */
										if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_156, (int) (2L)))
											{	/* Eval/evmeaning.scm 1021 */
												return
													eval_funcall_2(BgL_funz00_156, BgL_a0z00_2182,
													BgL_a1z00_2183);
											}
										else
											{	/* Eval/evmeaning.scm 1021 */
												return
													BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_2181,
													BgL_namez00_2180, (int) (2L),
													PROCEDURE_ARITY(BgL_funz00_156));
									}}
								else
									{	/* Eval/evmeaning.scm 1019 */
										return
											BGl_everrorz00zz__everrorz00(BgL_locz00_2181,
											BGl_string2318z00zz__evmeaningz00,
											BGl_string2324z00zz__evmeaningz00, BgL_namez00_2180);
									}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-funcall-3 */
	obj_t BGl_evmeaningzd2funcallzd23z00zz__evmeaningz00(obj_t BgL_codez00_157,
		obj_t BgL_stackz00_158, obj_t BgL_denvz00_159, obj_t BgL_funz00_160)
	{
		{	/* Eval/evmeaning.scm 1029 */
			{	/* Eval/evmeaning.scm 1030 */
				obj_t BgL_namez00_2188;

				BgL_namez00_2188 = VECTOR_REF(BgL_codez00_157, 2L);
				{	/* Eval/evmeaning.scm 1030 */
					obj_t BgL_locz00_2189;

					BgL_locz00_2189 = VECTOR_REF(BgL_codez00_157, 1L);
					{	/* Eval/evmeaning.scm 1031 */
						obj_t BgL_a0z00_2190;

						BgL_a0z00_2190 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_157, 4L),
							BgL_stackz00_158, BgL_denvz00_159);
						{	/* Eval/evmeaning.scm 1032 */
							obj_t BgL_a1z00_2191;

							BgL_a1z00_2191 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_157,
									5L), BgL_stackz00_158, BgL_denvz00_159);
							{	/* Eval/evmeaning.scm 1033 */
								obj_t BgL_a2z00_2192;

								BgL_a2z00_2192 =
									BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_157,
										6L), BgL_stackz00_158, BgL_denvz00_159);
								{	/* Eval/evmeaning.scm 1034 */

									BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_159, BgL_locz00_2189);
									if (PROCEDUREP(BgL_funz00_160))
										{	/* Eval/evmeaning.scm 1037 */
											if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_160, (int) (3L)))
												{	/* Eval/evmeaning.scm 1039 */
													return
														eval_funcall_3(BgL_funz00_160, BgL_a0z00_2190,
														BgL_a1z00_2191, BgL_a2z00_2192);
												}
											else
												{	/* Eval/evmeaning.scm 1039 */
													return
														BGl_evarityzd2errorzd2zz__everrorz00
														(BgL_locz00_2189, BgL_namez00_2188, (int) (3L),
														PROCEDURE_ARITY(BgL_funz00_160));
										}}
									else
										{	/* Eval/evmeaning.scm 1037 */
											return
												BGl_everrorz00zz__everrorz00(BgL_locz00_2189,
												BGl_string2318z00zz__evmeaningz00,
												BGl_string2324z00zz__evmeaningz00, BgL_namez00_2188);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-funcall-4 */
	obj_t BGl_evmeaningzd2funcallzd24z00zz__evmeaningz00(obj_t BgL_codez00_161,
		obj_t BgL_stackz00_162, obj_t BgL_denvz00_163, obj_t BgL_funz00_164)
	{
		{	/* Eval/evmeaning.scm 1047 */
			{	/* Eval/evmeaning.scm 1048 */
				obj_t BgL_namez00_2198;

				BgL_namez00_2198 = VECTOR_REF(BgL_codez00_161, 2L);
				{	/* Eval/evmeaning.scm 1048 */
					obj_t BgL_locz00_2199;

					BgL_locz00_2199 = VECTOR_REF(BgL_codez00_161, 1L);
					{	/* Eval/evmeaning.scm 1049 */
						obj_t BgL_a0z00_2200;

						BgL_a0z00_2200 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_161, 4L),
							BgL_stackz00_162, BgL_denvz00_163);
						{	/* Eval/evmeaning.scm 1050 */
							obj_t BgL_a1z00_2201;

							BgL_a1z00_2201 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_161,
									5L), BgL_stackz00_162, BgL_denvz00_163);
							{	/* Eval/evmeaning.scm 1051 */
								obj_t BgL_a2z00_2202;

								BgL_a2z00_2202 =
									BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_161,
										6L), BgL_stackz00_162, BgL_denvz00_163);
								{	/* Eval/evmeaning.scm 1052 */
									obj_t BgL_a3z00_2203;

									BgL_a3z00_2203 =
										BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_161,
											7L), BgL_stackz00_162, BgL_denvz00_163);
									{	/* Eval/evmeaning.scm 1053 */

										BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_163,
											BgL_locz00_2199);
										if (PROCEDUREP(BgL_funz00_164))
											{	/* Eval/evmeaning.scm 1056 */
												if (PROCEDURE_CORRECT_ARITYP(BgL_funz00_164,
														(int) (4L)))
													{	/* Eval/evmeaning.scm 1058 */
														return
															eval_funcall_4(BgL_funz00_164, BgL_a0z00_2200,
															BgL_a1z00_2201, BgL_a2z00_2202, BgL_a3z00_2203);
													}
												else
													{	/* Eval/evmeaning.scm 1058 */
														return
															BGl_evarityzd2errorzd2zz__everrorz00
															(BgL_locz00_2199, BgL_namez00_2198, (int) (4L),
															PROCEDURE_ARITY(BgL_funz00_164));
											}}
										else
											{	/* Eval/evmeaning.scm 1056 */
												return
													BGl_everrorz00zz__everrorz00(BgL_locz00_2199,
													BGl_string2318z00zz__evmeaningz00,
													BGl_string2324z00zz__evmeaningz00, BgL_namez00_2198);
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-tailcall-0-stack */
	obj_t BGl_evmeaningzd2tailcallzd20zd2stackzd2zz__evmeaningz00(obj_t
		BgL_codez00_165, obj_t BgL_stackz00_166, obj_t BgL_denvz00_167,
		obj_t BgL_funz00_168)
	{
		{	/* Eval/evmeaning.scm 1066 */
			{	/* Eval/evmeaning.scm 1067 */
				obj_t BgL_envdz00_2210;

				{	/* Eval/evmeaning.scm 952 */
					obj_t BgL_arg1794z00_3544;

					{	/* Eval/evmeaning.scm 952 */
						obj_t BgL_tmpz00_6180;

						BgL_tmpz00_6180 = ((obj_t) BgL_funz00_168);
						BgL_arg1794z00_3544 = PROCEDURE_ATTR(BgL_tmpz00_6180);
					}
					BgL_envdz00_2210 =
						STRUCT_REF(((obj_t) BgL_arg1794z00_3544), (int) (2L));
				}
				{	/* Eval/evmeaning.scm 1067 */
					obj_t BgL_arityz00_2211;

					{	/* Eval/evmeaning.scm 958 */
						obj_t BgL_arg1795z00_3547;

						{	/* Eval/evmeaning.scm 958 */
							obj_t BgL_tmpz00_6186;

							BgL_tmpz00_6186 = ((obj_t) BgL_funz00_168);
							BgL_arg1795z00_3547 = PROCEDURE_ATTR(BgL_tmpz00_6186);
						}
						BgL_arityz00_2211 =
							STRUCT_REF(((obj_t) BgL_arg1795z00_3547), (int) (0L));
					}
					{	/* Eval/evmeaning.scm 1068 */
						obj_t BgL_locz00_2212;

						BgL_locz00_2212 = VECTOR_REF(BgL_codez00_165, 1L);
						{	/* Eval/evmeaning.scm 1069 */

							{	/* Eval/evmeaning.scm 1070 */
								bool_t BgL_test2503z00_6193;

								{	/* Eval/evmeaning.scm 1070 */
									obj_t BgL_tmpz00_6194;

									BgL_tmpz00_6194 = VECTOR_REF(BgL_codez00_165, 2L);
									BgL_test2503z00_6193 = SYMBOLP(BgL_tmpz00_6194);
								}
								if (BgL_test2503z00_6193)
									{	/* Eval/evmeaning.scm 1070 */
										{	/* Eval/evmeaning.scm 1071 */
											obj_t BgL_tmpz00_6197;

											BgL_tmpz00_6197 = VECTOR_REF(BgL_codez00_165, 2L);
											BGL_ENV_SET_TRACE_NAME(BgL_denvz00_167, BgL_tmpz00_6197);
										}
										BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_167,
											BgL_locz00_2212);
									}
								else
									{	/* Eval/evmeaning.scm 1070 */
										BFALSE;
									}
							}
							{

								if (INTEGERP(BgL_arityz00_2211))
									{	/* Eval/evmeaning.scm 1073 */
										switch ((long) CINT(BgL_arityz00_2211))
											{
											case 0L:

												return BgL_envdz00_2210;
												break;
											case -1L:

												return MAKE_YOUNG_PAIR(BNIL, BgL_envdz00_2210);
												break;
											default:
											BgL_case_else1122z00_2217:
												return
													BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_2212,
													VECTOR_REF(BgL_codez00_165, 2L),
													(int) (0L), CINT(BgL_arityz00_2211));
									}}
								else
									{	/* Eval/evmeaning.scm 1073 */
										goto BgL_case_else1122z00_2217;
									}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-tailcall-1-stack */
	obj_t BGl_evmeaningzd2tailcallzd21zd2stackzd2zz__evmeaningz00(obj_t
		BgL_codez00_169, obj_t BgL_stackz00_170, obj_t BgL_denvz00_171,
		obj_t BgL_funz00_172)
	{
		{	/* Eval/evmeaning.scm 1084 */
			{	/* Eval/evmeaning.scm 1085 */
				obj_t BgL_a0z00_2220;

				BgL_a0z00_2220 =
					BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_169, 4L),
					BgL_stackz00_170, BgL_denvz00_171);
				{	/* Eval/evmeaning.scm 1086 */
					obj_t BgL_envdz00_2221;

					{	/* Eval/evmeaning.scm 952 */
						obj_t BgL_arg1794z00_3555;

						{	/* Eval/evmeaning.scm 952 */
							obj_t BgL_tmpz00_6212;

							BgL_tmpz00_6212 = ((obj_t) BgL_funz00_172);
							BgL_arg1794z00_3555 = PROCEDURE_ATTR(BgL_tmpz00_6212);
						}
						BgL_envdz00_2221 =
							STRUCT_REF(((obj_t) BgL_arg1794z00_3555), (int) (2L));
					}
					{	/* Eval/evmeaning.scm 1086 */
						obj_t BgL_arityz00_2222;

						{	/* Eval/evmeaning.scm 958 */
							obj_t BgL_arg1795z00_3558;

							{	/* Eval/evmeaning.scm 958 */
								obj_t BgL_tmpz00_6218;

								BgL_tmpz00_6218 = ((obj_t) BgL_funz00_172);
								BgL_arg1795z00_3558 = PROCEDURE_ATTR(BgL_tmpz00_6218);
							}
							BgL_arityz00_2222 =
								STRUCT_REF(((obj_t) BgL_arg1795z00_3558), (int) (0L));
						}
						{	/* Eval/evmeaning.scm 1087 */
							obj_t BgL_locz00_2223;

							BgL_locz00_2223 = VECTOR_REF(BgL_codez00_169, 1L);
							{	/* Eval/evmeaning.scm 1088 */

								{	/* Eval/evmeaning.scm 1089 */
									bool_t BgL_test2505z00_6225;

									{	/* Eval/evmeaning.scm 1089 */
										obj_t BgL_tmpz00_6226;

										BgL_tmpz00_6226 = VECTOR_REF(BgL_codez00_169, 2L);
										BgL_test2505z00_6225 = SYMBOLP(BgL_tmpz00_6226);
									}
									if (BgL_test2505z00_6225)
										{	/* Eval/evmeaning.scm 1089 */
											{	/* Eval/evmeaning.scm 1090 */
												obj_t BgL_tmpz00_6229;

												BgL_tmpz00_6229 = VECTOR_REF(BgL_codez00_169, 2L);
												BGL_ENV_SET_TRACE_NAME(BgL_denvz00_171,
													BgL_tmpz00_6229);
											}
											BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_171,
												BgL_locz00_2223);
										}
									else
										{	/* Eval/evmeaning.scm 1089 */
											BFALSE;
										}
								}
								{

									if (INTEGERP(BgL_arityz00_2222))
										{	/* Eval/evmeaning.scm 1092 */
											switch ((long) CINT(BgL_arityz00_2222))
												{
												case 1L:

													return
														MAKE_YOUNG_PAIR(BgL_a0z00_2220, BgL_envdz00_2221);
													break;
												case -1L:

													{	/* Eval/evmeaning.scm 1096 */
														obj_t BgL_arg1833z00_2230;

														{	/* Eval/evmeaning.scm 1096 */
															obj_t BgL_list1834z00_2231;

															BgL_list1834z00_2231 =
																MAKE_YOUNG_PAIR(BgL_a0z00_2220, BNIL);
															BgL_arg1833z00_2230 = BgL_list1834z00_2231;
														}
														return
															MAKE_YOUNG_PAIR(BgL_arg1833z00_2230,
															BgL_envdz00_2221);
													}
													break;
												case -2L:

													{	/* Eval/evmeaning.scm 1098 */
														obj_t BgL_arg1835z00_2232;

														BgL_arg1835z00_2232 =
															MAKE_YOUNG_PAIR(BNIL, BgL_envdz00_2221);
														return
															MAKE_YOUNG_PAIR(BgL_a0z00_2220,
															BgL_arg1835z00_2232);
													}
													break;
												default:
												BgL_case_else1124z00_2228:
													return
														BGl_evarityzd2errorzd2zz__everrorz00
														(BgL_locz00_2223, VECTOR_REF(BgL_codez00_169, 2L),
														(int) (1L), CINT(BgL_arityz00_2222));
										}}
									else
										{	/* Eval/evmeaning.scm 1092 */
											goto BgL_case_else1124z00_2228;
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-tailcall-2-stack */
	obj_t BGl_evmeaningzd2tailcallzd22zd2stackzd2zz__evmeaningz00(obj_t
		BgL_codez00_173, obj_t BgL_stackz00_174, obj_t BgL_denvz00_175,
		obj_t BgL_funz00_176)
	{
		{	/* Eval/evmeaning.scm 1105 */
			{	/* Eval/evmeaning.scm 1106 */
				obj_t BgL_a0z00_2235;

				BgL_a0z00_2235 =
					BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_173, 4L),
					BgL_stackz00_174, BgL_denvz00_175);
				{	/* Eval/evmeaning.scm 1106 */
					obj_t BgL_a1z00_2236;

					BgL_a1z00_2236 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_173, 5L),
						BgL_stackz00_174, BgL_denvz00_175);
					{	/* Eval/evmeaning.scm 1107 */

						{	/* Eval/evmeaning.scm 1108 */
							obj_t BgL_envdz00_2237;

							{	/* Eval/evmeaning.scm 952 */
								obj_t BgL_arg1794z00_3568;

								{	/* Eval/evmeaning.scm 952 */
									obj_t BgL_tmpz00_6250;

									BgL_tmpz00_6250 = ((obj_t) BgL_funz00_176);
									BgL_arg1794z00_3568 = PROCEDURE_ATTR(BgL_tmpz00_6250);
								}
								BgL_envdz00_2237 =
									STRUCT_REF(((obj_t) BgL_arg1794z00_3568), (int) (2L));
							}
							{	/* Eval/evmeaning.scm 1108 */
								obj_t BgL_arityz00_2238;

								{	/* Eval/evmeaning.scm 958 */
									obj_t BgL_arg1795z00_3571;

									{	/* Eval/evmeaning.scm 958 */
										obj_t BgL_tmpz00_6256;

										BgL_tmpz00_6256 = ((obj_t) BgL_funz00_176);
										BgL_arg1795z00_3571 = PROCEDURE_ATTR(BgL_tmpz00_6256);
									}
									BgL_arityz00_2238 =
										STRUCT_REF(((obj_t) BgL_arg1795z00_3571), (int) (0L));
								}
								{	/* Eval/evmeaning.scm 1109 */
									obj_t BgL_locz00_2239;

									BgL_locz00_2239 = VECTOR_REF(BgL_codez00_173, 1L);
									{	/* Eval/evmeaning.scm 1110 */

										{	/* Eval/evmeaning.scm 1111 */
											bool_t BgL_test2507z00_6263;

											{	/* Eval/evmeaning.scm 1111 */
												obj_t BgL_tmpz00_6264;

												BgL_tmpz00_6264 = VECTOR_REF(BgL_codez00_173, 2L);
												BgL_test2507z00_6263 = SYMBOLP(BgL_tmpz00_6264);
											}
											if (BgL_test2507z00_6263)
												{	/* Eval/evmeaning.scm 1111 */
													{	/* Eval/evmeaning.scm 1112 */
														obj_t BgL_tmpz00_6267;

														BgL_tmpz00_6267 = VECTOR_REF(BgL_codez00_173, 2L);
														BGL_ENV_SET_TRACE_NAME(BgL_denvz00_175,
															BgL_tmpz00_6267);
													}
													BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_175,
														BgL_locz00_2239);
												}
											else
												{	/* Eval/evmeaning.scm 1111 */
													BFALSE;
												}
										}
										{

											if (INTEGERP(BgL_arityz00_2238))
												{	/* Eval/evmeaning.scm 1114 */
													switch ((long) CINT(BgL_arityz00_2238))
														{
														case 2L:

															{	/* Eval/evmeaning.scm 1116 */
																obj_t BgL_arg1843z00_2246;

																BgL_arg1843z00_2246 =
																	MAKE_YOUNG_PAIR(BgL_a1z00_2236,
																	BgL_envdz00_2237);
																return MAKE_YOUNG_PAIR(BgL_a0z00_2235,
																	BgL_arg1843z00_2246);
															}
															break;
														case -1L:

															{	/* Eval/evmeaning.scm 1118 */
																obj_t BgL_arg1844z00_2247;

																{	/* Eval/evmeaning.scm 1118 */
																	obj_t BgL_list1845z00_2248;

																	{	/* Eval/evmeaning.scm 1118 */
																		obj_t BgL_arg1846z00_2249;

																		BgL_arg1846z00_2249 =
																			MAKE_YOUNG_PAIR(BgL_a1z00_2236, BNIL);
																		BgL_list1845z00_2248 =
																			MAKE_YOUNG_PAIR(BgL_a0z00_2235,
																			BgL_arg1846z00_2249);
																	}
																	BgL_arg1844z00_2247 = BgL_list1845z00_2248;
																}
																return
																	MAKE_YOUNG_PAIR(BgL_arg1844z00_2247,
																	BgL_envdz00_2237);
															}
															break;
														case -2L:

															{	/* Eval/evmeaning.scm 1120 */
																obj_t BgL_arg1847z00_2250;

																{	/* Eval/evmeaning.scm 1120 */
																	obj_t BgL_arg1848z00_2251;

																	{	/* Eval/evmeaning.scm 1120 */
																		obj_t BgL_list1849z00_2252;

																		BgL_list1849z00_2252 =
																			MAKE_YOUNG_PAIR(BgL_a1z00_2236, BNIL);
																		BgL_arg1848z00_2251 = BgL_list1849z00_2252;
																	}
																	BgL_arg1847z00_2250 =
																		MAKE_YOUNG_PAIR(BgL_arg1848z00_2251,
																		BgL_envdz00_2237);
																}
																return
																	MAKE_YOUNG_PAIR(BgL_a0z00_2235,
																	BgL_arg1847z00_2250);
															}
															break;
														case -3L:

															{	/* Eval/evmeaning.scm 1122 */
																obj_t BgL_arg1850z00_2253;

																{	/* Eval/evmeaning.scm 1122 */
																	obj_t BgL_arg1851z00_2254;

																	BgL_arg1851z00_2254 =
																		MAKE_YOUNG_PAIR(BNIL, BgL_envdz00_2237);
																	BgL_arg1850z00_2253 =
																		MAKE_YOUNG_PAIR(BgL_a1z00_2236,
																		BgL_arg1851z00_2254);
																}
																return
																	MAKE_YOUNG_PAIR(BgL_a0z00_2235,
																	BgL_arg1850z00_2253);
															}
															break;
														default:
														BgL_case_else1126z00_2244:
															return
																BGl_evarityzd2errorzd2zz__everrorz00
																(BgL_locz00_2239, VECTOR_REF(BgL_codez00_173,
																	2L), (int) (2L), CINT(BgL_arityz00_2238));
												}}
											else
												{	/* Eval/evmeaning.scm 1114 */
													goto BgL_case_else1126z00_2244;
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-tailcall-3-stack */
	obj_t BGl_evmeaningzd2tailcallzd23zd2stackzd2zz__evmeaningz00(obj_t
		BgL_codez00_177, obj_t BgL_stackz00_178, obj_t BgL_denvz00_179,
		obj_t BgL_funz00_180)
	{
		{	/* Eval/evmeaning.scm 1129 */
			{	/* Eval/evmeaning.scm 1130 */
				obj_t BgL_a0z00_2258;

				BgL_a0z00_2258 =
					BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_177, 4L),
					BgL_stackz00_178, BgL_denvz00_179);
				{	/* Eval/evmeaning.scm 1130 */
					obj_t BgL_a1z00_2259;

					BgL_a1z00_2259 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_177, 5L),
						BgL_stackz00_178, BgL_denvz00_179);
					{	/* Eval/evmeaning.scm 1131 */
						obj_t BgL_a2z00_2260;

						BgL_a2z00_2260 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_177, 6L),
							BgL_stackz00_178, BgL_denvz00_179);
						{	/* Eval/evmeaning.scm 1132 */

							{	/* Eval/evmeaning.scm 1133 */
								obj_t BgL_envdz00_2261;

								{	/* Eval/evmeaning.scm 952 */
									obj_t BgL_arg1794z00_3583;

									{	/* Eval/evmeaning.scm 952 */
										obj_t BgL_tmpz00_6296;

										BgL_tmpz00_6296 = ((obj_t) BgL_funz00_180);
										BgL_arg1794z00_3583 = PROCEDURE_ATTR(BgL_tmpz00_6296);
									}
									BgL_envdz00_2261 =
										STRUCT_REF(((obj_t) BgL_arg1794z00_3583), (int) (2L));
								}
								{	/* Eval/evmeaning.scm 1133 */
									obj_t BgL_arityz00_2262;

									{	/* Eval/evmeaning.scm 958 */
										obj_t BgL_arg1795z00_3586;

										{	/* Eval/evmeaning.scm 958 */
											obj_t BgL_tmpz00_6302;

											BgL_tmpz00_6302 = ((obj_t) BgL_funz00_180);
											BgL_arg1795z00_3586 = PROCEDURE_ATTR(BgL_tmpz00_6302);
										}
										BgL_arityz00_2262 =
											STRUCT_REF(((obj_t) BgL_arg1795z00_3586), (int) (0L));
									}
									{	/* Eval/evmeaning.scm 1134 */
										obj_t BgL_locz00_2263;

										BgL_locz00_2263 = VECTOR_REF(BgL_codez00_177, 1L);
										{	/* Eval/evmeaning.scm 1135 */

											{	/* Eval/evmeaning.scm 1136 */
												bool_t BgL_test2509z00_6309;

												{	/* Eval/evmeaning.scm 1136 */
													obj_t BgL_tmpz00_6310;

													BgL_tmpz00_6310 = VECTOR_REF(BgL_codez00_177, 2L);
													BgL_test2509z00_6309 = SYMBOLP(BgL_tmpz00_6310);
												}
												if (BgL_test2509z00_6309)
													{	/* Eval/evmeaning.scm 1136 */
														{	/* Eval/evmeaning.scm 1137 */
															obj_t BgL_tmpz00_6313;

															BgL_tmpz00_6313 = VECTOR_REF(BgL_codez00_177, 2L);
															BGL_ENV_SET_TRACE_NAME(BgL_denvz00_179,
																BgL_tmpz00_6313);
														}
														BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_179,
															BgL_locz00_2263);
													}
												else
													{	/* Eval/evmeaning.scm 1136 */
														BFALSE;
													}
											}
											{

												if (INTEGERP(BgL_arityz00_2262))
													{	/* Eval/evmeaning.scm 1139 */
														switch ((long) CINT(BgL_arityz00_2262))
															{
															case 3L:

																{	/* Eval/evmeaning.scm 1141 */
																	obj_t BgL_arg1860z00_2270;

																	{	/* Eval/evmeaning.scm 1141 */
																		obj_t BgL_arg1862z00_2271;

																		BgL_arg1862z00_2271 =
																			MAKE_YOUNG_PAIR(BgL_a2z00_2260,
																			BgL_envdz00_2261);
																		BgL_arg1860z00_2270 =
																			MAKE_YOUNG_PAIR(BgL_a1z00_2259,
																			BgL_arg1862z00_2271);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_a0z00_2258,
																		BgL_arg1860z00_2270);
																}
																break;
															case -1L:

																{	/* Eval/evmeaning.scm 1143 */
																	obj_t BgL_arg1863z00_2272;

																	{	/* Eval/evmeaning.scm 1143 */
																		obj_t BgL_list1864z00_2273;

																		{	/* Eval/evmeaning.scm 1143 */
																			obj_t BgL_arg1866z00_2274;

																			{	/* Eval/evmeaning.scm 1143 */
																				obj_t BgL_arg1868z00_2275;

																				BgL_arg1868z00_2275 =
																					MAKE_YOUNG_PAIR(BgL_a2z00_2260, BNIL);
																				BgL_arg1866z00_2274 =
																					MAKE_YOUNG_PAIR(BgL_a1z00_2259,
																					BgL_arg1868z00_2275);
																			}
																			BgL_list1864z00_2273 =
																				MAKE_YOUNG_PAIR(BgL_a0z00_2258,
																				BgL_arg1866z00_2274);
																		}
																		BgL_arg1863z00_2272 = BgL_list1864z00_2273;
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_arg1863z00_2272,
																		BgL_envdz00_2261);
																}
																break;
															case -2L:

																{	/* Eval/evmeaning.scm 1145 */
																	obj_t BgL_arg1869z00_2276;

																	{	/* Eval/evmeaning.scm 1145 */
																		obj_t BgL_arg1870z00_2277;

																		{	/* Eval/evmeaning.scm 1145 */
																			obj_t BgL_list1871z00_2278;

																			{	/* Eval/evmeaning.scm 1145 */
																				obj_t BgL_arg1872z00_2279;

																				BgL_arg1872z00_2279 =
																					MAKE_YOUNG_PAIR(BgL_a2z00_2260, BNIL);
																				BgL_list1871z00_2278 =
																					MAKE_YOUNG_PAIR(BgL_a1z00_2259,
																					BgL_arg1872z00_2279);
																			}
																			BgL_arg1870z00_2277 =
																				BgL_list1871z00_2278;
																		}
																		BgL_arg1869z00_2276 =
																			MAKE_YOUNG_PAIR(BgL_arg1870z00_2277,
																			BgL_envdz00_2261);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_a0z00_2258,
																		BgL_arg1869z00_2276);
																}
																break;
															case -3L:

																{	/* Eval/evmeaning.scm 1147 */
																	obj_t BgL_arg1873z00_2280;

																	{	/* Eval/evmeaning.scm 1147 */
																		obj_t BgL_arg1874z00_2281;

																		{	/* Eval/evmeaning.scm 1147 */
																			obj_t BgL_arg1875z00_2282;

																			{	/* Eval/evmeaning.scm 1147 */
																				obj_t BgL_list1876z00_2283;

																				BgL_list1876z00_2283 =
																					MAKE_YOUNG_PAIR(BgL_a2z00_2260, BNIL);
																				BgL_arg1875z00_2282 =
																					BgL_list1876z00_2283;
																			}
																			BgL_arg1874z00_2281 =
																				MAKE_YOUNG_PAIR(BgL_arg1875z00_2282,
																				BgL_envdz00_2261);
																		}
																		BgL_arg1873z00_2280 =
																			MAKE_YOUNG_PAIR(BgL_a1z00_2259,
																			BgL_arg1874z00_2281);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_a0z00_2258,
																		BgL_arg1873z00_2280);
																}
																break;
															case -4L:

																{	/* Eval/evmeaning.scm 1149 */
																	obj_t BgL_arg1877z00_2284;

																	{	/* Eval/evmeaning.scm 1149 */
																		obj_t BgL_arg1878z00_2285;

																		{	/* Eval/evmeaning.scm 1149 */
																			obj_t BgL_arg1879z00_2286;

																			BgL_arg1879z00_2286 =
																				MAKE_YOUNG_PAIR(BNIL, BgL_envdz00_2261);
																			BgL_arg1878z00_2285 =
																				MAKE_YOUNG_PAIR(BgL_a2z00_2260,
																				BgL_arg1879z00_2286);
																		}
																		BgL_arg1877z00_2284 =
																			MAKE_YOUNG_PAIR(BgL_a1z00_2259,
																			BgL_arg1878z00_2285);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_a0z00_2258,
																		BgL_arg1877z00_2284);
																}
																break;
															default:
															BgL_case_else1128z00_2268:
																return
																	BGl_evarityzd2errorzd2zz__everrorz00
																	(BgL_locz00_2263, VECTOR_REF(BgL_codez00_177,
																		2L), (int) (3L), CINT(BgL_arityz00_2262));
													}}
												else
													{	/* Eval/evmeaning.scm 1139 */
														goto BgL_case_else1128z00_2268;
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-tailcall-4-stack */
	obj_t BGl_evmeaningzd2tailcallzd24zd2stackzd2zz__evmeaningz00(obj_t
		BgL_codez00_181, obj_t BgL_stackz00_182, obj_t BgL_denvz00_183,
		obj_t BgL_funz00_184)
	{
		{	/* Eval/evmeaning.scm 1156 */
			{	/* Eval/evmeaning.scm 1157 */
				obj_t BgL_a0z00_2291;

				BgL_a0z00_2291 =
					BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_181, 4L),
					BgL_stackz00_182, BgL_denvz00_183);
				{	/* Eval/evmeaning.scm 1157 */
					obj_t BgL_a1z00_2292;

					BgL_a1z00_2292 =
						BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_181, 5L),
						BgL_stackz00_182, BgL_denvz00_183);
					{	/* Eval/evmeaning.scm 1158 */
						obj_t BgL_a2z00_2293;

						BgL_a2z00_2293 =
							BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_181, 6L),
							BgL_stackz00_182, BgL_denvz00_183);
						{	/* Eval/evmeaning.scm 1159 */
							obj_t BgL_a3z00_2294;

							BgL_a3z00_2294 =
								BGl_evmeaningz00zz__evmeaningz00(VECTOR_REF(BgL_codez00_181,
									7L), BgL_stackz00_182, BgL_denvz00_183);
							{	/* Eval/evmeaning.scm 1160 */

								{	/* Eval/evmeaning.scm 1161 */
									obj_t BgL_envdz00_2295;

									{	/* Eval/evmeaning.scm 952 */
										obj_t BgL_arg1794z00_3600;

										{	/* Eval/evmeaning.scm 952 */
											obj_t BgL_tmpz00_6352;

											BgL_tmpz00_6352 = ((obj_t) BgL_funz00_184);
											BgL_arg1794z00_3600 = PROCEDURE_ATTR(BgL_tmpz00_6352);
										}
										BgL_envdz00_2295 =
											STRUCT_REF(((obj_t) BgL_arg1794z00_3600), (int) (2L));
									}
									{	/* Eval/evmeaning.scm 1161 */
										obj_t BgL_arityz00_2296;

										{	/* Eval/evmeaning.scm 958 */
											obj_t BgL_arg1795z00_3603;

											{	/* Eval/evmeaning.scm 958 */
												obj_t BgL_tmpz00_6358;

												BgL_tmpz00_6358 = ((obj_t) BgL_funz00_184);
												BgL_arg1795z00_3603 = PROCEDURE_ATTR(BgL_tmpz00_6358);
											}
											BgL_arityz00_2296 =
												STRUCT_REF(((obj_t) BgL_arg1795z00_3603), (int) (0L));
										}
										{	/* Eval/evmeaning.scm 1162 */
											obj_t BgL_locz00_2297;

											BgL_locz00_2297 = VECTOR_REF(BgL_codez00_181, 1L);
											{	/* Eval/evmeaning.scm 1163 */

												{	/* Eval/evmeaning.scm 1164 */
													bool_t BgL_test2511z00_6365;

													{	/* Eval/evmeaning.scm 1164 */
														obj_t BgL_tmpz00_6366;

														BgL_tmpz00_6366 = VECTOR_REF(BgL_codez00_181, 2L);
														BgL_test2511z00_6365 = SYMBOLP(BgL_tmpz00_6366);
													}
													if (BgL_test2511z00_6365)
														{	/* Eval/evmeaning.scm 1164 */
															{	/* Eval/evmeaning.scm 1165 */
																obj_t BgL_tmpz00_6369;

																BgL_tmpz00_6369 =
																	VECTOR_REF(BgL_codez00_181, 2L);
																BGL_ENV_SET_TRACE_NAME(BgL_denvz00_183,
																	BgL_tmpz00_6369);
															}
															BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_183,
																BgL_locz00_2297);
														}
													else
														{	/* Eval/evmeaning.scm 1164 */
															BFALSE;
														}
												}
												{

													if (INTEGERP(BgL_arityz00_2296))
														{	/* Eval/evmeaning.scm 1167 */
															switch ((long) CINT(BgL_arityz00_2296))
																{
																case 4L:

																	{	/* Eval/evmeaning.scm 1169 */
																		obj_t BgL_arg1889z00_2304;

																		{	/* Eval/evmeaning.scm 1169 */
																			obj_t BgL_arg1890z00_2305;

																			{	/* Eval/evmeaning.scm 1169 */
																				obj_t BgL_arg1891z00_2306;

																				BgL_arg1891z00_2306 =
																					MAKE_YOUNG_PAIR(BgL_a3z00_2294,
																					BgL_envdz00_2295);
																				BgL_arg1890z00_2305 =
																					MAKE_YOUNG_PAIR(BgL_a2z00_2293,
																					BgL_arg1891z00_2306);
																			}
																			BgL_arg1889z00_2304 =
																				MAKE_YOUNG_PAIR(BgL_a1z00_2292,
																				BgL_arg1890z00_2305);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_a0z00_2291,
																			BgL_arg1889z00_2304);
																	}
																	break;
																case -1L:

																	{	/* Eval/evmeaning.scm 1171 */
																		obj_t BgL_arg1892z00_2307;

																		{	/* Eval/evmeaning.scm 1171 */
																			obj_t BgL_list1893z00_2308;

																			{	/* Eval/evmeaning.scm 1171 */
																				obj_t BgL_arg1894z00_2309;

																				{	/* Eval/evmeaning.scm 1171 */
																					obj_t BgL_arg1896z00_2310;

																					{	/* Eval/evmeaning.scm 1171 */
																						obj_t BgL_arg1897z00_2311;

																						BgL_arg1897z00_2311 =
																							MAKE_YOUNG_PAIR(BgL_a3z00_2294,
																							BNIL);
																						BgL_arg1896z00_2310 =
																							MAKE_YOUNG_PAIR(BgL_a2z00_2293,
																							BgL_arg1897z00_2311);
																					}
																					BgL_arg1894z00_2309 =
																						MAKE_YOUNG_PAIR(BgL_a1z00_2292,
																						BgL_arg1896z00_2310);
																				}
																				BgL_list1893z00_2308 =
																					MAKE_YOUNG_PAIR(BgL_a0z00_2291,
																					BgL_arg1894z00_2309);
																			}
																			BgL_arg1892z00_2307 =
																				BgL_list1893z00_2308;
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_arg1892z00_2307,
																			BgL_envdz00_2295);
																	}
																	break;
																case -2L:

																	{	/* Eval/evmeaning.scm 1173 */
																		obj_t BgL_arg1898z00_2312;

																		{	/* Eval/evmeaning.scm 1173 */
																			obj_t BgL_arg1899z00_2313;

																			{	/* Eval/evmeaning.scm 1173 */
																				obj_t BgL_list1900z00_2314;

																				{	/* Eval/evmeaning.scm 1173 */
																					obj_t BgL_arg1901z00_2315;

																					{	/* Eval/evmeaning.scm 1173 */
																						obj_t BgL_arg1902z00_2316;

																						BgL_arg1902z00_2316 =
																							MAKE_YOUNG_PAIR(BgL_a3z00_2294,
																							BNIL);
																						BgL_arg1901z00_2315 =
																							MAKE_YOUNG_PAIR(BgL_a2z00_2293,
																							BgL_arg1902z00_2316);
																					}
																					BgL_list1900z00_2314 =
																						MAKE_YOUNG_PAIR(BgL_a1z00_2292,
																						BgL_arg1901z00_2315);
																				}
																				BgL_arg1899z00_2313 =
																					BgL_list1900z00_2314;
																			}
																			BgL_arg1898z00_2312 =
																				MAKE_YOUNG_PAIR(BgL_arg1899z00_2313,
																				BgL_envdz00_2295);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_a0z00_2291,
																			BgL_arg1898z00_2312);
																	}
																	break;
																case -3L:

																	{	/* Eval/evmeaning.scm 1175 */
																		obj_t BgL_arg1903z00_2317;

																		{	/* Eval/evmeaning.scm 1175 */
																			obj_t BgL_arg1904z00_2318;

																			{	/* Eval/evmeaning.scm 1175 */
																				obj_t BgL_arg1906z00_2319;

																				{	/* Eval/evmeaning.scm 1175 */
																					obj_t BgL_list1907z00_2320;

																					{	/* Eval/evmeaning.scm 1175 */
																						obj_t BgL_arg1910z00_2321;

																						BgL_arg1910z00_2321 =
																							MAKE_YOUNG_PAIR(BgL_a3z00_2294,
																							BNIL);
																						BgL_list1907z00_2320 =
																							MAKE_YOUNG_PAIR(BgL_a2z00_2293,
																							BgL_arg1910z00_2321);
																					}
																					BgL_arg1906z00_2319 =
																						BgL_list1907z00_2320;
																				}
																				BgL_arg1904z00_2318 =
																					MAKE_YOUNG_PAIR(BgL_arg1906z00_2319,
																					BgL_envdz00_2295);
																			}
																			BgL_arg1903z00_2317 =
																				MAKE_YOUNG_PAIR(BgL_a1z00_2292,
																				BgL_arg1904z00_2318);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_a0z00_2291,
																			BgL_arg1903z00_2317);
																	}
																	break;
																case -4L:

																	{	/* Eval/evmeaning.scm 1177 */
																		obj_t BgL_arg1911z00_2322;

																		{	/* Eval/evmeaning.scm 1177 */
																			obj_t BgL_arg1912z00_2323;

																			{	/* Eval/evmeaning.scm 1177 */
																				obj_t BgL_arg1913z00_2324;

																				{	/* Eval/evmeaning.scm 1177 */
																					obj_t BgL_arg1914z00_2325;

																					{	/* Eval/evmeaning.scm 1177 */
																						obj_t BgL_list1915z00_2326;

																						BgL_list1915z00_2326 =
																							MAKE_YOUNG_PAIR(BgL_a3z00_2294,
																							BNIL);
																						BgL_arg1914z00_2325 =
																							BgL_list1915z00_2326;
																					}
																					BgL_arg1913z00_2324 =
																						MAKE_YOUNG_PAIR(BgL_arg1914z00_2325,
																						BgL_envdz00_2295);
																				}
																				BgL_arg1912z00_2323 =
																					MAKE_YOUNG_PAIR(BgL_a2z00_2293,
																					BgL_arg1913z00_2324);
																			}
																			BgL_arg1911z00_2322 =
																				MAKE_YOUNG_PAIR(BgL_a1z00_2292,
																				BgL_arg1912z00_2323);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_a0z00_2291,
																			BgL_arg1911z00_2322);
																	}
																	break;
																case -5L:

																	{	/* Eval/evmeaning.scm 1179 */
																		obj_t BgL_arg1916z00_2327;

																		{	/* Eval/evmeaning.scm 1179 */
																			obj_t BgL_arg1917z00_2328;

																			{	/* Eval/evmeaning.scm 1179 */
																				obj_t BgL_arg1918z00_2329;

																				{	/* Eval/evmeaning.scm 1179 */
																					obj_t BgL_arg1919z00_2330;

																					BgL_arg1919z00_2330 =
																						MAKE_YOUNG_PAIR(BNIL,
																						BgL_envdz00_2295);
																					BgL_arg1918z00_2329 =
																						MAKE_YOUNG_PAIR(BgL_a3z00_2294,
																						BgL_arg1919z00_2330);
																				}
																				BgL_arg1917z00_2328 =
																					MAKE_YOUNG_PAIR(BgL_a2z00_2293,
																					BgL_arg1918z00_2329);
																			}
																			BgL_arg1916z00_2327 =
																				MAKE_YOUNG_PAIR(BgL_a1z00_2292,
																				BgL_arg1917z00_2328);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_a0z00_2291,
																			BgL_arg1916z00_2327);
																	}
																	break;
																default:
																BgL_case_else1130z00_2302:
																	return
																		BGl_evarityzd2errorzd2zz__everrorz00
																		(BgL_locz00_2297,
																		VECTOR_REF(BgL_codez00_181, 2L), (int) (4L),
																		CINT(BgL_arityz00_2296));
														}}
													else
														{	/* Eval/evmeaning.scm 1167 */
															goto BgL_case_else1130z00_2302;
														}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-make-traced-4procedure */
	obj_t BGl_evmeaningzd2makezd2tracedzd24procedurezd2zz__evmeaningz00(obj_t
		BgL_codez00_190, obj_t BgL_stackz00_191, obj_t BgL_denvz00_192)
	{
		{	/* Eval/evmeaning.scm 1198 */
			{	/* Eval/evmeaning.scm 1199 */
				obj_t BgL_bodyz00_2340;
				obj_t BgL_wherez00_2341;
				obj_t BgL_formalsz00_2342;
				obj_t BgL_locz00_2343;

				BgL_bodyz00_2340 = VECTOR_REF(BgL_codez00_190, 2L);
				BgL_wherez00_2341 = VECTOR_REF(BgL_codez00_190, 3L);
				BgL_formalsz00_2342 = VECTOR_REF(BgL_codez00_190, 4L);
				BgL_locz00_2343 = VECTOR_REF(BgL_codez00_190, 1L);
				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_formalsz00_2342))
					{	/* Eval/evmeaning.scm 1204 */
						long BgL_lfz00_2345;

						BgL_lfz00_2345 = bgl_list_length(BgL_formalsz00_2342);
						{	/* Eval/evmeaning.scm 1207 */
							obj_t BgL_zc3z04anonymousza31933ze3z87_4076;

							BgL_zc3z04anonymousza31933ze3z87_4076 =
								MAKE_VA_PROCEDURE
								(BGl_z62zc3z04anonymousza31933ze3ze5zz__evmeaningz00,
								(int) (-1L), (int) (6L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31933ze3z87_4076, (int) (0L),
								BgL_wherez00_2341);
							PROCEDURE_SET(BgL_zc3z04anonymousza31933ze3z87_4076, (int) (1L),
								BgL_locz00_2343);
							PROCEDURE_SET(BgL_zc3z04anonymousza31933ze3z87_4076, (int) (2L),
								BgL_codez00_190);
							PROCEDURE_SET(BgL_zc3z04anonymousza31933ze3z87_4076, (int) (3L),
								BINT(BgL_lfz00_2345));
							PROCEDURE_SET(BgL_zc3z04anonymousza31933ze3z87_4076, (int) (4L),
								BgL_stackz00_191);
							PROCEDURE_SET(BgL_zc3z04anonymousza31933ze3z87_4076, (int) (5L),
								BgL_bodyz00_2340);
							{	/* Eval/evmeaning.scm 939 */
								obj_t BgL_arg1792z00_3624;

								{	/* Eval/evmeaning.scm 927 */
									obj_t BgL_newz00_3625;

									BgL_newz00_3625 =
										create_struct(BGl_symbol2321z00zz__evmeaningz00,
										(int) (3L));
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6435;

										BgL_tmpz00_6435 = (int) (2L);
										STRUCT_SET(BgL_newz00_3625, BgL_tmpz00_6435,
											BgL_stackz00_191);
									}
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6438;

										BgL_tmpz00_6438 = (int) (1L);
										STRUCT_SET(BgL_newz00_3625, BgL_tmpz00_6438,
											BgL_bodyz00_2340);
									}
									{	/* Eval/evmeaning.scm 927 */
										obj_t BgL_auxz00_6443;
										int BgL_tmpz00_6441;

										BgL_auxz00_6443 = BINT(BgL_lfz00_2345);
										BgL_tmpz00_6441 = (int) (0L);
										STRUCT_SET(BgL_newz00_3625, BgL_tmpz00_6441,
											BgL_auxz00_6443);
									}
									BgL_arg1792z00_3624 = BgL_newz00_3625;
								}
								PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31933ze3z87_4076,
									BgL_arg1792z00_3624);
								BgL_arg1792z00_3624;
							}
							return BgL_zc3z04anonymousza31933ze3z87_4076;
						}
					}
				else
					{	/* Eval/evmeaning.scm 1219 */
						long BgL_lfz00_2355;

						{
							obj_t BgL_formalsz00_2366;
							long BgL_numz00_2367;

							BgL_formalsz00_2366 = BgL_formalsz00_2342;
							BgL_numz00_2367 = -1L;
						BgL_zc3z04anonymousza31936ze3z87_2368:
							if (PAIRP(BgL_formalsz00_2366))
								{
									long BgL_numz00_6451;
									obj_t BgL_formalsz00_6449;

									BgL_formalsz00_6449 = CDR(BgL_formalsz00_2366);
									BgL_numz00_6451 = (BgL_numz00_2367 - 1L);
									BgL_numz00_2367 = BgL_numz00_6451;
									BgL_formalsz00_2366 = BgL_formalsz00_6449;
									goto BgL_zc3z04anonymousza31936ze3z87_2368;
								}
							else
								{	/* Eval/evmeaning.scm 1221 */
									BgL_lfz00_2355 = BgL_numz00_2367;
								}
						}
						{	/* Eval/evmeaning.scm 1226 */
							obj_t BgL_zc3z04anonymousza31935ze3z87_4077;

							BgL_zc3z04anonymousza31935ze3z87_4077 =
								MAKE_VA_PROCEDURE
								(BGl_z62zc3z04anonymousza31935ze3ze5zz__evmeaningz00,
								(int) (-1L), (int) (6L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31935ze3z87_4077, (int) (0L),
								BgL_wherez00_2341);
							PROCEDURE_SET(BgL_zc3z04anonymousza31935ze3z87_4077, (int) (1L),
								BgL_locz00_2343);
							PROCEDURE_SET(BgL_zc3z04anonymousza31935ze3z87_4077, (int) (2L),
								BgL_codez00_190);
							PROCEDURE_SET(BgL_zc3z04anonymousza31935ze3z87_4077, (int) (3L),
								BINT(BgL_lfz00_2355));
							PROCEDURE_SET(BgL_zc3z04anonymousza31935ze3z87_4077, (int) (4L),
								BgL_stackz00_191);
							PROCEDURE_SET(BgL_zc3z04anonymousza31935ze3z87_4077, (int) (5L),
								BgL_bodyz00_2340);
							{	/* Eval/evmeaning.scm 939 */
								obj_t BgL_arg1792z00_3631;

								{	/* Eval/evmeaning.scm 927 */
									obj_t BgL_newz00_3632;

									BgL_newz00_3632 =
										create_struct(BGl_symbol2321z00zz__evmeaningz00,
										(int) (3L));
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6471;

										BgL_tmpz00_6471 = (int) (2L);
										STRUCT_SET(BgL_newz00_3632, BgL_tmpz00_6471,
											BgL_stackz00_191);
									}
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6474;

										BgL_tmpz00_6474 = (int) (1L);
										STRUCT_SET(BgL_newz00_3632, BgL_tmpz00_6474,
											BgL_bodyz00_2340);
									}
									{	/* Eval/evmeaning.scm 927 */
										obj_t BgL_auxz00_6479;
										int BgL_tmpz00_6477;

										BgL_auxz00_6479 = BINT(BgL_lfz00_2355);
										BgL_tmpz00_6477 = (int) (0L);
										STRUCT_SET(BgL_newz00_3632, BgL_tmpz00_6477,
											BgL_auxz00_6479);
									}
									BgL_arg1792z00_3631 = BgL_newz00_3632;
								}
								PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31935ze3z87_4077,
									BgL_arg1792z00_3631);
								BgL_arg1792z00_3631;
							}
							return BgL_zc3z04anonymousza31935ze3z87_4077;
						}
					}
			}
		}

	}



/* &<@anonymous:1933> */
	obj_t BGl_z62zc3z04anonymousza31933ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4078, obj_t BgL_xz00_4085)
	{
		{	/* Eval/evmeaning.scm 1206 */
			{	/* Eval/evmeaning.scm 1207 */
				obj_t BgL_wherez00_4079;
				obj_t BgL_locz00_4080;
				obj_t BgL_codez00_4081;
				long BgL_lfz00_4082;
				obj_t BgL_stackz00_4083;
				obj_t BgL_bodyz00_4084;

				BgL_wherez00_4079 = PROCEDURE_REF(BgL_envz00_4078, (int) (0L));
				BgL_locz00_4080 = PROCEDURE_REF(BgL_envz00_4078, (int) (1L));
				BgL_codez00_4081 = ((obj_t) PROCEDURE_REF(BgL_envz00_4078, (int) (2L)));
				BgL_lfz00_4082 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4078, (int) (3L)));
				BgL_stackz00_4083 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4078, (int) (4L)));
				BgL_bodyz00_4084 = PROCEDURE_REF(BgL_envz00_4078, (int) (5L));
				{	/* Eval/evmeaning.scm 1207 */
					obj_t BgL_z12denvz12_4202;

					BgL_z12denvz12_4202 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 1210 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4202, BgL_wherez00_4079,
							BgL_locz00_4080);
						{	/* Eval/evmeaning.scm 1212 */
							obj_t BgL_e2z00_4203;

							BgL_e2z00_4203 =
								BGl_evmeaningzd2pushzd2fxargsz00zz__evmeaningz00
								(BgL_wherez00_4079, BgL_codez00_4081, BgL_xz00_4085,
								BINT(BgL_lfz00_4082), BgL_stackz00_4083);
							{	/* Eval/evmeaning.scm 1213 */
								obj_t BgL_resz00_4204;

								BgL_resz00_4204 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4084,
									BgL_e2z00_4203, BgL_z12denvz12_4202);
								BGL_ENV_POP_TRACE(BgL_z12denvz12_4202);
								return BgL_resz00_4204;
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1935> */
	obj_t BGl_z62zc3z04anonymousza31935ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4086, obj_t BgL_xz00_4093)
	{
		{	/* Eval/evmeaning.scm 1225 */
			{	/* Eval/evmeaning.scm 1226 */
				obj_t BgL_wherez00_4087;
				obj_t BgL_locz00_4088;
				obj_t BgL_codez00_4089;
				long BgL_lfz00_4090;
				obj_t BgL_stackz00_4091;
				obj_t BgL_bodyz00_4092;

				BgL_wherez00_4087 = PROCEDURE_REF(BgL_envz00_4086, (int) (0L));
				BgL_locz00_4088 = PROCEDURE_REF(BgL_envz00_4086, (int) (1L));
				BgL_codez00_4089 = ((obj_t) PROCEDURE_REF(BgL_envz00_4086, (int) (2L)));
				BgL_lfz00_4090 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4086, (int) (3L)));
				BgL_stackz00_4091 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4086, (int) (4L)));
				BgL_bodyz00_4092 = PROCEDURE_REF(BgL_envz00_4086, (int) (5L));
				{	/* Eval/evmeaning.scm 1226 */
					obj_t BgL_z12denvz12_4205;

					BgL_z12denvz12_4205 = BGL_CURRENT_DYNAMIC_ENV();
					{	/* Eval/evmeaning.scm 1229 */

						BGL_ENV_PUSH_TRACE(BgL_z12denvz12_4205, BgL_wherez00_4087,
							BgL_locz00_4088);
						{	/* Eval/evmeaning.scm 1231 */
							obj_t BgL_e2z00_4206;

							BgL_e2z00_4206 =
								BGl_evmeaningzd2pushzd2vaargsz00zz__evmeaningz00
								(BgL_wherez00_4087, BgL_codez00_4089, BgL_xz00_4093,
								BINT(BgL_lfz00_4090), BgL_stackz00_4091);
							{	/* Eval/evmeaning.scm 1232 */
								obj_t BgL_resz00_4207;

								BgL_resz00_4207 =
									BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4092,
									BgL_e2z00_4206, BgL_z12denvz12_4205);
								BGL_ENV_POP_TRACE(BgL_z12denvz12_4205);
								return BgL_resz00_4207;
							}
						}
					}
				}
			}
		}

	}



/* evmeaning-make-4procedure */
	obj_t BGl_evmeaningzd2makezd24procedurez00zz__evmeaningz00(obj_t
		BgL_codez00_193, obj_t BgL_stackz00_194, obj_t BgL_denvz00_195)
	{
		{	/* Eval/evmeaning.scm 1242 */
			{	/* Eval/evmeaning.scm 1243 */
				obj_t BgL_bodyz00_2373;
				obj_t BgL_formalsz00_2374;

				BgL_bodyz00_2373 = VECTOR_REF(BgL_codez00_193, 2L);
				BgL_formalsz00_2374 = VECTOR_REF(BgL_codez00_193, 3L);
				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_formalsz00_2374))
					{	/* Eval/evmeaning.scm 1246 */
						long BgL_lfz00_2376;

						BgL_lfz00_2376 = bgl_list_length(BgL_formalsz00_2374);
						{	/* Eval/evmeaning.scm 1249 */
							obj_t BgL_zc3z04anonymousza31942ze3z87_4094;

							BgL_zc3z04anonymousza31942ze3z87_4094 =
								MAKE_VA_PROCEDURE
								(BGl_z62zc3z04anonymousza31942ze3ze5zz__evmeaningz00,
								(int) (-1L), (int) (5L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31942ze3z87_4094, (int) (0L),
								BgL_codez00_193);
							PROCEDURE_SET(BgL_zc3z04anonymousza31942ze3z87_4094, (int) (1L),
								BINT(BgL_lfz00_2376));
							PROCEDURE_SET(BgL_zc3z04anonymousza31942ze3z87_4094, (int) (2L),
								BgL_stackz00_194);
							PROCEDURE_SET(BgL_zc3z04anonymousza31942ze3z87_4094, (int) (3L),
								BgL_bodyz00_2373);
							PROCEDURE_SET(BgL_zc3z04anonymousza31942ze3z87_4094, (int) (4L),
								BgL_denvz00_195);
							{	/* Eval/evmeaning.scm 939 */
								obj_t BgL_arg1792z00_3638;

								{	/* Eval/evmeaning.scm 927 */
									obj_t BgL_newz00_3639;

									BgL_newz00_3639 =
										create_struct(BGl_symbol2321z00zz__evmeaningz00,
										(int) (3L));
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6546;

										BgL_tmpz00_6546 = (int) (2L);
										STRUCT_SET(BgL_newz00_3639, BgL_tmpz00_6546,
											BgL_stackz00_194);
									}
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6549;

										BgL_tmpz00_6549 = (int) (1L);
										STRUCT_SET(BgL_newz00_3639, BgL_tmpz00_6549,
											BgL_bodyz00_2373);
									}
									{	/* Eval/evmeaning.scm 927 */
										obj_t BgL_auxz00_6554;
										int BgL_tmpz00_6552;

										BgL_auxz00_6554 = BINT(BgL_lfz00_2376);
										BgL_tmpz00_6552 = (int) (0L);
										STRUCT_SET(BgL_newz00_3639, BgL_tmpz00_6552,
											BgL_auxz00_6554);
									}
									BgL_arg1792z00_3638 = BgL_newz00_3639;
								}
								PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31942ze3z87_4094,
									BgL_arg1792z00_3638);
								BgL_arg1792z00_3638;
							}
							return BgL_zc3z04anonymousza31942ze3z87_4094;
						}
					}
				else
					{	/* Eval/evmeaning.scm 1256 */
						long BgL_lfz00_2384;

						{
							obj_t BgL_formalsz00_2393;
							long BgL_numz00_2394;

							BgL_formalsz00_2393 = BgL_formalsz00_2374;
							BgL_numz00_2394 = -1L;
						BgL_zc3z04anonymousza31945ze3z87_2395:
							if (PAIRP(BgL_formalsz00_2393))
								{
									long BgL_numz00_6562;
									obj_t BgL_formalsz00_6560;

									BgL_formalsz00_6560 = CDR(BgL_formalsz00_2393);
									BgL_numz00_6562 = (BgL_numz00_2394 - 1L);
									BgL_numz00_2394 = BgL_numz00_6562;
									BgL_formalsz00_2393 = BgL_formalsz00_6560;
									goto BgL_zc3z04anonymousza31945ze3z87_2395;
								}
							else
								{	/* Eval/evmeaning.scm 1258 */
									BgL_lfz00_2384 = BgL_numz00_2394;
								}
						}
						{	/* Eval/evmeaning.scm 1263 */
							obj_t BgL_zc3z04anonymousza31944ze3z87_4095;

							BgL_zc3z04anonymousza31944ze3z87_4095 =
								MAKE_VA_PROCEDURE
								(BGl_z62zc3z04anonymousza31944ze3ze5zz__evmeaningz00,
								(int) (-1L), (int) (5L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4095, (int) (0L),
								BgL_codez00_193);
							PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4095, (int) (1L),
								BINT(BgL_lfz00_2384));
							PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4095, (int) (2L),
								BgL_stackz00_194);
							PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4095, (int) (3L),
								BgL_bodyz00_2373);
							PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4095, (int) (4L),
								BgL_denvz00_195);
							{	/* Eval/evmeaning.scm 939 */
								obj_t BgL_arg1792z00_3645;

								{	/* Eval/evmeaning.scm 927 */
									obj_t BgL_newz00_3646;

									BgL_newz00_3646 =
										create_struct(BGl_symbol2321z00zz__evmeaningz00,
										(int) (3L));
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6580;

										BgL_tmpz00_6580 = (int) (2L);
										STRUCT_SET(BgL_newz00_3646, BgL_tmpz00_6580,
											BgL_stackz00_194);
									}
									{	/* Eval/evmeaning.scm 927 */
										int BgL_tmpz00_6583;

										BgL_tmpz00_6583 = (int) (1L);
										STRUCT_SET(BgL_newz00_3646, BgL_tmpz00_6583,
											BgL_bodyz00_2373);
									}
									{	/* Eval/evmeaning.scm 927 */
										obj_t BgL_auxz00_6588;
										int BgL_tmpz00_6586;

										BgL_auxz00_6588 = BINT(BgL_lfz00_2384);
										BgL_tmpz00_6586 = (int) (0L);
										STRUCT_SET(BgL_newz00_3646, BgL_tmpz00_6586,
											BgL_auxz00_6588);
									}
									BgL_arg1792z00_3645 = BgL_newz00_3646;
								}
								PROCEDURE_ATTR_SET(BgL_zc3z04anonymousza31944ze3z87_4095,
									BgL_arg1792z00_3645);
								BgL_arg1792z00_3645;
							}
							return BgL_zc3z04anonymousza31944ze3z87_4095;
						}
					}
			}
		}

	}



/* &<@anonymous:1942> */
	obj_t BGl_z62zc3z04anonymousza31942ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4096, obj_t BgL_xz00_4102)
	{
		{	/* Eval/evmeaning.scm 1248 */
			{	/* Eval/evmeaning.scm 1251 */
				obj_t BgL_codez00_4097;
				long BgL_lfz00_4098;
				obj_t BgL_stackz00_4099;
				obj_t BgL_bodyz00_4100;
				obj_t BgL_denvz00_4101;

				BgL_codez00_4097 = ((obj_t) PROCEDURE_REF(BgL_envz00_4096, (int) (0L)));
				BgL_lfz00_4098 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4096, (int) (1L)));
				BgL_stackz00_4099 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4096, (int) (2L)));
				BgL_bodyz00_4100 = PROCEDURE_REF(BgL_envz00_4096, (int) (3L));
				BgL_denvz00_4101 = ((obj_t) PROCEDURE_REF(BgL_envz00_4096, (int) (4L)));
				return
					BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4100,
					BGl_evmeaningzd2pushzd2fxargsz00zz__evmeaningz00(BgL_xz00_4102,
						BgL_codez00_4097, BgL_xz00_4102, BINT(BgL_lfz00_4098),
						BgL_stackz00_4099), BgL_denvz00_4101);
			}
		}

	}



/* &<@anonymous:1944> */
	obj_t BGl_z62zc3z04anonymousza31944ze3ze5zz__evmeaningz00(obj_t
		BgL_envz00_4103, obj_t BgL_xz00_4109)
	{
		{	/* Eval/evmeaning.scm 1262 */
			{	/* Eval/evmeaning.scm 1265 */
				obj_t BgL_codez00_4104;
				long BgL_lfz00_4105;
				obj_t BgL_stackz00_4106;
				obj_t BgL_bodyz00_4107;
				obj_t BgL_denvz00_4108;

				BgL_codez00_4104 = ((obj_t) PROCEDURE_REF(BgL_envz00_4103, (int) (0L)));
				BgL_lfz00_4105 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4103, (int) (1L)));
				BgL_stackz00_4106 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4103, (int) (2L)));
				BgL_bodyz00_4107 = PROCEDURE_REF(BgL_envz00_4103, (int) (3L));
				BgL_denvz00_4108 = ((obj_t) PROCEDURE_REF(BgL_envz00_4103, (int) (4L)));
				return
					BGl_evmeaningz00zz__evmeaningz00(BgL_bodyz00_4107,
					BGl_evmeaningzd2pushzd2vaargsz00zz__evmeaningz00(BgL_xz00_4109,
						BgL_codez00_4104, BgL_xz00_4109, BINT(BgL_lfz00_4105),
						BgL_stackz00_4106), BgL_denvz00_4108);
			}
		}

	}



/* evmeaning-push-fxargs */
	obj_t BGl_evmeaningzd2pushzd2fxargsz00zz__evmeaningz00(obj_t BgL_namez00_196,
		obj_t BgL_locz00_197, obj_t BgL_actualsz00_198, obj_t BgL_numz00_199,
		obj_t BgL_stackz00_200)
	{
		{	/* Eval/evmeaning.scm 1274 */
			return
				BGl__loop_ze71ze7zz__evmeaningz00(BgL_stackz00_200, BgL_numz00_199,
				BgL_namez00_196, BgL_locz00_197, BgL_actualsz00_198, BgL_actualsz00_198,
				BgL_numz00_199);
		}

	}



/* _loop_~1 */
	obj_t BGl__loop_ze71ze7zz__evmeaningz00(obj_t BgL_stackz00_4119,
		obj_t BgL_numz00_4118, obj_t BgL_namez00_4117, obj_t BgL_locz00_4116,
		obj_t BgL_actualsz00_4115, obj_t BgL_az00_2401, obj_t BgL_nz00_2402)
	{
		{	/* Eval/evmeaning.scm 1275 */
			if (((long) CINT(BgL_nz00_2402) == 0L))
				{	/* Eval/evmeaning.scm 1278 */
					if (NULLP(BgL_az00_2401))
						{	/* Eval/evmeaning.scm 1279 */
							return BgL_stackz00_4119;
						}
					else
						{	/* Eval/evmeaning.scm 1280 */
							long BgL_arg1952z00_2406;

							BgL_arg1952z00_2406 = bgl_list_length(BgL_actualsz00_4115);
							return
								BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_4116,
								BgL_namez00_4117, (int) (BgL_arg1952z00_2406),
								CINT(BgL_numz00_4118));
				}}
			else
				{	/* Eval/evmeaning.scm 1278 */
					if (NULLP(BgL_az00_2401))
						{	/* Eval/evmeaning.scm 1283 */
							long BgL_arg1954z00_2408;

							BgL_arg1954z00_2408 = bgl_list_length(BgL_actualsz00_4115);
							return
								BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_4116,
								BgL_namez00_4117, (int) (BgL_arg1954z00_2408),
								CINT(BgL_numz00_4118));
						}
					else
						{	/* Eval/evmeaning.scm 1285 */
							obj_t BgL_arg1955z00_2409;
							obj_t BgL_arg1956z00_2410;

							BgL_arg1955z00_2409 = CAR(((obj_t) BgL_az00_2401));
							{	/* Eval/evmeaning.scm 1285 */
								obj_t BgL_arg1957z00_2411;
								long BgL_arg1958z00_2412;

								BgL_arg1957z00_2411 = CDR(((obj_t) BgL_az00_2401));
								BgL_arg1958z00_2412 = ((long) CINT(BgL_nz00_2402) - 1L);
								BgL_arg1956z00_2410 =
									BGl__loop_ze71ze7zz__evmeaningz00(BgL_stackz00_4119,
									BgL_numz00_4118, BgL_namez00_4117, BgL_locz00_4116,
									BgL_actualsz00_4115, BgL_arg1957z00_2411,
									BINT(BgL_arg1958z00_2412));
							}
							return MAKE_YOUNG_PAIR(BgL_arg1955z00_2409, BgL_arg1956z00_2410);
						}
				}
		}

	}



/* evmeaning-push-vaargs */
	obj_t BGl_evmeaningzd2pushzd2vaargsz00zz__evmeaningz00(obj_t BgL_namez00_201,
		obj_t BgL_locz00_202, obj_t BgL_actualsz00_203, obj_t BgL_numz00_204,
		obj_t BgL_stackz00_205)
	{
		{	/* Eval/evmeaning.scm 1290 */
			return
				BGl__loop_ze70ze7zz__evmeaningz00(BgL_stackz00_205, BgL_numz00_204,
				BgL_namez00_201, BgL_locz00_202, BgL_actualsz00_203, BgL_actualsz00_203,
				BgL_numz00_204);
		}

	}



/* _loop_~0 */
	obj_t BGl__loop_ze70ze7zz__evmeaningz00(obj_t BgL_stackz00_4114,
		obj_t BgL_numz00_4113, obj_t BgL_namez00_4112, obj_t BgL_locz00_4111,
		obj_t BgL_actualsz00_4110, obj_t BgL_az00_2415, obj_t BgL_nz00_2416)
	{
		{	/* Eval/evmeaning.scm 1291 */
			if (((long) CINT(BgL_nz00_2416) == -1L))
				{	/* Eval/evmeaning.scm 1294 */
					return MAKE_YOUNG_PAIR(BgL_az00_2415, BgL_stackz00_4114);
				}
			else
				{	/* Eval/evmeaning.scm 1294 */
					if (NULLP(BgL_az00_2415))
						{	/* Eval/evmeaning.scm 1297 */
							long BgL_arg1962z00_2420;

							BgL_arg1962z00_2420 = bgl_list_length(BgL_actualsz00_4110);
							return
								BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_4111,
								BgL_namez00_4112, (int) (BgL_arg1962z00_2420),
								CINT(BgL_numz00_4113));
						}
					else
						{	/* Eval/evmeaning.scm 1299 */
							obj_t BgL_arg1963z00_2421;
							obj_t BgL_arg1964z00_2422;

							BgL_arg1963z00_2421 = CAR(((obj_t) BgL_az00_2415));
							{	/* Eval/evmeaning.scm 1299 */
								obj_t BgL_arg1965z00_2423;
								long BgL_arg1966z00_2424;

								BgL_arg1965z00_2423 = CDR(((obj_t) BgL_az00_2415));
								BgL_arg1966z00_2424 = ((long) CINT(BgL_nz00_2416) + 1L);
								BgL_arg1964z00_2422 =
									BGl__loop_ze70ze7zz__evmeaningz00(BgL_stackz00_4114,
									BgL_numz00_4113, BgL_namez00_4112, BgL_locz00_4111,
									BgL_actualsz00_4110, BgL_arg1965z00_2423,
									BINT(BgL_arg1966z00_2424));
							}
							return MAKE_YOUNG_PAIR(BgL_arg1963z00_2421, BgL_arg1964z00_2422);
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evmeaningz00(void)
	{
		{	/* Eval/evmeaning.scm 14 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
			return BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string2344z00zz__evmeaningz00));
		}

	}

#ifdef __cplusplus
}
#endif
