/*===========================================================================*/
/*   (Eval/expdargs.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdargs.scm -indent -o objs/obj_u/Eval/expdargs.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_ARGS_TYPE_DEFINITIONS
#define BGL___EXPANDER_ARGS_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_ARGS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_symbol2560z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2480z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2562z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2482z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2564z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2484z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2argszd2parsez62zz__expander_argsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2566z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2486z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2488z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2490z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2492z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2494z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2576z00zz__expander_argsz00 = BUNSPEC;
	static obj_t
		BGl_fetchzd2optionzd2embedzd2argumentzd2zz__expander_argsz00(obj_t);
	static obj_t BGl_symbol2496z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2498z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_argsz00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	static bool_t BGl_helpzd2messagezf3z21zz__expander_argsz00(obj_t);
	static obj_t BGl_dozd2expandzd2argszd2parsezd2zz__expander_argsz00(obj_t,
		obj_t);
	static obj_t BGl_bindzd2optionz12zc0zz__expander_argsz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_concatze70ze7zz__expander_argsz00(obj_t);
	static obj_t BGl_makezd2simplezd2optzd2parserzd2zz__expander_argsz00(obj_t,
		obj_t);
	extern obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2argszd2parsez00zz__expander_argsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_argszd2parsezd2usagez00zz__expander_argsz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_argsz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__expander_argsz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_argsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_argsz00(void);
	static obj_t BGl_makezd2multiplezd2optzd2parserzd2zz__expander_argsz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__expander_argsz00(void);
	static bool_t BGl_synopsiszf3zf3zz__expander_argsz00(obj_t);
	static obj_t BGl_makezd2optzd2parserz00zz__expander_argsz00(obj_t, obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32107ze3ze5zz__expander_argsz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2500z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2502z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2504z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2506z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2508z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_bindzd2optionzd2argumentsz00zz__expander_argsz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_makezd2parserzd2zz__expander_argsz00(obj_t, obj_t);
	static obj_t BGl_symbol2510z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2512z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2514z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__expander_argsz00(void);
	static obj_t BGl_symbol2516z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2518z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_fetchzd2optionzd2argumentsz00zz__expander_argsz00(obj_t);
	static obj_t BGl_z62argszd2parsezd2usagez62zz__expander_argsz00(obj_t, obj_t);
	static obj_t BGl_symbol2520z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2522z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2524z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2526z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2528z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__expander_argsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_loopze71ze7zz__expander_argsz00(obj_t, obj_t);
	static obj_t BGl_loopze72ze7zz__expander_argsz00(obj_t, obj_t);
	static obj_t BGl_makezd2helpzd2zz__expander_argsz00(obj_t);
	static obj_t BGl_symbol2530z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_makezd2synopsiszd2namez00zz__expander_argsz00(obj_t);
	static obj_t BGl_symbol2532z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2535z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_symbol2537z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_symbol2462z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2544z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_symbol2464z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2546z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2466z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_fetchzd2argumentzd2namez00zz__expander_argsz00(obj_t, obj_t);
	static obj_t BGl_symbol2548z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2468z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2470z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2552z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2472z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2554z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2474z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2556z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2558z00zz__expander_argsz00 = BUNSPEC;
	static obj_t BGl_symbol2478z00zz__expander_argsz00 = BUNSPEC;
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_argszd2parsezd2usagezd2envzd2zz__expander_argsz00,
		BgL_bgl_za762argsza7d2parseza72580za7,
		BGl_z62argszd2parsezd2usagez62zz__expander_argsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2501z00zz__expander_argsz00,
		BgL_bgl_string2501za700za7za7_2581za7, "list?", 5);
	      DEFINE_STRING(BGl_string2503z00zz__expander_argsz00,
		BgL_bgl_string2503za700za7za7_2582za7, "not", 3);
	      DEFINE_STRING(BGl_string2505z00zz__expander_argsz00,
		BgL_bgl_string2505za700za7za7_2583za7, "Illegal argument list", 21);
	      DEFINE_STRING(BGl_string2507z00zz__expander_argsz00,
		BgL_bgl_string2507za700za7za7_2584za7, "nv", 2);
	      DEFINE_STRING(BGl_string2509z00zz__expander_argsz00,
		BgL_bgl_string2509za700za7za7_2585za7, "na*", 3);
	      DEFINE_STRING(BGl_string2511z00zz__expander_argsz00,
		BgL_bgl_string2511za700za7za7_2586za7, "action", 6);
	      DEFINE_STRING(BGl_string2513z00zz__expander_argsz00,
		BgL_bgl_string2513za700za7za7_2587za7, "next", 4);
	      DEFINE_STRING(BGl_string2515z00zz__expander_argsz00,
		BgL_bgl_string2515za700za7za7_2588za7, "loop", 4);
	      DEFINE_STRING(BGl_string2517z00zz__expander_argsz00,
		BgL_bgl_string2517za700za7za7_2589za7, "fail", 4);
	      DEFINE_STRING(BGl_string2519z00zz__expander_argsz00,
		BgL_bgl_string2519za700za7za7_2590za7, "cdr", 3);
	      DEFINE_STRING(BGl_string2521z00zz__expander_argsz00,
		BgL_bgl_string2521za700za7za7_2591za7, "liip", 4);
	      DEFINE_STRING(BGl_string2523z00zz__expander_argsz00,
		BgL_bgl_string2523za700za7za7_2592za7, "case", 4);
	      DEFINE_STRING(BGl_string2525z00zz__expander_argsz00,
		BgL_bgl_string2525za700za7za7_2593za7, "multiple-value-bind", 19);
	      DEFINE_STRING(BGl_string2527z00zz__expander_argsz00,
		BgL_bgl_string2527za700za7za7_2594za7, "let", 3);
	      DEFINE_STRING(BGl_string2529z00zz__expander_argsz00,
		BgL_bgl_string2529za700za7za7_2595za7, "let*", 4);
	      DEFINE_STRING(BGl_string2531z00zz__expander_argsz00,
		BgL_bgl_string2531za700za7za7_2596za7, "help", 4);
	      DEFINE_STRING(BGl_string2533z00zz__expander_argsz00,
		BgL_bgl_string2533za700za7za7_2597za7, "synopsis", 8);
	      DEFINE_STRING(BGl_string2534z00zz__expander_argsz00,
		BgL_bgl_string2534za700za7za7_2598za7, "Illegal help message", 20);
	      DEFINE_STRING(BGl_string2536z00zz__expander_argsz00,
		BgL_bgl_string2536za700za7za7_2599za7, "section", 7);
	      DEFINE_STRING(BGl_string2456z00zz__expander_argsz00,
		BgL_bgl_string2456za700za7za7_2600za7, "args-parse", 10);
	      DEFINE_STRING(BGl_string2538z00zz__expander_argsz00,
		BgL_bgl_string2538za700za7za7_2601za7, "else", 4);
	      DEFINE_STRING(BGl_string2457z00zz__expander_argsz00,
		BgL_bgl_string2457za700za7za7_2602za7, "Illegal syntax", 14);
	      DEFINE_STRING(BGl_string2539z00zz__expander_argsz00,
		BgL_bgl_string2539za700za7za7_2603za7, " ", 1);
	      DEFINE_STRING(BGl_string2458z00zz__expander_argsz00,
		BgL_bgl_string2458za700za7za7_2604za7,
		"/tmp/bigloo/runtime/Eval/expdargs.scm", 37);
	      DEFINE_STRING(BGl_string2459z00zz__expander_argsz00,
		BgL_bgl_string2459za700za7za7_2605za7, "&expand-args-parse", 18);
	      DEFINE_STRING(BGl_string2540z00zz__expander_argsz00,
		BgL_bgl_string2540za700za7za7_2606za7, "Illegal options", 15);
	      DEFINE_STRING(BGl_string2541z00zz__expander_argsz00,
		BgL_bgl_string2541za700za7za7_2607za7, "Illegal clause", 14);
	      DEFINE_STRING(BGl_string2460z00zz__expander_argsz00,
		BgL_bgl_string2460za700za7za7_2608za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2542z00zz__expander_argsz00,
		BgL_bgl_string2542za700za7za7_2609za7, "", 0);
	      DEFINE_STRING(BGl_string2461z00zz__expander_argsz00,
		BgL_bgl_string2461za700za7za7_2610za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2543z00zz__expander_argsz00,
		BgL_bgl_string2543za700za7za7_2611za7, ",", 1);
	      DEFINE_STRING(BGl_string2463z00zz__expander_argsz00,
		BgL_bgl_string2463za700za7za7_2612za7, "v", 1);
	      DEFINE_STRING(BGl_string2545z00zz__expander_argsz00,
		BgL_bgl_string2545za700za7za7_2613za7, "begin", 5);
	      DEFINE_STRING(BGl_string2465z00zz__expander_argsz00,
		BgL_bgl_string2465za700za7za7_2614za7, "a", 1);
	      DEFINE_STRING(BGl_string2547z00zz__expander_argsz00,
		BgL_bgl_string2547za700za7za7_2615za7, "pair?", 5);
	      DEFINE_STRING(BGl_string2467z00zz__expander_argsz00,
		BgL_bgl_string2467za700za7za7_2616za7, "null?", 5);
	      DEFINE_STRING(BGl_string2549z00zz__expander_argsz00,
		BgL_bgl_string2549za700za7za7_2617za7, "rest", 4);
	      DEFINE_STRING(BGl_string2469z00zz__expander_argsz00,
		BgL_bgl_string2469za700za7za7_2618za7, "end", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2argszd2parsezd2envzd2zz__expander_argsz00,
		BgL_bgl_za762expandza7d2args2619z00,
		BGl_z62expandzd2argszd2parsez62zz__expander_argsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2550z00zz__expander_argsz00,
		BgL_bgl_string2550za700za7za7_2620za7, "  ", 2);
	      DEFINE_STRING(BGl_string2551z00zz__expander_argsz00,
		BgL_bgl_string2551za700za7za7_2621za7, " -- Option overridden:", 22);
	      DEFINE_STRING(BGl_string2471z00zz__expander_argsz00,
		BgL_bgl_string2471za700za7za7_2622za7, "quote", 5);
	      DEFINE_STRING(BGl_string2553z00zz__expander_argsz00,
		BgL_bgl_string2553za700za7za7_2623za7, "substring=?", 11);
	      DEFINE_STRING(BGl_string2473z00zz__expander_argsz00,
		BgL_bgl_string2473za700za7za7_2624za7, "values", 6);
	      DEFINE_STRING(BGl_string2555z00zz__expander_argsz00,
		BgL_bgl_string2555za700za7za7_2625za7, "and", 3);
	      DEFINE_STRING(BGl_string2475z00zz__expander_argsz00,
		BgL_bgl_string2475za700za7za7_2626za7, "car", 3);
	      DEFINE_STRING(BGl_string2557z00zz__expander_argsz00,
		BgL_bgl_string2557za700za7za7_2627za7, "string-length", 13);
	      DEFINE_STRING(BGl_string2476z00zz__expander_argsz00,
		BgL_bgl_string2476za700za7za7_2628za7, "see -help", 9);
	      DEFINE_STRING(BGl_string2477z00zz__expander_argsz00,
		BgL_bgl_string2477za700za7za7_2629za7, "Illegal option", 14);
	      DEFINE_STRING(BGl_string2559z00zz__expander_argsz00,
		BgL_bgl_string2559za700za7za7_2630za7, "substring", 9);
	      DEFINE_STRING(BGl_string2479z00zz__expander_argsz00,
		BgL_bgl_string2479za700za7za7_2631za7, "error", 5);
	      DEFINE_STRING(BGl_string2561z00zz__expander_argsz00,
		BgL_bgl_string2561za700za7za7_2632za7, "the-remaining-args", 18);
	      DEFINE_STRING(BGl_string2481z00zz__expander_argsz00,
		BgL_bgl_string2481za700za7za7_2633za7, "if", 2);
	      DEFINE_STRING(BGl_string2563z00zz__expander_argsz00,
		BgL_bgl_string2563za700za7za7_2634za7, "string=?", 8);
	      DEFINE_STRING(BGl_string2483z00zz__expander_argsz00,
		BgL_bgl_string2483za700za7za7_2635za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2565z00zz__expander_argsz00,
		BgL_bgl_string2565za700za7za7_2636za7, "cond", 4);
	      DEFINE_STRING(BGl_string2485z00zz__expander_argsz00,
		BgL_bgl_string2485za700za7za7_2637za7, "unquote", 7);
	      DEFINE_STRING(BGl_string2567z00zz__expander_argsz00,
		BgL_bgl_string2567za700za7za7_2638za7, "or", 2);
	      DEFINE_STRING(BGl_string2568z00zz__expander_argsz00,
		BgL_bgl_string2568za700za7za7_2639za7, "missing argument", 16);
	      DEFINE_STRING(BGl_string2487z00zz__expander_argsz00,
		BgL_bgl_string2487za700za7za7_2640za7, "quasiquote", 10);
	      DEFINE_STRING(BGl_string2569z00zz__expander_argsz00,
		BgL_bgl_string2569za700za7za7_2641za7, "Illegal option argument `", 25);
	      DEFINE_STRING(BGl_string2489z00zz__expander_argsz00,
		BgL_bgl_string2489za700za7za7_2642za7, "args-parse-usage", 16);
	      DEFINE_STRING(BGl_string2570z00zz__expander_argsz00,
		BgL_bgl_string2570za700za7za7_2643za7, "'", 1);
	      DEFINE_STRING(BGl_string2571z00zz__expander_argsz00,
		BgL_bgl_string2571za700za7za7_2644za7, "Illegal option argument", 23);
	      DEFINE_STRING(BGl_string2572z00zz__expander_argsz00,
		BgL_bgl_string2572za700za7za7_2645za7, "&args-parse-usage", 17);
	      DEFINE_STRING(BGl_string2491z00zz__expander_argsz00,
		BgL_bgl_string2491za700za7za7_2646za7, "list", 4);
	      DEFINE_STRING(BGl_string2573z00zz__expander_argsz00,
		BgL_bgl_string2573za700za7za7_2647za7, "(", 1);
	      DEFINE_STRING(BGl_string2574z00zz__expander_argsz00,
		BgL_bgl_string2574za700za7za7_2648za7, "   ", 3);
	      DEFINE_STRING(BGl_string2493z00zz__expander_argsz00,
		BgL_bgl_string2493za700za7za7_2649za7, "p*", 2);
	      DEFINE_STRING(BGl_string2575z00zz__expander_argsz00,
		BgL_bgl_string2575za700za7za7_2650za7, ":", 1);
	      DEFINE_STRING(BGl_string2495z00zz__expander_argsz00,
		BgL_bgl_string2495za700za7za7_2651za7, "a*", 2);
	      DEFINE_STRING(BGl_string2577z00zz__expander_argsz00,
		BgL_bgl_string2577za700za7za7_2652za7, "usage-done", 10);
	      DEFINE_STRING(BGl_string2578z00zz__expander_argsz00,
		BgL_bgl_string2578za700za7za7_2653za7, ")", 1);
	      DEFINE_STRING(BGl_string2497z00zz__expander_argsz00,
		BgL_bgl_string2497za700za7za7_2654za7, "cons", 4);
	      DEFINE_STRING(BGl_string2579z00zz__expander_argsz00,
		BgL_bgl_string2579za700za7za7_2655za7, "__expander_args", 15);
	      DEFINE_STRING(BGl_string2499z00zz__expander_argsz00,
		BgL_bgl_string2499za700za7za7_2656za7, "set!", 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2560z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2480z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2562z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2482z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2564z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2484z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2566z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2486z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2488z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2490z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2492z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2494z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2576z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2496z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2498z00zz__expander_argsz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2500z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2502z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2504z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2506z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2508z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2510z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2512z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2514z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2516z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2518z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2520z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2522z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2524z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2526z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2528z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2530z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2532z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2535z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2537z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2462z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2544z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2464z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2546z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2466z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2548z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2468z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2470z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2552z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2472z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2554z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2474z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2556z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2558z00zz__expander_argsz00));
		     ADD_ROOT((void *) (&BGl_symbol2478z00zz__expander_argsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_argsz00(long
		BgL_checksumz00_3288, char *BgL_fromz00_3289)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_argsz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_argsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_argsz00();
					BGl_cnstzd2initzd2zz__expander_argsz00();
					BGl_importedzd2moduleszd2initz00zz__expander_argsz00();
					return BGl_methodzd2initzd2zz__expander_argsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_argsz00(void)
	{
		{	/* Eval/expdargs.scm 19 */
			BGl_symbol2462z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2463z00zz__expander_argsz00);
			BGl_symbol2464z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2465z00zz__expander_argsz00);
			BGl_symbol2466z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2467z00zz__expander_argsz00);
			BGl_symbol2468z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2469z00zz__expander_argsz00);
			BGl_symbol2470z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2471z00zz__expander_argsz00);
			BGl_symbol2472z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2473z00zz__expander_argsz00);
			BGl_symbol2474z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2475z00zz__expander_argsz00);
			BGl_symbol2478z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2479z00zz__expander_argsz00);
			BGl_symbol2480z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2481z00zz__expander_argsz00);
			BGl_symbol2482z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2483z00zz__expander_argsz00);
			BGl_symbol2484z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2485z00zz__expander_argsz00);
			BGl_symbol2486z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2487z00zz__expander_argsz00);
			BGl_symbol2488z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2489z00zz__expander_argsz00);
			BGl_symbol2490z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2491z00zz__expander_argsz00);
			BGl_symbol2492z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2493z00zz__expander_argsz00);
			BGl_symbol2494z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2495z00zz__expander_argsz00);
			BGl_symbol2496z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2497z00zz__expander_argsz00);
			BGl_symbol2498z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2499z00zz__expander_argsz00);
			BGl_symbol2500z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2501z00zz__expander_argsz00);
			BGl_symbol2502z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2503z00zz__expander_argsz00);
			BGl_symbol2504z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2456z00zz__expander_argsz00);
			BGl_symbol2506z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2507z00zz__expander_argsz00);
			BGl_symbol2508z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2509z00zz__expander_argsz00);
			BGl_symbol2510z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2511z00zz__expander_argsz00);
			BGl_symbol2512z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2513z00zz__expander_argsz00);
			BGl_symbol2514z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2515z00zz__expander_argsz00);
			BGl_symbol2516z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2517z00zz__expander_argsz00);
			BGl_symbol2518z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2519z00zz__expander_argsz00);
			BGl_symbol2520z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2521z00zz__expander_argsz00);
			BGl_symbol2522z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2523z00zz__expander_argsz00);
			BGl_symbol2524z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2525z00zz__expander_argsz00);
			BGl_symbol2526z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2527z00zz__expander_argsz00);
			BGl_symbol2528z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2529z00zz__expander_argsz00);
			BGl_symbol2530z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2531z00zz__expander_argsz00);
			BGl_symbol2532z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2533z00zz__expander_argsz00);
			BGl_symbol2535z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2536z00zz__expander_argsz00);
			BGl_symbol2537z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2538z00zz__expander_argsz00);
			BGl_symbol2544z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2545z00zz__expander_argsz00);
			BGl_symbol2546z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2547z00zz__expander_argsz00);
			BGl_symbol2548z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2549z00zz__expander_argsz00);
			BGl_symbol2552z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2553z00zz__expander_argsz00);
			BGl_symbol2554z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2555z00zz__expander_argsz00);
			BGl_symbol2556z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2557z00zz__expander_argsz00);
			BGl_symbol2558z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2559z00zz__expander_argsz00);
			BGl_symbol2560z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2561z00zz__expander_argsz00);
			BGl_symbol2562z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2563z00zz__expander_argsz00);
			BGl_symbol2564z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2565z00zz__expander_argsz00);
			BGl_symbol2566z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2567z00zz__expander_argsz00);
			return (BGl_symbol2576z00zz__expander_argsz00 =
				bstring_to_symbol(BGl_string2577z00zz__expander_argsz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_argsz00(void)
	{
		{	/* Eval/expdargs.scm 19 */
			return bgl_gc_roots_register();
		}

	}



/* expand-args-parse */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2argszd2parsez00zz__expander_argsz00(obj_t
		BgL_xz00_6, obj_t BgL_ez00_7)
	{
		{	/* Eval/expdargs.scm 76 */
			if (NULLP(BgL_xz00_6))
				{	/* Eval/expdargs.scm 77 */
					return
						BGl_expandzd2errorzd2zz__expandz00
						(BGl_string2456z00zz__expander_argsz00,
						BGl_string2457z00zz__expander_argsz00, BgL_xz00_6);
				}
			else
				{	/* Eval/expdargs.scm 77 */
					obj_t BgL_cdrzd2105zd2_1159;

					BgL_cdrzd2105zd2_1159 = CDR(((obj_t) BgL_xz00_6));
					if (PAIRP(BgL_cdrzd2105zd2_1159))
						{	/* Eval/expdargs.scm 77 */
							bool_t BgL_test2660z00_3354;

							{	/* Eval/expdargs.scm 77 */
								obj_t BgL_tmpz00_3355;

								BgL_tmpz00_3355 = CDR(BgL_cdrzd2105zd2_1159);
								BgL_test2660z00_3354 = PAIRP(BgL_tmpz00_3355);
							}
							if (BgL_test2660z00_3354)
								{	/* Eval/expdargs.scm 79 */
									obj_t BgL_arg1272z00_2612;

									BgL_arg1272z00_2612 =
										BGl_dozd2expandzd2argszd2parsezd2zz__expander_argsz00
										(BgL_xz00_6, BgL_ez00_7);
									return BGL_PROCEDURE_CALL2(BgL_ez00_7, BgL_arg1272z00_2612,
										BgL_ez00_7);
								}
							else
								{	/* Eval/expdargs.scm 77 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string2456z00zz__expander_argsz00,
										BGl_string2457z00zz__expander_argsz00, BgL_xz00_6);
								}
						}
					else
						{	/* Eval/expdargs.scm 77 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string2456z00zz__expander_argsz00,
								BGl_string2457z00zz__expander_argsz00, BgL_xz00_6);
						}
				}
		}

	}



/* &expand-args-parse */
	obj_t BGl_z62expandzd2argszd2parsez62zz__expander_argsz00(obj_t
		BgL_envz00_3199, obj_t BgL_xz00_3200, obj_t BgL_ez00_3201)
	{
		{	/* Eval/expdargs.scm 76 */
			{	/* Eval/expdargs.scm 77 */
				obj_t BgL_auxz00_3373;
				obj_t BgL_auxz00_3366;

				if (PROCEDUREP(BgL_ez00_3201))
					{	/* Eval/expdargs.scm 77 */
						BgL_auxz00_3373 = BgL_ez00_3201;
					}
				else
					{
						obj_t BgL_auxz00_3376;

						BgL_auxz00_3376 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2458z00zz__expander_argsz00, BINT(2941L),
							BGl_string2459z00zz__expander_argsz00,
							BGl_string2461z00zz__expander_argsz00, BgL_ez00_3201);
						FAILURE(BgL_auxz00_3376, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_3200))
					{	/* Eval/expdargs.scm 77 */
						BgL_auxz00_3366 = BgL_xz00_3200;
					}
				else
					{
						obj_t BgL_auxz00_3369;

						BgL_auxz00_3369 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2458z00zz__expander_argsz00, BINT(2941L),
							BGl_string2459z00zz__expander_argsz00,
							BGl_string2460z00zz__expander_argsz00, BgL_xz00_3200);
						FAILURE(BgL_auxz00_3369, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2argszd2parsez00zz__expander_argsz00(BgL_auxz00_3366,
					BgL_auxz00_3373);
			}
		}

	}



/* do-expand-args-parse */
	obj_t BGl_dozd2expandzd2argszd2parsezd2zz__expander_argsz00(obj_t BgL_xz00_8,
		obj_t BgL_ez00_9)
	{
		{	/* Eval/expdargs.scm 86 */
			{	/* Eval/expdargs.scm 87 */
				obj_t BgL_expz00_1165;

				BgL_expz00_1165 = CAR(CDR(BgL_xz00_8));
				{	/* Eval/expdargs.scm 87 */
					obj_t BgL_clausesz00_1166;

					BgL_clausesz00_1166 = CDR(CDR(BgL_xz00_8));
					{	/* Eval/expdargs.scm 88 */
						obj_t BgL_otablez00_1167;

						{	/* Eval/expdargs.scm 89 */
							obj_t BgL_list1473z00_1380;

							BgL_list1473z00_1380 = MAKE_YOUNG_PAIR(BINT(20L), BNIL);
							BgL_otablez00_1167 =
								BGl_makezd2hashtablezd2zz__hashz00(BgL_list1473z00_1380);
						}
						{	/* Eval/expdargs.scm 89 */
							obj_t BgL_parsersz00_1168;

							if (NULLP(BgL_clausesz00_1166))
								{	/* Eval/expdargs.scm 90 */
									BgL_parsersz00_1168 = BNIL;
								}
							else
								{	/* Eval/expdargs.scm 90 */
									obj_t BgL_head1091z00_1369;

									BgL_head1091z00_1369 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1089z00_2640;
										obj_t BgL_tail1092z00_2641;

										BgL_l1089z00_2640 = BgL_clausesz00_1166;
										BgL_tail1092z00_2641 = BgL_head1091z00_1369;
									BgL_zc3z04anonymousza31467ze3z87_2639:
										if (NULLP(BgL_l1089z00_2640))
											{	/* Eval/expdargs.scm 90 */
												BgL_parsersz00_1168 = CDR(BgL_head1091z00_1369);
											}
										else
											{	/* Eval/expdargs.scm 90 */
												obj_t BgL_newtail1093z00_2648;

												{	/* Eval/expdargs.scm 90 */
													obj_t BgL_arg1472z00_2649;

													{	/* Eval/expdargs.scm 90 */
														obj_t BgL_cz00_2650;

														BgL_cz00_2650 = CAR(((obj_t) BgL_l1089z00_2640));
														BgL_arg1472z00_2649 =
															BGl_makezd2parserzd2zz__expander_argsz00
															(BgL_cz00_2650, BgL_otablez00_1167);
													}
													BgL_newtail1093z00_2648 =
														MAKE_YOUNG_PAIR(BgL_arg1472z00_2649, BNIL);
												}
												SET_CDR(BgL_tail1092z00_2641, BgL_newtail1093z00_2648);
												{	/* Eval/expdargs.scm 90 */
													obj_t BgL_arg1469z00_2651;

													BgL_arg1469z00_2651 =
														CDR(((obj_t) BgL_l1089z00_2640));
													{
														obj_t BgL_tail1092z00_3402;
														obj_t BgL_l1089z00_3401;

														BgL_l1089z00_3401 = BgL_arg1469z00_2651;
														BgL_tail1092z00_3402 = BgL_newtail1093z00_2648;
														BgL_tail1092z00_2641 = BgL_tail1092z00_3402;
														BgL_l1089z00_2640 = BgL_l1089z00_3401;
														goto BgL_zc3z04anonymousza31467ze3z87_2639;
													}
												}
											}
									}
								}
							{	/* Eval/expdargs.scm 90 */
								obj_t BgL_lastzd2parserzd2_1169;

								{	/* Eval/expdargs.scm 91 */
									obj_t BgL_arg1444z00_1345;

									{	/* Eval/expdargs.scm 91 */
										obj_t BgL_arg1445z00_1346;
										obj_t BgL_arg1446z00_1347;

										{	/* Eval/expdargs.scm 91 */
											obj_t BgL_arg1447z00_1348;

											BgL_arg1447z00_1348 =
												MAKE_YOUNG_PAIR(BGl_symbol2462z00zz__expander_argsz00,
												BNIL);
											BgL_arg1445z00_1346 =
												MAKE_YOUNG_PAIR(BGl_symbol2464z00zz__expander_argsz00,
												BgL_arg1447z00_1348);
										}
										{	/* Eval/expdargs.scm 92 */
											obj_t BgL_arg1448z00_1349;

											{	/* Eval/expdargs.scm 92 */
												obj_t BgL_arg1449z00_1350;

												{	/* Eval/expdargs.scm 92 */
													obj_t BgL_arg1450z00_1351;
													obj_t BgL_arg1451z00_1352;

													{	/* Eval/expdargs.scm 92 */
														obj_t BgL_arg1452z00_1353;

														BgL_arg1452z00_1353 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2464z00zz__expander_argsz00, BNIL);
														BgL_arg1450z00_1351 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2466z00zz__expander_argsz00,
															BgL_arg1452z00_1353);
													}
													{	/* Eval/expdargs.scm 93 */
														obj_t BgL_arg1453z00_1354;
														obj_t BgL_arg1454z00_1355;

														{	/* Eval/expdargs.scm 93 */
															obj_t BgL_arg1455z00_1356;

															{	/* Eval/expdargs.scm 93 */
																obj_t BgL_arg1456z00_1357;
																obj_t BgL_arg1457z00_1358;

																{	/* Eval/expdargs.scm 93 */
																	obj_t BgL_arg1458z00_1359;

																	BgL_arg1458z00_1359 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2468z00zz__expander_argsz00,
																		BNIL);
																	BgL_arg1456z00_1357 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2470z00zz__expander_argsz00,
																		BgL_arg1458z00_1359);
																}
																{	/* Eval/expdargs.scm 93 */
																	obj_t BgL_arg1459z00_1360;

																	BgL_arg1459z00_1360 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2462z00zz__expander_argsz00,
																		BNIL);
																	BgL_arg1457z00_1358 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2464z00zz__expander_argsz00,
																		BgL_arg1459z00_1360);
																}
																BgL_arg1455z00_1356 =
																	MAKE_YOUNG_PAIR(BgL_arg1456z00_1357,
																	BgL_arg1457z00_1358);
															}
															BgL_arg1453z00_1354 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2472z00zz__expander_argsz00,
																BgL_arg1455z00_1356);
														}
														{	/* Eval/expdargs.scm 94 */
															obj_t BgL_arg1460z00_1361;

															{	/* Eval/expdargs.scm 94 */
																obj_t BgL_arg1461z00_1362;

																{	/* Eval/expdargs.scm 94 */
																	obj_t BgL_arg1462z00_1363;
																	obj_t BgL_arg1463z00_1364;

																	{	/* Eval/expdargs.scm 94 */
																		obj_t BgL_arg1464z00_1365;

																		BgL_arg1464z00_1365 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2464z00zz__expander_argsz00,
																			BNIL);
																		BgL_arg1462z00_1363 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2474z00zz__expander_argsz00,
																			BgL_arg1464z00_1365);
																	}
																	{	/* Eval/expdargs.scm 94 */
																		obj_t BgL_arg1465z00_1366;

																		BgL_arg1465z00_1366 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2476z00zz__expander_argsz00,
																			BNIL);
																		BgL_arg1463z00_1364 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2477z00zz__expander_argsz00,
																			BgL_arg1465z00_1366);
																	}
																	BgL_arg1461z00_1362 =
																		MAKE_YOUNG_PAIR(BgL_arg1462z00_1363,
																		BgL_arg1463z00_1364);
																}
																BgL_arg1460z00_1361 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2478z00zz__expander_argsz00,
																	BgL_arg1461z00_1362);
															}
															BgL_arg1454z00_1355 =
																MAKE_YOUNG_PAIR(BgL_arg1460z00_1361, BNIL);
														}
														BgL_arg1451z00_1352 =
															MAKE_YOUNG_PAIR(BgL_arg1453z00_1354,
															BgL_arg1454z00_1355);
													}
													BgL_arg1449z00_1350 =
														MAKE_YOUNG_PAIR(BgL_arg1450z00_1351,
														BgL_arg1451z00_1352);
												}
												BgL_arg1448z00_1349 =
													MAKE_YOUNG_PAIR(BGl_symbol2480z00zz__expander_argsz00,
													BgL_arg1449z00_1350);
											}
											BgL_arg1446z00_1347 =
												MAKE_YOUNG_PAIR(BgL_arg1448z00_1349, BNIL);
										}
										BgL_arg1444z00_1345 =
											MAKE_YOUNG_PAIR(BgL_arg1445z00_1346, BgL_arg1446z00_1347);
									}
									BgL_lastzd2parserzd2_1169 =
										MAKE_YOUNG_PAIR(BGl_symbol2482z00zz__expander_argsz00,
										BgL_arg1444z00_1345);
								}
								{	/* Eval/expdargs.scm 91 */
									obj_t BgL_descrsz00_1170;

									{	/* Eval/expdargs.scm 95 */
										obj_t BgL_hook1104z00_1314;

										BgL_hook1104z00_1314 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
										{	/* Eval/expdargs.scm 95 */
											obj_t BgL_g1105z00_1315;

											if (NULLP(BgL_clausesz00_1166))
												{	/* Eval/expdargs.scm 95 */
													BgL_g1105z00_1315 = BNIL;
												}
											else
												{	/* Eval/expdargs.scm 95 */
													obj_t BgL_head1096z00_1331;

													{	/* Eval/expdargs.scm 95 */
														obj_t BgL_arg1442z00_1343;

														{	/* Eval/expdargs.scm 95 */
															obj_t BgL_arg1443z00_1344;

															BgL_arg1443z00_1344 =
																CAR(((obj_t) BgL_clausesz00_1166));
															BgL_arg1442z00_1343 =
																BGl_makezd2helpzd2zz__expander_argsz00
																(BgL_arg1443z00_1344);
														}
														BgL_head1096z00_1331 =
															MAKE_YOUNG_PAIR(BgL_arg1442z00_1343, BNIL);
													}
													{	/* Eval/expdargs.scm 95 */
														obj_t BgL_g1099z00_1332;

														BgL_g1099z00_1332 =
															CDR(((obj_t) BgL_clausesz00_1166));
														{
															obj_t BgL_l1094z00_2675;
															obj_t BgL_tail1097z00_2676;

															BgL_l1094z00_2675 = BgL_g1099z00_1332;
															BgL_tail1097z00_2676 = BgL_head1096z00_1331;
														BgL_zc3z04anonymousza31437ze3z87_2674:
															if (NULLP(BgL_l1094z00_2675))
																{	/* Eval/expdargs.scm 95 */
																	BgL_g1105z00_1315 = BgL_head1096z00_1331;
																}
															else
																{	/* Eval/expdargs.scm 95 */
																	obj_t BgL_newtail1098z00_2683;

																	{	/* Eval/expdargs.scm 95 */
																		obj_t BgL_arg1440z00_2684;

																		{	/* Eval/expdargs.scm 95 */
																			obj_t BgL_arg1441z00_2685;

																			BgL_arg1441z00_2685 =
																				CAR(((obj_t) BgL_l1094z00_2675));
																			BgL_arg1440z00_2684 =
																				BGl_makezd2helpzd2zz__expander_argsz00
																				(BgL_arg1441z00_2685);
																		}
																		BgL_newtail1098z00_2683 =
																			MAKE_YOUNG_PAIR(BgL_arg1440z00_2684,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1097z00_2676,
																		BgL_newtail1098z00_2683);
																	{	/* Eval/expdargs.scm 95 */
																		obj_t BgL_arg1439z00_2686;

																		BgL_arg1439z00_2686 =
																			CDR(((obj_t) BgL_l1094z00_2675));
																		{
																			obj_t BgL_tail1097z00_3445;
																			obj_t BgL_l1094z00_3444;

																			BgL_l1094z00_3444 = BgL_arg1439z00_2686;
																			BgL_tail1097z00_3445 =
																				BgL_newtail1098z00_2683;
																			BgL_tail1097z00_2676 =
																				BgL_tail1097z00_3445;
																			BgL_l1094z00_2675 = BgL_l1094z00_3444;
																			goto
																				BgL_zc3z04anonymousza31437ze3z87_2674;
																		}
																	}
																}
														}
													}
												}
											{
												obj_t BgL_l1101z00_1317;
												obj_t BgL_h1102z00_1318;

												BgL_l1101z00_1317 = BgL_g1105z00_1315;
												BgL_h1102z00_1318 = BgL_hook1104z00_1314;
											BgL_zc3z04anonymousza31428ze3z87_1319:
												if (NULLP(BgL_l1101z00_1317))
													{	/* Eval/expdargs.scm 95 */
														BgL_descrsz00_1170 = CDR(BgL_hook1104z00_1314);
													}
												else
													{	/* Eval/expdargs.scm 95 */
														if (CBOOL(CAR(((obj_t) BgL_l1101z00_1317))))
															{	/* Eval/expdargs.scm 95 */
																obj_t BgL_nh1103z00_1323;

																{	/* Eval/expdargs.scm 95 */
																	obj_t BgL_arg1434z00_1325;

																	BgL_arg1434z00_1325 =
																		CAR(((obj_t) BgL_l1101z00_1317));
																	BgL_nh1103z00_1323 =
																		MAKE_YOUNG_PAIR(BgL_arg1434z00_1325, BNIL);
																}
																SET_CDR(BgL_h1102z00_1318, BgL_nh1103z00_1323);
																{	/* Eval/expdargs.scm 95 */
																	obj_t BgL_arg1431z00_1324;

																	BgL_arg1431z00_1324 =
																		CDR(((obj_t) BgL_l1101z00_1317));
																	{
																		obj_t BgL_h1102z00_3460;
																		obj_t BgL_l1101z00_3459;

																		BgL_l1101z00_3459 = BgL_arg1431z00_1324;
																		BgL_h1102z00_3460 = BgL_nh1103z00_1323;
																		BgL_h1102z00_1318 = BgL_h1102z00_3460;
																		BgL_l1101z00_1317 = BgL_l1101z00_3459;
																		goto BgL_zc3z04anonymousza31428ze3z87_1319;
																	}
																}
															}
														else
															{	/* Eval/expdargs.scm 95 */
																obj_t BgL_arg1435z00_1326;

																BgL_arg1435z00_1326 =
																	CDR(((obj_t) BgL_l1101z00_1317));
																{
																	obj_t BgL_l1101z00_3463;

																	BgL_l1101z00_3463 = BgL_arg1435z00_1326;
																	BgL_l1101z00_1317 = BgL_l1101z00_3463;
																	goto BgL_zc3z04anonymousza31428ze3z87_1319;
																}
															}
													}
											}
										}
									}
									{	/* Eval/expdargs.scm 95 */

										{	/* Eval/expdargs.scm 98 */
											obj_t BgL_arg1284z00_1171;

											{	/* Eval/expdargs.scm 98 */
												obj_t BgL_arg1304z00_1172;
												obj_t BgL_arg1305z00_1173;

												{	/* Eval/expdargs.scm 98 */
													obj_t BgL_arg1306z00_1174;
													obj_t BgL_arg1307z00_1175;

													{	/* Eval/expdargs.scm 98 */
														obj_t BgL_arg1308z00_1176;

														{	/* Eval/expdargs.scm 98 */
															obj_t BgL_arg1309z00_1177;

															{	/* Eval/expdargs.scm 98 */
																obj_t BgL_arg1310z00_1178;

																{	/* Eval/expdargs.scm 98 */
																	obj_t BgL_arg1311z00_1179;

																	{	/* Eval/expdargs.scm 98 */
																		obj_t BgL_arg1312z00_1180;

																		{	/* Eval/expdargs.scm 98 */
																			bool_t BgL_test2669z00_3464;

																			{
																				obj_t BgL_l1106z00_1197;

																				BgL_l1106z00_1197 = BgL_descrsz00_1170;
																			BgL_zc3z04anonymousza31321ze3z87_1198:
																				if (NULLP(BgL_l1106z00_1197))
																					{	/* Eval/expdargs.scm 98 */
																						BgL_test2669z00_3464 = ((bool_t) 0);
																					}
																				else
																					{	/* Eval/expdargs.scm 99 */
																						bool_t BgL__ortest_1108z00_1200;

																						{	/* Eval/expdargs.scm 99 */
																							obj_t BgL_fz00_1202;

																							BgL_fz00_1202 =
																								CAR(
																								((obj_t) BgL_l1106z00_1197));
																							if (PAIRP(BgL_fz00_1202))
																								{	/* Eval/expdargs.scm 100 */
																									bool_t BgL_test2672z00_3471;

																									{	/* Eval/expdargs.scm 100 */
																										obj_t BgL_tmpz00_3472;

																										BgL_tmpz00_3472 =
																											CDR(BgL_fz00_1202);
																										BgL_test2672z00_3471 =
																											PAIRP(BgL_tmpz00_3472);
																									}
																									if (BgL_test2672z00_3471)
																										{	/* Eval/expdargs.scm 100 */
																											BgL__ortest_1108z00_1200 =
																												(CAR(CDR(BgL_fz00_1202))
																												==
																												BGl_symbol2484z00zz__expander_argsz00);
																										}
																									else
																										{	/* Eval/expdargs.scm 100 */
																											BgL__ortest_1108z00_1200 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Eval/expdargs.scm 99 */
																									BgL__ortest_1108z00_1200 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL__ortest_1108z00_1200)
																							{	/* Eval/expdargs.scm 98 */
																								BgL_test2669z00_3464 =
																									BgL__ortest_1108z00_1200;
																							}
																						else
																							{	/* Eval/expdargs.scm 98 */
																								obj_t BgL_arg1322z00_1201;

																								BgL_arg1322z00_1201 =
																									CDR(
																									((obj_t) BgL_l1106z00_1197));
																								{
																									obj_t BgL_l1106z00_3481;

																									BgL_l1106z00_3481 =
																										BgL_arg1322z00_1201;
																									BgL_l1106z00_1197 =
																										BgL_l1106z00_3481;
																									goto
																										BgL_zc3z04anonymousza31321ze3z87_1198;
																								}
																							}
																					}
																			}
																			if (BgL_test2669z00_3464)
																				{	/* Eval/expdargs.scm 98 */
																					BgL_arg1312z00_1180 =
																						BGl_symbol2486z00zz__expander_argsz00;
																				}
																			else
																				{	/* Eval/expdargs.scm 98 */
																					BgL_arg1312z00_1180 =
																						BGl_symbol2470z00zz__expander_argsz00;
																				}
																		}
																		{	/* Eval/expdargs.scm 98 */
																			obj_t BgL_list1313z00_1181;

																			{	/* Eval/expdargs.scm 98 */
																				obj_t BgL_arg1314z00_1182;

																				BgL_arg1314z00_1182 =
																					MAKE_YOUNG_PAIR(BgL_descrsz00_1170,
																					BNIL);
																				BgL_list1313z00_1181 =
																					MAKE_YOUNG_PAIR(BgL_arg1312z00_1180,
																					BgL_arg1314z00_1182);
																			}
																			BgL_arg1311z00_1179 =
																				BgL_list1313z00_1181;
																		}
																	}
																	BgL_arg1310z00_1178 =
																		MAKE_YOUNG_PAIR(BgL_arg1311z00_1179, BNIL);
																}
																BgL_arg1309z00_1177 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2488z00zz__expander_argsz00,
																	BgL_arg1310z00_1178);
															}
															BgL_arg1308z00_1176 =
																MAKE_YOUNG_PAIR(BgL_arg1309z00_1177, BNIL);
														}
														BgL_arg1306z00_1174 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2488z00zz__expander_argsz00,
															BgL_arg1308z00_1176);
													}
													{	/* Eval/expdargs.scm 106 */
														obj_t BgL_arg1326z00_1208;
														obj_t BgL_arg1327z00_1209;

														{	/* Eval/expdargs.scm 106 */
															obj_t BgL_arg1328z00_1210;

															{	/* Eval/expdargs.scm 106 */
																obj_t BgL_arg1329z00_1211;

																{	/* Eval/expdargs.scm 106 */
																	obj_t BgL_arg1331z00_1212;

																	BgL_arg1331z00_1212 =
																		MAKE_YOUNG_PAIR(BgL_lastzd2parserzd2_1169,
																		BNIL);
																	BgL_arg1329z00_1211 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2490z00zz__expander_argsz00,
																		BgL_arg1331z00_1212);
																}
																BgL_arg1328z00_1210 =
																	MAKE_YOUNG_PAIR(BgL_arg1329z00_1211, BNIL);
															}
															BgL_arg1326z00_1208 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2492z00zz__expander_argsz00,
																BgL_arg1328z00_1210);
														}
														{	/* Eval/expdargs.scm 107 */
															obj_t BgL_arg1332z00_1213;

															{	/* Eval/expdargs.scm 107 */
																obj_t BgL_arg1333z00_1214;

																BgL_arg1333z00_1214 =
																	MAKE_YOUNG_PAIR(BgL_expz00_1165, BNIL);
																BgL_arg1332z00_1213 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2494z00zz__expander_argsz00,
																	BgL_arg1333z00_1214);
															}
															BgL_arg1327z00_1209 =
																MAKE_YOUNG_PAIR(BgL_arg1332z00_1213, BNIL);
														}
														BgL_arg1307z00_1175 =
															MAKE_YOUNG_PAIR(BgL_arg1326z00_1208,
															BgL_arg1327z00_1209);
													}
													BgL_arg1304z00_1172 =
														MAKE_YOUNG_PAIR(BgL_arg1306z00_1174,
														BgL_arg1307z00_1175);
												}
												{	/* Eval/expdargs.scm 108 */
													obj_t BgL_arg1334z00_1215;
													obj_t BgL_arg1335z00_1216;

													{	/* Eval/expdargs.scm 108 */
														obj_t BgL_l1114z00_1217;

														{	/* Eval/expdargs.scm 109 */
															obj_t BgL_arg1347z00_1235;

															{	/* Eval/expdargs.scm 109 */
																obj_t BgL_hook1113z00_1236;

																BgL_hook1113z00_1236 =
																	MAKE_YOUNG_PAIR(BFALSE, BNIL);
																{
																	obj_t BgL_l1110z00_1238;
																	obj_t BgL_h1111z00_1239;

																	BgL_l1110z00_1238 = BgL_parsersz00_1168;
																	BgL_h1111z00_1239 = BgL_hook1113z00_1236;
																BgL_zc3z04anonymousza31348ze3z87_1240:
																	if (NULLP(BgL_l1110z00_1238))
																		{	/* Eval/expdargs.scm 109 */
																			BgL_arg1347z00_1235 =
																				CDR(BgL_hook1113z00_1236);
																		}
																	else
																		{	/* Eval/expdargs.scm 109 */
																			bool_t BgL_test2675z00_3501;

																			{	/* Eval/expdargs.scm 109 */
																				obj_t BgL_tmpz00_3502;

																				BgL_tmpz00_3502 =
																					CAR(((obj_t) BgL_l1110z00_1238));
																				BgL_test2675z00_3501 =
																					PAIRP(BgL_tmpz00_3502);
																			}
																			if (BgL_test2675z00_3501)
																				{	/* Eval/expdargs.scm 109 */
																					obj_t BgL_nh1112z00_1244;

																					{	/* Eval/expdargs.scm 109 */
																						obj_t BgL_arg1354z00_1246;

																						BgL_arg1354z00_1246 =
																							CAR(((obj_t) BgL_l1110z00_1238));
																						BgL_nh1112z00_1244 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1354z00_1246, BNIL);
																					}
																					SET_CDR(BgL_h1111z00_1239,
																						BgL_nh1112z00_1244);
																					{	/* Eval/expdargs.scm 109 */
																						obj_t BgL_arg1352z00_1245;

																						BgL_arg1352z00_1245 =
																							CDR(((obj_t) BgL_l1110z00_1238));
																						{
																							obj_t BgL_h1111z00_3513;
																							obj_t BgL_l1110z00_3512;

																							BgL_l1110z00_3512 =
																								BgL_arg1352z00_1245;
																							BgL_h1111z00_3513 =
																								BgL_nh1112z00_1244;
																							BgL_h1111z00_1239 =
																								BgL_h1111z00_3513;
																							BgL_l1110z00_1238 =
																								BgL_l1110z00_3512;
																							goto
																								BgL_zc3z04anonymousza31348ze3z87_1240;
																						}
																					}
																				}
																			else
																				{	/* Eval/expdargs.scm 109 */
																					obj_t BgL_arg1356z00_1247;

																					BgL_arg1356z00_1247 =
																						CDR(((obj_t) BgL_l1110z00_1238));
																					{
																						obj_t BgL_l1110z00_3516;

																						BgL_l1110z00_3516 =
																							BgL_arg1356z00_1247;
																						BgL_l1110z00_1238 =
																							BgL_l1110z00_3516;
																						goto
																							BgL_zc3z04anonymousza31348ze3z87_1240;
																					}
																				}
																		}
																}
															}
															BgL_l1114z00_1217 =
																bgl_reverse_bang(BgL_arg1347z00_1235);
														}
														if (NULLP(BgL_l1114z00_1217))
															{	/* Eval/expdargs.scm 108 */
																BgL_arg1334z00_1215 = BNIL;
															}
														else
															{	/* Eval/expdargs.scm 108 */
																obj_t BgL_head1116z00_1219;

																BgL_head1116z00_1219 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1114z00_1221;
																	obj_t BgL_tail1117z00_1222;

																	BgL_l1114z00_1221 = BgL_l1114z00_1217;
																	BgL_tail1117z00_1222 = BgL_head1116z00_1219;
																BgL_zc3z04anonymousza31337ze3z87_1223:
																	if (NULLP(BgL_l1114z00_1221))
																		{	/* Eval/expdargs.scm 108 */
																			BgL_arg1334z00_1215 =
																				CDR(BgL_head1116z00_1219);
																		}
																	else
																		{	/* Eval/expdargs.scm 108 */
																			obj_t BgL_newtail1118z00_1225;

																			{	/* Eval/expdargs.scm 108 */
																				obj_t BgL_arg1340z00_1227;

																				{	/* Eval/expdargs.scm 108 */
																					obj_t BgL_pz00_1228;

																					BgL_pz00_1228 =
																						CAR(((obj_t) BgL_l1114z00_1221));
																					{	/* Eval/expdargs.scm 108 */
																						obj_t BgL_arg1341z00_1229;

																						{	/* Eval/expdargs.scm 108 */
																							obj_t BgL_arg1342z00_1230;

																							{	/* Eval/expdargs.scm 108 */
																								obj_t BgL_arg1343z00_1231;

																								{	/* Eval/expdargs.scm 108 */
																									obj_t BgL_arg1344z00_1232;

																									{	/* Eval/expdargs.scm 108 */
																										obj_t BgL_arg1346z00_1233;

																										BgL_arg1346z00_1233 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2492z00zz__expander_argsz00,
																											BNIL);
																										BgL_arg1344z00_1232 =
																											MAKE_YOUNG_PAIR
																											(BgL_pz00_1228,
																											BgL_arg1346z00_1233);
																									}
																									BgL_arg1343z00_1231 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2496z00zz__expander_argsz00,
																										BgL_arg1344z00_1232);
																								}
																								BgL_arg1342z00_1230 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1343z00_1231, BNIL);
																							}
																							BgL_arg1341z00_1229 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2492z00zz__expander_argsz00,
																								BgL_arg1342z00_1230);
																						}
																						BgL_arg1340z00_1227 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2498z00zz__expander_argsz00,
																							BgL_arg1341z00_1229);
																					}
																				}
																				BgL_newtail1118z00_1225 =
																					MAKE_YOUNG_PAIR(BgL_arg1340z00_1227,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1117z00_1222,
																				BgL_newtail1118z00_1225);
																			{	/* Eval/expdargs.scm 108 */
																				obj_t BgL_arg1339z00_1226;

																				BgL_arg1339z00_1226 =
																					CDR(((obj_t) BgL_l1114z00_1221));
																				{
																					obj_t BgL_tail1117z00_3537;
																					obj_t BgL_l1114z00_3536;

																					BgL_l1114z00_3536 =
																						BgL_arg1339z00_1226;
																					BgL_tail1117z00_3537 =
																						BgL_newtail1118z00_1225;
																					BgL_tail1117z00_1222 =
																						BgL_tail1117z00_3537;
																					BgL_l1114z00_1221 = BgL_l1114z00_3536;
																					goto
																						BgL_zc3z04anonymousza31337ze3z87_1223;
																				}
																			}
																		}
																}
															}
													}
													{	/* Eval/expdargs.scm 110 */
														obj_t BgL_arg1358z00_1250;

														{	/* Eval/expdargs.scm 110 */
															obj_t BgL_arg1359z00_1251;

															{	/* Eval/expdargs.scm 110 */
																obj_t BgL_arg1360z00_1252;
																obj_t BgL_arg1361z00_1253;

																{	/* Eval/expdargs.scm 110 */
																	obj_t BgL_arg1362z00_1254;

																	{	/* Eval/expdargs.scm 110 */
																		obj_t BgL_arg1363z00_1255;

																		{	/* Eval/expdargs.scm 110 */
																			obj_t BgL_arg1364z00_1256;

																			BgL_arg1364z00_1256 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2494z00zz__expander_argsz00,
																				BNIL);
																			BgL_arg1363z00_1255 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2500z00zz__expander_argsz00,
																				BgL_arg1364z00_1256);
																		}
																		BgL_arg1362z00_1254 =
																			MAKE_YOUNG_PAIR(BgL_arg1363z00_1255,
																			BNIL);
																	}
																	BgL_arg1360z00_1252 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2502z00zz__expander_argsz00,
																		BgL_arg1362z00_1254);
																}
																{	/* Eval/expdargs.scm 111 */
																	obj_t BgL_arg1365z00_1257;
																	obj_t BgL_arg1366z00_1258;

																	{	/* Eval/expdargs.scm 111 */
																		obj_t BgL_arg1367z00_1259;

																		{	/* Eval/expdargs.scm 111 */
																			obj_t BgL_arg1368z00_1260;
																			obj_t BgL_arg1369z00_1261;

																			{	/* Eval/expdargs.scm 111 */
																				obj_t BgL_arg1370z00_1262;

																				BgL_arg1370z00_1262 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2504z00zz__expander_argsz00,
																					BNIL);
																				BgL_arg1368z00_1260 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2470z00zz__expander_argsz00,
																					BgL_arg1370z00_1262);
																			}
																			{	/* Eval/expdargs.scm 111 */
																				obj_t BgL_arg1371z00_1263;

																				BgL_arg1371z00_1263 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2494z00zz__expander_argsz00,
																					BNIL);
																				BgL_arg1369z00_1261 =
																					MAKE_YOUNG_PAIR
																					(BGl_string2505z00zz__expander_argsz00,
																					BgL_arg1371z00_1263);
																			}
																			BgL_arg1367z00_1259 =
																				MAKE_YOUNG_PAIR(BgL_arg1368z00_1260,
																				BgL_arg1369z00_1261);
																		}
																		BgL_arg1365z00_1257 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2478z00zz__expander_argsz00,
																			BgL_arg1367z00_1259);
																	}
																	{	/* Eval/expdargs.scm 112 */
																		obj_t BgL_arg1372z00_1264;

																		{	/* Eval/expdargs.scm 112 */
																			obj_t BgL_arg1373z00_1265;

																			{	/* Eval/expdargs.scm 112 */
																				obj_t BgL_arg1375z00_1266;

																				{	/* Eval/expdargs.scm 112 */
																					obj_t BgL_arg1376z00_1267;
																					obj_t BgL_arg1377z00_1268;

																					{	/* Eval/expdargs.scm 112 */
																						obj_t BgL_arg1378z00_1269;
																						obj_t BgL_arg1379z00_1270;

																						{	/* Eval/expdargs.scm 112 */
																							obj_t BgL_arg1380z00_1271;

																							BgL_arg1380z00_1271 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2494z00zz__expander_argsz00,
																								BNIL);
																							BgL_arg1378z00_1269 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2494z00zz__expander_argsz00,
																								BgL_arg1380z00_1271);
																						}
																						{	/* Eval/expdargs.scm 113 */
																							obj_t BgL_arg1382z00_1272;

																							{	/* Eval/expdargs.scm 113 */
																								obj_t BgL_arg1383z00_1273;

																								BgL_arg1383z00_1273 =
																									MAKE_YOUNG_PAIR(BFALSE, BNIL);
																								BgL_arg1382z00_1272 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2462z00zz__expander_argsz00,
																									BgL_arg1383z00_1273);
																							}
																							BgL_arg1379z00_1270 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1382z00_1272, BNIL);
																						}
																						BgL_arg1376z00_1267 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1378z00_1269,
																							BgL_arg1379z00_1270);
																					}
																					{	/* Eval/expdargs.scm 114 */
																						obj_t BgL_arg1384z00_1274;

																						{	/* Eval/expdargs.scm 114 */
																							obj_t BgL_arg1387z00_1275;

																							{	/* Eval/expdargs.scm 114 */
																								obj_t BgL_arg1388z00_1276;

																								{	/* Eval/expdargs.scm 114 */
																									obj_t BgL_arg1389z00_1277;
																									obj_t BgL_arg1390z00_1278;

																									{	/* Eval/expdargs.scm 114 */
																										obj_t BgL_arg1391z00_1279;

																										{	/* Eval/expdargs.scm 114 */
																											obj_t BgL_arg1392z00_1280;

																											BgL_arg1392z00_1280 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2492z00zz__expander_argsz00,
																												BNIL);
																											BgL_arg1391z00_1279 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2492z00zz__expander_argsz00,
																												BgL_arg1392z00_1280);
																										}
																										BgL_arg1389z00_1277 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1391z00_1279,
																											BNIL);
																									}
																									{	/* Eval/expdargs.scm 115 */
																										obj_t BgL_arg1393z00_1281;

																										{	/* Eval/expdargs.scm 115 */
																											obj_t BgL_arg1394z00_1282;

																											{	/* Eval/expdargs.scm 115 */
																												obj_t
																													BgL_arg1395z00_1283;
																												obj_t
																													BgL_arg1396z00_1284;
																												{	/* Eval/expdargs.scm 115 */
																													obj_t
																														BgL_arg1397z00_1285;
																													{	/* Eval/expdargs.scm 115 */
																														obj_t
																															BgL_arg1399z00_1286;
																														BgL_arg1399z00_1286
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2506z00zz__expander_argsz00,
																															BNIL);
																														BgL_arg1397z00_1285
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2508z00zz__expander_argsz00,
																															BgL_arg1399z00_1286);
																													}
																													BgL_arg1395z00_1283 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2510z00zz__expander_argsz00,
																														BgL_arg1397z00_1285);
																												}
																												{	/* Eval/expdargs.scm 116 */
																													obj_t
																														BgL_arg1400z00_1287;
																													obj_t
																														BgL_arg1401z00_1288;
																													{	/* Eval/expdargs.scm 116 */
																														obj_t
																															BgL_arg1402z00_1289;
																														obj_t
																															BgL_arg1403z00_1290;
																														{	/* Eval/expdargs.scm 116 */
																															obj_t
																																BgL_arg1404z00_1291;
																															BgL_arg1404z00_1291
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2492z00zz__expander_argsz00,
																																BNIL);
																															BgL_arg1402z00_1289
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2474z00zz__expander_argsz00,
																																BgL_arg1404z00_1291);
																														}
																														{	/* Eval/expdargs.scm 116 */
																															obj_t
																																BgL_arg1405z00_1292;
																															BgL_arg1405z00_1292
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2462z00zz__expander_argsz00,
																																BNIL);
																															BgL_arg1403z00_1290
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2494z00zz__expander_argsz00,
																																BgL_arg1405z00_1292);
																														}
																														BgL_arg1400z00_1287
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1402z00_1289,
																															BgL_arg1403z00_1290);
																													}
																													{	/* Eval/expdargs.scm 118 */
																														obj_t
																															BgL_arg1406z00_1293;
																														{	/* Eval/expdargs.scm 118 */
																															obj_t
																																BgL_arg1407z00_1294;
																															{	/* Eval/expdargs.scm 118 */
																																obj_t
																																	BgL_arg1408z00_1295;
																																{	/* Eval/expdargs.scm 118 */
																																	obj_t
																																		BgL_arg1410z00_1296;
																																	obj_t
																																		BgL_arg1411z00_1297;
																																	{	/* Eval/expdargs.scm 118 */
																																		obj_t
																																			BgL_arg1412z00_1298;
																																		obj_t
																																			BgL_arg1413z00_1299;
																																		BgL_arg1412z00_1298
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2512z00zz__expander_argsz00,
																																			BNIL);
																																		{	/* Eval/expdargs.scm 119 */
																																			obj_t
																																				BgL_arg1414z00_1300;
																																			{	/* Eval/expdargs.scm 119 */
																																				obj_t
																																					BgL_arg1415z00_1301;
																																				{	/* Eval/expdargs.scm 119 */
																																					obj_t
																																						BgL_arg1416z00_1302;
																																					BgL_arg1416z00_1302
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2506z00zz__expander_argsz00,
																																						BNIL);
																																					BgL_arg1415z00_1301
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2508z00zz__expander_argsz00,
																																						BgL_arg1416z00_1302);
																																				}
																																				BgL_arg1414z00_1300
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2514z00zz__expander_argsz00,
																																					BgL_arg1415z00_1301);
																																			}
																																			BgL_arg1413z00_1299
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1414z00_1300,
																																				BNIL);
																																		}
																																		BgL_arg1410z00_1296
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1412z00_1298,
																																			BgL_arg1413z00_1299);
																																	}
																																	{	/* Eval/expdargs.scm 120 */
																																		obj_t
																																			BgL_arg1417z00_1303;
																																		obj_t
																																			BgL_arg1418z00_1304;
																																		{	/* Eval/expdargs.scm 120 */
																																			obj_t
																																				BgL_arg1419z00_1305;
																																			obj_t
																																				BgL_arg1420z00_1306;
																																			BgL_arg1419z00_1305
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2516z00zz__expander_argsz00,
																																				BNIL);
																																			{	/* Eval/expdargs.scm 121 */
																																				obj_t
																																					BgL_arg1421z00_1307;
																																				{	/* Eval/expdargs.scm 121 */
																																					obj_t
																																						BgL_arg1422z00_1308;
																																					{	/* Eval/expdargs.scm 121 */
																																						obj_t
																																							BgL_arg1423z00_1309;
																																						{	/* Eval/expdargs.scm 121 */
																																							obj_t
																																								BgL_arg1424z00_1310;
																																							BgL_arg1424z00_1310
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2492z00zz__expander_argsz00,
																																								BNIL);
																																							BgL_arg1423z00_1309
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2518z00zz__expander_argsz00,
																																								BgL_arg1424z00_1310);
																																						}
																																						BgL_arg1422z00_1308
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1423z00_1309,
																																							BNIL);
																																					}
																																					BgL_arg1421z00_1307
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2520z00zz__expander_argsz00,
																																						BgL_arg1422z00_1308);
																																				}
																																				BgL_arg1420z00_1306
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1421z00_1307,
																																					BNIL);
																																			}
																																			BgL_arg1417z00_1303
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1419z00_1305,
																																				BgL_arg1420z00_1306);
																																		}
																																		{	/* Eval/expdargs.scm 122 */
																																			obj_t
																																				BgL_arg1425z00_1311;
																																			{	/* Eval/expdargs.scm 122 */
																																				obj_t
																																					BgL_arg1426z00_1312;
																																				obj_t
																																					BgL_arg1427z00_1313;
																																				BgL_arg1426z00_1312
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2468z00zz__expander_argsz00,
																																					BNIL);
																																				BgL_arg1427z00_1313
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2506z00zz__expander_argsz00,
																																					BNIL);
																																				BgL_arg1425z00_1311
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1426z00_1312,
																																					BgL_arg1427z00_1313);
																																			}
																																			BgL_arg1418z00_1304
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1425z00_1311,
																																				BNIL);
																																		}
																																		BgL_arg1411z00_1297
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1417z00_1303,
																																			BgL_arg1418z00_1304);
																																	}
																																	BgL_arg1408z00_1295
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1410z00_1296,
																																		BgL_arg1411z00_1297);
																																}
																																BgL_arg1407z00_1294
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2510z00zz__expander_argsz00,
																																	BgL_arg1408z00_1295);
																															}
																															BgL_arg1406z00_1293
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2522z00zz__expander_argsz00,
																																BgL_arg1407z00_1294);
																														}
																														BgL_arg1401z00_1288
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1406z00_1293,
																															BNIL);
																													}
																													BgL_arg1396z00_1284 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1400z00_1287,
																														BgL_arg1401z00_1288);
																												}
																												BgL_arg1394z00_1282 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1395z00_1283,
																													BgL_arg1396z00_1284);
																											}
																											BgL_arg1393z00_1281 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2524z00zz__expander_argsz00,
																												BgL_arg1394z00_1282);
																										}
																										BgL_arg1390z00_1278 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1393z00_1281,
																											BNIL);
																									}
																									BgL_arg1388z00_1276 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1389z00_1277,
																										BgL_arg1390z00_1278);
																								}
																								BgL_arg1387z00_1275 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2520z00zz__expander_argsz00,
																									BgL_arg1388z00_1276);
																							}
																							BgL_arg1384z00_1274 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2526z00zz__expander_argsz00,
																								BgL_arg1387z00_1275);
																						}
																						BgL_arg1377z00_1268 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1384z00_1274, BNIL);
																					}
																					BgL_arg1375z00_1266 =
																						MAKE_YOUNG_PAIR(BgL_arg1376z00_1267,
																						BgL_arg1377z00_1268);
																				}
																				BgL_arg1373z00_1265 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2514z00zz__expander_argsz00,
																					BgL_arg1375z00_1266);
																			}
																			BgL_arg1372z00_1264 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2526z00zz__expander_argsz00,
																				BgL_arg1373z00_1265);
																		}
																		BgL_arg1366z00_1258 =
																			MAKE_YOUNG_PAIR(BgL_arg1372z00_1264,
																			BNIL);
																	}
																	BgL_arg1361z00_1253 =
																		MAKE_YOUNG_PAIR(BgL_arg1365z00_1257,
																		BgL_arg1366z00_1258);
																}
																BgL_arg1359z00_1251 =
																	MAKE_YOUNG_PAIR(BgL_arg1360z00_1252,
																	BgL_arg1361z00_1253);
															}
															BgL_arg1358z00_1250 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2480z00zz__expander_argsz00,
																BgL_arg1359z00_1251);
														}
														BgL_arg1335z00_1216 =
															MAKE_YOUNG_PAIR(BgL_arg1358z00_1250, BNIL);
													}
													BgL_arg1305z00_1173 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1334z00_1215, BgL_arg1335z00_1216);
												}
												BgL_arg1284z00_1171 =
													MAKE_YOUNG_PAIR(BgL_arg1304z00_1172,
													BgL_arg1305z00_1173);
											}
											return
												MAKE_YOUNG_PAIR(BGl_symbol2528z00zz__expander_argsz00,
												BgL_arg1284z00_1171);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-help */
	obj_t BGl_makezd2helpzd2zz__expander_argsz00(obj_t BgL_clausez00_10)
	{
		{	/* Eval/expdargs.scm 128 */
			{
				obj_t BgL_optz00_1386;

				if (PAIRP(BgL_clausez00_10))
					{	/* Eval/expdargs.scm 129 */
						obj_t BgL_cdrzd2121zd2_1391;

						BgL_cdrzd2121zd2_1391 = CDR(((obj_t) BgL_clausez00_10));
						if (
							(CAR(
									((obj_t) BgL_clausez00_10)) ==
								BGl_symbol2535z00zz__expander_argsz00))
							{	/* Eval/expdargs.scm 129 */
								if (PAIRP(BgL_cdrzd2121zd2_1391))
									{	/* Eval/expdargs.scm 129 */
										if (NULLP(CDR(BgL_cdrzd2121zd2_1391)))
											{	/* Eval/expdargs.scm 129 */
												obj_t BgL_arg1481z00_1397;

												BgL_arg1481z00_1397 = CAR(BgL_cdrzd2121zd2_1391);
												return
													MAKE_YOUNG_PAIR(BGl_symbol2535z00zz__expander_argsz00,
													BgL_arg1481z00_1397);
											}
										else
											{	/* Eval/expdargs.scm 129 */
												obj_t BgL_carzd2129zd2_1398;

												BgL_carzd2129zd2_1398 = CAR(((obj_t) BgL_clausez00_10));
												if (PAIRP(BgL_carzd2129zd2_1398))
													{	/* Eval/expdargs.scm 129 */
														if (NULLP(CDR(BgL_carzd2129zd2_1398)))
															{	/* Eval/expdargs.scm 129 */
																return BFALSE;
															}
														else
															{	/* Eval/expdargs.scm 129 */
																BgL_optz00_1386 = BgL_carzd2129zd2_1398;
															BgL_tagzd2114zd2_1387:
																{	/* Eval/expdargs.scm 139 */
																	obj_t BgL_synz00_1427;

																	BgL_synz00_1427 =
																		CAR
																		(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_optz00_1386));
																	{
																		obj_t BgL_synopsisz00_1432;
																		obj_t BgL_msgz00_1433;

																		if (PAIRP(BgL_synz00_1427))
																			{	/* Eval/expdargs.scm 140 */
																				obj_t BgL_cdrzd2197zd2_1438;

																				BgL_cdrzd2197zd2_1438 =
																					CDR(((obj_t) BgL_synz00_1427));
																				if (PAIRP(BgL_cdrzd2197zd2_1438))
																					{	/* Eval/expdargs.scm 140 */
																						obj_t BgL_cdrzd2202zd2_1440;

																						BgL_cdrzd2202zd2_1440 =
																							CDR(BgL_cdrzd2197zd2_1438);
																						if (PAIRP(BgL_cdrzd2202zd2_1440))
																							{	/* Eval/expdargs.scm 140 */
																								if (NULLP(CDR
																										(BgL_cdrzd2202zd2_1440)))
																									{	/* Eval/expdargs.scm 140 */
																										obj_t BgL_arg1516z00_1444;
																										obj_t BgL_arg1517z00_1445;
																										obj_t BgL_arg1521z00_1446;

																										BgL_arg1516z00_1444 =
																											CAR(
																											((obj_t)
																												BgL_synz00_1427));
																										BgL_arg1517z00_1445 =
																											CAR
																											(BgL_cdrzd2197zd2_1438);
																										BgL_arg1521z00_1446 =
																											CAR
																											(BgL_cdrzd2202zd2_1440);
																										if (BGl_synopsiszf3zf3zz__expander_argsz00(BgL_arg1516z00_1444))
																											{	/* Eval/expdargs.scm 144 */
																												obj_t
																													BgL_arg1529z00_2731;
																												if (STRINGP
																													(BgL_arg1521z00_1446))
																													{	/* Eval/expdargs.scm 144 */
																														BgL_arg1529z00_2731
																															=
																															BgL_arg1521z00_1446;
																													}
																												else
																													{	/* Eval/expdargs.scm 144 */
																														obj_t
																															BgL_list1531z00_2733;
																														{	/* Eval/expdargs.scm 144 */
																															obj_t
																																BgL_arg1535z00_2734;
																															BgL_arg1535z00_2734
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1521z00_1446,
																																BNIL);
																															BgL_list1531z00_2733
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2484z00zz__expander_argsz00,
																																BgL_arg1535z00_2734);
																														}
																														BgL_arg1529z00_2731
																															=
																															BgL_list1531z00_2733;
																													}
																												return
																													MAKE_YOUNG_PAIR
																													(BgL_arg1517z00_1445,
																													BgL_arg1529z00_2731);
																											}
																										else
																											{	/* Eval/expdargs.scm 142 */
																												return
																													BGl_expandzd2errorzd2zz__expandz00
																													(BGl_string2456z00zz__expander_argsz00,
																													BGl_string2534z00zz__expander_argsz00,
																													BgL_clausez00_10);
																											}
																									}
																								else
																									{	/* Eval/expdargs.scm 140 */
																										return BFALSE;
																									}
																							}
																						else
																							{	/* Eval/expdargs.scm 140 */
																								if (NULLP(CDR(
																											((obj_t)
																												BgL_cdrzd2197zd2_1438))))
																									{	/* Eval/expdargs.scm 140 */
																										obj_t BgL_arg1525z00_1451;
																										obj_t BgL_arg1526z00_1452;

																										BgL_arg1525z00_1451 =
																											CAR(
																											((obj_t)
																												BgL_synz00_1427));
																										BgL_arg1526z00_1452 =
																											CAR(((obj_t)
																												BgL_cdrzd2197zd2_1438));
																										BgL_synopsisz00_1432 =
																											BgL_arg1525z00_1451;
																										BgL_msgz00_1433 =
																											BgL_arg1526z00_1452;
																										{	/* Eval/expdargs.scm 146 */
																											bool_t
																												BgL_test2691z00_3662;
																											{	/* Eval/expdargs.scm 459 */
																												bool_t
																													BgL__ortest_1047z00_2722;
																												BgL__ortest_1047z00_2722
																													=
																													(BgL_synopsisz00_1432
																													==
																													BGl_symbol2530z00zz__expander_argsz00);
																												if (BgL__ortest_1047z00_2722)
																													{	/* Eval/expdargs.scm 459 */
																														BgL_test2691z00_3662
																															=
																															BgL__ortest_1047z00_2722;
																													}
																												else
																													{	/* Eval/expdargs.scm 459 */
																														BgL_test2691z00_3662
																															=
																															(BgL_synopsisz00_1432
																															==
																															BGl_symbol2532z00zz__expander_argsz00);
																													}
																											}
																											if (BgL_test2691z00_3662)
																												{	/* Eval/expdargs.scm 148 */
																													obj_t
																														BgL_arg1537z00_1460;
																													obj_t
																														BgL_arg1539z00_1461;
																													BgL_arg1537z00_1460 =
																														BGl_makezd2synopsiszd2namez00zz__expander_argsz00
																														(BgL_clausez00_10);
																													if (STRINGP
																														(BgL_msgz00_1433))
																														{	/* Eval/expdargs.scm 149 */
																															BgL_arg1539z00_1461
																																=
																																BgL_msgz00_1433;
																														}
																													else
																														{	/* Eval/expdargs.scm 149 */
																															obj_t
																																BgL_list1541z00_1463;
																															{	/* Eval/expdargs.scm 149 */
																																obj_t
																																	BgL_arg1543z00_1464;
																																BgL_arg1543z00_1464
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_msgz00_1433,
																																	BNIL);
																																BgL_list1541z00_1463
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2484z00zz__expander_argsz00,
																																	BgL_arg1543z00_1464);
																															}
																															BgL_arg1539z00_1461
																																=
																																BgL_list1541z00_1463;
																														}
																													return
																														MAKE_YOUNG_PAIR
																														(BgL_arg1537z00_1460,
																														BgL_arg1539z00_1461);
																												}
																											else
																												{	/* Eval/expdargs.scm 146 */
																													return
																														BGl_expandzd2errorzd2zz__expandz00
																														(BGl_string2456z00zz__expander_argsz00,
																														BGl_string2534z00zz__expander_argsz00,
																														BgL_clausez00_10);
																												}
																										}
																									}
																								else
																									{	/* Eval/expdargs.scm 140 */
																										return BFALSE;
																									}
																							}
																					}
																				else
																					{	/* Eval/expdargs.scm 140 */
																						return BFALSE;
																					}
																			}
																		else
																			{	/* Eval/expdargs.scm 140 */
																				return BFALSE;
																			}
																	}
																}
															}
													}
												else
													{
														obj_t BgL_optz00_3673;

														BgL_optz00_3673 = BgL_carzd2129zd2_1398;
														BgL_optz00_1386 = BgL_optz00_3673;
														goto BgL_tagzd2114zd2_1387;
													}
											}
									}
								else
									{	/* Eval/expdargs.scm 129 */
										obj_t BgL_carzd2146zd2_1406;

										BgL_carzd2146zd2_1406 = CAR(((obj_t) BgL_clausez00_10));
										if (PAIRP(BgL_carzd2146zd2_1406))
											{	/* Eval/expdargs.scm 129 */
												if (NULLP(CDR(BgL_carzd2146zd2_1406)))
													{	/* Eval/expdargs.scm 129 */
														return BFALSE;
													}
												else
													{
														obj_t BgL_optz00_3681;

														BgL_optz00_3681 = BgL_carzd2146zd2_1406;
														BgL_optz00_1386 = BgL_optz00_3681;
														goto BgL_tagzd2114zd2_1387;
													}
											}
										else
											{
												obj_t BgL_optz00_3682;

												BgL_optz00_3682 = BgL_carzd2146zd2_1406;
												BgL_optz00_1386 = BgL_optz00_3682;
												goto BgL_tagzd2114zd2_1387;
											}
									}
							}
						else
							{	/* Eval/expdargs.scm 129 */
								if (NULLP(CAR(((obj_t) BgL_clausez00_10))))
									{	/* Eval/expdargs.scm 129 */
										return BFALSE;
									}
								else
									{	/* Eval/expdargs.scm 129 */
										if (
											(CAR(
													((obj_t) BgL_clausez00_10)) ==
												BGl_symbol2537z00zz__expander_argsz00))
											{	/* Eval/expdargs.scm 129 */
												return BFALSE;
											}
										else
											{	/* Eval/expdargs.scm 129 */
												obj_t BgL_carzd2167zd2_1417;

												BgL_carzd2167zd2_1417 = CAR(((obj_t) BgL_clausez00_10));
												if (PAIRP(BgL_carzd2167zd2_1417))
													{	/* Eval/expdargs.scm 129 */
														if (NULLP(CDR(BgL_carzd2167zd2_1417)))
															{	/* Eval/expdargs.scm 129 */
																return BFALSE;
															}
														else
															{
																obj_t BgL_optz00_3698;

																BgL_optz00_3698 = BgL_carzd2167zd2_1417;
																BgL_optz00_1386 = BgL_optz00_3698;
																goto BgL_tagzd2114zd2_1387;
															}
													}
												else
													{
														obj_t BgL_optz00_3699;

														BgL_optz00_3699 = BgL_carzd2167zd2_1417;
														BgL_optz00_1386 = BgL_optz00_3699;
														goto BgL_tagzd2114zd2_1387;
													}
											}
									}
							}
					}
				else
					{	/* Eval/expdargs.scm 129 */
						return BFALSE;
					}
			}
		}

	}



/* make-synopsis-name */
	obj_t BGl_makezd2synopsiszd2namez00zz__expander_argsz00(obj_t
		BgL_clausez00_12)
	{
		{	/* Eval/expdargs.scm 164 */
			{
				obj_t BgL_optz00_1493;
				obj_t BgL_oz00_1494;
				obj_t BgL_argsz00_1495;
				obj_t BgL_optz00_1513;
				obj_t BgL_ozb2zb2_1514;
				obj_t BgL_argsz00_1515;

				{	/* Eval/expdargs.scm 222 */
					obj_t BgL_optz00_1468;

					BgL_optz00_1468 = CAR(((obj_t) BgL_clausez00_12));
					{	/* Eval/expdargs.scm 222 */
						obj_t BgL_oz00_1469;

						BgL_oz00_1469 = CAR(((obj_t) BgL_optz00_1468));
						{	/* Eval/expdargs.scm 223 */
							obj_t BgL_argsz00_1470;

							BgL_argsz00_1470 =
								BGl_fetchzd2optionzd2argumentsz00zz__expander_argsz00
								(BgL_optz00_1468);
							{	/* Eval/expdargs.scm 224 */

								if (STRINGP(BgL_oz00_1469))
									{	/* Eval/expdargs.scm 226 */
										BgL_optz00_1493 = BgL_optz00_1468;
										BgL_oz00_1494 = BgL_oz00_1469;
										BgL_argsz00_1495 = BgL_argsz00_1470;
										{	/* Eval/expdargs.scm 166 */
											obj_t BgL_oidz00_1497;

											BgL_oidz00_1497 =
												BGl_fetchzd2optionzd2embedzd2argumentzd2zz__expander_argsz00
												(BgL_oz00_1494);
											{	/* Eval/expdargs.scm 167 */
												obj_t BgL_aidz00_1498;

												{	/* Eval/expdargs.scm 169 */
													obj_t BgL_tmpz00_2758;

													{	/* Eval/expdargs.scm 169 */
														int BgL_tmpz00_3708;

														BgL_tmpz00_3708 = (int) (1L);
														BgL_tmpz00_2758 = BGL_MVALUES_VAL(BgL_tmpz00_3708);
													}
													{	/* Eval/expdargs.scm 169 */
														int BgL_tmpz00_3711;

														BgL_tmpz00_3711 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3711, BUNSPEC);
													}
													BgL_aidz00_1498 = BgL_tmpz00_2758;
												}
												{	/* Eval/expdargs.scm 169 */
													bool_t BgL_test2701z00_3714;

													if (CBOOL(BgL_aidz00_1498))
														{	/* Eval/expdargs.scm 169 */
															BgL_test2701z00_3714 = PAIRP(BgL_argsz00_1495);
														}
													else
														{	/* Eval/expdargs.scm 169 */
															BgL_test2701z00_3714 = ((bool_t) 0);
														}
													if (BgL_test2701z00_3714)
														{	/* Eval/expdargs.scm 169 */
															return
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string2456z00zz__expander_argsz00,
																BGl_string2541z00zz__expander_argsz00,
																BgL_clausez00_12);
														}
													else
														{	/* Eval/expdargs.scm 171 */
															bool_t BgL_test2703z00_3719;

															if (CBOOL(BgL_aidz00_1498))
																{	/* Eval/expdargs.scm 171 */
																	BgL_test2703z00_3719 = ((bool_t) 1);
																}
															else
																{	/* Eval/expdargs.scm 171 */
																	BgL_test2703z00_3719 =
																		PAIRP(BgL_argsz00_1495);
																}
															if (BgL_test2703z00_3719)
																{	/* Eval/expdargs.scm 171 */
																	if (CBOOL(BgL_aidz00_1498))
																		{	/* Eval/expdargs.scm 173 */
																			return
																				string_append(BgL_oidz00_1497,
																				BGl_stringzd2upcasezd2zz__r4_strings_6_7z00
																				(BgL_aidz00_1498));
																		}
																	else
																		{	/* Eval/expdargs.scm 173 */
																			return
																				string_append(BgL_oidz00_1497,
																				BGl_loopze71ze7zz__expander_argsz00
																				(BgL_clausez00_12, BgL_argsz00_1495));
																		}
																}
															else
																{	/* Eval/expdargs.scm 171 */
																	return BgL_oidz00_1497;
																}
														}
												}
											}
										}
									}
								else
									{	/* Eval/expdargs.scm 228 */
										bool_t BgL_test2706z00_3729;

										if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
											(BgL_oz00_1469))
											{
												obj_t BgL_l1129z00_1485;

												BgL_l1129z00_1485 = BgL_oz00_1469;
											BgL_zc3z04anonymousza31554ze3z87_1486:
												if (NULLP(BgL_l1129z00_1485))
													{	/* Eval/expdargs.scm 228 */
														BgL_test2706z00_3729 = ((bool_t) 1);
													}
												else
													{	/* Eval/expdargs.scm 228 */
														bool_t BgL_test2709z00_3734;

														{	/* Eval/expdargs.scm 228 */
															obj_t BgL_tmpz00_3735;

															BgL_tmpz00_3735 =
																CAR(((obj_t) BgL_l1129z00_1485));
															BgL_test2709z00_3734 = STRINGP(BgL_tmpz00_3735);
														}
														if (BgL_test2709z00_3734)
															{
																obj_t BgL_l1129z00_3739;

																BgL_l1129z00_3739 =
																	CDR(((obj_t) BgL_l1129z00_1485));
																BgL_l1129z00_1485 = BgL_l1129z00_3739;
																goto BgL_zc3z04anonymousza31554ze3z87_1486;
															}
														else
															{	/* Eval/expdargs.scm 228 */
																BgL_test2706z00_3729 = ((bool_t) 0);
															}
													}
											}
										else
											{	/* Eval/expdargs.scm 228 */
												BgL_test2706z00_3729 = ((bool_t) 0);
											}
										if (BgL_test2706z00_3729)
											{	/* Eval/expdargs.scm 228 */
												BgL_optz00_1513 = BgL_optz00_1468;
												BgL_ozb2zb2_1514 = BgL_oz00_1469;
												BgL_argsz00_1515 = BgL_argsz00_1470;
												{	/* Eval/expdargs.scm 192 */
													obj_t BgL_oidzb2zb2_1518;

													{
														obj_t BgL_ozb2zb2_1574;
														obj_t BgL_oidzb2zb2_1575;
														obj_t BgL_aidzb2zb2_1576;

														BgL_ozb2zb2_1574 = BgL_ozb2zb2_1514;
														BgL_oidzb2zb2_1575 = BNIL;
														BgL_aidzb2zb2_1576 = BNIL;
													BgL_zc3z04anonymousza31612ze3z87_1577:
														if (NULLP(BgL_ozb2zb2_1574))
															{	/* Eval/expdargs.scm 197 */
																obj_t BgL_val0_1119z00_1579;
																obj_t BgL_val1_1120z00_1580;

																BgL_val0_1119z00_1579 =
																	bgl_reverse_bang(BgL_oidzb2zb2_1575);
																BgL_val1_1120z00_1580 =
																	bgl_reverse_bang(BgL_aidzb2zb2_1576);
																{	/* Eval/expdargs.scm 197 */
																	int BgL_tmpz00_3746;

																	BgL_tmpz00_3746 = (int) (2L);
																	BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3746);
																}
																{	/* Eval/expdargs.scm 197 */
																	int BgL_tmpz00_3749;

																	BgL_tmpz00_3749 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_3749,
																		BgL_val1_1120z00_1580);
																}
																BgL_oidzb2zb2_1518 = BgL_val0_1119z00_1579;
															}
														else
															{	/* Eval/expdargs.scm 198 */
																obj_t BgL_oidz00_1581;

																{	/* Eval/expdargs.scm 199 */
																	obj_t BgL_arg1618z00_1586;

																	BgL_arg1618z00_1586 =
																		CAR(((obj_t) BgL_ozb2zb2_1574));
																	BgL_oidz00_1581 =
																		BGl_fetchzd2optionzd2embedzd2argumentzd2zz__expander_argsz00
																		(BgL_arg1618z00_1586);
																}
																{	/* Eval/expdargs.scm 199 */
																	obj_t BgL_aidz00_1582;

																	{	/* Eval/expdargs.scm 200 */
																		obj_t BgL_tmpz00_2766;

																		{	/* Eval/expdargs.scm 200 */
																			int BgL_tmpz00_3755;

																			BgL_tmpz00_3755 = (int) (1L);
																			BgL_tmpz00_2766 =
																				BGL_MVALUES_VAL(BgL_tmpz00_3755);
																		}
																		{	/* Eval/expdargs.scm 200 */
																			int BgL_tmpz00_3758;

																			BgL_tmpz00_3758 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_3758,
																				BUNSPEC);
																		}
																		BgL_aidz00_1582 = BgL_tmpz00_2766;
																	}
																	{	/* Eval/expdargs.scm 200 */
																		obj_t BgL_arg1615z00_1583;
																		obj_t BgL_arg1616z00_1584;
																		obj_t BgL_arg1617z00_1585;

																		BgL_arg1615z00_1583 =
																			CDR(((obj_t) BgL_ozb2zb2_1574));
																		BgL_arg1616z00_1584 =
																			MAKE_YOUNG_PAIR(BgL_oidz00_1581,
																			BgL_oidzb2zb2_1575);
																		BgL_arg1617z00_1585 =
																			MAKE_YOUNG_PAIR(BgL_aidz00_1582,
																			BgL_aidzb2zb2_1576);
																		{
																			obj_t BgL_aidzb2zb2_3767;
																			obj_t BgL_oidzb2zb2_3766;
																			obj_t BgL_ozb2zb2_3765;

																			BgL_ozb2zb2_3765 = BgL_arg1615z00_1583;
																			BgL_oidzb2zb2_3766 = BgL_arg1616z00_1584;
																			BgL_aidzb2zb2_3767 = BgL_arg1617z00_1585;
																			BgL_aidzb2zb2_1576 = BgL_aidzb2zb2_3767;
																			BgL_oidzb2zb2_1575 = BgL_oidzb2zb2_3766;
																			BgL_ozb2zb2_1574 = BgL_ozb2zb2_3765;
																			goto
																				BgL_zc3z04anonymousza31612ze3z87_1577;
																		}
																	}
																}
															}
													}
													{	/* Eval/expdargs.scm 193 */
														obj_t BgL_aidzb2zb2_1519;

														{	/* Eval/expdargs.scm 202 */
															obj_t BgL_tmpz00_2768;

															{	/* Eval/expdargs.scm 202 */
																int BgL_tmpz00_3768;

																BgL_tmpz00_3768 = (int) (1L);
																BgL_tmpz00_2768 =
																	BGL_MVALUES_VAL(BgL_tmpz00_3768);
															}
															{	/* Eval/expdargs.scm 202 */
																int BgL_tmpz00_3771;

																BgL_tmpz00_3771 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_3771, BUNSPEC);
															}
															BgL_aidzb2zb2_1519 = BgL_tmpz00_2768;
														}
														{	/* Eval/expdargs.scm 202 */
															bool_t BgL_test2711z00_3774;

															if (PAIRP(BgL_aidzb2zb2_1519))
																{
																	obj_t BgL_l1121z00_2782;

																	{	/* Eval/expdargs.scm 202 */
																		obj_t BgL_tmpz00_3777;

																		BgL_l1121z00_2782 = BgL_aidzb2zb2_1519;
																	BgL_zc3z04anonymousza31610ze3z87_2781:
																		if (NULLP(BgL_l1121z00_2782))
																			{	/* Eval/expdargs.scm 202 */
																				BgL_tmpz00_3777 = BFALSE;
																			}
																		else
																			{	/* Eval/expdargs.scm 202 */
																				obj_t BgL__ortest_1123z00_2788;

																				BgL__ortest_1123z00_2788 =
																					CAR(((obj_t) BgL_l1121z00_2782));
																				if (CBOOL(BgL__ortest_1123z00_2788))
																					{	/* Eval/expdargs.scm 202 */
																						BgL_tmpz00_3777 =
																							BgL__ortest_1123z00_2788;
																					}
																				else
																					{
																						obj_t BgL_l1121z00_3784;

																						BgL_l1121z00_3784 =
																							CDR(((obj_t) BgL_l1121z00_2782));
																						BgL_l1121z00_2782 =
																							BgL_l1121z00_3784;
																						goto
																							BgL_zc3z04anonymousza31610ze3z87_2781;
																					}
																			}
																		BgL_test2711z00_3774 =
																			CBOOL(BgL_tmpz00_3777);
																	}
																}
															else
																{	/* Eval/expdargs.scm 202 */
																	BgL_test2711z00_3774 = ((bool_t) 0);
																}
															if (BgL_test2711z00_3774)
																{	/* Eval/expdargs.scm 202 */
																	if (NULLP(BgL_argsz00_1515))
																		{	/* Eval/expdargs.scm 215 */
																			obj_t BgL_runner1596z00_1549;

																			{	/* Eval/expdargs.scm 216 */
																				obj_t BgL_arg1585z00_1532;
																				obj_t BgL_arg1586z00_1533;

																				BgL_arg1585z00_1532 =
																					BGl_concatze70ze7zz__expander_argsz00
																					(BgL_oidzb2zb2_1518);
																				{	/* Eval/expdargs.scm 217 */
																					obj_t BgL_head1126z00_1537;

																					BgL_head1126z00_1537 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1124z00_1539;
																						obj_t BgL_tail1127z00_1540;

																						BgL_l1124z00_1539 =
																							BgL_aidzb2zb2_1519;
																						BgL_tail1127z00_1540 =
																							BgL_head1126z00_1537;
																					BgL_zc3z04anonymousza31589ze3z87_1541:
																						if (NULLP
																							(BgL_l1124z00_1539))
																							{	/* Eval/expdargs.scm 217 */
																								BgL_arg1586z00_1533 =
																									CDR(BgL_head1126z00_1537);
																							}
																						else
																							{	/* Eval/expdargs.scm 217 */
																								obj_t BgL_newtail1128z00_1543;

																								{	/* Eval/expdargs.scm 217 */
																									obj_t BgL_arg1594z00_1545;

																									{	/* Eval/expdargs.scm 217 */
																										obj_t BgL_az00_1546;

																										BgL_az00_1546 =
																											CAR(
																											((obj_t)
																												BgL_l1124z00_1539));
																										BgL_arg1594z00_1545 =
																											string_append
																											(BGl_string2539z00zz__expander_argsz00,
																											BGl_stringzd2upcasezd2zz__r4_strings_6_7z00
																											(BgL_az00_1546));
																									}
																									BgL_newtail1128z00_1543 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1594z00_1545, BNIL);
																								}
																								SET_CDR(BgL_tail1127z00_1540,
																									BgL_newtail1128z00_1543);
																								{	/* Eval/expdargs.scm 217 */
																									obj_t BgL_arg1593z00_1544;

																									BgL_arg1593z00_1544 =
																										CDR(
																										((obj_t)
																											BgL_l1124z00_1539));
																									{
																										obj_t BgL_tail1127z00_3804;
																										obj_t BgL_l1124z00_3803;

																										BgL_l1124z00_3803 =
																											BgL_arg1593z00_1544;
																										BgL_tail1127z00_3804 =
																											BgL_newtail1128z00_1543;
																										BgL_tail1127z00_1540 =
																											BgL_tail1127z00_3804;
																										BgL_l1124z00_1539 =
																											BgL_l1124z00_3803;
																										goto
																											BgL_zc3z04anonymousza31589ze3z87_1541;
																									}
																								}
																							}
																					}
																				}
																				{	/* Eval/expdargs.scm 215 */
																					obj_t BgL_list1587z00_1534;

																					BgL_list1587z00_1534 =
																						MAKE_YOUNG_PAIR(BgL_arg1586z00_1533,
																						BNIL);
																					BgL_runner1596z00_1549 =
																						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
																						(BgL_arg1585z00_1532,
																						BgL_list1587z00_1534);
																				}
																			}
																			BGL_TAIL return
																				BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																				(BgL_runner1596z00_1549);
																		}
																	else
																		{	/* Eval/expdargs.scm 212 */
																			return
																				BGl_expandzd2errorzd2zz__expandz00
																				(BGl_string2456z00zz__expander_argsz00,
																				BGl_string2540z00zz__expander_argsz00,
																				BgL_clausez00_12);
																		}
																}
															else
																{	/* Eval/expdargs.scm 202 */
																	return
																		string_append
																		(BGl_concatze70ze7zz__expander_argsz00
																		(BgL_oidzb2zb2_1518),
																		BGl_loopze72ze7zz__expander_argsz00
																		(BgL_clausez00_12, BgL_argsz00_1515));
																}
														}
													}
												}
											}
										else
											{	/* Eval/expdargs.scm 228 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string2456z00zz__expander_argsz00,
													BGl_string2541z00zz__expander_argsz00,
													BgL_clausez00_12);
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__expander_argsz00(obj_t BgL_clausez00_3210,
		obj_t BgL_argsz00_1504)
	{
		{	/* Eval/expdargs.scm 178 */
			if (NULLP(BgL_argsz00_1504))
				{	/* Eval/expdargs.scm 179 */
					return BGl_string2542z00zz__expander_argsz00;
				}
			else
				{	/* Eval/expdargs.scm 183 */
					obj_t BgL_arg1565z00_1507;
					obj_t BgL_arg1567z00_1508;

					{	/* Eval/expdargs.scm 183 */
						obj_t BgL_arg1571z00_1509;

						{	/* Eval/expdargs.scm 183 */
							obj_t BgL_arg1573z00_1510;

							BgL_arg1573z00_1510 = CAR(((obj_t) BgL_argsz00_1504));
							BgL_arg1571z00_1509 =
								BGl_fetchzd2argumentzd2namez00zz__expander_argsz00
								(BgL_arg1573z00_1510, BgL_clausez00_3210);
						}
						BgL_arg1565z00_1507 =
							BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_arg1571z00_1509);
					}
					{	/* Eval/expdargs.scm 184 */
						obj_t BgL_arg1575z00_1511;

						BgL_arg1575z00_1511 = CDR(((obj_t) BgL_argsz00_1504));
						BgL_arg1567z00_1508 =
							BGl_loopze71ze7zz__expander_argsz00(BgL_clausez00_3210,
							BgL_arg1575z00_1511);
					}
					return
						string_append_3(BGl_string2539z00zz__expander_argsz00,
						BgL_arg1565z00_1507, BgL_arg1567z00_1508);
				}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zz__expander_argsz00(obj_t BgL_clausez00_3211,
		obj_t BgL_argsz00_1553)
	{
		{	/* Eval/expdargs.scm 205 */
			if (NULLP(BgL_argsz00_1553))
				{	/* Eval/expdargs.scm 206 */
					return BGl_string2542z00zz__expander_argsz00;
				}
			else
				{	/* Eval/expdargs.scm 210 */
					obj_t BgL_arg1605z00_1556;
					obj_t BgL_arg1606z00_1557;

					{	/* Eval/expdargs.scm 210 */
						obj_t BgL_arg1607z00_1558;

						{	/* Eval/expdargs.scm 210 */
							obj_t BgL_arg1608z00_1559;

							BgL_arg1608z00_1559 = CAR(((obj_t) BgL_argsz00_1553));
							BgL_arg1607z00_1558 =
								BGl_fetchzd2argumentzd2namez00zz__expander_argsz00
								(BgL_arg1608z00_1559, BgL_clausez00_3211);
						}
						BgL_arg1605z00_1556 =
							BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_arg1607z00_1558);
					}
					{	/* Eval/expdargs.scm 211 */
						obj_t BgL_arg1609z00_1560;

						BgL_arg1609z00_1560 = CDR(((obj_t) BgL_argsz00_1553));
						BgL_arg1606z00_1557 =
							BGl_loopze72ze7zz__expander_argsz00(BgL_clausez00_3211,
							BgL_arg1609z00_1560);
					}
					return
						string_append_3(BGl_string2539z00zz__expander_argsz00,
						BgL_arg1605z00_1556, BgL_arg1606z00_1557);
				}
		}

	}



/* concat~0 */
	obj_t BGl_concatze70ze7zz__expander_argsz00(obj_t BgL_lz00_1588)
	{
		{	/* Eval/expdargs.scm 191 */
			if (NULLP(CDR(((obj_t) BgL_lz00_1588))))
				{	/* Eval/expdargs.scm 187 */
					return CAR(((obj_t) BgL_lz00_1588));
				}
			else
				{	/* Eval/expdargs.scm 189 */
					obj_t BgL_arg1622z00_1592;
					obj_t BgL_arg1623z00_1593;

					BgL_arg1622z00_1592 = CAR(((obj_t) BgL_lz00_1588));
					{	/* Eval/expdargs.scm 191 */
						obj_t BgL_arg1624z00_1594;

						BgL_arg1624z00_1594 = CDR(((obj_t) BgL_lz00_1588));
						BgL_arg1623z00_1593 =
							BGl_concatze70ze7zz__expander_argsz00(BgL_arg1624z00_1594);
					}
					return
						string_append_3(BgL_arg1622z00_1592,
						BGl_string2543z00zz__expander_argsz00, BgL_arg1623z00_1593);
				}
		}

	}



/* make-parser */
	obj_t BGl_makezd2parserzd2zz__expander_argsz00(obj_t BgL_clausez00_13,
		obj_t BgL_otablez00_14)
	{
		{	/* Eval/expdargs.scm 236 */
			{
				obj_t BgL_exprza2za2_1603;
				obj_t BgL_exprza2za2_1601;

				if (PAIRP(BgL_clausez00_13))
					{	/* Eval/expdargs.scm 237 */
						obj_t BgL_cdrzd2249zd2_1609;

						BgL_cdrzd2249zd2_1609 = CDR(((obj_t) BgL_clausez00_13));
						if (
							(CAR(
									((obj_t) BgL_clausez00_13)) ==
								BGl_symbol2535z00zz__expander_argsz00))
							{	/* Eval/expdargs.scm 237 */
								if (PAIRP(BgL_cdrzd2249zd2_1609))
									{	/* Eval/expdargs.scm 237 */
										if (NULLP(CDR(BgL_cdrzd2249zd2_1609)))
											{	/* Eval/expdargs.scm 237 */
												return BFALSE;
											}
										else
											{	/* Eval/expdargs.scm 237 */
												bool_t BgL_test2724z00_3858;

												{	/* Eval/expdargs.scm 237 */
													obj_t BgL_arg1638z00_1618;

													BgL_arg1638z00_1618 = CAR(((obj_t) BgL_clausez00_13));
													BgL_test2724z00_3858 =
														BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1638z00_1618);
												}
												if (BgL_test2724z00_3858)
													{	/* Eval/expdargs.scm 237 */
														return
															BGl_makezd2optzd2parserz00zz__expander_argsz00
															(BgL_clausez00_13, BgL_otablez00_14);
													}
												else
													{	/* Eval/expdargs.scm 237 */
														return
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2456z00zz__expander_argsz00,
															BGl_string2541z00zz__expander_argsz00,
															BgL_clausez00_13);
													}
											}
									}
								else
									{	/* Eval/expdargs.scm 237 */
										bool_t BgL_test2725z00_3864;

										{	/* Eval/expdargs.scm 237 */
											obj_t BgL_arg1642z00_1622;

											BgL_arg1642z00_1622 = CAR(((obj_t) BgL_clausez00_13));
											BgL_test2725z00_3864 =
												BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1642z00_1622);
										}
										if (BgL_test2725z00_3864)
											{	/* Eval/expdargs.scm 237 */
												return
													BGl_makezd2optzd2parserz00zz__expander_argsz00
													(BgL_clausez00_13, BgL_otablez00_14);
											}
										else
											{	/* Eval/expdargs.scm 237 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string2456z00zz__expander_argsz00,
													BGl_string2541z00zz__expander_argsz00,
													BgL_clausez00_13);
											}
									}
							}
						else
							{	/* Eval/expdargs.scm 237 */
								if (NULLP(CAR(((obj_t) BgL_clausez00_13))))
									{	/* Eval/expdargs.scm 237 */
										BgL_exprza2za2_1601 = BgL_cdrzd2249zd2_1609;
										{	/* Eval/expdargs.scm 241 */
											obj_t BgL_az00_1635;
											obj_t BgL_vz00_1636;

											{	/* Eval/expdargs.scm 241 */

												{	/* Eval/expdargs.scm 241 */

													BgL_az00_1635 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
												}
											}
											{	/* Eval/expdargs.scm 242 */

												{	/* Eval/expdargs.scm 242 */

													BgL_vz00_1636 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
												}
											}
											{	/* Eval/expdargs.scm 243 */
												obj_t BgL_arg1656z00_1637;

												{	/* Eval/expdargs.scm 243 */
													obj_t BgL_arg1657z00_1638;
													obj_t BgL_arg1658z00_1639;

													{	/* Eval/expdargs.scm 243 */
														obj_t BgL_arg1661z00_1640;

														BgL_arg1661z00_1640 =
															MAKE_YOUNG_PAIR(BgL_vz00_1636, BNIL);
														BgL_arg1657z00_1638 =
															MAKE_YOUNG_PAIR(BgL_az00_1635,
															BgL_arg1661z00_1640);
													}
													{	/* Eval/expdargs.scm 244 */
														obj_t BgL_arg1663z00_1641;

														{	/* Eval/expdargs.scm 244 */
															obj_t BgL_arg1664z00_1642;

															{	/* Eval/expdargs.scm 244 */
																obj_t BgL_arg1667z00_1643;
																obj_t BgL_arg1668z00_1644;

																{	/* Eval/expdargs.scm 244 */
																	obj_t BgL_arg1669z00_1645;

																	BgL_arg1669z00_1645 =
																		MAKE_YOUNG_PAIR(BgL_az00_1635, BNIL);
																	BgL_arg1667z00_1643 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2466z00zz__expander_argsz00,
																		BgL_arg1669z00_1645);
																}
																{	/* Eval/expdargs.scm 245 */
																	obj_t BgL_arg1670z00_1646;
																	obj_t BgL_arg1675z00_1647;

																	{	/* Eval/expdargs.scm 245 */
																		obj_t BgL_arg1676z00_1648;

																		{	/* Eval/expdargs.scm 245 */
																			obj_t BgL_arg1678z00_1649;
																			obj_t BgL_arg1681z00_1650;

																			{	/* Eval/expdargs.scm 245 */
																				obj_t BgL_arg1684z00_1651;

																				BgL_arg1684z00_1651 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2468z00zz__expander_argsz00,
																					BNIL);
																				BgL_arg1678z00_1649 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2470z00zz__expander_argsz00,
																					BgL_arg1684z00_1651);
																			}
																			{	/* Eval/expdargs.scm 245 */
																				obj_t BgL_arg1685z00_1652;

																				{	/* Eval/expdargs.scm 245 */
																					obj_t BgL_arg1688z00_1653;

																					{	/* Eval/expdargs.scm 245 */
																						obj_t BgL_arg1689z00_1654;

																						BgL_arg1689z00_1654 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_exprza2za2_1601, BNIL);
																						BgL_arg1688z00_1653 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2544z00zz__expander_argsz00,
																							BgL_arg1689z00_1654);
																					}
																					BgL_arg1685z00_1652 =
																						MAKE_YOUNG_PAIR(BgL_arg1688z00_1653,
																						BNIL);
																				}
																				BgL_arg1681z00_1650 =
																					MAKE_YOUNG_PAIR(BgL_az00_1635,
																					BgL_arg1685z00_1652);
																			}
																			BgL_arg1676z00_1648 =
																				MAKE_YOUNG_PAIR(BgL_arg1678z00_1649,
																				BgL_arg1681z00_1650);
																		}
																		BgL_arg1670z00_1646 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2472z00zz__expander_argsz00,
																			BgL_arg1676z00_1648);
																	}
																	{	/* Eval/expdargs.scm 246 */
																		obj_t BgL_arg1691z00_1655;

																		{	/* Eval/expdargs.scm 246 */
																			obj_t BgL_arg1692z00_1656;

																			{	/* Eval/expdargs.scm 246 */
																				obj_t BgL_arg1699z00_1657;
																				obj_t BgL_arg1700z00_1658;

																				{	/* Eval/expdargs.scm 246 */
																					obj_t BgL_arg1701z00_1659;

																					BgL_arg1701z00_1659 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2516z00zz__expander_argsz00,
																						BNIL);
																					BgL_arg1699z00_1657 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2470z00zz__expander_argsz00,
																						BgL_arg1701z00_1659);
																				}
																				{	/* Eval/expdargs.scm 246 */
																					obj_t BgL_arg1702z00_1660;

																					BgL_arg1702z00_1660 =
																						MAKE_YOUNG_PAIR(BgL_vz00_1636,
																						BNIL);
																					BgL_arg1700z00_1658 =
																						MAKE_YOUNG_PAIR(BgL_az00_1635,
																						BgL_arg1702z00_1660);
																				}
																				BgL_arg1692z00_1656 =
																					MAKE_YOUNG_PAIR(BgL_arg1699z00_1657,
																					BgL_arg1700z00_1658);
																			}
																			BgL_arg1691z00_1655 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2472z00zz__expander_argsz00,
																				BgL_arg1692z00_1656);
																		}
																		BgL_arg1675z00_1647 =
																			MAKE_YOUNG_PAIR(BgL_arg1691z00_1655,
																			BNIL);
																	}
																	BgL_arg1668z00_1644 =
																		MAKE_YOUNG_PAIR(BgL_arg1670z00_1646,
																		BgL_arg1675z00_1647);
																}
																BgL_arg1664z00_1642 =
																	MAKE_YOUNG_PAIR(BgL_arg1667z00_1643,
																	BgL_arg1668z00_1644);
															}
															BgL_arg1663z00_1641 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2480z00zz__expander_argsz00,
																BgL_arg1664z00_1642);
														}
														BgL_arg1658z00_1639 =
															MAKE_YOUNG_PAIR(BgL_arg1663z00_1641, BNIL);
													}
													BgL_arg1656z00_1637 =
														MAKE_YOUNG_PAIR(BgL_arg1657z00_1638,
														BgL_arg1658z00_1639);
												}
												return
													MAKE_YOUNG_PAIR(BGl_symbol2482z00zz__expander_argsz00,
													BgL_arg1656z00_1637);
											}
										}
									}
								else
									{	/* Eval/expdargs.scm 237 */
										if (
											(CAR(
													((obj_t) BgL_clausez00_13)) ==
												BGl_symbol2537z00zz__expander_argsz00))
											{	/* Eval/expdargs.scm 237 */
												BgL_exprza2za2_1603 = BgL_cdrzd2249zd2_1609;
												{	/* Eval/expdargs.scm 248 */
													obj_t BgL_az00_1663;
													obj_t BgL_vz00_1664;

													{	/* Eval/expdargs.scm 248 */

														{	/* Eval/expdargs.scm 248 */

															BgL_az00_1663 =
																BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
														}
													}
													{	/* Eval/expdargs.scm 249 */

														{	/* Eval/expdargs.scm 249 */

															BgL_vz00_1664 =
																BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
														}
													}
													{	/* Eval/expdargs.scm 250 */
														obj_t BgL_arg1703z00_1665;

														{	/* Eval/expdargs.scm 250 */
															obj_t BgL_arg1704z00_1666;
															obj_t BgL_arg1705z00_1667;

															{	/* Eval/expdargs.scm 250 */
																obj_t BgL_arg1706z00_1668;

																BgL_arg1706z00_1668 =
																	MAKE_YOUNG_PAIR(BgL_vz00_1664, BNIL);
																BgL_arg1704z00_1666 =
																	MAKE_YOUNG_PAIR(BgL_az00_1663,
																	BgL_arg1706z00_1668);
															}
															{	/* Eval/expdargs.scm 251 */
																obj_t BgL_arg1707z00_1669;

																{	/* Eval/expdargs.scm 251 */
																	obj_t BgL_arg1708z00_1670;

																	{	/* Eval/expdargs.scm 251 */
																		obj_t BgL_arg1709z00_1671;
																		obj_t BgL_arg1710z00_1672;

																		{	/* Eval/expdargs.scm 251 */
																			obj_t BgL_arg1711z00_1673;

																			BgL_arg1711z00_1673 =
																				MAKE_YOUNG_PAIR(BgL_az00_1663, BNIL);
																			BgL_arg1709z00_1671 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2546z00zz__expander_argsz00,
																				BgL_arg1711z00_1673);
																		}
																		{	/* Eval/expdargs.scm 252 */
																			obj_t BgL_arg1714z00_1674;
																			obj_t BgL_arg1715z00_1675;

																			{	/* Eval/expdargs.scm 252 */
																				obj_t BgL_arg1717z00_1676;

																				{	/* Eval/expdargs.scm 252 */
																					obj_t BgL_arg1718z00_1677;
																					obj_t BgL_arg1720z00_1678;

																					{	/* Eval/expdargs.scm 252 */
																						obj_t BgL_arg1722z00_1679;
																						obj_t BgL_arg1723z00_1680;

																						{	/* Eval/expdargs.scm 252 */
																							obj_t BgL_arg1724z00_1681;

																							{	/* Eval/expdargs.scm 252 */
																								obj_t BgL_arg1725z00_1682;

																								{	/* Eval/expdargs.scm 252 */
																									obj_t BgL_arg1726z00_1683;

																									BgL_arg1726z00_1683 =
																										MAKE_YOUNG_PAIR
																										(BgL_az00_1663, BNIL);
																									BgL_arg1725z00_1682 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2474z00zz__expander_argsz00,
																										BgL_arg1726z00_1683);
																								}
																								BgL_arg1724z00_1681 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1725z00_1682, BNIL);
																							}
																							BgL_arg1722z00_1679 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2537z00zz__expander_argsz00,
																								BgL_arg1724z00_1681);
																						}
																						{	/* Eval/expdargs.scm 253 */
																							obj_t BgL_arg1727z00_1684;

																							{	/* Eval/expdargs.scm 253 */
																								obj_t BgL_arg1728z00_1685;

																								BgL_arg1728z00_1685 =
																									MAKE_YOUNG_PAIR(BgL_az00_1663,
																									BNIL);
																								BgL_arg1727z00_1684 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2548z00zz__expander_argsz00,
																									BgL_arg1728z00_1685);
																							}
																							BgL_arg1723z00_1680 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1727z00_1684, BNIL);
																						}
																						BgL_arg1718z00_1677 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1722z00_1679,
																							BgL_arg1723z00_1680);
																					}
																					{	/* Eval/expdargs.scm 254 */
																						obj_t BgL_arg1729z00_1686;

																						{	/* Eval/expdargs.scm 254 */
																							obj_t BgL_arg1730z00_1687;

																							{	/* Eval/expdargs.scm 254 */
																								obj_t BgL_arg1731z00_1688;
																								obj_t BgL_arg1733z00_1689;

																								{	/* Eval/expdargs.scm 254 */
																									obj_t BgL_arg1734z00_1690;

																									BgL_arg1734z00_1690 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2512z00zz__expander_argsz00,
																										BNIL);
																									BgL_arg1731z00_1688 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2470z00zz__expander_argsz00,
																										BgL_arg1734z00_1690);
																								}
																								{	/* Eval/expdargs.scm 254 */
																									obj_t BgL_arg1735z00_1691;
																									obj_t BgL_arg1736z00_1692;

																									{	/* Eval/expdargs.scm 254 */
																										obj_t BgL_arg1737z00_1693;

																										BgL_arg1737z00_1693 =
																											MAKE_YOUNG_PAIR
																											(BgL_az00_1663, BNIL);
																										BgL_arg1735z00_1691 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2518z00zz__expander_argsz00,
																											BgL_arg1737z00_1693);
																									}
																									{	/* Eval/expdargs.scm 254 */
																										obj_t BgL_arg1738z00_1694;

																										{	/* Eval/expdargs.scm 254 */
																											obj_t BgL_arg1739z00_1695;

																											BgL_arg1739z00_1695 =
																												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																												(BgL_exprza2za2_1603,
																												BNIL);
																											BgL_arg1738z00_1694 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2544z00zz__expander_argsz00,
																												BgL_arg1739z00_1695);
																										}
																										BgL_arg1736z00_1692 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1738z00_1694,
																											BNIL);
																									}
																									BgL_arg1733z00_1689 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1735z00_1691,
																										BgL_arg1736z00_1692);
																								}
																								BgL_arg1730z00_1687 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1731z00_1688,
																									BgL_arg1733z00_1689);
																							}
																							BgL_arg1729z00_1686 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2472z00zz__expander_argsz00,
																								BgL_arg1730z00_1687);
																						}
																						BgL_arg1720z00_1678 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1729z00_1686, BNIL);
																					}
																					BgL_arg1717z00_1676 =
																						MAKE_YOUNG_PAIR(BgL_arg1718z00_1677,
																						BgL_arg1720z00_1678);
																				}
																				BgL_arg1714z00_1674 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2526z00zz__expander_argsz00,
																					BgL_arg1717z00_1676);
																			}
																			{	/* Eval/expdargs.scm 255 */
																				obj_t BgL_arg1740z00_1696;

																				{	/* Eval/expdargs.scm 255 */
																					obj_t BgL_arg1741z00_1697;

																					{	/* Eval/expdargs.scm 255 */
																						obj_t BgL_arg1743z00_1698;
																						obj_t BgL_arg1744z00_1699;

																						{	/* Eval/expdargs.scm 255 */
																							obj_t BgL_arg1745z00_1700;

																							BgL_arg1745z00_1700 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2516z00zz__expander_argsz00,
																								BNIL);
																							BgL_arg1743z00_1698 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2470z00zz__expander_argsz00,
																								BgL_arg1745z00_1700);
																						}
																						{	/* Eval/expdargs.scm 255 */
																							obj_t BgL_arg1746z00_1701;

																							BgL_arg1746z00_1701 =
																								MAKE_YOUNG_PAIR(BgL_vz00_1664,
																								BNIL);
																							BgL_arg1744z00_1699 =
																								MAKE_YOUNG_PAIR(BgL_az00_1663,
																								BgL_arg1746z00_1701);
																						}
																						BgL_arg1741z00_1697 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1743z00_1698,
																							BgL_arg1744z00_1699);
																					}
																					BgL_arg1740z00_1696 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2472z00zz__expander_argsz00,
																						BgL_arg1741z00_1697);
																				}
																				BgL_arg1715z00_1675 =
																					MAKE_YOUNG_PAIR(BgL_arg1740z00_1696,
																					BNIL);
																			}
																			BgL_arg1710z00_1672 =
																				MAKE_YOUNG_PAIR(BgL_arg1714z00_1674,
																				BgL_arg1715z00_1675);
																		}
																		BgL_arg1708z00_1670 =
																			MAKE_YOUNG_PAIR(BgL_arg1709z00_1671,
																			BgL_arg1710z00_1672);
																	}
																	BgL_arg1707z00_1669 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2480z00zz__expander_argsz00,
																		BgL_arg1708z00_1670);
																}
																BgL_arg1705z00_1667 =
																	MAKE_YOUNG_PAIR(BgL_arg1707z00_1669, BNIL);
															}
															BgL_arg1703z00_1665 =
																MAKE_YOUNG_PAIR(BgL_arg1704z00_1666,
																BgL_arg1705z00_1667);
														}
														return
															MAKE_YOUNG_PAIR
															(BGl_symbol2482z00zz__expander_argsz00,
															BgL_arg1703z00_1665);
													}
												}
											}
										else
											{	/* Eval/expdargs.scm 237 */
												bool_t BgL_test2728z00_3945;

												{	/* Eval/expdargs.scm 237 */
													obj_t BgL_arg1651z00_1631;

													BgL_arg1651z00_1631 = CAR(((obj_t) BgL_clausez00_13));
													BgL_test2728z00_3945 =
														BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1651z00_1631);
												}
												if (BgL_test2728z00_3945)
													{	/* Eval/expdargs.scm 237 */
														return
															BGl_makezd2optzd2parserz00zz__expander_argsz00
															(BgL_clausez00_13, BgL_otablez00_14);
													}
												else
													{	/* Eval/expdargs.scm 237 */
														return
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2456z00zz__expander_argsz00,
															BGl_string2541z00zz__expander_argsz00,
															BgL_clausez00_13);
													}
											}
									}
							}
					}
				else
					{	/* Eval/expdargs.scm 237 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string2456z00zz__expander_argsz00,
							BGl_string2541z00zz__expander_argsz00, BgL_clausez00_13);
					}
			}
		}

	}



/* bind-option! */
	obj_t BGl_bindzd2optionz12zc0zz__expander_argsz00(obj_t BgL_otablez00_15,
		obj_t BgL_oidz00_16, obj_t BgL_clausez00_17)
	{
		{	/* Eval/expdargs.scm 264 */
			{	/* Eval/expdargs.scm 265 */
				obj_t BgL_oldz00_1704;

				BgL_oldz00_1704 =
					BGl_hashtablezd2getzd2zz__hashz00(BgL_otablez00_15, BgL_oidz00_16);
				if (CBOOL(BgL_oldz00_1704))
					{	/* Eval/expdargs.scm 267 */
						obj_t BgL_list1747z00_1705;

						{	/* Eval/expdargs.scm 267 */
							obj_t BgL_arg1748z00_1706;

							{	/* Eval/expdargs.scm 267 */
								obj_t BgL_arg1749z00_1707;

								{	/* Eval/expdargs.scm 267 */
									obj_t BgL_arg1750z00_1708;

									{	/* Eval/expdargs.scm 267 */
										obj_t BgL_arg1751z00_1709;

										{	/* Eval/expdargs.scm 267 */
											obj_t BgL_arg1752z00_1710;

											{	/* Eval/expdargs.scm 267 */
												obj_t BgL_arg1753z00_1711;

												{	/* Eval/expdargs.scm 267 */
													obj_t BgL_arg1754z00_1712;

													{	/* Eval/expdargs.scm 267 */
														obj_t BgL_arg1755z00_1713;

														BgL_arg1755z00_1713 =
															MAKE_YOUNG_PAIR(BgL_clausez00_17, BNIL);
														BgL_arg1754z00_1712 =
															MAKE_YOUNG_PAIR
															(BGl_string2550z00zz__expander_argsz00,
															BgL_arg1755z00_1713);
													}
													BgL_arg1753z00_1711 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
														BgL_arg1754z00_1712);
												}
												BgL_arg1752z00_1710 =
													MAKE_YOUNG_PAIR(BgL_oldz00_1704, BgL_arg1753z00_1711);
											}
											BgL_arg1751z00_1709 =
												MAKE_YOUNG_PAIR(BGl_string2550z00zz__expander_argsz00,
												BgL_arg1752z00_1710);
										}
										BgL_arg1750z00_1708 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
											BgL_arg1751z00_1709);
									}
									BgL_arg1749z00_1707 =
										MAKE_YOUNG_PAIR(BGl_string2551z00zz__expander_argsz00,
										BgL_arg1750z00_1708);
								}
								BgL_arg1748z00_1706 =
									MAKE_YOUNG_PAIR(BgL_oidz00_16, BgL_arg1749z00_1707);
							}
							BgL_list1747z00_1705 =
								MAKE_YOUNG_PAIR(BGl_symbol2504z00zz__expander_argsz00,
								BgL_arg1748z00_1706);
						}
						return BGl_warningz00zz__errorz00(BgL_list1747z00_1705);
					}
				else
					{	/* Eval/expdargs.scm 266 */
						return
							BGl_hashtablezd2putz12zc0zz__hashz00(BgL_otablez00_15,
							BgL_oidz00_16, BgL_clausez00_17);
					}
			}
		}

	}



/* make-opt-parser */
	obj_t BGl_makezd2optzd2parserz00zz__expander_argsz00(obj_t BgL_clausez00_18,
		obj_t BgL_otablez00_19)
	{
		{	/* Eval/expdargs.scm 275 */
			{	/* Eval/expdargs.scm 276 */
				obj_t BgL_optz00_1714;

				BgL_optz00_1714 = CAR(BgL_clausez00_18);
				{	/* Eval/expdargs.scm 276 */
					obj_t BgL_oz00_1715;

					BgL_oz00_1715 = CAR(((obj_t) BgL_optz00_1714));
					{	/* Eval/expdargs.scm 277 */

						if (STRINGP(BgL_oz00_1715))
							{	/* Eval/expdargs.scm 279 */
								BGL_TAIL return
									BGl_makezd2simplezd2optzd2parserzd2zz__expander_argsz00
									(BgL_clausez00_18, BgL_otablez00_19);
							}
						else
							{	/* Eval/expdargs.scm 281 */
								bool_t BgL_test2731z00_3974;

								if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_oz00_1715))
									{
										obj_t BgL_l1132z00_1730;

										BgL_l1132z00_1730 = BgL_oz00_1715;
									BgL_zc3z04anonymousza31763ze3z87_1731:
										if (NULLP(BgL_l1132z00_1730))
											{	/* Eval/expdargs.scm 281 */
												BgL_test2731z00_3974 = ((bool_t) 1);
											}
										else
											{	/* Eval/expdargs.scm 281 */
												bool_t BgL_test2734z00_3979;

												{	/* Eval/expdargs.scm 281 */
													obj_t BgL_tmpz00_3980;

													BgL_tmpz00_3980 = CAR(((obj_t) BgL_l1132z00_1730));
													BgL_test2734z00_3979 = STRINGP(BgL_tmpz00_3980);
												}
												if (BgL_test2734z00_3979)
													{
														obj_t BgL_l1132z00_3984;

														BgL_l1132z00_3984 =
															CDR(((obj_t) BgL_l1132z00_1730));
														BgL_l1132z00_1730 = BgL_l1132z00_3984;
														goto BgL_zc3z04anonymousza31763ze3z87_1731;
													}
												else
													{	/* Eval/expdargs.scm 281 */
														BgL_test2731z00_3974 = ((bool_t) 0);
													}
											}
									}
								else
									{	/* Eval/expdargs.scm 281 */
										BgL_test2731z00_3974 = ((bool_t) 0);
									}
								if (BgL_test2731z00_3974)
									{	/* Eval/expdargs.scm 281 */
										BGL_TAIL return
											BGl_makezd2multiplezd2optzd2parserzd2zz__expander_argsz00
											(BgL_clausez00_18, BgL_otablez00_19);
									}
								else
									{	/* Eval/expdargs.scm 281 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2456z00zz__expander_argsz00,
											BGl_string2477z00zz__expander_argsz00, BgL_clausez00_18);
									}
							}
					}
				}
			}
		}

	}



/* make-simple-opt-parser */
	obj_t BGl_makezd2simplezd2optzd2parserzd2zz__expander_argsz00(obj_t
		BgL_clausez00_20, obj_t BgL_otablez00_21)
	{
		{	/* Eval/expdargs.scm 289 */
			{	/* Eval/expdargs.scm 290 */
				obj_t BgL_optz00_1738;

				BgL_optz00_1738 = CAR(BgL_clausez00_20);
				{	/* Eval/expdargs.scm 290 */
					obj_t BgL_oz00_1739;

					BgL_oz00_1739 = CAR(((obj_t) BgL_optz00_1738));
					{	/* Eval/expdargs.scm 291 */
						obj_t BgL_argsz00_1740;

						BgL_argsz00_1740 =
							BGl_fetchzd2optionzd2argumentsz00zz__expander_argsz00
							(BgL_optz00_1738);
						{	/* Eval/expdargs.scm 292 */
							obj_t BgL_exprza2za2_1741;

							BgL_exprza2za2_1741 = CDR(BgL_clausez00_20);
							{	/* Eval/expdargs.scm 293 */

								{	/* Eval/expdargs.scm 294 */
									obj_t BgL_oidz00_1742;

									BgL_oidz00_1742 =
										BGl_fetchzd2optionzd2embedzd2argumentzd2zz__expander_argsz00
										(BgL_oz00_1739);
									{	/* Eval/expdargs.scm 295 */
										obj_t BgL_aidz00_1743;

										{	/* Eval/expdargs.scm 297 */
											obj_t BgL_tmpz00_2821;

											{	/* Eval/expdargs.scm 297 */
												int BgL_tmpz00_3995;

												BgL_tmpz00_3995 = (int) (1L);
												BgL_tmpz00_2821 = BGL_MVALUES_VAL(BgL_tmpz00_3995);
											}
											{	/* Eval/expdargs.scm 297 */
												int BgL_tmpz00_3998;

												BgL_tmpz00_3998 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_3998, BUNSPEC);
											}
											BgL_aidz00_1743 = BgL_tmpz00_2821;
										}
										{	/* Eval/expdargs.scm 297 */
											bool_t BgL_test2735z00_4001;

											if (CBOOL(BgL_aidz00_1743))
												{	/* Eval/expdargs.scm 297 */
													BgL_test2735z00_4001 = PAIRP(BgL_argsz00_1740);
												}
											else
												{	/* Eval/expdargs.scm 297 */
													BgL_test2735z00_4001 = ((bool_t) 0);
												}
											if (BgL_test2735z00_4001)
												{	/* Eval/expdargs.scm 297 */
													return
														BGl_expandzd2errorzd2zz__expandz00
														(BGl_string2456z00zz__expander_argsz00,
														BGl_string2540z00zz__expander_argsz00,
														BgL_clausez00_20);
												}
											else
												{	/* Eval/expdargs.scm 297 */
													if (STRINGP(BgL_aidz00_1743))
														{	/* Eval/expdargs.scm 299 */
															BGl_bindzd2optionz12zc0zz__expander_argsz00
																(BgL_otablez00_21, BgL_oidz00_1742,
																BgL_clausez00_20);
															{	/* Eval/expdargs.scm 301 */
																obj_t BgL_az00_1746;
																obj_t BgL_vz00_1747;
																long BgL_oidlz00_1748;

																{	/* Eval/expdargs.scm 301 */

																	{	/* Eval/expdargs.scm 301 */

																		BgL_az00_1746 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																}}
																{	/* Eval/expdargs.scm 302 */

																	{	/* Eval/expdargs.scm 302 */

																		BgL_vz00_1747 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																}}
																BgL_oidlz00_1748 =
																	STRING_LENGTH(((obj_t) BgL_oidz00_1742));
																{	/* Eval/expdargs.scm 304 */
																	obj_t BgL_arg1768z00_1749;

																	{	/* Eval/expdargs.scm 304 */
																		obj_t BgL_arg1769z00_1750;
																		obj_t BgL_arg1770z00_1751;

																		{	/* Eval/expdargs.scm 304 */
																			obj_t BgL_arg1771z00_1752;

																			BgL_arg1771z00_1752 =
																				MAKE_YOUNG_PAIR(BgL_vz00_1747, BNIL);
																			BgL_arg1769z00_1750 =
																				MAKE_YOUNG_PAIR(BgL_az00_1746,
																				BgL_arg1771z00_1752);
																		}
																		{	/* Eval/expdargs.scm 305 */
																			obj_t BgL_arg1772z00_1753;

																			{	/* Eval/expdargs.scm 305 */
																				obj_t BgL_arg1773z00_1754;

																				{	/* Eval/expdargs.scm 305 */
																					obj_t BgL_arg1774z00_1755;
																					obj_t BgL_arg1775z00_1756;

																					{	/* Eval/expdargs.scm 305 */
																						obj_t BgL_arg1777z00_1757;

																						{	/* Eval/expdargs.scm 305 */
																							obj_t BgL_arg1779z00_1758;
																							obj_t BgL_arg1781z00_1759;

																							{	/* Eval/expdargs.scm 305 */
																								obj_t BgL_arg1782z00_1760;

																								BgL_arg1782z00_1760 =
																									MAKE_YOUNG_PAIR(BgL_az00_1746,
																									BNIL);
																								BgL_arg1779z00_1758 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2546z00zz__expander_argsz00,
																									BgL_arg1782z00_1760);
																							}
																							{	/* Eval/expdargs.scm 306 */
																								obj_t BgL_arg1783z00_1761;

																								{	/* Eval/expdargs.scm 306 */
																									obj_t BgL_arg1785z00_1762;

																									{	/* Eval/expdargs.scm 306 */
																										obj_t BgL_arg1786z00_1763;

																										{	/* Eval/expdargs.scm 306 */
																											obj_t BgL_arg1787z00_1764;
																											obj_t BgL_arg1788z00_1765;

																											{	/* Eval/expdargs.scm 306 */
																												obj_t
																													BgL_arg1789z00_1766;
																												BgL_arg1789z00_1766 =
																													MAKE_YOUNG_PAIR
																													(BgL_az00_1746, BNIL);
																												BgL_arg1787z00_1764 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2474z00zz__expander_argsz00,
																													BgL_arg1789z00_1766);
																											}
																											BgL_arg1788z00_1765 =
																												MAKE_YOUNG_PAIR(BINT
																												(BgL_oidlz00_1748),
																												BNIL);
																											BgL_arg1786z00_1763 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1787z00_1764,
																												BgL_arg1788z00_1765);
																										}
																										BgL_arg1785z00_1762 =
																											MAKE_YOUNG_PAIR
																											(BgL_oidz00_1742,
																											BgL_arg1786z00_1763);
																									}
																									BgL_arg1783z00_1761 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2552z00zz__expander_argsz00,
																										BgL_arg1785z00_1762);
																								}
																								BgL_arg1781z00_1759 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1783z00_1761, BNIL);
																							}
																							BgL_arg1777z00_1757 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1779z00_1758,
																								BgL_arg1781z00_1759);
																						}
																						BgL_arg1774z00_1755 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2554z00zz__expander_argsz00,
																							BgL_arg1777z00_1757);
																					}
																					{	/* Eval/expdargs.scm 307 */
																						obj_t BgL_arg1790z00_1767;
																						obj_t BgL_arg1791z00_1768;

																						{	/* Eval/expdargs.scm 307 */
																							obj_t BgL_arg1792z00_1769;

																							{	/* Eval/expdargs.scm 307 */
																								obj_t BgL_arg1793z00_1770;
																								obj_t BgL_arg1794z00_1771;

																								{	/* Eval/expdargs.scm 307 */
																									obj_t BgL_arg1795z00_1772;
																									obj_t BgL_arg1796z00_1773;

																									{	/* Eval/expdargs.scm 307 */
																										obj_t BgL_arg1797z00_1774;
																										obj_t BgL_arg1798z00_1775;

																										BgL_arg1797z00_1774 =
																											bstring_to_symbol
																											(BgL_aidz00_1743);
																										{	/* Eval/expdargs.scm 308 */
																											obj_t BgL_arg1799z00_1776;

																											{	/* Eval/expdargs.scm 308 */
																												obj_t
																													BgL_arg1800z00_1777;
																												{	/* Eval/expdargs.scm 308 */
																													obj_t
																														BgL_arg1801z00_1778;
																													obj_t
																														BgL_arg1802z00_1779;
																													{	/* Eval/expdargs.scm 308 */
																														obj_t
																															BgL_arg1803z00_1780;
																														BgL_arg1803z00_1780
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_az00_1746,
																															BNIL);
																														BgL_arg1801z00_1778
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2474z00zz__expander_argsz00,
																															BgL_arg1803z00_1780);
																													}
																													{	/* Eval/expdargs.scm 310 */
																														obj_t
																															BgL_arg1804z00_1781;
																														{	/* Eval/expdargs.scm 310 */
																															obj_t
																																BgL_arg1805z00_1782;
																															{	/* Eval/expdargs.scm 310 */
																																obj_t
																																	BgL_arg1806z00_1783;
																																{	/* Eval/expdargs.scm 310 */
																																	obj_t
																																		BgL_arg1807z00_1784;
																																	{	/* Eval/expdargs.scm 310 */
																																		obj_t
																																			BgL_arg1808z00_1785;
																																		BgL_arg1808z00_1785
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_az00_1746,
																																			BNIL);
																																		BgL_arg1807z00_1784
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2474z00zz__expander_argsz00,
																																			BgL_arg1808z00_1785);
																																	}
																																	BgL_arg1806z00_1783
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1807z00_1784,
																																		BNIL);
																																}
																																BgL_arg1805z00_1782
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2556z00zz__expander_argsz00,
																																	BgL_arg1806z00_1783);
																															}
																															BgL_arg1804z00_1781
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1805z00_1782,
																																BNIL);
																														}
																														BgL_arg1802z00_1779
																															=
																															MAKE_YOUNG_PAIR
																															(BINT
																															(BgL_oidlz00_1748),
																															BgL_arg1804z00_1781);
																													}
																													BgL_arg1800z00_1777 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1801z00_1778,
																														BgL_arg1802z00_1779);
																												}
																												BgL_arg1799z00_1776 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2558z00zz__expander_argsz00,
																													BgL_arg1800z00_1777);
																											}
																											BgL_arg1798z00_1775 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1799z00_1776,
																												BNIL);
																										}
																										BgL_arg1795z00_1772 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1797z00_1774,
																											BgL_arg1798z00_1775);
																									}
																									{	/* Eval/expdargs.scm 311 */
																										obj_t BgL_arg1809z00_1786;
																										obj_t BgL_arg1810z00_1787;

																										{	/* Eval/expdargs.scm 311 */
																											obj_t BgL_arg1811z00_1788;

																											{	/* Eval/expdargs.scm 311 */
																												obj_t
																													BgL_arg1812z00_1789;
																												{	/* Eval/expdargs.scm 311 */
																													obj_t
																														BgL_arg1813z00_1790;
																													BgL_arg1813z00_1790 =
																														MAKE_YOUNG_PAIR
																														(BgL_az00_1746,
																														BNIL);
																													BgL_arg1812z00_1789 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2518z00zz__expander_argsz00,
																														BgL_arg1813z00_1790);
																												}
																												BgL_arg1811z00_1788 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1812z00_1789,
																													BNIL);
																											}
																											BgL_arg1809z00_1786 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2560z00zz__expander_argsz00,
																												BgL_arg1811z00_1788);
																										}
																										{	/* Eval/expdargs.scm 312 */
																											obj_t BgL_arg1814z00_1791;

																											{	/* Eval/expdargs.scm 312 */
																												obj_t
																													BgL_arg1815z00_1792;
																												{	/* Eval/expdargs.scm 312 */
																													obj_t
																														BgL_arg1816z00_1793;
																													{	/* Eval/expdargs.scm 312 */
																														obj_t
																															BgL_arg1817z00_1794;
																														BgL_arg1817z00_1794
																															=
																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																															(BgL_exprza2za2_1741,
																															BNIL);
																														BgL_arg1816z00_1793
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2544z00zz__expander_argsz00,
																															BgL_arg1817z00_1794);
																													}
																													BgL_arg1815z00_1792 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1816z00_1793,
																														BNIL);
																												}
																												BgL_arg1814z00_1791 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2506z00zz__expander_argsz00,
																													BgL_arg1815z00_1792);
																											}
																											BgL_arg1810z00_1787 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1814z00_1791,
																												BNIL);
																										}
																										BgL_arg1796z00_1773 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1809z00_1786,
																											BgL_arg1810z00_1787);
																									}
																									BgL_arg1793z00_1770 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1795z00_1772,
																										BgL_arg1796z00_1773);
																								}
																								{	/* Eval/expdargs.scm 313 */
																									obj_t BgL_arg1818z00_1795;

																									{	/* Eval/expdargs.scm 313 */
																										obj_t BgL_arg1819z00_1796;

																										{	/* Eval/expdargs.scm 313 */
																											obj_t BgL_arg1820z00_1797;
																											obj_t BgL_arg1822z00_1798;

																											{	/* Eval/expdargs.scm 313 */
																												obj_t
																													BgL_arg1823z00_1799;
																												BgL_arg1823z00_1799 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2512z00zz__expander_argsz00,
																													BNIL);
																												BgL_arg1820z00_1797 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2470z00zz__expander_argsz00,
																													BgL_arg1823z00_1799);
																											}
																											{	/* Eval/expdargs.scm 313 */
																												obj_t
																													BgL_arg1826z00_1800;
																												BgL_arg1826z00_1800 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2506z00zz__expander_argsz00,
																													BNIL);
																												BgL_arg1822z00_1798 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2560z00zz__expander_argsz00,
																													BgL_arg1826z00_1800);
																											}
																											BgL_arg1819z00_1796 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1820z00_1797,
																												BgL_arg1822z00_1798);
																										}
																										BgL_arg1818z00_1795 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2472z00zz__expander_argsz00,
																											BgL_arg1819z00_1796);
																									}
																									BgL_arg1794z00_1771 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1818z00_1795, BNIL);
																								}
																								BgL_arg1792z00_1769 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1793z00_1770,
																									BgL_arg1794z00_1771);
																							}
																							BgL_arg1790z00_1767 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2528z00zz__expander_argsz00,
																								BgL_arg1792z00_1769);
																						}
																						{	/* Eval/expdargs.scm 314 */
																							obj_t BgL_arg1827z00_1801;

																							{	/* Eval/expdargs.scm 314 */
																								obj_t BgL_arg1828z00_1802;

																								{	/* Eval/expdargs.scm 314 */
																									obj_t BgL_arg1829z00_1803;
																									obj_t BgL_arg1831z00_1804;

																									{	/* Eval/expdargs.scm 314 */
																										obj_t BgL_arg1832z00_1805;

																										BgL_arg1832z00_1805 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2516z00zz__expander_argsz00,
																											BNIL);
																										BgL_arg1829z00_1803 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2470z00zz__expander_argsz00,
																											BgL_arg1832z00_1805);
																									}
																									{	/* Eval/expdargs.scm 314 */
																										obj_t BgL_arg1833z00_1806;

																										BgL_arg1833z00_1806 =
																											MAKE_YOUNG_PAIR
																											(BgL_vz00_1747, BNIL);
																										BgL_arg1831z00_1804 =
																											MAKE_YOUNG_PAIR
																											(BgL_az00_1746,
																											BgL_arg1833z00_1806);
																									}
																									BgL_arg1828z00_1802 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1829z00_1803,
																										BgL_arg1831z00_1804);
																								}
																								BgL_arg1827z00_1801 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2472z00zz__expander_argsz00,
																									BgL_arg1828z00_1802);
																							}
																							BgL_arg1791z00_1768 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1827z00_1801, BNIL);
																						}
																						BgL_arg1775z00_1756 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1790z00_1767,
																							BgL_arg1791z00_1768);
																					}
																					BgL_arg1773z00_1754 =
																						MAKE_YOUNG_PAIR(BgL_arg1774z00_1755,
																						BgL_arg1775z00_1756);
																				}
																				BgL_arg1772z00_1753 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2480z00zz__expander_argsz00,
																					BgL_arg1773z00_1754);
																			}
																			BgL_arg1770z00_1751 =
																				MAKE_YOUNG_PAIR(BgL_arg1772z00_1753,
																				BNIL);
																		}
																		BgL_arg1768z00_1749 =
																			MAKE_YOUNG_PAIR(BgL_arg1769z00_1750,
																			BgL_arg1770z00_1751);
																	}
																	return
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2482z00zz__expander_argsz00,
																		BgL_arg1768z00_1749);
																}
															}
														}
													else
														{	/* Eval/expdargs.scm 299 */
															BGl_bindzd2optionz12zc0zz__expander_argsz00
																(BgL_otablez00_21, BgL_oidz00_1742,
																BgL_clausez00_20);
															{	/* Eval/expdargs.scm 317 */
																obj_t BgL_az00_1809;
																obj_t BgL_vz00_1810;
																obj_t BgL_naz00_1811;

																{	/* Eval/expdargs.scm 317 */

																	{	/* Eval/expdargs.scm 317 */

																		BgL_az00_1809 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																	}
																}
																{	/* Eval/expdargs.scm 318 */

																	{	/* Eval/expdargs.scm 318 */

																		BgL_vz00_1810 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																	}
																}
																BgL_naz00_1811 =
																	BGl_symbol2560z00zz__expander_argsz00;
																{	/* Eval/expdargs.scm 320 */
																	obj_t BgL_arg1834z00_1812;

																	{	/* Eval/expdargs.scm 320 */
																		obj_t BgL_arg1835z00_1813;
																		obj_t BgL_arg1836z00_1814;

																		{	/* Eval/expdargs.scm 320 */
																			obj_t BgL_arg1837z00_1815;

																			BgL_arg1837z00_1815 =
																				MAKE_YOUNG_PAIR(BgL_vz00_1810, BNIL);
																			BgL_arg1835z00_1813 =
																				MAKE_YOUNG_PAIR(BgL_az00_1809,
																				BgL_arg1837z00_1815);
																		}
																		{	/* Eval/expdargs.scm 321 */
																			obj_t BgL_arg1838z00_1816;

																			{	/* Eval/expdargs.scm 321 */
																				obj_t BgL_arg1839z00_1817;

																				{	/* Eval/expdargs.scm 321 */
																					obj_t BgL_arg1840z00_1818;
																					obj_t BgL_arg1842z00_1819;

																					{	/* Eval/expdargs.scm 321 */
																						obj_t BgL_arg1843z00_1820;

																						{	/* Eval/expdargs.scm 321 */
																							obj_t BgL_arg1844z00_1821;
																							obj_t BgL_arg1845z00_1822;

																							{	/* Eval/expdargs.scm 321 */
																								obj_t BgL_arg1846z00_1823;

																								BgL_arg1846z00_1823 =
																									MAKE_YOUNG_PAIR(BgL_az00_1809,
																									BNIL);
																								BgL_arg1844z00_1821 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2546z00zz__expander_argsz00,
																									BgL_arg1846z00_1823);
																							}
																							{	/* Eval/expdargs.scm 321 */
																								obj_t BgL_arg1847z00_1824;

																								{	/* Eval/expdargs.scm 321 */
																									obj_t BgL_arg1848z00_1825;

																									{	/* Eval/expdargs.scm 321 */
																										obj_t BgL_arg1849z00_1826;

																										{	/* Eval/expdargs.scm 321 */
																											obj_t BgL_arg1850z00_1827;

																											{	/* Eval/expdargs.scm 321 */
																												obj_t
																													BgL_arg1851z00_1828;
																												BgL_arg1851z00_1828 =
																													MAKE_YOUNG_PAIR
																													(BgL_az00_1809, BNIL);
																												BgL_arg1850z00_1827 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2474z00zz__expander_argsz00,
																													BgL_arg1851z00_1828);
																											}
																											BgL_arg1849z00_1826 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1850z00_1827,
																												BNIL);
																										}
																										BgL_arg1848z00_1825 =
																											MAKE_YOUNG_PAIR
																											(BgL_oidz00_1742,
																											BgL_arg1849z00_1826);
																									}
																									BgL_arg1847z00_1824 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2562z00zz__expander_argsz00,
																										BgL_arg1848z00_1825);
																								}
																								BgL_arg1845z00_1822 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1847z00_1824, BNIL);
																							}
																							BgL_arg1843z00_1820 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1844z00_1821,
																								BgL_arg1845z00_1822);
																						}
																						BgL_arg1840z00_1818 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2554z00zz__expander_argsz00,
																							BgL_arg1843z00_1820);
																					}
																					{	/* Eval/expdargs.scm 322 */
																						obj_t BgL_arg1852z00_1829;
																						obj_t BgL_arg1853z00_1830;

																						{	/* Eval/expdargs.scm 322 */
																							obj_t BgL_arg1854z00_1831;

																							{	/* Eval/expdargs.scm 322 */
																								obj_t BgL_arg1856z00_1832;
																								obj_t BgL_arg1857z00_1833;

																								{	/* Eval/expdargs.scm 322 */
																									obj_t BgL_arg1858z00_1834;
																									obj_t BgL_arg1859z00_1835;

																									BgL_arg1858z00_1834 =
																										BGl_bindzd2optionzd2argumentsz00zz__expander_argsz00
																										(BgL_argsz00_1740,
																										BgL_az00_1809,
																										BgL_naz00_1811,
																										BgL_clausez00_20);
																									{	/* Eval/expdargs.scm 323 */
																										obj_t BgL_arg1860z00_1836;

																										{	/* Eval/expdargs.scm 323 */
																											obj_t BgL_arg1862z00_1837;

																											{	/* Eval/expdargs.scm 323 */
																												obj_t
																													BgL_arg1863z00_1838;
																												{	/* Eval/expdargs.scm 323 */
																													obj_t
																														BgL_arg1864z00_1839;
																													BgL_arg1864z00_1839 =
																														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																														(BgL_exprza2za2_1741,
																														BNIL);
																													BgL_arg1863z00_1838 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2544z00zz__expander_argsz00,
																														BgL_arg1864z00_1839);
																												}
																												BgL_arg1862z00_1837 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1863z00_1838,
																													BNIL);
																											}
																											BgL_arg1860z00_1836 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2506z00zz__expander_argsz00,
																												BgL_arg1862z00_1837);
																										}
																										BgL_arg1859z00_1835 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1860z00_1836,
																											BNIL);
																									}
																									BgL_arg1856z00_1832 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_arg1858z00_1834,
																										BgL_arg1859z00_1835);
																								}
																								{	/* Eval/expdargs.scm 324 */
																									obj_t BgL_arg1866z00_1840;

																									{	/* Eval/expdargs.scm 324 */
																										obj_t BgL_arg1868z00_1841;

																										{	/* Eval/expdargs.scm 324 */
																											obj_t BgL_arg1869z00_1842;
																											obj_t BgL_arg1870z00_1843;

																											{	/* Eval/expdargs.scm 324 */
																												obj_t
																													BgL_arg1872z00_1844;
																												BgL_arg1872z00_1844 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2512z00zz__expander_argsz00,
																													BNIL);
																												BgL_arg1869z00_1842 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2470z00zz__expander_argsz00,
																													BgL_arg1872z00_1844);
																											}
																											{	/* Eval/expdargs.scm 324 */
																												obj_t
																													BgL_arg1873z00_1845;
																												BgL_arg1873z00_1845 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2506z00zz__expander_argsz00,
																													BNIL);
																												BgL_arg1870z00_1843 =
																													MAKE_YOUNG_PAIR
																													(BgL_naz00_1811,
																													BgL_arg1873z00_1845);
																											}
																											BgL_arg1868z00_1841 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1869z00_1842,
																												BgL_arg1870z00_1843);
																										}
																										BgL_arg1866z00_1840 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2472z00zz__expander_argsz00,
																											BgL_arg1868z00_1841);
																									}
																									BgL_arg1857z00_1833 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1866z00_1840, BNIL);
																								}
																								BgL_arg1854z00_1831 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1856z00_1832,
																									BgL_arg1857z00_1833);
																							}
																							BgL_arg1852z00_1829 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2528z00zz__expander_argsz00,
																								BgL_arg1854z00_1831);
																						}
																						{	/* Eval/expdargs.scm 325 */
																							obj_t BgL_arg1874z00_1846;

																							{	/* Eval/expdargs.scm 325 */
																								obj_t BgL_arg1875z00_1847;

																								{	/* Eval/expdargs.scm 325 */
																									obj_t BgL_arg1876z00_1848;
																									obj_t BgL_arg1877z00_1849;

																									{	/* Eval/expdargs.scm 325 */
																										obj_t BgL_arg1878z00_1850;

																										BgL_arg1878z00_1850 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2516z00zz__expander_argsz00,
																											BNIL);
																										BgL_arg1876z00_1848 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2470z00zz__expander_argsz00,
																											BgL_arg1878z00_1850);
																									}
																									{	/* Eval/expdargs.scm 325 */
																										obj_t BgL_arg1879z00_1851;

																										BgL_arg1879z00_1851 =
																											MAKE_YOUNG_PAIR
																											(BgL_vz00_1810, BNIL);
																										BgL_arg1877z00_1849 =
																											MAKE_YOUNG_PAIR
																											(BgL_az00_1809,
																											BgL_arg1879z00_1851);
																									}
																									BgL_arg1875z00_1847 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1876z00_1848,
																										BgL_arg1877z00_1849);
																								}
																								BgL_arg1874z00_1846 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2472z00zz__expander_argsz00,
																									BgL_arg1875z00_1847);
																							}
																							BgL_arg1853z00_1830 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1874z00_1846, BNIL);
																						}
																						BgL_arg1842z00_1819 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1852z00_1829,
																							BgL_arg1853z00_1830);
																					}
																					BgL_arg1839z00_1817 =
																						MAKE_YOUNG_PAIR(BgL_arg1840z00_1818,
																						BgL_arg1842z00_1819);
																				}
																				BgL_arg1838z00_1816 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2480z00zz__expander_argsz00,
																					BgL_arg1839z00_1817);
																			}
																			BgL_arg1836z00_1814 =
																				MAKE_YOUNG_PAIR(BgL_arg1838z00_1816,
																				BNIL);
																		}
																		BgL_arg1834z00_1812 =
																			MAKE_YOUNG_PAIR(BgL_arg1835z00_1813,
																			BgL_arg1836z00_1814);
																	}
																	return
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2482z00zz__expander_argsz00,
																		BgL_arg1834z00_1812);
																}
															}
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-multiple-opt-parser */
	obj_t BGl_makezd2multiplezd2optzd2parserzd2zz__expander_argsz00(obj_t
		BgL_clausez00_22, obj_t BgL_otablez00_23)
	{
		{	/* Eval/expdargs.scm 330 */
			{	/* Eval/expdargs.scm 331 */
				obj_t BgL_optz00_1854;

				BgL_optz00_1854 = CAR(BgL_clausez00_22);
				{	/* Eval/expdargs.scm 331 */
					obj_t BgL_ozb2zb2_1855;

					BgL_ozb2zb2_1855 = CAR(((obj_t) BgL_optz00_1854));
					{	/* Eval/expdargs.scm 332 */
						obj_t BgL_argsz00_1856;

						BgL_argsz00_1856 =
							BGl_fetchzd2optionzd2argumentsz00zz__expander_argsz00
							(BgL_optz00_1854);
						{	/* Eval/expdargs.scm 333 */
							obj_t BgL_exprza2za2_1857;

							BgL_exprza2za2_1857 = CDR(BgL_clausez00_22);
							{	/* Eval/expdargs.scm 334 */

								{	/* Eval/expdargs.scm 335 */
									obj_t BgL_oidzb2zb2_1858;

									{
										obj_t BgL_ozb2zb2_2055;
										obj_t BgL_oidzb2zb2_2056;
										obj_t BgL_aidzb2zb2_2057;

										BgL_ozb2zb2_2055 = BgL_ozb2zb2_1855;
										BgL_oidzb2zb2_2056 = BNIL;
										BgL_aidzb2zb2_2057 = BNIL;
									BgL_zc3z04anonymousza32030ze3z87_2058:
										if (NULLP(BgL_ozb2zb2_2055))
											{	/* Eval/expdargs.scm 340 */
												obj_t BgL_val0_1135z00_2060;
												obj_t BgL_val1_1136z00_2061;

												BgL_val0_1135z00_2060 =
													bgl_reverse_bang(BgL_oidzb2zb2_2056);
												BgL_val1_1136z00_2061 =
													bgl_reverse_bang(BgL_aidzb2zb2_2057);
												{	/* Eval/expdargs.scm 340 */
													int BgL_tmpz00_4127;

													BgL_tmpz00_4127 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4127);
												}
												{	/* Eval/expdargs.scm 340 */
													int BgL_tmpz00_4130;

													BgL_tmpz00_4130 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_4130,
														BgL_val1_1136z00_2061);
												}
												BgL_oidzb2zb2_1858 = BgL_val0_1135z00_2060;
											}
										else
											{	/* Eval/expdargs.scm 341 */
												obj_t BgL_oidz00_2062;

												{	/* Eval/expdargs.scm 342 */
													obj_t BgL_arg2037z00_2067;

													BgL_arg2037z00_2067 = CAR(((obj_t) BgL_ozb2zb2_2055));
													BgL_oidz00_2062 =
														BGl_fetchzd2optionzd2embedzd2argumentzd2zz__expander_argsz00
														(BgL_arg2037z00_2067);
												}
												{	/* Eval/expdargs.scm 342 */
													obj_t BgL_aidz00_2063;

													{	/* Eval/expdargs.scm 343 */
														obj_t BgL_tmpz00_2828;

														{	/* Eval/expdargs.scm 343 */
															int BgL_tmpz00_4136;

															BgL_tmpz00_4136 = (int) (1L);
															BgL_tmpz00_2828 =
																BGL_MVALUES_VAL(BgL_tmpz00_4136);
														}
														{	/* Eval/expdargs.scm 343 */
															int BgL_tmpz00_4139;

															BgL_tmpz00_4139 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_4139, BUNSPEC);
														}
														BgL_aidz00_2063 = BgL_tmpz00_2828;
													}
													{	/* Eval/expdargs.scm 343 */
														obj_t BgL_arg2033z00_2064;
														obj_t BgL_arg2034z00_2065;
														obj_t BgL_arg2036z00_2066;

														BgL_arg2033z00_2064 =
															CDR(((obj_t) BgL_ozb2zb2_2055));
														BgL_arg2034z00_2065 =
															MAKE_YOUNG_PAIR(BgL_oidz00_2062,
															BgL_oidzb2zb2_2056);
														BgL_arg2036z00_2066 =
															MAKE_YOUNG_PAIR(BgL_aidz00_2063,
															BgL_aidzb2zb2_2057);
														{
															obj_t BgL_aidzb2zb2_4148;
															obj_t BgL_oidzb2zb2_4147;
															obj_t BgL_ozb2zb2_4146;

															BgL_ozb2zb2_4146 = BgL_arg2033z00_2064;
															BgL_oidzb2zb2_4147 = BgL_arg2034z00_2065;
															BgL_aidzb2zb2_4148 = BgL_arg2036z00_2066;
															BgL_aidzb2zb2_2057 = BgL_aidzb2zb2_4148;
															BgL_oidzb2zb2_2056 = BgL_oidzb2zb2_4147;
															BgL_ozb2zb2_2055 = BgL_ozb2zb2_4146;
															goto BgL_zc3z04anonymousza32030ze3z87_2058;
														}
													}
												}
											}
									}
									{	/* Eval/expdargs.scm 336 */
										obj_t BgL_aidzb2zb2_1859;

										{	/* Eval/expdargs.scm 345 */
											obj_t BgL_tmpz00_2830;

											{	/* Eval/expdargs.scm 345 */
												int BgL_tmpz00_4149;

												BgL_tmpz00_4149 = (int) (1L);
												BgL_tmpz00_2830 = BGL_MVALUES_VAL(BgL_tmpz00_4149);
											}
											{	/* Eval/expdargs.scm 345 */
												int BgL_tmpz00_4152;

												BgL_tmpz00_4152 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_4152, BUNSPEC);
											}
											BgL_aidzb2zb2_1859 = BgL_tmpz00_2830;
										}
										{	/* Eval/expdargs.scm 345 */
											bool_t BgL_test2739z00_4155;

											if (PAIRP(BgL_aidzb2zb2_1859))
												{
													obj_t BgL_l1137z00_2844;

													{	/* Eval/expdargs.scm 345 */
														obj_t BgL_tmpz00_4158;

														BgL_l1137z00_2844 = BgL_aidzb2zb2_1859;
													BgL_zc3z04anonymousza32028ze3z87_2843:
														if (NULLP(BgL_l1137z00_2844))
															{	/* Eval/expdargs.scm 345 */
																BgL_tmpz00_4158 = BFALSE;
															}
														else
															{	/* Eval/expdargs.scm 345 */
																obj_t BgL__ortest_1139z00_2850;

																BgL__ortest_1139z00_2850 =
																	CAR(((obj_t) BgL_l1137z00_2844));
																if (CBOOL(BgL__ortest_1139z00_2850))
																	{	/* Eval/expdargs.scm 345 */
																		BgL_tmpz00_4158 = BgL__ortest_1139z00_2850;
																	}
																else
																	{
																		obj_t BgL_l1137z00_4165;

																		BgL_l1137z00_4165 =
																			CDR(((obj_t) BgL_l1137z00_2844));
																		BgL_l1137z00_2844 = BgL_l1137z00_4165;
																		goto BgL_zc3z04anonymousza32028ze3z87_2843;
																	}
															}
														BgL_test2739z00_4155 = CBOOL(BgL_tmpz00_4158);
													}
												}
											else
												{	/* Eval/expdargs.scm 345 */
													BgL_test2739z00_4155 = ((bool_t) 0);
												}
											if (BgL_test2739z00_4155)
												{	/* Eval/expdargs.scm 345 */
													if (NULLP(BgL_argsz00_1856))
														{	/* Eval/expdargs.scm 359 */
															{
																obj_t BgL_l1147z00_2866;

																BgL_l1147z00_2866 = BgL_oidzb2zb2_1858;
															BgL_zc3z04anonymousza31886ze3z87_2865:
																if (PAIRP(BgL_l1147z00_2866))
																	{	/* Eval/expdargs.scm 360 */
																		BGl_bindzd2optionz12zc0zz__expander_argsz00
																			(BgL_otablez00_23, CAR(BgL_l1147z00_2866),
																			BgL_clausez00_22);
																		{
																			obj_t BgL_l1147z00_4175;

																			BgL_l1147z00_4175 =
																				CDR(BgL_l1147z00_2866);
																			BgL_l1147z00_2866 = BgL_l1147z00_4175;
																			goto
																				BgL_zc3z04anonymousza31886ze3z87_2865;
																		}
																	}
																else
																	{	/* Eval/expdargs.scm 360 */
																		((bool_t) 1);
																	}
															}
															{	/* Eval/expdargs.scm 361 */
																obj_t BgL_az00_1878;
																obj_t BgL_vz00_1879;
																obj_t BgL_naz00_1880;

																{	/* Eval/expdargs.scm 361 */

																	{	/* Eval/expdargs.scm 361 */

																		BgL_az00_1878 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																	}
																}
																{	/* Eval/expdargs.scm 362 */

																	{	/* Eval/expdargs.scm 362 */

																		BgL_vz00_1879 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																	}
																}
																BgL_naz00_1880 =
																	BGl_symbol2560z00zz__expander_argsz00;
																{	/* Eval/expdargs.scm 364 */
																	obj_t BgL_arg1889z00_1881;

																	{	/* Eval/expdargs.scm 364 */
																		obj_t BgL_arg1890z00_1882;
																		obj_t BgL_arg1891z00_1883;

																		{	/* Eval/expdargs.scm 364 */
																			obj_t BgL_arg1892z00_1884;

																			BgL_arg1892z00_1884 =
																				MAKE_YOUNG_PAIR(BgL_vz00_1879, BNIL);
																			BgL_arg1890z00_1882 =
																				MAKE_YOUNG_PAIR(BgL_az00_1878,
																				BgL_arg1892z00_1884);
																		}
																		{	/* Eval/expdargs.scm 366 */
																			obj_t BgL_arg1893z00_1885;

																			{	/* Eval/expdargs.scm 366 */
																				obj_t BgL_arg1894z00_1886;

																				{	/* Eval/expdargs.scm 366 */
																					obj_t BgL_arg1896z00_1887;
																					obj_t BgL_arg1897z00_1888;

																					{	/* Eval/expdargs.scm 366 */
																						obj_t BgL_arg1898z00_1889;
																						obj_t BgL_arg1899z00_1890;

																						{	/* Eval/expdargs.scm 366 */
																							obj_t BgL_arg1901z00_1891;

																							BgL_arg1901z00_1891 =
																								MAKE_YOUNG_PAIR(BgL_az00_1878,
																								BNIL);
																							BgL_arg1898z00_1889 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2546z00zz__expander_argsz00,
																								BgL_arg1901z00_1891);
																						}
																						{	/* Eval/expdargs.scm 367 */
																							obj_t BgL_arg1902z00_1892;

																							{	/* Eval/expdargs.scm 367 */
																								obj_t BgL_arg1903z00_1893;

																								{	/* Eval/expdargs.scm 367 */
																									obj_t BgL_arg1904z00_1894;
																									obj_t BgL_arg1906z00_1895;

																									if (NULLP(BgL_oidzb2zb2_1858))
																										{	/* Eval/expdargs.scm 367 */
																											BgL_arg1904z00_1894 =
																												BNIL;
																										}
																									else
																										{	/* Eval/expdargs.scm 367 */
																											obj_t
																												BgL_head1151z00_1899;
																											BgL_head1151z00_1899 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t
																													BgL_ll1149z00_1901;
																												obj_t
																													BgL_ll1150z00_1902;
																												obj_t
																													BgL_tail1152z00_1903;
																												BgL_ll1149z00_1901 =
																													BgL_oidzb2zb2_1858;
																												BgL_ll1150z00_1902 =
																													BgL_aidzb2zb2_1859;
																												BgL_tail1152z00_1903 =
																													BgL_head1151z00_1899;
																											BgL_zc3z04anonymousza31908ze3z87_1904:
																												if (NULLP
																													(BgL_ll1149z00_1901))
																													{	/* Eval/expdargs.scm 367 */
																														BgL_arg1904z00_1894
																															=
																															CDR
																															(BgL_head1151z00_1899);
																													}
																												else
																													{	/* Eval/expdargs.scm 367 */
																														obj_t
																															BgL_newtail1153z00_1906;
																														{	/* Eval/expdargs.scm 367 */
																															obj_t
																																BgL_arg1912z00_1909;
																															{	/* Eval/expdargs.scm 367 */
																																obj_t
																																	BgL_oidz00_1910;
																																obj_t
																																	BgL_aidz00_1911;
																																BgL_oidz00_1910
																																	=
																																	CAR(((obj_t)
																																		BgL_ll1149z00_1901));
																																BgL_aidz00_1911
																																	=
																																	CAR(((obj_t)
																																		BgL_ll1150z00_1902));
																																if (CBOOL
																																	(BgL_aidz00_1911))
																																	{	/* Eval/expdargs.scm 369 */
																																		long
																																			BgL_oidlz00_1912;
																																		BgL_oidlz00_1912
																																			=
																																			STRING_LENGTH
																																			(((obj_t)
																																				BgL_oidz00_1910));
																																		{	/* Eval/expdargs.scm 370 */
																																			obj_t
																																				BgL_arg1913z00_1913;
																																			obj_t
																																				BgL_arg1914z00_1914;
																																			{	/* Eval/expdargs.scm 370 */
																																				obj_t
																																					BgL_arg1916z00_1915;
																																				{	/* Eval/expdargs.scm 370 */
																																					obj_t
																																						BgL_arg1917z00_1916;
																																					{	/* Eval/expdargs.scm 370 */
																																						obj_t
																																							BgL_arg1918z00_1917;
																																						obj_t
																																							BgL_arg1919z00_1918;
																																						{	/* Eval/expdargs.scm 370 */
																																							obj_t
																																								BgL_arg1920z00_1919;
																																							BgL_arg1920z00_1919
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_az00_1878,
																																								BNIL);
																																							BgL_arg1918z00_1917
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2474z00zz__expander_argsz00,
																																								BgL_arg1920z00_1919);
																																						}
																																						BgL_arg1919z00_1918
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BINT
																																							(BgL_oidlz00_1912),
																																							BNIL);
																																						BgL_arg1917z00_1916
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1918z00_1917,
																																							BgL_arg1919z00_1918);
																																					}
																																					BgL_arg1916z00_1915
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_oidz00_1910,
																																						BgL_arg1917z00_1916);
																																				}
																																				BgL_arg1913z00_1913
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2552z00zz__expander_argsz00,
																																					BgL_arg1916z00_1915);
																																			}
																																			{	/* Eval/expdargs.scm 371 */
																																				obj_t
																																					BgL_arg1923z00_1920;
																																				{	/* Eval/expdargs.scm 371 */
																																					obj_t
																																						BgL_arg1924z00_1921;
																																					{	/* Eval/expdargs.scm 371 */
																																						obj_t
																																							BgL_arg1925z00_1922;
																																						obj_t
																																							BgL_arg1926z00_1923;
																																						{	/* Eval/expdargs.scm 371 */
																																							obj_t
																																								BgL_arg1927z00_1924;
																																							obj_t
																																								BgL_arg1928z00_1925;
																																							{	/* Eval/expdargs.scm 371 */
																																								obj_t
																																									BgL_arg1929z00_1926;
																																								obj_t
																																									BgL_arg1930z00_1927;
																																								BgL_arg1929z00_1926
																																									=
																																									bstring_to_symbol
																																									(
																																									((obj_t) BgL_aidz00_1911));
																																								{	/* Eval/expdargs.scm 373 */
																																									obj_t
																																										BgL_arg1931z00_1928;
																																									{	/* Eval/expdargs.scm 373 */
																																										obj_t
																																											BgL_arg1932z00_1929;
																																										{	/* Eval/expdargs.scm 373 */
																																											obj_t
																																												BgL_arg1933z00_1930;
																																											obj_t
																																												BgL_arg1934z00_1931;
																																											{	/* Eval/expdargs.scm 373 */
																																												obj_t
																																													BgL_arg1935z00_1932;
																																												BgL_arg1935z00_1932
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_az00_1878,
																																													BNIL);
																																												BgL_arg1933z00_1930
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2474z00zz__expander_argsz00,
																																													BgL_arg1935z00_1932);
																																											}
																																											{	/* Eval/expdargs.scm 375 */
																																												obj_t
																																													BgL_arg1936z00_1933;
																																												{	/* Eval/expdargs.scm 375 */
																																													obj_t
																																														BgL_arg1937z00_1934;
																																													{	/* Eval/expdargs.scm 375 */
																																														obj_t
																																															BgL_arg1938z00_1935;
																																														{	/* Eval/expdargs.scm 375 */
																																															obj_t
																																																BgL_arg1939z00_1936;
																																															{	/* Eval/expdargs.scm 375 */
																																																obj_t
																																																	BgL_arg1940z00_1937;
																																																BgL_arg1940z00_1937
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_az00_1878,
																																																	BNIL);
																																																BgL_arg1939z00_1936
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_symbol2474z00zz__expander_argsz00,
																																																	BgL_arg1940z00_1937);
																																															}
																																															BgL_arg1938z00_1935
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg1939z00_1936,
																																																BNIL);
																																														}
																																														BgL_arg1937z00_1934
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2556z00zz__expander_argsz00,
																																															BgL_arg1938z00_1935);
																																													}
																																													BgL_arg1936z00_1933
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1937z00_1934,
																																														BNIL);
																																												}
																																												BgL_arg1934z00_1931
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BINT
																																													(BgL_oidlz00_1912),
																																													BgL_arg1936z00_1933);
																																											}
																																											BgL_arg1932z00_1929
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1933z00_1930,
																																												BgL_arg1934z00_1931);
																																										}
																																										BgL_arg1931z00_1928
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2558z00zz__expander_argsz00,
																																											BgL_arg1932z00_1929);
																																									}
																																									BgL_arg1930z00_1927
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1931z00_1928,
																																										BNIL);
																																								}
																																								BgL_arg1927z00_1924
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1929z00_1926,
																																									BgL_arg1930z00_1927);
																																							}
																																							{	/* Eval/expdargs.scm 376 */
																																								obj_t
																																									BgL_arg1941z00_1938;
																																								obj_t
																																									BgL_arg1942z00_1939;
																																								BgL_arg1941z00_1938
																																									=
																																									BGl_bindzd2optionzd2argumentsz00zz__expander_argsz00
																																									(BgL_argsz00_1856,
																																									BgL_az00_1878,
																																									BgL_naz00_1880,
																																									BgL_clausez00_22);
																																								{	/* Eval/expdargs.scm 377 */
																																									obj_t
																																										BgL_arg1943z00_1940;
																																									{	/* Eval/expdargs.scm 377 */
																																										obj_t
																																											BgL_arg1944z00_1941;
																																										{	/* Eval/expdargs.scm 377 */
																																											obj_t
																																												BgL_arg1945z00_1942;
																																											{	/* Eval/expdargs.scm 377 */
																																												obj_t
																																													BgL_arg1946z00_1943;
																																												BgL_arg1946z00_1943
																																													=
																																													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																													(BgL_exprza2za2_1857,
																																													BNIL);
																																												BgL_arg1945z00_1942
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2544z00zz__expander_argsz00,
																																													BgL_arg1946z00_1943);
																																											}
																																											BgL_arg1944z00_1941
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1945z00_1942,
																																												BNIL);
																																										}
																																										BgL_arg1943z00_1940
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2506z00zz__expander_argsz00,
																																											BgL_arg1944z00_1941);
																																									}
																																									BgL_arg1942z00_1939
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1943z00_1940,
																																										BNIL);
																																								}
																																								BgL_arg1928z00_1925
																																									=
																																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																									(BgL_arg1941z00_1938,
																																									BgL_arg1942z00_1939);
																																							}
																																							BgL_arg1925z00_1922
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1927z00_1924,
																																								BgL_arg1928z00_1925);
																																						}
																																						{	/* Eval/expdargs.scm 378 */
																																							obj_t
																																								BgL_arg1947z00_1944;
																																							{	/* Eval/expdargs.scm 378 */
																																								obj_t
																																									BgL_arg1948z00_1945;
																																								{	/* Eval/expdargs.scm 378 */
																																									obj_t
																																										BgL_arg1949z00_1946;
																																									obj_t
																																										BgL_arg1950z00_1947;
																																									{	/* Eval/expdargs.scm 378 */
																																										obj_t
																																											BgL_arg1951z00_1948;
																																										BgL_arg1951z00_1948
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2512z00zz__expander_argsz00,
																																											BNIL);
																																										BgL_arg1949z00_1946
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2470z00zz__expander_argsz00,
																																											BgL_arg1951z00_1948);
																																									}
																																									{	/* Eval/expdargs.scm 378 */
																																										obj_t
																																											BgL_arg1952z00_1949;
																																										BgL_arg1952z00_1949
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2506z00zz__expander_argsz00,
																																											BNIL);
																																										BgL_arg1950z00_1947
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_naz00_1880,
																																											BgL_arg1952z00_1949);
																																									}
																																									BgL_arg1948z00_1945
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1949z00_1946,
																																										BgL_arg1950z00_1947);
																																								}
																																								BgL_arg1947z00_1944
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2472z00zz__expander_argsz00,
																																									BgL_arg1948z00_1945);
																																							}
																																							BgL_arg1926z00_1923
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1947z00_1944,
																																								BNIL);
																																						}
																																						BgL_arg1924z00_1921
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1925z00_1922,
																																							BgL_arg1926z00_1923);
																																					}
																																					BgL_arg1923z00_1920
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2528z00zz__expander_argsz00,
																																						BgL_arg1924z00_1921);
																																				}
																																				BgL_arg1914z00_1914
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1923z00_1920,
																																					BNIL);
																																			}
																																			BgL_arg1912z00_1909
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1913z00_1913,
																																				BgL_arg1914z00_1914);
																																	}}
																																else
																																	{	/* Eval/expdargs.scm 379 */
																																		obj_t
																																			BgL_arg1953z00_1950;
																																		{	/* Eval/expdargs.scm 379 */
																																			obj_t
																																				BgL_arg1954z00_1951;
																																			{	/* Eval/expdargs.scm 379 */
																																				obj_t
																																					BgL_arg1955z00_1952;
																																				{	/* Eval/expdargs.scm 379 */
																																					obj_t
																																						BgL_arg1956z00_1953;
																																					obj_t
																																						BgL_arg1957z00_1954;
																																					{	/* Eval/expdargs.scm 379 */
																																						obj_t
																																							BgL_arg1958z00_1955;
																																						BgL_arg1958z00_1955
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2516z00zz__expander_argsz00,
																																							BNIL);
																																						BgL_arg1956z00_1953
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2470z00zz__expander_argsz00,
																																							BgL_arg1958z00_1955);
																																					}
																																					{	/* Eval/expdargs.scm 379 */
																																						obj_t
																																							BgL_arg1959z00_1956;
																																						BgL_arg1959z00_1956
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_vz00_1879,
																																							BNIL);
																																						BgL_arg1957z00_1954
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_az00_1878,
																																							BgL_arg1959z00_1956);
																																					}
																																					BgL_arg1955z00_1952
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1956z00_1953,
																																						BgL_arg1957z00_1954);
																																				}
																																				BgL_arg1954z00_1951
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2472z00zz__expander_argsz00,
																																					BgL_arg1955z00_1952);
																																			}
																																			BgL_arg1953z00_1950
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1954z00_1951,
																																				BNIL);
																																		}
																																		BgL_arg1912z00_1909
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2537z00zz__expander_argsz00,
																																			BgL_arg1953z00_1950);
																																	}
																															}
																															BgL_newtail1153z00_1906
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1912z00_1909,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1152z00_1903,
																															BgL_newtail1153z00_1906);
																														{	/* Eval/expdargs.scm 367 */
																															obj_t
																																BgL_arg1910z00_1907;
																															obj_t
																																BgL_arg1911z00_1908;
																															BgL_arg1910z00_1907
																																=
																																CDR(((obj_t)
																																	BgL_ll1149z00_1901));
																															BgL_arg1911z00_1908
																																=
																																CDR(((obj_t)
																																	BgL_ll1150z00_1902));
																															{
																																obj_t
																																	BgL_tail1152z00_4254;
																																obj_t
																																	BgL_ll1150z00_4253;
																																obj_t
																																	BgL_ll1149z00_4252;
																																BgL_ll1149z00_4252
																																	=
																																	BgL_arg1910z00_1907;
																																BgL_ll1150z00_4253
																																	=
																																	BgL_arg1911z00_1908;
																																BgL_tail1152z00_4254
																																	=
																																	BgL_newtail1153z00_1906;
																																BgL_tail1152z00_1903
																																	=
																																	BgL_tail1152z00_4254;
																																BgL_ll1150z00_1902
																																	=
																																	BgL_ll1150z00_4253;
																																BgL_ll1149z00_1901
																																	=
																																	BgL_ll1149z00_4252;
																																goto
																																	BgL_zc3z04anonymousza31908ze3z87_1904;
																															}
																														}
																													}
																											}
																										}
																									{	/* Eval/expdargs.scm 381 */
																										obj_t BgL_arg1960z00_1958;

																										{	/* Eval/expdargs.scm 381 */
																											obj_t BgL_arg1961z00_1959;

																											{	/* Eval/expdargs.scm 381 */
																												obj_t
																													BgL_arg1962z00_1960;
																												{	/* Eval/expdargs.scm 381 */
																													obj_t
																														BgL_arg1963z00_1961;
																													{	/* Eval/expdargs.scm 381 */
																														obj_t
																															BgL_arg1964z00_1962;
																														obj_t
																															BgL_arg1965z00_1963;
																														{	/* Eval/expdargs.scm 381 */
																															obj_t
																																BgL_arg1966z00_1964;
																															BgL_arg1966z00_1964
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2516z00zz__expander_argsz00,
																																BNIL);
																															BgL_arg1964z00_1962
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2470z00zz__expander_argsz00,
																																BgL_arg1966z00_1964);
																														}
																														{	/* Eval/expdargs.scm 381 */
																															obj_t
																																BgL_arg1967z00_1965;
																															BgL_arg1967z00_1965
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_vz00_1879,
																																BNIL);
																															BgL_arg1965z00_1963
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_az00_1878,
																																BgL_arg1967z00_1965);
																														}
																														BgL_arg1963z00_1961
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1964z00_1962,
																															BgL_arg1965z00_1963);
																													}
																													BgL_arg1962z00_1960 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2472z00zz__expander_argsz00,
																														BgL_arg1963z00_1961);
																												}
																												BgL_arg1961z00_1959 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1962z00_1960,
																													BNIL);
																											}
																											BgL_arg1960z00_1958 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2537z00zz__expander_argsz00,
																												BgL_arg1961z00_1959);
																										}
																										BgL_arg1906z00_1895 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1960z00_1958,
																											BNIL);
																									}
																									BgL_arg1903z00_1893 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_arg1904z00_1894,
																										BgL_arg1906z00_1895);
																								}
																								BgL_arg1902z00_1892 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2564z00zz__expander_argsz00,
																									BgL_arg1903z00_1893);
																							}
																							BgL_arg1899z00_1890 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1902z00_1892, BNIL);
																						}
																						BgL_arg1896z00_1887 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1898z00_1889,
																							BgL_arg1899z00_1890);
																					}
																					{	/* Eval/expdargs.scm 383 */
																						obj_t BgL_arg1968z00_1966;

																						{	/* Eval/expdargs.scm 383 */
																							obj_t BgL_arg1969z00_1967;

																							{	/* Eval/expdargs.scm 383 */
																								obj_t BgL_arg1970z00_1968;

																								{	/* Eval/expdargs.scm 383 */
																									obj_t BgL_arg1971z00_1969;

																									{	/* Eval/expdargs.scm 383 */
																										obj_t BgL_arg1972z00_1970;
																										obj_t BgL_arg1973z00_1971;

																										{	/* Eval/expdargs.scm 383 */
																											obj_t BgL_arg1974z00_1972;

																											BgL_arg1974z00_1972 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2516z00zz__expander_argsz00,
																												BNIL);
																											BgL_arg1972z00_1970 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2470z00zz__expander_argsz00,
																												BgL_arg1974z00_1972);
																										}
																										{	/* Eval/expdargs.scm 383 */
																											obj_t BgL_arg1975z00_1973;

																											BgL_arg1975z00_1973 =
																												MAKE_YOUNG_PAIR
																												(BgL_vz00_1879, BNIL);
																											BgL_arg1973z00_1971 =
																												MAKE_YOUNG_PAIR
																												(BgL_az00_1878,
																												BgL_arg1975z00_1973);
																										}
																										BgL_arg1971z00_1969 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1972z00_1970,
																											BgL_arg1973z00_1971);
																									}
																									BgL_arg1970z00_1968 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2472z00zz__expander_argsz00,
																										BgL_arg1971z00_1969);
																								}
																								BgL_arg1969z00_1967 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1970z00_1968, BNIL);
																							}
																							BgL_arg1968z00_1966 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2537z00zz__expander_argsz00,
																								BgL_arg1969z00_1967);
																						}
																						BgL_arg1897z00_1888 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1968z00_1966, BNIL);
																					}
																					BgL_arg1894z00_1886 =
																						MAKE_YOUNG_PAIR(BgL_arg1896z00_1887,
																						BgL_arg1897z00_1888);
																				}
																				BgL_arg1893z00_1885 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2564z00zz__expander_argsz00,
																					BgL_arg1894z00_1886);
																			}
																			BgL_arg1891z00_1883 =
																				MAKE_YOUNG_PAIR(BgL_arg1893z00_1885,
																				BNIL);
																		}
																		BgL_arg1889z00_1881 =
																			MAKE_YOUNG_PAIR(BgL_arg1890z00_1882,
																			BgL_arg1891z00_1883);
																	}
																	return
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2482z00zz__expander_argsz00,
																		BgL_arg1889z00_1881);
																}
															}
														}
													else
														{	/* Eval/expdargs.scm 359 */
															return
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string2456z00zz__expander_argsz00,
																BGl_string2540z00zz__expander_argsz00,
																BgL_clausez00_22);
														}
												}
											else
												{	/* Eval/expdargs.scm 345 */
													{
														obj_t BgL_l1140z00_2894;

														BgL_l1140z00_2894 = BgL_oidzb2zb2_1858;
													BgL_zc3z04anonymousza31976ze3z87_2893:
														if (PAIRP(BgL_l1140z00_2894))
															{	/* Eval/expdargs.scm 346 */
																BGl_bindzd2optionz12zc0zz__expander_argsz00
																	(BgL_otablez00_23, CAR(BgL_l1140z00_2894),
																	BgL_clausez00_22);
																{
																	obj_t BgL_l1140z00_4287;

																	BgL_l1140z00_4287 = CDR(BgL_l1140z00_2894);
																	BgL_l1140z00_2894 = BgL_l1140z00_4287;
																	goto BgL_zc3z04anonymousza31976ze3z87_2893;
																}
															}
														else
															{	/* Eval/expdargs.scm 346 */
																((bool_t) 1);
															}
													}
													{	/* Eval/expdargs.scm 347 */
														obj_t BgL_az00_1983;
														obj_t BgL_vz00_1984;
														obj_t BgL_naz00_1985;

														{	/* Eval/expdargs.scm 347 */

															{	/* Eval/expdargs.scm 347 */

																BgL_az00_1983 =
																	BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
															}
														}
														{	/* Eval/expdargs.scm 348 */

															{	/* Eval/expdargs.scm 348 */

																BgL_vz00_1984 =
																	BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
															}
														}
														BgL_naz00_1985 =
															BGl_symbol2560z00zz__expander_argsz00;
														{	/* Eval/expdargs.scm 350 */
															obj_t BgL_arg1979z00_1986;

															{	/* Eval/expdargs.scm 350 */
																obj_t BgL_arg1980z00_1987;
																obj_t BgL_arg1981z00_1988;

																{	/* Eval/expdargs.scm 350 */
																	obj_t BgL_arg1982z00_1989;

																	BgL_arg1982z00_1989 =
																		MAKE_YOUNG_PAIR(BgL_vz00_1984, BNIL);
																	BgL_arg1980z00_1987 =
																		MAKE_YOUNG_PAIR(BgL_az00_1983,
																		BgL_arg1982z00_1989);
																}
																{	/* Eval/expdargs.scm 351 */
																	obj_t BgL_arg1983z00_1990;

																	{	/* Eval/expdargs.scm 351 */
																		obj_t BgL_arg1984z00_1991;

																		{	/* Eval/expdargs.scm 351 */
																			obj_t BgL_arg1985z00_1992;
																			obj_t BgL_arg1986z00_1993;

																			{	/* Eval/expdargs.scm 351 */
																				obj_t BgL_arg1987z00_1994;

																				{	/* Eval/expdargs.scm 351 */
																					obj_t BgL_arg1988z00_1995;
																					obj_t BgL_arg1989z00_1996;

																					{	/* Eval/expdargs.scm 351 */
																						obj_t BgL_arg1990z00_1997;

																						BgL_arg1990z00_1997 =
																							MAKE_YOUNG_PAIR(BgL_az00_1983,
																							BNIL);
																						BgL_arg1988z00_1995 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2546z00zz__expander_argsz00,
																							BgL_arg1990z00_1997);
																					}
																					{	/* Eval/expdargs.scm 352 */
																						obj_t BgL_arg1991z00_1998;

																						{	/* Eval/expdargs.scm 352 */
																							obj_t BgL_arg1992z00_1999;

																							{	/* Eval/expdargs.scm 352 */
																								obj_t BgL_arg1993z00_2000;

																								if (NULLP(BgL_ozb2zb2_1855))
																									{	/* Eval/expdargs.scm 352 */
																										BgL_arg1993z00_2000 = BNIL;
																									}
																								else
																									{	/* Eval/expdargs.scm 352 */
																										obj_t BgL_head1144z00_2003;

																										BgL_head1144z00_2003 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										{
																											obj_t BgL_l1142z00_2005;
																											obj_t
																												BgL_tail1145z00_2006;
																											BgL_l1142z00_2005 =
																												BgL_ozb2zb2_1855;
																											BgL_tail1145z00_2006 =
																												BgL_head1144z00_2003;
																										BgL_zc3z04anonymousza31995ze3z87_2007:
																											if (NULLP
																												(BgL_l1142z00_2005))
																												{	/* Eval/expdargs.scm 352 */
																													BgL_arg1993z00_2000 =
																														CDR
																														(BgL_head1144z00_2003);
																												}
																											else
																												{	/* Eval/expdargs.scm 352 */
																													obj_t
																														BgL_newtail1146z00_2009;
																													{	/* Eval/expdargs.scm 352 */
																														obj_t
																															BgL_arg1998z00_2011;
																														{	/* Eval/expdargs.scm 352 */
																															obj_t
																																BgL_oz00_2012;
																															BgL_oz00_2012 =
																																CAR(((obj_t)
																																	BgL_l1142z00_2005));
																															{	/* Eval/expdargs.scm 353 */
																																obj_t
																																	BgL_arg1999z00_2013;
																																{	/* Eval/expdargs.scm 353 */
																																	obj_t
																																		BgL_arg2000z00_2014;
																																	{	/* Eval/expdargs.scm 353 */
																																		obj_t
																																			BgL_arg2001z00_2015;
																																		{	/* Eval/expdargs.scm 353 */
																																			obj_t
																																				BgL_arg2002z00_2016;
																																			BgL_arg2002z00_2016
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_az00_1983,
																																				BNIL);
																																			BgL_arg2001z00_2015
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2474z00zz__expander_argsz00,
																																				BgL_arg2002z00_2016);
																																		}
																																		BgL_arg2000z00_2014
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2001z00_2015,
																																			BNIL);
																																	}
																																	BgL_arg1999z00_2013
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_oz00_2012,
																																		BgL_arg2000z00_2014);
																																}
																																BgL_arg1998z00_2011
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2562z00zz__expander_argsz00,
																																	BgL_arg1999z00_2013);
																															}
																														}
																														BgL_newtail1146z00_2009
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1998z00_2011,
																															BNIL);
																													}
																													SET_CDR
																														(BgL_tail1145z00_2006,
																														BgL_newtail1146z00_2009);
																													{	/* Eval/expdargs.scm 352 */
																														obj_t
																															BgL_arg1997z00_2010;
																														BgL_arg1997z00_2010
																															=
																															CDR(((obj_t)
																																BgL_l1142z00_2005));
																														{
																															obj_t
																																BgL_tail1145z00_4313;
																															obj_t
																																BgL_l1142z00_4312;
																															BgL_l1142z00_4312
																																=
																																BgL_arg1997z00_2010;
																															BgL_tail1145z00_4313
																																=
																																BgL_newtail1146z00_2009;
																															BgL_tail1145z00_2006
																																=
																																BgL_tail1145z00_4313;
																															BgL_l1142z00_2005
																																=
																																BgL_l1142z00_4312;
																															goto
																																BgL_zc3z04anonymousza31995ze3z87_2007;
																														}
																													}
																												}
																										}
																									}
																								BgL_arg1992z00_1999 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_arg1993z00_2000, BNIL);
																							}
																							BgL_arg1991z00_1998 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2566z00zz__expander_argsz00,
																								BgL_arg1992z00_1999);
																						}
																						BgL_arg1989z00_1996 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1991z00_1998, BNIL);
																					}
																					BgL_arg1987z00_1994 =
																						MAKE_YOUNG_PAIR(BgL_arg1988z00_1995,
																						BgL_arg1989z00_1996);
																				}
																				BgL_arg1985z00_1992 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2554z00zz__expander_argsz00,
																					BgL_arg1987z00_1994);
																			}
																			{	/* Eval/expdargs.scm 355 */
																				obj_t BgL_arg2003z00_2018;
																				obj_t BgL_arg2004z00_2019;

																				{	/* Eval/expdargs.scm 355 */
																					obj_t BgL_arg2006z00_2020;

																					{	/* Eval/expdargs.scm 355 */
																						obj_t BgL_arg2007z00_2021;
																						obj_t BgL_arg2008z00_2022;

																						{	/* Eval/expdargs.scm 355 */
																							obj_t BgL_arg2009z00_2023;
																							obj_t BgL_arg2010z00_2024;

																							BgL_arg2009z00_2023 =
																								BGl_bindzd2optionzd2argumentsz00zz__expander_argsz00
																								(BgL_argsz00_1856,
																								BgL_az00_1983, BgL_naz00_1985,
																								BgL_clausez00_22);
																							{	/* Eval/expdargs.scm 356 */
																								obj_t BgL_arg2011z00_2025;

																								{	/* Eval/expdargs.scm 356 */
																									obj_t BgL_arg2012z00_2026;

																									{	/* Eval/expdargs.scm 356 */
																										obj_t BgL_arg2013z00_2027;

																										{	/* Eval/expdargs.scm 356 */
																											obj_t BgL_arg2014z00_2028;

																											BgL_arg2014z00_2028 =
																												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																												(BgL_exprza2za2_1857,
																												BNIL);
																											BgL_arg2013z00_2027 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2544z00zz__expander_argsz00,
																												BgL_arg2014z00_2028);
																										}
																										BgL_arg2012z00_2026 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2013z00_2027,
																											BNIL);
																									}
																									BgL_arg2011z00_2025 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2506z00zz__expander_argsz00,
																										BgL_arg2012z00_2026);
																								}
																								BgL_arg2010z00_2024 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2011z00_2025, BNIL);
																							}
																							BgL_arg2007z00_2021 =
																								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																								(BgL_arg2009z00_2023,
																								BgL_arg2010z00_2024);
																						}
																						{	/* Eval/expdargs.scm 357 */
																							obj_t BgL_arg2015z00_2029;

																							{	/* Eval/expdargs.scm 357 */
																								obj_t BgL_arg2016z00_2030;

																								{	/* Eval/expdargs.scm 357 */
																									obj_t BgL_arg2017z00_2031;
																									obj_t BgL_arg2018z00_2032;

																									{	/* Eval/expdargs.scm 357 */
																										obj_t BgL_arg2019z00_2033;

																										BgL_arg2019z00_2033 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2512z00zz__expander_argsz00,
																											BNIL);
																										BgL_arg2017z00_2031 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2470z00zz__expander_argsz00,
																											BgL_arg2019z00_2033);
																									}
																									{	/* Eval/expdargs.scm 357 */
																										obj_t BgL_arg2020z00_2034;

																										BgL_arg2020z00_2034 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2506z00zz__expander_argsz00,
																											BNIL);
																										BgL_arg2018z00_2032 =
																											MAKE_YOUNG_PAIR
																											(BgL_naz00_1985,
																											BgL_arg2020z00_2034);
																									}
																									BgL_arg2016z00_2030 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2017z00_2031,
																										BgL_arg2018z00_2032);
																								}
																								BgL_arg2015z00_2029 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2472z00zz__expander_argsz00,
																									BgL_arg2016z00_2030);
																							}
																							BgL_arg2008z00_2022 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2015z00_2029, BNIL);
																						}
																						BgL_arg2006z00_2020 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2007z00_2021,
																							BgL_arg2008z00_2022);
																					}
																					BgL_arg2003z00_2018 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2528z00zz__expander_argsz00,
																						BgL_arg2006z00_2020);
																				}
																				{	/* Eval/expdargs.scm 358 */
																					obj_t BgL_arg2021z00_2035;

																					{	/* Eval/expdargs.scm 358 */
																						obj_t BgL_arg2022z00_2036;

																						{	/* Eval/expdargs.scm 358 */
																							obj_t BgL_arg2024z00_2037;
																							obj_t BgL_arg2025z00_2038;

																							{	/* Eval/expdargs.scm 358 */
																								obj_t BgL_arg2026z00_2039;

																								BgL_arg2026z00_2039 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2516z00zz__expander_argsz00,
																									BNIL);
																								BgL_arg2024z00_2037 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2470z00zz__expander_argsz00,
																									BgL_arg2026z00_2039);
																							}
																							{	/* Eval/expdargs.scm 358 */
																								obj_t BgL_arg2027z00_2040;

																								BgL_arg2027z00_2040 =
																									MAKE_YOUNG_PAIR(BgL_vz00_1984,
																									BNIL);
																								BgL_arg2025z00_2038 =
																									MAKE_YOUNG_PAIR(BgL_az00_1983,
																									BgL_arg2027z00_2040);
																							}
																							BgL_arg2022z00_2036 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2024z00_2037,
																								BgL_arg2025z00_2038);
																						}
																						BgL_arg2021z00_2035 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2472z00zz__expander_argsz00,
																							BgL_arg2022z00_2036);
																					}
																					BgL_arg2004z00_2019 =
																						MAKE_YOUNG_PAIR(BgL_arg2021z00_2035,
																						BNIL);
																				}
																				BgL_arg1986z00_1993 =
																					MAKE_YOUNG_PAIR(BgL_arg2003z00_2018,
																					BgL_arg2004z00_2019);
																			}
																			BgL_arg1984z00_1991 =
																				MAKE_YOUNG_PAIR(BgL_arg1985z00_1992,
																				BgL_arg1986z00_1993);
																		}
																		BgL_arg1983z00_1990 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2480z00zz__expander_argsz00,
																			BgL_arg1984z00_1991);
																	}
																	BgL_arg1981z00_1988 =
																		MAKE_YOUNG_PAIR(BgL_arg1983z00_1990, BNIL);
																}
																BgL_arg1979z00_1986 =
																	MAKE_YOUNG_PAIR(BgL_arg1980z00_1987,
																	BgL_arg1981z00_1988);
															}
															return
																MAKE_YOUNG_PAIR
																(BGl_symbol2482z00zz__expander_argsz00,
																BgL_arg1979z00_1986);
														}
													}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* fetch-option-arguments */
	obj_t BGl_fetchzd2optionzd2argumentsz00zz__expander_argsz00(obj_t
		BgL_optz00_24)
	{
		{	/* Eval/expdargs.scm 392 */
			{	/* Eval/expdargs.scm 393 */
				obj_t BgL_g1045z00_2069;

				BgL_g1045z00_2069 = CDR(((obj_t) BgL_optz00_24));
				{
					obj_t BgL_iz00_2927;
					obj_t BgL_resz00_2928;

					BgL_iz00_2927 = BgL_g1045z00_2069;
					BgL_resz00_2928 = BNIL;
				BgL_loopz00_2926:
					{	/* Eval/expdargs.scm 396 */
						bool_t BgL_test2751z00_4350;

						if (NULLP(BgL_iz00_2927))
							{	/* Eval/expdargs.scm 396 */
								BgL_test2751z00_4350 = ((bool_t) 1);
							}
						else
							{	/* Eval/expdargs.scm 396 */
								BgL_test2751z00_4350 =
									BGl_helpzd2messagezf3z21zz__expander_argsz00(CAR(
										((obj_t) BgL_iz00_2927)));
							}
						if (BgL_test2751z00_4350)
							{	/* Eval/expdargs.scm 396 */
								return bgl_reverse_bang(BgL_resz00_2928);
							}
						else
							{	/* Eval/expdargs.scm 399 */
								obj_t BgL_arg2042z00_2938;
								obj_t BgL_arg2044z00_2939;

								BgL_arg2042z00_2938 = CDR(((obj_t) BgL_iz00_2927));
								{	/* Eval/expdargs.scm 399 */
									obj_t BgL_arg2045z00_2940;

									BgL_arg2045z00_2940 = CAR(((obj_t) BgL_iz00_2927));
									BgL_arg2044z00_2939 =
										MAKE_YOUNG_PAIR(BgL_arg2045z00_2940, BgL_resz00_2928);
								}
								{
									obj_t BgL_resz00_4363;
									obj_t BgL_iz00_4362;

									BgL_iz00_4362 = BgL_arg2042z00_2938;
									BgL_resz00_4363 = BgL_arg2044z00_2939;
									BgL_resz00_2928 = BgL_resz00_4363;
									BgL_iz00_2927 = BgL_iz00_4362;
									goto BgL_loopz00_2926;
								}
							}
					}
				}
			}
		}

	}



/* bind-option-arguments */
	obj_t BGl_bindzd2optionzd2argumentsz00zz__expander_argsz00(obj_t
		BgL_argsz00_25, obj_t BgL_azb2zb2_26, obj_t BgL_naz00_27,
		obj_t BgL_clausez00_28)
	{
		{	/* Eval/expdargs.scm 404 */
			{	/* Eval/expdargs.scm 405 */
				obj_t BgL_arg2047z00_2084;
				obj_t BgL_arg2048z00_2085;

				{	/* Eval/expdargs.scm 405 */
					obj_t BgL_arg2049z00_2086;

					{	/* Eval/expdargs.scm 405 */
						obj_t BgL_arg2050z00_2087;

						{	/* Eval/expdargs.scm 405 */
							obj_t BgL_arg2051z00_2088;

							BgL_arg2051z00_2088 = MAKE_YOUNG_PAIR(BgL_azb2zb2_26, BNIL);
							BgL_arg2050z00_2087 =
								MAKE_YOUNG_PAIR(BGl_symbol2518z00zz__expander_argsz00,
								BgL_arg2051z00_2088);
						}
						BgL_arg2049z00_2086 = MAKE_YOUNG_PAIR(BgL_arg2050z00_2087, BNIL);
					}
					BgL_arg2047z00_2084 =
						MAKE_YOUNG_PAIR(BgL_naz00_27, BgL_arg2049z00_2086);
				}
				BgL_arg2048z00_2085 =
					BGl_loopze70ze7zz__expander_argsz00(BgL_naz00_27, BgL_clausez00_28,
					BgL_argsz00_25);
				return MAKE_YOUNG_PAIR(BgL_arg2047z00_2084, BgL_arg2048z00_2085);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__expander_argsz00(obj_t BgL_naz00_3209,
		obj_t BgL_clausez00_3208, obj_t BgL_argsz00_2090)
	{
		{	/* Eval/expdargs.scm 406 */
			if (PAIRP(BgL_argsz00_2090))
				{	/* Eval/expdargs.scm 408 */
					obj_t BgL_idz00_2093;

					BgL_idz00_2093 =
						BGl_fetchzd2argumentzd2namez00zz__expander_argsz00(CAR
						(BgL_argsz00_2090), BgL_clausez00_3208);
					{	/* Eval/expdargs.scm 409 */
						obj_t BgL_arg2055z00_2094;
						obj_t BgL_arg2056z00_2095;

						{	/* Eval/expdargs.scm 409 */
							obj_t BgL_arg2057z00_2096;
							obj_t BgL_arg2058z00_2097;

							BgL_arg2057z00_2096 = bstring_to_symbol(((obj_t) BgL_idz00_2093));
							{	/* Eval/expdargs.scm 410 */
								obj_t BgL_arg2059z00_2098;

								{	/* Eval/expdargs.scm 410 */
									obj_t BgL_arg2060z00_2099;

									{	/* Eval/expdargs.scm 410 */
										obj_t BgL_arg2061z00_2100;
										obj_t BgL_arg2062z00_2101;

										{	/* Eval/expdargs.scm 410 */
											obj_t BgL_arg2063z00_2102;

											BgL_arg2063z00_2102 =
												MAKE_YOUNG_PAIR(BgL_naz00_3209, BNIL);
											BgL_arg2061z00_2100 =
												MAKE_YOUNG_PAIR(BGl_symbol2546z00zz__expander_argsz00,
												BgL_arg2063z00_2102);
										}
										{	/* Eval/expdargs.scm 411 */
											obj_t BgL_arg2064z00_2103;
											obj_t BgL_arg2065z00_2104;

											{	/* Eval/expdargs.scm 411 */
												obj_t BgL_arg2067z00_2105;

												BgL_arg2067z00_2105 =
													MAKE_YOUNG_PAIR(BgL_naz00_3209, BNIL);
												BgL_arg2064z00_2103 =
													MAKE_YOUNG_PAIR(BGl_symbol2474z00zz__expander_argsz00,
													BgL_arg2067z00_2105);
											}
											{	/* Eval/expdargs.scm 412 */
												obj_t BgL_arg2068z00_2106;

												{	/* Eval/expdargs.scm 412 */
													obj_t BgL_arg2069z00_2107;

													{	/* Eval/expdargs.scm 412 */
														obj_t BgL_arg2070z00_2108;
														obj_t BgL_arg2072z00_2109;

														{	/* Eval/expdargs.scm 412 */
															obj_t BgL_arg2074z00_2110;

															BgL_arg2074z00_2110 =
																MAKE_YOUNG_PAIR(CAR(BgL_argsz00_2090), BNIL);
															BgL_arg2070z00_2108 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2470z00zz__expander_argsz00,
																BgL_arg2074z00_2110);
														}
														{	/* Eval/expdargs.scm 412 */
															obj_t BgL_arg2076z00_2112;

															{	/* Eval/expdargs.scm 412 */
																obj_t BgL_arg2077z00_2113;

																{	/* Eval/expdargs.scm 412 */
																	obj_t BgL_arg2078z00_2114;

																	BgL_arg2078z00_2114 =
																		MAKE_YOUNG_PAIR(BgL_clausez00_3208, BNIL);
																	BgL_arg2077z00_2113 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2470z00zz__expander_argsz00,
																		BgL_arg2078z00_2114);
																}
																BgL_arg2076z00_2112 =
																	MAKE_YOUNG_PAIR(BgL_arg2077z00_2113, BNIL);
															}
															BgL_arg2072z00_2109 =
																MAKE_YOUNG_PAIR
																(BGl_string2568z00zz__expander_argsz00,
																BgL_arg2076z00_2112);
														}
														BgL_arg2069z00_2107 =
															MAKE_YOUNG_PAIR(BgL_arg2070z00_2108,
															BgL_arg2072z00_2109);
													}
													BgL_arg2068z00_2106 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2478z00zz__expander_argsz00,
														BgL_arg2069z00_2107);
												}
												BgL_arg2065z00_2104 =
													MAKE_YOUNG_PAIR(BgL_arg2068z00_2106, BNIL);
											}
											BgL_arg2062z00_2101 =
												MAKE_YOUNG_PAIR(BgL_arg2064z00_2103,
												BgL_arg2065z00_2104);
										}
										BgL_arg2060z00_2099 =
											MAKE_YOUNG_PAIR(BgL_arg2061z00_2100, BgL_arg2062z00_2101);
									}
									BgL_arg2059z00_2098 =
										MAKE_YOUNG_PAIR(BGl_symbol2480z00zz__expander_argsz00,
										BgL_arg2060z00_2099);
								}
								BgL_arg2058z00_2097 =
									MAKE_YOUNG_PAIR(BgL_arg2059z00_2098, BNIL);
							}
							BgL_arg2055z00_2094 =
								MAKE_YOUNG_PAIR(BgL_arg2057z00_2096, BgL_arg2058z00_2097);
						}
						{	/* Eval/expdargs.scm 413 */
							obj_t BgL_arg2079z00_2115;
							obj_t BgL_arg2080z00_2116;

							{	/* Eval/expdargs.scm 413 */
								obj_t BgL_arg2081z00_2117;

								{	/* Eval/expdargs.scm 413 */
									obj_t BgL_arg2082z00_2118;

									{	/* Eval/expdargs.scm 413 */
										obj_t BgL_arg2083z00_2119;

										BgL_arg2083z00_2119 = MAKE_YOUNG_PAIR(BgL_naz00_3209, BNIL);
										BgL_arg2082z00_2118 =
											MAKE_YOUNG_PAIR(BGl_symbol2518z00zz__expander_argsz00,
											BgL_arg2083z00_2119);
									}
									BgL_arg2081z00_2117 =
										MAKE_YOUNG_PAIR(BgL_arg2082z00_2118, BNIL);
								}
								BgL_arg2079z00_2115 =
									MAKE_YOUNG_PAIR(BgL_naz00_3209, BgL_arg2081z00_2117);
							}
							BgL_arg2080z00_2116 =
								BGl_loopze70ze7zz__expander_argsz00(BgL_naz00_3209,
								BgL_clausez00_3208, CDR(BgL_argsz00_2090));
							BgL_arg2056z00_2095 =
								MAKE_YOUNG_PAIR(BgL_arg2079z00_2115, BgL_arg2080z00_2116);
						}
						return MAKE_YOUNG_PAIR(BgL_arg2055z00_2094, BgL_arg2056z00_2095);
					}
				}
			else
				{	/* Eval/expdargs.scm 407 */
					return BNIL;
				}
		}

	}



/* fetch-argument-name */
	obj_t BGl_fetchzd2argumentzd2namez00zz__expander_argsz00(obj_t BgL_az00_29,
		obj_t BgL_clausez00_30)
	{
		{	/* Eval/expdargs.scm 420 */
			if (SYMBOLP(BgL_az00_29))
				{	/* Eval/expdargs.scm 423 */
					obj_t BgL_sz00_2124;

					{	/* Eval/expdargs.scm 423 */
						obj_t BgL_arg2251z00_2949;

						BgL_arg2251z00_2949 = SYMBOL_TO_STRING(BgL_az00_29);
						BgL_sz00_2124 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2251z00_2949);
					}
					if ((STRING_REF(BgL_sz00_2124, 0L) == ((unsigned char) '?')))
						{	/* Eval/expdargs.scm 424 */
							return
								c_substring(BgL_sz00_2124, 1L, STRING_LENGTH(BgL_sz00_2124));
						}
					else
						{	/* Eval/expdargs.scm 424 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string2456z00zz__expander_argsz00,
								string_append_3(BGl_string2569z00zz__expander_argsz00,
									BgL_sz00_2124, BGl_string2570z00zz__expander_argsz00),
								BgL_clausez00_30);
						}
				}
			else
				{	/* Eval/expdargs.scm 421 */
					return
						BGl_expandzd2errorzd2zz__expandz00
						(BGl_string2456z00zz__expander_argsz00,
						BGl_string2571z00zz__expander_argsz00, BgL_clausez00_30);
				}
		}

	}



/* fetch-option-embed-argument */
	obj_t BGl_fetchzd2optionzd2embedzd2argumentzd2zz__expander_argsz00(obj_t
		BgL_optz00_31)
	{
		{	/* Eval/expdargs.scm 435 */
			{	/* Eval/expdargs.scm 436 */
				long BgL_lenz00_2130;

				BgL_lenz00_2130 = (STRING_LENGTH(((obj_t) BgL_optz00_31)) - 1L);
				{
					long BgL_iz00_2132;

					BgL_iz00_2132 = 0L;
				BgL_zc3z04anonymousza32094ze3z87_2133:
					if ((BgL_iz00_2132 >= BgL_lenz00_2130))
						{	/* Eval/expdargs.scm 439 */
							{	/* Eval/expdargs.scm 440 */
								int BgL_tmpz00_4420;

								BgL_tmpz00_4420 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4420);
							}
							{	/* Eval/expdargs.scm 440 */
								int BgL_tmpz00_4423;

								BgL_tmpz00_4423 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_4423, BFALSE);
							}
							return BgL_optz00_31;
						}
					else
						{	/* Eval/expdargs.scm 439 */
							if (
								(STRING_REF(
										((obj_t) BgL_optz00_31),
										BgL_iz00_2132) == ((unsigned char) '?')))
								{	/* Eval/expdargs.scm 442 */
									obj_t BgL_val0_1157z00_2139;
									obj_t BgL_val1_1158z00_2140;

									BgL_val0_1157z00_2139 =
										c_substring(((obj_t) BgL_optz00_31), 0L, BgL_iz00_2132);
									{	/* Eval/expdargs.scm 443 */
										long BgL_arg2098z00_2141;
										long BgL_arg2099z00_2142;

										BgL_arg2098z00_2141 = (1L + BgL_iz00_2132);
										BgL_arg2099z00_2142 = (BgL_lenz00_2130 + 1L);
										BgL_val1_1158z00_2140 =
											c_substring(
											((obj_t) BgL_optz00_31), BgL_arg2098z00_2141,
											BgL_arg2099z00_2142);
									}
									{	/* Eval/expdargs.scm 442 */
										int BgL_tmpz00_4436;

										BgL_tmpz00_4436 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4436);
									}
									{	/* Eval/expdargs.scm 442 */
										int BgL_tmpz00_4439;

										BgL_tmpz00_4439 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_4439, BgL_val1_1158z00_2140);
									}
									return BgL_val0_1157z00_2139;
								}
							else
								{
									long BgL_iz00_4442;

									BgL_iz00_4442 = (BgL_iz00_2132 + 1L);
									BgL_iz00_2132 = BgL_iz00_4442;
									goto BgL_zc3z04anonymousza32094ze3z87_2133;
								}
						}
				}
			}
		}

	}



/* help-message? */
	bool_t BGl_helpzd2messagezf3z21zz__expander_argsz00(obj_t BgL_expz00_32)
	{
		{	/* Eval/expdargs.scm 450 */
			if (PAIRP(BgL_expz00_32))
				{	/* Eval/expdargs.scm 451 */
					obj_t BgL_arg2106z00_2153;

					BgL_arg2106z00_2153 = CAR(BgL_expz00_32);
					{	/* Eval/expdargs.scm 459 */
						bool_t BgL__ortest_1047z00_2973;

						BgL__ortest_1047z00_2973 =
							(BgL_arg2106z00_2153 == BGl_symbol2530z00zz__expander_argsz00);
						if (BgL__ortest_1047z00_2973)
							{	/* Eval/expdargs.scm 459 */
								return BgL__ortest_1047z00_2973;
							}
						else
							{	/* Eval/expdargs.scm 459 */
								return
									(BgL_arg2106z00_2153 ==
									BGl_symbol2532z00zz__expander_argsz00);
							}
					}
				}
			else
				{	/* Eval/expdargs.scm 451 */
					return ((bool_t) 0);
				}
		}

	}



/* synopsis? */
	bool_t BGl_synopsiszf3zf3zz__expander_argsz00(obj_t BgL_symz00_33)
	{
		{	/* Eval/expdargs.scm 458 */
			{	/* Eval/expdargs.scm 459 */
				bool_t BgL__ortest_1047z00_2974;

				BgL__ortest_1047z00_2974 =
					(BgL_symz00_33 == BGl_symbol2530z00zz__expander_argsz00);
				if (BgL__ortest_1047z00_2974)
					{	/* Eval/expdargs.scm 459 */
						return BgL__ortest_1047z00_2974;
					}
				else
					{	/* Eval/expdargs.scm 459 */
						return (BgL_symz00_33 == BGl_symbol2532z00zz__expander_argsz00);
					}
			}
		}

	}



/* args-parse-usage */
	BGL_EXPORTED_DEF obj_t BGl_argszd2parsezd2usagez00zz__expander_argsz00(obj_t
		BgL_descrsz00_34)
	{
		{	/* Eval/expdargs.scm 464 */
			{	/* Eval/expdargs.scm 465 */
				obj_t BgL_zc3z04anonymousza32107ze3z87_3202;

				BgL_zc3z04anonymousza32107ze3z87_3202 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza32107ze3ze5zz__expander_argsz00, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32107ze3z87_3202, (int) (0L),
					BgL_descrsz00_34);
				return BgL_zc3z04anonymousza32107ze3z87_3202;
			}
		}

	}



/* &args-parse-usage */
	obj_t BGl_z62argszd2parsezd2usagez62zz__expander_argsz00(obj_t
		BgL_envz00_3203, obj_t BgL_descrsz00_3204)
	{
		{	/* Eval/expdargs.scm 464 */
			{	/* Eval/expdargs.scm 465 */
				obj_t BgL_auxz00_4458;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_descrsz00_3204))
					{	/* Eval/expdargs.scm 465 */
						BgL_auxz00_4458 = BgL_descrsz00_3204;
					}
				else
					{
						obj_t BgL_auxz00_4461;

						BgL_auxz00_4461 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2458z00zz__expander_argsz00, BINT(16196L),
							BGl_string2572z00zz__expander_argsz00,
							BGl_string2460z00zz__expander_argsz00, BgL_descrsz00_3204);
						FAILURE(BgL_auxz00_4461, BFALSE, BFALSE);
					}
				return BGl_argszd2parsezd2usagez00zz__expander_argsz00(BgL_auxz00_4458);
			}
		}

	}



/* &<@anonymous:2107> */
	obj_t BGl_z62zc3z04anonymousza32107ze3ze5zz__expander_argsz00(obj_t
		BgL_envz00_3205, obj_t BgL_manualzf3zf3_3207)
	{
		{	/* Eval/expdargs.scm 465 */
			{	/* Eval/expdargs.scm 466 */
				obj_t BgL_descrsz00_3206;

				BgL_descrsz00_3206 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3205, (int) (0L)));
				if (CBOOL(BgL_manualzf3zf3_3207))
					{	/* Eval/expdargs.scm 466 */
						obj_t BgL_port1159z00_3267;

						{	/* Eval/expdargs.scm 466 */
							obj_t BgL_tmpz00_4471;

							BgL_tmpz00_4471 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1159z00_3267 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4471);
						}
						bgl_display_string(BGl_string2573z00zz__expander_argsz00,
							BgL_port1159z00_3267);
						bgl_display_char(((unsigned char) 10), BgL_port1159z00_3267);
					}
				else
					{	/* Eval/expdargs.scm 466 */
						BFALSE;
					}
				{	/* Eval/expdargs.scm 467 */
					long BgL_mlenzd2symzd2_3268;

					BgL_mlenzd2symzd2_3268 = 0L;
					{
						obj_t BgL_l1160z00_3270;

						BgL_l1160z00_3270 = BgL_descrsz00_3206;
					BgL_zc3z04anonymousza32108ze3z87_3269:
						if (PAIRP(BgL_l1160z00_3270))
							{	/* Eval/expdargs.scm 469 */
								{	/* Eval/expdargs.scm 470 */
									obj_t BgL_optz00_3271;

									BgL_optz00_3271 = CAR(BgL_l1160z00_3270);
									{	/* Eval/expdargs.scm 470 */
										obj_t BgL_namez00_3272;

										BgL_namez00_3272 = CAR(((obj_t) BgL_optz00_3271));
										if (STRINGP(BgL_namez00_3272))
											{	/* Eval/expdargs.scm 472 */
												long BgL_lenz00_3273;

												BgL_lenz00_3273 = STRING_LENGTH(BgL_namez00_3272);
												if ((BgL_lenz00_3273 > BgL_mlenzd2symzd2_3268))
													{	/* Eval/expdargs.scm 473 */
														BgL_mlenzd2symzd2_3268 = BgL_lenz00_3273;
													}
												else
													{	/* Eval/expdargs.scm 473 */
														BFALSE;
													}
											}
										else
											{	/* Eval/expdargs.scm 471 */
												BFALSE;
											}
									}
								}
								{
									obj_t BgL_l1160z00_4486;

									BgL_l1160z00_4486 = CDR(BgL_l1160z00_3270);
									BgL_l1160z00_3270 = BgL_l1160z00_4486;
									goto BgL_zc3z04anonymousza32108ze3z87_3269;
								}
							}
						else
							{	/* Eval/expdargs.scm 469 */
								((bool_t) 1);
							}
					}
					{
						obj_t BgL_l1164z00_3275;

						BgL_l1164z00_3275 = BgL_descrsz00_3206;
					BgL_zc3z04anonymousza32113ze3z87_3274:
						if (PAIRP(BgL_l1164z00_3275))
							{	/* Eval/expdargs.scm 476 */
								{	/* Eval/expdargs.scm 477 */
									obj_t BgL_optz00_3276;

									BgL_optz00_3276 = CAR(BgL_l1164z00_3275);
									{	/* Eval/expdargs.scm 477 */
										obj_t BgL_namez00_3277;

										BgL_namez00_3277 = CAR(((obj_t) BgL_optz00_3276));
										if (STRINGP(BgL_namez00_3277))
											{	/* Eval/expdargs.scm 480 */
												long BgL_lenz00_3278;

												BgL_lenz00_3278 =
													STRING_LENGTH(((obj_t) BgL_namez00_3277));
												{	/* Eval/expdargs.scm 481 */
													obj_t BgL_descz00_3279;

													BgL_descz00_3279 = CDR(((obj_t) BgL_optz00_3276));
													{	/* Eval/expdargs.scm 482 */
														obj_t BgL_tabz00_3280;

														BgL_tabz00_3280 =
															make_string(
															(BgL_mlenzd2symzd2_3268 - BgL_lenz00_3278),
															((unsigned char) ' '));
														{	/* Eval/expdargs.scm 483 */

															if (CBOOL(BgL_manualzf3zf3_3207))
																{	/* Eval/expdargs.scm 485 */
																	{	/* Eval/expdargs.scm 487 */
																		obj_t BgL_arg2116z00_3281;

																		{	/* Eval/expdargs.scm 487 */
																			obj_t BgL_arg2118z00_3282;

																			BgL_arg2118z00_3282 =
																				MAKE_YOUNG_PAIR(BgL_descz00_3279, BNIL);
																			BgL_arg2116z00_3281 =
																				MAKE_YOUNG_PAIR(BgL_namez00_3277,
																				BgL_arg2118z00_3282);
																		}
																		BGl_writez00zz__r4_output_6_10_3z00
																			(BgL_arg2116z00_3281, BNIL);
																	}
																	{	/* Eval/expdargs.scm 488 */
																		obj_t BgL_arg2119z00_3283;

																		{	/* Eval/expdargs.scm 488 */
																			obj_t BgL_tmpz00_4506;

																			BgL_tmpz00_4506 =
																				BGL_CURRENT_DYNAMIC_ENV();
																			BgL_arg2119z00_3283 =
																				BGL_ENV_CURRENT_OUTPUT_PORT
																				(BgL_tmpz00_4506);
																		}
																		bgl_display_char(((unsigned char) 10),
																			BgL_arg2119z00_3283);
																}}
															else
																{	/* Eval/expdargs.scm 489 */
																	obj_t BgL_port1162z00_3284;

																	{	/* Eval/expdargs.scm 489 */
																		obj_t BgL_tmpz00_4510;

																		BgL_tmpz00_4510 = BGL_CURRENT_DYNAMIC_ENV();
																		BgL_port1162z00_3284 =
																			BGL_ENV_CURRENT_OUTPUT_PORT
																			(BgL_tmpz00_4510);
																	}
																	bgl_display_string
																		(BGl_string2574z00zz__expander_argsz00,
																		BgL_port1162z00_3284);
																	bgl_display_obj(BgL_namez00_3277,
																		BgL_port1162z00_3284);
																	bgl_display_obj(BgL_tabz00_3280,
																		BgL_port1162z00_3284);
																	bgl_display_string
																		(BGl_string2539z00zz__expander_argsz00,
																		BgL_port1162z00_3284);
																	bgl_display_obj(BgL_descz00_3279,
																		BgL_port1162z00_3284);
																	bgl_display_char(((unsigned char) 10),
																		BgL_port1162z00_3284);
											}}}}}
										else
											{	/* Eval/expdargs.scm 479 */
												if (
													(BgL_namez00_3277 ==
														BGl_symbol2535z00zz__expander_argsz00))
													{	/* Eval/expdargs.scm 491 */
														obj_t BgL_port1163z00_3285;

														{	/* Eval/expdargs.scm 491 */
															obj_t BgL_tmpz00_4521;

															BgL_tmpz00_4521 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_port1163z00_3285 =
																BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4521);
														}
														bgl_display_char(((unsigned char) 10),
															BgL_port1163z00_3285);
														{	/* Eval/expdargs.scm 491 */
															obj_t BgL_arg2121z00_3286;

															BgL_arg2121z00_3286 =
																CDR(((obj_t) BgL_optz00_3276));
															bgl_display_obj(BgL_arg2121z00_3286,
																BgL_port1163z00_3285);
														}
														bgl_display_string
															(BGl_string2575z00zz__expander_argsz00,
															BgL_port1163z00_3285);
														bgl_display_char(((unsigned char) 10),
															BgL_port1163z00_3285);
													}
												else
													{	/* Eval/expdargs.scm 490 */
														BFALSE;
													}
											}
									}
								}
								{
									obj_t BgL_l1164z00_4530;

									BgL_l1164z00_4530 = CDR(BgL_l1164z00_3275);
									BgL_l1164z00_3275 = BgL_l1164z00_4530;
									goto BgL_zc3z04anonymousza32113ze3z87_3274;
								}
							}
						else
							{	/* Eval/expdargs.scm 476 */
								((bool_t) 1);
							}
					}
					BGl_symbol2576z00zz__expander_argsz00;
				}
				if (CBOOL(BgL_manualzf3zf3_3207))
					{	/* Eval/expdargs.scm 494 */
						obj_t BgL_port1166z00_3287;

						{	/* Eval/expdargs.scm 494 */
							obj_t BgL_tmpz00_4534;

							BgL_tmpz00_4534 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1166z00_3287 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4534);
						}
						bgl_display_string(BGl_string2578z00zz__expander_argsz00,
							BgL_port1166z00_3287);
						return bgl_display_char(((unsigned char) 10), BgL_port1166z00_3287);
					}
				else
					{	/* Eval/expdargs.scm 494 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_argsz00(void)
	{
		{	/* Eval/expdargs.scm 19 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_argsz00(void)
	{
		{	/* Eval/expdargs.scm 19 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_argsz00(void)
	{
		{	/* Eval/expdargs.scm 19 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_argsz00(void)
	{
		{	/* Eval/expdargs.scm 19 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(515155867L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2579z00zz__expander_argsz00));
		}

	}

#ifdef __cplusplus
}
#endif
