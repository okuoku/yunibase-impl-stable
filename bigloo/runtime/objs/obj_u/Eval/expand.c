/*===========================================================================*/
/*   (Eval/expand.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expand.scm -indent -o objs/obj_u/Eval/expand.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPAND_TYPE_DEFINITIONS
#define BGL___EXPAND_TYPE_DEFINITIONS
#endif													// BGL___EXPAND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62expandzd2errorzb0zz__expandz00(obj_t, obj_t, obj_t,
		obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62z52lexicalzd2stackze2zz__expandz00(obj_t);
	static obj_t BGl_z62initialzd2expanderzb0zz__expandz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__expandz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2oncezb0zz__expandz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_expandz00zz__expandz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_z52withzd2lexicalz80zz__expandz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31269ze3ze5zz__expandz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31188ze3ze5zz__expandz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expandz00(void);
	static obj_t BGl_genericzd2initzd2zz__expandz00(void);
	BGL_EXPORTED_DECL obj_t BGl_z52lexicalzd2stackz80zz__expandz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expandz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expandz00(void);
	static obj_t BGl_objectzd2initzd2zz__expandz00(void);
	extern obj_t BGl_getzd2evalzd2expanderz00zz__macroz00(obj_t);
	static obj_t BGl_z62identifierzd2evalzd2expanderz62zz__expandz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31216ze3ze5zz__expandz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62applicationzd2evalzd2expanderz12z70zz__expandz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandz12z12zz__expandz00(obj_t);
	extern obj_t BGl_getzd2sourcezd2locationz00zz__readerz00(obj_t);
	static obj_t BGl_z62expandz12z70zz__expandz00(obj_t, obj_t);
	extern obj_t BGl_parsezd2formalzd2identz00zz__evutilsz00(obj_t, obj_t);
	static obj_t BGl_z62z52withzd2lexicalze2zz__expandz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2oncezd2zz__expandz00(obj_t);
	static obj_t BGl_z62initialzd2expanderz12za2zz__expandz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zz__expandz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__expandz00(void);
	static obj_t BGl_loopze70ze7zz__expandz00(obj_t, obj_t);
	static obj_t BGl_z62expandz62zz__expandz00(obj_t, obj_t);
	static obj_t BGl_initialzd2expanderzf2applicationz20zz__expandz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1668z00zz__expandz00 = BUNSPEC;
	static obj_t BGl_z62applicationzd2evalzd2expanderz62zz__expandz00(obj_t,
		obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_identifierzd2evalzd2expanderzd2envzd2zz__expandz00,
		BgL_bgl_za762identifierza7d21671z00,
		BGl_z62identifierzd2evalzd2expanderz62zz__expandz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_initialzd2expanderzd2envz00zz__expandz00,
		BgL_bgl_za762initialza7d2exp1672z00,
		BGl_z62initialzd2expanderzb0zz__expandz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2oncezd2envz00zz__expandz00,
		BgL_bgl_za762expandza7d2once1673z00, BGl_z62expandzd2oncezb0zz__expandz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2envzd2zz__expandz00,
		BgL_bgl_za762expandza762za7za7__1674z00, BGl_z62expandz62zz__expandz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52withzd2lexicalzd2envz52zz__expandz00,
		BgL_bgl_za762za752withza7d2lex1675za7,
		BGl_z62z52withzd2lexicalze2zz__expandz00, 0L, BUNSPEC, 4);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_applicationzd2evalzd2expanderzd2envzd2zz__expandz00,
		BgL_bgl_za762applicationza7d1676z00,
		BGl_z62applicationzd2evalzd2expanderz62zz__expandz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_initialzd2expanderz12zd2envz12zz__expandz00,
		BgL_bgl_za762initialza7d2exp1677z00,
		BGl_z62initialzd2expanderz12za2zz__expandz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1660z00zz__expandz00,
		BgL_bgl_string1660za700za7za7_1678za7, "expand", 6);
	      DEFINE_STRING(BGl_string1661z00zz__expandz00,
		BgL_bgl_string1661za700za7za7_1679za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1663z00zz__expandz00,
		BgL_bgl_string1663za700za7za7_1680za7, "application", 11);
	      DEFINE_STRING(BGl_string1664z00zz__expandz00,
		BgL_bgl_string1664za700za7za7_1681za7,
		"/tmp/bigloo/runtime/Eval/expand.scm", 35);
	      DEFINE_STRING(BGl_string1665z00zz__expandz00,
		BgL_bgl_string1665za700za7za7_1682za7, "&%with-lexical", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1659z00zz__expandz00,
		BgL_bgl_za762za7c3za704anonymo1683za7,
		BGl_z62zc3z04anonymousza31188ze3ze5zz__expandz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1666z00zz__expandz00,
		BgL_bgl_string1666za700za7za7_1684za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1667z00zz__expandz00,
		BgL_bgl_string1667za700za7za7_1685za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1669z00zz__expandz00,
		BgL_bgl_string1669za700za7za7_1686za7, "at", 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandz12zd2envzc0zz__expandz00,
		BgL_bgl_za762expandza712za770za71687z00, BGl_z62expandz12z70zz__expandz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1662z00zz__expandz00,
		BgL_bgl_za762za7c3za704anonymo1688za7,
		BGl_z62zc3z04anonymousza31216ze3ze5zz__expandz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1670z00zz__expandz00,
		BgL_bgl_string1670za700za7za7_1689za7, "__expand", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2errorzd2envz00zz__expandz00,
		BgL_bgl_za762expandza7d2erro1690z00, BGl_z62expandzd2errorzb0zz__expandz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_z52lexicalzd2stackzd2envz52zz__expandz00,
		BgL_bgl_za762za752lexicalza7d21691za7,
		BGl_z62z52lexicalzd2stackze2zz__expandz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_applicationzd2evalzd2expanderz12zd2envzc0zz__expandz00,
		BgL_bgl_za762applicationza7d1692z00,
		BGl_z62applicationzd2evalzd2expanderz12z70zz__expandz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__expandz00));
		     ADD_ROOT((void *) (&BGl_symbol1668z00zz__expandz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long
		BgL_checksumz00_1975, char *BgL_fromz00_1976)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expandz00))
				{
					BGl_requirezd2initializa7ationz75zz__expandz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expandz00();
					BGl_cnstzd2initzd2zz__expandz00();
					BGl_importedzd2moduleszd2initz00zz__expandz00();
					return BGl_methodzd2initzd2zz__expandz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expandz00(void)
	{
		{	/* Eval/expand.scm 14 */
			return (BGl_symbol1668z00zz__expandz00 =
				bstring_to_symbol(BGl_string1669z00zz__expandz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expandz00(void)
	{
		{	/* Eval/expand.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__expandz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1140;

				BgL_headz00_1140 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1687;
					obj_t BgL_tailz00_1688;

					BgL_prevz00_1687 = BgL_headz00_1140;
					BgL_tailz00_1688 = BgL_l1z00_1;
				BgL_loopz00_1686:
					if (PAIRP(BgL_tailz00_1688))
						{
							obj_t BgL_newzd2prevzd2_1694;

							BgL_newzd2prevzd2_1694 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1688), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1687, BgL_newzd2prevzd2_1694);
							{
								obj_t BgL_tailz00_1993;
								obj_t BgL_prevz00_1992;

								BgL_prevz00_1992 = BgL_newzd2prevzd2_1694;
								BgL_tailz00_1993 = CDR(BgL_tailz00_1688);
								BgL_tailz00_1688 = BgL_tailz00_1993;
								BgL_prevz00_1687 = BgL_prevz00_1992;
								goto BgL_loopz00_1686;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1140);
			}
		}

	}



/* expand */
	BGL_EXPORTED_DEF obj_t BGl_expandz00zz__expandz00(obj_t BgL_xz00_3)
	{
		{	/* Eval/expand.scm 71 */
			return
				BGl_initialzd2expanderzf2applicationz20zz__expandz00(BgL_xz00_3,
				BGl_initialzd2expanderzd2envz00zz__expandz00,
				BGl_applicationzd2evalzd2expanderzd2envzd2zz__expandz00);
		}

	}



/* &expand */
	obj_t BGl_z62expandz62zz__expandz00(obj_t BgL_envz00_1923,
		obj_t BgL_xz00_1924)
	{
		{	/* Eval/expand.scm 71 */
			return BGl_expandz00zz__expandz00(BgL_xz00_1924);
		}

	}



/* expand! */
	BGL_EXPORTED_DEF obj_t BGl_expandz12z12zz__expandz00(obj_t BgL_xz00_4)
	{
		{	/* Eval/expand.scm 77 */
			return
				BGl_initialzd2expanderzf2applicationz20zz__expandz00(BgL_xz00_4,
				BGl_initialzd2expanderz12zd2envz12zz__expandz00,
				BGl_applicationzd2evalzd2expanderz12zd2envzc0zz__expandz00);
		}

	}



/* &expand! */
	obj_t BGl_z62expandz12z70zz__expandz00(obj_t BgL_envz00_1931,
		obj_t BgL_xz00_1932)
	{
		{	/* Eval/expand.scm 77 */
			return BGl_expandz12z12zz__expandz00(BgL_xz00_1932);
		}

	}



/* expand-once */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2oncezd2zz__expandz00(obj_t BgL_xz00_5)
	{
		{	/* Eval/expand.scm 83 */
			return
				BGl_initialzd2expanderzf2applicationz20zz__expandz00(BgL_xz00_5,
				BGl_proc1659z00zz__expandz00,
				BGl_applicationzd2evalzd2expanderzd2envzd2zz__expandz00);
		}

	}



/* &expand-once */
	obj_t BGl_z62expandzd2oncezb0zz__expandz00(obj_t BgL_envz00_1940,
		obj_t BgL_xz00_1941)
	{
		{	/* Eval/expand.scm 83 */
			return BGl_expandzd2oncezd2zz__expandz00(BgL_xz00_1941);
		}

	}



/* &<@anonymous:1188> */
	obj_t BGl_z62zc3z04anonymousza31188ze3ze5zz__expandz00(obj_t BgL_envz00_1942,
		obj_t BgL_xz00_1943, obj_t BgL_ez00_1944)
	{
		{	/* Eval/expand.scm 84 */
			return BgL_xz00_1943;
		}

	}



/* &initial-expander */
	obj_t BGl_z62initialzd2expanderzb0zz__expandz00(obj_t BgL_envz00_1925,
		obj_t BgL_xz00_1926, obj_t BgL_ez00_1927)
	{
		{	/* Eval/expand.scm 89 */
			return
				BGl_initialzd2expanderzf2applicationz20zz__expandz00(BgL_xz00_1926,
				BgL_ez00_1927, BGl_applicationzd2evalzd2expanderzd2envzd2zz__expandz00);
		}

	}



/* &initial-expander! */
	obj_t BGl_z62initialzd2expanderz12za2zz__expandz00(obj_t BgL_envz00_1933,
		obj_t BgL_xz00_1934, obj_t BgL_ez00_1935)
	{
		{	/* Eval/expand.scm 95 */
			return
				BGl_initialzd2expanderzf2applicationz20zz__expandz00(BgL_xz00_1934,
				BgL_ez00_1935,
				BGl_applicationzd2evalzd2expanderz12zd2envzc0zz__expandz00);
		}

	}



/* initial-expander/application */
	obj_t BGl_initialzd2expanderzf2applicationz20zz__expandz00(obj_t BgL_xz00_10,
		obj_t BgL_ez00_11, obj_t BgL_aez00_12)
	{
		{	/* Eval/expand.scm 101 */
			{	/* Eval/expand.scm 102 */
				obj_t BgL_e1z00_1153;

				if (SYMBOLP(BgL_xz00_10))
					{	/* Eval/expand.scm 103 */
						BgL_e1z00_1153 =
							BGl_identifierzd2evalzd2expanderzd2envzd2zz__expandz00;
					}
				else
					{	/* Eval/expand.scm 103 */
						if (NULLP(BgL_xz00_10))
							{	/* Eval/expand.scm 105 */
								BgL_e1z00_1153 =
									BGl_errorz00zz__errorz00(BGl_string1660z00zz__expandz00,
									BGl_string1661z00zz__expandz00, BNIL);
							}
						else
							{	/* Eval/expand.scm 105 */
								if (PAIRP(BgL_xz00_10))
									{	/* Eval/expand.scm 109 */
										bool_t BgL_test1698z00_2011;

										{	/* Eval/expand.scm 109 */
											obj_t BgL_tmpz00_2012;

											BgL_tmpz00_2012 = CAR(BgL_xz00_10);
											BgL_test1698z00_2011 = SYMBOLP(BgL_tmpz00_2012);
										}
										if (BgL_test1698z00_2011)
											{	/* Eval/expand.scm 111 */
												obj_t BgL_g1039z00_1168;

												BgL_g1039z00_1168 =
													BGl_getzd2evalzd2expanderz00zz__macroz00(CAR
													(BgL_xz00_10));
												if (CBOOL(BgL_g1039z00_1168))
													{	/* Eval/expand.scm 111 */
														BgL_e1z00_1153 = BgL_g1039z00_1168;
													}
												else
													{	/* Eval/expand.scm 115 */
														obj_t BgL_locz00_1171;

														BgL_locz00_1171 =
															BGl_getzd2sourcezd2locationz00zz__readerz00
															(BgL_xz00_10);
														{	/* Eval/expand.scm 115 */
															obj_t BgL_idz00_1172;

															{	/* Eval/expand.scm 116 */
																obj_t BgL_arg1209z00_1181;

																BgL_arg1209z00_1181 =
																	BGl_parsezd2formalzd2identz00zz__evutilsz00
																	(CAR(BgL_xz00_10), BgL_locz00_1171);
																BgL_idz00_1172 =
																	CAR(((obj_t) BgL_arg1209z00_1181));
															}
															{	/* Eval/expand.scm 116 */

																{	/* Eval/expand.scm 118 */
																	bool_t BgL_test1700z00_2024;

																	{	/* Eval/expand.scm 118 */
																		obj_t BgL_arg1206z00_1179;

																		{	/* Eval/expand.scm 118 */
																			obj_t BgL_arg1208z00_1180;

																			BgL_arg1208z00_1180 = BGL_LEXICAL_STACK();
																			BgL_arg1206z00_1179 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_idz00_1172, BgL_arg1208z00_1180);
																		}
																		BgL_test1700z00_2024 =
																			PAIRP(BgL_arg1206z00_1179);
																	}
																	if (BgL_test1700z00_2024)
																		{	/* Eval/expand.scm 118 */
																			BgL_e1z00_1153 = BgL_aez00_12;
																		}
																	else
																		{	/* Eval/expand.scm 120 */
																			obj_t BgL_g1041z00_1176;

																			BgL_g1041z00_1176 =
																				BGl_getzd2evalzd2expanderz00zz__macroz00
																				(BgL_idz00_1172);
																			if (CBOOL(BgL_g1041z00_1176))
																				{	/* Eval/expand.scm 120 */
																					BgL_e1z00_1153 = BgL_g1041z00_1176;
																				}
																			else
																				{	/* Eval/expand.scm 120 */
																					BgL_e1z00_1153 = BgL_aez00_12;
																				}
																		}
																}
															}
														}
													}
											}
										else
											{	/* Eval/expand.scm 109 */
												BgL_e1z00_1153 = BgL_aez00_12;
											}
									}
								else
									{	/* Eval/expand.scm 107 */
										BgL_e1z00_1153 = BGl_proc1662z00zz__expandz00;
									}
							}
					}
				{	/* Eval/expand.scm 127 */
					obj_t BgL_newz00_1154;

					BgL_newz00_1154 =
						BGL_PROCEDURE_CALL2(BgL_e1z00_1153, BgL_xz00_10, BgL_ez00_11);
					{	/* Eval/expand.scm 128 */
						bool_t BgL_test1702z00_2036;

						if (PAIRP(BgL_newz00_1154))
							{	/* Eval/expand.scm 128 */
								if (EPAIRP(BgL_newz00_1154))
									{	/* Eval/expand.scm 128 */
										BgL_test1702z00_2036 = ((bool_t) 0);
									}
								else
									{	/* Eval/expand.scm 128 */
										BgL_test1702z00_2036 = EPAIRP(BgL_xz00_10);
									}
							}
						else
							{	/* Eval/expand.scm 128 */
								BgL_test1702z00_2036 = ((bool_t) 0);
							}
						if (BgL_test1702z00_2036)
							{	/* Eval/expand.scm 129 */
								obj_t BgL_arg1193z00_1158;
								obj_t BgL_arg1194z00_1159;
								obj_t BgL_arg1196z00_1160;

								BgL_arg1193z00_1158 = CAR(BgL_newz00_1154);
								BgL_arg1194z00_1159 = CDR(BgL_newz00_1154);
								BgL_arg1196z00_1160 = CER(((obj_t) BgL_xz00_10));
								{	/* Eval/expand.scm 129 */
									obj_t BgL_res1653z00_1711;

									BgL_res1653z00_1711 =
										MAKE_YOUNG_EPAIR(BgL_arg1193z00_1158, BgL_arg1194z00_1159,
										BgL_arg1196z00_1160);
									return BgL_res1653z00_1711;
								}
							}
						else
							{	/* Eval/expand.scm 128 */
								return BgL_newz00_1154;
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1216> */
	obj_t BGl_z62zc3z04anonymousza31216ze3ze5zz__expandz00(obj_t BgL_envz00_1946,
		obj_t BgL_xz00_1947, obj_t BgL_ez00_1948)
	{
		{	/* Eval/expand.scm 108 */
			return BgL_xz00_1947;
		}

	}



/* &identifier-eval-expander */
	obj_t BGl_z62identifierzd2evalzd2expanderz62zz__expandz00(obj_t
		BgL_envz00_1949, obj_t BgL_xz00_1950, obj_t BgL_ez00_1951)
	{
		{	/* Eval/expand.scm 135 */
			return BgL_xz00_1950;
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__expandz00(obj_t BgL_ez00_1965, obj_t BgL_xz00_1189)
	{
		{	/* Eval/expand.scm 142 */
			if (NULLP(BgL_xz00_1189))
				{	/* Eval/expand.scm 144 */
					return BNIL;
				}
			else
				{	/* Eval/expand.scm 144 */
					if (PAIRP(BgL_xz00_1189))
						{	/* Eval/expand.scm 146 */
							if (EPAIRP(BgL_xz00_1189))
								{	/* Eval/expand.scm 149 */
									obj_t BgL_arg1221z00_1194;
									obj_t BgL_arg1223z00_1195;
									obj_t BgL_arg1225z00_1196;

									{	/* Eval/expand.scm 149 */
										obj_t BgL_arg1226z00_1197;

										BgL_arg1226z00_1197 = CAR(BgL_xz00_1189);
										BgL_arg1221z00_1194 =
											BGL_PROCEDURE_CALL2(BgL_ez00_1965, BgL_arg1226z00_1197,
											BgL_ez00_1965);
									}
									BgL_arg1223z00_1195 =
										BGl_loopze70ze7zz__expandz00(BgL_ez00_1965,
										CDR(BgL_xz00_1189));
									BgL_arg1225z00_1196 = CER(BgL_xz00_1189);
									{	/* Eval/expand.scm 149 */
										obj_t BgL_res1654z00_1715;

										BgL_res1654z00_1715 =
											MAKE_YOUNG_EPAIR(BgL_arg1221z00_1194, BgL_arg1223z00_1195,
											BgL_arg1225z00_1196);
										return BgL_res1654z00_1715;
									}
								}
							else
								{	/* Eval/expand.scm 151 */
									obj_t BgL_arg1228z00_1199;
									obj_t BgL_arg1229z00_1200;

									{	/* Eval/expand.scm 151 */
										obj_t BgL_arg1230z00_1201;

										BgL_arg1230z00_1201 = CAR(BgL_xz00_1189);
										BgL_arg1228z00_1199 =
											BGL_PROCEDURE_CALL2(BgL_ez00_1965, BgL_arg1230z00_1201,
											BgL_ez00_1965);
									}
									BgL_arg1229z00_1200 =
										BGl_loopze70ze7zz__expandz00(BgL_ez00_1965,
										CDR(BgL_xz00_1189));
									return
										MAKE_YOUNG_PAIR(BgL_arg1228z00_1199, BgL_arg1229z00_1200);
								}
						}
					else
						{	/* Eval/expand.scm 146 */
							return
								BGl_errorz00zz__errorz00(BGl_string1663z00zz__expandz00,
								BGl_string1661z00zz__expandz00, BgL_xz00_1189);
						}
				}
		}

	}



/* &application-eval-expander */
	obj_t BGl_z62applicationzd2evalzd2expanderz62zz__expandz00(obj_t
		BgL_envz00_1928, obj_t BgL_xz00_1929, obj_t BgL_ez00_1930)
	{
		{	/* Eval/expand.scm 141 */
			return BGl_loopze70ze7zz__expandz00(BgL_ez00_1930, BgL_xz00_1929);
		}

	}



/* &application-eval-expander! */
	obj_t BGl_z62applicationzd2evalzd2expanderz12z70zz__expandz00(obj_t
		BgL_envz00_1936, obj_t BgL_xz00_1937, obj_t BgL_ez00_1938)
	{
		{	/* Eval/expand.scm 156 */
			{
				obj_t BgL_yz00_1972;

				BgL_yz00_1972 = BgL_xz00_1937;
			BgL_loopz00_1971:
				if (NULLP(BgL_yz00_1972))
					{	/* Eval/expand.scm 159 */
						return BgL_xz00_1937;
					}
				else
					{	/* Eval/expand.scm 159 */
						if (PAIRP(BgL_yz00_1972))
							{	/* Eval/expand.scm 161 */
								{	/* Eval/expand.scm 164 */
									obj_t BgL_arg1236z00_1973;

									{	/* Eval/expand.scm 164 */
										obj_t BgL_arg1238z00_1974;

										BgL_arg1238z00_1974 = CAR(BgL_yz00_1972);
										BgL_arg1236z00_1973 =
											BGL_PROCEDURE_CALL2(BgL_ez00_1938, BgL_arg1238z00_1974,
											BgL_ez00_1938);
									}
									SET_CAR(BgL_yz00_1972, BgL_arg1236z00_1973);
								}
								{
									obj_t BgL_yz00_2085;

									BgL_yz00_2085 = CDR(BgL_yz00_1972);
									BgL_yz00_1972 = BgL_yz00_2085;
									goto BgL_loopz00_1971;
								}
							}
						else
							{	/* Eval/expand.scm 161 */
								return
									BGl_errorz00zz__errorz00(BGl_string1663z00zz__expandz00,
									BGl_string1661z00zz__expandz00, BgL_yz00_1972);
							}
					}
			}
		}

	}



/* %lexical-stack */
	BGL_EXPORTED_DEF obj_t BGl_z52lexicalzd2stackz80zz__expandz00(void)
	{
		{	/* Eval/expand.scm 170 */
			return BGL_LEXICAL_STACK();
		}

	}



/* &%lexical-stack */
	obj_t BGl_z62z52lexicalzd2stackze2zz__expandz00(obj_t BgL_envz00_1952)
	{
		{	/* Eval/expand.scm 170 */
			return BGl_z52lexicalzd2stackz80zz__expandz00();
		}

	}



/* %with-lexical */
	BGL_EXPORTED_DEF obj_t BGl_z52withzd2lexicalz80zz__expandz00(obj_t
		BgL_newz00_19, obj_t BgL_formz00_20, obj_t BgL_ez00_21, obj_t BgL_keyz00_22)
	{
		{	/* Eval/expand.scm 176 */
			{	/* Eval/expand.scm 177 */
				obj_t BgL_oldzd2lexicalzd2stackz00_1213;

				BgL_oldzd2lexicalzd2stackz00_1213 = BGL_LEXICAL_STACK();
				{	/* Eval/expand.scm 179 */
					obj_t BgL_arg1242z00_1214;

					{	/* Eval/expand.scm 179 */
						obj_t BgL_arg1244z00_1215;

						if (NULLP(BgL_newz00_19))
							{	/* Eval/expand.scm 179 */
								BgL_arg1244z00_1215 = BNIL;
							}
						else
							{	/* Eval/expand.scm 179 */
								obj_t BgL_head1090z00_1218;

								BgL_head1090z00_1218 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1088z00_1220;
									obj_t BgL_tail1091z00_1221;

									BgL_l1088z00_1220 = BgL_newz00_19;
									BgL_tail1091z00_1221 = BgL_head1090z00_1218;
								BgL_zc3z04anonymousza31246ze3z87_1222:
									if (NULLP(BgL_l1088z00_1220))
										{	/* Eval/expand.scm 179 */
											BgL_arg1244z00_1215 = CDR(BgL_head1090z00_1218);
										}
									else
										{	/* Eval/expand.scm 179 */
											obj_t BgL_newtail1092z00_1224;

											{	/* Eval/expand.scm 179 */
												obj_t BgL_arg1249z00_1226;

												{	/* Eval/expand.scm 179 */
													obj_t BgL_nz00_1227;

													BgL_nz00_1227 = CAR(((obj_t) BgL_l1088z00_1220));
													{	/* Eval/expand.scm 180 */
														obj_t BgL_fz00_1228;

														BgL_fz00_1228 =
															BGl_parsezd2formalzd2identz00zz__evutilsz00
															(BgL_nz00_1227,
															BGl_getzd2sourcezd2locationz00zz__readerz00
															(BgL_ez00_21));
														if (PAIRP(BgL_fz00_1228))
															{	/* Eval/expand.scm 182 */
																BgL_arg1249z00_1226 =
																	MAKE_YOUNG_PAIR(CAR(BgL_fz00_1228),
																	BgL_keyz00_22);
															}
														else
															{	/* Eval/expand.scm 182 */
																BgL_arg1249z00_1226 =
																	MAKE_YOUNG_PAIR(BgL_nz00_1227, BgL_keyz00_22);
															}
													}
												}
												BgL_newtail1092z00_1224 =
													MAKE_YOUNG_PAIR(BgL_arg1249z00_1226, BNIL);
											}
											SET_CDR(BgL_tail1091z00_1221, BgL_newtail1092z00_1224);
											{	/* Eval/expand.scm 179 */
												obj_t BgL_arg1248z00_1225;

												BgL_arg1248z00_1225 = CDR(((obj_t) BgL_l1088z00_1220));
												{
													obj_t BgL_tail1091z00_2111;
													obj_t BgL_l1088z00_2110;

													BgL_l1088z00_2110 = BgL_arg1248z00_1225;
													BgL_tail1091z00_2111 = BgL_newtail1092z00_1224;
													BgL_tail1091z00_1221 = BgL_tail1091z00_2111;
													BgL_l1088z00_1220 = BgL_l1088z00_2110;
													goto BgL_zc3z04anonymousza31246ze3z87_1222;
												}
											}
										}
								}
							}
						BgL_arg1242z00_1214 =
							BGl_appendzd221011zd2zz__expandz00(BgL_arg1244z00_1215,
							BgL_oldzd2lexicalzd2stackz00_1213);
					}
					BGL_LEXICAL_STACK_SET(BgL_arg1242z00_1214);
					BUNSPEC;
				}
				{	/* Eval/expand.scm 187 */
					obj_t BgL_exitd1043z00_1233;

					BgL_exitd1043z00_1233 = BGL_EXITD_TOP_AS_OBJ();
					{
						obj_t BgL_zc3z04anonymousza31269ze3z87_1953;

						BgL_zc3z04anonymousza31269ze3z87_1953 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31269ze3ze5zz__expandz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31269ze3z87_1953, (int) (0L),
							BgL_oldzd2lexicalzd2stackz00_1213);
						{	/* Eval/expand.scm 187 */
							obj_t BgL_arg1651z00_1726;

							{	/* Eval/expand.scm 187 */
								obj_t BgL_arg1652z00_1727;

								BgL_arg1652z00_1727 = BGL_EXITD_PROTECT(BgL_exitd1043z00_1233);
								BgL_arg1651z00_1726 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31269ze3z87_1953,
									BgL_arg1652z00_1727);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1043z00_1233, BgL_arg1651z00_1726);
							BUNSPEC;
						}
						{	/* Eval/expand.scm 188 */
							obj_t BgL_tmp1045z00_1235;

							BgL_tmp1045z00_1235 =
								BGL_PROCEDURE_CALL2(BgL_ez00_21, BgL_formz00_20, BgL_ez00_21);
							{	/* Eval/expand.scm 187 */
								bool_t BgL_test1713z00_2128;

								{	/* Eval/expand.scm 187 */
									obj_t BgL_arg1650z00_1729;

									BgL_arg1650z00_1729 =
										BGL_EXITD_PROTECT(BgL_exitd1043z00_1233);
									BgL_test1713z00_2128 = PAIRP(BgL_arg1650z00_1729);
								}
								if (BgL_test1713z00_2128)
									{	/* Eval/expand.scm 187 */
										obj_t BgL_arg1648z00_1730;

										{	/* Eval/expand.scm 187 */
											obj_t BgL_arg1649z00_1731;

											BgL_arg1649z00_1731 =
												BGL_EXITD_PROTECT(BgL_exitd1043z00_1233);
											BgL_arg1648z00_1730 = CDR(((obj_t) BgL_arg1649z00_1731));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1043z00_1233,
											BgL_arg1648z00_1730);
										BUNSPEC;
									}
								else
									{	/* Eval/expand.scm 187 */
										BFALSE;
									}
							}
							BGL_LEXICAL_STACK_SET(BgL_oldzd2lexicalzd2stackz00_1213);
							BUNSPEC;
							return BgL_tmp1045z00_1235;
						}
					}
				}
			}
		}

	}



/* &%with-lexical */
	obj_t BGl_z62z52withzd2lexicalze2zz__expandz00(obj_t BgL_envz00_1954,
		obj_t BgL_newz00_1955, obj_t BgL_formz00_1956, obj_t BgL_ez00_1957,
		obj_t BgL_keyz00_1958)
	{
		{	/* Eval/expand.scm 176 */
			{	/* Eval/expand.scm 177 */
				obj_t BgL_auxz00_2143;
				obj_t BgL_auxz00_2136;

				if (PROCEDUREP(BgL_ez00_1957))
					{	/* Eval/expand.scm 177 */
						BgL_auxz00_2143 = BgL_ez00_1957;
					}
				else
					{
						obj_t BgL_auxz00_2146;

						BgL_auxz00_2146 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__expandz00,
							BINT(6267L), BGl_string1665z00zz__expandz00,
							BGl_string1667z00zz__expandz00, BgL_ez00_1957);
						FAILURE(BgL_auxz00_2146, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_newz00_1955))
					{	/* Eval/expand.scm 177 */
						BgL_auxz00_2136 = BgL_newz00_1955;
					}
				else
					{
						obj_t BgL_auxz00_2139;

						BgL_auxz00_2139 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1664z00zz__expandz00,
							BINT(6267L), BGl_string1665z00zz__expandz00,
							BGl_string1666z00zz__expandz00, BgL_newz00_1955);
						FAILURE(BgL_auxz00_2139, BFALSE, BFALSE);
					}
				return
					BGl_z52withzd2lexicalz80zz__expandz00(BgL_auxz00_2136,
					BgL_formz00_1956, BgL_auxz00_2143, BgL_keyz00_1958);
			}
		}

	}



/* &<@anonymous:1269> */
	obj_t BGl_z62zc3z04anonymousza31269ze3ze5zz__expandz00(obj_t BgL_envz00_1959)
	{
		{	/* Eval/expand.scm 187 */
			{
				obj_t BgL_oldzd2lexicalzd2stackz00_1960;

				BgL_oldzd2lexicalzd2stackz00_1960 =
					((obj_t) PROCEDURE_REF(BgL_envz00_1959, (int) (0L)));
				BGL_LEXICAL_STACK_SET(BgL_oldzd2lexicalzd2stackz00_1960);
				return BUNSPEC;
			}
		}

	}



/* expand-error */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t
		BgL_procz00_23, obj_t BgL_msgz00_24, obj_t BgL_objz00_25)
	{
		{	/* Eval/expand.scm 194 */
			if (EPAIRP(BgL_objz00_25))
				{	/* Eval/expand.scm 196 */
					obj_t BgL_ezd2103zd2_1243;

					BgL_ezd2103zd2_1243 = CER(((obj_t) BgL_objz00_25));
					if (PAIRP(BgL_ezd2103zd2_1243))
						{	/* Eval/expand.scm 196 */
							obj_t BgL_cdrzd2109zd2_1245;

							BgL_cdrzd2109zd2_1245 = CDR(BgL_ezd2103zd2_1243);
							if ((CAR(BgL_ezd2103zd2_1243) == BGl_symbol1668z00zz__expandz00))
								{	/* Eval/expand.scm 196 */
									if (PAIRP(BgL_cdrzd2109zd2_1245))
										{	/* Eval/expand.scm 196 */
											obj_t BgL_cdrzd2113zd2_1249;

											BgL_cdrzd2113zd2_1249 = CDR(BgL_cdrzd2109zd2_1245);
											if (PAIRP(BgL_cdrzd2113zd2_1249))
												{	/* Eval/expand.scm 196 */
													if (NULLP(CDR(BgL_cdrzd2113zd2_1249)))
														{	/* Eval/expand.scm 196 */
															return
																BGl_errorzf2locationzf2zz__errorz00
																(BgL_procz00_23, BgL_msgz00_24, BgL_objz00_25,
																CAR(BgL_cdrzd2109zd2_1245),
																CAR(BgL_cdrzd2113zd2_1249));
														}
													else
														{	/* Eval/expand.scm 196 */
															BGL_TAIL return
																BGl_errorz00zz__errorz00(BgL_procz00_23,
																BgL_msgz00_24, BgL_objz00_25);
														}
												}
											else
												{	/* Eval/expand.scm 196 */
													BGL_TAIL return
														BGl_errorz00zz__errorz00(BgL_procz00_23,
														BgL_msgz00_24, BgL_objz00_25);
												}
										}
									else
										{	/* Eval/expand.scm 196 */
											BGL_TAIL return
												BGl_errorz00zz__errorz00(BgL_procz00_23, BgL_msgz00_24,
												BgL_objz00_25);
										}
								}
							else
								{	/* Eval/expand.scm 196 */
									BGL_TAIL return
										BGl_errorz00zz__errorz00(BgL_procz00_23, BgL_msgz00_24,
										BgL_objz00_25);
								}
						}
					else
						{	/* Eval/expand.scm 196 */
							BGL_TAIL return
								BGl_errorz00zz__errorz00(BgL_procz00_23, BgL_msgz00_24,
								BgL_objz00_25);
						}
				}
			else
				{	/* Eval/expand.scm 195 */
					BGL_TAIL return
						BGl_errorz00zz__errorz00(BgL_procz00_23, BgL_msgz00_24,
						BgL_objz00_25);
				}
		}

	}



/* &expand-error */
	obj_t BGl_z62expandzd2errorzb0zz__expandz00(obj_t BgL_envz00_1961,
		obj_t BgL_procz00_1962, obj_t BgL_msgz00_1963, obj_t BgL_objz00_1964)
	{
		{	/* Eval/expand.scm 194 */
			return
				BGl_expandzd2errorzd2zz__expandz00(BgL_procz00_1962, BgL_msgz00_1963,
				BgL_objz00_1964);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expandz00(void)
	{
		{	/* Eval/expand.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expandz00(void)
	{
		{	/* Eval/expand.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expandz00(void)
	{
		{	/* Eval/expand.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expandz00(void)
	{
		{	/* Eval/expand.scm 14 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			BGl_modulezd2initializa7ationz75zz__evutilsz00(470356213L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
			return BGl_modulezd2initializa7ationz75zz__macroz00(261397491L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expandz00));
		}

	}

#ifdef __cplusplus
}
#endif
