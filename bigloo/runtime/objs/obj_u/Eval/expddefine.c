/*===========================================================================*/
/*   (Eval/expddefine.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expddefine.scm -indent -o objs/obj_u/Eval/expddefine.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_DEFINE_TYPE_DEFINITIONS
#define BGL___EXPANDER_DEFINE_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_DEFINE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_mapzb2zb2zz__expander_definez00(obj_t, obj_t);
	static obj_t BGl_lambdazd2defineszd2zz__expander_definez00(obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_pairzd2ze3listze70zd6zz__expander_definez00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_definez00 =
		BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2lambdaz00zz__expander_definez00(obj_t, obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	static obj_t BGl_z62newez62zz__expander_definez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_definez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t
		BGl_z62expandzd2evalzd2definezd2inlinezb0zz__expander_definez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62expandzd2evalzd2lambdaz62zz__expander_definez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62evalzd2beginzd2expanderz62zz__expander_definez00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31811ze3ze5zz__expander_definez00(obj_t,
		obj_t);
	extern obj_t BGl_z52withzd2lexicalz80zz__expandz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_definez00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__expander_definez00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_definez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_definez00(void);
	extern obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
	extern obj_t BGl_argszd2ze3listz31zz__evutilsz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__expander_definez00(void);
	static obj_t BGl_z62zc3z04anonymousza31821ze3ze5zz__expander_definez00(obj_t,
		obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2definezd2genericzd2zz__expander_definez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2definezd2inlinezd2zz__expander_definez00(obj_t, obj_t);
	static obj_t BGl_symbol2400z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2403z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2405z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2407z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2412z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2415z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2417z00zz__expander_definez00 = BUNSPEC;
	extern obj_t BGl_getzd2sourcezd2locationz00zz__readerz00(obj_t);
	static obj_t BGl_symbol2419z00zz__expander_definez00 = BUNSPEC;
	extern obj_t BGl_parsezd2formalzd2identz00zz__evutilsz00(obj_t, obj_t);
	static obj_t
		BGl_z62expandzd2evalzd2definezd2genericzb0zz__expander_definez00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_expandzd2evalzd2externalzd2definezd2zz__expander_definez00(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t
		BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t,
		obj_t, bool_t);
	static obj_t BGl_symbol2421z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2423z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2425z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2428z00zz__expander_definez00 = BUNSPEC;
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__expander_definez00(obj_t, obj_t);
	static obj_t BGl_symbol2431z00zz__expander_definez00 = BUNSPEC;
	static obj_t
		BGl_z62expandzd2evalzd2definezd2methodzb0zz__expander_definez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2433z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__expander_definez00(void);
	static obj_t BGl_symbol2436z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2438z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32053ze3ze5zz__expander_definez00(obj_t,
		obj_t);
	static obj_t BGl_symbol2442z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2444z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2446z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_symbol2448z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__expander_definez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evalzd2beginzd2expanderz00zz__expander_definez00(obj_t);
	extern obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2definez00zz__expander_definez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31245ze3ze5zz__expander_definez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31657ze3ze5zz__expander_definez00(obj_t,
		obj_t);
	static bool_t BGl_allzf3zf3zz__expander_definez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2definezd2methodzd2zz__expander_definez00(obj_t, obj_t);
	static obj_t BGl_getzd2argsze70z35zz__expander_definez00(obj_t, obj_t);
	static obj_t BGl_symbol2395z00zz__expander_definez00 = BUNSPEC;
	static obj_t BGl_dssslzd2formalszd2ze3namesze3zz__expander_definez00(obj_t);
	extern bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t BGl_z62expandzd2evalzd2definez62zz__expander_definez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t
		BGl_expandzd2evalzd2internalzd2definezd2zz__expander_definez00(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2definezd2envzd2zz__expander_definez00,
		BgL_bgl_za762expandza7d2eval2454z00,
		BGl_z62expandzd2evalzd2definez62zz__expander_definez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2definezd2genericzd2envz00zz__expander_definez00,
		BgL_bgl_za762expandza7d2eval2455z00,
		BGl_z62expandzd2evalzd2definezd2genericzb0zz__expander_definez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2beginzd2expanderzd2envzd2zz__expander_definez00,
		BgL_bgl_za762evalza7d2beginza72456za7,
		BGl_z62evalzd2beginzd2expanderz62zz__expander_definez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2401z00zz__expander_definez00,
		BgL_bgl_string2401za700za7za7_2457za7, "&expand-eval-lambda", 19);
	      DEFINE_STRING(BGl_string2402z00zz__expander_definez00,
		BgL_bgl_string2402za700za7za7_2458za7, "&expand-eval-define", 19);
	      DEFINE_STRING(BGl_string2404z00zz__expander_definez00,
		BgL_bgl_string2404za700za7za7_2459za7, "define", 6);
	      DEFINE_STRING(BGl_string2406z00zz__expander_definez00,
		BgL_bgl_string2406za700za7za7_2460za7, "set!", 4);
	      DEFINE_STRING(BGl_string2408z00zz__expander_definez00,
		BgL_bgl_string2408za700za7za7_2461za7, "let", 3);
	      DEFINE_STRING(BGl_string2409z00zz__expander_definez00,
		BgL_bgl_string2409za700za7za7_2462za7, "define-inline", 13);
	      DEFINE_STRING(BGl_string2410z00zz__expander_definez00,
		BgL_bgl_string2410za700za7za7_2463za7, "&expand-eval-define-inline", 26);
	      DEFINE_STRING(BGl_string2413z00zz__expander_definez00,
		BgL_bgl_string2413za700za7za7_2464za7, "generic-default", 15);
	      DEFINE_STRING(BGl_string2416z00zz__expander_definez00,
		BgL_bgl_string2416za700za7za7_2465za7, "apply", 5);
	      DEFINE_STRING(BGl_string2418z00zz__expander_definez00,
		BgL_bgl_string2418za700za7za7_2466za7, "object?", 7);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2411z00zz__expander_definez00,
		BgL_bgl_za762za7c3za704anonymo2467za7,
		BGl_z62zc3z04anonymousza31811ze3ze5zz__expander_definez00);
	      DEFINE_STRING(BGl_string2420z00zz__expander_definez00,
		BgL_bgl_string2420za700za7za7_2468za7, "find-method", 11);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2414z00zz__expander_definez00,
		BgL_bgl_za762za7c3za704anonymo2469za7,
		BGl_z62zc3z04anonymousza31657ze3ze5zz__expander_definez00);
	extern obj_t BGl_errorzd2envzd2zz__errorz00;
	   
		 
		DEFINE_STRING(BGl_string2422z00zz__expander_definez00,
		BgL_bgl_string2422za700za7za7_2470za7, "and", 3);
	      DEFINE_STRING(BGl_string2424z00zz__expander_definez00,
		BgL_bgl_string2424za700za7za7_2471za7, "procedure?", 10);
	      DEFINE_STRING(BGl_string2426z00zz__expander_definez00,
		BgL_bgl_string2426za700za7za7_2472za7, "if", 2);
	      DEFINE_STRING(BGl_string2427z00zz__expander_definez00,
		BgL_bgl_string2427za700za7za7_2473za7,
		"generics can only use one DSSSL keyword", 39);
	      DEFINE_STRING(BGl_string2429z00zz__expander_definez00,
		BgL_bgl_string2429za700za7za7_2474za7, "args", 4);
	      DEFINE_STRING(BGl_string2430z00zz__expander_definez00,
		BgL_bgl_string2430za700za7za7_2475za7,
		"Illegal formal arguments for generic function", 45);
	      DEFINE_STRING(BGl_string2432z00zz__expander_definez00,
		BgL_bgl_string2432za700za7za7_2476za7, "procedure->generic", 18);
	      DEFINE_STRING(BGl_string2434z00zz__expander_definez00,
		BgL_bgl_string2434za700za7za7_2477za7, "quote", 5);
	      DEFINE_STRING(BGl_string2435z00zz__expander_definez00,
		BgL_bgl_string2435za700za7za7_2478za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2437z00zz__expander_definez00,
		BgL_bgl_string2437za700za7za7_2479za7, "error", 5);
	      DEFINE_STRING(BGl_string2439z00zz__expander_definez00,
		BgL_bgl_string2439za700za7za7_2480za7, "register-generic!", 17);
	extern obj_t BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00;
	   
		 
		DEFINE_STRING(BGl_string2440z00zz__expander_definez00,
		BgL_bgl_string2440za700za7za7_2481za7, "define-generic", 14);
	      DEFINE_STRING(BGl_string2441z00zz__expander_definez00,
		BgL_bgl_string2441za700za7za7_2482za7, "&expand-eval-define-generic", 27);
	      DEFINE_STRING(BGl_string2443z00zz__expander_definez00,
		BgL_bgl_string2443za700za7za7_2483za7, "call-next-method", 16);
	      DEFINE_STRING(BGl_string2445z00zz__expander_definez00,
		BgL_bgl_string2445za700za7za7_2484za7, "find-super-class-method", 23);
	      DEFINE_STRING(BGl_string2447z00zz__expander_definez00,
		BgL_bgl_string2447za700za7za7_2485za7, "next", 4);
	      DEFINE_STRING(BGl_string2449z00zz__expander_definez00,
		BgL_bgl_string2449za700za7za7_2486za7, "generic-add-eval-method!", 24);
	      DEFINE_STRING(BGl_string2450z00zz__expander_definez00,
		BgL_bgl_string2450za700za7za7_2487za7, "define-method", 13);
	      DEFINE_STRING(BGl_string2451z00zz__expander_definez00,
		BgL_bgl_string2451za700za7za7_2488za7, "&expand-eval-define-method", 26);
	      DEFINE_STRING(BGl_string2453z00zz__expander_definez00,
		BgL_bgl_string2453za700za7za7_2489za7, "__expander_define", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2definezd2methodzd2envz00zz__expander_definez00,
		BgL_bgl_za762expandza7d2eval2490z00,
		BGl_z62expandzd2evalzd2definezd2methodzb0zz__expander_definez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2452z00zz__expander_definez00,
		BgL_bgl_za762za7c3za704anonymo2491za7,
		BGl_z62zc3z04anonymousza32053ze3ze5zz__expander_definez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2definezd2inlinezd2envz00zz__expander_definez00,
		BgL_bgl_za762expandza7d2eval2492z00,
		BGl_z62expandzd2evalzd2definezd2inlinezb0zz__expander_definez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2390z00zz__expander_definez00,
		BgL_bgl_string2390za700za7za7_2493za7, "expand", 6);
	      DEFINE_STRING(BGl_string2391z00zz__expander_definez00,
		BgL_bgl_string2391za700za7za7_2494za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string2392z00zz__expander_definez00,
		BgL_bgl_string2392za700za7za7_2495za7,
		"/tmp/bigloo/runtime/Eval/expddefine.scm", 39);
	      DEFINE_STRING(BGl_string2393z00zz__expander_definez00,
		BgL_bgl_string2393za700za7za7_2496za7, "&eval-begin-expander", 20);
	      DEFINE_STRING(BGl_string2394z00zz__expander_definez00,
		BgL_bgl_string2394za700za7za7_2497za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2396z00zz__expander_definez00,
		BgL_bgl_string2396za700za7za7_2498za7, "begin", 5);
	      DEFINE_STRING(BGl_string2397z00zz__expander_definez00,
		BgL_bgl_string2397za700za7za7_2499za7, "Illegal `begin' form", 20);
	      DEFINE_STRING(BGl_string2398z00zz__expander_definez00,
		BgL_bgl_string2398za700za7za7_2500za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2399z00zz__expander_definez00,
		BgL_bgl_string2399za700za7za7_2501za7, "Illegal form", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2lambdazd2envzd2zz__expander_definez00,
		BgL_bgl_za762expandza7d2eval2502z00,
		BGl_z62expandzd2evalzd2lambdaz62zz__expander_definez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2400z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2403z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2405z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2407z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2412z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2415z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2417z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2419z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2421z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2423z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2425z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2428z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2431z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2433z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2436z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2438z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2442z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2444z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2446z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2448z00zz__expander_definez00));
		     ADD_ROOT((void *) (&BGl_symbol2395z00zz__expander_definez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_definez00(long
		BgL_checksumz00_3247, char *BgL_fromz00_3248)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_definez00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_definez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_definez00();
					BGl_cnstzd2initzd2zz__expander_definez00();
					BGl_importedzd2moduleszd2initz00zz__expander_definez00();
					return BGl_methodzd2initzd2zz__expander_definez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_definez00(void)
	{
		{	/* Eval/expddefine.scm 15 */
			BGl_symbol2395z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2396z00zz__expander_definez00);
			BGl_symbol2400z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2398z00zz__expander_definez00);
			BGl_symbol2403z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2404z00zz__expander_definez00);
			BGl_symbol2405z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2406z00zz__expander_definez00);
			BGl_symbol2407z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2408z00zz__expander_definez00);
			BGl_symbol2412z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2413z00zz__expander_definez00);
			BGl_symbol2415z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2416z00zz__expander_definez00);
			BGl_symbol2417z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2418z00zz__expander_definez00);
			BGl_symbol2419z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2420z00zz__expander_definez00);
			BGl_symbol2421z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2422z00zz__expander_definez00);
			BGl_symbol2423z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2424z00zz__expander_definez00);
			BGl_symbol2425z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2426z00zz__expander_definez00);
			BGl_symbol2428z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2429z00zz__expander_definez00);
			BGl_symbol2431z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2432z00zz__expander_definez00);
			BGl_symbol2433z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2434z00zz__expander_definez00);
			BGl_symbol2436z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2437z00zz__expander_definez00);
			BGl_symbol2438z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2439z00zz__expander_definez00);
			BGl_symbol2442z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2443z00zz__expander_definez00);
			BGl_symbol2444z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2445z00zz__expander_definez00);
			BGl_symbol2446z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2447z00zz__expander_definez00);
			return (BGl_symbol2448z00zz__expander_definez00 =
				bstring_to_symbol(BGl_string2449z00zz__expander_definez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_definez00(void)
	{
		{	/* Eval/expddefine.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__expander_definez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1131;

				BgL_headz00_1131 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2525;
					obj_t BgL_tailz00_2526;

					BgL_prevz00_2525 = BgL_headz00_1131;
					BgL_tailz00_2526 = BgL_l1z00_1;
				BgL_loopz00_2524:
					if (PAIRP(BgL_tailz00_2526))
						{
							obj_t BgL_newzd2prevzd2_2532;

							BgL_newzd2prevzd2_2532 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2526), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2525, BgL_newzd2prevzd2_2532);
							{
								obj_t BgL_tailz00_3285;
								obj_t BgL_prevz00_3284;

								BgL_prevz00_3284 = BgL_newzd2prevzd2_2532;
								BgL_tailz00_3285 = CDR(BgL_tailz00_2526);
								BgL_tailz00_2526 = BgL_tailz00_3285;
								BgL_prevz00_2525 = BgL_prevz00_3284;
								goto BgL_loopz00_2524;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1131);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__expander_definez00(obj_t BgL_ez00_3196,
		obj_t BgL_argsz00_1140)
	{
		{	/* Eval/expddefine.scm 64 */
			if (NULLP(BgL_argsz00_1140))
				{	/* Eval/expddefine.scm 66 */
					return BNIL;
				}
			else
				{	/* Eval/expddefine.scm 66 */
					if (SYMBOLP(BgL_argsz00_1140))
						{	/* Eval/expddefine.scm 68 */
							return BgL_argsz00_1140;
						}
					else
						{	/* Eval/expddefine.scm 68 */
							if (PAIRP(BgL_argsz00_1140))
								{	/* Eval/expddefine.scm 72 */
									bool_t BgL_test2508z00_3294;

									{	/* Eval/expddefine.scm 72 */
										bool_t BgL_test2509z00_3295;

										{	/* Eval/expddefine.scm 72 */
											obj_t BgL_tmpz00_3296;

											BgL_tmpz00_3296 = CAR(BgL_argsz00_1140);
											BgL_test2509z00_3295 = PAIRP(BgL_tmpz00_3296);
										}
										if (BgL_test2509z00_3295)
											{	/* Eval/expddefine.scm 73 */
												bool_t BgL_test2510z00_3299;

												{	/* Eval/expddefine.scm 73 */
													obj_t BgL_tmpz00_3300;

													BgL_tmpz00_3300 = CDR(CAR(BgL_argsz00_1140));
													BgL_test2510z00_3299 = PAIRP(BgL_tmpz00_3300);
												}
												if (BgL_test2510z00_3299)
													{	/* Eval/expddefine.scm 74 */
														obj_t BgL_tmpz00_3304;

														{	/* Eval/expddefine.scm 74 */
															obj_t BgL_pairz00_2543;

															BgL_pairz00_2543 = CAR(BgL_argsz00_1140);
															BgL_tmpz00_3304 = CDR(CDR(BgL_pairz00_2543));
														}
														BgL_test2508z00_3294 = NULLP(BgL_tmpz00_3304);
													}
												else
													{	/* Eval/expddefine.scm 73 */
														BgL_test2508z00_3294 = ((bool_t) 0);
													}
											}
										else
											{	/* Eval/expddefine.scm 72 */
												BgL_test2508z00_3294 = ((bool_t) 0);
											}
									}
									if (BgL_test2508z00_3294)
										{	/* Eval/expddefine.scm 77 */
											obj_t BgL_arg1220z00_1156;
											obj_t BgL_arg1221z00_1157;

											{	/* Eval/expddefine.scm 77 */
												obj_t BgL_arg1223z00_1158;
												obj_t BgL_arg1225z00_1159;

												BgL_arg1223z00_1158 = CAR(CAR(BgL_argsz00_1140));
												{	/* Eval/expddefine.scm 77 */
													obj_t BgL_arg1229z00_1163;

													{	/* Eval/expddefine.scm 77 */
														obj_t BgL_pairz00_2550;

														BgL_pairz00_2550 = CAR(BgL_argsz00_1140);
														BgL_arg1229z00_1163 = CAR(CDR(BgL_pairz00_2550));
													}
													BgL_arg1225z00_1159 =
														BGL_PROCEDURE_CALL2(BgL_ez00_3196,
														BgL_arg1229z00_1163, BgL_ez00_3196);
												}
												{	/* Eval/expddefine.scm 77 */
													obj_t BgL_list1226z00_1160;

													{	/* Eval/expddefine.scm 77 */
														obj_t BgL_arg1227z00_1161;

														BgL_arg1227z00_1161 =
															MAKE_YOUNG_PAIR(BgL_arg1225z00_1159, BNIL);
														BgL_list1226z00_1160 =
															MAKE_YOUNG_PAIR(BgL_arg1223z00_1158,
															BgL_arg1227z00_1161);
													}
													BgL_arg1220z00_1156 = BgL_list1226z00_1160;
												}
											}
											BgL_arg1221z00_1157 =
												BGl_loopze70ze7zz__expander_definez00(BgL_ez00_3196,
												CDR(BgL_argsz00_1140));
											return
												MAKE_YOUNG_PAIR(BgL_arg1220z00_1156,
												BgL_arg1221z00_1157);
										}
									else
										{	/* Eval/expddefine.scm 72 */
											return
												MAKE_YOUNG_PAIR(CAR(BgL_argsz00_1140),
												BGl_loopze70ze7zz__expander_definez00(BgL_ez00_3196,
													CDR(BgL_argsz00_1140)));
										}
								}
							else
								{	/* Eval/expddefine.scm 70 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string2390z00zz__expander_definez00,
										BGl_string2391z00zz__expander_definez00, BgL_argsz00_1140);
								}
						}
				}
		}

	}



/* eval-begin-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_evalzd2beginzd2expanderz00zz__expander_definez00(obj_t BgL_oldez00_5)
	{
		{	/* Eval/expddefine.scm 83 */
			{	/* Eval/expddefine.scm 84 */
				obj_t BgL_zc3z04anonymousza31245ze3z87_3150;

				BgL_zc3z04anonymousza31245ze3z87_3150 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31245ze3ze5zz__expander_definez00,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31245ze3z87_3150, (int) (0L),
					((obj_t) BgL_oldez00_5));
				return BgL_zc3z04anonymousza31245ze3z87_3150;
			}
		}

	}



/* &eval-begin-expander */
	obj_t BGl_z62evalzd2beginzd2expanderz62zz__expander_definez00(obj_t
		BgL_envz00_3151, obj_t BgL_oldez00_3152)
	{
		{	/* Eval/expddefine.scm 83 */
			{	/* Eval/expddefine.scm 84 */
				obj_t BgL_auxz00_3335;

				if (PROCEDUREP(BgL_oldez00_3152))
					{	/* Eval/expddefine.scm 84 */
						BgL_auxz00_3335 = BgL_oldez00_3152;
					}
				else
					{
						obj_t BgL_auxz00_3338;

						BgL_auxz00_3338 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2392z00zz__expander_definez00, BINT(2836L),
							BGl_string2393z00zz__expander_definez00,
							BGl_string2394z00zz__expander_definez00, BgL_oldez00_3152);
						FAILURE(BgL_auxz00_3338, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2beginzd2expanderz00zz__expander_definez00(BgL_auxz00_3335);
			}
		}

	}



/* &<@anonymous:1245> */
	obj_t BGl_z62zc3z04anonymousza31245ze3ze5zz__expander_definez00(obj_t
		BgL_envz00_3153, obj_t BgL_xz00_3155, obj_t BgL_ez00_3156)
	{
		{	/* Eval/expddefine.scm 84 */
			{	/* Eval/expddefine.scm 85 */
				obj_t BgL_oldez00_3154;

				BgL_oldez00_3154 = PROCEDURE_REF(BgL_envz00_3153, (int) (0L));
				{	/* Eval/expddefine.scm 85 */
					obj_t BgL_resz00_3230;

					{
						obj_t BgL_restz00_3235;

						if (PAIRP(BgL_xz00_3155))
							{	/* Eval/expddefine.scm 85 */
								if (
									(CAR(
											((obj_t) BgL_xz00_3155)) ==
										BGl_symbol2395z00zz__expander_definez00))
									{	/* Eval/expddefine.scm 85 */
										if (NULLP(CDR(((obj_t) BgL_xz00_3155))))
											{	/* Eval/expddefine.scm 85 */
												BgL_resz00_3230 = BUNSPEC;
											}
										else
											{	/* Eval/expddefine.scm 85 */
												obj_t BgL_arg1268z00_3245;

												BgL_arg1268z00_3245 = CDR(((obj_t) BgL_xz00_3155));
												BgL_restz00_3235 = BgL_arg1268z00_3245;
												if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
													(BgL_restz00_3235))
													{	/* Eval/expddefine.scm 91 */
														obj_t BgL_arg1304z00_3236;

														if (NULLP(BgL_restz00_3235))
															{	/* Eval/expddefine.scm 91 */
																BgL_arg1304z00_3236 = BNIL;
															}
														else
															{	/* Eval/expddefine.scm 91 */
																obj_t BgL_head1091z00_3237;

																BgL_head1091z00_3237 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1089z00_3239;
																	obj_t BgL_tail1092z00_3240;

																	BgL_l1089z00_3239 = BgL_restz00_3235;
																	BgL_tail1092z00_3240 = BgL_head1091z00_3237;
																BgL_zc3z04anonymousza31306ze3z87_3238:
																	if (NULLP(BgL_l1089z00_3239))
																		{	/* Eval/expddefine.scm 91 */
																			BgL_arg1304z00_3236 =
																				CDR(BgL_head1091z00_3237);
																		}
																	else
																		{	/* Eval/expddefine.scm 91 */
																			obj_t BgL_newtail1093z00_3241;

																			{	/* Eval/expddefine.scm 91 */
																				obj_t BgL_arg1309z00_3242;

																				{	/* Eval/expddefine.scm 91 */
																					obj_t BgL_xz00_3243;

																					BgL_xz00_3243 =
																						CAR(((obj_t) BgL_l1089z00_3239));
																					BgL_arg1309z00_3242 =
																						BGL_PROCEDURE_CALL2
																						(BgL_oldez00_3154, BgL_xz00_3243,
																						BgL_ez00_3156);
																				}
																				BgL_newtail1093z00_3241 =
																					MAKE_YOUNG_PAIR(BgL_arg1309z00_3242,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1092z00_3240,
																				BgL_newtail1093z00_3241);
																			{	/* Eval/expddefine.scm 91 */
																				obj_t BgL_arg1308z00_3244;

																				BgL_arg1308z00_3244 =
																					CDR(((obj_t) BgL_l1089z00_3239));
																				{
																					obj_t BgL_tail1092z00_3377;
																					obj_t BgL_l1089z00_3376;

																					BgL_l1089z00_3376 =
																						BgL_arg1308z00_3244;
																					BgL_tail1092z00_3377 =
																						BgL_newtail1093z00_3241;
																					BgL_tail1092z00_3240 =
																						BgL_tail1092z00_3377;
																					BgL_l1089z00_3239 = BgL_l1089z00_3376;
																					goto
																						BgL_zc3z04anonymousza31306ze3z87_3238;
																				}
																			}
																		}
																}
															}
														BgL_resz00_3230 =
															BGl_lambdazd2defineszd2zz__expander_definez00
															(BgL_arg1304z00_3236);
													}
												else
													{	/* Eval/expddefine.scm 89 */
														BgL_resz00_3230 =
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2396z00zz__expander_definez00,
															BGl_string2397z00zz__expander_definez00,
															BgL_xz00_3155);
													}
											}
									}
								else
									{	/* Eval/expddefine.scm 85 */
									BgL_tagzd2103zd2_3231:
										{	/* Eval/expddefine.scm 93 */
											obj_t BgL_nxz00_3233;

											BgL_nxz00_3233 =
												BGL_PROCEDURE_CALL2(BgL_oldez00_3154, BgL_xz00_3155,
												BgL_ez00_3156);
											if (PAIRP(BgL_nxz00_3233))
												{	/* Eval/expddefine.scm 94 */
													if (
														(CAR(
																((obj_t) BgL_nxz00_3233)) ==
															BGl_symbol2395z00zz__expander_definez00))
														{	/* Eval/expddefine.scm 94 */
															if (NULLP(CDR(((obj_t) BgL_nxz00_3233))))
																{	/* Eval/expddefine.scm 94 */
																	BgL_resz00_3230 = BUNSPEC;
																}
															else
																{	/* Eval/expddefine.scm 94 */
																	obj_t BgL_arg1316z00_3234;

																	BgL_arg1316z00_3234 =
																		CDR(((obj_t) BgL_nxz00_3233));
																	if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_arg1316z00_3234))
																		{	/* Eval/expddefine.scm 98 */
																			BgL_resz00_3230 =
																				BGl_lambdazd2defineszd2zz__expander_definez00
																				(BgL_arg1316z00_3234);
																		}
																	else
																		{	/* Eval/expddefine.scm 98 */
																			BgL_resz00_3230 =
																				BGl_expandzd2errorzd2zz__expandz00
																				(BGl_string2396z00zz__expander_definez00,
																				BGl_string2397z00zz__expander_definez00,
																				BgL_xz00_3155);
																		}
																}
														}
													else
														{	/* Eval/expddefine.scm 94 */
															BgL_resz00_3230 = BgL_nxz00_3233;
														}
												}
											else
												{	/* Eval/expddefine.scm 94 */
													BgL_resz00_3230 = BgL_nxz00_3233;
												}
										}
									}
							}
						else
							{	/* Eval/expddefine.scm 85 */
								goto BgL_tagzd2103zd2_3231;
							}
					}
					return BGl_evepairifyz00zz__prognz00(BgL_resz00_3230, BgL_xz00_3155);
				}
			}
		}

	}



/* expand-eval-lambda */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2lambdaz00zz__expander_definez00(obj_t BgL_xz00_6,
		obj_t BgL_ez00_7)
	{
		{	/* Eval/expddefine.scm 108 */
			{	/* Eval/expddefine.scm 109 */
				obj_t BgL_resz00_1224;

				if (PAIRP(BgL_xz00_6))
					{	/* Eval/expddefine.scm 109 */
						obj_t BgL_cdrzd2141zd2_1231;

						BgL_cdrzd2141zd2_1231 = CDR(((obj_t) BgL_xz00_6));
						if (PAIRP(BgL_cdrzd2141zd2_1231))
							{	/* Eval/expddefine.scm 109 */
								obj_t BgL_cdrzd2145zd2_1233;

								BgL_cdrzd2145zd2_1233 = CDR(BgL_cdrzd2141zd2_1231);
								if (NULLP(BgL_cdrzd2145zd2_1233))
									{	/* Eval/expddefine.scm 109 */
										BgL_resz00_1224 =
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2398z00zz__expander_definez00,
											BGl_string2399z00zz__expander_definez00, BgL_xz00_6);
									}
								else
									{	/* Eval/expddefine.scm 109 */
										obj_t BgL_arg1323z00_1235;

										BgL_arg1323z00_1235 = CAR(BgL_cdrzd2141zd2_1231);
										{	/* Eval/expddefine.scm 111 */
											obj_t BgL_eargsz00_2611;

											BgL_eargsz00_2611 =
												BGl_loopze70ze7zz__expander_definez00(BgL_ez00_7,
												BgL_arg1323z00_1235);
											{	/* Eval/expddefine.scm 111 */
												obj_t BgL_ebodyz00_2612;

												BgL_ebodyz00_2612 =
													BGl_expandzd2prognzd2zz__prognz00
													(BgL_cdrzd2145zd2_1233);
												{	/* Eval/expddefine.scm 112 */
													obj_t BgL_nez00_2613;

													BgL_nez00_2613 =
														BGl_evalzd2beginzd2expanderz00zz__expander_definez00
														(BgL_ez00_7);
													{	/* Eval/expddefine.scm 113 */

														{	/* Eval/expddefine.scm 115 */
															obj_t BgL_arg1325z00_2614;

															{	/* Eval/expddefine.scm 115 */
																obj_t BgL_arg1326z00_2615;

																BgL_arg1326z00_2615 =
																	MAKE_YOUNG_PAIR
																	(BGl_z52withzd2lexicalz80zz__expandz00
																	(BGl_argszd2ze3listz31zz__evutilsz00
																		(BgL_eargsz00_2611), BgL_ebodyz00_2612,
																		BgL_nez00_2613, BFALSE), BNIL);
																BgL_arg1325z00_2614 =
																	MAKE_YOUNG_PAIR(BgL_eargsz00_2611,
																	BgL_arg1326z00_2615);
															}
															BgL_resz00_1224 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2400z00zz__expander_definez00,
																BgL_arg1325z00_2614);
														}
													}
												}
											}
										}
									}
							}
						else
							{	/* Eval/expddefine.scm 109 */
								BgL_resz00_1224 =
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string2398z00zz__expander_definez00,
									BGl_string2399z00zz__expander_definez00, BgL_xz00_6);
							}
					}
				else
					{	/* Eval/expddefine.scm 109 */
						BgL_resz00_1224 =
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string2398z00zz__expander_definez00,
							BGl_string2399z00zz__expander_definez00, BgL_xz00_6);
					}
				return BGl_evepairifyz00zz__prognz00(BgL_resz00_1224, BgL_xz00_6);
			}
		}

	}



/* &expand-eval-lambda */
	obj_t BGl_z62expandzd2evalzd2lambdaz62zz__expander_definez00(obj_t
		BgL_envz00_3157, obj_t BgL_xz00_3158, obj_t BgL_ez00_3159)
	{
		{	/* Eval/expddefine.scm 108 */
			{	/* Eval/expddefine.scm 109 */
				obj_t BgL_auxz00_3424;

				if (PROCEDUREP(BgL_ez00_3159))
					{	/* Eval/expddefine.scm 109 */
						BgL_auxz00_3424 = BgL_ez00_3159;
					}
				else
					{
						obj_t BgL_auxz00_3427;

						BgL_auxz00_3427 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2392z00zz__expander_definez00, BINT(3621L),
							BGl_string2401z00zz__expander_definez00,
							BGl_string2394z00zz__expander_definez00, BgL_ez00_3159);
						FAILURE(BgL_auxz00_3427, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2evalzd2lambdaz00zz__expander_definez00(BgL_xz00_3158,
					BgL_auxz00_3424);
			}
		}

	}



/* expand-eval-define */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2definez00zz__expander_definez00(obj_t BgL_xz00_8,
		obj_t BgL_oldez00_9)
	{
		{	/* Eval/expddefine.scm 127 */
			{	/* Eval/expddefine.scm 128 */
				obj_t BgL_newez00_3160;

				BgL_newez00_3160 =
					MAKE_FX_PROCEDURE(BGl_z62newez62zz__expander_definez00,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_newez00_3160, (int) (0L), BgL_oldez00_9);
				BGL_TAIL return
					BGl_expandzd2evalzd2externalzd2definezd2zz__expander_definez00
					(BgL_xz00_8, BgL_newez00_3160);
			}
		}

	}



/* &expand-eval-define */
	obj_t BGl_z62expandzd2evalzd2definez62zz__expander_definez00(obj_t
		BgL_envz00_3161, obj_t BgL_xz00_3162, obj_t BgL_oldez00_3163)
	{
		{	/* Eval/expddefine.scm 127 */
			{	/* Eval/expddefine.scm 128 */
				obj_t BgL_auxz00_3438;

				if (PROCEDUREP(BgL_oldez00_3163))
					{	/* Eval/expddefine.scm 128 */
						BgL_auxz00_3438 = BgL_oldez00_3163;
					}
				else
					{
						obj_t BgL_auxz00_3441;

						BgL_auxz00_3441 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2392z00zz__expander_definez00, BINT(4526L),
							BGl_string2402z00zz__expander_definez00,
							BGl_string2394z00zz__expander_definez00, BgL_oldez00_3163);
						FAILURE(BgL_auxz00_3441, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2evalzd2definez00zz__expander_definez00(BgL_xz00_3162,
					BgL_auxz00_3438);
			}
		}

	}



/* &newe */
	obj_t BGl_z62newez62zz__expander_definez00(obj_t BgL_envz00_3164,
		obj_t BgL_xz00_3166, obj_t BgL_ez00_3167)
	{
		{	/* Eval/expddefine.scm 128 */
			{	/* Eval/expddefine.scm 128 */
				obj_t BgL_oldez00_3165;

				BgL_oldez00_3165 = ((obj_t) PROCEDURE_REF(BgL_envz00_3164, (int) (0L)));
				if (PAIRP(BgL_xz00_3166))
					{	/* Eval/expddefine.scm 128 */
						if (
							(CAR(
									((obj_t) BgL_xz00_3166)) ==
								BGl_symbol2403z00zz__expander_definez00))
							{	/* Eval/expddefine.scm 128 */
								return
									BGl_expandzd2evalzd2internalzd2definezd2zz__expander_definez00
									(BgL_xz00_3166, BgL_ez00_3167);
							}
						else
							{	/* Eval/expddefine.scm 128 */
								return
									BGL_PROCEDURE_CALL2(BgL_oldez00_3165, BgL_xz00_3166,
									BgL_ez00_3167);
							}
					}
				else
					{	/* Eval/expddefine.scm 128 */
						return
							BGL_PROCEDURE_CALL2(BgL_oldez00_3165, BgL_xz00_3166,
							BgL_ez00_3167);
					}
			}
		}

	}



/* expand-eval-external-define */
	obj_t BGl_expandzd2evalzd2externalzd2definezd2zz__expander_definez00(obj_t
		BgL_xz00_10, obj_t BgL_ez00_11)
	{
		{	/* Eval/expddefine.scm 139 */
			{	/* Eval/expddefine.scm 140 */
				obj_t BgL_ez00_1255;

				BgL_ez00_1255 =
					BGl_evalzd2beginzd2expanderz00zz__expander_definez00(BgL_ez00_11);
				{	/* Eval/expddefine.scm 141 */
					obj_t BgL_locz00_1257;

					BgL_locz00_1257 =
						BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_xz00_10);
					{	/* Eval/expddefine.scm 142 */
						obj_t BgL_resz00_1258;

						{	/* Eval/expddefine.scm 143 */
							bool_t BgL_test2529z00_3468;

							if (PAIRP(BgL_xz00_10))
								{	/* Eval/expddefine.scm 143 */
									bool_t BgL_test2531z00_3471;

									{	/* Eval/expddefine.scm 143 */
										obj_t BgL_tmpz00_3472;

										BgL_tmpz00_3472 = CDR(BgL_xz00_10);
										BgL_test2531z00_3471 = PAIRP(BgL_tmpz00_3472);
									}
									if (BgL_test2531z00_3471)
										{	/* Eval/expddefine.scm 143 */
											obj_t BgL_tmpz00_3475;

											BgL_tmpz00_3475 = CDR(CDR(BgL_xz00_10));
											BgL_test2529z00_3468 = PAIRP(BgL_tmpz00_3475);
										}
									else
										{	/* Eval/expddefine.scm 143 */
											BgL_test2529z00_3468 = ((bool_t) 0);
										}
								}
							else
								{	/* Eval/expddefine.scm 143 */
									BgL_test2529z00_3468 = ((bool_t) 0);
								}
							if (BgL_test2529z00_3468)
								{	/* Eval/expddefine.scm 144 */
									obj_t BgL_typez00_1265;

									BgL_typez00_1265 = CAR(CDR(BgL_xz00_10));
									{	/* Eval/expddefine.scm 146 */
										bool_t BgL_test2532z00_3481;

										if (PAIRP(BgL_typez00_1265))
											{	/* Eval/expddefine.scm 146 */
												obj_t BgL_tmpz00_3484;

												BgL_tmpz00_3484 = CAR(BgL_typez00_1265);
												BgL_test2532z00_3481 = SYMBOLP(BgL_tmpz00_3484);
											}
										else
											{	/* Eval/expddefine.scm 146 */
												BgL_test2532z00_3481 = ((bool_t) 0);
											}
										if (BgL_test2532z00_3481)
											{	/* Eval/expddefine.scm 147 */
												obj_t BgL_arg1343z00_1269;

												{	/* Eval/expddefine.scm 147 */
													obj_t BgL_arg1344z00_1270;
													obj_t BgL_arg1346z00_1271;

													{	/* Eval/expddefine.scm 147 */
														obj_t BgL_arg1347z00_1272;

														BgL_arg1347z00_1272 =
															BGl_parsezd2formalzd2identz00zz__evutilsz00(CAR
															(BgL_typez00_1265), BgL_locz00_1257);
														BgL_arg1344z00_1270 =
															CAR(((obj_t) BgL_arg1347z00_1272));
													}
													{	/* Eval/expddefine.scm 148 */
														obj_t BgL_arg1349z00_1274;

														{	/* Eval/expddefine.scm 148 */
															obj_t BgL_arg1350z00_1275;

															{	/* Eval/expddefine.scm 148 */
																obj_t BgL_arg1351z00_1276;
																obj_t BgL_arg1352z00_1277;

																BgL_arg1351z00_1276 =
																	BGl_loopze70ze7zz__expander_definez00
																	(BgL_ez00_1255, CDR(BgL_typez00_1265));
																{	/* Eval/expddefine.scm 149 */
																	obj_t BgL_arg1356z00_1279;

																	{	/* Eval/expddefine.scm 149 */
																		obj_t BgL_arg1357z00_1280;

																		BgL_arg1357z00_1280 =
																			BGl_expandzd2prognzd2zz__prognz00(CDR(CDR
																				(BgL_xz00_10)));
																		BgL_arg1356z00_1279 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_1255,
																			BgL_arg1357z00_1280, BgL_ez00_1255);
																	}
																	BgL_arg1352z00_1277 =
																		MAKE_YOUNG_PAIR(BgL_arg1356z00_1279, BNIL);
																}
																BgL_arg1350z00_1275 =
																	MAKE_YOUNG_PAIR(BgL_arg1351z00_1276,
																	BgL_arg1352z00_1277);
															}
															BgL_arg1349z00_1274 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2400z00zz__expander_definez00,
																BgL_arg1350z00_1275);
														}
														BgL_arg1346z00_1271 =
															MAKE_YOUNG_PAIR(BgL_arg1349z00_1274, BNIL);
													}
													BgL_arg1343z00_1269 =
														MAKE_YOUNG_PAIR(BgL_arg1344z00_1270,
														BgL_arg1346z00_1271);
												}
												BgL_resz00_1258 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2403z00zz__expander_definez00,
													BgL_arg1343z00_1269);
											}
										else
											{	/* Eval/expddefine.scm 146 */
												if (SYMBOLP(BgL_typez00_1265))
													{	/* Eval/expddefine.scm 151 */
														obj_t BgL_arg1360z00_1283;

														{	/* Eval/expddefine.scm 151 */
															obj_t BgL_arg1361z00_1284;
															obj_t BgL_arg1362z00_1285;

															{	/* Eval/expddefine.scm 151 */
																obj_t BgL_arg1363z00_1286;

																BgL_arg1363z00_1286 =
																	BGl_parsezd2formalzd2identz00zz__evutilsz00
																	(BgL_typez00_1265, BgL_locz00_1257);
																BgL_arg1361z00_1284 =
																	CAR(((obj_t) BgL_arg1363z00_1286));
															}
															{	/* Eval/expddefine.scm 152 */
																obj_t BgL_arg1364z00_1287;

																{	/* Eval/expddefine.scm 152 */
																	obj_t BgL_arg1365z00_1288;

																	BgL_arg1365z00_1288 =
																		BGl_expandzd2prognzd2zz__prognz00(CDR(CDR
																			(BgL_xz00_10)));
																	BgL_arg1364z00_1287 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_1255,
																		BgL_arg1365z00_1288, BgL_ez00_1255);
																}
																BgL_arg1362z00_1285 =
																	MAKE_YOUNG_PAIR(BgL_arg1364z00_1287, BNIL);
															}
															BgL_arg1360z00_1283 =
																MAKE_YOUNG_PAIR(BgL_arg1361z00_1284,
																BgL_arg1362z00_1285);
														}
														BgL_resz00_1258 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2403z00zz__expander_definez00,
															BgL_arg1360z00_1283);
													}
												else
													{	/* Eval/expddefine.scm 150 */
														BgL_resz00_1258 =
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2404z00zz__expander_definez00,
															BGl_string2399z00zz__expander_definez00,
															BgL_xz00_10);
													}
											}
									}
								}
							else
								{	/* Eval/expddefine.scm 143 */
									BgL_resz00_1258 =
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string2404z00zz__expander_definez00,
										BGl_string2399z00zz__expander_definez00, BgL_xz00_10);
								}
						}
						{	/* Eval/expddefine.scm 143 */

							return
								BGl_evepairifyz00zz__prognz00(BgL_resz00_1258, BgL_xz00_10);
						}
					}
				}
			}
		}

	}



/* expand-eval-internal-define */
	obj_t BGl_expandzd2evalzd2internalzd2definezd2zz__expander_definez00(obj_t
		BgL_xz00_12, obj_t BgL_ez00_13)
	{
		{	/* Eval/expddefine.scm 161 */
			{
				obj_t BgL_namez00_1298;
				obj_t BgL_argsz00_1299;
				obj_t BgL_bodyz00_1300;

				{	/* Eval/expddefine.scm 162 */
					obj_t BgL_cdrzd2170zd2_1308;

					BgL_cdrzd2170zd2_1308 = CDR(BgL_xz00_12);
					if (PAIRP(BgL_cdrzd2170zd2_1308))
						{	/* Eval/expddefine.scm 162 */
							obj_t BgL_carzd2174zd2_1310;
							obj_t BgL_cdrzd2175zd2_1311;

							BgL_carzd2174zd2_1310 = CAR(BgL_cdrzd2170zd2_1308);
							BgL_cdrzd2175zd2_1311 = CDR(BgL_cdrzd2170zd2_1308);
							if (PAIRP(BgL_carzd2174zd2_1310))
								{	/* Eval/expddefine.scm 162 */
									if (NULLP(BgL_cdrzd2175zd2_1311))
										{	/* Eval/expddefine.scm 162 */
											obj_t BgL_cdrzd2193zd2_1315;

											BgL_cdrzd2193zd2_1315 =
												CDR(((obj_t) BgL_cdrzd2170zd2_1308));
											if (PAIRP(BgL_cdrzd2193zd2_1315))
												{	/* Eval/expddefine.scm 162 */
													obj_t BgL_carzd2197zd2_1317;

													BgL_carzd2197zd2_1317 = CAR(BgL_cdrzd2193zd2_1315);
													if (PAIRP(BgL_carzd2197zd2_1317))
														{	/* Eval/expddefine.scm 162 */
															obj_t BgL_cdrzd2202zd2_1319;

															BgL_cdrzd2202zd2_1319 =
																CDR(BgL_carzd2197zd2_1317);
															if (
																(CAR(BgL_carzd2197zd2_1317) ==
																	BGl_symbol2400z00zz__expander_definez00))
																{	/* Eval/expddefine.scm 162 */
																	if (PAIRP(BgL_cdrzd2202zd2_1319))
																		{	/* Eval/expddefine.scm 162 */
																			obj_t BgL_cdrzd2206zd2_1323;

																			BgL_cdrzd2206zd2_1323 =
																				CDR(BgL_cdrzd2202zd2_1319);
																			if (NULLP(BgL_cdrzd2206zd2_1323))
																				{	/* Eval/expddefine.scm 162 */
																					obj_t BgL_cdrzd2215zd2_1325;

																					BgL_cdrzd2215zd2_1325 =
																						CDR(BgL_xz00_12);
																					{	/* Eval/expddefine.scm 162 */
																						obj_t BgL_cdrzd2219zd2_1326;

																						BgL_cdrzd2219zd2_1326 =
																							CDR(
																							((obj_t) BgL_cdrzd2215zd2_1325));
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_cdrzd2219zd2_1326))))
																							{	/* Eval/expddefine.scm 162 */
																								obj_t BgL_arg1384z00_1329;
																								obj_t BgL_arg1387z00_1330;

																								BgL_arg1384z00_1329 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2215zd2_1325));
																								BgL_arg1387z00_1330 =
																									CAR(((obj_t)
																										BgL_cdrzd2219zd2_1326));
																								{	/* Eval/expddefine.scm 175 */
																									obj_t BgL_locz00_2664;

																									BgL_locz00_2664 =
																										BGl_getzd2sourcezd2locationz00zz__readerz00
																										(BgL_xz00_12);
																									{	/* Eval/expddefine.scm 175 */
																										obj_t BgL_resz00_2665;

																										{	/* Eval/expddefine.scm 176 */
																											obj_t BgL_arg1463z00_2666;

																											{	/* Eval/expddefine.scm 176 */
																												obj_t
																													BgL_arg1464z00_2667;
																												obj_t
																													BgL_arg1465z00_2668;
																												{	/* Eval/expddefine.scm 176 */
																													obj_t
																														BgL_arg1466z00_2669;
																													BgL_arg1466z00_2669 =
																														BGl_parsezd2formalzd2identz00zz__evutilsz00
																														(BgL_arg1384z00_1329,
																														BgL_locz00_2664);
																													BgL_arg1464z00_2667 =
																														CAR(((obj_t)
																															BgL_arg1466z00_2669));
																												}
																												{	/* Eval/expddefine.scm 176 */
																													obj_t
																														BgL_arg1467z00_2670;
																													BgL_arg1467z00_2670 =
																														BGL_PROCEDURE_CALL2
																														(BgL_ez00_13,
																														BgL_arg1387z00_1330,
																														BgL_ez00_13);
																													BgL_arg1465z00_2668 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1467z00_2670,
																														BNIL);
																												}
																												BgL_arg1463z00_2666 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1464z00_2667,
																													BgL_arg1465z00_2668);
																											}
																											BgL_resz00_2665 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2403z00zz__expander_definez00,
																												BgL_arg1463z00_2666);
																										}
																										{	/* Eval/expddefine.scm 176 */

																											return
																												BGl_evepairifyz00zz__prognz00
																												(BgL_resz00_2665,
																												BgL_xz00_12);
																										}
																									}
																								}
																							}
																						else
																							{	/* Eval/expddefine.scm 162 */
																								return
																									BGl_expandzd2errorzd2zz__expandz00
																									(BGl_string2404z00zz__expander_definez00,
																									BGl_string2399z00zz__expander_definez00,
																									BgL_xz00_12);
																							}
																					}
																				}
																			else
																				{	/* Eval/expddefine.scm 162 */
																					if (NULLP(CDR(BgL_cdrzd2193zd2_1315)))
																						{	/* Eval/expddefine.scm 162 */
																							obj_t BgL_arg1391z00_1334;
																							obj_t BgL_arg1392z00_1335;

																							BgL_arg1391z00_1334 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd2170zd2_1308));
																							BgL_arg1392z00_1335 =
																								CAR(BgL_cdrzd2202zd2_1319);
																							BgL_namez00_1298 =
																								BgL_arg1391z00_1334;
																							BgL_argsz00_1299 =
																								BgL_arg1392z00_1335;
																							BgL_bodyz00_1300 =
																								BgL_cdrzd2206zd2_1323;
																						BgL_tagzd2154zd2_1301:
																							{	/* Eval/expddefine.scm 166 */
																								obj_t BgL_locz00_1414;

																								BgL_locz00_1414 =
																									BGl_getzd2sourcezd2locationz00zz__readerz00
																									(BgL_xz00_12);
																								{	/* Eval/expddefine.scm 166 */
																									obj_t BgL_eargsz00_1415;

																									BgL_eargsz00_1415 =
																										BGl_loopze70ze7zz__expander_definez00
																										(BgL_ez00_13,
																										BgL_argsz00_1299);
																									{	/* Eval/expddefine.scm 167 */
																										obj_t BgL_resz00_1416;

																										{	/* Eval/expddefine.scm 168 */
																											obj_t BgL_arg1453z00_1417;

																											{	/* Eval/expddefine.scm 168 */
																												obj_t
																													BgL_arg1454z00_1418;
																												obj_t
																													BgL_arg1455z00_1419;
																												{	/* Eval/expddefine.scm 168 */
																													obj_t
																														BgL_arg1456z00_1420;
																													BgL_arg1456z00_1420 =
																														BGl_parsezd2formalzd2identz00zz__evutilsz00
																														(BgL_namez00_1298,
																														BgL_locz00_1414);
																													BgL_arg1454z00_1418 =
																														CAR(((obj_t)
																															BgL_arg1456z00_1420));
																												}
																												{	/* Eval/expddefine.scm 171 */
																													obj_t
																														BgL_arg1457z00_1421;
																													{	/* Eval/expddefine.scm 171 */
																														obj_t
																															BgL_arg1458z00_1422;
																														{	/* Eval/expddefine.scm 171 */
																															obj_t
																																BgL_arg1459z00_1423;
																															BgL_arg1459z00_1423
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_z52withzd2lexicalz80zz__expandz00
																																(BGl_argszd2ze3listz31zz__evutilsz00
																																	(BgL_eargsz00_1415),
																																	BGl_expandzd2prognzd2zz__prognz00
																																	(BgL_bodyz00_1300),
																																	BgL_ez00_13,
																																	BFALSE),
																																BNIL);
																															BgL_arg1458z00_1422
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_eargsz00_1415,
																																BgL_arg1459z00_1423);
																														}
																														BgL_arg1457z00_1421
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2400z00zz__expander_definez00,
																															BgL_arg1458z00_1422);
																													}
																													BgL_arg1455z00_1419 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1457z00_1421,
																														BNIL);
																												}
																												BgL_arg1453z00_1417 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1454z00_1418,
																													BgL_arg1455z00_1419);
																											}
																											BgL_resz00_1416 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2403z00zz__expander_definez00,
																												BgL_arg1453z00_1417);
																										}
																										{	/* Eval/expddefine.scm 168 */

																											return
																												BGl_evepairifyz00zz__prognz00
																												(BgL_resz00_1416,
																												BgL_xz00_12);
																										}
																									}
																								}
																							}
																						}
																					else
																						{	/* Eval/expddefine.scm 162 */
																							return
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string2404z00zz__expander_definez00,
																								BGl_string2399z00zz__expander_definez00,
																								BgL_xz00_12);
																						}
																				}
																		}
																	else
																		{	/* Eval/expddefine.scm 162 */
																			obj_t BgL_cdrzd2234zd2_1337;

																			BgL_cdrzd2234zd2_1337 = CDR(BgL_xz00_12);
																			{	/* Eval/expddefine.scm 162 */
																				obj_t BgL_cdrzd2238zd2_1338;

																				BgL_cdrzd2238zd2_1338 =
																					CDR(((obj_t) BgL_cdrzd2234zd2_1337));
																				if (NULLP(CDR(
																							((obj_t) BgL_cdrzd2238zd2_1338))))
																					{	/* Eval/expddefine.scm 162 */
																						obj_t BgL_arg1396z00_1341;
																						obj_t BgL_arg1397z00_1342;

																						BgL_arg1396z00_1341 =
																							CAR(
																							((obj_t) BgL_cdrzd2234zd2_1337));
																						BgL_arg1397z00_1342 =
																							CAR(
																							((obj_t) BgL_cdrzd2238zd2_1338));
																						{	/* Eval/expddefine.scm 175 */
																							obj_t BgL_locz00_2680;

																							BgL_locz00_2680 =
																								BGl_getzd2sourcezd2locationz00zz__readerz00
																								(BgL_xz00_12);
																							{	/* Eval/expddefine.scm 175 */
																								obj_t BgL_resz00_2681;

																								{	/* Eval/expddefine.scm 176 */
																									obj_t BgL_arg1463z00_2682;

																									{	/* Eval/expddefine.scm 176 */
																										obj_t BgL_arg1464z00_2683;
																										obj_t BgL_arg1465z00_2684;

																										{	/* Eval/expddefine.scm 176 */
																											obj_t BgL_arg1466z00_2685;

																											BgL_arg1466z00_2685 =
																												BGl_parsezd2formalzd2identz00zz__evutilsz00
																												(BgL_arg1396z00_1341,
																												BgL_locz00_2680);
																											BgL_arg1464z00_2683 =
																												CAR(((obj_t)
																													BgL_arg1466z00_2685));
																										}
																										{	/* Eval/expddefine.scm 176 */
																											obj_t BgL_arg1467z00_2686;

																											BgL_arg1467z00_2686 =
																												BGL_PROCEDURE_CALL2
																												(BgL_ez00_13,
																												BgL_arg1397z00_1342,
																												BgL_ez00_13);
																											BgL_arg1465z00_2684 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1467z00_2686,
																												BNIL);
																										}
																										BgL_arg1463z00_2682 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1464z00_2683,
																											BgL_arg1465z00_2684);
																									}
																									BgL_resz00_2681 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2403z00zz__expander_definez00,
																										BgL_arg1463z00_2682);
																								}
																								{	/* Eval/expddefine.scm 176 */

																									return
																										BGl_evepairifyz00zz__prognz00
																										(BgL_resz00_2681,
																										BgL_xz00_12);
																								}
																							}
																						}
																					}
																				else
																					{	/* Eval/expddefine.scm 162 */
																						return
																							BGl_expandzd2errorzd2zz__expandz00
																							(BGl_string2404z00zz__expander_definez00,
																							BGl_string2399z00zz__expander_definez00,
																							BgL_xz00_12);
																					}
																			}
																		}
																}
															else
																{	/* Eval/expddefine.scm 162 */
																	obj_t BgL_cdrzd2249zd2_1344;

																	BgL_cdrzd2249zd2_1344 = CDR(BgL_xz00_12);
																	{	/* Eval/expddefine.scm 162 */
																		obj_t BgL_cdrzd2253zd2_1345;

																		BgL_cdrzd2253zd2_1345 =
																			CDR(((obj_t) BgL_cdrzd2249zd2_1344));
																		if (NULLP(CDR(
																					((obj_t) BgL_cdrzd2253zd2_1345))))
																			{	/* Eval/expddefine.scm 162 */
																				obj_t BgL_arg1402z00_1348;
																				obj_t BgL_arg1403z00_1349;

																				BgL_arg1402z00_1348 =
																					CAR(((obj_t) BgL_cdrzd2249zd2_1344));
																				BgL_arg1403z00_1349 =
																					CAR(((obj_t) BgL_cdrzd2253zd2_1345));
																				{	/* Eval/expddefine.scm 175 */
																					obj_t BgL_locz00_2693;

																					BgL_locz00_2693 =
																						BGl_getzd2sourcezd2locationz00zz__readerz00
																						(BgL_xz00_12);
																					{	/* Eval/expddefine.scm 175 */
																						obj_t BgL_resz00_2694;

																						{	/* Eval/expddefine.scm 176 */
																							obj_t BgL_arg1463z00_2695;

																							{	/* Eval/expddefine.scm 176 */
																								obj_t BgL_arg1464z00_2696;
																								obj_t BgL_arg1465z00_2697;

																								{	/* Eval/expddefine.scm 176 */
																									obj_t BgL_arg1466z00_2698;

																									BgL_arg1466z00_2698 =
																										BGl_parsezd2formalzd2identz00zz__evutilsz00
																										(BgL_arg1402z00_1348,
																										BgL_locz00_2693);
																									BgL_arg1464z00_2696 =
																										CAR(((obj_t)
																											BgL_arg1466z00_2698));
																								}
																								{	/* Eval/expddefine.scm 176 */
																									obj_t BgL_arg1467z00_2699;

																									BgL_arg1467z00_2699 =
																										BGL_PROCEDURE_CALL2
																										(BgL_ez00_13,
																										BgL_arg1403z00_1349,
																										BgL_ez00_13);
																									BgL_arg1465z00_2697 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1467z00_2699, BNIL);
																								}
																								BgL_arg1463z00_2695 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1464z00_2696,
																									BgL_arg1465z00_2697);
																							}
																							BgL_resz00_2694 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2403z00zz__expander_definez00,
																								BgL_arg1463z00_2695);
																						}
																						{	/* Eval/expddefine.scm 176 */

																							return
																								BGl_evepairifyz00zz__prognz00
																								(BgL_resz00_2694, BgL_xz00_12);
																						}
																					}
																				}
																			}
																		else
																			{	/* Eval/expddefine.scm 162 */
																				return
																					BGl_expandzd2errorzd2zz__expandz00
																					(BGl_string2404z00zz__expander_definez00,
																					BGl_string2399z00zz__expander_definez00,
																					BgL_xz00_12);
																			}
																	}
																}
														}
													else
														{	/* Eval/expddefine.scm 162 */
															obj_t BgL_cdrzd2268zd2_1353;

															BgL_cdrzd2268zd2_1353 =
																CDR(((obj_t) BgL_cdrzd2170zd2_1308));
															if (NULLP(CDR(((obj_t) BgL_cdrzd2268zd2_1353))))
																{	/* Eval/expddefine.scm 162 */
																	obj_t BgL_arg1408z00_1356;
																	obj_t BgL_arg1410z00_1357;

																	BgL_arg1408z00_1356 =
																		CAR(((obj_t) BgL_cdrzd2170zd2_1308));
																	BgL_arg1410z00_1357 =
																		CAR(((obj_t) BgL_cdrzd2268zd2_1353));
																	{	/* Eval/expddefine.scm 175 */
																		obj_t BgL_locz00_2706;

																		BgL_locz00_2706 =
																			BGl_getzd2sourcezd2locationz00zz__readerz00
																			(BgL_xz00_12);
																		{	/* Eval/expddefine.scm 175 */
																			obj_t BgL_resz00_2707;

																			{	/* Eval/expddefine.scm 176 */
																				obj_t BgL_arg1463z00_2708;

																				{	/* Eval/expddefine.scm 176 */
																					obj_t BgL_arg1464z00_2709;
																					obj_t BgL_arg1465z00_2710;

																					{	/* Eval/expddefine.scm 176 */
																						obj_t BgL_arg1466z00_2711;

																						BgL_arg1466z00_2711 =
																							BGl_parsezd2formalzd2identz00zz__evutilsz00
																							(BgL_arg1408z00_1356,
																							BgL_locz00_2706);
																						BgL_arg1464z00_2709 =
																							CAR(((obj_t)
																								BgL_arg1466z00_2711));
																					}
																					{	/* Eval/expddefine.scm 176 */
																						obj_t BgL_arg1467z00_2712;

																						BgL_arg1467z00_2712 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_13,
																							BgL_arg1410z00_1357, BgL_ez00_13);
																						BgL_arg1465z00_2710 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1467z00_2712, BNIL);
																					}
																					BgL_arg1463z00_2708 =
																						MAKE_YOUNG_PAIR(BgL_arg1464z00_2709,
																						BgL_arg1465z00_2710);
																				}
																				BgL_resz00_2707 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2403z00zz__expander_definez00,
																					BgL_arg1463z00_2708);
																			}
																			{	/* Eval/expddefine.scm 176 */

																				return
																					BGl_evepairifyz00zz__prognz00
																					(BgL_resz00_2707, BgL_xz00_12);
																			}
																		}
																	}
																}
															else
																{	/* Eval/expddefine.scm 162 */
																	return
																		BGl_expandzd2errorzd2zz__expandz00
																		(BGl_string2404z00zz__expander_definez00,
																		BGl_string2399z00zz__expander_definez00,
																		BgL_xz00_12);
																}
														}
												}
											else
												{	/* Eval/expddefine.scm 162 */
													obj_t BgL_cdrzd2283zd2_1360;

													BgL_cdrzd2283zd2_1360 =
														CDR(((obj_t) BgL_cdrzd2170zd2_1308));
													if (PAIRP(BgL_cdrzd2283zd2_1360))
														{	/* Eval/expddefine.scm 162 */
															if (NULLP(CDR(BgL_cdrzd2283zd2_1360)))
																{	/* Eval/expddefine.scm 162 */
																	obj_t BgL_arg1415z00_1364;
																	obj_t BgL_arg1416z00_1365;

																	BgL_arg1415z00_1364 =
																		CAR(((obj_t) BgL_cdrzd2170zd2_1308));
																	BgL_arg1416z00_1365 =
																		CAR(BgL_cdrzd2283zd2_1360);
																	{	/* Eval/expddefine.scm 175 */
																		obj_t BgL_locz00_2719;

																		BgL_locz00_2719 =
																			BGl_getzd2sourcezd2locationz00zz__readerz00
																			(BgL_xz00_12);
																		{	/* Eval/expddefine.scm 175 */
																			obj_t BgL_resz00_2720;

																			{	/* Eval/expddefine.scm 176 */
																				obj_t BgL_arg1463z00_2721;

																				{	/* Eval/expddefine.scm 176 */
																					obj_t BgL_arg1464z00_2722;
																					obj_t BgL_arg1465z00_2723;

																					{	/* Eval/expddefine.scm 176 */
																						obj_t BgL_arg1466z00_2724;

																						BgL_arg1466z00_2724 =
																							BGl_parsezd2formalzd2identz00zz__evutilsz00
																							(BgL_arg1415z00_1364,
																							BgL_locz00_2719);
																						BgL_arg1464z00_2722 =
																							CAR(((obj_t)
																								BgL_arg1466z00_2724));
																					}
																					{	/* Eval/expddefine.scm 176 */
																						obj_t BgL_arg1467z00_2725;

																						BgL_arg1467z00_2725 =
																							BGL_PROCEDURE_CALL2(BgL_ez00_13,
																							BgL_arg1416z00_1365, BgL_ez00_13);
																						BgL_arg1465z00_2723 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1467z00_2725, BNIL);
																					}
																					BgL_arg1463z00_2721 =
																						MAKE_YOUNG_PAIR(BgL_arg1464z00_2722,
																						BgL_arg1465z00_2723);
																				}
																				BgL_resz00_2720 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2403z00zz__expander_definez00,
																					BgL_arg1463z00_2721);
																			}
																			{	/* Eval/expddefine.scm 176 */

																				return
																					BGl_evepairifyz00zz__prognz00
																					(BgL_resz00_2720, BgL_xz00_12);
																			}
																		}
																	}
																}
															else
																{	/* Eval/expddefine.scm 162 */
																	return
																		BGl_expandzd2errorzd2zz__expandz00
																		(BGl_string2404z00zz__expander_definez00,
																		BGl_string2399z00zz__expander_definez00,
																		BgL_xz00_12);
																}
														}
													else
														{	/* Eval/expddefine.scm 162 */
															return
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string2404z00zz__expander_definez00,
																BGl_string2399z00zz__expander_definez00,
																BgL_xz00_12);
														}
												}
										}
									else
										{
											obj_t BgL_bodyz00_3701;
											obj_t BgL_argsz00_3699;
											obj_t BgL_namez00_3697;

											BgL_namez00_3697 = CAR(BgL_carzd2174zd2_1310);
											BgL_argsz00_3699 = CDR(BgL_carzd2174zd2_1310);
											BgL_bodyz00_3701 = BgL_cdrzd2175zd2_1311;
											BgL_bodyz00_1300 = BgL_bodyz00_3701;
											BgL_argsz00_1299 = BgL_argsz00_3699;
											BgL_namez00_1298 = BgL_namez00_3697;
											goto BgL_tagzd2154zd2_1301;
										}
								}
							else
								{	/* Eval/expddefine.scm 162 */
									obj_t BgL_cdrzd2298zd2_1370;

									BgL_cdrzd2298zd2_1370 = CDR(((obj_t) BgL_cdrzd2170zd2_1308));
									if (PAIRP(BgL_cdrzd2298zd2_1370))
										{	/* Eval/expddefine.scm 162 */
											obj_t BgL_carzd2302zd2_1372;

											BgL_carzd2302zd2_1372 = CAR(BgL_cdrzd2298zd2_1370);
											if (PAIRP(BgL_carzd2302zd2_1372))
												{	/* Eval/expddefine.scm 162 */
													obj_t BgL_cdrzd2307zd2_1374;

													BgL_cdrzd2307zd2_1374 = CDR(BgL_carzd2302zd2_1372);
													if (
														(CAR(BgL_carzd2302zd2_1372) ==
															BGl_symbol2400z00zz__expander_definez00))
														{	/* Eval/expddefine.scm 162 */
															if (PAIRP(BgL_cdrzd2307zd2_1374))
																{	/* Eval/expddefine.scm 162 */
																	obj_t BgL_cdrzd2311zd2_1378;

																	BgL_cdrzd2311zd2_1378 =
																		CDR(BgL_cdrzd2307zd2_1374);
																	if (NULLP(BgL_cdrzd2311zd2_1378))
																		{	/* Eval/expddefine.scm 162 */
																			obj_t BgL_cdrzd2321zd2_1380;

																			BgL_cdrzd2321zd2_1380 = CDR(BgL_xz00_12);
																			{	/* Eval/expddefine.scm 162 */
																				obj_t BgL_cdrzd2326zd2_1381;

																				BgL_cdrzd2326zd2_1381 =
																					CDR(((obj_t) BgL_cdrzd2321zd2_1380));
																				if (NULLP(CDR(
																							((obj_t) BgL_cdrzd2326zd2_1381))))
																					{	/* Eval/expddefine.scm 162 */
																						obj_t BgL_arg1428z00_1384;
																						obj_t BgL_arg1429z00_1385;

																						BgL_arg1428z00_1384 =
																							CAR(
																							((obj_t) BgL_cdrzd2321zd2_1380));
																						BgL_arg1429z00_1385 =
																							CAR(
																							((obj_t) BgL_cdrzd2326zd2_1381));
																						{	/* Eval/expddefine.scm 175 */
																							obj_t BgL_locz00_2740;

																							BgL_locz00_2740 =
																								BGl_getzd2sourcezd2locationz00zz__readerz00
																								(BgL_xz00_12);
																							{	/* Eval/expddefine.scm 175 */
																								obj_t BgL_resz00_2741;

																								{	/* Eval/expddefine.scm 176 */
																									obj_t BgL_arg1463z00_2742;

																									{	/* Eval/expddefine.scm 176 */
																										obj_t BgL_arg1464z00_2743;
																										obj_t BgL_arg1465z00_2744;

																										{	/* Eval/expddefine.scm 176 */
																											obj_t BgL_arg1466z00_2745;

																											BgL_arg1466z00_2745 =
																												BGl_parsezd2formalzd2identz00zz__evutilsz00
																												(BgL_arg1428z00_1384,
																												BgL_locz00_2740);
																											BgL_arg1464z00_2743 =
																												CAR(((obj_t)
																													BgL_arg1466z00_2745));
																										}
																										{	/* Eval/expddefine.scm 176 */
																											obj_t BgL_arg1467z00_2746;

																											BgL_arg1467z00_2746 =
																												BGL_PROCEDURE_CALL2
																												(BgL_ez00_13,
																												BgL_arg1429z00_1385,
																												BgL_ez00_13);
																											BgL_arg1465z00_2744 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1467z00_2746,
																												BNIL);
																										}
																										BgL_arg1463z00_2742 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1464z00_2743,
																											BgL_arg1465z00_2744);
																									}
																									BgL_resz00_2741 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2403z00zz__expander_definez00,
																										BgL_arg1463z00_2742);
																								}
																								{	/* Eval/expddefine.scm 176 */

																									return
																										BGl_evepairifyz00zz__prognz00
																										(BgL_resz00_2741,
																										BgL_xz00_12);
																								}
																							}
																						}
																					}
																				else
																					{	/* Eval/expddefine.scm 162 */
																						return
																							BGl_expandzd2errorzd2zz__expandz00
																							(BGl_string2404z00zz__expander_definez00,
																							BGl_string2399z00zz__expander_definez00,
																							BgL_xz00_12);
																					}
																			}
																		}
																	else
																		{	/* Eval/expddefine.scm 162 */
																			if (NULLP(CDR(BgL_cdrzd2298zd2_1370)))
																				{	/* Eval/expddefine.scm 162 */
																					obj_t BgL_arg1434z00_1389;
																					obj_t BgL_arg1435z00_1390;

																					BgL_arg1434z00_1389 =
																						CAR(
																						((obj_t) BgL_cdrzd2170zd2_1308));
																					BgL_arg1435z00_1390 =
																						CAR(BgL_cdrzd2307zd2_1374);
																					{
																						obj_t BgL_bodyz00_3751;
																						obj_t BgL_argsz00_3750;
																						obj_t BgL_namez00_3749;

																						BgL_namez00_3749 =
																							BgL_arg1434z00_1389;
																						BgL_argsz00_3750 =
																							BgL_arg1435z00_1390;
																						BgL_bodyz00_3751 =
																							BgL_cdrzd2311zd2_1378;
																						BgL_bodyz00_1300 = BgL_bodyz00_3751;
																						BgL_argsz00_1299 = BgL_argsz00_3750;
																						BgL_namez00_1298 = BgL_namez00_3749;
																						goto BgL_tagzd2154zd2_1301;
																					}
																				}
																			else
																				{	/* Eval/expddefine.scm 162 */
																					return
																						BGl_expandzd2errorzd2zz__expandz00
																						(BGl_string2404z00zz__expander_definez00,
																						BGl_string2399z00zz__expander_definez00,
																						BgL_xz00_12);
																				}
																		}
																}
															else
																{	/* Eval/expddefine.scm 162 */
																	obj_t BgL_cdrzd2344zd2_1392;

																	BgL_cdrzd2344zd2_1392 = CDR(BgL_xz00_12);
																	{	/* Eval/expddefine.scm 162 */
																		obj_t BgL_cdrzd2349zd2_1393;

																		BgL_cdrzd2349zd2_1393 =
																			CDR(((obj_t) BgL_cdrzd2344zd2_1392));
																		if (NULLP(CDR(
																					((obj_t) BgL_cdrzd2349zd2_1393))))
																			{	/* Eval/expddefine.scm 162 */
																				obj_t BgL_arg1439z00_1396;
																				obj_t BgL_arg1440z00_1397;

																				BgL_arg1439z00_1396 =
																					CAR(((obj_t) BgL_cdrzd2344zd2_1392));
																				BgL_arg1440z00_1397 =
																					CAR(((obj_t) BgL_cdrzd2349zd2_1393));
																				{	/* Eval/expddefine.scm 175 */
																					obj_t BgL_locz00_2756;

																					BgL_locz00_2756 =
																						BGl_getzd2sourcezd2locationz00zz__readerz00
																						(BgL_xz00_12);
																					{	/* Eval/expddefine.scm 175 */
																						obj_t BgL_resz00_2757;

																						{	/* Eval/expddefine.scm 176 */
																							obj_t BgL_arg1463z00_2758;

																							{	/* Eval/expddefine.scm 176 */
																								obj_t BgL_arg1464z00_2759;
																								obj_t BgL_arg1465z00_2760;

																								{	/* Eval/expddefine.scm 176 */
																									obj_t BgL_arg1466z00_2761;

																									BgL_arg1466z00_2761 =
																										BGl_parsezd2formalzd2identz00zz__evutilsz00
																										(BgL_arg1439z00_1396,
																										BgL_locz00_2756);
																									BgL_arg1464z00_2759 =
																										CAR(((obj_t)
																											BgL_arg1466z00_2761));
																								}
																								{	/* Eval/expddefine.scm 176 */
																									obj_t BgL_arg1467z00_2762;

																									BgL_arg1467z00_2762 =
																										BGL_PROCEDURE_CALL2
																										(BgL_ez00_13,
																										BgL_arg1440z00_1397,
																										BgL_ez00_13);
																									BgL_arg1465z00_2760 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1467z00_2762, BNIL);
																								}
																								BgL_arg1463z00_2758 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1464z00_2759,
																									BgL_arg1465z00_2760);
																							}
																							BgL_resz00_2757 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2403z00zz__expander_definez00,
																								BgL_arg1463z00_2758);
																						}
																						{	/* Eval/expddefine.scm 176 */

																							return
																								BGl_evepairifyz00zz__prognz00
																								(BgL_resz00_2757, BgL_xz00_12);
																						}
																					}
																				}
																			}
																		else
																			{	/* Eval/expddefine.scm 162 */
																				return
																					BGl_expandzd2errorzd2zz__expandz00
																					(BGl_string2404z00zz__expander_definez00,
																					BGl_string2399z00zz__expander_definez00,
																					BgL_xz00_12);
																			}
																	}
																}
														}
													else
														{	/* Eval/expddefine.scm 162 */
															obj_t BgL_cdrzd2362zd2_1399;

															BgL_cdrzd2362zd2_1399 = CDR(BgL_xz00_12);
															{	/* Eval/expddefine.scm 162 */
																obj_t BgL_cdrzd2367zd2_1400;

																BgL_cdrzd2367zd2_1400 =
																	CDR(((obj_t) BgL_cdrzd2362zd2_1399));
																if (NULLP(CDR(((obj_t) BgL_cdrzd2367zd2_1400))))
																	{	/* Eval/expddefine.scm 162 */
																		obj_t BgL_arg1444z00_1403;
																		obj_t BgL_arg1445z00_1404;

																		BgL_arg1444z00_1403 =
																			CAR(((obj_t) BgL_cdrzd2362zd2_1399));
																		BgL_arg1445z00_1404 =
																			CAR(((obj_t) BgL_cdrzd2367zd2_1400));
																		{	/* Eval/expddefine.scm 175 */
																			obj_t BgL_locz00_2769;

																			BgL_locz00_2769 =
																				BGl_getzd2sourcezd2locationz00zz__readerz00
																				(BgL_xz00_12);
																			{	/* Eval/expddefine.scm 175 */
																				obj_t BgL_resz00_2770;

																				{	/* Eval/expddefine.scm 176 */
																					obj_t BgL_arg1463z00_2771;

																					{	/* Eval/expddefine.scm 176 */
																						obj_t BgL_arg1464z00_2772;
																						obj_t BgL_arg1465z00_2773;

																						{	/* Eval/expddefine.scm 176 */
																							obj_t BgL_arg1466z00_2774;

																							BgL_arg1466z00_2774 =
																								BGl_parsezd2formalzd2identz00zz__evutilsz00
																								(BgL_arg1444z00_1403,
																								BgL_locz00_2769);
																							BgL_arg1464z00_2772 =
																								CAR(((obj_t)
																									BgL_arg1466z00_2774));
																						}
																						{	/* Eval/expddefine.scm 176 */
																							obj_t BgL_arg1467z00_2775;

																							BgL_arg1467z00_2775 =
																								BGL_PROCEDURE_CALL2(BgL_ez00_13,
																								BgL_arg1445z00_1404,
																								BgL_ez00_13);
																							BgL_arg1465z00_2773 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1467z00_2775, BNIL);
																						}
																						BgL_arg1463z00_2771 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1464z00_2772,
																							BgL_arg1465z00_2773);
																					}
																					BgL_resz00_2770 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2403z00zz__expander_definez00,
																						BgL_arg1463z00_2771);
																				}
																				{	/* Eval/expddefine.scm 176 */

																					return
																						BGl_evepairifyz00zz__prognz00
																						(BgL_resz00_2770, BgL_xz00_12);
																				}
																			}
																		}
																	}
																else
																	{	/* Eval/expddefine.scm 162 */
																		return
																			BGl_expandzd2errorzd2zz__expandz00
																			(BGl_string2404z00zz__expander_definez00,
																			BGl_string2399z00zz__expander_definez00,
																			BgL_xz00_12);
																	}
															}
														}
												}
											else
												{	/* Eval/expddefine.scm 162 */
													obj_t BgL_cdrzd2385zd2_1408;

													BgL_cdrzd2385zd2_1408 =
														CDR(((obj_t) BgL_cdrzd2170zd2_1308));
													if (NULLP(CDR(((obj_t) BgL_cdrzd2385zd2_1408))))
														{	/* Eval/expddefine.scm 162 */
															obj_t BgL_arg1450z00_1411;
															obj_t BgL_arg1451z00_1412;

															BgL_arg1450z00_1411 =
																CAR(((obj_t) BgL_cdrzd2170zd2_1308));
															BgL_arg1451z00_1412 =
																CAR(((obj_t) BgL_cdrzd2385zd2_1408));
															{	/* Eval/expddefine.scm 175 */
																obj_t BgL_locz00_2782;

																BgL_locz00_2782 =
																	BGl_getzd2sourcezd2locationz00zz__readerz00
																	(BgL_xz00_12);
																{	/* Eval/expddefine.scm 175 */
																	obj_t BgL_resz00_2783;

																	{	/* Eval/expddefine.scm 176 */
																		obj_t BgL_arg1463z00_2784;

																		{	/* Eval/expddefine.scm 176 */
																			obj_t BgL_arg1464z00_2785;
																			obj_t BgL_arg1465z00_2786;

																			{	/* Eval/expddefine.scm 176 */
																				obj_t BgL_arg1466z00_2787;

																				BgL_arg1466z00_2787 =
																					BGl_parsezd2formalzd2identz00zz__evutilsz00
																					(BgL_arg1450z00_1411,
																					BgL_locz00_2782);
																				BgL_arg1464z00_2785 =
																					CAR(((obj_t) BgL_arg1466z00_2787));
																			}
																			{	/* Eval/expddefine.scm 176 */
																				obj_t BgL_arg1467z00_2788;

																				BgL_arg1467z00_2788 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_13,
																					BgL_arg1451z00_1412, BgL_ez00_13);
																				BgL_arg1465z00_2786 =
																					MAKE_YOUNG_PAIR(BgL_arg1467z00_2788,
																					BNIL);
																			}
																			BgL_arg1463z00_2784 =
																				MAKE_YOUNG_PAIR(BgL_arg1464z00_2785,
																				BgL_arg1465z00_2786);
																		}
																		BgL_resz00_2783 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2403z00zz__expander_definez00,
																			BgL_arg1463z00_2784);
																	}
																	{	/* Eval/expddefine.scm 176 */

																		return
																			BGl_evepairifyz00zz__prognz00
																			(BgL_resz00_2783, BgL_xz00_12);
																	}
																}
															}
														}
													else
														{	/* Eval/expddefine.scm 162 */
															return
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string2404z00zz__expander_definez00,
																BGl_string2399z00zz__expander_definez00,
																BgL_xz00_12);
														}
												}
										}
									else
										{	/* Eval/expddefine.scm 162 */
											return
												BGl_expandzd2errorzd2zz__expandz00
												(BGl_string2404z00zz__expander_definez00,
												BGl_string2399z00zz__expander_definez00, BgL_xz00_12);
										}
								}
						}
					else
						{	/* Eval/expddefine.scm 162 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string2404z00zz__expander_definez00,
								BGl_string2399z00zz__expander_definez00, BgL_xz00_12);
						}
				}
			}
		}

	}



/* lambda-defines */
	obj_t BGl_lambdazd2defineszd2zz__expander_definez00(obj_t BgL_bodyz00_14)
	{
		{	/* Eval/expddefine.scm 185 */
			{
				obj_t BgL_oldformsz00_1438;
				obj_t BgL_newformsz00_1439;
				obj_t BgL_varsz00_1440;
				obj_t BgL_setsz00_1441;

				BgL_oldformsz00_1438 = BgL_bodyz00_14;
				BgL_newformsz00_1439 = BNIL;
				BgL_varsz00_1440 = BNIL;
				BgL_setsz00_1441 = BNIL;
			BgL_zc3z04anonymousza31468ze3z87_1442:
				if (PAIRP(BgL_oldformsz00_1438))
					{	/* Eval/expddefine.scm 191 */
						obj_t BgL_formz00_1444;

						BgL_formz00_1444 = CAR(BgL_oldformsz00_1438);
						{	/* Eval/expddefine.scm 191 */
							obj_t BgL_locz00_1445;

							{	/* Eval/expddefine.scm 192 */
								obj_t BgL__ortest_1042z00_1467;

								BgL__ortest_1042z00_1467 =
									BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_formz00_1444);
								if (CBOOL(BgL__ortest_1042z00_1467))
									{	/* Eval/expddefine.scm 192 */
										BgL_locz00_1445 = BgL__ortest_1042z00_1467;
									}
								else
									{	/* Eval/expddefine.scm 192 */
										BgL_locz00_1445 =
											BGl_getzd2sourcezd2locationz00zz__readerz00
											(BgL_oldformsz00_1438);
									}
							}
							{	/* Eval/expddefine.scm 192 */

								{	/* Eval/expddefine.scm 194 */
									bool_t BgL_test2562z00_3836;

									if (PAIRP(BgL_formz00_1444))
										{	/* Eval/expddefine.scm 194 */
											if (
												(CAR(BgL_formz00_1444) ==
													BGl_symbol2403z00zz__expander_definez00))
												{	/* Eval/expddefine.scm 195 */
													BgL_test2562z00_3836 = ((bool_t) 0);
												}
											else
												{	/* Eval/expddefine.scm 195 */
													BgL_test2562z00_3836 = ((bool_t) 1);
												}
										}
									else
										{	/* Eval/expddefine.scm 194 */
											BgL_test2562z00_3836 = ((bool_t) 1);
										}
									if (BgL_test2562z00_3836)
										{	/* Eval/expddefine.scm 196 */
											obj_t BgL_arg1476z00_1451;
											obj_t BgL_arg1477z00_1452;

											BgL_arg1476z00_1451 = CDR(BgL_oldformsz00_1438);
											BgL_arg1477z00_1452 =
												MAKE_YOUNG_PAIR(BgL_formz00_1444, BgL_newformsz00_1439);
											{
												obj_t BgL_newformsz00_3845;
												obj_t BgL_oldformsz00_3844;

												BgL_oldformsz00_3844 = BgL_arg1476z00_1451;
												BgL_newformsz00_3845 = BgL_arg1477z00_1452;
												BgL_newformsz00_1439 = BgL_newformsz00_3845;
												BgL_oldformsz00_1438 = BgL_oldformsz00_3844;
												goto BgL_zc3z04anonymousza31468ze3z87_1442;
											}
										}
									else
										{	/* Eval/expddefine.scm 200 */
											obj_t BgL_arg1478z00_1453;
											obj_t BgL_arg1479z00_1454;
											obj_t BgL_arg1480z00_1455;

											BgL_arg1478z00_1453 = CDR(BgL_oldformsz00_1438);
											{	/* Eval/expddefine.scm 201 */
												obj_t BgL_arg1481z00_1456;

												{	/* Eval/expddefine.scm 201 */
													obj_t BgL_pairz00_2797;

													BgL_pairz00_2797 = CDR(((obj_t) BgL_formz00_1444));
													BgL_arg1481z00_1456 = CAR(BgL_pairz00_2797);
												}
												BgL_arg1479z00_1454 =
													MAKE_YOUNG_PAIR(BgL_arg1481z00_1456,
													BgL_varsz00_1440);
											}
											{	/* Eval/expddefine.scm 202 */
												obj_t BgL_arg1482z00_1457;

												{	/* Eval/expddefine.scm 202 */
													obj_t BgL_arg1483z00_1458;

													{	/* Eval/expddefine.scm 202 */
														obj_t BgL_arg1484z00_1459;
														obj_t BgL_arg1485z00_1460;

														{	/* Eval/expddefine.scm 202 */
															obj_t BgL_arg1486z00_1461;

															{	/* Eval/expddefine.scm 202 */
																obj_t BgL_arg1487z00_1462;

																{	/* Eval/expddefine.scm 202 */
																	obj_t BgL_pairz00_2801;

																	BgL_pairz00_2801 =
																		CDR(((obj_t) BgL_formz00_1444));
																	BgL_arg1487z00_1462 = CAR(BgL_pairz00_2801);
																}
																BgL_arg1486z00_1461 =
																	BGl_parsezd2formalzd2identz00zz__evutilsz00
																	(BgL_arg1487z00_1462, BgL_locz00_1445);
															}
															BgL_arg1484z00_1459 =
																CAR(((obj_t) BgL_arg1486z00_1461));
														}
														{	/* Eval/expddefine.scm 203 */
															obj_t BgL_arg1488z00_1463;

															{	/* Eval/expddefine.scm 203 */
																obj_t BgL_pairz00_2808;

																{	/* Eval/expddefine.scm 203 */
																	obj_t BgL_pairz00_2807;

																	BgL_pairz00_2807 =
																		CDR(((obj_t) BgL_formz00_1444));
																	BgL_pairz00_2808 = CDR(BgL_pairz00_2807);
																}
																BgL_arg1488z00_1463 = CAR(BgL_pairz00_2808);
															}
															BgL_arg1485z00_1460 =
																MAKE_YOUNG_PAIR(BgL_arg1488z00_1463, BNIL);
														}
														BgL_arg1483z00_1458 =
															MAKE_YOUNG_PAIR(BgL_arg1484z00_1459,
															BgL_arg1485z00_1460);
													}
													BgL_arg1482z00_1457 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2405z00zz__expander_definez00,
														BgL_arg1483z00_1458);
												}
												BgL_arg1480z00_1455 =
													MAKE_YOUNG_PAIR(BgL_arg1482z00_1457,
													BgL_setsz00_1441);
											}
											{
												obj_t BgL_setsz00_3867;
												obj_t BgL_varsz00_3866;
												obj_t BgL_oldformsz00_3865;

												BgL_oldformsz00_3865 = BgL_arg1478z00_1453;
												BgL_varsz00_3866 = BgL_arg1479z00_1454;
												BgL_setsz00_3867 = BgL_arg1480z00_1455;
												BgL_setsz00_1441 = BgL_setsz00_3867;
												BgL_varsz00_1440 = BgL_varsz00_3866;
												BgL_oldformsz00_1438 = BgL_oldformsz00_3865;
												goto BgL_zc3z04anonymousza31468ze3z87_1442;
											}
										}
								}
							}
						}
					}
				else
					{	/* Eval/expddefine.scm 190 */
						if (NULLP(BgL_varsz00_1440))
							{	/* Eval/expddefine.scm 205 */
								BGL_TAIL return
									BGl_expandzd2prognzd2zz__prognz00(BgL_bodyz00_14);
							}
						else
							{	/* Eval/expddefine.scm 206 */
								obj_t BgL_arg1492z00_1469;

								{	/* Eval/expddefine.scm 206 */
									obj_t BgL_arg1494z00_1470;
									obj_t BgL_arg1495z00_1471;

									{	/* Eval/expddefine.scm 206 */
										obj_t BgL_head1096z00_1474;

										BgL_head1096z00_1474 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1094z00_1476;
											obj_t BgL_tail1097z00_1477;

											BgL_l1094z00_1476 = BgL_varsz00_1440;
											BgL_tail1097z00_1477 = BgL_head1096z00_1474;
										BgL_zc3z04anonymousza31497ze3z87_1478:
											if (NULLP(BgL_l1094z00_1476))
												{	/* Eval/expddefine.scm 206 */
													BgL_arg1494z00_1470 = CDR(BgL_head1096z00_1474);
												}
											else
												{	/* Eval/expddefine.scm 206 */
													obj_t BgL_newtail1098z00_1480;

													{	/* Eval/expddefine.scm 206 */
														obj_t BgL_arg1500z00_1482;

														{	/* Eval/expddefine.scm 206 */
															obj_t BgL_vz00_1483;

															BgL_vz00_1483 = CAR(((obj_t) BgL_l1094z00_1476));
															{	/* Eval/expddefine.scm 206 */
																obj_t BgL_list1501z00_1484;

																{	/* Eval/expddefine.scm 206 */
																	obj_t BgL_arg1502z00_1485;

																	BgL_arg1502z00_1485 =
																		MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																	BgL_list1501z00_1484 =
																		MAKE_YOUNG_PAIR(BgL_vz00_1483,
																		BgL_arg1502z00_1485);
																}
																BgL_arg1500z00_1482 = BgL_list1501z00_1484;
															}
														}
														BgL_newtail1098z00_1480 =
															MAKE_YOUNG_PAIR(BgL_arg1500z00_1482, BNIL);
													}
													SET_CDR(BgL_tail1097z00_1477,
														BgL_newtail1098z00_1480);
													{	/* Eval/expddefine.scm 206 */
														obj_t BgL_arg1499z00_1481;

														BgL_arg1499z00_1481 =
															CDR(((obj_t) BgL_l1094z00_1476));
														{
															obj_t BgL_tail1097z00_3884;
															obj_t BgL_l1094z00_3883;

															BgL_l1094z00_3883 = BgL_arg1499z00_1481;
															BgL_tail1097z00_3884 = BgL_newtail1098z00_1480;
															BgL_tail1097z00_1477 = BgL_tail1097z00_3884;
															BgL_l1094z00_1476 = BgL_l1094z00_3883;
															goto BgL_zc3z04anonymousza31497ze3z87_1478;
														}
													}
												}
										}
									}
									BgL_arg1495z00_1471 =
										MAKE_YOUNG_PAIR(BGl_expandzd2prognzd2zz__prognz00
										(BGl_appendzd221011zd2zz__expander_definez00(bgl_reverse
												(BgL_setsz00_1441), bgl_reverse(BgL_newformsz00_1439))),
										BNIL);
									BgL_arg1492z00_1469 =
										MAKE_YOUNG_PAIR(BgL_arg1494z00_1470, BgL_arg1495z00_1471);
								}
								return
									MAKE_YOUNG_PAIR(BGl_symbol2407z00zz__expander_definez00,
									BgL_arg1492z00_1469);
							}
					}
			}
		}

	}



/* expand-eval-define-inline */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2definezd2inlinezd2zz__expander_definez00(obj_t
		BgL_xz00_15, obj_t BgL_ez00_16)
	{
		{	/* Eval/expddefine.scm 213 */
			{
				obj_t BgL_funz00_1492;
				obj_t BgL_formalsz00_1493;
				obj_t BgL_bodyz00_1494;

				if (PAIRP(BgL_xz00_15))
					{	/* Eval/expddefine.scm 214 */
						obj_t BgL_cdrzd2421zd2_1499;

						BgL_cdrzd2421zd2_1499 = CDR(((obj_t) BgL_xz00_15));
						if (PAIRP(BgL_cdrzd2421zd2_1499))
							{	/* Eval/expddefine.scm 214 */
								obj_t BgL_carzd2425zd2_1501;
								obj_t BgL_cdrzd2426zd2_1502;

								BgL_carzd2425zd2_1501 = CAR(BgL_cdrzd2421zd2_1499);
								BgL_cdrzd2426zd2_1502 = CDR(BgL_cdrzd2421zd2_1499);
								if (PAIRP(BgL_carzd2425zd2_1501))
									{	/* Eval/expddefine.scm 214 */
										if (NULLP(BgL_cdrzd2426zd2_1502))
											{	/* Eval/expddefine.scm 214 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string2409z00zz__expander_definez00,
													BGl_string2399z00zz__expander_definez00, BgL_xz00_15);
											}
										else
											{	/* Eval/expddefine.scm 214 */
												BgL_funz00_1492 = CAR(BgL_carzd2425zd2_1501);
												BgL_formalsz00_1493 = CDR(BgL_carzd2425zd2_1501);
												BgL_bodyz00_1494 = BgL_cdrzd2426zd2_1502;
												{	/* Eval/expddefine.scm 216 */
													obj_t BgL_locz00_1507;

													BgL_locz00_1507 =
														BGl_getzd2sourcezd2locationz00zz__readerz00
														(BgL_xz00_15);
													{	/* Eval/expddefine.scm 216 */
														obj_t BgL_resz00_1508;

														{	/* Eval/expddefine.scm 217 */
															obj_t BgL_arg1514z00_1509;

															{	/* Eval/expddefine.scm 217 */
																obj_t BgL_arg1516z00_1510;
																obj_t BgL_arg1517z00_1511;

																{	/* Eval/expddefine.scm 217 */
																	obj_t BgL_arg1521z00_1512;

																	BgL_arg1521z00_1512 =
																		BGl_parsezd2formalzd2identz00zz__evutilsz00
																		(BgL_funz00_1492, BgL_locz00_1507);
																	BgL_arg1516z00_1510 =
																		CAR(((obj_t) BgL_arg1521z00_1512));
																}
																{	/* Eval/expddefine.scm 218 */
																	obj_t BgL_arg1522z00_1513;

																	{	/* Eval/expddefine.scm 218 */
																		obj_t BgL_arg1523z00_1514;

																		{	/* Eval/expddefine.scm 218 */
																			obj_t BgL_arg1524z00_1515;

																			{	/* Eval/expddefine.scm 218 */
																				obj_t BgL_arg1525z00_1516;
																				obj_t BgL_arg1526z00_1517;

																				BgL_arg1525z00_1516 =
																					BGl_loopze70ze7zz__expander_definez00
																					(BgL_ez00_16, BgL_formalsz00_1493);
																				BgL_arg1526z00_1517 =
																					MAKE_YOUNG_PAIR
																					(BGl_expandzd2prognzd2zz__prognz00
																					(BgL_bodyz00_1494), BNIL);
																				BgL_arg1524z00_1515 =
																					MAKE_YOUNG_PAIR(BgL_arg1525z00_1516,
																					BgL_arg1526z00_1517);
																			}
																			BgL_arg1523z00_1514 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2400z00zz__expander_definez00,
																				BgL_arg1524z00_1515);
																		}
																		BgL_arg1522z00_1513 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_16,
																			BgL_arg1523z00_1514, BgL_ez00_16);
																	}
																	BgL_arg1517z00_1511 =
																		MAKE_YOUNG_PAIR(BgL_arg1522z00_1513, BNIL);
																}
																BgL_arg1514z00_1509 =
																	MAKE_YOUNG_PAIR(BgL_arg1516z00_1510,
																	BgL_arg1517z00_1511);
															}
															BgL_resz00_1508 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2403z00zz__expander_definez00,
																BgL_arg1514z00_1509);
														}
														{	/* Eval/expddefine.scm 217 */

															return
																BGl_evepairifyz00zz__prognz00(BgL_resz00_1508,
																BgL_xz00_15);
														}
													}
												}
											}
									}
								else
									{	/* Eval/expddefine.scm 214 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2409z00zz__expander_definez00,
											BGl_string2399z00zz__expander_definez00, BgL_xz00_15);
									}
							}
						else
							{	/* Eval/expddefine.scm 214 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string2409z00zz__expander_definez00,
									BGl_string2399z00zz__expander_definez00, BgL_xz00_15);
							}
					}
				else
					{	/* Eval/expddefine.scm 214 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string2409z00zz__expander_definez00,
							BGl_string2399z00zz__expander_definez00, BgL_xz00_15);
					}
			}
		}

	}



/* &expand-eval-define-inline */
	obj_t BGl_z62expandzd2evalzd2definezd2inlinezb0zz__expander_definez00(obj_t
		BgL_envz00_3168, obj_t BgL_xz00_3169, obj_t BgL_ez00_3170)
	{
		{	/* Eval/expddefine.scm 213 */
			{	/* Eval/expddefine.scm 214 */
				obj_t BgL_auxz00_3928;

				if (PROCEDUREP(BgL_ez00_3170))
					{	/* Eval/expddefine.scm 214 */
						BgL_auxz00_3928 = BgL_ez00_3170;
					}
				else
					{
						obj_t BgL_auxz00_3931;

						BgL_auxz00_3931 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2392z00zz__expander_definez00, BINT(7844L),
							BGl_string2410z00zz__expander_definez00,
							BGl_string2394z00zz__expander_definez00, BgL_ez00_3170);
						FAILURE(BgL_auxz00_3931, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2evalzd2definezd2inlinezd2zz__expander_definez00
					(BgL_xz00_3169, BgL_auxz00_3928);
			}
		}

	}



/* map+ */
	obj_t BGl_mapzb2zb2zz__expander_definez00(obj_t BgL_fz00_17,
		obj_t BgL_lstz00_18)
	{
		{	/* Eval/expddefine.scm 227 */
			if (NULLP(BgL_lstz00_18))
				{	/* Eval/expddefine.scm 229 */
					return BNIL;
				}
			else
				{	/* Eval/expddefine.scm 229 */
					if (PAIRP(BgL_lstz00_18))
						{	/* Eval/expddefine.scm 234 */
							obj_t BgL_arg1530z00_1521;
							obj_t BgL_arg1531z00_1522;

							{	/* Eval/expddefine.scm 234 */
								obj_t BgL_arg1535z00_1523;

								BgL_arg1535z00_1523 = CAR(BgL_lstz00_18);
								BgL_arg1530z00_1521 =
									((obj_t(*)(obj_t,
											obj_t)) PROCEDURE_L_ENTRY(BgL_fz00_17)) (BgL_fz00_17,
									BgL_arg1535z00_1523);
							}
							BgL_arg1531z00_1522 =
								BGl_mapzb2zb2zz__expander_definez00(BgL_fz00_17,
								CDR(BgL_lstz00_18));
							return MAKE_YOUNG_PAIR(BgL_arg1530z00_1521, BgL_arg1531z00_1522);
						}
					else
						{	/* Eval/expddefine.scm 231 */
							return
								((obj_t(*)(obj_t,
										obj_t)) PROCEDURE_L_ENTRY(BgL_fz00_17)) (BgL_fz00_17,
								BgL_lstz00_18);
						}
				}
		}

	}



/* all? */
	bool_t BGl_allzf3zf3zz__expander_definez00(obj_t BgL_predz00_19,
		obj_t BgL_pz00_20)
	{
		{	/* Eval/expddefine.scm 239 */
			{
				obj_t BgL_pz00_1526;

				BgL_pz00_1526 = BgL_pz00_20;
			BgL_zc3z04anonymousza31537ze3z87_1527:
				if (NULLP(BgL_pz00_1526))
					{	/* Eval/expddefine.scm 242 */
						return ((bool_t) 1);
					}
				else
					{	/* Eval/expddefine.scm 242 */
						if (PAIRP(BgL_pz00_1526))
							{	/* Eval/expddefine.scm 243 */
								bool_t BgL_test2576z00_3956;

								{	/* Eval/expddefine.scm 243 */
									obj_t BgL_arg1544z00_1533;

									BgL_arg1544z00_1533 = CAR(BgL_pz00_1526);
									BgL_test2576z00_3956 =
										CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_19,
											BgL_arg1544z00_1533));
								}
								if (BgL_test2576z00_3956)
									{
										obj_t BgL_pz00_3963;

										BgL_pz00_3963 = CDR(BgL_pz00_1526);
										BgL_pz00_1526 = BgL_pz00_3963;
										goto BgL_zc3z04anonymousza31537ze3z87_1527;
									}
								else
									{	/* Eval/expddefine.scm 243 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Eval/expddefine.scm 243 */
								return
									CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_19, BgL_pz00_1526));
							}
					}
			}
		}

	}



/* expand-eval-define-generic */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2definezd2genericzd2zz__expander_definez00(obj_t
		BgL_xz00_21, obj_t BgL_ez00_22)
	{
		{	/* Eval/expddefine.scm 250 */
			{
				obj_t BgL_funz00_1536;
				obj_t BgL_f0z00_1537;
				obj_t BgL_formalsz00_1538;
				obj_t BgL_bodyz00_1539;

				if (PAIRP(BgL_xz00_21))
					{	/* Eval/expddefine.scm 251 */
						obj_t BgL_cdrzd2447zd2_1544;

						BgL_cdrzd2447zd2_1544 = CDR(((obj_t) BgL_xz00_21));
						if (PAIRP(BgL_cdrzd2447zd2_1544))
							{	/* Eval/expddefine.scm 251 */
								obj_t BgL_carzd2452zd2_1546;

								BgL_carzd2452zd2_1546 = CAR(BgL_cdrzd2447zd2_1544);
								if (PAIRP(BgL_carzd2452zd2_1546))
									{	/* Eval/expddefine.scm 251 */
										obj_t BgL_cdrzd2458zd2_1548;

										BgL_cdrzd2458zd2_1548 = CDR(BgL_carzd2452zd2_1546);
										if (PAIRP(BgL_cdrzd2458zd2_1548))
											{	/* Eval/expddefine.scm 251 */
												BgL_funz00_1536 = CAR(BgL_carzd2452zd2_1546);
												BgL_f0z00_1537 = CAR(BgL_cdrzd2458zd2_1548);
												BgL_formalsz00_1538 = CDR(BgL_cdrzd2458zd2_1548);
												BgL_bodyz00_1539 = CDR(BgL_cdrzd2447zd2_1544);
												{	/* Eval/expddefine.scm 253 */
													obj_t BgL_locz00_1554;

													BgL_locz00_1554 =
														BGl_getzd2sourcezd2locationz00zz__readerz00
														(BgL_xz00_21);
													{	/* Eval/expddefine.scm 253 */
														obj_t BgL_pfz00_1555;

														BgL_pfz00_1555 =
															BGl_parsezd2formalzd2identz00zz__evutilsz00
															(BgL_funz00_1536, BgL_locz00_1554);
														{	/* Eval/expddefine.scm 254 */
															obj_t BgL_idz00_1556;

															BgL_idz00_1556 = CAR(((obj_t) BgL_pfz00_1555));
															{	/* Eval/expddefine.scm 255 */
																obj_t BgL_paz00_1557;

																{	/* Eval/expddefine.scm 256 */
																	obj_t BgL_arg1820z00_1820;

																	BgL_arg1820z00_1820 =
																		MAKE_YOUNG_PAIR(BgL_f0z00_1537,
																		BgL_formalsz00_1538);
																	{	/* Eval/expddefine.scm 256 */
																		obj_t BgL_zc3z04anonymousza31821ze3z87_3173;

																		{
																			int BgL_tmpz00_3987;

																			BgL_tmpz00_3987 = (int) (1L);
																			BgL_zc3z04anonymousza31821ze3z87_3173 =
																				MAKE_L_PROCEDURE
																				(BGl_z62zc3z04anonymousza31821ze3ze5zz__expander_definez00,
																				BgL_tmpz00_3987);
																		}
																		PROCEDURE_L_SET
																			(BgL_zc3z04anonymousza31821ze3z87_3173,
																			(int) (0L), BgL_locz00_1554);
																		BgL_paz00_1557 =
																			BGl_mapzb2zb2zz__expander_definez00
																			(BgL_zc3z04anonymousza31821ze3z87_3173,
																			BgL_arg1820z00_1820);
																}}
																{	/* Eval/expddefine.scm 256 */
																	obj_t BgL_defz00_1558;

																	BgL_defz00_1558 =
																		BGl_gensymz00zz__r4_symbols_6_4z00
																		(BgL_idz00_1556);
																	{	/* Eval/expddefine.scm 258 */
																		obj_t BgL_epaz00_1559;

																		BgL_epaz00_1559 =
																			BGl_loopze70ze7zz__expander_definez00
																			(BgL_ez00_22, BgL_paz00_1557);
																		{	/* Eval/expddefine.scm 259 */
																			bool_t BgL_vaz00_1560;

																			if (NULLP(BgL_formalsz00_1538))
																				{	/* Eval/expddefine.scm 260 */
																					BgL_vaz00_1560 = ((bool_t) 0);
																				}
																			else
																				{	/* Eval/expddefine.scm 261 */
																					bool_t BgL__ortest_1044z00_1812;

																					if (PAIRP(BgL_formalsz00_1538))
																						{	/* Eval/expddefine.scm 261 */
																							BgL__ortest_1044z00_1812 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Eval/expddefine.scm 261 */
																							BgL__ortest_1044z00_1812 =
																								((bool_t) 1);
																						}
																					if (BgL__ortest_1044z00_1812)
																						{	/* Eval/expddefine.scm 261 */
																							BgL_vaz00_1560 =
																								BgL__ortest_1044z00_1812;
																						}
																					else
																						{	/* Eval/expddefine.scm 261 */
																							if (NULLP(CDR
																									(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_formalsz00_1538))))
																								{	/* Eval/expddefine.scm 262 */
																									BgL_vaz00_1560 = ((bool_t) 0);
																								}
																							else
																								{	/* Eval/expddefine.scm 262 */
																									BgL_vaz00_1560 = ((bool_t) 1);
																								}
																						}
																				}
																			{	/* Eval/expddefine.scm 260 */
																				obj_t BgL_metz00_1561;

																				BgL_metz00_1561 =
																					BGl_gensymz00zz__r4_symbols_6_4z00
																					(BgL_idz00_1556);
																				{	/* Eval/expddefine.scm 263 */
																					obj_t BgL_metzd2bodyzd2_1562;

																					BgL_metzd2bodyzd2_1562 =
																						MAKE_YOUNG_PAIR(BgL_metz00_1561,
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BGl_mapzb2zb2zz__expander_definez00
																							(BGl_proc2411z00zz__expander_definez00,
																								BgL_epaz00_1559), BNIL));
																					{	/* Eval/expddefine.scm 264 */
																						obj_t BgL_procz00_1563;

																						{	/* Eval/expddefine.scm 268 */
																							bool_t BgL_test2585z00_4008;

																							{	/* Eval/expddefine.scm 268 */
																								obj_t BgL_arg1806z00_1802;

																								{	/* Eval/expddefine.scm 268 */
																									obj_t BgL_pairz00_2832;

																									{	/* Eval/expddefine.scm 268 */
																										obj_t BgL_pairz00_2831;

																										BgL_pairz00_2831 =
																											CDR(
																											((obj_t) BgL_xz00_21));
																										BgL_pairz00_2832 =
																											CAR(BgL_pairz00_2831);
																									}
																									BgL_arg1806z00_1802 =
																										CDR(BgL_pairz00_2832);
																								}
																								BgL_test2585z00_4008 =
																									BGl_allzf3zf3zz__expander_definez00
																									(BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00,
																									BgL_arg1806z00_1802);
																							}
																							if (BgL_test2585z00_4008)
																								{	/* Eval/expddefine.scm 269 */
																									obj_t BgL_defzd2bodyzd2_1599;

																									{	/* Eval/expddefine.scm 269 */
																										obj_t BgL_arg1651z00_1637;
																										obj_t BgL_arg1652z00_1638;

																										{	/* Eval/expddefine.scm 269 */
																											obj_t BgL_arg1653z00_1639;

																											BgL_arg1653z00_1639 =
																												MAKE_YOUNG_PAIR
																												(BgL_idz00_1556, BNIL);
																											BgL_arg1651z00_1637 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2412z00zz__expander_definez00,
																												BgL_arg1653z00_1639);
																										}
																										BgL_arg1652z00_1638 =
																											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																											(BGl_mapzb2zb2zz__expander_definez00
																											(BGl_proc2414z00zz__expander_definez00,
																												BgL_epaz00_1559), BNIL);
																										BgL_defzd2bodyzd2_1599 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1651z00_1637,
																											BgL_arg1652z00_1638);
																									}
																									{	/* Eval/expddefine.scm 273 */
																										obj_t BgL_arg1609z00_1600;

																										{	/* Eval/expddefine.scm 273 */
																											obj_t BgL_arg1610z00_1601;
																											obj_t BgL_arg1611z00_1602;

																											BgL_arg1610z00_1601 =
																												MAKE_YOUNG_PAIR
																												(BgL_f0z00_1537,
																												BgL_formalsz00_1538);
																											{	/* Eval/expddefine.scm 275 */
																												obj_t
																													BgL_arg1612z00_1603;
																												{	/* Eval/expddefine.scm 275 */
																													obj_t
																														BgL_arg1613z00_1604;
																													{	/* Eval/expddefine.scm 275 */
																														obj_t
																															BgL_arg1615z00_1605;
																														obj_t
																															BgL_arg1616z00_1606;
																														{	/* Eval/expddefine.scm 275 */
																															obj_t
																																BgL_arg1617z00_1607;
																															{	/* Eval/expddefine.scm 275 */
																																obj_t
																																	BgL_arg1618z00_1608;
																																{	/* Eval/expddefine.scm 275 */
																																	obj_t
																																		BgL_arg1619z00_1609;
																																	{	/* Eval/expddefine.scm 275 */
																																		obj_t
																																			BgL_arg1620z00_1610;
																																		{	/* Eval/expddefine.scm 275 */
																																			obj_t
																																				BgL_arg1621z00_1611;
																																			{	/* Eval/expddefine.scm 275 */
																																				obj_t
																																					BgL_arg1622z00_1612;
																																				if (BgL_vaz00_1560)
																																					{	/* Eval/expddefine.scm 275 */
																																						BgL_arg1622z00_1612
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2415z00zz__expander_definez00,
																																							BgL_defzd2bodyzd2_1599);
																																					}
																																				else
																																					{	/* Eval/expddefine.scm 275 */
																																						BgL_arg1622z00_1612
																																							=
																																							BgL_defzd2bodyzd2_1599;
																																					}
																																				BgL_arg1621z00_1611
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1622z00_1612,
																																					BNIL);
																																			}
																																			BgL_arg1620z00_1610
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BNIL,
																																				BgL_arg1621z00_1611);
																																		}
																																		BgL_arg1619z00_1609
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2400z00zz__expander_definez00,
																																			BgL_arg1620z00_1610);
																																	}
																																	BgL_arg1618z00_1608
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1619z00_1609,
																																		BNIL);
																																}
																																BgL_arg1617z00_1607
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_defz00_1558,
																																	BgL_arg1618z00_1608);
																															}
																															BgL_arg1615z00_1605
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1617z00_1607,
																																BNIL);
																														}
																														{	/* Eval/expddefine.scm 278 */
																															obj_t
																																BgL_arg1623z00_1613;
																															{	/* Eval/expddefine.scm 278 */
																																obj_t
																																	BgL_arg1624z00_1614;
																																{	/* Eval/expddefine.scm 278 */
																																	obj_t
																																		BgL_arg1625z00_1615;
																																	obj_t
																																		BgL_arg1626z00_1616;
																																	{	/* Eval/expddefine.scm 278 */
																																		obj_t
																																			BgL_arg1627z00_1617;
																																		{	/* Eval/expddefine.scm 278 */
																																			obj_t
																																				BgL_arg1628z00_1618;
																																			{	/* Eval/expddefine.scm 278 */
																																				obj_t
																																					BgL_arg1629z00_1619;
																																				{	/* Eval/expddefine.scm 278 */
																																					obj_t
																																						BgL_arg1630z00_1620;
																																					{	/* Eval/expddefine.scm 278 */
																																						obj_t
																																							BgL_arg1631z00_1621;
																																						obj_t
																																							BgL_arg1634z00_1622;
																																						{	/* Eval/expddefine.scm 278 */
																																							obj_t
																																								BgL_arg1636z00_1623;
																																							{	/* Eval/expddefine.scm 278 */
																																								obj_t
																																									BgL_arg1637z00_1624;
																																								{	/* Eval/expddefine.scm 278 */
																																									obj_t
																																										BgL_pairz00_2838;
																																									BgL_pairz00_2838
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_paz00_1557));
																																									BgL_arg1637z00_1624
																																										=
																																										CAR
																																										(BgL_pairz00_2838);
																																								}
																																								BgL_arg1636z00_1623
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1637z00_1624,
																																									BNIL);
																																							}
																																							BgL_arg1631z00_1621
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2417z00zz__expander_definez00,
																																								BgL_arg1636z00_1623);
																																						}
																																						{	/* Eval/expddefine.scm 279 */
																																							obj_t
																																								BgL_arg1638z00_1625;
																																							{	/* Eval/expddefine.scm 279 */
																																								obj_t
																																									BgL_arg1639z00_1626;
																																								{	/* Eval/expddefine.scm 279 */
																																									obj_t
																																										BgL_arg1640z00_1627;
																																									obj_t
																																										BgL_arg1641z00_1628;
																																									{	/* Eval/expddefine.scm 279 */
																																										obj_t
																																											BgL_pairz00_2842;
																																										BgL_pairz00_2842
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_paz00_1557));
																																										BgL_arg1640z00_1627
																																											=
																																											CAR
																																											(BgL_pairz00_2842);
																																									}
																																									BgL_arg1641z00_1628
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_idz00_1556,
																																										BNIL);
																																									BgL_arg1639z00_1626
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1640z00_1627,
																																										BgL_arg1641z00_1628);
																																								}
																																								BgL_arg1638z00_1625
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2419z00zz__expander_definez00,
																																									BgL_arg1639z00_1626);
																																							}
																																							BgL_arg1634z00_1622
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1638z00_1625,
																																								BNIL);
																																						}
																																						BgL_arg1630z00_1620
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1631z00_1621,
																																							BgL_arg1634z00_1622);
																																					}
																																					BgL_arg1629z00_1619
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2421z00zz__expander_definez00,
																																						BgL_arg1630z00_1620);
																																				}
																																				BgL_arg1628z00_1618
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1629z00_1619,
																																					BNIL);
																																			}
																																			BgL_arg1627z00_1617
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_metz00_1561,
																																				BgL_arg1628z00_1618);
																																		}
																																		BgL_arg1625z00_1615
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1627z00_1617,
																																			BNIL);
																																	}
																																	{	/* Eval/expddefine.scm 280 */
																																		obj_t
																																			BgL_arg1642z00_1629;
																																		{	/* Eval/expddefine.scm 280 */
																																			obj_t
																																				BgL_arg1643z00_1630;
																																			{	/* Eval/expddefine.scm 280 */
																																				obj_t
																																					BgL_arg1644z00_1631;
																																				obj_t
																																					BgL_arg1645z00_1632;
																																				{	/* Eval/expddefine.scm 280 */
																																					obj_t
																																						BgL_arg1646z00_1633;
																																					BgL_arg1646z00_1633
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_metz00_1561,
																																						BNIL);
																																					BgL_arg1644z00_1631
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2423z00zz__expander_definez00,
																																						BgL_arg1646z00_1633);
																																				}
																																				{	/* Eval/expddefine.scm 281 */
																																					obj_t
																																						BgL_arg1648z00_1634;
																																					obj_t
																																						BgL_arg1649z00_1635;
																																					if (BgL_vaz00_1560)
																																						{	/* Eval/expddefine.scm 281 */
																																							BgL_arg1648z00_1634
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2415z00zz__expander_definez00,
																																								BgL_metzd2bodyzd2_1562);
																																						}
																																					else
																																						{	/* Eval/expddefine.scm 281 */
																																							BgL_arg1648z00_1634
																																								=
																																								BgL_metzd2bodyzd2_1562;
																																						}
																																					{	/* Eval/expddefine.scm 282 */
																																						obj_t
																																							BgL_arg1650z00_1636;
																																						BgL_arg1650z00_1636
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_defz00_1558,
																																							BNIL);
																																						BgL_arg1649z00_1635
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1650z00_1636,
																																							BNIL);
																																					}
																																					BgL_arg1645z00_1632
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1648z00_1634,
																																						BgL_arg1649z00_1635);
																																				}
																																				BgL_arg1643z00_1630
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1644z00_1631,
																																					BgL_arg1645z00_1632);
																																			}
																																			BgL_arg1642z00_1629
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2425z00zz__expander_definez00,
																																				BgL_arg1643z00_1630);
																																		}
																																		BgL_arg1626z00_1616
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1642z00_1629,
																																			BNIL);
																																	}
																																	BgL_arg1624z00_1614
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1625z00_1615,
																																		BgL_arg1626z00_1616);
																																}
																																BgL_arg1623z00_1613
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2407z00zz__expander_definez00,
																																	BgL_arg1624z00_1614);
																															}
																															BgL_arg1616z00_1606
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1623z00_1613,
																																BNIL);
																														}
																														BgL_arg1613z00_1604
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1615z00_1605,
																															BgL_arg1616z00_1606);
																													}
																													BgL_arg1612z00_1603 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2407z00zz__expander_definez00,
																														BgL_arg1613z00_1604);
																												}
																												BgL_arg1611z00_1602 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1612z00_1603,
																													BNIL);
																											}
																											BgL_arg1609z00_1600 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1610z00_1601,
																												BgL_arg1611z00_1602);
																										}
																										BgL_procz00_1563 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2400z00zz__expander_definez00,
																											BgL_arg1609z00_1600);
																									}
																								}
																							else
																								{	/* Eval/expddefine.scm 283 */
																									bool_t BgL_test2588z00_4063;

																									if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_formalsz00_1538))
																										{	/* Eval/expddefine.scm 283 */
																											BgL_test2588z00_4063 =
																												CBOOL
																												(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																												((BOPTIONAL),
																													BgL_formalsz00_1538));
																										}
																									else
																										{	/* Eval/expddefine.scm 283 */
																											BgL_test2588z00_4063 =
																												((bool_t) 0);
																										}
																									if (BgL_test2588z00_4063)
																										{	/* Eval/expddefine.scm 284 */
																											bool_t
																												BgL_test2590z00_4068;
																											{	/* Eval/expddefine.scm 284 */
																												obj_t
																													BgL_arg1738z00_1719;
																												{	/* Eval/expddefine.scm 284 */
																													obj_t
																														BgL_arg1739z00_1720;
																													{	/* Eval/expddefine.scm 284 */
																														obj_t
																															BgL_hook1103z00_1721;
																														BgL_hook1103z00_1721
																															=
																															MAKE_YOUNG_PAIR
																															(BFALSE, BNIL);
																														{
																															obj_t
																																BgL_l1100z00_1723;
																															obj_t
																																BgL_h1101z00_1724;
																															BgL_l1100z00_1723
																																=
																																BgL_formalsz00_1538;
																															BgL_h1101z00_1724
																																=
																																BgL_hook1103z00_1721;
																														BgL_zc3z04anonymousza31740ze3z87_1725:
																															if (NULLP
																																(BgL_l1100z00_1723))
																																{	/* Eval/expddefine.scm 284 */
																																	BgL_arg1739z00_1720
																																		=
																																		CDR
																																		(BgL_hook1103z00_1721);
																																}
																															else
																																{	/* Eval/expddefine.scm 284 */
																																	bool_t
																																		BgL_test2592z00_4073;
																																	{	/* Eval/expddefine.scm 284 */
																																		obj_t
																																			BgL_arg1745z00_1732;
																																		BgL_arg1745z00_1732
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_l1100z00_1723));
																																		BgL_test2592z00_4073
																																			=
																																			BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																																			(BgL_arg1745z00_1732);
																																	}
																																	if (BgL_test2592z00_4073)
																																		{	/* Eval/expddefine.scm 284 */
																																			obj_t
																																				BgL_nh1102z00_1728;
																																			{	/* Eval/expddefine.scm 284 */
																																				obj_t
																																					BgL_arg1743z00_1730;
																																				BgL_arg1743z00_1730
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_l1100z00_1723));
																																				BgL_nh1102z00_1728
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1743z00_1730,
																																					BNIL);
																																			}
																																			SET_CDR
																																				(BgL_h1101z00_1724,
																																				BgL_nh1102z00_1728);
																																			{	/* Eval/expddefine.scm 284 */
																																				obj_t
																																					BgL_arg1741z00_1729;
																																				BgL_arg1741z00_1729
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_l1100z00_1723));
																																				{
																																					obj_t
																																						BgL_h1101z00_4084;
																																					obj_t
																																						BgL_l1100z00_4083;
																																					BgL_l1100z00_4083
																																						=
																																						BgL_arg1741z00_1729;
																																					BgL_h1101z00_4084
																																						=
																																						BgL_nh1102z00_1728;
																																					BgL_h1101z00_1724
																																						=
																																						BgL_h1101z00_4084;
																																					BgL_l1100z00_1723
																																						=
																																						BgL_l1100z00_4083;
																																					goto
																																						BgL_zc3z04anonymousza31740ze3z87_1725;
																																				}
																																			}
																																		}
																																	else
																																		{	/* Eval/expddefine.scm 284 */
																																			obj_t
																																				BgL_arg1744z00_1731;
																																			BgL_arg1744z00_1731
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_l1100z00_1723));
																																			{
																																				obj_t
																																					BgL_l1100z00_4087;
																																				BgL_l1100z00_4087
																																					=
																																					BgL_arg1744z00_1731;
																																				BgL_l1100z00_1723
																																					=
																																					BgL_l1100z00_4087;
																																				goto
																																					BgL_zc3z04anonymousza31740ze3z87_1725;
																																			}
																																		}
																																}
																														}
																													}
																													BgL_arg1738z00_1719 =
																														CDR(
																														((obj_t)
																															BgL_arg1739z00_1720));
																												}
																												BgL_test2590z00_4068 =
																													PAIRP
																													(BgL_arg1738z00_1719);
																											}
																											if (BgL_test2590z00_4068)
																												{	/* Eval/expddefine.scm 284 */
																													BgL_procz00_1563 =
																														BGl_expandzd2errorzd2zz__expandz00
																														(BgL_funz00_1536,
																														BGl_string2427z00zz__expander_definez00,
																														BgL_xz00_21);
																												}
																											else
																												{	/* Eval/expddefine.scm 288 */
																													obj_t BgL_locz00_1665;

																													BgL_locz00_1665 =
																														BGl_getzd2sourcezd2locationz00zz__readerz00
																														(BgL_xz00_21);
																													{	/* Eval/expddefine.scm 288 */
																														obj_t
																															BgL_argsz00_1666;
																														BgL_argsz00_1666 =
																															BGl_gensymz00zz__r4_symbols_6_4z00
																															(BGl_symbol2428z00zz__expander_definez00);
																														{	/* Eval/expddefine.scm 289 */
																															obj_t
																																BgL_namesz00_1667;
																															BgL_namesz00_1667
																																=
																																BGl_dssslzd2formalszd2ze3namesze3zz__expander_definez00
																																(BgL_formalsz00_1538);
																															{	/* Eval/expddefine.scm 290 */
																																obj_t
																																	BgL_unamesz00_1668;
																																if (NULLP
																																	(BgL_namesz00_1667))
																																	{	/* Eval/expddefine.scm 291 */
																																		BgL_unamesz00_1668
																																			= BNIL;
																																	}
																																else
																																	{	/* Eval/expddefine.scm 291 */
																																		obj_t
																																			BgL_head1106z00_1707;
																																		BgL_head1106z00_1707
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BNIL,
																																			BNIL);
																																		{
																																			obj_t
																																				BgL_l1104z00_1709;
																																			obj_t
																																				BgL_tail1107z00_1710;
																																			BgL_l1104z00_1709
																																				=
																																				BgL_namesz00_1667;
																																			BgL_tail1107z00_1710
																																				=
																																				BgL_head1106z00_1707;
																																		BgL_zc3z04anonymousza31733ze3z87_1711:
																																			if (NULLP
																																				(BgL_l1104z00_1709))
																																				{	/* Eval/expddefine.scm 291 */
																																					BgL_unamesz00_1668
																																						=
																																						CDR
																																						(BgL_head1106z00_1707);
																																				}
																																			else
																																				{	/* Eval/expddefine.scm 291 */
																																					obj_t
																																						BgL_newtail1108z00_1713;
																																					{	/* Eval/expddefine.scm 291 */
																																						obj_t
																																							BgL_arg1736z00_1715;
																																						{	/* Eval/expddefine.scm 291 */
																																							obj_t
																																								BgL_fz00_1716;
																																							BgL_fz00_1716
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_l1104z00_1709));
																																							{	/* Eval/expddefine.scm 291 */
																																								obj_t
																																									BgL_arg1737z00_1717;
																																								BgL_arg1737z00_1717
																																									=
																																									BGl_parsezd2formalzd2identz00zz__evutilsz00
																																									(BgL_fz00_1716,
																																									BgL_locz00_1665);
																																								BgL_arg1736z00_1715
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_arg1737z00_1717));
																																							}
																																						}
																																						BgL_newtail1108z00_1713
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1736z00_1715,
																																							BNIL);
																																					}
																																					SET_CDR
																																						(BgL_tail1107z00_1710,
																																						BgL_newtail1108z00_1713);
																																					{	/* Eval/expddefine.scm 291 */
																																						obj_t
																																							BgL_arg1735z00_1714;
																																						BgL_arg1735z00_1714
																																							=
																																							CDR
																																							(((obj_t) BgL_l1104z00_1709));
																																						{
																																							obj_t
																																								BgL_tail1107z00_4111;
																																							obj_t
																																								BgL_l1104z00_4110;
																																							BgL_l1104z00_4110
																																								=
																																								BgL_arg1735z00_1714;
																																							BgL_tail1107z00_4111
																																								=
																																								BgL_newtail1108z00_1713;
																																							BgL_tail1107z00_1710
																																								=
																																								BgL_tail1107z00_4111;
																																							BgL_l1104z00_1709
																																								=
																																								BgL_l1104z00_4110;
																																							goto
																																								BgL_zc3z04anonymousza31733ze3z87_1711;
																																						}
																																					}
																																				}
																																		}
																																	}
																																{	/* Eval/expddefine.scm 291 */

																																	{	/* Eval/expddefine.scm 292 */
																																		obj_t
																																			BgL_arg1678z00_1669;
																																		{	/* Eval/expddefine.scm 292 */
																																			obj_t
																																				BgL_arg1681z00_1670;
																																			obj_t
																																				BgL_arg1684z00_1671;
																																			BgL_arg1681z00_1670
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_f0z00_1537,
																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																				(BgL_formalsz00_1538,
																																					BNIL));
																																			{	/* Eval/expddefine.scm 293 */
																																				obj_t
																																					BgL_arg1688z00_1673;
																																				{	/* Eval/expddefine.scm 293 */
																																					obj_t
																																						BgL_arg1689z00_1674;
																																					{	/* Eval/expddefine.scm 293 */
																																						obj_t
																																							BgL_arg1691z00_1675;
																																						obj_t
																																							BgL_arg1692z00_1676;
																																						{	/* Eval/expddefine.scm 293 */
																																							obj_t
																																								BgL_arg1699z00_1677;
																																							{	/* Eval/expddefine.scm 293 */
																																								obj_t
																																									BgL_arg1700z00_1678;
																																								{	/* Eval/expddefine.scm 293 */
																																									obj_t
																																										BgL_arg1701z00_1679;
																																									{	/* Eval/expddefine.scm 293 */
																																										obj_t
																																											BgL_arg1702z00_1680;
																																										{	/* Eval/expddefine.scm 293 */
																																											obj_t
																																												BgL_arg1703z00_1681;
																																											obj_t
																																												BgL_arg1704z00_1682;
																																											{	/* Eval/expddefine.scm 293 */
																																												obj_t
																																													BgL_arg1705z00_1683;
																																												{	/* Eval/expddefine.scm 293 */
																																													obj_t
																																														BgL_arg1706z00_1684;
																																													{	/* Eval/expddefine.scm 293 */
																																														obj_t
																																															BgL_pairz00_2858;
																																														BgL_pairz00_2858
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_paz00_1557));
																																														BgL_arg1706z00_1684
																																															=
																																															CAR
																																															(BgL_pairz00_2858);
																																													}
																																													BgL_arg1705z00_1683
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1706z00_1684,
																																														BNIL);
																																												}
																																												BgL_arg1703z00_1681
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2417z00zz__expander_definez00,
																																													BgL_arg1705z00_1683);
																																											}
																																											{	/* Eval/expddefine.scm 294 */
																																												obj_t
																																													BgL_arg1707z00_1685;
																																												{	/* Eval/expddefine.scm 294 */
																																													obj_t
																																														BgL_arg1708z00_1686;
																																													{	/* Eval/expddefine.scm 294 */
																																														obj_t
																																															BgL_arg1709z00_1687;
																																														obj_t
																																															BgL_arg1710z00_1688;
																																														{	/* Eval/expddefine.scm 294 */
																																															obj_t
																																																BgL_pairz00_2862;
																																															BgL_pairz00_2862
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_paz00_1557));
																																															BgL_arg1709z00_1687
																																																=
																																																CAR
																																																(BgL_pairz00_2862);
																																														}
																																														BgL_arg1710z00_1688
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_idz00_1556,
																																															BNIL);
																																														BgL_arg1708z00_1686
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg1709z00_1687,
																																															BgL_arg1710z00_1688);
																																													}
																																													BgL_arg1707z00_1685
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2419z00zz__expander_definez00,
																																														BgL_arg1708z00_1686);
																																												}
																																												BgL_arg1704z00_1682
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1707z00_1685,
																																													BNIL);
																																											}
																																											BgL_arg1702z00_1680
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1703z00_1681,
																																												BgL_arg1704z00_1682);
																																										}
																																										BgL_arg1701z00_1679
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2421z00zz__expander_definez00,
																																											BgL_arg1702z00_1680);
																																									}
																																									BgL_arg1700z00_1678
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1701z00_1679,
																																										BNIL);
																																								}
																																								BgL_arg1699z00_1677
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_metz00_1561,
																																									BgL_arg1700z00_1678);
																																							}
																																							BgL_arg1691z00_1675
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1699z00_1677,
																																								BNIL);
																																						}
																																						{	/* Eval/expddefine.scm 295 */
																																							obj_t
																																								BgL_arg1711z00_1689;
																																							{	/* Eval/expddefine.scm 295 */
																																								obj_t
																																									BgL_arg1714z00_1690;
																																								{	/* Eval/expddefine.scm 295 */
																																									obj_t
																																										BgL_arg1715z00_1691;
																																									obj_t
																																										BgL_arg1717z00_1692;
																																									{	/* Eval/expddefine.scm 295 */
																																										obj_t
																																											BgL_arg1718z00_1693;
																																										BgL_arg1718z00_1693
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_metz00_1561,
																																											BNIL);
																																										BgL_arg1715z00_1691
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2423z00zz__expander_definez00,
																																											BgL_arg1718z00_1693);
																																									}
																																									{	/* Eval/expddefine.scm 296 */
																																										obj_t
																																											BgL_arg1720z00_1694;
																																										obj_t
																																											BgL_arg1722z00_1695;
																																										{	/* Eval/expddefine.scm 296 */
																																											obj_t
																																												BgL_arg1723z00_1696;
																																											{	/* Eval/expddefine.scm 296 */
																																												obj_t
																																													BgL_arg1724z00_1697;
																																												obj_t
																																													BgL_arg1725z00_1698;
																																												{	/* Eval/expddefine.scm 296 */
																																													obj_t
																																														BgL_pairz00_2866;
																																													BgL_pairz00_2866
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_paz00_1557));
																																													BgL_arg1724z00_1697
																																														=
																																														CAR
																																														(BgL_pairz00_2866);
																																												}
																																												BgL_arg1725z00_1698
																																													=
																																													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																													(BgL_unamesz00_1668,
																																													BNIL);
																																												BgL_arg1723z00_1696
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1724z00_1697,
																																													BgL_arg1725z00_1698);
																																											}
																																											BgL_arg1720z00_1694
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_metz00_1561,
																																												BgL_arg1723z00_1696);
																																										}
																																										{	/* Eval/expddefine.scm 297 */
																																											obj_t
																																												BgL_arg1726z00_1699;
																																											{	/* Eval/expddefine.scm 297 */
																																												obj_t
																																													BgL_arg1727z00_1700;
																																												obj_t
																																													BgL_arg1728z00_1701;
																																												{	/* Eval/expddefine.scm 297 */
																																													obj_t
																																														BgL_arg1729z00_1702;
																																													BgL_arg1729z00_1702
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_idz00_1556,
																																														BNIL);
																																													BgL_arg1727z00_1700
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2412z00zz__expander_definez00,
																																														BgL_arg1729z00_1702);
																																												}
																																												{	/* Eval/expddefine.scm 297 */
																																													obj_t
																																														BgL_arg1730z00_1703;
																																													obj_t
																																														BgL_arg1731z00_1704;
																																													{	/* Eval/expddefine.scm 297 */
																																														obj_t
																																															BgL_pairz00_2870;
																																														BgL_pairz00_2870
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_paz00_1557));
																																														BgL_arg1730z00_1703
																																															=
																																															CAR
																																															(BgL_pairz00_2870);
																																													}
																																													BgL_arg1731z00_1704
																																														=
																																														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																														(BgL_unamesz00_1668,
																																														BNIL);
																																													BgL_arg1728z00_1701
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1730z00_1703,
																																														BgL_arg1731z00_1704);
																																												}
																																												BgL_arg1726z00_1699
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1727z00_1700,
																																													BgL_arg1728z00_1701);
																																											}
																																											BgL_arg1722z00_1695
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1726z00_1699,
																																												BNIL);
																																										}
																																										BgL_arg1717z00_1692
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1720z00_1694,
																																											BgL_arg1722z00_1695);
																																									}
																																									BgL_arg1714z00_1690
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1715z00_1691,
																																										BgL_arg1717z00_1692);
																																								}
																																								BgL_arg1711z00_1689
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2425z00zz__expander_definez00,
																																									BgL_arg1714z00_1690);
																																							}
																																							BgL_arg1692z00_1676
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1711z00_1689,
																																								BNIL);
																																						}
																																						BgL_arg1689z00_1674
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1691z00_1675,
																																							BgL_arg1692z00_1676);
																																					}
																																					BgL_arg1688z00_1673
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2407z00zz__expander_definez00,
																																						BgL_arg1689z00_1674);
																																				}
																																				BgL_arg1684z00_1671
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1688z00_1673,
																																					BNIL);
																																			}
																																			BgL_arg1678z00_1669
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1681z00_1670,
																																				BgL_arg1684z00_1671);
																																		}
																																		BgL_procz00_1563
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2400z00zz__expander_definez00,
																																			BgL_arg1678z00_1669);
																																	}
																																}
																															}
																														}
																													}
																												}
																										}
																									else
																										{	/* Eval/expddefine.scm 298 */
																											bool_t
																												BgL_test2595z00_4157;
																											if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_formalsz00_1538))
																												{
																													obj_t
																														BgL_l1109z00_1794;
																													BgL_l1109z00_1794 =
																														BgL_formalsz00_1538;
																												BgL_zc3z04anonymousza31803ze3z87_1795:
																													if (NULLP
																														(BgL_l1109z00_1794))
																														{	/* Eval/expddefine.scm 298 */
																															BgL_test2595z00_4157
																																= ((bool_t) 0);
																														}
																													else
																														{	/* Eval/expddefine.scm 298 */
																															bool_t
																																BgL__ortest_1111z00_1797;
																															{	/* Eval/expddefine.scm 298 */
																																obj_t
																																	BgL_arg1805z00_1799;
																																BgL_arg1805z00_1799
																																	=
																																	CAR(((obj_t)
																																		BgL_l1109z00_1794));
																																BgL__ortest_1111z00_1797
																																	=
																																	BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																																	(BgL_arg1805z00_1799);
																															}
																															if (BgL__ortest_1111z00_1797)
																																{	/* Eval/expddefine.scm 298 */
																																	BgL_test2595z00_4157
																																		=
																																		BgL__ortest_1111z00_1797;
																																}
																															else
																																{	/* Eval/expddefine.scm 298 */
																																	obj_t
																																		BgL_arg1804z00_1798;
																																	BgL_arg1804z00_1798
																																		=
																																		CDR(((obj_t)
																																			BgL_l1109z00_1794));
																																	{
																																		obj_t
																																			BgL_l1109z00_4168;
																																		BgL_l1109z00_4168
																																			=
																																			BgL_arg1804z00_1798;
																																		BgL_l1109z00_1794
																																			=
																																			BgL_l1109z00_4168;
																																		goto
																																			BgL_zc3z04anonymousza31803ze3z87_1795;
																																	}
																																}
																														}
																												}
																											else
																												{	/* Eval/expddefine.scm 298 */
																													BgL_test2595z00_4157 =
																														((bool_t) 0);
																												}
																											if (BgL_test2595z00_4157)
																												{	/* Eval/expddefine.scm 299 */
																													obj_t
																														BgL_argsz00_1744;
																													BgL_argsz00_1744 =
																														BGl_gensymz00zz__r4_symbols_6_4z00
																														(BGl_symbol2428z00zz__expander_definez00);
																													{	/* Eval/expddefine.scm 300 */
																														obj_t
																															BgL_arg1752z00_1745;
																														{	/* Eval/expddefine.scm 300 */
																															obj_t
																																BgL_arg1753z00_1746;
																															obj_t
																																BgL_arg1754z00_1747;
																															BgL_arg1753z00_1746
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_f0z00_1537,
																																BgL_argsz00_1744);
																															{	/* Eval/expddefine.scm 302 */
																																obj_t
																																	BgL_arg1755z00_1748;
																																{	/* Eval/expddefine.scm 302 */
																																	obj_t
																																		BgL_arg1756z00_1749;
																																	{	/* Eval/expddefine.scm 302 */
																																		obj_t
																																			BgL_arg1757z00_1750;
																																		obj_t
																																			BgL_arg1758z00_1751;
																																		{	/* Eval/expddefine.scm 302 */
																																			obj_t
																																				BgL_arg1759z00_1752;
																																			{	/* Eval/expddefine.scm 302 */
																																				obj_t
																																					BgL_arg1760z00_1753;
																																				{	/* Eval/expddefine.scm 302 */
																																					obj_t
																																						BgL_arg1761z00_1754;
																																					{	/* Eval/expddefine.scm 302 */
																																						obj_t
																																							BgL_arg1762z00_1755;
																																						{	/* Eval/expddefine.scm 302 */
																																							obj_t
																																								BgL_arg1763z00_1756;
																																							{	/* Eval/expddefine.scm 302 */
																																								obj_t
																																									BgL_arg1764z00_1757;
																																								{	/* Eval/expddefine.scm 302 */
																																									obj_t
																																										BgL_arg1765z00_1758;
																																									{	/* Eval/expddefine.scm 302 */
																																										obj_t
																																											BgL_arg1766z00_1759;
																																										obj_t
																																											BgL_arg1767z00_1760;
																																										{	/* Eval/expddefine.scm 302 */
																																											obj_t
																																												BgL_arg1768z00_1761;
																																											BgL_arg1768z00_1761
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_idz00_1556,
																																												BNIL);
																																											BgL_arg1766z00_1759
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2412z00zz__expander_definez00,
																																												BgL_arg1768z00_1761);
																																										}
																																										{	/* Eval/expddefine.scm 302 */
																																											obj_t
																																												BgL_arg1769z00_1762;
																																											obj_t
																																												BgL_arg1770z00_1763;
																																											{	/* Eval/expddefine.scm 302 */
																																												obj_t
																																													BgL_pairz00_2876;
																																												BgL_pairz00_2876
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_paz00_1557));
																																												BgL_arg1769z00_1762
																																													=
																																													CAR
																																													(BgL_pairz00_2876);
																																											}
																																											BgL_arg1770z00_1763
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_argsz00_1744,
																																												BNIL);
																																											BgL_arg1767z00_1760
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1769z00_1762,
																																												BgL_arg1770z00_1763);
																																										}
																																										BgL_arg1765z00_1758
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1766z00_1759,
																																											BgL_arg1767z00_1760);
																																									}
																																									BgL_arg1764z00_1757
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2415z00zz__expander_definez00,
																																										BgL_arg1765z00_1758);
																																								}
																																								BgL_arg1763z00_1756
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1764z00_1757,
																																									BNIL);
																																							}
																																							BgL_arg1762z00_1755
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BNIL,
																																								BgL_arg1763z00_1756);
																																						}
																																						BgL_arg1761z00_1754
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2400z00zz__expander_definez00,
																																							BgL_arg1762z00_1755);
																																					}
																																					BgL_arg1760z00_1753
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1761z00_1754,
																																						BNIL);
																																				}
																																				BgL_arg1759z00_1752
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_defz00_1558,
																																					BgL_arg1760z00_1753);
																																			}
																																			BgL_arg1757z00_1750
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1759z00_1752,
																																				BNIL);
																																		}
																																		{	/* Eval/expddefine.scm 303 */
																																			obj_t
																																				BgL_arg1771z00_1764;
																																			{	/* Eval/expddefine.scm 303 */
																																				obj_t
																																					BgL_arg1772z00_1765;
																																				{	/* Eval/expddefine.scm 303 */
																																					obj_t
																																						BgL_arg1773z00_1766;
																																					obj_t
																																						BgL_arg1774z00_1767;
																																					{	/* Eval/expddefine.scm 303 */
																																						obj_t
																																							BgL_arg1775z00_1768;
																																						{	/* Eval/expddefine.scm 303 */
																																							obj_t
																																								BgL_arg1777z00_1769;
																																							{	/* Eval/expddefine.scm 303 */
																																								obj_t
																																									BgL_arg1779z00_1770;
																																								{	/* Eval/expddefine.scm 303 */
																																									obj_t
																																										BgL_arg1781z00_1771;
																																									{	/* Eval/expddefine.scm 303 */
																																										obj_t
																																											BgL_arg1782z00_1772;
																																										obj_t
																																											BgL_arg1783z00_1773;
																																										{	/* Eval/expddefine.scm 303 */
																																											obj_t
																																												BgL_arg1785z00_1774;
																																											{	/* Eval/expddefine.scm 303 */
																																												obj_t
																																													BgL_arg1786z00_1775;
																																												{	/* Eval/expddefine.scm 303 */
																																													obj_t
																																														BgL_pairz00_2880;
																																													BgL_pairz00_2880
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_paz00_1557));
																																													BgL_arg1786z00_1775
																																														=
																																														CAR
																																														(BgL_pairz00_2880);
																																												}
																																												BgL_arg1785z00_1774
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1786z00_1775,
																																													BNIL);
																																											}
																																											BgL_arg1782z00_1772
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2417z00zz__expander_definez00,
																																												BgL_arg1785z00_1774);
																																										}
																																										{	/* Eval/expddefine.scm 304 */
																																											obj_t
																																												BgL_arg1787z00_1776;
																																											{	/* Eval/expddefine.scm 304 */
																																												obj_t
																																													BgL_arg1788z00_1777;
																																												{	/* Eval/expddefine.scm 304 */
																																													obj_t
																																														BgL_arg1789z00_1778;
																																													obj_t
																																														BgL_arg1790z00_1779;
																																													{	/* Eval/expddefine.scm 304 */
																																														obj_t
																																															BgL_pairz00_2884;
																																														BgL_pairz00_2884
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_paz00_1557));
																																														BgL_arg1789z00_1778
																																															=
																																															CAR
																																															(BgL_pairz00_2884);
																																													}
																																													BgL_arg1790z00_1779
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_idz00_1556,
																																														BNIL);
																																													BgL_arg1788z00_1777
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1789z00_1778,
																																														BgL_arg1790z00_1779);
																																												}
																																												BgL_arg1787z00_1776
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2419z00zz__expander_definez00,
																																													BgL_arg1788z00_1777);
																																											}
																																											BgL_arg1783z00_1773
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1787z00_1776,
																																												BNIL);
																																										}
																																										BgL_arg1781z00_1771
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1782z00_1772,
																																											BgL_arg1783z00_1773);
																																									}
																																									BgL_arg1779z00_1770
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2421z00zz__expander_definez00,
																																										BgL_arg1781z00_1771);
																																								}
																																								BgL_arg1777z00_1769
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1779z00_1770,
																																									BNIL);
																																							}
																																							BgL_arg1775z00_1768
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_metz00_1561,
																																								BgL_arg1777z00_1769);
																																						}
																																						BgL_arg1773z00_1766
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1775z00_1768,
																																							BNIL);
																																					}
																																					{	/* Eval/expddefine.scm 305 */
																																						obj_t
																																							BgL_arg1791z00_1780;
																																						{	/* Eval/expddefine.scm 305 */
																																							obj_t
																																								BgL_arg1792z00_1781;
																																							{	/* Eval/expddefine.scm 305 */
																																								obj_t
																																									BgL_arg1793z00_1782;
																																								obj_t
																																									BgL_arg1794z00_1783;
																																								{	/* Eval/expddefine.scm 305 */
																																									obj_t
																																										BgL_arg1795z00_1784;
																																									BgL_arg1795z00_1784
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_metz00_1561,
																																										BNIL);
																																									BgL_arg1793z00_1782
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2423z00zz__expander_definez00,
																																										BgL_arg1795z00_1784);
																																								}
																																								{	/* Eval/expddefine.scm 306 */
																																									obj_t
																																										BgL_arg1796z00_1785;
																																									obj_t
																																										BgL_arg1797z00_1786;
																																									{	/* Eval/expddefine.scm 306 */
																																										obj_t
																																											BgL_arg1798z00_1787;
																																										{	/* Eval/expddefine.scm 306 */
																																											obj_t
																																												BgL_arg1799z00_1788;
																																											{	/* Eval/expddefine.scm 306 */
																																												obj_t
																																													BgL_arg1800z00_1789;
																																												obj_t
																																													BgL_arg1801z00_1790;
																																												{	/* Eval/expddefine.scm 306 */
																																													obj_t
																																														BgL_pairz00_2888;
																																													BgL_pairz00_2888
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_paz00_1557));
																																													BgL_arg1800z00_1789
																																														=
																																														CAR
																																														(BgL_pairz00_2888);
																																												}
																																												BgL_arg1801z00_1790
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_argsz00_1744,
																																													BNIL);
																																												BgL_arg1799z00_1788
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1800z00_1789,
																																													BgL_arg1801z00_1790);
																																											}
																																											BgL_arg1798z00_1787
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_metz00_1561,
																																												BgL_arg1799z00_1788);
																																										}
																																										BgL_arg1796z00_1785
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2415z00zz__expander_definez00,
																																											BgL_arg1798z00_1787);
																																									}
																																									{	/* Eval/expddefine.scm 307 */
																																										obj_t
																																											BgL_arg1802z00_1791;
																																										BgL_arg1802z00_1791
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_defz00_1558,
																																											BNIL);
																																										BgL_arg1797z00_1786
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1802z00_1791,
																																											BNIL);
																																									}
																																									BgL_arg1794z00_1783
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1796z00_1785,
																																										BgL_arg1797z00_1786);
																																								}
																																								BgL_arg1792z00_1781
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1793z00_1782,
																																									BgL_arg1794z00_1783);
																																							}
																																							BgL_arg1791z00_1780
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2425z00zz__expander_definez00,
																																								BgL_arg1792z00_1781);
																																						}
																																						BgL_arg1774z00_1767
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1791z00_1780,
																																							BNIL);
																																					}
																																					BgL_arg1772z00_1765
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1773z00_1766,
																																						BgL_arg1774z00_1767);
																																				}
																																				BgL_arg1771z00_1764
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2407z00zz__expander_definez00,
																																					BgL_arg1772z00_1765);
																																			}
																																			BgL_arg1758z00_1751
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1771z00_1764,
																																				BNIL);
																																		}
																																		BgL_arg1756z00_1749
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1757z00_1750,
																																			BgL_arg1758z00_1751);
																																	}
																																	BgL_arg1755z00_1748
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2407z00zz__expander_definez00,
																																		BgL_arg1756z00_1749);
																																}
																																BgL_arg1754z00_1747
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1755z00_1748,
																																	BNIL);
																															}
																															BgL_arg1752z00_1745
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1753z00_1746,
																																BgL_arg1754z00_1747);
																														}
																														BgL_procz00_1563 =
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2400z00zz__expander_definez00,
																															BgL_arg1752z00_1745);
																													}
																												}
																											else
																												{	/* Eval/expddefine.scm 298 */
																													BgL_procz00_1563 =
																														BGl_expandzd2errorzd2zz__expandz00
																														(BgL_funz00_1536,
																														BGl_string2430z00zz__expander_definez00,
																														BgL_xz00_21);
																												}
																										}
																								}
																						}
																						{	/* Eval/expddefine.scm 267 */

																							{	/* Eval/expddefine.scm 313 */
																								obj_t BgL_arg1556z00_1564;

																								{	/* Eval/expddefine.scm 313 */
																									obj_t BgL_arg1557z00_1565;

																									{	/* Eval/expddefine.scm 313 */
																										obj_t BgL_arg1558z00_1566;
																										obj_t BgL_arg1559z00_1567;

																										{	/* Eval/expddefine.scm 313 */
																											obj_t BgL_arg1561z00_1568;

																											{	/* Eval/expddefine.scm 313 */
																												obj_t
																													BgL_arg1562z00_1569;
																												{	/* Eval/expddefine.scm 313 */
																													obj_t
																														BgL_arg1564z00_1570;
																													{	/* Eval/expddefine.scm 313 */
																														obj_t
																															BgL_arg1565z00_1571;
																														BgL_arg1565z00_1571
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_procz00_1563,
																															BNIL);
																														BgL_arg1564z00_1570
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2431z00zz__expander_definez00,
																															BgL_arg1565z00_1571);
																													}
																													BgL_arg1562z00_1569 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1564z00_1570,
																														BNIL);
																												}
																												BgL_arg1561z00_1568 =
																													MAKE_YOUNG_PAIR
																													(BgL_funz00_1536,
																													BgL_arg1562z00_1569);
																											}
																											BgL_arg1558z00_1566 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2403z00zz__expander_definez00,
																												BgL_arg1561z00_1568);
																										}
																										{	/* Eval/expddefine.scm 316 */
																											obj_t BgL_arg1567z00_1572;

																											{	/* Eval/expddefine.scm 316 */
																												obj_t
																													BgL_arg1571z00_1573;
																												{	/* Eval/expddefine.scm 316 */
																													obj_t
																														BgL_arg1573z00_1574;
																													{	/* Eval/expddefine.scm 316 */
																														obj_t
																															BgL_arg1575z00_1575;
																														obj_t
																															BgL_arg1576z00_1576;
																														{	/* Eval/expddefine.scm 316 */
																															obj_t
																																BgL_arg1578z00_1577;
																															{	/* Eval/expddefine.scm 316 */
																																obj_t
																																	BgL_arg1579z00_1578;
																																obj_t
																																	BgL_arg1580z00_1579;
																																{	/* Eval/expddefine.scm 316 */
																																	obj_t
																																		BgL_arg1582z00_1580;
																																	if (CBOOL
																																		(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																			((BOPTIONAL), BgL_formalsz00_1538)))
																																		{	/* Eval/expddefine.scm 316 */
																																			BgL_arg1582z00_1580
																																				=
																																				BGl_dssslzd2formalszd2ze3namesze3zz__expander_definez00
																																				(BgL_formalsz00_1538);
																																		}
																																	else
																																		{	/* Eval/expddefine.scm 316 */
																																			BgL_arg1582z00_1580
																																				=
																																				BgL_formalsz00_1538;
																																		}
																																	BgL_arg1579z00_1578
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_f0z00_1537,
																																		BgL_arg1582z00_1580);
																																}
																																{	/* Eval/expddefine.scm 319 */
																																	obj_t
																																		BgL_arg1584z00_1582;
																																	if (PAIRP
																																		(BgL_bodyz00_1539))
																																		{	/* Eval/expddefine.scm 320 */
																																			obj_t
																																				BgL_arg1586z00_1584;
																																			BgL_arg1586z00_1584
																																				=
																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																				(BgL_bodyz00_1539,
																																				BNIL);
																																			BgL_arg1584z00_1582
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2395z00zz__expander_definez00,
																																				BgL_arg1586z00_1584);
																																		}
																																	else
																																		{	/* Eval/expddefine.scm 321 */
																																			obj_t
																																				BgL_arg1587z00_1585;
																																			{	/* Eval/expddefine.scm 321 */
																																				obj_t
																																					BgL_arg1589z00_1586;
																																				obj_t
																																					BgL_arg1591z00_1587;
																																				{	/* Eval/expddefine.scm 321 */
																																					obj_t
																																						BgL_arg1593z00_1588;
																																					BgL_arg1593z00_1588
																																						=
																																						CAR(
																																						((obj_t) BgL_pfz00_1555));
																																					{	/* Eval/expddefine.scm 321 */
																																						obj_t
																																							BgL_arg2186z00_2891;
																																						BgL_arg2186z00_2891
																																							=
																																							SYMBOL_TO_STRING
																																							(((obj_t) BgL_arg1593z00_1588));
																																						BgL_arg1589z00_1586
																																							=
																																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																							(BgL_arg2186z00_2891);
																																					}
																																				}
																																				{	/* Eval/expddefine.scm 323 */
																																					obj_t
																																						BgL_arg1594z00_1589;
																																					{	/* Eval/expddefine.scm 323 */
																																						obj_t
																																							BgL_arg1595z00_1590;
																																						{	/* Eval/expddefine.scm 323 */
																																							obj_t
																																								BgL_arg1598z00_1591;
																																							{	/* Eval/expddefine.scm 323 */
																																								obj_t
																																									BgL_arg1601z00_1592;
																																								{	/* Eval/expddefine.scm 323 */
																																									obj_t
																																										BgL_pairz00_2893;
																																									BgL_pairz00_2893
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_paz00_1557));
																																									BgL_arg1601z00_1592
																																										=
																																										CAR
																																										(BgL_pairz00_2893);
																																								}
																																								BgL_arg1598z00_1591
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1601z00_1592,
																																									BNIL);
																																							}
																																							BgL_arg1595z00_1590
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2433z00zz__expander_definez00,
																																								BgL_arg1598z00_1591);
																																						}
																																						BgL_arg1594z00_1589
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1595z00_1590,
																																							BNIL);
																																					}
																																					BgL_arg1591z00_1587
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_string2435z00zz__expander_definez00,
																																						BgL_arg1594z00_1589);
																																				}
																																				BgL_arg1587z00_1585
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1589z00_1586,
																																					BgL_arg1591z00_1587);
																																			}
																																			BgL_arg1584z00_1582
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2436z00zz__expander_definez00,
																																				BgL_arg1587z00_1585);
																																		}
																																	BgL_arg1580z00_1579
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1584z00_1582,
																																		BNIL);
																																}
																																BgL_arg1578z00_1577
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1579z00_1578,
																																	BgL_arg1580z00_1579);
																															}
																															BgL_arg1575z00_1575
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2400z00zz__expander_definez00,
																																BgL_arg1578z00_1577);
																														}
																														{	/* Eval/expddefine.scm 325 */
																															obj_t
																																BgL_arg1603z00_1594;
																															{	/* Eval/expddefine.scm 325 */
																																obj_t
																																	BgL_arg1605z00_1595;
																																{	/* Eval/expddefine.scm 325 */
																																	obj_t
																																		BgL_arg2186z00_2895;
																																	BgL_arg2186z00_2895
																																		=
																																		SYMBOL_TO_STRING
																																		(((obj_t)
																																			BgL_idz00_1556));
																																	BgL_arg1605z00_1595
																																		=
																																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																		(BgL_arg2186z00_2895);
																																}
																																BgL_arg1603z00_1594
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1605z00_1595,
																																	BNIL);
																															}
																															BgL_arg1576z00_1576
																																=
																																MAKE_YOUNG_PAIR
																																(BFALSE,
																																BgL_arg1603z00_1594);
																														}
																														BgL_arg1573z00_1574
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1575z00_1575,
																															BgL_arg1576z00_1576);
																													}
																													BgL_arg1571z00_1573 =
																														MAKE_YOUNG_PAIR
																														(BgL_idz00_1556,
																														BgL_arg1573z00_1574);
																												}
																												BgL_arg1567z00_1572 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2438z00zz__expander_definez00,
																													BgL_arg1571z00_1573);
																											}
																											BgL_arg1559z00_1567 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1567z00_1572,
																												BNIL);
																										}
																										BgL_arg1557z00_1565 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1558z00_1566,
																											BgL_arg1559z00_1567);
																									}
																									BgL_arg1556z00_1564 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2395z00zz__expander_definez00,
																										BgL_arg1557z00_1565);
																								}
																								return
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_22,
																									BgL_arg1556z00_1564,
																									BgL_ez00_22);
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										else
											{	/* Eval/expddefine.scm 251 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string2440z00zz__expander_definez00,
													BGl_string2399z00zz__expander_definez00, BgL_xz00_21);
											}
									}
								else
									{	/* Eval/expddefine.scm 251 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2440z00zz__expander_definez00,
											BGl_string2399z00zz__expander_definez00, BgL_xz00_21);
									}
							}
						else
							{	/* Eval/expddefine.scm 251 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string2440z00zz__expander_definez00,
									BGl_string2399z00zz__expander_definez00, BgL_xz00_21);
							}
					}
				else
					{	/* Eval/expddefine.scm 251 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string2440z00zz__expander_definez00,
							BGl_string2399z00zz__expander_definez00, BgL_xz00_21);
					}
			}
		}

	}



/* &expand-eval-define-generic */
	obj_t BGl_z62expandzd2evalzd2definezd2genericzb0zz__expander_definez00(obj_t
		BgL_envz00_3174, obj_t BgL_xz00_3175, obj_t BgL_ez00_3176)
	{
		{	/* Eval/expddefine.scm 250 */
			{	/* Eval/expddefine.scm 251 */
				obj_t BgL_auxz00_4282;

				if (PROCEDUREP(BgL_ez00_3176))
					{	/* Eval/expddefine.scm 251 */
						BgL_auxz00_4282 = BgL_ez00_3176;
					}
				else
					{
						obj_t BgL_auxz00_4285;

						BgL_auxz00_4285 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2392z00zz__expander_definez00, BINT(9224L),
							BGl_string2441z00zz__expander_definez00,
							BGl_string2394z00zz__expander_definez00, BgL_ez00_3176);
						FAILURE(BgL_auxz00_4285, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2evalzd2definezd2genericzd2zz__expander_definez00
					(BgL_xz00_3175, BgL_auxz00_4282);
			}
		}

	}



/* &<@anonymous:1657> */
	obj_t BGl_z62zc3z04anonymousza31657ze3ze5zz__expander_definez00(obj_t
		BgL_envz00_3177, obj_t BgL_az00_3178)
	{
		{	/* Eval/expddefine.scm 270 */
			if (PAIRP(BgL_az00_3178))
				{	/* Eval/expddefine.scm 271 */
					return CAR(BgL_az00_3178);
				}
			else
				{	/* Eval/expddefine.scm 271 */
					return BgL_az00_3178;
				}
		}

	}



/* &<@anonymous:1811> */
	obj_t BGl_z62zc3z04anonymousza31811ze3ze5zz__expander_definez00(obj_t
		BgL_envz00_3179, obj_t BgL_az00_3180)
	{
		{	/* Eval/expddefine.scm 264 */
			if (PAIRP(BgL_az00_3180))
				{	/* Eval/expddefine.scm 265 */
					return CAR(BgL_az00_3180);
				}
			else
				{	/* Eval/expddefine.scm 265 */
					return BgL_az00_3180;
				}
		}

	}



/* &<@anonymous:1821> */
	obj_t BGl_z62zc3z04anonymousza31821ze3ze5zz__expander_definez00(obj_t
		BgL_envz00_3181, obj_t BgL_iz00_3183)
	{
		{	/* Eval/expddefine.scm 256 */
			return
				BGl_parsezd2formalzd2identz00zz__evutilsz00(BgL_iz00_3183,
				PROCEDURE_L_REF(BgL_envz00_3181, (int) (0L)));
		}

	}



/* expand-eval-define-method */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2definezd2methodzd2zz__expander_definez00(obj_t
		BgL_xz00_23, obj_t BgL_ez00_24)
	{
		{	/* Eval/expddefine.scm 333 */
			{
				obj_t BgL_funz00_1827;
				obj_t BgL_f0z00_1828;
				obj_t BgL_formalsz00_1829;
				obj_t BgL_bodyz00_1830;

				if (PAIRP(BgL_xz00_23))
					{	/* Eval/expddefine.scm 357 */
						obj_t BgL_cdrzd2479zd2_1835;

						BgL_cdrzd2479zd2_1835 = CDR(((obj_t) BgL_xz00_23));
						if (PAIRP(BgL_cdrzd2479zd2_1835))
							{	/* Eval/expddefine.scm 357 */
								obj_t BgL_carzd2484zd2_1837;
								obj_t BgL_cdrzd2485zd2_1838;

								BgL_carzd2484zd2_1837 = CAR(BgL_cdrzd2479zd2_1835);
								BgL_cdrzd2485zd2_1838 = CDR(BgL_cdrzd2479zd2_1835);
								if (PAIRP(BgL_carzd2484zd2_1837))
									{	/* Eval/expddefine.scm 357 */
										obj_t BgL_cdrzd2490zd2_1840;

										BgL_cdrzd2490zd2_1840 = CDR(BgL_carzd2484zd2_1837);
										if (PAIRP(BgL_cdrzd2490zd2_1840))
											{	/* Eval/expddefine.scm 357 */
												if (NULLP(BgL_cdrzd2485zd2_1838))
													{	/* Eval/expddefine.scm 357 */
														return
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2450z00zz__expander_definez00,
															BGl_string2399z00zz__expander_definez00,
															BgL_xz00_23);
													}
												else
													{	/* Eval/expddefine.scm 357 */
														BgL_funz00_1827 = CAR(BgL_carzd2484zd2_1837);
														BgL_f0z00_1828 = CAR(BgL_cdrzd2490zd2_1840);
														BgL_formalsz00_1829 = CDR(BgL_cdrzd2490zd2_1840);
														BgL_bodyz00_1830 = BgL_cdrzd2485zd2_1838;
														{	/* Eval/expddefine.scm 359 */
															obj_t BgL_locz00_1846;

															BgL_locz00_1846 =
																BGl_getzd2sourcezd2locationz00zz__readerz00
																(BgL_xz00_23);
															{	/* Eval/expddefine.scm 359 */
																obj_t BgL_pfz00_1847;

																BgL_pfz00_1847 =
																	BGl_parsezd2formalzd2identz00zz__evutilsz00
																	(BgL_funz00_1827, BgL_locz00_1846);
																{	/* Eval/expddefine.scm 360 */
																	obj_t BgL_p0z00_1848;

																	BgL_p0z00_1848 =
																		BGl_parsezd2formalzd2identz00zz__evutilsz00
																		(BgL_f0z00_1828, BgL_locz00_1846);
																	{	/* Eval/expddefine.scm 361 */
																		obj_t BgL_restz00_1849;

																		BgL_restz00_1849 =
																			BGl_getzd2argsze70z35zz__expander_definez00
																			(BgL_formalsz00_1829, BgL_locz00_1846);
																		{	/* Eval/expddefine.scm 362 */
																			bool_t BgL_vaz00_1850;

																			if (NULLP(BgL_formalsz00_1829))
																				{	/* Eval/expddefine.scm 363 */
																					BgL_vaz00_1850 = ((bool_t) 0);
																				}
																			else
																				{	/* Eval/expddefine.scm 364 */
																					bool_t BgL__ortest_1047z00_2053;

																					if (PAIRP(BgL_formalsz00_1829))
																						{	/* Eval/expddefine.scm 364 */
																							BgL__ortest_1047z00_2053 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Eval/expddefine.scm 364 */
																							BgL__ortest_1047z00_2053 =
																								((bool_t) 1);
																						}
																					if (BgL__ortest_1047z00_2053)
																						{	/* Eval/expddefine.scm 364 */
																							BgL_vaz00_1850 =
																								BgL__ortest_1047z00_2053;
																						}
																					else
																						{	/* Eval/expddefine.scm 364 */
																							if (NULLP(CDR
																									(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_formalsz00_1829))))
																								{	/* Eval/expddefine.scm 365 */
																									BgL_vaz00_1850 = ((bool_t) 0);
																								}
																							else
																								{	/* Eval/expddefine.scm 365 */
																									BgL_vaz00_1850 = ((bool_t) 1);
																								}
																						}
																				}
																			{	/* Eval/expddefine.scm 363 */

																				{	/* Eval/expddefine.scm 366 */
																					bool_t BgL_test2613z00_4328;

																					if (PAIRP(BgL_p0z00_1848))
																						{	/* Eval/expddefine.scm 366 */
																							obj_t BgL_tmpz00_4331;

																							BgL_tmpz00_4331 =
																								CDR(BgL_p0z00_1848);
																							BgL_test2613z00_4328 =
																								SYMBOLP(BgL_tmpz00_4331);
																						}
																					else
																						{	/* Eval/expddefine.scm 366 */
																							BgL_test2613z00_4328 =
																								((bool_t) 0);
																						}
																					if (BgL_test2613z00_4328)
																						{	/* Eval/expddefine.scm 368 */
																							bool_t BgL_test2615z00_4334;

																							if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_formalsz00_1829))
																								{	/* Eval/expddefine.scm 368 */
																									bool_t BgL_test2617z00_4337;

																									{
																										obj_t BgL_l1112z00_2043;

																										BgL_l1112z00_2043 =
																											BgL_formalsz00_1829;
																									BgL_zc3z04anonymousza32020ze3z87_2044:
																										if (NULLP
																											(BgL_l1112z00_2043))
																											{	/* Eval/expddefine.scm 368 */
																												BgL_test2617z00_4337 =
																													((bool_t) 0);
																											}
																										else
																											{	/* Eval/expddefine.scm 368 */
																												bool_t
																													BgL__ortest_1114z00_2046;
																												{	/* Eval/expddefine.scm 368 */
																													obj_t
																														BgL_arg2022z00_2048;
																													BgL_arg2022z00_2048 =
																														CAR(((obj_t)
																															BgL_l1112z00_2043));
																													BgL__ortest_1114z00_2046
																														=
																														BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																														(BgL_arg2022z00_2048);
																												}
																												if (BgL__ortest_1114z00_2046)
																													{	/* Eval/expddefine.scm 368 */
																														BgL_test2617z00_4337
																															=
																															BgL__ortest_1114z00_2046;
																													}
																												else
																													{	/* Eval/expddefine.scm 368 */
																														obj_t
																															BgL_arg2021z00_2047;
																														BgL_arg2021z00_2047
																															=
																															CDR(((obj_t)
																																BgL_l1112z00_2043));
																														{
																															obj_t
																																BgL_l1112z00_4346;
																															BgL_l1112z00_4346
																																=
																																BgL_arg2021z00_2047;
																															BgL_l1112z00_2043
																																=
																																BgL_l1112z00_4346;
																															goto
																																BgL_zc3z04anonymousza32020ze3z87_2044;
																														}
																													}
																											}
																									}
																									if (BgL_test2617z00_4337)
																										{	/* Eval/expddefine.scm 368 */
																											BgL_test2615z00_4334 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Eval/expddefine.scm 368 */
																											BgL_test2615z00_4334 =
																												((bool_t) 1);
																										}
																								}
																							else
																								{	/* Eval/expddefine.scm 368 */
																									BgL_test2615z00_4334 =
																										((bool_t) 1);
																								}
																							if (BgL_test2615z00_4334)
																								{	/* Eval/expddefine.scm 369 */
																									obj_t BgL_resz00_1873;

																									{	/* Eval/expddefine.scm 370 */
																										obj_t BgL_arg1844z00_1874;

																										{	/* Eval/expddefine.scm 370 */
																											obj_t BgL_arg1845z00_1875;
																											obj_t BgL_arg1846z00_1876;

																											BgL_arg1845z00_1875 =
																												CAR(
																												((obj_t)
																													BgL_pfz00_1847));
																											{	/* Eval/expddefine.scm 371 */
																												obj_t
																													BgL_arg1847z00_1877;
																												obj_t
																													BgL_arg1848z00_1878;
																												BgL_arg1847z00_1877 =
																													CDR(BgL_p0z00_1848);
																												{	/* Eval/expddefine.scm 372 */
																													obj_t
																														BgL_arg1849z00_1879;
																													obj_t
																														BgL_arg1850z00_1880;
																													{	/* Eval/expddefine.scm 372 */
																														obj_t
																															BgL_arg1851z00_1881;
																														{	/* Eval/expddefine.scm 372 */
																															obj_t
																																BgL_arg1852z00_1882;
																															{	/* Eval/expddefine.scm 372 */
																																obj_t
																																	BgL_arg1853z00_1883;
																																obj_t
																																	BgL_arg1854z00_1884;
																																{	/* Eval/expddefine.scm 372 */
																																	obj_t
																																		BgL_arg1856z00_1885;
																																	BgL_arg1856z00_1885
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_f0z00_1828,
																																		BgL_formalsz00_1829);
																																	BgL_arg1853z00_1883
																																		=
																																		BGl_loopze70ze7zz__expander_definez00
																																		(BgL_ez00_24,
																																		BgL_arg1856z00_1885);
																																}
																																{	/* Eval/expddefine.scm 373 */
																																	obj_t
																																		BgL_arg1857z00_1886;
																																	obj_t
																																		BgL_arg1858z00_1887;
																																	{	/* Eval/expddefine.scm 373 */
																																		obj_t
																																			BgL_arg1859z00_1888;
																																		{	/* Eval/expddefine.scm 373 */
																																			obj_t
																																				BgL_arg1860z00_1889;
																																			obj_t
																																				BgL_arg1862z00_1890;
																																			BgL_arg1860z00_1889
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2442z00zz__expander_definez00,
																																				BNIL);
																																			{	/* Eval/expddefine.scm 375 */
																																				obj_t
																																					BgL_arg1863z00_1891;
																																				{	/* Eval/expddefine.scm 375 */
																																					obj_t
																																						BgL_arg1864z00_1892;
																																					{	/* Eval/expddefine.scm 375 */
																																						obj_t
																																							BgL_arg1866z00_1893;
																																						obj_t
																																							BgL_arg1868z00_1894;
																																						{	/* Eval/expddefine.scm 375 */
																																							obj_t
																																								BgL_arg1869z00_1895;
																																							{	/* Eval/expddefine.scm 375 */
																																								obj_t
																																									BgL_arg1870z00_1896;
																																								{	/* Eval/expddefine.scm 375 */
																																									obj_t
																																										BgL_arg1872z00_1897;
																																									{	/* Eval/expddefine.scm 375 */
																																										obj_t
																																											BgL_arg1873z00_1898;
																																										{	/* Eval/expddefine.scm 375 */
																																											obj_t
																																												BgL_arg1874z00_1899;
																																											obj_t
																																												BgL_arg1875z00_1900;
																																											BgL_arg1874z00_1899
																																												=
																																												CAR
																																												(BgL_p0z00_1848);
																																											{	/* Eval/expddefine.scm 376 */
																																												obj_t
																																													BgL_arg1876z00_1901;
																																												obj_t
																																													BgL_arg1877z00_1902;
																																												BgL_arg1876z00_1901
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_pfz00_1847));
																																												BgL_arg1877z00_1902
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CDR
																																													(BgL_p0z00_1848),
																																													BNIL);
																																												BgL_arg1875z00_1900
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1876z00_1901,
																																													BgL_arg1877z00_1902);
																																											}
																																											BgL_arg1873z00_1898
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1874z00_1899,
																																												BgL_arg1875z00_1900);
																																										}
																																										BgL_arg1872z00_1897
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2444z00zz__expander_definez00,
																																											BgL_arg1873z00_1898);
																																									}
																																									BgL_arg1870z00_1896
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1872z00_1897,
																																										BNIL);
																																								}
																																								BgL_arg1869z00_1895
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2446z00zz__expander_definez00,
																																									BgL_arg1870z00_1896);
																																							}
																																							BgL_arg1866z00_1893
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1869z00_1895,
																																								BNIL);
																																						}
																																						{	/* Eval/expddefine.scm 378 */
																																							obj_t
																																								BgL_arg1879z00_1904;
																																							{	/* Eval/expddefine.scm 378 */
																																								obj_t
																																									BgL_arg1880z00_1905;
																																								{	/* Eval/expddefine.scm 378 */
																																									obj_t
																																										BgL_arg1882z00_1906;
																																									obj_t
																																										BgL_arg1883z00_1907;
																																									{	/* Eval/expddefine.scm 378 */
																																										obj_t
																																											BgL_arg1884z00_1908;
																																										BgL_arg1884z00_1908
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2446z00zz__expander_definez00,
																																											BNIL);
																																										BgL_arg1882z00_1906
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2423z00zz__expander_definez00,
																																											BgL_arg1884z00_1908);
																																									}
																																									{	/* Eval/expddefine.scm 379 */
																																										obj_t
																																											BgL_arg1885z00_1909;
																																										obj_t
																																											BgL_arg1887z00_1910;
																																										if (BgL_vaz00_1850)
																																											{	/* Eval/expddefine.scm 380 */
																																												obj_t
																																													BgL_arg1888z00_1911;
																																												{	/* Eval/expddefine.scm 380 */
																																													obj_t
																																														BgL_arg1889z00_1912;
																																													BgL_arg1889z00_1912
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CAR
																																														(BgL_p0z00_1848),
																																														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																														(BgL_restz00_1849,
																																															BNIL));
																																													BgL_arg1888z00_1911
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2446z00zz__expander_definez00,
																																														BgL_arg1889z00_1912);
																																												}
																																												BgL_arg1885z00_1909
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2415z00zz__expander_definez00,
																																													BgL_arg1888z00_1911);
																																											}
																																										else
																																											{	/* Eval/expddefine.scm 381 */
																																												obj_t
																																													BgL_arg1892z00_1915;
																																												BgL_arg1892z00_1915
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CAR
																																													(BgL_p0z00_1848),
																																													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																													(BgL_restz00_1849,
																																														BNIL));
																																												BgL_arg1885z00_1909
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2446z00zz__expander_definez00,
																																													BgL_arg1892z00_1915);
																																											}
																																										{	/* Eval/expddefine.scm 382 */
																																											obj_t
																																												BgL_arg1896z00_1918;
																																											if (BgL_vaz00_1850)
																																												{	/* Eval/expddefine.scm 383 */
																																													obj_t
																																														BgL_arg1897z00_1919;
																																													{	/* Eval/expddefine.scm 383 */
																																														obj_t
																																															BgL_arg1898z00_1920;
																																														obj_t
																																															BgL_arg1899z00_1921;
																																														BgL_arg1898z00_1920
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_pfz00_1847));
																																														BgL_arg1899z00_1921
																																															=
																																															MAKE_YOUNG_PAIR
																																															(CAR
																																															(BgL_p0z00_1848),
																																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																															(BgL_restz00_1849,
																																																BNIL));
																																														BgL_arg1897z00_1919
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg1898z00_1920,
																																															BgL_arg1899z00_1921);
																																													}
																																													BgL_arg1896z00_1918
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2415z00zz__expander_definez00,
																																														BgL_arg1897z00_1919);
																																												}
																																											else
																																												{	/* Eval/expddefine.scm 384 */
																																													obj_t
																																														BgL_arg1903z00_1924;
																																													obj_t
																																														BgL_arg1904z00_1925;
																																													BgL_arg1903z00_1924
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_pfz00_1847));
																																													BgL_arg1904z00_1925
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CAR
																																														(BgL_p0z00_1848),
																																														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																														(BgL_restz00_1849,
																																															BNIL));
																																													BgL_arg1896z00_1918
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1903z00_1924,
																																														BgL_arg1904z00_1925);
																																												}
																																											BgL_arg1887z00_1910
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1896z00_1918,
																																												BNIL);
																																										}
																																										BgL_arg1883z00_1907
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1885z00_1909,
																																											BgL_arg1887z00_1910);
																																									}
																																									BgL_arg1880z00_1905
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1882z00_1906,
																																										BgL_arg1883z00_1907);
																																								}
																																								BgL_arg1879z00_1904
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2425z00zz__expander_definez00,
																																									BgL_arg1880z00_1905);
																																							}
																																							BgL_arg1868z00_1894
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1879z00_1904,
																																								BNIL);
																																						}
																																						BgL_arg1864z00_1892
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1866z00_1893,
																																							BgL_arg1868z00_1894);
																																					}
																																					BgL_arg1863z00_1891
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2407z00zz__expander_definez00,
																																						BgL_arg1864z00_1892);
																																				}
																																				BgL_arg1862z00_1890
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1863z00_1891,
																																					BNIL);
																																			}
																																			BgL_arg1859z00_1888
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1860z00_1889,
																																				BgL_arg1862z00_1890);
																																		}
																																		BgL_arg1857z00_1886
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2403z00zz__expander_definez00,
																																			BgL_arg1859z00_1888);
																																	}
																																	BgL_arg1858z00_1887
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_bodyz00_1830,
																																		BNIL);
																																	BgL_arg1854z00_1884
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1857z00_1886,
																																		BgL_arg1858z00_1887);
																																}
																																BgL_arg1852z00_1882
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1853z00_1883,
																																	BgL_arg1854z00_1884);
																															}
																															BgL_arg1851z00_1881
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2400z00zz__expander_definez00,
																																BgL_arg1852z00_1882);
																														}
																														BgL_arg1849z00_1879
																															=
																															BGL_PROCEDURE_CALL2
																															(BgL_ez00_24,
																															BgL_arg1851z00_1881,
																															BgL_ez00_24);
																													}
																													{	/* Eval/expddefine.scm 386 */
																														obj_t
																															BgL_arg1911z00_1928;
																														{	/* Eval/expddefine.scm 386 */
																															obj_t
																																BgL_arg1912z00_1929;
																															BgL_arg1912z00_1929
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_f0z00_1828,
																																BNIL);
																															BgL_arg1911z00_1928
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2433z00zz__expander_definez00,
																																BgL_arg1912z00_1929);
																														}
																														BgL_arg1850z00_1880
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1911z00_1928,
																															BNIL);
																													}
																													BgL_arg1848z00_1878 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1849z00_1879,
																														BgL_arg1850z00_1880);
																												}
																												BgL_arg1846z00_1876 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1847z00_1877,
																													BgL_arg1848z00_1878);
																											}
																											BgL_arg1844z00_1874 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1845z00_1875,
																												BgL_arg1846z00_1876);
																										}
																										BgL_resz00_1873 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2448z00zz__expander_definez00,
																											BgL_arg1844z00_1874);
																									}
																									{	/* Eval/expddefine.scm 369 */

																										return
																											BGl_evepairifyz00zz__prognz00
																											(BgL_resz00_1873,
																											BgL_xz00_23);
																									}
																								}
																							else
																								{	/* Eval/expddefine.scm 368 */
																									if (CBOOL
																										(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																											((BOPTIONAL),
																												BgL_formalsz00_1829)))
																										{	/* Eval/expddefine.scm 389 */
																											obj_t
																												BgL_tformalsz00_1931;
																											BgL_tformalsz00_1931 =
																												BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
																												(BgL_formalsz00_1829,
																												BGl_errorzd2envzd2zz__errorz00,
																												((bool_t) 1));
																											{	/* Eval/expddefine.scm 389 */
																												obj_t
																													BgL_uformalsz00_1932;
																												BgL_uformalsz00_1932 =
																													BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
																													(BgL_formalsz00_1829,
																													BGl_errorzd2envzd2zz__errorz00,
																													((bool_t) 0));
																												{	/* Eval/expddefine.scm 390 */
																													obj_t
																														BgL_namesz00_1933;
																													BgL_namesz00_1933 =
																														BGl_dssslzd2formalszd2ze3namesze3zz__expander_definez00
																														(BgL_formalsz00_1829);
																													{	/* Eval/expddefine.scm 391 */
																														obj_t
																															BgL_resz00_1934;
																														{	/* Eval/expddefine.scm 393 */
																															obj_t
																																BgL_arg1914z00_1935;
																															{	/* Eval/expddefine.scm 393 */
																																obj_t
																																	BgL_arg1916z00_1936;
																																obj_t
																																	BgL_arg1917z00_1937;
																																BgL_arg1916z00_1936
																																	=
																																	CAR(((obj_t)
																																		BgL_pfz00_1847));
																																{	/* Eval/expddefine.scm 394 */
																																	obj_t
																																		BgL_arg1918z00_1938;
																																	obj_t
																																		BgL_arg1919z00_1939;
																																	BgL_arg1918z00_1938
																																		=
																																		CDR
																																		(BgL_p0z00_1848);
																																	{	/* Eval/expddefine.scm 395 */
																																		obj_t
																																			BgL_arg1920z00_1940;
																																		obj_t
																																			BgL_arg1923z00_1941;
																																		{	/* Eval/expddefine.scm 395 */
																																			obj_t
																																				BgL_arg1924z00_1942;
																																			{	/* Eval/expddefine.scm 395 */
																																				obj_t
																																					BgL_arg1925z00_1943;
																																				{	/* Eval/expddefine.scm 395 */
																																					obj_t
																																						BgL_arg1926z00_1944;
																																					obj_t
																																						BgL_arg1927z00_1945;
																																					{	/* Eval/expddefine.scm 395 */
																																						obj_t
																																							BgL_arg1928z00_1946;
																																						BgL_arg1928z00_1946
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_f0z00_1828,
																																							BgL_namesz00_1933);
																																						BgL_arg1926z00_1944
																																							=
																																							BGl_loopze70ze7zz__expander_definez00
																																							(BgL_ez00_24,
																																							BgL_arg1928z00_1946);
																																					}
																																					{	/* Eval/expddefine.scm 396 */
																																						obj_t
																																							BgL_arg1929z00_1947;
																																						obj_t
																																							BgL_arg1930z00_1948;
																																						{	/* Eval/expddefine.scm 396 */
																																							obj_t
																																								BgL_arg1931z00_1949;
																																							{	/* Eval/expddefine.scm 396 */
																																								obj_t
																																									BgL_arg1932z00_1950;
																																								obj_t
																																									BgL_arg1933z00_1951;
																																								BgL_arg1932z00_1950
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2442z00zz__expander_definez00,
																																									BNIL);
																																								{	/* Eval/expddefine.scm 397 */
																																									obj_t
																																										BgL_arg1934z00_1952;
																																									{	/* Eval/expddefine.scm 397 */
																																										obj_t
																																											BgL_arg1935z00_1953;
																																										{	/* Eval/expddefine.scm 397 */
																																											obj_t
																																												BgL_arg1936z00_1954;
																																											obj_t
																																												BgL_arg1937z00_1955;
																																											{	/* Eval/expddefine.scm 397 */
																																												obj_t
																																													BgL_arg1938z00_1956;
																																												{	/* Eval/expddefine.scm 397 */
																																													obj_t
																																														BgL_arg1939z00_1957;
																																													{	/* Eval/expddefine.scm 397 */
																																														obj_t
																																															BgL_arg1940z00_1958;
																																														{	/* Eval/expddefine.scm 397 */
																																															obj_t
																																																BgL_arg1941z00_1959;
																																															{	/* Eval/expddefine.scm 397 */
																																																obj_t
																																																	BgL_arg1942z00_1960;
																																																obj_t
																																																	BgL_arg1943z00_1961;
																																																BgL_arg1942z00_1960
																																																	=
																																																	CAR
																																																	(BgL_p0z00_1848);
																																																{	/* Eval/expddefine.scm 398 */
																																																	obj_t
																																																		BgL_arg1944z00_1962;
																																																	obj_t
																																																		BgL_arg1945z00_1963;
																																																	BgL_arg1944z00_1962
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_pfz00_1847));
																																																	BgL_arg1945z00_1963
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(CDR
																																																		(BgL_p0z00_1848),
																																																		BNIL);
																																																	BgL_arg1943z00_1961
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg1944z00_1962,
																																																		BgL_arg1945z00_1963);
																																																}
																																																BgL_arg1941z00_1959
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1942z00_1960,
																																																	BgL_arg1943z00_1961);
																																															}
																																															BgL_arg1940z00_1958
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_symbol2444z00zz__expander_definez00,
																																																BgL_arg1941z00_1959);
																																														}
																																														BgL_arg1939z00_1957
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg1940z00_1958,
																																															BNIL);
																																													}
																																													BgL_arg1938z00_1956
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2446z00zz__expander_definez00,
																																														BgL_arg1939z00_1957);
																																												}
																																												BgL_arg1936z00_1954
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1938z00_1956,
																																													BNIL);
																																											}
																																											{	/* Eval/expddefine.scm 400 */
																																												obj_t
																																													BgL_arg1947z00_1965;
																																												{	/* Eval/expddefine.scm 400 */
																																													obj_t
																																														BgL_arg1948z00_1966;
																																													{	/* Eval/expddefine.scm 400 */
																																														obj_t
																																															BgL_arg1949z00_1967;
																																														obj_t
																																															BgL_arg1950z00_1968;
																																														{	/* Eval/expddefine.scm 400 */
																																															obj_t
																																																BgL_arg1951z00_1969;
																																															BgL_arg1951z00_1969
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_symbol2446z00zz__expander_definez00,
																																																BNIL);
																																															BgL_arg1949z00_1967
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_symbol2423z00zz__expander_definez00,
																																																BgL_arg1951z00_1969);
																																														}
																																														{	/* Eval/expddefine.scm 401 */
																																															obj_t
																																																BgL_arg1952z00_1970;
																																															obj_t
																																																BgL_arg1953z00_1971;
																																															{	/* Eval/expddefine.scm 401 */
																																																obj_t
																																																	BgL_arg1954z00_1972;
																																																BgL_arg1954z00_1972
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(CAR
																																																	(BgL_p0z00_1848),
																																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																	(BgL_namesz00_1933,
																																																		BNIL));
																																																BgL_arg1952z00_1970
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_symbol2446z00zz__expander_definez00,
																																																	BgL_arg1954z00_1972);
																																															}
																																															{	/* Eval/expddefine.scm 402 */
																																																obj_t
																																																	BgL_arg1957z00_1975;
																																																{	/* Eval/expddefine.scm 402 */
																																																	obj_t
																																																		BgL_arg1958z00_1976;
																																																	{	/* Eval/expddefine.scm 402 */
																																																		obj_t
																																																			BgL_arg1959z00_1977;
																																																		obj_t
																																																			BgL_arg1960z00_1978;
																																																		BgL_arg1959z00_1977
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_pfz00_1847));
																																																		BgL_arg1960z00_1978
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(CAR
																																																			(BgL_p0z00_1848),
																																																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																			(BgL_namesz00_1933,
																																																				BNIL));
																																																		BgL_arg1958z00_1976
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg1959z00_1977,
																																																			BgL_arg1960z00_1978);
																																																	}
																																																	BgL_arg1957z00_1975
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BGl_symbol2415z00zz__expander_definez00,
																																																		BgL_arg1958z00_1976);
																																																}
																																																BgL_arg1953z00_1971
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1957z00_1975,
																																																	BNIL);
																																															}
																																															BgL_arg1950z00_1968
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg1952z00_1970,
																																																BgL_arg1953z00_1971);
																																														}
																																														BgL_arg1948z00_1966
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg1949z00_1967,
																																															BgL_arg1950z00_1968);
																																													}
																																													BgL_arg1947z00_1965
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BGl_symbol2425z00zz__expander_definez00,
																																														BgL_arg1948z00_1966);
																																												}
																																												BgL_arg1937z00_1955
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1947z00_1965,
																																													BNIL);
																																											}
																																											BgL_arg1935z00_1953
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1936z00_1954,
																																												BgL_arg1937z00_1955);
																																										}
																																										BgL_arg1934z00_1952
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_symbol2407z00zz__expander_definez00,
																																											BgL_arg1935z00_1953);
																																									}
																																									BgL_arg1933z00_1951
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg1934z00_1952,
																																										BNIL);
																																								}
																																								BgL_arg1931z00_1949
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1932z00_1950,
																																									BgL_arg1933z00_1951);
																																							}
																																							BgL_arg1929z00_1947
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2403z00zz__expander_definez00,
																																								BgL_arg1931z00_1949);
																																						}
																																						BgL_arg1930z00_1948
																																							=
																																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																							(BgL_bodyz00_1830,
																																							BNIL);
																																						BgL_arg1927z00_1945
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1929z00_1947,
																																							BgL_arg1930z00_1948);
																																					}
																																					BgL_arg1925z00_1943
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1926z00_1944,
																																						BgL_arg1927z00_1945);
																																				}
																																				BgL_arg1924z00_1942
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2400z00zz__expander_definez00,
																																					BgL_arg1925z00_1943);
																																			}
																																			BgL_arg1920z00_1940
																																				=
																																				BGL_PROCEDURE_CALL2
																																				(BgL_ez00_24,
																																				BgL_arg1924z00_1942,
																																				BgL_ez00_24);
																																		}
																																		{	/* Eval/expddefine.scm 405 */
																																			obj_t
																																				BgL_arg1963z00_1981;
																																			{	/* Eval/expddefine.scm 405 */
																																				obj_t
																																					BgL_arg1964z00_1982;
																																				BgL_arg1964z00_1982
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_f0z00_1828,
																																					BNIL);
																																				BgL_arg1963z00_1981
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2433z00zz__expander_definez00,
																																					BgL_arg1964z00_1982);
																																			}
																																			BgL_arg1923z00_1941
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1963z00_1981,
																																				BNIL);
																																		}
																																		BgL_arg1919z00_1939
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1920z00_1940,
																																			BgL_arg1923z00_1941);
																																	}
																																	BgL_arg1917z00_1937
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1918z00_1938,
																																		BgL_arg1919z00_1939);
																																}
																																BgL_arg1914z00_1935
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1916z00_1936,
																																	BgL_arg1917z00_1937);
																															}
																															BgL_resz00_1934 =
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2448z00zz__expander_definez00,
																																BgL_arg1914z00_1935);
																														}
																														{	/* Eval/expddefine.scm 392 */

																															return
																																BGl_evepairifyz00zz__prognz00
																																(BgL_resz00_1934,
																																BgL_xz00_23);
																														}
																													}
																												}
																											}
																										}
																									else
																										{	/* Eval/expddefine.scm 409 */
																											obj_t
																												BgL_tformalsz00_1983;
																											BgL_tformalsz00_1983 =
																												BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
																												(BgL_formalsz00_1829,
																												BGl_errorzd2envzd2zz__errorz00,
																												((bool_t) 1));
																											{	/* Eval/expddefine.scm 409 */
																												obj_t
																													BgL_uformalsz00_1984;
																												BgL_uformalsz00_1984 =
																													BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
																													(BgL_formalsz00_1829,
																													BGl_errorzd2envzd2zz__errorz00,
																													((bool_t) 0));
																												{	/* Eval/expddefine.scm 410 */
																													obj_t BgL_resz00_1985;

																													{	/* Eval/expddefine.scm 412 */
																														obj_t
																															BgL_arg1965z00_1986;
																														{	/* Eval/expddefine.scm 412 */
																															obj_t
																																BgL_arg1966z00_1987;
																															obj_t
																																BgL_arg1967z00_1988;
																															BgL_arg1966z00_1987
																																=
																																CAR(((obj_t)
																																	BgL_pfz00_1847));
																															{	/* Eval/expddefine.scm 413 */
																																obj_t
																																	BgL_arg1968z00_1989;
																																obj_t
																																	BgL_arg1969z00_1990;
																																BgL_arg1968z00_1989
																																	=
																																	CDR
																																	(BgL_p0z00_1848);
																																{	/* Eval/expddefine.scm 414 */
																																	obj_t
																																		BgL_arg1970z00_1991;
																																	obj_t
																																		BgL_arg1971z00_1992;
																																	{	/* Eval/expddefine.scm 414 */
																																		obj_t
																																			BgL_arg1972z00_1993;
																																		{	/* Eval/expddefine.scm 414 */
																																			obj_t
																																				BgL_arg1973z00_1994;
																																			{	/* Eval/expddefine.scm 414 */
																																				obj_t
																																					BgL_arg1974z00_1995;
																																				obj_t
																																					BgL_arg1975z00_1996;
																																				{	/* Eval/expddefine.scm 414 */
																																					obj_t
																																						BgL_arg1976z00_1997;
																																					BgL_arg1976z00_1997
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_f0z00_1828,
																																						BgL_tformalsz00_1983);
																																					BgL_arg1974z00_1995
																																						=
																																						BGl_loopze70ze7zz__expander_definez00
																																						(BgL_ez00_24,
																																						BgL_arg1976z00_1997);
																																				}
																																				{	/* Eval/expddefine.scm 415 */
																																					obj_t
																																						BgL_arg1977z00_1998;
																																					obj_t
																																						BgL_arg1978z00_1999;
																																					{	/* Eval/expddefine.scm 415 */
																																						obj_t
																																							BgL_arg1979z00_2000;
																																						{	/* Eval/expddefine.scm 415 */
																																							obj_t
																																								BgL_arg1980z00_2001;
																																							obj_t
																																								BgL_arg1981z00_2002;
																																							BgL_arg1980z00_2001
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_symbol2442z00zz__expander_definez00,
																																								BNIL);
																																							{	/* Eval/expddefine.scm 416 */
																																								obj_t
																																									BgL_arg1982z00_2003;
																																								{	/* Eval/expddefine.scm 416 */
																																									obj_t
																																										BgL_arg1983z00_2004;
																																									{	/* Eval/expddefine.scm 416 */
																																										obj_t
																																											BgL_arg1984z00_2005;
																																										obj_t
																																											BgL_arg1985z00_2006;
																																										{	/* Eval/expddefine.scm 416 */
																																											obj_t
																																												BgL_arg1986z00_2007;
																																											{	/* Eval/expddefine.scm 416 */
																																												obj_t
																																													BgL_arg1987z00_2008;
																																												{	/* Eval/expddefine.scm 416 */
																																													obj_t
																																														BgL_arg1988z00_2009;
																																													{	/* Eval/expddefine.scm 416 */
																																														obj_t
																																															BgL_arg1989z00_2010;
																																														{	/* Eval/expddefine.scm 416 */
																																															obj_t
																																																BgL_arg1990z00_2011;
																																															obj_t
																																																BgL_arg1991z00_2012;
																																															BgL_arg1990z00_2011
																																																=
																																																CAR
																																																(BgL_p0z00_1848);
																																															{	/* Eval/expddefine.scm 417 */
																																																obj_t
																																																	BgL_arg1992z00_2013;
																																																obj_t
																																																	BgL_arg1993z00_2014;
																																																BgL_arg1992z00_2013
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_pfz00_1847));
																																																BgL_arg1993z00_2014
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(CDR
																																																	(BgL_p0z00_1848),
																																																	BNIL);
																																																BgL_arg1991z00_2012
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1992z00_2013,
																																																	BgL_arg1993z00_2014);
																																															}
																																															BgL_arg1989z00_2010
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg1990z00_2011,
																																																BgL_arg1991z00_2012);
																																														}
																																														BgL_arg1988z00_2009
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2444z00zz__expander_definez00,
																																															BgL_arg1989z00_2010);
																																													}
																																													BgL_arg1987z00_2008
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1988z00_2009,
																																														BNIL);
																																												}
																																												BgL_arg1986z00_2007
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2446z00zz__expander_definez00,
																																													BgL_arg1987z00_2008);
																																											}
																																											BgL_arg1984z00_2005
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1986z00_2007,
																																												BNIL);
																																										}
																																										{	/* Eval/expddefine.scm 419 */
																																											obj_t
																																												BgL_arg1995z00_2016;
																																											{	/* Eval/expddefine.scm 419 */
																																												obj_t
																																													BgL_arg1996z00_2017;
																																												{	/* Eval/expddefine.scm 419 */
																																													obj_t
																																														BgL_arg1997z00_2018;
																																													obj_t
																																														BgL_arg1998z00_2019;
																																													{	/* Eval/expddefine.scm 419 */
																																														obj_t
																																															BgL_arg1999z00_2020;
																																														BgL_arg1999z00_2020
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2446z00zz__expander_definez00,
																																															BNIL);
																																														BgL_arg1997z00_2018
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2423z00zz__expander_definez00,
																																															BgL_arg1999z00_2020);
																																													}
																																													{	/* Eval/expddefine.scm 420 */
																																														obj_t
																																															BgL_arg2000z00_2021;
																																														obj_t
																																															BgL_arg2001z00_2022;
																																														{	/* Eval/expddefine.scm 420 */
																																															obj_t
																																																BgL_arg2002z00_2023;
																																															{	/* Eval/expddefine.scm 420 */
																																																obj_t
																																																	BgL_arg2003z00_2024;
																																																BgL_arg2003z00_2024
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(CAR
																																																	(BgL_p0z00_1848),
																																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																	(BGl_pairzd2ze3listze70zd6zz__expander_definez00
																																																		(BgL_uformalsz00_1984),
																																																		BNIL));
																																																BgL_arg2002z00_2023
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_symbol2446z00zz__expander_definez00,
																																																	BgL_arg2003z00_2024);
																																															}
																																															BgL_arg2000z00_2021
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_symbol2415z00zz__expander_definez00,
																																																BgL_arg2002z00_2023);
																																														}
																																														{	/* Eval/expddefine.scm 422 */
																																															obj_t
																																																BgL_arg2008z00_2028;
																																															{	/* Eval/expddefine.scm 422 */
																																																obj_t
																																																	BgL_arg2009z00_2029;
																																																{	/* Eval/expddefine.scm 422 */
																																																	obj_t
																																																		BgL_arg2010z00_2030;
																																																	obj_t
																																																		BgL_arg2011z00_2031;
																																																	BgL_arg2010z00_2030
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_pfz00_1847));
																																																	BgL_arg2011z00_2031
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(CAR
																																																		(BgL_p0z00_1848),
																																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																																		(BGl_pairzd2ze3listze70zd6zz__expander_definez00
																																																			(BgL_uformalsz00_1984),
																																																			BNIL));
																																																	BgL_arg2009z00_2029
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg2010z00_2030,
																																																		BgL_arg2011z00_2031);
																																																}
																																																BgL_arg2008z00_2028
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_symbol2415z00zz__expander_definez00,
																																																	BgL_arg2009z00_2029);
																																															}
																																															BgL_arg2001z00_2022
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg2008z00_2028,
																																																BNIL);
																																														}
																																														BgL_arg1998z00_2019
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_arg2000z00_2021,
																																															BgL_arg2001z00_2022);
																																													}
																																													BgL_arg1996z00_2017
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1997z00_2018,
																																														BgL_arg1998z00_2019);
																																												}
																																												BgL_arg1995z00_2016
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_symbol2425z00zz__expander_definez00,
																																													BgL_arg1996z00_2017);
																																											}
																																											BgL_arg1985z00_2006
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1995z00_2016,
																																												BNIL);
																																										}
																																										BgL_arg1983z00_2004
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1984z00_2005,
																																											BgL_arg1985z00_2006);
																																									}
																																									BgL_arg1982z00_2003
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2407z00zz__expander_definez00,
																																										BgL_arg1983z00_2004);
																																								}
																																								BgL_arg1981z00_2002
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1982z00_2003,
																																									BNIL);
																																							}
																																							BgL_arg1979z00_2000
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1980z00_2001,
																																								BgL_arg1981z00_2002);
																																						}
																																						BgL_arg1977z00_1998
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2403z00zz__expander_definez00,
																																							BgL_arg1979z00_2000);
																																					}
																																					{	/* Eval/expddefine.scm 425 */
																																						obj_t
																																							BgL_arg2015z00_2035;
																																						{	/* Eval/expddefine.scm 425 */
																																							obj_t
																																								BgL_arg2016z00_2036;
																																							{	/* Eval/expddefine.scm 425 */
																																								obj_t
																																									BgL_arg2017z00_2037;
																																								BgL_arg2017z00_2037
																																									=
																																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																									(BgL_bodyz00_1830,
																																									BNIL);
																																								BgL_arg2016z00_2036
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_symbol2395z00zz__expander_definez00,
																																									BgL_arg2017z00_2037);
																																							}
																																							BgL_arg2015z00_2035
																																								=
																																								BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00
																																								(BgL_funz00_1827,
																																								BgL_formalsz00_1829,
																																								BgL_arg2016z00_2036,
																																								BGl_errorzd2envzd2zz__errorz00);
																																						}
																																						BgL_arg1978z00_1999
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2015z00_2035,
																																							BNIL);
																																					}
																																					BgL_arg1975z00_1996
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1977z00_1998,
																																						BgL_arg1978z00_1999);
																																				}
																																				BgL_arg1973z00_1994
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1974z00_1995,
																																					BgL_arg1975z00_1996);
																																			}
																																			BgL_arg1972z00_1993
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2400z00zz__expander_definez00,
																																				BgL_arg1973z00_1994);
																																		}
																																		BgL_arg1970z00_1991
																																			=
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_24,
																																			BgL_arg1972z00_1993,
																																			BgL_ez00_24);
																																	}
																																	{	/* Eval/expddefine.scm 427 */
																																		obj_t
																																			BgL_arg2018z00_2038;
																																		{	/* Eval/expddefine.scm 427 */
																																			obj_t
																																				BgL_arg2019z00_2039;
																																			BgL_arg2019z00_2039
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_f0z00_1828,
																																				BNIL);
																																			BgL_arg2018z00_2038
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2433z00zz__expander_definez00,
																																				BgL_arg2019z00_2039);
																																		}
																																		BgL_arg1971z00_1992
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2018z00_2038,
																																			BNIL);
																																	}
																																	BgL_arg1969z00_1990
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1970z00_1991,
																																		BgL_arg1971z00_1992);
																																}
																																BgL_arg1967z00_1988
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1968z00_1989,
																																	BgL_arg1969z00_1990);
																															}
																															BgL_arg1965z00_1986
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1966z00_1987,
																																BgL_arg1967z00_1988);
																														}
																														BgL_resz00_1985 =
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2448z00zz__expander_definez00,
																															BgL_arg1965z00_1986);
																													}
																													{	/* Eval/expddefine.scm 411 */

																														return
																															BGl_evepairifyz00zz__prognz00
																															(BgL_resz00_1985,
																															BgL_xz00_23);
																													}
																												}
																											}
																										}
																								}
																						}
																					else
																						{	/* Eval/expddefine.scm 366 */
																							return
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string2450z00zz__expander_definez00,
																								BGl_string2399z00zz__expander_definez00,
																								BgL_xz00_23);
																						}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
											}
										else
											{	/* Eval/expddefine.scm 357 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string2450z00zz__expander_definez00,
													BGl_string2399z00zz__expander_definez00, BgL_xz00_23);
											}
									}
								else
									{	/* Eval/expddefine.scm 357 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2450z00zz__expander_definez00,
											BGl_string2399z00zz__expander_definez00, BgL_xz00_23);
									}
							}
						else
							{	/* Eval/expddefine.scm 357 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string2450z00zz__expander_definez00,
									BGl_string2399z00zz__expander_definez00, BgL_xz00_23);
							}
					}
				else
					{	/* Eval/expddefine.scm 357 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string2450z00zz__expander_definez00,
							BGl_string2399z00zz__expander_definez00, BgL_xz00_23);
					}
			}
		}

	}



/* get-args~0 */
	obj_t BGl_getzd2argsze70z35zz__expander_definez00(obj_t BgL_argsz00_2065,
		obj_t BgL_locz00_2066)
	{
		{	/* Eval/expddefine.scm 349 */
			if (NULLP(BgL_argsz00_2065))
				{	/* Eval/expddefine.scm 343 */
					return BNIL;
				}
			else
				{	/* Eval/expddefine.scm 343 */
					if (PAIRP(BgL_argsz00_2065))
						{	/* Eval/expddefine.scm 346 */
							obj_t BgL_arg2037z00_2070;
							obj_t BgL_arg2038z00_2071;

							{	/* Eval/expddefine.scm 346 */
								obj_t BgL_arg2039z00_2072;
								obj_t BgL_arg2040z00_2073;

								BgL_arg2039z00_2072 = CAR(BgL_argsz00_2065);
								{	/* Eval/expddefine.scm 346 */
									obj_t BgL__ortest_1045z00_2074;

									BgL__ortest_1045z00_2074 =
										BGl_getzd2sourcezd2locationz00zz__readerz00
										(BgL_argsz00_2065);
									if (CBOOL(BgL__ortest_1045z00_2074))
										{	/* Eval/expddefine.scm 346 */
											BgL_arg2040z00_2073 = BgL__ortest_1045z00_2074;
										}
									else
										{	/* Eval/expddefine.scm 346 */
											BgL_arg2040z00_2073 = BgL_locz00_2066;
										}
								}
								{	/* Eval/expddefine.scm 336 */
									obj_t BgL_rz00_2907;

									BgL_rz00_2907 =
										BGl_parsezd2formalzd2identz00zz__evutilsz00
										(BgL_arg2039z00_2072, BgL_arg2040z00_2073);
									if (PAIRP(BgL_rz00_2907))
										{	/* Eval/expddefine.scm 337 */
											BgL_arg2037z00_2070 = CAR(BgL_rz00_2907);
										}
									else
										{	/* Eval/expddefine.scm 337 */
											BgL_arg2037z00_2070 = BgL_rz00_2907;
										}
								}
							}
							BgL_arg2038z00_2071 =
								BGl_getzd2argsze70z35zz__expander_definez00(CDR
								(BgL_argsz00_2065), BgL_locz00_2066);
							return MAKE_YOUNG_PAIR(BgL_arg2037z00_2070, BgL_arg2038z00_2071);
						}
					else
						{	/* Eval/expddefine.scm 349 */
							obj_t BgL_arg2042z00_2076;

							{	/* Eval/expddefine.scm 336 */
								obj_t BgL_rz00_2911;

								BgL_rz00_2911 =
									BGl_parsezd2formalzd2identz00zz__evutilsz00(BgL_argsz00_2065,
									BgL_locz00_2066);
								if (PAIRP(BgL_rz00_2911))
									{	/* Eval/expddefine.scm 337 */
										BgL_arg2042z00_2076 = CAR(BgL_rz00_2911);
									}
								else
									{	/* Eval/expddefine.scm 337 */
										BgL_arg2042z00_2076 = BgL_rz00_2911;
									}
							}
							{	/* Eval/expddefine.scm 349 */
								obj_t BgL_list2043z00_2077;

								BgL_list2043z00_2077 =
									MAKE_YOUNG_PAIR(BgL_arg2042z00_2076, BNIL);
								return BgL_list2043z00_2077;
							}
						}
				}
		}

	}



/* pair->list~0 */
	obj_t BGl_pairzd2ze3listze70zd6zz__expander_definez00(obj_t BgL_pz00_2078)
	{
		{	/* Eval/expddefine.scm 355 */
			if (PAIRP(BgL_pz00_2078))
				{	/* Eval/expddefine.scm 353 */
					return
						MAKE_YOUNG_PAIR(CAR(BgL_pz00_2078),
						BGl_pairzd2ze3listze70zd6zz__expander_definez00(CDR
							(BgL_pz00_2078)));
				}
			else
				{	/* Eval/expddefine.scm 353 */
					if (NULLP(BgL_pz00_2078))
						{	/* Eval/expddefine.scm 354 */
							return BNIL;
						}
					else
						{	/* Eval/expddefine.scm 355 */
							obj_t BgL_list2050z00_2085;

							BgL_list2050z00_2085 = MAKE_YOUNG_PAIR(BgL_pz00_2078, BNIL);
							return BgL_list2050z00_2085;
						}
				}
		}

	}



/* &expand-eval-define-method */
	obj_t BGl_z62expandzd2evalzd2definezd2methodzb0zz__expander_definez00(obj_t
		BgL_envz00_3186, obj_t BgL_xz00_3187, obj_t BgL_ez00_3188)
	{
		{	/* Eval/expddefine.scm 333 */
			{	/* Eval/expddefine.scm 336 */
				obj_t BgL_auxz00_4582;

				if (PROCEDUREP(BgL_ez00_3188))
					{	/* Eval/expddefine.scm 336 */
						BgL_auxz00_4582 = BgL_ez00_3188;
					}
				else
					{
						obj_t BgL_auxz00_4585;

						BgL_auxz00_4585 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2392z00zz__expander_definez00, BINT(12172L),
							BGl_string2451z00zz__expander_definez00,
							BGl_string2394z00zz__expander_definez00, BgL_ez00_3188);
						FAILURE(BgL_auxz00_4585, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2evalzd2definezd2methodzd2zz__expander_definez00
					(BgL_xz00_3187, BgL_auxz00_4582);
			}
		}

	}



/* dsssl-formals->names */
	obj_t BGl_dssslzd2formalszd2ze3namesze3zz__expander_definez00(obj_t
		BgL_formalsz00_25)
	{
		{	/* Eval/expddefine.scm 438 */
			{	/* Eval/expddefine.scm 439 */
				obj_t BgL_list2052z00_2090;

				BgL_list2052z00_2090 = MAKE_YOUNG_PAIR(BgL_formalsz00_25, BNIL);
				return
					BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
					(BGl_proc2452z00zz__expander_definez00, BgL_list2052z00_2090);
			}
		}

	}



/* &<@anonymous:2053> */
	obj_t BGl_z62zc3z04anonymousza32053ze3ze5zz__expander_definez00(obj_t
		BgL_envz00_3194, obj_t BgL_pz00_3195)
	{
		{	/* Eval/expddefine.scm 439 */
			if (SYMBOLP(BgL_pz00_3195))
				{	/* Eval/expddefine.scm 441 */
					return BgL_pz00_3195;
				}
			else
				{	/* Eval/expddefine.scm 441 */
					if (PAIRP(BgL_pz00_3195))
						{	/* Eval/expddefine.scm 442 */
							return CAR(BgL_pz00_3195);
						}
					else
						{	/* Eval/expddefine.scm 442 */
							return BFALSE;
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_definez00(void)
	{
		{	/* Eval/expddefine.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_definez00(void)
	{
		{	/* Eval/expddefine.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_definez00(void)
	{
		{	/* Eval/expddefine.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_definez00(void)
	{
		{	/* Eval/expddefine.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2453z00zz__expander_definez00));
		}

	}

#ifdef __cplusplus
}
#endif
