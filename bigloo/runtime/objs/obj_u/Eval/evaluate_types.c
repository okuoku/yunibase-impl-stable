/*===========================================================================*/
/*   (Eval/evaluate_types.scm)                                               */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evaluate_types.scm -indent -o objs/obj_u/Eval/evaluate_types.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVALUATE_TYPES_TYPE_DEFINITIONS
#define BGL___EVALUATE_TYPES_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ev_exprz00_bgl
	{
		header_t header;
		obj_t widening;
	}                 *BgL_ev_exprz00_bglt;

	typedef struct BgL_ev_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_effz00;
		obj_t BgL_typez00;
	}                *BgL_ev_varz00_bglt;

	typedef struct BgL_ev_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                   *BgL_ev_globalz00_bglt;

	typedef struct BgL_ev_littz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_valuez00;
	}                 *BgL_ev_littz00_bglt;

	typedef struct BgL_ev_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_pz00;
		struct BgL_ev_exprz00_bgl *BgL_tz00;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}               *BgL_ev_ifz00_bglt;

	typedef struct BgL_ev_listz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                 *BgL_ev_listz00_bglt;

	typedef struct BgL_ev_orz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}               *BgL_ev_orz00_bglt;

	typedef struct BgL_ev_andz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                *BgL_ev_andz00_bglt;

	typedef struct BgL_ev_prog2z00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_e1z00;
		struct BgL_ev_exprz00_bgl *BgL_e2z00;
	}                  *BgL_ev_prog2z00_bglt;

	typedef struct BgL_ev_hookz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_hookz00_bglt;

	typedef struct BgL_ev_trapz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_trapz00_bglt;

	typedef struct BgL_ev_setlocalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_varz00_bgl *BgL_vz00;
	}                     *BgL_ev_setlocalz00_bglt;

	typedef struct BgL_ev_setglobalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                      *BgL_ev_setglobalz00_bglt;

	typedef struct BgL_ev_defglobalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                      *BgL_ev_defglobalz00_bglt;

	typedef struct BgL_ev_bindzd2exitzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_varz00_bgl *BgL_varz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                        *BgL_ev_bindzd2exitzd2_bglt;

	typedef struct BgL_ev_unwindzd2protectzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                             *BgL_ev_unwindzd2protectzd2_bglt;

	typedef struct BgL_ev_withzd2handlerzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_handlerz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                           *BgL_ev_withzd2handlerzd2_bglt;

	typedef struct BgL_ev_synchroniza7eza7_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_mutexz00;
		struct BgL_ev_exprz00_bgl *BgL_prelockz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                          *BgL_ev_synchroniza7eza7_bglt;

	typedef struct BgL_ev_binderz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_binderz00_bglt;

	typedef struct BgL_ev_letz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_letz00_bglt;

	typedef struct BgL_ev_letza2za2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_letza2za2_bglt;

	typedef struct BgL_ev_letrecz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_letrecz00_bglt;

	typedef struct BgL_ev_labelsz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		obj_t BgL_envz00;
		obj_t BgL_stkz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_labelsz00_bglt;

	typedef struct BgL_ev_gotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_varz00_bgl *BgL_labelz00;
		struct BgL_ev_labelsz00_bgl *BgL_labelsz00;
		obj_t BgL_argsz00;
	}                 *BgL_ev_gotoz00_bglt;

	typedef struct BgL_ev_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_tailzf3zf3;
	}                *BgL_ev_appz00_bglt;

	typedef struct BgL_ev_absz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_wherez00;
		obj_t BgL_arityz00;
		obj_t BgL_varsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		int BgL_siza7eza7;
		obj_t BgL_bindz00;
		obj_t BgL_freez00;
		obj_t BgL_innerz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_absz00_bglt;


#endif													// BGL___EVALUATE_TYPES_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1927z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1767z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1686z62zz__evaluate_typesz00(obj_t, obj_t);
	static BgL_ev_letza2za2_bglt BGl_z62lambda1849z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1768z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1687z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2643z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2564z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2484z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2649z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2568z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2489z00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_letza2za2_bglt BGl_z62lambda1851z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_gotoz00_bglt BGl_z62lambda1934z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_defglobalz00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_gotoz00_bglt BGl_z62lambda1936z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_synchroniza7eza7_bglt
		BGl_z62lambda1774z62zz__evaluate_typesz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_ev_synchroniza7eza7_bglt
		BGl_z62lambda1776z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1859z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_symbol2572z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2655z00zz__evaluate_typesz00 = BUNSPEC;
	extern obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_symbol2493z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2498z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2579z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda1860z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1942z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1943z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31758ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1783z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1784z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_varz00_bglt BGl_z62lambda1947z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1948z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_letrecz00_bglt BGl_z62lambda1869z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1788z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1789z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2660z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2584z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2666z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2589z00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_labelsz00_bglt BGl_z62lambda1952z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_letrecz00_bglt BGl_z62lambda1871z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1953z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1793z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1794z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1957z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1958z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_labelsz00_bglt BGl_z62lambda1879z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1798z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1799z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2670z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2597z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2679z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31873ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31938ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_letrecz00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_labelsz00_bglt BGl_z62lambda1881z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_appz00_bglt BGl_z62lambda1964z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_ev_appz00_bglt BGl_z62lambda1966z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1888z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1889z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2681z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2686z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__evaluate_typesz00(void);
	static obj_t BGl_z62lambda1972z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1973z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31599ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1893z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1894z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_letz00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_exprz00_bglt BGl_z62lambda1977z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1978z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2691z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2696z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31883ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31778ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1982z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1983z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__evaluate_typesz00(void);
	static obj_t BGl_z62lambda1987z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1988z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_prog2z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32040ze3ze5zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32032ze3ze5zz__evaluate_typesz00(obj_t);
	extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__evaluate_typesz00(void);
	BGL_EXPORTED_DEF obj_t BGl_ev_bindzd2exitzd2zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_absz00_bglt BGl_z62lambda1994z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_ev_absz00_bglt BGl_z62lambda1996z62zz__evaluate_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31968ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_letza2za2zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_labelsz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__evaluate_typesz00(void);
	static obj_t BGl_z62zc3z04anonymousza31502ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_absz00zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_trapz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32055ze3ze5zz__evaluate_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_setglobalz00zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_ifz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda2002z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2003z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32048ze3ze5zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda2008z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2009z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_hookz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda2013z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31522ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2014z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2018z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2019z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31998ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda2023z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2024z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2030z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2031z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31621ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31435ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_gotoz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda2038z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2039z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2046z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2047z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	BGL_EXPORTED_DEF obj_t BGl_ev_globalz00zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_binderz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31704ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2053z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31445ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2054z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_listz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda2060z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2061z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_littz00zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_orz00zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_varz00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_littz00_bglt BGl_z62lambda1500z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1506z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1507z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1429z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evaluate_typesz00(void);
	extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_typesz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evaluate_typesz00(void);
	static BgL_ev_exprz00_bglt BGl_z62lambda1431z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31715ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31553ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_ifz00_bglt BGl_z62lambda1514z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_ev_ifz00_bglt BGl_z62lambda1517z62zz__evaluate_typesz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_withzd2handlerzd2zz__evaluate_typesz00 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_andz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31902ze3ze5zz__evaluate_typesz00(obj_t);
	static BgL_ev_varz00_bglt BGl_z62lambda1441z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1604z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31473ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1605z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_varz00_bglt BGl_z62lambda1443z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__evaluate_typesz00(obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1526z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1527z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_objectz00zz__objectz00;
	static BgL_ev_exprz00_bglt BGl_z62lambda1609z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1449z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_symbol2404z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2408z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda1610z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1531z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1450z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1532z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_hookz00_bglt BGl_z62lambda1617z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1455z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1456z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_hookz00_bglt BGl_z62lambda1619z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1538z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1539z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2414z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2419z00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_defglobalz00_bglt
		BGl_z62lambda1700z62zz__evaluate_typesz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_ev_defglobalz00_bglt
		BGl_z62lambda1702z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31572ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1461z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31637ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1462z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1625z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1626z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_exprz00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_listz00_bglt BGl_z62lambda1548z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_globalz00_bglt BGl_z62lambda1468z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_appz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2423z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2428z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2509z00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_bindzd2exitzd2_bglt
		BGl_z62lambda1710z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31913ze3ze5zz__evaluate_typesz00(obj_t);
	static BgL_ev_bindzd2exitzd2_bglt
		BGl_z62lambda1712z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_listz00_bglt BGl_z62lambda1550z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_trapz00_bglt BGl_z62lambda1632z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_globalz00_bglt BGl_z62lambda1470z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_trapz00_bglt BGl_z62lambda1635z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1557z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1558z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1478z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1479z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2432z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2514z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2436z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2518z00zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_varz00_bglt BGl_z62lambda1721z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1722z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31809ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_binderz00_bglt BGl_z62lambda1805z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31647ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_setlocalz00_bglt
		BGl_z62lambda1643z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ev_synchroniza7eza7zz__evaluate_typesz00 = BUNSPEC;
	static BgL_ev_binderz00_bglt BGl_z62lambda1807z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1726z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_setlocalz00_bglt
		BGl_z62lambda1645z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1483z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1727z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1484z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_orz00_bglt BGl_z62lambda1566z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_orz00_bglt BGl_z62lambda1568z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1488z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1489z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2440z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2602z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2522z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2445z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2527z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2449z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda1813z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31664ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_varz00_bglt BGl_z62lambda1651z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1814z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1652z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_unwindzd2protectzd2_bglt
		BGl_z62lambda1734z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_unwindzd2protectzd2_bglt
		BGl_z62lambda1736z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1818z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1819z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_setglobalz00_bglt
		BGl_z62lambda1659z62zz__evaluate_typesz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_ev_littz00_bglt BGl_z62lambda1498z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2611z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2454z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2616z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2536z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2459z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda1900z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1901z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__evaluate_typesz00(obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1742z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_andz00_bglt BGl_z62lambda1580z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1824z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31738ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1743z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_setglobalz00_bglt
		BGl_z62lambda1662z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1825z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_andz00_bglt BGl_z62lambda1583z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1747z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1748z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2540z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2704z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2380z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2706z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2382z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2626z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2464z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2547z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2386z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2468z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2388z00zz__evaluate_typesz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ev_setlocalz00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62lambda1911z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1912z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31836ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_letz00_bglt BGl_z62lambda1832z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1670z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1671z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31585ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_letz00_bglt BGl_z62lambda1834z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_withzd2handlerzd2_bglt
		BGl_z62lambda1754z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1918z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static BgL_ev_withzd2handlerzd2_bglt
		BGl_z62lambda1756z62zz__evaluate_typesz00(obj_t);
	static BgL_ev_prog2z00_bglt BGl_z62lambda1594z62zz__evaluate_typesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1919z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_prog2z00_bglt BGl_z62lambda1596z62zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62lambda1679z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_symbol2632z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2472z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2553z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2636z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2393z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2395z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2557z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2477z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_symbol2399z00zz__evaluate_typesz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31861ze3ze5zz__evaluate_typesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31853ze3ze5zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1841z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1842z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1680z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static BgL_ev_exprz00_bglt BGl_z62lambda1762z62zz__evaluate_typesz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1763z62zz__evaluate_typesz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1926z62zz__evaluate_typesz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2400z00zz__evaluate_typesz00,
		BgL_bgl_string2400za700za7za7_2708za7, "type", 4);
	      DEFINE_STRING(BGl_string2405z00zz__evaluate_typesz00,
		BgL_bgl_string2405za700za7za7_2709za7, "ev_var", 6);
	      DEFINE_STRING(BGl_string2409z00zz__evaluate_typesz00,
		BgL_bgl_string2409za700za7za7_2710za7, "loc", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2401z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2711za7,
		BGl_z62zc3z04anonymousza31445ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2402z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1443za7622712z00,
		BGl_z62lambda1443z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2403z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1441za7622713z00,
		BGl_z62lambda1441z62zz__evaluate_typesz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2406z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1479za7622714z00,
		BGl_z62lambda1479z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2407z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1478za7622715z00,
		BGl_z62lambda1478z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2415z00zz__evaluate_typesz00,
		BgL_bgl_string2415za700za7za7_2716za7, "mod", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2410z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1484za7622717z00,
		BGl_z62lambda1484z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2411z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1483za7622718z00,
		BGl_z62lambda1483z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2412z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1489za7622719z00,
		BGl_z62lambda1489z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2413z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1488za7622720z00,
		BGl_z62lambda1488z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2420z00zz__evaluate_typesz00,
		BgL_bgl_string2420za700za7za7_2721za7, "ev_global", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2416z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2722za7,
		BGl_z62zc3z04anonymousza31473ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2417z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1470za7622723z00,
		BGl_z62lambda1470z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2418z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1468za7622724z00,
		BGl_z62lambda1468z62zz__evaluate_typesz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2424z00zz__evaluate_typesz00,
		BgL_bgl_string2424za700za7za7_2725za7, "value", 5);
	      DEFINE_STRING(BGl_string2429z00zz__evaluate_typesz00,
		BgL_bgl_string2429za700za7za7_2726za7, "ev_litt", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2500z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1671za7622727z00,
		BGl_z62lambda1671z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2501z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1670za7622728z00,
		BGl_z62lambda1670z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2421z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1507za7622729z00,
		BGl_z62lambda1507z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2502z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1680za7622730z00,
		BGl_z62lambda1680z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2422z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1506za7622731z00,
		BGl_z62lambda1506z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2503z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1679za7622732z00,
		BGl_z62lambda1679z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2504z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1687za7622733z00,
		BGl_z62lambda1687z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2510z00zz__evaluate_typesz00,
		BgL_bgl_string2510za700za7za7_2734za7, "ev_setglobal", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2505z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1686za7622735z00,
		BGl_z62lambda1686z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2425z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2736za7,
		BGl_z62zc3z04anonymousza31502ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2506z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2737za7,
		BGl_z62zc3z04anonymousza31664ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2426z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1500za7622738z00,
		BGl_z62lambda1500z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2507z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1662za7622739z00,
		BGl_z62lambda1662z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2427z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1498za7622740z00,
		BGl_z62lambda1498z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2433z00zz__evaluate_typesz00,
		BgL_bgl_string2433za700za7za7_2741za7, "p", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2508z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1659za7622742z00,
		BGl_z62lambda1659z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2515z00zz__evaluate_typesz00,
		BgL_bgl_string2515za700za7za7_2743za7, "ev_defglobal", 12);
	      DEFINE_STRING(BGl_string2437z00zz__evaluate_typesz00,
		BgL_bgl_string2437za700za7za7_2744za7, "t", 1);
	      DEFINE_STRING(BGl_string2519z00zz__evaluate_typesz00,
		BgL_bgl_string2519za700za7za7_2745za7, "var", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2430z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1527za7622746z00,
		BGl_z62lambda1527z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2511z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2747za7,
		BGl_z62zc3z04anonymousza31704ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2431z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1526za7622748z00,
		BGl_z62lambda1526z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2512z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1702za7622749z00,
		BGl_z62lambda1702z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2513z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1700za7622750z00,
		BGl_z62lambda1700z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2434z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1532za7622751z00,
		BGl_z62lambda1532z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2435z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1531za7622752z00,
		BGl_z62lambda1531z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2441z00zz__evaluate_typesz00,
		BgL_bgl_string2441za700za7za7_2753za7, "e", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2516z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1722za7622754z00,
		BGl_z62lambda1722z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2603z00zz__evaluate_typesz00,
		BgL_bgl_string2603za700za7za7_2755za7, "ev_letrec", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2517z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1721za7622756z00,
		BGl_z62lambda1721z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2523z00zz__evaluate_typesz00,
		BgL_bgl_string2523za700za7za7_2757za7, "body", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2438z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1539za7622758z00,
		BGl_z62lambda1539z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2439z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1538za7622759z00,
		BGl_z62lambda1538z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2446z00zz__evaluate_typesz00,
		BgL_bgl_string2446za700za7za7_2760za7, "ev_if", 5);
	      DEFINE_STRING(BGl_string2528z00zz__evaluate_typesz00,
		BgL_bgl_string2528za700za7za7_2761za7, "ev_bind-exit", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2600z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1871za7622762z00,
		BGl_z62lambda1871z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2520z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1727za7622763z00,
		BGl_z62lambda1727z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2601z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1869za7622764z00,
		BGl_z62lambda1869z62zz__evaluate_typesz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2521z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1726za7622765z00,
		BGl_z62lambda1726z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2442z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2766za7,
		BGl_z62zc3z04anonymousza31522ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2604z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1889za7622767z00,
		BGl_z62lambda1889z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2443z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1517za7622768z00,
		BGl_z62lambda1517z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2524z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2769za7,
		BGl_z62zc3z04anonymousza31715ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2605z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1888za7622770z00,
		BGl_z62lambda1888z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2444z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1514za7622771z00,
		BGl_z62lambda1514z62zz__evaluate_typesz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2450z00zz__evaluate_typesz00,
		BgL_bgl_string2450za700za7za7_2772za7, "args", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2525z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1712za7622773z00,
		BGl_z62lambda1712z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2606z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1894za7622774z00,
		BGl_z62lambda1894z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2612z00zz__evaluate_typesz00,
		BgL_bgl_string2612za700za7za7_2775za7, "env", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2526z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1710za7622776z00,
		BGl_z62lambda1710z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2607z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1893za7622777z00,
		BGl_z62lambda1893z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2608z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2778za7,
		BGl_z62zc3z04anonymousza31902ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2447z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1558za7622779z00,
		BGl_z62lambda1558z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2609z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1901za7622780z00,
		BGl_z62lambda1901z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2448z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1557za7622781z00,
		BGl_z62lambda1557z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2529z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1743za7622782z00,
		BGl_z62lambda1743z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2455z00zz__evaluate_typesz00,
		BgL_bgl_string2455za700za7za7_2783za7, "ev_list", 7);
	      DEFINE_STRING(BGl_string2617z00zz__evaluate_typesz00,
		BgL_bgl_string2617za700za7za7_2784za7, "stk", 3);
	      DEFINE_STRING(BGl_string2537z00zz__evaluate_typesz00,
		BgL_bgl_string2537za700za7za7_2785za7, "ev_unwind-protect", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2610z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1900za7622786z00,
		BGl_z62lambda1900z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2530z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1742za7622787z00,
		BGl_z62lambda1742z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2531z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1748za7622788z00,
		BGl_z62lambda1748z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2451z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2789za7,
		BGl_z62zc3z04anonymousza31553ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2532z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1747za7622790z00,
		BGl_z62lambda1747z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2613z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2791za7,
		BGl_z62zc3z04anonymousza31913ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2452z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1550za7622792z00,
		BGl_z62lambda1550z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2533z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2793za7,
		BGl_z62zc3z04anonymousza31738ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2614z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1912za7622794z00,
		BGl_z62lambda1912z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2453z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1548za7622795z00,
		BGl_z62lambda1548z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2534z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1736za7622796z00,
		BGl_z62lambda1736z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2615z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1911za7622797z00,
		BGl_z62lambda1911z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2460z00zz__evaluate_typesz00,
		BgL_bgl_string2460za700za7za7_2798za7, "ev_or", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2535z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1734za7622799z00,
		BGl_z62lambda1734z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2541z00zz__evaluate_typesz00,
		BgL_bgl_string2541za700za7za7_2800za7, "handler", 7);
	      DEFINE_STRING(BGl_string2705z00zz__evaluate_typesz00,
		BgL_bgl_string2705za700za7za7_2801za7, "ev_abs", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2618z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1919za7622802z00,
		BGl_z62lambda1919z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2381z00zz__evaluate_typesz00,
		BgL_bgl_string2381za700za7za7_2803za7, "ev_expr", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2456z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2804za7,
		BGl_z62zc3z04anonymousza31572ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2619z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1918za7622805z00,
		BGl_z62lambda1918z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2457z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1568za7622806z00,
		BGl_z62lambda1568z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2538z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1763za7622807z00,
		BGl_z62lambda1763z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2707z00zz__evaluate_typesz00,
		BgL_bgl_string2707za700za7za7_2808za7, "_", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2377z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2809za7,
		BGl_z62zc3z04anonymousza31435ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2383z00zz__evaluate_typesz00,
		BgL_bgl_string2383za700za7za7_2810za7, "__evaluate_types", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2458z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1566za7622811z00,
		BGl_z62lambda1566z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2539z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1762za7622812z00,
		BGl_z62lambda1762z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2627z00zz__evaluate_typesz00,
		BgL_bgl_string2627za700za7za7_2813za7, "ev_labels", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2378z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1431za7622814z00,
		BGl_z62lambda1431z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2465z00zz__evaluate_typesz00,
		BgL_bgl_string2465za700za7za7_2815za7, "ev_and", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2379z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1429za7622816z00,
		BGl_z62lambda1429z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2548z00zz__evaluate_typesz00,
		BgL_bgl_string2548za700za7za7_2817za7, "ev_with-handler", 15);
	      DEFINE_STRING(BGl_string2387z00zz__evaluate_typesz00,
		BgL_bgl_string2387za700za7za7_2818za7, "name", 4);
	      DEFINE_STRING(BGl_string2469z00zz__evaluate_typesz00,
		BgL_bgl_string2469za700za7za7_2819za7, "e1", 2);
	      DEFINE_STRING(BGl_string2389z00zz__evaluate_typesz00,
		BgL_bgl_string2389za700za7za7_2820za7, "symbol", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2700z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2060za7622821z00,
		BGl_z62lambda2060z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2701z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2822za7,
		BGl_z62zc3z04anonymousza31998ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2620z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2823za7,
		BGl_z62zc3z04anonymousza31928ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2702z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1996za7622824z00,
		BGl_z62lambda1996z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2621z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1927za7622825z00,
		BGl_z62lambda1927z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2703z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1994za7622826z00,
		BGl_z62lambda1994z62zz__evaluate_typesz00, 0L, BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2622z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1926za7622827z00,
		BGl_z62lambda1926z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2623z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2828za7,
		BGl_z62zc3z04anonymousza31883ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2461z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2829za7,
		BGl_z62zc3z04anonymousza31585ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2542z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1768za7622830z00,
		BGl_z62lambda1768z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2624z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1881za7622831z00,
		BGl_z62lambda1881z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2462z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1583za7622832z00,
		BGl_z62lambda1583z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2543z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1767za7622833z00,
		BGl_z62lambda1767z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2625z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1879za7622834z00,
		BGl_z62lambda1879z62zz__evaluate_typesz00, 0L, BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2463z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1580za7622835z00,
		BGl_z62lambda1580z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2544z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2836za7,
		BGl_z62zc3z04anonymousza31758ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2545z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1756za7622837z00,
		BGl_z62lambda1756z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2633z00zz__evaluate_typesz00,
		BgL_bgl_string2633za700za7za7_2838za7, "label", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2384z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1450za7622839z00,
		BGl_z62lambda1450z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2546z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1754za7622840z00,
		BGl_z62lambda1754z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2628z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1943za7622841z00,
		BGl_z62lambda1943z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2385z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1449za7622842z00,
		BGl_z62lambda1449z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2466z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1605za7622843z00,
		BGl_z62lambda1605z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2629z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1942za7622844z00,
		BGl_z62lambda1942z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2467z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1604za7622845z00,
		BGl_z62lambda1604z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2473z00zz__evaluate_typesz00,
		BgL_bgl_string2473za700za7za7_2846za7, "e2", 2);
	      DEFINE_STRING(BGl_string2554z00zz__evaluate_typesz00,
		BgL_bgl_string2554za700za7za7_2847za7, "mutex", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2549z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1784za7622848z00,
		BGl_z62lambda1784z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2637z00zz__evaluate_typesz00,
		BgL_bgl_string2637za700za7za7_2849za7, "labels", 6);
	      DEFINE_STRING(BGl_string2394z00zz__evaluate_typesz00,
		BgL_bgl_string2394za700za7za7_2850za7, "eff", 3);
	      DEFINE_STRING(BGl_string2396z00zz__evaluate_typesz00,
		BgL_bgl_string2396za700za7za7_2851za7, "obj", 3);
	      DEFINE_STRING(BGl_string2558z00zz__evaluate_typesz00,
		BgL_bgl_string2558za700za7za7_2852za7, "prelock", 7);
	      DEFINE_STRING(BGl_string2478z00zz__evaluate_typesz00,
		BgL_bgl_string2478za700za7za7_2853za7, "ev_prog2", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2630z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1948za7622854z00,
		BGl_z62lambda1948z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2631z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1947za7622855z00,
		BGl_z62lambda1947z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2550z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1783za7622856z00,
		BGl_z62lambda1783z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2470z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1610za7622857z00,
		BGl_z62lambda1610z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2551z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1789za7622858z00,
		BGl_z62lambda1789z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2390z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2859za7,
		BGl_z62zc3z04anonymousza31457ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2471z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1609za7622860z00,
		BGl_z62lambda1609z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2552z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1788za7622861z00,
		BGl_z62lambda1788z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2634z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1953za7622862z00,
		BGl_z62lambda1953z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2391z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1456za7622863z00,
		BGl_z62lambda1456z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2635z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1952za7622864z00,
		BGl_z62lambda1952z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2392z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1455za7622865z00,
		BGl_z62lambda1455z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2474z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2866za7,
		BGl_z62zc3z04anonymousza31599ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2555z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1794za7622867z00,
		BGl_z62lambda1794z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2475z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1596za7622868z00,
		BGl_z62lambda1596z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2556z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1793za7622869z00,
		BGl_z62lambda1793z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2644z00zz__evaluate_typesz00,
		BgL_bgl_string2644za700za7za7_2870za7, "ev_goto", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2638z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1958za7622871z00,
		BGl_z62lambda1958z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2476z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1594za7622872z00,
		BGl_z62lambda1594z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2639z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1957za7622873z00,
		BGl_z62lambda1957z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2397z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1462za7622874z00,
		BGl_z62lambda1462z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2559z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1799za7622875z00,
		BGl_z62lambda1799z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2565z00zz__evaluate_typesz00,
		BgL_bgl_string2565za700za7za7_2876za7, "ev_synchronize", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2398z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1461za7622877z00,
		BGl_z62lambda1461z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2479z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1626za7622878z00,
		BGl_z62lambda1626z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2485z00zz__evaluate_typesz00,
		BgL_bgl_string2485za700za7za7_2879za7, "ev_hook", 7);
	      DEFINE_STRING(BGl_string2569z00zz__evaluate_typesz00,
		BgL_bgl_string2569za700za7za7_2880za7, "vars", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2640z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2881za7,
		BGl_z62zc3z04anonymousza31938ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2641z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1936za7622882z00,
		BGl_z62lambda1936z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2560z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1798za7622883z00,
		BGl_z62lambda1798z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2642z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1934za7622884z00,
		BGl_z62lambda1934z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2480z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1625za7622885z00,
		BGl_z62lambda1625z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2561z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2886za7,
		BGl_z62zc3z04anonymousza31778ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2481z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2887za7,
		BGl_z62zc3z04anonymousza31621ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2562z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1776za7622888z00,
		BGl_z62lambda1776z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2650z00zz__evaluate_typesz00,
		BgL_bgl_string2650za700za7za7_2889za7, "fun", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2482z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1619za7622890z00,
		BGl_z62lambda1619z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2563z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1774za7622891z00,
		BGl_z62lambda1774z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2645z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1973za7622892z00,
		BGl_z62lambda1973z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2483z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1617za7622893z00,
		BGl_z62lambda1617z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2646z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1972za7622894z00,
		BGl_z62lambda1972z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2490z00zz__evaluate_typesz00,
		BgL_bgl_string2490za700za7za7_2895za7, "ev_trap", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2647z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1978za7622896z00,
		BGl_z62lambda1978z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2566z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1814za7622897z00,
		BGl_z62lambda1814z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2648z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1977za7622898z00,
		BGl_z62lambda1977z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2486z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2899za7,
		BGl_z62zc3z04anonymousza31637ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2567z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1813za7622900z00,
		BGl_z62lambda1813z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2573z00zz__evaluate_typesz00,
		BgL_bgl_string2573za700za7za7_2901za7, "vals", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2487z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1635za7622902z00,
		BGl_z62lambda1635z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2656z00zz__evaluate_typesz00,
		BgL_bgl_string2656za700za7za7_2903za7, "tail?", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2488z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1632za7622904z00,
		BGl_z62lambda1632z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2494z00zz__evaluate_typesz00,
		BgL_bgl_string2494za700za7za7_2905za7, "v", 1);
	      DEFINE_STRING(BGl_string2499z00zz__evaluate_typesz00,
		BgL_bgl_string2499za700za7za7_2906za7, "ev_setlocal", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2651z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1983za7622907z00,
		BGl_z62lambda1983z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2570z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1819za7622908z00,
		BGl_z62lambda1819z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2652z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1982za7622909z00,
		BGl_z62lambda1982z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2571z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1818za7622910z00,
		BGl_z62lambda1818z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2653z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1988za7622911z00,
		BGl_z62lambda1988z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2491z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1652za7622912z00,
		BGl_z62lambda1652z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2654z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1987za7622913z00,
		BGl_z62lambda1987z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2492z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1651za7622914z00,
		BGl_z62lambda1651z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2661z00zz__evaluate_typesz00,
		BgL_bgl_string2661za700za7za7_2915za7, "ev_app", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2574z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1825za7622916z00,
		BGl_z62lambda1825z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2580z00zz__evaluate_typesz00,
		BgL_bgl_string2580za700za7za7_2917za7, "ev_binder", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2575z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1824za7622918z00,
		BGl_z62lambda1824z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2657z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2919za7,
		BGl_z62zc3z04anonymousza31968ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2495z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2920za7,
		BGl_z62zc3z04anonymousza31647ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2576z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2921za7,
		BGl_z62zc3z04anonymousza31809ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2658z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1966za7622922z00,
		BGl_z62lambda1966z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2496z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1645za7622923z00,
		BGl_z62lambda1645z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2577z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1807za7622924z00,
		BGl_z62lambda1807z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2659z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1964za7622925z00,
		BGl_z62lambda1964z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2497z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1643za7622926z00,
		BGl_z62lambda1643z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2578z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1805za7622927z00,
		BGl_z62lambda1805z62zz__evaluate_typesz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2585z00zz__evaluate_typesz00,
		BgL_bgl_string2585za700za7za7_2928za7, "boxes", 5);
	      DEFINE_STRING(BGl_string2667z00zz__evaluate_typesz00,
		BgL_bgl_string2667za700za7za7_2929za7, "where", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2662z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2003za7622930z00,
		BGl_z62lambda2003z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2581z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2931za7,
		BGl_z62zc3z04anonymousza31843ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2663z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2002za7622932z00,
		BGl_z62lambda2002z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2582z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1842za7622933z00,
		BGl_z62lambda1842z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2664z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2009za7622934z00,
		BGl_z62lambda2009z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2583z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1841za7622935z00,
		BGl_z62lambda1841z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2671z00zz__evaluate_typesz00,
		BgL_bgl_string2671za700za7za7_2936za7, "arity", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2665z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2008za7622937z00,
		BGl_z62lambda2008z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2590z00zz__evaluate_typesz00,
		BgL_bgl_string2590za700za7za7_2938za7, "ev_let", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2586z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2939za7,
		BGl_z62zc3z04anonymousza31836ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2668z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2014za7622940z00,
		BGl_z62lambda2014z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2587z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1834za7622941z00,
		BGl_z62lambda1834z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2669z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2013za7622942z00,
		BGl_z62lambda2013z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2588z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1832za7622943z00,
		BGl_z62lambda1832z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2598z00zz__evaluate_typesz00,
		BgL_bgl_string2598za700za7za7_2944za7, "ev_let*", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2672z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2019za7622945z00,
		BGl_z62lambda2019z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2591z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2946za7,
		BGl_z62zc3z04anonymousza31861ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2673z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2018za7622947z00,
		BGl_z62lambda2018z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2592z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1860za7622948z00,
		BGl_z62lambda1860z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2680z00zz__evaluate_typesz00,
		BgL_bgl_string2680za700za7za7_2949za7, "size", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2674z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2024za7622950z00,
		BGl_z62lambda2024z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2593z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1859za7622951z00,
		BGl_z62lambda1859z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2675z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2023za7622952z00,
		BGl_z62lambda2023z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2594z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2953za7,
		BGl_z62zc3z04anonymousza31853ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2682z00zz__evaluate_typesz00,
		BgL_bgl_string2682za700za7za7_2954za7, "int", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2676z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2955za7,
		BGl_z62zc3z04anonymousza32032ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2595z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1851za7622956z00,
		BGl_z62lambda1851z62zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2677z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2031za7622957z00,
		BGl_z62lambda2031z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2596z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda1849za7622958z00,
		BGl_z62lambda1849z62zz__evaluate_typesz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2678z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2030za7622959z00,
		BGl_z62lambda2030z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2599z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2960za7,
		BGl_z62zc3z04anonymousza31873ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2687z00zz__evaluate_typesz00,
		BgL_bgl_string2687za700za7za7_2961za7, "bind", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2683z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2962za7,
		BGl_z62zc3z04anonymousza32040ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2684z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2039za7622963z00,
		BGl_z62lambda2039z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2685z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2038za7622964z00,
		BGl_z62lambda2038z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2692z00zz__evaluate_typesz00,
		BgL_bgl_string2692za700za7za7_2965za7, "free", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2688z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2966za7,
		BGl_z62zc3z04anonymousza32048ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2689z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2047za7622967z00,
		BGl_z62lambda2047z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2697z00zz__evaluate_typesz00,
		BgL_bgl_string2697za700za7za7_2968za7, "inner", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2690z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2046za7622969z00,
		BGl_z62lambda2046z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2693z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2970za7,
		BGl_z62zc3z04anonymousza32055ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2694z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2054za7622971z00,
		BGl_z62lambda2054z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2695z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2053za7622972z00,
		BGl_z62lambda2053z62zz__evaluate_typesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2698z00zz__evaluate_typesz00,
		BgL_bgl_za762za7c3za704anonymo2973za7,
		BGl_z62zc3z04anonymousza32062ze3ze5zz__evaluate_typesz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2699z00zz__evaluate_typesz00,
		BgL_bgl_za762lambda2061za7622974z00,
		BGl_z62lambda2061z62zz__evaluate_typesz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2643z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2564z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2484z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2649z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2568z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2489z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_defglobalz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2572z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2655z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2493z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2498z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2579z00zz__evaluate_typesz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2660z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2584z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2666z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2589z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2670z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2597z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2679z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_letrecz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2681z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2686z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_letz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2691z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2696z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_prog2z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_bindzd2exitzd2zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_letza2za2zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_labelsz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_absz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_trapz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_setglobalz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_ifz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_hookz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_gotoz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_globalz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_binderz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_listz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_littz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_orz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_varz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_withzd2handlerzd2zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_andz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2404z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2408z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2414z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2419z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_exprz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_appz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2423z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2428z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2509z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2432z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2514z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2436z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2518z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_synchroniza7eza7zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2440z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2602z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2522z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2445z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2527z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2449z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2611z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2454z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2616z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2536z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2459z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2540z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2704z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2380z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2706z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2382z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2626z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2464z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2547z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2386z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2468z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2388z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_ev_setlocalz00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2632z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2472z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2553z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2636z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2393z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2395z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2557z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2477z00zz__evaluate_typesz00));
		     ADD_ROOT((void *) (&BGl_symbol2399z00zz__evaluate_typesz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long
		BgL_checksumz00_4404, char *BgL_fromz00_4405)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evaluate_typesz00))
				{
					BGl_requirezd2initializa7ationz75zz__evaluate_typesz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evaluate_typesz00();
					BGl_cnstzd2initzd2zz__evaluate_typesz00();
					BGl_importedzd2moduleszd2initz00zz__evaluate_typesz00();
					BGl_objectzd2initzd2zz__evaluate_typesz00();
					return BGl_toplevelzd2initzd2zz__evaluate_typesz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			BGl_symbol2380z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2381z00zz__evaluate_typesz00);
			BGl_symbol2382z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2383z00zz__evaluate_typesz00);
			BGl_symbol2386z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2387z00zz__evaluate_typesz00);
			BGl_symbol2388z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2389z00zz__evaluate_typesz00);
			BGl_symbol2393z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2394z00zz__evaluate_typesz00);
			BGl_symbol2395z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2396z00zz__evaluate_typesz00);
			BGl_symbol2399z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2400z00zz__evaluate_typesz00);
			BGl_symbol2404z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2405z00zz__evaluate_typesz00);
			BGl_symbol2408z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2409z00zz__evaluate_typesz00);
			BGl_symbol2414z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2415z00zz__evaluate_typesz00);
			BGl_symbol2419z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2420z00zz__evaluate_typesz00);
			BGl_symbol2423z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2424z00zz__evaluate_typesz00);
			BGl_symbol2428z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2429z00zz__evaluate_typesz00);
			BGl_symbol2432z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2433z00zz__evaluate_typesz00);
			BGl_symbol2436z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2437z00zz__evaluate_typesz00);
			BGl_symbol2440z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2441z00zz__evaluate_typesz00);
			BGl_symbol2445z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2446z00zz__evaluate_typesz00);
			BGl_symbol2449z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2450z00zz__evaluate_typesz00);
			BGl_symbol2454z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2455z00zz__evaluate_typesz00);
			BGl_symbol2459z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2460z00zz__evaluate_typesz00);
			BGl_symbol2464z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2465z00zz__evaluate_typesz00);
			BGl_symbol2468z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2469z00zz__evaluate_typesz00);
			BGl_symbol2472z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2473z00zz__evaluate_typesz00);
			BGl_symbol2477z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2478z00zz__evaluate_typesz00);
			BGl_symbol2484z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2485z00zz__evaluate_typesz00);
			BGl_symbol2489z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2490z00zz__evaluate_typesz00);
			BGl_symbol2493z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2494z00zz__evaluate_typesz00);
			BGl_symbol2498z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2499z00zz__evaluate_typesz00);
			BGl_symbol2509z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2510z00zz__evaluate_typesz00);
			BGl_symbol2514z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2515z00zz__evaluate_typesz00);
			BGl_symbol2518z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2519z00zz__evaluate_typesz00);
			BGl_symbol2522z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2523z00zz__evaluate_typesz00);
			BGl_symbol2527z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2528z00zz__evaluate_typesz00);
			BGl_symbol2536z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2537z00zz__evaluate_typesz00);
			BGl_symbol2540z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2541z00zz__evaluate_typesz00);
			BGl_symbol2547z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2548z00zz__evaluate_typesz00);
			BGl_symbol2553z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2554z00zz__evaluate_typesz00);
			BGl_symbol2557z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2558z00zz__evaluate_typesz00);
			BGl_symbol2564z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2565z00zz__evaluate_typesz00);
			BGl_symbol2568z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2569z00zz__evaluate_typesz00);
			BGl_symbol2572z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2573z00zz__evaluate_typesz00);
			BGl_symbol2579z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2580z00zz__evaluate_typesz00);
			BGl_symbol2584z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2585z00zz__evaluate_typesz00);
			BGl_symbol2589z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2590z00zz__evaluate_typesz00);
			BGl_symbol2597z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2598z00zz__evaluate_typesz00);
			BGl_symbol2602z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2603z00zz__evaluate_typesz00);
			BGl_symbol2611z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2612z00zz__evaluate_typesz00);
			BGl_symbol2616z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2617z00zz__evaluate_typesz00);
			BGl_symbol2626z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2627z00zz__evaluate_typesz00);
			BGl_symbol2632z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2633z00zz__evaluate_typesz00);
			BGl_symbol2636z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2637z00zz__evaluate_typesz00);
			BGl_symbol2643z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2644z00zz__evaluate_typesz00);
			BGl_symbol2649z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2650z00zz__evaluate_typesz00);
			BGl_symbol2655z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2656z00zz__evaluate_typesz00);
			BGl_symbol2660z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2661z00zz__evaluate_typesz00);
			BGl_symbol2666z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2667z00zz__evaluate_typesz00);
			BGl_symbol2670z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2671z00zz__evaluate_typesz00);
			BGl_symbol2679z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2680z00zz__evaluate_typesz00);
			BGl_symbol2681z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2682z00zz__evaluate_typesz00);
			BGl_symbol2686z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2687z00zz__evaluate_typesz00);
			BGl_symbol2691z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2692z00zz__evaluate_typesz00);
			BGl_symbol2696z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2697z00zz__evaluate_typesz00);
			BGl_symbol2704z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2705z00zz__evaluate_typesz00);
			return (BGl_symbol2706z00zz__evaluate_typesz00 =
				bstring_to_symbol(BGl_string2707z00zz__evaluate_typesz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			{	/* Eval/evaluate_types.scm 49 */
				obj_t BgL_arg1427z00_1117;
				obj_t BgL_arg1428z00_1118;

				{	/* Eval/evaluate_types.scm 49 */
					obj_t BgL_v1291z00_1128;

					BgL_v1291z00_1128 = create_vector(0L);
					BgL_arg1427z00_1117 = BgL_v1291z00_1128;
				}
				{	/* Eval/evaluate_types.scm 49 */
					obj_t BgL_v1292z00_1129;

					BgL_v1292z00_1129 = create_vector(0L);
					BgL_arg1428z00_1118 = BgL_v1292z00_1129;
				}
				BGl_ev_exprz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2380z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00, BGl_objectz00zz__objectz00,
					36682L, BGl_proc2379z00zz__evaluate_typesz00,
					BGl_proc2378z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2377z00zz__evaluate_typesz00, BFALSE, BgL_arg1427z00_1117,
					BgL_arg1428z00_1118);
			}
			{	/* Eval/evaluate_types.scm 51 */
				obj_t BgL_arg1439z00_1136;
				obj_t BgL_arg1440z00_1137;

				{	/* Eval/evaluate_types.scm 51 */
					obj_t BgL_v1293z00_1150;

					BgL_v1293z00_1150 = create_vector(3L);
					VECTOR_SET(BgL_v1293z00_1150, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2386z00zz__evaluate_typesz00,
							BGl_proc2385z00zz__evaluate_typesz00,
							BGl_proc2384z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2388z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1293z00_1150, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2393z00zz__evaluate_typesz00,
							BGl_proc2392z00zz__evaluate_typesz00,
							BGl_proc2391z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2390z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1293z00_1150, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2399z00zz__evaluate_typesz00,
							BGl_proc2398z00zz__evaluate_typesz00,
							BGl_proc2397z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1439z00_1136 = BgL_v1293z00_1150;
				}
				{	/* Eval/evaluate_types.scm 51 */
					obj_t BgL_v1294z00_1184;

					BgL_v1294z00_1184 = create_vector(0L);
					BgL_arg1440z00_1137 = BgL_v1294z00_1184;
				}
				BGl_ev_varz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2404z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 7285L,
					BGl_proc2403z00zz__evaluate_typesz00,
					BGl_proc2402z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2401z00zz__evaluate_typesz00, BFALSE, BgL_arg1439z00_1136,
					BgL_arg1440z00_1137);
			}
			{	/* Eval/evaluate_types.scm 55 */
				obj_t BgL_arg1466z00_1191;
				obj_t BgL_arg1467z00_1192;

				{	/* Eval/evaluate_types.scm 55 */
					obj_t BgL_v1295z00_1205;

					BgL_v1295z00_1205 = create_vector(3L);
					VECTOR_SET(BgL_v1295z00_1205, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2408z00zz__evaluate_typesz00,
							BGl_proc2407z00zz__evaluate_typesz00,
							BGl_proc2406z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1295z00_1205, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2386z00zz__evaluate_typesz00,
							BGl_proc2411z00zz__evaluate_typesz00,
							BGl_proc2410z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2388z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1295z00_1205, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2414z00zz__evaluate_typesz00,
							BGl_proc2413z00zz__evaluate_typesz00,
							BGl_proc2412z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1466z00_1191 = BgL_v1295z00_1205;
				}
				{	/* Eval/evaluate_types.scm 55 */
					obj_t BgL_v1296z00_1236;

					BgL_v1296z00_1236 = create_vector(0L);
					BgL_arg1467z00_1192 = BgL_v1296z00_1236;
				}
				BGl_ev_globalz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2419z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 59175L,
					BGl_proc2418z00zz__evaluate_typesz00,
					BGl_proc2417z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2416z00zz__evaluate_typesz00, BFALSE, BgL_arg1466z00_1191,
					BgL_arg1467z00_1192);
			}
			{	/* Eval/evaluate_types.scm 60 */
				obj_t BgL_arg1495z00_1243;
				obj_t BgL_arg1497z00_1244;

				{	/* Eval/evaluate_types.scm 60 */
					obj_t BgL_v1297z00_1255;

					BgL_v1297z00_1255 = create_vector(1L);
					VECTOR_SET(BgL_v1297z00_1255, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2423z00zz__evaluate_typesz00,
							BGl_proc2422z00zz__evaluate_typesz00,
							BGl_proc2421z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1495z00_1243 = BgL_v1297z00_1255;
				}
				{	/* Eval/evaluate_types.scm 60 */
					obj_t BgL_v1298z00_1266;

					BgL_v1298z00_1266 = create_vector(0L);
					BgL_arg1497z00_1244 = BgL_v1298z00_1266;
				}
				BGl_ev_littz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2428z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 42418L,
					BGl_proc2427z00zz__evaluate_typesz00,
					BGl_proc2426z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2425z00zz__evaluate_typesz00, BFALSE, BgL_arg1495z00_1243,
					BgL_arg1497z00_1244);
			}
			{	/* Eval/evaluate_types.scm 62 */
				obj_t BgL_arg1511z00_1273;
				obj_t BgL_arg1513z00_1274;

				{	/* Eval/evaluate_types.scm 62 */
					obj_t BgL_v1299z00_1287;

					BgL_v1299z00_1287 = create_vector(3L);
					VECTOR_SET(BgL_v1299z00_1287, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2432z00zz__evaluate_typesz00,
							BGl_proc2431z00zz__evaluate_typesz00,
							BGl_proc2430z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1299z00_1287, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2436z00zz__evaluate_typesz00,
							BGl_proc2435z00zz__evaluate_typesz00,
							BGl_proc2434z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1299z00_1287, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2440z00zz__evaluate_typesz00,
							BGl_proc2439z00zz__evaluate_typesz00,
							BGl_proc2438z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1511z00_1273 = BgL_v1299z00_1287;
				}
				{	/* Eval/evaluate_types.scm 62 */
					obj_t BgL_v1300z00_1318;

					BgL_v1300z00_1318 = create_vector(0L);
					BgL_arg1513z00_1274 = BgL_v1300z00_1318;
				}
				BGl_ev_ifz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2445z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 19099L,
					BGl_proc2444z00zz__evaluate_typesz00,
					BGl_proc2443z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2442z00zz__evaluate_typesz00, BFALSE, BgL_arg1511z00_1273,
					BgL_arg1513z00_1274);
			}
			{	/* Eval/evaluate_types.scm 66 */
				obj_t BgL_arg1546z00_1325;
				obj_t BgL_arg1547z00_1326;

				{	/* Eval/evaluate_types.scm 66 */
					obj_t BgL_v1301z00_1337;

					BgL_v1301z00_1337 = create_vector(1L);
					VECTOR_SET(BgL_v1301z00_1337, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2449z00zz__evaluate_typesz00,
							BGl_proc2448z00zz__evaluate_typesz00,
							BGl_proc2447z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1546z00_1325 = BgL_v1301z00_1337;
				}
				{	/* Eval/evaluate_types.scm 66 */
					obj_t BgL_v1302z00_1348;

					BgL_v1302z00_1348 = create_vector(0L);
					BgL_arg1547z00_1326 = BgL_v1302z00_1348;
				}
				BGl_ev_listz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2454z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 39617L,
					BGl_proc2453z00zz__evaluate_typesz00,
					BGl_proc2452z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2451z00zz__evaluate_typesz00, BFALSE, BgL_arg1546z00_1325,
					BgL_arg1547z00_1326);
			}
			{	/* Eval/evaluate_types.scm 68 */
				obj_t BgL_arg1564z00_1355;
				obj_t BgL_arg1565z00_1356;

				{	/* Eval/evaluate_types.scm 68 */
					obj_t BgL_v1303z00_1367;

					BgL_v1303z00_1367 = create_vector(0L);
					BgL_arg1564z00_1355 = BgL_v1303z00_1367;
				}
				{	/* Eval/evaluate_types.scm 68 */
					obj_t BgL_v1304z00_1368;

					BgL_v1304z00_1368 = create_vector(0L);
					BgL_arg1565z00_1356 = BgL_v1304z00_1368;
				}
				BGl_ev_orz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2459z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_listz00zz__evaluate_typesz00, 50334L,
					BGl_proc2458z00zz__evaluate_typesz00,
					BGl_proc2457z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2456z00zz__evaluate_typesz00, BFALSE, BgL_arg1564z00_1355,
					BgL_arg1565z00_1356);
			}
			{	/* Eval/evaluate_types.scm 69 */
				obj_t BgL_arg1578z00_1375;
				obj_t BgL_arg1579z00_1376;

				{	/* Eval/evaluate_types.scm 69 */
					obj_t BgL_v1305z00_1387;

					BgL_v1305z00_1387 = create_vector(0L);
					BgL_arg1578z00_1375 = BgL_v1305z00_1387;
				}
				{	/* Eval/evaluate_types.scm 69 */
					obj_t BgL_v1306z00_1388;

					BgL_v1306z00_1388 = create_vector(0L);
					BgL_arg1579z00_1376 = BgL_v1306z00_1388;
				}
				BGl_ev_andz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2464z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_listz00zz__evaluate_typesz00, 26888L,
					BGl_proc2463z00zz__evaluate_typesz00,
					BGl_proc2462z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2461z00zz__evaluate_typesz00, BFALSE, BgL_arg1578z00_1375,
					BgL_arg1579z00_1376);
			}
			{	/* Eval/evaluate_types.scm 70 */
				obj_t BgL_arg1591z00_1395;
				obj_t BgL_arg1593z00_1396;

				{	/* Eval/evaluate_types.scm 70 */
					obj_t BgL_v1307z00_1408;

					BgL_v1307z00_1408 = create_vector(2L);
					VECTOR_SET(BgL_v1307z00_1408, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2468z00zz__evaluate_typesz00,
							BGl_proc2467z00zz__evaluate_typesz00,
							BGl_proc2466z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1307z00_1408, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2472z00zz__evaluate_typesz00,
							BGl_proc2471z00zz__evaluate_typesz00,
							BGl_proc2470z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1591z00_1395 = BgL_v1307z00_1408;
				}
				{	/* Eval/evaluate_types.scm 70 */
					obj_t BgL_v1308z00_1429;

					BgL_v1308z00_1429 = create_vector(0L);
					BgL_arg1593z00_1396 = BgL_v1308z00_1429;
				}
				BGl_ev_prog2z00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2477z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 14177L,
					BGl_proc2476z00zz__evaluate_typesz00,
					BGl_proc2475z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2474z00zz__evaluate_typesz00, BFALSE, BgL_arg1591z00_1395,
					BgL_arg1593z00_1396);
			}
			{	/* Eval/evaluate_types.scm 73 */
				obj_t BgL_arg1615z00_1436;
				obj_t BgL_arg1616z00_1437;

				{	/* Eval/evaluate_types.scm 73 */
					obj_t BgL_v1309z00_1448;

					BgL_v1309z00_1448 = create_vector(1L);
					VECTOR_SET(BgL_v1309z00_1448, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2440z00zz__evaluate_typesz00,
							BGl_proc2480z00zz__evaluate_typesz00,
							BGl_proc2479z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1615z00_1436 = BgL_v1309z00_1448;
				}
				{	/* Eval/evaluate_types.scm 73 */
					obj_t BgL_v1310z00_1459;

					BgL_v1310z00_1459 = create_vector(0L);
					BgL_arg1616z00_1437 = BgL_v1310z00_1459;
				}
				BGl_ev_hookz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2484z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 32125L,
					BGl_proc2483z00zz__evaluate_typesz00,
					BGl_proc2482z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2481z00zz__evaluate_typesz00, BFALSE, BgL_arg1615z00_1436,
					BgL_arg1616z00_1437);
			}
			{	/* Eval/evaluate_types.scm 75 */
				obj_t BgL_arg1630z00_1466;
				obj_t BgL_arg1631z00_1467;

				{	/* Eval/evaluate_types.scm 75 */
					obj_t BgL_v1311z00_1478;

					BgL_v1311z00_1478 = create_vector(0L);
					BgL_arg1630z00_1466 = BgL_v1311z00_1478;
				}
				{	/* Eval/evaluate_types.scm 75 */
					obj_t BgL_v1312z00_1479;

					BgL_v1312z00_1479 = create_vector(0L);
					BgL_arg1631z00_1467 = BgL_v1312z00_1479;
				}
				BGl_ev_trapz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2489z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_hookz00zz__evaluate_typesz00, 24451L,
					BGl_proc2488z00zz__evaluate_typesz00,
					BGl_proc2487z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2486z00zz__evaluate_typesz00, BFALSE, BgL_arg1630z00_1466,
					BgL_arg1631z00_1467);
			}
			{	/* Eval/evaluate_types.scm 76 */
				obj_t BgL_arg1641z00_1486;
				obj_t BgL_arg1642z00_1487;

				{	/* Eval/evaluate_types.scm 76 */
					obj_t BgL_v1313z00_1499;

					BgL_v1313z00_1499 = create_vector(1L);
					VECTOR_SET(BgL_v1313z00_1499, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2493z00zz__evaluate_typesz00,
							BGl_proc2492z00zz__evaluate_typesz00,
							BGl_proc2491z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_varz00zz__evaluate_typesz00));
					BgL_arg1641z00_1486 = BgL_v1313z00_1499;
				}
				{	/* Eval/evaluate_types.scm 76 */
					obj_t BgL_v1314z00_1510;

					BgL_v1314z00_1510 = create_vector(0L);
					BgL_arg1642z00_1487 = BgL_v1314z00_1510;
				}
				BGl_ev_setlocalz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2498z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_hookz00zz__evaluate_typesz00, 37950L,
					BGl_proc2497z00zz__evaluate_typesz00,
					BGl_proc2496z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2495z00zz__evaluate_typesz00, BFALSE, BgL_arg1641z00_1486,
					BgL_arg1642z00_1487);
			}
			{	/* Eval/evaluate_types.scm 78 */
				obj_t BgL_arg1657z00_1517;
				obj_t BgL_arg1658z00_1518;

				{	/* Eval/evaluate_types.scm 78 */
					obj_t BgL_v1315z00_1532;

					BgL_v1315z00_1532 = create_vector(3L);
					VECTOR_SET(BgL_v1315z00_1532, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2408z00zz__evaluate_typesz00,
							BGl_proc2501z00zz__evaluate_typesz00,
							BGl_proc2500z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1315z00_1532, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2386z00zz__evaluate_typesz00,
							BGl_proc2503z00zz__evaluate_typesz00,
							BGl_proc2502z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2388z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1315z00_1532, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2414z00zz__evaluate_typesz00,
							BGl_proc2505z00zz__evaluate_typesz00,
							BGl_proc2504z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1657z00_1517 = BgL_v1315z00_1532;
				}
				{	/* Eval/evaluate_types.scm 78 */
					obj_t BgL_v1316z00_1563;

					BgL_v1316z00_1563 = create_vector(0L);
					BgL_arg1658z00_1518 = BgL_v1316z00_1563;
				}
				BGl_ev_setglobalz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2509z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_hookz00zz__evaluate_typesz00, 62117L,
					BGl_proc2508z00zz__evaluate_typesz00,
					BGl_proc2507z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2506z00zz__evaluate_typesz00, BFALSE, BgL_arg1657z00_1517,
					BgL_arg1658z00_1518);
			}
			{	/* Eval/evaluate_types.scm 82 */
				obj_t BgL_arg1692z00_1570;
				obj_t BgL_arg1699z00_1571;

				{	/* Eval/evaluate_types.scm 82 */
					obj_t BgL_v1317z00_1585;

					BgL_v1317z00_1585 = create_vector(0L);
					BgL_arg1692z00_1570 = BgL_v1317z00_1585;
				}
				{	/* Eval/evaluate_types.scm 82 */
					obj_t BgL_v1318z00_1586;

					BgL_v1318z00_1586 = create_vector(0L);
					BgL_arg1699z00_1571 = BgL_v1318z00_1586;
				}
				BGl_ev_defglobalz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2514z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_setglobalz00zz__evaluate_typesz00, 36158L,
					BGl_proc2513z00zz__evaluate_typesz00,
					BGl_proc2512z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2511z00zz__evaluate_typesz00, BFALSE, BgL_arg1692z00_1570,
					BgL_arg1699z00_1571);
			}
			{	/* Eval/evaluate_types.scm 83 */
				obj_t BgL_arg1708z00_1593;
				obj_t BgL_arg1709z00_1594;

				{	/* Eval/evaluate_types.scm 83 */
					obj_t BgL_v1319z00_1606;

					BgL_v1319z00_1606 = create_vector(2L);
					VECTOR_SET(BgL_v1319z00_1606, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2518z00zz__evaluate_typesz00,
							BGl_proc2517z00zz__evaluate_typesz00,
							BGl_proc2516z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_varz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1319z00_1606, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2521z00zz__evaluate_typesz00,
							BGl_proc2520z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1708z00_1593 = BgL_v1319z00_1606;
				}
				{	/* Eval/evaluate_types.scm 83 */
					obj_t BgL_v1320z00_1627;

					BgL_v1320z00_1627 = create_vector(0L);
					BgL_arg1709z00_1594 = BgL_v1320z00_1627;
				}
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2527z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 10723L,
					BGl_proc2526z00zz__evaluate_typesz00,
					BGl_proc2525z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2524z00zz__evaluate_typesz00, BFALSE, BgL_arg1708z00_1593,
					BgL_arg1709z00_1594);
			}
			{	/* Eval/evaluate_types.scm 86 */
				obj_t BgL_arg1731z00_1634;
				obj_t BgL_arg1733z00_1635;

				{	/* Eval/evaluate_types.scm 86 */
					obj_t BgL_v1321z00_1647;

					BgL_v1321z00_1647 = create_vector(2L);
					VECTOR_SET(BgL_v1321z00_1647, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2440z00zz__evaluate_typesz00,
							BGl_proc2530z00zz__evaluate_typesz00,
							BGl_proc2529z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1321z00_1647, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2532z00zz__evaluate_typesz00,
							BGl_proc2531z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1731z00_1634 = BgL_v1321z00_1647;
				}
				{	/* Eval/evaluate_types.scm 86 */
					obj_t BgL_v1322z00_1668;

					BgL_v1322z00_1668 = create_vector(0L);
					BgL_arg1733z00_1635 = BgL_v1322z00_1668;
				}
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2536z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 59644L,
					BGl_proc2535z00zz__evaluate_typesz00,
					BGl_proc2534z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2533z00zz__evaluate_typesz00, BFALSE, BgL_arg1731z00_1634,
					BgL_arg1733z00_1635);
			}
			{	/* Eval/evaluate_types.scm 89 */
				obj_t BgL_arg1752z00_1675;
				obj_t BgL_arg1753z00_1676;

				{	/* Eval/evaluate_types.scm 89 */
					obj_t BgL_v1323z00_1688;

					BgL_v1323z00_1688 = create_vector(2L);
					VECTOR_SET(BgL_v1323z00_1688, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2540z00zz__evaluate_typesz00,
							BGl_proc2539z00zz__evaluate_typesz00,
							BGl_proc2538z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1323z00_1688, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2543z00zz__evaluate_typesz00,
							BGl_proc2542z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1752z00_1675 = BgL_v1323z00_1688;
				}
				{	/* Eval/evaluate_types.scm 89 */
					obj_t BgL_v1324z00_1709;

					BgL_v1324z00_1709 = create_vector(0L);
					BgL_arg1753z00_1676 = BgL_v1324z00_1709;
				}
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2547z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 54033L,
					BGl_proc2546z00zz__evaluate_typesz00,
					BGl_proc2545z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2544z00zz__evaluate_typesz00, BFALSE, BgL_arg1752z00_1675,
					BgL_arg1753z00_1676);
			}
			{	/* Eval/evaluate_types.scm 92 */
				obj_t BgL_arg1772z00_1716;
				obj_t BgL_arg1773z00_1717;

				{	/* Eval/evaluate_types.scm 92 */
					obj_t BgL_v1325z00_1731;

					BgL_v1325z00_1731 = create_vector(4L);
					VECTOR_SET(BgL_v1325z00_1731, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2408z00zz__evaluate_typesz00,
							BGl_proc2550z00zz__evaluate_typesz00,
							BGl_proc2549z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1325z00_1731, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2553z00zz__evaluate_typesz00,
							BGl_proc2552z00zz__evaluate_typesz00,
							BGl_proc2551z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1325z00_1731, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2557z00zz__evaluate_typesz00,
							BGl_proc2556z00zz__evaluate_typesz00,
							BGl_proc2555z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1325z00_1731, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2560z00zz__evaluate_typesz00,
							BGl_proc2559z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1772z00_1716 = BgL_v1325z00_1731;
				}
				{	/* Eval/evaluate_types.scm 92 */
					obj_t BgL_v1326z00_1772;

					BgL_v1326z00_1772 = create_vector(0L);
					BgL_arg1773z00_1717 = BgL_v1326z00_1772;
				}
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2564z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 40653L,
					BGl_proc2563z00zz__evaluate_typesz00,
					BGl_proc2562z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2561z00zz__evaluate_typesz00, BFALSE, BgL_arg1772z00_1716,
					BgL_arg1773z00_1717);
			}
			{	/* Eval/evaluate_types.scm 97 */
				obj_t BgL_arg1803z00_1779;
				obj_t BgL_arg1804z00_1780;

				{	/* Eval/evaluate_types.scm 97 */
					obj_t BgL_v1327z00_1793;

					BgL_v1327z00_1793 = create_vector(3L);
					VECTOR_SET(BgL_v1327z00_1793, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2568z00zz__evaluate_typesz00,
							BGl_proc2567z00zz__evaluate_typesz00,
							BGl_proc2566z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1327z00_1793, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2572z00zz__evaluate_typesz00,
							BGl_proc2571z00zz__evaluate_typesz00,
							BGl_proc2570z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1327z00_1793, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2575z00zz__evaluate_typesz00,
							BGl_proc2574z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					BgL_arg1803z00_1779 = BgL_v1327z00_1793;
				}
				{	/* Eval/evaluate_types.scm 97 */
					obj_t BgL_v1328z00_1824;

					BgL_v1328z00_1824 = create_vector(0L);
					BgL_arg1804z00_1780 = BgL_v1328z00_1824;
				}
				BGl_ev_binderz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2579z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 50341L,
					BGl_proc2578z00zz__evaluate_typesz00,
					BGl_proc2577z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2576z00zz__evaluate_typesz00, BFALSE, BgL_arg1803z00_1779,
					BgL_arg1804z00_1780);
			}
			{	/* Eval/evaluate_types.scm 101 */
				obj_t BgL_arg1829z00_1831;
				obj_t BgL_arg1831z00_1832;

				{	/* Eval/evaluate_types.scm 101 */
					obj_t BgL_v1329z00_1846;

					BgL_v1329z00_1846 = create_vector(1L);
					VECTOR_SET(BgL_v1329z00_1846, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2584z00zz__evaluate_typesz00,
							BGl_proc2583z00zz__evaluate_typesz00,
							BGl_proc2582z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2581z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1829z00_1831 = BgL_v1329z00_1846;
				}
				{	/* Eval/evaluate_types.scm 101 */
					obj_t BgL_v1330z00_1860;

					BgL_v1330z00_1860 = create_vector(0L);
					BgL_arg1831z00_1832 = BgL_v1330z00_1860;
				}
				BGl_ev_letz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2589z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_binderz00zz__evaluate_typesz00, 20484L,
					BGl_proc2588z00zz__evaluate_typesz00,
					BGl_proc2587z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2586z00zz__evaluate_typesz00, BFALSE, BgL_arg1829z00_1831,
					BgL_arg1831z00_1832);
			}
			{	/* Eval/evaluate_types.scm 103 */
				obj_t BgL_arg1847z00_1867;
				obj_t BgL_arg1848z00_1868;

				{	/* Eval/evaluate_types.scm 103 */
					obj_t BgL_v1331z00_1882;

					BgL_v1331z00_1882 = create_vector(1L);
					VECTOR_SET(BgL_v1331z00_1882, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2584z00zz__evaluate_typesz00,
							BGl_proc2593z00zz__evaluate_typesz00,
							BGl_proc2592z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2591z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1847z00_1867 = BgL_v1331z00_1882;
				}
				{	/* Eval/evaluate_types.scm 103 */
					obj_t BgL_v1332z00_1896;

					BgL_v1332z00_1896 = create_vector(0L);
					BgL_arg1848z00_1868 = BgL_v1332z00_1896;
				}
				BGl_ev_letza2za2zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2597z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_binderz00zz__evaluate_typesz00, 9906L,
					BGl_proc2596z00zz__evaluate_typesz00,
					BGl_proc2595z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2594z00zz__evaluate_typesz00, BFALSE, BgL_arg1847z00_1867,
					BgL_arg1848z00_1868);
			}
			{	/* Eval/evaluate_types.scm 105 */
				obj_t BgL_arg1866z00_1903;
				obj_t BgL_arg1868z00_1904;

				{	/* Eval/evaluate_types.scm 105 */
					obj_t BgL_v1333z00_1917;

					BgL_v1333z00_1917 = create_vector(0L);
					BgL_arg1866z00_1903 = BgL_v1333z00_1917;
				}
				{	/* Eval/evaluate_types.scm 105 */
					obj_t BgL_v1334z00_1918;

					BgL_v1334z00_1918 = create_vector(0L);
					BgL_arg1868z00_1904 = BgL_v1334z00_1918;
				}
				BGl_ev_letrecz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2602z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_binderz00zz__evaluate_typesz00, 43956L,
					BGl_proc2601z00zz__evaluate_typesz00,
					BGl_proc2600z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2599z00zz__evaluate_typesz00, BFALSE, BgL_arg1866z00_1903,
					BgL_arg1868z00_1904);
			}
			{	/* Eval/evaluate_types.scm 106 */
				obj_t BgL_arg1877z00_1925;
				obj_t BgL_arg1878z00_1926;

				{	/* Eval/evaluate_types.scm 106 */
					obj_t BgL_v1335z00_1942;

					BgL_v1335z00_1942 = create_vector(6L);
					VECTOR_SET(BgL_v1335z00_1942, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2568z00zz__evaluate_typesz00,
							BGl_proc2605z00zz__evaluate_typesz00,
							BGl_proc2604z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1335z00_1942, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2572z00zz__evaluate_typesz00,
							BGl_proc2607z00zz__evaluate_typesz00,
							BGl_proc2606z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1335z00_1942, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2611z00zz__evaluate_typesz00,
							BGl_proc2610z00zz__evaluate_typesz00,
							BGl_proc2609z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2608z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1335z00_1942, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2616z00zz__evaluate_typesz00,
							BGl_proc2615z00zz__evaluate_typesz00,
							BGl_proc2614z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2613z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1335z00_1942, 4L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2619z00zz__evaluate_typesz00,
							BGl_proc2618z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1335z00_1942, 5L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2584z00zz__evaluate_typesz00,
							BGl_proc2622z00zz__evaluate_typesz00,
							BGl_proc2621z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2620z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1877z00_1925 = BgL_v1335z00_1942;
				}
				{	/* Eval/evaluate_types.scm 106 */
					obj_t BgL_v1336z00_2012;

					BgL_v1336z00_2012 = create_vector(0L);
					BgL_arg1878z00_1926 = BgL_v1336z00_2012;
				}
				BGl_ev_labelsz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2626z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 51941L,
					BGl_proc2625z00zz__evaluate_typesz00,
					BGl_proc2624z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2623z00zz__evaluate_typesz00, BFALSE, BgL_arg1877z00_1925,
					BgL_arg1878z00_1926);
			}
			{	/* Eval/evaluate_types.scm 114 */
				obj_t BgL_arg1932z00_2019;
				obj_t BgL_arg1933z00_2020;

				{	/* Eval/evaluate_types.scm 114 */
					obj_t BgL_v1337z00_2034;

					BgL_v1337z00_2034 = create_vector(4L);
					VECTOR_SET(BgL_v1337z00_2034, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2408z00zz__evaluate_typesz00,
							BGl_proc2629z00zz__evaluate_typesz00,
							BGl_proc2628z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1337z00_2034, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2632z00zz__evaluate_typesz00,
							BGl_proc2631z00zz__evaluate_typesz00,
							BGl_proc2630z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_varz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1337z00_2034, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2636z00zz__evaluate_typesz00,
							BGl_proc2635z00zz__evaluate_typesz00,
							BGl_proc2634z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_labelsz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1337z00_2034, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2449z00zz__evaluate_typesz00,
							BGl_proc2639z00zz__evaluate_typesz00,
							BGl_proc2638z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1932z00_2019 = BgL_v1337z00_2034;
				}
				{	/* Eval/evaluate_types.scm 114 */
					obj_t BgL_v1338z00_2075;

					BgL_v1338z00_2075 = create_vector(0L);
					BgL_arg1933z00_2020 = BgL_v1338z00_2075;
				}
				BGl_ev_gotoz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2643z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 25746L,
					BGl_proc2642z00zz__evaluate_typesz00,
					BGl_proc2641z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2640z00zz__evaluate_typesz00, BFALSE, BgL_arg1932z00_2019,
					BgL_arg1933z00_2020);
			}
			{	/* Eval/evaluate_types.scm 119 */
				obj_t BgL_arg1962z00_2082;
				obj_t BgL_arg1963z00_2083;

				{	/* Eval/evaluate_types.scm 119 */
					obj_t BgL_v1339z00_2097;

					BgL_v1339z00_2097 = create_vector(4L);
					VECTOR_SET(BgL_v1339z00_2097, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2408z00zz__evaluate_typesz00,
							BGl_proc2646z00zz__evaluate_typesz00,
							BGl_proc2645z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1339z00_2097, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2649z00zz__evaluate_typesz00,
							BGl_proc2648z00zz__evaluate_typesz00,
							BGl_proc2647z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1339z00_2097, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2449z00zz__evaluate_typesz00,
							BGl_proc2652z00zz__evaluate_typesz00,
							BGl_proc2651z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1339z00_2097, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2655z00zz__evaluate_typesz00,
							BGl_proc2654z00zz__evaluate_typesz00,
							BGl_proc2653z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1962z00_2082 = BgL_v1339z00_2097;
				}
				{	/* Eval/evaluate_types.scm 119 */
					obj_t BgL_v1340z00_2138;

					BgL_v1340z00_2138 = create_vector(0L);
					BgL_arg1963z00_2083 = BgL_v1340z00_2138;
				}
				BGl_ev_appz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2660z00zz__evaluate_typesz00,
					BGl_symbol2382z00zz__evaluate_typesz00,
					BGl_ev_exprz00zz__evaluate_typesz00, 55822L,
					BGl_proc2659z00zz__evaluate_typesz00,
					BGl_proc2658z00zz__evaluate_typesz00, BFALSE,
					BGl_proc2657z00zz__evaluate_typesz00, BFALSE, BgL_arg1962z00_2082,
					BgL_arg1963z00_2083);
			}
			{	/* Eval/evaluate_types.scm 122 */
				obj_t BgL_arg1992z00_2145;
				obj_t BgL_arg1993z00_2146;

				{	/* Eval/evaluate_types.scm 122 */
					obj_t BgL_v1341z00_2166;

					BgL_v1341z00_2166 = create_vector(10L);
					VECTOR_SET(BgL_v1341z00_2166, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2408z00zz__evaluate_typesz00,
							BGl_proc2663z00zz__evaluate_typesz00,
							BGl_proc2662z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2666z00zz__evaluate_typesz00,
							BGl_proc2665z00zz__evaluate_typesz00,
							BGl_proc2664z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2670z00zz__evaluate_typesz00,
							BGl_proc2669z00zz__evaluate_typesz00,
							BGl_proc2668z00zz__evaluate_typesz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2568z00zz__evaluate_typesz00,
							BGl_proc2673z00zz__evaluate_typesz00,
							BGl_proc2672z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 4L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2522z00zz__evaluate_typesz00,
							BGl_proc2675z00zz__evaluate_typesz00,
							BGl_proc2674z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_ev_exprz00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 5L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2679z00zz__evaluate_typesz00,
							BGl_proc2678z00zz__evaluate_typesz00,
							BGl_proc2677z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2676z00zz__evaluate_typesz00,
							BGl_symbol2681z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 6L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2686z00zz__evaluate_typesz00,
							BGl_proc2685z00zz__evaluate_typesz00,
							BGl_proc2684z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2683z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 7L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2691z00zz__evaluate_typesz00,
							BGl_proc2690z00zz__evaluate_typesz00,
							BGl_proc2689z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2688z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 8L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2696z00zz__evaluate_typesz00,
							BGl_proc2695z00zz__evaluate_typesz00,
							BGl_proc2694z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2693z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					VECTOR_SET(BgL_v1341z00_2166, 9L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2584z00zz__evaluate_typesz00,
							BGl_proc2700z00zz__evaluate_typesz00,
							BGl_proc2699z00zz__evaluate_typesz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2698z00zz__evaluate_typesz00,
							BGl_symbol2395z00zz__evaluate_typesz00));
					BgL_arg1992z00_2145 = BgL_v1341z00_2166;
				}
				{	/* Eval/evaluate_types.scm 122 */
					obj_t BgL_v1342z00_2282;

					BgL_v1342z00_2282 = create_vector(0L);
					BgL_arg1993z00_2146 = BgL_v1342z00_2282;
				}
				return (BGl_ev_absz00zz__evaluate_typesz00 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2704z00zz__evaluate_typesz00,
						BGl_symbol2382z00zz__evaluate_typesz00,
						BGl_ev_exprz00zz__evaluate_typesz00, 38362L,
						BGl_proc2703z00zz__evaluate_typesz00,
						BGl_proc2702z00zz__evaluate_typesz00, BFALSE,
						BGl_proc2701z00zz__evaluate_typesz00, BFALSE, BgL_arg1992z00_2145,
						BgL_arg1993z00_2146), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1998> */
	obj_t BGl_z62zc3z04anonymousza31998ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3477, obj_t BgL_new1193z00_3478)
	{
		{	/* Eval/evaluate_types.scm 122 */
			{
				BgL_ev_absz00_bglt BgL_auxz00_4671;

				((((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt) BgL_new1193z00_3478)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_wherez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_arityz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_varsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_4680;

					{	/* Eval/evaluate_types.scm 122 */
						obj_t BgL_classz00_4017;

						BgL_classz00_4017 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 122 */
							obj_t BgL__ortest_1290z00_4018;

							BgL__ortest_1290z00_4018 = BGL_CLASS_NIL(BgL_classz00_4017);
							if (CBOOL(BgL__ortest_1290z00_4018))
								{	/* Eval/evaluate_types.scm 122 */
									BgL_auxz00_4680 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4018);
								}
							else
								{	/* Eval/evaluate_types.scm 122 */
									BgL_auxz00_4680 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4017));
								}
						}
					}
					((((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_new1193z00_3478)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_4680), BUNSPEC);
				}
				((((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt) BgL_new1193z00_3478)))->BgL_siza7eza7) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_bindz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_freez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_innerz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt)
									BgL_new1193z00_3478)))->BgL_boxesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_4671 = ((BgL_ev_absz00_bglt) BgL_new1193z00_3478);
				return ((obj_t) BgL_auxz00_4671);
			}
		}

	}



/* &lambda1996 */
	BgL_ev_absz00_bglt BGl_z62lambda1996z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3479)
	{
		{	/* Eval/evaluate_types.scm 122 */
			{	/* Eval/evaluate_types.scm 122 */
				BgL_ev_absz00_bglt BgL_new1192z00_4019;

				BgL_new1192z00_4019 =
					((BgL_ev_absz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_absz00_bgl))));
				{	/* Eval/evaluate_types.scm 122 */
					long BgL_arg1997z00_4020;

					BgL_arg1997z00_4020 =
						BGL_CLASS_NUM(BGl_ev_absz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1192z00_4019), BgL_arg1997z00_4020);
				}
				return BgL_new1192z00_4019;
			}
		}

	}



/* &lambda1994 */
	BgL_ev_absz00_bglt BGl_z62lambda1994z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3480, obj_t BgL_loc1182z00_3481, obj_t BgL_where1183z00_3482,
		obj_t BgL_arity1184z00_3483, obj_t BgL_vars1185z00_3484,
		obj_t BgL_body1186z00_3485, obj_t BgL_siza7e1187za7_3486,
		obj_t BgL_bind1188z00_3487, obj_t BgL_free1189z00_3488,
		obj_t BgL_inner1190z00_3489, obj_t BgL_boxes1191z00_3490)
	{
		{	/* Eval/evaluate_types.scm 122 */
			{	/* Eval/evaluate_types.scm 122 */
				int BgL_siza7e1187za7_4022;

				BgL_siza7e1187za7_4022 = CINT(BgL_siza7e1187za7_3486);
				{	/* Eval/evaluate_types.scm 122 */
					BgL_ev_absz00_bglt BgL_new1249z00_4023;

					{	/* Eval/evaluate_types.scm 122 */
						BgL_ev_absz00_bglt BgL_new1248z00_4024;

						BgL_new1248z00_4024 =
							((BgL_ev_absz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_ev_absz00_bgl))));
						{	/* Eval/evaluate_types.scm 122 */
							long BgL_arg1995z00_4025;

							BgL_arg1995z00_4025 =
								BGL_CLASS_NUM(BGl_ev_absz00zz__evaluate_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1248z00_4024),
								BgL_arg1995z00_4025);
						}
						BgL_new1249z00_4023 = BgL_new1248z00_4024;
					}
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_locz00) =
						((obj_t) BgL_loc1182z00_3481), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_wherez00) =
						((obj_t) BgL_where1183z00_3482), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_arityz00) =
						((obj_t) BgL_arity1184z00_3483), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_varsz00) =
						((obj_t) BgL_vars1185z00_3484), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt)
								BgL_body1186z00_3485)), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->
							BgL_siza7eza7) = ((int) BgL_siza7e1187za7_4022), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_bindz00) =
						((obj_t) BgL_bind1188z00_3487), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_freez00) =
						((obj_t) BgL_free1189z00_3488), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_innerz00) =
						((obj_t) BgL_inner1190z00_3489), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1249z00_4023))->BgL_boxesz00) =
						((obj_t) BgL_boxes1191z00_3490), BUNSPEC);
					return BgL_new1249z00_4023;
				}
			}
		}

	}



/* &<@anonymous:2062> */
	obj_t BGl_z62zc3z04anonymousza32062ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3491)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return BNIL;
		}

	}



/* &lambda2061 */
	obj_t BGl_z62lambda2061z62zz__evaluate_typesz00(obj_t BgL_envz00_3492,
		obj_t BgL_oz00_3493, obj_t BgL_vz00_3494)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3493)))->BgL_boxesz00) =
				((obj_t) BgL_vz00_3494), BUNSPEC);
		}

	}



/* &lambda2060 */
	obj_t BGl_z62lambda2060z62zz__evaluate_typesz00(obj_t BgL_envz00_3495,
		obj_t BgL_oz00_3496)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3496)))->BgL_boxesz00);
		}

	}



/* &<@anonymous:2055> */
	obj_t BGl_z62zc3z04anonymousza32055ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3497)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return BNIL;
		}

	}



/* &lambda2054 */
	obj_t BGl_z62lambda2054z62zz__evaluate_typesz00(obj_t BgL_envz00_3498,
		obj_t BgL_oz00_3499, obj_t BgL_vz00_3500)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3499)))->BgL_innerz00) =
				((obj_t) BgL_vz00_3500), BUNSPEC);
		}

	}



/* &lambda2053 */
	obj_t BGl_z62lambda2053z62zz__evaluate_typesz00(obj_t BgL_envz00_3501,
		obj_t BgL_oz00_3502)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3502)))->BgL_innerz00);
		}

	}



/* &<@anonymous:2048> */
	obj_t BGl_z62zc3z04anonymousza32048ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3503)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return BNIL;
		}

	}



/* &lambda2047 */
	obj_t BGl_z62lambda2047z62zz__evaluate_typesz00(obj_t BgL_envz00_3504,
		obj_t BgL_oz00_3505, obj_t BgL_vz00_3506)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3505)))->BgL_freez00) =
				((obj_t) BgL_vz00_3506), BUNSPEC);
		}

	}



/* &lambda2046 */
	obj_t BGl_z62lambda2046z62zz__evaluate_typesz00(obj_t BgL_envz00_3507,
		obj_t BgL_oz00_3508)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3508)))->BgL_freez00);
		}

	}



/* &<@anonymous:2040> */
	obj_t BGl_z62zc3z04anonymousza32040ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3509)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return BNIL;
		}

	}



/* &lambda2039 */
	obj_t BGl_z62lambda2039z62zz__evaluate_typesz00(obj_t BgL_envz00_3510,
		obj_t BgL_oz00_3511, obj_t BgL_vz00_3512)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3511)))->BgL_bindz00) =
				((obj_t) BgL_vz00_3512), BUNSPEC);
		}

	}



/* &lambda2038 */
	obj_t BGl_z62lambda2038z62zz__evaluate_typesz00(obj_t BgL_envz00_3513,
		obj_t BgL_oz00_3514)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3514)))->BgL_bindz00);
		}

	}



/* &<@anonymous:2032> */
	obj_t BGl_z62zc3z04anonymousza32032ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3515)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return BINT(0L);
		}

	}



/* &lambda2031 */
	obj_t BGl_z62lambda2031z62zz__evaluate_typesz00(obj_t BgL_envz00_3516,
		obj_t BgL_oz00_3517, obj_t BgL_vz00_3518)
	{
		{	/* Eval/evaluate_types.scm 122 */
			{	/* Eval/evaluate_types.scm 122 */
				int BgL_vz00_4035;

				BgL_vz00_4035 = CINT(BgL_vz00_3518);
				return
					((((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt) BgL_oz00_3517)))->BgL_siza7eza7) =
					((int) BgL_vz00_4035), BUNSPEC);
		}}

	}



/* &lambda2030 */
	obj_t BGl_z62lambda2030z62zz__evaluate_typesz00(obj_t BgL_envz00_3519,
		obj_t BgL_oz00_3520)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				BINT(
				(((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3520)))->BgL_siza7eza7));
		}

	}



/* &lambda2024 */
	obj_t BGl_z62lambda2024z62zz__evaluate_typesz00(obj_t BgL_envz00_3521,
		obj_t BgL_oz00_3522, obj_t BgL_vz00_3523)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3522)))->BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3523)), BUNSPEC);
		}

	}



/* &lambda2023 */
	BgL_ev_exprz00_bglt BGl_z62lambda2023z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3524, obj_t BgL_oz00_3525)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3525)))->BgL_bodyz00);
		}

	}



/* &lambda2019 */
	obj_t BGl_z62lambda2019z62zz__evaluate_typesz00(obj_t BgL_envz00_3526,
		obj_t BgL_oz00_3527, obj_t BgL_vz00_3528)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3527)))->BgL_varsz00) =
				((obj_t) BgL_vz00_3528), BUNSPEC);
		}

	}



/* &lambda2018 */
	obj_t BGl_z62lambda2018z62zz__evaluate_typesz00(obj_t BgL_envz00_3529,
		obj_t BgL_oz00_3530)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3530)))->BgL_varsz00);
		}

	}



/* &lambda2014 */
	obj_t BGl_z62lambda2014z62zz__evaluate_typesz00(obj_t BgL_envz00_3531,
		obj_t BgL_oz00_3532, obj_t BgL_vz00_3533)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3532)))->BgL_arityz00) =
				((obj_t) BgL_vz00_3533), BUNSPEC);
		}

	}



/* &lambda2013 */
	obj_t BGl_z62lambda2013z62zz__evaluate_typesz00(obj_t BgL_envz00_3534,
		obj_t BgL_oz00_3535)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3535)))->BgL_arityz00);
		}

	}



/* &lambda2009 */
	obj_t BGl_z62lambda2009z62zz__evaluate_typesz00(obj_t BgL_envz00_3536,
		obj_t BgL_oz00_3537, obj_t BgL_vz00_3538)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3537)))->BgL_wherez00) =
				((obj_t) BgL_vz00_3538), BUNSPEC);
		}

	}



/* &lambda2008 */
	obj_t BGl_z62lambda2008z62zz__evaluate_typesz00(obj_t BgL_envz00_3539,
		obj_t BgL_oz00_3540)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3540)))->BgL_wherez00);
		}

	}



/* &lambda2003 */
	obj_t BGl_z62lambda2003z62zz__evaluate_typesz00(obj_t BgL_envz00_3541,
		obj_t BgL_oz00_3542, obj_t BgL_vz00_3543)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				((((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_oz00_3542)))->BgL_locz00) =
				((obj_t) BgL_vz00_3543), BUNSPEC);
		}

	}



/* &lambda2002 */
	obj_t BGl_z62lambda2002z62zz__evaluate_typesz00(obj_t BgL_envz00_3544,
		obj_t BgL_oz00_3545)
	{
		{	/* Eval/evaluate_types.scm 122 */
			return
				(((BgL_ev_absz00_bglt) COBJECT(
						((BgL_ev_absz00_bglt) BgL_oz00_3545)))->BgL_locz00);
		}

	}



/* &<@anonymous:1968> */
	obj_t BGl_z62zc3z04anonymousza31968ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3546, obj_t BgL_new1180z00_3547)
	{
		{	/* Eval/evaluate_types.scm 119 */
			{
				BgL_ev_appz00_bglt BgL_auxz00_4766;

				((((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_new1180z00_3547)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_4769;

					{	/* Eval/evaluate_types.scm 119 */
						obj_t BgL_classz00_4049;

						BgL_classz00_4049 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 119 */
							obj_t BgL__ortest_1290z00_4050;

							BgL__ortest_1290z00_4050 = BGL_CLASS_NIL(BgL_classz00_4049);
							if (CBOOL(BgL__ortest_1290z00_4050))
								{	/* Eval/evaluate_types.scm 119 */
									BgL_auxz00_4769 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4050);
								}
							else
								{	/* Eval/evaluate_types.scm 119 */
									BgL_auxz00_4769 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4049));
								}
						}
					}
					((((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_new1180z00_3547)))->BgL_funz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_4769), BUNSPEC);
				}
				((((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_new1180z00_3547)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_appz00_bglt) COBJECT(((BgL_ev_appz00_bglt)
									BgL_new1180z00_3547)))->BgL_tailzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_4766 = ((BgL_ev_appz00_bglt) BgL_new1180z00_3547);
				return ((obj_t) BgL_auxz00_4766);
			}
		}

	}



/* &lambda1966 */
	BgL_ev_appz00_bglt BGl_z62lambda1966z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3548)
	{
		{	/* Eval/evaluate_types.scm 119 */
			{	/* Eval/evaluate_types.scm 119 */
				BgL_ev_appz00_bglt BgL_new1179z00_4051;

				BgL_new1179z00_4051 =
					((BgL_ev_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_appz00_bgl))));
				{	/* Eval/evaluate_types.scm 119 */
					long BgL_arg1967z00_4052;

					BgL_arg1967z00_4052 =
						BGL_CLASS_NUM(BGl_ev_appz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1179z00_4051), BgL_arg1967z00_4052);
				}
				return BgL_new1179z00_4051;
			}
		}

	}



/* &lambda1964 */
	BgL_ev_appz00_bglt BGl_z62lambda1964z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3549, obj_t BgL_loc1175z00_3550, obj_t BgL_fun1176z00_3551,
		obj_t BgL_args1177z00_3552, obj_t BgL_tailzf31178zf3_3553)
	{
		{	/* Eval/evaluate_types.scm 119 */
			{	/* Eval/evaluate_types.scm 119 */
				BgL_ev_appz00_bglt BgL_new1247z00_4054;

				{	/* Eval/evaluate_types.scm 119 */
					BgL_ev_appz00_bglt BgL_new1246z00_4055;

					BgL_new1246z00_4055 =
						((BgL_ev_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_appz00_bgl))));
					{	/* Eval/evaluate_types.scm 119 */
						long BgL_arg1965z00_4056;

						BgL_arg1965z00_4056 =
							BGL_CLASS_NUM(BGl_ev_appz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1246z00_4055), BgL_arg1965z00_4056);
					}
					BgL_new1247z00_4054 = BgL_new1246z00_4055;
				}
				((((BgL_ev_appz00_bglt) COBJECT(BgL_new1247z00_4054))->BgL_locz00) =
					((obj_t) BgL_loc1175z00_3550), BUNSPEC);
				((((BgL_ev_appz00_bglt) COBJECT(BgL_new1247z00_4054))->BgL_funz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_fun1176z00_3551)),
					BUNSPEC);
				((((BgL_ev_appz00_bglt) COBJECT(BgL_new1247z00_4054))->BgL_argsz00) =
					((obj_t) BgL_args1177z00_3552), BUNSPEC);
				((((BgL_ev_appz00_bglt) COBJECT(BgL_new1247z00_4054))->BgL_tailzf3zf3) =
					((obj_t) BgL_tailzf31178zf3_3553), BUNSPEC);
				return BgL_new1247z00_4054;
			}
		}

	}



/* &lambda1988 */
	obj_t BGl_z62lambda1988z62zz__evaluate_typesz00(obj_t BgL_envz00_3554,
		obj_t BgL_oz00_3555, obj_t BgL_vz00_3556)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				((((BgL_ev_appz00_bglt) COBJECT(
							((BgL_ev_appz00_bglt) BgL_oz00_3555)))->BgL_tailzf3zf3) =
				((obj_t) BgL_vz00_3556), BUNSPEC);
		}

	}



/* &lambda1987 */
	obj_t BGl_z62lambda1987z62zz__evaluate_typesz00(obj_t BgL_envz00_3557,
		obj_t BgL_oz00_3558)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				(((BgL_ev_appz00_bglt) COBJECT(
						((BgL_ev_appz00_bglt) BgL_oz00_3558)))->BgL_tailzf3zf3);
		}

	}



/* &lambda1983 */
	obj_t BGl_z62lambda1983z62zz__evaluate_typesz00(obj_t BgL_envz00_3559,
		obj_t BgL_oz00_3560, obj_t BgL_vz00_3561)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				((((BgL_ev_appz00_bglt) COBJECT(
							((BgL_ev_appz00_bglt) BgL_oz00_3560)))->BgL_argsz00) =
				((obj_t) BgL_vz00_3561), BUNSPEC);
		}

	}



/* &lambda1982 */
	obj_t BGl_z62lambda1982z62zz__evaluate_typesz00(obj_t BgL_envz00_3562,
		obj_t BgL_oz00_3563)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				(((BgL_ev_appz00_bglt) COBJECT(
						((BgL_ev_appz00_bglt) BgL_oz00_3563)))->BgL_argsz00);
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zz__evaluate_typesz00(obj_t BgL_envz00_3564,
		obj_t BgL_oz00_3565, obj_t BgL_vz00_3566)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				((((BgL_ev_appz00_bglt) COBJECT(
							((BgL_ev_appz00_bglt) BgL_oz00_3565)))->BgL_funz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3566)), BUNSPEC);
		}

	}



/* &lambda1977 */
	BgL_ev_exprz00_bglt BGl_z62lambda1977z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3567, obj_t BgL_oz00_3568)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				(((BgL_ev_appz00_bglt) COBJECT(
						((BgL_ev_appz00_bglt) BgL_oz00_3568)))->BgL_funz00);
		}

	}



/* &lambda1973 */
	obj_t BGl_z62lambda1973z62zz__evaluate_typesz00(obj_t BgL_envz00_3569,
		obj_t BgL_oz00_3570, obj_t BgL_vz00_3571)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				((((BgL_ev_appz00_bglt) COBJECT(
							((BgL_ev_appz00_bglt) BgL_oz00_3570)))->BgL_locz00) =
				((obj_t) BgL_vz00_3571), BUNSPEC);
		}

	}



/* &lambda1972 */
	obj_t BGl_z62lambda1972z62zz__evaluate_typesz00(obj_t BgL_envz00_3572,
		obj_t BgL_oz00_3573)
	{
		{	/* Eval/evaluate_types.scm 119 */
			return
				(((BgL_ev_appz00_bglt) COBJECT(
						((BgL_ev_appz00_bglt) BgL_oz00_3573)))->BgL_locz00);
		}

	}



/* &<@anonymous:1938> */
	obj_t BGl_z62zc3z04anonymousza31938ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3574, obj_t BgL_new1173z00_3575)
	{
		{	/* Eval/evaluate_types.scm 114 */
			{
				BgL_ev_gotoz00_bglt BgL_auxz00_4814;

				((((BgL_ev_gotoz00_bglt) COBJECT(
								((BgL_ev_gotoz00_bglt) BgL_new1173z00_3575)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_varz00_bglt BgL_auxz00_4817;

					{	/* Eval/evaluate_types.scm 114 */
						obj_t BgL_classz00_4067;

						BgL_classz00_4067 = BGl_ev_varz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 114 */
							obj_t BgL__ortest_1290z00_4068;

							BgL__ortest_1290z00_4068 = BGL_CLASS_NIL(BgL_classz00_4067);
							if (CBOOL(BgL__ortest_1290z00_4068))
								{	/* Eval/evaluate_types.scm 114 */
									BgL_auxz00_4817 =
										((BgL_ev_varz00_bglt) BgL__ortest_1290z00_4068);
								}
							else
								{	/* Eval/evaluate_types.scm 114 */
									BgL_auxz00_4817 =
										((BgL_ev_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4067));
								}
						}
					}
					((((BgL_ev_gotoz00_bglt) COBJECT(
									((BgL_ev_gotoz00_bglt) BgL_new1173z00_3575)))->BgL_labelz00) =
						((BgL_ev_varz00_bglt) BgL_auxz00_4817), BUNSPEC);
				}
				{
					BgL_ev_labelsz00_bglt BgL_auxz00_4826;

					{	/* Eval/evaluate_types.scm 114 */
						obj_t BgL_classz00_4069;

						BgL_classz00_4069 = BGl_ev_labelsz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 114 */
							obj_t BgL__ortest_1290z00_4070;

							BgL__ortest_1290z00_4070 = BGL_CLASS_NIL(BgL_classz00_4069);
							if (CBOOL(BgL__ortest_1290z00_4070))
								{	/* Eval/evaluate_types.scm 114 */
									BgL_auxz00_4826 =
										((BgL_ev_labelsz00_bglt) BgL__ortest_1290z00_4070);
								}
							else
								{	/* Eval/evaluate_types.scm 114 */
									BgL_auxz00_4826 =
										((BgL_ev_labelsz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4069));
								}
						}
					}
					((((BgL_ev_gotoz00_bglt) COBJECT(
									((BgL_ev_gotoz00_bglt) BgL_new1173z00_3575)))->
							BgL_labelsz00) =
						((BgL_ev_labelsz00_bglt) BgL_auxz00_4826), BUNSPEC);
				}
				((((BgL_ev_gotoz00_bglt) COBJECT(
								((BgL_ev_gotoz00_bglt) BgL_new1173z00_3575)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_4814 = ((BgL_ev_gotoz00_bglt) BgL_new1173z00_3575);
				return ((obj_t) BgL_auxz00_4814);
			}
		}

	}



/* &lambda1936 */
	BgL_ev_gotoz00_bglt BGl_z62lambda1936z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3576)
	{
		{	/* Eval/evaluate_types.scm 114 */
			{	/* Eval/evaluate_types.scm 114 */
				BgL_ev_gotoz00_bglt BgL_new1172z00_4071;

				BgL_new1172z00_4071 =
					((BgL_ev_gotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_gotoz00_bgl))));
				{	/* Eval/evaluate_types.scm 114 */
					long BgL_arg1937z00_4072;

					BgL_arg1937z00_4072 =
						BGL_CLASS_NUM(BGl_ev_gotoz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1172z00_4071), BgL_arg1937z00_4072);
				}
				return BgL_new1172z00_4071;
			}
		}

	}



/* &lambda1934 */
	BgL_ev_gotoz00_bglt BGl_z62lambda1934z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3577, obj_t BgL_loc1168z00_3578, obj_t BgL_label1169z00_3579,
		obj_t BgL_labels1170z00_3580, obj_t BgL_args1171z00_3581)
	{
		{	/* Eval/evaluate_types.scm 114 */
			{	/* Eval/evaluate_types.scm 114 */
				BgL_ev_gotoz00_bglt BgL_new1245z00_4075;

				{	/* Eval/evaluate_types.scm 114 */
					BgL_ev_gotoz00_bglt BgL_new1244z00_4076;

					BgL_new1244z00_4076 =
						((BgL_ev_gotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_gotoz00_bgl))));
					{	/* Eval/evaluate_types.scm 114 */
						long BgL_arg1935z00_4077;

						BgL_arg1935z00_4077 =
							BGL_CLASS_NUM(BGl_ev_gotoz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1244z00_4076), BgL_arg1935z00_4077);
					}
					BgL_new1245z00_4075 = BgL_new1244z00_4076;
				}
				((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1245z00_4075))->BgL_locz00) =
					((obj_t) BgL_loc1168z00_3578), BUNSPEC);
				((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1245z00_4075))->BgL_labelz00) =
					((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_label1169z00_3579)),
					BUNSPEC);
				((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1245z00_4075))->BgL_labelsz00) =
					((BgL_ev_labelsz00_bglt) ((BgL_ev_labelsz00_bglt)
							BgL_labels1170z00_3580)), BUNSPEC);
				((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1245z00_4075))->BgL_argsz00) =
					((obj_t) BgL_args1171z00_3581), BUNSPEC);
				return BgL_new1245z00_4075;
			}
		}

	}



/* &lambda1958 */
	obj_t BGl_z62lambda1958z62zz__evaluate_typesz00(obj_t BgL_envz00_3582,
		obj_t BgL_oz00_3583, obj_t BgL_vz00_3584)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				((((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_oz00_3583)))->BgL_argsz00) =
				((obj_t) BgL_vz00_3584), BUNSPEC);
		}

	}



/* &lambda1957 */
	obj_t BGl_z62lambda1957z62zz__evaluate_typesz00(obj_t BgL_envz00_3585,
		obj_t BgL_oz00_3586)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				(((BgL_ev_gotoz00_bglt) COBJECT(
						((BgL_ev_gotoz00_bglt) BgL_oz00_3586)))->BgL_argsz00);
		}

	}



/* &lambda1953 */
	obj_t BGl_z62lambda1953z62zz__evaluate_typesz00(obj_t BgL_envz00_3587,
		obj_t BgL_oz00_3588, obj_t BgL_vz00_3589)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				((((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_oz00_3588)))->BgL_labelsz00) =
				((BgL_ev_labelsz00_bglt) ((BgL_ev_labelsz00_bglt) BgL_vz00_3589)),
				BUNSPEC);
		}

	}



/* &lambda1952 */
	BgL_ev_labelsz00_bglt BGl_z62lambda1952z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3590, obj_t BgL_oz00_3591)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				(((BgL_ev_gotoz00_bglt) COBJECT(
						((BgL_ev_gotoz00_bglt) BgL_oz00_3591)))->BgL_labelsz00);
		}

	}



/* &lambda1948 */
	obj_t BGl_z62lambda1948z62zz__evaluate_typesz00(obj_t BgL_envz00_3592,
		obj_t BgL_oz00_3593, obj_t BgL_vz00_3594)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				((((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_oz00_3593)))->BgL_labelz00) =
				((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_vz00_3594)), BUNSPEC);
		}

	}



/* &lambda1947 */
	BgL_ev_varz00_bglt BGl_z62lambda1947z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3595, obj_t BgL_oz00_3596)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				(((BgL_ev_gotoz00_bglt) COBJECT(
						((BgL_ev_gotoz00_bglt) BgL_oz00_3596)))->BgL_labelz00);
		}

	}



/* &lambda1943 */
	obj_t BGl_z62lambda1943z62zz__evaluate_typesz00(obj_t BgL_envz00_3597,
		obj_t BgL_oz00_3598, obj_t BgL_vz00_3599)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				((((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_oz00_3598)))->BgL_locz00) =
				((obj_t) BgL_vz00_3599), BUNSPEC);
		}

	}



/* &lambda1942 */
	obj_t BGl_z62lambda1942z62zz__evaluate_typesz00(obj_t BgL_envz00_3600,
		obj_t BgL_oz00_3601)
	{
		{	/* Eval/evaluate_types.scm 114 */
			return
				(((BgL_ev_gotoz00_bglt) COBJECT(
						((BgL_ev_gotoz00_bglt) BgL_oz00_3601)))->BgL_locz00);
		}

	}



/* &<@anonymous:1883> */
	obj_t BGl_z62zc3z04anonymousza31883ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3602, obj_t BgL_new1166z00_3603)
	{
		{	/* Eval/evaluate_types.scm 106 */
			{
				BgL_ev_labelsz00_bglt BgL_auxz00_4871;

				((((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_new1166z00_3603)))->BgL_varsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(((BgL_ev_labelsz00_bglt)
									BgL_new1166z00_3603)))->BgL_valsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(((BgL_ev_labelsz00_bglt)
									BgL_new1166z00_3603)))->BgL_envz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(((BgL_ev_labelsz00_bglt)
									BgL_new1166z00_3603)))->BgL_stkz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_4880;

					{	/* Eval/evaluate_types.scm 106 */
						obj_t BgL_classz00_4089;

						BgL_classz00_4089 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 106 */
							obj_t BgL__ortest_1290z00_4090;

							BgL__ortest_1290z00_4090 = BGL_CLASS_NIL(BgL_classz00_4089);
							if (CBOOL(BgL__ortest_1290z00_4090))
								{	/* Eval/evaluate_types.scm 106 */
									BgL_auxz00_4880 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4090);
								}
							else
								{	/* Eval/evaluate_types.scm 106 */
									BgL_auxz00_4880 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4089));
								}
						}
					}
					((((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_new1166z00_3603)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_4880), BUNSPEC);
				}
				((((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_new1166z00_3603)))->BgL_boxesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_4871 = ((BgL_ev_labelsz00_bglt) BgL_new1166z00_3603);
				return ((obj_t) BgL_auxz00_4871);
			}
		}

	}



/* &lambda1881 */
	BgL_ev_labelsz00_bglt BGl_z62lambda1881z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3604)
	{
		{	/* Eval/evaluate_types.scm 106 */
			{	/* Eval/evaluate_types.scm 106 */
				BgL_ev_labelsz00_bglt BgL_new1165z00_4091;

				BgL_new1165z00_4091 =
					((BgL_ev_labelsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_labelsz00_bgl))));
				{	/* Eval/evaluate_types.scm 106 */
					long BgL_arg1882z00_4092;

					BgL_arg1882z00_4092 =
						BGL_CLASS_NUM(BGl_ev_labelsz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1165z00_4091), BgL_arg1882z00_4092);
				}
				return BgL_new1165z00_4091;
			}
		}

	}



/* &lambda1879 */
	BgL_ev_labelsz00_bglt BGl_z62lambda1879z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3605, obj_t BgL_vars1159z00_3606, obj_t BgL_vals1160z00_3607,
		obj_t BgL_env1161z00_3608, obj_t BgL_stk1162z00_3609,
		obj_t BgL_body1163z00_3610, obj_t BgL_boxes1164z00_3611)
	{
		{	/* Eval/evaluate_types.scm 106 */
			{	/* Eval/evaluate_types.scm 106 */
				BgL_ev_labelsz00_bglt BgL_new1243z00_4094;

				{	/* Eval/evaluate_types.scm 106 */
					BgL_ev_labelsz00_bglt BgL_new1242z00_4095;

					BgL_new1242z00_4095 =
						((BgL_ev_labelsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_labelsz00_bgl))));
					{	/* Eval/evaluate_types.scm 106 */
						long BgL_arg1880z00_4096;

						BgL_arg1880z00_4096 =
							BGL_CLASS_NUM(BGl_ev_labelsz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1242z00_4095), BgL_arg1880z00_4096);
					}
					BgL_new1243z00_4094 = BgL_new1242z00_4095;
				}
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1243z00_4094))->BgL_varsz00) =
					((obj_t) BgL_vars1159z00_3606), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1243z00_4094))->BgL_valsz00) =
					((obj_t) BgL_vals1160z00_3607), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1243z00_4094))->BgL_envz00) =
					((obj_t) BgL_env1161z00_3608), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1243z00_4094))->BgL_stkz00) =
					((obj_t) BgL_stk1162z00_3609), BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1243z00_4094))->BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1163z00_3610)),
					BUNSPEC);
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1243z00_4094))->
						BgL_boxesz00) = ((obj_t) BgL_boxes1164z00_3611), BUNSPEC);
				return BgL_new1243z00_4094;
			}
		}

	}



/* &<@anonymous:1928> */
	obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3612)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return BNIL;
		}

	}



/* &lambda1927 */
	obj_t BGl_z62lambda1927z62zz__evaluate_typesz00(obj_t BgL_envz00_3613,
		obj_t BgL_oz00_3614, obj_t BgL_vz00_3615)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				((((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_oz00_3614)))->BgL_boxesz00) =
				((obj_t) BgL_vz00_3615), BUNSPEC);
		}

	}



/* &lambda1926 */
	obj_t BGl_z62lambda1926z62zz__evaluate_typesz00(obj_t BgL_envz00_3616,
		obj_t BgL_oz00_3617)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				(((BgL_ev_labelsz00_bglt) COBJECT(
						((BgL_ev_labelsz00_bglt) BgL_oz00_3617)))->BgL_boxesz00);
		}

	}



/* &lambda1919 */
	obj_t BGl_z62lambda1919z62zz__evaluate_typesz00(obj_t BgL_envz00_3618,
		obj_t BgL_oz00_3619, obj_t BgL_vz00_3620)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				((((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_oz00_3619)))->BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3620)), BUNSPEC);
		}

	}



/* &lambda1918 */
	BgL_ev_exprz00_bglt BGl_z62lambda1918z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3621, obj_t BgL_oz00_3622)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				(((BgL_ev_labelsz00_bglt) COBJECT(
						((BgL_ev_labelsz00_bglt) BgL_oz00_3622)))->BgL_bodyz00);
		}

	}



/* &<@anonymous:1913> */
	obj_t BGl_z62zc3z04anonymousza31913ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3623)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return BNIL;
		}

	}



/* &lambda1912 */
	obj_t BGl_z62lambda1912z62zz__evaluate_typesz00(obj_t BgL_envz00_3624,
		obj_t BgL_oz00_3625, obj_t BgL_vz00_3626)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				((((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_oz00_3625)))->BgL_stkz00) =
				((obj_t) BgL_vz00_3626), BUNSPEC);
		}

	}



/* &lambda1911 */
	obj_t BGl_z62lambda1911z62zz__evaluate_typesz00(obj_t BgL_envz00_3627,
		obj_t BgL_oz00_3628)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				(((BgL_ev_labelsz00_bglt) COBJECT(
						((BgL_ev_labelsz00_bglt) BgL_oz00_3628)))->BgL_stkz00);
		}

	}



/* &<@anonymous:1902> */
	obj_t BGl_z62zc3z04anonymousza31902ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3629)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return BNIL;
		}

	}



/* &lambda1901 */
	obj_t BGl_z62lambda1901z62zz__evaluate_typesz00(obj_t BgL_envz00_3630,
		obj_t BgL_oz00_3631, obj_t BgL_vz00_3632)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				((((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_oz00_3631)))->BgL_envz00) =
				((obj_t) BgL_vz00_3632), BUNSPEC);
		}

	}



/* &lambda1900 */
	obj_t BGl_z62lambda1900z62zz__evaluate_typesz00(obj_t BgL_envz00_3633,
		obj_t BgL_oz00_3634)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				(((BgL_ev_labelsz00_bglt) COBJECT(
						((BgL_ev_labelsz00_bglt) BgL_oz00_3634)))->BgL_envz00);
		}

	}



/* &lambda1894 */
	obj_t BGl_z62lambda1894z62zz__evaluate_typesz00(obj_t BgL_envz00_3635,
		obj_t BgL_oz00_3636, obj_t BgL_vz00_3637)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				((((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_oz00_3636)))->BgL_valsz00) =
				((obj_t) BgL_vz00_3637), BUNSPEC);
		}

	}



/* &lambda1893 */
	obj_t BGl_z62lambda1893z62zz__evaluate_typesz00(obj_t BgL_envz00_3638,
		obj_t BgL_oz00_3639)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				(((BgL_ev_labelsz00_bglt) COBJECT(
						((BgL_ev_labelsz00_bglt) BgL_oz00_3639)))->BgL_valsz00);
		}

	}



/* &lambda1889 */
	obj_t BGl_z62lambda1889z62zz__evaluate_typesz00(obj_t BgL_envz00_3640,
		obj_t BgL_oz00_3641, obj_t BgL_vz00_3642)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				((((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_oz00_3641)))->BgL_varsz00) =
				((obj_t) BgL_vz00_3642), BUNSPEC);
		}

	}



/* &lambda1888 */
	obj_t BGl_z62lambda1888z62zz__evaluate_typesz00(obj_t BgL_envz00_3643,
		obj_t BgL_oz00_3644)
	{
		{	/* Eval/evaluate_types.scm 106 */
			return
				(((BgL_ev_labelsz00_bglt) COBJECT(
						((BgL_ev_labelsz00_bglt) BgL_oz00_3644)))->BgL_varsz00);
		}

	}



/* &<@anonymous:1873> */
	obj_t BGl_z62zc3z04anonymousza31873ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3645, obj_t BgL_new1157z00_3646)
	{
		{	/* Eval/evaluate_types.scm 105 */
			{
				BgL_ev_letrecz00_bglt BgL_auxz00_4933;

				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letrecz00_bglt) BgL_new1157z00_3646))))->
						BgL_varsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_binderz00_bglt)
							COBJECT(((BgL_ev_binderz00_bglt) ((BgL_ev_letrecz00_bglt)
										BgL_new1157z00_3646))))->BgL_valsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_4940;

					{	/* Eval/evaluate_types.scm 105 */
						obj_t BgL_classz00_4111;

						BgL_classz00_4111 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 105 */
							obj_t BgL__ortest_1290z00_4112;

							BgL__ortest_1290z00_4112 = BGL_CLASS_NIL(BgL_classz00_4111);
							if (CBOOL(BgL__ortest_1290z00_4112))
								{	/* Eval/evaluate_types.scm 105 */
									BgL_auxz00_4940 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4112);
								}
							else
								{	/* Eval/evaluate_types.scm 105 */
									BgL_auxz00_4940 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4111));
								}
						}
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_new1157z00_3646))))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_4940), BUNSPEC);
				}
				BgL_auxz00_4933 = ((BgL_ev_letrecz00_bglt) BgL_new1157z00_3646);
				return ((obj_t) BgL_auxz00_4933);
			}
		}

	}



/* &lambda1871 */
	BgL_ev_letrecz00_bglt BGl_z62lambda1871z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3647)
	{
		{	/* Eval/evaluate_types.scm 105 */
			{	/* Eval/evaluate_types.scm 105 */
				BgL_ev_letrecz00_bglt BgL_new1156z00_4113;

				BgL_new1156z00_4113 =
					((BgL_ev_letrecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_letrecz00_bgl))));
				{	/* Eval/evaluate_types.scm 105 */
					long BgL_arg1872z00_4114;

					BgL_arg1872z00_4114 =
						BGL_CLASS_NUM(BGl_ev_letrecz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1156z00_4113), BgL_arg1872z00_4114);
				}
				return BgL_new1156z00_4113;
			}
		}

	}



/* &lambda1869 */
	BgL_ev_letrecz00_bglt BGl_z62lambda1869z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3648, obj_t BgL_vars1153z00_3649, obj_t BgL_vals1154z00_3650,
		obj_t BgL_body1155z00_3651)
	{
		{	/* Eval/evaluate_types.scm 105 */
			{	/* Eval/evaluate_types.scm 105 */
				BgL_ev_letrecz00_bglt BgL_new1240z00_4116;

				{	/* Eval/evaluate_types.scm 105 */
					BgL_ev_letrecz00_bglt BgL_new1239z00_4117;

					BgL_new1239z00_4117 =
						((BgL_ev_letrecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_letrecz00_bgl))));
					{	/* Eval/evaluate_types.scm 105 */
						long BgL_arg1870z00_4118;

						BgL_arg1870z00_4118 =
							BGL_CLASS_NUM(BGl_ev_letrecz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1239z00_4117), BgL_arg1870z00_4118);
					}
					BgL_new1240z00_4116 = BgL_new1239z00_4117;
				}
				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_new1240z00_4116)))->BgL_varsz00) =
					((obj_t) BgL_vars1153z00_3649), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1240z00_4116)))->BgL_valsz00) =
					((obj_t) BgL_vals1154z00_3650), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1240z00_4116)))->BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1155z00_3651)),
					BUNSPEC);
				return BgL_new1240z00_4116;
			}
		}

	}



/* &<@anonymous:1853> */
	obj_t BGl_z62zc3z04anonymousza31853ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3652, obj_t BgL_new1151z00_3653)
	{
		{	/* Eval/evaluate_types.scm 103 */
			{
				BgL_ev_letza2za2_bglt BgL_auxz00_4967;

				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_new1151z00_3653))))->
						BgL_varsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_binderz00_bglt)
							COBJECT(((BgL_ev_binderz00_bglt) ((BgL_ev_letza2za2_bglt)
										BgL_new1151z00_3653))))->BgL_valsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_4974;

					{	/* Eval/evaluate_types.scm 103 */
						obj_t BgL_classz00_4120;

						BgL_classz00_4120 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 103 */
							obj_t BgL__ortest_1290z00_4121;

							BgL__ortest_1290z00_4121 = BGL_CLASS_NIL(BgL_classz00_4120);
							if (CBOOL(BgL__ortest_1290z00_4121))
								{	/* Eval/evaluate_types.scm 103 */
									BgL_auxz00_4974 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4121);
								}
							else
								{	/* Eval/evaluate_types.scm 103 */
									BgL_auxz00_4974 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4120));
								}
						}
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letza2za2_bglt) BgL_new1151z00_3653))))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_4974), BUNSPEC);
				}
				((((BgL_ev_letza2za2_bglt) COBJECT(
								((BgL_ev_letza2za2_bglt) BgL_new1151z00_3653)))->BgL_boxesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_4967 = ((BgL_ev_letza2za2_bglt) BgL_new1151z00_3653);
				return ((obj_t) BgL_auxz00_4967);
			}
		}

	}



/* &lambda1851 */
	BgL_ev_letza2za2_bglt BGl_z62lambda1851z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3654)
	{
		{	/* Eval/evaluate_types.scm 103 */
			{	/* Eval/evaluate_types.scm 103 */
				BgL_ev_letza2za2_bglt BgL_new1150z00_4122;

				BgL_new1150z00_4122 =
					((BgL_ev_letza2za2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_letza2za2_bgl))));
				{	/* Eval/evaluate_types.scm 103 */
					long BgL_arg1852z00_4123;

					BgL_arg1852z00_4123 =
						BGL_CLASS_NUM(BGl_ev_letza2za2zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1150z00_4122), BgL_arg1852z00_4123);
				}
				return BgL_new1150z00_4122;
			}
		}

	}



/* &lambda1849 */
	BgL_ev_letza2za2_bglt BGl_z62lambda1849z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3655, obj_t BgL_vars1146z00_3656, obj_t BgL_vals1147z00_3657,
		obj_t BgL_body1148z00_3658, obj_t BgL_boxes1149z00_3659)
	{
		{	/* Eval/evaluate_types.scm 103 */
			{	/* Eval/evaluate_types.scm 103 */
				BgL_ev_letza2za2_bglt BgL_new1238z00_4125;

				{	/* Eval/evaluate_types.scm 103 */
					BgL_ev_letza2za2_bglt BgL_new1237z00_4126;

					BgL_new1237z00_4126 =
						((BgL_ev_letza2za2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_letza2za2_bgl))));
					{	/* Eval/evaluate_types.scm 103 */
						long BgL_arg1850z00_4127;

						BgL_arg1850z00_4127 =
							BGL_CLASS_NUM(BGl_ev_letza2za2zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1237z00_4126), BgL_arg1850z00_4127);
					}
					BgL_new1238z00_4125 = BgL_new1237z00_4126;
				}
				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_new1238z00_4125)))->BgL_varsz00) =
					((obj_t) BgL_vars1146z00_3656), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1238z00_4125)))->BgL_valsz00) =
					((obj_t) BgL_vals1147z00_3657), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1238z00_4125)))->BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1148z00_3658)),
					BUNSPEC);
				((((BgL_ev_letza2za2_bglt) COBJECT(BgL_new1238z00_4125))->
						BgL_boxesz00) = ((obj_t) BgL_boxes1149z00_3659), BUNSPEC);
				return BgL_new1238z00_4125;
			}
		}

	}



/* &<@anonymous:1861> */
	obj_t BGl_z62zc3z04anonymousza31861ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3660)
	{
		{	/* Eval/evaluate_types.scm 103 */
			return BNIL;
		}

	}



/* &lambda1860 */
	obj_t BGl_z62lambda1860z62zz__evaluate_typesz00(obj_t BgL_envz00_3661,
		obj_t BgL_oz00_3662, obj_t BgL_vz00_3663)
	{
		{	/* Eval/evaluate_types.scm 103 */
			return
				((((BgL_ev_letza2za2_bglt) COBJECT(
							((BgL_ev_letza2za2_bglt) BgL_oz00_3662)))->BgL_boxesz00) =
				((obj_t) BgL_vz00_3663), BUNSPEC);
		}

	}



/* &lambda1859 */
	obj_t BGl_z62lambda1859z62zz__evaluate_typesz00(obj_t BgL_envz00_3664,
		obj_t BgL_oz00_3665)
	{
		{	/* Eval/evaluate_types.scm 103 */
			return
				(((BgL_ev_letza2za2_bglt) COBJECT(
						((BgL_ev_letza2za2_bglt) BgL_oz00_3665)))->BgL_boxesz00);
		}

	}



/* &<@anonymous:1836> */
	obj_t BGl_z62zc3z04anonymousza31836ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3666, obj_t BgL_new1144z00_3667)
	{
		{	/* Eval/evaluate_types.scm 101 */
			{
				BgL_ev_letz00_bglt BgL_auxz00_5008;

				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letz00_bglt) BgL_new1144z00_3667))))->BgL_varsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_binderz00_bglt)
							COBJECT(((BgL_ev_binderz00_bglt) ((BgL_ev_letz00_bglt)
										BgL_new1144z00_3667))))->BgL_valsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5015;

					{	/* Eval/evaluate_types.scm 101 */
						obj_t BgL_classz00_4131;

						BgL_classz00_4131 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 101 */
							obj_t BgL__ortest_1290z00_4132;

							BgL__ortest_1290z00_4132 = BGL_CLASS_NIL(BgL_classz00_4131);
							if (CBOOL(BgL__ortest_1290z00_4132))
								{	/* Eval/evaluate_types.scm 101 */
									BgL_auxz00_5015 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4132);
								}
							else
								{	/* Eval/evaluate_types.scm 101 */
									BgL_auxz00_5015 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4131));
								}
						}
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letz00_bglt) BgL_new1144z00_3667))))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5015), BUNSPEC);
				}
				((((BgL_ev_letz00_bglt) COBJECT(
								((BgL_ev_letz00_bglt) BgL_new1144z00_3667)))->BgL_boxesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5008 = ((BgL_ev_letz00_bglt) BgL_new1144z00_3667);
				return ((obj_t) BgL_auxz00_5008);
			}
		}

	}



/* &lambda1834 */
	BgL_ev_letz00_bglt BGl_z62lambda1834z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3668)
	{
		{	/* Eval/evaluate_types.scm 101 */
			{	/* Eval/evaluate_types.scm 101 */
				BgL_ev_letz00_bglt BgL_new1143z00_4133;

				BgL_new1143z00_4133 =
					((BgL_ev_letz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_letz00_bgl))));
				{	/* Eval/evaluate_types.scm 101 */
					long BgL_arg1835z00_4134;

					BgL_arg1835z00_4134 =
						BGL_CLASS_NUM(BGl_ev_letz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1143z00_4133), BgL_arg1835z00_4134);
				}
				return BgL_new1143z00_4133;
			}
		}

	}



/* &lambda1832 */
	BgL_ev_letz00_bglt BGl_z62lambda1832z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3669, obj_t BgL_vars1139z00_3670, obj_t BgL_vals1140z00_3671,
		obj_t BgL_body1141z00_3672, obj_t BgL_boxes1142z00_3673)
	{
		{	/* Eval/evaluate_types.scm 101 */
			{	/* Eval/evaluate_types.scm 101 */
				BgL_ev_letz00_bglt BgL_new1236z00_4136;

				{	/* Eval/evaluate_types.scm 101 */
					BgL_ev_letz00_bglt BgL_new1235z00_4137;

					BgL_new1235z00_4137 =
						((BgL_ev_letz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_letz00_bgl))));
					{	/* Eval/evaluate_types.scm 101 */
						long BgL_arg1833z00_4138;

						BgL_arg1833z00_4138 =
							BGL_CLASS_NUM(BGl_ev_letz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1235z00_4137), BgL_arg1833z00_4138);
					}
					BgL_new1236z00_4136 = BgL_new1235z00_4137;
				}
				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_new1236z00_4136)))->BgL_varsz00) =
					((obj_t) BgL_vars1139z00_3670), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1236z00_4136)))->BgL_valsz00) =
					((obj_t) BgL_vals1140z00_3671), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1236z00_4136)))->BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1141z00_3672)),
					BUNSPEC);
				((((BgL_ev_letz00_bglt) COBJECT(BgL_new1236z00_4136))->BgL_boxesz00) =
					((obj_t) BgL_boxes1142z00_3673), BUNSPEC);
				return BgL_new1236z00_4136;
			}
		}

	}



/* &<@anonymous:1843> */
	obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3674)
	{
		{	/* Eval/evaluate_types.scm 101 */
			return BNIL;
		}

	}



/* &lambda1842 */
	obj_t BGl_z62lambda1842z62zz__evaluate_typesz00(obj_t BgL_envz00_3675,
		obj_t BgL_oz00_3676, obj_t BgL_vz00_3677)
	{
		{	/* Eval/evaluate_types.scm 101 */
			return
				((((BgL_ev_letz00_bglt) COBJECT(
							((BgL_ev_letz00_bglt) BgL_oz00_3676)))->BgL_boxesz00) =
				((obj_t) BgL_vz00_3677), BUNSPEC);
		}

	}



/* &lambda1841 */
	obj_t BGl_z62lambda1841z62zz__evaluate_typesz00(obj_t BgL_envz00_3678,
		obj_t BgL_oz00_3679)
	{
		{	/* Eval/evaluate_types.scm 101 */
			return
				(((BgL_ev_letz00_bglt) COBJECT(
						((BgL_ev_letz00_bglt) BgL_oz00_3679)))->BgL_boxesz00);
		}

	}



/* &<@anonymous:1809> */
	obj_t BGl_z62zc3z04anonymousza31809ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3680, obj_t BgL_new1137z00_3681)
	{
		{	/* Eval/evaluate_types.scm 97 */
			{
				BgL_ev_binderz00_bglt BgL_auxz00_5049;

				((((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_new1137z00_3681)))->BgL_varsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt)
									BgL_new1137z00_3681)))->BgL_valsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5054;

					{	/* Eval/evaluate_types.scm 97 */
						obj_t BgL_classz00_4142;

						BgL_classz00_4142 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 97 */
							obj_t BgL__ortest_1290z00_4143;

							BgL__ortest_1290z00_4143 = BGL_CLASS_NIL(BgL_classz00_4142);
							if (CBOOL(BgL__ortest_1290z00_4143))
								{	/* Eval/evaluate_types.scm 97 */
									BgL_auxz00_5054 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4143);
								}
							else
								{	/* Eval/evaluate_types.scm 97 */
									BgL_auxz00_5054 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4142));
								}
						}
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt) BgL_new1137z00_3681)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5054), BUNSPEC);
				}
				BgL_auxz00_5049 = ((BgL_ev_binderz00_bglt) BgL_new1137z00_3681);
				return ((obj_t) BgL_auxz00_5049);
			}
		}

	}



/* &lambda1807 */
	BgL_ev_binderz00_bglt BGl_z62lambda1807z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3682)
	{
		{	/* Eval/evaluate_types.scm 97 */
			{	/* Eval/evaluate_types.scm 97 */
				BgL_ev_binderz00_bglt BgL_new1136z00_4144;

				BgL_new1136z00_4144 =
					((BgL_ev_binderz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_binderz00_bgl))));
				{	/* Eval/evaluate_types.scm 97 */
					long BgL_arg1808z00_4145;

					BgL_arg1808z00_4145 =
						BGL_CLASS_NUM(BGl_ev_binderz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1136z00_4144), BgL_arg1808z00_4145);
				}
				return BgL_new1136z00_4144;
			}
		}

	}



/* &lambda1805 */
	BgL_ev_binderz00_bglt BGl_z62lambda1805z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3683, obj_t BgL_vars1133z00_3684, obj_t BgL_vals1134z00_3685,
		obj_t BgL_body1135z00_3686)
	{
		{	/* Eval/evaluate_types.scm 97 */
			{	/* Eval/evaluate_types.scm 97 */
				BgL_ev_binderz00_bglt BgL_new1234z00_4147;

				{	/* Eval/evaluate_types.scm 97 */
					BgL_ev_binderz00_bglt BgL_new1233z00_4148;

					BgL_new1233z00_4148 =
						((BgL_ev_binderz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_binderz00_bgl))));
					{	/* Eval/evaluate_types.scm 97 */
						long BgL_arg1806z00_4149;

						BgL_arg1806z00_4149 =
							BGL_CLASS_NUM(BGl_ev_binderz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1233z00_4148), BgL_arg1806z00_4149);
					}
					BgL_new1234z00_4147 = BgL_new1233z00_4148;
				}
				((((BgL_ev_binderz00_bglt) COBJECT(BgL_new1234z00_4147))->BgL_varsz00) =
					((obj_t) BgL_vars1133z00_3684), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(BgL_new1234z00_4147))->BgL_valsz00) =
					((obj_t) BgL_vals1134z00_3685), BUNSPEC);
				((((BgL_ev_binderz00_bglt) COBJECT(BgL_new1234z00_4147))->BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1135z00_3686)),
					BUNSPEC);
				return BgL_new1234z00_4147;
			}
		}

	}



/* &lambda1825 */
	obj_t BGl_z62lambda1825z62zz__evaluate_typesz00(obj_t BgL_envz00_3687,
		obj_t BgL_oz00_3688, obj_t BgL_vz00_3689)
	{
		{	/* Eval/evaluate_types.scm 97 */
			return
				((((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt) BgL_oz00_3688)))->BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3689)), BUNSPEC);
		}

	}



/* &lambda1824 */
	BgL_ev_exprz00_bglt BGl_z62lambda1824z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3690, obj_t BgL_oz00_3691)
	{
		{	/* Eval/evaluate_types.scm 97 */
			return
				(((BgL_ev_binderz00_bglt) COBJECT(
						((BgL_ev_binderz00_bglt) BgL_oz00_3691)))->BgL_bodyz00);
		}

	}



/* &lambda1819 */
	obj_t BGl_z62lambda1819z62zz__evaluate_typesz00(obj_t BgL_envz00_3692,
		obj_t BgL_oz00_3693, obj_t BgL_vz00_3694)
	{
		{	/* Eval/evaluate_types.scm 97 */
			return
				((((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt) BgL_oz00_3693)))->BgL_valsz00) =
				((obj_t) BgL_vz00_3694), BUNSPEC);
		}

	}



/* &lambda1818 */
	obj_t BGl_z62lambda1818z62zz__evaluate_typesz00(obj_t BgL_envz00_3695,
		obj_t BgL_oz00_3696)
	{
		{	/* Eval/evaluate_types.scm 97 */
			return
				(((BgL_ev_binderz00_bglt) COBJECT(
						((BgL_ev_binderz00_bglt) BgL_oz00_3696)))->BgL_valsz00);
		}

	}



/* &lambda1814 */
	obj_t BGl_z62lambda1814z62zz__evaluate_typesz00(obj_t BgL_envz00_3697,
		obj_t BgL_oz00_3698, obj_t BgL_vz00_3699)
	{
		{	/* Eval/evaluate_types.scm 97 */
			return
				((((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt) BgL_oz00_3698)))->BgL_varsz00) =
				((obj_t) BgL_vz00_3699), BUNSPEC);
		}

	}



/* &lambda1813 */
	obj_t BGl_z62lambda1813z62zz__evaluate_typesz00(obj_t BgL_envz00_3700,
		obj_t BgL_oz00_3701)
	{
		{	/* Eval/evaluate_types.scm 97 */
			return
				(((BgL_ev_binderz00_bglt) COBJECT(
						((BgL_ev_binderz00_bglt) BgL_oz00_3701)))->BgL_varsz00);
		}

	}



/* &<@anonymous:1778> */
	obj_t BGl_z62zc3z04anonymousza31778ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3702, obj_t BgL_new1131z00_3703)
	{
		{	/* Eval/evaluate_types.scm 92 */
			{
				BgL_ev_synchroniza7eza7_bglt BgL_auxz00_5090;

				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
								((BgL_ev_synchroniza7eza7_bglt) BgL_new1131z00_3703)))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5093;

					{	/* Eval/evaluate_types.scm 92 */
						obj_t BgL_classz00_4158;

						BgL_classz00_4158 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 92 */
							obj_t BgL__ortest_1290z00_4159;

							BgL__ortest_1290z00_4159 = BGL_CLASS_NIL(BgL_classz00_4158);
							if (CBOOL(BgL__ortest_1290z00_4159))
								{	/* Eval/evaluate_types.scm 92 */
									BgL_auxz00_5093 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4159);
								}
							else
								{	/* Eval/evaluate_types.scm 92 */
									BgL_auxz00_5093 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4158));
								}
						}
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_new1131z00_3703)))->
							BgL_mutexz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5093), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5102;

					{	/* Eval/evaluate_types.scm 92 */
						obj_t BgL_classz00_4160;

						BgL_classz00_4160 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 92 */
							obj_t BgL__ortest_1290z00_4161;

							BgL__ortest_1290z00_4161 = BGL_CLASS_NIL(BgL_classz00_4160);
							if (CBOOL(BgL__ortest_1290z00_4161))
								{	/* Eval/evaluate_types.scm 92 */
									BgL_auxz00_5102 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4161);
								}
							else
								{	/* Eval/evaluate_types.scm 92 */
									BgL_auxz00_5102 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4160));
								}
						}
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_new1131z00_3703)))->
							BgL_prelockz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5102), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5111;

					{	/* Eval/evaluate_types.scm 92 */
						obj_t BgL_classz00_4162;

						BgL_classz00_4162 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 92 */
							obj_t BgL__ortest_1290z00_4163;

							BgL__ortest_1290z00_4163 = BGL_CLASS_NIL(BgL_classz00_4162);
							if (CBOOL(BgL__ortest_1290z00_4163))
								{	/* Eval/evaluate_types.scm 92 */
									BgL_auxz00_5111 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4163);
								}
							else
								{	/* Eval/evaluate_types.scm 92 */
									BgL_auxz00_5111 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4162));
								}
						}
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_new1131z00_3703)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5111), BUNSPEC);
				}
				BgL_auxz00_5090 = ((BgL_ev_synchroniza7eza7_bglt) BgL_new1131z00_3703);
				return ((obj_t) BgL_auxz00_5090);
			}
		}

	}



/* &lambda1776 */
	BgL_ev_synchroniza7eza7_bglt BGl_z62lambda1776z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3704)
	{
		{	/* Eval/evaluate_types.scm 92 */
			{	/* Eval/evaluate_types.scm 92 */
				BgL_ev_synchroniza7eza7_bglt BgL_new1130z00_4164;

				BgL_new1130z00_4164 =
					((BgL_ev_synchroniza7eza7_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_synchroniza7eza7_bgl))));
				{	/* Eval/evaluate_types.scm 92 */
					long BgL_arg1777z00_4165;

					BgL_arg1777z00_4165 =
						BGL_CLASS_NUM(BGl_ev_synchroniza7eza7zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1130z00_4164), BgL_arg1777z00_4165);
				}
				return BgL_new1130z00_4164;
			}
		}

	}



/* &lambda1774 */
	BgL_ev_synchroniza7eza7_bglt BGl_z62lambda1774z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3705, obj_t BgL_loc1126z00_3706, obj_t BgL_mutex1127z00_3707,
		obj_t BgL_prelock1128z00_3708, obj_t BgL_body1129z00_3709)
	{
		{	/* Eval/evaluate_types.scm 92 */
			{	/* Eval/evaluate_types.scm 92 */
				BgL_ev_synchroniza7eza7_bglt BgL_new1232z00_4169;

				{	/* Eval/evaluate_types.scm 92 */
					BgL_ev_synchroniza7eza7_bglt BgL_new1231z00_4170;

					BgL_new1231z00_4170 =
						((BgL_ev_synchroniza7eza7_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_synchroniza7eza7_bgl))));
					{	/* Eval/evaluate_types.scm 92 */
						long BgL_arg1775z00_4171;

						BgL_arg1775z00_4171 =
							BGL_CLASS_NUM(BGl_ev_synchroniza7eza7zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1231z00_4170), BgL_arg1775z00_4171);
					}
					BgL_new1232z00_4169 = BgL_new1231z00_4170;
				}
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1232z00_4169))->
						BgL_locz00) = ((obj_t) BgL_loc1126z00_3706), BUNSPEC);
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1232z00_4169))->
						BgL_mutexz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_mutex1127z00_3707)),
					BUNSPEC);
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1232z00_4169))->
						BgL_prelockz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt)
							BgL_prelock1128z00_3708)), BUNSPEC);
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1232z00_4169))->
						BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1129z00_3709)),
					BUNSPEC);
				return BgL_new1232z00_4169;
			}
		}

	}



/* &lambda1799 */
	obj_t BGl_z62lambda1799z62zz__evaluate_typesz00(obj_t BgL_envz00_3710,
		obj_t BgL_oz00_3711, obj_t BgL_vz00_3712)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
							((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3711)))->BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3712)), BUNSPEC);
		}

	}



/* &lambda1798 */
	BgL_ev_exprz00_bglt BGl_z62lambda1798z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3713, obj_t BgL_oz00_3714)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
						((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3714)))->BgL_bodyz00);
		}

	}



/* &lambda1794 */
	obj_t BGl_z62lambda1794z62zz__evaluate_typesz00(obj_t BgL_envz00_3715,
		obj_t BgL_oz00_3716, obj_t BgL_vz00_3717)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
							((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3716)))->
					BgL_prelockz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3717)), BUNSPEC);
		}

	}



/* &lambda1793 */
	BgL_ev_exprz00_bglt BGl_z62lambda1793z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3718, obj_t BgL_oz00_3719)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
						((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3719)))->BgL_prelockz00);
		}

	}



/* &lambda1789 */
	obj_t BGl_z62lambda1789z62zz__evaluate_typesz00(obj_t BgL_envz00_3720,
		obj_t BgL_oz00_3721, obj_t BgL_vz00_3722)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
							((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3721)))->BgL_mutexz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3722)), BUNSPEC);
		}

	}



/* &lambda1788 */
	BgL_ev_exprz00_bglt BGl_z62lambda1788z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3723, obj_t BgL_oz00_3724)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
						((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3724)))->BgL_mutexz00);
		}

	}



/* &lambda1784 */
	obj_t BGl_z62lambda1784z62zz__evaluate_typesz00(obj_t BgL_envz00_3725,
		obj_t BgL_oz00_3726, obj_t BgL_vz00_3727)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
							((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3726)))->BgL_locz00) =
				((obj_t) BgL_vz00_3727), BUNSPEC);
		}

	}



/* &lambda1783 */
	obj_t BGl_z62lambda1783z62zz__evaluate_typesz00(obj_t BgL_envz00_3728,
		obj_t BgL_oz00_3729)
	{
		{	/* Eval/evaluate_types.scm 92 */
			return
				(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
						((BgL_ev_synchroniza7eza7_bglt) BgL_oz00_3729)))->BgL_locz00);
		}

	}



/* &<@anonymous:1758> */
	obj_t BGl_z62zc3z04anonymousza31758ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3730, obj_t BgL_new1124z00_3731)
	{
		{	/* Eval/evaluate_types.scm 89 */
			{
				BgL_ev_withzd2handlerzd2_bglt BgL_auxz00_5156;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5157;

					{	/* Eval/evaluate_types.scm 89 */
						obj_t BgL_classz00_4184;

						BgL_classz00_4184 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 89 */
							obj_t BgL__ortest_1290z00_4185;

							BgL__ortest_1290z00_4185 = BGL_CLASS_NIL(BgL_classz00_4184);
							if (CBOOL(BgL__ortest_1290z00_4185))
								{	/* Eval/evaluate_types.scm 89 */
									BgL_auxz00_5157 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4185);
								}
							else
								{	/* Eval/evaluate_types.scm 89 */
									BgL_auxz00_5157 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4184));
								}
						}
					}
					((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_new1124z00_3731)))->
							BgL_handlerz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5157), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5166;

					{	/* Eval/evaluate_types.scm 89 */
						obj_t BgL_classz00_4186;

						BgL_classz00_4186 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 89 */
							obj_t BgL__ortest_1290z00_4187;

							BgL__ortest_1290z00_4187 = BGL_CLASS_NIL(BgL_classz00_4186);
							if (CBOOL(BgL__ortest_1290z00_4187))
								{	/* Eval/evaluate_types.scm 89 */
									BgL_auxz00_5166 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4187);
								}
							else
								{	/* Eval/evaluate_types.scm 89 */
									BgL_auxz00_5166 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4186));
								}
						}
					}
					((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_new1124z00_3731)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5166), BUNSPEC);
				}
				BgL_auxz00_5156 = ((BgL_ev_withzd2handlerzd2_bglt) BgL_new1124z00_3731);
				return ((obj_t) BgL_auxz00_5156);
			}
		}

	}



/* &lambda1756 */
	BgL_ev_withzd2handlerzd2_bglt BGl_z62lambda1756z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3732)
	{
		{	/* Eval/evaluate_types.scm 89 */
			{	/* Eval/evaluate_types.scm 89 */
				BgL_ev_withzd2handlerzd2_bglt BgL_new1123z00_4188;

				BgL_new1123z00_4188 =
					((BgL_ev_withzd2handlerzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_withzd2handlerzd2_bgl))));
				{	/* Eval/evaluate_types.scm 89 */
					long BgL_arg1757z00_4189;

					BgL_arg1757z00_4189 =
						BGL_CLASS_NUM(BGl_ev_withzd2handlerzd2zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1123z00_4188), BgL_arg1757z00_4189);
				}
				return BgL_new1123z00_4188;
			}
		}

	}



/* &lambda1754 */
	BgL_ev_withzd2handlerzd2_bglt BGl_z62lambda1754z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3733, obj_t BgL_handler1121z00_3734, obj_t BgL_body1122z00_3735)
	{
		{	/* Eval/evaluate_types.scm 89 */
			{	/* Eval/evaluate_types.scm 89 */
				BgL_ev_withzd2handlerzd2_bglt BgL_new1230z00_4192;

				{	/* Eval/evaluate_types.scm 89 */
					BgL_ev_withzd2handlerzd2_bglt BgL_new1229z00_4193;

					BgL_new1229z00_4193 =
						((BgL_ev_withzd2handlerzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_withzd2handlerzd2_bgl))));
					{	/* Eval/evaluate_types.scm 89 */
						long BgL_arg1755z00_4194;

						BgL_arg1755z00_4194 =
							BGL_CLASS_NUM(BGl_ev_withzd2handlerzd2zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1229z00_4193), BgL_arg1755z00_4194);
					}
					BgL_new1230z00_4192 = BgL_new1229z00_4193;
				}
				((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(BgL_new1230z00_4192))->
						BgL_handlerz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt)
							BgL_handler1121z00_3734)), BUNSPEC);
				((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(BgL_new1230z00_4192))->
						BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1122z00_3735)),
					BUNSPEC);
				return BgL_new1230z00_4192;
			}
		}

	}



/* &lambda1768 */
	obj_t BGl_z62lambda1768z62zz__evaluate_typesz00(obj_t BgL_envz00_3736,
		obj_t BgL_oz00_3737, obj_t BgL_vz00_3738)
	{
		{	/* Eval/evaluate_types.scm 89 */
			return
				((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
							((BgL_ev_withzd2handlerzd2_bglt) BgL_oz00_3737)))->BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3738)), BUNSPEC);
		}

	}



/* &lambda1767 */
	BgL_ev_exprz00_bglt BGl_z62lambda1767z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3739, obj_t BgL_oz00_3740)
	{
		{	/* Eval/evaluate_types.scm 89 */
			return
				(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
						((BgL_ev_withzd2handlerzd2_bglt) BgL_oz00_3740)))->BgL_bodyz00);
		}

	}



/* &lambda1763 */
	obj_t BGl_z62lambda1763z62zz__evaluate_typesz00(obj_t BgL_envz00_3741,
		obj_t BgL_oz00_3742, obj_t BgL_vz00_3743)
	{
		{	/* Eval/evaluate_types.scm 89 */
			return
				((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
							((BgL_ev_withzd2handlerzd2_bglt) BgL_oz00_3742)))->
					BgL_handlerz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3743)), BUNSPEC);
		}

	}



/* &lambda1762 */
	BgL_ev_exprz00_bglt BGl_z62lambda1762z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3744, obj_t BgL_oz00_3745)
	{
		{	/* Eval/evaluate_types.scm 89 */
			return
				(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
						((BgL_ev_withzd2handlerzd2_bglt) BgL_oz00_3745)))->BgL_handlerz00);
		}

	}



/* &<@anonymous:1738> */
	obj_t BGl_z62zc3z04anonymousza31738ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3746, obj_t BgL_new1119z00_3747)
	{
		{	/* Eval/evaluate_types.scm 86 */
			{
				BgL_ev_unwindzd2protectzd2_bglt BgL_auxz00_5199;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5200;

					{	/* Eval/evaluate_types.scm 86 */
						obj_t BgL_classz00_4202;

						BgL_classz00_4202 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 86 */
							obj_t BgL__ortest_1290z00_4203;

							BgL__ortest_1290z00_4203 = BGL_CLASS_NIL(BgL_classz00_4202);
							if (CBOOL(BgL__ortest_1290z00_4203))
								{	/* Eval/evaluate_types.scm 86 */
									BgL_auxz00_5200 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4203);
								}
							else
								{	/* Eval/evaluate_types.scm 86 */
									BgL_auxz00_5200 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4202));
								}
						}
					}
					((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_new1119z00_3747)))->
							BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5200), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5209;

					{	/* Eval/evaluate_types.scm 86 */
						obj_t BgL_classz00_4204;

						BgL_classz00_4204 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 86 */
							obj_t BgL__ortest_1290z00_4205;

							BgL__ortest_1290z00_4205 = BGL_CLASS_NIL(BgL_classz00_4204);
							if (CBOOL(BgL__ortest_1290z00_4205))
								{	/* Eval/evaluate_types.scm 86 */
									BgL_auxz00_5209 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4205);
								}
							else
								{	/* Eval/evaluate_types.scm 86 */
									BgL_auxz00_5209 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4204));
								}
						}
					}
					((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_new1119z00_3747)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5209), BUNSPEC);
				}
				BgL_auxz00_5199 =
					((BgL_ev_unwindzd2protectzd2_bglt) BgL_new1119z00_3747);
				return ((obj_t) BgL_auxz00_5199);
			}
		}

	}



/* &lambda1736 */
	BgL_ev_unwindzd2protectzd2_bglt
		BGl_z62lambda1736z62zz__evaluate_typesz00(obj_t BgL_envz00_3748)
	{
		{	/* Eval/evaluate_types.scm 86 */
			{	/* Eval/evaluate_types.scm 86 */
				BgL_ev_unwindzd2protectzd2_bglt BgL_new1118z00_4206;

				BgL_new1118z00_4206 =
					((BgL_ev_unwindzd2protectzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_unwindzd2protectzd2_bgl))));
				{	/* Eval/evaluate_types.scm 86 */
					long BgL_arg1737z00_4207;

					BgL_arg1737z00_4207 =
						BGL_CLASS_NUM(BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1118z00_4206), BgL_arg1737z00_4207);
				}
				return BgL_new1118z00_4206;
			}
		}

	}



/* &lambda1734 */
	BgL_ev_unwindzd2protectzd2_bglt
		BGl_z62lambda1734z62zz__evaluate_typesz00(obj_t BgL_envz00_3749,
		obj_t BgL_e1116z00_3750, obj_t BgL_body1117z00_3751)
	{
		{	/* Eval/evaluate_types.scm 86 */
			{	/* Eval/evaluate_types.scm 86 */
				BgL_ev_unwindzd2protectzd2_bglt BgL_new1228z00_4210;

				{	/* Eval/evaluate_types.scm 86 */
					BgL_ev_unwindzd2protectzd2_bglt BgL_new1226z00_4211;

					BgL_new1226z00_4211 =
						((BgL_ev_unwindzd2protectzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_unwindzd2protectzd2_bgl))));
					{	/* Eval/evaluate_types.scm 86 */
						long BgL_arg1735z00_4212;

						BgL_arg1735z00_4212 =
							BGL_CLASS_NUM(BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1226z00_4211), BgL_arg1735z00_4212);
					}
					BgL_new1228z00_4210 = BgL_new1226z00_4211;
				}
				((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(BgL_new1228z00_4210))->
						BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1116z00_3750)),
					BUNSPEC);
				((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(BgL_new1228z00_4210))->
						BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1117z00_3751)),
					BUNSPEC);
				return BgL_new1228z00_4210;
			}
		}

	}



/* &lambda1748 */
	obj_t BGl_z62lambda1748z62zz__evaluate_typesz00(obj_t BgL_envz00_3752,
		obj_t BgL_oz00_3753, obj_t BgL_vz00_3754)
	{
		{	/* Eval/evaluate_types.scm 86 */
			return
				((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
							((BgL_ev_unwindzd2protectzd2_bglt) BgL_oz00_3753)))->
					BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3754)), BUNSPEC);
		}

	}



/* &lambda1747 */
	BgL_ev_exprz00_bglt BGl_z62lambda1747z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3755, obj_t BgL_oz00_3756)
	{
		{	/* Eval/evaluate_types.scm 86 */
			return
				(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
						((BgL_ev_unwindzd2protectzd2_bglt) BgL_oz00_3756)))->BgL_bodyz00);
		}

	}



/* &lambda1743 */
	obj_t BGl_z62lambda1743z62zz__evaluate_typesz00(obj_t BgL_envz00_3757,
		obj_t BgL_oz00_3758, obj_t BgL_vz00_3759)
	{
		{	/* Eval/evaluate_types.scm 86 */
			return
				((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
							((BgL_ev_unwindzd2protectzd2_bglt) BgL_oz00_3758)))->BgL_ez00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3759)), BUNSPEC);
		}

	}



/* &lambda1742 */
	BgL_ev_exprz00_bglt BGl_z62lambda1742z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3760, obj_t BgL_oz00_3761)
	{
		{	/* Eval/evaluate_types.scm 86 */
			return
				(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
						((BgL_ev_unwindzd2protectzd2_bglt) BgL_oz00_3761)))->BgL_ez00);
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3762, obj_t BgL_new1114z00_3763)
	{
		{	/* Eval/evaluate_types.scm 83 */
			{
				BgL_ev_bindzd2exitzd2_bglt BgL_auxz00_5242;

				{
					BgL_ev_varz00_bglt BgL_auxz00_5243;

					{	/* Eval/evaluate_types.scm 83 */
						obj_t BgL_classz00_4220;

						BgL_classz00_4220 = BGl_ev_varz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 83 */
							obj_t BgL__ortest_1290z00_4221;

							BgL__ortest_1290z00_4221 = BGL_CLASS_NIL(BgL_classz00_4220);
							if (CBOOL(BgL__ortest_1290z00_4221))
								{	/* Eval/evaluate_types.scm 83 */
									BgL_auxz00_5243 =
										((BgL_ev_varz00_bglt) BgL__ortest_1290z00_4221);
								}
							else
								{	/* Eval/evaluate_types.scm 83 */
									BgL_auxz00_5243 =
										((BgL_ev_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4220));
								}
						}
					}
					((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_new1114z00_3763)))->
							BgL_varz00) = ((BgL_ev_varz00_bglt) BgL_auxz00_5243), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5252;

					{	/* Eval/evaluate_types.scm 83 */
						obj_t BgL_classz00_4222;

						BgL_classz00_4222 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 83 */
							obj_t BgL__ortest_1290z00_4223;

							BgL__ortest_1290z00_4223 = BGL_CLASS_NIL(BgL_classz00_4222);
							if (CBOOL(BgL__ortest_1290z00_4223))
								{	/* Eval/evaluate_types.scm 83 */
									BgL_auxz00_5252 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4223);
								}
							else
								{	/* Eval/evaluate_types.scm 83 */
									BgL_auxz00_5252 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4222));
								}
						}
					}
					((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_new1114z00_3763)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5252), BUNSPEC);
				}
				BgL_auxz00_5242 = ((BgL_ev_bindzd2exitzd2_bglt) BgL_new1114z00_3763);
				return ((obj_t) BgL_auxz00_5242);
			}
		}

	}



/* &lambda1712 */
	BgL_ev_bindzd2exitzd2_bglt BGl_z62lambda1712z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3764)
	{
		{	/* Eval/evaluate_types.scm 83 */
			{	/* Eval/evaluate_types.scm 83 */
				BgL_ev_bindzd2exitzd2_bglt BgL_new1113z00_4224;

				BgL_new1113z00_4224 =
					((BgL_ev_bindzd2exitzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_bindzd2exitzd2_bgl))));
				{	/* Eval/evaluate_types.scm 83 */
					long BgL_arg1714z00_4225;

					BgL_arg1714z00_4225 =
						BGL_CLASS_NUM(BGl_ev_bindzd2exitzd2zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1113z00_4224), BgL_arg1714z00_4225);
				}
				return BgL_new1113z00_4224;
			}
		}

	}



/* &lambda1710 */
	BgL_ev_bindzd2exitzd2_bglt BGl_z62lambda1710z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3765, obj_t BgL_var1110z00_3766, obj_t BgL_body1111z00_3767)
	{
		{	/* Eval/evaluate_types.scm 83 */
			{	/* Eval/evaluate_types.scm 83 */
				BgL_ev_bindzd2exitzd2_bglt BgL_new1225z00_4228;

				{	/* Eval/evaluate_types.scm 83 */
					BgL_ev_bindzd2exitzd2_bglt BgL_new1224z00_4229;

					BgL_new1224z00_4229 =
						((BgL_ev_bindzd2exitzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_bindzd2exitzd2_bgl))));
					{	/* Eval/evaluate_types.scm 83 */
						long BgL_arg1711z00_4230;

						BgL_arg1711z00_4230 =
							BGL_CLASS_NUM(BGl_ev_bindzd2exitzd2zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1224z00_4229), BgL_arg1711z00_4230);
					}
					BgL_new1225z00_4228 = BgL_new1224z00_4229;
				}
				((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(BgL_new1225z00_4228))->
						BgL_varz00) =
					((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_var1110z00_3766)),
					BUNSPEC);
				((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(BgL_new1225z00_4228))->
						BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_body1111z00_3767)),
					BUNSPEC);
				return BgL_new1225z00_4228;
			}
		}

	}



/* &lambda1727 */
	obj_t BGl_z62lambda1727z62zz__evaluate_typesz00(obj_t BgL_envz00_3768,
		obj_t BgL_oz00_3769, obj_t BgL_vz00_3770)
	{
		{	/* Eval/evaluate_types.scm 83 */
			return
				((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
							((BgL_ev_bindzd2exitzd2_bglt) BgL_oz00_3769)))->BgL_bodyz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3770)), BUNSPEC);
		}

	}



/* &lambda1726 */
	BgL_ev_exprz00_bglt BGl_z62lambda1726z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3771, obj_t BgL_oz00_3772)
	{
		{	/* Eval/evaluate_types.scm 83 */
			return
				(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
						((BgL_ev_bindzd2exitzd2_bglt) BgL_oz00_3772)))->BgL_bodyz00);
		}

	}



/* &lambda1722 */
	obj_t BGl_z62lambda1722z62zz__evaluate_typesz00(obj_t BgL_envz00_3773,
		obj_t BgL_oz00_3774, obj_t BgL_vz00_3775)
	{
		{	/* Eval/evaluate_types.scm 83 */
			return
				((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
							((BgL_ev_bindzd2exitzd2_bglt) BgL_oz00_3774)))->BgL_varz00) =
				((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_vz00_3775)), BUNSPEC);
		}

	}



/* &lambda1721 */
	BgL_ev_varz00_bglt BGl_z62lambda1721z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3776, obj_t BgL_oz00_3777)
	{
		{	/* Eval/evaluate_types.scm 83 */
			return
				(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
						((BgL_ev_bindzd2exitzd2_bglt) BgL_oz00_3777)))->BgL_varz00);
		}

	}



/* &<@anonymous:1704> */
	obj_t BGl_z62zc3z04anonymousza31704ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3778, obj_t BgL_new1108z00_3779)
	{
		{	/* Eval/evaluate_types.scm 82 */
			{
				BgL_ev_defglobalz00_bglt BgL_auxz00_5285;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5286;

					{	/* Eval/evaluate_types.scm 82 */
						obj_t BgL_classz00_4238;

						BgL_classz00_4238 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 82 */
							obj_t BgL__ortest_1290z00_4239;

							BgL__ortest_1290z00_4239 = BGL_CLASS_NIL(BgL_classz00_4238);
							if (CBOOL(BgL__ortest_1290z00_4239))
								{	/* Eval/evaluate_types.scm 82 */
									BgL_auxz00_5286 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4239);
								}
							else
								{	/* Eval/evaluate_types.scm 82 */
									BgL_auxz00_5286 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4238));
								}
						}
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt)
										((BgL_ev_defglobalz00_bglt) BgL_new1108z00_3779))))->
							BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5286), BUNSPEC);
				}
				((((BgL_ev_setglobalz00_bglt) COBJECT(
								((BgL_ev_setglobalz00_bglt)
									((BgL_ev_defglobalz00_bglt) BgL_new1108z00_3779))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt)
							COBJECT(((BgL_ev_setglobalz00_bglt) ((BgL_ev_defglobalz00_bglt)
										BgL_new1108z00_3779))))->BgL_namez00) =
					((obj_t) BGl_symbol2706z00zz__evaluate_typesz00), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt)
							COBJECT(((BgL_ev_setglobalz00_bglt) ((BgL_ev_defglobalz00_bglt)
										BgL_new1108z00_3779))))->BgL_modz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5285 = ((BgL_ev_defglobalz00_bglt) BgL_new1108z00_3779);
				return ((obj_t) BgL_auxz00_5285);
			}
		}

	}



/* &lambda1702 */
	BgL_ev_defglobalz00_bglt BGl_z62lambda1702z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3780)
	{
		{	/* Eval/evaluate_types.scm 82 */
			{	/* Eval/evaluate_types.scm 82 */
				BgL_ev_defglobalz00_bglt BgL_new1107z00_4240;

				BgL_new1107z00_4240 =
					((BgL_ev_defglobalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_defglobalz00_bgl))));
				{	/* Eval/evaluate_types.scm 82 */
					long BgL_arg1703z00_4241;

					BgL_arg1703z00_4241 =
						BGL_CLASS_NUM(BGl_ev_defglobalz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1107z00_4240), BgL_arg1703z00_4241);
				}
				return BgL_new1107z00_4240;
			}
		}

	}



/* &lambda1700 */
	BgL_ev_defglobalz00_bglt BGl_z62lambda1700z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3781, obj_t BgL_e1103z00_3782, obj_t BgL_loc1104z00_3783,
		obj_t BgL_name1105z00_3784, obj_t BgL_mod1106z00_3785)
	{
		{	/* Eval/evaluate_types.scm 82 */
			{	/* Eval/evaluate_types.scm 82 */
				BgL_ev_defglobalz00_bglt BgL_new1223z00_4244;

				{	/* Eval/evaluate_types.scm 82 */
					BgL_ev_defglobalz00_bglt BgL_new1222z00_4245;

					BgL_new1222z00_4245 =
						((BgL_ev_defglobalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_defglobalz00_bgl))));
					{	/* Eval/evaluate_types.scm 82 */
						long BgL_arg1701z00_4246;

						BgL_arg1701z00_4246 =
							BGL_CLASS_NUM(BGl_ev_defglobalz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1222z00_4245), BgL_arg1701z00_4246);
					}
					BgL_new1223z00_4244 = BgL_new1222z00_4245;
				}
				((((BgL_ev_hookz00_bglt) COBJECT(
								((BgL_ev_hookz00_bglt) BgL_new1223z00_4244)))->BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1103z00_3782)),
					BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt)
									BgL_new1223z00_4244)))->BgL_locz00) =
					((obj_t) BgL_loc1104z00_3783), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt)
									BgL_new1223z00_4244)))->BgL_namez00) =
					((obj_t) ((obj_t) BgL_name1105z00_3784)), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt)
									BgL_new1223z00_4244)))->BgL_modz00) =
					((obj_t) BgL_mod1106z00_3785), BUNSPEC);
				return BgL_new1223z00_4244;
			}
		}

	}



/* &<@anonymous:1664> */
	obj_t BGl_z62zc3z04anonymousza31664ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3786, obj_t BgL_new1101z00_3787)
	{
		{	/* Eval/evaluate_types.scm 78 */
			{
				BgL_ev_setglobalz00_bglt BgL_auxz00_5325;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5326;

					{	/* Eval/evaluate_types.scm 78 */
						obj_t BgL_classz00_4248;

						BgL_classz00_4248 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 78 */
							obj_t BgL__ortest_1290z00_4249;

							BgL__ortest_1290z00_4249 = BGL_CLASS_NIL(BgL_classz00_4248);
							if (CBOOL(BgL__ortest_1290z00_4249))
								{	/* Eval/evaluate_types.scm 78 */
									BgL_auxz00_5326 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4249);
								}
							else
								{	/* Eval/evaluate_types.scm 78 */
									BgL_auxz00_5326 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4248));
								}
						}
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt)
										((BgL_ev_setglobalz00_bglt) BgL_new1101z00_3787))))->
							BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5326), BUNSPEC);
				}
				((((BgL_ev_setglobalz00_bglt) COBJECT(
								((BgL_ev_setglobalz00_bglt) BgL_new1101z00_3787)))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt)
									BgL_new1101z00_3787)))->BgL_namez00) =
					((obj_t) BGl_symbol2706z00zz__evaluate_typesz00), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt)
									BgL_new1101z00_3787)))->BgL_modz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5325 = ((BgL_ev_setglobalz00_bglt) BgL_new1101z00_3787);
				return ((obj_t) BgL_auxz00_5325);
			}
		}

	}



/* &lambda1662 */
	BgL_ev_setglobalz00_bglt BGl_z62lambda1662z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3788)
	{
		{	/* Eval/evaluate_types.scm 78 */
			{	/* Eval/evaluate_types.scm 78 */
				BgL_ev_setglobalz00_bglt BgL_new1100z00_4250;

				BgL_new1100z00_4250 =
					((BgL_ev_setglobalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_setglobalz00_bgl))));
				{	/* Eval/evaluate_types.scm 78 */
					long BgL_arg1663z00_4251;

					BgL_arg1663z00_4251 =
						BGL_CLASS_NUM(BGl_ev_setglobalz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1100z00_4250), BgL_arg1663z00_4251);
				}
				return BgL_new1100z00_4250;
			}
		}

	}



/* &lambda1659 */
	BgL_ev_setglobalz00_bglt BGl_z62lambda1659z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3789, obj_t BgL_e1095z00_3790, obj_t BgL_loc1096z00_3791,
		obj_t BgL_name1097z00_3792, obj_t BgL_mod1098z00_3793)
	{
		{	/* Eval/evaluate_types.scm 78 */
			{	/* Eval/evaluate_types.scm 78 */
				BgL_ev_setglobalz00_bglt BgL_new1221z00_4254;

				{	/* Eval/evaluate_types.scm 78 */
					BgL_ev_setglobalz00_bglt BgL_new1220z00_4255;

					BgL_new1220z00_4255 =
						((BgL_ev_setglobalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_setglobalz00_bgl))));
					{	/* Eval/evaluate_types.scm 78 */
						long BgL_arg1661z00_4256;

						BgL_arg1661z00_4256 =
							BGL_CLASS_NUM(BGl_ev_setglobalz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1220z00_4255), BgL_arg1661z00_4256);
					}
					BgL_new1221z00_4254 = BgL_new1220z00_4255;
				}
				((((BgL_ev_hookz00_bglt) COBJECT(
								((BgL_ev_hookz00_bglt) BgL_new1221z00_4254)))->BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1095z00_3790)),
					BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1221z00_4254))->
						BgL_locz00) = ((obj_t) BgL_loc1096z00_3791), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1221z00_4254))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1097z00_3792)), BUNSPEC);
				((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1221z00_4254))->
						BgL_modz00) = ((obj_t) BgL_mod1098z00_3793), BUNSPEC);
				return BgL_new1221z00_4254;
			}
		}

	}



/* &lambda1687 */
	obj_t BGl_z62lambda1687z62zz__evaluate_typesz00(obj_t BgL_envz00_3794,
		obj_t BgL_oz00_3795, obj_t BgL_vz00_3796)
	{
		{	/* Eval/evaluate_types.scm 78 */
			return
				((((BgL_ev_setglobalz00_bglt) COBJECT(
							((BgL_ev_setglobalz00_bglt) BgL_oz00_3795)))->BgL_modz00) =
				((obj_t) BgL_vz00_3796), BUNSPEC);
		}

	}



/* &lambda1686 */
	obj_t BGl_z62lambda1686z62zz__evaluate_typesz00(obj_t BgL_envz00_3797,
		obj_t BgL_oz00_3798)
	{
		{	/* Eval/evaluate_types.scm 78 */
			return
				(((BgL_ev_setglobalz00_bglt) COBJECT(
						((BgL_ev_setglobalz00_bglt) BgL_oz00_3798)))->BgL_modz00);
		}

	}



/* &lambda1680 */
	obj_t BGl_z62lambda1680z62zz__evaluate_typesz00(obj_t BgL_envz00_3799,
		obj_t BgL_oz00_3800, obj_t BgL_vz00_3801)
	{
		{	/* Eval/evaluate_types.scm 78 */
			return
				((((BgL_ev_setglobalz00_bglt) COBJECT(
							((BgL_ev_setglobalz00_bglt) BgL_oz00_3800)))->BgL_namez00) =
				((obj_t) ((obj_t) BgL_vz00_3801)), BUNSPEC);
		}

	}



/* &lambda1679 */
	obj_t BGl_z62lambda1679z62zz__evaluate_typesz00(obj_t BgL_envz00_3802,
		obj_t BgL_oz00_3803)
	{
		{	/* Eval/evaluate_types.scm 78 */
			return
				(((BgL_ev_setglobalz00_bglt) COBJECT(
						((BgL_ev_setglobalz00_bglt) BgL_oz00_3803)))->BgL_namez00);
		}

	}



/* &lambda1671 */
	obj_t BGl_z62lambda1671z62zz__evaluate_typesz00(obj_t BgL_envz00_3804,
		obj_t BgL_oz00_3805, obj_t BgL_vz00_3806)
	{
		{	/* Eval/evaluate_types.scm 78 */
			return
				((((BgL_ev_setglobalz00_bglt) COBJECT(
							((BgL_ev_setglobalz00_bglt) BgL_oz00_3805)))->BgL_locz00) =
				((obj_t) BgL_vz00_3806), BUNSPEC);
		}

	}



/* &lambda1670 */
	obj_t BGl_z62lambda1670z62zz__evaluate_typesz00(obj_t BgL_envz00_3807,
		obj_t BgL_oz00_3808)
	{
		{	/* Eval/evaluate_types.scm 78 */
			return
				(((BgL_ev_setglobalz00_bglt) COBJECT(
						((BgL_ev_setglobalz00_bglt) BgL_oz00_3808)))->BgL_locz00);
		}

	}



/* &<@anonymous:1647> */
	obj_t BGl_z62zc3z04anonymousza31647ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3809, obj_t BgL_new1093z00_3810)
	{
		{	/* Eval/evaluate_types.scm 76 */
			{
				BgL_ev_setlocalz00_bglt BgL_auxz00_5372;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5373;

					{	/* Eval/evaluate_types.scm 76 */
						obj_t BgL_classz00_4265;

						BgL_classz00_4265 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 76 */
							obj_t BgL__ortest_1290z00_4266;

							BgL__ortest_1290z00_4266 = BGL_CLASS_NIL(BgL_classz00_4265);
							if (CBOOL(BgL__ortest_1290z00_4266))
								{	/* Eval/evaluate_types.scm 76 */
									BgL_auxz00_5373 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4266);
								}
							else
								{	/* Eval/evaluate_types.scm 76 */
									BgL_auxz00_5373 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4265));
								}
						}
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt)
										((BgL_ev_setlocalz00_bglt) BgL_new1093z00_3810))))->
							BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5373), BUNSPEC);
				}
				{
					BgL_ev_varz00_bglt BgL_auxz00_5383;

					{	/* Eval/evaluate_types.scm 76 */
						obj_t BgL_classz00_4267;

						BgL_classz00_4267 = BGl_ev_varz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 76 */
							obj_t BgL__ortest_1290z00_4268;

							BgL__ortest_1290z00_4268 = BGL_CLASS_NIL(BgL_classz00_4267);
							if (CBOOL(BgL__ortest_1290z00_4268))
								{	/* Eval/evaluate_types.scm 76 */
									BgL_auxz00_5383 =
										((BgL_ev_varz00_bglt) BgL__ortest_1290z00_4268);
								}
							else
								{	/* Eval/evaluate_types.scm 76 */
									BgL_auxz00_5383 =
										((BgL_ev_varz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4267));
								}
						}
					}
					((((BgL_ev_setlocalz00_bglt) COBJECT(
									((BgL_ev_setlocalz00_bglt) BgL_new1093z00_3810)))->BgL_vz00) =
						((BgL_ev_varz00_bglt) BgL_auxz00_5383), BUNSPEC);
				}
				BgL_auxz00_5372 = ((BgL_ev_setlocalz00_bglt) BgL_new1093z00_3810);
				return ((obj_t) BgL_auxz00_5372);
			}
		}

	}



/* &lambda1645 */
	BgL_ev_setlocalz00_bglt BGl_z62lambda1645z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3811)
	{
		{	/* Eval/evaluate_types.scm 76 */
			{	/* Eval/evaluate_types.scm 76 */
				BgL_ev_setlocalz00_bglt BgL_new1092z00_4269;

				BgL_new1092z00_4269 =
					((BgL_ev_setlocalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_setlocalz00_bgl))));
				{	/* Eval/evaluate_types.scm 76 */
					long BgL_arg1646z00_4270;

					BgL_arg1646z00_4270 =
						BGL_CLASS_NUM(BGl_ev_setlocalz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1092z00_4269), BgL_arg1646z00_4270);
				}
				return BgL_new1092z00_4269;
			}
		}

	}



/* &lambda1643 */
	BgL_ev_setlocalz00_bglt BGl_z62lambda1643z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3812, obj_t BgL_e1090z00_3813, obj_t BgL_v1091z00_3814)
	{
		{	/* Eval/evaluate_types.scm 76 */
			{	/* Eval/evaluate_types.scm 76 */
				BgL_ev_setlocalz00_bglt BgL_new1219z00_4273;

				{	/* Eval/evaluate_types.scm 76 */
					BgL_ev_setlocalz00_bglt BgL_new1218z00_4274;

					BgL_new1218z00_4274 =
						((BgL_ev_setlocalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_setlocalz00_bgl))));
					{	/* Eval/evaluate_types.scm 76 */
						long BgL_arg1644z00_4275;

						BgL_arg1644z00_4275 =
							BGL_CLASS_NUM(BGl_ev_setlocalz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1218z00_4274), BgL_arg1644z00_4275);
					}
					BgL_new1219z00_4273 = BgL_new1218z00_4274;
				}
				((((BgL_ev_hookz00_bglt) COBJECT(
								((BgL_ev_hookz00_bglt) BgL_new1219z00_4273)))->BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1090z00_3813)),
					BUNSPEC);
				((((BgL_ev_setlocalz00_bglt) COBJECT(BgL_new1219z00_4273))->BgL_vz00) =
					((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_v1091z00_3814)),
					BUNSPEC);
				return BgL_new1219z00_4273;
			}
		}

	}



/* &lambda1652 */
	obj_t BGl_z62lambda1652z62zz__evaluate_typesz00(obj_t BgL_envz00_3815,
		obj_t BgL_oz00_3816, obj_t BgL_vz00_3817)
	{
		{	/* Eval/evaluate_types.scm 76 */
			return
				((((BgL_ev_setlocalz00_bglt) COBJECT(
							((BgL_ev_setlocalz00_bglt) BgL_oz00_3816)))->BgL_vz00) =
				((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_vz00_3817)), BUNSPEC);
		}

	}



/* &lambda1651 */
	BgL_ev_varz00_bglt BGl_z62lambda1651z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3818, obj_t BgL_oz00_3819)
	{
		{	/* Eval/evaluate_types.scm 76 */
			return
				(((BgL_ev_setlocalz00_bglt) COBJECT(
						((BgL_ev_setlocalz00_bglt) BgL_oz00_3819)))->BgL_vz00);
		}

	}



/* &<@anonymous:1637> */
	obj_t BGl_z62zc3z04anonymousza31637ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3820, obj_t BgL_new1088z00_3821)
	{
		{	/* Eval/evaluate_types.scm 75 */
			{
				BgL_ev_trapz00_bglt BgL_auxz00_5412;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5413;

					{	/* Eval/evaluate_types.scm 75 */
						obj_t BgL_classz00_4280;

						BgL_classz00_4280 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 75 */
							obj_t BgL__ortest_1290z00_4281;

							BgL__ortest_1290z00_4281 = BGL_CLASS_NIL(BgL_classz00_4280);
							if (CBOOL(BgL__ortest_1290z00_4281))
								{	/* Eval/evaluate_types.scm 75 */
									BgL_auxz00_5413 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4281);
								}
							else
								{	/* Eval/evaluate_types.scm 75 */
									BgL_auxz00_5413 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4280));
								}
						}
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt)
										((BgL_ev_trapz00_bglt) BgL_new1088z00_3821))))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5413), BUNSPEC);
				}
				BgL_auxz00_5412 = ((BgL_ev_trapz00_bglt) BgL_new1088z00_3821);
				return ((obj_t) BgL_auxz00_5412);
			}
		}

	}



/* &lambda1635 */
	BgL_ev_trapz00_bglt BGl_z62lambda1635z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3822)
	{
		{	/* Eval/evaluate_types.scm 75 */
			{	/* Eval/evaluate_types.scm 75 */
				BgL_ev_trapz00_bglt BgL_new1087z00_4282;

				BgL_new1087z00_4282 =
					((BgL_ev_trapz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_trapz00_bgl))));
				{	/* Eval/evaluate_types.scm 75 */
					long BgL_arg1636z00_4283;

					BgL_arg1636z00_4283 =
						BGL_CLASS_NUM(BGl_ev_trapz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1087z00_4282), BgL_arg1636z00_4283);
				}
				return BgL_new1087z00_4282;
			}
		}

	}



/* &lambda1632 */
	BgL_ev_trapz00_bglt BGl_z62lambda1632z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3823, obj_t BgL_e1086z00_3824)
	{
		{	/* Eval/evaluate_types.scm 75 */
			{	/* Eval/evaluate_types.scm 75 */
				BgL_ev_trapz00_bglt BgL_new1217z00_4285;

				{	/* Eval/evaluate_types.scm 75 */
					BgL_ev_trapz00_bglt BgL_new1216z00_4286;

					BgL_new1216z00_4286 =
						((BgL_ev_trapz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_trapz00_bgl))));
					{	/* Eval/evaluate_types.scm 75 */
						long BgL_arg1634z00_4287;

						BgL_arg1634z00_4287 =
							BGL_CLASS_NUM(BGl_ev_trapz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1216z00_4286), BgL_arg1634z00_4287);
					}
					BgL_new1217z00_4285 = BgL_new1216z00_4286;
				}
				((((BgL_ev_hookz00_bglt) COBJECT(
								((BgL_ev_hookz00_bglt) BgL_new1217z00_4285)))->BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1086z00_3824)),
					BUNSPEC);
				return BgL_new1217z00_4285;
			}
		}

	}



/* &<@anonymous:1621> */
	obj_t BGl_z62zc3z04anonymousza31621ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3825, obj_t BgL_new1084z00_3826)
	{
		{	/* Eval/evaluate_types.scm 73 */
			{
				BgL_ev_hookz00_bglt BgL_auxz00_5436;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5437;

					{	/* Eval/evaluate_types.scm 73 */
						obj_t BgL_classz00_4289;

						BgL_classz00_4289 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 73 */
							obj_t BgL__ortest_1290z00_4290;

							BgL__ortest_1290z00_4290 = BGL_CLASS_NIL(BgL_classz00_4289);
							if (CBOOL(BgL__ortest_1290z00_4290))
								{	/* Eval/evaluate_types.scm 73 */
									BgL_auxz00_5437 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4290);
								}
							else
								{	/* Eval/evaluate_types.scm 73 */
									BgL_auxz00_5437 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4289));
								}
						}
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_new1084z00_3826)))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5437), BUNSPEC);
				}
				BgL_auxz00_5436 = ((BgL_ev_hookz00_bglt) BgL_new1084z00_3826);
				return ((obj_t) BgL_auxz00_5436);
			}
		}

	}



/* &lambda1619 */
	BgL_ev_hookz00_bglt BGl_z62lambda1619z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3827)
	{
		{	/* Eval/evaluate_types.scm 73 */
			{	/* Eval/evaluate_types.scm 73 */
				BgL_ev_hookz00_bglt BgL_new1083z00_4291;

				BgL_new1083z00_4291 =
					((BgL_ev_hookz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_hookz00_bgl))));
				{	/* Eval/evaluate_types.scm 73 */
					long BgL_arg1620z00_4292;

					BgL_arg1620z00_4292 =
						BGL_CLASS_NUM(BGl_ev_hookz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1083z00_4291), BgL_arg1620z00_4292);
				}
				return BgL_new1083z00_4291;
			}
		}

	}



/* &lambda1617 */
	BgL_ev_hookz00_bglt BGl_z62lambda1617z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3828, obj_t BgL_e1082z00_3829)
	{
		{	/* Eval/evaluate_types.scm 73 */
			{	/* Eval/evaluate_types.scm 73 */
				BgL_ev_hookz00_bglt BgL_new1215z00_4294;

				{	/* Eval/evaluate_types.scm 73 */
					BgL_ev_hookz00_bglt BgL_new1214z00_4295;

					BgL_new1214z00_4295 =
						((BgL_ev_hookz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_hookz00_bgl))));
					{	/* Eval/evaluate_types.scm 73 */
						long BgL_arg1618z00_4296;

						BgL_arg1618z00_4296 =
							BGL_CLASS_NUM(BGl_ev_hookz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1214z00_4295), BgL_arg1618z00_4296);
					}
					BgL_new1215z00_4294 = BgL_new1214z00_4295;
				}
				((((BgL_ev_hookz00_bglt) COBJECT(BgL_new1215z00_4294))->BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1082z00_3829)),
					BUNSPEC);
				return BgL_new1215z00_4294;
			}
		}

	}



/* &lambda1626 */
	obj_t BGl_z62lambda1626z62zz__evaluate_typesz00(obj_t BgL_envz00_3830,
		obj_t BgL_oz00_3831, obj_t BgL_vz00_3832)
	{
		{	/* Eval/evaluate_types.scm 73 */
			return
				((((BgL_ev_hookz00_bglt) COBJECT(
							((BgL_ev_hookz00_bglt) BgL_oz00_3831)))->BgL_ez00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3832)), BUNSPEC);
		}

	}



/* &lambda1625 */
	BgL_ev_exprz00_bglt BGl_z62lambda1625z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3833, obj_t BgL_oz00_3834)
	{
		{	/* Eval/evaluate_types.scm 73 */
			return
				(((BgL_ev_hookz00_bglt) COBJECT(
						((BgL_ev_hookz00_bglt) BgL_oz00_3834)))->BgL_ez00);
		}

	}



/* &<@anonymous:1599> */
	obj_t BGl_z62zc3z04anonymousza31599ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3835, obj_t BgL_new1080z00_3836)
	{
		{	/* Eval/evaluate_types.scm 70 */
			{
				BgL_ev_prog2z00_bglt BgL_auxz00_5463;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5464;

					{	/* Eval/evaluate_types.scm 70 */
						obj_t BgL_classz00_4301;

						BgL_classz00_4301 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 70 */
							obj_t BgL__ortest_1290z00_4302;

							BgL__ortest_1290z00_4302 = BGL_CLASS_NIL(BgL_classz00_4301);
							if (CBOOL(BgL__ortest_1290z00_4302))
								{	/* Eval/evaluate_types.scm 70 */
									BgL_auxz00_5464 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4302);
								}
							else
								{	/* Eval/evaluate_types.scm 70 */
									BgL_auxz00_5464 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4301));
								}
						}
					}
					((((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_new1080z00_3836)))->BgL_e1z00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5464), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5473;

					{	/* Eval/evaluate_types.scm 70 */
						obj_t BgL_classz00_4303;

						BgL_classz00_4303 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 70 */
							obj_t BgL__ortest_1290z00_4304;

							BgL__ortest_1290z00_4304 = BGL_CLASS_NIL(BgL_classz00_4303);
							if (CBOOL(BgL__ortest_1290z00_4304))
								{	/* Eval/evaluate_types.scm 70 */
									BgL_auxz00_5473 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4304);
								}
							else
								{	/* Eval/evaluate_types.scm 70 */
									BgL_auxz00_5473 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4303));
								}
						}
					}
					((((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_new1080z00_3836)))->BgL_e2z00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5473), BUNSPEC);
				}
				BgL_auxz00_5463 = ((BgL_ev_prog2z00_bglt) BgL_new1080z00_3836);
				return ((obj_t) BgL_auxz00_5463);
			}
		}

	}



/* &lambda1596 */
	BgL_ev_prog2z00_bglt BGl_z62lambda1596z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3837)
	{
		{	/* Eval/evaluate_types.scm 70 */
			{	/* Eval/evaluate_types.scm 70 */
				BgL_ev_prog2z00_bglt BgL_new1079z00_4305;

				BgL_new1079z00_4305 =
					((BgL_ev_prog2z00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_prog2z00_bgl))));
				{	/* Eval/evaluate_types.scm 70 */
					long BgL_arg1598z00_4306;

					BgL_arg1598z00_4306 =
						BGL_CLASS_NUM(BGl_ev_prog2z00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1079z00_4305), BgL_arg1598z00_4306);
				}
				return BgL_new1079z00_4305;
			}
		}

	}



/* &lambda1594 */
	BgL_ev_prog2z00_bglt BGl_z62lambda1594z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3838, obj_t BgL_e11077z00_3839, obj_t BgL_e21078z00_3840)
	{
		{	/* Eval/evaluate_types.scm 70 */
			{	/* Eval/evaluate_types.scm 70 */
				BgL_ev_prog2z00_bglt BgL_new1213z00_4309;

				{	/* Eval/evaluate_types.scm 70 */
					BgL_ev_prog2z00_bglt BgL_new1212z00_4310;

					BgL_new1212z00_4310 =
						((BgL_ev_prog2z00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_prog2z00_bgl))));
					{	/* Eval/evaluate_types.scm 70 */
						long BgL_arg1595z00_4311;

						BgL_arg1595z00_4311 =
							BGL_CLASS_NUM(BGl_ev_prog2z00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1212z00_4310), BgL_arg1595z00_4311);
					}
					BgL_new1213z00_4309 = BgL_new1212z00_4310;
				}
				((((BgL_ev_prog2z00_bglt) COBJECT(BgL_new1213z00_4309))->BgL_e1z00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e11077z00_3839)),
					BUNSPEC);
				((((BgL_ev_prog2z00_bglt) COBJECT(BgL_new1213z00_4309))->BgL_e2z00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e21078z00_3840)),
					BUNSPEC);
				return BgL_new1213z00_4309;
			}
		}

	}



/* &lambda1610 */
	obj_t BGl_z62lambda1610z62zz__evaluate_typesz00(obj_t BgL_envz00_3841,
		obj_t BgL_oz00_3842, obj_t BgL_vz00_3843)
	{
		{	/* Eval/evaluate_types.scm 70 */
			return
				((((BgL_ev_prog2z00_bglt) COBJECT(
							((BgL_ev_prog2z00_bglt) BgL_oz00_3842)))->BgL_e2z00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3843)), BUNSPEC);
		}

	}



/* &lambda1609 */
	BgL_ev_exprz00_bglt BGl_z62lambda1609z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3844, obj_t BgL_oz00_3845)
	{
		{	/* Eval/evaluate_types.scm 70 */
			return
				(((BgL_ev_prog2z00_bglt) COBJECT(
						((BgL_ev_prog2z00_bglt) BgL_oz00_3845)))->BgL_e2z00);
		}

	}



/* &lambda1605 */
	obj_t BGl_z62lambda1605z62zz__evaluate_typesz00(obj_t BgL_envz00_3846,
		obj_t BgL_oz00_3847, obj_t BgL_vz00_3848)
	{
		{	/* Eval/evaluate_types.scm 70 */
			return
				((((BgL_ev_prog2z00_bglt) COBJECT(
							((BgL_ev_prog2z00_bglt) BgL_oz00_3847)))->BgL_e1z00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3848)), BUNSPEC);
		}

	}



/* &lambda1604 */
	BgL_ev_exprz00_bglt BGl_z62lambda1604z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3849, obj_t BgL_oz00_3850)
	{
		{	/* Eval/evaluate_types.scm 70 */
			return
				(((BgL_ev_prog2z00_bglt) COBJECT(
						((BgL_ev_prog2z00_bglt) BgL_oz00_3850)))->BgL_e1z00);
		}

	}



/* &<@anonymous:1585> */
	obj_t BGl_z62zc3z04anonymousza31585ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3851, obj_t BgL_new1075z00_3852)
	{
		{	/* Eval/evaluate_types.scm 69 */
			{
				BgL_ev_andz00_bglt BgL_auxz00_5506;

				((((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt)
									((BgL_ev_andz00_bglt) BgL_new1075z00_3852))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5506 = ((BgL_ev_andz00_bglt) BgL_new1075z00_3852);
				return ((obj_t) BgL_auxz00_5506);
			}
		}

	}



/* &lambda1583 */
	BgL_ev_andz00_bglt BGl_z62lambda1583z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3853)
	{
		{	/* Eval/evaluate_types.scm 69 */
			{	/* Eval/evaluate_types.scm 69 */
				BgL_ev_andz00_bglt BgL_new1074z00_4319;

				BgL_new1074z00_4319 =
					((BgL_ev_andz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_andz00_bgl))));
				{	/* Eval/evaluate_types.scm 69 */
					long BgL_arg1584z00_4320;

					BgL_arg1584z00_4320 =
						BGL_CLASS_NUM(BGl_ev_andz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1074z00_4319), BgL_arg1584z00_4320);
				}
				return BgL_new1074z00_4319;
			}
		}

	}



/* &lambda1580 */
	BgL_ev_andz00_bglt BGl_z62lambda1580z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3854, obj_t BgL_args1073z00_3855)
	{
		{	/* Eval/evaluate_types.scm 69 */
			{	/* Eval/evaluate_types.scm 69 */
				BgL_ev_andz00_bglt BgL_new1211z00_4321;

				{	/* Eval/evaluate_types.scm 69 */
					BgL_ev_andz00_bglt BgL_new1210z00_4322;

					BgL_new1210z00_4322 =
						((BgL_ev_andz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_andz00_bgl))));
					{	/* Eval/evaluate_types.scm 69 */
						long BgL_arg1582z00_4323;

						BgL_arg1582z00_4323 =
							BGL_CLASS_NUM(BGl_ev_andz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1210z00_4322), BgL_arg1582z00_4323);
					}
					BgL_new1211z00_4321 = BgL_new1210z00_4322;
				}
				((((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_new1211z00_4321)))->BgL_argsz00) =
					((obj_t) BgL_args1073z00_3855), BUNSPEC);
				return BgL_new1211z00_4321;
			}
		}

	}



/* &<@anonymous:1572> */
	obj_t BGl_z62zc3z04anonymousza31572ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3856, obj_t BgL_new1071z00_3857)
	{
		{	/* Eval/evaluate_types.scm 68 */
			{
				BgL_ev_orz00_bglt BgL_auxz00_5522;

				((((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt)
									((BgL_ev_orz00_bglt) BgL_new1071z00_3857))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5522 = ((BgL_ev_orz00_bglt) BgL_new1071z00_3857);
				return ((obj_t) BgL_auxz00_5522);
			}
		}

	}



/* &lambda1568 */
	BgL_ev_orz00_bglt BGl_z62lambda1568z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3858)
	{
		{	/* Eval/evaluate_types.scm 68 */
			{	/* Eval/evaluate_types.scm 68 */
				BgL_ev_orz00_bglt BgL_new1070z00_4325;

				BgL_new1070z00_4325 =
					((BgL_ev_orz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_orz00_bgl))));
				{	/* Eval/evaluate_types.scm 68 */
					long BgL_arg1571z00_4326;

					BgL_arg1571z00_4326 =
						BGL_CLASS_NUM(BGl_ev_orz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1070z00_4325), BgL_arg1571z00_4326);
				}
				return BgL_new1070z00_4325;
			}
		}

	}



/* &lambda1566 */
	BgL_ev_orz00_bglt BGl_z62lambda1566z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3859, obj_t BgL_args1069z00_3860)
	{
		{	/* Eval/evaluate_types.scm 68 */
			{	/* Eval/evaluate_types.scm 68 */
				BgL_ev_orz00_bglt BgL_new1209z00_4327;

				{	/* Eval/evaluate_types.scm 68 */
					BgL_ev_orz00_bglt BgL_new1208z00_4328;

					BgL_new1208z00_4328 =
						((BgL_ev_orz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_orz00_bgl))));
					{	/* Eval/evaluate_types.scm 68 */
						long BgL_arg1567z00_4329;

						BgL_arg1567z00_4329 =
							BGL_CLASS_NUM(BGl_ev_orz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1208z00_4328), BgL_arg1567z00_4329);
					}
					BgL_new1209z00_4327 = BgL_new1208z00_4328;
				}
				((((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_new1209z00_4327)))->BgL_argsz00) =
					((obj_t) BgL_args1069z00_3860), BUNSPEC);
				return BgL_new1209z00_4327;
			}
		}

	}



/* &<@anonymous:1553> */
	obj_t BGl_z62zc3z04anonymousza31553ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3861, obj_t BgL_new1067z00_3862)
	{
		{	/* Eval/evaluate_types.scm 66 */
			{
				BgL_ev_listz00_bglt BgL_auxz00_5538;

				((((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_new1067z00_3862)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5538 = ((BgL_ev_listz00_bglt) BgL_new1067z00_3862);
				return ((obj_t) BgL_auxz00_5538);
			}
		}

	}



/* &lambda1550 */
	BgL_ev_listz00_bglt BGl_z62lambda1550z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3863)
	{
		{	/* Eval/evaluate_types.scm 66 */
			{	/* Eval/evaluate_types.scm 66 */
				BgL_ev_listz00_bglt BgL_new1066z00_4331;

				BgL_new1066z00_4331 =
					((BgL_ev_listz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_listz00_bgl))));
				{	/* Eval/evaluate_types.scm 66 */
					long BgL_arg1552z00_4332;

					BgL_arg1552z00_4332 =
						BGL_CLASS_NUM(BGl_ev_listz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1066z00_4331), BgL_arg1552z00_4332);
				}
				return BgL_new1066z00_4331;
			}
		}

	}



/* &lambda1548 */
	BgL_ev_listz00_bglt BGl_z62lambda1548z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3864, obj_t BgL_args1065z00_3865)
	{
		{	/* Eval/evaluate_types.scm 66 */
			{	/* Eval/evaluate_types.scm 66 */
				BgL_ev_listz00_bglt BgL_new1207z00_4333;

				{	/* Eval/evaluate_types.scm 66 */
					BgL_ev_listz00_bglt BgL_new1206z00_4334;

					BgL_new1206z00_4334 =
						((BgL_ev_listz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_listz00_bgl))));
					{	/* Eval/evaluate_types.scm 66 */
						long BgL_arg1549z00_4335;

						BgL_arg1549z00_4335 =
							BGL_CLASS_NUM(BGl_ev_listz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1206z00_4334), BgL_arg1549z00_4335);
					}
					BgL_new1207z00_4333 = BgL_new1206z00_4334;
				}
				((((BgL_ev_listz00_bglt) COBJECT(BgL_new1207z00_4333))->BgL_argsz00) =
					((obj_t) BgL_args1065z00_3865), BUNSPEC);
				return BgL_new1207z00_4333;
			}
		}

	}



/* &lambda1558 */
	obj_t BGl_z62lambda1558z62zz__evaluate_typesz00(obj_t BgL_envz00_3866,
		obj_t BgL_oz00_3867, obj_t BgL_vz00_3868)
	{
		{	/* Eval/evaluate_types.scm 66 */
			return
				((((BgL_ev_listz00_bglt) COBJECT(
							((BgL_ev_listz00_bglt) BgL_oz00_3867)))->BgL_argsz00) =
				((obj_t) BgL_vz00_3868), BUNSPEC);
		}

	}



/* &lambda1557 */
	obj_t BGl_z62lambda1557z62zz__evaluate_typesz00(obj_t BgL_envz00_3869,
		obj_t BgL_oz00_3870)
	{
		{	/* Eval/evaluate_types.scm 66 */
			return
				(((BgL_ev_listz00_bglt) COBJECT(
						((BgL_ev_listz00_bglt) BgL_oz00_3870)))->BgL_argsz00);
		}

	}



/* &<@anonymous:1522> */
	obj_t BGl_z62zc3z04anonymousza31522ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3871, obj_t BgL_new1063z00_3872)
	{
		{	/* Eval/evaluate_types.scm 62 */
			{
				BgL_ev_ifz00_bglt BgL_auxz00_5556;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_5557;

					{	/* Eval/evaluate_types.scm 62 */
						obj_t BgL_classz00_4339;

						BgL_classz00_4339 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 62 */
							obj_t BgL__ortest_1290z00_4340;

							BgL__ortest_1290z00_4340 = BGL_CLASS_NIL(BgL_classz00_4339);
							if (CBOOL(BgL__ortest_1290z00_4340))
								{	/* Eval/evaluate_types.scm 62 */
									BgL_auxz00_5557 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4340);
								}
							else
								{	/* Eval/evaluate_types.scm 62 */
									BgL_auxz00_5557 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4339));
								}
						}
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_new1063z00_3872)))->BgL_pz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5557), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5566;

					{	/* Eval/evaluate_types.scm 62 */
						obj_t BgL_classz00_4341;

						BgL_classz00_4341 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 62 */
							obj_t BgL__ortest_1290z00_4342;

							BgL__ortest_1290z00_4342 = BGL_CLASS_NIL(BgL_classz00_4341);
							if (CBOOL(BgL__ortest_1290z00_4342))
								{	/* Eval/evaluate_types.scm 62 */
									BgL_auxz00_5566 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4342);
								}
							else
								{	/* Eval/evaluate_types.scm 62 */
									BgL_auxz00_5566 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4341));
								}
						}
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_new1063z00_3872)))->BgL_tz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5566), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_5575;

					{	/* Eval/evaluate_types.scm 62 */
						obj_t BgL_classz00_4343;

						BgL_classz00_4343 = BGl_ev_exprz00zz__evaluate_typesz00;
						{	/* Eval/evaluate_types.scm 62 */
							obj_t BgL__ortest_1290z00_4344;

							BgL__ortest_1290z00_4344 = BGL_CLASS_NIL(BgL_classz00_4343);
							if (CBOOL(BgL__ortest_1290z00_4344))
								{	/* Eval/evaluate_types.scm 62 */
									BgL_auxz00_5575 =
										((BgL_ev_exprz00_bglt) BgL__ortest_1290z00_4344);
								}
							else
								{	/* Eval/evaluate_types.scm 62 */
									BgL_auxz00_5575 =
										((BgL_ev_exprz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4343));
								}
						}
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_new1063z00_3872)))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_5575), BUNSPEC);
				}
				BgL_auxz00_5556 = ((BgL_ev_ifz00_bglt) BgL_new1063z00_3872);
				return ((obj_t) BgL_auxz00_5556);
			}
		}

	}



/* &lambda1517 */
	BgL_ev_ifz00_bglt BGl_z62lambda1517z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3873)
	{
		{	/* Eval/evaluate_types.scm 62 */
			{	/* Eval/evaluate_types.scm 62 */
				BgL_ev_ifz00_bglt BgL_new1062z00_4345;

				BgL_new1062z00_4345 =
					((BgL_ev_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_ifz00_bgl))));
				{	/* Eval/evaluate_types.scm 62 */
					long BgL_arg1521z00_4346;

					BgL_arg1521z00_4346 =
						BGL_CLASS_NUM(BGl_ev_ifz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1062z00_4345), BgL_arg1521z00_4346);
				}
				return BgL_new1062z00_4345;
			}
		}

	}



/* &lambda1514 */
	BgL_ev_ifz00_bglt BGl_z62lambda1514z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3874, obj_t BgL_p1059z00_3875, obj_t BgL_t1060z00_3876,
		obj_t BgL_e1061z00_3877)
	{
		{	/* Eval/evaluate_types.scm 62 */
			{	/* Eval/evaluate_types.scm 62 */
				BgL_ev_ifz00_bglt BgL_new1205z00_4350;

				{	/* Eval/evaluate_types.scm 62 */
					BgL_ev_ifz00_bglt BgL_new1204z00_4351;

					BgL_new1204z00_4351 =
						((BgL_ev_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_ifz00_bgl))));
					{	/* Eval/evaluate_types.scm 62 */
						long BgL_arg1516z00_4352;

						BgL_arg1516z00_4352 =
							BGL_CLASS_NUM(BGl_ev_ifz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1204z00_4351), BgL_arg1516z00_4352);
					}
					BgL_new1205z00_4350 = BgL_new1204z00_4351;
				}
				((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1205z00_4350))->BgL_pz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_p1059z00_3875)),
					BUNSPEC);
				((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1205z00_4350))->BgL_tz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_t1060z00_3876)),
					BUNSPEC);
				((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1205z00_4350))->BgL_ez00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_e1061z00_3877)),
					BUNSPEC);
				return BgL_new1205z00_4350;
			}
		}

	}



/* &lambda1539 */
	obj_t BGl_z62lambda1539z62zz__evaluate_typesz00(obj_t BgL_envz00_3878,
		obj_t BgL_oz00_3879, obj_t BgL_vz00_3880)
	{
		{	/* Eval/evaluate_types.scm 62 */
			return
				((((BgL_ev_ifz00_bglt) COBJECT(
							((BgL_ev_ifz00_bglt) BgL_oz00_3879)))->BgL_ez00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3880)), BUNSPEC);
		}

	}



/* &lambda1538 */
	BgL_ev_exprz00_bglt BGl_z62lambda1538z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3881, obj_t BgL_oz00_3882)
	{
		{	/* Eval/evaluate_types.scm 62 */
			return
				(((BgL_ev_ifz00_bglt) COBJECT(
						((BgL_ev_ifz00_bglt) BgL_oz00_3882)))->BgL_ez00);
		}

	}



/* &lambda1532 */
	obj_t BGl_z62lambda1532z62zz__evaluate_typesz00(obj_t BgL_envz00_3883,
		obj_t BgL_oz00_3884, obj_t BgL_vz00_3885)
	{
		{	/* Eval/evaluate_types.scm 62 */
			return
				((((BgL_ev_ifz00_bglt) COBJECT(
							((BgL_ev_ifz00_bglt) BgL_oz00_3884)))->BgL_tz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3885)), BUNSPEC);
		}

	}



/* &lambda1531 */
	BgL_ev_exprz00_bglt BGl_z62lambda1531z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3886, obj_t BgL_oz00_3887)
	{
		{	/* Eval/evaluate_types.scm 62 */
			return
				(((BgL_ev_ifz00_bglt) COBJECT(
						((BgL_ev_ifz00_bglt) BgL_oz00_3887)))->BgL_tz00);
		}

	}



/* &lambda1527 */
	obj_t BGl_z62lambda1527z62zz__evaluate_typesz00(obj_t BgL_envz00_3888,
		obj_t BgL_oz00_3889, obj_t BgL_vz00_3890)
	{
		{	/* Eval/evaluate_types.scm 62 */
			return
				((((BgL_ev_ifz00_bglt) COBJECT(
							((BgL_ev_ifz00_bglt) BgL_oz00_3889)))->BgL_pz00) =
				((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_vz00_3890)), BUNSPEC);
		}

	}



/* &lambda1526 */
	BgL_ev_exprz00_bglt BGl_z62lambda1526z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3891, obj_t BgL_oz00_3892)
	{
		{	/* Eval/evaluate_types.scm 62 */
			return
				(((BgL_ev_ifz00_bglt) COBJECT(
						((BgL_ev_ifz00_bglt) BgL_oz00_3892)))->BgL_pz00);
		}

	}



/* &<@anonymous:1502> */
	obj_t BGl_z62zc3z04anonymousza31502ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3893, obj_t BgL_new1057z00_3894)
	{
		{	/* Eval/evaluate_types.scm 60 */
			{
				BgL_ev_littz00_bglt BgL_auxz00_5615;

				((((BgL_ev_littz00_bglt) COBJECT(
								((BgL_ev_littz00_bglt) BgL_new1057z00_3894)))->BgL_valuez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5615 = ((BgL_ev_littz00_bglt) BgL_new1057z00_3894);
				return ((obj_t) BgL_auxz00_5615);
			}
		}

	}



/* &lambda1500 */
	BgL_ev_littz00_bglt BGl_z62lambda1500z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3895)
	{
		{	/* Eval/evaluate_types.scm 60 */
			{	/* Eval/evaluate_types.scm 60 */
				BgL_ev_littz00_bglt BgL_new1056z00_4363;

				BgL_new1056z00_4363 =
					((BgL_ev_littz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_littz00_bgl))));
				{	/* Eval/evaluate_types.scm 60 */
					long BgL_arg1501z00_4364;

					BgL_arg1501z00_4364 =
						BGL_CLASS_NUM(BGl_ev_littz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1056z00_4363), BgL_arg1501z00_4364);
				}
				return BgL_new1056z00_4363;
			}
		}

	}



/* &lambda1498 */
	BgL_ev_littz00_bglt BGl_z62lambda1498z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3896, obj_t BgL_value1055z00_3897)
	{
		{	/* Eval/evaluate_types.scm 60 */
			{	/* Eval/evaluate_types.scm 60 */
				BgL_ev_littz00_bglt BgL_new1202z00_4365;

				{	/* Eval/evaluate_types.scm 60 */
					BgL_ev_littz00_bglt BgL_new1201z00_4366;

					BgL_new1201z00_4366 =
						((BgL_ev_littz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_littz00_bgl))));
					{	/* Eval/evaluate_types.scm 60 */
						long BgL_arg1499z00_4367;

						BgL_arg1499z00_4367 =
							BGL_CLASS_NUM(BGl_ev_littz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1201z00_4366), BgL_arg1499z00_4367);
					}
					BgL_new1202z00_4365 = BgL_new1201z00_4366;
				}
				((((BgL_ev_littz00_bglt) COBJECT(BgL_new1202z00_4365))->BgL_valuez00) =
					((obj_t) BgL_value1055z00_3897), BUNSPEC);
				return BgL_new1202z00_4365;
			}
		}

	}



/* &lambda1507 */
	obj_t BGl_z62lambda1507z62zz__evaluate_typesz00(obj_t BgL_envz00_3898,
		obj_t BgL_oz00_3899, obj_t BgL_vz00_3900)
	{
		{	/* Eval/evaluate_types.scm 60 */
			return
				((((BgL_ev_littz00_bglt) COBJECT(
							((BgL_ev_littz00_bglt) BgL_oz00_3899)))->BgL_valuez00) =
				((obj_t) BgL_vz00_3900), BUNSPEC);
		}

	}



/* &lambda1506 */
	obj_t BGl_z62lambda1506z62zz__evaluate_typesz00(obj_t BgL_envz00_3901,
		obj_t BgL_oz00_3902)
	{
		{	/* Eval/evaluate_types.scm 60 */
			return
				(((BgL_ev_littz00_bglt) COBJECT(
						((BgL_ev_littz00_bglt) BgL_oz00_3902)))->BgL_valuez00);
		}

	}



/* &<@anonymous:1473> */
	obj_t BGl_z62zc3z04anonymousza31473ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3903, obj_t BgL_new1053z00_3904)
	{
		{	/* Eval/evaluate_types.scm 55 */
			{
				BgL_ev_globalz00_bglt BgL_auxz00_5633;

				((((BgL_ev_globalz00_bglt) COBJECT(
								((BgL_ev_globalz00_bglt) BgL_new1053z00_3904)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_globalz00_bglt) COBJECT(((BgL_ev_globalz00_bglt)
									BgL_new1053z00_3904)))->BgL_namez00) =
					((obj_t) BGl_symbol2706z00zz__evaluate_typesz00), BUNSPEC);
				((((BgL_ev_globalz00_bglt) COBJECT(((BgL_ev_globalz00_bglt)
									BgL_new1053z00_3904)))->BgL_modz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5633 = ((BgL_ev_globalz00_bglt) BgL_new1053z00_3904);
				return ((obj_t) BgL_auxz00_5633);
			}
		}

	}



/* &lambda1470 */
	BgL_ev_globalz00_bglt BGl_z62lambda1470z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3905)
	{
		{	/* Eval/evaluate_types.scm 55 */
			{	/* Eval/evaluate_types.scm 55 */
				BgL_ev_globalz00_bglt BgL_new1052z00_4371;

				BgL_new1052z00_4371 =
					((BgL_ev_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_globalz00_bgl))));
				{	/* Eval/evaluate_types.scm 55 */
					long BgL_arg1472z00_4372;

					BgL_arg1472z00_4372 =
						BGL_CLASS_NUM(BGl_ev_globalz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1052z00_4371), BgL_arg1472z00_4372);
				}
				return BgL_new1052z00_4371;
			}
		}

	}



/* &lambda1468 */
	BgL_ev_globalz00_bglt BGl_z62lambda1468z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3906, obj_t BgL_loc1049z00_3907, obj_t BgL_name1050z00_3908,
		obj_t BgL_mod1051z00_3909)
	{
		{	/* Eval/evaluate_types.scm 55 */
			{	/* Eval/evaluate_types.scm 55 */
				BgL_ev_globalz00_bglt BgL_new1200z00_4374;

				{	/* Eval/evaluate_types.scm 55 */
					BgL_ev_globalz00_bglt BgL_new1198z00_4375;

					BgL_new1198z00_4375 =
						((BgL_ev_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_globalz00_bgl))));
					{	/* Eval/evaluate_types.scm 55 */
						long BgL_arg1469z00_4376;

						BgL_arg1469z00_4376 =
							BGL_CLASS_NUM(BGl_ev_globalz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1198z00_4375), BgL_arg1469z00_4376);
					}
					BgL_new1200z00_4374 = BgL_new1198z00_4375;
				}
				((((BgL_ev_globalz00_bglt) COBJECT(BgL_new1200z00_4374))->BgL_locz00) =
					((obj_t) BgL_loc1049z00_3907), BUNSPEC);
				((((BgL_ev_globalz00_bglt) COBJECT(BgL_new1200z00_4374))->BgL_namez00) =
					((obj_t) ((obj_t) BgL_name1050z00_3908)), BUNSPEC);
				((((BgL_ev_globalz00_bglt) COBJECT(BgL_new1200z00_4374))->BgL_modz00) =
					((obj_t) BgL_mod1051z00_3909), BUNSPEC);
				return BgL_new1200z00_4374;
			}
		}

	}



/* &lambda1489 */
	obj_t BGl_z62lambda1489z62zz__evaluate_typesz00(obj_t BgL_envz00_3910,
		obj_t BgL_oz00_3911, obj_t BgL_vz00_3912)
	{
		{	/* Eval/evaluate_types.scm 55 */
			return
				((((BgL_ev_globalz00_bglt) COBJECT(
							((BgL_ev_globalz00_bglt) BgL_oz00_3911)))->BgL_modz00) =
				((obj_t) BgL_vz00_3912), BUNSPEC);
		}

	}



/* &lambda1488 */
	obj_t BGl_z62lambda1488z62zz__evaluate_typesz00(obj_t BgL_envz00_3913,
		obj_t BgL_oz00_3914)
	{
		{	/* Eval/evaluate_types.scm 55 */
			return
				(((BgL_ev_globalz00_bglt) COBJECT(
						((BgL_ev_globalz00_bglt) BgL_oz00_3914)))->BgL_modz00);
		}

	}



/* &lambda1484 */
	obj_t BGl_z62lambda1484z62zz__evaluate_typesz00(obj_t BgL_envz00_3915,
		obj_t BgL_oz00_3916, obj_t BgL_vz00_3917)
	{
		{	/* Eval/evaluate_types.scm 55 */
			return
				((((BgL_ev_globalz00_bglt) COBJECT(
							((BgL_ev_globalz00_bglt) BgL_oz00_3916)))->BgL_namez00) = ((obj_t)
					((obj_t) BgL_vz00_3917)), BUNSPEC);
		}

	}



/* &lambda1483 */
	obj_t BGl_z62lambda1483z62zz__evaluate_typesz00(obj_t BgL_envz00_3918,
		obj_t BgL_oz00_3919)
	{
		{	/* Eval/evaluate_types.scm 55 */
			return
				(((BgL_ev_globalz00_bglt) COBJECT(
						((BgL_ev_globalz00_bglt) BgL_oz00_3919)))->BgL_namez00);
		}

	}



/* &lambda1479 */
	obj_t BGl_z62lambda1479z62zz__evaluate_typesz00(obj_t BgL_envz00_3920,
		obj_t BgL_oz00_3921, obj_t BgL_vz00_3922)
	{
		{	/* Eval/evaluate_types.scm 55 */
			return
				((((BgL_ev_globalz00_bglt) COBJECT(
							((BgL_ev_globalz00_bglt) BgL_oz00_3921)))->BgL_locz00) =
				((obj_t) BgL_vz00_3922), BUNSPEC);
		}

	}



/* &lambda1478 */
	obj_t BGl_z62lambda1478z62zz__evaluate_typesz00(obj_t BgL_envz00_3923,
		obj_t BgL_oz00_3924)
	{
		{	/* Eval/evaluate_types.scm 55 */
			return
				(((BgL_ev_globalz00_bglt) COBJECT(
						((BgL_ev_globalz00_bglt) BgL_oz00_3924)))->BgL_locz00);
		}

	}



/* &<@anonymous:1445> */
	obj_t BGl_z62zc3z04anonymousza31445ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3925, obj_t BgL_new1047z00_3926)
	{
		{	/* Eval/evaluate_types.scm 51 */
			{
				BgL_ev_varz00_bglt BgL_auxz00_5667;

				((((BgL_ev_varz00_bglt) COBJECT(
								((BgL_ev_varz00_bglt) BgL_new1047z00_3926)))->BgL_namez00) =
					((obj_t) BGl_symbol2706z00zz__evaluate_typesz00), BUNSPEC);
				((((BgL_ev_varz00_bglt) COBJECT(((BgL_ev_varz00_bglt)
									BgL_new1047z00_3926)))->BgL_effz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_ev_varz00_bglt) COBJECT(((BgL_ev_varz00_bglt)
									BgL_new1047z00_3926)))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_5667 = ((BgL_ev_varz00_bglt) BgL_new1047z00_3926);
				return ((obj_t) BgL_auxz00_5667);
			}
		}

	}



/* &lambda1443 */
	BgL_ev_varz00_bglt BGl_z62lambda1443z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3927)
	{
		{	/* Eval/evaluate_types.scm 51 */
			{	/* Eval/evaluate_types.scm 51 */
				BgL_ev_varz00_bglt BgL_new1046z00_4385;

				BgL_new1046z00_4385 =
					((BgL_ev_varz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_varz00_bgl))));
				{	/* Eval/evaluate_types.scm 51 */
					long BgL_arg1444z00_4386;

					BgL_arg1444z00_4386 =
						BGL_CLASS_NUM(BGl_ev_varz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1046z00_4385), BgL_arg1444z00_4386);
				}
				return BgL_new1046z00_4385;
			}
		}

	}



/* &lambda1441 */
	BgL_ev_varz00_bglt BGl_z62lambda1441z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3928, obj_t BgL_name1043z00_3929, obj_t BgL_eff1044z00_3930,
		obj_t BgL_type1045z00_3931)
	{
		{	/* Eval/evaluate_types.scm 51 */
			{	/* Eval/evaluate_types.scm 51 */
				BgL_ev_varz00_bglt BgL_new1197z00_4388;

				{	/* Eval/evaluate_types.scm 51 */
					BgL_ev_varz00_bglt BgL_new1196z00_4389;

					BgL_new1196z00_4389 =
						((BgL_ev_varz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_varz00_bgl))));
					{	/* Eval/evaluate_types.scm 51 */
						long BgL_arg1442z00_4390;

						BgL_arg1442z00_4390 =
							BGL_CLASS_NUM(BGl_ev_varz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1196z00_4389), BgL_arg1442z00_4390);
					}
					BgL_new1197z00_4388 = BgL_new1196z00_4389;
				}
				((((BgL_ev_varz00_bglt) COBJECT(BgL_new1197z00_4388))->BgL_namez00) =
					((obj_t) ((obj_t) BgL_name1043z00_3929)), BUNSPEC);
				((((BgL_ev_varz00_bglt) COBJECT(BgL_new1197z00_4388))->BgL_effz00) =
					((obj_t) BgL_eff1044z00_3930), BUNSPEC);
				((((BgL_ev_varz00_bglt) COBJECT(BgL_new1197z00_4388))->BgL_typez00) =
					((obj_t) BgL_type1045z00_3931), BUNSPEC);
				return BgL_new1197z00_4388;
			}
		}

	}



/* &lambda1462 */
	obj_t BGl_z62lambda1462z62zz__evaluate_typesz00(obj_t BgL_envz00_3932,
		obj_t BgL_oz00_3933, obj_t BgL_vz00_3934)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return
				((((BgL_ev_varz00_bglt) COBJECT(
							((BgL_ev_varz00_bglt) BgL_oz00_3933)))->BgL_typez00) =
				((obj_t) BgL_vz00_3934), BUNSPEC);
		}

	}



/* &lambda1461 */
	obj_t BGl_z62lambda1461z62zz__evaluate_typesz00(obj_t BgL_envz00_3935,
		obj_t BgL_oz00_3936)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return
				(((BgL_ev_varz00_bglt) COBJECT(
						((BgL_ev_varz00_bglt) BgL_oz00_3936)))->BgL_typez00);
		}

	}



/* &<@anonymous:1457> */
	obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3937)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1456 */
	obj_t BGl_z62lambda1456z62zz__evaluate_typesz00(obj_t BgL_envz00_3938,
		obj_t BgL_oz00_3939, obj_t BgL_vz00_3940)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return
				((((BgL_ev_varz00_bglt) COBJECT(
							((BgL_ev_varz00_bglt) BgL_oz00_3939)))->BgL_effz00) =
				((obj_t) BgL_vz00_3940), BUNSPEC);
		}

	}



/* &lambda1455 */
	obj_t BGl_z62lambda1455z62zz__evaluate_typesz00(obj_t BgL_envz00_3941,
		obj_t BgL_oz00_3942)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return
				(((BgL_ev_varz00_bglt) COBJECT(
						((BgL_ev_varz00_bglt) BgL_oz00_3942)))->BgL_effz00);
		}

	}



/* &lambda1450 */
	obj_t BGl_z62lambda1450z62zz__evaluate_typesz00(obj_t BgL_envz00_3943,
		obj_t BgL_oz00_3944, obj_t BgL_vz00_3945)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return
				((((BgL_ev_varz00_bglt) COBJECT(
							((BgL_ev_varz00_bglt) BgL_oz00_3944)))->BgL_namez00) = ((obj_t)
					((obj_t) BgL_vz00_3945)), BUNSPEC);
		}

	}



/* &lambda1449 */
	obj_t BGl_z62lambda1449z62zz__evaluate_typesz00(obj_t BgL_envz00_3946,
		obj_t BgL_oz00_3947)
	{
		{	/* Eval/evaluate_types.scm 51 */
			return
				(((BgL_ev_varz00_bglt) COBJECT(
						((BgL_ev_varz00_bglt) BgL_oz00_3947)))->BgL_namez00);
		}

	}



/* &<@anonymous:1435> */
	obj_t BGl_z62zc3z04anonymousza31435ze3ze5zz__evaluate_typesz00(obj_t
		BgL_envz00_3948, obj_t BgL_new1041z00_3949)
	{
		{	/* Eval/evaluate_types.scm 49 */
			return ((obj_t) ((BgL_ev_exprz00_bglt) BgL_new1041z00_3949));
		}

	}



/* &lambda1431 */
	BgL_ev_exprz00_bglt BGl_z62lambda1431z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3950)
	{
		{	/* Eval/evaluate_types.scm 49 */
			{	/* Eval/evaluate_types.scm 49 */
				BgL_ev_exprz00_bglt BgL_new1040z00_4399;

				BgL_new1040z00_4399 =
					((BgL_ev_exprz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ev_exprz00_bgl))));
				{	/* Eval/evaluate_types.scm 49 */
					long BgL_arg1434z00_4400;

					BgL_arg1434z00_4400 =
						BGL_CLASS_NUM(BGl_ev_exprz00zz__evaluate_typesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1040z00_4399), BgL_arg1434z00_4400);
				}
				return BgL_new1040z00_4399;
			}
		}

	}



/* &lambda1429 */
	BgL_ev_exprz00_bglt BGl_z62lambda1429z62zz__evaluate_typesz00(obj_t
		BgL_envz00_3951)
	{
		{	/* Eval/evaluate_types.scm 49 */
			{	/* Eval/evaluate_types.scm 49 */
				BgL_ev_exprz00_bglt BgL_new1195z00_4401;

				{	/* Eval/evaluate_types.scm 49 */
					BgL_ev_exprz00_bglt BgL_new1194z00_4402;

					BgL_new1194z00_4402 =
						((BgL_ev_exprz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ev_exprz00_bgl))));
					{	/* Eval/evaluate_types.scm 49 */
						long BgL_arg1430z00_4403;

						BgL_arg1430z00_4403 =
							BGL_CLASS_NUM(BGl_ev_exprz00zz__evaluate_typesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1194z00_4402), BgL_arg1430z00_4403);
					}
					BgL_new1195z00_4401 = BgL_new1194z00_4402;
				}
				return BgL_new1195z00_4401;
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_typesz00(void)
	{
		{	/* Eval/evaluate_types.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
			return BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2383z00zz__evaluate_typesz00));
		}

	}

#ifdef __cplusplus
}
#endif
