/*===========================================================================*/
/*   (Eval/evenv.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evenv.scm -indent -o objs/obj_u/Eval/evenv.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVENV_TYPE_DEFINITIONS
#define BGL___EVENV_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62warningz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                    *BgL_z62warningz62_bglt;

	typedef struct BgL_z62evalzd2warningzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                           *BgL_z62evalzd2warningzb0_bglt;


#endif													// BGL___EVENV_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62definezd2primopzd2refz12z70zz__evenvz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_initzd2thezd2globalzd2environmentz12zc0zz__evenvz00(void);
	extern obj_t BGl_z62evalzd2warningzb0zz__objectz00;
	static obj_t BGl_z62makezd2evalzd2globalz62zz__evenvz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62evalzd2globalzd2namez62zz__evenvz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evenvz00 = BUNSPEC;
	static obj_t BGl_z62evalzd2globalzd2valuez62zz__evenvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62definezd2primopz12za2zz__evenvz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bindzd2evalzd2globalz12z70zz__evenvz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	static obj_t BGl_z62evalzd2globalzf3z43zz__evenvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2evalzd2globalzd2valuez12zc0zz__evenvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2lookupzd2zz__evenvz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2globalzf3z21zz__evenvz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2globalzd2valuez00zz__evenvz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evalzd2globalzd2modulezd2setz12zc0zz__evenvz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evenvz00(void);
	static obj_t BGl_z62evalzd2globalzd2modulezd2setz12za2zz__evenvz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__evenvz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evenvz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evenvz00(void);
	BGL_EXPORTED_DECL obj_t BGl_bindzd2evalzd2globalz12z12zz__evenvz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__evenvz00(void);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2globalzd2modulez00zz__evenvz00(obj_t);
	static obj_t BGl_z62unbindzd2primopz12za2zz__evenvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_definezd2primopz12zc0zz__evenvz00(obj_t, obj_t);
	extern obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62evalzd2globalzd2tagzd2setz12za2zz__evenvz00(obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62evalzd2globalzd2modulez62zz__evenvz00(obj_t, obj_t);
	extern obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2evalzd2globalz00zz__evenvz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2globalzd2namez00zz__evenvz00(obj_t);
	extern obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__evenvz00(void);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2globalzd2tagzd2setz12zc0zz__evenvz00(obj_t,
		int);
	static obj_t BGl_symbol1637z00zz__evenvz00 = BUNSPEC;
	static obj_t BGl_z62definezd2primopzd2refzf2locz12z82zz__evenvz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62evalzd2lookupzb0zz__evenvz00(obj_t, obj_t);
	static obj_t BGl_symbol1642z00zz__evenvz00 = BUNSPEC;
	static obj_t BGl_symbol1647z00zz__evenvz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_evalzd2globalzd2locz00zz__evenvz00(obj_t);
	static obj_t BGl_z62evalzd2globalzd2locz62zz__evenvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_definezd2primopzd2refz12z12zz__evenvz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unbindzd2primopz12zc0zz__evenvz00(obj_t);
	static obj_t
		BGl_z62initzd2thezd2globalzd2environmentz12za2zz__evenvz00(obj_t);
	static obj_t BGl_z62setzd2evalzd2globalzd2valuez12za2zz__evenvz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_evalzd2globalzd2tagz00zz__evenvz00(obj_t);
	static obj_t BGl_z62evalzd2globalzd2tagz62zz__evenvz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2globalzd2loczd2envzd2zz__evenvz00,
		BgL_bgl_za762evalza7d2global1662z00,
		BGl_z62evalzd2globalzd2locz62zz__evenvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2globalzd2modulezd2setz12zd2envz12zz__evenvz00,
		BgL_bgl_za762evalza7d2global1663z00,
		BGl_z62evalzd2globalzd2modulezd2setz12za2zz__evenvz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unbindzd2primopz12zd2envz12zz__evenvz00,
		BgL_bgl_za762unbindza7d2prim1664z00,
		BGl_z62unbindzd2primopz12za2zz__evenvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2globalzd2valuezd2envzd2zz__evenvz00,
		BgL_bgl_za762evalza7d2global1665z00,
		BGl_z62evalzd2globalzd2valuez62zz__evenvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_definezd2primopzd2refz12zd2envzc0zz__evenvz00,
		BgL_bgl_za762defineza7d2prim1666z00,
		BGl_z62definezd2primopzd2refz12z70zz__evenvz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_definezd2primopz12zd2envz12zz__evenvz00,
		BgL_bgl_za762defineza7d2prim1667z00,
		BGl_z62definezd2primopz12za2zz__evenvz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2globalzf3zd2envzf3zz__evenvz00,
		BgL_bgl_za762evalza7d2global1668z00, BGl_z62evalzd2globalzf3z43zz__evenvz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1638z00zz__evenvz00,
		BgL_bgl_string1638za700za7za7_1669za7, "nothing", 7);
	      DEFINE_STRING(BGl_string1639z00zz__evenvz00,
		BgL_bgl_string1639za700za7za7_1670za7, "/tmp/bigloo/runtime/Eval/evenv.scm",
		34);
	      DEFINE_STRING(BGl_string1640z00zz__evenvz00,
		BgL_bgl_string1640za700za7za7_1671za7, "&make-eval-global", 17);
	      DEFINE_STRING(BGl_string1641z00zz__evenvz00,
		BgL_bgl_string1641za700za7za7_1672za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1643z00zz__evenvz00,
		BgL_bgl_string1643za700za7za7_1673za7, "_0000", 5);
	      DEFINE_STRING(BGl_string1644z00zz__evenvz00,
		BgL_bgl_string1644za700za7za7_1674za7, "&bind-eval-global!", 18);
	      DEFINE_STRING(BGl_string1645z00zz__evenvz00,
		BgL_bgl_string1645za700za7za7_1675za7, "vector", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bindzd2evalzd2globalz12zd2envzc0zz__evenvz00,
		BgL_bgl_za762bindza7d2evalza7d1676za7,
		BGl_z62bindzd2evalzd2globalz12z70zz__evenvz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1646z00zz__evenvz00,
		BgL_bgl_string1646za700za7za7_1677za7, "&unbind-primop!", 15);
	      DEFINE_STRING(BGl_string1648z00zz__evenvz00,
		BgL_bgl_string1648za700za7za7_1678za7, "_0000_assert", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2globalzd2namezd2envzd2zz__evenvz00,
		BgL_bgl_za762evalza7d2global1679z00,
		BGl_z62evalzd2globalzd2namez62zz__evenvz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1649z00zz__evenvz00,
		BgL_bgl_string1649za700za7za7_1680za7, "&define-primop!", 15);
	      DEFINE_STRING(BGl_string1650z00zz__evenvz00,
		BgL_bgl_string1650za700za7za7_1681za7, "overriding compiled constant", 28);
	      DEFINE_STRING(BGl_string1651z00zz__evenvz00,
		BgL_bgl_string1651za700za7za7_1682za7, "&define-primop-ref!", 19);
	      DEFINE_STRING(BGl_string1652z00zz__evenvz00,
		BgL_bgl_string1652za700za7za7_1683za7, "&define-primop-ref/loc!", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2globalzd2tagzd2envzd2zz__evenvz00,
		BgL_bgl_za762evalza7d2global1684z00,
		BGl_z62evalzd2globalzd2tagz62zz__evenvz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1653z00zz__evenvz00,
		BgL_bgl_string1653za700za7za7_1685za7, "&eval-lookup", 12);
	      DEFINE_STRING(BGl_string1654z00zz__evenvz00,
		BgL_bgl_string1654za700za7za7_1686za7, "&eval-global-tag", 16);
	      DEFINE_STRING(BGl_string1655z00zz__evenvz00,
		BgL_bgl_string1655za700za7za7_1687za7, "&eval-global-tag-set!", 21);
	      DEFINE_STRING(BGl_string1656z00zz__evenvz00,
		BgL_bgl_string1656za700za7za7_1688za7, "bint", 4);
	      DEFINE_STRING(BGl_string1657z00zz__evenvz00,
		BgL_bgl_string1657za700za7za7_1689za7, "&eval-global-name", 17);
	      DEFINE_STRING(BGl_string1658z00zz__evenvz00,
		BgL_bgl_string1658za700za7za7_1690za7, "&eval-global-value", 18);
	      DEFINE_STRING(BGl_string1659z00zz__evenvz00,
		BgL_bgl_string1659za700za7za7_1691za7, "&set-eval-global-value!", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2globalzd2modulezd2envzd2zz__evenvz00,
		BgL_bgl_za762evalza7d2global1692z00,
		BGl_z62evalzd2globalzd2modulez62zz__evenvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initzd2thezd2globalzd2environmentz12zd2envz12zz__evenvz00,
		BgL_bgl_za762initza7d2theza7d21693za7,
		BGl_z62initzd2thezd2globalzd2environmentz12za2zz__evenvz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2evalzd2globalzd2valuez12zd2envz12zz__evenvz00,
		BgL_bgl_za762setza7d2evalza7d21694za7,
		BGl_z62setzd2evalzd2globalzd2valuez12za2zz__evenvz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1660z00zz__evenvz00,
		BgL_bgl_string1660za700za7za7_1695za7, "&eval-global-module-set!", 24);
	      DEFINE_STRING(BGl_string1661z00zz__evenvz00,
		BgL_bgl_string1661za700za7za7_1696za7, "__evenv", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_definezd2primopzd2refzf2locz12zd2envz32zz__evenvz00,
		BgL_bgl_za762defineza7d2prim1697z00,
		BGl_z62definezd2primopzd2refzf2locz12z82zz__evenvz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2evalzd2globalzd2envzd2zz__evenvz00,
		BgL_bgl_za762makeza7d2evalza7d1698za7,
		BGl_z62makezd2evalzd2globalz62zz__evenvz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2globalzd2tagzd2setz12zd2envz12zz__evenvz00,
		BgL_bgl_za762evalza7d2global1699z00,
		BGl_z62evalzd2globalzd2tagzd2setz12za2zz__evenvz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2lookupzd2envz00zz__evenvz00,
		BgL_bgl_za762evalza7d2lookup1700z00, BGl_z62evalzd2lookupzb0zz__evenvz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evenvz00));
		     ADD_ROOT((void *) (&BGl_symbol1637z00zz__evenvz00));
		     ADD_ROOT((void *) (&BGl_symbol1642z00zz__evenvz00));
		     ADD_ROOT((void *) (&BGl_symbol1647z00zz__evenvz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long
		BgL_checksumz00_1914, char *BgL_fromz00_1915)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evenvz00))
				{
					BGl_requirezd2initializa7ationz75zz__evenvz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evenvz00();
					BGl_cnstzd2initzd2zz__evenvz00();
					BGl_importedzd2moduleszd2initz00zz__evenvz00();
					return BGl_methodzd2initzd2zz__evenvz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 14 */
			BGl_symbol1637z00zz__evenvz00 =
				bstring_to_symbol(BGl_string1638z00zz__evenvz00);
			BGl_symbol1642z00zz__evenvz00 =
				bstring_to_symbol(BGl_string1643z00zz__evenvz00);
			return (BGl_symbol1647z00zz__evenvz00 =
				bstring_to_symbol(BGl_string1648z00zz__evenvz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* init-the-global-environment! */
	BGL_EXPORTED_DEF obj_t
		BGl_initzd2thezd2globalzd2environmentz12zc0zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 87 */
			return BGl_symbol1637z00zz__evenvz00;
		}

	}



/* &init-the-global-environment! */
	obj_t BGl_z62initzd2thezd2globalzd2environmentz12za2zz__evenvz00(obj_t
		BgL_envz00_1837)
	{
		{	/* Eval/evenv.scm 87 */
			return BGl_initzd2thezd2globalzd2environmentz12zc0zz__evenvz00();
		}

	}



/* eval-global? */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2globalzf3z21zz__evenvz00(obj_t
		BgL_variablez00_3)
	{
		{	/* Eval/evenv.scm 107 */
			if (VECTORP(BgL_variablez00_3))
				{	/* Eval/evenv.scm 108 */
					return BBOOL((VECTOR_LENGTH(BgL_variablez00_3) == 5L));
				}
			else
				{	/* Eval/evenv.scm 108 */
					return BFALSE;
				}
		}

	}



/* &eval-global? */
	obj_t BGl_z62evalzd2globalzf3z43zz__evenvz00(obj_t BgL_envz00_1838,
		obj_t BgL_variablez00_1839)
	{
		{	/* Eval/evenv.scm 107 */
			return BGl_evalzd2globalzf3z21zz__evenvz00(BgL_variablez00_1839);
		}

	}



/* make-eval-global */
	BGL_EXPORTED_DEF obj_t BGl_makezd2evalzd2globalz00zz__evenvz00(obj_t
		BgL_idz00_4, obj_t BgL_modz00_5, obj_t BgL_locz00_6)
	{
		{	/* Eval/evenv.scm 115 */
			{	/* Eval/evenv.scm 116 */
				obj_t BgL_v1084z00_1913;

				BgL_v1084z00_1913 = create_vector(5L);
				VECTOR_SET(BgL_v1084z00_1913, 0L, BINT(2L));
				VECTOR_SET(BgL_v1084z00_1913, 1L, BgL_idz00_4);
				VECTOR_SET(BgL_v1084z00_1913, 2L, BUNSPEC);
				VECTOR_SET(BgL_v1084z00_1913, 3L, BgL_modz00_5);
				VECTOR_SET(BgL_v1084z00_1913, 4L, BgL_locz00_6);
				return BgL_v1084z00_1913;
			}
		}

	}



/* &make-eval-global */
	obj_t BGl_z62makezd2evalzd2globalz62zz__evenvz00(obj_t BgL_envz00_1840,
		obj_t BgL_idz00_1841, obj_t BgL_modz00_1842, obj_t BgL_locz00_1843)
	{
		{	/* Eval/evenv.scm 115 */
			{	/* Eval/evenv.scm 116 */
				obj_t BgL_auxz00_1941;

				if (SYMBOLP(BgL_idz00_1841))
					{	/* Eval/evenv.scm 116 */
						BgL_auxz00_1941 = BgL_idz00_1841;
					}
				else
					{
						obj_t BgL_auxz00_1944;

						BgL_auxz00_1944 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(5032L), BGl_string1640z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_idz00_1841);
						FAILURE(BgL_auxz00_1944, BFALSE, BFALSE);
					}
				return
					BGl_makezd2evalzd2globalz00zz__evenvz00(BgL_auxz00_1941,
					BgL_modz00_1842, BgL_locz00_1843);
			}
		}

	}



/* bind-eval-global! */
	BGL_EXPORTED_DEF obj_t BGl_bindzd2evalzd2globalz12z12zz__evenvz00(obj_t
		BgL_namez00_7, obj_t BgL_varz00_8)
	{
		{	/* Eval/evenv.scm 121 */
			return
				BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_namez00_7,
				BGl_symbol1642z00zz__evenvz00, BgL_varz00_8);
		}

	}



/* &bind-eval-global! */
	obj_t BGl_z62bindzd2evalzd2globalz12z70zz__evenvz00(obj_t BgL_envz00_1844,
		obj_t BgL_namez00_1845, obj_t BgL_varz00_1846)
	{
		{	/* Eval/evenv.scm 121 */
			{	/* Eval/evenv.scm 122 */
				obj_t BgL_auxz00_1957;
				obj_t BgL_auxz00_1950;

				if (VECTORP(BgL_varz00_1846))
					{	/* Eval/evenv.scm 122 */
						BgL_auxz00_1957 = BgL_varz00_1846;
					}
				else
					{
						obj_t BgL_auxz00_1960;

						BgL_auxz00_1960 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(5331L), BGl_string1644z00zz__evenvz00,
							BGl_string1645z00zz__evenvz00, BgL_varz00_1846);
						FAILURE(BgL_auxz00_1960, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_namez00_1845))
					{	/* Eval/evenv.scm 122 */
						BgL_auxz00_1950 = BgL_namez00_1845;
					}
				else
					{
						obj_t BgL_auxz00_1953;

						BgL_auxz00_1953 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(5331L), BGl_string1644z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_namez00_1845);
						FAILURE(BgL_auxz00_1953, BFALSE, BFALSE);
					}
				return
					BGl_bindzd2evalzd2globalz12z12zz__evenvz00(BgL_auxz00_1950,
					BgL_auxz00_1957);
			}
		}

	}



/* unbind-primop! */
	BGL_EXPORTED_DEF obj_t BGl_unbindzd2primopz12zc0zz__evenvz00(obj_t
		BgL_namez00_11)
	{
		{	/* Eval/evenv.scm 133 */
			return
				BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_namez00_11,
				BGl_symbol1642z00zz__evenvz00);
		}

	}



/* &unbind-primop! */
	obj_t BGl_z62unbindzd2primopz12za2zz__evenvz00(obj_t BgL_envz00_1847,
		obj_t BgL_namez00_1848)
	{
		{	/* Eval/evenv.scm 133 */
			{	/* Eval/evenv.scm 134 */
				obj_t BgL_auxz00_1966;

				if (SYMBOLP(BgL_namez00_1848))
					{	/* Eval/evenv.scm 134 */
						BgL_auxz00_1966 = BgL_namez00_1848;
					}
				else
					{
						obj_t BgL_auxz00_1969;

						BgL_auxz00_1969 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(5920L), BGl_string1646z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_namez00_1848);
						FAILURE(BgL_auxz00_1969, BFALSE, BFALSE);
					}
				return BGl_unbindzd2primopz12zc0zz__evenvz00(BgL_auxz00_1966);
			}
		}

	}



/* define-primop! */
	BGL_EXPORTED_DEF obj_t BGl_definezd2primopz12zc0zz__evenvz00(obj_t
		BgL_varz00_13, obj_t BgL_valz00_14)
	{
		{	/* Eval/evenv.scm 145 */
			{	/* Eval/evenv.scm 146 */
				obj_t BgL_cellz00_1145;

				{	/* Eval/evenv.scm 195 */
					obj_t BgL_propz00_1596;

					BgL_propz00_1596 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_13,
						BGl_symbol1647z00zz__evenvz00);
					if (CBOOL(BgL_propz00_1596))
						{	/* Eval/evenv.scm 196 */
							BgL_cellz00_1145 = BgL_propz00_1596;
						}
					else
						{	/* Eval/evenv.scm 198 */
							obj_t BgL_propz00_1597;

							BgL_propz00_1597 =
								BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_13,
								BGl_symbol1642z00zz__evenvz00);
							if (CBOOL(BgL_propz00_1597))
								{	/* Eval/evenv.scm 199 */
									BgL_cellz00_1145 = BgL_propz00_1597;
								}
							else
								{	/* Eval/evenv.scm 199 */
									BgL_cellz00_1145 = BFALSE;
								}
						}
				}
				{	/* Eval/evenv.scm 147 */
					bool_t BgL_test1709z00_1980;

					if (VECTORP(BgL_cellz00_1145))
						{	/* Eval/evenv.scm 108 */
							BgL_test1709z00_1980 = (VECTOR_LENGTH(BgL_cellz00_1145) == 5L);
						}
					else
						{	/* Eval/evenv.scm 108 */
							BgL_test1709z00_1980 = ((bool_t) 0);
						}
					if (BgL_test1709z00_1980)
						{	/* Eval/evenv.scm 147 */
							return VECTOR_SET(BgL_cellz00_1145, 2L, BgL_valz00_14);
						}
					else
						{	/* Eval/evenv.scm 148 */
							obj_t BgL_arg1187z00_1147;

							{	/* Eval/evenv.scm 148 */
								obj_t BgL_v1085z00_1148;

								BgL_v1085z00_1148 = create_vector(5L);
								VECTOR_SET(BgL_v1085z00_1148, 0L, BINT(0L));
								VECTOR_SET(BgL_v1085z00_1148, 1L, BgL_varz00_13);
								VECTOR_SET(BgL_v1085z00_1148, 2L, BgL_valz00_14);
								VECTOR_SET(BgL_v1085z00_1148, 3L, BFALSE);
								VECTOR_SET(BgL_v1085z00_1148, 4L, BFALSE);
								BgL_arg1187z00_1147 = BgL_v1085z00_1148;
							}
							return
								BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_varz00_13,
								BGl_symbol1642z00zz__evenvz00, BgL_arg1187z00_1147);
						}
				}
			}
		}

	}



/* &define-primop! */
	obj_t BGl_z62definezd2primopz12za2zz__evenvz00(obj_t BgL_envz00_1849,
		obj_t BgL_varz00_1850, obj_t BgL_valz00_1851)
	{
		{	/* Eval/evenv.scm 145 */
			{	/* Eval/evenv.scm 146 */
				obj_t BgL_auxz00_1994;

				if (SYMBOLP(BgL_varz00_1850))
					{	/* Eval/evenv.scm 146 */
						BgL_auxz00_1994 = BgL_varz00_1850;
					}
				else
					{
						obj_t BgL_auxz00_1997;

						BgL_auxz00_1997 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(6497L), BGl_string1649z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_varz00_1850);
						FAILURE(BgL_auxz00_1997, BFALSE, BFALSE);
					}
				return
					BGl_definezd2primopz12zc0zz__evenvz00(BgL_auxz00_1994,
					BgL_valz00_1851);
			}
		}

	}



/* define-primop-ref! */
	BGL_EXPORTED_DEF obj_t BGl_definezd2primopzd2refz12z12zz__evenvz00(obj_t
		BgL_varz00_15, obj_t BgL_addrz00_16)
	{
		{	/* Eval/evenv.scm 154 */
			{	/* Eval/evenv.scm 155 */
				obj_t BgL_cellz00_1149;

				{	/* Eval/evenv.scm 195 */
					obj_t BgL_propz00_1612;

					BgL_propz00_1612 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_15,
						BGl_symbol1647z00zz__evenvz00);
					if (CBOOL(BgL_propz00_1612))
						{	/* Eval/evenv.scm 196 */
							BgL_cellz00_1149 = BgL_propz00_1612;
						}
					else
						{	/* Eval/evenv.scm 198 */
							obj_t BgL_propz00_1613;

							BgL_propz00_1613 =
								BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_15,
								BGl_symbol1642z00zz__evenvz00);
							if (CBOOL(BgL_propz00_1613))
								{	/* Eval/evenv.scm 199 */
									BgL_cellz00_1149 = BgL_propz00_1613;
								}
							else
								{	/* Eval/evenv.scm 199 */
									BgL_cellz00_1149 = BFALSE;
								}
						}
				}
				{	/* Eval/evenv.scm 156 */
					bool_t BgL_test1714z00_2008;

					if (VECTORP(BgL_cellz00_1149))
						{	/* Eval/evenv.scm 108 */
							BgL_test1714z00_2008 = (VECTOR_LENGTH(BgL_cellz00_1149) == 5L);
						}
					else
						{	/* Eval/evenv.scm 108 */
							BgL_test1714z00_2008 = ((bool_t) 0);
						}
					if (BgL_test1714z00_2008)
						{	/* Eval/evenv.scm 156 */
							VECTOR_SET(BgL_cellz00_1149, 2L, BgL_addrz00_16);
							{	/* Eval/evenv.scm 162 */
								BgL_z62evalzd2warningzb0_bglt BgL_arg1189z00_1151;

								{	/* Eval/evenv.scm 162 */
									BgL_z62evalzd2warningzb0_bglt BgL_new1040z00_1152;

									{	/* Eval/evenv.scm 163 */
										BgL_z62evalzd2warningzb0_bglt BgL_new1039z00_1155;

										BgL_new1039z00_1155 =
											((BgL_z62evalzd2warningzb0_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62evalzd2warningzb0_bgl))));
										{	/* Eval/evenv.scm 163 */
											long BgL_arg1193z00_1156;

											BgL_arg1193z00_1156 =
												BGL_CLASS_NUM(BGl_z62evalzd2warningzb0zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1039z00_1155),
												BgL_arg1193z00_1156);
										}
										BgL_new1040z00_1152 = BgL_new1039z00_1155;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1040z00_1152)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1040z00_1152)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1040z00_1152)))->BgL_stackz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_2024;

										{	/* Eval/evenv.scm 166 */
											obj_t BgL_list1190z00_1153;

											{	/* Eval/evenv.scm 166 */
												obj_t BgL_arg1191z00_1154;

												BgL_arg1191z00_1154 =
													MAKE_YOUNG_PAIR(BgL_varz00_15, BNIL);
												BgL_list1190z00_1153 =
													MAKE_YOUNG_PAIR(BGl_string1650z00zz__evenvz00,
													BgL_arg1191z00_1154);
											}
											BgL_auxz00_2024 = BgL_list1190z00_1153;
										}
										((((BgL_z62warningz62_bglt) COBJECT(
														((BgL_z62warningz62_bglt) BgL_new1040z00_1152)))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_2024), BUNSPEC);
									}
									BgL_arg1189z00_1151 = BgL_new1040z00_1152;
								}
								return
									BGl_warningzd2notifyzd2zz__errorz00(
									((obj_t) BgL_arg1189z00_1151));
							}
						}
					else
						{	/* Eval/evenv.scm 157 */
							obj_t BgL_arg1194z00_1157;

							{	/* Eval/evenv.scm 157 */
								obj_t BgL_v1086z00_1158;

								BgL_v1086z00_1158 = create_vector(5L);
								VECTOR_SET(BgL_v1086z00_1158, 0L, BINT(1L));
								VECTOR_SET(BgL_v1086z00_1158, 1L, BgL_varz00_15);
								VECTOR_SET(BgL_v1086z00_1158, 2L, BgL_addrz00_16);
								VECTOR_SET(BgL_v1086z00_1158, 3L, BFALSE);
								VECTOR_SET(BgL_v1086z00_1158, 4L, BFALSE);
								BgL_arg1194z00_1157 = BgL_v1086z00_1158;
							}
							return
								BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_varz00_15,
								BGl_symbol1642z00zz__evenvz00, BgL_arg1194z00_1157);
						}
				}
			}
		}

	}



/* &define-primop-ref! */
	obj_t BGl_z62definezd2primopzd2refz12z70zz__evenvz00(obj_t BgL_envz00_1852,
		obj_t BgL_varz00_1853, obj_t BgL_addrz00_1854)
	{
		{	/* Eval/evenv.scm 154 */
			{	/* Eval/evenv.scm 155 */
				obj_t BgL_auxz00_2039;

				if (SYMBOLP(BgL_varz00_1853))
					{	/* Eval/evenv.scm 155 */
						BgL_auxz00_2039 = BgL_varz00_1853;
					}
				else
					{
						obj_t BgL_auxz00_2042;

						BgL_auxz00_2042 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(6921L), BGl_string1651z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_varz00_1853);
						FAILURE(BgL_auxz00_2042, BFALSE, BFALSE);
					}
				return
					BGl_definezd2primopzd2refz12z12zz__evenvz00(BgL_auxz00_2039,
					BgL_addrz00_1854);
			}
		}

	}



/* define-primop-ref/loc! */
	BGL_EXPORTED_DEF obj_t BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(obj_t
		BgL_varz00_17, obj_t BgL_addrz00_18, obj_t BgL_fnamez00_19,
		obj_t BgL_locationz00_20)
	{
		{	/* Eval/evenv.scm 171 */
			{	/* Eval/evenv.scm 172 */
				obj_t BgL_cellz00_1159;

				{	/* Eval/evenv.scm 195 */
					obj_t BgL_propz00_1632;

					BgL_propz00_1632 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_17,
						BGl_symbol1647z00zz__evenvz00);
					if (CBOOL(BgL_propz00_1632))
						{	/* Eval/evenv.scm 196 */
							BgL_cellz00_1159 = BgL_propz00_1632;
						}
					else
						{	/* Eval/evenv.scm 198 */
							obj_t BgL_propz00_1633;

							BgL_propz00_1633 =
								BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_17,
								BGl_symbol1642z00zz__evenvz00);
							if (CBOOL(BgL_propz00_1633))
								{	/* Eval/evenv.scm 199 */
									BgL_cellz00_1159 = BgL_propz00_1633;
								}
							else
								{	/* Eval/evenv.scm 199 */
									BgL_cellz00_1159 = BFALSE;
								}
						}
				}
				{	/* Eval/evenv.scm 173 */
					bool_t BgL_test1719z00_2053;

					if (VECTORP(BgL_cellz00_1159))
						{	/* Eval/evenv.scm 108 */
							BgL_test1719z00_2053 = (VECTOR_LENGTH(BgL_cellz00_1159) == 5L);
						}
					else
						{	/* Eval/evenv.scm 108 */
							BgL_test1719z00_2053 = ((bool_t) 0);
						}
					if (BgL_test1719z00_2053)
						{	/* Eval/evenv.scm 173 */
							VECTOR_SET(BgL_cellz00_1159, 2L, BgL_addrz00_18);
							{	/* Eval/evenv.scm 179 */
								BgL_z62evalzd2warningzb0_bglt BgL_arg1196z00_1161;

								{	/* Eval/evenv.scm 179 */
									BgL_z62evalzd2warningzb0_bglt BgL_new1042z00_1162;

									{	/* Eval/evenv.scm 180 */
										BgL_z62evalzd2warningzb0_bglt BgL_new1041z00_1165;

										BgL_new1041z00_1165 =
											((BgL_z62evalzd2warningzb0_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62evalzd2warningzb0_bgl))));
										{	/* Eval/evenv.scm 180 */
											long BgL_arg1199z00_1166;

											BgL_arg1199z00_1166 =
												BGL_CLASS_NUM(BGl_z62evalzd2warningzb0zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1041z00_1165),
												BgL_arg1199z00_1166);
										}
										BgL_new1042z00_1162 = BgL_new1041z00_1165;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1042z00_1162)))->
											BgL_fnamez00) = ((obj_t) BgL_fnamez00_19), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1042z00_1162)))->BgL_locationz00) =
										((obj_t) BgL_locationz00_20), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1042z00_1162)))->BgL_stackz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_2069;

										{	/* Eval/evenv.scm 183 */
											obj_t BgL_list1197z00_1163;

											{	/* Eval/evenv.scm 183 */
												obj_t BgL_arg1198z00_1164;

												BgL_arg1198z00_1164 =
													MAKE_YOUNG_PAIR(BgL_varz00_17, BNIL);
												BgL_list1197z00_1163 =
													MAKE_YOUNG_PAIR(BGl_string1650z00zz__evenvz00,
													BgL_arg1198z00_1164);
											}
											BgL_auxz00_2069 = BgL_list1197z00_1163;
										}
										((((BgL_z62warningz62_bglt) COBJECT(
														((BgL_z62warningz62_bglt) BgL_new1042z00_1162)))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_2069), BUNSPEC);
									}
									BgL_arg1196z00_1161 = BgL_new1042z00_1162;
								}
								return
									BGl_warningzd2notifyzd2zz__errorz00(
									((obj_t) BgL_arg1196z00_1161));
							}
						}
					else
						{	/* Eval/evenv.scm 174 */
							obj_t BgL_arg1200z00_1167;

							{	/* Eval/evenv.scm 174 */
								obj_t BgL_v1087z00_1168;

								BgL_v1087z00_1168 = create_vector(5L);
								VECTOR_SET(BgL_v1087z00_1168, 0L, BINT(1L));
								VECTOR_SET(BgL_v1087z00_1168, 1L, BgL_varz00_17);
								VECTOR_SET(BgL_v1087z00_1168, 2L, BgL_addrz00_18);
								VECTOR_SET(BgL_v1087z00_1168, 3L, BFALSE);
								VECTOR_SET(BgL_v1087z00_1168, 4L, BFALSE);
								BgL_arg1200z00_1167 = BgL_v1087z00_1168;
							}
							return
								BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_varz00_17,
								BGl_symbol1642z00zz__evenvz00, BgL_arg1200z00_1167);
						}
				}
			}
		}

	}



/* &define-primop-ref/loc! */
	obj_t BGl_z62definezd2primopzd2refzf2locz12z82zz__evenvz00(obj_t
		BgL_envz00_1855, obj_t BgL_varz00_1856, obj_t BgL_addrz00_1857,
		obj_t BgL_fnamez00_1858, obj_t BgL_locationz00_1859)
	{
		{	/* Eval/evenv.scm 171 */
			{	/* Eval/evenv.scm 172 */
				obj_t BgL_auxz00_2084;

				if (SYMBOLP(BgL_varz00_1856))
					{	/* Eval/evenv.scm 172 */
						BgL_auxz00_2084 = BgL_varz00_1856;
					}
				else
					{
						obj_t BgL_auxz00_2087;

						BgL_auxz00_2087 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(7560L), BGl_string1652z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_varz00_1856);
						FAILURE(BgL_auxz00_2087, BFALSE, BFALSE);
					}
				return
					BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(BgL_auxz00_2084,
					BgL_addrz00_1857, BgL_fnamez00_1858, BgL_locationz00_1859);
			}
		}

	}



/* eval-lookup */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2lookupzd2zz__evenvz00(obj_t BgL_varz00_23)
	{
		{	/* Eval/evenv.scm 194 */
			{	/* Eval/evenv.scm 195 */
				obj_t BgL_propz00_1651;

				BgL_propz00_1651 =
					BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_23,
					BGl_symbol1647z00zz__evenvz00);
				if (CBOOL(BgL_propz00_1651))
					{	/* Eval/evenv.scm 196 */
						return BgL_propz00_1651;
					}
				else
					{	/* Eval/evenv.scm 198 */
						obj_t BgL_propz00_1652;

						BgL_propz00_1652 =
							BGl_getpropz00zz__r4_symbols_6_4z00(BgL_varz00_23,
							BGl_symbol1642z00zz__evenvz00);
						if (CBOOL(BgL_propz00_1652))
							{	/* Eval/evenv.scm 199 */
								return BgL_propz00_1652;
							}
						else
							{	/* Eval/evenv.scm 199 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &eval-lookup */
	obj_t BGl_z62evalzd2lookupzb0zz__evenvz00(obj_t BgL_envz00_1860,
		obj_t BgL_varz00_1861)
	{
		{	/* Eval/evenv.scm 194 */
			{	/* Eval/evenv.scm 195 */
				obj_t BgL_auxz00_2098;

				if (SYMBOLP(BgL_varz00_1861))
					{	/* Eval/evenv.scm 195 */
						BgL_auxz00_2098 = BgL_varz00_1861;
					}
				else
					{
						obj_t BgL_auxz00_2101;

						BgL_auxz00_2101 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(8506L), BGl_string1653z00zz__evenvz00,
							BGl_string1641z00zz__evenvz00, BgL_varz00_1861);
						FAILURE(BgL_auxz00_2101, BFALSE, BFALSE);
					}
				return BGl_evalzd2lookupzd2zz__evenvz00(BgL_auxz00_2098);
			}
		}

	}



/* eval-global-tag */
	BGL_EXPORTED_DEF int BGl_evalzd2globalzd2tagz00zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_24)
	{
		{	/* Eval/evenv.scm 206 */
			return CINT(VECTOR_REF(BgL_evalzd2globalzd2_24, 0L));
		}

	}



/* &eval-global-tag */
	obj_t BGl_z62evalzd2globalzd2tagz62zz__evenvz00(obj_t BgL_envz00_1862,
		obj_t BgL_evalzd2globalzd2_1863)
	{
		{	/* Eval/evenv.scm 206 */
			{	/* Eval/evenv.scm 207 */
				int BgL_tmpz00_2108;

				{	/* Eval/evenv.scm 207 */
					obj_t BgL_auxz00_2109;

					if (VECTORP(BgL_evalzd2globalzd2_1863))
						{	/* Eval/evenv.scm 207 */
							BgL_auxz00_2109 = BgL_evalzd2globalzd2_1863;
						}
					else
						{
							obj_t BgL_auxz00_2112;

							BgL_auxz00_2112 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
								BINT(8914L), BGl_string1654z00zz__evenvz00,
								BGl_string1645z00zz__evenvz00, BgL_evalzd2globalzd2_1863);
							FAILURE(BgL_auxz00_2112, BFALSE, BFALSE);
						}
					BgL_tmpz00_2108 =
						BGl_evalzd2globalzd2tagz00zz__evenvz00(BgL_auxz00_2109);
				}
				return BINT(BgL_tmpz00_2108);
			}
		}

	}



/* eval-global-tag-set! */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2globalzd2tagzd2setz12zc0zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_25, int BgL_tagz00_26)
	{
		{	/* Eval/evenv.scm 212 */
			return VECTOR_SET(BgL_evalzd2globalzd2_25, 0L, BINT(BgL_tagz00_26));
		}

	}



/* &eval-global-tag-set! */
	obj_t BGl_z62evalzd2globalzd2tagzd2setz12za2zz__evenvz00(obj_t
		BgL_envz00_1864, obj_t BgL_evalzd2globalzd2_1865, obj_t BgL_tagz00_1866)
	{
		{	/* Eval/evenv.scm 212 */
			{	/* Eval/evenv.scm 213 */
				int BgL_auxz00_2127;
				obj_t BgL_auxz00_2120;

				{	/* Eval/evenv.scm 213 */
					obj_t BgL_tmpz00_2128;

					if (INTEGERP(BgL_tagz00_1866))
						{	/* Eval/evenv.scm 213 */
							BgL_tmpz00_2128 = BgL_tagz00_1866;
						}
					else
						{
							obj_t BgL_auxz00_2131;

							BgL_auxz00_2131 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
								BINT(9225L), BGl_string1655z00zz__evenvz00,
								BGl_string1656z00zz__evenvz00, BgL_tagz00_1866);
							FAILURE(BgL_auxz00_2131, BFALSE, BFALSE);
						}
					BgL_auxz00_2127 = CINT(BgL_tmpz00_2128);
				}
				if (VECTORP(BgL_evalzd2globalzd2_1865))
					{	/* Eval/evenv.scm 213 */
						BgL_auxz00_2120 = BgL_evalzd2globalzd2_1865;
					}
				else
					{
						obj_t BgL_auxz00_2123;

						BgL_auxz00_2123 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(9225L), BGl_string1655z00zz__evenvz00,
							BGl_string1645z00zz__evenvz00, BgL_evalzd2globalzd2_1865);
						FAILURE(BgL_auxz00_2123, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2globalzd2tagzd2setz12zc0zz__evenvz00(BgL_auxz00_2120,
					BgL_auxz00_2127);
			}
		}

	}



/* eval-global-name */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2globalzd2namez00zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_27)
	{
		{	/* Eval/evenv.scm 218 */
			return VECTOR_REF(BgL_evalzd2globalzd2_27, 1L);
		}

	}



/* &eval-global-name */
	obj_t BGl_z62evalzd2globalzd2namez62zz__evenvz00(obj_t BgL_envz00_1867,
		obj_t BgL_evalzd2globalzd2_1868)
	{
		{	/* Eval/evenv.scm 218 */
			{	/* Eval/evenv.scm 219 */
				obj_t BgL_auxz00_2138;

				if (VECTORP(BgL_evalzd2globalzd2_1868))
					{	/* Eval/evenv.scm 219 */
						BgL_auxz00_2138 = BgL_evalzd2globalzd2_1868;
					}
				else
					{
						obj_t BgL_auxz00_2141;

						BgL_auxz00_2141 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(9533L), BGl_string1657z00zz__evenvz00,
							BGl_string1645z00zz__evenvz00, BgL_evalzd2globalzd2_1868);
						FAILURE(BgL_auxz00_2141, BFALSE, BFALSE);
					}
				return BGl_evalzd2globalzd2namez00zz__evenvz00(BgL_auxz00_2138);
			}
		}

	}



/* eval-global-value */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2globalzd2valuez00zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_28)
	{
		{	/* Eval/evenv.scm 224 */
			return VECTOR_REF(BgL_evalzd2globalzd2_28, 2L);
		}

	}



/* &eval-global-value */
	obj_t BGl_z62evalzd2globalzd2valuez62zz__evenvz00(obj_t BgL_envz00_1869,
		obj_t BgL_evalzd2globalzd2_1870)
	{
		{	/* Eval/evenv.scm 224 */
			{	/* Eval/evenv.scm 225 */
				obj_t BgL_auxz00_2147;

				if (VECTORP(BgL_evalzd2globalzd2_1870))
					{	/* Eval/evenv.scm 225 */
						BgL_auxz00_2147 = BgL_evalzd2globalzd2_1870;
					}
				else
					{
						obj_t BgL_auxz00_2150;

						BgL_auxz00_2150 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(9837L), BGl_string1658z00zz__evenvz00,
							BGl_string1645z00zz__evenvz00, BgL_evalzd2globalzd2_1870);
						FAILURE(BgL_auxz00_2150, BFALSE, BFALSE);
					}
				return BGl_evalzd2globalzd2valuez00zz__evenvz00(BgL_auxz00_2147);
			}
		}

	}



/* set-eval-global-value! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2evalzd2globalzd2valuez12zc0zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_29, obj_t BgL_valuez00_30)
	{
		{	/* Eval/evenv.scm 230 */
			return VECTOR_SET(BgL_evalzd2globalzd2_29, 2L, BgL_valuez00_30);
		}

	}



/* &set-eval-global-value! */
	obj_t BGl_z62setzd2evalzd2globalzd2valuez12za2zz__evenvz00(obj_t
		BgL_envz00_1871, obj_t BgL_evalzd2globalzd2_1872, obj_t BgL_valuez00_1873)
	{
		{	/* Eval/evenv.scm 230 */
			{	/* Eval/evenv.scm 231 */
				obj_t BgL_auxz00_2156;

				if (VECTORP(BgL_evalzd2globalzd2_1872))
					{	/* Eval/evenv.scm 231 */
						BgL_auxz00_2156 = BgL_evalzd2globalzd2_1872;
					}
				else
					{
						obj_t BgL_auxz00_2159;

						BgL_auxz00_2159 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(10152L), BGl_string1659z00zz__evenvz00,
							BGl_string1645z00zz__evenvz00, BgL_evalzd2globalzd2_1872);
						FAILURE(BgL_auxz00_2159, BFALSE, BFALSE);
					}
				return
					BGl_setzd2evalzd2globalzd2valuez12zc0zz__evenvz00(BgL_auxz00_2156,
					BgL_valuez00_1873);
			}
		}

	}



/* eval-global-module */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2globalzd2modulez00zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_31)
	{
		{	/* Eval/evenv.scm 236 */
			return VECTOR_REF(((obj_t) BgL_evalzd2globalzd2_31), 3L);
		}

	}



/* &eval-global-module */
	obj_t BGl_z62evalzd2globalzd2modulez62zz__evenvz00(obj_t BgL_envz00_1874,
		obj_t BgL_evalzd2globalzd2_1875)
	{
		{	/* Eval/evenv.scm 236 */
			return
				BGl_evalzd2globalzd2modulez00zz__evenvz00(BgL_evalzd2globalzd2_1875);
		}

	}



/* eval-global-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_evalzd2globalzd2modulezd2setz12zc0zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_32, obj_t BgL_mz00_33)
	{
		{	/* Eval/evenv.scm 242 */
			return VECTOR_SET(BgL_evalzd2globalzd2_32, 3L, BgL_mz00_33);
		}

	}



/* &eval-global-module-set! */
	obj_t BGl_z62evalzd2globalzd2modulezd2setz12za2zz__evenvz00(obj_t
		BgL_envz00_1876, obj_t BgL_evalzd2globalzd2_1877, obj_t BgL_mz00_1878)
	{
		{	/* Eval/evenv.scm 242 */
			{	/* Eval/evenv.scm 243 */
				obj_t BgL_auxz00_2168;

				if (VECTORP(BgL_evalzd2globalzd2_1877))
					{	/* Eval/evenv.scm 243 */
						BgL_auxz00_2168 = BgL_evalzd2globalzd2_1877;
					}
				else
					{
						obj_t BgL_auxz00_2171;

						BgL_auxz00_2171 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1639z00zz__evenvz00,
							BINT(10776L), BGl_string1660z00zz__evenvz00,
							BGl_string1645z00zz__evenvz00, BgL_evalzd2globalzd2_1877);
						FAILURE(BgL_auxz00_2171, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2globalzd2modulezd2setz12zc0zz__evenvz00(BgL_auxz00_2168,
					BgL_mz00_1878);
			}
		}

	}



/* eval-global-loc */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2globalzd2locz00zz__evenvz00(obj_t
		BgL_evalzd2globalzd2_34)
	{
		{	/* Eval/evenv.scm 248 */
			return VECTOR_REF(((obj_t) BgL_evalzd2globalzd2_34), 4L);
		}

	}



/* &eval-global-loc */
	obj_t BGl_z62evalzd2globalzd2locz62zz__evenvz00(obj_t BgL_envz00_1879,
		obj_t BgL_evalzd2globalzd2_1880)
	{
		{	/* Eval/evenv.scm 248 */
			return BGl_evalzd2globalzd2locz00zz__evenvz00(BgL_evalzd2globalzd2_1880);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evenvz00(void)
	{
		{	/* Eval/evenv.scm 14 */
			return
				BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1661z00zz__evenvz00));
		}

	}

#ifdef __cplusplus
}
#endif
