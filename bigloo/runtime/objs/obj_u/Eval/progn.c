/*===========================================================================*/
/*   (Eval/progn.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/progn.scm -indent -o objs/obj_u/Eval/progn.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___PROGN_TYPE_DEFINITIONS
#define BGL___PROGN_TYPE_DEFINITIONS
#endif													// BGL___PROGN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_makezd2epairzd2zz__prognz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__prognz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62evepairifyz62zz__prognz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evepairifyza2zc0zz__prognz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__prognz00(void);
	static obj_t BGl_genericzd2initzd2zz__prognz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__prognz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__prognz00(void);
	static obj_t BGl_objectzd2initzd2zz__prognz00(void);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_appendzd221011zd2zz__prognz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__prognz00(void);
	static obj_t BGl_symbol1708z00zz__prognz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__prognz00(obj_t, obj_t);
	static obj_t BGl_loopze71ze7zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evepairifyza2za2zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evepairifyzd2deepzd2zz__prognz00(obj_t, obj_t);
	static obj_t BGl_z62evepairifyzd2deepzb0zz__prognz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62expandzd2prognzb0zz__prognz00(obj_t, obj_t);
	static obj_t BGl_flattenzd2sequenceze70z35zz__prognz00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evepairifyzd2envzd2zz__prognz00,
		BgL_bgl_za762evepairifyza7621715z00, BGl_z62evepairifyz62zz__prognz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evepairifyza2zd2envz70zz__prognz00,
		BgL_bgl_za762evepairifyza7a21716z00, BGl_z62evepairifyza2zc0zz__prognz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1709z00zz__prognz00,
		BgL_bgl_string1709za700za7za7_1717za7, "begin", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evepairifyzd2deepzd2envz00zz__prognz00,
		BgL_bgl_za762evepairifyza7d21718z00,
		BGl_z62evepairifyzd2deepzb0zz__prognz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1710z00zz__prognz00,
		BgL_bgl_string1710za700za7za7_1719za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1711z00zz__prognz00,
		BgL_bgl_string1711za700za7za7_1720za7, "/tmp/bigloo/runtime/Eval/progn.scm",
		34);
	      DEFINE_STRING(BGl_string1712z00zz__prognz00,
		BgL_bgl_string1712za700za7za7_1721za7, "&expand-progn", 13);
	      DEFINE_STRING(BGl_string1713z00zz__prognz00,
		BgL_bgl_string1713za700za7za7_1722za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1714z00zz__prognz00,
		BgL_bgl_string1714za700za7za7_1723za7, "__progn", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2prognzd2envz00zz__prognz00,
		BgL_bgl_za762expandza7d2prog1724z00, BGl_z62expandzd2prognzb0zz__prognz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__prognz00));
		     ADD_ROOT((void *) (&BGl_symbol1708z00zz__prognz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long
		BgL_checksumz00_1932, char *BgL_fromz00_1933)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__prognz00))
				{
					BGl_requirezd2initializa7ationz75zz__prognz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__prognz00();
					BGl_cnstzd2initzd2zz__prognz00();
					BGl_importedzd2moduleszd2initz00zz__prognz00();
					return BGl_methodzd2initzd2zz__prognz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__prognz00(void)
	{
		{	/* Eval/progn.scm 14 */
			return (BGl_symbol1708z00zz__prognz00 =
				bstring_to_symbol(BGl_string1709z00zz__prognz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__prognz00(void)
	{
		{	/* Eval/progn.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__prognz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1119;

				BgL_headz00_1119 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1668;
					obj_t BgL_tailz00_1669;

					BgL_prevz00_1668 = BgL_headz00_1119;
					BgL_tailz00_1669 = BgL_l1z00_1;
				BgL_loopz00_1667:
					if (PAIRP(BgL_tailz00_1669))
						{
							obj_t BgL_newzd2prevzd2_1675;

							BgL_newzd2prevzd2_1675 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1669), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1668, BgL_newzd2prevzd2_1675);
							{
								obj_t BgL_tailz00_1950;
								obj_t BgL_prevz00_1949;

								BgL_prevz00_1949 = BgL_newzd2prevzd2_1675;
								BgL_tailz00_1950 = CDR(BgL_tailz00_1669);
								BgL_tailz00_1669 = BgL_tailz00_1950;
								BgL_prevz00_1668 = BgL_prevz00_1949;
								goto BgL_loopz00_1667;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1119);
			}
		}

	}



/* make-epair */
	obj_t BGl_makezd2epairzd2zz__prognz00(obj_t BgL_az00_3, obj_t BgL_bz00_4,
		obj_t BgL_ez00_5)
	{
		{	/* Eval/progn.scm 55 */
			if (EPAIRP(BgL_ez00_5))
				{	/* Eval/progn.scm 58 */
					obj_t BgL_arg1166z00_1128;

					BgL_arg1166z00_1128 = CER(((obj_t) BgL_ez00_5));
					{	/* Eval/progn.scm 58 */
						obj_t BgL_res1700z00_1683;

						BgL_res1700z00_1683 =
							MAKE_YOUNG_EPAIR(BgL_az00_3, BgL_bz00_4, BgL_arg1166z00_1128);
						return BgL_res1700z00_1683;
					}
				}
			else
				{	/* Eval/progn.scm 57 */
					if (EPAIRP(BgL_bz00_4))
						{	/* Eval/progn.scm 60 */
							obj_t BgL_arg1171z00_1130;

							BgL_arg1171z00_1130 = CER(((obj_t) BgL_bz00_4));
							{	/* Eval/progn.scm 60 */
								obj_t BgL_res1701z00_1685;

								BgL_res1701z00_1685 =
									MAKE_YOUNG_EPAIR(BgL_az00_3, BgL_bz00_4, BgL_arg1171z00_1130);
								return BgL_res1701z00_1685;
							}
						}
					else
						{	/* Eval/progn.scm 59 */
							if (EPAIRP(BgL_az00_3))
								{	/* Eval/progn.scm 62 */
									obj_t BgL_arg1182z00_1132;

									BgL_arg1182z00_1132 = CER(((obj_t) BgL_az00_3));
									{	/* Eval/progn.scm 62 */
										obj_t BgL_res1702z00_1687;

										BgL_res1702z00_1687 =
											MAKE_YOUNG_EPAIR(BgL_az00_3, BgL_bz00_4,
											BgL_arg1182z00_1132);
										return BgL_res1702z00_1687;
									}
								}
							else
								{	/* Eval/progn.scm 61 */
									return MAKE_YOUNG_PAIR(BgL_az00_3, BgL_bz00_4);
								}
						}
				}
		}

	}



/* evepairify */
	BGL_EXPORTED_DEF obj_t BGl_evepairifyz00zz__prognz00(obj_t BgL_pairz00_6,
		obj_t BgL_epairz00_7)
	{
		{	/* Eval/progn.scm 74 */
			if (EPAIRP(BgL_epairz00_7))
				{	/* Eval/progn.scm 76 */
					if (EPAIRP(BgL_pairz00_6))
						{	/* Eval/progn.scm 78 */
							return BgL_pairz00_6;
						}
					else
						{	/* Eval/progn.scm 78 */
							if (PAIRP(BgL_pairz00_6))
								{	/* Eval/progn.scm 83 */
									obj_t BgL_arg1187z00_1136;
									obj_t BgL_arg1188z00_1137;
									obj_t BgL_arg1189z00_1138;

									BgL_arg1187z00_1136 = CAR(BgL_pairz00_6);
									BgL_arg1188z00_1137 = CDR(BgL_pairz00_6);
									BgL_arg1189z00_1138 = CER(((obj_t) BgL_epairz00_7));
									{	/* Eval/progn.scm 83 */
										obj_t BgL_res1703z00_1691;

										BgL_res1703z00_1691 =
											MAKE_YOUNG_EPAIR(BgL_arg1187z00_1136, BgL_arg1188z00_1137,
											BgL_arg1189z00_1138);
										return BgL_res1703z00_1691;
									}
								}
							else
								{	/* Eval/progn.scm 80 */
									return BgL_pairz00_6;
								}
						}
				}
			else
				{	/* Eval/progn.scm 76 */
					return BgL_pairz00_6;
				}
		}

	}



/* &evepairify */
	obj_t BGl_z62evepairifyz62zz__prognz00(obj_t BgL_envz00_1916,
		obj_t BgL_pairz00_1917, obj_t BgL_epairz00_1918)
	{
		{	/* Eval/progn.scm 74 */
			return BGl_evepairifyz00zz__prognz00(BgL_pairz00_1917, BgL_epairz00_1918);
		}

	}



/* evepairify* */
	BGL_EXPORTED_DEF obj_t BGl_evepairifyza2za2zz__prognz00(obj_t BgL_pairz00_8,
		obj_t BgL_epairz00_9)
	{
		{	/* Eval/progn.scm 93 */
			if (EPAIRP(BgL_epairz00_9))
				{	/* Eval/progn.scm 96 */
					obj_t BgL_ez00_1140;

					BgL_ez00_1140 = CER(((obj_t) BgL_epairz00_9));
					BGL_TAIL return
						BGl_loopze71ze7zz__prognz00(BgL_ez00_1140, BgL_pairz00_8);
				}
			else
				{	/* Eval/progn.scm 94 */
					return BgL_pairz00_8;
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__prognz00(obj_t BgL_ez00_1928, obj_t BgL_objz00_1142)
	{
		{	/* Eval/progn.scm 97 */
			if (PAIRP(BgL_objz00_1142))
				{	/* Eval/progn.scm 99 */
					if (EPAIRP(BgL_objz00_1142))
						{	/* Eval/progn.scm 101 */
							return BgL_objz00_1142;
						}
					else
						{	/* Eval/progn.scm 104 */
							obj_t BgL_arg1194z00_1146;
							obj_t BgL_arg1196z00_1147;

							BgL_arg1194z00_1146 =
								BGl_loopze71ze7zz__prognz00(BgL_ez00_1928,
								CAR(BgL_objz00_1142));
							BgL_arg1196z00_1147 =
								BGl_loopze71ze7zz__prognz00(BgL_ez00_1928,
								CDR(BgL_objz00_1142));
							{	/* Eval/progn.scm 104 */
								obj_t BgL_res1704z00_1695;

								BgL_res1704z00_1695 =
									MAKE_YOUNG_EPAIR(BgL_arg1194z00_1146, BgL_arg1196z00_1147,
									BgL_ez00_1928);
								return BgL_res1704z00_1695;
							}
						}
				}
			else
				{	/* Eval/progn.scm 99 */
					return BgL_objz00_1142;
				}
		}

	}



/* &evepairify* */
	obj_t BGl_z62evepairifyza2zc0zz__prognz00(obj_t BgL_envz00_1919,
		obj_t BgL_pairz00_1920, obj_t BgL_epairz00_1921)
	{
		{	/* Eval/progn.scm 93 */
			return
				BGl_evepairifyza2za2zz__prognz00(BgL_pairz00_1920, BgL_epairz00_1921);
		}

	}



/* evepairify-deep */
	BGL_EXPORTED_DEF obj_t BGl_evepairifyzd2deepzd2zz__prognz00(obj_t
		BgL_pairz00_10, obj_t BgL_epairz00_11)
	{
		{	/* Eval/progn.scm 109 */
			if (EPAIRP(BgL_epairz00_11))
				{	/* Eval/progn.scm 111 */
					if (PAIRP(BgL_pairz00_10))
						{	/* Eval/progn.scm 113 */
							if (EPAIRP(BgL_pairz00_10))
								{	/* Eval/progn.scm 115 */
									return BgL_pairz00_10;
								}
							else
								{	/* Eval/progn.scm 118 */
									obj_t BgL_arg1202z00_1154;
									obj_t BgL_arg1203z00_1155;
									obj_t BgL_arg1206z00_1156;

									{	/* Eval/progn.scm 118 */
										obj_t BgL_arg1208z00_1157;
										obj_t BgL_arg1209z00_1158;

										BgL_arg1208z00_1157 = CAR(BgL_pairz00_10);
										BgL_arg1209z00_1158 = CAR(((obj_t) BgL_epairz00_11));
										BgL_arg1202z00_1154 =
											BGl_evepairifyzd2deepzd2zz__prognz00(BgL_arg1208z00_1157,
											BgL_arg1209z00_1158);
									}
									{	/* Eval/progn.scm 119 */
										obj_t BgL_arg1210z00_1159;
										obj_t BgL_arg1212z00_1160;

										BgL_arg1210z00_1159 = CDR(BgL_pairz00_10);
										BgL_arg1212z00_1160 = CDR(((obj_t) BgL_epairz00_11));
										BgL_arg1203z00_1155 =
											BGl_evepairifyzd2deepzd2zz__prognz00(BgL_arg1210z00_1159,
											BgL_arg1212z00_1160);
									}
									BgL_arg1206z00_1156 = CER(((obj_t) BgL_epairz00_11));
									{	/* Eval/progn.scm 118 */
										obj_t BgL_res1705z00_1701;

										BgL_res1705z00_1701 =
											MAKE_YOUNG_EPAIR(BgL_arg1202z00_1154, BgL_arg1203z00_1155,
											BgL_arg1206z00_1156);
										return BgL_res1705z00_1701;
									}
								}
						}
					else
						{	/* Eval/progn.scm 113 */
							return BgL_pairz00_10;
						}
				}
			else
				{	/* Eval/progn.scm 111 */
					return BgL_pairz00_10;
				}
		}

	}



/* &evepairify-deep */
	obj_t BGl_z62evepairifyzd2deepzb0zz__prognz00(obj_t BgL_envz00_1922,
		obj_t BgL_pairz00_1923, obj_t BgL_epairz00_1924)
	{
		{	/* Eval/progn.scm 109 */
			return
				BGl_evepairifyzd2deepzd2zz__prognz00(BgL_pairz00_1923,
				BgL_epairz00_1924);
		}

	}



/* expand-progn */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t BgL_expsz00_12)
	{
		{	/* Eval/progn.scm 128 */
			if (NULLP(BgL_expsz00_12))
				{	/* Eval/progn.scm 142 */
					return BUNSPEC;
				}
			else
				{	/* Eval/progn.scm 142 */
					if (NULLP(CDR(BgL_expsz00_12)))
						{	/* Eval/progn.scm 144 */
							return CAR(BgL_expsz00_12);
						}
					else
						{	/* Eval/progn.scm 146 */
							obj_t BgL_esz00_1165;

							BgL_esz00_1165 =
								BGl_flattenzd2sequenceze70z35zz__prognz00(BgL_expsz00_12);
							if (NULLP(BgL_esz00_1165))
								{	/* Eval/progn.scm 148 */
									return BUNSPEC;
								}
							else
								{	/* Eval/progn.scm 148 */
									if (PAIRP(BgL_esz00_1165))
										{	/* Eval/progn.scm 150 */
											if (NULLP(CDR(BgL_esz00_1165)))
												{	/* Eval/progn.scm 152 */
													return CAR(BgL_esz00_1165);
												}
											else
												{	/* Eval/progn.scm 155 */
													obj_t BgL_az00_1721;

													BgL_az00_1721 = BGl_symbol1708z00zz__prognz00;
													if (EPAIRP(BgL_esz00_1165))
														{	/* Eval/progn.scm 58 */
															obj_t BgL_arg1166z00_1723;

															BgL_arg1166z00_1723 = CER(BgL_esz00_1165);
															{	/* Eval/progn.scm 58 */
																obj_t BgL_res1700z00_1725;

																BgL_res1700z00_1725 =
																	MAKE_YOUNG_EPAIR(BgL_az00_1721,
																	BgL_esz00_1165, BgL_arg1166z00_1723);
																return BgL_res1700z00_1725;
															}
														}
													else
														{	/* Eval/progn.scm 57 */
															if (EPAIRP(BgL_esz00_1165))
																{	/* Eval/progn.scm 60 */
																	obj_t BgL_arg1171z00_1727;

																	BgL_arg1171z00_1727 = CER(BgL_esz00_1165);
																	{	/* Eval/progn.scm 60 */
																		obj_t BgL_res1701z00_1729;

																		BgL_res1701z00_1729 =
																			MAKE_YOUNG_EPAIR(BgL_az00_1721,
																			BgL_esz00_1165, BgL_arg1171z00_1727);
																		return BgL_res1701z00_1729;
																	}
																}
															else
																{	/* Eval/progn.scm 59 */
																	if (EPAIRP(BgL_az00_1721))
																		{	/* Eval/progn.scm 62 */
																			obj_t BgL_arg1182z00_1731;

																			BgL_arg1182z00_1731 =
																				CER(((obj_t) BgL_az00_1721));
																			{	/* Eval/progn.scm 62 */
																				obj_t BgL_res1702z00_1733;

																				BgL_res1702z00_1733 =
																					MAKE_YOUNG_EPAIR(BgL_az00_1721,
																					BgL_esz00_1165, BgL_arg1182z00_1731);
																				return BgL_res1702z00_1733;
																			}
																		}
																	else
																		{	/* Eval/progn.scm 61 */
																			return
																				MAKE_YOUNG_PAIR(BgL_az00_1721,
																				BgL_esz00_1165);
																		}
																}
														}
												}
										}
									else
										{	/* Eval/progn.scm 150 */
											return BgL_esz00_1165;
										}
								}
						}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__prognz00(obj_t BgL_expsz00_1927,
		obj_t BgL_esz00_1175)
	{
		{	/* Eval/progn.scm 130 */
		BGl_loopze70ze7zz__prognz00:
			{	/* Eval/progn.scm 132 */
				bool_t BgL_test1747z00_2043;

				if (NULLP(BgL_esz00_1175))
					{	/* Eval/progn.scm 132 */
						BgL_test1747z00_2043 = ((bool_t) 1);
					}
				else
					{	/* Eval/progn.scm 132 */
						BgL_test1747z00_2043 = NULLP(CDR(((obj_t) BgL_esz00_1175)));
					}
				if (BgL_test1747z00_2043)
					{	/* Eval/progn.scm 132 */
						return BgL_esz00_1175;
					}
				else
					{	/* Eval/progn.scm 132 */
						if (PAIRP(BgL_esz00_1175))
							{	/* Eval/progn.scm 136 */
								bool_t BgL_test1750z00_2051;

								{	/* Eval/progn.scm 136 */
									obj_t BgL_tmpz00_2052;

									BgL_tmpz00_2052 = CAR(BgL_esz00_1175);
									BgL_test1750z00_2051 = PAIRP(BgL_tmpz00_2052);
								}
								if (BgL_test1750z00_2051)
									{	/* Eval/progn.scm 136 */
										if (
											(CAR(CAR(BgL_esz00_1175)) ==
												BGl_symbol1708z00zz__prognz00))
											{
												obj_t BgL_esz00_2059;

												BgL_esz00_2059 =
													BGl_evepairifyz00zz__prognz00
													(BGl_appendzd221011zd2zz__prognz00(CDR(CAR
															(BgL_esz00_1175)),
														BGl_loopze70ze7zz__prognz00(BgL_expsz00_1927,
															CDR(BgL_esz00_1175))), CAR(BgL_esz00_1175));
												BgL_esz00_1175 = BgL_esz00_2059;
												goto BGl_loopze70ze7zz__prognz00;
											}
										else
											{	/* Eval/progn.scm 138 */
												return
													BGl_makezd2epairzd2zz__prognz00(CAR(BgL_esz00_1175),
													BGl_loopze70ze7zz__prognz00(BgL_expsz00_1927,
														CDR(BgL_esz00_1175)), BgL_esz00_1175);
											}
									}
								else
									{
										obj_t BgL_esz00_2071;

										BgL_esz00_2071 = CDR(BgL_esz00_1175);
										BgL_esz00_1175 = BgL_esz00_2071;
										goto BGl_loopze70ze7zz__prognz00;
									}
							}
						else
							{	/* Eval/progn.scm 134 */
								return
									BGl_errorz00zz__errorz00(BGl_string1709z00zz__prognz00,
									BGl_string1710z00zz__prognz00, BgL_expsz00_1927);
							}
					}
			}
		}

	}



/* flatten-sequence~0 */
	obj_t BGl_flattenzd2sequenceze70z35zz__prognz00(obj_t BgL_expsz00_1172)
	{
		{	/* Eval/progn.scm 141 */
			return BGl_loopze70ze7zz__prognz00(BgL_expsz00_1172, BgL_expsz00_1172);
		}

	}



/* &expand-progn */
	obj_t BGl_z62expandzd2prognzb0zz__prognz00(obj_t BgL_envz00_1925,
		obj_t BgL_expsz00_1926)
	{
		{	/* Eval/progn.scm 128 */
			{	/* Eval/progn.scm 132 */
				obj_t BgL_auxz00_2075;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_expsz00_1926))
					{	/* Eval/progn.scm 132 */
						BgL_auxz00_2075 = BgL_expsz00_1926;
					}
				else
					{
						obj_t BgL_auxz00_2078;

						BgL_auxz00_2078 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1711z00zz__prognz00,
							BINT(4826L), BGl_string1712z00zz__prognz00,
							BGl_string1713z00zz__prognz00, BgL_expsz00_1926);
						FAILURE(BgL_auxz00_2078, BFALSE, BFALSE);
					}
				return BGl_expandzd2prognzd2zz__prognz00(BgL_auxz00_2075);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__prognz00(void)
	{
		{	/* Eval/progn.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__prognz00(void)
	{
		{	/* Eval/progn.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__prognz00(void)
	{
		{	/* Eval/progn.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__prognz00(void)
	{
		{	/* Eval/progn.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
			return BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1714z00zz__prognz00));
		}

	}

#ifdef __cplusplus
}
#endif
