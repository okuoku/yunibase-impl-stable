/*===========================================================================*/
/*   (Eval/library.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/library.scm -indent -o objs/obj_u/Eval/library.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___LIBRARY_TYPE_DEFINITIONS
#define BGL___LIBRARY_TYPE_DEFINITIONS
#endif													// BGL___LIBRARY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1911z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1914z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1884z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1886z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1888z00zz__libraryz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2loadzd2zz__libraryz00(obj_t, obj_t);
	static obj_t BGl_symbol1924z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_za2loadedzd2librariesza2zd2zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1929z00zz__libraryz00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__libraryz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_declarezd2libraryz12zc0zz__libraryz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t, obj_t);
	static obj_t BGl_keyword1890z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1892z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1894z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1896z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_za2loadedzd2initzd2filesza2z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1898z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1934z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1936z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1938z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31444ze3ze5zz__libraryz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_evalzd2modulezd2zz__evmodulez00(void);
	static obj_t BGl_untranslatezd2libraryzd2namez00zz__libraryz00(obj_t);
	static obj_t BGl_symbol1861z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_z62libraryzd2loadzd2initz62zz__libraryz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol1949z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1868z00zz__libraryz00 = BUNSPEC;
	extern bool_t fexists(char *);
	extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t
		BGl_z62libraryzd2translationzd2tablezd2addz12za2zz__libraryz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1870z00zz__libraryz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2load_ezd2zz__libraryz00(obj_t, obj_t);
	static obj_t BGl_symbol1954z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1873z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_z62libraryzd2initzd2filez62zz__libraryz00(obj_t, obj_t);
	static obj_t BGl_symbol1956z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1876z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__libraryz00(void);
	extern obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_symbol1960z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_symbol1881z00zz__libraryz00 = BUNSPEC;
	extern obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_getenvz00zz__osz00(obj_t);
	extern obj_t create_struct(obj_t, int);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__libraryz00(void);
	static obj_t BGl_genericzd2initzd2zz__libraryz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__libraryz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__libraryz00(void);
	static obj_t BGl_za2librariesza2z00zz__libraryz00 = BUNSPEC;
	extern obj_t BGl_evwarningz00zz__everrorz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__libraryz00(void);
	extern obj_t bgl_register_eval_srfi(obj_t);
	extern obj_t bigloo_mangle(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_libraryzd2multithreadzd2setz12z12zz__libraryz00(bool_t);
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2loadzd2initz00zz__libraryz00(obj_t,
		obj_t);
	static obj_t BGl_za2loadedzd2initzd2librariesza2z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_za2libraryzd2mutexza2zd2zz__libraryz00 = BUNSPEC;
	static obj_t BGl_list1952z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_list1953z00zz__libraryz00 = BUNSPEC;
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_list1958z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_list1959z00zz__libraryz00 = BUNSPEC;
	extern obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_loadzd2initzd2zz__libraryz00(obj_t);
	extern obj_t BGl_interactionzd2environmentzd2zz__evalz00(void);
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2initzd2filez00zz__libraryz00(obj_t);
	extern obj_t BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(obj_t);
	static obj_t BGl_list1883z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31377ze3ze5zz__libraryz00(obj_t);
	static obj_t BGl_z62libraryzd2filezd2namez62zz__libraryz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_unixzd2pathzd2ze3listze3zz__osz00(obj_t);
	static obj_t BGl_z62libraryzd2markzd2loadedz12z70zz__libraryz00(obj_t, obj_t);
	static obj_t BGl__declarezd2libraryz12zc0zz__libraryz00(obj_t, obj_t);
	static obj_t BGl_z62libraryzd2infozb0zz__libraryz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_libraryzd2translationzd2tablezd2addz12zc0zz__libraryz00(obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_keyword1900z00zz__libraryz00 = BUNSPEC;
	static obj_t BGl_keyword1902z00zz__libraryz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2existszf3z21zz__libraryz00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__libraryz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_libraryzd2markzd2loadedz12z12zz__libraryz00(obj_t);
	static obj_t BGl_z62libraryzd2existszf3z43zz__libraryz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2infozd2zz__libraryz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_libraryzd2threadzd2suffixzd2setz12zc0zz__libraryz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_libraryzd2loadedzf3z21zz__libraryz00(obj_t);
	static obj_t BGl_z62libraryzd2loadedzf3z43zz__libraryz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2filezd2namez00zz__libraryz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_dynamiczd2loadzd2zz__osz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62libraryzd2multithreadzd2setz12z70zz__libraryz00(obj_t,
		obj_t);
	static obj_t BGl_z62libraryzd2threadzd2suffixzd2setz12za2zz__libraryz00(obj_t,
		obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62libraryzd2load_ezb0zz__libraryz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_bigloozd2libraryzd2pathz00zz__paramz00(void);
	static obj_t BGl_libraryzd2threadzd2suffixz00zz__libraryz00 = BUNSPEC;
	extern bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	static obj_t BGl_symbol1904z00zz__libraryz00 = BUNSPEC;
	extern obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t BGl_z62libraryzd2loadzb0zz__libraryz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_evalzd2libraryzd2suffixz00zz__libraryz00(void);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1950z00zz__libraryz00,
		BgL_bgl_string1950za700za7za7_1966za7, "library-load", 12);
	      DEFINE_STRING(BGl_string1951z00zz__libraryz00,
		BgL_bgl_string1951za700za7za7_1967za7,
		"Can't find _e library `~a' (`~a') in path ", 42);
	      DEFINE_STRING(BGl_string1871z00zz__libraryz00,
		BgL_bgl_string1871za700za7za7_1968za7, "unsafe", 6);
	      DEFINE_STRING(BGl_string1872z00zz__libraryz00,
		BgL_bgl_string1872za700za7za7_1969za7, "u", 1);
	      DEFINE_STRING(BGl_string1955z00zz__libraryz00,
		BgL_bgl_string1955za700za7za7_1970za7, "libinfo-init", 12);
	      DEFINE_STRING(BGl_string1874z00zz__libraryz00,
		BgL_bgl_string1874za700za7za7_1971za7, "safe", 4);
	      DEFINE_STRING(BGl_string1875z00zz__libraryz00,
		BgL_bgl_string1875za700za7za7_1972za7, "s", 1);
	      DEFINE_STRING(BGl_string1957z00zz__libraryz00,
		BgL_bgl_string1957za700za7za7_1973za7, "info", 4);
	      DEFINE_STRING(BGl_string1877z00zz__libraryz00,
		BgL_bgl_string1877za700za7za7_1974za7, "profile", 7);
	      DEFINE_STRING(BGl_string1878z00zz__libraryz00,
		BgL_bgl_string1878za700za7za7_1975za7, "p", 1);
	      DEFINE_STRING(BGl_string1879z00zz__libraryz00,
		BgL_bgl_string1879za700za7za7_1976za7, "_declare-library!", 17);
	      DEFINE_STRING(BGl_string1961z00zz__libraryz00,
		BgL_bgl_string1961za700za7za7_1977za7, "libinfo-eval", 12);
	      DEFINE_STRING(BGl_string1880z00zz__libraryz00,
		BgL_bgl_string1880za700za7za7_1978za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1962z00zz__libraryz00,
		BgL_bgl_string1962za700za7za7_1979za7, "string or symbol", 16);
	      DEFINE_STRING(BGl_string1963z00zz__libraryz00,
		BgL_bgl_string1963za700za7za7_1980za7, ".heap", 5);
	      DEFINE_STRING(BGl_string1882z00zz__libraryz00,
		BgL_bgl_string1882za700za7za7_1981za7, "release-number", 14);
	      DEFINE_STRING(BGl_string1964z00zz__libraryz00,
		BgL_bgl_string1964za700za7za7_1982za7, "&library-exists?", 16);
	      DEFINE_STRING(BGl_string1965z00zz__libraryz00,
		BgL_bgl_string1965za700za7za7_1983za7, "__library", 9);
	      DEFINE_STRING(BGl_string1885z00zz__libraryz00,
		BgL_bgl_string1885za700za7za7_1984za7, "basename", 8);
	      DEFINE_STRING(BGl_string1887z00zz__libraryz00,
		BgL_bgl_string1887za700za7za7_1985za7, "class-eval", 10);
	      DEFINE_STRING(BGl_string1889z00zz__libraryz00,
		BgL_bgl_string1889za700za7za7_1986za7, "class-init", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2multithreadzd2setz12zd2envzc0zz__libraryz00,
		BgL_bgl_za762libraryza7d2mul1987z00,
		BGl_z62libraryzd2multithreadzd2setz12z70zz__libraryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2loadzd2initzd2envzd2zz__libraryz00,
		BgL_bgl_za762libraryza7d2loa1988z00,
		BGl_z62libraryzd2loadzd2initz62zz__libraryz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_libraryzd2infozd2envz00zz__libraryz00,
		BgL_bgl_za762libraryza7d2inf1989z00, BGl_z62libraryzd2infozb0zz__libraryz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1891z00zz__libraryz00,
		BgL_bgl_string1891za700za7za7_1990za7, "dlopen-init", 11);
	      DEFINE_STRING(BGl_string1893z00zz__libraryz00,
		BgL_bgl_string1893za700za7za7_1991za7, "eval", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2threadzd2suffixzd2setz12zd2envz12zz__libraryz00,
		BgL_bgl_za762libraryza7d2thr1992z00,
		BGl_z62libraryzd2threadzd2suffixzd2setz12za2zz__libraryz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1895z00zz__libraryz00,
		BgL_bgl_string1895za700za7za7_1993za7, "init", 4);
	      DEFINE_STRING(BGl_string1897z00zz__libraryz00,
		BgL_bgl_string1897za700za7za7_1994za7, "module-eval", 11);
	      DEFINE_STRING(BGl_string1899z00zz__libraryz00,
		BgL_bgl_string1899za700za7za7_1995za7, "module-init", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2initzd2filezd2envzd2zz__libraryz00,
		BgL_bgl_za762libraryza7d2ini1996z00,
		BGl_z62libraryzd2initzd2filez62zz__libraryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_libraryzd2loadedzf3zd2envzf3zz__libraryz00,
		BgL_bgl_za762libraryza7d2loa1997z00,
		BGl_z62libraryzd2loadedzf3z43zz__libraryz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_libraryzd2loadzd2envz00zz__libraryz00,
		BgL_bgl_za762libraryza7d2loa1998z00, va_generic_entry,
		BGl_z62libraryzd2loadzb0zz__libraryz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2markzd2loadedz12zd2envzc0zz__libraryz00,
		BgL_bgl_za762libraryza7d2mar1999z00,
		BGl_z62libraryzd2markzd2loadedz12z70zz__libraryz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_libraryzd2load_ezd2envz00zz__libraryz00,
		BgL_bgl_za762libraryza7d2loa2000z00, va_generic_entry,
		BGl_z62libraryzd2load_ezb0zz__libraryz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1901z00zz__libraryz00,
		BgL_bgl_string1901za700za7za7_2001za7, "srfi", 4);
	      DEFINE_STRING(BGl_string1903z00zz__libraryz00,
		BgL_bgl_string1903za700za7za7_2002za7, "version", 7);
	      DEFINE_STRING(BGl_string1905z00zz__libraryz00,
		BgL_bgl_string1905za700za7za7_2003za7, "declare-library!", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2filezd2namezd2envzd2zz__libraryz00,
		BgL_bgl_za762libraryza7d2fil2004z00,
		BGl_z62libraryzd2filezd2namez62zz__libraryz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1906z00zz__libraryz00,
		BgL_bgl_string1906za700za7za7_2005za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string1907z00zz__libraryz00,
		BgL_bgl_string1907za700za7za7_2006za7,
		"wrong number of arguments: [1..11] expected, provided", 53);
	      DEFINE_STRING(BGl_string1908z00zz__libraryz00,
		BgL_bgl_string1908za700za7za7_2007za7, "bint", 4);
	      DEFINE_STRING(BGl_string1909z00zz__libraryz00,
		BgL_bgl_string1909za700za7za7_2008za7, "~a_~a", 5);
	      DEFINE_STRING(BGl_string1910z00zz__libraryz00,
		BgL_bgl_string1910za700za7za7_2009za7, "~a_e~a", 6);
	      DEFINE_STRING(BGl_string1912z00zz__libraryz00,
		BgL_bgl_string1912za700za7za7_2010za7, "libinfo", 7);
	      DEFINE_STRING(BGl_string1913z00zz__libraryz00,
		BgL_bgl_string1913za700za7za7_2011za7, "&library-info", 13);
	      DEFINE_STRING(BGl_string1915z00zz__libraryz00,
		BgL_bgl_string1915za700za7za7_2012za7, "library-translation-table-add!",
		30);
	      DEFINE_STRING(BGl_string1916z00zz__libraryz00,
		BgL_bgl_string1916za700za7za7_2013za7, "Illegal :dlopen-init argument", 29);
	      DEFINE_STRING(BGl_string1917z00zz__libraryz00,
		BgL_bgl_string1917za700za7za7_2014za7, "Illegal :dlopen-init value", 26);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_declarezd2libraryz12zd2envz12zz__libraryz00,
		BgL_bgl__declareza7d2libra2015za7, opt_generic_entry,
		BGl__declarezd2libraryz12zc0zz__libraryz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1918z00zz__libraryz00,
		BgL_bgl_string1918za700za7za7_2016za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string1919z00zz__libraryz00,
		BgL_bgl_string1919za700za7za7_2017za7, "_", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2translationzd2tablezd2addz12zd2envz12zz__libraryz00,
		BgL_bgl_za762libraryza7d2tra2018z00, va_generic_entry,
		BGl_z62libraryzd2translationzd2tablezd2addz12za2zz__libraryz00, BUNSPEC,
		-3);
	      DEFINE_STRING(BGl_string1920z00zz__libraryz00,
		BgL_bgl_string1920za700za7za7_2019za7, "_e", 2);
	      DEFINE_STRING(BGl_string1921z00zz__libraryz00,
		BgL_bgl_string1921za700za7za7_2020za7, "&library-translation-table-add!",
		31);
	      DEFINE_STRING(BGl_string1922z00zz__libraryz00,
		BgL_bgl_string1922za700za7za7_2021za7, ".init", 5);
	      DEFINE_STRING(BGl_string1923z00zz__libraryz00,
		BgL_bgl_string1923za700za7za7_2022za7, "&library-init-file", 18);
	      DEFINE_STRING(BGl_string1925z00zz__libraryz00,
		BgL_bgl_string1925za700za7za7_2023za7, "bigloo-c", 8);
	      DEFINE_STRING(BGl_string1926z00zz__libraryz00,
		BgL_bgl_string1926za700za7za7_2024za7, "unix", 4);
	      DEFINE_STRING(BGl_string1927z00zz__libraryz00,
		BgL_bgl_string1927za700za7za7_2025za7, "mingw", 5);
	      DEFINE_STRING(BGl_string1928z00zz__libraryz00,
		BgL_bgl_string1928za700za7za7_2026za7, "-", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_libraryzd2existszf3zd2envzf3zz__libraryz00,
		BgL_bgl_za762libraryza7d2exi2027z00, va_generic_entry,
		BGl_z62libraryzd2existszf3z43zz__libraryz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1930z00zz__libraryz00,
		BgL_bgl_string1930za700za7za7_2028za7, "library-file-name", 17);
	      DEFINE_STRING(BGl_string1931z00zz__libraryz00,
		BgL_bgl_string1931za700za7za7_2029za7, "Illegal version", 15);
	      DEFINE_STRING(BGl_string1932z00zz__libraryz00,
		BgL_bgl_string1932za700za7za7_2030za7, "win32", 5);
	      DEFINE_STRING(BGl_string1933z00zz__libraryz00,
		BgL_bgl_string1933za700za7za7_2031za7, "Unknown os", 10);
	      DEFINE_STRING(BGl_string1935z00zz__libraryz00,
		BgL_bgl_string1935za700za7za7_2032za7, "bigloo-jvm", 10);
	      DEFINE_STRING(BGl_string1937z00zz__libraryz00,
		BgL_bgl_string1937za700za7za7_2033za7, "bigloo-.net", 11);
	      DEFINE_STRING(BGl_string1939z00zz__libraryz00,
		BgL_bgl_string1939za700za7za7_2034za7, "bigloo.net", 10);
	      DEFINE_STRING(BGl_string1940z00zz__libraryz00,
		BgL_bgl_string1940za700za7za7_2035za7, "Illegal backend", 15);
	      DEFINE_STRING(BGl_string1941z00zz__libraryz00,
		BgL_bgl_string1941za700za7za7_2036za7, "&library-file-name", 18);
	      DEFINE_STRING(BGl_string1942z00zz__libraryz00,
		BgL_bgl_string1942za700za7za7_2037za7, "&library-loaded?", 16);
	      DEFINE_STRING(BGl_string1943z00zz__libraryz00,
		BgL_bgl_string1943za700za7za7_2038za7, "&library-mark-loaded!", 21);
	      DEFINE_STRING(BGl_string1862z00zz__libraryz00,
		BgL_bgl_string1862za700za7za7_2039za7, "library", 7);
	      DEFINE_STRING(BGl_string1944z00zz__libraryz00,
		BgL_bgl_string1944za700za7za7_2040za7, "BIGLOOLIB", 9);
	      DEFINE_STRING(BGl_string1863z00zz__libraryz00,
		BgL_bgl_string1863za700za7za7_2041za7, "", 0);
	      DEFINE_STRING(BGl_string1945z00zz__libraryz00,
		BgL_bgl_string1945za700za7za7_2042za7, ".", 1);
	      DEFINE_STRING(BGl_string1864z00zz__libraryz00,
		BgL_bgl_string1864za700za7za7_2043za7,
		"/tmp/bigloo/runtime/Eval/library.scm", 36);
	      DEFINE_STRING(BGl_string1946z00zz__libraryz00,
		BgL_bgl_string1946za700za7za7_2044za7, "/resource/bigloo/", 17);
	      DEFINE_STRING(BGl_string1865z00zz__libraryz00,
		BgL_bgl_string1865za700za7za7_2045za7, "&library-thread-suffix-set!", 27);
	      DEFINE_STRING(BGl_string1947z00zz__libraryz00,
		BgL_bgl_string1947za700za7za7_2046za7, "/make_lib.class", 15);
	      DEFINE_STRING(BGl_string1866z00zz__libraryz00,
		BgL_bgl_string1866za700za7za7_2047za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1948z00zz__libraryz00,
		BgL_bgl_string1948za700za7za7_2048za7, "Can't find library `~a' (`~a')",
		30);
	      DEFINE_STRING(BGl_string1867z00zz__libraryz00,
		BgL_bgl_string1867za700za7za7_2049za7, "_mt", 3);
	      DEFINE_STRING(BGl_string1869z00zz__libraryz00,
		BgL_bgl_string1869za700za7za7_2050za7, "library-safety", 14);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1911z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1914z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1884z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1886z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1888z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1924z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_za2loadedzd2librariesza2zd2zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1929z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1890z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1892z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1894z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1896z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_za2loadedzd2initzd2filesza2z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1898z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1934z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1936z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1938z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1861z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1949z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1868z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1870z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1954z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1873z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1956z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1876z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1960z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1881z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_za2librariesza2z00zz__libraryz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2loadedzd2initzd2librariesza2z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_za2libraryzd2mutexza2zd2zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_list1952z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_list1953z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_list1958z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_list1959z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_list1883z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1900z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_keyword1902z00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_libraryzd2threadzd2suffixz00zz__libraryz00));
		     ADD_ROOT((void *) (&BGl_symbol1904z00zz__libraryz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long
		BgL_checksumz00_2844, char *BgL_fromz00_2845)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__libraryz00))
				{
					BGl_requirezd2initializa7ationz75zz__libraryz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__libraryz00();
					BGl_cnstzd2initzd2zz__libraryz00();
					BGl_importedzd2moduleszd2initz00zz__libraryz00();
					return BGl_toplevelzd2initzd2zz__libraryz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			BGl_symbol1861z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1862z00zz__libraryz00);
			BGl_symbol1868z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1869z00zz__libraryz00);
			BGl_symbol1870z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1871z00zz__libraryz00);
			BGl_symbol1873z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1874z00zz__libraryz00);
			BGl_symbol1876z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1877z00zz__libraryz00);
			BGl_symbol1881z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1882z00zz__libraryz00);
			BGl_keyword1884z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1885z00zz__libraryz00);
			BGl_keyword1886z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1887z00zz__libraryz00);
			BGl_keyword1888z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1889z00zz__libraryz00);
			BGl_keyword1890z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1891z00zz__libraryz00);
			BGl_keyword1892z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1893z00zz__libraryz00);
			BGl_keyword1894z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1895z00zz__libraryz00);
			BGl_keyword1896z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1897z00zz__libraryz00);
			BGl_keyword1898z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1899z00zz__libraryz00);
			BGl_keyword1900z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1901z00zz__libraryz00);
			BGl_keyword1902z00zz__libraryz00 =
				bstring_to_keyword(BGl_string1903z00zz__libraryz00);
			BGl_list1883z00zz__libraryz00 =
				MAKE_YOUNG_PAIR(BGl_keyword1884z00zz__libraryz00,
				MAKE_YOUNG_PAIR(BGl_keyword1886z00zz__libraryz00,
					MAKE_YOUNG_PAIR(BGl_keyword1888z00zz__libraryz00,
						MAKE_YOUNG_PAIR(BGl_keyword1890z00zz__libraryz00,
							MAKE_YOUNG_PAIR(BGl_keyword1892z00zz__libraryz00,
								MAKE_YOUNG_PAIR(BGl_keyword1894z00zz__libraryz00,
									MAKE_YOUNG_PAIR(BGl_keyword1896z00zz__libraryz00,
										MAKE_YOUNG_PAIR(BGl_keyword1898z00zz__libraryz00,
											MAKE_YOUNG_PAIR(BGl_keyword1900z00zz__libraryz00,
												MAKE_YOUNG_PAIR(BGl_keyword1902z00zz__libraryz00,
													BNIL))))))))));
			BGl_symbol1904z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1905z00zz__libraryz00);
			BGl_symbol1911z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1912z00zz__libraryz00);
			BGl_symbol1914z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1915z00zz__libraryz00);
			BGl_symbol1924z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1925z00zz__libraryz00);
			BGl_symbol1929z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1930z00zz__libraryz00);
			BGl_symbol1934z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1935z00zz__libraryz00);
			BGl_symbol1936z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1937z00zz__libraryz00);
			BGl_symbol1938z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1939z00zz__libraryz00);
			BGl_symbol1949z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1950z00zz__libraryz00);
			BGl_symbol1954z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1955z00zz__libraryz00);
			BGl_symbol1956z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1957z00zz__libraryz00);
			BGl_list1953z00zz__libraryz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1954z00zz__libraryz00,
				MAKE_YOUNG_PAIR(BGl_symbol1956z00zz__libraryz00, BNIL));
			BGl_list1952z00zz__libraryz00 =
				MAKE_YOUNG_PAIR(BGl_list1953z00zz__libraryz00, BNIL);
			BGl_symbol1960z00zz__libraryz00 =
				bstring_to_symbol(BGl_string1961z00zz__libraryz00);
			BGl_list1959z00zz__libraryz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1960z00zz__libraryz00,
				MAKE_YOUNG_PAIR(BGl_symbol1956z00zz__libraryz00, BNIL));
			return (BGl_list1958z00zz__libraryz00 =
				MAKE_YOUNG_PAIR(BGl_list1959z00zz__libraryz00, BNIL), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			BGl_za2libraryzd2mutexza2zd2zz__libraryz00 =
				bgl_make_mutex(BGl_symbol1861z00zz__libraryz00);
			BGl_libraryzd2threadzd2suffixz00zz__libraryz00 =
				BGl_string1863z00zz__libraryz00;
			BGl_za2librariesza2z00zz__libraryz00 = BNIL;
			BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00 = BFALSE;
			BGl_za2loadedzd2librariesza2zd2zz__libraryz00 = BNIL;
			BGl_za2loadedzd2initzd2librariesza2z00zz__libraryz00 = BNIL;
			return (BGl_za2loadedzd2initzd2filesza2z00zz__libraryz00 = BNIL, BUNSPEC);
		}

	}



/* library-thread-suffix-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_libraryzd2threadzd2suffixzd2setz12zc0zz__libraryz00(obj_t BgL_sufz00_3)
	{
		{	/* Eval/library.scm 92 */
			return (BGl_libraryzd2threadzd2suffixz00zz__libraryz00 =
				BgL_sufz00_3, BUNSPEC);
		}

	}



/* &library-thread-suffix-set! */
	obj_t BGl_z62libraryzd2threadzd2suffixzd2setz12za2zz__libraryz00(obj_t
		BgL_envz00_2733, obj_t BgL_sufz00_2734)
	{
		{	/* Eval/library.scm 92 */
			{	/* Eval/library.scm 93 */
				obj_t BgL_auxz00_2899;

				if (STRINGP(BgL_sufz00_2734))
					{	/* Eval/library.scm 93 */
						BgL_auxz00_2899 = BgL_sufz00_2734;
					}
				else
					{
						obj_t BgL_auxz00_2902;

						BgL_auxz00_2902 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(3262L), BGl_string1865z00zz__libraryz00,
							BGl_string1866z00zz__libraryz00, BgL_sufz00_2734);
						FAILURE(BgL_auxz00_2902, BFALSE, BFALSE);
					}
				return
					BGl_libraryzd2threadzd2suffixzd2setz12zc0zz__libraryz00
					(BgL_auxz00_2899);
			}
		}

	}



/* library-multithread-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_libraryzd2multithreadzd2setz12z12zz__libraryz00(bool_t BgL_valz00_4)
	{
		{	/* Eval/library.scm 98 */
			if (BgL_valz00_4)
				{	/* Eval/library.scm 99 */
					return (BGl_libraryzd2threadzd2suffixz00zz__libraryz00 =
						BGl_string1867z00zz__libraryz00, BUNSPEC);
				}
			else
				{	/* Eval/library.scm 99 */
					return (BGl_libraryzd2threadzd2suffixz00zz__libraryz00 =
						BGl_string1863z00zz__libraryz00, BUNSPEC);
				}
		}

	}



/* &library-multithread-set! */
	obj_t BGl_z62libraryzd2multithreadzd2setz12z70zz__libraryz00(obj_t
		BgL_envz00_2735, obj_t BgL_valz00_2736)
	{
		{	/* Eval/library.scm 98 */
			return
				BGl_libraryzd2multithreadzd2setz12z12zz__libraryz00(CBOOL
				(BgL_valz00_2736));
		}

	}



/* eval-library-suffix */
	obj_t BGl_evalzd2libraryzd2suffixz00zz__libraryz00(void)
	{
		{	/* Eval/library.scm 127 */
			if (CBOOL(BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00))
				{	/* Eval/library.scm 128 */
					BFALSE;
				}
			else
				{	/* Eval/library.scm 130 */
					obj_t BgL_casezd2valuezd2_1293;

					BgL_casezd2valuezd2_1293 =
						BGl_bigloozd2configzd2zz__configurez00
						(BGl_symbol1868z00zz__libraryz00);
					if ((BgL_casezd2valuezd2_1293 == BGl_symbol1870z00zz__libraryz00))
						{	/* Eval/library.scm 130 */
							BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00 =
								BGl_string1872z00zz__libraryz00;
						}
					else
						{	/* Eval/library.scm 130 */
							if ((BgL_casezd2valuezd2_1293 == BGl_symbol1873z00zz__libraryz00))
								{	/* Eval/library.scm 130 */
									BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00 =
										BGl_string1875z00zz__libraryz00;
								}
							else
								{	/* Eval/library.scm 130 */
									if (
										(BgL_casezd2valuezd2_1293 ==
											BGl_symbol1876z00zz__libraryz00))
										{	/* Eval/library.scm 130 */
											BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00 =
												BGl_string1878z00zz__libraryz00;
										}
									else
										{	/* Eval/library.scm 130 */
											BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00 =
												BUNSPEC;
										}
								}
						}
				}
			return BGl_za2evalzd2libraryzd2suffixza2z00zz__libraryz00;
		}

	}



/* _declare-library! */
	obj_t BGl__declarezd2libraryz12zc0zz__libraryz00(obj_t BgL_env1120z00_67,
		obj_t BgL_opt1119z00_66)
	{
		{	/* Eval/library.scm 139 */
			{	/* Eval/library.scm 139 */
				obj_t BgL_idz00_1301;

				BgL_idz00_1301 = VECTOR_REF(BgL_opt1119z00_66, 0L);
				{	/* Eval/library.scm 142 */
					obj_t BgL_basenamez00_1302;

					{	/* Eval/library.scm 142 */
						obj_t BgL_symbolz00_2068;

						if (SYMBOLP(BgL_idz00_1301))
							{	/* Eval/library.scm 142 */
								BgL_symbolz00_2068 = BgL_idz00_1301;
							}
						else
							{
								obj_t BgL_auxz00_2922;

								BgL_auxz00_2922 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string1864z00zz__libraryz00, BINT(5479L),
									BGl_string1879z00zz__libraryz00,
									BGl_string1880z00zz__libraryz00, BgL_idz00_1301);
								FAILURE(BgL_auxz00_2922, BFALSE, BFALSE);
							}
						{	/* Eval/library.scm 142 */
							obj_t BgL_arg1623z00_2069;

							BgL_arg1623z00_2069 = SYMBOL_TO_STRING(BgL_symbolz00_2068);
							BgL_basenamez00_1302 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1623z00_2069);
						}
					}
					{	/* Eval/library.scm 139 */
						obj_t BgL_classzd2evalzd2_1303;

						BgL_classzd2evalzd2_1303 = BFALSE;
						{	/* Eval/library.scm 139 */
							obj_t BgL_classzd2initzd2_1304;

							BgL_classzd2initzd2_1304 = BFALSE;
							{	/* Eval/library.scm 139 */
								obj_t BgL_dlopenzd2initzd2_1305;

								BgL_dlopenzd2initzd2_1305 = BFALSE;
								{	/* Eval/library.scm 139 */
									obj_t BgL_evalz00_1306;

									BgL_evalz00_1306 = BFALSE;
									{	/* Eval/library.scm 139 */
										obj_t BgL_initz00_1307;

										BgL_initz00_1307 = BFALSE;
										{	/* Eval/library.scm 139 */
											obj_t BgL_modulezd2evalzd2_1308;

											BgL_modulezd2evalzd2_1308 = BFALSE;
											{	/* Eval/library.scm 139 */
												obj_t BgL_modulezd2initzd2_1309;

												BgL_modulezd2initzd2_1309 = BFALSE;
												{	/* Eval/library.scm 148 */
													obj_t BgL_srfiz00_1310;

													BgL_srfiz00_1310 = BNIL;
													{	/* Eval/library.scm 141 */
														obj_t BgL_versionz00_1311;

														BgL_versionz00_1311 =
															BGl_bigloozd2configzd2zz__configurez00
															(BGl_symbol1881z00zz__libraryz00);
														{	/* Eval/library.scm 139 */

															{
																long BgL_iz00_1312;

																BgL_iz00_1312 = 1L;
															BgL_check1123z00_1313:
																if (
																	(BgL_iz00_1312 ==
																		VECTOR_LENGTH(BgL_opt1119z00_66)))
																	{	/* Eval/library.scm 139 */
																		BNIL;
																	}
																else
																	{	/* Eval/library.scm 139 */
																		bool_t BgL_test2060z00_2932;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_arg1272z00_1319;

																			BgL_arg1272z00_1319 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_iz00_1312);
																			BgL_test2060z00_2932 =
																				CBOOL
																				(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1272z00_1319,
																					BGl_list1883z00zz__libraryz00));
																		}
																		if (BgL_test2060z00_2932)
																			{
																				long BgL_iz00_2936;

																				BgL_iz00_2936 = (BgL_iz00_1312 + 2L);
																				BgL_iz00_1312 = BgL_iz00_2936;
																				goto BgL_check1123z00_1313;
																			}
																		else
																			{	/* Eval/library.scm 139 */
																				obj_t BgL_arg1268z00_1318;

																				BgL_arg1268z00_1318 =
																					VECTOR_REF(BgL_opt1119z00_66,
																					BgL_iz00_1312);
																				BGl_errorz00zz__errorz00
																					(BGl_symbol1904z00zz__libraryz00,
																					BGl_string1906z00zz__libraryz00,
																					BgL_arg1268z00_1318);
																			}
																	}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1125z00_1320;

																{
																	long BgL_iz00_2078;

																	BgL_iz00_2078 = 1L;
																BgL_search1122z00_2077:
																	if (
																		(BgL_iz00_2078 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1125z00_1320 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2078 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1125z00_1320 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2088;

																					BgL_vz00_2088 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2078);
																					if ((BgL_vz00_2088 ==
																							BGl_keyword1884z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1125z00_1320 =
																								BINT((BgL_iz00_2078 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_2956;

																							BgL_iz00_2956 =
																								(BgL_iz00_2078 + 2L);
																							BgL_iz00_2078 = BgL_iz00_2956;
																							goto BgL_search1122z00_2077;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2064z00_2958;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2092;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_2959;

																			if (INTEGERP(BgL_index1125z00_1320))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_2959 =
																						BgL_index1125z00_1320;
																				}
																			else
																				{
																					obj_t BgL_auxz00_2962;

																					BgL_auxz00_2962 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1125z00_1320);
																					FAILURE(BgL_auxz00_2962, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2092 =
																				(long) CINT(BgL_tmpz00_2959);
																		}
																		BgL_test2064z00_2958 =
																			(BgL_n1z00_2092 >= 0L);
																	}
																	if (BgL_test2064z00_2958)
																		{
																			long BgL_auxz00_2968;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_2969;

																				if (INTEGERP(BgL_index1125z00_1320))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_2969 =
																							BgL_index1125z00_1320;
																					}
																				else
																					{
																						obj_t BgL_auxz00_2972;

																						BgL_auxz00_2972 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1125z00_1320);
																						FAILURE(BgL_auxz00_2972, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_2968 =
																					(long) CINT(BgL_tmpz00_2969);
																			}
																			BgL_basenamez00_1302 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_2968);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1126z00_1322;

																{
																	long BgL_iz00_2094;

																	BgL_iz00_2094 = 1L;
																BgL_search1122z00_2093:
																	if (
																		(BgL_iz00_2094 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1126z00_1322 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2094 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1126z00_1322 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2104;

																					BgL_vz00_2104 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2094);
																					if ((BgL_vz00_2104 ==
																							BGl_keyword1886z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1126z00_1322 =
																								BINT((BgL_iz00_2094 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_2994;

																							BgL_iz00_2994 =
																								(BgL_iz00_2094 + 2L);
																							BgL_iz00_2094 = BgL_iz00_2994;
																							goto BgL_search1122z00_2093;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2070z00_2996;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2108;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_2997;

																			if (INTEGERP(BgL_index1126z00_1322))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_2997 =
																						BgL_index1126z00_1322;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3000;

																					BgL_auxz00_3000 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1126z00_1322);
																					FAILURE(BgL_auxz00_3000, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2108 =
																				(long) CINT(BgL_tmpz00_2997);
																		}
																		BgL_test2070z00_2996 =
																			(BgL_n1z00_2108 >= 0L);
																	}
																	if (BgL_test2070z00_2996)
																		{
																			long BgL_auxz00_3006;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3007;

																				if (INTEGERP(BgL_index1126z00_1322))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3007 =
																							BgL_index1126z00_1322;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3010;

																						BgL_auxz00_3010 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1126z00_1322);
																						FAILURE(BgL_auxz00_3010, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3006 =
																					(long) CINT(BgL_tmpz00_3007);
																			}
																			BgL_classzd2evalzd2_1303 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3006);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1127z00_1324;

																{
																	long BgL_iz00_2110;

																	BgL_iz00_2110 = 1L;
																BgL_search1122z00_2109:
																	if (
																		(BgL_iz00_2110 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1127z00_1324 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2110 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1127z00_1324 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2120;

																					BgL_vz00_2120 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2110);
																					if ((BgL_vz00_2120 ==
																							BGl_keyword1888z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1127z00_1324 =
																								BINT((BgL_iz00_2110 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3032;

																							BgL_iz00_3032 =
																								(BgL_iz00_2110 + 2L);
																							BgL_iz00_2110 = BgL_iz00_3032;
																							goto BgL_search1122z00_2109;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2076z00_3034;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2124;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3035;

																			if (INTEGERP(BgL_index1127z00_1324))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3035 =
																						BgL_index1127z00_1324;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3038;

																					BgL_auxz00_3038 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1127z00_1324);
																					FAILURE(BgL_auxz00_3038, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2124 =
																				(long) CINT(BgL_tmpz00_3035);
																		}
																		BgL_test2076z00_3034 =
																			(BgL_n1z00_2124 >= 0L);
																	}
																	if (BgL_test2076z00_3034)
																		{
																			long BgL_auxz00_3044;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3045;

																				if (INTEGERP(BgL_index1127z00_1324))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3045 =
																							BgL_index1127z00_1324;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3048;

																						BgL_auxz00_3048 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1127z00_1324);
																						FAILURE(BgL_auxz00_3048, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3044 =
																					(long) CINT(BgL_tmpz00_3045);
																			}
																			BgL_classzd2initzd2_1304 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3044);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1128z00_1326;

																{
																	long BgL_iz00_2126;

																	BgL_iz00_2126 = 1L;
																BgL_search1122z00_2125:
																	if (
																		(BgL_iz00_2126 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1128z00_1326 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2126 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1128z00_1326 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2136;

																					BgL_vz00_2136 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2126);
																					if ((BgL_vz00_2136 ==
																							BGl_keyword1890z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1128z00_1326 =
																								BINT((BgL_iz00_2126 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3070;

																							BgL_iz00_3070 =
																								(BgL_iz00_2126 + 2L);
																							BgL_iz00_2126 = BgL_iz00_3070;
																							goto BgL_search1122z00_2125;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2082z00_3072;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2140;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3073;

																			if (INTEGERP(BgL_index1128z00_1326))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3073 =
																						BgL_index1128z00_1326;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3076;

																					BgL_auxz00_3076 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1128z00_1326);
																					FAILURE(BgL_auxz00_3076, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2140 =
																				(long) CINT(BgL_tmpz00_3073);
																		}
																		BgL_test2082z00_3072 =
																			(BgL_n1z00_2140 >= 0L);
																	}
																	if (BgL_test2082z00_3072)
																		{
																			long BgL_auxz00_3082;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3083;

																				if (INTEGERP(BgL_index1128z00_1326))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3083 =
																							BgL_index1128z00_1326;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3086;

																						BgL_auxz00_3086 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1128z00_1326);
																						FAILURE(BgL_auxz00_3086, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3082 =
																					(long) CINT(BgL_tmpz00_3083);
																			}
																			BgL_dlopenzd2initzd2_1305 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3082);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1129z00_1328;

																{
																	long BgL_iz00_2142;

																	BgL_iz00_2142 = 1L;
																BgL_search1122z00_2141:
																	if (
																		(BgL_iz00_2142 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1129z00_1328 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2142 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1129z00_1328 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2152;

																					BgL_vz00_2152 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2142);
																					if ((BgL_vz00_2152 ==
																							BGl_keyword1892z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1129z00_1328 =
																								BINT((BgL_iz00_2142 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3108;

																							BgL_iz00_3108 =
																								(BgL_iz00_2142 + 2L);
																							BgL_iz00_2142 = BgL_iz00_3108;
																							goto BgL_search1122z00_2141;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2088z00_3110;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2156;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3111;

																			if (INTEGERP(BgL_index1129z00_1328))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3111 =
																						BgL_index1129z00_1328;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3114;

																					BgL_auxz00_3114 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1129z00_1328);
																					FAILURE(BgL_auxz00_3114, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2156 =
																				(long) CINT(BgL_tmpz00_3111);
																		}
																		BgL_test2088z00_3110 =
																			(BgL_n1z00_2156 >= 0L);
																	}
																	if (BgL_test2088z00_3110)
																		{
																			long BgL_auxz00_3120;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3121;

																				if (INTEGERP(BgL_index1129z00_1328))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3121 =
																							BgL_index1129z00_1328;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3124;

																						BgL_auxz00_3124 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1129z00_1328);
																						FAILURE(BgL_auxz00_3124, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3120 =
																					(long) CINT(BgL_tmpz00_3121);
																			}
																			BgL_evalz00_1306 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3120);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1130z00_1330;

																{
																	long BgL_iz00_2158;

																	BgL_iz00_2158 = 1L;
																BgL_search1122z00_2157:
																	if (
																		(BgL_iz00_2158 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1130z00_1330 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2158 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1130z00_1330 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2168;

																					BgL_vz00_2168 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2158);
																					if ((BgL_vz00_2168 ==
																							BGl_keyword1894z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1130z00_1330 =
																								BINT((BgL_iz00_2158 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3146;

																							BgL_iz00_3146 =
																								(BgL_iz00_2158 + 2L);
																							BgL_iz00_2158 = BgL_iz00_3146;
																							goto BgL_search1122z00_2157;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2094z00_3148;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2172;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3149;

																			if (INTEGERP(BgL_index1130z00_1330))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3149 =
																						BgL_index1130z00_1330;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3152;

																					BgL_auxz00_3152 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1130z00_1330);
																					FAILURE(BgL_auxz00_3152, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2172 =
																				(long) CINT(BgL_tmpz00_3149);
																		}
																		BgL_test2094z00_3148 =
																			(BgL_n1z00_2172 >= 0L);
																	}
																	if (BgL_test2094z00_3148)
																		{
																			long BgL_auxz00_3158;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3159;

																				if (INTEGERP(BgL_index1130z00_1330))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3159 =
																							BgL_index1130z00_1330;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3162;

																						BgL_auxz00_3162 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1130z00_1330);
																						FAILURE(BgL_auxz00_3162, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3158 =
																					(long) CINT(BgL_tmpz00_3159);
																			}
																			BgL_initz00_1307 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3158);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1131z00_1332;

																{
																	long BgL_iz00_2174;

																	BgL_iz00_2174 = 1L;
																BgL_search1122z00_2173:
																	if (
																		(BgL_iz00_2174 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1131z00_1332 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2174 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1131z00_1332 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2184;

																					BgL_vz00_2184 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2174);
																					if ((BgL_vz00_2184 ==
																							BGl_keyword1896z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1131z00_1332 =
																								BINT((BgL_iz00_2174 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3184;

																							BgL_iz00_3184 =
																								(BgL_iz00_2174 + 2L);
																							BgL_iz00_2174 = BgL_iz00_3184;
																							goto BgL_search1122z00_2173;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2100z00_3186;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2188;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3187;

																			if (INTEGERP(BgL_index1131z00_1332))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3187 =
																						BgL_index1131z00_1332;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3190;

																					BgL_auxz00_3190 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1131z00_1332);
																					FAILURE(BgL_auxz00_3190, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2188 =
																				(long) CINT(BgL_tmpz00_3187);
																		}
																		BgL_test2100z00_3186 =
																			(BgL_n1z00_2188 >= 0L);
																	}
																	if (BgL_test2100z00_3186)
																		{
																			long BgL_auxz00_3196;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3197;

																				if (INTEGERP(BgL_index1131z00_1332))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3197 =
																							BgL_index1131z00_1332;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3200;

																						BgL_auxz00_3200 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1131z00_1332);
																						FAILURE(BgL_auxz00_3200, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3196 =
																					(long) CINT(BgL_tmpz00_3197);
																			}
																			BgL_modulezd2evalzd2_1308 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3196);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1132z00_1334;

																{
																	long BgL_iz00_2190;

																	BgL_iz00_2190 = 1L;
																BgL_search1122z00_2189:
																	if (
																		(BgL_iz00_2190 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1132z00_1334 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2190 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1132z00_1334 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2200;

																					BgL_vz00_2200 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2190);
																					if ((BgL_vz00_2200 ==
																							BGl_keyword1898z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1132z00_1334 =
																								BINT((BgL_iz00_2190 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3222;

																							BgL_iz00_3222 =
																								(BgL_iz00_2190 + 2L);
																							BgL_iz00_2190 = BgL_iz00_3222;
																							goto BgL_search1122z00_2189;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2106z00_3224;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2204;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3225;

																			if (INTEGERP(BgL_index1132z00_1334))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3225 =
																						BgL_index1132z00_1334;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3228;

																					BgL_auxz00_3228 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1132z00_1334);
																					FAILURE(BgL_auxz00_3228, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2204 =
																				(long) CINT(BgL_tmpz00_3225);
																		}
																		BgL_test2106z00_3224 =
																			(BgL_n1z00_2204 >= 0L);
																	}
																	if (BgL_test2106z00_3224)
																		{
																			long BgL_auxz00_3234;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3235;

																				if (INTEGERP(BgL_index1132z00_1334))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3235 =
																							BgL_index1132z00_1334;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3238;

																						BgL_auxz00_3238 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1132z00_1334);
																						FAILURE(BgL_auxz00_3238, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3234 =
																					(long) CINT(BgL_tmpz00_3235);
																			}
																			BgL_modulezd2initzd2_1309 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3234);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1133z00_1336;

																{
																	long BgL_iz00_2206;

																	BgL_iz00_2206 = 1L;
																BgL_search1122z00_2205:
																	if (
																		(BgL_iz00_2206 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1133z00_1336 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2206 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1133z00_1336 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2216;

																					BgL_vz00_2216 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2206);
																					if ((BgL_vz00_2216 ==
																							BGl_keyword1900z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1133z00_1336 =
																								BINT((BgL_iz00_2206 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3260;

																							BgL_iz00_3260 =
																								(BgL_iz00_2206 + 2L);
																							BgL_iz00_2206 = BgL_iz00_3260;
																							goto BgL_search1122z00_2205;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2112z00_3262;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2220;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3263;

																			if (INTEGERP(BgL_index1133z00_1336))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3263 =
																						BgL_index1133z00_1336;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3266;

																					BgL_auxz00_3266 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1133z00_1336);
																					FAILURE(BgL_auxz00_3266, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2220 =
																				(long) CINT(BgL_tmpz00_3263);
																		}
																		BgL_test2112z00_3262 =
																			(BgL_n1z00_2220 >= 0L);
																	}
																	if (BgL_test2112z00_3262)
																		{
																			long BgL_auxz00_3272;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3273;

																				if (INTEGERP(BgL_index1133z00_1336))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3273 =
																							BgL_index1133z00_1336;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3276;

																						BgL_auxz00_3276 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1133z00_1336);
																						FAILURE(BgL_auxz00_3276, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3272 =
																					(long) CINT(BgL_tmpz00_3273);
																			}
																			BgL_srfiz00_1310 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3272);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_index1134z00_1338;

																{
																	long BgL_iz00_2222;

																	BgL_iz00_2222 = 1L;
																BgL_search1122z00_2221:
																	if (
																		(BgL_iz00_2222 ==
																			VECTOR_LENGTH(BgL_opt1119z00_66)))
																		{	/* Eval/library.scm 139 */
																			BgL_index1134z00_1338 = BINT(-1L);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			if (
																				(BgL_iz00_2222 ==
																					(VECTOR_LENGTH(BgL_opt1119z00_66) -
																						1L)))
																				{	/* Eval/library.scm 139 */
																					BgL_index1134z00_1338 =
																						BGl_errorz00zz__errorz00
																						(BGl_symbol1904z00zz__libraryz00,
																						BGl_string1907z00zz__libraryz00,
																						BINT(VECTOR_LENGTH
																							(BgL_opt1119z00_66)));
																				}
																			else
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_vz00_2232;

																					BgL_vz00_2232 =
																						VECTOR_REF(BgL_opt1119z00_66,
																						BgL_iz00_2222);
																					if ((BgL_vz00_2232 ==
																							BGl_keyword1902z00zz__libraryz00))
																						{	/* Eval/library.scm 139 */
																							BgL_index1134z00_1338 =
																								BINT((BgL_iz00_2222 + 1L));
																						}
																					else
																						{
																							long BgL_iz00_3298;

																							BgL_iz00_3298 =
																								(BgL_iz00_2222 + 2L);
																							BgL_iz00_2222 = BgL_iz00_3298;
																							goto BgL_search1122z00_2221;
																						}
																				}
																		}
																}
																{	/* Eval/library.scm 139 */
																	bool_t BgL_test2118z00_3300;

																	{	/* Eval/library.scm 139 */
																		long BgL_n1z00_2236;

																		{	/* Eval/library.scm 139 */
																			obj_t BgL_tmpz00_3301;

																			if (INTEGERP(BgL_index1134z00_1338))
																				{	/* Eval/library.scm 139 */
																					BgL_tmpz00_3301 =
																						BgL_index1134z00_1338;
																				}
																			else
																				{
																					obj_t BgL_auxz00_3304;

																					BgL_auxz00_3304 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string1864z00zz__libraryz00,
																						BINT(5364L),
																						BGl_string1879z00zz__libraryz00,
																						BGl_string1908z00zz__libraryz00,
																						BgL_index1134z00_1338);
																					FAILURE(BgL_auxz00_3304, BFALSE,
																						BFALSE);
																				}
																			BgL_n1z00_2236 =
																				(long) CINT(BgL_tmpz00_3301);
																		}
																		BgL_test2118z00_3300 =
																			(BgL_n1z00_2236 >= 0L);
																	}
																	if (BgL_test2118z00_3300)
																		{
																			long BgL_auxz00_3310;

																			{	/* Eval/library.scm 139 */
																				obj_t BgL_tmpz00_3311;

																				if (INTEGERP(BgL_index1134z00_1338))
																					{	/* Eval/library.scm 139 */
																						BgL_tmpz00_3311 =
																							BgL_index1134z00_1338;
																					}
																				else
																					{
																						obj_t BgL_auxz00_3314;

																						BgL_auxz00_3314 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string1864z00zz__libraryz00,
																							BINT(5364L),
																							BGl_string1879z00zz__libraryz00,
																							BGl_string1908z00zz__libraryz00,
																							BgL_index1134z00_1338);
																						FAILURE(BgL_auxz00_3314, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_3310 =
																					(long) CINT(BgL_tmpz00_3311);
																			}
																			BgL_versionz00_1311 =
																				VECTOR_REF(BgL_opt1119z00_66,
																				BgL_auxz00_3310);
																		}
																	else
																		{	/* Eval/library.scm 139 */
																			BFALSE;
																		}
																}
															}
															{	/* Eval/library.scm 139 */
																obj_t BgL_arg1284z00_1340;

																BgL_arg1284z00_1340 =
																	VECTOR_REF(BgL_opt1119z00_66, 0L);
																{	/* Eval/library.scm 139 */
																	obj_t BgL_basenamez00_1341;

																	BgL_basenamez00_1341 = BgL_basenamez00_1302;
																	{	/* Eval/library.scm 139 */
																		obj_t BgL_classzd2evalzd2_1342;

																		BgL_classzd2evalzd2_1342 =
																			BgL_classzd2evalzd2_1303;
																		{	/* Eval/library.scm 139 */
																			obj_t BgL_classzd2initzd2_1343;

																			BgL_classzd2initzd2_1343 =
																				BgL_classzd2initzd2_1304;
																			{	/* Eval/library.scm 139 */
																				obj_t BgL_dlopenzd2initzd2_1344;

																				BgL_dlopenzd2initzd2_1344 =
																					BgL_dlopenzd2initzd2_1305;
																				{	/* Eval/library.scm 139 */
																					obj_t BgL_evalz00_1345;

																					BgL_evalz00_1345 = BgL_evalz00_1306;
																					{	/* Eval/library.scm 139 */
																						obj_t BgL_initz00_1346;

																						BgL_initz00_1346 = BgL_initz00_1307;
																						{	/* Eval/library.scm 139 */
																							obj_t BgL_modulezd2evalzd2_1347;

																							BgL_modulezd2evalzd2_1347 =
																								BgL_modulezd2evalzd2_1308;
																							{	/* Eval/library.scm 139 */
																								obj_t BgL_modulezd2initzd2_1348;

																								BgL_modulezd2initzd2_1348 =
																									BgL_modulezd2initzd2_1309;
																								{	/* Eval/library.scm 139 */
																									obj_t BgL_srfiz00_1349;

																									BgL_srfiz00_1349 =
																										BgL_srfiz00_1310;
																									{	/* Eval/library.scm 139 */
																										obj_t BgL_versionz00_1350;

																										BgL_versionz00_1350 =
																											BgL_versionz00_1311;
																										{	/* Eval/library.scm 149 */
																											obj_t BgL_top2122z00_3322;

																											BgL_top2122z00_3322 =
																												BGL_EXITD_TOP_AS_OBJ();
																											BGL_MUTEX_LOCK
																												(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
																											BGL_EXITD_PUSH_PROTECT
																												(BgL_top2122z00_3322,
																												BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
																											BUNSPEC;
																											{	/* Eval/library.scm 149 */
																												obj_t
																													BgL_tmp2121z00_3321;
																												if (CBOOL
																													(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																														(BgL_arg1284z00_1340,
																															BGl_za2librariesza2z00zz__libraryz00)))
																													{	/* Eval/library.scm 150 */
																														BgL_tmp2121z00_3321
																															= BFALSE;
																													}
																												else
																													{	/* Eval/library.scm 150 */
																														{	/* Eval/library.scm 154 */
																															obj_t
																																BgL_arg1310z00_2238;
																															{	/* Eval/library.scm 154 */
																																obj_t
																																	BgL_arg1311z00_2239;
																																{	/* Eval/library.scm 154 */
																																	obj_t
																																		BgL_arg1312z00_2240;
																																	obj_t
																																		BgL_arg1314z00_2241;
																																	if (CBOOL
																																		(BgL_dlopenzd2initzd2_1344))
																																		{	/* Eval/library.scm 157 */
																																			obj_t
																																				BgL_arg1315z00_2242;
																																			BgL_arg1315z00_2242
																																				=
																																				BGl_evalzd2libraryzd2suffixz00zz__libraryz00
																																				();
																																			{	/* Eval/library.scm 155 */
																																				obj_t
																																					BgL_list1316z00_2243;
																																				{	/* Eval/library.scm 155 */
																																					obj_t
																																						BgL_arg1317z00_2244;
																																					BgL_arg1317z00_2244
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1315z00_2242,
																																						BNIL);
																																					BgL_list1316z00_2243
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_dlopenzd2initzd2_1344,
																																						BgL_arg1317z00_2244);
																																				}
																																				BgL_arg1312z00_2240
																																					=
																																					BGl_formatz00zz__r4_output_6_10_3z00
																																					(BGl_string1909z00zz__libraryz00,
																																					BgL_list1316z00_2243);
																																			}
																																		}
																																	else
																																		{	/* Eval/library.scm 154 */
																																			BgL_arg1312z00_2240
																																				=
																																				BFALSE;
																																		}
																																	if (CBOOL
																																		(BgL_dlopenzd2initzd2_1344))
																																		{	/* Eval/library.scm 161 */
																																			obj_t
																																				BgL_arg1318z00_2245;
																																			BgL_arg1318z00_2245
																																				=
																																				BGl_evalzd2libraryzd2suffixz00zz__libraryz00
																																				();
																																			{	/* Eval/library.scm 159 */
																																				obj_t
																																					BgL_list1319z00_2246;
																																				{	/* Eval/library.scm 159 */
																																					obj_t
																																						BgL_arg1320z00_2247;
																																					BgL_arg1320z00_2247
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1318z00_2245,
																																						BNIL);
																																					BgL_list1319z00_2246
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_dlopenzd2initzd2_1344,
																																						BgL_arg1320z00_2247);
																																				}
																																				BgL_arg1314z00_2241
																																					=
																																					BGl_formatz00zz__r4_output_6_10_3z00
																																					(BGl_string1910z00zz__libraryz00,
																																					BgL_list1319z00_2246);
																																			}
																																		}
																																	else
																																		{	/* Eval/library.scm 158 */
																																			BgL_arg1314z00_2241
																																				=
																																				BFALSE;
																																		}
																																	{	/* Eval/library.scm 107 */
																																		obj_t
																																			BgL_newz00_2253;
																																		BgL_newz00_2253
																																			=
																																			create_struct
																																			(BGl_symbol1911z00zz__libraryz00,
																																			(int)
																																			(12L));
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3343;
																																			BgL_tmpz00_3343
																																				=
																																				(int)
																																				(11L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3343,
																																				BgL_srfiz00_1349);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3346;
																																			BgL_tmpz00_3346
																																				=
																																				(int)
																																				(10L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3346,
																																				BgL_evalz00_1345);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3349;
																																			BgL_tmpz00_3349
																																				=
																																				(int)
																																				(9L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3349,
																																				BgL_initz00_1346);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3352;
																																			BgL_tmpz00_3352
																																				=
																																				(int)
																																				(8L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3352,
																																				BgL_classzd2evalzd2_1342);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3355;
																																			BgL_tmpz00_3355
																																				=
																																				(int)
																																				(7L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3355,
																																				BgL_classzd2initzd2_1343);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3358;
																																			BgL_tmpz00_3358
																																				=
																																				(int)
																																				(6L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3358,
																																				BgL_modulezd2evalzd2_1347);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3361;
																																			BgL_tmpz00_3361
																																				=
																																				(int)
																																				(5L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3361,
																																				BgL_modulezd2initzd2_1348);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3364;
																																			BgL_tmpz00_3364
																																				=
																																				(int)
																																				(4L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3364,
																																				BgL_arg1314z00_2241);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3367;
																																			BgL_tmpz00_3367
																																				=
																																				(int)
																																				(3L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3367,
																																				BgL_arg1312z00_2240);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3370;
																																			BgL_tmpz00_3370
																																				=
																																				(int)
																																				(2L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3370,
																																				BgL_versionz00_1350);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3373;
																																			BgL_tmpz00_3373
																																				=
																																				(int)
																																				(1L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3373,
																																				BgL_basenamez00_1341);
																																		}
																																		{	/* Eval/library.scm 107 */
																																			int
																																				BgL_tmpz00_3376;
																																			BgL_tmpz00_3376
																																				=
																																				(int)
																																				(0L);
																																			STRUCT_SET
																																				(BgL_newz00_2253,
																																				BgL_tmpz00_3376,
																																				BgL_arg1284z00_1340);
																																		}
																																		BgL_arg1311z00_2239
																																			=
																																			BgL_newz00_2253;
																																}}
																																BgL_arg1310z00_2238
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1284z00_1340,
																																	BgL_arg1311z00_2239);
																															}
																															BGl_za2librariesza2z00zz__libraryz00
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1310z00_2238,
																																BGl_za2librariesza2z00zz__libraryz00);
																														}
																														{
																															obj_t
																																BgL_l1097z00_2249;
																															{	/* Eval/library.scm 166 */
																																bool_t
																																	BgL_tmpz00_3381;
																																BgL_l1097z00_2249
																																	=
																																	BgL_srfiz00_1349;
																															BgL_zc3z04anonymousza31321ze3z87_2248:
																																if (PAIRP
																																	(BgL_l1097z00_2249))
																																	{	/* Eval/library.scm 166 */
																																		{	/* Eval/library.scm 167 */
																																			obj_t
																																				BgL_sz00_2267;
																																			BgL_sz00_2267
																																				=
																																				CAR
																																				(BgL_l1097z00_2249);
																																			BGl_registerzd2srfiz12zc0zz__expander_srfi0z00
																																				(BgL_sz00_2267);
																																			bgl_register_eval_srfi
																																				(BgL_sz00_2267);
																																		}
																																		{
																																			obj_t
																																				BgL_l1097z00_3387;
																																			BgL_l1097z00_3387
																																				=
																																				CDR
																																				(BgL_l1097z00_2249);
																																			BgL_l1097z00_2249
																																				=
																																				BgL_l1097z00_3387;
																																			goto
																																				BgL_zc3z04anonymousza31321ze3z87_2248;
																																		}
																																	}
																																else
																																	{	/* Eval/library.scm 166 */
																																		BgL_tmpz00_3381
																																			=
																																			((bool_t)
																																			1);
																																	}
																																BgL_tmp2121z00_3321
																																	=
																																	BBOOL
																																	(BgL_tmpz00_3381);
																															}
																														}
																													}
																												BGL_EXITD_POP_PROTECT
																													(BgL_top2122z00_3322);
																												BUNSPEC;
																												BGL_MUTEX_UNLOCK
																													(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
																												return
																													BgL_tmp2121z00_3321;
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* declare-library! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2libraryz12zc0zz__libraryz00(obj_t
		BgL_idz00_55, obj_t BgL_basenamez00_56, obj_t BgL_classzd2evalzd2_57,
		obj_t BgL_classzd2initzd2_58, obj_t BgL_dlopenzd2initzd2_59,
		obj_t BgL_evalz00_60, obj_t BgL_initz00_61, obj_t BgL_modulezd2evalzd2_62,
		obj_t BgL_modulezd2initzd2_63, obj_t BgL_srfiz00_64,
		obj_t BgL_versionz00_65)
	{
		{	/* Eval/library.scm 139 */
			{	/* Eval/library.scm 149 */
				obj_t BgL_top2128z00_3393;

				BgL_top2128z00_3393 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2128z00_3393,
					BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BUNSPEC;
				{	/* Eval/library.scm 149 */
					obj_t BgL_tmp2127z00_3392;

					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_55,
								BGl_za2librariesza2z00zz__libraryz00)))
						{	/* Eval/library.scm 150 */
							BgL_tmp2127z00_3392 = BFALSE;
						}
					else
						{	/* Eval/library.scm 150 */
							{	/* Eval/library.scm 154 */
								obj_t BgL_arg1310z00_2272;

								{	/* Eval/library.scm 154 */
									obj_t BgL_arg1311z00_2273;

									{	/* Eval/library.scm 154 */
										obj_t BgL_arg1312z00_2274;
										obj_t BgL_arg1314z00_2275;

										if (CBOOL(BgL_dlopenzd2initzd2_59))
											{	/* Eval/library.scm 157 */
												obj_t BgL_arg1315z00_2276;

												BgL_arg1315z00_2276 =
													BGl_evalzd2libraryzd2suffixz00zz__libraryz00();
												{	/* Eval/library.scm 155 */
													obj_t BgL_list1316z00_2277;

													{	/* Eval/library.scm 155 */
														obj_t BgL_arg1317z00_2278;

														BgL_arg1317z00_2278 =
															MAKE_YOUNG_PAIR(BgL_arg1315z00_2276, BNIL);
														BgL_list1316z00_2277 =
															MAKE_YOUNG_PAIR(BgL_dlopenzd2initzd2_59,
															BgL_arg1317z00_2278);
													}
													BgL_arg1312z00_2274 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string1909z00zz__libraryz00,
														BgL_list1316z00_2277);
												}
											}
										else
											{	/* Eval/library.scm 154 */
												BgL_arg1312z00_2274 = BFALSE;
											}
										if (CBOOL(BgL_dlopenzd2initzd2_59))
											{	/* Eval/library.scm 161 */
												obj_t BgL_arg1318z00_2279;

												BgL_arg1318z00_2279 =
													BGl_evalzd2libraryzd2suffixz00zz__libraryz00();
												{	/* Eval/library.scm 159 */
													obj_t BgL_list1319z00_2280;

													{	/* Eval/library.scm 159 */
														obj_t BgL_arg1320z00_2281;

														BgL_arg1320z00_2281 =
															MAKE_YOUNG_PAIR(BgL_arg1318z00_2279, BNIL);
														BgL_list1319z00_2280 =
															MAKE_YOUNG_PAIR(BgL_dlopenzd2initzd2_59,
															BgL_arg1320z00_2281);
													}
													BgL_arg1314z00_2275 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string1910z00zz__libraryz00,
														BgL_list1319z00_2280);
												}
											}
										else
											{	/* Eval/library.scm 158 */
												BgL_arg1314z00_2275 = BFALSE;
											}
										{	/* Eval/library.scm 107 */
											obj_t BgL_newz00_2287;

											BgL_newz00_2287 =
												create_struct(BGl_symbol1911z00zz__libraryz00,
												(int) (12L));
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3414;

												BgL_tmpz00_3414 = (int) (11L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3414,
													BgL_srfiz00_64);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3417;

												BgL_tmpz00_3417 = (int) (10L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3417,
													BgL_evalz00_60);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3420;

												BgL_tmpz00_3420 = (int) (9L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3420,
													BgL_initz00_61);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3423;

												BgL_tmpz00_3423 = (int) (8L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3423,
													BgL_classzd2evalzd2_57);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3426;

												BgL_tmpz00_3426 = (int) (7L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3426,
													BgL_classzd2initzd2_58);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3429;

												BgL_tmpz00_3429 = (int) (6L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3429,
													BgL_modulezd2evalzd2_62);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3432;

												BgL_tmpz00_3432 = (int) (5L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3432,
													BgL_modulezd2initzd2_63);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3435;

												BgL_tmpz00_3435 = (int) (4L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3435,
													BgL_arg1314z00_2275);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3438;

												BgL_tmpz00_3438 = (int) (3L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3438,
													BgL_arg1312z00_2274);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3441;

												BgL_tmpz00_3441 = (int) (2L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3441,
													BgL_versionz00_65);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3444;

												BgL_tmpz00_3444 = (int) (1L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3444,
													BgL_basenamez00_56);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3447;

												BgL_tmpz00_3447 = (int) (0L);
												STRUCT_SET(BgL_newz00_2287, BgL_tmpz00_3447,
													BgL_idz00_55);
											}
											BgL_arg1311z00_2273 = BgL_newz00_2287;
									}}
									BgL_arg1310z00_2272 =
										MAKE_YOUNG_PAIR(BgL_idz00_55, BgL_arg1311z00_2273);
								}
								BGl_za2librariesza2z00zz__libraryz00 =
									MAKE_YOUNG_PAIR(BgL_arg1310z00_2272,
									BGl_za2librariesza2z00zz__libraryz00);
							}
							{
								obj_t BgL_l1097z00_2311;

								{	/* Eval/library.scm 166 */
									bool_t BgL_tmpz00_3452;

									BgL_l1097z00_2311 = BgL_srfiz00_64;
								BgL_zc3z04anonymousza31321ze3z87_2310:
									if (PAIRP(BgL_l1097z00_2311))
										{	/* Eval/library.scm 166 */
											{	/* Eval/library.scm 167 */
												obj_t BgL_sz00_2316;

												BgL_sz00_2316 = CAR(BgL_l1097z00_2311);
												BGl_registerzd2srfiz12zc0zz__expander_srfi0z00
													(BgL_sz00_2316);
												bgl_register_eval_srfi(BgL_sz00_2316);
											}
											{
												obj_t BgL_l1097z00_3458;

												BgL_l1097z00_3458 = CDR(BgL_l1097z00_2311);
												BgL_l1097z00_2311 = BgL_l1097z00_3458;
												goto BgL_zc3z04anonymousza31321ze3z87_2310;
											}
										}
									else
										{	/* Eval/library.scm 166 */
											BgL_tmpz00_3452 = ((bool_t) 1);
										}
									BgL_tmp2127z00_3392 = BBOOL(BgL_tmpz00_3452);
								}
							}
						}
					BGL_EXITD_POP_PROTECT(BgL_top2128z00_3393);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
					return BgL_tmp2127z00_3392;
				}
			}
		}

	}



/* library-info */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2infozd2zz__libraryz00(obj_t BgL_idz00_68)
	{
		{	/* Eval/library.scm 174 */
			{	/* Eval/library.scm 175 */
				obj_t BgL_g1040z00_2320;

				BgL_g1040z00_2320 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_68,
					BGl_za2librariesza2z00zz__libraryz00);
				if (CBOOL(BgL_g1040z00_2320))
					{	/* Eval/library.scm 175 */
						return CDR(((obj_t) BgL_g1040z00_2320));
					}
				else
					{	/* Eval/library.scm 175 */
						return BFALSE;
					}
			}
		}

	}



/* &library-info */
	obj_t BGl_z62libraryzd2infozb0zz__libraryz00(obj_t BgL_envz00_2737,
		obj_t BgL_idz00_2738)
	{
		{	/* Eval/library.scm 174 */
			{	/* Eval/library.scm 175 */
				obj_t BgL_auxz00_3468;

				if (SYMBOLP(BgL_idz00_2738))
					{	/* Eval/library.scm 175 */
						BgL_auxz00_3468 = BgL_idz00_2738;
					}
				else
					{
						obj_t BgL_auxz00_3471;

						BgL_auxz00_3471 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(6378L), BGl_string1913z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_idz00_2738);
						FAILURE(BgL_auxz00_3471, BFALSE, BFALSE);
					}
				return BGl_libraryzd2infozd2zz__libraryz00(BgL_auxz00_3468);
			}
		}

	}



/* library-translation-table-add! */
	BGL_EXPORTED_DEF obj_t
		BGl_libraryzd2translationzd2tablezd2addz12zc0zz__libraryz00(obj_t
		BgL_namez00_69, obj_t BgL_translationz00_70, obj_t BgL_optz00_71)
	{
		{	/* Eval/library.scm 180 */
			{	/* Eval/library.scm 181 */
				obj_t BgL_versionz00_1378;
				obj_t BgL_dlopenzd2initzd2_1379;

				BgL_versionz00_1378 =
					BGl_bigloozd2configzd2zz__configurez00
					(BGl_symbol1881z00zz__libraryz00);
				{	/* Eval/library.scm 182 */
					obj_t BgL_arg1623z00_2324;

					BgL_arg1623z00_2324 = SYMBOL_TO_STRING(BgL_namez00_69);
					BgL_dlopenzd2initzd2_1379 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1623z00_2324);
				}
				{
					obj_t BgL_optz00_1381;

					BgL_optz00_1381 = BgL_optz00_71;
				BgL_zc3z04anonymousza31324ze3z87_1382:
					if (PAIRP(BgL_optz00_1381))
						{	/* Eval/library.scm 186 */
							bool_t BgL_test2136z00_3481;

							{	/* Eval/library.scm 186 */
								obj_t BgL_tmpz00_3482;

								BgL_tmpz00_3482 = CAR(BgL_optz00_1381);
								BgL_test2136z00_3481 = STRINGP(BgL_tmpz00_3482);
							}
							if (BgL_test2136z00_3481)
								{	/* Eval/library.scm 186 */
									BgL_versionz00_1378 = CAR(BgL_optz00_1381);
									{
										obj_t BgL_optz00_3486;

										BgL_optz00_3486 = CDR(BgL_optz00_1381);
										BgL_optz00_1381 = BgL_optz00_3486;
										goto BgL_zc3z04anonymousza31324ze3z87_1382;
									}
								}
							else
								{	/* Eval/library.scm 186 */
									if (CBOOL(CAR(BgL_optz00_1381)))
										{	/* Eval/library.scm 189 */
											if (
												(CAR(BgL_optz00_1381) ==
													BGl_keyword1890z00zz__libraryz00))
												{	/* Eval/library.scm 192 */
													if (NULLP(CDR(BgL_optz00_1381)))
														{	/* Eval/library.scm 194 */
															BGl_errorz00zz__errorz00
																(BGl_symbol1914z00zz__libraryz00,
																BGl_string1916z00zz__libraryz00,
																BgL_optz00_1381);
														}
													else
														{	/* Eval/library.scm 198 */
															bool_t BgL_test2140z00_3498;

															{	/* Eval/library.scm 198 */
																obj_t BgL_tmpz00_3499;

																BgL_tmpz00_3499 = CAR(CDR(BgL_optz00_1381));
																BgL_test2140z00_3498 = STRINGP(BgL_tmpz00_3499);
															}
															if (BgL_test2140z00_3498)
																{	/* Eval/library.scm 198 */
																	BgL_dlopenzd2initzd2_1379 =
																		CAR(CDR(BgL_optz00_1381));
																	{
																		obj_t BgL_optz00_3505;

																		BgL_optz00_3505 = CDR(CDR(BgL_optz00_1381));
																		BgL_optz00_1381 = BgL_optz00_3505;
																		goto BgL_zc3z04anonymousza31324ze3z87_1382;
																	}
																}
															else
																{	/* Eval/library.scm 198 */
																	BGl_errorz00zz__errorz00
																		(BGl_symbol1914z00zz__libraryz00,
																		BGl_string1917z00zz__libraryz00,
																		BgL_optz00_1381);
																}
														}
												}
											else
												{	/* Eval/library.scm 192 */
													BGl_errorz00zz__errorz00
														(BGl_symbol1914z00zz__libraryz00,
														BGl_string1918z00zz__libraryz00, BgL_optz00_1381);
												}
										}
									else
										{	/* Eval/library.scm 189 */
											BgL_versionz00_1378 = BFALSE;
											{
												obj_t BgL_optz00_3510;

												BgL_optz00_3510 = CDR(BgL_optz00_1381);
												BgL_optz00_1381 = BgL_optz00_3510;
												goto BgL_zc3z04anonymousza31324ze3z87_1382;
											}
										}
								}
						}
					else
						{	/* Eval/library.scm 184 */
							BFALSE;
						}
				}
				{	/* Eval/library.scm 209 */
					obj_t BgL_top2142z00_3513;

					BgL_top2142z00_3513 = BGL_EXITD_TOP_AS_OBJ();
					BGL_MUTEX_LOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
					BGL_EXITD_PUSH_PROTECT(BgL_top2142z00_3513,
						BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
					BUNSPEC;
					{	/* Eval/library.scm 209 */
						obj_t BgL_tmp2141z00_3512;

						{	/* Eval/library.scm 213 */
							obj_t BgL_arg1343z00_1401;

							{	/* Eval/library.scm 213 */
								obj_t BgL_arg1344z00_1402;

								{	/* Eval/library.scm 213 */
									obj_t BgL_arg1346z00_1403;
									obj_t BgL_arg1347z00_1404;

									if (CBOOL(BgL_dlopenzd2initzd2_1379))
										{	/* Eval/library.scm 214 */
											obj_t BgL_arg1348z00_1405;
											obj_t BgL_arg1349z00_1406;

											{	/* Eval/library.scm 214 */
												obj_t BgL_namez00_2344;

												BgL_namez00_2344 = BgL_dlopenzd2initzd2_1379;
												if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00
													(BgL_namez00_2344))
													{	/* Eval/library.scm 278 */
														BgL_arg1348z00_1405 =
															bigloo_mangle(BgL_namez00_2344);
													}
												else
													{	/* Eval/library.scm 278 */
														BgL_arg1348z00_1405 = BgL_namez00_2344;
													}
											}
											BgL_arg1349z00_1406 =
												BGl_evalzd2libraryzd2suffixz00zz__libraryz00();
											BgL_arg1346z00_1403 =
												string_append_3(BgL_arg1348z00_1405,
												BGl_string1919z00zz__libraryz00, BgL_arg1349z00_1406);
										}
									else
										{	/* Eval/library.scm 213 */
											BgL_arg1346z00_1403 = BFALSE;
										}
									if (CBOOL(BgL_dlopenzd2initzd2_1379))
										{	/* Eval/library.scm 218 */
											obj_t BgL_arg1350z00_1407;
											obj_t BgL_arg1351z00_1408;

											{	/* Eval/library.scm 218 */
												obj_t BgL_namez00_2346;

												BgL_namez00_2346 = BgL_dlopenzd2initzd2_1379;
												if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00
													(BgL_namez00_2346))
													{	/* Eval/library.scm 278 */
														BgL_arg1350z00_1407 =
															bigloo_mangle(BgL_namez00_2346);
													}
												else
													{	/* Eval/library.scm 278 */
														BgL_arg1350z00_1407 = BgL_namez00_2346;
													}
											}
											BgL_arg1351z00_1408 =
												BGl_evalzd2libraryzd2suffixz00zz__libraryz00();
											BgL_arg1347z00_1404 =
												string_append_3(BgL_arg1350z00_1407,
												BGl_string1920z00zz__libraryz00, BgL_arg1351z00_1408);
										}
									else
										{	/* Eval/library.scm 217 */
											BgL_arg1347z00_1404 = BFALSE;
										}
									{	/* Eval/library.scm 212 */
										obj_t BgL_versionz00_2348;

										BgL_versionz00_2348 = BgL_versionz00_1378;
										{	/* Eval/library.scm 107 */
											obj_t BgL_newz00_2349;

											BgL_newz00_2349 =
												create_struct(BGl_symbol1911z00zz__libraryz00,
												(int) (12L));
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3533;

												BgL_tmpz00_3533 = (int) (11L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3533, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3536;

												BgL_tmpz00_3536 = (int) (10L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3536, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3539;

												BgL_tmpz00_3539 = (int) (9L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3539, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3542;

												BgL_tmpz00_3542 = (int) (8L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3542, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3545;

												BgL_tmpz00_3545 = (int) (7L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3545, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3548;

												BgL_tmpz00_3548 = (int) (6L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3548, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3551;

												BgL_tmpz00_3551 = (int) (5L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3551, BFALSE);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3554;

												BgL_tmpz00_3554 = (int) (4L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3554,
													BgL_arg1347z00_1404);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3557;

												BgL_tmpz00_3557 = (int) (3L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3557,
													BgL_arg1346z00_1403);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3560;

												BgL_tmpz00_3560 = (int) (2L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3560,
													BgL_versionz00_2348);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3563;

												BgL_tmpz00_3563 = (int) (1L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3563,
													BgL_translationz00_70);
											}
											{	/* Eval/library.scm 107 */
												int BgL_tmpz00_3566;

												BgL_tmpz00_3566 = (int) (0L);
												STRUCT_SET(BgL_newz00_2349, BgL_tmpz00_3566,
													BgL_namez00_69);
											}
											BgL_arg1344z00_1402 = BgL_newz00_2349;
								}}}
								BgL_arg1343z00_1401 =
									MAKE_YOUNG_PAIR(BgL_namez00_69, BgL_arg1344z00_1402);
							}
							BgL_tmp2141z00_3512 = (BGl_za2librariesza2z00zz__libraryz00 =
								MAKE_YOUNG_PAIR(BgL_arg1343z00_1401,
									BGl_za2librariesza2z00zz__libraryz00), BUNSPEC);
						}
						BGL_EXITD_POP_PROTECT(BgL_top2142z00_3513);
						BUNSPEC;
						BGL_MUTEX_UNLOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
						return BgL_tmp2141z00_3512;
					}
				}
			}
		}

	}



/* &library-translation-table-add! */
	obj_t BGl_z62libraryzd2translationzd2tablezd2addz12za2zz__libraryz00(obj_t
		BgL_envz00_2739, obj_t BgL_namez00_2740, obj_t BgL_translationz00_2741,
		obj_t BgL_optz00_2742)
	{
		{	/* Eval/library.scm 180 */
			{	/* Eval/library.scm 181 */
				obj_t BgL_auxz00_3580;
				obj_t BgL_auxz00_3573;

				if (STRINGP(BgL_translationz00_2741))
					{	/* Eval/library.scm 181 */
						BgL_auxz00_3580 = BgL_translationz00_2741;
					}
				else
					{
						obj_t BgL_auxz00_3583;

						BgL_auxz00_3583 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(6700L), BGl_string1921z00zz__libraryz00,
							BGl_string1866z00zz__libraryz00, BgL_translationz00_2741);
						FAILURE(BgL_auxz00_3583, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_namez00_2740))
					{	/* Eval/library.scm 181 */
						BgL_auxz00_3573 = BgL_namez00_2740;
					}
				else
					{
						obj_t BgL_auxz00_3576;

						BgL_auxz00_3576 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(6700L), BGl_string1921z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_namez00_2740);
						FAILURE(BgL_auxz00_3576, BFALSE, BFALSE);
					}
				return
					BGl_libraryzd2translationzd2tablezd2addz12zc0zz__libraryz00
					(BgL_auxz00_3573, BgL_auxz00_3580, BgL_optz00_2742);
			}
		}

	}



/* library-init-file */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2initzd2filez00zz__libraryz00(obj_t
		BgL_libz00_72)
	{
		{	/* Eval/library.scm 229 */
			{	/* Eval/library.scm 230 */
				obj_t BgL_arg1352z00_2362;

				{	/* Eval/library.scm 230 */
					obj_t BgL_arg1623z00_2364;

					BgL_arg1623z00_2364 = SYMBOL_TO_STRING(BgL_libz00_72);
					BgL_arg1352z00_2362 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1623z00_2364);
				}
				return
					string_append(BgL_arg1352z00_2362, BGl_string1922z00zz__libraryz00);
			}
		}

	}



/* &library-init-file */
	obj_t BGl_z62libraryzd2initzd2filez62zz__libraryz00(obj_t BgL_envz00_2743,
		obj_t BgL_libz00_2744)
	{
		{	/* Eval/library.scm 229 */
			{	/* Eval/library.scm 230 */
				obj_t BgL_auxz00_3591;

				if (SYMBOLP(BgL_libz00_2744))
					{	/* Eval/library.scm 230 */
						BgL_auxz00_3591 = BgL_libz00_2744;
					}
				else
					{
						obj_t BgL_auxz00_3594;

						BgL_auxz00_3594 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(8119L), BGl_string1923z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_libz00_2744);
						FAILURE(BgL_auxz00_3594, BFALSE, BFALSE);
					}
				return BGl_libraryzd2initzd2filez00zz__libraryz00(BgL_auxz00_3591);
			}
		}

	}



/* untranslate-library-name */
	obj_t BGl_untranslatezd2libraryzd2namez00zz__libraryz00(obj_t
		BgL_libraryz00_73)
	{
		{	/* Eval/library.scm 235 */
			{	/* Eval/library.scm 236 */
				obj_t BgL_infoz00_1410;

				{	/* Eval/library.scm 175 */
					obj_t BgL_g1040z00_2366;

					BgL_g1040z00_2366 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_libraryz00_73,
						BGl_za2librariesza2z00zz__libraryz00);
					if (CBOOL(BgL_g1040z00_2366))
						{	/* Eval/library.scm 175 */
							BgL_infoz00_1410 = CDR(((obj_t) BgL_g1040z00_2366));
						}
					else
						{	/* Eval/library.scm 175 */
							BgL_infoz00_1410 = BFALSE;
						}
				}
				if (CBOOL(BgL_infoz00_1410))
					{	/* Eval/library.scm 238 */
						obj_t BgL_val0_1099z00_1411;
						obj_t BgL_val1_1100z00_1412;

						BgL_val0_1099z00_1411 =
							STRUCT_REF(((obj_t) BgL_infoz00_1410), (int) (1L));
						BgL_val1_1100z00_1412 =
							STRUCT_REF(((obj_t) BgL_infoz00_1410), (int) (2L));
						{	/* Eval/library.scm 238 */
							int BgL_tmpz00_3612;

							BgL_tmpz00_3612 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3612);
						}
						{	/* Eval/library.scm 238 */
							int BgL_tmpz00_3615;

							BgL_tmpz00_3615 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3615, BgL_val1_1100z00_1412);
						}
						return BgL_val0_1099z00_1411;
					}
				else
					{	/* Eval/library.scm 239 */
						obj_t BgL_val0_1101z00_1413;
						obj_t BgL_val1_1102z00_1414;

						{	/* Eval/library.scm 239 */
							obj_t BgL_arg1623z00_2372;

							BgL_arg1623z00_2372 = SYMBOL_TO_STRING(BgL_libraryz00_73);
							BgL_val0_1101z00_1413 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1623z00_2372);
						}
						BgL_val1_1102z00_1414 =
							BGl_bigloozd2configzd2zz__configurez00
							(BGl_symbol1881z00zz__libraryz00);
						{	/* Eval/library.scm 239 */
							int BgL_tmpz00_3621;

							BgL_tmpz00_3621 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3621);
						}
						{	/* Eval/library.scm 239 */
							int BgL_tmpz00_3624;

							BgL_tmpz00_3624 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3624, BgL_val1_1102z00_1414);
						}
						return BgL_val0_1101z00_1413;
					}
			}
		}

	}



/* library-file-name */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2filezd2namez00zz__libraryz00(obj_t
		BgL_libraryz00_74, obj_t BgL_suffixz00_75, obj_t BgL_backendz00_76)
	{
		{	/* Eval/library.scm 244 */
			{	/* Eval/library.scm 255 */
				obj_t BgL_basez00_1416;

				BgL_basez00_1416 =
					BGl_untranslatezd2libraryzd2namez00zz__libraryz00(BgL_libraryz00_74);
				{	/* Eval/library.scm 256 */
					obj_t BgL_versionz00_1417;

					{	/* Eval/library.scm 257 */
						obj_t BgL_tmpz00_2379;

						{	/* Eval/library.scm 257 */
							int BgL_tmpz00_3628;

							BgL_tmpz00_3628 = (int) (1L);
							BgL_tmpz00_2379 = BGL_MVALUES_VAL(BgL_tmpz00_3628);
						}
						{	/* Eval/library.scm 257 */
							int BgL_tmpz00_3631;

							BgL_tmpz00_3631 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3631, BUNSPEC);
						}
						BgL_versionz00_1417 = BgL_tmpz00_2379;
					}
					if ((BgL_backendz00_76 == BGl_symbol1924z00zz__libraryz00))
						{	/* Eval/library.scm 260 */
							bool_t BgL_test2153z00_3636;

							{	/* Eval/library.scm 260 */
								bool_t BgL_test2154z00_3637;

								{	/* Eval/library.scm 260 */
									obj_t BgL_string1z00_2381;

									BgL_string1z00_2381 = string_to_bstring(OS_CLASS);
									{	/* Eval/library.scm 260 */
										long BgL_l1z00_2383;

										BgL_l1z00_2383 = STRING_LENGTH(BgL_string1z00_2381);
										if ((BgL_l1z00_2383 == 4L))
											{	/* Eval/library.scm 260 */
												int BgL_arg1504z00_2386;

												{	/* Eval/library.scm 260 */
													char *BgL_auxz00_3644;
													char *BgL_tmpz00_3642;

													BgL_auxz00_3644 =
														BSTRING_TO_STRING(BGl_string1926z00zz__libraryz00);
													BgL_tmpz00_3642 =
														BSTRING_TO_STRING(BgL_string1z00_2381);
													BgL_arg1504z00_2386 =
														memcmp(BgL_tmpz00_3642, BgL_auxz00_3644,
														BgL_l1z00_2383);
												}
												BgL_test2154z00_3637 =
													((long) (BgL_arg1504z00_2386) == 0L);
											}
										else
											{	/* Eval/library.scm 260 */
												BgL_test2154z00_3637 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2154z00_3637)
									{	/* Eval/library.scm 260 */
										BgL_test2153z00_3636 = ((bool_t) 1);
									}
								else
									{	/* Eval/library.scm 261 */
										obj_t BgL_string1z00_2392;

										BgL_string1z00_2392 = string_to_bstring(OS_CLASS);
										{	/* Eval/library.scm 261 */
											long BgL_l1z00_2394;

											BgL_l1z00_2394 = STRING_LENGTH(BgL_string1z00_2392);
											if ((BgL_l1z00_2394 == 5L))
												{	/* Eval/library.scm 261 */
													int BgL_arg1504z00_2397;

													{	/* Eval/library.scm 261 */
														char *BgL_auxz00_3655;
														char *BgL_tmpz00_3653;

														BgL_auxz00_3655 =
															BSTRING_TO_STRING
															(BGl_string1927z00zz__libraryz00);
														BgL_tmpz00_3653 =
															BSTRING_TO_STRING(BgL_string1z00_2392);
														BgL_arg1504z00_2397 =
															memcmp(BgL_tmpz00_3653, BgL_auxz00_3655,
															BgL_l1z00_2394);
													}
													BgL_test2153z00_3636 =
														((long) (BgL_arg1504z00_2397) == 0L);
												}
											else
												{	/* Eval/library.scm 261 */
													BgL_test2153z00_3636 = ((bool_t) 0);
												}
										}
									}
							}
							if (BgL_test2153z00_3636)
								{	/* Eval/library.scm 260 */
									if (CBOOL(BgL_versionz00_1417))
										{	/* Eval/library.scm 248 */
											if (STRINGP(BgL_versionz00_1417))
												{	/* Eval/library.scm 251 */
													obj_t BgL_list1369z00_2404;

													{	/* Eval/library.scm 251 */
														obj_t BgL_arg1370z00_2405;

														{	/* Eval/library.scm 251 */
															obj_t BgL_arg1371z00_2406;

															{	/* Eval/library.scm 251 */
																obj_t BgL_arg1372z00_2407;

																{	/* Eval/library.scm 251 */
																	obj_t BgL_arg1373z00_2408;

																	BgL_arg1373z00_2408 =
																		MAKE_YOUNG_PAIR(BgL_versionz00_1417, BNIL);
																	BgL_arg1372z00_2407 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1928z00zz__libraryz00,
																		BgL_arg1373z00_2408);
																}
																BgL_arg1371z00_2406 =
																	MAKE_YOUNG_PAIR
																	(BGl_libraryzd2threadzd2suffixz00zz__libraryz00,
																	BgL_arg1372z00_2407);
															}
															BgL_arg1370z00_2405 =
																MAKE_YOUNG_PAIR(BgL_suffixz00_75,
																BgL_arg1371z00_2406);
														}
														BgL_list1369z00_2404 =
															MAKE_YOUNG_PAIR(BgL_basez00_1416,
															BgL_arg1370z00_2405);
													}
													return
														BGl_stringzd2appendzd2zz__r4_strings_6_7z00
														(BgL_list1369z00_2404);
												}
											else
												{	/* Eval/library.scm 250 */
													return
														BGl_errorz00zz__errorz00
														(BGl_symbol1929z00zz__libraryz00,
														BGl_string1931z00zz__libraryz00,
														BgL_versionz00_1417);
												}
										}
									else
										{	/* Eval/library.scm 248 */
											return
												string_append_3(BgL_basez00_1416, BgL_suffixz00_75,
												BGl_libraryzd2threadzd2suffixz00zz__libraryz00);
										}
								}
							else
								{	/* Eval/library.scm 263 */
									bool_t BgL_test2159z00_3672;

									{	/* Eval/library.scm 263 */
										obj_t BgL_string1z00_2409;

										BgL_string1z00_2409 = string_to_bstring(OS_CLASS);
										{	/* Eval/library.scm 263 */
											long BgL_l1z00_2411;

											BgL_l1z00_2411 = STRING_LENGTH(BgL_string1z00_2409);
											if ((BgL_l1z00_2411 == 5L))
												{	/* Eval/library.scm 263 */
													int BgL_arg1504z00_2414;

													{	/* Eval/library.scm 263 */
														char *BgL_auxz00_3679;
														char *BgL_tmpz00_3677;

														BgL_auxz00_3679 =
															BSTRING_TO_STRING
															(BGl_string1932z00zz__libraryz00);
														BgL_tmpz00_3677 =
															BSTRING_TO_STRING(BgL_string1z00_2409);
														BgL_arg1504z00_2414 =
															memcmp(BgL_tmpz00_3677, BgL_auxz00_3679,
															BgL_l1z00_2411);
													}
													BgL_test2159z00_3672 =
														((long) (BgL_arg1504z00_2414) == 0L);
												}
											else
												{	/* Eval/library.scm 263 */
													BgL_test2159z00_3672 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2159z00_3672)
										{	/* Eval/library.scm 263 */
											return string_append(BgL_basez00_1416, BgL_suffixz00_75);
										}
									else
										{	/* Eval/library.scm 263 */
											return
												BGl_errorz00zz__errorz00
												(BGl_symbol1929z00zz__libraryz00,
												BGl_string1933z00zz__libraryz00,
												string_to_bstring(OS_CLASS));
										}
								}
						}
					else
						{	/* Eval/library.scm 257 */
							if ((BgL_backendz00_76 == BGl_symbol1934z00zz__libraryz00))
								{	/* Eval/library.scm 257 */
									if (CBOOL(BgL_versionz00_1417))
										{	/* Eval/library.scm 248 */
											if (STRINGP(BgL_versionz00_1417))
												{	/* Eval/library.scm 251 */
													obj_t BgL_list1369z00_2422;

													{	/* Eval/library.scm 251 */
														obj_t BgL_arg1370z00_2423;

														{	/* Eval/library.scm 251 */
															obj_t BgL_arg1371z00_2424;

															{	/* Eval/library.scm 251 */
																obj_t BgL_arg1372z00_2425;

																{	/* Eval/library.scm 251 */
																	obj_t BgL_arg1373z00_2426;

																	BgL_arg1373z00_2426 =
																		MAKE_YOUNG_PAIR(BgL_versionz00_1417, BNIL);
																	BgL_arg1372z00_2425 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1928z00zz__libraryz00,
																		BgL_arg1373z00_2426);
																}
																BgL_arg1371z00_2424 =
																	MAKE_YOUNG_PAIR
																	(BGl_libraryzd2threadzd2suffixz00zz__libraryz00,
																	BgL_arg1372z00_2425);
															}
															BgL_arg1370z00_2423 =
																MAKE_YOUNG_PAIR(BgL_suffixz00_75,
																BgL_arg1371z00_2424);
														}
														BgL_list1369z00_2422 =
															MAKE_YOUNG_PAIR(BgL_basez00_1416,
															BgL_arg1370z00_2423);
													}
													return
														BGl_stringzd2appendzd2zz__r4_strings_6_7z00
														(BgL_list1369z00_2422);
												}
											else
												{	/* Eval/library.scm 250 */
													return
														BGl_errorz00zz__errorz00
														(BGl_symbol1929z00zz__libraryz00,
														BGl_string1931z00zz__libraryz00,
														BgL_versionz00_1417);
												}
										}
									else
										{	/* Eval/library.scm 248 */
											return
												string_append_3(BgL_basez00_1416, BgL_suffixz00_75,
												BGl_libraryzd2threadzd2suffixz00zz__libraryz00);
										}
								}
							else
								{	/* Eval/library.scm 257 */
									bool_t BgL_test2164z00_3701;

									{	/* Eval/library.scm 257 */
										bool_t BgL__ortest_1042z00_1435;

										BgL__ortest_1042z00_1435 =
											(BgL_backendz00_76 == BGl_symbol1936z00zz__libraryz00);
										if (BgL__ortest_1042z00_1435)
											{	/* Eval/library.scm 257 */
												BgL_test2164z00_3701 = BgL__ortest_1042z00_1435;
											}
										else
											{	/* Eval/library.scm 257 */
												BgL_test2164z00_3701 =
													(BgL_backendz00_76 ==
													BGl_symbol1938z00zz__libraryz00);
											}
									}
									if (BgL_test2164z00_3701)
										{	/* Eval/library.scm 257 */
											if (CBOOL(BgL_versionz00_1417))
												{	/* Eval/library.scm 248 */
													if (STRINGP(BgL_versionz00_1417))
														{	/* Eval/library.scm 251 */
															obj_t BgL_list1369z00_2428;

															{	/* Eval/library.scm 251 */
																obj_t BgL_arg1370z00_2429;

																{	/* Eval/library.scm 251 */
																	obj_t BgL_arg1371z00_2430;

																	{	/* Eval/library.scm 251 */
																		obj_t BgL_arg1372z00_2431;

																		{	/* Eval/library.scm 251 */
																			obj_t BgL_arg1373z00_2432;

																			BgL_arg1373z00_2432 =
																				MAKE_YOUNG_PAIR(BgL_versionz00_1417,
																				BNIL);
																			BgL_arg1372z00_2431 =
																				MAKE_YOUNG_PAIR
																				(BGl_string1928z00zz__libraryz00,
																				BgL_arg1373z00_2432);
																		}
																		BgL_arg1371z00_2430 =
																			MAKE_YOUNG_PAIR
																			(BGl_libraryzd2threadzd2suffixz00zz__libraryz00,
																			BgL_arg1372z00_2431);
																	}
																	BgL_arg1370z00_2429 =
																		MAKE_YOUNG_PAIR(BgL_suffixz00_75,
																		BgL_arg1371z00_2430);
																}
																BgL_list1369z00_2428 =
																	MAKE_YOUNG_PAIR(BgL_basez00_1416,
																	BgL_arg1370z00_2429);
															}
															return
																BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																(BgL_list1369z00_2428);
														}
													else
														{	/* Eval/library.scm 250 */
															return
																BGl_errorz00zz__errorz00
																(BGl_symbol1929z00zz__libraryz00,
																BGl_string1931z00zz__libraryz00,
																BgL_versionz00_1417);
														}
												}
											else
												{	/* Eval/library.scm 248 */
													return
														string_append_3(BgL_basez00_1416, BgL_suffixz00_75,
														BGl_libraryzd2threadzd2suffixz00zz__libraryz00);
												}
										}
									else
										{	/* Eval/library.scm 257 */
											return
												BGl_errorz00zz__errorz00
												(BGl_symbol1929z00zz__libraryz00,
												BGl_string1940z00zz__libraryz00, BgL_backendz00_76);
										}
								}
						}
				}
			}
		}

	}



/* &library-file-name */
	obj_t BGl_z62libraryzd2filezd2namez62zz__libraryz00(obj_t BgL_envz00_2745,
		obj_t BgL_libraryz00_2746, obj_t BgL_suffixz00_2747,
		obj_t BgL_backendz00_2748)
	{
		{	/* Eval/library.scm 244 */
			{	/* Eval/library.scm 255 */
				obj_t BgL_auxz00_3732;
				obj_t BgL_auxz00_3725;
				obj_t BgL_auxz00_3718;

				if (SYMBOLP(BgL_backendz00_2748))
					{	/* Eval/library.scm 255 */
						BgL_auxz00_3732 = BgL_backendz00_2748;
					}
				else
					{
						obj_t BgL_auxz00_3735;

						BgL_auxz00_3735 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(9176L), BGl_string1941z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_backendz00_2748);
						FAILURE(BgL_auxz00_3735, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_suffixz00_2747))
					{	/* Eval/library.scm 255 */
						BgL_auxz00_3725 = BgL_suffixz00_2747;
					}
				else
					{
						obj_t BgL_auxz00_3728;

						BgL_auxz00_3728 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(9176L), BGl_string1941z00zz__libraryz00,
							BGl_string1866z00zz__libraryz00, BgL_suffixz00_2747);
						FAILURE(BgL_auxz00_3728, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_libraryz00_2746))
					{	/* Eval/library.scm 255 */
						BgL_auxz00_3718 = BgL_libraryz00_2746;
					}
				else
					{
						obj_t BgL_auxz00_3721;

						BgL_auxz00_3721 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(9176L), BGl_string1941z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_libraryz00_2746);
						FAILURE(BgL_auxz00_3721, BFALSE, BFALSE);
					}
				return
					BGl_libraryzd2filezd2namez00zz__libraryz00(BgL_auxz00_3718,
					BgL_auxz00_3725, BgL_auxz00_3732);
			}
		}

	}



/* library-loaded? */
	BGL_EXPORTED_DEF bool_t BGl_libraryzd2loadedzf3z21zz__libraryz00(obj_t
		BgL_libz00_78)
	{
		{	/* Eval/library.scm 291 */
			{	/* Eval/library.scm 292 */
				obj_t BgL_top2172z00_3741;

				BgL_top2172z00_3741 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2172z00_3741,
					BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BUNSPEC;
				{	/* Eval/library.scm 292 */
					bool_t BgL_tmp2171z00_3740;

					BgL_tmp2171z00_3740 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_libz00_78,
							BGl_za2loadedzd2librariesza2zd2zz__libraryz00));
					BGL_EXITD_POP_PROTECT(BgL_top2172z00_3741);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
					return BgL_tmp2171z00_3740;
				}
			}
		}

	}



/* &library-loaded? */
	obj_t BGl_z62libraryzd2loadedzf3z43zz__libraryz00(obj_t BgL_envz00_2749,
		obj_t BgL_libz00_2750)
	{
		{	/* Eval/library.scm 291 */
			{	/* Eval/library.scm 292 */
				bool_t BgL_tmpz00_3749;

				{	/* Eval/library.scm 292 */
					obj_t BgL_auxz00_3750;

					if (SYMBOLP(BgL_libz00_2750))
						{	/* Eval/library.scm 292 */
							BgL_auxz00_3750 = BgL_libz00_2750;
						}
					else
						{
							obj_t BgL_auxz00_3753;

							BgL_auxz00_3753 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
								BINT(10618L), BGl_string1942z00zz__libraryz00,
								BGl_string1880z00zz__libraryz00, BgL_libz00_2750);
							FAILURE(BgL_auxz00_3753, BFALSE, BFALSE);
						}
					BgL_tmpz00_3749 =
						BGl_libraryzd2loadedzf3z21zz__libraryz00(BgL_auxz00_3750);
				}
				return BBOOL(BgL_tmpz00_3749);
			}
		}

	}



/* library-mark-loaded! */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2markzd2loadedz12z12zz__libraryz00(obj_t
		BgL_libz00_79)
	{
		{	/* Eval/library.scm 298 */
			BGL_MUTEX_LOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
			{	/* Eval/library.scm 299 */
				obj_t BgL_tmp2174z00_3759;

				BgL_tmp2174z00_3759 = (BGl_za2loadedzd2librariesza2zd2zz__libraryz00 =
					MAKE_YOUNG_PAIR(BgL_libz00_79,
						BGl_za2loadedzd2librariesza2zd2zz__libraryz00), BUNSPEC);
				BGL_MUTEX_UNLOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BgL_tmp2174z00_3759;
			}
			return BUNSPEC;
		}

	}



/* &library-mark-loaded! */
	obj_t BGl_z62libraryzd2markzd2loadedz12z70zz__libraryz00(obj_t
		BgL_envz00_2751, obj_t BgL_libz00_2752)
	{
		{	/* Eval/library.scm 298 */
			{	/* Eval/library.scm 300 */
				obj_t BgL_auxz00_3763;

				if (SYMBOLP(BgL_libz00_2752))
					{	/* Eval/library.scm 300 */
						BgL_auxz00_3763 = BgL_libz00_2752;
					}
				else
					{
						obj_t BgL_auxz00_3766;

						BgL_auxz00_3766 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(11036L), BGl_string1943z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_libz00_2752);
						FAILURE(BgL_auxz00_3766, BFALSE, BFALSE);
					}
				return BGl_libraryzd2markzd2loadedz12z12zz__libraryz00(BgL_auxz00_3763);
			}
		}

	}



/* load-init */
	obj_t BGl_loadzd2initzd2zz__libraryz00(obj_t BgL_initz00_80)
	{
		{	/* Eval/library.scm 306 */
			{	/* Eval/library.scm 307 */
				obj_t BgL_top2177z00_3772;

				BgL_top2177z00_3772 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2177z00_3772,
					BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
				BUNSPEC;
				{	/* Eval/library.scm 307 */
					obj_t BgL_tmp2176z00_3771;

					if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00(BgL_initz00_80,
								BGl_za2loadedzd2initzd2librariesza2z00zz__libraryz00)))
						{	/* Eval/library.scm 308 */
							BgL_tmp2176z00_3771 = BFALSE;
						}
					else
						{	/* Eval/library.scm 308 */
							BGl_za2loadedzd2initzd2librariesza2z00zz__libraryz00 =
								MAKE_YOUNG_PAIR(BgL_initz00_80,
								BGl_za2loadedzd2initzd2librariesza2z00zz__libraryz00);
							{	/* Eval/eval.scm 92 */
								obj_t BgL_envz00_1450;

								BgL_envz00_1450 = BGl_defaultzd2environmentzd2zz__evalz00();
								{	/* Eval/eval.scm 92 */

									BgL_tmp2176z00_3771 =
										BGl_loadqz00zz__evalz00(BgL_initz00_80, BgL_envz00_1450);
								}
							}
						}
					BGL_EXITD_POP_PROTECT(BgL_top2177z00_3772);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
					return BgL_tmp2176z00_3771;
				}
			}
		}

	}



/* library-load */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2loadzd2zz__libraryz00(obj_t
		BgL_libz00_81, obj_t BgL_pathz00_82)
	{
		{	/* Eval/library.scm 315 */
			{

				{	/* Eval/library.scm 399 */
					obj_t BgL_modz00_1452;

					BgL_modz00_1452 = BGl_evalzd2modulezd2zz__evmodulez00();
					{	/* Eval/library.scm 400 */
						obj_t BgL_tmpz00_3785;

						BgL_tmpz00_3785 = BGl_interactionzd2environmentzd2zz__evalz00();
						BGL_MODULE_SET(BgL_tmpz00_3785);
					}
					{	/* Eval/library.scm 401 */
						obj_t BgL_exitd1048z00_1454;

						BgL_exitd1048z00_1454 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Eval/library.scm 403 */
							obj_t BgL_zc3z04anonymousza31377ze3z87_2753;

							BgL_zc3z04anonymousza31377ze3z87_2753 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31377ze3ze5zz__libraryz00, (int) (0L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_2753, (int) (0L),
								BgL_modz00_1452);
							{	/* Eval/library.scm 401 */
								obj_t BgL_arg1783z00_2459;

								{	/* Eval/library.scm 401 */
									obj_t BgL_arg1785z00_2460;

									BgL_arg1785z00_2460 =
										BGL_EXITD_PROTECT(BgL_exitd1048z00_1454);
									BgL_arg1783z00_2459 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31377ze3z87_2753,
										BgL_arg1785z00_2460);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1048z00_1454,
									BgL_arg1783z00_2459);
								BUNSPEC;
							}
							{	/* Eval/library.scm 402 */
								obj_t BgL_tmp1050z00_1456;

								if (STRINGP(BgL_libz00_81))
									{	/* Llib/os.scm 286 */

										BgL_tmp1050z00_1456 =
											BGl_dynamiczd2loadzd2zz__osz00(BgL_libz00_81,
											string_to_bstring(BGL_DYNAMIC_LOAD_INIT), BFALSE);
									}
								else
									{	/* Eval/library.scm 319 */
										if (SYMBOLP(BgL_libz00_81))
											{	/* Eval/library.scm 323 */
												bool_t BgL_test2181z00_3803;

												{	/* Eval/library.scm 323 */
													bool_t BgL_res1816z00_2435;

													{	/* Eval/library.scm 292 */
														obj_t BgL_top2183z00_3805;

														BgL_top2183z00_3805 = BGL_EXITD_TOP_AS_OBJ();
														BGL_MUTEX_LOCK
															(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
														BGL_EXITD_PUSH_PROTECT(BgL_top2183z00_3805,
															BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
														BUNSPEC;
														{	/* Eval/library.scm 292 */
															bool_t BgL_tmp2182z00_3804;

															BgL_tmp2182z00_3804 =
																CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(BgL_libz00_81,
																	BGl_za2loadedzd2librariesza2zd2zz__libraryz00));
															BGL_EXITD_POP_PROTECT(BgL_top2183z00_3805);
															BUNSPEC;
															BGL_MUTEX_UNLOCK
																(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
															BgL_res1816z00_2435 = BgL_tmp2182z00_3804;
														}
													}
													BgL_test2181z00_3803 = BgL_res1816z00_2435;
												}
												if (BgL_test2181z00_3803)
													{	/* Eval/library.scm 175 */
														obj_t BgL_g1040z00_2437;

														BgL_g1040z00_2437 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_libz00_81,
															BGl_za2librariesza2z00zz__libraryz00);
														if (CBOOL(BgL_g1040z00_2437))
															{	/* Eval/library.scm 175 */
																BgL_tmp1050z00_1456 =
																	CDR(((obj_t) BgL_g1040z00_2437));
															}
														else
															{	/* Eval/library.scm 175 */
																BgL_tmp1050z00_1456 = BFALSE;
															}
													}
												else
													{	/* Eval/library.scm 326 */
														obj_t BgL_pathz00_1466;

														if (PAIRP(BgL_pathz00_82))
															{	/* Eval/library.scm 326 */
																BgL_pathz00_1466 = BgL_pathz00_82;
															}
														else
															{	/* Eval/library.scm 328 */
																obj_t BgL_venvz00_1520;

																BgL_venvz00_1520 =
																	BGl_getenvz00zz__osz00
																	(BGl_string1944z00zz__libraryz00);
																if (CBOOL(BgL_venvz00_1520))
																	{	/* Eval/library.scm 329 */
																		BgL_pathz00_1466 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1945z00zz__libraryz00,
																			BGl_unixzd2pathzd2ze3listze3zz__osz00
																			(BgL_venvz00_1520));
																	}
																else
																	{	/* Eval/library.scm 329 */
																		BgL_pathz00_1466 =
																			BGl_bigloozd2libraryzd2pathz00zz__paramz00
																			();
																	}
															}
														{	/* Eval/library.scm 326 */
															obj_t BgL_initz00_1467;

															{	/* Eval/library.scm 332 */
																obj_t BgL_arg1411z00_1518;

																{	/* Eval/library.scm 230 */
																	obj_t BgL_arg1352z00_2441;

																	{	/* Eval/library.scm 230 */
																		obj_t BgL_arg1623z00_2443;

																		BgL_arg1623z00_2443 =
																			SYMBOL_TO_STRING(BgL_libz00_81);
																		BgL_arg1352z00_2441 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1623z00_2443);
																	}
																	BgL_arg1411z00_1518 =
																		string_append(BgL_arg1352z00_2441,
																		BGl_string1922z00zz__libraryz00);
																}
																BgL_initz00_1467 =
																	BGl_findzd2filezf2pathz20zz__osz00
																	(BgL_arg1411z00_1518, BgL_pathz00_1466);
															}
															{	/* Eval/library.scm 332 */
																obj_t BgL_bez00_1468;

																BgL_bez00_1468 =
																	BGl_symbol1924z00zz__libraryz00;
																{	/* Eval/library.scm 333 */

																	if (CBOOL(BgL_initz00_1467))
																		{	/* Eval/library.scm 336 */
																			BGl_loadzd2initzd2zz__libraryz00
																				(BgL_initz00_1467);
																		}
																	else
																		{	/* Eval/library.scm 336 */
																			BFALSE;
																		}
																	{	/* Eval/library.scm 337 */
																		obj_t BgL_infoz00_1469;

																		{	/* Eval/library.scm 175 */
																			obj_t BgL_g1040z00_2445;

																			BgL_g1040z00_2445 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_libz00_81,
																				BGl_za2librariesza2z00zz__libraryz00);
																			if (CBOOL(BgL_g1040z00_2445))
																				{	/* Eval/library.scm 175 */
																					BgL_infoz00_1469 =
																						CDR(((obj_t) BgL_g1040z00_2445));
																				}
																			else
																				{	/* Eval/library.scm 175 */
																					BgL_infoz00_1469 = BFALSE;
																				}
																		}
																		{	/* Eval/library.scm 337 */
																			obj_t BgL_nz00_1470;

																			BgL_nz00_1470 =
																				BGl_makezd2sharedzd2libzd2namezd2zz__osz00
																				(BGl_libraryzd2filezd2namez00zz__libraryz00
																				(BgL_libz00_81,
																					BGl_string1863z00zz__libraryz00,
																					BgL_bez00_1468), BgL_bez00_1468);
																			{	/* Eval/library.scm 338 */
																				obj_t BgL_nsz00_1471;

																				BgL_nsz00_1471 =
																					BGl_makezd2sharedzd2libzd2namezd2zz__osz00
																					(BGl_libraryzd2filezd2namez00zz__libraryz00
																					(BgL_libz00_81,
																						string_append
																						(BGl_string1919z00zz__libraryz00,
																							BGl_evalzd2libraryzd2suffixz00zz__libraryz00
																							()), BgL_bez00_1468),
																					BgL_bez00_1468);
																				{	/* Eval/library.scm 340 */
																					obj_t BgL_nez00_1472;

																					BgL_nez00_1472 =
																						BGl_makezd2sharedzd2libzd2namezd2zz__osz00
																						(BGl_libraryzd2filezd2namez00zz__libraryz00
																						(BgL_libz00_81,
																							string_append
																							(BGl_string1920z00zz__libraryz00,
																								BGl_evalzd2libraryzd2suffixz00zz__libraryz00
																								()), BgL_bez00_1468),
																						BgL_bez00_1468);
																					{	/* Eval/library.scm 344 */
																						obj_t BgL_rscz00_1473;

																						{	/* Eval/library.scm 348 */
																							obj_t BgL_pz00_1508;

																							{	/* Eval/library.scm 349 */
																								obj_t BgL_arg1402z00_1510;

																								{	/* Eval/library.scm 349 */
																									obj_t BgL_arg1623z00_2449;

																									BgL_arg1623z00_2449 =
																										SYMBOL_TO_STRING
																										(BgL_libz00_81);
																									BgL_arg1402z00_1510 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg1623z00_2449);
																								}
																								BgL_pz00_1508 =
																									string_append_3
																									(BGl_string1946z00zz__libraryz00,
																									BgL_arg1402z00_1510,
																									BGl_string1947z00zz__libraryz00);
																							}
																							if (fexists(BSTRING_TO_STRING
																									(BgL_pz00_1508)))
																								{	/* Eval/library.scm 357 */
																									BgL_rscz00_1473 =
																										BgL_pz00_1508;
																								}
																							else
																								{	/* Eval/library.scm 357 */
																									BgL_rscz00_1473 = BFALSE;
																								}
																						}
																						{	/* Eval/library.scm 348 */
																							obj_t BgL_libsz00_1474;

																							BgL_libsz00_1474 =
																								BGl_findzd2filezf2pathz20zz__osz00
																								(BgL_nsz00_1471,
																								BgL_pathz00_1466);
																							{	/* Eval/library.scm 358 */
																								obj_t BgL_libez00_1475;

																								BgL_libez00_1475 =
																									BGl_findzd2filezf2pathz20zz__osz00
																									(BgL_nez00_1472,
																									BgL_pathz00_1466);
																								{	/* Eval/library.scm 359 */
																									obj_t BgL_namez00_1476;

																									{	/* Eval/library.scm 360 */
																										obj_t BgL_arg1623z00_2452;

																										BgL_arg1623z00_2452 =
																											SYMBOL_TO_STRING
																											(BgL_libz00_81);
																										BgL_namez00_1476 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1623z00_2452);
																									}
																									{	/* Eval/library.scm 360 */
																										obj_t BgL_init_sz00_1477;

																										if (CBOOL(BgL_infoz00_1469))
																											{	/* Eval/library.scm 361 */
																												BgL_init_sz00_1477 =
																													STRUCT_REF(
																													((obj_t)
																														BgL_infoz00_1469),
																													(int) (3L));
																											}
																										else
																											{	/* Eval/library.scm 361 */
																												BgL_init_sz00_1477 =
																													BFALSE;
																											}
																										{	/* Eval/library.scm 361 */
																											obj_t BgL_init_ez00_1478;

																											if (CBOOL
																												(BgL_infoz00_1469))
																												{	/* Eval/library.scm 362 */
																													BgL_init_ez00_1478 =
																														STRUCT_REF(
																														((obj_t)
																															BgL_infoz00_1469),
																														(int) (4L));
																												}
																											else
																												{	/* Eval/library.scm 362 */
																													BgL_init_ez00_1478 =
																														BFALSE;
																												}
																											{	/* Eval/library.scm 362 */
																												obj_t
																													BgL_module_sz00_1479;
																												if (CBOOL
																													(BgL_infoz00_1469))
																													{	/* Eval/library.scm 363 */
																														BgL_module_sz00_1479
																															=
																															STRUCT_REF((
																																(obj_t)
																																BgL_infoz00_1469),
																															(int) (5L));
																													}
																												else
																													{	/* Eval/library.scm 363 */
																														BgL_module_sz00_1479
																															= BFALSE;
																													}
																												{	/* Eval/library.scm 363 */
																													obj_t
																														BgL_module_ez00_1480;
																													if (CBOOL
																														(BgL_infoz00_1469))
																														{	/* Eval/library.scm 367 */
																															BgL_module_ez00_1480
																																=
																																STRUCT_REF((
																																	(obj_t)
																																	BgL_infoz00_1469),
																																(int) (6L));
																														}
																													else
																														{	/* Eval/library.scm 367 */
																															BgL_module_ez00_1480
																																= BFALSE;
																														}
																													{	/* Eval/library.scm 367 */

																														{	/* Eval/library.scm 372 */
																															bool_t
																																BgL_test2194z00_3878;
																															if (STRINGP
																																(BgL_rscz00_1473))
																																{	/* Eval/library.scm 372 */
																																	BgL_test2194z00_3878
																																		=
																																		((bool_t)
																																		0);
																																}
																															else
																																{	/* Eval/library.scm 372 */
																																	if (STRINGP
																																		(BgL_libsz00_1474))
																																		{	/* Eval/library.scm 372 */
																																			BgL_test2194z00_3878
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																	else
																																		{	/* Eval/library.scm 372 */
																																			BgL_test2194z00_3878
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																}
																															if (BgL_test2194z00_3878)
																																{	/* Eval/library.scm 374 */
																																	obj_t
																																		BgL_arg1387z00_1484;
																																	{	/* Eval/library.scm 374 */
																																		obj_t
																																			BgL_list1388z00_1485;
																																		{	/* Eval/library.scm 374 */
																																			obj_t
																																				BgL_arg1389z00_1486;
																																			BgL_arg1389z00_1486
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_nsz00_1471,
																																				BNIL);
																																			BgL_list1388z00_1485
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_libz00_81,
																																				BgL_arg1389z00_1486);
																																		}
																																		BgL_arg1387z00_1484
																																			=
																																			BGl_formatz00zz__r4_output_6_10_3z00
																																			(BGl_string1948z00zz__libraryz00,
																																			BgL_list1388z00_1485);
																																	}
																																	BGl_errorz00zz__errorz00
																																		(BGl_symbol1949z00zz__libraryz00,
																																		BgL_arg1387z00_1484,
																																		BgL_pathz00_1466);
																																}
																															else
																																{	/* Eval/library.scm 372 */
																																	if (STRINGP
																																		(BgL_libez00_1475))
																																		{	/* Eval/library.scm 376 */
																																			if (STRINGP(BgL_libsz00_1474))
																																				{	/* Eval/library.scm 389 */
																																					BGl_dynamiczd2loadzd2zz__osz00
																																						(BgL_libsz00_1474,
																																						BgL_init_sz00_1477,
																																						BgL_module_sz00_1479);
																																				}
																																			else
																																				{	/* Eval/library.scm 389 */
																																					BGl_dynamiczd2loadzd2zz__osz00
																																						(BgL_rscz00_1473,
																																						BgL_init_sz00_1477,
																																						BgL_module_sz00_1479);
																																				}
																																			BGl_dynamiczd2loadzd2zz__osz00
																																				(BgL_libez00_1475,
																																				BgL_init_ez00_1478,
																																				BgL_module_ez00_1480);
																																		}
																																	else
																																		{	/* Eval/library.scm 376 */
																																			{	/* Eval/library.scm 382 */
																																				obj_t
																																					BgL_arg1392z00_1489;
																																				{	/* Eval/library.scm 382 */
																																					obj_t
																																						BgL_list1396z00_1493;
																																					{	/* Eval/library.scm 382 */
																																						obj_t
																																							BgL_arg1397z00_1494;
																																						BgL_arg1397z00_1494
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_nez00_1472,
																																							BNIL);
																																						BgL_list1396z00_1493
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_libz00_81,
																																							BgL_arg1397z00_1494);
																																					}
																																					BgL_arg1392z00_1489
																																						=
																																						BGl_formatz00zz__r4_output_6_10_3z00
																																						(BGl_string1951z00zz__libraryz00,
																																						BgL_list1396z00_1493);
																																				}
																																				{	/* Eval/library.scm 379 */
																																					obj_t
																																						BgL_list1393z00_1490;
																																					{	/* Eval/library.scm 379 */
																																						obj_t
																																							BgL_arg1394z00_1491;
																																						{	/* Eval/library.scm 379 */
																																							obj_t
																																								BgL_arg1395z00_1492;
																																							BgL_arg1395z00_1492
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_pathz00_1466,
																																								BNIL);
																																							BgL_arg1394z00_1491
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1392z00_1489,
																																								BgL_arg1395z00_1492);
																																						}
																																						BgL_list1393z00_1490
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_string1950z00zz__libraryz00,
																																							BgL_arg1394z00_1491);
																																					}
																																					BGl_evwarningz00zz__everrorz00
																																						(BFALSE,
																																						BgL_list1393z00_1490);
																																				}
																																			}
																																			if (STRINGP(BgL_libsz00_1474))
																																				{	/* Eval/library.scm 385 */
																																					BGl_dynamiczd2loadzd2zz__osz00
																																						(BgL_libsz00_1474,
																																						BgL_init_sz00_1477,
																																						BgL_module_sz00_1479);
																																				}
																																			else
																																				{	/* Eval/library.scm 385 */
																																					BGl_dynamiczd2loadzd2zz__osz00
																																						(BgL_rscz00_1473,
																																						BgL_init_sz00_1477,
																																						BgL_module_sz00_1479);
																																				}
																																		}
																																}
																														}
																														{	/* Eval/library.scm 393 */
																															bool_t
																																BgL_test2200z00_3905;
																															if (CBOOL
																																(BgL_infoz00_1469))
																																{	/* Eval/library.scm 393 */
																																	BgL_test2200z00_3905
																																		=
																																		CBOOL
																																		(STRUCT_REF(
																																			((obj_t)
																																				BgL_infoz00_1469),
																																			(int)
																																			(9L)));
																																}
																															else
																																{	/* Eval/library.scm 393 */
																																	BgL_test2200z00_3905
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test2200z00_3905)
																																{	/* Eval/library.scm 394 */
																																	obj_t
																																		BgL_g1400z00_1499;
																																	BgL_g1400z00_1499
																																		=
																																		BGl_list1952z00zz__libraryz00;
																																	{	/* Eval/eval.scm 81 */
																																		obj_t
																																			BgL_envz00_1500;
																																		BgL_envz00_1500
																																			=
																																			BGl_defaultzd2environmentzd2zz__evalz00
																																			();
																																		{	/* Eval/eval.scm 81 */

																																			BGl_evalz00zz__evalz00
																																				(BgL_g1400z00_1499,
																																				BgL_envz00_1500);
																																		}
																																	}
																																}
																															else
																																{	/* Eval/library.scm 393 */
																																	BFALSE;
																																}
																														}
																														{	/* Eval/library.scm 395 */
																															bool_t
																																BgL_test2202z00_3914;
																															if (CBOOL
																																(BgL_infoz00_1469))
																																{	/* Eval/library.scm 395 */
																																	BgL_test2202z00_3914
																																		=
																																		CBOOL
																																		(STRUCT_REF(
																																			((obj_t)
																																				BgL_infoz00_1469),
																																			(int)
																																			(10L)));
																																}
																															else
																																{	/* Eval/library.scm 395 */
																																	BgL_test2202z00_3914
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test2202z00_3914)
																																{	/* Eval/library.scm 396 */
																																	obj_t
																																		BgL_g1400z00_1502;
																																	BgL_g1400z00_1502
																																		=
																																		BGl_list1958z00zz__libraryz00;
																																	{	/* Eval/eval.scm 81 */
																																		obj_t
																																			BgL_envz00_1503;
																																		BgL_envz00_1503
																																			=
																																			BGl_defaultzd2environmentzd2zz__evalz00
																																			();
																																		{	/* Eval/eval.scm 81 */

																																			BGl_evalz00zz__evalz00
																																				(BgL_g1400z00_1502,
																																				BgL_envz00_1503);
																																		}
																																	}
																																}
																															else
																																{	/* Eval/library.scm 395 */
																																	BFALSE;
																																}
																														}
																														BgL_tmp1050z00_1456
																															=
																															BgL_infoz00_1469;
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
											}
										else
											{	/* Eval/library.scm 321 */
												BgL_tmp1050z00_1456 =
													BGl_bigloozd2typezd2errorz00zz__errorz00
													(BGl_symbol1949z00zz__libraryz00,
													BGl_string1962z00zz__libraryz00, BgL_libz00_81);
											}
									}
								{	/* Eval/library.scm 401 */
									bool_t BgL_test2204z00_3924;

									{	/* Eval/library.scm 401 */
										obj_t BgL_arg1782z00_2462;

										BgL_arg1782z00_2462 =
											BGL_EXITD_PROTECT(BgL_exitd1048z00_1454);
										BgL_test2204z00_3924 = PAIRP(BgL_arg1782z00_2462);
									}
									if (BgL_test2204z00_3924)
										{	/* Eval/library.scm 401 */
											obj_t BgL_arg1779z00_2463;

											{	/* Eval/library.scm 401 */
												obj_t BgL_arg1781z00_2464;

												BgL_arg1781z00_2464 =
													BGL_EXITD_PROTECT(BgL_exitd1048z00_1454);
												BgL_arg1779z00_2463 =
													CDR(((obj_t) BgL_arg1781z00_2464));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1048z00_1454,
												BgL_arg1779z00_2463);
											BUNSPEC;
										}
									else
										{	/* Eval/library.scm 401 */
											BFALSE;
										}
								}
								BGL_MODULE_SET(BgL_modz00_1452);
								return BgL_tmp1050z00_1456;
							}
						}
					}
				}
			}
		}

	}



/* &library-load */
	obj_t BGl_z62libraryzd2loadzb0zz__libraryz00(obj_t BgL_envz00_2754,
		obj_t BgL_libz00_2755, obj_t BgL_pathz00_2756)
	{
		{	/* Eval/library.scm 315 */
			return
				BGl_libraryzd2loadzd2zz__libraryz00(BgL_libz00_2755, BgL_pathz00_2756);
		}

	}



/* &<@anonymous:1377> */
	obj_t BGl_z62zc3z04anonymousza31377ze3ze5zz__libraryz00(obj_t BgL_envz00_2757)
	{
		{	/* Eval/library.scm 401 */
			{	/* Eval/library.scm 403 */
				obj_t BgL_tmpz00_3933;

				BgL_tmpz00_3933 = PROCEDURE_REF(BgL_envz00_2757, (int) (0L));
				return BGL_MODULE_SET(BgL_tmpz00_3933);
			}
		}

	}



/* library-load-init */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2loadzd2initz00zz__libraryz00(obj_t
		BgL_libz00_83, obj_t BgL_pathz00_84)
	{
		{	/* Eval/library.scm 413 */
			{	/* Eval/library.scm 414 */
				obj_t BgL_initz00_1523;

				{	/* Eval/library.scm 414 */
					obj_t BgL_arg1415z00_1526;

					{	/* Eval/library.scm 230 */
						obj_t BgL_arg1352z00_2467;

						{	/* Eval/library.scm 230 */
							obj_t BgL_arg1623z00_2469;

							BgL_arg1623z00_2469 = SYMBOL_TO_STRING(((obj_t) BgL_libz00_83));
							BgL_arg1352z00_2467 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1623z00_2469);
						}
						BgL_arg1415z00_1526 =
							string_append(BgL_arg1352z00_2467,
							BGl_string1922z00zz__libraryz00);
					}
					BgL_initz00_1523 =
						BGl_findzd2filezf2pathz20zz__osz00(BgL_arg1415z00_1526,
						BgL_pathz00_84);
				}
				if (CBOOL(BgL_initz00_1523))
					{	/* Eval/library.scm 416 */
						bool_t BgL_tozd2loadzd2_1524;

						BgL_tozd2loadzd2_1524 = ((bool_t) 0);
						{	/* Eval/library.scm 417 */
							obj_t BgL_top2207z00_3945;

							BgL_top2207z00_3945 = BGL_EXITD_TOP_AS_OBJ();
							BGL_MUTEX_LOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
							BGL_EXITD_PUSH_PROTECT(BgL_top2207z00_3945,
								BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
							BUNSPEC;
							{	/* Eval/library.scm 417 */
								obj_t BgL_tmp2206z00_3944;

								if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
										(BgL_initz00_1523,
											BGl_za2loadedzd2initzd2filesza2z00zz__libraryz00)))
									{	/* Eval/library.scm 418 */
										BgL_tmp2206z00_3944 = BFALSE;
									}
								else
									{	/* Eval/library.scm 418 */
										BgL_tozd2loadzd2_1524 = ((bool_t) 1);
										BgL_tmp2206z00_3944 =
											(BGl_za2loadedzd2initzd2filesza2z00zz__libraryz00 =
											MAKE_YOUNG_PAIR(BgL_initz00_1523,
												BGl_za2loadedzd2initzd2filesza2z00zz__libraryz00),
											BUNSPEC);
									}
								BGL_EXITD_POP_PROTECT(BgL_top2207z00_3945);
								BUNSPEC;
								BGL_MUTEX_UNLOCK(BGl_za2libraryzd2mutexza2zd2zz__libraryz00);
								BgL_tmp2206z00_3944;
							}
						}
						if (BgL_tozd2loadzd2_1524)
							{	/* Eval/library.scm 421 */
								return BGl_loadzd2initzd2zz__libraryz00(BgL_initz00_1523);
							}
						else
							{	/* Eval/library.scm 421 */
								return BFALSE;
							}
					}
				else
					{	/* Eval/library.scm 415 */
						return BFALSE;
					}
			}
		}

	}



/* &library-load-init */
	obj_t BGl_z62libraryzd2loadzd2initz62zz__libraryz00(obj_t BgL_envz00_2759,
		obj_t BgL_libz00_2760, obj_t BgL_pathz00_2761)
	{
		{	/* Eval/library.scm 413 */
			return
				BGl_libraryzd2loadzd2initz00zz__libraryz00(BgL_libz00_2760,
				BgL_pathz00_2761);
		}

	}



/* library-load_e */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2load_ezd2zz__libraryz00(obj_t
		BgL_libz00_85, obj_t BgL_pathz00_86)
	{
		{	/* Eval/library.scm 427 */
			{	/* Eval/library.scm 428 */
				obj_t BgL_modz00_1527;

				BgL_modz00_1527 = BGl_evalzd2modulezd2zz__evmodulez00();
				{	/* Eval/library.scm 429 */
					obj_t BgL_tmpz00_3959;

					BgL_tmpz00_3959 = BGl_interactionzd2environmentzd2zz__evalz00();
					BGL_MODULE_SET(BgL_tmpz00_3959);
				}
				{	/* Eval/library.scm 430 */
					obj_t BgL_exitd1052z00_1529;

					BgL_exitd1052z00_1529 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Eval/library.scm 496 */
						obj_t BgL_zc3z04anonymousza31444ze3z87_2762;

						BgL_zc3z04anonymousza31444ze3z87_2762 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31444ze3ze5zz__libraryz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31444ze3z87_2762, (int) (0L),
							BgL_modz00_1527);
						{	/* Eval/library.scm 430 */
							obj_t BgL_arg1783z00_2470;

							{	/* Eval/library.scm 430 */
								obj_t BgL_arg1785z00_2471;

								BgL_arg1785z00_2471 = BGL_EXITD_PROTECT(BgL_exitd1052z00_1529);
								BgL_arg1783z00_2470 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31444ze3z87_2762,
									BgL_arg1785z00_2471);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1052z00_1529, BgL_arg1783z00_2470);
							BUNSPEC;
						}
						{	/* Eval/library.scm 432 */
							obj_t BgL_tmp1054z00_1531;

							if (STRINGP(BgL_libz00_85))
								{	/* Llib/os.scm 286 */

									BgL_tmp1054z00_1531 =
										BGl_dynamiczd2loadzd2zz__osz00(BgL_libz00_85,
										string_to_bstring(BGL_DYNAMIC_LOAD_INIT), BFALSE);
								}
							else
								{	/* Eval/library.scm 432 */
									if (SYMBOLP(BgL_libz00_85))
										{	/* Eval/library.scm 437 */
											obj_t BgL_pathz00_1537;

											if (PAIRP(BgL_pathz00_86))
												{	/* Eval/library.scm 437 */
													BgL_pathz00_1537 = BgL_pathz00_86;
												}
											else
												{	/* Eval/library.scm 439 */
													obj_t BgL_venvz00_1578;

													BgL_venvz00_1578 =
														BGl_getenvz00zz__osz00
														(BGl_string1944z00zz__libraryz00);
													if (CBOOL(BgL_venvz00_1578))
														{	/* Eval/library.scm 440 */
															BgL_pathz00_1537 =
																MAKE_YOUNG_PAIR(BGl_string1945z00zz__libraryz00,
																BGl_unixzd2pathzd2ze3listze3zz__osz00
																(BgL_venvz00_1578));
														}
													else
														{	/* Eval/library.scm 440 */
															BgL_pathz00_1537 =
																BGl_bigloozd2libraryzd2pathz00zz__paramz00();
														}
												}
											{	/* Eval/library.scm 437 */
												obj_t BgL_bez00_1538;

												BgL_bez00_1538 = BGl_symbol1924z00zz__libraryz00;
												{	/* Eval/library.scm 443 */

													BGl_libraryzd2loadzd2initz00zz__libraryz00
														(BgL_libz00_85, BgL_pathz00_1537);
													{	/* Eval/library.scm 447 */
														obj_t BgL_infoz00_1539;

														{	/* Eval/library.scm 175 */
															obj_t BgL_g1040z00_2473;

															BgL_g1040z00_2473 =
																BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																(BgL_libz00_85,
																BGl_za2librariesza2z00zz__libraryz00);
															if (CBOOL(BgL_g1040z00_2473))
																{	/* Eval/library.scm 175 */
																	BgL_infoz00_1539 =
																		CDR(((obj_t) BgL_g1040z00_2473));
																}
															else
																{	/* Eval/library.scm 175 */
																	BgL_infoz00_1539 = BFALSE;
																}
														}
														{	/* Eval/library.scm 447 */
															obj_t BgL_nz00_1540;

															BgL_nz00_1540 =
																BGl_makezd2sharedzd2libzd2namezd2zz__osz00
																(BGl_libraryzd2filezd2namez00zz__libraryz00
																(BgL_libz00_85, BGl_string1863z00zz__libraryz00,
																	BgL_bez00_1538), BgL_bez00_1538);
															{	/* Eval/library.scm 448 */
																obj_t BgL_nsz00_1541;

																BgL_nsz00_1541 =
																	BGl_makezd2sharedzd2libzd2namezd2zz__osz00
																	(BGl_libraryzd2filezd2namez00zz__libraryz00
																	(BgL_libz00_85,
																		string_append
																		(BGl_string1919z00zz__libraryz00,
																			BGl_evalzd2libraryzd2suffixz00zz__libraryz00
																			()), BgL_bez00_1538), BgL_bez00_1538);
																{	/* Eval/library.scm 451 */
																	obj_t BgL_nez00_1542;

																	BgL_nez00_1542 =
																		BGl_makezd2sharedzd2libzd2namezd2zz__osz00
																		(BGl_libraryzd2filezd2namez00zz__libraryz00
																		(BgL_libz00_85,
																			string_append
																			(BGl_string1920z00zz__libraryz00,
																				BGl_evalzd2libraryzd2suffixz00zz__libraryz00
																				()), BgL_bez00_1538), BgL_bez00_1538);
																	{	/* Eval/library.scm 455 */
																		obj_t BgL_rscz00_1543;

																		{	/* Eval/library.scm 459 */
																			obj_t BgL_pz00_1567;

																			{	/* Eval/library.scm 460 */
																				obj_t BgL_arg1434z00_1569;

																				{	/* Eval/library.scm 460 */
																					obj_t BgL_arg1623z00_2477;

																					BgL_arg1623z00_2477 =
																						SYMBOL_TO_STRING(BgL_libz00_85);
																					BgL_arg1434z00_1569 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1623z00_2477);
																				}
																				BgL_pz00_1567 =
																					string_append_3
																					(BGl_string1946z00zz__libraryz00,
																					BgL_arg1434z00_1569,
																					BGl_string1947z00zz__libraryz00);
																			}
																			if (fexists(BSTRING_TO_STRING
																					(BgL_pz00_1567)))
																				{	/* Eval/library.scm 468 */
																					BgL_rscz00_1543 = BgL_pz00_1567;
																				}
																			else
																				{	/* Eval/library.scm 468 */
																					BgL_rscz00_1543 = BFALSE;
																				}
																		}
																		{	/* Eval/library.scm 459 */
																			obj_t BgL_libez00_1544;

																			BgL_libez00_1544 =
																				BGl_findzd2filezf2pathz20zz__osz00
																				(BgL_nez00_1542, BgL_pathz00_1537);
																			{	/* Eval/library.scm 469 */
																				obj_t BgL_namez00_1545;

																				{	/* Eval/library.scm 470 */
																					obj_t BgL_arg1623z00_2480;

																					BgL_arg1623z00_2480 =
																						SYMBOL_TO_STRING(BgL_libz00_85);
																					BgL_namez00_1545 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1623z00_2480);
																				}
																				{	/* Eval/library.scm 470 */
																					obj_t BgL_init_ez00_1546;

																					if (CBOOL(BgL_infoz00_1539))
																						{	/* Eval/library.scm 471 */
																							BgL_init_ez00_1546 =
																								STRUCT_REF(
																								((obj_t) BgL_infoz00_1539),
																								(int) (4L));
																						}
																					else
																						{	/* Eval/library.scm 471 */
																							BgL_init_ez00_1546 = BFALSE;
																						}
																					{	/* Eval/library.scm 471 */
																						obj_t BgL_module_ez00_1547;

																						if (CBOOL(BgL_infoz00_1539))
																							{	/* Eval/library.scm 472 */
																								BgL_module_ez00_1547 =
																									STRUCT_REF(
																									((obj_t) BgL_infoz00_1539),
																									(int) (6L));
																							}
																						else
																							{	/* Eval/library.scm 472 */
																								BgL_module_ez00_1547 = BFALSE;
																							}
																						{	/* Eval/library.scm 472 */

																							if (STRINGP(BgL_rscz00_1543))
																								{	/* Eval/library.scm 477 */
																									if (STRINGP(BgL_libez00_1544))
																										{	/* Eval/library.scm 481 */
																											BGl_dynamiczd2loadzd2zz__osz00
																												(BgL_libez00_1544,
																												BgL_init_ez00_1546,
																												BgL_module_ez00_1547);
																										}
																									else
																										{	/* Eval/library.scm 487 */
																											obj_t BgL_arg1421z00_1550;

																											{	/* Eval/library.scm 487 */
																												obj_t
																													BgL_list1425z00_1554;
																												{	/* Eval/library.scm 487 */
																													obj_t
																														BgL_arg1426z00_1555;
																													BgL_arg1426z00_1555 =
																														MAKE_YOUNG_PAIR
																														(BgL_nez00_1542,
																														BNIL);
																													BgL_list1425z00_1554 =
																														MAKE_YOUNG_PAIR
																														(BgL_libz00_85,
																														BgL_arg1426z00_1555);
																												}
																												BgL_arg1421z00_1550 =
																													BGl_formatz00zz__r4_output_6_10_3z00
																													(BGl_string1951z00zz__libraryz00,
																													BgL_list1425z00_1554);
																											}
																											{	/* Eval/library.scm 484 */
																												obj_t
																													BgL_list1422z00_1551;
																												{	/* Eval/library.scm 484 */
																													obj_t
																														BgL_arg1423z00_1552;
																													{	/* Eval/library.scm 484 */
																														obj_t
																															BgL_arg1424z00_1553;
																														BgL_arg1424z00_1553
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_pathz00_1537,
																															BNIL);
																														BgL_arg1423z00_1552
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1421z00_1550,
																															BgL_arg1424z00_1553);
																													}
																													BgL_list1422z00_1551 =
																														MAKE_YOUNG_PAIR
																														(BGl_string1950z00zz__libraryz00,
																														BgL_arg1423z00_1552);
																												}
																												BGl_evwarningz00zz__everrorz00
																													(BFALSE,
																													BgL_list1422z00_1551);
																											}
																										}
																								}
																							else
																								{	/* Eval/library.scm 479 */
																									obj_t BgL_arg1427z00_1556;

																									{	/* Eval/library.scm 479 */
																										obj_t BgL_list1428z00_1557;

																										{	/* Eval/library.scm 479 */
																											obj_t BgL_arg1429z00_1558;

																											BgL_arg1429z00_1558 =
																												MAKE_YOUNG_PAIR
																												(BgL_nsz00_1541, BNIL);
																											BgL_list1428z00_1557 =
																												MAKE_YOUNG_PAIR
																												(BgL_libz00_85,
																												BgL_arg1429z00_1558);
																										}
																										BgL_arg1427z00_1556 =
																											BGl_formatz00zz__r4_output_6_10_3z00
																											(BGl_string1948z00zz__libraryz00,
																											BgL_list1428z00_1557);
																									}
																									BGl_errorz00zz__errorz00
																										(BGl_symbol1949z00zz__libraryz00,
																										BgL_arg1427z00_1556,
																										BgL_pathz00_1537);
																								}
																							{	/* Eval/library.scm 492 */
																								bool_t BgL_test2220z00_4036;

																								if (CBOOL(BgL_infoz00_1539))
																									{	/* Eval/library.scm 492 */
																										BgL_test2220z00_4036 =
																											CBOOL(STRUCT_REF(
																												((obj_t)
																													BgL_infoz00_1539),
																												(int) (9L)));
																									}
																								else
																									{	/* Eval/library.scm 492 */
																										BgL_test2220z00_4036 =
																											((bool_t) 0);
																									}
																								if (BgL_test2220z00_4036)
																									{	/* Eval/library.scm 493 */
																										obj_t BgL_g1400z00_1560;

																										BgL_g1400z00_1560 =
																											BGl_list1952z00zz__libraryz00;
																										{	/* Eval/eval.scm 81 */
																											obj_t BgL_envz00_1561;

																											BgL_envz00_1561 =
																												BGl_defaultzd2environmentzd2zz__evalz00
																												();
																											{	/* Eval/eval.scm 81 */

																												BGl_evalz00zz__evalz00
																													(BgL_g1400z00_1560,
																													BgL_envz00_1561);
																											}
																										}
																									}
																								else
																									{	/* Eval/library.scm 492 */
																										BFALSE;
																									}
																							}
																							{	/* Eval/library.scm 494 */
																								bool_t BgL_test2222z00_4045;

																								if (CBOOL(BgL_infoz00_1539))
																									{	/* Eval/library.scm 494 */
																										BgL_test2222z00_4045 =
																											CBOOL(STRUCT_REF(
																												((obj_t)
																													BgL_infoz00_1539),
																												(int) (10L)));
																									}
																								else
																									{	/* Eval/library.scm 494 */
																										BgL_test2222z00_4045 =
																											((bool_t) 0);
																									}
																								if (BgL_test2222z00_4045)
																									{	/* Eval/library.scm 495 */
																										obj_t BgL_g1400z00_1563;

																										BgL_g1400z00_1563 =
																											BGl_list1958z00zz__libraryz00;
																										{	/* Eval/eval.scm 81 */
																											obj_t BgL_envz00_1564;

																											BgL_envz00_1564 =
																												BGl_defaultzd2environmentzd2zz__evalz00
																												();
																											{	/* Eval/eval.scm 81 */

																												BgL_tmp1054z00_1531 =
																													BGl_evalz00zz__evalz00
																													(BgL_g1400z00_1563,
																													BgL_envz00_1564);
																											}
																										}
																									}
																								else
																									{	/* Eval/library.scm 494 */
																										BgL_tmp1054z00_1531 =
																											BFALSE;
																									}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									else
										{	/* Eval/library.scm 434 */
											BgL_tmp1054z00_1531 =
												BGl_bigloozd2typezd2errorz00zz__errorz00
												(BGl_symbol1949z00zz__libraryz00,
												BGl_string1962z00zz__libraryz00, BgL_libz00_85);
										}
								}
							{	/* Eval/library.scm 430 */
								bool_t BgL_test2224z00_4055;

								{	/* Eval/library.scm 430 */
									obj_t BgL_arg1782z00_2486;

									BgL_arg1782z00_2486 =
										BGL_EXITD_PROTECT(BgL_exitd1052z00_1529);
									BgL_test2224z00_4055 = PAIRP(BgL_arg1782z00_2486);
								}
								if (BgL_test2224z00_4055)
									{	/* Eval/library.scm 430 */
										obj_t BgL_arg1779z00_2487;

										{	/* Eval/library.scm 430 */
											obj_t BgL_arg1781z00_2488;

											BgL_arg1781z00_2488 =
												BGL_EXITD_PROTECT(BgL_exitd1052z00_1529);
											BgL_arg1779z00_2487 = CDR(((obj_t) BgL_arg1781z00_2488));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1052z00_1529,
											BgL_arg1779z00_2487);
										BUNSPEC;
									}
								else
									{	/* Eval/library.scm 430 */
										BFALSE;
									}
							}
							BGL_MODULE_SET(BgL_modz00_1527);
							return BgL_tmp1054z00_1531;
						}
					}
				}
			}
		}

	}



/* &library-load_e */
	obj_t BGl_z62libraryzd2load_ezb0zz__libraryz00(obj_t BgL_envz00_2763,
		obj_t BgL_libz00_2764, obj_t BgL_pathz00_2765)
	{
		{	/* Eval/library.scm 427 */
			return
				BGl_libraryzd2load_ezd2zz__libraryz00(BgL_libz00_2764,
				BgL_pathz00_2765);
		}

	}



/* &<@anonymous:1444> */
	obj_t BGl_z62zc3z04anonymousza31444ze3ze5zz__libraryz00(obj_t BgL_envz00_2766)
	{
		{	/* Eval/library.scm 430 */
			{	/* Eval/library.scm 496 */
				obj_t BgL_tmpz00_4064;

				BgL_tmpz00_4064 = PROCEDURE_REF(BgL_envz00_2766, (int) (0L));
				return BGL_MODULE_SET(BgL_tmpz00_4064);
			}
		}

	}



/* library-exists? */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2existszf3z21zz__libraryz00(obj_t
		BgL_libz00_87, obj_t BgL_pathz00_88)
	{
		{	/* Eval/library.scm 501 */
			{	/* Eval/library.scm 502 */
				obj_t BgL_pathz00_1582;

				if (PAIRP(BgL_pathz00_88))
					{	/* Eval/library.scm 502 */
						BgL_pathz00_1582 = BgL_pathz00_88;
					}
				else
					{	/* Eval/library.scm 504 */
						obj_t BgL_venvz00_1591;

						BgL_venvz00_1591 =
							BGl_getenvz00zz__osz00(BGl_string1944z00zz__libraryz00);
						if (CBOOL(BgL_venvz00_1591))
							{	/* Eval/library.scm 505 */
								BgL_pathz00_1582 =
									MAKE_YOUNG_PAIR(BGl_string1945z00zz__libraryz00,
									BGl_unixzd2pathzd2ze3listze3zz__osz00(BgL_venvz00_1591));
							}
						else
							{	/* Eval/library.scm 505 */
								BgL_pathz00_1582 = BGl_bigloozd2libraryzd2pathz00zz__paramz00();
							}
					}
				{	/* Eval/library.scm 508 */
					obj_t BgL_heapz00_1584;

					{	/* Eval/library.scm 511 */
						obj_t BgL_arg1447z00_1589;

						{	/* Eval/library.scm 511 */
							obj_t BgL_arg1623z00_2491;

							BgL_arg1623z00_2491 = SYMBOL_TO_STRING(BgL_libz00_87);
							BgL_arg1447z00_1589 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1623z00_2491);
						}
						BgL_heapz00_1584 =
							string_append(BgL_arg1447z00_1589,
							BGl_string1963z00zz__libraryz00);
					}
					{	/* Eval/library.scm 511 */
						obj_t BgL_initz00_1585;

						{	/* Eval/library.scm 512 */
							obj_t BgL_arg1446z00_1588;

							{	/* Eval/library.scm 512 */
								obj_t BgL_arg1623z00_2493;

								BgL_arg1623z00_2493 = SYMBOL_TO_STRING(BgL_libz00_87);
								BgL_arg1446z00_1588 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1623z00_2493);
							}
							BgL_initz00_1585 =
								string_append(BgL_arg1446z00_1588,
								BGl_string1922z00zz__libraryz00);
						}
						{	/* Eval/library.scm 512 */

							{	/* Eval/library.scm 513 */
								obj_t BgL_arg1445z00_1586;

								{	/* Eval/library.scm 513 */
									obj_t BgL__ortest_1059z00_1587;

									BgL__ortest_1059z00_1587 =
										BGl_findzd2filezf2pathz20zz__osz00(BgL_heapz00_1584,
										BgL_pathz00_1582);
									if (CBOOL(BgL__ortest_1059z00_1587))
										{	/* Eval/library.scm 513 */
											BgL_arg1445z00_1586 = BgL__ortest_1059z00_1587;
										}
									else
										{	/* Eval/library.scm 513 */
											BgL_arg1445z00_1586 =
												BGl_findzd2filezf2pathz20zz__osz00(BgL_initz00_1585,
												BgL_pathz00_1582);
										}
								}
								return BBOOL(STRINGP(BgL_arg1445z00_1586));
							}
						}
					}
				}
			}
		}

	}



/* &library-exists? */
	obj_t BGl_z62libraryzd2existszf3z43zz__libraryz00(obj_t BgL_envz00_2768,
		obj_t BgL_libz00_2769, obj_t BgL_pathz00_2770)
	{
		{	/* Eval/library.scm 501 */
			{	/* Eval/library.scm 502 */
				obj_t BgL_auxz00_4088;

				if (SYMBOLP(BgL_libz00_2769))
					{	/* Eval/library.scm 502 */
						BgL_auxz00_4088 = BgL_libz00_2769;
					}
				else
					{
						obj_t BgL_auxz00_4091;

						BgL_auxz00_4091 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1864z00zz__libraryz00,
							BINT(18154L), BGl_string1964z00zz__libraryz00,
							BGl_string1880z00zz__libraryz00, BgL_libz00_2769);
						FAILURE(BgL_auxz00_4091, BFALSE, BFALSE);
					}
				return
					BGl_libraryzd2existszf3z21zz__libraryz00(BgL_auxz00_4088,
					BgL_pathz00_2770);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__libraryz00(void)
	{
		{	/* Eval/library.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__configurez00(35034923L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__evalz00(35539458L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
			return
				BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(498124579L,
				BSTRING_TO_STRING(BGl_string1965z00zz__libraryz00));
		}

	}

#ifdef __cplusplus
}
#endif
