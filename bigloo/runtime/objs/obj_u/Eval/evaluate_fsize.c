/*===========================================================================*/
/*   (Eval/evaluate_fsize.scm)                                               */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evaluate_fsize.scm -indent -o objs/obj_u/Eval/evaluate_fsize.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVALUATE_FSIZE_TYPE_DEFINITIONS
#define BGL___EVALUATE_FSIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ev_exprz00_bgl
	{
		header_t header;
		obj_t widening;
	}                 *BgL_ev_exprz00_bglt;

	typedef struct BgL_ev_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_effz00;
		obj_t BgL_typez00;
	}                *BgL_ev_varz00_bglt;

	typedef struct BgL_ev_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                   *BgL_ev_globalz00_bglt;

	typedef struct BgL_ev_littz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_valuez00;
	}                 *BgL_ev_littz00_bglt;

	typedef struct BgL_ev_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_pz00;
		struct BgL_ev_exprz00_bgl *BgL_tz00;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}               *BgL_ev_ifz00_bglt;

	typedef struct BgL_ev_listz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                 *BgL_ev_listz00_bglt;

	typedef struct BgL_ev_prog2z00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_e1z00;
		struct BgL_ev_exprz00_bgl *BgL_e2z00;
	}                  *BgL_ev_prog2z00_bglt;

	typedef struct BgL_ev_hookz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_hookz00_bglt;

	typedef struct BgL_ev_setlocalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_varz00_bgl *BgL_vz00;
	}                     *BgL_ev_setlocalz00_bglt;

	typedef struct BgL_ev_bindzd2exitzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_varz00_bgl *BgL_varz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                        *BgL_ev_bindzd2exitzd2_bglt;

	typedef struct BgL_ev_unwindzd2protectzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                             *BgL_ev_unwindzd2protectzd2_bglt;

	typedef struct BgL_ev_withzd2handlerzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_handlerz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                           *BgL_ev_withzd2handlerzd2_bglt;

	typedef struct BgL_ev_synchroniza7eza7_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_mutexz00;
		struct BgL_ev_exprz00_bgl *BgL_prelockz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                          *BgL_ev_synchroniza7eza7_bglt;

	typedef struct BgL_ev_binderz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_binderz00_bglt;

	typedef struct BgL_ev_letz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_letz00_bglt;

	typedef struct BgL_ev_letza2za2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_letza2za2_bglt;

	typedef struct BgL_ev_letrecz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_letrecz00_bglt;

	typedef struct BgL_ev_labelsz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		obj_t BgL_envz00;
		obj_t BgL_stkz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_labelsz00_bglt;

	typedef struct BgL_ev_gotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_varz00_bgl *BgL_labelz00;
		struct BgL_ev_labelsz00_bgl *BgL_labelsz00;
		obj_t BgL_argsz00;
	}                 *BgL_ev_gotoz00_bglt;

	typedef struct BgL_ev_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_tailzf3zf3;
	}                *BgL_ev_appz00_bglt;

	typedef struct BgL_ev_absz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_wherez00;
		obj_t BgL_arityz00;
		obj_t BgL_varsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		int BgL_siza7eza7;
		obj_t BgL_bindz00;
		obj_t BgL_freez00;
		obj_t BgL_innerz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_absz00_bglt;


#endif													// BGL___EVALUATE_FSIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62searchzd2letreczb0zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_bin1346z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_let1348z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62hasvarzf3zd2ev_var1441z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evaluate_fsiza7eza7 =
		BUNSPEC;
	static obj_t BGl_z62tailposzd2ev_letza21426z12zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_ev_exprz00_bglt
		BGl_extractzd2loopszd2zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62hasvarzf3zd2ev_list1449z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62subst_gotozd2ev_goto1386zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static bool_t BGl_letrectailzf3zf3zz__evaluate_fsiza7eza7(obj_t, obj_t,
		BgL_ev_exprz00_bglt);
	extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_letrecz00zz__evaluate_typesz00;
	static obj_t BGl_toplevelzd2initzd2zz__evaluate_fsiza7eza7(void);
	static obj_t BGl_z62hasvarzf3zd2ev_global1443z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_let1308z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_letz00zz__evaluate_typesz00;
	static obj_t BGl_z62hasvarzf3zd2ev_prog21452z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_pro1334z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62searchzd2letrec1321zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_global1288z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62hasvarzf3zd2ev_labels1468z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_let1423zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62subst_gotozd2ev_withzd2h1377z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_if1401zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__evaluate_fsiza7eza7(void);
	static obj_t
		BGl_z62searchzd2letreczd2ev_hoo1336z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	extern obj_t BGl_ev_prog2z00zz__evaluate_typesz00;
	BGL_EXPORTED_DECL int
		BGl_framezd2siza7ez75zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt);
	static obj_t BGl_objectzd2initzd2zz__evaluate_fsiza7eza7(void);
	static obj_t
		BGl_z62searchzd2letreczd2ev_app1350z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	extern obj_t BGl_ev_bindzd2exitzd2zz__evaluate_typesz00;
	static obj_t BGl_z62fsiza7ezd2ev_letza21310zb5zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposz62zz__evaluate_fsiza7eza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62subst_gotozd2ev_var1356zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t
		BGl_z62fsiza7ezd2ev_unwindzd2prot1302zc5zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62hasvarzf3zd2ev_litt1445z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62hasvarzf3zd2ev_unwindzd2pr1460z91zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62hasvarzf3z91zz__evaluate_fsiza7eza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_letrec1312z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62subst_gotozd2ev_binder1382zb0zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62framezd2siza7ez17zz__evaluate_fsiza7eza7(obj_t, obj_t);
	extern obj_t BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00;
	static obj_t BGl_z62hasvarzf3zd2ev_app1472z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_prog21296z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_if1292z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_litt1399zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t
		BGl_z62hasvarzf3zd2ev_bindzd2exit1458z91zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_letza2za2zz__evaluate_typesz00;
	extern obj_t BGl_ev_labelsz00zz__evaluate_typesz00;
	static obj_t BGl_z62fsiza7ezd2ev_list1294z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_app1318z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62hasvarzf3zd2ev_synchroni1464z43zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__evaluate_fsiza7eza7(void);
	static obj_t
		BGl_z62hasvarzf3zd2ev_withzd2hand1462z91zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_var1394zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_var1286z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_absz00zz__evaluate_typesz00;
	static obj_t BGl_z62tailposzd2ev_app1434zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62subst_gotozd2ev_synchr1380zb0zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt);
	static obj_t
		BGl_z62searchzd2letreczd2ev_abs1352z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_hook1408zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62subst_gotozd2ev_unwind1374zb0zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_ifz00zz__evaluate_typesz00;
	static BgL_ev_labelsz00_bglt
		BGl_modifyzd2letreczd2zz__evaluate_fsiza7eza7(obj_t, obj_t,
		BgL_ev_exprz00_bglt);
	static obj_t BGl_z62hasvarzf3zd2ev_abs1474z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62subst_gotozd2ev_list1364zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	static int BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt, int);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62subst_gotozd2ev_app1388zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_ev_hookz00zz__evaluate_typesz00;
	static obj_t BGl_z62fsiza7ezd2ev_litt1290z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_glo1326z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_lis1332z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_unw1340z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_labels1430zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_setlocal1412zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_abs1320z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_abs1436zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_gotoz00zz__evaluate_typesz00;
	static obj_t BGl_z62tailposzd2ev_goto1432zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62hasvarzf3zd2ev_binder1466z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_fsiza7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_uncompz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	static obj_t
		BGl_z62searchzd2letreczd2ev_lit1328z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezc5zz__evaluate_fsiza7eza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_ev_globalz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_binderz00zz__evaluate_typesz00;
	static obj_t BGl_z62hasvarzf3zd2ev_hook1454z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_listz00zz__evaluate_typesz00;
	static obj_t BGl_z62subst_gotozd2ev_litt1360zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	static bool_t BGl_searchzd2letrecza2z70zz__evaluate_fsiza7eza7(obj_t);
	extern obj_t BGl_ev_littz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_varz00zz__evaluate_typesz00;
	static obj_t BGl_z62subst_gotozd2ev_prog21366zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evaluate_fsiza7eza7(void);
	static obj_t BGl_z62subst_gotozd2ev_abs1390zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_fsiza7eza7(void);
	static obj_t BGl_z62tailposzd2ev_global1396zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__evaluate_fsiza7eza7(void);
	static obj_t BGl_z62fsiza7ezd2ev_goto1316z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62hasvarzf3zd2ev_setlocal1456z43zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_ev_exprz00_bglt
		BGl_z62extractzd2loopszb0zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt,
		BgL_ev_varz00_bglt);
	static obj_t BGl_z62tailposzd2ev_prog21406zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_withzd2handlerzd2zz__evaluate_typesz00;
	static obj_t
		BGl_z62searchzd2letreczd2ev_wit1342z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62fsiza7ezd2ev_synchroniza7e1306zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_syn1344z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62hasvarzf3zd2ev_goto1470z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt,
		BgL_ev_varz00_bglt);
	static obj_t BGl_z62subst_gotoz62zz__evaluate_fsiza7eza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62fsiza7e1283zc5zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62hasvarzf31438z91zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62subst_gotozd2ev_bindzd2e1371z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_letrec1428zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62subst_gotozd2ev_global1358zb0zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62tailposzd2ev_unwindzd2pr1416z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_exprz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_appz00zz__evaluate_typesz00;
	static obj_t BGl_z62fsiza7ezd2ev_hook1298z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62fsiza7ezd2ev_bindzd2exit1300zc5zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2270z00zz__evaluate_fsiza7eza7 = BUNSPEC;
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2272z00zz__evaluate_fsiza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2275z00zz__evaluate_fsiza7eza7 = BUNSPEC;
	static obj_t BGl_symbol2277z00zz__evaluate_fsiza7eza7 = BUNSPEC;
	static obj_t BGl_z62hasvarzf3zd2ev_if1447z43zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailpos1391z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_synchroniza7eza7zz__evaluate_typesz00;
	static obj_t
		BGl_z62subst_gotozd2ev_labels1384zb0zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_if1330z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62tailposzd2ev_bindzd2exit1414z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62tailposzd2ev_withzd2hand1418z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62subst_goto1353z62zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tailposzd2ev_list1403zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62tailposzd2ev_synchroni1420zb0zz__evaluate_fsiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62subst_gotozd2ev_if1362zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_ev_setlocalz00zz__evaluate_typesz00;
	static obj_t BGl_z62subst_gotozd2ev_hook1368zb0zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_var1324z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t BGl_z62fsiza7ezd2ev_labels1314z17zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62fsiza7ezd2ev_withzd2handle1304zc5zz__evaluate_fsiza7eza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62searchzd2letreczd2ev_bin1338z62zz__evaluate_fsiza7eza7(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2300z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2376za7,
		BGl_z62fsiza7ezd2ev_let1308z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2377za7,
		BGl_z62fsiza7ezd2ev_letza21310zb5zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2378za7,
		BGl_z62fsiza7ezd2ev_letrec1312z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2303z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2379za7,
		BGl_z62fsiza7ezd2ev_labels1314z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2304z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2380za7,
		BGl_z62fsiza7ezd2ev_goto1316z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2305z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2381za7,
		BGl_z62fsiza7ezd2ev_app1318z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2306z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2382za7,
		BGl_z62fsiza7ezd2ev_abs1320z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2307z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2383z00,
		BGl_z62searchzd2letreczd2ev_var1324z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2308z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2384z00,
		BGl_z62searchzd2letreczd2ev_glo1326z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2385z00,
		BGl_z62searchzd2letreczd2ev_lit1328z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2310z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2386z00,
		BGl_z62searchzd2letreczd2ev_if1330z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2311z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2387z00,
		BGl_z62searchzd2letreczd2ev_lis1332z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2388z00,
		BGl_z62searchzd2letreczd2ev_pro1334z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2389z00,
		BGl_z62searchzd2letreczd2ev_hoo1336z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2390z00,
		BGl_z62searchzd2letreczd2ev_bin1338z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2391z00,
		BGl_z62searchzd2letreczd2ev_unw1340z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2316z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2392z00,
		BGl_z62searchzd2letreczd2ev_wit1342z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2393z00,
		BGl_z62searchzd2letreczd2ev_syn1344z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2394z00,
		BGl_z62searchzd2letreczd2ev_bin1346z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2395z00,
		BGl_z62searchzd2letreczd2ev_let1348z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2320z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2396z00,
		BGl_z62searchzd2letreczd2ev_app1350z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2397z00,
		BGl_z62searchzd2letreczd2ev_abs1352z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22398z00,
		BGl_z62subst_gotozd2ev_var1356zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2323z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22399z00,
		BGl_z62subst_gotozd2ev_global1358zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22400z00,
		BGl_z62subst_gotozd2ev_litt1360zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2325z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22401z00,
		BGl_z62subst_gotozd2ev_if1362zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22402z00,
		BGl_z62subst_gotozd2ev_list1364zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22403z00,
		BGl_z62subst_gotozd2ev_prog21366zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22404z00,
		BGl_z62subst_gotozd2ev_hook1368zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2329z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22405z00,
		BGl_z62subst_gotozd2ev_bindzd2e1371z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string2256z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2256za700za7za7_2406za7,
		"/tmp/bigloo/runtime/Eval/evaluate_fsize.scm", 43);
	      DEFINE_STRING(BGl_string2257z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2257za700za7za7_2407za7, "&frame-size", 11);
	      DEFINE_STRING(BGl_string2258z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2258za700za7za7_2408za7, "ev_expr", 7);
	      DEFINE_STRING(BGl_string2259z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2259za700za7za7_2409za7, "&extract-loops", 14);
	      DEFINE_STATIC_BGL_GENERIC(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza762za7za7_2410z00,
		BGl_z62tailposz62zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22411z00,
		BGl_z62subst_gotozd2ev_unwind1374zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2331z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22412z00,
		BGl_z62subst_gotozd2ev_withzd2h1377z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2332z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22413z00,
		BGl_z62subst_gotozd2ev_synchr1380zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22414z00,
		BGl_z62subst_gotozd2ev_binder1382zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2334z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22415z00,
		BGl_z62subst_gotozd2ev_labels1384zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2335z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22416z00,
		BGl_z62subst_gotozd2ev_goto1386zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2336z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22417z00,
		BGl_z62subst_gotozd2ev_app1388zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2261z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2261za700za7za7_2418za7, "fsize1283", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2337z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7d22419z00,
		BGl_z62subst_gotozd2ev_abs1390zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2338z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2420z00,
		BGl_z62tailposzd2ev_var1394zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2263z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2263za700za7za7_2421za7, "search-letrec1321", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2422z00,
		BGl_z62tailposzd2ev_global1396zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2265z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2265za700za7za7_2423za7, "subst_goto1353", 14);
	      DEFINE_STRING(BGl_string2267z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2267za700za7za7_2424za7, "tailpos1391", 11);
	      DEFINE_STRING(BGl_string2269z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2269za700za7za7_2425za7, "hasvar?1438", 11);
	      DEFINE_STATIC_BGL_GENERIC(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za791za72426z00,
		BGl_z62hasvarzf3z91zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2340z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2427z00,
		BGl_z62tailposzd2ev_litt1399zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2341z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2428z00,
		BGl_z62tailposzd2ev_if1401zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2260z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7e1283za7c2429za7,
		BGl_z62fsiza7e1283zc5zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2430z00,
		BGl_z62tailposzd2ev_list1403zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2343z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2431z00,
		BGl_z62tailposzd2ev_prog21406zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2262z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2432z00,
		BGl_z62searchzd2letrec1321zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2344z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2433z00,
		BGl_z62tailposzd2ev_hook1408zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2434z00,
		BGl_z62tailposzd2ev_setlocal1412zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2264z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_goto1352435za7,
		BGl_z62subst_goto1353z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2346z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2436z00,
		BGl_z62tailposzd2ev_bindzd2exit1414z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2271z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2271za700za7za7_2437za7, "No method for this object", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2347z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2438z00,
		BGl_z62tailposzd2ev_unwindzd2pr1416z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2266z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailpos1391za762439z00,
		BGl_z62tailpos1391z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2348z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2440z00,
		BGl_z62tailposzd2ev_withzd2hand1418z62zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2273z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2273za700za7za7_2441za7, "tailpos", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2349z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2442z00,
		BGl_z62tailposzd2ev_synchroni1420zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2274z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2274za700za7za7_2443za7, "not defined for", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2268z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f314382444z00,
		BGl_z62hasvarzf31438z91zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2276z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2276za700za7za7_2445za7, "subst_goto", 10);
	      DEFINE_STRING(BGl_string2358z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2358za700za7za7_2446za7, "hasvar?", 7);
	      DEFINE_STRING(BGl_string2278z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2278za700za7za7_2447za7, "search-letrec", 13);
	      DEFINE_STRING(BGl_string2279z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2279za700za7za7_2448za7, "eval", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2350z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2449z00,
		BGl_z62tailposzd2ev_let1423zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2450z00,
		BGl_z62tailposzd2ev_letza21426z12zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2352z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2451z00,
		BGl_z62tailposzd2ev_letrec1428zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2353z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2452z00,
		BGl_z62tailposzd2ev_labels1430zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2354z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2453z00,
		BGl_z62tailposzd2ev_goto1432zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2355z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2454z00,
		BGl_z62tailposzd2ev_app1434zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2280z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2280za700za7za7_2455za7, "internal error: not defined for",
		31);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2356z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762tailposza7d2ev_2456z00,
		BGl_z62tailposzd2ev_abs1436zb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2281z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2281za700za7za7_2457za7, "&fsize", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2357z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2458za7,
		BGl_z62hasvarzf3zd2ev_var1441z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2282z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2282za700za7za7_2459za7, "bint", 4);
	      DEFINE_STRING(BGl_string2283z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2283za700za7za7_2460za7, "&search-letrec", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2359z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2461za7,
		BGl_z62hasvarzf3zd2ev_global1443z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2284z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2284za700za7za7_2462za7, "&subst_goto", 11);
	      DEFINE_STRING(BGl_string2285z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2285za700za7za7_2463za7, "&tailpos", 8);
	      DEFINE_STRING(BGl_string2286z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2286za700za7za7_2464za7, "ev_var", 6);
	      DEFINE_STRING(BGl_string2287z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2287za700za7za7_2465za7, "&hasvar?", 8);
	      DEFINE_STRING(BGl_string2289z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2289za700za7za7_2466za7, "fsize::int", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2360z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2467za7,
		BGl_z62hasvarzf3zd2ev_litt1445z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2361z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2468za7,
		BGl_z62hasvarzf3zd2ev_if1447z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2362z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2469za7,
		BGl_z62hasvarzf3zd2ev_list1449z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2363z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2470za7,
		BGl_z62hasvarzf3zd2ev_prog21452z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2364z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2471za7,
		BGl_z62hasvarzf3zd2ev_hook1454z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2365z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2472za7,
		BGl_z62hasvarzf3zd2ev_setlocal1456z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2366z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2473za7,
		BGl_z62hasvarzf3zd2ev_bindzd2exit1458z91zz__evaluate_fsiza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2367z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2474za7,
		BGl_z62hasvarzf3zd2ev_unwindzd2pr1460z91zz__evaluate_fsiza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2368z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2475za7,
		BGl_z62hasvarzf3zd2ev_withzd2hand1462z91zz__evaluate_fsiza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7,
		BgL_bgl_string2375za700za7za7_2476za7, "__evaluate_fsize", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2369z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2477za7,
		BGl_z62hasvarzf3zd2ev_synchroni1464z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2288z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2478za7,
		BGl_z62fsiza7ezd2ev_var1286z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2370z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2479za7,
		BGl_z62hasvarzf3zd2ev_binder1466z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2371z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2480za7,
		BGl_z62hasvarzf3zd2ev_labels1468z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2290z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2481za7,
		BGl_z62fsiza7ezd2ev_global1288z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2372z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2482za7,
		BGl_z62hasvarzf3zd2ev_goto1470z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2291z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2483za7,
		BGl_z62fsiza7ezd2ev_litt1290z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2373z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2484za7,
		BGl_z62hasvarzf3zd2ev_app1472z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2292z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2485za7,
		BGl_z62fsiza7ezd2ev_if1292z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2374z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762hasvarza7f3za7d2e2486za7,
		BGl_z62hasvarzf3zd2ev_abs1474z43zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2293z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2487za7,
		BGl_z62fsiza7ezd2ev_list1294z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2294z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2488za7,
		BGl_z62fsiza7ezd2ev_prog21296z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2295z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2489za7,
		BGl_z62fsiza7ezd2ev_hook1298z17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2296z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2490za7,
		BGl_z62fsiza7ezd2ev_bindzd2exit1300zc5zz__evaluate_fsiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2297z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2491za7,
		BGl_z62fsiza7ezd2ev_unwindzd2prot1302zc5zz__evaluate_fsiza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2298z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2492za7,
		BGl_z62fsiza7ezd2ev_withzd2handle1304zc5zz__evaluate_fsiza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2299z00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7d2ev_2493za7,
		BGl_z62fsiza7ezd2ev_synchroniza7e1306zb0zz__evaluate_fsiza7eza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_framezd2siza7ezd2envza7zz__evaluate_fsiza7eza7,
		BgL_bgl_za762frameza7d2siza7a72494za7,
		BGl_z62framezd2siza7ez17zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
		BgL_bgl_za762fsiza7a7eza7c5za7za7_2495za7,
		BGl_z62fsiza7ezc5zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762searchza7d2letr2496z00,
		BGl_z62searchzd2letreczb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_extractzd2loopszd2envz00zz__evaluate_fsiza7eza7,
		BgL_bgl_za762extractza7d2loo2497z00,
		BGl_z62extractzd2loopszb0zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
		BgL_bgl_za762subst_gotoza7622498z00,
		BGl_z62subst_gotoz62zz__evaluate_fsiza7eza7, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__evaluate_fsiza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2270z00zz__evaluate_fsiza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2272z00zz__evaluate_fsiza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2275z00zz__evaluate_fsiza7eza7));
		     ADD_ROOT((void *) (&BGl_symbol2277z00zz__evaluate_fsiza7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_fsiza7eza7(long
		BgL_checksumz00_4922, char *BgL_fromz00_4923)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evaluate_fsiza7eza7))
				{
					BGl_requirezd2initializa7ationz75zz__evaluate_fsiza7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evaluate_fsiza7eza7();
					BGl_cnstzd2initzd2zz__evaluate_fsiza7eza7();
					BGl_importedzd2moduleszd2initz00zz__evaluate_fsiza7eza7();
					BGl_genericzd2initzd2zz__evaluate_fsiza7eza7();
					BGl_methodzd2initzd2zz__evaluate_fsiza7eza7();
					return BGl_toplevelzd2initzd2zz__evaluate_fsiza7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			BGl_symbol2270z00zz__evaluate_fsiza7eza7 =
				bstring_to_symbol(BGl_string2269z00zz__evaluate_fsiza7eza7);
			BGl_symbol2272z00zz__evaluate_fsiza7eza7 =
				bstring_to_symbol(BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_symbol2275z00zz__evaluate_fsiza7eza7 =
				bstring_to_symbol(BGl_string2276z00zz__evaluate_fsiza7eza7);
			return (BGl_symbol2277z00zz__evaluate_fsiza7eza7 =
				bstring_to_symbol(BGl_string2278z00zz__evaluate_fsiza7eza7), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			return BUNSPEC;
		}

	}



/* frame-size */
	BGL_EXPORTED_DEF int
		BGl_framezd2siza7ez75zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt BgL_ez00_3)
	{
		{	/* Eval/evaluate_fsize.scm 65 */
			return BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_ez00_3, (int) (0L));
		}

	}



/* &frame-size */
	obj_t BGl_z62framezd2siza7ez17zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3810,
		obj_t BgL_ez00_3811)
	{
		{	/* Eval/evaluate_fsize.scm 65 */
			{	/* Eval/evaluate_fsize.scm 66 */
				int BgL_tmpz00_4940;

				{	/* Eval/evaluate_fsize.scm 66 */
					BgL_ev_exprz00_bglt BgL_auxz00_4941;

					if (BGl_isazf3zf3zz__objectz00(BgL_ez00_3811,
							BGl_ev_exprz00zz__evaluate_typesz00))
						{	/* Eval/evaluate_fsize.scm 66 */
							BgL_auxz00_4941 = ((BgL_ev_exprz00_bglt) BgL_ez00_3811);
						}
					else
						{
							obj_t BgL_auxz00_4945;

							BgL_auxz00_4945 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(1965L),
								BGl_string2257z00zz__evaluate_fsiza7eza7,
								BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3811);
							FAILURE(BgL_auxz00_4945, BFALSE, BFALSE);
						}
					BgL_tmpz00_4940 =
						BGl_framezd2siza7ez75zz__evaluate_fsiza7eza7(BgL_auxz00_4941);
				}
				return BINT(BgL_tmpz00_4940);
			}
		}

	}



/* extract-loops */
	BGL_EXPORTED_DEF BgL_ev_exprz00_bglt
		BGl_extractzd2loopszd2zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt
		BgL_ez00_42)
	{
		{	/* Eval/evaluate_fsize.scm 167 */
			return
				((BgL_ev_exprz00_bglt)
				BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7(BgL_ez00_42));
		}

	}



/* &extract-loops */
	BgL_ev_exprz00_bglt BGl_z62extractzd2loopszb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3812, obj_t BgL_ez00_3813)
	{
		{	/* Eval/evaluate_fsize.scm 167 */
			{	/* Eval/evaluate_fsize.scm 169 */
				BgL_ev_exprz00_bglt BgL_auxz00_4953;

				if (BGl_isazf3zf3zz__objectz00(BgL_ez00_3813,
						BGl_ev_exprz00zz__evaluate_typesz00))
					{	/* Eval/evaluate_fsize.scm 169 */
						BgL_auxz00_4953 = ((BgL_ev_exprz00_bglt) BgL_ez00_3813);
					}
				else
					{
						obj_t BgL_auxz00_4957;

						BgL_auxz00_4957 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(5040L),
							BGl_string2259z00zz__evaluate_fsiza7eza7,
							BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3813);
						FAILURE(BgL_auxz00_4957, BFALSE, BFALSE);
					}
				return BGl_extractzd2loopszd2zz__evaluate_fsiza7eza7(BgL_auxz00_4953);
			}
		}

	}



/* search-letrec* */
	bool_t BGl_searchzd2letrecza2z70zz__evaluate_fsiza7eza7(obj_t BgL_lz00_43)
	{
		{	/* Eval/evaluate_fsize.scm 172 */
			{
				obj_t BgL_lz00_1323;

				BgL_lz00_1323 = BgL_lz00_43;
			BgL_zc3z04anonymousza31545ze3z87_1324:
				if (NULLP(BgL_lz00_1323))
					{	/* Eval/evaluate_fsize.scm 174 */
						return ((bool_t) 0);
					}
				else
					{	/* Eval/evaluate_fsize.scm 174 */
						{	/* Eval/evaluate_fsize.scm 175 */
							obj_t BgL_arg1547z00_1326;

							{	/* Eval/evaluate_fsize.scm 175 */
								obj_t BgL_arg1549z00_1327;

								BgL_arg1549z00_1327 = CAR(((obj_t) BgL_lz00_1323));
								BgL_arg1547z00_1326 =
									BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7(
									((BgL_ev_exprz00_bglt) BgL_arg1549z00_1327));
							}
							{	/* Eval/evaluate_fsize.scm 175 */
								obj_t BgL_tmpz00_4968;

								BgL_tmpz00_4968 = ((obj_t) BgL_lz00_1323);
								SET_CAR(BgL_tmpz00_4968, BgL_arg1547z00_1326);
							}
						}
						{	/* Eval/evaluate_fsize.scm 176 */
							obj_t BgL_arg1552z00_1328;

							BgL_arg1552z00_1328 = CDR(((obj_t) BgL_lz00_1323));
							{
								obj_t BgL_lz00_4973;

								BgL_lz00_4973 = BgL_arg1552z00_1328;
								BgL_lz00_1323 = BgL_lz00_4973;
								goto BgL_zc3z04anonymousza31545ze3z87_1324;
							}
						}
					}
			}
		}

	}



/* modify-letrec */
	BgL_ev_labelsz00_bglt BGl_modifyzd2letreczd2zz__evaluate_fsiza7eza7(obj_t
		BgL_varsz00_60, obj_t BgL_valsz00_61, BgL_ev_exprz00_bglt BgL_bodyz00_62)
	{
		{	/* Eval/evaluate_fsize.scm 268 */
			{	/* Eval/evaluate_fsize.scm 269 */
				BgL_ev_labelsz00_bglt BgL_rz00_1330;

				{	/* Eval/evaluate_fsize.scm 269 */
					BgL_ev_labelsz00_bglt BgL_new1094z00_1349;

					{	/* Eval/evaluate_fsize.scm 269 */
						BgL_ev_labelsz00_bglt BgL_new1093z00_1353;

						BgL_new1093z00_1353 =
							((BgL_ev_labelsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_ev_labelsz00_bgl))));
						{	/* Eval/evaluate_fsize.scm 269 */
							long BgL_arg1564z00_1354;

							BgL_arg1564z00_1354 =
								BGL_CLASS_NUM(BGl_ev_labelsz00zz__evaluate_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1093z00_1353),
								BgL_arg1564z00_1354);
						}
						BgL_new1094z00_1349 = BgL_new1093z00_1353;
					}
					((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1094z00_1349))->
							BgL_varsz00) = ((obj_t) BgL_varsz00_60), BUNSPEC);
					((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1094z00_1349))->
							BgL_valsz00) = ((obj_t) BNIL), BUNSPEC);
					((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1094z00_1349))->
							BgL_envz00) = ((obj_t) BNIL), BUNSPEC);
					((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1094z00_1349))->
							BgL_stkz00) = ((obj_t) BNIL), BUNSPEC);
					{
						BgL_ev_exprz00_bglt BgL_auxz00_4982;

						{	/* Eval/evaluate_fsize.scm 270 */
							BgL_ev_littz00_bglt BgL_new1096z00_1350;

							{	/* Eval/evaluate_fsize.scm 270 */
								BgL_ev_littz00_bglt BgL_new1095z00_1351;

								BgL_new1095z00_1351 =
									((BgL_ev_littz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_ev_littz00_bgl))));
								{	/* Eval/evaluate_fsize.scm 270 */
									long BgL_arg1562z00_1352;

									{	/* Eval/evaluate_fsize.scm 270 */
										obj_t BgL_classz00_2760;

										BgL_classz00_2760 = BGl_ev_littz00zz__evaluate_typesz00;
										BgL_arg1562z00_1352 = BGL_CLASS_NUM(BgL_classz00_2760);
									}
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1095z00_1351),
										BgL_arg1562z00_1352);
								}
								BgL_new1096z00_1350 = BgL_new1095z00_1351;
							}
							((((BgL_ev_littz00_bglt) COBJECT(BgL_new1096z00_1350))->
									BgL_valuez00) = ((obj_t) BINT(0L)), BUNSPEC);
							BgL_auxz00_4982 = ((BgL_ev_exprz00_bglt) BgL_new1096z00_1350);
						}
						((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1094z00_1349))->
								BgL_bodyz00) =
							((BgL_ev_exprz00_bglt) BgL_auxz00_4982), BUNSPEC);
					}
					((((BgL_ev_labelsz00_bglt) COBJECT(BgL_new1094z00_1349))->
							BgL_boxesz00) = ((obj_t) BNIL), BUNSPEC);
					BgL_rz00_1330 = BgL_new1094z00_1349;
				}
				((((BgL_ev_labelsz00_bglt) COBJECT(BgL_rz00_1330))->BgL_bodyz00) =
					((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_bodyz00_62,
								BgL_varsz00_60, ((obj_t) BgL_rz00_1330)))), BUNSPEC);
				{
					obj_t BgL_auxz00_4996;

					if (NULLP(BgL_valsz00_61))
						{	/* Eval/evaluate_fsize.scm 273 */
							BgL_auxz00_4996 = BNIL;
						}
					else
						{	/* Eval/evaluate_fsize.scm 273 */
							obj_t BgL_head1217z00_1334;

							BgL_head1217z00_1334 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1215z00_1336;
								obj_t BgL_tail1218z00_1337;

								BgL_l1215z00_1336 = BgL_valsz00_61;
								BgL_tail1218z00_1337 = BgL_head1217z00_1334;
							BgL_zc3z04anonymousza31554ze3z87_1338:
								if (NULLP(BgL_l1215z00_1336))
									{	/* Eval/evaluate_fsize.scm 273 */
										BgL_auxz00_4996 = CDR(BgL_head1217z00_1334);
									}
								else
									{	/* Eval/evaluate_fsize.scm 273 */
										obj_t BgL_newtail1219z00_1340;

										{	/* Eval/evaluate_fsize.scm 273 */
											obj_t BgL_arg1557z00_1342;

											{	/* Eval/evaluate_fsize.scm 273 */
												obj_t BgL_valz00_1343;

												BgL_valz00_1343 = CAR(((obj_t) BgL_l1215z00_1336));
												{	/* Eval/evaluate_fsize.scm 275 */
													obj_t BgL_arg1558z00_1345;
													obj_t BgL_arg1559z00_1346;

													BgL_arg1558z00_1345 =
														(((BgL_ev_absz00_bglt) COBJECT(
																((BgL_ev_absz00_bglt) BgL_valz00_1343)))->
														BgL_varsz00);
													{	/* Eval/evaluate_fsize.scm 275 */
														BgL_ev_exprz00_bglt BgL_arg1561z00_1347;

														BgL_arg1561z00_1347 =
															(((BgL_ev_absz00_bglt) COBJECT(
																	((BgL_ev_absz00_bglt) BgL_valz00_1343)))->
															BgL_bodyz00);
														BgL_arg1559z00_1346 =
															BGl_subst_gotoz00zz__evaluate_fsiza7eza7
															(BgL_arg1561z00_1347, BgL_varsz00_60,
															((obj_t) BgL_rz00_1330));
													}
													BgL_arg1557z00_1342 =
														MAKE_YOUNG_PAIR(BgL_arg1558z00_1345,
														BgL_arg1559z00_1346);
												}
											}
											BgL_newtail1219z00_1340 =
												MAKE_YOUNG_PAIR(BgL_arg1557z00_1342, BNIL);
										}
										SET_CDR(BgL_tail1218z00_1337, BgL_newtail1219z00_1340);
										{	/* Eval/evaluate_fsize.scm 273 */
											obj_t BgL_arg1556z00_1341;

											BgL_arg1556z00_1341 = CDR(((obj_t) BgL_l1215z00_1336));
											{
												obj_t BgL_tail1218z00_5017;
												obj_t BgL_l1215z00_5016;

												BgL_l1215z00_5016 = BgL_arg1556z00_1341;
												BgL_tail1218z00_5017 = BgL_newtail1219z00_1340;
												BgL_tail1218z00_1337 = BgL_tail1218z00_5017;
												BgL_l1215z00_1336 = BgL_l1215z00_5016;
												goto BgL_zc3z04anonymousza31554ze3z87_1338;
											}
										}
									}
							}
						}
					((((BgL_ev_labelsz00_bglt) COBJECT(BgL_rz00_1330))->BgL_valsz00) =
						((obj_t) BgL_auxz00_4996), BUNSPEC);
				}
				return BgL_rz00_1330;
			}
		}

	}



/* letrectail? */
	bool_t BGl_letrectailzf3zf3zz__evaluate_fsiza7eza7(obj_t BgL_varsz00_63,
		obj_t BgL_valsz00_64, BgL_ev_exprz00_bglt BgL_bodyz00_65)
	{
		{	/* Eval/evaluate_fsize.scm 279 */
			{
				obj_t BgL_l1223z00_1356;

				BgL_l1223z00_1356 = BgL_varsz00_63;
			BgL_zc3z04anonymousza31565ze3z87_1357:
				if (NULLP(BgL_l1223z00_1356))
					{	/* Eval/evaluate_fsize.scm 280 */
						return ((bool_t) 1);
					}
				else
					{	/* Eval/evaluate_fsize.scm 280 */
						bool_t BgL_test2506z00_5021;

						{	/* Eval/evaluate_fsize.scm 281 */
							obj_t BgL_vz00_1362;

							BgL_vz00_1362 = CAR(((obj_t) BgL_l1223z00_1356));
							{	/* Eval/evaluate_fsize.scm 281 */
								obj_t BgL__andtest_1099z00_1363;

								BgL__andtest_1099z00_1363 =
									BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_bodyz00_65,
									((BgL_ev_varz00_bglt) BgL_vz00_1362));
								if (CBOOL(BgL__andtest_1099z00_1363))
									{
										obj_t BgL_l1220z00_1365;

										BgL_l1220z00_1365 = BgL_valsz00_64;
									BgL_zc3z04anonymousza31568ze3z87_1366:
										if (NULLP(BgL_l1220z00_1365))
											{	/* Eval/evaluate_fsize.scm 282 */
												BgL_test2506z00_5021 = ((bool_t) 1);
											}
										else
											{	/* Eval/evaluate_fsize.scm 283 */
												obj_t BgL_nvz00_1368;

												{	/* Eval/evaluate_fsize.scm 283 */
													obj_t BgL_ez00_1371;

													BgL_ez00_1371 = CAR(((obj_t) BgL_l1220z00_1365));
													{	/* Eval/evaluate_fsize.scm 283 */
														bool_t BgL_test2509z00_5032;

														{	/* Eval/evaluate_fsize.scm 283 */
															obj_t BgL_classz00_2769;

															BgL_classz00_2769 =
																BGl_ev_absz00zz__evaluate_typesz00;
															if (BGL_OBJECTP(BgL_ez00_1371))
																{	/* Eval/evaluate_fsize.scm 283 */
																	BgL_objectz00_bglt BgL_arg2177z00_2771;

																	BgL_arg2177z00_2771 =
																		(BgL_objectz00_bglt) (BgL_ez00_1371);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Eval/evaluate_fsize.scm 283 */
																			long BgL_idxz00_2777;

																			BgL_idxz00_2777 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg2177z00_2771);
																			BgL_test2509z00_5032 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2777 + 2L)) ==
																				BgL_classz00_2769);
																		}
																	else
																		{	/* Eval/evaluate_fsize.scm 283 */
																			bool_t BgL_res2198z00_2802;

																			{	/* Eval/evaluate_fsize.scm 283 */
																				obj_t BgL_oclassz00_2785;

																				{	/* Eval/evaluate_fsize.scm 283 */
																					obj_t BgL_arg2184z00_2793;
																					long BgL_arg2185z00_2794;

																					BgL_arg2184z00_2793 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Eval/evaluate_fsize.scm 283 */
																						long BgL_arg2186z00_2795;

																						BgL_arg2186z00_2795 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg2177z00_2771);
																						BgL_arg2185z00_2794 =
																							(BgL_arg2186z00_2795 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2785 =
																						VECTOR_REF(BgL_arg2184z00_2793,
																						BgL_arg2185z00_2794);
																				}
																				{	/* Eval/evaluate_fsize.scm 283 */
																					bool_t BgL__ortest_1212z00_2786;

																					BgL__ortest_1212z00_2786 =
																						(BgL_classz00_2769 ==
																						BgL_oclassz00_2785);
																					if (BgL__ortest_1212z00_2786)
																						{	/* Eval/evaluate_fsize.scm 283 */
																							BgL_res2198z00_2802 =
																								BgL__ortest_1212z00_2786;
																						}
																					else
																						{	/* Eval/evaluate_fsize.scm 283 */
																							long BgL_odepthz00_2787;

																							{	/* Eval/evaluate_fsize.scm 283 */
																								obj_t BgL_arg2174z00_2788;

																								BgL_arg2174z00_2788 =
																									(BgL_oclassz00_2785);
																								BgL_odepthz00_2787 =
																									BGL_CLASS_DEPTH
																									(BgL_arg2174z00_2788);
																							}
																							if ((2L < BgL_odepthz00_2787))
																								{	/* Eval/evaluate_fsize.scm 283 */
																									obj_t BgL_arg2172z00_2790;

																									{	/* Eval/evaluate_fsize.scm 283 */
																										obj_t BgL_arg2173z00_2791;

																										BgL_arg2173z00_2791 =
																											(BgL_oclassz00_2785);
																										BgL_arg2172z00_2790 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg2173z00_2791, 2L);
																									}
																									BgL_res2198z00_2802 =
																										(BgL_arg2172z00_2790 ==
																										BgL_classz00_2769);
																								}
																							else
																								{	/* Eval/evaluate_fsize.scm 283 */
																									BgL_res2198z00_2802 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2509z00_5032 =
																				BgL_res2198z00_2802;
																		}
																}
															else
																{	/* Eval/evaluate_fsize.scm 283 */
																	BgL_test2509z00_5032 = ((bool_t) 0);
																}
														}
														if (BgL_test2509z00_5032)
															{	/* Eval/evaluate_fsize.scm 283 */
																if (
																	((long) CINT(
																			(((BgL_ev_absz00_bglt) COBJECT(
																						((BgL_ev_absz00_bglt)
																							BgL_ez00_1371)))->
																				BgL_arityz00)) >= 0L))
																	{	/* Eval/evaluate_fsize.scm 286 */
																		BgL_ev_exprz00_bglt BgL_arg1573z00_1375;

																		BgL_arg1573z00_1375 =
																			(((BgL_ev_absz00_bglt) COBJECT(
																					((BgL_ev_absz00_bglt)
																						BgL_ez00_1371)))->BgL_bodyz00);
																		BgL_nvz00_1368 =
																			BGl_tailposz00zz__evaluate_fsiza7eza7
																			(BgL_arg1573z00_1375,
																			((BgL_ev_varz00_bglt) BgL_vz00_1362));
																	}
																else
																	{	/* Eval/evaluate_fsize.scm 285 */
																		BgL_nvz00_1368 = BFALSE;
																	}
															}
														else
															{	/* Eval/evaluate_fsize.scm 283 */
																BgL_nvz00_1368 = BFALSE;
															}
													}
												}
												if (CBOOL(BgL_nvz00_1368))
													{	/* Eval/evaluate_fsize.scm 282 */
														obj_t BgL_arg1571z00_1370;

														BgL_arg1571z00_1370 =
															CDR(((obj_t) BgL_l1220z00_1365));
														{
															obj_t BgL_l1220z00_5068;

															BgL_l1220z00_5068 = BgL_arg1571z00_1370;
															BgL_l1220z00_1365 = BgL_l1220z00_5068;
															goto BgL_zc3z04anonymousza31568ze3z87_1366;
														}
													}
												else
													{	/* Eval/evaluate_fsize.scm 282 */
														BgL_test2506z00_5021 = ((bool_t) 0);
													}
											}
									}
								else
									{	/* Eval/evaluate_fsize.scm 281 */
										BgL_test2506z00_5021 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2506z00_5021)
							{	/* Eval/evaluate_fsize.scm 280 */
								obj_t BgL_arg1567z00_1361;

								BgL_arg1567z00_1361 = CDR(((obj_t) BgL_l1223z00_1356));
								{
									obj_t BgL_l1223z00_5071;

									BgL_l1223z00_5071 = BgL_arg1567z00_1361;
									BgL_l1223z00_1356 = BgL_l1223z00_5071;
									goto BgL_zc3z04anonymousza31565ze3z87_1357;
								}
							}
						else
							{	/* Eval/evaluate_fsize.scm 280 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_proc2260z00zz__evaluate_fsiza7eza7,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string2261z00zz__evaluate_fsiza7eza7);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_proc2262z00zz__evaluate_fsiza7eza7,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string2263z00zz__evaluate_fsiza7eza7);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_proc2264z00zz__evaluate_fsiza7eza7,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string2265z00zz__evaluate_fsiza7eza7);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_proc2266z00zz__evaluate_fsiza7eza7,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string2267z00zz__evaluate_fsiza7eza7);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_proc2268z00zz__evaluate_fsiza7eza7,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string2269z00zz__evaluate_fsiza7eza7);
		}

	}



/* &hasvar?1438 */
	obj_t BGl_z62hasvarzf31438z91zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3819,
		obj_t BgL_ez00_3820, obj_t BgL_vz00_3821)
	{
		{	/* Eval/evaluate_fsize.scm 491 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2270z00zz__evaluate_fsiza7eza7,
				BGl_string2271z00zz__evaluate_fsiza7eza7,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_3820)));
		}

	}



/* &tailpos1391 */
	obj_t BGl_z62tailpos1391z62zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3822,
		obj_t BgL_ez00_3823, obj_t BgL_vz00_3824)
	{
		{	/* Eval/evaluate_fsize.scm 403 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2272z00zz__evaluate_fsiza7eza7,
				BGl_string2274z00zz__evaluate_fsiza7eza7,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_3823)));
		}

	}



/* &subst_goto1353 */
	obj_t BGl_z62subst_goto1353z62zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3825,
		obj_t BgL_ez00_3826, obj_t BgL_varsz00_3827, obj_t BgL_lblsz00_3828)
	{
		{	/* Eval/evaluate_fsize.scm 298 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2275z00zz__evaluate_fsiza7eza7,
				BGl_string2274z00zz__evaluate_fsiza7eza7,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_3826)));
		}

	}



/* &search-letrec1321 */
	obj_t BGl_z62searchzd2letrec1321zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3829, obj_t BgL_ez00_3830)
	{
		{	/* Eval/evaluate_fsize.scm 178 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol2277z00zz__evaluate_fsiza7eza7,
				BGl_string2274z00zz__evaluate_fsiza7eza7,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_3830)));
		}

	}



/* &fsize1283 */
	obj_t BGl_z62fsiza7e1283zc5zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3831,
		obj_t BgL_ez00_3832, obj_t BgL_nz00_3833)
	{
		{	/* Eval/evaluate_fsize.scm 68 */
			return
				BGl_errorz00zz__errorz00(BGl_string2279z00zz__evaluate_fsiza7eza7,
				BGl_string2280z00zz__evaluate_fsiza7eza7,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_3832)));
		}

	}



/* fsize */
	int BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt BgL_ez00_4,
		int BgL_nz00_5)
	{
		{	/* Eval/evaluate_fsize.scm 68 */
			{	/* Eval/evaluate_fsize.scm 68 */
				obj_t BgL_method1284z00_1412;

				{	/* Eval/evaluate_fsize.scm 68 */
					obj_t BgL_res2203z00_2845;

					{	/* Eval/evaluate_fsize.scm 68 */
						long BgL_objzd2classzd2numz00_2816;

						BgL_objzd2classzd2numz00_2816 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_4));
						{	/* Eval/evaluate_fsize.scm 68 */
							obj_t BgL_arg2181z00_2817;

							BgL_arg2181z00_2817 =
								PROCEDURE_REF(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
								(int) (1L));
							{	/* Eval/evaluate_fsize.scm 68 */
								int BgL_offsetz00_2820;

								BgL_offsetz00_2820 = (int) (BgL_objzd2classzd2numz00_2816);
								{	/* Eval/evaluate_fsize.scm 68 */
									long BgL_offsetz00_2821;

									BgL_offsetz00_2821 =
										((long) (BgL_offsetz00_2820) - OBJECT_TYPE);
									{	/* Eval/evaluate_fsize.scm 68 */
										long BgL_modz00_2822;

										BgL_modz00_2822 =
											(BgL_offsetz00_2821 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_fsize.scm 68 */
											long BgL_restz00_2824;

											BgL_restz00_2824 =
												(BgL_offsetz00_2821 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_fsize.scm 68 */

												{	/* Eval/evaluate_fsize.scm 68 */
													obj_t BgL_bucketz00_2826;

													BgL_bucketz00_2826 =
														VECTOR_REF(
														((obj_t) BgL_arg2181z00_2817), BgL_modz00_2822);
													BgL_res2203z00_2845 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2826), BgL_restz00_2824);
					}}}}}}}}
					BgL_method1284z00_1412 = BgL_res2203z00_2845;
				}
				return
					CINT(BGL_PROCEDURE_CALL2(BgL_method1284z00_1412,
						((obj_t) BgL_ez00_4), BINT(BgL_nz00_5)));
			}
		}

	}



/* &fsize */
	obj_t BGl_z62fsiza7ezc5zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3834,
		obj_t BgL_ez00_3835, obj_t BgL_nz00_3836)
	{
		{	/* Eval/evaluate_fsize.scm 68 */
			{	/* Eval/evaluate_fsize.scm 68 */
				int BgL_tmpz00_5125;

				{	/* Eval/evaluate_fsize.scm 68 */
					int BgL_auxz00_5156;
					BgL_ev_exprz00_bglt BgL_auxz00_5126;

					{	/* Eval/evaluate_fsize.scm 68 */
						obj_t BgL_tmpz00_5157;

						if (INTEGERP(BgL_nz00_3836))
							{	/* Eval/evaluate_fsize.scm 68 */
								BgL_tmpz00_5157 = BgL_nz00_3836;
							}
						else
							{
								obj_t BgL_auxz00_5160;

								BgL_auxz00_5160 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(1980L),
									BGl_string2281z00zz__evaluate_fsiza7eza7,
									BGl_string2282z00zz__evaluate_fsiza7eza7, BgL_nz00_3836);
								FAILURE(BgL_auxz00_5160, BFALSE, BFALSE);
							}
						BgL_auxz00_5156 = CINT(BgL_tmpz00_5157);
					}
					{	/* Eval/evaluate_fsize.scm 68 */
						bool_t BgL_test2516z00_5127;

						{	/* Eval/evaluate_fsize.scm 68 */
							obj_t BgL_classz00_4239;

							BgL_classz00_4239 = BGl_ev_exprz00zz__evaluate_typesz00;
							if (BGL_OBJECTP(BgL_ez00_3835))
								{	/* Eval/evaluate_fsize.scm 68 */
									BgL_objectz00_bglt BgL_arg2179z00_4241;
									long BgL_arg2180z00_4242;

									BgL_arg2179z00_4241 = (BgL_objectz00_bglt) (BgL_ez00_3835);
									BgL_arg2180z00_4242 = BGL_CLASS_DEPTH(BgL_classz00_4239);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Eval/evaluate_fsize.scm 68 */
											long BgL_idxz00_4250;

											BgL_idxz00_4250 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4241);
											{	/* Eval/evaluate_fsize.scm 68 */
												obj_t BgL_arg2170z00_4251;

												{	/* Eval/evaluate_fsize.scm 68 */
													long BgL_arg2171z00_4252;

													BgL_arg2171z00_4252 =
														(BgL_idxz00_4250 + BgL_arg2180z00_4242);
													BgL_arg2170z00_4251 =
														VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														BgL_arg2171z00_4252);
												}
												BgL_test2516z00_5127 =
													(BgL_arg2170z00_4251 == BgL_classz00_4239);
										}}
									else
										{	/* Eval/evaluate_fsize.scm 68 */
											bool_t BgL_res2225z00_4257;

											{	/* Eval/evaluate_fsize.scm 68 */
												obj_t BgL_oclassz00_4261;

												{	/* Eval/evaluate_fsize.scm 68 */
													obj_t BgL_arg2184z00_4263;
													long BgL_arg2185z00_4264;

													BgL_arg2184z00_4263 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Eval/evaluate_fsize.scm 68 */
														long BgL_arg2186z00_4265;

														BgL_arg2186z00_4265 =
															BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4241);
														BgL_arg2185z00_4264 =
															(BgL_arg2186z00_4265 - OBJECT_TYPE);
													}
													BgL_oclassz00_4261 =
														VECTOR_REF(BgL_arg2184z00_4263,
														BgL_arg2185z00_4264);
												}
												{	/* Eval/evaluate_fsize.scm 68 */
													bool_t BgL__ortest_1212z00_4271;

													BgL__ortest_1212z00_4271 =
														(BgL_classz00_4239 == BgL_oclassz00_4261);
													if (BgL__ortest_1212z00_4271)
														{	/* Eval/evaluate_fsize.scm 68 */
															BgL_res2225z00_4257 = BgL__ortest_1212z00_4271;
														}
													else
														{	/* Eval/evaluate_fsize.scm 68 */
															long BgL_odepthz00_4272;

															{	/* Eval/evaluate_fsize.scm 68 */
																obj_t BgL_arg2174z00_4273;

																BgL_arg2174z00_4273 = (BgL_oclassz00_4261);
																BgL_odepthz00_4272 =
																	BGL_CLASS_DEPTH(BgL_arg2174z00_4273);
															}
															if ((BgL_arg2180z00_4242 < BgL_odepthz00_4272))
																{	/* Eval/evaluate_fsize.scm 68 */
																	obj_t BgL_arg2172z00_4277;

																	{	/* Eval/evaluate_fsize.scm 68 */
																		obj_t BgL_arg2173z00_4278;

																		BgL_arg2173z00_4278 = (BgL_oclassz00_4261);
																		BgL_arg2172z00_4277 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg2173z00_4278,
																			BgL_arg2180z00_4242);
																	}
																	BgL_res2225z00_4257 =
																		(BgL_arg2172z00_4277 == BgL_classz00_4239);
																}
															else
																{	/* Eval/evaluate_fsize.scm 68 */
																	BgL_res2225z00_4257 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2516z00_5127 = BgL_res2225z00_4257;
										}
								}
							else
								{	/* Eval/evaluate_fsize.scm 68 */
									BgL_test2516z00_5127 = ((bool_t) 0);
								}
						}
						if (BgL_test2516z00_5127)
							{	/* Eval/evaluate_fsize.scm 68 */
								BgL_auxz00_5126 = ((BgL_ev_exprz00_bglt) BgL_ez00_3835);
							}
						else
							{
								obj_t BgL_auxz00_5152;

								BgL_auxz00_5152 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(1980L),
									BGl_string2281z00zz__evaluate_fsiza7eza7,
									BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3835);
								FAILURE(BgL_auxz00_5152, BFALSE, BFALSE);
							}
					}
					BgL_tmpz00_5125 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_auxz00_5126,
						BgL_auxz00_5156);
				}
				return BINT(BgL_tmpz00_5125);
			}
		}

	}



/* search-letrec */
	obj_t BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt
		BgL_ez00_44)
	{
		{	/* Eval/evaluate_fsize.scm 178 */
			{	/* Eval/evaluate_fsize.scm 178 */
				obj_t BgL_method1322z00_1413;

				{	/* Eval/evaluate_fsize.scm 178 */
					obj_t BgL_res2208z00_2876;

					{	/* Eval/evaluate_fsize.scm 178 */
						long BgL_objzd2classzd2numz00_2847;

						BgL_objzd2classzd2numz00_2847 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_44));
						{	/* Eval/evaluate_fsize.scm 178 */
							obj_t BgL_arg2181z00_2848;

							BgL_arg2181z00_2848 =
								PROCEDURE_REF
								(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
								(int) (1L));
							{	/* Eval/evaluate_fsize.scm 178 */
								int BgL_offsetz00_2851;

								BgL_offsetz00_2851 = (int) (BgL_objzd2classzd2numz00_2847);
								{	/* Eval/evaluate_fsize.scm 178 */
									long BgL_offsetz00_2852;

									BgL_offsetz00_2852 =
										((long) (BgL_offsetz00_2851) - OBJECT_TYPE);
									{	/* Eval/evaluate_fsize.scm 178 */
										long BgL_modz00_2853;

										BgL_modz00_2853 =
											(BgL_offsetz00_2852 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_fsize.scm 178 */
											long BgL_restz00_2855;

											BgL_restz00_2855 =
												(BgL_offsetz00_2852 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_fsize.scm 178 */

												{	/* Eval/evaluate_fsize.scm 178 */
													obj_t BgL_bucketz00_2857;

													BgL_bucketz00_2857 =
														VECTOR_REF(
														((obj_t) BgL_arg2181z00_2848), BgL_modz00_2853);
													BgL_res2208z00_2876 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2857), BgL_restz00_2855);
					}}}}}}}}
					BgL_method1322z00_1413 = BgL_res2208z00_2876;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1322z00_1413, ((obj_t) BgL_ez00_44));
			}
		}

	}



/* &search-letrec */
	obj_t BGl_z62searchzd2letreczb0zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3837,
		obj_t BgL_ez00_3838)
	{
		{	/* Eval/evaluate_fsize.scm 178 */
			{	/* Eval/evaluate_fsize.scm 178 */
				BgL_ev_exprz00_bglt BgL_auxz00_5197;

				{	/* Eval/evaluate_fsize.scm 178 */
					bool_t BgL_test2522z00_5198;

					{	/* Eval/evaluate_fsize.scm 178 */
						obj_t BgL_classz00_4279;

						BgL_classz00_4279 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_3838))
							{	/* Eval/evaluate_fsize.scm 178 */
								BgL_objectz00_bglt BgL_arg2179z00_4281;
								long BgL_arg2180z00_4282;

								BgL_arg2179z00_4281 = (BgL_objectz00_bglt) (BgL_ez00_3838);
								BgL_arg2180z00_4282 = BGL_CLASS_DEPTH(BgL_classz00_4279);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_fsize.scm 178 */
										long BgL_idxz00_4290;

										BgL_idxz00_4290 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4281);
										{	/* Eval/evaluate_fsize.scm 178 */
											obj_t BgL_arg2170z00_4291;

											{	/* Eval/evaluate_fsize.scm 178 */
												long BgL_arg2171z00_4292;

												BgL_arg2171z00_4292 =
													(BgL_idxz00_4290 + BgL_arg2180z00_4282);
												BgL_arg2170z00_4291 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2171z00_4292);
											}
											BgL_test2522z00_5198 =
												(BgL_arg2170z00_4291 == BgL_classz00_4279);
									}}
								else
									{	/* Eval/evaluate_fsize.scm 178 */
										bool_t BgL_res2225z00_4297;

										{	/* Eval/evaluate_fsize.scm 178 */
											obj_t BgL_oclassz00_4301;

											{	/* Eval/evaluate_fsize.scm 178 */
												obj_t BgL_arg2184z00_4303;
												long BgL_arg2185z00_4304;

												BgL_arg2184z00_4303 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_fsize.scm 178 */
													long BgL_arg2186z00_4305;

													BgL_arg2186z00_4305 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4281);
													BgL_arg2185z00_4304 =
														(BgL_arg2186z00_4305 - OBJECT_TYPE);
												}
												BgL_oclassz00_4301 =
													VECTOR_REF(BgL_arg2184z00_4303, BgL_arg2185z00_4304);
											}
											{	/* Eval/evaluate_fsize.scm 178 */
												bool_t BgL__ortest_1212z00_4311;

												BgL__ortest_1212z00_4311 =
													(BgL_classz00_4279 == BgL_oclassz00_4301);
												if (BgL__ortest_1212z00_4311)
													{	/* Eval/evaluate_fsize.scm 178 */
														BgL_res2225z00_4297 = BgL__ortest_1212z00_4311;
													}
												else
													{	/* Eval/evaluate_fsize.scm 178 */
														long BgL_odepthz00_4312;

														{	/* Eval/evaluate_fsize.scm 178 */
															obj_t BgL_arg2174z00_4313;

															BgL_arg2174z00_4313 = (BgL_oclassz00_4301);
															BgL_odepthz00_4312 =
																BGL_CLASS_DEPTH(BgL_arg2174z00_4313);
														}
														if ((BgL_arg2180z00_4282 < BgL_odepthz00_4312))
															{	/* Eval/evaluate_fsize.scm 178 */
																obj_t BgL_arg2172z00_4317;

																{	/* Eval/evaluate_fsize.scm 178 */
																	obj_t BgL_arg2173z00_4318;

																	BgL_arg2173z00_4318 = (BgL_oclassz00_4301);
																	BgL_arg2172z00_4317 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2173z00_4318,
																		BgL_arg2180z00_4282);
																}
																BgL_res2225z00_4297 =
																	(BgL_arg2172z00_4317 == BgL_classz00_4279);
															}
														else
															{	/* Eval/evaluate_fsize.scm 178 */
																BgL_res2225z00_4297 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2522z00_5198 = BgL_res2225z00_4297;
									}
							}
						else
							{	/* Eval/evaluate_fsize.scm 178 */
								BgL_test2522z00_5198 = ((bool_t) 0);
							}
					}
					if (BgL_test2522z00_5198)
						{	/* Eval/evaluate_fsize.scm 178 */
							BgL_auxz00_5197 = ((BgL_ev_exprz00_bglt) BgL_ez00_3838);
						}
					else
						{
							obj_t BgL_auxz00_5223;

							BgL_auxz00_5223 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(5204L),
								BGl_string2283z00zz__evaluate_fsiza7eza7,
								BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3838);
							FAILURE(BgL_auxz00_5223, BFALSE, BFALSE);
						}
				}
				return BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7(BgL_auxz00_5197);
			}
		}

	}



/* subst_goto */
	obj_t BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt
		BgL_ez00_69, obj_t BgL_varsz00_70, obj_t BgL_lblsz00_71)
	{
		{	/* Eval/evaluate_fsize.scm 298 */
			{	/* Eval/evaluate_fsize.scm 298 */
				obj_t BgL_method1354z00_1414;

				{	/* Eval/evaluate_fsize.scm 298 */
					obj_t BgL_res2213z00_2907;

					{	/* Eval/evaluate_fsize.scm 298 */
						long BgL_objzd2classzd2numz00_2878;

						BgL_objzd2classzd2numz00_2878 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_69));
						{	/* Eval/evaluate_fsize.scm 298 */
							obj_t BgL_arg2181z00_2879;

							BgL_arg2181z00_2879 =
								PROCEDURE_REF(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
								(int) (1L));
							{	/* Eval/evaluate_fsize.scm 298 */
								int BgL_offsetz00_2882;

								BgL_offsetz00_2882 = (int) (BgL_objzd2classzd2numz00_2878);
								{	/* Eval/evaluate_fsize.scm 298 */
									long BgL_offsetz00_2883;

									BgL_offsetz00_2883 =
										((long) (BgL_offsetz00_2882) - OBJECT_TYPE);
									{	/* Eval/evaluate_fsize.scm 298 */
										long BgL_modz00_2884;

										BgL_modz00_2884 =
											(BgL_offsetz00_2883 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_fsize.scm 298 */
											long BgL_restz00_2886;

											BgL_restz00_2886 =
												(BgL_offsetz00_2883 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_fsize.scm 298 */

												{	/* Eval/evaluate_fsize.scm 298 */
													obj_t BgL_bucketz00_2888;

													BgL_bucketz00_2888 =
														VECTOR_REF(
														((obj_t) BgL_arg2181z00_2879), BgL_modz00_2884);
													BgL_res2213z00_2907 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2888), BgL_restz00_2886);
					}}}}}}}}
					BgL_method1354z00_1414 = BgL_res2213z00_2907;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1354z00_1414,
					((obj_t) BgL_ez00_69), BgL_varsz00_70, BgL_lblsz00_71);
			}
		}

	}



/* &subst_goto */
	obj_t BGl_z62subst_gotoz62zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3839,
		obj_t BgL_ez00_3840, obj_t BgL_varsz00_3841, obj_t BgL_lblsz00_3842)
	{
		{	/* Eval/evaluate_fsize.scm 298 */
			{	/* Eval/evaluate_fsize.scm 298 */
				BgL_ev_exprz00_bglt BgL_auxz00_5260;

				{	/* Eval/evaluate_fsize.scm 298 */
					bool_t BgL_test2527z00_5261;

					{	/* Eval/evaluate_fsize.scm 298 */
						obj_t BgL_classz00_4319;

						BgL_classz00_4319 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_3840))
							{	/* Eval/evaluate_fsize.scm 298 */
								BgL_objectz00_bglt BgL_arg2179z00_4321;
								long BgL_arg2180z00_4322;

								BgL_arg2179z00_4321 = (BgL_objectz00_bglt) (BgL_ez00_3840);
								BgL_arg2180z00_4322 = BGL_CLASS_DEPTH(BgL_classz00_4319);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_fsize.scm 298 */
										long BgL_idxz00_4330;

										BgL_idxz00_4330 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4321);
										{	/* Eval/evaluate_fsize.scm 298 */
											obj_t BgL_arg2170z00_4331;

											{	/* Eval/evaluate_fsize.scm 298 */
												long BgL_arg2171z00_4332;

												BgL_arg2171z00_4332 =
													(BgL_idxz00_4330 + BgL_arg2180z00_4322);
												BgL_arg2170z00_4331 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2171z00_4332);
											}
											BgL_test2527z00_5261 =
												(BgL_arg2170z00_4331 == BgL_classz00_4319);
									}}
								else
									{	/* Eval/evaluate_fsize.scm 298 */
										bool_t BgL_res2225z00_4337;

										{	/* Eval/evaluate_fsize.scm 298 */
											obj_t BgL_oclassz00_4341;

											{	/* Eval/evaluate_fsize.scm 298 */
												obj_t BgL_arg2184z00_4343;
												long BgL_arg2185z00_4344;

												BgL_arg2184z00_4343 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_fsize.scm 298 */
													long BgL_arg2186z00_4345;

													BgL_arg2186z00_4345 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4321);
													BgL_arg2185z00_4344 =
														(BgL_arg2186z00_4345 - OBJECT_TYPE);
												}
												BgL_oclassz00_4341 =
													VECTOR_REF(BgL_arg2184z00_4343, BgL_arg2185z00_4344);
											}
											{	/* Eval/evaluate_fsize.scm 298 */
												bool_t BgL__ortest_1212z00_4351;

												BgL__ortest_1212z00_4351 =
													(BgL_classz00_4319 == BgL_oclassz00_4341);
												if (BgL__ortest_1212z00_4351)
													{	/* Eval/evaluate_fsize.scm 298 */
														BgL_res2225z00_4337 = BgL__ortest_1212z00_4351;
													}
												else
													{	/* Eval/evaluate_fsize.scm 298 */
														long BgL_odepthz00_4352;

														{	/* Eval/evaluate_fsize.scm 298 */
															obj_t BgL_arg2174z00_4353;

															BgL_arg2174z00_4353 = (BgL_oclassz00_4341);
															BgL_odepthz00_4352 =
																BGL_CLASS_DEPTH(BgL_arg2174z00_4353);
														}
														if ((BgL_arg2180z00_4322 < BgL_odepthz00_4352))
															{	/* Eval/evaluate_fsize.scm 298 */
																obj_t BgL_arg2172z00_4357;

																{	/* Eval/evaluate_fsize.scm 298 */
																	obj_t BgL_arg2173z00_4358;

																	BgL_arg2173z00_4358 = (BgL_oclassz00_4341);
																	BgL_arg2172z00_4357 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2173z00_4358,
																		BgL_arg2180z00_4322);
																}
																BgL_res2225z00_4337 =
																	(BgL_arg2172z00_4357 == BgL_classz00_4319);
															}
														else
															{	/* Eval/evaluate_fsize.scm 298 */
																BgL_res2225z00_4337 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2527z00_5261 = BgL_res2225z00_4337;
									}
							}
						else
							{	/* Eval/evaluate_fsize.scm 298 */
								BgL_test2527z00_5261 = ((bool_t) 0);
							}
					}
					if (BgL_test2527z00_5261)
						{	/* Eval/evaluate_fsize.scm 298 */
							BgL_auxz00_5260 = ((BgL_ev_exprz00_bglt) BgL_ez00_3840);
						}
					else
						{
							obj_t BgL_auxz00_5286;

							BgL_auxz00_5286 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(8471L),
								BGl_string2284z00zz__evaluate_fsiza7eza7,
								BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3840);
							FAILURE(BgL_auxz00_5286, BFALSE, BFALSE);
						}
				}
				return
					BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_auxz00_5260,
					BgL_varsz00_3841, BgL_lblsz00_3842);
			}
		}

	}



/* tailpos */
	obj_t BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt BgL_ez00_120,
		BgL_ev_varz00_bglt BgL_vz00_121)
	{
		{	/* Eval/evaluate_fsize.scm 403 */
			{	/* Eval/evaluate_fsize.scm 403 */
				obj_t BgL_method1392z00_1415;

				{	/* Eval/evaluate_fsize.scm 403 */
					obj_t BgL_res2218z00_2938;

					{	/* Eval/evaluate_fsize.scm 403 */
						long BgL_objzd2classzd2numz00_2909;

						BgL_objzd2classzd2numz00_2909 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_120));
						{	/* Eval/evaluate_fsize.scm 403 */
							obj_t BgL_arg2181z00_2910;

							BgL_arg2181z00_2910 =
								PROCEDURE_REF(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
								(int) (1L));
							{	/* Eval/evaluate_fsize.scm 403 */
								int BgL_offsetz00_2913;

								BgL_offsetz00_2913 = (int) (BgL_objzd2classzd2numz00_2909);
								{	/* Eval/evaluate_fsize.scm 403 */
									long BgL_offsetz00_2914;

									BgL_offsetz00_2914 =
										((long) (BgL_offsetz00_2913) - OBJECT_TYPE);
									{	/* Eval/evaluate_fsize.scm 403 */
										long BgL_modz00_2915;

										BgL_modz00_2915 =
											(BgL_offsetz00_2914 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_fsize.scm 403 */
											long BgL_restz00_2917;

											BgL_restz00_2917 =
												(BgL_offsetz00_2914 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_fsize.scm 403 */

												{	/* Eval/evaluate_fsize.scm 403 */
													obj_t BgL_bucketz00_2919;

													BgL_bucketz00_2919 =
														VECTOR_REF(
														((obj_t) BgL_arg2181z00_2910), BgL_modz00_2915);
													BgL_res2218z00_2938 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2919), BgL_restz00_2917);
					}}}}}}}}
					BgL_method1392z00_1415 = BgL_res2218z00_2938;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1392z00_1415,
					((obj_t) BgL_ez00_120), ((obj_t) BgL_vz00_121));
			}
		}

	}



/* &tailpos */
	obj_t BGl_z62tailposz62zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3843,
		obj_t BgL_ez00_3844, obj_t BgL_vz00_3845)
	{
		{	/* Eval/evaluate_fsize.scm 403 */
			{	/* Eval/evaluate_fsize.scm 403 */
				BgL_ev_varz00_bglt BgL_auxz00_5353;
				BgL_ev_exprz00_bglt BgL_auxz00_5323;

				{	/* Eval/evaluate_fsize.scm 403 */
					bool_t BgL_test2537z00_5354;

					{	/* Eval/evaluate_fsize.scm 403 */
						obj_t BgL_classz00_4399;

						BgL_classz00_4399 = BGl_ev_varz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_vz00_3845))
							{	/* Eval/evaluate_fsize.scm 403 */
								BgL_objectz00_bglt BgL_arg2179z00_4401;
								long BgL_arg2180z00_4402;

								BgL_arg2179z00_4401 = (BgL_objectz00_bglt) (BgL_vz00_3845);
								BgL_arg2180z00_4402 = BGL_CLASS_DEPTH(BgL_classz00_4399);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_fsize.scm 403 */
										long BgL_idxz00_4410;

										BgL_idxz00_4410 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4401);
										{	/* Eval/evaluate_fsize.scm 403 */
											obj_t BgL_arg2170z00_4411;

											{	/* Eval/evaluate_fsize.scm 403 */
												long BgL_arg2171z00_4412;

												BgL_arg2171z00_4412 =
													(BgL_idxz00_4410 + BgL_arg2180z00_4402);
												BgL_arg2170z00_4411 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2171z00_4412);
											}
											BgL_test2537z00_5354 =
												(BgL_arg2170z00_4411 == BgL_classz00_4399);
									}}
								else
									{	/* Eval/evaluate_fsize.scm 403 */
										bool_t BgL_res2225z00_4417;

										{	/* Eval/evaluate_fsize.scm 403 */
											obj_t BgL_oclassz00_4421;

											{	/* Eval/evaluate_fsize.scm 403 */
												obj_t BgL_arg2184z00_4423;
												long BgL_arg2185z00_4424;

												BgL_arg2184z00_4423 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_fsize.scm 403 */
													long BgL_arg2186z00_4425;

													BgL_arg2186z00_4425 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4401);
													BgL_arg2185z00_4424 =
														(BgL_arg2186z00_4425 - OBJECT_TYPE);
												}
												BgL_oclassz00_4421 =
													VECTOR_REF(BgL_arg2184z00_4423, BgL_arg2185z00_4424);
											}
											{	/* Eval/evaluate_fsize.scm 403 */
												bool_t BgL__ortest_1212z00_4431;

												BgL__ortest_1212z00_4431 =
													(BgL_classz00_4399 == BgL_oclassz00_4421);
												if (BgL__ortest_1212z00_4431)
													{	/* Eval/evaluate_fsize.scm 403 */
														BgL_res2225z00_4417 = BgL__ortest_1212z00_4431;
													}
												else
													{	/* Eval/evaluate_fsize.scm 403 */
														long BgL_odepthz00_4432;

														{	/* Eval/evaluate_fsize.scm 403 */
															obj_t BgL_arg2174z00_4433;

															BgL_arg2174z00_4433 = (BgL_oclassz00_4421);
															BgL_odepthz00_4432 =
																BGL_CLASS_DEPTH(BgL_arg2174z00_4433);
														}
														if ((BgL_arg2180z00_4402 < BgL_odepthz00_4432))
															{	/* Eval/evaluate_fsize.scm 403 */
																obj_t BgL_arg2172z00_4437;

																{	/* Eval/evaluate_fsize.scm 403 */
																	obj_t BgL_arg2173z00_4438;

																	BgL_arg2173z00_4438 = (BgL_oclassz00_4421);
																	BgL_arg2172z00_4437 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2173z00_4438,
																		BgL_arg2180z00_4402);
																}
																BgL_res2225z00_4417 =
																	(BgL_arg2172z00_4437 == BgL_classz00_4399);
															}
														else
															{	/* Eval/evaluate_fsize.scm 403 */
																BgL_res2225z00_4417 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2537z00_5354 = BgL_res2225z00_4417;
									}
							}
						else
							{	/* Eval/evaluate_fsize.scm 403 */
								BgL_test2537z00_5354 = ((bool_t) 0);
							}
					}
					if (BgL_test2537z00_5354)
						{	/* Eval/evaluate_fsize.scm 403 */
							BgL_auxz00_5353 = ((BgL_ev_varz00_bglt) BgL_vz00_3845);
						}
					else
						{
							obj_t BgL_auxz00_5379;

							BgL_auxz00_5379 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(12049L),
								BGl_string2285z00zz__evaluate_fsiza7eza7,
								BGl_string2286z00zz__evaluate_fsiza7eza7, BgL_vz00_3845);
							FAILURE(BgL_auxz00_5379, BFALSE, BFALSE);
						}
				}
				{	/* Eval/evaluate_fsize.scm 403 */
					bool_t BgL_test2532z00_5324;

					{	/* Eval/evaluate_fsize.scm 403 */
						obj_t BgL_classz00_4359;

						BgL_classz00_4359 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_3844))
							{	/* Eval/evaluate_fsize.scm 403 */
								BgL_objectz00_bglt BgL_arg2179z00_4361;
								long BgL_arg2180z00_4362;

								BgL_arg2179z00_4361 = (BgL_objectz00_bglt) (BgL_ez00_3844);
								BgL_arg2180z00_4362 = BGL_CLASS_DEPTH(BgL_classz00_4359);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_fsize.scm 403 */
										long BgL_idxz00_4370;

										BgL_idxz00_4370 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4361);
										{	/* Eval/evaluate_fsize.scm 403 */
											obj_t BgL_arg2170z00_4371;

											{	/* Eval/evaluate_fsize.scm 403 */
												long BgL_arg2171z00_4372;

												BgL_arg2171z00_4372 =
													(BgL_idxz00_4370 + BgL_arg2180z00_4362);
												BgL_arg2170z00_4371 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2171z00_4372);
											}
											BgL_test2532z00_5324 =
												(BgL_arg2170z00_4371 == BgL_classz00_4359);
									}}
								else
									{	/* Eval/evaluate_fsize.scm 403 */
										bool_t BgL_res2225z00_4377;

										{	/* Eval/evaluate_fsize.scm 403 */
											obj_t BgL_oclassz00_4381;

											{	/* Eval/evaluate_fsize.scm 403 */
												obj_t BgL_arg2184z00_4383;
												long BgL_arg2185z00_4384;

												BgL_arg2184z00_4383 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_fsize.scm 403 */
													long BgL_arg2186z00_4385;

													BgL_arg2186z00_4385 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4361);
													BgL_arg2185z00_4384 =
														(BgL_arg2186z00_4385 - OBJECT_TYPE);
												}
												BgL_oclassz00_4381 =
													VECTOR_REF(BgL_arg2184z00_4383, BgL_arg2185z00_4384);
											}
											{	/* Eval/evaluate_fsize.scm 403 */
												bool_t BgL__ortest_1212z00_4391;

												BgL__ortest_1212z00_4391 =
													(BgL_classz00_4359 == BgL_oclassz00_4381);
												if (BgL__ortest_1212z00_4391)
													{	/* Eval/evaluate_fsize.scm 403 */
														BgL_res2225z00_4377 = BgL__ortest_1212z00_4391;
													}
												else
													{	/* Eval/evaluate_fsize.scm 403 */
														long BgL_odepthz00_4392;

														{	/* Eval/evaluate_fsize.scm 403 */
															obj_t BgL_arg2174z00_4393;

															BgL_arg2174z00_4393 = (BgL_oclassz00_4381);
															BgL_odepthz00_4392 =
																BGL_CLASS_DEPTH(BgL_arg2174z00_4393);
														}
														if ((BgL_arg2180z00_4362 < BgL_odepthz00_4392))
															{	/* Eval/evaluate_fsize.scm 403 */
																obj_t BgL_arg2172z00_4397;

																{	/* Eval/evaluate_fsize.scm 403 */
																	obj_t BgL_arg2173z00_4398;

																	BgL_arg2173z00_4398 = (BgL_oclassz00_4381);
																	BgL_arg2172z00_4397 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2173z00_4398,
																		BgL_arg2180z00_4362);
																}
																BgL_res2225z00_4377 =
																	(BgL_arg2172z00_4397 == BgL_classz00_4359);
															}
														else
															{	/* Eval/evaluate_fsize.scm 403 */
																BgL_res2225z00_4377 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2532z00_5324 = BgL_res2225z00_4377;
									}
							}
						else
							{	/* Eval/evaluate_fsize.scm 403 */
								BgL_test2532z00_5324 = ((bool_t) 0);
							}
					}
					if (BgL_test2532z00_5324)
						{	/* Eval/evaluate_fsize.scm 403 */
							BgL_auxz00_5323 = ((BgL_ev_exprz00_bglt) BgL_ez00_3844);
						}
					else
						{
							obj_t BgL_auxz00_5349;

							BgL_auxz00_5349 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(12049L),
								BGl_string2285z00zz__evaluate_fsiza7eza7,
								BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3844);
							FAILURE(BgL_auxz00_5349, BFALSE, BFALSE);
						}
				}
				return
					BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_auxz00_5323,
					BgL_auxz00_5353);
			}
		}

	}



/* hasvar? */
	obj_t BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt
		BgL_ez00_160, BgL_ev_varz00_bglt BgL_vz00_161)
	{
		{	/* Eval/evaluate_fsize.scm 491 */
			{	/* Eval/evaluate_fsize.scm 491 */
				obj_t BgL_method1439z00_1416;

				{	/* Eval/evaluate_fsize.scm 491 */
					obj_t BgL_res2223z00_2969;

					{	/* Eval/evaluate_fsize.scm 491 */
						long BgL_objzd2classzd2numz00_2940;

						BgL_objzd2classzd2numz00_2940 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_160));
						{	/* Eval/evaluate_fsize.scm 491 */
							obj_t BgL_arg2181z00_2941;

							BgL_arg2181z00_2941 =
								PROCEDURE_REF(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
								(int) (1L));
							{	/* Eval/evaluate_fsize.scm 491 */
								int BgL_offsetz00_2944;

								BgL_offsetz00_2944 = (int) (BgL_objzd2classzd2numz00_2940);
								{	/* Eval/evaluate_fsize.scm 491 */
									long BgL_offsetz00_2945;

									BgL_offsetz00_2945 =
										((long) (BgL_offsetz00_2944) - OBJECT_TYPE);
									{	/* Eval/evaluate_fsize.scm 491 */
										long BgL_modz00_2946;

										BgL_modz00_2946 =
											(BgL_offsetz00_2945 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_fsize.scm 491 */
											long BgL_restz00_2948;

											BgL_restz00_2948 =
												(BgL_offsetz00_2945 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_fsize.scm 491 */

												{	/* Eval/evaluate_fsize.scm 491 */
													obj_t BgL_bucketz00_2950;

													BgL_bucketz00_2950 =
														VECTOR_REF(
														((obj_t) BgL_arg2181z00_2941), BgL_modz00_2946);
													BgL_res2223z00_2969 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2950), BgL_restz00_2948);
					}}}}}}}}
					BgL_method1439z00_1416 = BgL_res2223z00_2969;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1439z00_1416,
					((obj_t) BgL_ez00_160), ((obj_t) BgL_vz00_161));
			}
		}

	}



/* &hasvar? */
	obj_t BGl_z62hasvarzf3z91zz__evaluate_fsiza7eza7(obj_t BgL_envz00_3846,
		obj_t BgL_ez00_3847, obj_t BgL_vz00_3848)
	{
		{	/* Eval/evaluate_fsize.scm 491 */
			{	/* Eval/evaluate_fsize.scm 491 */
				BgL_ev_varz00_bglt BgL_auxz00_5446;
				BgL_ev_exprz00_bglt BgL_auxz00_5416;

				{	/* Eval/evaluate_fsize.scm 491 */
					bool_t BgL_test2547z00_5447;

					{	/* Eval/evaluate_fsize.scm 491 */
						obj_t BgL_classz00_4479;

						BgL_classz00_4479 = BGl_ev_varz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_vz00_3848))
							{	/* Eval/evaluate_fsize.scm 491 */
								BgL_objectz00_bglt BgL_arg2179z00_4481;
								long BgL_arg2180z00_4482;

								BgL_arg2179z00_4481 = (BgL_objectz00_bglt) (BgL_vz00_3848);
								BgL_arg2180z00_4482 = BGL_CLASS_DEPTH(BgL_classz00_4479);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_fsize.scm 491 */
										long BgL_idxz00_4490;

										BgL_idxz00_4490 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4481);
										{	/* Eval/evaluate_fsize.scm 491 */
											obj_t BgL_arg2170z00_4491;

											{	/* Eval/evaluate_fsize.scm 491 */
												long BgL_arg2171z00_4492;

												BgL_arg2171z00_4492 =
													(BgL_idxz00_4490 + BgL_arg2180z00_4482);
												BgL_arg2170z00_4491 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2171z00_4492);
											}
											BgL_test2547z00_5447 =
												(BgL_arg2170z00_4491 == BgL_classz00_4479);
									}}
								else
									{	/* Eval/evaluate_fsize.scm 491 */
										bool_t BgL_res2225z00_4497;

										{	/* Eval/evaluate_fsize.scm 491 */
											obj_t BgL_oclassz00_4501;

											{	/* Eval/evaluate_fsize.scm 491 */
												obj_t BgL_arg2184z00_4503;
												long BgL_arg2185z00_4504;

												BgL_arg2184z00_4503 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_fsize.scm 491 */
													long BgL_arg2186z00_4505;

													BgL_arg2186z00_4505 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4481);
													BgL_arg2185z00_4504 =
														(BgL_arg2186z00_4505 - OBJECT_TYPE);
												}
												BgL_oclassz00_4501 =
													VECTOR_REF(BgL_arg2184z00_4503, BgL_arg2185z00_4504);
											}
											{	/* Eval/evaluate_fsize.scm 491 */
												bool_t BgL__ortest_1212z00_4511;

												BgL__ortest_1212z00_4511 =
													(BgL_classz00_4479 == BgL_oclassz00_4501);
												if (BgL__ortest_1212z00_4511)
													{	/* Eval/evaluate_fsize.scm 491 */
														BgL_res2225z00_4497 = BgL__ortest_1212z00_4511;
													}
												else
													{	/* Eval/evaluate_fsize.scm 491 */
														long BgL_odepthz00_4512;

														{	/* Eval/evaluate_fsize.scm 491 */
															obj_t BgL_arg2174z00_4513;

															BgL_arg2174z00_4513 = (BgL_oclassz00_4501);
															BgL_odepthz00_4512 =
																BGL_CLASS_DEPTH(BgL_arg2174z00_4513);
														}
														if ((BgL_arg2180z00_4482 < BgL_odepthz00_4512))
															{	/* Eval/evaluate_fsize.scm 491 */
																obj_t BgL_arg2172z00_4517;

																{	/* Eval/evaluate_fsize.scm 491 */
																	obj_t BgL_arg2173z00_4518;

																	BgL_arg2173z00_4518 = (BgL_oclassz00_4501);
																	BgL_arg2172z00_4517 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2173z00_4518,
																		BgL_arg2180z00_4482);
																}
																BgL_res2225z00_4497 =
																	(BgL_arg2172z00_4517 == BgL_classz00_4479);
															}
														else
															{	/* Eval/evaluate_fsize.scm 491 */
																BgL_res2225z00_4497 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2547z00_5447 = BgL_res2225z00_4497;
									}
							}
						else
							{	/* Eval/evaluate_fsize.scm 491 */
								BgL_test2547z00_5447 = ((bool_t) 0);
							}
					}
					if (BgL_test2547z00_5447)
						{	/* Eval/evaluate_fsize.scm 491 */
							BgL_auxz00_5446 = ((BgL_ev_varz00_bglt) BgL_vz00_3848);
						}
					else
						{
							obj_t BgL_auxz00_5472;

							BgL_auxz00_5472 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(14969L),
								BGl_string2287z00zz__evaluate_fsiza7eza7,
								BGl_string2286z00zz__evaluate_fsiza7eza7, BgL_vz00_3848);
							FAILURE(BgL_auxz00_5472, BFALSE, BFALSE);
						}
				}
				{	/* Eval/evaluate_fsize.scm 491 */
					bool_t BgL_test2542z00_5417;

					{	/* Eval/evaluate_fsize.scm 491 */
						obj_t BgL_classz00_4439;

						BgL_classz00_4439 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_3847))
							{	/* Eval/evaluate_fsize.scm 491 */
								BgL_objectz00_bglt BgL_arg2179z00_4441;
								long BgL_arg2180z00_4442;

								BgL_arg2179z00_4441 = (BgL_objectz00_bglt) (BgL_ez00_3847);
								BgL_arg2180z00_4442 = BGL_CLASS_DEPTH(BgL_classz00_4439);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_fsize.scm 491 */
										long BgL_idxz00_4450;

										BgL_idxz00_4450 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2179z00_4441);
										{	/* Eval/evaluate_fsize.scm 491 */
											obj_t BgL_arg2170z00_4451;

											{	/* Eval/evaluate_fsize.scm 491 */
												long BgL_arg2171z00_4452;

												BgL_arg2171z00_4452 =
													(BgL_idxz00_4450 + BgL_arg2180z00_4442);
												BgL_arg2170z00_4451 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2171z00_4452);
											}
											BgL_test2542z00_5417 =
												(BgL_arg2170z00_4451 == BgL_classz00_4439);
									}}
								else
									{	/* Eval/evaluate_fsize.scm 491 */
										bool_t BgL_res2225z00_4457;

										{	/* Eval/evaluate_fsize.scm 491 */
											obj_t BgL_oclassz00_4461;

											{	/* Eval/evaluate_fsize.scm 491 */
												obj_t BgL_arg2184z00_4463;
												long BgL_arg2185z00_4464;

												BgL_arg2184z00_4463 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_fsize.scm 491 */
													long BgL_arg2186z00_4465;

													BgL_arg2186z00_4465 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2179z00_4441);
													BgL_arg2185z00_4464 =
														(BgL_arg2186z00_4465 - OBJECT_TYPE);
												}
												BgL_oclassz00_4461 =
													VECTOR_REF(BgL_arg2184z00_4463, BgL_arg2185z00_4464);
											}
											{	/* Eval/evaluate_fsize.scm 491 */
												bool_t BgL__ortest_1212z00_4471;

												BgL__ortest_1212z00_4471 =
													(BgL_classz00_4439 == BgL_oclassz00_4461);
												if (BgL__ortest_1212z00_4471)
													{	/* Eval/evaluate_fsize.scm 491 */
														BgL_res2225z00_4457 = BgL__ortest_1212z00_4471;
													}
												else
													{	/* Eval/evaluate_fsize.scm 491 */
														long BgL_odepthz00_4472;

														{	/* Eval/evaluate_fsize.scm 491 */
															obj_t BgL_arg2174z00_4473;

															BgL_arg2174z00_4473 = (BgL_oclassz00_4461);
															BgL_odepthz00_4472 =
																BGL_CLASS_DEPTH(BgL_arg2174z00_4473);
														}
														if ((BgL_arg2180z00_4442 < BgL_odepthz00_4472))
															{	/* Eval/evaluate_fsize.scm 491 */
																obj_t BgL_arg2172z00_4477;

																{	/* Eval/evaluate_fsize.scm 491 */
																	obj_t BgL_arg2173z00_4478;

																	BgL_arg2173z00_4478 = (BgL_oclassz00_4461);
																	BgL_arg2172z00_4477 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2173z00_4478,
																		BgL_arg2180z00_4442);
																}
																BgL_res2225z00_4457 =
																	(BgL_arg2172z00_4477 == BgL_classz00_4439);
															}
														else
															{	/* Eval/evaluate_fsize.scm 491 */
																BgL_res2225z00_4457 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2542z00_5417 = BgL_res2225z00_4457;
									}
							}
						else
							{	/* Eval/evaluate_fsize.scm 491 */
								BgL_test2542z00_5417 = ((bool_t) 0);
							}
					}
					if (BgL_test2542z00_5417)
						{	/* Eval/evaluate_fsize.scm 491 */
							BgL_auxz00_5416 = ((BgL_ev_exprz00_bglt) BgL_ez00_3847);
						}
					else
						{
							obj_t BgL_auxz00_5442;

							BgL_auxz00_5442 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2256z00zz__evaluate_fsiza7eza7, BINT(14969L),
								BGl_string2287z00zz__evaluate_fsiza7eza7,
								BGl_string2258z00zz__evaluate_fsiza7eza7, BgL_ez00_3847);
							FAILURE(BgL_auxz00_5442, BFALSE, BFALSE);
						}
				}
				return
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_auxz00_5416,
					BgL_auxz00_5446);
			}
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_varz00zz__evaluate_typesz00,
				BGl_proc2288z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc2290z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc2291z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc2292z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_listz00zz__evaluate_typesz00,
				BGl_proc2293z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc2294z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_hookz00zz__evaluate_typesz00,
				BGl_proc2295z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc2296z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc2297z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc2298z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc2299z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_letz00zz__evaluate_typesz00,
				BGl_proc2300z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_letza2za2zz__evaluate_typesz00,
				BGl_proc2301z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_letrecz00zz__evaluate_typesz00,
				BGl_proc2302z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc2303z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_gotoz00zz__evaluate_typesz00,
				BGl_proc2304z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_appz00zz__evaluate_typesz00,
				BGl_proc2305z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_fsiza7ezd2envz75zz__evaluate_fsiza7eza7,
				BGl_ev_absz00zz__evaluate_typesz00,
				BGl_proc2306z00zz__evaluate_fsiza7eza7,
				BGl_string2289z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_varz00zz__evaluate_typesz00,
				BGl_proc2307z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc2308z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc2309z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc2310z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_listz00zz__evaluate_typesz00,
				BGl_proc2311z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc2312z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_hookz00zz__evaluate_typesz00,
				BGl_proc2313z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc2314z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc2315z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc2316z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc2317z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_binderz00zz__evaluate_typesz00,
				BGl_proc2318z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_letrecz00zz__evaluate_typesz00,
				BGl_proc2319z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_appz00zz__evaluate_typesz00,
				BGl_proc2320z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_searchzd2letreczd2envz00zz__evaluate_fsiza7eza7,
				BGl_ev_absz00zz__evaluate_typesz00,
				BGl_proc2321z00zz__evaluate_fsiza7eza7,
				BGl_string2278z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_varz00zz__evaluate_typesz00,
				BGl_proc2322z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc2323z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc2324z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc2325z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_listz00zz__evaluate_typesz00,
				BGl_proc2326z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc2327z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_hookz00zz__evaluate_typesz00,
				BGl_proc2328z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc2329z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc2330z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc2331z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc2332z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_binderz00zz__evaluate_typesz00,
				BGl_proc2333z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc2334z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_gotoz00zz__evaluate_typesz00,
				BGl_proc2335z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_appz00zz__evaluate_typesz00,
				BGl_proc2336z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_subst_gotozd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_absz00zz__evaluate_typesz00,
				BGl_proc2337z00zz__evaluate_fsiza7eza7,
				BGl_string2276z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_varz00zz__evaluate_typesz00,
				BGl_proc2338z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc2339z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc2340z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc2341z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_listz00zz__evaluate_typesz00,
				BGl_proc2342z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc2343z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_hookz00zz__evaluate_typesz00,
				BGl_proc2344z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_setlocalz00zz__evaluate_typesz00,
				BGl_proc2345z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc2346z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc2347z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc2348z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc2349z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_letz00zz__evaluate_typesz00,
				BGl_proc2350z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_letza2za2zz__evaluate_typesz00,
				BGl_proc2351z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_letrecz00zz__evaluate_typesz00,
				BGl_proc2352z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc2353z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_gotoz00zz__evaluate_typesz00,
				BGl_proc2354z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_appz00zz__evaluate_typesz00,
				BGl_proc2355z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_tailposzd2envzd2zz__evaluate_fsiza7eza7,
				BGl_ev_absz00zz__evaluate_typesz00,
				BGl_proc2356z00zz__evaluate_fsiza7eza7,
				BGl_string2273z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_varz00zz__evaluate_typesz00,
				BGl_proc2357z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc2359z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc2360z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc2361z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_listz00zz__evaluate_typesz00,
				BGl_proc2362z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc2363z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_hookz00zz__evaluate_typesz00,
				BGl_proc2364z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_setlocalz00zz__evaluate_typesz00,
				BGl_proc2365z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc2366z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc2367z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc2368z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc2369z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_binderz00zz__evaluate_typesz00,
				BGl_proc2370z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc2371z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_gotoz00zz__evaluate_typesz00,
				BGl_proc2372z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_appz00zz__evaluate_typesz00,
				BGl_proc2373z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hasvarzf3zd2envz21zz__evaluate_fsiza7eza7,
				BGl_ev_absz00zz__evaluate_typesz00,
				BGl_proc2374z00zz__evaluate_fsiza7eza7,
				BGl_string2358z00zz__evaluate_fsiza7eza7);
		}

	}



/* &hasvar?-ev_abs1474 */
	obj_t BGl_z62hasvarzf3zd2ev_abs1474z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3934, obj_t BgL_ez00_3935, obj_t BgL_vz00_3936)
	{
		{	/* Eval/evaluate_fsize.scm 556 */
			return
				BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
				(((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_ez00_3935)))->BgL_bodyz00),
				((BgL_ev_varz00_bglt) BgL_vz00_3936));
		}

	}



/* &hasvar?-ev_app1472 */
	obj_t BGl_z62hasvarzf3zd2ev_app1472z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3937, obj_t BgL_ez00_3938, obj_t BgL_vz00_3939)
	{
		{	/* Eval/evaluate_fsize.scm 552 */
			{	/* Eval/evaluate_fsize.scm 554 */
				obj_t BgL__ortest_1172z00_4523;

				BgL__ortest_1172z00_4523 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_3938)))->BgL_funz00),
					((BgL_ev_varz00_bglt) BgL_vz00_3939));
				if (CBOOL(BgL__ortest_1172z00_4523))
					{	/* Eval/evaluate_fsize.scm 554 */
						return BgL__ortest_1172z00_4523;
					}
				else
					{	/* Eval/evaluate_fsize.scm 554 */
						obj_t BgL_g1268z00_4524;

						BgL_g1268z00_4524 =
							(((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_3938)))->BgL_argsz00);
						{
							obj_t BgL_l1266z00_4526;

							BgL_l1266z00_4526 = BgL_g1268z00_4524;
						BgL_zc3z04anonymousza31877ze3z87_4525:
							if (NULLP(BgL_l1266z00_4526))
								{	/* Eval/evaluate_fsize.scm 554 */
									return BFALSE;
								}
							else
								{	/* Eval/evaluate_fsize.scm 554 */
									obj_t BgL__ortest_1269z00_4527;

									{	/* Eval/evaluate_fsize.scm 554 */
										obj_t BgL_ez00_4528;

										BgL_ez00_4528 = CAR(((obj_t) BgL_l1266z00_4526));
										BgL__ortest_1269z00_4527 =
											BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_ez00_4528),
											((BgL_ev_varz00_bglt) BgL_vz00_3939));
									}
									if (CBOOL(BgL__ortest_1269z00_4527))
										{	/* Eval/evaluate_fsize.scm 554 */
											return BgL__ortest_1269z00_4527;
										}
									else
										{	/* Eval/evaluate_fsize.scm 554 */
											obj_t BgL_arg1879z00_4529;

											BgL_arg1879z00_4529 = CDR(((obj_t) BgL_l1266z00_4526));
											{
												obj_t BgL_l1266z00_5585;

												BgL_l1266z00_5585 = BgL_arg1879z00_4529;
												BgL_l1266z00_4526 = BgL_l1266z00_5585;
												goto BgL_zc3z04anonymousza31877ze3z87_4525;
											}
										}
								}
						}
					}
			}
		}

	}



/* &hasvar?-ev_goto1470 */
	obj_t BGl_z62hasvarzf3zd2ev_goto1470z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3940, obj_t BgL_ez00_3941, obj_t BgL_vz00_3942)
	{
		{	/* Eval/evaluate_fsize.scm 548 */
			{	/* Eval/evaluate_fsize.scm 550 */
				obj_t BgL_g1264z00_4532;

				BgL_g1264z00_4532 =
					(((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_ez00_3941)))->BgL_argsz00);
				{
					obj_t BgL_l1262z00_4534;

					BgL_l1262z00_4534 = BgL_g1264z00_4532;
				BgL_zc3z04anonymousza31874ze3z87_4533:
					if (NULLP(BgL_l1262z00_4534))
						{	/* Eval/evaluate_fsize.scm 550 */
							return BFALSE;
						}
					else
						{	/* Eval/evaluate_fsize.scm 550 */
							obj_t BgL__ortest_1265z00_4535;

							{	/* Eval/evaluate_fsize.scm 550 */
								obj_t BgL_ez00_4536;

								BgL_ez00_4536 = CAR(((obj_t) BgL_l1262z00_4534));
								BgL__ortest_1265z00_4535 =
									BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
									((BgL_ev_exprz00_bglt) BgL_ez00_4536),
									((BgL_ev_varz00_bglt) BgL_vz00_3942));
							}
							if (CBOOL(BgL__ortest_1265z00_4535))
								{	/* Eval/evaluate_fsize.scm 550 */
									return BgL__ortest_1265z00_4535;
								}
							else
								{	/* Eval/evaluate_fsize.scm 550 */
									obj_t BgL_arg1876z00_4537;

									BgL_arg1876z00_4537 = CDR(((obj_t) BgL_l1262z00_4534));
									{
										obj_t BgL_l1262z00_5599;

										BgL_l1262z00_5599 = BgL_arg1876z00_4537;
										BgL_l1262z00_4534 = BgL_l1262z00_5599;
										goto BgL_zc3z04anonymousza31874ze3z87_4533;
									}
								}
						}
				}
			}
		}

	}



/* &hasvar?-ev_labels1468 */
	obj_t BGl_z62hasvarzf3zd2ev_labels1468z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3943, obj_t BgL_ez00_3944, obj_t BgL_vz00_3945)
	{
		{	/* Eval/evaluate_fsize.scm 543 */
			{	/* Eval/evaluate_fsize.scm 545 */
				obj_t BgL__ortest_1169z00_4540;

				{	/* Eval/evaluate_fsize.scm 545 */
					obj_t BgL_g1260z00_4541;

					BgL_g1260z00_4541 =
						(((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_ez00_3944)))->BgL_valsz00);
					{
						obj_t BgL_l1258z00_4543;

						BgL_l1258z00_4543 = BgL_g1260z00_4541;
					BgL_zc3z04anonymousza31870ze3z87_4542:
						if (NULLP(BgL_l1258z00_4543))
							{	/* Eval/evaluate_fsize.scm 545 */
								BgL__ortest_1169z00_4540 = BFALSE;
							}
						else
							{	/* Eval/evaluate_fsize.scm 545 */
								obj_t BgL__ortest_1261z00_4544;

								{	/* Eval/evaluate_fsize.scm 545 */
									obj_t BgL_ez00_4545;

									BgL_ez00_4545 = CAR(((obj_t) BgL_l1258z00_4543));
									{	/* Eval/evaluate_fsize.scm 545 */
										obj_t BgL_arg1873z00_4546;

										BgL_arg1873z00_4546 = CDR(((obj_t) BgL_ez00_4545));
										BgL__ortest_1261z00_4544 =
											BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1873z00_4546),
											((BgL_ev_varz00_bglt) BgL_vz00_3945));
									}
								}
								if (CBOOL(BgL__ortest_1261z00_4544))
									{	/* Eval/evaluate_fsize.scm 545 */
										BgL__ortest_1169z00_4540 = BgL__ortest_1261z00_4544;
									}
								else
									{	/* Eval/evaluate_fsize.scm 545 */
										obj_t BgL_arg1872z00_4547;

										BgL_arg1872z00_4547 = CDR(((obj_t) BgL_l1258z00_4543));
										{
											obj_t BgL_l1258z00_5615;

											BgL_l1258z00_5615 = BgL_arg1872z00_4547;
											BgL_l1258z00_4543 = BgL_l1258z00_5615;
											goto BgL_zc3z04anonymousza31870ze3z87_4542;
										}
									}
							}
					}
				}
				if (CBOOL(BgL__ortest_1169z00_4540))
					{	/* Eval/evaluate_fsize.scm 545 */
						return BgL__ortest_1169z00_4540;
					}
				else
					{	/* Eval/evaluate_fsize.scm 545 */
						return
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_labelsz00_bglt) COBJECT(
										((BgL_ev_labelsz00_bglt) BgL_ez00_3944)))->BgL_bodyz00),
							((BgL_ev_varz00_bglt) BgL_vz00_3945));
					}
			}
		}

	}



/* &hasvar?-ev_binder1466 */
	obj_t BGl_z62hasvarzf3zd2ev_binder1466z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3946, obj_t BgL_ez00_3947, obj_t BgL_vz00_3948)
	{
		{	/* Eval/evaluate_fsize.scm 538 */
			{	/* Eval/evaluate_fsize.scm 540 */
				obj_t BgL__ortest_1167z00_4550;

				{	/* Eval/evaluate_fsize.scm 540 */
					obj_t BgL_g1256z00_4551;

					BgL_g1256z00_4551 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_ez00_3947)))->BgL_valsz00);
					{
						obj_t BgL_l1254z00_4553;

						BgL_l1254z00_4553 = BgL_g1256z00_4551;
					BgL_zc3z04anonymousza31865ze3z87_4552:
						if (NULLP(BgL_l1254z00_4553))
							{	/* Eval/evaluate_fsize.scm 540 */
								BgL__ortest_1167z00_4550 = BFALSE;
							}
						else
							{	/* Eval/evaluate_fsize.scm 540 */
								obj_t BgL__ortest_1257z00_4554;

								{	/* Eval/evaluate_fsize.scm 540 */
									obj_t BgL_ez00_4555;

									BgL_ez00_4555 = CAR(((obj_t) BgL_l1254z00_4553));
									BgL__ortest_1257z00_4554 =
										BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
										((BgL_ev_exprz00_bglt) BgL_ez00_4555),
										((BgL_ev_varz00_bglt) BgL_vz00_3948));
								}
								if (CBOOL(BgL__ortest_1257z00_4554))
									{	/* Eval/evaluate_fsize.scm 540 */
										BgL__ortest_1167z00_4550 = BgL__ortest_1257z00_4554;
									}
								else
									{	/* Eval/evaluate_fsize.scm 540 */
										obj_t BgL_arg1868z00_4556;

										BgL_arg1868z00_4556 = CDR(((obj_t) BgL_l1254z00_4553));
										{
											obj_t BgL_l1254z00_5635;

											BgL_l1254z00_5635 = BgL_arg1868z00_4556;
											BgL_l1254z00_4553 = BgL_l1254z00_5635;
											goto BgL_zc3z04anonymousza31865ze3z87_4552;
										}
									}
							}
					}
				}
				if (CBOOL(BgL__ortest_1167z00_4550))
					{	/* Eval/evaluate_fsize.scm 540 */
						return BgL__ortest_1167z00_4550;
					}
				else
					{	/* Eval/evaluate_fsize.scm 540 */
						return
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt) BgL_ez00_3947)))->BgL_bodyz00),
							((BgL_ev_varz00_bglt) BgL_vz00_3948));
					}
			}
		}

	}



/* &hasvar?-ev_synchroni1464 */
	obj_t BGl_z62hasvarzf3zd2ev_synchroni1464z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3949, obj_t BgL_ez00_3950, obj_t BgL_vz00_3951)
	{
		{	/* Eval/evaluate_fsize.scm 534 */
			{	/* Eval/evaluate_fsize.scm 536 */
				obj_t BgL__ortest_1164z00_4559;

				BgL__ortest_1164z00_4559 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
								((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_3950)))->BgL_mutexz00),
					((BgL_ev_varz00_bglt) BgL_vz00_3951));
				if (CBOOL(BgL__ortest_1164z00_4559))
					{	/* Eval/evaluate_fsize.scm 536 */
						return BgL__ortest_1164z00_4559;
					}
				else
					{	/* Eval/evaluate_fsize.scm 536 */
						obj_t BgL__ortest_1165z00_4560;

						BgL__ortest_1165z00_4560 =
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
										((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_3950)))->
								BgL_prelockz00), ((BgL_ev_varz00_bglt) BgL_vz00_3951));
						if (CBOOL(BgL__ortest_1165z00_4560))
							{	/* Eval/evaluate_fsize.scm 536 */
								return BgL__ortest_1165z00_4560;
							}
						else
							{	/* Eval/evaluate_fsize.scm 536 */
								return
									BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
									(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
												((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_3950)))->
										BgL_bodyz00), ((BgL_ev_varz00_bglt) BgL_vz00_3951));
							}
					}
			}
		}

	}



/* &hasvar?-ev_with-hand1462 */
	obj_t BGl_z62hasvarzf3zd2ev_withzd2hand1462z91zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3952, obj_t BgL_ez00_3953, obj_t BgL_vz00_3954)
	{
		{	/* Eval/evaluate_fsize.scm 530 */
			{	/* Eval/evaluate_fsize.scm 532 */
				obj_t BgL__ortest_1162z00_4563;

				BgL__ortest_1162z00_4563 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
								((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_3953)))->
						BgL_handlerz00), ((BgL_ev_varz00_bglt) BgL_vz00_3954));
				if (CBOOL(BgL__ortest_1162z00_4563))
					{	/* Eval/evaluate_fsize.scm 532 */
						return BgL__ortest_1162z00_4563;
					}
				else
					{	/* Eval/evaluate_fsize.scm 532 */
						return
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
										((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_3953)))->
								BgL_bodyz00), ((BgL_ev_varz00_bglt) BgL_vz00_3954));
					}
			}
		}

	}



/* &hasvar?-ev_unwind-pr1460 */
	obj_t BGl_z62hasvarzf3zd2ev_unwindzd2pr1460z91zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3955, obj_t BgL_ez00_3956, obj_t BgL_vz00_3957)
	{
		{	/* Eval/evaluate_fsize.scm 526 */
			{	/* Eval/evaluate_fsize.scm 528 */
				obj_t BgL__ortest_1160z00_4566;

				BgL__ortest_1160z00_4566 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
								((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_3956)))->BgL_ez00),
					((BgL_ev_varz00_bglt) BgL_vz00_3957));
				if (CBOOL(BgL__ortest_1160z00_4566))
					{	/* Eval/evaluate_fsize.scm 528 */
						return BgL__ortest_1160z00_4566;
					}
				else
					{	/* Eval/evaluate_fsize.scm 528 */
						return
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
										((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_3956)))->
								BgL_bodyz00), ((BgL_ev_varz00_bglt) BgL_vz00_3957));
					}
			}
		}

	}



/* &hasvar?-ev_bind-exit1458 */
	obj_t BGl_z62hasvarzf3zd2ev_bindzd2exit1458z91zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3958, obj_t BgL_ez00_3959, obj_t BgL_vz00_3960)
	{
		{	/* Eval/evaluate_fsize.scm 522 */
			return
				BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
				(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
							((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_3959)))->BgL_bodyz00),
				((BgL_ev_varz00_bglt) BgL_vz00_3960));
		}

	}



/* &hasvar?-ev_setlocal1456 */
	obj_t BGl_z62hasvarzf3zd2ev_setlocal1456z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3961, obj_t BgL_ez00_3962, obj_t BgL_varz00_3963)
	{
		{	/* Eval/evaluate_fsize.scm 518 */
			{	/* Eval/evaluate_fsize.scm 520 */
				bool_t BgL__ortest_1157z00_4571;

				BgL__ortest_1157z00_4571 =
					(
					((obj_t)
						(((BgL_ev_setlocalz00_bglt) COBJECT(
									((BgL_ev_setlocalz00_bglt) BgL_ez00_3962)))->BgL_vz00)) ==
					((obj_t) ((BgL_ev_varz00_bglt) BgL_varz00_3963)));
				if (BgL__ortest_1157z00_4571)
					{	/* Eval/evaluate_fsize.scm 520 */
						return BBOOL(BgL__ortest_1157z00_4571);
					}
				else
					{	/* Eval/evaluate_fsize.scm 520 */
						BgL_ev_exprz00_bglt BgL_arg1852z00_4572;

						BgL_arg1852z00_4572 =
							(((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt)
										((BgL_ev_setlocalz00_bglt) BgL_ez00_3962))))->BgL_ez00);
						return
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_arg1852z00_4572,
							((BgL_ev_varz00_bglt) BgL_varz00_3963));
					}
			}
		}

	}



/* &hasvar?-ev_hook1454 */
	obj_t BGl_z62hasvarzf3zd2ev_hook1454z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3964, obj_t BgL_ez00_3965, obj_t BgL_vz00_3966)
	{
		{	/* Eval/evaluate_fsize.scm 514 */
			return
				BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
				(((BgL_ev_hookz00_bglt) COBJECT(
							((BgL_ev_hookz00_bglt) BgL_ez00_3965)))->BgL_ez00),
				((BgL_ev_varz00_bglt) BgL_vz00_3966));
		}

	}



/* &hasvar?-ev_prog21452 */
	obj_t BGl_z62hasvarzf3zd2ev_prog21452z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3967, obj_t BgL_ez00_3968, obj_t BgL_vz00_3969)
	{
		{	/* Eval/evaluate_fsize.scm 510 */
			{	/* Eval/evaluate_fsize.scm 512 */
				obj_t BgL__ortest_1154z00_4577;

				BgL__ortest_1154z00_4577 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_prog2z00_bglt) COBJECT(
								((BgL_ev_prog2z00_bglt) BgL_ez00_3968)))->BgL_e1z00),
					((BgL_ev_varz00_bglt) BgL_vz00_3969));
				if (CBOOL(BgL__ortest_1154z00_4577))
					{	/* Eval/evaluate_fsize.scm 512 */
						return BgL__ortest_1154z00_4577;
					}
				else
					{	/* Eval/evaluate_fsize.scm 512 */
						return
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_prog2z00_bglt) COBJECT(
										((BgL_ev_prog2z00_bglt) BgL_ez00_3968)))->BgL_e2z00),
							((BgL_ev_varz00_bglt) BgL_vz00_3969));
					}
			}
		}

	}



/* &hasvar?-ev_list1449 */
	obj_t BGl_z62hasvarzf3zd2ev_list1449z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3970, obj_t BgL_ez00_3971, obj_t BgL_vz00_3972)
	{
		{	/* Eval/evaluate_fsize.scm 506 */
			{	/* Eval/evaluate_fsize.scm 508 */
				obj_t BgL_g1252z00_4580;

				BgL_g1252z00_4580 =
					(((BgL_ev_listz00_bglt) COBJECT(
							((BgL_ev_listz00_bglt) BgL_ez00_3971)))->BgL_argsz00);
				{
					obj_t BgL_l1250z00_4582;

					BgL_l1250z00_4582 = BgL_g1252z00_4580;
				BgL_zc3z04anonymousza31846ze3z87_4581:
					if (NULLP(BgL_l1250z00_4582))
						{	/* Eval/evaluate_fsize.scm 508 */
							return BFALSE;
						}
					else
						{	/* Eval/evaluate_fsize.scm 508 */
							obj_t BgL__ortest_1253z00_4583;

							{	/* Eval/evaluate_fsize.scm 508 */
								obj_t BgL_az00_4584;

								BgL_az00_4584 = CAR(((obj_t) BgL_l1250z00_4582));
								BgL__ortest_1253z00_4583 =
									BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
									((BgL_ev_exprz00_bglt) BgL_az00_4584),
									((BgL_ev_varz00_bglt) BgL_vz00_3972));
							}
							if (CBOOL(BgL__ortest_1253z00_4583))
								{	/* Eval/evaluate_fsize.scm 508 */
									return BgL__ortest_1253z00_4583;
								}
							else
								{	/* Eval/evaluate_fsize.scm 508 */
									obj_t BgL_arg1848z00_4585;

									BgL_arg1848z00_4585 = CDR(((obj_t) BgL_l1250z00_4582));
									{
										obj_t BgL_l1250z00_5722;

										BgL_l1250z00_5722 = BgL_arg1848z00_4585;
										BgL_l1250z00_4582 = BgL_l1250z00_5722;
										goto BgL_zc3z04anonymousza31846ze3z87_4581;
									}
								}
						}
				}
			}
		}

	}



/* &hasvar?-ev_if1447 */
	obj_t BGl_z62hasvarzf3zd2ev_if1447z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3973, obj_t BgL_ez00_3974, obj_t BgL_vz00_3975)
	{
		{	/* Eval/evaluate_fsize.scm 502 */
			{	/* Eval/evaluate_fsize.scm 504 */
				obj_t BgL__ortest_1150z00_4588;

				BgL__ortest_1150z00_4588 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_ifz00_bglt) COBJECT(
								((BgL_ev_ifz00_bglt) BgL_ez00_3974)))->BgL_pz00),
					((BgL_ev_varz00_bglt) BgL_vz00_3975));
				if (CBOOL(BgL__ortest_1150z00_4588))
					{	/* Eval/evaluate_fsize.scm 504 */
						return BgL__ortest_1150z00_4588;
					}
				else
					{	/* Eval/evaluate_fsize.scm 504 */
						obj_t BgL__ortest_1151z00_4589;

						BgL__ortest_1151z00_4589 =
							BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
							(((BgL_ev_ifz00_bglt) COBJECT(
										((BgL_ev_ifz00_bglt) BgL_ez00_3974)))->BgL_tz00),
							((BgL_ev_varz00_bglt) BgL_vz00_3975));
						if (CBOOL(BgL__ortest_1151z00_4589))
							{	/* Eval/evaluate_fsize.scm 504 */
								return BgL__ortest_1151z00_4589;
							}
						else
							{	/* Eval/evaluate_fsize.scm 504 */
								return
									BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
									(((BgL_ev_ifz00_bglt) COBJECT(
												((BgL_ev_ifz00_bglt) BgL_ez00_3974)))->BgL_ez00),
									((BgL_ev_varz00_bglt) BgL_vz00_3975));
							}
					}
			}
		}

	}



/* &hasvar?-ev_litt1445 */
	obj_t BGl_z62hasvarzf3zd2ev_litt1445z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3976, obj_t BgL_ez00_3977, obj_t BgL_vz00_3978)
	{
		{	/* Eval/evaluate_fsize.scm 499 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &hasvar?-ev_global1443 */
	obj_t BGl_z62hasvarzf3zd2ev_global1443z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3979, obj_t BgL_ez00_3980, obj_t BgL_vz00_3981)
	{
		{	/* Eval/evaluate_fsize.scm 496 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &hasvar?-ev_var1441 */
	obj_t BGl_z62hasvarzf3zd2ev_var1441z43zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3982, obj_t BgL_ez00_3983, obj_t BgL_vz00_3984)
	{
		{	/* Eval/evaluate_fsize.scm 493 */
			return
				BBOOL(
				(((obj_t)
						((BgL_ev_varz00_bglt) BgL_ez00_3983)) ==
					((obj_t) ((BgL_ev_varz00_bglt) BgL_vz00_3984))));
		}

	}



/* &tailpos-ev_abs1436 */
	obj_t BGl_z62tailposzd2ev_abs1436zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3985, obj_t BgL_ez00_3986, obj_t BgL_vz00_3987)
	{
		{	/* Eval/evaluate_fsize.scm 485 */
			{	/* Eval/evaluate_fsize.scm 486 */
				bool_t BgL_tmpz00_5747;

				{	/* Eval/evaluate_fsize.scm 487 */
					bool_t BgL_test2573z00_5748;

					{	/* Eval/evaluate_fsize.scm 487 */
						BgL_ev_exprz00_bglt BgL_arg1842z00_4598;

						BgL_arg1842z00_4598 =
							(((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_3986)))->BgL_bodyz00);
						BgL_test2573z00_5748 =
							CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_arg1842z00_4598,
								((BgL_ev_varz00_bglt) BgL_vz00_3987)));
					}
					if (BgL_test2573z00_5748)
						{	/* Eval/evaluate_fsize.scm 487 */
							BgL_tmpz00_5747 = ((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 487 */
							BgL_tmpz00_5747 = ((bool_t) 1);
						}
				}
				return BBOOL(BgL_tmpz00_5747);
			}
		}

	}



/* &tailpos-ev_app1434 */
	obj_t BGl_z62tailposzd2ev_app1434zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3988, obj_t BgL_ez00_3989, obj_t BgL_vz00_3990)
	{
		{	/* Eval/evaluate_fsize.scm 479 */
			{	/* Eval/evaluate_fsize.scm 480 */
				bool_t BgL_tmpz00_5755;

				{	/* Eval/evaluate_fsize.scm 481 */
					bool_t BgL_test2574z00_5756;

					{	/* Eval/evaluate_fsize.scm 481 */
						obj_t BgL_g1248z00_4601;

						BgL_g1248z00_4601 =
							(((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_3989)))->BgL_argsz00);
						{
							obj_t BgL_l1246z00_4603;

							BgL_l1246z00_4603 = BgL_g1248z00_4601;
						BgL_zc3z04anonymousza31835ze3z87_4602:
							if (NULLP(BgL_l1246z00_4603))
								{	/* Eval/evaluate_fsize.scm 481 */
									BgL_test2574z00_5756 = ((bool_t) 1);
								}
							else
								{	/* Eval/evaluate_fsize.scm 481 */
									bool_t BgL_test2576z00_5761;

									if (CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
												((BgL_ev_exprz00_bglt)
													CAR(
														((obj_t) BgL_l1246z00_4603))),
												((BgL_ev_varz00_bglt) BgL_vz00_3990))))
										{	/* Eval/evaluate_fsize.scm 481 */
											BgL_test2576z00_5761 = ((bool_t) 0);
										}
									else
										{	/* Eval/evaluate_fsize.scm 481 */
											BgL_test2576z00_5761 = ((bool_t) 1);
										}
									if (BgL_test2576z00_5761)
										{	/* Eval/evaluate_fsize.scm 481 */
											obj_t BgL_arg1837z00_4604;

											BgL_arg1837z00_4604 = CDR(((obj_t) BgL_l1246z00_4603));
											{
												obj_t BgL_l1246z00_5771;

												BgL_l1246z00_5771 = BgL_arg1837z00_4604;
												BgL_l1246z00_4603 = BgL_l1246z00_5771;
												goto BgL_zc3z04anonymousza31835ze3z87_4602;
											}
										}
									else
										{	/* Eval/evaluate_fsize.scm 481 */
											BgL_test2574z00_5756 = ((bool_t) 0);
										}
								}
						}
					}
					if (BgL_test2574z00_5756)
						{	/* Eval/evaluate_fsize.scm 482 */
							bool_t BgL__ortest_1147z00_4605;

							BgL__ortest_1147z00_4605 =
								(
								((obj_t)
									(((BgL_ev_appz00_bglt) COBJECT(
												((BgL_ev_appz00_bglt) BgL_ez00_3989)))->BgL_funz00)) ==
								((obj_t) ((BgL_ev_varz00_bglt) BgL_vz00_3990)));
							if (BgL__ortest_1147z00_4605)
								{	/* Eval/evaluate_fsize.scm 482 */
									BgL_tmpz00_5755 = BgL__ortest_1147z00_4605;
								}
							else
								{	/* Eval/evaluate_fsize.scm 483 */
									bool_t BgL_test2579z00_5779;

									{	/* Eval/evaluate_fsize.scm 483 */
										BgL_ev_exprz00_bglt BgL_arg1833z00_4606;

										BgL_arg1833z00_4606 =
											(((BgL_ev_appz00_bglt) COBJECT(
													((BgL_ev_appz00_bglt) BgL_ez00_3989)))->BgL_funz00);
										BgL_test2579z00_5779 =
											CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7
											(BgL_arg1833z00_4606,
												((BgL_ev_varz00_bglt) BgL_vz00_3990)));
									}
									if (BgL_test2579z00_5779)
										{	/* Eval/evaluate_fsize.scm 483 */
											BgL_tmpz00_5755 = ((bool_t) 0);
										}
									else
										{	/* Eval/evaluate_fsize.scm 483 */
											BgL_tmpz00_5755 = ((bool_t) 1);
										}
								}
						}
					else
						{	/* Eval/evaluate_fsize.scm 481 */
							BgL_tmpz00_5755 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_5755);
			}
		}

	}



/* &tailpos-ev_goto1432 */
	obj_t BGl_z62tailposzd2ev_goto1432zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3991, obj_t BgL_ez00_3992, obj_t BgL_vz00_3993)
	{
		{	/* Eval/evaluate_fsize.scm 475 */
			{	/* Eval/evaluate_fsize.scm 476 */
				bool_t BgL_tmpz00_5786;

				{	/* Eval/evaluate_fsize.scm 477 */
					obj_t BgL_g1244z00_4609;

					BgL_g1244z00_4609 =
						(((BgL_ev_gotoz00_bglt) COBJECT(
								((BgL_ev_gotoz00_bglt) BgL_ez00_3992)))->BgL_argsz00);
					{
						obj_t BgL_l1242z00_4611;

						BgL_l1242z00_4611 = BgL_g1244z00_4609;
					BgL_zc3z04anonymousza31827ze3z87_4610:
						if (NULLP(BgL_l1242z00_4611))
							{	/* Eval/evaluate_fsize.scm 477 */
								BgL_tmpz00_5786 = ((bool_t) 1);
							}
						else
							{	/* Eval/evaluate_fsize.scm 477 */
								bool_t BgL_test2581z00_5791;

								if (CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt)
												CAR(
													((obj_t) BgL_l1242z00_4611))),
											((BgL_ev_varz00_bglt) BgL_vz00_3993))))
									{	/* Eval/evaluate_fsize.scm 477 */
										BgL_test2581z00_5791 = ((bool_t) 0);
									}
								else
									{	/* Eval/evaluate_fsize.scm 477 */
										BgL_test2581z00_5791 = ((bool_t) 1);
									}
								if (BgL_test2581z00_5791)
									{	/* Eval/evaluate_fsize.scm 477 */
										obj_t BgL_arg1829z00_4612;

										BgL_arg1829z00_4612 = CDR(((obj_t) BgL_l1242z00_4611));
										{
											obj_t BgL_l1242z00_5801;

											BgL_l1242z00_5801 = BgL_arg1829z00_4612;
											BgL_l1242z00_4611 = BgL_l1242z00_5801;
											goto BgL_zc3z04anonymousza31827ze3z87_4610;
										}
									}
								else
									{	/* Eval/evaluate_fsize.scm 477 */
										BgL_tmpz00_5786 = ((bool_t) 0);
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_5786);
			}
		}

	}



/* &tailpos-ev_labels1430 */
	obj_t BGl_z62tailposzd2ev_labels1430zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3994, obj_t BgL_ez00_3995, obj_t BgL_vz00_3996)
	{
		{	/* Eval/evaluate_fsize.scm 470 */
			{	/* Eval/evaluate_fsize.scm 472 */
				bool_t BgL_test2583z00_5803;

				{	/* Eval/evaluate_fsize.scm 472 */
					obj_t BgL_g1240z00_4615;

					BgL_g1240z00_4615 =
						(((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_ez00_3995)))->BgL_valsz00);
					{
						obj_t BgL_l1238z00_4617;

						BgL_l1238z00_4617 = BgL_g1240z00_4615;
					BgL_zc3z04anonymousza31821ze3z87_4616:
						if (NULLP(BgL_l1238z00_4617))
							{	/* Eval/evaluate_fsize.scm 472 */
								BgL_test2583z00_5803 = ((bool_t) 1);
							}
						else
							{	/* Eval/evaluate_fsize.scm 472 */
								obj_t BgL_nvz00_4618;

								{	/* Eval/evaluate_fsize.scm 472 */
									obj_t BgL_ez00_4619;

									BgL_ez00_4619 = CAR(((obj_t) BgL_l1238z00_4617));
									{	/* Eval/evaluate_fsize.scm 472 */
										obj_t BgL_arg1826z00_4620;

										BgL_arg1826z00_4620 = CDR(((obj_t) BgL_ez00_4619));
										BgL_nvz00_4618 =
											BGl_tailposz00zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1826z00_4620),
											((BgL_ev_varz00_bglt) BgL_vz00_3996));
									}
								}
								if (CBOOL(BgL_nvz00_4618))
									{	/* Eval/evaluate_fsize.scm 472 */
										obj_t BgL_arg1823z00_4621;

										BgL_arg1823z00_4621 = CDR(((obj_t) BgL_l1238z00_4617));
										{
											obj_t BgL_l1238z00_5819;

											BgL_l1238z00_5819 = BgL_arg1823z00_4621;
											BgL_l1238z00_4617 = BgL_l1238z00_5819;
											goto BgL_zc3z04anonymousza31821ze3z87_4616;
										}
									}
								else
									{	/* Eval/evaluate_fsize.scm 472 */
										BgL_test2583z00_5803 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test2583z00_5803)
					{	/* Eval/evaluate_fsize.scm 472 */
						return
							BGl_tailposz00zz__evaluate_fsiza7eza7(
							(((BgL_ev_labelsz00_bglt) COBJECT(
										((BgL_ev_labelsz00_bglt) BgL_ez00_3995)))->BgL_bodyz00),
							((BgL_ev_varz00_bglt) BgL_vz00_3996));
					}
				else
					{	/* Eval/evaluate_fsize.scm 472 */
						return BFALSE;
					}
			}
		}

	}



/* &tailpos-ev_letrec1428 */
	obj_t BGl_z62tailposzd2ev_letrec1428zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_3997, obj_t BgL_ez00_3998, obj_t BgL_vz00_3999)
	{
		{	/* Eval/evaluate_fsize.scm 465 */
			{	/* Eval/evaluate_fsize.scm 467 */
				bool_t BgL_test2586z00_5824;

				{	/* Eval/evaluate_fsize.scm 467 */
					obj_t BgL_g1236z00_4624;

					BgL_g1236z00_4624 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letrecz00_bglt) BgL_ez00_3998))))->BgL_valsz00);
					{
						obj_t BgL_l1234z00_4626;

						BgL_l1234z00_4626 = BgL_g1236z00_4624;
					BgL_zc3z04anonymousza31816ze3z87_4625:
						if (NULLP(BgL_l1234z00_4626))
							{	/* Eval/evaluate_fsize.scm 467 */
								BgL_test2586z00_5824 = ((bool_t) 1);
							}
						else
							{	/* Eval/evaluate_fsize.scm 467 */
								bool_t BgL_test2588z00_5830;

								if (CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt)
												CAR(
													((obj_t) BgL_l1234z00_4626))),
											((BgL_ev_varz00_bglt) BgL_vz00_3999))))
									{	/* Eval/evaluate_fsize.scm 467 */
										BgL_test2588z00_5830 = ((bool_t) 0);
									}
								else
									{	/* Eval/evaluate_fsize.scm 467 */
										BgL_test2588z00_5830 = ((bool_t) 1);
									}
								if (BgL_test2588z00_5830)
									{	/* Eval/evaluate_fsize.scm 467 */
										obj_t BgL_arg1818z00_4627;

										BgL_arg1818z00_4627 = CDR(((obj_t) BgL_l1234z00_4626));
										{
											obj_t BgL_l1234z00_5840;

											BgL_l1234z00_5840 = BgL_arg1818z00_4627;
											BgL_l1234z00_4626 = BgL_l1234z00_5840;
											goto BgL_zc3z04anonymousza31816ze3z87_4625;
										}
									}
								else
									{	/* Eval/evaluate_fsize.scm 467 */
										BgL_test2586z00_5824 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test2586z00_5824)
					{	/* Eval/evaluate_fsize.scm 468 */
						BgL_ev_exprz00_bglt BgL_arg1815z00_4628;

						BgL_arg1815z00_4628 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_3998))))->BgL_bodyz00);
						return
							BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_arg1815z00_4628,
							((BgL_ev_varz00_bglt) BgL_vz00_3999));
					}
				else
					{	/* Eval/evaluate_fsize.scm 467 */
						return BFALSE;
					}
			}
		}

	}



/* &tailpos-ev_let*1426 */
	obj_t BGl_z62tailposzd2ev_letza21426z12zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4000, obj_t BgL_ez00_4001, obj_t BgL_vz00_4002)
	{
		{	/* Eval/evaluate_fsize.scm 460 */
			{	/* Eval/evaluate_fsize.scm 462 */
				bool_t BgL_test2590z00_5846;

				{	/* Eval/evaluate_fsize.scm 462 */
					obj_t BgL_g1232z00_4631;

					BgL_g1232z00_4631 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_ez00_4001))))->BgL_valsz00);
					{
						obj_t BgL_l1230z00_4633;

						BgL_l1230z00_4633 = BgL_g1232z00_4631;
					BgL_zc3z04anonymousza31811ze3z87_4632:
						if (NULLP(BgL_l1230z00_4633))
							{	/* Eval/evaluate_fsize.scm 462 */
								BgL_test2590z00_5846 = ((bool_t) 1);
							}
						else
							{	/* Eval/evaluate_fsize.scm 462 */
								bool_t BgL_test2592z00_5852;

								if (CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt)
												CAR(
													((obj_t) BgL_l1230z00_4633))),
											((BgL_ev_varz00_bglt) BgL_vz00_4002))))
									{	/* Eval/evaluate_fsize.scm 462 */
										BgL_test2592z00_5852 = ((bool_t) 0);
									}
								else
									{	/* Eval/evaluate_fsize.scm 462 */
										BgL_test2592z00_5852 = ((bool_t) 1);
									}
								if (BgL_test2592z00_5852)
									{	/* Eval/evaluate_fsize.scm 462 */
										obj_t BgL_arg1813z00_4634;

										BgL_arg1813z00_4634 = CDR(((obj_t) BgL_l1230z00_4633));
										{
											obj_t BgL_l1230z00_5862;

											BgL_l1230z00_5862 = BgL_arg1813z00_4634;
											BgL_l1230z00_4633 = BgL_l1230z00_5862;
											goto BgL_zc3z04anonymousza31811ze3z87_4632;
										}
									}
								else
									{	/* Eval/evaluate_fsize.scm 462 */
										BgL_test2590z00_5846 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test2590z00_5846)
					{	/* Eval/evaluate_fsize.scm 463 */
						BgL_ev_exprz00_bglt BgL_arg1810z00_4635;

						BgL_arg1810z00_4635 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letza2za2_bglt) BgL_ez00_4001))))->BgL_bodyz00);
						return
							BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_arg1810z00_4635,
							((BgL_ev_varz00_bglt) BgL_vz00_4002));
					}
				else
					{	/* Eval/evaluate_fsize.scm 462 */
						return BFALSE;
					}
			}
		}

	}



/* &tailpos-ev_let1423 */
	obj_t BGl_z62tailposzd2ev_let1423zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4003, obj_t BgL_ez00_4004, obj_t BgL_vz00_4005)
	{
		{	/* Eval/evaluate_fsize.scm 455 */
			{	/* Eval/evaluate_fsize.scm 457 */
				bool_t BgL_test2594z00_5868;

				{	/* Eval/evaluate_fsize.scm 457 */
					obj_t BgL_g1228z00_4638;

					BgL_g1228z00_4638 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letz00_bglt) BgL_ez00_4004))))->BgL_valsz00);
					{
						obj_t BgL_l1226z00_4640;

						BgL_l1226z00_4640 = BgL_g1228z00_4638;
					BgL_zc3z04anonymousza31806ze3z87_4639:
						if (NULLP(BgL_l1226z00_4640))
							{	/* Eval/evaluate_fsize.scm 457 */
								BgL_test2594z00_5868 = ((bool_t) 1);
							}
						else
							{	/* Eval/evaluate_fsize.scm 457 */
								bool_t BgL_test2596z00_5874;

								if (CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt)
												CAR(
													((obj_t) BgL_l1226z00_4640))),
											((BgL_ev_varz00_bglt) BgL_vz00_4005))))
									{	/* Eval/evaluate_fsize.scm 457 */
										BgL_test2596z00_5874 = ((bool_t) 0);
									}
								else
									{	/* Eval/evaluate_fsize.scm 457 */
										BgL_test2596z00_5874 = ((bool_t) 1);
									}
								if (BgL_test2596z00_5874)
									{	/* Eval/evaluate_fsize.scm 457 */
										obj_t BgL_arg1808z00_4641;

										BgL_arg1808z00_4641 = CDR(((obj_t) BgL_l1226z00_4640));
										{
											obj_t BgL_l1226z00_5884;

											BgL_l1226z00_5884 = BgL_arg1808z00_4641;
											BgL_l1226z00_4640 = BgL_l1226z00_5884;
											goto BgL_zc3z04anonymousza31806ze3z87_4639;
										}
									}
								else
									{	/* Eval/evaluate_fsize.scm 457 */
										BgL_test2594z00_5868 = ((bool_t) 0);
									}
							}
					}
				}
				if (BgL_test2594z00_5868)
					{	/* Eval/evaluate_fsize.scm 458 */
						BgL_ev_exprz00_bglt BgL_arg1805z00_4642;

						BgL_arg1805z00_4642 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letz00_bglt) BgL_ez00_4004))))->BgL_bodyz00);
						return
							BGl_tailposz00zz__evaluate_fsiza7eza7(BgL_arg1805z00_4642,
							((BgL_ev_varz00_bglt) BgL_vz00_4005));
					}
				else
					{	/* Eval/evaluate_fsize.scm 457 */
						return BFALSE;
					}
			}
		}

	}



/* &tailpos-ev_synchroni1420 */
	obj_t BGl_z62tailposzd2ev_synchroni1420zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4006, obj_t BgL_ez00_4007, obj_t BgL_vz00_4008)
	{
		{	/* Eval/evaluate_fsize.scm 451 */
			{	/* Eval/evaluate_fsize.scm 452 */
				bool_t BgL_tmpz00_5890;

				{	/* Eval/evaluate_fsize.scm 453 */
					obj_t BgL__andtest_1134z00_4645;

					BgL__andtest_1134z00_4645 =
						BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
						(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4007)))->
							BgL_mutexz00), ((BgL_ev_varz00_bglt) BgL_vz00_4008));
					if (CBOOL(BgL__andtest_1134z00_4645))
						{	/* Eval/evaluate_fsize.scm 453 */
							BgL_tmpz00_5890 = ((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 453 */
							obj_t BgL__andtest_1135z00_4646;

							BgL__andtest_1135z00_4646 =
								BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
								(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
											((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4007)))->
									BgL_prelockz00), ((BgL_ev_varz00_bglt) BgL_vz00_4008));
							if (CBOOL(BgL__andtest_1135z00_4646))
								{	/* Eval/evaluate_fsize.scm 453 */
									BgL_tmpz00_5890 = ((bool_t) 0);
								}
							else
								{	/* Eval/evaluate_fsize.scm 453 */
									bool_t BgL_test2600z00_5903;

									{	/* Eval/evaluate_fsize.scm 453 */
										BgL_ev_exprz00_bglt BgL_arg1802z00_4647;

										BgL_arg1802z00_4647 =
											(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
													((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4007)))->
											BgL_bodyz00);
										BgL_test2600z00_5903 =
											CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7
											(BgL_arg1802z00_4647,
												((BgL_ev_varz00_bglt) BgL_vz00_4008)));
									}
									if (BgL_test2600z00_5903)
										{	/* Eval/evaluate_fsize.scm 453 */
											BgL_tmpz00_5890 = ((bool_t) 0);
										}
									else
										{	/* Eval/evaluate_fsize.scm 453 */
											BgL_tmpz00_5890 = ((bool_t) 1);
										}
								}
						}
				}
				return BBOOL(BgL_tmpz00_5890);
			}
		}

	}



/* &tailpos-ev_with-hand1418 */
	obj_t BGl_z62tailposzd2ev_withzd2hand1418z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4009, obj_t BgL_ez00_4010, obj_t BgL_vz00_4011)
	{
		{	/* Eval/evaluate_fsize.scm 447 */
			{	/* Eval/evaluate_fsize.scm 448 */
				bool_t BgL_tmpz00_5910;

				{	/* Eval/evaluate_fsize.scm 449 */
					obj_t BgL__andtest_1132z00_4650;

					BgL__andtest_1132z00_4650 =
						BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
						(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4010)))->
							BgL_handlerz00), ((BgL_ev_varz00_bglt) BgL_vz00_4011));
					if (CBOOL(BgL__andtest_1132z00_4650))
						{	/* Eval/evaluate_fsize.scm 449 */
							BgL_tmpz00_5910 = ((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 449 */
							bool_t BgL_test2602z00_5917;

							{	/* Eval/evaluate_fsize.scm 449 */
								BgL_ev_exprz00_bglt BgL_arg1798z00_4651;

								BgL_arg1798z00_4651 =
									(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
											((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4010)))->
									BgL_bodyz00);
								BgL_test2602z00_5917 =
									CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7
									(BgL_arg1798z00_4651, ((BgL_ev_varz00_bglt) BgL_vz00_4011)));
							}
							if (BgL_test2602z00_5917)
								{	/* Eval/evaluate_fsize.scm 449 */
									BgL_tmpz00_5910 = ((bool_t) 0);
								}
							else
								{	/* Eval/evaluate_fsize.scm 449 */
									BgL_tmpz00_5910 = ((bool_t) 1);
								}
						}
				}
				return BBOOL(BgL_tmpz00_5910);
			}
		}

	}



/* &tailpos-ev_unwind-pr1416 */
	obj_t BGl_z62tailposzd2ev_unwindzd2pr1416z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4012, obj_t BgL_ez00_4013, obj_t BgL_vz00_4014)
	{
		{	/* Eval/evaluate_fsize.scm 443 */
			{	/* Eval/evaluate_fsize.scm 444 */
				bool_t BgL_tmpz00_5924;

				{	/* Eval/evaluate_fsize.scm 445 */
					obj_t BgL__andtest_1130z00_4654;

					BgL__andtest_1130z00_4654 =
						BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
						(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_4013)))->
							BgL_ez00), ((BgL_ev_varz00_bglt) BgL_vz00_4014));
					if (CBOOL(BgL__andtest_1130z00_4654))
						{	/* Eval/evaluate_fsize.scm 445 */
							BgL_tmpz00_5924 = ((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 445 */
							bool_t BgL_test2604z00_5931;

							{	/* Eval/evaluate_fsize.scm 445 */
								BgL_ev_exprz00_bglt BgL_arg1794z00_4655;

								BgL_arg1794z00_4655 =
									(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
											((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_4013)))->
									BgL_bodyz00);
								BgL_test2604z00_5931 =
									CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7
									(BgL_arg1794z00_4655, ((BgL_ev_varz00_bglt) BgL_vz00_4014)));
							}
							if (BgL_test2604z00_5931)
								{	/* Eval/evaluate_fsize.scm 445 */
									BgL_tmpz00_5924 = ((bool_t) 0);
								}
							else
								{	/* Eval/evaluate_fsize.scm 445 */
									BgL_tmpz00_5924 = ((bool_t) 1);
								}
						}
				}
				return BBOOL(BgL_tmpz00_5924);
			}
		}

	}



/* &tailpos-ev_bind-exit1414 */
	obj_t BGl_z62tailposzd2ev_bindzd2exit1414z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4015, obj_t BgL_ez00_4016, obj_t BgL_vz00_4017)
	{
		{	/* Eval/evaluate_fsize.scm 439 */
			{	/* Eval/evaluate_fsize.scm 440 */
				bool_t BgL_tmpz00_5938;

				{	/* Eval/evaluate_fsize.scm 441 */
					bool_t BgL_test2605z00_5939;

					{	/* Eval/evaluate_fsize.scm 441 */
						BgL_ev_exprz00_bglt BgL_arg1791z00_4658;

						BgL_arg1791z00_4658 =
							(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4016)))->BgL_bodyz00);
						BgL_test2605z00_5939 =
							CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_arg1791z00_4658,
								((BgL_ev_varz00_bglt) BgL_vz00_4017)));
					}
					if (BgL_test2605z00_5939)
						{	/* Eval/evaluate_fsize.scm 441 */
							BgL_tmpz00_5938 = ((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 441 */
							BgL_tmpz00_5938 = ((bool_t) 1);
						}
				}
				return BBOOL(BgL_tmpz00_5938);
			}
		}

	}



/* &tailpos-ev_setlocal1412 */
	obj_t BGl_z62tailposzd2ev_setlocal1412zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4018, obj_t BgL_ez00_4019, obj_t BgL_varz00_4020)
	{
		{	/* Eval/evaluate_fsize.scm 435 */
			{	/* Eval/evaluate_fsize.scm 436 */
				bool_t BgL_tmpz00_5946;

				if (
					(((obj_t)
							(((BgL_ev_setlocalz00_bglt) COBJECT(
										((BgL_ev_setlocalz00_bglt) BgL_ez00_4019)))->BgL_vz00)) ==
						((obj_t) ((BgL_ev_varz00_bglt) BgL_varz00_4020))))
					{	/* Eval/evaluate_fsize.scm 437 */
						BgL_tmpz00_5946 = ((bool_t) 0);
					}
				else
					{	/* Eval/evaluate_fsize.scm 437 */
						bool_t BgL_test2607z00_5954;

						{	/* Eval/evaluate_fsize.scm 437 */
							BgL_ev_exprz00_bglt BgL_arg1787z00_4661;

							BgL_arg1787z00_4661 =
								(((BgL_ev_hookz00_bglt) COBJECT(
										((BgL_ev_hookz00_bglt)
											((BgL_ev_setlocalz00_bglt) BgL_ez00_4019))))->BgL_ez00);
							BgL_test2607z00_5954 =
								CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7
								(BgL_arg1787z00_4661, ((BgL_ev_varz00_bglt) BgL_varz00_4020)));
						}
						if (BgL_test2607z00_5954)
							{	/* Eval/evaluate_fsize.scm 437 */
								BgL_tmpz00_5946 = ((bool_t) 0);
							}
						else
							{	/* Eval/evaluate_fsize.scm 437 */
								BgL_tmpz00_5946 = ((bool_t) 1);
							}
					}
				return BBOOL(BgL_tmpz00_5946);
			}
		}

	}



/* &tailpos-ev_hook1408 */
	obj_t BGl_z62tailposzd2ev_hook1408zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4021, obj_t BgL_ez00_4022, obj_t BgL_vz00_4023)
	{
		{	/* Eval/evaluate_fsize.scm 431 */
			{	/* Eval/evaluate_fsize.scm 432 */
				bool_t BgL_tmpz00_5962;

				{	/* Eval/evaluate_fsize.scm 433 */
					bool_t BgL_test2608z00_5963;

					{	/* Eval/evaluate_fsize.scm 433 */
						BgL_ev_exprz00_bglt BgL_arg1783z00_4664;

						BgL_arg1783z00_4664 =
							(((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_ez00_4022)))->BgL_ez00);
						BgL_test2608z00_5963 =
							CBOOL(BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(BgL_arg1783z00_4664,
								((BgL_ev_varz00_bglt) BgL_vz00_4023)));
					}
					if (BgL_test2608z00_5963)
						{	/* Eval/evaluate_fsize.scm 433 */
							BgL_tmpz00_5962 = ((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 433 */
							BgL_tmpz00_5962 = ((bool_t) 1);
						}
				}
				return BBOOL(BgL_tmpz00_5962);
			}
		}

	}



/* &tailpos-ev_prog21406 */
	obj_t BGl_z62tailposzd2ev_prog21406zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4024, obj_t BgL_ez00_4025, obj_t BgL_vz00_4026)
	{
		{	/* Eval/evaluate_fsize.scm 427 */
			{	/* Eval/evaluate_fsize.scm 429 */
				obj_t BgL__andtest_1124z00_4667;

				BgL__andtest_1124z00_4667 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_prog2z00_bglt) COBJECT(
								((BgL_ev_prog2z00_bglt) BgL_ez00_4025)))->BgL_e1z00),
					((BgL_ev_varz00_bglt) BgL_vz00_4026));
				if (CBOOL(BgL__andtest_1124z00_4667))
					{	/* Eval/evaluate_fsize.scm 429 */
						return BFALSE;
					}
				else
					{	/* Eval/evaluate_fsize.scm 429 */
						return
							BGl_tailposz00zz__evaluate_fsiza7eza7(
							(((BgL_ev_prog2z00_bglt) COBJECT(
										((BgL_ev_prog2z00_bglt) BgL_ez00_4025)))->BgL_e2z00),
							((BgL_ev_varz00_bglt) BgL_vz00_4026));
					}
			}
		}

	}



/* &tailpos-ev_list1403 */
	obj_t BGl_z62tailposzd2ev_list1403zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4027, obj_t BgL_ez00_4028, obj_t BgL_vz00_4029)
	{
		{	/* Eval/evaluate_fsize.scm 419 */
			{
				obj_t BgL_lz00_4671;

				BgL_lz00_4671 =
					(((BgL_ev_listz00_bglt) COBJECT(
							((BgL_ev_listz00_bglt) BgL_ez00_4028)))->BgL_argsz00);
			BgL_recz00_4670:
				if (NULLP(CDR(((obj_t) BgL_lz00_4671))))
					{	/* Eval/evaluate_fsize.scm 423 */
						obj_t BgL_arg1772z00_4672;

						BgL_arg1772z00_4672 = CAR(((obj_t) BgL_lz00_4671));
						return
							BGl_tailposz00zz__evaluate_fsiza7eza7(
							((BgL_ev_exprz00_bglt) BgL_arg1772z00_4672),
							((BgL_ev_varz00_bglt) BgL_vz00_4029));
					}
				else
					{	/* Eval/evaluate_fsize.scm 424 */
						obj_t BgL__andtest_1122z00_4673;

						{	/* Eval/evaluate_fsize.scm 424 */
							obj_t BgL_arg1774z00_4674;

							BgL_arg1774z00_4674 = CAR(((obj_t) BgL_lz00_4671));
							BgL__andtest_1122z00_4673 =
								BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
								((BgL_ev_exprz00_bglt) BgL_arg1774z00_4674),
								((BgL_ev_varz00_bglt) BgL_vz00_4029));
						}
						if (CBOOL(BgL__andtest_1122z00_4673))
							{	/* Eval/evaluate_fsize.scm 424 */
								return BFALSE;
							}
						else
							{	/* Eval/evaluate_fsize.scm 425 */
								obj_t BgL_arg1773z00_4675;

								BgL_arg1773z00_4675 = CDR(((obj_t) BgL_lz00_4671));
								{
									obj_t BgL_lz00_5998;

									BgL_lz00_5998 = BgL_arg1773z00_4675;
									BgL_lz00_4671 = BgL_lz00_5998;
									goto BgL_recz00_4670;
								}
							}
					}
			}
		}

	}



/* &tailpos-ev_if1401 */
	obj_t BGl_z62tailposzd2ev_if1401zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4030, obj_t BgL_ez00_4031, obj_t BgL_vz00_4032)
	{
		{	/* Eval/evaluate_fsize.scm 415 */
			{	/* Eval/evaluate_fsize.scm 417 */
				obj_t BgL__andtest_1119z00_4678;

				BgL__andtest_1119z00_4678 =
					BGl_hasvarzf3zf3zz__evaluate_fsiza7eza7(
					(((BgL_ev_ifz00_bglt) COBJECT(
								((BgL_ev_ifz00_bglt) BgL_ez00_4031)))->BgL_pz00),
					((BgL_ev_varz00_bglt) BgL_vz00_4032));
				if (CBOOL(BgL__andtest_1119z00_4678))
					{	/* Eval/evaluate_fsize.scm 417 */
						return BFALSE;
					}
				else
					{	/* Eval/evaluate_fsize.scm 417 */
						obj_t BgL__andtest_1120z00_4679;

						BgL__andtest_1120z00_4679 =
							BGl_tailposz00zz__evaluate_fsiza7eza7(
							(((BgL_ev_ifz00_bglt) COBJECT(
										((BgL_ev_ifz00_bglt) BgL_ez00_4031)))->BgL_tz00),
							((BgL_ev_varz00_bglt) BgL_vz00_4032));
						if (CBOOL(BgL__andtest_1120z00_4679))
							{	/* Eval/evaluate_fsize.scm 417 */
								return
									BGl_tailposz00zz__evaluate_fsiza7eza7(
									(((BgL_ev_ifz00_bglt) COBJECT(
												((BgL_ev_ifz00_bglt) BgL_ez00_4031)))->BgL_ez00),
									((BgL_ev_varz00_bglt) BgL_vz00_4032));
							}
						else
							{	/* Eval/evaluate_fsize.scm 417 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &tailpos-ev_litt1399 */
	obj_t BGl_z62tailposzd2ev_litt1399zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4033, obj_t BgL_ez00_4034, obj_t BgL_vz00_4035)
	{
		{	/* Eval/evaluate_fsize.scm 412 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &tailpos-ev_global1396 */
	obj_t BGl_z62tailposzd2ev_global1396zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4036, obj_t BgL_ez00_4037, obj_t BgL_vz00_4038)
	{
		{	/* Eval/evaluate_fsize.scm 409 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &tailpos-ev_var1394 */
	obj_t BGl_z62tailposzd2ev_var1394zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4039, obj_t BgL_ez00_4040, obj_t BgL_vz00_4041)
	{
		{	/* Eval/evaluate_fsize.scm 406 */
			{	/* Eval/evaluate_fsize.scm 407 */
				bool_t BgL_tmpz00_6019;

				if (
					(((obj_t)
							((BgL_ev_varz00_bglt) BgL_ez00_4040)) ==
						((obj_t) ((BgL_ev_varz00_bglt) BgL_vz00_4041))))
					{	/* Eval/evaluate_fsize.scm 407 */
						BgL_tmpz00_6019 = ((bool_t) 0);
					}
				else
					{	/* Eval/evaluate_fsize.scm 407 */
						BgL_tmpz00_6019 = ((bool_t) 1);
					}
				return BBOOL(BgL_tmpz00_6019);
			}
		}

	}



/* &subst_goto-ev_abs1390 */
	obj_t BGl_z62subst_gotozd2ev_abs1390zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4042, obj_t BgL_ez00_4043, obj_t BgL_varsz00_4044,
		obj_t BgL_lblsz00_4045)
	{
		{	/* Eval/evaluate_fsize.scm 394 */
			{
				BgL_ev_absz00_bglt BgL_auxz00_6027;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6028;

					{	/* Eval/evaluate_fsize.scm 396 */
						BgL_ev_exprz00_bglt BgL_arg1764z00_4687;

						BgL_arg1764z00_4687 =
							(((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_4043)))->BgL_bodyz00);
						BgL_auxz00_6028 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1764z00_4687,
								BgL_varsz00_4044, BgL_lblsz00_4045));
					}
					((((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_4043)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6028), BUNSPEC);
				}
				BgL_auxz00_6027 = ((BgL_ev_absz00_bglt) BgL_ez00_4043);
				return ((obj_t) BgL_auxz00_6027);
			}
		}

	}



/* &subst_goto-ev_app1388 */
	obj_t BGl_z62subst_gotozd2ev_app1388zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4046, obj_t BgL_ez00_4047, obj_t BgL_varsz00_4048,
		obj_t BgL_lblsz00_4049)
	{
		{	/* Eval/evaluate_fsize.scm 384 */
			{
				BgL_ev_exprz00_bglt BgL_auxz00_6037;

				{	/* Eval/evaluate_fsize.scm 387 */
					obj_t BgL_arg1758z00_4689;

					BgL_arg1758z00_4689 =
						(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_4047)))->BgL_argsz00);
					{
						obj_t BgL_lz00_4691;

						BgL_lz00_4691 = BgL_arg1758z00_4689;
					BgL_recz00_4690:
						if (NULLP(BgL_lz00_4691))
							{	/* Eval/evaluate_fsize.scm 294 */
								((bool_t) 0);
							}
						else
							{	/* Eval/evaluate_fsize.scm 294 */
								{	/* Eval/evaluate_fsize.scm 295 */
									obj_t BgL_arg1578z00_4692;

									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_arg1579z00_4693;

										BgL_arg1579z00_4693 = CAR(((obj_t) BgL_lz00_4691));
										BgL_arg1578z00_4692 =
											BGl_subst_gotoz00zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1579z00_4693),
											BgL_varsz00_4048, BgL_lblsz00_4049);
									}
									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_tmpz00_6046;

										BgL_tmpz00_6046 = ((obj_t) BgL_lz00_4691);
										SET_CAR(BgL_tmpz00_6046, BgL_arg1578z00_4692);
									}
								}
								{	/* Eval/evaluate_fsize.scm 296 */
									obj_t BgL_arg1580z00_4694;

									BgL_arg1580z00_4694 = CDR(((obj_t) BgL_lz00_4691));
									{
										obj_t BgL_lz00_6051;

										BgL_lz00_6051 = BgL_arg1580z00_4694;
										BgL_lz00_4691 = BgL_lz00_6051;
										goto BgL_recz00_4690;
									}
								}
							}
					}
				}
				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							((obj_t)
								(((BgL_ev_appz00_bglt) COBJECT(
											((BgL_ev_appz00_bglt) BgL_ez00_4047)))->BgL_funz00)),
							BgL_varsz00_4048)))
					{	/* Eval/evaluate_fsize.scm 389 */
						BgL_ev_gotoz00_bglt BgL_new1115z00_4695;

						{	/* Eval/evaluate_fsize.scm 389 */
							BgL_ev_gotoz00_bglt BgL_new1114z00_4696;

							BgL_new1114z00_4696 =
								((BgL_ev_gotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_ev_gotoz00_bgl))));
							{	/* Eval/evaluate_fsize.scm 389 */
								long BgL_arg1761z00_4697;

								BgL_arg1761z00_4697 =
									BGL_CLASS_NUM(BGl_ev_gotoz00zz__evaluate_typesz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1114z00_4696),
									BgL_arg1761z00_4697);
							}
							BgL_new1115z00_4695 = BgL_new1114z00_4696;
						}
						((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1115z00_4695))->
								BgL_locz00) =
							((obj_t) (((BgL_ev_appz00_bglt) COBJECT(((BgL_ev_appz00_bglt)
												BgL_ez00_4047)))->BgL_locz00)), BUNSPEC);
						((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1115z00_4695))->
								BgL_labelz00) =
							((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) (((BgL_ev_appz00_bglt)
											COBJECT(((BgL_ev_appz00_bglt) BgL_ez00_4047)))->
										BgL_funz00))), BUNSPEC);
						((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1115z00_4695))->
								BgL_labelsz00) =
							((BgL_ev_labelsz00_bglt) ((BgL_ev_labelsz00_bglt)
									BgL_lblsz00_4049)), BUNSPEC);
						((((BgL_ev_gotoz00_bglt) COBJECT(BgL_new1115z00_4695))->
								BgL_argsz00) =
							((obj_t) (((BgL_ev_appz00_bglt) COBJECT(((BgL_ev_appz00_bglt)
												BgL_ez00_4047)))->BgL_argsz00)), BUNSPEC);
						BgL_auxz00_6037 = ((BgL_ev_exprz00_bglt) BgL_new1115z00_4695);
					}
				else
					{	/* Eval/evaluate_fsize.scm 388 */
						{
							BgL_ev_exprz00_bglt BgL_auxz00_6075;

							{	/* Eval/evaluate_fsize.scm 390 */
								BgL_ev_exprz00_bglt BgL_arg1762z00_4698;

								BgL_arg1762z00_4698 =
									(((BgL_ev_appz00_bglt) COBJECT(
											((BgL_ev_appz00_bglt) BgL_ez00_4047)))->BgL_funz00);
								BgL_auxz00_6075 =
									((BgL_ev_exprz00_bglt)
									BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1762z00_4698,
										BgL_varsz00_4048, BgL_lblsz00_4049));
							}
							((((BgL_ev_appz00_bglt) COBJECT(
											((BgL_ev_appz00_bglt) BgL_ez00_4047)))->BgL_funz00) =
								((BgL_ev_exprz00_bglt) BgL_auxz00_6075), BUNSPEC);
						}
						((((BgL_ev_appz00_bglt) COBJECT(
										((BgL_ev_appz00_bglt) BgL_ez00_4047)))->BgL_tailzf3zf3) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_auxz00_6037 =
							((BgL_ev_exprz00_bglt) ((BgL_ev_appz00_bglt) BgL_ez00_4047));
					}
				return ((obj_t) BgL_auxz00_6037);
			}
		}

	}



/* &subst_goto-ev_goto1386 */
	obj_t BGl_z62subst_gotozd2ev_goto1386zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4050, obj_t BgL_ez00_4051, obj_t BgL_varsz00_4052,
		obj_t BgL_lblsz00_4053)
	{
		{	/* Eval/evaluate_fsize.scm 372 */
			{
				BgL_ev_gotoz00_bglt BgL_auxz00_6087;

				{	/* Eval/evaluate_fsize.scm 374 */
					obj_t BgL_arg1757z00_4700;

					BgL_arg1757z00_4700 =
						(((BgL_ev_gotoz00_bglt) COBJECT(
								((BgL_ev_gotoz00_bglt) BgL_ez00_4051)))->BgL_argsz00);
					{
						obj_t BgL_lz00_4702;

						BgL_lz00_4702 = BgL_arg1757z00_4700;
					BgL_recz00_4701:
						if (NULLP(BgL_lz00_4702))
							{	/* Eval/evaluate_fsize.scm 294 */
								((bool_t) 0);
							}
						else
							{	/* Eval/evaluate_fsize.scm 294 */
								{	/* Eval/evaluate_fsize.scm 295 */
									obj_t BgL_arg1578z00_4703;

									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_arg1579z00_4704;

										BgL_arg1579z00_4704 = CAR(((obj_t) BgL_lz00_4702));
										BgL_arg1578z00_4703 =
											BGl_subst_gotoz00zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1579z00_4704),
											BgL_varsz00_4052, BgL_lblsz00_4053);
									}
									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_tmpz00_6096;

										BgL_tmpz00_6096 = ((obj_t) BgL_lz00_4702);
										SET_CAR(BgL_tmpz00_6096, BgL_arg1578z00_4703);
									}
								}
								{	/* Eval/evaluate_fsize.scm 296 */
									obj_t BgL_arg1580z00_4705;

									BgL_arg1580z00_4705 = CDR(((obj_t) BgL_lz00_4702));
									{
										obj_t BgL_lz00_6101;

										BgL_lz00_6101 = BgL_arg1580z00_4705;
										BgL_lz00_4702 = BgL_lz00_6101;
										goto BgL_recz00_4701;
									}
								}
							}
					}
				}
				BgL_auxz00_6087 = ((BgL_ev_gotoz00_bglt) BgL_ez00_4051);
				return ((obj_t) BgL_auxz00_6087);
			}
		}

	}



/* &subst_goto-ev_labels1384 */
	obj_t BGl_z62subst_gotozd2ev_labels1384zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4054, obj_t BgL_ez00_4055, obj_t BgL_varsz00_4056,
		obj_t BgL_lblsz00_4057)
	{
		{	/* Eval/evaluate_fsize.scm 363 */
			{
				BgL_ev_labelsz00_bglt BgL_auxz00_6104;

				{
					obj_t BgL_lz00_4708;

					BgL_lz00_4708 =
						(((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_ez00_4055)))->BgL_valsz00);
				BgL_recz00_4707:
					if (NULLP(BgL_lz00_4708))
						{	/* Eval/evaluate_fsize.scm 366 */
							((bool_t) 0);
						}
					else
						{	/* Eval/evaluate_fsize.scm 366 */
							{	/* Eval/evaluate_fsize.scm 367 */
								obj_t BgL_arg1752z00_4709;
								obj_t BgL_arg1753z00_4710;

								BgL_arg1752z00_4709 = CAR(((obj_t) BgL_lz00_4708));
								{	/* Eval/evaluate_fsize.scm 367 */
									obj_t BgL_arg1754z00_4711;

									{	/* Eval/evaluate_fsize.scm 367 */
										obj_t BgL_pairz00_4712;

										BgL_pairz00_4712 = CAR(((obj_t) BgL_lz00_4708));
										BgL_arg1754z00_4711 = CDR(BgL_pairz00_4712);
									}
									BgL_arg1753z00_4710 =
										BGl_subst_gotoz00zz__evaluate_fsiza7eza7(
										((BgL_ev_exprz00_bglt) BgL_arg1754z00_4711),
										BgL_varsz00_4056, BgL_lblsz00_4057);
								}
								{	/* Eval/evaluate_fsize.scm 367 */
									obj_t BgL_tmpz00_6114;

									BgL_tmpz00_6114 = ((obj_t) BgL_arg1752z00_4709);
									SET_CDR(BgL_tmpz00_6114, BgL_arg1753z00_4710);
								}
							}
							{	/* Eval/evaluate_fsize.scm 368 */
								obj_t BgL_arg1755z00_4713;

								BgL_arg1755z00_4713 = CDR(((obj_t) BgL_lz00_4708));
								{
									obj_t BgL_lz00_6119;

									BgL_lz00_6119 = BgL_arg1755z00_4713;
									BgL_lz00_4708 = BgL_lz00_6119;
									goto BgL_recz00_4707;
								}
							}
						}
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6122;

					{	/* Eval/evaluate_fsize.scm 369 */
						BgL_ev_exprz00_bglt BgL_arg1756z00_4714;

						BgL_arg1756z00_4714 =
							(((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_4055)))->BgL_bodyz00);
						BgL_auxz00_6122 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1756z00_4714,
								BgL_varsz00_4056, BgL_lblsz00_4057));
					}
					((((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_4055)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6122), BUNSPEC);
				}
				BgL_auxz00_6104 = ((BgL_ev_labelsz00_bglt) BgL_ez00_4055);
				return ((obj_t) BgL_auxz00_6104);
			}
		}

	}



/* &subst_goto-ev_binder1382 */
	obj_t BGl_z62subst_gotozd2ev_binder1382zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4058, obj_t BgL_ez00_4059, obj_t BgL_varsz00_4060,
		obj_t BgL_lblsz00_4061)
	{
		{	/* Eval/evaluate_fsize.scm 357 */
			{
				BgL_ev_binderz00_bglt BgL_auxz00_6131;

				{	/* Eval/evaluate_fsize.scm 359 */
					obj_t BgL_arg1747z00_4716;

					BgL_arg1747z00_4716 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_ez00_4059)))->BgL_valsz00);
					{
						obj_t BgL_lz00_4718;

						BgL_lz00_4718 = BgL_arg1747z00_4716;
					BgL_recz00_4717:
						if (NULLP(BgL_lz00_4718))
							{	/* Eval/evaluate_fsize.scm 294 */
								((bool_t) 0);
							}
						else
							{	/* Eval/evaluate_fsize.scm 294 */
								{	/* Eval/evaluate_fsize.scm 295 */
									obj_t BgL_arg1578z00_4719;

									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_arg1579z00_4720;

										BgL_arg1579z00_4720 = CAR(((obj_t) BgL_lz00_4718));
										BgL_arg1578z00_4719 =
											BGl_subst_gotoz00zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1579z00_4720),
											BgL_varsz00_4060, BgL_lblsz00_4061);
									}
									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_tmpz00_6140;

										BgL_tmpz00_6140 = ((obj_t) BgL_lz00_4718);
										SET_CAR(BgL_tmpz00_6140, BgL_arg1578z00_4719);
									}
								}
								{	/* Eval/evaluate_fsize.scm 296 */
									obj_t BgL_arg1580z00_4721;

									BgL_arg1580z00_4721 = CDR(((obj_t) BgL_lz00_4718));
									{
										obj_t BgL_lz00_6145;

										BgL_lz00_6145 = BgL_arg1580z00_4721;
										BgL_lz00_4718 = BgL_lz00_6145;
										goto BgL_recz00_4717;
									}
								}
							}
					}
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6146;

					{	/* Eval/evaluate_fsize.scm 360 */
						BgL_ev_exprz00_bglt BgL_arg1748z00_4722;

						BgL_arg1748z00_4722 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt) BgL_ez00_4059)))->BgL_bodyz00);
						BgL_auxz00_6146 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1748z00_4722,
								BgL_varsz00_4060, BgL_lblsz00_4061));
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt) BgL_ez00_4059)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6146), BUNSPEC);
				}
				BgL_auxz00_6131 = ((BgL_ev_binderz00_bglt) BgL_ez00_4059);
				return ((obj_t) BgL_auxz00_6131);
			}
		}

	}



/* &subst_goto-ev_synchr1380 */
	obj_t BGl_z62subst_gotozd2ev_synchr1380zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4062, obj_t BgL_ez00_4063, obj_t BgL_varsz00_4064,
		obj_t BgL_lblsz00_4065)
	{
		{	/* Eval/evaluate_fsize.scm 350 */
			{
				BgL_ev_synchroniza7eza7_bglt BgL_auxz00_6155;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6156;

					{	/* Eval/evaluate_fsize.scm 352 */
						BgL_ev_exprz00_bglt BgL_arg1744z00_4724;

						BgL_arg1744z00_4724 =
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063)))->
							BgL_mutexz00);
						BgL_auxz00_6156 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1744z00_4724,
								BgL_varsz00_4064, BgL_lblsz00_4065));
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063)))->
							BgL_mutexz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6156), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6163;

					{	/* Eval/evaluate_fsize.scm 353 */
						BgL_ev_exprz00_bglt BgL_arg1745z00_4725;

						BgL_arg1745z00_4725 =
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063)))->
							BgL_prelockz00);
						BgL_auxz00_6163 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1745z00_4725,
								BgL_varsz00_4064, BgL_lblsz00_4065));
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063)))->
							BgL_prelockz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6163), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6170;

					{	/* Eval/evaluate_fsize.scm 354 */
						BgL_ev_exprz00_bglt BgL_arg1746z00_4726;

						BgL_arg1746z00_4726 =
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063)))->
							BgL_bodyz00);
						BgL_auxz00_6170 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1746z00_4726,
								BgL_varsz00_4064, BgL_lblsz00_4065));
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6170), BUNSPEC);
				}
				BgL_auxz00_6155 = ((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4063);
				return ((obj_t) BgL_auxz00_6155);
			}
		}

	}



/* &subst_goto-ev_with-h1377 */
	obj_t BGl_z62subst_gotozd2ev_withzd2h1377z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4066, obj_t BgL_ez00_4067, obj_t BgL_varsz00_4068,
		obj_t BgL_lblsz00_4069)
	{
		{	/* Eval/evaluate_fsize.scm 344 */
			{
				BgL_ev_withzd2handlerzd2_bglt BgL_auxz00_6179;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6180;

					{	/* Eval/evaluate_fsize.scm 346 */
						BgL_ev_exprz00_bglt BgL_arg1741z00_4728;

						BgL_arg1741z00_4728 =
							(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4067)))->
							BgL_handlerz00);
						BgL_auxz00_6180 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1741z00_4728,
								BgL_varsz00_4068, BgL_lblsz00_4069));
					}
					((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4067)))->
							BgL_handlerz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6180), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6187;

					{	/* Eval/evaluate_fsize.scm 347 */
						BgL_ev_exprz00_bglt BgL_arg1743z00_4729;

						BgL_arg1743z00_4729 =
							(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4067)))->
							BgL_bodyz00);
						BgL_auxz00_6187 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1743z00_4729,
								BgL_varsz00_4068, BgL_lblsz00_4069));
					}
					((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4067)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6187), BUNSPEC);
				}
				BgL_auxz00_6179 = ((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4067);
				return ((obj_t) BgL_auxz00_6179);
			}
		}

	}



/* &subst_goto-ev_unwind1374 */
	obj_t BGl_z62subst_gotozd2ev_unwind1374zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4070, obj_t BgL_exprz00_4071, obj_t BgL_varsz00_4072,
		obj_t BgL_lblsz00_4073)
	{
		{	/* Eval/evaluate_fsize.scm 338 */
			{
				BgL_ev_unwindzd2protectzd2_bglt BgL_auxz00_6196;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6197;

					{	/* Eval/evaluate_fsize.scm 340 */
						BgL_ev_exprz00_bglt BgL_arg1739z00_4731;

						BgL_arg1739z00_4731 =
							(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4071)))->
							BgL_ez00);
						BgL_auxz00_6197 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1739z00_4731,
								BgL_varsz00_4072, BgL_lblsz00_4073));
					}
					((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4071)))->
							BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6197), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6204;

					{	/* Eval/evaluate_fsize.scm 341 */
						BgL_ev_exprz00_bglt BgL_arg1740z00_4732;

						BgL_arg1740z00_4732 =
							(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4071)))->
							BgL_bodyz00);
						BgL_auxz00_6204 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1740z00_4732,
								BgL_varsz00_4072, BgL_lblsz00_4073));
					}
					((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4071)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6204), BUNSPEC);
				}
				BgL_auxz00_6196 = ((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4071);
				return ((obj_t) BgL_auxz00_6196);
			}
		}

	}



/* &subst_goto-ev_bind-e1371 */
	obj_t BGl_z62subst_gotozd2ev_bindzd2e1371z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4074, obj_t BgL_ez00_4075, obj_t BgL_varsz00_4076,
		obj_t BgL_lblsz00_4077)
	{
		{	/* Eval/evaluate_fsize.scm 333 */
			{
				BgL_ev_bindzd2exitzd2_bglt BgL_auxz00_6213;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6214;

					{	/* Eval/evaluate_fsize.scm 335 */
						BgL_ev_exprz00_bglt BgL_arg1738z00_4734;

						BgL_arg1738z00_4734 =
							(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4075)))->BgL_bodyz00);
						BgL_auxz00_6214 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1738z00_4734,
								BgL_varsz00_4076, BgL_lblsz00_4077));
					}
					((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4075)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6214), BUNSPEC);
				}
				BgL_auxz00_6213 = ((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4075);
				return ((obj_t) BgL_auxz00_6213);
			}
		}

	}



/* &subst_goto-ev_hook1368 */
	obj_t BGl_z62subst_gotozd2ev_hook1368zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4078, obj_t BgL_exprz00_4079, obj_t BgL_varsz00_4080,
		obj_t BgL_lblsz00_4081)
	{
		{	/* Eval/evaluate_fsize.scm 328 */
			{
				BgL_ev_hookz00_bglt BgL_auxz00_6223;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6224;

					{	/* Eval/evaluate_fsize.scm 330 */
						BgL_ev_exprz00_bglt BgL_arg1737z00_4736;

						BgL_arg1737z00_4736 =
							(((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_exprz00_4079)))->BgL_ez00);
						BgL_auxz00_6224 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1737z00_4736,
								BgL_varsz00_4080, BgL_lblsz00_4081));
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_exprz00_4079)))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6224), BUNSPEC);
				}
				BgL_auxz00_6223 = ((BgL_ev_hookz00_bglt) BgL_exprz00_4079);
				return ((obj_t) BgL_auxz00_6223);
			}
		}

	}



/* &subst_goto-ev_prog21366 */
	obj_t BGl_z62subst_gotozd2ev_prog21366zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4082, obj_t BgL_ez00_4083, obj_t BgL_varsz00_4084,
		obj_t BgL_lblsz00_4085)
	{
		{	/* Eval/evaluate_fsize.scm 322 */
			{
				BgL_ev_prog2z00_bglt BgL_auxz00_6233;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6234;

					{	/* Eval/evaluate_fsize.scm 324 */
						BgL_ev_exprz00_bglt BgL_arg1735z00_4738;

						BgL_arg1735z00_4738 =
							(((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4083)))->BgL_e1z00);
						BgL_auxz00_6234 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1735z00_4738,
								BgL_varsz00_4084, BgL_lblsz00_4085));
					}
					((((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4083)))->BgL_e1z00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6234), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6241;

					{	/* Eval/evaluate_fsize.scm 325 */
						BgL_ev_exprz00_bglt BgL_arg1736z00_4739;

						BgL_arg1736z00_4739 =
							(((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4083)))->BgL_e2z00);
						BgL_auxz00_6241 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1736z00_4739,
								BgL_varsz00_4084, BgL_lblsz00_4085));
					}
					((((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4083)))->BgL_e2z00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6241), BUNSPEC);
				}
				BgL_auxz00_6233 = ((BgL_ev_prog2z00_bglt) BgL_ez00_4083);
				return ((obj_t) BgL_auxz00_6233);
			}
		}

	}



/* &subst_goto-ev_list1364 */
	obj_t BGl_z62subst_gotozd2ev_list1364zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4086, obj_t BgL_ez00_4087, obj_t BgL_varsz00_4088,
		obj_t BgL_lblsz00_4089)
	{
		{	/* Eval/evaluate_fsize.scm 317 */
			{
				BgL_ev_listz00_bglt BgL_auxz00_6250;

				{	/* Eval/evaluate_fsize.scm 319 */
					obj_t BgL_arg1734z00_4741;

					BgL_arg1734z00_4741 =
						(((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_ez00_4087)))->BgL_argsz00);
					{
						obj_t BgL_lz00_4743;

						BgL_lz00_4743 = BgL_arg1734z00_4741;
					BgL_recz00_4742:
						if (NULLP(BgL_lz00_4743))
							{	/* Eval/evaluate_fsize.scm 294 */
								((bool_t) 0);
							}
						else
							{	/* Eval/evaluate_fsize.scm 294 */
								{	/* Eval/evaluate_fsize.scm 295 */
									obj_t BgL_arg1578z00_4744;

									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_arg1579z00_4745;

										BgL_arg1579z00_4745 = CAR(((obj_t) BgL_lz00_4743));
										BgL_arg1578z00_4744 =
											BGl_subst_gotoz00zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1579z00_4745),
											BgL_varsz00_4088, BgL_lblsz00_4089);
									}
									{	/* Eval/evaluate_fsize.scm 295 */
										obj_t BgL_tmpz00_6259;

										BgL_tmpz00_6259 = ((obj_t) BgL_lz00_4743);
										SET_CAR(BgL_tmpz00_6259, BgL_arg1578z00_4744);
									}
								}
								{	/* Eval/evaluate_fsize.scm 296 */
									obj_t BgL_arg1580z00_4746;

									BgL_arg1580z00_4746 = CDR(((obj_t) BgL_lz00_4743));
									{
										obj_t BgL_lz00_6264;

										BgL_lz00_6264 = BgL_arg1580z00_4746;
										BgL_lz00_4743 = BgL_lz00_6264;
										goto BgL_recz00_4742;
									}
								}
							}
					}
				}
				BgL_auxz00_6250 = ((BgL_ev_listz00_bglt) BgL_ez00_4087);
				return ((obj_t) BgL_auxz00_6250);
			}
		}

	}



/* &subst_goto-ev_if1362 */
	obj_t BGl_z62subst_gotozd2ev_if1362zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4090, obj_t BgL_exprz00_4091, obj_t BgL_varsz00_4092,
		obj_t BgL_lblsz00_4093)
	{
		{	/* Eval/evaluate_fsize.scm 310 */
			{
				BgL_ev_ifz00_bglt BgL_auxz00_6267;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6268;

					{	/* Eval/evaluate_fsize.scm 312 */
						BgL_ev_exprz00_bglt BgL_arg1730z00_4748;

						BgL_arg1730z00_4748 =
							(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4091)))->BgL_pz00);
						BgL_auxz00_6268 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1730z00_4748,
								BgL_varsz00_4092, BgL_lblsz00_4093));
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4091)))->BgL_pz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6268), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6275;

					{	/* Eval/evaluate_fsize.scm 313 */
						BgL_ev_exprz00_bglt BgL_arg1731z00_4749;

						BgL_arg1731z00_4749 =
							(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4091)))->BgL_tz00);
						BgL_auxz00_6275 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1731z00_4749,
								BgL_varsz00_4092, BgL_lblsz00_4093));
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4091)))->BgL_tz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6275), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6282;

					{	/* Eval/evaluate_fsize.scm 314 */
						BgL_ev_exprz00_bglt BgL_arg1733z00_4750;

						BgL_arg1733z00_4750 =
							(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4091)))->BgL_ez00);
						BgL_auxz00_6282 =
							((BgL_ev_exprz00_bglt)
							BGl_subst_gotoz00zz__evaluate_fsiza7eza7(BgL_arg1733z00_4750,
								BgL_varsz00_4092, BgL_lblsz00_4093));
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4091)))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6282), BUNSPEC);
				}
				BgL_auxz00_6267 = ((BgL_ev_ifz00_bglt) BgL_exprz00_4091);
				return ((obj_t) BgL_auxz00_6267);
			}
		}

	}



/* &subst_goto-ev_litt1360 */
	obj_t BGl_z62subst_gotozd2ev_litt1360zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4094, obj_t BgL_ez00_4095, obj_t BgL_varsz00_4096,
		obj_t BgL_lblsz00_4097)
	{
		{	/* Eval/evaluate_fsize.scm 307 */
			return ((obj_t) ((BgL_ev_littz00_bglt) BgL_ez00_4095));
		}

	}



/* &subst_goto-ev_global1358 */
	obj_t BGl_z62subst_gotozd2ev_global1358zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4098, obj_t BgL_ez00_4099, obj_t BgL_varsz00_4100,
		obj_t BgL_lblsz00_4101)
	{
		{	/* Eval/evaluate_fsize.scm 304 */
			return ((obj_t) ((BgL_ev_globalz00_bglt) BgL_ez00_4099));
		}

	}



/* &subst_goto-ev_var1356 */
	obj_t BGl_z62subst_gotozd2ev_var1356zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4102, obj_t BgL_ez00_4103, obj_t BgL_varsz00_4104,
		obj_t BgL_lblsz00_4105)
	{
		{	/* Eval/evaluate_fsize.scm 301 */
			return ((obj_t) ((BgL_ev_varz00_bglt) BgL_ez00_4103));
		}

	}



/* &search-letrec-ev_abs1352 */
	obj_t BGl_z62searchzd2letreczd2ev_abs1352z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4106, obj_t BgL_ez00_4107)
	{
		{	/* Eval/evaluate_fsize.scm 260 */
			{
				BgL_ev_absz00_bglt BgL_auxz00_6297;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6298;

					{	/* Eval/evaluate_fsize.scm 262 */
						BgL_ev_exprz00_bglt BgL_arg1729z00_4755;

						BgL_arg1729z00_4755 =
							(((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_4107)))->BgL_bodyz00);
						BgL_auxz00_6298 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1729z00_4755));
					}
					((((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_4107)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6298), BUNSPEC);
				}
				BgL_auxz00_6297 = ((BgL_ev_absz00_bglt) BgL_ez00_4107);
				return ((obj_t) BgL_auxz00_6297);
			}
		}

	}



/* &search-letrec-ev_app1350 */
	obj_t BGl_z62searchzd2letreczd2ev_app1350z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4108, obj_t BgL_ez00_4109)
	{
		{	/* Eval/evaluate_fsize.scm 254 */
			{
				BgL_ev_appz00_bglt BgL_auxz00_6307;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6308;

					{	/* Eval/evaluate_fsize.scm 256 */
						BgL_ev_exprz00_bglt BgL_arg1727z00_4757;

						BgL_arg1727z00_4757 =
							(((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_4109)))->BgL_funz00);
						BgL_auxz00_6308 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1727z00_4757));
					}
					((((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_4109)))->BgL_funz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6308), BUNSPEC);
				}
				BGl_searchzd2letrecza2z70zz__evaluate_fsiza7eza7(
					(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_4109)))->BgL_argsz00));
				BgL_auxz00_6307 = ((BgL_ev_appz00_bglt) BgL_ez00_4109);
				return ((obj_t) BgL_auxz00_6307);
			}
		}

	}



/* &search-letrec-ev_let1348 */
	obj_t BGl_z62searchzd2letreczd2ev_let1348z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4110, obj_t BgL_ez00_4111)
	{
		{	/* Eval/evaluate_fsize.scm 243 */
			{
				BgL_ev_exprz00_bglt BgL_auxz00_6320;

				{	/* Eval/evaluate_fsize.scm 245 */
					obj_t BgL_arg1717z00_4759;

					BgL_arg1717z00_4759 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_valsz00);
					BGl_searchzd2letrecza2z70zz__evaluate_fsiza7eza7(BgL_arg1717z00_4759);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6325;

					{	/* Eval/evaluate_fsize.scm 246 */
						BgL_ev_exprz00_bglt BgL_arg1718z00_4760;

						BgL_arg1718z00_4760 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_bodyz00);
						BgL_auxz00_6325 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1718z00_4760));
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6325), BUNSPEC);
				}
				{	/* Eval/evaluate_fsize.scm 250 */
					bool_t BgL_test2621z00_6334;

					{	/* Eval/evaluate_fsize.scm 247 */
						obj_t BgL_arg1724z00_4761;
						obj_t BgL_arg1725z00_4762;
						BgL_ev_exprz00_bglt BgL_arg1726z00_4763;

						BgL_arg1724z00_4761 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_varsz00);
						BgL_arg1725z00_4762 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_valsz00);
						BgL_arg1726z00_4763 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_bodyz00);
						BgL_test2621z00_6334 =
							BGl_letrectailzf3zf3zz__evaluate_fsiza7eza7(BgL_arg1724z00_4761,
							BgL_arg1725z00_4762, BgL_arg1726z00_4763);
					}
					if (BgL_test2621z00_6334)
						{	/* Eval/evaluate_fsize.scm 251 */
							obj_t BgL_arg1720z00_4764;
							obj_t BgL_arg1722z00_4765;
							BgL_ev_exprz00_bglt BgL_arg1723z00_4766;

							BgL_arg1720z00_4764 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_varsz00);
							BgL_arg1722z00_4765 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_valsz00);
							BgL_arg1723z00_4766 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_4111))))->BgL_bodyz00);
							BgL_auxz00_6320 =
								((BgL_ev_exprz00_bglt)
								BGl_modifyzd2letreczd2zz__evaluate_fsiza7eza7
								(BgL_arg1720z00_4764, BgL_arg1722z00_4765,
									BgL_arg1723z00_4766));
						}
					else
						{	/* Eval/evaluate_fsize.scm 250 */
							BgL_auxz00_6320 =
								((BgL_ev_exprz00_bglt) ((BgL_ev_letrecz00_bglt) BgL_ez00_4111));
						}
				}
				return ((obj_t) BgL_auxz00_6320);
			}
		}

	}



/* &search-letrec-ev_bin1346 */
	obj_t BGl_z62searchzd2letreczd2ev_bin1346z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4112, obj_t BgL_ez00_4113)
	{
		{	/* Eval/evaluate_fsize.scm 237 */
			{
				BgL_ev_binderz00_bglt BgL_auxz00_6359;

				BGl_searchzd2letrecza2z70zz__evaluate_fsiza7eza7(
					(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_ez00_4113)))->BgL_valsz00));
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6363;

					{	/* Eval/evaluate_fsize.scm 240 */
						BgL_ev_exprz00_bglt BgL_arg1715z00_4768;

						BgL_arg1715z00_4768 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt) BgL_ez00_4113)))->BgL_bodyz00);
						BgL_auxz00_6363 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1715z00_4768));
					}
					((((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt) BgL_ez00_4113)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6363), BUNSPEC);
				}
				BgL_auxz00_6359 = ((BgL_ev_binderz00_bglt) BgL_ez00_4113);
				return ((obj_t) BgL_auxz00_6359);
			}
		}

	}



/* &search-letrec-ev_syn1344 */
	obj_t BGl_z62searchzd2letreczd2ev_syn1344z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4114, obj_t BgL_ez00_4115)
	{
		{	/* Eval/evaluate_fsize.scm 230 */
			{
				BgL_ev_synchroniza7eza7_bglt BgL_auxz00_6372;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6373;

					{	/* Eval/evaluate_fsize.scm 232 */
						BgL_ev_exprz00_bglt BgL_arg1709z00_4770;

						BgL_arg1709z00_4770 =
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115)))->
							BgL_mutexz00);
						BgL_auxz00_6373 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1709z00_4770));
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115)))->
							BgL_mutexz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6373), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6380;

					{	/* Eval/evaluate_fsize.scm 233 */
						BgL_ev_exprz00_bglt BgL_arg1710z00_4771;

						BgL_arg1710z00_4771 =
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115)))->
							BgL_prelockz00);
						BgL_auxz00_6380 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1710z00_4771));
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115)))->
							BgL_prelockz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6380), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6387;

					{	/* Eval/evaluate_fsize.scm 234 */
						BgL_ev_exprz00_bglt BgL_arg1711z00_4772;

						BgL_arg1711z00_4772 =
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115)))->
							BgL_bodyz00);
						BgL_auxz00_6387 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1711z00_4772));
					}
					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6387), BUNSPEC);
				}
				BgL_auxz00_6372 = ((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4115);
				return ((obj_t) BgL_auxz00_6372);
			}
		}

	}



/* &search-letrec-ev_wit1342 */
	obj_t BGl_z62searchzd2letreczd2ev_wit1342z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4116, obj_t BgL_ez00_4117)
	{
		{	/* Eval/evaluate_fsize.scm 224 */
			{
				BgL_ev_withzd2handlerzd2_bglt BgL_auxz00_6396;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6397;

					{	/* Eval/evaluate_fsize.scm 226 */
						BgL_ev_exprz00_bglt BgL_arg1707z00_4774;

						BgL_arg1707z00_4774 =
							(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4117)))->
							BgL_handlerz00);
						BgL_auxz00_6397 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1707z00_4774));
					}
					((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4117)))->
							BgL_handlerz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6397), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6404;

					{	/* Eval/evaluate_fsize.scm 227 */
						BgL_ev_exprz00_bglt BgL_arg1708z00_4775;

						BgL_arg1708z00_4775 =
							(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4117)))->
							BgL_bodyz00);
						BgL_auxz00_6404 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1708z00_4775));
					}
					((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4117)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6404), BUNSPEC);
				}
				BgL_auxz00_6396 = ((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4117);
				return ((obj_t) BgL_auxz00_6396);
			}
		}

	}



/* &search-letrec-ev_unw1340 */
	obj_t BGl_z62searchzd2letreczd2ev_unw1340z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4118, obj_t BgL_exprz00_4119)
	{
		{	/* Eval/evaluate_fsize.scm 218 */
			{
				BgL_ev_unwindzd2protectzd2_bglt BgL_auxz00_6413;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6414;

					{	/* Eval/evaluate_fsize.scm 220 */
						BgL_ev_exprz00_bglt BgL_arg1705z00_4777;

						BgL_arg1705z00_4777 =
							(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4119)))->
							BgL_ez00);
						BgL_auxz00_6414 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1705z00_4777));
					}
					((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4119)))->
							BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6414), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6421;

					{	/* Eval/evaluate_fsize.scm 221 */
						BgL_ev_exprz00_bglt BgL_arg1706z00_4778;

						BgL_arg1706z00_4778 =
							(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4119)))->
							BgL_bodyz00);
						BgL_auxz00_6421 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1706z00_4778));
					}
					((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4119)))->
							BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6421), BUNSPEC);
				}
				BgL_auxz00_6413 = ((BgL_ev_unwindzd2protectzd2_bglt) BgL_exprz00_4119);
				return ((obj_t) BgL_auxz00_6413);
			}
		}

	}



/* &search-letrec-ev_bin1338 */
	obj_t BGl_z62searchzd2letreczd2ev_bin1338z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4120, obj_t BgL_ez00_4121)
	{
		{	/* Eval/evaluate_fsize.scm 213 */
			{
				BgL_ev_bindzd2exitzd2_bglt BgL_auxz00_6430;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6431;

					{	/* Eval/evaluate_fsize.scm 215 */
						BgL_ev_exprz00_bglt BgL_arg1704z00_4780;

						BgL_arg1704z00_4780 =
							(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4121)))->BgL_bodyz00);
						BgL_auxz00_6431 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1704z00_4780));
					}
					((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4121)))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6431), BUNSPEC);
				}
				BgL_auxz00_6430 = ((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4121);
				return ((obj_t) BgL_auxz00_6430);
			}
		}

	}



/* &search-letrec-ev_hoo1336 */
	obj_t BGl_z62searchzd2letreczd2ev_hoo1336z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4122, obj_t BgL_exprz00_4123)
	{
		{	/* Eval/evaluate_fsize.scm 208 */
			{
				BgL_ev_hookz00_bglt BgL_auxz00_6440;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6441;

					{	/* Eval/evaluate_fsize.scm 210 */
						BgL_ev_exprz00_bglt BgL_arg1703z00_4782;

						BgL_arg1703z00_4782 =
							(((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_exprz00_4123)))->BgL_ez00);
						BgL_auxz00_6441 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1703z00_4782));
					}
					((((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_exprz00_4123)))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6441), BUNSPEC);
				}
				BgL_auxz00_6440 = ((BgL_ev_hookz00_bglt) BgL_exprz00_4123);
				return ((obj_t) BgL_auxz00_6440);
			}
		}

	}



/* &search-letrec-ev_pro1334 */
	obj_t BGl_z62searchzd2letreczd2ev_pro1334z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4124, obj_t BgL_ez00_4125)
	{
		{	/* Eval/evaluate_fsize.scm 202 */
			{
				BgL_ev_prog2z00_bglt BgL_auxz00_6450;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6451;

					{	/* Eval/evaluate_fsize.scm 204 */
						BgL_ev_exprz00_bglt BgL_arg1701z00_4784;

						BgL_arg1701z00_4784 =
							(((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4125)))->BgL_e1z00);
						BgL_auxz00_6451 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1701z00_4784));
					}
					((((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4125)))->BgL_e1z00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6451), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6458;

					{	/* Eval/evaluate_fsize.scm 205 */
						BgL_ev_exprz00_bglt BgL_arg1702z00_4785;

						BgL_arg1702z00_4785 =
							(((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4125)))->BgL_e2z00);
						BgL_auxz00_6458 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1702z00_4785));
					}
					((((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4125)))->BgL_e2z00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6458), BUNSPEC);
				}
				BgL_auxz00_6450 = ((BgL_ev_prog2z00_bglt) BgL_ez00_4125);
				return ((obj_t) BgL_auxz00_6450);
			}
		}

	}



/* &search-letrec-ev_lis1332 */
	obj_t BGl_z62searchzd2letreczd2ev_lis1332z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4126, obj_t BgL_ez00_4127)
	{
		{	/* Eval/evaluate_fsize.scm 197 */
			{
				BgL_ev_listz00_bglt BgL_auxz00_6467;

				BGl_searchzd2letrecza2z70zz__evaluate_fsiza7eza7(
					(((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_ez00_4127)))->BgL_argsz00));
				BgL_auxz00_6467 = ((BgL_ev_listz00_bglt) BgL_ez00_4127);
				return ((obj_t) BgL_auxz00_6467);
			}
		}

	}



/* &search-letrec-ev_if1330 */
	obj_t BGl_z62searchzd2letreczd2ev_if1330z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4128, obj_t BgL_exprz00_4129)
	{
		{	/* Eval/evaluate_fsize.scm 190 */
			{
				BgL_ev_ifz00_bglt BgL_auxz00_6473;

				{
					BgL_ev_exprz00_bglt BgL_auxz00_6474;

					{	/* Eval/evaluate_fsize.scm 192 */
						BgL_ev_exprz00_bglt BgL_arg1691z00_4788;

						BgL_arg1691z00_4788 =
							(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4129)))->BgL_pz00);
						BgL_auxz00_6474 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1691z00_4788));
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4129)))->BgL_pz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6474), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6481;

					{	/* Eval/evaluate_fsize.scm 193 */
						BgL_ev_exprz00_bglt BgL_arg1692z00_4789;

						BgL_arg1692z00_4789 =
							(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4129)))->BgL_tz00);
						BgL_auxz00_6481 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1692z00_4789));
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4129)))->BgL_tz00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6481), BUNSPEC);
				}
				{
					BgL_ev_exprz00_bglt BgL_auxz00_6488;

					{	/* Eval/evaluate_fsize.scm 194 */
						BgL_ev_exprz00_bglt BgL_arg1699z00_4790;

						BgL_arg1699z00_4790 =
							(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4129)))->BgL_ez00);
						BgL_auxz00_6488 =
							((BgL_ev_exprz00_bglt)
							BGl_searchzd2letreczd2zz__evaluate_fsiza7eza7
							(BgL_arg1699z00_4790));
					}
					((((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_exprz00_4129)))->BgL_ez00) =
						((BgL_ev_exprz00_bglt) BgL_auxz00_6488), BUNSPEC);
				}
				BgL_auxz00_6473 = ((BgL_ev_ifz00_bglt) BgL_exprz00_4129);
				return ((obj_t) BgL_auxz00_6473);
			}
		}

	}



/* &search-letrec-ev_lit1328 */
	obj_t BGl_z62searchzd2letreczd2ev_lit1328z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4130, obj_t BgL_ez00_4131)
	{
		{	/* Eval/evaluate_fsize.scm 187 */
			return ((obj_t) ((BgL_ev_littz00_bglt) BgL_ez00_4131));
		}

	}



/* &search-letrec-ev_glo1326 */
	obj_t BGl_z62searchzd2letreczd2ev_glo1326z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4132, obj_t BgL_ez00_4133)
	{
		{	/* Eval/evaluate_fsize.scm 184 */
			return ((obj_t) ((BgL_ev_globalz00_bglt) BgL_ez00_4133));
		}

	}



/* &search-letrec-ev_var1324 */
	obj_t BGl_z62searchzd2letreczd2ev_var1324z62zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4134, obj_t BgL_ez00_4135)
	{
		{	/* Eval/evaluate_fsize.scm 181 */
			return ((obj_t) ((BgL_ev_varz00_bglt) BgL_ez00_4135));
		}

	}



/* &fsize-ev_abs1320 */
	obj_t BGl_z62fsiza7ezd2ev_abs1320z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4136, obj_t BgL_ez00_4137, obj_t BgL_nz00_4138)
	{
		{	/* Eval/evaluate_fsize.scm 158 */
			{	/* Eval/evaluate_fsize.scm 159 */
				int BgL_tmpz00_6503;

				{	/* Eval/evaluate_fsize.scm 159 */
					int BgL_nz00_4795;

					BgL_nz00_4795 = CINT(BgL_nz00_4138);
					{	/* Eval/evaluate_fsize.scm 160 */
						int BgL_nnz00_4796;

						{	/* Eval/evaluate_fsize.scm 160 */
							BgL_ev_exprz00_bglt BgL_arg1685z00_4797;
							long BgL_arg1688z00_4798;

							BgL_arg1685z00_4797 =
								(((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_ez00_4137)))->BgL_bodyz00);
							BgL_arg1688z00_4798 =
								bgl_list_length(
								(((BgL_ev_absz00_bglt) COBJECT(
											((BgL_ev_absz00_bglt) BgL_ez00_4137)))->BgL_varsz00));
							BgL_nnz00_4796 =
								BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_arg1685z00_4797,
								(int) (BgL_arg1688z00_4798));
						}
						((((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_ez00_4137)))->BgL_siza7eza7) =
							((int) BgL_nnz00_4796), BUNSPEC);
						BgL_tmpz00_6503 = BgL_nz00_4795;
				}}
				return BINT(BgL_tmpz00_6503);
			}
		}

	}



/* &fsize-ev_app1318 */
	obj_t BGl_z62fsiza7ezd2ev_app1318z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4139, obj_t BgL_ez00_4140, obj_t BgL_nz00_4141)
	{
		{	/* Eval/evaluate_fsize.scm 151 */
			{	/* Eval/evaluate_fsize.scm 152 */
				int BgL_nz00_4800;

				BgL_nz00_4800 = CINT(BgL_nz00_4141);
				{	/* Eval/evaluate_fsize.scm 153 */
					int BgL_g1079z00_4801;

					BgL_g1079z00_4801 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_4140)))->BgL_funz00),
						BgL_nz00_4800);
					{	/* Eval/evaluate_fsize.scm 153 */
						obj_t BgL_arg1670z00_4802;

						BgL_arg1670z00_4802 =
							(((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_4140)))->BgL_argsz00);
						{
							obj_t BgL_lz00_4804;
							int BgL_nz00_4805;
							obj_t BgL_rz00_4806;

							BgL_lz00_4804 = BgL_arg1670z00_4802;
							BgL_nz00_4805 = BgL_nz00_4800;
							BgL_rz00_4806 = BINT(BgL_g1079z00_4801);
						BgL_recz00_4803:
							if (NULLP(BgL_lz00_4804))
								{	/* Eval/evaluate_fsize.scm 154 */
									return
										BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_nz00_4805),
										BgL_rz00_4806);
								}
							else
								{	/* Eval/evaluate_fsize.scm 156 */
									obj_t BgL_arg1675z00_4807;
									long BgL_arg1676z00_4808;
									obj_t BgL_arg1678z00_4809;

									BgL_arg1675z00_4807 = CDR(((obj_t) BgL_lz00_4804));
									BgL_arg1676z00_4808 = ((long) (BgL_nz00_4805) + 1L);
									{	/* Eval/evaluate_fsize.scm 156 */
										int BgL_xz00_4810;

										{	/* Eval/evaluate_fsize.scm 156 */
											obj_t BgL_arg1681z00_4811;

											BgL_arg1681z00_4811 = CAR(((obj_t) BgL_lz00_4804));
											BgL_xz00_4810 =
												BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
												((BgL_ev_exprz00_bglt) BgL_arg1681z00_4811),
												BgL_nz00_4805);
										}
										BgL_arg1678z00_4809 =
											BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4810),
											BgL_rz00_4806);
									}
									{
										obj_t BgL_rz00_6538;
										int BgL_nz00_6536;
										obj_t BgL_lz00_6535;

										BgL_lz00_6535 = BgL_arg1675z00_4807;
										BgL_nz00_6536 = (int) (BgL_arg1676z00_4808);
										BgL_rz00_6538 = BgL_arg1678z00_4809;
										BgL_rz00_4806 = BgL_rz00_6538;
										BgL_nz00_4805 = BgL_nz00_6536;
										BgL_lz00_4804 = BgL_lz00_6535;
										goto BgL_recz00_4803;
									}
								}
						}
					}
				}
			}
		}

	}



/* &fsize-ev_goto1316 */
	obj_t BGl_z62fsiza7ezd2ev_goto1316z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4142, obj_t BgL_ez00_4143, obj_t BgL_nz00_4144)
	{
		{	/* Eval/evaluate_fsize.scm 144 */
			{	/* Eval/evaluate_fsize.scm 146 */
				obj_t BgL_arg1661z00_4813;

				BgL_arg1661z00_4813 =
					(((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_ez00_4143)))->BgL_argsz00);
				{
					obj_t BgL_lz00_4815;
					obj_t BgL_nz00_4816;
					obj_t BgL_rz00_4817;

					BgL_lz00_4815 = BgL_arg1661z00_4813;
					BgL_nz00_4816 = BgL_nz00_4144;
					BgL_rz00_4817 = BgL_nz00_4144;
				BgL_recz00_4814:
					if (NULLP(BgL_lz00_4815))
						{	/* Eval/evaluate_fsize.scm 147 */
							return
								BGl_2maxz00zz__r4_numbers_6_5z00(BgL_nz00_4816, BgL_rz00_4817);
						}
					else
						{	/* Eval/evaluate_fsize.scm 149 */
							obj_t BgL_arg1664z00_4818;
							long BgL_arg1667z00_4819;
							obj_t BgL_arg1668z00_4820;

							BgL_arg1664z00_4818 = CDR(((obj_t) BgL_lz00_4815));
							BgL_arg1667z00_4819 = ((long) CINT(BgL_nz00_4816) + 1L);
							{	/* Eval/evaluate_fsize.scm 149 */
								int BgL_xz00_4821;

								{	/* Eval/evaluate_fsize.scm 149 */
									obj_t BgL_arg1669z00_4822;

									BgL_arg1669z00_4822 = CAR(((obj_t) BgL_lz00_4815));
									BgL_xz00_4821 =
										BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
										((BgL_ev_exprz00_bglt) BgL_arg1669z00_4822),
										CINT(BgL_nz00_4816));
								}
								BgL_arg1668z00_4820 =
									BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4821),
									BgL_rz00_4817);
							}
							{
								obj_t BgL_rz00_6559;
								obj_t BgL_nz00_6557;
								obj_t BgL_lz00_6556;

								BgL_lz00_6556 = BgL_arg1664z00_4818;
								BgL_nz00_6557 = BINT(BgL_arg1667z00_4819);
								BgL_rz00_6559 = BgL_arg1668z00_4820;
								BgL_rz00_4817 = BgL_rz00_6559;
								BgL_nz00_4816 = BgL_nz00_6557;
								BgL_lz00_4815 = BgL_lz00_6556;
								goto BgL_recz00_4814;
							}
						}
				}
			}
		}

	}



/* &fsize-ev_labels1314 */
	obj_t BGl_z62fsiza7ezd2ev_labels1314z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4145, obj_t BgL_ez00_4146, obj_t BgL_nz00_4147)
	{
		{	/* Eval/evaluate_fsize.scm 137 */
			{
				obj_t BgL_lz00_4825;
				obj_t BgL_rz00_4826;

				BgL_lz00_4825 =
					(((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_ez00_4146)))->BgL_valsz00);
				BgL_rz00_4826 = BgL_nz00_4147;
			BgL_recz00_4824:
				if (NULLP(BgL_lz00_4825))
					{	/* Eval/evaluate_fsize.scm 141 */
						int BgL_xz00_4827;

						BgL_xz00_4827 =
							BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
							(((BgL_ev_labelsz00_bglt) COBJECT(
										((BgL_ev_labelsz00_bglt) BgL_ez00_4146)))->BgL_bodyz00),
							CINT(BgL_nz00_4147));
						return
							BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4827),
							BgL_rz00_4826);
					}
				else
					{	/* Eval/evaluate_fsize.scm 142 */
						obj_t BgL_arg1652z00_4828;
						obj_t BgL_arg1653z00_4829;

						BgL_arg1652z00_4828 = CDR(((obj_t) BgL_lz00_4825));
						{	/* Eval/evaluate_fsize.scm 142 */
							int BgL_xz00_4830;

							{	/* Eval/evaluate_fsize.scm 142 */
								obj_t BgL_arg1654z00_4831;
								long BgL_arg1656z00_4832;

								{	/* Eval/evaluate_fsize.scm 142 */
									obj_t BgL_pairz00_4833;

									BgL_pairz00_4833 = CAR(((obj_t) BgL_lz00_4825));
									BgL_arg1654z00_4831 = CDR(BgL_pairz00_4833);
								}
								{	/* Eval/evaluate_fsize.scm 142 */
									long BgL_tmpz00_6573;

									{	/* Eval/evaluate_fsize.scm 142 */
										obj_t BgL_auxz00_6575;

										{	/* Eval/evaluate_fsize.scm 142 */
											obj_t BgL_pairz00_4834;

											BgL_pairz00_4834 = CAR(((obj_t) BgL_lz00_4825));
											BgL_auxz00_6575 = CAR(BgL_pairz00_4834);
										}
										BgL_tmpz00_6573 = bgl_list_length(BgL_auxz00_6575);
									}
									BgL_arg1656z00_4832 =
										((long) CINT(BgL_nz00_4147) + BgL_tmpz00_6573);
								}
								BgL_xz00_4830 =
									BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
									((BgL_ev_exprz00_bglt) BgL_arg1654z00_4831),
									(int) (BgL_arg1656z00_4832));
							}
							BgL_arg1653z00_4829 =
								BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4830),
								BgL_rz00_4826);
						}
						{
							obj_t BgL_rz00_6587;
							obj_t BgL_lz00_6586;

							BgL_lz00_6586 = BgL_arg1652z00_4828;
							BgL_rz00_6587 = BgL_arg1653z00_4829;
							BgL_rz00_4826 = BgL_rz00_6587;
							BgL_lz00_4825 = BgL_lz00_6586;
							goto BgL_recz00_4824;
						}
					}
			}
		}

	}



/* &fsize-ev_letrec1312 */
	obj_t BGl_z62fsiza7ezd2ev_letrec1312z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4148, obj_t BgL_ez00_4149, obj_t BgL_nz00_4150)
	{
		{	/* Eval/evaluate_fsize.scm 129 */
			{	/* Eval/evaluate_fsize.scm 130 */
				int BgL_nz00_4836;

				BgL_nz00_4836 = CINT(BgL_nz00_4150);
				{	/* Eval/evaluate_fsize.scm 131 */
					long BgL_nz00_4837;

					BgL_nz00_4837 =
						(
						(long) (BgL_nz00_4836) +
						bgl_list_length(
							(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_4149))))->
								BgL_valsz00)));
					{
						obj_t BgL_lz00_4839;
						obj_t BgL_rz00_4840;

						{	/* Eval/evaluate_fsize.scm 132 */
							obj_t BgL_arg1638z00_4847;

							BgL_arg1638z00_4847 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_4149))))->BgL_valsz00);
							BgL_lz00_4839 = BgL_arg1638z00_4847;
							BgL_rz00_4840 = BINT(BgL_nz00_4837);
						BgL_recz00_4838:
							if (NULLP(BgL_lz00_4839))
								{	/* Eval/evaluate_fsize.scm 134 */
									int BgL_xz00_4841;

									{	/* Eval/evaluate_fsize.scm 134 */
										BgL_ev_exprz00_bglt BgL_arg1641z00_4842;

										BgL_arg1641z00_4842 =
											(((BgL_ev_binderz00_bglt) COBJECT(
													((BgL_ev_binderz00_bglt)
														((BgL_ev_letrecz00_bglt) BgL_ez00_4149))))->
											BgL_bodyz00);
										BgL_xz00_4841 =
											BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_arg1641z00_4842,
											(int) (BgL_nz00_4837));
									}
									return
										BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4841),
										BgL_rz00_4840);
								}
							else
								{	/* Eval/evaluate_fsize.scm 135 */
									obj_t BgL_arg1642z00_4843;
									obj_t BgL_arg1643z00_4844;

									BgL_arg1642z00_4843 = CDR(((obj_t) BgL_lz00_4839));
									{	/* Eval/evaluate_fsize.scm 135 */
										int BgL_xz00_4845;

										{	/* Eval/evaluate_fsize.scm 135 */
											obj_t BgL_arg1644z00_4846;

											BgL_arg1644z00_4846 = CAR(((obj_t) BgL_lz00_4839));
											BgL_xz00_4845 =
												BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
												((BgL_ev_exprz00_bglt) BgL_arg1644z00_4846),
												(int) (BgL_nz00_4837));
										}
										BgL_arg1643z00_4844 =
											BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4845),
											BgL_rz00_4840);
									}
									{
										obj_t BgL_rz00_6619;
										obj_t BgL_lz00_6618;

										BgL_lz00_6618 = BgL_arg1642z00_4843;
										BgL_rz00_6619 = BgL_arg1643z00_4844;
										BgL_rz00_4840 = BgL_rz00_6619;
										BgL_lz00_4839 = BgL_lz00_6618;
										goto BgL_recz00_4838;
									}
								}
						}
					}
				}
			}
		}

	}



/* &fsize-ev_let*1310 */
	obj_t BGl_z62fsiza7ezd2ev_letza21310zb5zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4151, obj_t BgL_ez00_4152, obj_t BgL_nz00_4153)
	{
		{	/* Eval/evaluate_fsize.scm 122 */
			{	/* Eval/evaluate_fsize.scm 123 */
				int BgL_nz00_4849;

				BgL_nz00_4849 = CINT(BgL_nz00_4153);
				{	/* Eval/evaluate_fsize.scm 124 */
					obj_t BgL_arg1627z00_4850;

					BgL_arg1627z00_4850 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_ez00_4152))))->BgL_valsz00);
					{
						obj_t BgL_lz00_4852;
						int BgL_nz00_4853;
						obj_t BgL_rz00_4854;

						BgL_lz00_4852 = BgL_arg1627z00_4850;
						BgL_nz00_4853 = BgL_nz00_4849;
						BgL_rz00_4854 = BINT(BgL_nz00_4849);
					BgL_recz00_4851:
						if (NULLP(BgL_lz00_4852))
							{	/* Eval/evaluate_fsize.scm 126 */
								int BgL_xz00_4855;

								{	/* Eval/evaluate_fsize.scm 126 */
									BgL_ev_exprz00_bglt BgL_arg1630z00_4856;

									BgL_arg1630z00_4856 =
										(((BgL_ev_binderz00_bglt) COBJECT(
												((BgL_ev_binderz00_bglt)
													((BgL_ev_letza2za2_bglt) BgL_ez00_4152))))->
										BgL_bodyz00);
									BgL_xz00_4855 =
										BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_arg1630z00_4856,
										BgL_nz00_4853);
								}
								return
									BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4855),
									BgL_rz00_4854);
							}
						else
							{	/* Eval/evaluate_fsize.scm 127 */
								obj_t BgL_arg1631z00_4857;
								long BgL_arg1634z00_4858;
								obj_t BgL_arg1636z00_4859;

								BgL_arg1631z00_4857 = CDR(((obj_t) BgL_lz00_4852));
								BgL_arg1634z00_4858 = ((long) (BgL_nz00_4853) + 1L);
								{	/* Eval/evaluate_fsize.scm 127 */
									int BgL_xz00_4860;

									{	/* Eval/evaluate_fsize.scm 127 */
										obj_t BgL_arg1637z00_4861;

										BgL_arg1637z00_4861 = CAR(((obj_t) BgL_lz00_4852));
										BgL_xz00_4860 =
											BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1637z00_4861),
											BgL_nz00_4853);
									}
									BgL_arg1636z00_4859 =
										BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4860),
										BgL_rz00_4854);
								}
								{
									obj_t BgL_rz00_6646;
									int BgL_nz00_6644;
									obj_t BgL_lz00_6643;

									BgL_lz00_6643 = BgL_arg1631z00_4857;
									BgL_nz00_6644 = (int) (BgL_arg1634z00_4858);
									BgL_rz00_6646 = BgL_arg1636z00_4859;
									BgL_rz00_4854 = BgL_rz00_6646;
									BgL_nz00_4853 = BgL_nz00_6644;
									BgL_lz00_4852 = BgL_lz00_6643;
									goto BgL_recz00_4851;
								}
							}
					}
				}
			}
		}

	}



/* &fsize-ev_let1308 */
	obj_t BGl_z62fsiza7ezd2ev_let1308z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4154, obj_t BgL_ez00_4155, obj_t BgL_nz00_4156)
	{
		{	/* Eval/evaluate_fsize.scm 115 */
			{	/* Eval/evaluate_fsize.scm 116 */
				int BgL_nz00_4863;

				BgL_nz00_4863 = CINT(BgL_nz00_4156);
				{	/* Eval/evaluate_fsize.scm 117 */
					obj_t BgL_arg1619z00_4864;

					BgL_arg1619z00_4864 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letz00_bglt) BgL_ez00_4155))))->BgL_valsz00);
					{
						obj_t BgL_lz00_4866;
						int BgL_nz00_4867;
						obj_t BgL_rz00_4868;

						BgL_lz00_4866 = BgL_arg1619z00_4864;
						BgL_nz00_4867 = BgL_nz00_4863;
						BgL_rz00_4868 = BINT(BgL_nz00_4863);
					BgL_recz00_4865:
						if (NULLP(BgL_lz00_4866))
							{	/* Eval/evaluate_fsize.scm 119 */
								int BgL_xz00_4869;

								{	/* Eval/evaluate_fsize.scm 119 */
									BgL_ev_exprz00_bglt BgL_arg1622z00_4870;

									BgL_arg1622z00_4870 =
										(((BgL_ev_binderz00_bglt) COBJECT(
												((BgL_ev_binderz00_bglt)
													((BgL_ev_letz00_bglt) BgL_ez00_4155))))->BgL_bodyz00);
									BgL_xz00_4869 =
										BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_arg1622z00_4870,
										BgL_nz00_4867);
								}
								return
									BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4869),
									BgL_rz00_4868);
							}
						else
							{	/* Eval/evaluate_fsize.scm 120 */
								obj_t BgL_arg1623z00_4871;
								long BgL_arg1624z00_4872;
								obj_t BgL_arg1625z00_4873;

								BgL_arg1623z00_4871 = CDR(((obj_t) BgL_lz00_4866));
								BgL_arg1624z00_4872 = ((long) (BgL_nz00_4867) + 1L);
								{	/* Eval/evaluate_fsize.scm 120 */
									int BgL_xz00_4874;

									{	/* Eval/evaluate_fsize.scm 120 */
										obj_t BgL_arg1626z00_4875;

										BgL_arg1626z00_4875 = CAR(((obj_t) BgL_lz00_4866));
										BgL_xz00_4874 =
											BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1626z00_4875),
											BgL_nz00_4867);
									}
									BgL_arg1625z00_4873 =
										BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4874),
										BgL_rz00_4868);
								}
								{
									obj_t BgL_rz00_6673;
									int BgL_nz00_6671;
									obj_t BgL_lz00_6670;

									BgL_lz00_6670 = BgL_arg1623z00_4871;
									BgL_nz00_6671 = (int) (BgL_arg1624z00_4872);
									BgL_rz00_6673 = BgL_arg1625z00_4873;
									BgL_rz00_4868 = BgL_rz00_6673;
									BgL_nz00_4867 = BgL_nz00_6671;
									BgL_lz00_4866 = BgL_lz00_6670;
									goto BgL_recz00_4865;
								}
							}
					}
				}
			}
		}

	}



/* &fsize-ev_synchronize1306 */
	obj_t BGl_z62fsiza7ezd2ev_synchroniza7e1306zb0zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4157, obj_t BgL_ez00_4158, obj_t BgL_nz00_4159)
	{
		{	/* Eval/evaluate_fsize.scm 111 */
			{	/* Eval/evaluate_fsize.scm 112 */
				int BgL_nz00_4877;

				BgL_nz00_4877 = CINT(BgL_nz00_4159);
				{	/* Eval/evaluate_fsize.scm 113 */
					int BgL_arg1613z00_4878;
					obj_t BgL_arg1615z00_4879;

					BgL_arg1613z00_4878 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4158)))->
							BgL_mutexz00), BgL_nz00_4877);
					{	/* Eval/evaluate_fsize.scm 113 */
						int BgL_xz00_4880;
						int BgL_yz00_4881;

						BgL_xz00_4880 =
							BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
							(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
										((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4158)))->
								BgL_prelockz00), BgL_nz00_4877);
						BgL_yz00_4881 =
							BGl_fsiza7eza7zz__evaluate_fsiza7eza7(((
									(BgL_ev_synchroniza7eza7_bglt)
									COBJECT(((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_4158)))->
								BgL_bodyz00), BgL_nz00_4877);
						BgL_arg1615z00_4879 =
							BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4880),
							BINT(BgL_yz00_4881));
					}
					return
						BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_arg1613z00_4878),
						BgL_arg1615z00_4879);
				}
			}
		}

	}



/* &fsize-ev_with-handle1304 */
	obj_t BGl_z62fsiza7ezd2ev_withzd2handle1304zc5zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4160, obj_t BgL_ez00_4161, obj_t BgL_nz00_4162)
	{
		{	/* Eval/evaluate_fsize.scm 107 */
			{	/* Eval/evaluate_fsize.scm 108 */
				int BgL_nz00_4883;

				BgL_nz00_4883 = CINT(BgL_nz00_4162);
				{	/* Eval/evaluate_fsize.scm 109 */
					int BgL_xz00_4884;
					int BgL_yz00_4885;

					BgL_xz00_4884 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4161)))->
							BgL_handlerz00), BgL_nz00_4883);
					BgL_yz00_4885 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(((
								(BgL_ev_withzd2handlerzd2_bglt)
								COBJECT(((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_4161)))->
							BgL_bodyz00), BgL_nz00_4883);
					return BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4884),
						BINT(BgL_yz00_4885));
				}
			}
		}

	}



/* &fsize-ev_unwind-prot1302 */
	obj_t BGl_z62fsiza7ezd2ev_unwindzd2prot1302zc5zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4163, obj_t BgL_ez00_4164, obj_t BgL_nz00_4165)
	{
		{	/* Eval/evaluate_fsize.scm 103 */
			{	/* Eval/evaluate_fsize.scm 104 */
				int BgL_nz00_4887;

				BgL_nz00_4887 = CINT(BgL_nz00_4165);
				{	/* Eval/evaluate_fsize.scm 105 */
					int BgL_xz00_4888;
					int BgL_yz00_4889;

					BgL_xz00_4888 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_4164)))->
							BgL_ez00), BgL_nz00_4887);
					BgL_yz00_4889 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(((
								(BgL_ev_unwindzd2protectzd2_bglt)
								COBJECT(((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_4164)))->
							BgL_bodyz00), BgL_nz00_4887);
					return BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4888),
						BINT(BgL_yz00_4889));
				}
			}
		}

	}



/* &fsize-ev_bind-exit1300 */
	obj_t BGl_z62fsiza7ezd2ev_bindzd2exit1300zc5zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4166, obj_t BgL_ez00_4167, obj_t BgL_nz00_4168)
	{
		{	/* Eval/evaluate_fsize.scm 99 */
			{	/* Eval/evaluate_fsize.scm 100 */
				int BgL_tmpz00_6710;

				{	/* Eval/evaluate_fsize.scm 100 */
					int BgL_nz00_4891;

					BgL_nz00_4891 = CINT(BgL_nz00_4168);
					{	/* Eval/evaluate_fsize.scm 101 */
						BgL_ev_exprz00_bglt BgL_arg1607z00_4892;
						long BgL_arg1608z00_4893;

						BgL_arg1607z00_4892 =
							(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
									((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_4167)))->BgL_bodyz00);
						BgL_arg1608z00_4893 = ((long) (BgL_nz00_4891) + 1L);
						BgL_tmpz00_6710 =
							BGl_fsiza7eza7zz__evaluate_fsiza7eza7(BgL_arg1607z00_4892,
							(int) (BgL_arg1608z00_4893));
				}}
				return BINT(BgL_tmpz00_6710);
			}
		}

	}



/* &fsize-ev_hook1298 */
	obj_t BGl_z62fsiza7ezd2ev_hook1298z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4169, obj_t BgL_ez00_4170, obj_t BgL_nz00_4171)
	{
		{	/* Eval/evaluate_fsize.scm 95 */
			{	/* Eval/evaluate_fsize.scm 96 */
				int BgL_tmpz00_6719;

				{	/* Eval/evaluate_fsize.scm 96 */
					int BgL_nz00_4895;

					BgL_nz00_4895 = CINT(BgL_nz00_4171);
					BgL_tmpz00_6719 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt) BgL_ez00_4170)))->BgL_ez00),
						BgL_nz00_4895);
				}
				return BINT(BgL_tmpz00_6719);
			}
		}

	}



/* &fsize-ev_prog21296 */
	obj_t BGl_z62fsiza7ezd2ev_prog21296z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4172, obj_t BgL_ez00_4173, obj_t BgL_nz00_4174)
	{
		{	/* Eval/evaluate_fsize.scm 91 */
			{	/* Eval/evaluate_fsize.scm 92 */
				int BgL_nz00_4897;

				BgL_nz00_4897 = CINT(BgL_nz00_4174);
				{	/* Eval/evaluate_fsize.scm 93 */
					int BgL_xz00_4898;
					int BgL_yz00_4899;

					BgL_xz00_4898 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_prog2z00_bglt) COBJECT(
									((BgL_ev_prog2z00_bglt) BgL_ez00_4173)))->BgL_e1z00),
						BgL_nz00_4897);
					BgL_yz00_4899 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7((((BgL_ev_prog2z00_bglt)
								COBJECT(((BgL_ev_prog2z00_bglt) BgL_ez00_4173)))->BgL_e2z00),
						BgL_nz00_4897);
					return BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4898),
						BINT(BgL_yz00_4899));
				}
			}
		}

	}



/* &fsize-ev_list1294 */
	obj_t BGl_z62fsiza7ezd2ev_list1294z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4175, obj_t BgL_ez00_4176, obj_t BgL_nz00_4177)
	{
		{	/* Eval/evaluate_fsize.scm 84 */
			{	/* Eval/evaluate_fsize.scm 85 */
				int BgL_nz00_4901;

				BgL_nz00_4901 = CINT(BgL_nz00_4177);
				{	/* Eval/evaluate_fsize.scm 86 */
					obj_t BgL_arg1595z00_4902;

					BgL_arg1595z00_4902 =
						(((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_ez00_4176)))->BgL_argsz00);
					{
						obj_t BgL_lz00_4904;
						obj_t BgL_rz00_4905;

						BgL_lz00_4904 = BgL_arg1595z00_4902;
						BgL_rz00_4905 = BINT(BgL_nz00_4901);
					BgL_recz00_4903:
						if (NULLP(BgL_lz00_4904))
							{	/* Eval/evaluate_fsize.scm 87 */
								return BgL_rz00_4905;
							}
						else
							{	/* Eval/evaluate_fsize.scm 89 */
								obj_t BgL_arg1598z00_4906;
								obj_t BgL_arg1601z00_4907;

								BgL_arg1598z00_4906 = CDR(((obj_t) BgL_lz00_4904));
								{	/* Eval/evaluate_fsize.scm 89 */
									int BgL_yz00_4908;

									{	/* Eval/evaluate_fsize.scm 89 */
										obj_t BgL_arg1602z00_4909;

										BgL_arg1602z00_4909 = CAR(((obj_t) BgL_lz00_4904));
										BgL_yz00_4908 =
											BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
											((BgL_ev_exprz00_bglt) BgL_arg1602z00_4909),
											BgL_nz00_4901);
									}
									BgL_arg1601z00_4907 =
										BGl_2maxz00zz__r4_numbers_6_5z00(BgL_rz00_4905,
										BINT(BgL_yz00_4908));
								}
								{
									obj_t BgL_rz00_6749;
									obj_t BgL_lz00_6748;

									BgL_lz00_6748 = BgL_arg1598z00_4906;
									BgL_rz00_6749 = BgL_arg1601z00_4907;
									BgL_rz00_4905 = BgL_rz00_6749;
									BgL_lz00_4904 = BgL_lz00_6748;
									goto BgL_recz00_4903;
								}
							}
					}
				}
			}
		}

	}



/* &fsize-ev_if1292 */
	obj_t BGl_z62fsiza7ezd2ev_if1292z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4178, obj_t BgL_ez00_4179, obj_t BgL_nz00_4180)
	{
		{	/* Eval/evaluate_fsize.scm 80 */
			{	/* Eval/evaluate_fsize.scm 81 */
				int BgL_nz00_4911;

				BgL_nz00_4911 = CINT(BgL_nz00_4180);
				{	/* Eval/evaluate_fsize.scm 82 */
					int BgL_arg1587z00_4912;
					obj_t BgL_arg1589z00_4913;

					BgL_arg1587z00_4912 =
						BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
						(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_ez00_4179)))->BgL_pz00),
						BgL_nz00_4911);
					{	/* Eval/evaluate_fsize.scm 82 */
						int BgL_xz00_4914;
						int BgL_yz00_4915;

						BgL_xz00_4914 =
							BGl_fsiza7eza7zz__evaluate_fsiza7eza7(
							(((BgL_ev_ifz00_bglt) COBJECT(
										((BgL_ev_ifz00_bglt) BgL_ez00_4179)))->BgL_tz00),
							BgL_nz00_4911);
						BgL_yz00_4915 =
							BGl_fsiza7eza7zz__evaluate_fsiza7eza7((((BgL_ev_ifz00_bglt)
									COBJECT(((BgL_ev_ifz00_bglt) BgL_ez00_4179)))->BgL_ez00),
							BgL_nz00_4911);
						BgL_arg1589z00_4913 =
							BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_xz00_4914),
							BINT(BgL_yz00_4915));
					}
					return
						BGl_2maxz00zz__r4_numbers_6_5z00(BINT(BgL_arg1587z00_4912),
						BgL_arg1589z00_4913);
				}
			}
		}

	}



/* &fsize-ev_litt1290 */
	obj_t BGl_z62fsiza7ezd2ev_litt1290z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4181, obj_t BgL_ez00_4182, obj_t BgL_nz00_4183)
	{
		{	/* Eval/evaluate_fsize.scm 77 */
			return BINT(CINT(BgL_nz00_4183));
		}

	}



/* &fsize-ev_global1288 */
	obj_t BGl_z62fsiza7ezd2ev_global1288z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4184, obj_t BgL_varz00_4185, obj_t BgL_nz00_4186)
	{
		{	/* Eval/evaluate_fsize.scm 74 */
			return BINT(CINT(BgL_nz00_4186));
		}

	}



/* &fsize-ev_var1286 */
	obj_t BGl_z62fsiza7ezd2ev_var1286z17zz__evaluate_fsiza7eza7(obj_t
		BgL_envz00_4187, obj_t BgL_ez00_4188, obj_t BgL_nz00_4189)
	{
		{	/* Eval/evaluate_fsize.scm 71 */
			return BINT(CINT(BgL_nz00_4189));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_fsiza7eza7(void)
	{
		{	/* Eval/evaluate_fsize.scm 16 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__ppz00(233942021L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			BGl_modulezd2initializa7ationz75zz__evaluate_uncompz00(193509656L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
			return BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(0L,
				BSTRING_TO_STRING(BGl_string2375z00zz__evaluate_fsiza7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
