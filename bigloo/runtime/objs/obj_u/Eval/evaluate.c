/*===========================================================================*/
/*   (Eval/evaluate.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evaluate.scm -indent -o objs/obj_u/Eval/evaluate.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVALUATE_TYPE_DEFINITIONS
#define BGL___EVALUATE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ev_exprz00_bgl
	{
		header_t header;
		obj_t widening;
	}                 *BgL_ev_exprz00_bglt;

	typedef struct BgL_ev_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_effz00;
		obj_t BgL_typez00;
	}                *BgL_ev_varz00_bglt;

	typedef struct BgL_ev_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                   *BgL_ev_globalz00_bglt;

	typedef struct BgL_ev_littz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_valuez00;
	}                 *BgL_ev_littz00_bglt;

	typedef struct BgL_ev_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_pz00;
		struct BgL_ev_exprz00_bgl *BgL_tz00;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}               *BgL_ev_ifz00_bglt;

	typedef struct BgL_ev_listz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                 *BgL_ev_listz00_bglt;

	typedef struct BgL_ev_orz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}               *BgL_ev_orz00_bglt;

	typedef struct BgL_ev_andz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                *BgL_ev_andz00_bglt;

	typedef struct BgL_ev_prog2z00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_e1z00;
		struct BgL_ev_exprz00_bgl *BgL_e2z00;
	}                  *BgL_ev_prog2z00_bglt;

	typedef struct BgL_ev_hookz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_hookz00_bglt;

	typedef struct BgL_ev_trapz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_trapz00_bglt;

	typedef struct BgL_ev_setlocalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_varz00_bgl *BgL_vz00;
	}                     *BgL_ev_setlocalz00_bglt;

	typedef struct BgL_ev_setglobalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                      *BgL_ev_setglobalz00_bglt;

	typedef struct BgL_ev_defglobalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                      *BgL_ev_defglobalz00_bglt;

	typedef struct BgL_ev_bindzd2exitzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_varz00_bgl *BgL_varz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                        *BgL_ev_bindzd2exitzd2_bglt;

	typedef struct BgL_ev_unwindzd2protectzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                             *BgL_ev_unwindzd2protectzd2_bglt;

	typedef struct BgL_ev_withzd2handlerzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_handlerz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                           *BgL_ev_withzd2handlerzd2_bglt;

	typedef struct BgL_ev_synchroniza7eza7_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_mutexz00;
		struct BgL_ev_exprz00_bgl *BgL_prelockz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                          *BgL_ev_synchroniza7eza7_bglt;

	typedef struct BgL_ev_binderz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_binderz00_bglt;

	typedef struct BgL_ev_letz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_letz00_bglt;

	typedef struct BgL_ev_letza2za2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_letza2za2_bglt;

	typedef struct BgL_ev_letrecz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_letrecz00_bglt;

	typedef struct BgL_ev_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_tailzf3zf3;
	}                *BgL_ev_appz00_bglt;

	typedef struct BgL_ev_absz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_wherez00;
		obj_t BgL_arityz00;
		obj_t BgL_varsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		int BgL_siza7eza7;
		obj_t BgL_bindz00;
		obj_t BgL_freez00;
		obj_t BgL_innerz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_absz00_bglt;


#endif													// BGL___EVALUATE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
	extern obj_t BGl_findzd2statezd2zz__evaluate_compz00(void);
	extern obj_t BGl_ev_defglobalz00zz__evaluate_typesz00;
	extern obj_t BGl_evmodulez00zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_untypezd2identzd2zz__evaluatez00(obj_t);
	static obj_t BGl_convzd2beginzd2zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, bool_t);
	extern obj_t BGl_compilez00zz__evaluate_compz00(BgL_ev_exprz00_bglt);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_classzd2fieldzd2typez00zz__objectz00(obj_t);
	static obj_t BGl_symbol2904z00zz__evaluatez00 = BUNSPEC;
	extern bool_t BGl_classzd2fieldzf3z21zz__objectz00(obj_t);
	static BgL_ev_absz00_bglt BGl_convzd2lambdaze70z35zz__evaluatez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern BgL_ev_exprz00_bglt
		BGl_extractzd2loopszd2zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol3001z00zz__evaluatez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evaluatez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_compz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_uncompz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_fsiza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_avarz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	static obj_t BGl_symbol3003z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol3005z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol3007z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_expandz00zz__expandz00(obj_t);
	static obj_t BGl_symbol3009z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_symbol2912z00zz__evaluatez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(obj_t);
	static obj_t BGl_symbol2914z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_getzd2locationzd2zz__evaluatez00(obj_t, obj_t);
	static obj_t BGl_symbol2916z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2917z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_z62setzd2evaluationzd2contextz12z70zz__evaluatez00(obj_t,
		obj_t);
	static obj_t BGl_symbol2919z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_ev_globalz00zz__evaluate_typesz00;
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	extern obj_t BGl_ev_letrecz00zz__evaluate_typesz00;
	static obj_t BGl_symbol3011z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol3013z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol3017z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol3019z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2921z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2923z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2925z00zz__evaluatez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00(obj_t);
	static obj_t BGl_symbol2927z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2929z00zz__evaluatez00 = BUNSPEC;
	extern int bgl_debug(void);
	static obj_t BGl_z62zc3z04anonymousza31373ze3ze5zz__evaluatez00(obj_t);
	extern obj_t BGl_ev_littz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_orz00zz__evaluate_typesz00;
	extern obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_ev_letz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_varz00zz__evaluate_typesz00;
	static obj_t BGl_symbol2930z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2932z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2934z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2936z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2938z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_typezd2checkzd2zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62evaluate2z62zz__evaluatez00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evaluatez00(void);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__evaluatez00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_classzd2existszd2zz__objectz00(obj_t);
	extern obj_t BGl_ev_prog2z00zz__evaluate_typesz00;
	static obj_t BGl_symbol2940z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2942z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2944z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2946z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__evaluatez00(void);
	static obj_t BGl_symbol2948z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__evaluatez00(void);
	extern int BGl_framezd2siza7ez75zz__evaluate_fsiza7eza7(BgL_ev_exprz00_bglt);
	static obj_t BGl_objectzd2initzd2zz__evaluatez00(void);
	extern bool_t BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(obj_t);
	extern obj_t BGl_ev_bindzd2exitzd2zz__evaluate_typesz00;
	extern obj_t BGl_ev_withzd2handlerzd2zz__evaluate_typesz00;
	static obj_t BGl_symbol2950z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2952z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2954z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2956z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_ev_andz00zz__evaluate_typesz00;
	static obj_t BGl_symbol2958z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_convzd2fieldzd2refz00zz__evaluatez00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, bool_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62getzd2evaluationzd2contextz62zz__evaluatez00(obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_uconvzf2locze70z15zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2960z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_convzd2valsze70z35zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2962z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2964z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2966z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2968z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_findzd2classzd2fieldz00zz__objectz00(obj_t, obj_t);
	extern obj_t BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00;
	extern bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t);
	extern obj_t BGl_getzd2sourcezd2locationz00zz__readerz00(obj_t);
	static obj_t BGl_symbol2970z00zz__evaluatez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_evaluate2z00zz__evaluatez00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2972z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2974z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2976z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2978z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_ev_appz00zz__evaluate_typesz00;
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t
		BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t,
		obj_t, bool_t);
	static obj_t BGl_convzd2fieldzd2setz00zz__evaluatez00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, bool_t);
	extern obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t);
	extern obj_t make_vector(long, obj_t);
	extern obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_ev_letza2za2zz__evaluate_typesz00;
	extern obj_t BGl_evalzd2findzd2modulez00zz__evmodulez00(obj_t);
	static obj_t BGl_symbol2982z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_convzd2varzd2zz__evaluatez00(obj_t, obj_t);
	static obj_t BGl_symbol2984z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_typezd2resultzd2zz__evaluatez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evaluate2zd2restorezd2statez12z70zz__evaluatez00(obj_t,
		obj_t);
	static obj_t BGl_symbol2987z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2989z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__evaluatez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(int);
	extern obj_t BGl_evcompilezd2errorzd2zz__evcompilez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_typezd2checkszd2zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__evaluatez00(void);
	static obj_t BGl_symbol2991z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2993z00zz__evaluatez00 = BUNSPEC;
	static obj_t BGl_symbol2995z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_ev_absz00zz__evaluate_typesz00;
	static obj_t BGl_symbol2997z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_ev_trapz00zz__evaluate_typesz00;
	static obj_t BGl_symbol2999z00zz__evaluatez00 = BUNSPEC;
	extern obj_t BGl_ev_synchroniza7eza7zz__evaluate_typesz00;
	static obj_t BGl_loopze70ze7zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_loopze71ze7zz__evaluatez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_setglobalz00zz__evaluate_typesz00;
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2evaluationzd2contextz00zz__evaluatez00(void);
	extern obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t);
	extern obj_t BGl_ev_ifz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_setlocalz00zz__evaluate_typesz00;
	static obj_t BGl_convz00zz__evaluatez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t);
	extern obj_t BGl_analysezd2varszd2zz__evaluate_avarz00(BgL_ev_exprz00_bglt);
	static obj_t BGl_z62evaluate2zd2restorezd2bpz12z70zz__evaluatez00(obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_keyword3015z00zz__evaluatez00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string3010z00zz__evaluatez00,
		BgL_bgl_string3010za700za7za7_3024za7, "unwind-protect", 14);
	      DEFINE_STRING(BGl_string3012z00zz__evaluatez00,
		BgL_bgl_string3012za700za7za7_3025za7, "with-handler", 12);
	      DEFINE_STRING(BGl_string3014z00zz__evaluatez00,
		BgL_bgl_string3014za700za7za7_3026za7, "synchronize", 11);
	      DEFINE_STRING(BGl_string3016z00zz__evaluatez00,
		BgL_bgl_string3016za700za7za7_3027za7, "prelock", 7);
	      DEFINE_STRING(BGl_string3018z00zz__evaluatez00,
		BgL_bgl_string3018za700za7za7_3028za7, ".", 1);
	      DEFINE_STRING(BGl_string2920z00zz__evaluatez00,
		BgL_bgl_string2920za700za7za7_3029za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2922z00zz__evaluatez00,
		BgL_bgl_string2922za700za7za7_3030za7, "symbol?", 7);
	      DEFINE_STRING(BGl_string2924z00zz__evaluatez00,
		BgL_bgl_string2924za700za7za7_3031za7, "char", 4);
	      DEFINE_STRING(BGl_string2926z00zz__evaluatez00,
		BgL_bgl_string2926za700za7za7_3032za7, "char?", 5);
	      DEFINE_STRING(BGl_string2928z00zz__evaluatez00,
		BgL_bgl_string2928za700za7za7_3033za7, "int", 3);
	      DEFINE_STRING(BGl_string3020z00zz__evaluatez00,
		BgL_bgl_string3020za700za7za7_3034za7, "free-pragma::obj", 16);
	      DEFINE_STRING(BGl_string3021z00zz__evaluatez00,
		BgL_bgl_string3021za700za7za7_3035za7, "free-pragma", 11);
	      DEFINE_STRING(BGl_string3022z00zz__evaluatez00,
		BgL_bgl_string3022za700za7za7_3036za7, "not supported in eval", 21);
	      DEFINE_STRING(BGl_string3023z00zz__evaluatez00,
		BgL_bgl_string3023za700za7za7_3037za7, "__evaluate", 10);
	      DEFINE_STRING(BGl_string2931z00zz__evaluatez00,
		BgL_bgl_string2931za700za7za7_3038za7, "integer?", 8);
	      DEFINE_STRING(BGl_string2933z00zz__evaluatez00,
		BgL_bgl_string2933za700za7za7_3039za7, "real", 4);
	      DEFINE_STRING(BGl_string2935z00zz__evaluatez00,
		BgL_bgl_string2935za700za7za7_3040za7, "breal", 5);
	      DEFINE_STRING(BGl_string2937z00zz__evaluatez00,
		BgL_bgl_string2937za700za7za7_3041za7, "real?", 5);
	      DEFINE_STRING(BGl_string2939z00zz__evaluatez00,
		BgL_bgl_string2939za700za7za7_3042za7, "bool", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evaluate2zd2restorezd2bpz12zd2envzc0zz__evaluatez00,
		BgL_bgl_za762evaluate2za7d2r3043z00,
		BGl_z62evaluate2zd2restorezd2bpz12z70zz__evaluatez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2evaluationzd2contextz12zd2envzc0zz__evaluatez00,
		BgL_bgl_za762setza7d2evaluat3044z00,
		BGl_z62setzd2evaluationzd2contextz12z70zz__evaluatez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2941z00zz__evaluatez00,
		BgL_bgl_string2941za700za7za7_3045za7, "boolean?", 8);
	      DEFINE_STRING(BGl_string2943z00zz__evaluatez00,
		BgL_bgl_string2943za700za7za7_3046za7, "struct", 6);
	      DEFINE_STRING(BGl_string2945z00zz__evaluatez00,
		BgL_bgl_string2945za700za7za7_3047za7, "struct?", 7);
	      DEFINE_STRING(BGl_string2947z00zz__evaluatez00,
		BgL_bgl_string2947za700za7za7_3048za7, "class", 5);
	      DEFINE_STRING(BGl_string2949z00zz__evaluatez00,
		BgL_bgl_string2949za700za7za7_3049za7, "class?", 6);
	      DEFINE_STRING(BGl_string2951z00zz__evaluatez00,
		BgL_bgl_string2951za700za7za7_3050za7, "string", 6);
	      DEFINE_STRING(BGl_string2953z00zz__evaluatez00,
		BgL_bgl_string2953za700za7za7_3051za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2955z00zz__evaluatez00,
		BgL_bgl_string2955za700za7za7_3052za7, "string?", 7);
	      DEFINE_STRING(BGl_string2957z00zz__evaluatez00,
		BgL_bgl_string2957za700za7za7_3053za7, "o", 1);
	      DEFINE_STRING(BGl_string2959z00zz__evaluatez00,
		BgL_bgl_string2959za700za7za7_3054za7, "quote", 5);
	      DEFINE_STRING(BGl_string2961z00zz__evaluatez00,
		BgL_bgl_string2961za700za7za7_3055za7, "class-exists", 12);
	      DEFINE_STRING(BGl_string2963z00zz__evaluatez00,
		BgL_bgl_string2963za700za7za7_3056za7, "c", 1);
	      DEFINE_STRING(BGl_string2965z00zz__evaluatez00,
		BgL_bgl_string2965za700za7za7_3057za7, "isa?", 4);
	      DEFINE_STRING(BGl_string2967z00zz__evaluatez00,
		BgL_bgl_string2967za700za7za7_3058za7, "if", 2);
	      DEFINE_STRING(BGl_string2969z00zz__evaluatez00,
		BgL_bgl_string2969za700za7za7_3059za7, "let", 3);
	      DEFINE_STRING(BGl_string2971z00zz__evaluatez00,
		BgL_bgl_string2971za700za7za7_3060za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2973z00zz__evaluatez00,
		BgL_bgl_string2973za700za7za7_3061za7, "bigloo-type-error/location", 26);
	      DEFINE_STRING(BGl_string2975z00zz__evaluatez00,
		BgL_bgl_string2975za700za7za7_3062za7, "bigloo-type-error", 17);
	      DEFINE_STRING(BGl_string2977z00zz__evaluatez00,
		BgL_bgl_string2977za700za7za7_3063za7, "at", 2);
	      DEFINE_STRING(BGl_string2979z00zz__evaluatez00,
		BgL_bgl_string2979za700za7za7_3064za7, "tmp", 3);
	      DEFINE_STRING(BGl_string2898z00zz__evaluatez00,
		BgL_bgl_string2898za700za7za7_3065za7, "", 0);
	      DEFINE_STRING(BGl_string2899z00zz__evaluatez00,
		BgL_bgl_string2899za700za7za7_3066za7,
		"/tmp/bigloo/runtime/Eval/evaluate.scm", 37);
	extern obj_t BGl_errorzd2envzd2zz__errorz00;
	   
		 
		DEFINE_STRING(BGl_string2980z00zz__evaluatez00,
		BgL_bgl_string2980za700za7za7_3067za7, "~a::~a", 6);
	      DEFINE_STRING(BGl_string2981z00zz__evaluatez00,
		BgL_bgl_string2981za700za7za7_3068za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2983z00zz__evaluatez00,
		BgL_bgl_string2983za700za7za7_3069za7, "begin", 5);
	      DEFINE_STRING(BGl_string2985z00zz__evaluatez00,
		BgL_bgl_string2985za700za7za7_3070za7, " ", 1);
	      DEFINE_STRING(BGl_string2986z00zz__evaluatez00,
		BgL_bgl_string2986za700za7za7_3071za7,
		"Illegal non toplevel module declaration", 39);
	      DEFINE_STRING(BGl_string2988z00zz__evaluatez00,
		BgL_bgl_string2988za700za7za7_3072za7, "module", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evaluate2zd2restorezd2statez12zd2envzc0zz__evaluatez00,
		BgL_bgl_za762evaluate2za7d2r3073z00,
		BGl_z62evaluate2zd2restorezd2statez12z70zz__evaluatez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2990z00zz__evaluatez00,
		BgL_bgl_string2990za700za7za7_3074za7, "@", 1);
	      DEFINE_STRING(BGl_string2992z00zz__evaluatez00,
		BgL_bgl_string2992za700za7za7_3075za7, "->", 2);
	      DEFINE_STRING(BGl_string2994z00zz__evaluatez00,
		BgL_bgl_string2994za700za7za7_3076za7, "trap", 4);
	      DEFINE_STRING(BGl_string2996z00zz__evaluatez00,
		BgL_bgl_string2996za700za7za7_3077za7, "or", 2);
	      DEFINE_STRING(BGl_string2998z00zz__evaluatez00,
		BgL_bgl_string2998za700za7za7_3078za7, "and", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2evaluationzd2contextzd2envzd2zz__evaluatez00,
		BgL_bgl_za762getza7d2evaluat3079z00,
		BGl_z62getzd2evaluationzd2contextz62zz__evaluatez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evaluate2zd2envzd2zz__evaluatez00,
		BgL_bgl_za762evaluate2za762za73080za7, BGl_z62evaluate2z62zz__evaluatez00,
		0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2900z00zz__evaluatez00,
		BgL_bgl_string2900za700za7za7_3081za7, "&evaluate2-restore-bp!", 22);
	      DEFINE_STRING(BGl_string2901z00zz__evaluatez00,
		BgL_bgl_string2901za700za7za7_3082za7, "bint", 4);
	      DEFINE_STRING(BGl_string2902z00zz__evaluatez00,
		BgL_bgl_string2902za700za7za7_3083za7, "&evaluate2-restore-state!", 25);
	      DEFINE_STRING(BGl_string2903z00zz__evaluatez00,
		BgL_bgl_string2903za700za7za7_3084za7, "vector", 6);
	      DEFINE_STRING(BGl_string2905z00zz__evaluatez00,
		BgL_bgl_string2905za700za7za7_3085za7, "toplevel", 8);
	      DEFINE_STRING(BGl_string2906z00zz__evaluatez00,
		BgL_bgl_string2906za700za7za7_3086za7, "eval", 4);
	      DEFINE_STRING(BGl_string2907z00zz__evaluatez00,
		BgL_bgl_string2907za700za7za7_3087za7, "Bad syntax", 10);
	      DEFINE_STRING(BGl_string2908z00zz__evaluatez00,
		BgL_bgl_string2908za700za7za7_3088za7, "Class \"~a\" has no field \"~a\"",
		28);
	      DEFINE_STRING(BGl_string2909z00zz__evaluatez00,
		BgL_bgl_string2909za700za7za7_3089za7, "Static type not a class", 23);
	      DEFINE_STRING(BGl_string3000z00zz__evaluatez00,
		BgL_bgl_string3000za700za7za7_3090za7, "let*", 4);
	      DEFINE_STRING(BGl_string3002z00zz__evaluatez00,
		BgL_bgl_string3002za700za7za7_3091za7, "letrec", 6);
	      DEFINE_STRING(BGl_string3004z00zz__evaluatez00,
		BgL_bgl_string3004za700za7za7_3092za7, "set!", 4);
	      DEFINE_STRING(BGl_string3006z00zz__evaluatez00,
		BgL_bgl_string3006za700za7za7_3093za7, "define", 6);
	      DEFINE_STRING(BGl_string3008z00zz__evaluatez00,
		BgL_bgl_string3008za700za7za7_3094za7, "bind-exit", 9);
	      DEFINE_STRING(BGl_string2910z00zz__evaluatez00,
		BgL_bgl_string2910za700za7za7_3095za7, "Variable unbound", 16);
	      DEFINE_STRING(BGl_string2911z00zz__evaluatez00,
		BgL_bgl_string2911za700za7za7_3096za7, "Field read-only", 15);
	      DEFINE_STRING(BGl_string2913z00zz__evaluatez00,
		BgL_bgl_string2913za700za7za7_3097za7, "pair", 4);
	      DEFINE_STRING(BGl_string2915z00zz__evaluatez00,
		BgL_bgl_string2915za700za7za7_3098za7, "pair?", 5);
	      DEFINE_STRING(BGl_string2918z00zz__evaluatez00,
		BgL_bgl_string2918za700za7za7_3099za7, "vector?", 7);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2904z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3001z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3003z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3005z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3007z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3009z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2912z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2914z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2916z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2917z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2919z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3011z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3013z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3017z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol3019z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2921z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2923z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2925z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2927z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2929z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2930z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2932z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2934z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2936z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2938z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2940z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2942z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2944z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2946z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2948z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2950z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2952z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2954z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2956z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2958z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2960z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2962z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2964z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2966z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2968z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2970z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2972z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2974z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2976z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2978z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2982z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2984z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2987z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2989z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2991z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2993z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2995z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2997z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_symbol2999z00zz__evaluatez00));
		     ADD_ROOT((void *) (&BGl_keyword3015z00zz__evaluatez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evaluatez00(long
		BgL_checksumz00_4955, char *BgL_fromz00_4956)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evaluatez00))
				{
					BGl_requirezd2initializa7ationz75zz__evaluatez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evaluatez00();
					BGl_cnstzd2initzd2zz__evaluatez00();
					BGl_importedzd2moduleszd2initz00zz__evaluatez00();
					return BGl_methodzd2initzd2zz__evaluatez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 15 */
			BGl_symbol2904z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2905z00zz__evaluatez00);
			BGl_symbol2912z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2913z00zz__evaluatez00);
			BGl_symbol2914z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2915z00zz__evaluatez00);
			BGl_symbol2916z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2903z00zz__evaluatez00);
			BGl_symbol2917z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2918z00zz__evaluatez00);
			BGl_symbol2919z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2920z00zz__evaluatez00);
			BGl_symbol2921z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2922z00zz__evaluatez00);
			BGl_symbol2923z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2924z00zz__evaluatez00);
			BGl_symbol2925z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2926z00zz__evaluatez00);
			BGl_symbol2927z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2928z00zz__evaluatez00);
			BGl_symbol2929z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2901z00zz__evaluatez00);
			BGl_symbol2930z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2931z00zz__evaluatez00);
			BGl_symbol2932z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2933z00zz__evaluatez00);
			BGl_symbol2934z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2935z00zz__evaluatez00);
			BGl_symbol2936z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2937z00zz__evaluatez00);
			BGl_symbol2938z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2939z00zz__evaluatez00);
			BGl_symbol2940z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2941z00zz__evaluatez00);
			BGl_symbol2942z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2943z00zz__evaluatez00);
			BGl_symbol2944z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2945z00zz__evaluatez00);
			BGl_symbol2946z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2947z00zz__evaluatez00);
			BGl_symbol2948z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2949z00zz__evaluatez00);
			BGl_symbol2950z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2951z00zz__evaluatez00);
			BGl_symbol2952z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2953z00zz__evaluatez00);
			BGl_symbol2954z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2955z00zz__evaluatez00);
			BGl_symbol2956z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2957z00zz__evaluatez00);
			BGl_symbol2958z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2959z00zz__evaluatez00);
			BGl_symbol2960z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2961z00zz__evaluatez00);
			BGl_symbol2962z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2963z00zz__evaluatez00);
			BGl_symbol2964z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2965z00zz__evaluatez00);
			BGl_symbol2966z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2967z00zz__evaluatez00);
			BGl_symbol2968z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2969z00zz__evaluatez00);
			BGl_symbol2970z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2971z00zz__evaluatez00);
			BGl_symbol2972z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2973z00zz__evaluatez00);
			BGl_symbol2974z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2975z00zz__evaluatez00);
			BGl_symbol2976z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2977z00zz__evaluatez00);
			BGl_symbol2978z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2979z00zz__evaluatez00);
			BGl_symbol2982z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2983z00zz__evaluatez00);
			BGl_symbol2984z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2985z00zz__evaluatez00);
			BGl_symbol2987z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2988z00zz__evaluatez00);
			BGl_symbol2989z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2990z00zz__evaluatez00);
			BGl_symbol2991z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2992z00zz__evaluatez00);
			BGl_symbol2993z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2994z00zz__evaluatez00);
			BGl_symbol2995z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2996z00zz__evaluatez00);
			BGl_symbol2997z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string2998z00zz__evaluatez00);
			BGl_symbol2999z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3000z00zz__evaluatez00);
			BGl_symbol3001z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3002z00zz__evaluatez00);
			BGl_symbol3003z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3004z00zz__evaluatez00);
			BGl_symbol3005z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3006z00zz__evaluatez00);
			BGl_symbol3007z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3008z00zz__evaluatez00);
			BGl_symbol3009z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3010z00zz__evaluatez00);
			BGl_symbol3011z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3012z00zz__evaluatez00);
			BGl_symbol3013z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3014z00zz__evaluatez00);
			BGl_keyword3015z00zz__evaluatez00 =
				bstring_to_keyword(BGl_string3016z00zz__evaluatez00);
			BGl_symbol3017z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3018z00zz__evaluatez00);
			return (BGl_symbol3019z00zz__evaluatez00 =
				bstring_to_symbol(BGl_string3020z00zz__evaluatez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__evaluatez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1194;

				BgL_headz00_1194 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_3221;
					obj_t BgL_tailz00_3222;

					BgL_prevz00_3221 = BgL_headz00_1194;
					BgL_tailz00_3222 = BgL_l1z00_1;
				BgL_loopz00_3220:
					if (PAIRP(BgL_tailz00_3222))
						{
							obj_t BgL_newzd2prevzd2_3228;

							BgL_newzd2prevzd2_3228 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_3222), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_3221, BgL_newzd2prevzd2_3228);
							{
								obj_t BgL_tailz00_5027;
								obj_t BgL_prevz00_5026;

								BgL_prevz00_5026 = BgL_newzd2prevzd2_3228;
								BgL_tailz00_5027 = CDR(BgL_tailz00_3222);
								BgL_tailz00_3222 = BgL_tailz00_5027;
								BgL_prevz00_3221 = BgL_prevz00_5026;
								goto BgL_loopz00_3220;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1194);
			}
		}

	}



/* untype-ident */
	obj_t BGl_untypezd2identzd2zz__evaluatez00(obj_t BgL_idz00_3)
	{
		{	/* Eval/evaluate.scm 73 */
			{	/* Eval/evaluate.scm 74 */
				obj_t BgL_stringz00_1202;

				{	/* Eval/evaluate.scm 74 */
					obj_t BgL_arg2648z00_3236;

					BgL_arg2648z00_3236 = SYMBOL_TO_STRING(BgL_idz00_3);
					BgL_stringz00_1202 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2648z00_3236);
				}
				{	/* Eval/evaluate.scm 74 */
					long BgL_lenz00_1203;

					BgL_lenz00_1203 = STRING_LENGTH(BgL_stringz00_1202);
					{	/* Eval/evaluate.scm 75 */

						{
							long BgL_walkerz00_1205;

							BgL_walkerz00_1205 = 0L;
						BgL_zc3z04anonymousza31340ze3z87_1206:
							if ((BgL_walkerz00_1205 == BgL_lenz00_1203))
								{	/* Eval/evaluate.scm 78 */
									return MAKE_YOUNG_PAIR(BgL_idz00_3, BFALSE);
								}
							else
								{	/* Eval/evaluate.scm 80 */
									bool_t BgL_test3103z00_5036;

									if (
										(STRING_REF(BgL_stringz00_1202,
												BgL_walkerz00_1205) == ((unsigned char) ':')))
										{	/* Eval/evaluate.scm 80 */
											if ((BgL_walkerz00_1205 < (BgL_lenz00_1203 - 1L)))
												{	/* Eval/evaluate.scm 81 */
													BgL_test3103z00_5036 =
														(STRING_REF(BgL_stringz00_1202,
															(BgL_walkerz00_1205 + 1L)) ==
														((unsigned char) ':'));
												}
											else
												{	/* Eval/evaluate.scm 81 */
													BgL_test3103z00_5036 = ((bool_t) 0);
												}
										}
									else
										{	/* Eval/evaluate.scm 80 */
											BgL_test3103z00_5036 = ((bool_t) 0);
										}
									if (BgL_test3103z00_5036)
										{	/* Eval/evaluate.scm 83 */
											obj_t BgL_arg1351z00_1217;
											obj_t BgL_arg1352z00_1218;

											BgL_arg1351z00_1217 =
												bstring_to_symbol(c_substring(BgL_stringz00_1202, 0L,
													BgL_walkerz00_1205));
											{	/* Eval/evaluate.scm 84 */
												obj_t BgL_arg1356z00_1220;

												{	/* Eval/evaluate.scm 84 */
													long BgL_arg1357z00_1221;

													BgL_arg1357z00_1221 = (BgL_walkerz00_1205 + 2L);
													{	/* Ieee/string.scm 194 */
														long BgL_endz00_1224;

														BgL_endz00_1224 = STRING_LENGTH(BgL_stringz00_1202);
														{	/* Ieee/string.scm 194 */

															BgL_arg1356z00_1220 =
																BGl_substringz00zz__r4_strings_6_7z00
																(BgL_stringz00_1202, BgL_arg1357z00_1221,
																BgL_endz00_1224);
												}}}
												BgL_arg1352z00_1218 =
													bstring_to_symbol(BgL_arg1356z00_1220);
											}
											return
												MAKE_YOUNG_PAIR(BgL_arg1351z00_1217,
												BgL_arg1352z00_1218);
										}
									else
										{
											long BgL_walkerz00_5053;

											BgL_walkerz00_5053 = (BgL_walkerz00_1205 + 1L);
											BgL_walkerz00_1205 = BgL_walkerz00_5053;
											goto BgL_zc3z04anonymousza31340ze3z87_1206;
										}
								}
						}
					}
				}
			}
		}

	}



/* get-evaluation-context */
	BGL_EXPORTED_DEF obj_t BGl_getzd2evaluationzd2contextz00zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 91 */
			{	/* Eval/evaluate.scm 92 */
				obj_t BgL_sz00_1233;

				BgL_sz00_1233 = BGl_findzd2statezd2zz__evaluate_compz00();
				{	/* Eval/evaluate.scm 93 */
					obj_t BgL_bpz00_1234;

					BgL_bpz00_1234 = VECTOR_REF(((obj_t) BgL_sz00_1233), 0L);
					{	/* Eval/evaluate.scm 94 */
						obj_t BgL_rz00_1235;

						BgL_rz00_1235 =
							make_vector(
							(long) CINT(BgL_bpz00_1234), BGl_string2898z00zz__evaluatez00);
						{
							long BgL_iz00_3268;

							BgL_iz00_3268 = 0L;
						BgL_recz00_3267:
							if ((BgL_iz00_3268 < (long) CINT(BgL_bpz00_1234)))
								{	/* Eval/evaluate.scm 96 */
									{	/* Eval/evaluate.scm 97 */
										obj_t BgL_arg1366z00_3272;

										BgL_arg1366z00_3272 =
											VECTOR_REF(((obj_t) BgL_sz00_1233), BgL_iz00_3268);
										VECTOR_SET(BgL_rz00_1235, BgL_iz00_3268,
											BgL_arg1366z00_3272);
									}
									{
										long BgL_iz00_5066;

										BgL_iz00_5066 = (BgL_iz00_3268 + 1L);
										BgL_iz00_3268 = BgL_iz00_5066;
										goto BgL_recz00_3267;
									}
								}
							else
								{	/* Eval/evaluate.scm 96 */
									((bool_t) 0);
								}
						}
						return BgL_rz00_1235;
					}
				}
			}
		}

	}



/* &get-evaluation-context */
	obj_t BGl_z62getzd2evaluationzd2contextz62zz__evaluatez00(obj_t
		BgL_envz00_4858)
	{
		{	/* Eval/evaluate.scm 91 */
			return BGl_getzd2evaluationzd2contextz00zz__evaluatez00();
		}

	}



/* set-evaluation-context! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(obj_t BgL_vz00_4)
	{
		{	/* Eval/evaluate.scm 104 */
			{	/* Eval/evaluate.scm 105 */
				obj_t BgL_sz00_1243;

				BgL_sz00_1243 = BGl_findzd2statezd2zz__evaluate_compz00();
				{	/* Eval/evaluate.scm 106 */
					obj_t BgL_bpz00_1244;

					BgL_bpz00_1244 = VECTOR_REF(((obj_t) BgL_vz00_4), 0L);
					{
						long BgL_iz00_3288;

						{	/* Eval/evaluate.scm 107 */
							bool_t BgL_tmpz00_5072;

							BgL_iz00_3288 = 0L;
						BgL_recz00_3287:
							if ((BgL_iz00_3288 < (long) CINT(BgL_bpz00_1244)))
								{	/* Eval/evaluate.scm 108 */
									{	/* Eval/evaluate.scm 109 */
										obj_t BgL_arg1370z00_3292;

										BgL_arg1370z00_3292 =
											VECTOR_REF(((obj_t) BgL_vz00_4), BgL_iz00_3288);
										VECTOR_SET(
											((obj_t) BgL_sz00_1243), BgL_iz00_3288,
											BgL_arg1370z00_3292);
									}
									{
										long BgL_iz00_5080;

										BgL_iz00_5080 = (BgL_iz00_3288 + 1L);
										BgL_iz00_3288 = BgL_iz00_5080;
										goto BgL_recz00_3287;
									}
								}
							else
								{	/* Eval/evaluate.scm 108 */
									BgL_tmpz00_5072 = ((bool_t) 0);
								}
							return BBOOL(BgL_tmpz00_5072);
						}
					}
				}
			}
		}

	}



/* &set-evaluation-context! */
	obj_t BGl_z62setzd2evaluationzd2contextz12z70zz__evaluatez00(obj_t
		BgL_envz00_4859, obj_t BgL_vz00_4860)
	{
		{	/* Eval/evaluate.scm 104 */
			return BGl_setzd2evaluationzd2contextz12z12zz__evaluatez00(BgL_vz00_4860);
		}

	}



/* evaluate2-restore-bp! */
	BGL_EXPORTED_DEF obj_t BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(int
		BgL_bpz00_5)
	{
		{	/* Eval/evaluate.scm 115 */
			{	/* Eval/evaluate.scm 116 */
				obj_t BgL_sz00_3299;

				{	/* Eval/evaluate.scm 116 */
					obj_t BgL_tmpz00_5084;

					BgL_tmpz00_5084 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_sz00_3299 = BGL_ENV_EVSTATE(BgL_tmpz00_5084);
				}
				return VECTOR_SET(((obj_t) BgL_sz00_3299), 0L, BINT(BgL_bpz00_5));
			}
		}

	}



/* &evaluate2-restore-bp! */
	obj_t BGl_z62evaluate2zd2restorezd2bpz12z70zz__evaluatez00(obj_t
		BgL_envz00_4861, obj_t BgL_bpz00_4862)
	{
		{	/* Eval/evaluate.scm 115 */
			{	/* Eval/evaluate.scm 116 */
				int BgL_auxz00_5090;

				{	/* Eval/evaluate.scm 116 */
					obj_t BgL_tmpz00_5091;

					if (INTEGERP(BgL_bpz00_4862))
						{	/* Eval/evaluate.scm 116 */
							BgL_tmpz00_5091 = BgL_bpz00_4862;
						}
					else
						{
							obj_t BgL_auxz00_5094;

							BgL_auxz00_5094 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string2899z00zz__evaluatez00, BINT(3890L),
								BGl_string2900z00zz__evaluatez00,
								BGl_string2901z00zz__evaluatez00, BgL_bpz00_4862);
							FAILURE(BgL_auxz00_5094, BFALSE, BFALSE);
						}
					BgL_auxz00_5090 = CINT(BgL_tmpz00_5091);
				}
				return
					BGl_evaluate2zd2restorezd2bpz12z12zz__evaluatez00(BgL_auxz00_5090);
			}
		}

	}



/* evaluate2-restore-state! */
	BGL_EXPORTED_DEF obj_t
		BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00(obj_t BgL_statez00_6)
	{
		{	/* Eval/evaluate.scm 122 */
			{	/* Eval/evaluate.scm 124 */
				obj_t BgL_tmpz00_5100;

				BgL_tmpz00_5100 = BGL_CURRENT_DYNAMIC_ENV();
				return BGL_ENV_EVSTATE_SET(BgL_tmpz00_5100, BgL_statez00_6);
			}
		}

	}



/* &evaluate2-restore-state! */
	obj_t BGl_z62evaluate2zd2restorezd2statez12z70zz__evaluatez00(obj_t
		BgL_envz00_4863, obj_t BgL_statez00_4864)
	{
		{	/* Eval/evaluate.scm 122 */
			{	/* Eval/evaluate.scm 123 */
				obj_t BgL_auxz00_5103;

				if (VECTORP(BgL_statez00_4864))
					{	/* Eval/evaluate.scm 123 */
						BgL_auxz00_5103 = BgL_statez00_4864;
					}
				else
					{
						obj_t BgL_auxz00_5106;

						BgL_auxz00_5106 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2899z00zz__evaluatez00,
							BINT(4240L), BGl_string2902z00zz__evaluatez00,
							BGl_string2903z00zz__evaluatez00, BgL_statez00_4864);
						FAILURE(BgL_auxz00_5106, BFALSE, BFALSE);
					}
				return
					BGl_evaluate2zd2restorezd2statez12z12zz__evaluatez00(BgL_auxz00_5103);
			}
		}

	}



/* evaluate2 */
	BGL_EXPORTED_DEF obj_t BGl_evaluate2z00zz__evaluatez00(obj_t BgL_sexpz00_7,
		obj_t BgL_envz00_8, obj_t BgL_locz00_9)
	{
		{	/* Eval/evaluate.scm 129 */
			{	/* Eval/evaluate.scm 130 */
				BgL_ev_exprz00_bglt BgL_astz00_1255;

				{	/* Eval/evaluate.scm 130 */
					obj_t BgL_arg1375z00_1265;

					BgL_arg1375z00_1265 =
						BGl_convz00zz__evaluatez00(BgL_sexpz00_7, BNIL, BgL_envz00_8,
						BFALSE, BGl_symbol2904z00zz__evaluatez00, BgL_locz00_9,
						((bool_t) 1));
					BgL_astz00_1255 =
						BGl_extractzd2loopszd2zz__evaluate_fsiza7eza7(((BgL_ev_exprz00_bglt)
							BgL_arg1375z00_1265));
				}
				BGl_analysezd2varszd2zz__evaluate_avarz00(BgL_astz00_1255);
				{	/* Eval/evaluate.scm 132 */
					int BgL_nz00_1256;

					BgL_nz00_1256 =
						BGl_framezd2siza7ez75zz__evaluate_fsiza7eza7(BgL_astz00_1255);
					{	/* Eval/evaluate.scm 133 */
						obj_t BgL_fz00_1257;

						BgL_fz00_1257 = BGl_compilez00zz__evaluate_compz00(BgL_astz00_1255);
						{	/* Eval/evaluate.scm 134 */
							obj_t BgL_sz00_1258;

							BgL_sz00_1258 = BGl_findzd2statezd2zz__evaluate_compz00();
							{	/* Eval/evaluate.scm 135 */
								obj_t BgL_bpz00_1259;

								BgL_bpz00_1259 = VECTOR_REF(((obj_t) BgL_sz00_1258), 0L);
								{	/* Eval/evaluate.scm 136 */
									obj_t BgL_exitd1065z00_1260;

									BgL_exitd1065z00_1260 = BGL_EXITD_TOP_AS_OBJ();
									{	/* Eval/evaluate.scm 138 */
										obj_t BgL_zc3z04anonymousza31373ze3z87_4865;

										BgL_zc3z04anonymousza31373ze3z87_4865 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31373ze3ze5zz__evaluatez00,
											(int) (0L), (int) (2L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31373ze3z87_4865,
											(int) (0L), BgL_sz00_1258);
										PROCEDURE_SET(BgL_zc3z04anonymousza31373ze3z87_4865,
											(int) (1L), BgL_bpz00_1259);
										{	/* Eval/evaluate.scm 136 */
											obj_t BgL_arg2875z00_3305;

											{	/* Eval/evaluate.scm 136 */
												obj_t BgL_arg2876z00_3306;

												BgL_arg2876z00_3306 =
													BGL_EXITD_PROTECT(BgL_exitd1065z00_1260);
												BgL_arg2875z00_3305 =
													MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31373ze3z87_4865,
													BgL_arg2876z00_3306);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1065z00_1260,
												BgL_arg2875z00_3305);
											BUNSPEC;
										}
										{	/* Eval/evaluate.scm 137 */
											obj_t BgL_tmp1067z00_1262;

											BgL_tmp1067z00_1262 =
												BGL_PROCEDURE_CALL1(BgL_fz00_1257, BgL_sz00_1258);
											{	/* Eval/evaluate.scm 136 */
												bool_t BgL_test3110z00_5135;

												{	/* Eval/evaluate.scm 136 */
													obj_t BgL_arg2874z00_3308;

													BgL_arg2874z00_3308 =
														BGL_EXITD_PROTECT(BgL_exitd1065z00_1260);
													BgL_test3110z00_5135 = PAIRP(BgL_arg2874z00_3308);
												}
												if (BgL_test3110z00_5135)
													{	/* Eval/evaluate.scm 136 */
														obj_t BgL_arg2872z00_3309;

														{	/* Eval/evaluate.scm 136 */
															obj_t BgL_arg2873z00_3310;

															BgL_arg2873z00_3310 =
																BGL_EXITD_PROTECT(BgL_exitd1065z00_1260);
															BgL_arg2872z00_3309 =
																CDR(((obj_t) BgL_arg2873z00_3310));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1065z00_1260,
															BgL_arg2872z00_3309);
														BUNSPEC;
													}
												else
													{	/* Eval/evaluate.scm 136 */
														BFALSE;
													}
											}
											VECTOR_SET(((obj_t) BgL_sz00_1258), 0L, BgL_bpz00_1259);
											return BgL_tmp1067z00_1262;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &evaluate2 */
	obj_t BGl_z62evaluate2z62zz__evaluatez00(obj_t BgL_envz00_4866,
		obj_t BgL_sexpz00_4867, obj_t BgL_envz00_4868, obj_t BgL_locz00_4869)
	{
		{	/* Eval/evaluate.scm 129 */
			return
				BGl_evaluate2z00zz__evaluatez00(BgL_sexpz00_4867, BgL_envz00_4868,
				BgL_locz00_4869);
		}

	}



/* &<@anonymous:1373> */
	obj_t BGl_z62zc3z04anonymousza31373ze3ze5zz__evaluatez00(obj_t
		BgL_envz00_4870)
	{
		{	/* Eval/evaluate.scm 136 */
			{	/* Eval/evaluate.scm 138 */
				obj_t BgL_sz00_4871;
				obj_t BgL_bpz00_4872;

				BgL_sz00_4871 = PROCEDURE_REF(BgL_envz00_4870, (int) (0L));
				BgL_bpz00_4872 = PROCEDURE_REF(BgL_envz00_4870, (int) (1L));
				return VECTOR_SET(((obj_t) BgL_sz00_4871), 0L, BgL_bpz00_4872);
			}
		}

	}



/* get-location */
	obj_t BGl_getzd2locationzd2zz__evaluatez00(obj_t BgL_expz00_10,
		obj_t BgL_locz00_11)
	{
		{	/* Eval/evaluate.scm 143 */
			{	/* Eval/evaluate.scm 144 */
				obj_t BgL__ortest_1069z00_3313;

				BgL__ortest_1069z00_3313 =
					BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_expz00_10);
				if (CBOOL(BgL__ortest_1069z00_3313))
					{	/* Eval/evaluate.scm 144 */
						return BgL__ortest_1069z00_3313;
					}
				else
					{	/* Eval/evaluate.scm 144 */
						return BgL_locz00_11;
					}
			}
		}

	}



/* conv-var */
	obj_t BGl_convzd2varzd2zz__evaluatez00(obj_t BgL_vz00_18,
		obj_t BgL_localsz00_19)
	{
		{	/* Eval/evaluate.scm 163 */
			{
				obj_t BgL_lz00_1270;

				BgL_lz00_1270 = BgL_localsz00_19;
			BgL_zc3z04anonymousza31376ze3z87_1271:
				if (NULLP(BgL_lz00_1270))
					{	/* Eval/evaluate.scm 165 */
						return BFALSE;
					}
				else
					{	/* Eval/evaluate.scm 167 */
						obj_t BgL_rvz00_1273;

						BgL_rvz00_1273 = CAR(((obj_t) BgL_lz00_1270));
						if (
							(BgL_vz00_18 ==
								(((BgL_ev_varz00_bglt) COBJECT(
											((BgL_ev_varz00_bglt) BgL_rvz00_1273)))->BgL_namez00)))
							{	/* Eval/evaluate.scm 169 */
								return BgL_rvz00_1273;
							}
						else
							{
								obj_t BgL_lz00_5162;

								BgL_lz00_5162 = CDR(((obj_t) BgL_lz00_1270));
								BgL_lz00_1270 = BgL_lz00_5162;
								goto BgL_zc3z04anonymousza31376ze3z87_1271;
							}
					}
			}
		}

	}



/* conv-begin */
	obj_t BGl_convzd2beginzd2zz__evaluatez00(obj_t BgL_lz00_20,
		obj_t BgL_localsz00_21, obj_t BgL_globalsz00_22, obj_t BgL_tailzf3zf3_23,
		obj_t BgL_wherez00_24, obj_t BgL_locz00_25, bool_t BgL_topzf3zf3_26)
	{
		{	/* Eval/evaluate.scm 176 */
			{	/* Eval/evaluate.scm 177 */
				obj_t BgL_locz00_1280;

				{	/* Eval/evaluate.scm 144 */
					obj_t BgL__ortest_1069z00_3318;

					BgL__ortest_1069z00_3318 =
						BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_lz00_20);
					if (CBOOL(BgL__ortest_1069z00_3318))
						{	/* Eval/evaluate.scm 144 */
							BgL_locz00_1280 = BgL__ortest_1069z00_3318;
						}
					else
						{	/* Eval/evaluate.scm 144 */
							BgL_locz00_1280 = BgL_locz00_25;
						}
				}
				{
					obj_t BgL_e1z00_1284;
					obj_t BgL_rz00_1285;

					if (NULLP(BgL_lz00_20))
						{
							BgL_ev_littz00_bglt BgL_auxz00_5170;

							{	/* Eval/evaluate.scm 180 */
								BgL_ev_littz00_bglt BgL_new1074z00_1297;

								{	/* Eval/evaluate.scm 181 */
									BgL_ev_littz00_bglt BgL_new1073z00_1298;

									BgL_new1073z00_1298 =
										((BgL_ev_littz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_ev_littz00_bgl))));
									{	/* Eval/evaluate.scm 181 */
										long BgL_arg1393z00_1299;

										BgL_arg1393z00_1299 =
											BGL_CLASS_NUM(BGl_ev_littz00zz__evaluate_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1073z00_1298),
											BgL_arg1393z00_1299);
									}
									BgL_new1074z00_1297 = BgL_new1073z00_1298;
								}
								((((BgL_ev_littz00_bglt) COBJECT(BgL_new1074z00_1297))->
										BgL_valuez00) = ((obj_t) BUNSPEC), BUNSPEC);
								BgL_auxz00_5170 = BgL_new1074z00_1297;
							}
							return ((obj_t) BgL_auxz00_5170);
						}
					else
						{	/* Eval/evaluate.scm 178 */
							if (PAIRP(BgL_lz00_20))
								{	/* Eval/evaluate.scm 178 */
									if (NULLP(CDR(((obj_t) BgL_lz00_20))))
										{	/* Eval/evaluate.scm 178 */
											obj_t BgL_arg1389z00_1293;

											BgL_arg1389z00_1293 = CAR(((obj_t) BgL_lz00_20));
											{	/* Eval/evaluate.scm 183 */
												obj_t BgL_arg1394z00_3330;

												{	/* Eval/evaluate.scm 144 */
													obj_t BgL__ortest_1069z00_3331;

													BgL__ortest_1069z00_3331 =
														BGl_getzd2sourcezd2locationz00zz__readerz00
														(BgL_arg1389z00_1293);
													if (CBOOL(BgL__ortest_1069z00_3331))
														{	/* Eval/evaluate.scm 144 */
															BgL_arg1394z00_3330 = BgL__ortest_1069z00_3331;
														}
													else
														{	/* Eval/evaluate.scm 144 */
															BgL_arg1394z00_3330 = BgL_locz00_1280;
														}
												}
												BGL_TAIL return
													BGl_convz00zz__evaluatez00(BgL_arg1389z00_1293,
													BgL_localsz00_21, BgL_globalsz00_22,
													BgL_tailzf3zf3_23, BgL_wherez00_24,
													BgL_arg1394z00_3330, BgL_topzf3zf3_26);
											}
										}
									else
										{	/* Eval/evaluate.scm 178 */
											obj_t BgL_arg1390z00_1294;
											obj_t BgL_arg1391z00_1295;

											BgL_arg1390z00_1294 = CAR(((obj_t) BgL_lz00_20));
											BgL_arg1391z00_1295 = CDR(((obj_t) BgL_lz00_20));
											{
												BgL_ev_prog2z00_bglt BgL_auxz00_5193;

												BgL_e1z00_1284 = BgL_arg1390z00_1294;
												BgL_rz00_1285 = BgL_arg1391z00_1295;
												{	/* Eval/evaluate.scm 185 */
													BgL_ev_prog2z00_bglt BgL_new1076z00_1301;

													{	/* Eval/evaluate.scm 186 */
														BgL_ev_prog2z00_bglt BgL_new1075z00_1303;

														BgL_new1075z00_1303 =
															((BgL_ev_prog2z00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_ev_prog2z00_bgl))));
														{	/* Eval/evaluate.scm 186 */
															long BgL_arg1396z00_1304;

															BgL_arg1396z00_1304 =
																BGL_CLASS_NUM
																(BGl_ev_prog2z00zz__evaluate_typesz00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1075z00_1303), BgL_arg1396z00_1304);
														}
														BgL_new1076z00_1301 = BgL_new1075z00_1303;
													}
													{
														BgL_ev_exprz00_bglt BgL_auxz00_5198;

														{	/* Eval/evaluate.scm 186 */
															obj_t BgL_arg1395z00_1302;

															{	/* Eval/evaluate.scm 144 */
																obj_t BgL__ortest_1069z00_3327;

																BgL__ortest_1069z00_3327 =
																	BGl_getzd2sourcezd2locationz00zz__readerz00
																	(BgL_e1z00_1284);
																if (CBOOL(BgL__ortest_1069z00_3327))
																	{	/* Eval/evaluate.scm 144 */
																		BgL_arg1395z00_1302 =
																			BgL__ortest_1069z00_3327;
																	}
																else
																	{	/* Eval/evaluate.scm 144 */
																		BgL_arg1395z00_1302 = BgL_locz00_1280;
																	}
															}
															BgL_auxz00_5198 =
																((BgL_ev_exprz00_bglt)
																BGl_convz00zz__evaluatez00(BgL_e1z00_1284,
																	BgL_localsz00_21, BgL_globalsz00_22, BFALSE,
																	BgL_wherez00_24, BgL_arg1395z00_1302,
																	BgL_topzf3zf3_26));
														}
														((((BgL_ev_prog2z00_bglt)
																	COBJECT(BgL_new1076z00_1301))->BgL_e1z00) =
															((BgL_ev_exprz00_bglt) BgL_auxz00_5198), BUNSPEC);
													}
													((((BgL_ev_prog2z00_bglt)
																COBJECT(BgL_new1076z00_1301))->BgL_e2z00) =
														((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt)
																BGl_convzd2beginzd2zz__evaluatez00
																(BgL_rz00_1285, BgL_localsz00_21,
																	BgL_globalsz00_22, BgL_tailzf3zf3_23,
																	BgL_wherez00_24, BgL_locz00_1280,
																	BgL_topzf3zf3_26))), BUNSPEC);
													BgL_auxz00_5193 = BgL_new1076z00_1301;
												}
												return ((obj_t) BgL_auxz00_5193);
											}
										}
								}
							else
								{	/* Eval/evaluate.scm 178 */
									return
										BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_1280,
										BGl_string2906z00zz__evaluatez00,
										BGl_string2907z00zz__evaluatez00, BgL_lz00_20);
								}
						}
				}
			}
		}

	}



/* conv-field-ref */
	obj_t BGl_convzd2fieldzd2refz00zz__evaluatez00(obj_t BgL_ez00_30,
		obj_t BgL_localsz00_31, obj_t BgL_globalsz00_32, obj_t BgL_tailzf3zf3_33,
		obj_t BgL_wherez00_34, obj_t BgL_locz00_35, bool_t BgL_topzf3zf3_36)
	{
		{	/* Eval/evaluate.scm 203 */
			{	/* Eval/evaluate.scm 204 */
				obj_t BgL_lz00_1309;

				BgL_lz00_1309 = CDR(((obj_t) BgL_ez00_30));
				{	/* Eval/evaluate.scm 204 */
					obj_t BgL_vz00_1310;

					BgL_vz00_1310 =
						BGl_convzd2varzd2zz__evaluatez00(CAR(
							((obj_t) BgL_lz00_1309)), BgL_localsz00_31);
					{	/* Eval/evaluate.scm 205 */

						{	/* Eval/evaluate.scm 206 */
							bool_t BgL_test3120z00_5215;

							{	/* Eval/evaluate.scm 206 */
								obj_t BgL_classz00_3343;

								BgL_classz00_3343 = BGl_ev_varz00zz__evaluate_typesz00;
								if (BGL_OBJECTP(BgL_vz00_1310))
									{	/* Eval/evaluate.scm 206 */
										BgL_objectz00_bglt BgL_arg2836z00_3345;

										BgL_arg2836z00_3345 = (BgL_objectz00_bglt) (BgL_vz00_1310);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Eval/evaluate.scm 206 */
												long BgL_idxz00_3351;

												BgL_idxz00_3351 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg2836z00_3345);
												BgL_test3120z00_5215 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3351 + 2L)) == BgL_classz00_3343);
											}
										else
											{	/* Eval/evaluate.scm 206 */
												bool_t BgL_res2877z00_3376;

												{	/* Eval/evaluate.scm 206 */
													obj_t BgL_oclassz00_3359;

													{	/* Eval/evaluate.scm 206 */
														obj_t BgL_arg2848z00_3367;
														long BgL_arg2856z00_3368;

														BgL_arg2848z00_3367 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Eval/evaluate.scm 206 */
															long BgL_arg2858z00_3369;

															BgL_arg2858z00_3369 =
																BGL_OBJECT_CLASS_NUM(BgL_arg2836z00_3345);
															BgL_arg2856z00_3368 =
																(BgL_arg2858z00_3369 - OBJECT_TYPE);
														}
														BgL_oclassz00_3359 =
															VECTOR_REF(BgL_arg2848z00_3367,
															BgL_arg2856z00_3368);
													}
													{	/* Eval/evaluate.scm 206 */
														bool_t BgL__ortest_1205z00_3360;

														BgL__ortest_1205z00_3360 =
															(BgL_classz00_3343 == BgL_oclassz00_3359);
														if (BgL__ortest_1205z00_3360)
															{	/* Eval/evaluate.scm 206 */
																BgL_res2877z00_3376 = BgL__ortest_1205z00_3360;
															}
														else
															{	/* Eval/evaluate.scm 206 */
																long BgL_odepthz00_3361;

																{	/* Eval/evaluate.scm 206 */
																	obj_t BgL_arg2833z00_3362;

																	BgL_arg2833z00_3362 = (BgL_oclassz00_3359);
																	BgL_odepthz00_3361 =
																		BGL_CLASS_DEPTH(BgL_arg2833z00_3362);
																}
																if ((2L < BgL_odepthz00_3361))
																	{	/* Eval/evaluate.scm 206 */
																		obj_t BgL_arg2831z00_3364;

																		{	/* Eval/evaluate.scm 206 */
																			obj_t BgL_arg2832z00_3365;

																			BgL_arg2832z00_3365 =
																				(BgL_oclassz00_3359);
																			BgL_arg2831z00_3364 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg2832z00_3365, 2L);
																		}
																		BgL_res2877z00_3376 =
																			(BgL_arg2831z00_3364 ==
																			BgL_classz00_3343);
																	}
																else
																	{	/* Eval/evaluate.scm 206 */
																		BgL_res2877z00_3376 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3120z00_5215 = BgL_res2877z00_3376;
											}
									}
								else
									{	/* Eval/evaluate.scm 206 */
										BgL_test3120z00_5215 = ((bool_t) 0);
									}
							}
							if (BgL_test3120z00_5215)
								{	/* Eval/evaluate.scm 208 */
									obj_t BgL_g1080z00_1313;
									obj_t BgL_g1081z00_1314;

									{	/* Eval/evaluate.scm 209 */
										obj_t BgL_arg1416z00_1337;

										BgL_arg1416z00_1337 =
											(((BgL_ev_varz00_bglt) COBJECT(
													((BgL_ev_varz00_bglt) BgL_vz00_1310)))->BgL_typez00);
										BgL_g1080z00_1313 =
											BGl_classzd2existszd2zz__objectz00(BgL_arg1416z00_1337);
									}
									BgL_g1081z00_1314 = CDR(((obj_t) BgL_lz00_1309));
									{
										obj_t BgL_nodez00_1316;
										obj_t BgL_klassz00_1317;
										obj_t BgL_fieldsz00_1318;

										BgL_nodez00_1316 = BgL_vz00_1310;
										BgL_klassz00_1317 = BgL_g1080z00_1313;
										BgL_fieldsz00_1318 = BgL_g1081z00_1314;
									BgL_zc3z04anonymousza31401ze3z87_1319:
										if (NULLP(BgL_fieldsz00_1318))
											{	/* Eval/evaluate.scm 212 */
												return BgL_nodez00_1316;
											}
										else
											{	/* Eval/evaluate.scm 212 */
												if (BGl_classzf3zf3zz__objectz00(BgL_klassz00_1317))
													{	/* Eval/evaluate.scm 215 */
														obj_t BgL_fieldz00_1322;

														{	/* Eval/evaluate.scm 215 */
															obj_t BgL_arg1414z00_1333;

															BgL_arg1414z00_1333 =
																CAR(((obj_t) BgL_fieldsz00_1318));
															BgL_fieldz00_1322 =
																BGl_findzd2classzd2fieldz00zz__objectz00
																(BgL_klassz00_1317, BgL_arg1414z00_1333);
														}
														if (BGl_classzd2fieldzf3z21zz__objectz00
															(BgL_fieldz00_1322))
															{	/* Eval/evaluate.scm 217 */
																BgL_ev_appz00_bglt BgL_nodez00_1324;

																{	/* Eval/evaluate.scm 271 */
																	obj_t BgL_getz00_3379;

																	BgL_getz00_3379 =
																		BGl_classzd2fieldzd2accessorz00zz__objectz00
																		(BgL_fieldz00_1322);
																	{	/* Eval/evaluate.scm 272 */
																		BgL_ev_appz00_bglt BgL_new1088z00_3380;

																		{	/* Eval/evaluate.scm 273 */
																			BgL_ev_appz00_bglt BgL_new1087z00_3381;

																			BgL_new1087z00_3381 =
																				((BgL_ev_appz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_ev_appz00_bgl))));
																			{	/* Eval/evaluate.scm 273 */
																				long BgL_arg1449z00_3382;

																				BgL_arg1449z00_3382 =
																					BGL_CLASS_NUM
																					(BGl_ev_appz00zz__evaluate_typesz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1087z00_3381),
																					BgL_arg1449z00_3382);
																			}
																			BgL_new1088z00_3380 = BgL_new1087z00_3381;
																		}
																		((((BgL_ev_appz00_bglt)
																					COBJECT(BgL_new1088z00_3380))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_35), BUNSPEC);
																		{
																			BgL_ev_exprz00_bglt BgL_auxz00_5258;

																			{	/* Eval/evaluate.scm 274 */
																				BgL_ev_littz00_bglt BgL_new1090z00_3383;

																				{	/* Eval/evaluate.scm 274 */
																					BgL_ev_littz00_bglt
																						BgL_new1089z00_3384;
																					BgL_new1089z00_3384 =
																						((BgL_ev_littz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_ev_littz00_bgl))));
																					{	/* Eval/evaluate.scm 274 */
																						long BgL_arg1447z00_3385;

																						{	/* Eval/evaluate.scm 274 */
																							obj_t BgL_classz00_3390;

																							BgL_classz00_3390 =
																								BGl_ev_littz00zz__evaluate_typesz00;
																							BgL_arg1447z00_3385 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3390);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1089z00_3384),
																							BgL_arg1447z00_3385);
																					}
																					BgL_new1090z00_3383 =
																						BgL_new1089z00_3384;
																				}
																				((((BgL_ev_littz00_bglt)
																							COBJECT(BgL_new1090z00_3383))->
																						BgL_valuez00) =
																					((obj_t) BgL_getz00_3379), BUNSPEC);
																				BgL_auxz00_5258 =
																					((BgL_ev_exprz00_bglt)
																					BgL_new1090z00_3383);
																			}
																			((((BgL_ev_appz00_bglt)
																						COBJECT(BgL_new1088z00_3380))->
																					BgL_funz00) =
																				((BgL_ev_exprz00_bglt) BgL_auxz00_5258),
																				BUNSPEC);
																		}
																		{
																			obj_t BgL_auxz00_5266;

																			{	/* Eval/evaluate.scm 275 */
																				obj_t BgL_list1448z00_3386;

																				BgL_list1448z00_3386 =
																					MAKE_YOUNG_PAIR(BgL_nodez00_1316,
																					BNIL);
																				BgL_auxz00_5266 = BgL_list1448z00_3386;
																			}
																			((((BgL_ev_appz00_bglt)
																						COBJECT(BgL_new1088z00_3380))->
																					BgL_argsz00) =
																				((obj_t) BgL_auxz00_5266), BUNSPEC);
																		}
																		((((BgL_ev_appz00_bglt)
																					COBJECT(BgL_new1088z00_3380))->
																				BgL_tailzf3zf3) =
																			((obj_t) BgL_tailzf3zf3_33), BUNSPEC);
																		BgL_nodez00_1324 = BgL_new1088z00_3380;
																}}
																{	/* Eval/evaluate.scm 220 */
																	obj_t BgL_arg1405z00_1325;
																	obj_t BgL_arg1406z00_1326;

																	BgL_arg1405z00_1325 =
																		BGl_classzd2fieldzd2typez00zz__objectz00
																		(BgL_fieldz00_1322);
																	BgL_arg1406z00_1326 =
																		CDR(((obj_t) BgL_fieldsz00_1318));
																	{
																		obj_t BgL_fieldsz00_5276;
																		obj_t BgL_klassz00_5275;
																		obj_t BgL_nodez00_5273;

																		BgL_nodez00_5273 =
																			((obj_t) BgL_nodez00_1324);
																		BgL_klassz00_5275 = BgL_arg1405z00_1325;
																		BgL_fieldsz00_5276 = BgL_arg1406z00_1326;
																		BgL_fieldsz00_1318 = BgL_fieldsz00_5276;
																		BgL_klassz00_1317 = BgL_klassz00_5275;
																		BgL_nodez00_1316 = BgL_nodez00_5273;
																		goto BgL_zc3z04anonymousza31401ze3z87_1319;
																	}
																}
															}
														else
															{	/* Eval/evaluate.scm 222 */
																obj_t BgL_arg1407z00_1327;
																obj_t BgL_arg1408z00_1328;

																BgL_arg1407z00_1327 =
																	(((BgL_ev_varz00_bglt) COBJECT(
																			((BgL_ev_varz00_bglt) BgL_vz00_1310)))->
																	BgL_typez00);
																{	/* Eval/evaluate.scm 223 */
																	obj_t BgL_arg1410z00_1329;
																	obj_t BgL_arg1411z00_1330;

																	BgL_arg1410z00_1329 =
																		(((BgL_ev_varz00_bglt) COBJECT(
																				((BgL_ev_varz00_bglt) BgL_vz00_1310)))->
																		BgL_typez00);
																	BgL_arg1411z00_1330 =
																		CAR(((obj_t) BgL_fieldsz00_1318));
																	{	/* Eval/evaluate.scm 223 */
																		obj_t BgL_list1412z00_1331;

																		{	/* Eval/evaluate.scm 223 */
																			obj_t BgL_arg1413z00_1332;

																			BgL_arg1413z00_1332 =
																				MAKE_YOUNG_PAIR(BgL_arg1411z00_1330,
																				BNIL);
																			BgL_list1412z00_1331 =
																				MAKE_YOUNG_PAIR(BgL_arg1410z00_1329,
																				BgL_arg1413z00_1332);
																		}
																		BgL_arg1408z00_1328 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string2908z00zz__evaluatez00,
																			BgL_list1412z00_1331);
																	}
																}
																return
																	BGl_evcompilezd2errorzd2zz__evcompilez00
																	(BgL_locz00_35, BgL_arg1407z00_1327,
																	BgL_arg1408z00_1328, BgL_ez00_30);
															}
													}
												else
													{	/* Eval/evaluate.scm 226 */
														obj_t BgL_arg1415z00_1334;

														{	/* Eval/evaluate.scm 226 */
															obj_t BgL__ortest_1082z00_1335;

															BgL__ortest_1082z00_1335 =
																(((BgL_ev_varz00_bglt) COBJECT(
																		((BgL_ev_varz00_bglt) BgL_vz00_1310)))->
																BgL_typez00);
															if (CBOOL(BgL__ortest_1082z00_1335))
																{	/* Eval/evaluate.scm 226 */
																	BgL_arg1415z00_1334 =
																		BgL__ortest_1082z00_1335;
																}
															else
																{	/* Eval/evaluate.scm 226 */
																	BgL_arg1415z00_1334 =
																		(((BgL_ev_varz00_bglt) COBJECT(
																				((BgL_ev_varz00_bglt) BgL_vz00_1310)))->
																		BgL_namez00);
																}
														}
														return
															BGl_evcompilezd2errorzd2zz__evcompilez00
															(BgL_locz00_35, BgL_arg1415z00_1334,
															BGl_string2909z00zz__evaluatez00, BgL_ez00_30);
													}
											}
									}
								}
							else
								{	/* Eval/evaluate.scm 228 */
									obj_t BgL_arg1417z00_1338;

									{	/* Eval/evaluate.scm 228 */
										obj_t BgL_pairz00_3399;

										BgL_pairz00_3399 = CDR(((obj_t) BgL_ez00_30));
										BgL_arg1417z00_1338 = CAR(BgL_pairz00_3399);
									}
									return
										BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_35,
										BgL_arg1417z00_1338, BGl_string2910z00zz__evaluatez00,
										BgL_ez00_30);
								}
						}
					}
				}
			}
		}

	}



/* conv-field-set */
	obj_t BGl_convzd2fieldzd2setz00zz__evaluatez00(obj_t BgL_lz00_37,
		obj_t BgL_e2z00_38, obj_t BgL_ez00_39, obj_t BgL_localsz00_40,
		obj_t BgL_globalsz00_41, obj_t BgL_tailzf3zf3_42, obj_t BgL_wherez00_43,
		obj_t BgL_locz00_44, bool_t BgL_topzf3zf3_45)
	{
		{	/* Eval/evaluate.scm 233 */
			{	/* Eval/evaluate.scm 234 */
				obj_t BgL_vz00_1340;

				BgL_vz00_1340 =
					BGl_convzd2varzd2zz__evaluatez00(CAR(BgL_lz00_37), BgL_localsz00_40);
				{	/* Eval/evaluate.scm 234 */
					obj_t BgL_e2z00_1341;

					BgL_e2z00_1341 =
						BGl_convz00zz__evaluatez00(BgL_e2z00_38, BgL_localsz00_40,
						BgL_globalsz00_41, BFALSE, BgL_wherez00_43, BgL_locz00_44,
						((bool_t) 0));
					{	/* Eval/evaluate.scm 235 */

						{	/* Eval/evaluate.scm 236 */
							bool_t BgL_test3129z00_5301;

							{	/* Eval/evaluate.scm 236 */
								obj_t BgL_classz00_3401;

								BgL_classz00_3401 = BGl_ev_varz00zz__evaluate_typesz00;
								if (BGL_OBJECTP(BgL_vz00_1340))
									{	/* Eval/evaluate.scm 236 */
										BgL_objectz00_bglt BgL_arg2836z00_3403;

										BgL_arg2836z00_3403 = (BgL_objectz00_bglt) (BgL_vz00_1340);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Eval/evaluate.scm 236 */
												long BgL_idxz00_3409;

												BgL_idxz00_3409 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg2836z00_3403);
												BgL_test3129z00_5301 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3409 + 2L)) == BgL_classz00_3401);
											}
										else
											{	/* Eval/evaluate.scm 236 */
												bool_t BgL_res2879z00_3434;

												{	/* Eval/evaluate.scm 236 */
													obj_t BgL_oclassz00_3417;

													{	/* Eval/evaluate.scm 236 */
														obj_t BgL_arg2848z00_3425;
														long BgL_arg2856z00_3426;

														BgL_arg2848z00_3425 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Eval/evaluate.scm 236 */
															long BgL_arg2858z00_3427;

															BgL_arg2858z00_3427 =
																BGL_OBJECT_CLASS_NUM(BgL_arg2836z00_3403);
															BgL_arg2856z00_3426 =
																(BgL_arg2858z00_3427 - OBJECT_TYPE);
														}
														BgL_oclassz00_3417 =
															VECTOR_REF(BgL_arg2848z00_3425,
															BgL_arg2856z00_3426);
													}
													{	/* Eval/evaluate.scm 236 */
														bool_t BgL__ortest_1205z00_3418;

														BgL__ortest_1205z00_3418 =
															(BgL_classz00_3401 == BgL_oclassz00_3417);
														if (BgL__ortest_1205z00_3418)
															{	/* Eval/evaluate.scm 236 */
																BgL_res2879z00_3434 = BgL__ortest_1205z00_3418;
															}
														else
															{	/* Eval/evaluate.scm 236 */
																long BgL_odepthz00_3419;

																{	/* Eval/evaluate.scm 236 */
																	obj_t BgL_arg2833z00_3420;

																	BgL_arg2833z00_3420 = (BgL_oclassz00_3417);
																	BgL_odepthz00_3419 =
																		BGL_CLASS_DEPTH(BgL_arg2833z00_3420);
																}
																if ((2L < BgL_odepthz00_3419))
																	{	/* Eval/evaluate.scm 236 */
																		obj_t BgL_arg2831z00_3422;

																		{	/* Eval/evaluate.scm 236 */
																			obj_t BgL_arg2832z00_3423;

																			BgL_arg2832z00_3423 =
																				(BgL_oclassz00_3417);
																			BgL_arg2831z00_3422 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg2832z00_3423, 2L);
																		}
																		BgL_res2879z00_3434 =
																			(BgL_arg2831z00_3422 ==
																			BgL_classz00_3401);
																	}
																else
																	{	/* Eval/evaluate.scm 236 */
																		BgL_res2879z00_3434 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3129z00_5301 = BgL_res2879z00_3434;
											}
									}
								else
									{	/* Eval/evaluate.scm 236 */
										BgL_test3129z00_5301 = ((bool_t) 0);
									}
							}
							if (BgL_test3129z00_5301)
								{	/* Eval/evaluate.scm 238 */
									obj_t BgL_g1084z00_1344;
									obj_t BgL_g1085z00_1345;

									{	/* Eval/evaluate.scm 239 */
										obj_t BgL_arg1444z00_1376;

										BgL_arg1444z00_1376 =
											(((BgL_ev_varz00_bglt) COBJECT(
													((BgL_ev_varz00_bglt) BgL_vz00_1340)))->BgL_typez00);
										BgL_g1084z00_1344 =
											BGl_classzd2existszd2zz__objectz00(BgL_arg1444z00_1376);
									}
									BgL_g1085z00_1345 = CDR(BgL_lz00_37);
									{
										obj_t BgL_nodez00_1347;
										obj_t BgL_klassz00_1348;
										obj_t BgL_fieldsz00_1349;

										BgL_nodez00_1347 = BgL_vz00_1340;
										BgL_klassz00_1348 = BgL_g1084z00_1344;
										BgL_fieldsz00_1349 = BgL_g1085z00_1345;
									BgL_zc3z04anonymousza31420ze3z87_1350:
										if (NULLP(BgL_fieldsz00_1349))
											{	/* Eval/evaluate.scm 242 */
												return BgL_nodez00_1347;
											}
										else
											{	/* Eval/evaluate.scm 242 */
												if (BGl_classzf3zf3zz__objectz00(BgL_klassz00_1348))
													{	/* Eval/evaluate.scm 245 */
														obj_t BgL_fieldz00_1353;

														{	/* Eval/evaluate.scm 245 */
															obj_t BgL_arg1442z00_1372;

															BgL_arg1442z00_1372 =
																CAR(((obj_t) BgL_fieldsz00_1349));
															BgL_fieldz00_1353 =
																BGl_findzd2classzd2fieldz00zz__objectz00
																(BgL_klassz00_1348, BgL_arg1442z00_1372);
														}
														if (BGl_classzd2fieldzf3z21zz__objectz00
															(BgL_fieldz00_1353))
															{	/* Eval/evaluate.scm 246 */
																if (NULLP(CDR(((obj_t) BgL_fieldsz00_1349))))
																	{	/* Eval/evaluate.scm 247 */
																		if (BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(BgL_fieldz00_1353))
																			{	/* Eval/evaluate.scm 250 */
																				obj_t BgL_arg1427z00_1358;

																				{	/* Eval/evaluate.scm 250 */
																					obj_t BgL_list1428z00_1359;

																					{	/* Eval/evaluate.scm 250 */
																						obj_t BgL_arg1429z00_1360;

																						BgL_arg1429z00_1360 =
																							MAKE_YOUNG_PAIR(BgL_e2z00_1341,
																							BNIL);
																						BgL_list1428z00_1359 =
																							MAKE_YOUNG_PAIR(BgL_nodez00_1347,
																							BgL_arg1429z00_1360);
																					}
																					BgL_arg1427z00_1358 =
																						BgL_list1428z00_1359;
																				}
																				{	/* Eval/evaluate.scm 282 */
																					obj_t BgL_setz00_3439;

																					BgL_setz00_3439 =
																						BGl_classzd2fieldzd2mutatorz00zz__objectz00
																						(BgL_fieldz00_1353);
																					{	/* Eval/evaluate.scm 283 */
																						BgL_ev_appz00_bglt
																							BgL_new1092z00_3440;
																						{	/* Eval/evaluate.scm 284 */
																							BgL_ev_appz00_bglt
																								BgL_new1091z00_3441;
																							BgL_new1091z00_3441 =
																								((BgL_ev_appz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_ev_appz00_bgl))));
																							{	/* Eval/evaluate.scm 284 */
																								long BgL_arg1451z00_3442;

																								BgL_arg1451z00_3442 =
																									BGL_CLASS_NUM
																									(BGl_ev_appz00zz__evaluate_typesz00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1091z00_3441),
																									BgL_arg1451z00_3442);
																							}
																							BgL_new1092z00_3440 =
																								BgL_new1091z00_3441;
																						}
																						((((BgL_ev_appz00_bglt)
																									COBJECT
																									(BgL_new1092z00_3440))->
																								BgL_locz00) =
																							((obj_t) BgL_locz00_44), BUNSPEC);
																						{
																							BgL_ev_exprz00_bglt
																								BgL_auxz00_5351;
																							{	/* Eval/evaluate.scm 285 */
																								BgL_ev_littz00_bglt
																									BgL_new1094z00_3443;
																								{	/* Eval/evaluate.scm 285 */
																									BgL_ev_littz00_bglt
																										BgL_new1093z00_3444;
																									BgL_new1093z00_3444 =
																										((BgL_ev_littz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_ev_littz00_bgl))));
																									{	/* Eval/evaluate.scm 285 */
																										long BgL_arg1450z00_3445;

																										{	/* Eval/evaluate.scm 285 */
																											obj_t BgL_classz00_3449;

																											BgL_classz00_3449 =
																												BGl_ev_littz00zz__evaluate_typesz00;
																											BgL_arg1450z00_3445 =
																												BGL_CLASS_NUM
																												(BgL_classz00_3449);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1093z00_3444),
																											BgL_arg1450z00_3445);
																									}
																									BgL_new1094z00_3443 =
																										BgL_new1093z00_3444;
																								}
																								((((BgL_ev_littz00_bglt)
																											COBJECT
																											(BgL_new1094z00_3443))->
																										BgL_valuez00) =
																									((obj_t) BgL_setz00_3439),
																									BUNSPEC);
																								BgL_auxz00_5351 =
																									((BgL_ev_exprz00_bglt)
																									BgL_new1094z00_3443);
																							}
																							((((BgL_ev_appz00_bglt)
																										COBJECT
																										(BgL_new1092z00_3440))->
																									BgL_funz00) =
																								((BgL_ev_exprz00_bglt)
																									BgL_auxz00_5351), BUNSPEC);
																						}
																						((((BgL_ev_appz00_bglt)
																									COBJECT
																									(BgL_new1092z00_3440))->
																								BgL_argsz00) =
																							((obj_t) BgL_arg1427z00_1358),
																							BUNSPEC);
																						((((BgL_ev_appz00_bglt)
																									COBJECT
																									(BgL_new1092z00_3440))->
																								BgL_tailzf3zf3) =
																							((obj_t) BgL_tailzf3zf3_42),
																							BUNSPEC);
																						return ((obj_t)
																							BgL_new1092z00_3440);
																					}
																				}
																			}
																		else
																			{	/* Eval/evaluate.scm 251 */
																				obj_t BgL_arg1430z00_1361;

																				BgL_arg1430z00_1361 =
																					CAR(((obj_t) BgL_fieldsz00_1349));
																				return
																					BGl_evcompilezd2errorzd2zz__evcompilez00
																					(BgL_locz00_44, BgL_arg1430z00_1361,
																					BGl_string2911z00zz__evaluatez00,
																					BgL_ez00_39);
																			}
																	}
																else
																	{	/* Eval/evaluate.scm 254 */
																		BgL_ev_appz00_bglt BgL_nodez00_1362;

																		{	/* Eval/evaluate.scm 271 */
																			obj_t BgL_getz00_3453;

																			BgL_getz00_3453 =
																				BGl_classzd2fieldzd2accessorz00zz__objectz00
																				(BgL_fieldz00_1353);
																			{	/* Eval/evaluate.scm 272 */
																				BgL_ev_appz00_bglt BgL_new1088z00_3454;

																				{	/* Eval/evaluate.scm 273 */
																					BgL_ev_appz00_bglt
																						BgL_new1087z00_3455;
																					BgL_new1087z00_3455 =
																						((BgL_ev_appz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_ev_appz00_bgl))));
																					{	/* Eval/evaluate.scm 273 */
																						long BgL_arg1449z00_3456;

																						BgL_arg1449z00_3456 =
																							BGL_CLASS_NUM
																							(BGl_ev_appz00zz__evaluate_typesz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1087z00_3455),
																							BgL_arg1449z00_3456);
																					}
																					BgL_new1088z00_3454 =
																						BgL_new1087z00_3455;
																				}
																				((((BgL_ev_appz00_bglt)
																							COBJECT(BgL_new1088z00_3454))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_44), BUNSPEC);
																				{
																					BgL_ev_exprz00_bglt BgL_auxz00_5371;

																					{	/* Eval/evaluate.scm 274 */
																						BgL_ev_littz00_bglt
																							BgL_new1090z00_3457;
																						{	/* Eval/evaluate.scm 274 */
																							BgL_ev_littz00_bglt
																								BgL_new1089z00_3458;
																							BgL_new1089z00_3458 =
																								((BgL_ev_littz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_ev_littz00_bgl))));
																							{	/* Eval/evaluate.scm 274 */
																								long BgL_arg1447z00_3459;

																								{	/* Eval/evaluate.scm 274 */
																									obj_t BgL_classz00_3464;

																									BgL_classz00_3464 =
																										BGl_ev_littz00zz__evaluate_typesz00;
																									BgL_arg1447z00_3459 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3464);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1089z00_3458),
																									BgL_arg1447z00_3459);
																							}
																							BgL_new1090z00_3457 =
																								BgL_new1089z00_3458;
																						}
																						((((BgL_ev_littz00_bglt)
																									COBJECT
																									(BgL_new1090z00_3457))->
																								BgL_valuez00) =
																							((obj_t) BgL_getz00_3453),
																							BUNSPEC);
																						BgL_auxz00_5371 =
																							((BgL_ev_exprz00_bglt)
																							BgL_new1090z00_3457);
																					}
																					((((BgL_ev_appz00_bglt)
																								COBJECT(BgL_new1088z00_3454))->
																							BgL_funz00) =
																						((BgL_ev_exprz00_bglt)
																							BgL_auxz00_5371), BUNSPEC);
																				}
																				{
																					obj_t BgL_auxz00_5379;

																					{	/* Eval/evaluate.scm 275 */
																						obj_t BgL_list1448z00_3460;

																						BgL_list1448z00_3460 =
																							MAKE_YOUNG_PAIR(BgL_nodez00_1347,
																							BNIL);
																						BgL_auxz00_5379 =
																							BgL_list1448z00_3460;
																					}
																					((((BgL_ev_appz00_bglt)
																								COBJECT(BgL_new1088z00_3454))->
																							BgL_argsz00) =
																						((obj_t) BgL_auxz00_5379), BUNSPEC);
																				}
																				((((BgL_ev_appz00_bglt)
																							COBJECT(BgL_new1088z00_3454))->
																						BgL_tailzf3zf3) =
																					((obj_t) BgL_tailzf3zf3_42), BUNSPEC);
																				BgL_nodez00_1362 = BgL_new1088z00_3454;
																		}}
																		{	/* Eval/evaluate.scm 257 */
																			obj_t BgL_arg1431z00_1363;
																			obj_t BgL_arg1434z00_1364;

																			BgL_arg1431z00_1363 =
																				BGl_classzd2fieldzd2typez00zz__objectz00
																				(BgL_fieldz00_1353);
																			BgL_arg1434z00_1364 =
																				CDR(((obj_t) BgL_fieldsz00_1349));
																			{
																				obj_t BgL_fieldsz00_5389;
																				obj_t BgL_klassz00_5388;
																				obj_t BgL_nodez00_5386;

																				BgL_nodez00_5386 =
																					((obj_t) BgL_nodez00_1362);
																				BgL_klassz00_5388 = BgL_arg1431z00_1363;
																				BgL_fieldsz00_5389 =
																					BgL_arg1434z00_1364;
																				BgL_fieldsz00_1349 = BgL_fieldsz00_5389;
																				BgL_klassz00_1348 = BgL_klassz00_5388;
																				BgL_nodez00_1347 = BgL_nodez00_5386;
																				goto
																					BgL_zc3z04anonymousza31420ze3z87_1350;
																			}
																		}
																	}
															}
														else
															{	/* Eval/evaluate.scm 259 */
																obj_t BgL_arg1436z00_1366;
																obj_t BgL_arg1437z00_1367;

																BgL_arg1436z00_1366 =
																	(((BgL_ev_varz00_bglt) COBJECT(
																			((BgL_ev_varz00_bglt) BgL_vz00_1340)))->
																	BgL_typez00);
																{	/* Eval/evaluate.scm 260 */
																	obj_t BgL_arg1438z00_1368;
																	obj_t BgL_arg1439z00_1369;

																	BgL_arg1438z00_1368 =
																		(((BgL_ev_varz00_bglt) COBJECT(
																				((BgL_ev_varz00_bglt) BgL_vz00_1340)))->
																		BgL_typez00);
																	BgL_arg1439z00_1369 =
																		CAR(((obj_t) BgL_fieldsz00_1349));
																	{	/* Eval/evaluate.scm 260 */
																		obj_t BgL_list1440z00_1370;

																		{	/* Eval/evaluate.scm 260 */
																			obj_t BgL_arg1441z00_1371;

																			BgL_arg1441z00_1371 =
																				MAKE_YOUNG_PAIR(BgL_arg1439z00_1369,
																				BNIL);
																			BgL_list1440z00_1370 =
																				MAKE_YOUNG_PAIR(BgL_arg1438z00_1368,
																				BgL_arg1441z00_1371);
																		}
																		BgL_arg1437z00_1367 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string2908z00zz__evaluatez00,
																			BgL_list1440z00_1370);
																	}
																}
																return
																	BGl_evcompilezd2errorzd2zz__evcompilez00
																	(BgL_locz00_44, BgL_arg1436z00_1366,
																	BgL_arg1437z00_1367, BgL_ez00_39);
															}
													}
												else
													{	/* Eval/evaluate.scm 264 */
														obj_t BgL_arg1443z00_1373;

														{	/* Eval/evaluate.scm 264 */
															obj_t BgL__ortest_1086z00_1374;

															BgL__ortest_1086z00_1374 =
																(((BgL_ev_varz00_bglt) COBJECT(
																		((BgL_ev_varz00_bglt) BgL_vz00_1340)))->
																BgL_typez00);
															if (CBOOL(BgL__ortest_1086z00_1374))
																{	/* Eval/evaluate.scm 264 */
																	BgL_arg1443z00_1373 =
																		BgL__ortest_1086z00_1374;
																}
															else
																{	/* Eval/evaluate.scm 264 */
																	BgL_arg1443z00_1373 =
																		(((BgL_ev_varz00_bglt) COBJECT(
																				((BgL_ev_varz00_bglt) BgL_vz00_1340)))->
																		BgL_namez00);
																}
														}
														return
															BGl_evcompilezd2errorzd2zz__evcompilez00
															(BgL_locz00_44, BgL_arg1443z00_1373,
															BGl_string2909z00zz__evaluatez00, BgL_ez00_39);
													}
											}
									}
								}
							else
								{	/* Eval/evaluate.scm 236 */
									return
										BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_44,
										CAR(BgL_lz00_37), BGl_string2910z00zz__evaluatez00,
										BgL_ez00_39);
								}
						}
					}
				}
			}
		}

	}



/* type-check */
	obj_t BGl_typezd2checkzd2zz__evaluatez00(obj_t BgL_varz00_56,
		obj_t BgL_tnamez00_57, obj_t BgL_locz00_58, obj_t BgL_procnamez00_59,
		obj_t BgL_bodyz00_60)
	{
		{	/* Eval/evaluate.scm 298 */
			if (SYMBOLP(BgL_tnamez00_57))
				{	/* Eval/evaluate.scm 301 */
					obj_t BgL_predz00_1397;

					if ((BgL_tnamez00_57 == BGl_symbol2912z00zz__evaluatez00))
						{	/* Eval/evaluate.scm 301 */
							BgL_predz00_1397 = BGl_symbol2914z00zz__evaluatez00;
						}
					else
						{	/* Eval/evaluate.scm 301 */
							if ((BgL_tnamez00_57 == BGl_symbol2916z00zz__evaluatez00))
								{	/* Eval/evaluate.scm 301 */
									BgL_predz00_1397 = BGl_symbol2917z00zz__evaluatez00;
								}
							else
								{	/* Eval/evaluate.scm 301 */
									if ((BgL_tnamez00_57 == BGl_symbol2919z00zz__evaluatez00))
										{	/* Eval/evaluate.scm 301 */
											BgL_predz00_1397 = BGl_symbol2921z00zz__evaluatez00;
										}
									else
										{	/* Eval/evaluate.scm 301 */
											if ((BgL_tnamez00_57 == BGl_symbol2923z00zz__evaluatez00))
												{	/* Eval/evaluate.scm 301 */
													BgL_predz00_1397 = BGl_symbol2925z00zz__evaluatez00;
												}
											else
												{	/* Eval/evaluate.scm 301 */
													bool_t BgL_test3145z00_5419;

													{	/* Eval/evaluate.scm 301 */
														bool_t BgL__ortest_1095z00_1473;

														BgL__ortest_1095z00_1473 =
															(BgL_tnamez00_57 ==
															BGl_symbol2927z00zz__evaluatez00);
														if (BgL__ortest_1095z00_1473)
															{	/* Eval/evaluate.scm 301 */
																BgL_test3145z00_5419 = BgL__ortest_1095z00_1473;
															}
														else
															{	/* Eval/evaluate.scm 301 */
																BgL_test3145z00_5419 =
																	(BgL_tnamez00_57 ==
																	BGl_symbol2929z00zz__evaluatez00);
															}
													}
													if (BgL_test3145z00_5419)
														{	/* Eval/evaluate.scm 301 */
															BgL_predz00_1397 =
																BGl_symbol2930z00zz__evaluatez00;
														}
													else
														{	/* Eval/evaluate.scm 301 */
															bool_t BgL_test3147z00_5423;

															{	/* Eval/evaluate.scm 301 */
																bool_t BgL__ortest_1096z00_1472;

																BgL__ortest_1096z00_1472 =
																	(BgL_tnamez00_57 ==
																	BGl_symbol2932z00zz__evaluatez00);
																if (BgL__ortest_1096z00_1472)
																	{	/* Eval/evaluate.scm 301 */
																		BgL_test3147z00_5423 =
																			BgL__ortest_1096z00_1472;
																	}
																else
																	{	/* Eval/evaluate.scm 301 */
																		BgL_test3147z00_5423 =
																			(BgL_tnamez00_57 ==
																			BGl_symbol2934z00zz__evaluatez00);
																	}
															}
															if (BgL_test3147z00_5423)
																{	/* Eval/evaluate.scm 301 */
																	BgL_predz00_1397 =
																		BGl_symbol2936z00zz__evaluatez00;
																}
															else
																{	/* Eval/evaluate.scm 301 */
																	if (
																		(BgL_tnamez00_57 ==
																			BGl_symbol2938z00zz__evaluatez00))
																		{	/* Eval/evaluate.scm 301 */
																			BgL_predz00_1397 =
																				BGl_symbol2940z00zz__evaluatez00;
																		}
																	else
																		{	/* Eval/evaluate.scm 301 */
																			if (
																				(BgL_tnamez00_57 ==
																					BGl_symbol2942z00zz__evaluatez00))
																				{	/* Eval/evaluate.scm 301 */
																					BgL_predz00_1397 =
																						BGl_symbol2944z00zz__evaluatez00;
																				}
																			else
																				{	/* Eval/evaluate.scm 301 */
																					if (
																						(BgL_tnamez00_57 ==
																							BGl_symbol2946z00zz__evaluatez00))
																						{	/* Eval/evaluate.scm 301 */
																							BgL_predz00_1397 =
																								BGl_symbol2948z00zz__evaluatez00;
																						}
																					else
																						{	/* Eval/evaluate.scm 301 */
																							bool_t BgL_test3152z00_5433;

																							{	/* Eval/evaluate.scm 301 */
																								bool_t BgL__ortest_1097z00_1471;

																								BgL__ortest_1097z00_1471 =
																									(BgL_tnamez00_57 ==
																									BGl_symbol2950z00zz__evaluatez00);
																								if (BgL__ortest_1097z00_1471)
																									{	/* Eval/evaluate.scm 301 */
																										BgL_test3152z00_5433 =
																											BgL__ortest_1097z00_1471;
																									}
																								else
																									{	/* Eval/evaluate.scm 301 */
																										BgL_test3152z00_5433 =
																											(BgL_tnamez00_57 ==
																											BGl_symbol2952z00zz__evaluatez00);
																									}
																							}
																							if (BgL_test3152z00_5433)
																								{	/* Eval/evaluate.scm 301 */
																									BgL_predz00_1397 =
																										BGl_symbol2954z00zz__evaluatez00;
																								}
																							else
																								{	/* Eval/evaluate.scm 312 */
																									obj_t BgL_arg1500z00_1451;

																									{	/* Eval/evaluate.scm 312 */
																										obj_t BgL_arg1501z00_1452;
																										obj_t BgL_arg1502z00_1453;

																										BgL_arg1501z00_1452 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2956z00zz__evaluatez00,
																											BNIL);
																										{	/* Eval/evaluate.scm 313 */
																											obj_t BgL_arg1503z00_1454;

																											{	/* Eval/evaluate.scm 313 */
																												obj_t
																													BgL_arg1504z00_1455;
																												{	/* Eval/evaluate.scm 313 */
																													obj_t
																														BgL_arg1505z00_1456;
																													obj_t
																														BgL_arg1506z00_1457;
																													{	/* Eval/evaluate.scm 313 */
																														obj_t
																															BgL_arg1507z00_1458;
																														{	/* Eval/evaluate.scm 313 */
																															obj_t
																																BgL_arg1508z00_1459;
																															{	/* Eval/evaluate.scm 313 */
																																obj_t
																																	BgL_arg1509z00_1460;
																																{	/* Eval/evaluate.scm 313 */
																																	obj_t
																																		BgL_arg1510z00_1461;
																																	{	/* Eval/evaluate.scm 313 */
																																		obj_t
																																			BgL_arg1511z00_1462;
																																		{	/* Eval/evaluate.scm 313 */
																																			obj_t
																																				BgL_arg1513z00_1463;
																																			BgL_arg1513z00_1463
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_tnamez00_57,
																																				BNIL);
																																			BgL_arg1511z00_1462
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2958z00zz__evaluatez00,
																																				BgL_arg1513z00_1463);
																																		}
																																		BgL_arg1510z00_1461
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1511z00_1462,
																																			BNIL);
																																	}
																																	BgL_arg1509z00_1460
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2960z00zz__evaluatez00,
																																		BgL_arg1510z00_1461);
																																}
																																BgL_arg1508z00_1459
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1509z00_1460,
																																	BNIL);
																															}
																															BgL_arg1507z00_1458
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2962z00zz__evaluatez00,
																																BgL_arg1508z00_1459);
																														}
																														BgL_arg1505z00_1456
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1507z00_1458,
																															BNIL);
																													}
																													{	/* Eval/evaluate.scm 314 */
																														obj_t
																															BgL_arg1514z00_1464;
																														{	/* Eval/evaluate.scm 314 */
																															obj_t
																																BgL_arg1516z00_1465;
																															{	/* Eval/evaluate.scm 314 */
																																obj_t
																																	BgL_arg1517z00_1466;
																																{	/* Eval/evaluate.scm 314 */
																																	obj_t
																																		BgL_arg1521z00_1467;
																																	obj_t
																																		BgL_arg1522z00_1468;
																																	{	/* Eval/evaluate.scm 314 */
																																		obj_t
																																			BgL_arg1523z00_1469;
																																		{	/* Eval/evaluate.scm 314 */
																																			obj_t
																																				BgL_arg1524z00_1470;
																																			BgL_arg1524z00_1470
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2962z00zz__evaluatez00,
																																				BNIL);
																																			BgL_arg1523z00_1469
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2956z00zz__evaluatez00,
																																				BgL_arg1524z00_1470);
																																		}
																																		BgL_arg1521z00_1467
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2964z00zz__evaluatez00,
																																			BgL_arg1523z00_1469);
																																	}
																																	BgL_arg1522z00_1468
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BTRUE,
																																		BNIL);
																																	BgL_arg1517z00_1466
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1521z00_1467,
																																		BgL_arg1522z00_1468);
																																}
																																BgL_arg1516z00_1465
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol2962z00zz__evaluatez00,
																																	BgL_arg1517z00_1466);
																															}
																															BgL_arg1514z00_1464
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2966z00zz__evaluatez00,
																																BgL_arg1516z00_1465);
																														}
																														BgL_arg1506z00_1457
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1514z00_1464,
																															BNIL);
																													}
																													BgL_arg1504z00_1455 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1505z00_1456,
																														BgL_arg1506z00_1457);
																												}
																												BgL_arg1503z00_1454 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2968z00zz__evaluatez00,
																													BgL_arg1504z00_1455);
																											}
																											BgL_arg1502z00_1453 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1503z00_1454,
																												BNIL);
																										}
																										BgL_arg1500z00_1451 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1501z00_1452,
																											BgL_arg1502z00_1453);
																									}
																									BgL_predz00_1397 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2970z00zz__evaluatez00,
																										BgL_arg1500z00_1451);
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
					{	/* Eval/evaluate.scm 316 */
						obj_t BgL_arg1455z00_1398;

						{	/* Eval/evaluate.scm 316 */
							obj_t BgL_arg1456z00_1399;

							{	/* Eval/evaluate.scm 316 */
								obj_t BgL_arg1457z00_1400;
								obj_t BgL_arg1458z00_1401;

								{	/* Eval/evaluate.scm 316 */
									obj_t BgL_arg1459z00_1402;

									BgL_arg1459z00_1402 = MAKE_YOUNG_PAIR(BgL_varz00_56, BNIL);
									BgL_arg1457z00_1400 =
										MAKE_YOUNG_PAIR(BgL_predz00_1397, BgL_arg1459z00_1402);
								}
								{	/* Eval/evaluate.scm 316 */
									obj_t BgL_arg1460z00_1403;

									{	/* Eval/evaluate.scm 316 */
										obj_t BgL_arg1461z00_1404;

										{
											obj_t BgL_fnamez00_1405;
											obj_t BgL_posz00_1406;

											if (PAIRP(BgL_locz00_58))
												{	/* Eval/evaluate.scm 316 */
													obj_t BgL_cdrzd2138zd2_1411;

													BgL_cdrzd2138zd2_1411 = CDR(((obj_t) BgL_locz00_58));
													if (
														(CAR(
																((obj_t) BgL_locz00_58)) ==
															BGl_symbol2976z00zz__evaluatez00))
														{	/* Eval/evaluate.scm 316 */
															if (PAIRP(BgL_cdrzd2138zd2_1411))
																{	/* Eval/evaluate.scm 316 */
																	obj_t BgL_cdrzd2142zd2_1415;

																	BgL_cdrzd2142zd2_1415 =
																		CDR(BgL_cdrzd2138zd2_1411);
																	if (PAIRP(BgL_cdrzd2142zd2_1415))
																		{	/* Eval/evaluate.scm 316 */
																			if (NULLP(CDR(BgL_cdrzd2142zd2_1415)))
																				{	/* Eval/evaluate.scm 316 */
																					BgL_fnamez00_1405 =
																						CAR(BgL_cdrzd2138zd2_1411);
																					BgL_posz00_1406 =
																						CAR(BgL_cdrzd2142zd2_1415);
																					{	/* Eval/evaluate.scm 321 */
																						obj_t BgL_arg1476z00_1423;

																						{	/* Eval/evaluate.scm 321 */
																							obj_t BgL_arg1477z00_1424;
																							obj_t BgL_arg1478z00_1425;

																							if (SYMBOLP(BgL_procnamez00_59))
																								{	/* Eval/evaluate.scm 321 */
																									obj_t BgL_arg2648z00_3512;

																									BgL_arg2648z00_3512 =
																										SYMBOL_TO_STRING
																										(BgL_procnamez00_59);
																									BgL_arg1477z00_1424 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg2648z00_3512);
																								}
																							else
																								{	/* Eval/evaluate.scm 321 */
																									BgL_arg1477z00_1424 = BFALSE;
																								}
																							{	/* Eval/evaluate.scm 322 */
																								obj_t BgL_arg1480z00_1427;
																								obj_t BgL_arg1481z00_1428;

																								{	/* Eval/evaluate.scm 322 */
																									obj_t BgL_arg2648z00_3514;

																									BgL_arg2648z00_3514 =
																										SYMBOL_TO_STRING(
																										((obj_t) BgL_tnamez00_57));
																									BgL_arg1480z00_1427 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg2648z00_3514);
																								}
																								{	/* Eval/evaluate.scm 320 */
																									obj_t BgL_arg1482z00_1429;

																									{	/* Eval/evaluate.scm 320 */
																										obj_t BgL_arg1483z00_1430;

																										BgL_arg1483z00_1430 =
																											MAKE_YOUNG_PAIR
																											(BgL_posz00_1406, BNIL);
																										BgL_arg1482z00_1429 =
																											MAKE_YOUNG_PAIR
																											(BgL_fnamez00_1405,
																											BgL_arg1483z00_1430);
																									}
																									BgL_arg1481z00_1428 =
																										MAKE_YOUNG_PAIR
																										(BgL_varz00_56,
																										BgL_arg1482z00_1429);
																								}
																								BgL_arg1478z00_1425 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1480z00_1427,
																									BgL_arg1481z00_1428);
																							}
																							BgL_arg1476z00_1423 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1477z00_1424,
																								BgL_arg1478z00_1425);
																						}
																						BgL_arg1461z00_1404 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2972z00zz__evaluatez00,
																							BgL_arg1476z00_1423);
																					}
																				}
																			else
																				{	/* Eval/evaluate.scm 316 */
																				BgL_tagzd2131zd2_1408:
																					{	/* Eval/evaluate.scm 326 */
																						obj_t BgL_arg1484z00_1431;

																						{	/* Eval/evaluate.scm 326 */
																							obj_t BgL_arg1485z00_1432;
																							obj_t BgL_arg1486z00_1433;

																							if (SYMBOLP(BgL_procnamez00_59))
																								{	/* Eval/evaluate.scm 326 */
																									obj_t BgL_arg2648z00_3516;

																									BgL_arg2648z00_3516 =
																										SYMBOL_TO_STRING
																										(BgL_procnamez00_59);
																									BgL_arg1485z00_1432 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg2648z00_3516);
																								}
																							else
																								{	/* Eval/evaluate.scm 326 */
																									BgL_arg1485z00_1432 = BFALSE;
																								}
																							{	/* Eval/evaluate.scm 327 */
																								obj_t BgL_arg1488z00_1435;
																								obj_t BgL_arg1489z00_1436;

																								{	/* Eval/evaluate.scm 327 */
																									obj_t BgL_arg2648z00_3518;

																									BgL_arg2648z00_3518 =
																										SYMBOL_TO_STRING(
																										((obj_t) BgL_tnamez00_57));
																									BgL_arg1488z00_1435 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg2648z00_3518);
																								}
																								BgL_arg1489z00_1436 =
																									MAKE_YOUNG_PAIR(BgL_varz00_56,
																									BNIL);
																								BgL_arg1486z00_1433 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1488z00_1435,
																									BgL_arg1489z00_1436);
																							}
																							BgL_arg1484z00_1431 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1485z00_1432,
																								BgL_arg1486z00_1433);
																						}
																						BgL_arg1461z00_1404 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2974z00zz__evaluatez00,
																							BgL_arg1484z00_1431);
																					}
																				}
																		}
																	else
																		{	/* Eval/evaluate.scm 316 */
																			goto BgL_tagzd2131zd2_1408;
																		}
																}
															else
																{	/* Eval/evaluate.scm 316 */
																	goto BgL_tagzd2131zd2_1408;
																}
														}
													else
														{	/* Eval/evaluate.scm 316 */
															goto BgL_tagzd2131zd2_1408;
														}
												}
											else
												{	/* Eval/evaluate.scm 316 */
													goto BgL_tagzd2131zd2_1408;
												}
										}
										BgL_arg1460z00_1403 =
											MAKE_YOUNG_PAIR(BgL_arg1461z00_1404, BNIL);
									}
									BgL_arg1458z00_1401 =
										MAKE_YOUNG_PAIR(BgL_bodyz00_60, BgL_arg1460z00_1403);
								}
								BgL_arg1456z00_1399 =
									MAKE_YOUNG_PAIR(BgL_arg1457z00_1400, BgL_arg1458z00_1401);
							}
							BgL_arg1455z00_1398 =
								MAKE_YOUNG_PAIR(BGl_symbol2966z00zz__evaluatez00,
								BgL_arg1456z00_1399);
						}
						{	/* Eval/evaluate.scm 293 */
							obj_t BgL_arg1452z00_3525;
							obj_t BgL_arg1453z00_3526;

							BgL_arg1452z00_3525 = CAR(BgL_arg1455z00_1398);
							BgL_arg1453z00_3526 = CDR(BgL_arg1455z00_1398);
							{	/* Eval/evaluate.scm 293 */
								obj_t BgL_res2884z00_3529;

								BgL_res2884z00_3529 =
									MAKE_YOUNG_EPAIR(BgL_arg1452z00_3525, BgL_arg1453z00_3526,
									BgL_locz00_58);
								return BgL_res2884z00_3529;
							}
						}
					}
				}
			else
				{	/* Eval/evaluate.scm 300 */
					return BgL_bodyz00_60;
				}
		}

	}



/* type-checks */
	obj_t BGl_typezd2checkszd2zz__evaluatez00(obj_t BgL_varsz00_61,
		obj_t BgL_srcsz00_62, obj_t BgL_bodyz00_63, obj_t BgL_locz00_64,
		obj_t BgL_procnamez00_65)
	{
		{	/* Eval/evaluate.scm 334 */
			{	/* Eval/evaluate.scm 335 */
				bool_t BgL_test3161z00_5509;

				{	/* Eval/evaluate.scm 335 */
					int BgL_arg1539z00_1492;

					BgL_arg1539z00_1492 = bgl_debug();
					BgL_test3161z00_5509 = ((long) (BgL_arg1539z00_1492) <= 0L);
				}
				if (BgL_test3161z00_5509)
					{	/* Eval/evaluate.scm 335 */
						return BgL_bodyz00_63;
					}
				else
					{	/* Eval/evaluate.scm 335 */
						BGL_TAIL return
							BGl_loopze71ze7zz__evaluatez00(BgL_bodyz00_63, BgL_procnamez00_65,
							BgL_locz00_64, BgL_varsz00_61, BgL_srcsz00_62);
					}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__evaluatez00(obj_t BgL_bodyz00_4896,
		obj_t BgL_procnamez00_4895, obj_t BgL_locz00_4894, obj_t BgL_varsz00_1477,
		obj_t BgL_srcsz00_1478)
	{
		{	/* Eval/evaluate.scm 337 */
		BGl_loopze71ze7zz__evaluatez00:
			if (NULLP(BgL_varsz00_1477))
				{	/* Eval/evaluate.scm 339 */
					return BgL_bodyz00_4896;
				}
			else
				{	/* Eval/evaluate.scm 341 */
					obj_t BgL_vz00_1481;

					BgL_vz00_1481 = CAR(((obj_t) BgL_varsz00_1477));
					{	/* Eval/evaluate.scm 341 */
						obj_t BgL_idz00_1482;

						BgL_idz00_1482 = CAR(((obj_t) BgL_vz00_1481));
						{	/* Eval/evaluate.scm 342 */
							obj_t BgL_tnamez00_1483;

							BgL_tnamez00_1483 = CDR(((obj_t) BgL_vz00_1481));
							{	/* Eval/evaluate.scm 343 */

								if (CBOOL(BgL_tnamez00_1483))
									{	/* Eval/evaluate.scm 345 */
										obj_t BgL_locz00_1484;

										{	/* Eval/evaluate.scm 345 */
											obj_t BgL_arg1535z00_1488;

											BgL_arg1535z00_1488 = CAR(((obj_t) BgL_srcsz00_1478));
											{	/* Eval/evaluate.scm 150 */
												obj_t BgL__ortest_1070z00_3535;

												BgL__ortest_1070z00_3535 =
													BGl_getzd2sourcezd2locationz00zz__readerz00
													(BgL_arg1535z00_1488);
												if (CBOOL(BgL__ortest_1070z00_3535))
													{	/* Eval/evaluate.scm 150 */
														BgL_locz00_1484 = BgL__ortest_1070z00_3535;
													}
												else
													{	/* Eval/evaluate.scm 151 */
														obj_t BgL__ortest_1071z00_3536;

														BgL__ortest_1071z00_3536 =
															BGl_getzd2sourcezd2locationz00zz__readerz00
															(BgL_srcsz00_1478);
														if (CBOOL(BgL__ortest_1071z00_3536))
															{	/* Eval/evaluate.scm 151 */
																BgL_locz00_1484 = BgL__ortest_1071z00_3536;
															}
														else
															{	/* Eval/evaluate.scm 151 */
																BgL_locz00_1484 = BgL_locz00_4894;
															}
													}
											}
										}
										{	/* Eval/evaluate.scm 347 */
											obj_t BgL_arg1529z00_1485;

											{	/* Eval/evaluate.scm 347 */
												obj_t BgL_arg1530z00_1486;
												obj_t BgL_arg1531z00_1487;

												BgL_arg1530z00_1486 = CDR(((obj_t) BgL_varsz00_1477));
												BgL_arg1531z00_1487 = CDR(((obj_t) BgL_srcsz00_1478));
												BgL_arg1529z00_1485 =
													BGl_loopze71ze7zz__evaluatez00(BgL_bodyz00_4896,
													BgL_procnamez00_4895, BgL_locz00_4894,
													BgL_arg1530z00_1486, BgL_arg1531z00_1487);
											}
											BGL_TAIL return
												BGl_typezd2checkzd2zz__evaluatez00(BgL_idz00_1482,
												BgL_tnamez00_1483, BgL_locz00_1484,
												BgL_procnamez00_4895, BgL_arg1529z00_1485);
										}
									}
								else
									{	/* Eval/evaluate.scm 348 */
										obj_t BgL_arg1536z00_1489;
										obj_t BgL_arg1537z00_1490;

										BgL_arg1536z00_1489 = CDR(((obj_t) BgL_varsz00_1477));
										BgL_arg1537z00_1490 = CDR(((obj_t) BgL_srcsz00_1478));
										{
											obj_t BgL_srcsz00_5543;
											obj_t BgL_varsz00_5542;

											BgL_varsz00_5542 = BgL_arg1536z00_1489;
											BgL_srcsz00_5543 = BgL_arg1537z00_1490;
											BgL_srcsz00_1478 = BgL_srcsz00_5543;
											BgL_varsz00_1477 = BgL_varsz00_5542;
											goto BGl_loopze71ze7zz__evaluatez00;
										}
									}
							}
						}
					}
				}
		}

	}



/* type-result */
	obj_t BGl_typezd2resultzd2zz__evaluatez00(obj_t BgL_typez00_66,
		obj_t BgL_bodyz00_67, obj_t BgL_locz00_68)
	{
		{	/* Eval/evaluate.scm 353 */
			{	/* Eval/evaluate.scm 354 */
				bool_t BgL_test3166z00_5544;

				if (CBOOL(BgL_typez00_66))
					{	/* Eval/evaluate.scm 354 */
						int BgL_arg1558z00_1507;

						BgL_arg1558z00_1507 = bgl_debug();
						BgL_test3166z00_5544 = ((long) (BgL_arg1558z00_1507) >= 1L);
					}
				else
					{	/* Eval/evaluate.scm 354 */
						BgL_test3166z00_5544 = ((bool_t) 0);
					}
				if (BgL_test3166z00_5544)
					{	/* Eval/evaluate.scm 355 */
						obj_t BgL_tmpz00_1495;

						BgL_tmpz00_1495 =
							BGl_gensymz00zz__r4_symbols_6_4z00
							(BGl_symbol2978z00zz__evaluatez00);
						{	/* Eval/evaluate.scm 357 */
							obj_t BgL_arg1543z00_1496;
							obj_t BgL_arg1544z00_1497;

							{	/* Eval/evaluate.scm 357 */
								obj_t BgL_arg1546z00_1498;

								{	/* Eval/evaluate.scm 357 */
									obj_t BgL_arg1547z00_1499;
									obj_t BgL_arg1549z00_1500;

									{	/* Eval/evaluate.scm 357 */
										obj_t BgL_arg1552z00_1501;

										{	/* Eval/evaluate.scm 357 */
											obj_t BgL_arg1553z00_1502;
											obj_t BgL_arg1554z00_1503;

											{	/* Eval/evaluate.scm 357 */
												obj_t BgL_arg1555z00_1504;

												{	/* Eval/evaluate.scm 357 */
													obj_t BgL_list1556z00_1505;

													{	/* Eval/evaluate.scm 357 */
														obj_t BgL_arg1557z00_1506;

														BgL_arg1557z00_1506 =
															MAKE_YOUNG_PAIR(BgL_typez00_66, BNIL);
														BgL_list1556z00_1505 =
															MAKE_YOUNG_PAIR(BgL_tmpz00_1495,
															BgL_arg1557z00_1506);
													}
													BgL_arg1555z00_1504 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string2980z00zz__evaluatez00,
														BgL_list1556z00_1505);
												}
												BgL_arg1553z00_1502 =
													bstring_to_symbol(BgL_arg1555z00_1504);
											}
											BgL_arg1554z00_1503 =
												MAKE_YOUNG_PAIR(BgL_bodyz00_67, BNIL);
											BgL_arg1552z00_1501 =
												MAKE_YOUNG_PAIR(BgL_arg1553z00_1502,
												BgL_arg1554z00_1503);
										}
										BgL_arg1547z00_1499 =
											MAKE_YOUNG_PAIR(BgL_arg1552z00_1501, BNIL);
									}
									BgL_arg1549z00_1500 = MAKE_YOUNG_PAIR(BgL_tmpz00_1495, BNIL);
									BgL_arg1546z00_1498 =
										MAKE_YOUNG_PAIR(BgL_arg1547z00_1499, BgL_arg1549z00_1500);
								}
								BgL_arg1543z00_1496 =
									MAKE_YOUNG_PAIR(BGl_symbol2968z00zz__evaluatez00,
									BgL_arg1546z00_1498);
							}
							{	/* Eval/evaluate.scm 144 */
								obj_t BgL__ortest_1069z00_3543;

								BgL__ortest_1069z00_3543 =
									BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_bodyz00_67);
								if (CBOOL(BgL__ortest_1069z00_3543))
									{	/* Eval/evaluate.scm 144 */
										BgL_arg1544z00_1497 = BgL__ortest_1069z00_3543;
									}
								else
									{	/* Eval/evaluate.scm 144 */
										BgL_arg1544z00_1497 = BgL_locz00_68;
									}
							}
							{	/* Eval/evaluate.scm 293 */
								obj_t BgL_arg1452z00_3544;
								obj_t BgL_arg1453z00_3545;

								BgL_arg1452z00_3544 = CAR(BgL_arg1543z00_1496);
								BgL_arg1453z00_3545 = CDR(BgL_arg1543z00_1496);
								{	/* Eval/evaluate.scm 293 */
									obj_t BgL_res2885z00_3548;

									BgL_res2885z00_3548 =
										MAKE_YOUNG_EPAIR(BgL_arg1452z00_3544, BgL_arg1453z00_3545,
										BgL_arg1544z00_1497);
									return BgL_res2885z00_3548;
								}
							}
						}
					}
				else
					{	/* Eval/evaluate.scm 354 */
						return BgL_bodyz00_67;
					}
			}
		}

	}



/* conv */
	obj_t BGl_convz00zz__evaluatez00(obj_t BgL_ez00_69, obj_t BgL_localsz00_70,
		obj_t BgL_globalsz00_71, obj_t BgL_tailzf3zf3_72, obj_t BgL_wherez00_73,
		obj_t BgL_locz00_74, bool_t BgL_topzf3zf3_75)
	{
		{	/* Eval/evaluate.scm 365 */
		BGl_convz00zz__evaluatez00:
			{
				obj_t BgL_xz00_1516;
				obj_t BgL_bahz00_1518;
				obj_t BgL_lz00_1523;
				obj_t BgL_funz00_1525;
				obj_t BgL_argsz00_1526;
				obj_t BgL_pz00_1532;
				obj_t BgL_tz00_1533;
				obj_t BgL_oz00_1534;
				obj_t BgL_pz00_1536;
				obj_t BgL_tz00_1537;
				obj_t BgL_bindsz00_1545;
				obj_t BgL_bodyz00_1546;
				obj_t BgL_bindsz00_1548;
				obj_t BgL_bodyz00_1549;
				obj_t BgL_bindsz00_1551;
				obj_t BgL_bodyz00_1552;
				obj_t BgL_lz00_1558;
				obj_t BgL_e2z00_1559;
				obj_t BgL_vz00_1561;
				obj_t BgL_ez00_1562;
				obj_t BgL_gvz00_1565;
				obj_t BgL_formalsz00_1566;
				obj_t BgL_bodyz00_1567;
				obj_t BgL_gvz00_1569;
				obj_t BgL_gez00_1570;
				obj_t BgL_vz00_1572;
				obj_t BgL_bodyz00_1573;
				obj_t BgL_mz00_1585;
				obj_t BgL_bodyz00_1586;
				obj_t BgL_fz00_1592;
				obj_t BgL_argsz00_1593;

				if (PAIRP(BgL_ez00_69))
					{	/* Eval/evaluate.scm 424 */
						if (
							(CAR(((obj_t) BgL_ez00_69)) == BGl_symbol2987z00zz__evaluatez00))
							{	/* Eval/evaluate.scm 424 */
								obj_t BgL_arg1564z00_1600;

								BgL_arg1564z00_1600 = CDR(((obj_t) BgL_ez00_69));
								BgL_bahz00_1518 = BgL_arg1564z00_1600;
								if (BgL_topzf3zf3_75)
									{	/* Eval/evaluate.scm 432 */
										obj_t BgL_formsz00_2383;

										{	/* Eval/evaluate.scm 432 */
											obj_t BgL_arg2277z00_2386;

											{	/* Eval/evaluate.scm 144 */
												obj_t BgL__ortest_1069z00_3585;

												BgL__ortest_1069z00_3585 =
													BGl_getzd2sourcezd2locationz00zz__readerz00
													(BgL_ez00_69);
												if (CBOOL(BgL__ortest_1069z00_3585))
													{	/* Eval/evaluate.scm 144 */
														BgL_arg2277z00_2386 = BgL__ortest_1069z00_3585;
													}
												else
													{	/* Eval/evaluate.scm 144 */
														BgL_arg2277z00_2386 = BgL_locz00_74;
													}
											}
											BgL_formsz00_2383 =
												BGl_evmodulez00zz__evmodulez00(BgL_ez00_69,
												BgL_arg2277z00_2386);
										}
										{	/* Eval/evaluate.scm 433 */
											obj_t BgL_arg2275z00_2384;
											obj_t BgL_arg2276z00_2385;

											BgL_arg2275z00_2384 =
												BGl_expandz00zz__expandz00(BgL_formsz00_2383);
											BgL_arg2276z00_2385 = BGL_MODULE();
											{
												bool_t BgL_topzf3zf3_5586;
												obj_t BgL_wherez00_5585;
												obj_t BgL_tailzf3zf3_5584;
												obj_t BgL_globalsz00_5583;
												obj_t BgL_ez00_5582;

												BgL_ez00_5582 = BgL_arg2275z00_2384;
												BgL_globalsz00_5583 = BgL_arg2276z00_2385;
												BgL_tailzf3zf3_5584 = BgL_wherez00_73;
												BgL_wherez00_5585 = BFALSE;
												BgL_topzf3zf3_5586 = ((bool_t) 1);
												BgL_topzf3zf3_75 = BgL_topzf3zf3_5586;
												BgL_wherez00_73 = BgL_wherez00_5585;
												BgL_tailzf3zf3_72 = BgL_tailzf3zf3_5584;
												BgL_globalsz00_71 = BgL_globalsz00_5583;
												BgL_ez00_69 = BgL_ez00_5582;
												goto BGl_convz00zz__evaluatez00;
											}
										}
									}
								else
									{	/* Eval/evaluate.scm 431 */
										return
											BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_74,
											BGl_string2906z00zz__evaluatez00,
											BGl_string2986z00zz__evaluatez00, BgL_ez00_69);
									}
							}
						else
							{	/* Eval/evaluate.scm 424 */
								obj_t BgL_cdrzd2250zd2_1601;

								BgL_cdrzd2250zd2_1601 = CDR(((obj_t) BgL_ez00_69));
								if (
									(CAR(
											((obj_t) BgL_ez00_69)) ==
										BGl_symbol2989z00zz__evaluatez00))
									{	/* Eval/evaluate.scm 424 */
										if (PAIRP(BgL_cdrzd2250zd2_1601))
											{	/* Eval/evaluate.scm 424 */
												obj_t BgL_carzd2253zd2_1605;
												obj_t BgL_cdrzd2254zd2_1606;

												BgL_carzd2253zd2_1605 = CAR(BgL_cdrzd2250zd2_1601);
												BgL_cdrzd2254zd2_1606 = CDR(BgL_cdrzd2250zd2_1601);
												if (SYMBOLP(BgL_carzd2253zd2_1605))
													{	/* Eval/evaluate.scm 424 */
														if (PAIRP(BgL_cdrzd2254zd2_1606))
															{	/* Eval/evaluate.scm 424 */
																obj_t BgL_carzd2258zd2_1609;

																BgL_carzd2258zd2_1609 =
																	CAR(BgL_cdrzd2254zd2_1606);
																if (SYMBOLP(BgL_carzd2258zd2_1609))
																	{	/* Eval/evaluate.scm 424 */
																		if (NULLP(CDR(BgL_cdrzd2254zd2_1606)))
																			{	/* Eval/evaluate.scm 437 */
																				BgL_ev_globalz00_bglt
																					BgL_new1108z00_3941;
																				{	/* Eval/evaluate.scm 438 */
																					BgL_ev_globalz00_bglt
																						BgL_new1107z00_3942;
																					BgL_new1107z00_3942 =
																						((BgL_ev_globalz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_ev_globalz00_bgl))));
																					{	/* Eval/evaluate.scm 438 */
																						long BgL_arg2279z00_3943;

																						BgL_arg2279z00_3943 =
																							BGL_CLASS_NUM
																							(BGl_ev_globalz00zz__evaluate_typesz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1107z00_3942),
																							BgL_arg2279z00_3943);
																					}
																					BgL_new1108z00_3941 =
																						BgL_new1107z00_3942;
																				}
																				((((BgL_ev_globalz00_bglt)
																							COBJECT(BgL_new1108z00_3941))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_74), BUNSPEC);
																				((((BgL_ev_globalz00_bglt)
																							COBJECT(BgL_new1108z00_3941))->
																						BgL_namez00) =
																					((obj_t) BgL_carzd2253zd2_1605),
																					BUNSPEC);
																				((((BgL_ev_globalz00_bglt)
																							COBJECT(BgL_new1108z00_3941))->
																						BgL_modz00) =
																					((obj_t)
																						BGl_evalzd2findzd2modulez00zz__evmodulez00
																						(BgL_carzd2258zd2_1609)), BUNSPEC);
																				return ((obj_t) BgL_new1108z00_3941);
																			}
																		else
																			{	/* Eval/evaluate.scm 424 */
																				obj_t BgL_carzd2303zd2_1613;

																				BgL_carzd2303zd2_1613 =
																					CAR(((obj_t) BgL_ez00_69));
																				if (SYMBOLP(BgL_carzd2303zd2_1613))
																					{	/* Eval/evaluate.scm 424 */
																						if (CBOOL
																							(BGl_convzd2varzd2zz__evaluatez00
																								(BgL_carzd2303zd2_1613,
																									BgL_localsz00_70)))
																							{
																								BgL_ev_appz00_bglt
																									BgL_auxz00_5624;
																								BgL_funz00_1525 =
																									BgL_carzd2303zd2_1613;
																								BgL_argsz00_1526 =
																									BgL_cdrzd2250zd2_1601;
																							BgL_tagzd2152zd2_1527:
																								{	/* Eval/evaluate.scm 449 */
																									obj_t BgL_funz00_2416;
																									obj_t BgL_argsz00_2417;

																									{	/* Eval/evaluate.scm 380 */
																										obj_t BgL_arg2446z00_3595;

																										{	/* Eval/evaluate.scm 144 */
																											obj_t
																												BgL__ortest_1069z00_3596;
																											BgL__ortest_1069z00_3596 =
																												BGl_getzd2sourcezd2locationz00zz__readerz00
																												(BgL_funz00_1525);
																											if (CBOOL
																												(BgL__ortest_1069z00_3596))
																												{	/* Eval/evaluate.scm 144 */
																													BgL_arg2446z00_3595 =
																														BgL__ortest_1069z00_3596;
																												}
																											else
																												{	/* Eval/evaluate.scm 144 */
																													BgL_arg2446z00_3595 =
																														BgL_locz00_74;
																												}
																										}
																										BgL_funz00_2416 =
																											BGl_convz00zz__evaluatez00
																											(BgL_funz00_1525,
																											BgL_localsz00_70,
																											BgL_globalsz00_71, BFALSE,
																											BgL_wherez00_73,
																											BgL_arg2446z00_3595,
																											((bool_t) 0));
																									}
																									BgL_argsz00_2417 =
																										BGl_loopze70ze7zz__evaluatez00
																										(BgL_wherez00_73,
																										BgL_globalsz00_71,
																										BgL_localsz00_70,
																										BgL_locz00_74,
																										BgL_argsz00_1526);
																									{	/* Eval/evaluate.scm 450 */
																										BgL_ev_appz00_bglt
																											BgL_new1110z00_2418;
																										{	/* Eval/evaluate.scm 451 */
																											BgL_ev_appz00_bglt
																												BgL_new1109z00_2419;
																											BgL_new1109z00_2419 =
																												((BgL_ev_appz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_ev_appz00_bgl))));
																											{	/* Eval/evaluate.scm 451 */
																												long
																													BgL_arg2294z00_2420;
																												BgL_arg2294z00_2420 =
																													BGL_CLASS_NUM
																													(BGl_ev_appz00zz__evaluate_typesz00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1109z00_2419),
																													BgL_arg2294z00_2420);
																											}
																											BgL_new1110z00_2418 =
																												BgL_new1109z00_2419;
																										}
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1110z00_2418))->
																												BgL_locz00) =
																											((obj_t) BgL_locz00_74),
																											BUNSPEC);
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1110z00_2418))->
																												BgL_funz00) =
																											((BgL_ev_exprz00_bglt) (
																													(BgL_ev_exprz00_bglt)
																													BgL_funz00_2416)),
																											BUNSPEC);
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1110z00_2418))->
																												BgL_argsz00) =
																											((obj_t)
																												BgL_argsz00_2417),
																											BUNSPEC);
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1110z00_2418))->
																												BgL_tailzf3zf3) =
																											((obj_t)
																												BgL_tailzf3zf3_72),
																											BUNSPEC);
																										BgL_auxz00_5624 =
																											BgL_new1110z00_2418;
																								}}
																								return
																									((obj_t) BgL_auxz00_5624);
																							}
																						else
																							{
																								BgL_ev_appz00_bglt
																									BgL_auxz00_5640;
																								BgL_fz00_1592 =
																									BgL_carzd2303zd2_1613;
																								BgL_argsz00_1593 =
																									BgL_cdrzd2250zd2_1601;
																							BgL_tagzd2176zd2_1594:
																								{	/* Eval/evaluate.scm 603 */
																									obj_t BgL_funz00_2688;
																									obj_t BgL_argsz00_2689;

																									{	/* Eval/evaluate.scm 380 */
																										obj_t BgL_arg2446z00_3928;

																										{	/* Eval/evaluate.scm 144 */
																											obj_t
																												BgL__ortest_1069z00_3929;
																											BgL__ortest_1069z00_3929 =
																												BGl_getzd2sourcezd2locationz00zz__readerz00
																												(BgL_fz00_1592);
																											if (CBOOL
																												(BgL__ortest_1069z00_3929))
																												{	/* Eval/evaluate.scm 144 */
																													BgL_arg2446z00_3928 =
																														BgL__ortest_1069z00_3929;
																												}
																											else
																												{	/* Eval/evaluate.scm 144 */
																													BgL_arg2446z00_3928 =
																														BgL_locz00_74;
																												}
																										}
																										BgL_funz00_2688 =
																											BGl_convz00zz__evaluatez00
																											(BgL_fz00_1592,
																											BgL_localsz00_70,
																											BgL_globalsz00_71, BFALSE,
																											BgL_wherez00_73,
																											BgL_arg2446z00_3928,
																											((bool_t) 0));
																									}
																									BgL_argsz00_2689 =
																										BGl_loopze70ze7zz__evaluatez00
																										(BgL_wherez00_73,
																										BgL_globalsz00_71,
																										BgL_localsz00_70,
																										BgL_locz00_74,
																										BgL_argsz00_1593);
																									{	/* Eval/evaluate.scm 604 */
																										BgL_ev_appz00_bglt
																											BgL_new1166z00_2690;
																										{	/* Eval/evaluate.scm 605 */
																											BgL_ev_appz00_bglt
																												BgL_new1165z00_2691;
																											BgL_new1165z00_2691 =
																												((BgL_ev_appz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_ev_appz00_bgl))));
																											{	/* Eval/evaluate.scm 605 */
																												long
																													BgL_arg2438z00_2692;
																												BgL_arg2438z00_2692 =
																													BGL_CLASS_NUM
																													(BGl_ev_appz00zz__evaluate_typesz00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1165z00_2691),
																													BgL_arg2438z00_2692);
																											}
																											BgL_new1166z00_2690 =
																												BgL_new1165z00_2691;
																										}
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1166z00_2690))->
																												BgL_locz00) =
																											((obj_t) BgL_locz00_74),
																											BUNSPEC);
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1166z00_2690))->
																												BgL_funz00) =
																											((BgL_ev_exprz00_bglt) (
																													(BgL_ev_exprz00_bglt)
																													BgL_funz00_2688)),
																											BUNSPEC);
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1166z00_2690))->
																												BgL_argsz00) =
																											((obj_t)
																												BgL_argsz00_2689),
																											BUNSPEC);
																										((((BgL_ev_appz00_bglt)
																													COBJECT
																													(BgL_new1166z00_2690))->
																												BgL_tailzf3zf3) =
																											((obj_t)
																												BgL_tailzf3zf3_72),
																											BUNSPEC);
																										BgL_auxz00_5640 =
																											BgL_new1166z00_2690;
																								}}
																								return
																									((obj_t) BgL_auxz00_5640);
																							}
																					}
																				else
																					{
																						BgL_ev_appz00_bglt BgL_auxz00_5656;

																						{
																							obj_t BgL_argsz00_5658;
																							obj_t BgL_fz00_5657;

																							BgL_fz00_5657 =
																								BgL_carzd2303zd2_1613;
																							BgL_argsz00_5658 =
																								BgL_cdrzd2250zd2_1601;
																							BgL_argsz00_1593 =
																								BgL_argsz00_5658;
																							BgL_fz00_1592 = BgL_fz00_5657;
																							goto BgL_tagzd2176zd2_1594;
																						}
																						return ((obj_t) BgL_auxz00_5656);
																					}
																			}
																	}
																else
																	{	/* Eval/evaluate.scm 424 */
																		obj_t BgL_carzd21010zd2_1624;

																		BgL_carzd21010zd2_1624 =
																			CAR(((obj_t) BgL_ez00_69));
																		if (SYMBOLP(BgL_carzd21010zd2_1624))
																			{	/* Eval/evaluate.scm 424 */
																				if (CBOOL
																					(BGl_convzd2varzd2zz__evaluatez00
																						(BgL_carzd21010zd2_1624,
																							BgL_localsz00_70)))
																					{
																						BgL_ev_appz00_bglt BgL_auxz00_5667;

																						{
																							obj_t BgL_argsz00_5669;
																							obj_t BgL_funz00_5668;

																							BgL_funz00_5668 =
																								BgL_carzd21010zd2_1624;
																							BgL_argsz00_5669 =
																								BgL_cdrzd2250zd2_1601;
																							BgL_argsz00_1526 =
																								BgL_argsz00_5669;
																							BgL_funz00_1525 = BgL_funz00_5668;
																							goto BgL_tagzd2152zd2_1527;
																						}
																						return ((obj_t) BgL_auxz00_5667);
																					}
																				else
																					{
																						BgL_ev_appz00_bglt BgL_auxz00_5671;

																						{
																							obj_t BgL_argsz00_5673;
																							obj_t BgL_fz00_5672;

																							BgL_fz00_5672 =
																								BgL_carzd21010zd2_1624;
																							BgL_argsz00_5673 =
																								BgL_cdrzd2250zd2_1601;
																							BgL_argsz00_1593 =
																								BgL_argsz00_5673;
																							BgL_fz00_1592 = BgL_fz00_5672;
																							goto BgL_tagzd2176zd2_1594;
																						}
																						return ((obj_t) BgL_auxz00_5671);
																					}
																			}
																		else
																			{
																				BgL_ev_appz00_bglt BgL_auxz00_5675;

																				{
																					obj_t BgL_argsz00_5677;
																					obj_t BgL_fz00_5676;

																					BgL_fz00_5676 =
																						BgL_carzd21010zd2_1624;
																					BgL_argsz00_5677 =
																						BgL_cdrzd2250zd2_1601;
																					BgL_argsz00_1593 = BgL_argsz00_5677;
																					BgL_fz00_1592 = BgL_fz00_5676;
																					goto BgL_tagzd2176zd2_1594;
																				}
																				return ((obj_t) BgL_auxz00_5675);
																			}
																	}
															}
														else
															{	/* Eval/evaluate.scm 424 */
																obj_t BgL_carzd21717zd2_1634;

																BgL_carzd21717zd2_1634 =
																	CAR(((obj_t) BgL_ez00_69));
																if (SYMBOLP(BgL_carzd21717zd2_1634))
																	{	/* Eval/evaluate.scm 424 */
																		if (CBOOL(BGl_convzd2varzd2zz__evaluatez00
																				(BgL_carzd21717zd2_1634,
																					BgL_localsz00_70)))
																			{
																				BgL_ev_appz00_bglt BgL_auxz00_5686;

																				{
																					obj_t BgL_argsz00_5688;
																					obj_t BgL_funz00_5687;

																					BgL_funz00_5687 =
																						BgL_carzd21717zd2_1634;
																					BgL_argsz00_5688 =
																						BgL_cdrzd2250zd2_1601;
																					BgL_argsz00_1526 = BgL_argsz00_5688;
																					BgL_funz00_1525 = BgL_funz00_5687;
																					goto BgL_tagzd2152zd2_1527;
																				}
																				return ((obj_t) BgL_auxz00_5686);
																			}
																		else
																			{
																				BgL_ev_appz00_bglt BgL_auxz00_5690;

																				{
																					obj_t BgL_argsz00_5692;
																					obj_t BgL_fz00_5691;

																					BgL_fz00_5691 =
																						BgL_carzd21717zd2_1634;
																					BgL_argsz00_5692 =
																						BgL_cdrzd2250zd2_1601;
																					BgL_argsz00_1593 = BgL_argsz00_5692;
																					BgL_fz00_1592 = BgL_fz00_5691;
																					goto BgL_tagzd2176zd2_1594;
																				}
																				return ((obj_t) BgL_auxz00_5690);
																			}
																	}
																else
																	{
																		BgL_ev_appz00_bglt BgL_auxz00_5694;

																		{
																			obj_t BgL_argsz00_5696;
																			obj_t BgL_fz00_5695;

																			BgL_fz00_5695 = BgL_carzd21717zd2_1634;
																			BgL_argsz00_5696 = BgL_cdrzd2250zd2_1601;
																			BgL_argsz00_1593 = BgL_argsz00_5696;
																			BgL_fz00_1592 = BgL_fz00_5695;
																			goto BgL_tagzd2176zd2_1594;
																		}
																		return ((obj_t) BgL_auxz00_5694);
																	}
															}
													}
												else
													{	/* Eval/evaluate.scm 424 */
														obj_t BgL_carzd22424zd2_1644;

														BgL_carzd22424zd2_1644 = CAR(((obj_t) BgL_ez00_69));
														if (SYMBOLP(BgL_carzd22424zd2_1644))
															{	/* Eval/evaluate.scm 424 */
																if (CBOOL(BGl_convzd2varzd2zz__evaluatez00
																		(BgL_carzd22424zd2_1644, BgL_localsz00_70)))
																	{
																		BgL_ev_appz00_bglt BgL_auxz00_5705;

																		{
																			obj_t BgL_argsz00_5707;
																			obj_t BgL_funz00_5706;

																			BgL_funz00_5706 = BgL_carzd22424zd2_1644;
																			BgL_argsz00_5707 = BgL_cdrzd2250zd2_1601;
																			BgL_argsz00_1526 = BgL_argsz00_5707;
																			BgL_funz00_1525 = BgL_funz00_5706;
																			goto BgL_tagzd2152zd2_1527;
																		}
																		return ((obj_t) BgL_auxz00_5705);
																	}
																else
																	{
																		BgL_ev_appz00_bglt BgL_auxz00_5709;

																		{
																			obj_t BgL_argsz00_5711;
																			obj_t BgL_fz00_5710;

																			BgL_fz00_5710 = BgL_carzd22424zd2_1644;
																			BgL_argsz00_5711 = BgL_cdrzd2250zd2_1601;
																			BgL_argsz00_1593 = BgL_argsz00_5711;
																			BgL_fz00_1592 = BgL_fz00_5710;
																			goto BgL_tagzd2176zd2_1594;
																		}
																		return ((obj_t) BgL_auxz00_5709);
																	}
															}
														else
															{
																BgL_ev_appz00_bglt BgL_auxz00_5713;

																{
																	obj_t BgL_argsz00_5715;
																	obj_t BgL_fz00_5714;

																	BgL_fz00_5714 = BgL_carzd22424zd2_1644;
																	BgL_argsz00_5715 = BgL_cdrzd2250zd2_1601;
																	BgL_argsz00_1593 = BgL_argsz00_5715;
																	BgL_fz00_1592 = BgL_fz00_5714;
																	goto BgL_tagzd2176zd2_1594;
																}
																return ((obj_t) BgL_auxz00_5713);
															}
													}
											}
										else
											{	/* Eval/evaluate.scm 424 */
												obj_t BgL_carzd23131zd2_1654;

												BgL_carzd23131zd2_1654 = CAR(((obj_t) BgL_ez00_69));
												if (SYMBOLP(BgL_carzd23131zd2_1654))
													{	/* Eval/evaluate.scm 424 */
														if (CBOOL(BGl_convzd2varzd2zz__evaluatez00
																(BgL_carzd23131zd2_1654, BgL_localsz00_70)))
															{
																BgL_ev_appz00_bglt BgL_auxz00_5724;

																{
																	obj_t BgL_argsz00_5726;
																	obj_t BgL_funz00_5725;

																	BgL_funz00_5725 = BgL_carzd23131zd2_1654;
																	BgL_argsz00_5726 = BgL_cdrzd2250zd2_1601;
																	BgL_argsz00_1526 = BgL_argsz00_5726;
																	BgL_funz00_1525 = BgL_funz00_5725;
																	goto BgL_tagzd2152zd2_1527;
																}
																return ((obj_t) BgL_auxz00_5724);
															}
														else
															{
																BgL_ev_appz00_bglt BgL_auxz00_5728;

																{
																	obj_t BgL_argsz00_5730;
																	obj_t BgL_fz00_5729;

																	BgL_fz00_5729 = BgL_carzd23131zd2_1654;
																	BgL_argsz00_5730 = BgL_cdrzd2250zd2_1601;
																	BgL_argsz00_1593 = BgL_argsz00_5730;
																	BgL_fz00_1592 = BgL_fz00_5729;
																	goto BgL_tagzd2176zd2_1594;
																}
																return ((obj_t) BgL_auxz00_5728);
															}
													}
												else
													{
														BgL_ev_appz00_bglt BgL_auxz00_5732;

														{
															obj_t BgL_argsz00_5734;
															obj_t BgL_fz00_5733;

															BgL_fz00_5733 = BgL_carzd23131zd2_1654;
															BgL_argsz00_5734 = BgL_cdrzd2250zd2_1601;
															BgL_argsz00_1593 = BgL_argsz00_5734;
															BgL_fz00_1592 = BgL_fz00_5733;
															goto BgL_tagzd2176zd2_1594;
														}
														return ((obj_t) BgL_auxz00_5732);
													}
											}
									}
								else
									{	/* Eval/evaluate.scm 424 */
										if (
											(CAR(
													((obj_t) BgL_ez00_69)) ==
												BGl_symbol2991z00zz__evaluatez00))
											{	/* Eval/evaluate.scm 424 */
												BgL_lz00_1523 = BgL_cdrzd2250zd2_1601;
												{	/* Eval/evaluate.scm 442 */
													bool_t BgL_test3192z00_5740;

													if (PAIRP(BgL_lz00_1523))
														{	/* Eval/evaluate.scm 442 */
															bool_t BgL_test3194z00_5743;

															{	/* Eval/evaluate.scm 442 */
																obj_t BgL_tmpz00_5744;

																BgL_tmpz00_5744 = CDR(BgL_lz00_1523);
																BgL_test3194z00_5743 = PAIRP(BgL_tmpz00_5744);
															}
															if (BgL_test3194z00_5743)
																{
																	obj_t BgL_l1217z00_2407;

																	BgL_l1217z00_2407 = BgL_lz00_1523;
																BgL_zc3z04anonymousza32290ze3z87_2408:
																	if (NULLP(BgL_l1217z00_2407))
																		{	/* Eval/evaluate.scm 442 */
																			BgL_test3192z00_5740 = ((bool_t) 1);
																		}
																	else
																		{	/* Eval/evaluate.scm 442 */
																			bool_t BgL_test3196z00_5749;

																			{	/* Eval/evaluate.scm 442 */
																				obj_t BgL_tmpz00_5750;

																				BgL_tmpz00_5750 =
																					CAR(((obj_t) BgL_l1217z00_2407));
																				BgL_test3196z00_5749 =
																					SYMBOLP(BgL_tmpz00_5750);
																			}
																			if (BgL_test3196z00_5749)
																				{
																					obj_t BgL_l1217z00_5754;

																					BgL_l1217z00_5754 =
																						CDR(((obj_t) BgL_l1217z00_2407));
																					BgL_l1217z00_2407 = BgL_l1217z00_5754;
																					goto
																						BgL_zc3z04anonymousza32290ze3z87_2408;
																				}
																			else
																				{	/* Eval/evaluate.scm 442 */
																					BgL_test3192z00_5740 = ((bool_t) 0);
																				}
																		}
																}
															else
																{	/* Eval/evaluate.scm 442 */
																	BgL_test3192z00_5740 = ((bool_t) 0);
																}
														}
													else
														{	/* Eval/evaluate.scm 442 */
															BgL_test3192z00_5740 = ((bool_t) 0);
														}
													if (BgL_test3192z00_5740)
														{	/* Eval/evaluate.scm 442 */
															BGL_TAIL return
																BGl_convzd2fieldzd2refz00zz__evaluatez00
																(BgL_ez00_69, BgL_localsz00_70,
																BgL_globalsz00_71, BgL_tailzf3zf3_72,
																BgL_wherez00_73, BgL_locz00_74,
																BgL_topzf3zf3_75);
														}
													else
														{	/* Eval/evaluate.scm 442 */
															return
																BGl_evcompilezd2errorzd2zz__evcompilez00
																(BgL_locz00_74,
																BGl_string2906z00zz__evaluatez00,
																BGl_string2981z00zz__evaluatez00, BgL_ez00_69);
														}
												}
											}
										else
											{	/* Eval/evaluate.scm 424 */
												obj_t BgL_carzd23841zd2_1667;

												BgL_carzd23841zd2_1667 = CAR(((obj_t) BgL_ez00_69));
												if (SYMBOLP(BgL_carzd23841zd2_1667))
													{	/* Eval/evaluate.scm 424 */
														if (CBOOL(BGl_convzd2varzd2zz__evaluatez00
																(BgL_carzd23841zd2_1667, BgL_localsz00_70)))
															{
																BgL_ev_appz00_bglt BgL_auxz00_5766;

																{
																	obj_t BgL_argsz00_5768;
																	obj_t BgL_funz00_5767;

																	BgL_funz00_5767 = BgL_carzd23841zd2_1667;
																	BgL_argsz00_5768 = BgL_cdrzd2250zd2_1601;
																	BgL_argsz00_1526 = BgL_argsz00_5768;
																	BgL_funz00_1525 = BgL_funz00_5767;
																	goto BgL_tagzd2152zd2_1527;
																}
																return ((obj_t) BgL_auxz00_5766);
															}
														else
															{	/* Eval/evaluate.scm 424 */
																if (
																	(BgL_carzd23841zd2_1667 ==
																		BGl_symbol2993z00zz__evaluatez00))
																	{	/* Eval/evaluate.scm 424 */
																		if (PAIRP(BgL_cdrzd2250zd2_1601))
																			{	/* Eval/evaluate.scm 424 */
																				if (NULLP(CDR(BgL_cdrzd2250zd2_1601)))
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg1629z00_1678;

																						BgL_arg1629z00_1678 =
																							CAR(BgL_cdrzd2250zd2_1601);
																						{	/* Eval/evaluate.scm 456 */
																							BgL_ev_trapz00_bglt
																								BgL_new1113z00_3985;
																							{	/* Eval/evaluate.scm 457 */
																								BgL_ev_trapz00_bglt
																									BgL_new1111z00_3986;
																								BgL_new1111z00_3986 =
																									((BgL_ev_trapz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_ev_trapz00_bgl))));
																								{	/* Eval/evaluate.scm 457 */
																									long BgL_arg2295z00_3987;

																									BgL_arg2295z00_3987 =
																										BGL_CLASS_NUM
																										(BGl_ev_trapz00zz__evaluate_typesz00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1111z00_3986),
																										BgL_arg2295z00_3987);
																								}
																								BgL_new1113z00_3985 =
																									BgL_new1111z00_3986;
																							}
																							{
																								BgL_ev_exprz00_bglt
																									BgL_auxz00_5782;
																								{	/* Eval/evaluate.scm 380 */
																									obj_t BgL_arg2446z00_3991;

																									BgL_arg2446z00_3991 =
																										BGl_getzd2locationzd2zz__evaluatez00
																										(BgL_arg1629z00_1678,
																										BgL_locz00_74);
																									BgL_auxz00_5782 =
																										((BgL_ev_exprz00_bglt)
																										BGl_uconvzf2locze70z15zz__evaluatez00
																										(BgL_wherez00_73,
																											BgL_globalsz00_71,
																											BgL_localsz00_70,
																											BgL_arg1629z00_1678,
																											BgL_arg2446z00_3991));
																								}
																								((((BgL_ev_hookz00_bglt)
																											COBJECT((
																													(BgL_ev_hookz00_bglt)
																													BgL_new1113z00_3985)))->
																										BgL_ez00) =
																									((BgL_ev_exprz00_bglt)
																										BgL_auxz00_5782), BUNSPEC);
																							}
																							return
																								((obj_t) BgL_new1113z00_3985);
																						}
																					}
																				else
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg1630z00_1679;
																						obj_t BgL_arg1631z00_1680;

																						BgL_arg1630z00_1679 =
																							CAR(((obj_t) BgL_ez00_69));
																						BgL_arg1631z00_1680 =
																							CDR(((obj_t) BgL_ez00_69));
																						{
																							BgL_ev_appz00_bglt
																								BgL_auxz00_5793;
																							{
																								obj_t BgL_argsz00_5795;
																								obj_t BgL_fz00_5794;

																								BgL_fz00_5794 =
																									BgL_arg1630z00_1679;
																								BgL_argsz00_5795 =
																									BgL_arg1631z00_1680;
																								BgL_argsz00_1593 =
																									BgL_argsz00_5795;
																								BgL_fz00_1592 = BgL_fz00_5794;
																								goto BgL_tagzd2176zd2_1594;
																							}
																							return ((obj_t) BgL_auxz00_5793);
																						}
																					}
																			}
																		else
																			{	/* Eval/evaluate.scm 424 */
																				obj_t BgL_arg1636z00_1682;
																				obj_t BgL_arg1637z00_1683;

																				BgL_arg1636z00_1682 =
																					CAR(((obj_t) BgL_ez00_69));
																				BgL_arg1637z00_1683 =
																					CDR(((obj_t) BgL_ez00_69));
																				{
																					BgL_ev_appz00_bglt BgL_auxz00_5801;

																					{
																						obj_t BgL_argsz00_5803;
																						obj_t BgL_fz00_5802;

																						BgL_fz00_5802 = BgL_arg1636z00_1682;
																						BgL_argsz00_5803 =
																							BgL_arg1637z00_1683;
																						BgL_argsz00_1593 = BgL_argsz00_5803;
																						BgL_fz00_1592 = BgL_fz00_5802;
																						goto BgL_tagzd2176zd2_1594;
																					}
																					return ((obj_t) BgL_auxz00_5801);
																				}
																			}
																	}
																else
																	{	/* Eval/evaluate.scm 424 */
																		obj_t BgL_cdrzd24514zd2_1684;

																		BgL_cdrzd24514zd2_1684 =
																			CDR(((obj_t) BgL_ez00_69));
																		if (
																			(CAR(
																					((obj_t) BgL_ez00_69)) ==
																				BGl_symbol2958z00zz__evaluatez00))
																			{	/* Eval/evaluate.scm 424 */
																				if (PAIRP(BgL_cdrzd24514zd2_1684))
																					{	/* Eval/evaluate.scm 424 */
																						if (NULLP(CDR
																								(BgL_cdrzd24514zd2_1684)))
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_arg1643z00_1690;

																								BgL_arg1643z00_1690 =
																									CAR(BgL_cdrzd24514zd2_1684);
																								{	/* Eval/evaluate.scm 459 */
																									BgL_ev_littz00_bglt
																										BgL_new1115z00_4000;
																									{	/* Eval/evaluate.scm 460 */
																										BgL_ev_littz00_bglt
																											BgL_new1114z00_4001;
																										BgL_new1114z00_4001 =
																											((BgL_ev_littz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_ev_littz00_bgl))));
																										{	/* Eval/evaluate.scm 460 */
																											long BgL_arg2296z00_4002;

																											BgL_arg2296z00_4002 =
																												BGL_CLASS_NUM
																												(BGl_ev_littz00zz__evaluate_typesz00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1114z00_4001),
																												BgL_arg2296z00_4002);
																										}
																										BgL_new1115z00_4000 =
																											BgL_new1114z00_4001;
																									}
																									((((BgL_ev_littz00_bglt)
																												COBJECT
																												(BgL_new1115z00_4000))->
																											BgL_valuez00) =
																										((obj_t)
																											BgL_arg1643z00_1690),
																										BUNSPEC);
																									return ((obj_t)
																										BgL_new1115z00_4000);
																								}
																							}
																						else
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_arg1644z00_1691;

																								BgL_arg1644z00_1691 =
																									CAR(((obj_t) BgL_ez00_69));
																								{
																									BgL_ev_appz00_bglt
																										BgL_auxz00_5825;
																									{
																										obj_t BgL_argsz00_5827;
																										obj_t BgL_fz00_5826;

																										BgL_fz00_5826 =
																											BgL_arg1644z00_1691;
																										BgL_argsz00_5827 =
																											BgL_cdrzd24514zd2_1684;
																										BgL_argsz00_1593 =
																											BgL_argsz00_5827;
																										BgL_fz00_1592 =
																											BgL_fz00_5826;
																										goto BgL_tagzd2176zd2_1594;
																									}
																									return
																										((obj_t) BgL_auxz00_5825);
																								}
																							}
																					}
																				else
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg1648z00_1694;

																						BgL_arg1648z00_1694 =
																							CAR(((obj_t) BgL_ez00_69));
																						{
																							BgL_ev_appz00_bglt
																								BgL_auxz00_5831;
																							{
																								obj_t BgL_argsz00_5833;
																								obj_t BgL_fz00_5832;

																								BgL_fz00_5832 =
																									BgL_arg1648z00_1694;
																								BgL_argsz00_5833 =
																									BgL_cdrzd24514zd2_1684;
																								BgL_argsz00_1593 =
																									BgL_argsz00_5833;
																								BgL_fz00_1592 = BgL_fz00_5832;
																								goto BgL_tagzd2176zd2_1594;
																							}
																							return ((obj_t) BgL_auxz00_5831);
																						}
																					}
																			}
																		else
																			{	/* Eval/evaluate.scm 424 */
																				if (
																					(CAR(
																							((obj_t) BgL_ez00_69)) ==
																						BGl_symbol2966z00zz__evaluatez00))
																					{	/* Eval/evaluate.scm 424 */
																						if (PAIRP(BgL_cdrzd24514zd2_1684))
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_cdrzd25128zd2_1700;

																								BgL_cdrzd25128zd2_1700 =
																									CDR(BgL_cdrzd24514zd2_1684);
																								if (PAIRP
																									(BgL_cdrzd25128zd2_1700))
																									{	/* Eval/evaluate.scm 424 */
																										obj_t
																											BgL_cdrzd25133zd2_1702;
																										BgL_cdrzd25133zd2_1702 =
																											CDR
																											(BgL_cdrzd25128zd2_1700);
																										if (PAIRP
																											(BgL_cdrzd25133zd2_1702))
																											{	/* Eval/evaluate.scm 424 */
																												if (NULLP(CDR
																														(BgL_cdrzd25133zd2_1702)))
																													{	/* Eval/evaluate.scm 424 */
																														obj_t
																															BgL_arg1658z00_1706;
																														obj_t
																															BgL_arg1661z00_1707;
																														obj_t
																															BgL_arg1663z00_1708;
																														BgL_arg1658z00_1706
																															=
																															CAR
																															(BgL_cdrzd24514zd2_1684);
																														BgL_arg1661z00_1707
																															=
																															CAR
																															(BgL_cdrzd25128zd2_1700);
																														BgL_arg1663z00_1708
																															=
																															CAR
																															(BgL_cdrzd25133zd2_1702);
																														{
																															BgL_ev_ifz00_bglt
																																BgL_auxz00_5853;
																															BgL_pz00_1532 =
																																BgL_arg1658z00_1706;
																															BgL_tz00_1533 =
																																BgL_arg1661z00_1707;
																															BgL_oz00_1534 =
																																BgL_arg1663z00_1708;
																														BgL_tagzd2155zd2_1535:
																															{	/* Eval/evaluate.scm 462 */
																																BgL_ev_ifz00_bglt
																																	BgL_new1118z00_2427;
																																{	/* Eval/evaluate.scm 463 */
																																	BgL_ev_ifz00_bglt
																																		BgL_new1116z00_2431;
																																	BgL_new1116z00_2431
																																		=
																																		(
																																		(BgL_ev_ifz00_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_ev_ifz00_bgl))));
																																	{	/* Eval/evaluate.scm 463 */
																																		long
																																			BgL_arg2301z00_2432;
																																		BgL_arg2301z00_2432
																																			=
																																			BGL_CLASS_NUM
																																			(BGl_ev_ifz00zz__evaluate_typesz00);
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1116z00_2431), BgL_arg2301z00_2432);
																																	}
																																	BgL_new1118z00_2427
																																		=
																																		BgL_new1116z00_2431;
																																}
																																{
																																	BgL_ev_exprz00_bglt
																																		BgL_auxz00_5858;
																																	{	/* Eval/evaluate.scm 463 */
																																		obj_t
																																			BgL_arg2297z00_2428;
																																		{	/* Eval/evaluate.scm 144 */
																																			obj_t
																																				BgL__ortest_1069z00_3617;
																																			BgL__ortest_1069z00_3617
																																				=
																																				BGl_getzd2sourcezd2locationz00zz__readerz00
																																				(BgL_pz00_1532);
																																			if (CBOOL
																																				(BgL__ortest_1069z00_3617))
																																				{	/* Eval/evaluate.scm 144 */
																																					BgL_arg2297z00_2428
																																						=
																																						BgL__ortest_1069z00_3617;
																																				}
																																			else
																																				{	/* Eval/evaluate.scm 144 */
																																					BgL_arg2297z00_2428
																																						=
																																						BgL_locz00_74;
																																				}
																																		}
																																		BgL_auxz00_5858
																																			=
																																			(
																																			(BgL_ev_exprz00_bglt)
																																			BGl_convz00zz__evaluatez00
																																			(BgL_pz00_1532,
																																				BgL_localsz00_70,
																																				BgL_globalsz00_71,
																																				BFALSE,
																																				BgL_wherez00_73,
																																				BgL_arg2297z00_2428,
																																				((bool_t) 0)));
																																	}
																																	((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1118z00_2427))->BgL_pz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5858), BUNSPEC);
																																}
																																{
																																	BgL_ev_exprz00_bglt
																																		BgL_auxz00_5865;
																																	{	/* Eval/evaluate.scm 464 */
																																		obj_t
																																			BgL_arg2298z00_2429;
																																		{	/* Eval/evaluate.scm 144 */
																																			obj_t
																																				BgL__ortest_1069z00_3618;
																																			BgL__ortest_1069z00_3618
																																				=
																																				BGl_getzd2sourcezd2locationz00zz__readerz00
																																				(BgL_tz00_1533);
																																			if (CBOOL
																																				(BgL__ortest_1069z00_3618))
																																				{	/* Eval/evaluate.scm 144 */
																																					BgL_arg2298z00_2429
																																						=
																																						BgL__ortest_1069z00_3618;
																																				}
																																			else
																																				{	/* Eval/evaluate.scm 144 */
																																					BgL_arg2298z00_2429
																																						=
																																						BgL_locz00_74;
																																				}
																																		}
																																		BgL_auxz00_5865
																																			=
																																			(
																																			(BgL_ev_exprz00_bglt)
																																			BGl_convz00zz__evaluatez00
																																			(BgL_tz00_1533,
																																				BgL_localsz00_70,
																																				BgL_globalsz00_71,
																																				BgL_tailzf3zf3_72,
																																				BgL_wherez00_73,
																																				BgL_arg2298z00_2429,
																																				((bool_t) 0)));
																																	}
																																	((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1118z00_2427))->BgL_tz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5865), BUNSPEC);
																																}
																																{
																																	BgL_ev_exprz00_bglt
																																		BgL_auxz00_5872;
																																	{	/* Eval/evaluate.scm 465 */
																																		obj_t
																																			BgL_arg2299z00_2430;
																																		{	/* Eval/evaluate.scm 144 */
																																			obj_t
																																				BgL__ortest_1069z00_3619;
																																			BgL__ortest_1069z00_3619
																																				=
																																				BGl_getzd2sourcezd2locationz00zz__readerz00
																																				(BgL_oz00_1534);
																																			if (CBOOL
																																				(BgL__ortest_1069z00_3619))
																																				{	/* Eval/evaluate.scm 144 */
																																					BgL_arg2299z00_2430
																																						=
																																						BgL__ortest_1069z00_3619;
																																				}
																																			else
																																				{	/* Eval/evaluate.scm 144 */
																																					BgL_arg2299z00_2430
																																						=
																																						BgL_locz00_74;
																																				}
																																		}
																																		BgL_auxz00_5872
																																			=
																																			(
																																			(BgL_ev_exprz00_bglt)
																																			BGl_convz00zz__evaluatez00
																																			(BgL_oz00_1534,
																																				BgL_localsz00_70,
																																				BgL_globalsz00_71,
																																				BgL_tailzf3zf3_72,
																																				BgL_wherez00_73,
																																				BgL_arg2299z00_2430,
																																				((bool_t) 0)));
																																	}
																																	((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1118z00_2427))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5872), BUNSPEC);
																																}
																																BgL_auxz00_5853
																																	=
																																	BgL_new1118z00_2427;
																															}
																															return
																																((obj_t)
																																BgL_auxz00_5853);
																														}
																													}
																												else
																													{	/* Eval/evaluate.scm 424 */
																														obj_t
																															BgL_arg1664z00_1709;
																														obj_t
																															BgL_arg1667z00_1710;
																														BgL_arg1664z00_1709
																															=
																															CAR(((obj_t)
																																BgL_ez00_69));
																														BgL_arg1667z00_1710
																															=
																															CDR(((obj_t)
																																BgL_ez00_69));
																														{
																															BgL_ev_appz00_bglt
																																BgL_auxz00_5884;
																															{
																																obj_t
																																	BgL_argsz00_5886;
																																obj_t
																																	BgL_fz00_5885;
																																BgL_fz00_5885 =
																																	BgL_arg1664z00_1709;
																																BgL_argsz00_5886
																																	=
																																	BgL_arg1667z00_1710;
																																BgL_argsz00_1593
																																	=
																																	BgL_argsz00_5886;
																																BgL_fz00_1592 =
																																	BgL_fz00_5885;
																																goto
																																	BgL_tagzd2176zd2_1594;
																															}
																															return
																																((obj_t)
																																BgL_auxz00_5884);
																														}
																													}
																											}
																										else
																											{	/* Eval/evaluate.scm 424 */
																												obj_t
																													BgL_cdrzd25433zd2_1712;
																												BgL_cdrzd25433zd2_1712 =
																													CDR(((obj_t)
																														BgL_ez00_69));
																												{	/* Eval/evaluate.scm 424 */
																													obj_t
																														BgL_cdrzd25439zd2_1713;
																													BgL_cdrzd25439zd2_1713
																														=
																														CDR(((obj_t)
																															BgL_cdrzd25433zd2_1712));
																													if (NULLP(CDR(((obj_t)
																																	BgL_cdrzd25439zd2_1713))))
																														{	/* Eval/evaluate.scm 424 */
																															obj_t
																																BgL_arg1675z00_1716;
																															obj_t
																																BgL_arg1676z00_1717;
																															BgL_arg1675z00_1716
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd25433zd2_1712));
																															BgL_arg1676z00_1717
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd25439zd2_1713));
																															{
																																BgL_ev_ifz00_bglt
																																	BgL_auxz00_5900;
																																BgL_pz00_1536 =
																																	BgL_arg1675z00_1716;
																																BgL_tz00_1537 =
																																	BgL_arg1676z00_1717;
																															BgL_tagzd2156zd2_1538:
																																{	/* Eval/evaluate.scm 467 */
																																	BgL_ev_ifz00_bglt
																																		BgL_new1120z00_2433;
																																	{	/* Eval/evaluate.scm 468 */
																																		BgL_ev_ifz00_bglt
																																			BgL_new1119z00_2437;
																																		BgL_new1119z00_2437
																																			=
																																			(
																																			(BgL_ev_ifz00_bglt)
																																			BOBJECT
																																			(GC_MALLOC
																																				(sizeof
																																					(struct
																																						BgL_ev_ifz00_bgl))));
																																		{	/* Eval/evaluate.scm 468 */
																																			long
																																				BgL_arg2306z00_2438;
																																			BgL_arg2306z00_2438
																																				=
																																				BGL_CLASS_NUM
																																				(BGl_ev_ifz00zz__evaluate_typesz00);
																																			BGL_OBJECT_CLASS_NUM_SET
																																				(((BgL_objectz00_bglt) BgL_new1119z00_2437), BgL_arg2306z00_2438);
																																		}
																																		BgL_new1120z00_2433
																																			=
																																			BgL_new1119z00_2437;
																																	}
																																	{
																																		BgL_ev_exprz00_bglt
																																			BgL_auxz00_5905;
																																		{	/* Eval/evaluate.scm 468 */
																																			obj_t
																																				BgL_arg2302z00_2434;
																																			{	/* Eval/evaluate.scm 144 */
																																				obj_t
																																					BgL__ortest_1069z00_3623;
																																				BgL__ortest_1069z00_3623
																																					=
																																					BGl_getzd2sourcezd2locationz00zz__readerz00
																																					(BgL_pz00_1536);
																																				if (CBOOL(BgL__ortest_1069z00_3623))
																																					{	/* Eval/evaluate.scm 144 */
																																						BgL_arg2302z00_2434
																																							=
																																							BgL__ortest_1069z00_3623;
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 144 */
																																						BgL_arg2302z00_2434
																																							=
																																							BgL_locz00_74;
																																					}
																																			}
																																			BgL_auxz00_5905
																																				=
																																				(
																																				(BgL_ev_exprz00_bglt)
																																				BGl_convz00zz__evaluatez00
																																				(BgL_pz00_1536,
																																					BgL_localsz00_70,
																																					BgL_globalsz00_71,
																																					BFALSE,
																																					BgL_wherez00_73,
																																					BgL_arg2302z00_2434,
																																					((bool_t) 0)));
																																		}
																																		((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1120z00_2433))->BgL_pz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5905), BUNSPEC);
																																	}
																																	{
																																		BgL_ev_exprz00_bglt
																																			BgL_auxz00_5912;
																																		{	/* Eval/evaluate.scm 469 */
																																			obj_t
																																				BgL_arg2304z00_2435;
																																			{	/* Eval/evaluate.scm 144 */
																																				obj_t
																																					BgL__ortest_1069z00_3624;
																																				BgL__ortest_1069z00_3624
																																					=
																																					BGl_getzd2sourcezd2locationz00zz__readerz00
																																					(BgL_tz00_1537);
																																				if (CBOOL(BgL__ortest_1069z00_3624))
																																					{	/* Eval/evaluate.scm 144 */
																																						BgL_arg2304z00_2435
																																							=
																																							BgL__ortest_1069z00_3624;
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 144 */
																																						BgL_arg2304z00_2435
																																							=
																																							BgL_locz00_74;
																																					}
																																			}
																																			BgL_auxz00_5912
																																				=
																																				(
																																				(BgL_ev_exprz00_bglt)
																																				BGl_convz00zz__evaluatez00
																																				(BgL_tz00_1537,
																																					BgL_localsz00_70,
																																					BgL_globalsz00_71,
																																					BgL_tailzf3zf3_72,
																																					BgL_wherez00_73,
																																					BgL_arg2304z00_2435,
																																					((bool_t) 0)));
																																		}
																																		((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1120z00_2433))->BgL_tz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5912), BUNSPEC);
																																	}
																																	{
																																		BgL_ev_exprz00_bglt
																																			BgL_auxz00_5919;
																																		{	/* Eval/evaluate.scm 470 */
																																			obj_t
																																				BgL_arg2305z00_2436;
																																			{	/* Eval/evaluate.scm 144 */
																																				obj_t
																																					BgL__ortest_1069z00_3625;
																																				BgL__ortest_1069z00_3625
																																					=
																																					BGl_getzd2sourcezd2locationz00zz__readerz00
																																					(BFALSE);
																																				if (CBOOL(BgL__ortest_1069z00_3625))
																																					{	/* Eval/evaluate.scm 144 */
																																						BgL_arg2305z00_2436
																																							=
																																							BgL__ortest_1069z00_3625;
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 144 */
																																						BgL_arg2305z00_2436
																																							=
																																							BgL_locz00_74;
																																					}
																																			}
																																			BgL_auxz00_5919
																																				=
																																				(
																																				(BgL_ev_exprz00_bglt)
																																				BGl_convz00zz__evaluatez00
																																				(BgL_ez00_69,
																																					BgL_localsz00_70,
																																					BgL_globalsz00_71,
																																					BgL_tailzf3zf3_72,
																																					BgL_wherez00_73,
																																					BgL_arg2305z00_2436,
																																					((bool_t) 0)));
																																		}
																																		((((BgL_ev_ifz00_bglt) COBJECT(BgL_new1120z00_2433))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_5919), BUNSPEC);
																																	}
																																	BgL_auxz00_5900
																																		=
																																		BgL_new1120z00_2433;
																																}
																																return
																																	((obj_t)
																																	BgL_auxz00_5900);
																															}
																														}
																													else
																														{	/* Eval/evaluate.scm 424 */
																															obj_t
																																BgL_arg1678z00_1718;
																															BgL_arg1678z00_1718
																																=
																																CAR(((obj_t)
																																	BgL_ez00_69));
																															{
																																BgL_ev_appz00_bglt
																																	BgL_auxz00_5929;
																																{
																																	obj_t
																																		BgL_argsz00_5931;
																																	obj_t
																																		BgL_fz00_5930;
																																	BgL_fz00_5930
																																		=
																																		BgL_arg1678z00_1718;
																																	BgL_argsz00_5931
																																		=
																																		BgL_cdrzd25433zd2_1712;
																																	BgL_argsz00_1593
																																		=
																																		BgL_argsz00_5931;
																																	BgL_fz00_1592
																																		=
																																		BgL_fz00_5930;
																																	goto
																																		BgL_tagzd2176zd2_1594;
																																}
																																return
																																	((obj_t)
																																	BgL_auxz00_5929);
																															}
																														}
																												}
																											}
																									}
																								else
																									{	/* Eval/evaluate.scm 424 */
																										obj_t BgL_arg1685z00_1721;
																										obj_t BgL_arg1688z00_1722;

																										BgL_arg1685z00_1721 =
																											CAR(
																											((obj_t) BgL_ez00_69));
																										BgL_arg1688z00_1722 =
																											CDR(
																											((obj_t) BgL_ez00_69));
																										{
																											BgL_ev_appz00_bglt
																												BgL_auxz00_5937;
																											{
																												obj_t BgL_argsz00_5939;
																												obj_t BgL_fz00_5938;

																												BgL_fz00_5938 =
																													BgL_arg1685z00_1721;
																												BgL_argsz00_5939 =
																													BgL_arg1688z00_1722;
																												BgL_argsz00_1593 =
																													BgL_argsz00_5939;
																												BgL_fz00_1592 =
																													BgL_fz00_5938;
																												goto
																													BgL_tagzd2176zd2_1594;
																											}
																											return
																												((obj_t)
																												BgL_auxz00_5937);
																										}
																									}
																							}
																						else
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_arg1689z00_1723;
																								obj_t BgL_arg1691z00_1724;

																								BgL_arg1689z00_1723 =
																									CAR(((obj_t) BgL_ez00_69));
																								BgL_arg1691z00_1724 =
																									CDR(((obj_t) BgL_ez00_69));
																								{
																									BgL_ev_appz00_bglt
																										BgL_auxz00_5945;
																									{
																										obj_t BgL_argsz00_5947;
																										obj_t BgL_fz00_5946;

																										BgL_fz00_5946 =
																											BgL_arg1689z00_1723;
																										BgL_argsz00_5947 =
																											BgL_arg1691z00_1724;
																										BgL_argsz00_1593 =
																											BgL_argsz00_5947;
																										BgL_fz00_1592 =
																											BgL_fz00_5946;
																										goto BgL_tagzd2176zd2_1594;
																									}
																									return
																										((obj_t) BgL_auxz00_5945);
																								}
																							}
																					}
																				else
																					{	/* Eval/evaluate.scm 424 */
																						if (
																							(CAR(
																									((obj_t) BgL_ez00_69)) ==
																								BGl_symbol2995z00zz__evaluatez00))
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_arg1699z00_1727;

																								BgL_arg1699z00_1727 =
																									CDR(((obj_t) BgL_ez00_69));
																								{	/* Eval/evaluate.scm 472 */
																									BgL_ev_orz00_bglt
																										BgL_new1123z00_4033;
																									{	/* Eval/evaluate.scm 473 */
																										BgL_ev_orz00_bglt
																											BgL_new1122z00_4034;
																										BgL_new1122z00_4034 =
																											((BgL_ev_orz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_ev_orz00_bgl))));
																										{	/* Eval/evaluate.scm 473 */
																											long BgL_arg2307z00_4035;

																											BgL_arg2307z00_4035 =
																												BGL_CLASS_NUM
																												(BGl_ev_orz00zz__evaluate_typesz00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1122z00_4034),
																												BgL_arg2307z00_4035);
																										}
																										BgL_new1123z00_4033 =
																											BgL_new1122z00_4034;
																									}
																									((((BgL_ev_listz00_bglt)
																												COBJECT((
																														(BgL_ev_listz00_bglt)
																														BgL_new1123z00_4033)))->
																											BgL_argsz00) =
																										((obj_t)
																											BGl_loopze70ze7zz__evaluatez00
																											(BgL_wherez00_73,
																												BgL_globalsz00_71,
																												BgL_localsz00_70,
																												BgL_locz00_74,
																												BgL_arg1699z00_1727)),
																										BUNSPEC);
																									return ((obj_t)
																										BgL_new1123z00_4033);
																								}
																							}
																						else
																							{	/* Eval/evaluate.scm 424 */
																								if (
																									(CAR(
																											((obj_t) BgL_ez00_69)) ==
																										BGl_symbol2997z00zz__evaluatez00))
																									{	/* Eval/evaluate.scm 424 */
																										obj_t BgL_arg1702z00_1730;

																										BgL_arg1702z00_1730 =
																											CDR(
																											((obj_t) BgL_ez00_69));
																										{	/* Eval/evaluate.scm 475 */
																											BgL_ev_andz00_bglt
																												BgL_new1125z00_4041;
																											{	/* Eval/evaluate.scm 476 */
																												BgL_ev_andz00_bglt
																													BgL_new1124z00_4042;
																												BgL_new1124z00_4042 =
																													((BgL_ev_andz00_bglt)
																													BOBJECT(GC_MALLOC
																														(sizeof(struct
																																BgL_ev_andz00_bgl))));
																												{	/* Eval/evaluate.scm 476 */
																													long
																														BgL_arg2308z00_4043;
																													BgL_arg2308z00_4043 =
																														BGL_CLASS_NUM
																														(BGl_ev_andz00zz__evaluate_typesz00);
																													BGL_OBJECT_CLASS_NUM_SET
																														(((BgL_objectz00_bglt) BgL_new1124z00_4042), BgL_arg2308z00_4043);
																												}
																												BgL_new1125z00_4041 =
																													BgL_new1124z00_4042;
																											}
																											((((BgL_ev_listz00_bglt)
																														COBJECT((
																																(BgL_ev_listz00_bglt)
																																BgL_new1125z00_4041)))->
																													BgL_argsz00) =
																												((obj_t)
																													BGl_loopze70ze7zz__evaluatez00
																													(BgL_wherez00_73,
																														BgL_globalsz00_71,
																														BgL_localsz00_70,
																														BgL_locz00_74,
																														BgL_arg1702z00_1730)),
																												BUNSPEC);
																											return ((obj_t)
																												BgL_new1125z00_4041);
																										}
																									}
																								else
																									{	/* Eval/evaluate.scm 424 */
																										if (
																											(CAR(
																													((obj_t) BgL_ez00_69))
																												==
																												BGl_symbol2982z00zz__evaluatez00))
																											{	/* Eval/evaluate.scm 424 */
																												obj_t
																													BgL_arg1705z00_1733;
																												BgL_arg1705z00_1733 =
																													CDR(((obj_t)
																														BgL_ez00_69));
																												BGL_TAIL return
																													BGl_convzd2beginzd2zz__evaluatez00
																													(BgL_arg1705z00_1733,
																													BgL_localsz00_70,
																													BgL_globalsz00_71,
																													BgL_tailzf3zf3_72,
																													BgL_wherez00_73,
																													BgL_locz00_74,
																													BgL_topzf3zf3_75);
																											}
																										else
																											{	/* Eval/evaluate.scm 424 */
																												obj_t
																													BgL_cdrzd26338zd2_1734;
																												BgL_cdrzd26338zd2_1734 =
																													CDR(((obj_t)
																														BgL_ez00_69));
																												if ((CAR(((obj_t)
																																BgL_ez00_69)) ==
																														BGl_symbol2968z00zz__evaluatez00))
																													{	/* Eval/evaluate.scm 424 */
																														if (PAIRP
																															(BgL_cdrzd26338zd2_1734))
																															{	/* Eval/evaluate.scm 424 */
																																obj_t
																																	BgL_arg1709z00_1738;
																																obj_t
																																	BgL_arg1710z00_1739;
																																BgL_arg1709z00_1738
																																	=
																																	CAR
																																	(BgL_cdrzd26338zd2_1734);
																																BgL_arg1710z00_1739
																																	=
																																	CDR
																																	(BgL_cdrzd26338zd2_1734);
																																{
																																	BgL_ev_letz00_bglt
																																		BgL_auxz00_5994;
																																	BgL_bindsz00_1545
																																		=
																																		BgL_arg1709z00_1738;
																																	BgL_bodyz00_1546
																																		=
																																		BgL_arg1710z00_1739;
																																BgL_tagzd2160zd2_1547:
																																	{	/* Eval/evaluate.scm 480 */
																																		obj_t
																																			BgL_ubindsz00_2445;
																																		if (NULLP
																																			(BgL_bindsz00_1545))
																																			{	/* Eval/evaluate.scm 480 */
																																				BgL_ubindsz00_2445
																																					=
																																					BNIL;
																																			}
																																		else
																																			{	/* Eval/evaluate.scm 480 */
																																				obj_t
																																					BgL_head1222z00_2490;
																																				BgL_head1222z00_2490
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BNIL,
																																					BNIL);
																																				{
																																					obj_t
																																						BgL_l1220z00_3661;
																																					obj_t
																																						BgL_tail1223z00_3662;
																																					BgL_l1220z00_3661
																																						=
																																						BgL_bindsz00_1545;
																																					BgL_tail1223z00_3662
																																						=
																																						BgL_head1222z00_2490;
																																				BgL_zc3z04anonymousza32327ze3z87_3660:
																																					if (NULLP(BgL_l1220z00_3661))
																																						{	/* Eval/evaluate.scm 480 */
																																							BgL_ubindsz00_2445
																																								=
																																								CDR
																																								(BgL_head1222z00_2490);
																																						}
																																					else
																																						{	/* Eval/evaluate.scm 480 */
																																							obj_t
																																								BgL_newtail1224z00_3670;
																																							{	/* Eval/evaluate.scm 480 */
																																								obj_t
																																									BgL_arg2331z00_3671;
																																								{	/* Eval/evaluate.scm 480 */
																																									obj_t
																																										BgL_bz00_3672;
																																									BgL_bz00_3672
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_l1220z00_3661));
																																									{	/* Eval/evaluate.scm 480 */
																																										obj_t
																																											BgL_arg2333z00_3673;
																																										BgL_arg2333z00_3673
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_bz00_3672));
																																										BgL_arg2331z00_3671
																																											=
																																											BGl_untypezd2identzd2zz__evaluatez00
																																											(BgL_arg2333z00_3673);
																																									}
																																								}
																																								BgL_newtail1224z00_3670
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2331z00_3671,
																																									BNIL);
																																							}
																																							SET_CDR
																																								(BgL_tail1223z00_3662,
																																								BgL_newtail1224z00_3670);
																																							{	/* Eval/evaluate.scm 480 */
																																								obj_t
																																									BgL_arg2330z00_3674;
																																								BgL_arg2330z00_3674
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_l1220z00_3661));
																																								{
																																									obj_t
																																										BgL_tail1223z00_6011;
																																									obj_t
																																										BgL_l1220z00_6010;
																																									BgL_l1220z00_6010
																																										=
																																										BgL_arg2330z00_3674;
																																									BgL_tail1223z00_6011
																																										=
																																										BgL_newtail1224z00_3670;
																																									BgL_tail1223z00_3662
																																										=
																																										BgL_tail1223z00_6011;
																																									BgL_l1220z00_3661
																																										=
																																										BgL_l1220z00_6010;
																																									goto
																																										BgL_zc3z04anonymousza32327ze3z87_3660;
																																								}
																																							}
																																						}
																																				}
																																			}
																																		{	/* Eval/evaluate.scm 480 */
																																			obj_t
																																				BgL_varsz00_2446;
																																			if (NULLP
																																				(BgL_ubindsz00_2445))
																																				{	/* Eval/evaluate.scm 481 */
																																					BgL_varsz00_2446
																																						=
																																						BNIL;
																																				}
																																			else
																																				{	/* Eval/evaluate.scm 481 */
																																					obj_t
																																						BgL_head1227z00_2474;
																																					BgL_head1227z00_2474
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BNIL,
																																						BNIL);
																																					{
																																						obj_t
																																							BgL_l1225z00_2476;
																																						obj_t
																																							BgL_tail1228z00_2477;
																																						BgL_l1225z00_2476
																																							=
																																							BgL_ubindsz00_2445;
																																						BgL_tail1228z00_2477
																																							=
																																							BgL_head1227z00_2474;
																																					BgL_zc3z04anonymousza32321ze3z87_2478:
																																						if (NULLP(BgL_l1225z00_2476))
																																							{	/* Eval/evaluate.scm 481 */
																																								BgL_varsz00_2446
																																									=
																																									CDR
																																									(BgL_head1227z00_2474);
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 481 */
																																								obj_t
																																									BgL_newtail1229z00_2480;
																																								{	/* Eval/evaluate.scm 481 */
																																									BgL_ev_varz00_bglt
																																										BgL_arg2324z00_2482;
																																									{	/* Eval/evaluate.scm 481 */
																																										obj_t
																																											BgL_iz00_2483;
																																										BgL_iz00_2483
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_l1225z00_2476));
																																										{	/* Eval/evaluate.scm 482 */
																																											BgL_ev_varz00_bglt
																																												BgL_new1129z00_2484;
																																											{	/* Eval/evaluate.scm 484 */
																																												BgL_ev_varz00_bglt
																																													BgL_new1127z00_2485;
																																												BgL_new1127z00_2485
																																													=
																																													(
																																													(BgL_ev_varz00_bglt)
																																													BOBJECT
																																													(GC_MALLOC
																																														(sizeof
																																															(struct
																																																BgL_ev_varz00_bgl))));
																																												{	/* Eval/evaluate.scm 484 */
																																													long
																																														BgL_arg2325z00_2486;
																																													BgL_arg2325z00_2486
																																														=
																																														BGL_CLASS_NUM
																																														(BGl_ev_varz00zz__evaluate_typesz00);
																																													BGL_OBJECT_CLASS_NUM_SET
																																														(
																																														((BgL_objectz00_bglt) BgL_new1127z00_2485), BgL_arg2325z00_2486);
																																												}
																																												BgL_new1129z00_2484
																																													=
																																													BgL_new1127z00_2485;
																																											}
																																											((((BgL_ev_varz00_bglt) COBJECT(BgL_new1129z00_2484))->BgL_namez00) = ((obj_t) CAR(((obj_t) BgL_iz00_2483))), BUNSPEC);
																																											((((BgL_ev_varz00_bglt) COBJECT(BgL_new1129z00_2484))->BgL_effz00) = ((obj_t) BFALSE), BUNSPEC);
																																											((((BgL_ev_varz00_bglt) COBJECT(BgL_new1129z00_2484))->BgL_typez00) = ((obj_t) CDR(((obj_t) BgL_iz00_2483))), BUNSPEC);
																																											BgL_arg2324z00_2482
																																												=
																																												BgL_new1129z00_2484;
																																									}}
																																									BgL_newtail1229z00_2480
																																										=
																																										MAKE_YOUNG_PAIR
																																										(
																																										((obj_t) BgL_arg2324z00_2482), BNIL);
																																								}
																																								SET_CDR
																																									(BgL_tail1228z00_2477,
																																									BgL_newtail1229z00_2480);
																																								{	/* Eval/evaluate.scm 481 */
																																									obj_t
																																										BgL_arg2323z00_2481;
																																									BgL_arg2323z00_2481
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_l1225z00_2476));
																																									{
																																										obj_t
																																											BgL_tail1228z00_6037;
																																										obj_t
																																											BgL_l1225z00_6036;
																																										BgL_l1225z00_6036
																																											=
																																											BgL_arg2323z00_2481;
																																										BgL_tail1228z00_6037
																																											=
																																											BgL_newtail1229z00_2480;
																																										BgL_tail1228z00_2477
																																											=
																																											BgL_tail1228z00_6037;
																																										BgL_l1225z00_2476
																																											=
																																											BgL_l1225z00_6036;
																																										goto
																																											BgL_zc3z04anonymousza32321ze3z87_2478;
																																									}
																																								}
																																							}
																																					}
																																				}
																																			{	/* Eval/evaluate.scm 481 */
																																				obj_t
																																					BgL_bodyz00_2447;
																																				{	/* Eval/evaluate.scm 486 */
																																					bool_t
																																						BgL_test3226z00_6038;
																																					{	/* Eval/evaluate.scm 486 */
																																						obj_t
																																							BgL_tmpz00_6039;
																																						BgL_tmpz00_6039
																																							=
																																							CDR
																																							(((obj_t) BgL_bodyz00_1546));
																																						BgL_test3226z00_6038
																																							=
																																							PAIRP
																																							(BgL_tmpz00_6039);
																																					}
																																					if (BgL_test3226z00_6038)
																																						{	/* Eval/evaluate.scm 486 */
																																							obj_t
																																								BgL_res2886z00_3691;
																																							BgL_res2886z00_3691
																																								=
																																								MAKE_YOUNG_EPAIR
																																								(BGl_symbol2982z00zz__evaluatez00,
																																								BgL_bodyz00_1546,
																																								BgL_locz00_74);
																																							BgL_bodyz00_2447
																																								=
																																								BgL_res2886z00_3691;
																																						}
																																					else
																																						{	/* Eval/evaluate.scm 486 */
																																							BgL_bodyz00_2447
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_bodyz00_1546));
																																						}
																																				}
																																				{	/* Eval/evaluate.scm 486 */
																																					obj_t
																																						BgL_tbodyz00_2448;
																																					BgL_tbodyz00_2448
																																						=
																																						BGl_typezd2checkszd2zz__evaluatez00
																																						(BgL_ubindsz00_2445,
																																						BgL_bindsz00_1545,
																																						BgL_bodyz00_2447,
																																						BgL_locz00_74,
																																						BgL_wherez00_73);
																																					{	/* Eval/evaluate.scm 487 */

																																						{	/* Eval/evaluate.scm 488 */
																																							obj_t
																																								BgL_blocz00_2449;
																																							{	/* Eval/evaluate.scm 144 */
																																								obj_t
																																									BgL__ortest_1069z00_3693;
																																								BgL__ortest_1069z00_3693
																																									=
																																									BGl_getzd2sourcezd2locationz00zz__readerz00
																																									(BgL_bindsz00_1545);
																																								if (CBOOL(BgL__ortest_1069z00_3693))
																																									{	/* Eval/evaluate.scm 144 */
																																										BgL_blocz00_2449
																																											=
																																											BgL__ortest_1069z00_3693;
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 144 */
																																										BgL_blocz00_2449
																																											=
																																											BgL_locz00_74;
																																									}
																																							}
																																							{	/* Eval/evaluate.scm 489 */
																																								BgL_ev_letz00_bglt
																																									BgL_new1131z00_2450;
																																								{	/* Eval/evaluate.scm 490 */
																																									BgL_ev_letz00_bglt
																																										BgL_new1130z00_2467;
																																									BgL_new1130z00_2467
																																										=
																																										(
																																										(BgL_ev_letz00_bglt)
																																										BOBJECT
																																										(GC_MALLOC
																																											(sizeof
																																												(struct
																																													BgL_ev_letz00_bgl))));
																																									{	/* Eval/evaluate.scm 490 */
																																										long
																																											BgL_arg2316z00_2468;
																																										BgL_arg2316z00_2468
																																											=
																																											BGL_CLASS_NUM
																																											(BGl_ev_letz00zz__evaluate_typesz00);
																																										BGL_OBJECT_CLASS_NUM_SET
																																											(
																																											((BgL_objectz00_bglt) BgL_new1130z00_2467), BgL_arg2316z00_2468);
																																									}
																																									BgL_new1131z00_2450
																																										=
																																										BgL_new1130z00_2467;
																																								}
																																								((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1131z00_2450)))->BgL_varsz00) = ((obj_t) BgL_varsz00_2446), BUNSPEC);
																																								{
																																									obj_t
																																										BgL_auxz00_6056;
																																									if (NULLP(BgL_bindsz00_1545))
																																										{	/* Eval/evaluate.scm 491 */
																																											BgL_auxz00_6056
																																												=
																																												BNIL;
																																										}
																																									else
																																										{	/* Eval/evaluate.scm 491 */
																																											obj_t
																																												BgL_head1232z00_2453;
																																											BgL_head1232z00_2453
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BNIL,
																																												BNIL);
																																											{
																																												obj_t
																																													BgL_l1230z00_2455;
																																												obj_t
																																													BgL_tail1233z00_2456;
																																												BgL_l1230z00_2455
																																													=
																																													BgL_bindsz00_1545;
																																												BgL_tail1233z00_2456
																																													=
																																													BgL_head1232z00_2453;
																																											BgL_zc3z04anonymousza32310ze3z87_2457:
																																												if (NULLP(BgL_l1230z00_2455))
																																													{	/* Eval/evaluate.scm 491 */
																																														BgL_auxz00_6056
																																															=
																																															CDR
																																															(BgL_head1232z00_2453);
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 491 */
																																														obj_t
																																															BgL_newtail1234z00_2459;
																																														{	/* Eval/evaluate.scm 491 */
																																															obj_t
																																																BgL_arg2313z00_2461;
																																															{	/* Eval/evaluate.scm 491 */
																																																obj_t
																																																	BgL_bz00_2462;
																																																BgL_bz00_2462
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_l1230z00_2455));
																																																{	/* Eval/evaluate.scm 492 */
																																																	obj_t
																																																		BgL_locz00_2463;
																																																	{	/* Eval/evaluate.scm 144 */
																																																		obj_t
																																																			BgL__ortest_1069z00_3699;
																																																		BgL__ortest_1069z00_3699
																																																			=
																																																			BGl_getzd2sourcezd2locationz00zz__readerz00
																																																			(BgL_bz00_2462);
																																																		if (CBOOL(BgL__ortest_1069z00_3699))
																																																			{	/* Eval/evaluate.scm 144 */
																																																				BgL_locz00_2463
																																																					=
																																																					BgL__ortest_1069z00_3699;
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 144 */
																																																				BgL_locz00_2463
																																																					=
																																																					BgL_blocz00_2449;
																																																			}
																																																	}
																																																	{	/* Eval/evaluate.scm 493 */
																																																		obj_t
																																																			BgL_arg2314z00_2464;
																																																		{	/* Eval/evaluate.scm 493 */
																																																			obj_t
																																																				BgL_pairz00_3703;
																																																			BgL_pairz00_3703
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_bz00_2462));
																																																			BgL_arg2314z00_2464
																																																				=
																																																				CAR
																																																				(BgL_pairz00_3703);
																																																		}
																																																		BgL_arg2313z00_2461
																																																			=
																																																			BGl_convz00zz__evaluatez00
																																																			(BgL_arg2314z00_2464,
																																																			BgL_localsz00_70,
																																																			BgL_globalsz00_71,
																																																			BFALSE,
																																																			BgL_wherez00_73,
																																																			BgL_locz00_2463,
																																																			((bool_t) 0));
																																																	}
																																																}
																																															}
																																															BgL_newtail1234z00_2459
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg2313z00_2461,
																																																BNIL);
																																														}
																																														SET_CDR
																																															(BgL_tail1233z00_2456,
																																															BgL_newtail1234z00_2459);
																																														{	/* Eval/evaluate.scm 491 */
																																															obj_t
																																																BgL_arg2312z00_2460;
																																															BgL_arg2312z00_2460
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_l1230z00_2455));
																																															{
																																																obj_t
																																																	BgL_tail1233z00_6078;
																																																obj_t
																																																	BgL_l1230z00_6077;
																																																BgL_l1230z00_6077
																																																	=
																																																	BgL_arg2312z00_2460;
																																																BgL_tail1233z00_6078
																																																	=
																																																	BgL_newtail1234z00_2459;
																																																BgL_tail1233z00_2456
																																																	=
																																																	BgL_tail1233z00_6078;
																																																BgL_l1230z00_2455
																																																	=
																																																	BgL_l1230z00_6077;
																																																goto
																																																	BgL_zc3z04anonymousza32310ze3z87_2457;
																																															}
																																														}
																																													}
																																											}
																																										}
																																									((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1131z00_2450)))->BgL_valsz00) = ((obj_t) BgL_auxz00_6056), BUNSPEC);
																																								}
																																								{
																																									BgL_ev_exprz00_bglt
																																										BgL_auxz00_6080;
																																									{	/* Eval/evaluate.scm 495 */
																																										obj_t
																																											BgL_arg2315z00_2466;
																																										BgL_arg2315z00_2466
																																											=
																																											BGl_appendzd221011zd2zz__evaluatez00
																																											(BgL_varsz00_2446,
																																											BgL_localsz00_70);
																																										BgL_auxz00_6080
																																											=
																																											(
																																											(BgL_ev_exprz00_bglt)
																																											BGl_convz00zz__evaluatez00
																																											(BgL_tbodyz00_2448,
																																												BgL_arg2315z00_2466,
																																												BgL_globalsz00_71,
																																												BgL_tailzf3zf3_72,
																																												BgL_wherez00_73,
																																												BgL_locz00_74,
																																												((bool_t) 0)));
																																									}
																																									((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1131z00_2450)))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6080), BUNSPEC);
																																								}
																																								((((BgL_ev_letz00_bglt) COBJECT(BgL_new1131z00_2450))->BgL_boxesz00) = ((obj_t) BNIL), BUNSPEC);
																																								BgL_auxz00_5994
																																									=
																																									BgL_new1131z00_2450;
																																							}
																																						}
																																					}
																																				}
																																			}
																																		}
																																	}
																																	return
																																		((obj_t)
																																		BgL_auxz00_5994);
																																}
																															}
																														else
																															{	/* Eval/evaluate.scm 424 */
																																obj_t
																																	BgL_arg1711z00_1740;
																																BgL_arg1711z00_1740
																																	=
																																	CAR(((obj_t)
																																		BgL_ez00_69));
																																{
																																	BgL_ev_appz00_bglt
																																		BgL_auxz00_6090;
																																	{
																																		obj_t
																																			BgL_argsz00_6092;
																																		obj_t
																																			BgL_fz00_6091;
																																		BgL_fz00_6091
																																			=
																																			BgL_arg1711z00_1740;
																																		BgL_argsz00_6092
																																			=
																																			BgL_cdrzd26338zd2_1734;
																																		BgL_argsz00_1593
																																			=
																																			BgL_argsz00_6092;
																																		BgL_fz00_1592
																																			=
																																			BgL_fz00_6091;
																																		goto
																																			BgL_tagzd2176zd2_1594;
																																	}
																																	return
																																		((obj_t)
																																		BgL_auxz00_6090);
																																}
																															}
																													}
																												else
																													{	/* Eval/evaluate.scm 424 */
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_ez00_69))
																																==
																																BGl_symbol2999z00zz__evaluatez00))
																															{	/* Eval/evaluate.scm 424 */
																																if (PAIRP
																																	(BgL_cdrzd26338zd2_1734))
																																	{	/* Eval/evaluate.scm 424 */
																																		obj_t
																																			BgL_arg1718z00_1746;
																																		obj_t
																																			BgL_arg1720z00_1747;
																																		BgL_arg1718z00_1746
																																			=
																																			CAR
																																			(BgL_cdrzd26338zd2_1734);
																																		BgL_arg1720z00_1747
																																			=
																																			CDR
																																			(BgL_cdrzd26338zd2_1734);
																																		{
																																			BgL_ev_letza2za2_bglt
																																				BgL_auxz00_6102;
																																			BgL_bindsz00_1548
																																				=
																																				BgL_arg1718z00_1746;
																																			BgL_bodyz00_1549
																																				=
																																				BgL_arg1720z00_1747;
																																		BgL_tagzd2161zd2_1550:
																																			{	/* Eval/evaluate.scm 503 */
																																				obj_t
																																					BgL_varsz00_2503;
																																				obj_t
																																					BgL_blocz00_2504;
																																				if (NULLP(BgL_bindsz00_1548))
																																					{	/* Eval/evaluate.scm 503 */
																																						BgL_varsz00_2503
																																							=
																																							BNIL;
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 503 */
																																						obj_t
																																							BgL_head1237z00_2512;
																																						BgL_head1237z00_2512
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BNIL,
																																							BNIL);
																																						{
																																							obj_t
																																								BgL_l1235z00_2514;
																																							obj_t
																																								BgL_tail1238z00_2515;
																																							BgL_l1235z00_2514
																																								=
																																								BgL_bindsz00_1548;
																																							BgL_tail1238z00_2515
																																								=
																																								BgL_head1237z00_2512;
																																						BgL_zc3z04anonymousza32339ze3z87_2516:
																																							if (NULLP(BgL_l1235z00_2514))
																																								{	/* Eval/evaluate.scm 503 */
																																									BgL_varsz00_2503
																																										=
																																										CDR
																																										(BgL_head1237z00_2512);
																																								}
																																							else
																																								{	/* Eval/evaluate.scm 503 */
																																									obj_t
																																										BgL_newtail1239z00_2518;
																																									{	/* Eval/evaluate.scm 503 */
																																										BgL_ev_varz00_bglt
																																											BgL_arg2342z00_2520;
																																										{	/* Eval/evaluate.scm 503 */
																																											obj_t
																																												BgL_bz00_2521;
																																											BgL_bz00_2521
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_l1235z00_2514));
																																											{	/* Eval/evaluate.scm 504 */
																																												obj_t
																																													BgL_iz00_2522;
																																												{	/* Eval/evaluate.scm 504 */
																																													obj_t
																																														BgL_arg2346z00_2526;
																																													BgL_arg2346z00_2526
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_bz00_2521));
																																													BgL_iz00_2522
																																														=
																																														BGl_untypezd2identzd2zz__evaluatez00
																																														(BgL_arg2346z00_2526);
																																												}
																																												{	/* Eval/evaluate.scm 505 */
																																													BgL_ev_varz00_bglt
																																														BgL_new1134z00_2523;
																																													{	/* Eval/evaluate.scm 507 */
																																														BgL_ev_varz00_bglt
																																															BgL_new1133z00_2524;
																																														BgL_new1133z00_2524
																																															=
																																															(
																																															(BgL_ev_varz00_bglt)
																																															BOBJECT
																																															(GC_MALLOC
																																																(sizeof
																																																	(struct
																																																		BgL_ev_varz00_bgl))));
																																														{	/* Eval/evaluate.scm 507 */
																																															long
																																																BgL_arg2345z00_2525;
																																															BgL_arg2345z00_2525
																																																=
																																																BGL_CLASS_NUM
																																																(BGl_ev_varz00zz__evaluate_typesz00);
																																															BGL_OBJECT_CLASS_NUM_SET
																																																(
																																																((BgL_objectz00_bglt) BgL_new1133z00_2524), BgL_arg2345z00_2525);
																																														}
																																														BgL_new1134z00_2523
																																															=
																																															BgL_new1133z00_2524;
																																													}
																																													((((BgL_ev_varz00_bglt) COBJECT(BgL_new1134z00_2523))->BgL_namez00) = ((obj_t) CAR(BgL_iz00_2522)), BUNSPEC);
																																													((((BgL_ev_varz00_bglt) COBJECT(BgL_new1134z00_2523))->BgL_effz00) = ((obj_t) BFALSE), BUNSPEC);
																																													((((BgL_ev_varz00_bglt) COBJECT(BgL_new1134z00_2523))->BgL_typez00) = ((obj_t) CDR(BgL_iz00_2522)), BUNSPEC);
																																													BgL_arg2342z00_2520
																																														=
																																														BgL_new1134z00_2523;
																																										}}}
																																										BgL_newtail1239z00_2518
																																											=
																																											MAKE_YOUNG_PAIR
																																											(
																																											((obj_t) BgL_arg2342z00_2520), BNIL);
																																									}
																																									SET_CDR
																																										(BgL_tail1238z00_2515,
																																										BgL_newtail1239z00_2518);
																																									{	/* Eval/evaluate.scm 503 */
																																										obj_t
																																											BgL_arg2341z00_2519;
																																										BgL_arg2341z00_2519
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_l1235z00_2514));
																																										{
																																											obj_t
																																												BgL_tail1238z00_6129;
																																											obj_t
																																												BgL_l1235z00_6128;
																																											BgL_l1235z00_6128
																																												=
																																												BgL_arg2341z00_2519;
																																											BgL_tail1238z00_6129
																																												=
																																												BgL_newtail1239z00_2518;
																																											BgL_tail1238z00_2515
																																												=
																																												BgL_tail1238z00_6129;
																																											BgL_l1235z00_2514
																																												=
																																												BgL_l1235z00_6128;
																																											goto
																																												BgL_zc3z04anonymousza32339ze3z87_2516;
																																										}
																																									}
																																								}
																																						}
																																					}
																																				{	/* Eval/evaluate.scm 144 */
																																					obj_t
																																						BgL__ortest_1069z00_3737;
																																					BgL__ortest_1069z00_3737
																																						=
																																						BGl_getzd2sourcezd2locationz00zz__readerz00
																																						(BgL_bindsz00_1548);
																																					if (CBOOL(BgL__ortest_1069z00_3737))
																																						{	/* Eval/evaluate.scm 144 */
																																							BgL_blocz00_2504
																																								=
																																								BgL__ortest_1069z00_3737;
																																						}
																																					else
																																						{	/* Eval/evaluate.scm 144 */
																																							BgL_blocz00_2504
																																								=
																																								BgL_locz00_74;
																																						}
																																				}
																																				{	/* Eval/evaluate.scm 510 */
																																					BgL_ev_letza2za2_bglt
																																						BgL_new1136z00_2505;
																																					{	/* Eval/evaluate.scm 511 */
																																						BgL_ev_letza2za2_bglt
																																							BgL_new1135z00_2508;
																																						BgL_new1135z00_2508
																																							=
																																							(
																																							(BgL_ev_letza2za2_bglt)
																																							BOBJECT
																																							(GC_MALLOC
																																								(sizeof
																																									(struct
																																										BgL_ev_letza2za2_bgl))));
																																						{	/* Eval/evaluate.scm 511 */
																																							long
																																								BgL_arg2337z00_2509;
																																							BgL_arg2337z00_2509
																																								=
																																								BGL_CLASS_NUM
																																								(BGl_ev_letza2za2zz__evaluate_typesz00);
																																							BGL_OBJECT_CLASS_NUM_SET
																																								(
																																								((BgL_objectz00_bglt) BgL_new1135z00_2508), BgL_arg2337z00_2509);
																																						}
																																						BgL_new1136z00_2505
																																							=
																																							BgL_new1135z00_2508;
																																					}
																																					((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1136z00_2505)))->BgL_varsz00) = ((obj_t) BgL_varsz00_2503), BUNSPEC);
																																					((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1136z00_2505)))->BgL_valsz00) = ((obj_t) BGl_convzd2valsze70z35zz__evaluatez00(BgL_wherez00_73, BgL_globalsz00_71, BgL_bindsz00_1548, BgL_varsz00_2503, BgL_localsz00_70, BgL_blocz00_2504)), BUNSPEC);
																																					{
																																						BgL_ev_exprz00_bglt
																																							BgL_auxz00_6142;
																																						{	/* Eval/evaluate.scm 513 */
																																							obj_t
																																								BgL_arg2335z00_2506;
																																							{	/* Eval/evaluate.scm 513 */
																																								obj_t
																																									BgL_arg2336z00_2507;
																																								BgL_arg2336z00_2507
																																									=
																																									bgl_reverse
																																									(BgL_varsz00_2503);
																																								BgL_arg2335z00_2506
																																									=
																																									BGl_appendzd221011zd2zz__evaluatez00
																																									(BgL_arg2336z00_2507,
																																									BgL_localsz00_70);
																																							}
																																							BgL_auxz00_6142
																																								=
																																								(
																																								(BgL_ev_exprz00_bglt)
																																								BGl_convzd2beginzd2zz__evaluatez00
																																								(BgL_bodyz00_1549,
																																									BgL_arg2335z00_2506,
																																									BgL_globalsz00_71,
																																									BgL_tailzf3zf3_72,
																																									BgL_wherez00_73,
																																									BgL_locz00_74,
																																									((bool_t) 0)));
																																						}
																																						((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1136z00_2505)))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6142), BUNSPEC);
																																					}
																																					((((BgL_ev_letza2za2_bglt) COBJECT(BgL_new1136z00_2505))->BgL_boxesz00) = ((obj_t) BNIL), BUNSPEC);
																																					BgL_auxz00_6102
																																						=
																																						BgL_new1136z00_2505;
																																			}}
																																			return
																																				((obj_t)
																																				BgL_auxz00_6102);
																																		}
																																	}
																																else
																																	{	/* Eval/evaluate.scm 424 */
																																		obj_t
																																			BgL_arg1722z00_1748;
																																		obj_t
																																			BgL_arg1723z00_1749;
																																		BgL_arg1722z00_1748
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_ez00_69));
																																		BgL_arg1723z00_1749
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_ez00_69));
																																		{
																																			BgL_ev_appz00_bglt
																																				BgL_auxz00_6155;
																																			{
																																				obj_t
																																					BgL_argsz00_6157;
																																				obj_t
																																					BgL_fz00_6156;
																																				BgL_fz00_6156
																																					=
																																					BgL_arg1722z00_1748;
																																				BgL_argsz00_6157
																																					=
																																					BgL_arg1723z00_1749;
																																				BgL_argsz00_1593
																																					=
																																					BgL_argsz00_6157;
																																				BgL_fz00_1592
																																					=
																																					BgL_fz00_6156;
																																				goto
																																					BgL_tagzd2176zd2_1594;
																																			}
																																			return
																																				((obj_t)
																																				BgL_auxz00_6155);
																																		}
																																	}
																															}
																														else
																															{	/* Eval/evaluate.scm 424 */
																																obj_t
																																	BgL_cdrzd26736zd2_1750;
																																BgL_cdrzd26736zd2_1750
																																	=
																																	CDR(((obj_t)
																																		BgL_ez00_69));
																																if ((CAR((
																																				(obj_t)
																																				BgL_ez00_69))
																																		==
																																		BGl_symbol3001z00zz__evaluatez00))
																																	{	/* Eval/evaluate.scm 424 */
																																		if (PAIRP
																																			(BgL_cdrzd26736zd2_1750))
																																			{	/* Eval/evaluate.scm 424 */
																																				obj_t
																																					BgL_arg1727z00_1754;
																																				obj_t
																																					BgL_arg1728z00_1755;
																																				BgL_arg1727z00_1754
																																					=
																																					CAR
																																					(BgL_cdrzd26736zd2_1750);
																																				BgL_arg1728z00_1755
																																					=
																																					CDR
																																					(BgL_cdrzd26736zd2_1750);
																																				{
																																					BgL_ev_letrecz00_bglt
																																						BgL_auxz00_6169;
																																					BgL_bindsz00_1551
																																						=
																																						BgL_arg1727z00_1754;
																																					BgL_bodyz00_1552
																																						=
																																						BgL_arg1728z00_1755;
																																				BgL_tagzd2162zd2_1553:
																																					{	/* Eval/evaluate.scm 515 */
																																						obj_t
																																							BgL_ubindsz00_2544;
																																						if (NULLP(BgL_bindsz00_1551))
																																							{	/* Eval/evaluate.scm 515 */
																																								BgL_ubindsz00_2544
																																									=
																																									BNIL;
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 515 */
																																								obj_t
																																									BgL_head1242z00_2594;
																																								BgL_head1242z00_2594
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BNIL,
																																									BNIL);
																																								{
																																									obj_t
																																										BgL_l1240z00_3764;
																																									obj_t
																																										BgL_tail1243z00_3765;
																																									BgL_l1240z00_3764
																																										=
																																										BgL_bindsz00_1551;
																																									BgL_tail1243z00_3765
																																										=
																																										BgL_head1242z00_2594;
																																								BgL_zc3z04anonymousza32383ze3z87_3763:
																																									if (NULLP(BgL_l1240z00_3764))
																																										{	/* Eval/evaluate.scm 515 */
																																											BgL_ubindsz00_2544
																																												=
																																												CDR
																																												(BgL_head1242z00_2594);
																																										}
																																									else
																																										{	/* Eval/evaluate.scm 515 */
																																											obj_t
																																												BgL_newtail1244z00_3773;
																																											{	/* Eval/evaluate.scm 515 */
																																												obj_t
																																													BgL_arg2386z00_3774;
																																												{	/* Eval/evaluate.scm 515 */
																																													obj_t
																																														BgL_bz00_3775;
																																													BgL_bz00_3775
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_l1240z00_3764));
																																													{	/* Eval/evaluate.scm 515 */
																																														obj_t
																																															BgL_arg2387z00_3776;
																																														BgL_arg2387z00_3776
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_bz00_3775));
																																														BgL_arg2386z00_3774
																																															=
																																															BGl_untypezd2identzd2zz__evaluatez00
																																															(BgL_arg2387z00_3776);
																																													}
																																												}
																																												BgL_newtail1244z00_3773
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg2386z00_3774,
																																													BNIL);
																																											}
																																											SET_CDR
																																												(BgL_tail1243z00_3765,
																																												BgL_newtail1244z00_3773);
																																											{	/* Eval/evaluate.scm 515 */
																																												obj_t
																																													BgL_arg2385z00_3777;
																																												BgL_arg2385z00_3777
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_l1240z00_3764));
																																												{
																																													obj_t
																																														BgL_tail1243z00_6186;
																																													obj_t
																																														BgL_l1240z00_6185;
																																													BgL_l1240z00_6185
																																														=
																																														BgL_arg2385z00_3777;
																																													BgL_tail1243z00_6186
																																														=
																																														BgL_newtail1244z00_3773;
																																													BgL_tail1243z00_3765
																																														=
																																														BgL_tail1243z00_6186;
																																													BgL_l1240z00_3764
																																														=
																																														BgL_l1240z00_6185;
																																													goto
																																														BgL_zc3z04anonymousza32383ze3z87_3763;
																																												}
																																											}
																																										}
																																								}
																																							}
																																						{	/* Eval/evaluate.scm 515 */
																																							obj_t
																																								BgL_varsz00_2545;
																																							if (NULLP(BgL_ubindsz00_2544))
																																								{	/* Eval/evaluate.scm 516 */
																																									BgL_varsz00_2545
																																										=
																																										BNIL;
																																								}
																																							else
																																								{	/* Eval/evaluate.scm 516 */
																																									obj_t
																																										BgL_head1247z00_2578;
																																									BgL_head1247z00_2578
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BNIL,
																																										BNIL);
																																									{
																																										obj_t
																																											BgL_l1245z00_2580;
																																										obj_t
																																											BgL_tail1248z00_2581;
																																										BgL_l1245z00_2580
																																											=
																																											BgL_ubindsz00_2544;
																																										BgL_tail1248z00_2581
																																											=
																																											BgL_head1247z00_2578;
																																									BgL_zc3z04anonymousza32377ze3z87_2582:
																																										if (NULLP(BgL_l1245z00_2580))
																																											{	/* Eval/evaluate.scm 516 */
																																												BgL_varsz00_2545
																																													=
																																													CDR
																																													(BgL_head1247z00_2578);
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 516 */
																																												obj_t
																																													BgL_newtail1249z00_2584;
																																												{	/* Eval/evaluate.scm 516 */
																																													BgL_ev_varz00_bglt
																																														BgL_arg2380z00_2586;
																																													{	/* Eval/evaluate.scm 516 */
																																														obj_t
																																															BgL_iz00_2587;
																																														BgL_iz00_2587
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_l1245z00_2580));
																																														{	/* Eval/evaluate.scm 517 */
																																															BgL_ev_varz00_bglt
																																																BgL_new1138z00_2588;
																																															{	/* Eval/evaluate.scm 519 */
																																																BgL_ev_varz00_bglt
																																																	BgL_new1137z00_2589;
																																																BgL_new1137z00_2589
																																																	=
																																																	(
																																																	(BgL_ev_varz00_bglt)
																																																	BOBJECT
																																																	(GC_MALLOC
																																																		(sizeof
																																																			(struct
																																																				BgL_ev_varz00_bgl))));
																																																{	/* Eval/evaluate.scm 519 */
																																																	long
																																																		BgL_arg2381z00_2590;
																																																	BgL_arg2381z00_2590
																																																		=
																																																		BGL_CLASS_NUM
																																																		(BGl_ev_varz00zz__evaluate_typesz00);
																																																	BGL_OBJECT_CLASS_NUM_SET
																																																		(
																																																		((BgL_objectz00_bglt) BgL_new1137z00_2589), BgL_arg2381z00_2590);
																																																}
																																																BgL_new1138z00_2588
																																																	=
																																																	BgL_new1137z00_2589;
																																															}
																																															((((BgL_ev_varz00_bglt) COBJECT(BgL_new1138z00_2588))->BgL_namez00) = ((obj_t) CAR(((obj_t) BgL_iz00_2587))), BUNSPEC);
																																															((((BgL_ev_varz00_bglt) COBJECT(BgL_new1138z00_2588))->BgL_effz00) = ((obj_t) BFALSE), BUNSPEC);
																																															((((BgL_ev_varz00_bglt) COBJECT(BgL_new1138z00_2588))->BgL_typez00) = ((obj_t) CDR(((obj_t) BgL_iz00_2587))), BUNSPEC);
																																															BgL_arg2380z00_2586
																																																=
																																																BgL_new1138z00_2588;
																																													}}
																																													BgL_newtail1249z00_2584
																																														=
																																														MAKE_YOUNG_PAIR
																																														(
																																														((obj_t) BgL_arg2380z00_2586), BNIL);
																																												}
																																												SET_CDR
																																													(BgL_tail1248z00_2581,
																																													BgL_newtail1249z00_2584);
																																												{	/* Eval/evaluate.scm 516 */
																																													obj_t
																																														BgL_arg2379z00_2585;
																																													BgL_arg2379z00_2585
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_l1245z00_2580));
																																													{
																																														obj_t
																																															BgL_tail1248z00_6212;
																																														obj_t
																																															BgL_l1245z00_6211;
																																														BgL_l1245z00_6211
																																															=
																																															BgL_arg2379z00_2585;
																																														BgL_tail1248z00_6212
																																															=
																																															BgL_newtail1249z00_2584;
																																														BgL_tail1248z00_2581
																																															=
																																															BgL_tail1248z00_6212;
																																														BgL_l1245z00_2580
																																															=
																																															BgL_l1245z00_6211;
																																														goto
																																															BgL_zc3z04anonymousza32377ze3z87_2582;
																																													}
																																												}
																																											}
																																									}
																																								}
																																							{	/* Eval/evaluate.scm 516 */
																																								obj_t
																																									BgL_localsz00_2546;
																																								BgL_localsz00_2546
																																									=
																																									BGl_appendzd221011zd2zz__evaluatez00
																																									(BgL_varsz00_2545,
																																									BgL_localsz00_70);
																																								{	/* Eval/evaluate.scm 521 */
																																									obj_t
																																										BgL_bodyz00_2547;
																																									{	/* Eval/evaluate.scm 522 */
																																										bool_t
																																											BgL_test3242z00_6214;
																																										{	/* Eval/evaluate.scm 522 */
																																											obj_t
																																												BgL_tmpz00_6215;
																																											BgL_tmpz00_6215
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_bodyz00_1552));
																																											BgL_test3242z00_6214
																																												=
																																												PAIRP
																																												(BgL_tmpz00_6215);
																																										}
																																										if (BgL_test3242z00_6214)
																																											{	/* Eval/evaluate.scm 522 */
																																												obj_t
																																													BgL_res2887z00_3794;
																																												BgL_res2887z00_3794
																																													=
																																													MAKE_YOUNG_EPAIR
																																													(BGl_symbol2982z00zz__evaluatez00,
																																													BgL_bodyz00_1552,
																																													BgL_locz00_74);
																																												BgL_bodyz00_2547
																																													=
																																													BgL_res2887z00_3794;
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 522 */
																																												BgL_bodyz00_2547
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_bodyz00_1552));
																																											}
																																									}
																																									{	/* Eval/evaluate.scm 522 */
																																										obj_t
																																											BgL_tbodyz00_2548;
																																										BgL_tbodyz00_2548
																																											=
																																											BGl_typezd2checkszd2zz__evaluatez00
																																											(BgL_ubindsz00_2544,
																																											BgL_bindsz00_1551,
																																											BgL_bodyz00_2547,
																																											BgL_locz00_74,
																																											BgL_wherez00_73);
																																										{	/* Eval/evaluate.scm 523 */
																																											obj_t
																																												BgL_blocz00_2549;
																																											{	/* Eval/evaluate.scm 144 */
																																												obj_t
																																													BgL__ortest_1069z00_3796;
																																												BgL__ortest_1069z00_3796
																																													=
																																													BGl_getzd2sourcezd2locationz00zz__readerz00
																																													(BgL_bindsz00_1551);
																																												if (CBOOL(BgL__ortest_1069z00_3796))
																																													{	/* Eval/evaluate.scm 144 */
																																														BgL_blocz00_2549
																																															=
																																															BgL__ortest_1069z00_3796;
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 144 */
																																														BgL_blocz00_2549
																																															=
																																															BgL_locz00_74;
																																													}
																																											}
																																											{	/* Eval/evaluate.scm 524 */

																																												{	/* Eval/evaluate.scm 525 */
																																													BgL_ev_letrecz00_bglt
																																														BgL_new1140z00_2550;
																																													{	/* Eval/evaluate.scm 526 */
																																														BgL_ev_letrecz00_bglt
																																															BgL_new1139z00_2571;
																																														BgL_new1139z00_2571
																																															=
																																															(
																																															(BgL_ev_letrecz00_bglt)
																																															BOBJECT
																																															(GC_MALLOC
																																																(sizeof
																																																	(struct
																																																		BgL_ev_letrecz00_bgl))));
																																														{	/* Eval/evaluate.scm 526 */
																																															long
																																																BgL_arg2371z00_2572;
																																															BgL_arg2371z00_2572
																																																=
																																																BGL_CLASS_NUM
																																																(BGl_ev_letrecz00zz__evaluate_typesz00);
																																															BGL_OBJECT_CLASS_NUM_SET
																																																(
																																																((BgL_objectz00_bglt) BgL_new1139z00_2571), BgL_arg2371z00_2572);
																																														}
																																														BgL_new1140z00_2550
																																															=
																																															BgL_new1139z00_2571;
																																													}
																																													((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1140z00_2550)))->BgL_varsz00) = ((obj_t) BgL_varsz00_2545), BUNSPEC);
																																													{
																																														obj_t
																																															BgL_auxz00_6232;
																																														if (NULLP(BgL_bindsz00_1551))
																																															{	/* Eval/evaluate.scm 527 */
																																																BgL_auxz00_6232
																																																	=
																																																	BNIL;
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 527 */
																																																obj_t
																																																	BgL_head1252z00_2553;
																																																BgL_head1252z00_2553
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BNIL,
																																																	BNIL);
																																																{
																																																	obj_t
																																																		BgL_l1250z00_2555;
																																																	obj_t
																																																		BgL_tail1253z00_2556;
																																																	BgL_l1250z00_2555
																																																		=
																																																		BgL_bindsz00_1551;
																																																	BgL_tail1253z00_2556
																																																		=
																																																		BgL_head1252z00_2553;
																																																BgL_zc3z04anonymousza32358ze3z87_2557:
																																																	if (NULLP(BgL_l1250z00_2555))
																																																		{	/* Eval/evaluate.scm 527 */
																																																			BgL_auxz00_6232
																																																				=
																																																				CDR
																																																				(BgL_head1252z00_2553);
																																																		}
																																																	else
																																																		{	/* Eval/evaluate.scm 527 */
																																																			obj_t
																																																				BgL_newtail1254z00_2559;
																																																			{	/* Eval/evaluate.scm 527 */
																																																				obj_t
																																																					BgL_arg2363z00_2561;
																																																				{	/* Eval/evaluate.scm 527 */
																																																					obj_t
																																																						BgL_bz00_2562;
																																																					BgL_bz00_2562
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_l1250z00_2555));
																																																					{	/* Eval/evaluate.scm 528 */
																																																						obj_t
																																																							BgL_arg2364z00_2563;
																																																						obj_t
																																																							BgL_arg2365z00_2564;
																																																						obj_t
																																																							BgL_arg2366z00_2565;
																																																						{	/* Eval/evaluate.scm 528 */
																																																							obj_t
																																																								BgL_pairz00_3805;
																																																							BgL_pairz00_3805
																																																								=
																																																								CDR
																																																								(
																																																								((obj_t) BgL_bz00_2562));
																																																							BgL_arg2364z00_2563
																																																								=
																																																								CAR
																																																								(BgL_pairz00_3805);
																																																						}
																																																						{	/* Eval/evaluate.scm 528 */
																																																							obj_t
																																																								BgL_arg2367z00_2566;
																																																							BgL_arg2367z00_2566
																																																								=
																																																								CAR
																																																								(
																																																								((obj_t) BgL_bz00_2562));
																																																							{	/* Eval/evaluate.scm 528 */
																																																								obj_t
																																																									BgL_list2368z00_2567;
																																																								{	/* Eval/evaluate.scm 528 */
																																																									obj_t
																																																										BgL_arg2369z00_2568;
																																																									{	/* Eval/evaluate.scm 528 */
																																																										obj_t
																																																											BgL_arg2370z00_2569;
																																																										BgL_arg2370z00_2569
																																																											=
																																																											MAKE_YOUNG_PAIR
																																																											(BgL_wherez00_73,
																																																											BNIL);
																																																										BgL_arg2369z00_2568
																																																											=
																																																											MAKE_YOUNG_PAIR
																																																											(BGl_symbol2984z00zz__evaluatez00,
																																																											BgL_arg2370z00_2569);
																																																									}
																																																									BgL_list2368z00_2567
																																																										=
																																																										MAKE_YOUNG_PAIR
																																																										(BgL_arg2367z00_2566,
																																																										BgL_arg2369z00_2568);
																																																								}
																																																								BgL_arg2365z00_2564
																																																									=
																																																									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																																									(BgL_list2368z00_2567);
																																																							}
																																																						}
																																																						{	/* Eval/evaluate.scm 144 */
																																																							obj_t
																																																								BgL__ortest_1069z00_3807;
																																																							BgL__ortest_1069z00_3807
																																																								=
																																																								BGl_getzd2sourcezd2locationz00zz__readerz00
																																																								(BgL_bz00_2562);
																																																							if (CBOOL(BgL__ortest_1069z00_3807))
																																																								{	/* Eval/evaluate.scm 144 */
																																																									BgL_arg2366z00_2565
																																																										=
																																																										BgL__ortest_1069z00_3807;
																																																								}
																																																							else
																																																								{	/* Eval/evaluate.scm 144 */
																																																									BgL_arg2366z00_2565
																																																										=
																																																										BgL_blocz00_2549;
																																																								}
																																																						}
																																																						BgL_arg2363z00_2561
																																																							=
																																																							BGl_convz00zz__evaluatez00
																																																							(BgL_arg2364z00_2563,
																																																							BgL_localsz00_2546,
																																																							BgL_globalsz00_71,
																																																							BFALSE,
																																																							BgL_arg2365z00_2564,
																																																							BgL_arg2366z00_2565,
																																																							((bool_t) 0));
																																																					}
																																																				}
																																																				BgL_newtail1254z00_2559
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg2363z00_2561,
																																																					BNIL);
																																																			}
																																																			SET_CDR
																																																				(BgL_tail1253z00_2556,
																																																				BgL_newtail1254z00_2559);
																																																			{	/* Eval/evaluate.scm 527 */
																																																				obj_t
																																																					BgL_arg2361z00_2560;
																																																				BgL_arg2361z00_2560
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_l1250z00_2555));
																																																				{
																																																					obj_t
																																																						BgL_tail1253z00_6260;
																																																					obj_t
																																																						BgL_l1250z00_6259;
																																																					BgL_l1250z00_6259
																																																						=
																																																						BgL_arg2361z00_2560;
																																																					BgL_tail1253z00_6260
																																																						=
																																																						BgL_newtail1254z00_2559;
																																																					BgL_tail1253z00_2556
																																																						=
																																																						BgL_tail1253z00_6260;
																																																					BgL_l1250z00_2555
																																																						=
																																																						BgL_l1250z00_6259;
																																																					goto
																																																						BgL_zc3z04anonymousza32358ze3z87_2557;
																																																				}
																																																			}
																																																		}
																																																}
																																															}
																																														((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1140z00_2550)))->BgL_valsz00) = ((obj_t) BgL_auxz00_6232), BUNSPEC);
																																													}
																																													((((BgL_ev_binderz00_bglt) COBJECT(((BgL_ev_binderz00_bglt) BgL_new1140z00_2550)))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convz00zz__evaluatez00(BgL_tbodyz00_2548, BgL_localsz00_2546, BgL_globalsz00_71, BgL_tailzf3zf3_72, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																													BgL_auxz00_6169
																																														=
																																														BgL_new1140z00_2550;
																																												}
																																											}
																																										}
																																									}
																																								}
																																							}
																																						}
																																					}
																																					return
																																						(
																																						(obj_t)
																																						BgL_auxz00_6169);
																																				}
																																			}
																																		else
																																			{	/* Eval/evaluate.scm 424 */
																																				obj_t
																																					BgL_arg1729z00_1756;
																																				BgL_arg1729z00_1756
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_ez00_69));
																																				{
																																					BgL_ev_appz00_bglt
																																						BgL_auxz00_6269;
																																					{
																																						obj_t
																																							BgL_argsz00_6271;
																																						obj_t
																																							BgL_fz00_6270;
																																						BgL_fz00_6270
																																							=
																																							BgL_arg1729z00_1756;
																																						BgL_argsz00_6271
																																							=
																																							BgL_cdrzd26736zd2_1750;
																																						BgL_argsz00_1593
																																							=
																																							BgL_argsz00_6271;
																																						BgL_fz00_1592
																																							=
																																							BgL_fz00_6270;
																																						goto
																																							BgL_tagzd2176zd2_1594;
																																					}
																																					return
																																						(
																																						(obj_t)
																																						BgL_auxz00_6269);
																																				}
																																			}
																																	}
																																else
																																	{	/* Eval/evaluate.scm 424 */
																																		if (
																																			(CAR(
																																					((obj_t) BgL_ez00_69)) == BGl_symbol3003z00zz__evaluatez00))
																																			{	/* Eval/evaluate.scm 424 */
																																				if (PAIRP(BgL_cdrzd26736zd2_1750))
																																					{	/* Eval/evaluate.scm 424 */
																																						obj_t
																																							BgL_carzd26912zd2_1762;
																																						obj_t
																																							BgL_cdrzd26913zd2_1763;
																																						BgL_carzd26912zd2_1762
																																							=
																																							CAR
																																							(BgL_cdrzd26736zd2_1750);
																																						BgL_cdrzd26913zd2_1763
																																							=
																																							CDR
																																							(BgL_cdrzd26736zd2_1750);
																																						if (PAIRP(BgL_carzd26912zd2_1762))
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_cdrzd26917zd2_1765;
																																								BgL_cdrzd26917zd2_1765
																																									=
																																									CDR
																																									(BgL_carzd26912zd2_1762);
																																								if ((CAR(BgL_carzd26912zd2_1762) == BGl_symbol2989z00zz__evaluatez00))
																																									{	/* Eval/evaluate.scm 424 */
																																										if (PAIRP(BgL_cdrzd26917zd2_1765))
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_carzd26920zd2_1769;
																																												obj_t
																																													BgL_cdrzd26921zd2_1770;
																																												BgL_carzd26920zd2_1769
																																													=
																																													CAR
																																													(BgL_cdrzd26917zd2_1765);
																																												BgL_cdrzd26921zd2_1770
																																													=
																																													CDR
																																													(BgL_cdrzd26917zd2_1765);
																																												if (SYMBOLP(BgL_carzd26920zd2_1769))
																																													{	/* Eval/evaluate.scm 424 */
																																														if (PAIRP(BgL_cdrzd26921zd2_1770))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_carzd26925zd2_1773;
																																																BgL_carzd26925zd2_1773
																																																	=
																																																	CAR
																																																	(BgL_cdrzd26921zd2_1770);
																																																if (SYMBOLP(BgL_carzd26925zd2_1773))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		if (NULLP(CDR(BgL_cdrzd26921zd2_1770)))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				if (PAIRP(BgL_cdrzd26913zd2_1763))
																																																					{	/* Eval/evaluate.scm 424 */
																																																						if (NULLP(CDR(BgL_cdrzd26913zd2_1763)))
																																																							{	/* Eval/evaluate.scm 424 */
																																																								obj_t
																																																									BgL_arg1746z00_1780;
																																																								BgL_arg1746z00_1780
																																																									=
																																																									CAR
																																																									(BgL_cdrzd26913zd2_1763);
																																																								{	/* Eval/evaluate.scm 531 */
																																																									BgL_ev_setglobalz00_bglt
																																																										BgL_new1142z00_4079;
																																																									{	/* Eval/evaluate.scm 535 */
																																																										BgL_ev_setglobalz00_bglt
																																																											BgL_new1141z00_4080;
																																																										BgL_new1141z00_4080
																																																											=
																																																											(
																																																											(BgL_ev_setglobalz00_bglt)
																																																											BOBJECT
																																																											(GC_MALLOC
																																																												(sizeof
																																																													(struct
																																																														BgL_ev_setglobalz00_bgl))));
																																																										{	/* Eval/evaluate.scm 535 */
																																																											long
																																																												BgL_arg2388z00_4081;
																																																											BgL_arg2388z00_4081
																																																												=
																																																												BGL_CLASS_NUM
																																																												(BGl_ev_setglobalz00zz__evaluate_typesz00);
																																																											BGL_OBJECT_CLASS_NUM_SET
																																																												(
																																																												((BgL_objectz00_bglt) BgL_new1141z00_4080), BgL_arg2388z00_4081);
																																																										}
																																																										BgL_new1142z00_4079
																																																											=
																																																											BgL_new1141z00_4080;
																																																									}
																																																									{
																																																										BgL_ev_exprz00_bglt
																																																											BgL_auxz00_6311;
																																																										{	/* Eval/evaluate.scm 380 */
																																																											obj_t
																																																												BgL_arg2446z00_4085;
																																																											BgL_arg2446z00_4085
																																																												=
																																																												BGl_getzd2locationzd2zz__evaluatez00
																																																												(BgL_arg1746z00_1780,
																																																												BgL_locz00_74);
																																																											BgL_auxz00_6311
																																																												=
																																																												(
																																																												(BgL_ev_exprz00_bglt)
																																																												BGl_uconvzf2locze70z15zz__evaluatez00
																																																												(BgL_wherez00_73,
																																																													BgL_globalsz00_71,
																																																													BgL_localsz00_70,
																																																													BgL_arg1746z00_1780,
																																																													BgL_arg2446z00_4085));
																																																										}
																																																										((((BgL_ev_hookz00_bglt) COBJECT(((BgL_ev_hookz00_bglt) BgL_new1142z00_4079)))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6311), BUNSPEC);
																																																									}
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1142z00_4079))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1142z00_4079))->BgL_namez00) = ((obj_t) BgL_carzd26920zd2_1769), BUNSPEC);
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1142z00_4079))->BgL_modz00) = ((obj_t) BGl_evalzd2findzd2modulez00zz__evmodulez00(BgL_carzd26925zd2_1773)), BUNSPEC);
																																																									return
																																																										(
																																																										(obj_t)
																																																										BgL_new1142z00_4079);
																																																								}
																																																							}
																																																						else
																																																							{	/* Eval/evaluate.scm 424 */
																																																								return
																																																									BGl_evcompilezd2errorzd2zz__evcompilez00
																																																									(BgL_locz00_74,
																																																									BGl_string2906z00zz__evaluatez00,
																																																									BGl_string2981z00zz__evaluatez00,
																																																									BgL_ez00_69);
																																																							}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 424 */
																																																						return
																																																							BGl_evcompilezd2errorzd2zz__evcompilez00
																																																							(BgL_locz00_74,
																																																							BGl_string2906z00zz__evaluatez00,
																																																							BGl_string2981z00zz__evaluatez00,
																																																							BgL_ez00_69);
																																																					}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_cdrzd27041zd2_1782;
																																																				BgL_cdrzd27041zd2_1782
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				{	/* Eval/evaluate.scm 424 */
																																																					obj_t
																																																						BgL_cdrzd27045zd2_1783;
																																																					BgL_cdrzd27045zd2_1783
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_cdrzd27041zd2_1782));
																																																					if (PAIRP(BgL_cdrzd27045zd2_1783))
																																																						{	/* Eval/evaluate.scm 424 */
																																																							if (NULLP(CDR(BgL_cdrzd27045zd2_1783)))
																																																								{	/* Eval/evaluate.scm 424 */
																																																									obj_t
																																																										BgL_arg1751z00_1787;
																																																									obj_t
																																																										BgL_arg1752z00_1788;
																																																									BgL_arg1751z00_1787
																																																										=
																																																										CAR
																																																										(
																																																										((obj_t) BgL_cdrzd27041zd2_1782));
																																																									BgL_arg1752z00_1788
																																																										=
																																																										CAR
																																																										(BgL_cdrzd27045zd2_1783);
																																																									{
																																																										BgL_ev_hookz00_bglt
																																																											BgL_auxz00_6336;
																																																										BgL_vz00_1561
																																																											=
																																																											BgL_arg1751z00_1787;
																																																										BgL_ez00_1562
																																																											=
																																																											BgL_arg1752z00_1788;
																																																									BgL_tagzd2165zd2_1563:
																																																										{	/* Eval/evaluate.scm 541 */
																																																											obj_t
																																																												BgL_cvz00_2635;
																																																											BgL_cvz00_2635
																																																												=
																																																												BGl_convzd2varzd2zz__evaluatez00
																																																												(BgL_vz00_1561,
																																																												BgL_localsz00_70);
																																																											{	/* Eval/evaluate.scm 541 */
																																																												obj_t
																																																													BgL_ez00_2636;
																																																												{	/* Eval/evaluate.scm 380 */
																																																													obj_t
																																																														BgL_arg2446z00_3821;
																																																													{	/* Eval/evaluate.scm 144 */
																																																														obj_t
																																																															BgL__ortest_1069z00_3822;
																																																														BgL__ortest_1069z00_3822
																																																															=
																																																															BGl_getzd2sourcezd2locationz00zz__readerz00
																																																															(BgL_ez00_1562);
																																																														if (CBOOL(BgL__ortest_1069z00_3822))
																																																															{	/* Eval/evaluate.scm 144 */
																																																																BgL_arg2446z00_3821
																																																																	=
																																																																	BgL__ortest_1069z00_3822;
																																																															}
																																																														else
																																																															{	/* Eval/evaluate.scm 144 */
																																																																BgL_arg2446z00_3821
																																																																	=
																																																																	BgL_locz00_74;
																																																															}
																																																													}
																																																													BgL_ez00_2636
																																																														=
																																																														BGl_convz00zz__evaluatez00
																																																														(BgL_ez00_1562,
																																																														BgL_localsz00_70,
																																																														BgL_globalsz00_71,
																																																														BFALSE,
																																																														BgL_wherez00_73,
																																																														BgL_arg2446z00_3821,
																																																														((bool_t) 0));
																																																												}
																																																												{	/* Eval/evaluate.scm 542 */

																																																													{	/* Eval/evaluate.scm 543 */
																																																														bool_t
																																																															BgL_test3261z00_6342;
																																																														{	/* Eval/evaluate.scm 543 */
																																																															obj_t
																																																																BgL_classz00_3823;
																																																															BgL_classz00_3823
																																																																=
																																																																BGl_ev_absz00zz__evaluate_typesz00;
																																																															if (BGL_OBJECTP(BgL_ez00_2636))
																																																																{	/* Eval/evaluate.scm 543 */
																																																																	BgL_objectz00_bglt
																																																																		BgL_arg2836z00_3825;
																																																																	BgL_arg2836z00_3825
																																																																		=
																																																																		(BgL_objectz00_bglt)
																																																																		(BgL_ez00_2636);
																																																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																																																		{	/* Eval/evaluate.scm 543 */
																																																																			long
																																																																				BgL_idxz00_3831;
																																																																			BgL_idxz00_3831
																																																																				=
																																																																				BGL_OBJECT_INHERITANCE_NUM
																																																																				(BgL_arg2836z00_3825);
																																																																			BgL_test3261z00_6342
																																																																				=
																																																																				(VECTOR_REF
																																																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																																																					(BgL_idxz00_3831
																																																																						+
																																																																						2L))
																																																																				==
																																																																				BgL_classz00_3823);
																																																																		}
																																																																	else
																																																																		{	/* Eval/evaluate.scm 543 */
																																																																			bool_t
																																																																				BgL_res2888z00_3856;
																																																																			{	/* Eval/evaluate.scm 543 */
																																																																				obj_t
																																																																					BgL_oclassz00_3839;
																																																																				{	/* Eval/evaluate.scm 543 */
																																																																					obj_t
																																																																						BgL_arg2848z00_3847;
																																																																					long
																																																																						BgL_arg2856z00_3848;
																																																																					BgL_arg2848z00_3847
																																																																						=
																																																																						(BGl_za2classesza2z00zz__objectz00);
																																																																					{	/* Eval/evaluate.scm 543 */
																																																																						long
																																																																							BgL_arg2858z00_3849;
																																																																						BgL_arg2858z00_3849
																																																																							=
																																																																							BGL_OBJECT_CLASS_NUM
																																																																							(BgL_arg2836z00_3825);
																																																																						BgL_arg2856z00_3848
																																																																							=
																																																																							(BgL_arg2858z00_3849
																																																																							-
																																																																							OBJECT_TYPE);
																																																																					}
																																																																					BgL_oclassz00_3839
																																																																						=
																																																																						VECTOR_REF
																																																																						(BgL_arg2848z00_3847,
																																																																						BgL_arg2856z00_3848);
																																																																				}
																																																																				{	/* Eval/evaluate.scm 543 */
																																																																					bool_t
																																																																						BgL__ortest_1205z00_3840;
																																																																					BgL__ortest_1205z00_3840
																																																																						=
																																																																						(BgL_classz00_3823
																																																																						==
																																																																						BgL_oclassz00_3839);
																																																																					if (BgL__ortest_1205z00_3840)
																																																																						{	/* Eval/evaluate.scm 543 */
																																																																							BgL_res2888z00_3856
																																																																								=
																																																																								BgL__ortest_1205z00_3840;
																																																																						}
																																																																					else
																																																																						{	/* Eval/evaluate.scm 543 */
																																																																							long
																																																																								BgL_odepthz00_3841;
																																																																							{	/* Eval/evaluate.scm 543 */
																																																																								obj_t
																																																																									BgL_arg2833z00_3842;
																																																																								BgL_arg2833z00_3842
																																																																									=
																																																																									(BgL_oclassz00_3839);
																																																																								BgL_odepthz00_3841
																																																																									=
																																																																									BGL_CLASS_DEPTH
																																																																									(BgL_arg2833z00_3842);
																																																																							}
																																																																							if ((2L < BgL_odepthz00_3841))
																																																																								{	/* Eval/evaluate.scm 543 */
																																																																									obj_t
																																																																										BgL_arg2831z00_3844;
																																																																									{	/* Eval/evaluate.scm 543 */
																																																																										obj_t
																																																																											BgL_arg2832z00_3845;
																																																																										BgL_arg2832z00_3845
																																																																											=
																																																																											(BgL_oclassz00_3839);
																																																																										BgL_arg2831z00_3844
																																																																											=
																																																																											BGL_CLASS_ANCESTORS_REF
																																																																											(BgL_arg2832z00_3845,
																																																																											2L);
																																																																									}
																																																																									BgL_res2888z00_3856
																																																																										=
																																																																										(BgL_arg2831z00_3844
																																																																										==
																																																																										BgL_classz00_3823);
																																																																								}
																																																																							else
																																																																								{	/* Eval/evaluate.scm 543 */
																																																																									BgL_res2888z00_3856
																																																																										=
																																																																										(
																																																																										(bool_t)
																																																																										0);
																																																																								}
																																																																						}
																																																																				}
																																																																			}
																																																																			BgL_test3261z00_6342
																																																																				=
																																																																				BgL_res2888z00_3856;
																																																																		}
																																																																}
																																																															else
																																																																{	/* Eval/evaluate.scm 543 */
																																																																	BgL_test3261z00_6342
																																																																		=
																																																																		(
																																																																		(bool_t)
																																																																		0);
																																																																}
																																																														}
																																																														if (BgL_test3261z00_6342)
																																																															{
																																																																obj_t
																																																																	BgL_auxz00_6365;
																																																																{	/* Eval/evaluate.scm 545 */
																																																																	obj_t
																																																																		BgL_arg2405z00_2639;
																																																																	{	/* Eval/evaluate.scm 545 */
																																																																		obj_t
																																																																			BgL_arg2407z00_2640;
																																																																		obj_t
																																																																			BgL_arg2408z00_2641;
																																																																		{	/* Eval/evaluate.scm 545 */
																																																																			obj_t
																																																																				BgL_arg2648z00_3858;
																																																																			BgL_arg2648z00_3858
																																																																				=
																																																																				SYMBOL_TO_STRING
																																																																				(
																																																																				((obj_t) BgL_vz00_1561));
																																																																			BgL_arg2407z00_2640
																																																																				=
																																																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																																				(BgL_arg2648z00_3858);
																																																																		}
																																																																		{	/* Eval/evaluate.scm 545 */
																																																																			obj_t
																																																																				BgL_arg2410z00_2642;
																																																																			BgL_arg2410z00_2642
																																																																				=
																																																																				(
																																																																				((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt) BgL_ez00_2636)))->BgL_wherez00);
																																																																			{	/* Eval/evaluate.scm 545 */
																																																																				obj_t
																																																																					BgL_arg2648z00_3860;
																																																																				BgL_arg2648z00_3860
																																																																					=
																																																																					SYMBOL_TO_STRING
																																																																					(
																																																																					((obj_t) BgL_arg2410z00_2642));
																																																																				BgL_arg2408z00_2641
																																																																					=
																																																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																																					(BgL_arg2648z00_3860);
																																																																			}
																																																																		}
																																																																		BgL_arg2405z00_2639
																																																																			=
																																																																			string_append
																																																																			(BgL_arg2407z00_2640,
																																																																			BgL_arg2408z00_2641);
																																																																	}
																																																																	BgL_auxz00_6365
																																																																		=
																																																																		bstring_to_symbol
																																																																		(BgL_arg2405z00_2639);
																																																																}
																																																																((((BgL_ev_absz00_bglt) COBJECT(((BgL_ev_absz00_bglt) BgL_ez00_2636)))->BgL_wherez00) = ((obj_t) BgL_auxz00_6365), BUNSPEC);
																																																															}
																																																														else
																																																															{	/* Eval/evaluate.scm 543 */
																																																																BFALSE;
																																																															}
																																																													}
																																																													if (CBOOL(BgL_cvz00_2635))
																																																														{	/* Eval/evaluate.scm 547 */
																																																															BgL_ev_setlocalz00_bglt
																																																																BgL_new1145z00_2643;
																																																															{	/* Eval/evaluate.scm 549 */
																																																																BgL_ev_setlocalz00_bglt
																																																																	BgL_new1144z00_2644;
																																																																BgL_new1144z00_2644
																																																																	=
																																																																	(
																																																																	(BgL_ev_setlocalz00_bglt)
																																																																	BOBJECT
																																																																	(GC_MALLOC
																																																																		(sizeof
																																																																			(struct
																																																																				BgL_ev_setlocalz00_bgl))));
																																																																{	/* Eval/evaluate.scm 549 */
																																																																	long
																																																																		BgL_arg2411z00_2645;
																																																																	BgL_arg2411z00_2645
																																																																		=
																																																																		BGL_CLASS_NUM
																																																																		(BGl_ev_setlocalz00zz__evaluate_typesz00);
																																																																	BGL_OBJECT_CLASS_NUM_SET
																																																																		(
																																																																		((BgL_objectz00_bglt) BgL_new1144z00_2644), BgL_arg2411z00_2645);
																																																																}
																																																																BgL_new1145z00_2643
																																																																	=
																																																																	BgL_new1144z00_2644;
																																																															}
																																																															((((BgL_ev_hookz00_bglt) COBJECT(((BgL_ev_hookz00_bglt) BgL_new1145z00_2643)))->BgL_ez00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_ez00_2636)), BUNSPEC);
																																																															((((BgL_ev_setlocalz00_bglt) COBJECT(BgL_new1145z00_2643))->BgL_vz00) = ((BgL_ev_varz00_bglt) ((BgL_ev_varz00_bglt) BgL_cvz00_2635)), BUNSPEC);
																																																															BgL_auxz00_6336
																																																																=
																																																																(
																																																																(BgL_ev_hookz00_bglt)
																																																																BgL_new1145z00_2643);
																																																														}
																																																													else
																																																														{	/* Eval/evaluate.scm 550 */
																																																															BgL_ev_setglobalz00_bglt
																																																																BgL_new1147z00_2646;
																																																															{	/* Eval/evaluate.scm 554 */
																																																																BgL_ev_setglobalz00_bglt
																																																																	BgL_new1146z00_2648;
																																																																BgL_new1146z00_2648
																																																																	=
																																																																	(
																																																																	(BgL_ev_setglobalz00_bglt)
																																																																	BOBJECT
																																																																	(GC_MALLOC
																																																																		(sizeof
																																																																			(struct
																																																																				BgL_ev_setglobalz00_bgl))));
																																																																{	/* Eval/evaluate.scm 554 */
																																																																	long
																																																																		BgL_arg2414z00_2649;
																																																																	BgL_arg2414z00_2649
																																																																		=
																																																																		BGL_CLASS_NUM
																																																																		(BGl_ev_setglobalz00zz__evaluate_typesz00);
																																																																	BGL_OBJECT_CLASS_NUM_SET
																																																																		(
																																																																		((BgL_objectz00_bglt) BgL_new1146z00_2648), BgL_arg2414z00_2649);
																																																																}
																																																																BgL_new1147z00_2646
																																																																	=
																																																																	BgL_new1146z00_2648;
																																																															}
																																																															((((BgL_ev_hookz00_bglt) COBJECT(((BgL_ev_hookz00_bglt) BgL_new1147z00_2646)))->BgL_ez00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BgL_ez00_2636)), BUNSPEC);
																																																															((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1147z00_2646))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																															((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1147z00_2646))->BgL_namez00) = ((obj_t) BgL_vz00_1561), BUNSPEC);
																																																															{
																																																																obj_t
																																																																	BgL_auxz00_6399;
																																																																if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_globalsz00_71))
																																																																	{	/* Eval/evaluate.scm 553 */
																																																																		BgL_auxz00_6399
																																																																			=
																																																																			BgL_globalsz00_71;
																																																																	}
																																																																else
																																																																	{	/* Eval/evaluate.scm 553 */
																																																																		BgL_auxz00_6399
																																																																			=
																																																																			BGL_MODULE
																																																																			();
																																																																	}
																																																																((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1147z00_2646))->BgL_modz00) = ((obj_t) BgL_auxz00_6399), BUNSPEC);
																																																															}
																																																															BgL_auxz00_6336
																																																																=
																																																																(
																																																																(BgL_ev_hookz00_bglt)
																																																																BgL_new1147z00_2646);
																																																														}
																																																												}
																																																											}
																																																										}
																																																										return
																																																											(
																																																											(obj_t)
																																																											BgL_auxz00_6336);
																																																									}
																																																								}
																																																							else
																																																								{	/* Eval/evaluate.scm 424 */
																																																									return
																																																										BGl_evcompilezd2errorzd2zz__evcompilez00
																																																										(BgL_locz00_74,
																																																										BGl_string2906z00zz__evaluatez00,
																																																										BGl_string2981z00zz__evaluatez00,
																																																										BgL_ez00_69);
																																																								}
																																																						}
																																																					else
																																																						{	/* Eval/evaluate.scm 424 */
																																																							return
																																																								BGl_evcompilezd2errorzd2zz__evcompilez00
																																																								(BgL_locz00_74,
																																																								BGl_string2906z00zz__evaluatez00,
																																																								BGl_string2981z00zz__evaluatez00,
																																																								BgL_ez00_69);
																																																						}
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_cdrzd27102zd2_1791;
																																																		BgL_cdrzd27102zd2_1791
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{	/* Eval/evaluate.scm 424 */
																																																			obj_t
																																																				BgL_cdrzd27106zd2_1792;
																																																			BgL_cdrzd27106zd2_1792
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd27102zd2_1791));
																																																			if (PAIRP(BgL_cdrzd27106zd2_1792))
																																																				{	/* Eval/evaluate.scm 424 */
																																																					if (NULLP(CDR(BgL_cdrzd27106zd2_1792)))
																																																						{	/* Eval/evaluate.scm 424 */
																																																							obj_t
																																																								BgL_arg1758z00_1796;
																																																							obj_t
																																																								BgL_arg1759z00_1797;
																																																							BgL_arg1758z00_1796
																																																								=
																																																								CAR
																																																								(
																																																								((obj_t) BgL_cdrzd27102zd2_1791));
																																																							BgL_arg1759z00_1797
																																																								=
																																																								CAR
																																																								(BgL_cdrzd27106zd2_1792);
																																																							{
																																																								BgL_ev_hookz00_bglt
																																																									BgL_auxz00_6420;
																																																								{
																																																									obj_t
																																																										BgL_ez00_6422;
																																																									obj_t
																																																										BgL_vz00_6421;
																																																									BgL_vz00_6421
																																																										=
																																																										BgL_arg1758z00_1796;
																																																									BgL_ez00_6422
																																																										=
																																																										BgL_arg1759z00_1797;
																																																									BgL_ez00_1562
																																																										=
																																																										BgL_ez00_6422;
																																																									BgL_vz00_1561
																																																										=
																																																										BgL_vz00_6421;
																																																									goto
																																																										BgL_tagzd2165zd2_1563;
																																																								}
																																																								return
																																																									(
																																																									(obj_t)
																																																									BgL_auxz00_6420);
																																																							}
																																																						}
																																																					else
																																																						{	/* Eval/evaluate.scm 424 */
																																																							return
																																																								BGl_evcompilezd2errorzd2zz__evcompilez00
																																																								(BgL_locz00_74,
																																																								BGl_string2906z00zz__evaluatez00,
																																																								BGl_string2981z00zz__evaluatez00,
																																																								BgL_ez00_69);
																																																						}
																																																				}
																																																			else
																																																				{	/* Eval/evaluate.scm 424 */
																																																					return
																																																						BGl_evcompilezd2errorzd2zz__evcompilez00
																																																						(BgL_locz00_74,
																																																						BGl_string2906z00zz__evaluatez00,
																																																						BGl_string2981z00zz__evaluatez00,
																																																						BgL_ez00_69);
																																																				}
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd27163zd2_1799;
																																																BgL_cdrzd27163zd2_1799
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{	/* Eval/evaluate.scm 424 */
																																																	obj_t
																																																		BgL_cdrzd27167zd2_1800;
																																																	BgL_cdrzd27167zd2_1800
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd27163zd2_1799));
																																																	if (PAIRP(BgL_cdrzd27167zd2_1800))
																																																		{	/* Eval/evaluate.scm 424 */
																																																			if (NULLP(CDR(BgL_cdrzd27167zd2_1800)))
																																																				{	/* Eval/evaluate.scm 424 */
																																																					obj_t
																																																						BgL_arg1764z00_1804;
																																																					obj_t
																																																						BgL_arg1765z00_1805;
																																																					BgL_arg1764z00_1804
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_cdrzd27163zd2_1799));
																																																					BgL_arg1765z00_1805
																																																						=
																																																						CAR
																																																						(BgL_cdrzd27167zd2_1800);
																																																					{
																																																						BgL_ev_hookz00_bglt
																																																							BgL_auxz00_6438;
																																																						{
																																																							obj_t
																																																								BgL_ez00_6440;
																																																							obj_t
																																																								BgL_vz00_6439;
																																																							BgL_vz00_6439
																																																								=
																																																								BgL_arg1764z00_1804;
																																																							BgL_ez00_6440
																																																								=
																																																								BgL_arg1765z00_1805;
																																																							BgL_ez00_1562
																																																								=
																																																								BgL_ez00_6440;
																																																							BgL_vz00_1561
																																																								=
																																																								BgL_vz00_6439;
																																																							goto
																																																								BgL_tagzd2165zd2_1563;
																																																						}
																																																						return
																																																							(
																																																							(obj_t)
																																																							BgL_auxz00_6438);
																																																					}
																																																				}
																																																			else
																																																				{	/* Eval/evaluate.scm 424 */
																																																					return
																																																						BGl_evcompilezd2errorzd2zz__evcompilez00
																																																						(BgL_locz00_74,
																																																						BGl_string2906z00zz__evaluatez00,
																																																						BGl_string2981z00zz__evaluatez00,
																																																						BgL_ez00_69);
																																																				}
																																																		}
																																																	else
																																																		{	/* Eval/evaluate.scm 424 */
																																																			return
																																																				BGl_evcompilezd2errorzd2zz__evcompilez00
																																																				(BgL_locz00_74,
																																																				BGl_string2906z00zz__evaluatez00,
																																																				BGl_string2981z00zz__evaluatez00,
																																																				BgL_ez00_69);
																																																		}
																																																}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_cdrzd27224zd2_1807;
																																														BgL_cdrzd27224zd2_1807
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{	/* Eval/evaluate.scm 424 */
																																															obj_t
																																																BgL_cdrzd27228zd2_1808;
																																															BgL_cdrzd27228zd2_1808
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd27224zd2_1807));
																																															if (PAIRP(BgL_cdrzd27228zd2_1808))
																																																{	/* Eval/evaluate.scm 424 */
																																																	if (NULLP(CDR(BgL_cdrzd27228zd2_1808)))
																																																		{	/* Eval/evaluate.scm 424 */
																																																			obj_t
																																																				BgL_arg1770z00_1812;
																																																			obj_t
																																																				BgL_arg1771z00_1813;
																																																			BgL_arg1770z00_1812
																																																				=
																																																				CAR
																																																				(
																																																				((obj_t) BgL_cdrzd27224zd2_1807));
																																																			BgL_arg1771z00_1813
																																																				=
																																																				CAR
																																																				(BgL_cdrzd27228zd2_1808);
																																																			{
																																																				BgL_ev_hookz00_bglt
																																																					BgL_auxz00_6456;
																																																				{
																																																					obj_t
																																																						BgL_ez00_6458;
																																																					obj_t
																																																						BgL_vz00_6457;
																																																					BgL_vz00_6457
																																																						=
																																																						BgL_arg1770z00_1812;
																																																					BgL_ez00_6458
																																																						=
																																																						BgL_arg1771z00_1813;
																																																					BgL_ez00_1562
																																																						=
																																																						BgL_ez00_6458;
																																																					BgL_vz00_1561
																																																						=
																																																						BgL_vz00_6457;
																																																					goto
																																																						BgL_tagzd2165zd2_1563;
																																																				}
																																																				return
																																																					(
																																																					(obj_t)
																																																					BgL_auxz00_6456);
																																																			}
																																																		}
																																																	else
																																																		{	/* Eval/evaluate.scm 424 */
																																																			return
																																																				BGl_evcompilezd2errorzd2zz__evcompilez00
																																																				(BgL_locz00_74,
																																																				BGl_string2906z00zz__evaluatez00,
																																																				BGl_string2981z00zz__evaluatez00,
																																																				BgL_ez00_69);
																																																		}
																																																}
																																															else
																																																{	/* Eval/evaluate.scm 424 */
																																																	return
																																																		BGl_evcompilezd2errorzd2zz__evcompilez00
																																																		(BgL_locz00_74,
																																																		BGl_string2906z00zz__evaluatez00,
																																																		BGl_string2981z00zz__evaluatez00,
																																																		BgL_ez00_69);
																																																}
																																														}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_cdrzd27285zd2_1815;
																																												BgL_cdrzd27285zd2_1815
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{	/* Eval/evaluate.scm 424 */
																																													obj_t
																																														BgL_cdrzd27289zd2_1816;
																																													BgL_cdrzd27289zd2_1816
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd27285zd2_1815));
																																													if (PAIRP(BgL_cdrzd27289zd2_1816))
																																														{	/* Eval/evaluate.scm 424 */
																																															if (NULLP(CDR(BgL_cdrzd27289zd2_1816)))
																																																{	/* Eval/evaluate.scm 424 */
																																																	obj_t
																																																		BgL_arg1777z00_1820;
																																																	obj_t
																																																		BgL_arg1779z00_1821;
																																																	BgL_arg1777z00_1820
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_cdrzd27285zd2_1815));
																																																	BgL_arg1779z00_1821
																																																		=
																																																		CAR
																																																		(BgL_cdrzd27289zd2_1816);
																																																	{
																																																		BgL_ev_hookz00_bglt
																																																			BgL_auxz00_6474;
																																																		{
																																																			obj_t
																																																				BgL_ez00_6476;
																																																			obj_t
																																																				BgL_vz00_6475;
																																																			BgL_vz00_6475
																																																				=
																																																				BgL_arg1777z00_1820;
																																																			BgL_ez00_6476
																																																				=
																																																				BgL_arg1779z00_1821;
																																																			BgL_ez00_1562
																																																				=
																																																				BgL_ez00_6476;
																																																			BgL_vz00_1561
																																																				=
																																																				BgL_vz00_6475;
																																																			goto
																																																				BgL_tagzd2165zd2_1563;
																																																		}
																																																		return
																																																			(
																																																			(obj_t)
																																																			BgL_auxz00_6474);
																																																	}
																																																}
																																															else
																																																{	/* Eval/evaluate.scm 424 */
																																																	return
																																																		BGl_evcompilezd2errorzd2zz__evcompilez00
																																																		(BgL_locz00_74,
																																																		BGl_string2906z00zz__evaluatez00,
																																																		BGl_string2981z00zz__evaluatez00,
																																																		BgL_ez00_69);
																																																}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_cdrzd27333zd2_1823;
																																										BgL_cdrzd27333zd2_1823
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										{	/* Eval/evaluate.scm 424 */
																																											obj_t
																																												BgL_carzd27336zd2_1824;
																																											obj_t
																																												BgL_cdrzd27337zd2_1825;
																																											BgL_carzd27336zd2_1824
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd27333zd2_1823));
																																											BgL_cdrzd27337zd2_1825
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd27333zd2_1823));
																																											if ((CAR(((obj_t) BgL_carzd27336zd2_1824)) == BGl_symbol2991z00zz__evaluatez00))
																																												{	/* Eval/evaluate.scm 424 */
																																													if (PAIRP(BgL_cdrzd27337zd2_1825))
																																														{	/* Eval/evaluate.scm 424 */
																																															if (NULLP(CDR(BgL_cdrzd27337zd2_1825)))
																																																{	/* Eval/evaluate.scm 424 */
																																																	obj_t
																																																		BgL_arg1787z00_1831;
																																																	obj_t
																																																		BgL_arg1788z00_1832;
																																																	BgL_arg1787z00_1831
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_carzd27336zd2_1824));
																																																	BgL_arg1788z00_1832
																																																		=
																																																		CAR
																																																		(BgL_cdrzd27337zd2_1825);
																																																	BgL_lz00_1558
																																																		=
																																																		BgL_arg1787z00_1831;
																																																	BgL_e2z00_1559
																																																		=
																																																		BgL_arg1788z00_1832;
																																																BgL_tagzd2164zd2_1560:
																																																	{	/* Eval/evaluate.scm 537 */
																																																		bool_t
																																																			BgL_test3279z00_6498;
																																																		if (PAIRP(BgL_lz00_1558))
																																																			{	/* Eval/evaluate.scm 537 */
																																																				bool_t
																																																					BgL_test3281z00_6501;
																																																				{	/* Eval/evaluate.scm 537 */
																																																					obj_t
																																																						BgL_tmpz00_6502;
																																																					BgL_tmpz00_6502
																																																						=
																																																						CDR
																																																						(BgL_lz00_1558);
																																																					BgL_test3281z00_6501
																																																						=
																																																						PAIRP
																																																						(BgL_tmpz00_6502);
																																																				}
																																																				if (BgL_test3281z00_6501)
																																																					{
																																																						obj_t
																																																							BgL_l1255z00_2626;
																																																						BgL_l1255z00_2626
																																																							=
																																																							BgL_lz00_1558;
																																																					BgL_zc3z04anonymousza32399ze3z87_2627:
																																																						if (NULLP(BgL_l1255z00_2626))
																																																							{	/* Eval/evaluate.scm 537 */
																																																								BgL_test3279z00_6498
																																																									=
																																																									(
																																																									(bool_t)
																																																									1);
																																																							}
																																																						else
																																																							{	/* Eval/evaluate.scm 537 */
																																																								bool_t
																																																									BgL_test3283z00_6507;
																																																								{	/* Eval/evaluate.scm 537 */
																																																									obj_t
																																																										BgL_tmpz00_6508;
																																																									BgL_tmpz00_6508
																																																										=
																																																										CAR
																																																										(
																																																										((obj_t) BgL_l1255z00_2626));
																																																									BgL_test3283z00_6507
																																																										=
																																																										SYMBOLP
																																																										(BgL_tmpz00_6508);
																																																								}
																																																								if (BgL_test3283z00_6507)
																																																									{
																																																										obj_t
																																																											BgL_l1255z00_6512;
																																																										BgL_l1255z00_6512
																																																											=
																																																											CDR
																																																											(
																																																											((obj_t) BgL_l1255z00_2626));
																																																										BgL_l1255z00_2626
																																																											=
																																																											BgL_l1255z00_6512;
																																																										goto
																																																											BgL_zc3z04anonymousza32399ze3z87_2627;
																																																									}
																																																								else
																																																									{	/* Eval/evaluate.scm 537 */
																																																										BgL_test3279z00_6498
																																																											=
																																																											(
																																																											(bool_t)
																																																											0);
																																																									}
																																																							}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 537 */
																																																						BgL_test3279z00_6498
																																																							=
																																																							(
																																																							(bool_t)
																																																							0);
																																																					}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 537 */
																																																				BgL_test3279z00_6498
																																																					=
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																		if (BgL_test3279z00_6498)
																																																			{	/* Eval/evaluate.scm 537 */
																																																				return
																																																					BGl_convzd2fieldzd2setz00zz__evaluatez00
																																																					(BgL_lz00_1558,
																																																					BgL_e2z00_1559,
																																																					BgL_ez00_69,
																																																					BgL_localsz00_70,
																																																					BgL_globalsz00_71,
																																																					BgL_tailzf3zf3_72,
																																																					BgL_wherez00_73,
																																																					BgL_locz00_74,
																																																					BgL_topzf3zf3_75);
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 537 */
																																																				return
																																																					BGl_evcompilezd2errorzd2zz__evcompilez00
																																																					(BgL_locz00_74,
																																																					BGl_string2906z00zz__evaluatez00,
																																																					BGl_string2981z00zz__evaluatez00,
																																																					BgL_ez00_69);
																																																			}
																																																	}
																																																}
																																															else
																																																{	/* Eval/evaluate.scm 424 */
																																																	return
																																																		BGl_evcompilezd2errorzd2zz__evcompilez00
																																																		(BgL_locz00_74,
																																																		BGl_string2906z00zz__evaluatez00,
																																																		BGl_string2981z00zz__evaluatez00,
																																																		BgL_ez00_69);
																																																}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																											else
																																												{	/* Eval/evaluate.scm 424 */
																																													obj_t
																																														BgL_cdrzd27413zd2_1835;
																																													BgL_cdrzd27413zd2_1835
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd27333zd2_1823));
																																													if (PAIRP(BgL_cdrzd27413zd2_1835))
																																														{	/* Eval/evaluate.scm 424 */
																																															if (NULLP(CDR(BgL_cdrzd27413zd2_1835)))
																																																{	/* Eval/evaluate.scm 424 */
																																																	obj_t
																																																		BgL_arg1793z00_1839;
																																																	obj_t
																																																		BgL_arg1794z00_1840;
																																																	BgL_arg1793z00_1839
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_cdrzd27333zd2_1823));
																																																	BgL_arg1794z00_1840
																																																		=
																																																		CAR
																																																		(BgL_cdrzd27413zd2_1835);
																																																	{
																																																		BgL_ev_hookz00_bglt
																																																			BgL_auxz00_6529;
																																																		{
																																																			obj_t
																																																				BgL_ez00_6531;
																																																			obj_t
																																																				BgL_vz00_6530;
																																																			BgL_vz00_6530
																																																				=
																																																				BgL_arg1793z00_1839;
																																																			BgL_ez00_6531
																																																				=
																																																				BgL_arg1794z00_1840;
																																																			BgL_ez00_1562
																																																				=
																																																				BgL_ez00_6531;
																																																			BgL_vz00_1561
																																																				=
																																																				BgL_vz00_6530;
																																																			goto
																																																				BgL_tagzd2165zd2_1563;
																																																		}
																																																		return
																																																			(
																																																			(obj_t)
																																																			BgL_auxz00_6529);
																																																	}
																																																}
																																															else
																																																{	/* Eval/evaluate.scm 424 */
																																																	return
																																																		BGl_evcompilezd2errorzd2zz__evcompilez00
																																																		(BgL_locz00_74,
																																																		BGl_string2906z00zz__evaluatez00,
																																																		BGl_string2981z00zz__evaluatez00,
																																																		BgL_ez00_69);
																																																}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_cdrzd27470zd2_1844;
																																								BgL_cdrzd27470zd2_1844
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								{	/* Eval/evaluate.scm 424 */
																																									obj_t
																																										BgL_cdrzd27474zd2_1845;
																																									BgL_cdrzd27474zd2_1845
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd27470zd2_1844));
																																									if (PAIRP(BgL_cdrzd27474zd2_1845))
																																										{	/* Eval/evaluate.scm 424 */
																																											if (NULLP(CDR(BgL_cdrzd27474zd2_1845)))
																																												{	/* Eval/evaluate.scm 424 */
																																													obj_t
																																														BgL_arg1801z00_1849;
																																													obj_t
																																														BgL_arg1802z00_1850;
																																													BgL_arg1801z00_1849
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd27470zd2_1844));
																																													BgL_arg1802z00_1850
																																														=
																																														CAR
																																														(BgL_cdrzd27474zd2_1845);
																																													{
																																														BgL_ev_hookz00_bglt
																																															BgL_auxz00_6547;
																																														{
																																															obj_t
																																																BgL_ez00_6549;
																																															obj_t
																																																BgL_vz00_6548;
																																															BgL_vz00_6548
																																																=
																																																BgL_arg1801z00_1849;
																																															BgL_ez00_6549
																																																=
																																																BgL_arg1802z00_1850;
																																															BgL_ez00_1562
																																																=
																																																BgL_ez00_6549;
																																															BgL_vz00_1561
																																																=
																																																BgL_vz00_6548;
																																															goto
																																																BgL_tagzd2165zd2_1563;
																																														}
																																														return
																																															(
																																															(obj_t)
																																															BgL_auxz00_6547);
																																													}
																																												}
																																											else
																																												{	/* Eval/evaluate.scm 424 */
																																													return
																																														BGl_evcompilezd2errorzd2zz__evcompilez00
																																														(BgL_locz00_74,
																																														BGl_string2906z00zz__evaluatez00,
																																														BGl_string2981z00zz__evaluatez00,
																																														BgL_ez00_69);
																																												}
																																										}
																																									else
																																										{	/* Eval/evaluate.scm 424 */
																																											return
																																												BGl_evcompilezd2errorzd2zz__evcompilez00
																																												(BgL_locz00_74,
																																												BGl_string2906z00zz__evaluatez00,
																																												BGl_string2981z00zz__evaluatez00,
																																												BgL_ez00_69);
																																										}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 424 */
																																						return
																																							BGl_evcompilezd2errorzd2zz__evcompilez00
																																							(BgL_locz00_74,
																																							BGl_string2906z00zz__evaluatez00,
																																							BGl_string2981z00zz__evaluatez00,
																																							BgL_ez00_69);
																																					}
																																			}
																																		else
																																			{	/* Eval/evaluate.scm 424 */
																																				obj_t
																																					BgL_cdrzd27595zd2_1852;
																																				BgL_cdrzd27595zd2_1852
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_ez00_69));
																																				if ((CAR
																																						(((obj_t) BgL_ez00_69)) == BGl_symbol3005z00zz__evaluatez00))
																																					{	/* Eval/evaluate.scm 424 */
																																						if (PAIRP(BgL_cdrzd27595zd2_1852))
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_cdrzd27600zd2_1856;
																																								BgL_cdrzd27600zd2_1856
																																									=
																																									CDR
																																									(BgL_cdrzd27595zd2_1852);
																																								if (PAIRP(BgL_cdrzd27600zd2_1856))
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_carzd27604zd2_1858;
																																										BgL_carzd27604zd2_1858
																																											=
																																											CAR
																																											(BgL_cdrzd27600zd2_1856);
																																										if (PAIRP(BgL_carzd27604zd2_1858))
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_cdrzd27609zd2_1860;
																																												BgL_cdrzd27609zd2_1860
																																													=
																																													CDR
																																													(BgL_carzd27604zd2_1858);
																																												if ((CAR(BgL_carzd27604zd2_1858) == BGl_symbol2970z00zz__evaluatez00))
																																													{	/* Eval/evaluate.scm 424 */
																																														if (PAIRP(BgL_cdrzd27609zd2_1860))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd27613zd2_1864;
																																																BgL_cdrzd27613zd2_1864
																																																	=
																																																	CDR
																																																	(BgL_cdrzd27609zd2_1860);
																																																if (PAIRP(BgL_cdrzd27613zd2_1864))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		if (NULLP(CDR(BgL_cdrzd27613zd2_1864)))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				if (NULLP(CDR(BgL_cdrzd27600zd2_1856)))
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1817z00_1870;
																																																						obj_t
																																																							BgL_arg1818z00_1871;
																																																						obj_t
																																																							BgL_arg1819z00_1872;
																																																						BgL_arg1817z00_1870
																																																							=
																																																							CAR
																																																							(BgL_cdrzd27595zd2_1852);
																																																						BgL_arg1818z00_1871
																																																							=
																																																							CAR
																																																							(BgL_cdrzd27609zd2_1860);
																																																						BgL_arg1819z00_1872
																																																							=
																																																							CAR
																																																							(BgL_cdrzd27613zd2_1864);
																																																						{
																																																							BgL_ev_defglobalz00_bglt
																																																								BgL_auxz00_6586;
																																																							BgL_gvz00_1565
																																																								=
																																																								BgL_arg1817z00_1870;
																																																							BgL_formalsz00_1566
																																																								=
																																																								BgL_arg1818z00_1871;
																																																							BgL_bodyz00_1567
																																																								=
																																																								BgL_arg1819z00_1872;
																																																						BgL_tagzd2167zd2_1568:
																																																							{	/* Eval/evaluate.scm 558 */
																																																								obj_t
																																																									BgL_tidz00_2650;
																																																								BgL_tidz00_2650
																																																									=
																																																									BGl_untypezd2identzd2zz__evaluatez00
																																																									(BgL_gvz00_1565);
																																																								{	/* Eval/evaluate.scm 559 */
																																																									BgL_ev_defglobalz00_bglt
																																																										BgL_new1149z00_2651;
																																																									{	/* Eval/evaluate.scm 563 */
																																																										BgL_ev_defglobalz00_bglt
																																																											BgL_new1148z00_2654;
																																																										BgL_new1148z00_2654
																																																											=
																																																											(
																																																											(BgL_ev_defglobalz00_bglt)
																																																											BOBJECT
																																																											(GC_MALLOC
																																																												(sizeof
																																																													(struct
																																																														BgL_ev_defglobalz00_bgl))));
																																																										{	/* Eval/evaluate.scm 563 */
																																																											long
																																																												BgL_arg2417z00_2655;
																																																											BgL_arg2417z00_2655
																																																												=
																																																												BGL_CLASS_NUM
																																																												(BGl_ev_defglobalz00zz__evaluate_typesz00);
																																																											BGL_OBJECT_CLASS_NUM_SET
																																																												(
																																																												((BgL_objectz00_bglt) BgL_new1148z00_2654), BgL_arg2417z00_2655);
																																																										}
																																																										BgL_new1149z00_2651
																																																											=
																																																											BgL_new1148z00_2654;
																																																									}
																																																									{
																																																										BgL_ev_exprz00_bglt
																																																											BgL_auxz00_6592;
																																																										{	/* Eval/evaluate.scm 563 */
																																																											obj_t
																																																												BgL_arg2415z00_2652;
																																																											BgL_arg2415z00_2652
																																																												=
																																																												CDR
																																																												(BgL_tidz00_2650);
																																																											BgL_auxz00_6592
																																																												=
																																																												(
																																																												(BgL_ev_exprz00_bglt)
																																																												BGl_convzd2lambdaze70z35zz__evaluatez00
																																																												(BgL_globalsz00_71,
																																																													BgL_localsz00_70,
																																																													BgL_ez00_69,
																																																													BgL_locz00_74,
																																																													BgL_formalsz00_1566,
																																																													BgL_bodyz00_1567,
																																																													BgL_gvz00_1565,
																																																													BgL_arg2415z00_2652));
																																																										}
																																																										((((BgL_ev_hookz00_bglt) COBJECT(((BgL_ev_hookz00_bglt) BgL_new1149z00_2651)))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6592), BUNSPEC);
																																																									}
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt) BgL_new1149z00_2651)))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt) BgL_new1149z00_2651)))->BgL_namez00) = ((obj_t) CAR(BgL_tidz00_2650)), BUNSPEC);
																																																									{
																																																										obj_t
																																																											BgL_auxz00_6603;
																																																										if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_globalsz00_71))
																																																											{	/* Eval/evaluate.scm 562 */
																																																												BgL_auxz00_6603
																																																													=
																																																													BgL_globalsz00_71;
																																																											}
																																																										else
																																																											{	/* Eval/evaluate.scm 562 */
																																																												BgL_auxz00_6603
																																																													=
																																																													BGL_MODULE
																																																													();
																																																											}
																																																										((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt) BgL_new1149z00_2651)))->BgL_modz00) = ((obj_t) BgL_auxz00_6603), BUNSPEC);
																																																									}
																																																									BgL_auxz00_6586
																																																										=
																																																										BgL_new1149z00_2651;
																																																								}
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_auxz00_6586);
																																																						}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1820z00_1873;
																																																						BgL_arg1820z00_1873
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_ez00_69));
																																																						{
																																																							BgL_ev_appz00_bglt
																																																								BgL_auxz00_6612;
																																																							{
																																																								obj_t
																																																									BgL_argsz00_6614;
																																																								obj_t
																																																									BgL_fz00_6613;
																																																								BgL_fz00_6613
																																																									=
																																																									BgL_arg1820z00_1873;
																																																								BgL_argsz00_6614
																																																									=
																																																									BgL_cdrzd27595zd2_1852;
																																																								BgL_argsz00_1593
																																																									=
																																																									BgL_argsz00_6614;
																																																								BgL_fz00_1592
																																																									=
																																																									BgL_fz00_6613;
																																																								goto
																																																									BgL_tagzd2176zd2_1594;
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_auxz00_6612);
																																																						}
																																																					}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_cdrzd27715zd2_1877;
																																																				BgL_cdrzd27715zd2_1877
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd27595zd2_1852));
																																																				if (NULLP(CDR(((obj_t) BgL_cdrzd27715zd2_1877))))
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1828z00_1880;
																																																						obj_t
																																																							BgL_arg1829z00_1881;
																																																						BgL_arg1828z00_1880
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_cdrzd27595zd2_1852));
																																																						BgL_arg1829z00_1881
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_cdrzd27715zd2_1877));
																																																						{
																																																							BgL_ev_defglobalz00_bglt
																																																								BgL_auxz00_6626;
																																																							BgL_gvz00_1569
																																																								=
																																																								BgL_arg1828z00_1880;
																																																							BgL_gez00_1570
																																																								=
																																																								BgL_arg1829z00_1881;
																																																						BgL_tagzd2168zd2_1571:
																																																							{	/* Eval/evaluate.scm 565 */
																																																								obj_t
																																																									BgL_tidz00_2656;
																																																								BgL_tidz00_2656
																																																									=
																																																									BGl_untypezd2identzd2zz__evaluatez00
																																																									(BgL_gvz00_1569);
																																																								{	/* Eval/evaluate.scm 566 */
																																																									BgL_ev_defglobalz00_bglt
																																																										BgL_new1151z00_2657;
																																																									{	/* Eval/evaluate.scm 570 */
																																																										BgL_ev_defglobalz00_bglt
																																																											BgL_new1150z00_2662;
																																																										BgL_new1150z00_2662
																																																											=
																																																											(
																																																											(BgL_ev_defglobalz00_bglt)
																																																											BOBJECT
																																																											(GC_MALLOC
																																																												(sizeof
																																																													(struct
																																																														BgL_ev_defglobalz00_bgl))));
																																																										{	/* Eval/evaluate.scm 570 */
																																																											long
																																																												BgL_arg2423z00_2663;
																																																											BgL_arg2423z00_2663
																																																												=
																																																												BGL_CLASS_NUM
																																																												(BGl_ev_defglobalz00zz__evaluate_typesz00);
																																																											BGL_OBJECT_CLASS_NUM_SET
																																																												(
																																																												((BgL_objectz00_bglt) BgL_new1150z00_2662), BgL_arg2423z00_2663);
																																																										}
																																																										BgL_new1151z00_2657
																																																											=
																																																											BgL_new1150z00_2662;
																																																									}
																																																									{
																																																										BgL_ev_exprz00_bglt
																																																											BgL_auxz00_6632;
																																																										{	/* Eval/evaluate.scm 571 */
																																																											obj_t
																																																												BgL_arg2418z00_2658;
																																																											{	/* Eval/evaluate.scm 571 */
																																																												obj_t
																																																													BgL_arg2420z00_2660;
																																																												BgL_arg2420z00_2660
																																																													=
																																																													CDR
																																																													(BgL_tidz00_2656);
																																																												BgL_arg2418z00_2658
																																																													=
																																																													BGl_typezd2resultzd2zz__evaluatez00
																																																													(BgL_arg2420z00_2660,
																																																													BgL_gez00_1570,
																																																													BgL_locz00_74);
																																																											}
																																																											{	/* Eval/evaluate.scm 383 */
																																																												obj_t
																																																													BgL_arg2449z00_3877;
																																																												{	/* Eval/evaluate.scm 144 */
																																																													obj_t
																																																														BgL__ortest_1069z00_3878;
																																																													BgL__ortest_1069z00_3878
																																																														=
																																																														BGl_getzd2sourcezd2locationz00zz__readerz00
																																																														(BgL_arg2418z00_2658);
																																																													if (CBOOL(BgL__ortest_1069z00_3878))
																																																														{	/* Eval/evaluate.scm 144 */
																																																															BgL_arg2449z00_3877
																																																																=
																																																																BgL__ortest_1069z00_3878;
																																																														}
																																																													else
																																																														{	/* Eval/evaluate.scm 144 */
																																																															BgL_arg2449z00_3877
																																																																=
																																																																BgL_locz00_74;
																																																														}
																																																												}
																																																												{
																																																													obj_t
																																																														BgL_auxz00_6639;
																																																													{	/* Eval/evaluate.scm 374 */
																																																														obj_t
																																																															BgL_auxz00_6640;
																																																														if (BgL_topzf3zf3_75)
																																																															{	/* Eval/evaluate.scm 572 */
																																																																BgL_auxz00_6640
																																																																	=
																																																																	BgL_gvz00_1569;
																																																															}
																																																														else
																																																															{	/* Eval/evaluate.scm 572 */
																																																																BgL_auxz00_6640
																																																																	=
																																																																	BgL_wherez00_73;
																																																															}
																																																														BgL_auxz00_6639
																																																															=
																																																															BGl_convz00zz__evaluatez00
																																																															(BgL_arg2418z00_2658,
																																																															BgL_localsz00_70,
																																																															BgL_globalsz00_71,
																																																															BFALSE,
																																																															BgL_auxz00_6640,
																																																															BgL_arg2449z00_3877,
																																																															((bool_t) 0));
																																																													}
																																																													BgL_auxz00_6632
																																																														=
																																																														(
																																																														(BgL_ev_exprz00_bglt)
																																																														BgL_auxz00_6639);
																																																												}
																																																											}
																																																										}
																																																										((((BgL_ev_hookz00_bglt) COBJECT(((BgL_ev_hookz00_bglt) BgL_new1151z00_2657)))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6632), BUNSPEC);
																																																									}
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt) BgL_new1151z00_2657)))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																									((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt) BgL_new1151z00_2657)))->BgL_namez00) = ((obj_t) CAR(BgL_tidz00_2656)), BUNSPEC);
																																																									{
																																																										obj_t
																																																											BgL_auxz00_6650;
																																																										if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_globalsz00_71))
																																																											{	/* Eval/evaluate.scm 569 */
																																																												BgL_auxz00_6650
																																																													=
																																																													BgL_globalsz00_71;
																																																											}
																																																										else
																																																											{	/* Eval/evaluate.scm 569 */
																																																												BgL_auxz00_6650
																																																													=
																																																													BGL_MODULE
																																																													();
																																																											}
																																																										((((BgL_ev_setglobalz00_bglt) COBJECT(((BgL_ev_setglobalz00_bglt) BgL_new1151z00_2657)))->BgL_modz00) = ((obj_t) BgL_auxz00_6650), BUNSPEC);
																																																									}
																																																									BgL_auxz00_6626
																																																										=
																																																										BgL_new1151z00_2657;
																																																								}
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_auxz00_6626);
																																																						}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1831z00_1882;
																																																						obj_t
																																																							BgL_arg1832z00_1883;
																																																						BgL_arg1831z00_1882
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_ez00_69));
																																																						BgL_arg1832z00_1883
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_ez00_69));
																																																						{
																																																							BgL_ev_appz00_bglt
																																																								BgL_auxz00_6661;
																																																							{
																																																								obj_t
																																																									BgL_argsz00_6663;
																																																								obj_t
																																																									BgL_fz00_6662;
																																																								BgL_fz00_6662
																																																									=
																																																									BgL_arg1831z00_1882;
																																																								BgL_argsz00_6663
																																																									=
																																																									BgL_arg1832z00_1883;
																																																								BgL_argsz00_1593
																																																									=
																																																									BgL_argsz00_6663;
																																																								BgL_fz00_1592
																																																									=
																																																									BgL_fz00_6662;
																																																								goto
																																																									BgL_tagzd2176zd2_1594;
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_auxz00_6661);
																																																						}
																																																					}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_cdrzd27804zd2_1887;
																																																		BgL_cdrzd27804zd2_1887
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd27595zd2_1852));
																																																		if (NULLP(CDR(((obj_t) BgL_cdrzd27804zd2_1887))))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg1837z00_1890;
																																																				obj_t
																																																					BgL_arg1838z00_1891;
																																																				BgL_arg1837z00_1890
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd27595zd2_1852));
																																																				BgL_arg1838z00_1891
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd27804zd2_1887));
																																																				{
																																																					BgL_ev_defglobalz00_bglt
																																																						BgL_auxz00_6675;
																																																					{
																																																						obj_t
																																																							BgL_gez00_6677;
																																																						obj_t
																																																							BgL_gvz00_6676;
																																																						BgL_gvz00_6676
																																																							=
																																																							BgL_arg1837z00_1890;
																																																						BgL_gez00_6677
																																																							=
																																																							BgL_arg1838z00_1891;
																																																						BgL_gez00_1570
																																																							=
																																																							BgL_gez00_6677;
																																																						BgL_gvz00_1569
																																																							=
																																																							BgL_gvz00_6676;
																																																						goto
																																																							BgL_tagzd2168zd2_1571;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_6675);
																																																				}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg1839z00_1892;
																																																				obj_t
																																																					BgL_arg1840z00_1893;
																																																				BgL_arg1839z00_1892
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				BgL_arg1840z00_1893
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				{
																																																					BgL_ev_appz00_bglt
																																																						BgL_auxz00_6683;
																																																					{
																																																						obj_t
																																																							BgL_argsz00_6685;
																																																						obj_t
																																																							BgL_fz00_6684;
																																																						BgL_fz00_6684
																																																							=
																																																							BgL_arg1839z00_1892;
																																																						BgL_argsz00_6685
																																																							=
																																																							BgL_arg1840z00_1893;
																																																						BgL_argsz00_1593
																																																							=
																																																							BgL_argsz00_6685;
																																																						BgL_fz00_1592
																																																							=
																																																							BgL_fz00_6684;
																																																						goto
																																																							BgL_tagzd2176zd2_1594;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_6683);
																																																				}
																																																			}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd27893zd2_1896;
																																																BgL_cdrzd27893zd2_1896
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd27595zd2_1852));
																																																if (NULLP(CDR(((obj_t) BgL_cdrzd27893zd2_1896))))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg1845z00_1899;
																																																		obj_t
																																																			BgL_arg1846z00_1900;
																																																		BgL_arg1845z00_1899
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd27595zd2_1852));
																																																		BgL_arg1846z00_1900
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd27893zd2_1896));
																																																		{
																																																			BgL_ev_defglobalz00_bglt
																																																				BgL_auxz00_6697;
																																																			{
																																																				obj_t
																																																					BgL_gez00_6699;
																																																				obj_t
																																																					BgL_gvz00_6698;
																																																				BgL_gvz00_6698
																																																					=
																																																					BgL_arg1845z00_1899;
																																																				BgL_gez00_6699
																																																					=
																																																					BgL_arg1846z00_1900;
																																																				BgL_gez00_1570
																																																					=
																																																					BgL_gez00_6699;
																																																				BgL_gvz00_1569
																																																					=
																																																					BgL_gvz00_6698;
																																																				goto
																																																					BgL_tagzd2168zd2_1571;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_6697);
																																																		}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg1847z00_1901;
																																																		obj_t
																																																			BgL_arg1848z00_1902;
																																																		BgL_arg1847z00_1901
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		BgL_arg1848z00_1902
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{
																																																			BgL_ev_appz00_bglt
																																																				BgL_auxz00_6705;
																																																			{
																																																				obj_t
																																																					BgL_argsz00_6707;
																																																				obj_t
																																																					BgL_fz00_6706;
																																																				BgL_fz00_6706
																																																					=
																																																					BgL_arg1847z00_1901;
																																																				BgL_argsz00_6707
																																																					=
																																																					BgL_arg1848z00_1902;
																																																				BgL_argsz00_1593
																																																					=
																																																					BgL_argsz00_6707;
																																																				BgL_fz00_1592
																																																					=
																																																					BgL_fz00_6706;
																																																				goto
																																																					BgL_tagzd2176zd2_1594;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_6705);
																																																		}
																																																	}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_cdrzd27982zd2_1905;
																																														BgL_cdrzd27982zd2_1905
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd27595zd2_1852));
																																														if (NULLP(CDR(((obj_t) BgL_cdrzd27982zd2_1905))))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg1852z00_1908;
																																																obj_t
																																																	BgL_arg1853z00_1909;
																																																BgL_arg1852z00_1908
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd27595zd2_1852));
																																																BgL_arg1853z00_1909
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd27982zd2_1905));
																																																{
																																																	BgL_ev_defglobalz00_bglt
																																																		BgL_auxz00_6719;
																																																	{
																																																		obj_t
																																																			BgL_gez00_6721;
																																																		obj_t
																																																			BgL_gvz00_6720;
																																																		BgL_gvz00_6720
																																																			=
																																																			BgL_arg1852z00_1908;
																																																		BgL_gez00_6721
																																																			=
																																																			BgL_arg1853z00_1909;
																																																		BgL_gez00_1570
																																																			=
																																																			BgL_gez00_6721;
																																																		BgL_gvz00_1569
																																																			=
																																																			BgL_gvz00_6720;
																																																		goto
																																																			BgL_tagzd2168zd2_1571;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_6719);
																																																}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg1854z00_1910;
																																																obj_t
																																																	BgL_arg1856z00_1911;
																																																BgL_arg1854z00_1910
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																BgL_arg1856z00_1911
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{
																																																	BgL_ev_appz00_bglt
																																																		BgL_auxz00_6727;
																																																	{
																																																		obj_t
																																																			BgL_argsz00_6729;
																																																		obj_t
																																																			BgL_fz00_6728;
																																																		BgL_fz00_6728
																																																			=
																																																			BgL_arg1854z00_1910;
																																																		BgL_argsz00_6729
																																																			=
																																																			BgL_arg1856z00_1911;
																																																		BgL_argsz00_1593
																																																			=
																																																			BgL_argsz00_6729;
																																																		BgL_fz00_1592
																																																			=
																																																			BgL_fz00_6728;
																																																		goto
																																																			BgL_tagzd2176zd2_1594;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_6727);
																																																}
																																															}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_cdrzd28071zd2_1915;
																																												BgL_cdrzd28071zd2_1915
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd27595zd2_1852));
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd28071zd2_1915))))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg1862z00_1918;
																																														obj_t
																																															BgL_arg1863z00_1919;
																																														BgL_arg1862z00_1918
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd27595zd2_1852));
																																														BgL_arg1863z00_1919
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd28071zd2_1915));
																																														{
																																															BgL_ev_defglobalz00_bglt
																																																BgL_auxz00_6741;
																																															{
																																																obj_t
																																																	BgL_gez00_6743;
																																																obj_t
																																																	BgL_gvz00_6742;
																																																BgL_gvz00_6742
																																																	=
																																																	BgL_arg1862z00_1918;
																																																BgL_gez00_6743
																																																	=
																																																	BgL_arg1863z00_1919;
																																																BgL_gez00_1570
																																																	=
																																																	BgL_gez00_6743;
																																																BgL_gvz00_1569
																																																	=
																																																	BgL_gvz00_6742;
																																																goto
																																																	BgL_tagzd2168zd2_1571;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_6741);
																																														}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg1864z00_1920;
																																														obj_t
																																															BgL_arg1866z00_1921;
																																														BgL_arg1864z00_1920
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														BgL_arg1866z00_1921
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{
																																															BgL_ev_appz00_bglt
																																																BgL_auxz00_6749;
																																															{
																																																obj_t
																																																	BgL_argsz00_6751;
																																																obj_t
																																																	BgL_fz00_6750;
																																																BgL_fz00_6750
																																																	=
																																																	BgL_arg1864z00_1920;
																																																BgL_argsz00_6751
																																																	=
																																																	BgL_arg1866z00_1921;
																																																BgL_argsz00_1593
																																																	=
																																																	BgL_argsz00_6751;
																																																BgL_fz00_1592
																																																	=
																																																	BgL_fz00_6750;
																																																goto
																																																	BgL_tagzd2176zd2_1594;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_6749);
																																														}
																																													}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_arg1869z00_1923;
																																										BgL_arg1869z00_1923
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										{
																																											BgL_ev_appz00_bglt
																																												BgL_auxz00_6755;
																																											{
																																												obj_t
																																													BgL_argsz00_6757;
																																												obj_t
																																													BgL_fz00_6756;
																																												BgL_fz00_6756
																																													=
																																													BgL_arg1869z00_1923;
																																												BgL_argsz00_6757
																																													=
																																													BgL_cdrzd27595zd2_1852;
																																												BgL_argsz00_1593
																																													=
																																													BgL_argsz00_6757;
																																												BgL_fz00_1592
																																													=
																																													BgL_fz00_6756;
																																												goto
																																													BgL_tagzd2176zd2_1594;
																																											}
																																											return
																																												(
																																												(obj_t)
																																												BgL_auxz00_6755);
																																										}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_arg1872z00_1925;
																																								BgL_arg1872z00_1925
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								{
																																									BgL_ev_appz00_bglt
																																										BgL_auxz00_6761;
																																									{
																																										obj_t
																																											BgL_argsz00_6763;
																																										obj_t
																																											BgL_fz00_6762;
																																										BgL_fz00_6762
																																											=
																																											BgL_arg1872z00_1925;
																																										BgL_argsz00_6763
																																											=
																																											BgL_cdrzd27595zd2_1852;
																																										BgL_argsz00_1593
																																											=
																																											BgL_argsz00_6763;
																																										BgL_fz00_1592
																																											=
																																											BgL_fz00_6762;
																																										goto
																																											BgL_tagzd2176zd2_1594;
																																									}
																																									return
																																										(
																																										(obj_t)
																																										BgL_auxz00_6761);
																																								}
																																							}
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 424 */
																																						if (
																																							(CAR
																																								(((obj_t) BgL_ez00_69)) == BGl_symbol3007z00zz__evaluatez00))
																																							{	/* Eval/evaluate.scm 424 */
																																								if (PAIRP(BgL_cdrzd27595zd2_1852))
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_carzd28319zd2_1931;
																																										BgL_carzd28319zd2_1931
																																											=
																																											CAR
																																											(BgL_cdrzd27595zd2_1852);
																																										if (PAIRP(BgL_carzd28319zd2_1931))
																																											{	/* Eval/evaluate.scm 424 */
																																												if (NULLP(CDR(BgL_carzd28319zd2_1931)))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg1880z00_1935;
																																														obj_t
																																															BgL_arg1882z00_1936;
																																														BgL_arg1880z00_1935
																																															=
																																															CAR
																																															(BgL_carzd28319zd2_1931);
																																														BgL_arg1882z00_1936
																																															=
																																															CDR
																																															(BgL_cdrzd27595zd2_1852);
																																														{
																																															BgL_ev_bindzd2exitzd2_bglt
																																																BgL_auxz00_6779;
																																															BgL_vz00_1572
																																																=
																																																BgL_arg1880z00_1935;
																																															BgL_bodyz00_1573
																																																=
																																																BgL_arg1882z00_1936;
																																														BgL_tagzd2169zd2_1574:
																																															{	/* Eval/evaluate.scm 574 */
																																																BgL_ev_varz00_bglt
																																																	BgL_varz00_2664;
																																																{	/* Eval/evaluate.scm 574 */
																																																	BgL_ev_varz00_bglt
																																																		BgL_new1153z00_2669;
																																																	{	/* Eval/evaluate.scm 574 */
																																																		BgL_ev_varz00_bglt
																																																			BgL_new1152z00_2670;
																																																		BgL_new1152z00_2670
																																																			=
																																																			(
																																																			(BgL_ev_varz00_bglt)
																																																			BOBJECT
																																																			(GC_MALLOC
																																																				(sizeof
																																																					(struct
																																																						BgL_ev_varz00_bgl))));
																																																		{	/* Eval/evaluate.scm 574 */
																																																			long
																																																				BgL_arg2426z00_2671;
																																																			BgL_arg2426z00_2671
																																																				=
																																																				BGL_CLASS_NUM
																																																				(BGl_ev_varz00zz__evaluate_typesz00);
																																																			BGL_OBJECT_CLASS_NUM_SET
																																																				(
																																																				((BgL_objectz00_bglt) BgL_new1152z00_2670), BgL_arg2426z00_2671);
																																																		}
																																																		BgL_new1153z00_2669
																																																			=
																																																			BgL_new1152z00_2670;
																																																	}
																																																	((((BgL_ev_varz00_bglt) COBJECT(BgL_new1153z00_2669))->BgL_namez00) = ((obj_t) BgL_vz00_1572), BUNSPEC);
																																																	((((BgL_ev_varz00_bglt) COBJECT(BgL_new1153z00_2669))->BgL_effz00) = ((obj_t) BFALSE), BUNSPEC);
																																																	((((BgL_ev_varz00_bglt) COBJECT(BgL_new1153z00_2669))->BgL_typez00) = ((obj_t) BFALSE), BUNSPEC);
																																																	BgL_varz00_2664
																																																		=
																																																		BgL_new1153z00_2669;
																																																}
																																																{	/* Eval/evaluate.scm 575 */
																																																	BgL_ev_bindzd2exitzd2_bglt
																																																		BgL_new1156z00_2665;
																																																	{	/* Eval/evaluate.scm 576 */
																																																		BgL_ev_bindzd2exitzd2_bglt
																																																			BgL_new1154z00_2667;
																																																		BgL_new1154z00_2667
																																																			=
																																																			(
																																																			(BgL_ev_bindzd2exitzd2_bglt)
																																																			BOBJECT
																																																			(GC_MALLOC
																																																				(sizeof
																																																					(struct
																																																						BgL_ev_bindzd2exitzd2_bgl))));
																																																		{	/* Eval/evaluate.scm 576 */
																																																			long
																																																				BgL_arg2425z00_2668;
																																																			BgL_arg2425z00_2668
																																																				=
																																																				BGL_CLASS_NUM
																																																				(BGl_ev_bindzd2exitzd2zz__evaluate_typesz00);
																																																			BGL_OBJECT_CLASS_NUM_SET
																																																				(
																																																				((BgL_objectz00_bglt) BgL_new1154z00_2667), BgL_arg2425z00_2668);
																																																		}
																																																		BgL_new1156z00_2665
																																																			=
																																																			BgL_new1154z00_2667;
																																																	}
																																																	((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(BgL_new1156z00_2665))->BgL_varz00) = ((BgL_ev_varz00_bglt) BgL_varz00_2664), BUNSPEC);
																																																	{
																																																		BgL_ev_exprz00_bglt
																																																			BgL_auxz00_6792;
																																																		{	/* Eval/evaluate.scm 577 */
																																																			obj_t
																																																				BgL_arg2424z00_2666;
																																																			BgL_arg2424z00_2666
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(
																																																				((obj_t) BgL_varz00_2664), BgL_localsz00_70);
																																																			BgL_auxz00_6792
																																																				=
																																																				(
																																																				(BgL_ev_exprz00_bglt)
																																																				BGl_convzd2beginzd2zz__evaluatez00
																																																				(BgL_bodyz00_1573,
																																																					BgL_arg2424z00_2666,
																																																					BgL_globalsz00_71,
																																																					BFALSE,
																																																					BgL_wherez00_73,
																																																					BgL_locz00_74,
																																																					((bool_t) 0)));
																																																		}
																																																		((((BgL_ev_bindzd2exitzd2_bglt) COBJECT(BgL_new1156z00_2665))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6792), BUNSPEC);
																																																	}
																																																	BgL_auxz00_6779
																																																		=
																																																		BgL_new1156z00_2665;
																																															}}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_6779);
																																														}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg1883z00_1937;
																																														obj_t
																																															BgL_arg1884z00_1938;
																																														BgL_arg1883z00_1937
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														BgL_arg1884z00_1938
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{
																																															BgL_ev_appz00_bglt
																																																BgL_auxz00_6803;
																																															{
																																																obj_t
																																																	BgL_argsz00_6805;
																																																obj_t
																																																	BgL_fz00_6804;
																																																BgL_fz00_6804
																																																	=
																																																	BgL_arg1883z00_1937;
																																																BgL_argsz00_6805
																																																	=
																																																	BgL_arg1884z00_1938;
																																																BgL_argsz00_1593
																																																	=
																																																	BgL_argsz00_6805;
																																																BgL_fz00_1592
																																																	=
																																																	BgL_fz00_6804;
																																																goto
																																																	BgL_tagzd2176zd2_1594;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_6803);
																																														}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg1887z00_1940;
																																												obj_t
																																													BgL_arg1888z00_1941;
																																												BgL_arg1887z00_1940
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												BgL_arg1888z00_1941
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{
																																													BgL_ev_appz00_bglt
																																														BgL_auxz00_6811;
																																													{
																																														obj_t
																																															BgL_argsz00_6813;
																																														obj_t
																																															BgL_fz00_6812;
																																														BgL_fz00_6812
																																															=
																																															BgL_arg1887z00_1940;
																																														BgL_argsz00_6813
																																															=
																																															BgL_arg1888z00_1941;
																																														BgL_argsz00_1593
																																															=
																																															BgL_argsz00_6813;
																																														BgL_fz00_1592
																																															=
																																															BgL_fz00_6812;
																																														goto
																																															BgL_tagzd2176zd2_1594;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_6811);
																																												}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_arg1889z00_1942;
																																										obj_t
																																											BgL_arg1890z00_1943;
																																										BgL_arg1889z00_1942
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										BgL_arg1890z00_1943
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										{
																																											BgL_ev_appz00_bglt
																																												BgL_auxz00_6819;
																																											{
																																												obj_t
																																													BgL_argsz00_6821;
																																												obj_t
																																													BgL_fz00_6820;
																																												BgL_fz00_6820
																																													=
																																													BgL_arg1889z00_1942;
																																												BgL_argsz00_6821
																																													=
																																													BgL_arg1890z00_1943;
																																												BgL_argsz00_1593
																																													=
																																													BgL_argsz00_6821;
																																												BgL_fz00_1592
																																													=
																																													BgL_fz00_6820;
																																												goto
																																													BgL_tagzd2176zd2_1594;
																																											}
																																											return
																																												(
																																												(obj_t)
																																												BgL_auxz00_6819);
																																										}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_cdrzd28490zd2_1944;
																																								BgL_cdrzd28490zd2_1944
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3009z00zz__evaluatez00))
																																									{	/* Eval/evaluate.scm 424 */
																																										if (PAIRP(BgL_cdrzd28490zd2_1944))
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg1894z00_1948;
																																												obj_t
																																													BgL_arg1896z00_1949;
																																												BgL_arg1894z00_1948
																																													=
																																													CAR
																																													(BgL_cdrzd28490zd2_1944);
																																												BgL_arg1896z00_1949
																																													=
																																													CDR
																																													(BgL_cdrzd28490zd2_1944);
																																												{	/* Eval/evaluate.scm 579 */
																																													BgL_ev_unwindzd2protectzd2_bglt
																																														BgL_new1158z00_4197;
																																													{	/* Eval/evaluate.scm 580 */
																																														BgL_ev_unwindzd2protectzd2_bglt
																																															BgL_new1157z00_4198;
																																														BgL_new1157z00_4198
																																															=
																																															(
																																															(BgL_ev_unwindzd2protectzd2_bglt)
																																															BOBJECT
																																															(GC_MALLOC
																																																(sizeof
																																																	(struct
																																																		BgL_ev_unwindzd2protectzd2_bgl))));
																																														{	/* Eval/evaluate.scm 580 */
																																															long
																																																BgL_arg2428z00_4199;
																																															BgL_arg2428z00_4199
																																																=
																																																BGL_CLASS_NUM
																																																(BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00);
																																															BGL_OBJECT_CLASS_NUM_SET
																																																(
																																																((BgL_objectz00_bglt) BgL_new1157z00_4198), BgL_arg2428z00_4199);
																																														}
																																														BgL_new1158z00_4197
																																															=
																																															BgL_new1157z00_4198;
																																													}
																																													{
																																														BgL_ev_exprz00_bglt
																																															BgL_auxz00_6837;
																																														{	/* Eval/evaluate.scm 380 */
																																															obj_t
																																																BgL_arg2446z00_4203;
																																															BgL_arg2446z00_4203
																																																=
																																																BGl_getzd2locationzd2zz__evaluatez00
																																																(BgL_arg1894z00_1948,
																																																BgL_locz00_74);
																																															BgL_auxz00_6837
																																																=
																																																(
																																																(BgL_ev_exprz00_bglt)
																																																BGl_uconvzf2locze70z15zz__evaluatez00
																																																(BgL_wherez00_73,
																																																	BgL_globalsz00_71,
																																																	BgL_localsz00_70,
																																																	BgL_arg1894z00_1948,
																																																	BgL_arg2446z00_4203));
																																														}
																																														((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(BgL_new1158z00_4197))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6837), BUNSPEC);
																																													}
																																													((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(BgL_new1158z00_4197))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_arg1896z00_1949, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																													return
																																														(
																																														(obj_t)
																																														BgL_new1158z00_4197);
																																												}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg1897z00_1950;
																																												BgL_arg1897z00_1950
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{
																																													BgL_ev_appz00_bglt
																																														BgL_auxz00_6848;
																																													{
																																														obj_t
																																															BgL_argsz00_6850;
																																														obj_t
																																															BgL_fz00_6849;
																																														BgL_fz00_6849
																																															=
																																															BgL_arg1897z00_1950;
																																														BgL_argsz00_6850
																																															=
																																															BgL_cdrzd28490zd2_1944;
																																														BgL_argsz00_1593
																																															=
																																															BgL_argsz00_6850;
																																														BgL_fz00_1592
																																															=
																																															BgL_fz00_6849;
																																														goto
																																															BgL_tagzd2176zd2_1594;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_6848);
																																												}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3011z00zz__evaluatez00))
																																											{	/* Eval/evaluate.scm 424 */
																																												if (PAIRP(BgL_cdrzd28490zd2_1944))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg1902z00_1956;
																																														obj_t
																																															BgL_arg1903z00_1957;
																																														BgL_arg1902z00_1956
																																															=
																																															CAR
																																															(BgL_cdrzd28490zd2_1944);
																																														BgL_arg1903z00_1957
																																															=
																																															CDR
																																															(BgL_cdrzd28490zd2_1944);
																																														{	/* Eval/evaluate.scm 583 */
																																															BgL_ev_withzd2handlerzd2_bglt
																																																BgL_new1160z00_4210;
																																															{	/* Eval/evaluate.scm 584 */
																																																BgL_ev_withzd2handlerzd2_bglt
																																																	BgL_new1159z00_4211;
																																																BgL_new1159z00_4211
																																																	=
																																																	(
																																																	(BgL_ev_withzd2handlerzd2_bglt)
																																																	BOBJECT
																																																	(GC_MALLOC
																																																		(sizeof
																																																			(struct
																																																				BgL_ev_withzd2handlerzd2_bgl))));
																																																{	/* Eval/evaluate.scm 584 */
																																																	long
																																																		BgL_arg2429z00_4212;
																																																	BgL_arg2429z00_4212
																																																		=
																																																		BGL_CLASS_NUM
																																																		(BGl_ev_withzd2handlerzd2zz__evaluate_typesz00);
																																																	BGL_OBJECT_CLASS_NUM_SET
																																																		(
																																																		((BgL_objectz00_bglt) BgL_new1159z00_4211), BgL_arg2429z00_4212);
																																																}
																																																BgL_new1160z00_4210
																																																	=
																																																	BgL_new1159z00_4211;
																																															}
																																															{
																																																BgL_ev_exprz00_bglt
																																																	BgL_auxz00_6864;
																																																{	/* Eval/evaluate.scm 380 */
																																																	obj_t
																																																		BgL_arg2446z00_4216;
																																																	BgL_arg2446z00_4216
																																																		=
																																																		BGl_getzd2locationzd2zz__evaluatez00
																																																		(BgL_arg1902z00_1956,
																																																		BgL_locz00_74);
																																																	BgL_auxz00_6864
																																																		=
																																																		(
																																																		(BgL_ev_exprz00_bglt)
																																																		BGl_uconvzf2locze70z15zz__evaluatez00
																																																		(BgL_wherez00_73,
																																																			BgL_globalsz00_71,
																																																			BgL_localsz00_70,
																																																			BgL_arg1902z00_1956,
																																																			BgL_arg2446z00_4216));
																																																}
																																																((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(BgL_new1160z00_4210))->BgL_handlerz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6864), BUNSPEC);
																																															}
																																															((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(BgL_new1160z00_4210))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_arg1903z00_1957, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																															return
																																																(
																																																(obj_t)
																																																BgL_new1160z00_4210);
																																														}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg1904z00_1958;
																																														obj_t
																																															BgL_arg1906z00_1959;
																																														BgL_arg1904z00_1958
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														BgL_arg1906z00_1959
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{
																																															BgL_ev_appz00_bglt
																																																BgL_auxz00_6877;
																																															{
																																																obj_t
																																																	BgL_argsz00_6879;
																																																obj_t
																																																	BgL_fz00_6878;
																																																BgL_fz00_6878
																																																	=
																																																	BgL_arg1904z00_1958;
																																																BgL_argsz00_6879
																																																	=
																																																	BgL_arg1906z00_1959;
																																																BgL_argsz00_1593
																																																	=
																																																	BgL_argsz00_6879;
																																																BgL_fz00_1592
																																																	=
																																																	BgL_fz00_6878;
																																																goto
																																																	BgL_tagzd2176zd2_1594;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_6877);
																																														}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_cdrzd28597zd2_1960;
																																												BgL_cdrzd28597zd2_1960
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3013z00zz__evaluatez00))
																																													{	/* Eval/evaluate.scm 424 */
																																														if (PAIRP(BgL_cdrzd28597zd2_1960))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd28602zd2_1964;
																																																BgL_cdrzd28602zd2_1964
																																																	=
																																																	CDR
																																																	(BgL_cdrzd28597zd2_1960);
																																																if (PAIRP(BgL_cdrzd28602zd2_1964))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_cdrzd28607zd2_1966;
																																																		BgL_cdrzd28607zd2_1966
																																																			=
																																																			CDR
																																																			(BgL_cdrzd28602zd2_1964);
																																																		if ((CAR(BgL_cdrzd28602zd2_1964) == BGl_keyword3015z00zz__evaluatez00))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				if (PAIRP(BgL_cdrzd28607zd2_1966))
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1917z00_1970;
																																																						obj_t
																																																							BgL_arg1918z00_1971;
																																																						obj_t
																																																							BgL_arg1919z00_1972;
																																																						BgL_arg1917z00_1970
																																																							=
																																																							CAR
																																																							(BgL_cdrzd28597zd2_1960);
																																																						BgL_arg1918z00_1971
																																																							=
																																																							CAR
																																																							(BgL_cdrzd28607zd2_1966);
																																																						BgL_arg1919z00_1972
																																																							=
																																																							CDR
																																																							(BgL_cdrzd28607zd2_1966);
																																																						{	/* Eval/evaluate.scm 587 */
																																																							BgL_ev_synchroniza7eza7_bglt
																																																								BgL_new1162z00_4227;
																																																							{	/* Eval/evaluate.scm 588 */
																																																								BgL_ev_synchroniza7eza7_bglt
																																																									BgL_new1161z00_4228;
																																																								BgL_new1161z00_4228
																																																									=
																																																									(
																																																									(BgL_ev_synchroniza7eza7_bglt)
																																																									BOBJECT
																																																									(GC_MALLOC
																																																										(sizeof
																																																											(struct
																																																												BgL_ev_synchroniza7eza7_bgl))));
																																																								{	/* Eval/evaluate.scm 588 */
																																																									long
																																																										BgL_arg2430z00_4229;
																																																									BgL_arg2430z00_4229
																																																										=
																																																										BGL_CLASS_NUM
																																																										(BGl_ev_synchroniza7eza7zz__evaluate_typesz00);
																																																									BGL_OBJECT_CLASS_NUM_SET
																																																										(
																																																										((BgL_objectz00_bglt) BgL_new1161z00_4228), BgL_arg2430z00_4229);
																																																								}
																																																								BgL_new1162z00_4227
																																																									=
																																																									BgL_new1161z00_4228;
																																																							}
																																																							((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4227))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																							{
																																																								BgL_ev_exprz00_bglt
																																																									BgL_auxz00_6906;
																																																								{	/* Eval/evaluate.scm 380 */
																																																									obj_t
																																																										BgL_arg2446z00_4233;
																																																									BgL_arg2446z00_4233
																																																										=
																																																										BGl_getzd2locationzd2zz__evaluatez00
																																																										(BgL_arg1917z00_1970,
																																																										BgL_locz00_74);
																																																									BgL_auxz00_6906
																																																										=
																																																										(
																																																										(BgL_ev_exprz00_bglt)
																																																										BGl_uconvzf2locze70z15zz__evaluatez00
																																																										(BgL_wherez00_73,
																																																											BgL_globalsz00_71,
																																																											BgL_localsz00_70,
																																																											BgL_arg1917z00_1970,
																																																											BgL_arg2446z00_4233));
																																																								}
																																																								((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4227))->BgL_mutexz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6906), BUNSPEC);
																																																							}
																																																							{
																																																								BgL_ev_exprz00_bglt
																																																									BgL_auxz00_6911;
																																																								{	/* Eval/evaluate.scm 380 */
																																																									obj_t
																																																										BgL_arg2446z00_4234;
																																																									BgL_arg2446z00_4234
																																																										=
																																																										BGl_getzd2locationzd2zz__evaluatez00
																																																										(BgL_arg1918z00_1971,
																																																										BgL_locz00_74);
																																																									BgL_auxz00_6911
																																																										=
																																																										(
																																																										(BgL_ev_exprz00_bglt)
																																																										BGl_uconvzf2locze70z15zz__evaluatez00
																																																										(BgL_wherez00_73,
																																																											BgL_globalsz00_71,
																																																											BgL_localsz00_70,
																																																											BgL_arg1918z00_1971,
																																																											BgL_arg2446z00_4234));
																																																								}
																																																								((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4227))->BgL_prelockz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6911), BUNSPEC);
																																																							}
																																																							((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4227))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_arg1919z00_1972, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_new1162z00_4227);
																																																						}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1920z00_1974;
																																																						obj_t
																																																							BgL_arg1923z00_1975;
																																																						BgL_arg1920z00_1974
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_cdrzd28597zd2_1960));
																																																						BgL_arg1923z00_1975
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_cdrzd28597zd2_1960));
																																																						{
																																																							BgL_ev_synchroniza7eza7_bglt
																																																								BgL_auxz00_6924;
																																																							BgL_mz00_1585
																																																								=
																																																								BgL_arg1920z00_1974;
																																																							BgL_bodyz00_1586
																																																								=
																																																								BgL_arg1923z00_1975;
																																																						BgL_tagzd2173zd2_1587:
																																																							{	/* Eval/evaluate.scm 593 */
																																																								BgL_ev_synchroniza7eza7_bglt
																																																									BgL_new1164z00_2681;
																																																								{	/* Eval/evaluate.scm 594 */
																																																									BgL_ev_synchroniza7eza7_bglt
																																																										BgL_new1163z00_2682;
																																																									BgL_new1163z00_2682
																																																										=
																																																										(
																																																										(BgL_ev_synchroniza7eza7_bglt)
																																																										BOBJECT
																																																										(GC_MALLOC
																																																											(sizeof
																																																												(struct
																																																													BgL_ev_synchroniza7eza7_bgl))));
																																																									{	/* Eval/evaluate.scm 594 */
																																																										long
																																																											BgL_arg2431z00_2683;
																																																										BgL_arg2431z00_2683
																																																											=
																																																											BGL_CLASS_NUM
																																																											(BGl_ev_synchroniza7eza7zz__evaluate_typesz00);
																																																										BGL_OBJECT_CLASS_NUM_SET
																																																											(
																																																											((BgL_objectz00_bglt) BgL_new1163z00_2682), BgL_arg2431z00_2683);
																																																									}
																																																									BgL_new1164z00_2681
																																																										=
																																																										BgL_new1163z00_2682;
																																																								}
																																																								((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1164z00_2681))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																								{
																																																									BgL_ev_exprz00_bglt
																																																										BgL_auxz00_6930;
																																																									{	/* Eval/evaluate.scm 380 */
																																																										obj_t
																																																											BgL_arg2446z00_3915;
																																																										{	/* Eval/evaluate.scm 144 */
																																																											obj_t
																																																												BgL__ortest_1069z00_3916;
																																																											BgL__ortest_1069z00_3916
																																																												=
																																																												BGl_getzd2sourcezd2locationz00zz__readerz00
																																																												(BgL_mz00_1585);
																																																											if (CBOOL(BgL__ortest_1069z00_3916))
																																																												{	/* Eval/evaluate.scm 144 */
																																																													BgL_arg2446z00_3915
																																																														=
																																																														BgL__ortest_1069z00_3916;
																																																												}
																																																											else
																																																												{	/* Eval/evaluate.scm 144 */
																																																													BgL_arg2446z00_3915
																																																														=
																																																														BgL_locz00_74;
																																																												}
																																																										}
																																																										BgL_auxz00_6930
																																																											=
																																																											(
																																																											(BgL_ev_exprz00_bglt)
																																																											BGl_convz00zz__evaluatez00
																																																											(BgL_mz00_1585,
																																																												BgL_localsz00_70,
																																																												BgL_globalsz00_71,
																																																												BFALSE,
																																																												BgL_wherez00_73,
																																																												BgL_arg2446z00_3915,
																																																												((bool_t) 0)));
																																																									}
																																																									((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1164z00_2681))->BgL_mutexz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6930), BUNSPEC);
																																																								}
																																																								{
																																																									BgL_ev_exprz00_bglt
																																																										BgL_auxz00_6937;
																																																									{	/* Eval/evaluate.scm 380 */
																																																										obj_t
																																																											BgL_arg2446z00_3917;
																																																										{	/* Eval/evaluate.scm 144 */
																																																											obj_t
																																																												BgL__ortest_1069z00_3918;
																																																											BgL__ortest_1069z00_3918
																																																												=
																																																												BGl_getzd2sourcezd2locationz00zz__readerz00
																																																												(BNIL);
																																																											if (CBOOL(BgL__ortest_1069z00_3918))
																																																												{	/* Eval/evaluate.scm 144 */
																																																													BgL_arg2446z00_3917
																																																														=
																																																														BgL__ortest_1069z00_3918;
																																																												}
																																																											else
																																																												{	/* Eval/evaluate.scm 144 */
																																																													BgL_arg2446z00_3917
																																																														=
																																																														BgL_locz00_74;
																																																												}
																																																										}
																																																										BgL_auxz00_6937
																																																											=
																																																											(
																																																											(BgL_ev_exprz00_bglt)
																																																											BGl_convz00zz__evaluatez00
																																																											(BNIL,
																																																												BgL_localsz00_70,
																																																												BgL_globalsz00_71,
																																																												BFALSE,
																																																												BgL_wherez00_73,
																																																												BgL_arg2446z00_3917,
																																																												((bool_t) 0)));
																																																									}
																																																									((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1164z00_2681))->BgL_prelockz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_6937), BUNSPEC);
																																																								}
																																																								((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1164z00_2681))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_bodyz00_1586, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																																								BgL_auxz00_6924
																																																									=
																																																									BgL_new1164z00_2681;
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_auxz00_6924);
																																																						}
																																																					}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg1924z00_1977;
																																																				obj_t
																																																					BgL_arg1925z00_1978;
																																																				BgL_arg1924z00_1977
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd28597zd2_1960));
																																																				BgL_arg1925z00_1978
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd28597zd2_1960));
																																																				{
																																																					BgL_ev_synchroniza7eza7_bglt
																																																						BgL_auxz00_6952;
																																																					{
																																																						obj_t
																																																							BgL_bodyz00_6954;
																																																						obj_t
																																																							BgL_mz00_6953;
																																																						BgL_mz00_6953
																																																							=
																																																							BgL_arg1924z00_1977;
																																																						BgL_bodyz00_6954
																																																							=
																																																							BgL_arg1925z00_1978;
																																																						BgL_bodyz00_1586
																																																							=
																																																							BgL_bodyz00_6954;
																																																						BgL_mz00_1585
																																																							=
																																																							BgL_mz00_6953;
																																																						goto
																																																							BgL_tagzd2173zd2_1587;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_6952);
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg1927z00_1981;
																																																		obj_t
																																																			BgL_arg1928z00_1982;
																																																		BgL_arg1927z00_1981
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd28597zd2_1960));
																																																		BgL_arg1928z00_1982
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd28597zd2_1960));
																																																		{
																																																			BgL_ev_synchroniza7eza7_bglt
																																																				BgL_auxz00_6960;
																																																			{
																																																				obj_t
																																																					BgL_bodyz00_6962;
																																																				obj_t
																																																					BgL_mz00_6961;
																																																				BgL_mz00_6961
																																																					=
																																																					BgL_arg1927z00_1981;
																																																				BgL_bodyz00_6962
																																																					=
																																																					BgL_arg1928z00_1982;
																																																				BgL_bodyz00_1586
																																																					=
																																																					BgL_bodyz00_6962;
																																																				BgL_mz00_1585
																																																					=
																																																					BgL_mz00_6961;
																																																				goto
																																																					BgL_tagzd2173zd2_1587;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_6960);
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg1929z00_1983;
																																																BgL_arg1929z00_1983
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{
																																																	BgL_ev_appz00_bglt
																																																		BgL_auxz00_6966;
																																																	{
																																																		obj_t
																																																			BgL_argsz00_6968;
																																																		obj_t
																																																			BgL_fz00_6967;
																																																		BgL_fz00_6967
																																																			=
																																																			BgL_arg1929z00_1983;
																																																		BgL_argsz00_6968
																																																			=
																																																			BgL_cdrzd28597zd2_1960;
																																																		BgL_argsz00_1593
																																																			=
																																																			BgL_argsz00_6968;
																																																		BgL_fz00_1592
																																																			=
																																																			BgL_fz00_6967;
																																																		goto
																																																			BgL_tagzd2176zd2_1594;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_6966);
																																																}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol2970z00zz__evaluatez00))
																																															{	/* Eval/evaluate.scm 424 */
																																																if (PAIRP(BgL_cdrzd28597zd2_1960))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_cdrzd28709zd2_1989;
																																																		BgL_cdrzd28709zd2_1989
																																																			=
																																																			CDR
																																																			(BgL_cdrzd28597zd2_1960);
																																																		if (PAIRP(BgL_cdrzd28709zd2_1989))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				if (NULLP(CDR(BgL_cdrzd28709zd2_1989)))
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1937z00_1993;
																																																						obj_t
																																																							BgL_arg1938z00_1994;
																																																						BgL_arg1937z00_1993
																																																							=
																																																							CAR
																																																							(BgL_cdrzd28597zd2_1960);
																																																						BgL_arg1938z00_1994
																																																							=
																																																							CAR
																																																							(BgL_cdrzd28709zd2_1989);
																																																						{	/* Eval/evaluate.scm 599 */
																																																							obj_t
																																																								BgL_arg2432z00_4252;
																																																							{	/* Eval/evaluate.scm 599 */
																																																								obj_t
																																																									BgL_arg2434z00_4253;
																																																								{	/* Eval/evaluate.scm 599 */
																																																									obj_t
																																																										BgL_arg2435z00_4254;
																																																									obj_t
																																																										BgL_arg2437z00_4255;
																																																									{	/* Eval/evaluate.scm 599 */
																																																										obj_t
																																																											BgL_symbolz00_4256;
																																																										BgL_symbolz00_4256
																																																											=
																																																											BGl_symbol3017z00zz__evaluatez00;
																																																										{	/* Eval/evaluate.scm 599 */
																																																											obj_t
																																																												BgL_arg2648z00_4257;
																																																											BgL_arg2648z00_4257
																																																												=
																																																												SYMBOL_TO_STRING
																																																												(BgL_symbolz00_4256);
																																																											BgL_arg2435z00_4254
																																																												=
																																																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																												(BgL_arg2648z00_4257);
																																																										}
																																																									}
																																																									{	/* Eval/evaluate.scm 599 */
																																																										obj_t
																																																											BgL_arg2648z00_4259;
																																																										BgL_arg2648z00_4259
																																																											=
																																																											SYMBOL_TO_STRING
																																																											(
																																																											((obj_t) BgL_wherez00_73));
																																																										BgL_arg2437z00_4255
																																																											=
																																																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																											(BgL_arg2648z00_4259);
																																																									}
																																																									BgL_arg2434z00_4253
																																																										=
																																																										string_append
																																																										(BgL_arg2435z00_4254,
																																																										BgL_arg2437z00_4255);
																																																								}
																																																								BgL_arg2432z00_4252
																																																									=
																																																									bstring_to_symbol
																																																									(BgL_arg2434z00_4253);
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BGl_convzd2lambdaze70z35zz__evaluatez00
																																																								(BgL_globalsz00_71,
																																																									BgL_localsz00_70,
																																																									BgL_ez00_69,
																																																									BgL_locz00_74,
																																																									BgL_arg1937z00_1993,
																																																									BgL_arg1938z00_1994,
																																																									BgL_arg2432z00_4252,
																																																									BFALSE));
																																																						}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg1939z00_1995;
																																																						obj_t
																																																							BgL_arg1940z00_1996;
																																																						BgL_arg1939z00_1995
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_ez00_69));
																																																						BgL_arg1940z00_1996
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_ez00_69));
																																																						{
																																																							BgL_ev_appz00_bglt
																																																								BgL_auxz00_6997;
																																																							{
																																																								obj_t
																																																									BgL_argsz00_6999;
																																																								obj_t
																																																									BgL_fz00_6998;
																																																								BgL_fz00_6998
																																																									=
																																																									BgL_arg1939z00_1995;
																																																								BgL_argsz00_6999
																																																									=
																																																									BgL_arg1940z00_1996;
																																																								BgL_argsz00_1593
																																																									=
																																																									BgL_argsz00_6999;
																																																								BgL_fz00_1592
																																																									=
																																																									BgL_fz00_6998;
																																																								goto
																																																									BgL_tagzd2176zd2_1594;
																																																							}
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_auxz00_6997);
																																																						}
																																																					}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg1942z00_1998;
																																																				obj_t
																																																					BgL_arg1943z00_1999;
																																																				BgL_arg1942z00_1998
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				BgL_arg1943z00_1999
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				{
																																																					BgL_ev_appz00_bglt
																																																						BgL_auxz00_7005;
																																																					{
																																																						obj_t
																																																							BgL_argsz00_7007;
																																																						obj_t
																																																							BgL_fz00_7006;
																																																						BgL_fz00_7006
																																																							=
																																																							BgL_arg1942z00_1998;
																																																						BgL_argsz00_7007
																																																							=
																																																							BgL_arg1943z00_1999;
																																																						BgL_argsz00_1593
																																																							=
																																																							BgL_argsz00_7007;
																																																						BgL_fz00_1592
																																																							=
																																																							BgL_fz00_7006;
																																																						goto
																																																							BgL_tagzd2176zd2_1594;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7005);
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg1944z00_2000;
																																																		obj_t
																																																			BgL_arg1945z00_2001;
																																																		BgL_arg1944z00_2000
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		BgL_arg1945z00_2001
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{
																																																			BgL_ev_appz00_bglt
																																																				BgL_auxz00_7013;
																																																			{
																																																				obj_t
																																																					BgL_argsz00_7015;
																																																				obj_t
																																																					BgL_fz00_7014;
																																																				BgL_fz00_7014
																																																					=
																																																					BgL_arg1944z00_2000;
																																																				BgL_argsz00_7015
																																																					=
																																																					BgL_arg1945z00_2001;
																																																				BgL_argsz00_1593
																																																					=
																																																					BgL_argsz00_7015;
																																																				BgL_fz00_1592
																																																					=
																																																					BgL_fz00_7014;
																																																				goto
																																																					BgL_tagzd2176zd2_1594;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_7013);
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3019z00zz__evaluatez00))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		return
																																																			BGl_errorz00zz__errorz00
																																																			(BGl_string3021z00zz__evaluatez00,
																																																			BGl_string3022z00zz__evaluatez00,
																																																			BgL_ez00_69);
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg1948z00_2004;
																																																		obj_t
																																																			BgL_arg1949z00_2005;
																																																		BgL_arg1948z00_2004
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		BgL_arg1949z00_2005
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{
																																																			BgL_ev_appz00_bglt
																																																				BgL_auxz00_7026;
																																																			{
																																																				obj_t
																																																					BgL_argsz00_7028;
																																																				obj_t
																																																					BgL_fz00_7027;
																																																				BgL_fz00_7027
																																																					=
																																																					BgL_arg1948z00_2004;
																																																				BgL_argsz00_7028
																																																					=
																																																					BgL_arg1949z00_2005;
																																																				BgL_argsz00_1593
																																																					=
																																																					BgL_argsz00_7028;
																																																				BgL_fz00_1592
																																																					=
																																																					BgL_fz00_7027;
																																																				goto
																																																					BgL_tagzd2176zd2_1594;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_7026);
																																																		}
																																																	}
																																															}
																																													}
																																											}
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
												else
													{	/* Eval/evaluate.scm 424 */
														if (
															(BgL_carzd23841zd2_1667 ==
																BGl_symbol2993z00zz__evaluatez00))
															{	/* Eval/evaluate.scm 424 */
																if (PAIRP(BgL_cdrzd2250zd2_1601))
																	{	/* Eval/evaluate.scm 424 */
																		if (NULLP(CDR(BgL_cdrzd2250zd2_1601)))
																			{	/* Eval/evaluate.scm 424 */
																				obj_t BgL_arg1972z00_2030;

																				BgL_arg1972z00_2030 =
																					CAR(BgL_cdrzd2250zd2_1601);
																				{	/* Eval/evaluate.scm 456 */
																					BgL_ev_trapz00_bglt
																						BgL_new1113z00_4274;
																					{	/* Eval/evaluate.scm 457 */
																						BgL_ev_trapz00_bglt
																							BgL_new1111z00_4275;
																						BgL_new1111z00_4275 =
																							((BgL_ev_trapz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_ev_trapz00_bgl))));
																						{	/* Eval/evaluate.scm 457 */
																							long BgL_arg2295z00_4276;

																							BgL_arg2295z00_4276 =
																								BGL_CLASS_NUM
																								(BGl_ev_trapz00zz__evaluate_typesz00);
																							BGL_OBJECT_CLASS_NUM_SET((
																									(BgL_objectz00_bglt)
																									BgL_new1111z00_4275),
																								BgL_arg2295z00_4276);
																						}
																						BgL_new1113z00_4274 =
																							BgL_new1111z00_4275;
																					}
																					{
																						BgL_ev_exprz00_bglt BgL_auxz00_7042;

																						{	/* Eval/evaluate.scm 380 */
																							obj_t BgL_arg2446z00_4280;

																							BgL_arg2446z00_4280 =
																								BGl_getzd2locationzd2zz__evaluatez00
																								(BgL_arg1972z00_2030,
																								BgL_locz00_74);
																							BgL_auxz00_7042 =
																								((BgL_ev_exprz00_bglt)
																								BGl_uconvzf2locze70z15zz__evaluatez00
																								(BgL_wherez00_73,
																									BgL_globalsz00_71,
																									BgL_localsz00_70,
																									BgL_arg1972z00_2030,
																									BgL_arg2446z00_4280));
																						}
																						((((BgL_ev_hookz00_bglt) COBJECT(
																										((BgL_ev_hookz00_bglt)
																											BgL_new1113z00_4274)))->
																								BgL_ez00) =
																							((BgL_ev_exprz00_bglt)
																								BgL_auxz00_7042), BUNSPEC);
																					}
																					return ((obj_t) BgL_new1113z00_4274);
																				}
																			}
																		else
																			{	/* Eval/evaluate.scm 424 */
																				obj_t BgL_arg1973z00_2031;
																				obj_t BgL_arg1974z00_2032;

																				BgL_arg1973z00_2031 =
																					CAR(((obj_t) BgL_ez00_69));
																				BgL_arg1974z00_2032 =
																					CDR(((obj_t) BgL_ez00_69));
																				{
																					BgL_ev_appz00_bglt BgL_auxz00_7053;

																					{
																						obj_t BgL_argsz00_7055;
																						obj_t BgL_fz00_7054;

																						BgL_fz00_7054 = BgL_arg1973z00_2031;
																						BgL_argsz00_7055 =
																							BgL_arg1974z00_2032;
																						BgL_argsz00_1593 = BgL_argsz00_7055;
																						BgL_fz00_1592 = BgL_fz00_7054;
																						goto BgL_tagzd2176zd2_1594;
																					}
																					return ((obj_t) BgL_auxz00_7053);
																				}
																			}
																	}
																else
																	{	/* Eval/evaluate.scm 424 */
																		obj_t BgL_arg1976z00_2034;
																		obj_t BgL_arg1977z00_2035;

																		BgL_arg1976z00_2034 =
																			CAR(((obj_t) BgL_ez00_69));
																		BgL_arg1977z00_2035 =
																			CDR(((obj_t) BgL_ez00_69));
																		{
																			BgL_ev_appz00_bglt BgL_auxz00_7061;

																			{
																				obj_t BgL_argsz00_7063;
																				obj_t BgL_fz00_7062;

																				BgL_fz00_7062 = BgL_arg1976z00_2034;
																				BgL_argsz00_7063 = BgL_arg1977z00_2035;
																				BgL_argsz00_1593 = BgL_argsz00_7063;
																				BgL_fz00_1592 = BgL_fz00_7062;
																				goto BgL_tagzd2176zd2_1594;
																			}
																			return ((obj_t) BgL_auxz00_7061);
																		}
																	}
															}
														else
															{	/* Eval/evaluate.scm 424 */
																obj_t BgL_cdrzd29422zd2_2036;

																BgL_cdrzd29422zd2_2036 =
																	CDR(((obj_t) BgL_ez00_69));
																if (
																	(CAR(
																			((obj_t) BgL_ez00_69)) ==
																		BGl_symbol2958z00zz__evaluatez00))
																	{	/* Eval/evaluate.scm 424 */
																		if (PAIRP(BgL_cdrzd29422zd2_2036))
																			{	/* Eval/evaluate.scm 424 */
																				if (NULLP(CDR(BgL_cdrzd29422zd2_2036)))
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg1983z00_2042;

																						BgL_arg1983z00_2042 =
																							CAR(BgL_cdrzd29422zd2_2036);
																						{	/* Eval/evaluate.scm 459 */
																							BgL_ev_littz00_bglt
																								BgL_new1115z00_4289;
																							{	/* Eval/evaluate.scm 460 */
																								BgL_ev_littz00_bglt
																									BgL_new1114z00_4290;
																								BgL_new1114z00_4290 =
																									((BgL_ev_littz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_ev_littz00_bgl))));
																								{	/* Eval/evaluate.scm 460 */
																									long BgL_arg2296z00_4291;

																									BgL_arg2296z00_4291 =
																										BGL_CLASS_NUM
																										(BGl_ev_littz00zz__evaluate_typesz00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1114z00_4290),
																										BgL_arg2296z00_4291);
																								}
																								BgL_new1115z00_4289 =
																									BgL_new1114z00_4290;
																							}
																							((((BgL_ev_littz00_bglt)
																										COBJECT
																										(BgL_new1115z00_4289))->
																									BgL_valuez00) =
																								((obj_t) BgL_arg1983z00_2042),
																								BUNSPEC);
																							return ((obj_t)
																								BgL_new1115z00_4289);
																						}
																					}
																				else
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg1984z00_2043;

																						BgL_arg1984z00_2043 =
																							CAR(((obj_t) BgL_ez00_69));
																						{
																							BgL_ev_appz00_bglt
																								BgL_auxz00_7085;
																							{
																								obj_t BgL_argsz00_7087;
																								obj_t BgL_fz00_7086;

																								BgL_fz00_7086 =
																									BgL_arg1984z00_2043;
																								BgL_argsz00_7087 =
																									BgL_cdrzd29422zd2_2036;
																								BgL_argsz00_1593 =
																									BgL_argsz00_7087;
																								BgL_fz00_1592 = BgL_fz00_7086;
																								goto BgL_tagzd2176zd2_1594;
																							}
																							return ((obj_t) BgL_auxz00_7085);
																						}
																					}
																			}
																		else
																			{	/* Eval/evaluate.scm 424 */
																				obj_t BgL_arg1987z00_2046;

																				BgL_arg1987z00_2046 =
																					CAR(((obj_t) BgL_ez00_69));
																				{
																					BgL_ev_appz00_bglt BgL_auxz00_7091;

																					{
																						obj_t BgL_argsz00_7093;
																						obj_t BgL_fz00_7092;

																						BgL_fz00_7092 = BgL_arg1987z00_2046;
																						BgL_argsz00_7093 =
																							BgL_cdrzd29422zd2_2036;
																						BgL_argsz00_1593 = BgL_argsz00_7093;
																						BgL_fz00_1592 = BgL_fz00_7092;
																						goto BgL_tagzd2176zd2_1594;
																					}
																					return ((obj_t) BgL_auxz00_7091);
																				}
																			}
																	}
																else
																	{	/* Eval/evaluate.scm 424 */
																		if (
																			(CAR(
																					((obj_t) BgL_ez00_69)) ==
																				BGl_symbol2966z00zz__evaluatez00))
																			{	/* Eval/evaluate.scm 424 */
																				if (PAIRP(BgL_cdrzd29422zd2_2036))
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_cdrzd210036zd2_2052;

																						BgL_cdrzd210036zd2_2052 =
																							CDR(BgL_cdrzd29422zd2_2036);
																						if (PAIRP(BgL_cdrzd210036zd2_2052))
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_cdrzd210041zd2_2054;

																								BgL_cdrzd210041zd2_2054 =
																									CDR(BgL_cdrzd210036zd2_2052);
																								if (PAIRP
																									(BgL_cdrzd210041zd2_2054))
																									{	/* Eval/evaluate.scm 424 */
																										if (NULLP(CDR
																												(BgL_cdrzd210041zd2_2054)))
																											{	/* Eval/evaluate.scm 424 */
																												obj_t
																													BgL_arg1996z00_2058;
																												obj_t
																													BgL_arg1997z00_2059;
																												obj_t
																													BgL_arg1998z00_2060;
																												BgL_arg1996z00_2058 =
																													CAR
																													(BgL_cdrzd29422zd2_2036);
																												BgL_arg1997z00_2059 =
																													CAR
																													(BgL_cdrzd210036zd2_2052);
																												BgL_arg1998z00_2060 =
																													CAR
																													(BgL_cdrzd210041zd2_2054);
																												{
																													BgL_ev_ifz00_bglt
																														BgL_auxz00_7113;
																													{
																														obj_t BgL_oz00_7116;
																														obj_t BgL_tz00_7115;
																														obj_t BgL_pz00_7114;

																														BgL_pz00_7114 =
																															BgL_arg1996z00_2058;
																														BgL_tz00_7115 =
																															BgL_arg1997z00_2059;
																														BgL_oz00_7116 =
																															BgL_arg1998z00_2060;
																														BgL_oz00_1534 =
																															BgL_oz00_7116;
																														BgL_tz00_1533 =
																															BgL_tz00_7115;
																														BgL_pz00_1532 =
																															BgL_pz00_7114;
																														goto
																															BgL_tagzd2155zd2_1535;
																													}
																													return
																														((obj_t)
																														BgL_auxz00_7113);
																												}
																											}
																										else
																											{	/* Eval/evaluate.scm 424 */
																												obj_t
																													BgL_arg1999z00_2061;
																												obj_t
																													BgL_arg2000z00_2062;
																												BgL_arg1999z00_2061 =
																													CAR(((obj_t)
																														BgL_ez00_69));
																												BgL_arg2000z00_2062 =
																													CDR(((obj_t)
																														BgL_ez00_69));
																												{
																													BgL_ev_appz00_bglt
																														BgL_auxz00_7122;
																													{
																														obj_t
																															BgL_argsz00_7124;
																														obj_t BgL_fz00_7123;

																														BgL_fz00_7123 =
																															BgL_arg1999z00_2061;
																														BgL_argsz00_7124 =
																															BgL_arg2000z00_2062;
																														BgL_argsz00_1593 =
																															BgL_argsz00_7124;
																														BgL_fz00_1592 =
																															BgL_fz00_7123;
																														goto
																															BgL_tagzd2176zd2_1594;
																													}
																													return
																														((obj_t)
																														BgL_auxz00_7122);
																												}
																											}
																									}
																								else
																									{	/* Eval/evaluate.scm 424 */
																										obj_t
																											BgL_cdrzd210341zd2_2064;
																										BgL_cdrzd210341zd2_2064 =
																											CDR(((obj_t)
																												BgL_ez00_69));
																										{	/* Eval/evaluate.scm 424 */
																											obj_t
																												BgL_cdrzd210347zd2_2065;
																											BgL_cdrzd210347zd2_2065 =
																												CDR(((obj_t)
																													BgL_cdrzd210341zd2_2064));
																											if (NULLP(CDR(((obj_t)
																															BgL_cdrzd210347zd2_2065))))
																												{	/* Eval/evaluate.scm 424 */
																													obj_t
																														BgL_arg2004z00_2068;
																													obj_t
																														BgL_arg2006z00_2069;
																													BgL_arg2004z00_2068 =
																														CAR(((obj_t)
																															BgL_cdrzd210341zd2_2064));
																													BgL_arg2006z00_2069 =
																														CAR(((obj_t)
																															BgL_cdrzd210347zd2_2065));
																													{
																														BgL_ev_ifz00_bglt
																															BgL_auxz00_7138;
																														{
																															obj_t
																																BgL_tz00_7140;
																															obj_t
																																BgL_pz00_7139;
																															BgL_pz00_7139 =
																																BgL_arg2004z00_2068;
																															BgL_tz00_7140 =
																																BgL_arg2006z00_2069;
																															BgL_tz00_1537 =
																																BgL_tz00_7140;
																															BgL_pz00_1536 =
																																BgL_pz00_7139;
																															goto
																																BgL_tagzd2156zd2_1538;
																														}
																														return
																															((obj_t)
																															BgL_auxz00_7138);
																													}
																												}
																											else
																												{	/* Eval/evaluate.scm 424 */
																													obj_t
																														BgL_arg2007z00_2070;
																													BgL_arg2007z00_2070 =
																														CAR(((obj_t)
																															BgL_ez00_69));
																													{
																														BgL_ev_appz00_bglt
																															BgL_auxz00_7144;
																														{
																															obj_t
																																BgL_argsz00_7146;
																															obj_t
																																BgL_fz00_7145;
																															BgL_fz00_7145 =
																																BgL_arg2007z00_2070;
																															BgL_argsz00_7146 =
																																BgL_cdrzd210341zd2_2064;
																															BgL_argsz00_1593 =
																																BgL_argsz00_7146;
																															BgL_fz00_1592 =
																																BgL_fz00_7145;
																															goto
																																BgL_tagzd2176zd2_1594;
																														}
																														return
																															((obj_t)
																															BgL_auxz00_7144);
																													}
																												}
																										}
																									}
																							}
																						else
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_arg2010z00_2073;
																								obj_t BgL_arg2011z00_2074;

																								BgL_arg2010z00_2073 =
																									CAR(((obj_t) BgL_ez00_69));
																								BgL_arg2011z00_2074 =
																									CDR(((obj_t) BgL_ez00_69));
																								{
																									BgL_ev_appz00_bglt
																										BgL_auxz00_7152;
																									{
																										obj_t BgL_argsz00_7154;
																										obj_t BgL_fz00_7153;

																										BgL_fz00_7153 =
																											BgL_arg2010z00_2073;
																										BgL_argsz00_7154 =
																											BgL_arg2011z00_2074;
																										BgL_argsz00_1593 =
																											BgL_argsz00_7154;
																										BgL_fz00_1592 =
																											BgL_fz00_7153;
																										goto BgL_tagzd2176zd2_1594;
																									}
																									return
																										((obj_t) BgL_auxz00_7152);
																								}
																							}
																					}
																				else
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg2012z00_2075;
																						obj_t BgL_arg2013z00_2076;

																						BgL_arg2012z00_2075 =
																							CAR(((obj_t) BgL_ez00_69));
																						BgL_arg2013z00_2076 =
																							CDR(((obj_t) BgL_ez00_69));
																						{
																							BgL_ev_appz00_bglt
																								BgL_auxz00_7160;
																							{
																								obj_t BgL_argsz00_7162;
																								obj_t BgL_fz00_7161;

																								BgL_fz00_7161 =
																									BgL_arg2012z00_2075;
																								BgL_argsz00_7162 =
																									BgL_arg2013z00_2076;
																								BgL_argsz00_1593 =
																									BgL_argsz00_7162;
																								BgL_fz00_1592 = BgL_fz00_7161;
																								goto BgL_tagzd2176zd2_1594;
																							}
																							return ((obj_t) BgL_auxz00_7160);
																						}
																					}
																			}
																		else
																			{	/* Eval/evaluate.scm 424 */
																				if (
																					(CAR(
																							((obj_t) BgL_ez00_69)) ==
																						BGl_symbol2995z00zz__evaluatez00))
																					{	/* Eval/evaluate.scm 424 */
																						obj_t BgL_arg2016z00_2079;

																						BgL_arg2016z00_2079 =
																							CDR(((obj_t) BgL_ez00_69));
																						{	/* Eval/evaluate.scm 472 */
																							BgL_ev_orz00_bglt
																								BgL_new1123z00_4322;
																							{	/* Eval/evaluate.scm 473 */
																								BgL_ev_orz00_bglt
																									BgL_new1122z00_4323;
																								BgL_new1122z00_4323 =
																									((BgL_ev_orz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_ev_orz00_bgl))));
																								{	/* Eval/evaluate.scm 473 */
																									long BgL_arg2307z00_4324;

																									BgL_arg2307z00_4324 =
																										BGL_CLASS_NUM
																										(BGl_ev_orz00zz__evaluate_typesz00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1122z00_4323),
																										BgL_arg2307z00_4324);
																								}
																								BgL_new1123z00_4322 =
																									BgL_new1122z00_4323;
																							}
																							((((BgL_ev_listz00_bglt) COBJECT(
																											((BgL_ev_listz00_bglt)
																												BgL_new1123z00_4322)))->
																									BgL_argsz00) =
																								((obj_t)
																									BGl_loopze70ze7zz__evaluatez00
																									(BgL_wherez00_73,
																										BgL_globalsz00_71,
																										BgL_localsz00_70,
																										BgL_locz00_74,
																										BgL_arg2016z00_2079)),
																								BUNSPEC);
																							return ((obj_t)
																								BgL_new1123z00_4322);
																						}
																					}
																				else
																					{	/* Eval/evaluate.scm 424 */
																						if (
																							(CAR(
																									((obj_t) BgL_ez00_69)) ==
																								BGl_symbol2997z00zz__evaluatez00))
																							{	/* Eval/evaluate.scm 424 */
																								obj_t BgL_arg2019z00_2082;

																								BgL_arg2019z00_2082 =
																									CDR(((obj_t) BgL_ez00_69));
																								{	/* Eval/evaluate.scm 475 */
																									BgL_ev_andz00_bglt
																										BgL_new1125z00_4330;
																									{	/* Eval/evaluate.scm 476 */
																										BgL_ev_andz00_bglt
																											BgL_new1124z00_4331;
																										BgL_new1124z00_4331 =
																											((BgL_ev_andz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_ev_andz00_bgl))));
																										{	/* Eval/evaluate.scm 476 */
																											long BgL_arg2308z00_4332;

																											BgL_arg2308z00_4332 =
																												BGL_CLASS_NUM
																												(BGl_ev_andz00zz__evaluate_typesz00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1124z00_4331),
																												BgL_arg2308z00_4332);
																										}
																										BgL_new1125z00_4330 =
																											BgL_new1124z00_4331;
																									}
																									((((BgL_ev_listz00_bglt)
																												COBJECT((
																														(BgL_ev_listz00_bglt)
																														BgL_new1125z00_4330)))->
																											BgL_argsz00) =
																										((obj_t)
																											BGl_loopze70ze7zz__evaluatez00
																											(BgL_wherez00_73,
																												BgL_globalsz00_71,
																												BgL_localsz00_70,
																												BgL_locz00_74,
																												BgL_arg2019z00_2082)),
																										BUNSPEC);
																									return ((obj_t)
																										BgL_new1125z00_4330);
																								}
																							}
																						else
																							{	/* Eval/evaluate.scm 424 */
																								if (
																									(CAR(
																											((obj_t) BgL_ez00_69)) ==
																										BGl_symbol2982z00zz__evaluatez00))
																									{	/* Eval/evaluate.scm 424 */
																										obj_t BgL_arg2022z00_2085;

																										BgL_arg2022z00_2085 =
																											CDR(
																											((obj_t) BgL_ez00_69));
																										BGL_TAIL return
																											BGl_convzd2beginzd2zz__evaluatez00
																											(BgL_arg2022z00_2085,
																											BgL_localsz00_70,
																											BgL_globalsz00_71,
																											BgL_tailzf3zf3_72,
																											BgL_wherez00_73,
																											BgL_locz00_74,
																											BgL_topzf3zf3_75);
																									}
																								else
																									{	/* Eval/evaluate.scm 424 */
																										obj_t
																											BgL_cdrzd211246zd2_2086;
																										BgL_cdrzd211246zd2_2086 =
																											CDR(((obj_t)
																												BgL_ez00_69));
																										if ((CAR(((obj_t)
																														BgL_ez00_69)) ==
																												BGl_symbol2968z00zz__evaluatez00))
																											{	/* Eval/evaluate.scm 424 */
																												if (PAIRP
																													(BgL_cdrzd211246zd2_2086))
																													{	/* Eval/evaluate.scm 424 */
																														obj_t
																															BgL_arg2027z00_2090;
																														obj_t
																															BgL_arg2029z00_2091;
																														BgL_arg2027z00_2090
																															=
																															CAR
																															(BgL_cdrzd211246zd2_2086);
																														BgL_arg2029z00_2091
																															=
																															CDR
																															(BgL_cdrzd211246zd2_2086);
																														{
																															BgL_ev_letz00_bglt
																																BgL_auxz00_7209;
																															{
																																obj_t
																																	BgL_bodyz00_7211;
																																obj_t
																																	BgL_bindsz00_7210;
																																BgL_bindsz00_7210
																																	=
																																	BgL_arg2027z00_2090;
																																BgL_bodyz00_7211
																																	=
																																	BgL_arg2029z00_2091;
																																BgL_bodyz00_1546
																																	=
																																	BgL_bodyz00_7211;
																																BgL_bindsz00_1545
																																	=
																																	BgL_bindsz00_7210;
																																goto
																																	BgL_tagzd2160zd2_1547;
																															}
																															return
																																((obj_t)
																																BgL_auxz00_7209);
																														}
																													}
																												else
																													{	/* Eval/evaluate.scm 424 */
																														obj_t
																															BgL_arg2030z00_2092;
																														BgL_arg2030z00_2092
																															=
																															CAR(((obj_t)
																																BgL_ez00_69));
																														{
																															BgL_ev_appz00_bglt
																																BgL_auxz00_7215;
																															{
																																obj_t
																																	BgL_argsz00_7217;
																																obj_t
																																	BgL_fz00_7216;
																																BgL_fz00_7216 =
																																	BgL_arg2030z00_2092;
																																BgL_argsz00_7217
																																	=
																																	BgL_cdrzd211246zd2_2086;
																																BgL_argsz00_1593
																																	=
																																	BgL_argsz00_7217;
																																BgL_fz00_1592 =
																																	BgL_fz00_7216;
																																goto
																																	BgL_tagzd2176zd2_1594;
																															}
																															return
																																((obj_t)
																																BgL_auxz00_7215);
																														}
																													}
																											}
																										else
																											{	/* Eval/evaluate.scm 424 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_ez00_69)) ==
																														BGl_symbol2999z00zz__evaluatez00))
																													{	/* Eval/evaluate.scm 424 */
																														if (PAIRP
																															(BgL_cdrzd211246zd2_2086))
																															{	/* Eval/evaluate.scm 424 */
																																obj_t
																																	BgL_arg2036z00_2098;
																																obj_t
																																	BgL_arg2037z00_2099;
																																BgL_arg2036z00_2098
																																	=
																																	CAR
																																	(BgL_cdrzd211246zd2_2086);
																																BgL_arg2037z00_2099
																																	=
																																	CDR
																																	(BgL_cdrzd211246zd2_2086);
																																{
																																	BgL_ev_letza2za2_bglt
																																		BgL_auxz00_7227;
																																	{
																																		obj_t
																																			BgL_bodyz00_7229;
																																		obj_t
																																			BgL_bindsz00_7228;
																																		BgL_bindsz00_7228
																																			=
																																			BgL_arg2036z00_2098;
																																		BgL_bodyz00_7229
																																			=
																																			BgL_arg2037z00_2099;
																																		BgL_bodyz00_1549
																																			=
																																			BgL_bodyz00_7229;
																																		BgL_bindsz00_1548
																																			=
																																			BgL_bindsz00_7228;
																																		goto
																																			BgL_tagzd2161zd2_1550;
																																	}
																																	return
																																		((obj_t)
																																		BgL_auxz00_7227);
																																}
																															}
																														else
																															{	/* Eval/evaluate.scm 424 */
																																obj_t
																																	BgL_arg2038z00_2100;
																																obj_t
																																	BgL_arg2039z00_2101;
																																BgL_arg2038z00_2100
																																	=
																																	CAR(((obj_t)
																																		BgL_ez00_69));
																																BgL_arg2039z00_2101
																																	=
																																	CDR(((obj_t)
																																		BgL_ez00_69));
																																{
																																	BgL_ev_appz00_bglt
																																		BgL_auxz00_7235;
																																	{
																																		obj_t
																																			BgL_argsz00_7237;
																																		obj_t
																																			BgL_fz00_7236;
																																		BgL_fz00_7236
																																			=
																																			BgL_arg2038z00_2100;
																																		BgL_argsz00_7237
																																			=
																																			BgL_arg2039z00_2101;
																																		BgL_argsz00_1593
																																			=
																																			BgL_argsz00_7237;
																																		BgL_fz00_1592
																																			=
																																			BgL_fz00_7236;
																																		goto
																																			BgL_tagzd2176zd2_1594;
																																	}
																																	return
																																		((obj_t)
																																		BgL_auxz00_7235);
																																}
																															}
																													}
																												else
																													{	/* Eval/evaluate.scm 424 */
																														obj_t
																															BgL_cdrzd211644zd2_2102;
																														BgL_cdrzd211644zd2_2102
																															=
																															CDR(((obj_t)
																																BgL_ez00_69));
																														if ((CAR(((obj_t)
																																		BgL_ez00_69))
																																==
																																BGl_symbol3001z00zz__evaluatez00))
																															{	/* Eval/evaluate.scm 424 */
																																if (PAIRP
																																	(BgL_cdrzd211644zd2_2102))
																																	{	/* Eval/evaluate.scm 424 */
																																		obj_t
																																			BgL_arg2044z00_2106;
																																		obj_t
																																			BgL_arg2045z00_2107;
																																		BgL_arg2044z00_2106
																																			=
																																			CAR
																																			(BgL_cdrzd211644zd2_2102);
																																		BgL_arg2045z00_2107
																																			=
																																			CDR
																																			(BgL_cdrzd211644zd2_2102);
																																		{
																																			BgL_ev_letrecz00_bglt
																																				BgL_auxz00_7249;
																																			{
																																				obj_t
																																					BgL_bodyz00_7251;
																																				obj_t
																																					BgL_bindsz00_7250;
																																				BgL_bindsz00_7250
																																					=
																																					BgL_arg2044z00_2106;
																																				BgL_bodyz00_7251
																																					=
																																					BgL_arg2045z00_2107;
																																				BgL_bodyz00_1552
																																					=
																																					BgL_bodyz00_7251;
																																				BgL_bindsz00_1551
																																					=
																																					BgL_bindsz00_7250;
																																				goto
																																					BgL_tagzd2162zd2_1553;
																																			}
																																			return
																																				((obj_t)
																																				BgL_auxz00_7249);
																																		}
																																	}
																																else
																																	{	/* Eval/evaluate.scm 424 */
																																		obj_t
																																			BgL_arg2046z00_2108;
																																		BgL_arg2046z00_2108
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_ez00_69));
																																		{
																																			BgL_ev_appz00_bglt
																																				BgL_auxz00_7255;
																																			{
																																				obj_t
																																					BgL_argsz00_7257;
																																				obj_t
																																					BgL_fz00_7256;
																																				BgL_fz00_7256
																																					=
																																					BgL_arg2046z00_2108;
																																				BgL_argsz00_7257
																																					=
																																					BgL_cdrzd211644zd2_2102;
																																				BgL_argsz00_1593
																																					=
																																					BgL_argsz00_7257;
																																				BgL_fz00_1592
																																					=
																																					BgL_fz00_7256;
																																				goto
																																					BgL_tagzd2176zd2_1594;
																																			}
																																			return
																																				((obj_t)
																																				BgL_auxz00_7255);
																																		}
																																	}
																															}
																														else
																															{	/* Eval/evaluate.scm 424 */
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_ez00_69))
																																		==
																																		BGl_symbol3003z00zz__evaluatez00))
																																	{	/* Eval/evaluate.scm 424 */
																																		if (PAIRP
																																			(BgL_cdrzd211644zd2_2102))
																																			{	/* Eval/evaluate.scm 424 */
																																				obj_t
																																					BgL_carzd211820zd2_2114;
																																				obj_t
																																					BgL_cdrzd211821zd2_2115;
																																				BgL_carzd211820zd2_2114
																																					=
																																					CAR
																																					(BgL_cdrzd211644zd2_2102);
																																				BgL_cdrzd211821zd2_2115
																																					=
																																					CDR
																																					(BgL_cdrzd211644zd2_2102);
																																				if (PAIRP(BgL_carzd211820zd2_2114))
																																					{	/* Eval/evaluate.scm 424 */
																																						obj_t
																																							BgL_cdrzd211825zd2_2117;
																																						BgL_cdrzd211825zd2_2117
																																							=
																																							CDR
																																							(BgL_carzd211820zd2_2114);
																																						if (
																																							(CAR
																																								(BgL_carzd211820zd2_2114)
																																								==
																																								BGl_symbol2989z00zz__evaluatez00))
																																							{	/* Eval/evaluate.scm 424 */
																																								if (PAIRP(BgL_cdrzd211825zd2_2117))
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_carzd211828zd2_2121;
																																										obj_t
																																											BgL_cdrzd211829zd2_2122;
																																										BgL_carzd211828zd2_2121
																																											=
																																											CAR
																																											(BgL_cdrzd211825zd2_2117);
																																										BgL_cdrzd211829zd2_2122
																																											=
																																											CDR
																																											(BgL_cdrzd211825zd2_2117);
																																										if (SYMBOLP(BgL_carzd211828zd2_2121))
																																											{	/* Eval/evaluate.scm 424 */
																																												if (PAIRP(BgL_cdrzd211829zd2_2122))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_carzd211833zd2_2125;
																																														BgL_carzd211833zd2_2125
																																															=
																																															CAR
																																															(BgL_cdrzd211829zd2_2122);
																																														if (SYMBOLP(BgL_carzd211833zd2_2125))
																																															{	/* Eval/evaluate.scm 424 */
																																																if (NULLP(CDR(BgL_cdrzd211829zd2_2122)))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		if (PAIRP(BgL_cdrzd211821zd2_2115))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				if (NULLP(CDR(BgL_cdrzd211821zd2_2115)))
																																																					{	/* Eval/evaluate.scm 424 */
																																																						obj_t
																																																							BgL_arg2063z00_2132;
																																																						BgL_arg2063z00_2132
																																																							=
																																																							CAR
																																																							(BgL_cdrzd211821zd2_2115);
																																																						{	/* Eval/evaluate.scm 531 */
																																																							BgL_ev_setglobalz00_bglt
																																																								BgL_new1142z00_4368;
																																																							{	/* Eval/evaluate.scm 535 */
																																																								BgL_ev_setglobalz00_bglt
																																																									BgL_new1141z00_4369;
																																																								BgL_new1141z00_4369
																																																									=
																																																									(
																																																									(BgL_ev_setglobalz00_bglt)
																																																									BOBJECT
																																																									(GC_MALLOC
																																																										(sizeof
																																																											(struct
																																																												BgL_ev_setglobalz00_bgl))));
																																																								{	/* Eval/evaluate.scm 535 */
																																																									long
																																																										BgL_arg2388z00_4370;
																																																									BgL_arg2388z00_4370
																																																										=
																																																										BGL_CLASS_NUM
																																																										(BGl_ev_setglobalz00zz__evaluate_typesz00);
																																																									BGL_OBJECT_CLASS_NUM_SET
																																																										(
																																																										((BgL_objectz00_bglt) BgL_new1141z00_4369), BgL_arg2388z00_4370);
																																																								}
																																																								BgL_new1142z00_4368
																																																									=
																																																									BgL_new1141z00_4369;
																																																							}
																																																							{
																																																								BgL_ev_exprz00_bglt
																																																									BgL_auxz00_7297;
																																																								{	/* Eval/evaluate.scm 380 */
																																																									obj_t
																																																										BgL_arg2446z00_4374;
																																																									BgL_arg2446z00_4374
																																																										=
																																																										BGl_getzd2locationzd2zz__evaluatez00
																																																										(BgL_arg2063z00_2132,
																																																										BgL_locz00_74);
																																																									BgL_auxz00_7297
																																																										=
																																																										(
																																																										(BgL_ev_exprz00_bglt)
																																																										BGl_uconvzf2locze70z15zz__evaluatez00
																																																										(BgL_wherez00_73,
																																																											BgL_globalsz00_71,
																																																											BgL_localsz00_70,
																																																											BgL_arg2063z00_2132,
																																																											BgL_arg2446z00_4374));
																																																								}
																																																								((((BgL_ev_hookz00_bglt) COBJECT(((BgL_ev_hookz00_bglt) BgL_new1142z00_4368)))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_7297), BUNSPEC);
																																																							}
																																																							((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1142z00_4368))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																							((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1142z00_4368))->BgL_namez00) = ((obj_t) BgL_carzd211828zd2_2121), BUNSPEC);
																																																							((((BgL_ev_setglobalz00_bglt) COBJECT(BgL_new1142z00_4368))->BgL_modz00) = ((obj_t) BGl_evalzd2findzd2modulez00zz__evmodulez00(BgL_carzd211833zd2_2125)), BUNSPEC);
																																																							return
																																																								(
																																																								(obj_t)
																																																								BgL_new1142z00_4368);
																																																						}
																																																					}
																																																				else
																																																					{	/* Eval/evaluate.scm 424 */
																																																						return
																																																							BGl_evcompilezd2errorzd2zz__evcompilez00
																																																							(BgL_locz00_74,
																																																							BGl_string2906z00zz__evaluatez00,
																																																							BGl_string2981z00zz__evaluatez00,
																																																							BgL_ez00_69);
																																																					}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				return
																																																					BGl_evcompilezd2errorzd2zz__evcompilez00
																																																					(BgL_locz00_74,
																																																					BGl_string2906z00zz__evaluatez00,
																																																					BGl_string2981z00zz__evaluatez00,
																																																					BgL_ez00_69);
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_cdrzd211949zd2_2134;
																																																		BgL_cdrzd211949zd2_2134
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{	/* Eval/evaluate.scm 424 */
																																																			obj_t
																																																				BgL_cdrzd211953zd2_2135;
																																																			BgL_cdrzd211953zd2_2135
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd211949zd2_2134));
																																																			if (PAIRP(BgL_cdrzd211953zd2_2135))
																																																				{	/* Eval/evaluate.scm 424 */
																																																					if (NULLP(CDR(BgL_cdrzd211953zd2_2135)))
																																																						{	/* Eval/evaluate.scm 424 */
																																																							obj_t
																																																								BgL_arg2069z00_2139;
																																																							obj_t
																																																								BgL_arg2070z00_2140;
																																																							BgL_arg2069z00_2139
																																																								=
																																																								CAR
																																																								(
																																																								((obj_t) BgL_cdrzd211949zd2_2134));
																																																							BgL_arg2070z00_2140
																																																								=
																																																								CAR
																																																								(BgL_cdrzd211953zd2_2135);
																																																							{
																																																								BgL_ev_hookz00_bglt
																																																									BgL_auxz00_7322;
																																																								{
																																																									obj_t
																																																										BgL_ez00_7324;
																																																									obj_t
																																																										BgL_vz00_7323;
																																																									BgL_vz00_7323
																																																										=
																																																										BgL_arg2069z00_2139;
																																																									BgL_ez00_7324
																																																										=
																																																										BgL_arg2070z00_2140;
																																																									BgL_ez00_1562
																																																										=
																																																										BgL_ez00_7324;
																																																									BgL_vz00_1561
																																																										=
																																																										BgL_vz00_7323;
																																																									goto
																																																										BgL_tagzd2165zd2_1563;
																																																								}
																																																								return
																																																									(
																																																									(obj_t)
																																																									BgL_auxz00_7322);
																																																							}
																																																						}
																																																					else
																																																						{	/* Eval/evaluate.scm 424 */
																																																							return
																																																								BGl_evcompilezd2errorzd2zz__evcompilez00
																																																								(BgL_locz00_74,
																																																								BGl_string2906z00zz__evaluatez00,
																																																								BGl_string2981z00zz__evaluatez00,
																																																								BgL_ez00_69);
																																																						}
																																																				}
																																																			else
																																																				{	/* Eval/evaluate.scm 424 */
																																																					return
																																																						BGl_evcompilezd2errorzd2zz__evcompilez00
																																																						(BgL_locz00_74,
																																																						BGl_string2906z00zz__evaluatez00,
																																																						BGl_string2981z00zz__evaluatez00,
																																																						BgL_ez00_69);
																																																				}
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd212010zd2_2143;
																																																BgL_cdrzd212010zd2_2143
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{	/* Eval/evaluate.scm 424 */
																																																	obj_t
																																																		BgL_cdrzd212014zd2_2144;
																																																	BgL_cdrzd212014zd2_2144
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd212010zd2_2143));
																																																	if (PAIRP(BgL_cdrzd212014zd2_2144))
																																																		{	/* Eval/evaluate.scm 424 */
																																																			if (NULLP(CDR(BgL_cdrzd212014zd2_2144)))
																																																				{	/* Eval/evaluate.scm 424 */
																																																					obj_t
																																																						BgL_arg2078z00_2148;
																																																					obj_t
																																																						BgL_arg2079z00_2149;
																																																					BgL_arg2078z00_2148
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_cdrzd212010zd2_2143));
																																																					BgL_arg2079z00_2149
																																																						=
																																																						CAR
																																																						(BgL_cdrzd212014zd2_2144);
																																																					{
																																																						BgL_ev_hookz00_bglt
																																																							BgL_auxz00_7340;
																																																						{
																																																							obj_t
																																																								BgL_ez00_7342;
																																																							obj_t
																																																								BgL_vz00_7341;
																																																							BgL_vz00_7341
																																																								=
																																																								BgL_arg2078z00_2148;
																																																							BgL_ez00_7342
																																																								=
																																																								BgL_arg2079z00_2149;
																																																							BgL_ez00_1562
																																																								=
																																																								BgL_ez00_7342;
																																																							BgL_vz00_1561
																																																								=
																																																								BgL_vz00_7341;
																																																							goto
																																																								BgL_tagzd2165zd2_1563;
																																																						}
																																																						return
																																																							(
																																																							(obj_t)
																																																							BgL_auxz00_7340);
																																																					}
																																																				}
																																																			else
																																																				{	/* Eval/evaluate.scm 424 */
																																																					return
																																																						BGl_evcompilezd2errorzd2zz__evcompilez00
																																																						(BgL_locz00_74,
																																																						BGl_string2906z00zz__evaluatez00,
																																																						BGl_string2981z00zz__evaluatez00,
																																																						BgL_ez00_69);
																																																				}
																																																		}
																																																	else
																																																		{	/* Eval/evaluate.scm 424 */
																																																			return
																																																				BGl_evcompilezd2errorzd2zz__evcompilez00
																																																				(BgL_locz00_74,
																																																				BGl_string2906z00zz__evaluatez00,
																																																				BGl_string2981z00zz__evaluatez00,
																																																				BgL_ez00_69);
																																																		}
																																																}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_cdrzd212071zd2_2151;
																																														BgL_cdrzd212071zd2_2151
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{	/* Eval/evaluate.scm 424 */
																																															obj_t
																																																BgL_cdrzd212075zd2_2152;
																																															BgL_cdrzd212075zd2_2152
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd212071zd2_2151));
																																															if (PAIRP(BgL_cdrzd212075zd2_2152))
																																																{	/* Eval/evaluate.scm 424 */
																																																	if (NULLP(CDR(BgL_cdrzd212075zd2_2152)))
																																																		{	/* Eval/evaluate.scm 424 */
																																																			obj_t
																																																				BgL_arg2084z00_2156;
																																																			obj_t
																																																				BgL_arg2086z00_2157;
																																																			BgL_arg2084z00_2156
																																																				=
																																																				CAR
																																																				(
																																																				((obj_t) BgL_cdrzd212071zd2_2151));
																																																			BgL_arg2086z00_2157
																																																				=
																																																				CAR
																																																				(BgL_cdrzd212075zd2_2152);
																																																			{
																																																				BgL_ev_hookz00_bglt
																																																					BgL_auxz00_7358;
																																																				{
																																																					obj_t
																																																						BgL_ez00_7360;
																																																					obj_t
																																																						BgL_vz00_7359;
																																																					BgL_vz00_7359
																																																						=
																																																						BgL_arg2084z00_2156;
																																																					BgL_ez00_7360
																																																						=
																																																						BgL_arg2086z00_2157;
																																																					BgL_ez00_1562
																																																						=
																																																						BgL_ez00_7360;
																																																					BgL_vz00_1561
																																																						=
																																																						BgL_vz00_7359;
																																																					goto
																																																						BgL_tagzd2165zd2_1563;
																																																				}
																																																				return
																																																					(
																																																					(obj_t)
																																																					BgL_auxz00_7358);
																																																			}
																																																		}
																																																	else
																																																		{	/* Eval/evaluate.scm 424 */
																																																			return
																																																				BGl_evcompilezd2errorzd2zz__evcompilez00
																																																				(BgL_locz00_74,
																																																				BGl_string2906z00zz__evaluatez00,
																																																				BGl_string2981z00zz__evaluatez00,
																																																				BgL_ez00_69);
																																																		}
																																																}
																																															else
																																																{	/* Eval/evaluate.scm 424 */
																																																	return
																																																		BGl_evcompilezd2errorzd2zz__evcompilez00
																																																		(BgL_locz00_74,
																																																		BGl_string2906z00zz__evaluatez00,
																																																		BGl_string2981z00zz__evaluatez00,
																																																		BgL_ez00_69);
																																																}
																																														}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_cdrzd212132zd2_2159;
																																												BgL_cdrzd212132zd2_2159
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{	/* Eval/evaluate.scm 424 */
																																													obj_t
																																														BgL_cdrzd212136zd2_2160;
																																													BgL_cdrzd212136zd2_2160
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd212132zd2_2159));
																																													if (PAIRP(BgL_cdrzd212136zd2_2160))
																																														{	/* Eval/evaluate.scm 424 */
																																															if (NULLP(CDR(BgL_cdrzd212136zd2_2160)))
																																																{	/* Eval/evaluate.scm 424 */
																																																	obj_t
																																																		BgL_arg2091z00_2164;
																																																	obj_t
																																																		BgL_arg2093z00_2165;
																																																	BgL_arg2091z00_2164
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_cdrzd212132zd2_2159));
																																																	BgL_arg2093z00_2165
																																																		=
																																																		CAR
																																																		(BgL_cdrzd212136zd2_2160);
																																																	{
																																																		BgL_ev_hookz00_bglt
																																																			BgL_auxz00_7376;
																																																		{
																																																			obj_t
																																																				BgL_ez00_7378;
																																																			obj_t
																																																				BgL_vz00_7377;
																																																			BgL_vz00_7377
																																																				=
																																																				BgL_arg2091z00_2164;
																																																			BgL_ez00_7378
																																																				=
																																																				BgL_arg2093z00_2165;
																																																			BgL_ez00_1562
																																																				=
																																																				BgL_ez00_7378;
																																																			BgL_vz00_1561
																																																				=
																																																				BgL_vz00_7377;
																																																			goto
																																																				BgL_tagzd2165zd2_1563;
																																																		}
																																																		return
																																																			(
																																																			(obj_t)
																																																			BgL_auxz00_7376);
																																																	}
																																																}
																																															else
																																																{	/* Eval/evaluate.scm 424 */
																																																	return
																																																		BGl_evcompilezd2errorzd2zz__evcompilez00
																																																		(BgL_locz00_74,
																																																		BGl_string2906z00zz__evaluatez00,
																																																		BGl_string2981z00zz__evaluatez00,
																																																		BgL_ez00_69);
																																																}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_cdrzd212193zd2_2167;
																																										BgL_cdrzd212193zd2_2167
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										{	/* Eval/evaluate.scm 424 */
																																											obj_t
																																												BgL_cdrzd212197zd2_2168;
																																											BgL_cdrzd212197zd2_2168
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd212193zd2_2167));
																																											if (PAIRP(BgL_cdrzd212197zd2_2168))
																																												{	/* Eval/evaluate.scm 424 */
																																													if (NULLP(CDR(BgL_cdrzd212197zd2_2168)))
																																														{	/* Eval/evaluate.scm 424 */
																																															obj_t
																																																BgL_arg2098z00_2172;
																																															obj_t
																																																BgL_arg2099z00_2173;
																																															BgL_arg2098z00_2172
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd212193zd2_2167));
																																															BgL_arg2099z00_2173
																																																=
																																																CAR
																																																(BgL_cdrzd212197zd2_2168);
																																															{
																																																BgL_ev_hookz00_bglt
																																																	BgL_auxz00_7394;
																																																{
																																																	obj_t
																																																		BgL_ez00_7396;
																																																	obj_t
																																																		BgL_vz00_7395;
																																																	BgL_vz00_7395
																																																		=
																																																		BgL_arg2098z00_2172;
																																																	BgL_ez00_7396
																																																		=
																																																		BgL_arg2099z00_2173;
																																																	BgL_ez00_1562
																																																		=
																																																		BgL_ez00_7396;
																																																	BgL_vz00_1561
																																																		=
																																																		BgL_vz00_7395;
																																																	goto
																																																		BgL_tagzd2165zd2_1563;
																																																}
																																																return
																																																	(
																																																	(obj_t)
																																																	BgL_auxz00_7394);
																																															}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																											else
																																												{	/* Eval/evaluate.scm 424 */
																																													return
																																														BGl_evcompilezd2errorzd2zz__evcompilez00
																																														(BgL_locz00_74,
																																														BGl_string2906z00zz__evaluatez00,
																																														BGl_string2981z00zz__evaluatez00,
																																														BgL_ez00_69);
																																												}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_cdrzd212241zd2_2175;
																																								BgL_cdrzd212241zd2_2175
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								{	/* Eval/evaluate.scm 424 */
																																									obj_t
																																										BgL_carzd212244zd2_2176;
																																									obj_t
																																										BgL_cdrzd212245zd2_2177;
																																									BgL_carzd212244zd2_2176
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd212241zd2_2175));
																																									BgL_cdrzd212245zd2_2177
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd212241zd2_2175));
																																									if ((CAR(((obj_t) BgL_carzd212244zd2_2176)) == BGl_symbol2991z00zz__evaluatez00))
																																										{	/* Eval/evaluate.scm 424 */
																																											if (PAIRP(BgL_cdrzd212245zd2_2177))
																																												{	/* Eval/evaluate.scm 424 */
																																													if (NULLP(CDR(BgL_cdrzd212245zd2_2177)))
																																														{	/* Eval/evaluate.scm 424 */
																																															obj_t
																																																BgL_arg2106z00_2183;
																																															obj_t
																																																BgL_arg2107z00_2184;
																																															BgL_arg2106z00_2183
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_carzd212244zd2_2176));
																																															BgL_arg2107z00_2184
																																																=
																																																CAR
																																																(BgL_cdrzd212245zd2_2177);
																																															{
																																																obj_t
																																																	BgL_e2z00_7419;
																																																obj_t
																																																	BgL_lz00_7418;
																																																BgL_lz00_7418
																																																	=
																																																	BgL_arg2106z00_2183;
																																																BgL_e2z00_7419
																																																	=
																																																	BgL_arg2107z00_2184;
																																																BgL_e2z00_1559
																																																	=
																																																	BgL_e2z00_7419;
																																																BgL_lz00_1558
																																																	=
																																																	BgL_lz00_7418;
																																																goto
																																																	BgL_tagzd2164zd2_1560;
																																															}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																											else
																																												{	/* Eval/evaluate.scm 424 */
																																													return
																																														BGl_evcompilezd2errorzd2zz__evcompilez00
																																														(BgL_locz00_74,
																																														BGl_string2906z00zz__evaluatez00,
																																														BGl_string2981z00zz__evaluatez00,
																																														BgL_ez00_69);
																																												}
																																										}
																																									else
																																										{	/* Eval/evaluate.scm 424 */
																																											obj_t
																																												BgL_cdrzd212321zd2_2187;
																																											BgL_cdrzd212321zd2_2187
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd212241zd2_2175));
																																											if (PAIRP(BgL_cdrzd212321zd2_2187))
																																												{	/* Eval/evaluate.scm 424 */
																																													if (NULLP(CDR(BgL_cdrzd212321zd2_2187)))
																																														{	/* Eval/evaluate.scm 424 */
																																															obj_t
																																																BgL_arg2112z00_2191;
																																															obj_t
																																																BgL_arg2113z00_2192;
																																															BgL_arg2112z00_2191
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd212241zd2_2175));
																																															BgL_arg2113z00_2192
																																																=
																																																CAR
																																																(BgL_cdrzd212321zd2_2187);
																																															{
																																																BgL_ev_hookz00_bglt
																																																	BgL_auxz00_7432;
																																																{
																																																	obj_t
																																																		BgL_ez00_7434;
																																																	obj_t
																																																		BgL_vz00_7433;
																																																	BgL_vz00_7433
																																																		=
																																																		BgL_arg2112z00_2191;
																																																	BgL_ez00_7434
																																																		=
																																																		BgL_arg2113z00_2192;
																																																	BgL_ez00_1562
																																																		=
																																																		BgL_ez00_7434;
																																																	BgL_vz00_1561
																																																		=
																																																		BgL_vz00_7433;
																																																	goto
																																																		BgL_tagzd2165zd2_1563;
																																																}
																																																return
																																																	(
																																																	(obj_t)
																																																	BgL_auxz00_7432);
																																															}
																																														}
																																													else
																																														{	/* Eval/evaluate.scm 424 */
																																															return
																																																BGl_evcompilezd2errorzd2zz__evcompilez00
																																																(BgL_locz00_74,
																																																BGl_string2906z00zz__evaluatez00,
																																																BGl_string2981z00zz__evaluatez00,
																																																BgL_ez00_69);
																																														}
																																												}
																																											else
																																												{	/* Eval/evaluate.scm 424 */
																																													return
																																														BGl_evcompilezd2errorzd2zz__evcompilez00
																																														(BgL_locz00_74,
																																														BGl_string2906z00zz__evaluatez00,
																																														BGl_string2981z00zz__evaluatez00,
																																														BgL_ez00_69);
																																												}
																																										}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 424 */
																																						obj_t
																																							BgL_cdrzd212378zd2_2196;
																																						BgL_cdrzd212378zd2_2196
																																							=
																																							CDR
																																							(((obj_t) BgL_ez00_69));
																																						{	/* Eval/evaluate.scm 424 */
																																							obj_t
																																								BgL_cdrzd212382zd2_2197;
																																							BgL_cdrzd212382zd2_2197
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd212378zd2_2196));
																																							if (PAIRP(BgL_cdrzd212382zd2_2197))
																																								{	/* Eval/evaluate.scm 424 */
																																									if (NULLP(CDR(BgL_cdrzd212382zd2_2197)))
																																										{	/* Eval/evaluate.scm 424 */
																																											obj_t
																																												BgL_arg2120z00_2201;
																																											obj_t
																																												BgL_arg2121z00_2202;
																																											BgL_arg2120z00_2201
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd212378zd2_2196));
																																											BgL_arg2121z00_2202
																																												=
																																												CAR
																																												(BgL_cdrzd212382zd2_2197);
																																											{
																																												BgL_ev_hookz00_bglt
																																													BgL_auxz00_7450;
																																												{
																																													obj_t
																																														BgL_ez00_7452;
																																													obj_t
																																														BgL_vz00_7451;
																																													BgL_vz00_7451
																																														=
																																														BgL_arg2120z00_2201;
																																													BgL_ez00_7452
																																														=
																																														BgL_arg2121z00_2202;
																																													BgL_ez00_1562
																																														=
																																														BgL_ez00_7452;
																																													BgL_vz00_1561
																																														=
																																														BgL_vz00_7451;
																																													goto
																																														BgL_tagzd2165zd2_1563;
																																												}
																																												return
																																													(
																																													(obj_t)
																																													BgL_auxz00_7450);
																																											}
																																										}
																																									else
																																										{	/* Eval/evaluate.scm 424 */
																																											return
																																												BGl_evcompilezd2errorzd2zz__evcompilez00
																																												(BgL_locz00_74,
																																												BGl_string2906z00zz__evaluatez00,
																																												BGl_string2981z00zz__evaluatez00,
																																												BgL_ez00_69);
																																										}
																																								}
																																							else
																																								{	/* Eval/evaluate.scm 424 */
																																									return
																																										BGl_evcompilezd2errorzd2zz__evcompilez00
																																										(BgL_locz00_74,
																																										BGl_string2906z00zz__evaluatez00,
																																										BGl_string2981z00zz__evaluatez00,
																																										BgL_ez00_69);
																																								}
																																						}
																																					}
																																			}
																																		else
																																			{	/* Eval/evaluate.scm 424 */
																																				return
																																					BGl_evcompilezd2errorzd2zz__evcompilez00
																																					(BgL_locz00_74,
																																					BGl_string2906z00zz__evaluatez00,
																																					BGl_string2981z00zz__evaluatez00,
																																					BgL_ez00_69);
																																			}
																																	}
																																else
																																	{	/* Eval/evaluate.scm 424 */
																																		obj_t
																																			BgL_cdrzd212503zd2_2204;
																																		BgL_cdrzd212503zd2_2204
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_ez00_69));
																																		if ((CAR((
																																						(obj_t)
																																						BgL_ez00_69))
																																				==
																																				BGl_symbol3005z00zz__evaluatez00))
																																			{	/* Eval/evaluate.scm 424 */
																																				if (PAIRP(BgL_cdrzd212503zd2_2204))
																																					{	/* Eval/evaluate.scm 424 */
																																						obj_t
																																							BgL_cdrzd212508zd2_2208;
																																						BgL_cdrzd212508zd2_2208
																																							=
																																							CDR
																																							(BgL_cdrzd212503zd2_2204);
																																						if (PAIRP(BgL_cdrzd212508zd2_2208))
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_carzd212512zd2_2210;
																																								BgL_carzd212512zd2_2210
																																									=
																																									CAR
																																									(BgL_cdrzd212508zd2_2208);
																																								if (PAIRP(BgL_carzd212512zd2_2210))
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_cdrzd212517zd2_2212;
																																										BgL_cdrzd212517zd2_2212
																																											=
																																											CDR
																																											(BgL_carzd212512zd2_2210);
																																										if ((CAR(BgL_carzd212512zd2_2210) == BGl_symbol2970z00zz__evaluatez00))
																																											{	/* Eval/evaluate.scm 424 */
																																												if (PAIRP(BgL_cdrzd212517zd2_2212))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_cdrzd212521zd2_2216;
																																														BgL_cdrzd212521zd2_2216
																																															=
																																															CDR
																																															(BgL_cdrzd212517zd2_2212);
																																														if (PAIRP(BgL_cdrzd212521zd2_2216))
																																															{	/* Eval/evaluate.scm 424 */
																																																if (NULLP(CDR(BgL_cdrzd212521zd2_2216)))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		if (NULLP(CDR(BgL_cdrzd212508zd2_2208)))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2137z00_2222;
																																																				obj_t
																																																					BgL_arg2138z00_2223;
																																																				obj_t
																																																					BgL_arg2139z00_2224;
																																																				BgL_arg2137z00_2222
																																																					=
																																																					CAR
																																																					(BgL_cdrzd212503zd2_2204);
																																																				BgL_arg2138z00_2223
																																																					=
																																																					CAR
																																																					(BgL_cdrzd212517zd2_2212);
																																																				BgL_arg2139z00_2224
																																																					=
																																																					CAR
																																																					(BgL_cdrzd212521zd2_2216);
																																																				{
																																																					BgL_ev_defglobalz00_bglt
																																																						BgL_auxz00_7489;
																																																					{
																																																						obj_t
																																																							BgL_bodyz00_7492;
																																																						obj_t
																																																							BgL_formalsz00_7491;
																																																						obj_t
																																																							BgL_gvz00_7490;
																																																						BgL_gvz00_7490
																																																							=
																																																							BgL_arg2137z00_2222;
																																																						BgL_formalsz00_7491
																																																							=
																																																							BgL_arg2138z00_2223;
																																																						BgL_bodyz00_7492
																																																							=
																																																							BgL_arg2139z00_2224;
																																																						BgL_bodyz00_1567
																																																							=
																																																							BgL_bodyz00_7492;
																																																						BgL_formalsz00_1566
																																																							=
																																																							BgL_formalsz00_7491;
																																																						BgL_gvz00_1565
																																																							=
																																																							BgL_gvz00_7490;
																																																						goto
																																																							BgL_tagzd2167zd2_1568;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7489);
																																																				}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2141z00_2225;
																																																				BgL_arg2141z00_2225
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				{
																																																					BgL_ev_appz00_bglt
																																																						BgL_auxz00_7496;
																																																					{
																																																						obj_t
																																																							BgL_argsz00_7498;
																																																						obj_t
																																																							BgL_fz00_7497;
																																																						BgL_fz00_7497
																																																							=
																																																							BgL_arg2141z00_2225;
																																																						BgL_argsz00_7498
																																																							=
																																																							BgL_cdrzd212503zd2_2204;
																																																						BgL_argsz00_1593
																																																							=
																																																							BgL_argsz00_7498;
																																																						BgL_fz00_1592
																																																							=
																																																							BgL_fz00_7497;
																																																						goto
																																																							BgL_tagzd2176zd2_1594;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7496);
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_cdrzd212623zd2_2229;
																																																		BgL_cdrzd212623zd2_2229
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd212503zd2_2204));
																																																		if (NULLP(CDR(((obj_t) BgL_cdrzd212623zd2_2229))))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2146z00_2232;
																																																				obj_t
																																																					BgL_arg2147z00_2233;
																																																				BgL_arg2146z00_2232
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd212503zd2_2204));
																																																				BgL_arg2147z00_2233
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd212623zd2_2229));
																																																				{
																																																					BgL_ev_defglobalz00_bglt
																																																						BgL_auxz00_7510;
																																																					{
																																																						obj_t
																																																							BgL_gez00_7512;
																																																						obj_t
																																																							BgL_gvz00_7511;
																																																						BgL_gvz00_7511
																																																							=
																																																							BgL_arg2146z00_2232;
																																																						BgL_gez00_7512
																																																							=
																																																							BgL_arg2147z00_2233;
																																																						BgL_gez00_1570
																																																							=
																																																							BgL_gez00_7512;
																																																						BgL_gvz00_1569
																																																							=
																																																							BgL_gvz00_7511;
																																																						goto
																																																							BgL_tagzd2168zd2_1571;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7510);
																																																				}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2148z00_2234;
																																																				obj_t
																																																					BgL_arg2149z00_2235;
																																																				BgL_arg2148z00_2234
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				BgL_arg2149z00_2235
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				{
																																																					BgL_ev_appz00_bglt
																																																						BgL_auxz00_7518;
																																																					{
																																																						obj_t
																																																							BgL_argsz00_7520;
																																																						obj_t
																																																							BgL_fz00_7519;
																																																						BgL_fz00_7519
																																																							=
																																																							BgL_arg2148z00_2234;
																																																						BgL_argsz00_7520
																																																							=
																																																							BgL_arg2149z00_2235;
																																																						BgL_argsz00_1593
																																																							=
																																																							BgL_argsz00_7520;
																																																						BgL_fz00_1592
																																																							=
																																																							BgL_fz00_7519;
																																																						goto
																																																							BgL_tagzd2176zd2_1594;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7518);
																																																				}
																																																			}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd212712zd2_2239;
																																																BgL_cdrzd212712zd2_2239
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd212503zd2_2204));
																																																if (NULLP(CDR(((obj_t) BgL_cdrzd212712zd2_2239))))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg2154z00_2242;
																																																		obj_t
																																																			BgL_arg2155z00_2243;
																																																		BgL_arg2154z00_2242
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd212503zd2_2204));
																																																		BgL_arg2155z00_2243
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd212712zd2_2239));
																																																		{
																																																			BgL_ev_defglobalz00_bglt
																																																				BgL_auxz00_7532;
																																																			{
																																																				obj_t
																																																					BgL_gez00_7534;
																																																				obj_t
																																																					BgL_gvz00_7533;
																																																				BgL_gvz00_7533
																																																					=
																																																					BgL_arg2154z00_2242;
																																																				BgL_gez00_7534
																																																					=
																																																					BgL_arg2155z00_2243;
																																																				BgL_gez00_1570
																																																					=
																																																					BgL_gez00_7534;
																																																				BgL_gvz00_1569
																																																					=
																																																					BgL_gvz00_7533;
																																																				goto
																																																					BgL_tagzd2168zd2_1571;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_7532);
																																																		}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg2156z00_2244;
																																																		obj_t
																																																			BgL_arg2157z00_2245;
																																																		BgL_arg2156z00_2244
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		BgL_arg2157z00_2245
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{
																																																			BgL_ev_appz00_bglt
																																																				BgL_auxz00_7540;
																																																			{
																																																				obj_t
																																																					BgL_argsz00_7542;
																																																				obj_t
																																																					BgL_fz00_7541;
																																																				BgL_fz00_7541
																																																					=
																																																					BgL_arg2156z00_2244;
																																																				BgL_argsz00_7542
																																																					=
																																																					BgL_arg2157z00_2245;
																																																				BgL_argsz00_1593
																																																					=
																																																					BgL_argsz00_7542;
																																																				BgL_fz00_1592
																																																					=
																																																					BgL_fz00_7541;
																																																				goto
																																																					BgL_tagzd2176zd2_1594;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_7540);
																																																		}
																																																	}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_cdrzd212801zd2_2248;
																																														BgL_cdrzd212801zd2_2248
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd212503zd2_2204));
																																														if (NULLP(CDR(((obj_t) BgL_cdrzd212801zd2_2248))))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg2161z00_2251;
																																																obj_t
																																																	BgL_arg2162z00_2252;
																																																BgL_arg2161z00_2251
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd212503zd2_2204));
																																																BgL_arg2162z00_2252
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd212801zd2_2248));
																																																{
																																																	BgL_ev_defglobalz00_bglt
																																																		BgL_auxz00_7554;
																																																	{
																																																		obj_t
																																																			BgL_gez00_7556;
																																																		obj_t
																																																			BgL_gvz00_7555;
																																																		BgL_gvz00_7555
																																																			=
																																																			BgL_arg2161z00_2251;
																																																		BgL_gez00_7556
																																																			=
																																																			BgL_arg2162z00_2252;
																																																		BgL_gez00_1570
																																																			=
																																																			BgL_gez00_7556;
																																																		BgL_gvz00_1569
																																																			=
																																																			BgL_gvz00_7555;
																																																		goto
																																																			BgL_tagzd2168zd2_1571;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_7554);
																																																}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg2163z00_2253;
																																																obj_t
																																																	BgL_arg2164z00_2254;
																																																BgL_arg2163z00_2253
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																BgL_arg2164z00_2254
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{
																																																	BgL_ev_appz00_bglt
																																																		BgL_auxz00_7562;
																																																	{
																																																		obj_t
																																																			BgL_argsz00_7564;
																																																		obj_t
																																																			BgL_fz00_7563;
																																																		BgL_fz00_7563
																																																			=
																																																			BgL_arg2163z00_2253;
																																																		BgL_argsz00_7564
																																																			=
																																																			BgL_arg2164z00_2254;
																																																		BgL_argsz00_1593
																																																			=
																																																			BgL_argsz00_7564;
																																																		BgL_fz00_1592
																																																			=
																																																			BgL_fz00_7563;
																																																		goto
																																																			BgL_tagzd2176zd2_1594;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_7562);
																																																}
																																															}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_cdrzd212890zd2_2257;
																																												BgL_cdrzd212890zd2_2257
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd212503zd2_2204));
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd212890zd2_2257))))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg2168z00_2260;
																																														obj_t
																																															BgL_arg2169z00_2261;
																																														BgL_arg2168z00_2260
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd212503zd2_2204));
																																														BgL_arg2169z00_2261
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd212890zd2_2257));
																																														{
																																															BgL_ev_defglobalz00_bglt
																																																BgL_auxz00_7576;
																																															{
																																																obj_t
																																																	BgL_gez00_7578;
																																																obj_t
																																																	BgL_gvz00_7577;
																																																BgL_gvz00_7577
																																																	=
																																																	BgL_arg2168z00_2260;
																																																BgL_gez00_7578
																																																	=
																																																	BgL_arg2169z00_2261;
																																																BgL_gez00_1570
																																																	=
																																																	BgL_gez00_7578;
																																																BgL_gvz00_1569
																																																	=
																																																	BgL_gvz00_7577;
																																																goto
																																																	BgL_tagzd2168zd2_1571;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_7576);
																																														}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg2170z00_2262;
																																														obj_t
																																															BgL_arg2171z00_2263;
																																														BgL_arg2170z00_2262
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														BgL_arg2171z00_2263
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{
																																															BgL_ev_appz00_bglt
																																																BgL_auxz00_7584;
																																															{
																																																obj_t
																																																	BgL_argsz00_7586;
																																																obj_t
																																																	BgL_fz00_7585;
																																																BgL_fz00_7585
																																																	=
																																																	BgL_arg2170z00_2262;
																																																BgL_argsz00_7586
																																																	=
																																																	BgL_arg2171z00_2263;
																																																BgL_argsz00_1593
																																																	=
																																																	BgL_argsz00_7586;
																																																BgL_fz00_1592
																																																	=
																																																	BgL_fz00_7585;
																																																goto
																																																	BgL_tagzd2176zd2_1594;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_7584);
																																														}
																																													}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_cdrzd212979zd2_2267;
																																										BgL_cdrzd212979zd2_2267
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd212503zd2_2204));
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd212979zd2_2267))))
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg2176z00_2270;
																																												obj_t
																																													BgL_arg2177z00_2271;
																																												BgL_arg2176z00_2270
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd212503zd2_2204));
																																												BgL_arg2177z00_2271
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd212979zd2_2267));
																																												{
																																													BgL_ev_defglobalz00_bglt
																																														BgL_auxz00_7598;
																																													{
																																														obj_t
																																															BgL_gez00_7600;
																																														obj_t
																																															BgL_gvz00_7599;
																																														BgL_gvz00_7599
																																															=
																																															BgL_arg2176z00_2270;
																																														BgL_gez00_7600
																																															=
																																															BgL_arg2177z00_2271;
																																														BgL_gez00_1570
																																															=
																																															BgL_gez00_7600;
																																														BgL_gvz00_1569
																																															=
																																															BgL_gvz00_7599;
																																														goto
																																															BgL_tagzd2168zd2_1571;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_7598);
																																												}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg2178z00_2272;
																																												obj_t
																																													BgL_arg2179z00_2273;
																																												BgL_arg2178z00_2272
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												BgL_arg2179z00_2273
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{
																																													BgL_ev_appz00_bglt
																																														BgL_auxz00_7606;
																																													{
																																														obj_t
																																															BgL_argsz00_7608;
																																														obj_t
																																															BgL_fz00_7607;
																																														BgL_fz00_7607
																																															=
																																															BgL_arg2178z00_2272;
																																														BgL_argsz00_7608
																																															=
																																															BgL_arg2179z00_2273;
																																														BgL_argsz00_1593
																																															=
																																															BgL_argsz00_7608;
																																														BgL_fz00_1592
																																															=
																																															BgL_fz00_7607;
																																														goto
																																															BgL_tagzd2176zd2_1594;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_7606);
																																												}
																																											}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_arg2181z00_2275;
																																								BgL_arg2181z00_2275
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								{
																																									BgL_ev_appz00_bglt
																																										BgL_auxz00_7612;
																																									{
																																										obj_t
																																											BgL_argsz00_7614;
																																										obj_t
																																											BgL_fz00_7613;
																																										BgL_fz00_7613
																																											=
																																											BgL_arg2181z00_2275;
																																										BgL_argsz00_7614
																																											=
																																											BgL_cdrzd212503zd2_2204;
																																										BgL_argsz00_1593
																																											=
																																											BgL_argsz00_7614;
																																										BgL_fz00_1592
																																											=
																																											BgL_fz00_7613;
																																										goto
																																											BgL_tagzd2176zd2_1594;
																																									}
																																									return
																																										(
																																										(obj_t)
																																										BgL_auxz00_7612);
																																								}
																																							}
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 424 */
																																						obj_t
																																							BgL_arg2183z00_2277;
																																						BgL_arg2183z00_2277
																																							=
																																							CAR
																																							(((obj_t) BgL_ez00_69));
																																						{
																																							BgL_ev_appz00_bglt
																																								BgL_auxz00_7618;
																																							{
																																								obj_t
																																									BgL_argsz00_7620;
																																								obj_t
																																									BgL_fz00_7619;
																																								BgL_fz00_7619
																																									=
																																									BgL_arg2183z00_2277;
																																								BgL_argsz00_7620
																																									=
																																									BgL_cdrzd212503zd2_2204;
																																								BgL_argsz00_1593
																																									=
																																									BgL_argsz00_7620;
																																								BgL_fz00_1592
																																									=
																																									BgL_fz00_7619;
																																								goto
																																									BgL_tagzd2176zd2_1594;
																																							}
																																							return
																																								(
																																								(obj_t)
																																								BgL_auxz00_7618);
																																						}
																																					}
																																			}
																																		else
																																			{	/* Eval/evaluate.scm 424 */
																																				if (
																																					(CAR(
																																							((obj_t) BgL_ez00_69)) == BGl_symbol3007z00zz__evaluatez00))
																																					{	/* Eval/evaluate.scm 424 */
																																						if (PAIRP(BgL_cdrzd212503zd2_2204))
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_carzd213227zd2_2283;
																																								BgL_carzd213227zd2_2283
																																									=
																																									CAR
																																									(BgL_cdrzd212503zd2_2204);
																																								if (PAIRP(BgL_carzd213227zd2_2283))
																																									{	/* Eval/evaluate.scm 424 */
																																										if (NULLP(CDR(BgL_carzd213227zd2_2283)))
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg2191z00_2287;
																																												obj_t
																																													BgL_arg2192z00_2288;
																																												BgL_arg2191z00_2287
																																													=
																																													CAR
																																													(BgL_carzd213227zd2_2283);
																																												BgL_arg2192z00_2288
																																													=
																																													CDR
																																													(BgL_cdrzd212503zd2_2204);
																																												{
																																													BgL_ev_bindzd2exitzd2_bglt
																																														BgL_auxz00_7636;
																																													{
																																														obj_t
																																															BgL_bodyz00_7638;
																																														obj_t
																																															BgL_vz00_7637;
																																														BgL_vz00_7637
																																															=
																																															BgL_arg2191z00_2287;
																																														BgL_bodyz00_7638
																																															=
																																															BgL_arg2192z00_2288;
																																														BgL_bodyz00_1573
																																															=
																																															BgL_bodyz00_7638;
																																														BgL_vz00_1572
																																															=
																																															BgL_vz00_7637;
																																														goto
																																															BgL_tagzd2169zd2_1574;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_7636);
																																												}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg2193z00_2289;
																																												obj_t
																																													BgL_arg2194z00_2290;
																																												BgL_arg2193z00_2289
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												BgL_arg2194z00_2290
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{
																																													BgL_ev_appz00_bglt
																																														BgL_auxz00_7644;
																																													{
																																														obj_t
																																															BgL_argsz00_7646;
																																														obj_t
																																															BgL_fz00_7645;
																																														BgL_fz00_7645
																																															=
																																															BgL_arg2193z00_2289;
																																														BgL_argsz00_7646
																																															=
																																															BgL_arg2194z00_2290;
																																														BgL_argsz00_1593
																																															=
																																															BgL_argsz00_7646;
																																														BgL_fz00_1592
																																															=
																																															BgL_fz00_7645;
																																														goto
																																															BgL_tagzd2176zd2_1594;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_7644);
																																												}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_arg2197z00_2292;
																																										obj_t
																																											BgL_arg2198z00_2293;
																																										BgL_arg2197z00_2292
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										BgL_arg2198z00_2293
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										{
																																											BgL_ev_appz00_bglt
																																												BgL_auxz00_7652;
																																											{
																																												obj_t
																																													BgL_argsz00_7654;
																																												obj_t
																																													BgL_fz00_7653;
																																												BgL_fz00_7653
																																													=
																																													BgL_arg2197z00_2292;
																																												BgL_argsz00_7654
																																													=
																																													BgL_arg2198z00_2293;
																																												BgL_argsz00_1593
																																													=
																																													BgL_argsz00_7654;
																																												BgL_fz00_1592
																																													=
																																													BgL_fz00_7653;
																																												goto
																																													BgL_tagzd2176zd2_1594;
																																											}
																																											return
																																												(
																																												(obj_t)
																																												BgL_auxz00_7652);
																																										}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								obj_t
																																									BgL_arg2199z00_2294;
																																								obj_t
																																									BgL_arg2200z00_2295;
																																								BgL_arg2199z00_2294
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								BgL_arg2200z00_2295
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_ez00_69));
																																								{
																																									BgL_ev_appz00_bglt
																																										BgL_auxz00_7660;
																																									{
																																										obj_t
																																											BgL_argsz00_7662;
																																										obj_t
																																											BgL_fz00_7661;
																																										BgL_fz00_7661
																																											=
																																											BgL_arg2199z00_2294;
																																										BgL_argsz00_7662
																																											=
																																											BgL_arg2200z00_2295;
																																										BgL_argsz00_1593
																																											=
																																											BgL_argsz00_7662;
																																										BgL_fz00_1592
																																											=
																																											BgL_fz00_7661;
																																										goto
																																											BgL_tagzd2176zd2_1594;
																																									}
																																									return
																																										(
																																										(obj_t)
																																										BgL_auxz00_7660);
																																								}
																																							}
																																					}
																																				else
																																					{	/* Eval/evaluate.scm 424 */
																																						obj_t
																																							BgL_cdrzd213398zd2_2296;
																																						BgL_cdrzd213398zd2_2296
																																							=
																																							CDR
																																							(((obj_t) BgL_ez00_69));
																																						if (
																																							(CAR
																																								(((obj_t) BgL_ez00_69)) == BGl_symbol3009z00zz__evaluatez00))
																																							{	/* Eval/evaluate.scm 424 */
																																								if (PAIRP(BgL_cdrzd213398zd2_2296))
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_arg2204z00_2300;
																																										obj_t
																																											BgL_arg2205z00_2301;
																																										BgL_arg2204z00_2300
																																											=
																																											CAR
																																											(BgL_cdrzd213398zd2_2296);
																																										BgL_arg2205z00_2301
																																											=
																																											CDR
																																											(BgL_cdrzd213398zd2_2296);
																																										{	/* Eval/evaluate.scm 579 */
																																											BgL_ev_unwindzd2protectzd2_bglt
																																												BgL_new1158z00_4486;
																																											{	/* Eval/evaluate.scm 580 */
																																												BgL_ev_unwindzd2protectzd2_bglt
																																													BgL_new1157z00_4487;
																																												BgL_new1157z00_4487
																																													=
																																													(
																																													(BgL_ev_unwindzd2protectzd2_bglt)
																																													BOBJECT
																																													(GC_MALLOC
																																														(sizeof
																																															(struct
																																																BgL_ev_unwindzd2protectzd2_bgl))));
																																												{	/* Eval/evaluate.scm 580 */
																																													long
																																														BgL_arg2428z00_4488;
																																													BgL_arg2428z00_4488
																																														=
																																														BGL_CLASS_NUM
																																														(BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00);
																																													BGL_OBJECT_CLASS_NUM_SET
																																														(
																																														((BgL_objectz00_bglt) BgL_new1157z00_4487), BgL_arg2428z00_4488);
																																												}
																																												BgL_new1158z00_4486
																																													=
																																													BgL_new1157z00_4487;
																																											}
																																											{
																																												BgL_ev_exprz00_bglt
																																													BgL_auxz00_7678;
																																												{	/* Eval/evaluate.scm 380 */
																																													obj_t
																																														BgL_arg2446z00_4492;
																																													BgL_arg2446z00_4492
																																														=
																																														BGl_getzd2locationzd2zz__evaluatez00
																																														(BgL_arg2204z00_2300,
																																														BgL_locz00_74);
																																													BgL_auxz00_7678
																																														=
																																														(
																																														(BgL_ev_exprz00_bglt)
																																														BGl_uconvzf2locze70z15zz__evaluatez00
																																														(BgL_wherez00_73,
																																															BgL_globalsz00_71,
																																															BgL_localsz00_70,
																																															BgL_arg2204z00_2300,
																																															BgL_arg2446z00_4492));
																																												}
																																												((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(BgL_new1158z00_4486))->BgL_ez00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_7678), BUNSPEC);
																																											}
																																											((((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(BgL_new1158z00_4486))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_arg2205z00_2301, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																											return
																																												(
																																												(obj_t)
																																												BgL_new1158z00_4486);
																																										}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_arg2206z00_2302;
																																										BgL_arg2206z00_2302
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										{
																																											BgL_ev_appz00_bglt
																																												BgL_auxz00_7689;
																																											{
																																												obj_t
																																													BgL_argsz00_7691;
																																												obj_t
																																													BgL_fz00_7690;
																																												BgL_fz00_7690
																																													=
																																													BgL_arg2206z00_2302;
																																												BgL_argsz00_7691
																																													=
																																													BgL_cdrzd213398zd2_2296;
																																												BgL_argsz00_1593
																																													=
																																													BgL_argsz00_7691;
																																												BgL_fz00_1592
																																													=
																																													BgL_fz00_7690;
																																												goto
																																													BgL_tagzd2176zd2_1594;
																																											}
																																											return
																																												(
																																												(obj_t)
																																												BgL_auxz00_7689);
																																										}
																																									}
																																							}
																																						else
																																							{	/* Eval/evaluate.scm 424 */
																																								if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3011z00zz__evaluatez00))
																																									{	/* Eval/evaluate.scm 424 */
																																										if (PAIRP(BgL_cdrzd213398zd2_2296))
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg2211z00_2308;
																																												obj_t
																																													BgL_arg2212z00_2309;
																																												BgL_arg2211z00_2308
																																													=
																																													CAR
																																													(BgL_cdrzd213398zd2_2296);
																																												BgL_arg2212z00_2309
																																													=
																																													CDR
																																													(BgL_cdrzd213398zd2_2296);
																																												{	/* Eval/evaluate.scm 583 */
																																													BgL_ev_withzd2handlerzd2_bglt
																																														BgL_new1160z00_4499;
																																													{	/* Eval/evaluate.scm 584 */
																																														BgL_ev_withzd2handlerzd2_bglt
																																															BgL_new1159z00_4500;
																																														BgL_new1159z00_4500
																																															=
																																															(
																																															(BgL_ev_withzd2handlerzd2_bglt)
																																															BOBJECT
																																															(GC_MALLOC
																																																(sizeof
																																																	(struct
																																																		BgL_ev_withzd2handlerzd2_bgl))));
																																														{	/* Eval/evaluate.scm 584 */
																																															long
																																																BgL_arg2429z00_4501;
																																															BgL_arg2429z00_4501
																																																=
																																																BGL_CLASS_NUM
																																																(BGl_ev_withzd2handlerzd2zz__evaluate_typesz00);
																																															BGL_OBJECT_CLASS_NUM_SET
																																																(
																																																((BgL_objectz00_bglt) BgL_new1159z00_4500), BgL_arg2429z00_4501);
																																														}
																																														BgL_new1160z00_4499
																																															=
																																															BgL_new1159z00_4500;
																																													}
																																													{
																																														BgL_ev_exprz00_bglt
																																															BgL_auxz00_7705;
																																														{	/* Eval/evaluate.scm 380 */
																																															obj_t
																																																BgL_arg2446z00_4505;
																																															BgL_arg2446z00_4505
																																																=
																																																BGl_getzd2locationzd2zz__evaluatez00
																																																(BgL_arg2211z00_2308,
																																																BgL_locz00_74);
																																															BgL_auxz00_7705
																																																=
																																																(
																																																(BgL_ev_exprz00_bglt)
																																																BGl_uconvzf2locze70z15zz__evaluatez00
																																																(BgL_wherez00_73,
																																																	BgL_globalsz00_71,
																																																	BgL_localsz00_70,
																																																	BgL_arg2211z00_2308,
																																																	BgL_arg2446z00_4505));
																																														}
																																														((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(BgL_new1160z00_4499))->BgL_handlerz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_7705), BUNSPEC);
																																													}
																																													((((BgL_ev_withzd2handlerzd2_bglt) COBJECT(BgL_new1160z00_4499))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_arg2212z00_2309, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																													return
																																														(
																																														(obj_t)
																																														BgL_new1160z00_4499);
																																												}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												obj_t
																																													BgL_arg2213z00_2310;
																																												obj_t
																																													BgL_arg2214z00_2311;
																																												BgL_arg2213z00_2310
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												BgL_arg2214z00_2311
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_ez00_69));
																																												{
																																													BgL_ev_appz00_bglt
																																														BgL_auxz00_7718;
																																													{
																																														obj_t
																																															BgL_argsz00_7720;
																																														obj_t
																																															BgL_fz00_7719;
																																														BgL_fz00_7719
																																															=
																																															BgL_arg2213z00_2310;
																																														BgL_argsz00_7720
																																															=
																																															BgL_arg2214z00_2311;
																																														BgL_argsz00_1593
																																															=
																																															BgL_argsz00_7720;
																																														BgL_fz00_1592
																																															=
																																															BgL_fz00_7719;
																																														goto
																																															BgL_tagzd2176zd2_1594;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_7718);
																																												}
																																											}
																																									}
																																								else
																																									{	/* Eval/evaluate.scm 424 */
																																										obj_t
																																											BgL_cdrzd213505zd2_2312;
																																										BgL_cdrzd213505zd2_2312
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_ez00_69));
																																										if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3013z00zz__evaluatez00))
																																											{	/* Eval/evaluate.scm 424 */
																																												if (PAIRP(BgL_cdrzd213505zd2_2312))
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_cdrzd213510zd2_2316;
																																														BgL_cdrzd213510zd2_2316
																																															=
																																															CDR
																																															(BgL_cdrzd213505zd2_2312);
																																														if (PAIRP(BgL_cdrzd213510zd2_2316))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd213515zd2_2318;
																																																BgL_cdrzd213515zd2_2318
																																																	=
																																																	CDR
																																																	(BgL_cdrzd213510zd2_2316);
																																																if ((CAR(BgL_cdrzd213510zd2_2316) == BGl_keyword3015z00zz__evaluatez00))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		if (PAIRP(BgL_cdrzd213515zd2_2318))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2222z00_2322;
																																																				obj_t
																																																					BgL_arg2223z00_2323;
																																																				obj_t
																																																					BgL_arg2224z00_2324;
																																																				BgL_arg2222z00_2322
																																																					=
																																																					CAR
																																																					(BgL_cdrzd213505zd2_2312);
																																																				BgL_arg2223z00_2323
																																																					=
																																																					CAR
																																																					(BgL_cdrzd213515zd2_2318);
																																																				BgL_arg2224z00_2324
																																																					=
																																																					CDR
																																																					(BgL_cdrzd213515zd2_2318);
																																																				{	/* Eval/evaluate.scm 587 */
																																																					BgL_ev_synchroniza7eza7_bglt
																																																						BgL_new1162z00_4516;
																																																					{	/* Eval/evaluate.scm 588 */
																																																						BgL_ev_synchroniza7eza7_bglt
																																																							BgL_new1161z00_4517;
																																																						BgL_new1161z00_4517
																																																							=
																																																							(
																																																							(BgL_ev_synchroniza7eza7_bglt)
																																																							BOBJECT
																																																							(GC_MALLOC
																																																								(sizeof
																																																									(struct
																																																										BgL_ev_synchroniza7eza7_bgl))));
																																																						{	/* Eval/evaluate.scm 588 */
																																																							long
																																																								BgL_arg2430z00_4518;
																																																							BgL_arg2430z00_4518
																																																								=
																																																								BGL_CLASS_NUM
																																																								(BGl_ev_synchroniza7eza7zz__evaluate_typesz00);
																																																							BGL_OBJECT_CLASS_NUM_SET
																																																								(
																																																								((BgL_objectz00_bglt) BgL_new1161z00_4517), BgL_arg2430z00_4518);
																																																						}
																																																						BgL_new1162z00_4516
																																																							=
																																																							BgL_new1161z00_4517;
																																																					}
																																																					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4516))->BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
																																																					{
																																																						BgL_ev_exprz00_bglt
																																																							BgL_auxz00_7747;
																																																						{	/* Eval/evaluate.scm 380 */
																																																							obj_t
																																																								BgL_arg2446z00_4522;
																																																							BgL_arg2446z00_4522
																																																								=
																																																								BGl_getzd2locationzd2zz__evaluatez00
																																																								(BgL_arg2222z00_2322,
																																																								BgL_locz00_74);
																																																							BgL_auxz00_7747
																																																								=
																																																								(
																																																								(BgL_ev_exprz00_bglt)
																																																								BGl_uconvzf2locze70z15zz__evaluatez00
																																																								(BgL_wherez00_73,
																																																									BgL_globalsz00_71,
																																																									BgL_localsz00_70,
																																																									BgL_arg2222z00_2322,
																																																									BgL_arg2446z00_4522));
																																																						}
																																																						((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4516))->BgL_mutexz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_7747), BUNSPEC);
																																																					}
																																																					{
																																																						BgL_ev_exprz00_bglt
																																																							BgL_auxz00_7752;
																																																						{	/* Eval/evaluate.scm 380 */
																																																							obj_t
																																																								BgL_arg2446z00_4523;
																																																							BgL_arg2446z00_4523
																																																								=
																																																								BGl_getzd2locationzd2zz__evaluatez00
																																																								(BgL_arg2223z00_2323,
																																																								BgL_locz00_74);
																																																							BgL_auxz00_7752
																																																								=
																																																								(
																																																								(BgL_ev_exprz00_bglt)
																																																								BGl_uconvzf2locze70z15zz__evaluatez00
																																																								(BgL_wherez00_73,
																																																									BgL_globalsz00_71,
																																																									BgL_localsz00_70,
																																																									BgL_arg2223z00_2323,
																																																									BgL_arg2446z00_4523));
																																																						}
																																																						((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4516))->BgL_prelockz00) = ((BgL_ev_exprz00_bglt) BgL_auxz00_7752), BUNSPEC);
																																																					}
																																																					((((BgL_ev_synchroniza7eza7_bglt) COBJECT(BgL_new1162z00_4516))->BgL_bodyz00) = ((BgL_ev_exprz00_bglt) ((BgL_ev_exprz00_bglt) BGl_convzd2beginzd2zz__evaluatez00(BgL_arg2224z00_2324, BgL_localsz00_70, BgL_globalsz00_71, BFALSE, BgL_wherez00_73, BgL_locz00_74, ((bool_t) 0)))), BUNSPEC);
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_new1162z00_4516);
																																																				}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2225z00_2326;
																																																				obj_t
																																																					BgL_arg2226z00_2327;
																																																				BgL_arg2225z00_2326
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd213505zd2_2312));
																																																				BgL_arg2226z00_2327
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd213505zd2_2312));
																																																				{
																																																					BgL_ev_synchroniza7eza7_bglt
																																																						BgL_auxz00_7765;
																																																					{
																																																						obj_t
																																																							BgL_bodyz00_7767;
																																																						obj_t
																																																							BgL_mz00_7766;
																																																						BgL_mz00_7766
																																																							=
																																																							BgL_arg2225z00_2326;
																																																						BgL_bodyz00_7767
																																																							=
																																																							BgL_arg2226z00_2327;
																																																						BgL_bodyz00_1586
																																																							=
																																																							BgL_bodyz00_7767;
																																																						BgL_mz00_1585
																																																							=
																																																							BgL_mz00_7766;
																																																						goto
																																																							BgL_tagzd2173zd2_1587;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7765);
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg2227z00_2329;
																																																		obj_t
																																																			BgL_arg2228z00_2330;
																																																		BgL_arg2227z00_2329
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd213505zd2_2312));
																																																		BgL_arg2228z00_2330
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd213505zd2_2312));
																																																		{
																																																			BgL_ev_synchroniza7eza7_bglt
																																																				BgL_auxz00_7773;
																																																			{
																																																				obj_t
																																																					BgL_bodyz00_7775;
																																																				obj_t
																																																					BgL_mz00_7774;
																																																				BgL_mz00_7774
																																																					=
																																																					BgL_arg2227z00_2329;
																																																				BgL_bodyz00_7775
																																																					=
																																																					BgL_arg2228z00_2330;
																																																				BgL_bodyz00_1586
																																																					=
																																																					BgL_bodyz00_7775;
																																																				BgL_mz00_1585
																																																					=
																																																					BgL_mz00_7774;
																																																				goto
																																																					BgL_tagzd2173zd2_1587;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_7773);
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg2230z00_2333;
																																																obj_t
																																																	BgL_arg2231z00_2334;
																																																BgL_arg2230z00_2333
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd213505zd2_2312));
																																																BgL_arg2231z00_2334
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd213505zd2_2312));
																																																{
																																																	BgL_ev_synchroniza7eza7_bglt
																																																		BgL_auxz00_7781;
																																																	{
																																																		obj_t
																																																			BgL_bodyz00_7783;
																																																		obj_t
																																																			BgL_mz00_7782;
																																																		BgL_mz00_7782
																																																			=
																																																			BgL_arg2230z00_2333;
																																																		BgL_bodyz00_7783
																																																			=
																																																			BgL_arg2231z00_2334;
																																																		BgL_bodyz00_1586
																																																			=
																																																			BgL_bodyz00_7783;
																																																		BgL_mz00_1585
																																																			=
																																																			BgL_mz00_7782;
																																																		goto
																																																			BgL_tagzd2173zd2_1587;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_7781);
																																																}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														obj_t
																																															BgL_arg2232z00_2335;
																																														BgL_arg2232z00_2335
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_ez00_69));
																																														{
																																															BgL_ev_appz00_bglt
																																																BgL_auxz00_7787;
																																															{
																																																obj_t
																																																	BgL_argsz00_7789;
																																																obj_t
																																																	BgL_fz00_7788;
																																																BgL_fz00_7788
																																																	=
																																																	BgL_arg2232z00_2335;
																																																BgL_argsz00_7789
																																																	=
																																																	BgL_cdrzd213505zd2_2312;
																																																BgL_argsz00_1593
																																																	=
																																																	BgL_argsz00_7789;
																																																BgL_fz00_1592
																																																	=
																																																	BgL_fz00_7788;
																																																goto
																																																	BgL_tagzd2176zd2_1594;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_7787);
																																														}
																																													}
																																											}
																																										else
																																											{	/* Eval/evaluate.scm 424 */
																																												if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol2970z00zz__evaluatez00))
																																													{	/* Eval/evaluate.scm 424 */
																																														if (PAIRP(BgL_cdrzd213505zd2_2312))
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_cdrzd213617zd2_2341;
																																																BgL_cdrzd213617zd2_2341
																																																	=
																																																	CDR
																																																	(BgL_cdrzd213505zd2_2312);
																																																if (PAIRP(BgL_cdrzd213617zd2_2341))
																																																	{	/* Eval/evaluate.scm 424 */
																																																		if (NULLP(CDR(BgL_cdrzd213617zd2_2341)))
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2240z00_2345;
																																																				obj_t
																																																					BgL_arg2241z00_2346;
																																																				BgL_arg2240z00_2345
																																																					=
																																																					CAR
																																																					(BgL_cdrzd213505zd2_2312);
																																																				BgL_arg2241z00_2346
																																																					=
																																																					CAR
																																																					(BgL_cdrzd213617zd2_2341);
																																																				{	/* Eval/evaluate.scm 599 */
																																																					obj_t
																																																						BgL_arg2432z00_4541;
																																																					{	/* Eval/evaluate.scm 599 */
																																																						obj_t
																																																							BgL_arg2434z00_4542;
																																																						{	/* Eval/evaluate.scm 599 */
																																																							obj_t
																																																								BgL_arg2435z00_4543;
																																																							obj_t
																																																								BgL_arg2437z00_4544;
																																																							{	/* Eval/evaluate.scm 599 */
																																																								obj_t
																																																									BgL_symbolz00_4545;
																																																								BgL_symbolz00_4545
																																																									=
																																																									BGl_symbol3017z00zz__evaluatez00;
																																																								{	/* Eval/evaluate.scm 599 */
																																																									obj_t
																																																										BgL_arg2648z00_4546;
																																																									BgL_arg2648z00_4546
																																																										=
																																																										SYMBOL_TO_STRING
																																																										(BgL_symbolz00_4545);
																																																									BgL_arg2435z00_4543
																																																										=
																																																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																										(BgL_arg2648z00_4546);
																																																								}
																																																							}
																																																							{	/* Eval/evaluate.scm 599 */
																																																								obj_t
																																																									BgL_arg2648z00_4548;
																																																								BgL_arg2648z00_4548
																																																									=
																																																									SYMBOL_TO_STRING
																																																									(
																																																									((obj_t) BgL_wherez00_73));
																																																								BgL_arg2437z00_4544
																																																									=
																																																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																									(BgL_arg2648z00_4548);
																																																							}
																																																							BgL_arg2434z00_4542
																																																								=
																																																								string_append
																																																								(BgL_arg2435z00_4543,
																																																								BgL_arg2437z00_4544);
																																																						}
																																																						BgL_arg2432z00_4541
																																																							=
																																																							bstring_to_symbol
																																																							(BgL_arg2434z00_4542);
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BGl_convzd2lambdaze70z35zz__evaluatez00
																																																						(BgL_globalsz00_71,
																																																							BgL_localsz00_70,
																																																							BgL_ez00_69,
																																																							BgL_locz00_74,
																																																							BgL_arg2240z00_2345,
																																																							BgL_arg2241z00_2346,
																																																							BgL_arg2432z00_4541,
																																																							BFALSE));
																																																				}
																																																			}
																																																		else
																																																			{	/* Eval/evaluate.scm 424 */
																																																				obj_t
																																																					BgL_arg2242z00_2347;
																																																				obj_t
																																																					BgL_arg2243z00_2348;
																																																				BgL_arg2242z00_2347
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				BgL_arg2243z00_2348
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_ez00_69));
																																																				{
																																																					BgL_ev_appz00_bglt
																																																						BgL_auxz00_7818;
																																																					{
																																																						obj_t
																																																							BgL_argsz00_7820;
																																																						obj_t
																																																							BgL_fz00_7819;
																																																						BgL_fz00_7819
																																																							=
																																																							BgL_arg2242z00_2347;
																																																						BgL_argsz00_7820
																																																							=
																																																							BgL_arg2243z00_2348;
																																																						BgL_argsz00_1593
																																																							=
																																																							BgL_argsz00_7820;
																																																						BgL_fz00_1592
																																																							=
																																																							BgL_fz00_7819;
																																																						goto
																																																							BgL_tagzd2176zd2_1594;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_7818);
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Eval/evaluate.scm 424 */
																																																		obj_t
																																																			BgL_arg2245z00_2350;
																																																		obj_t
																																																			BgL_arg2246z00_2351;
																																																		BgL_arg2245z00_2350
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		BgL_arg2246z00_2351
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_ez00_69));
																																																		{
																																																			BgL_ev_appz00_bglt
																																																				BgL_auxz00_7826;
																																																			{
																																																				obj_t
																																																					BgL_argsz00_7828;
																																																				obj_t
																																																					BgL_fz00_7827;
																																																				BgL_fz00_7827
																																																					=
																																																					BgL_arg2245z00_2350;
																																																				BgL_argsz00_7828
																																																					=
																																																					BgL_arg2246z00_2351;
																																																				BgL_argsz00_1593
																																																					=
																																																					BgL_argsz00_7828;
																																																				BgL_fz00_1592
																																																					=
																																																					BgL_fz00_7827;
																																																				goto
																																																					BgL_tagzd2176zd2_1594;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_7826);
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg2247z00_2352;
																																																obj_t
																																																	BgL_arg2248z00_2353;
																																																BgL_arg2247z00_2352
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																BgL_arg2248z00_2353
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{
																																																	BgL_ev_appz00_bglt
																																																		BgL_auxz00_7834;
																																																	{
																																																		obj_t
																																																			BgL_argsz00_7836;
																																																		obj_t
																																																			BgL_fz00_7835;
																																																		BgL_fz00_7835
																																																			=
																																																			BgL_arg2247z00_2352;
																																																		BgL_argsz00_7836
																																																			=
																																																			BgL_arg2248z00_2353;
																																																		BgL_argsz00_1593
																																																			=
																																																			BgL_argsz00_7836;
																																																		BgL_fz00_1592
																																																			=
																																																			BgL_fz00_7835;
																																																		goto
																																																			BgL_tagzd2176zd2_1594;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_7834);
																																																}
																																															}
																																													}
																																												else
																																													{	/* Eval/evaluate.scm 424 */
																																														if ((CAR(((obj_t) BgL_ez00_69)) == BGl_symbol3019z00zz__evaluatez00))
																																															{	/* Eval/evaluate.scm 424 */
																																																return
																																																	BGl_errorz00zz__errorz00
																																																	(BGl_string3021z00zz__evaluatez00,
																																																	BGl_string3022z00zz__evaluatez00,
																																																	BgL_ez00_69);
																																															}
																																														else
																																															{	/* Eval/evaluate.scm 424 */
																																																obj_t
																																																	BgL_arg2251z00_2356;
																																																obj_t
																																																	BgL_arg2252z00_2357;
																																																BgL_arg2251z00_2356
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																BgL_arg2252z00_2357
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_ez00_69));
																																																{
																																																	BgL_ev_appz00_bglt
																																																		BgL_auxz00_7847;
																																																	{
																																																		obj_t
																																																			BgL_argsz00_7849;
																																																		obj_t
																																																			BgL_fz00_7848;
																																																		BgL_fz00_7848
																																																			=
																																																			BgL_arg2251z00_2356;
																																																		BgL_argsz00_7849
																																																			=
																																																			BgL_arg2252z00_2357;
																																																		BgL_argsz00_1593
																																																			=
																																																			BgL_argsz00_7849;
																																																		BgL_fz00_1592
																																																			=
																																																			BgL_fz00_7848;
																																																		goto
																																																			BgL_tagzd2176zd2_1594;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_7847);
																																																}
																																															}
																																													}
																																											}
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Eval/evaluate.scm 424 */
						BgL_xz00_1516 = BgL_ez00_69;
						if (SYMBOLP(BgL_xz00_1516))
							{	/* Eval/evaluate.scm 427 */
								obj_t BgL__ortest_1104z00_2379;

								BgL__ortest_1104z00_2379 =
									BGl_convzd2varzd2zz__evaluatez00(BgL_xz00_1516,
									BgL_localsz00_70);
								if (CBOOL(BgL__ortest_1104z00_2379))
									{	/* Eval/evaluate.scm 427 */
										return BgL__ortest_1104z00_2379;
									}
								else
									{	/* Eval/evaluate.scm 195 */
										BgL_ev_globalz00_bglt BgL_new1078z00_3575;

										{	/* Eval/evaluate.scm 196 */
											BgL_ev_globalz00_bglt BgL_new1077z00_3576;

											BgL_new1077z00_3576 =
												((BgL_ev_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_ev_globalz00_bgl))));
											{	/* Eval/evaluate.scm 196 */
												long BgL_arg1399z00_3577;

												BgL_arg1399z00_3577 =
													BGL_CLASS_NUM(BGl_ev_globalz00zz__evaluate_typesz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1077z00_3576),
													BgL_arg1399z00_3577);
											}
											BgL_new1078z00_3575 = BgL_new1077z00_3576;
										}
										((((BgL_ev_globalz00_bglt) COBJECT(BgL_new1078z00_3575))->
												BgL_locz00) = ((obj_t) BgL_locz00_74), BUNSPEC);
										((((BgL_ev_globalz00_bglt) COBJECT(BgL_new1078z00_3575))->
												BgL_namez00) = ((obj_t) BgL_xz00_1516), BUNSPEC);
										{
											obj_t BgL_auxz00_7862;

											if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_globalsz00_71))
												{	/* Eval/evaluate.scm 198 */
													BgL_auxz00_7862 = BgL_globalsz00_71;
												}
											else
												{	/* Eval/evaluate.scm 198 */
													BgL_auxz00_7862 = BGL_MODULE();
												}
											((((BgL_ev_globalz00_bglt) COBJECT(BgL_new1078z00_3575))->
													BgL_modz00) = ((obj_t) BgL_auxz00_7862), BUNSPEC);
										}
										return ((obj_t) BgL_new1078z00_3575);
									}
							}
						else
							{	/* Eval/evaluate.scm 428 */
								BgL_ev_littz00_bglt BgL_new1106z00_2380;

								{	/* Eval/evaluate.scm 429 */
									BgL_ev_littz00_bglt BgL_new1105z00_2381;

									BgL_new1105z00_2381 =
										((BgL_ev_littz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_ev_littz00_bgl))));
									{	/* Eval/evaluate.scm 429 */
										long BgL_arg2274z00_2382;

										BgL_arg2274z00_2382 =
											BGL_CLASS_NUM(BGl_ev_littz00zz__evaluate_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1105z00_2381),
											BgL_arg2274z00_2382);
									}
									BgL_new1106z00_2380 = BgL_new1105z00_2381;
								}
								((((BgL_ev_littz00_bglt) COBJECT(BgL_new1106z00_2380))->
										BgL_valuez00) = ((obj_t) BgL_xz00_1516), BUNSPEC);
								return ((obj_t) BgL_new1106z00_2380);
							}
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__evaluatez00(obj_t BgL_wherez00_4880,
		obj_t BgL_globalsz00_4879, obj_t BgL_localsz00_4878, obj_t BgL_locz00_4877,
		obj_t BgL_esz00_2716)
	{
		{	/* Eval/evaluate.scm 386 */
			if (NULLP(BgL_esz00_2716))
				{	/* Eval/evaluate.scm 387 */
					return BNIL;
				}
			else
				{	/* Eval/evaluate.scm 389 */
					obj_t BgL_ez00_2719;

					BgL_ez00_2719 = CAR(((obj_t) BgL_esz00_2716));
					{	/* Eval/evaluate.scm 390 */
						obj_t BgL_arg2453z00_2720;
						obj_t BgL_arg2455z00_2721;

						{	/* Eval/evaluate.scm 390 */
							obj_t BgL_arg2456z00_2722;

							{	/* Eval/evaluate.scm 150 */
								obj_t BgL__ortest_1070z00_3554;

								BgL__ortest_1070z00_3554 =
									BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_ez00_2719);
								if (CBOOL(BgL__ortest_1070z00_3554))
									{	/* Eval/evaluate.scm 150 */
										BgL_arg2456z00_2722 = BgL__ortest_1070z00_3554;
									}
								else
									{	/* Eval/evaluate.scm 151 */
										obj_t BgL__ortest_1071z00_3555;

										BgL__ortest_1071z00_3555 =
											BGl_getzd2sourcezd2locationz00zz__readerz00
											(BgL_esz00_2716);
										if (CBOOL(BgL__ortest_1071z00_3555))
											{	/* Eval/evaluate.scm 151 */
												BgL_arg2456z00_2722 = BgL__ortest_1071z00_3555;
											}
										else
											{	/* Eval/evaluate.scm 151 */
												BgL_arg2456z00_2722 = BgL_locz00_4877;
											}
									}
							}
							BgL_arg2453z00_2720 =
								BGl_convz00zz__evaluatez00(BgL_ez00_2719, BgL_localsz00_4878,
								BgL_globalsz00_4879, BFALSE, BgL_wherez00_4880,
								BgL_arg2456z00_2722, ((bool_t) 0));
						}
						{	/* Eval/evaluate.scm 391 */
							obj_t BgL_arg2457z00_2723;

							BgL_arg2457z00_2723 = CDR(((obj_t) BgL_esz00_2716));
							BgL_arg2455z00_2721 =
								BGl_loopze70ze7zz__evaluatez00(BgL_wherez00_4880,
								BgL_globalsz00_4879, BgL_localsz00_4878, BgL_locz00_4877,
								BgL_arg2457z00_2723);
						}
						return MAKE_YOUNG_PAIR(BgL_arg2453z00_2720, BgL_arg2455z00_2721);
					}
				}
		}

	}



/* conv-vals~0 */
	obj_t BGl_convzd2valsze70z35zz__evaluatez00(obj_t BgL_wherez00_4882,
		obj_t BgL_globalsz00_4881, obj_t BgL_lz00_2528, obj_t BgL_varsz00_2529,
		obj_t BgL_localsz00_2530, obj_t BgL_locz00_2531)
	{
		{	/* Eval/evaluate.scm 502 */
			if (NULLP(BgL_lz00_2528))
				{	/* Eval/evaluate.scm 498 */
					return BNIL;
				}
			else
				{	/* Eval/evaluate.scm 500 */
					obj_t BgL_locz00_3707;

					{	/* Eval/evaluate.scm 500 */
						obj_t BgL_arg2356z00_3708;

						BgL_arg2356z00_3708 = CAR(((obj_t) BgL_lz00_2528));
						{	/* Eval/evaluate.scm 144 */
							obj_t BgL__ortest_1069z00_3717;

							BgL__ortest_1069z00_3717 =
								BGl_getzd2sourcezd2locationz00zz__readerz00
								(BgL_arg2356z00_3708);
							if (CBOOL(BgL__ortest_1069z00_3717))
								{	/* Eval/evaluate.scm 144 */
									BgL_locz00_3707 = BgL__ortest_1069z00_3717;
								}
							else
								{	/* Eval/evaluate.scm 144 */
									BgL_locz00_3707 = BgL_locz00_2531;
								}
						}
					}
					{	/* Eval/evaluate.scm 501 */
						obj_t BgL_arg2349z00_3709;
						obj_t BgL_arg2350z00_3710;

						{	/* Eval/evaluate.scm 501 */
							obj_t BgL_arg2351z00_3711;

							{	/* Eval/evaluate.scm 501 */
								obj_t BgL_pairz00_3723;

								{	/* Eval/evaluate.scm 501 */
									obj_t BgL_pairz00_3722;

									BgL_pairz00_3722 = CAR(((obj_t) BgL_lz00_2528));
									BgL_pairz00_3723 = CDR(BgL_pairz00_3722);
								}
								BgL_arg2351z00_3711 = CAR(BgL_pairz00_3723);
							}
							BgL_arg2349z00_3709 =
								BGl_convz00zz__evaluatez00(BgL_arg2351z00_3711,
								BgL_localsz00_2530, BgL_globalsz00_4881, BFALSE,
								BgL_wherez00_4882, BgL_locz00_3707, ((bool_t) 0));
						}
						{	/* Eval/evaluate.scm 502 */
							obj_t BgL_arg2352z00_3712;
							obj_t BgL_arg2353z00_3713;
							obj_t BgL_arg2354z00_3714;

							BgL_arg2352z00_3712 = CDR(((obj_t) BgL_lz00_2528));
							BgL_arg2353z00_3713 = CDR(((obj_t) BgL_varsz00_2529));
							{	/* Eval/evaluate.scm 502 */
								obj_t BgL_arg2355z00_3715;

								BgL_arg2355z00_3715 = CAR(((obj_t) BgL_varsz00_2529));
								BgL_arg2354z00_3714 =
									MAKE_YOUNG_PAIR(BgL_arg2355z00_3715, BgL_localsz00_2530);
							}
							BgL_arg2350z00_3710 =
								BGl_convzd2valsze70z35zz__evaluatez00(BgL_wherez00_4882,
								BgL_globalsz00_4881, BgL_arg2352z00_3712, BgL_arg2353z00_3713,
								BgL_arg2354z00_3714, BgL_locz00_3707);
						}
						return MAKE_YOUNG_PAIR(BgL_arg2349z00_3709, BgL_arg2350z00_3710);
					}
				}
		}

	}



/* conv-lambda~0 */
	BgL_ev_absz00_bglt BGl_convzd2lambdaze70z35zz__evaluatez00(obj_t
		BgL_globalsz00_4886, obj_t BgL_localsz00_4885, obj_t BgL_ez00_4884,
		obj_t BgL_locz00_4883, obj_t BgL_formalsz00_2725, obj_t BgL_bodyz00_2726,
		obj_t BgL_wherez00_2727, obj_t BgL_typez00_2728)
	{
		{	/* Eval/evaluate.scm 404 */
			{
				obj_t BgL_lz00_2759;

				{	/* Eval/evaluate.scm 406 */
					obj_t BgL_argsz00_2731;

					BgL_lz00_2759 =
						BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
						(BgL_formalsz00_2725, BGl_errorzd2envzd2zz__errorz00, ((bool_t) 1));
					{
						obj_t BgL_rz00_2763;
						obj_t BgL_flatz00_2764;
						long BgL_arityz00_2765;

						BgL_rz00_2763 = BgL_lz00_2759;
						BgL_flatz00_2764 = BNIL;
						BgL_arityz00_2765 = 0L;
					BgL_zc3z04anonymousza32475ze3z87_2766:
						if (NULLP(BgL_rz00_2763))
							{	/* Eval/evaluate.scm 399 */
								obj_t BgL_val0_1208z00_2768;

								BgL_val0_1208z00_2768 = bgl_reverse_bang(BgL_flatz00_2764);
								{	/* Eval/evaluate.scm 399 */
									int BgL_tmpz00_7913;

									BgL_tmpz00_7913 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7913);
								}
								{	/* Eval/evaluate.scm 399 */
									obj_t BgL_auxz00_7918;
									int BgL_tmpz00_7916;

									BgL_auxz00_7918 = BINT(BgL_arityz00_2765);
									BgL_tmpz00_7916 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7916, BgL_auxz00_7918);
								}
								BgL_argsz00_2731 = BgL_val0_1208z00_2768;
							}
						else
							{	/* Eval/evaluate.scm 398 */
								if (PAIRP(BgL_rz00_2763))
									{	/* Eval/evaluate.scm 403 */
										obj_t BgL_arg2479z00_2771;
										obj_t BgL_arg2480z00_2772;
										long BgL_arg2481z00_2773;

										BgL_arg2479z00_2771 = CDR(BgL_rz00_2763);
										BgL_arg2480z00_2772 =
											MAKE_YOUNG_PAIR(BGl_untypezd2identzd2zz__evaluatez00(CAR
												(BgL_rz00_2763)), BgL_flatz00_2764);
										BgL_arg2481z00_2773 = (BgL_arityz00_2765 + 1L);
										{
											long BgL_arityz00_7930;
											obj_t BgL_flatz00_7929;
											obj_t BgL_rz00_7928;

											BgL_rz00_7928 = BgL_arg2479z00_2771;
											BgL_flatz00_7929 = BgL_arg2480z00_2772;
											BgL_arityz00_7930 = BgL_arg2481z00_2773;
											BgL_arityz00_2765 = BgL_arityz00_7930;
											BgL_flatz00_2764 = BgL_flatz00_7929;
											BgL_rz00_2763 = BgL_rz00_7928;
											goto BgL_zc3z04anonymousza32475ze3z87_2766;
										}
									}
								else
									{	/* Eval/evaluate.scm 401 */
										obj_t BgL_val0_1210z00_2776;
										long BgL_val1_1211z00_2777;

										{	/* Eval/evaluate.scm 401 */
											obj_t BgL_arg2484z00_2778;

											BgL_arg2484z00_2778 =
												MAKE_YOUNG_PAIR(BGl_untypezd2identzd2zz__evaluatez00
												(BgL_rz00_2763), BgL_flatz00_2764);
											BgL_val0_1210z00_2776 =
												bgl_reverse_bang(BgL_arg2484z00_2778);
										}
										BgL_val1_1211z00_2777 = (-1L - BgL_arityz00_2765);
										{	/* Eval/evaluate.scm 401 */
											int BgL_tmpz00_7935;

											BgL_tmpz00_7935 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7935);
										}
										{	/* Eval/evaluate.scm 401 */
											obj_t BgL_auxz00_7940;
											int BgL_tmpz00_7938;

											BgL_auxz00_7940 = BINT(BgL_val1_1211z00_2777);
											BgL_tmpz00_7938 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_7938, BgL_auxz00_7940);
										}
										BgL_argsz00_2731 = BgL_val0_1210z00_2776;
					}}}
					{	/* Eval/evaluate.scm 407 */
						obj_t BgL_arityz00_2732;

						{	/* Eval/evaluate.scm 408 */
							obj_t BgL_tmpz00_3561;

							{	/* Eval/evaluate.scm 408 */
								int BgL_tmpz00_7944;

								BgL_tmpz00_7944 = (int) (1L);
								BgL_tmpz00_3561 = BGL_MVALUES_VAL(BgL_tmpz00_7944);
							}
							{	/* Eval/evaluate.scm 408 */
								int BgL_tmpz00_7947;

								BgL_tmpz00_7947 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_7947, BUNSPEC);
							}
							BgL_arityz00_2732 = BgL_tmpz00_3561;
						}
						{	/* Eval/evaluate.scm 408 */
							obj_t BgL_varsz00_2733;
							obj_t BgL_bodyz00_2734;
							obj_t BgL_nlocz00_2735;

							if (NULLP(BgL_argsz00_2731))
								{	/* Eval/evaluate.scm 408 */
									BgL_varsz00_2733 = BNIL;
								}
							else
								{	/* Eval/evaluate.scm 408 */
									obj_t BgL_head1214z00_2742;

									BgL_head1214z00_2742 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1212z00_2744;
										obj_t BgL_tail1215z00_2745;

										BgL_l1212z00_2744 = BgL_argsz00_2731;
										BgL_tail1215z00_2745 = BgL_head1214z00_2742;
									BgL_zc3z04anonymousza32462ze3z87_2746:
										if (NULLP(BgL_l1212z00_2744))
											{	/* Eval/evaluate.scm 408 */
												BgL_varsz00_2733 = CDR(BgL_head1214z00_2742);
											}
										else
											{	/* Eval/evaluate.scm 408 */
												obj_t BgL_newtail1216z00_2748;

												{	/* Eval/evaluate.scm 408 */
													BgL_ev_varz00_bglt BgL_arg2466z00_2750;

													{	/* Eval/evaluate.scm 408 */
														obj_t BgL_vz00_2751;

														BgL_vz00_2751 = CAR(((obj_t) BgL_l1212z00_2744));
														{	/* Eval/evaluate.scm 409 */
															BgL_ev_varz00_bglt BgL_new1101z00_2752;

															{	/* Eval/evaluate.scm 411 */
																BgL_ev_varz00_bglt BgL_new1100z00_2753;

																BgL_new1100z00_2753 =
																	((BgL_ev_varz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_ev_varz00_bgl))));
																{	/* Eval/evaluate.scm 411 */
																	long BgL_arg2469z00_2754;

																	BgL_arg2469z00_2754 =
																		BGL_CLASS_NUM
																		(BGl_ev_varz00zz__evaluate_typesz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_new1100z00_2753),
																		BgL_arg2469z00_2754);
																}
																BgL_new1101z00_2752 = BgL_new1100z00_2753;
															}
															((((BgL_ev_varz00_bglt)
																		COBJECT(BgL_new1101z00_2752))->
																	BgL_namez00) =
																((obj_t) CAR(((obj_t) BgL_vz00_2751))),
																BUNSPEC);
															((((BgL_ev_varz00_bglt)
																		COBJECT(BgL_new1101z00_2752))->BgL_effz00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_ev_varz00_bglt)
																		COBJECT(BgL_new1101z00_2752))->
																	BgL_typez00) =
																((obj_t) CDR(((obj_t) BgL_vz00_2751))),
																BUNSPEC);
															BgL_arg2466z00_2750 = BgL_new1101z00_2752;
													}}
													BgL_newtail1216z00_2748 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg2466z00_2750), BNIL);
												}
												SET_CDR(BgL_tail1215z00_2745, BgL_newtail1216z00_2748);
												{	/* Eval/evaluate.scm 408 */
													obj_t BgL_arg2465z00_2749;

													BgL_arg2465z00_2749 =
														CDR(((obj_t) BgL_l1212z00_2744));
													{
														obj_t BgL_tail1215z00_7975;
														obj_t BgL_l1212z00_7974;

														BgL_l1212z00_7974 = BgL_arg2465z00_2749;
														BgL_tail1215z00_7975 = BgL_newtail1216z00_2748;
														BgL_tail1215z00_2745 = BgL_tail1215z00_7975;
														BgL_l1212z00_2744 = BgL_l1212z00_7974;
														goto BgL_zc3z04anonymousza32462ze3z87_2746;
													}
												}
											}
									}
								}
							BgL_bodyz00_2734 =
								BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00
								(BgL_ez00_4884, BgL_formalsz00_2725,
								BGl_typezd2checkszd2zz__evaluatez00(BgL_argsz00_2731,
									BgL_argsz00_2731,
									BGl_typezd2resultzd2zz__evaluatez00(BgL_typez00_2728,
										BgL_bodyz00_2726, BgL_locz00_4883), BgL_locz00_4883,
									BgL_wherez00_2727), BGl_errorzd2envzd2zz__errorz00);
							{	/* Eval/evaluate.scm 144 */
								obj_t BgL__ortest_1069z00_3571;

								BgL__ortest_1069z00_3571 =
									BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_bodyz00_2726);
								if (CBOOL(BgL__ortest_1069z00_3571))
									{	/* Eval/evaluate.scm 144 */
										BgL_nlocz00_2735 = BgL__ortest_1069z00_3571;
									}
								else
									{	/* Eval/evaluate.scm 144 */
										BgL_nlocz00_2735 = BgL_locz00_4883;
									}
							}
							{	/* Eval/evaluate.scm 417 */
								BgL_ev_absz00_bglt BgL_new1103z00_2736;

								{	/* Eval/evaluate.scm 418 */
									BgL_ev_absz00_bglt BgL_new1102z00_2738;

									BgL_new1102z00_2738 =
										((BgL_ev_absz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_ev_absz00_bgl))));
									{	/* Eval/evaluate.scm 418 */
										long BgL_arg2460z00_2739;

										BgL_arg2460z00_2739 =
											BGL_CLASS_NUM(BGl_ev_absz00zz__evaluate_typesz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1102z00_2738),
											BgL_arg2460z00_2739);
									}
									BgL_new1103z00_2736 = BgL_new1102z00_2738;
								}
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_locz00) = ((obj_t) BgL_locz00_4883), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_wherez00) = ((obj_t) BgL_wherez00_2727), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_arityz00) = ((obj_t) BgL_arityz00_2732), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_varsz00) = ((obj_t) BgL_varsz00_2733), BUNSPEC);
								{
									BgL_ev_exprz00_bglt BgL_auxz00_7990;

									{	/* Eval/evaluate.scm 422 */
										obj_t BgL_arg2459z00_2737;

										BgL_arg2459z00_2737 =
											BGl_appendzd221011zd2zz__evaluatez00(BgL_varsz00_2733,
											BgL_localsz00_4885);
										BgL_auxz00_7990 =
											((BgL_ev_exprz00_bglt)
											BGl_convz00zz__evaluatez00(BgL_bodyz00_2734,
												BgL_arg2459z00_2737, BgL_globalsz00_4886, BTRUE,
												BgL_wherez00_2727, BgL_nlocz00_2735, ((bool_t) 0)));
									}
									((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
											BgL_bodyz00) =
										((BgL_ev_exprz00_bglt) BgL_auxz00_7990), BUNSPEC);
								}
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_siza7eza7) = ((int) (int) (0L)), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_bindz00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_freez00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_innerz00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_ev_absz00_bglt) COBJECT(BgL_new1103z00_2736))->
										BgL_boxesz00) = ((obj_t) BNIL), BUNSPEC);
								return BgL_new1103z00_2736;
							}
						}
					}
				}
			}
		}

	}



/* uconv/loc~0 */
	obj_t BGl_uconvzf2locze70z15zz__evaluatez00(obj_t BgL_wherez00_4893,
		obj_t BgL_globalsz00_4892, obj_t BgL_localsz00_4891, obj_t BgL_ez00_2703,
		obj_t BgL_locz00_2704)
	{
		{	/* Eval/evaluate.scm 377 */
			return
				BGl_convz00zz__evaluatez00(BgL_ez00_2703, BgL_localsz00_4891,
				BgL_globalsz00_4892, BFALSE, BgL_wherez00_4893, BgL_locz00_2704,
				((bool_t) 0));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evaluatez00(void)
	{
		{	/* Eval/evaluate.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__ppz00(233942021L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(0L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evaluate_avarz00(483789130L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evaluate_fsiza7eza7(121298760L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			BGl_modulezd2initializa7ationz75zz__evaluate_uncompz00(193509656L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
			return BGl_modulezd2initializa7ationz75zz__evaluate_compz00(444000678L,
				BSTRING_TO_STRING(BGl_string3023z00zz__evaluatez00));
		}

	}

#ifdef __cplusplus
}
#endif
