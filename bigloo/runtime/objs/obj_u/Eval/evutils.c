/*===========================================================================*/
/*   (Eval/evutils.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evutils.scm -indent -o objs/obj_u/Eval/evutils.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVUTILS_TYPE_DEFINITIONS
#define BGL___EVUTILS_TYPE_DEFINITIONS
#endif													// BGL___EVUTILS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__evutilsz00 = BUNSPEC;
	static obj_t BGl_z62bindingszd2ze3listz53zz__evutilsz00(obj_t, obj_t);
	extern obj_t BGl_errorzf2sourcezd2locationz20zz__errorz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evutilsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	static obj_t BGl_cnstzd2initzd2zz__evutilsz00(void);
	static obj_t BGl_genericzd2initzd2zz__evutilsz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evutilsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evutilsz00(void);
	BGL_EXPORTED_DECL obj_t BGl_argszd2ze3listz31zz__evutilsz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__evutilsz00(void);
	BGL_EXPORTED_DECL obj_t BGl_bindingszd2ze3listz31zz__evutilsz00(obj_t);
	static obj_t BGl_z62argszd2ze3listz53zz__evutilsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_parsezd2formalzd2identz00zz__evutilsz00(obj_t,
		obj_t);
	extern obj_t BGl_errorzf2sourcezf2zz__errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__evutilsz00(void);
	static obj_t BGl_z62parsezd2formalzd2identz62zz__evutilsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol1650z00zz__evutilsz00 = BUNSPEC;
	static obj_t BGl_symbol1653z00zz__evutilsz00 = BUNSPEC;
	static obj_t BGl_symbol1656z00zz__evutilsz00 = BUNSPEC;
	extern bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_parsezd2formalzd2identzd2envzd2zz__evutilsz00,
		BgL_bgl_za762parseza7d2forma1660z00,
		BGl_z62parsezd2formalzd2identz62zz__evutilsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_argszd2ze3listzd2envze3zz__evutilsz00,
		BgL_bgl_za762argsza7d2za7e3lis1661za7,
		BGl_z62argszd2ze3listz53zz__evutilsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1647z00zz__evutilsz00,
		BgL_bgl_string1647za700za7za7_1662za7, "parse-formal-ident", 18);
	      DEFINE_STRING(BGl_string1648z00zz__evutilsz00,
		BgL_bgl_string1648za700za7za7_1663za7, "Illegal empty identifier type", 29);
	      DEFINE_STRING(BGl_string1649z00zz__evutilsz00,
		BgL_bgl_string1649za700za7za7_1664za7, "", 0);
	      DEFINE_STRING(BGl_string1651z00zz__evutilsz00,
		BgL_bgl_string1651za700za7za7_1665za7, "dsssl", 5);
	      DEFINE_STRING(BGl_string1652z00zz__evutilsz00,
		BgL_bgl_string1652za700za7za7_1666za7, "Illegal identifier type", 23);
	      DEFINE_STRING(BGl_string1654z00zz__evutilsz00,
		BgL_bgl_string1654za700za7za7_1667za7, "args->list", 10);
	      DEFINE_STRING(BGl_string1655z00zz__evutilsz00,
		BgL_bgl_string1655za700za7za7_1668za7, "Illegal args list", 17);
	      DEFINE_STRING(BGl_string1657z00zz__evutilsz00,
		BgL_bgl_string1657za700za7za7_1669za7, "bindings->list", 14);
	      DEFINE_STRING(BGl_string1658z00zz__evutilsz00,
		BgL_bgl_string1658za700za7za7_1670za7, "Illegal bindings list", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bindingszd2ze3listzd2envze3zz__evutilsz00,
		BgL_bgl_za762bindingsza7d2za7e1671za7,
		BGl_z62bindingszd2ze3listz53zz__evutilsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1659z00zz__evutilsz00,
		BgL_bgl_string1659za700za7za7_1672za7, "__evutils", 9);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evutilsz00));
		     ADD_ROOT((void *) (&BGl_symbol1650z00zz__evutilsz00));
		     ADD_ROOT((void *) (&BGl_symbol1653z00zz__evutilsz00));
		     ADD_ROOT((void *) (&BGl_symbol1656z00zz__evutilsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evutilsz00(long
		BgL_checksumz00_1834, char *BgL_fromz00_1835)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evutilsz00))
				{
					BGl_requirezd2initializa7ationz75zz__evutilsz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evutilsz00();
					BGl_cnstzd2initzd2zz__evutilsz00();
					BGl_importedzd2moduleszd2initz00zz__evutilsz00();
					return BGl_methodzd2initzd2zz__evutilsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evutilsz00(void)
	{
		{	/* Eval/evutils.scm 15 */
			BGl_symbol1650z00zz__evutilsz00 =
				bstring_to_symbol(BGl_string1651z00zz__evutilsz00);
			BGl_symbol1653z00zz__evutilsz00 =
				bstring_to_symbol(BGl_string1654z00zz__evutilsz00);
			return (BGl_symbol1656z00zz__evutilsz00 =
				bstring_to_symbol(BGl_string1657z00zz__evutilsz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evutilsz00(void)
	{
		{	/* Eval/evutils.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* parse-formal-ident */
	BGL_EXPORTED_DEF obj_t BGl_parsezd2formalzd2identz00zz__evutilsz00(obj_t
		BgL_identz00_3, obj_t BgL_locz00_4)
	{
		{	/* Eval/evutils.scm 57 */
			{
				obj_t BgL_identz00_1138;

				if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_identz00_3))
					{	/* Eval/evutils.scm 84 */
						return
							MAKE_YOUNG_PAIR(BGl_gensymz00zz__r4_symbols_6_4z00
							(BGl_symbol1650z00zz__evutilsz00), BNIL);
					}
				else
					{	/* Eval/evutils.scm 86 */
						bool_t BgL_test1675z00_1851;

						if (PAIRP(BgL_identz00_3))
							{	/* Eval/evutils.scm 86 */
								obj_t BgL_tmpz00_1854;

								BgL_tmpz00_1854 = CAR(BgL_identz00_3);
								BgL_test1675z00_1851 = SYMBOLP(BgL_tmpz00_1854);
							}
						else
							{	/* Eval/evutils.scm 86 */
								BgL_test1675z00_1851 = ((bool_t) 0);
							}
						if (BgL_test1675z00_1851)
							{	/* Eval/evutils.scm 86 */
								return MAKE_YOUNG_PAIR(BgL_identz00_3, BNIL);
							}
						else
							{	/* Eval/evutils.scm 86 */
								if (SYMBOLP(BgL_identz00_3))
									{	/* Eval/evutils.scm 88 */
										BgL_identz00_1138 = BgL_identz00_3;
										{	/* Eval/evutils.scm 60 */
											obj_t BgL_strz00_1140;

											BgL_strz00_1140 = SYMBOL_TO_STRING(BgL_identz00_1138);
											{	/* Eval/evutils.scm 60 */
												long BgL_lenz00_1141;

												BgL_lenz00_1141 = STRING_LENGTH(BgL_strz00_1140);
												{	/* Eval/evutils.scm 61 */

													{
														long BgL_iz00_1143;

														BgL_iz00_1143 = 0L;
													BgL_zc3z04anonymousza31184ze3z87_1144:
														if ((BgL_iz00_1143 == BgL_lenz00_1141))
															{	/* Eval/evutils.scm 64 */
																return MAKE_YOUNG_PAIR(BgL_identz00_1138, BNIL);
															}
														else
															{	/* Eval/evutils.scm 66 */
																bool_t BgL_test1679z00_1865;

																if (
																	(STRING_REF(BgL_strz00_1140,
																			BgL_iz00_1143) == ((unsigned char) ':')))
																	{	/* Eval/evutils.scm 66 */
																		if (
																			(BgL_iz00_1143 < (BgL_lenz00_1141 - 1L)))
																			{	/* Eval/evutils.scm 67 */
																				BgL_test1679z00_1865 =
																					(STRING_REF(BgL_strz00_1140,
																						(BgL_iz00_1143 + 1L)) ==
																					((unsigned char) ':'));
																			}
																		else
																			{	/* Eval/evutils.scm 67 */
																				BgL_test1679z00_1865 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Eval/evutils.scm 66 */
																		BgL_test1679z00_1865 = ((bool_t) 0);
																	}
																if (BgL_test1679z00_1865)
																	{	/* Eval/evutils.scm 66 */
																		if (
																			(BgL_iz00_1143 == (BgL_lenz00_1141 - 2L)))
																			{	/* Eval/evutils.scm 70 */
																				return
																					BGl_errorzf2sourcezd2locationz20zz__errorz00
																					(BGl_string1647z00zz__evutilsz00,
																					BGl_string1648z00zz__evutilsz00,
																					BgL_identz00_1138, BgL_locz00_4);
																			}
																		else
																			{	/* Eval/evutils.scm 70 */
																				if ((BgL_iz00_1143 == 0L))
																					{	/* Eval/evutils.scm 75 */
																						return
																							MAKE_YOUNG_PAIR(bstring_to_symbol
																							(BGl_string1649z00zz__evutilsz00),
																							BgL_identz00_1138);
																					}
																				else
																					{	/* Eval/evutils.scm 75 */
																						return
																							MAKE_YOUNG_PAIR(bstring_to_symbol
																							(c_substring(BgL_strz00_1140, 0L,
																									BgL_iz00_1143)),
																							bstring_to_symbol(c_substring
																								(BgL_strz00_1140,
																									(BgL_iz00_1143 + 2L),
																									BgL_lenz00_1141)));
																					}
																			}
																	}
																else
																	{
																		long BgL_iz00_1889;

																		BgL_iz00_1889 = (BgL_iz00_1143 + 1L);
																		BgL_iz00_1143 = BgL_iz00_1889;
																		goto BgL_zc3z04anonymousza31184ze3z87_1144;
																	}
															}
													}
												}
											}
										}
									}
								else
									{	/* Eval/evutils.scm 88 */
										return
											BGl_errorzf2sourcezd2locationz20zz__errorz00
											(BGl_string1647z00zz__evutilsz00,
											BGl_string1652z00zz__evutilsz00, BgL_identz00_3,
											BgL_locz00_4);
									}
							}
					}
			}
		}

	}



/* &parse-formal-ident */
	obj_t BGl_z62parsezd2formalzd2identz62zz__evutilsz00(obj_t BgL_envz00_1824,
		obj_t BgL_identz00_1825, obj_t BgL_locz00_1826)
	{
		{	/* Eval/evutils.scm 57 */
			return
				BGl_parsezd2formalzd2identz00zz__evutilsz00(BgL_identz00_1825,
				BgL_locz00_1826);
		}

	}



/* args->list */
	BGL_EXPORTED_DEF obj_t BGl_argszd2ze3listz31zz__evutilsz00(obj_t
		BgL_argsz00_5)
	{
		{	/* Eval/evutils.scm 99 */
			if (NULLP(BgL_argsz00_5))
				{	/* Eval/evutils.scm 101 */
					return BNIL;
				}
			else
				{	/* Eval/evutils.scm 101 */
					if (SYMBOLP(BgL_argsz00_5))
						{	/* Eval/evutils.scm 104 */
							obj_t BgL_list1222z00_1176;

							BgL_list1222z00_1176 = MAKE_YOUNG_PAIR(BgL_argsz00_5, BNIL);
							return BgL_list1222z00_1176;
						}
					else
						{	/* Eval/evutils.scm 103 */
							if (PAIRP(BgL_argsz00_5))
								{	/* Eval/evutils.scm 105 */
									return
										MAKE_YOUNG_PAIR(CAR(BgL_argsz00_5),
										BGl_argszd2ze3listz31zz__evutilsz00(CDR(BgL_argsz00_5)));
								}
							else
								{	/* Eval/evutils.scm 105 */
									return
										BGl_errorzf2sourcezf2zz__errorz00
										(BGl_symbol1653z00zz__evutilsz00,
										BGl_string1655z00zz__evutilsz00, BgL_argsz00_5,
										BgL_argsz00_5);
								}
						}
				}
		}

	}



/* &args->list */
	obj_t BGl_z62argszd2ze3listz53zz__evutilsz00(obj_t BgL_envz00_1827,
		obj_t BgL_argsz00_1828)
	{
		{	/* Eval/evutils.scm 99 */
			return BGl_argszd2ze3listz31zz__evutilsz00(BgL_argsz00_1828);
		}

	}



/* bindings->list */
	BGL_EXPORTED_DEF obj_t BGl_bindingszd2ze3listz31zz__evutilsz00(obj_t
		BgL_bindingsz00_6)
	{
		{	/* Eval/evutils.scm 113 */
			if (NULLP(BgL_bindingsz00_6))
				{	/* Eval/evutils.scm 115 */
					return BNIL;
				}
			else
				{	/* Eval/evutils.scm 115 */
					if (PAIRP(BgL_bindingsz00_6))
						{	/* Eval/evutils.scm 119 */
							bool_t BgL_test1689z00_1910;

							{	/* Eval/evutils.scm 119 */
								obj_t BgL_tmpz00_1911;

								BgL_tmpz00_1911 = CAR(BgL_bindingsz00_6);
								BgL_test1689z00_1910 = SYMBOLP(BgL_tmpz00_1911);
							}
							if (BgL_test1689z00_1910)
								{	/* Eval/evutils.scm 119 */
									return
										MAKE_YOUNG_PAIR(BgL_bindingsz00_6,
										BGl_bindingszd2ze3listz31zz__evutilsz00(CDR
											(BgL_bindingsz00_6)));
								}
							else
								{	/* Eval/evutils.scm 121 */
									bool_t BgL_test1690z00_1917;

									{	/* Eval/evutils.scm 121 */
										obj_t BgL_tmpz00_1918;

										BgL_tmpz00_1918 = CAR(BgL_bindingsz00_6);
										BgL_test1690z00_1917 = PAIRP(BgL_tmpz00_1918);
									}
									if (BgL_test1690z00_1917)
										{	/* Eval/evutils.scm 121 */
											return
												MAKE_YOUNG_PAIR(CAR(BgL_bindingsz00_6),
												BGl_bindingszd2ze3listz31zz__evutilsz00(CDR
													(BgL_bindingsz00_6)));
										}
									else
										{	/* Eval/evutils.scm 121 */
											return
												BGl_errorzf2sourcezf2zz__errorz00
												(BGl_symbol1656z00zz__evutilsz00,
												BGl_string1658z00zz__evutilsz00, BgL_bindingsz00_6,
												BgL_bindingsz00_6);
										}
								}
						}
					else
						{	/* Eval/evutils.scm 117 */
							return
								BGl_errorzf2sourcezf2zz__errorz00
								(BGl_symbol1656z00zz__evutilsz00,
								BGl_string1658z00zz__evutilsz00, BgL_bindingsz00_6,
								BgL_bindingsz00_6);
						}
				}
		}

	}



/* &bindings->list */
	obj_t BGl_z62bindingszd2ze3listz53zz__evutilsz00(obj_t BgL_envz00_1829,
		obj_t BgL_bindingsz00_1830)
	{
		{	/* Eval/evutils.scm 113 */
			return BGl_bindingszd2ze3listz31zz__evutilsz00(BgL_bindingsz00_1830);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evutilsz00(void)
	{
		{	/* Eval/evutils.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evutilsz00(void)
	{
		{	/* Eval/evutils.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evutilsz00(void)
	{
		{	/* Eval/evutils.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evutilsz00(void)
	{
		{	/* Eval/evutils.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			BGl_modulezd2initializa7ationz75zz__macroz00(261397491L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
			return BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1659z00zz__evutilsz00));
		}

	}

#ifdef __cplusplus
}
#endif
