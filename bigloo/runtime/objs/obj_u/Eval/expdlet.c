/*===========================================================================*/
/*   (Eval/expdlet.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdlet.scm -indent -o objs/obj_u/Eval/expdlet.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_LET_TYPE_DEFINITIONS
#define BGL___EXPANDER_LET_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_LET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2005z00zz__expander_letz00 = BUNSPEC;
	static obj_t BGl_symbol2009z00zz__expander_letz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2letrecza2za2zz__expander_letz00(obj_t, obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_letz00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62expandzd2evalzd2labelsz62zz__expander_letz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2evalzd2letz00zz__expander_letz00(obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2evalzd2letz62zz__expander_letz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_definez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_z52withzd2lexicalz80zz__expandz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2evalzd2letza2zc0zz__expander_letz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_letz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__expander_letz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_letz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2z42letz42zz__expander_letz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_letz00(void);
	extern obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__expander_letz00(void);
	extern obj_t BGl_bindingszd2ze3listz31zz__evutilsz00(obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_symbol1996z00zz__expander_letz00 = BUNSPEC;
	static obj_t BGl_symbol1998z00zz__expander_letz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2evalzd2letrecza2zc0zz__expander_letz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2letrecz00zz__expander_letz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_letz00(void);
	static obj_t BGl_z62expandzd2evalzd2letrecz62zz__expander_letz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_loopze70ze7zz__expander_letz00(obj_t, obj_t);
	static obj_t BGl_z62expandzd2evalzd2z42letz20zz__expander_letz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evalzd2beginzd2expanderz00zz__expander_definez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31334ze3ze5zz__expander_letz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2letza2za2zz__expander_letz00(obj_t, obj_t);
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2labelsz00zz__expander_letz00(obj_t, obj_t);
	static obj_t BGl_symbol2000z00zz__expander_letz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2z42letzd2envz90zz__expander_letz00,
		BgL_bgl_za762expandza7d2eval2013z00,
		BGl_z62expandzd2evalzd2z42letz20zz__expander_letz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2letza2zd2envz70zz__expander_letz00,
		BgL_bgl_za762expandza7d2eval2014z00,
		BGl_z62expandzd2evalzd2letza2zc0zz__expander_letz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2labelszd2envzd2zz__expander_letz00,
		BgL_bgl_za762expandza7d2eval2015z00,
		BGl_z62expandzd2evalzd2labelsz62zz__expander_letz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1997z00zz__expander_letz00,
		BgL_bgl_string1997za700za7za7_2016za7, "lambda", 6);
	      DEFINE_STRING(BGl_string1999z00zz__expander_letz00,
		BgL_bgl_string1999za700za7za7_2017za7, "letrec", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zz__expander_letz00,
		BgL_bgl_za762za7c3za704anonymo2018za7,
		BGl_z62zc3z04anonymousza31334ze3ze5zz__expander_letz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2letrecza2zd2envz70zz__expander_letz00,
		BgL_bgl_za762expandza7d2eval2019z00,
		BGl_z62expandzd2evalzd2letrecza2zc0zz__expander_letz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2letreczd2envzd2zz__expander_letz00,
		BgL_bgl_za762expandza7d2eval2020z00,
		BGl_z62expandzd2evalzd2letrecz62zz__expander_letz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2001z00zz__expander_letz00,
		BgL_bgl_string2001za700za7za7_2021za7, "let", 3);
	      DEFINE_STRING(BGl_string2002z00zz__expander_letz00,
		BgL_bgl_string2002za700za7za7_2022za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2003z00zz__expander_letz00,
		BgL_bgl_string2003za700za7za7_2023za7, "Illegal binding form", 20);
	      DEFINE_STRING(BGl_string2004z00zz__expander_letz00,
		BgL_bgl_string2004za700za7za7_2024za7, "Illegal `let' form", 18);
	      DEFINE_STRING(BGl_string2006z00zz__expander_letz00,
		BgL_bgl_string2006za700za7za7_2025za7, "let*", 4);
	      DEFINE_STRING(BGl_string2007z00zz__expander_letz00,
		BgL_bgl_string2007za700za7za7_2026za7, "Illegal bindings form", 21);
	      DEFINE_STRING(BGl_string2008z00zz__expander_letz00,
		BgL_bgl_string2008za700za7za7_2027za7, "letrec*", 7);
	      DEFINE_STRING(BGl_string2010z00zz__expander_letz00,
		BgL_bgl_string2010za700za7za7_2028za7, "set!", 4);
	      DEFINE_STRING(BGl_string2011z00zz__expander_letz00,
		BgL_bgl_string2011za700za7za7_2029za7, "expand-labels", 13);
	      DEFINE_STRING(BGl_string2012z00zz__expander_letz00,
		BgL_bgl_string2012za700za7za7_2030za7, "__expander_let", 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2letzd2envzd2zz__expander_letz00,
		BgL_bgl_za762expandza7d2eval2031z00,
		BGl_z62expandzd2evalzd2letz62zz__expander_letz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2005z00zz__expander_letz00));
		     ADD_ROOT((void *) (&BGl_symbol2009z00zz__expander_letz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_letz00));
		     ADD_ROOT((void *) (&BGl_symbol1996z00zz__expander_letz00));
		     ADD_ROOT((void *) (&BGl_symbol1998z00zz__expander_letz00));
		     ADD_ROOT((void *) (&BGl_symbol2000z00zz__expander_letz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_letz00(long
		BgL_checksumz00_2739, char *BgL_fromz00_2740)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_letz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_letz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_letz00();
					BGl_cnstzd2initzd2zz__expander_letz00();
					BGl_importedzd2moduleszd2initz00zz__expander_letz00();
					return BGl_methodzd2initzd2zz__expander_letz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_letz00(void)
	{
		{	/* Eval/expdlet.scm 15 */
			BGl_symbol1996z00zz__expander_letz00 =
				bstring_to_symbol(BGl_string1997z00zz__expander_letz00);
			BGl_symbol1998z00zz__expander_letz00 =
				bstring_to_symbol(BGl_string1999z00zz__expander_letz00);
			BGl_symbol2000z00zz__expander_letz00 =
				bstring_to_symbol(BGl_string2001z00zz__expander_letz00);
			BGl_symbol2005z00zz__expander_letz00 =
				bstring_to_symbol(BGl_string2006z00zz__expander_letz00);
			return (BGl_symbol2009z00zz__expander_letz00 =
				bstring_to_symbol(BGl_string2010z00zz__expander_letz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_letz00(void)
	{
		{	/* Eval/expdlet.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* expand-eval-let */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2evalzd2letz00zz__expander_letz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expdlet.scm 64 */
			{
				obj_t BgL_bindingsz00_1325;
				obj_t BgL_bodyz00_1326;
				obj_t BgL_xz00_1327;
				obj_t BgL_ez00_1328;
				obj_t BgL_loopz00_1187;
				obj_t BgL_bindingsz00_1188;
				obj_t BgL_bodyz00_1189;
				obj_t BgL_xz00_1190;
				obj_t BgL_ez00_1191;

				{	/* Eval/expdlet.scm 110 */
					obj_t BgL_ez00_1131;

					BgL_ez00_1131 =
						BGl_evalzd2beginzd2expanderz00zz__expander_definez00(BgL_ez00_4);
					{	/* Eval/expdlet.scm 110 */
						obj_t BgL_resz00_1132;

						if (PAIRP(BgL_xz00_3))
							{	/* Eval/expdlet.scm 111 */
								obj_t BgL_cdrzd2140zd2_1145;

								BgL_cdrzd2140zd2_1145 = CDR(((obj_t) BgL_xz00_3));
								if (PAIRP(BgL_cdrzd2140zd2_1145))
									{	/* Eval/expdlet.scm 111 */
										obj_t BgL_cdrzd2143zd2_1147;

										BgL_cdrzd2143zd2_1147 = CDR(BgL_cdrzd2140zd2_1145);
										if (NULLP(CAR(BgL_cdrzd2140zd2_1145)))
											{	/* Eval/expdlet.scm 111 */
												if (NULLP(BgL_cdrzd2143zd2_1147))
													{	/* Eval/expdlet.scm 111 */
														obj_t BgL_carzd2157zd2_1152;
														obj_t BgL_cdrzd2158zd2_1153;

														BgL_carzd2157zd2_1152 =
															CAR(((obj_t) BgL_cdrzd2140zd2_1145));
														BgL_cdrzd2158zd2_1153 =
															CDR(((obj_t) BgL_cdrzd2140zd2_1145));
														if (SYMBOLP(BgL_carzd2157zd2_1152))
															{	/* Eval/expdlet.scm 111 */
																if (PAIRP(BgL_cdrzd2158zd2_1153))
																	{	/* Eval/expdlet.scm 111 */
																		obj_t BgL_cdrzd2165zd2_1156;

																		BgL_cdrzd2165zd2_1156 =
																			CDR(BgL_cdrzd2158zd2_1153);
																		if (NULLP(BgL_cdrzd2165zd2_1156))
																			{	/* Eval/expdlet.scm 111 */
																				obj_t BgL_carzd2177zd2_1159;

																				BgL_carzd2177zd2_1159 =
																					CAR(((obj_t) BgL_cdrzd2140zd2_1145));
																				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2177zd2_1159))
																					{	/* Eval/expdlet.scm 111 */
																						obj_t BgL_arg1219z00_1161;

																						BgL_arg1219z00_1161 =
																							CDR(
																							((obj_t) BgL_cdrzd2140zd2_1145));
																						BgL_bindingsz00_1325 =
																							BgL_carzd2177zd2_1159;
																						BgL_bodyz00_1326 =
																							BgL_arg1219z00_1161;
																						BgL_xz00_1327 = BgL_xz00_3;
																						BgL_ez00_1328 = BgL_ez00_1131;
																					BgL_zc3z04anonymousza31361ze3z87_1329:
																						{
																							obj_t BgL_bindingsz00_1333;
																							obj_t BgL_nbindingsz00_1334;
																							obj_t BgL_ebdgsz00_1335;

																							BgL_bindingsz00_1333 =
																								BgL_bindingsz00_1325;
																							BgL_nbindingsz00_1334 = BNIL;
																							BgL_ebdgsz00_1335 = BNIL;
																						BgL_zc3z04anonymousza31362ze3z87_1336:
																							if (NULLP
																								(BgL_bindingsz00_1333))
																								{	/* Eval/expdlet.scm 96 */
																									obj_t BgL_arg1364z00_1338;

																									{	/* Eval/expdlet.scm 96 */
																										obj_t BgL_arg1365z00_1339;
																										obj_t BgL_arg1366z00_1340;

																										BgL_arg1365z00_1339 =
																											bgl_reverse_bang
																											(BgL_nbindingsz00_1334);
																										BgL_arg1366z00_1340 =
																											MAKE_YOUNG_PAIR
																											(BGl_z52withzd2lexicalz80zz__expandz00
																											(BgL_ebdgsz00_1335,
																												BGl_expandzd2prognzd2zz__prognz00
																												(BgL_bodyz00_1326),
																												BgL_ez00_1328, BFALSE),
																											BNIL);
																										BgL_arg1364z00_1338 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1365z00_1339,
																											BgL_arg1366z00_1340);
																									}
																									BgL_resz00_1132 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2000z00zz__expander_letz00,
																										BgL_arg1364z00_1338);
																								}
																							else
																								{
																									obj_t BgL_varz00_1343;

																									{	/* Eval/expdlet.scm 98 */
																										obj_t BgL_ezd2111zd2_1349;

																										BgL_ezd2111zd2_1349 =
																											CAR(
																											((obj_t)
																												BgL_bindingsz00_1333));
																										if (SYMBOLP
																											(BgL_ezd2111zd2_1349))
																											{	/* Eval/expdlet.scm 98 */
																												BgL_varz00_1343 =
																													BgL_ezd2111zd2_1349;
																												{	/* Eval/expdlet.scm 100 */
																													obj_t
																														BgL_arg1377z00_1360;
																													obj_t
																														BgL_arg1378z00_1361;
																													obj_t
																														BgL_arg1379z00_1362;
																													BgL_arg1377z00_1360 =
																														CDR(((obj_t)
																															BgL_bindingsz00_1333));
																													{	/* Eval/expdlet.scm 101 */
																														obj_t
																															BgL_arg1380z00_1363;
																														{	/* Eval/expdlet.scm 89 */
																															obj_t
																																BgL_arg1358z00_2257;
																															{	/* Eval/expdlet.scm 89 */
																																obj_t
																																	BgL_list1359z00_2258;
																																{	/* Eval/expdlet.scm 89 */
																																	obj_t
																																		BgL_arg1360z00_2259;
																																	BgL_arg1360z00_2259
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BUNSPEC,
																																		BNIL);
																																	BgL_list1359z00_2258
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_varz00_1343,
																																		BgL_arg1360z00_2259);
																																}
																																BgL_arg1358z00_2257
																																	=
																																	BgL_list1359z00_2258;
																															}
																															BgL_arg1380z00_1363
																																=
																																BGl_evepairifyz00zz__prognz00
																																(BgL_arg1358z00_2257,
																																BgL_bindingsz00_1333);
																														}
																														BgL_arg1378z00_1361
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1380z00_1363,
																															BgL_nbindingsz00_1334);
																													}
																													BgL_arg1379z00_1362 =
																														MAKE_YOUNG_PAIR
																														(BgL_varz00_1343,
																														BgL_ebdgsz00_1335);
																													{
																														obj_t
																															BgL_ebdgsz00_2805;
																														obj_t
																															BgL_nbindingsz00_2804;
																														obj_t
																															BgL_bindingsz00_2803;
																														BgL_bindingsz00_2803
																															=
																															BgL_arg1377z00_1360;
																														BgL_nbindingsz00_2804
																															=
																															BgL_arg1378z00_1361;
																														BgL_ebdgsz00_2805 =
																															BgL_arg1379z00_1362;
																														BgL_ebdgsz00_1335 =
																															BgL_ebdgsz00_2805;
																														BgL_nbindingsz00_1334
																															=
																															BgL_nbindingsz00_2804;
																														BgL_bindingsz00_1333
																															=
																															BgL_bindingsz00_2803;
																														goto
																															BgL_zc3z04anonymousza31362ze3z87_1336;
																													}
																												}
																											}
																										else
																											{	/* Eval/expdlet.scm 98 */
																												if (PAIRP
																													(BgL_ezd2111zd2_1349))
																													{	/* Eval/expdlet.scm 98 */
																														obj_t
																															BgL_carzd2121zd2_1352;
																														obj_t
																															BgL_cdrzd2122zd2_1353;
																														BgL_carzd2121zd2_1352
																															=
																															CAR
																															(BgL_ezd2111zd2_1349);
																														BgL_cdrzd2122zd2_1353
																															=
																															CDR
																															(BgL_ezd2111zd2_1349);
																														if (SYMBOLP
																															(BgL_carzd2121zd2_1352))
																															{	/* Eval/expdlet.scm 98 */
																																if (PAIRP
																																	(BgL_cdrzd2122zd2_1353))
																																	{	/* Eval/expdlet.scm 98 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd2122zd2_1353)))
																																			{	/* Eval/expdlet.scm 98 */
																																				obj_t
																																					BgL_arg1375z00_1358;
																																				BgL_arg1375z00_1358
																																					=
																																					CAR
																																					(BgL_cdrzd2122zd2_1353);
																																				{	/* Eval/expdlet.scm 104 */
																																					obj_t
																																						BgL_arg1382z00_2278;
																																					obj_t
																																						BgL_arg1383z00_2279;
																																					obj_t
																																						BgL_arg1384z00_2280;
																																					BgL_arg1382z00_2278
																																						=
																																						CDR(
																																						((obj_t) BgL_bindingsz00_1333));
																																					{	/* Eval/expdlet.scm 105 */
																																						obj_t
																																							BgL_arg1387z00_2281;
																																						{	/* Eval/expdlet.scm 105 */
																																							obj_t
																																								BgL_arg1388z00_2282;
																																							BgL_arg1388z00_2282
																																								=
																																								BGL_PROCEDURE_CALL2
																																								(BgL_ez00_1328,
																																								BgL_arg1375z00_1358,
																																								BgL_ez00_1328);
																																							{	/* Eval/expdlet.scm 89 */
																																								obj_t
																																									BgL_arg1358z00_2286;
																																								{	/* Eval/expdlet.scm 89 */
																																									obj_t
																																										BgL_list1359z00_2287;
																																									{	/* Eval/expdlet.scm 89 */
																																										obj_t
																																											BgL_arg1360z00_2288;
																																										BgL_arg1360z00_2288
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1388z00_2282,
																																											BNIL);
																																										BgL_list1359z00_2287
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_carzd2121zd2_1352,
																																											BgL_arg1360z00_2288);
																																									}
																																									BgL_arg1358z00_2286
																																										=
																																										BgL_list1359z00_2287;
																																								}
																																								BgL_arg1387z00_2281
																																									=
																																									BGl_evepairifyz00zz__prognz00
																																									(BgL_arg1358z00_2286,
																																									BgL_ezd2111zd2_1349);
																																							}
																																						}
																																						BgL_arg1383z00_2279
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1387z00_2281,
																																							BgL_nbindingsz00_1334);
																																					}
																																					BgL_arg1384z00_2280
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_carzd2121zd2_1352,
																																						BgL_ebdgsz00_1335);
																																					{
																																						obj_t
																																							BgL_ebdgsz00_2832;
																																						obj_t
																																							BgL_nbindingsz00_2831;
																																						obj_t
																																							BgL_bindingsz00_2830;
																																						BgL_bindingsz00_2830
																																							=
																																							BgL_arg1382z00_2278;
																																						BgL_nbindingsz00_2831
																																							=
																																							BgL_arg1383z00_2279;
																																						BgL_ebdgsz00_2832
																																							=
																																							BgL_arg1384z00_2280;
																																						BgL_ebdgsz00_1335
																																							=
																																							BgL_ebdgsz00_2832;
																																						BgL_nbindingsz00_1334
																																							=
																																							BgL_nbindingsz00_2831;
																																						BgL_bindingsz00_1333
																																							=
																																							BgL_bindingsz00_2830;
																																						goto
																																							BgL_zc3z04anonymousza31362ze3z87_1336;
																																					}
																																				}
																																			}
																																		else
																																			{	/* Eval/expdlet.scm 98 */
																																				BgL_resz00_1132
																																					=
																																					BGl_expandzd2errorzd2zz__expandz00
																																					(BGl_string2001z00zz__expander_letz00,
																																					BGl_string2003z00zz__expander_letz00,
																																					BgL_xz00_1327);
																																			}
																																	}
																																else
																																	{	/* Eval/expdlet.scm 98 */
																																		BgL_resz00_1132
																																			=
																																			BGl_expandzd2errorzd2zz__expandz00
																																			(BGl_string2001z00zz__expander_letz00,
																																			BGl_string2003z00zz__expander_letz00,
																																			BgL_xz00_1327);
																																	}
																															}
																														else
																															{	/* Eval/expdlet.scm 98 */
																																BgL_resz00_1132
																																	=
																																	BGl_expandzd2errorzd2zz__expandz00
																																	(BGl_string2001z00zz__expander_letz00,
																																	BGl_string2003z00zz__expander_letz00,
																																	BgL_xz00_1327);
																															}
																													}
																												else
																													{	/* Eval/expdlet.scm 98 */
																														BgL_resz00_1132 =
																															BGl_expandzd2errorzd2zz__expandz00
																															(BGl_string2001z00zz__expander_letz00,
																															BGl_string2003z00zz__expander_letz00,
																															BgL_xz00_1327);
																													}
																											}
																									}
																								}
																						}
																					}
																				else
																					{	/* Eval/expdlet.scm 111 */
																						BgL_resz00_1132 =
																							BGl_expandzd2errorzd2zz__expandz00
																							(BGl_string2001z00zz__expander_letz00,
																							BGl_string2004z00zz__expander_letz00,
																							BgL_xz00_3);
																					}
																			}
																		else
																			{	/* Eval/expdlet.scm 111 */
																				BgL_loopz00_1187 =
																					BgL_carzd2157zd2_1152;
																				BgL_bindingsz00_1188 =
																					CAR(BgL_cdrzd2158zd2_1153);
																				BgL_bodyz00_1189 =
																					BgL_cdrzd2165zd2_1156;
																				BgL_xz00_1190 = BgL_xz00_3;
																				BgL_ez00_1191 = BgL_ez00_1131;
																			BgL_zc3z04anonymousza31233ze3z87_1192:
																				{	/* Eval/expdlet.scm 67 */
																					bool_t BgL_test2047z00_2838;

																					{
																						obj_t BgL_l1087z00_1302;

																						BgL_l1087z00_1302 =
																							BgL_bindingsz00_1188;
																					BgL_zc3z04anonymousza31353ze3z87_1303:
																						if (NULLP
																							(BgL_l1087z00_1302))
																							{	/* Eval/expdlet.scm 67 */
																								BgL_test2047z00_2838 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Eval/expdlet.scm 67 */
																								bool_t BgL_test2049z00_2841;

																								{	/* Eval/expdlet.scm 68 */
																									obj_t BgL_xz00_1308;

																									BgL_xz00_1308 =
																										CAR(
																										((obj_t)
																											BgL_l1087z00_1302));
																									if (PAIRP(BgL_xz00_1308))
																										{	/* Eval/expdlet.scm 68 */
																											obj_t
																												BgL_cdrzd2105zd2_1313;
																											BgL_cdrzd2105zd2_1313 =
																												CDR(BgL_xz00_1308);
																											if (PAIRP
																												(BgL_cdrzd2105zd2_1313))
																												{	/* Eval/expdlet.scm 68 */
																													BgL_test2049z00_2841 =
																														NULLP(CDR
																														(BgL_cdrzd2105zd2_1313));
																												}
																											else
																												{	/* Eval/expdlet.scm 68 */
																													BgL_test2049z00_2841 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Eval/expdlet.scm 68 */
																											BgL_test2049z00_2841 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test2049z00_2841)
																									{
																										obj_t BgL_l1087z00_2851;

																										BgL_l1087z00_2851 =
																											CDR(
																											((obj_t)
																												BgL_l1087z00_1302));
																										BgL_l1087z00_1302 =
																											BgL_l1087z00_2851;
																										goto
																											BgL_zc3z04anonymousza31353ze3z87_1303;
																									}
																								else
																									{	/* Eval/expdlet.scm 67 */
																										BgL_test2047z00_2838 =
																											((bool_t) 0);
																									}
																							}
																					}
																					if (BgL_test2047z00_2838)
																						{	/* Eval/expdlet.scm 73 */
																							obj_t BgL_varsz00_1212;

																							if (NULLP(BgL_bindingsz00_1188))
																								{	/* Eval/expdlet.scm 73 */
																									BgL_varsz00_1212 = BNIL;
																								}
																							else
																								{	/* Eval/expdlet.scm 73 */
																									obj_t BgL_head1092z00_1276;

																									BgL_head1092z00_1276 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_l1090z00_1278;
																										obj_t BgL_tail1093z00_1279;

																										BgL_l1090z00_1278 =
																											BgL_bindingsz00_1188;
																										BgL_tail1093z00_1279 =
																											BgL_head1092z00_1276;
																									BgL_zc3z04anonymousza31338ze3z87_1280:
																										if (NULLP
																											(BgL_l1090z00_1278))
																											{	/* Eval/expdlet.scm 73 */
																												BgL_varsz00_1212 =
																													CDR
																													(BgL_head1092z00_1276);
																											}
																										else
																											{	/* Eval/expdlet.scm 73 */
																												obj_t
																													BgL_newtail1094z00_1282;
																												{	/* Eval/expdlet.scm 73 */
																													obj_t
																														BgL_arg1341z00_1284;
																													{	/* Eval/expdlet.scm 73 */
																														obj_t BgL_xz00_1285;

																														BgL_xz00_1285 =
																															CAR(
																															((obj_t)
																																BgL_l1090z00_1278));
																														{	/* Eval/expdlet.scm 74 */
																															bool_t
																																BgL_test2054z00_2862;
																															{	/* Eval/expdlet.scm 74 */
																																bool_t
																																	BgL_test2055z00_2863;
																																{	/* Eval/expdlet.scm 74 */
																																	obj_t
																																		BgL_tmpz00_2864;
																																	{	/* Eval/expdlet.scm 74 */
																																		obj_t
																																			BgL_pairz00_2159;
																																		BgL_pairz00_2159
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_1285));
																																		BgL_tmpz00_2864
																																			=
																																			CDR
																																			(BgL_pairz00_2159);
																																	}
																																	BgL_test2055z00_2863
																																		=
																																		NULLP
																																		(BgL_tmpz00_2864);
																																}
																																if (BgL_test2055z00_2863)
																																	{	/* Eval/expdlet.scm 75 */
																																		bool_t
																																			BgL_test2056z00_2869;
																																		{	/* Eval/expdlet.scm 75 */
																																			obj_t
																																				BgL_tmpz00_2870;
																																			{	/* Eval/expdlet.scm 75 */
																																				obj_t
																																					BgL_pairz00_2163;
																																				BgL_pairz00_2163
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_xz00_1285));
																																				BgL_tmpz00_2870
																																					=
																																					CAR
																																					(BgL_pairz00_2163);
																																			}
																																			BgL_test2056z00_2869
																																				=
																																				PAIRP
																																				(BgL_tmpz00_2870);
																																		}
																																		if (BgL_test2056z00_2869)
																																			{	/* Eval/expdlet.scm 75 */
																																				BgL_test2054z00_2862
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																		else
																																			{	/* Eval/expdlet.scm 75 */
																																				BgL_test2054z00_2862
																																					=
																																					(
																																					(bool_t)
																																					1);
																																			}
																																	}
																																else
																																	{	/* Eval/expdlet.scm 74 */
																																		BgL_test2054z00_2862
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test2054z00_2862)
																																{	/* Eval/expdlet.scm 76 */
																																	obj_t
																																		BgL_arg1349z00_1293;
																																	{	/* Eval/expdlet.scm 76 */
																																		obj_t
																																			BgL_pairz00_2167;
																																		BgL_pairz00_2167
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_xz00_1285));
																																		BgL_arg1349z00_1293
																																			=
																																			CAR
																																			(BgL_pairz00_2167);
																																	}
																																	BgL_arg1341z00_1284
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BFALSE,
																																		BgL_arg1349z00_1293);
																																}
																															else
																																{	/* Eval/expdlet.scm 77 */
																																	obj_t
																																		BgL_arg1350z00_1294;
																																	{	/* Eval/expdlet.scm 77 */

																																		{	/* Eval/expdlet.scm 77 */

																																			BgL_arg1350z00_1294
																																				=
																																				BGl_gensymz00zz__r4_symbols_6_4z00
																																				(BFALSE);
																																		}
																																	}
																																	BgL_arg1341z00_1284
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BTRUE,
																																		BgL_arg1350z00_1294);
																																}
																														}
																													}
																													BgL_newtail1094z00_1282
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1341z00_1284,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1093z00_1279,
																													BgL_newtail1094z00_1282);
																												{	/* Eval/expdlet.scm 73 */
																													obj_t
																														BgL_arg1340z00_1283;
																													BgL_arg1340z00_1283 =
																														CDR(((obj_t)
																															BgL_l1090z00_1278));
																													{
																														obj_t
																															BgL_tail1093z00_2886;
																														obj_t
																															BgL_l1090z00_2885;
																														BgL_l1090z00_2885 =
																															BgL_arg1340z00_1283;
																														BgL_tail1093z00_2886
																															=
																															BgL_newtail1094z00_1282;
																														BgL_tail1093z00_1279
																															=
																															BgL_tail1093z00_2886;
																														BgL_l1090z00_1278 =
																															BgL_l1090z00_2885;
																														goto
																															BgL_zc3z04anonymousza31338ze3z87_1280;
																													}
																												}
																											}
																									}
																								}
																							{	/* Eval/expdlet.scm 73 */
																								obj_t BgL_auxz00_1213;

																								{	/* Eval/expdlet.scm 79 */
																									obj_t BgL_list1332z00_1265;

																									{	/* Eval/expdlet.scm 79 */
																										obj_t BgL_arg1333z00_1266;

																										BgL_arg1333z00_1266 =
																											MAKE_YOUNG_PAIR
																											(BgL_bindingsz00_1188,
																											BNIL);
																										BgL_list1332z00_1265 =
																											MAKE_YOUNG_PAIR
																											(BgL_varsz00_1212,
																											BgL_arg1333z00_1266);
																									}
																									BgL_auxz00_1213 =
																										BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																										(BGl_proc1995z00zz__expander_letz00,
																										BgL_list1332z00_1265);
																								}
																								{	/* Eval/expdlet.scm 79 */
																									obj_t BgL_recz00_1214;

																									{	/* Eval/expdlet.scm 82 */
																										obj_t BgL_arg1252z00_1219;

																										{	/* Eval/expdlet.scm 82 */
																											obj_t BgL_arg1268z00_1220;
																											obj_t BgL_arg1272z00_1221;

																											{	/* Eval/expdlet.scm 82 */
																												obj_t
																													BgL_arg1284z00_1222;
																												{	/* Eval/expdlet.scm 82 */
																													obj_t
																														BgL_arg1304z00_1223;
																													{	/* Eval/expdlet.scm 82 */
																														obj_t
																															BgL_arg1305z00_1224;
																														{	/* Eval/expdlet.scm 82 */
																															obj_t
																																BgL_arg1306z00_1225;
																															{	/* Eval/expdlet.scm 82 */
																																obj_t
																																	BgL_arg1307z00_1226;
																																obj_t
																																	BgL_arg1308z00_1227;
																																if (NULLP
																																	(BgL_bindingsz00_1188))
																																	{	/* Eval/expdlet.scm 82 */
																																		BgL_arg1307z00_1226
																																			= BNIL;
																																	}
																																else
																																	{	/* Eval/expdlet.scm 82 */
																																		obj_t
																																			BgL_head1097z00_1230;
																																		{	/* Eval/expdlet.scm 82 */
																																			obj_t
																																				BgL_arg1316z00_1242;
																																			{	/* Eval/expdlet.scm 82 */
																																				obj_t
																																					BgL_pairz00_2177;
																																				BgL_pairz00_2177
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_bindingsz00_1188));
																																				BgL_arg1316z00_1242
																																					=
																																					CAR
																																					(BgL_pairz00_2177);
																																			}
																																			BgL_head1097z00_1230
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1316z00_1242,
																																				BNIL);
																																		}
																																		{	/* Eval/expdlet.scm 82 */
																																			obj_t
																																				BgL_g1100z00_1231;
																																			BgL_g1100z00_1231
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_bindingsz00_1188));
																																			{
																																				obj_t
																																					BgL_l1095z00_2198;
																																				obj_t
																																					BgL_tail1098z00_2199;
																																				BgL_l1095z00_2198
																																					=
																																					BgL_g1100z00_1231;
																																				BgL_tail1098z00_2199
																																					=
																																					BgL_head1097z00_1230;
																																			BgL_zc3z04anonymousza31310ze3z87_2197:
																																				if (NULLP(BgL_l1095z00_2198))
																																					{	/* Eval/expdlet.scm 82 */
																																						BgL_arg1307z00_1226
																																							=
																																							BgL_head1097z00_1230;
																																					}
																																				else
																																					{	/* Eval/expdlet.scm 82 */
																																						obj_t
																																							BgL_newtail1099z00_2206;
																																						{	/* Eval/expdlet.scm 82 */
																																							obj_t
																																								BgL_arg1314z00_2207;
																																							{	/* Eval/expdlet.scm 82 */
																																								obj_t
																																									BgL_pairz00_2211;
																																								BgL_pairz00_2211
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_l1095z00_2198));
																																								BgL_arg1314z00_2207
																																									=
																																									CAR
																																									(BgL_pairz00_2211);
																																							}
																																							BgL_newtail1099z00_2206
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1314z00_2207,
																																								BNIL);
																																						}
																																						SET_CDR
																																							(BgL_tail1098z00_2199,
																																							BgL_newtail1099z00_2206);
																																						{	/* Eval/expdlet.scm 82 */
																																							obj_t
																																								BgL_arg1312z00_2209;
																																							BgL_arg1312z00_2209
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_l1095z00_2198));
																																							{
																																								obj_t
																																									BgL_tail1098z00_2908;
																																								obj_t
																																									BgL_l1095z00_2907;
																																								BgL_l1095z00_2907
																																									=
																																									BgL_arg1312z00_2209;
																																								BgL_tail1098z00_2908
																																									=
																																									BgL_newtail1099z00_2206;
																																								BgL_tail1098z00_2199
																																									=
																																									BgL_tail1098z00_2908;
																																								BgL_l1095z00_2198
																																									=
																																									BgL_l1095z00_2907;
																																								goto
																																									BgL_zc3z04anonymousza31310ze3z87_2197;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	}
																																BgL_arg1308z00_1227
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_expandzd2prognzd2zz__prognz00
																																	(BgL_bodyz00_1189),
																																	BNIL);
																																BgL_arg1306z00_1225
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1307z00_1226,
																																	BgL_arg1308z00_1227);
																															}
																															BgL_arg1305z00_1224
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1996z00zz__expander_letz00,
																																BgL_arg1306z00_1225);
																														}
																														BgL_arg1304z00_1223
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1305z00_1224,
																															BNIL);
																													}
																													BgL_arg1284z00_1222 =
																														MAKE_YOUNG_PAIR
																														(BgL_loopz00_1187,
																														BgL_arg1304z00_1223);
																												}
																												BgL_arg1268z00_1220 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1284z00_1222,
																													BNIL);
																											}
																											{	/* Eval/expdlet.scm 84 */
																												obj_t
																													BgL_arg1319z00_1245;
																												{	/* Eval/expdlet.scm 84 */
																													obj_t
																														BgL_arg1320z00_1246;
																													{	/* Eval/expdlet.scm 84 */
																														obj_t
																															BgL_arg1321z00_1247;
																														if (NULLP
																															(BgL_varsz00_1212))
																															{	/* Eval/expdlet.scm 84 */
																																BgL_arg1321z00_1247
																																	= BNIL;
																															}
																														else
																															{	/* Eval/expdlet.scm 84 */
																																obj_t
																																	BgL_head1103z00_1250;
																																{	/* Eval/expdlet.scm 84 */
																																	obj_t
																																		BgL_arg1328z00_1262;
																																	{	/* Eval/expdlet.scm 84 */
																																		obj_t
																																			BgL_pairz00_2215;
																																		BgL_pairz00_2215
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_varsz00_1212));
																																		BgL_arg1328z00_1262
																																			=
																																			CDR
																																			(BgL_pairz00_2215);
																																	}
																																	BgL_head1103z00_1250
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1328z00_1262,
																																		BNIL);
																																}
																																{	/* Eval/expdlet.scm 84 */
																																	obj_t
																																		BgL_g1106z00_1251;
																																	BgL_g1106z00_1251
																																		=
																																		CDR(((obj_t)
																																			BgL_varsz00_1212));
																																	{
																																		obj_t
																																			BgL_l1101z00_2236;
																																		obj_t
																																			BgL_tail1104z00_2237;
																																		BgL_l1101z00_2236
																																			=
																																			BgL_g1106z00_1251;
																																		BgL_tail1104z00_2237
																																			=
																																			BgL_head1103z00_1250;
																																	BgL_zc3z04anonymousza31323ze3z87_2235:
																																		if (NULLP
																																			(BgL_l1101z00_2236))
																																			{	/* Eval/expdlet.scm 84 */
																																				BgL_arg1321z00_1247
																																					=
																																					BgL_head1103z00_1250;
																																			}
																																		else
																																			{	/* Eval/expdlet.scm 84 */
																																				obj_t
																																					BgL_newtail1105z00_2244;
																																				{	/* Eval/expdlet.scm 84 */
																																					obj_t
																																						BgL_arg1326z00_2245;
																																					{	/* Eval/expdlet.scm 84 */
																																						obj_t
																																							BgL_pairz00_2249;
																																						BgL_pairz00_2249
																																							=
																																							CAR
																																							(((obj_t) BgL_l1101z00_2236));
																																						BgL_arg1326z00_2245
																																							=
																																							CDR
																																							(BgL_pairz00_2249);
																																					}
																																					BgL_newtail1105z00_2244
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1326z00_2245,
																																						BNIL);
																																				}
																																				SET_CDR
																																					(BgL_tail1104z00_2237,
																																					BgL_newtail1105z00_2244);
																																				{	/* Eval/expdlet.scm 84 */
																																					obj_t
																																						BgL_arg1325z00_2247;
																																					BgL_arg1325z00_2247
																																						=
																																						CDR(
																																						((obj_t) BgL_l1101z00_2236));
																																					{
																																						obj_t
																																							BgL_tail1104z00_2934;
																																						obj_t
																																							BgL_l1101z00_2933;
																																						BgL_l1101z00_2933
																																							=
																																							BgL_arg1325z00_2247;
																																						BgL_tail1104z00_2934
																																							=
																																							BgL_newtail1105z00_2244;
																																						BgL_tail1104z00_2237
																																							=
																																							BgL_tail1104z00_2934;
																																						BgL_l1101z00_2236
																																							=
																																							BgL_l1101z00_2933;
																																						goto
																																							BgL_zc3z04anonymousza31323ze3z87_2235;
																																					}
																																				}
																																			}
																																	}
																																}
																															}
																														BgL_arg1320z00_1246
																															=
																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																															(BgL_arg1321z00_1247,
																															BNIL);
																													}
																													BgL_arg1319z00_1245 =
																														MAKE_YOUNG_PAIR
																														(BgL_loopz00_1187,
																														BgL_arg1320z00_1246);
																												}
																												BgL_arg1272z00_1221 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1319z00_1245,
																													BNIL);
																											}
																											BgL_arg1252z00_1219 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1268z00_1220,
																												BgL_arg1272z00_1221);
																										}
																										BgL_recz00_1214 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol1998z00zz__expander_letz00,
																											BgL_arg1252z00_1219);
																									}
																									{	/* Eval/expdlet.scm 82 */
																										obj_t BgL_expz00_1215;

																										if (NULLP(BgL_auxz00_1213))
																											{	/* Eval/expdlet.scm 85 */
																												BgL_expz00_1215 =
																													BgL_recz00_1214;
																											}
																										else
																											{	/* Eval/expdlet.scm 85 */
																												obj_t
																													BgL_arg1248z00_1217;
																												{	/* Eval/expdlet.scm 85 */
																													obj_t
																														BgL_arg1249z00_1218;
																													BgL_arg1249z00_1218 =
																														MAKE_YOUNG_PAIR
																														(BgL_recz00_1214,
																														BNIL);
																													BgL_arg1248z00_1217 =
																														MAKE_YOUNG_PAIR
																														(BgL_auxz00_1213,
																														BgL_arg1249z00_1218);
																												}
																												BgL_expz00_1215 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2000z00zz__expander_letz00,
																													BgL_arg1248z00_1217);
																											}
																										{	/* Eval/expdlet.scm 85 */

																											BgL_resz00_1132 =
																												BGL_PROCEDURE_CALL2
																												(BgL_ez00_1191,
																												BgL_expz00_1215,
																												BgL_ez00_1191);
																										}
																									}
																								}
																							}
																						}
																					else
																						{	/* Eval/expdlet.scm 67 */
																							BgL_resz00_1132 =
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string2001z00zz__expander_letz00,
																								BGl_string2002z00zz__expander_letz00,
																								BgL_xz00_1190);
																						}
																				}
																			}
																	}
																else
																	{	/* Eval/expdlet.scm 111 */
																		BgL_resz00_1132 =
																			BGl_expandzd2errorzd2zz__expandz00
																			(BGl_string2001z00zz__expander_letz00,
																			BGl_string2004z00zz__expander_letz00,
																			BgL_xz00_3);
																	}
															}
														else
															{	/* Eval/expdlet.scm 111 */
																BgL_resz00_1132 =
																	BGl_expandzd2errorzd2zz__expandz00
																	(BGl_string2001z00zz__expander_letz00,
																	BGl_string2004z00zz__expander_letz00,
																	BgL_xz00_3);
															}
													}
												else
													{	/* Eval/expdlet.scm 113 */
														obj_t BgL_arg1232z00_2302;

														BgL_arg1232z00_2302 =
															BGl_expandzd2prognzd2zz__prognz00
															(BgL_cdrzd2143zd2_1147);
														BgL_resz00_1132 =
															BGL_PROCEDURE_CALL2(BgL_ez00_1131,
															BgL_arg1232z00_2302, BgL_ez00_1131);
													}
											}
										else
											{	/* Eval/expdlet.scm 111 */
												obj_t BgL_carzd2203zd2_1164;
												obj_t BgL_cdrzd2204zd2_1165;

												BgL_carzd2203zd2_1164 =
													CAR(((obj_t) BgL_cdrzd2140zd2_1145));
												BgL_cdrzd2204zd2_1165 =
													CDR(((obj_t) BgL_cdrzd2140zd2_1145));
												if (SYMBOLP(BgL_carzd2203zd2_1164))
													{	/* Eval/expdlet.scm 111 */
														if (PAIRP(BgL_cdrzd2204zd2_1165))
															{	/* Eval/expdlet.scm 111 */
																obj_t BgL_cdrzd2211zd2_1168;

																BgL_cdrzd2211zd2_1168 =
																	CDR(BgL_cdrzd2204zd2_1165);
																if (NULLP(BgL_cdrzd2211zd2_1168))
																	{	/* Eval/expdlet.scm 111 */
																		obj_t BgL_carzd2225zd2_1171;

																		BgL_carzd2225zd2_1171 =
																			CAR(((obj_t) BgL_cdrzd2140zd2_1145));
																		if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd2225zd2_1171))
																			{	/* Eval/expdlet.scm 111 */
																				obj_t BgL_arg1225z00_1173;

																				BgL_arg1225z00_1173 =
																					CDR(((obj_t) BgL_cdrzd2140zd2_1145));
																				{
																					obj_t BgL_ez00_2980;
																					obj_t BgL_xz00_2979;
																					obj_t BgL_bodyz00_2978;
																					obj_t BgL_bindingsz00_2977;

																					BgL_bindingsz00_2977 =
																						BgL_carzd2225zd2_1171;
																					BgL_bodyz00_2978 =
																						BgL_arg1225z00_1173;
																					BgL_xz00_2979 = BgL_xz00_3;
																					BgL_ez00_2980 = BgL_ez00_1131;
																					BgL_ez00_1328 = BgL_ez00_2980;
																					BgL_xz00_1327 = BgL_xz00_2979;
																					BgL_bodyz00_1326 = BgL_bodyz00_2978;
																					BgL_bindingsz00_1325 =
																						BgL_bindingsz00_2977;
																					goto
																						BgL_zc3z04anonymousza31361ze3z87_1329;
																				}
																			}
																		else
																			{	/* Eval/expdlet.scm 111 */
																				BgL_resz00_1132 =
																					BGl_expandzd2errorzd2zz__expandz00
																					(BGl_string2001z00zz__expander_letz00,
																					BGl_string2004z00zz__expander_letz00,
																					BgL_xz00_3);
																			}
																	}
																else
																	{
																		obj_t BgL_ez00_2987;
																		obj_t BgL_xz00_2986;
																		obj_t BgL_bodyz00_2985;
																		obj_t BgL_bindingsz00_2983;
																		obj_t BgL_loopz00_2982;

																		BgL_loopz00_2982 = BgL_carzd2203zd2_1164;
																		BgL_bindingsz00_2983 =
																			CAR(BgL_cdrzd2204zd2_1165);
																		BgL_bodyz00_2985 = BgL_cdrzd2211zd2_1168;
																		BgL_xz00_2986 = BgL_xz00_3;
																		BgL_ez00_2987 = BgL_ez00_1131;
																		BgL_ez00_1191 = BgL_ez00_2987;
																		BgL_xz00_1190 = BgL_xz00_2986;
																		BgL_bodyz00_1189 = BgL_bodyz00_2985;
																		BgL_bindingsz00_1188 = BgL_bindingsz00_2983;
																		BgL_loopz00_1187 = BgL_loopz00_2982;
																		goto BgL_zc3z04anonymousza31233ze3z87_1192;
																	}
															}
														else
															{	/* Eval/expdlet.scm 111 */
																obj_t BgL_carzd2244zd2_1176;
																obj_t BgL_cdrzd2245zd2_1177;

																BgL_carzd2244zd2_1176 =
																	CAR(((obj_t) BgL_cdrzd2140zd2_1145));
																BgL_cdrzd2245zd2_1177 =
																	CDR(((obj_t) BgL_cdrzd2140zd2_1145));
																if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
																	(BgL_carzd2244zd2_1176))
																	{	/* Eval/expdlet.scm 111 */
																		if (NULLP(BgL_cdrzd2245zd2_1177))
																			{	/* Eval/expdlet.scm 111 */
																				BgL_resz00_1132 =
																					BGl_expandzd2errorzd2zz__expandz00
																					(BGl_string2001z00zz__expander_letz00,
																					BGl_string2004z00zz__expander_letz00,
																					BgL_xz00_3);
																			}
																		else
																			{
																				obj_t BgL_ez00_3000;
																				obj_t BgL_xz00_2999;
																				obj_t BgL_bodyz00_2998;
																				obj_t BgL_bindingsz00_2997;

																				BgL_bindingsz00_2997 =
																					BgL_carzd2244zd2_1176;
																				BgL_bodyz00_2998 =
																					BgL_cdrzd2245zd2_1177;
																				BgL_xz00_2999 = BgL_xz00_3;
																				BgL_ez00_3000 = BgL_ez00_1131;
																				BgL_ez00_1328 = BgL_ez00_3000;
																				BgL_xz00_1327 = BgL_xz00_2999;
																				BgL_bodyz00_1326 = BgL_bodyz00_2998;
																				BgL_bindingsz00_1325 =
																					BgL_bindingsz00_2997;
																				goto
																					BgL_zc3z04anonymousza31361ze3z87_1329;
																			}
																	}
																else
																	{	/* Eval/expdlet.scm 111 */
																		BgL_resz00_1132 =
																			BGl_expandzd2errorzd2zz__expandz00
																			(BGl_string2001z00zz__expander_letz00,
																			BGl_string2004z00zz__expander_letz00,
																			BgL_xz00_3);
																	}
															}
													}
												else
													{	/* Eval/expdlet.scm 111 */
														obj_t BgL_carzd2261zd2_1181;
														obj_t BgL_cdrzd2262zd2_1182;

														BgL_carzd2261zd2_1181 =
															CAR(((obj_t) BgL_cdrzd2140zd2_1145));
														BgL_cdrzd2262zd2_1182 =
															CDR(((obj_t) BgL_cdrzd2140zd2_1145));
														if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
															(BgL_carzd2261zd2_1181))
															{	/* Eval/expdlet.scm 111 */
																if (NULLP(BgL_cdrzd2262zd2_1182))
																	{	/* Eval/expdlet.scm 111 */
																		BgL_resz00_1132 =
																			BGl_expandzd2errorzd2zz__expandz00
																			(BGl_string2001z00zz__expander_letz00,
																			BGl_string2004z00zz__expander_letz00,
																			BgL_xz00_3);
																	}
																else
																	{
																		obj_t BgL_ez00_3014;
																		obj_t BgL_xz00_3013;
																		obj_t BgL_bodyz00_3012;
																		obj_t BgL_bindingsz00_3011;

																		BgL_bindingsz00_3011 =
																			BgL_carzd2261zd2_1181;
																		BgL_bodyz00_3012 = BgL_cdrzd2262zd2_1182;
																		BgL_xz00_3013 = BgL_xz00_3;
																		BgL_ez00_3014 = BgL_ez00_1131;
																		BgL_ez00_1328 = BgL_ez00_3014;
																		BgL_xz00_1327 = BgL_xz00_3013;
																		BgL_bodyz00_1326 = BgL_bodyz00_3012;
																		BgL_bindingsz00_1325 = BgL_bindingsz00_3011;
																		goto BgL_zc3z04anonymousza31361ze3z87_1329;
																	}
															}
														else
															{	/* Eval/expdlet.scm 111 */
																BgL_resz00_1132 =
																	BGl_expandzd2errorzd2zz__expandz00
																	(BGl_string2001z00zz__expander_letz00,
																	BGl_string2004z00zz__expander_letz00,
																	BgL_xz00_3);
															}
													}
											}
									}
								else
									{	/* Eval/expdlet.scm 111 */
										BgL_resz00_1132 =
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2001z00zz__expander_letz00,
											BGl_string2004z00zz__expander_letz00, BgL_xz00_3);
									}
							}
						else
							{	/* Eval/expdlet.scm 111 */
								BgL_resz00_1132 =
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string2001z00zz__expander_letz00,
									BGl_string2004z00zz__expander_letz00, BgL_xz00_3);
							}
						{	/* Eval/expdlet.scm 111 */

							BGL_TAIL return
								BGl_evepairifyz00zz__prognz00(BgL_resz00_1132, BgL_xz00_3);
						}
					}
				}
			}
		}

	}



/* &expand-eval-let */
	obj_t BGl_z62expandzd2evalzd2letz62zz__expander_letz00(obj_t BgL_envz00_2709,
		obj_t BgL_xz00_2710, obj_t BgL_ez00_2711)
	{
		{	/* Eval/expdlet.scm 64 */
			return
				BGl_expandzd2evalzd2letz00zz__expander_letz00(BgL_xz00_2710,
				BgL_ez00_2711);
		}

	}



/* &<@anonymous:1334> */
	obj_t BGl_z62zc3z04anonymousza31334ze3ze5zz__expander_letz00(obj_t
		BgL_envz00_2712, obj_t BgL_xz00_2713, obj_t BgL_yz00_2714)
	{
		{	/* Eval/expdlet.scm 79 */
			{	/* Eval/expdlet.scm 80 */
				obj_t BgL__andtest_1039z00_2736;

				BgL__andtest_1039z00_2736 = CAR(((obj_t) BgL_xz00_2713));
				if (CBOOL(BgL__andtest_1039z00_2736))
					{	/* Eval/expdlet.scm 80 */
						obj_t BgL_arg1335z00_2737;
						obj_t BgL_arg1336z00_2738;

						BgL_arg1335z00_2737 = CDR(((obj_t) BgL_xz00_2713));
						BgL_arg1336z00_2738 = CDR(((obj_t) BgL_yz00_2714));
						return MAKE_YOUNG_PAIR(BgL_arg1335z00_2737, BgL_arg1336z00_2738);
					}
				else
					{	/* Eval/expdlet.scm 80 */
						return BFALSE;
					}
			}
		}

	}



/* expand-eval-$let */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2evalzd2z42letz42zz__expander_letz00(obj_t
		BgL_xz00_5, obj_t BgL_ez00_6)
	{
		{	/* Eval/expdlet.scm 130 */
			{	/* Eval/expdlet.scm 131 */
				obj_t BgL_nxz00_2317;

				if (EPAIRP(BgL_xz00_5))
					{	/* Eval/expdlet.scm 132 */
						obj_t BgL_arg1391z00_2319;
						obj_t BgL_arg1392z00_2320;

						BgL_arg1391z00_2319 = CDR(((obj_t) BgL_xz00_5));
						BgL_arg1392z00_2320 = CER(((obj_t) BgL_xz00_5));
						{	/* Eval/expdlet.scm 132 */
							obj_t BgL_res1987z00_2325;

							BgL_res1987z00_2325 =
								MAKE_YOUNG_EPAIR(BGl_symbol2000z00zz__expander_letz00,
								BgL_arg1391z00_2319, BgL_arg1392z00_2320);
							BgL_nxz00_2317 = BgL_res1987z00_2325;
						}
					}
				else
					{	/* Eval/expdlet.scm 133 */
						obj_t BgL_arg1393z00_2321;

						BgL_arg1393z00_2321 = CDR(((obj_t) BgL_xz00_5));
						BgL_nxz00_2317 =
							MAKE_YOUNG_PAIR(BGl_symbol2000z00zz__expander_letz00,
							BgL_arg1393z00_2321);
					}
				BGL_TAIL return
					BGl_expandzd2evalzd2letz00zz__expander_letz00(BgL_nxz00_2317,
					BgL_ez00_6);
			}
		}

	}



/* &expand-eval-$let */
	obj_t BGl_z62expandzd2evalzd2z42letz20zz__expander_letz00(obj_t
		BgL_envz00_2715, obj_t BgL_xz00_2716, obj_t BgL_ez00_2717)
	{
		{	/* Eval/expdlet.scm 130 */
			return
				BGl_expandzd2evalzd2z42letz42zz__expander_letz00(BgL_xz00_2716,
				BgL_ez00_2717);
		}

	}



/* expand-eval-let* */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2evalzd2letza2za2zz__expander_letz00(obj_t
		BgL_xz00_7, obj_t BgL_ez00_8)
	{
		{	/* Eval/expdlet.scm 139 */
			{	/* Eval/expdlet.scm 144 */
				obj_t BgL_ez00_1380;

				BgL_ez00_1380 =
					BGl_evalzd2beginzd2expanderz00zz__expander_definez00(BgL_ez00_8);
				{	/* Eval/expdlet.scm 144 */
					obj_t BgL_resz00_1381;

					{
						obj_t BgL_bindingsz00_1384;
						obj_t BgL_bodyz00_1385;

						if (PAIRP(BgL_xz00_7))
							{	/* Eval/expdlet.scm 145 */
								obj_t BgL_cdrzd2296zd2_1390;

								BgL_cdrzd2296zd2_1390 = CDR(((obj_t) BgL_xz00_7));
								if (PAIRP(BgL_cdrzd2296zd2_1390))
									{	/* Eval/expdlet.scm 145 */
										obj_t BgL_cdrzd2299zd2_1392;

										BgL_cdrzd2299zd2_1392 = CDR(BgL_cdrzd2296zd2_1390);
										if (NULLP(CAR(BgL_cdrzd2296zd2_1390)))
											{	/* Eval/expdlet.scm 145 */
												if (NULLP(BgL_cdrzd2299zd2_1392))
													{	/* Eval/expdlet.scm 145 */
														BgL_resz00_1381 =
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2006z00zz__expander_letz00,
															BGl_string2002z00zz__expander_letz00, BgL_xz00_7);
													}
												else
													{	/* Eval/expdlet.scm 147 */
														obj_t BgL_arg1402z00_2371;

														BgL_arg1402z00_2371 =
															BGl_expandzd2prognzd2zz__prognz00
															(BgL_cdrzd2299zd2_1392);
														BgL_resz00_1381 =
															BGL_PROCEDURE_CALL2(BgL_ez00_1380,
															BgL_arg1402z00_2371, BgL_ez00_1380);
													}
											}
										else
											{	/* Eval/expdlet.scm 145 */
												obj_t BgL_carzd2314zd2_1397;
												obj_t BgL_cdrzd2315zd2_1398;

												BgL_carzd2314zd2_1397 =
													CAR(((obj_t) BgL_cdrzd2296zd2_1390));
												BgL_cdrzd2315zd2_1398 =
													CDR(((obj_t) BgL_cdrzd2296zd2_1390));
												if (PAIRP(BgL_carzd2314zd2_1397))
													{	/* Eval/expdlet.scm 145 */
														if (NULLP(BgL_cdrzd2315zd2_1398))
															{	/* Eval/expdlet.scm 145 */
																BgL_resz00_1381 =
																	BGl_expandzd2errorzd2zz__expandz00
																	(BGl_string2006z00zz__expander_letz00,
																	BGl_string2002z00zz__expander_letz00,
																	BgL_xz00_7);
															}
														else
															{	/* Eval/expdlet.scm 145 */
																BgL_bindingsz00_1384 = BgL_carzd2314zd2_1397;
																BgL_bodyz00_1385 = BgL_cdrzd2315zd2_1398;
																{
																	obj_t BgL_bindingsz00_1406;
																	obj_t BgL_nbindingsz00_1407;
																	obj_t BgL_ebdgsz00_1408;

																	BgL_bindingsz00_1406 = BgL_bindingsz00_1384;
																	BgL_nbindingsz00_1407 = BNIL;
																	BgL_ebdgsz00_1408 = BNIL;
																BgL_zc3z04anonymousza31403ze3z87_1409:
																	if (NULLP(BgL_bindingsz00_1406))
																		{	/* Eval/expdlet.scm 154 */
																			obj_t BgL_arg1405z00_1411;

																			{	/* Eval/expdlet.scm 154 */
																				obj_t BgL_arg1406z00_1412;
																				obj_t BgL_arg1407z00_1413;

																				BgL_arg1406z00_1412 =
																					bgl_reverse_bang
																					(BgL_nbindingsz00_1407);
																				BgL_arg1407z00_1413 =
																					MAKE_YOUNG_PAIR
																					(BGl_z52withzd2lexicalz80zz__expandz00
																					(BgL_ebdgsz00_1408,
																						BGl_expandzd2prognzd2zz__prognz00
																						(BgL_bodyz00_1385), BgL_ez00_1380,
																						BFALSE), BNIL);
																				BgL_arg1405z00_1411 =
																					MAKE_YOUNG_PAIR(BgL_arg1406z00_1412,
																					BgL_arg1407z00_1413);
																			}
																			BgL_resz00_1381 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2005z00zz__expander_letz00,
																				BgL_arg1405z00_1411);
																		}
																	else
																		{	/* Eval/expdlet.scm 157 */
																			bool_t BgL_test2079z00_3078;

																			{	/* Eval/expdlet.scm 157 */
																				obj_t BgL_tmpz00_3079;

																				BgL_tmpz00_3079 =
																					CAR(((obj_t) BgL_bindingsz00_1406));
																				BgL_test2079z00_3078 =
																					PAIRP(BgL_tmpz00_3079);
																			}
																			if (BgL_test2079z00_3078)
																				{	/* Eval/expdlet.scm 163 */
																					bool_t BgL_test2080z00_3083;

																					{	/* Eval/expdlet.scm 163 */
																						bool_t BgL_test2081z00_3084;

																						{	/* Eval/expdlet.scm 163 */
																							obj_t BgL_tmpz00_3085;

																							{	/* Eval/expdlet.scm 163 */
																								obj_t BgL_pairz00_2336;

																								BgL_pairz00_2336 =
																									CAR(
																									((obj_t)
																										BgL_bindingsz00_1406));
																								BgL_tmpz00_3085 =
																									CDR(BgL_pairz00_2336);
																							}
																							BgL_test2081z00_3084 =
																								PAIRP(BgL_tmpz00_3085);
																						}
																						if (BgL_test2081z00_3084)
																							{	/* Eval/expdlet.scm 164 */
																								bool_t BgL_test2082z00_3090;

																								{	/* Eval/expdlet.scm 164 */
																									obj_t BgL_tmpz00_3091;

																									{	/* Eval/expdlet.scm 164 */
																										obj_t BgL_pairz00_2342;

																										{	/* Eval/expdlet.scm 164 */
																											obj_t BgL_pairz00_2341;

																											BgL_pairz00_2341 =
																												CAR(
																												((obj_t)
																													BgL_bindingsz00_1406));
																											BgL_pairz00_2342 =
																												CDR(BgL_pairz00_2341);
																										}
																										BgL_tmpz00_3091 =
																											CDR(BgL_pairz00_2342);
																									}
																									BgL_test2082z00_3090 =
																										NULLP(BgL_tmpz00_3091);
																								}
																								if (BgL_test2082z00_3090)
																									{	/* Eval/expdlet.scm 164 */
																										BgL_test2080z00_3083 =
																											((bool_t) 0);
																									}
																								else
																									{	/* Eval/expdlet.scm 164 */
																										BgL_test2080z00_3083 =
																											((bool_t) 1);
																									}
																							}
																						else
																							{	/* Eval/expdlet.scm 163 */
																								BgL_test2080z00_3083 =
																									((bool_t) 1);
																							}
																					}
																					if (BgL_test2080z00_3083)
																						{	/* Eval/expdlet.scm 163 */
																							BgL_resz00_1381 =
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string2006z00zz__expander_letz00,
																								BGl_string2007z00zz__expander_letz00,
																								BgL_xz00_7);
																						}
																					else
																						{	/* Eval/expdlet.scm 167 */
																							obj_t BgL_arg1420z00_1425;
																							obj_t BgL_arg1421z00_1426;
																							obj_t BgL_arg1422z00_1427;

																							BgL_arg1420z00_1425 =
																								CDR(
																								((obj_t) BgL_bindingsz00_1406));
																							{	/* Eval/expdlet.scm 168 */
																								obj_t BgL_arg1423z00_1428;

																								{	/* Eval/expdlet.scm 168 */
																									obj_t BgL_arg1424z00_1429;
																									obj_t BgL_arg1425z00_1430;
																									obj_t BgL_arg1426z00_1431;

																									{	/* Eval/expdlet.scm 168 */
																										obj_t BgL_pairz00_2347;

																										BgL_pairz00_2347 =
																											CAR(
																											((obj_t)
																												BgL_bindingsz00_1406));
																										BgL_arg1424z00_1429 =
																											CAR(BgL_pairz00_2347);
																									}
																									{	/* Eval/expdlet.scm 171 */
																										obj_t BgL_arg1427z00_1432;

																										{	/* Eval/expdlet.scm 171 */
																											obj_t BgL_arg1428z00_1433;

																											{	/* Eval/expdlet.scm 171 */
																												obj_t BgL_pairz00_2351;

																												BgL_pairz00_2351 =
																													CAR(
																													((obj_t)
																														BgL_bindingsz00_1406));
																												BgL_arg1428z00_1433 =
																													CDR(BgL_pairz00_2351);
																											}
																											BgL_arg1427z00_1432 =
																												BGl_expandzd2prognzd2zz__prognz00
																												(BgL_arg1428z00_1433);
																										}
																										BgL_arg1425z00_1430 =
																											BGl_z52withzd2lexicalz80zz__expandz00
																											(BgL_ebdgsz00_1408,
																											BgL_arg1427z00_1432,
																											BgL_ez00_1380, BFALSE);
																									}
																									BgL_arg1426z00_1431 =
																										CAR(
																										((obj_t)
																											BgL_bindingsz00_1406));
																									{	/* Eval/expdlet.scm 142 */
																										obj_t BgL_arg1442z00_2353;

																										{	/* Eval/expdlet.scm 142 */
																											obj_t
																												BgL_list1443z00_2354;
																											{	/* Eval/expdlet.scm 142 */
																												obj_t
																													BgL_arg1444z00_2355;
																												BgL_arg1444z00_2355 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1425z00_1430,
																													BNIL);
																												BgL_list1443z00_2354 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1424z00_1429,
																													BgL_arg1444z00_2355);
																											}
																											BgL_arg1442z00_2353 =
																												BgL_list1443z00_2354;
																										}
																										BgL_arg1423z00_1428 =
																											BGl_evepairifyz00zz__prognz00
																											(BgL_arg1442z00_2353,
																											BgL_arg1426z00_1431);
																									}
																								}
																								BgL_arg1421z00_1426 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1423z00_1428,
																									BgL_nbindingsz00_1407);
																							}
																							{	/* Eval/expdlet.scm 176 */
																								obj_t BgL_arg1429z00_1434;

																								{	/* Eval/expdlet.scm 176 */
																									obj_t BgL_pairz00_2360;

																									BgL_pairz00_2360 =
																										CAR(
																										((obj_t)
																											BgL_bindingsz00_1406));
																									BgL_arg1429z00_1434 =
																										CAR(BgL_pairz00_2360);
																								}
																								BgL_arg1422z00_1427 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1429z00_1434,
																									BgL_ebdgsz00_1408);
																							}
																							{
																								obj_t BgL_ebdgsz00_3120;
																								obj_t BgL_nbindingsz00_3119;
																								obj_t BgL_bindingsz00_3118;

																								BgL_bindingsz00_3118 =
																									BgL_arg1420z00_1425;
																								BgL_nbindingsz00_3119 =
																									BgL_arg1421z00_1426;
																								BgL_ebdgsz00_3120 =
																									BgL_arg1422z00_1427;
																								BgL_ebdgsz00_1408 =
																									BgL_ebdgsz00_3120;
																								BgL_nbindingsz00_1407 =
																									BgL_nbindingsz00_3119;
																								BgL_bindingsz00_1406 =
																									BgL_bindingsz00_3118;
																								goto
																									BgL_zc3z04anonymousza31403ze3z87_1409;
																							}
																						}
																				}
																			else
																				{	/* Eval/expdlet.scm 158 */
																					obj_t BgL_arg1434z00_1439;
																					obj_t BgL_arg1435z00_1440;
																					obj_t BgL_arg1436z00_1441;

																					BgL_arg1434z00_1439 =
																						CDR(((obj_t) BgL_bindingsz00_1406));
																					{	/* Eval/expdlet.scm 159 */
																						obj_t BgL_arg1437z00_1442;

																						{	/* Eval/expdlet.scm 159 */
																							obj_t BgL_arg1438z00_1443;

																							BgL_arg1438z00_1443 =
																								CAR(
																								((obj_t) BgL_bindingsz00_1406));
																							{	/* Eval/expdlet.scm 142 */
																								obj_t BgL_arg1442z00_2363;

																								{	/* Eval/expdlet.scm 142 */
																									obj_t BgL_list1443z00_2364;

																									{	/* Eval/expdlet.scm 142 */
																										obj_t BgL_arg1444z00_2365;

																										BgL_arg1444z00_2365 =
																											MAKE_YOUNG_PAIR(BUNSPEC,
																											BNIL);
																										BgL_list1443z00_2364 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1438z00_1443,
																											BgL_arg1444z00_2365);
																									}
																									BgL_arg1442z00_2363 =
																										BgL_list1443z00_2364;
																								}
																								BgL_arg1437z00_1442 =
																									BGl_evepairifyz00zz__prognz00
																									(BgL_arg1442z00_2363,
																									BgL_bindingsz00_1406);
																							}
																						}
																						BgL_arg1435z00_1440 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1437z00_1442,
																							BgL_nbindingsz00_1407);
																					}
																					{	/* Eval/expdlet.scm 162 */
																						obj_t BgL_arg1439z00_1444;

																						BgL_arg1439z00_1444 =
																							CAR(
																							((obj_t) BgL_bindingsz00_1406));
																						BgL_arg1436z00_1441 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1439z00_1444,
																							BgL_ebdgsz00_1408);
																					}
																					{
																						obj_t BgL_ebdgsz00_3134;
																						obj_t BgL_nbindingsz00_3133;
																						obj_t BgL_bindingsz00_3132;

																						BgL_bindingsz00_3132 =
																							BgL_arg1434z00_1439;
																						BgL_nbindingsz00_3133 =
																							BgL_arg1435z00_1440;
																						BgL_ebdgsz00_3134 =
																							BgL_arg1436z00_1441;
																						BgL_ebdgsz00_1408 =
																							BgL_ebdgsz00_3134;
																						BgL_nbindingsz00_1407 =
																							BgL_nbindingsz00_3133;
																						BgL_bindingsz00_1406 =
																							BgL_bindingsz00_3132;
																						goto
																							BgL_zc3z04anonymousza31403ze3z87_1409;
																					}
																				}
																		}
																}
															}
													}
												else
													{	/* Eval/expdlet.scm 145 */
														BgL_resz00_1381 =
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string2006z00zz__expander_letz00,
															BGl_string2002z00zz__expander_letz00, BgL_xz00_7);
													}
											}
									}
								else
									{	/* Eval/expdlet.scm 145 */
										BgL_resz00_1381 =
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string2006z00zz__expander_letz00,
											BGl_string2002z00zz__expander_letz00, BgL_xz00_7);
									}
							}
						else
							{	/* Eval/expdlet.scm 145 */
								BgL_resz00_1381 =
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string2006z00zz__expander_letz00,
									BGl_string2002z00zz__expander_letz00, BgL_xz00_7);
							}
					}
					{	/* Eval/expdlet.scm 145 */

						BGL_TAIL return
							BGl_evepairifyz00zz__prognz00(BgL_resz00_1381, BgL_xz00_7);
					}
				}
			}
		}

	}



/* &expand-eval-let* */
	obj_t BGl_z62expandzd2evalzd2letza2zc0zz__expander_letz00(obj_t
		BgL_envz00_2718, obj_t BgL_xz00_2719, obj_t BgL_ez00_2720)
	{
		{	/* Eval/expdlet.scm 139 */
			return
				BGl_expandzd2evalzd2letza2za2zz__expander_letz00(BgL_xz00_2719,
				BgL_ez00_2720);
		}

	}



/* expand-eval-letrec */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2evalzd2letrecz00zz__expander_letz00(obj_t
		BgL_xz00_9, obj_t BgL_ez00_10)
	{
		{	/* Eval/expdlet.scm 184 */
			{	/* Eval/expdlet.scm 185 */
				obj_t BgL_ez00_1455;

				BgL_ez00_1455 =
					BGl_evalzd2beginzd2expanderz00zz__expander_definez00(BgL_ez00_10);
				{	/* Eval/expdlet.scm 185 */
					obj_t BgL_resz00_1456;

					{
						obj_t BgL_bindingsz00_1459;
						obj_t BgL_bodyz00_1460;

						if (PAIRP(BgL_xz00_9))
							{	/* Eval/expdlet.scm 186 */
								obj_t BgL_cdrzd2337zd2_1465;

								BgL_cdrzd2337zd2_1465 = CDR(((obj_t) BgL_xz00_9));
								if (PAIRP(BgL_cdrzd2337zd2_1465))
									{	/* Eval/expdlet.scm 186 */
										obj_t BgL_cdrzd2340zd2_1467;

										BgL_cdrzd2340zd2_1467 = CDR(BgL_cdrzd2337zd2_1465);
										if (NULLP(CAR(BgL_cdrzd2337zd2_1465)))
											{	/* Eval/expdlet.scm 186 */
												if (NULLP(BgL_cdrzd2340zd2_1467))
													{	/* Eval/expdlet.scm 186 */
														BgL_resz00_1456 =
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string1999z00zz__expander_letz00,
															BGl_string2002z00zz__expander_letz00, BgL_xz00_9);
													}
												else
													{	/* Eval/expdlet.scm 188 */
														obj_t BgL_arg1453z00_2403;

														BgL_arg1453z00_2403 =
															BGl_expandzd2prognzd2zz__prognz00
															(BgL_cdrzd2340zd2_1467);
														BgL_resz00_1456 =
															BGL_PROCEDURE_CALL2(BgL_ez00_1455,
															BgL_arg1453z00_2403, BgL_ez00_1455);
													}
											}
										else
											{	/* Eval/expdlet.scm 186 */
												obj_t BgL_carzd2355zd2_1472;
												obj_t BgL_cdrzd2356zd2_1473;

												BgL_carzd2355zd2_1472 =
													CAR(((obj_t) BgL_cdrzd2337zd2_1465));
												BgL_cdrzd2356zd2_1473 =
													CDR(((obj_t) BgL_cdrzd2337zd2_1465));
												if (PAIRP(BgL_carzd2355zd2_1472))
													{	/* Eval/expdlet.scm 186 */
														if (NULLP(BgL_cdrzd2356zd2_1473))
															{	/* Eval/expdlet.scm 186 */
																BgL_resz00_1456 =
																	BGl_expandzd2errorzd2zz__expandz00
																	(BGl_string1999z00zz__expander_letz00,
																	BGl_string2002z00zz__expander_letz00,
																	BgL_xz00_9);
															}
														else
															{	/* Eval/expdlet.scm 186 */
																BgL_bindingsz00_1459 = BgL_carzd2355zd2_1472;
																BgL_bodyz00_1460 = BgL_cdrzd2356zd2_1473;
																{
																	obj_t BgL_bindingsz00_1480;
																	obj_t BgL_nbindingsz00_1481;

																	BgL_bindingsz00_1480 = BgL_bindingsz00_1459;
																	BgL_nbindingsz00_1481 = BNIL;
																BgL_zc3z04anonymousza31454ze3z87_1482:
																	if (NULLP(BgL_bindingsz00_1480))
																		{	/* Eval/expdlet.scm 194 */
																			obj_t BgL_nbindingsz00_1484;

																			BgL_nbindingsz00_1484 =
																				bgl_reverse_bang(BgL_nbindingsz00_1481);
																			{	/* Eval/expdlet.scm 194 */
																				obj_t BgL_ebodyz00_1485;

																				BgL_ebodyz00_1485 =
																					BGl_z52withzd2lexicalz80zz__expandz00
																					(BGl_bindingszd2ze3listz31zz__evutilsz00
																					(BgL_bindingsz00_1480),
																					BGl_expandzd2prognzd2zz__prognz00
																					(BgL_bodyz00_1460), BgL_ez00_1455,
																					BFALSE);
																				{	/* Eval/expdlet.scm 195 */

																					{	/* Eval/expdlet.scm 198 */
																						obj_t BgL_arg1456z00_1486;

																						{	/* Eval/expdlet.scm 198 */
																							obj_t BgL_arg1457z00_1487;

																							BgL_arg1457z00_1487 =
																								MAKE_YOUNG_PAIR
																								(BgL_ebodyz00_1485, BNIL);
																							BgL_arg1456z00_1486 =
																								MAKE_YOUNG_PAIR
																								(BgL_nbindingsz00_1484,
																								BgL_arg1457z00_1487);
																						}
																						BgL_resz00_1456 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1998z00zz__expander_letz00,
																							BgL_arg1456z00_1486);
																					}
																				}
																			}
																		}
																	else
																		{	/* Eval/expdlet.scm 199 */
																			bool_t BgL_test2090z00_3178;

																			{	/* Eval/expdlet.scm 199 */
																				obj_t BgL_tmpz00_3179;

																				BgL_tmpz00_3179 =
																					CAR(((obj_t) BgL_bindingsz00_1480));
																				BgL_test2090z00_3178 =
																					PAIRP(BgL_tmpz00_3179);
																			}
																			if (BgL_test2090z00_3178)
																				{	/* Eval/expdlet.scm 203 */
																					bool_t BgL_test2091z00_3183;

																					{	/* Eval/expdlet.scm 203 */
																						bool_t BgL_test2092z00_3184;

																						{	/* Eval/expdlet.scm 203 */
																							obj_t BgL_tmpz00_3185;

																							{	/* Eval/expdlet.scm 203 */
																								obj_t BgL_pairz00_2380;

																								BgL_pairz00_2380 =
																									CAR(
																									((obj_t)
																										BgL_bindingsz00_1480));
																								BgL_tmpz00_3185 =
																									CDR(BgL_pairz00_2380);
																							}
																							BgL_test2092z00_3184 =
																								PAIRP(BgL_tmpz00_3185);
																						}
																						if (BgL_test2092z00_3184)
																							{	/* Eval/expdlet.scm 204 */
																								bool_t BgL_test2093z00_3190;

																								{	/* Eval/expdlet.scm 204 */
																									obj_t BgL_tmpz00_3191;

																									{	/* Eval/expdlet.scm 204 */
																										obj_t BgL_pairz00_2386;

																										{	/* Eval/expdlet.scm 204 */
																											obj_t BgL_pairz00_2385;

																											BgL_pairz00_2385 =
																												CAR(
																												((obj_t)
																													BgL_bindingsz00_1480));
																											BgL_pairz00_2386 =
																												CDR(BgL_pairz00_2385);
																										}
																										BgL_tmpz00_3191 =
																											CDR(BgL_pairz00_2386);
																									}
																									BgL_test2093z00_3190 =
																										NULLP(BgL_tmpz00_3191);
																								}
																								if (BgL_test2093z00_3190)
																									{	/* Eval/expdlet.scm 204 */
																										BgL_test2091z00_3183 =
																											((bool_t) 0);
																									}
																								else
																									{	/* Eval/expdlet.scm 204 */
																										BgL_test2091z00_3183 =
																											((bool_t) 1);
																									}
																							}
																						else
																							{	/* Eval/expdlet.scm 203 */
																								BgL_test2091z00_3183 =
																									((bool_t) 1);
																							}
																					}
																					if (BgL_test2091z00_3183)
																						{	/* Eval/expdlet.scm 203 */
																							BgL_resz00_1456 =
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string1999z00zz__expander_letz00,
																								BGl_string2003z00zz__expander_letz00,
																								BgL_xz00_9);
																						}
																					else
																						{	/* Eval/expdlet.scm 207 */
																							obj_t BgL_arg1469z00_1499;
																							obj_t BgL_arg1472z00_1500;

																							BgL_arg1469z00_1499 =
																								CDR(
																								((obj_t) BgL_bindingsz00_1480));
																							{	/* Eval/expdlet.scm 208 */
																								obj_t BgL_arg1473z00_1501;

																								{	/* Eval/expdlet.scm 208 */
																									obj_t BgL_arg1474z00_1502;
																									obj_t BgL_arg1476z00_1503;

																									{	/* Eval/expdlet.scm 208 */
																										obj_t BgL_pairz00_2391;

																										BgL_pairz00_2391 =
																											CAR(
																											((obj_t)
																												BgL_bindingsz00_1480));
																										BgL_arg1474z00_1502 =
																											CAR(BgL_pairz00_2391);
																									}
																									{	/* Eval/expdlet.scm 209 */
																										obj_t BgL_arg1479z00_1506;

																										{	/* Eval/expdlet.scm 209 */
																											obj_t BgL_arg1480z00_1507;

																											{	/* Eval/expdlet.scm 209 */
																												obj_t BgL_pairz00_2395;

																												BgL_pairz00_2395 =
																													CAR(
																													((obj_t)
																														BgL_bindingsz00_1480));
																												BgL_arg1480z00_1507 =
																													CDR(BgL_pairz00_2395);
																											}
																											BgL_arg1479z00_1506 =
																												BGl_expandzd2prognzd2zz__prognz00
																												(BgL_arg1480z00_1507);
																										}
																										BgL_arg1476z00_1503 =
																											BGL_PROCEDURE_CALL2
																											(BgL_ez00_1455,
																											BgL_arg1479z00_1506,
																											BgL_ez00_1455);
																									}
																									{	/* Eval/expdlet.scm 208 */
																										obj_t BgL_list1477z00_1504;

																										{	/* Eval/expdlet.scm 208 */
																											obj_t BgL_arg1478z00_1505;

																											BgL_arg1478z00_1505 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1476z00_1503,
																												BNIL);
																											BgL_list1477z00_1504 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1474z00_1502,
																												BgL_arg1478z00_1505);
																										}
																										BgL_arg1473z00_1501 =
																											BgL_list1477z00_1504;
																									}
																								}
																								BgL_arg1472z00_1500 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1473z00_1501,
																									BgL_nbindingsz00_1481);
																							}
																							{
																								obj_t BgL_nbindingsz00_3216;
																								obj_t BgL_bindingsz00_3215;

																								BgL_bindingsz00_3215 =
																									BgL_arg1469z00_1499;
																								BgL_nbindingsz00_3216 =
																									BgL_arg1472z00_1500;
																								BgL_nbindingsz00_1481 =
																									BgL_nbindingsz00_3216;
																								BgL_bindingsz00_1480 =
																									BgL_bindingsz00_3215;
																								goto
																									BgL_zc3z04anonymousza31454ze3z87_1482;
																							}
																						}
																				}
																			else
																				{	/* Eval/expdlet.scm 200 */
																					obj_t BgL_arg1483z00_1512;
																					obj_t BgL_arg1484z00_1513;

																					BgL_arg1483z00_1512 =
																						CDR(((obj_t) BgL_bindingsz00_1480));
																					{	/* Eval/expdlet.scm 201 */
																						obj_t BgL_arg1485z00_1514;

																						{	/* Eval/expdlet.scm 201 */
																							obj_t BgL_arg1486z00_1515;

																							BgL_arg1486z00_1515 =
																								CAR(
																								((obj_t) BgL_bindingsz00_1480));
																							{	/* Eval/expdlet.scm 201 */
																								obj_t BgL_list1487z00_1516;

																								{	/* Eval/expdlet.scm 201 */
																									obj_t BgL_arg1488z00_1517;

																									BgL_arg1488z00_1517 =
																										MAKE_YOUNG_PAIR(BUNSPEC,
																										BNIL);
																									BgL_list1487z00_1516 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1486z00_1515,
																										BgL_arg1488z00_1517);
																								}
																								BgL_arg1485z00_1514 =
																									BgL_list1487z00_1516;
																							}
																						}
																						BgL_arg1484z00_1513 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1485z00_1514,
																							BgL_nbindingsz00_1481);
																					}
																					{
																						obj_t BgL_nbindingsz00_3225;
																						obj_t BgL_bindingsz00_3224;

																						BgL_bindingsz00_3224 =
																							BgL_arg1483z00_1512;
																						BgL_nbindingsz00_3225 =
																							BgL_arg1484z00_1513;
																						BgL_nbindingsz00_1481 =
																							BgL_nbindingsz00_3225;
																						BgL_bindingsz00_1480 =
																							BgL_bindingsz00_3224;
																						goto
																							BgL_zc3z04anonymousza31454ze3z87_1482;
																					}
																				}
																		}
																}
															}
													}
												else
													{	/* Eval/expdlet.scm 186 */
														BgL_resz00_1456 =
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string1999z00zz__expander_letz00,
															BGl_string2002z00zz__expander_letz00, BgL_xz00_9);
													}
											}
									}
								else
									{	/* Eval/expdlet.scm 186 */
										BgL_resz00_1456 =
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string1999z00zz__expander_letz00,
											BGl_string2002z00zz__expander_letz00, BgL_xz00_9);
									}
							}
						else
							{	/* Eval/expdlet.scm 186 */
								BgL_resz00_1456 =
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string1999z00zz__expander_letz00,
									BGl_string2002z00zz__expander_letz00, BgL_xz00_9);
							}
					}
					{	/* Eval/expdlet.scm 186 */

						BGL_TAIL return
							BGl_evepairifyz00zz__prognz00(BgL_resz00_1456, BgL_xz00_9);
					}
				}
			}
		}

	}



/* &expand-eval-letrec */
	obj_t BGl_z62expandzd2evalzd2letrecz62zz__expander_letz00(obj_t
		BgL_envz00_2721, obj_t BgL_xz00_2722, obj_t BgL_ez00_2723)
	{
		{	/* Eval/expdlet.scm 184 */
			return
				BGl_expandzd2evalzd2letrecz00zz__expander_letz00(BgL_xz00_2722,
				BgL_ez00_2723);
		}

	}



/* expand-eval-letrec* */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2letrecza2za2zz__expander_letz00(obj_t BgL_xz00_11,
		obj_t BgL_ez00_12)
	{
		{	/* Eval/expdlet.scm 218 */
			{
				obj_t BgL_idz00_1654;

				{	/* Eval/expdlet.scm 237 */
					obj_t BgL_ez00_1522;

					BgL_ez00_1522 =
						BGl_evalzd2beginzd2expanderz00zz__expander_definez00(BgL_ez00_12);
					{	/* Eval/expdlet.scm 237 */
						obj_t BgL_resz00_1523;

						{
							obj_t BgL_bindingsz00_1526;
							obj_t BgL_bodyz00_1527;

							if (PAIRP(BgL_xz00_11))
								{	/* Eval/expdlet.scm 238 */
									obj_t BgL_cdrzd2378zd2_1532;

									BgL_cdrzd2378zd2_1532 = CDR(((obj_t) BgL_xz00_11));
									if (PAIRP(BgL_cdrzd2378zd2_1532))
										{	/* Eval/expdlet.scm 238 */
											obj_t BgL_cdrzd2381zd2_1534;

											BgL_cdrzd2381zd2_1534 = CDR(BgL_cdrzd2378zd2_1532);
											if (NULLP(CAR(BgL_cdrzd2378zd2_1532)))
												{	/* Eval/expdlet.scm 238 */
													if (NULLP(BgL_cdrzd2381zd2_1534))
														{	/* Eval/expdlet.scm 238 */
															BgL_resz00_1523 =
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string2008z00zz__expander_letz00,
																BGl_string2002z00zz__expander_letz00,
																BgL_xz00_11);
														}
													else
														{	/* Eval/expdlet.scm 240 */
															obj_t BgL_arg1498z00_2483;

															BgL_arg1498z00_2483 =
																BGl_expandzd2prognzd2zz__prognz00
																(BgL_cdrzd2381zd2_1534);
															BgL_resz00_1523 =
																BGL_PROCEDURE_CALL2(BgL_ez00_1522,
																BgL_arg1498z00_2483, BgL_ez00_1522);
														}
												}
											else
												{	/* Eval/expdlet.scm 238 */
													obj_t BgL_carzd2396zd2_1539;
													obj_t BgL_cdrzd2397zd2_1540;

													BgL_carzd2396zd2_1539 =
														CAR(((obj_t) BgL_cdrzd2378zd2_1532));
													BgL_cdrzd2397zd2_1540 =
														CDR(((obj_t) BgL_cdrzd2378zd2_1532));
													if (PAIRP(BgL_carzd2396zd2_1539))
														{	/* Eval/expdlet.scm 238 */
															if (NULLP(BgL_cdrzd2397zd2_1540))
																{	/* Eval/expdlet.scm 238 */
																	BgL_resz00_1523 =
																		BGl_expandzd2errorzd2zz__expandz00
																		(BGl_string2008z00zz__expander_letz00,
																		BGl_string2002z00zz__expander_letz00,
																		BgL_xz00_11);
																}
															else
																{	/* Eval/expdlet.scm 238 */
																	BgL_bindingsz00_1526 = BgL_carzd2396zd2_1539;
																	BgL_bodyz00_1527 = BgL_cdrzd2397zd2_1540;
																	{
																		obj_t BgL_l1107z00_1546;

																		BgL_l1107z00_1546 = BgL_bindingsz00_1526;
																	BgL_zc3z04anonymousza31499ze3z87_1547:
																		if (PAIRP(BgL_l1107z00_1546))
																			{	/* Eval/expdlet.scm 243 */
																				{	/* Eval/expdlet.scm 244 */
																					obj_t BgL_bz00_1549;

																					BgL_bz00_1549 =
																						CAR(BgL_l1107z00_1546);
																					{	/* Eval/expdlet.scm 244 */
																						bool_t BgL_test2101z00_3263;

																						if (PAIRP(BgL_bz00_1549))
																							{	/* Eval/expdlet.scm 245 */
																								bool_t BgL_test2103z00_3266;

																								{	/* Eval/expdlet.scm 245 */
																									obj_t BgL_tmpz00_3267;

																									BgL_tmpz00_3267 =
																										CAR(BgL_bz00_1549);
																									BgL_test2103z00_3266 =
																										SYMBOLP(BgL_tmpz00_3267);
																								}
																								if (BgL_test2103z00_3266)
																									{	/* Eval/expdlet.scm 246 */
																										obj_t BgL_tmpz00_3270;

																										BgL_tmpz00_3270 =
																											CDR(BgL_bz00_1549);
																										BgL_test2101z00_3263 =
																											PAIRP(BgL_tmpz00_3270);
																									}
																								else
																									{	/* Eval/expdlet.scm 245 */
																										BgL_test2101z00_3263 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Eval/expdlet.scm 244 */
																								BgL_test2101z00_3263 =
																									((bool_t) 0);
																							}
																						if (BgL_test2101z00_3263)
																							{	/* Eval/expdlet.scm 244 */
																								BFALSE;
																							}
																						else
																							{	/* Eval/expdlet.scm 244 */
																								BGl_expandzd2errorzd2zz__expandz00
																									(BGl_string2008z00zz__expander_letz00,
																									BGl_string2002z00zz__expander_letz00,
																									BgL_xz00_11);
																							}
																					}
																				}
																				{
																					obj_t BgL_l1107z00_3274;

																					BgL_l1107z00_3274 =
																						CDR(BgL_l1107z00_1546);
																					BgL_l1107z00_1546 = BgL_l1107z00_3274;
																					goto
																						BgL_zc3z04anonymousza31499ze3z87_1547;
																				}
																			}
																		else
																			{	/* Eval/expdlet.scm 243 */
																				((bool_t) 1);
																			}
																	}
																	{	/* Eval/expdlet.scm 249 */
																		bool_t BgL_test2104z00_3276;

																		{
																			obj_t BgL_l1109z00_1640;

																			BgL_l1109z00_1640 = BgL_bindingsz00_1526;
																		BgL_zc3z04anonymousza31579ze3z87_1641:
																			if (NULLP(BgL_l1109z00_1640))
																				{	/* Eval/expdlet.scm 249 */
																					BgL_test2104z00_3276 = ((bool_t) 1);
																				}
																			else
																				{	/* Eval/expdlet.scm 249 */
																					bool_t BgL_test2106z00_3279;

																					{	/* Eval/expdlet.scm 249 */
																						obj_t BgL_arg1582z00_1646;

																						BgL_arg1582z00_1646 =
																							CAR(((obj_t) BgL_l1109z00_1640));
																						{	/* Eval/expdlet.scm 221 */
																							bool_t BgL_test2107z00_3282;

																							{	/* Eval/expdlet.scm 221 */
																								obj_t BgL_tmpz00_3283;

																								{	/* Eval/expdlet.scm 221 */
																									obj_t BgL_pairz00_2454;

																									BgL_pairz00_2454 =
																										CDR(
																										((obj_t)
																											BgL_arg1582z00_1646));
																									BgL_tmpz00_3283 =
																										CAR(BgL_pairz00_2454);
																								}
																								BgL_test2107z00_3282 =
																									PAIRP(BgL_tmpz00_3283);
																							}
																							if (BgL_test2107z00_3282)
																								{	/* Eval/expdlet.scm 221 */
																									obj_t BgL_tmpz00_3288;

																									{	/* Eval/expdlet.scm 221 */
																										obj_t BgL_pairz00_2459;

																										{	/* Eval/expdlet.scm 221 */
																											obj_t BgL_pairz00_2458;

																											BgL_pairz00_2458 =
																												CDR(
																												((obj_t)
																													BgL_arg1582z00_1646));
																											BgL_pairz00_2459 =
																												CAR(BgL_pairz00_2458);
																										}
																										BgL_tmpz00_3288 =
																											CAR(BgL_pairz00_2459);
																									}
																									BgL_test2106z00_3279 =
																										(BgL_tmpz00_3288 ==
																										BGl_symbol1996z00zz__expander_letz00);
																								}
																							else
																								{	/* Eval/expdlet.scm 221 */
																									BgL_test2106z00_3279 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2106z00_3279)
																						{	/* Eval/expdlet.scm 249 */
																							obj_t BgL_arg1580z00_1645;

																							BgL_arg1580z00_1645 =
																								CDR(
																								((obj_t) BgL_l1109z00_1640));
																							{
																								obj_t BgL_l1109z00_3296;

																								BgL_l1109z00_3296 =
																									BgL_arg1580z00_1645;
																								BgL_l1109z00_1640 =
																									BgL_l1109z00_3296;
																								goto
																									BgL_zc3z04anonymousza31579ze3z87_1641;
																							}
																						}
																					else
																						{	/* Eval/expdlet.scm 249 */
																							BgL_test2104z00_3276 =
																								((bool_t) 0);
																						}
																				}
																		}
																		if (BgL_test2104z00_3276)
																			{	/* Eval/expdlet.scm 253 */
																				obj_t BgL_arg1516z00_1572;

																				{	/* Eval/expdlet.scm 253 */
																					obj_t BgL_arg1517z00_1573;

																					{	/* Eval/expdlet.scm 253 */
																						obj_t BgL_arg1521z00_1574;

																						{	/* Eval/expdlet.scm 253 */
																							obj_t BgL_arg1522z00_1575;
																							obj_t BgL_arg1523z00_1576;

																							{	/* Eval/expdlet.scm 253 */
																								obj_t BgL_head1114z00_1579;

																								BgL_head1114z00_1579 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1112z00_1581;
																									obj_t BgL_tail1115z00_1582;

																									BgL_l1112z00_1581 =
																										BgL_bindingsz00_1526;
																									BgL_tail1115z00_1582 =
																										BgL_head1114z00_1579;
																								BgL_zc3z04anonymousza31525ze3z87_1583:
																									if (NULLP
																										(BgL_l1112z00_1581))
																										{	/* Eval/expdlet.scm 253 */
																											BgL_arg1522z00_1575 =
																												CDR
																												(BgL_head1114z00_1579);
																										}
																									else
																										{	/* Eval/expdlet.scm 253 */
																											obj_t
																												BgL_newtail1116z00_1585;
																											{	/* Eval/expdlet.scm 253 */
																												obj_t
																													BgL_arg1528z00_1587;
																												{	/* Eval/expdlet.scm 253 */
																													obj_t BgL_bz00_1588;

																													BgL_bz00_1588 =
																														CAR(
																														((obj_t)
																															BgL_l1112z00_1581));
																													{	/* Eval/expdlet.scm 254 */
																														obj_t
																															BgL_arg1529z00_1589;
																														obj_t
																															BgL_arg1530z00_1590;
																														BgL_arg1529z00_1589
																															=
																															CAR(((obj_t)
																																BgL_bz00_1588));
																														{	/* Eval/expdlet.scm 255 */
																															obj_t
																																BgL_arg1536z00_1593;
																															{	/* Eval/expdlet.scm 255 */
																																obj_t
																																	BgL_arg1537z00_1594;
																																BgL_arg1537z00_1594
																																	=
																																	CDR(((obj_t)
																																		BgL_bz00_1588));
																																BgL_arg1536z00_1593
																																	=
																																	BGl_expandzd2prognzd2zz__prognz00
																																	(BgL_arg1537z00_1594);
																															}
																															BgL_arg1530z00_1590
																																=
																																BGL_PROCEDURE_CALL2
																																(BgL_ez00_1522,
																																BgL_arg1536z00_1593,
																																BgL_ez00_1522);
																														}
																														{	/* Eval/expdlet.scm 254 */
																															obj_t
																																BgL_list1531z00_1591;
																															{	/* Eval/expdlet.scm 254 */
																																obj_t
																																	BgL_arg1535z00_1592;
																																BgL_arg1535z00_1592
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1530z00_1590,
																																	BNIL);
																																BgL_list1531z00_1591
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1529z00_1589,
																																	BgL_arg1535z00_1592);
																															}
																															BgL_arg1528z00_1587
																																=
																																BgL_list1531z00_1591;
																														}
																													}
																												}
																												BgL_newtail1116z00_1585
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg1528z00_1587,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1115z00_1582,
																												BgL_newtail1116z00_1585);
																											{	/* Eval/expdlet.scm 253 */
																												obj_t
																													BgL_arg1527z00_1586;
																												BgL_arg1527z00_1586 =
																													CDR(((obj_t)
																														BgL_l1112z00_1581));
																												{
																													obj_t
																														BgL_tail1115z00_3320;
																													obj_t
																														BgL_l1112z00_3319;
																													BgL_l1112z00_3319 =
																														BgL_arg1527z00_1586;
																													BgL_tail1115z00_3320 =
																														BgL_newtail1116z00_1585;
																													BgL_tail1115z00_1582 =
																														BgL_tail1115z00_3320;
																													BgL_l1112z00_1581 =
																														BgL_l1112z00_3319;
																													goto
																														BgL_zc3z04anonymousza31525ze3z87_1583;
																												}
																											}
																										}
																								}
																							}
																							BgL_arg1523z00_1576 =
																								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																								(BgL_bodyz00_1527, BNIL);
																							BgL_arg1521z00_1574 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1522z00_1575,
																								BgL_arg1523z00_1576);
																						}
																						BgL_arg1517z00_1573 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1998z00zz__expander_letz00,
																							BgL_arg1521z00_1574);
																					}
																					BgL_arg1516z00_1572 =
																						BGl_evepairifyz00zz__prognz00
																						(BgL_arg1517z00_1573, BgL_xz00_11);
																				}
																				BgL_resz00_1523 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_1522,
																					BgL_arg1516z00_1572, BgL_ez00_1522);
																			}
																		else
																			{	/* Eval/expdlet.scm 261 */
																				obj_t BgL_arg1539z00_1596;

																				{	/* Eval/expdlet.scm 261 */
																					obj_t BgL_arg1540z00_1597;

																					{	/* Eval/expdlet.scm 261 */
																						obj_t BgL_arg1543z00_1598;

																						{	/* Eval/expdlet.scm 261 */
																							obj_t BgL_arg1544z00_1599;
																							obj_t BgL_arg1546z00_1600;

																							{	/* Eval/expdlet.scm 261 */
																								obj_t BgL_head1119z00_1603;

																								BgL_head1119z00_1603 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1117z00_1605;
																									obj_t BgL_tail1120z00_1606;

																									BgL_l1117z00_1605 =
																										BgL_bindingsz00_1526;
																									BgL_tail1120z00_1606 =
																										BgL_head1119z00_1603;
																								BgL_zc3z04anonymousza31548ze3z87_1607:
																									if (NULLP
																										(BgL_l1117z00_1605))
																										{	/* Eval/expdlet.scm 261 */
																											BgL_arg1544z00_1599 =
																												CDR
																												(BgL_head1119z00_1603);
																										}
																									else
																										{	/* Eval/expdlet.scm 261 */
																											obj_t
																												BgL_newtail1121z00_1609;
																											{	/* Eval/expdlet.scm 261 */
																												obj_t
																													BgL_arg1553z00_1611;
																												{	/* Eval/expdlet.scm 261 */
																													obj_t BgL_bz00_1612;

																													BgL_bz00_1612 =
																														CAR(
																														((obj_t)
																															BgL_l1117z00_1605));
																													{	/* Eval/expdlet.scm 262 */
																														obj_t
																															BgL_arg1554z00_1613;
																														BgL_arg1554z00_1613
																															=
																															CAR(((obj_t)
																																BgL_bz00_1612));
																														{	/* Eval/expdlet.scm 262 */
																															obj_t
																																BgL_list1555z00_1614;
																															{	/* Eval/expdlet.scm 262 */
																																obj_t
																																	BgL_arg1556z00_1615;
																																BgL_arg1556z00_1615
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BUNSPEC,
																																	BNIL);
																																BgL_list1555z00_1614
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1554z00_1613,
																																	BgL_arg1556z00_1615);
																															}
																															BgL_arg1553z00_1611
																																=
																																BgL_list1555z00_1614;
																														}
																													}
																												}
																												BgL_newtail1121z00_1609
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg1553z00_1611,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1120z00_1606,
																												BgL_newtail1121z00_1609);
																											{	/* Eval/expdlet.scm 261 */
																												obj_t
																													BgL_arg1552z00_1610;
																												BgL_arg1552z00_1610 =
																													CDR(((obj_t)
																														BgL_l1117z00_1605));
																												{
																													obj_t
																														BgL_tail1120z00_3345;
																													obj_t
																														BgL_l1117z00_3344;
																													BgL_l1117z00_3344 =
																														BgL_arg1552z00_1610;
																													BgL_tail1120z00_3345 =
																														BgL_newtail1121z00_1609;
																													BgL_tail1120z00_1606 =
																														BgL_tail1120z00_3345;
																													BgL_l1117z00_1605 =
																														BgL_l1117z00_3344;
																													goto
																														BgL_zc3z04anonymousza31548ze3z87_1607;
																												}
																											}
																										}
																								}
																							}
																							{	/* Eval/expdlet.scm 264 */
																								obj_t BgL_arg1557z00_1617;
																								obj_t BgL_arg1558z00_1618;

																								{	/* Eval/expdlet.scm 264 */
																									obj_t BgL_head1124z00_1621;

																									BgL_head1124z00_1621 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_l1122z00_1623;
																										obj_t BgL_tail1125z00_1624;

																										BgL_l1122z00_1623 =
																											BgL_bindingsz00_1526;
																										BgL_tail1125z00_1624 =
																											BgL_head1124z00_1621;
																									BgL_zc3z04anonymousza31560ze3z87_1625:
																										if (NULLP
																											(BgL_l1122z00_1623))
																											{	/* Eval/expdlet.scm 264 */
																												BgL_arg1557z00_1617 =
																													CDR
																													(BgL_head1124z00_1621);
																											}
																										else
																											{	/* Eval/expdlet.scm 264 */
																												obj_t
																													BgL_newtail1126z00_1627;
																												{	/* Eval/expdlet.scm 264 */
																													obj_t
																														BgL_arg1564z00_1629;
																													{	/* Eval/expdlet.scm 264 */
																														obj_t BgL_bz00_1630;

																														BgL_bz00_1630 =
																															CAR(
																															((obj_t)
																																BgL_l1122z00_1623));
																														{	/* Eval/expdlet.scm 265 */
																															obj_t
																																BgL_arg1565z00_1631;
																															{	/* Eval/expdlet.scm 265 */
																																obj_t
																																	BgL_arg1567z00_1632;
																																obj_t
																																	BgL_arg1571z00_1633;
																																{	/* Eval/expdlet.scm 265 */
																																	obj_t
																																		BgL_arg1573z00_1634;
																																	BgL_arg1573z00_1634
																																		=
																																		CAR(((obj_t)
																																			BgL_bz00_1630));
																																	BgL_idz00_1654
																																		=
																																		BgL_arg1573z00_1634;
																																	{	/* Eval/expdlet.scm 224 */
																																		obj_t
																																			BgL_stringz00_1656;
																																		{	/* Eval/expdlet.scm 224 */
																																			obj_t
																																				BgL_arg1796z00_2421;
																																			BgL_arg1796z00_2421
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_idz00_1654);
																																			BgL_stringz00_1656
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1796z00_2421);
																																		}
																																		{	/* Eval/expdlet.scm 224 */
																																			long
																																				BgL_lenz00_1657;
																																			BgL_lenz00_1657
																																				=
																																				STRING_LENGTH
																																				(BgL_stringz00_1656);
																																			{	/* Eval/expdlet.scm 225 */

																																				{
																																					long
																																						BgL_walkerz00_1659;
																																					BgL_walkerz00_1659
																																						=
																																						0L;
																																				BgL_zc3z04anonymousza31588ze3z87_1660:
																																					if (
																																						(BgL_walkerz00_1659
																																							==
																																							BgL_lenz00_1657))
																																						{	/* Eval/expdlet.scm 228 */
																																							BgL_arg1567z00_1632
																																								=
																																								BgL_idz00_1654;
																																						}
																																					else
																																						{	/* Eval/expdlet.scm 230 */
																																							bool_t
																																								BgL_test2112z00_3359;
																																							if ((STRING_REF(BgL_stringz00_1656, BgL_walkerz00_1659) == ((unsigned char) ':')))
																																								{	/* Eval/expdlet.scm 230 */
																																									if ((BgL_walkerz00_1659 < (BgL_lenz00_1657 - 1L)))
																																										{	/* Eval/expdlet.scm 231 */
																																											BgL_test2112z00_3359
																																												=
																																												(STRING_REF
																																												(BgL_stringz00_1656,
																																													(BgL_walkerz00_1659
																																														+
																																														1L))
																																												==
																																												((unsigned char) ':'));
																																										}
																																									else
																																										{	/* Eval/expdlet.scm 231 */
																																											BgL_test2112z00_3359
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Eval/expdlet.scm 230 */
																																									BgL_test2112z00_3359
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																							if (BgL_test2112z00_3359)
																																								{	/* Eval/expdlet.scm 230 */
																																									BgL_arg1567z00_1632
																																										=
																																										bstring_to_symbol
																																										(c_substring
																																										(BgL_stringz00_1656,
																																											0L,
																																											BgL_walkerz00_1659));
																																								}
																																							else
																																								{
																																									long
																																										BgL_walkerz00_3371;
																																									BgL_walkerz00_3371
																																										=
																																										(BgL_walkerz00_1659
																																										+
																																										1L);
																																									BgL_walkerz00_1659
																																										=
																																										BgL_walkerz00_3371;
																																									goto
																																										BgL_zc3z04anonymousza31588ze3z87_1660;
																																								}
																																						}
																																				}
																																			}
																																		}
																																	}
																																}
																																{	/* Eval/expdlet.scm 266 */
																																	obj_t
																																		BgL_arg1575z00_1635;
																																	{	/* Eval/expdlet.scm 266 */
																																		obj_t
																																			BgL_arg1576z00_1636;
																																		{	/* Eval/expdlet.scm 266 */
																																			obj_t
																																				BgL_arg1578z00_1637;
																																			BgL_arg1578z00_1637
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_bz00_1630));
																																			BgL_arg1576z00_1636
																																				=
																																				BGl_expandzd2prognzd2zz__prognz00
																																				(BgL_arg1578z00_1637);
																																		}
																																		BgL_arg1575z00_1635
																																			=
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_1522,
																																			BgL_arg1576z00_1636,
																																			BgL_ez00_1522);
																																	}
																																	BgL_arg1571z00_1633
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1575z00_1635,
																																		BNIL);
																																}
																																BgL_arg1565z00_1631
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1567z00_1632,
																																	BgL_arg1571z00_1633);
																															}
																															BgL_arg1564z00_1629
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2009z00zz__expander_letz00,
																																BgL_arg1565z00_1631);
																														}
																													}
																													BgL_newtail1126z00_1627
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1564z00_1629,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1125z00_1624,
																													BgL_newtail1126z00_1627);
																												{	/* Eval/expdlet.scm 264 */
																													obj_t
																														BgL_arg1562z00_1628;
																													BgL_arg1562z00_1628 =
																														CDR(((obj_t)
																															BgL_l1122z00_1623));
																													{
																														obj_t
																															BgL_tail1125z00_3389;
																														obj_t
																															BgL_l1122z00_3388;
																														BgL_l1122z00_3388 =
																															BgL_arg1562z00_1628;
																														BgL_tail1125z00_3389
																															=
																															BgL_newtail1126z00_1627;
																														BgL_tail1125z00_1624
																															=
																															BgL_tail1125z00_3389;
																														BgL_l1122z00_1623 =
																															BgL_l1122z00_3388;
																														goto
																															BgL_zc3z04anonymousza31560ze3z87_1625;
																													}
																												}
																											}
																									}
																								}
																								BgL_arg1558z00_1618 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_bodyz00_1527, BNIL);
																								BgL_arg1546z00_1600 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_arg1557z00_1617,
																									BgL_arg1558z00_1618);
																							}
																							BgL_arg1543z00_1598 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1544z00_1599,
																								BgL_arg1546z00_1600);
																						}
																						BgL_arg1540z00_1597 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2000z00zz__expander_letz00,
																							BgL_arg1543z00_1598);
																					}
																					BgL_arg1539z00_1596 =
																						BGl_evepairifyz00zz__prognz00
																						(BgL_arg1540z00_1597, BgL_xz00_11);
																				}
																				BgL_resz00_1523 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_1522,
																					BgL_arg1539z00_1596, BgL_ez00_1522);
																			}
																	}
																}
														}
													else
														{	/* Eval/expdlet.scm 238 */
															BgL_resz00_1523 =
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string2008z00zz__expander_letz00,
																BGl_string2002z00zz__expander_letz00,
																BgL_xz00_11);
														}
												}
										}
									else
										{	/* Eval/expdlet.scm 238 */
											BgL_resz00_1523 =
												BGl_expandzd2errorzd2zz__expandz00
												(BGl_string2008z00zz__expander_letz00,
												BGl_string2002z00zz__expander_letz00, BgL_xz00_11);
										}
								}
							else
								{	/* Eval/expdlet.scm 238 */
									BgL_resz00_1523 =
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string2008z00zz__expander_letz00,
										BGl_string2002z00zz__expander_letz00, BgL_xz00_11);
								}
						}
						{	/* Eval/expdlet.scm 238 */

							BGL_TAIL return
								BGl_evepairifyz00zz__prognz00(BgL_resz00_1523, BgL_xz00_11);
						}
					}
				}
			}
		}

	}



/* &expand-eval-letrec* */
	obj_t BGl_z62expandzd2evalzd2letrecza2zc0zz__expander_letz00(obj_t
		BgL_envz00_2724, obj_t BgL_xz00_2725, obj_t BgL_ez00_2726)
	{
		{	/* Eval/expdlet.scm 218 */
			return
				BGl_expandzd2evalzd2letrecza2za2zz__expander_letz00(BgL_xz00_2725,
				BgL_ez00_2726);
		}

	}



/* expand-eval-labels */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2evalzd2labelsz00zz__expander_letz00(obj_t
		BgL_xz00_13, obj_t BgL_ez00_14)
	{
		{	/* Eval/expdlet.scm 278 */
			{	/* Eval/expdlet.scm 279 */
				obj_t BgL_resz00_1682;

				{
					obj_t BgL_bodyz00_1683;
					obj_t BgL_bindingsz00_1685;
					obj_t BgL_bodyz00_1686;

					if (PAIRP(BgL_xz00_13))
						{	/* Eval/expdlet.scm 279 */
							obj_t BgL_cdrzd2419zd2_1691;

							BgL_cdrzd2419zd2_1691 = CDR(((obj_t) BgL_xz00_13));
							if (PAIRP(BgL_cdrzd2419zd2_1691))
								{	/* Eval/expdlet.scm 279 */
									obj_t BgL_cdrzd2422zd2_1693;

									BgL_cdrzd2422zd2_1693 = CDR(BgL_cdrzd2419zd2_1691);
									if (NULLP(CAR(BgL_cdrzd2419zd2_1691)))
										{	/* Eval/expdlet.scm 279 */
											if (NULLP(BgL_cdrzd2422zd2_1693))
												{	/* Eval/expdlet.scm 279 */
													BgL_resz00_1682 =
														BGl_expandzd2errorzd2zz__expandz00
														(BGl_string2011z00zz__expander_letz00,
														BGl_string2002z00zz__expander_letz00, BgL_xz00_13);
												}
											else
												{	/* Eval/expdlet.scm 279 */
													BgL_bodyz00_1683 = BgL_cdrzd2422zd2_1693;
													{	/* Eval/expdlet.scm 281 */
														obj_t BgL_arg1619z00_1702;

														{	/* Eval/expdlet.scm 281 */
															obj_t BgL_arg1620z00_1703;

															{	/* Eval/expdlet.scm 281 */
																obj_t BgL_arg1621z00_1704;

																{	/* Eval/expdlet.scm 281 */
																	obj_t BgL_arg1622z00_1705;

																	BgL_arg1622z00_1705 =
																		MAKE_YOUNG_PAIR
																		(BGl_expandzd2prognzd2zz__prognz00
																		(BgL_bodyz00_1683), BNIL);
																	BgL_arg1621z00_1704 =
																		MAKE_YOUNG_PAIR(BNIL, BgL_arg1622z00_1705);
																}
																BgL_arg1620z00_1703 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1996z00zz__expander_letz00,
																	BgL_arg1621z00_1704);
															}
															BgL_arg1619z00_1702 =
																MAKE_YOUNG_PAIR(BgL_arg1620z00_1703, BNIL);
														}
														BgL_resz00_1682 =
															BGL_PROCEDURE_CALL2(BgL_ez00_14,
															BgL_arg1619z00_1702, BgL_ez00_14);
													}
												}
										}
									else
										{	/* Eval/expdlet.scm 279 */
											obj_t BgL_cdrzd2438zd2_1698;

											BgL_cdrzd2438zd2_1698 =
												CDR(((obj_t) BgL_cdrzd2419zd2_1691));
											if (NULLP(BgL_cdrzd2438zd2_1698))
												{	/* Eval/expdlet.scm 279 */
													BgL_resz00_1682 =
														BGl_expandzd2errorzd2zz__expandz00
														(BGl_string2011z00zz__expander_letz00,
														BGl_string2002z00zz__expander_letz00, BgL_xz00_13);
												}
											else
												{	/* Eval/expdlet.scm 279 */
													obj_t BgL_arg1617z00_1700;

													BgL_arg1617z00_1700 =
														CAR(((obj_t) BgL_cdrzd2419zd2_1691));
													BgL_bindingsz00_1685 = BgL_arg1617z00_1700;
													BgL_bodyz00_1686 = BgL_cdrzd2438zd2_1698;
													{	/* Eval/expdlet.scm 283 */
														obj_t BgL_newz00_1707;

														BgL_newz00_1707 =
															BGl_loopze70ze7zz__expander_letz00(BgL_xz00_13,
															BgL_bindingsz00_1685);
														{	/* Eval/expdlet.scm 298 */
															obj_t BgL_arg1624z00_1708;

															{	/* Eval/expdlet.scm 298 */
																obj_t BgL_arg1625z00_1709;

																BgL_arg1625z00_1709 =
																	MAKE_YOUNG_PAIR(BgL_newz00_1707,
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_bodyz00_1686, BNIL));
																BgL_arg1624z00_1708 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1998z00zz__expander_letz00,
																	BgL_arg1625z00_1709);
															}
															BgL_resz00_1682 =
																BGL_PROCEDURE_CALL2(BgL_ez00_14,
																BgL_arg1624z00_1708, BgL_ez00_14);
														}
													}
												}
										}
								}
							else
								{	/* Eval/expdlet.scm 279 */
									BgL_resz00_1682 =
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string2011z00zz__expander_letz00,
										BGl_string2002z00zz__expander_letz00, BgL_xz00_13);
								}
						}
					else
						{	/* Eval/expdlet.scm 279 */
							BgL_resz00_1682 =
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string2011z00zz__expander_letz00,
								BGl_string2002z00zz__expander_letz00, BgL_xz00_13);
						}
				}
				BGL_TAIL return
					BGl_evepairifyz00zz__prognz00(BgL_resz00_1682, BgL_xz00_13);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__expander_letz00(obj_t BgL_xz00_2730,
		obj_t BgL_bindingsz00_1712)
	{
		{	/* Eval/expdlet.scm 283 */
			if (NULLP(BgL_bindingsz00_1712))
				{	/* Eval/expdlet.scm 285 */
					return BNIL;
				}
			else
				{	/* Eval/expdlet.scm 285 */
					if (PAIRP(BgL_bindingsz00_1712))
						{	/* Eval/expdlet.scm 290 */
							obj_t BgL_ezd2452zd2_1721;

							BgL_ezd2452zd2_1721 = CAR(BgL_bindingsz00_1712);
							if (PAIRP(BgL_ezd2452zd2_1721))
								{	/* Eval/expdlet.scm 290 */
									obj_t BgL_cdrzd2460zd2_1723;

									BgL_cdrzd2460zd2_1723 = CDR(BgL_ezd2452zd2_1721);
									if (PAIRP(BgL_cdrzd2460zd2_1723))
										{	/* Eval/expdlet.scm 290 */
											obj_t BgL_arg1634z00_1725;
											obj_t BgL_arg1636z00_1726;
											obj_t BgL_arg1637z00_1727;

											BgL_arg1634z00_1725 = CAR(BgL_ezd2452zd2_1721);
											BgL_arg1636z00_1726 = CAR(BgL_cdrzd2460zd2_1723);
											BgL_arg1637z00_1727 = CDR(BgL_cdrzd2460zd2_1723);
											{	/* Eval/expdlet.scm 292 */
												obj_t BgL_arg1638z00_2500;
												obj_t BgL_arg1639z00_2501;

												{	/* Eval/expdlet.scm 292 */
													obj_t BgL_arg1640z00_2502;

													{	/* Eval/expdlet.scm 292 */
														obj_t BgL_arg1641z00_2503;

														{	/* Eval/expdlet.scm 292 */
															obj_t BgL_arg1642z00_2504;

															BgL_arg1642z00_2504 =
																MAKE_YOUNG_PAIR(BgL_arg1636z00_1726,
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_arg1637z00_1727, BNIL));
															BgL_arg1641z00_2503 =
																MAKE_YOUNG_PAIR
																(BGl_symbol1996z00zz__expander_letz00,
																BgL_arg1642z00_2504);
														}
														BgL_arg1640z00_2502 =
															MAKE_YOUNG_PAIR(BgL_arg1641z00_2503, BNIL);
													}
													BgL_arg1638z00_2500 =
														MAKE_YOUNG_PAIR(BgL_arg1634z00_1725,
														BgL_arg1640z00_2502);
												}
												BgL_arg1639z00_2501 =
													BGl_loopze70ze7zz__expander_letz00(BgL_xz00_2730,
													CDR(BgL_bindingsz00_1712));
												return
													MAKE_YOUNG_PAIR(BgL_arg1638z00_2500,
													BgL_arg1639z00_2501);
											}
										}
									else
										{	/* Eval/expdlet.scm 290 */
											return
												BGl_expandzd2errorzd2zz__expandz00
												(BGl_string2011z00zz__expander_letz00,
												BGl_string2002z00zz__expander_letz00, BgL_xz00_2730);
										}
								}
							else
								{	/* Eval/expdlet.scm 290 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string2011z00zz__expander_letz00,
										BGl_string2002z00zz__expander_letz00, BgL_xz00_2730);
								}
						}
					else
						{	/* Eval/expdlet.scm 287 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string2011z00zz__expander_letz00,
								BGl_string2002z00zz__expander_letz00, BgL_xz00_2730);
						}
				}
		}

	}



/* &expand-eval-labels */
	obj_t BGl_z62expandzd2evalzd2labelsz62zz__expander_letz00(obj_t
		BgL_envz00_2727, obj_t BgL_xz00_2728, obj_t BgL_ez00_2729)
	{
		{	/* Eval/expdlet.scm 278 */
			return
				BGl_expandzd2evalzd2labelsz00zz__expander_letz00(BgL_xz00_2728,
				BgL_ez00_2729);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_letz00(void)
	{
		{	/* Eval/expdlet.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_letz00(void)
	{
		{	/* Eval/expdlet.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_letz00(void)
	{
		{	/* Eval/expdlet.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_letz00(void)
	{
		{	/* Eval/expdlet.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__expander_definez00(380411245L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
			return BGl_modulezd2initializa7ationz75zz__evutilsz00(470356213L,
				BSTRING_TO_STRING(BGl_string2012z00zz__expander_letz00));
		}

	}

#ifdef __cplusplus
}
#endif
