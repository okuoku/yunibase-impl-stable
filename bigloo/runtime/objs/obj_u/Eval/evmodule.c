/*===========================================================================*/
/*   (Eval/evmodule.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evmodule.scm -indent -o objs/obj_u/Eval/evmodule.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVMODULE_TYPE_DEFINITIONS
#define BGL___EVMODULE_TYPE_DEFINITIONS
#endif													// BGL___EVMODULE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62evmodulezd2checkzd2unboundz62zz__evmodulez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62evmodulezd2staticzd2classz62zz__evmodulez00(obj_t, obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_evmodulezd2importzd2zz__evmodulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evalzd2findzd2modulez62zz__evmodulez00(obj_t, obj_t);
	extern obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31749ze3ze5zz__evmodulez00(obj_t);
	static obj_t BGl_z62evalzd2modulezd2setz12z70zz__evmodulez00(obj_t, obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_z52withzd2tracez80zz__tracez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmodulez00zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_untypezd2identzd2zz__evmodulez00(obj_t);
	static obj_t BGl_z62globalzd2checkzd2unboundz62zz__evmodulez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2moduleszd2mutexza2zd2zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_evmodulezd2staticzd2zz__evmodulez00(obj_t, obj_t, obj_t,
		bool_t);
	extern obj_t bgl_remq_bang(obj_t, obj_t);
	static obj_t
		BGl_evmodulezd2condzd2expandzd2clauseze70z35zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_TAGzd2881ze70z35zz__evmodulez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_evaliaszd2modulezd2zz__evmodulez00(obj_t);
	extern obj_t BGl_evcompilezd2loczd2filenamez00zz__evcompilez00(obj_t);
	static obj_t BGl_za2loadqzd2mutexza2zd2zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32020ze3ze5zz__evmodulez00(obj_t);
	static obj_t BGl_makezd2evmodulezd2zz__evmodulez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void);
	static obj_t BGl_z62evmodulezd2bindzd2globalz12z70zz__evmodulez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
	static obj_t BGl_z62importzd2modulezb0zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	extern obj_t BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evobjectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2modulezd2zz__evmodulez00(void);
	extern obj_t BGl_bigloozd2loadzd2modulez00zz__paramz00(void);
	static obj_t BGl_symbol3008z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_z62evmodulezd2compz12za2zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_evalzd2expandzd2instantiatez00zz__evobjectz00(obj_t);
	static obj_t BGl_zc3z04exitza31644ze3ze70z60zz__evmodulez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62callzd2withzd2evalzd2modulezb0zz__evmodulez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31348ze3ze5zz__evmodulez00(obj_t, obj_t);
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_locationzd2dirzd2zz__evmodulez00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__evmodulez00(void);
	static obj_t BGl_evmodulezd2loadzd2zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evalzd2lookupzd2zz__evenvz00(obj_t);
	extern obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62evmodulezd2loadqzb0zz__evmodulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evmodulezf3z91zz__evmodulez00(obj_t, obj_t);
	extern obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_evmodulezd2extensionzd2zz__evmodulez00(obj_t);
	static bool_t BGl_forzd2eachzf2locz20zz__evmodulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evmodulezd2findzd2globalz62zz__evmodulez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_z62makezd2z52evmoduleze2zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evmodulez00(void);
	static obj_t BGl_za2loadingzd2listza2zd2zz__evmodulez00 = BUNSPEC;
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__evmodulez00(void);
	static obj_t BGl_symbol3114z00zz__evmodulez00 = BUNSPEC;
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_symbol3039z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__evmodulez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evmodulez00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zz__evmodulez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31715ze3ze5zz__evmodulez00(obj_t);
	extern obj_t BGl_evwarningz00zz__everrorz00(obj_t, obj_t);
	extern obj_t BGl_bindzd2evalzd2globalz12z12zz__evenvz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__evmodulez00(void);
	extern obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_evalzd2expandzd2duplicatez00zz__evobjectz00(obj_t);
	static obj_t BGl_symbol3041z00zz__evmodulez00 = BUNSPEC;
	extern obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	static obj_t BGl_symbol3125z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3045z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_evmodulezd2fromzd2zz__evmodulez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00(void);
	static obj_t BGl_z62evmodulez62zz__evmodulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_evmodulezd2optionzd2zz__evmodulez00(obj_t, obj_t);
	extern obj_t BGl_hashtablezd2keyzd2listz00zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmodulezd2findzd2globalz00zz__evmodulez00(obj_t,
		obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62evmodulezd2extensionzb0zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_evmodulezd2importzd2bindingz12z12zz__evmodulez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2evalzd2expanderz00zz__macroz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmodulezd2pathzd2zz__evmodulez00(obj_t);
	static obj_t BGl_symbol3052z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_z62evmodulezd2exportzd2clausez62zz__evmodulez00(obj_t, obj_t,
		obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_dirnamez00zz__osz00(obj_t);
	static obj_t BGl_za2moduleszd2tableza2zd2zz__evmodulez00 = BUNSPEC;
	extern obj_t BGl_interactionzd2environmentzd2zz__evalz00(void);
	static obj_t BGl_symbol3056z00zz__evmodulez00 = BUNSPEC;
	extern obj_t BGl_pwdz00zz__osz00(void);
	extern obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	static obj_t BGl_evmodulezd2exportzd2zz__evmodulez00(obj_t, obj_t, obj_t,
		bool_t);
	static obj_t BGl_z62zc3z04anonymousza31709ze3ze5zz__evmodulez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62evmodulezd2macrozd2tablez62zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_evmodulezd2step1zd2zz__evmodulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_evmodulezd2step2zd2zz__evmodulez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00(obj_t);
	static obj_t BGl_evmodulezd2step3zd2zz__evmodulez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmodulezd2compz12zc0zz__evmodulez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol3060z00zz__evmodulez00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t);
	static obj_t BGl_symbol3062z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3064z00zz__evmodulez00 = BUNSPEC;
	extern obj_t BGl_getzd2sourcezd2locationz00zz__readerz00(obj_t);
	static obj_t BGl_symbol3148z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3068z00zz__evmodulez00 = BUNSPEC;
	extern bool_t BGl_hashtablezf3zf3zz__hashz00(obj_t);
	extern obj_t BGl_portzd2ze3listz31zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62loadzd2modulezb0zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol3070z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3153z00zz__evmodulez00 = BUNSPEC;
	extern int BGl_bigloozd2debugzd2modulez00zz__paramz00(void);
	static obj_t BGl_symbol3156z00zz__evmodulez00 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_symbol3158z00zz__evmodulez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_evalzd2findzd2modulez00zz__evmodulez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31412ze3ze5zz__evmodulez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmodulezd2namezd2zz__evmodulez00(obj_t);
	extern obj_t BGl_expandzd2oncezd2zz__expandz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31735ze3ze5zz__evmodulez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t make_struct(obj_t, int, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2modulezd2setz12z12zz__evmodulez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evmodulezd2macrozd2tablez00zz__evmodulez00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__evmodulez00(obj_t, obj_t);
	extern obj_t BGl_evcompilezd2errorzd2zz__evcompilez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol3160z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_z62evalzd2modulezb0zz__evmodulez00(obj_t);
	static obj_t BGl_symbol3162z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3081z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__evmodulez00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol3083z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3165z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3085z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_z62evmodulezd2staticzd2clausez62zz__evmodulez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol3167z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3169z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_markzd2globalz12zc0zz__evmodulez00(obj_t, obj_t, long,
		obj_t);
	extern obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern obj_t BGl_evalzd2classzd2zz__evobjectz00(obj_t, bool_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_symbol2997z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol2999z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_fromzd2moduleze70z35zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evmodulezd2staticzd2classz00zz__evmodulez00(obj_t);
	extern obj_t BGl_evalzd2expandzd2withzd2accesszd2zz__evobjectz00(obj_t);
	static obj_t BGl_z62evmodulezd2pathzb0zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_symbol3171z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_evmodulezd2libraryzd2zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_symbol3174z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3176z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3095z00zz__evmodulez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2uninitializa7edz75zz__evmodulez00 =
		BUNSPEC;
	static obj_t BGl_loopze70ze7zz__evmodulez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00(obj_t);
	static obj_t BGl_zc3z04anonymousza31974ze3ze70z60zz__evmodulez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evmodulezd2extensionzd2setz12z12zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_symbol3189z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_z62evmodulezd2extensionzd2setz12z70zz__evmodulez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31924ze3ze5zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__evmodulez00(obj_t);
	extern obj_t BGl_hashtablezd2updatez12zc0zz__hashz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza31957ze3ze70z60zz__evmodulez00(obj_t,
		obj_t);
	static obj_t BGl_symbol3191z00zz__evmodulez00 = BUNSPEC;
	static obj_t BGl_symbol3193z00zz__evmodulez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_evmodulezd2includezd2zz__evmodulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evmodulezd2namezb0zz__evmodulez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31997ze3ze5zz__evmodulez00(obj_t);
	static bool_t BGl_aliaszd2pairzf3z21zz__evmodulez00(obj_t);
	static obj_t BGl_evmodulezd2fromz12zc0zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_warningzf2loczf2zz__errorz00(obj_t, obj_t);
	extern obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t BGl_evmodulezd2importz12zc0zz__evmodulez00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_callzd2withzd2evalzd2modulezd2zz__evmodulez00(obj_t, obj_t);
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string3010z00zz__evmodulez00,
		BgL_bgl_string3010za700za7za7_3199za7, "evmodule-path", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2extensionzd2setz12zd2envzc0zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2ex3200z00,
		BGl_z62evmodulezd2extensionzd2setz12z70zz__evmodulez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3011z00zz__evmodulez00,
		BgL_bgl_string3011za700za7za7_3201za7, "evmodule-macro-table", 20);
	      DEFINE_STRING(BGl_string3012z00zz__evmodulez00,
		BgL_bgl_string3012za700za7za7_3202za7, "evmodule-extension", 18);
	      DEFINE_STRING(BGl_string3013z00zz__evmodulez00,
		BgL_bgl_string3013za700za7za7_3203za7, "evmodule-extension-set!", 23);
	      DEFINE_STRING(BGl_string3014z00zz__evmodulez00,
		BgL_bgl_string3014za700za7za7_3204za7, "make-evmodule", 13);
	      DEFINE_STRING(BGl_string3015z00zz__evmodulez00,
		BgL_bgl_string3015za700za7za7_3205za7, "bstring", 7);
	      DEFINE_STRING(BGl_string3016z00zz__evmodulez00,
		BgL_bgl_string3016za700za7za7_3206za7, "\"", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2extensionzd2envz00zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2ex3207z00,
		BGl_z62evmodulezd2extensionzb0zz__evmodulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3017z00zz__evmodulez00,
		BgL_bgl_string3017za700za7za7_3208za7, "\", new (ignored) \"", 18);
	      DEFINE_STRING(BGl_string3018z00zz__evmodulez00,
		BgL_bgl_string3018za700za7za7_3209za7, "'. Previous \"", 13);
	      DEFINE_STRING(BGl_string3019z00zz__evmodulez00,
		BgL_bgl_string3019za700za7za7_3210za7, "Module redefinition `", 21);
	      DEFINE_STRING(BGl_string3100z00zz__evmodulez00,
		BgL_bgl_string3100za700za7za7_3211za7, "~a unbound variable~a", 21);
	      DEFINE_STRING(BGl_string3101z00zz__evmodulez00,
		BgL_bgl_string3101za700za7za7_3212za7, "<@anonymous:1661>", 17);
	      DEFINE_STRING(BGl_string3020z00zz__evmodulez00,
		BgL_bgl_string3020za700za7za7_3213za7, "eval-module-set!", 16);
	      DEFINE_STRING(BGl_string3102z00zz__evmodulez00,
		BgL_bgl_string3102za700za7za7_3214za7, "map", 3);
	      DEFINE_STRING(BGl_string3021z00zz__evmodulez00,
		BgL_bgl_string3021za700za7za7_3215za7, "Illegal module", 14);
	      DEFINE_STRING(BGl_string3103z00zz__evmodulez00,
		BgL_bgl_string3103za700za7za7_3216za7, "~l", 2);
	      DEFINE_STRING(BGl_string3022z00zz__evmodulez00,
		BgL_bgl_string3022za700za7za7_3217za7, "eval-find-module", 16);
	      DEFINE_STRING(BGl_string3104z00zz__evmodulez00,
		BgL_bgl_string3104za700za7za7_3218za7, "<@exit:1644>~0", 14);
	      DEFINE_STRING(BGl_string3023z00zz__evmodulez00,
		BgL_bgl_string3023za700za7za7_3219za7, "&eval-find-module", 17);
	      DEFINE_STRING(BGl_string3105z00zz__evmodulez00,
		BgL_bgl_string3105za700za7za7_3220za7, "Unbound variable", 16);
	      DEFINE_STRING(BGl_string3024z00zz__evmodulez00,
		BgL_bgl_string3024za700za7za7_3221za7, "evalias-module", 14);
	      DEFINE_STRING(BGl_string3106z00zz__evmodulez00,
		BgL_bgl_string3106za700za7za7_3222za7, "global-check-unbound", 20);
	      DEFINE_STRING(BGl_string3025z00zz__evmodulez00,
		BgL_bgl_string3025za700za7za7_3223za7, "vector", 6);
	      DEFINE_STRING(BGl_string3107z00zz__evmodulez00,
		BgL_bgl_string3107za700za7za7_3224za7, "evmodule-load", 13);
	      DEFINE_STRING(BGl_string3026z00zz__evmodulez00,
		BgL_bgl_string3026za700za7za7_3225za7, "evmodule-find-global", 20);
	      DEFINE_STRING(BGl_string3108z00zz__evmodulez00,
		BgL_bgl_string3108za700za7za7_3226za7, "incorect user module loader", 27);
	      DEFINE_STRING(BGl_string3027z00zz__evmodulez00,
		BgL_bgl_string3027za700za7za7_3227za7, "bint", 4);
	      DEFINE_STRING(BGl_string3109z00zz__evmodulez00,
		BgL_bgl_string3109za700za7za7_3228za7, "<@anonymous:1689>", 17);
	      DEFINE_STRING(BGl_string3028z00zz__evmodulez00,
		BgL_bgl_string3028za700za7za7_3229za7, "&evmodule-find-global", 21);
	      DEFINE_STRING(BGl_string3029z00zz__evmodulez00,
		BgL_bgl_string3029za700za7za7_3230za7, "Variable `", 10);
	      DEFINE_STRING(BGl_string3110z00zz__evmodulez00,
		BgL_bgl_string3110za700za7za7_3231za7, "procedure", 9);
	      DEFINE_STRING(BGl_string3111z00zz__evmodulez00,
		BgL_bgl_string3111za700za7za7_3232za7, "~a:cannot find module \"~a\"", 26);
	      DEFINE_STRING(BGl_string3030z00zz__evmodulez00,
		BgL_bgl_string3030za700za7za7_3233za7, "' hidden by an expander.", 24);
	      DEFINE_STRING(BGl_string3112z00zz__evmodulez00,
		BgL_bgl_string3112za700za7za7_3234za7, "<@anonymous:1709>", 17);
	      DEFINE_STRING(BGl_string3031z00zz__evmodulez00,
		BgL_bgl_string3031za700za7za7_3235za7, "evmodule-bind-global!", 21);
	      DEFINE_STRING(BGl_string3113z00zz__evmodulez00,
		BgL_bgl_string3113za700za7za7_3236za7, "evmodule-loadq", 14);
	      DEFINE_STRING(BGl_string3032z00zz__evmodulez00,
		BgL_bgl_string3032za700za7za7_3237za7, "&evmodule-bind-global!", 22);
	      DEFINE_STRING(BGl_string3033z00zz__evmodulez00,
		BgL_bgl_string3033za700za7za7_3238za7, "every", 5);
	      DEFINE_STRING(BGl_string3115z00zz__evmodulez00,
		BgL_bgl_string3115za700za7za7_3239za7,
		"/tmp/bigloo/runtime/Llib/thread.scm", 35);
	      DEFINE_STRING(BGl_string3034z00zz__evmodulez00,
		BgL_bgl_string3034za700za7za7_3240za7, "list", 4);
	      DEFINE_STRING(BGl_string3116z00zz__evmodulez00,
		BgL_bgl_string3116za700za7za7_3241za7, "loop", 4);
	      DEFINE_STRING(BGl_string3035z00zz__evmodulez00,
		BgL_bgl_string3035za700za7za7_3242za7, "evmodule-library", 16);
	      DEFINE_STRING(BGl_string3117z00zz__evmodulez00,
		BgL_bgl_string3117za700za7za7_3243za7, "condvar", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_callzd2withzd2evalzd2modulezd2envz00zz__evmodulez00,
		BgL_bgl_za762callza7d2withza7d3244za7,
		BGl_z62callzd2withzd2evalzd2modulezb0zz__evmodulez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3118z00zz__evmodulez00,
		BgL_bgl_string3118za700za7za7_3245za7, "evmodule-import!", 16);
	      DEFINE_STRING(BGl_string3037z00zz__evmodulez00,
		BgL_bgl_string3037za700za7za7_3246za7, "eval", 4);
	      DEFINE_STRING(BGl_string3119z00zz__evmodulez00,
		BgL_bgl_string3119za700za7za7_3247za7,
		"Cannot find imported module in base \"~a\"", 40);
	      DEFINE_STRING(BGl_string3038z00zz__evmodulez00,
		BgL_bgl_string3038za700za7za7_3248za7, "Illegal `library' clause", 24);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evmodulezd2pathzd2envz00zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2pa3249z00,
		BGl_z62evmodulezd2pathzb0zz__evmodulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3120z00zz__evmodulez00,
		BgL_bgl_string3120za700za7za7_3250za7, "eval:~a", 7);
	      DEFINE_STRING(BGl_string3121z00zz__evmodulez00,
		BgL_bgl_string3121za700za7za7_3251za7, "&load-module", 12);
	      DEFINE_STRING(BGl_string3040z00zz__evmodulez00,
		BgL_bgl_string3040za700za7za7_3252za7, "quote", 5);
	      DEFINE_STRING(BGl_string3122z00zz__evmodulez00,
		BgL_bgl_string3122za700za7za7_3253za7, "&import-module", 14);
	      DEFINE_STRING(BGl_string3123z00zz__evmodulez00,
		BgL_bgl_string3123za700za7za7_3254za7, "<@anonymous:1736>", 17);
	      DEFINE_STRING(BGl_string3042z00zz__evmodulez00,
		BgL_bgl_string3042za700za7za7_3255za7, "library-load", 12);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc3036z00zz__evmodulez00,
		BgL_bgl_za762za7c3za704anonymo3256za7,
		BGl_z62zc3z04anonymousza31412ze3ze5zz__evmodulez00);
	      DEFINE_STRING(BGl_string3124z00zz__evmodulez00,
		BgL_bgl_string3124za700za7za7_3257za7, "<@anonymous:1735>", 17);
	      DEFINE_STRING(BGl_string3043z00zz__evmodulez00,
		BgL_bgl_string3043za700za7za7_3258za7, "<@anonymous:1425>", 17);
	      DEFINE_STRING(BGl_string3044z00zz__evmodulez00,
		BgL_bgl_string3044za700za7za7_3259za7, "for-each", 8);
	      DEFINE_STRING(BGl_string3126z00zz__evmodulez00,
		BgL_bgl_string3126za700za7za7_3260za7, "at", 2);
	      DEFINE_STRING(BGl_string3127z00zz__evmodulez00,
		BgL_bgl_string3127za700za7za7_3261za7, "location-dir", 12);
	      DEFINE_STRING(BGl_string3046z00zz__evmodulez00,
		BgL_bgl_string3046za700za7za7_3262za7, "option", 6);
	      DEFINE_STRING(BGl_string3128z00zz__evmodulez00,
		BgL_bgl_string3128za700za7za7_3263za7, "find-module-files", 17);
	      DEFINE_STRING(BGl_string3047z00zz__evmodulez00,
		BgL_bgl_string3047za700za7za7_3264za7, "Illegal `option' clause", 23);
	      DEFINE_STRING(BGl_string3129z00zz__evmodulez00,
		BgL_bgl_string3129za700za7za7_3265za7, "find-module-imports", 19);
	      DEFINE_STRING(BGl_string3048z00zz__evmodulez00,
		BgL_bgl_string3048za700za7za7_3266za7, "mark-global!", 12);
	      DEFINE_STRING(BGl_string3049z00zz__evmodulez00,
		BgL_bgl_string3049za700za7za7_3267za7, "variable unbound", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evmodulezd2envzd2zz__evmodulez00,
		BgL_bgl_za762evmoduleza762za7za73268z00, BGl_z62evmodulez62zz__evmodulez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3130z00zz__evmodulez00,
		BgL_bgl_string3130za700za7za7_3269za7, "find-module-aliases", 19);
	      DEFINE_STRING(BGl_string3131z00zz__evmodulez00,
		BgL_bgl_string3131za700za7za7_3270za7, "Illegal `import' clause", 23);
	      DEFINE_STRING(BGl_string3050z00zz__evmodulez00,
		BgL_bgl_string3050za700za7za7_3271za7, "evmodule-static", 15);
	      DEFINE_STRING(BGl_string3132z00zz__evmodulez00,
		BgL_bgl_string3132za700za7za7_3272za7, "<@anonymous:1806>", 17);
	      DEFINE_STRING(BGl_string3051z00zz__evmodulez00,
		BgL_bgl_string3051za700za7za7_3273za7, "Illegal `static' clause", 23);
	      DEFINE_STRING(BGl_string3133z00zz__evmodulez00,
		BgL_bgl_string3133za700za7za7_3274za7, "evmodule-import", 15);
	      DEFINE_STRING(BGl_string3134z00zz__evmodulez00,
		BgL_bgl_string3134za700za7za7_3275za7, "evmodule-from!", 14);
	      DEFINE_STRING(BGl_string3053z00zz__evmodulez00,
		BgL_bgl_string3053za700za7za7_3276za7, "define", 6);
	      DEFINE_STRING(BGl_string3135z00zz__evmodulez00,
		BgL_bgl_string3135za700za7za7_3277za7, "*** loading module `", 20);
	      DEFINE_STRING(BGl_string3054z00zz__evmodulez00,
		BgL_bgl_string3054za700za7za7_3278za7, "TAG-114", 7);
	      DEFINE_STRING(BGl_string3136z00zz__evmodulez00,
		BgL_bgl_string3136za700za7za7_3279za7, "' [", 3);
	      DEFINE_STRING(BGl_string3055z00zz__evmodulez00,
		BgL_bgl_string3055za700za7za7_3280za7, "TAG-109", 7);
	      DEFINE_STRING(BGl_string3137z00zz__evmodulez00,
		BgL_bgl_string3137za700za7za7_3281za7, "]...", 4);
	      DEFINE_STRING(BGl_string3138z00zz__evmodulez00,
		BgL_bgl_string3138za700za7za7_3282za7, "Cannot find module", 18);
	      DEFINE_STRING(BGl_string3057z00zz__evmodulez00,
		BgL_bgl_string3057za700za7za7_3283za7, "class", 5);
	      DEFINE_STRING(BGl_string3139z00zz__evmodulez00,
		BgL_bgl_string3139za700za7za7_3284za7, "from-module~0", 13);
	      DEFINE_STRING(BGl_string3058z00zz__evmodulez00,
		BgL_bgl_string3058za700za7za7_3285za7, "evmodule-static-clause", 22);
	      DEFINE_STRING(BGl_string3059z00zz__evmodulez00,
		BgL_bgl_string3059za700za7za7_3286za7, "pair-nil", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2checkzd2unboundzd2envzd2zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2ch3287z00,
		BGl_z62evmodulezd2checkzd2unboundz62zz__evmodulez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3140z00zz__evmodulez00,
		BgL_bgl_string3140za700za7za7_3288za7, "<@anonymous:1846>", 17);
	      DEFINE_STRING(BGl_string3141z00zz__evmodulez00,
		BgL_bgl_string3141za700za7za7_3289za7, "Illegal `from' clause", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2bindzd2globalz12zd2envzc0zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2bi3290z00,
		BGl_z62evmodulezd2bindzd2globalz12z70zz__evmodulez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3142z00zz__evmodulez00,
		BgL_bgl_string3142za700za7za7_3291za7, "from-clause", 11);
	      DEFINE_STRING(BGl_string3061z00zz__evmodulez00,
		BgL_bgl_string3061za700za7za7_3292za7, "final-class", 11);
	      DEFINE_STRING(BGl_string3143z00zz__evmodulez00,
		BgL_bgl_string3143za700za7za7_3293za7, "evmodule-from", 13);
	      DEFINE_STRING(BGl_string3144z00zz__evmodulez00,
		BgL_bgl_string3144za700za7za7_3294za7, "evmodule-include-file!", 22);
	      DEFINE_STRING(BGl_string3063z00zz__evmodulez00,
		BgL_bgl_string3063za700za7za7_3295za7, "abstract-class", 14);
	      DEFINE_STRING(BGl_string3146z00zz__evmodulez00,
		BgL_bgl_string3146za700za7za7_3296za7, "Cannot find include file ~s", 27);
	      DEFINE_STRING(BGl_string3065z00zz__evmodulez00,
		BgL_bgl_string3065za700za7za7_3297za7, "wide-class", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evmodulezd2namezd2envz00zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2na3298z00,
		BGl_z62evmodulezd2namezb0zz__evmodulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3147z00zz__evmodulez00,
		BgL_bgl_string3147za700za7za7_3299za7, "evmodule-include", 16);
	      DEFINE_STRING(BGl_string3066z00zz__evmodulez00,
		BgL_bgl_string3066za700za7za7_3300za7,
		"Wide classes are not supported within eval", 42);
	      DEFINE_STRING(BGl_string3067z00zz__evmodulez00,
		BgL_bgl_string3067za700za7za7_3301za7, "KAP-393", 7);
	      DEFINE_STRING(BGl_string3149z00zz__evmodulez00,
		BgL_bgl_string3149za700za7za7_3302za7, "include", 7);
	      DEFINE_STRING(BGl_string3069z00zz__evmodulez00,
		BgL_bgl_string3069za700za7za7_3303za7, "inline", 6);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_evmodulezd2loadqzd2envz00zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2lo3304z00,
		BGl_z62evmodulezd2loadqzb0zz__evmodulez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3150z00zz__evmodulez00,
		BgL_bgl_string3150za700za7za7_3305za7, "Illegal module clause", 21);
	      DEFINE_STRING(BGl_string3151z00zz__evmodulez00,
		BgL_bgl_string3151za700za7za7_3306za7, "<@anonymous:1924>", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3145z00zz__evmodulez00,
		BgL_bgl_za762za7c3za704anonymo3307za7,
		BGl_z62zc3z04anonymousza31924ze3ze5zz__evmodulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3152z00zz__evmodulez00,
		BgL_bgl_string3152za700za7za7_3308za7, "input-port", 10);
	      DEFINE_STRING(BGl_string3071z00zz__evmodulez00,
		BgL_bgl_string3071za700za7za7_3309za7, "generic", 7);
	      DEFINE_STRING(BGl_string3072z00zz__evmodulez00,
		BgL_bgl_string3072za700za7za7_3310za7, "_eval/loc", 9);
	      DEFINE_STRING(BGl_string3154z00zz__evmodulez00,
		BgL_bgl_string3154za700za7za7_3311za7, "directives", 10);
	      DEFINE_STRING(BGl_string3073z00zz__evmodulez00,
		BgL_bgl_string3073za700za7za7_3312za7, "evmodule-export", 15);
	      DEFINE_STRING(BGl_string3155z00zz__evmodulez00,
		BgL_bgl_string3155za700za7za7_3313za7, "<@anonymous:1939>", 17);
	      DEFINE_STRING(BGl_string3074z00zz__evmodulez00,
		BgL_bgl_string3074za700za7za7_3314za7, "Illegal `export' clause", 23);
	      DEFINE_STRING(BGl_string3075z00zz__evmodulez00,
		BgL_bgl_string3075za700za7za7_3315za7, "TAG-449", 7);
	      DEFINE_STRING(BGl_string3157z00zz__evmodulez00,
		BgL_bgl_string3157za700za7za7_3316za7, "library", 7);
	      DEFINE_STRING(BGl_string3076z00zz__evmodulez00,
		BgL_bgl_string3076za700za7za7_3317za7, "TAG-445", 7);
	      DEFINE_STRING(BGl_string3077z00zz__evmodulez00,
		BgL_bgl_string3077za700za7za7_3318za7, "TAG-443", 7);
	      DEFINE_STRING(BGl_string3159z00zz__evmodulez00,
		BgL_bgl_string3159za700za7za7_3319za7, "static", 6);
	      DEFINE_STRING(BGl_string3078z00zz__evmodulez00,
		BgL_bgl_string3078za700za7za7_3320za7, "TAG-442", 7);
	      DEFINE_STRING(BGl_string3079z00zz__evmodulez00,
		BgL_bgl_string3079za700za7za7_3321za7, "TAG-441", 7);
	      DEFINE_STRING(BGl_string3161z00zz__evmodulez00,
		BgL_bgl_string3161za700za7za7_3322za7, "export", 6);
	      DEFINE_STRING(BGl_string3080z00zz__evmodulez00,
		BgL_bgl_string3080za700za7za7_3323za7, "TAG-440", 7);
	      DEFINE_STRING(BGl_string3163z00zz__evmodulez00,
		BgL_bgl_string3163za700za7za7_3324za7, "load", 4);
	      DEFINE_STRING(BGl_string3082z00zz__evmodulez00,
		BgL_bgl_string3082za700za7za7_3325za7, "macro", 5);
	      DEFINE_STRING(BGl_string3164z00zz__evmodulez00,
		BgL_bgl_string3164za700za7za7_3326za7, "<@anonymous:1948>", 17);
	      DEFINE_STRING(BGl_string3084z00zz__evmodulez00,
		BgL_bgl_string3084za700za7za7_3327za7, "syntax", 6);
	      DEFINE_STRING(BGl_string3166z00zz__evmodulez00,
		BgL_bgl_string3166za700za7za7_3328za7, "import", 6);
	      DEFINE_STRING(BGl_string3086z00zz__evmodulez00,
		BgL_bgl_string3086za700za7za7_3329za7, "expander", 8);
	      DEFINE_STRING(BGl_string3168z00zz__evmodulez00,
		BgL_bgl_string3168za700za7za7_3330za7, "use", 3);
	      DEFINE_STRING(BGl_string3087z00zz__evmodulez00,
		BgL_bgl_string3087za700za7za7_3331za7, "KAP-763", 7);
	      DEFINE_STRING(BGl_string3088z00zz__evmodulez00,
		BgL_bgl_string3088za700za7za7_3332za7, "evmodule-import-binding!", 24);
	      DEFINE_STRING(BGl_string3089z00zz__evmodulez00,
		BgL_bgl_string3089za700za7za7_3333za7, "ERROR: ", 7);
	extern obj_t BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00;
	   
		 
		DEFINE_STRING(BGl_string2995z00zz__evmodulez00,
		BgL_bgl_string2995za700za7za7_3334za7, "modules", 7);
	      DEFINE_STRING(BGl_string2996z00zz__evmodulez00,
		BgL_bgl_string2996za700za7za7_3335za7, "loadq", 5);
	      DEFINE_STRING(BGl_string2998z00zz__evmodulez00,
		BgL_bgl_string2998za700za7za7_3336za7, "#uninitialized", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evmodulezf3zd2envz21zz__evmodulez00,
		BgL_bgl_za762evmoduleza7f3za793337za7, BGl_z62evmodulezf3z91zz__evmodulez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3170z00zz__evmodulez00,
		BgL_bgl_string3170za700za7za7_3338za7, "with", 4);
	      DEFINE_STRING(BGl_string3090z00zz__evmodulez00,
		BgL_bgl_string3090za700za7za7_3339za7, ":", 1);
	      DEFINE_STRING(BGl_string3172z00zz__evmodulez00,
		BgL_bgl_string3172za700za7za7_3340za7, "from", 4);
	      DEFINE_STRING(BGl_string3091z00zz__evmodulez00,
		BgL_bgl_string3091za700za7za7_3341za7, ",", 1);
	      DEFINE_STRING(BGl_string3173z00zz__evmodulez00,
		BgL_bgl_string3173za700za7za7_3342za7, "<@anonymous:1953>", 17);
	      DEFINE_STRING(BGl_string3092z00zz__evmodulez00,
		BgL_bgl_string3092za700za7za7_3343za7, "Eval/evmodule.scm", 17);
	      DEFINE_STRING(BGl_string3093z00zz__evmodulez00,
		BgL_bgl_string3093za700za7za7_3344za7,
		"Cannot find imported variable from module `", 43);
	      DEFINE_STRING(BGl_string3175z00zz__evmodulez00,
		BgL_bgl_string3175za700za7za7_3345za7, "cond-expand", 11);
	      DEFINE_STRING(BGl_string3094z00zz__evmodulez00,
		BgL_bgl_string3094za700za7za7_3346za7, "'", 1);
	      DEFINE_STRING(BGl_string3177z00zz__evmodulez00,
		BgL_bgl_string3177za700za7za7_3347za7, "begin", 5);
	      DEFINE_STRING(BGl_string3096z00zz__evmodulez00,
		BgL_bgl_string3096za700za7za7_3348za7, "@", 1);
	      DEFINE_STRING(BGl_string3178z00zz__evmodulez00,
		BgL_bgl_string3178za700za7za7_3349za7, "<@anonymous:1974>~0", 19);
	      DEFINE_STRING(BGl_string3097z00zz__evmodulez00,
		BgL_bgl_string3097za700za7za7_3350za7, "evmodule-check-unbound", 22);
	      DEFINE_STRING(BGl_string3179z00zz__evmodulez00,
		BgL_bgl_string3179za700za7za7_3351za7, "<@anonymous:1957>~0", 19);
	      DEFINE_STRING(BGl_string3098z00zz__evmodulez00,
		BgL_bgl_string3098za700za7za7_3352za7, "s", 1);
	      DEFINE_STRING(BGl_string3099z00zz__evmodulez00,
		BgL_bgl_string3099za700za7za7_3353za7, "", 0);
	extern obj_t BGl_readzd2envzd2zz__readerz00;
	   
		 
		DEFINE_STRING(BGl_string3180z00zz__evmodulez00,
		BgL_bgl_string3180za700za7za7_3354za7, "Illegal module expression", 25);
	      DEFINE_STRING(BGl_string3182z00zz__evmodulez00,
		BgL_bgl_string3182za700za7za7_3355za7, ".", 1);
	      DEFINE_STRING(BGl_string3183z00zz__evmodulez00,
		BgL_bgl_string3183za700za7za7_3356za7, "TAG-881~0", 9);
	      DEFINE_STRING(BGl_string3184z00zz__evmodulez00,
		BgL_bgl_string3184za700za7za7_3357za7, "Illegal module clauses", 22);
	      DEFINE_STRING(BGl_string3185z00zz__evmodulez00,
		BgL_bgl_string3185za700za7za7_3358za7, "&evmodule", 9);
	      DEFINE_STRING(BGl_string3186z00zz__evmodulez00,
		BgL_bgl_string3186za700za7za7_3359za7, "<@anonymous:1998>", 17);
	      DEFINE_STRING(BGl_string3187z00zz__evmodulez00,
		BgL_bgl_string3187za700za7za7_3360za7, "&evmodule-comp!", 15);
	      DEFINE_STRING(BGl_string3188z00zz__evmodulez00,
		BgL_bgl_string3188za700za7za7_3361za7, "evmodule-static-class", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2findzd2globalzd2envzd2zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2fi3362z00,
		BGl_z62evmodulezd2findzd2globalz62zz__evmodulez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3190z00zz__evmodulez00,
		BgL_bgl_string3190za700za7za7_3363za7, "define-class", 12);
	      DEFINE_STRING(BGl_string3192z00zz__evmodulez00,
		BgL_bgl_string3192za700za7za7_3364za7, "define-final-class", 18);
	      DEFINE_STRING(BGl_string3194z00zz__evmodulez00,
		BgL_bgl_string3194za700za7za7_3365za7, "define-abstract-class", 21);
	      DEFINE_STRING(BGl_string3195z00zz__evmodulez00,
		BgL_bgl_string3195za700za7za7_3366za7, "&evmodule-static-class", 22);
	      DEFINE_STRING(BGl_string3196z00zz__evmodulez00,
		BgL_bgl_string3196za700za7za7_3367za7, "call-with-eval-module", 21);
	      DEFINE_STRING(BGl_string3197z00zz__evmodulez00,
		BgL_bgl_string3197za700za7za7_3368za7, "&call-with-eval-module", 22);
	      DEFINE_STRING(BGl_string3198z00zz__evmodulez00,
		BgL_bgl_string3198za700za7za7_3369za7, "__evmodule", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2findzd2modulezd2envzd2zz__evmodulez00,
		BgL_bgl_za762evalza7d2findza7d3370za7,
		BGl_z62evalzd2findzd2modulez62zz__evmodulez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2modulezd2setz12zd2envzc0zz__evmodulez00,
		BgL_bgl_za762evalza7d2module3371z00,
		BGl_z62evalzd2modulezd2setz12z70zz__evmodulez00, 0L, BUNSPEC, 1);
	extern obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2macrozd2tablezd2envzd2zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2ma3372z00,
		BGl_z62evmodulezd2macrozd2tablez62zz__evmodulez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2modulezd2envz00zz__evmodulez00,
		BgL_bgl_za762evalza7d2module3373z00, BGl_z62evalzd2modulezb0zz__evmodulez00,
		0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_makezd2z52evmodulezd2envz52zz__evmodulez00,
		BgL_bgl_za762makeza7d2za752evm3374za7, va_generic_entry,
		BGl_z62makezd2z52evmoduleze2zz__evmodulez00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evmodulezd2staticzd2classzd2envzd2zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2st3375z00,
		BGl_z62evmodulezd2staticzd2classz62zz__evmodulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3000z00zz__evmodulez00,
		BgL_bgl_string3000za700za7za7_3376za7, "%evmodule", 9);
	      DEFINE_STRING(BGl_string3001z00zz__evmodulez00,
		BgL_bgl_string3001za700za7za7_3377za7,
		"/tmp/bigloo/runtime/Eval/evmodule.scm", 37);
	      DEFINE_STRING(BGl_string3002z00zz__evmodulez00,
		BgL_bgl_string3002za700za7za7_3378za7, "make-%evmodule", 14);
	      DEFINE_STRING(BGl_string3003z00zz__evmodulez00,
		BgL_bgl_string3003za700za7za7_3379za7, "pair", 4);
	      DEFINE_STRING(BGl_string3004z00zz__evmodulez00,
		BgL_bgl_string3004za700za7za7_3380za7, "evmodule?", 9);
	      DEFINE_STRING(BGl_string3005z00zz__evmodulez00,
		BgL_bgl_string3005za700za7za7_3381za7, "symbol", 6);
	      DEFINE_STRING(BGl_string3006z00zz__evmodulez00,
		BgL_bgl_string3006za700za7za7_3382za7, "evmodule-name", 13);
	      DEFINE_STRING(BGl_string3007z00zz__evmodulez00,
		BgL_bgl_string3007za700za7za7_3383za7, "struct", 6);
	      DEFINE_STRING(BGl_string3009z00zz__evmodulez00,
		BgL_bgl_string3009za700za7za7_3384za7, "module", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evmodulezd2compz12zd2envz12zz__evmodulez00,
		BgL_bgl_za762evmoduleza7d2co3385z00, va_generic_entry,
		BGl_z62evmodulezd2compz12za2zz__evmodulez00, BUNSPEC, -4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2moduleszd2mutexza2zd2zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_za2loadqzd2mutexza2zd2zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3008z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_za2loadingzd2listza2zd2zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3114z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3039z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3041z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3125z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3045z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3052z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_za2moduleszd2tableza2zd2zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3056z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3060z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3062z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3064z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3148z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3068z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3070z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3153z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3156z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3158z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3160z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3162z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3081z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3083z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3165z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3085z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3167z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3169z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol2997z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol2999z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3171z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3174z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3176z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3095z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_evmodulezd2uninitializa7edz75zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3189z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3191z00zz__evmodulez00));
		     ADD_ROOT((void *) (&BGl_symbol3193z00zz__evmodulez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long
		BgL_checksumz00_5388, char *BgL_fromz00_5389)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evmodulez00))
				{
					BGl_requirezd2initializa7ationz75zz__evmodulez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evmodulez00();
					BGl_cnstzd2initzd2zz__evmodulez00();
					BGl_importedzd2moduleszd2initz00zz__evmodulez00();
					return BGl_toplevelzd2initzd2zz__evmodulez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			BGl_symbol2997z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string2998z00zz__evmodulez00);
			BGl_symbol2999z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3000z00zz__evmodulez00);
			BGl_symbol3008z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3009z00zz__evmodulez00);
			BGl_symbol3039z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3040z00zz__evmodulez00);
			BGl_symbol3041z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3042z00zz__evmodulez00);
			BGl_symbol3045z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3046z00zz__evmodulez00);
			BGl_symbol3052z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3053z00zz__evmodulez00);
			BGl_symbol3056z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3057z00zz__evmodulez00);
			BGl_symbol3060z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3061z00zz__evmodulez00);
			BGl_symbol3062z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3063z00zz__evmodulez00);
			BGl_symbol3064z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3065z00zz__evmodulez00);
			BGl_symbol3068z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3069z00zz__evmodulez00);
			BGl_symbol3070z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3071z00zz__evmodulez00);
			BGl_symbol3081z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3082z00zz__evmodulez00);
			BGl_symbol3083z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3084z00zz__evmodulez00);
			BGl_symbol3085z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3086z00zz__evmodulez00);
			BGl_symbol3095z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3096z00zz__evmodulez00);
			BGl_symbol3114z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string2996z00zz__evmodulez00);
			BGl_symbol3125z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3126z00zz__evmodulez00);
			BGl_symbol3148z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3149z00zz__evmodulez00);
			BGl_symbol3153z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3154z00zz__evmodulez00);
			BGl_symbol3156z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3157z00zz__evmodulez00);
			BGl_symbol3158z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3159z00zz__evmodulez00);
			BGl_symbol3160z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3161z00zz__evmodulez00);
			BGl_symbol3162z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3163z00zz__evmodulez00);
			BGl_symbol3165z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3166z00zz__evmodulez00);
			BGl_symbol3167z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3168z00zz__evmodulez00);
			BGl_symbol3169z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3170z00zz__evmodulez00);
			BGl_symbol3171z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3172z00zz__evmodulez00);
			BGl_symbol3174z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3175z00zz__evmodulez00);
			BGl_symbol3176z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3177z00zz__evmodulez00);
			BGl_symbol3189z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3190z00zz__evmodulez00);
			BGl_symbol3191z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3192z00zz__evmodulez00);
			return (BGl_symbol3193z00zz__evmodulez00 =
				bstring_to_symbol(BGl_string3194z00zz__evmodulez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			BGl_za2moduleszd2mutexza2zd2zz__evmodulez00 =
				bgl_make_mutex(BGl_string2995z00zz__evmodulez00);
			BGl_za2loadqzd2mutexza2zd2zz__evmodulez00 =
				bgl_make_mutex(BGl_string2996z00zz__evmodulez00);
			BGl_evmodulezd2uninitializa7edz75zz__evmodulez00 =
				BGl_symbol2997z00zz__evmodulez00;
			BGl_za2moduleszd2tableza2zd2zz__evmodulez00 = BNIL;
			return (BGl_za2loadingzd2listza2zd2zz__evmodulez00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__evmodulez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1276;

				BgL_headz00_1276 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2908;
					obj_t BgL_tailz00_2909;

					BgL_prevz00_2908 = BgL_headz00_1276;
					BgL_tailz00_2909 = BgL_l1z00_1;
				BgL_loopz00_2907:
					if (PAIRP(BgL_tailz00_2909))
						{
							obj_t BgL_newzd2prevzd2_2915;

							BgL_newzd2prevzd2_2915 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2909), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2908, BgL_newzd2prevzd2_2915);
							{
								obj_t BgL_tailz00_5441;
								obj_t BgL_prevz00_5440;

								BgL_prevz00_5440 = BgL_newzd2prevzd2_2915;
								BgL_tailz00_5441 = CDR(BgL_tailz00_2909);
								BgL_tailz00_2909 = BgL_tailz00_5441;
								BgL_prevz00_2908 = BgL_prevz00_5440;
								goto BgL_loopz00_2907;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1276);
			}
		}

	}



/* untype-ident */
	obj_t BGl_untypezd2identzd2zz__evmodulez00(obj_t BgL_idz00_3)
	{
		{	/* Eval/evmodule.scm 105 */
			if (SYMBOLP(BgL_idz00_3))
				{	/* Eval/evmodule.scm 108 */
					obj_t BgL_stringz00_1285;

					{	/* Eval/evmodule.scm 108 */
						obj_t BgL_arg2162z00_2923;

						BgL_arg2162z00_2923 = SYMBOL_TO_STRING(BgL_idz00_3);
						BgL_stringz00_1285 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2162z00_2923);
					}
					{	/* Eval/evmodule.scm 108 */
						long BgL_lenz00_1286;

						BgL_lenz00_1286 = STRING_LENGTH(BgL_stringz00_1285);
						{	/* Eval/evmodule.scm 109 */

							{
								long BgL_walkerz00_1288;

								BgL_walkerz00_1288 = 0L;
							BgL_zc3z04anonymousza31307ze3z87_1289:
								if ((BgL_walkerz00_1288 == BgL_lenz00_1286))
									{	/* Eval/evmodule.scm 112 */
										return BgL_idz00_3;
									}
								else
									{	/* Eval/evmodule.scm 114 */
										bool_t BgL_test3390z00_5451;

										if (
											(STRING_REF(BgL_stringz00_1285,
													BgL_walkerz00_1288) == ((unsigned char) ':')))
											{	/* Eval/evmodule.scm 114 */
												if ((BgL_walkerz00_1288 < (BgL_lenz00_1286 - 1L)))
													{	/* Eval/evmodule.scm 115 */
														BgL_test3390z00_5451 =
															(STRING_REF(BgL_stringz00_1285,
																(BgL_walkerz00_1288 + 1L)) ==
															((unsigned char) ':'));
													}
												else
													{	/* Eval/evmodule.scm 115 */
														BgL_test3390z00_5451 = ((bool_t) 0);
													}
											}
										else
											{	/* Eval/evmodule.scm 114 */
												BgL_test3390z00_5451 = ((bool_t) 0);
											}
										if (BgL_test3390z00_5451)
											{	/* Eval/evmodule.scm 114 */
												return
													bstring_to_symbol(c_substring(BgL_stringz00_1285, 0L,
														BgL_walkerz00_1288));
											}
										else
											{
												long BgL_walkerz00_5463;

												BgL_walkerz00_5463 = (BgL_walkerz00_1288 + 1L);
												BgL_walkerz00_1288 = BgL_walkerz00_5463;
												goto BgL_zc3z04anonymousza31307ze3z87_1289;
											}
									}
							}
						}
					}
				}
			else
				{	/* Eval/evmodule.scm 106 */
					return BgL_idz00_3;
				}
		}

	}



/* &make-%evmodule */
	obj_t BGl_z62makezd2z52evmoduleze2zz__evmodulez00(obj_t BgL_envz00_4193,
		obj_t BgL_initz00_4194)
	{
		{	/* Eval/evmodule.scm 129 */
			if (PAIRP(BgL_initz00_4194))
				{	/* Eval/evmodule.scm 129 */
					if (NULLP(CDR(BgL_initz00_4194)))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_arg1329z00_5041;

							BgL_arg1329z00_5041 = CAR(BgL_initz00_4194);
							return
								make_struct(BGl_symbol2999z00zz__evmodulez00,
								(int) (7L), BgL_arg1329z00_5041);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_runner1337z00_5043;

							BgL_runner1337z00_5043 = BgL_initz00_4194;
							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_aux1330z00_5044;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_pairz00_5045;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux2370z00_5046;

										BgL_aux2370z00_5046 = BgL_runner1337z00_5043;
										if (PAIRP(BgL_aux2370z00_5046))
											{	/* Eval/evmodule.scm 129 */
												BgL_pairz00_5045 = BgL_aux2370z00_5046;
											}
										else
											{
												obj_t BgL_auxz00_5475;

												BgL_auxz00_5475 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(4439L),
													BGl_string3002z00zz__evmodulez00,
													BGl_string3003z00zz__evmodulez00,
													BgL_aux2370z00_5046);
												FAILURE(BgL_auxz00_5475, BFALSE, BFALSE);
											}
									}
									BgL_aux1330z00_5044 = CAR(BgL_pairz00_5045);
								}
								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_pairz00_5047;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux2372z00_5048;

										BgL_aux2372z00_5048 = BgL_runner1337z00_5043;
										if (PAIRP(BgL_aux2372z00_5048))
											{	/* Eval/evmodule.scm 129 */
												BgL_pairz00_5047 = BgL_aux2372z00_5048;
											}
										else
											{
												obj_t BgL_auxz00_5482;

												BgL_auxz00_5482 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(4439L),
													BGl_string3002z00zz__evmodulez00,
													BGl_string3003z00zz__evmodulez00,
													BgL_aux2372z00_5048);
												FAILURE(BgL_auxz00_5482, BFALSE, BFALSE);
											}
									}
									BgL_runner1337z00_5043 = CDR(BgL_pairz00_5047);
								}
								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux1331z00_5049;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_pairz00_5050;

										{	/* Eval/evmodule.scm 129 */
											obj_t BgL_aux2374z00_5051;

											BgL_aux2374z00_5051 = BgL_runner1337z00_5043;
											if (PAIRP(BgL_aux2374z00_5051))
												{	/* Eval/evmodule.scm 129 */
													BgL_pairz00_5050 = BgL_aux2374z00_5051;
												}
											else
												{
													obj_t BgL_auxz00_5489;

													BgL_auxz00_5489 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(4439L),
														BGl_string3002z00zz__evmodulez00,
														BGl_string3003z00zz__evmodulez00,
														BgL_aux2374z00_5051);
													FAILURE(BgL_auxz00_5489, BFALSE, BFALSE);
												}
										}
										BgL_aux1331z00_5049 = CAR(BgL_pairz00_5050);
									}
									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_pairz00_5052;

										{	/* Eval/evmodule.scm 129 */
											obj_t BgL_aux2376z00_5053;

											BgL_aux2376z00_5053 = BgL_runner1337z00_5043;
											if (PAIRP(BgL_aux2376z00_5053))
												{	/* Eval/evmodule.scm 129 */
													BgL_pairz00_5052 = BgL_aux2376z00_5053;
												}
											else
												{
													obj_t BgL_auxz00_5496;

													BgL_auxz00_5496 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(4439L),
														BGl_string3002z00zz__evmodulez00,
														BGl_string3003z00zz__evmodulez00,
														BgL_aux2376z00_5053);
													FAILURE(BgL_auxz00_5496, BFALSE, BFALSE);
												}
										}
										BgL_runner1337z00_5043 = CDR(BgL_pairz00_5052);
									}
									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux1332z00_5054;

										{	/* Eval/evmodule.scm 129 */
											obj_t BgL_pairz00_5055;

											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_aux2378z00_5056;

												BgL_aux2378z00_5056 = BgL_runner1337z00_5043;
												if (PAIRP(BgL_aux2378z00_5056))
													{	/* Eval/evmodule.scm 129 */
														BgL_pairz00_5055 = BgL_aux2378z00_5056;
													}
												else
													{
														obj_t BgL_auxz00_5503;

														BgL_auxz00_5503 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(4439L),
															BGl_string3002z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00,
															BgL_aux2378z00_5056);
														FAILURE(BgL_auxz00_5503, BFALSE, BFALSE);
													}
											}
											BgL_aux1332z00_5054 = CAR(BgL_pairz00_5055);
										}
										{	/* Eval/evmodule.scm 129 */
											obj_t BgL_pairz00_5057;

											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_aux2380z00_5058;

												BgL_aux2380z00_5058 = BgL_runner1337z00_5043;
												if (PAIRP(BgL_aux2380z00_5058))
													{	/* Eval/evmodule.scm 129 */
														BgL_pairz00_5057 = BgL_aux2380z00_5058;
													}
												else
													{
														obj_t BgL_auxz00_5510;

														BgL_auxz00_5510 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(4439L),
															BGl_string3002z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00,
															BgL_aux2380z00_5058);
														FAILURE(BgL_auxz00_5510, BFALSE, BFALSE);
													}
											}
											BgL_runner1337z00_5043 = CDR(BgL_pairz00_5057);
										}
										{	/* Eval/evmodule.scm 129 */
											obj_t BgL_aux1333z00_5059;

											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_pairz00_5060;

												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_aux2382z00_5061;

													BgL_aux2382z00_5061 = BgL_runner1337z00_5043;
													if (PAIRP(BgL_aux2382z00_5061))
														{	/* Eval/evmodule.scm 129 */
															BgL_pairz00_5060 = BgL_aux2382z00_5061;
														}
													else
														{
															obj_t BgL_auxz00_5517;

															BgL_auxz00_5517 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(4439L),
																BGl_string3002z00zz__evmodulez00,
																BGl_string3003z00zz__evmodulez00,
																BgL_aux2382z00_5061);
															FAILURE(BgL_auxz00_5517, BFALSE, BFALSE);
														}
												}
												BgL_aux1333z00_5059 = CAR(BgL_pairz00_5060);
											}
											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_pairz00_5062;

												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_aux2384z00_5063;

													BgL_aux2384z00_5063 = BgL_runner1337z00_5043;
													if (PAIRP(BgL_aux2384z00_5063))
														{	/* Eval/evmodule.scm 129 */
															BgL_pairz00_5062 = BgL_aux2384z00_5063;
														}
													else
														{
															obj_t BgL_auxz00_5524;

															BgL_auxz00_5524 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(4439L),
																BGl_string3002z00zz__evmodulez00,
																BGl_string3003z00zz__evmodulez00,
																BgL_aux2384z00_5063);
															FAILURE(BgL_auxz00_5524, BFALSE, BFALSE);
														}
												}
												BgL_runner1337z00_5043 = CDR(BgL_pairz00_5062);
											}
											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_aux1334z00_5064;

												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_pairz00_5065;

													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_aux2386z00_5066;

														BgL_aux2386z00_5066 = BgL_runner1337z00_5043;
														if (PAIRP(BgL_aux2386z00_5066))
															{	/* Eval/evmodule.scm 129 */
																BgL_pairz00_5065 = BgL_aux2386z00_5066;
															}
														else
															{
																obj_t BgL_auxz00_5531;

																BgL_auxz00_5531 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(4439L), BGl_string3002z00zz__evmodulez00,
																	BGl_string3003z00zz__evmodulez00,
																	BgL_aux2386z00_5066);
																FAILURE(BgL_auxz00_5531, BFALSE, BFALSE);
															}
													}
													BgL_aux1334z00_5064 = CAR(BgL_pairz00_5065);
												}
												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_pairz00_5067;

													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_aux2388z00_5068;

														BgL_aux2388z00_5068 = BgL_runner1337z00_5043;
														if (PAIRP(BgL_aux2388z00_5068))
															{	/* Eval/evmodule.scm 129 */
																BgL_pairz00_5067 = BgL_aux2388z00_5068;
															}
														else
															{
																obj_t BgL_auxz00_5538;

																BgL_auxz00_5538 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(4439L), BGl_string3002z00zz__evmodulez00,
																	BGl_string3003z00zz__evmodulez00,
																	BgL_aux2388z00_5068);
																FAILURE(BgL_auxz00_5538, BFALSE, BFALSE);
															}
													}
													BgL_runner1337z00_5043 = CDR(BgL_pairz00_5067);
												}
												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_aux1335z00_5069;

													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_pairz00_5070;

														{	/* Eval/evmodule.scm 129 */
															obj_t BgL_aux2390z00_5071;

															BgL_aux2390z00_5071 = BgL_runner1337z00_5043;
															if (PAIRP(BgL_aux2390z00_5071))
																{	/* Eval/evmodule.scm 129 */
																	BgL_pairz00_5070 = BgL_aux2390z00_5071;
																}
															else
																{
																	obj_t BgL_auxz00_5545;

																	BgL_auxz00_5545 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(4439L),
																		BGl_string3002z00zz__evmodulez00,
																		BGl_string3003z00zz__evmodulez00,
																		BgL_aux2390z00_5071);
																	FAILURE(BgL_auxz00_5545, BFALSE, BFALSE);
																}
														}
														BgL_aux1335z00_5069 = CAR(BgL_pairz00_5070);
													}
													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_pairz00_5072;

														{	/* Eval/evmodule.scm 129 */
															obj_t BgL_aux2392z00_5073;

															BgL_aux2392z00_5073 = BgL_runner1337z00_5043;
															if (PAIRP(BgL_aux2392z00_5073))
																{	/* Eval/evmodule.scm 129 */
																	BgL_pairz00_5072 = BgL_aux2392z00_5073;
																}
															else
																{
																	obj_t BgL_auxz00_5552;

																	BgL_auxz00_5552 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(4439L),
																		BGl_string3002z00zz__evmodulez00,
																		BGl_string3003z00zz__evmodulez00,
																		BgL_aux2392z00_5073);
																	FAILURE(BgL_auxz00_5552, BFALSE, BFALSE);
																}
														}
														BgL_runner1337z00_5043 = CDR(BgL_pairz00_5072);
													}
													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_aux1336z00_5074;

														{	/* Eval/evmodule.scm 129 */
															obj_t BgL_pairz00_5075;

															{	/* Eval/evmodule.scm 129 */
																obj_t BgL_aux2394z00_5076;

																BgL_aux2394z00_5076 = BgL_runner1337z00_5043;
																if (PAIRP(BgL_aux2394z00_5076))
																	{	/* Eval/evmodule.scm 129 */
																		BgL_pairz00_5075 = BgL_aux2394z00_5076;
																	}
																else
																	{
																		obj_t BgL_auxz00_5559;

																		BgL_auxz00_5559 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(4439L),
																			BGl_string3002z00zz__evmodulez00,
																			BGl_string3003z00zz__evmodulez00,
																			BgL_aux2394z00_5076);
																		FAILURE(BgL_auxz00_5559, BFALSE, BFALSE);
																	}
															}
															BgL_aux1336z00_5074 = CAR(BgL_pairz00_5075);
														}
														{	/* Eval/evmodule.scm 129 */
															obj_t BgL_newz00_5077;

															BgL_newz00_5077 =
																create_struct(BGl_symbol2999z00zz__evmodulez00,
																(int) (7L));
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5566;

																BgL_tmpz00_5566 = (int) (6L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5566,
																	BgL_aux1336z00_5074);
															}
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5569;

																BgL_tmpz00_5569 = (int) (5L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5569,
																	BgL_aux1335z00_5069);
															}
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5572;

																BgL_tmpz00_5572 = (int) (4L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5572,
																	BgL_aux1334z00_5064);
															}
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5575;

																BgL_tmpz00_5575 = (int) (3L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5575,
																	BgL_aux1333z00_5059);
															}
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5578;

																BgL_tmpz00_5578 = (int) (2L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5578,
																	BgL_aux1332z00_5054);
															}
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5581;

																BgL_tmpz00_5581 = (int) (1L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5581,
																	BgL_aux1331z00_5049);
															}
															{	/* Eval/evmodule.scm 129 */
																int BgL_tmpz00_5584;

																BgL_tmpz00_5584 = (int) (0L);
																STRUCT_SET(BgL_newz00_5077, BgL_tmpz00_5584,
																	BgL_aux1330z00_5044);
															}
															return BgL_newz00_5077;
														}
													}
												}
											}
										}
									}
								}
							}
						}
				}
			else
				{	/* Eval/evmodule.scm 129 */
					return
						make_struct(BGl_symbol2999z00zz__evmodulez00, (int) (7L), BNIL);
		}}

	}



/* evmodule? */
	BGL_EXPORTED_DEF bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t BgL_objz00_34)
	{
		{	/* Eval/evmodule.scm 134 */
			{	/* Eval/evmodule.scm 135 */
				bool_t BgL_test3408z00_5589;

				if (STRUCTP(BgL_objz00_34))
					{	/* Eval/evmodule.scm 129 */
						obj_t BgL_tmpz00_5592;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_res2336z00_3011;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_aux2396z00_4369;

								BgL_aux2396z00_4369 = STRUCT_KEY(BgL_objz00_34);
								if (SYMBOLP(BgL_aux2396z00_4369))
									{	/* Eval/evmodule.scm 129 */
										BgL_res2336z00_3011 = BgL_aux2396z00_4369;
									}
								else
									{
										obj_t BgL_auxz00_5596;

										BgL_auxz00_5596 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(4439L),
											BGl_string3004z00zz__evmodulez00,
											BGl_string3005z00zz__evmodulez00, BgL_aux2396z00_4369);
										FAILURE(BgL_auxz00_5596, BFALSE, BFALSE);
									}
							}
							BgL_tmpz00_5592 = BgL_res2336z00_3011;
						}
						BgL_test3408z00_5589 =
							(BgL_tmpz00_5592 == BGl_symbol2999z00zz__evmodulez00);
					}
				else
					{	/* Eval/evmodule.scm 129 */
						BgL_test3408z00_5589 = ((bool_t) 0);
					}
				if (BgL_test3408z00_5589)
					{	/* Eval/evmodule.scm 135 */
						return
							(STRUCT_REF(BgL_objz00_34,
								(int) (0L)) == BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
					}
				else
					{	/* Eval/evmodule.scm 135 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &evmodule? */
	obj_t BGl_z62evmodulezf3z91zz__evmodulez00(obj_t BgL_envz00_4195,
		obj_t BgL_objz00_4196)
	{
		{	/* Eval/evmodule.scm 134 */
			return BBOOL(BGl_evmodulezf3zf3zz__evmodulez00(BgL_objz00_4196));
		}

	}



/* evmodule-name */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2namezd2zz__evmodulez00(obj_t
		BgL_modz00_35)
	{
		{	/* Eval/evmodule.scm 140 */
			{	/* Eval/evmodule.scm 141 */
				bool_t BgL_test3411z00_5606;

				{	/* Eval/evmodule.scm 135 */
					bool_t BgL_test3412z00_5607;

					if (STRUCTP(BgL_modz00_35))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_tmpz00_5610;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_res2337z00_3019;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2398z00_4371;

									BgL_aux2398z00_4371 = STRUCT_KEY(BgL_modz00_35);
									if (SYMBOLP(BgL_aux2398z00_4371))
										{	/* Eval/evmodule.scm 129 */
											BgL_res2337z00_3019 = BgL_aux2398z00_4371;
										}
									else
										{
											obj_t BgL_auxz00_5614;

											BgL_auxz00_5614 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3006z00zz__evmodulez00,
												BGl_string3005z00zz__evmodulez00, BgL_aux2398z00_4371);
											FAILURE(BgL_auxz00_5614, BFALSE, BFALSE);
										}
								}
								BgL_tmpz00_5610 = BgL_res2337z00_3019;
							}
							BgL_test3412z00_5607 =
								(BgL_tmpz00_5610 == BGl_symbol2999z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							BgL_test3412z00_5607 = ((bool_t) 0);
						}
					if (BgL_test3412z00_5607)
						{	/* Eval/evmodule.scm 135 */
							BgL_test3411z00_5606 =
								(STRUCT_REF(BgL_modz00_35,
									(int) (0L)) ==
								BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 135 */
							BgL_test3411z00_5606 = ((bool_t) 0);
						}
				}
				if (BgL_test3411z00_5606)
					{	/* Eval/evmodule.scm 129 */
						obj_t BgL_sz00_3021;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_aux2400z00_4373;

							BgL_aux2400z00_4373 = BgL_modz00_35;
							if (STRUCTP(BgL_aux2400z00_4373))
								{	/* Eval/evmodule.scm 129 */
									BgL_sz00_3021 = BgL_aux2400z00_4373;
								}
							else
								{
									obj_t BgL_auxz00_5624;

									BgL_auxz00_5624 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(4439L),
										BGl_string3006z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_aux2400z00_4373);
									FAILURE(BgL_auxz00_5624, BFALSE, BFALSE);
								}
						}
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_aux2402z00_4375;

							BgL_aux2402z00_4375 = STRUCT_REF(BgL_sz00_3021, (int) (1L));
							if (SYMBOLP(BgL_aux2402z00_4375))
								{	/* Eval/evmodule.scm 129 */
									return BgL_aux2402z00_4375;
								}
							else
								{
									obj_t BgL_auxz00_5632;

									BgL_auxz00_5632 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(4439L),
										BGl_string3006z00zz__evmodulez00,
										BGl_string3005z00zz__evmodulez00, BgL_aux2402z00_4375);
									FAILURE(BgL_auxz00_5632, BFALSE, BFALSE);
								}
						}
					}
				else
					{	/* Eval/evmodule.scm 143 */
						obj_t BgL_aux2404z00_4377;

						BgL_aux2404z00_4377 =
							BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string3006z00zz__evmodulez00,
							BGl_symbol3008z00zz__evmodulez00, BgL_modz00_35);
						if (SYMBOLP(BgL_aux2404z00_4377))
							{	/* Eval/evmodule.scm 143 */
								return BgL_aux2404z00_4377;
							}
						else
							{
								obj_t BgL_auxz00_5639;

								BgL_auxz00_5639 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(5128L),
									BGl_string3006z00zz__evmodulez00,
									BGl_string3005z00zz__evmodulez00, BgL_aux2404z00_4377);
								FAILURE(BgL_auxz00_5639, BFALSE, BFALSE);
							}
					}
			}
		}

	}



/* &evmodule-name */
	obj_t BGl_z62evmodulezd2namezb0zz__evmodulez00(obj_t BgL_envz00_4197,
		obj_t BgL_modz00_4198)
	{
		{	/* Eval/evmodule.scm 140 */
			return BGl_evmodulezd2namezd2zz__evmodulez00(BgL_modz00_4198);
		}

	}



/* evmodule-path */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2pathzd2zz__evmodulez00(obj_t
		BgL_modz00_36)
	{
		{	/* Eval/evmodule.scm 148 */
			{	/* Eval/evmodule.scm 149 */
				bool_t BgL_test3418z00_5644;

				{	/* Eval/evmodule.scm 135 */
					bool_t BgL_test3419z00_5645;

					if (STRUCTP(BgL_modz00_36))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_tmpz00_5648;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_res2338z00_3028;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2406z00_4379;

									BgL_aux2406z00_4379 = STRUCT_KEY(BgL_modz00_36);
									if (SYMBOLP(BgL_aux2406z00_4379))
										{	/* Eval/evmodule.scm 129 */
											BgL_res2338z00_3028 = BgL_aux2406z00_4379;
										}
									else
										{
											obj_t BgL_auxz00_5652;

											BgL_auxz00_5652 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3010z00zz__evmodulez00,
												BGl_string3005z00zz__evmodulez00, BgL_aux2406z00_4379);
											FAILURE(BgL_auxz00_5652, BFALSE, BFALSE);
										}
								}
								BgL_tmpz00_5648 = BgL_res2338z00_3028;
							}
							BgL_test3419z00_5645 =
								(BgL_tmpz00_5648 == BGl_symbol2999z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							BgL_test3419z00_5645 = ((bool_t) 0);
						}
					if (BgL_test3419z00_5645)
						{	/* Eval/evmodule.scm 135 */
							BgL_test3418z00_5644 =
								(STRUCT_REF(BgL_modz00_36,
									(int) (0L)) ==
								BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 135 */
							BgL_test3418z00_5644 = ((bool_t) 0);
						}
				}
				if (BgL_test3418z00_5644)
					{	/* Eval/evmodule.scm 129 */
						obj_t BgL_sz00_3030;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_aux2408z00_4381;

							BgL_aux2408z00_4381 = BgL_modz00_36;
							if (STRUCTP(BgL_aux2408z00_4381))
								{	/* Eval/evmodule.scm 129 */
									BgL_sz00_3030 = BgL_aux2408z00_4381;
								}
							else
								{
									obj_t BgL_auxz00_5662;

									BgL_auxz00_5662 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(4439L),
										BGl_string3010z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_aux2408z00_4381);
									FAILURE(BgL_auxz00_5662, BFALSE, BFALSE);
								}
						}
						return STRUCT_REF(BgL_sz00_3030, (int) (2L));
					}
				else
					{	/* Eval/evmodule.scm 149 */
						return
							BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string3010z00zz__evmodulez00,
							BGl_symbol3008z00zz__evmodulez00, BgL_modz00_36);
					}
			}
		}

	}



/* &evmodule-path */
	obj_t BGl_z62evmodulezd2pathzb0zz__evmodulez00(obj_t BgL_envz00_4199,
		obj_t BgL_modz00_4200)
	{
		{	/* Eval/evmodule.scm 148 */
			return BGl_evmodulezd2pathzd2zz__evmodulez00(BgL_modz00_4200);
		}

	}



/* evmodule-macro-table */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2macrozd2tablez00zz__evmodulez00(obj_t
		BgL_modz00_37)
	{
		{	/* Eval/evmodule.scm 156 */
			{	/* Eval/evmodule.scm 157 */
				bool_t BgL_test3423z00_5670;

				{	/* Eval/evmodule.scm 135 */
					bool_t BgL_test3424z00_5671;

					if (STRUCTP(BgL_modz00_37))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_tmpz00_5674;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_res2339z00_3037;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2410z00_4383;

									BgL_aux2410z00_4383 = STRUCT_KEY(BgL_modz00_37);
									if (SYMBOLP(BgL_aux2410z00_4383))
										{	/* Eval/evmodule.scm 129 */
											BgL_res2339z00_3037 = BgL_aux2410z00_4383;
										}
									else
										{
											obj_t BgL_auxz00_5678;

											BgL_auxz00_5678 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3011z00zz__evmodulez00,
												BGl_string3005z00zz__evmodulez00, BgL_aux2410z00_4383);
											FAILURE(BgL_auxz00_5678, BFALSE, BFALSE);
										}
								}
								BgL_tmpz00_5674 = BgL_res2339z00_3037;
							}
							BgL_test3424z00_5671 =
								(BgL_tmpz00_5674 == BGl_symbol2999z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							BgL_test3424z00_5671 = ((bool_t) 0);
						}
					if (BgL_test3424z00_5671)
						{	/* Eval/evmodule.scm 135 */
							BgL_test3423z00_5670 =
								(STRUCT_REF(BgL_modz00_37,
									(int) (0L)) ==
								BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 135 */
							BgL_test3423z00_5670 = ((bool_t) 0);
						}
				}
				if (BgL_test3423z00_5670)
					{	/* Eval/evmodule.scm 129 */
						obj_t BgL_sz00_3039;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_aux2412z00_4385;

							BgL_aux2412z00_4385 = BgL_modz00_37;
							if (STRUCTP(BgL_aux2412z00_4385))
								{	/* Eval/evmodule.scm 129 */
									BgL_sz00_3039 = BgL_aux2412z00_4385;
								}
							else
								{
									obj_t BgL_auxz00_5688;

									BgL_auxz00_5688 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(4439L),
										BGl_string3011z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_aux2412z00_4385);
									FAILURE(BgL_auxz00_5688, BFALSE, BFALSE);
								}
						}
						return STRUCT_REF(BgL_sz00_3039, (int) (5L));
					}
				else
					{	/* Eval/evmodule.scm 157 */
						return
							BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string3011z00zz__evmodulez00,
							BGl_symbol3008z00zz__evmodulez00, BgL_modz00_37);
					}
			}
		}

	}



/* &evmodule-macro-table */
	obj_t BGl_z62evmodulezd2macrozd2tablez62zz__evmodulez00(obj_t BgL_envz00_4201,
		obj_t BgL_modz00_4202)
	{
		{	/* Eval/evmodule.scm 156 */
			return BGl_evmodulezd2macrozd2tablez00zz__evmodulez00(BgL_modz00_4202);
		}

	}



/* evmodule-extension */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2extensionzd2zz__evmodulez00(obj_t
		BgL_modz00_38)
	{
		{	/* Eval/evmodule.scm 164 */
			{	/* Eval/evmodule.scm 165 */
				bool_t BgL_test3428z00_5696;

				{	/* Eval/evmodule.scm 135 */
					bool_t BgL_test3429z00_5697;

					if (STRUCTP(BgL_modz00_38))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_tmpz00_5700;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_res2340z00_3046;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2414z00_4387;

									BgL_aux2414z00_4387 = STRUCT_KEY(BgL_modz00_38);
									if (SYMBOLP(BgL_aux2414z00_4387))
										{	/* Eval/evmodule.scm 129 */
											BgL_res2340z00_3046 = BgL_aux2414z00_4387;
										}
									else
										{
											obj_t BgL_auxz00_5704;

											BgL_auxz00_5704 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3012z00zz__evmodulez00,
												BGl_string3005z00zz__evmodulez00, BgL_aux2414z00_4387);
											FAILURE(BgL_auxz00_5704, BFALSE, BFALSE);
										}
								}
								BgL_tmpz00_5700 = BgL_res2340z00_3046;
							}
							BgL_test3429z00_5697 =
								(BgL_tmpz00_5700 == BGl_symbol2999z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							BgL_test3429z00_5697 = ((bool_t) 0);
						}
					if (BgL_test3429z00_5697)
						{	/* Eval/evmodule.scm 135 */
							BgL_test3428z00_5696 =
								(STRUCT_REF(BgL_modz00_38,
									(int) (0L)) ==
								BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 135 */
							BgL_test3428z00_5696 = ((bool_t) 0);
						}
				}
				if (BgL_test3428z00_5696)
					{	/* Eval/evmodule.scm 129 */
						obj_t BgL_sz00_3048;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_aux2416z00_4389;

							BgL_aux2416z00_4389 = BgL_modz00_38;
							if (STRUCTP(BgL_aux2416z00_4389))
								{	/* Eval/evmodule.scm 129 */
									BgL_sz00_3048 = BgL_aux2416z00_4389;
								}
							else
								{
									obj_t BgL_auxz00_5714;

									BgL_auxz00_5714 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(4439L),
										BGl_string3012z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_aux2416z00_4389);
									FAILURE(BgL_auxz00_5714, BFALSE, BFALSE);
								}
						}
						return STRUCT_REF(BgL_sz00_3048, (int) (6L));
					}
				else
					{	/* Eval/evmodule.scm 165 */
						return
							BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string3012z00zz__evmodulez00,
							BGl_symbol3008z00zz__evmodulez00, BgL_modz00_38);
					}
			}
		}

	}



/* &evmodule-extension */
	obj_t BGl_z62evmodulezd2extensionzb0zz__evmodulez00(obj_t BgL_envz00_4203,
		obj_t BgL_modz00_4204)
	{
		{	/* Eval/evmodule.scm 164 */
			return BGl_evmodulezd2extensionzd2zz__evmodulez00(BgL_modz00_4204);
		}

	}



/* evmodule-extension-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_evmodulezd2extensionzd2setz12z12zz__evmodulez00(obj_t BgL_modz00_39,
		obj_t BgL_extz00_40)
	{
		{	/* Eval/evmodule.scm 172 */
			{	/* Eval/evmodule.scm 129 */
				obj_t BgL_sz00_3049;

				if (STRUCTP(BgL_modz00_39))
					{	/* Eval/evmodule.scm 129 */
						BgL_sz00_3049 = BgL_modz00_39;
					}
				else
					{
						obj_t BgL_auxz00_5724;

						BgL_auxz00_5724 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(4439L), BGl_string3013z00zz__evmodulez00,
							BGl_string3007z00zz__evmodulez00, BgL_modz00_39);
						FAILURE(BgL_auxz00_5724, BFALSE, BFALSE);
					}
				{	/* Eval/evmodule.scm 129 */
					int BgL_tmpz00_5728;

					BgL_tmpz00_5728 = (int) (6L);
					return STRUCT_SET(BgL_sz00_3049, BgL_tmpz00_5728, BgL_extz00_40);
				}
			}
		}

	}



/* &evmodule-extension-set! */
	obj_t BGl_z62evmodulezd2extensionzd2setz12z70zz__evmodulez00(obj_t
		BgL_envz00_4205, obj_t BgL_modz00_4206, obj_t BgL_extz00_4207)
	{
		{	/* Eval/evmodule.scm 172 */
			return
				BGl_evmodulezd2extensionzd2setz12z12zz__evmodulez00(BgL_modz00_4206,
				BgL_extz00_4207);
		}

	}



/* make-evmodule */
	obj_t BGl_makezd2evmodulezd2zz__evmodulez00(obj_t BgL_idz00_41,
		obj_t BgL_pathz00_42, obj_t BgL_locz00_43)
	{
		{	/* Eval/evmodule.scm 178 */
			{	/* Eval/evmodule.scm 179 */
				obj_t BgL_top3435z00_5733;

				BgL_top3435z00_5733 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2moduleszd2mutexza2zd2zz__evmodulez00);
				BGL_EXITD_PUSH_PROTECT(BgL_top3435z00_5733,
					BGl_za2moduleszd2mutexza2zd2zz__evmodulez00);
				BUNSPEC;
				{	/* Eval/evmodule.scm 179 */
					obj_t BgL_tmp3434z00_5732;

					{	/* Eval/evmodule.scm 180 */
						obj_t BgL_envz00_1331;

						{	/* Eval/evmodule.scm 180 */
							obj_t BgL_list1365z00_1356;

							{	/* Eval/evmodule.scm 180 */
								obj_t BgL_arg1366z00_1357;

								{	/* Eval/evmodule.scm 180 */
									obj_t BgL_arg1367z00_1358;

									BgL_arg1367z00_1358 =
										MAKE_YOUNG_PAIR(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00,
										BNIL);
									BgL_arg1366z00_1357 =
										MAKE_YOUNG_PAIR(BUNSPEC, BgL_arg1367z00_1358);
								}
								BgL_list1365z00_1356 =
									MAKE_YOUNG_PAIR(BINT(100L), BgL_arg1366z00_1357);
							}
							BgL_envz00_1331 =
								BGl_makezd2hashtablezd2zz__hashz00(BgL_list1365z00_1356);
						}
						{	/* Eval/evmodule.scm 180 */
							obj_t BgL_mactablez00_1332;

							{	/* Eval/evmodule.scm 181 */
								obj_t BgL_list1364z00_1355;

								BgL_list1364z00_1355 = MAKE_YOUNG_PAIR(BINT(64L), BNIL);
								BgL_mactablez00_1332 =
									BGl_makezd2hashtablezd2zz__hashz00(BgL_list1364z00_1355);
							}
							{	/* Eval/evmodule.scm 181 */
								obj_t BgL_modz00_1333;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_newz00_3050;

									BgL_newz00_3050 =
										create_struct(BGl_symbol2999z00zz__evmodulez00, (int) (7L));
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5747;

										BgL_tmpz00_5747 = (int) (6L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5747, BNIL);
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5750;

										BgL_tmpz00_5750 = (int) (5L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5750,
											BgL_mactablez00_1332);
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5753;

										BgL_tmpz00_5753 = (int) (4L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5753, BNIL);
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5756;

										BgL_tmpz00_5756 = (int) (3L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5756,
											BgL_envz00_1331);
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5759;

										BgL_tmpz00_5759 = (int) (2L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5759,
											BgL_pathz00_42);
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5762;

										BgL_tmpz00_5762 = (int) (1L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5762, BgL_idz00_41);
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_5765;

										BgL_tmpz00_5765 = (int) (0L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_5765,
											BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
									}
									BgL_modz00_1333 = BgL_newz00_3050;
								}
								{	/* Eval/evmodule.scm 182 */

									if (BGl_hashtablezf3zf3zz__hashz00
										(BGl_za2moduleszd2tableza2zd2zz__evmodulez00))
										{	/* Eval/evmodule.scm 187 */
											obj_t BgL_oldz00_1335;

											{	/* Eval/evmodule.scm 187 */
												obj_t BgL_auxz00_5770;

												{	/* Eval/evmodule.scm 187 */
													obj_t BgL_aux2420z00_4393;

													BgL_aux2420z00_4393 =
														BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
													if (STRUCTP(BgL_aux2420z00_4393))
														{	/* Eval/evmodule.scm 187 */
															BgL_auxz00_5770 = BgL_aux2420z00_4393;
														}
													else
														{
															obj_t BgL_auxz00_5773;

															BgL_auxz00_5773 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(7219L),
																BGl_string3014z00zz__evmodulez00,
																BGl_string3007z00zz__evmodulez00,
																BgL_aux2420z00_4393);
															FAILURE(BgL_auxz00_5773, BFALSE, BFALSE);
														}
												}
												BgL_oldz00_1335 =
													BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_5770,
													BgL_idz00_41);
											}
											if (CBOOL(BgL_oldz00_1335))
												{	/* Eval/evmodule.scm 188 */
													{	/* Eval/evmodule.scm 191 */
														obj_t BgL_zc3z04anonymousza31348ze3z87_4208;

														BgL_zc3z04anonymousza31348ze3z87_4208 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31348ze3ze5zz__evmodulez00,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31348ze3z87_4208,
															(int) (0L), BgL_modz00_1333);
														{	/* Eval/evmodule.scm 190 */
															obj_t BgL_auxz00_5785;

															{	/* Eval/evmodule.scm 190 */
																obj_t BgL_aux2422z00_4395;

																BgL_aux2422z00_4395 =
																	BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
																if (STRUCTP(BgL_aux2422z00_4395))
																	{	/* Eval/evmodule.scm 190 */
																		BgL_auxz00_5785 = BgL_aux2422z00_4395;
																	}
																else
																	{
																		obj_t BgL_auxz00_5788;

																		BgL_auxz00_5788 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(7292L),
																			BGl_string3014z00zz__evmodulez00,
																			BGl_string3007z00zz__evmodulez00,
																			BgL_aux2422z00_4395);
																		FAILURE(BgL_auxz00_5788, BFALSE, BFALSE);
																	}
															}
															BGl_hashtablezd2updatez12zc0zz__hashz00
																(BgL_auxz00_5785, BgL_idz00_41,
																BgL_zc3z04anonymousza31348ze3z87_4208,
																BgL_modz00_1333);
														}
													}
													{	/* Eval/evmodule.scm 192 */
														bool_t BgL_test3440z00_5793;

														{	/* Eval/evmodule.scm 192 */
															obj_t BgL_arg1362z00_1353;

															{	/* Eval/evmodule.scm 129 */
																obj_t BgL_sz00_3058;

																if (STRUCTP(BgL_oldz00_1335))
																	{	/* Eval/evmodule.scm 129 */
																		BgL_sz00_3058 = BgL_oldz00_1335;
																	}
																else
																	{
																		obj_t BgL_auxz00_5796;

																		BgL_auxz00_5796 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(4439L),
																			BGl_string3014z00zz__evmodulez00,
																			BGl_string3007z00zz__evmodulez00,
																			BgL_oldz00_1335);
																		FAILURE(BgL_auxz00_5796, BFALSE, BFALSE);
																	}
																BgL_arg1362z00_1353 =
																	STRUCT_REF(BgL_sz00_3058, (int) (2L));
															}
															{	/* Eval/evmodule.scm 192 */
																obj_t BgL_string1z00_3059;
																obj_t BgL_string2z00_3060;

																if (STRINGP(BgL_arg1362z00_1353))
																	{	/* Eval/evmodule.scm 192 */
																		BgL_string1z00_3059 = BgL_arg1362z00_1353;
																	}
																else
																	{
																		obj_t BgL_auxz00_5804;

																		BgL_auxz00_5804 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(7384L),
																			BGl_string3014z00zz__evmodulez00,
																			BGl_string3015z00zz__evmodulez00,
																			BgL_arg1362z00_1353);
																		FAILURE(BgL_auxz00_5804, BFALSE, BFALSE);
																	}
																if (STRINGP(BgL_pathz00_42))
																	{	/* Eval/evmodule.scm 192 */
																		BgL_string2z00_3060 = BgL_pathz00_42;
																	}
																else
																	{
																		obj_t BgL_auxz00_5810;

																		BgL_auxz00_5810 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(7386L),
																			BGl_string3014z00zz__evmodulez00,
																			BGl_string3015z00zz__evmodulez00,
																			BgL_pathz00_42);
																		FAILURE(BgL_auxz00_5810, BFALSE, BFALSE);
																	}
																{	/* Eval/evmodule.scm 192 */
																	long BgL_l1z00_3061;

																	BgL_l1z00_3061 =
																		STRING_LENGTH(BgL_string1z00_3059);
																	if (
																		(BgL_l1z00_3061 ==
																			STRING_LENGTH(BgL_string2z00_3060)))
																		{	/* Eval/evmodule.scm 192 */
																			int BgL_arg2156z00_3064;

																			{	/* Eval/evmodule.scm 192 */
																				char *BgL_auxz00_5820;
																				char *BgL_tmpz00_5818;

																				BgL_auxz00_5820 =
																					BSTRING_TO_STRING
																					(BgL_string2z00_3060);
																				BgL_tmpz00_5818 =
																					BSTRING_TO_STRING
																					(BgL_string1z00_3059);
																				BgL_arg2156z00_3064 =
																					memcmp(BgL_tmpz00_5818,
																					BgL_auxz00_5820, BgL_l1z00_3061);
																			}
																			BgL_test3440z00_5793 =
																				((long) (BgL_arg2156z00_3064) == 0L);
																		}
																	else
																		{	/* Eval/evmodule.scm 192 */
																			BgL_test3440z00_5793 = ((bool_t) 0);
																		}
																}
															}
														}
														if (BgL_test3440z00_5793)
															{	/* Eval/evmodule.scm 192 */
																BFALSE;
															}
														else
															{	/* Eval/evmodule.scm 193 */
																obj_t BgL_msgz00_1342;

																{	/* Eval/evmodule.scm 194 */
																	obj_t BgL_arg1352z00_1344;
																	obj_t BgL_arg1354z00_1345;

																	{	/* Eval/evmodule.scm 194 */
																		obj_t BgL_arg2162z00_3071;

																		BgL_arg2162z00_3071 =
																			SYMBOL_TO_STRING(BgL_idz00_41);
																		BgL_arg1352z00_1344 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg2162z00_3071);
																	}
																	{	/* Eval/evmodule.scm 129 */
																		obj_t BgL_sz00_3072;

																		if (STRUCTP(BgL_oldz00_1335))
																			{	/* Eval/evmodule.scm 129 */
																				BgL_sz00_3072 = BgL_oldz00_1335;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5829;

																				BgL_auxz00_5829 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(4439L),
																					BGl_string3014z00zz__evmodulez00,
																					BGl_string3007z00zz__evmodulez00,
																					BgL_oldz00_1335);
																				FAILURE(BgL_auxz00_5829, BFALSE,
																					BFALSE);
																			}
																		BgL_arg1354z00_1345 =
																			STRUCT_REF(BgL_sz00_3072, (int) (2L));
																	}
																	{	/* Eval/evmodule.scm 193 */
																		obj_t BgL_list1355z00_1346;

																		{	/* Eval/evmodule.scm 193 */
																			obj_t BgL_arg1356z00_1347;

																			{	/* Eval/evmodule.scm 193 */
																				obj_t BgL_arg1357z00_1348;

																				{	/* Eval/evmodule.scm 193 */
																					obj_t BgL_arg1358z00_1349;

																					{	/* Eval/evmodule.scm 193 */
																						obj_t BgL_arg1359z00_1350;

																						{	/* Eval/evmodule.scm 193 */
																							obj_t BgL_arg1360z00_1351;

																							{	/* Eval/evmodule.scm 193 */
																								obj_t BgL_arg1361z00_1352;

																								BgL_arg1361z00_1352 =
																									MAKE_YOUNG_PAIR
																									(BGl_string3016z00zz__evmodulez00,
																									BNIL);
																								BgL_arg1360z00_1351 =
																									MAKE_YOUNG_PAIR
																									(BgL_pathz00_42,
																									BgL_arg1361z00_1352);
																							}
																							BgL_arg1359z00_1350 =
																								MAKE_YOUNG_PAIR
																								(BGl_string3017z00zz__evmodulez00,
																								BgL_arg1360z00_1351);
																						}
																						BgL_arg1358z00_1349 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1354z00_1345,
																							BgL_arg1359z00_1350);
																					}
																					BgL_arg1357z00_1348 =
																						MAKE_YOUNG_PAIR
																						(BGl_string3018z00zz__evmodulez00,
																						BgL_arg1358z00_1349);
																				}
																				BgL_arg1356z00_1347 =
																					MAKE_YOUNG_PAIR(BgL_arg1352z00_1344,
																					BgL_arg1357z00_1348);
																			}
																			BgL_list1355z00_1346 =
																				MAKE_YOUNG_PAIR
																				(BGl_string3019z00zz__evmodulez00,
																				BgL_arg1356z00_1347);
																		}
																		BgL_msgz00_1342 =
																			BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																			(BgL_list1355z00_1346);
																}}
																{	/* Eval/evmodule.scm 199 */
																	obj_t BgL_list1351z00_1343;

																	BgL_list1351z00_1343 =
																		MAKE_YOUNG_PAIR(BgL_msgz00_1342, BNIL);
																	BGl_warningzf2loczf2zz__errorz00
																		(BgL_locz00_43, BgL_list1351z00_1343);
												}}}}
											else
												{	/* Eval/evmodule.scm 200 */
													obj_t BgL_auxz00_5845;

													{	/* Eval/evmodule.scm 200 */
														obj_t BgL_aux2432z00_4405;

														BgL_aux2432z00_4405 =
															BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
														if (STRUCTP(BgL_aux2432z00_4405))
															{	/* Eval/evmodule.scm 200 */
																BgL_auxz00_5845 = BgL_aux2432z00_4405;
															}
														else
															{
																obj_t BgL_auxz00_5848;

																BgL_auxz00_5848 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(7621L), BGl_string3014z00zz__evmodulez00,
																	BGl_string3007z00zz__evmodulez00,
																	BgL_aux2432z00_4405);
																FAILURE(BgL_auxz00_5848, BFALSE, BFALSE);
															}
													}
													BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_5845,
														BgL_idz00_41, BgL_modz00_1333);
												}
										}
									else
										{	/* Eval/evmodule.scm 183 */
											{	/* Eval/evmodule.scm 185 */
												obj_t BgL_list1363z00_1354;

												BgL_list1363z00_1354 =
													MAKE_YOUNG_PAIR(BINT(256L), BNIL);
												BGl_za2moduleszd2tableza2zd2zz__evmodulez00 =
													BGl_makezd2hashtablezd2zz__hashz00
													(BgL_list1363z00_1354);
											}
											{	/* Eval/evmodule.scm 186 */
												obj_t BgL_auxz00_5856;

												{	/* Eval/evmodule.scm 186 */
													obj_t BgL_aux2434z00_4407;

													BgL_aux2434z00_4407 =
														BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
													if (STRUCTP(BgL_aux2434z00_4407))
														{	/* Eval/evmodule.scm 186 */
															BgL_auxz00_5856 = BgL_aux2434z00_4407;
														}
													else
														{
															obj_t BgL_auxz00_5859;

															BgL_auxz00_5859 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(7162L),
																BGl_string3014z00zz__evmodulez00,
																BGl_string3007z00zz__evmodulez00,
																BgL_aux2434z00_4407);
															FAILURE(BgL_auxz00_5859, BFALSE, BFALSE);
														}
												}
												BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_5856,
													BgL_idz00_41, BgL_modz00_1333);
											}
										}
									BgL_tmp3434z00_5732 = BgL_modz00_1333;
								}
							}
						}
					}
					BGL_EXITD_POP_PROTECT(BgL_top3435z00_5733);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2moduleszd2mutexza2zd2zz__evmodulez00);
					return BgL_tmp3434z00_5732;
				}
			}
		}

	}



/* &<@anonymous:1348> */
	obj_t BGl_z62zc3z04anonymousza31348ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4209, obj_t BgL_vz00_4211)
	{
		{	/* Eval/evmodule.scm 191 */
			return ((obj_t) PROCEDURE_REF(BgL_envz00_4209, (int) (0L)));
		}

	}



/* eval-module */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2modulezd2zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 206 */
			return BGL_MODULE();
		}

	}



/* &eval-module */
	obj_t BGl_z62evalzd2modulezb0zz__evmodulez00(obj_t BgL_envz00_4215)
	{
		{	/* Eval/evmodule.scm 206 */
			return BGl_evalzd2modulezd2zz__evmodulez00();
		}

	}



/* eval-module-set! */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2modulezd2setz12z12zz__evmodulez00(obj_t
		BgL_modz00_44)
	{
		{	/* Eval/evmodule.scm 212 */
			{	/* Eval/evmodule.scm 213 */
				bool_t BgL_test3448z00_5871;

				{	/* Eval/evmodule.scm 213 */
					bool_t BgL_test3449z00_5872;

					{	/* Eval/evmodule.scm 135 */
						bool_t BgL_test3450z00_5873;

						if (STRUCTP(BgL_modz00_44))
							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_tmpz00_5876;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_res2341z00_3078;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux2436z00_4409;

										BgL_aux2436z00_4409 = STRUCT_KEY(BgL_modz00_44);
										if (SYMBOLP(BgL_aux2436z00_4409))
											{	/* Eval/evmodule.scm 129 */
												BgL_res2341z00_3078 = BgL_aux2436z00_4409;
											}
										else
											{
												obj_t BgL_auxz00_5880;

												BgL_auxz00_5880 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(4439L),
													BGl_string3020z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00,
													BgL_aux2436z00_4409);
												FAILURE(BgL_auxz00_5880, BFALSE, BFALSE);
											}
									}
									BgL_tmpz00_5876 = BgL_res2341z00_3078;
								}
								BgL_test3450z00_5873 =
									(BgL_tmpz00_5876 == BGl_symbol2999z00zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 129 */
								BgL_test3450z00_5873 = ((bool_t) 0);
							}
						if (BgL_test3450z00_5873)
							{	/* Eval/evmodule.scm 135 */
								BgL_test3449z00_5872 =
									(STRUCT_REF(BgL_modz00_44,
										(int) (0L)) ==
									BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 135 */
								BgL_test3449z00_5872 = ((bool_t) 0);
							}
					}
					if (BgL_test3449z00_5872)
						{	/* Eval/evmodule.scm 213 */
							BgL_test3448z00_5871 = ((bool_t) 1);
						}
					else
						{	/* Eval/evmodule.scm 213 */
							if (
								(BgL_modz00_44 ==
									BGl_interactionzd2environmentzd2zz__evalz00()))
								{	/* Eval/evmodule.scm 214 */
									BgL_test3448z00_5871 = ((bool_t) 1);
								}
							else
								{	/* Eval/evmodule.scm 214 */
									BgL_test3448z00_5871 = (BgL_modz00_44 == BUNSPEC);
								}
						}
				}
				if (BgL_test3448z00_5871)
					{	/* Eval/evmodule.scm 213 */
						return BGL_MODULE_SET(BgL_modz00_44);
					}
				else
					{	/* Eval/evmodule.scm 213 */
						return
							BGl_errorz00zz__errorz00(BGl_string3020z00zz__evmodulez00,
							BGl_string3021z00zz__evmodulez00, BgL_modz00_44);
					}
			}
		}

	}



/* &eval-module-set! */
	obj_t BGl_z62evalzd2modulezd2setz12z70zz__evmodulez00(obj_t BgL_envz00_4216,
		obj_t BgL_modz00_4217)
	{
		{	/* Eval/evmodule.scm 212 */
			return BGl_evalzd2modulezd2setz12z12zz__evmodulez00(BgL_modz00_4217);
		}

	}



/* eval-find-module */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2findzd2modulez00zz__evmodulez00(obj_t
		BgL_idz00_45)
	{
		{	/* Eval/evmodule.scm 222 */
			if (BGl_hashtablezf3zf3zz__hashz00
				(BGl_za2moduleszd2tableza2zd2zz__evmodulez00))
				{	/* Eval/evmodule.scm 224 */
					obj_t BgL_auxz00_5897;

					{	/* Eval/evmodule.scm 224 */
						obj_t BgL_aux2438z00_4411;

						BgL_aux2438z00_4411 = BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
						if (STRUCTP(BgL_aux2438z00_4411))
							{	/* Eval/evmodule.scm 224 */
								BgL_auxz00_5897 = BgL_aux2438z00_4411;
							}
						else
							{
								obj_t BgL_auxz00_5900;

								BgL_auxz00_5900 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(8670L),
									BGl_string3022z00zz__evmodulez00,
									BGl_string3007z00zz__evmodulez00, BgL_aux2438z00_4411);
								FAILURE(BgL_auxz00_5900, BFALSE, BFALSE);
							}
					}
					return
						BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_5897, BgL_idz00_45);
				}
			else
				{	/* Eval/evmodule.scm 223 */
					return BFALSE;
				}
		}

	}



/* &eval-find-module */
	obj_t BGl_z62evalzd2findzd2modulez62zz__evmodulez00(obj_t BgL_envz00_4218,
		obj_t BgL_idz00_4219)
	{
		{	/* Eval/evmodule.scm 222 */
			{	/* Eval/evmodule.scm 223 */
				obj_t BgL_auxz00_5905;

				if (SYMBOLP(BgL_idz00_4219))
					{	/* Eval/evmodule.scm 223 */
						BgL_auxz00_5905 = BgL_idz00_4219;
					}
				else
					{
						obj_t BgL_auxz00_5908;

						BgL_auxz00_5908 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(8614L), BGl_string3023z00zz__evmodulez00,
							BGl_string3005z00zz__evmodulez00, BgL_idz00_4219);
						FAILURE(BgL_auxz00_5908, BFALSE, BFALSE);
					}
				return BGl_evalzd2findzd2modulez00zz__evmodulez00(BgL_auxz00_5905);
			}
		}

	}



/* evalias-module */
	obj_t BGl_evaliaszd2modulezd2zz__evmodulez00(obj_t BgL_vz00_47)
	{
		{	/* Eval/evmodule.scm 235 */
			{	/* Eval/evmodule.scm 237 */
				bool_t BgL_test3457z00_5913;

				{	/* Eval/evmodule.scm 237 */
					obj_t BgL_arg1383z00_1377;

					{	/* Eval/evmodule.scm 237 */
						obj_t BgL_vectorz00_3091;

						if (VECTORP(BgL_vz00_47))
							{	/* Eval/evmodule.scm 237 */
								BgL_vectorz00_3091 = BgL_vz00_47;
							}
						else
							{
								obj_t BgL_auxz00_5916;

								BgL_auxz00_5916 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(9268L),
									BGl_string3024z00zz__evmodulez00,
									BGl_string3025z00zz__evmodulez00, BgL_vz00_47);
								FAILURE(BgL_auxz00_5916, BFALSE, BFALSE);
							}
						BgL_arg1383z00_1377 = VECTOR_REF(BgL_vectorz00_3091, 3L);
					}
					{	/* Eval/evmodule.scm 135 */
						bool_t BgL_test3459z00_5921;

						if (STRUCTP(BgL_arg1383z00_1377))
							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_tmpz00_5924;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_res2343z00_3097;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux2444z00_4417;

										BgL_aux2444z00_4417 = STRUCT_KEY(BgL_arg1383z00_1377);
										if (SYMBOLP(BgL_aux2444z00_4417))
											{	/* Eval/evmodule.scm 129 */
												BgL_res2343z00_3097 = BgL_aux2444z00_4417;
											}
										else
											{
												obj_t BgL_auxz00_5928;

												BgL_auxz00_5928 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(4439L),
													BGl_string3024z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00,
													BgL_aux2444z00_4417);
												FAILURE(BgL_auxz00_5928, BFALSE, BFALSE);
											}
									}
									BgL_tmpz00_5924 = BgL_res2343z00_3097;
								}
								BgL_test3459z00_5921 =
									(BgL_tmpz00_5924 == BGl_symbol2999z00zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 129 */
								BgL_test3459z00_5921 = ((bool_t) 0);
							}
						if (BgL_test3459z00_5921)
							{	/* Eval/evmodule.scm 135 */
								BgL_test3457z00_5913 =
									(STRUCT_REF(BgL_arg1383z00_1377,
										(int) (0L)) ==
									BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 135 */
								BgL_test3457z00_5913 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test3457z00_5913)
					{	/* Eval/evmodule.scm 238 */
						obj_t BgL_vectorz00_3099;

						if (VECTORP(BgL_vz00_47))
							{	/* Eval/evmodule.scm 238 */
								BgL_vectorz00_3099 = BgL_vz00_47;
							}
						else
							{
								obj_t BgL_auxz00_5938;

								BgL_auxz00_5938 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(9299L),
									BGl_string3024z00zz__evmodulez00,
									BGl_string3025z00zz__evmodulez00, BgL_vz00_47);
								FAILURE(BgL_auxz00_5938, BFALSE, BFALSE);
							}
						return VECTOR_REF(BgL_vectorz00_3099, 3L);
					}
				else
					{	/* Eval/evmodule.scm 239 */
						bool_t BgL_test3463z00_5943;

						{	/* Eval/evmodule.scm 239 */
							obj_t BgL_arg1382z00_1376;

							{	/* Eval/evmodule.scm 239 */
								obj_t BgL_vectorz00_3100;

								if (VECTORP(BgL_vz00_47))
									{	/* Eval/evmodule.scm 239 */
										BgL_vectorz00_3100 = BgL_vz00_47;
									}
								else
									{
										obj_t BgL_auxz00_5946;

										BgL_auxz00_5946 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(9339L),
											BGl_string3024z00zz__evmodulez00,
											BGl_string3025z00zz__evmodulez00, BgL_vz00_47);
										FAILURE(BgL_auxz00_5946, BFALSE, BFALSE);
									}
								BgL_arg1382z00_1376 = VECTOR_REF(BgL_vectorz00_3100, 3L);
							}
							BgL_test3463z00_5943 = SYMBOLP(BgL_arg1382z00_1376);
						}
						if (BgL_test3463z00_5943)
							{	/* Eval/evmodule.scm 240 */
								obj_t BgL_mz00_1374;

								{	/* Eval/evmodule.scm 240 */
									obj_t BgL_arg1380z00_1375;

									{	/* Eval/evmodule.scm 240 */
										obj_t BgL_vectorz00_3101;

										if (VECTORP(BgL_vz00_47))
											{	/* Eval/evmodule.scm 240 */
												BgL_vectorz00_3101 = BgL_vz00_47;
											}
										else
											{
												obj_t BgL_auxz00_5954;

												BgL_auxz00_5954 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(9397L),
													BGl_string3024z00zz__evmodulez00,
													BGl_string3025z00zz__evmodulez00, BgL_vz00_47);
												FAILURE(BgL_auxz00_5954, BFALSE, BFALSE);
											}
										BgL_arg1380z00_1375 = VECTOR_REF(BgL_vectorz00_3101, 3L);
									}
									{	/* Eval/evmodule.scm 240 */
										obj_t BgL_idz00_3102;

										if (SYMBOLP(BgL_arg1380z00_1375))
											{	/* Eval/evmodule.scm 240 */
												BgL_idz00_3102 = BgL_arg1380z00_1375;
											}
										else
											{
												obj_t BgL_auxz00_5961;

												BgL_auxz00_5961 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(9418L),
													BGl_string3024z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00,
													BgL_arg1380z00_1375);
												FAILURE(BgL_auxz00_5961, BFALSE, BFALSE);
											}
										if (BGl_hashtablezf3zf3zz__hashz00
											(BGl_za2moduleszd2tableza2zd2zz__evmodulez00))
											{	/* Eval/evmodule.scm 224 */
												obj_t BgL_auxz00_5967;

												{	/* Eval/evmodule.scm 224 */
													obj_t BgL_aux2454z00_4427;

													BgL_aux2454z00_4427 =
														BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
													if (STRUCTP(BgL_aux2454z00_4427))
														{	/* Eval/evmodule.scm 224 */
															BgL_auxz00_5967 = BgL_aux2454z00_4427;
														}
													else
														{
															obj_t BgL_auxz00_5970;

															BgL_auxz00_5970 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(8670L),
																BGl_string3024z00zz__evmodulez00,
																BGl_string3007z00zz__evmodulez00,
																BgL_aux2454z00_4427);
															FAILURE(BgL_auxz00_5970, BFALSE, BFALSE);
														}
												}
												BgL_mz00_1374 =
													BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_5967,
													BgL_idz00_3102);
											}
										else
											{	/* Eval/evmodule.scm 223 */
												BgL_mz00_1374 = BFALSE;
											}
									}
								}
								{	/* Eval/evmodule.scm 241 */
									obj_t BgL_evalzd2globalzd2_3104;

									if (VECTORP(BgL_vz00_47))
										{	/* Eval/evmodule.scm 241 */
											BgL_evalzd2globalzd2_3104 = BgL_vz00_47;
										}
									else
										{
											obj_t BgL_auxz00_5977;

											BgL_auxz00_5977 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(9451L),
												BGl_string3024z00zz__evmodulez00,
												BGl_string3025z00zz__evmodulez00, BgL_vz00_47);
											FAILURE(BgL_auxz00_5977, BFALSE, BFALSE);
										}
									VECTOR_SET(BgL_evalzd2globalzd2_3104, 3L, BgL_mz00_1374);
								}
								return BgL_mz00_1374;
							}
						else
							{	/* Eval/evmodule.scm 239 */
								return BFALSE;
							}
					}
			}
		}

	}



/* evmodule-find-global */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2findzd2globalz00zz__evmodulez00(obj_t
		BgL_modz00_48, obj_t BgL_idz00_49)
	{
		{	/* Eval/evmodule.scm 249 */
		BGl_evmodulezd2findzd2globalz00zz__evmodulez00:
			{	/* Eval/evmodule.scm 250 */
				bool_t BgL_test3470z00_5982;

				{	/* Eval/evmodule.scm 135 */
					bool_t BgL_test3471z00_5983;

					if (STRUCTP(BgL_modz00_48))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_tmpz00_5986;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_res2344z00_3111;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2458z00_4431;

									BgL_aux2458z00_4431 = STRUCT_KEY(BgL_modz00_48);
									if (SYMBOLP(BgL_aux2458z00_4431))
										{	/* Eval/evmodule.scm 129 */
											BgL_res2344z00_3111 = BgL_aux2458z00_4431;
										}
									else
										{
											obj_t BgL_auxz00_5990;

											BgL_auxz00_5990 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3026z00zz__evmodulez00,
												BGl_string3005z00zz__evmodulez00, BgL_aux2458z00_4431);
											FAILURE(BgL_auxz00_5990, BFALSE, BFALSE);
										}
								}
								BgL_tmpz00_5986 = BgL_res2344z00_3111;
							}
							BgL_test3471z00_5983 =
								(BgL_tmpz00_5986 == BGl_symbol2999z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							BgL_test3471z00_5983 = ((bool_t) 0);
						}
					if (BgL_test3471z00_5983)
						{	/* Eval/evmodule.scm 135 */
							BgL_test3470z00_5982 =
								(STRUCT_REF(BgL_modz00_48,
									(int) (0L)) ==
								BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 135 */
							BgL_test3470z00_5982 = ((bool_t) 0);
						}
				}
				if (BgL_test3470z00_5982)
					{	/* Eval/evmodule.scm 251 */
						obj_t BgL_vz00_1379;

						{	/* Eval/evmodule.scm 251 */
							obj_t BgL_auxz00_5998;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_sz00_3113;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2460z00_4433;

									BgL_aux2460z00_4433 = BgL_modz00_48;
									if (STRUCTP(BgL_aux2460z00_4433))
										{	/* Eval/evmodule.scm 129 */
											BgL_sz00_3113 = BgL_aux2460z00_4433;
										}
									else
										{
											obj_t BgL_auxz00_6001;

											BgL_auxz00_6001 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3026z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_aux2460z00_4433);
											FAILURE(BgL_auxz00_6001, BFALSE, BFALSE);
										}
								}
								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2462z00_4435;

									BgL_aux2462z00_4435 = STRUCT_REF(BgL_sz00_3113, (int) (3L));
									if (STRUCTP(BgL_aux2462z00_4435))
										{	/* Eval/evmodule.scm 129 */
											BgL_auxz00_5998 = BgL_aux2462z00_4435;
										}
									else
										{
											obj_t BgL_auxz00_6009;

											BgL_auxz00_6009 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3026z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_aux2462z00_4435);
											FAILURE(BgL_auxz00_6009, BFALSE, BFALSE);
										}
								}
							}
							BgL_vz00_1379 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_5998,
								BgL_idz00_49);
						}
						{	/* Eval/evmodule.scm 252 */
							bool_t BgL_test3476z00_6014;

							{	/* Eval/evmodule.scm 230 */
								bool_t BgL_test3477z00_6015;

								if (VECTORP(BgL_vz00_1379))
									{	/* Eval/evmodule.scm 230 */
										BgL_test3477z00_6015 = (VECTOR_LENGTH(BgL_vz00_1379) == 5L);
									}
								else
									{	/* Eval/evmodule.scm 230 */
										BgL_test3477z00_6015 = ((bool_t) 0);
									}
								if (BgL_test3477z00_6015)
									{	/* Eval/evmodule.scm 230 */
										int BgL_arg1375z00_3115;

										{	/* Eval/evmodule.scm 230 */
											int BgL_res2345z00_3122;

											{	/* Eval/evmodule.scm 230 */
												obj_t BgL_tmpz00_6020;

												{	/* Eval/evmodule.scm 230 */
													obj_t BgL_aux2464z00_4437;

													BgL_aux2464z00_4437 = VECTOR_REF(BgL_vz00_1379, 0L);
													if (INTEGERP(BgL_aux2464z00_4437))
														{	/* Eval/evmodule.scm 230 */
															BgL_tmpz00_6020 = BgL_aux2464z00_4437;
														}
													else
														{
															obj_t BgL_auxz00_6024;

															BgL_auxz00_6024 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(8966L),
																BGl_string3026z00zz__evmodulez00,
																BGl_string3027z00zz__evmodulez00,
																BgL_aux2464z00_4437);
															FAILURE(BgL_auxz00_6024, BFALSE, BFALSE);
														}
												}
												BgL_res2345z00_3122 = CINT(BgL_tmpz00_6020);
											}
											BgL_arg1375z00_3115 = BgL_res2345z00_3122;
										}
										BgL_test3476z00_6014 = ((long) (BgL_arg1375z00_3115) == 6L);
									}
								else
									{	/* Eval/evmodule.scm 230 */
										BgL_test3476z00_6014 = ((bool_t) 0);
									}
							}
							if (BgL_test3476z00_6014)
								{	/* Eval/evmodule.scm 253 */
									obj_t BgL_arg1387z00_1381;
									obj_t BgL_arg1388z00_1382;

									BgL_arg1387z00_1381 =
										BGl_evaliaszd2modulezd2zz__evmodulez00(BgL_vz00_1379);
									{	/* Eval/evmodule.scm 253 */
										obj_t BgL_evalzd2globalzd2_3124;

										{	/* Eval/evmodule.scm 253 */
											obj_t BgL_aux2465z00_4438;

											BgL_aux2465z00_4438 = BgL_vz00_1379;
											if (VECTORP(BgL_aux2465z00_4438))
												{	/* Eval/evmodule.scm 253 */
													BgL_evalzd2globalzd2_3124 = BgL_aux2465z00_4438;
												}
											else
												{
													obj_t BgL_auxz00_6034;

													BgL_auxz00_6034 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(9916L),
														BGl_string3026z00zz__evmodulez00,
														BGl_string3025z00zz__evmodulez00,
														BgL_aux2465z00_4438);
													FAILURE(BgL_auxz00_6034, BFALSE, BFALSE);
												}
										}
										BgL_arg1388z00_1382 =
											VECTOR_REF(BgL_evalzd2globalzd2_3124, 2L);
									}
									{
										obj_t BgL_idz00_6040;
										obj_t BgL_modz00_6039;

										BgL_modz00_6039 = BgL_arg1387z00_1381;
										if (SYMBOLP(BgL_arg1388z00_1382))
											{	/* Eval/evmodule.scm 253 */
												BgL_idz00_6040 = BgL_arg1388z00_1382;
											}
										else
											{
												obj_t BgL_auxz00_6043;

												BgL_auxz00_6043 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(9917L),
													BGl_string3026z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00,
													BgL_arg1388z00_1382);
												FAILURE(BgL_auxz00_6043, BFALSE, BFALSE);
											}
										BgL_idz00_49 = BgL_idz00_6040;
										BgL_modz00_48 = BgL_modz00_6039;
										goto BGl_evmodulezd2findzd2globalz00zz__evmodulez00;
									}
								}
							else
								{	/* Eval/evmodule.scm 252 */
									if (CBOOL(BgL_vz00_1379))
										{	/* Eval/evmodule.scm 254 */
											return BgL_vz00_1379;
										}
									else
										{	/* Eval/evmodule.scm 254 */
											return BGl_evalzd2lookupzd2zz__evenvz00(BgL_idz00_49);
										}
								}
						}
					}
				else
					{	/* Eval/evmodule.scm 250 */
						return BGl_evalzd2lookupzd2zz__evenvz00(BgL_idz00_49);
					}
			}
		}

	}



/* &evmodule-find-global */
	obj_t BGl_z62evmodulezd2findzd2globalz62zz__evmodulez00(obj_t BgL_envz00_4220,
		obj_t BgL_modz00_4221, obj_t BgL_idz00_4222)
	{
		{	/* Eval/evmodule.scm 249 */
			{	/* Eval/evmodule.scm 250 */
				obj_t BgL_auxz00_6051;

				if (SYMBOLP(BgL_idz00_4222))
					{	/* Eval/evmodule.scm 250 */
						BgL_auxz00_6051 = BgL_idz00_4222;
					}
				else
					{
						obj_t BgL_auxz00_6054;

						BgL_auxz00_6054 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(9752L), BGl_string3028z00zz__evmodulez00,
							BGl_string3005z00zz__evmodulez00, BgL_idz00_4222);
						FAILURE(BgL_auxz00_6054, BFALSE, BFALSE);
					}
				return
					BGl_evmodulezd2findzd2globalz00zz__evmodulez00(BgL_modz00_4221,
					BgL_auxz00_6051);
			}
		}

	}



/* evmodule-bind-global! */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00(obj_t
		BgL_modz00_50, obj_t BgL_idz00_51, obj_t BgL_varz00_52, obj_t BgL_locz00_53)
	{
		{	/* Eval/evmodule.scm 260 */
			if (CBOOL(BGl_getzd2evalzd2expanderz00zz__macroz00(BgL_idz00_51)))
				{	/* Eval/evmodule.scm 262 */
					obj_t BgL_msgz00_3127;

					{	/* Eval/evmodule.scm 262 */
						obj_t BgL_arg1392z00_3128;

						{	/* Eval/evmodule.scm 262 */
							obj_t BgL_arg2162z00_3133;

							BgL_arg2162z00_3133 = SYMBOL_TO_STRING(BgL_idz00_51);
							BgL_arg1392z00_3128 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg2162z00_3133);
						}
						BgL_msgz00_3127 =
							string_append_3(BGl_string3029z00zz__evmodulez00,
							BgL_arg1392z00_3128, BGl_string3030z00zz__evmodulez00);
					}
					{	/* Eval/evmodule.scm 264 */
						obj_t BgL_list1391z00_3129;

						BgL_list1391z00_3129 = MAKE_YOUNG_PAIR(BgL_msgz00_3127, BNIL);
						BGl_evwarningz00zz__everrorz00(BgL_locz00_53, BgL_list1391z00_3129);
					}
				}
			else
				{	/* Eval/evmodule.scm 261 */
					BFALSE;
				}
			{	/* Eval/evmodule.scm 265 */
				bool_t BgL_test3485z00_6067;

				{	/* Eval/evmodule.scm 135 */
					bool_t BgL_test3486z00_6068;

					if (STRUCTP(BgL_modz00_50))
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_tmpz00_6071;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_res2346z00_3139;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_aux2471z00_4444;

									BgL_aux2471z00_4444 = STRUCT_KEY(BgL_modz00_50);
									if (SYMBOLP(BgL_aux2471z00_4444))
										{	/* Eval/evmodule.scm 129 */
											BgL_res2346z00_3139 = BgL_aux2471z00_4444;
										}
									else
										{
											obj_t BgL_auxz00_6075;

											BgL_auxz00_6075 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3031z00zz__evmodulez00,
												BGl_string3005z00zz__evmodulez00, BgL_aux2471z00_4444);
											FAILURE(BgL_auxz00_6075, BFALSE, BFALSE);
										}
								}
								BgL_tmpz00_6071 = BgL_res2346z00_3139;
							}
							BgL_test3486z00_6068 =
								(BgL_tmpz00_6071 == BGl_symbol2999z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 129 */
							BgL_test3486z00_6068 = ((bool_t) 0);
						}
					if (BgL_test3486z00_6068)
						{	/* Eval/evmodule.scm 135 */
							BgL_test3485z00_6067 =
								(STRUCT_REF(BgL_modz00_50,
									(int) (0L)) ==
								BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 135 */
							BgL_test3485z00_6067 = ((bool_t) 0);
						}
				}
				if (BgL_test3485z00_6067)
					{	/* Eval/evmodule.scm 266 */
						obj_t BgL_arg1394z00_3131;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_sz00_3141;

							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_aux2473z00_4446;

								BgL_aux2473z00_4446 = BgL_modz00_50;
								if (STRUCTP(BgL_aux2473z00_4446))
									{	/* Eval/evmodule.scm 129 */
										BgL_sz00_3141 = BgL_aux2473z00_4446;
									}
								else
									{
										obj_t BgL_auxz00_6085;

										BgL_auxz00_6085 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(4439L),
											BGl_string3031z00zz__evmodulez00,
											BGl_string3007z00zz__evmodulez00, BgL_aux2473z00_4446);
										FAILURE(BgL_auxz00_6085, BFALSE, BFALSE);
									}
							}
							BgL_arg1394z00_3131 = STRUCT_REF(BgL_sz00_3141, (int) (3L));
						}
						{	/* Eval/evmodule.scm 266 */
							obj_t BgL_auxz00_6091;

							if (STRUCTP(BgL_arg1394z00_3131))
								{	/* Eval/evmodule.scm 266 */
									BgL_auxz00_6091 = BgL_arg1394z00_3131;
								}
							else
								{
									obj_t BgL_auxz00_6094;

									BgL_auxz00_6094 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(10468L),
										BGl_string3031z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_arg1394z00_3131);
									FAILURE(BgL_auxz00_6094, BFALSE, BFALSE);
								}
							return
								BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_6091,
								BgL_idz00_51, BgL_varz00_52);
						}
					}
				else
					{	/* Eval/evmodule.scm 267 */
						obj_t BgL_auxz00_6099;

						if (VECTORP(BgL_varz00_52))
							{	/* Eval/evmodule.scm 267 */
								BgL_auxz00_6099 = BgL_varz00_52;
							}
						else
							{
								obj_t BgL_auxz00_6102;

								BgL_auxz00_6102 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(10507L),
									BGl_string3031z00zz__evmodulez00,
									BGl_string3025z00zz__evmodulez00, BgL_varz00_52);
								FAILURE(BgL_auxz00_6102, BFALSE, BFALSE);
							}
						return
							BGl_bindzd2evalzd2globalz12z12zz__evenvz00(BgL_idz00_51,
							BgL_auxz00_6099);
					}
			}
		}

	}



/* &evmodule-bind-global! */
	obj_t BGl_z62evmodulezd2bindzd2globalz12z70zz__evmodulez00(obj_t
		BgL_envz00_4223, obj_t BgL_modz00_4224, obj_t BgL_idz00_4225,
		obj_t BgL_varz00_4226, obj_t BgL_locz00_4227)
	{
		{	/* Eval/evmodule.scm 260 */
			{	/* Eval/evmodule.scm 264 */
				obj_t BgL_auxz00_6107;

				if (SYMBOLP(BgL_idz00_4225))
					{	/* Eval/evmodule.scm 264 */
						BgL_auxz00_6107 = BgL_idz00_4225;
					}
				else
					{
						obj_t BgL_auxz00_6110;

						BgL_auxz00_6110 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(10402L), BGl_string3032z00zz__evmodulez00,
							BGl_string3005z00zz__evmodulez00, BgL_idz00_4225);
						FAILURE(BgL_auxz00_6110, BFALSE, BFALSE);
					}
				return
					BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00(BgL_modz00_4224,
					BgL_auxz00_6107, BgL_varz00_4226, BgL_locz00_4227);
			}
		}

	}



/* for-each/loc */
	bool_t BGl_forzd2eachzf2locz20zz__evmodulez00(obj_t BgL_locz00_54,
		obj_t BgL_procz00_55, obj_t BgL_lz00_56)
	{
		{	/* Eval/evmodule.scm 272 */
			{	/* Eval/evmodule.scm 273 */
				obj_t BgL_g1043z00_1391;

				{	/* Eval/evmodule.scm 274 */
					obj_t BgL__ortest_1042z00_1403;

					BgL__ortest_1042z00_1403 =
						BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_lz00_56);
					if (CBOOL(BgL__ortest_1042z00_1403))
						{	/* Eval/evmodule.scm 274 */
							BgL_g1043z00_1391 = BgL__ortest_1042z00_1403;
						}
					else
						{	/* Eval/evmodule.scm 274 */
							BgL_g1043z00_1391 = BgL_locz00_54;
						}
				}
				{
					obj_t BgL_lz00_1393;
					obj_t BgL_locz00_1394;

					BgL_lz00_1393 = BgL_lz00_56;
					BgL_locz00_1394 = BgL_g1043z00_1391;
				BgL_zc3z04anonymousza31395ze3z87_1395:
					if (PAIRP(BgL_lz00_1393))
						{	/* Eval/evmodule.scm 275 */
							{	/* Eval/evmodule.scm 276 */
								obj_t BgL_arg1397z00_1397;

								BgL_arg1397z00_1397 = CAR(BgL_lz00_1393);
								((obj_t(*)(obj_t, obj_t,
											obj_t))
									PROCEDURE_L_ENTRY(BgL_procz00_55)) (BgL_procz00_55,
									BgL_locz00_1394, BgL_arg1397z00_1397);
							}
							{	/* Eval/evmodule.scm 277 */
								obj_t BgL_arg1399z00_1398;
								obj_t BgL_arg1400z00_1399;

								BgL_arg1399z00_1398 = CDR(BgL_lz00_1393);
								{	/* Eval/evmodule.scm 277 */
									obj_t BgL__ortest_1044z00_1400;

									BgL__ortest_1044z00_1400 =
										BGl_getzd2sourcezd2locationz00zz__readerz00(CDR
										(BgL_lz00_1393));
									if (CBOOL(BgL__ortest_1044z00_1400))
										{	/* Eval/evmodule.scm 277 */
											BgL_arg1400z00_1399 = BgL__ortest_1044z00_1400;
										}
									else
										{	/* Eval/evmodule.scm 277 */
											BgL_arg1400z00_1399 = BgL_locz00_1394;
										}
								}
								{
									obj_t BgL_locz00_6132;
									obj_t BgL_lz00_6131;

									BgL_lz00_6131 = BgL_arg1399z00_1398;
									BgL_locz00_6132 = BgL_arg1400z00_1399;
									BgL_locz00_1394 = BgL_locz00_6132;
									BgL_lz00_1393 = BgL_lz00_6131;
									goto BgL_zc3z04anonymousza31395ze3z87_1395;
								}
							}
						}
					else
						{	/* Eval/evmodule.scm 275 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* evmodule-library */
	obj_t BGl_evmodulezd2libraryzd2zz__evmodulez00(obj_t BgL_clausez00_57,
		obj_t BgL_locz00_58)
	{
		{	/* Eval/evmodule.scm 282 */
			{	/* Eval/evmodule.scm 283 */
				bool_t BgL_test3496z00_6133;

				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausez00_57))
					{
						obj_t BgL_l1148z00_1431;

						{	/* Eval/evmodule.scm 283 */
							obj_t BgL_tmpz00_6136;

							BgL_l1148z00_1431 = BgL_clausez00_57;
						BgL_zc3z04anonymousza31417ze3z87_1432:
							if (NULLP(BgL_l1148z00_1431))
								{	/* Eval/evmodule.scm 283 */
									BgL_tmpz00_6136 = BTRUE;
								}
							else
								{	/* Eval/evmodule.scm 283 */
									if (PAIRP(BgL_l1148z00_1431))
										{	/* Eval/evmodule.scm 283 */
											bool_t BgL_test3500z00_6141;

											{	/* Eval/evmodule.scm 283 */
												obj_t BgL_tmpz00_6142;

												BgL_tmpz00_6142 = CAR(BgL_l1148z00_1431);
												BgL_test3500z00_6141 = SYMBOLP(BgL_tmpz00_6142);
											}
											if (BgL_test3500z00_6141)
												{
													obj_t BgL_l1148z00_6145;

													BgL_l1148z00_6145 = CDR(BgL_l1148z00_1431);
													BgL_l1148z00_1431 = BgL_l1148z00_6145;
													goto BgL_zc3z04anonymousza31417ze3z87_1432;
												}
											else
												{	/* Eval/evmodule.scm 283 */
													BgL_tmpz00_6136 = BFALSE;
												}
										}
									else
										{	/* Eval/evmodule.scm 283 */
											BgL_tmpz00_6136 =
												BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
												(BGl_string3033z00zz__evmodulez00,
												BGl_string3034z00zz__evmodulez00, BgL_l1148z00_1431,
												BGl_string3001z00zz__evmodulez00, BINT(11236L));
										}
								}
							BgL_test3496z00_6133 = CBOOL(BgL_tmpz00_6136);
						}
					}
				else
					{	/* Eval/evmodule.scm 283 */
						BgL_test3496z00_6133 = ((bool_t) 0);
					}
				if (BgL_test3496z00_6133)
					{	/* Eval/evmodule.scm 286 */
						obj_t BgL_arg1411z00_1417;

						{	/* Eval/evmodule.scm 288 */
							obj_t BgL_pairz00_3147;

							if (PAIRP(BgL_clausez00_57))
								{	/* Eval/evmodule.scm 288 */
									BgL_pairz00_3147 = BgL_clausez00_57;
								}
							else
								{
									obj_t BgL_auxz00_6152;

									BgL_auxz00_6152 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(11430L),
										BGl_string3035z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_clausez00_57);
									FAILURE(BgL_auxz00_6152, BFALSE, BFALSE);
								}
							BgL_arg1411z00_1417 = CDR(BgL_pairz00_3147);
						}
						return
							BBOOL(BGl_forzd2eachzf2locz20zz__evmodulez00(BgL_locz00_58,
								BGl_proc3036z00zz__evmodulez00, BgL_arg1411z00_1417));
					}
				else
					{	/* Eval/evmodule.scm 283 */
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_58,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3038z00zz__evmodulez00, BgL_clausez00_57);
					}
			}
		}

	}



/* &<@anonymous:1412> */
	obj_t BGl_z62zc3z04anonymousza31412ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4229, obj_t BgL_locz00_4230, obj_t BgL_sz00_4231)
	{
		{	/* Eval/evmodule.scm 286 */
			{	/* Eval/evmodule.scm 287 */
				obj_t BgL_arg1413z00_5079;

				{	/* Eval/evmodule.scm 287 */
					obj_t BgL_arg1414z00_5080;

					{	/* Eval/evmodule.scm 287 */
						obj_t BgL_arg1415z00_5081;

						{	/* Eval/evmodule.scm 287 */
							obj_t BgL_arg1416z00_5082;

							BgL_arg1416z00_5082 = MAKE_YOUNG_PAIR(BgL_sz00_4231, BNIL);
							BgL_arg1415z00_5081 =
								MAKE_YOUNG_PAIR(BGl_symbol3039z00zz__evmodulez00,
								BgL_arg1416z00_5082);
						}
						BgL_arg1414z00_5080 = MAKE_YOUNG_PAIR(BgL_arg1415z00_5081, BNIL);
					}
					BgL_arg1413z00_5079 =
						MAKE_YOUNG_PAIR(BGl_symbol3041z00zz__evmodulez00,
						BgL_arg1414z00_5080);
				}
				{	/* Eval/evmodule.scm 287 */

					{	/* Eval/evmodule.scm 387 */
						obj_t BgL_expz00_5083;

						if (CBOOL(BgL_locz00_4230))
							{	/* Eval/evmodule.scm 388 */
								obj_t BgL_arg1494z00_5084;
								obj_t BgL_arg1495z00_5085;

								BgL_arg1494z00_5084 = CAR(BgL_arg1413z00_5079);
								BgL_arg1495z00_5085 = CDR(BgL_arg1413z00_5079);
								{	/* Eval/evmodule.scm 388 */
									obj_t BgL_res2347z00_5086;

									BgL_res2347z00_5086 =
										MAKE_YOUNG_EPAIR(BgL_arg1494z00_5084, BgL_arg1495z00_5085,
										BgL_locz00_4230);
									BgL_expz00_5083 = BgL_res2347z00_5086;
								}
							}
						else
							{	/* Eval/evmodule.scm 387 */
								BgL_expz00_5083 = BgL_arg1413z00_5079;
							}
						return BGl_evalz00zz__evalz00(BgL_expz00_5083, BFALSE);
					}
				}
			}
		}

	}



/* evmodule-option */
	obj_t BGl_evmodulezd2optionzd2zz__evmodulez00(obj_t BgL_clausez00_59,
		obj_t BgL_locz00_60)
	{
		{	/* Eval/evmodule.scm 293 */
			{
				obj_t BgL_expz00_1440;

				if (PAIRP(BgL_clausez00_59))
					{	/* Eval/evmodule.scm 294 */
						if (
							(CAR(
									((obj_t) BgL_clausez00_59)) ==
								BGl_symbol3045z00zz__evmodulez00))
							{	/* Eval/evmodule.scm 294 */
								obj_t BgL_arg1423z00_1447;

								BgL_arg1423z00_1447 = CDR(((obj_t) BgL_clausez00_59));
								BgL_expz00_1440 = BgL_arg1423z00_1447;
								{	/* Eval/evmodule.scm 296 */
									obj_t BgL_locz00_1449;

									{	/* Eval/evmodule.scm 296 */
										obj_t BgL__ortest_1045z00_1461;

										BgL__ortest_1045z00_1461 =
											BGl_getzd2sourcezd2locationz00zz__readerz00
											(BgL_clausez00_59);
										if (CBOOL(BgL__ortest_1045z00_1461))
											{	/* Eval/evmodule.scm 296 */
												BgL_locz00_1449 = BgL__ortest_1045z00_1461;
											}
										else
											{	/* Eval/evmodule.scm 296 */
												BgL_locz00_1449 = BgL_locz00_60;
											}
									}
									{
										obj_t BgL_l1151z00_1451;

										BgL_l1151z00_1451 = BgL_expz00_1440;
									BgL_zc3z04anonymousza31425ze3z87_1452:
										if (PAIRP(BgL_l1151z00_1451))
											{	/* Eval/evmodule.scm 297 */
												{	/* Eval/evmodule.scm 297 */
													obj_t BgL_ez00_1454;

													BgL_ez00_1454 = CAR(BgL_l1151z00_1451);
													{	/* Eval/evmodule.scm 297 */

														{	/* Eval/evmodule.scm 387 */
															obj_t BgL_expz00_3162;

															if (CBOOL(BgL_locz00_1449))
																{	/* Eval/evmodule.scm 388 */
																	obj_t BgL_arg1494z00_3163;
																	obj_t BgL_arg1495z00_3164;

																	{	/* Eval/evmodule.scm 388 */
																		obj_t BgL_pairz00_3165;

																		if (PAIRP(BgL_ez00_1454))
																			{	/* Eval/evmodule.scm 388 */
																				BgL_pairz00_3165 = BgL_ez00_1454;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6188;

																				BgL_auxz00_6188 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(15560L),
																					BGl_string3043z00zz__evmodulez00,
																					BGl_string3003z00zz__evmodulez00,
																					BgL_ez00_1454);
																				FAILURE(BgL_auxz00_6188, BFALSE,
																					BFALSE);
																			}
																		BgL_arg1494z00_3163 = CAR(BgL_pairz00_3165);
																	}
																	{	/* Eval/evmodule.scm 388 */
																		obj_t BgL_pairz00_3166;

																		if (PAIRP(BgL_ez00_1454))
																			{	/* Eval/evmodule.scm 388 */
																				BgL_pairz00_3166 = BgL_ez00_1454;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6195;

																				BgL_auxz00_6195 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(15570L),
																					BGl_string3043z00zz__evmodulez00,
																					BGl_string3003z00zz__evmodulez00,
																					BgL_ez00_1454);
																				FAILURE(BgL_auxz00_6195, BFALSE,
																					BFALSE);
																			}
																		BgL_arg1495z00_3164 = CDR(BgL_pairz00_3166);
																	}
																	{	/* Eval/evmodule.scm 388 */
																		obj_t BgL_res2348z00_3167;

																		BgL_res2348z00_3167 =
																			MAKE_YOUNG_EPAIR(BgL_arg1494z00_3163,
																			BgL_arg1495z00_3164, BgL_locz00_1449);
																		BgL_expz00_3162 = BgL_res2348z00_3167;
																	}
																}
															else
																{	/* Eval/evmodule.scm 387 */
																	BgL_expz00_3162 = BgL_ez00_1454;
																}
															BGl_evalz00zz__evalz00(BgL_expz00_3162, BFALSE);
														}
													}
												}
												{
													obj_t BgL_l1151z00_6202;

													BgL_l1151z00_6202 = CDR(BgL_l1151z00_1451);
													BgL_l1151z00_1451 = BgL_l1151z00_6202;
													goto BgL_zc3z04anonymousza31425ze3z87_1452;
												}
											}
										else
											{	/* Eval/evmodule.scm 297 */
												if (NULLP(BgL_l1151z00_1451))
													{	/* Eval/evmodule.scm 297 */
														return BTRUE;
													}
												else
													{	/* Eval/evmodule.scm 297 */
														return
															BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
															(BGl_string3044z00zz__evmodulez00,
															BGl_string3034z00zz__evmodulez00,
															BgL_l1151z00_1451,
															BGl_string3001z00zz__evmodulez00, BINT(11807L));
													}
											}
									}
								}
							}
						else
							{	/* Eval/evmodule.scm 294 */
								return
									BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_60,
									BGl_string3037z00zz__evmodulez00,
									BGl_string3047z00zz__evmodulez00, BgL_clausez00_59);
							}
					}
				else
					{	/* Eval/evmodule.scm 294 */
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_60,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3047z00zz__evmodulez00, BgL_clausez00_59);
					}
			}
		}

	}



/* mark-global! */
	obj_t BGl_markzd2globalz12zc0zz__evmodulez00(obj_t BgL_idz00_61,
		obj_t BgL_modz00_62, long BgL_tagz00_63, obj_t BgL_locz00_64)
	{
		{	/* Eval/evmodule.scm 304 */
			{	/* Eval/evmodule.scm 305 */
				obj_t BgL_vz00_3171;

				{	/* Eval/evmodule.scm 305 */
					obj_t BgL_auxz00_6210;

					if (SYMBOLP(BgL_idz00_61))
						{	/* Eval/evmodule.scm 305 */
							BgL_auxz00_6210 = BgL_idz00_61;
						}
					else
						{
							obj_t BgL_auxz00_6213;

							BgL_auxz00_6213 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(12237L),
								BGl_string3048z00zz__evmodulez00,
								BGl_string3005z00zz__evmodulez00, BgL_idz00_61);
							FAILURE(BgL_auxz00_6213, BFALSE, BFALSE);
						}
					BgL_vz00_3171 =
						BGl_evmodulezd2findzd2globalz00zz__evmodulez00(BgL_modz00_62,
						BgL_auxz00_6210);
				}
				{	/* Eval/evmodule.scm 306 */
					bool_t BgL_test3512z00_6218;

					if (VECTORP(BgL_vz00_3171))
						{	/* Eval/evmodule.scm 306 */
							BgL_test3512z00_6218 = (VECTOR_LENGTH(BgL_vz00_3171) == 5L);
						}
					else
						{	/* Eval/evmodule.scm 306 */
							BgL_test3512z00_6218 = ((bool_t) 0);
						}
					if (BgL_test3512z00_6218)
						{	/* Eval/evmodule.scm 306 */
							{	/* Eval/evmodule.scm 308 */
								int BgL_tagz00_3178;

								BgL_tagz00_3178 = (int) (BgL_tagz00_63);
								VECTOR_SET(BgL_vz00_3171, 0L, BINT(BgL_tagz00_3178));
							}
							return BgL_idz00_61;
						}
					else
						{	/* Eval/evmodule.scm 306 */
							return
								BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_64,
								BGl_string3037z00zz__evmodulez00,
								BGl_string3049z00zz__evmodulez00, BgL_idz00_61);
						}
				}
			}
		}

	}



/* evmodule-static */
	obj_t BGl_evmodulezd2staticzd2zz__evmodulez00(obj_t BgL_modz00_74,
		obj_t BgL_clausez00_75, obj_t BgL_locz00_76, bool_t BgL_classpz00_77)
	{
		{	/* Eval/evmodule.scm 335 */
			{	/* Eval/evmodule.scm 372 */
				obj_t BgL_evmodulezd2staticzd2clausez00_4232;

				{
					int BgL_tmpz00_6227;

					BgL_tmpz00_6227 = (int) (3L);
					BgL_evmodulezd2staticzd2clausez00_4232 =
						MAKE_L_PROCEDURE
						(BGl_z62evmodulezd2staticzd2clausez62zz__evmodulez00,
						BgL_tmpz00_6227);
				}
				PROCEDURE_L_SET(BgL_evmodulezd2staticzd2clausez00_4232,
					(int) (0L), BgL_modz00_74);
				PROCEDURE_L_SET(BgL_evmodulezd2staticzd2clausez00_4232,
					(int) (1L), BBOOL(BgL_classpz00_77));
				PROCEDURE_L_SET(BgL_evmodulezd2staticzd2clausez00_4232,
					(int) (2L), BgL_clausez00_75);
				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausez00_75))
					{	/* Eval/evmodule.scm 375 */
						obj_t BgL_arg1431z00_1467;

						{	/* Eval/evmodule.scm 375 */
							obj_t BgL_pairz00_3390;

							if (PAIRP(BgL_clausez00_75))
								{	/* Eval/evmodule.scm 375 */
									BgL_pairz00_3390 = BgL_clausez00_75;
								}
							else
								{
									obj_t BgL_auxz00_6241;

									BgL_auxz00_6241 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(14911L),
										BGl_string3050z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_clausez00_75);
									FAILURE(BgL_auxz00_6241, BFALSE, BFALSE);
								}
							BgL_arg1431z00_1467 = CDR(BgL_pairz00_3390);
						}
						return
							BBOOL(BGl_forzd2eachzf2locz20zz__evmodulez00(BgL_locz00_76,
								BgL_evmodulezd2staticzd2clausez00_4232, BgL_arg1431z00_1467));
					}
				else
					{	/* Eval/evmodule.scm 373 */
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_76,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3051z00zz__evmodulez00, BgL_clausez00_75);
					}
			}
		}

	}



/* &evmodule-static-clause */
	obj_t BGl_z62evmodulezd2staticzd2clausez62zz__evmodulez00(obj_t
		BgL_envz00_4233, obj_t BgL_locz00_4237, obj_t BgL_sz00_4238)
	{
		{	/* Eval/evmodule.scm 372 */
			{	/* Eval/evmodule.scm 372 */
				obj_t BgL_modz00_4234;
				bool_t BgL_classpz00_4235;
				obj_t BgL_clausez00_4236;

				BgL_modz00_4234 = PROCEDURE_L_REF(BgL_envz00_4233, (int) (0L));
				BgL_classpz00_4235 =
					CBOOL(PROCEDURE_L_REF(BgL_envz00_4233, (int) (1L)));
				BgL_clausez00_4236 = PROCEDURE_L_REF(BgL_envz00_4233, (int) (2L));
				{
					obj_t BgL_sz00_5092;

					if (SYMBOLP(BgL_sz00_4238))
						{	/* Eval/evmodule.scm 372 */
							if (BgL_classpz00_4235)
								{	/* Eval/evmodule.scm 339 */
									return BFALSE;
								}
							else
								{	/* Eval/evmodule.scm 340 */
									obj_t BgL_idz00_5104;

									BgL_idz00_5104 =
										BGl_untypezd2identzd2zz__evmodulez00(BgL_sz00_4238);
									{	/* Eval/evmodule.scm 328 */
										obj_t BgL_gz00_5105;

										{	/* Eval/evmodule.scm 328 */
											obj_t BgL_idz00_5106;

											if (SYMBOLP(BgL_idz00_5104))
												{	/* Eval/evmodule.scm 328 */
													BgL_idz00_5106 = BgL_idz00_5104;
												}
											else
												{
													obj_t BgL_auxz00_6262;

													BgL_auxz00_6262 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(13270L),
														BGl_string3055z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00, BgL_idz00_5104);
													FAILURE(BgL_auxz00_6262, BFALSE, BFALSE);
												}
											{	/* Eval/evmodule.scm 328 */
												obj_t BgL_v1213z00_5107;

												BgL_v1213z00_5107 = create_vector(5L);
												VECTOR_SET(BgL_v1213z00_5107, 0L, BINT(2L));
												VECTOR_SET(BgL_v1213z00_5107, 1L, BgL_idz00_5106);
												VECTOR_SET(BgL_v1213z00_5107, 2L, BUNSPEC);
												VECTOR_SET(BgL_v1213z00_5107, 3L, BgL_modz00_4234);
												VECTOR_SET(BgL_v1213z00_5107, 4L, BgL_locz00_4237);
												BgL_gz00_5105 = BgL_v1213z00_5107;
											}
										}
										{	/* Eval/evmodule.scm 329 */
											obj_t BgL_auxz00_6273;

											if (SYMBOLP(BgL_idz00_5104))
												{	/* Eval/evmodule.scm 329 */
													BgL_auxz00_6273 = BgL_idz00_5104;
												}
											else
												{
													obj_t BgL_auxz00_6276;

													BgL_auxz00_6276 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(13317L),
														BGl_string3055z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00, BgL_idz00_5104);
													FAILURE(BgL_auxz00_6276, BFALSE, BFALSE);
												}
											BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
												(BgL_modz00_4234, BgL_auxz00_6273, BgL_gz00_5105,
												BgL_locz00_4237);
										}
										BgL_gz00_5105;
									}
									{	/* Eval/evmodule.scm 305 */
										obj_t BgL_vz00_5108;

										{	/* Eval/evmodule.scm 305 */
											obj_t BgL_auxz00_6281;

											if (SYMBOLP(BgL_idz00_5104))
												{	/* Eval/evmodule.scm 305 */
													BgL_auxz00_6281 = BgL_idz00_5104;
												}
											else
												{
													obj_t BgL_auxz00_6284;

													BgL_auxz00_6284 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(12237L),
														BGl_string3055z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00, BgL_idz00_5104);
													FAILURE(BgL_auxz00_6284, BFALSE, BFALSE);
												}
											BgL_vz00_5108 =
												BGl_evmodulezd2findzd2globalz00zz__evmodulez00
												(BgL_modz00_4234, BgL_auxz00_6281);
										}
										{	/* Eval/evmodule.scm 306 */
											bool_t BgL_test3521z00_6289;

											if (VECTORP(BgL_vz00_5108))
												{	/* Eval/evmodule.scm 306 */
													BgL_test3521z00_6289 =
														(VECTOR_LENGTH(BgL_vz00_5108) == 5L);
												}
											else
												{	/* Eval/evmodule.scm 306 */
													BgL_test3521z00_6289 = ((bool_t) 0);
												}
											if (BgL_test3521z00_6289)
												{	/* Eval/evmodule.scm 306 */
													VECTOR_SET(BgL_vz00_5108, 0L, BINT(3L));
													return BgL_idz00_5104;
												}
											else
												{	/* Eval/evmodule.scm 306 */
													return
														BGl_evcompilezd2errorzd2zz__evcompilez00
														(BgL_locz00_4237, BGl_string3037z00zz__evmodulez00,
														BGl_string3049z00zz__evmodulez00, BgL_idz00_5104);
												}
										}
									}
								}
						}
					else
						{	/* Eval/evmodule.scm 372 */
							if (PAIRP(BgL_sz00_4238))
								{	/* Eval/evmodule.scm 372 */
									obj_t BgL_cdrzd2127zd2_5109;

									BgL_cdrzd2127zd2_5109 = CDR(((obj_t) BgL_sz00_4238));
									if (
										(CAR(
												((obj_t) BgL_sz00_4238)) ==
											BGl_symbol3056z00zz__evmodulez00))
										{	/* Eval/evmodule.scm 372 */
											if (PAIRP(BgL_cdrzd2127zd2_5109))
												{	/* Eval/evmodule.scm 372 */
													obj_t BgL_carzd2130zd2_5112;

													BgL_carzd2130zd2_5112 = CAR(BgL_cdrzd2127zd2_5109);
													if (SYMBOLP(BgL_carzd2130zd2_5112))
														{	/* Eval/evmodule.scm 372 */
															obj_t BgL_arg1439z00_5113;

															BgL_arg1439z00_5113 = CDR(BgL_cdrzd2127zd2_5109);
															if (BgL_classpz00_4235)
																{	/* Eval/evmodule.scm 345 */
																	obj_t BgL_auxz00_6312;

																	{	/* Eval/evmodule.scm 345 */
																		bool_t BgL_test3528z00_6313;

																		if (PAIRP(BgL_arg1439z00_5113))
																			{	/* Eval/evmodule.scm 345 */
																				BgL_test3528z00_6313 = ((bool_t) 1);
																			}
																		else
																			{	/* Eval/evmodule.scm 345 */
																				BgL_test3528z00_6313 =
																					NULLP(BgL_arg1439z00_5113);
																			}
																		if (BgL_test3528z00_6313)
																			{	/* Eval/evmodule.scm 345 */
																				BgL_auxz00_6312 = BgL_arg1439z00_5113;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6317;

																				BgL_auxz00_6317 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(13900L),
																					BGl_string3058z00zz__evmodulez00,
																					BGl_string3059z00zz__evmodulez00,
																					BgL_arg1439z00_5113);
																				FAILURE(BgL_auxz00_6317, BFALSE,
																					BFALSE);
																			}
																	}
																	return
																		BGl_evalzd2classzd2zz__evobjectz00
																		(BgL_carzd2130zd2_5112, ((bool_t) 0),
																		BgL_auxz00_6312, BgL_sz00_4238,
																		BgL_modz00_4234);
																}
															else
																{	/* Eval/evmodule.scm 344 */
																	return BFALSE;
																}
														}
													else
														{	/* Eval/evmodule.scm 372 */
															obj_t BgL_carzd2174zd2_5115;

															BgL_carzd2174zd2_5115 =
																CAR(((obj_t) BgL_sz00_4238));
															if (SYMBOLP(BgL_carzd2174zd2_5115))
																{	/* Eval/evmodule.scm 372 */
																	if (BgL_classpz00_4235)
																		{	/* Eval/evmodule.scm 365 */
																			return BFALSE;
																		}
																	else
																		{	/* Eval/evmodule.scm 366 */
																			obj_t BgL_idz00_5117;

																			BgL_idz00_5117 =
																				BGl_untypezd2identzd2zz__evmodulez00
																				(BgL_carzd2174zd2_5115);
																			{	/* Eval/evmodule.scm 328 */
																				obj_t BgL_gz00_5118;

																				{	/* Eval/evmodule.scm 328 */
																					obj_t BgL_idz00_5119;

																					if (SYMBOLP(BgL_idz00_5117))
																						{	/* Eval/evmodule.scm 328 */
																							BgL_idz00_5119 = BgL_idz00_5117;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6330;

																							BgL_auxz00_6330 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(13270L),
																								BGl_string3058z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5117);
																							FAILURE(BgL_auxz00_6330, BFALSE,
																								BFALSE);
																						}
																					{	/* Eval/evmodule.scm 328 */
																						obj_t BgL_v1213z00_5120;

																						BgL_v1213z00_5120 =
																							create_vector(5L);
																						VECTOR_SET(BgL_v1213z00_5120, 0L,
																							BINT(2L));
																						VECTOR_SET(BgL_v1213z00_5120, 1L,
																							BgL_idz00_5119);
																						VECTOR_SET(BgL_v1213z00_5120, 2L,
																							BUNSPEC);
																						VECTOR_SET(BgL_v1213z00_5120, 3L,
																							BgL_modz00_4234);
																						VECTOR_SET(BgL_v1213z00_5120, 4L,
																							BgL_locz00_4237);
																						BgL_gz00_5118 = BgL_v1213z00_5120;
																					}
																				}
																				{	/* Eval/evmodule.scm 329 */
																					obj_t BgL_auxz00_6341;

																					if (SYMBOLP(BgL_idz00_5117))
																						{	/* Eval/evmodule.scm 329 */
																							BgL_auxz00_6341 = BgL_idz00_5117;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6344;

																							BgL_auxz00_6344 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(13317L),
																								BGl_string3058z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5117);
																							FAILURE(BgL_auxz00_6344, BFALSE,
																								BFALSE);
																						}
																					BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																						(BgL_modz00_4234, BgL_auxz00_6341,
																						BgL_gz00_5118, BgL_locz00_4237);
																				}
																				BgL_gz00_5118;
																			}
																			return
																				BGl_markzd2globalz12zc0zz__evmodulez00
																				(BgL_idz00_5117, BgL_modz00_4234, 4L,
																				BgL_locz00_4237);
																		}
																}
															else
																{	/* Eval/evmodule.scm 372 */
																	return
																		BGl_evcompilezd2errorzd2zz__evcompilez00
																		(BgL_locz00_4237,
																		BGl_string3037z00zz__evmodulez00,
																		BGl_string3051z00zz__evmodulez00,
																		BgL_clausez00_4236);
																}
														}
												}
											else
												{	/* Eval/evmodule.scm 372 */
													obj_t BgL_carzd2198zd2_5121;

													BgL_carzd2198zd2_5121 = CAR(((obj_t) BgL_sz00_4238));
													if (SYMBOLP(BgL_carzd2198zd2_5121))
														{	/* Eval/evmodule.scm 372 */
															if (BgL_classpz00_4235)
																{	/* Eval/evmodule.scm 365 */
																	return BFALSE;
																}
															else
																{	/* Eval/evmodule.scm 366 */
																	obj_t BgL_idz00_5123;

																	BgL_idz00_5123 =
																		BGl_untypezd2identzd2zz__evmodulez00
																		(BgL_carzd2198zd2_5121);
																	{	/* Eval/evmodule.scm 328 */
																		obj_t BgL_gz00_5124;

																		{	/* Eval/evmodule.scm 328 */
																			obj_t BgL_idz00_5125;

																			if (SYMBOLP(BgL_idz00_5123))
																				{	/* Eval/evmodule.scm 328 */
																					BgL_idz00_5125 = BgL_idz00_5123;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6359;

																					BgL_auxz00_6359 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3001z00zz__evmodulez00,
																						BINT(13270L),
																						BGl_string3058z00zz__evmodulez00,
																						BGl_string3005z00zz__evmodulez00,
																						BgL_idz00_5123);
																					FAILURE(BgL_auxz00_6359, BFALSE,
																						BFALSE);
																				}
																			{	/* Eval/evmodule.scm 328 */
																				obj_t BgL_v1213z00_5126;

																				BgL_v1213z00_5126 = create_vector(5L);
																				VECTOR_SET(BgL_v1213z00_5126, 0L,
																					BINT(2L));
																				VECTOR_SET(BgL_v1213z00_5126, 1L,
																					BgL_idz00_5125);
																				VECTOR_SET(BgL_v1213z00_5126, 2L,
																					BUNSPEC);
																				VECTOR_SET(BgL_v1213z00_5126, 3L,
																					BgL_modz00_4234);
																				VECTOR_SET(BgL_v1213z00_5126, 4L,
																					BgL_locz00_4237);
																				BgL_gz00_5124 = BgL_v1213z00_5126;
																			}
																		}
																		{	/* Eval/evmodule.scm 329 */
																			obj_t BgL_auxz00_6370;

																			if (SYMBOLP(BgL_idz00_5123))
																				{	/* Eval/evmodule.scm 329 */
																					BgL_auxz00_6370 = BgL_idz00_5123;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6373;

																					BgL_auxz00_6373 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3001z00zz__evmodulez00,
																						BINT(13317L),
																						BGl_string3058z00zz__evmodulez00,
																						BGl_string3005z00zz__evmodulez00,
																						BgL_idz00_5123);
																					FAILURE(BgL_auxz00_6373, BFALSE,
																						BFALSE);
																				}
																			BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																				(BgL_modz00_4234, BgL_auxz00_6370,
																				BgL_gz00_5124, BgL_locz00_4237);
																		}
																		BgL_gz00_5124;
																	}
																	return
																		BGl_markzd2globalz12zc0zz__evmodulez00
																		(BgL_idz00_5123, BgL_modz00_4234, 4L,
																		BgL_locz00_4237);
																}
														}
													else
														{	/* Eval/evmodule.scm 372 */
															return
																BGl_evcompilezd2errorzd2zz__evcompilez00
																(BgL_locz00_4237,
																BGl_string3037z00zz__evmodulez00,
																BGl_string3051z00zz__evmodulez00,
																BgL_clausez00_4236);
														}
												}
										}
									else
										{	/* Eval/evmodule.scm 372 */
											obj_t BgL_cdrzd2209zd2_5127;

											BgL_cdrzd2209zd2_5127 = CDR(((obj_t) BgL_sz00_4238));
											if (
												(CAR(
														((obj_t) BgL_sz00_4238)) ==
													BGl_symbol3060z00zz__evmodulez00))
												{	/* Eval/evmodule.scm 372 */
													if (PAIRP(BgL_cdrzd2209zd2_5127))
														{	/* Eval/evmodule.scm 372 */
															obj_t BgL_carzd2212zd2_5130;

															BgL_carzd2212zd2_5130 =
																CAR(BgL_cdrzd2209zd2_5127);
															if (SYMBOLP(BgL_carzd2212zd2_5130))
																{	/* Eval/evmodule.scm 372 */
																	obj_t BgL_arg1449z00_5131;

																	BgL_arg1449z00_5131 =
																		CDR(BgL_cdrzd2209zd2_5127);
																	if (BgL_classpz00_4235)
																		{	/* Eval/evmodule.scm 348 */
																			obj_t BgL_auxz00_6393;

																			{	/* Eval/evmodule.scm 348 */
																				bool_t BgL_test3542z00_6394;

																				if (PAIRP(BgL_arg1449z00_5131))
																					{	/* Eval/evmodule.scm 348 */
																						BgL_test3542z00_6394 = ((bool_t) 1);
																					}
																				else
																					{	/* Eval/evmodule.scm 348 */
																						BgL_test3542z00_6394 =
																							NULLP(BgL_arg1449z00_5131);
																					}
																				if (BgL_test3542z00_6394)
																					{	/* Eval/evmodule.scm 348 */
																						BgL_auxz00_6393 =
																							BgL_arg1449z00_5131;
																					}
																				else
																					{
																						obj_t BgL_auxz00_6398;

																						BgL_auxz00_6398 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string3001z00zz__evmodulez00,
																							BINT(14009L),
																							BGl_string3058z00zz__evmodulez00,
																							BGl_string3059z00zz__evmodulez00,
																							BgL_arg1449z00_5131);
																						FAILURE(BgL_auxz00_6398, BFALSE,
																							BFALSE);
																					}
																			}
																			return
																				BGl_evalzd2classzd2zz__evobjectz00
																				(BgL_carzd2212zd2_5130, ((bool_t) 0),
																				BgL_auxz00_6393, BgL_sz00_4238,
																				BgL_modz00_4234);
																		}
																	else
																		{	/* Eval/evmodule.scm 347 */
																			return BFALSE;
																		}
																}
															else
																{	/* Eval/evmodule.scm 372 */
																	obj_t BgL_carzd2251zd2_5133;

																	BgL_carzd2251zd2_5133 =
																		CAR(((obj_t) BgL_sz00_4238));
																	if (SYMBOLP(BgL_carzd2251zd2_5133))
																		{	/* Eval/evmodule.scm 372 */
																			if (BgL_classpz00_4235)
																				{	/* Eval/evmodule.scm 365 */
																					return BFALSE;
																				}
																			else
																				{	/* Eval/evmodule.scm 366 */
																					obj_t BgL_idz00_5135;

																					BgL_idz00_5135 =
																						BGl_untypezd2identzd2zz__evmodulez00
																						(BgL_carzd2251zd2_5133);
																					{	/* Eval/evmodule.scm 328 */
																						obj_t BgL_gz00_5136;

																						{	/* Eval/evmodule.scm 328 */
																							obj_t BgL_idz00_5137;

																							if (SYMBOLP(BgL_idz00_5135))
																								{	/* Eval/evmodule.scm 328 */
																									BgL_idz00_5137 =
																										BgL_idz00_5135;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6411;

																									BgL_auxz00_6411 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(13270L),
																										BGl_string3058z00zz__evmodulez00,
																										BGl_string3005z00zz__evmodulez00,
																										BgL_idz00_5135);
																									FAILURE(BgL_auxz00_6411,
																										BFALSE, BFALSE);
																								}
																							{	/* Eval/evmodule.scm 328 */
																								obj_t BgL_v1213z00_5138;

																								BgL_v1213z00_5138 =
																									create_vector(5L);
																								VECTOR_SET(BgL_v1213z00_5138,
																									0L, BINT(2L));
																								VECTOR_SET(BgL_v1213z00_5138,
																									1L, BgL_idz00_5137);
																								VECTOR_SET(BgL_v1213z00_5138,
																									2L, BUNSPEC);
																								VECTOR_SET(BgL_v1213z00_5138,
																									3L, BgL_modz00_4234);
																								VECTOR_SET(BgL_v1213z00_5138,
																									4L, BgL_locz00_4237);
																								BgL_gz00_5136 =
																									BgL_v1213z00_5138;
																							}
																						}
																						{	/* Eval/evmodule.scm 329 */
																							obj_t BgL_auxz00_6422;

																							if (SYMBOLP(BgL_idz00_5135))
																								{	/* Eval/evmodule.scm 329 */
																									BgL_auxz00_6422 =
																										BgL_idz00_5135;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6425;

																									BgL_auxz00_6425 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(13317L),
																										BGl_string3058z00zz__evmodulez00,
																										BGl_string3005z00zz__evmodulez00,
																										BgL_idz00_5135);
																									FAILURE(BgL_auxz00_6425,
																										BFALSE, BFALSE);
																								}
																							BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																								(BgL_modz00_4234,
																								BgL_auxz00_6422, BgL_gz00_5136,
																								BgL_locz00_4237);
																						}
																						BgL_gz00_5136;
																					}
																					return
																						BGl_markzd2globalz12zc0zz__evmodulez00
																						(BgL_idz00_5135, BgL_modz00_4234,
																						4L, BgL_locz00_4237);
																				}
																		}
																	else
																		{	/* Eval/evmodule.scm 372 */
																			return
																				BGl_evcompilezd2errorzd2zz__evcompilez00
																				(BgL_locz00_4237,
																				BGl_string3037z00zz__evmodulez00,
																				BGl_string3051z00zz__evmodulez00,
																				BgL_clausez00_4236);
																		}
																}
														}
													else
														{	/* Eval/evmodule.scm 372 */
															obj_t BgL_carzd2270zd2_5139;

															BgL_carzd2270zd2_5139 =
																CAR(((obj_t) BgL_sz00_4238));
															if (SYMBOLP(BgL_carzd2270zd2_5139))
																{	/* Eval/evmodule.scm 372 */
																	if (BgL_classpz00_4235)
																		{	/* Eval/evmodule.scm 365 */
																			return BFALSE;
																		}
																	else
																		{	/* Eval/evmodule.scm 366 */
																			obj_t BgL_idz00_5141;

																			BgL_idz00_5141 =
																				BGl_untypezd2identzd2zz__evmodulez00
																				(BgL_carzd2270zd2_5139);
																			{	/* Eval/evmodule.scm 328 */
																				obj_t BgL_gz00_5142;

																				{	/* Eval/evmodule.scm 328 */
																					obj_t BgL_idz00_5143;

																					if (SYMBOLP(BgL_idz00_5141))
																						{	/* Eval/evmodule.scm 328 */
																							BgL_idz00_5143 = BgL_idz00_5141;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6440;

																							BgL_auxz00_6440 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(13270L),
																								BGl_string3058z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5141);
																							FAILURE(BgL_auxz00_6440, BFALSE,
																								BFALSE);
																						}
																					{	/* Eval/evmodule.scm 328 */
																						obj_t BgL_v1213z00_5144;

																						BgL_v1213z00_5144 =
																							create_vector(5L);
																						VECTOR_SET(BgL_v1213z00_5144, 0L,
																							BINT(2L));
																						VECTOR_SET(BgL_v1213z00_5144, 1L,
																							BgL_idz00_5143);
																						VECTOR_SET(BgL_v1213z00_5144, 2L,
																							BUNSPEC);
																						VECTOR_SET(BgL_v1213z00_5144, 3L,
																							BgL_modz00_4234);
																						VECTOR_SET(BgL_v1213z00_5144, 4L,
																							BgL_locz00_4237);
																						BgL_gz00_5142 = BgL_v1213z00_5144;
																					}
																				}
																				{	/* Eval/evmodule.scm 329 */
																					obj_t BgL_auxz00_6451;

																					if (SYMBOLP(BgL_idz00_5141))
																						{	/* Eval/evmodule.scm 329 */
																							BgL_auxz00_6451 = BgL_idz00_5141;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6454;

																							BgL_auxz00_6454 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(13317L),
																								BGl_string3058z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5141);
																							FAILURE(BgL_auxz00_6454, BFALSE,
																								BFALSE);
																						}
																					BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																						(BgL_modz00_4234, BgL_auxz00_6451,
																						BgL_gz00_5142, BgL_locz00_4237);
																				}
																				BgL_gz00_5142;
																			}
																			return
																				BGl_markzd2globalz12zc0zz__evmodulez00
																				(BgL_idz00_5141, BgL_modz00_4234, 4L,
																				BgL_locz00_4237);
																		}
																}
															else
																{	/* Eval/evmodule.scm 372 */
																	return
																		BGl_evcompilezd2errorzd2zz__evcompilez00
																		(BgL_locz00_4237,
																		BGl_string3037z00zz__evmodulez00,
																		BGl_string3051z00zz__evmodulez00,
																		BgL_clausez00_4236);
																}
														}
												}
											else
												{	/* Eval/evmodule.scm 372 */
													obj_t BgL_cdrzd2281zd2_5145;

													BgL_cdrzd2281zd2_5145 = CDR(((obj_t) BgL_sz00_4238));
													if (
														(CAR(
																((obj_t) BgL_sz00_4238)) ==
															BGl_symbol3062z00zz__evmodulez00))
														{	/* Eval/evmodule.scm 372 */
															if (PAIRP(BgL_cdrzd2281zd2_5145))
																{	/* Eval/evmodule.scm 372 */
																	obj_t BgL_carzd2284zd2_5148;

																	BgL_carzd2284zd2_5148 =
																		CAR(BgL_cdrzd2281zd2_5145);
																	if (SYMBOLP(BgL_carzd2284zd2_5148))
																		{	/* Eval/evmodule.scm 372 */
																			obj_t BgL_arg1459z00_5149;

																			BgL_arg1459z00_5149 =
																				CDR(BgL_cdrzd2281zd2_5145);
																			if (BgL_classpz00_4235)
																				{	/* Eval/evmodule.scm 351 */
																					obj_t BgL_auxz00_6474;

																					{	/* Eval/evmodule.scm 351 */
																						bool_t BgL_test3556z00_6475;

																						if (PAIRP(BgL_arg1459z00_5149))
																							{	/* Eval/evmodule.scm 351 */
																								BgL_test3556z00_6475 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Eval/evmodule.scm 351 */
																								BgL_test3556z00_6475 =
																									NULLP(BgL_arg1459z00_5149);
																							}
																						if (BgL_test3556z00_6475)
																							{	/* Eval/evmodule.scm 351 */
																								BgL_auxz00_6474 =
																									BgL_arg1459z00_5149;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6479;

																								BgL_auxz00_6479 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3001z00zz__evmodulez00,
																									BINT(14121L),
																									BGl_string3058z00zz__evmodulez00,
																									BGl_string3059z00zz__evmodulez00,
																									BgL_arg1459z00_5149);
																								FAILURE(BgL_auxz00_6479, BFALSE,
																									BFALSE);
																							}
																					}
																					return
																						BGl_evalzd2classzd2zz__evobjectz00
																						(BgL_carzd2284zd2_5148,
																						((bool_t) 1), BgL_auxz00_6474,
																						BgL_sz00_4238, BgL_modz00_4234);
																				}
																			else
																				{	/* Eval/evmodule.scm 350 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Eval/evmodule.scm 372 */
																			obj_t BgL_carzd2318zd2_5151;

																			BgL_carzd2318zd2_5151 =
																				CAR(((obj_t) BgL_sz00_4238));
																			if (SYMBOLP(BgL_carzd2318zd2_5151))
																				{	/* Eval/evmodule.scm 372 */
																					if (BgL_classpz00_4235)
																						{	/* Eval/evmodule.scm 365 */
																							return BFALSE;
																						}
																					else
																						{	/* Eval/evmodule.scm 366 */
																							obj_t BgL_idz00_5153;

																							BgL_idz00_5153 =
																								BGl_untypezd2identzd2zz__evmodulez00
																								(BgL_carzd2318zd2_5151);
																							{	/* Eval/evmodule.scm 328 */
																								obj_t BgL_gz00_5154;

																								{	/* Eval/evmodule.scm 328 */
																									obj_t BgL_idz00_5155;

																									if (SYMBOLP(BgL_idz00_5153))
																										{	/* Eval/evmodule.scm 328 */
																											BgL_idz00_5155 =
																												BgL_idz00_5153;
																										}
																									else
																										{
																											obj_t BgL_auxz00_6492;

																											BgL_auxz00_6492 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string3001z00zz__evmodulez00,
																												BINT(13270L),
																												BGl_string3058z00zz__evmodulez00,
																												BGl_string3005z00zz__evmodulez00,
																												BgL_idz00_5153);
																											FAILURE(BgL_auxz00_6492,
																												BFALSE, BFALSE);
																										}
																									{	/* Eval/evmodule.scm 328 */
																										obj_t BgL_v1213z00_5156;

																										BgL_v1213z00_5156 =
																											create_vector(5L);
																										VECTOR_SET
																											(BgL_v1213z00_5156, 0L,
																											BINT(2L));
																										VECTOR_SET
																											(BgL_v1213z00_5156, 1L,
																											BgL_idz00_5155);
																										VECTOR_SET
																											(BgL_v1213z00_5156, 2L,
																											BUNSPEC);
																										VECTOR_SET
																											(BgL_v1213z00_5156, 3L,
																											BgL_modz00_4234);
																										VECTOR_SET
																											(BgL_v1213z00_5156, 4L,
																											BgL_locz00_4237);
																										BgL_gz00_5154 =
																											BgL_v1213z00_5156;
																									}
																								}
																								{	/* Eval/evmodule.scm 329 */
																									obj_t BgL_auxz00_6503;

																									if (SYMBOLP(BgL_idz00_5153))
																										{	/* Eval/evmodule.scm 329 */
																											BgL_auxz00_6503 =
																												BgL_idz00_5153;
																										}
																									else
																										{
																											obj_t BgL_auxz00_6506;

																											BgL_auxz00_6506 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string3001z00zz__evmodulez00,
																												BINT(13317L),
																												BGl_string3058z00zz__evmodulez00,
																												BGl_string3005z00zz__evmodulez00,
																												BgL_idz00_5153);
																											FAILURE(BgL_auxz00_6506,
																												BFALSE, BFALSE);
																										}
																									BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																										(BgL_modz00_4234,
																										BgL_auxz00_6503,
																										BgL_gz00_5154,
																										BgL_locz00_4237);
																								}
																								BgL_gz00_5154;
																							}
																							return
																								BGl_markzd2globalz12zc0zz__evmodulez00
																								(BgL_idz00_5153,
																								BgL_modz00_4234, 4L,
																								BgL_locz00_4237);
																						}
																				}
																			else
																				{	/* Eval/evmodule.scm 372 */
																					return
																						BGl_evcompilezd2errorzd2zz__evcompilez00
																						(BgL_locz00_4237,
																						BGl_string3037z00zz__evmodulez00,
																						BGl_string3051z00zz__evmodulez00,
																						BgL_clausez00_4236);
																				}
																		}
																}
															else
																{	/* Eval/evmodule.scm 372 */
																	obj_t BgL_carzd2332zd2_5157;

																	BgL_carzd2332zd2_5157 =
																		CAR(((obj_t) BgL_sz00_4238));
																	if (SYMBOLP(BgL_carzd2332zd2_5157))
																		{	/* Eval/evmodule.scm 372 */
																			if (BgL_classpz00_4235)
																				{	/* Eval/evmodule.scm 365 */
																					return BFALSE;
																				}
																			else
																				{	/* Eval/evmodule.scm 366 */
																					obj_t BgL_idz00_5159;

																					BgL_idz00_5159 =
																						BGl_untypezd2identzd2zz__evmodulez00
																						(BgL_carzd2332zd2_5157);
																					{	/* Eval/evmodule.scm 328 */
																						obj_t BgL_gz00_5160;

																						{	/* Eval/evmodule.scm 328 */
																							obj_t BgL_idz00_5161;

																							if (SYMBOLP(BgL_idz00_5159))
																								{	/* Eval/evmodule.scm 328 */
																									BgL_idz00_5161 =
																										BgL_idz00_5159;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6521;

																									BgL_auxz00_6521 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(13270L),
																										BGl_string3058z00zz__evmodulez00,
																										BGl_string3005z00zz__evmodulez00,
																										BgL_idz00_5159);
																									FAILURE(BgL_auxz00_6521,
																										BFALSE, BFALSE);
																								}
																							{	/* Eval/evmodule.scm 328 */
																								obj_t BgL_v1213z00_5162;

																								BgL_v1213z00_5162 =
																									create_vector(5L);
																								VECTOR_SET(BgL_v1213z00_5162,
																									0L, BINT(2L));
																								VECTOR_SET(BgL_v1213z00_5162,
																									1L, BgL_idz00_5161);
																								VECTOR_SET(BgL_v1213z00_5162,
																									2L, BUNSPEC);
																								VECTOR_SET(BgL_v1213z00_5162,
																									3L, BgL_modz00_4234);
																								VECTOR_SET(BgL_v1213z00_5162,
																									4L, BgL_locz00_4237);
																								BgL_gz00_5160 =
																									BgL_v1213z00_5162;
																							}
																						}
																						{	/* Eval/evmodule.scm 329 */
																							obj_t BgL_auxz00_6532;

																							if (SYMBOLP(BgL_idz00_5159))
																								{	/* Eval/evmodule.scm 329 */
																									BgL_auxz00_6532 =
																										BgL_idz00_5159;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6535;

																									BgL_auxz00_6535 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(13317L),
																										BGl_string3058z00zz__evmodulez00,
																										BGl_string3005z00zz__evmodulez00,
																										BgL_idz00_5159);
																									FAILURE(BgL_auxz00_6535,
																										BFALSE, BFALSE);
																								}
																							BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																								(BgL_modz00_4234,
																								BgL_auxz00_6532, BgL_gz00_5160,
																								BgL_locz00_4237);
																						}
																						BgL_gz00_5160;
																					}
																					return
																						BGl_markzd2globalz12zc0zz__evmodulez00
																						(BgL_idz00_5159, BgL_modz00_4234,
																						4L, BgL_locz00_4237);
																				}
																		}
																	else
																		{	/* Eval/evmodule.scm 372 */
																			return
																				BGl_evcompilezd2errorzd2zz__evcompilez00
																				(BgL_locz00_4237,
																				BGl_string3037z00zz__evmodulez00,
																				BGl_string3051z00zz__evmodulez00,
																				BgL_clausez00_4236);
																		}
																}
														}
													else
														{	/* Eval/evmodule.scm 372 */
															obj_t BgL_cdrzd2343zd2_5163;

															BgL_cdrzd2343zd2_5163 =
																CDR(((obj_t) BgL_sz00_4238));
															if (
																(CAR(
																		((obj_t) BgL_sz00_4238)) ==
																	BGl_symbol3064z00zz__evmodulez00))
																{	/* Eval/evmodule.scm 372 */
																	if (PAIRP(BgL_cdrzd2343zd2_5163))
																		{	/* Eval/evmodule.scm 372 */
																			obj_t BgL_carzd2346zd2_5166;

																			BgL_carzd2346zd2_5166 =
																				CAR(BgL_cdrzd2343zd2_5163);
																			if (SYMBOLP(BgL_carzd2346zd2_5166))
																				{	/* Eval/evmodule.scm 372 */
																					if (BgL_classpz00_4235)
																						{	/* Eval/evmodule.scm 353 */
																							return
																								BGl_evcompilezd2errorzd2zz__evcompilez00
																								(BgL_locz00_4237,
																								BGl_string3037z00zz__evmodulez00,
																								BGl_string3066z00zz__evmodulez00,
																								BgL_clausez00_4236);
																						}
																					else
																						{	/* Eval/evmodule.scm 353 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Eval/evmodule.scm 372 */
																					obj_t BgL_carzd2375zd2_5167;

																					BgL_carzd2375zd2_5167 =
																						CAR(((obj_t) BgL_sz00_4238));
																					if (SYMBOLP(BgL_carzd2375zd2_5167))
																						{	/* Eval/evmodule.scm 372 */
																							if (BgL_classpz00_4235)
																								{	/* Eval/evmodule.scm 365 */
																									return BFALSE;
																								}
																							else
																								{	/* Eval/evmodule.scm 366 */
																									obj_t BgL_idz00_5169;

																									BgL_idz00_5169 =
																										BGl_untypezd2identzd2zz__evmodulez00
																										(BgL_carzd2375zd2_5167);
																									{	/* Eval/evmodule.scm 328 */
																										obj_t BgL_gz00_5170;

																										{	/* Eval/evmodule.scm 328 */
																											obj_t BgL_idz00_5171;

																											if (SYMBOLP
																												(BgL_idz00_5169))
																												{	/* Eval/evmodule.scm 328 */
																													BgL_idz00_5171 =
																														BgL_idz00_5169;
																												}
																											else
																												{
																													obj_t BgL_auxz00_6563;

																													BgL_auxz00_6563 =
																														BGl_typezd2errorzd2zz__errorz00
																														(BGl_string3001z00zz__evmodulez00,
																														BINT(13270L),
																														BGl_string3058z00zz__evmodulez00,
																														BGl_string3005z00zz__evmodulez00,
																														BgL_idz00_5169);
																													FAILURE
																														(BgL_auxz00_6563,
																														BFALSE, BFALSE);
																												}
																											{	/* Eval/evmodule.scm 328 */
																												obj_t BgL_v1213z00_5172;

																												BgL_v1213z00_5172 =
																													create_vector(5L);
																												VECTOR_SET
																													(BgL_v1213z00_5172,
																													0L, BINT(2L));
																												VECTOR_SET
																													(BgL_v1213z00_5172,
																													1L, BgL_idz00_5171);
																												VECTOR_SET
																													(BgL_v1213z00_5172,
																													2L, BUNSPEC);
																												VECTOR_SET
																													(BgL_v1213z00_5172,
																													3L, BgL_modz00_4234);
																												VECTOR_SET
																													(BgL_v1213z00_5172,
																													4L, BgL_locz00_4237);
																												BgL_gz00_5170 =
																													BgL_v1213z00_5172;
																											}
																										}
																										{	/* Eval/evmodule.scm 329 */
																											obj_t BgL_auxz00_6574;

																											if (SYMBOLP
																												(BgL_idz00_5169))
																												{	/* Eval/evmodule.scm 329 */
																													BgL_auxz00_6574 =
																														BgL_idz00_5169;
																												}
																											else
																												{
																													obj_t BgL_auxz00_6577;

																													BgL_auxz00_6577 =
																														BGl_typezd2errorzd2zz__errorz00
																														(BGl_string3001z00zz__evmodulez00,
																														BINT(13317L),
																														BGl_string3058z00zz__evmodulez00,
																														BGl_string3005z00zz__evmodulez00,
																														BgL_idz00_5169);
																													FAILURE
																														(BgL_auxz00_6577,
																														BFALSE, BFALSE);
																												}
																											BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																												(BgL_modz00_4234,
																												BgL_auxz00_6574,
																												BgL_gz00_5170,
																												BgL_locz00_4237);
																										}
																										BgL_gz00_5170;
																									}
																									return
																										BGl_markzd2globalz12zc0zz__evmodulez00
																										(BgL_idz00_5169,
																										BgL_modz00_4234, 4L,
																										BgL_locz00_4237);
																								}
																						}
																					else
																						{	/* Eval/evmodule.scm 372 */
																							return
																								BGl_evcompilezd2errorzd2zz__evcompilez00
																								(BgL_locz00_4237,
																								BGl_string3037z00zz__evmodulez00,
																								BGl_string3051z00zz__evmodulez00,
																								BgL_clausez00_4236);
																						}
																				}
																		}
																	else
																		{	/* Eval/evmodule.scm 372 */
																			obj_t BgL_carzd2384zd2_5173;

																			BgL_carzd2384zd2_5173 =
																				CAR(((obj_t) BgL_sz00_4238));
																			if (SYMBOLP(BgL_carzd2384zd2_5173))
																				{	/* Eval/evmodule.scm 372 */
																					if (BgL_classpz00_4235)
																						{	/* Eval/evmodule.scm 365 */
																							return BFALSE;
																						}
																					else
																						{	/* Eval/evmodule.scm 366 */
																							obj_t BgL_idz00_5175;

																							BgL_idz00_5175 =
																								BGl_untypezd2identzd2zz__evmodulez00
																								(BgL_carzd2384zd2_5173);
																							{	/* Eval/evmodule.scm 328 */
																								obj_t BgL_gz00_5176;

																								{	/* Eval/evmodule.scm 328 */
																									obj_t BgL_idz00_5177;

																									if (SYMBOLP(BgL_idz00_5175))
																										{	/* Eval/evmodule.scm 328 */
																											BgL_idz00_5177 =
																												BgL_idz00_5175;
																										}
																									else
																										{
																											obj_t BgL_auxz00_6592;

																											BgL_auxz00_6592 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string3001z00zz__evmodulez00,
																												BINT(13270L),
																												BGl_string3058z00zz__evmodulez00,
																												BGl_string3005z00zz__evmodulez00,
																												BgL_idz00_5175);
																											FAILURE(BgL_auxz00_6592,
																												BFALSE, BFALSE);
																										}
																									{	/* Eval/evmodule.scm 328 */
																										obj_t BgL_v1213z00_5178;

																										BgL_v1213z00_5178 =
																											create_vector(5L);
																										VECTOR_SET
																											(BgL_v1213z00_5178, 0L,
																											BINT(2L));
																										VECTOR_SET
																											(BgL_v1213z00_5178, 1L,
																											BgL_idz00_5177);
																										VECTOR_SET
																											(BgL_v1213z00_5178, 2L,
																											BUNSPEC);
																										VECTOR_SET
																											(BgL_v1213z00_5178, 3L,
																											BgL_modz00_4234);
																										VECTOR_SET
																											(BgL_v1213z00_5178, 4L,
																											BgL_locz00_4237);
																										BgL_gz00_5176 =
																											BgL_v1213z00_5178;
																									}
																								}
																								{	/* Eval/evmodule.scm 329 */
																									obj_t BgL_auxz00_6603;

																									if (SYMBOLP(BgL_idz00_5175))
																										{	/* Eval/evmodule.scm 329 */
																											BgL_auxz00_6603 =
																												BgL_idz00_5175;
																										}
																									else
																										{
																											obj_t BgL_auxz00_6606;

																											BgL_auxz00_6606 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string3001z00zz__evmodulez00,
																												BINT(13317L),
																												BGl_string3058z00zz__evmodulez00,
																												BGl_string3005z00zz__evmodulez00,
																												BgL_idz00_5175);
																											FAILURE(BgL_auxz00_6606,
																												BFALSE, BFALSE);
																										}
																									BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																										(BgL_modz00_4234,
																										BgL_auxz00_6603,
																										BgL_gz00_5176,
																										BgL_locz00_4237);
																								}
																								BgL_gz00_5176;
																							}
																							return
																								BGl_markzd2globalz12zc0zz__evmodulez00
																								(BgL_idz00_5175,
																								BgL_modz00_4234, 4L,
																								BgL_locz00_4237);
																						}
																				}
																			else
																				{	/* Eval/evmodule.scm 372 */
																					return
																						BGl_evcompilezd2errorzd2zz__evcompilez00
																						(BgL_locz00_4237,
																						BGl_string3037z00zz__evmodulez00,
																						BGl_string3051z00zz__evmodulez00,
																						BgL_clausez00_4236);
																				}
																		}
																}
															else
																{	/* Eval/evmodule.scm 372 */
																	obj_t BgL_carzd2391zd2_5179;
																	obj_t BgL_cdrzd2392zd2_5180;

																	BgL_carzd2391zd2_5179 =
																		CAR(((obj_t) BgL_sz00_4238));
																	BgL_cdrzd2392zd2_5180 =
																		CDR(((obj_t) BgL_sz00_4238));
																	{

																		if (
																			(BgL_carzd2391zd2_5179 ==
																				BGl_symbol3068z00zz__evmodulez00))
																			{	/* Eval/evmodule.scm 372 */
																			BgL_kapzd2393zd2_5183:
																				if (PAIRP(BgL_cdrzd2392zd2_5180))
																					{	/* Eval/evmodule.scm 372 */
																						obj_t BgL_carzd2395zd2_5184;

																						BgL_carzd2395zd2_5184 =
																							CAR(BgL_cdrzd2392zd2_5180);
																						if (SYMBOLP(BgL_carzd2395zd2_5184))
																							{	/* Eval/evmodule.scm 372 */
																								BgL_sz00_5092 =
																									BgL_carzd2395zd2_5184;
																								if (BgL_classpz00_4235)
																									{	/* Eval/evmodule.scm 360 */
																										return BFALSE;
																									}
																								else
																									{	/* Eval/evmodule.scm 361 */
																										obj_t BgL_idz00_5093;

																										BgL_idz00_5093 =
																											BGl_untypezd2identzd2zz__evmodulez00
																											(BgL_sz00_5092);
																										{	/* Eval/evmodule.scm 362 */
																											obj_t BgL_arg1484z00_5094;

																											{	/* Eval/evmodule.scm 362 */
																												obj_t
																													BgL_arg1485z00_5095;
																												{	/* Eval/evmodule.scm 362 */
																													obj_t
																														BgL_arg1486z00_5096;
																													{	/* Eval/evmodule.scm 362 */
																														obj_t
																															BgL_arg1487z00_5097;
																														{	/* Eval/evmodule.scm 362 */
																															obj_t
																																BgL_arg1488z00_5098;
																															BgL_arg1488z00_5098
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_evmodulezd2uninitializa7edz75zz__evmodulez00,
																																BNIL);
																															BgL_arg1487z00_5097
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol3039z00zz__evmodulez00,
																																BgL_arg1488z00_5098);
																														}
																														BgL_arg1486z00_5096
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1487z00_5097,
																															BNIL);
																													}
																													BgL_arg1485z00_5095 =
																														MAKE_YOUNG_PAIR
																														(BgL_idz00_5093,
																														BgL_arg1486z00_5096);
																												}
																												BgL_arg1484z00_5094 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol3052z00zz__evmodulez00,
																													BgL_arg1485z00_5095);
																											}
																											{	/* Eval/evmodule.scm 387 */
																												obj_t BgL_expz00_5099;

																												if (CBOOL
																													(BgL_locz00_4237))
																													{	/* Eval/evmodule.scm 388 */
																														obj_t
																															BgL_arg1494z00_5100;
																														obj_t
																															BgL_arg1495z00_5101;
																														BgL_arg1494z00_5100
																															=
																															CAR
																															(BgL_arg1484z00_5094);
																														BgL_arg1495z00_5101
																															=
																															CDR
																															(BgL_arg1484z00_5094);
																														{	/* Eval/evmodule.scm 388 */
																															obj_t
																																BgL_res2349z00_5102;
																															BgL_res2349z00_5102
																																=
																																MAKE_YOUNG_EPAIR
																																(BgL_arg1494z00_5100,
																																BgL_arg1495z00_5101,
																																BgL_locz00_4237);
																															BgL_expz00_5099 =
																																BgL_res2349z00_5102;
																														}
																													}
																												else
																													{	/* Eval/evmodule.scm 387 */
																														BgL_expz00_5099 =
																															BgL_arg1484z00_5094;
																													}
																												BGl_evalz00zz__evalz00
																													(BgL_expz00_5099,
																													BgL_modz00_4234);
																											}
																										}
																										{	/* Eval/evmodule.scm 305 */
																											obj_t BgL_vz00_5103;

																											{	/* Eval/evmodule.scm 305 */
																												obj_t BgL_auxz00_6637;

																												if (SYMBOLP
																													(BgL_idz00_5093))
																													{	/* Eval/evmodule.scm 305 */
																														BgL_auxz00_6637 =
																															BgL_idz00_5093;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_6640;
																														BgL_auxz00_6640 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string3001z00zz__evmodulez00,
																															BINT(12237L),
																															BGl_string3054z00zz__evmodulez00,
																															BGl_string3005z00zz__evmodulez00,
																															BgL_idz00_5093);
																														FAILURE
																															(BgL_auxz00_6640,
																															BFALSE, BFALSE);
																													}
																												BgL_vz00_5103 =
																													BGl_evmodulezd2findzd2globalz00zz__evmodulez00
																													(BgL_modz00_4234,
																													BgL_auxz00_6637);
																											}
																											{	/* Eval/evmodule.scm 306 */
																												bool_t
																													BgL_test3584z00_6645;
																												if (VECTORP
																													(BgL_vz00_5103))
																													{	/* Eval/evmodule.scm 306 */
																														BgL_test3584z00_6645
																															=
																															(VECTOR_LENGTH
																															(BgL_vz00_5103) ==
																															5L);
																													}
																												else
																													{	/* Eval/evmodule.scm 306 */
																														BgL_test3584z00_6645
																															= ((bool_t) 0);
																													}
																												if (BgL_test3584z00_6645)
																													{	/* Eval/evmodule.scm 306 */
																														VECTOR_SET
																															(BgL_vz00_5103,
																															0L, BINT(4L));
																														return
																															BgL_idz00_5093;
																													}
																												else
																													{	/* Eval/evmodule.scm 306 */
																														return
																															BGl_evcompilezd2errorzd2zz__evcompilez00
																															(BgL_locz00_4237,
																															BGl_string3037z00zz__evmodulez00,
																															BGl_string3049z00zz__evmodulez00,
																															BgL_idz00_5093);
																													}
																											}
																										}
																									}
																							}
																						else
																							{	/* Eval/evmodule.scm 372 */
																								obj_t BgL_carzd2402zd2_5185;

																								BgL_carzd2402zd2_5185 =
																									CAR(((obj_t) BgL_sz00_4238));
																								if (SYMBOLP
																									(BgL_carzd2402zd2_5185))
																									{	/* Eval/evmodule.scm 372 */
																										if (BgL_classpz00_4235)
																											{	/* Eval/evmodule.scm 365 */
																												return BFALSE;
																											}
																										else
																											{	/* Eval/evmodule.scm 366 */
																												obj_t BgL_idz00_5187;

																												BgL_idz00_5187 =
																													BGl_untypezd2identzd2zz__evmodulez00
																													(BgL_carzd2402zd2_5185);
																												{	/* Eval/evmodule.scm 328 */
																													obj_t BgL_gz00_5188;

																													{	/* Eval/evmodule.scm 328 */
																														obj_t
																															BgL_idz00_5189;
																														if (SYMBOLP
																															(BgL_idz00_5187))
																															{	/* Eval/evmodule.scm 328 */
																																BgL_idz00_5189 =
																																	BgL_idz00_5187;
																															}
																														else
																															{
																																obj_t
																																	BgL_auxz00_6661;
																																BgL_auxz00_6661
																																	=
																																	BGl_typezd2errorzd2zz__errorz00
																																	(BGl_string3001z00zz__evmodulez00,
																																	BINT(13270L),
																																	BGl_string3067z00zz__evmodulez00,
																																	BGl_string3005z00zz__evmodulez00,
																																	BgL_idz00_5187);
																																FAILURE
																																	(BgL_auxz00_6661,
																																	BFALSE,
																																	BFALSE);
																															}
																														{	/* Eval/evmodule.scm 328 */
																															obj_t
																																BgL_v1213z00_5190;
																															BgL_v1213z00_5190
																																=
																																create_vector
																																(5L);
																															VECTOR_SET
																																(BgL_v1213z00_5190,
																																0L, BINT(2L));
																															VECTOR_SET
																																(BgL_v1213z00_5190,
																																1L,
																																BgL_idz00_5189);
																															VECTOR_SET
																																(BgL_v1213z00_5190,
																																2L, BUNSPEC);
																															VECTOR_SET
																																(BgL_v1213z00_5190,
																																3L,
																																BgL_modz00_4234);
																															VECTOR_SET
																																(BgL_v1213z00_5190,
																																4L,
																																BgL_locz00_4237);
																															BgL_gz00_5188 =
																																BgL_v1213z00_5190;
																														}
																													}
																													{	/* Eval/evmodule.scm 329 */
																														obj_t
																															BgL_auxz00_6672;
																														if (SYMBOLP
																															(BgL_idz00_5187))
																															{	/* Eval/evmodule.scm 329 */
																																BgL_auxz00_6672
																																	=
																																	BgL_idz00_5187;
																															}
																														else
																															{
																																obj_t
																																	BgL_auxz00_6675;
																																BgL_auxz00_6675
																																	=
																																	BGl_typezd2errorzd2zz__errorz00
																																	(BGl_string3001z00zz__evmodulez00,
																																	BINT(13317L),
																																	BGl_string3067z00zz__evmodulez00,
																																	BGl_string3005z00zz__evmodulez00,
																																	BgL_idz00_5187);
																																FAILURE
																																	(BgL_auxz00_6675,
																																	BFALSE,
																																	BFALSE);
																															}
																														BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																															(BgL_modz00_4234,
																															BgL_auxz00_6672,
																															BgL_gz00_5188,
																															BgL_locz00_4237);
																													}
																													BgL_gz00_5188;
																												}
																												return
																													BGl_markzd2globalz12zc0zz__evmodulez00
																													(BgL_idz00_5187,
																													BgL_modz00_4234, 4L,
																													BgL_locz00_4237);
																											}
																									}
																								else
																									{	/* Eval/evmodule.scm 372 */
																										return
																											BGl_evcompilezd2errorzd2zz__evcompilez00
																											(BgL_locz00_4237,
																											BGl_string3037z00zz__evmodulez00,
																											BGl_string3051z00zz__evmodulez00,
																											BgL_clausez00_4236);
																									}
																							}
																					}
																				else
																					{	/* Eval/evmodule.scm 372 */
																						obj_t BgL_carzd2409zd2_5191;

																						BgL_carzd2409zd2_5191 =
																							CAR(((obj_t) BgL_sz00_4238));
																						if (SYMBOLP(BgL_carzd2409zd2_5191))
																							{	/* Eval/evmodule.scm 372 */
																								if (BgL_classpz00_4235)
																									{	/* Eval/evmodule.scm 365 */
																										return BFALSE;
																									}
																								else
																									{	/* Eval/evmodule.scm 366 */
																										obj_t BgL_idz00_5193;

																										BgL_idz00_5193 =
																											BGl_untypezd2identzd2zz__evmodulez00
																											(BgL_carzd2409zd2_5191);
																										{	/* Eval/evmodule.scm 328 */
																											obj_t BgL_gz00_5194;

																											{	/* Eval/evmodule.scm 328 */
																												obj_t BgL_idz00_5195;

																												if (SYMBOLP
																													(BgL_idz00_5193))
																													{	/* Eval/evmodule.scm 328 */
																														BgL_idz00_5195 =
																															BgL_idz00_5193;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_6690;
																														BgL_auxz00_6690 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string3001z00zz__evmodulez00,
																															BINT(13270L),
																															BGl_string3067z00zz__evmodulez00,
																															BGl_string3005z00zz__evmodulez00,
																															BgL_idz00_5193);
																														FAILURE
																															(BgL_auxz00_6690,
																															BFALSE, BFALSE);
																													}
																												{	/* Eval/evmodule.scm 328 */
																													obj_t
																														BgL_v1213z00_5196;
																													BgL_v1213z00_5196 =
																														create_vector(5L);
																													VECTOR_SET
																														(BgL_v1213z00_5196,
																														0L, BINT(2L));
																													VECTOR_SET
																														(BgL_v1213z00_5196,
																														1L, BgL_idz00_5195);
																													VECTOR_SET
																														(BgL_v1213z00_5196,
																														2L, BUNSPEC);
																													VECTOR_SET
																														(BgL_v1213z00_5196,
																														3L,
																														BgL_modz00_4234);
																													VECTOR_SET
																														(BgL_v1213z00_5196,
																														4L,
																														BgL_locz00_4237);
																													BgL_gz00_5194 =
																														BgL_v1213z00_5196;
																												}
																											}
																											{	/* Eval/evmodule.scm 329 */
																												obj_t BgL_auxz00_6701;

																												if (SYMBOLP
																													(BgL_idz00_5193))
																													{	/* Eval/evmodule.scm 329 */
																														BgL_auxz00_6701 =
																															BgL_idz00_5193;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_6704;
																														BgL_auxz00_6704 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string3001z00zz__evmodulez00,
																															BINT(13317L),
																															BGl_string3067z00zz__evmodulez00,
																															BGl_string3005z00zz__evmodulez00,
																															BgL_idz00_5193);
																														FAILURE
																															(BgL_auxz00_6704,
																															BFALSE, BFALSE);
																													}
																												BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																													(BgL_modz00_4234,
																													BgL_auxz00_6701,
																													BgL_gz00_5194,
																													BgL_locz00_4237);
																											}
																											BgL_gz00_5194;
																										}
																										return
																											BGl_markzd2globalz12zc0zz__evmodulez00
																											(BgL_idz00_5193,
																											BgL_modz00_4234, 4L,
																											BgL_locz00_4237);
																									}
																							}
																						else
																							{	/* Eval/evmodule.scm 372 */
																								return
																									BGl_evcompilezd2errorzd2zz__evcompilez00
																									(BgL_locz00_4237,
																									BGl_string3037z00zz__evmodulez00,
																									BGl_string3051z00zz__evmodulez00,
																									BgL_clausez00_4236);
																							}
																					}
																			}
																		else
																			{	/* Eval/evmodule.scm 372 */
																				if (
																					(BgL_carzd2391zd2_5179 ==
																						BGl_symbol3070z00zz__evmodulez00))
																					{	/* Eval/evmodule.scm 372 */
																						goto BgL_kapzd2393zd2_5183;
																					}
																				else
																					{	/* Eval/evmodule.scm 372 */
																						obj_t BgL_carzd2416zd2_5197;

																						BgL_carzd2416zd2_5197 =
																							CAR(((obj_t) BgL_sz00_4238));
																						if (SYMBOLP(BgL_carzd2416zd2_5197))
																							{	/* Eval/evmodule.scm 372 */
																								if (BgL_classpz00_4235)
																									{	/* Eval/evmodule.scm 365 */
																										return BFALSE;
																									}
																								else
																									{	/* Eval/evmodule.scm 366 */
																										obj_t BgL_idz00_5199;

																										BgL_idz00_5199 =
																											BGl_untypezd2identzd2zz__evmodulez00
																											(BgL_carzd2416zd2_5197);
																										{	/* Eval/evmodule.scm 328 */
																											obj_t BgL_gz00_5200;

																											{	/* Eval/evmodule.scm 328 */
																												obj_t BgL_idz00_5201;

																												if (SYMBOLP
																													(BgL_idz00_5199))
																													{	/* Eval/evmodule.scm 328 */
																														BgL_idz00_5201 =
																															BgL_idz00_5199;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_6721;
																														BgL_auxz00_6721 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string3001z00zz__evmodulez00,
																															BINT(13270L),
																															BGl_string3058z00zz__evmodulez00,
																															BGl_string3005z00zz__evmodulez00,
																															BgL_idz00_5199);
																														FAILURE
																															(BgL_auxz00_6721,
																															BFALSE, BFALSE);
																													}
																												{	/* Eval/evmodule.scm 328 */
																													obj_t
																														BgL_v1213z00_5202;
																													BgL_v1213z00_5202 =
																														create_vector(5L);
																													VECTOR_SET
																														(BgL_v1213z00_5202,
																														0L, BINT(2L));
																													VECTOR_SET
																														(BgL_v1213z00_5202,
																														1L, BgL_idz00_5201);
																													VECTOR_SET
																														(BgL_v1213z00_5202,
																														2L, BUNSPEC);
																													VECTOR_SET
																														(BgL_v1213z00_5202,
																														3L,
																														BgL_modz00_4234);
																													VECTOR_SET
																														(BgL_v1213z00_5202,
																														4L,
																														BgL_locz00_4237);
																													BgL_gz00_5200 =
																														BgL_v1213z00_5202;
																												}
																											}
																											{	/* Eval/evmodule.scm 329 */
																												obj_t BgL_auxz00_6732;

																												if (SYMBOLP
																													(BgL_idz00_5199))
																													{	/* Eval/evmodule.scm 329 */
																														BgL_auxz00_6732 =
																															BgL_idz00_5199;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_6735;
																														BgL_auxz00_6735 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string3001z00zz__evmodulez00,
																															BINT(13317L),
																															BGl_string3058z00zz__evmodulez00,
																															BGl_string3005z00zz__evmodulez00,
																															BgL_idz00_5199);
																														FAILURE
																															(BgL_auxz00_6735,
																															BFALSE, BFALSE);
																													}
																												BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																													(BgL_modz00_4234,
																													BgL_auxz00_6732,
																													BgL_gz00_5200,
																													BgL_locz00_4237);
																											}
																											BgL_gz00_5200;
																										}
																										return
																											BGl_markzd2globalz12zc0zz__evmodulez00
																											(BgL_idz00_5199,
																											BgL_modz00_4234, 4L,
																											BgL_locz00_4237);
																									}
																							}
																						else
																							{	/* Eval/evmodule.scm 372 */
																								return
																									BGl_evcompilezd2errorzd2zz__evcompilez00
																									(BgL_locz00_4237,
																									BGl_string3037z00zz__evmodulez00,
																									BGl_string3051z00zz__evmodulez00,
																									BgL_clausez00_4236);
																							}
																					}
																			}
																	}
																}
														}
												}
										}
								}
							else
								{	/* Eval/evmodule.scm 372 */
									return
										BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_4237,
										BGl_string3037z00zz__evmodulez00,
										BGl_string3051z00zz__evmodulez00, BgL_clausez00_4236);
								}
						}
				}
			}
		}

	}



/* evmodule-export */
	obj_t BGl_evmodulezd2exportzd2zz__evmodulez00(obj_t BgL_modz00_86,
		obj_t BgL_clausez00_87, obj_t BgL_locz00_88, bool_t BgL_classpz00_89)
	{
		{	/* Eval/evmodule.scm 395 */
			{	/* Eval/evmodule.scm 444 */
				obj_t BgL_evmodulezd2exportzd2clausez00_4239;

				{
					int BgL_tmpz00_6743;

					BgL_tmpz00_6743 = (int) (3L);
					BgL_evmodulezd2exportzd2clausez00_4239 =
						MAKE_L_PROCEDURE
						(BGl_z62evmodulezd2exportzd2clausez62zz__evmodulez00,
						BgL_tmpz00_6743);
				}
				PROCEDURE_L_SET(BgL_evmodulezd2exportzd2clausez00_4239,
					(int) (0L), BgL_modz00_86);
				PROCEDURE_L_SET(BgL_evmodulezd2exportzd2clausez00_4239,
					(int) (1L), BBOOL(BgL_classpz00_89));
				PROCEDURE_L_SET(BgL_evmodulezd2exportzd2clausez00_4239,
					(int) (2L), BgL_clausez00_87);
				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausez00_87))
					{	/* Eval/evmodule.scm 447 */
						obj_t BgL_arg1497z00_1599;

						{	/* Eval/evmodule.scm 447 */
							obj_t BgL_pairz00_3541;

							if (PAIRP(BgL_clausez00_87))
								{	/* Eval/evmodule.scm 447 */
									BgL_pairz00_3541 = BgL_clausez00_87;
								}
							else
								{
									obj_t BgL_auxz00_6757;

									BgL_auxz00_6757 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(17619L),
										BGl_string3073z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_clausez00_87);
									FAILURE(BgL_auxz00_6757, BFALSE, BFALSE);
								}
							BgL_arg1497z00_1599 = CDR(BgL_pairz00_3541);
						}
						return
							BBOOL(BGl_forzd2eachzf2locz20zz__evmodulez00(BgL_locz00_88,
								BgL_evmodulezd2exportzd2clausez00_4239, BgL_arg1497z00_1599));
					}
				else
					{	/* Eval/evmodule.scm 445 */
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_88,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3074z00zz__evmodulez00, BgL_clausez00_87);
					}
			}
		}

	}



/* &evmodule-export-clause */
	obj_t BGl_z62evmodulezd2exportzd2clausez62zz__evmodulez00(obj_t
		BgL_envz00_4240, obj_t BgL_locz00_4244, obj_t BgL_sz00_4245)
	{
		{	/* Eval/evmodule.scm 444 */
			{	/* Eval/evmodule.scm 444 */
				obj_t BgL_modz00_4241;
				bool_t BgL_classpz00_4242;
				obj_t BgL_clausez00_4243;

				BgL_modz00_4241 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4240, (int) (0L)));
				BgL_classpz00_4242 =
					CBOOL(PROCEDURE_L_REF(BgL_envz00_4240, (int) (1L)));
				BgL_clausez00_4243 = PROCEDURE_L_REF(BgL_envz00_4240, (int) (2L));
				{
					obj_t BgL_claz00_5256;
					obj_t BgL_clausesz00_5257;
					obj_t BgL_claz00_5246;
					obj_t BgL_clausesz00_5247;
					obj_t BgL_claz00_5236;
					obj_t BgL_clausesz00_5237;
					obj_t BgL_sz00_5221;
					obj_t BgL_sz00_5212;

					if (SYMBOLP(BgL_sz00_4245))
						{	/* Eval/evmodule.scm 444 */
							if (BgL_classpz00_4242)
								{	/* Eval/evmodule.scm 399 */
									return BFALSE;
								}
							else
								{	/* Eval/evmodule.scm 400 */
									obj_t BgL_idz00_5266;

									BgL_idz00_5266 =
										BGl_untypezd2identzd2zz__evmodulez00(BgL_sz00_4245);
									{	/* Eval/evmodule.scm 328 */
										obj_t BgL_gz00_5267;

										{	/* Eval/evmodule.scm 328 */
											obj_t BgL_idz00_5268;

											if (SYMBOLP(BgL_idz00_5266))
												{	/* Eval/evmodule.scm 328 */
													BgL_idz00_5268 = BgL_idz00_5266;
												}
											else
												{
													obj_t BgL_auxz00_6779;

													BgL_auxz00_6779 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(13270L),
														BGl_string3080z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00, BgL_idz00_5266);
													FAILURE(BgL_auxz00_6779, BFALSE, BFALSE);
												}
											{	/* Eval/evmodule.scm 328 */
												obj_t BgL_v1213z00_5269;

												BgL_v1213z00_5269 = create_vector(5L);
												VECTOR_SET(BgL_v1213z00_5269, 0L, BINT(2L));
												VECTOR_SET(BgL_v1213z00_5269, 1L, BgL_idz00_5268);
												VECTOR_SET(BgL_v1213z00_5269, 2L, BUNSPEC);
												VECTOR_SET(BgL_v1213z00_5269, 3L, BgL_modz00_4241);
												VECTOR_SET(BgL_v1213z00_5269, 4L, BgL_locz00_4244);
												BgL_gz00_5267 = BgL_v1213z00_5269;
											}
										}
										{	/* Eval/evmodule.scm 329 */
											obj_t BgL_auxz00_6790;

											if (SYMBOLP(BgL_idz00_5266))
												{	/* Eval/evmodule.scm 329 */
													BgL_auxz00_6790 = BgL_idz00_5266;
												}
											else
												{
													obj_t BgL_auxz00_6793;

													BgL_auxz00_6793 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(13317L),
														BGl_string3080z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00, BgL_idz00_5266);
													FAILURE(BgL_auxz00_6793, BFALSE, BFALSE);
												}
											BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
												(BgL_modz00_4241, BgL_auxz00_6790, BgL_gz00_5267,
												BgL_locz00_4244);
										}
										BgL_gz00_5267;
									}
									{	/* Eval/evmodule.scm 381 */
										obj_t BgL_arg1489z00_5270;

										{	/* Eval/evmodule.scm 381 */
											obj_t BgL_arg1490z00_5271;
											obj_t BgL_arg1492z00_5272;

											BgL_arg1490z00_5271 =
												MAKE_YOUNG_PAIR(BgL_idz00_5266, BgL_modz00_4241);
											BgL_arg1492z00_5272 =
												STRUCT_REF(BgL_modz00_4241, (int) (4L));
											BgL_arg1489z00_5270 =
												MAKE_YOUNG_PAIR(BgL_arg1490z00_5271,
												BgL_arg1492z00_5272);
										}
										{	/* Eval/evmodule.scm 129 */
											int BgL_tmpz00_6802;

											BgL_tmpz00_6802 = (int) (4L);
											STRUCT_SET(BgL_modz00_4241, BgL_tmpz00_6802,
												BgL_arg1489z00_5270);
									}}
									{	/* Eval/evmodule.scm 305 */
										obj_t BgL_vz00_5273;

										{	/* Eval/evmodule.scm 305 */
											obj_t BgL_auxz00_6805;

											if (SYMBOLP(BgL_idz00_5266))
												{	/* Eval/evmodule.scm 305 */
													BgL_auxz00_6805 = BgL_idz00_5266;
												}
											else
												{
													obj_t BgL_auxz00_6808;

													BgL_auxz00_6808 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(12237L),
														BGl_string3080z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00, BgL_idz00_5266);
													FAILURE(BgL_auxz00_6808, BFALSE, BFALSE);
												}
											BgL_vz00_5273 =
												BGl_evmodulezd2findzd2globalz00zz__evmodulez00
												(BgL_modz00_4241, BgL_auxz00_6805);
										}
										{	/* Eval/evmodule.scm 306 */
											bool_t BgL_test3606z00_6813;

											if (VECTORP(BgL_vz00_5273))
												{	/* Eval/evmodule.scm 306 */
													BgL_test3606z00_6813 =
														(VECTOR_LENGTH(BgL_vz00_5273) == 5L);
												}
											else
												{	/* Eval/evmodule.scm 306 */
													BgL_test3606z00_6813 = ((bool_t) 0);
												}
											if (BgL_test3606z00_6813)
												{	/* Eval/evmodule.scm 306 */
													VECTOR_SET(BgL_vz00_5273, 0L, BINT(3L));
													return BgL_idz00_5266;
												}
											else
												{	/* Eval/evmodule.scm 306 */
													return
														BGl_evcompilezd2errorzd2zz__evcompilez00
														(BgL_locz00_4244, BGl_string3037z00zz__evmodulez00,
														BGl_string3049z00zz__evmodulez00, BgL_idz00_5266);
												}
										}
									}
								}
						}
					else
						{	/* Eval/evmodule.scm 444 */
							if (PAIRP(BgL_sz00_4245))
								{	/* Eval/evmodule.scm 444 */
									obj_t BgL_cdrzd2461zd2_5274;

									BgL_cdrzd2461zd2_5274 = CDR(((obj_t) BgL_sz00_4245));
									if (
										(CAR(
												((obj_t) BgL_sz00_4245)) ==
											BGl_symbol3056z00zz__evmodulez00))
										{	/* Eval/evmodule.scm 444 */
											if (PAIRP(BgL_cdrzd2461zd2_5274))
												{	/* Eval/evmodule.scm 444 */
													obj_t BgL_carzd2464zd2_5277;

													BgL_carzd2464zd2_5277 = CAR(BgL_cdrzd2461zd2_5274);
													if (SYMBOLP(BgL_carzd2464zd2_5277))
														{	/* Eval/evmodule.scm 444 */
															BgL_claz00_5256 = BgL_carzd2464zd2_5277;
															BgL_clausesz00_5257 = CDR(BgL_cdrzd2461zd2_5274);
															if (BgL_classpz00_4242)
																{	/* Eval/evmodule.scm 406 */
																	obj_t BgL_identsz00_5258;

																	{	/* Eval/evmodule.scm 406 */
																		obj_t BgL_auxz00_6844;
																		obj_t BgL_auxz00_6835;

																		if (PAIRP(BgL_sz00_4245))
																			{	/* Eval/evmodule.scm 406 */
																				BgL_auxz00_6844 = BgL_sz00_4245;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6847;

																				BgL_auxz00_6847 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(16228L),
																					BGl_string3079z00zz__evmodulez00,
																					BGl_string3003z00zz__evmodulez00,
																					BgL_sz00_4245);
																				FAILURE(BgL_auxz00_6847, BFALSE,
																					BFALSE);
																			}
																		{	/* Eval/evmodule.scm 406 */
																			bool_t BgL_test3613z00_6836;

																			if (PAIRP(BgL_clausesz00_5257))
																				{	/* Eval/evmodule.scm 406 */
																					BgL_test3613z00_6836 = ((bool_t) 1);
																				}
																			else
																				{	/* Eval/evmodule.scm 406 */
																					BgL_test3613z00_6836 =
																						NULLP(BgL_clausesz00_5257);
																				}
																			if (BgL_test3613z00_6836)
																				{	/* Eval/evmodule.scm 406 */
																					BgL_auxz00_6835 = BgL_clausesz00_5257;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6840;

																					BgL_auxz00_6840 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3001z00zz__evmodulez00,
																						BINT(16220L),
																						BGl_string3079z00zz__evmodulez00,
																						BGl_string3059z00zz__evmodulez00,
																						BgL_clausesz00_5257);
																					FAILURE(BgL_auxz00_6840, BFALSE,
																						BFALSE);
																				}
																		}
																		BgL_identsz00_5258 =
																			BGl_evalzd2classzd2zz__evobjectz00
																			(BgL_claz00_5256, ((bool_t) 0),
																			BgL_auxz00_6835, BgL_auxz00_6844,
																			BgL_modz00_4241);
																	}
																	{
																		obj_t BgL_l1154z00_5261;

																		BgL_l1154z00_5261 = BgL_identsz00_5258;
																	BgL_zc3z04anonymousza31602ze3z87_5260:
																		if (PAIRP(BgL_l1154z00_5261))
																			{	/* Eval/evmodule.scm 407 */
																				{	/* Eval/evmodule.scm 407 */
																					obj_t BgL_iz00_5262;

																					BgL_iz00_5262 =
																						CAR(BgL_l1154z00_5261);
																					{	/* Eval/evmodule.scm 381 */
																						obj_t BgL_arg1489z00_5263;

																						{	/* Eval/evmodule.scm 381 */
																							obj_t BgL_arg1490z00_5264;
																							obj_t BgL_arg1492z00_5265;

																							BgL_arg1490z00_5264 =
																								MAKE_YOUNG_PAIR(BgL_iz00_5262,
																								BgL_modz00_4241);
																							BgL_arg1492z00_5265 =
																								STRUCT_REF(BgL_modz00_4241,
																								(int) (4L));
																							BgL_arg1489z00_5263 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1490z00_5264,
																								BgL_arg1492z00_5265);
																						}
																						{	/* Eval/evmodule.scm 129 */
																							int BgL_tmpz00_6859;

																							BgL_tmpz00_6859 = (int) (4L);
																							STRUCT_SET(BgL_modz00_4241,
																								BgL_tmpz00_6859,
																								BgL_arg1489z00_5263);
																				}}}
																				{
																					obj_t BgL_l1154z00_6862;

																					BgL_l1154z00_6862 =
																						CDR(BgL_l1154z00_5261);
																					BgL_l1154z00_5261 = BgL_l1154z00_6862;
																					goto
																						BgL_zc3z04anonymousza31602ze3z87_5260;
																				}
																			}
																		else
																			{	/* Eval/evmodule.scm 407 */
																				if (NULLP(BgL_l1154z00_5261))
																					{	/* Eval/evmodule.scm 407 */
																						return BTRUE;
																					}
																				else
																					{	/* Eval/evmodule.scm 407 */
																						return
																							BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
																							(BGl_string3044z00zz__evmodulez00,
																							BGl_string3034z00zz__evmodulez00,
																							BgL_l1154z00_5261,
																							BGl_string3001z00zz__evmodulez00,
																							BINT(16239L));
																					}
																			}
																	}
																}
															else
																{	/* Eval/evmodule.scm 405 */
																	return BFALSE;
																}
														}
													else
														{	/* Eval/evmodule.scm 444 */
															obj_t BgL_carzd2514zd2_5278;

															BgL_carzd2514zd2_5278 =
																CAR(((obj_t) BgL_sz00_4245));
															if (SYMBOLP(BgL_carzd2514zd2_5278))
																{	/* Eval/evmodule.scm 444 */
																	BgL_sz00_5212 = BgL_carzd2514zd2_5278;
																BgL_tagzd2449zd2_5206:
																	if (BgL_classpz00_4242)
																		{	/* Eval/evmodule.scm 436 */
																			return BFALSE;
																		}
																	else
																		{	/* Eval/evmodule.scm 437 */
																			obj_t BgL_idz00_5213;

																			BgL_idz00_5213 =
																				BGl_untypezd2identzd2zz__evmodulez00
																				(BgL_sz00_5212);
																			{	/* Eval/evmodule.scm 328 */
																				obj_t BgL_gz00_5214;

																				{	/* Eval/evmodule.scm 328 */
																					obj_t BgL_idz00_5215;

																					if (SYMBOLP(BgL_idz00_5213))
																						{	/* Eval/evmodule.scm 328 */
																							BgL_idz00_5215 = BgL_idz00_5213;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6877;

																							BgL_auxz00_6877 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(13270L),
																								BGl_string3075z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5213);
																							FAILURE(BgL_auxz00_6877, BFALSE,
																								BFALSE);
																						}
																					{	/* Eval/evmodule.scm 328 */
																						obj_t BgL_v1213z00_5216;

																						BgL_v1213z00_5216 =
																							create_vector(5L);
																						VECTOR_SET(BgL_v1213z00_5216, 0L,
																							BINT(2L));
																						VECTOR_SET(BgL_v1213z00_5216, 1L,
																							BgL_idz00_5215);
																						VECTOR_SET(BgL_v1213z00_5216, 2L,
																							BUNSPEC);
																						VECTOR_SET(BgL_v1213z00_5216, 3L,
																							BgL_modz00_4241);
																						VECTOR_SET(BgL_v1213z00_5216, 4L,
																							BgL_locz00_4244);
																						BgL_gz00_5214 = BgL_v1213z00_5216;
																					}
																				}
																				{	/* Eval/evmodule.scm 329 */
																					obj_t BgL_auxz00_6888;

																					if (SYMBOLP(BgL_idz00_5213))
																						{	/* Eval/evmodule.scm 329 */
																							BgL_auxz00_6888 = BgL_idz00_5213;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6891;

																							BgL_auxz00_6891 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(13317L),
																								BGl_string3075z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5213);
																							FAILURE(BgL_auxz00_6891, BFALSE,
																								BFALSE);
																						}
																					BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																						(BgL_modz00_4241, BgL_auxz00_6888,
																						BgL_gz00_5214, BgL_locz00_4244);
																				}
																				BgL_gz00_5214;
																			}
																			{	/* Eval/evmodule.scm 381 */
																				obj_t BgL_arg1489z00_5217;

																				{	/* Eval/evmodule.scm 381 */
																					obj_t BgL_arg1490z00_5218;
																					obj_t BgL_arg1492z00_5219;

																					BgL_arg1490z00_5218 =
																						MAKE_YOUNG_PAIR(BgL_idz00_5213,
																						BgL_modz00_4241);
																					BgL_arg1492z00_5219 =
																						STRUCT_REF(BgL_modz00_4241,
																						(int) (4L));
																					BgL_arg1489z00_5217 =
																						MAKE_YOUNG_PAIR(BgL_arg1490z00_5218,
																						BgL_arg1492z00_5219);
																				}
																				{	/* Eval/evmodule.scm 129 */
																					int BgL_tmpz00_6900;

																					BgL_tmpz00_6900 = (int) (4L);
																					STRUCT_SET(BgL_modz00_4241,
																						BgL_tmpz00_6900,
																						BgL_arg1489z00_5217);
																			}}
																			{	/* Eval/evmodule.scm 305 */
																				obj_t BgL_vz00_5220;

																				{	/* Eval/evmodule.scm 305 */
																					obj_t BgL_auxz00_6903;

																					if (SYMBOLP(BgL_idz00_5213))
																						{	/* Eval/evmodule.scm 305 */
																							BgL_auxz00_6903 = BgL_idz00_5213;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6906;

																							BgL_auxz00_6906 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(12237L),
																								BGl_string3075z00zz__evmodulez00,
																								BGl_string3005z00zz__evmodulez00,
																								BgL_idz00_5213);
																							FAILURE(BgL_auxz00_6906, BFALSE,
																								BFALSE);
																						}
																					BgL_vz00_5220 =
																						BGl_evmodulezd2findzd2globalz00zz__evmodulez00
																						(BgL_modz00_4241, BgL_auxz00_6903);
																				}
																				{	/* Eval/evmodule.scm 306 */
																					bool_t BgL_test3623z00_6911;

																					if (VECTORP(BgL_vz00_5220))
																						{	/* Eval/evmodule.scm 306 */
																							BgL_test3623z00_6911 =
																								(VECTOR_LENGTH(BgL_vz00_5220) ==
																								5L);
																						}
																					else
																						{	/* Eval/evmodule.scm 306 */
																							BgL_test3623z00_6911 =
																								((bool_t) 0);
																						}
																					if (BgL_test3623z00_6911)
																						{	/* Eval/evmodule.scm 306 */
																							VECTOR_SET(BgL_vz00_5220, 0L,
																								BINT(4L));
																							return BgL_idz00_5213;
																						}
																					else
																						{	/* Eval/evmodule.scm 306 */
																							return
																								BGl_evcompilezd2errorzd2zz__evcompilez00
																								(BgL_locz00_4244,
																								BGl_string3037z00zz__evmodulez00,
																								BGl_string3049z00zz__evmodulez00,
																								BgL_idz00_5213);
																						}
																				}
																			}
																		}
																}
															else
																{	/* Eval/evmodule.scm 444 */
																	return
																		BGl_evcompilezd2errorzd2zz__evcompilez00
																		(BgL_locz00_4244,
																		BGl_string3037z00zz__evmodulez00,
																		BGl_string3074z00zz__evmodulez00,
																		BgL_clausez00_4243);
																}
														}
												}
											else
												{	/* Eval/evmodule.scm 444 */
													obj_t BgL_carzd2541zd2_5280;

													BgL_carzd2541zd2_5280 = CAR(((obj_t) BgL_sz00_4245));
													if (SYMBOLP(BgL_carzd2541zd2_5280))
														{
															obj_t BgL_sz00_6924;

															BgL_sz00_6924 = BgL_carzd2541zd2_5280;
															BgL_sz00_5212 = BgL_sz00_6924;
															goto BgL_tagzd2449zd2_5206;
														}
													else
														{	/* Eval/evmodule.scm 444 */
															return
																BGl_evcompilezd2errorzd2zz__evcompilez00
																(BgL_locz00_4244,
																BGl_string3037z00zz__evmodulez00,
																BGl_string3074z00zz__evmodulez00,
																BgL_clausez00_4243);
														}
												}
										}
									else
										{	/* Eval/evmodule.scm 444 */
											obj_t BgL_cdrzd2552zd2_5282;

											BgL_cdrzd2552zd2_5282 = CDR(((obj_t) BgL_sz00_4245));
											if (
												(CAR(
														((obj_t) BgL_sz00_4245)) ==
													BGl_symbol3060z00zz__evmodulez00))
												{	/* Eval/evmodule.scm 444 */
													if (PAIRP(BgL_cdrzd2552zd2_5282))
														{	/* Eval/evmodule.scm 444 */
															obj_t BgL_carzd2555zd2_5285;

															BgL_carzd2555zd2_5285 =
																CAR(BgL_cdrzd2552zd2_5282);
															if (SYMBOLP(BgL_carzd2555zd2_5285))
																{	/* Eval/evmodule.scm 444 */
																	BgL_claz00_5246 = BgL_carzd2555zd2_5285;
																	BgL_clausesz00_5247 =
																		CDR(BgL_cdrzd2552zd2_5282);
																	if (BgL_classpz00_4242)
																		{	/* Eval/evmodule.scm 410 */
																			obj_t BgL_identsz00_5248;

																			{	/* Eval/evmodule.scm 410 */
																				obj_t BgL_auxz00_6947;
																				obj_t BgL_auxz00_6938;

																				if (PAIRP(BgL_sz00_4245))
																					{	/* Eval/evmodule.scm 410 */
																						BgL_auxz00_6947 = BgL_sz00_4245;
																					}
																				else
																					{
																						obj_t BgL_auxz00_6950;

																						BgL_auxz00_6950 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string3001z00zz__evmodulez00,
																							BINT(16416L),
																							BGl_string3078z00zz__evmodulez00,
																							BGl_string3003z00zz__evmodulez00,
																							BgL_sz00_4245);
																						FAILURE(BgL_auxz00_6950, BFALSE,
																							BFALSE);
																					}
																				{	/* Eval/evmodule.scm 410 */
																					bool_t BgL_test3630z00_6939;

																					if (PAIRP(BgL_clausesz00_5247))
																						{	/* Eval/evmodule.scm 410 */
																							BgL_test3630z00_6939 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Eval/evmodule.scm 410 */
																							BgL_test3630z00_6939 =
																								NULLP(BgL_clausesz00_5247);
																						}
																					if (BgL_test3630z00_6939)
																						{	/* Eval/evmodule.scm 410 */
																							BgL_auxz00_6938 =
																								BgL_clausesz00_5247;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6943;

																							BgL_auxz00_6943 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(16408L),
																								BGl_string3078z00zz__evmodulez00,
																								BGl_string3059z00zz__evmodulez00,
																								BgL_clausesz00_5247);
																							FAILURE(BgL_auxz00_6943, BFALSE,
																								BFALSE);
																						}
																				}
																				BgL_identsz00_5248 =
																					BGl_evalzd2classzd2zz__evobjectz00
																					(BgL_claz00_5246, ((bool_t) 0),
																					BgL_auxz00_6938, BgL_auxz00_6947,
																					BgL_modz00_4241);
																			}
																			{
																				obj_t BgL_l1156z00_5251;

																				BgL_l1156z00_5251 = BgL_identsz00_5248;
																			BgL_zc3z04anonymousza31607ze3z87_5250:
																				if (PAIRP(BgL_l1156z00_5251))
																					{	/* Eval/evmodule.scm 411 */
																						{	/* Eval/evmodule.scm 411 */
																							obj_t BgL_iz00_5252;

																							BgL_iz00_5252 =
																								CAR(BgL_l1156z00_5251);
																							{	/* Eval/evmodule.scm 381 */
																								obj_t BgL_arg1489z00_5253;

																								{	/* Eval/evmodule.scm 381 */
																									obj_t BgL_arg1490z00_5254;
																									obj_t BgL_arg1492z00_5255;

																									BgL_arg1490z00_5254 =
																										MAKE_YOUNG_PAIR
																										(BgL_iz00_5252,
																										BgL_modz00_4241);
																									BgL_arg1492z00_5255 =
																										STRUCT_REF(BgL_modz00_4241,
																										(int) (4L));
																									BgL_arg1489z00_5253 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1490z00_5254,
																										BgL_arg1492z00_5255);
																								}
																								{	/* Eval/evmodule.scm 129 */
																									int BgL_tmpz00_6962;

																									BgL_tmpz00_6962 = (int) (4L);
																									STRUCT_SET(BgL_modz00_4241,
																										BgL_tmpz00_6962,
																										BgL_arg1489z00_5253);
																						}}}
																						{
																							obj_t BgL_l1156z00_6965;

																							BgL_l1156z00_6965 =
																								CDR(BgL_l1156z00_5251);
																							BgL_l1156z00_5251 =
																								BgL_l1156z00_6965;
																							goto
																								BgL_zc3z04anonymousza31607ze3z87_5250;
																						}
																					}
																				else
																					{	/* Eval/evmodule.scm 411 */
																						if (NULLP(BgL_l1156z00_5251))
																							{	/* Eval/evmodule.scm 411 */
																								return BTRUE;
																							}
																						else
																							{	/* Eval/evmodule.scm 411 */
																								return
																									BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
																									(BGl_string3044z00zz__evmodulez00,
																									BGl_string3034z00zz__evmodulez00,
																									BgL_l1156z00_5251,
																									BGl_string3001z00zz__evmodulez00,
																									BINT(16427L));
																							}
																					}
																			}
																		}
																	else
																		{	/* Eval/evmodule.scm 409 */
																			return BFALSE;
																		}
																}
															else
																{	/* Eval/evmodule.scm 444 */
																	obj_t BgL_carzd2600zd2_5286;

																	BgL_carzd2600zd2_5286 =
																		CAR(((obj_t) BgL_sz00_4245));
																	if (SYMBOLP(BgL_carzd2600zd2_5286))
																		{
																			obj_t BgL_sz00_6976;

																			BgL_sz00_6976 = BgL_carzd2600zd2_5286;
																			BgL_sz00_5212 = BgL_sz00_6976;
																			goto BgL_tagzd2449zd2_5206;
																		}
																	else
																		{	/* Eval/evmodule.scm 444 */
																			return
																				BGl_evcompilezd2errorzd2zz__evcompilez00
																				(BgL_locz00_4244,
																				BGl_string3037z00zz__evmodulez00,
																				BGl_string3074z00zz__evmodulez00,
																				BgL_clausez00_4243);
																		}
																}
														}
													else
														{	/* Eval/evmodule.scm 444 */
															obj_t BgL_carzd2622zd2_5288;

															BgL_carzd2622zd2_5288 =
																CAR(((obj_t) BgL_sz00_4245));
															if (SYMBOLP(BgL_carzd2622zd2_5288))
																{
																	obj_t BgL_sz00_6982;

																	BgL_sz00_6982 = BgL_carzd2622zd2_5288;
																	BgL_sz00_5212 = BgL_sz00_6982;
																	goto BgL_tagzd2449zd2_5206;
																}
															else
																{	/* Eval/evmodule.scm 444 */
																	return
																		BGl_evcompilezd2errorzd2zz__evcompilez00
																		(BgL_locz00_4244,
																		BGl_string3037z00zz__evmodulez00,
																		BGl_string3074z00zz__evmodulez00,
																		BgL_clausez00_4243);
																}
														}
												}
											else
												{	/* Eval/evmodule.scm 444 */
													obj_t BgL_cdrzd2633zd2_5290;

													BgL_cdrzd2633zd2_5290 = CDR(((obj_t) BgL_sz00_4245));
													if (
														(CAR(
																((obj_t) BgL_sz00_4245)) ==
															BGl_symbol3062z00zz__evmodulez00))
														{	/* Eval/evmodule.scm 444 */
															if (PAIRP(BgL_cdrzd2633zd2_5290))
																{	/* Eval/evmodule.scm 444 */
																	obj_t BgL_carzd2636zd2_5293;

																	BgL_carzd2636zd2_5293 =
																		CAR(BgL_cdrzd2633zd2_5290);
																	if (SYMBOLP(BgL_carzd2636zd2_5293))
																		{	/* Eval/evmodule.scm 444 */
																			BgL_claz00_5236 = BgL_carzd2636zd2_5293;
																			BgL_clausesz00_5237 =
																				CDR(BgL_cdrzd2633zd2_5290);
																			if (BgL_classpz00_4242)
																				{	/* Eval/evmodule.scm 414 */
																					obj_t BgL_identsz00_5238;

																					{	/* Eval/evmodule.scm 414 */
																						obj_t BgL_auxz00_7005;
																						obj_t BgL_auxz00_6996;

																						if (PAIRP(BgL_sz00_4245))
																							{	/* Eval/evmodule.scm 414 */
																								BgL_auxz00_7005 = BgL_sz00_4245;
																							}
																						else
																							{
																								obj_t BgL_auxz00_7008;

																								BgL_auxz00_7008 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3001z00zz__evmodulez00,
																									BINT(16607L),
																									BGl_string3077z00zz__evmodulez00,
																									BGl_string3003z00zz__evmodulez00,
																									BgL_sz00_4245);
																								FAILURE(BgL_auxz00_7008, BFALSE,
																									BFALSE);
																							}
																						{	/* Eval/evmodule.scm 414 */
																							bool_t BgL_test3641z00_6997;

																							if (PAIRP(BgL_clausesz00_5237))
																								{	/* Eval/evmodule.scm 414 */
																									BgL_test3641z00_6997 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Eval/evmodule.scm 414 */
																									BgL_test3641z00_6997 =
																										NULLP(BgL_clausesz00_5237);
																								}
																							if (BgL_test3641z00_6997)
																								{	/* Eval/evmodule.scm 414 */
																									BgL_auxz00_6996 =
																										BgL_clausesz00_5237;
																								}
																							else
																								{
																									obj_t BgL_auxz00_7001;

																									BgL_auxz00_7001 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(16599L),
																										BGl_string3077z00zz__evmodulez00,
																										BGl_string3059z00zz__evmodulez00,
																										BgL_clausesz00_5237);
																									FAILURE(BgL_auxz00_7001,
																										BFALSE, BFALSE);
																								}
																						}
																						BgL_identsz00_5238 =
																							BGl_evalzd2classzd2zz__evobjectz00
																							(BgL_claz00_5236, ((bool_t) 1),
																							BgL_auxz00_6996, BgL_auxz00_7005,
																							BgL_modz00_4241);
																					}
																					{
																						obj_t BgL_l1158z00_5241;

																						BgL_l1158z00_5241 =
																							BgL_identsz00_5238;
																					BgL_zc3z04anonymousza31611ze3z87_5240:
																						if (PAIRP
																							(BgL_l1158z00_5241))
																							{	/* Eval/evmodule.scm 415 */
																								{	/* Eval/evmodule.scm 415 */
																									obj_t BgL_iz00_5242;

																									BgL_iz00_5242 =
																										CAR(BgL_l1158z00_5241);
																									{	/* Eval/evmodule.scm 381 */
																										obj_t BgL_arg1489z00_5243;

																										{	/* Eval/evmodule.scm 381 */
																											obj_t BgL_arg1490z00_5244;
																											obj_t BgL_arg1492z00_5245;

																											BgL_arg1490z00_5244 =
																												MAKE_YOUNG_PAIR
																												(BgL_iz00_5242,
																												BgL_modz00_4241);
																											BgL_arg1492z00_5245 =
																												STRUCT_REF
																												(BgL_modz00_4241,
																												(int) (4L));
																											BgL_arg1489z00_5243 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1490z00_5244,
																												BgL_arg1492z00_5245);
																										}
																										{	/* Eval/evmodule.scm 129 */
																											int BgL_tmpz00_7020;

																											BgL_tmpz00_7020 =
																												(int) (4L);
																											STRUCT_SET
																												(BgL_modz00_4241,
																												BgL_tmpz00_7020,
																												BgL_arg1489z00_5243);
																								}}}
																								{
																									obj_t BgL_l1158z00_7023;

																									BgL_l1158z00_7023 =
																										CDR(BgL_l1158z00_5241);
																									BgL_l1158z00_5241 =
																										BgL_l1158z00_7023;
																									goto
																										BgL_zc3z04anonymousza31611ze3z87_5240;
																								}
																							}
																						else
																							{	/* Eval/evmodule.scm 415 */
																								if (NULLP(BgL_l1158z00_5241))
																									{	/* Eval/evmodule.scm 415 */
																										return BTRUE;
																									}
																								else
																									{	/* Eval/evmodule.scm 415 */
																										return
																											BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
																											(BGl_string3044z00zz__evmodulez00,
																											BGl_string3034z00zz__evmodulez00,
																											BgL_l1158z00_5241,
																											BGl_string3001z00zz__evmodulez00,
																											BINT(16618L));
																									}
																							}
																					}
																				}
																			else
																				{	/* Eval/evmodule.scm 413 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Eval/evmodule.scm 444 */
																			obj_t BgL_carzd2676zd2_5294;

																			BgL_carzd2676zd2_5294 =
																				CAR(((obj_t) BgL_sz00_4245));
																			if (SYMBOLP(BgL_carzd2676zd2_5294))
																				{
																					obj_t BgL_sz00_7034;

																					BgL_sz00_7034 = BgL_carzd2676zd2_5294;
																					BgL_sz00_5212 = BgL_sz00_7034;
																					goto BgL_tagzd2449zd2_5206;
																				}
																			else
																				{	/* Eval/evmodule.scm 444 */
																					return
																						BGl_evcompilezd2errorzd2zz__evcompilez00
																						(BgL_locz00_4244,
																						BGl_string3037z00zz__evmodulez00,
																						BGl_string3074z00zz__evmodulez00,
																						BgL_clausez00_4243);
																				}
																		}
																}
															else
																{	/* Eval/evmodule.scm 444 */
																	obj_t BgL_carzd2693zd2_5296;

																	BgL_carzd2693zd2_5296 =
																		CAR(((obj_t) BgL_sz00_4245));
																	if (SYMBOLP(BgL_carzd2693zd2_5296))
																		{
																			obj_t BgL_sz00_7040;

																			BgL_sz00_7040 = BgL_carzd2693zd2_5296;
																			BgL_sz00_5212 = BgL_sz00_7040;
																			goto BgL_tagzd2449zd2_5206;
																		}
																	else
																		{	/* Eval/evmodule.scm 444 */
																			return
																				BGl_evcompilezd2errorzd2zz__evcompilez00
																				(BgL_locz00_4244,
																				BGl_string3037z00zz__evmodulez00,
																				BGl_string3074z00zz__evmodulez00,
																				BgL_clausez00_4243);
																		}
																}
														}
													else
														{	/* Eval/evmodule.scm 444 */
															obj_t BgL_cdrzd2704zd2_5298;

															BgL_cdrzd2704zd2_5298 =
																CDR(((obj_t) BgL_sz00_4245));
															if (
																(CAR(
																		((obj_t) BgL_sz00_4245)) ==
																	BGl_symbol3064z00zz__evmodulez00))
																{	/* Eval/evmodule.scm 444 */
																	if (PAIRP(BgL_cdrzd2704zd2_5298))
																		{	/* Eval/evmodule.scm 444 */
																			obj_t BgL_carzd2707zd2_5301;

																			BgL_carzd2707zd2_5301 =
																				CAR(BgL_cdrzd2704zd2_5298);
																			if (SYMBOLP(BgL_carzd2707zd2_5301))
																				{	/* Eval/evmodule.scm 444 */
																					if (BgL_classpz00_4242)
																						{	/* Eval/evmodule.scm 417 */
																							return
																								BGl_evcompilezd2errorzd2zz__evcompilez00
																								(BgL_locz00_4244,
																								BGl_string3037z00zz__evmodulez00,
																								BGl_string3066z00zz__evmodulez00,
																								BgL_clausez00_4243);
																						}
																					else
																						{	/* Eval/evmodule.scm 417 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Eval/evmodule.scm 444 */
																					obj_t BgL_carzd2742zd2_5302;

																					BgL_carzd2742zd2_5302 =
																						CAR(((obj_t) BgL_sz00_4245));
																					if (SYMBOLP(BgL_carzd2742zd2_5302))
																						{
																							obj_t BgL_sz00_7059;

																							BgL_sz00_7059 =
																								BgL_carzd2742zd2_5302;
																							BgL_sz00_5212 = BgL_sz00_7059;
																							goto BgL_tagzd2449zd2_5206;
																						}
																					else
																						{	/* Eval/evmodule.scm 444 */
																							return
																								BGl_evcompilezd2errorzd2zz__evcompilez00
																								(BgL_locz00_4244,
																								BGl_string3037z00zz__evmodulez00,
																								BGl_string3074z00zz__evmodulez00,
																								BgL_clausez00_4243);
																						}
																				}
																		}
																	else
																		{	/* Eval/evmodule.scm 444 */
																			obj_t BgL_carzd2754zd2_5304;

																			BgL_carzd2754zd2_5304 =
																				CAR(((obj_t) BgL_sz00_4245));
																			if (SYMBOLP(BgL_carzd2754zd2_5304))
																				{
																					obj_t BgL_sz00_7065;

																					BgL_sz00_7065 = BgL_carzd2754zd2_5304;
																					BgL_sz00_5212 = BgL_sz00_7065;
																					goto BgL_tagzd2449zd2_5206;
																				}
																			else
																				{	/* Eval/evmodule.scm 444 */
																					return
																						BGl_evcompilezd2errorzd2zz__evcompilez00
																						(BgL_locz00_4244,
																						BGl_string3037z00zz__evmodulez00,
																						BGl_string3074z00zz__evmodulez00,
																						BgL_clausez00_4243);
																				}
																		}
																}
															else
																{	/* Eval/evmodule.scm 444 */
																	obj_t BgL_carzd2761zd2_5306;
																	obj_t BgL_cdrzd2762zd2_5307;

																	BgL_carzd2761zd2_5306 =
																		CAR(((obj_t) BgL_sz00_4245));
																	BgL_cdrzd2762zd2_5307 =
																		CDR(((obj_t) BgL_sz00_4245));
																	{

																		if (
																			(BgL_carzd2761zd2_5306 ==
																				BGl_symbol3068z00zz__evmodulez00))
																			{	/* Eval/evmodule.scm 444 */
																			BgL_kapzd2763zd2_5310:
																				if (PAIRP(BgL_cdrzd2762zd2_5307))
																					{	/* Eval/evmodule.scm 444 */
																						obj_t BgL_carzd2765zd2_5311;

																						BgL_carzd2765zd2_5311 =
																							CAR(BgL_cdrzd2762zd2_5307);
																						if (SYMBOLP(BgL_carzd2765zd2_5311))
																							{	/* Eval/evmodule.scm 444 */
																								BgL_sz00_5221 =
																									BgL_carzd2765zd2_5311;
																								if (BgL_classpz00_4242)
																									{	/* Eval/evmodule.scm 424 */
																										return BFALSE;
																									}
																								else
																									{	/* Eval/evmodule.scm 425 */
																										obj_t BgL_idz00_5222;

																										BgL_idz00_5222 =
																											BGl_untypezd2identzd2zz__evmodulez00
																											(BgL_sz00_5221);
																										{	/* Eval/evmodule.scm 381 */
																											obj_t BgL_arg1489z00_5223;

																											{	/* Eval/evmodule.scm 381 */
																												obj_t
																													BgL_arg1490z00_5224;
																												obj_t
																													BgL_arg1492z00_5225;
																												BgL_arg1490z00_5224 =
																													MAKE_YOUNG_PAIR
																													(BgL_idz00_5222,
																													BgL_modz00_4241);
																												BgL_arg1492z00_5225 =
																													STRUCT_REF
																													(BgL_modz00_4241,
																													(int) (4L));
																												BgL_arg1489z00_5223 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1490z00_5224,
																													BgL_arg1492z00_5225);
																											}
																											{	/* Eval/evmodule.scm 129 */
																												int BgL_tmpz00_7084;

																												BgL_tmpz00_7084 =
																													(int) (4L);
																												STRUCT_SET
																													(BgL_modz00_4241,
																													BgL_tmpz00_7084,
																													BgL_arg1489z00_5223);
																										}}
																										{	/* Eval/evmodule.scm 427 */
																											obj_t BgL_arg1615z00_5226;

																											{	/* Eval/evmodule.scm 427 */
																												obj_t
																													BgL_arg1616z00_5227;
																												{	/* Eval/evmodule.scm 427 */
																													obj_t
																														BgL_arg1617z00_5228;
																													{	/* Eval/evmodule.scm 427 */
																														obj_t
																															BgL_arg1618z00_5229;
																														{	/* Eval/evmodule.scm 427 */
																															obj_t
																																BgL_arg1619z00_5230;
																															BgL_arg1619z00_5230
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_evmodulezd2uninitializa7edz75zz__evmodulez00,
																																BNIL);
																															BgL_arg1618z00_5229
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol3039z00zz__evmodulez00,
																																BgL_arg1619z00_5230);
																														}
																														BgL_arg1617z00_5228
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1618z00_5229,
																															BNIL);
																													}
																													BgL_arg1616z00_5227 =
																														MAKE_YOUNG_PAIR
																														(BgL_idz00_5222,
																														BgL_arg1617z00_5228);
																												}
																												BgL_arg1615z00_5226 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol3052z00zz__evmodulez00,
																													BgL_arg1616z00_5227);
																											}
																											{	/* Eval/evmodule.scm 387 */
																												obj_t BgL_expz00_5231;

																												if (CBOOL
																													(BgL_locz00_4244))
																													{	/* Eval/evmodule.scm 388 */
																														obj_t
																															BgL_arg1494z00_5232;
																														obj_t
																															BgL_arg1495z00_5233;
																														BgL_arg1494z00_5232
																															=
																															CAR
																															(BgL_arg1615z00_5226);
																														BgL_arg1495z00_5233
																															=
																															CDR
																															(BgL_arg1615z00_5226);
																														{	/* Eval/evmodule.scm 388 */
																															obj_t
																																BgL_res2353z00_5234;
																															BgL_res2353z00_5234
																																=
																																MAKE_YOUNG_EPAIR
																																(BgL_arg1494z00_5232,
																																BgL_arg1495z00_5233,
																																BgL_locz00_4244);
																															BgL_expz00_5231 =
																																BgL_res2353z00_5234;
																														}
																													}
																												else
																													{	/* Eval/evmodule.scm 387 */
																														BgL_expz00_5231 =
																															BgL_arg1615z00_5226;
																													}
																												BGl_evalz00zz__evalz00
																													(BgL_expz00_5231,
																													BgL_modz00_4241);
																											}
																										}
																										{	/* Eval/evmodule.scm 305 */
																											obj_t BgL_vz00_5235;

																											{	/* Eval/evmodule.scm 305 */
																												obj_t BgL_auxz00_7098;

																												if (SYMBOLP
																													(BgL_idz00_5222))
																													{	/* Eval/evmodule.scm 305 */
																														BgL_auxz00_7098 =
																															BgL_idz00_5222;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_7101;
																														BgL_auxz00_7101 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string3001z00zz__evmodulez00,
																															BINT(12237L),
																															BGl_string3076z00zz__evmodulez00,
																															BGl_string3005z00zz__evmodulez00,
																															BgL_idz00_5222);
																														FAILURE
																															(BgL_auxz00_7101,
																															BFALSE, BFALSE);
																													}
																												BgL_vz00_5235 =
																													BGl_evmodulezd2findzd2globalz00zz__evmodulez00
																													(BgL_modz00_4241,
																													BgL_auxz00_7098);
																											}
																											{	/* Eval/evmodule.scm 306 */
																												bool_t
																													BgL_test3660z00_7106;
																												if (VECTORP
																													(BgL_vz00_5235))
																													{	/* Eval/evmodule.scm 306 */
																														BgL_test3660z00_7106
																															=
																															(VECTOR_LENGTH
																															(BgL_vz00_5235) ==
																															5L);
																													}
																												else
																													{	/* Eval/evmodule.scm 306 */
																														BgL_test3660z00_7106
																															= ((bool_t) 0);
																													}
																												if (BgL_test3660z00_7106)
																													{	/* Eval/evmodule.scm 306 */
																														VECTOR_SET
																															(BgL_vz00_5235,
																															0L, BINT(4L));
																														return
																															BgL_idz00_5222;
																													}
																												else
																													{	/* Eval/evmodule.scm 306 */
																														return
																															BGl_evcompilezd2errorzd2zz__evcompilez00
																															(BgL_locz00_4244,
																															BGl_string3037z00zz__evmodulez00,
																															BGl_string3049z00zz__evmodulez00,
																															BgL_idz00_5222);
																													}
																											}
																										}
																									}
																							}
																						else
																							{	/* Eval/evmodule.scm 444 */
																								if (
																									(CAR(
																											((obj_t) BgL_sz00_4245))
																										==
																										BGl_symbol3081z00zz__evmodulez00))
																									{	/* Eval/evmodule.scm 444 */
																										return BUNSPEC;
																									}
																								else
																									{	/* Eval/evmodule.scm 444 */
																										if (
																											(CAR(
																													((obj_t)
																														BgL_sz00_4245)) ==
																												BGl_symbol3083z00zz__evmodulez00))
																											{	/* Eval/evmodule.scm 444 */
																												return BUNSPEC;
																											}
																										else
																											{	/* Eval/evmodule.scm 444 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_sz00_4245))
																														==
																														BGl_symbol3085z00zz__evmodulez00))
																													{	/* Eval/evmodule.scm 444 */
																														bool_t
																															BgL_test3665z00_7126;
																														{	/* Eval/evmodule.scm 444 */
																															obj_t
																																BgL_tmpz00_7127;
																															{	/* Eval/evmodule.scm 444 */
																																obj_t
																																	BgL_pairz00_5315;
																																{	/* Eval/evmodule.scm 444 */
																																	obj_t
																																		BgL_aux2675z00_5317;
																																	BgL_aux2675z00_5317
																																		=
																																		CDR(((obj_t)
																																			BgL_sz00_4245));
																																	if (PAIRP
																																		(BgL_aux2675z00_5317))
																																		{	/* Eval/evmodule.scm 444 */
																																			BgL_pairz00_5315
																																				=
																																				BgL_aux2675z00_5317;
																																		}
																																	else
																																		{
																																			obj_t
																																				BgL_auxz00_7132;
																																			BgL_auxz00_7132
																																				=
																																				BGl_typezd2errorzd2zz__errorz00
																																				(BGl_string3001z00zz__evmodulez00,
																																				BINT
																																				(17466L),
																																				BGl_string3087z00zz__evmodulez00,
																																				BGl_string3003z00zz__evmodulez00,
																																				BgL_aux2675z00_5317);
																																			FAILURE
																																				(BgL_auxz00_7132,
																																				BFALSE,
																																				BFALSE);
																																		}
																																}
																																BgL_tmpz00_7127
																																	=
																																	CDR
																																	(BgL_pairz00_5315);
																															}
																															BgL_test3665z00_7126
																																=
																																NULLP
																																(BgL_tmpz00_7127);
																														}
																														if (BgL_test3665z00_7126)
																															{	/* Eval/evmodule.scm 444 */
																																return BUNSPEC;
																															}
																														else
																															{	/* Eval/evmodule.scm 444 */
																																obj_t
																																	BgL_carzd2783zd2_5318;
																																BgL_carzd2783zd2_5318
																																	=
																																	CAR(((obj_t)
																																		BgL_sz00_4245));
																																if (SYMBOLP
																																	(BgL_carzd2783zd2_5318))
																																	{
																																		obj_t
																																			BgL_sz00_7142;
																																		BgL_sz00_7142
																																			=
																																			BgL_carzd2783zd2_5318;
																																		BgL_sz00_5212
																																			=
																																			BgL_sz00_7142;
																																		goto
																																			BgL_tagzd2449zd2_5206;
																																	}
																																else
																																	{	/* Eval/evmodule.scm 444 */
																																		return
																																			BGl_evcompilezd2errorzd2zz__evcompilez00
																																			(BgL_locz00_4244,
																																			BGl_string3037z00zz__evmodulez00,
																																			BGl_string3074z00zz__evmodulez00,
																																			BgL_clausez00_4243);
																																	}
																															}
																													}
																												else
																													{	/* Eval/evmodule.scm 444 */
																														obj_t
																															BgL_carzd2790zd2_5320;
																														BgL_carzd2790zd2_5320
																															=
																															CAR(((obj_t)
																																BgL_sz00_4245));
																														if (SYMBOLP
																															(BgL_carzd2790zd2_5320))
																															{
																																obj_t
																																	BgL_sz00_7148;
																																BgL_sz00_7148 =
																																	BgL_carzd2790zd2_5320;
																																BgL_sz00_5212 =
																																	BgL_sz00_7148;
																																goto
																																	BgL_tagzd2449zd2_5206;
																															}
																														else
																															{	/* Eval/evmodule.scm 444 */
																																return
																																	BGl_evcompilezd2errorzd2zz__evcompilez00
																																	(BgL_locz00_4244,
																																	BGl_string3037z00zz__evmodulez00,
																																	BGl_string3074z00zz__evmodulez00,
																																	BgL_clausez00_4243);
																															}
																													}
																											}
																									}
																							}
																					}
																				else
																					{	/* Eval/evmodule.scm 444 */
																						if (
																							(CAR(
																									((obj_t) BgL_sz00_4245)) ==
																								BGl_symbol3081z00zz__evmodulez00))
																							{	/* Eval/evmodule.scm 444 */
																								return BUNSPEC;
																							}
																						else
																							{	/* Eval/evmodule.scm 444 */
																								if (
																									(CAR(
																											((obj_t) BgL_sz00_4245))
																										==
																										BGl_symbol3083z00zz__evmodulez00))
																									{	/* Eval/evmodule.scm 444 */
																										return BUNSPEC;
																									}
																								else
																									{	/* Eval/evmodule.scm 444 */
																										obj_t BgL_carzd2804zd2_5324;

																										BgL_carzd2804zd2_5324 =
																											CAR(
																											((obj_t) BgL_sz00_4245));
																										if (SYMBOLP
																											(BgL_carzd2804zd2_5324))
																											{
																												obj_t BgL_sz00_7162;

																												BgL_sz00_7162 =
																													BgL_carzd2804zd2_5324;
																												BgL_sz00_5212 =
																													BgL_sz00_7162;
																												goto
																													BgL_tagzd2449zd2_5206;
																											}
																										else
																											{	/* Eval/evmodule.scm 444 */
																												return
																													BGl_evcompilezd2errorzd2zz__evcompilez00
																													(BgL_locz00_4244,
																													BGl_string3037z00zz__evmodulez00,
																													BGl_string3074z00zz__evmodulez00,
																													BgL_clausez00_4243);
																											}
																									}
																							}
																					}
																			}
																		else
																			{	/* Eval/evmodule.scm 444 */
																				if (
																					(BgL_carzd2761zd2_5306 ==
																						BGl_symbol3070z00zz__evmodulez00))
																					{	/* Eval/evmodule.scm 444 */
																						goto BgL_kapzd2763zd2_5310;
																					}
																				else
																					{	/* Eval/evmodule.scm 444 */
																						if (
																							(CAR(
																									((obj_t) BgL_sz00_4245)) ==
																								BGl_symbol3081z00zz__evmodulez00))
																							{	/* Eval/evmodule.scm 444 */
																								return BUNSPEC;
																							}
																						else
																							{	/* Eval/evmodule.scm 444 */
																								if (
																									(CAR(
																											((obj_t) BgL_sz00_4245))
																										==
																										BGl_symbol3083z00zz__evmodulez00))
																									{	/* Eval/evmodule.scm 444 */
																										return BUNSPEC;
																									}
																								else
																									{	/* Eval/evmodule.scm 444 */
																										obj_t BgL_cdrzd2817zd2_5328;

																										BgL_cdrzd2817zd2_5328 =
																											CDR(
																											((obj_t) BgL_sz00_4245));
																										if (
																											(CAR(
																													((obj_t)
																														BgL_sz00_4245)) ==
																												BGl_symbol3085z00zz__evmodulez00))
																											{	/* Eval/evmodule.scm 444 */
																												if (PAIRP
																													(BgL_cdrzd2817zd2_5328))
																													{	/* Eval/evmodule.scm 444 */
																														if (NULLP(CDR
																																(BgL_cdrzd2817zd2_5328)))
																															{	/* Eval/evmodule.scm 444 */
																																return BUNSPEC;
																															}
																														else
																															{	/* Eval/evmodule.scm 444 */
																																obj_t
																																	BgL_carzd2822zd2_5331;
																																BgL_carzd2822zd2_5331
																																	=
																																	CAR(((obj_t)
																																		BgL_sz00_4245));
																																if (SYMBOLP
																																	(BgL_carzd2822zd2_5331))
																																	{
																																		obj_t
																																			BgL_sz00_7189;
																																		BgL_sz00_7189
																																			=
																																			BgL_carzd2822zd2_5331;
																																		BgL_sz00_5212
																																			=
																																			BgL_sz00_7189;
																																		goto
																																			BgL_tagzd2449zd2_5206;
																																	}
																																else
																																	{	/* Eval/evmodule.scm 444 */
																																		return
																																			BGl_evcompilezd2errorzd2zz__evcompilez00
																																			(BgL_locz00_4244,
																																			BGl_string3037z00zz__evmodulez00,
																																			BGl_string3074z00zz__evmodulez00,
																																			BgL_clausez00_4243);
																																	}
																															}
																													}
																												else
																													{	/* Eval/evmodule.scm 444 */
																														obj_t
																															BgL_carzd2829zd2_5333;
																														BgL_carzd2829zd2_5333
																															=
																															CAR(((obj_t)
																																BgL_sz00_4245));
																														if (SYMBOLP
																															(BgL_carzd2829zd2_5333))
																															{
																																obj_t
																																	BgL_sz00_7195;
																																BgL_sz00_7195 =
																																	BgL_carzd2829zd2_5333;
																																BgL_sz00_5212 =
																																	BgL_sz00_7195;
																																goto
																																	BgL_tagzd2449zd2_5206;
																															}
																														else
																															{	/* Eval/evmodule.scm 444 */
																																return
																																	BGl_evcompilezd2errorzd2zz__evcompilez00
																																	(BgL_locz00_4244,
																																	BGl_string3037z00zz__evmodulez00,
																																	BGl_string3074z00zz__evmodulez00,
																																	BgL_clausez00_4243);
																															}
																													}
																											}
																										else
																											{	/* Eval/evmodule.scm 444 */
																												obj_t
																													BgL_carzd2836zd2_5335;
																												BgL_carzd2836zd2_5335 =
																													CAR(((obj_t)
																														BgL_sz00_4245));
																												if (SYMBOLP
																													(BgL_carzd2836zd2_5335))
																													{
																														obj_t BgL_sz00_7201;

																														BgL_sz00_7201 =
																															BgL_carzd2836zd2_5335;
																														BgL_sz00_5212 =
																															BgL_sz00_7201;
																														goto
																															BgL_tagzd2449zd2_5206;
																													}
																												else
																													{	/* Eval/evmodule.scm 444 */
																														return
																															BGl_evcompilezd2errorzd2zz__evcompilez00
																															(BgL_locz00_4244,
																															BGl_string3037z00zz__evmodulez00,
																															BGl_string3074z00zz__evmodulez00,
																															BgL_clausez00_4243);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
																}
														}
												}
										}
								}
							else
								{	/* Eval/evmodule.scm 444 */
									return
										BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_4244,
										BGl_string3037z00zz__evmodulez00,
										BGl_string3074z00zz__evmodulez00, BgL_clausez00_4243);
								}
						}
				}
			}
		}

	}



/* evmodule-import-binding! */
	obj_t BGl_evmodulezd2importzd2bindingz12z12zz__evmodulez00(obj_t
		BgL_tozd2modzd2_90, obj_t BgL_tozd2identzd2_91, obj_t BgL_fromzd2modzd2_92,
		obj_t BgL_fromzd2identzd2_93, obj_t BgL_locz00_94)
	{
		{	/* Eval/evmodule.scm 452 */
			{	/* Eval/evmodule.scm 453 */
				obj_t BgL_varz00_1787;

				{	/* Eval/evmodule.scm 453 */
					obj_t BgL_auxz00_7204;

					if (SYMBOLP(BgL_fromzd2identzd2_93))
						{	/* Eval/evmodule.scm 453 */
							BgL_auxz00_7204 = BgL_fromzd2identzd2_93;
						}
					else
						{
							obj_t BgL_auxz00_7207;

							BgL_auxz00_7207 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(17973L),
								BGl_string3088z00zz__evmodulez00,
								BGl_string3005z00zz__evmodulez00, BgL_fromzd2identzd2_93);
							FAILURE(BgL_auxz00_7207, BFALSE, BFALSE);
						}
					BgL_varz00_1787 =
						BGl_evmodulezd2findzd2globalz00zz__evmodulez00(BgL_fromzd2modzd2_92,
						BgL_auxz00_7204);
				}
				if (CBOOL(BgL_varz00_1787))
					{	/* Eval/evmodule.scm 463 */
						obj_t BgL_idz00_3542;

						if (SYMBOLP(BgL_tozd2identzd2_91))
							{	/* Eval/evmodule.scm 463 */
								BgL_idz00_3542 = BgL_tozd2identzd2_91;
							}
						else
							{
								obj_t BgL_auxz00_7216;

								BgL_auxz00_7216 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(18325L),
									BGl_string3088z00zz__evmodulez00,
									BGl_string3005z00zz__evmodulez00, BgL_tozd2identzd2_91);
								FAILURE(BgL_auxz00_7216, BFALSE, BFALSE);
							}
						if (CBOOL(BGl_getzd2evalzd2expanderz00zz__macroz00(BgL_idz00_3542)))
							{	/* Eval/evmodule.scm 262 */
								obj_t BgL_msgz00_3544;

								{	/* Eval/evmodule.scm 262 */
									obj_t BgL_arg1392z00_3545;

									{	/* Eval/evmodule.scm 262 */
										obj_t BgL_arg2162z00_3550;

										BgL_arg2162z00_3550 = SYMBOL_TO_STRING(BgL_idz00_3542);
										BgL_arg1392z00_3545 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg2162z00_3550);
									}
									BgL_msgz00_3544 =
										string_append_3(BGl_string3029z00zz__evmodulez00,
										BgL_arg1392z00_3545, BGl_string3030z00zz__evmodulez00);
								}
								{	/* Eval/evmodule.scm 264 */
									obj_t BgL_list1391z00_3546;

									BgL_list1391z00_3546 = MAKE_YOUNG_PAIR(BgL_msgz00_3544, BNIL);
									BGl_evwarningz00zz__everrorz00(BgL_locz00_94,
										BgL_list1391z00_3546);
								}
							}
						else
							{	/* Eval/evmodule.scm 261 */
								BFALSE;
							}
						if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_tozd2modzd2_90))
							{	/* Eval/evmodule.scm 266 */
								obj_t BgL_arg1394z00_3548;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_sz00_3551;

									if (STRUCTP(BgL_tozd2modzd2_90))
										{	/* Eval/evmodule.scm 129 */
											BgL_sz00_3551 = BgL_tozd2modzd2_90;
										}
									else
										{
											obj_t BgL_auxz00_7232;

											BgL_auxz00_7232 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3088z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_tozd2modzd2_90);
											FAILURE(BgL_auxz00_7232, BFALSE, BFALSE);
										}
									BgL_arg1394z00_3548 = STRUCT_REF(BgL_sz00_3551, (int) (3L));
								}
								{	/* Eval/evmodule.scm 266 */
									obj_t BgL_auxz00_7238;

									if (STRUCTP(BgL_arg1394z00_3548))
										{	/* Eval/evmodule.scm 266 */
											BgL_auxz00_7238 = BgL_arg1394z00_3548;
										}
									else
										{
											obj_t BgL_auxz00_7241;

											BgL_auxz00_7241 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(10468L),
												BGl_string3088z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_arg1394z00_3548);
											FAILURE(BgL_auxz00_7241, BFALSE, BFALSE);
										}
									return
										BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_7238,
										BgL_idz00_3542, BgL_varz00_1787);
								}
							}
						else
							{	/* Eval/evmodule.scm 267 */
								obj_t BgL_auxz00_7246;

								if (VECTORP(BgL_varz00_1787))
									{	/* Eval/evmodule.scm 267 */
										BgL_auxz00_7246 = BgL_varz00_1787;
									}
								else
									{
										obj_t BgL_auxz00_7249;

										BgL_auxz00_7249 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(10507L),
											BGl_string3088z00zz__evmodulez00,
											BGl_string3025z00zz__evmodulez00, BgL_varz00_1787);
										FAILURE(BgL_auxz00_7249, BFALSE, BFALSE);
									}
								return
									BGl_bindzd2evalzd2globalz12z12zz__evenvz00(BgL_idz00_3542,
									BgL_auxz00_7246);
							}
					}
				else
					{	/* Eval/evmodule.scm 454 */
						{	/* Eval/evmodule.scm 456 */
							obj_t BgL_arg1620z00_1788;
							obj_t BgL_arg1621z00_1789;

							{	/* Eval/evmodule.scm 456 */
								obj_t BgL_tmpz00_7254;

								BgL_tmpz00_7254 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1620z00_1788 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7254);
							}
							{	/* Eval/evmodule.scm 456 */
								obj_t BgL_arg1628z00_1796;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_sz00_3553;

									if (STRUCTP(BgL_fromzd2modzd2_92))
										{	/* Eval/evmodule.scm 129 */
											BgL_sz00_3553 = BgL_fromzd2modzd2_92;
										}
									else
										{
											obj_t BgL_auxz00_7259;

											BgL_auxz00_7259 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(4439L),
												BGl_string3088z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_fromzd2modzd2_92);
											FAILURE(BgL_auxz00_7259, BFALSE, BFALSE);
										}
									BgL_arg1628z00_1796 = STRUCT_REF(BgL_sz00_3553, (int) (3L));
								}
								{	/* Eval/evmodule.scm 456 */
									obj_t BgL_auxz00_7265;

									if (STRUCTP(BgL_arg1628z00_1796))
										{	/* Eval/evmodule.scm 456 */
											BgL_auxz00_7265 = BgL_arg1628z00_1796;
										}
									else
										{
											obj_t BgL_auxz00_7268;

											BgL_auxz00_7268 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(18084L),
												BGl_string3088z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_arg1628z00_1796);
											FAILURE(BgL_auxz00_7268, BFALSE, BFALSE);
										}
									BgL_arg1621z00_1789 =
										BGl_hashtablezd2keyzd2listz00zz__hashz00(BgL_auxz00_7265);
								}
							}
							{	/* Eval/evmodule.scm 456 */
								obj_t BgL_list1622z00_1790;

								{	/* Eval/evmodule.scm 456 */
									obj_t BgL_arg1623z00_1791;

									{	/* Eval/evmodule.scm 456 */
										obj_t BgL_arg1624z00_1792;

										{	/* Eval/evmodule.scm 456 */
											obj_t BgL_arg1625z00_1793;

											{	/* Eval/evmodule.scm 456 */
												obj_t BgL_arg1626z00_1794;

												{	/* Eval/evmodule.scm 456 */
													obj_t BgL_arg1627z00_1795;

													BgL_arg1627z00_1795 =
														MAKE_YOUNG_PAIR(BgL_arg1621z00_1789, BNIL);
													BgL_arg1626z00_1794 =
														MAKE_YOUNG_PAIR(BGl_string3089z00zz__evmodulez00,
														BgL_arg1627z00_1795);
												}
												BgL_arg1625z00_1793 =
													MAKE_YOUNG_PAIR(BGl_string3090z00zz__evmodulez00,
													BgL_arg1626z00_1794);
											}
											BgL_arg1624z00_1792 =
												MAKE_YOUNG_PAIR(BINT(456L), BgL_arg1625z00_1793);
										}
										BgL_arg1623z00_1791 =
											MAKE_YOUNG_PAIR(BGl_string3091z00zz__evmodulez00,
											BgL_arg1624z00_1792);
									}
									BgL_list1622z00_1790 =
										MAKE_YOUNG_PAIR(BGl_string3092z00zz__evmodulez00,
										BgL_arg1623z00_1791);
								}
								BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1620z00_1788,
									BgL_list1622z00_1790);
							}
						}
						{	/* Eval/evmodule.scm 460 */
							obj_t BgL_arg1629z00_1797;
							obj_t BgL_arg1630z00_1798;

							{	/* Eval/evmodule.scm 460 */
								obj_t BgL_arg1631z00_1799;

								{	/* Eval/evmodule.scm 460 */
									obj_t BgL_arg1634z00_1800;

									{	/* Eval/evmodule.scm 460 */
										obj_t BgL_res2354z00_3556;

										if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_tozd2modzd2_90))
											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_sz00_3555;

												if (STRUCTP(BgL_tozd2modzd2_90))
													{	/* Eval/evmodule.scm 129 */
														BgL_sz00_3555 = BgL_tozd2modzd2_90;
													}
												else
													{
														obj_t BgL_auxz00_7285;

														BgL_auxz00_7285 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(4439L),
															BGl_string3088z00zz__evmodulez00,
															BGl_string3007z00zz__evmodulez00,
															BgL_tozd2modzd2_90);
														FAILURE(BgL_auxz00_7285, BFALSE, BFALSE);
													}
												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_aux2717z00_4690;

													BgL_aux2717z00_4690 =
														STRUCT_REF(BgL_sz00_3555, (int) (1L));
													if (SYMBOLP(BgL_aux2717z00_4690))
														{	/* Eval/evmodule.scm 129 */
															BgL_res2354z00_3556 = BgL_aux2717z00_4690;
														}
													else
														{
															obj_t BgL_auxz00_7293;

															BgL_auxz00_7293 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(4439L),
																BGl_string3088z00zz__evmodulez00,
																BGl_string3005z00zz__evmodulez00,
																BgL_aux2717z00_4690);
															FAILURE(BgL_auxz00_7293, BFALSE, BFALSE);
														}
												}
											}
										else
											{	/* Eval/evmodule.scm 143 */
												obj_t BgL_aux2719z00_4692;

												BgL_aux2719z00_4692 =
													BGl_bigloozd2typezd2errorz00zz__errorz00
													(BGl_string3006z00zz__evmodulez00,
													BGl_symbol3008z00zz__evmodulez00, BgL_tozd2modzd2_90);
												if (SYMBOLP(BgL_aux2719z00_4692))
													{	/* Eval/evmodule.scm 143 */
														BgL_res2354z00_3556 = BgL_aux2719z00_4692;
													}
												else
													{
														obj_t BgL_auxz00_7300;

														BgL_auxz00_7300 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(5128L),
															BGl_string3088z00zz__evmodulez00,
															BGl_string3005z00zz__evmodulez00,
															BgL_aux2719z00_4692);
														FAILURE(BgL_auxz00_7300, BFALSE, BFALSE);
													}
											}
										BgL_arg1634z00_1800 = BgL_res2354z00_3556;
									}
									{	/* Eval/evmodule.scm 460 */
										obj_t BgL_arg2162z00_3558;

										BgL_arg2162z00_3558 = SYMBOL_TO_STRING(BgL_arg1634z00_1800);
										BgL_arg1631z00_1799 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg2162z00_3558);
									}
								}
								BgL_arg1629z00_1797 =
									string_append_3(BGl_string3093z00zz__evmodulez00,
									BgL_arg1631z00_1799, BGl_string3094z00zz__evmodulez00);
							}
							{	/* Eval/evmodule.scm 462 */
								obj_t BgL_arg1636z00_1801;

								{	/* Eval/evmodule.scm 462 */
									obj_t BgL_arg1637z00_1802;

									{	/* Eval/evmodule.scm 462 */
										obj_t BgL_arg1638z00_1803;

										{	/* Eval/evmodule.scm 462 */
											obj_t BgL_res2355z00_3561;

											if (BGl_evmodulezf3zf3zz__evmodulez00
												(BgL_fromzd2modzd2_92))
												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_sz00_3560;

													if (STRUCTP(BgL_fromzd2modzd2_92))
														{	/* Eval/evmodule.scm 129 */
															BgL_sz00_3560 = BgL_fromzd2modzd2_92;
														}
													else
														{
															obj_t BgL_auxz00_7311;

															BgL_auxz00_7311 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(4439L),
																BGl_string3088z00zz__evmodulez00,
																BGl_string3007z00zz__evmodulez00,
																BgL_fromzd2modzd2_92);
															FAILURE(BgL_auxz00_7311, BFALSE, BFALSE);
														}
													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_aux2723z00_4696;

														BgL_aux2723z00_4696 =
															STRUCT_REF(BgL_sz00_3560, (int) (1L));
														if (SYMBOLP(BgL_aux2723z00_4696))
															{	/* Eval/evmodule.scm 129 */
																BgL_res2355z00_3561 = BgL_aux2723z00_4696;
															}
														else
															{
																obj_t BgL_auxz00_7319;

																BgL_auxz00_7319 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(4439L), BGl_string3088z00zz__evmodulez00,
																	BGl_string3005z00zz__evmodulez00,
																	BgL_aux2723z00_4696);
																FAILURE(BgL_auxz00_7319, BFALSE, BFALSE);
															}
													}
												}
											else
												{	/* Eval/evmodule.scm 143 */
													obj_t BgL_aux2725z00_4698;

													BgL_aux2725z00_4698 =
														BGl_bigloozd2typezd2errorz00zz__errorz00
														(BGl_string3006z00zz__evmodulez00,
														BGl_symbol3008z00zz__evmodulez00,
														BgL_fromzd2modzd2_92);
													if (SYMBOLP(BgL_aux2725z00_4698))
														{	/* Eval/evmodule.scm 143 */
															BgL_res2355z00_3561 = BgL_aux2725z00_4698;
														}
													else
														{
															obj_t BgL_auxz00_7326;

															BgL_auxz00_7326 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(5128L),
																BGl_string3088z00zz__evmodulez00,
																BGl_string3005z00zz__evmodulez00,
																BgL_aux2725z00_4698);
															FAILURE(BgL_auxz00_7326, BFALSE, BFALSE);
														}
												}
											BgL_arg1638z00_1803 = BgL_res2355z00_3561;
										}
										BgL_arg1637z00_1802 =
											MAKE_YOUNG_PAIR(BgL_arg1638z00_1803, BNIL);
									}
									BgL_arg1636z00_1801 =
										MAKE_YOUNG_PAIR(BgL_fromzd2identzd2_93,
										BgL_arg1637z00_1802);
								}
								BgL_arg1630z00_1798 =
									MAKE_YOUNG_PAIR(BGl_symbol3095z00zz__evmodulez00,
									BgL_arg1636z00_1801);
							}
							return
								BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_94,
								BGl_string3037z00zz__evmodulez00, BgL_arg1629z00_1797,
								BgL_arg1630z00_1798);
						}
					}
			}
		}

	}



/* evmodule-check-unbound */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00(obj_t
		BgL_modz00_95, obj_t BgL_locz00_96)
	{
		{	/* Eval/evmodule.scm 470 */
			{	/* Eval/evmodule.scm 477 */
				obj_t BgL_lz00_4256;

				BgL_lz00_4256 = MAKE_CELL(BNIL);
				{	/* Eval/evmodule.scm 480 */
					obj_t BgL_globalzd2checkzd2unboundz00_4246;

					BgL_globalzd2checkzd2unboundz00_4246 =
						MAKE_FX_PROCEDURE(BGl_z62globalzd2checkzd2unboundz62zz__evmodulez00,
						(int) (2L), (int) (2L));
					PROCEDURE_SET(BgL_globalzd2checkzd2unboundz00_4246,
						(int) (0L), ((obj_t) BgL_lz00_4256));
					PROCEDURE_SET(BgL_globalzd2checkzd2unboundz00_4246,
						(int) (1L), BgL_modz00_95);
					{	/* Eval/evmodule.scm 486 */
						obj_t BgL_arg1639z00_1807;

						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_sz00_3588;

							if (STRUCTP(BgL_modz00_95))
								{	/* Eval/evmodule.scm 129 */
									BgL_sz00_3588 = BgL_modz00_95;
								}
							else
								{
									obj_t BgL_auxz00_7344;

									BgL_auxz00_7344 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(4439L),
										BGl_string3097z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_modz00_95);
									FAILURE(BgL_auxz00_7344, BFALSE, BFALSE);
								}
							BgL_arg1639z00_1807 = STRUCT_REF(BgL_sz00_3588, (int) (3L));
						}
						{	/* Eval/evmodule.scm 486 */
							obj_t BgL_auxz00_7350;

							if (STRUCTP(BgL_arg1639z00_1807))
								{	/* Eval/evmodule.scm 486 */
									BgL_auxz00_7350 = BgL_arg1639z00_1807;
								}
							else
								{
									obj_t BgL_auxz00_7353;

									BgL_auxz00_7353 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(19206L),
										BGl_string3097z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_arg1639z00_1807);
									FAILURE(BgL_auxz00_7353, BFALSE, BFALSE);
								}
							BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_auxz00_7350,
								BgL_globalzd2checkzd2unboundz00_4246);
						}
					}
					{	/* Eval/evmodule.scm 487 */
						bool_t BgL_test3701z00_7358;

						{	/* Eval/evmodule.scm 487 */
							obj_t BgL_objz00_3589;

							BgL_objz00_3589 = CELL_REF(BgL_lz00_4256);
							BgL_test3701z00_7358 = PAIRP(BgL_objz00_3589);
						}
						if (BgL_test3701z00_7358)
							{	/* Eval/evmodule.scm 487 */
								{
									obj_t BgL_l1160z00_1810;

									BgL_l1160z00_1810 = CELL_REF(BgL_lz00_4256);
								BgL_zc3z04anonymousza31641ze3z87_1811:
									if (PAIRP(BgL_l1160z00_1810))
										{	/* Eval/evmodule.scm 488 */
											{	/* Eval/evmodule.scm 489 */
												obj_t BgL_vz00_1813;

												BgL_vz00_1813 = CAR(BgL_l1160z00_1810);
												{	/* Eval/evmodule.scm 489 */
													obj_t BgL_env1057z00_1815;

													BgL_env1057z00_1815 = BGL_CURRENT_DYNAMIC_ENV();
													{	/* Eval/evmodule.scm 489 */
														obj_t BgL_cell1052z00_1816;

														BgL_cell1052z00_1816 = MAKE_STACK_CELL(BUNSPEC);
														{	/* Eval/evmodule.scm 489 */
															obj_t BgL_val1056z00_1817;

															BgL_val1056z00_1817 =
																BGl_zc3z04exitza31644ze3ze70z60zz__evmodulez00
																(BgL_modz00_95, BgL_locz00_96, BgL_vz00_1813,
																BgL_cell1052z00_1816, BgL_env1057z00_1815);
															if ((BgL_val1056z00_1817 == BgL_cell1052z00_1816))
																{	/* Eval/evmodule.scm 489 */
																	{	/* Eval/evmodule.scm 489 */
																		int BgL_tmpz00_7368;

																		BgL_tmpz00_7368 = (int) (0L);
																		BGL_SIGSETMASK(BgL_tmpz00_7368);
																	}
																	{	/* Eval/evmodule.scm 489 */
																		obj_t BgL_arg1643z00_1818;

																		BgL_arg1643z00_1818 =
																			CELL_REF(((obj_t) BgL_val1056z00_1817));
																		BGl_errorzd2notifyzd2zz__errorz00
																			(BgL_arg1643z00_1818);
																		{	/* Eval/evmodule.scm 492 */
																			obj_t BgL_arg1648z00_3607;

																			{	/* Eval/evmodule.scm 492 */
																				obj_t BgL_tmpz00_7374;

																				BgL_tmpz00_7374 =
																					BGL_CURRENT_DYNAMIC_ENV();
																				BgL_arg1648z00_3607 =
																					BGL_ENV_CURRENT_ERROR_PORT
																					(BgL_tmpz00_7374);
																			}
																			bgl_display_char(((unsigned char) 10),
																				BgL_arg1648z00_3607);
																		} BFALSE;
																}}
															else
																{	/* Eval/evmodule.scm 489 */
																	BgL_val1056z00_1817;
																}
														}
													}
												}
											}
											{
												obj_t BgL_l1160z00_7378;

												BgL_l1160z00_7378 = CDR(BgL_l1160z00_1810);
												BgL_l1160z00_1810 = BgL_l1160z00_7378;
												goto BgL_zc3z04anonymousza31641ze3z87_1811;
											}
										}
									else
										{	/* Eval/evmodule.scm 488 */
											if (NULLP(BgL_l1160z00_1810))
												{	/* Eval/evmodule.scm 488 */
													BTRUE;
												}
											else
												{	/* Eval/evmodule.scm 488 */
													BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
														(BGl_string3044z00zz__evmodulez00,
														BGl_string3034z00zz__evmodulez00, BgL_l1160z00_1810,
														BGl_string3001z00zz__evmodulez00, BINT(19254L));
												}
										}
								}
								{	/* Eval/evmodule.scm 496 */
									long BgL_lenz00_1836;

									{	/* Eval/evmodule.scm 496 */
										obj_t BgL_auxz00_7384;

										{	/* Eval/evmodule.scm 496 */
											obj_t BgL_aux2731z00_4704;

											BgL_aux2731z00_4704 = CELL_REF(BgL_lz00_4256);
											{	/* Eval/evmodule.scm 496 */
												bool_t BgL_test3705z00_7385;

												if (PAIRP(BgL_aux2731z00_4704))
													{	/* Eval/evmodule.scm 496 */
														BgL_test3705z00_7385 = ((bool_t) 1);
													}
												else
													{	/* Eval/evmodule.scm 496 */
														BgL_test3705z00_7385 = NULLP(BgL_aux2731z00_4704);
													}
												if (BgL_test3705z00_7385)
													{	/* Eval/evmodule.scm 496 */
														BgL_auxz00_7384 = BgL_aux2731z00_4704;
													}
												else
													{
														obj_t BgL_auxz00_7389;

														BgL_auxz00_7389 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(19440L),
															BGl_string3097z00zz__evmodulez00,
															BGl_string3059z00zz__evmodulez00,
															BgL_aux2731z00_4704);
														FAILURE(BgL_auxz00_7389, BFALSE, BFALSE);
													}
											}
										}
										BgL_lenz00_1836 = bgl_list_length(BgL_auxz00_7384);
									}
									{	/* Eval/evmodule.scm 498 */
										obj_t BgL_arg1651z00_1837;
										obj_t BgL_arg1652z00_1838;
										obj_t BgL_arg1653z00_1839;

										{	/* Eval/evmodule.scm 498 */
											obj_t BgL_res2359z00_3613;

											if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_modz00_95))
												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_sz00_3612;

													if (STRUCTP(BgL_modz00_95))
														{	/* Eval/evmodule.scm 129 */
															BgL_sz00_3612 = BgL_modz00_95;
														}
													else
														{
															obj_t BgL_auxz00_7398;

															BgL_auxz00_7398 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(4439L),
																BGl_string3097z00zz__evmodulez00,
																BGl_string3007z00zz__evmodulez00,
																BgL_modz00_95);
															FAILURE(BgL_auxz00_7398, BFALSE, BFALSE);
														}
													{	/* Eval/evmodule.scm 129 */
														obj_t BgL_aux2735z00_4708;

														BgL_aux2735z00_4708 =
															STRUCT_REF(BgL_sz00_3612, (int) (1L));
														if (SYMBOLP(BgL_aux2735z00_4708))
															{	/* Eval/evmodule.scm 129 */
																BgL_res2359z00_3613 = BgL_aux2735z00_4708;
															}
														else
															{
																obj_t BgL_auxz00_7406;

																BgL_auxz00_7406 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(4439L), BGl_string3097z00zz__evmodulez00,
																	BGl_string3005z00zz__evmodulez00,
																	BgL_aux2735z00_4708);
																FAILURE(BgL_auxz00_7406, BFALSE, BFALSE);
															}
													}
												}
											else
												{	/* Eval/evmodule.scm 143 */
													obj_t BgL_aux2737z00_4710;

													BgL_aux2737z00_4710 =
														BGl_bigloozd2typezd2errorz00zz__errorz00
														(BGl_string3006z00zz__evmodulez00,
														BGl_symbol3008z00zz__evmodulez00, BgL_modz00_95);
													if (SYMBOLP(BgL_aux2737z00_4710))
														{	/* Eval/evmodule.scm 143 */
															BgL_res2359z00_3613 = BgL_aux2737z00_4710;
														}
													else
														{
															obj_t BgL_auxz00_7413;

															BgL_auxz00_7413 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(5128L),
																BGl_string3097z00zz__evmodulez00,
																BGl_string3005z00zz__evmodulez00,
																BgL_aux2737z00_4710);
															FAILURE(BgL_auxz00_7413, BFALSE, BFALSE);
														}
												}
											BgL_arg1651z00_1837 = BgL_res2359z00_3613;
										}
										{	/* Eval/evmodule.scm 499 */
											obj_t BgL_arg1654z00_1840;

											if ((BgL_lenz00_1836 > 1L))
												{	/* Eval/evmodule.scm 499 */
													BgL_arg1654z00_1840 =
														BGl_string3098z00zz__evmodulez00;
												}
											else
												{	/* Eval/evmodule.scm 499 */
													BgL_arg1654z00_1840 =
														BGl_string3099z00zz__evmodulez00;
												}
											{	/* Eval/evmodule.scm 499 */
												obj_t BgL_list1655z00_1841;

												{	/* Eval/evmodule.scm 499 */
													obj_t BgL_arg1656z00_1842;

													BgL_arg1656z00_1842 =
														MAKE_YOUNG_PAIR(BgL_arg1654z00_1840, BNIL);
													BgL_list1655z00_1841 =
														MAKE_YOUNG_PAIR(BINT(BgL_lenz00_1836),
														BgL_arg1656z00_1842);
												}
												BgL_arg1652z00_1838 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3100z00zz__evmodulez00,
													BgL_list1655z00_1841);
											}
										}
										{	/* Eval/evmodule.scm 500 */
											obj_t BgL_arg1658z00_1844;

											{	/* Eval/evmodule.scm 500 */
												obj_t BgL_l1162z00_1846;

												BgL_l1162z00_1846 = CELL_REF(BgL_lz00_4256);
												if (NULLP(BgL_l1162z00_1846))
													{	/* Eval/evmodule.scm 500 */
														BgL_arg1658z00_1844 = BNIL;
													}
												else
													{	/* Eval/evmodule.scm 500 */
														obj_t BgL_head1164z00_1848;

														{	/* Eval/evmodule.scm 500 */
															obj_t BgL_arg1669z00_1861;

															{	/* Eval/evmodule.scm 500 */
																obj_t BgL_arg1670z00_1862;

																{	/* Eval/evmodule.scm 500 */
																	obj_t BgL_pairz00_3615;

																	if (PAIRP(BgL_l1162z00_1846))
																		{	/* Eval/evmodule.scm 500 */
																			BgL_pairz00_3615 = BgL_l1162z00_1846;
																		}
																	else
																		{
																			obj_t BgL_auxz00_7427;

																			BgL_auxz00_7427 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(19586L),
																				BGl_string3097z00zz__evmodulez00,
																				BGl_string3003z00zz__evmodulez00,
																				BgL_l1162z00_1846);
																			FAILURE(BgL_auxz00_7427, BFALSE, BFALSE);
																		}
																	BgL_arg1670z00_1862 = CAR(BgL_pairz00_3615);
																}
																{	/* Eval/evmodule.scm 500 */
																	obj_t BgL_evalzd2globalzd2_3616;

																	if (VECTORP(BgL_arg1670z00_1862))
																		{	/* Eval/evmodule.scm 500 */
																			BgL_evalzd2globalzd2_3616 =
																				BgL_arg1670z00_1862;
																		}
																	else
																		{
																			obj_t BgL_auxz00_7434;

																			BgL_auxz00_7434 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(19586L),
																				BGl_string3097z00zz__evmodulez00,
																				BGl_string3025z00zz__evmodulez00,
																				BgL_arg1670z00_1862);
																			FAILURE(BgL_auxz00_7434, BFALSE, BFALSE);
																		}
																	BgL_arg1669z00_1861 =
																		VECTOR_REF(BgL_evalzd2globalzd2_3616, 1L);
																}
															}
															BgL_head1164z00_1848 =
																MAKE_YOUNG_PAIR(BgL_arg1669z00_1861, BNIL);
														}
														{	/* Eval/evmodule.scm 500 */
															obj_t BgL_g1167z00_1849;

															{	/* Eval/evmodule.scm 500 */
																obj_t BgL_pairz00_3618;

																if (PAIRP(BgL_l1162z00_1846))
																	{	/* Eval/evmodule.scm 500 */
																		BgL_pairz00_3618 = BgL_l1162z00_1846;
																	}
																else
																	{
																		obj_t BgL_auxz00_7442;

																		BgL_auxz00_7442 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(19586L),
																			BGl_string3097z00zz__evmodulez00,
																			BGl_string3003z00zz__evmodulez00,
																			BgL_l1162z00_1846);
																		FAILURE(BgL_auxz00_7442, BFALSE, BFALSE);
																	}
																BgL_g1167z00_1849 = CDR(BgL_pairz00_3618);
															}
															{
																obj_t BgL_l1162z00_1851;
																obj_t BgL_tail1165z00_1852;

																BgL_l1162z00_1851 = BgL_g1167z00_1849;
																BgL_tail1165z00_1852 = BgL_head1164z00_1848;
															BgL_zc3z04anonymousza31661ze3z87_1853:
																if (PAIRP(BgL_l1162z00_1851))
																	{	/* Eval/evmodule.scm 500 */
																		obj_t BgL_newtail1166z00_1855;

																		{	/* Eval/evmodule.scm 500 */
																			obj_t BgL_arg1664z00_1857;

																			{	/* Eval/evmodule.scm 500 */
																				obj_t BgL_arg1667z00_1858;

																				BgL_arg1667z00_1858 =
																					CAR(BgL_l1162z00_1851);
																				{	/* Eval/evmodule.scm 500 */
																					obj_t BgL_evalzd2globalzd2_3620;

																					if (VECTORP(BgL_arg1667z00_1858))
																						{	/* Eval/evmodule.scm 500 */
																							BgL_evalzd2globalzd2_3620 =
																								BgL_arg1667z00_1858;
																						}
																					else
																						{
																							obj_t BgL_auxz00_7452;

																							BgL_auxz00_7452 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(19586L),
																								BGl_string3101z00zz__evmodulez00,
																								BGl_string3025z00zz__evmodulez00,
																								BgL_arg1667z00_1858);
																							FAILURE(BgL_auxz00_7452, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1664z00_1857 =
																						VECTOR_REF
																						(BgL_evalzd2globalzd2_3620, 1L);
																				}
																			}
																			BgL_newtail1166z00_1855 =
																				MAKE_YOUNG_PAIR(BgL_arg1664z00_1857,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1165z00_1852,
																			BgL_newtail1166z00_1855);
																		{
																			obj_t BgL_tail1165z00_7461;
																			obj_t BgL_l1162z00_7459;

																			BgL_l1162z00_7459 =
																				CDR(BgL_l1162z00_1851);
																			BgL_tail1165z00_7461 =
																				BgL_newtail1166z00_1855;
																			BgL_tail1165z00_1852 =
																				BgL_tail1165z00_7461;
																			BgL_l1162z00_1851 = BgL_l1162z00_7459;
																			goto
																				BgL_zc3z04anonymousza31661ze3z87_1853;
																		}
																	}
																else
																	{	/* Eval/evmodule.scm 500 */
																		if (NULLP(BgL_l1162z00_1851))
																			{	/* Eval/evmodule.scm 500 */
																				BgL_arg1658z00_1844 =
																					BgL_head1164z00_1848;
																			}
																		else
																			{	/* Eval/evmodule.scm 500 */
																				BgL_arg1658z00_1844 =
																					BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
																					(BGl_string3102z00zz__evmodulez00,
																					BGl_string3034z00zz__evmodulez00,
																					BgL_l1162z00_1851,
																					BGl_string3001z00zz__evmodulez00,
																					BINT(19586L));
																			}
																	}
															}
														}
													}
											}
											{	/* Eval/evmodule.scm 500 */
												obj_t BgL_list1659z00_1845;

												BgL_list1659z00_1845 =
													MAKE_YOUNG_PAIR(BgL_arg1658z00_1844, BNIL);
												BgL_arg1653z00_1839 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3103z00zz__evmodulez00,
													BgL_list1659z00_1845);
											}
										}
										return
											BGl_evcompilezd2errorzd2zz__evcompilez00(BFALSE,
											BgL_arg1651z00_1837, BgL_arg1652z00_1838,
											BgL_arg1653z00_1839);
									}
								}
							}
						else
							{	/* Eval/evmodule.scm 487 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* <@exit:1644>~0 */
	obj_t BGl_zc3z04exitza31644ze3ze70z60zz__evmodulez00(obj_t BgL_modz00_4341,
		obj_t BgL_locz00_4340, obj_t BgL_vz00_4339, obj_t BgL_cell1052z00_4338,
		obj_t BgL_env1057z00_4337)
	{
		{	/* Eval/evmodule.scm 489 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1061z00_1820;

			if (SET_EXIT(BgL_an_exit1061z00_1820))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1061z00_1820 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1057z00_4337, BgL_an_exit1061z00_1820, 1L);
					{	/* Eval/evmodule.scm 489 */
						obj_t BgL_escape1053z00_1821;

						BgL_escape1053z00_1821 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1057z00_4337);
						{	/* Eval/evmodule.scm 489 */
							obj_t BgL_res1064z00_1822;

							{	/* Eval/evmodule.scm 489 */
								obj_t BgL_ohs1049z00_1823;

								BgL_ohs1049z00_1823 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1057z00_4337);
								{	/* Eval/evmodule.scm 489 */
									obj_t BgL_hds1050z00_1824;

									BgL_hds1050z00_1824 =
										MAKE_STACK_PAIR(BgL_escape1053z00_1821,
										BgL_cell1052z00_4338);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1057z00_4337,
										BgL_hds1050z00_1824);
									BUNSPEC;
									{	/* Eval/evmodule.scm 489 */
										obj_t BgL_exitd1058z00_1825;

										BgL_exitd1058z00_1825 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1057z00_4337);
										{	/* Eval/evmodule.scm 489 */
											obj_t BgL_tmp1060z00_1826;

											{	/* Eval/evmodule.scm 489 */
												obj_t BgL_arg1645z00_1828;

												BgL_arg1645z00_1828 =
													BGL_EXITD_PROTECT(BgL_exitd1058z00_1825);
												BgL_tmp1060z00_1826 =
													MAKE_YOUNG_PAIR(BgL_ohs1049z00_1823,
													BgL_arg1645z00_1828);
											}
											{	/* Eval/evmodule.scm 489 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1058z00_1825,
													BgL_tmp1060z00_1826);
												BUNSPEC;
												{	/* Eval/evmodule.scm 494 */
													obj_t BgL_tmp1059z00_1827;

													{	/* Eval/evmodule.scm 473 */
														obj_t BgL_arg1684z00_3594;
														obj_t BgL_arg1685z00_3595;
														obj_t BgL_arg1688z00_3596;

														{	/* Eval/evmodule.scm 473 */
															obj_t BgL__ortest_1047z00_3597;

															{	/* Eval/evmodule.scm 473 */
																obj_t BgL_vectorz00_3598;

																if (VECTORP(BgL_vz00_4339))
																	{	/* Eval/evmodule.scm 473 */
																		BgL_vectorz00_3598 = BgL_vz00_4339;
																	}
																else
																	{
																		obj_t BgL_auxz00_7482;

																		BgL_auxz00_7482 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(18818L),
																			BGl_string3104z00zz__evmodulez00,
																			BGl_string3025z00zz__evmodulez00,
																			BgL_vz00_4339);
																		FAILURE(BgL_auxz00_7482, BFALSE, BFALSE);
																	}
																BgL__ortest_1047z00_3597 =
																	VECTOR_REF(BgL_vectorz00_3598, 4L);
															}
															if (CBOOL(BgL__ortest_1047z00_3597))
																{	/* Eval/evmodule.scm 473 */
																	BgL_arg1684z00_3594 =
																		BgL__ortest_1047z00_3597;
																}
															else
																{	/* Eval/evmodule.scm 473 */
																	BgL_arg1684z00_3594 = BgL_locz00_4340;
																}
														}
														BgL_arg1685z00_3595 =
															BGl_evmodulezd2namezd2zz__evmodulez00
															(BgL_modz00_4341);
														{	/* Eval/evmodule.scm 475 */
															obj_t BgL_evalzd2globalzd2_3599;

															if (VECTORP(BgL_vz00_4339))
																{	/* Eval/evmodule.scm 475 */
																	BgL_evalzd2globalzd2_3599 = BgL_vz00_4339;
																}
															else
																{
																	obj_t BgL_auxz00_7492;

																	BgL_auxz00_7492 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(18904L),
																		BGl_string3104z00zz__evmodulez00,
																		BGl_string3025z00zz__evmodulez00,
																		BgL_vz00_4339);
																	FAILURE(BgL_auxz00_7492, BFALSE, BFALSE);
																}
															BgL_arg1688z00_3596 =
																VECTOR_REF(BgL_evalzd2globalzd2_3599, 1L);
														}
														BgL_tmp1059z00_1827 =
															BGl_evcompilezd2errorzd2zz__evcompilez00
															(BgL_arg1684z00_3594, BgL_arg1685z00_3595,
															BGl_string3105z00zz__evmodulez00,
															BgL_arg1688z00_3596);
													}
													{	/* Eval/evmodule.scm 489 */
														bool_t BgL_test3722z00_7498;

														{	/* Eval/evmodule.scm 489 */
															obj_t BgL_arg2330z00_3602;

															BgL_arg2330z00_3602 =
																BGL_EXITD_PROTECT(BgL_exitd1058z00_1825);
															BgL_test3722z00_7498 = PAIRP(BgL_arg2330z00_3602);
														}
														if (BgL_test3722z00_7498)
															{	/* Eval/evmodule.scm 489 */
																obj_t BgL_arg2327z00_3603;

																{	/* Eval/evmodule.scm 489 */
																	obj_t BgL_arg2328z00_3604;

																	BgL_arg2328z00_3604 =
																		BGL_EXITD_PROTECT(BgL_exitd1058z00_1825);
																	{	/* Eval/evmodule.scm 489 */
																		obj_t BgL_pairz00_3605;

																		if (PAIRP(BgL_arg2328z00_3604))
																			{	/* Eval/evmodule.scm 489 */
																				BgL_pairz00_3605 = BgL_arg2328z00_3604;
																			}
																		else
																			{
																				obj_t BgL_auxz00_7504;

																				BgL_auxz00_7504 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(19284L),
																					BGl_string3104z00zz__evmodulez00,
																					BGl_string3003z00zz__evmodulez00,
																					BgL_arg2328z00_3604);
																				FAILURE(BgL_auxz00_7504, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2327z00_3603 = CDR(BgL_pairz00_3605);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1058z00_1825,
																	BgL_arg2327z00_3603);
																BUNSPEC;
															}
														else
															{	/* Eval/evmodule.scm 489 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1057z00_4337,
														BgL_ohs1049z00_1823);
													BUNSPEC;
													BgL_res1064z00_1822 = BgL_tmp1059z00_1827;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1057z00_4337);
							return BgL_res1064z00_1822;
						}
					}
				}
		}

	}



/* &evmodule-check-unbound */
	obj_t BGl_z62evmodulezd2checkzd2unboundz62zz__evmodulez00(obj_t
		BgL_envz00_4247, obj_t BgL_modz00_4248, obj_t BgL_locz00_4249)
	{
		{	/* Eval/evmodule.scm 470 */
			return
				BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00(BgL_modz00_4248,
				BgL_locz00_4249);
		}

	}



/* &global-check-unbound */
	obj_t BGl_z62globalzd2checkzd2unboundz62zz__evmodulez00(obj_t BgL_envz00_4250,
		obj_t BgL_kz00_4253, obj_t BgL_gz00_4254)
	{
		{	/* Eval/evmodule.scm 484 */
			{	/* Eval/evmodule.scm 480 */
				obj_t BgL_lz00_4251;
				obj_t BgL_modz00_4252;

				BgL_lz00_4251 = PROCEDURE_REF(BgL_envz00_4250, (int) (0L));
				BgL_modz00_4252 = PROCEDURE_REF(BgL_envz00_4250, (int) (1L));
				{	/* Eval/evmodule.scm 480 */
					bool_t BgL_test3724z00_7517;

					{	/* Eval/evmodule.scm 230 */
						bool_t BgL_test3725z00_7518;

						if (VECTORP(BgL_gz00_4254))
							{	/* Eval/evmodule.scm 230 */
								BgL_test3725z00_7518 = (VECTOR_LENGTH(BgL_gz00_4254) == 5L);
							}
						else
							{	/* Eval/evmodule.scm 230 */
								BgL_test3725z00_7518 = ((bool_t) 0);
							}
						if (BgL_test3725z00_7518)
							{	/* Eval/evmodule.scm 230 */
								int BgL_arg1375z00_5338;

								{	/* Eval/evmodule.scm 230 */
									int BgL_res2357z00_5339;

									{	/* Eval/evmodule.scm 230 */
										obj_t BgL_tmpz00_7523;

										{	/* Eval/evmodule.scm 230 */
											obj_t BgL_aux2753z00_5340;

											BgL_aux2753z00_5340 = VECTOR_REF(BgL_gz00_4254, 0L);
											if (INTEGERP(BgL_aux2753z00_5340))
												{	/* Eval/evmodule.scm 230 */
													BgL_tmpz00_7523 = BgL_aux2753z00_5340;
												}
											else
												{
													obj_t BgL_auxz00_7527;

													BgL_auxz00_7527 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(8966L),
														BGl_string3106z00zz__evmodulez00,
														BGl_string3027z00zz__evmodulez00,
														BgL_aux2753z00_5340);
													FAILURE(BgL_auxz00_7527, BFALSE, BFALSE);
												}
										}
										BgL_res2357z00_5339 = CINT(BgL_tmpz00_7523);
									}
									BgL_arg1375z00_5338 = BgL_res2357z00_5339;
								}
								BgL_test3724z00_7517 = ((long) (BgL_arg1375z00_5338) == 6L);
							}
						else
							{	/* Eval/evmodule.scm 230 */
								BgL_test3724z00_7517 = ((bool_t) 0);
							}
					}
					if (BgL_test3724z00_7517)
						{	/* Eval/evmodule.scm 480 */
							return BFALSE;
						}
					else
						{	/* Eval/evmodule.scm 481 */
							int BgL_tagz00_5341;

							{	/* Eval/evmodule.scm 481 */
								int BgL_res2358z00_5342;

								{	/* Eval/evmodule.scm 481 */
									obj_t BgL_evalzd2globalzd2_5343;

									if (VECTORP(BgL_gz00_4254))
										{	/* Eval/evmodule.scm 481 */
											BgL_evalzd2globalzd2_5343 = BgL_gz00_4254;
										}
									else
										{
											obj_t BgL_auxz00_7536;

											BgL_auxz00_7536 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(19035L),
												BGl_string3106z00zz__evmodulez00,
												BGl_string3025z00zz__evmodulez00, BgL_gz00_4254);
											FAILURE(BgL_auxz00_7536, BFALSE, BFALSE);
										}
									{	/* Eval/evmodule.scm 481 */
										obj_t BgL_tmpz00_7540;

										{	/* Eval/evmodule.scm 481 */
											obj_t BgL_aux2756z00_5344;

											BgL_aux2756z00_5344 =
												VECTOR_REF(BgL_evalzd2globalzd2_5343, 0L);
											if (INTEGERP(BgL_aux2756z00_5344))
												{	/* Eval/evmodule.scm 481 */
													BgL_tmpz00_7540 = BgL_aux2756z00_5344;
												}
											else
												{
													obj_t BgL_auxz00_7544;

													BgL_auxz00_7544 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(19018L),
														BGl_string3106z00zz__evmodulez00,
														BGl_string3027z00zz__evmodulez00,
														BgL_aux2756z00_5344);
													FAILURE(BgL_auxz00_7544, BFALSE, BFALSE);
												}
										}
										BgL_res2358z00_5342 = CINT(BgL_tmpz00_7540);
									}
								}
								BgL_tagz00_5341 = BgL_res2358z00_5342;
							}
							{	/* Eval/evmodule.scm 482 */
								bool_t BgL_test3730z00_7549;

								{	/* Eval/evmodule.scm 482 */
									bool_t BgL_test3731z00_7550;

									{	/* Eval/evmodule.scm 482 */
										obj_t BgL_arg1681z00_5345;

										{	/* Eval/evmodule.scm 482 */
											obj_t BgL_vectorz00_5346;

											if (VECTORP(BgL_gz00_4254))
												{	/* Eval/evmodule.scm 482 */
													BgL_vectorz00_5346 = BgL_gz00_4254;
												}
											else
												{
													obj_t BgL_auxz00_7553;

													BgL_auxz00_7553 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(19064L),
														BGl_string3106z00zz__evmodulez00,
														BGl_string3025z00zz__evmodulez00, BgL_gz00_4254);
													FAILURE(BgL_auxz00_7553, BFALSE, BFALSE);
												}
											BgL_arg1681z00_5345 = VECTOR_REF(BgL_vectorz00_5346, 3L);
										}
										BgL_test3731z00_7550 =
											(BgL_arg1681z00_5345 == BgL_modz00_4252);
									}
									if (BgL_test3731z00_7550)
										{	/* Eval/evmodule.scm 483 */
											bool_t BgL__ortest_1048z00_5347;

											BgL__ortest_1048z00_5347 =
												((long) (BgL_tagz00_5341) == 3L);
											if (BgL__ortest_1048z00_5347)
												{	/* Eval/evmodule.scm 483 */
													BgL_test3730z00_7549 = BgL__ortest_1048z00_5347;
												}
											else
												{	/* Eval/evmodule.scm 483 */
													BgL_test3730z00_7549 =
														((long) (BgL_tagz00_5341) == 4L);
										}}
									else
										{	/* Eval/evmodule.scm 482 */
											BgL_test3730z00_7549 = ((bool_t) 0);
										}
								}
								if (BgL_test3730z00_7549)
									{	/* Eval/evmodule.scm 484 */
										obj_t BgL_auxz00_5348;

										BgL_auxz00_5348 =
											MAKE_YOUNG_PAIR(BgL_gz00_4254, CELL_REF(BgL_lz00_4251));
										return CELL_SET(BgL_lz00_4251, BgL_auxz00_5348);
									}
								else
									{	/* Eval/evmodule.scm 482 */
										return BFALSE;
									}
							}
						}
				}
			}
		}

	}



/* evmodule-load */
	obj_t BGl_evmodulezd2loadzd2zz__evmodulez00(obj_t BgL_modz00_97,
		obj_t BgL_identz00_98, obj_t BgL_pathsz00_99, obj_t BgL_locz00_100)
	{
		{	/* Eval/evmodule.scm 505 */
			{

				{	/* Eval/evmodule.scm 519 */
					obj_t BgL_loadz00_1885;

					{	/* Eval/evmodule.scm 519 */
						obj_t BgL__ortest_1066z00_1894;

						{	/* Eval/evmodule.scm 508 */
							obj_t BgL_procz00_1906;

							BgL_procz00_1906 = BGl_bigloozd2loadzd2modulez00zz__paramz00();
							if (PROCEDUREP(BgL_procz00_1906))
								{	/* Eval/evmodule.scm 509 */
									if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_1906, (int) (2L)))
										{	/* Eval/evmodule.scm 511 */
											BgL__ortest_1066z00_1894 = BgL_procz00_1906;
										}
									else
										{	/* Eval/evmodule.scm 511 */
											if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_1906,
													(int) (1L)))
												{	/* Eval/evmodule.scm 514 */
													obj_t BgL_zc3z04anonymousza31709ze3z87_4258;

													BgL_zc3z04anonymousza31709ze3z87_4258 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31709ze3ze5zz__evmodulez00,
														(int) (2L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31709ze3z87_4258,
														(int) (0L), BgL_procz00_1906);
													BgL__ortest_1066z00_1894 =
														BgL_zc3z04anonymousza31709ze3z87_4258;
												}
											else
												{	/* Eval/evmodule.scm 513 */
													BgL__ortest_1066z00_1894 =
														BGl_errorz00zz__errorz00
														(BGl_string3107z00zz__evmodulez00,
														BGl_string3108z00zz__evmodulez00, BgL_procz00_1906);
												}
										}
								}
							else
								{	/* Eval/evmodule.scm 509 */
									BgL__ortest_1066z00_1894 = BFALSE;
								}
						}
						if (CBOOL(BgL__ortest_1066z00_1894))
							{	/* Eval/evmodule.scm 519 */
								BgL_loadz00_1885 = BgL__ortest_1066z00_1894;
							}
						else
							{	/* Eval/evmodule.scm 519 */
								BgL_loadz00_1885 = BGl_evmodulezd2loadqzd2envz00zz__evmodulez00;
							}
					}
					{
						obj_t BgL_l1168z00_1887;

						BgL_l1168z00_1887 = BgL_pathsz00_99;
					BgL_zc3z04anonymousza31689ze3z87_1888:
						if (PAIRP(BgL_l1168z00_1887))
							{	/* Eval/evmodule.scm 520 */
								{	/* Eval/evmodule.scm 520 */
									obj_t BgL_pz00_1890;

									BgL_pz00_1890 = CAR(BgL_l1168z00_1887);
									{	/* Eval/evmodule.scm 520 */
										obj_t BgL_tmpfunz00_7589;

										{	/* Eval/evmodule.scm 520 */
											bool_t BgL_test2760z00_4733;

											BgL_test2760z00_4733 = PROCEDUREP(BgL_loadz00_1885);
											if (BgL_test2760z00_4733)
												{	/* Eval/evmodule.scm 520 */
													BgL_tmpfunz00_7589 = BgL_loadz00_1885;
												}
											else
												{
													obj_t BgL_auxz00_7592;

													BgL_auxz00_7592 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(20276L),
														BGl_string3109z00zz__evmodulez00,
														BGl_string3110z00zz__evmodulez00, BgL_loadz00_1885);
													FAILURE(BgL_auxz00_7592, BFALSE, BFALSE);
												}
										}
										(VA_PROCEDUREP(BgL_tmpfunz00_7589) ? ((obj_t(*)(obj_t,
														...))
												PROCEDURE_ENTRY(BgL_tmpfunz00_7589)) (BgL_loadz00_1885,
												BgL_pz00_1890, BgL_modz00_97, BEOA) : ((obj_t(*)(obj_t,
														obj_t,
														obj_t))
												PROCEDURE_ENTRY(BgL_tmpfunz00_7589)) (BgL_loadz00_1885,
												BgL_pz00_1890, BgL_modz00_97));
									}
								}
								{
									obj_t BgL_l1168z00_7596;

									BgL_l1168z00_7596 = CDR(BgL_l1168z00_1887);
									BgL_l1168z00_1887 = BgL_l1168z00_7596;
									goto BgL_zc3z04anonymousza31689ze3z87_1888;
								}
							}
						else
							{	/* Eval/evmodule.scm 520 */
								if (NULLP(BgL_l1168z00_1887))
									{	/* Eval/evmodule.scm 520 */
										BTRUE;
									}
								else
									{	/* Eval/evmodule.scm 520 */
										BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
											(BGl_string3044z00zz__evmodulez00,
											BGl_string3034z00zz__evmodulez00, BgL_l1168z00_1887,
											BGl_string3001z00zz__evmodulez00, BINT(20254L));
									}
							}
					}
				}
				{	/* Eval/evmodule.scm 522 */
					obj_t BgL_mz00_1895;

					{	/* Eval/evmodule.scm 522 */
						obj_t BgL_idz00_3626;

						if (SYMBOLP(BgL_identz00_98))
							{	/* Eval/evmodule.scm 522 */
								BgL_idz00_3626 = BgL_identz00_98;
							}
						else
							{
								obj_t BgL_auxz00_7604;

								BgL_auxz00_7604 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(20332L),
									BGl_string3107z00zz__evmodulez00,
									BGl_string3005z00zz__evmodulez00, BgL_identz00_98);
								FAILURE(BgL_auxz00_7604, BFALSE, BFALSE);
							}
						if (BGl_hashtablezf3zf3zz__hashz00
							(BGl_za2moduleszd2tableza2zd2zz__evmodulez00))
							{	/* Eval/evmodule.scm 224 */
								obj_t BgL_auxz00_7610;

								{	/* Eval/evmodule.scm 224 */
									obj_t BgL_aux2763z00_4736;

									BgL_aux2763z00_4736 =
										BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
									if (STRUCTP(BgL_aux2763z00_4736))
										{	/* Eval/evmodule.scm 224 */
											BgL_auxz00_7610 = BgL_aux2763z00_4736;
										}
									else
										{
											obj_t BgL_auxz00_7613;

											BgL_auxz00_7613 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(8670L),
												BGl_string3107z00zz__evmodulez00,
												BGl_string3007z00zz__evmodulez00, BgL_aux2763z00_4736);
											FAILURE(BgL_auxz00_7613, BFALSE, BFALSE);
										}
								}
								BgL_mz00_1895 =
									BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_7610,
									BgL_idz00_3626);
							}
						else
							{	/* Eval/evmodule.scm 223 */
								BgL_mz00_1895 = BFALSE;
							}
					}
					{	/* Eval/evmodule.scm 523 */
						bool_t BgL_test3744z00_7618;

						{	/* Eval/evmodule.scm 135 */
							bool_t BgL_test3745z00_7619;

							if (STRUCTP(BgL_mz00_1895))
								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_tmpz00_7622;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_res2360z00_3633;

										{	/* Eval/evmodule.scm 129 */
											obj_t BgL_aux2765z00_4738;

											BgL_aux2765z00_4738 = STRUCT_KEY(BgL_mz00_1895);
											if (SYMBOLP(BgL_aux2765z00_4738))
												{	/* Eval/evmodule.scm 129 */
													BgL_res2360z00_3633 = BgL_aux2765z00_4738;
												}
											else
												{
													obj_t BgL_auxz00_7626;

													BgL_auxz00_7626 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(4439L),
														BGl_string3107z00zz__evmodulez00,
														BGl_string3005z00zz__evmodulez00,
														BgL_aux2765z00_4738);
													FAILURE(BgL_auxz00_7626, BFALSE, BFALSE);
												}
										}
										BgL_tmpz00_7622 = BgL_res2360z00_3633;
									}
									BgL_test3745z00_7619 =
										(BgL_tmpz00_7622 == BGl_symbol2999z00zz__evmodulez00);
								}
							else
								{	/* Eval/evmodule.scm 129 */
									BgL_test3745z00_7619 = ((bool_t) 0);
								}
							if (BgL_test3745z00_7619)
								{	/* Eval/evmodule.scm 135 */
									BgL_test3744z00_7618 =
										(STRUCT_REF(BgL_mz00_1895,
											(int) (0L)) ==
										BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
								}
							else
								{	/* Eval/evmodule.scm 135 */
									BgL_test3744z00_7618 = ((bool_t) 0);
								}
						}
						if (BgL_test3744z00_7618)
							{	/* Eval/evmodule.scm 523 */
								BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00(BgL_mz00_1895,
									BgL_locz00_100);
								return BgL_mz00_1895;
							}
						else
							{	/* Eval/evmodule.scm 528 */
								obj_t BgL_arg1699z00_1897;
								obj_t BgL_arg1700z00_1898;

								{	/* Eval/evmodule.scm 528 */
									obj_t BgL_arg1701z00_1899;

									{	/* Eval/evmodule.scm 528 */
										obj_t BgL_res2361z00_3637;

										if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_modz00_97))
											{	/* Eval/evmodule.scm 129 */
												obj_t BgL_sz00_3636;

												if (STRUCTP(BgL_modz00_97))
													{	/* Eval/evmodule.scm 129 */
														BgL_sz00_3636 = BgL_modz00_97;
													}
												else
													{
														obj_t BgL_auxz00_7639;

														BgL_auxz00_7639 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(4439L),
															BGl_string3107z00zz__evmodulez00,
															BGl_string3007z00zz__evmodulez00, BgL_modz00_97);
														FAILURE(BgL_auxz00_7639, BFALSE, BFALSE);
													}
												{	/* Eval/evmodule.scm 129 */
													obj_t BgL_aux2769z00_4742;

													BgL_aux2769z00_4742 =
														STRUCT_REF(BgL_sz00_3636, (int) (1L));
													if (SYMBOLP(BgL_aux2769z00_4742))
														{	/* Eval/evmodule.scm 129 */
															BgL_res2361z00_3637 = BgL_aux2769z00_4742;
														}
													else
														{
															obj_t BgL_auxz00_7647;

															BgL_auxz00_7647 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(4439L),
																BGl_string3107z00zz__evmodulez00,
																BGl_string3005z00zz__evmodulez00,
																BgL_aux2769z00_4742);
															FAILURE(BgL_auxz00_7647, BFALSE, BFALSE);
														}
												}
											}
										else
											{	/* Eval/evmodule.scm 143 */
												obj_t BgL_aux2771z00_4744;

												BgL_aux2771z00_4744 =
													BGl_bigloozd2typezd2errorz00zz__errorz00
													(BGl_string3006z00zz__evmodulez00,
													BGl_symbol3008z00zz__evmodulez00, BgL_modz00_97);
												if (SYMBOLP(BgL_aux2771z00_4744))
													{	/* Eval/evmodule.scm 143 */
														BgL_res2361z00_3637 = BgL_aux2771z00_4744;
													}
												else
													{
														obj_t BgL_auxz00_7654;

														BgL_auxz00_7654 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(5128L),
															BGl_string3107z00zz__evmodulez00,
															BGl_string3005z00zz__evmodulez00,
															BgL_aux2771z00_4744);
														FAILURE(BgL_auxz00_7654, BFALSE, BFALSE);
													}
											}
										BgL_arg1701z00_1899 = BgL_res2361z00_3637;
									}
									{	/* Eval/evmodule.scm 528 */
										obj_t BgL_list1702z00_1900;

										{	/* Eval/evmodule.scm 528 */
											obj_t BgL_arg1703z00_1901;

											BgL_arg1703z00_1901 =
												MAKE_YOUNG_PAIR(BgL_identz00_98, BNIL);
											BgL_list1702z00_1900 =
												MAKE_YOUNG_PAIR(BgL_arg1701z00_1899,
												BgL_arg1703z00_1901);
										}
										BgL_arg1699z00_1897 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string3111z00zz__evmodulez00, BgL_list1702z00_1900);
									}
								}
								{	/* Eval/evmodule.scm 529 */
									bool_t BgL_test3752z00_7661;

									{	/* Eval/evmodule.scm 529 */
										obj_t BgL_tmpz00_7662;

										{	/* Eval/evmodule.scm 529 */
											obj_t BgL_pairz00_3638;

											if (PAIRP(BgL_pathsz00_99))
												{	/* Eval/evmodule.scm 529 */
													BgL_pairz00_3638 = BgL_pathsz00_99;
												}
											else
												{
													obj_t BgL_auxz00_7665;

													BgL_auxz00_7665 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(20546L),
														BGl_string3107z00zz__evmodulez00,
														BGl_string3003z00zz__evmodulez00, BgL_pathsz00_99);
													FAILURE(BgL_auxz00_7665, BFALSE, BFALSE);
												}
											BgL_tmpz00_7662 = CDR(BgL_pairz00_3638);
										}
										BgL_test3752z00_7661 = PAIRP(BgL_tmpz00_7662);
									}
									if (BgL_test3752z00_7661)
										{	/* Eval/evmodule.scm 529 */
											BgL_arg1700z00_1898 = BgL_pathsz00_99;
										}
									else
										{	/* Eval/evmodule.scm 531 */
											obj_t BgL_pairz00_3639;

											if (PAIRP(BgL_pathsz00_99))
												{	/* Eval/evmodule.scm 531 */
													BgL_pairz00_3639 = BgL_pathsz00_99;
												}
											else
												{
													obj_t BgL_auxz00_7673;

													BgL_auxz00_7673 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(20571L),
														BGl_string3107z00zz__evmodulez00,
														BGl_string3003z00zz__evmodulez00, BgL_pathsz00_99);
													FAILURE(BgL_auxz00_7673, BFALSE, BFALSE);
												}
											BgL_arg1700z00_1898 = CAR(BgL_pairz00_3639);
										}
								}
								BGL_TAIL return
									BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_100,
									BGl_string3037z00zz__evmodulez00, BgL_arg1699z00_1897,
									BgL_arg1700z00_1898);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1709> */
	obj_t BGl_z62zc3z04anonymousza31709ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4259, obj_t BgL_pathz00_4261, obj_t BgL__z00_4262)
	{
		{	/* Eval/evmodule.scm 514 */
			{	/* Eval/evmodule.scm 514 */
				obj_t BgL_procz00_4260;

				BgL_procz00_4260 = PROCEDURE_REF(BgL_envz00_4259, (int) (0L));
				{	/* Eval/evmodule.scm 514 */
					obj_t BgL_tmpfunz00_7684;

					{	/* Eval/evmodule.scm 514 */
						bool_t BgL_test2778z00_5349;

						BgL_test2778z00_5349 = PROCEDUREP(BgL_procz00_4260);
						if (BgL_test2778z00_5349)
							{	/* Eval/evmodule.scm 514 */
								BgL_tmpfunz00_7684 = BgL_procz00_4260;
							}
						else
							{
								obj_t BgL_auxz00_7687;

								BgL_auxz00_7687 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(20092L),
									BGl_string3112z00zz__evmodulez00,
									BGl_string3110z00zz__evmodulez00, BgL_procz00_4260);
								FAILURE(BgL_auxz00_7687, BFALSE, BFALSE);
							}
					}
					return
						(VA_PROCEDUREP(BgL_tmpfunz00_7684) ? ((obj_t(*)(obj_t,
									...)) PROCEDURE_ENTRY(BgL_tmpfunz00_7684)) (BgL_procz00_4260,
							BgL_pathz00_4261, BEOA) : ((obj_t(*)(obj_t,
									obj_t))
							PROCEDURE_ENTRY(BgL_tmpfunz00_7684)) (BgL_procz00_4260,
							BgL_pathz00_4261));
				}
			}
		}

	}



/* &evmodule-loadq */
	obj_t BGl_z62evmodulezd2loadqzb0zz__evmodulez00(obj_t BgL_envz00_4263,
		obj_t BgL_filez00_4264, obj_t BgL_modz00_4265)
	{
		{	/* Eval/evmodule.scm 541 */
			{	/* Eval/evmodule.scm 542 */
				obj_t BgL_pathz00_5350;

				{	/* Eval/evmodule.scm 542 */
					obj_t BgL_auxz00_7691;

					if (STRINGP(BgL_filez00_4264))
						{	/* Eval/evmodule.scm 542 */
							BgL_auxz00_7691 = BgL_filez00_4264;
						}
					else
						{
							obj_t BgL_auxz00_7694;

							BgL_auxz00_7694 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(21136L),
								BGl_string3113z00zz__evmodulez00,
								BGl_string3015z00zz__evmodulez00, BgL_filez00_4264);
							FAILURE(BgL_auxz00_7694, BFALSE, BFALSE);
						}
					BgL_pathz00_5350 =
						BGl_filezd2namezd2unixzd2canonicaliza7ez75zz__osz00
						(BgL_auxz00_7691);
				}
				{	/* Eval/evmodule.scm 542 */
					obj_t BgL_cvz00_5351;

					{	/* Eval/evmodule.scm 543 */
						obj_t BgL_tmpz00_7699;

						BgL_tmpz00_7699 =
							BGl_gensymz00zz__r4_symbols_6_4z00
							(BGl_symbol3114z00zz__evmodulez00);
						BgL_cvz00_5351 = bgl_make_condvar(BgL_tmpz00_7699);
					}
					{	/* Eval/evmodule.scm 543 */
						obj_t BgL_cellz00_5352;

						BgL_cellz00_5352 =
							MAKE_YOUNG_PAIR(BgL_pathz00_5350, BgL_cvz00_5351);
						{	/* Eval/evmodule.scm 544 */

							{

							BgL_loopz00_5353:
								{	/* Eval/evmodule.scm 546 */
									obj_t BgL_top3758z00_7704;

									BgL_top3758z00_7704 = BGL_EXITD_TOP_AS_OBJ();
									BGL_MUTEX_LOCK(BGl_za2loadqzd2mutexza2zd2zz__evmodulez00);
									BGL_EXITD_PUSH_PROTECT(BgL_top3758z00_7704,
										BGl_za2loadqzd2mutexza2zd2zz__evmodulez00);
									BUNSPEC;
									{	/* Eval/evmodule.scm 546 */
										obj_t BgL_tmp3757z00_7703;

										{	/* Eval/evmodule.scm 547 */
											obj_t BgL_cz00_5354;

											BgL_cz00_5354 =
												BGl_assocz00zz__r4_pairs_and_lists_6_3z00
												(BgL_pathz00_5350,
												BGl_za2loadingzd2listza2zd2zz__evmodulez00);
											if (PAIRP(BgL_cz00_5354))
												{	/* Eval/evmodule.scm 549 */
													{	/* Eval/evmodule.scm 553 */
														obj_t BgL_arg1714z00_5355;

														BgL_arg1714z00_5355 = CDR(BgL_cz00_5354);
														{	/* Eval/evmodule.scm 553 */
															obj_t BgL_g1233z00_5356;

															BgL_g1233z00_5356 =
																BGl_za2loadqzd2mutexza2zd2zz__evmodulez00;
															{	/* Eval/evmodule.scm 553 */

																{	/* Eval/evmodule.scm 553 */
																	obj_t BgL_cz00_5357;

																	if (BGL_CONDVARP(BgL_arg1714z00_5355))
																		{	/* Eval/evmodule.scm 553 */
																			BgL_cz00_5357 = BgL_arg1714z00_5355;
																		}
																	else
																		{
																			obj_t BgL_auxz00_7714;

																			BgL_auxz00_7714 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3115z00zz__evmodulez00,
																				BINT(8736L),
																				BGl_string3116z00zz__evmodulez00,
																				BGl_string3117z00zz__evmodulez00,
																				BgL_arg1714z00_5355);
																			FAILURE(BgL_auxz00_7714, BFALSE, BFALSE);
																		}
																	BGL_CONDVAR_WAIT(BgL_cz00_5357,
																		BgL_g1233z00_5356);
																}
															}
														}
													}
													goto BgL_loopz00_5353;
												}
											else
												{	/* Eval/evmodule.scm 549 */
													BgL_tmp3757z00_7703 =
														(BGl_za2loadingzd2listza2zd2zz__evmodulez00 =
														MAKE_YOUNG_PAIR(BgL_cellz00_5352,
															BGl_za2loadingzd2listza2zd2zz__evmodulez00),
														BUNSPEC);
												}
										}
										BGL_EXITD_POP_PROTECT(BgL_top3758z00_7704);
										BUNSPEC;
										BGL_MUTEX_UNLOCK(BGl_za2loadqzd2mutexza2zd2zz__evmodulez00);
										BgL_tmp3757z00_7703;
									}
								}
							}
							{	/* Eval/evmodule.scm 557 */
								obj_t BgL_exitd1067z00_5358;

								BgL_exitd1067z00_5358 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Eval/evmodule.scm 559 */
									obj_t BgL_zc3z04anonymousza31715ze3z87_5359;

									BgL_zc3z04anonymousza31715ze3z87_5359 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31715ze3ze5zz__evmodulez00,
										(int) (0L), (int) (2L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31715ze3z87_5359,
										(int) (0L), BgL_cellz00_5352);
									PROCEDURE_SET(BgL_zc3z04anonymousza31715ze3z87_5359,
										(int) (1L), BgL_cvz00_5351);
									{	/* Eval/evmodule.scm 557 */
										obj_t BgL_arg2331z00_5360;

										{	/* Eval/evmodule.scm 557 */
											obj_t BgL_arg2333z00_5361;

											BgL_arg2333z00_5361 =
												BGL_EXITD_PROTECT(BgL_exitd1067z00_5358);
											BgL_arg2331z00_5360 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31715ze3z87_5359,
												BgL_arg2333z00_5361);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_5358,
											BgL_arg2331z00_5360);
										BUNSPEC;
									}
									{	/* Eval/evmodule.scm 558 */
										obj_t BgL_tmp1069z00_5362;

										{	/* Eval/evmodule.scm 559 */
											obj_t BgL_envz00_5363;

											BgL_envz00_5363 =
												BGl_defaultzd2environmentzd2zz__evalz00();
											{	/* Eval/evmodule.scm 559 */

												BgL_tmp1069z00_5362 =
													BGl_loadqz00zz__evalz00(BgL_pathz00_5350,
													BgL_envz00_5363);
										}}
										{	/* Eval/evmodule.scm 557 */
											bool_t BgL_test3761z00_7735;

											{	/* Eval/evmodule.scm 557 */
												obj_t BgL_arg2330z00_5364;

												BgL_arg2330z00_5364 =
													BGL_EXITD_PROTECT(BgL_exitd1067z00_5358);
												BgL_test3761z00_7735 = PAIRP(BgL_arg2330z00_5364);
											}
											if (BgL_test3761z00_7735)
												{	/* Eval/evmodule.scm 557 */
													obj_t BgL_arg2327z00_5365;

													{	/* Eval/evmodule.scm 557 */
														obj_t BgL_arg2328z00_5366;

														BgL_arg2328z00_5366 =
															BGL_EXITD_PROTECT(BgL_exitd1067z00_5358);
														{	/* Eval/evmodule.scm 557 */
															obj_t BgL_pairz00_5367;

															if (PAIRP(BgL_arg2328z00_5366))
																{	/* Eval/evmodule.scm 557 */
																	BgL_pairz00_5367 = BgL_arg2328z00_5366;
																}
															else
																{
																	obj_t BgL_auxz00_7741;

																	BgL_auxz00_7741 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(21609L),
																		BGl_string3113z00zz__evmodulez00,
																		BGl_string3003z00zz__evmodulez00,
																		BgL_arg2328z00_5366);
																	FAILURE(BgL_auxz00_7741, BFALSE, BFALSE);
																}
															BgL_arg2327z00_5365 = CDR(BgL_pairz00_5367);
														}
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_5358,
														BgL_arg2327z00_5365);
													BUNSPEC;
												}
											else
												{	/* Eval/evmodule.scm 557 */
													BFALSE;
												}
										}
										BGl_z62zc3z04anonymousza31715ze3ze5zz__evmodulez00
											(BgL_zc3z04anonymousza31715ze3z87_5359);
										return BgL_tmp1069z00_5362;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4267)
	{
		{	/* Eval/evmodule.scm 557 */
			{	/* Eval/evmodule.scm 559 */
				obj_t BgL_cellz00_4268;
				obj_t BgL_cvz00_4269;

				BgL_cellz00_4268 = ((obj_t) PROCEDURE_REF(BgL_envz00_4267, (int) (0L)));
				BgL_cvz00_4269 = ((obj_t) PROCEDURE_REF(BgL_envz00_4267, (int) (1L)));
				{	/* Eval/evmodule.scm 559 */
					bool_t BgL_tmpz00_7754;

					{	/* Eval/evmodule.scm 559 */
						obj_t BgL_top3764z00_7756;

						BgL_top3764z00_7756 = BGL_EXITD_TOP_AS_OBJ();
						BGL_MUTEX_LOCK(BGl_za2loadqzd2mutexza2zd2zz__evmodulez00);
						BGL_EXITD_PUSH_PROTECT(BgL_top3764z00_7756,
							BGl_za2loadqzd2mutexza2zd2zz__evmodulez00);
						BUNSPEC;
						{	/* Eval/evmodule.scm 559 */
							bool_t BgL_tmp3763z00_7755;

							BGl_za2loadingzd2listza2zd2zz__evmodulez00 =
								bgl_remq_bang(BgL_cellz00_4268,
								BGl_za2loadingzd2listza2zd2zz__evmodulez00);
							BgL_tmp3763z00_7755 = BGL_CONDVAR_SIGNAL(BgL_cvz00_4269);
							BGL_EXITD_POP_PROTECT(BgL_top3764z00_7756);
							BUNSPEC;
							BGL_MUTEX_UNLOCK(BGl_za2loadqzd2mutexza2zd2zz__evmodulez00);
							BgL_tmpz00_7754 = BgL_tmp3763z00_7755;
					}}
					return BBOOL(BgL_tmpz00_7754);
				}
			}
		}

	}



/* evmodule-import! */
	obj_t BGl_evmodulezd2importz12zc0zz__evmodulez00(obj_t BgL_modz00_103,
		obj_t BgL_identz00_104, obj_t BgL_pathz00_105, obj_t BgL_setz00_106,
		obj_t BgL_abasez00_107, obj_t BgL_locz00_108)
	{
		{	/* Eval/evmodule.scm 566 */
			{	/* Eval/evmodule.scm 588 */
				obj_t BgL_mod2z00_1935;

				{	/* Eval/evmodule.scm 588 */
					obj_t BgL_idz00_3675;

					if (SYMBOLP(BgL_identz00_104))
						{	/* Eval/evmodule.scm 588 */
							BgL_idz00_3675 = BgL_identz00_104;
						}
					else
						{
							obj_t BgL_auxz00_7767;

							BgL_auxz00_7767 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(22730L),
								BGl_string3118z00zz__evmodulez00,
								BGl_string3005z00zz__evmodulez00, BgL_identz00_104);
							FAILURE(BgL_auxz00_7767, BFALSE, BFALSE);
						}
					if (BGl_hashtablezf3zf3zz__hashz00
						(BGl_za2moduleszd2tableza2zd2zz__evmodulez00))
						{	/* Eval/evmodule.scm 224 */
							obj_t BgL_auxz00_7773;

							{	/* Eval/evmodule.scm 224 */
								obj_t BgL_aux2787z00_4760;

								BgL_aux2787z00_4760 =
									BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
								if (STRUCTP(BgL_aux2787z00_4760))
									{	/* Eval/evmodule.scm 224 */
										BgL_auxz00_7773 = BgL_aux2787z00_4760;
									}
								else
									{
										obj_t BgL_auxz00_7776;

										BgL_auxz00_7776 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(8670L),
											BGl_string3118z00zz__evmodulez00,
											BGl_string3007z00zz__evmodulez00, BgL_aux2787z00_4760);
										FAILURE(BgL_auxz00_7776, BFALSE, BFALSE);
									}
							}
							BgL_mod2z00_1935 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_7773,
								BgL_idz00_3675);
						}
					else
						{	/* Eval/evmodule.scm 223 */
							BgL_mod2z00_1935 = BFALSE;
						}
				}
				{	/* Eval/evmodule.scm 590 */
					bool_t BgL_test3768z00_7781;

					{	/* Eval/evmodule.scm 135 */
						bool_t BgL_test3769z00_7782;

						if (STRUCTP(BgL_mod2z00_1935))
							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_tmpz00_7785;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_res2363z00_3682;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux2789z00_4762;

										BgL_aux2789z00_4762 = STRUCT_KEY(BgL_mod2z00_1935);
										if (SYMBOLP(BgL_aux2789z00_4762))
											{	/* Eval/evmodule.scm 129 */
												BgL_res2363z00_3682 = BgL_aux2789z00_4762;
											}
										else
											{
												obj_t BgL_auxz00_7789;

												BgL_auxz00_7789 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(4439L),
													BGl_string3118z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00,
													BgL_aux2789z00_4762);
												FAILURE(BgL_auxz00_7789, BFALSE, BFALSE);
											}
									}
									BgL_tmpz00_7785 = BgL_res2363z00_3682;
								}
								BgL_test3769z00_7782 =
									(BgL_tmpz00_7785 == BGl_symbol2999z00zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 129 */
								BgL_test3769z00_7782 = ((bool_t) 0);
							}
						if (BgL_test3769z00_7782)
							{	/* Eval/evmodule.scm 135 */
								BgL_test3768z00_7781 =
									(STRUCT_REF(BgL_mod2z00_1935,
										(int) (0L)) ==
									BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 135 */
								BgL_test3768z00_7781 = ((bool_t) 0);
							}
					}
					if (BgL_test3768z00_7781)
						{	/* Eval/evmodule.scm 590 */
							return
								BGl_z62importzd2modulezb0zz__evmodulez00(BgL_setz00_106,
								BgL_locz00_108, BgL_modz00_103, BgL_mod2z00_1935);
						}
					else
						{	/* Eval/evmodule.scm 590 */
							if (PAIRP(BgL_pathz00_105))
								{	/* Eval/evmodule.scm 597 */
									bool_t BgL_test3773z00_7800;

									{	/* Eval/evmodule.scm 597 */
										int BgL_arg1724z00_1943;

										BgL_arg1724z00_1943 =
											BGl_bigloozd2debugzd2modulez00zz__paramz00();
										BgL_test3773z00_7800 = ((long) (BgL_arg1724z00_1943) > 0L);
									}
									if (BgL_test3773z00_7800)
										{	/* Eval/evmodule.scm 601 */
											obj_t BgL_zc3z04anonymousza31723ze3z87_4274;

											BgL_zc3z04anonymousza31723ze3z87_4274 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31723ze3ze5zz__evmodulez00,
												(int) (0L), (int) (5L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4274,
												(int) (0L), BgL_setz00_106);
											PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4274,
												(int) (1L), BgL_modz00_103);
											PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4274,
												(int) (2L), BgL_identz00_104);
											PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4274,
												(int) (3L), BgL_pathz00_105);
											PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4274,
												(int) (4L), BgL_locz00_108);
											return
												BGl_z52withzd2tracez80zz__tracez00
												(BGl_symbol3008z00zz__evmodulez00, BgL_identz00_104,
												BgL_zc3z04anonymousza31723ze3z87_4274);
										}
									else
										{	/* Eval/evmodule.scm 597 */
											return
												BGl_z62loadzd2modulezb0zz__evmodulez00(BgL_locz00_108,
												BgL_pathz00_105, BgL_identz00_104, BgL_modz00_103,
												BgL_setz00_106);
										}
								}
							else
								{	/* Eval/evmodule.scm 594 */
									obj_t BgL_arg1725z00_1944;

									{	/* Eval/evmodule.scm 594 */
										obj_t BgL_list1726z00_1945;

										BgL_list1726z00_1945 =
											MAKE_YOUNG_PAIR(BgL_abasez00_107, BNIL);
										BgL_arg1725z00_1944 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string3119z00zz__evmodulez00, BgL_list1726z00_1945);
									}
									{	/* Eval/evmodule.scm 569 */
										obj_t BgL_arg1728z00_3685;

										{	/* Eval/evmodule.scm 569 */
											obj_t BgL_arg1729z00_3686;

											BgL_arg1729z00_3686 =
												BGl_evmodulezd2namezd2zz__evmodulez00(BgL_modz00_103);
											{	/* Eval/evmodule.scm 569 */
												obj_t BgL_list1730z00_3687;

												BgL_list1730z00_3687 =
													MAKE_YOUNG_PAIR(BgL_arg1729z00_3686, BNIL);
												BgL_arg1728z00_3685 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3120z00zz__evmodulez00,
													BgL_list1730z00_3687);
											}
										}
										return
											BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_108,
											BgL_arg1728z00_3685, BgL_arg1725z00_1944,
											BgL_identz00_104);
									}
								}
						}
				}
			}
		}

	}



/* &load-module */
	obj_t BGl_z62loadzd2modulezb0zz__evmodulez00(obj_t BgL_locz00_4279,
		obj_t BgL_pathz00_4278, obj_t BgL_identz00_4277, obj_t BgL_modz00_4276,
		obj_t BgL_setz00_4275)
	{
		{	/* Eval/evmodule.scm 586 */
			{	/* Eval/evmodule.scm 584 */
				obj_t BgL_exitd1071z00_1979;

				BgL_exitd1071z00_1979 = BGL_EXITD_TOP_AS_OBJ();
				{	/* Eval/evmodule.scm 586 */
					obj_t BgL_zc3z04anonymousza31749ze3z87_4272;

					BgL_zc3z04anonymousza31749ze3z87_4272 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31749ze3ze5zz__evmodulez00, (int) (0L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31749ze3z87_4272, (int) (0L),
						BgL_modz00_4276);
					{	/* Eval/evmodule.scm 584 */
						obj_t BgL_arg2331z00_3668;

						{	/* Eval/evmodule.scm 584 */
							obj_t BgL_arg2333z00_3669;

							BgL_arg2333z00_3669 = BGL_EXITD_PROTECT(BgL_exitd1071z00_1979);
							BgL_arg2331z00_3668 =
								MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31749ze3z87_4272,
								BgL_arg2333z00_3669);
						}
						BGL_EXITD_PROTECT_SET(BgL_exitd1071z00_1979, BgL_arg2331z00_3668);
						BUNSPEC;
					}
					{	/* Eval/evmodule.scm 585 */
						obj_t BgL_tmp1073z00_1981;

						BgL_tmp1073z00_1981 =
							BGl_z62importzd2modulezb0zz__evmodulez00(BgL_setz00_4275,
							BgL_locz00_4279, BgL_modz00_4276,
							BGl_evmodulezd2loadzd2zz__evmodulez00(BgL_modz00_4276,
								BgL_identz00_4277, BgL_pathz00_4278, BgL_locz00_4279));
						{	/* Eval/evmodule.scm 584 */
							bool_t BgL_test3774z00_7836;

							{	/* Eval/evmodule.scm 584 */
								obj_t BgL_arg2330z00_3671;

								BgL_arg2330z00_3671 = BGL_EXITD_PROTECT(BgL_exitd1071z00_1979);
								BgL_test3774z00_7836 = PAIRP(BgL_arg2330z00_3671);
							}
							if (BgL_test3774z00_7836)
								{	/* Eval/evmodule.scm 584 */
									obj_t BgL_arg2327z00_3672;

									{	/* Eval/evmodule.scm 584 */
										obj_t BgL_arg2328z00_3673;

										BgL_arg2328z00_3673 =
											BGL_EXITD_PROTECT(BgL_exitd1071z00_1979);
										{	/* Eval/evmodule.scm 584 */
											obj_t BgL_pairz00_3674;

											if (PAIRP(BgL_arg2328z00_3673))
												{	/* Eval/evmodule.scm 584 */
													BgL_pairz00_3674 = BgL_arg2328z00_3673;
												}
											else
												{
													obj_t BgL_auxz00_7842;

													BgL_auxz00_7842 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(22596L),
														BGl_string3121z00zz__evmodulez00,
														BGl_string3003z00zz__evmodulez00,
														BgL_arg2328z00_3673);
													FAILURE(BgL_auxz00_7842, BFALSE, BFALSE);
												}
											BgL_arg2327z00_3672 = CDR(BgL_pairz00_3674);
										}
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1071z00_1979,
										BgL_arg2327z00_3672);
									BUNSPEC;
								}
							else
								{	/* Eval/evmodule.scm 584 */
									BFALSE;
								}
						}
						BGL_MODULE_SET(BgL_modz00_4276);
						return BgL_tmp1073z00_1981;
					}
				}
			}
		}

	}



/* &import-module */
	obj_t BGl_z62importzd2modulezb0zz__evmodulez00(obj_t BgL_setz00_4282,
		obj_t BgL_locz00_4281, obj_t BgL_modz00_4280, obj_t BgL_mod2z00_1953)
	{
		{	/* Eval/evmodule.scm 576 */
			{	/* Eval/evmodule.scm 573 */
				obj_t BgL_tz00_1955;

				BgL_tz00_1955 = STRUCT_REF(BgL_modz00_4280, (int) (5L));
				{	/* Eval/evmodule.scm 574 */
					obj_t BgL_arg1733z00_1956;

					{	/* Eval/evmodule.scm 129 */
						obj_t BgL_sz00_3661;

						if (STRUCTP(BgL_mod2z00_1953))
							{	/* Eval/evmodule.scm 129 */
								BgL_sz00_3661 = BgL_mod2z00_1953;
							}
						else
							{
								obj_t BgL_auxz00_7853;

								BgL_auxz00_7853 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(4439L),
									BGl_string3122z00zz__evmodulez00,
									BGl_string3007z00zz__evmodulez00, BgL_mod2z00_1953);
								FAILURE(BgL_auxz00_7853, BFALSE, BFALSE);
							}
						BgL_arg1733z00_1956 = STRUCT_REF(BgL_sz00_3661, (int) (5L));
					}
					{	/* Eval/evmodule.scm 576 */
						obj_t BgL_zc3z04anonymousza31735ze3z87_4270;

						BgL_zc3z04anonymousza31735ze3z87_4270 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31735ze3ze5zz__evmodulez00, (int) (2L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31735ze3z87_4270, (int) (0L),
							BgL_tz00_1955);
						{	/* Eval/evmodule.scm 574 */
							obj_t BgL_auxz00_7864;

							if (STRUCTP(BgL_arg1733z00_1956))
								{	/* Eval/evmodule.scm 574 */
									BgL_auxz00_7864 = BgL_arg1733z00_1956;
								}
							else
								{
									obj_t BgL_auxz00_7867;

									BgL_auxz00_7867 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(22316L),
										BGl_string3122z00zz__evmodulez00,
										BGl_string3007z00zz__evmodulez00, BgL_arg1733z00_1956);
									FAILURE(BgL_auxz00_7867, BFALSE, BFALSE);
								}
							BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_auxz00_7864,
								BgL_zc3z04anonymousza31735ze3z87_4270);
						}
					}
				}
			}
			{	/* Eval/evmodule.scm 578 */
				obj_t BgL_g1172z00_1962;

				{	/* Eval/evmodule.scm 129 */
					obj_t BgL_sz00_3662;

					if (STRUCTP(BgL_mod2z00_1953))
						{	/* Eval/evmodule.scm 129 */
							BgL_sz00_3662 = BgL_mod2z00_1953;
						}
					else
						{
							obj_t BgL_auxz00_7874;

							BgL_auxz00_7874 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(4439L),
								BGl_string3122z00zz__evmodulez00,
								BGl_string3007z00zz__evmodulez00, BgL_mod2z00_1953);
							FAILURE(BgL_auxz00_7874, BFALSE, BFALSE);
						}
					BgL_g1172z00_1962 = STRUCT_REF(BgL_sz00_3662, (int) (4L));
				}
				{
					obj_t BgL_l1170z00_1964;

					BgL_l1170z00_1964 = BgL_g1172z00_1962;
				BgL_zc3z04anonymousza31736ze3z87_1965:
					if (PAIRP(BgL_l1170z00_1964))
						{	/* Eval/evmodule.scm 581 */
							{	/* Eval/evmodule.scm 579 */
								obj_t BgL_bz00_1967;

								BgL_bz00_1967 = CAR(BgL_l1170z00_1964);
								{	/* Eval/evmodule.scm 579 */
									bool_t BgL_test3780z00_7883;

									if (NULLP(BgL_setz00_4282))
										{	/* Eval/evmodule.scm 579 */
											BgL_test3780z00_7883 = ((bool_t) 1);
										}
									else
										{	/* Eval/evmodule.scm 579 */
											obj_t BgL_tmpz00_7886;

											{	/* Eval/evmodule.scm 579 */
												obj_t BgL_auxz00_7887;

												{	/* Eval/evmodule.scm 579 */
													obj_t BgL_pairz00_3664;

													if (PAIRP(BgL_bz00_1967))
														{	/* Eval/evmodule.scm 579 */
															BgL_pairz00_3664 = BgL_bz00_1967;
														}
													else
														{
															obj_t BgL_auxz00_7890;

															BgL_auxz00_7890 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(22460L),
																BGl_string3123z00zz__evmodulez00,
																BGl_string3003z00zz__evmodulez00,
																BgL_bz00_1967);
															FAILURE(BgL_auxz00_7890, BFALSE, BFALSE);
														}
													BgL_auxz00_7887 = CAR(BgL_pairz00_3664);
												}
												BgL_tmpz00_7886 =
													BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_auxz00_7887, BgL_setz00_4282);
											}
											BgL_test3780z00_7883 = CBOOL(BgL_tmpz00_7886);
										}
									if (BgL_test3780z00_7883)
										{	/* Eval/evmodule.scm 580 */
											obj_t BgL_arg1741z00_1971;
											obj_t BgL_arg1743z00_1972;

											{	/* Eval/evmodule.scm 580 */
												obj_t BgL_pairz00_3665;

												if (PAIRP(BgL_bz00_1967))
													{	/* Eval/evmodule.scm 580 */
														BgL_pairz00_3665 = BgL_bz00_1967;
													}
												else
													{
														obj_t BgL_auxz00_7899;

														BgL_auxz00_7899 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(22512L),
															BGl_string3123z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00, BgL_bz00_1967);
														FAILURE(BgL_auxz00_7899, BFALSE, BFALSE);
													}
												BgL_arg1741z00_1971 = CAR(BgL_pairz00_3665);
											}
											{	/* Eval/evmodule.scm 580 */
												obj_t BgL_pairz00_3666;

												if (PAIRP(BgL_bz00_1967))
													{	/* Eval/evmodule.scm 580 */
														BgL_pairz00_3666 = BgL_bz00_1967;
													}
												else
													{
														obj_t BgL_auxz00_7906;

														BgL_auxz00_7906 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(22525L),
															BGl_string3123z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00, BgL_bz00_1967);
														FAILURE(BgL_auxz00_7906, BFALSE, BFALSE);
													}
												BgL_arg1743z00_1972 = CAR(BgL_pairz00_3666);
											}
											BGl_evmodulezd2importzd2bindingz12z12zz__evmodulez00
												(BgL_modz00_4280, BgL_arg1741z00_1971, BgL_mod2z00_1953,
												BgL_arg1743z00_1972, BgL_locz00_4281);
										}
									else
										{	/* Eval/evmodule.scm 579 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1170z00_7912;

								BgL_l1170z00_7912 = CDR(BgL_l1170z00_1964);
								BgL_l1170z00_1964 = BgL_l1170z00_7912;
								goto BgL_zc3z04anonymousza31736ze3z87_1965;
							}
						}
					else
						{	/* Eval/evmodule.scm 581 */
							if (NULLP(BgL_l1170z00_1964))
								{	/* Eval/evmodule.scm 581 */
									return BTRUE;
								}
							else
								{	/* Eval/evmodule.scm 581 */
									return
										BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
										(BGl_string3044z00zz__evmodulez00,
										BGl_string3034z00zz__evmodulez00, BgL_l1170z00_1964,
										BGl_string3001z00zz__evmodulez00, BINT(22400L));
								}
						}
				}
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4283)
	{
		{	/* Eval/evmodule.scm 599 */
			{	/* Eval/evmodule.scm 601 */
				obj_t BgL_setz00_4284;
				obj_t BgL_modz00_4285;
				obj_t BgL_identz00_4286;
				obj_t BgL_pathz00_4287;
				obj_t BgL_locz00_4288;

				BgL_setz00_4284 = ((obj_t) PROCEDURE_REF(BgL_envz00_4283, (int) (0L)));
				BgL_modz00_4285 = ((obj_t) PROCEDURE_REF(BgL_envz00_4283, (int) (1L)));
				BgL_identz00_4286 = PROCEDURE_REF(BgL_envz00_4283, (int) (2L));
				BgL_pathz00_4287 = PROCEDURE_REF(BgL_envz00_4283, (int) (3L));
				BgL_locz00_4288 = PROCEDURE_REF(BgL_envz00_4283, (int) (4L));
				return
					BGl_z62loadzd2modulezb0zz__evmodulez00(BgL_locz00_4288,
					BgL_pathz00_4287, BgL_identz00_4286, BgL_modz00_4285,
					BgL_setz00_4284);
			}
		}

	}



/* &<@anonymous:1749> */
	obj_t BGl_z62zc3z04anonymousza31749ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4289)
	{
		{	/* Eval/evmodule.scm 584 */
			{	/* Eval/evmodule.scm 586 */
				obj_t BgL_modz00_4290;

				BgL_modz00_4290 = ((obj_t) PROCEDURE_REF(BgL_envz00_4289, (int) (0L)));
				return BGL_MODULE_SET(BgL_modz00_4290);
			}
		}

	}



/* &<@anonymous:1735> */
	obj_t BGl_z62zc3z04anonymousza31735ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4291, obj_t BgL_kz00_4293, obj_t BgL_vz00_4294)
	{
		{	/* Eval/evmodule.scm 575 */
			{	/* Eval/evmodule.scm 576 */
				obj_t BgL_tz00_4292;

				BgL_tz00_4292 = PROCEDURE_REF(BgL_envz00_4291, (int) (0L));
				{	/* Eval/evmodule.scm 576 */
					obj_t BgL_auxz00_7937;

					if (STRUCTP(BgL_tz00_4292))
						{	/* Eval/evmodule.scm 576 */
							BgL_auxz00_7937 = BgL_tz00_4292;
						}
					else
						{
							obj_t BgL_auxz00_7940;

							BgL_auxz00_7940 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(22360L),
								BGl_string3124z00zz__evmodulez00,
								BGl_string3007z00zz__evmodulez00, BgL_tz00_4292);
							FAILURE(BgL_auxz00_7940, BFALSE, BFALSE);
						}
					return
						BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_7937, BgL_kz00_4293,
						BgL_vz00_4294);
				}
			}
		}

	}



/* location-dir */
	obj_t BGl_locationzd2dirzd2zz__evmodulez00(obj_t BgL_locz00_109)
	{
		{	/* Eval/evmodule.scm 607 */
			if (PAIRP(BgL_locz00_109))
				{	/* Eval/evmodule.scm 608 */
					obj_t BgL_cdrzd2869zd2_1993;

					BgL_cdrzd2869zd2_1993 = CDR(((obj_t) BgL_locz00_109));
					if (
						(CAR(((obj_t) BgL_locz00_109)) == BGl_symbol3125z00zz__evmodulez00))
						{	/* Eval/evmodule.scm 608 */
							if (PAIRP(BgL_cdrzd2869zd2_1993))
								{	/* Eval/evmodule.scm 608 */
									obj_t BgL_arg1754z00_1997;

									BgL_arg1754z00_1997 = CAR(BgL_cdrzd2869zd2_1993);
									{	/* Eval/evmodule.scm 609 */
										obj_t BgL_auxz00_7956;

										if (STRINGP(BgL_arg1754z00_1997))
											{	/* Eval/evmodule.scm 609 */
												BgL_auxz00_7956 = BgL_arg1754z00_1997;
											}
										else
											{
												obj_t BgL_auxz00_7959;

												BgL_auxz00_7959 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(23390L),
													BGl_string3127z00zz__evmodulez00,
													BGl_string3015z00zz__evmodulez00,
													BgL_arg1754z00_1997);
												FAILURE(BgL_auxz00_7959, BFALSE, BFALSE);
											}
										return BGl_dirnamez00zz__osz00(BgL_auxz00_7956);
									}
								}
							else
								{	/* Eval/evmodule.scm 608 */
									return BFALSE;
								}
						}
					else
						{	/* Eval/evmodule.scm 608 */
							return BFALSE;
						}
				}
			else
				{	/* Eval/evmodule.scm 608 */
					return BFALSE;
				}
		}

	}



/* evmodule-import */
	obj_t BGl_evmodulezd2importzd2zz__evmodulez00(obj_t BgL_modz00_110,
		obj_t BgL_clausez00_111, obj_t BgL_locz00_112)
	{
		{	/* Eval/evmodule.scm 615 */
			{
				obj_t BgL_sz00_2058;
				obj_t BgL_clausez00_2042;
				obj_t BgL_clausez00_2023;
				obj_t BgL_clausez00_2016;

				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausez00_111))
					{	/* Eval/evmodule.scm 674 */
						obj_t BgL_g1179z00_2005;

						{	/* Eval/evmodule.scm 674 */
							obj_t BgL_pairz00_3730;

							if (PAIRP(BgL_clausez00_111))
								{	/* Eval/evmodule.scm 674 */
									BgL_pairz00_3730 = BgL_clausez00_111;
								}
							else
								{
									obj_t BgL_auxz00_7968;

									BgL_auxz00_7968 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(25418L),
										BGl_string3133z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_clausez00_111);
									FAILURE(BgL_auxz00_7968, BFALSE, BFALSE);
								}
							BgL_g1179z00_2005 = CDR(BgL_pairz00_3730);
						}
						{
							obj_t BgL_l1177z00_2007;

							BgL_l1177z00_2007 = BgL_g1179z00_2005;
						BgL_zc3z04anonymousza31757ze3z87_2008:
							if (PAIRP(BgL_l1177z00_2007))
								{	/* Eval/evmodule.scm 674 */
									BgL_sz00_2058 = CAR(BgL_l1177z00_2007);
									{	/* Eval/evmodule.scm 647 */
										obj_t BgL_locz00_2060;
										obj_t BgL_abasez00_2061;

										{	/* Eval/evmodule.scm 647 */
											obj_t BgL__ortest_1078z00_2111;

											BgL__ortest_1078z00_2111 =
												BGl_getzd2sourcezd2locationz00zz__readerz00
												(BgL_sz00_2058);
											if (CBOOL(BgL__ortest_1078z00_2111))
												{	/* Eval/evmodule.scm 647 */
													BgL_locz00_2060 = BgL__ortest_1078z00_2111;
												}
											else
												{	/* Eval/evmodule.scm 647 */
													BgL_locz00_2060 = BgL_locz00_112;
												}
										}
										BgL_abasez00_2061 =
											BGl_locationzd2dirzd2zz__evmodulez00(BgL_locz00_112);
										if (SYMBOLP(BgL_sz00_2058))
											{	/* Eval/evmodule.scm 651 */
												obj_t BgL_pathz00_2063;

												{	/* Eval/evmodule.scm 651 */
													obj_t BgL_fun1795z00_2064;

													BgL_fun1795z00_2064 =
														BGl_bigloozd2modulezd2resolverz00zz__modulez00();
													BgL_pathz00_2063 =
														BGL_PROCEDURE_CALL3(BgL_fun1795z00_2064,
														BgL_sz00_2058, BNIL, BgL_abasez00_2061);
												}
												BGl_evmodulezd2importz12zc0zz__evmodulez00
													(BgL_modz00_110, BgL_sz00_2058, BgL_pathz00_2063,
													BNIL, BgL_abasez00_2061, BgL_locz00_2060);
											}
										else
											{	/* Eval/evmodule.scm 653 */
												bool_t BgL_test3796z00_7989;

												if (PAIRP(BgL_sz00_2058))
													{	/* Eval/evmodule.scm 653 */
														if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
															(BgL_sz00_2058))
															{	/* Eval/evmodule.scm 655 */
																bool_t BgL_test3799z00_7994;

																{	/* Eval/evmodule.scm 655 */
																	bool_t BgL_test3800z00_7995;

																	{	/* Eval/evmodule.scm 655 */
																		obj_t BgL_tmpz00_7996;

																		BgL_tmpz00_7996 = CAR(BgL_sz00_2058);
																		BgL_test3800z00_7995 =
																			SYMBOLP(BgL_tmpz00_7996);
																	}
																	if (BgL_test3800z00_7995)
																		{	/* Eval/evmodule.scm 655 */
																			BgL_test3799z00_7994 = ((bool_t) 1);
																		}
																	else
																		{	/* Eval/evmodule.scm 655 */
																			BgL_test3799z00_7994 =
																				BGl_aliaszd2pairzf3z21zz__evmodulez00
																				(CAR(BgL_sz00_2058));
																		}
																}
																if (BgL_test3799z00_7994)
																	{	/* Eval/evmodule.scm 655 */
																		BgL_test3796z00_7989 = ((bool_t) 0);
																	}
																else
																	{	/* Eval/evmodule.scm 655 */
																		BgL_test3796z00_7989 = ((bool_t) 1);
																	}
															}
														else
															{	/* Eval/evmodule.scm 654 */
																BgL_test3796z00_7989 = ((bool_t) 1);
															}
													}
												else
													{	/* Eval/evmodule.scm 653 */
														BgL_test3796z00_7989 = ((bool_t) 1);
													}
												if (BgL_test3796z00_7989)
													{	/* Eval/evmodule.scm 653 */
														BGl_evcompilezd2errorzd2zz__evcompilez00
															(BgL_locz00_112, BGl_string3037z00zz__evmodulez00,
															BGl_string3131z00zz__evmodulez00, BgL_sz00_2058);
													}
												else
													{	/* Eval/evmodule.scm 658 */
														obj_t BgL_filesz00_2076;
														obj_t BgL_imodz00_2077;
														obj_t BgL_importsz00_2078;
														obj_t BgL_aliasesz00_2079;
														obj_t BgL_dirz00_2080;

														BgL_clausez00_2016 = BgL_sz00_2058;
													BgL_zc3z04anonymousza31763ze3z87_2017:
														if (NULLP(BgL_clausez00_2016))
															{	/* Eval/evmodule.scm 622 */
																BgL_filesz00_2076 = BNIL;
															}
														else
															{	/* Eval/evmodule.scm 623 */
																bool_t BgL_test3802z00_8004;

																{	/* Eval/evmodule.scm 623 */
																	obj_t BgL_tmpz00_8005;

																	{	/* Eval/evmodule.scm 623 */
																		obj_t BgL_pairz00_3691;

																		if (PAIRP(BgL_clausez00_2016))
																			{	/* Eval/evmodule.scm 623 */
																				BgL_pairz00_3691 = BgL_clausez00_2016;
																			}
																		else
																			{
																				obj_t BgL_auxz00_8008;

																				BgL_auxz00_8008 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(23874L),
																					BGl_string3128z00zz__evmodulez00,
																					BGl_string3003z00zz__evmodulez00,
																					BgL_clausez00_2016);
																				FAILURE(BgL_auxz00_8008, BFALSE,
																					BFALSE);
																			}
																		BgL_tmpz00_8005 = CAR(BgL_pairz00_3691);
																	}
																	BgL_test3802z00_8004 =
																		STRINGP(BgL_tmpz00_8005);
																}
																if (BgL_test3802z00_8004)
																	{	/* Eval/evmodule.scm 623 */
																		BgL_filesz00_2076 = BgL_clausez00_2016;
																	}
																else
																	{
																		obj_t BgL_clausez00_8014;

																		{	/* Eval/evmodule.scm 624 */
																			obj_t BgL_pairz00_3692;

																			if (PAIRP(BgL_clausez00_2016))
																				{	/* Eval/evmodule.scm 624 */
																					BgL_pairz00_3692 = BgL_clausez00_2016;
																				}
																			else
																				{
																					obj_t BgL_auxz00_8017;

																					BgL_auxz00_8017 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3001z00zz__evmodulez00,
																						BINT(23923L),
																						BGl_string3128z00zz__evmodulez00,
																						BGl_string3003z00zz__evmodulez00,
																						BgL_clausez00_2016);
																					FAILURE(BgL_auxz00_8017, BFALSE,
																						BFALSE);
																				}
																			BgL_clausez00_8014 =
																				CDR(BgL_pairz00_3692);
																		}
																		BgL_clausez00_2016 = BgL_clausez00_8014;
																		goto BgL_zc3z04anonymousza31763ze3z87_2017;
																	}
															}
														{
															obj_t BgL_list1174z00_2096;

															BgL_list1174z00_2096 = BgL_sz00_2058;
														BgL_zc3z04anonymousza31814ze3z87_2097:
															if (PAIRP(BgL_list1174z00_2096))
																{	/* Eval/evmodule.scm 659 */
																	bool_t BgL_test3806z00_8024;

																	{	/* Eval/evmodule.scm 659 */
																		obj_t BgL_tmpz00_8025;

																		BgL_tmpz00_8025 = CAR(BgL_list1174z00_2096);
																		BgL_test3806z00_8024 =
																			SYMBOLP(BgL_tmpz00_8025);
																	}
																	if (BgL_test3806z00_8024)
																		{	/* Eval/evmodule.scm 659 */
																			BgL_imodz00_2077 =
																				CAR(BgL_list1174z00_2096);
																		}
																	else
																		{
																			obj_t BgL_list1174z00_8029;

																			BgL_list1174z00_8029 =
																				CDR(BgL_list1174z00_2096);
																			BgL_list1174z00_2096 =
																				BgL_list1174z00_8029;
																			goto
																				BgL_zc3z04anonymousza31814ze3z87_2097;
																		}
																}
															else
																{	/* Eval/evmodule.scm 659 */
																	BgL_imodz00_2077 = BFALSE;
																}
														}
														BgL_clausez00_2023 = BgL_sz00_2058;
														{	/* Eval/evmodule.scm 627 */
															obj_t BgL_lz00_2025;

															{	/* Eval/evmodule.scm 627 */
																obj_t BgL_auxz00_8031;

																{	/* Eval/evmodule.scm 627 */
																	bool_t BgL_test3807z00_8032;

																	if (PAIRP(BgL_clausez00_2023))
																		{	/* Eval/evmodule.scm 627 */
																			BgL_test3807z00_8032 = ((bool_t) 1);
																		}
																	else
																		{	/* Eval/evmodule.scm 627 */
																			BgL_test3807z00_8032 =
																				NULLP(BgL_clausez00_2023);
																		}
																	if (BgL_test3807z00_8032)
																		{	/* Eval/evmodule.scm 627 */
																			BgL_auxz00_8031 = BgL_clausez00_2023;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8036;

																			BgL_auxz00_8036 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(24010L),
																				BGl_string3129z00zz__evmodulez00,
																				BGl_string3059z00zz__evmodulez00,
																				BgL_clausez00_2023);
																			FAILURE(BgL_auxz00_8036, BFALSE, BFALSE);
																		}
																}
																BgL_lz00_2025 =
																	BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00
																	(BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00,
																	BgL_auxz00_8031);
															}
															{
																obj_t BgL_lstz00_2028;
																obj_t BgL_resz00_2029;

																BgL_lstz00_2028 = BgL_clausez00_2023;
																BgL_resz00_2029 = BNIL;
															BgL_zc3z04anonymousza31770ze3z87_2030:
																if ((BgL_lstz00_2028 == BgL_lz00_2025))
																	{	/* Eval/evmodule.scm 630 */
																		BgL_importsz00_2078 = BgL_resz00_2029;
																	}
																else
																	{	/* Eval/evmodule.scm 632 */
																		bool_t BgL_test3810z00_8043;

																		{	/* Eval/evmodule.scm 632 */
																			obj_t BgL_auxz00_8044;

																			{	/* Eval/evmodule.scm 632 */
																				obj_t BgL_pairz00_3693;

																				if (PAIRP(BgL_lstz00_2028))
																					{	/* Eval/evmodule.scm 632 */
																						BgL_pairz00_3693 = BgL_lstz00_2028;
																					}
																				else
																					{
																						obj_t BgL_auxz00_8047;

																						BgL_auxz00_8047 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string3001z00zz__evmodulez00,
																							BINT(24114L),
																							BGl_string3116z00zz__evmodulez00,
																							BGl_string3003z00zz__evmodulez00,
																							BgL_lstz00_2028);
																						FAILURE(BgL_auxz00_8047, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_8044 = CAR(BgL_pairz00_3693);
																			}
																			BgL_test3810z00_8043 =
																				BGl_aliaszd2pairzf3z21zz__evmodulez00
																				(BgL_auxz00_8044);
																		}
																		if (BgL_test3810z00_8043)
																			{	/* Eval/evmodule.scm 633 */
																				obj_t BgL_arg1773z00_2033;
																				obj_t BgL_arg1774z00_2034;

																				{	/* Eval/evmodule.scm 633 */
																					obj_t BgL_pairz00_3694;

																					if (PAIRP(BgL_lstz00_2028))
																						{	/* Eval/evmodule.scm 633 */
																							BgL_pairz00_3694 =
																								BgL_lstz00_2028;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8055;

																							BgL_auxz00_8055 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(24137L),
																								BGl_string3116z00zz__evmodulez00,
																								BGl_string3003z00zz__evmodulez00,
																								BgL_lstz00_2028);
																							FAILURE(BgL_auxz00_8055, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1773z00_2033 =
																						CDR(BgL_pairz00_3694);
																				}
																				{	/* Eval/evmodule.scm 633 */
																					obj_t BgL_arg1775z00_2035;

																					{	/* Eval/evmodule.scm 633 */
																						obj_t BgL_pairz00_3696;

																						{	/* Eval/evmodule.scm 633 */
																							obj_t BgL_pairz00_3695;

																							if (PAIRP(BgL_lstz00_2028))
																								{	/* Eval/evmodule.scm 633 */
																									BgL_pairz00_3695 =
																										BgL_lstz00_2028;
																								}
																							else
																								{
																									obj_t BgL_auxz00_8062;

																									BgL_auxz00_8062 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(24159L),
																										BGl_string3116z00zz__evmodulez00,
																										BGl_string3003z00zz__evmodulez00,
																										BgL_lstz00_2028);
																									FAILURE(BgL_auxz00_8062,
																										BFALSE, BFALSE);
																								}
																							{	/* Eval/evmodule.scm 633 */
																								obj_t BgL_aux2825z00_4798;

																								BgL_aux2825z00_4798 =
																									CAR(BgL_pairz00_3695);
																								if (PAIRP(BgL_aux2825z00_4798))
																									{	/* Eval/evmodule.scm 633 */
																										BgL_pairz00_3696 =
																											BgL_aux2825z00_4798;
																									}
																								else
																									{
																										obj_t BgL_auxz00_8069;

																										BgL_auxz00_8069 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string3001z00zz__evmodulez00,
																											BINT(24154L),
																											BGl_string3116z00zz__evmodulez00,
																											BGl_string3003z00zz__evmodulez00,
																											BgL_aux2825z00_4798);
																										FAILURE(BgL_auxz00_8069,
																											BFALSE, BFALSE);
																									}
																							}
																						}
																						{	/* Eval/evmodule.scm 633 */
																							obj_t BgL_pairz00_3699;

																							{	/* Eval/evmodule.scm 633 */
																								obj_t BgL_aux2827z00_4800;

																								BgL_aux2827z00_4800 =
																									CDR(BgL_pairz00_3696);
																								if (PAIRP(BgL_aux2827z00_4800))
																									{	/* Eval/evmodule.scm 633 */
																										BgL_pairz00_3699 =
																											BgL_aux2827z00_4800;
																									}
																								else
																									{
																										obj_t BgL_auxz00_8076;

																										BgL_auxz00_8076 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string3001z00zz__evmodulez00,
																											BINT(24148L),
																											BGl_string3116z00zz__evmodulez00,
																											BGl_string3003z00zz__evmodulez00,
																											BgL_aux2827z00_4800);
																										FAILURE(BgL_auxz00_8076,
																											BFALSE, BFALSE);
																									}
																							}
																							BgL_arg1775z00_2035 =
																								CAR(BgL_pairz00_3699);
																						}
																					}
																					BgL_arg1774z00_2034 =
																						MAKE_YOUNG_PAIR(BgL_arg1775z00_2035,
																						BgL_resz00_2029);
																				}
																				{
																					obj_t BgL_resz00_8083;
																					obj_t BgL_lstz00_8082;

																					BgL_lstz00_8082 = BgL_arg1773z00_2033;
																					BgL_resz00_8083 = BgL_arg1774z00_2034;
																					BgL_resz00_2029 = BgL_resz00_8083;
																					BgL_lstz00_2028 = BgL_lstz00_8082;
																					goto
																						BgL_zc3z04anonymousza31770ze3z87_2030;
																				}
																			}
																		else
																			{	/* Eval/evmodule.scm 634 */
																				obj_t BgL_arg1779z00_2037;
																				obj_t BgL_arg1781z00_2038;

																				{	/* Eval/evmodule.scm 634 */
																					obj_t BgL_pairz00_3700;

																					if (PAIRP(BgL_lstz00_2028))
																						{	/* Eval/evmodule.scm 634 */
																							BgL_pairz00_3700 =
																								BgL_lstz00_2028;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8086;

																							BgL_auxz00_8086 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(24188L),
																								BGl_string3116z00zz__evmodulez00,
																								BGl_string3003z00zz__evmodulez00,
																								BgL_lstz00_2028);
																							FAILURE(BgL_auxz00_8086, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1779z00_2037 =
																						CDR(BgL_pairz00_3700);
																				}
																				{	/* Eval/evmodule.scm 634 */
																					obj_t BgL_arg1782z00_2039;

																					{	/* Eval/evmodule.scm 634 */
																						obj_t BgL_pairz00_3701;

																						if (PAIRP(BgL_lstz00_2028))
																							{	/* Eval/evmodule.scm 634 */
																								BgL_pairz00_3701 =
																									BgL_lstz00_2028;
																							}
																						else
																							{
																								obj_t BgL_auxz00_8093;

																								BgL_auxz00_8093 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3001z00zz__evmodulez00,
																									BINT(24204L),
																									BGl_string3116z00zz__evmodulez00,
																									BGl_string3003z00zz__evmodulez00,
																									BgL_lstz00_2028);
																								FAILURE(BgL_auxz00_8093, BFALSE,
																									BFALSE);
																							}
																						BgL_arg1782z00_2039 =
																							CAR(BgL_pairz00_3701);
																					}
																					BgL_arg1781z00_2038 =
																						MAKE_YOUNG_PAIR(BgL_arg1782z00_2039,
																						BgL_resz00_2029);
																				}
																				{
																					obj_t BgL_resz00_8100;
																					obj_t BgL_lstz00_8099;

																					BgL_lstz00_8099 = BgL_arg1779z00_2037;
																					BgL_resz00_8100 = BgL_arg1781z00_2038;
																					BgL_resz00_2029 = BgL_resz00_8100;
																					BgL_lstz00_2028 = BgL_lstz00_8099;
																					goto
																						BgL_zc3z04anonymousza31770ze3z87_2030;
																				}
																			}
																	}
															}
														}
														BgL_clausez00_2042 = BgL_sz00_2058;
														{	/* Eval/evmodule.scm 637 */
															obj_t BgL_lz00_2044;

															{	/* Eval/evmodule.scm 637 */
																obj_t BgL_auxz00_8101;

																{	/* Eval/evmodule.scm 637 */
																	bool_t BgL_test3818z00_8102;

																	if (PAIRP(BgL_clausez00_2042))
																		{	/* Eval/evmodule.scm 637 */
																			BgL_test3818z00_8102 = ((bool_t) 1);
																		}
																	else
																		{	/* Eval/evmodule.scm 637 */
																			BgL_test3818z00_8102 =
																				NULLP(BgL_clausez00_2042);
																		}
																	if (BgL_test3818z00_8102)
																		{	/* Eval/evmodule.scm 637 */
																			BgL_auxz00_8101 = BgL_clausez00_2042;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8106;

																			BgL_auxz00_8106 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(24295L),
																				BGl_string3130z00zz__evmodulez00,
																				BGl_string3059z00zz__evmodulez00,
																				BgL_clausez00_2042);
																			FAILURE(BgL_auxz00_8106, BFALSE, BFALSE);
																		}
																}
																BgL_lz00_2044 =
																	BGl_findzd2tailzd2zz__r4_pairs_and_lists_6_3z00
																	(BGl_symbolzf3zd2envz21zz__r4_symbols_6_4z00,
																	BgL_auxz00_8101);
															}
															{
																obj_t BgL_lstz00_2047;
																obj_t BgL_resz00_2048;

																BgL_lstz00_2047 = BgL_clausez00_2042;
																BgL_resz00_2048 = BNIL;
															BgL_zc3z04anonymousza31785ze3z87_2049:
																if ((BgL_lstz00_2047 == BgL_lz00_2044))
																	{	/* Eval/evmodule.scm 640 */
																		BgL_aliasesz00_2079 = BgL_resz00_2048;
																	}
																else
																	{	/* Eval/evmodule.scm 642 */
																		bool_t BgL_test3821z00_8113;

																		{	/* Eval/evmodule.scm 642 */
																			obj_t BgL_auxz00_8114;

																			{	/* Eval/evmodule.scm 642 */
																				obj_t BgL_pairz00_3702;

																				if (PAIRP(BgL_lstz00_2047))
																					{	/* Eval/evmodule.scm 642 */
																						BgL_pairz00_3702 = BgL_lstz00_2047;
																					}
																				else
																					{
																						obj_t BgL_auxz00_8117;

																						BgL_auxz00_8117 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string3001z00zz__evmodulez00,
																							BINT(24399L),
																							BGl_string3116z00zz__evmodulez00,
																							BGl_string3003z00zz__evmodulez00,
																							BgL_lstz00_2047);
																						FAILURE(BgL_auxz00_8117, BFALSE,
																							BFALSE);
																					}
																				BgL_auxz00_8114 = CAR(BgL_pairz00_3702);
																			}
																			BgL_test3821z00_8113 =
																				BGl_aliaszd2pairzf3z21zz__evmodulez00
																				(BgL_auxz00_8114);
																		}
																		if (BgL_test3821z00_8113)
																			{	/* Eval/evmodule.scm 643 */
																				obj_t BgL_arg1788z00_2052;
																				obj_t BgL_arg1789z00_2053;

																				{	/* Eval/evmodule.scm 643 */
																					obj_t BgL_pairz00_3703;

																					if (PAIRP(BgL_lstz00_2047))
																						{	/* Eval/evmodule.scm 643 */
																							BgL_pairz00_3703 =
																								BgL_lstz00_2047;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8125;

																							BgL_auxz00_8125 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(24422L),
																								BGl_string3116z00zz__evmodulez00,
																								BGl_string3003z00zz__evmodulez00,
																								BgL_lstz00_2047);
																							FAILURE(BgL_auxz00_8125, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1788z00_2052 =
																						CDR(BgL_pairz00_3703);
																				}
																				{	/* Eval/evmodule.scm 643 */
																					obj_t BgL_arg1790z00_2054;

																					{	/* Eval/evmodule.scm 643 */
																						obj_t BgL_pairz00_3704;

																						if (PAIRP(BgL_lstz00_2047))
																							{	/* Eval/evmodule.scm 643 */
																								BgL_pairz00_3704 =
																									BgL_lstz00_2047;
																							}
																						else
																							{
																								obj_t BgL_auxz00_8132;

																								BgL_auxz00_8132 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3001z00zz__evmodulez00,
																									BINT(24438L),
																									BGl_string3116z00zz__evmodulez00,
																									BGl_string3003z00zz__evmodulez00,
																									BgL_lstz00_2047);
																								FAILURE(BgL_auxz00_8132, BFALSE,
																									BFALSE);
																							}
																						BgL_arg1790z00_2054 =
																							CAR(BgL_pairz00_3704);
																					}
																					BgL_arg1789z00_2053 =
																						MAKE_YOUNG_PAIR(BgL_arg1790z00_2054,
																						BgL_resz00_2048);
																				}
																				{
																					obj_t BgL_resz00_8139;
																					obj_t BgL_lstz00_8138;

																					BgL_lstz00_8138 = BgL_arg1788z00_2052;
																					BgL_resz00_8139 = BgL_arg1789z00_2053;
																					BgL_resz00_2048 = BgL_resz00_8139;
																					BgL_lstz00_2047 = BgL_lstz00_8138;
																					goto
																						BgL_zc3z04anonymousza31785ze3z87_2049;
																				}
																			}
																		else
																			{	/* Eval/evmodule.scm 644 */
																				obj_t BgL_arg1791z00_2055;

																				{	/* Eval/evmodule.scm 644 */
																					obj_t BgL_pairz00_3705;

																					if (PAIRP(BgL_lstz00_2047))
																						{	/* Eval/evmodule.scm 644 */
																							BgL_pairz00_3705 =
																								BgL_lstz00_2047;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8142;

																							BgL_auxz00_8142 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(24466L),
																								BGl_string3116z00zz__evmodulez00,
																								BGl_string3003z00zz__evmodulez00,
																								BgL_lstz00_2047);
																							FAILURE(BgL_auxz00_8142, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1791z00_2055 =
																						CDR(BgL_pairz00_3705);
																				}
																				{
																					obj_t BgL_lstz00_8147;

																					BgL_lstz00_8147 = BgL_arg1791z00_2055;
																					BgL_lstz00_2047 = BgL_lstz00_8147;
																					goto
																						BgL_zc3z04anonymousza31785ze3z87_2049;
																				}
																			}
																	}
															}
														}
														{	/* Eval/evmodule.scm 662 */
															obj_t BgL__ortest_1079z00_2104;

															BgL__ortest_1079z00_2104 =
																BGl_locationzd2dirzd2zz__evmodulez00
																(BgL_locz00_2060);
															if (CBOOL(BgL__ortest_1079z00_2104))
																{	/* Eval/evmodule.scm 662 */
																	BgL_dirz00_2080 = BgL__ortest_1079z00_2104;
																}
															else
																{	/* Eval/evmodule.scm 662 */
																	BgL_dirz00_2080 = BGl_pwdz00zz__osz00();
																}
														}
														{	/* Eval/evmodule.scm 663 */
															obj_t BgL_pathz00_2081;

															{	/* Eval/evmodule.scm 663 */
																obj_t BgL_fun1813z00_2094;

																BgL_fun1813z00_2094 =
																	BGl_bigloozd2modulezd2resolverz00zz__modulez00
																	();
																BgL_pathz00_2081 =
																	BGL_PROCEDURE_CALL3(BgL_fun1813z00_2094,
																	BgL_imodz00_2077, BgL_filesz00_2076,
																	BgL_dirz00_2080);
															}
															{
																obj_t BgL_l1175z00_2083;

																BgL_l1175z00_2083 = BgL_aliasesz00_2079;
															BgL_zc3z04anonymousza31806ze3z87_2084:
																if (PAIRP(BgL_l1175z00_2083))
																	{	/* Eval/evmodule.scm 664 */
																		{	/* Eval/evmodule.scm 665 */
																			obj_t BgL_apz00_2086;

																			BgL_apz00_2086 = CAR(BgL_l1175z00_2083);
																			{	/* Eval/evmodule.scm 666 */
																				obj_t BgL_arg1808z00_2087;
																				obj_t BgL_arg1809z00_2088;
																				obj_t BgL_arg1810z00_2089;

																				{	/* Eval/evmodule.scm 666 */
																					obj_t BgL_pairz00_3712;

																					if (PAIRP(BgL_apz00_2086))
																						{	/* Eval/evmodule.scm 666 */
																							BgL_pairz00_3712 = BgL_apz00_2086;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8164;

																							BgL_auxz00_8164 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(25178L),
																								BGl_string3132z00zz__evmodulez00,
																								BGl_string3003z00zz__evmodulez00,
																								BgL_apz00_2086);
																							FAILURE(BgL_auxz00_8164, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1808z00_2087 =
																						CAR(BgL_pairz00_3712);
																				}
																				{	/* Eval/evmodule.scm 667 */
																					obj_t BgL_pairz00_3713;

																					if (PAIRP(BgL_apz00_2086))
																						{	/* Eval/evmodule.scm 667 */
																							BgL_pairz00_3713 = BgL_apz00_2086;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8171;

																							BgL_auxz00_8171 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string3001z00zz__evmodulez00,
																								BINT(25195L),
																								BGl_string3132z00zz__evmodulez00,
																								BGl_string3003z00zz__evmodulez00,
																								BgL_apz00_2086);
																							FAILURE(BgL_auxz00_8171, BFALSE,
																								BFALSE);
																						}
																					{	/* Eval/evmodule.scm 667 */
																						obj_t BgL_pairz00_3716;

																						{	/* Eval/evmodule.scm 667 */
																							obj_t BgL_aux2847z00_4820;

																							BgL_aux2847z00_4820 =
																								CDR(BgL_pairz00_3713);
																							if (PAIRP(BgL_aux2847z00_4820))
																								{	/* Eval/evmodule.scm 667 */
																									BgL_pairz00_3716 =
																										BgL_aux2847z00_4820;
																								}
																							else
																								{
																									obj_t BgL_auxz00_8178;

																									BgL_auxz00_8178 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string3001z00zz__evmodulez00,
																										BINT(25189L),
																										BGl_string3132z00zz__evmodulez00,
																										BGl_string3003z00zz__evmodulez00,
																										BgL_aux2847z00_4820);
																									FAILURE(BgL_auxz00_8178,
																										BFALSE, BFALSE);
																								}
																						}
																						BgL_arg1809z00_2088 =
																							CAR(BgL_pairz00_3716);
																					}
																				}
																				{	/* Eval/evmodule.scm 668 */
																					obj_t BgL__ortest_1080z00_2090;

																					BgL__ortest_1080z00_2090 =
																						BGl_getzd2sourcezd2locationz00zz__readerz00
																						(BgL_apz00_2086);
																					if (CBOOL(BgL__ortest_1080z00_2090))
																						{	/* Eval/evmodule.scm 668 */
																							BgL_arg1810z00_2089 =
																								BgL__ortest_1080z00_2090;
																						}
																					else
																						{	/* Eval/evmodule.scm 668 */
																							BgL_arg1810z00_2089 =
																								BgL_locz00_2060;
																						}
																				}
																				{	/* Eval/evmodule.scm 680 */
																					obj_t BgL_az00_3717;

																					{	/* Eval/evmodule.scm 680 */
																						obj_t BgL_idz00_3718;

																						if (SYMBOLP(BgL_arg1808z00_2087))
																							{	/* Eval/evmodule.scm 680 */
																								BgL_idz00_3718 =
																									BgL_arg1808z00_2087;
																							}
																						else
																							{
																								obj_t BgL_auxz00_8188;

																								BgL_auxz00_8188 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3001z00zz__evmodulez00,
																									BINT(25732L),
																									BGl_string3132z00zz__evmodulez00,
																									BGl_string3005z00zz__evmodulez00,
																									BgL_arg1808z00_2087);
																								FAILURE(BgL_auxz00_8188, BFALSE,
																									BFALSE);
																							}
																						{	/* Eval/evmodule.scm 680 */
																							obj_t BgL_v1213z00_3719;

																							BgL_v1213z00_3719 =
																								create_vector(5L);
																							VECTOR_SET(BgL_v1213z00_3719, 0L,
																								BINT(2L));
																							VECTOR_SET(BgL_v1213z00_3719, 1L,
																								BgL_idz00_3718);
																							VECTOR_SET(BgL_v1213z00_3719, 2L,
																								BUNSPEC);
																							VECTOR_SET(BgL_v1213z00_3719, 3L,
																								BgL_imodz00_2077);
																							VECTOR_SET(BgL_v1213z00_3719, 4L,
																								BgL_arg1810z00_2089);
																							BgL_az00_3717 = BgL_v1213z00_3719;
																						}
																					}
																					VECTOR_SET(BgL_az00_3717, 0L,
																						BINT(6L));
																					VECTOR_SET(BgL_az00_3717, 2L,
																						BgL_arg1809z00_2088);
																					{	/* Eval/evmodule.scm 683 */
																						obj_t BgL_auxz00_8202;

																						if (SYMBOLP(BgL_arg1808z00_2087))
																							{	/* Eval/evmodule.scm 683 */
																								BgL_auxz00_8202 =
																									BgL_arg1808z00_2087;
																							}
																						else
																							{
																								obj_t BgL_auxz00_8205;

																								BgL_auxz00_8205 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string3001z00zz__evmodulez00,
																									BINT(25858L),
																									BGl_string3132z00zz__evmodulez00,
																									BGl_string3005z00zz__evmodulez00,
																									BgL_arg1808z00_2087);
																								FAILURE(BgL_auxz00_8205, BFALSE,
																									BFALSE);
																							}
																						BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
																							(BgL_modz00_110, BgL_auxz00_8202,
																							BgL_az00_3717,
																							BgL_arg1810z00_2089);
																					}
																				}
																			}
																		}
																		{
																			obj_t BgL_l1175z00_8210;

																			BgL_l1175z00_8210 =
																				CDR(BgL_l1175z00_2083);
																			BgL_l1175z00_2083 = BgL_l1175z00_8210;
																			goto
																				BgL_zc3z04anonymousza31806ze3z87_2084;
																		}
																	}
																else
																	{	/* Eval/evmodule.scm 664 */
																		if (NULLP(BgL_l1175z00_2083))
																			{	/* Eval/evmodule.scm 664 */
																				BTRUE;
																			}
																		else
																			{	/* Eval/evmodule.scm 664 */
																				BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
																					(BGl_string3044z00zz__evmodulez00,
																					BGl_string3034z00zz__evmodulez00,
																					BgL_l1175z00_2083,
																					BGl_string3001z00zz__evmodulez00,
																					BINT(25117L));
																			}
																	}
															}
															BGl_evmodulezd2importz12zc0zz__evmodulez00
																(BgL_modz00_110, BgL_imodz00_2077,
																BgL_pathz00_2081, BgL_importsz00_2078,
																BgL_abasez00_2061, BgL_locz00_2060);
														}
													}
											}
									}
									{
										obj_t BgL_l1177z00_8218;

										BgL_l1177z00_8218 = CDR(BgL_l1177z00_2007);
										BgL_l1177z00_2007 = BgL_l1177z00_8218;
										goto BgL_zc3z04anonymousza31757ze3z87_2008;
									}
								}
							else
								{	/* Eval/evmodule.scm 674 */
									if (NULLP(BgL_l1177z00_2007))
										{	/* Eval/evmodule.scm 674 */
											return BTRUE;
										}
									else
										{	/* Eval/evmodule.scm 674 */
											return
												BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
												(BGl_string3044z00zz__evmodulez00,
												BGl_string3034z00zz__evmodulez00, BgL_l1177z00_2007,
												BGl_string3001z00zz__evmodulez00, BINT(25389L));
										}
								}
						}
					}
				else
					{	/* Eval/evmodule.scm 672 */
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_112,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3131z00zz__evmodulez00, BgL_clausez00_111);
					}
			}
		}

	}



/* alias-pair? */
	bool_t BGl_aliaszd2pairzf3z21zz__evmodulez00(obj_t BgL_vz00_118)
	{
		{	/* Eval/evmodule.scm 688 */
			if (PAIRP(BgL_vz00_118))
				{	/* Eval/evmodule.scm 689 */
					obj_t BgL_cdrzd2878zd2_2122;

					BgL_cdrzd2878zd2_2122 = CDR(((obj_t) BgL_vz00_118));
					{	/* Eval/evmodule.scm 689 */
						bool_t BgL_test3837z00_8229;

						{	/* Eval/evmodule.scm 689 */
							obj_t BgL_tmpz00_8230;

							BgL_tmpz00_8230 = CAR(((obj_t) BgL_vz00_118));
							BgL_test3837z00_8229 = SYMBOLP(BgL_tmpz00_8230);
						}
						if (BgL_test3837z00_8229)
							{	/* Eval/evmodule.scm 689 */
								if (PAIRP(BgL_cdrzd2878zd2_2122))
									{	/* Eval/evmodule.scm 689 */
										bool_t BgL_test3839z00_8236;

										{	/* Eval/evmodule.scm 689 */
											obj_t BgL_tmpz00_8237;

											BgL_tmpz00_8237 = CAR(BgL_cdrzd2878zd2_2122);
											BgL_test3839z00_8236 = SYMBOLP(BgL_tmpz00_8237);
										}
										if (BgL_test3839z00_8236)
											{	/* Eval/evmodule.scm 689 */
												return NULLP(CDR(BgL_cdrzd2878zd2_2122));
											}
										else
											{	/* Eval/evmodule.scm 689 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Eval/evmodule.scm 689 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Eval/evmodule.scm 689 */
								return ((bool_t) 0);
							}
					}
				}
			else
				{	/* Eval/evmodule.scm 689 */
					return ((bool_t) 0);
				}
		}

	}



/* evmodule-from! */
	obj_t BGl_evmodulezd2fromz12zc0zz__evmodulez00(obj_t BgL_modz00_119,
		obj_t BgL_identz00_120, obj_t BgL_pathz00_121, obj_t BgL_setz00_122,
		obj_t BgL_locz00_123)
	{
		{	/* Eval/evmodule.scm 696 */
			{	/* Eval/evmodule.scm 709 */
				obj_t BgL_mod2z00_2135;

				{	/* Eval/evmodule.scm 709 */
					obj_t BgL_idz00_3786;

					if (SYMBOLP(BgL_identz00_120))
						{	/* Eval/evmodule.scm 709 */
							BgL_idz00_3786 = BgL_identz00_120;
						}
					else
						{
							obj_t BgL_auxz00_8244;

							BgL_auxz00_8244 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(26917L),
								BGl_string3134z00zz__evmodulez00,
								BGl_string3005z00zz__evmodulez00, BgL_identz00_120);
							FAILURE(BgL_auxz00_8244, BFALSE, BFALSE);
						}
					if (BGl_hashtablezf3zf3zz__hashz00
						(BGl_za2moduleszd2tableza2zd2zz__evmodulez00))
						{	/* Eval/evmodule.scm 224 */
							obj_t BgL_auxz00_8250;

							{	/* Eval/evmodule.scm 224 */
								obj_t BgL_aux2861z00_4834;

								BgL_aux2861z00_4834 =
									BGl_za2moduleszd2tableza2zd2zz__evmodulez00;
								if (STRUCTP(BgL_aux2861z00_4834))
									{	/* Eval/evmodule.scm 224 */
										BgL_auxz00_8250 = BgL_aux2861z00_4834;
									}
								else
									{
										obj_t BgL_auxz00_8253;

										BgL_auxz00_8253 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(8670L),
											BGl_string3134z00zz__evmodulez00,
											BGl_string3007z00zz__evmodulez00, BgL_aux2861z00_4834);
										FAILURE(BgL_auxz00_8253, BFALSE, BFALSE);
									}
							}
							BgL_mod2z00_2135 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_auxz00_8250,
								BgL_idz00_3786);
						}
					else
						{	/* Eval/evmodule.scm 223 */
							BgL_mod2z00_2135 = BFALSE;
						}
				}
				{	/* Eval/evmodule.scm 711 */
					bool_t BgL_test3843z00_8258;

					{	/* Eval/evmodule.scm 135 */
						bool_t BgL_test3844z00_8259;

						if (STRUCTP(BgL_mod2z00_2135))
							{	/* Eval/evmodule.scm 129 */
								obj_t BgL_tmpz00_8262;

								{	/* Eval/evmodule.scm 129 */
									obj_t BgL_res2364z00_3793;

									{	/* Eval/evmodule.scm 129 */
										obj_t BgL_aux2863z00_4836;

										BgL_aux2863z00_4836 = STRUCT_KEY(BgL_mod2z00_2135);
										if (SYMBOLP(BgL_aux2863z00_4836))
											{	/* Eval/evmodule.scm 129 */
												BgL_res2364z00_3793 = BgL_aux2863z00_4836;
											}
										else
											{
												obj_t BgL_auxz00_8266;

												BgL_auxz00_8266 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(4439L),
													BGl_string3134z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00,
													BgL_aux2863z00_4836);
												FAILURE(BgL_auxz00_8266, BFALSE, BFALSE);
											}
									}
									BgL_tmpz00_8262 = BgL_res2364z00_3793;
								}
								BgL_test3844z00_8259 =
									(BgL_tmpz00_8262 == BGl_symbol2999z00zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 129 */
								BgL_test3844z00_8259 = ((bool_t) 0);
							}
						if (BgL_test3844z00_8259)
							{	/* Eval/evmodule.scm 135 */
								BgL_test3843z00_8258 =
									(STRUCT_REF(BgL_mod2z00_2135,
										(int) (0L)) ==
									BGl_makezd2z52evmodulezd2envz52zz__evmodulez00);
							}
						else
							{	/* Eval/evmodule.scm 135 */
								BgL_test3843z00_8258 = ((bool_t) 0);
							}
					}
					if (BgL_test3843z00_8258)
						{	/* Eval/evmodule.scm 711 */
							return
								BGl_fromzd2moduleze70z35zz__evmodulez00(BgL_locz00_123,
								BgL_modz00_119, BgL_setz00_122, BgL_mod2z00_2135);
						}
					else
						{	/* Eval/evmodule.scm 711 */
							if (PAIRP(BgL_pathz00_121))
								{	/* Eval/evmodule.scm 713 */
									{	/* Eval/evmodule.scm 716 */
										bool_t BgL_test3848z00_8277;

										{	/* Eval/evmodule.scm 716 */
											int BgL_arg1840z00_2141;

											BgL_arg1840z00_2141 =
												BGl_bigloozd2debugzd2modulez00zz__paramz00();
											BgL_test3848z00_8277 =
												((long) (BgL_arg1840z00_2141) > 0L);
										}
										if (BgL_test3848z00_8277)
											{	/* Eval/evmodule.scm 717 */
												obj_t BgL_port1188z00_2140;

												{	/* Eval/evmodule.scm 717 */
													obj_t BgL_tmpz00_8281;

													BgL_tmpz00_8281 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_port1188z00_2140 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8281);
												}
												bgl_display_string(BGl_string3135z00zz__evmodulez00,
													BgL_port1188z00_2140);
												bgl_display_obj(BgL_identz00_120, BgL_port1188z00_2140);
												bgl_display_string(BGl_string3136z00zz__evmodulez00,
													BgL_port1188z00_2140);
												bgl_display_obj(BgL_pathz00_121, BgL_port1188z00_2140);
												bgl_display_string(BGl_string3137z00zz__evmodulez00,
													BgL_port1188z00_2140);
												bgl_display_char(((unsigned char) 10),
													BgL_port1188z00_2140);
											}
										else
											{	/* Eval/evmodule.scm 716 */
												BFALSE;
											}
									}
									{	/* Eval/evmodule.scm 719 */
										obj_t BgL_exitd1081z00_2142;

										BgL_exitd1081z00_2142 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Eval/evmodule.scm 721 */
											obj_t BgL_zc3z04anonymousza31843ze3z87_4297;

											BgL_zc3z04anonymousza31843ze3z87_4297 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31843ze3ze5zz__evmodulez00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4297,
												(int) (0L), BgL_modz00_119);
											{	/* Eval/evmodule.scm 719 */
												obj_t BgL_arg2331z00_3804;

												{	/* Eval/evmodule.scm 719 */
													obj_t BgL_arg2333z00_3805;

													BgL_arg2333z00_3805 =
														BGL_EXITD_PROTECT(BgL_exitd1081z00_2142);
													BgL_arg2331z00_3804 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31843ze3z87_4297,
														BgL_arg2333z00_3805);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1081z00_2142,
													BgL_arg2331z00_3804);
												BUNSPEC;
											}
											{	/* Eval/evmodule.scm 720 */
												obj_t BgL_tmp1083z00_2144;

												BgL_tmp1083z00_2144 =
													BGl_fromzd2moduleze70z35zz__evmodulez00
													(BgL_locz00_123, BgL_modz00_119, BgL_setz00_122,
													BGl_evmodulezd2loadzd2zz__evmodulez00(BgL_modz00_119,
														BgL_identz00_120, BgL_pathz00_121, BgL_locz00_123));
												{	/* Eval/evmodule.scm 719 */
													bool_t BgL_test3849z00_8301;

													{	/* Eval/evmodule.scm 719 */
														obj_t BgL_arg2330z00_3807;

														BgL_arg2330z00_3807 =
															BGL_EXITD_PROTECT(BgL_exitd1081z00_2142);
														BgL_test3849z00_8301 = PAIRP(BgL_arg2330z00_3807);
													}
													if (BgL_test3849z00_8301)
														{	/* Eval/evmodule.scm 719 */
															obj_t BgL_arg2327z00_3808;

															{	/* Eval/evmodule.scm 719 */
																obj_t BgL_arg2328z00_3809;

																BgL_arg2328z00_3809 =
																	BGL_EXITD_PROTECT(BgL_exitd1081z00_2142);
																{	/* Eval/evmodule.scm 719 */
																	obj_t BgL_pairz00_3810;

																	if (PAIRP(BgL_arg2328z00_3809))
																		{	/* Eval/evmodule.scm 719 */
																			BgL_pairz00_3810 = BgL_arg2328z00_3809;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8307;

																			BgL_auxz00_8307 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(27188L),
																				BGl_string3134z00zz__evmodulez00,
																				BGl_string3003z00zz__evmodulez00,
																				BgL_arg2328z00_3809);
																			FAILURE(BgL_auxz00_8307, BFALSE, BFALSE);
																		}
																	BgL_arg2327z00_3808 = CDR(BgL_pairz00_3810);
																}
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1081z00_2142,
																BgL_arg2327z00_3808);
															BUNSPEC;
														}
													else
														{	/* Eval/evmodule.scm 719 */
															BFALSE;
														}
												}
												BGL_MODULE_SET(BgL_modz00_119);
												return BgL_tmp1083z00_2144;
											}
										}
									}
								}
							else
								{	/* Eval/evmodule.scm 713 */
									return
										BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_123,
										BGl_string3037z00zz__evmodulez00,
										BGl_string3138z00zz__evmodulez00, BgL_identz00_120);
								}
						}
				}
			}
		}

	}



/* from-module~0 */
	obj_t BGl_fromzd2moduleze70z35zz__evmodulez00(obj_t BgL_locz00_4336,
		obj_t BgL_modz00_4335, obj_t BgL_setz00_4334, obj_t BgL_mod2z00_2151)
	{
		{	/* Eval/evmodule.scm 708 */
			{	/* Eval/evmodule.scm 700 */
				obj_t BgL_exz00_2153;

				{	/* Eval/evmodule.scm 129 */
					obj_t BgL_sz00_3767;

					if (STRUCTP(BgL_mod2z00_2151))
						{	/* Eval/evmodule.scm 129 */
							BgL_sz00_3767 = BgL_mod2z00_2151;
						}
					else
						{
							obj_t BgL_auxz00_8317;

							BgL_auxz00_8317 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string3001z00zz__evmodulez00, BINT(4439L),
								BGl_string3139z00zz__evmodulez00,
								BGl_string3007z00zz__evmodulez00, BgL_mod2z00_2151);
							FAILURE(BgL_auxz00_8317, BFALSE, BFALSE);
						}
					BgL_exz00_2153 = STRUCT_REF(BgL_sz00_3767, (int) (4L));
				}
				{	/* Eval/evmodule.scm 700 */
					obj_t BgL_nxz00_2154;

					{	/* Eval/evmodule.scm 704 */
						obj_t BgL_arg1852z00_2165;

						BgL_arg1852z00_2165 = STRUCT_REF(BgL_modz00_4335, (int) (4L));
						{	/* Eval/evmodule.scm 704 */
							obj_t BgL_auxz00_8325;

							{	/* Eval/evmodule.scm 704 */
								bool_t BgL_test3852z00_8326;

								if (PAIRP(BgL_exz00_2153))
									{	/* Eval/evmodule.scm 704 */
										BgL_test3852z00_8326 = ((bool_t) 1);
									}
								else
									{	/* Eval/evmodule.scm 704 */
										BgL_test3852z00_8326 = NULLP(BgL_exz00_2153);
									}
								if (BgL_test3852z00_8326)
									{	/* Eval/evmodule.scm 704 */
										BgL_auxz00_8325 = BgL_exz00_2153;
									}
								else
									{
										obj_t BgL_auxz00_8330;

										BgL_auxz00_8330 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(26725L),
											BGl_string3139z00zz__evmodulez00,
											BGl_string3059z00zz__evmodulez00, BgL_exz00_2153);
										FAILURE(BgL_auxz00_8330, BFALSE, BFALSE);
									}
							}
							BgL_nxz00_2154 =
								BGl_appendzd221011zd2zz__evmodulez00(BgL_auxz00_8325,
								BgL_arg1852z00_2165);
						}
					}
					{	/* Eval/evmodule.scm 704 */

						{
							obj_t BgL_l1186z00_2156;

							BgL_l1186z00_2156 = BgL_exz00_2153;
						BgL_zc3z04anonymousza31846ze3z87_2157:
							if (PAIRP(BgL_l1186z00_2156))
								{	/* Eval/evmodule.scm 705 */
									{	/* Eval/evmodule.scm 706 */
										obj_t BgL_bz00_2159;

										BgL_bz00_2159 = CAR(BgL_l1186z00_2156);
										{	/* Eval/evmodule.scm 706 */
											obj_t BgL_arg1848z00_2160;
											obj_t BgL_arg1849z00_2161;

											{	/* Eval/evmodule.scm 706 */
												obj_t BgL_pairz00_3770;

												if (PAIRP(BgL_bz00_2159))
													{	/* Eval/evmodule.scm 706 */
														BgL_pairz00_3770 = BgL_bz00_2159;
													}
												else
													{
														obj_t BgL_auxz00_8340;

														BgL_auxz00_8340 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(26822L),
															BGl_string3140z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00, BgL_bz00_2159);
														FAILURE(BgL_auxz00_8340, BFALSE, BFALSE);
													}
												BgL_arg1848z00_2160 = CAR(BgL_pairz00_3770);
											}
											{	/* Eval/evmodule.scm 706 */
												obj_t BgL_pairz00_3771;

												if (PAIRP(BgL_bz00_2159))
													{	/* Eval/evmodule.scm 706 */
														BgL_pairz00_3771 = BgL_bz00_2159;
													}
												else
													{
														obj_t BgL_auxz00_8347;

														BgL_auxz00_8347 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(26830L),
															BGl_string3140z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00, BgL_bz00_2159);
														FAILURE(BgL_auxz00_8347, BFALSE, BFALSE);
													}
												BgL_arg1849z00_2161 = CAR(BgL_pairz00_3771);
											}
											{	/* Eval/evmodule.scm 680 */
												obj_t BgL_az00_3772;

												{	/* Eval/evmodule.scm 680 */
													obj_t BgL_idz00_3773;

													if (SYMBOLP(BgL_arg1848z00_2160))
														{	/* Eval/evmodule.scm 680 */
															BgL_idz00_3773 = BgL_arg1848z00_2160;
														}
													else
														{
															obj_t BgL_auxz00_8354;

															BgL_auxz00_8354 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(25732L),
																BGl_string3140z00zz__evmodulez00,
																BGl_string3005z00zz__evmodulez00,
																BgL_arg1848z00_2160);
															FAILURE(BgL_auxz00_8354, BFALSE, BFALSE);
														}
													{	/* Eval/evmodule.scm 680 */
														obj_t BgL_v1213z00_3774;

														BgL_v1213z00_3774 = create_vector(5L);
														VECTOR_SET(BgL_v1213z00_3774, 0L, BINT(2L));
														VECTOR_SET(BgL_v1213z00_3774, 1L, BgL_idz00_3773);
														VECTOR_SET(BgL_v1213z00_3774, 2L, BUNSPEC);
														VECTOR_SET(BgL_v1213z00_3774, 3L, BgL_mod2z00_2151);
														VECTOR_SET(BgL_v1213z00_3774, 4L, BgL_locz00_4336);
														BgL_az00_3772 = BgL_v1213z00_3774;
													}
												}
												VECTOR_SET(BgL_az00_3772, 0L, BINT(6L));
												VECTOR_SET(BgL_az00_3772, 2L, BgL_arg1849z00_2161);
												{	/* Eval/evmodule.scm 683 */
													obj_t BgL_auxz00_8368;

													if (SYMBOLP(BgL_arg1848z00_2160))
														{	/* Eval/evmodule.scm 683 */
															BgL_auxz00_8368 = BgL_arg1848z00_2160;
														}
													else
														{
															obj_t BgL_auxz00_8371;

															BgL_auxz00_8371 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(25858L),
																BGl_string3140z00zz__evmodulez00,
																BGl_string3005z00zz__evmodulez00,
																BgL_arg1848z00_2160);
															FAILURE(BgL_auxz00_8371, BFALSE, BFALSE);
														}
													BGl_evmodulezd2bindzd2globalz12z12zz__evmodulez00
														(BgL_modz00_4335, BgL_auxz00_8368, BgL_az00_3772,
														BgL_locz00_4336);
												}
											}
										}
									}
									{
										obj_t BgL_l1186z00_8376;

										BgL_l1186z00_8376 = CDR(BgL_l1186z00_2156);
										BgL_l1186z00_2156 = BgL_l1186z00_8376;
										goto BgL_zc3z04anonymousza31846ze3z87_2157;
									}
								}
							else
								{	/* Eval/evmodule.scm 705 */
									if (NULLP(BgL_l1186z00_2156))
										{	/* Eval/evmodule.scm 705 */
											BTRUE;
										}
									else
										{	/* Eval/evmodule.scm 705 */
											BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
												(BGl_string3044z00zz__evmodulez00,
												BGl_string3034z00zz__evmodulez00, BgL_l1186z00_2156,
												BGl_string3001z00zz__evmodulez00, BINT(26765L));
										}
								}
						}
						{	/* Eval/evmodule.scm 129 */
							obj_t BgL_xz00_4852;

							{	/* Eval/evmodule.scm 129 */
								int BgL_tmpz00_8382;

								BgL_tmpz00_8382 = (int) (4L);
								BgL_xz00_4852 =
									STRUCT_SET(BgL_modz00_4335, BgL_tmpz00_8382, BgL_nxz00_2154);
							}
							return BUNSPEC;
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1843> */
	obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4298)
	{
		{	/* Eval/evmodule.scm 719 */
			{	/* Eval/evmodule.scm 721 */
				obj_t BgL_modz00_4299;

				BgL_modz00_4299 = ((obj_t) PROCEDURE_REF(BgL_envz00_4298, (int) (0L)));
				return BGL_MODULE_SET(BgL_modz00_4299);
			}
		}

	}



/* evmodule-from */
	obj_t BGl_evmodulezd2fromzd2zz__evmodulez00(obj_t BgL_modz00_124,
		obj_t BgL_clausez00_125, obj_t BgL_locz00_126)
	{
		{	/* Eval/evmodule.scm 726 */
			{
				obj_t BgL_sz00_2200;

				if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausez00_125))
					{	/* Eval/evmodule.scm 746 */
						obj_t BgL_g1191z00_2189;

						{	/* Eval/evmodule.scm 746 */
							obj_t BgL_pairz00_3815;

							if (PAIRP(BgL_clausez00_125))
								{	/* Eval/evmodule.scm 746 */
									BgL_pairz00_3815 = BgL_clausez00_125;
								}
							else
								{
									obj_t BgL_auxz00_8393;

									BgL_auxz00_8393 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(28203L),
										BGl_string3143z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_clausez00_125);
									FAILURE(BgL_auxz00_8393, BFALSE, BFALSE);
								}
							BgL_g1191z00_2189 = CDR(BgL_pairz00_3815);
						}
						{
							obj_t BgL_l1189z00_2191;

							BgL_l1189z00_2191 = BgL_g1191z00_2189;
						BgL_zc3z04anonymousza31864ze3z87_2192:
							if (PAIRP(BgL_l1189z00_2191))
								{	/* Eval/evmodule.scm 746 */
									BgL_sz00_2200 = CAR(BgL_l1189z00_2191);
									{	/* Eval/evmodule.scm 732 */
										obj_t BgL_locz00_2202;
										obj_t BgL_abasez00_2203;

										{	/* Eval/evmodule.scm 732 */
											obj_t BgL__ortest_1085z00_2223;

											BgL__ortest_1085z00_2223 =
												BGl_getzd2sourcezd2locationz00zz__readerz00
												(BgL_sz00_2200);
											if (CBOOL(BgL__ortest_1085z00_2223))
												{	/* Eval/evmodule.scm 732 */
													BgL_locz00_2202 = BgL__ortest_1085z00_2223;
												}
											else
												{	/* Eval/evmodule.scm 732 */
													BgL_locz00_2202 = BgL_locz00_126;
												}
										}
										BgL_abasez00_2203 =
											BGl_locationzd2dirzd2zz__evmodulez00(BgL_locz00_126);
										if (SYMBOLP(BgL_sz00_2200))
											{	/* Eval/evmodule.scm 736 */
												obj_t BgL_pathz00_2205;

												{	/* Eval/evmodule.scm 736 */
													obj_t BgL_fun1873z00_2206;

													BgL_fun1873z00_2206 =
														BGl_bigloozd2modulezd2resolverz00zz__modulez00();
													BgL_pathz00_2205 =
														BGL_PROCEDURE_CALL3(BgL_fun1873z00_2206,
														BgL_sz00_2200, BNIL, BgL_abasez00_2203);
												}
												BGl_evmodulezd2fromz12zc0zz__evmodulez00(BgL_modz00_124,
													BgL_sz00_2200, BgL_pathz00_2205, BNIL,
													BgL_locz00_2202);
											}
										else
											{	/* Eval/evmodule.scm 738 */
												bool_t BgL_test3865z00_8414;

												if (PAIRP(BgL_sz00_2200))
													{	/* Eval/evmodule.scm 738 */
														if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
															(BgL_sz00_2200))
															{	/* Eval/evmodule.scm 738 */
																bool_t BgL_test3868z00_8419;

																{	/* Eval/evmodule.scm 738 */
																	obj_t BgL_tmpz00_8420;

																	BgL_tmpz00_8420 = CAR(BgL_sz00_2200);
																	BgL_test3868z00_8419 =
																		SYMBOLP(BgL_tmpz00_8420);
																}
																if (BgL_test3868z00_8419)
																	{	/* Eval/evmodule.scm 738 */
																		BgL_test3865z00_8414 = ((bool_t) 0);
																	}
																else
																	{	/* Eval/evmodule.scm 738 */
																		BgL_test3865z00_8414 = ((bool_t) 1);
																	}
															}
														else
															{	/* Eval/evmodule.scm 738 */
																BgL_test3865z00_8414 = ((bool_t) 1);
															}
													}
												else
													{	/* Eval/evmodule.scm 738 */
														BgL_test3865z00_8414 = ((bool_t) 1);
													}
												if (BgL_test3865z00_8414)
													{	/* Eval/evmodule.scm 738 */
														BGl_evcompilezd2errorzd2zz__evcompilez00
															(BgL_locz00_126, BGl_string3037z00zz__evmodulez00,
															BGl_string3141z00zz__evmodulez00, BgL_sz00_2200);
													}
												else
													{	/* Eval/evmodule.scm 741 */
														obj_t BgL_pathz00_2213;

														{	/* Eval/evmodule.scm 741 */
															obj_t BgL_fun1885z00_2215;

															BgL_fun1885z00_2215 =
																BGl_bigloozd2modulezd2resolverz00zz__modulez00
																();
															{	/* Eval/evmodule.scm 741 */
																obj_t BgL_arg1882z00_2216;
																obj_t BgL_arg1883z00_2217;
																obj_t BgL_arg1884z00_2218;

																{	/* Eval/evmodule.scm 741 */
																	obj_t BgL_pairz00_3812;

																	if (PAIRP(BgL_sz00_2200))
																		{	/* Eval/evmodule.scm 741 */
																			BgL_pairz00_3812 = BgL_sz00_2200;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8427;

																			BgL_auxz00_8427 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(28041L),
																				BGl_string3142z00zz__evmodulez00,
																				BGl_string3003z00zz__evmodulez00,
																				BgL_sz00_2200);
																			FAILURE(BgL_auxz00_8427, BFALSE, BFALSE);
																		}
																	BgL_arg1882z00_2216 = CAR(BgL_pairz00_3812);
																}
																{	/* Eval/evmodule.scm 741 */
																	obj_t BgL_pairz00_3813;

																	if (PAIRP(BgL_sz00_2200))
																		{	/* Eval/evmodule.scm 741 */
																			BgL_pairz00_3813 = BgL_sz00_2200;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8434;

																			BgL_auxz00_8434 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(28049L),
																				BGl_string3142z00zz__evmodulez00,
																				BGl_string3003z00zz__evmodulez00,
																				BgL_sz00_2200);
																			FAILURE(BgL_auxz00_8434, BFALSE, BFALSE);
																		}
																	BgL_arg1883z00_2217 = CDR(BgL_pairz00_3813);
																}
																BgL_arg1884z00_2218 = BGl_pwdz00zz__osz00();
																BgL_pathz00_2213 =
																	BGL_PROCEDURE_CALL3(BgL_fun1885z00_2215,
																	BgL_arg1882z00_2216, BgL_arg1883z00_2217,
																	BgL_arg1884z00_2218);
															}
														}
														{	/* Eval/evmodule.scm 742 */
															obj_t BgL_arg1880z00_2214;

															{	/* Eval/evmodule.scm 742 */
																obj_t BgL_pairz00_3814;

																if (PAIRP(BgL_sz00_2200))
																	{	/* Eval/evmodule.scm 742 */
																		BgL_pairz00_3814 = BgL_sz00_2200;
																	}
																else
																	{
																		obj_t BgL_auxz00_8448;

																		BgL_auxz00_8448 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(28088L),
																			BGl_string3142z00zz__evmodulez00,
																			BGl_string3003z00zz__evmodulez00,
																			BgL_sz00_2200);
																		FAILURE(BgL_auxz00_8448, BFALSE, BFALSE);
																	}
																BgL_arg1880z00_2214 = CAR(BgL_pairz00_3814);
															}
															BGl_evmodulezd2fromz12zc0zz__evmodulez00
																(BgL_modz00_124, BgL_arg1880z00_2214,
																BgL_pathz00_2213, BNIL, BgL_locz00_2202);
														}
													}
											}
									}
									{
										obj_t BgL_l1189z00_8455;

										BgL_l1189z00_8455 = CDR(BgL_l1189z00_2191);
										BgL_l1189z00_2191 = BgL_l1189z00_8455;
										goto BgL_zc3z04anonymousza31864ze3z87_2192;
									}
								}
							else
								{	/* Eval/evmodule.scm 746 */
									if (NULLP(BgL_l1189z00_2191))
										{	/* Eval/evmodule.scm 746 */
											return BTRUE;
										}
									else
										{	/* Eval/evmodule.scm 746 */
											return
												BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
												(BGl_string3044z00zz__evmodulez00,
												BGl_string3034z00zz__evmodulez00, BgL_l1189z00_2191,
												BGl_string3001z00zz__evmodulez00, BINT(28176L));
										}
								}
						}
					}
				else
					{	/* Eval/evmodule.scm 744 */
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_126,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3141z00zz__evmodulez00, BgL_clausez00_125);
					}
			}
		}

	}



/* evmodule-include */
	obj_t BGl_evmodulezd2includezd2zz__evmodulez00(obj_t BgL_modz00_127,
		obj_t BgL_clausesz00_128, obj_t BgL_locz00_129)
	{
		{	/* Eval/evmodule.scm 751 */
			{
				obj_t BgL_filesz00_2292;
				obj_t BgL_pathz00_2293;
				obj_t BgL_filez00_2268;
				obj_t BgL_pathz00_2269;

				{	/* Eval/evmodule.scm 776 */
					obj_t BgL_pathz00_2228;

					{	/* Eval/evmodule.scm 776 */
						bool_t BgL_test3873z00_8462;

						{	/* Eval/evmodule.scm 776 */
							obj_t BgL_tmpz00_8463;

							BgL_tmpz00_8463 = STRUCT_REF(BgL_modz00_127, (int) (2L));
							BgL_test3873z00_8462 = STRINGP(BgL_tmpz00_8463);
						}
						if (BgL_test3873z00_8462)
							{	/* Eval/evmodule.scm 777 */
								obj_t BgL_arg1918z00_2265;

								{	/* Eval/evmodule.scm 777 */
									obj_t BgL_arg1919z00_2266;

									BgL_arg1919z00_2266 = STRUCT_REF(BgL_modz00_127, (int) (2L));
									{	/* Eval/evmodule.scm 777 */
										obj_t BgL_auxz00_8469;

										if (STRINGP(BgL_arg1919z00_2266))
											{	/* Eval/evmodule.scm 777 */
												BgL_auxz00_8469 = BgL_arg1919z00_2266;
											}
										else
											{
												obj_t BgL_auxz00_8472;

												BgL_auxz00_8472 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(29333L),
													BGl_string3147z00zz__evmodulez00,
													BGl_string3015z00zz__evmodulez00,
													BgL_arg1919z00_2266);
												FAILURE(BgL_auxz00_8472, BFALSE, BFALSE);
											}
										BgL_arg1918z00_2265 =
											BGl_dirnamez00zz__osz00(BgL_auxz00_8469);
									}
								}
								BgL_pathz00_2228 =
									MAKE_YOUNG_PAIR(BgL_arg1918z00_2265,
									BGl_za2loadzd2pathza2zd2zz__evalz00);
							}
						else
							{	/* Eval/evmodule.scm 776 */
								BgL_pathz00_2228 = BGl_za2loadzd2pathza2zd2zz__evalz00;
							}
					}
					{
						obj_t BgL_clausesz00_2232;
						obj_t BgL_iclausesz00_2233;
						obj_t BgL_iexprsz00_2234;

						BgL_clausesz00_2232 = BgL_clausesz00_128;
						BgL_iclausesz00_2233 = BNIL;
						BgL_iexprsz00_2234 = BNIL;
					BgL_zc3z04anonymousza31888ze3z87_2235:
						if (NULLP(BgL_clausesz00_2232))
							{	/* Eval/evmodule.scm 783 */
								{	/* Eval/evmodule.scm 784 */
									int BgL_tmpz00_8480;

									BgL_tmpz00_8480 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8480);
								}
								{	/* Eval/evmodule.scm 784 */
									int BgL_tmpz00_8483;

									BgL_tmpz00_8483 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_8483, BgL_iexprsz00_2234);
								}
								return BgL_iclausesz00_2233;
							}
						else
							{	/* Eval/evmodule.scm 785 */
								bool_t BgL_test3876z00_8486;

								{	/* Eval/evmodule.scm 785 */
									obj_t BgL_tmpz00_8487;

									{	/* Eval/evmodule.scm 785 */
										obj_t BgL_pairz00_3871;

										if (PAIRP(BgL_clausesz00_2232))
											{	/* Eval/evmodule.scm 785 */
												BgL_pairz00_3871 = BgL_clausesz00_2232;
											}
										else
											{
												obj_t BgL_auxz00_8490;

												BgL_auxz00_8490 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(29524L),
													BGl_string3116z00zz__evmodulez00,
													BGl_string3003z00zz__evmodulez00,
													BgL_clausesz00_2232);
												FAILURE(BgL_auxz00_8490, BFALSE, BFALSE);
											}
										BgL_tmpz00_8487 = CAR(BgL_pairz00_3871);
									}
									BgL_test3876z00_8486 = PAIRP(BgL_tmpz00_8487);
								}
								if (BgL_test3876z00_8486)
									{	/* Eval/evmodule.scm 787 */
										bool_t BgL_test3878z00_8496;

										{	/* Eval/evmodule.scm 787 */
											obj_t BgL_tmpz00_8497;

											{	/* Eval/evmodule.scm 787 */
												obj_t BgL_pairz00_3872;

												if (PAIRP(BgL_clausesz00_2232))
													{	/* Eval/evmodule.scm 787 */
														BgL_pairz00_3872 = BgL_clausesz00_2232;
													}
												else
													{
														obj_t BgL_auxz00_8500;

														BgL_auxz00_8500 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(29626L),
															BGl_string3116z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00,
															BgL_clausesz00_2232);
														FAILURE(BgL_auxz00_8500, BFALSE, BFALSE);
													}
												{	/* Eval/evmodule.scm 787 */
													obj_t BgL_pairz00_3875;

													{	/* Eval/evmodule.scm 787 */
														obj_t BgL_aux2903z00_4877;

														BgL_aux2903z00_4877 = CAR(BgL_pairz00_3872);
														if (PAIRP(BgL_aux2903z00_4877))
															{	/* Eval/evmodule.scm 787 */
																BgL_pairz00_3875 = BgL_aux2903z00_4877;
															}
														else
															{
																obj_t BgL_auxz00_8507;

																BgL_auxz00_8507 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(29620L),
																	BGl_string3116z00zz__evmodulez00,
																	BGl_string3003z00zz__evmodulez00,
																	BgL_aux2903z00_4877);
																FAILURE(BgL_auxz00_8507, BFALSE, BFALSE);
															}
													}
													BgL_tmpz00_8497 = CAR(BgL_pairz00_3875);
												}
											}
											BgL_test3878z00_8496 =
												(BgL_tmpz00_8497 == BGl_symbol3148z00zz__evmodulez00);
										}
										if (BgL_test3878z00_8496)
											{	/* Eval/evmodule.scm 788 */
												obj_t BgL_icz00_2243;

												{	/* Eval/evmodule.scm 789 */
													obj_t BgL_arg1902z00_2253;

													{	/* Eval/evmodule.scm 789 */
														obj_t BgL_pairz00_3876;

														if (PAIRP(BgL_clausesz00_2232))
															{	/* Eval/evmodule.scm 789 */
																BgL_pairz00_3876 = BgL_clausesz00_2232;
															}
														else
															{
																obj_t BgL_auxz00_8515;

																BgL_auxz00_8515 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(29713L),
																	BGl_string3116z00zz__evmodulez00,
																	BGl_string3003z00zz__evmodulez00,
																	BgL_clausesz00_2232);
																FAILURE(BgL_auxz00_8515, BFALSE, BFALSE);
															}
														{	/* Eval/evmodule.scm 789 */
															obj_t BgL_pairz00_3879;

															{	/* Eval/evmodule.scm 789 */
																obj_t BgL_aux2907z00_4881;

																BgL_aux2907z00_4881 = CAR(BgL_pairz00_3876);
																if (PAIRP(BgL_aux2907z00_4881))
																	{	/* Eval/evmodule.scm 789 */
																		BgL_pairz00_3879 = BgL_aux2907z00_4881;
																	}
																else
																	{
																		obj_t BgL_auxz00_8522;

																		BgL_auxz00_8522 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(29707L),
																			BGl_string3116z00zz__evmodulez00,
																			BGl_string3003z00zz__evmodulez00,
																			BgL_aux2907z00_4881);
																		FAILURE(BgL_auxz00_8522, BFALSE, BFALSE);
																	}
															}
															BgL_arg1902z00_2253 = CDR(BgL_pairz00_3879);
														}
													}
													BgL_filesz00_2292 = BgL_arg1902z00_2253;
													BgL_pathz00_2293 = BgL_pathz00_2228;
													{
														obj_t BgL_filesz00_3845;
														obj_t BgL_iclausesz00_3846;
														obj_t BgL_iexprsz00_3847;

														BgL_filesz00_3845 = BgL_filesz00_2292;
														BgL_iclausesz00_3846 = BNIL;
														BgL_iexprsz00_3847 = BNIL;
													BgL_loopz00_3844:
														if (NULLP(BgL_filesz00_3845))
															{	/* Eval/evmodule.scm 770 */
																{	/* Eval/evmodule.scm 771 */
																	int BgL_tmpz00_8529;

																	BgL_tmpz00_8529 = (int) (2L);
																	BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8529);
																}
																{	/* Eval/evmodule.scm 771 */
																	int BgL_tmpz00_8532;

																	BgL_tmpz00_8532 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_8532,
																		BgL_iexprsz00_3847);
																}
																BgL_icz00_2243 = BgL_iclausesz00_3846;
															}
														else
															{	/* Eval/evmodule.scm 772 */
																obj_t BgL_icz00_3860;

																{	/* Eval/evmodule.scm 773 */
																	obj_t BgL_arg1938z00_3861;

																	{	/* Eval/evmodule.scm 773 */
																		obj_t BgL_pairz00_3866;

																		if (PAIRP(BgL_filesz00_3845))
																			{	/* Eval/evmodule.scm 773 */
																				BgL_pairz00_3866 = BgL_filesz00_3845;
																			}
																		else
																			{
																				obj_t BgL_auxz00_8537;

																				BgL_auxz00_8537 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(29162L),
																					BGl_string3116z00zz__evmodulez00,
																					BGl_string3003z00zz__evmodulez00,
																					BgL_filesz00_3845);
																				FAILURE(BgL_auxz00_8537, BFALSE,
																					BFALSE);
																			}
																		BgL_arg1938z00_3861 = CAR(BgL_pairz00_3866);
																	}
																	BgL_filez00_2268 = BgL_arg1938z00_3861;
																	BgL_pathz00_2269 = BgL_pathz00_2293;
																	{	/* Eval/evmodule.scm 754 */
																		obj_t BgL_ffilez00_2271;

																		{	/* Eval/evmodule.scm 754 */
																			obj_t BgL_auxz00_8542;

																			if (STRINGP(BgL_filez00_2268))
																				{	/* Eval/evmodule.scm 754 */
																					BgL_auxz00_8542 = BgL_filez00_2268;
																				}
																			else
																				{
																					obj_t BgL_auxz00_8545;

																					BgL_auxz00_8545 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3001z00zz__evmodulez00,
																						BINT(28562L),
																						BGl_string3144z00zz__evmodulez00,
																						BGl_string3015z00zz__evmodulez00,
																						BgL_filez00_2268);
																					FAILURE(BgL_auxz00_8545, BFALSE,
																						BFALSE);
																				}
																			BgL_ffilez00_2271 =
																				BGl_findzd2filezf2pathz20zz__osz00
																				(BgL_auxz00_8542, BgL_pathz00_2269);
																		}
																		if (STRINGP(BgL_ffilez00_2271))
																			{	/* Eval/evmodule.scm 755 */
																				BgL_icz00_3860 =
																					BGl_callzd2withzd2inputzd2filezd2zz__r4_ports_6_10_1z00
																					(BgL_ffilez00_2271,
																					BGl_proc3145z00zz__evmodulez00);
																			}
																		else
																			{	/* Eval/evmodule.scm 763 */
																				obj_t BgL_arg1930z00_2290;

																				{	/* Eval/evmodule.scm 763 */
																					obj_t BgL_list1931z00_2291;

																					BgL_list1931z00_2291 =
																						MAKE_YOUNG_PAIR(BgL_filez00_2268,
																						BNIL);
																					BgL_arg1930z00_2290 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string3146z00zz__evmodulez00,
																						BgL_list1931z00_2291);
																				}
																				BgL_icz00_3860 =
																					BGl_evcompilezd2errorzd2zz__evcompilez00
																					(BgL_locz00_129,
																					BGl_string3037z00zz__evmodulez00,
																					BgL_arg1930z00_2290,
																					BgL_pathz00_2269);
																			}
																	}
																}
																{	/* Eval/evmodule.scm 773 */
																	obj_t BgL_iez00_3862;

																	{	/* Eval/evmodule.scm 774 */
																		obj_t BgL_tmpz00_3867;

																		{	/* Eval/evmodule.scm 774 */
																			int BgL_tmpz00_8556;

																			BgL_tmpz00_8556 = (int) (1L);
																			BgL_tmpz00_3867 =
																				BGL_MVALUES_VAL(BgL_tmpz00_8556);
																		}
																		{	/* Eval/evmodule.scm 774 */
																			int BgL_tmpz00_8559;

																			BgL_tmpz00_8559 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_8559,
																				BUNSPEC);
																		}
																		BgL_iez00_3862 = BgL_tmpz00_3867;
																	}
																	{	/* Eval/evmodule.scm 774 */
																		obj_t BgL_arg1935z00_3863;
																		obj_t BgL_arg1936z00_3864;
																		obj_t BgL_arg1937z00_3865;

																		{	/* Eval/evmodule.scm 774 */
																			obj_t BgL_pairz00_3868;

																			if (PAIRP(BgL_filesz00_3845))
																				{	/* Eval/evmodule.scm 774 */
																					BgL_pairz00_3868 = BgL_filesz00_3845;
																				}
																			else
																				{
																					obj_t BgL_auxz00_8564;

																					BgL_auxz00_8564 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string3001z00zz__evmodulez00,
																						BINT(29188L),
																						BGl_string3116z00zz__evmodulez00,
																						BGl_string3003z00zz__evmodulez00,
																						BgL_filesz00_3845);
																					FAILURE(BgL_auxz00_8564, BFALSE,
																						BFALSE);
																				}
																			BgL_arg1935z00_3863 =
																				CDR(BgL_pairz00_3868);
																		}
																		{	/* Eval/evmodule.scm 774 */
																			obj_t BgL_auxz00_8569;

																			{	/* Eval/evmodule.scm 774 */
																				bool_t BgL_test3888z00_8570;

																				if (PAIRP(BgL_iclausesz00_3846))
																					{	/* Eval/evmodule.scm 774 */
																						BgL_test3888z00_8570 = ((bool_t) 1);
																					}
																				else
																					{	/* Eval/evmodule.scm 774 */
																						BgL_test3888z00_8570 =
																							NULLP(BgL_iclausesz00_3846);
																					}
																				if (BgL_test3888z00_8570)
																					{	/* Eval/evmodule.scm 774 */
																						BgL_auxz00_8569 =
																							BgL_iclausesz00_3846;
																					}
																				else
																					{
																						obj_t BgL_auxz00_8574;

																						BgL_auxz00_8574 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string3001z00zz__evmodulez00,
																							BINT(29195L),
																							BGl_string3116z00zz__evmodulez00,
																							BGl_string3059z00zz__evmodulez00,
																							BgL_iclausesz00_3846);
																						FAILURE(BgL_auxz00_8574, BFALSE,
																							BFALSE);
																					}
																			}
																			BgL_arg1936z00_3864 =
																				BGl_appendzd221011zd2zz__evmodulez00
																				(BgL_auxz00_8569, BgL_icz00_3860);
																		}
																		{	/* Eval/evmodule.scm 774 */
																			obj_t BgL_auxz00_8579;

																			{	/* Eval/evmodule.scm 774 */
																				bool_t BgL_test3890z00_8580;

																				if (PAIRP(BgL_iexprsz00_3847))
																					{	/* Eval/evmodule.scm 774 */
																						BgL_test3890z00_8580 = ((bool_t) 1);
																					}
																				else
																					{	/* Eval/evmodule.scm 774 */
																						BgL_test3890z00_8580 =
																							NULLP(BgL_iexprsz00_3847);
																					}
																				if (BgL_test3890z00_8580)
																					{	/* Eval/evmodule.scm 774 */
																						BgL_auxz00_8579 =
																							BgL_iexprsz00_3847;
																					}
																				else
																					{
																						obj_t BgL_auxz00_8584;

																						BgL_auxz00_8584 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string3001z00zz__evmodulez00,
																							BINT(29216L),
																							BGl_string3116z00zz__evmodulez00,
																							BGl_string3059z00zz__evmodulez00,
																							BgL_iexprsz00_3847);
																						FAILURE(BgL_auxz00_8584, BFALSE,
																							BFALSE);
																					}
																			}
																			BgL_arg1937z00_3865 =
																				BGl_appendzd221011zd2zz__evmodulez00
																				(BgL_auxz00_8579, BgL_iez00_3862);
																		}
																		{
																			obj_t BgL_iexprsz00_8591;
																			obj_t BgL_iclausesz00_8590;
																			obj_t BgL_filesz00_8589;

																			BgL_filesz00_8589 = BgL_arg1935z00_3863;
																			BgL_iclausesz00_8590 =
																				BgL_arg1936z00_3864;
																			BgL_iexprsz00_8591 = BgL_arg1937z00_3865;
																			BgL_iexprsz00_3847 = BgL_iexprsz00_8591;
																			BgL_iclausesz00_3846 =
																				BgL_iclausesz00_8590;
																			BgL_filesz00_3845 = BgL_filesz00_8589;
																			goto BgL_loopz00_3844;
																		}
																	}
																}
															}
													}
												}
												{	/* Eval/evmodule.scm 789 */
													obj_t BgL_iez00_2244;

													{	/* Eval/evmodule.scm 790 */
														obj_t BgL_tmpz00_3880;

														{	/* Eval/evmodule.scm 790 */
															int BgL_tmpz00_8592;

															BgL_tmpz00_8592 = (int) (1L);
															BgL_tmpz00_3880 =
																BGL_MVALUES_VAL(BgL_tmpz00_8592);
														}
														{	/* Eval/evmodule.scm 790 */
															int BgL_tmpz00_8595;

															BgL_tmpz00_8595 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_8595, BUNSPEC);
														}
														BgL_iez00_2244 = BgL_tmpz00_3880;
													}
													{	/* Eval/evmodule.scm 790 */
														obj_t BgL_ic2z00_2245;

														BgL_ic2z00_2245 =
															BGl_evmodulezd2includezd2zz__evmodulez00
															(BgL_modz00_127, BgL_icz00_2243, BgL_locz00_129);
														{	/* Eval/evmodule.scm 791 */
															obj_t BgL_ie2z00_2246;

															{	/* Eval/evmodule.scm 792 */
																obj_t BgL_tmpz00_3881;

																{	/* Eval/evmodule.scm 792 */
																	int BgL_tmpz00_8599;

																	BgL_tmpz00_8599 = (int) (1L);
																	BgL_tmpz00_3881 =
																		BGL_MVALUES_VAL(BgL_tmpz00_8599);
																}
																{	/* Eval/evmodule.scm 792 */
																	int BgL_tmpz00_8602;

																	BgL_tmpz00_8602 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_8602, BUNSPEC);
																}
																BgL_ie2z00_2246 = BgL_tmpz00_3881;
															}
															{	/* Eval/evmodule.scm 792 */
																obj_t BgL_arg1894z00_2247;
																obj_t BgL_arg1896z00_2248;
																obj_t BgL_arg1897z00_2249;

																{	/* Eval/evmodule.scm 792 */
																	obj_t BgL_pairz00_3882;

																	if (PAIRP(BgL_clausesz00_2232))
																		{	/* Eval/evmodule.scm 792 */
																			BgL_pairz00_3882 = BgL_clausesz00_2232;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8607;

																			BgL_auxz00_8607 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string3001z00zz__evmodulez00,
																				BINT(29812L),
																				BGl_string3116z00zz__evmodulez00,
																				BGl_string3003z00zz__evmodulez00,
																				BgL_clausesz00_2232);
																			FAILURE(BgL_auxz00_8607, BFALSE, BFALSE);
																		}
																	BgL_arg1894z00_2247 = CDR(BgL_pairz00_3882);
																}
																{	/* Eval/evmodule.scm 793 */
																	obj_t BgL_auxz00_8612;

																	{	/* Eval/evmodule.scm 793 */
																		bool_t BgL_test3893z00_8613;

																		if (PAIRP(BgL_iclausesz00_2233))
																			{	/* Eval/evmodule.scm 793 */
																				BgL_test3893z00_8613 = ((bool_t) 1);
																			}
																		else
																			{	/* Eval/evmodule.scm 793 */
																				BgL_test3893z00_8613 =
																					NULLP(BgL_iclausesz00_2233);
																			}
																		if (BgL_test3893z00_8613)
																			{	/* Eval/evmodule.scm 793 */
																				BgL_auxz00_8612 = BgL_iclausesz00_2233;
																			}
																		else
																			{
																				obj_t BgL_auxz00_8617;

																				BgL_auxz00_8617 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(29825L),
																					BGl_string3116z00zz__evmodulez00,
																					BGl_string3059z00zz__evmodulez00,
																					BgL_iclausesz00_2233);
																				FAILURE(BgL_auxz00_8617, BFALSE,
																					BFALSE);
																			}
																	}
																	BgL_arg1896z00_2248 =
																		BGl_appendzd221011zd2zz__evmodulez00
																		(BgL_auxz00_8612, BgL_ic2z00_2245);
																}
																{	/* Eval/evmodule.scm 794 */
																	obj_t BgL_list1898z00_2250;

																	{	/* Eval/evmodule.scm 794 */
																		obj_t BgL_arg1899z00_2251;

																		{	/* Eval/evmodule.scm 794 */
																			obj_t BgL_arg1901z00_2252;

																			BgL_arg1901z00_2252 =
																				MAKE_YOUNG_PAIR(BgL_iez00_2244, BNIL);
																			BgL_arg1899z00_2251 =
																				MAKE_YOUNG_PAIR(BgL_ie2z00_2246,
																				BgL_arg1901z00_2252);
																		}
																		BgL_list1898z00_2250 =
																			MAKE_YOUNG_PAIR(BgL_iexprsz00_2234,
																			BgL_arg1899z00_2251);
																	}
																	BgL_arg1897z00_2249 =
																		BGl_appendz00zz__r4_pairs_and_lists_6_3z00
																		(BgL_list1898z00_2250);
																}
																{
																	obj_t BgL_iexprsz00_8628;
																	obj_t BgL_iclausesz00_8627;
																	obj_t BgL_clausesz00_8626;

																	BgL_clausesz00_8626 = BgL_arg1894z00_2247;
																	BgL_iclausesz00_8627 = BgL_arg1896z00_2248;
																	BgL_iexprsz00_8628 = BgL_arg1897z00_2249;
																	BgL_iexprsz00_2234 = BgL_iexprsz00_8628;
																	BgL_iclausesz00_2233 = BgL_iclausesz00_8627;
																	BgL_clausesz00_2232 = BgL_clausesz00_8626;
																	goto BgL_zc3z04anonymousza31888ze3z87_2235;
																}
															}
														}
													}
												}
											}
										else
											{	/* Eval/evmodule.scm 796 */
												obj_t BgL_arg1903z00_2254;
												obj_t BgL_arg1904z00_2255;

												{	/* Eval/evmodule.scm 796 */
													obj_t BgL_pairz00_3883;

													if (PAIRP(BgL_clausesz00_2232))
														{	/* Eval/evmodule.scm 796 */
															BgL_pairz00_3883 = BgL_clausesz00_2232;
														}
													else
														{
															obj_t BgL_auxz00_8631;

															BgL_auxz00_8631 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(29906L),
																BGl_string3116z00zz__evmodulez00,
																BGl_string3003z00zz__evmodulez00,
																BgL_clausesz00_2232);
															FAILURE(BgL_auxz00_8631, BFALSE, BFALSE);
														}
													BgL_arg1903z00_2254 = CDR(BgL_pairz00_3883);
												}
												{	/* Eval/evmodule.scm 797 */
													obj_t BgL_arg1906z00_2256;

													{	/* Eval/evmodule.scm 797 */
														obj_t BgL_arg1910z00_2257;

														{	/* Eval/evmodule.scm 797 */
															obj_t BgL_pairz00_3884;

															if (PAIRP(BgL_clausesz00_2232))
																{	/* Eval/evmodule.scm 797 */
																	BgL_pairz00_3884 = BgL_clausesz00_2232;
																}
															else
																{
																	obj_t BgL_auxz00_8638;

																	BgL_auxz00_8638 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(29948L),
																		BGl_string3116z00zz__evmodulez00,
																		BGl_string3003z00zz__evmodulez00,
																		BgL_clausesz00_2232);
																	FAILURE(BgL_auxz00_8638, BFALSE, BFALSE);
																}
															BgL_arg1910z00_2257 = CAR(BgL_pairz00_3884);
														}
														{	/* Eval/evmodule.scm 797 */
															obj_t BgL_list1911z00_2258;

															BgL_list1911z00_2258 =
																MAKE_YOUNG_PAIR(BgL_arg1910z00_2257, BNIL);
															BgL_arg1906z00_2256 = BgL_list1911z00_2258;
														}
													}
													{	/* Eval/evmodule.scm 797 */
														obj_t BgL_auxz00_8644;

														{	/* Eval/evmodule.scm 797 */
															bool_t BgL_test3897z00_8645;

															if (PAIRP(BgL_iclausesz00_2233))
																{	/* Eval/evmodule.scm 797 */
																	BgL_test3897z00_8645 = ((bool_t) 1);
																}
															else
																{	/* Eval/evmodule.scm 797 */
																	BgL_test3897z00_8645 =
																		NULLP(BgL_iclausesz00_2233);
																}
															if (BgL_test3897z00_8645)
																{	/* Eval/evmodule.scm 797 */
																	BgL_auxz00_8644 = BgL_iclausesz00_2233;
																}
															else
																{
																	obj_t BgL_auxz00_8649;

																	BgL_auxz00_8649 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(29920L),
																		BGl_string3116z00zz__evmodulez00,
																		BGl_string3059z00zz__evmodulez00,
																		BgL_iclausesz00_2233);
																	FAILURE(BgL_auxz00_8649, BFALSE, BFALSE);
																}
														}
														BgL_arg1904z00_2255 =
															BGl_appendzd221011zd2zz__evmodulez00
															(BgL_auxz00_8644, BgL_arg1906z00_2256);
													}
												}
												{
													obj_t BgL_iclausesz00_8655;
													obj_t BgL_clausesz00_8654;

													BgL_clausesz00_8654 = BgL_arg1903z00_2254;
													BgL_iclausesz00_8655 = BgL_arg1904z00_2255;
													BgL_iclausesz00_2233 = BgL_iclausesz00_8655;
													BgL_clausesz00_2232 = BgL_clausesz00_8654;
													goto BgL_zc3z04anonymousza31888ze3z87_2235;
												}
											}
									}
								else
									{	/* Eval/evmodule.scm 786 */
										obj_t BgL_arg1913z00_2260;

										{	/* Eval/evmodule.scm 786 */
											obj_t BgL_pairz00_3886;

											if (PAIRP(BgL_clausesz00_2232))
												{	/* Eval/evmodule.scm 786 */
													BgL_pairz00_3886 = BgL_clausesz00_2232;
												}
											else
												{
													obj_t BgL_auxz00_8658;

													BgL_auxz00_8658 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string3001z00zz__evmodulez00, BINT(29598L),
														BGl_string3116z00zz__evmodulez00,
														BGl_string3003z00zz__evmodulez00,
														BgL_clausesz00_2232);
													FAILURE(BgL_auxz00_8658, BFALSE, BFALSE);
												}
											BgL_arg1913z00_2260 = CAR(BgL_pairz00_3886);
										}
										return
											BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_129,
											BGl_string3037z00zz__evmodulez00,
											BGl_string3150z00zz__evmodulez00, BgL_arg1913z00_2260);
									}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1924> */
	obj_t BGl_z62zc3z04anonymousza31924ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4304, obj_t BgL_pz00_4305)
	{
		{	/* Eval/evmodule.scm 757 */
			{	/* Eval/evmodule.scm 758 */
				obj_t BgL_e0z00_5375;

				{	/* Eval/evmodule.scm 758 */

					{	/* Eval/evmodule.scm 758 */
						obj_t BgL_iportz00_5376;

						if (INPUT_PORTP(BgL_pz00_4305))
							{	/* Eval/evmodule.scm 758 */
								BgL_iportz00_5376 = BgL_pz00_4305;
							}
						else
							{
								obj_t BgL_auxz00_8666;

								BgL_auxz00_8666 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(28660L),
									BGl_string3151z00zz__evmodulez00,
									BGl_string3152z00zz__evmodulez00, BgL_pz00_4305);
								FAILURE(BgL_auxz00_8666, BFALSE, BFALSE);
							}
						{	/* Eval/evmodule.scm 758 */

							BgL_e0z00_5375 =
								BGl_readz00zz__readerz00(BgL_iportz00_5376, BFALSE);
						}
					}
				}
				{	/* Eval/evmodule.scm 759 */
					bool_t BgL_test3901z00_8671;

					if (PAIRP(BgL_e0z00_5375))
						{	/* Eval/evmodule.scm 759 */
							BgL_test3901z00_8671 =
								(CAR(BgL_e0z00_5375) == BGl_symbol3153z00zz__evmodulez00);
						}
					else
						{	/* Eval/evmodule.scm 759 */
							BgL_test3901z00_8671 = ((bool_t) 0);
						}
					if (BgL_test3901z00_8671)
						{	/* Eval/evmodule.scm 760 */
							obj_t BgL_val0_1192z00_5377;
							obj_t BgL_val1_1193z00_5378;

							BgL_val0_1192z00_5377 = CDR(BgL_e0z00_5375);
							{	/* Eval/evmodule.scm 760 */
								obj_t BgL_auxz00_8677;

								if (INPUT_PORTP(BgL_pz00_4305))
									{	/* Eval/evmodule.scm 760 */
										BgL_auxz00_8677 = BgL_pz00_4305;
									}
								else
									{
										obj_t BgL_auxz00_8680;

										BgL_auxz00_8680 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(28766L),
											BGl_string3151z00zz__evmodulez00,
											BGl_string3152z00zz__evmodulez00, BgL_pz00_4305);
										FAILURE(BgL_auxz00_8680, BFALSE, BFALSE);
									}
								BgL_val1_1193z00_5378 =
									BGl_portzd2ze3listz31zz__readerz00
									(BGl_readzd2envzd2zz__readerz00, BgL_auxz00_8677);
							}
							{	/* Eval/evmodule.scm 760 */
								int BgL_tmpz00_8685;

								BgL_tmpz00_8685 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8685);
							}
							{	/* Eval/evmodule.scm 760 */
								int BgL_tmpz00_8688;

								BgL_tmpz00_8688 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_8688, BgL_val1_1193z00_5378);
							}
							return BgL_val0_1192z00_5377;
						}
					else
						{	/* Eval/evmodule.scm 761 */
							obj_t BgL_val1_1195z00_5379;

							{	/* Eval/evmodule.scm 761 */
								obj_t BgL_arg1928z00_5380;

								{	/* Eval/evmodule.scm 761 */
									obj_t BgL_auxz00_8691;

									if (INPUT_PORTP(BgL_pz00_4305))
										{	/* Eval/evmodule.scm 761 */
											BgL_auxz00_8691 = BgL_pz00_4305;
										}
									else
										{
											obj_t BgL_auxz00_8694;

											BgL_auxz00_8694 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(28813L),
												BGl_string3151z00zz__evmodulez00,
												BGl_string3152z00zz__evmodulez00, BgL_pz00_4305);
											FAILURE(BgL_auxz00_8694, BFALSE, BFALSE);
										}
									BgL_arg1928z00_5380 =
										BGl_portzd2ze3listz31zz__readerz00
										(BGl_readzd2envzd2zz__readerz00, BgL_auxz00_8691);
								}
								BgL_val1_1195z00_5379 =
									MAKE_YOUNG_PAIR(BgL_e0z00_5375, BgL_arg1928z00_5380);
							}
							{	/* Eval/evmodule.scm 761 */
								int BgL_tmpz00_8700;

								BgL_tmpz00_8700 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8700);
							}
							{	/* Eval/evmodule.scm 761 */
								int BgL_tmpz00_8703;

								BgL_tmpz00_8703 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_8703, BgL_val1_1195z00_5379);
							}
							return BNIL;
						}
				}
			}
		}

	}



/* evmodule-step1 */
	obj_t BGl_evmodulezd2step1zd2zz__evmodulez00(obj_t BgL_modz00_130,
		obj_t BgL_clausesz00_131, obj_t BgL_locz00_132)
	{
		{	/* Eval/evmodule.scm 803 */
			{
				obj_t BgL_l1200z00_2315;

				BgL_l1200z00_2315 = BgL_clausesz00_131;
			BgL_zc3z04anonymousza31939ze3z87_2316:
				if (PAIRP(BgL_l1200z00_2315))
					{	/* Eval/evmodule.scm 804 */
						{	/* Eval/evmodule.scm 805 */
							obj_t BgL_clausez00_2318;

							BgL_clausez00_2318 = CAR(BgL_l1200z00_2315);
							BGL_MODULE_SET(BgL_modz00_130);
							{	/* Eval/evmodule.scm 806 */
								obj_t BgL_locz00_2319;

								{	/* Eval/evmodule.scm 806 */
									obj_t BgL__ortest_1091z00_2327;

									BgL__ortest_1091z00_2327 =
										BGl_getzd2sourcezd2locationz00zz__readerz00
										(BgL_clausez00_2318);
									if (CBOOL(BgL__ortest_1091z00_2327))
										{	/* Eval/evmodule.scm 806 */
											BgL_locz00_2319 = BgL__ortest_1091z00_2327;
										}
									else
										{	/* Eval/evmodule.scm 806 */
											BgL_locz00_2319 = BgL_locz00_132;
										}
								}
								{	/* Eval/evmodule.scm 807 */
									obj_t BgL_casezd2valuezd2_2320;

									{	/* Eval/evmodule.scm 807 */
										obj_t BgL_pairz00_3888;

										if (PAIRP(BgL_clausez00_2318))
											{	/* Eval/evmodule.scm 807 */
												BgL_pairz00_3888 = BgL_clausez00_2318;
											}
										else
											{
												obj_t BgL_auxz00_8715;

												BgL_auxz00_8715 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(30366L),
													BGl_string3155z00zz__evmodulez00,
													BGl_string3003z00zz__evmodulez00, BgL_clausez00_2318);
												FAILURE(BgL_auxz00_8715, BFALSE, BFALSE);
											}
										BgL_casezd2valuezd2_2320 = CAR(BgL_pairz00_3888);
									}
									if (
										(BgL_casezd2valuezd2_2320 ==
											BGl_symbol3156z00zz__evmodulez00))
										{	/* Eval/evmodule.scm 807 */
											BGl_evmodulezd2libraryzd2zz__evmodulez00
												(BgL_clausez00_2318, BgL_locz00_2319);
										}
									else
										{	/* Eval/evmodule.scm 807 */
											if (
												(BgL_casezd2valuezd2_2320 ==
													BGl_symbol3158z00zz__evmodulez00))
												{	/* Eval/evmodule.scm 807 */
													BGl_evmodulezd2staticzd2zz__evmodulez00
														(BgL_modz00_130, BgL_clausez00_2318,
														BgL_locz00_2319, ((bool_t) 0));
												}
											else
												{	/* Eval/evmodule.scm 807 */
													if (
														(BgL_casezd2valuezd2_2320 ==
															BGl_symbol3160z00zz__evmodulez00))
														{	/* Eval/evmodule.scm 807 */
															BGl_evmodulezd2exportzd2zz__evmodulez00
																(BgL_modz00_130, BgL_clausez00_2318,
																BgL_locz00_2319, ((bool_t) 0));
														}
													else
														{	/* Eval/evmodule.scm 807 */
															if (
																(BgL_casezd2valuezd2_2320 ==
																	BGl_symbol3162z00zz__evmodulez00))
																{	/* Eval/evmodule.scm 807 */
																	BGl_evmodulezd2importzd2zz__evmodulez00
																		(BgL_modz00_130, BgL_clausez00_2318,
																		BgL_locz00_2319);
																}
															else
																{	/* Eval/evmodule.scm 807 */
																	if (
																		(BgL_casezd2valuezd2_2320 ==
																			BGl_symbol3045z00zz__evmodulez00))
																		{	/* Eval/evmodule.scm 807 */
																			BGl_evmodulezd2optionzd2zz__evmodulez00
																				(BgL_clausez00_2318, BgL_locz00_2319);
																		}
																	else
																		{	/* Eval/evmodule.scm 807 */
																			BUNSPEC;
																		}
																}
														}
												}
										}
								}
							}
						}
						{
							obj_t BgL_l1200z00_8735;

							BgL_l1200z00_8735 = CDR(BgL_l1200z00_2315);
							BgL_l1200z00_2315 = BgL_l1200z00_8735;
							goto BgL_zc3z04anonymousza31939ze3z87_2316;
						}
					}
				else
					{	/* Eval/evmodule.scm 804 */
						if (NULLP(BgL_l1200z00_2315))
							{	/* Eval/evmodule.scm 804 */
								return BTRUE;
							}
						else
							{	/* Eval/evmodule.scm 804 */
								return
									BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
									(BGl_string3044z00zz__evmodulez00,
									BGl_string3034z00zz__evmodulez00, BgL_l1200z00_2315,
									BGl_string3001z00zz__evmodulez00, BINT(30244L));
							}
					}
			}
		}

	}



/* evmodule-step2 */
	obj_t BGl_evmodulezd2step2zd2zz__evmodulez00(obj_t BgL_modz00_133,
		obj_t BgL_clausesz00_134, obj_t BgL_locz00_135)
	{
		{	/* Eval/evmodule.scm 823 */
			{
				obj_t BgL_l1202z00_2332;

				BgL_l1202z00_2332 = BgL_clausesz00_134;
			BgL_zc3z04anonymousza31948ze3z87_2333:
				if (PAIRP(BgL_l1202z00_2332))
					{	/* Eval/evmodule.scm 824 */
						{	/* Eval/evmodule.scm 825 */
							obj_t BgL_clausez00_2335;

							BgL_clausez00_2335 = CAR(BgL_l1202z00_2332);
							BGL_MODULE_SET(BgL_modz00_133);
							{	/* Eval/evmodule.scm 826 */
								obj_t BgL_locz00_2336;

								{	/* Eval/evmodule.scm 826 */
									obj_t BgL__ortest_1092z00_2343;

									BgL__ortest_1092z00_2343 =
										BGl_getzd2sourcezd2locationz00zz__readerz00
										(BgL_clausez00_2335);
									if (CBOOL(BgL__ortest_1092z00_2343))
										{	/* Eval/evmodule.scm 826 */
											BgL_locz00_2336 = BgL__ortest_1092z00_2343;
										}
									else
										{	/* Eval/evmodule.scm 826 */
											BgL_locz00_2336 = BgL_locz00_135;
										}
								}
								{	/* Eval/evmodule.scm 827 */
									obj_t BgL_casezd2valuezd2_2337;

									{	/* Eval/evmodule.scm 827 */
										obj_t BgL_pairz00_3896;

										if (PAIRP(BgL_clausez00_2335))
											{	/* Eval/evmodule.scm 827 */
												BgL_pairz00_3896 = BgL_clausez00_2335;
											}
										else
											{
												obj_t BgL_auxz00_8750;

												BgL_auxz00_8750 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(31085L),
													BGl_string3164z00zz__evmodulez00,
													BGl_string3003z00zz__evmodulez00, BgL_clausez00_2335);
												FAILURE(BgL_auxz00_8750, BFALSE, BFALSE);
											}
										BgL_casezd2valuezd2_2337 = CAR(BgL_pairz00_3896);
									}
									{	/* Eval/evmodule.scm 827 */
										bool_t BgL_test3917z00_8755;

										{	/* Eval/evmodule.scm 827 */
											bool_t BgL__ortest_1093z00_2341;

											BgL__ortest_1093z00_2341 =
												(BgL_casezd2valuezd2_2337 ==
												BGl_symbol3165z00zz__evmodulez00);
											if (BgL__ortest_1093z00_2341)
												{	/* Eval/evmodule.scm 827 */
													BgL_test3917z00_8755 = BgL__ortest_1093z00_2341;
												}
											else
												{	/* Eval/evmodule.scm 827 */
													bool_t BgL__ortest_1094z00_2342;

													BgL__ortest_1094z00_2342 =
														(BgL_casezd2valuezd2_2337 ==
														BGl_symbol3167z00zz__evmodulez00);
													if (BgL__ortest_1094z00_2342)
														{	/* Eval/evmodule.scm 827 */
															BgL_test3917z00_8755 = BgL__ortest_1094z00_2342;
														}
													else
														{	/* Eval/evmodule.scm 827 */
															BgL_test3917z00_8755 =
																(BgL_casezd2valuezd2_2337 ==
																BGl_symbol3169z00zz__evmodulez00);
														}
												}
										}
										if (BgL_test3917z00_8755)
											{	/* Eval/evmodule.scm 827 */
												BGl_evmodulezd2importzd2zz__evmodulez00(BgL_modz00_133,
													BgL_clausez00_2335, BgL_locz00_2336);
											}
										else
											{	/* Eval/evmodule.scm 827 */
												if (
													(BgL_casezd2valuezd2_2337 ==
														BGl_symbol3171z00zz__evmodulez00))
													{	/* Eval/evmodule.scm 827 */
														BGl_evmodulezd2fromzd2zz__evmodulez00
															(BgL_modz00_133, BgL_clausez00_2335,
															BgL_locz00_2336);
													}
												else
													{	/* Eval/evmodule.scm 827 */
														BUNSPEC;
													}
											}
									}
								}
							}
						}
						{
							obj_t BgL_l1202z00_8765;

							BgL_l1202z00_8765 = CDR(BgL_l1202z00_2332);
							BgL_l1202z00_2332 = BgL_l1202z00_8765;
							goto BgL_zc3z04anonymousza31948ze3z87_2333;
						}
					}
				else
					{	/* Eval/evmodule.scm 824 */
						if (NULLP(BgL_l1202z00_2332))
							{	/* Eval/evmodule.scm 824 */
								return BTRUE;
							}
						else
							{	/* Eval/evmodule.scm 824 */
								return
									BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
									(BGl_string3044z00zz__evmodulez00,
									BGl_string3034z00zz__evmodulez00, BgL_l1202z00_2332,
									BGl_string3001z00zz__evmodulez00, BINT(30963L));
							}
					}
			}
		}

	}



/* evmodule-step3 */
	obj_t BGl_evmodulezd2step3zd2zz__evmodulez00(obj_t BgL_modz00_136,
		obj_t BgL_clausesz00_137, obj_t BgL_locz00_138)
	{
		{	/* Eval/evmodule.scm 837 */
			{
				obj_t BgL_l1204z00_2348;

				BgL_l1204z00_2348 = BgL_clausesz00_137;
			BgL_zc3z04anonymousza31953ze3z87_2349:
				if (PAIRP(BgL_l1204z00_2348))
					{	/* Eval/evmodule.scm 838 */
						{	/* Eval/evmodule.scm 839 */
							obj_t BgL_clausez00_2351;

							BgL_clausez00_2351 = CAR(BgL_l1204z00_2348);
							BGL_MODULE_SET(BgL_modz00_136);
							{	/* Eval/evmodule.scm 840 */
								obj_t BgL_locz00_2352;

								{	/* Eval/evmodule.scm 840 */
									obj_t BgL__ortest_1095z00_2354;

									BgL__ortest_1095z00_2354 =
										BGl_getzd2sourcezd2locationz00zz__readerz00
										(BgL_clausez00_2351);
									if (CBOOL(BgL__ortest_1095z00_2354))
										{	/* Eval/evmodule.scm 840 */
											BgL_locz00_2352 = BgL__ortest_1095z00_2354;
										}
									else
										{	/* Eval/evmodule.scm 840 */
											BgL_locz00_2352 = BgL_locz00_138;
										}
								}
								{	/* Eval/evmodule.scm 841 */
									obj_t BgL_casezd2valuezd2_2353;

									{	/* Eval/evmodule.scm 841 */
										obj_t BgL_pairz00_3899;

										if (PAIRP(BgL_clausez00_2351))
											{	/* Eval/evmodule.scm 841 */
												BgL_pairz00_3899 = BgL_clausez00_2351;
											}
										else
											{
												obj_t BgL_auxz00_8780;

												BgL_auxz00_8780 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(31628L),
													BGl_string3173z00zz__evmodulez00,
													BGl_string3003z00zz__evmodulez00, BgL_clausez00_2351);
												FAILURE(BgL_auxz00_8780, BFALSE, BFALSE);
											}
										BgL_casezd2valuezd2_2353 = CAR(BgL_pairz00_3899);
									}
									if (
										(BgL_casezd2valuezd2_2353 ==
											BGl_symbol3158z00zz__evmodulez00))
										{	/* Eval/evmodule.scm 841 */
											BGl_evmodulezd2staticzd2zz__evmodulez00(BgL_modz00_136,
												BgL_clausez00_2351, BgL_locz00_2352, ((bool_t) 1));
										}
									else
										{	/* Eval/evmodule.scm 841 */
											if (
												(BgL_casezd2valuezd2_2353 ==
													BGl_symbol3160z00zz__evmodulez00))
												{	/* Eval/evmodule.scm 841 */
													BGl_evmodulezd2exportzd2zz__evmodulez00
														(BgL_modz00_136, BgL_clausez00_2351,
														BgL_locz00_2352, ((bool_t) 1));
												}
											else
												{	/* Eval/evmodule.scm 841 */
													BUNSPEC;
												}
										}
								}
							}
						}
						{
							obj_t BgL_l1204z00_8791;

							BgL_l1204z00_8791 = CDR(BgL_l1204z00_2348);
							BgL_l1204z00_2348 = BgL_l1204z00_8791;
							goto BgL_zc3z04anonymousza31953ze3z87_2349;
						}
					}
				else
					{	/* Eval/evmodule.scm 838 */
						if (NULLP(BgL_l1204z00_2348))
							{	/* Eval/evmodule.scm 838 */
								return BTRUE;
							}
						else
							{	/* Eval/evmodule.scm 838 */
								return
									BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
									(BGl_string3044z00zz__evmodulez00,
									BGl_string3034z00zz__evmodulez00, BgL_l1204z00_2348,
									BGl_string3001z00zz__evmodulez00, BINT(31506L));
							}
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__evmodulez00(obj_t BgL_c0z00_4329,
		obj_t BgL_locz00_4328, obj_t BgL_cz00_2371)
	{
		{	/* Eval/evmodule.scm 854 */
		BGl_loopze70ze7zz__evmodulez00:
			{	/* Eval/evmodule.scm 856 */
				bool_t BgL_test3928z00_8797;

				if (PAIRP(BgL_cz00_2371))
					{	/* Eval/evmodule.scm 856 */
						if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_cz00_2371))
							{	/* Eval/evmodule.scm 856 */
								obj_t BgL_tmpz00_8802;

								BgL_tmpz00_8802 = CAR(BgL_cz00_2371);
								BgL_test3928z00_8797 = SYMBOLP(BgL_tmpz00_8802);
							}
						else
							{	/* Eval/evmodule.scm 856 */
								BgL_test3928z00_8797 = ((bool_t) 0);
							}
					}
				else
					{	/* Eval/evmodule.scm 856 */
						BgL_test3928z00_8797 = ((bool_t) 0);
					}
				if (BgL_test3928z00_8797)
					{	/* Eval/evmodule.scm 856 */
						if ((CAR(BgL_cz00_2371) == BGl_symbol3174z00zz__evmodulez00))
							{	/* Eval/evmodule.scm 860 */
								obj_t BgL_ncz00_2379;

								BgL_ncz00_2379 =
									BGl_expandzd2oncezd2zz__expandz00(BgL_cz00_2371);
								if (PAIRP(BgL_ncz00_2379))
									{	/* Eval/evmodule.scm 862 */
										if (
											(CAR(BgL_ncz00_2379) == BGl_symbol3176z00zz__evmodulez00))
											{	/* Eval/evmodule.scm 863 */
												BGL_TAIL return
													BGl_zc3z04anonymousza31974ze3ze70z60zz__evmodulez00
													(BgL_c0z00_4329, BgL_locz00_4328,
													CDR(BgL_ncz00_2379));
											}
										else
											{
												obj_t BgL_cz00_8816;

												BgL_cz00_8816 = BgL_ncz00_2379;
												BgL_cz00_2371 = BgL_cz00_8816;
												goto BGl_loopze70ze7zz__evmodulez00;
											}
									}
								else
									{	/* Eval/evmodule.scm 862 */
										if ((BgL_ncz00_2379 == BUNSPEC))
											{	/* Eval/evmodule.scm 866 */
												return BNIL;
											}
										else
											{	/* Eval/evmodule.scm 869 */
												obj_t BgL_list1981z00_2394;

												BgL_list1981z00_2394 =
													MAKE_YOUNG_PAIR(BgL_ncz00_2379, BNIL);
												return BgL_list1981z00_2394;
											}
									}
							}
						else
							{	/* Eval/evmodule.scm 871 */
								obj_t BgL_list1982z00_2395;

								BgL_list1982z00_2395 = MAKE_YOUNG_PAIR(BgL_cz00_2371, BNIL);
								return BgL_list1982z00_2395;
							}
					}
				else
					{	/* Eval/evmodule.scm 857 */
						obj_t BgL_locz00_2397;

						{	/* Eval/evmodule.scm 857 */
							obj_t BgL__ortest_1096z00_2398;

							BgL__ortest_1096z00_2398 =
								BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_cz00_2371);
							if (CBOOL(BgL__ortest_1096z00_2398))
								{	/* Eval/evmodule.scm 857 */
									BgL_locz00_2397 = BgL__ortest_1096z00_2398;
								}
							else
								{	/* Eval/evmodule.scm 857 */
									BgL_locz00_2397 = BgL_locz00_4328;
								}
						}
						return
							BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_2397,
							BGl_string3037z00zz__evmodulez00,
							BGl_string3150z00zz__evmodulez00, BgL_c0z00_4329);
					}
			}
		}

	}



/* <@anonymous:1974>~0 */
	obj_t BGl_zc3z04anonymousza31974ze3ze70z60zz__evmodulez00(obj_t
		BgL_c0z00_4331, obj_t BgL_locz00_4330, obj_t BgL_l1207z00_2385)
	{
		{	/* Eval/evmodule.scm 864 */
			if (NULLP(BgL_l1207z00_2385))
				{	/* Eval/evmodule.scm 864 */
					return BNIL;
				}
			else
				{	/* Eval/evmodule.scm 864 */
					obj_t BgL_arg1976z00_2388;
					obj_t BgL_arg1977z00_2389;

					{	/* Eval/evmodule.scm 864 */
						obj_t BgL_arg1978z00_2390;

						{	/* Eval/evmodule.scm 864 */
							obj_t BgL_pairz00_3905;

							if (PAIRP(BgL_l1207z00_2385))
								{	/* Eval/evmodule.scm 864 */
									BgL_pairz00_3905 = BgL_l1207z00_2385;
								}
							else
								{
									obj_t BgL_auxz00_8829;

									BgL_auxz00_8829 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(32446L),
										BGl_string3178z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_l1207z00_2385);
									FAILURE(BgL_auxz00_8829, BFALSE, BFALSE);
								}
							BgL_arg1978z00_2390 = CAR(BgL_pairz00_3905);
						}
						BgL_arg1976z00_2388 =
							BGl_loopze70ze7zz__evmodulez00(BgL_c0z00_4331, BgL_locz00_4330,
							BgL_arg1978z00_2390);
					}
					{	/* Eval/evmodule.scm 864 */
						obj_t BgL_arg1979z00_2391;

						{	/* Eval/evmodule.scm 864 */
							obj_t BgL_pairz00_3906;

							if (PAIRP(BgL_l1207z00_2385))
								{	/* Eval/evmodule.scm 864 */
									BgL_pairz00_3906 = BgL_l1207z00_2385;
								}
							else
								{
									obj_t BgL_auxz00_8837;

									BgL_auxz00_8837 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(32446L),
										BGl_string3178z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_l1207z00_2385);
									FAILURE(BgL_auxz00_8837, BFALSE, BFALSE);
								}
							BgL_arg1979z00_2391 = CDR(BgL_pairz00_3906);
						}
						BgL_arg1977z00_2389 =
							BGl_zc3z04anonymousza31974ze3ze70z60zz__evmodulez00
							(BgL_c0z00_4331, BgL_locz00_4330, BgL_arg1979z00_2391);
					}
					{	/* Eval/evmodule.scm 864 */
						obj_t BgL_auxz00_8843;

						{	/* Eval/evmodule.scm 864 */
							bool_t BgL_test3939z00_8844;

							if (PAIRP(BgL_arg1976z00_2388))
								{	/* Eval/evmodule.scm 864 */
									BgL_test3939z00_8844 = ((bool_t) 1);
								}
							else
								{	/* Eval/evmodule.scm 864 */
									BgL_test3939z00_8844 = NULLP(BgL_arg1976z00_2388);
								}
							if (BgL_test3939z00_8844)
								{	/* Eval/evmodule.scm 864 */
									BgL_auxz00_8843 = BgL_arg1976z00_2388;
								}
							else
								{
									obj_t BgL_auxz00_8848;

									BgL_auxz00_8848 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(32446L),
										BGl_string3178z00zz__evmodulez00,
										BGl_string3059z00zz__evmodulez00, BgL_arg1976z00_2388);
									FAILURE(BgL_auxz00_8848, BFALSE, BFALSE);
								}
						}
						return bgl_append2(BgL_auxz00_8843, BgL_arg1977z00_2389);
					}
				}
		}

	}



/* evmodule-cond-expand-clause~0 */
	obj_t BGl_evmodulezd2condzd2expandzd2clauseze70z35zz__evmodulez00(obj_t
		BgL_locz00_4332, obj_t BgL_c0z00_2368)
	{
		{	/* Eval/evmodule.scm 871 */
			return
				BGl_loopze70ze7zz__evmodulez00(BgL_c0z00_2368, BgL_locz00_4332,
				BgL_c0z00_2368);
		}

	}



/* <@anonymous:1957>~0 */
	obj_t BGl_zc3z04anonymousza31957ze3ze70z60zz__evmodulez00(obj_t
		BgL_locz00_4333, obj_t BgL_l1210z00_2360)
	{
		{	/* Eval/evmodule.scm 873 */
			if (NULLP(BgL_l1210z00_2360))
				{	/* Eval/evmodule.scm 873 */
					return BNIL;
				}
			else
				{	/* Eval/evmodule.scm 873 */
					obj_t BgL_arg1959z00_2363;
					obj_t BgL_arg1960z00_2364;

					{	/* Eval/evmodule.scm 873 */
						obj_t BgL_arg1961z00_2365;

						{	/* Eval/evmodule.scm 873 */
							obj_t BgL_pairz00_3909;

							if (PAIRP(BgL_l1210z00_2360))
								{	/* Eval/evmodule.scm 873 */
									BgL_pairz00_3909 = BgL_l1210z00_2360;
								}
							else
								{
									obj_t BgL_auxz00_8858;

									BgL_auxz00_8858 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(32578L),
										BGl_string3179z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_l1210z00_2360);
									FAILURE(BgL_auxz00_8858, BFALSE, BFALSE);
								}
							BgL_arg1961z00_2365 = CAR(BgL_pairz00_3909);
						}
						BgL_arg1959z00_2363 =
							BGl_evmodulezd2condzd2expandzd2clauseze70z35zz__evmodulez00
							(BgL_locz00_4333, BgL_arg1961z00_2365);
					}
					{	/* Eval/evmodule.scm 873 */
						obj_t BgL_arg1962z00_2366;

						{	/* Eval/evmodule.scm 873 */
							obj_t BgL_pairz00_3910;

							if (PAIRP(BgL_l1210z00_2360))
								{	/* Eval/evmodule.scm 873 */
									BgL_pairz00_3910 = BgL_l1210z00_2360;
								}
							else
								{
									obj_t BgL_auxz00_8866;

									BgL_auxz00_8866 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(32578L),
										BGl_string3179z00zz__evmodulez00,
										BGl_string3003z00zz__evmodulez00, BgL_l1210z00_2360);
									FAILURE(BgL_auxz00_8866, BFALSE, BFALSE);
								}
							BgL_arg1962z00_2366 = CDR(BgL_pairz00_3910);
						}
						BgL_arg1960z00_2364 =
							BGl_zc3z04anonymousza31957ze3ze70z60zz__evmodulez00
							(BgL_locz00_4333, BgL_arg1962z00_2366);
					}
					{	/* Eval/evmodule.scm 873 */
						obj_t BgL_auxz00_8872;

						{	/* Eval/evmodule.scm 873 */
							bool_t BgL_test3944z00_8873;

							if (PAIRP(BgL_arg1959z00_2363))
								{	/* Eval/evmodule.scm 873 */
									BgL_test3944z00_8873 = ((bool_t) 1);
								}
							else
								{	/* Eval/evmodule.scm 873 */
									BgL_test3944z00_8873 = NULLP(BgL_arg1959z00_2363);
								}
							if (BgL_test3944z00_8873)
								{	/* Eval/evmodule.scm 873 */
									BgL_auxz00_8872 = BgL_arg1959z00_2363;
								}
							else
								{
									obj_t BgL_auxz00_8877;

									BgL_auxz00_8877 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string3001z00zz__evmodulez00, BINT(32578L),
										BGl_string3179z00zz__evmodulez00,
										BGl_string3059z00zz__evmodulez00, BgL_arg1959z00_2363);
									FAILURE(BgL_auxz00_8877, BFALSE, BFALSE);
								}
						}
						return bgl_append2(BgL_auxz00_8872, BgL_arg1960z00_2364);
					}
				}
		}

	}



/* evmodule */
	BGL_EXPORTED_DEF obj_t BGl_evmodulez00zz__evmodulez00(obj_t BgL_expz00_145,
		obj_t BgL_locz00_146)
	{
		{	/* Eval/evmodule.scm 899 */
			{	/* Eval/evmodule.scm 900 */
				obj_t BgL_locz00_2408;

				{	/* Eval/evmodule.scm 900 */
					obj_t BgL__ortest_1097z00_2436;

					BgL__ortest_1097z00_2436 =
						BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_expz00_145);
					if (CBOOL(BgL__ortest_1097z00_2436))
						{	/* Eval/evmodule.scm 900 */
							BgL_locz00_2408 = BgL__ortest_1097z00_2436;
						}
					else
						{	/* Eval/evmodule.scm 900 */
							BgL_locz00_2408 = BgL_locz00_146;
						}
				}
				{	/* Eval/evmodule.scm 900 */
					obj_t BgL_hdlz00_2409;

					BgL_hdlz00_2409 =
						BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00();
					{	/* Eval/evmodule.scm 901 */

						if (NULLP(BgL_expz00_145))
							{	/* Eval/evmodule.scm 902 */
								return
									BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_2408,
									BGl_string3037z00zz__evmodulez00,
									BGl_string3180z00zz__evmodulez00, BgL_expz00_145);
							}
						else
							{	/* Eval/evmodule.scm 902 */
								obj_t BgL_cdrzd2889zd2_2416;

								{	/* Eval/evmodule.scm 902 */
									obj_t BgL_pairz00_3929;

									BgL_pairz00_3929 = BgL_expz00_145;
									BgL_cdrzd2889zd2_2416 = CDR(BgL_pairz00_3929);
								}
								{	/* Eval/evmodule.scm 902 */
									bool_t BgL_test3948z00_8890;

									{	/* Eval/evmodule.scm 902 */
										obj_t BgL_tmpz00_8891;

										{	/* Eval/evmodule.scm 902 */
											obj_t BgL_pairz00_3930;

											BgL_pairz00_3930 = BgL_expz00_145;
											BgL_tmpz00_8891 = CAR(BgL_pairz00_3930);
										}
										BgL_test3948z00_8890 =
											(BgL_tmpz00_8891 == BGl_symbol3008z00zz__evmodulez00);
									}
									if (BgL_test3948z00_8890)
										{	/* Eval/evmodule.scm 902 */
											if (PAIRP(BgL_cdrzd2889zd2_2416))
												{	/* Eval/evmodule.scm 902 */
													obj_t BgL_carzd2892zd2_2420;

													BgL_carzd2892zd2_2420 = CAR(BgL_cdrzd2889zd2_2416);
													if (SYMBOLP(BgL_carzd2892zd2_2420))
														{	/* Eval/evmodule.scm 902 */
															return
																BGl_TAGzd2881ze70z35zz__evmodulez00
																(BgL_expz00_145, BgL_hdlz00_2409,
																BgL_locz00_2408, BgL_carzd2892zd2_2420,
																CDR(BgL_cdrzd2889zd2_2416));
														}
													else
														{	/* Eval/evmodule.scm 902 */
															return
																BGl_evcompilezd2errorzd2zz__evcompilez00
																(BgL_locz00_2408,
																BGl_string3037z00zz__evmodulez00,
																BGl_string3180z00zz__evmodulez00,
																BgL_expz00_145);
														}
												}
											else
												{	/* Eval/evmodule.scm 902 */
													return
														BGl_evcompilezd2errorzd2zz__evcompilez00
														(BgL_locz00_2408, BGl_string3037z00zz__evmodulez00,
														BGl_string3180z00zz__evmodulez00, BgL_expz00_145);
												}
										}
									else
										{	/* Eval/evmodule.scm 902 */
											return
												BGl_evcompilezd2errorzd2zz__evcompilez00
												(BgL_locz00_2408, BGl_string3037z00zz__evmodulez00,
												BGl_string3180z00zz__evmodulez00, BgL_expz00_145);
										}
								}
							}
					}
				}
			}
		}

	}



/* TAG-881~0 */
	obj_t BGl_TAGzd2881ze70z35zz__evmodulez00(obj_t BgL_expz00_4327,
		obj_t BgL_hdlz00_4326, obj_t BgL_locz00_4325, obj_t BgL_namez00_2410,
		obj_t BgL_clausesz00_2411)
	{
		{	/* Eval/evmodule.scm 902 */
			if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausesz00_2411))
				{	/* Eval/evmodule.scm 906 */
					obj_t BgL_pathz00_2425;

					{	/* Eval/evmodule.scm 906 */
						obj_t BgL__ortest_1098z00_2435;

						BgL__ortest_1098z00_2435 =
							BGl_evcompilezd2loczd2filenamez00zz__evcompilez00
							(BgL_locz00_4325);
						if (CBOOL(BgL__ortest_1098z00_2435))
							{	/* Eval/evmodule.scm 906 */
								BgL_pathz00_2425 = BgL__ortest_1098z00_2435;
							}
						else
							{	/* Eval/evmodule.scm 906 */
								BgL_pathz00_2425 = BGl_string3182z00zz__evmodulez00;
							}
					}
					{	/* Eval/evmodule.scm 906 */
						obj_t BgL_modz00_2426;

						BgL_modz00_2426 =
							BGl_makezd2evmodulezd2zz__evmodulez00(BgL_namez00_2410,
							BgL_pathz00_2425, BgL_locz00_4325);
						{	/* Eval/evmodule.scm 907 */

							{	/* Eval/evmodule.scm 908 */
								obj_t BgL_arg1994z00_2427;

								{	/* Eval/evmodule.scm 908 */
									obj_t BgL_auxz00_8910;

									if (STRINGP(BgL_pathz00_2425))
										{	/* Eval/evmodule.scm 908 */
											BgL_auxz00_8910 = BgL_pathz00_2425;
										}
									else
										{
											obj_t BgL_auxz00_8913;

											BgL_auxz00_8913 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string3001z00zz__evmodulez00, BINT(34341L),
												BGl_string3183z00zz__evmodulez00,
												BGl_string3015z00zz__evmodulez00, BgL_pathz00_2425);
											FAILURE(BgL_auxz00_8913, BFALSE, BFALSE);
										}
									BgL_arg1994z00_2427 =
										BGl_dirnamez00zz__osz00(BgL_auxz00_8910);
								}
								BGl_modulezd2loadzd2accesszd2filezd2zz__modulez00
									(BgL_arg1994z00_2427);
							}
							if (PROCEDUREP(BgL_hdlz00_4326))
								{	/* Eval/evmodule.scm 910 */
									obj_t BgL_arg1996z00_2429;

									{	/* Eval/evmodule.scm 910 */
										obj_t BgL_tmpfunz00_8924;

										{	/* Eval/evmodule.scm 910 */
											obj_t BgL_aux2951z00_4925;

											BgL_aux2951z00_4925 = BgL_hdlz00_4326;
											{	/* Eval/evmodule.scm 910 */
												bool_t BgL_test2952z00_4926;

												BgL_test2952z00_4926 = PROCEDUREP(BgL_aux2951z00_4925);
												if (BgL_test2952z00_4926)
													{	/* Eval/evmodule.scm 910 */
														BgL_tmpfunz00_8924 = BgL_aux2951z00_4925;
													}
												else
													{
														obj_t BgL_auxz00_8927;

														BgL_auxz00_8927 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(34409L),
															BGl_string3183z00zz__evmodulez00,
															BGl_string3110z00zz__evmodulez00,
															BgL_aux2951z00_4925);
														FAILURE(BgL_auxz00_8927, BFALSE, BFALSE);
													}
											}
										}
										BgL_arg1996z00_2429 =
											(VA_PROCEDUREP(BgL_tmpfunz00_8924) ? ((obj_t(*)(obj_t,
														...))
												PROCEDURE_ENTRY(BgL_tmpfunz00_8924)) (BgL_hdlz00_4326,
												BgL_expz00_4327, BEOA) : ((obj_t(*)(obj_t,
														obj_t))
												PROCEDURE_ENTRY(BgL_tmpfunz00_8924)) (BgL_hdlz00_4326,
												BgL_expz00_4327));
									}
									{	/* Eval/evmodule.scm 129 */
										int BgL_tmpz00_8931;

										BgL_tmpz00_8931 = (int) (6L);
										STRUCT_SET(BgL_modz00_2426, BgL_tmpz00_8931,
											BgL_arg1996z00_2429);
								}}
							else
								{	/* Eval/evmodule.scm 909 */
									BFALSE;
								}
							{	/* Eval/evmodule.scm 911 */
								obj_t BgL_exitd1099z00_2430;

								BgL_exitd1099z00_2430 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Eval/evmodule.scm 913 */
									obj_t BgL_zc3z04anonymousza31997ze3z87_4306;

									BgL_zc3z04anonymousza31997ze3z87_4306 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31997ze3ze5zz__evmodulez00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31997ze3z87_4306,
										(int) (0L), BgL_modz00_2426);
									{	/* Eval/evmodule.scm 911 */
										obj_t BgL_arg2331z00_3917;

										{	/* Eval/evmodule.scm 911 */
											obj_t BgL_arg2333z00_3918;

											BgL_arg2333z00_3918 =
												BGL_EXITD_PROTECT(BgL_exitd1099z00_2430);
											BgL_arg2331z00_3917 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31997ze3z87_4306,
												BgL_arg2333z00_3918);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1099z00_2430,
											BgL_arg2331z00_3917);
										BUNSPEC;
									}
									{	/* Eval/evmodule.scm 912 */
										obj_t BgL_tmp1101z00_2432;

										{	/* Eval/evmodule.scm 880 */
											obj_t BgL_mclausesz00_3919;

											BgL_mclausesz00_3919 =
												BGl_zc3z04anonymousza31957ze3ze70z60zz__evmodulez00
												(BgL_locz00_4325, BgL_clausesz00_2411);
											{	/* Eval/evmodule.scm 881 */
												obj_t BgL_iclausesz00_3920;

												BgL_iclausesz00_3920 =
													BGl_evmodulezd2includezd2zz__evmodulez00
													(BgL_modz00_2426, BgL_mclausesz00_3919,
													BgL_locz00_4325);
												{	/* Eval/evmodule.scm 882 */
													obj_t BgL_iexprsz00_3921;

													{	/* Eval/evmodule.scm 886 */
														obj_t BgL_tmpz00_3923;

														{	/* Eval/evmodule.scm 886 */
															int BgL_tmpz00_8945;

															BgL_tmpz00_8945 = (int) (1L);
															BgL_tmpz00_3923 =
																BGL_MVALUES_VAL(BgL_tmpz00_8945);
														}
														{	/* Eval/evmodule.scm 886 */
															int BgL_tmpz00_8948;

															BgL_tmpz00_8948 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_8948, BUNSPEC);
														}
														BgL_iexprsz00_3921 = BgL_tmpz00_3923;
													}
													BGl_evmodulezd2step1zd2zz__evmodulez00
														(BgL_modz00_2426, BgL_iclausesz00_3920,
														BgL_locz00_4325);
													BGl_evmodulezd2step2zd2zz__evmodulez00
														(BgL_modz00_2426, BgL_iclausesz00_3920,
														BgL_locz00_4325);
													BGl_evmodulezd2step3zd2zz__evmodulez00
														(BgL_modz00_2426, BgL_iclausesz00_3920,
														BgL_locz00_4325);
													{	/* Eval/evmodule.scm 892 */
														obj_t BgL_arg1985z00_3922;

														{	/* Eval/evmodule.scm 892 */
															obj_t BgL_auxz00_8954;

															{	/* Eval/evmodule.scm 892 */
																bool_t BgL_test3956z00_8955;

																if (PAIRP(BgL_iexprsz00_3921))
																	{	/* Eval/evmodule.scm 892 */
																		BgL_test3956z00_8955 = ((bool_t) 1);
																	}
																else
																	{	/* Eval/evmodule.scm 892 */
																		BgL_test3956z00_8955 =
																			NULLP(BgL_iexprsz00_3921);
																	}
																if (BgL_test3956z00_8955)
																	{	/* Eval/evmodule.scm 892 */
																		BgL_auxz00_8954 = BgL_iexprsz00_3921;
																	}
																else
																	{
																		obj_t BgL_auxz00_8959;

																		BgL_auxz00_8959 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(33521L),
																			BGl_string3183z00zz__evmodulez00,
																			BGl_string3059z00zz__evmodulez00,
																			BgL_iexprsz00_3921);
																		FAILURE(BgL_auxz00_8959, BFALSE, BFALSE);
																	}
															}
															BgL_arg1985z00_3922 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_auxz00_8954, BNIL);
														}
														BgL_tmp1101z00_2432 =
															MAKE_YOUNG_PAIR(BGl_symbol3176z00zz__evmodulez00,
															BgL_arg1985z00_3922);
													}
												}
											}
										}
										{	/* Eval/evmodule.scm 911 */
											bool_t BgL_test3958z00_8965;

											{	/* Eval/evmodule.scm 911 */
												obj_t BgL_arg2330z00_3925;

												BgL_arg2330z00_3925 =
													BGL_EXITD_PROTECT(BgL_exitd1099z00_2430);
												BgL_test3958z00_8965 = PAIRP(BgL_arg2330z00_3925);
											}
											if (BgL_test3958z00_8965)
												{	/* Eval/evmodule.scm 911 */
													obj_t BgL_arg2327z00_3926;

													{	/* Eval/evmodule.scm 911 */
														obj_t BgL_arg2328z00_3927;

														BgL_arg2328z00_3927 =
															BGL_EXITD_PROTECT(BgL_exitd1099z00_2430);
														{	/* Eval/evmodule.scm 911 */
															obj_t BgL_pairz00_3928;

															if (PAIRP(BgL_arg2328z00_3927))
																{	/* Eval/evmodule.scm 911 */
																	BgL_pairz00_3928 = BgL_arg2328z00_3927;
																}
															else
																{
																	obj_t BgL_auxz00_8971;

																	BgL_auxz00_8971 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string3001z00zz__evmodulez00,
																		BINT(34424L),
																		BGl_string3183z00zz__evmodulez00,
																		BGl_string3003z00zz__evmodulez00,
																		BgL_arg2328z00_3927);
																	FAILURE(BgL_auxz00_8971, BFALSE, BFALSE);
																}
															BgL_arg2327z00_3926 = CDR(BgL_pairz00_3928);
														}
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1099z00_2430,
														BgL_arg2327z00_3926);
													BUNSPEC;
												}
											else
												{	/* Eval/evmodule.scm 911 */
													BFALSE;
												}
										}
										BGL_MODULE_SET(BgL_modz00_2426);
										return BgL_tmp1101z00_2432;
									}
								}
							}
						}
					}
				}
			else
				{	/* Eval/evmodule.scm 904 */
					return
						BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_locz00_4325,
						BGl_string3037z00zz__evmodulez00, BGl_string3184z00zz__evmodulez00,
						BgL_clausesz00_2411);
				}
		}

	}



/* &evmodule */
	obj_t BGl_z62evmodulez62zz__evmodulez00(obj_t BgL_envz00_4307,
		obj_t BgL_expz00_4308, obj_t BgL_locz00_4309)
	{
		{	/* Eval/evmodule.scm 899 */
			{	/* Eval/evmodule.scm 900 */
				obj_t BgL_auxz00_8979;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_expz00_4308))
					{	/* Eval/evmodule.scm 900 */
						BgL_auxz00_8979 = BgL_expz00_4308;
					}
				else
					{
						obj_t BgL_auxz00_8982;

						BgL_auxz00_8982 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(33942L), BGl_string3185z00zz__evmodulez00,
							BGl_string3059z00zz__evmodulez00, BgL_expz00_4308);
						FAILURE(BgL_auxz00_8982, BFALSE, BFALSE);
					}
				return BGl_evmodulez00zz__evmodulez00(BgL_auxz00_8979, BgL_locz00_4309);
			}
		}

	}



/* &<@anonymous:1997> */
	obj_t BGl_z62zc3z04anonymousza31997ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4310)
	{
		{	/* Eval/evmodule.scm 911 */
			{	/* Eval/evmodule.scm 913 */
				obj_t BgL_modz00_4311;

				BgL_modz00_4311 = ((obj_t) PROCEDURE_REF(BgL_envz00_4310, (int) (0L)));
				return BGL_MODULE_SET(BgL_modz00_4311);
			}
		}

	}



/* evmodule-comp! */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2compz12zc0zz__evmodulez00(obj_t
		BgL_idz00_147, obj_t BgL_pathz00_148, obj_t BgL_locz00_149,
		obj_t BgL_exportsz00_150)
	{
		{	/* Eval/evmodule.scm 920 */
			{	/* Eval/evmodule.scm 921 */
				obj_t BgL_modz00_2437;

				BgL_modz00_2437 =
					BGl_makezd2evmodulezd2zz__evmodulez00(BgL_idz00_147, BgL_pathz00_148,
					BgL_locz00_149);
				{
					obj_t BgL_l1211z00_2439;

					BgL_l1211z00_2439 = BgL_exportsz00_150;
				BgL_zc3z04anonymousza31998ze3z87_2440:
					if (PAIRP(BgL_l1211z00_2439))
						{	/* Eval/evmodule.scm 922 */
							{	/* Eval/evmodule.scm 923 */
								obj_t BgL_gz00_2442;

								BgL_gz00_2442 = CAR(BgL_l1211z00_2439);
								{	/* Eval/evmodule.scm 923 */
									obj_t BgL_idz00_2443;
									obj_t BgL_valz00_2444;

									{	/* Eval/evmodule.scm 923 */
										obj_t BgL_evalzd2globalzd2_3934;

										if (VECTORP(BgL_gz00_2442))
											{	/* Eval/evmodule.scm 923 */
												BgL_evalzd2globalzd2_3934 = BgL_gz00_2442;
											}
										else
											{
												obj_t BgL_auxz00_8997;

												BgL_auxz00_8997 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(34965L),
													BGl_string3186z00zz__evmodulez00,
													BGl_string3025z00zz__evmodulez00, BgL_gz00_2442);
												FAILURE(BgL_auxz00_8997, BFALSE, BFALSE);
											}
										BgL_idz00_2443 = VECTOR_REF(BgL_evalzd2globalzd2_3934, 1L);
									}
									{	/* Eval/evmodule.scm 924 */
										obj_t BgL_evalzd2globalzd2_3936;

										if (VECTORP(BgL_gz00_2442))
											{	/* Eval/evmodule.scm 924 */
												BgL_evalzd2globalzd2_3936 = BgL_gz00_2442;
											}
										else
											{
												obj_t BgL_auxz00_9004;

												BgL_auxz00_9004 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(34997L),
													BGl_string3186z00zz__evmodulez00,
													BGl_string3025z00zz__evmodulez00, BgL_gz00_2442);
												FAILURE(BgL_auxz00_9004, BFALSE, BFALSE);
											}
										BgL_valz00_2444 = VECTOR_REF(BgL_evalzd2globalzd2_3936, 2L);
									}
									if (BGl_classzf3zf3zz__objectz00(BgL_valz00_2444))
										{	/* Eval/evmodule.scm 925 */
											{	/* Eval/evmodule.scm 926 */
												obj_t BgL_auxz00_9011;

												if (BGl_classzf3zf3zz__objectz00(BgL_valz00_2444))
													{	/* Eval/evmodule.scm 926 */
														BgL_auxz00_9011 = BgL_valz00_2444;
													}
												else
													{
														obj_t BgL_auxz00_9014;

														BgL_auxz00_9014 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(35058L),
															BGl_string3186z00zz__evmodulez00,
															BGl_string3057z00zz__evmodulez00,
															BgL_valz00_2444);
														FAILURE(BgL_auxz00_9014, BFALSE, BFALSE);
													}
												BGl_evalzd2expandzd2instantiatez00zz__evobjectz00
													(BgL_auxz00_9011);
											}
											{	/* Eval/evmodule.scm 927 */
												obj_t BgL_auxz00_9019;

												if (BGl_classzf3zf3zz__objectz00(BgL_valz00_2444))
													{	/* Eval/evmodule.scm 927 */
														BgL_auxz00_9019 = BgL_valz00_2444;
													}
												else
													{
														obj_t BgL_auxz00_9022;

														BgL_auxz00_9022 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(35090L),
															BGl_string3186z00zz__evmodulez00,
															BGl_string3057z00zz__evmodulez00,
															BgL_valz00_2444);
														FAILURE(BgL_auxz00_9022, BFALSE, BFALSE);
													}
												BGl_evalzd2expandzd2duplicatez00zz__evobjectz00
													(BgL_auxz00_9019);
											}
											{	/* Eval/evmodule.scm 928 */
												obj_t BgL_auxz00_9027;

												if (BGl_classzf3zf3zz__objectz00(BgL_valz00_2444))
													{	/* Eval/evmodule.scm 928 */
														BgL_auxz00_9027 = BgL_valz00_2444;
													}
												else
													{
														obj_t BgL_auxz00_9030;

														BgL_auxz00_9030 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(35124L),
															BGl_string3186z00zz__evmodulez00,
															BGl_string3057z00zz__evmodulez00,
															BgL_valz00_2444);
														FAILURE(BgL_auxz00_9030, BFALSE, BFALSE);
													}
												BGl_evalzd2expandzd2withzd2accesszd2zz__evobjectz00
													(BgL_auxz00_9027);
											}
										}
									else
										{	/* Eval/evmodule.scm 925 */
											BFALSE;
										}
									{	/* Eval/evmodule.scm 381 */
										obj_t BgL_arg1489z00_3938;

										{	/* Eval/evmodule.scm 381 */
											obj_t BgL_arg1490z00_3939;
											obj_t BgL_arg1492z00_3940;

											BgL_arg1490z00_3939 =
												MAKE_YOUNG_PAIR(BgL_idz00_2443, BgL_gz00_2442);
											BgL_arg1492z00_3940 =
												STRUCT_REF(BgL_modz00_2437, (int) (4L));
											BgL_arg1489z00_3938 =
												MAKE_YOUNG_PAIR(BgL_arg1490z00_3939,
												BgL_arg1492z00_3940);
										}
										{	/* Eval/evmodule.scm 129 */
											int BgL_tmpz00_9039;

											BgL_tmpz00_9039 = (int) (4L);
											STRUCT_SET(BgL_modz00_2437, BgL_tmpz00_9039,
												BgL_arg1489z00_3938);
									}}
									{	/* Eval/evmodule.scm 930 */
										obj_t BgL_idz00_3943;

										if (SYMBOLP(BgL_idz00_2443))
											{	/* Eval/evmodule.scm 930 */
												BgL_idz00_3943 = BgL_idz00_2443;
											}
										else
											{
												obj_t BgL_auxz00_9044;

												BgL_auxz00_9044 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string3001z00zz__evmodulez00, BINT(35201L),
													BGl_string3186z00zz__evmodulez00,
													BGl_string3005z00zz__evmodulez00, BgL_idz00_2443);
												FAILURE(BgL_auxz00_9044, BFALSE, BFALSE);
											}
										if (CBOOL(BGl_getzd2evalzd2expanderz00zz__macroz00
												(BgL_idz00_3943)))
											{	/* Eval/evmodule.scm 262 */
												obj_t BgL_msgz00_3945;

												{	/* Eval/evmodule.scm 262 */
													obj_t BgL_arg1392z00_3946;

													{	/* Eval/evmodule.scm 262 */
														obj_t BgL_arg2162z00_3951;

														BgL_arg2162z00_3951 =
															SYMBOL_TO_STRING(BgL_idz00_3943);
														BgL_arg1392z00_3946 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg2162z00_3951);
													}
													BgL_msgz00_3945 =
														string_append_3(BGl_string3029z00zz__evmodulez00,
														BgL_arg1392z00_3946,
														BGl_string3030z00zz__evmodulez00);
												}
												{	/* Eval/evmodule.scm 264 */
													obj_t BgL_list1391z00_3947;

													BgL_list1391z00_3947 =
														MAKE_YOUNG_PAIR(BgL_msgz00_3945, BNIL);
													BGl_evwarningz00zz__everrorz00(BgL_locz00_149,
														BgL_list1391z00_3947);
												}
											}
										else
											{	/* Eval/evmodule.scm 261 */
												BFALSE;
											}
										if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_modz00_2437))
											{	/* Eval/evmodule.scm 266 */
												obj_t BgL_arg1394z00_3949;

												BgL_arg1394z00_3949 =
													STRUCT_REF(BgL_modz00_2437, (int) (3L));
												{	/* Eval/evmodule.scm 266 */
													obj_t BgL_auxz00_9060;

													if (STRUCTP(BgL_arg1394z00_3949))
														{	/* Eval/evmodule.scm 266 */
															BgL_auxz00_9060 = BgL_arg1394z00_3949;
														}
													else
														{
															obj_t BgL_auxz00_9063;

															BgL_auxz00_9063 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string3001z00zz__evmodulez00, BINT(10468L),
																BGl_string3186z00zz__evmodulez00,
																BGl_string3007z00zz__evmodulez00,
																BgL_arg1394z00_3949);
															FAILURE(BgL_auxz00_9063, BFALSE, BFALSE);
														}
													BGl_hashtablezd2putz12zc0zz__hashz00(BgL_auxz00_9060,
														BgL_idz00_3943, BgL_gz00_2442);
												}
											}
										else
											{	/* Eval/evmodule.scm 267 */
												obj_t BgL_auxz00_9068;

												if (VECTORP(BgL_gz00_2442))
													{	/* Eval/evmodule.scm 267 */
														BgL_auxz00_9068 = BgL_gz00_2442;
													}
												else
													{
														obj_t BgL_auxz00_9071;

														BgL_auxz00_9071 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(10507L),
															BGl_string3186z00zz__evmodulez00,
															BGl_string3025z00zz__evmodulez00, BgL_gz00_2442);
														FAILURE(BgL_auxz00_9071, BFALSE, BFALSE);
													}
												BGl_bindzd2evalzd2globalz12z12zz__evenvz00
													(BgL_idz00_3943, BgL_auxz00_9068);
											}
									}
								}
							}
							{
								obj_t BgL_l1211z00_9076;

								BgL_l1211z00_9076 = CDR(BgL_l1211z00_2439);
								BgL_l1211z00_2439 = BgL_l1211z00_9076;
								goto BgL_zc3z04anonymousza31998ze3z87_2440;
							}
						}
					else
						{	/* Eval/evmodule.scm 922 */
							if (NULLP(BgL_l1211z00_2439))
								{	/* Eval/evmodule.scm 922 */
									BTRUE;
								}
							else
								{	/* Eval/evmodule.scm 922 */
									BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
										(BGl_string3044z00zz__evmodulez00,
										BGl_string3034z00zz__evmodulez00, BgL_l1211z00_2439,
										BGl_string3001z00zz__evmodulez00, BINT(34910L));
								}
						}
				}
				return BFALSE;
			}
		}

	}



/* &evmodule-comp! */
	obj_t BGl_z62evmodulezd2compz12za2zz__evmodulez00(obj_t BgL_envz00_4312,
		obj_t BgL_idz00_4313, obj_t BgL_pathz00_4314, obj_t BgL_locz00_4315,
		obj_t BgL_exportsz00_4316)
	{
		{	/* Eval/evmodule.scm 920 */
			{	/* Eval/evmodule.scm 921 */
				obj_t BgL_auxz00_9089;
				obj_t BgL_auxz00_9082;

				if (PAIRP(BgL_pathz00_4314))
					{	/* Eval/evmodule.scm 921 */
						BgL_auxz00_9089 = BgL_pathz00_4314;
					}
				else
					{
						obj_t BgL_auxz00_9092;

						BgL_auxz00_9092 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(34863L), BGl_string3187z00zz__evmodulez00,
							BGl_string3003z00zz__evmodulez00, BgL_pathz00_4314);
						FAILURE(BgL_auxz00_9092, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_idz00_4313))
					{	/* Eval/evmodule.scm 921 */
						BgL_auxz00_9082 = BgL_idz00_4313;
					}
				else
					{
						obj_t BgL_auxz00_9085;

						BgL_auxz00_9085 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(34863L), BGl_string3187z00zz__evmodulez00,
							BGl_string3005z00zz__evmodulez00, BgL_idz00_4313);
						FAILURE(BgL_auxz00_9085, BFALSE, BFALSE);
					}
				return
					BGl_evmodulezd2compz12zc0zz__evmodulez00(BgL_auxz00_9082,
					BgL_auxz00_9089, BgL_locz00_4315, BgL_exportsz00_4316);
			}
		}

	}



/* evmodule-static-class */
	BGL_EXPORTED_DEF obj_t BGl_evmodulezd2staticzd2classz00zz__evmodulez00(obj_t
		BgL_xz00_151)
	{
		{	/* Eval/evmodule.scm 937 */
			{	/* Eval/evmodule.scm 938 */
				obj_t BgL_modz00_2449;

				BgL_modz00_2449 = BGL_MODULE();
				{	/* Eval/evmodule.scm 938 */
					obj_t BgL_restz00_2450;

					{	/* Eval/evmodule.scm 939 */
						obj_t BgL_pairz00_3954;

						if (NULLP(BgL_xz00_151))
							{
								obj_t BgL_auxz00_9100;

								BgL_auxz00_9100 =
									BGl_typezd2errorzd2zz__errorz00
									(BGl_string3001z00zz__evmodulez00, BINT(35536L),
									BGl_string3188z00zz__evmodulez00,
									BGl_string3003z00zz__evmodulez00, BgL_xz00_151);
								FAILURE(BgL_auxz00_9100, BFALSE, BFALSE);
							}
						else
							{	/* Eval/evmodule.scm 939 */
								BgL_pairz00_3954 = BgL_xz00_151;
							}
						BgL_restz00_2450 = CDR(BgL_pairz00_3954);
					}
					{	/* Eval/evmodule.scm 939 */
						obj_t BgL_clausez00_2451;

						{	/* Eval/evmodule.scm 940 */
							obj_t BgL_casezd2valuezd2_2453;

							{	/* Eval/evmodule.scm 940 */
								obj_t BgL_pairz00_3955;

								if (NULLP(BgL_xz00_151))
									{
										obj_t BgL_auxz00_9107;

										BgL_auxz00_9107 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string3001z00zz__evmodulez00, BINT(35562L),
											BGl_string3188z00zz__evmodulez00,
											BGl_string3003z00zz__evmodulez00, BgL_xz00_151);
										FAILURE(BgL_auxz00_9107, BFALSE, BFALSE);
									}
								else
									{	/* Eval/evmodule.scm 940 */
										BgL_pairz00_3955 = BgL_xz00_151;
									}
								BgL_casezd2valuezd2_2453 = CAR(BgL_pairz00_3955);
							}
							if (
								(BgL_casezd2valuezd2_2453 == BGl_symbol3189z00zz__evmodulez00))
								{	/* Eval/evmodule.scm 942 */
									obj_t BgL_arg2006z00_2455;

									{	/* Eval/evmodule.scm 942 */
										obj_t BgL_arg2007z00_2456;

										{	/* Eval/evmodule.scm 942 */
											obj_t BgL_arg2008z00_2457;

											{	/* Eval/evmodule.scm 942 */
												obj_t BgL_arg2009z00_2458;

												{	/* Eval/evmodule.scm 942 */
													obj_t BgL_auxz00_9114;

													{	/* Eval/evmodule.scm 942 */
														bool_t BgL_test3979z00_9115;

														if (PAIRP(BgL_restz00_2450))
															{	/* Eval/evmodule.scm 942 */
																BgL_test3979z00_9115 = ((bool_t) 1);
															}
														else
															{	/* Eval/evmodule.scm 942 */
																BgL_test3979z00_9115 = NULLP(BgL_restz00_2450);
															}
														if (BgL_test3979z00_9115)
															{	/* Eval/evmodule.scm 942 */
																BgL_auxz00_9114 = BgL_restz00_2450;
															}
														else
															{
																obj_t BgL_auxz00_9119;

																BgL_auxz00_9119 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string3001z00zz__evmodulez00,
																	BINT(35617L),
																	BGl_string3188z00zz__evmodulez00,
																	BGl_string3059z00zz__evmodulez00,
																	BgL_restz00_2450);
																FAILURE(BgL_auxz00_9119, BFALSE, BFALSE);
															}
													}
													BgL_arg2009z00_2458 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_auxz00_9114, BNIL);
												}
												BgL_arg2008z00_2457 =
													MAKE_YOUNG_PAIR(BGl_symbol3056z00zz__evmodulez00,
													BgL_arg2009z00_2458);
											}
											BgL_arg2007z00_2456 =
												MAKE_YOUNG_PAIR(BgL_arg2008z00_2457, BNIL);
										}
										BgL_arg2006z00_2455 =
											MAKE_YOUNG_PAIR(BGl_symbol3158z00zz__evmodulez00,
											BgL_arg2007z00_2456);
									}
									BgL_clausez00_2451 =
										BGl_evepairifyz00zz__prognz00(BgL_arg2006z00_2455,
										BgL_xz00_151);
								}
							else
								{	/* Eval/evmodule.scm 940 */
									if (
										(BgL_casezd2valuezd2_2453 ==
											BGl_symbol3191z00zz__evmodulez00))
										{	/* Eval/evmodule.scm 944 */
											obj_t BgL_arg2011z00_2460;

											{	/* Eval/evmodule.scm 944 */
												obj_t BgL_arg2012z00_2461;

												{	/* Eval/evmodule.scm 944 */
													obj_t BgL_arg2013z00_2462;

													{	/* Eval/evmodule.scm 944 */
														obj_t BgL_arg2014z00_2463;

														{	/* Eval/evmodule.scm 944 */
															obj_t BgL_auxz00_9130;

															{	/* Eval/evmodule.scm 944 */
																bool_t BgL_test3982z00_9131;

																if (PAIRP(BgL_restz00_2450))
																	{	/* Eval/evmodule.scm 944 */
																		BgL_test3982z00_9131 = ((bool_t) 1);
																	}
																else
																	{	/* Eval/evmodule.scm 944 */
																		BgL_test3982z00_9131 =
																			NULLP(BgL_restz00_2450);
																	}
																if (BgL_test3982z00_9131)
																	{	/* Eval/evmodule.scm 944 */
																		BgL_auxz00_9130 = BgL_restz00_2450;
																	}
																else
																	{
																		obj_t BgL_auxz00_9135;

																		BgL_auxz00_9135 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string3001z00zz__evmodulez00,
																			BINT(35695L),
																			BGl_string3188z00zz__evmodulez00,
																			BGl_string3059z00zz__evmodulez00,
																			BgL_restz00_2450);
																		FAILURE(BgL_auxz00_9135, BFALSE, BFALSE);
																	}
															}
															BgL_arg2014z00_2463 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_auxz00_9130, BNIL);
														}
														BgL_arg2013z00_2462 =
															MAKE_YOUNG_PAIR(BGl_symbol3060z00zz__evmodulez00,
															BgL_arg2014z00_2463);
													}
													BgL_arg2012z00_2461 =
														MAKE_YOUNG_PAIR(BgL_arg2013z00_2462, BNIL);
												}
												BgL_arg2011z00_2460 =
													MAKE_YOUNG_PAIR(BGl_symbol3158z00zz__evmodulez00,
													BgL_arg2012z00_2461);
											}
											BgL_clausez00_2451 =
												BGl_evepairifyz00zz__prognz00(BgL_arg2011z00_2460,
												BgL_xz00_151);
										}
									else
										{	/* Eval/evmodule.scm 940 */
											if (
												(BgL_casezd2valuezd2_2453 ==
													BGl_symbol3193z00zz__evmodulez00))
												{	/* Eval/evmodule.scm 946 */
													obj_t BgL_arg2016z00_2465;

													{	/* Eval/evmodule.scm 946 */
														obj_t BgL_arg2017z00_2466;

														{	/* Eval/evmodule.scm 946 */
															obj_t BgL_arg2018z00_2467;

															{	/* Eval/evmodule.scm 946 */
																obj_t BgL_arg2019z00_2468;

																{	/* Eval/evmodule.scm 946 */
																	obj_t BgL_auxz00_9146;

																	{	/* Eval/evmodule.scm 946 */
																		bool_t BgL_test3985z00_9147;

																		if (PAIRP(BgL_restz00_2450))
																			{	/* Eval/evmodule.scm 946 */
																				BgL_test3985z00_9147 = ((bool_t) 1);
																			}
																		else
																			{	/* Eval/evmodule.scm 946 */
																				BgL_test3985z00_9147 =
																					NULLP(BgL_restz00_2450);
																			}
																		if (BgL_test3985z00_9147)
																			{	/* Eval/evmodule.scm 946 */
																				BgL_auxz00_9146 = BgL_restz00_2450;
																			}
																		else
																			{
																				obj_t BgL_auxz00_9151;

																				BgL_auxz00_9151 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string3001z00zz__evmodulez00,
																					BINT(35782L),
																					BGl_string3188z00zz__evmodulez00,
																					BGl_string3059z00zz__evmodulez00,
																					BgL_restz00_2450);
																				FAILURE(BgL_auxz00_9151, BFALSE,
																					BFALSE);
																			}
																	}
																	BgL_arg2019z00_2468 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_auxz00_9146, BNIL);
																}
																BgL_arg2018z00_2467 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol3062z00zz__evmodulez00,
																	BgL_arg2019z00_2468);
															}
															BgL_arg2017z00_2466 =
																MAKE_YOUNG_PAIR(BgL_arg2018z00_2467, BNIL);
														}
														BgL_arg2016z00_2465 =
															MAKE_YOUNG_PAIR(BGl_symbol3158z00zz__evmodulez00,
															BgL_arg2017z00_2466);
													}
													BgL_clausez00_2451 =
														BGl_evepairifyz00zz__prognz00(BgL_arg2016z00_2465,
														BgL_xz00_151);
												}
											else
												{	/* Eval/evmodule.scm 940 */
													BgL_clausez00_2451 = BUNSPEC;
												}
										}
								}
						}
						{	/* Eval/evmodule.scm 940 */

							return
								BGl_evmodulezd2staticzd2zz__evmodulez00(BgL_modz00_2449,
								BgL_clausez00_2451,
								BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_xz00_151),
								((bool_t) 1));
						}
					}
				}
			}
		}

	}



/* &evmodule-static-class */
	obj_t BGl_z62evmodulezd2staticzd2classz62zz__evmodulez00(obj_t
		BgL_envz00_4317, obj_t BgL_xz00_4318)
	{
		{	/* Eval/evmodule.scm 937 */
			{	/* Eval/evmodule.scm 938 */
				obj_t BgL_auxz00_9162;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_4318))
					{	/* Eval/evmodule.scm 938 */
						BgL_auxz00_9162 = BgL_xz00_4318;
					}
				else
					{
						obj_t BgL_auxz00_9165;

						BgL_auxz00_9165 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(35495L), BGl_string3195z00zz__evmodulez00,
							BGl_string3059z00zz__evmodulez00, BgL_xz00_4318);
						FAILURE(BgL_auxz00_9165, BFALSE, BFALSE);
					}
				return BGl_evmodulezd2staticzd2classz00zz__evmodulez00(BgL_auxz00_9162);
			}
		}

	}



/* call-with-eval-module */
	BGL_EXPORTED_DEF obj_t BGl_callzd2withzd2evalzd2modulezd2zz__evmodulez00(obj_t
		BgL_newmz00_152, obj_t BgL_procz00_153)
	{
		{	/* Eval/evmodule.scm 952 */
			{	/* Eval/evmodule.scm 953 */
				obj_t BgL_oldmz00_3959;

				BgL_oldmz00_3959 = BGL_MODULE();
				BGl_evalzd2modulezd2setz12z12zz__evmodulez00(BgL_newmz00_152);
				{	/* Eval/evmodule.scm 955 */
					obj_t BgL_exitd1103z00_3960;

					BgL_exitd1103z00_3960 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Eval/evmodule.scm 957 */
						obj_t BgL_zc3z04anonymousza32020ze3z87_4319;

						BgL_zc3z04anonymousza32020ze3z87_4319 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32020ze3ze5zz__evmodulez00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32020ze3z87_4319, (int) (0L),
							BgL_oldmz00_3959);
						{	/* Eval/evmodule.scm 955 */
							obj_t BgL_arg2331z00_3963;

							{	/* Eval/evmodule.scm 955 */
								obj_t BgL_arg2333z00_3964;

								BgL_arg2333z00_3964 = BGL_EXITD_PROTECT(BgL_exitd1103z00_3960);
								BgL_arg2331z00_3963 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32020ze3z87_4319,
									BgL_arg2333z00_3964);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_3960, BgL_arg2331z00_3963);
							BUNSPEC;
						}
						{	/* Eval/evmodule.scm 956 */
							obj_t BgL_tmp1105z00_3962;

							BgL_tmp1105z00_3962 = BGL_PROCEDURE_CALL0(BgL_procz00_153);
							{	/* Eval/evmodule.scm 955 */
								bool_t BgL_test3988z00_9184;

								{	/* Eval/evmodule.scm 955 */
									obj_t BgL_arg2330z00_3966;

									BgL_arg2330z00_3966 =
										BGL_EXITD_PROTECT(BgL_exitd1103z00_3960);
									BgL_test3988z00_9184 = PAIRP(BgL_arg2330z00_3966);
								}
								if (BgL_test3988z00_9184)
									{	/* Eval/evmodule.scm 955 */
										obj_t BgL_arg2327z00_3967;

										{	/* Eval/evmodule.scm 955 */
											obj_t BgL_arg2328z00_3968;

											BgL_arg2328z00_3968 =
												BGL_EXITD_PROTECT(BgL_exitd1103z00_3960);
											{	/* Eval/evmodule.scm 955 */
												obj_t BgL_pairz00_3969;

												if (PAIRP(BgL_arg2328z00_3968))
													{	/* Eval/evmodule.scm 955 */
														BgL_pairz00_3969 = BgL_arg2328z00_3968;
													}
												else
													{
														obj_t BgL_auxz00_9190;

														BgL_auxz00_9190 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string3001z00zz__evmodulez00, BINT(36210L),
															BGl_string3196z00zz__evmodulez00,
															BGl_string3003z00zz__evmodulez00,
															BgL_arg2328z00_3968);
														FAILURE(BgL_auxz00_9190, BFALSE, BFALSE);
													}
												BgL_arg2327z00_3967 = CDR(BgL_pairz00_3969);
											}
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1103z00_3960,
											BgL_arg2327z00_3967);
										BUNSPEC;
									}
								else
									{	/* Eval/evmodule.scm 955 */
										BFALSE;
									}
							}
							BGl_evalzd2modulezd2setz12z12zz__evmodulez00(BgL_oldmz00_3959);
							return BgL_tmp1105z00_3962;
						}
					}
				}
			}
		}

	}



/* &call-with-eval-module */
	obj_t BGl_z62callzd2withzd2evalzd2modulezb0zz__evmodulez00(obj_t
		BgL_envz00_4320, obj_t BgL_newmz00_4321, obj_t BgL_procz00_4322)
	{
		{	/* Eval/evmodule.scm 952 */
			{	/* Eval/evmodule.scm 953 */
				obj_t BgL_auxz00_9197;

				if (PROCEDUREP(BgL_procz00_4322))
					{	/* Eval/evmodule.scm 953 */
						BgL_auxz00_9197 = BgL_procz00_4322;
					}
				else
					{
						obj_t BgL_auxz00_9200;

						BgL_auxz00_9200 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string3001z00zz__evmodulez00,
							BINT(36146L), BGl_string3197z00zz__evmodulez00,
							BGl_string3110z00zz__evmodulez00, BgL_procz00_4322);
						FAILURE(BgL_auxz00_9200, BFALSE, BFALSE);
					}
				return
					BGl_callzd2withzd2evalzd2modulezd2zz__evmodulez00(BgL_newmz00_4321,
					BgL_auxz00_9197);
			}
		}

	}



/* &<@anonymous:2020> */
	obj_t BGl_z62zc3z04anonymousza32020ze3ze5zz__evmodulez00(obj_t
		BgL_envz00_4323)
	{
		{	/* Eval/evmodule.scm 955 */
			return
				BGl_evalzd2modulezd2setz12z12zz__evmodulez00(PROCEDURE_REF
				(BgL_envz00_4323, (int) (0L)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evmodulez00(void)
	{
		{	/* Eval/evmodule.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__modulez00(416350269L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__tracez00(81989385L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__evalz00(35539458L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
			return BGl_modulezd2initializa7ationz75zz__evobjectz00(481718039L,
				BSTRING_TO_STRING(BGl_string3198z00zz__evmodulez00));
		}

	}

#ifdef __cplusplus
}
#endif
