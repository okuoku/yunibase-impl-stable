/*===========================================================================*/
/*   (Eval/expddo.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expddo.scm -indent -o objs/obj_u/Eval/expddo.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_DO_TYPE_DEFINITIONS
#define BGL___EXPANDER_DO_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_DO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_doz00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_doz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62expandzd2dozb0zz__expander_doz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_doz00(void);
	extern obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zz__expander_doz00(void);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_doz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_doz00(void);
	static obj_t BGl_objectzd2initzd2zz__expander_doz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2dozd2zz__expander_doz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_doz00(void);
	static obj_t BGl_symbol1637z00zz__expander_doz00 = BUNSPEC;
	static obj_t BGl_symbol1639z00zz__expander_doz00 = BUNSPEC;
	extern obj_t BGl_evepairifyzd2deepzd2zz__prognz00(obj_t, obj_t);
	static obj_t BGl_symbol1641z00zz__expander_doz00 = BUNSPEC;
	static obj_t BGl_symbol1643z00zz__expander_doz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2dozd2envz00zz__expander_doz00,
		BgL_bgl_za762expandza7d2doza7b1646za7,
		BGl_z62expandzd2dozb0zz__expander_doz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1633z00zz__expander_doz00,
		BgL_bgl_string1633za700za7za7_1647za7, "do-loop--", 9);
	      DEFINE_STRING(BGl_string1634z00zz__expander_doz00,
		BgL_bgl_string1634za700za7za7_1648za7, "do", 2);
	      DEFINE_STRING(BGl_string1635z00zz__expander_doz00,
		BgL_bgl_string1635za700za7za7_1649za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1636z00zz__expander_doz00,
		BgL_bgl_string1636za700za7za7_1650za7, "Illegal form:", 13);
	      DEFINE_STRING(BGl_string1638z00zz__expander_doz00,
		BgL_bgl_string1638za700za7za7_1651za7, "begin", 5);
	      DEFINE_STRING(BGl_string1640z00zz__expander_doz00,
		BgL_bgl_string1640za700za7za7_1652za7, "if", 2);
	      DEFINE_STRING(BGl_string1642z00zz__expander_doz00,
		BgL_bgl_string1642za700za7za7_1653za7, "lambda", 6);
	      DEFINE_STRING(BGl_string1644z00zz__expander_doz00,
		BgL_bgl_string1644za700za7za7_1654za7, "letrec", 6);
	      DEFINE_STRING(BGl_string1645z00zz__expander_doz00,
		BgL_bgl_string1645za700za7za7_1655za7, "__expander_do", 13);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_doz00));
		     ADD_ROOT((void *) (&BGl_symbol1637z00zz__expander_doz00));
		     ADD_ROOT((void *) (&BGl_symbol1639z00zz__expander_doz00));
		     ADD_ROOT((void *) (&BGl_symbol1641z00zz__expander_doz00));
		     ADD_ROOT((void *) (&BGl_symbol1643z00zz__expander_doz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__expander_doz00(long
		BgL_checksumz00_1821, char *BgL_fromz00_1822)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_doz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_doz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_doz00();
					BGl_cnstzd2initzd2zz__expander_doz00();
					BGl_importedzd2moduleszd2initz00zz__expander_doz00();
					return BGl_methodzd2initzd2zz__expander_doz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_doz00(void)
	{
		{	/* Eval/expddo.scm 14 */
			BGl_symbol1637z00zz__expander_doz00 =
				bstring_to_symbol(BGl_string1638z00zz__expander_doz00);
			BGl_symbol1639z00zz__expander_doz00 =
				bstring_to_symbol(BGl_string1640z00zz__expander_doz00);
			BGl_symbol1641z00zz__expander_doz00 =
				bstring_to_symbol(BGl_string1642z00zz__expander_doz00);
			return (BGl_symbol1643z00zz__expander_doz00 =
				bstring_to_symbol(BGl_string1644z00zz__expander_doz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_doz00(void)
	{
		{	/* Eval/expddo.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* expand-do */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2dozd2zz__expander_doz00(obj_t
		BgL_expz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expddo.scm 61 */
			{
				obj_t BgL_bindingsz00_1118;
				obj_t BgL_endz00_1119;
				obj_t BgL_commandz00_1120;

				if (PAIRP(BgL_expz00_3))
					{	/* Eval/expddo.scm 62 */
						obj_t BgL_cdrzd2111zd2_1125;

						BgL_cdrzd2111zd2_1125 = CDR(((obj_t) BgL_expz00_3));
						if (PAIRP(BgL_cdrzd2111zd2_1125))
							{	/* Eval/expddo.scm 62 */
								obj_t BgL_cdrzd2116zd2_1127;

								BgL_cdrzd2116zd2_1127 = CDR(BgL_cdrzd2111zd2_1125);
								if (PAIRP(BgL_cdrzd2116zd2_1127))
									{	/* Eval/expddo.scm 62 */
										BgL_bindingsz00_1118 = CAR(BgL_cdrzd2111zd2_1125);
										BgL_endz00_1119 = CAR(BgL_cdrzd2116zd2_1127);
										BgL_commandz00_1120 = CDR(BgL_cdrzd2116zd2_1127);
										{	/* Eval/expddo.scm 64 */
											obj_t BgL_varsz00_1133;

											BgL_varsz00_1133 = BNIL;
											{	/* Eval/expddo.scm 65 */
												obj_t BgL_initsz00_1134;

												BgL_initsz00_1134 = BNIL;
												{	/* Eval/expddo.scm 66 */
													obj_t BgL_stepsz00_1135;

													BgL_stepsz00_1135 = BNIL;
													{	/* Eval/expddo.scm 67 */
														obj_t BgL_loopz00_1136;

														BgL_loopz00_1136 =
															BGl_gensymz00zz__r4_symbols_6_4z00
															(BGl_string1633z00zz__expander_doz00);
														{	/* Eval/expddo.scm 68 */
															obj_t BgL_testz00_1137;

															if (PAIRP(BgL_endz00_1119))
																{	/* Eval/expddo.scm 69 */
																	BgL_testz00_1137 = CAR(BgL_endz00_1119);
																}
															else
																{	/* Eval/expddo.scm 69 */
																	BgL_testz00_1137 =
																		BGl_expandzd2errorzd2zz__expandz00
																		(BGl_string1634z00zz__expander_doz00,
																		BGl_string1635z00zz__expander_doz00,
																		BgL_expz00_3);
																}
															{	/* Eval/expddo.scm 69 */
																obj_t BgL_endingz00_1138;

																if (NULLP(CDR(((obj_t) BgL_endz00_1119))))
																	{	/* Eval/expddo.scm 73 */
																		obj_t BgL_list1235z00_1188;

																		BgL_list1235z00_1188 =
																			MAKE_YOUNG_PAIR(BFALSE, BNIL);
																		BgL_endingz00_1138 = BgL_list1235z00_1188;
																	}
																else
																	{	/* Eval/expddo.scm 72 */
																		BgL_endingz00_1138 =
																			CDR(((obj_t) BgL_endz00_1119));
																	}
																{	/* Eval/expddo.scm 75 */

																	{	/* Eval/expddo.scm 76 */
																		obj_t BgL_g1082z00_1140;

																		BgL_g1082z00_1140 =
																			bgl_reverse(BgL_bindingsz00_1118);
																		{
																			obj_t BgL_l1080z00_1142;

																			BgL_l1080z00_1142 = BgL_g1082z00_1140;
																		BgL_zc3z04anonymousza31183ze3z87_1143:
																			if (PAIRP(BgL_l1080z00_1142))
																				{	/* Eval/expddo.scm 89 */
																					{	/* Eval/expddo.scm 78 */
																						obj_t BgL_varzd2initzd2stepz00_1145;

																						BgL_varzd2initzd2stepz00_1145 =
																							CAR(BgL_l1080z00_1142);
																						{	/* Eval/expddo.scm 78 */
																							bool_t BgL_test1663z00_1860;

																							if (
																								(bgl_list_length
																									(BgL_varzd2initzd2stepz00_1145)
																									>= 2L))
																								{	/* Eval/expddo.scm 78 */
																									BgL_test1663z00_1860 =
																										(bgl_list_length
																										(BgL_varzd2initzd2stepz00_1145)
																										<= 3L);
																								}
																							else
																								{	/* Eval/expddo.scm 78 */
																									BgL_test1663z00_1860 =
																										((bool_t) 0);
																								}
																							if (BgL_test1663z00_1860)
																								{	/* Eval/expddo.scm 80 */
																									obj_t BgL_varz00_1151;

																									BgL_varz00_1151 =
																										CAR(
																										((obj_t)
																											BgL_varzd2initzd2stepz00_1145));
																									{	/* Eval/expddo.scm 80 */
																										obj_t BgL_initz00_1152;

																										{	/* Eval/expddo.scm 81 */
																											obj_t BgL_pairz00_1616;

																											BgL_pairz00_1616 =
																												CDR(
																												((obj_t)
																													BgL_varzd2initzd2stepz00_1145));
																											BgL_initz00_1152 =
																												CAR(BgL_pairz00_1616);
																										}
																										{	/* Eval/expddo.scm 81 */
																											obj_t BgL_stepz00_1153;

																											{	/* Eval/expddo.scm 82 */
																												bool_t
																													BgL_test1665z00_1871;
																												{	/* Eval/expddo.scm 82 */
																													obj_t BgL_tmpz00_1872;

																													{	/* Eval/expddo.scm 82 */
																														obj_t
																															BgL_pairz00_1620;
																														BgL_pairz00_1620 =
																															CDR(((obj_t)
																																BgL_varzd2initzd2stepz00_1145));
																														BgL_tmpz00_1872 =
																															CDR
																															(BgL_pairz00_1620);
																													}
																													BgL_test1665z00_1871 =
																														NULLP
																														(BgL_tmpz00_1872);
																												}
																												if (BgL_test1665z00_1871)
																													{	/* Eval/expddo.scm 82 */
																														BgL_stepz00_1153 =
																															BgL_varz00_1151;
																													}
																												else
																													{	/* Eval/expddo.scm 83 */
																														obj_t
																															BgL_pairz00_1625;
																														{	/* Eval/expddo.scm 83 */
																															obj_t
																																BgL_pairz00_1624;
																															BgL_pairz00_1624 =
																																CDR(((obj_t)
																																	BgL_varzd2initzd2stepz00_1145));
																															BgL_pairz00_1625 =
																																CDR
																																(BgL_pairz00_1624);
																														}
																														BgL_stepz00_1153 =
																															CAR
																															(BgL_pairz00_1625);
																													}
																											}
																											{	/* Eval/expddo.scm 82 */

																												BgL_varsz00_1133 =
																													MAKE_YOUNG_PAIR
																													(BgL_varz00_1151,
																													BgL_varsz00_1133);
																												BgL_stepsz00_1135 =
																													MAKE_YOUNG_PAIR
																													(BgL_stepz00_1153,
																													BgL_stepsz00_1135);
																												BgL_initsz00_1134 =
																													MAKE_YOUNG_PAIR
																													(BgL_initz00_1152,
																													BgL_initsz00_1134);
																											}
																										}
																									}
																								}
																							else
																								{	/* Eval/expddo.scm 78 */
																									BGl_expandzd2errorzd2zz__expandz00
																										(BGl_string1634z00zz__expander_doz00,
																										BGl_string1636z00zz__expander_doz00,
																										BgL_varzd2initzd2stepz00_1145);
																								}
																						}
																					}
																					{
																						obj_t BgL_l1080z00_1885;

																						BgL_l1080z00_1885 =
																							CDR(BgL_l1080z00_1142);
																						BgL_l1080z00_1142 =
																							BgL_l1080z00_1885;
																						goto
																							BgL_zc3z04anonymousza31183ze3z87_1143;
																					}
																				}
																			else
																				{	/* Eval/expddo.scm 89 */
																					((bool_t) 1);
																				}
																		}
																	}
																	{	/* Eval/expddo.scm 93 */
																		obj_t BgL_arg1201z00_1163;

																		{	/* Eval/expddo.scm 93 */
																			obj_t BgL_arg1202z00_1164;

																			{	/* Eval/expddo.scm 93 */
																				obj_t BgL_arg1203z00_1165;

																				{	/* Eval/expddo.scm 93 */
																					obj_t BgL_arg1206z00_1166;
																					obj_t BgL_arg1208z00_1167;

																					{	/* Eval/expddo.scm 93 */
																						obj_t BgL_arg1209z00_1168;

																						{	/* Eval/expddo.scm 93 */
																							obj_t BgL_arg1210z00_1169;

																							{	/* Eval/expddo.scm 93 */
																								obj_t BgL_arg1212z00_1170;

																								{	/* Eval/expddo.scm 93 */
																									obj_t BgL_arg1215z00_1171;

																									{	/* Eval/expddo.scm 93 */
																										obj_t BgL_arg1216z00_1172;

																										{	/* Eval/expddo.scm 93 */
																											obj_t BgL_arg1218z00_1173;

																											{	/* Eval/expddo.scm 93 */
																												obj_t
																													BgL_arg1219z00_1174;
																												{	/* Eval/expddo.scm 93 */
																													obj_t
																														BgL_arg1220z00_1175;
																													{	/* Eval/expddo.scm 93 */
																														obj_t
																															BgL_arg1221z00_1176;
																														obj_t
																															BgL_arg1223z00_1177;
																														{	/* Eval/expddo.scm 93 */
																															obj_t
																																BgL_arg1225z00_1178;
																															BgL_arg1225z00_1178
																																=
																																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																(BgL_endingz00_1138,
																																BNIL);
																															BgL_arg1221z00_1176
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1637z00zz__expander_doz00,
																																BgL_arg1225z00_1178);
																														}
																														{	/* Eval/expddo.scm 94 */
																															obj_t
																																BgL_arg1226z00_1179;
																															{	/* Eval/expddo.scm 94 */
																																obj_t
																																	BgL_arg1227z00_1180;
																																{	/* Eval/expddo.scm 94 */
																																	obj_t
																																		BgL_arg1228z00_1181;
																																	{	/* Eval/expddo.scm 94 */
																																		obj_t
																																			BgL_arg1229z00_1182;
																																		BgL_arg1229z00_1182
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_loopz00_1136,
																																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																			(BgL_stepsz00_1135,
																																				BNIL));
																																		BgL_arg1228z00_1181
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1229z00_1182,
																																			BNIL);
																																	}
																																	BgL_arg1227z00_1180
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_commandz00_1120,
																																		BgL_arg1228z00_1181);
																																}
																																BgL_arg1226z00_1179
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol1637z00zz__expander_doz00,
																																	BgL_arg1227z00_1180);
																															}
																															BgL_arg1223z00_1177
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1226z00_1179,
																																BNIL);
																														}
																														BgL_arg1220z00_1175
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1221z00_1176,
																															BgL_arg1223z00_1177);
																													}
																													BgL_arg1219z00_1174 =
																														MAKE_YOUNG_PAIR
																														(BgL_testz00_1137,
																														BgL_arg1220z00_1175);
																												}
																												BgL_arg1218z00_1173 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1639z00zz__expander_doz00,
																													BgL_arg1219z00_1174);
																											}
																											BgL_arg1216z00_1172 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1218z00_1173,
																												BNIL);
																										}
																										BgL_arg1215z00_1171 =
																											MAKE_YOUNG_PAIR
																											(BgL_varsz00_1133,
																											BgL_arg1216z00_1172);
																									}
																									BgL_arg1212z00_1170 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1641z00zz__expander_doz00,
																										BgL_arg1215z00_1171);
																								}
																								BgL_arg1210z00_1169 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1212z00_1170, BNIL);
																							}
																							BgL_arg1209z00_1168 =
																								MAKE_YOUNG_PAIR
																								(BgL_loopz00_1136,
																								BgL_arg1210z00_1169);
																						}
																						BgL_arg1206z00_1166 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1209z00_1168, BNIL);
																					}
																					{	/* Eval/expddo.scm 95 */
																						obj_t BgL_arg1231z00_1184;

																						BgL_arg1231z00_1184 =
																							MAKE_YOUNG_PAIR(BgL_loopz00_1136,
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_initsz00_1134, BNIL));
																						BgL_arg1208z00_1167 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1231z00_1184, BNIL);
																					}
																					BgL_arg1203z00_1165 =
																						MAKE_YOUNG_PAIR(BgL_arg1206z00_1166,
																						BgL_arg1208z00_1167);
																				}
																				BgL_arg1202z00_1164 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1643z00zz__expander_doz00,
																					BgL_arg1203z00_1165);
																			}
																			BgL_arg1201z00_1163 =
																				BGl_evepairifyzd2deepzd2zz__prognz00
																				(BgL_arg1202z00_1164, BgL_expz00_3);
																		}
																		return
																			BGL_PROCEDURE_CALL2(BgL_ez00_4,
																			BgL_arg1201z00_1163, BgL_ez00_4);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Eval/expddo.scm 62 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string1634z00zz__expander_doz00,
											BGl_string1635z00zz__expander_doz00, BgL_expz00_3);
									}
							}
						else
							{	/* Eval/expddo.scm 62 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string1634z00zz__expander_doz00,
									BGl_string1635z00zz__expander_doz00, BgL_expz00_3);
							}
					}
				else
					{	/* Eval/expddo.scm 62 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string1634z00zz__expander_doz00,
							BGl_string1635z00zz__expander_doz00, BgL_expz00_3);
					}
			}
		}

	}



/* &expand-do */
	obj_t BGl_z62expandzd2dozb0zz__expander_doz00(obj_t BgL_envz00_1814,
		obj_t BgL_expz00_1815, obj_t BgL_ez00_1816)
	{
		{	/* Eval/expddo.scm 61 */
			return
				BGl_expandzd2dozd2zz__expander_doz00(BgL_expz00_1815, BgL_ez00_1816);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_doz00(void)
	{
		{	/* Eval/expddo.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_doz00(void)
	{
		{	/* Eval/expddo.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_doz00(void)
	{
		{	/* Eval/expddo.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_doz00(void)
	{
		{	/* Eval/expddo.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
			return BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1645z00zz__expander_doz00));
		}

	}

#ifdef __cplusplus
}
#endif
