/*===========================================================================*/
/*   (Eval/evaluate_uncomp.scm)                                              */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evaluate_uncomp.scm -indent -o objs/obj_u/Eval/evaluate_uncomp.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVALUATE_UNCOMP_TYPE_DEFINITIONS
#define BGL___EVALUATE_UNCOMP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ev_exprz00_bgl
	{
		header_t header;
		obj_t widening;
	}                 *BgL_ev_exprz00_bglt;

	typedef struct BgL_ev_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_effz00;
		obj_t BgL_typez00;
	}                *BgL_ev_varz00_bglt;

	typedef struct BgL_ev_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                   *BgL_ev_globalz00_bglt;

	typedef struct BgL_ev_littz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_valuez00;
	}                 *BgL_ev_littz00_bglt;

	typedef struct BgL_ev_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_pz00;
		struct BgL_ev_exprz00_bgl *BgL_tz00;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}               *BgL_ev_ifz00_bglt;

	typedef struct BgL_ev_listz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                 *BgL_ev_listz00_bglt;

	typedef struct BgL_ev_orz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}               *BgL_ev_orz00_bglt;

	typedef struct BgL_ev_andz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                *BgL_ev_andz00_bglt;

	typedef struct BgL_ev_prog2z00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_e1z00;
		struct BgL_ev_exprz00_bgl *BgL_e2z00;
	}                  *BgL_ev_prog2z00_bglt;

	typedef struct BgL_ev_hookz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_hookz00_bglt;

	typedef struct BgL_ev_trapz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_trapz00_bglt;

	typedef struct BgL_ev_setlocalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_varz00_bgl *BgL_vz00;
	}                     *BgL_ev_setlocalz00_bglt;

	typedef struct BgL_ev_setglobalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                      *BgL_ev_setglobalz00_bglt;

	typedef struct BgL_ev_defglobalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_modz00;
	}                      *BgL_ev_defglobalz00_bglt;

	typedef struct BgL_ev_bindzd2exitzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_varz00_bgl *BgL_varz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                        *BgL_ev_bindzd2exitzd2_bglt;

	typedef struct BgL_ev_unwindzd2protectzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                             *BgL_ev_unwindzd2protectzd2_bglt;

	typedef struct BgL_ev_withzd2handlerzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_handlerz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                           *BgL_ev_withzd2handlerzd2_bglt;

	typedef struct BgL_ev_synchroniza7eza7_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_mutexz00;
		struct BgL_ev_exprz00_bgl *BgL_prelockz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                          *BgL_ev_synchroniza7eza7_bglt;

	typedef struct BgL_ev_binderz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_binderz00_bglt;

	typedef struct BgL_ev_letz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_letz00_bglt;

	typedef struct BgL_ev_letza2za2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_letza2za2_bglt;

	typedef struct BgL_ev_letrecz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_letrecz00_bglt;

	typedef struct BgL_ev_labelsz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		obj_t BgL_envz00;
		obj_t BgL_stkz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_labelsz00_bglt;

	typedef struct BgL_ev_gotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_varz00_bgl *BgL_labelz00;
		struct BgL_ev_labelsz00_bgl *BgL_labelsz00;
		obj_t BgL_argsz00;
	}                 *BgL_ev_gotoz00_bglt;

	typedef struct BgL_ev_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_tailzf3zf3;
	}                *BgL_ev_appz00_bglt;

	typedef struct BgL_ev_absz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_wherez00;
		obj_t BgL_arityz00;
		obj_t BgL_varsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		int BgL_siza7eza7;
		obj_t BgL_bindz00;
		obj_t BgL_freez00;
		obj_t BgL_innerz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_absz00_bglt;


#endif													// BGL___EVALUATE_UNCOMP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62uncompzd2ev_and1215zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62uncompzd2ev_synchroniza71233z17zz__evaluate_uncompz00(obj_t, obj_t);
	static obj_t BGl_z62uncompzd2ev_or1213zb0zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_ev_defglobalz00zz__evaluate_typesz00;
	static obj_t BGl_keyword1966z00zz__evaluate_uncompz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62uncompzd2ev_setlocal1225zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evaluate_uncompz00 =
		BUNSPEC;
	static obj_t BGl_z62uncompzd2ev_if1211zb0zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_ev_gotoz00zz__evaluate_typesz00;
	static obj_t BGl_z62uncompzd2ev_global1207zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_symbol1932z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t
		BGl_z62uncompzd2ev_bindzd2exit1227z62zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_uncompz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62uncompzd2ev_abs1247zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t BGl_ev_globalz00zz__evaluate_typesz00;
	extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_letrecz00zz__evaluate_typesz00;
	static obj_t BGl_toplevelzd2initzd2zz__evaluate_uncompz00(void);
	static obj_t BGl_symbol1958z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_z62uncompzd2ev_var1205zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_z62uncompzd2ev_prog21217zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t BGl_ev_littz00zz__evaluate_typesz00;
	static obj_t BGl_z62uncompzd2ev_labels1241zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t BGl_ev_orz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_letz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_varz00zz__evaluate_typesz00;
	static obj_t BGl_symbol1960z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1962z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1964z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1968z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__evaluate_uncompz00(void);
	static obj_t BGl_genericzd2initzd2zz__evaluate_uncompz00(void);
	extern obj_t BGl_ev_prog2z00zz__evaluate_typesz00;
	static obj_t BGl_symbol1970z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1972z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1974z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_uncompz00(void);
	static obj_t BGl_symbol1976z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1978z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__evaluate_uncompz00(void);
	static obj_t BGl_redovarsze70ze7zz__evaluate_uncompz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__evaluate_uncompz00(void);
	extern obj_t BGl_ev_bindzd2exitzd2zz__evaluate_typesz00;
	static obj_t
		BGl_z62uncompzd2ev_unwindzd2pro1229z62zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_ev_withzd2handlerzd2zz__evaluate_typesz00;
	static obj_t BGl_symbol1980z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1982z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_symbol1984z00zz__evaluate_uncompz00 = BUNSPEC;
	extern obj_t BGl_ev_andz00zz__evaluate_typesz00;
	static obj_t BGl_symbol1986z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_z62uncompzd2ev_let1235zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_symbol1988z00zz__evaluate_uncompz00 = BUNSPEC;
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_symbol1990z00zz__evaluate_uncompz00 = BUNSPEC;
	static obj_t BGl_z62uncompilez62zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00;
	static obj_t BGl_z62uncompzd2ev_trap1219zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_z62uncompzd2ev_litt1209zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_z62uncompz62zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_ev_exprz00zz__evaluate_typesz00;
	static obj_t BGl_z62uncompzd2ev_letza21237z12zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t BGl_ev_appz00zz__evaluate_typesz00;
	extern bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62uncompzd2ev_goto1243zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t BGl_ev_letza2za2zz__evaluate_typesz00;
	extern obj_t BGl_ev_labelsz00zz__evaluate_typesz00;
	static obj_t BGl_z62uncomp1201z62zz__evaluate_uncompz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__evaluate_uncompz00(void);
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_recze70ze7zz__evaluate_uncompz00(obj_t);
	extern obj_t BGl_ev_absz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_trapz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_synchroniza7eza7zz__evaluate_typesz00;
	static obj_t BGl_z62uncompzd2ev_defglobal1223zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_uncompilez00zz__evaluate_uncompz00(BgL_ev_exprz00_bglt);
	static obj_t BGl_z62uncompzd2ev_setglobal1221zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_z62uncompzd2ev_app1245zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	static obj_t BGl_z62uncompzd2ev_letrec1239zb0zz__evaluate_uncompz00(obj_t,
		obj_t);
	extern obj_t BGl_ev_setglobalz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_ifz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_setlocalz00zz__evaluate_typesz00;
	static obj_t
		BGl_z62uncompzd2ev_withzd2handl1231z62zz__evaluate_uncompz00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_uncompz00zz__evaluate_uncompz00(BgL_ev_exprz00_bglt);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1940z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_o1994z00,
		BGl_z62uncompzd2ev_or1213zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1941z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_a1995z00,
		BGl_z62uncompzd2ev_and1215zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1942z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_p1996z00,
		BGl_z62uncompzd2ev_prog21217zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1943z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_t1997z00,
		BGl_z62uncompzd2ev_trap1219zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1944z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_s1998z00,
		BGl_z62uncompzd2ev_setglobal1221zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1945z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_d1999z00,
		BGl_z62uncompzd2ev_defglobal1223zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1946z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_s2000z00,
		BGl_z62uncompzd2ev_setlocal1225zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1947z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_b2001z00,
		BGl_z62uncompzd2ev_bindzd2exit1227z62zz__evaluate_uncompz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1948z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_u2002z00,
		BGl_z62uncompzd2ev_unwindzd2pro1229z62zz__evaluate_uncompz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1949z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_w2003z00,
		BGl_z62uncompzd2ev_withzd2handl1231z62zz__evaluate_uncompz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1959z00zz__evaluate_uncompz00,
		BgL_bgl_string1959za700za7za7_2004za7, "lambda", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1950z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_s2005z00,
		BGl_z62uncompzd2ev_synchroniza71233z17zz__evaluate_uncompz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1951z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_l2006z00,
		BGl_z62uncompzd2ev_let1235zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1952z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_l2007z00,
		BGl_z62uncompzd2ev_letza21237z12zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1953z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_l2008z00,
		BGl_z62uncompzd2ev_letrec1239zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1954z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_l2009z00,
		BGl_z62uncompzd2ev_labels1241zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1961z00zz__evaluate_uncompz00,
		BgL_bgl_string1961za700za7za7_2010za7, "letrec", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1955z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_g2011z00,
		BGl_z62uncompzd2ev_goto1243zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1956z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_a2012z00,
		BGl_z62uncompzd2ev_app1245zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1963z00zz__evaluate_uncompz00,
		BgL_bgl_string1963za700za7za7_2013za7, "let*", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1957z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_a2014z00,
		BGl_z62uncompzd2ev_abs1247zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1965z00zz__evaluate_uncompz00,
		BgL_bgl_string1965za700za7za7_2015za7, "let", 3);
	      DEFINE_STRING(BGl_string1967z00zz__evaluate_uncompz00,
		BgL_bgl_string1967za700za7za7_2016za7, "prelock", 7);
	      DEFINE_STRING(BGl_string1969z00zz__evaluate_uncompz00,
		BgL_bgl_string1969za700za7za7_2017za7, "synchronize", 11);
	      DEFINE_STRING(BGl_string1971z00zz__evaluate_uncompz00,
		BgL_bgl_string1971za700za7za7_2018za7, "with-handler", 12);
	      DEFINE_STRING(BGl_string1973z00zz__evaluate_uncompz00,
		BgL_bgl_string1973za700za7za7_2019za7, "unwind-protect", 14);
	      DEFINE_STRING(BGl_string1975z00zz__evaluate_uncompz00,
		BgL_bgl_string1975za700za7za7_2020za7, "bind-exit", 9);
	      DEFINE_STRING(BGl_string1977z00zz__evaluate_uncompz00,
		BgL_bgl_string1977za700za7za7_2021za7, "set!", 4);
	      DEFINE_STRING(BGl_string1979z00zz__evaluate_uncompz00,
		BgL_bgl_string1979za700za7za7_2022za7, "define", 6);
	      DEFINE_STRING(BGl_string1981z00zz__evaluate_uncompz00,
		BgL_bgl_string1981za700za7za7_2023za7, "trap", 4);
	      DEFINE_STRING(BGl_string1983z00zz__evaluate_uncompz00,
		BgL_bgl_string1983za700za7za7_2024za7, "begin", 5);
	      DEFINE_STRING(BGl_string1985z00zz__evaluate_uncompz00,
		BgL_bgl_string1985za700za7za7_2025za7, "and", 3);
	      DEFINE_STRING(BGl_string1987z00zz__evaluate_uncompz00,
		BgL_bgl_string1987za700za7za7_2026za7, "or", 2);
	      DEFINE_STRING(BGl_string1989z00zz__evaluate_uncompz00,
		BgL_bgl_string1989za700za7za7_2027za7, "if", 2);
	      DEFINE_STRING(BGl_string1991z00zz__evaluate_uncompz00,
		BgL_bgl_string1991za700za7za7_2028za7, "quote", 5);
	      DEFINE_STRING(BGl_string1992z00zz__evaluate_uncompz00,
		BgL_bgl_string1992za700za7za7_2029za7, "__evaluate_uncomp", 17);
	      DEFINE_STATIC_BGL_GENERIC(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza762za7za7__2030z00,
		BGl_z62uncompz62zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_uncompilezd2envzd2zz__evaluate_uncompz00,
		BgL_bgl_za762uncompileza762za72031za7,
		BGl_z62uncompilez62zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1927z00zz__evaluate_uncompz00,
		BgL_bgl_string1927za700za7za7_2032za7,
		"/tmp/bigloo/runtime/Eval/evaluate_uncomp.scm", 44);
	      DEFINE_STRING(BGl_string1928z00zz__evaluate_uncompz00,
		BgL_bgl_string1928za700za7za7_2033za7, "&uncompile", 10);
	      DEFINE_STRING(BGl_string1929z00zz__evaluate_uncompz00,
		BgL_bgl_string1929za700za7za7_2034za7, "ev_expr", 7);
	      DEFINE_STRING(BGl_string1931z00zz__evaluate_uncompz00,
		BgL_bgl_string1931za700za7za7_2035za7, "uncomp1201", 10);
	      DEFINE_STRING(BGl_string1933z00zz__evaluate_uncompz00,
		BgL_bgl_string1933za700za7za7_2036za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1934z00zz__evaluate_uncompz00,
		BgL_bgl_string1934za700za7za7_2037za7, "&uncomp", 7);
	      DEFINE_STRING(BGl_string1936z00zz__evaluate_uncompz00,
		BgL_bgl_string1936za700za7za7_2038za7, "uncomp", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncomp1201za7622039z00,
		BGl_z62uncomp1201z62zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_v2040z00,
		BGl_z62uncompzd2ev_var1205zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_g2041z00,
		BGl_z62uncompzd2ev_global1207zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_l2042z00,
		BGl_z62uncompzd2ev_litt1209zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zz__evaluate_uncompz00,
		BgL_bgl_za762uncompza7d2ev_i2043z00,
		BGl_z62uncompzd2ev_if1211zb0zz__evaluate_uncompz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_keyword1966z00zz__evaluate_uncompz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1932z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1958z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1960z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1962z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1964z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1968z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1970z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1972z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1974z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1976z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1978z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1980z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1982z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1984z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1986z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1988z00zz__evaluate_uncompz00));
		     ADD_ROOT((void *) (&BGl_symbol1990z00zz__evaluate_uncompz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_uncompz00(long
		BgL_checksumz00_2893, char *BgL_fromz00_2894)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evaluate_uncompz00))
				{
					BGl_requirezd2initializa7ationz75zz__evaluate_uncompz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evaluate_uncompz00();
					BGl_cnstzd2initzd2zz__evaluate_uncompz00();
					BGl_importedzd2moduleszd2initz00zz__evaluate_uncompz00();
					BGl_genericzd2initzd2zz__evaluate_uncompz00();
					BGl_methodzd2initzd2zz__evaluate_uncompz00();
					return BGl_toplevelzd2initzd2zz__evaluate_uncompz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			BGl_symbol1932z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1931z00zz__evaluate_uncompz00);
			BGl_symbol1958z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1959z00zz__evaluate_uncompz00);
			BGl_symbol1960z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1961z00zz__evaluate_uncompz00);
			BGl_symbol1962z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1963z00zz__evaluate_uncompz00);
			BGl_symbol1964z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1965z00zz__evaluate_uncompz00);
			BGl_keyword1966z00zz__evaluate_uncompz00 =
				bstring_to_keyword(BGl_string1967z00zz__evaluate_uncompz00);
			BGl_symbol1968z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1969z00zz__evaluate_uncompz00);
			BGl_symbol1970z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1971z00zz__evaluate_uncompz00);
			BGl_symbol1972z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1973z00zz__evaluate_uncompz00);
			BGl_symbol1974z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1975z00zz__evaluate_uncompz00);
			BGl_symbol1976z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1977z00zz__evaluate_uncompz00);
			BGl_symbol1978z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1979z00zz__evaluate_uncompz00);
			BGl_symbol1980z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1981z00zz__evaluate_uncompz00);
			BGl_symbol1982z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1983z00zz__evaluate_uncompz00);
			BGl_symbol1984z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1985z00zz__evaluate_uncompz00);
			BGl_symbol1986z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1987z00zz__evaluate_uncompz00);
			BGl_symbol1988z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1989z00zz__evaluate_uncompz00);
			return (BGl_symbol1990z00zz__evaluate_uncompz00 =
				bstring_to_symbol(BGl_string1991z00zz__evaluate_uncompz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			return BUNSPEC;
		}

	}



/* uncompile */
	BGL_EXPORTED_DEF obj_t
		BGl_uncompilez00zz__evaluate_uncompz00(BgL_ev_exprz00_bglt BgL_ez00_3)
	{
		{	/* Eval/evaluate_uncomp.scm 62 */
			BGL_TAIL return BGl_uncompz00zz__evaluate_uncompz00(BgL_ez00_3);
		}

	}



/* &uncompile */
	obj_t BGl_z62uncompilez62zz__evaluate_uncompz00(obj_t BgL_envz00_2513,
		obj_t BgL_ez00_2514)
	{
		{	/* Eval/evaluate_uncomp.scm 62 */
			{	/* Eval/evaluate_uncomp.scm 63 */
				BgL_ev_exprz00_bglt BgL_auxz00_2924;

				if (BGl_isazf3zf3zz__objectz00(BgL_ez00_2514,
						BGl_ev_exprz00zz__evaluate_typesz00))
					{	/* Eval/evaluate_uncomp.scm 63 */
						BgL_auxz00_2924 = ((BgL_ev_exprz00_bglt) BgL_ez00_2514);
					}
				else
					{
						obj_t BgL_auxz00_2928;

						BgL_auxz00_2928 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1927z00zz__evaluate_uncompz00, BINT(1816L),
							BGl_string1928z00zz__evaluate_uncompz00,
							BGl_string1929z00zz__evaluate_uncompz00, BgL_ez00_2514);
						FAILURE(BgL_auxz00_2928, BFALSE, BFALSE);
					}
				return BGl_uncompilez00zz__evaluate_uncompz00(BgL_auxz00_2924);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_proc1930z00zz__evaluate_uncompz00,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string1931z00zz__evaluate_uncompz00);
		}

	}



/* &uncomp1201 */
	obj_t BGl_z62uncomp1201z62zz__evaluate_uncompz00(obj_t BgL_envz00_2516,
		obj_t BgL_ez00_2517)
	{
		{	/* Eval/evaluate_uncomp.scm 65 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol1932z00zz__evaluate_uncompz00,
				BGl_string1933z00zz__evaluate_uncompz00,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_2517)));
		}

	}



/* uncomp */
	obj_t BGl_uncompz00zz__evaluate_uncompz00(BgL_ev_exprz00_bglt BgL_ez00_4)
	{
		{	/* Eval/evaluate_uncomp.scm 65 */
			{	/* Eval/evaluate_uncomp.scm 65 */
				obj_t BgL_method1203z00_1157;

				{	/* Eval/evaluate_uncomp.scm 65 */
					obj_t BgL_res1911z00_2021;

					{	/* Eval/evaluate_uncomp.scm 65 */
						long BgL_objzd2classzd2numz00_1992;

						BgL_objzd2classzd2numz00_1992 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_4));
						{	/* Eval/evaluate_uncomp.scm 65 */
							obj_t BgL_arg1888z00_1993;

							BgL_arg1888z00_1993 =
								PROCEDURE_REF(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
								(int) (1L));
							{	/* Eval/evaluate_uncomp.scm 65 */
								int BgL_offsetz00_1996;

								BgL_offsetz00_1996 = (int) (BgL_objzd2classzd2numz00_1992);
								{	/* Eval/evaluate_uncomp.scm 65 */
									long BgL_offsetz00_1997;

									BgL_offsetz00_1997 =
										((long) (BgL_offsetz00_1996) - OBJECT_TYPE);
									{	/* Eval/evaluate_uncomp.scm 65 */
										long BgL_modz00_1998;

										BgL_modz00_1998 =
											(BgL_offsetz00_1997 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_uncomp.scm 65 */
											long BgL_restz00_2000;

											BgL_restz00_2000 =
												(BgL_offsetz00_1997 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_uncomp.scm 65 */

												{	/* Eval/evaluate_uncomp.scm 65 */
													obj_t BgL_bucketz00_2002;

													BgL_bucketz00_2002 =
														VECTOR_REF(
														((obj_t) BgL_arg1888z00_1993), BgL_modz00_1998);
													BgL_res1911z00_2021 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2002), BgL_restz00_2000);
					}}}}}}}}
					BgL_method1203z00_1157 = BgL_res1911z00_2021;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1203z00_1157, ((obj_t) BgL_ez00_4));
			}
		}

	}



/* &uncomp */
	obj_t BGl_z62uncompz62zz__evaluate_uncompz00(obj_t BgL_envz00_2518,
		obj_t BgL_ez00_2519)
	{
		{	/* Eval/evaluate_uncomp.scm 65 */
			{	/* Eval/evaluate_uncomp.scm 65 */
				BgL_ev_exprz00_bglt BgL_auxz00_2967;

				{	/* Eval/evaluate_uncomp.scm 65 */
					bool_t BgL_test2046z00_2968;

					{	/* Eval/evaluate_uncomp.scm 65 */
						obj_t BgL_classz00_2613;

						BgL_classz00_2613 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_2519))
							{	/* Eval/evaluate_uncomp.scm 65 */
								BgL_objectz00_bglt BgL_arg1885z00_2615;
								long BgL_arg1887z00_2616;

								BgL_arg1885z00_2615 = (BgL_objectz00_bglt) (BgL_ez00_2519);
								BgL_arg1887z00_2616 = BGL_CLASS_DEPTH(BgL_classz00_2613);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_uncomp.scm 65 */
										long BgL_idxz00_2624;

										BgL_idxz00_2624 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1885z00_2615);
										{	/* Eval/evaluate_uncomp.scm 65 */
											obj_t BgL_arg1876z00_2625;

											{	/* Eval/evaluate_uncomp.scm 65 */
												long BgL_arg1877z00_2626;

												BgL_arg1877z00_2626 =
													(BgL_idxz00_2624 + BgL_arg1887z00_2616);
												BgL_arg1876z00_2625 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1877z00_2626);
											}
											BgL_test2046z00_2968 =
												(BgL_arg1876z00_2625 == BgL_classz00_2613);
									}}
								else
									{	/* Eval/evaluate_uncomp.scm 65 */
										bool_t BgL_res1993z00_2652;

										{	/* Eval/evaluate_uncomp.scm 65 */
											obj_t BgL_oclassz00_2634;

											{	/* Eval/evaluate_uncomp.scm 65 */
												obj_t BgL_arg1891z00_2642;
												long BgL_arg1892z00_2643;

												BgL_arg1891z00_2642 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_uncomp.scm 65 */
													long BgL_arg1893z00_2644;

													BgL_arg1893z00_2644 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1885z00_2615);
													BgL_arg1892z00_2643 =
														(BgL_arg1893z00_2644 - OBJECT_TYPE);
												}
												BgL_oclassz00_2634 =
													VECTOR_REF(BgL_arg1891z00_2642, BgL_arg1892z00_2643);
											}
											{	/* Eval/evaluate_uncomp.scm 65 */
												bool_t BgL__ortest_1126z00_2635;

												BgL__ortest_1126z00_2635 =
													(BgL_classz00_2613 == BgL_oclassz00_2634);
												if (BgL__ortest_1126z00_2635)
													{	/* Eval/evaluate_uncomp.scm 65 */
														BgL_res1993z00_2652 = BgL__ortest_1126z00_2635;
													}
												else
													{	/* Eval/evaluate_uncomp.scm 65 */
														long BgL_odepthz00_2636;

														{	/* Eval/evaluate_uncomp.scm 65 */
															obj_t BgL_arg1880z00_2637;

															BgL_arg1880z00_2637 = (BgL_oclassz00_2634);
															BgL_odepthz00_2636 =
																BGL_CLASS_DEPTH(BgL_arg1880z00_2637);
														}
														if ((BgL_arg1887z00_2616 < BgL_odepthz00_2636))
															{	/* Eval/evaluate_uncomp.scm 65 */
																obj_t BgL_arg1878z00_2639;

																{	/* Eval/evaluate_uncomp.scm 65 */
																	obj_t BgL_arg1879z00_2640;

																	BgL_arg1879z00_2640 = (BgL_oclassz00_2634);
																	BgL_arg1878z00_2639 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1879z00_2640,
																		BgL_arg1887z00_2616);
																}
																BgL_res1993z00_2652 =
																	(BgL_arg1878z00_2639 == BgL_classz00_2613);
															}
														else
															{	/* Eval/evaluate_uncomp.scm 65 */
																BgL_res1993z00_2652 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2046z00_2968 = BgL_res1993z00_2652;
									}
							}
						else
							{	/* Eval/evaluate_uncomp.scm 65 */
								BgL_test2046z00_2968 = ((bool_t) 0);
							}
					}
					if (BgL_test2046z00_2968)
						{	/* Eval/evaluate_uncomp.scm 65 */
							BgL_auxz00_2967 = ((BgL_ev_exprz00_bglt) BgL_ez00_2519);
						}
					else
						{
							obj_t BgL_auxz00_2993;

							BgL_auxz00_2993 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1927z00zz__evaluate_uncompz00, BINT(1830L),
								BGl_string1934z00zz__evaluate_uncompz00,
								BGl_string1929z00zz__evaluate_uncompz00, BgL_ez00_2519);
							FAILURE(BgL_auxz00_2993, BFALSE, BFALSE);
						}
				}
				return BGl_uncompz00zz__evaluate_uncompz00(BgL_auxz00_2967);
			}
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_varz00zz__evaluate_typesz00,
				BGl_proc1935z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc1937z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc1938z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc1939z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_orz00zz__evaluate_typesz00,
				BGl_proc1940z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_andz00zz__evaluate_typesz00,
				BGl_proc1941z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc1942z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_trapz00zz__evaluate_typesz00,
				BGl_proc1943z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_setglobalz00zz__evaluate_typesz00,
				BGl_proc1944z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_defglobalz00zz__evaluate_typesz00,
				BGl_proc1945z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_setlocalz00zz__evaluate_typesz00,
				BGl_proc1946z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc1947z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc1948z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc1949z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc1950z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_letz00zz__evaluate_typesz00,
				BGl_proc1951z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_letza2za2zz__evaluate_typesz00,
				BGl_proc1952z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_letrecz00zz__evaluate_typesz00,
				BGl_proc1953z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc1954z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_gotoz00zz__evaluate_typesz00,
				BGl_proc1955z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_appz00zz__evaluate_typesz00,
				BGl_proc1956z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_uncompzd2envzd2zz__evaluate_uncompz00,
				BGl_ev_absz00zz__evaluate_typesz00,
				BGl_proc1957z00zz__evaluate_uncompz00,
				BGl_string1936z00zz__evaluate_uncompz00);
		}

	}



/* &uncomp-ev_abs1247 */
	obj_t BGl_z62uncompzd2ev_abs1247zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2542, obj_t BgL_ez00_2543)
	{
		{	/* Eval/evaluate_uncomp.scm 157 */
			{	/* Eval/evaluate_uncomp.scm 166 */
				obj_t BgL_arg1522z00_2654;

				{	/* Eval/evaluate_uncomp.scm 166 */
					obj_t BgL_arg1523z00_2655;
					obj_t BgL_arg1524z00_2656;

					{	/* Eval/evaluate_uncomp.scm 166 */
						obj_t BgL_arg1525z00_2657;
						obj_t BgL_arg1526z00_2658;

						BgL_arg1525z00_2657 =
							(((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_2543)))->BgL_arityz00);
						{	/* Eval/evaluate_uncomp.scm 166 */
							obj_t BgL_l1183z00_2659;

							BgL_l1183z00_2659 =
								(((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_ez00_2543)))->BgL_varsz00);
							if (NULLP(BgL_l1183z00_2659))
								{	/* Eval/evaluate_uncomp.scm 166 */
									BgL_arg1526z00_2658 = BNIL;
								}
							else
								{	/* Eval/evaluate_uncomp.scm 166 */
									obj_t BgL_head1185z00_2660;

									BgL_head1185z00_2660 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1183z00_2662;
										obj_t BgL_tail1186z00_2663;

										BgL_l1183z00_2662 = BgL_l1183z00_2659;
										BgL_tail1186z00_2663 = BgL_head1185z00_2660;
									BgL_zc3z04anonymousza31528ze3z87_2661:
										if (NULLP(BgL_l1183z00_2662))
											{	/* Eval/evaluate_uncomp.scm 166 */
												BgL_arg1526z00_2658 = CDR(BgL_head1185z00_2660);
											}
										else
											{	/* Eval/evaluate_uncomp.scm 166 */
												obj_t BgL_newtail1187z00_2664;

												{	/* Eval/evaluate_uncomp.scm 166 */
													obj_t BgL_arg1531z00_2665;

													BgL_arg1531z00_2665 =
														(((BgL_ev_varz00_bglt) COBJECT(
																((BgL_ev_varz00_bglt)
																	CAR(
																		((obj_t) BgL_l1183z00_2662)))))->
														BgL_namez00);
													BgL_newtail1187z00_2664 =
														MAKE_YOUNG_PAIR(BgL_arg1531z00_2665, BNIL);
												}
												SET_CDR(BgL_tail1186z00_2663, BgL_newtail1187z00_2664);
												{	/* Eval/evaluate_uncomp.scm 166 */
													obj_t BgL_arg1530z00_2666;

													BgL_arg1530z00_2666 =
														CDR(((obj_t) BgL_l1183z00_2662));
													{
														obj_t BgL_tail1186z00_3039;
														obj_t BgL_l1183z00_3038;

														BgL_l1183z00_3038 = BgL_arg1530z00_2666;
														BgL_tail1186z00_3039 = BgL_newtail1187z00_2664;
														BgL_tail1186z00_2663 = BgL_tail1186z00_3039;
														BgL_l1183z00_2662 = BgL_l1183z00_3038;
														goto BgL_zc3z04anonymousza31528ze3z87_2661;
													}
												}
											}
									}
								}
						}
						BgL_arg1523z00_2655 =
							BGl_redovarsze70ze7zz__evaluate_uncompz00(BgL_arg1525z00_2657,
							BgL_arg1526z00_2658);
					}
					BgL_arg1524z00_2656 =
						MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(
							(((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_ez00_2543)))->BgL_bodyz00)),
						BNIL);
					BgL_arg1522z00_2654 =
						MAKE_YOUNG_PAIR(BgL_arg1523z00_2655, BgL_arg1524z00_2656);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1958z00zz__evaluate_uncompz00,
					BgL_arg1522z00_2654);
			}
		}

	}



/* rec~0 */
	obj_t BGl_recze70ze7zz__evaluate_uncompz00(obj_t BgL_lz00_1564)
	{
		{	/* Eval/evaluate_uncomp.scm 162 */
			if (NULLP(CDR(((obj_t) BgL_lz00_1564))))
				{	/* Eval/evaluate_uncomp.scm 163 */
					return CAR(((obj_t) BgL_lz00_1564));
				}
			else
				{	/* Eval/evaluate_uncomp.scm 165 */
					obj_t BgL_arg1543z00_1568;
					obj_t BgL_arg1544z00_1569;

					BgL_arg1543z00_1568 = CAR(((obj_t) BgL_lz00_1564));
					{	/* Eval/evaluate_uncomp.scm 165 */
						obj_t BgL_arg1546z00_1570;

						BgL_arg1546z00_1570 = CDR(((obj_t) BgL_lz00_1564));
						BgL_arg1544z00_1569 =
							BGl_recze70ze7zz__evaluate_uncompz00(BgL_arg1546z00_1570);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1543z00_1568, BgL_arg1544z00_1569);
				}
		}

	}



/* redovars~0 */
	obj_t BGl_redovarsze70ze7zz__evaluate_uncompz00(obj_t BgL_nz00_1559,
		obj_t BgL_lz00_1560)
	{
		{	/* Eval/evaluate_uncomp.scm 165 */
			if (((long) CINT(BgL_nz00_1559) >= 0L))
				{	/* Eval/evaluate_uncomp.scm 160 */
					return BgL_lz00_1560;
				}
			else
				{	/* Eval/evaluate_uncomp.scm 160 */
					return BGl_recze70ze7zz__evaluate_uncompz00(BgL_lz00_1560);
				}
		}

	}



/* &uncomp-ev_app1245 */
	obj_t BGl_z62uncompzd2ev_app1245zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2544, obj_t BgL_ez00_2545)
	{
		{	/* Eval/evaluate_uncomp.scm 153 */
			{	/* Eval/evaluate_uncomp.scm 155 */
				obj_t BgL_arg1506z00_2668;
				obj_t BgL_arg1507z00_2669;

				BgL_arg1506z00_2668 =
					BGl_uncompz00zz__evaluate_uncompz00(
					(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_2545)))->BgL_funz00));
				{	/* Eval/evaluate_uncomp.scm 155 */
					obj_t BgL_arg1509z00_2670;

					{	/* Eval/evaluate_uncomp.scm 155 */
						obj_t BgL_l1177z00_2671;

						BgL_l1177z00_2671 =
							(((BgL_ev_appz00_bglt) COBJECT(
									((BgL_ev_appz00_bglt) BgL_ez00_2545)))->BgL_argsz00);
						if (NULLP(BgL_l1177z00_2671))
							{	/* Eval/evaluate_uncomp.scm 155 */
								BgL_arg1509z00_2670 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 155 */
								obj_t BgL_head1179z00_2672;

								{	/* Eval/evaluate_uncomp.scm 155 */
									obj_t BgL_arg1517z00_2673;

									{	/* Eval/evaluate_uncomp.scm 155 */
										obj_t BgL_arg1521z00_2674;

										BgL_arg1521z00_2674 = CAR(((obj_t) BgL_l1177z00_2671));
										BgL_arg1517z00_2673 =
											BGl_uncompz00zz__evaluate_uncompz00(
											((BgL_ev_exprz00_bglt) BgL_arg1521z00_2674));
									}
									BgL_head1179z00_2672 =
										MAKE_YOUNG_PAIR(BgL_arg1517z00_2673, BNIL);
								}
								{	/* Eval/evaluate_uncomp.scm 155 */
									obj_t BgL_g1182z00_2675;

									BgL_g1182z00_2675 = CDR(((obj_t) BgL_l1177z00_2671));
									{
										obj_t BgL_l1177z00_2677;
										obj_t BgL_tail1180z00_2678;

										BgL_l1177z00_2677 = BgL_g1182z00_2675;
										BgL_tail1180z00_2678 = BgL_head1179z00_2672;
									BgL_zc3z04anonymousza31511ze3z87_2676:
										if (NULLP(BgL_l1177z00_2677))
											{	/* Eval/evaluate_uncomp.scm 155 */
												BgL_arg1509z00_2670 = BgL_head1179z00_2672;
											}
										else
											{	/* Eval/evaluate_uncomp.scm 155 */
												obj_t BgL_newtail1181z00_2679;

												{	/* Eval/evaluate_uncomp.scm 155 */
													obj_t BgL_arg1514z00_2680;

													{	/* Eval/evaluate_uncomp.scm 155 */
														obj_t BgL_arg1516z00_2681;

														BgL_arg1516z00_2681 =
															CAR(((obj_t) BgL_l1177z00_2677));
														BgL_arg1514z00_2680 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_arg1516z00_2681));
													}
													BgL_newtail1181z00_2679 =
														MAKE_YOUNG_PAIR(BgL_arg1514z00_2680, BNIL);
												}
												SET_CDR(BgL_tail1180z00_2678, BgL_newtail1181z00_2679);
												{	/* Eval/evaluate_uncomp.scm 155 */
													obj_t BgL_arg1513z00_2682;

													BgL_arg1513z00_2682 =
														CDR(((obj_t) BgL_l1177z00_2677));
													{
														obj_t BgL_tail1180z00_3088;
														obj_t BgL_l1177z00_3087;

														BgL_l1177z00_3087 = BgL_arg1513z00_2682;
														BgL_tail1180z00_3088 = BgL_newtail1181z00_2679;
														BgL_tail1180z00_2678 = BgL_tail1180z00_3088;
														BgL_l1177z00_2677 = BgL_l1177z00_3087;
														goto BgL_zc3z04anonymousza31511ze3z87_2676;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1507z00_2669 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1509z00_2670,
						BNIL);
				}
				return MAKE_YOUNG_PAIR(BgL_arg1506z00_2668, BgL_arg1507z00_2669);
			}
		}

	}



/* &uncomp-ev_goto1243 */
	obj_t BGl_z62uncompzd2ev_goto1243zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2546, obj_t BgL_ez00_2547)
	{
		{	/* Eval/evaluate_uncomp.scm 149 */
			{	/* Eval/evaluate_uncomp.scm 151 */
				obj_t BgL_arg1492z00_2684;
				obj_t BgL_arg1494z00_2685;

				{	/* Eval/evaluate_uncomp.scm 151 */
					BgL_ev_varz00_bglt BgL_arg1495z00_2686;

					BgL_arg1495z00_2686 =
						(((BgL_ev_gotoz00_bglt) COBJECT(
								((BgL_ev_gotoz00_bglt) BgL_ez00_2547)))->BgL_labelz00);
					BgL_arg1492z00_2684 =
						BGl_uncompz00zz__evaluate_uncompz00(
						((BgL_ev_exprz00_bglt) BgL_arg1495z00_2686));
				}
				{	/* Eval/evaluate_uncomp.scm 151 */
					obj_t BgL_arg1497z00_2687;

					{	/* Eval/evaluate_uncomp.scm 151 */
						obj_t BgL_l1171z00_2688;

						BgL_l1171z00_2688 =
							(((BgL_ev_gotoz00_bglt) COBJECT(
									((BgL_ev_gotoz00_bglt) BgL_ez00_2547)))->BgL_argsz00);
						if (NULLP(BgL_l1171z00_2688))
							{	/* Eval/evaluate_uncomp.scm 151 */
								BgL_arg1497z00_2687 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 151 */
								obj_t BgL_head1173z00_2689;

								{	/* Eval/evaluate_uncomp.scm 151 */
									obj_t BgL_arg1504z00_2690;

									{	/* Eval/evaluate_uncomp.scm 151 */
										obj_t BgL_arg1505z00_2691;

										BgL_arg1505z00_2691 = CAR(((obj_t) BgL_l1171z00_2688));
										BgL_arg1504z00_2690 =
											BGl_uncompz00zz__evaluate_uncompz00(
											((BgL_ev_exprz00_bglt) BgL_arg1505z00_2691));
									}
									BgL_head1173z00_2689 =
										MAKE_YOUNG_PAIR(BgL_arg1504z00_2690, BNIL);
								}
								{	/* Eval/evaluate_uncomp.scm 151 */
									obj_t BgL_g1176z00_2692;

									BgL_g1176z00_2692 = CDR(((obj_t) BgL_l1171z00_2688));
									{
										obj_t BgL_l1171z00_2694;
										obj_t BgL_tail1174z00_2695;

										BgL_l1171z00_2694 = BgL_g1176z00_2692;
										BgL_tail1174z00_2695 = BgL_head1173z00_2689;
									BgL_zc3z04anonymousza31499ze3z87_2693:
										if (NULLP(BgL_l1171z00_2694))
											{	/* Eval/evaluate_uncomp.scm 151 */
												BgL_arg1497z00_2687 = BgL_head1173z00_2689;
											}
										else
											{	/* Eval/evaluate_uncomp.scm 151 */
												obj_t BgL_newtail1175z00_2696;

												{	/* Eval/evaluate_uncomp.scm 151 */
													obj_t BgL_arg1502z00_2697;

													{	/* Eval/evaluate_uncomp.scm 151 */
														obj_t BgL_arg1503z00_2698;

														BgL_arg1503z00_2698 =
															CAR(((obj_t) BgL_l1171z00_2694));
														BgL_arg1502z00_2697 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_arg1503z00_2698));
													}
													BgL_newtail1175z00_2696 =
														MAKE_YOUNG_PAIR(BgL_arg1502z00_2697, BNIL);
												}
												SET_CDR(BgL_tail1174z00_2695, BgL_newtail1175z00_2696);
												{	/* Eval/evaluate_uncomp.scm 151 */
													obj_t BgL_arg1501z00_2699;

													BgL_arg1501z00_2699 =
														CDR(((obj_t) BgL_l1171z00_2694));
													{
														obj_t BgL_tail1174z00_3117;
														obj_t BgL_l1171z00_3116;

														BgL_l1171z00_3116 = BgL_arg1501z00_2699;
														BgL_tail1174z00_3117 = BgL_newtail1175z00_2696;
														BgL_tail1174z00_2695 = BgL_tail1174z00_3117;
														BgL_l1171z00_2694 = BgL_l1171z00_3116;
														goto BgL_zc3z04anonymousza31499ze3z87_2693;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1494z00_2685 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1497z00_2687,
						BNIL);
				}
				return MAKE_YOUNG_PAIR(BgL_arg1492z00_2684, BgL_arg1494z00_2685);
			}
		}

	}



/* &uncomp-ev_labels1241 */
	obj_t BGl_z62uncompzd2ev_labels1241zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2548, obj_t BgL_ez00_2549)
	{
		{	/* Eval/evaluate_uncomp.scm 142 */
			{	/* Eval/evaluate_uncomp.scm 144 */
				obj_t BgL_arg1461z00_2701;

				{	/* Eval/evaluate_uncomp.scm 144 */
					obj_t BgL_arg1462z00_2702;
					obj_t BgL_arg1463z00_2703;

					{	/* Eval/evaluate_uncomp.scm 144 */
						obj_t BgL_ll1165z00_2704;
						obj_t BgL_ll1166z00_2705;

						BgL_ll1165z00_2704 =
							(((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_2549)))->BgL_varsz00);
						BgL_ll1166z00_2705 =
							(((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_2549)))->BgL_valsz00);
						if (NULLP(BgL_ll1165z00_2704))
							{	/* Eval/evaluate_uncomp.scm 144 */
								BgL_arg1462z00_2702 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 144 */
								obj_t BgL_head1167z00_2706;

								BgL_head1167z00_2706 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_ll1165z00_2708;
									obj_t BgL_ll1166z00_2709;
									obj_t BgL_tail1168z00_2710;

									BgL_ll1165z00_2708 = BgL_ll1165z00_2704;
									BgL_ll1166z00_2709 = BgL_ll1166z00_2705;
									BgL_tail1168z00_2710 = BgL_head1167z00_2706;
								BgL_zc3z04anonymousza31465ze3z87_2707:
									if (NULLP(BgL_ll1165z00_2708))
										{	/* Eval/evaluate_uncomp.scm 144 */
											BgL_arg1462z00_2702 = CDR(BgL_head1167z00_2706);
										}
									else
										{	/* Eval/evaluate_uncomp.scm 144 */
											obj_t BgL_newtail1169z00_2711;

											{	/* Eval/evaluate_uncomp.scm 144 */
												obj_t BgL_arg1469z00_2712;

												{	/* Eval/evaluate_uncomp.scm 144 */
													obj_t BgL_vz00_2713;
													obj_t BgL_az00_2714;

													BgL_vz00_2713 = CAR(((obj_t) BgL_ll1165z00_2708));
													BgL_az00_2714 = CAR(((obj_t) BgL_ll1166z00_2709));
													{	/* Eval/evaluate_uncomp.scm 144 */
														obj_t BgL_arg1472z00_2715;
														obj_t BgL_arg1473z00_2716;

														BgL_arg1472z00_2715 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_vz00_2713));
														{	/* Eval/evaluate_uncomp.scm 144 */
															obj_t BgL_arg1474z00_2717;

															{	/* Eval/evaluate_uncomp.scm 144 */
																obj_t BgL_arg1476z00_2718;

																{	/* Eval/evaluate_uncomp.scm 144 */
																	obj_t BgL_arg1477z00_2719;
																	obj_t BgL_arg1478z00_2720;

																	{	/* Eval/evaluate_uncomp.scm 144 */
																		obj_t BgL_l1159z00_2721;

																		BgL_l1159z00_2721 =
																			CAR(((obj_t) BgL_az00_2714));
																		if (NULLP(BgL_l1159z00_2721))
																			{	/* Eval/evaluate_uncomp.scm 144 */
																				BgL_arg1477z00_2719 = BNIL;
																			}
																		else
																			{	/* Eval/evaluate_uncomp.scm 144 */
																				obj_t BgL_head1161z00_2722;

																				{	/* Eval/evaluate_uncomp.scm 144 */
																					obj_t BgL_arg1485z00_2723;

																					{	/* Eval/evaluate_uncomp.scm 144 */
																						obj_t BgL_arg1486z00_2724;

																						BgL_arg1486z00_2724 =
																							CAR(((obj_t) BgL_l1159z00_2721));
																						BgL_arg1485z00_2723 =
																							BGl_uncompz00zz__evaluate_uncompz00
																							(((BgL_ev_exprz00_bglt)
																								BgL_arg1486z00_2724));
																					}
																					BgL_head1161z00_2722 =
																						MAKE_YOUNG_PAIR(BgL_arg1485z00_2723,
																						BNIL);
																				}
																				{	/* Eval/evaluate_uncomp.scm 144 */
																					obj_t BgL_g1164z00_2725;

																					BgL_g1164z00_2725 =
																						CDR(((obj_t) BgL_l1159z00_2721));
																					{
																						obj_t BgL_l1159z00_2727;
																						obj_t BgL_tail1162z00_2728;

																						BgL_l1159z00_2727 =
																							BgL_g1164z00_2725;
																						BgL_tail1162z00_2728 =
																							BgL_head1161z00_2722;
																					BgL_zc3z04anonymousza31480ze3z87_2726:
																						if (NULLP
																							(BgL_l1159z00_2727))
																							{	/* Eval/evaluate_uncomp.scm 144 */
																								BgL_arg1477z00_2719 =
																									BgL_head1161z00_2722;
																							}
																						else
																							{	/* Eval/evaluate_uncomp.scm 144 */
																								obj_t BgL_newtail1163z00_2729;

																								{	/* Eval/evaluate_uncomp.scm 144 */
																									obj_t BgL_arg1483z00_2730;

																									{	/* Eval/evaluate_uncomp.scm 144 */
																										obj_t BgL_arg1484z00_2731;

																										BgL_arg1484z00_2731 =
																											CAR(
																											((obj_t)
																												BgL_l1159z00_2727));
																										BgL_arg1483z00_2730 =
																											BGl_uncompz00zz__evaluate_uncompz00
																											(((BgL_ev_exprz00_bglt)
																												BgL_arg1484z00_2731));
																									}
																									BgL_newtail1163z00_2729 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1483z00_2730, BNIL);
																								}
																								SET_CDR(BgL_tail1162z00_2728,
																									BgL_newtail1163z00_2729);
																								{	/* Eval/evaluate_uncomp.scm 144 */
																									obj_t BgL_arg1482z00_2732;

																									BgL_arg1482z00_2732 =
																										CDR(
																										((obj_t)
																											BgL_l1159z00_2727));
																									{
																										obj_t BgL_tail1162z00_3158;
																										obj_t BgL_l1159z00_3157;

																										BgL_l1159z00_3157 =
																											BgL_arg1482z00_2732;
																										BgL_tail1162z00_3158 =
																											BgL_newtail1163z00_2729;
																										BgL_tail1162z00_2728 =
																											BgL_tail1162z00_3158;
																										BgL_l1159z00_2727 =
																											BgL_l1159z00_3157;
																										goto
																											BgL_zc3z04anonymousza31480ze3z87_2726;
																									}
																								}
																							}
																					}
																				}
																			}
																	}
																	{	/* Eval/evaluate_uncomp.scm 145 */
																		obj_t BgL_arg1487z00_2733;

																		{	/* Eval/evaluate_uncomp.scm 145 */
																			obj_t BgL_arg1488z00_2734;

																			BgL_arg1488z00_2734 =
																				CDR(((obj_t) BgL_az00_2714));
																			BgL_arg1487z00_2733 =
																				BGl_uncompz00zz__evaluate_uncompz00(
																				((BgL_ev_exprz00_bglt)
																					BgL_arg1488z00_2734));
																		}
																		BgL_arg1478z00_2720 =
																			MAKE_YOUNG_PAIR(BgL_arg1487z00_2733,
																			BNIL);
																	}
																	BgL_arg1476z00_2718 =
																		MAKE_YOUNG_PAIR(BgL_arg1477z00_2719,
																		BgL_arg1478z00_2720);
																}
																BgL_arg1474z00_2717 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1958z00zz__evaluate_uncompz00,
																	BgL_arg1476z00_2718);
															}
															BgL_arg1473z00_2716 =
																MAKE_YOUNG_PAIR(BgL_arg1474z00_2717, BNIL);
														}
														BgL_arg1469z00_2712 =
															MAKE_YOUNG_PAIR(BgL_arg1472z00_2715,
															BgL_arg1473z00_2716);
													}
												}
												BgL_newtail1169z00_2711 =
													MAKE_YOUNG_PAIR(BgL_arg1469z00_2712, BNIL);
											}
											SET_CDR(BgL_tail1168z00_2710, BgL_newtail1169z00_2711);
											{	/* Eval/evaluate_uncomp.scm 144 */
												obj_t BgL_arg1467z00_2735;
												obj_t BgL_arg1468z00_2736;

												BgL_arg1467z00_2735 = CDR(((obj_t) BgL_ll1165z00_2708));
												BgL_arg1468z00_2736 = CDR(((obj_t) BgL_ll1166z00_2709));
												{
													obj_t BgL_tail1168z00_3176;
													obj_t BgL_ll1166z00_3175;
													obj_t BgL_ll1165z00_3174;

													BgL_ll1165z00_3174 = BgL_arg1467z00_2735;
													BgL_ll1166z00_3175 = BgL_arg1468z00_2736;
													BgL_tail1168z00_3176 = BgL_newtail1169z00_2711;
													BgL_tail1168z00_2710 = BgL_tail1168z00_3176;
													BgL_ll1166z00_2709 = BgL_ll1166z00_3175;
													BgL_ll1165z00_2708 = BgL_ll1165z00_3174;
													goto BgL_zc3z04anonymousza31465ze3z87_2707;
												}
											}
										}
								}
							}
					}
					BgL_arg1463z00_2703 =
						MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(
							(((BgL_ev_labelsz00_bglt) COBJECT(
										((BgL_ev_labelsz00_bglt) BgL_ez00_2549)))->BgL_bodyz00)),
						BNIL);
					BgL_arg1461z00_2701 =
						MAKE_YOUNG_PAIR(BgL_arg1462z00_2702, BgL_arg1463z00_2703);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1960z00zz__evaluate_uncompz00,
					BgL_arg1461z00_2701);
			}
		}

	}



/* &uncomp-ev_letrec1239 */
	obj_t BGl_z62uncompzd2ev_letrec1239zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2550, obj_t BgL_ez00_2551)
	{
		{	/* Eval/evaluate_uncomp.scm 138 */
			{	/* Eval/evaluate_uncomp.scm 140 */
				obj_t BgL_arg1447z00_2738;

				{	/* Eval/evaluate_uncomp.scm 140 */
					obj_t BgL_arg1448z00_2739;
					obj_t BgL_arg1449z00_2740;

					{	/* Eval/evaluate_uncomp.scm 140 */
						obj_t BgL_ll1153z00_2741;
						obj_t BgL_ll1154z00_2742;

						BgL_ll1153z00_2741 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_2551))))->BgL_varsz00);
						BgL_ll1154z00_2742 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_2551))))->BgL_valsz00);
						if (NULLP(BgL_ll1153z00_2741))
							{	/* Eval/evaluate_uncomp.scm 140 */
								BgL_arg1448z00_2739 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 140 */
								obj_t BgL_head1155z00_2743;

								BgL_head1155z00_2743 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_ll1153z00_2745;
									obj_t BgL_ll1154z00_2746;
									obj_t BgL_tail1156z00_2747;

									BgL_ll1153z00_2745 = BgL_ll1153z00_2741;
									BgL_ll1154z00_2746 = BgL_ll1154z00_2742;
									BgL_tail1156z00_2747 = BgL_head1155z00_2743;
								BgL_zc3z04anonymousza31451ze3z87_2744:
									if (NULLP(BgL_ll1153z00_2745))
										{	/* Eval/evaluate_uncomp.scm 140 */
											BgL_arg1448z00_2739 = CDR(BgL_head1155z00_2743);
										}
									else
										{	/* Eval/evaluate_uncomp.scm 140 */
											obj_t BgL_newtail1157z00_2748;

											{	/* Eval/evaluate_uncomp.scm 140 */
												obj_t BgL_arg1455z00_2749;

												{	/* Eval/evaluate_uncomp.scm 140 */
													obj_t BgL_vz00_2750;
													obj_t BgL_az00_2751;

													BgL_vz00_2750 = CAR(((obj_t) BgL_ll1153z00_2745));
													BgL_az00_2751 = CAR(((obj_t) BgL_ll1154z00_2746));
													{	/* Eval/evaluate_uncomp.scm 140 */
														obj_t BgL_arg1456z00_2752;
														obj_t BgL_arg1457z00_2753;

														BgL_arg1456z00_2752 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_vz00_2750));
														{	/* Eval/evaluate_uncomp.scm 140 */
															obj_t BgL_arg1458z00_2754;

															BgL_arg1458z00_2754 =
																BGl_uncompz00zz__evaluate_uncompz00(
																((BgL_ev_exprz00_bglt) BgL_az00_2751));
															BgL_arg1457z00_2753 =
																MAKE_YOUNG_PAIR(BgL_arg1458z00_2754, BNIL);
														}
														BgL_arg1455z00_2749 =
															MAKE_YOUNG_PAIR(BgL_arg1456z00_2752,
															BgL_arg1457z00_2753);
													}
												}
												BgL_newtail1157z00_2748 =
													MAKE_YOUNG_PAIR(BgL_arg1455z00_2749, BNIL);
											}
											SET_CDR(BgL_tail1156z00_2747, BgL_newtail1157z00_2748);
											{	/* Eval/evaluate_uncomp.scm 140 */
												obj_t BgL_arg1453z00_2755;
												obj_t BgL_arg1454z00_2756;

												BgL_arg1453z00_2755 = CDR(((obj_t) BgL_ll1153z00_2745));
												BgL_arg1454z00_2756 = CDR(((obj_t) BgL_ll1154z00_2746));
												{
													obj_t BgL_tail1156z00_3213;
													obj_t BgL_ll1154z00_3212;
													obj_t BgL_ll1153z00_3211;

													BgL_ll1153z00_3211 = BgL_arg1453z00_2755;
													BgL_ll1154z00_3212 = BgL_arg1454z00_2756;
													BgL_tail1156z00_3213 = BgL_newtail1157z00_2748;
													BgL_tail1156z00_2747 = BgL_tail1156z00_3213;
													BgL_ll1154z00_2746 = BgL_ll1154z00_3212;
													BgL_ll1153z00_2745 = BgL_ll1153z00_3211;
													goto BgL_zc3z04anonymousza31451ze3z87_2744;
												}
											}
										}
								}
							}
					}
					{	/* Eval/evaluate_uncomp.scm 140 */
						obj_t BgL_arg1459z00_2757;

						{	/* Eval/evaluate_uncomp.scm 140 */
							BgL_ev_exprz00_bglt BgL_arg1460z00_2758;

							BgL_arg1460z00_2758 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_2551))))->BgL_bodyz00);
							BgL_arg1459z00_2757 =
								BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1460z00_2758);
						}
						BgL_arg1449z00_2740 = MAKE_YOUNG_PAIR(BgL_arg1459z00_2757, BNIL);
					}
					BgL_arg1447z00_2738 =
						MAKE_YOUNG_PAIR(BgL_arg1448z00_2739, BgL_arg1449z00_2740);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1960z00zz__evaluate_uncompz00,
					BgL_arg1447z00_2738);
			}
		}

	}



/* &uncomp-ev_let*1237 */
	obj_t BGl_z62uncompzd2ev_letza21237z12zz__evaluate_uncompz00(obj_t
		BgL_envz00_2552, obj_t BgL_ez00_2553)
	{
		{	/* Eval/evaluate_uncomp.scm 134 */
			{	/* Eval/evaluate_uncomp.scm 136 */
				obj_t BgL_arg1431z00_2760;

				{	/* Eval/evaluate_uncomp.scm 136 */
					obj_t BgL_arg1434z00_2761;
					obj_t BgL_arg1435z00_2762;

					{	/* Eval/evaluate_uncomp.scm 136 */
						obj_t BgL_ll1147z00_2763;
						obj_t BgL_ll1148z00_2764;

						BgL_ll1147z00_2763 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letza2za2_bglt) BgL_ez00_2553))))->BgL_varsz00);
						BgL_ll1148z00_2764 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letza2za2_bglt) BgL_ez00_2553))))->BgL_valsz00);
						if (NULLP(BgL_ll1147z00_2763))
							{	/* Eval/evaluate_uncomp.scm 136 */
								BgL_arg1434z00_2761 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 136 */
								obj_t BgL_head1149z00_2765;

								BgL_head1149z00_2765 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_ll1147z00_2767;
									obj_t BgL_ll1148z00_2768;
									obj_t BgL_tail1150z00_2769;

									BgL_ll1147z00_2767 = BgL_ll1147z00_2763;
									BgL_ll1148z00_2768 = BgL_ll1148z00_2764;
									BgL_tail1150z00_2769 = BgL_head1149z00_2765;
								BgL_zc3z04anonymousza31437ze3z87_2766:
									if (NULLP(BgL_ll1147z00_2767))
										{	/* Eval/evaluate_uncomp.scm 136 */
											BgL_arg1434z00_2761 = CDR(BgL_head1149z00_2765);
										}
									else
										{	/* Eval/evaluate_uncomp.scm 136 */
											obj_t BgL_newtail1151z00_2770;

											{	/* Eval/evaluate_uncomp.scm 136 */
												obj_t BgL_arg1441z00_2771;

												{	/* Eval/evaluate_uncomp.scm 136 */
													obj_t BgL_vz00_2772;
													obj_t BgL_az00_2773;

													BgL_vz00_2772 = CAR(((obj_t) BgL_ll1147z00_2767));
													BgL_az00_2773 = CAR(((obj_t) BgL_ll1148z00_2768));
													{	/* Eval/evaluate_uncomp.scm 136 */
														obj_t BgL_arg1442z00_2774;
														obj_t BgL_arg1443z00_2775;

														BgL_arg1442z00_2774 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_vz00_2772));
														{	/* Eval/evaluate_uncomp.scm 136 */
															obj_t BgL_arg1444z00_2776;

															BgL_arg1444z00_2776 =
																BGl_uncompz00zz__evaluate_uncompz00(
																((BgL_ev_exprz00_bglt) BgL_az00_2773));
															BgL_arg1443z00_2775 =
																MAKE_YOUNG_PAIR(BgL_arg1444z00_2776, BNIL);
														}
														BgL_arg1441z00_2771 =
															MAKE_YOUNG_PAIR(BgL_arg1442z00_2774,
															BgL_arg1443z00_2775);
													}
												}
												BgL_newtail1151z00_2770 =
													MAKE_YOUNG_PAIR(BgL_arg1441z00_2771, BNIL);
											}
											SET_CDR(BgL_tail1150z00_2769, BgL_newtail1151z00_2770);
											{	/* Eval/evaluate_uncomp.scm 136 */
												obj_t BgL_arg1439z00_2777;
												obj_t BgL_arg1440z00_2778;

												BgL_arg1439z00_2777 = CDR(((obj_t) BgL_ll1147z00_2767));
												BgL_arg1440z00_2778 = CDR(((obj_t) BgL_ll1148z00_2768));
												{
													obj_t BgL_tail1150z00_3251;
													obj_t BgL_ll1148z00_3250;
													obj_t BgL_ll1147z00_3249;

													BgL_ll1147z00_3249 = BgL_arg1439z00_2777;
													BgL_ll1148z00_3250 = BgL_arg1440z00_2778;
													BgL_tail1150z00_3251 = BgL_newtail1151z00_2770;
													BgL_tail1150z00_2769 = BgL_tail1150z00_3251;
													BgL_ll1148z00_2768 = BgL_ll1148z00_3250;
													BgL_ll1147z00_2767 = BgL_ll1147z00_3249;
													goto BgL_zc3z04anonymousza31437ze3z87_2766;
												}
											}
										}
								}
							}
					}
					{	/* Eval/evaluate_uncomp.scm 136 */
						obj_t BgL_arg1445z00_2779;

						{	/* Eval/evaluate_uncomp.scm 136 */
							BgL_ev_exprz00_bglt BgL_arg1446z00_2780;

							BgL_arg1446z00_2780 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letza2za2_bglt) BgL_ez00_2553))))->BgL_bodyz00);
							BgL_arg1445z00_2779 =
								BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1446z00_2780);
						}
						BgL_arg1435z00_2762 = MAKE_YOUNG_PAIR(BgL_arg1445z00_2779, BNIL);
					}
					BgL_arg1431z00_2760 =
						MAKE_YOUNG_PAIR(BgL_arg1434z00_2761, BgL_arg1435z00_2762);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1962z00zz__evaluate_uncompz00,
					BgL_arg1431z00_2760);
			}
		}

	}



/* &uncomp-ev_let1235 */
	obj_t BGl_z62uncompzd2ev_let1235zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2554, obj_t BgL_ez00_2555)
	{
		{	/* Eval/evaluate_uncomp.scm 130 */
			{	/* Eval/evaluate_uncomp.scm 132 */
				obj_t BgL_arg1417z00_2782;

				{	/* Eval/evaluate_uncomp.scm 132 */
					obj_t BgL_arg1418z00_2783;
					obj_t BgL_arg1419z00_2784;

					{	/* Eval/evaluate_uncomp.scm 132 */
						obj_t BgL_ll1141z00_2785;
						obj_t BgL_ll1142z00_2786;

						BgL_ll1141z00_2785 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letz00_bglt) BgL_ez00_2555))))->BgL_varsz00);
						BgL_ll1142z00_2786 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letz00_bglt) BgL_ez00_2555))))->BgL_valsz00);
						if (NULLP(BgL_ll1141z00_2785))
							{	/* Eval/evaluate_uncomp.scm 132 */
								BgL_arg1418z00_2783 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 132 */
								obj_t BgL_head1143z00_2787;

								BgL_head1143z00_2787 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_ll1141z00_2789;
									obj_t BgL_ll1142z00_2790;
									obj_t BgL_tail1144z00_2791;

									BgL_ll1141z00_2789 = BgL_ll1141z00_2785;
									BgL_ll1142z00_2790 = BgL_ll1142z00_2786;
									BgL_tail1144z00_2791 = BgL_head1143z00_2787;
								BgL_zc3z04anonymousza31421ze3z87_2788:
									if (NULLP(BgL_ll1141z00_2789))
										{	/* Eval/evaluate_uncomp.scm 132 */
											BgL_arg1418z00_2783 = CDR(BgL_head1143z00_2787);
										}
									else
										{	/* Eval/evaluate_uncomp.scm 132 */
											obj_t BgL_newtail1145z00_2792;

											{	/* Eval/evaluate_uncomp.scm 132 */
												obj_t BgL_arg1425z00_2793;

												{	/* Eval/evaluate_uncomp.scm 132 */
													obj_t BgL_vz00_2794;
													obj_t BgL_az00_2795;

													BgL_vz00_2794 = CAR(((obj_t) BgL_ll1141z00_2789));
													BgL_az00_2795 = CAR(((obj_t) BgL_ll1142z00_2790));
													{	/* Eval/evaluate_uncomp.scm 132 */
														obj_t BgL_arg1426z00_2796;
														obj_t BgL_arg1427z00_2797;

														BgL_arg1426z00_2796 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_vz00_2794));
														{	/* Eval/evaluate_uncomp.scm 132 */
															obj_t BgL_arg1428z00_2798;

															BgL_arg1428z00_2798 =
																BGl_uncompz00zz__evaluate_uncompz00(
																((BgL_ev_exprz00_bglt) BgL_az00_2795));
															BgL_arg1427z00_2797 =
																MAKE_YOUNG_PAIR(BgL_arg1428z00_2798, BNIL);
														}
														BgL_arg1425z00_2793 =
															MAKE_YOUNG_PAIR(BgL_arg1426z00_2796,
															BgL_arg1427z00_2797);
													}
												}
												BgL_newtail1145z00_2792 =
													MAKE_YOUNG_PAIR(BgL_arg1425z00_2793, BNIL);
											}
											SET_CDR(BgL_tail1144z00_2791, BgL_newtail1145z00_2792);
											{	/* Eval/evaluate_uncomp.scm 132 */
												obj_t BgL_arg1423z00_2799;
												obj_t BgL_arg1424z00_2800;

												BgL_arg1423z00_2799 = CDR(((obj_t) BgL_ll1141z00_2789));
												BgL_arg1424z00_2800 = CDR(((obj_t) BgL_ll1142z00_2790));
												{
													obj_t BgL_tail1144z00_3289;
													obj_t BgL_ll1142z00_3288;
													obj_t BgL_ll1141z00_3287;

													BgL_ll1141z00_3287 = BgL_arg1423z00_2799;
													BgL_ll1142z00_3288 = BgL_arg1424z00_2800;
													BgL_tail1144z00_3289 = BgL_newtail1145z00_2792;
													BgL_tail1144z00_2791 = BgL_tail1144z00_3289;
													BgL_ll1142z00_2790 = BgL_ll1142z00_3288;
													BgL_ll1141z00_2789 = BgL_ll1141z00_3287;
													goto BgL_zc3z04anonymousza31421ze3z87_2788;
												}
											}
										}
								}
							}
					}
					{	/* Eval/evaluate_uncomp.scm 132 */
						obj_t BgL_arg1429z00_2801;

						{	/* Eval/evaluate_uncomp.scm 132 */
							BgL_ev_exprz00_bglt BgL_arg1430z00_2802;

							BgL_arg1430z00_2802 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letz00_bglt) BgL_ez00_2555))))->BgL_bodyz00);
							BgL_arg1429z00_2801 =
								BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1430z00_2802);
						}
						BgL_arg1419z00_2784 = MAKE_YOUNG_PAIR(BgL_arg1429z00_2801, BNIL);
					}
					BgL_arg1417z00_2782 =
						MAKE_YOUNG_PAIR(BgL_arg1418z00_2783, BgL_arg1419z00_2784);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1964z00zz__evaluate_uncompz00,
					BgL_arg1417z00_2782);
			}
		}

	}



/* &uncomp-ev_synchroniz1233 */
	obj_t BGl_z62uncompzd2ev_synchroniza71233z17zz__evaluate_uncompz00(obj_t
		BgL_envz00_2556, obj_t BgL_ez00_2557)
	{
		{	/* Eval/evaluate_uncomp.scm 126 */
			{	/* Eval/evaluate_uncomp.scm 128 */
				obj_t BgL_arg1406z00_2804;

				{	/* Eval/evaluate_uncomp.scm 128 */
					obj_t BgL_arg1407z00_2805;
					obj_t BgL_arg1408z00_2806;

					BgL_arg1407z00_2805 =
						BGl_uncompz00zz__evaluate_uncompz00(
						(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
									((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2557)))->
							BgL_mutexz00));
					{	/* Eval/evaluate_uncomp.scm 128 */
						obj_t BgL_arg1411z00_2807;

						{	/* Eval/evaluate_uncomp.scm 128 */
							obj_t BgL_arg1412z00_2808;
							obj_t BgL_arg1413z00_2809;

							BgL_arg1412z00_2808 =
								BGl_uncompz00zz__evaluate_uncompz00(
								(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
											((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2557)))->
									BgL_prelockz00));
							BgL_arg1413z00_2809 =
								MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(((
											(BgL_ev_synchroniza7eza7_bglt)
											COBJECT(((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2557)))->
										BgL_bodyz00)), BNIL);
							BgL_arg1411z00_2807 =
								MAKE_YOUNG_PAIR(BgL_arg1412z00_2808, BgL_arg1413z00_2809);
						}
						BgL_arg1408z00_2806 =
							MAKE_YOUNG_PAIR(BGl_keyword1966z00zz__evaluate_uncompz00,
							BgL_arg1411z00_2807);
					}
					BgL_arg1406z00_2804 =
						MAKE_YOUNG_PAIR(BgL_arg1407z00_2805, BgL_arg1408z00_2806);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1968z00zz__evaluate_uncompz00,
					BgL_arg1406z00_2804);
			}
		}

	}



/* &uncomp-ev_with-handl1231 */
	obj_t BGl_z62uncompzd2ev_withzd2handl1231z62zz__evaluate_uncompz00(obj_t
		BgL_envz00_2558, obj_t BgL_ez00_2559)
	{
		{	/* Eval/evaluate_uncomp.scm 122 */
			{	/* Eval/evaluate_uncomp.scm 124 */
				obj_t BgL_arg1400z00_2811;

				{	/* Eval/evaluate_uncomp.scm 124 */
					obj_t BgL_arg1401z00_2812;
					obj_t BgL_arg1402z00_2813;

					BgL_arg1401z00_2812 =
						BGl_uncompz00zz__evaluate_uncompz00(
						(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
									((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_2559)))->
							BgL_handlerz00));
					BgL_arg1402z00_2813 =
						MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(((
									(BgL_ev_withzd2handlerzd2_bglt)
									COBJECT(((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_2559)))->
								BgL_bodyz00)), BNIL);
					BgL_arg1400z00_2811 =
						MAKE_YOUNG_PAIR(BgL_arg1401z00_2812, BgL_arg1402z00_2813);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1970z00zz__evaluate_uncompz00,
					BgL_arg1400z00_2811);
			}
		}

	}



/* &uncomp-ev_unwind-pro1229 */
	obj_t BGl_z62uncompzd2ev_unwindzd2pro1229z62zz__evaluate_uncompz00(obj_t
		BgL_envz00_2560, obj_t BgL_ez00_2561)
	{
		{	/* Eval/evaluate_uncomp.scm 118 */
			{	/* Eval/evaluate_uncomp.scm 120 */
				obj_t BgL_arg1393z00_2815;

				{	/* Eval/evaluate_uncomp.scm 120 */
					obj_t BgL_arg1394z00_2816;
					obj_t BgL_arg1395z00_2817;

					BgL_arg1394z00_2816 =
						BGl_uncompz00zz__evaluate_uncompz00(
						(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
									((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_2561)))->
							BgL_ez00));
					BgL_arg1395z00_2817 =
						MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(((
									(BgL_ev_unwindzd2protectzd2_bglt)
									COBJECT(((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_2561)))->
								BgL_bodyz00)), BNIL);
					BgL_arg1393z00_2815 =
						MAKE_YOUNG_PAIR(BgL_arg1394z00_2816, BgL_arg1395z00_2817);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1972z00zz__evaluate_uncompz00,
					BgL_arg1393z00_2815);
			}
		}

	}



/* &uncomp-ev_bind-exit1227 */
	obj_t BGl_z62uncompzd2ev_bindzd2exit1227z62zz__evaluate_uncompz00(obj_t
		BgL_envz00_2562, obj_t BgL_ez00_2563)
	{
		{	/* Eval/evaluate_uncomp.scm 114 */
			{	/* Eval/evaluate_uncomp.scm 116 */
				obj_t BgL_arg1384z00_2819;

				{	/* Eval/evaluate_uncomp.scm 116 */
					obj_t BgL_arg1387z00_2820;
					obj_t BgL_arg1388z00_2821;

					{	/* Eval/evaluate_uncomp.scm 116 */
						obj_t BgL_arg1389z00_2822;

						{	/* Eval/evaluate_uncomp.scm 116 */
							BgL_ev_varz00_bglt BgL_arg1390z00_2823;

							BgL_arg1390z00_2823 =
								(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
										((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_2563)))->BgL_varz00);
							BgL_arg1389z00_2822 =
								BGl_uncompz00zz__evaluate_uncompz00(
								((BgL_ev_exprz00_bglt) BgL_arg1390z00_2823));
						}
						BgL_arg1387z00_2820 = MAKE_YOUNG_PAIR(BgL_arg1389z00_2822, BNIL);
					}
					BgL_arg1388z00_2821 =
						MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(
							(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
										((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_2563)))->
								BgL_bodyz00)), BNIL);
					BgL_arg1384z00_2819 =
						MAKE_YOUNG_PAIR(BgL_arg1387z00_2820, BgL_arg1388z00_2821);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1974z00zz__evaluate_uncompz00,
					BgL_arg1384z00_2819);
			}
		}

	}



/* &uncomp-ev_setlocal1225 */
	obj_t BGl_z62uncompzd2ev_setlocal1225zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2564, obj_t BgL_ez00_2565)
	{
		{	/* Eval/evaluate_uncomp.scm 110 */
			{	/* Eval/evaluate_uncomp.scm 112 */
				obj_t BgL_arg1377z00_2825;

				{	/* Eval/evaluate_uncomp.scm 112 */
					obj_t BgL_arg1378z00_2826;
					obj_t BgL_arg1379z00_2827;

					{	/* Eval/evaluate_uncomp.scm 112 */
						BgL_ev_varz00_bglt BgL_arg1380z00_2828;

						BgL_arg1380z00_2828 =
							(((BgL_ev_setlocalz00_bglt) COBJECT(
									((BgL_ev_setlocalz00_bglt) BgL_ez00_2565)))->BgL_vz00);
						BgL_arg1378z00_2826 =
							BGl_uncompz00zz__evaluate_uncompz00(
							((BgL_ev_exprz00_bglt) BgL_arg1380z00_2828));
					}
					{	/* Eval/evaluate_uncomp.scm 112 */
						obj_t BgL_arg1382z00_2829;

						{	/* Eval/evaluate_uncomp.scm 112 */
							BgL_ev_exprz00_bglt BgL_arg1383z00_2830;

							BgL_arg1383z00_2830 =
								(((BgL_ev_hookz00_bglt) COBJECT(
										((BgL_ev_hookz00_bglt)
											((BgL_ev_setlocalz00_bglt) BgL_ez00_2565))))->BgL_ez00);
							BgL_arg1382z00_2829 =
								BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1383z00_2830);
						}
						BgL_arg1379z00_2827 = MAKE_YOUNG_PAIR(BgL_arg1382z00_2829, BNIL);
					}
					BgL_arg1377z00_2825 =
						MAKE_YOUNG_PAIR(BgL_arg1378z00_2826, BgL_arg1379z00_2827);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1976z00zz__evaluate_uncompz00,
					BgL_arg1377z00_2825);
			}
		}

	}



/* &uncomp-ev_defglobal1223 */
	obj_t BGl_z62uncompzd2ev_defglobal1223zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2566, obj_t BgL_ez00_2567)
	{
		{	/* Eval/evaluate_uncomp.scm 106 */
			{	/* Eval/evaluate_uncomp.scm 108 */
				obj_t BgL_arg1371z00_2832;

				{	/* Eval/evaluate_uncomp.scm 108 */
					obj_t BgL_arg1372z00_2833;
					obj_t BgL_arg1373z00_2834;

					BgL_arg1372z00_2833 =
						(((BgL_ev_setglobalz00_bglt) COBJECT(
								((BgL_ev_setglobalz00_bglt)
									((BgL_ev_defglobalz00_bglt) BgL_ez00_2567))))->BgL_namez00);
					{	/* Eval/evaluate_uncomp.scm 108 */
						obj_t BgL_arg1375z00_2835;

						{	/* Eval/evaluate_uncomp.scm 108 */
							BgL_ev_exprz00_bglt BgL_arg1376z00_2836;

							BgL_arg1376z00_2836 =
								(((BgL_ev_hookz00_bglt) COBJECT(
										((BgL_ev_hookz00_bglt)
											((BgL_ev_defglobalz00_bglt) BgL_ez00_2567))))->BgL_ez00);
							BgL_arg1375z00_2835 =
								BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1376z00_2836);
						}
						BgL_arg1373z00_2834 = MAKE_YOUNG_PAIR(BgL_arg1375z00_2835, BNIL);
					}
					BgL_arg1371z00_2832 =
						MAKE_YOUNG_PAIR(BgL_arg1372z00_2833, BgL_arg1373z00_2834);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1978z00zz__evaluate_uncompz00,
					BgL_arg1371z00_2832);
			}
		}

	}



/* &uncomp-ev_setglobal1221 */
	obj_t BGl_z62uncompzd2ev_setglobal1221zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2568, obj_t BgL_ez00_2569)
	{
		{	/* Eval/evaluate_uncomp.scm 102 */
			{	/* Eval/evaluate_uncomp.scm 104 */
				obj_t BgL_arg1366z00_2838;

				{	/* Eval/evaluate_uncomp.scm 104 */
					obj_t BgL_arg1367z00_2839;
					obj_t BgL_arg1368z00_2840;

					BgL_arg1367z00_2839 =
						(((BgL_ev_setglobalz00_bglt) COBJECT(
								((BgL_ev_setglobalz00_bglt) BgL_ez00_2569)))->BgL_namez00);
					{	/* Eval/evaluate_uncomp.scm 104 */
						obj_t BgL_arg1369z00_2841;

						{	/* Eval/evaluate_uncomp.scm 104 */
							BgL_ev_exprz00_bglt BgL_arg1370z00_2842;

							BgL_arg1370z00_2842 =
								(((BgL_ev_hookz00_bglt) COBJECT(
										((BgL_ev_hookz00_bglt)
											((BgL_ev_setglobalz00_bglt) BgL_ez00_2569))))->BgL_ez00);
							BgL_arg1369z00_2841 =
								BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1370z00_2842);
						}
						BgL_arg1368z00_2840 = MAKE_YOUNG_PAIR(BgL_arg1369z00_2841, BNIL);
					}
					BgL_arg1366z00_2838 =
						MAKE_YOUNG_PAIR(BgL_arg1367z00_2839, BgL_arg1368z00_2840);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1976z00zz__evaluate_uncompz00,
					BgL_arg1366z00_2838);
			}
		}

	}



/* &uncomp-ev_trap1219 */
	obj_t BGl_z62uncompzd2ev_trap1219zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2570, obj_t BgL_ez00_2571)
	{
		{	/* Eval/evaluate_uncomp.scm 98 */
			{	/* Eval/evaluate_uncomp.scm 100 */
				obj_t BgL_arg1363z00_2844;

				{	/* Eval/evaluate_uncomp.scm 100 */
					obj_t BgL_arg1364z00_2845;

					{	/* Eval/evaluate_uncomp.scm 100 */
						BgL_ev_exprz00_bglt BgL_arg1365z00_2846;

						BgL_arg1365z00_2846 =
							(((BgL_ev_hookz00_bglt) COBJECT(
									((BgL_ev_hookz00_bglt)
										((BgL_ev_trapz00_bglt) BgL_ez00_2571))))->BgL_ez00);
						BgL_arg1364z00_2845 =
							BGl_uncompz00zz__evaluate_uncompz00(BgL_arg1365z00_2846);
					}
					BgL_arg1363z00_2844 = MAKE_YOUNG_PAIR(BgL_arg1364z00_2845, BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1980z00zz__evaluate_uncompz00,
					BgL_arg1363z00_2844);
			}
		}

	}



/* &uncomp-ev_prog21217 */
	obj_t BGl_z62uncompzd2ev_prog21217zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2572, obj_t BgL_ez00_2573)
	{
		{	/* Eval/evaluate_uncomp.scm 91 */
			{	/* Eval/evaluate_uncomp.scm 93 */
				obj_t BgL_e1z00_2848;
				obj_t BgL_e2z00_2849;

				BgL_e1z00_2848 =
					BGl_uncompz00zz__evaluate_uncompz00(
					(((BgL_ev_prog2z00_bglt) COBJECT(
								((BgL_ev_prog2z00_bglt) BgL_ez00_2573)))->BgL_e1z00));
				BgL_e2z00_2849 =
					BGl_uncompz00zz__evaluate_uncompz00(
					(((BgL_ev_prog2z00_bglt) COBJECT(
								((BgL_ev_prog2z00_bglt) BgL_ez00_2573)))->BgL_e2z00));
				{	/* Eval/evaluate_uncomp.scm 94 */
					bool_t BgL_test2069z00_3382;

					if (PAIRP(BgL_e2z00_2849))
						{	/* Eval/evaluate_uncomp.scm 94 */
							BgL_test2069z00_3382 =
								(CAR(BgL_e2z00_2849) ==
								BGl_symbol1982z00zz__evaluate_uncompz00);
						}
					else
						{	/* Eval/evaluate_uncomp.scm 94 */
							BgL_test2069z00_3382 = ((bool_t) 0);
						}
					if (BgL_test2069z00_3382)
						{	/* Eval/evaluate_uncomp.scm 95 */
							obj_t BgL_arg1354z00_2850;

							BgL_arg1354z00_2850 =
								MAKE_YOUNG_PAIR(BgL_e1z00_2848,
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(CDR
									(BgL_e2z00_2849), BNIL));
							return MAKE_YOUNG_PAIR(BGl_symbol1982z00zz__evaluate_uncompz00,
								BgL_arg1354z00_2850);
						}
					else
						{	/* Eval/evaluate_uncomp.scm 96 */
							obj_t BgL_arg1358z00_2851;

							{	/* Eval/evaluate_uncomp.scm 96 */
								obj_t BgL_arg1359z00_2852;

								BgL_arg1359z00_2852 = MAKE_YOUNG_PAIR(BgL_e2z00_2849, BNIL);
								BgL_arg1358z00_2851 =
									MAKE_YOUNG_PAIR(BgL_e1z00_2848, BgL_arg1359z00_2852);
							}
							return
								MAKE_YOUNG_PAIR(BGl_symbol1982z00zz__evaluate_uncompz00,
								BgL_arg1358z00_2851);
						}
				}
			}
		}

	}



/* &uncomp-ev_and1215 */
	obj_t BGl_z62uncompzd2ev_and1215zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2574, obj_t BgL_ez00_2575)
	{
		{	/* Eval/evaluate_uncomp.scm 87 */
			{	/* Eval/evaluate_uncomp.scm 89 */
				obj_t BgL_arg1341z00_2854;

				{	/* Eval/evaluate_uncomp.scm 89 */
					obj_t BgL_arg1342z00_2855;

					{	/* Eval/evaluate_uncomp.scm 89 */
						obj_t BgL_l1135z00_2856;

						BgL_l1135z00_2856 =
							(((BgL_ev_listz00_bglt) COBJECT(
									((BgL_ev_listz00_bglt)
										((BgL_ev_andz00_bglt) BgL_ez00_2575))))->BgL_argsz00);
						if (NULLP(BgL_l1135z00_2856))
							{	/* Eval/evaluate_uncomp.scm 89 */
								BgL_arg1342z00_2855 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 89 */
								obj_t BgL_head1137z00_2857;

								{	/* Eval/evaluate_uncomp.scm 89 */
									obj_t BgL_arg1349z00_2858;

									{	/* Eval/evaluate_uncomp.scm 89 */
										obj_t BgL_arg1350z00_2859;

										BgL_arg1350z00_2859 = CAR(((obj_t) BgL_l1135z00_2856));
										BgL_arg1349z00_2858 =
											BGl_uncompz00zz__evaluate_uncompz00(
											((BgL_ev_exprz00_bglt) BgL_arg1350z00_2859));
									}
									BgL_head1137z00_2857 =
										MAKE_YOUNG_PAIR(BgL_arg1349z00_2858, BNIL);
								}
								{	/* Eval/evaluate_uncomp.scm 89 */
									obj_t BgL_g1140z00_2860;

									BgL_g1140z00_2860 = CDR(((obj_t) BgL_l1135z00_2856));
									{
										obj_t BgL_l1135z00_2862;
										obj_t BgL_tail1138z00_2863;

										BgL_l1135z00_2862 = BgL_g1140z00_2860;
										BgL_tail1138z00_2863 = BgL_head1137z00_2857;
									BgL_zc3z04anonymousza31344ze3z87_2861:
										if (NULLP(BgL_l1135z00_2862))
											{	/* Eval/evaluate_uncomp.scm 89 */
												BgL_arg1342z00_2855 = BgL_head1137z00_2857;
											}
										else
											{	/* Eval/evaluate_uncomp.scm 89 */
												obj_t BgL_newtail1139z00_2864;

												{	/* Eval/evaluate_uncomp.scm 89 */
													obj_t BgL_arg1347z00_2865;

													{	/* Eval/evaluate_uncomp.scm 89 */
														obj_t BgL_arg1348z00_2866;

														BgL_arg1348z00_2866 =
															CAR(((obj_t) BgL_l1135z00_2862));
														BgL_arg1347z00_2865 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_arg1348z00_2866));
													}
													BgL_newtail1139z00_2864 =
														MAKE_YOUNG_PAIR(BgL_arg1347z00_2865, BNIL);
												}
												SET_CDR(BgL_tail1138z00_2863, BgL_newtail1139z00_2864);
												{	/* Eval/evaluate_uncomp.scm 89 */
													obj_t BgL_arg1346z00_2867;

													BgL_arg1346z00_2867 =
														CDR(((obj_t) BgL_l1135z00_2862));
													{
														obj_t BgL_tail1138z00_3417;
														obj_t BgL_l1135z00_3416;

														BgL_l1135z00_3416 = BgL_arg1346z00_2867;
														BgL_tail1138z00_3417 = BgL_newtail1139z00_2864;
														BgL_tail1138z00_2863 = BgL_tail1138z00_3417;
														BgL_l1135z00_2862 = BgL_l1135z00_3416;
														goto BgL_zc3z04anonymousza31344ze3z87_2861;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1341z00_2854 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1342z00_2855,
						BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1984z00zz__evaluate_uncompz00,
					BgL_arg1341z00_2854);
			}
		}

	}



/* &uncomp-ev_or1213 */
	obj_t BGl_z62uncompzd2ev_or1213zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2576, obj_t BgL_ez00_2577)
	{
		{	/* Eval/evaluate_uncomp.scm 83 */
			{	/* Eval/evaluate_uncomp.scm 85 */
				obj_t BgL_arg1331z00_2869;

				{	/* Eval/evaluate_uncomp.scm 85 */
					obj_t BgL_arg1332z00_2870;

					{	/* Eval/evaluate_uncomp.scm 85 */
						obj_t BgL_l1129z00_2871;

						BgL_l1129z00_2871 =
							(((BgL_ev_listz00_bglt) COBJECT(
									((BgL_ev_listz00_bglt)
										((BgL_ev_orz00_bglt) BgL_ez00_2577))))->BgL_argsz00);
						if (NULLP(BgL_l1129z00_2871))
							{	/* Eval/evaluate_uncomp.scm 85 */
								BgL_arg1332z00_2870 = BNIL;
							}
						else
							{	/* Eval/evaluate_uncomp.scm 85 */
								obj_t BgL_head1131z00_2872;

								{	/* Eval/evaluate_uncomp.scm 85 */
									obj_t BgL_arg1339z00_2873;

									{	/* Eval/evaluate_uncomp.scm 85 */
										obj_t BgL_arg1340z00_2874;

										BgL_arg1340z00_2874 = CAR(((obj_t) BgL_l1129z00_2871));
										BgL_arg1339z00_2873 =
											BGl_uncompz00zz__evaluate_uncompz00(
											((BgL_ev_exprz00_bglt) BgL_arg1340z00_2874));
									}
									BgL_head1131z00_2872 =
										MAKE_YOUNG_PAIR(BgL_arg1339z00_2873, BNIL);
								}
								{	/* Eval/evaluate_uncomp.scm 85 */
									obj_t BgL_g1134z00_2875;

									BgL_g1134z00_2875 = CDR(((obj_t) BgL_l1129z00_2871));
									{
										obj_t BgL_l1129z00_2877;
										obj_t BgL_tail1132z00_2878;

										BgL_l1129z00_2877 = BgL_g1134z00_2875;
										BgL_tail1132z00_2878 = BgL_head1131z00_2872;
									BgL_zc3z04anonymousza31334ze3z87_2876:
										if (NULLP(BgL_l1129z00_2877))
											{	/* Eval/evaluate_uncomp.scm 85 */
												BgL_arg1332z00_2870 = BgL_head1131z00_2872;
											}
										else
											{	/* Eval/evaluate_uncomp.scm 85 */
												obj_t BgL_newtail1133z00_2879;

												{	/* Eval/evaluate_uncomp.scm 85 */
													obj_t BgL_arg1337z00_2880;

													{	/* Eval/evaluate_uncomp.scm 85 */
														obj_t BgL_arg1338z00_2881;

														BgL_arg1338z00_2881 =
															CAR(((obj_t) BgL_l1129z00_2877));
														BgL_arg1337z00_2880 =
															BGl_uncompz00zz__evaluate_uncompz00(
															((BgL_ev_exprz00_bglt) BgL_arg1338z00_2881));
													}
													BgL_newtail1133z00_2879 =
														MAKE_YOUNG_PAIR(BgL_arg1337z00_2880, BNIL);
												}
												SET_CDR(BgL_tail1132z00_2878, BgL_newtail1133z00_2879);
												{	/* Eval/evaluate_uncomp.scm 85 */
													obj_t BgL_arg1336z00_2882;

													BgL_arg1336z00_2882 =
														CDR(((obj_t) BgL_l1129z00_2877));
													{
														obj_t BgL_tail1132z00_3443;
														obj_t BgL_l1129z00_3442;

														BgL_l1129z00_3442 = BgL_arg1336z00_2882;
														BgL_tail1132z00_3443 = BgL_newtail1133z00_2879;
														BgL_tail1132z00_2878 = BgL_tail1132z00_3443;
														BgL_l1129z00_2877 = BgL_l1129z00_3442;
														goto BgL_zc3z04anonymousza31334ze3z87_2876;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1331z00_2869 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1332z00_2870,
						BNIL);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1986z00zz__evaluate_uncompz00,
					BgL_arg1331z00_2869);
			}
		}

	}



/* &uncomp-ev_if1211 */
	obj_t BGl_z62uncompzd2ev_if1211zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2578, obj_t BgL_ez00_2579)
	{
		{	/* Eval/evaluate_uncomp.scm 79 */
			{	/* Eval/evaluate_uncomp.scm 81 */
				obj_t BgL_arg1320z00_2884;

				{	/* Eval/evaluate_uncomp.scm 81 */
					obj_t BgL_arg1321z00_2885;
					obj_t BgL_arg1322z00_2886;

					BgL_arg1321z00_2885 =
						BGl_uncompz00zz__evaluate_uncompz00(
						(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_ez00_2579)))->BgL_pz00));
					{	/* Eval/evaluate_uncomp.scm 81 */
						obj_t BgL_arg1325z00_2887;
						obj_t BgL_arg1326z00_2888;

						BgL_arg1325z00_2887 =
							BGl_uncompz00zz__evaluate_uncompz00(
							(((BgL_ev_ifz00_bglt) COBJECT(
										((BgL_ev_ifz00_bglt) BgL_ez00_2579)))->BgL_tz00));
						BgL_arg1326z00_2888 =
							MAKE_YOUNG_PAIR(BGl_uncompz00zz__evaluate_uncompz00(
								(((BgL_ev_ifz00_bglt) COBJECT(
											((BgL_ev_ifz00_bglt) BgL_ez00_2579)))->BgL_ez00)), BNIL);
						BgL_arg1322z00_2886 =
							MAKE_YOUNG_PAIR(BgL_arg1325z00_2887, BgL_arg1326z00_2888);
					}
					BgL_arg1320z00_2884 =
						MAKE_YOUNG_PAIR(BgL_arg1321z00_2885, BgL_arg1322z00_2886);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol1988z00zz__evaluate_uncompz00,
					BgL_arg1320z00_2884);
			}
		}

	}



/* &uncomp-ev_litt1209 */
	obj_t BGl_z62uncompzd2ev_litt1209zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2580, obj_t BgL_ez00_2581)
	{
		{	/* Eval/evaluate_uncomp.scm 75 */
			{	/* Eval/evaluate_uncomp.scm 77 */
				obj_t BgL_arg1318z00_2890;

				BgL_arg1318z00_2890 =
					MAKE_YOUNG_PAIR(
					(((BgL_ev_littz00_bglt) COBJECT(
								((BgL_ev_littz00_bglt) BgL_ez00_2581)))->BgL_valuez00), BNIL);
				return
					MAKE_YOUNG_PAIR(BGl_symbol1990z00zz__evaluate_uncompz00,
					BgL_arg1318z00_2890);
			}
		}

	}



/* &uncomp-ev_global1207 */
	obj_t BGl_z62uncompzd2ev_global1207zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2582, obj_t BgL_varz00_2583)
	{
		{	/* Eval/evaluate_uncomp.scm 71 */
			return
				(((BgL_ev_globalz00_bglt) COBJECT(
						((BgL_ev_globalz00_bglt) BgL_varz00_2583)))->BgL_namez00);
		}

	}



/* &uncomp-ev_var1205 */
	obj_t BGl_z62uncompzd2ev_var1205zb0zz__evaluate_uncompz00(obj_t
		BgL_envz00_2584, obj_t BgL_varz00_2585)
	{
		{	/* Eval/evaluate_uncomp.scm 67 */
			return
				(((BgL_ev_varz00_bglt) COBJECT(
						((BgL_ev_varz00_bglt) BgL_varz00_2585)))->BgL_namez00);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_uncompz00(void)
	{
		{	/* Eval/evaluate_uncomp.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__ppz00(233942021L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
			return BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(0L,
				BSTRING_TO_STRING(BGl_string1992z00zz__evaluate_uncompz00));
		}

	}

#ifdef __cplusplus
}
#endif
