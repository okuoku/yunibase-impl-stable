/*===========================================================================*/
/*   (Eval/expdrecord.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdrecord.scm -indent -o objs/obj_u/Eval/expdrecord.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_RECORD_TYPE_DEFINITIONS
#define BGL___EXPANDER_RECORD_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_RECORD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1757z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1759z00zz__expander_recordz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1761z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1763z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1765z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1767z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1769z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_recordz00 =
		BUNSPEC;
	static obj_t BGl_expandzd2errorzd2zz__expander_recordz00(bool_t, obj_t,
		obj_t);
	static obj_t BGl_symbol1771z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1773z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1775z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1777z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1779z00zz__expander_recordz00 = BUNSPEC;
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	static obj_t
		BGl_z62expandzd2definezd2recordzd2typezb0zz__expander_recordz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_recordz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol1781z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1783z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1787z00zz__expander_recordz00 = BUNSPEC;
	static obj_t BGl_symbol1789z00zz__expander_recordz00 = BUNSPEC;
	extern obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_recordz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__expander_recordz00(void);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_recordz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_recordz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2definezd2recordzd2typezd2zz__expander_recordz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__expander_recordz00(void);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_recordzd2ze3structz31zz__expander_recordz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_recordz00(void);
	static obj_t BGl_loopze70ze7zz__expander_recordz00(obj_t, obj_t, obj_t, long);
	static obj_t BGl_loopze71ze7zz__expander_recordz00(obj_t, obj_t, obj_t, long);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1790z00zz__expander_recordz00,
		BgL_bgl_string1790za700za7za7_1793za7, "struct-ref", 10);
	      DEFINE_STRING(BGl_string1791z00zz__expander_recordz00,
		BgL_bgl_string1791za700za7za7_1794za7, "invalid field spec", 18);
	      DEFINE_STRING(BGl_string1792z00zz__expander_recordz00,
		BgL_bgl_string1792za700za7za7_1795za7, "__expander_record", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2definezd2recordzd2typezd2envz00zz__expander_recordz00,
		BgL_bgl_za762expandza7d2defi1796z00,
		BGl_z62expandzd2definezd2recordzd2typezb0zz__expander_recordz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1756z00zz__expander_recordz00,
		BgL_bgl_string1756za700za7za7_1797za7, "invalid form", 12);
	      DEFINE_STRING(BGl_string1758z00zz__expander_recordz00,
		BgL_bgl_string1758za700za7za7_1798za7, ">", 1);
	      DEFINE_STRING(BGl_string1760z00zz__expander_recordz00,
		BgL_bgl_string1760za700za7za7_1799za7, "<", 1);
	      DEFINE_STRING(BGl_string1762z00zz__expander_recordz00,
		BgL_bgl_string1762za700za7za7_1800za7, "quote", 5);
	      DEFINE_STRING(BGl_string1764z00zz__expander_recordz00,
		BgL_bgl_string1764za700za7za7_1801za7, "make-struct", 11);
	      DEFINE_STRING(BGl_string1766z00zz__expander_recordz00,
		BgL_bgl_string1766za700za7za7_1802za7, "begin", 5);
	      DEFINE_STRING(BGl_string1768z00zz__expander_recordz00,
		BgL_bgl_string1768za700za7za7_1803za7, "let", 3);
	      DEFINE_STRING(BGl_string1770z00zz__expander_recordz00,
		BgL_bgl_string1770za700za7za7_1804za7, "define", 6);
	      DEFINE_STRING(BGl_string1772z00zz__expander_recordz00,
		BgL_bgl_string1772za700za7za7_1805za7, "o", 1);
	      DEFINE_STRING(BGl_string1774z00zz__expander_recordz00,
		BgL_bgl_string1774za700za7za7_1806za7, "struct?", 7);
	      DEFINE_STRING(BGl_string1776z00zz__expander_recordz00,
		BgL_bgl_string1776za700za7za7_1807za7, "struct-key", 10);
	      DEFINE_STRING(BGl_string1778z00zz__expander_recordz00,
		BgL_bgl_string1778za700za7za7_1808za7, "eq?", 3);
	      DEFINE_STRING(BGl_string1780z00zz__expander_recordz00,
		BgL_bgl_string1780za700za7za7_1809za7, "struct-length", 13);
	      DEFINE_STRING(BGl_string1782z00zz__expander_recordz00,
		BgL_bgl_string1782za700za7za7_1810za7, "=", 1);
	      DEFINE_STRING(BGl_string1784z00zz__expander_recordz00,
		BgL_bgl_string1784za700za7za7_1811za7, "and", 3);
	      DEFINE_STRING(BGl_string1785z00zz__expander_recordz00,
		BgL_bgl_string1785za700za7za7_1812za7, "invalid constructor", 19);
	      DEFINE_STRING(BGl_string1786z00zz__expander_recordz00,
		BgL_bgl_string1786za700za7za7_1813za7, "invalid fields", 14);
	      DEFINE_STRING(BGl_string1788z00zz__expander_recordz00,
		BgL_bgl_string1788za700za7za7_1814za7, "struct-set!", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1757z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1759z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1761z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1763z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1765z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1767z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1769z00zz__expander_recordz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1771z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1773z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1775z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1777z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1779z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1781z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1783z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1787z00zz__expander_recordz00));
		     ADD_ROOT((void *) (&BGl_symbol1789z00zz__expander_recordz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_recordz00(long
		BgL_checksumz00_2087, char *BgL_fromz00_2088)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_recordz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_recordz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_recordz00();
					BGl_cnstzd2initzd2zz__expander_recordz00();
					BGl_importedzd2moduleszd2initz00zz__expander_recordz00();
					return BGl_methodzd2initzd2zz__expander_recordz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_recordz00(void)
	{
		{	/* Eval/expdrecord.scm 18 */
			BGl_symbol1757z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1758z00zz__expander_recordz00);
			BGl_symbol1759z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1760z00zz__expander_recordz00);
			BGl_symbol1761z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1762z00zz__expander_recordz00);
			BGl_symbol1763z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1764z00zz__expander_recordz00);
			BGl_symbol1765z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1766z00zz__expander_recordz00);
			BGl_symbol1767z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1768z00zz__expander_recordz00);
			BGl_symbol1769z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1770z00zz__expander_recordz00);
			BGl_symbol1771z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1772z00zz__expander_recordz00);
			BGl_symbol1773z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1774z00zz__expander_recordz00);
			BGl_symbol1775z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1776z00zz__expander_recordz00);
			BGl_symbol1777z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1778z00zz__expander_recordz00);
			BGl_symbol1779z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1780z00zz__expander_recordz00);
			BGl_symbol1781z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1782z00zz__expander_recordz00);
			BGl_symbol1783z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1784z00zz__expander_recordz00);
			BGl_symbol1787z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1788z00zz__expander_recordz00);
			return (BGl_symbol1789z00zz__expander_recordz00 =
				bstring_to_symbol(BGl_string1790z00zz__expander_recordz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_recordz00(void)
	{
		{	/* Eval/expdrecord.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* expand-define-record-type */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2definezd2recordzd2typezd2zz__expander_recordz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expdrecord.scm 59 */
			if (PAIRP(BgL_xz00_3))
				{	/* Eval/expdrecord.scm 60 */
					obj_t BgL_cdrzd2113zd2_1133;

					BgL_cdrzd2113zd2_1133 = CDR(((obj_t) BgL_xz00_3));
					if (PAIRP(BgL_cdrzd2113zd2_1133))
						{	/* Eval/expdrecord.scm 60 */
							obj_t BgL_cdrzd2119zd2_1135;

							BgL_cdrzd2119zd2_1135 = CDR(BgL_cdrzd2113zd2_1133);
							if (PAIRP(BgL_cdrzd2119zd2_1135))
								{	/* Eval/expdrecord.scm 60 */
									obj_t BgL_cdrzd2125zd2_1137;

									BgL_cdrzd2125zd2_1137 = CDR(BgL_cdrzd2119zd2_1135);
									if (PAIRP(BgL_cdrzd2125zd2_1137))
										{	/* Eval/expdrecord.scm 60 */
											obj_t BgL_arg1171z00_1139;
											obj_t BgL_arg1172z00_1140;
											obj_t BgL_arg1182z00_1141;
											obj_t BgL_arg1183z00_1142;

											BgL_arg1171z00_1139 = CAR(BgL_cdrzd2113zd2_1133);
											BgL_arg1172z00_1140 = CAR(BgL_cdrzd2119zd2_1135);
											BgL_arg1182z00_1141 = CAR(BgL_cdrzd2125zd2_1137);
											BgL_arg1183z00_1142 = CDR(BgL_cdrzd2125zd2_1137);
											{	/* Eval/expdrecord.scm 62 */
												obj_t BgL_arg1187z00_1745;

												{	/* Eval/expdrecord.scm 62 */
													obj_t BgL_arg1188z00_1746;

													BgL_arg1188z00_1746 =
														BGl_recordzd2ze3structz31zz__expander_recordz00
														(BgL_arg1171z00_1139, BgL_arg1172z00_1140,
														BgL_arg1182z00_1141, BgL_arg1183z00_1142);
													BgL_arg1187z00_1745 =
														BGL_PROCEDURE_CALL2(BgL_ez00_4, BgL_arg1188z00_1746,
														BgL_ez00_4);
												}
												BGL_TAIL return
													BGl_evepairifyz00zz__prognz00(BgL_arg1187z00_1745,
													BgL_xz00_3);
											}
										}
									else
										{	/* Eval/expdrecord.scm 60 */
											return
												BGl_expandzd2errorzd2zz__expander_recordz00(((bool_t)
													0), BGl_string1756z00zz__expander_recordz00,
												BgL_xz00_3);
										}
								}
							else
								{	/* Eval/expdrecord.scm 60 */
									return
										BGl_expandzd2errorzd2zz__expander_recordz00(((bool_t) 0),
										BGl_string1756z00zz__expander_recordz00, BgL_xz00_3);
								}
						}
					else
						{	/* Eval/expdrecord.scm 60 */
							return
								BGl_expandzd2errorzd2zz__expander_recordz00(((bool_t) 0),
								BGl_string1756z00zz__expander_recordz00, BgL_xz00_3);
						}
				}
			else
				{	/* Eval/expdrecord.scm 60 */
					return
						BGl_expandzd2errorzd2zz__expander_recordz00(((bool_t) 0),
						BGl_string1756z00zz__expander_recordz00, BgL_xz00_3);
				}
		}

	}



/* &expand-define-record-type */
	obj_t BGl_z62expandzd2definezd2recordzd2typezb0zz__expander_recordz00(obj_t
		BgL_envz00_2064, obj_t BgL_xz00_2065, obj_t BgL_ez00_2066)
	{
		{	/* Eval/expdrecord.scm 59 */
			return
				BGl_expandzd2definezd2recordzd2typezd2zz__expander_recordz00
				(BgL_xz00_2065, BgL_ez00_2066);
		}

	}



/* record->struct */
	obj_t BGl_recordzd2ze3structz31zz__expander_recordz00(obj_t BgL_namez00_5,
		obj_t BgL_constrz00_6, obj_t BgL_predicatez00_7, obj_t BgL_fieldsz00_8)
	{
		{	/* Eval/expdrecord.scm 69 */
			if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_fieldsz00_8))
				{	/* Eval/expdrecord.scm 71 */
					if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_constrz00_6))
						{	/* Eval/expdrecord.scm 76 */
							obj_t BgL_tmpz00_1147;
							obj_t BgL_valz00_1148;
							obj_t BgL_keyz00_1149;

							{	/* Eval/expdrecord.scm 76 */

								{	/* Eval/expdrecord.scm 76 */

									BgL_tmpz00_1147 = BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
								}
							}
							{	/* Eval/expdrecord.scm 77 */

								{	/* Eval/expdrecord.scm 77 */

									BgL_valz00_1148 = BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
								}
							}
							{	/* Eval/expdrecord.scm 78 */
								obj_t BgL_list1374z00_1280;

								{	/* Eval/expdrecord.scm 78 */
									obj_t BgL_arg1375z00_1281;

									{	/* Eval/expdrecord.scm 78 */
										obj_t BgL_arg1376z00_1282;

										BgL_arg1376z00_1282 =
											MAKE_YOUNG_PAIR(BGl_symbol1757z00zz__expander_recordz00,
											BNIL);
										BgL_arg1375z00_1281 =
											MAKE_YOUNG_PAIR(BgL_namez00_5, BgL_arg1376z00_1282);
									}
									BgL_list1374z00_1280 =
										MAKE_YOUNG_PAIR(BGl_symbol1759z00zz__expander_recordz00,
										BgL_arg1375z00_1281);
								}
								BgL_keyz00_1149 =
									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
									(BgL_list1374z00_1280);
							}
							{	/* Eval/expdrecord.scm 81 */
								obj_t BgL_arg1191z00_1150;

								{	/* Eval/expdrecord.scm 81 */
									obj_t BgL_arg1193z00_1151;
									obj_t BgL_arg1194z00_1152;

									{	/* Eval/expdrecord.scm 81 */
										obj_t BgL_arg1196z00_1153;

										{	/* Eval/expdrecord.scm 81 */
											obj_t BgL_arg1197z00_1154;

											{	/* Eval/expdrecord.scm 81 */
												obj_t BgL_arg1198z00_1155;

												{	/* Eval/expdrecord.scm 81 */
													obj_t BgL_arg1199z00_1156;

													{	/* Eval/expdrecord.scm 81 */
														obj_t BgL_arg1200z00_1157;
														obj_t BgL_arg1201z00_1158;

														{	/* Eval/expdrecord.scm 81 */
															obj_t BgL_arg1202z00_1159;

															{	/* Eval/expdrecord.scm 81 */
																obj_t BgL_arg1203z00_1160;

																{	/* Eval/expdrecord.scm 81 */
																	obj_t BgL_arg1206z00_1161;

																	{	/* Eval/expdrecord.scm 81 */
																		obj_t BgL_arg1208z00_1162;

																		{	/* Eval/expdrecord.scm 81 */
																			obj_t BgL_arg1209z00_1163;
																			obj_t BgL_arg1210z00_1164;

																			{	/* Eval/expdrecord.scm 81 */
																				obj_t BgL_arg1212z00_1165;

																				BgL_arg1212z00_1165 =
																					MAKE_YOUNG_PAIR(BgL_keyz00_1149,
																					BNIL);
																				BgL_arg1209z00_1163 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1761z00zz__expander_recordz00,
																					BgL_arg1212z00_1165);
																			}
																			{	/* Eval/expdrecord.scm 81 */
																				long BgL_arg1215z00_1166;
																				obj_t BgL_arg1216z00_1167;

																				BgL_arg1215z00_1166 =
																					bgl_list_length(BgL_fieldsz00_8);
																				BgL_arg1216z00_1167 =
																					MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																				BgL_arg1210z00_1164 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1215z00_1166),
																					BgL_arg1216z00_1167);
																			}
																			BgL_arg1208z00_1162 =
																				MAKE_YOUNG_PAIR(BgL_arg1209z00_1163,
																				BgL_arg1210z00_1164);
																		}
																		BgL_arg1206z00_1161 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1763z00zz__expander_recordz00,
																			BgL_arg1208z00_1162);
																	}
																	BgL_arg1203z00_1160 =
																		MAKE_YOUNG_PAIR(BgL_arg1206z00_1161, BNIL);
																}
																BgL_arg1202z00_1159 =
																	MAKE_YOUNG_PAIR(BgL_tmpz00_1147,
																	BgL_arg1203z00_1160);
															}
															BgL_arg1200z00_1157 =
																MAKE_YOUNG_PAIR(BgL_arg1202z00_1159, BNIL);
														}
														{	/* Eval/expdrecord.scm 86 */
															obj_t BgL_arg1218z00_1168;

															{	/* Eval/expdrecord.scm 86 */
																obj_t BgL_arg1219z00_1169;

																{	/* Eval/expdrecord.scm 86 */
																	obj_t BgL_arg1220z00_1170;
																	obj_t BgL_arg1221z00_1171;

																	BgL_arg1220z00_1170 =
																		BGl_loopze70ze7zz__expander_recordz00
																		(BgL_constrz00_6, BgL_tmpz00_1147,
																		BgL_fieldsz00_8, 0L);
																	BgL_arg1221z00_1171 =
																		MAKE_YOUNG_PAIR(BgL_tmpz00_1147, BNIL);
																	BgL_arg1219z00_1169 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_arg1220z00_1170, BgL_arg1221z00_1171);
																}
																BgL_arg1218z00_1168 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1765z00zz__expander_recordz00,
																	BgL_arg1219z00_1169);
															}
															BgL_arg1201z00_1158 =
																MAKE_YOUNG_PAIR(BgL_arg1218z00_1168, BNIL);
														}
														BgL_arg1199z00_1156 =
															MAKE_YOUNG_PAIR(BgL_arg1200z00_1157,
															BgL_arg1201z00_1158);
													}
													BgL_arg1198z00_1155 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1767z00zz__expander_recordz00,
														BgL_arg1199z00_1156);
												}
												BgL_arg1197z00_1154 =
													MAKE_YOUNG_PAIR(BgL_arg1198z00_1155, BNIL);
											}
											BgL_arg1196z00_1153 =
												MAKE_YOUNG_PAIR(BgL_constrz00_6, BgL_arg1197z00_1154);
										}
										BgL_arg1193z00_1151 =
											MAKE_YOUNG_PAIR(BGl_symbol1769z00zz__expander_recordz00,
											BgL_arg1196z00_1153);
									}
									{	/* Eval/expdrecord.scm 94 */
										obj_t BgL_arg1248z00_1193;
										obj_t BgL_arg1249z00_1194;

										{	/* Eval/expdrecord.scm 94 */
											obj_t BgL_arg1252z00_1195;

											{	/* Eval/expdrecord.scm 94 */
												obj_t BgL_arg1268z00_1196;
												obj_t BgL_arg1272z00_1197;

												{	/* Eval/expdrecord.scm 94 */
													obj_t BgL_arg1284z00_1198;

													BgL_arg1284z00_1198 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1771z00zz__expander_recordz00, BNIL);
													BgL_arg1268z00_1196 =
														MAKE_YOUNG_PAIR(BgL_predicatez00_7,
														BgL_arg1284z00_1198);
												}
												{	/* Eval/expdrecord.scm 95 */
													obj_t BgL_arg1304z00_1199;

													{	/* Eval/expdrecord.scm 95 */
														obj_t BgL_arg1305z00_1200;

														{	/* Eval/expdrecord.scm 95 */
															obj_t BgL_arg1306z00_1201;
															obj_t BgL_arg1307z00_1202;

															{	/* Eval/expdrecord.scm 95 */
																obj_t BgL_arg1308z00_1203;

																BgL_arg1308z00_1203 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1771z00zz__expander_recordz00,
																	BNIL);
																BgL_arg1306z00_1201 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1773z00zz__expander_recordz00,
																	BgL_arg1308z00_1203);
															}
															{	/* Eval/expdrecord.scm 96 */
																obj_t BgL_arg1309z00_1204;
																obj_t BgL_arg1310z00_1205;

																{	/* Eval/expdrecord.scm 96 */
																	obj_t BgL_arg1311z00_1206;

																	{	/* Eval/expdrecord.scm 96 */
																		obj_t BgL_arg1312z00_1207;
																		obj_t BgL_arg1314z00_1208;

																		{	/* Eval/expdrecord.scm 96 */
																			obj_t BgL_arg1315z00_1209;

																			BgL_arg1315z00_1209 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1771z00zz__expander_recordz00,
																				BNIL);
																			BgL_arg1312z00_1207 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1775z00zz__expander_recordz00,
																				BgL_arg1315z00_1209);
																		}
																		{	/* Eval/expdrecord.scm 96 */
																			obj_t BgL_arg1316z00_1210;

																			{	/* Eval/expdrecord.scm 96 */
																				obj_t BgL_arg1317z00_1211;

																				BgL_arg1317z00_1211 =
																					MAKE_YOUNG_PAIR(BgL_keyz00_1149,
																					BNIL);
																				BgL_arg1316z00_1210 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1761z00zz__expander_recordz00,
																					BgL_arg1317z00_1211);
																			}
																			BgL_arg1314z00_1208 =
																				MAKE_YOUNG_PAIR(BgL_arg1316z00_1210,
																				BNIL);
																		}
																		BgL_arg1311z00_1206 =
																			MAKE_YOUNG_PAIR(BgL_arg1312z00_1207,
																			BgL_arg1314z00_1208);
																	}
																	BgL_arg1309z00_1204 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol1777z00zz__expander_recordz00,
																		BgL_arg1311z00_1206);
																}
																{	/* Eval/expdrecord.scm 97 */
																	obj_t BgL_arg1318z00_1212;

																	{	/* Eval/expdrecord.scm 97 */
																		obj_t BgL_arg1319z00_1213;

																		{	/* Eval/expdrecord.scm 97 */
																			obj_t BgL_arg1320z00_1214;
																			obj_t BgL_arg1321z00_1215;

																			{	/* Eval/expdrecord.scm 97 */
																				obj_t BgL_arg1322z00_1216;

																				BgL_arg1322z00_1216 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1771z00zz__expander_recordz00,
																					BNIL);
																				BgL_arg1320z00_1214 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1779z00zz__expander_recordz00,
																					BgL_arg1322z00_1216);
																			}
																			{	/* Eval/expdrecord.scm 97 */
																				long BgL_arg1323z00_1217;

																				BgL_arg1323z00_1217 =
																					bgl_list_length(BgL_fieldsz00_8);
																				BgL_arg1321z00_1215 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg1323z00_1217), BNIL);
																			}
																			BgL_arg1319z00_1213 =
																				MAKE_YOUNG_PAIR(BgL_arg1320z00_1214,
																				BgL_arg1321z00_1215);
																		}
																		BgL_arg1318z00_1212 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1781z00zz__expander_recordz00,
																			BgL_arg1319z00_1213);
																	}
																	BgL_arg1310z00_1205 =
																		MAKE_YOUNG_PAIR(BgL_arg1318z00_1212, BNIL);
																}
																BgL_arg1307z00_1202 =
																	MAKE_YOUNG_PAIR(BgL_arg1309z00_1204,
																	BgL_arg1310z00_1205);
															}
															BgL_arg1305z00_1200 =
																MAKE_YOUNG_PAIR(BgL_arg1306z00_1201,
																BgL_arg1307z00_1202);
														}
														BgL_arg1304z00_1199 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1783z00zz__expander_recordz00,
															BgL_arg1305z00_1200);
													}
													BgL_arg1272z00_1197 =
														MAKE_YOUNG_PAIR(BgL_arg1304z00_1199, BNIL);
												}
												BgL_arg1252z00_1195 =
													MAKE_YOUNG_PAIR(BgL_arg1268z00_1196,
													BgL_arg1272z00_1197);
											}
											BgL_arg1248z00_1193 =
												MAKE_YOUNG_PAIR(BGl_symbol1769z00zz__expander_recordz00,
												BgL_arg1252z00_1195);
										}
										BgL_arg1249z00_1194 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BGl_loopze71ze7zz__expander_recordz00(BgL_valz00_1148,
												BgL_tmpz00_1147, BgL_fieldsz00_8, 0L), BNIL);
										BgL_arg1194z00_1152 =
											MAKE_YOUNG_PAIR(BgL_arg1248z00_1193, BgL_arg1249z00_1194);
									}
									BgL_arg1191z00_1150 =
										MAKE_YOUNG_PAIR(BgL_arg1193z00_1151, BgL_arg1194z00_1152);
								}
								return
									MAKE_YOUNG_PAIR(BGl_symbol1765z00zz__expander_recordz00,
									BgL_arg1191z00_1150);
							}
						}
					else
						{	/* Eval/expdrecord.scm 124 */
							obj_t BgL_locz00_1808;

							if (EPAIRP(BgL_constrz00_6))
								{	/* Eval/expdrecord.scm 124 */
									BgL_locz00_1808 = CER(((obj_t) BgL_constrz00_6));
								}
							else
								{	/* Eval/expdrecord.scm 124 */
									BgL_locz00_1808 = BFALSE;
								}
							{	/* Eval/expdrecord.scm 125 */
								bool_t BgL_test1823z00_2206;

								if (PAIRP(BgL_locz00_1808))
									{	/* Eval/expdrecord.scm 125 */
										bool_t BgL_test1825z00_2209;

										{	/* Eval/expdrecord.scm 125 */
											obj_t BgL_tmpz00_2210;

											BgL_tmpz00_2210 = CDR(BgL_locz00_1808);
											BgL_test1825z00_2209 = PAIRP(BgL_tmpz00_2210);
										}
										if (BgL_test1825z00_2209)
											{	/* Eval/expdrecord.scm 125 */
												obj_t BgL_tmpz00_2213;

												BgL_tmpz00_2213 = CDR(CDR(BgL_locz00_1808));
												BgL_test1823z00_2206 = PAIRP(BgL_tmpz00_2213);
											}
										else
											{	/* Eval/expdrecord.scm 125 */
												BgL_test1823z00_2206 = ((bool_t) 0);
											}
									}
								else
									{	/* Eval/expdrecord.scm 125 */
										BgL_test1823z00_2206 = ((bool_t) 0);
									}
								if (BgL_test1823z00_2206)
									{	/* Eval/expdrecord.scm 125 */
										return
											BGl_errorzf2locationzf2zz__errorz00(BFALSE,
											BGl_string1785z00zz__expander_recordz00, BgL_constrz00_6,
											CAR(CDR(BgL_locz00_1808)),
											CAR(CDR(CDR(BgL_locz00_1808))));
									}
								else
									{	/* Eval/expdrecord.scm 125 */
										return
											BGl_errorz00zz__errorz00(BFALSE,
											BGl_string1785z00zz__expander_recordz00, BgL_constrz00_6);
									}
							}
						}
				}
			else
				{	/* Eval/expdrecord.scm 124 */
					obj_t BgL_locz00_1833;

					if (EPAIRP(BgL_fieldsz00_8))
						{	/* Eval/expdrecord.scm 124 */
							BgL_locz00_1833 = CER(((obj_t) BgL_fieldsz00_8));
						}
					else
						{	/* Eval/expdrecord.scm 124 */
							BgL_locz00_1833 = BFALSE;
						}
					{	/* Eval/expdrecord.scm 125 */
						bool_t BgL_test1827z00_2228;

						if (PAIRP(BgL_locz00_1833))
							{	/* Eval/expdrecord.scm 125 */
								bool_t BgL_test1829z00_2231;

								{	/* Eval/expdrecord.scm 125 */
									obj_t BgL_tmpz00_2232;

									BgL_tmpz00_2232 = CDR(BgL_locz00_1833);
									BgL_test1829z00_2231 = PAIRP(BgL_tmpz00_2232);
								}
								if (BgL_test1829z00_2231)
									{	/* Eval/expdrecord.scm 125 */
										obj_t BgL_tmpz00_2235;

										BgL_tmpz00_2235 = CDR(CDR(BgL_locz00_1833));
										BgL_test1827z00_2228 = PAIRP(BgL_tmpz00_2235);
									}
								else
									{	/* Eval/expdrecord.scm 125 */
										BgL_test1827z00_2228 = ((bool_t) 0);
									}
							}
						else
							{	/* Eval/expdrecord.scm 125 */
								BgL_test1827z00_2228 = ((bool_t) 0);
							}
						if (BgL_test1827z00_2228)
							{	/* Eval/expdrecord.scm 125 */
								return
									BGl_errorzf2locationzf2zz__errorz00(BFALSE,
									BGl_string1786z00zz__expander_recordz00, BgL_fieldsz00_8,
									CAR(CDR(BgL_locz00_1833)), CAR(CDR(CDR(BgL_locz00_1833))));
							}
						else
							{	/* Eval/expdrecord.scm 125 */
								return
									BGl_errorz00zz__errorz00(BFALSE,
									BGl_string1786z00zz__expander_recordz00, BgL_fieldsz00_8);
							}
					}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__expander_recordz00(obj_t BgL_constrz00_2068,
		obj_t BgL_tmpz00_2067, obj_t BgL_fieldsz00_1173, long BgL_iz00_1174)
	{
		{	/* Eval/expdrecord.scm 83 */
		BGl_loopze70ze7zz__expander_recordz00:
			if (NULLP(BgL_fieldsz00_1173))
				{	/* Eval/expdrecord.scm 86 */
					return BNIL;
				}
			else
				{	/* Eval/expdrecord.scm 88 */
					bool_t BgL_test1831z00_2248;

					{	/* Eval/expdrecord.scm 88 */
						obj_t BgL_tmpz00_2249;

						{	/* Eval/expdrecord.scm 88 */
							obj_t BgL_auxz00_2250;

							{	/* Eval/expdrecord.scm 88 */
								obj_t BgL_pairz00_1750;

								BgL_pairz00_1750 = CAR(((obj_t) BgL_fieldsz00_1173));
								BgL_auxz00_2250 = CAR(BgL_pairz00_1750);
							}
							BgL_tmpz00_2249 =
								BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_auxz00_2250,
								CDR(((obj_t) BgL_constrz00_2068)));
						}
						BgL_test1831z00_2248 = CBOOL(BgL_tmpz00_2249);
					}
					if (BgL_test1831z00_2248)
						{	/* Eval/expdrecord.scm 89 */
							obj_t BgL_arg1228z00_1180;
							obj_t BgL_arg1229z00_1181;

							{	/* Eval/expdrecord.scm 89 */
								obj_t BgL_arg1230z00_1182;

								{	/* Eval/expdrecord.scm 89 */
									obj_t BgL_arg1231z00_1183;

									{	/* Eval/expdrecord.scm 89 */
										obj_t BgL_arg1232z00_1184;

										{	/* Eval/expdrecord.scm 89 */
											obj_t BgL_arg1233z00_1185;

											{	/* Eval/expdrecord.scm 89 */
												obj_t BgL_pairz00_1755;

												BgL_pairz00_1755 = CAR(((obj_t) BgL_fieldsz00_1173));
												BgL_arg1233z00_1185 = CAR(BgL_pairz00_1755);
											}
											BgL_arg1232z00_1184 =
												MAKE_YOUNG_PAIR(BgL_arg1233z00_1185, BNIL);
										}
										BgL_arg1231z00_1183 =
											MAKE_YOUNG_PAIR(BINT(BgL_iz00_1174), BgL_arg1232z00_1184);
									}
									BgL_arg1230z00_1182 =
										MAKE_YOUNG_PAIR(BgL_tmpz00_2067, BgL_arg1231z00_1183);
								}
								BgL_arg1228z00_1180 =
									MAKE_YOUNG_PAIR(BGl_symbol1787z00zz__expander_recordz00,
									BgL_arg1230z00_1182);
							}
							{	/* Eval/expdrecord.scm 90 */
								obj_t BgL_arg1234z00_1186;
								long BgL_arg1236z00_1187;

								BgL_arg1234z00_1186 = CDR(((obj_t) BgL_fieldsz00_1173));
								BgL_arg1236z00_1187 = (BgL_iz00_1174 + 1L);
								BgL_arg1229z00_1181 =
									BGl_loopze70ze7zz__expander_recordz00(BgL_constrz00_2068,
									BgL_tmpz00_2067, BgL_arg1234z00_1186, BgL_arg1236z00_1187);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1228z00_1180, BgL_arg1229z00_1181);
						}
					else
						{	/* Eval/expdrecord.scm 92 */
							obj_t BgL_arg1238z00_1188;
							long BgL_arg1239z00_1189;

							BgL_arg1238z00_1188 = CDR(((obj_t) BgL_fieldsz00_1173));
							BgL_arg1239z00_1189 = (BgL_iz00_1174 + 1L);
							{
								long BgL_iz00_2275;
								obj_t BgL_fieldsz00_2274;

								BgL_fieldsz00_2274 = BgL_arg1238z00_1188;
								BgL_iz00_2275 = BgL_arg1239z00_1189;
								BgL_iz00_1174 = BgL_iz00_2275;
								BgL_fieldsz00_1173 = BgL_fieldsz00_2274;
								goto BGl_loopze70ze7zz__expander_recordz00;
							}
						}
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zz__expander_recordz00(obj_t BgL_valz00_2070,
		obj_t BgL_tmpz00_2069, obj_t BgL_fieldsz00_1220, long BgL_iz00_1221)
	{
		{	/* Eval/expdrecord.scm 98 */
			if (NULLP(BgL_fieldsz00_1220))
				{	/* Eval/expdrecord.scm 100 */
					return BNIL;
				}
			else
				{	/* Eval/expdrecord.scm 102 */
					obj_t BgL_fieldz00_1224;

					BgL_fieldz00_1224 = CAR(((obj_t) BgL_fieldsz00_1220));
					{	/* Eval/expdrecord.scm 104 */
						bool_t BgL_test1833z00_2280;

						{	/* Eval/expdrecord.scm 104 */

							BgL_test1833z00_2280 = (bgl_list_length(BgL_fieldz00_1224) == 2L);
						}
						if (BgL_test1833z00_2280)
							{	/* Eval/expdrecord.scm 105 */
								obj_t BgL_arg1331z00_1229;
								obj_t BgL_arg1332z00_1230;

								{	/* Eval/expdrecord.scm 105 */
									obj_t BgL_arg1333z00_1231;

									{	/* Eval/expdrecord.scm 105 */
										obj_t BgL_arg1334z00_1232;
										obj_t BgL_arg1335z00_1233;

										{	/* Eval/expdrecord.scm 105 */
											obj_t BgL_arg1336z00_1234;
											obj_t BgL_arg1337z00_1235;

											{	/* Eval/expdrecord.scm 105 */
												obj_t BgL_pairz00_1766;

												BgL_pairz00_1766 = CDR(((obj_t) BgL_fieldz00_1224));
												BgL_arg1336z00_1234 = CAR(BgL_pairz00_1766);
											}
											BgL_arg1337z00_1235 =
												MAKE_YOUNG_PAIR(BgL_tmpz00_2069, BNIL);
											BgL_arg1334z00_1232 =
												MAKE_YOUNG_PAIR(BgL_arg1336z00_1234,
												BgL_arg1337z00_1235);
										}
										{	/* Eval/expdrecord.scm 106 */
											obj_t BgL_arg1338z00_1236;

											{	/* Eval/expdrecord.scm 106 */
												obj_t BgL_arg1339z00_1237;

												{	/* Eval/expdrecord.scm 106 */
													obj_t BgL_arg1340z00_1238;

													BgL_arg1340z00_1238 =
														MAKE_YOUNG_PAIR(BINT(BgL_iz00_1221), BNIL);
													BgL_arg1339z00_1237 =
														MAKE_YOUNG_PAIR(BgL_tmpz00_2069,
														BgL_arg1340z00_1238);
												}
												BgL_arg1338z00_1236 =
													MAKE_YOUNG_PAIR
													(BGl_symbol1789z00zz__expander_recordz00,
													BgL_arg1339z00_1237);
											}
											BgL_arg1335z00_1233 =
												MAKE_YOUNG_PAIR(BgL_arg1338z00_1236, BNIL);
										}
										BgL_arg1333z00_1231 =
											MAKE_YOUNG_PAIR(BgL_arg1334z00_1232, BgL_arg1335z00_1233);
									}
									BgL_arg1331z00_1229 =
										MAKE_YOUNG_PAIR(BGl_symbol1769z00zz__expander_recordz00,
										BgL_arg1333z00_1231);
								}
								{	/* Eval/expdrecord.scm 107 */
									obj_t BgL_arg1341z00_1239;
									long BgL_arg1342z00_1240;

									BgL_arg1341z00_1239 = CDR(((obj_t) BgL_fieldsz00_1220));
									BgL_arg1342z00_1240 = (BgL_iz00_1221 + 1L);
									BgL_arg1332z00_1230 =
										BGl_loopze71ze7zz__expander_recordz00(BgL_valz00_2070,
										BgL_tmpz00_2069, BgL_arg1341z00_1239, BgL_arg1342z00_1240);
								}
								return
									MAKE_YOUNG_PAIR(BgL_arg1331z00_1229, BgL_arg1332z00_1230);
							}
						else
							{	/* Eval/expdrecord.scm 108 */
								bool_t BgL_test1834z00_2300;

								{	/* Eval/expdrecord.scm 108 */

									BgL_test1834z00_2300 =
										(bgl_list_length(BgL_fieldz00_1224) == 3L);
								}
								if (BgL_test1834z00_2300)
									{	/* Eval/expdrecord.scm 110 */
										obj_t BgL_arg1346z00_1245;
										obj_t BgL_arg1347z00_1246;

										{	/* Eval/expdrecord.scm 110 */
											obj_t BgL_arg1348z00_1247;

											{	/* Eval/expdrecord.scm 110 */
												obj_t BgL_arg1349z00_1248;
												obj_t BgL_arg1350z00_1249;

												{	/* Eval/expdrecord.scm 110 */
													obj_t BgL_arg1351z00_1250;

													{	/* Eval/expdrecord.scm 110 */
														obj_t BgL_arg1352z00_1251;
														obj_t BgL_arg1354z00_1252;

														{	/* Eval/expdrecord.scm 110 */
															obj_t BgL_arg1356z00_1253;
															obj_t BgL_arg1357z00_1254;

															{	/* Eval/expdrecord.scm 110 */
																obj_t BgL_pairz00_1774;

																BgL_pairz00_1774 =
																	CDR(((obj_t) BgL_fieldz00_1224));
																BgL_arg1356z00_1253 = CAR(BgL_pairz00_1774);
															}
															BgL_arg1357z00_1254 =
																MAKE_YOUNG_PAIR(BgL_tmpz00_2069, BNIL);
															BgL_arg1352z00_1251 =
																MAKE_YOUNG_PAIR(BgL_arg1356z00_1253,
																BgL_arg1357z00_1254);
														}
														{	/* Eval/expdrecord.scm 111 */
															obj_t BgL_arg1358z00_1255;

															{	/* Eval/expdrecord.scm 111 */
																obj_t BgL_arg1359z00_1256;

																{	/* Eval/expdrecord.scm 111 */
																	obj_t BgL_arg1360z00_1257;

																	BgL_arg1360z00_1257 =
																		MAKE_YOUNG_PAIR(BINT(BgL_iz00_1221), BNIL);
																	BgL_arg1359z00_1256 =
																		MAKE_YOUNG_PAIR(BgL_tmpz00_2069,
																		BgL_arg1360z00_1257);
																}
																BgL_arg1358z00_1255 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1789z00zz__expander_recordz00,
																	BgL_arg1359z00_1256);
															}
															BgL_arg1354z00_1252 =
																MAKE_YOUNG_PAIR(BgL_arg1358z00_1255, BNIL);
														}
														BgL_arg1351z00_1250 =
															MAKE_YOUNG_PAIR(BgL_arg1352z00_1251,
															BgL_arg1354z00_1252);
													}
													BgL_arg1349z00_1248 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1769z00zz__expander_recordz00,
														BgL_arg1351z00_1250);
												}
												{	/* Eval/expdrecord.scm 112 */
													obj_t BgL_arg1361z00_1258;

													{	/* Eval/expdrecord.scm 112 */
														obj_t BgL_arg1362z00_1259;

														{	/* Eval/expdrecord.scm 112 */
															obj_t BgL_arg1363z00_1260;
															obj_t BgL_arg1364z00_1261;

															{	/* Eval/expdrecord.scm 112 */
																obj_t BgL_arg1365z00_1262;
																obj_t BgL_arg1366z00_1263;

																{	/* Eval/expdrecord.scm 112 */
																	obj_t BgL_pairz00_1780;

																	{	/* Eval/expdrecord.scm 112 */
																		obj_t BgL_pairz00_1779;

																		BgL_pairz00_1779 =
																			CDR(((obj_t) BgL_fieldz00_1224));
																		BgL_pairz00_1780 = CDR(BgL_pairz00_1779);
																	}
																	BgL_arg1365z00_1262 = CAR(BgL_pairz00_1780);
																}
																{	/* Eval/expdrecord.scm 112 */
																	obj_t BgL_arg1367z00_1264;

																	BgL_arg1367z00_1264 =
																		MAKE_YOUNG_PAIR(BgL_valz00_2070, BNIL);
																	BgL_arg1366z00_1263 =
																		MAKE_YOUNG_PAIR(BgL_tmpz00_2069,
																		BgL_arg1367z00_1264);
																}
																BgL_arg1363z00_1260 =
																	MAKE_YOUNG_PAIR(BgL_arg1365z00_1262,
																	BgL_arg1366z00_1263);
															}
															{	/* Eval/expdrecord.scm 113 */
																obj_t BgL_arg1368z00_1265;

																{	/* Eval/expdrecord.scm 113 */
																	obj_t BgL_arg1369z00_1266;

																	{	/* Eval/expdrecord.scm 113 */
																		obj_t BgL_arg1370z00_1267;

																		{	/* Eval/expdrecord.scm 113 */
																			obj_t BgL_arg1371z00_1268;

																			BgL_arg1371z00_1268 =
																				MAKE_YOUNG_PAIR(BgL_valz00_2070, BNIL);
																			BgL_arg1370z00_1267 =
																				MAKE_YOUNG_PAIR(BINT(BgL_iz00_1221),
																				BgL_arg1371z00_1268);
																		}
																		BgL_arg1369z00_1266 =
																			MAKE_YOUNG_PAIR(BgL_tmpz00_2069,
																			BgL_arg1370z00_1267);
																	}
																	BgL_arg1368z00_1265 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol1787z00zz__expander_recordz00,
																		BgL_arg1369z00_1266);
																}
																BgL_arg1364z00_1261 =
																	MAKE_YOUNG_PAIR(BgL_arg1368z00_1265, BNIL);
															}
															BgL_arg1362z00_1259 =
																MAKE_YOUNG_PAIR(BgL_arg1363z00_1260,
																BgL_arg1364z00_1261);
														}
														BgL_arg1361z00_1258 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1769z00zz__expander_recordz00,
															BgL_arg1362z00_1259);
													}
													BgL_arg1350z00_1249 =
														MAKE_YOUNG_PAIR(BgL_arg1361z00_1258, BNIL);
												}
												BgL_arg1348z00_1247 =
													MAKE_YOUNG_PAIR(BgL_arg1349z00_1248,
													BgL_arg1350z00_1249);
											}
											BgL_arg1346z00_1245 =
												MAKE_YOUNG_PAIR(BGl_symbol1765z00zz__expander_recordz00,
												BgL_arg1348z00_1247);
										}
										{	/* Eval/expdrecord.scm 114 */
											obj_t BgL_arg1372z00_1269;
											long BgL_arg1373z00_1270;

											BgL_arg1372z00_1269 = CDR(((obj_t) BgL_fieldsz00_1220));
											BgL_arg1373z00_1270 = (BgL_iz00_1221 + 1L);
											BgL_arg1347z00_1246 =
												BGl_loopze71ze7zz__expander_recordz00(BgL_valz00_2070,
												BgL_tmpz00_2069, BgL_arg1372z00_1269,
												BgL_arg1373z00_1270);
										}
										return
											MAKE_YOUNG_PAIR(BgL_arg1346z00_1245, BgL_arg1347z00_1246);
									}
								else
									{	/* Eval/expdrecord.scm 124 */
										obj_t BgL_locz00_1783;

										if (EPAIRP(BgL_fieldz00_1224))
											{	/* Eval/expdrecord.scm 124 */
												BgL_locz00_1783 = CER(((obj_t) BgL_fieldz00_1224));
											}
										else
											{	/* Eval/expdrecord.scm 124 */
												BgL_locz00_1783 = BFALSE;
											}
										{	/* Eval/expdrecord.scm 125 */
											bool_t BgL_test1836z00_2342;

											if (PAIRP(BgL_locz00_1783))
												{	/* Eval/expdrecord.scm 125 */
													bool_t BgL_test1838z00_2345;

													{	/* Eval/expdrecord.scm 125 */
														obj_t BgL_tmpz00_2346;

														BgL_tmpz00_2346 = CDR(BgL_locz00_1783);
														BgL_test1838z00_2345 = PAIRP(BgL_tmpz00_2346);
													}
													if (BgL_test1838z00_2345)
														{	/* Eval/expdrecord.scm 125 */
															obj_t BgL_tmpz00_2349;

															BgL_tmpz00_2349 = CDR(CDR(BgL_locz00_1783));
															BgL_test1836z00_2342 = PAIRP(BgL_tmpz00_2349);
														}
													else
														{	/* Eval/expdrecord.scm 125 */
															BgL_test1836z00_2342 = ((bool_t) 0);
														}
												}
											else
												{	/* Eval/expdrecord.scm 125 */
													BgL_test1836z00_2342 = ((bool_t) 0);
												}
											if (BgL_test1836z00_2342)
												{	/* Eval/expdrecord.scm 125 */
													return
														BGl_errorzf2locationzf2zz__errorz00(BFALSE,
														BGl_string1791z00zz__expander_recordz00,
														BgL_fieldz00_1224, CAR(CDR(BgL_locz00_1783)),
														CAR(CDR(CDR(BgL_locz00_1783))));
												}
											else
												{	/* Eval/expdrecord.scm 125 */
													return
														BGl_errorz00zz__errorz00(BFALSE,
														BGl_string1791z00zz__expander_recordz00,
														BgL_fieldz00_1224);
												}
										}
									}
							}
					}
				}
		}

	}



/* expand-error */
	obj_t BGl_expandzd2errorzd2zz__expander_recordz00(bool_t BgL_pz00_9,
		obj_t BgL_mz00_10, obj_t BgL_xz00_11)
	{
		{	/* Eval/expdrecord.scm 123 */
			{	/* Eval/expdrecord.scm 124 */
				obj_t BgL_locz00_1283;

				if (EPAIRP(BgL_xz00_11))
					{	/* Eval/expdrecord.scm 124 */
						BgL_locz00_1283 = CER(((obj_t) BgL_xz00_11));
					}
				else
					{	/* Eval/expdrecord.scm 124 */
						BgL_locz00_1283 = BFALSE;
					}
				{	/* Eval/expdrecord.scm 125 */
					bool_t BgL_test1840z00_2364;

					if (PAIRP(BgL_locz00_1283))
						{	/* Eval/expdrecord.scm 125 */
							bool_t BgL_test1842z00_2367;

							{	/* Eval/expdrecord.scm 125 */
								obj_t BgL_tmpz00_2368;

								BgL_tmpz00_2368 = CDR(BgL_locz00_1283);
								BgL_test1842z00_2367 = PAIRP(BgL_tmpz00_2368);
							}
							if (BgL_test1842z00_2367)
								{	/* Eval/expdrecord.scm 125 */
									obj_t BgL_tmpz00_2371;

									BgL_tmpz00_2371 = CDR(CDR(BgL_locz00_1283));
									BgL_test1840z00_2364 = PAIRP(BgL_tmpz00_2371);
								}
							else
								{	/* Eval/expdrecord.scm 125 */
									BgL_test1840z00_2364 = ((bool_t) 0);
								}
						}
					else
						{	/* Eval/expdrecord.scm 125 */
							BgL_test1840z00_2364 = ((bool_t) 0);
						}
					if (BgL_test1840z00_2364)
						{	/* Eval/expdrecord.scm 125 */
							return
								BGl_errorzf2locationzf2zz__errorz00(BBOOL(BgL_pz00_9),
								BgL_mz00_10, BgL_xz00_11, CAR(CDR(BgL_locz00_1283)),
								CAR(CDR(CDR(BgL_locz00_1283))));
						}
					else
						{	/* Eval/expdrecord.scm 125 */
							return
								BGl_errorz00zz__errorz00(BBOOL(BgL_pz00_9), BgL_mz00_10,
								BgL_xz00_11);
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_recordz00(void)
	{
		{	/* Eval/expdrecord.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_recordz00(void)
	{
		{	/* Eval/expdrecord.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_recordz00(void)
	{
		{	/* Eval/expdrecord.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_recordz00(void)
	{
		{	/* Eval/expdrecord.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(515155867L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
			return BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1792z00zz__expander_recordz00));
		}

	}

#ifdef __cplusplus
}
#endif
