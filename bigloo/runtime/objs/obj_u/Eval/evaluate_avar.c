/*===========================================================================*/
/*   (Eval/evaluate_avar.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evaluate_avar.scm -indent -o objs/obj_u/Eval/evaluate_avar.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVALUATE_AVAR_TYPE_DEFINITIONS
#define BGL___EVALUATE_AVAR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ev_exprz00_bgl
	{
		header_t header;
		obj_t widening;
	}                 *BgL_ev_exprz00_bglt;

	typedef struct BgL_ev_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_effz00;
		obj_t BgL_typez00;
	}                *BgL_ev_varz00_bglt;

	typedef struct BgL_ev_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_pz00;
		struct BgL_ev_exprz00_bgl *BgL_tz00;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}               *BgL_ev_ifz00_bglt;

	typedef struct BgL_ev_listz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                 *BgL_ev_listz00_bglt;

	typedef struct BgL_ev_prog2z00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_e1z00;
		struct BgL_ev_exprz00_bgl *BgL_e2z00;
	}                  *BgL_ev_prog2z00_bglt;

	typedef struct BgL_ev_hookz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_hookz00_bglt;

	typedef struct BgL_ev_setlocalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_varz00_bgl *BgL_vz00;
	}                     *BgL_ev_setlocalz00_bglt;

	typedef struct BgL_ev_bindzd2exitzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_varz00_bgl *BgL_varz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                        *BgL_ev_bindzd2exitzd2_bglt;

	typedef struct BgL_ev_unwindzd2protectzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                             *BgL_ev_unwindzd2protectzd2_bglt;

	typedef struct BgL_ev_withzd2handlerzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_handlerz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                           *BgL_ev_withzd2handlerzd2_bglt;

	typedef struct BgL_ev_synchroniza7eza7_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_mutexz00;
		struct BgL_ev_exprz00_bgl *BgL_prelockz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                          *BgL_ev_synchroniza7eza7_bglt;

	typedef struct BgL_ev_binderz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_binderz00_bglt;

	typedef struct BgL_ev_letz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_letz00_bglt;

	typedef struct BgL_ev_letza2za2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_letza2za2_bglt;

	typedef struct BgL_ev_letrecz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_letrecz00_bglt;

	typedef struct BgL_ev_labelsz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		obj_t BgL_envz00;
		obj_t BgL_stkz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_labelsz00_bglt;

	typedef struct BgL_ev_gotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_varz00_bgl *BgL_labelz00;
		struct BgL_ev_labelsz00_bgl *BgL_labelsz00;
		obj_t BgL_argsz00;
	}                 *BgL_ev_gotoz00_bglt;

	typedef struct BgL_ev_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_tailzf3zf3;
	}                *BgL_ev_appz00_bglt;

	typedef struct BgL_ev_absz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_wherez00;
		obj_t BgL_arityz00;
		obj_t BgL_varsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		int BgL_siza7eza7;
		obj_t BgL_bindz00;
		obj_t BgL_freez00;
		obj_t BgL_innerz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_absz00_bglt;


#endif													// BGL___EVALUATE_AVAR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62avarzd2ev_if1223zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_global1219zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evaluate_avarz00 = BUNSPEC;
	extern obj_t BGl_ev_gotoz00zz__evaluate_typesz00;
	static obj_t BGl_z62avarzd2ev_unwindzd2prote1235z62zz__evaluate_avarz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_labels1247zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_avarz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	static obj_t BGl_funionze70ze7zz__evaluate_avarz00(obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_ev_globalz00zz__evaluate_typesz00;
	extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62avarzd2ev_hook1229zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_listz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_letrecz00zz__evaluate_typesz00;
	static obj_t BGl_z62avarzd2ev_list1225zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_app1251zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1876z00zz__evaluate_avarz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__evaluate_avarz00(void);
	static obj_t BGl_symbol1878z00zz__evaluate_avarz00 = BUNSPEC;
	extern obj_t BGl_ev_littz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_letz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_varz00zz__evaluate_typesz00;
	static obj_t BGl_symbol1888z00zz__evaluate_avarz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__evaluate_avarz00(void);
	static obj_t BGl_genericzd2initzd2zz__evaluate_avarz00(void);
	extern obj_t BGl_ev_prog2z00zz__evaluate_typesz00;
	static obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_avarz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evaluate_avarz00(void);
	static obj_t BGl_objectzd2initzd2zz__evaluate_avarz00(void);
	extern obj_t BGl_ev_bindzd2exitzd2zz__evaluate_typesz00;
	extern obj_t BGl_ev_withzd2handlerzd2zz__evaluate_typesz00;
	static obj_t BGl_z62avarz62zz__evaluate_avarz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62avarzd2ev_goto1249zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_synchroniza7e1239z17zz__evaluate_avarz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_letza21243z12zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_litt1221zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00;
	static obj_t BGl_z62avarzd2ev_bindzd2exit1233z62zz__evaluate_avarz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_avarz00zz__evaluate_avarz00(BgL_ev_exprz00_bglt, obj_t,
		obj_t);
	static bool_t BGl_za2za2callzf2cczd2compliantza2za2z20zz__evaluate_avarz00;
	static obj_t BGl_z62avarzd2ev_abs1253zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avar1214z62zz__evaluate_avarz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_exprz00zz__evaluate_typesz00;
	static obj_t BGl_z62checkzd2varzb0zz__evaluate_avarz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_appz00zz__evaluate_typesz00;
	extern bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_ev_letza2za2zz__evaluate_typesz00;
	extern obj_t BGl_ev_labelsz00zz__evaluate_typesz00;
	static obj_t BGl_appendzd221011zd2zz__evaluate_avarz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__evaluate_avarz00(void);
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(BgL_ev_absz00_bglt,
		obj_t);
	static obj_t BGl_z62avarzd2ev_var1217zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_prog21227zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_absz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_synchroniza7eza7zz__evaluate_typesz00;
	static obj_t BGl_z62avarzd2ev_letrec1245zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62avarzd2ev_withzd2handler1237z62zz__evaluate_avarz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_diffz00zz__evaluate_avarz00(obj_t, obj_t);
	static obj_t BGl_z62analysezd2varszb0zz__evaluate_avarz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_checkzd2varzd2zz__evaluate_avarz00(BgL_ev_varz00_bglt, obj_t,
		BgL_ev_absz00_bglt);
	static obj_t BGl_z62avarzd2ev_let1241zb0zz__evaluate_avarz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ev_ifz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_setlocalz00zz__evaluate_typesz00;
	BGL_EXPORTED_DECL obj_t
		BGl_analysezd2varszd2zz__evaluate_avarz00(BgL_ev_exprz00_bglt);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_ev_hookz00zz__evaluate_typesz00;
	static obj_t BGl_z62avarzd2ev_setlocal1231zb0zz__evaluate_avarz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_avarzd2envzd2zz__evaluate_avarz00,
		BgL_bgl_za762avarza762za7za7__ev1913z00, BGl_z62avarz62zz__evaluate_avarz00,
		0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1877z00zz__evaluate_avarz00,
		BgL_bgl_string1877za700za7za7_1914za7, "never", 5);
	      DEFINE_STRING(BGl_string1879z00zz__evaluate_avarz00,
		BgL_bgl_string1879za700za7za7_1915za7, "fake", 4);
	      DEFINE_STRING(BGl_string1880z00zz__evaluate_avarz00,
		BgL_bgl_string1880za700za7za7_1916za7,
		"/tmp/bigloo/runtime/Eval/evaluate_avar.scm", 42);
	      DEFINE_STRING(BGl_string1881z00zz__evaluate_avarz00,
		BgL_bgl_string1881za700za7za7_1917za7, "&analyse-vars", 13);
	      DEFINE_STRING(BGl_string1882z00zz__evaluate_avarz00,
		BgL_bgl_string1882za700za7za7_1918za7, "ev_expr", 7);
	      DEFINE_STRING(BGl_string1883z00zz__evaluate_avarz00,
		BgL_bgl_string1883za700za7za7_1919za7, "&check-var", 10);
	      DEFINE_STRING(BGl_string1884z00zz__evaluate_avarz00,
		BgL_bgl_string1884za700za7za7_1920za7, "ev_var", 6);
	      DEFINE_STRING(BGl_string1885z00zz__evaluate_avarz00,
		BgL_bgl_string1885za700za7za7_1921za7, "ev_abs", 6);
	      DEFINE_STRING(BGl_string1887z00zz__evaluate_avarz00,
		BgL_bgl_string1887za700za7za7_1922za7, "avar1214", 8);
	      DEFINE_STRING(BGl_string1889z00zz__evaluate_avarz00,
		BgL_bgl_string1889za700za7za7_1923za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1890z00zz__evaluate_avarz00,
		BgL_bgl_string1890za700za7za7_1924za7, "&avar", 5);
	      DEFINE_STRING(BGl_string1892z00zz__evaluate_avarz00,
		BgL_bgl_string1892za700za7za7_1925za7, "avar", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zz__evaluate_avarz00,
		BgL_bgl_za762avar1214za762za7za71926z00,
		BGl_z62avar1214z62zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_var1927z00,
		BGl_z62avarzd2ev_var1217zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_glo1928z00,
		BGl_z62avarzd2ev_global1219zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1894z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_lit1929z00,
		BGl_z62avarzd2ev_litt1221zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1895z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_if11930z00,
		BGl_z62avarzd2ev_if1223zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_lis1931z00,
		BGl_z62avarzd2ev_list1225zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_pro1932z00,
		BGl_z62avarzd2ev_prog21227zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_hoo1933z00,
		BGl_z62avarzd2ev_hook1229zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_set1934z00,
		BGl_z62avarzd2ev_setlocal1231zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_analysezd2varszd2envz00zz__evaluate_avarz00,
		BgL_bgl_za762analyseza7d2var1935z00,
		BGl_z62analysezd2varszb0zz__evaluate_avarz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1900z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_bin1936z00,
		BGl_z62avarzd2ev_bindzd2exit1233z62zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1901z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_unw1937z00,
		BGl_z62avarzd2ev_unwindzd2prote1235z62zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1902z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_wit1938z00,
		BGl_z62avarzd2ev_withzd2handler1237z62zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1903z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_syn1939z00,
		BGl_z62avarzd2ev_synchroniza7e1239z17zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_let1940z00,
		BGl_z62avarzd2ev_let1241zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1911z00zz__evaluate_avarz00,
		BgL_bgl_string1911za700za7za7_1941za7, "__evaluate_avar", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1905z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_let1942z00,
		BGl_z62avarzd2ev_letza21243z12zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_let1943z00,
		BGl_z62avarzd2ev_letrec1245zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_lab1944z00,
		BGl_z62avarzd2ev_labels1247zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1908z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_got1945z00,
		BGl_z62avarzd2ev_goto1249zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_app1946z00,
		BGl_z62avarzd2ev_app1251zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_checkzd2varzd2envz00zz__evaluate_avarz00,
		BgL_bgl_za762checkza7d2varza7b1947za7,
		BGl_z62checkzd2varzb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1910z00zz__evaluate_avarz00,
		BgL_bgl_za762avarza7d2ev_abs1948z00,
		BGl_z62avarzd2ev_abs1253zb0zz__evaluate_avarz00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__evaluate_avarz00));
		     ADD_ROOT((void *) (&BGl_symbol1876z00zz__evaluate_avarz00));
		     ADD_ROOT((void *) (&BGl_symbol1878z00zz__evaluate_avarz00));
		     ADD_ROOT((void *) (&BGl_symbol1888z00zz__evaluate_avarz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_avarz00(long
		BgL_checksumz00_3043, char *BgL_fromz00_3044)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evaluate_avarz00))
				{
					BGl_requirezd2initializa7ationz75zz__evaluate_avarz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evaluate_avarz00();
					BGl_cnstzd2initzd2zz__evaluate_avarz00();
					BGl_importedzd2moduleszd2initz00zz__evaluate_avarz00();
					BGl_genericzd2initzd2zz__evaluate_avarz00();
					BGl_methodzd2initzd2zz__evaluate_avarz00();
					return BGl_toplevelzd2initzd2zz__evaluate_avarz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			BGl_symbol1876z00zz__evaluate_avarz00 =
				bstring_to_symbol(BGl_string1877z00zz__evaluate_avarz00);
			BGl_symbol1878z00zz__evaluate_avarz00 =
				bstring_to_symbol(BGl_string1879z00zz__evaluate_avarz00);
			return (BGl_symbol1888z00zz__evaluate_avarz00 =
				bstring_to_symbol(BGl_string1887z00zz__evaluate_avarz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			BGl_za2za2callzf2cczd2compliantza2za2z20zz__evaluate_avarz00 =
				((bool_t) 0);
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__evaluate_avarz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1198;

				BgL_headz00_1198 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2057;
					obj_t BgL_tailz00_2058;

					BgL_prevz00_2057 = BgL_headz00_1198;
					BgL_tailz00_2058 = BgL_l1z00_1;
				BgL_loopz00_2056:
					if (PAIRP(BgL_tailz00_2058))
						{
							obj_t BgL_newzd2prevzd2_2064;

							BgL_newzd2prevzd2_2064 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2058), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2057, BgL_newzd2prevzd2_2064);
							{
								obj_t BgL_tailz00_3065;
								obj_t BgL_prevz00_3064;

								BgL_prevz00_3064 = BgL_newzd2prevzd2_2064;
								BgL_tailz00_3065 = CDR(BgL_tailz00_2058);
								BgL_tailz00_2058 = BgL_tailz00_3065;
								BgL_prevz00_2057 = BgL_prevz00_3064;
								goto BgL_loopz00_2056;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1198);
			}
		}

	}



/* diff */
	obj_t BGl_diffz00zz__evaluate_avarz00(obj_t BgL_l1z00_11, obj_t BgL_l2z00_12)
	{
		{	/* Eval/evaluate_avar.scm 90 */
		BGl_diffz00zz__evaluate_avarz00:
			if (NULLP(BgL_l1z00_11))
				{	/* Eval/evaluate_avar.scm 91 */
					return BNIL;
				}
			else
				{	/* Eval/evaluate_avar.scm 93 */
					obj_t BgL_xz00_1251;

					BgL_xz00_1251 = CAR(((obj_t) BgL_l1z00_11));
					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_xz00_1251,
								BgL_l2z00_12)))
						{	/* Eval/evaluate_avar.scm 95 */
							obj_t BgL_arg1357z00_1253;

							BgL_arg1357z00_1253 = CDR(((obj_t) BgL_l1z00_11));
							{
								obj_t BgL_l1z00_3077;

								BgL_l1z00_3077 = BgL_arg1357z00_1253;
								BgL_l1z00_11 = BgL_l1z00_3077;
								goto BGl_diffz00zz__evaluate_avarz00;
							}
						}
					else
						{	/* Eval/evaluate_avar.scm 96 */
							obj_t BgL_arg1358z00_1254;

							{	/* Eval/evaluate_avar.scm 96 */
								obj_t BgL_arg1359z00_1255;

								BgL_arg1359z00_1255 = CDR(((obj_t) BgL_l1z00_11));
								BgL_arg1358z00_1254 =
									BGl_diffz00zz__evaluate_avarz00(BgL_arg1359z00_1255,
									BgL_l2z00_12);
							}
							return MAKE_YOUNG_PAIR(BgL_xz00_1251, BgL_arg1358z00_1254);
						}
				}
		}

	}



/* analyse-vars */
	BGL_EXPORTED_DEF obj_t
		BGl_analysezd2varszd2zz__evaluate_avarz00(BgL_ev_exprz00_bglt BgL_ez00_14)
	{
		{	/* Eval/evaluate_avar.scm 112 */
			{	/* Eval/evaluate_avar.scm 113 */
				BgL_ev_absz00_bglt BgL_fakez00_1258;

				{	/* Eval/evaluate_avar.scm 113 */
					BgL_ev_absz00_bglt BgL_new1068z00_1259;

					{	/* Eval/evaluate_avar.scm 113 */
						BgL_ev_absz00_bglt BgL_new1067z00_1260;

						BgL_new1067z00_1260 =
							((BgL_ev_absz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_ev_absz00_bgl))));
						{	/* Eval/evaluate_avar.scm 113 */
							long BgL_arg1361z00_1261;

							BgL_arg1361z00_1261 =
								BGL_CLASS_NUM(BGl_ev_absz00zz__evaluate_typesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1067z00_1260),
								BgL_arg1361z00_1261);
						}
						BgL_new1068z00_1259 = BgL_new1067z00_1260;
					}
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_locz00) =
						((obj_t) BGl_symbol1876z00zz__evaluate_avarz00), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_wherez00) =
						((obj_t) BGl_symbol1878z00zz__evaluate_avarz00), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_arityz00) =
						((obj_t) BINT(0L)), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_varsz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_bodyz00) =
						((BgL_ev_exprz00_bglt) BgL_ez00_14), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->
							BgL_siza7eza7) = ((int) (int) (0L)), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_bindz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_freez00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_innerz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_ev_absz00_bglt) COBJECT(BgL_new1068z00_1259))->BgL_boxesz00) =
						((obj_t) BNIL), BUNSPEC);
					BgL_fakez00_1258 = BgL_new1068z00_1259;
				}
				return
					BGl_avarz00zz__evaluate_avarz00(BgL_ez00_14, BNIL,
					((obj_t) BgL_fakez00_1258));
			}
		}

	}



/* &analyse-vars */
	obj_t BGl_z62analysezd2varszb0zz__evaluate_avarz00(obj_t BgL_envz00_2734,
		obj_t BgL_ez00_2735)
	{
		{	/* Eval/evaluate_avar.scm 112 */
			{	/* Eval/evaluate_avar.scm 113 */
				BgL_ev_exprz00_bglt BgL_auxz00_3100;

				if (BGl_isazf3zf3zz__objectz00(BgL_ez00_2735,
						BGl_ev_exprz00zz__evaluate_typesz00))
					{	/* Eval/evaluate_avar.scm 113 */
						BgL_auxz00_3100 = ((BgL_ev_exprz00_bglt) BgL_ez00_2735);
					}
				else
					{
						obj_t BgL_auxz00_3104;

						BgL_auxz00_3104 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1880z00zz__evaluate_avarz00, BINT(2941L),
							BGl_string1881z00zz__evaluate_avarz00,
							BGl_string1882z00zz__evaluate_avarz00, BgL_ez00_2735);
						FAILURE(BgL_auxz00_3104, BFALSE, BFALSE);
					}
				return BGl_analysezd2varszd2zz__evaluate_avarz00(BgL_auxz00_3100);
			}
		}

	}



/* check-var */
	BGL_EXPORTED_DEF obj_t
		BGl_checkzd2varzd2zz__evaluate_avarz00(BgL_ev_varz00_bglt BgL_varz00_15,
		obj_t BgL_localz00_16, BgL_ev_absz00_bglt BgL_absz00_17)
	{
		{	/* Eval/evaluate_avar.scm 117 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_varz00_15), BgL_localz00_16)))
				{	/* Eval/evaluate_avar.scm 118 */
					return BFALSE;
				}
			else
				{	/* Eval/evaluate_avar.scm 118 */
					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
								((obj_t) BgL_varz00_15),
								(((BgL_ev_absz00_bglt) COBJECT(BgL_absz00_17))->BgL_freez00))))
						{	/* Eval/evaluate_avar.scm 120 */
							return BFALSE;
						}
					else
						{
							obj_t BgL_auxz00_3118;

							{	/* Eval/evaluate_avar.scm 120 */
								obj_t BgL_arg1365z00_2095;

								BgL_arg1365z00_2095 =
									(((BgL_ev_absz00_bglt) COBJECT(BgL_absz00_17))->BgL_freez00);
								BgL_auxz00_3118 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_varz00_15), BgL_arg1365z00_2095);
							}
							return
								((((BgL_ev_absz00_bglt) COBJECT(BgL_absz00_17))->BgL_freez00) =
								((obj_t) BgL_auxz00_3118), BUNSPEC);
						}
				}
		}

	}



/* &check-var */
	obj_t BGl_z62checkzd2varzb0zz__evaluate_avarz00(obj_t BgL_envz00_2736,
		obj_t BgL_varz00_2737, obj_t BgL_localz00_2738, obj_t BgL_absz00_2739)
	{
		{	/* Eval/evaluate_avar.scm 117 */
			{	/* Eval/evaluate_avar.scm 118 */
				BgL_ev_absz00_bglt BgL_auxz00_3131;
				BgL_ev_varz00_bglt BgL_auxz00_3123;

				if (BGl_isazf3zf3zz__objectz00(BgL_absz00_2739,
						BGl_ev_absz00zz__evaluate_typesz00))
					{	/* Eval/evaluate_avar.scm 118 */
						BgL_auxz00_3131 = ((BgL_ev_absz00_bglt) BgL_absz00_2739);
					}
				else
					{
						obj_t BgL_auxz00_3135;

						BgL_auxz00_3135 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1880z00zz__evaluate_avarz00, BINT(3159L),
							BGl_string1883z00zz__evaluate_avarz00,
							BGl_string1885z00zz__evaluate_avarz00, BgL_absz00_2739);
						FAILURE(BgL_auxz00_3135, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_varz00_2737,
						BGl_ev_varz00zz__evaluate_typesz00))
					{	/* Eval/evaluate_avar.scm 118 */
						BgL_auxz00_3123 = ((BgL_ev_varz00_bglt) BgL_varz00_2737);
					}
				else
					{
						obj_t BgL_auxz00_3127;

						BgL_auxz00_3127 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1880z00zz__evaluate_avarz00, BINT(3159L),
							BGl_string1883z00zz__evaluate_avarz00,
							BGl_string1884z00zz__evaluate_avarz00, BgL_varz00_2737);
						FAILURE(BgL_auxz00_3127, BFALSE, BFALSE);
					}
				return
					BGl_checkzd2varzd2zz__evaluate_avarz00(BgL_auxz00_3123,
					BgL_localz00_2738, BgL_auxz00_3131);
			}
		}

	}



/* bind-and-reset-effect */
	obj_t
		BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(BgL_ev_absz00_bglt
		BgL_absz00_75, obj_t BgL_varsz00_76)
	{
		{	/* Eval/evaluate_avar.scm 226 */
			{	/* Eval/evaluate_avar.scm 232 */
				obj_t BgL_ifreez00_1269;

				BgL_ifreez00_1269 =
					BGl_funionze70ze7zz__evaluate_avarz00(
					(((BgL_ev_absz00_bglt) COBJECT(BgL_absz00_75))->BgL_innerz00));
				{
					obj_t BgL_auxz00_3142;

					{	/* Eval/evaluate_avar.scm 234 */
						obj_t BgL_arg1367z00_1271;

						BgL_arg1367z00_1271 =
							(((BgL_ev_absz00_bglt) COBJECT(BgL_absz00_75))->BgL_bindz00);
						BgL_auxz00_3142 =
							BGl_appendzd221011zd2zz__evaluate_avarz00(BgL_varsz00_76,
							BgL_arg1367z00_1271);
					}
					((((BgL_ev_absz00_bglt) COBJECT(BgL_absz00_75))->BgL_bindz00) =
						((obj_t) BgL_auxz00_3142), BUNSPEC);
				}
				{	/* Eval/evaluate_avar.scm 236 */
					obj_t BgL_g1194z00_1272;

					BgL_g1194z00_1272 =
						BGl_diffz00zz__evaluate_avarz00(BgL_varsz00_76, BgL_ifreez00_1269);
					{
						obj_t BgL_l1192z00_2126;

						BgL_l1192z00_2126 = BgL_g1194z00_1272;
					BgL_zc3z04anonymousza31368ze3z87_2125:
						if (PAIRP(BgL_l1192z00_2126))
							{	/* Eval/evaluate_avar.scm 239 */
								{	/* Eval/evaluate_avar.scm 237 */
									obj_t BgL_vz00_2132;

									BgL_vz00_2132 = CAR(BgL_l1192z00_2126);
									((((BgL_ev_varz00_bglt) COBJECT(
													((BgL_ev_varz00_bglt) BgL_vz00_2132)))->BgL_effz00) =
										((obj_t) BFALSE), BUNSPEC);
								}
								{
									obj_t BgL_l1192z00_3152;

									BgL_l1192z00_3152 = CDR(BgL_l1192z00_2126);
									BgL_l1192z00_2126 = BgL_l1192z00_3152;
									goto BgL_zc3z04anonymousza31368ze3z87_2125;
								}
							}
						else
							{	/* Eval/evaluate_avar.scm 239 */
								((bool_t) 1);
							}
					}
				}
				return BgL_ifreez00_1269;
			}
		}

	}



/* funion~0 */
	obj_t BGl_funionze70ze7zz__evaluate_avarz00(obj_t BgL_lz00_1283)
	{
		{	/* Eval/evaluate_avar.scm 231 */
			if (NULLP(BgL_lz00_1283))
				{	/* Eval/evaluate_avar.scm 229 */
					return BNIL;
				}
			else
				{	/* Eval/evaluate_avar.scm 231 */
					obj_t BgL_arg1375z00_1286;
					obj_t BgL_arg1376z00_1287;

					BgL_arg1375z00_1286 =
						(((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt)
									CAR(((obj_t) BgL_lz00_1283)))))->BgL_freez00);
					{	/* Eval/evaluate_avar.scm 231 */
						obj_t BgL_arg1377z00_1289;

						BgL_arg1377z00_1289 = CDR(((obj_t) BgL_lz00_1283));
						BgL_arg1376z00_1287 =
							BGl_funionze70ze7zz__evaluate_avarz00(BgL_arg1377z00_1289);
					}
					{
						obj_t BgL_l1z00_2099;
						obj_t BgL_l2z00_2100;

						BgL_l1z00_2099 = BgL_arg1375z00_1286;
						BgL_l2z00_2100 = BgL_arg1376z00_1287;
					BgL_unionz00_2098:
						if (NULLP(BgL_l1z00_2099))
							{	/* Eval/evaluate_avar.scm 77 */
								return BgL_l2z00_2100;
							}
						else
							{	/* Eval/evaluate_avar.scm 79 */
								obj_t BgL_xz00_2107;

								BgL_xz00_2107 = CAR(((obj_t) BgL_l1z00_2099));
								{	/* Eval/evaluate_avar.scm 80 */
									obj_t BgL_arg1346z00_2108;
									obj_t BgL_arg1347z00_2109;

									BgL_arg1346z00_2108 = CDR(((obj_t) BgL_l1z00_2099));
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_xz00_2107, BgL_l2z00_2100)))
										{	/* Eval/evaluate_avar.scm 80 */
											BgL_arg1347z00_2109 = BgL_l2z00_2100;
										}
									else
										{	/* Eval/evaluate_avar.scm 80 */
											BgL_arg1347z00_2109 =
												MAKE_YOUNG_PAIR(BgL_xz00_2107, BgL_l2z00_2100);
										}
									{
										obj_t BgL_l2z00_3174;
										obj_t BgL_l1z00_3173;

										BgL_l1z00_3173 = BgL_arg1346z00_2108;
										BgL_l2z00_3174 = BgL_arg1347z00_2109;
										BgL_l2z00_2100 = BgL_l2z00_3174;
										BgL_l1z00_2099 = BgL_l1z00_3173;
										goto BgL_unionz00_2098;
									}
								}
							}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_proc1886z00zz__evaluate_avarz00,
				BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string1887z00zz__evaluate_avarz00);
		}

	}



/* &avar1214 */
	obj_t BGl_z62avar1214z62zz__evaluate_avarz00(obj_t BgL_envz00_2741,
		obj_t BgL_ez00_2742, obj_t BgL_localz00_2743, obj_t BgL_absz00_2744)
	{
		{	/* Eval/evaluate_avar.scm 122 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol1888z00zz__evaluate_avarz00,
				BGl_string1889z00zz__evaluate_avarz00,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_2742)));
		}

	}



/* avar */
	obj_t BGl_avarz00zz__evaluate_avarz00(BgL_ev_exprz00_bglt BgL_ez00_18,
		obj_t BgL_localz00_19, obj_t BgL_absz00_20)
	{
		{	/* Eval/evaluate_avar.scm 122 */
			{	/* Eval/evaluate_avar.scm 122 */
				obj_t BgL_method1215z00_1297;

				{	/* Eval/evaluate_avar.scm 122 */
					obj_t BgL_res1856z00_2167;

					{	/* Eval/evaluate_avar.scm 122 */
						long BgL_objzd2classzd2numz00_2138;

						BgL_objzd2classzd2numz00_2138 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_18));
						{	/* Eval/evaluate_avar.scm 122 */
							obj_t BgL_arg1835z00_2139;

							BgL_arg1835z00_2139 =
								PROCEDURE_REF(BGl_avarzd2envzd2zz__evaluate_avarz00,
								(int) (1L));
							{	/* Eval/evaluate_avar.scm 122 */
								int BgL_offsetz00_2142;

								BgL_offsetz00_2142 = (int) (BgL_objzd2classzd2numz00_2138);
								{	/* Eval/evaluate_avar.scm 122 */
									long BgL_offsetz00_2143;

									BgL_offsetz00_2143 =
										((long) (BgL_offsetz00_2142) - OBJECT_TYPE);
									{	/* Eval/evaluate_avar.scm 122 */
										long BgL_modz00_2144;

										BgL_modz00_2144 =
											(BgL_offsetz00_2143 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_avar.scm 122 */
											long BgL_restz00_2146;

											BgL_restz00_2146 =
												(BgL_offsetz00_2143 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_avar.scm 122 */

												{	/* Eval/evaluate_avar.scm 122 */
													obj_t BgL_bucketz00_2148;

													BgL_bucketz00_2148 =
														VECTOR_REF(
														((obj_t) BgL_arg1835z00_2139), BgL_modz00_2144);
													BgL_res1856z00_2167 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2148), BgL_restz00_2146);
					}}}}}}}}
					BgL_method1215z00_1297 = BgL_res1856z00_2167;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1215z00_1297,
					((obj_t) BgL_ez00_18), BgL_localz00_19, BgL_absz00_20);
			}
		}

	}



/* &avar */
	obj_t BGl_z62avarz62zz__evaluate_avarz00(obj_t BgL_envz00_2745,
		obj_t BgL_ez00_2746, obj_t BgL_localz00_2747, obj_t BgL_absz00_2748)
	{
		{	/* Eval/evaluate_avar.scm 122 */
			{	/* Eval/evaluate_avar.scm 122 */
				BgL_ev_exprz00_bglt BgL_auxz00_3211;

				{	/* Eval/evaluate_avar.scm 122 */
					bool_t BgL_test1962z00_3212;

					{	/* Eval/evaluate_avar.scm 122 */
						obj_t BgL_classz00_2864;

						BgL_classz00_2864 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_2746))
							{	/* Eval/evaluate_avar.scm 122 */
								BgL_objectz00_bglt BgL_arg1833z00_2866;
								long BgL_arg1834z00_2867;

								BgL_arg1833z00_2866 = (BgL_objectz00_bglt) (BgL_ez00_2746);
								BgL_arg1834z00_2867 = BGL_CLASS_DEPTH(BgL_classz00_2864);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_avar.scm 122 */
										long BgL_idxz00_2875;

										BgL_idxz00_2875 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1833z00_2866);
										{	/* Eval/evaluate_avar.scm 122 */
											obj_t BgL_arg1822z00_2876;

											{	/* Eval/evaluate_avar.scm 122 */
												long BgL_arg1823z00_2877;

												BgL_arg1823z00_2877 =
													(BgL_idxz00_2875 + BgL_arg1834z00_2867);
												BgL_arg1822z00_2876 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1823z00_2877);
											}
											BgL_test1962z00_3212 =
												(BgL_arg1822z00_2876 == BgL_classz00_2864);
									}}
								else
									{	/* Eval/evaluate_avar.scm 122 */
										bool_t BgL_res1912z00_2903;

										{	/* Eval/evaluate_avar.scm 122 */
											obj_t BgL_oclassz00_2885;

											{	/* Eval/evaluate_avar.scm 122 */
												obj_t BgL_arg1838z00_2893;
												long BgL_arg1839z00_2894;

												BgL_arg1838z00_2893 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_avar.scm 122 */
													long BgL_arg1840z00_2895;

													BgL_arg1840z00_2895 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1833z00_2866);
													BgL_arg1839z00_2894 =
														(BgL_arg1840z00_2895 - OBJECT_TYPE);
												}
												BgL_oclassz00_2885 =
													VECTOR_REF(BgL_arg1838z00_2893, BgL_arg1839z00_2894);
											}
											{	/* Eval/evaluate_avar.scm 122 */
												bool_t BgL__ortest_1137z00_2886;

												BgL__ortest_1137z00_2886 =
													(BgL_classz00_2864 == BgL_oclassz00_2885);
												if (BgL__ortest_1137z00_2886)
													{	/* Eval/evaluate_avar.scm 122 */
														BgL_res1912z00_2903 = BgL__ortest_1137z00_2886;
													}
												else
													{	/* Eval/evaluate_avar.scm 122 */
														long BgL_odepthz00_2887;

														{	/* Eval/evaluate_avar.scm 122 */
															obj_t BgL_arg1828z00_2888;

															BgL_arg1828z00_2888 = (BgL_oclassz00_2885);
															BgL_odepthz00_2887 =
																BGL_CLASS_DEPTH(BgL_arg1828z00_2888);
														}
														if ((BgL_arg1834z00_2867 < BgL_odepthz00_2887))
															{	/* Eval/evaluate_avar.scm 122 */
																obj_t BgL_arg1826z00_2890;

																{	/* Eval/evaluate_avar.scm 122 */
																	obj_t BgL_arg1827z00_2891;

																	BgL_arg1827z00_2891 = (BgL_oclassz00_2885);
																	BgL_arg1826z00_2890 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1827z00_2891,
																		BgL_arg1834z00_2867);
																}
																BgL_res1912z00_2903 =
																	(BgL_arg1826z00_2890 == BgL_classz00_2864);
															}
														else
															{	/* Eval/evaluate_avar.scm 122 */
																BgL_res1912z00_2903 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1962z00_3212 = BgL_res1912z00_2903;
									}
							}
						else
							{	/* Eval/evaluate_avar.scm 122 */
								BgL_test1962z00_3212 = ((bool_t) 0);
							}
					}
					if (BgL_test1962z00_3212)
						{	/* Eval/evaluate_avar.scm 122 */
							BgL_auxz00_3211 = ((BgL_ev_exprz00_bglt) BgL_ez00_2746);
						}
					else
						{
							obj_t BgL_auxz00_3237;

							BgL_auxz00_3237 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1880z00zz__evaluate_avarz00, BINT(3282L),
								BGl_string1890z00zz__evaluate_avarz00,
								BGl_string1882z00zz__evaluate_avarz00, BgL_ez00_2746);
							FAILURE(BgL_auxz00_3237, BFALSE, BFALSE);
						}
				}
				return
					BGl_avarz00zz__evaluate_avarz00(BgL_auxz00_3211, BgL_localz00_2747,
					BgL_absz00_2748);
			}
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_varz00zz__evaluate_typesz00, BGl_proc1891z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc1893z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_littz00zz__evaluate_typesz00,
				BGl_proc1894z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_ifz00zz__evaluate_typesz00, BGl_proc1895z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_listz00zz__evaluate_typesz00,
				BGl_proc1896z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc1897z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_hookz00zz__evaluate_typesz00,
				BGl_proc1898z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_setlocalz00zz__evaluate_typesz00,
				BGl_proc1899z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc1900z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc1901z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc1902z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc1903z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_letz00zz__evaluate_typesz00, BGl_proc1904z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_letza2za2zz__evaluate_typesz00,
				BGl_proc1905z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_letrecz00zz__evaluate_typesz00,
				BGl_proc1906z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc1907z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_gotoz00zz__evaluate_typesz00,
				BGl_proc1908z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_appz00zz__evaluate_typesz00, BGl_proc1909z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_avarzd2envzd2zz__evaluate_avarz00,
				BGl_ev_absz00zz__evaluate_typesz00, BGl_proc1910z00zz__evaluate_avarz00,
				BGl_string1892z00zz__evaluate_avarz00);
		}

	}



/* &avar-ev_abs1253 */
	obj_t BGl_z62avarzd2ev_abs1253zb0zz__evaluate_avarz00(obj_t BgL_envz00_2768,
		obj_t BgL_ez00_2769, obj_t BgL_localz00_2770, obj_t BgL_absz00_2771)
	{
		{	/* Eval/evaluate_avar.scm 242 */
			{
				obj_t BgL_auxz00_3261;

				{	/* Eval/evaluate_avar.scm 245 */
					obj_t BgL_arg1472z00_2905;

					BgL_arg1472z00_2905 =
						(((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt) BgL_absz00_2771)))->BgL_innerz00);
					BgL_auxz00_3261 =
						MAKE_YOUNG_PAIR(
						((obj_t)
							((BgL_ev_absz00_bglt) BgL_ez00_2769)), BgL_arg1472z00_2905);
				}
				((((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt) BgL_absz00_2771)))->BgL_innerz00) =
					((obj_t) BgL_auxz00_3261), BUNSPEC);
			}
			{	/* Eval/evaluate_avar.scm 246 */
				BgL_ev_exprz00_bglt BgL_arg1473z00_2906;
				obj_t BgL_arg1474z00_2907;

				BgL_arg1473z00_2906 =
					(((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_bodyz00);
				BgL_arg1474z00_2907 =
					(((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_varsz00);
				BGl_avarz00zz__evaluate_avarz00(BgL_arg1473z00_2906,
					BgL_arg1474z00_2907, ((obj_t) ((BgL_ev_absz00_bglt) BgL_ez00_2769)));
			}
			{	/* Eval/evaluate_avar.scm 247 */
				obj_t BgL_ifreez00_2908;

				BgL_ifreez00_2908 =
					BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(
					((BgL_ev_absz00_bglt) BgL_ez00_2769),
					(((BgL_ev_absz00_bglt) COBJECT(
								((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_varsz00));
				{
					obj_t BgL_auxz00_3280;

					{	/* Eval/evaluate_avar.scm 249 */
						obj_t BgL_arg1476z00_2909;
						obj_t BgL_arg1477z00_2910;

						{	/* Eval/evaluate_avar.scm 249 */
							obj_t BgL_arg1478z00_2911;

							BgL_arg1478z00_2911 =
								(((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_freez00);
							{
								obj_t BgL_l1z00_2913;
								obj_t BgL_l2z00_2914;

								BgL_l1z00_2913 = BgL_ifreez00_2908;
								BgL_l2z00_2914 = BgL_arg1478z00_2911;
							BgL_unionz00_2912:
								if (NULLP(BgL_l1z00_2913))
									{	/* Eval/evaluate_avar.scm 77 */
										BgL_arg1476z00_2909 = BgL_l2z00_2914;
									}
								else
									{	/* Eval/evaluate_avar.scm 79 */
										obj_t BgL_xz00_2915;

										BgL_xz00_2915 = CAR(((obj_t) BgL_l1z00_2913));
										{	/* Eval/evaluate_avar.scm 80 */
											obj_t BgL_arg1346z00_2916;
											obj_t BgL_arg1347z00_2917;

											BgL_arg1346z00_2916 = CDR(((obj_t) BgL_l1z00_2913));
											if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_xz00_2915, BgL_l2z00_2914)))
												{	/* Eval/evaluate_avar.scm 80 */
													BgL_arg1347z00_2917 = BgL_l2z00_2914;
												}
											else
												{	/* Eval/evaluate_avar.scm 80 */
													BgL_arg1347z00_2917 =
														MAKE_YOUNG_PAIR(BgL_xz00_2915, BgL_l2z00_2914);
												}
											{
												obj_t BgL_l2z00_3295;
												obj_t BgL_l1z00_3294;

												BgL_l1z00_3294 = BgL_arg1346z00_2916;
												BgL_l2z00_3295 = BgL_arg1347z00_2917;
												BgL_l2z00_2914 = BgL_l2z00_3295;
												BgL_l1z00_2913 = BgL_l1z00_3294;
												goto BgL_unionz00_2912;
											}
										}
									}
							}
						}
						BgL_arg1477z00_2910 =
							(((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_bindz00);
						BgL_auxz00_3280 =
							BGl_diffz00zz__evaluate_avarz00(BgL_arg1476z00_2909,
							BgL_arg1477z00_2910);
					}
					((((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_freez00) =
						((obj_t) BgL_auxz00_3280), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_3300;

					{	/* Eval/evaluate_avar.scm 250 */
						obj_t BgL_hook1199z00_2918;

						BgL_hook1199z00_2918 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{	/* Eval/evaluate_avar.scm 250 */
							obj_t BgL_g1200z00_2919;

							BgL_g1200z00_2919 =
								(((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_varsz00);
							{
								obj_t BgL_l1196z00_2921;
								obj_t BgL_h1197z00_2922;

								BgL_l1196z00_2921 = BgL_g1200z00_2919;
								BgL_h1197z00_2922 = BgL_hook1199z00_2918;
							BgL_zc3z04anonymousza31479ze3z87_2920:
								if (NULLP(BgL_l1196z00_2921))
									{	/* Eval/evaluate_avar.scm 250 */
										BgL_auxz00_3300 = CDR(BgL_hook1199z00_2918);
									}
								else
									{	/* Eval/evaluate_avar.scm 250 */
										if (CBOOL(
												(((BgL_ev_varz00_bglt) COBJECT(
															((BgL_ev_varz00_bglt)
																CAR(
																	((obj_t) BgL_l1196z00_2921)))))->BgL_effz00)))
											{	/* Eval/evaluate_avar.scm 250 */
												obj_t BgL_nh1198z00_2923;

												{	/* Eval/evaluate_avar.scm 250 */
													obj_t BgL_arg1483z00_2924;

													BgL_arg1483z00_2924 =
														CAR(((obj_t) BgL_l1196z00_2921));
													BgL_nh1198z00_2923 =
														MAKE_YOUNG_PAIR(BgL_arg1483z00_2924, BNIL);
												}
												SET_CDR(BgL_h1197z00_2922, BgL_nh1198z00_2923);
												{	/* Eval/evaluate_avar.scm 250 */
													obj_t BgL_arg1482z00_2925;

													BgL_arg1482z00_2925 =
														CDR(((obj_t) BgL_l1196z00_2921));
													{
														obj_t BgL_h1197z00_3321;
														obj_t BgL_l1196z00_3320;

														BgL_l1196z00_3320 = BgL_arg1482z00_2925;
														BgL_h1197z00_3321 = BgL_nh1198z00_2923;
														BgL_h1197z00_2922 = BgL_h1197z00_3321;
														BgL_l1196z00_2921 = BgL_l1196z00_3320;
														goto BgL_zc3z04anonymousza31479ze3z87_2920;
													}
												}
											}
										else
											{	/* Eval/evaluate_avar.scm 250 */
												obj_t BgL_arg1484z00_2926;

												BgL_arg1484z00_2926 = CDR(((obj_t) BgL_l1196z00_2921));
												{
													obj_t BgL_l1196z00_3324;

													BgL_l1196z00_3324 = BgL_arg1484z00_2926;
													BgL_l1196z00_2921 = BgL_l1196z00_3324;
													goto BgL_zc3z04anonymousza31479ze3z87_2920;
												}
											}
									}
							}
						}
					}
					return
						((((BgL_ev_absz00_bglt) COBJECT(
									((BgL_ev_absz00_bglt) BgL_ez00_2769)))->BgL_boxesz00) =
						((obj_t) BgL_auxz00_3300), BUNSPEC);
				}
			}
		}

	}



/* &avar-ev_app1251 */
	obj_t BGl_z62avarzd2ev_app1251zb0zz__evaluate_avarz00(obj_t BgL_envz00_2772,
		obj_t BgL_ez00_2773, obj_t BgL_localz00_2774, obj_t BgL_absz00_2775)
	{
		{	/* Eval/evaluate_avar.scm 220 */
			{	/* Eval/evaluate_avar.scm 221 */
				bool_t BgL_tmpz00_3326;

				BGl_avarz00zz__evaluate_avarz00(
					(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_2773)))->BgL_funz00),
					BgL_localz00_2774, BgL_absz00_2775);
				{	/* Eval/evaluate_avar.scm 223 */
					obj_t BgL_g1191z00_2928;

					BgL_g1191z00_2928 =
						(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_2773)))->BgL_argsz00);
					{
						obj_t BgL_l1189z00_2930;

						BgL_l1189z00_2930 = BgL_g1191z00_2928;
					BgL_zc3z04anonymousza31467ze3z87_2929:
						if (PAIRP(BgL_l1189z00_2930))
							{	/* Eval/evaluate_avar.scm 223 */
								{	/* Eval/evaluate_avar.scm 223 */
									obj_t BgL_ez00_2931;

									BgL_ez00_2931 = CAR(BgL_l1189z00_2930);
									BGl_avarz00zz__evaluate_avarz00(
										((BgL_ev_exprz00_bglt) BgL_ez00_2931), BgL_localz00_2774,
										BgL_absz00_2775);
								}
								{
									obj_t BgL_l1189z00_3337;

									BgL_l1189z00_3337 = CDR(BgL_l1189z00_2930);
									BgL_l1189z00_2930 = BgL_l1189z00_3337;
									goto BgL_zc3z04anonymousza31467ze3z87_2929;
								}
							}
						else
							{	/* Eval/evaluate_avar.scm 223 */
								BgL_tmpz00_3326 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3326);
			}
		}

	}



/* &avar-ev_goto1249 */
	obj_t BGl_z62avarzd2ev_goto1249zb0zz__evaluate_avarz00(obj_t BgL_envz00_2776,
		obj_t BgL_ez00_2777, obj_t BgL_localz00_2778, obj_t BgL_absz00_2779)
	{
		{	/* Eval/evaluate_avar.scm 216 */
			{	/* Eval/evaluate_avar.scm 217 */
				bool_t BgL_tmpz00_3340;

				{	/* Eval/evaluate_avar.scm 218 */
					obj_t BgL_g1188z00_2933;

					BgL_g1188z00_2933 =
						(((BgL_ev_gotoz00_bglt) COBJECT(
								((BgL_ev_gotoz00_bglt) BgL_ez00_2777)))->BgL_argsz00);
					{
						obj_t BgL_l1186z00_2935;

						BgL_l1186z00_2935 = BgL_g1188z00_2933;
					BgL_zc3z04anonymousza31463ze3z87_2934:
						if (PAIRP(BgL_l1186z00_2935))
							{	/* Eval/evaluate_avar.scm 218 */
								{	/* Eval/evaluate_avar.scm 218 */
									obj_t BgL_ez00_2936;

									BgL_ez00_2936 = CAR(BgL_l1186z00_2935);
									BGl_avarz00zz__evaluate_avarz00(
										((BgL_ev_exprz00_bglt) BgL_ez00_2936), BgL_localz00_2778,
										BgL_absz00_2779);
								}
								{
									obj_t BgL_l1186z00_3348;

									BgL_l1186z00_3348 = CDR(BgL_l1186z00_2935);
									BgL_l1186z00_2935 = BgL_l1186z00_3348;
									goto BgL_zc3z04anonymousza31463ze3z87_2934;
								}
							}
						else
							{	/* Eval/evaluate_avar.scm 218 */
								BgL_tmpz00_3340 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3340);
			}
		}

	}



/* &avar-ev_labels1247 */
	obj_t BGl_z62avarzd2ev_labels1247zb0zz__evaluate_avarz00(obj_t
		BgL_envz00_2780, obj_t BgL_ez00_2781, obj_t BgL_localz00_2782,
		obj_t BgL_absz00_2783)
	{
		{	/* Eval/evaluate_avar.scm 204 */
			{	/* Eval/evaluate_avar.scm 206 */
				obj_t BgL_localz00_2938;

				BgL_localz00_2938 =
					BGl_appendzd221011zd2zz__evaluate_avarz00(
					(((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_ez00_2781)))->BgL_varsz00),
					BgL_localz00_2782);
				{	/* Eval/evaluate_avar.scm 207 */
					obj_t BgL_g1174z00_2939;

					BgL_g1174z00_2939 =
						(((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_ez00_2781)))->BgL_valsz00);
					{
						obj_t BgL_l1172z00_2941;

						BgL_l1172z00_2941 = BgL_g1174z00_2939;
					BgL_zc3z04anonymousza31442ze3z87_2940:
						if (PAIRP(BgL_l1172z00_2941))
							{	/* Eval/evaluate_avar.scm 207 */
								{	/* Eval/evaluate_avar.scm 209 */
									obj_t BgL_sz00_2942;

									BgL_sz00_2942 = CAR(BgL_l1172z00_2941);
									{	/* Eval/evaluate_avar.scm 209 */
										obj_t BgL_arg1444z00_2943;
										obj_t BgL_arg1445z00_2944;

										BgL_arg1444z00_2943 = CDR(((obj_t) BgL_sz00_2942));
										{	/* Eval/evaluate_avar.scm 209 */
											obj_t BgL_arg1446z00_2945;

											BgL_arg1446z00_2945 = CAR(((obj_t) BgL_sz00_2942));
											BgL_arg1445z00_2944 =
												BGl_appendzd221011zd2zz__evaluate_avarz00
												(BgL_arg1446z00_2945, BgL_localz00_2938);
										}
										BGl_avarz00zz__evaluate_avarz00(
											((BgL_ev_exprz00_bglt) BgL_arg1444z00_2943),
											BgL_arg1445z00_2944, BgL_absz00_2783);
									}
									{	/* Eval/evaluate_avar.scm 210 */
										obj_t BgL_arg1447z00_2946;

										BgL_arg1447z00_2946 = CAR(((obj_t) BgL_sz00_2942));
										BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(
											((BgL_ev_absz00_bglt) BgL_absz00_2783),
											BgL_arg1447z00_2946);
									}
								}
								{
									obj_t BgL_l1172z00_3370;

									BgL_l1172z00_3370 = CDR(BgL_l1172z00_2941);
									BgL_l1172z00_2941 = BgL_l1172z00_3370;
									goto BgL_zc3z04anonymousza31442ze3z87_2940;
								}
							}
						else
							{	/* Eval/evaluate_avar.scm 207 */
								((bool_t) 1);
							}
					}
				}
				{
					obj_t BgL_auxz00_3372;

					{	/* Eval/evaluate_avar.scm 213 */
						obj_t BgL_ll1180z00_2947;
						obj_t BgL_ll1181z00_2948;

						BgL_ll1180z00_2947 =
							(((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_2781)))->BgL_varsz00);
						BgL_ll1181z00_2948 =
							(((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_2781)))->BgL_valsz00);
						if (NULLP(BgL_ll1180z00_2947))
							{	/* Eval/evaluate_avar.scm 213 */
								BgL_auxz00_3372 = BNIL;
							}
						else
							{	/* Eval/evaluate_avar.scm 213 */
								obj_t BgL_head1182z00_2949;

								BgL_head1182z00_2949 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_ll1180z00_2951;
									obj_t BgL_ll1181z00_2952;
									obj_t BgL_tail1183z00_2953;

									BgL_ll1180z00_2951 = BgL_ll1180z00_2947;
									BgL_ll1181z00_2952 = BgL_ll1181z00_2948;
									BgL_tail1183z00_2953 = BgL_head1182z00_2949;
								BgL_zc3z04anonymousza31450ze3z87_2950:
									if (NULLP(BgL_ll1180z00_2951))
										{	/* Eval/evaluate_avar.scm 213 */
											BgL_auxz00_3372 = CDR(BgL_head1182z00_2949);
										}
									else
										{	/* Eval/evaluate_avar.scm 213 */
											obj_t BgL_newtail1184z00_2954;

											{	/* Eval/evaluate_avar.scm 213 */
												obj_t BgL_arg1454z00_2955;

												{	/* Eval/evaluate_avar.scm 213 */
													obj_t BgL_varz00_2956;
													obj_t BgL_sz00_2957;

													BgL_varz00_2956 = CAR(((obj_t) BgL_ll1180z00_2951));
													BgL_sz00_2957 = CAR(((obj_t) BgL_ll1181z00_2952));
													{	/* Eval/evaluate_avar.scm 213 */
														obj_t BgL_arg1455z00_2958;

														{	/* Eval/evaluate_avar.scm 213 */
															obj_t BgL_l1175z00_2959;

															BgL_l1175z00_2959 = CAR(((obj_t) BgL_sz00_2957));
															if (NULLP(BgL_l1175z00_2959))
																{	/* Eval/evaluate_avar.scm 213 */
																	BgL_arg1455z00_2958 = BNIL;
																}
															else
																{	/* Eval/evaluate_avar.scm 213 */
																	obj_t BgL_head1177z00_2960;

																	BgL_head1177z00_2960 =
																		MAKE_YOUNG_PAIR(BNIL, BNIL);
																	{
																		obj_t BgL_l1175z00_2962;
																		obj_t BgL_tail1178z00_2963;

																		BgL_l1175z00_2962 = BgL_l1175z00_2959;
																		BgL_tail1178z00_2963 = BgL_head1177z00_2960;
																	BgL_zc3z04anonymousza31457ze3z87_2961:
																		if (NULLP(BgL_l1175z00_2962))
																			{	/* Eval/evaluate_avar.scm 213 */
																				BgL_arg1455z00_2958 =
																					CDR(BgL_head1177z00_2960);
																			}
																		else
																			{	/* Eval/evaluate_avar.scm 213 */
																				obj_t BgL_newtail1179z00_2964;

																				{	/* Eval/evaluate_avar.scm 213 */
																					obj_t BgL_arg1460z00_2965;

																					BgL_arg1460z00_2965 =
																						(((BgL_ev_varz00_bglt) COBJECT(
																								((BgL_ev_varz00_bglt)
																									CAR(
																										((obj_t)
																											BgL_l1175z00_2962)))))->
																						BgL_effz00);
																					BgL_newtail1179z00_2964 =
																						MAKE_YOUNG_PAIR(BgL_arg1460z00_2965,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1178z00_2963,
																					BgL_newtail1179z00_2964);
																				{	/* Eval/evaluate_avar.scm 213 */
																					obj_t BgL_arg1459z00_2966;

																					BgL_arg1459z00_2966 =
																						CDR(((obj_t) BgL_l1175z00_2962));
																					{
																						obj_t BgL_tail1178z00_3405;
																						obj_t BgL_l1175z00_3404;

																						BgL_l1175z00_3404 =
																							BgL_arg1459z00_2966;
																						BgL_tail1178z00_3405 =
																							BgL_newtail1179z00_2964;
																						BgL_tail1178z00_2963 =
																							BgL_tail1178z00_3405;
																						BgL_l1175z00_2962 =
																							BgL_l1175z00_3404;
																						goto
																							BgL_zc3z04anonymousza31457ze3z87_2961;
																					}
																				}
																			}
																	}
																}
														}
														BgL_arg1454z00_2955 =
															MAKE_YOUNG_PAIR(BgL_varz00_2956,
															BgL_arg1455z00_2958);
													}
												}
												BgL_newtail1184z00_2954 =
													MAKE_YOUNG_PAIR(BgL_arg1454z00_2955, BNIL);
											}
											SET_CDR(BgL_tail1183z00_2953, BgL_newtail1184z00_2954);
											{	/* Eval/evaluate_avar.scm 213 */
												obj_t BgL_arg1452z00_2967;
												obj_t BgL_arg1453z00_2968;

												BgL_arg1452z00_2967 = CDR(((obj_t) BgL_ll1180z00_2951));
												BgL_arg1453z00_2968 = CDR(((obj_t) BgL_ll1181z00_2952));
												{
													obj_t BgL_tail1183z00_3415;
													obj_t BgL_ll1181z00_3414;
													obj_t BgL_ll1180z00_3413;

													BgL_ll1180z00_3413 = BgL_arg1452z00_2967;
													BgL_ll1181z00_3414 = BgL_arg1453z00_2968;
													BgL_tail1183z00_3415 = BgL_newtail1184z00_2954;
													BgL_tail1183z00_2953 = BgL_tail1183z00_3415;
													BgL_ll1181z00_2952 = BgL_ll1181z00_3414;
													BgL_ll1180z00_2951 = BgL_ll1180z00_3413;
													goto BgL_zc3z04anonymousza31450ze3z87_2950;
												}
											}
										}
								}
							}
					}
					((((BgL_ev_labelsz00_bglt) COBJECT(
									((BgL_ev_labelsz00_bglt) BgL_ez00_2781)))->BgL_boxesz00) =
						((obj_t) BgL_auxz00_3372), BUNSPEC);
				}
				return
					BGl_avarz00zz__evaluate_avarz00(
					(((BgL_ev_labelsz00_bglt) COBJECT(
								((BgL_ev_labelsz00_bglt) BgL_ez00_2781)))->BgL_bodyz00),
					BgL_localz00_2938, BgL_absz00_2783);
			}
		}

	}



/* &avar-ev_letrec1245 */
	obj_t BGl_z62avarzd2ev_letrec1245zb0zz__evaluate_avarz00(obj_t
		BgL_envz00_2784, obj_t BgL_ez00_2785, obj_t BgL_localz00_2786,
		obj_t BgL_absz00_2787)
	{
		{	/* Eval/evaluate_avar.scm 195 */
			{	/* Eval/evaluate_avar.scm 196 */
				bool_t BgL_tmpz00_3420;

				{	/* Eval/evaluate_avar.scm 197 */
					obj_t BgL_localz00_2970;

					{	/* Eval/evaluate_avar.scm 197 */
						obj_t BgL_arg1441z00_2971;

						BgL_arg1441z00_2971 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_2785))))->BgL_varsz00);
						BgL_localz00_2970 =
							BGl_appendzd221011zd2zz__evaluate_avarz00(BgL_arg1441z00_2971,
							BgL_localz00_2786);
					}
					{	/* Eval/evaluate_avar.scm 198 */
						obj_t BgL_g1168z00_2972;

						BgL_g1168z00_2972 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_2785))))->BgL_valsz00);
						{
							obj_t BgL_l1166z00_2974;

							BgL_l1166z00_2974 = BgL_g1168z00_2972;
						BgL_zc3z04anonymousza31431ze3z87_2973:
							if (PAIRP(BgL_l1166z00_2974))
								{	/* Eval/evaluate_avar.scm 198 */
									{	/* Eval/evaluate_avar.scm 198 */
										obj_t BgL_ez00_2975;

										BgL_ez00_2975 = CAR(BgL_l1166z00_2974);
										BGl_avarz00zz__evaluate_avarz00(
											((BgL_ev_exprz00_bglt) BgL_ez00_2975), BgL_localz00_2970,
											BgL_absz00_2787);
									}
									{
										obj_t BgL_l1166z00_3433;

										BgL_l1166z00_3433 = CDR(BgL_l1166z00_2974);
										BgL_l1166z00_2974 = BgL_l1166z00_3433;
										goto BgL_zc3z04anonymousza31431ze3z87_2973;
									}
								}
							else
								{	/* Eval/evaluate_avar.scm 198 */
									((bool_t) 1);
								}
						}
					}
					{	/* Eval/evaluate_avar.scm 199 */
						BgL_ev_exprz00_bglt BgL_arg1435z00_2976;

						BgL_arg1435z00_2976 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_2785))))->BgL_bodyz00);
						BGl_avarz00zz__evaluate_avarz00(BgL_arg1435z00_2976,
							BgL_localz00_2970, BgL_absz00_2787);
					}
					{
						obj_t BgL_auxz00_3439;

						{	/* Eval/evaluate_avar.scm 201 */
							obj_t BgL_arg1436z00_2977;
							obj_t BgL_arg1437z00_2978;

							BgL_arg1436z00_2977 =
								(((BgL_ev_binderz00_bglt) COBJECT(
										((BgL_ev_binderz00_bglt)
											((BgL_ev_letrecz00_bglt) BgL_ez00_2785))))->BgL_varsz00);
							BgL_arg1437z00_2978 =
								(((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_absz00_2787)))->BgL_bindz00);
							BgL_auxz00_3439 =
								BGl_appendzd221011zd2zz__evaluate_avarz00(BgL_arg1436z00_2977,
								BgL_arg1437z00_2978);
						}
						((((BgL_ev_absz00_bglt) COBJECT(
										((BgL_ev_absz00_bglt) BgL_absz00_2787)))->BgL_bindz00) =
							((obj_t) BgL_auxz00_3439), BUNSPEC);
					}
					{	/* Eval/evaluate_avar.scm 202 */
						obj_t BgL_g1171z00_2979;

						BgL_g1171z00_2979 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letrecz00_bglt) BgL_ez00_2785))))->BgL_varsz00);
						{
							obj_t BgL_l1169z00_2981;

							BgL_l1169z00_2981 = BgL_g1171z00_2979;
						BgL_zc3z04anonymousza31438ze3z87_2980:
							if (PAIRP(BgL_l1169z00_2981))
								{	/* Eval/evaluate_avar.scm 202 */
									{	/* Eval/evaluate_avar.scm 202 */
										obj_t BgL_vz00_2982;

										BgL_vz00_2982 = CAR(BgL_l1169z00_2981);
										((((BgL_ev_varz00_bglt) COBJECT(
														((BgL_ev_varz00_bglt) BgL_vz00_2982)))->
												BgL_effz00) = ((obj_t) BTRUE), BUNSPEC);
									}
									{
										obj_t BgL_l1169z00_3456;

										BgL_l1169z00_3456 = CDR(BgL_l1169z00_2981);
										BgL_l1169z00_2981 = BgL_l1169z00_3456;
										goto BgL_zc3z04anonymousza31438ze3z87_2980;
									}
								}
							else
								{	/* Eval/evaluate_avar.scm 202 */
									BgL_tmpz00_3420 = ((bool_t) 1);
								}
						}
					}
				}
				return BBOOL(BgL_tmpz00_3420);
			}
		}

	}



/* &avar-ev_let*1243 */
	obj_t BGl_z62avarzd2ev_letza21243z12zz__evaluate_avarz00(obj_t
		BgL_envz00_2788, obj_t BgL_ez00_2789, obj_t BgL_localz00_2790,
		obj_t BgL_absz00_2791)
	{
		{	/* Eval/evaluate_avar.scm 187 */
			{	/* Eval/evaluate_avar.scm 189 */
				obj_t BgL_localz00_2984;

				{	/* Eval/evaluate_avar.scm 189 */
					obj_t BgL_arg1430z00_2985;

					BgL_arg1430z00_2985 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_ez00_2789))))->BgL_varsz00);
					BgL_localz00_2984 =
						BGl_appendzd221011zd2zz__evaluate_avarz00(BgL_arg1430z00_2985,
						BgL_localz00_2790);
				}
				{	/* Eval/evaluate_avar.scm 190 */
					obj_t BgL_g1160z00_2986;

					BgL_g1160z00_2986 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_ez00_2789))))->BgL_valsz00);
					{
						obj_t BgL_l1158z00_2988;

						BgL_l1158z00_2988 = BgL_g1160z00_2986;
					BgL_zc3z04anonymousza31420ze3z87_2987:
						if (PAIRP(BgL_l1158z00_2988))
							{	/* Eval/evaluate_avar.scm 190 */
								{	/* Eval/evaluate_avar.scm 190 */
									obj_t BgL_ez00_2989;

									BgL_ez00_2989 = CAR(BgL_l1158z00_2988);
									BGl_avarz00zz__evaluate_avarz00(
										((BgL_ev_exprz00_bglt) BgL_ez00_2989), BgL_localz00_2984,
										BgL_absz00_2791);
								}
								{
									obj_t BgL_l1158z00_3471;

									BgL_l1158z00_3471 = CDR(BgL_l1158z00_2988);
									BgL_l1158z00_2988 = BgL_l1158z00_3471;
									goto BgL_zc3z04anonymousza31420ze3z87_2987;
								}
							}
						else
							{	/* Eval/evaluate_avar.scm 190 */
								((bool_t) 1);
							}
					}
				}
				{	/* Eval/evaluate_avar.scm 191 */
					BgL_ev_exprz00_bglt BgL_arg1423z00_2990;

					BgL_arg1423z00_2990 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_ez00_2789))))->BgL_bodyz00);
					BGl_avarz00zz__evaluate_avarz00(BgL_arg1423z00_2990,
						BgL_localz00_2984, BgL_absz00_2791);
				}
				{	/* Eval/evaluate_avar.scm 192 */
					obj_t BgL_arg1424z00_2991;

					BgL_arg1424z00_2991 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letza2za2_bglt) BgL_ez00_2789))))->BgL_varsz00);
					BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(
						((BgL_ev_absz00_bglt) BgL_absz00_2791), BgL_arg1424z00_2991);
				}
				{
					obj_t BgL_auxz00_3482;

					{	/* Eval/evaluate_avar.scm 193 */
						obj_t BgL_l1161z00_2992;

						BgL_l1161z00_2992 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letza2za2_bglt) BgL_ez00_2789))))->BgL_varsz00);
						if (NULLP(BgL_l1161z00_2992))
							{	/* Eval/evaluate_avar.scm 193 */
								BgL_auxz00_3482 = BNIL;
							}
						else
							{	/* Eval/evaluate_avar.scm 193 */
								obj_t BgL_head1163z00_2993;

								BgL_head1163z00_2993 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1161z00_2995;
									obj_t BgL_tail1164z00_2996;

									BgL_l1161z00_2995 = BgL_l1161z00_2992;
									BgL_tail1164z00_2996 = BgL_head1163z00_2993;
								BgL_zc3z04anonymousza31426ze3z87_2994:
									if (NULLP(BgL_l1161z00_2995))
										{	/* Eval/evaluate_avar.scm 193 */
											BgL_auxz00_3482 = CDR(BgL_head1163z00_2993);
										}
									else
										{	/* Eval/evaluate_avar.scm 193 */
											obj_t BgL_newtail1165z00_2997;

											{	/* Eval/evaluate_avar.scm 193 */
												obj_t BgL_arg1429z00_2998;

												BgL_arg1429z00_2998 =
													(((BgL_ev_varz00_bglt) COBJECT(
															((BgL_ev_varz00_bglt)
																CAR(
																	((obj_t) BgL_l1161z00_2995)))))->BgL_effz00);
												BgL_newtail1165z00_2997 =
													MAKE_YOUNG_PAIR(BgL_arg1429z00_2998, BNIL);
											}
											SET_CDR(BgL_tail1164z00_2996, BgL_newtail1165z00_2997);
											{	/* Eval/evaluate_avar.scm 193 */
												obj_t BgL_arg1428z00_2999;

												BgL_arg1428z00_2999 = CDR(((obj_t) BgL_l1161z00_2995));
												{
													obj_t BgL_tail1164z00_3502;
													obj_t BgL_l1161z00_3501;

													BgL_l1161z00_3501 = BgL_arg1428z00_2999;
													BgL_tail1164z00_3502 = BgL_newtail1165z00_2997;
													BgL_tail1164z00_2996 = BgL_tail1164z00_3502;
													BgL_l1161z00_2995 = BgL_l1161z00_3501;
													goto BgL_zc3z04anonymousza31426ze3z87_2994;
												}
											}
										}
								}
							}
					}
					return
						((((BgL_ev_letza2za2_bglt) COBJECT(
									((BgL_ev_letza2za2_bglt) BgL_ez00_2789)))->BgL_boxesz00) =
						((obj_t) BgL_auxz00_3482), BUNSPEC);
				}
			}
		}

	}



/* &avar-ev_let1241 */
	obj_t BGl_z62avarzd2ev_let1241zb0zz__evaluate_avarz00(obj_t BgL_envz00_2792,
		obj_t BgL_ez00_2793, obj_t BgL_localz00_2794, obj_t BgL_absz00_2795)
	{
		{	/* Eval/evaluate_avar.scm 180 */
			{	/* Eval/evaluate_avar.scm 182 */
				obj_t BgL_g1151z00_3001;

				BgL_g1151z00_3001 =
					(((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt)
								((BgL_ev_letz00_bglt) BgL_ez00_2793))))->BgL_valsz00);
				{
					obj_t BgL_l1149z00_3003;

					BgL_l1149z00_3003 = BgL_g1151z00_3001;
				BgL_zc3z04anonymousza31406ze3z87_3002:
					if (PAIRP(BgL_l1149z00_3003))
						{	/* Eval/evaluate_avar.scm 182 */
							{	/* Eval/evaluate_avar.scm 182 */
								obj_t BgL_ez00_3004;

								BgL_ez00_3004 = CAR(BgL_l1149z00_3003);
								BGl_avarz00zz__evaluate_avarz00(
									((BgL_ev_exprz00_bglt) BgL_ez00_3004), BgL_localz00_2794,
									BgL_absz00_2795);
							}
							{
								obj_t BgL_l1149z00_3512;

								BgL_l1149z00_3512 = CDR(BgL_l1149z00_3003);
								BgL_l1149z00_3003 = BgL_l1149z00_3512;
								goto BgL_zc3z04anonymousza31406ze3z87_3002;
							}
						}
					else
						{	/* Eval/evaluate_avar.scm 182 */
							((bool_t) 1);
						}
				}
			}
			{	/* Eval/evaluate_avar.scm 183 */
				BgL_ev_exprz00_bglt BgL_arg1410z00_3005;
				obj_t BgL_arg1411z00_3006;

				BgL_arg1410z00_3005 =
					(((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt)
								((BgL_ev_letz00_bglt) BgL_ez00_2793))))->BgL_bodyz00);
				{	/* Eval/evaluate_avar.scm 183 */
					obj_t BgL_arg1412z00_3007;

					BgL_arg1412z00_3007 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt)
									((BgL_ev_letz00_bglt) BgL_ez00_2793))))->BgL_varsz00);
					BgL_arg1411z00_3006 =
						BGl_appendzd221011zd2zz__evaluate_avarz00(BgL_arg1412z00_3007,
						BgL_localz00_2794);
				}
				BGl_avarz00zz__evaluate_avarz00(BgL_arg1410z00_3005,
					BgL_arg1411z00_3006, BgL_absz00_2795);
			}
			{	/* Eval/evaluate_avar.scm 184 */
				obj_t BgL_arg1413z00_3008;

				BgL_arg1413z00_3008 =
					(((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt)
								((BgL_ev_letz00_bglt) BgL_ez00_2793))))->BgL_varsz00);
				BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(
					((BgL_ev_absz00_bglt) BgL_absz00_2795), BgL_arg1413z00_3008);
			}
			{
				obj_t BgL_auxz00_3527;

				{	/* Eval/evaluate_avar.scm 185 */
					obj_t BgL_hook1156z00_3009;

					BgL_hook1156z00_3009 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{	/* Eval/evaluate_avar.scm 185 */
						obj_t BgL_g1157z00_3010;

						BgL_g1157z00_3010 =
							(((BgL_ev_binderz00_bglt) COBJECT(
									((BgL_ev_binderz00_bglt)
										((BgL_ev_letz00_bglt) BgL_ez00_2793))))->BgL_varsz00);
						{
							obj_t BgL_l1153z00_3012;
							obj_t BgL_h1154z00_3013;

							BgL_l1153z00_3012 = BgL_g1157z00_3010;
							BgL_h1154z00_3013 = BgL_hook1156z00_3009;
						BgL_zc3z04anonymousza31414ze3z87_3011:
							if (NULLP(BgL_l1153z00_3012))
								{	/* Eval/evaluate_avar.scm 185 */
									BgL_auxz00_3527 = CDR(BgL_hook1156z00_3009);
								}
							else
								{	/* Eval/evaluate_avar.scm 185 */
									if (CBOOL(
											(((BgL_ev_varz00_bglt) COBJECT(
														((BgL_ev_varz00_bglt)
															CAR(((obj_t) BgL_l1153z00_3012)))))->BgL_effz00)))
										{	/* Eval/evaluate_avar.scm 185 */
											obj_t BgL_nh1155z00_3014;

											{	/* Eval/evaluate_avar.scm 185 */
												obj_t BgL_arg1418z00_3015;

												BgL_arg1418z00_3015 = CAR(((obj_t) BgL_l1153z00_3012));
												BgL_nh1155z00_3014 =
													MAKE_YOUNG_PAIR(BgL_arg1418z00_3015, BNIL);
											}
											SET_CDR(BgL_h1154z00_3013, BgL_nh1155z00_3014);
											{	/* Eval/evaluate_avar.scm 185 */
												obj_t BgL_arg1417z00_3016;

												BgL_arg1417z00_3016 = CDR(((obj_t) BgL_l1153z00_3012));
												{
													obj_t BgL_h1154z00_3549;
													obj_t BgL_l1153z00_3548;

													BgL_l1153z00_3548 = BgL_arg1417z00_3016;
													BgL_h1154z00_3549 = BgL_nh1155z00_3014;
													BgL_h1154z00_3013 = BgL_h1154z00_3549;
													BgL_l1153z00_3012 = BgL_l1153z00_3548;
													goto BgL_zc3z04anonymousza31414ze3z87_3011;
												}
											}
										}
									else
										{	/* Eval/evaluate_avar.scm 185 */
											obj_t BgL_arg1419z00_3017;

											BgL_arg1419z00_3017 = CDR(((obj_t) BgL_l1153z00_3012));
											{
												obj_t BgL_l1153z00_3552;

												BgL_l1153z00_3552 = BgL_arg1419z00_3017;
												BgL_l1153z00_3012 = BgL_l1153z00_3552;
												goto BgL_zc3z04anonymousza31414ze3z87_3011;
											}
										}
								}
						}
					}
				}
				return
					((((BgL_ev_letz00_bglt) COBJECT(
								((BgL_ev_letz00_bglt) BgL_ez00_2793)))->BgL_boxesz00) =
					((obj_t) BgL_auxz00_3527), BUNSPEC);
			}
		}

	}



/* &avar-ev_synchronize1239 */
	obj_t BGl_z62avarzd2ev_synchroniza7e1239z17zz__evaluate_avarz00(obj_t
		BgL_envz00_2796, obj_t BgL_ez00_2797, obj_t BgL_localz00_2798,
		obj_t BgL_absz00_2799)
	{
		{	/* Eval/evaluate_avar.scm 174 */
			BGl_avarz00zz__evaluate_avarz00(
				(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
							((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2797)))->BgL_mutexz00),
				BgL_localz00_2798, BgL_absz00_2799);
			BGl_avarz00zz__evaluate_avarz00((((BgL_ev_synchroniza7eza7_bglt)
						COBJECT(((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2797)))->
					BgL_prelockz00), BgL_localz00_2798, BgL_absz00_2799);
			return BGl_avarz00zz__evaluate_avarz00((((BgL_ev_synchroniza7eza7_bglt)
						COBJECT(((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2797)))->
					BgL_bodyz00), BgL_localz00_2798, BgL_absz00_2799);
		}

	}



/* &avar-ev_with-handler1237 */
	obj_t BGl_z62avarzd2ev_withzd2handler1237z62zz__evaluate_avarz00(obj_t
		BgL_envz00_2800, obj_t BgL_ez00_2801, obj_t BgL_localz00_2802,
		obj_t BgL_absz00_2803)
	{
		{	/* Eval/evaluate_avar.scm 169 */
			BGl_avarz00zz__evaluate_avarz00(
				(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
							((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_2801)))->
					BgL_handlerz00), BgL_localz00_2802, BgL_absz00_2803);
			return BGl_avarz00zz__evaluate_avarz00((((BgL_ev_withzd2handlerzd2_bglt)
						COBJECT(((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_2801)))->
					BgL_bodyz00), BgL_localz00_2802, BgL_absz00_2803);
		}

	}



/* &avar-ev_unwind-prote1235 */
	obj_t BGl_z62avarzd2ev_unwindzd2prote1235z62zz__evaluate_avarz00(obj_t
		BgL_envz00_2804, obj_t BgL_ez00_2805, obj_t BgL_localz00_2806,
		obj_t BgL_absz00_2807)
	{
		{	/* Eval/evaluate_avar.scm 164 */
			BGl_avarz00zz__evaluate_avarz00(
				(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
							((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_2805)))->BgL_ez00),
				BgL_localz00_2806, BgL_absz00_2807);
			return BGl_avarz00zz__evaluate_avarz00((((BgL_ev_unwindzd2protectzd2_bglt)
						COBJECT(((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_2805)))->
					BgL_bodyz00), BgL_localz00_2806, BgL_absz00_2807);
		}

	}



/* &avar-ev_bind-exit1233 */
	obj_t BGl_z62avarzd2ev_bindzd2exit1233z62zz__evaluate_avarz00(obj_t
		BgL_envz00_2808, obj_t BgL_ez00_2809, obj_t BgL_localz00_2810,
		obj_t BgL_absz00_2811)
	{
		{	/* Eval/evaluate_avar.scm 159 */
			{	/* Eval/evaluate_avar.scm 161 */
				BgL_ev_exprz00_bglt BgL_arg1393z00_3022;
				obj_t BgL_arg1394z00_3023;

				BgL_arg1393z00_3022 =
					(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
							((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_2809)))->BgL_bodyz00);
				{	/* Eval/evaluate_avar.scm 161 */
					BgL_ev_varz00_bglt BgL_arg1395z00_3024;

					BgL_arg1395z00_3024 =
						(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
								((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_2809)))->BgL_varz00);
					BgL_arg1394z00_3023 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_arg1395z00_3024), BgL_localz00_2810);
				}
				BGl_avarz00zz__evaluate_avarz00(BgL_arg1393z00_3022,
					BgL_arg1394z00_3023, BgL_absz00_2811);
			}
			{	/* Eval/evaluate_avar.scm 162 */
				obj_t BgL_arg1396z00_3025;

				{	/* Eval/evaluate_avar.scm 162 */
					BgL_ev_varz00_bglt BgL_arg1397z00_3026;

					BgL_arg1397z00_3026 =
						(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
								((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_2809)))->BgL_varz00);
					BgL_arg1396z00_3025 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_arg1397z00_3026), BNIL);
				}
				return
					BGl_bindzd2andzd2resetzd2effectzd2zz__evaluate_avarz00(
					((BgL_ev_absz00_bglt) BgL_absz00_2811), BgL_arg1396z00_3025);
			}
		}

	}



/* &avar-ev_setlocal1231 */
	obj_t BGl_z62avarzd2ev_setlocal1231zb0zz__evaluate_avarz00(obj_t
		BgL_envz00_2812, obj_t BgL_ez00_2813, obj_t BgL_localz00_2814,
		obj_t BgL_absz00_2815)
	{
		{	/* Eval/evaluate_avar.scm 152 */
			{	/* Eval/evaluate_avar.scm 154 */
				BgL_ev_varz00_bglt BgL_arg1391z00_3028;

				BgL_arg1391z00_3028 =
					(((BgL_ev_setlocalz00_bglt) COBJECT(
							((BgL_ev_setlocalz00_bglt) BgL_ez00_2813)))->BgL_vz00);
				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							((obj_t) BgL_arg1391z00_3028), BgL_localz00_2814)))
					{	/* Eval/evaluate_avar.scm 118 */
						BFALSE;
					}
				else
					{	/* Eval/evaluate_avar.scm 118 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
									((obj_t) BgL_arg1391z00_3028),
									(((BgL_ev_absz00_bglt) COBJECT(
												((BgL_ev_absz00_bglt) BgL_absz00_2815)))->
										BgL_freez00))))
							{	/* Eval/evaluate_avar.scm 120 */
								BFALSE;
							}
						else
							{
								obj_t BgL_auxz00_3600;

								{	/* Eval/evaluate_avar.scm 120 */
									obj_t BgL_arg1365z00_3029;

									BgL_arg1365z00_3029 =
										(((BgL_ev_absz00_bglt) COBJECT(
												((BgL_ev_absz00_bglt) BgL_absz00_2815)))->BgL_freez00);
									BgL_auxz00_3600 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg1391z00_3028), BgL_arg1365z00_3029);
								}
								((((BgL_ev_absz00_bglt) COBJECT(
												((BgL_ev_absz00_bglt) BgL_absz00_2815)))->BgL_freez00) =
									((obj_t) BgL_auxz00_3600), BUNSPEC);
							}
					}
			}
			((((BgL_ev_varz00_bglt) COBJECT(
							(((BgL_ev_setlocalz00_bglt) COBJECT(
										((BgL_ev_setlocalz00_bglt) BgL_ez00_2813)))->BgL_vz00)))->
					BgL_effz00) = ((obj_t) BTRUE), BUNSPEC);
			{	/* Eval/evaluate_avar.scm 157 */
				BgL_ev_exprz00_bglt BgL_arg1392z00_3030;

				BgL_arg1392z00_3030 =
					(((BgL_ev_hookz00_bglt) COBJECT(
							((BgL_ev_hookz00_bglt)
								((BgL_ev_setlocalz00_bglt) BgL_ez00_2813))))->BgL_ez00);
				return
					BGl_avarz00zz__evaluate_avarz00(BgL_arg1392z00_3030,
					BgL_localz00_2814, BgL_absz00_2815);
			}
		}

	}



/* &avar-ev_hook1229 */
	obj_t BGl_z62avarzd2ev_hook1229zb0zz__evaluate_avarz00(obj_t BgL_envz00_2816,
		obj_t BgL_ez00_2817, obj_t BgL_localz00_2818, obj_t BgL_absz00_2819)
	{
		{	/* Eval/evaluate_avar.scm 148 */
			return
				BGl_avarz00zz__evaluate_avarz00(
				(((BgL_ev_hookz00_bglt) COBJECT(
							((BgL_ev_hookz00_bglt) BgL_ez00_2817)))->BgL_ez00),
				BgL_localz00_2818, BgL_absz00_2819);
		}

	}



/* &avar-ev_prog21227 */
	obj_t BGl_z62avarzd2ev_prog21227zb0zz__evaluate_avarz00(obj_t BgL_envz00_2820,
		obj_t BgL_ez00_2821, obj_t BgL_localz00_2822, obj_t BgL_absz00_2823)
	{
		{	/* Eval/evaluate_avar.scm 143 */
			BGl_avarz00zz__evaluate_avarz00(
				(((BgL_ev_prog2z00_bglt) COBJECT(
							((BgL_ev_prog2z00_bglt) BgL_ez00_2821)))->BgL_e1z00),
				BgL_localz00_2822, BgL_absz00_2823);
			return BGl_avarz00zz__evaluate_avarz00((((BgL_ev_prog2z00_bglt)
						COBJECT(((BgL_ev_prog2z00_bglt) BgL_ez00_2821)))->BgL_e2z00),
				BgL_localz00_2822, BgL_absz00_2823);
		}

	}



/* &avar-ev_list1225 */
	obj_t BGl_z62avarzd2ev_list1225zb0zz__evaluate_avarz00(obj_t BgL_envz00_2824,
		obj_t BgL_ez00_2825, obj_t BgL_localz00_2826, obj_t BgL_absz00_2827)
	{
		{	/* Eval/evaluate_avar.scm 139 */
			{	/* Eval/evaluate_avar.scm 140 */
				bool_t BgL_tmpz00_3623;

				{	/* Eval/evaluate_avar.scm 141 */
					obj_t BgL_g1148z00_3034;

					BgL_g1148z00_3034 =
						(((BgL_ev_listz00_bglt) COBJECT(
								((BgL_ev_listz00_bglt) BgL_ez00_2825)))->BgL_argsz00);
					{
						obj_t BgL_l1146z00_3036;

						BgL_l1146z00_3036 = BgL_g1148z00_3034;
					BgL_zc3z04anonymousza31383ze3z87_3035:
						if (PAIRP(BgL_l1146z00_3036))
							{	/* Eval/evaluate_avar.scm 141 */
								{	/* Eval/evaluate_avar.scm 141 */
									obj_t BgL_ez00_3037;

									BgL_ez00_3037 = CAR(BgL_l1146z00_3036);
									BGl_avarz00zz__evaluate_avarz00(
										((BgL_ev_exprz00_bglt) BgL_ez00_3037), BgL_localz00_2826,
										BgL_absz00_2827);
								}
								{
									obj_t BgL_l1146z00_3631;

									BgL_l1146z00_3631 = CDR(BgL_l1146z00_3036);
									BgL_l1146z00_3036 = BgL_l1146z00_3631;
									goto BgL_zc3z04anonymousza31383ze3z87_3035;
								}
							}
						else
							{	/* Eval/evaluate_avar.scm 141 */
								BgL_tmpz00_3623 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3623);
			}
		}

	}



/* &avar-ev_if1223 */
	obj_t BGl_z62avarzd2ev_if1223zb0zz__evaluate_avarz00(obj_t BgL_envz00_2828,
		obj_t BgL_ez00_2829, obj_t BgL_localz00_2830, obj_t BgL_absz00_2831)
	{
		{	/* Eval/evaluate_avar.scm 133 */
			BGl_avarz00zz__evaluate_avarz00(
				(((BgL_ev_ifz00_bglt) COBJECT(
							((BgL_ev_ifz00_bglt) BgL_ez00_2829)))->BgL_pz00),
				BgL_localz00_2830, BgL_absz00_2831);
			BGl_avarz00zz__evaluate_avarz00((((BgL_ev_ifz00_bglt)
						COBJECT(((BgL_ev_ifz00_bglt) BgL_ez00_2829)))->BgL_tz00),
				BgL_localz00_2830, BgL_absz00_2831);
			return BGl_avarz00zz__evaluate_avarz00((((BgL_ev_ifz00_bglt)
						COBJECT(((BgL_ev_ifz00_bglt) BgL_ez00_2829)))->BgL_ez00),
				BgL_localz00_2830, BgL_absz00_2831);
		}

	}



/* &avar-ev_litt1221 */
	obj_t BGl_z62avarzd2ev_litt1221zb0zz__evaluate_avarz00(obj_t BgL_envz00_2832,
		obj_t BgL_ez00_2833, obj_t BgL_localz00_2834, obj_t BgL_absz00_2835)
	{
		{	/* Eval/evaluate_avar.scm 130 */
			return BUNSPEC;
		}

	}



/* &avar-ev_global1219 */
	obj_t BGl_z62avarzd2ev_global1219zb0zz__evaluate_avarz00(obj_t
		BgL_envz00_2836, obj_t BgL_varz00_2837, obj_t BgL_localz00_2838,
		obj_t BgL_absz00_2839)
	{
		{	/* Eval/evaluate_avar.scm 127 */
			return BUNSPEC;
		}

	}



/* &avar-ev_var1217 */
	obj_t BGl_z62avarzd2ev_var1217zb0zz__evaluate_avarz00(obj_t BgL_envz00_2840,
		obj_t BgL_varz00_2841, obj_t BgL_localz00_2842, obj_t BgL_absz00_2843)
	{
		{	/* Eval/evaluate_avar.scm 124 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t)
							((BgL_ev_varz00_bglt) BgL_varz00_2841)), BgL_localz00_2842)))
				{	/* Eval/evaluate_avar.scm 118 */
					return BFALSE;
				}
			else
				{	/* Eval/evaluate_avar.scm 118 */
					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
								((obj_t)
									((BgL_ev_varz00_bglt) BgL_varz00_2841)),
								(((BgL_ev_absz00_bglt) COBJECT(
											((BgL_ev_absz00_bglt) BgL_absz00_2843)))->BgL_freez00))))
						{	/* Eval/evaluate_avar.scm 120 */
							return BFALSE;
						}
					else
						{
							obj_t BgL_auxz00_3655;

							{	/* Eval/evaluate_avar.scm 120 */
								obj_t BgL_arg1365z00_3042;

								BgL_arg1365z00_3042 =
									(((BgL_ev_absz00_bglt) COBJECT(
											((BgL_ev_absz00_bglt) BgL_absz00_2843)))->BgL_freez00);
								BgL_auxz00_3655 =
									MAKE_YOUNG_PAIR(
									((obj_t)
										((BgL_ev_varz00_bglt) BgL_varz00_2841)),
									BgL_arg1365z00_3042);
							}
							return
								((((BgL_ev_absz00_bglt) COBJECT(
											((BgL_ev_absz00_bglt) BgL_absz00_2843)))->BgL_freez00) =
								((obj_t) BgL_auxz00_3655), BUNSPEC);
						}
				}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_avarz00(void)
	{
		{	/* Eval/evaluate_avar.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__ppz00(233942021L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
			return BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(0L,
				BSTRING_TO_STRING(BGl_string1911z00zz__evaluate_avarz00));
		}

	}

#ifdef __cplusplus
}
#endif
