/*===========================================================================*/
/*   (Eval/evobject.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evobject.scm -indent -o objs/obj_u/Eval/evobject.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVOBJECT_TYPE_DEFINITIONS
#define BGL___EVOBJECT_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___EVOBJECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_evalzd2instantiatezd2expanderz00zz__evobjectz00(obj_t);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_symbol2480z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2482z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2484z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2488z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_zc3z04anonymousza32001ze3ze70z60zz__evobjectz00(obj_t,
		obj_t);
	static obj_t BGl_list2458z00zz__evobjectz00 = BUNSPEC;
	extern obj_t bgl_append2(obj_t, obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2490z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_makezd2classzd2fieldsz00zz__evobjectz00(obj_t, obj_t, obj_t,
		long, obj_t);
	static obj_t BGl_symbol2492z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2494z00zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	static obj_t BGl_list2465z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2499z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_list2468z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	static long BGl_getzd2classzd2hashz00zz__evobjectz00(obj_t);
	static obj_t BGl_expandzd2errorzd2zz__evobjectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31839ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_makezd2classzd2virtualzd2fieldszd2zz__evobjectz00(obj_t);
	extern obj_t BGl_z52objectzd2wideningz80zz__objectz00(BgL_objectz00_bglt);
	static obj_t BGl_evalzd2parsezd2classzd2slotzd2zz__evobjectz00(obj_t, obj_t);
	extern obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31703ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_classzd2evdatazd2setz12z12zz__objectz00(obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32007ze3ze70z60zz__evobjectz00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evobjectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_definez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t, obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	extern bool_t BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evalzd2expandzd2instantiatez00zz__evobjectz00(obj_t);
	extern obj_t BGl_evalz12z12zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62evalzd2expandzd2instantiatez62zz__evobjectz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31461ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31679ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	extern obj_t BGl_classzd2fieldszd2zz__objectz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__evobjectz00(void);
	static obj_t BGl_vectorzd2filterzd2mapz00zz__evobjectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31527ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t
		BGl_z62evalzd2cozd2instantiatezd2expanderzb0zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t create_struct(obj_t, int);
	extern obj_t BGl_z52withzd2lexicalz80zz__expandz00(obj_t, obj_t, obj_t,
		obj_t);
	static bool_t BGl_patchzd2fieldzd2defaultzd2valuesz12zc0zz__evobjectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__evobjectz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__evobjectz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	extern obj_t BGl_classzd2existszd2zz__objectz00(obj_t);
	static obj_t BGl_vector2457z00zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_z52lexicalzd2stackz80zz__expandz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evobjectz00(void);
	static bool_t BGl_patchzd2vfieldzd2accessorsz12z12zz__evobjectz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__evobjectz00(void);
	extern obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t, obj_t);
	extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__evobjectz00(obj_t, obj_t);
	static obj_t BGl_evalzd2parsezd2classz00zz__evobjectz00(obj_t, obj_t);
	static obj_t BGl_classgenzd2slotzd2anonymousz00zz__evobjectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__evobjectz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_evalzd2cozd2instantiatezd2expanderzd2zz__evobjectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evalzd2expandzd2duplicatez00zz__evobjectz00(obj_t);
	extern bool_t BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00(obj_t);
	static obj_t BGl_duplicatezd2expanderzd2zz__evobjectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_findzd2classzd2zz__objectz00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31392ze3ze5zz__evobjectz00(obj_t);
	static obj_t BGl_evalzd2withzd2accesszd2expanderzd2zz__evobjectz00(obj_t);
	extern obj_t BGl_objectz00zz__objectz00;
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_zc3z04anonymousza31981ze3ze70z60zz__evobjectz00(obj_t,
		obj_t);
	static obj_t BGl_withzd2accesszd2expanderz00zz__evobjectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31644ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31377ze3ze5zz__evobjectz00(obj_t, obj_t);
	extern bool_t BGl_classzd2abstractzf3z21zz__objectz00(obj_t);
	extern obj_t
		BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(BgL_objectz00_bglt,
		obj_t);
	extern bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t);
	extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	extern obj_t BGl_getzd2sourcezd2locationz00zz__readerz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31580ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31556ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_classzd2evdatazd2zz__objectz00(obj_t);
	static obj_t BGl_evalzd2registerzd2classz00zz__evobjectz00(obj_t, obj_t,
		obj_t, bool_t, obj_t, long, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2502z00zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t);
	static obj_t BGl_symbol2505z00zz__evobjectz00 = BUNSPEC;
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_symbol2507z00zz__evobjectz00 = BUNSPEC;
	extern obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_evmodulezd2namezd2zz__evmodulez00(obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__evobjectz00(obj_t, obj_t);
	extern bool_t BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(obj_t);
	extern obj_t BGl_evcompilezd2errorzd2zz__evcompilez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2512z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_decomposezd2identzd2zz__evobjectz00(obj_t);
	static obj_t BGl_symbol2514z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__evobjectz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62evalzd2expandzd2duplicatez62zz__evobjectz00(obj_t, obj_t);
	static obj_t BGl_symbol2516z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_evalzd2creatorzd2zz__evobjectz00(obj_t, obj_t, long, obj_t);
	static obj_t BGl_symbol2518z00zz__evobjectz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_evalzd2classzd2zz__evobjectz00(obj_t, bool_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_classzd2evfieldszd2setz12z12zz__objectz00(obj_t, obj_t);
	static obj_t BGl_localiza7eza7zz__evobjectz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31485ze3ze5zz__evobjectz00(obj_t, obj_t);
	extern obj_t BGl_classzd2fieldzd2namez00zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31396ze3ze5zz__evobjectz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_evalzd2expandzd2withzd2accesszd2zz__evobjectz00(obj_t);
	static obj_t BGl_symbol2520z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2524z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_z62evalzd2expandzd2withzd2accesszb0zz__evobjectz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2526z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2528z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__evobjectz00(obj_t, obj_t);
	static long BGl_loopze71ze7zz__evobjectz00(obj_t, long);
	extern obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_classzd2creatorzd2zz__objectz00(obj_t);
	static obj_t BGl_evalzd2duplicatezd2expanderz00zz__evobjectz00(obj_t);
	static obj_t BGl_instantiatezd2fillzd2zz__evobjectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_evalzd2beginzd2expanderz00zz__expander_definez00(obj_t);
	static obj_t BGl_symbol2530z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2534z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2453z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2455z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2537z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2539z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2459z00zz__evobjectz00 = BUNSPEC;
	extern bool_t BGl_evalzd2classzf3z21zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31512ze3ze5zz__evobjectz00(obj_t, obj_t);
	static obj_t BGl_z62evalzd2classzb0zz__evobjectz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31754ze3ze5zz__evobjectz00(obj_t, obj_t,
		obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_symbol2461z00zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_classzd2allocatorzd2zz__objectz00(obj_t);
	static obj_t BGl_symbol2463z00zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t);
	static obj_t BGl_symbol2466z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2548z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2469z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_cozd2instantiatezd2ze3letze3zz__evobjectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t);
	static obj_t BGl_symbol2551z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_symbol2471z00zz__evobjectz00 = BUNSPEC;
	static obj_t BGl_findzd2fieldzd2offsetz00zz__evobjectz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t BGl_symbol2475z00zz__evobjectz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2cozd2instantiatezd2expanderzd2envz00zz__evobjectz00,
		BgL_bgl_za762evalza7d2coza7d2i2558za7,
		BGl_z62evalzd2cozd2instantiatezd2expanderzb0zz__evobjectz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2500z00zz__evobjectz00,
		BgL_bgl_string2500za700za7za7_2559za7, "duplicated", 10);
	      DEFINE_STRING(BGl_string2501z00zz__evobjectz00,
		BgL_bgl_string2501za700za7za7_2560za7, "Illegal ~a, field unknown \"~a\"",
		30);
	      DEFINE_STRING(BGl_string2503z00zz__evobjectz00,
		BgL_bgl_string2503za700za7za7_2561za7, "with-access::", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2classzd2envz00zz__evobjectz00,
		BgL_bgl_za762evalza7d2classza72562za7,
		BGl_z62evalzd2classzb0zz__evobjectz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2504z00zz__evobjectz00,
		BgL_bgl_string2504za700za7za7_2563za7, "&eval-expand-with-access", 24);
	      DEFINE_STRING(BGl_string2506z00zz__evobjectz00,
		BgL_bgl_string2506za700za7za7_2564za7, "i", 1);
	      DEFINE_STRING(BGl_string2508z00zz__evobjectz00,
		BgL_bgl_string2508za700za7za7_2565za7, "::", 2);
	      DEFINE_STRING(BGl_string2509z00zz__evobjectz00,
		BgL_bgl_string2509za700za7za7_2566za7, "Illegal field", 13);
	      DEFINE_STRING(BGl_string2510z00zz__evobjectz00,
		BgL_bgl_string2510za700za7za7_2567za7, "with-access", 11);
	      DEFINE_STRING(BGl_string2511z00zz__evobjectz00,
		BgL_bgl_string2511za700za7za7_2568za7, "Illegal with-access", 19);
	      DEFINE_STRING(BGl_string2513z00zz__evobjectz00,
		BgL_bgl_string2513za700za7za7_2569za7, "->", 2);
	      DEFINE_STRING(BGl_string2515z00zz__evobjectz00,
		BgL_bgl_string2515za700za7za7_2570za7, "set!", 4);
	      DEFINE_STRING(BGl_string2517z00zz__evobjectz00,
		BgL_bgl_string2517za700za7za7_2571za7, "obj", 3);
	      DEFINE_STRING(BGl_string2519z00zz__evobjectz00,
		BgL_bgl_string2519za700za7za7_2572za7, "slot", 4);
	      DEFINE_STRING(BGl_string2521z00zz__evobjectz00,
		BgL_bgl_string2521za700za7za7_2573za7, "read-only", 9);
	      DEFINE_STRING(BGl_string2522z00zz__evobjectz00,
		BgL_bgl_string2522za700za7za7_2574za7, "eval", 4);
	      DEFINE_STRING(BGl_string2523z00zz__evobjectz00,
		BgL_bgl_string2523za700za7za7_2575za7, "Illegal slot declaration", 24);
	      DEFINE_STRING(BGl_string2525z00zz__evobjectz00,
		BgL_bgl_string2525za700za7za7_2576za7, "info", 4);
	      DEFINE_STRING(BGl_string2527z00zz__evobjectz00,
		BgL_bgl_string2527za700za7za7_2577za7, "get", 3);
	      DEFINE_STRING(BGl_string2529z00zz__evobjectz00,
		BgL_bgl_string2529za700za7za7_2578za7, "set", 3);
	      DEFINE_STRING(BGl_string2531z00zz__evobjectz00,
		BgL_bgl_string2531za700za7za7_2579za7, "default", 7);
	      DEFINE_STRING(BGl_string2532z00zz__evobjectz00,
		BgL_bgl_string2532za700za7za7_2580za7, "Missing virtual set", 19);
	      DEFINE_STRING(BGl_string2533z00zz__evobjectz00,
		BgL_bgl_string2533za700za7za7_2581za7, "Missing virtual get", 19);
	      DEFINE_STRING(BGl_string2452z00zz__evobjectz00,
		BgL_bgl_string2452za700za7za7_2582za7,
		"Wrong number of arguments for constructor (expecting ~a)", 56);
	      DEFINE_STRING(BGl_string2535z00zz__evobjectz00,
		BgL_bgl_string2535za700za7za7_2583za7, "o", 1);
	      DEFINE_STRING(BGl_string2454z00zz__evobjectz00,
		BgL_bgl_string2454za700za7za7_2584za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2536z00zz__evobjectz00,
		BgL_bgl_string2536za700za7za7_2585za7, "Illegal class declaration", 25);
	      DEFINE_STRING(BGl_string2456z00zz__evobjectz00,
		BgL_bgl_string2456za700za7za7_2586za7, "", 0);
	      DEFINE_STRING(BGl_string2538z00zz__evobjectz00,
		BgL_bgl_string2538za700za7za7_2587za7, "object", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2expandzd2duplicatezd2envzd2zz__evobjectz00,
		BgL_bgl_za762evalza7d2expand2588z00,
		BGl_z62evalzd2expandzd2duplicatez62zz__evobjectz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2expandzd2instantiatezd2envzd2zz__evobjectz00,
		BgL_bgl_za762evalza7d2expand2589z00,
		BGl_z62evalzd2expandzd2instantiatez62zz__evobjectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2540z00zz__evobjectz00,
		BgL_bgl_string2540za700za7za7_2590za7, "define", 6);
	      DEFINE_STRING(BGl_string2541z00zz__evobjectz00,
		BgL_bgl_string2541za700za7za7_2591za7, "Cannot find super class", 23);
	      DEFINE_STRING(BGl_string2460z00zz__evobjectz00,
		BgL_bgl_string2460za700za7za7_2592za7, "patch-field-default-values!", 27);
	      DEFINE_STRING(BGl_string2542z00zz__evobjectz00,
		BgL_bgl_string2542za700za7za7_2593za7, "&eval-class", 11);
	      DEFINE_STRING(BGl_string2543z00zz__evobjectz00,
		BgL_bgl_string2543za700za7za7_2594za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2462z00zz__evobjectz00,
		BgL_bgl_string2462za700za7za7_2595za7, "slots", 5);
	      DEFINE_STRING(BGl_string2544z00zz__evobjectz00,
		BgL_bgl_string2544za700za7za7_2596za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2545z00zz__evobjectz00,
		BgL_bgl_string2545za700za7za7_2597za7, "pair", 4);
	      DEFINE_STRING(BGl_string2464z00zz__evobjectz00,
		BgL_bgl_string2464za700za7za7_2598za7, "fields", 6);
	      DEFINE_STRING(BGl_string2546z00zz__evobjectz00,
		BgL_bgl_string2546za700za7za7_2599za7, "co-instantiate", 14);
	      DEFINE_STRING(BGl_string2547z00zz__evobjectz00,
		BgL_bgl_string2547za700za7za7_2600za7, "Illegal `co-instantiate' form", 29);
	      DEFINE_STRING(BGl_string2467z00zz__evobjectz00,
		BgL_bgl_string2467za700za7za7_2601za7, "patch-vfield-accessors!", 23);
	      DEFINE_STRING(BGl_string2549z00zz__evobjectz00,
		BgL_bgl_string2549za700za7za7_2602za7, "&eval-co-instantiate-expander", 29);
	      DEFINE_STRING(BGl_string2550z00zz__evobjectz00,
		BgL_bgl_string2550za700za7za7_2603za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2470z00zz__evobjectz00,
		BgL_bgl_string2470za700za7za7_2604za7, "class-virtual", 13);
	      DEFINE_STRING(BGl_string2552z00zz__evobjectz00,
		BgL_bgl_string2552za700za7za7_2605za7, "instantiate", 11);
	      DEFINE_STRING(BGl_string2553z00zz__evobjectz00,
		BgL_bgl_string2553za700za7za7_2606za7,
		"Abstract classes can't be instantiated", 38);
	      DEFINE_STRING(BGl_string2472z00zz__evobjectz00,
		BgL_bgl_string2472za700za7za7_2607za7, "clazz", 5);
	      DEFINE_STRING(BGl_string2554z00zz__evobjectz00,
		BgL_bgl_string2554za700za7za7_2608za7, "Illegal class", 13);
	      DEFINE_STRING(BGl_string2555z00zz__evobjectz00,
		BgL_bgl_string2555za700za7za7_2609za7, "Illegal binding", 15);
	      DEFINE_STRING(BGl_string2556z00zz__evobjectz00,
		BgL_bgl_string2556za700za7za7_2610za7, "Illegal variable type", 21);
	      DEFINE_STRING(BGl_string2557z00zz__evobjectz00,
		BgL_bgl_string2557za700za7za7_2611za7, "__evobject", 10);
	      DEFINE_STRING(BGl_string2476z00zz__evobjectz00,
		BgL_bgl_string2476za700za7za7_2612za7, "instantiate::", 13);
	      DEFINE_STRING(BGl_string2477z00zz__evobjectz00,
		BgL_bgl_string2477za700za7za7_2613za7,
		"/tmp/bigloo/runtime/Eval/evobject.scm", 37);
	      DEFINE_STRING(BGl_string2478z00zz__evobjectz00,
		BgL_bgl_string2478za700za7za7_2614za7, "&eval-expand-instantiate", 24);
	      DEFINE_STRING(BGl_string2479z00zz__evobjectz00,
		BgL_bgl_string2479za700za7za7_2615za7, "class", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2473z00zz__evobjectz00,
		BgL_bgl_za762za7c3za704anonymo2616za7,
		BGl_z62zc3z04anonymousza31485ze3ze5zz__evobjectz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2474z00zz__evobjectz00,
		BgL_bgl_za762za7c3za704anonymo2617za7,
		BGl_z62zc3z04anonymousza31512ze3ze5zz__evobjectz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2481z00zz__evobjectz00,
		BgL_bgl_string2481za700za7za7_2618za7, "__object", 8);
	      DEFINE_STRING(BGl_string2483z00zz__evobjectz00,
		BgL_bgl_string2483za700za7za7_2619za7, "class-field-default-value", 25);
	      DEFINE_STRING(BGl_string2485z00zz__evobjectz00,
		BgL_bgl_string2485za700za7za7_2620za7, "@", 1);
	      DEFINE_STRING(BGl_string2486z00zz__evobjectz00,
		BgL_bgl_string2486za700za7za7_2621za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string2487z00zz__evobjectz00,
		BgL_bgl_string2487za700za7za7_2622za7, "Missing value for field \"~a\"",
		28);
	      DEFINE_STRING(BGl_string2489z00zz__evobjectz00,
		BgL_bgl_string2489za700za7za7_2623za7, "new", 3);
	      DEFINE_STRING(BGl_string2491z00zz__evobjectz00,
		BgL_bgl_string2491za700za7za7_2624za7, "begin", 5);
	      DEFINE_STRING(BGl_string2493z00zz__evobjectz00,
		BgL_bgl_string2493za700za7za7_2625za7, "let", 3);
	      DEFINE_STRING(BGl_string2495z00zz__evobjectz00,
		BgL_bgl_string2495za700za7za7_2626za7, "duplicate::", 11);
	      DEFINE_STRING(BGl_string2496z00zz__evobjectz00,
		BgL_bgl_string2496za700za7za7_2627za7, "&eval-expand-duplicate", 22);
	      DEFINE_STRING(BGl_string2497z00zz__evobjectz00,
		BgL_bgl_string2497za700za7za7_2628za7, "duplicate", 9);
	      DEFINE_STRING(BGl_string2498z00zz__evobjectz00,
		BgL_bgl_string2498za700za7za7_2629za7, "Illegal form", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2expandzd2withzd2accesszd2envz00zz__evobjectz00,
		BgL_bgl_za762evalza7d2expand2630z00,
		BGl_z62evalzd2expandzd2withzd2accesszb0zz__evobjectz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2480z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2482z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2484z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2488z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_list2458z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2490z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2492z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2494z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_list2465z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2499z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_list2468z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_vector2457z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2502z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2505z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2507z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2512z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2514z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2516z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2518z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2520z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2524z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2526z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2528z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2530z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2534z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2453z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2455z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2537z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2539z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2459z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2461z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2463z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2466z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2548z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2469z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2551z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2471z00zz__evobjectz00));
		     ADD_ROOT((void *) (&BGl_symbol2475z00zz__evobjectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evobjectz00(long
		BgL_checksumz00_4668, char *BgL_fromz00_4669)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evobjectz00))
				{
					BGl_requirezd2initializa7ationz75zz__evobjectz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evobjectz00();
					BGl_cnstzd2initzd2zz__evobjectz00();
					BGl_importedzd2moduleszd2initz00zz__evobjectz00();
					return BGl_toplevelzd2initzd2zz__evobjectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			BGl_symbol2453z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2454z00zz__evobjectz00);
			BGl_symbol2455z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2456z00zz__evobjectz00);
			BGl_vector2457z00zz__evobjectz00 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BNIL);
			BGl_symbol2459z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2460z00zz__evobjectz00);
			BGl_symbol2461z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2462z00zz__evobjectz00);
			BGl_symbol2463z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2464z00zz__evobjectz00);
			BGl_list2458z00zz__evobjectz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2459z00zz__evobjectz00,
				MAKE_YOUNG_PAIR(BGl_symbol2461z00zz__evobjectz00,
					MAKE_YOUNG_PAIR(BGl_symbol2463z00zz__evobjectz00, BNIL)));
			BGl_symbol2466z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2467z00zz__evobjectz00);
			BGl_symbol2469z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2470z00zz__evobjectz00);
			BGl_symbol2471z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2472z00zz__evobjectz00);
			BGl_list2468z00zz__evobjectz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2469z00zz__evobjectz00,
				MAKE_YOUNG_PAIR(BGl_symbol2471z00zz__evobjectz00, BNIL));
			BGl_list2465z00zz__evobjectz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2466z00zz__evobjectz00,
				MAKE_YOUNG_PAIR(BGl_symbol2461z00zz__evobjectz00,
					MAKE_YOUNG_PAIR(BGl_symbol2463z00zz__evobjectz00,
						MAKE_YOUNG_PAIR(BGl_list2468z00zz__evobjectz00, BNIL))));
			BGl_symbol2475z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2476z00zz__evobjectz00);
			BGl_symbol2480z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2481z00zz__evobjectz00);
			BGl_symbol2482z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2483z00zz__evobjectz00);
			BGl_symbol2484z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2485z00zz__evobjectz00);
			BGl_symbol2488z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2489z00zz__evobjectz00);
			BGl_symbol2490z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2491z00zz__evobjectz00);
			BGl_symbol2492z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2493z00zz__evobjectz00);
			BGl_symbol2494z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2495z00zz__evobjectz00);
			BGl_symbol2499z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2500z00zz__evobjectz00);
			BGl_symbol2502z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2503z00zz__evobjectz00);
			BGl_symbol2505z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2506z00zz__evobjectz00);
			BGl_symbol2507z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2508z00zz__evobjectz00);
			BGl_symbol2512z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2513z00zz__evobjectz00);
			BGl_symbol2514z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2515z00zz__evobjectz00);
			BGl_symbol2516z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2517z00zz__evobjectz00);
			BGl_symbol2518z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2519z00zz__evobjectz00);
			BGl_symbol2520z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2521z00zz__evobjectz00);
			BGl_symbol2524z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2525z00zz__evobjectz00);
			BGl_symbol2526z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2527z00zz__evobjectz00);
			BGl_symbol2528z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2529z00zz__evobjectz00);
			BGl_symbol2530z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2531z00zz__evobjectz00);
			BGl_symbol2534z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2535z00zz__evobjectz00);
			BGl_symbol2537z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2538z00zz__evobjectz00);
			BGl_symbol2539z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2540z00zz__evobjectz00);
			BGl_symbol2548z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2546z00zz__evobjectz00);
			return (BGl_symbol2551z00zz__evobjectz00 =
				bstring_to_symbol(BGl_string2552z00zz__evobjectz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__evobjectz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1246;

				BgL_headz00_1246 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2860;
					obj_t BgL_tailz00_2861;

					BgL_prevz00_2860 = BgL_headz00_1246;
					BgL_tailz00_2861 = BgL_l1z00_1;
				BgL_loopz00_2859:
					if (PAIRP(BgL_tailz00_2861))
						{
							obj_t BgL_newzd2prevzd2_2867;

							BgL_newzd2prevzd2_2867 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2861), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2860, BgL_newzd2prevzd2_2867);
							{
								obj_t BgL_tailz00_4729;
								obj_t BgL_prevz00_4728;

								BgL_prevz00_4728 = BgL_newzd2prevzd2_2867;
								BgL_tailz00_4729 = CDR(BgL_tailz00_2861);
								BgL_tailz00_2861 = BgL_tailz00_4729;
								BgL_prevz00_2860 = BgL_prevz00_4728;
								goto BgL_loopz00_2859;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1246);
			}
		}

	}



/* get-class-hash */
	long BGl_getzd2classzd2hashz00zz__evobjectz00(obj_t BgL_defz00_3)
	{
		{	/* Llib/object.sch 19 */
			return BGl_loopze71ze7zz__evobjectz00(BgL_defz00_3, 1705L);
		}

	}



/* loop~1 */
	long BGl_loopze71ze7zz__evobjectz00(obj_t BgL_defz00_1256,
		long BgL_hashz00_1257)
	{
		{	/* Llib/object.sch 24 */
		BGl_loopze71ze7zz__evobjectz00:
			if (NULLP(BgL_defz00_1256))
				{	/* Llib/object.sch 27 */
					return BgL_hashz00_1257;
				}
			else
				{	/* Llib/object.sch 27 */
					if (PAIRP(BgL_defz00_1256))
						{
							long BgL_hashz00_4739;
							obj_t BgL_defz00_4737;

							BgL_defz00_4737 = CDR(BgL_defz00_1256);
							BgL_hashz00_4739 =
								BGl_loopze71ze7zz__evobjectz00(CAR(BgL_defz00_1256),
								(1966L ^ BgL_hashz00_1257));
							BgL_hashz00_1257 = BgL_hashz00_4739;
							BgL_defz00_1256 = BgL_defz00_4737;
							goto BGl_loopze71ze7zz__evobjectz00;
						}
					else
						{	/* Llib/object.sch 29 */
							return
								(
								(BGl_getzd2hashnumberzd2persistentz00zz__hashz00
									(BgL_defz00_1256) & 65535L) ^ BgL_hashz00_1257);
						}
				}
		}

	}



/* expand-error */
	obj_t BGl_expandzd2errorzd2zz__evobjectz00(obj_t BgL_pz00_4, obj_t BgL_mz00_5,
		obj_t BgL_xz00_6)
	{
		{	/* Eval/evobject.scm 71 */
			{	/* Eval/evobject.scm 72 */
				obj_t BgL_locz00_1271;

				if (EPAIRP(BgL_xz00_6))
					{	/* Eval/evobject.scm 72 */
						BgL_locz00_1271 = CER(((obj_t) BgL_xz00_6));
					}
				else
					{	/* Eval/evobject.scm 72 */
						BgL_locz00_1271 = BFALSE;
					}
				{	/* Eval/evobject.scm 73 */
					bool_t BgL_test2636z00_4750;

					if (PAIRP(BgL_locz00_1271))
						{	/* Eval/evobject.scm 73 */
							bool_t BgL_test2638z00_4753;

							{	/* Eval/evobject.scm 73 */
								obj_t BgL_tmpz00_4754;

								BgL_tmpz00_4754 = CDR(BgL_locz00_1271);
								BgL_test2638z00_4753 = PAIRP(BgL_tmpz00_4754);
							}
							if (BgL_test2638z00_4753)
								{	/* Eval/evobject.scm 73 */
									obj_t BgL_tmpz00_4757;

									BgL_tmpz00_4757 = CDR(CDR(BgL_locz00_1271));
									BgL_test2636z00_4750 = PAIRP(BgL_tmpz00_4757);
								}
							else
								{	/* Eval/evobject.scm 73 */
									BgL_test2636z00_4750 = ((bool_t) 0);
								}
						}
					else
						{	/* Eval/evobject.scm 73 */
							BgL_test2636z00_4750 = ((bool_t) 0);
						}
					if (BgL_test2636z00_4750)
						{	/* Eval/evobject.scm 73 */
							return
								BGl_errorzf2locationzf2zz__errorz00(BgL_pz00_4, BgL_mz00_5,
								BgL_xz00_6, CAR(CDR(BgL_locz00_1271)),
								CAR(CDR(CDR(BgL_locz00_1271))));
						}
					else
						{	/* Eval/evobject.scm 73 */
							return
								BGl_errorz00zz__errorz00(BgL_pz00_4, BgL_mz00_5, BgL_xz00_6);
						}
				}
			}
		}

	}



/* decompose-ident */
	obj_t BGl_decomposezd2identzd2zz__evobjectz00(obj_t BgL_idz00_42)
	{
		{	/* Eval/evobject.scm 92 */
			{	/* Eval/evobject.scm 93 */
				obj_t BgL_stringz00_1302;

				BgL_stringz00_1302 = SYMBOL_TO_STRING(BgL_idz00_42);
				{	/* Eval/evobject.scm 93 */
					long BgL_lenz00_1303;

					BgL_lenz00_1303 = STRING_LENGTH(BgL_stringz00_1302);
					{	/* Eval/evobject.scm 94 */

						{
							long BgL_walkerz00_1305;

							BgL_walkerz00_1305 = 0L;
						BgL_zc3z04anonymousza31335ze3z87_1306:
							if ((BgL_walkerz00_1305 == BgL_lenz00_1303))
								{	/* Eval/evobject.scm 97 */
									{	/* Eval/evobject.scm 98 */
										int BgL_tmpz00_4772;

										BgL_tmpz00_4772 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4772);
									}
									{	/* Eval/evobject.scm 98 */
										int BgL_tmpz00_4775;

										BgL_tmpz00_4775 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_4775, BFALSE);
									}
									return BgL_idz00_42;
								}
							else
								{	/* Eval/evobject.scm 99 */
									bool_t BgL_test2640z00_4778;

									if (
										(STRING_REF(BgL_stringz00_1302,
												BgL_walkerz00_1305) == ((unsigned char) ':')))
										{	/* Eval/evobject.scm 99 */
											if ((BgL_walkerz00_1305 < (BgL_lenz00_1303 - 1L)))
												{	/* Eval/evobject.scm 100 */
													BgL_test2640z00_4778 =
														(STRING_REF(BgL_stringz00_1302,
															(BgL_walkerz00_1305 + 1L)) ==
														((unsigned char) ':'));
												}
											else
												{	/* Eval/evobject.scm 100 */
													BgL_test2640z00_4778 = ((bool_t) 0);
												}
										}
									else
										{	/* Eval/evobject.scm 99 */
											BgL_test2640z00_4778 = ((bool_t) 0);
										}
									if (BgL_test2640z00_4778)
										{	/* Eval/evobject.scm 102 */
											obj_t BgL_val0_1097z00_1319;
											obj_t BgL_val1_1098z00_1320;

											BgL_val0_1097z00_1319 =
												bstring_to_symbol(c_substring(BgL_stringz00_1302, 0L,
													BgL_walkerz00_1305));
											BgL_val1_1098z00_1320 =
												bstring_to_symbol(c_substring(BgL_stringz00_1302,
													(BgL_walkerz00_1305 + 2L), BgL_lenz00_1303));
											{	/* Eval/evobject.scm 102 */
												int BgL_tmpz00_4793;

												BgL_tmpz00_4793 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4793);
											}
											{	/* Eval/evobject.scm 102 */
												int BgL_tmpz00_4796;

												BgL_tmpz00_4796 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_4796,
													BgL_val1_1098z00_1320);
											}
											return BgL_val0_1097z00_1319;
										}
									else
										{
											long BgL_walkerz00_4799;

											BgL_walkerz00_4799 = (BgL_walkerz00_1305 + 1L);
											BgL_walkerz00_1305 = BgL_walkerz00_4799;
											goto BgL_zc3z04anonymousza31335ze3z87_1306;
										}
								}
						}
					}
				}
			}
		}

	}



/* localize */
	obj_t BGl_localiza7eza7zz__evobjectz00(obj_t BgL_locz00_43, obj_t BgL_pz00_44)
	{
		{	/* Eval/evobject.scm 110 */
			{	/* Eval/evobject.scm 111 */
				bool_t BgL_test2643z00_4801;

				if (CBOOL(BgL_locz00_43))
					{	/* Eval/evobject.scm 111 */
						if (EPAIRP(BgL_locz00_43))
							{	/* Eval/evobject.scm 111 */
								BgL_test2643z00_4801 = ((bool_t) 0);
							}
						else
							{	/* Eval/evobject.scm 111 */
								BgL_test2643z00_4801 = ((bool_t) 1);
							}
					}
				else
					{	/* Eval/evobject.scm 111 */
						BgL_test2643z00_4801 = ((bool_t) 1);
					}
				if (BgL_test2643z00_4801)
					{	/* Eval/evobject.scm 111 */
						return BgL_pz00_44;
					}
				else
					{	/* Eval/evobject.scm 111 */
						BGL_TAIL return
							BGl_loopze70ze7zz__evobjectz00(BgL_locz00_43, BgL_pz00_44);
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__evobjectz00(obj_t BgL_locz00_4430,
		obj_t BgL_pz00_1335)
	{
		{	/* Eval/evobject.scm 113 */
			{	/* Eval/evobject.scm 114 */
				bool_t BgL_test2646z00_4807;

				if (EPAIRP(BgL_pz00_1335))
					{	/* Eval/evobject.scm 114 */
						BgL_test2646z00_4807 = ((bool_t) 1);
					}
				else
					{	/* Eval/evobject.scm 114 */
						if (PAIRP(BgL_pz00_1335))
							{	/* Eval/evobject.scm 114 */
								BgL_test2646z00_4807 = ((bool_t) 0);
							}
						else
							{	/* Eval/evobject.scm 114 */
								BgL_test2646z00_4807 = ((bool_t) 1);
							}
					}
				if (BgL_test2646z00_4807)
					{	/* Eval/evobject.scm 114 */
						return BgL_pz00_1335;
					}
				else
					{	/* Eval/evobject.scm 116 */
						obj_t BgL_arg1361z00_1340;
						obj_t BgL_arg1362z00_1341;
						obj_t BgL_arg1363z00_1342;

						{	/* Eval/evobject.scm 116 */
							obj_t BgL_arg1364z00_1343;

							BgL_arg1364z00_1343 = CAR(((obj_t) BgL_pz00_1335));
							BgL_arg1361z00_1340 =
								BGl_loopze70ze7zz__evobjectz00(BgL_locz00_4430,
								BgL_arg1364z00_1343);
						}
						{	/* Eval/evobject.scm 116 */
							obj_t BgL_arg1365z00_1344;

							BgL_arg1365z00_1344 = CDR(((obj_t) BgL_pz00_1335));
							BgL_arg1362z00_1341 =
								BGl_loopze70ze7zz__evobjectz00(BgL_locz00_4430,
								BgL_arg1365z00_1344);
						}
						BgL_arg1363z00_1342 = CER(((obj_t) BgL_locz00_4430));
						{	/* Eval/evobject.scm 116 */
							obj_t BgL_res2420z00_2953;

							BgL_res2420z00_2953 =
								MAKE_YOUNG_EPAIR(BgL_arg1361z00_1340, BgL_arg1362z00_1341,
								BgL_arg1363z00_1342);
							return BgL_res2420z00_2953;
						}
					}
			}
		}

	}



/* eval-creator */
	obj_t BGl_evalzd2creatorzd2zz__evobjectz00(obj_t BgL_idz00_45,
		obj_t BgL_nativez00_46, long BgL_lenz00_47, obj_t BgL_classnumz00_48)
	{
		{	/* Eval/evobject.scm 121 */
			{
				long BgL_nz00_1365;

				{	/* Eval/evobject.scm 137 */
					obj_t BgL_fieldsz00_1350;

					{	/* Eval/evobject.scm 137 */
						obj_t BgL_tmpz00_4821;

						BgL_tmpz00_4821 = ((obj_t) BgL_nativez00_46);
						BgL_fieldsz00_1350 = BGL_CLASS_ALL_FIELDS(BgL_tmpz00_4821);
					}
					{	/* Eval/evobject.scm 138 */
						long BgL_g1039z00_1351;

						BgL_g1039z00_1351 = (VECTOR_LENGTH(BgL_fieldsz00_1350) - 1L);
						{
							long BgL_iz00_2973;
							long BgL_nz00_2974;

							BgL_iz00_2973 = BgL_g1039z00_1351;
							BgL_nz00_2974 = 0L;
						BgL_loopz00_2972:
							if ((BgL_iz00_2973 == -1L))
								{	/* Eval/evobject.scm 141 */
									BgL_nz00_1365 = BgL_nz00_2974;
									{	/* Eval/evobject.scm 124 */
										obj_t BgL_creatorz00_1367;

										BgL_creatorz00_1367 =
											BGl_classzd2creatorzd2zz__objectz00(BgL_nativez00_46);
										{	/* Eval/evobject.scm 125 */
											obj_t BgL_zc3z04anonymousza31377ze3z87_4327;

											BgL_zc3z04anonymousza31377ze3z87_4327 =
												MAKE_VA_PROCEDURE
												(BGl_z62zc3z04anonymousza31377ze3ze5zz__evobjectz00,
												(int) (-1L), (int) (5L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_4327,
												(int) (0L), BINT(BgL_nz00_1365));
											PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_4327,
												(int) (1L), BINT(BgL_lenz00_47));
											PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_4327,
												(int) (2L), BgL_idz00_45);
											PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_4327,
												(int) (3L), BgL_creatorz00_1367);
											PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_4327,
												(int) (4L), BgL_classnumz00_48);
											return BgL_zc3z04anonymousza31377ze3z87_4327;
										}
									}
								}
							else
								{	/* Eval/evobject.scm 141 */
									if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(VECTOR_REF
											(BgL_fieldsz00_1350, BgL_iz00_2973)))
										{
											long BgL_iz00_4847;

											BgL_iz00_4847 = (BgL_iz00_2973 - 1L);
											BgL_iz00_2973 = BgL_iz00_4847;
											goto BgL_loopz00_2972;
										}
									else
										{
											long BgL_nz00_4851;
											long BgL_iz00_4849;

											BgL_iz00_4849 = (BgL_iz00_2973 - 1L);
											BgL_nz00_4851 = (BgL_nz00_2974 + 1L);
											BgL_nz00_2974 = BgL_nz00_4851;
											BgL_iz00_2973 = BgL_iz00_4849;
											goto BgL_loopz00_2972;
										}
								}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1377> */
	obj_t BGl_z62zc3z04anonymousza31377ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4328, obj_t BgL_lz00_4334)
	{
		{	/* Eval/evobject.scm 125 */
			{	/* Eval/evobject.scm 126 */
				long BgL_nz00_4329;
				long BgL_lenz00_4330;
				obj_t BgL_idz00_4331;
				obj_t BgL_creatorz00_4332;
				obj_t BgL_classnumz00_4333;

				BgL_nz00_4329 = (long) CINT(PROCEDURE_REF(BgL_envz00_4328, (int) (0L)));
				BgL_lenz00_4330 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4328, (int) (1L)));
				BgL_idz00_4331 = ((obj_t) PROCEDURE_REF(BgL_envz00_4328, (int) (2L)));
				BgL_creatorz00_4332 = PROCEDURE_REF(BgL_envz00_4328, (int) (3L));
				BgL_classnumz00_4333 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4328, (int) (4L)));
				if (
					(bgl_list_length(BgL_lz00_4334) == (BgL_nz00_4329 + BgL_lenz00_4330)))
					{	/* Eval/evobject.scm 127 */
						obj_t BgL_oz00_4486;

						BgL_oz00_4486 =
							apply(BgL_creatorz00_4332,
							BGl_takez00zz__r4_pairs_and_lists_6_3z00(BgL_lz00_4334,
								BgL_nz00_4329));
						{	/* Eval/evobject.scm 128 */
							obj_t BgL_arg1382z00_4487;

							BgL_arg1382z00_4487 = CELL_REF(BgL_classnumz00_4333);
							{	/* Eval/evobject.scm 128 */
								long BgL_numz00_4488;

								BgL_numz00_4488 = (long) CINT(BgL_arg1382z00_4487);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_oz00_4486), BgL_numz00_4488);
						}}
						{	/* Eval/evobject.scm 129 */
							obj_t BgL_arg1383z00_4489;

							BgL_arg1383z00_4489 =
								BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
								(BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_lz00_4334,
									BgL_nz00_4329));
							BGl_z52objectzd2wideningzd2setz12z40zz__objectz00((
									(BgL_objectz00_bglt) BgL_oz00_4486), BgL_arg1383z00_4489);
						}
						return BgL_oz00_4486;
					}
				else
					{	/* Eval/evobject.scm 134 */
						obj_t BgL_arg1387z00_4490;

						{	/* Eval/evobject.scm 134 */
							long BgL_arg1388z00_4491;

							BgL_arg1388z00_4491 = (BgL_nz00_4329 + BgL_lenz00_4330);
							{	/* Eval/evobject.scm 132 */
								obj_t BgL_list1389z00_4492;

								BgL_list1389z00_4492 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg1388z00_4491), BNIL);
								BgL_arg1387z00_4490 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2452z00zz__evobjectz00, BgL_list1389z00_4492);
						}}
						return
							BGl_expandzd2errorzd2zz__evobjectz00(BgL_idz00_4331,
							BgL_arg1387z00_4490, BgL_lz00_4334);
					}
			}
		}

	}



/* patch-field-default-values! */
	bool_t BGl_patchzd2fieldzd2defaultzd2valuesz12zc0zz__evobjectz00(obj_t
		BgL_slotsz00_55, obj_t BgL_fieldsz00_56, obj_t BgL_modulez00_57)
	{
		{	/* Eval/evobject.scm 171 */
			{	/* Eval/evobject.scm 174 */
				obj_t BgL_g1102z00_1393;

				BgL_g1102z00_1393 =
					BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_fieldsz00_56);
				{
					obj_t BgL_ll1099z00_1395;
					obj_t BgL_ll1100z00_1396;

					BgL_ll1099z00_1395 = BgL_g1102z00_1393;
					BgL_ll1100z00_1396 = BgL_slotsz00_55;
				BgL_zc3z04anonymousza31400ze3z87_1397:
					if (NULLP(BgL_ll1099z00_1395))
						{	/* Eval/evobject.scm 181 */
							return ((bool_t) 1);
						}
					else
						{	/* Eval/evobject.scm 181 */
							{	/* Eval/evobject.scm 177 */
								obj_t BgL_fieldz00_1399;
								obj_t BgL_slotz00_1400;

								BgL_fieldz00_1399 = CAR(((obj_t) BgL_ll1099z00_1395));
								BgL_slotz00_1400 = CAR(((obj_t) BgL_ll1100z00_1396));
								{	/* Eval/evobject.scm 179 */
									obj_t BgL_arg1402z00_1401;

									{	/* Eval/evobject.scm 179 */
										obj_t BgL_arg1403z00_1402;

										{	/* Eval/evobject.scm 179 */
											obj_t BgL_arg1404z00_1403;

											{	/* Eval/evobject.scm 179 */
												obj_t BgL_arg1405z00_1404;

												{	/* Eval/evobject.scm 179 */
													obj_t BgL_arg1406z00_1405;

													BgL_arg1406z00_1405 =
														STRUCT_REF(((obj_t) BgL_slotz00_1400), (int) (3L));
													BgL_arg1405z00_1404 =
														MAKE_YOUNG_PAIR(BgL_arg1406z00_1405, BNIL);
												}
												BgL_arg1404z00_1403 =
													MAKE_YOUNG_PAIR(BNIL, BgL_arg1405z00_1404);
											}
											BgL_arg1403z00_1402 =
												MAKE_YOUNG_PAIR(BGl_symbol2453z00zz__evobjectz00,
												BgL_arg1404z00_1403);
										}
										BgL_arg1402z00_1401 =
											BGl_evalz12z12zz__evalz00(BgL_arg1403z00_1402,
											BgL_modulez00_57);
									}
									VECTOR_SET(
										((obj_t) BgL_fieldz00_1399), 6L, BgL_arg1402z00_1401);
							}}
							{	/* Eval/evobject.scm 181 */
								obj_t BgL_arg1407z00_1406;
								obj_t BgL_arg1408z00_1407;

								BgL_arg1407z00_1406 = CDR(((obj_t) BgL_ll1099z00_1395));
								BgL_arg1408z00_1407 = CDR(((obj_t) BgL_ll1100z00_1396));
								{
									obj_t BgL_ll1100z00_4908;
									obj_t BgL_ll1099z00_4907;

									BgL_ll1099z00_4907 = BgL_arg1407z00_1406;
									BgL_ll1100z00_4908 = BgL_arg1408z00_1407;
									BgL_ll1100z00_1396 = BgL_ll1100z00_4908;
									BgL_ll1099z00_1395 = BgL_ll1099z00_4907;
									goto BgL_zc3z04anonymousza31400ze3z87_1397;
								}
							}
						}
				}
			}
		}

	}



/* patch-vfield-accessors! */
	bool_t BGl_patchzd2vfieldzd2accessorsz12z12zz__evobjectz00(obj_t
		BgL_slotsz00_58, obj_t BgL_fieldsz00_59, obj_t BgL_virtualsz00_60,
		obj_t BgL_modulez00_61)
	{
		{	/* Eval/evobject.scm 186 */
			{	/* Eval/evobject.scm 189 */
				obj_t BgL_g1107z00_1409;

				BgL_g1107z00_1409 =
					BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_fieldsz00_59);
				{
					obj_t BgL_l1105z00_1411;

					BgL_l1105z00_1411 = BgL_g1107z00_1409;
				BgL_zc3z04anonymousza31409ze3z87_1412:
					if (PAIRP(BgL_l1105z00_1411))
						{	/* Eval/evobject.scm 203 */
							{	/* Eval/evobject.scm 190 */
								obj_t BgL_fieldz00_1414;

								BgL_fieldz00_1414 = CAR(BgL_l1105z00_1411);
								{	/* Eval/evobject.scm 190 */
									obj_t BgL_idz00_1415;

									BgL_idz00_1415 =
										BGl_classzd2fieldzd2namez00zz__objectz00(BgL_fieldz00_1414);
									{	/* Eval/evobject.scm 190 */
										obj_t BgL_slotz00_1416;

										{
											obj_t BgL_list1104z00_1429;

											BgL_list1104z00_1429 = BgL_slotsz00_58;
										BgL_zc3z04anonymousza31420ze3z87_1430:
											if (PAIRP(BgL_list1104z00_1429))
												{	/* Eval/evobject.scm 191 */
													if (
														(STRUCT_REF(CAR(BgL_list1104z00_1429),
																(int) (0L)) == BgL_idz00_1415))
														{	/* Eval/evobject.scm 191 */
															BgL_slotz00_1416 = CAR(BgL_list1104z00_1429);
														}
													else
														{
															obj_t BgL_list1104z00_4922;

															BgL_list1104z00_4922 = CDR(BgL_list1104z00_1429);
															BgL_list1104z00_1429 = BgL_list1104z00_4922;
															goto BgL_zc3z04anonymousza31420ze3z87_1430;
														}
												}
											else
												{	/* Eval/evobject.scm 191 */
													BgL_slotz00_1416 = BFALSE;
												}
										}
										{	/* Eval/evobject.scm 191 */

											if (CBOOL(STRUCT_REF(
														((obj_t) BgL_slotz00_1416), (int) (5L))))
												{	/* Eval/evobject.scm 194 */
													{	/* Eval/evobject.scm 195 */
														obj_t BgL_arg1412z00_1418;

														{	/* Eval/evobject.scm 195 */
															obj_t BgL_arg1413z00_1419;

															BgL_arg1413z00_1419 =
																STRUCT_REF(
																((obj_t) BgL_slotz00_1416), (int) (5L));
															BgL_arg1412z00_1418 =
																BGl_evalz12z12zz__evalz00(BgL_arg1413z00_1419,
																BgL_modulez00_61);
														}
														{	/* Eval/evobject.scm 80 */
															int BgL_auxz00_4935;
															obj_t BgL_tmpz00_4933;

															BgL_auxz00_4935 = (int) (5L);
															BgL_tmpz00_4933 = ((obj_t) BgL_slotz00_1416);
															STRUCT_SET(BgL_tmpz00_4933, BgL_auxz00_4935,
																BgL_arg1412z00_1418);
													}}
													{	/* Eval/evobject.scm 196 */
														obj_t BgL_arg1414z00_1420;

														{	/* Eval/evobject.scm 196 */
															obj_t BgL_arg1415z00_1421;

															BgL_arg1415z00_1421 =
																STRUCT_REF(
																((obj_t) BgL_slotz00_1416), (int) (6L));
															BgL_arg1414z00_1420 =
																BGl_evalz12z12zz__evalz00(BgL_arg1415z00_1421,
																BgL_modulez00_61);
														}
														{	/* Eval/evobject.scm 80 */
															int BgL_auxz00_4944;
															obj_t BgL_tmpz00_4942;

															BgL_auxz00_4944 = (int) (6L);
															BgL_tmpz00_4942 = ((obj_t) BgL_slotz00_1416);
															STRUCT_SET(BgL_tmpz00_4942, BgL_auxz00_4944,
																BgL_arg1414z00_1420);
													}}
													{	/* Eval/evobject.scm 197 */
														obj_t BgL_arg1416z00_1422;

														BgL_arg1416z00_1422 =
															STRUCT_REF(
															((obj_t) BgL_slotz00_1416), (int) (5L));
														VECTOR_SET(
															((obj_t) BgL_fieldz00_1414), 1L,
															BgL_arg1416z00_1422);
													}
													{	/* Eval/evobject.scm 198 */
														obj_t BgL_arg1417z00_1423;

														BgL_arg1417z00_1423 =
															STRUCT_REF(
															((obj_t) BgL_slotz00_1416), (int) (6L));
														VECTOR_SET(
															((obj_t) BgL_fieldz00_1414), 2L,
															BgL_arg1417z00_1423);
													}
													{	/* Eval/evobject.scm 199 */
														obj_t BgL_numz00_1424;

														BgL_numz00_1424 =
															STRUCT_REF(
															((obj_t) BgL_slotz00_1416), (int) (4L));
														{	/* Eval/evobject.scm 199 */
															obj_t BgL_vfieldz00_1425;

															BgL_vfieldz00_1425 =
																VECTOR_REF(BgL_virtualsz00_60,
																(long) CINT(BgL_numz00_1424));
															{	/* Eval/evobject.scm 200 */

																{	/* Eval/evobject.scm 201 */
																	obj_t BgL_arg1418z00_1426;

																	BgL_arg1418z00_1426 =
																		STRUCT_REF(
																		((obj_t) BgL_slotz00_1416), (int) (5L));
																	{	/* Eval/evobject.scm 201 */
																		obj_t BgL_tmpz00_4965;

																		BgL_tmpz00_4965 =
																			((obj_t) BgL_vfieldz00_1425);
																		SET_CAR(BgL_tmpz00_4965,
																			BgL_arg1418z00_1426);
																}}
																{	/* Eval/evobject.scm 202 */
																	obj_t BgL_arg1419z00_1427;

																	BgL_arg1419z00_1427 =
																		STRUCT_REF(
																		((obj_t) BgL_slotz00_1416), (int) (6L));
																	{	/* Eval/evobject.scm 202 */
																		obj_t BgL_tmpz00_4971;

																		BgL_tmpz00_4971 =
																			((obj_t) BgL_vfieldz00_1425);
																		SET_CDR(BgL_tmpz00_4971,
																			BgL_arg1419z00_1427);
												}}}}}}
											else
												{	/* Eval/evobject.scm 194 */
													BFALSE;
												}
										}
									}
								}
							}
							{
								obj_t BgL_l1105z00_4974;

								BgL_l1105z00_4974 = CDR(BgL_l1105z00_1411);
								BgL_l1105z00_1411 = BgL_l1105z00_4974;
								goto BgL_zc3z04anonymousza31409ze3z87_1412;
							}
						}
					else
						{	/* Eval/evobject.scm 203 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* eval-register-class */
	obj_t BGl_evalzd2registerzd2classz00zz__evobjectz00(obj_t BgL_idz00_62,
		obj_t BgL_modulez00_63, obj_t BgL_superz00_64, bool_t BgL_abstractz00_65,
		obj_t BgL_slotsz00_66, long BgL_hashz00_67, obj_t BgL_constrz00_68)
	{
		{	/* Eval/evobject.scm 208 */
			{	/* Eval/evobject.scm 209 */
				long BgL_siza7eza7_1441;

				{	/* Eval/evobject.scm 209 */
					obj_t BgL_arg1443z00_1466;

					{	/* Eval/evobject.scm 209 */
						obj_t BgL_hook1112z00_1467;

						BgL_hook1112z00_1467 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{
							obj_t BgL_l1109z00_1469;
							obj_t BgL_h1110z00_1470;

							BgL_l1109z00_1469 = BgL_slotsz00_66;
							BgL_h1110z00_1470 = BgL_hook1112z00_1467;
						BgL_zc3z04anonymousza31444ze3z87_1471:
							if (NULLP(BgL_l1109z00_1469))
								{	/* Eval/evobject.scm 209 */
									BgL_arg1443z00_1466 = CDR(BgL_hook1112z00_1467);
								}
							else
								{	/* Eval/evobject.scm 209 */
									bool_t BgL_test2658z00_4980;

									{	/* Eval/evobject.scm 209 */
										bool_t BgL_test2659z00_4981;

										{	/* Eval/evobject.scm 80 */
											obj_t BgL_sz00_3036;

											BgL_sz00_3036 = CAR(((obj_t) BgL_l1109z00_1469));
											BgL_test2659z00_4981 =
												CBOOL(STRUCT_REF(BgL_sz00_3036, (int) (5L)));
										}
										if (BgL_test2659z00_4981)
											{	/* Eval/evobject.scm 209 */
												BgL_test2658z00_4980 = ((bool_t) 0);
											}
										else
											{	/* Eval/evobject.scm 209 */
												BgL_test2658z00_4980 = ((bool_t) 1);
											}
									}
									if (BgL_test2658z00_4980)
										{	/* Eval/evobject.scm 209 */
											obj_t BgL_nh1111z00_1476;

											{	/* Eval/evobject.scm 209 */
												obj_t BgL_arg1449z00_1478;

												BgL_arg1449z00_1478 = CAR(((obj_t) BgL_l1109z00_1469));
												BgL_nh1111z00_1476 =
													MAKE_YOUNG_PAIR(BgL_arg1449z00_1478, BNIL);
											}
											SET_CDR(BgL_h1110z00_1470, BgL_nh1111z00_1476);
											{	/* Eval/evobject.scm 209 */
												obj_t BgL_arg1448z00_1477;

												BgL_arg1448z00_1477 = CDR(((obj_t) BgL_l1109z00_1469));
												{
													obj_t BgL_h1110z00_4994;
													obj_t BgL_l1109z00_4993;

													BgL_l1109z00_4993 = BgL_arg1448z00_1477;
													BgL_h1110z00_4994 = BgL_nh1111z00_1476;
													BgL_h1110z00_1470 = BgL_h1110z00_4994;
													BgL_l1109z00_1469 = BgL_l1109z00_4993;
													goto BgL_zc3z04anonymousza31444ze3z87_1471;
												}
											}
										}
									else
										{	/* Eval/evobject.scm 209 */
											obj_t BgL_arg1450z00_1479;

											BgL_arg1450z00_1479 = CDR(((obj_t) BgL_l1109z00_1469));
											{
												obj_t BgL_l1109z00_4997;

												BgL_l1109z00_4997 = BgL_arg1450z00_1479;
												BgL_l1109z00_1469 = BgL_l1109z00_4997;
												goto BgL_zc3z04anonymousza31444ze3z87_1471;
											}
										}
								}
						}
					}
					BgL_siza7eza7_1441 = bgl_list_length(BgL_arg1443z00_1466);
				}
				{	/* Eval/evobject.scm 209 */
					obj_t BgL_offsetz00_1442;

					if (BGl_evalzd2classzf3z21zz__objectz00(BgL_superz00_64))
						{	/* Eval/evobject.scm 210 */
							BgL_offsetz00_1442 =
								BGl_classzd2evdatazd2zz__objectz00(BgL_superz00_64);
						}
					else
						{	/* Eval/evobject.scm 210 */
							BgL_offsetz00_1442 = BINT(0L);
						}
					{	/* Eval/evobject.scm 210 */
						obj_t BgL_nativez00_1443;

						{
							obj_t BgL_superz00_1458;

							BgL_superz00_1458 = BgL_superz00_64;
						BgL_zc3z04anonymousza31437ze3z87_1459:
							if (BGl_evalzd2classzf3z21zz__objectz00(BgL_superz00_1458))
								{
									obj_t BgL_superz00_5005;

									BgL_superz00_5005 =
										BGl_classzd2superzd2zz__objectz00(BgL_superz00_1458);
									BgL_superz00_1458 = BgL_superz00_5005;
									goto BgL_zc3z04anonymousza31437ze3z87_1459;
								}
							else
								{	/* Eval/evobject.scm 213 */
									if ((BgL_superz00_1458 == BGl_objectz00zz__objectz00))
										{	/* Eval/evobject.scm 214 */
											BgL_nativez00_1443 = BGl_objectz00zz__objectz00;
										}
									else
										{	/* Eval/evobject.scm 214 */
											if (BGl_classzd2abstractzf3z21zz__objectz00
												(BgL_superz00_1458))
												{
													obj_t BgL_superz00_5011;

													BgL_superz00_5011 =
														BGl_classzd2superzd2zz__objectz00
														(BgL_superz00_1458);
													BgL_superz00_1458 = BgL_superz00_5011;
													goto BgL_zc3z04anonymousza31437ze3z87_1459;
												}
											else
												{	/* Eval/evobject.scm 215 */
													BgL_nativez00_1443 = BgL_superz00_1458;
												}
										}
								}
						}
						{	/* Eval/evobject.scm 211 */
							long BgL_lengthz00_1444;

							BgL_lengthz00_1444 =
								((long) CINT(BgL_offsetz00_1442) + BgL_siza7eza7_1441);
							{	/* Eval/evobject.scm 217 */
								obj_t BgL_classnumz00_1445;

								BgL_classnumz00_1445 = MAKE_YOUNG_CELL(BINT(-1L));
								{	/* Eval/evobject.scm 218 */
									obj_t BgL_claza7za7z00_1446;

									{	/* Eval/evobject.scm 223 */
										obj_t BgL_arg1428z00_1449;
										obj_t BgL_arg1429z00_1450;
										obj_t BgL_arg1430z00_1451;
										obj_t BgL_arg1431z00_1452;
										obj_t BgL_arg1434z00_1453;
										obj_t BgL_arg1435z00_1454;

										if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_modulez00_63))
											{	/* Eval/evobject.scm 223 */
												BgL_arg1428z00_1449 =
													BGl_evmodulezd2namezd2zz__evmodulez00
													(BgL_modulez00_63);
											}
										else
											{	/* Eval/evobject.scm 223 */
												BgL_arg1428z00_1449 = BGl_symbol2455z00zz__evobjectz00;
											}
										BgL_arg1429z00_1450 =
											BGl_evalzd2creatorzd2zz__evobjectz00(BgL_idz00_62,
											BgL_nativez00_1443, BgL_lengthz00_1444,
											BgL_classnumz00_1445);
										{	/* Eval/evobject.scm 152 */
											obj_t BgL_allocz00_3043;

											BgL_allocz00_3043 =
												BGl_classzd2allocatorzd2zz__objectz00
												(BgL_nativez00_1443);
											{	/* Eval/evobject.scm 153 */
												obj_t BgL_zc3z04anonymousza31392ze3z87_4336;

												BgL_zc3z04anonymousza31392ze3z87_4336 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31392ze3ze5zz__evobjectz00,
													(int) (0L), (int) (3L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31392ze3z87_4336,
													(int) (0L), BgL_allocz00_3043);
												PROCEDURE_SET(BgL_zc3z04anonymousza31392ze3z87_4336,
													(int) (1L), BgL_classnumz00_1445);
												PROCEDURE_SET(BgL_zc3z04anonymousza31392ze3z87_4336,
													(int) (2L), BINT(BgL_lengthz00_1444));
												BgL_arg1430z00_1451 =
													BgL_zc3z04anonymousza31392ze3z87_4336;
										}}
										if (CBOOL(BgL_constrz00_68))
											{	/* Eval/evobject.scm 235 */
												BgL_arg1431z00_1452 = BgL_constrz00_68;
											}
										else
											{
												obj_t BgL_cz00_3054;

												BgL_cz00_3054 = BgL_superz00_64;
											BgL_findzd2classzd2constructorz00_3053:
												{	/* Eval/evobject.scm 259 */
													obj_t BgL_constz00_3058;

													BgL_constz00_3058 =
														BGl_classzd2constructorzd2zz__objectz00
														(BgL_cz00_3054);
													if (CBOOL(BgL_constz00_3058))
														{	/* Eval/evobject.scm 260 */
															BgL_arg1431z00_1452 = BgL_constz00_3058;
														}
													else
														{	/* Eval/evobject.scm 262 */
															obj_t BgL_sz00_3059;

															BgL_sz00_3059 =
																BGl_classzd2superzd2zz__objectz00
																(BgL_cz00_3054);
															if (BGl_classzf3zf3zz__objectz00(BgL_sz00_3059))
																{
																	obj_t BgL_cz00_5040;

																	BgL_cz00_5040 = BgL_sz00_3059;
																	BgL_cz00_3054 = BgL_cz00_5040;
																	goto BgL_findzd2classzd2constructorz00_3053;
																}
															else
																{	/* Eval/evobject.scm 263 */
																	BgL_arg1431z00_1452 = BFALSE;
																}
														}
												}
											}
										{	/* Eval/evobject.scm 163 */
											obj_t BgL_zc3z04anonymousza31396ze3z87_4335;

											BgL_zc3z04anonymousza31396ze3z87_4335 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31396ze3ze5zz__evobjectz00,
												(int) (1L), (int) (2L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_4335,
												(int) (0L), BgL_classnumz00_1445);
											PROCEDURE_SET(BgL_zc3z04anonymousza31396ze3z87_4335,
												(int) (1L), BINT(BgL_lengthz00_1444));
											BgL_arg1434z00_1453 =
												BgL_zc3z04anonymousza31396ze3z87_4335;
										}
										BgL_arg1435z00_1454 =
											BGl_makezd2classzd2virtualzd2fieldszd2zz__evobjectz00
											(BgL_slotsz00_66);
										BgL_claza7za7z00_1446 =
											BGl_registerzd2classz12zc0zz__objectz00(BgL_idz00_62,
											BgL_arg1428z00_1449, BgL_superz00_64, BgL_hashz00_67,
											BgL_arg1429z00_1450, BgL_arg1430z00_1451,
											BgL_arg1431z00_1452, BgL_arg1434z00_1453, BFALSE,
											BGl_vector2457z00zz__evobjectz00, BgL_arg1435z00_1454);
									}
									{	/* Eval/evobject.scm 219 */

										{	/* Eval/evobject.scm 244 */
											long BgL_arg1427z00_1447;

											BgL_arg1427z00_1447 =
												BGL_CLASS_NUM(BgL_claza7za7z00_1446);
											CELL_SET(BgL_classnumz00_1445, BINT(BgL_arg1427z00_1447));
										}
										BGl_classzd2evdatazd2setz12z12zz__objectz00
											(BgL_claza7za7z00_1446, BINT(BgL_lengthz00_1444));
										{	/* Eval/evobject.scm 247 */
											obj_t BgL_fieldsz00_1448;

											BgL_fieldsz00_1448 =
												BGl_makezd2classzd2fieldsz00zz__evobjectz00
												(BgL_idz00_62, BgL_claza7za7z00_1446, BgL_slotsz00_66,
												BgL_siza7eza7_1441, BgL_offsetz00_1442);
											BGl_classzd2evfieldszd2setz12z12zz__objectz00
												(BgL_claza7za7z00_1446, BgL_fieldsz00_1448);
											BGl_list2458z00zz__evobjectz00;
											BGl_list2465z00zz__evobjectz00;
										}
										return BgL_claza7za7z00_1446;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1396> */
	obj_t BGl_z62zc3z04anonymousza31396ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4337, obj_t BgL_oz00_4340)
	{
		{	/* Eval/evobject.scm 163 */
			{	/* Eval/evobject.scm 164 */
				obj_t BgL_classnumz00_4338;
				long BgL_lengthz00_4339;

				BgL_classnumz00_4338 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4337, (int) (0L)));
				BgL_lengthz00_4339 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4337, (int) (1L)));
				{	/* Eval/evobject.scm 164 */
					obj_t BgL_arg1397z00_4493;

					BgL_arg1397z00_4493 = CELL_REF(BgL_classnumz00_4338);
					{	/* Eval/evobject.scm 164 */
						long BgL_numz00_4494;

						BgL_numz00_4494 = (long) CINT(BgL_arg1397z00_4493);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_oz00_4340), BgL_numz00_4494);
				}}
				{	/* Eval/evobject.scm 165 */
					obj_t BgL_arg1399z00_4495;

					BgL_arg1399z00_4495 = make_vector(BgL_lengthz00_4339, BFALSE);
					BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(
						((BgL_objectz00_bglt) BgL_oz00_4340), BgL_arg1399z00_4495);
				}
				return BgL_oz00_4340;
			}
		}

	}



/* &<@anonymous:1392> */
	obj_t BGl_z62zc3z04anonymousza31392ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4341)
	{
		{	/* Eval/evobject.scm 153 */
			{	/* Eval/evobject.scm 154 */
				obj_t BgL_allocz00_4342;
				obj_t BgL_classnumz00_4343;
				long BgL_lengthz00_4344;

				BgL_allocz00_4342 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4341, (int) (0L)));
				BgL_classnumz00_4343 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4341, (int) (1L)));
				BgL_lengthz00_4344 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4341, (int) (2L)));
				{	/* Eval/evobject.scm 154 */
					obj_t BgL_oz00_4496;

					BgL_oz00_4496 = BGL_PROCEDURE_CALL0(BgL_allocz00_4342);
					{	/* Eval/evobject.scm 155 */
						obj_t BgL_arg1393z00_4497;

						BgL_arg1393z00_4497 = CELL_REF(BgL_classnumz00_4343);
						{	/* Eval/evobject.scm 155 */
							long BgL_numz00_4498;

							BgL_numz00_4498 = (long) CINT(BgL_arg1393z00_4497);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_oz00_4496), BgL_numz00_4498);
					}}
					{	/* Eval/evobject.scm 156 */
						obj_t BgL_arg1394z00_4499;

						BgL_arg1394z00_4499 = make_vector(BgL_lengthz00_4344, BUNSPEC);
						BGl_z52objectzd2wideningzd2setz12z40zz__objectz00(
							((BgL_objectz00_bglt) BgL_oz00_4496), BgL_arg1394z00_4499);
					}
					return BgL_oz00_4496;
				}
			}
		}

	}



/* classgen-slot-anonymous */
	obj_t BGl_classgenzd2slotzd2anonymousz00zz__evobjectz00(obj_t BgL_indexz00_70,
		obj_t BgL_sz00_71, obj_t BgL_classz00_72)
	{
		{	/* Eval/evobject.scm 273 */
			{	/* Eval/evobject.scm 276 */
				obj_t BgL_zc3z04anonymousza31461ze3z87_4346;
				obj_t BgL_zc3z04anonymousza31456ze3z87_4345;

				BgL_zc3z04anonymousza31461ze3z87_4346 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31461ze3ze5zz__evobjectz00,
					(int) (2L), (int) (3L));
				BgL_zc3z04anonymousza31456ze3z87_4345 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31456ze3ze5zz__evobjectz00,
					(int) (1L), (int) (3L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31461ze3z87_4346,
					(int) (0L), BgL_sz00_71);
				PROCEDURE_SET(BgL_zc3z04anonymousza31461ze3z87_4346,
					(int) (1L), BgL_classz00_72);
				PROCEDURE_SET(BgL_zc3z04anonymousza31461ze3z87_4346,
					(int) (2L), BgL_indexz00_70);
				PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_4345,
					(int) (0L), BgL_sz00_71);
				PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_4345,
					(int) (1L), BgL_classz00_72);
				PROCEDURE_SET(BgL_zc3z04anonymousza31456ze3z87_4345,
					(int) (2L), BgL_indexz00_70);
				{	/* Eval/evobject.scm 274 */
					obj_t BgL_list1454z00_1488;

					{	/* Eval/evobject.scm 274 */
						obj_t BgL_arg1455z00_1489;

						BgL_arg1455z00_1489 =
							MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31461ze3z87_4346, BNIL);
						BgL_list1454z00_1488 =
							MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31456ze3z87_4345,
							BgL_arg1455z00_1489);
					}
					return BgL_list1454z00_1488;
				}
			}
		}

	}



/* &<@anonymous:1456> */
	obj_t BGl_z62zc3z04anonymousza31456ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4347, obj_t BgL_oz00_4351)
	{
		{	/* Eval/evobject.scm 275 */
			{	/* Eval/evobject.scm 276 */
				obj_t BgL_sz00_4348;
				obj_t BgL_classz00_4349;
				obj_t BgL_indexz00_4350;

				BgL_sz00_4348 = PROCEDURE_REF(BgL_envz00_4347, (int) (0L));
				BgL_classz00_4349 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4347, (int) (1L)));
				BgL_indexz00_4350 = PROCEDURE_REF(BgL_envz00_4347, (int) (2L));
				{	/* Eval/evobject.scm 276 */
					bool_t BgL_test2668z00_5117;

					if (BGL_OBJECTP(BgL_oz00_4351))
						{	/* Eval/evobject.scm 276 */
							BgL_objectz00_bglt BgL_arg2397z00_4500;
							long BgL_arg2398z00_4501;

							BgL_arg2397z00_4500 = (BgL_objectz00_bglt) (BgL_oz00_4351);
							BgL_arg2398z00_4501 = BGL_CLASS_DEPTH(BgL_classz00_4349);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Eval/evobject.scm 276 */
									long BgL_idxz00_4502;

									BgL_idxz00_4502 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg2397z00_4500);
									BgL_test2668z00_5117 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4502 + BgL_arg2398z00_4501)) ==
										BgL_classz00_4349);
								}
							else
								{	/* Eval/evobject.scm 276 */
									bool_t BgL_res2423z00_4505;

									{	/* Eval/evobject.scm 276 */
										obj_t BgL_oclassz00_4506;

										{	/* Eval/evobject.scm 276 */
											obj_t BgL_arg2403z00_4507;
											long BgL_arg2404z00_4508;

											BgL_arg2403z00_4507 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Eval/evobject.scm 276 */
												long BgL_arg2405z00_4509;

												BgL_arg2405z00_4509 =
													BGL_OBJECT_CLASS_NUM(BgL_arg2397z00_4500);
												BgL_arg2404z00_4508 =
													(BgL_arg2405z00_4509 - OBJECT_TYPE);
											}
											BgL_oclassz00_4506 =
												VECTOR_REF(BgL_arg2403z00_4507, BgL_arg2404z00_4508);
										}
										{	/* Eval/evobject.scm 276 */
											bool_t BgL__ortest_1092z00_4510;

											BgL__ortest_1092z00_4510 =
												(BgL_classz00_4349 == BgL_oclassz00_4506);
											if (BgL__ortest_1092z00_4510)
												{	/* Eval/evobject.scm 276 */
													BgL_res2423z00_4505 = BgL__ortest_1092z00_4510;
												}
											else
												{	/* Eval/evobject.scm 276 */
													long BgL_odepthz00_4511;

													{	/* Eval/evobject.scm 276 */
														obj_t BgL_arg2392z00_4512;

														BgL_arg2392z00_4512 = (BgL_oclassz00_4506);
														BgL_odepthz00_4511 =
															BGL_CLASS_DEPTH(BgL_arg2392z00_4512);
													}
													if ((BgL_arg2398z00_4501 < BgL_odepthz00_4511))
														{	/* Eval/evobject.scm 276 */
															obj_t BgL_arg2390z00_4513;

															{	/* Eval/evobject.scm 276 */
																obj_t BgL_arg2391z00_4514;

																BgL_arg2391z00_4514 = (BgL_oclassz00_4506);
																BgL_arg2390z00_4513 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg2391z00_4514,
																	BgL_arg2398z00_4501);
															}
															BgL_res2423z00_4505 =
																(BgL_arg2390z00_4513 == BgL_classz00_4349);
														}
													else
														{	/* Eval/evobject.scm 276 */
															BgL_res2423z00_4505 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2668z00_5117 = BgL_res2423z00_4505;
								}
						}
					else
						{	/* Eval/evobject.scm 276 */
							BgL_test2668z00_5117 = ((bool_t) 0);
						}
					if (BgL_test2668z00_5117)
						{	/* Eval/evobject.scm 277 */
							obj_t BgL_arg1458z00_4515;

							BgL_arg1458z00_4515 =
								BGl_z52objectzd2wideningz80zz__objectz00(
								((BgL_objectz00_bglt) BgL_oz00_4351));
							{	/* Eval/evobject.scm 277 */
								long BgL_kz00_4516;

								BgL_kz00_4516 = (long) CINT(BgL_indexz00_4350);
								return VECTOR_REF(((obj_t) BgL_arg1458z00_4515), BgL_kz00_4516);
							}
						}
					else
						{	/* Eval/evobject.scm 278 */
							obj_t BgL_arg1459z00_4517;
							obj_t BgL_arg1460z00_4518;

							BgL_arg1459z00_4517 =
								STRUCT_REF(((obj_t) BgL_sz00_4348), (int) (0L));
							BgL_arg1460z00_4518 =
								BGl_classzd2namezd2zz__objectz00(BgL_classz00_4349);
							return
								BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_arg1459z00_4517,
								BgL_arg1460z00_4518, BgL_oz00_4351);
						}
				}
			}
		}

	}



/* &<@anonymous:1461> */
	obj_t BGl_z62zc3z04anonymousza31461ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4352, obj_t BgL_oz00_4356, obj_t BgL_vz00_4357)
	{
		{	/* Eval/evobject.scm 279 */
			{	/* Eval/evobject.scm 280 */
				obj_t BgL_sz00_4353;
				obj_t BgL_classz00_4354;
				obj_t BgL_indexz00_4355;

				BgL_sz00_4353 = PROCEDURE_REF(BgL_envz00_4352, (int) (0L));
				BgL_classz00_4354 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4352, (int) (1L)));
				BgL_indexz00_4355 = PROCEDURE_REF(BgL_envz00_4352, (int) (2L));
				{	/* Eval/evobject.scm 280 */
					bool_t BgL_test2673z00_5158;

					if (BGL_OBJECTP(BgL_oz00_4356))
						{	/* Eval/evobject.scm 280 */
							BgL_objectz00_bglt BgL_arg2397z00_4519;
							long BgL_arg2398z00_4520;

							BgL_arg2397z00_4519 = (BgL_objectz00_bglt) (BgL_oz00_4356);
							BgL_arg2398z00_4520 = BGL_CLASS_DEPTH(BgL_classz00_4354);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Eval/evobject.scm 280 */
									long BgL_idxz00_4521;

									BgL_idxz00_4521 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg2397z00_4519);
									BgL_test2673z00_5158 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4521 + BgL_arg2398z00_4520)) ==
										BgL_classz00_4354);
								}
							else
								{	/* Eval/evobject.scm 280 */
									bool_t BgL_res2424z00_4524;

									{	/* Eval/evobject.scm 280 */
										obj_t BgL_oclassz00_4525;

										{	/* Eval/evobject.scm 280 */
											obj_t BgL_arg2403z00_4526;
											long BgL_arg2404z00_4527;

											BgL_arg2403z00_4526 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Eval/evobject.scm 280 */
												long BgL_arg2405z00_4528;

												BgL_arg2405z00_4528 =
													BGL_OBJECT_CLASS_NUM(BgL_arg2397z00_4519);
												BgL_arg2404z00_4527 =
													(BgL_arg2405z00_4528 - OBJECT_TYPE);
											}
											BgL_oclassz00_4525 =
												VECTOR_REF(BgL_arg2403z00_4526, BgL_arg2404z00_4527);
										}
										{	/* Eval/evobject.scm 280 */
											bool_t BgL__ortest_1092z00_4529;

											BgL__ortest_1092z00_4529 =
												(BgL_classz00_4354 == BgL_oclassz00_4525);
											if (BgL__ortest_1092z00_4529)
												{	/* Eval/evobject.scm 280 */
													BgL_res2424z00_4524 = BgL__ortest_1092z00_4529;
												}
											else
												{	/* Eval/evobject.scm 280 */
													long BgL_odepthz00_4530;

													{	/* Eval/evobject.scm 280 */
														obj_t BgL_arg2392z00_4531;

														BgL_arg2392z00_4531 = (BgL_oclassz00_4525);
														BgL_odepthz00_4530 =
															BGL_CLASS_DEPTH(BgL_arg2392z00_4531);
													}
													if ((BgL_arg2398z00_4520 < BgL_odepthz00_4530))
														{	/* Eval/evobject.scm 280 */
															obj_t BgL_arg2390z00_4532;

															{	/* Eval/evobject.scm 280 */
																obj_t BgL_arg2391z00_4533;

																BgL_arg2391z00_4533 = (BgL_oclassz00_4525);
																BgL_arg2390z00_4532 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg2391z00_4533,
																	BgL_arg2398z00_4520);
															}
															BgL_res2424z00_4524 =
																(BgL_arg2390z00_4532 == BgL_classz00_4354);
														}
													else
														{	/* Eval/evobject.scm 280 */
															BgL_res2424z00_4524 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2673z00_5158 = BgL_res2424z00_4524;
								}
						}
					else
						{	/* Eval/evobject.scm 280 */
							BgL_test2673z00_5158 = ((bool_t) 0);
						}
					if (BgL_test2673z00_5158)
						{	/* Eval/evobject.scm 281 */
							obj_t BgL_arg1463z00_4534;

							BgL_arg1463z00_4534 =
								BGl_z52objectzd2wideningz80zz__objectz00(
								((BgL_objectz00_bglt) BgL_oz00_4356));
							{	/* Eval/evobject.scm 281 */
								long BgL_kz00_4535;

								BgL_kz00_4535 = (long) CINT(BgL_indexz00_4355);
								return
									VECTOR_SET(
									((obj_t) BgL_arg1463z00_4534), BgL_kz00_4535, BgL_vz00_4357);
							}
						}
					else
						{	/* Eval/evobject.scm 282 */
							obj_t BgL_arg1464z00_4536;
							obj_t BgL_arg1465z00_4537;

							BgL_arg1464z00_4536 =
								STRUCT_REF(((obj_t) BgL_sz00_4353), (int) (0L));
							BgL_arg1465z00_4537 =
								BGl_classzd2namezd2zz__objectz00(BgL_classz00_4354);
							return
								BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_arg1464z00_4536,
								BgL_arg1465z00_4537, BgL_oz00_4356);
						}
				}
			}
		}

	}



/* make-class-fields */
	obj_t BGl_makezd2classzd2fieldsz00zz__evobjectz00(obj_t BgL_idz00_73,
		obj_t BgL_classz00_74, obj_t BgL_slotsz00_75, long BgL_siza7eza7_76,
		obj_t BgL_offsetz00_77)
	{
		{	/* Eval/evobject.scm 287 */
			{
				obj_t BgL_sz00_1562;
				obj_t BgL_iz00_1563;
				obj_t BgL_classz00_1564;

				{	/* Eval/evobject.scm 312 */
					obj_t BgL_arg1466z00_1507;

					{	/* Eval/evobject.scm 312 */
						obj_t BgL_arg1467z00_1508;
						obj_t BgL_arg1468z00_1509;

						{	/* Eval/evobject.scm 312 */
							obj_t BgL_ll1118z00_1510;
							obj_t BgL_ll1119z00_1511;

							{	/* Eval/evobject.scm 314 */
								obj_t BgL_hook1117z00_1527;

								BgL_hook1117z00_1527 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{
									obj_t BgL_l1114z00_1529;
									obj_t BgL_h1115z00_1530;

									BgL_l1114z00_1529 = BgL_slotsz00_75;
									BgL_h1115z00_1530 = BgL_hook1117z00_1527;
								BgL_zc3z04anonymousza31475ze3z87_1531:
									if (NULLP(BgL_l1114z00_1529))
										{	/* Eval/evobject.scm 314 */
											BgL_ll1118z00_1510 = CDR(BgL_hook1117z00_1527);
										}
									else
										{	/* Eval/evobject.scm 314 */
											bool_t BgL_test2679z00_5196;

											{	/* Eval/evobject.scm 314 */
												bool_t BgL_test2680z00_5197;

												{	/* Eval/evobject.scm 80 */
													obj_t BgL_sz00_3188;

													BgL_sz00_3188 = CAR(((obj_t) BgL_l1114z00_1529));
													BgL_test2680z00_5197 =
														CBOOL(STRUCT_REF(BgL_sz00_3188, (int) (5L)));
												}
												if (BgL_test2680z00_5197)
													{	/* Eval/evobject.scm 314 */
														BgL_test2679z00_5196 = ((bool_t) 0);
													}
												else
													{	/* Eval/evobject.scm 314 */
														BgL_test2679z00_5196 = ((bool_t) 1);
													}
											}
											if (BgL_test2679z00_5196)
												{	/* Eval/evobject.scm 314 */
													obj_t BgL_nh1116z00_1536;

													{	/* Eval/evobject.scm 314 */
														obj_t BgL_arg1480z00_1538;

														BgL_arg1480z00_1538 =
															CAR(((obj_t) BgL_l1114z00_1529));
														BgL_nh1116z00_1536 =
															MAKE_YOUNG_PAIR(BgL_arg1480z00_1538, BNIL);
													}
													SET_CDR(BgL_h1115z00_1530, BgL_nh1116z00_1536);
													{	/* Eval/evobject.scm 314 */
														obj_t BgL_arg1479z00_1537;

														BgL_arg1479z00_1537 =
															CDR(((obj_t) BgL_l1114z00_1529));
														{
															obj_t BgL_h1115z00_5210;
															obj_t BgL_l1114z00_5209;

															BgL_l1114z00_5209 = BgL_arg1479z00_1537;
															BgL_h1115z00_5210 = BgL_nh1116z00_1536;
															BgL_h1115z00_1530 = BgL_h1115z00_5210;
															BgL_l1114z00_1529 = BgL_l1114z00_5209;
															goto BgL_zc3z04anonymousza31475ze3z87_1531;
														}
													}
												}
											else
												{	/* Eval/evobject.scm 314 */
													obj_t BgL_arg1481z00_1539;

													BgL_arg1481z00_1539 =
														CDR(((obj_t) BgL_l1114z00_1529));
													{
														obj_t BgL_l1114z00_5213;

														BgL_l1114z00_5213 = BgL_arg1481z00_1539;
														BgL_l1114z00_1529 = BgL_l1114z00_5213;
														goto BgL_zc3z04anonymousza31475ze3z87_1531;
													}
												}
										}
								}
							}
							{	/* Eval/evobject.scm 315 */
								obj_t BgL_list1482z00_1543;

								BgL_list1482z00_1543 = MAKE_YOUNG_PAIR(BgL_offsetz00_77, BNIL);
								BgL_ll1119z00_1511 =
									BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(
									(int) (BgL_siza7eza7_76), BgL_list1482z00_1543);
							}
							if (NULLP(BgL_ll1118z00_1510))
								{	/* Eval/evobject.scm 312 */
									BgL_arg1467z00_1508 = BNIL;
								}
							else
								{	/* Eval/evobject.scm 312 */
									obj_t BgL_head1120z00_1513;

									BgL_head1120z00_1513 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_ll1118z00_3220;
										obj_t BgL_ll1119z00_3221;
										obj_t BgL_tail1121z00_3222;

										BgL_ll1118z00_3220 = BgL_ll1118z00_1510;
										BgL_ll1119z00_3221 = BgL_ll1119z00_1511;
										BgL_tail1121z00_3222 = BgL_head1120z00_1513;
									BgL_zc3z04anonymousza31470ze3z87_3219:
										if (NULLP(BgL_ll1118z00_3220))
											{	/* Eval/evobject.scm 312 */
												BgL_arg1467z00_1508 = CDR(BgL_head1120z00_1513);
											}
										else
											{	/* Eval/evobject.scm 312 */
												obj_t BgL_newtail1122z00_3231;

												{	/* Eval/evobject.scm 312 */
													obj_t BgL_arg1474z00_3232;

													{	/* Eval/evobject.scm 312 */
														obj_t BgL_slotz00_3233;
														obj_t BgL_indexz00_3234;

														BgL_slotz00_3233 =
															CAR(((obj_t) BgL_ll1118z00_3220));
														BgL_indexz00_3234 =
															CAR(((obj_t) BgL_ll1119z00_3221));
														BgL_sz00_1562 = BgL_slotz00_3233;
														BgL_iz00_1563 = BgL_indexz00_3234;
														BgL_classz00_1564 = BgL_classz00_74;
														{	/* Eval/evobject.scm 299 */
															obj_t BgL_defsz00_1566;

															BgL_defsz00_1566 =
																BGl_classgenzd2slotzd2anonymousz00zz__evobjectz00
																(BgL_iz00_1563, BgL_sz00_1562,
																BgL_classz00_1564);
															{	/* Eval/evobject.scm 301 */
																obj_t BgL_arg1501z00_1567;
																obj_t BgL_arg1502z00_1568;
																obj_t BgL_arg1503z00_1569;
																obj_t BgL_arg1504z00_1570;
																obj_t BgL_arg1505z00_1571;
																obj_t BgL_arg1506z00_1572;
																obj_t BgL_arg1507z00_1573;

																BgL_arg1501z00_1567 =
																	STRUCT_REF(
																	((obj_t) BgL_sz00_1562), (int) (0L));
																BgL_arg1502z00_1568 =
																	CAR(((obj_t) BgL_defsz00_1566));
																{	/* Eval/evobject.scm 302 */
																	obj_t BgL_pairz00_3181;

																	BgL_pairz00_3181 =
																		CDR(((obj_t) BgL_defsz00_1566));
																	BgL_arg1503z00_1569 = CAR(BgL_pairz00_3181);
																}
																BgL_arg1504z00_1570 =
																	STRUCT_REF(
																	((obj_t) BgL_sz00_1562), (int) (2L));
																{	/* Eval/evobject.scm 304 */
																	obj_t BgL_arg1508z00_1574;

																	BgL_arg1508z00_1574 =
																		STRUCT_REF(
																		((obj_t) BgL_sz00_1562), (int) (7L));
																	{	/* Eval/eval.scm 82 */
																		obj_t BgL_envz00_1576;

																		BgL_envz00_1576 =
																			BGl_defaultzd2environmentzd2zz__evalz00();
																		{	/* Eval/eval.scm 82 */

																			BgL_arg1505z00_1571 =
																				BGl_evalz12z12zz__evalz00
																				(BgL_arg1508z00_1574, BgL_envz00_1576);
																}}}
																BgL_arg1506z00_1572 =
																	STRUCT_REF(
																	((obj_t) BgL_sz00_1562), (int) (3L));
																BgL_arg1507z00_1573 =
																	STRUCT_REF(
																	((obj_t) BgL_sz00_1562), (int) (1L));
																BgL_arg1474z00_3232 =
																	BGl_makezd2classzd2fieldz00zz__objectz00
																	(BgL_arg1501z00_1567, BgL_arg1502z00_1568,
																	BgL_arg1503z00_1569,
																	CBOOL(BgL_arg1504z00_1570), ((bool_t) 0),
																	BgL_arg1505z00_1571, BgL_arg1506z00_1572,
																	BgL_arg1507z00_1573);
													}}}
													BgL_newtail1122z00_3231 =
														MAKE_YOUNG_PAIR(BgL_arg1474z00_3232, BNIL);
												}
												SET_CDR(BgL_tail1121z00_3222, BgL_newtail1122z00_3231);
												{	/* Eval/evobject.scm 312 */
													obj_t BgL_arg1472z00_3235;
													obj_t BgL_arg1473z00_3236;

													BgL_arg1472z00_3235 =
														CDR(((obj_t) BgL_ll1118z00_3220));
													BgL_arg1473z00_3236 =
														CDR(((obj_t) BgL_ll1119z00_3221));
													{
														obj_t BgL_tail1121z00_5260;
														obj_t BgL_ll1119z00_5259;
														obj_t BgL_ll1118z00_5258;

														BgL_ll1118z00_5258 = BgL_arg1472z00_3235;
														BgL_ll1119z00_5259 = BgL_arg1473z00_3236;
														BgL_tail1121z00_5260 = BgL_newtail1122z00_3231;
														BgL_tail1121z00_3222 = BgL_tail1121z00_5260;
														BgL_ll1119z00_3221 = BgL_ll1119z00_5259;
														BgL_ll1118z00_3220 = BgL_ll1118z00_5258;
														goto BgL_zc3z04anonymousza31470ze3z87_3219;
													}
												}
											}
									}
								}
						}
						{	/* Eval/evobject.scm 316 */
							obj_t BgL_list1484z00_1545;

							BgL_list1484z00_1545 = MAKE_YOUNG_PAIR(BgL_slotsz00_75, BNIL);
							BgL_arg1468z00_1509 =
								BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
								(BGl_proc2473z00zz__evobjectz00, BgL_list1484z00_1545);
						}
						BgL_arg1466z00_1507 =
							BGl_appendzd221011zd2zz__evobjectz00(BgL_arg1467z00_1508,
							BgL_arg1468z00_1509);
					}
					return
						BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_arg1466z00_1507);
				}
			}
		}

	}



/* &<@anonymous:1485> */
	obj_t BGl_z62zc3z04anonymousza31485ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4359, obj_t BgL_slotz00_4360)
	{
		{	/* Eval/evobject.scm 316 */
			{
				obj_t BgL_sz00_4539;

				if (CBOOL(STRUCT_REF(((obj_t) BgL_slotz00_4360), (int) (5L))))
					{	/* Eval/evobject.scm 317 */
						BgL_sz00_4539 = BgL_slotz00_4360;
						{	/* Eval/evobject.scm 291 */
							obj_t BgL_arg1488z00_4540;
							obj_t BgL_arg1489z00_4541;
							obj_t BgL_arg1490z00_4542;
							obj_t BgL_arg1492z00_4543;
							obj_t BgL_arg1494z00_4544;
							obj_t BgL_arg1495z00_4545;
							obj_t BgL_arg1497z00_4546;

							BgL_arg1488z00_4540 =
								STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (0L));
							BgL_arg1489z00_4541 =
								STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (5L));
							BgL_arg1490z00_4542 =
								STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (6L));
							BgL_arg1492z00_4543 =
								STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (2L));
							{	/* Eval/evobject.scm 294 */
								obj_t BgL_arg1498z00_4547;

								BgL_arg1498z00_4547 =
									STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (7L));
								{	/* Eval/evobject.scm 294 */
									obj_t BgL_envz00_4548;

									BgL_envz00_4548 = BGl_defaultzd2environmentzd2zz__evalz00();
									{	/* Eval/evobject.scm 294 */

										BgL_arg1494z00_4544 =
											BGl_evalz12z12zz__evalz00(BgL_arg1498z00_4547,
											BgL_envz00_4548);
							}}}
							BgL_arg1495z00_4545 =
								STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (3L));
							BgL_arg1497z00_4546 =
								STRUCT_REF(((obj_t) BgL_sz00_4539), (int) (1L));
							return
								BGl_makezd2classzd2fieldz00zz__objectz00(BgL_arg1488z00_4540,
								BgL_arg1489z00_4541, BgL_arg1490z00_4542,
								CBOOL(BgL_arg1492z00_4543), ((bool_t) 1), BgL_arg1494z00_4544,
								BgL_arg1495z00_4545, BgL_arg1497z00_4546);
						}
					}
				else
					{	/* Eval/evobject.scm 317 */
						return BFALSE;
					}
			}
		}

	}



/* make-class-virtual-fields */
	obj_t BGl_makezd2classzd2virtualzd2fieldszd2zz__evobjectz00(obj_t
		BgL_slotsz00_78)
	{
		{	/* Eval/evobject.scm 324 */
			{	/* Eval/evobject.scm 327 */
				obj_t BgL_arg1509z00_1579;

				{	/* Eval/evobject.scm 326 */
					obj_t BgL_list1511z00_1581;

					BgL_list1511z00_1581 = MAKE_YOUNG_PAIR(BgL_slotsz00_78, BNIL);
					BgL_arg1509z00_1579 =
						BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
						(BGl_proc2474z00zz__evobjectz00, BgL_list1511z00_1581);
				}
				return
					BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_arg1509z00_1579);
			}
		}

	}



/* &<@anonymous:1512> */
	obj_t BGl_z62zc3z04anonymousza31512ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4362, obj_t BgL_slotz00_4363)
	{
		{	/* Eval/evobject.scm 326 */
			if (CBOOL(STRUCT_REF(((obj_t) BgL_slotz00_4363), (int) (5L))))
				{	/* Eval/evobject.scm 328 */
					obj_t BgL_arg1514z00_4549;
					obj_t BgL_arg1516z00_4550;

					BgL_arg1514z00_4549 =
						STRUCT_REF(((obj_t) BgL_slotz00_4363), (int) (4L));
					{	/* Eval/evobject.scm 329 */
						obj_t BgL_arg1517z00_4551;
						obj_t BgL_arg1521z00_4552;

						BgL_arg1517z00_4551 =
							STRUCT_REF(((obj_t) BgL_slotz00_4363), (int) (5L));
						BgL_arg1521z00_4552 =
							STRUCT_REF(((obj_t) BgL_slotz00_4363), (int) (6L));
						BgL_arg1516z00_4550 =
							MAKE_YOUNG_PAIR(BgL_arg1517z00_4551, BgL_arg1521z00_4552);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1514z00_4549, BgL_arg1516z00_4550);
				}
			else
				{	/* Eval/evobject.scm 327 */
					return BFALSE;
				}
		}

	}



/* eval-expand-instantiate */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2expandzd2instantiatez00zz__evobjectz00(obj_t
		BgL_classz00_79)
	{
		{	/* Eval/evobject.scm 335 */
			{	/* Eval/evobject.scm 336 */
				obj_t BgL_widz00_1590;

				{	/* Eval/evobject.scm 336 */
					obj_t BgL_arg1523z00_1592;

					{	/* Eval/evobject.scm 336 */
						obj_t BgL_arg1524z00_1593;
						obj_t BgL_arg1525z00_1594;

						{	/* Eval/evobject.scm 336 */
							obj_t BgL_symbolz00_3249;

							BgL_symbolz00_3249 = BGl_symbol2475z00zz__evobjectz00;
							{	/* Eval/evobject.scm 336 */
								obj_t BgL_arg2230z00_3250;

								BgL_arg2230z00_3250 = SYMBOL_TO_STRING(BgL_symbolz00_3249);
								BgL_arg1524z00_1593 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2230z00_3250);
							}
						}
						{	/* Eval/evobject.scm 336 */
							obj_t BgL_arg1526z00_1595;

							BgL_arg1526z00_1595 =
								BGl_classzd2namezd2zz__objectz00(BgL_classz00_79);
							{	/* Eval/evobject.scm 336 */
								obj_t BgL_arg2230z00_3252;

								BgL_arg2230z00_3252 = SYMBOL_TO_STRING(BgL_arg1526z00_1595);
								BgL_arg1525z00_1594 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2230z00_3252);
							}
						}
						BgL_arg1523z00_1592 =
							string_append(BgL_arg1524z00_1593, BgL_arg1525z00_1594);
					}
					BgL_widz00_1590 = bstring_to_symbol(BgL_arg1523z00_1592);
				}
				return
					BGl_installzd2expanderzd2zz__macroz00(BgL_widz00_1590,
					BGl_evalzd2instantiatezd2expanderz00zz__evobjectz00(BgL_classz00_79));
			}
		}

	}



/* &eval-expand-instantiate */
	obj_t BGl_z62evalzd2expandzd2instantiatez62zz__evobjectz00(obj_t
		BgL_envz00_4364, obj_t BgL_classz00_4365)
	{
		{	/* Eval/evobject.scm 335 */
			{	/* Eval/evobject.scm 336 */
				obj_t BgL_auxz00_5323;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_4365))
					{	/* Eval/evobject.scm 336 */
						BgL_auxz00_5323 = BgL_classz00_4365;
					}
				else
					{
						obj_t BgL_auxz00_5326;

						BgL_auxz00_5326 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(12797L), BGl_string2478z00zz__evobjectz00,
							BGl_string2479z00zz__evobjectz00, BgL_classz00_4365);
						FAILURE(BgL_auxz00_5326, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2expandzd2instantiatez00zz__evobjectz00(BgL_auxz00_5323);
			}
		}

	}



/* eval-instantiate-expander */
	obj_t BGl_evalzd2instantiatezd2expanderz00zz__evobjectz00(obj_t
		BgL_classz00_80)
	{
		{	/* Eval/evobject.scm 343 */
			{	/* Eval/evobject.scm 344 */
				obj_t BgL_zc3z04anonymousza31527ze3z87_4366;

				BgL_zc3z04anonymousza31527ze3z87_4366 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31527ze3ze5zz__evobjectz00,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31527ze3z87_4366,
					(int) (0L), BgL_classz00_80);
				return BgL_zc3z04anonymousza31527ze3z87_4366;
			}
		}

	}



/* &<@anonymous:1527> */
	obj_t BGl_z62zc3z04anonymousza31527ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4367, obj_t BgL_xz00_4369, obj_t BgL_ez00_4370)
	{
		{	/* Eval/evobject.scm 344 */
			{	/* Eval/evobject.scm 345 */
				obj_t BgL_classz00_4368;

				BgL_classz00_4368 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4367, (int) (0L)));
				{	/* Eval/evobject.scm 345 */
					obj_t BgL_arg1528z00_4553;
					obj_t BgL_arg1529z00_4554;
					obj_t BgL_arg1530z00_4555;
					obj_t BgL_arg1531z00_4556;

					BgL_arg1528z00_4553 = CAR(((obj_t) BgL_xz00_4369));
					BgL_arg1529z00_4554 = CDR(((obj_t) BgL_xz00_4369));
					BgL_arg1530z00_4555 = BGL_CLASS_ALL_FIELDS(BgL_classz00_4368);
					{	/* Eval/evobject.scm 347 */
						obj_t BgL_arg1535z00_4557;

						BgL_arg1535z00_4557 =
							MAKE_YOUNG_PAIR(BGl_classzd2allocatorzd2zz__objectz00
							(BgL_classz00_4368), BNIL);
						BgL_arg1531z00_4556 =
							BGl_localiza7eza7zz__evobjectz00(BgL_xz00_4369,
							BgL_arg1535z00_4557);
					}
					return
						BGl_instantiatezd2fillzd2zz__evobjectz00(BgL_arg1528z00_4553,
						BgL_arg1529z00_4554, BgL_classz00_4368, BgL_arg1530z00_4555,
						BgL_arg1531z00_4556, BgL_xz00_4369, BgL_ez00_4370);
				}
			}
		}

	}



/* instantiate-fill */
	obj_t BGl_instantiatezd2fillzd2zz__evobjectz00(obj_t BgL_opz00_81,
		obj_t BgL_providedz00_82, obj_t BgL_classz00_83, obj_t BgL_fieldsz00_84,
		obj_t BgL_initz00_85, obj_t BgL_xz00_86, obj_t BgL_ez00_87)
	{
		{	/* Eval/evobject.scm 352 */
			{
				obj_t BgL_argsz00_1700;
				obj_t BgL_fieldsz00_1701;
				obj_t BgL_fieldsz00_1656;

				{	/* Eval/evobject.scm 403 */
					obj_t BgL_newz00_1607;

					BgL_newz00_1607 =
						BGl_gensymz00zz__r4_symbols_6_4z00
						(BGl_symbol2488z00zz__evobjectz00);
					{	/* Eval/evobject.scm 403 */
						obj_t BgL_argsz00_1608;

						BgL_fieldsz00_1656 = BgL_fieldsz00_84;
						{	/* Eval/evobject.scm 355 */
							obj_t BgL_vargsz00_1659;

							BgL_vargsz00_1659 =
								make_vector(VECTOR_LENGTH(BgL_fieldsz00_1656), BUNSPEC);
							{	/* Eval/evobject.scm 356 */

								{
									long BgL_iz00_1661;

									BgL_iz00_1661 = 0L;
								BgL_zc3z04anonymousza31593ze3z87_1662:
									if ((BgL_iz00_1661 < VECTOR_LENGTH(BgL_fieldsz00_1656)))
										{	/* Eval/evobject.scm 360 */
											obj_t BgL_sz00_1664;

											BgL_sz00_1664 =
												VECTOR_REF(BgL_fieldsz00_1656, BgL_iz00_1661);
											if (BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00
												(BgL_sz00_1664))
												{	/* Eval/evobject.scm 365 */
													obj_t BgL_arg1598z00_1666;

													{	/* Eval/evobject.scm 365 */
														obj_t BgL_arg1601z00_1667;

														{	/* Eval/evobject.scm 365 */
															obj_t BgL_arg1602z00_1668;
															obj_t BgL_arg1603z00_1669;

															{	/* Eval/evobject.scm 365 */
																obj_t BgL_arg1605z00_1670;

																{	/* Eval/evobject.scm 365 */
																	obj_t BgL_arg1606z00_1671;

																	BgL_arg1606z00_1671 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2480z00zz__evobjectz00, BNIL);
																	BgL_arg1605z00_1670 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2482z00zz__evobjectz00,
																		BgL_arg1606z00_1671);
																}
																BgL_arg1602z00_1668 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2484z00zz__evobjectz00,
																	BgL_arg1605z00_1670);
															}
															BgL_arg1603z00_1669 =
																MAKE_YOUNG_PAIR(BgL_sz00_1664, BNIL);
															BgL_arg1601z00_1667 =
																MAKE_YOUNG_PAIR(BgL_arg1602z00_1668,
																BgL_arg1603z00_1669);
														}
														BgL_arg1598z00_1666 =
															MAKE_YOUNG_PAIR(BTRUE, BgL_arg1601z00_1667);
													}
													VECTOR_SET(BgL_vargsz00_1659, BgL_iz00_1661,
														BgL_arg1598z00_1666);
												}
											else
												{	/* Eval/evobject.scm 368 */
													obj_t BgL_arg1607z00_1672;

													BgL_arg1607z00_1672 =
														MAKE_YOUNG_PAIR(BFALSE, BUNSPEC);
													VECTOR_SET(BgL_vargsz00_1659, BgL_iz00_1661,
														BgL_arg1607z00_1672);
												}
											{
												long BgL_iz00_5366;

												BgL_iz00_5366 = (BgL_iz00_1661 + 1L);
												BgL_iz00_1661 = BgL_iz00_5366;
												goto BgL_zc3z04anonymousza31593ze3z87_1662;
											}
										}
									else
										{	/* Eval/evobject.scm 359 */
											((bool_t) 0);
										}
								}
								{
									obj_t BgL_providedz00_1676;

									BgL_providedz00_1676 = BgL_providedz00_82;
								BgL_zc3z04anonymousza31609ze3z87_1677:
									if (PAIRP(BgL_providedz00_1676))
										{	/* Eval/evobject.scm 373 */
											obj_t BgL_pz00_1679;

											BgL_pz00_1679 = CAR(BgL_providedz00_1676);
											if (PAIRP(BgL_pz00_1679))
												{	/* Eval/evobject.scm 374 */
													obj_t BgL_carzd2108zd2_1686;
													obj_t BgL_cdrzd2109zd2_1687;

													BgL_carzd2108zd2_1686 = CAR(((obj_t) BgL_pz00_1679));
													BgL_cdrzd2109zd2_1687 = CDR(((obj_t) BgL_pz00_1679));
													if (SYMBOLP(BgL_carzd2108zd2_1686))
														{	/* Eval/evobject.scm 374 */
															if (PAIRP(BgL_cdrzd2109zd2_1687))
																{	/* Eval/evobject.scm 374 */
																	if (NULLP(CDR(BgL_cdrzd2109zd2_1687)))
																		{	/* Eval/evobject.scm 374 */
																			obj_t BgL_arg1617z00_1692;

																			BgL_arg1617z00_1692 =
																				CAR(BgL_cdrzd2109zd2_1687);
																			{	/* Eval/evobject.scm 377 */
																				obj_t BgL_pvalz00_3311;

																				{	/* Eval/evobject.scm 379 */
																					obj_t BgL_arg1620z00_3312;

																					BgL_arg1620z00_3312 =
																						BGl_findzd2fieldzd2offsetz00zz__evobjectz00
																						(BgL_fieldsz00_1656,
																						BgL_carzd2108zd2_1686, BgL_opz00_81,
																						BgL_pz00_1679);
																					BgL_pvalz00_3311 =
																						VECTOR_REF(BgL_vargsz00_1659,
																						(long) CINT(BgL_arg1620z00_3312));
																				}
																				{	/* Eval/evobject.scm 380 */
																					obj_t BgL_tmpz00_5388;

																					BgL_tmpz00_5388 =
																						((obj_t) BgL_pvalz00_3311);
																					SET_CAR(BgL_tmpz00_5388, BTRUE);
																				}
																				{	/* Eval/evobject.scm 381 */
																					obj_t BgL_arg1619z00_3313;

																					BgL_arg1619z00_3313 =
																						BGl_localiza7eza7zz__evobjectz00
																						(BgL_pz00_1679,
																						BgL_arg1617z00_1692);
																					{	/* Eval/evobject.scm 381 */
																						obj_t BgL_tmpz00_5392;

																						BgL_tmpz00_5392 =
																							((obj_t) BgL_pvalz00_3311);
																						SET_CDR(BgL_tmpz00_5392,
																							BgL_arg1619z00_3313);
																		}}}}
																	else
																		{	/* Eval/evobject.scm 374 */
																			BGl_expandzd2errorzd2zz__evobjectz00
																				(BgL_opz00_81,
																				BGl_string2486z00zz__evobjectz00,
																				BgL_pz00_1679);
																		}
																}
															else
																{	/* Eval/evobject.scm 374 */
																	BGl_expandzd2errorzd2zz__evobjectz00
																		(BgL_opz00_81,
																		BGl_string2486z00zz__evobjectz00,
																		BgL_pz00_1679);
																}
														}
													else
														{	/* Eval/evobject.scm 374 */
															BGl_expandzd2errorzd2zz__evobjectz00(BgL_opz00_81,
																BGl_string2486z00zz__evobjectz00,
																BgL_pz00_1679);
														}
												}
											else
												{	/* Eval/evobject.scm 374 */
													BGl_expandzd2errorzd2zz__evobjectz00(BgL_opz00_81,
														BGl_string2486z00zz__evobjectz00, BgL_pz00_1679);
												}
											{
												obj_t BgL_providedz00_5399;

												BgL_providedz00_5399 = CDR(BgL_providedz00_1676);
												BgL_providedz00_1676 = BgL_providedz00_5399;
												goto BgL_zc3z04anonymousza31609ze3z87_1677;
											}
										}
									else
										{	/* Eval/evobject.scm 372 */
											((bool_t) 0);
										}
								}
								BgL_argsz00_1608 = BgL_vargsz00_1659;
							}
						}
						{	/* Eval/evobject.scm 404 */

							BgL_argsz00_1700 = BgL_argsz00_1608;
							BgL_fieldsz00_1701 = BgL_fieldsz00_84;
							{
								long BgL_iz00_1705;

								BgL_iz00_1705 = 0L;
							BgL_zc3z04anonymousza31624ze3z87_1706:
								if ((BgL_iz00_1705 < VECTOR_LENGTH(BgL_fieldsz00_1701)))
									{	/* Eval/evobject.scm 391 */
										{	/* Eval/evobject.scm 392 */
											obj_t BgL_az00_1708;
											obj_t BgL_sz00_1709;

											BgL_az00_1708 =
												VECTOR_REF(BgL_argsz00_1700, BgL_iz00_1705);
											BgL_sz00_1709 =
												VECTOR_REF(BgL_fieldsz00_1701, BgL_iz00_1705);
											if (CBOOL(CAR(((obj_t) BgL_az00_1708))))
												{	/* Eval/evobject.scm 394 */
													BFALSE;
												}
											else
												{	/* Eval/evobject.scm 394 */
													if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00
														(BgL_sz00_1709))
														{	/* Eval/evobject.scm 395 */
															BFALSE;
														}
													else
														{	/* Eval/evobject.scm 399 */
															obj_t BgL_arg1628z00_1712;

															{	/* Eval/evobject.scm 399 */
																obj_t BgL_arg1629z00_1713;

																BgL_arg1629z00_1713 =
																	BGl_classzd2fieldzd2namez00zz__objectz00
																	(BgL_sz00_1709);
																{	/* Eval/evobject.scm 398 */
																	obj_t BgL_list1630z00_1714;

																	BgL_list1630z00_1714 =
																		MAKE_YOUNG_PAIR(BgL_arg1629z00_1713, BNIL);
																	BgL_arg1628z00_1712 =
																		BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string2487z00zz__evobjectz00,
																		BgL_list1630z00_1714);
																}
															}
															BGl_expandzd2errorzd2zz__evobjectz00(BgL_opz00_81,
																BgL_arg1628z00_1712, BgL_xz00_86);
														}
												}
										}
										{
											long BgL_iz00_5416;

											BgL_iz00_5416 = (BgL_iz00_1705 + 1L);
											BgL_iz00_1705 = BgL_iz00_5416;
											goto BgL_zc3z04anonymousza31624ze3z87_1706;
										}
									}
								else
									{	/* Eval/evobject.scm 391 */
										((bool_t) 0);
									}
							}
							{	/* Eval/evobject.scm 410 */
								obj_t BgL_arg1537z00_1609;

								{	/* Eval/evobject.scm 410 */
									obj_t BgL_arg1539z00_1610;

									{	/* Eval/evobject.scm 410 */
										obj_t BgL_arg1540z00_1611;
										obj_t BgL_arg1543z00_1612;

										{	/* Eval/evobject.scm 410 */
											obj_t BgL_arg1544z00_1613;

											{	/* Eval/evobject.scm 410 */
												obj_t BgL_arg1546z00_1614;

												{	/* Eval/evobject.scm 410 */
													obj_t BgL_arg1547z00_1615;

													BgL_arg1547z00_1615 =
														BGL_PROCEDURE_CALL2(BgL_ez00_87, BgL_initz00_85,
														BgL_ez00_87);
													BgL_arg1546z00_1614 =
														MAKE_YOUNG_PAIR(BgL_arg1547z00_1615, BNIL);
												}
												BgL_arg1544z00_1613 =
													MAKE_YOUNG_PAIR(BgL_newz00_1607, BgL_arg1546z00_1614);
											}
											BgL_arg1540z00_1611 =
												MAKE_YOUNG_PAIR(BgL_arg1544z00_1613, BNIL);
										}
										{	/* Eval/evobject.scm 415 */
											obj_t BgL_arg1549z00_1616;

											{	/* Eval/evobject.scm 415 */
												obj_t BgL_arg1552z00_1617;

												{	/* Eval/evobject.scm 415 */
													obj_t BgL_arg1553z00_1618;
													obj_t BgL_arg1554z00_1619;

													{	/* Eval/evobject.scm 415 */
														obj_t BgL_zc3z04anonymousza31556ze3z87_4372;

														{
															int BgL_tmpz00_5426;

															BgL_tmpz00_5426 = (int) (2L);
															BgL_zc3z04anonymousza31556ze3z87_4372 =
																MAKE_L_PROCEDURE
																(BGl_z62zc3z04anonymousza31556ze3ze5zz__evobjectz00,
																BgL_tmpz00_5426);
														}
														PROCEDURE_L_SET
															(BgL_zc3z04anonymousza31556ze3z87_4372,
															(int) (0L), BgL_ez00_87);
														PROCEDURE_L_SET
															(BgL_zc3z04anonymousza31556ze3z87_4372,
															(int) (1L), BgL_newz00_1607);
														BgL_arg1553z00_1618 =
															BGl_vectorzd2filterzd2mapz00zz__evobjectz00
															(BgL_zc3z04anonymousza31556ze3z87_4372,
															BgL_fieldsz00_84, BgL_argsz00_1608);
													}
													{	/* Eval/evobject.scm 421 */
														obj_t BgL_arg1565z00_1632;
														obj_t BgL_arg1567z00_1633;

														{	/* Eval/evobject.scm 421 */
															obj_t BgL_constrz00_1634;

															{
																obj_t BgL_cz00_3330;

																BgL_cz00_3330 = BgL_classz00_83;
															BgL_findzd2classzd2constructorz00_3329:
																{	/* Eval/evobject.scm 259 */
																	obj_t BgL_constz00_3334;

																	BgL_constz00_3334 =
																		BGl_classzd2constructorzd2zz__objectz00
																		(BgL_cz00_3330);
																	if (CBOOL(BgL_constz00_3334))
																		{	/* Eval/evobject.scm 260 */
																			BgL_constrz00_1634 = BgL_constz00_3334;
																		}
																	else
																		{	/* Eval/evobject.scm 262 */
																			obj_t BgL_sz00_3335;

																			BgL_sz00_3335 =
																				BGl_classzd2superzd2zz__objectz00
																				(BgL_cz00_3330);
																			if (BGl_classzf3zf3zz__objectz00
																				(BgL_sz00_3335))
																				{
																					obj_t BgL_cz00_5440;

																					BgL_cz00_5440 = BgL_sz00_3335;
																					BgL_cz00_3330 = BgL_cz00_5440;
																					goto
																						BgL_findzd2classzd2constructorz00_3329;
																				}
																			else
																				{	/* Eval/evobject.scm 263 */
																					BgL_constrz00_1634 = BFALSE;
																				}
																		}
																}
															}
															if (CBOOL(BgL_constrz00_1634))
																{	/* Eval/evobject.scm 423 */
																	obj_t BgL_arg1571z00_1635;

																	{	/* Eval/evobject.scm 423 */
																		obj_t BgL_arg1573z00_1636;

																		{	/* Eval/evobject.scm 423 */
																			obj_t BgL_arg1575z00_1637;

																			BgL_arg1575z00_1637 =
																				MAKE_YOUNG_PAIR(BgL_newz00_1607, BNIL);
																			BgL_arg1573z00_1636 =
																				MAKE_YOUNG_PAIR(BgL_constrz00_1634,
																				BgL_arg1575z00_1637);
																		}
																		BgL_arg1571z00_1635 =
																			BGl_localiza7eza7zz__evobjectz00
																			(BgL_xz00_86, BgL_arg1573z00_1636);
																	}
																	BgL_arg1565z00_1632 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_87,
																		BgL_arg1571z00_1635, BgL_ez00_87);
																}
															else
																{	/* Eval/evobject.scm 422 */
																	BgL_arg1565z00_1632 = BFALSE;
																}
														}
														{	/* Eval/evobject.scm 427 */
															obj_t BgL_arg1576z00_1638;
															obj_t BgL_arg1578z00_1639;

															{	/* Eval/evobject.scm 427 */
																obj_t BgL_zc3z04anonymousza31580ze3z87_4371;

																{
																	int BgL_tmpz00_5451;

																	BgL_tmpz00_5451 = (int) (2L);
																	BgL_zc3z04anonymousza31580ze3z87_4371 =
																		MAKE_L_PROCEDURE
																		(BGl_z62zc3z04anonymousza31580ze3ze5zz__evobjectz00,
																		BgL_tmpz00_5451);
																}
																PROCEDURE_L_SET
																	(BgL_zc3z04anonymousza31580ze3z87_4371,
																	(int) (0L), BgL_ez00_87);
																PROCEDURE_L_SET
																	(BgL_zc3z04anonymousza31580ze3z87_4371,
																	(int) (1L), BgL_newz00_1607);
																BgL_arg1576z00_1638 =
																	BGl_vectorzd2filterzd2mapz00zz__evobjectz00
																	(BgL_zc3z04anonymousza31580ze3z87_4371,
																	BgL_fieldsz00_84, BgL_argsz00_1608);
															}
															BgL_arg1578z00_1639 =
																MAKE_YOUNG_PAIR(BgL_newz00_1607, BNIL);
															BgL_arg1567z00_1633 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_arg1576z00_1638, BgL_arg1578z00_1639);
														}
														BgL_arg1554z00_1619 =
															MAKE_YOUNG_PAIR(BgL_arg1565z00_1632,
															BgL_arg1567z00_1633);
													}
													BgL_arg1552z00_1617 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1553z00_1618, BgL_arg1554z00_1619);
												}
												BgL_arg1549z00_1616 =
													MAKE_YOUNG_PAIR(BGl_symbol2490z00zz__evobjectz00,
													BgL_arg1552z00_1617);
											}
											BgL_arg1543z00_1612 =
												MAKE_YOUNG_PAIR(BgL_arg1549z00_1616, BNIL);
										}
										BgL_arg1539z00_1610 =
											MAKE_YOUNG_PAIR(BgL_arg1540z00_1611, BgL_arg1543z00_1612);
									}
									BgL_arg1537z00_1609 =
										MAKE_YOUNG_PAIR(BGl_symbol2492z00zz__evobjectz00,
										BgL_arg1539z00_1610);
								}
								return
									BGl_localiza7eza7zz__evobjectz00(BgL_xz00_86,
									BgL_arg1537z00_1609);
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1580> */
	obj_t BGl_z62zc3z04anonymousza31580ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4373, obj_t BgL_fieldz00_4376, obj_t BgL_valz00_4377)
	{
		{	/* Eval/evobject.scm 426 */
			{	/* Eval/evobject.scm 427 */
				obj_t BgL_ez00_4374;
				obj_t BgL_newz00_4375;

				BgL_ez00_4374 = PROCEDURE_L_REF(BgL_envz00_4373, (int) (0L));
				BgL_newz00_4375 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4373, (int) (1L)));
				{	/* Eval/evobject.scm 427 */
					bool_t BgL_test2699z00_5473;

					if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_fieldz00_4376))
						{	/* Eval/evobject.scm 427 */
							if (BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00
								(BgL_fieldz00_4376))
								{	/* Eval/evobject.scm 428 */
									BgL_test2699z00_5473 = CBOOL(CAR(((obj_t) BgL_valz00_4377)));
								}
							else
								{	/* Eval/evobject.scm 428 */
									BgL_test2699z00_5473 = ((bool_t) 0);
								}
						}
					else
						{	/* Eval/evobject.scm 427 */
							BgL_test2699z00_5473 = ((bool_t) 0);
						}
					if (BgL_test2699z00_5473)
						{	/* Eval/evobject.scm 430 */
							obj_t BgL_vz00_4558;

							{	/* Eval/evobject.scm 430 */
								obj_t BgL_arg1591z00_4559;

								BgL_arg1591z00_4559 = CDR(((obj_t) BgL_valz00_4377));
								BgL_vz00_4558 =
									BGL_PROCEDURE_CALL2(BgL_ez00_4374, BgL_arg1591z00_4559,
									BgL_ez00_4374);
							}
							{	/* Eval/evobject.scm 432 */
								obj_t BgL_arg1585z00_4560;

								{	/* Eval/evobject.scm 432 */
									obj_t BgL_arg1586z00_4561;
									obj_t BgL_arg1587z00_4562;

									BgL_arg1586z00_4561 =
										BGl_classzd2fieldzd2mutatorz00zz__objectz00
										(BgL_fieldz00_4376);
									{	/* Eval/evobject.scm 432 */
										obj_t BgL_arg1589z00_4563;

										BgL_arg1589z00_4563 = MAKE_YOUNG_PAIR(BgL_vz00_4558, BNIL);
										BgL_arg1587z00_4562 =
											MAKE_YOUNG_PAIR(BgL_newz00_4375, BgL_arg1589z00_4563);
									}
									BgL_arg1585z00_4560 =
										MAKE_YOUNG_PAIR(BgL_arg1586z00_4561, BgL_arg1587z00_4562);
								}
								return
									BGl_localiza7eza7zz__evobjectz00(BgL_valz00_4377,
									BgL_arg1585z00_4560);
							}
						}
					else
						{	/* Eval/evobject.scm 427 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:1556> */
	obj_t BGl_z62zc3z04anonymousza31556ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4378, obj_t BgL_fieldz00_4381, obj_t BgL_valz00_4382)
	{
		{	/* Eval/evobject.scm 414 */
			{	/* Eval/evobject.scm 415 */
				obj_t BgL_ez00_4379;
				obj_t BgL_newz00_4380;

				BgL_ez00_4379 = PROCEDURE_L_REF(BgL_envz00_4378, (int) (0L));
				BgL_newz00_4380 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4378, (int) (1L)));
				if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_fieldz00_4381))
					{	/* Eval/evobject.scm 415 */
						return BFALSE;
					}
				else
					{	/* Eval/evobject.scm 416 */
						obj_t BgL_vz00_4564;

						{	/* Eval/evobject.scm 416 */
							obj_t BgL_arg1564z00_4565;

							BgL_arg1564z00_4565 = CDR(((obj_t) BgL_valz00_4382));
							BgL_vz00_4564 =
								BGL_PROCEDURE_CALL2(BgL_ez00_4379, BgL_arg1564z00_4565,
								BgL_ez00_4379);
						}
						{	/* Eval/evobject.scm 418 */
							obj_t BgL_arg1558z00_4566;

							{	/* Eval/evobject.scm 418 */
								obj_t BgL_arg1559z00_4567;
								obj_t BgL_arg1561z00_4568;

								BgL_arg1559z00_4567 =
									BGl_classzd2fieldzd2mutatorz00zz__objectz00
									(BgL_fieldz00_4381);
								{	/* Eval/evobject.scm 418 */
									obj_t BgL_arg1562z00_4569;

									BgL_arg1562z00_4569 = MAKE_YOUNG_PAIR(BgL_vz00_4564, BNIL);
									BgL_arg1561z00_4568 =
										MAKE_YOUNG_PAIR(BgL_newz00_4380, BgL_arg1562z00_4569);
								}
								BgL_arg1558z00_4566 =
									MAKE_YOUNG_PAIR(BgL_arg1559z00_4567, BgL_arg1561z00_4568);
							}
							return
								BGl_localiza7eza7zz__evobjectz00(BgL_valz00_4382,
								BgL_arg1558z00_4566);
						}
					}
			}
		}

	}



/* vector-filter-map */
	obj_t BGl_vectorzd2filterzd2mapz00zz__evobjectz00(obj_t BgL_fz00_88,
		obj_t BgL_v1z00_89, obj_t BgL_v2z00_90)
	{
		{	/* Eval/evobject.scm 440 */
			{
				long BgL_iz00_3348;
				obj_t BgL_accz00_3349;

				BgL_iz00_3348 = 0L;
				BgL_accz00_3349 = BNIL;
			BgL_loopz00_3347:
				if ((BgL_iz00_3348 == VECTOR_LENGTH(BgL_v1z00_89)))
					{	/* Eval/evobject.scm 444 */
						return bgl_reverse_bang(BgL_accz00_3349);
					}
				else
					{	/* Eval/evobject.scm 446 */
						obj_t BgL_vz00_3353;

						{	/* Eval/evobject.scm 446 */
							obj_t BgL_arg1637z00_3354;
							obj_t BgL_arg1638z00_3355;

							BgL_arg1637z00_3354 = VECTOR_REF(BgL_v1z00_89, BgL_iz00_3348);
							BgL_arg1638z00_3355 = VECTOR_REF(BgL_v2z00_90, BgL_iz00_3348);
							BgL_vz00_3353 =
								((obj_t(*)(obj_t, obj_t,
										obj_t)) PROCEDURE_L_ENTRY(BgL_fz00_88)) (BgL_fz00_88,
								BgL_arg1637z00_3354, BgL_arg1638z00_3355);
						}
						{	/* Eval/evobject.scm 447 */
							long BgL_arg1634z00_3360;
							obj_t BgL_arg1636z00_3361;

							BgL_arg1634z00_3360 = (BgL_iz00_3348 + 1L);
							if (CBOOL(BgL_vz00_3353))
								{	/* Eval/evobject.scm 447 */
									BgL_arg1636z00_3361 =
										MAKE_YOUNG_PAIR(BgL_vz00_3353, BgL_accz00_3349);
								}
							else
								{	/* Eval/evobject.scm 447 */
									BgL_arg1636z00_3361 = BgL_accz00_3349;
								}
							{
								obj_t BgL_accz00_5528;
								long BgL_iz00_5527;

								BgL_iz00_5527 = BgL_arg1634z00_3360;
								BgL_accz00_5528 = BgL_arg1636z00_3361;
								BgL_accz00_3349 = BgL_accz00_5528;
								BgL_iz00_3348 = BgL_iz00_5527;
								goto BgL_loopz00_3347;
							}
						}
					}
			}
		}

	}



/* eval-expand-duplicate */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2expandzd2duplicatez00zz__evobjectz00(obj_t
		BgL_classz00_91)
	{
		{	/* Eval/evobject.scm 452 */
			{	/* Eval/evobject.scm 453 */
				obj_t BgL_didz00_1732;

				{	/* Eval/evobject.scm 453 */
					obj_t BgL_arg1640z00_1734;

					{	/* Eval/evobject.scm 453 */
						obj_t BgL_arg1641z00_1735;
						obj_t BgL_arg1642z00_1736;

						{	/* Eval/evobject.scm 453 */
							obj_t BgL_symbolz00_3363;

							BgL_symbolz00_3363 = BGl_symbol2494z00zz__evobjectz00;
							{	/* Eval/evobject.scm 453 */
								obj_t BgL_arg2230z00_3364;

								BgL_arg2230z00_3364 = SYMBOL_TO_STRING(BgL_symbolz00_3363);
								BgL_arg1641z00_1735 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2230z00_3364);
							}
						}
						{	/* Eval/evobject.scm 453 */
							obj_t BgL_arg1643z00_1737;

							BgL_arg1643z00_1737 =
								BGl_classzd2namezd2zz__objectz00(BgL_classz00_91);
							{	/* Eval/evobject.scm 453 */
								obj_t BgL_arg2230z00_3366;

								BgL_arg2230z00_3366 = SYMBOL_TO_STRING(BgL_arg1643z00_1737);
								BgL_arg1642z00_1736 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2230z00_3366);
							}
						}
						BgL_arg1640z00_1734 =
							string_append(BgL_arg1641z00_1735, BgL_arg1642z00_1736);
					}
					BgL_didz00_1732 = bstring_to_symbol(BgL_arg1640z00_1734);
				}
				return
					BGl_installzd2expanderzd2zz__macroz00(BgL_didz00_1732,
					BGl_evalzd2duplicatezd2expanderz00zz__evobjectz00(BgL_classz00_91));
			}
		}

	}



/* &eval-expand-duplicate */
	obj_t BGl_z62evalzd2expandzd2duplicatez62zz__evobjectz00(obj_t
		BgL_envz00_4383, obj_t BgL_classz00_4384)
	{
		{	/* Eval/evobject.scm 452 */
			{	/* Eval/evobject.scm 453 */
				obj_t BgL_auxz00_5538;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_4384))
					{	/* Eval/evobject.scm 453 */
						BgL_auxz00_5538 = BgL_classz00_4384;
					}
				else
					{
						obj_t BgL_auxz00_5541;

						BgL_auxz00_5541 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(16779L), BGl_string2496z00zz__evobjectz00,
							BGl_string2479z00zz__evobjectz00, BgL_classz00_4384);
						FAILURE(BgL_auxz00_5541, BFALSE, BFALSE);
					}
				return BGl_evalzd2expandzd2duplicatez00zz__evobjectz00(BgL_auxz00_5538);
			}
		}

	}



/* eval-duplicate-expander */
	obj_t BGl_evalzd2duplicatezd2expanderz00zz__evobjectz00(obj_t BgL_classz00_92)
	{
		{	/* Eval/evobject.scm 460 */
			{	/* Eval/evobject.scm 461 */
				obj_t BgL_zc3z04anonymousza31644ze3z87_4385;

				BgL_zc3z04anonymousza31644ze3z87_4385 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31644ze3ze5zz__evobjectz00,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31644ze3z87_4385,
					(int) (0L), BgL_classz00_92);
				return BgL_zc3z04anonymousza31644ze3z87_4385;
			}
		}

	}



/* &<@anonymous:1644> */
	obj_t BGl_z62zc3z04anonymousza31644ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4386, obj_t BgL_xz00_4388, obj_t BgL_ez00_4389)
	{
		{	/* Eval/evobject.scm 461 */
			{	/* Eval/evobject.scm 461 */
				obj_t BgL_classz00_4387;

				BgL_classz00_4387 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4386, (int) (0L)));
				if (PAIRP(BgL_xz00_4388))
					{	/* Eval/evobject.scm 461 */
						obj_t BgL_cdrzd2127zd2_4570;

						BgL_cdrzd2127zd2_4570 = CDR(((obj_t) BgL_xz00_4388));
						if (PAIRP(BgL_cdrzd2127zd2_4570))
							{	/* Eval/evobject.scm 461 */
								return
									BGl_duplicatezd2expanderzd2zz__evobjectz00(BgL_classz00_4387,
									CAR(BgL_cdrzd2127zd2_4570),
									CDR(BgL_cdrzd2127zd2_4570), BgL_xz00_4388, BgL_ez00_4389);
							}
						else
							{	/* Eval/evobject.scm 461 */
								return
									BGl_expandzd2errorzd2zz__evobjectz00
									(BGl_string2497z00zz__evobjectz00,
									BGl_string2498z00zz__evobjectz00, BgL_xz00_4388);
							}
					}
				else
					{	/* Eval/evobject.scm 461 */
						return
							BGl_expandzd2errorzd2zz__evobjectz00
							(BGl_string2497z00zz__evobjectz00,
							BGl_string2498z00zz__evobjectz00, BgL_xz00_4388);
					}
			}
		}

	}



/* duplicate-expander */
	obj_t BGl_duplicatezd2expanderzd2zz__evobjectz00(obj_t BgL_classz00_93,
		obj_t BgL_duplicatedz00_94, obj_t BgL_providedz00_95, obj_t BgL_xz00_96,
		obj_t BgL_ez00_97)
	{
		{	/* Eval/evobject.scm 471 */
			{
				obj_t BgL_fieldsz00_1805;
				obj_t BgL_dupvarz00_1806;

				{	/* Eval/evobject.scm 505 */
					obj_t BgL_fieldsz00_1754;

					{	/* Eval/evobject.scm 505 */
						obj_t BgL_tmpz00_5565;

						BgL_tmpz00_5565 = ((obj_t) BgL_classz00_93);
						BgL_fieldsz00_1754 = BGL_CLASS_ALL_FIELDS(BgL_tmpz00_5565);
					}
					{	/* Eval/evobject.scm 505 */
						obj_t BgL_newz00_1755;

						BgL_newz00_1755 =
							BGl_gensymz00zz__r4_symbols_6_4z00
							(BGl_symbol2488z00zz__evobjectz00);
						{	/* Eval/evobject.scm 506 */
							obj_t BgL_dupvarz00_1756;

							BgL_dupvarz00_1756 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_symbol2499z00zz__evobjectz00);
							{	/* Eval/evobject.scm 507 */
								obj_t BgL_argsz00_1757;

								BgL_fieldsz00_1805 = BgL_fieldsz00_1754;
								BgL_dupvarz00_1806 = BgL_dupvarz00_1756;
								{	/* Eval/evobject.scm 474 */
									obj_t BgL_vargsz00_1809;

									BgL_vargsz00_1809 =
										make_vector(VECTOR_LENGTH(BgL_fieldsz00_1805), BUNSPEC);
									{	/* Eval/evobject.scm 475 */

										{
											obj_t BgL_providedz00_1811;

											BgL_providedz00_1811 = BgL_providedz00_95;
										BgL_zc3z04anonymousza31711ze3z87_1812:
											if (PAIRP(BgL_providedz00_1811))
												{	/* Eval/evobject.scm 479 */
													obj_t BgL_pz00_1814;

													BgL_pz00_1814 = CAR(BgL_providedz00_1811);
													if (PAIRP(BgL_pz00_1814))
														{	/* Eval/evobject.scm 480 */
															obj_t BgL_carzd2142zd2_1821;
															obj_t BgL_cdrzd2143zd2_1822;

															BgL_carzd2142zd2_1821 =
																CAR(((obj_t) BgL_pz00_1814));
															BgL_cdrzd2143zd2_1822 =
																CDR(((obj_t) BgL_pz00_1814));
															if (SYMBOLP(BgL_carzd2142zd2_1821))
																{	/* Eval/evobject.scm 480 */
																	if (PAIRP(BgL_cdrzd2143zd2_1822))
																		{	/* Eval/evobject.scm 480 */
																			if (NULLP(CDR(BgL_cdrzd2143zd2_1822)))
																				{	/* Eval/evobject.scm 480 */
																					obj_t BgL_arg1720z00_1827;

																					BgL_arg1720z00_1827 =
																						CAR(BgL_cdrzd2143zd2_1822);
																					{	/* Eval/evobject.scm 483 */
																						obj_t BgL_iz00_3455;

																						BgL_iz00_3455 =
																							BGl_findzd2fieldzd2offsetz00zz__evobjectz00
																							(BgL_fieldsz00_1805,
																							BgL_carzd2142zd2_1821,
																							BGl_string2497z00zz__evobjectz00,
																							BgL_pz00_1814);
																						{	/* Eval/evobject.scm 486 */
																							obj_t BgL_arg1723z00_3456;

																							BgL_arg1723z00_3456 =
																								MAKE_YOUNG_PAIR(BTRUE,
																								BGl_localiza7eza7zz__evobjectz00
																								(BgL_pz00_1814,
																									BgL_arg1720z00_1827));
																							VECTOR_SET(BgL_vargsz00_1809,
																								(long) CINT(BgL_iz00_3455),
																								BgL_arg1723z00_3456);
																				}}}
																			else
																				{	/* Eval/evobject.scm 480 */
																					BGl_expandzd2errorzd2zz__evobjectz00
																						(CAR(BgL_xz00_96),
																						BGl_string2498z00zz__evobjectz00,
																						BgL_xz00_96);
																				}
																		}
																	else
																		{	/* Eval/evobject.scm 480 */
																			BGl_expandzd2errorzd2zz__evobjectz00(CAR
																				(BgL_xz00_96),
																				BGl_string2498z00zz__evobjectz00,
																				BgL_xz00_96);
																		}
																}
															else
																{	/* Eval/evobject.scm 480 */
																	BGl_expandzd2errorzd2zz__evobjectz00(CAR
																		(BgL_xz00_96),
																		BGl_string2498z00zz__evobjectz00,
																		BgL_xz00_96);
																}
														}
													else
														{	/* Eval/evobject.scm 480 */
															BGl_expandzd2errorzd2zz__evobjectz00(CAR
																(BgL_xz00_96), BGl_string2498z00zz__evobjectz00,
																BgL_xz00_96);
														}
													{
														obj_t BgL_providedz00_5602;

														BgL_providedz00_5602 = CDR(BgL_providedz00_1811);
														BgL_providedz00_1811 = BgL_providedz00_5602;
														goto BgL_zc3z04anonymousza31711ze3z87_1812;
													}
												}
											else
												{	/* Eval/evobject.scm 478 */
													((bool_t) 0);
												}
										}
										{
											long BgL_iz00_1836;

											BgL_iz00_1836 = 0L;
										BgL_zc3z04anonymousza31727ze3z87_1837:
											if ((BgL_iz00_1836 < VECTOR_LENGTH(BgL_fieldsz00_1805)))
												{	/* Eval/evobject.scm 493 */
													obj_t BgL_valuez00_1839;

													BgL_valuez00_1839 =
														VECTOR_REF(BgL_vargsz00_1809, BgL_iz00_1836);
													if (PAIRP(BgL_valuez00_1839))
														{	/* Eval/evobject.scm 494 */
															BFALSE;
														}
													else
														{	/* Eval/evobject.scm 495 */
															obj_t BgL_fieldz00_1841;

															BgL_fieldz00_1841 =
																VECTOR_REF(BgL_fieldsz00_1805, BgL_iz00_1836);
															{	/* Eval/evobject.scm 500 */
																obj_t BgL_arg1730z00_1842;

																{	/* Eval/evobject.scm 500 */
																	obj_t BgL_arg1731z00_1843;

																	{	/* Eval/evobject.scm 500 */
																		obj_t BgL_arg1733z00_1844;
																		obj_t BgL_arg1734z00_1845;

																		BgL_arg1733z00_1844 =
																			BGl_classzd2fieldzd2accessorz00zz__objectz00
																			(BgL_fieldz00_1841);
																		BgL_arg1734z00_1845 =
																			MAKE_YOUNG_PAIR(BgL_dupvarz00_1806, BNIL);
																		BgL_arg1731z00_1843 =
																			MAKE_YOUNG_PAIR(BgL_arg1733z00_1844,
																			BgL_arg1734z00_1845);
																	}
																	BgL_arg1730z00_1842 =
																		MAKE_YOUNG_PAIR(BTRUE, BgL_arg1731z00_1843);
																}
																VECTOR_SET(BgL_vargsz00_1809, BgL_iz00_1836,
																	BgL_arg1730z00_1842);
															}
														}
													{
														long BgL_iz00_5616;

														BgL_iz00_5616 = (BgL_iz00_1836 + 1L);
														BgL_iz00_1836 = BgL_iz00_5616;
														goto BgL_zc3z04anonymousza31727ze3z87_1837;
													}
												}
											else
												{	/* Eval/evobject.scm 492 */
													((bool_t) 0);
												}
										}
										BgL_argsz00_1757 = BgL_vargsz00_1809;
									}
								}
								{	/* Eval/evobject.scm 508 */

									{	/* Eval/evobject.scm 511 */
										obj_t BgL_arg1651z00_1758;

										{	/* Eval/evobject.scm 511 */
											obj_t BgL_arg1652z00_1759;
											obj_t BgL_arg1653z00_1760;

											{	/* Eval/evobject.scm 511 */
												obj_t BgL_arg1654z00_1761;
												obj_t BgL_arg1656z00_1762;

												{	/* Eval/evobject.scm 511 */
													obj_t BgL_arg1657z00_1763;

													{	/* Eval/evobject.scm 511 */
														obj_t BgL_arg1658z00_1764;

														BgL_arg1658z00_1764 =
															BGL_PROCEDURE_CALL2(BgL_ez00_97,
															BgL_duplicatedz00_94, BgL_ez00_97);
														BgL_arg1657z00_1763 =
															MAKE_YOUNG_PAIR(BgL_arg1658z00_1764, BNIL);
													}
													BgL_arg1654z00_1761 =
														MAKE_YOUNG_PAIR(BgL_dupvarz00_1756,
														BgL_arg1657z00_1763);
												}
												{	/* Eval/evobject.scm 512 */
													obj_t BgL_arg1661z00_1765;

													{	/* Eval/evobject.scm 512 */
														obj_t BgL_arg1663z00_1766;

														{	/* Eval/evobject.scm 512 */
															obj_t BgL_arg1664z00_1767;

															{	/* Eval/evobject.scm 512 */
																obj_t BgL_arg1667z00_1768;

																BgL_arg1667z00_1768 =
																	MAKE_YOUNG_PAIR
																	(BGl_classzd2allocatorzd2zz__objectz00
																	(BgL_classz00_93), BNIL);
																BgL_arg1664z00_1767 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_97,
																	BgL_arg1667z00_1768, BgL_ez00_97);
															}
															BgL_arg1663z00_1766 =
																MAKE_YOUNG_PAIR(BgL_arg1664z00_1767, BNIL);
														}
														BgL_arg1661z00_1765 =
															MAKE_YOUNG_PAIR(BgL_newz00_1755,
															BgL_arg1663z00_1766);
													}
													BgL_arg1656z00_1762 =
														MAKE_YOUNG_PAIR(BgL_arg1661z00_1765, BNIL);
												}
												BgL_arg1652z00_1759 =
													MAKE_YOUNG_PAIR(BgL_arg1654z00_1761,
													BgL_arg1656z00_1762);
											}
											{	/* Eval/evobject.scm 517 */
												obj_t BgL_arg1669z00_1770;

												{	/* Eval/evobject.scm 517 */
													obj_t BgL_arg1670z00_1771;

													{	/* Eval/evobject.scm 517 */
														obj_t BgL_arg1675z00_1772;
														obj_t BgL_arg1676z00_1773;

														{	/* Eval/evobject.scm 517 */
															obj_t BgL_zc3z04anonymousza31679ze3z87_4391;

															{
																int BgL_tmpz00_5636;

																BgL_tmpz00_5636 = (int) (2L);
																BgL_zc3z04anonymousza31679ze3z87_4391 =
																	MAKE_L_PROCEDURE
																	(BGl_z62zc3z04anonymousza31679ze3ze5zz__evobjectz00,
																	BgL_tmpz00_5636);
															}
															PROCEDURE_L_SET
																(BgL_zc3z04anonymousza31679ze3z87_4391,
																(int) (0L), BgL_ez00_97);
															PROCEDURE_L_SET
																(BgL_zc3z04anonymousza31679ze3z87_4391,
																(int) (1L), BgL_newz00_1755);
															BgL_arg1675z00_1772 =
																BGl_vectorzd2filterzd2mapz00zz__evobjectz00
																(BgL_zc3z04anonymousza31679ze3z87_4391,
																BgL_fieldsz00_1754, BgL_argsz00_1757);
														}
														{	/* Eval/evobject.scm 522 */
															obj_t BgL_arg1689z00_1785;
															obj_t BgL_arg1691z00_1786;

															{	/* Eval/evobject.scm 522 */
																obj_t BgL_constrz00_1787;

																{
																	obj_t BgL_cz00_3487;

																	BgL_cz00_3487 = BgL_classz00_93;
																BgL_findzd2classzd2constructorz00_3486:
																	{	/* Eval/evobject.scm 259 */
																		obj_t BgL_constz00_3491;

																		BgL_constz00_3491 =
																			BGl_classzd2constructorzd2zz__objectz00
																			(BgL_cz00_3487);
																		if (CBOOL(BgL_constz00_3491))
																			{	/* Eval/evobject.scm 260 */
																				BgL_constrz00_1787 = BgL_constz00_3491;
																			}
																		else
																			{	/* Eval/evobject.scm 262 */
																				obj_t BgL_sz00_3492;

																				BgL_sz00_3492 =
																					BGl_classzd2superzd2zz__objectz00
																					(BgL_cz00_3487);
																				if (BGl_classzf3zf3zz__objectz00
																					(BgL_sz00_3492))
																					{
																						obj_t BgL_cz00_5650;

																						BgL_cz00_5650 = BgL_sz00_3492;
																						BgL_cz00_3487 = BgL_cz00_5650;
																						goto
																							BgL_findzd2classzd2constructorz00_3486;
																					}
																				else
																					{	/* Eval/evobject.scm 263 */
																						BgL_constrz00_1787 = BFALSE;
																					}
																			}
																	}
																}
																if (CBOOL(BgL_constrz00_1787))
																	{	/* Eval/evobject.scm 524 */
																		obj_t BgL_arg1692z00_1788;

																		{	/* Eval/evobject.scm 524 */
																			obj_t BgL_arg1699z00_1789;

																			BgL_arg1699z00_1789 =
																				MAKE_YOUNG_PAIR(BgL_newz00_1755, BNIL);
																			BgL_arg1692z00_1788 =
																				MAKE_YOUNG_PAIR(BgL_constrz00_1787,
																				BgL_arg1699z00_1789);
																		}
																		BgL_arg1689z00_1785 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_97,
																			BgL_arg1692z00_1788, BgL_ez00_97);
																	}
																else
																	{	/* Eval/evobject.scm 523 */
																		BgL_arg1689z00_1785 = BFALSE;
																	}
															}
															{	/* Eval/evobject.scm 528 */
																obj_t BgL_arg1700z00_1790;
																obj_t BgL_arg1701z00_1791;

																{	/* Eval/evobject.scm 528 */
																	obj_t BgL_zc3z04anonymousza31703ze3z87_4390;

																	{
																		int BgL_tmpz00_5660;

																		BgL_tmpz00_5660 = (int) (2L);
																		BgL_zc3z04anonymousza31703ze3z87_4390 =
																			MAKE_L_PROCEDURE
																			(BGl_z62zc3z04anonymousza31703ze3ze5zz__evobjectz00,
																			BgL_tmpz00_5660);
																	}
																	PROCEDURE_L_SET
																		(BgL_zc3z04anonymousza31703ze3z87_4390,
																		(int) (0L), BgL_ez00_97);
																	PROCEDURE_L_SET
																		(BgL_zc3z04anonymousza31703ze3z87_4390,
																		(int) (1L), BgL_newz00_1755);
																	BgL_arg1700z00_1790 =
																		BGl_vectorzd2filterzd2mapz00zz__evobjectz00
																		(BgL_zc3z04anonymousza31703ze3z87_4390,
																		BgL_fieldsz00_1754, BgL_argsz00_1757);
																}
																BgL_arg1701z00_1791 =
																	MAKE_YOUNG_PAIR(BgL_newz00_1755, BNIL);
																BgL_arg1691z00_1786 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_arg1700z00_1790, BgL_arg1701z00_1791);
															}
															BgL_arg1676z00_1773 =
																MAKE_YOUNG_PAIR(BgL_arg1689z00_1785,
																BgL_arg1691z00_1786);
														}
														BgL_arg1670z00_1771 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1675z00_1772, BgL_arg1676z00_1773);
													}
													BgL_arg1669z00_1770 =
														MAKE_YOUNG_PAIR(BGl_symbol2490z00zz__evobjectz00,
														BgL_arg1670z00_1771);
												}
												BgL_arg1653z00_1760 =
													MAKE_YOUNG_PAIR(BgL_arg1669z00_1770, BNIL);
											}
											BgL_arg1651z00_1758 =
												MAKE_YOUNG_PAIR(BgL_arg1652z00_1759,
												BgL_arg1653z00_1760);
										}
										return
											MAKE_YOUNG_PAIR(BGl_symbol2492z00zz__evobjectz00,
											BgL_arg1651z00_1758);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1703> */
	obj_t BGl_z62zc3z04anonymousza31703ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4392, obj_t BgL_fieldz00_4395, obj_t BgL_valz00_4396)
	{
		{	/* Eval/evobject.scm 527 */
			{	/* Eval/evobject.scm 528 */
				obj_t BgL_ez00_4393;
				obj_t BgL_newz00_4394;

				BgL_ez00_4393 = PROCEDURE_L_REF(BgL_envz00_4392, (int) (0L));
				BgL_newz00_4394 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4392, (int) (1L)));
				{	/* Eval/evobject.scm 528 */
					bool_t BgL_test2718z00_5681;

					if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_fieldz00_4395))
						{	/* Eval/evobject.scm 528 */
							BgL_test2718z00_5681 =
								BGl_classzd2fieldzd2mutablezf3zf3zz__objectz00
								(BgL_fieldz00_4395);
						}
					else
						{	/* Eval/evobject.scm 528 */
							BgL_test2718z00_5681 = ((bool_t) 0);
						}
					if (BgL_test2718z00_5681)
						{	/* Eval/evobject.scm 530 */
							obj_t BgL_vz00_4571;

							{	/* Eval/evobject.scm 530 */
								obj_t BgL_arg1709z00_4572;

								BgL_arg1709z00_4572 = CDR(((obj_t) BgL_valz00_4396));
								BgL_vz00_4571 =
									BGL_PROCEDURE_CALL2(BgL_ez00_4393, BgL_arg1709z00_4572,
									BgL_ez00_4393);
							}
							{	/* Eval/evobject.scm 531 */
								obj_t BgL_arg1706z00_4573;
								obj_t BgL_arg1707z00_4574;

								BgL_arg1706z00_4573 =
									BGl_classzd2fieldzd2mutatorz00zz__objectz00
									(BgL_fieldz00_4395);
								{	/* Eval/evobject.scm 531 */
									obj_t BgL_arg1708z00_4575;

									BgL_arg1708z00_4575 = MAKE_YOUNG_PAIR(BgL_vz00_4571, BNIL);
									BgL_arg1707z00_4574 =
										MAKE_YOUNG_PAIR(BgL_newz00_4394, BgL_arg1708z00_4575);
								}
								return
									MAKE_YOUNG_PAIR(BgL_arg1706z00_4573, BgL_arg1707z00_4574);
							}
						}
					else
						{	/* Eval/evobject.scm 528 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &<@anonymous:1679> */
	obj_t BGl_z62zc3z04anonymousza31679ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4397, obj_t BgL_fieldz00_4400, obj_t BgL_valz00_4401)
	{
		{	/* Eval/evobject.scm 516 */
			{	/* Eval/evobject.scm 517 */
				obj_t BgL_ez00_4398;
				obj_t BgL_newz00_4399;

				BgL_ez00_4398 = PROCEDURE_L_REF(BgL_envz00_4397, (int) (0L));
				BgL_newz00_4399 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4397, (int) (1L)));
				if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_fieldz00_4400))
					{	/* Eval/evobject.scm 517 */
						return BFALSE;
					}
				else
					{	/* Eval/evobject.scm 518 */
						obj_t BgL_vz00_4576;

						{	/* Eval/evobject.scm 518 */
							obj_t BgL_arg1688z00_4577;

							BgL_arg1688z00_4577 = CDR(((obj_t) BgL_valz00_4401));
							BgL_vz00_4576 =
								BGL_PROCEDURE_CALL2(BgL_ez00_4398, BgL_arg1688z00_4577,
								BgL_ez00_4398);
						}
						{	/* Eval/evobject.scm 519 */
							obj_t BgL_arg1681z00_4578;
							obj_t BgL_arg1684z00_4579;

							BgL_arg1681z00_4578 =
								BGl_classzd2fieldzd2mutatorz00zz__objectz00(BgL_fieldz00_4400);
							{	/* Eval/evobject.scm 519 */
								obj_t BgL_arg1685z00_4580;

								BgL_arg1685z00_4580 = MAKE_YOUNG_PAIR(BgL_vz00_4576, BNIL);
								BgL_arg1684z00_4579 =
									MAKE_YOUNG_PAIR(BgL_newz00_4399, BgL_arg1685z00_4580);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1681z00_4578, BgL_arg1684z00_4579);
						}
					}
			}
		}

	}



/* find-field-offset */
	obj_t BGl_findzd2fieldzd2offsetz00zz__evobjectz00(obj_t BgL_fieldsz00_98,
		obj_t BgL_namez00_99, obj_t BgL_formz00_100, obj_t BgL_sexpz00_101)
	{
		{	/* Eval/evobject.scm 539 */
			{
				long BgL_iz00_1852;

				BgL_iz00_1852 = 0L;
			BgL_zc3z04anonymousza31737ze3z87_1853:
				if ((BgL_iz00_1852 == VECTOR_LENGTH(BgL_fieldsz00_98)))
					{	/* Eval/evobject.scm 545 */
						obj_t BgL_arg1739z00_1855;

						{	/* Eval/evobject.scm 545 */
							obj_t BgL_list1740z00_1856;

							{	/* Eval/evobject.scm 545 */
								obj_t BgL_arg1741z00_1857;

								BgL_arg1741z00_1857 = MAKE_YOUNG_PAIR(BgL_namez00_99, BNIL);
								BgL_list1740z00_1856 =
									MAKE_YOUNG_PAIR(BgL_formz00_100, BgL_arg1741z00_1857);
							}
							BgL_arg1739z00_1855 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2501z00zz__evobjectz00, BgL_list1740z00_1856);
						}
						return
							BGl_expandzd2errorzd2zz__evobjectz00(BgL_namez00_99,
							BgL_arg1739z00_1855, BgL_sexpz00_101);
					}
				else
					{	/* Eval/evobject.scm 543 */
						if (
							(BGl_classzd2fieldzd2namez00zz__objectz00(VECTOR_REF
									(BgL_fieldsz00_98, BgL_iz00_1852)) == BgL_namez00_99))
							{	/* Eval/evobject.scm 547 */
								return BINT(BgL_iz00_1852);
							}
						else
							{
								long BgL_iz00_5726;

								BgL_iz00_5726 = (BgL_iz00_1852 + 1L);
								BgL_iz00_1852 = BgL_iz00_5726;
								goto BgL_zc3z04anonymousza31737ze3z87_1853;
							}
					}
			}
		}

	}



/* eval-expand-with-access */
	BGL_EXPORTED_DEF obj_t
		BGl_evalzd2expandzd2withzd2accesszd2zz__evobjectz00(obj_t BgL_classz00_102)
	{
		{	/* Eval/evobject.scm 555 */
			{	/* Eval/evobject.scm 556 */
				obj_t BgL_widz00_1865;

				{	/* Eval/evobject.scm 556 */
					obj_t BgL_arg1750z00_1867;

					{	/* Eval/evobject.scm 556 */
						obj_t BgL_arg1751z00_1868;
						obj_t BgL_arg1752z00_1869;

						{	/* Eval/evobject.scm 556 */
							obj_t BgL_symbolz00_3501;

							BgL_symbolz00_3501 = BGl_symbol2502z00zz__evobjectz00;
							{	/* Eval/evobject.scm 556 */
								obj_t BgL_arg2230z00_3502;

								BgL_arg2230z00_3502 = SYMBOL_TO_STRING(BgL_symbolz00_3501);
								BgL_arg1751z00_1868 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2230z00_3502);
							}
						}
						{	/* Eval/evobject.scm 556 */
							obj_t BgL_arg1753z00_1870;

							BgL_arg1753z00_1870 =
								BGl_classzd2namezd2zz__objectz00(BgL_classz00_102);
							{	/* Eval/evobject.scm 556 */
								obj_t BgL_arg2230z00_3504;

								BgL_arg2230z00_3504 = SYMBOL_TO_STRING(BgL_arg1753z00_1870);
								BgL_arg1752z00_1869 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg2230z00_3504);
							}
						}
						BgL_arg1750z00_1867 =
							string_append(BgL_arg1751z00_1868, BgL_arg1752z00_1869);
					}
					BgL_widz00_1865 = bstring_to_symbol(BgL_arg1750z00_1867);
				}
				return
					BGl_installzd2expanderzd2zz__macroz00(BgL_widz00_1865,
					BGl_evalzd2withzd2accesszd2expanderzd2zz__evobjectz00
					(BgL_classz00_102));
			}
		}

	}



/* &eval-expand-with-access */
	obj_t BGl_z62evalzd2expandzd2withzd2accesszb0zz__evobjectz00(obj_t
		BgL_envz00_4402, obj_t BgL_classz00_4403)
	{
		{	/* Eval/evobject.scm 555 */
			{	/* Eval/evobject.scm 556 */
				obj_t BgL_auxz00_5737;

				if (BGl_classzf3zf3zz__objectz00(BgL_classz00_4403))
					{	/* Eval/evobject.scm 556 */
						BgL_auxz00_5737 = BgL_classz00_4403;
					}
				else
					{
						obj_t BgL_auxz00_5740;

						BgL_auxz00_5740 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(20459L), BGl_string2504z00zz__evobjectz00,
							BGl_string2479z00zz__evobjectz00, BgL_classz00_4403);
						FAILURE(BgL_auxz00_5740, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2expandzd2withzd2accesszd2zz__evobjectz00(BgL_auxz00_5737);
			}
		}

	}



/* eval-with-access-expander */
	obj_t BGl_evalzd2withzd2accesszd2expanderzd2zz__evobjectz00(obj_t
		BgL_classz00_103)
	{
		{	/* Eval/evobject.scm 562 */
			{	/* Eval/evobject.scm 564 */
				obj_t BgL_zc3z04anonymousza31754ze3z87_4404;

				BgL_zc3z04anonymousza31754ze3z87_4404 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31754ze3ze5zz__evobjectz00,
					(int) (2L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31754ze3z87_4404,
					(int) (0L), BgL_classz00_103);
				return BgL_zc3z04anonymousza31754ze3z87_4404;
			}
		}

	}



/* &<@anonymous:1754> */
	obj_t BGl_z62zc3z04anonymousza31754ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4405, obj_t BgL_xz00_4407, obj_t BgL_ez00_4408)
	{
		{	/* Eval/evobject.scm 564 */
			{	/* Eval/evobject.scm 564 */
				obj_t BgL_classz00_4406;

				BgL_classz00_4406 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4405, (int) (0L)));
				{
					obj_t BgL_waccessz00_4582;
					obj_t BgL_instancez00_4583;
					obj_t BgL_fieldsz00_4584;
					obj_t BgL_bodyz00_4585;

					if (PAIRP(BgL_xz00_4407))
						{	/* Eval/evobject.scm 564 */
							obj_t BgL_cdrzd2163zd2_4631;

							BgL_cdrzd2163zd2_4631 = CDR(((obj_t) BgL_xz00_4407));
							if (PAIRP(BgL_cdrzd2163zd2_4631))
								{	/* Eval/evobject.scm 564 */
									obj_t BgL_cdrzd2169zd2_4632;

									BgL_cdrzd2169zd2_4632 = CDR(BgL_cdrzd2163zd2_4631);
									if (PAIRP(BgL_cdrzd2169zd2_4632))
										{	/* Eval/evobject.scm 564 */
											obj_t BgL_carzd2173zd2_4633;
											obj_t BgL_cdrzd2174zd2_4634;

											BgL_carzd2173zd2_4633 = CAR(BgL_cdrzd2169zd2_4632);
											BgL_cdrzd2174zd2_4634 = CDR(BgL_cdrzd2169zd2_4632);
											if (PAIRP(BgL_carzd2173zd2_4633))
												{	/* Eval/evobject.scm 564 */
													if (PAIRP(BgL_cdrzd2174zd2_4634))
														{	/* Eval/evobject.scm 564 */
															obj_t BgL_arg1760z00_4635;
															obj_t BgL_arg1761z00_4636;

															BgL_arg1760z00_4635 =
																CAR(((obj_t) BgL_xz00_4407));
															BgL_arg1761z00_4636 = CAR(BgL_cdrzd2163zd2_4631);
															BgL_waccessz00_4582 = BgL_arg1760z00_4635;
															BgL_instancez00_4583 = BgL_arg1761z00_4636;
															BgL_fieldsz00_4584 = BgL_carzd2173zd2_4633;
															BgL_bodyz00_4585 = BgL_cdrzd2174zd2_4634;
															{
																obj_t BgL_sz00_4587;
																obj_t BgL_nfieldsz00_4588;

																BgL_sz00_4587 = BgL_fieldsz00_4584;
																BgL_nfieldsz00_4588 = BNIL;
															BgL_loopz00_4586:
																if (NULLP(BgL_sz00_4587))
																	{	/* Eval/evobject.scm 571 */
																		obj_t BgL_instancez00_4589;

																		BgL_instancez00_4589 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_4408,
																			BgL_instancez00_4583, BgL_ez00_4408);
																		{	/* Eval/evobject.scm 571 */
																			obj_t BgL_auxz00_4590;

																			BgL_auxz00_4590 =
																				BGl_gensymz00zz__r4_symbols_6_4z00
																				(BGl_symbol2505z00zz__evobjectz00);
																			{	/* Eval/evobject.scm 572 */
																				obj_t BgL_tauxz00_4591;

																				{	/* Eval/evobject.scm 573 */
																					obj_t BgL_arg1786z00_4592;

																					BgL_arg1786z00_4592 =
																						BGl_classzd2namezd2zz__objectz00
																						(BgL_classz00_4406);
																					{	/* Eval/evobject.scm 573 */
																						obj_t BgL_list1787z00_4593;

																						{	/* Eval/evobject.scm 573 */
																							obj_t BgL_arg1788z00_4594;

																							{	/* Eval/evobject.scm 573 */
																								obj_t BgL_arg1789z00_4595;

																								BgL_arg1789z00_4595 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1786z00_4592, BNIL);
																								BgL_arg1788z00_4594 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2507z00zz__evobjectz00,
																									BgL_arg1789z00_4595);
																							}
																							BgL_list1787z00_4593 =
																								MAKE_YOUNG_PAIR(BgL_auxz00_4590,
																								BgL_arg1788z00_4594);
																						}
																						BgL_tauxz00_4591 =
																							BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																							(BgL_list1787z00_4593);
																					}
																				}
																				{	/* Eval/evobject.scm 573 */

																					{	/* Eval/evobject.scm 576 */
																						obj_t BgL_arg1764z00_4596;

																						{	/* Eval/evobject.scm 576 */
																							obj_t BgL_arg1765z00_4597;

																							{	/* Eval/evobject.scm 576 */
																								obj_t BgL_arg1766z00_4598;
																								obj_t BgL_arg1767z00_4599;

																								{	/* Eval/evobject.scm 576 */
																									obj_t BgL_arg1768z00_4600;

																									{	/* Eval/evobject.scm 576 */
																										obj_t BgL_arg1769z00_4601;

																										BgL_arg1769z00_4601 =
																											MAKE_YOUNG_PAIR
																											(BgL_instancez00_4589,
																											BNIL);
																										BgL_arg1768z00_4600 =
																											MAKE_YOUNG_PAIR
																											(BgL_tauxz00_4591,
																											BgL_arg1769z00_4601);
																									}
																									BgL_arg1766z00_4598 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1768z00_4600, BNIL);
																								}
																								{	/* Eval/evobject.scm 578 */
																									obj_t BgL_arg1770z00_4602;

																									{	/* Eval/evobject.scm 578 */
																										obj_t BgL_arg1771z00_4603;
																										obj_t BgL_arg1772z00_4604;
																										obj_t BgL_arg1773z00_4605;

																										if (NULLP
																											(BgL_nfieldsz00_4588))
																											{	/* Eval/evobject.scm 578 */
																												BgL_arg1771z00_4603 =
																													BNIL;
																											}
																										else
																											{	/* Eval/evobject.scm 578 */
																												obj_t
																													BgL_head1126z00_4606;
																												{	/* Eval/evobject.scm 578 */
																													obj_t
																														BgL_arg1782z00_4607;
																													{	/* Eval/evobject.scm 578 */
																														obj_t
																															BgL_pairz00_4608;
																														BgL_pairz00_4608 =
																															CAR(((obj_t)
																																BgL_nfieldsz00_4588));
																														BgL_arg1782z00_4607
																															=
																															CAR
																															(BgL_pairz00_4608);
																													}
																													BgL_head1126z00_4606 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1782z00_4607,
																														BNIL);
																												}
																												{	/* Eval/evobject.scm 578 */
																													obj_t
																														BgL_g1129z00_4609;
																													BgL_g1129z00_4609 =
																														CDR(((obj_t)
																															BgL_nfieldsz00_4588));
																													{
																														obj_t
																															BgL_l1124z00_4611;
																														obj_t
																															BgL_tail1127z00_4612;
																														BgL_l1124z00_4611 =
																															BgL_g1129z00_4609;
																														BgL_tail1127z00_4612
																															=
																															BgL_head1126z00_4606;
																													BgL_zc3z04anonymousza31775ze3z87_4610:
																														if (NULLP
																															(BgL_l1124z00_4611))
																															{	/* Eval/evobject.scm 578 */
																																BgL_arg1771z00_4603
																																	=
																																	BgL_head1126z00_4606;
																															}
																														else
																															{	/* Eval/evobject.scm 578 */
																																obj_t
																																	BgL_newtail1128z00_4613;
																																{	/* Eval/evobject.scm 578 */
																																	obj_t
																																		BgL_arg1779z00_4614;
																																	{	/* Eval/evobject.scm 578 */
																																		obj_t
																																			BgL_pairz00_4615;
																																		BgL_pairz00_4615
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_l1124z00_4611));
																																		BgL_arg1779z00_4614
																																			=
																																			CAR
																																			(BgL_pairz00_4615);
																																	}
																																	BgL_newtail1128z00_4613
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1779z00_4614,
																																		BNIL);
																																}
																																SET_CDR
																																	(BgL_tail1127z00_4612,
																																	BgL_newtail1128z00_4613);
																																{	/* Eval/evobject.scm 578 */
																																	obj_t
																																		BgL_arg1777z00_4616;
																																	BgL_arg1777z00_4616
																																		=
																																		CDR(((obj_t)
																																			BgL_l1124z00_4611));
																																	{
																																		obj_t
																																			BgL_tail1127z00_5805;
																																		obj_t
																																			BgL_l1124z00_5804;
																																		BgL_l1124z00_5804
																																			=
																																			BgL_arg1777z00_4616;
																																		BgL_tail1127z00_5805
																																			=
																																			BgL_newtail1128z00_4613;
																																		BgL_tail1127z00_4612
																																			=
																																			BgL_tail1127z00_5805;
																																		BgL_l1124z00_4611
																																			=
																																			BgL_l1124z00_5804;
																																		goto
																																			BgL_zc3z04anonymousza31775ze3z87_4610;
																																	}
																																}
																															}
																													}
																												}
																											}
																										BgL_arg1772z00_4604 =
																											BGl_expandzd2prognzd2zz__prognz00
																											(BgL_bodyz00_4585);
																										BgL_arg1773z00_4605 =
																											BGl_evalzd2beginzd2expanderz00zz__expander_definez00
																											(BGl_withzd2accesszd2expanderz00zz__evobjectz00
																											(BgL_ez00_4408,
																												BgL_auxz00_4590,
																												BgL_nfieldsz00_4588,
																												BgL_xz00_4407));
																										BgL_arg1770z00_4602 =
																											BGl_z52withzd2lexicalz80zz__expandz00
																											(BgL_arg1771z00_4603,
																											BgL_arg1772z00_4604,
																											BgL_arg1773z00_4605,
																											BgL_auxz00_4590);
																									}
																									BgL_arg1767z00_4599 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1770z00_4602, BNIL);
																								}
																								BgL_arg1765z00_4597 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1766z00_4598,
																									BgL_arg1767z00_4599);
																							}
																							BgL_arg1764z00_4596 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2492z00zz__evobjectz00,
																								BgL_arg1765z00_4597);
																						}
																						return
																							BGl_localiza7eza7zz__evobjectz00
																							(BgL_xz00_4407,
																							BgL_arg1764z00_4596);
																					}
																				}
																			}
																		}
																	}
																else
																	{	/* Eval/evobject.scm 570 */
																		if (PAIRP(BgL_sz00_4587))
																			{	/* Eval/evobject.scm 586 */
																				bool_t BgL_test2733z00_5816;

																				{	/* Eval/evobject.scm 586 */
																					obj_t BgL_tmpz00_5817;

																					BgL_tmpz00_5817 = CAR(BgL_sz00_4587);
																					BgL_test2733z00_5816 =
																						SYMBOLP(BgL_tmpz00_5817);
																				}
																				if (BgL_test2733z00_5816)
																					{	/* Eval/evobject.scm 587 */
																						obj_t BgL_arg1793z00_4617;
																						obj_t BgL_arg1794z00_4618;

																						BgL_arg1793z00_4617 =
																							CDR(BgL_sz00_4587);
																						{	/* Eval/evobject.scm 587 */
																							obj_t BgL_arg1795z00_4619;

																							{	/* Eval/evobject.scm 587 */
																								obj_t BgL_arg1796z00_4620;
																								obj_t BgL_arg1797z00_4621;

																								BgL_arg1796z00_4620 =
																									CAR(BgL_sz00_4587);
																								BgL_arg1797z00_4621 =
																									CAR(BgL_sz00_4587);
																								{	/* Eval/evobject.scm 587 */
																									obj_t BgL_list1798z00_4622;

																									{	/* Eval/evobject.scm 587 */
																										obj_t BgL_arg1799z00_4623;

																										BgL_arg1799z00_4623 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1797z00_4621,
																											BNIL);
																										BgL_list1798z00_4622 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1796z00_4620,
																											BgL_arg1799z00_4623);
																									}
																									BgL_arg1795z00_4619 =
																										BgL_list1798z00_4622;
																								}
																							}
																							BgL_arg1794z00_4618 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1795z00_4619,
																								BgL_nfieldsz00_4588);
																						}
																						{
																							obj_t BgL_nfieldsz00_5827;
																							obj_t BgL_sz00_5826;

																							BgL_sz00_5826 =
																								BgL_arg1793z00_4617;
																							BgL_nfieldsz00_5827 =
																								BgL_arg1794z00_4618;
																							BgL_nfieldsz00_4588 =
																								BgL_nfieldsz00_5827;
																							BgL_sz00_4587 = BgL_sz00_5826;
																							goto BgL_loopz00_4586;
																						}
																					}
																				else
																					{	/* Eval/evobject.scm 588 */
																						bool_t BgL_test2734z00_5828;

																						{	/* Eval/evobject.scm 588 */
																							bool_t BgL_test2735z00_5829;

																							{	/* Eval/evobject.scm 588 */
																								obj_t BgL_tmpz00_5830;

																								BgL_tmpz00_5830 =
																									CAR(BgL_sz00_4587);
																								BgL_test2735z00_5829 =
																									PAIRP(BgL_tmpz00_5830);
																							}
																							if (BgL_test2735z00_5829)
																								{	/* Eval/evobject.scm 589 */
																									bool_t BgL_test2736z00_5833;

																									{	/* Eval/evobject.scm 589 */
																										obj_t BgL_tmpz00_5834;

																										BgL_tmpz00_5834 =
																											CAR(CAR(BgL_sz00_4587));
																										BgL_test2736z00_5833 =
																											SYMBOLP(BgL_tmpz00_5834);
																									}
																									if (BgL_test2736z00_5833)
																										{	/* Eval/evobject.scm 590 */
																											bool_t
																												BgL_test2737z00_5838;
																											{	/* Eval/evobject.scm 590 */
																												obj_t BgL_tmpz00_5839;

																												BgL_tmpz00_5839 =
																													CDR(CAR
																													(BgL_sz00_4587));
																												BgL_test2737z00_5838 =
																													PAIRP
																													(BgL_tmpz00_5839);
																											}
																											if (BgL_test2737z00_5838)
																												{	/* Eval/evobject.scm 591 */
																													bool_t
																														BgL_test2738z00_5843;
																													{	/* Eval/evobject.scm 591 */
																														obj_t
																															BgL_tmpz00_5844;
																														{	/* Eval/evobject.scm 591 */
																															obj_t
																																BgL_pairz00_4624;
																															BgL_pairz00_4624 =
																																CAR
																																(BgL_sz00_4587);
																															BgL_tmpz00_5844 =
																																CAR(CDR
																																(BgL_pairz00_4624));
																														}
																														BgL_test2738z00_5843
																															=
																															SYMBOLP
																															(BgL_tmpz00_5844);
																													}
																													if (BgL_test2738z00_5843)
																														{	/* Eval/evobject.scm 592 */
																															obj_t
																																BgL_tmpz00_5849;
																															{	/* Eval/evobject.scm 592 */
																																obj_t
																																	BgL_pairz00_4625;
																																BgL_pairz00_4625
																																	=
																																	CAR
																																	(BgL_sz00_4587);
																																BgL_tmpz00_5849
																																	=
																																	CDR(CDR
																																	(BgL_pairz00_4625));
																															}
																															BgL_test2734z00_5828
																																=
																																NULLP
																																(BgL_tmpz00_5849);
																														}
																													else
																														{	/* Eval/evobject.scm 591 */
																															BgL_test2734z00_5828
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Eval/evobject.scm 590 */
																													BgL_test2734z00_5828 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Eval/evobject.scm 589 */
																											BgL_test2734z00_5828 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Eval/evobject.scm 588 */
																									BgL_test2734z00_5828 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test2734z00_5828)
																							{	/* Eval/evobject.scm 593 */
																								obj_t BgL_arg1822z00_4626;
																								obj_t BgL_arg1823z00_4627;

																								BgL_arg1822z00_4626 =
																									CDR(BgL_sz00_4587);
																								BgL_arg1823z00_4627 =
																									MAKE_YOUNG_PAIR(CAR
																									(BgL_sz00_4587),
																									BgL_nfieldsz00_4588);
																								{
																									obj_t BgL_nfieldsz00_5858;
																									obj_t BgL_sz00_5857;

																									BgL_sz00_5857 =
																										BgL_arg1822z00_4626;
																									BgL_nfieldsz00_5858 =
																										BgL_arg1823z00_4627;
																									BgL_nfieldsz00_4588 =
																										BgL_nfieldsz00_5858;
																									BgL_sz00_4587 = BgL_sz00_5857;
																									goto BgL_loopz00_4586;
																								}
																							}
																						else
																							{	/* Eval/evobject.scm 595 */
																								obj_t BgL_arg1827z00_4628;

																								BgL_arg1827z00_4628 =
																									CAR(BgL_sz00_4587);
																								{	/* Eval/evobject.scm 72 */
																									obj_t BgL_locz00_4629;

																									if (EPAIRP(BgL_xz00_4407))
																										{	/* Eval/evobject.scm 72 */
																											BgL_locz00_4629 =
																												CER(
																												((obj_t)
																													BgL_xz00_4407));
																										}
																									else
																										{	/* Eval/evobject.scm 72 */
																											BgL_locz00_4629 = BFALSE;
																										}
																									{	/* Eval/evobject.scm 73 */
																										bool_t BgL_test2740z00_5864;

																										if (PAIRP(BgL_locz00_4629))
																											{	/* Eval/evobject.scm 73 */
																												bool_t
																													BgL_test2742z00_5867;
																												{	/* Eval/evobject.scm 73 */
																													obj_t BgL_tmpz00_5868;

																													BgL_tmpz00_5868 =
																														CDR
																														(BgL_locz00_4629);
																													BgL_test2742z00_5867 =
																														PAIRP
																														(BgL_tmpz00_5868);
																												}
																												if (BgL_test2742z00_5867)
																													{	/* Eval/evobject.scm 73 */
																														obj_t
																															BgL_tmpz00_5871;
																														BgL_tmpz00_5871 =
																															CDR(CDR
																															(BgL_locz00_4629));
																														BgL_test2740z00_5864
																															=
																															PAIRP
																															(BgL_tmpz00_5871);
																													}
																												else
																													{	/* Eval/evobject.scm 73 */
																														BgL_test2740z00_5864
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Eval/evobject.scm 73 */
																												BgL_test2740z00_5864 =
																													((bool_t) 0);
																											}
																										if (BgL_test2740z00_5864)
																											{	/* Eval/evobject.scm 73 */
																												return
																													BGl_errorzf2locationzf2zz__errorz00
																													(BgL_arg1827z00_4628,
																													BGl_string2498z00zz__evobjectz00,
																													BgL_xz00_4407,
																													CAR(CDR
																														(BgL_locz00_4629)),
																													CAR(CDR(CDR
																															(BgL_locz00_4629))));
																											}
																										else
																											{	/* Eval/evobject.scm 73 */
																												return
																													BGl_errorz00zz__errorz00
																													(BgL_arg1827z00_4628,
																													BGl_string2498z00zz__evobjectz00,
																													BgL_xz00_4407);
																											}
																									}
																								}
																							}
																					}
																			}
																		else
																			{	/* Eval/evobject.scm 72 */
																				obj_t BgL_locz00_4630;

																				if (EPAIRP(BgL_xz00_4407))
																					{	/* Eval/evobject.scm 72 */
																						BgL_locz00_4630 =
																							CER(((obj_t) BgL_xz00_4407));
																					}
																				else
																					{	/* Eval/evobject.scm 72 */
																						BgL_locz00_4630 = BFALSE;
																					}
																				{	/* Eval/evobject.scm 73 */
																					bool_t BgL_test2744z00_5886;

																					if (PAIRP(BgL_locz00_4630))
																						{	/* Eval/evobject.scm 73 */
																							bool_t BgL_test2746z00_5889;

																							{	/* Eval/evobject.scm 73 */
																								obj_t BgL_tmpz00_5890;

																								BgL_tmpz00_5890 =
																									CDR(BgL_locz00_4630);
																								BgL_test2746z00_5889 =
																									PAIRP(BgL_tmpz00_5890);
																							}
																							if (BgL_test2746z00_5889)
																								{	/* Eval/evobject.scm 73 */
																									obj_t BgL_tmpz00_5893;

																									BgL_tmpz00_5893 =
																										CDR(CDR(BgL_locz00_4630));
																									BgL_test2744z00_5886 =
																										PAIRP(BgL_tmpz00_5893);
																								}
																							else
																								{	/* Eval/evobject.scm 73 */
																									BgL_test2744z00_5886 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Eval/evobject.scm 73 */
																							BgL_test2744z00_5886 =
																								((bool_t) 0);
																						}
																					if (BgL_test2744z00_5886)
																						{	/* Eval/evobject.scm 73 */
																							return
																								BGl_errorzf2locationzf2zz__errorz00
																								(BgL_sz00_4587,
																								BGl_string2509z00zz__evobjectz00,
																								BgL_xz00_4407,
																								CAR(CDR(BgL_locz00_4630)),
																								CAR(CDR(CDR(BgL_locz00_4630))));
																						}
																					else
																						{	/* Eval/evobject.scm 73 */
																							return
																								BGl_errorz00zz__errorz00
																								(BgL_sz00_4587,
																								BGl_string2509z00zz__evobjectz00,
																								BgL_xz00_4407);
																						}
																				}
																			}
																	}
															}
														}
													else
														{	/* Eval/evobject.scm 564 */
															return
																BGl_expandzd2errorzd2zz__evobjectz00
																(BGl_string2510z00zz__evobjectz00,
																BGl_string2511z00zz__evobjectz00,
																BgL_xz00_4407);
														}
												}
											else
												{	/* Eval/evobject.scm 564 */
													return
														BGl_expandzd2errorzd2zz__evobjectz00
														(BGl_string2510z00zz__evobjectz00,
														BGl_string2511z00zz__evobjectz00, BgL_xz00_4407);
												}
										}
									else
										{	/* Eval/evobject.scm 564 */
											return
												BGl_expandzd2errorzd2zz__evobjectz00
												(BGl_string2510z00zz__evobjectz00,
												BGl_string2511z00zz__evobjectz00, BgL_xz00_4407);
										}
								}
							else
								{	/* Eval/evobject.scm 564 */
									return
										BGl_expandzd2errorzd2zz__evobjectz00
										(BGl_string2510z00zz__evobjectz00,
										BGl_string2511z00zz__evobjectz00, BgL_xz00_4407);
								}
						}
					else
						{	/* Eval/evobject.scm 564 */
							return
								BGl_expandzd2errorzd2zz__evobjectz00
								(BGl_string2510z00zz__evobjectz00,
								BGl_string2511z00zz__evobjectz00, BgL_xz00_4407);
						}
				}
			}
		}

	}



/* with-access-expander */
	obj_t BGl_withzd2accesszd2expanderz00zz__evobjectz00(obj_t BgL_oldez00_104,
		obj_t BgL_iz00_105, obj_t BgL_fieldsz00_106, obj_t BgL_formz00_107)
	{
		{	/* Eval/evobject.scm 602 */
			{	/* Eval/evobject.scm 606 */
				obj_t BgL_idsz00_1983;

				if (NULLP(BgL_fieldsz00_106))
					{	/* Eval/evobject.scm 606 */
						BgL_idsz00_1983 = BNIL;
					}
				else
					{	/* Eval/evobject.scm 606 */
						obj_t BgL_head1132z00_2051;

						{	/* Eval/evobject.scm 606 */
							obj_t BgL_arg1891z00_2063;

							{	/* Eval/evobject.scm 606 */
								obj_t BgL_pairz00_3654;

								BgL_pairz00_3654 = CAR(((obj_t) BgL_fieldsz00_106));
								BgL_arg1891z00_2063 = CAR(BgL_pairz00_3654);
							}
							BgL_head1132z00_2051 = MAKE_YOUNG_PAIR(BgL_arg1891z00_2063, BNIL);
						}
						{	/* Eval/evobject.scm 606 */
							obj_t BgL_g1135z00_2052;

							BgL_g1135z00_2052 = CDR(((obj_t) BgL_fieldsz00_106));
							{
								obj_t BgL_l1130z00_3675;
								obj_t BgL_tail1133z00_3676;

								BgL_l1130z00_3675 = BgL_g1135z00_2052;
								BgL_tail1133z00_3676 = BgL_head1132z00_2051;
							BgL_zc3z04anonymousza31886ze3z87_3674:
								if (NULLP(BgL_l1130z00_3675))
									{	/* Eval/evobject.scm 606 */
										BgL_idsz00_1983 = BgL_head1132z00_2051;
									}
								else
									{	/* Eval/evobject.scm 606 */
										obj_t BgL_newtail1134z00_3683;

										{	/* Eval/evobject.scm 606 */
											obj_t BgL_arg1889z00_3684;

											{	/* Eval/evobject.scm 606 */
												obj_t BgL_pairz00_3688;

												BgL_pairz00_3688 = CAR(((obj_t) BgL_l1130z00_3675));
												BgL_arg1889z00_3684 = CAR(BgL_pairz00_3688);
											}
											BgL_newtail1134z00_3683 =
												MAKE_YOUNG_PAIR(BgL_arg1889z00_3684, BNIL);
										}
										SET_CDR(BgL_tail1133z00_3676, BgL_newtail1134z00_3683);
										{	/* Eval/evobject.scm 606 */
											obj_t BgL_arg1888z00_3686;

											BgL_arg1888z00_3686 = CDR(((obj_t) BgL_l1130z00_3675));
											{
												obj_t BgL_tail1133z00_5927;
												obj_t BgL_l1130z00_5926;

												BgL_l1130z00_5926 = BgL_arg1888z00_3686;
												BgL_tail1133z00_5927 = BgL_newtail1134z00_3683;
												BgL_tail1133z00_3676 = BgL_tail1133z00_5927;
												BgL_l1130z00_3675 = BgL_l1130z00_5926;
												goto BgL_zc3z04anonymousza31886ze3z87_3674;
											}
										}
									}
							}
						}
					}
				{	/* Eval/evobject.scm 607 */
					obj_t BgL_zc3z04anonymousza31839ze3z87_4409;

					BgL_zc3z04anonymousza31839ze3z87_4409 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31839ze3ze5zz__evobjectz00, (int) (2L),
						(int) (4L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31839ze3z87_4409, (int) (0L),
						BgL_oldez00_104);
					PROCEDURE_SET(BgL_zc3z04anonymousza31839ze3z87_4409, (int) (1L),
						BgL_iz00_105);
					PROCEDURE_SET(BgL_zc3z04anonymousza31839ze3z87_4409, (int) (2L),
						BgL_fieldsz00_106);
					PROCEDURE_SET(BgL_zc3z04anonymousza31839ze3z87_4409, (int) (3L),
						BgL_idsz00_1983);
					return BgL_zc3z04anonymousza31839ze3z87_4409;
				}
			}
		}

	}



/* &<@anonymous:1839> */
	obj_t BGl_z62zc3z04anonymousza31839ze3ze5zz__evobjectz00(obj_t
		BgL_envz00_4410, obj_t BgL_xz00_4415, obj_t BgL_ez00_4416)
	{
		{	/* Eval/evobject.scm 607 */
			{	/* Eval/evobject.scm 607 */
				obj_t BgL_oldez00_4411;
				obj_t BgL_iz00_4412;
				obj_t BgL_fieldsz00_4413;
				obj_t BgL_idsz00_4414;

				BgL_oldez00_4411 = PROCEDURE_REF(BgL_envz00_4410, (int) (0L));
				BgL_iz00_4412 = ((obj_t) PROCEDURE_REF(BgL_envz00_4410, (int) (1L)));
				BgL_fieldsz00_4413 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4410, (int) (2L)));
				BgL_idsz00_4414 = ((obj_t) PROCEDURE_REF(BgL_envz00_4410, (int) (3L)));
				{
					obj_t BgL_varz00_4658;
					obj_t BgL_varz00_4639;
					obj_t BgL_valz00_4640;

					if (SYMBOLP(BgL_xz00_4415))
						{	/* Eval/evobject.scm 607 */
							BgL_varz00_4658 = BgL_xz00_4415;
							{	/* Eval/evobject.scm 610 */
								bool_t BgL_test2750z00_5952;

								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_varz00_4658, BgL_idsz00_4414)))
									{	/* Eval/evobject.scm 611 */
										obj_t BgL_cellz00_4659;

										BgL_cellz00_4659 =
											BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_varz00_4658,
											BGl_z52lexicalzd2stackz80zz__expandz00());
										if (PAIRP(BgL_cellz00_4659))
											{	/* Eval/evobject.scm 612 */
												BgL_test2750z00_5952 =
													(CDR(BgL_cellz00_4659) == BgL_iz00_4412);
											}
										else
											{	/* Eval/evobject.scm 612 */
												BgL_test2750z00_5952 = ((bool_t) 0);
											}
									}
								else
									{	/* Eval/evobject.scm 610 */
										BgL_test2750z00_5952 = ((bool_t) 0);
									}
								if (BgL_test2750z00_5952)
									{	/* Eval/evobject.scm 613 */
										obj_t BgL_arg1856z00_4660;

										{	/* Eval/evobject.scm 613 */
											obj_t BgL_arg1857z00_4661;
											obj_t BgL_arg1858z00_4662;

											BgL_arg1857z00_4661 =
												BGL_PROCEDURE_CALL2(BgL_oldez00_4411, BgL_iz00_4412,
												BgL_oldez00_4411);
											{	/* Eval/evobject.scm 613 */
												obj_t BgL_arg1859z00_4663;

												{	/* Eval/evobject.scm 604 */
													obj_t BgL_pairz00_4664;

													BgL_pairz00_4664 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_varz00_4658, BgL_fieldsz00_4413);
													BgL_arg1859z00_4663 = CAR(CDR(BgL_pairz00_4664));
												}
												BgL_arg1858z00_4662 =
													MAKE_YOUNG_PAIR(BgL_arg1859z00_4663, BNIL);
											}
											BgL_arg1856z00_4660 =
												MAKE_YOUNG_PAIR(BgL_arg1857z00_4661,
												BgL_arg1858z00_4662);
										}
										return
											MAKE_YOUNG_PAIR(BGl_symbol2512z00zz__evobjectz00,
											BgL_arg1856z00_4660);
									}
								else
									{	/* Eval/evobject.scm 610 */
										return
											BGL_PROCEDURE_CALL2(BgL_oldez00_4411, BgL_varz00_4658,
											BgL_oldez00_4411);
									}
							}
						}
					else
						{	/* Eval/evobject.scm 607 */
							if (PAIRP(BgL_xz00_4415))
								{	/* Eval/evobject.scm 607 */
									obj_t BgL_cdrzd2194zd2_4665;

									BgL_cdrzd2194zd2_4665 = CDR(((obj_t) BgL_xz00_4415));
									if (
										(CAR(
												((obj_t) BgL_xz00_4415)) ==
											BGl_symbol2514z00zz__evobjectz00))
										{	/* Eval/evobject.scm 607 */
											if (PAIRP(BgL_cdrzd2194zd2_4665))
												{	/* Eval/evobject.scm 607 */
													obj_t BgL_carzd2197zd2_4666;
													obj_t BgL_cdrzd2198zd2_4667;

													BgL_carzd2197zd2_4666 = CAR(BgL_cdrzd2194zd2_4665);
													BgL_cdrzd2198zd2_4667 = CDR(BgL_cdrzd2194zd2_4665);
													if (SYMBOLP(BgL_carzd2197zd2_4666))
														{	/* Eval/evobject.scm 607 */
															if (PAIRP(BgL_cdrzd2198zd2_4667))
																{	/* Eval/evobject.scm 607 */
																	if (NULLP(CDR(BgL_cdrzd2198zd2_4667)))
																		{	/* Eval/evobject.scm 607 */
																			BgL_varz00_4639 = BgL_carzd2197zd2_4666;
																			BgL_valz00_4640 =
																				CAR(BgL_cdrzd2198zd2_4667);
																			{	/* Eval/evobject.scm 616 */
																				obj_t BgL_valz00_4641;

																				BgL_valz00_4641 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_4416,
																					BgL_valz00_4640, BgL_ez00_4416);
																				{	/* Eval/evobject.scm 617 */
																					bool_t BgL_test2759z00_6002;

																					if (CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																							(BgL_varz00_4639,
																								BgL_idsz00_4414)))
																						{	/* Eval/evobject.scm 618 */
																							obj_t BgL_cellz00_4642;

																							BgL_cellz00_4642 =
																								BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_varz00_4639,
																								BGl_z52lexicalzd2stackz80zz__expandz00
																								());
																							if (PAIRP(BgL_cellz00_4642))
																								{	/* Eval/evobject.scm 619 */
																									BgL_test2759z00_6002 =
																										(CDR(BgL_cellz00_4642) ==
																										BgL_iz00_4412);
																								}
																							else
																								{	/* Eval/evobject.scm 619 */
																									BgL_test2759z00_6002 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Eval/evobject.scm 617 */
																							BgL_test2759z00_6002 =
																								((bool_t) 0);
																						}
																					if (BgL_test2759z00_6002)
																						{	/* Eval/evobject.scm 620 */
																							obj_t BgL_arg1868z00_4643;

																							{	/* Eval/evobject.scm 620 */
																								obj_t BgL_arg1869z00_4644;
																								obj_t BgL_arg1870z00_4645;

																								{	/* Eval/evobject.scm 620 */
																									obj_t BgL_arg1872z00_4646;

																									{	/* Eval/evobject.scm 620 */
																										obj_t BgL_arg1873z00_4647;
																										obj_t BgL_arg1874z00_4648;

																										BgL_arg1873z00_4647 =
																											BGL_PROCEDURE_CALL2
																											(BgL_oldez00_4411,
																											BgL_iz00_4412,
																											BgL_oldez00_4411);
																										{	/* Eval/evobject.scm 620 */
																											obj_t BgL_arg1875z00_4649;

																											{	/* Eval/evobject.scm 604 */
																												obj_t BgL_pairz00_4650;

																												BgL_pairz00_4650 =
																													BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																													(BgL_varz00_4639,
																													BgL_fieldsz00_4413);
																												BgL_arg1875z00_4649 =
																													CAR(CDR
																													(BgL_pairz00_4650));
																											}
																											BgL_arg1874z00_4648 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1875z00_4649,
																												BNIL);
																										}
																										BgL_arg1872z00_4646 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1873z00_4647,
																											BgL_arg1874z00_4648);
																									}
																									BgL_arg1869z00_4644 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2512z00zz__evobjectz00,
																										BgL_arg1872z00_4646);
																								}
																								{	/* Eval/evobject.scm 620 */
																									obj_t BgL_arg1876z00_4651;

																									BgL_arg1876z00_4651 =
																										BGL_PROCEDURE_CALL2
																										(BgL_oldez00_4411,
																										BgL_valz00_4641,
																										BgL_oldez00_4411);
																									BgL_arg1870z00_4645 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1876z00_4651, BNIL);
																								}
																								BgL_arg1868z00_4643 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1869z00_4644,
																									BgL_arg1870z00_4645);
																							}
																							return
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2514z00zz__evobjectz00,
																								BgL_arg1868z00_4643);
																						}
																					else
																						{	/* Eval/evobject.scm 621 */
																							obj_t BgL_arg1877z00_4652;

																							{	/* Eval/evobject.scm 621 */
																								obj_t BgL_arg1878z00_4653;

																								{	/* Eval/evobject.scm 621 */
																									obj_t BgL_arg1879z00_4654;

																									{	/* Eval/evobject.scm 621 */
																										obj_t BgL_arg1880z00_4655;
																										obj_t BgL_arg1882z00_4656;

																										{	/* Eval/evobject.scm 621 */
																											obj_t BgL_pairz00_4657;

																											BgL_pairz00_4657 =
																												CDR(
																												((obj_t)
																													BgL_xz00_4415));
																											BgL_arg1880z00_4655 =
																												CAR(BgL_pairz00_4657);
																										}
																										BgL_arg1882z00_4656 =
																											MAKE_YOUNG_PAIR
																											(BgL_valz00_4641, BNIL);
																										BgL_arg1879z00_4654 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1880z00_4655,
																											BgL_arg1882z00_4656);
																									}
																									BgL_arg1878z00_4653 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2514z00zz__evobjectz00,
																										BgL_arg1879z00_4654);
																								}
																								BgL_arg1877z00_4652 =
																									BGL_PROCEDURE_CALL2
																									(BgL_oldez00_4411,
																									BgL_arg1878z00_4653,
																									BgL_oldez00_4411);
																							}
																							return
																								BGl_localiza7eza7zz__evobjectz00
																								(BgL_xz00_4415,
																								BgL_arg1877z00_4652);
																						}
																				}
																			}
																		}
																	else
																		{	/* Eval/evobject.scm 607 */
																			return
																				BGL_PROCEDURE_CALL2(BgL_oldez00_4411,
																				BgL_xz00_4415, BgL_ez00_4416);
																		}
																}
															else
																{	/* Eval/evobject.scm 607 */
																	return
																		BGL_PROCEDURE_CALL2(BgL_oldez00_4411,
																		BgL_xz00_4415, BgL_ez00_4416);
																}
														}
													else
														{	/* Eval/evobject.scm 607 */
															return
																BGL_PROCEDURE_CALL2(BgL_oldez00_4411,
																BgL_xz00_4415, BgL_ez00_4416);
														}
												}
											else
												{	/* Eval/evobject.scm 607 */
													return
														BGL_PROCEDURE_CALL2(BgL_oldez00_4411, BgL_xz00_4415,
														BgL_ez00_4416);
												}
										}
									else
										{	/* Eval/evobject.scm 607 */
											return
												BGL_PROCEDURE_CALL2(BgL_oldez00_4411, BgL_xz00_4415,
												BgL_ez00_4416);
										}
								}
							else
								{	/* Eval/evobject.scm 607 */
									return
										BGL_PROCEDURE_CALL2(BgL_oldez00_4411, BgL_xz00_4415,
										BgL_ez00_4416);
								}
						}
				}
			}
		}

	}



/* eval-parse-class-slot */
	obj_t BGl_evalzd2parsezd2classzd2slotzd2zz__evobjectz00(obj_t BgL_locz00_108,
		obj_t BgL_fz00_109)
	{
		{	/* Eval/evobject.scm 628 */
			if (SYMBOLP(BgL_fz00_109))
				{	/* Eval/evobject.scm 631 */
					obj_t BgL_idz00_2070;

					BgL_idz00_2070 =
						BGl_decomposezd2identzd2zz__evobjectz00(BgL_fz00_109);
					{	/* Eval/evobject.scm 632 */
						obj_t BgL_typez00_2071;

						{	/* Eval/evobject.scm 633 */
							obj_t BgL_tmpz00_3713;

							{	/* Eval/evobject.scm 633 */
								int BgL_tmpz00_6077;

								BgL_tmpz00_6077 = (int) (1L);
								BgL_tmpz00_3713 = BGL_MVALUES_VAL(BgL_tmpz00_6077);
							}
							{	/* Eval/evobject.scm 633 */
								int BgL_tmpz00_6080;

								BgL_tmpz00_6080 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6080, BUNSPEC);
							}
							BgL_typez00_2071 = BgL_tmpz00_3713;
						}
						{	/* Eval/evobject.scm 633 */
							obj_t BgL_arg1896z00_2072;

							{	/* Eval/evobject.scm 633 */
								obj_t BgL_arg1898z00_2074;

								if (CBOOL(BgL_typez00_2071))
									{	/* Eval/evobject.scm 633 */
										obj_t BgL__ortest_1045z00_2075;

										BgL__ortest_1045z00_2075 =
											BGl_classzd2existszd2zz__objectz00(BgL_typez00_2071);
										if (CBOOL(BgL__ortest_1045z00_2075))
											{	/* Eval/evobject.scm 633 */
												BgL_arg1898z00_2074 = BgL__ortest_1045z00_2075;
											}
										else
											{	/* Eval/evobject.scm 633 */
												BgL_arg1898z00_2074 = BgL_typez00_2071;
											}
									}
								else
									{	/* Eval/evobject.scm 633 */
										BgL_arg1898z00_2074 = BGl_symbol2516z00zz__evobjectz00;
									}
								{	/* Eval/evobject.scm 80 */
									obj_t BgL_newz00_3714;

									BgL_newz00_3714 =
										create_struct(BGl_symbol2518z00zz__evobjectz00, (int) (8L));
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6090;

										BgL_tmpz00_6090 = (int) (7L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6090, BFALSE);
									}
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6093;

										BgL_tmpz00_6093 = (int) (6L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6093, BFALSE);
									}
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6096;

										BgL_tmpz00_6096 = (int) (5L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6096, BFALSE);
									}
									{	/* Eval/evobject.scm 80 */
										obj_t BgL_auxz00_6101;
										int BgL_tmpz00_6099;

										BgL_auxz00_6101 = BINT(0L);
										BgL_tmpz00_6099 = (int) (4L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6099,
											BgL_auxz00_6101);
									}
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6104;

										BgL_tmpz00_6104 = (int) (3L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6104, BFALSE);
									}
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6107;

										BgL_tmpz00_6107 = (int) (2L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6107, BFALSE);
									}
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6110;

										BgL_tmpz00_6110 = (int) (1L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6110,
											BgL_arg1898z00_2074);
									}
									{	/* Eval/evobject.scm 80 */
										int BgL_tmpz00_6113;

										BgL_tmpz00_6113 = (int) (0L);
										STRUCT_SET(BgL_newz00_3714, BgL_tmpz00_6113,
											BgL_idz00_2070);
									}
									BgL_arg1896z00_2072 = BgL_newz00_3714;
							}}
							{	/* Eval/evobject.scm 633 */
								obj_t BgL_list1897z00_2073;

								BgL_list1897z00_2073 =
									MAKE_YOUNG_PAIR(BgL_arg1896z00_2072, BNIL);
								return BgL_list1897z00_2073;
							}
						}
					}
				}
			else
				{	/* Eval/evobject.scm 635 */
					bool_t BgL_test2765z00_6117;

					if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_fz00_109))
						{	/* Eval/evobject.scm 635 */
							obj_t BgL_tmpz00_6120;

							BgL_tmpz00_6120 = CAR(((obj_t) BgL_fz00_109));
							BgL_test2765z00_6117 = SYMBOLP(BgL_tmpz00_6120);
						}
					else
						{	/* Eval/evobject.scm 635 */
							BgL_test2765z00_6117 = ((bool_t) 0);
						}
					if (BgL_test2765z00_6117)
						{	/* Eval/evobject.scm 639 */
							obj_t BgL_idz00_2079;
							obj_t BgL_attrsz00_2080;

							BgL_idz00_2079 = CAR(((obj_t) BgL_fz00_109));
							BgL_attrsz00_2080 = CDR(((obj_t) BgL_fz00_109));
							{	/* Eval/evobject.scm 641 */
								obj_t BgL_idz00_2081;

								BgL_idz00_2081 =
									BGl_decomposezd2identzd2zz__evobjectz00(BgL_idz00_2079);
								{	/* Eval/evobject.scm 642 */
									obj_t BgL_typez00_2082;

									{	/* Eval/evobject.scm 643 */
										obj_t BgL_tmpz00_3727;

										{	/* Eval/evobject.scm 643 */
											int BgL_tmpz00_6129;

											BgL_tmpz00_6129 = (int) (1L);
											BgL_tmpz00_3727 = BGL_MVALUES_VAL(BgL_tmpz00_6129);
										}
										{	/* Eval/evobject.scm 643 */
											int BgL_tmpz00_6132;

											BgL_tmpz00_6132 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_6132, BUNSPEC);
										}
										BgL_typez00_2082 = BgL_tmpz00_3727;
									}
									{	/* Eval/evobject.scm 643 */
										obj_t BgL_defz00_2083;
										obj_t BgL_getz00_2084;
										obj_t BgL_setz00_2085;
										obj_t BgL_infoz00_2086;
										bool_t BgL_ronlyz00_2087;

										BgL_defz00_2083 = BFALSE;
										BgL_getz00_2084 = BFALSE;
										BgL_setz00_2085 = BFALSE;
										BgL_infoz00_2086 = BFALSE;
										BgL_ronlyz00_2087 = ((bool_t) 0);
										{
											obj_t BgL_l1136z00_2089;

											BgL_l1136z00_2089 = BgL_attrsz00_2080;
										BgL_zc3z04anonymousza31903ze3z87_2090:
											if (PAIRP(BgL_l1136z00_2089))
												{	/* Eval/evobject.scm 648 */
													{	/* Eval/evobject.scm 650 */
														obj_t BgL_attrz00_2092;

														BgL_attrz00_2092 = CAR(BgL_l1136z00_2089);
														if (
															(BgL_attrz00_2092 ==
																BGl_symbol2520z00zz__evobjectz00))
															{	/* Eval/evobject.scm 650 */
																BgL_ronlyz00_2087 = ((bool_t) 1);
															}
														else
															{
																obj_t BgL_exprz00_2097;
																obj_t BgL_exprz00_2095;

																if (PAIRP(BgL_attrz00_2092))
																	{	/* Eval/evobject.scm 653 */
																		obj_t BgL_cdrzd2216zd2_2104;

																		BgL_cdrzd2216zd2_2104 =
																			CDR(((obj_t) BgL_attrz00_2092));
																		if (
																			(CAR(
																					((obj_t) BgL_attrz00_2092)) ==
																				BGl_symbol2524z00zz__evobjectz00))
																			{	/* Eval/evobject.scm 653 */
																				if (PAIRP(BgL_cdrzd2216zd2_2104))
																					{	/* Eval/evobject.scm 653 */
																						if (NULLP(CDR
																								(BgL_cdrzd2216zd2_2104)))
																							{	/* Eval/evobject.scm 653 */
																								obj_t BgL_arg1912z00_2110;

																								BgL_arg1912z00_2110 =
																									CAR(BgL_cdrzd2216zd2_2104);
																								BgL_infoz00_2086 =
																									BgL_arg1912z00_2110;
																							}
																						else
																							{	/* Eval/evobject.scm 653 */
																							BgL_tagzd2210zd2_2101:
																								{	/* Eval/evobject.scm 673 */
																									obj_t BgL_arg1955z00_2160;

																									{	/* Eval/evobject.scm 673 */
																										obj_t
																											BgL__ortest_1047z00_2161;
																										BgL__ortest_1047z00_2161 =
																											BGl_getzd2sourcezd2locationz00zz__readerz00
																											(BgL_fz00_109);
																										if (CBOOL
																											(BgL__ortest_1047z00_2161))
																											{	/* Eval/evobject.scm 673 */
																												BgL_arg1955z00_2160 =
																													BgL__ortest_1047z00_2161;
																											}
																										else
																											{	/* Eval/evobject.scm 673 */
																												BgL_arg1955z00_2160 =
																													BgL_locz00_108;
																											}
																									}
																									BGl_evcompilezd2errorzd2zz__evcompilez00
																										(BgL_arg1955z00_2160,
																										BGl_string2522z00zz__evobjectz00,
																										BGl_string2523z00zz__evobjectz00,
																										BgL_fz00_109);
																								}
																							}
																					}
																				else
																					{	/* Eval/evobject.scm 653 */
																						goto BgL_tagzd2210zd2_2101;
																					}
																			}
																		else
																			{	/* Eval/evobject.scm 653 */
																				if (
																					(CAR(
																							((obj_t) BgL_attrz00_2092)) ==
																						BGl_symbol2526z00zz__evobjectz00))
																					{	/* Eval/evobject.scm 653 */
																						if (PAIRP(BgL_cdrzd2216zd2_2104))
																							{	/* Eval/evobject.scm 653 */
																								if (NULLP(CDR
																										(BgL_cdrzd2216zd2_2104)))
																									{	/* Eval/evobject.scm 653 */
																										BgL_exprz00_2095 =
																											CAR
																											(BgL_cdrzd2216zd2_2104);
																										if (SYMBOLP
																											(BgL_exprz00_2095))
																											{	/* Eval/evobject.scm 658 */
																												obj_t BgL_oz00_2141;

																												{	/* Eval/evobject.scm 658 */

																													{	/* Eval/evobject.scm 658 */

																														BgL_oz00_2141 =
																															BGl_gensymz00zz__r4_symbols_6_4z00
																															(BFALSE);
																													}
																												}
																												{	/* Eval/evobject.scm 659 */
																													obj_t
																														BgL_arg1942z00_2142;
																													{	/* Eval/evobject.scm 659 */
																														obj_t
																															BgL_arg1943z00_2143;
																														obj_t
																															BgL_arg1944z00_2144;
																														BgL_arg1943z00_2143
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_oz00_2141,
																															BNIL);
																														{	/* Eval/evobject.scm 660 */
																															obj_t
																																BgL_arg1945z00_2145;
																															{	/* Eval/evobject.scm 660 */
																																obj_t
																																	BgL_arg1946z00_2146;
																																BgL_arg1946z00_2146
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_oz00_2141,
																																	BNIL);
																																BgL_arg1945z00_2145
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_exprz00_2095,
																																	BgL_arg1946z00_2146);
																															}
																															BgL_arg1944z00_2144
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1945z00_2145,
																																BNIL);
																														}
																														BgL_arg1942z00_2142
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1943z00_2143,
																															BgL_arg1944z00_2144);
																													}
																													BgL_getz00_2084 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2453z00zz__evobjectz00,
																														BgL_arg1942z00_2142);
																												}
																											}
																										else
																											{	/* Eval/evobject.scm 657 */
																												BgL_getz00_2084 =
																													BgL_exprz00_2095;
																											}
																									}
																								else
																									{	/* Eval/evobject.scm 653 */
																										goto BgL_tagzd2210zd2_2101;
																									}
																							}
																						else
																							{	/* Eval/evobject.scm 653 */
																								goto BgL_tagzd2210zd2_2101;
																							}
																					}
																				else
																					{	/* Eval/evobject.scm 653 */
																						obj_t BgL_cdrzd2252zd2_2120;

																						BgL_cdrzd2252zd2_2120 =
																							CDR(((obj_t) BgL_attrz00_2092));
																						if (
																							(CAR(
																									((obj_t) BgL_attrz00_2092)) ==
																								BGl_symbol2528z00zz__evobjectz00))
																							{	/* Eval/evobject.scm 653 */
																								if (PAIRP
																									(BgL_cdrzd2252zd2_2120))
																									{	/* Eval/evobject.scm 653 */
																										if (NULLP(CDR
																												(BgL_cdrzd2252zd2_2120)))
																											{	/* Eval/evobject.scm 653 */
																												BgL_exprz00_2097 =
																													CAR
																													(BgL_cdrzd2252zd2_2120);
																												if (SYMBOLP
																													(BgL_exprz00_2097))
																													{	/* Eval/evobject.scm 664 */
																														obj_t BgL_oz00_2149;
																														obj_t BgL_vz00_2150;

																														{	/* Eval/evobject.scm 664 */

																															{	/* Eval/evobject.scm 664 */

																																BgL_oz00_2149 =
																																	BGl_gensymz00zz__r4_symbols_6_4z00
																																	(BFALSE);
																															}
																														}
																														{	/* Eval/evobject.scm 665 */

																															{	/* Eval/evobject.scm 665 */

																																BgL_vz00_2150 =
																																	BGl_gensymz00zz__r4_symbols_6_4z00
																																	(BFALSE);
																															}
																														}
																														{	/* Eval/evobject.scm 666 */
																															obj_t
																																BgL_arg1948z00_2151;
																															{	/* Eval/evobject.scm 666 */
																																obj_t
																																	BgL_arg1949z00_2152;
																																obj_t
																																	BgL_arg1950z00_2153;
																																{	/* Eval/evobject.scm 666 */
																																	obj_t
																																		BgL_arg1951z00_2154;
																																	BgL_arg1951z00_2154
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_vz00_2150,
																																		BNIL);
																																	BgL_arg1949z00_2152
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_oz00_2149,
																																		BgL_arg1951z00_2154);
																																}
																																{	/* Eval/evobject.scm 667 */
																																	obj_t
																																		BgL_arg1952z00_2155;
																																	{	/* Eval/evobject.scm 667 */
																																		obj_t
																																			BgL_arg1953z00_2156;
																																		{	/* Eval/evobject.scm 667 */
																																			obj_t
																																				BgL_arg1954z00_2157;
																																			BgL_arg1954z00_2157
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_vz00_2150,
																																				BNIL);
																																			BgL_arg1953z00_2156
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_oz00_2149,
																																				BgL_arg1954z00_2157);
																																		}
																																		BgL_arg1952z00_2155
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_exprz00_2097,
																																			BgL_arg1953z00_2156);
																																	}
																																	BgL_arg1950z00_2153
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1952z00_2155,
																																		BNIL);
																																}
																																BgL_arg1948z00_2151
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1949z00_2152,
																																	BgL_arg1950z00_2153);
																															}
																															BgL_setz00_2085 =
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2453z00zz__evobjectz00,
																																BgL_arg1948z00_2151);
																														}
																													}
																												else
																													{	/* Eval/evobject.scm 663 */
																														BgL_setz00_2085 =
																															BgL_exprz00_2097;
																													}
																											}
																										else
																											{	/* Eval/evobject.scm 653 */
																												goto
																													BgL_tagzd2210zd2_2101;
																											}
																									}
																								else
																									{	/* Eval/evobject.scm 653 */
																										goto BgL_tagzd2210zd2_2101;
																									}
																							}
																						else
																							{	/* Eval/evobject.scm 653 */
																								if (
																									(CAR(
																											((obj_t)
																												BgL_attrz00_2092)) ==
																										BGl_symbol2530z00zz__evobjectz00))
																									{	/* Eval/evobject.scm 653 */
																										if (PAIRP
																											(BgL_cdrzd2252zd2_2120))
																											{	/* Eval/evobject.scm 653 */
																												if (NULLP(CDR
																														(BgL_cdrzd2252zd2_2120)))
																													{	/* Eval/evobject.scm 653 */
																														obj_t
																															BgL_arg1935z00_2134;
																														BgL_arg1935z00_2134
																															=
																															CAR
																															(BgL_cdrzd2252zd2_2120);
																														BgL_defz00_2083 =
																															BgL_arg1935z00_2134;
																													}
																												else
																													{	/* Eval/evobject.scm 653 */
																														goto
																															BgL_tagzd2210zd2_2101;
																													}
																											}
																										else
																											{	/* Eval/evobject.scm 653 */
																												goto
																													BgL_tagzd2210zd2_2101;
																											}
																									}
																								else
																									{	/* Eval/evobject.scm 653 */
																										goto BgL_tagzd2210zd2_2101;
																									}
																							}
																					}
																			}
																	}
																else
																	{	/* Eval/evobject.scm 653 */
																		goto BgL_tagzd2210zd2_2101;
																	}
															}
													}
													{
														obj_t BgL_l1136z00_6211;

														BgL_l1136z00_6211 = CDR(BgL_l1136z00_2089);
														BgL_l1136z00_2089 = BgL_l1136z00_6211;
														goto BgL_zc3z04anonymousza31903ze3z87_2090;
													}
												}
											else
												{	/* Eval/evobject.scm 648 */
													((bool_t) 1);
												}
										}
										{	/* Eval/evobject.scm 677 */
											bool_t BgL_test2785z00_6213;

											if (CBOOL(BgL_getz00_2084))
												{	/* Eval/evobject.scm 677 */
													if (BgL_ronlyz00_2087)
														{	/* Eval/evobject.scm 677 */
															BgL_test2785z00_6213 = ((bool_t) 0);
														}
													else
														{	/* Eval/evobject.scm 677 */
															if (CBOOL(BgL_setz00_2085))
																{	/* Eval/evobject.scm 677 */
																	BgL_test2785z00_6213 = ((bool_t) 0);
																}
															else
																{	/* Eval/evobject.scm 677 */
																	BgL_test2785z00_6213 = ((bool_t) 1);
																}
														}
												}
											else
												{	/* Eval/evobject.scm 677 */
													BgL_test2785z00_6213 = ((bool_t) 0);
												}
											if (BgL_test2785z00_6213)
												{	/* Eval/evobject.scm 679 */
													obj_t BgL_arg1958z00_2165;

													{	/* Eval/evobject.scm 679 */
														obj_t BgL__ortest_1048z00_2166;

														BgL__ortest_1048z00_2166 =
															BGl_getzd2sourcezd2locationz00zz__readerz00
															(BgL_fz00_109);
														if (CBOOL(BgL__ortest_1048z00_2166))
															{	/* Eval/evobject.scm 679 */
																BgL_arg1958z00_2165 = BgL__ortest_1048z00_2166;
															}
														else
															{	/* Eval/evobject.scm 679 */
																BgL_arg1958z00_2165 = BgL_locz00_108;
															}
													}
													return
														BGl_evcompilezd2errorzd2zz__evcompilez00
														(BgL_arg1958z00_2165,
														BGl_string2522z00zz__evobjectz00,
														BGl_string2532z00zz__evobjectz00, BgL_fz00_109);
												}
											else
												{	/* Eval/evobject.scm 681 */
													bool_t BgL_test2790z00_6223;

													if (CBOOL(BgL_setz00_2085))
														{	/* Eval/evobject.scm 681 */
															if (CBOOL(BgL_getz00_2084))
																{	/* Eval/evobject.scm 681 */
																	BgL_test2790z00_6223 = ((bool_t) 0);
																}
															else
																{	/* Eval/evobject.scm 681 */
																	BgL_test2790z00_6223 = ((bool_t) 1);
																}
														}
													else
														{	/* Eval/evobject.scm 681 */
															BgL_test2790z00_6223 = ((bool_t) 0);
														}
													if (BgL_test2790z00_6223)
														{	/* Eval/evobject.scm 683 */
															obj_t BgL_arg1960z00_2168;

															{	/* Eval/evobject.scm 683 */
																obj_t BgL__ortest_1049z00_2169;

																BgL__ortest_1049z00_2169 =
																	BGl_getzd2sourcezd2locationz00zz__readerz00
																	(BgL_fz00_109);
																if (CBOOL(BgL__ortest_1049z00_2169))
																	{	/* Eval/evobject.scm 683 */
																		BgL_arg1960z00_2168 =
																			BgL__ortest_1049z00_2169;
																	}
																else
																	{	/* Eval/evobject.scm 683 */
																		BgL_arg1960z00_2168 = BgL_locz00_108;
																	}
															}
															return
																BGl_evcompilezd2errorzd2zz__evcompilez00
																(BgL_arg1960z00_2168,
																BGl_string2522z00zz__evobjectz00,
																BGl_string2533z00zz__evobjectz00, BgL_fz00_109);
														}
													else
														{	/* Eval/evobject.scm 686 */
															obj_t BgL_sz00_2170;

															{	/* Eval/evobject.scm 687 */
																obj_t BgL_arg1962z00_2172;

																if (CBOOL(BgL_typez00_2082))
																	{	/* Eval/evobject.scm 687 */
																		obj_t BgL__ortest_1050z00_2173;

																		BgL__ortest_1050z00_2173 =
																			BGl_classzd2existszd2zz__objectz00
																			(BgL_typez00_2082);
																		if (CBOOL(BgL__ortest_1050z00_2173))
																			{	/* Eval/evobject.scm 687 */
																				BgL_arg1962z00_2172 =
																					BgL__ortest_1050z00_2173;
																			}
																		else
																			{	/* Eval/evobject.scm 687 */
																				BgL_arg1962z00_2172 = BgL_typez00_2082;
																			}
																	}
																else
																	{	/* Eval/evobject.scm 687 */
																		BgL_arg1962z00_2172 =
																			BGl_symbol2516z00zz__evobjectz00;
																	}
																{	/* Eval/evobject.scm 686 */
																	bool_t BgL_readzd2onlyzf3z21_3746;
																	obj_t BgL_defaultzd2valuezd2_3747;
																	obj_t BgL_getterz00_3748;
																	obj_t BgL_setterz00_3749;
																	obj_t BgL_userzd2infozd2_3750;

																	BgL_readzd2onlyzf3z21_3746 =
																		BgL_ronlyz00_2087;
																	BgL_defaultzd2valuezd2_3747 = BgL_defz00_2083;
																	BgL_getterz00_3748 = BgL_getz00_2084;
																	BgL_setterz00_3749 = BgL_setz00_2085;
																	BgL_userzd2infozd2_3750 = BgL_infoz00_2086;
																	{	/* Eval/evobject.scm 80 */
																		obj_t BgL_newz00_3751;

																		BgL_newz00_3751 =
																			create_struct
																			(BGl_symbol2518z00zz__evobjectz00,
																			(int) (8L));
																		{	/* Eval/evobject.scm 80 */
																			int BgL_tmpz00_6239;

																			BgL_tmpz00_6239 = (int) (7L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6239,
																				BgL_userzd2infozd2_3750);
																		}
																		{	/* Eval/evobject.scm 80 */
																			int BgL_tmpz00_6242;

																			BgL_tmpz00_6242 = (int) (6L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6242, BgL_setterz00_3749);
																		}
																		{	/* Eval/evobject.scm 80 */
																			int BgL_tmpz00_6245;

																			BgL_tmpz00_6245 = (int) (5L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6245, BgL_getterz00_3748);
																		}
																		{	/* Eval/evobject.scm 80 */
																			obj_t BgL_auxz00_6250;
																			int BgL_tmpz00_6248;

																			BgL_auxz00_6250 = BINT(0L);
																			BgL_tmpz00_6248 = (int) (4L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6248, BgL_auxz00_6250);
																		}
																		{	/* Eval/evobject.scm 80 */
																			int BgL_tmpz00_6253;

																			BgL_tmpz00_6253 = (int) (3L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6253,
																				BgL_defaultzd2valuezd2_3747);
																		}
																		{	/* Eval/evobject.scm 80 */
																			obj_t BgL_auxz00_6258;
																			int BgL_tmpz00_6256;

																			BgL_auxz00_6258 =
																				BBOOL(BgL_readzd2onlyzf3z21_3746);
																			BgL_tmpz00_6256 = (int) (2L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6256, BgL_auxz00_6258);
																		}
																		{	/* Eval/evobject.scm 80 */
																			int BgL_tmpz00_6261;

																			BgL_tmpz00_6261 = (int) (1L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6261, BgL_arg1962z00_2172);
																		}
																		{	/* Eval/evobject.scm 80 */
																			int BgL_tmpz00_6264;

																			BgL_tmpz00_6264 = (int) (0L);
																			STRUCT_SET(BgL_newz00_3751,
																				BgL_tmpz00_6264, BgL_idz00_2081);
																		}
																		BgL_sz00_2170 = BgL_newz00_3751;
															}}}
															{	/* Eval/evobject.scm 689 */
																obj_t BgL_list1961z00_2171;

																BgL_list1961z00_2171 =
																	MAKE_YOUNG_PAIR(BgL_sz00_2170, BNIL);
																return BgL_list1961z00_2171;
															}
														}
												}
										}
									}
								}
							}
						}
					else
						{	/* Eval/evobject.scm 636 */
							obj_t BgL_arg1963z00_2174;

							{	/* Eval/evobject.scm 636 */
								obj_t BgL__ortest_1046z00_2175;

								BgL__ortest_1046z00_2175 =
									BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_fz00_109);
								if (CBOOL(BgL__ortest_1046z00_2175))
									{	/* Eval/evobject.scm 636 */
										BgL_arg1963z00_2174 = BgL__ortest_1046z00_2175;
									}
								else
									{	/* Eval/evobject.scm 636 */
										BgL_arg1963z00_2174 = BgL_locz00_108;
									}
							}
							return
								BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_arg1963z00_2174,
								BGl_string2522z00zz__evobjectz00,
								BGl_string2523z00zz__evobjectz00, BgL_fz00_109);
						}
				}
		}

	}



/* eval-parse-class */
	obj_t BGl_evalzd2parsezd2classz00zz__evobjectz00(obj_t BgL_locz00_110,
		obj_t BgL_clausesz00_111)
	{
		{	/* Eval/evobject.scm 697 */
			{	/* Eval/evobject.scm 698 */
				obj_t BgL_locz00_2178;

				{	/* Eval/evobject.scm 698 */
					obj_t BgL__ortest_1051z00_2274;

					BgL__ortest_1051z00_2274 =
						BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_clausesz00_111);
					if (CBOOL(BgL__ortest_1051z00_2274))
						{	/* Eval/evobject.scm 698 */
							BgL_locz00_2178 = BgL__ortest_1051z00_2274;
						}
					else
						{	/* Eval/evobject.scm 698 */
							BgL_locz00_2178 = BgL_locz00_110;
						}
				}
				if (NULLP(BgL_clausesz00_111))
					{	/* Eval/evobject.scm 700 */
						{	/* Eval/evobject.scm 701 */
							int BgL_tmpz00_6277;

							BgL_tmpz00_6277 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6277);
						}
						{	/* Eval/evobject.scm 701 */
							int BgL_tmpz00_6280;

							BgL_tmpz00_6280 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6280, BNIL);
						}
						return BFALSE;
					}
				else
					{	/* Eval/evobject.scm 700 */
						if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_clausesz00_111))
							{	/* Eval/evobject.scm 705 */
								bool_t BgL_test2800z00_6285;

								{	/* Eval/evobject.scm 705 */
									obj_t BgL_ezd2277zd2_2266;

									BgL_ezd2277zd2_2266 = CAR(BgL_clausesz00_111);
									if (PAIRP(BgL_ezd2277zd2_2266))
										{	/* Eval/evobject.scm 705 */
											bool_t BgL_test2802z00_6289;

											{	/* Eval/evobject.scm 705 */
												obj_t BgL_tmpz00_6290;

												BgL_tmpz00_6290 = CAR(BgL_ezd2277zd2_2266);
												BgL_test2802z00_6289 = SYMBOLP(BgL_tmpz00_6290);
											}
											if (BgL_test2802z00_6289)
												{	/* Eval/evobject.scm 705 */
													BgL_test2800z00_6285 =
														NULLP(CDR(BgL_ezd2277zd2_2266));
												}
											else
												{	/* Eval/evobject.scm 705 */
													BgL_test2800z00_6285 = ((bool_t) 0);
												}
										}
									else
										{	/* Eval/evobject.scm 705 */
											BgL_test2800z00_6285 = ((bool_t) 0);
										}
								}
								if (BgL_test2800z00_6285)
									{	/* Eval/evobject.scm 708 */
										obj_t BgL_val0_1143z00_2194;
										obj_t BgL_val1_1144z00_2195;

										{	/* Eval/evobject.scm 708 */
											obj_t BgL_arg1975z00_2196;

											{	/* Eval/evobject.scm 708 */
												obj_t BgL_arg1976z00_2197;
												obj_t BgL_arg1977z00_2198;

												BgL_arg1976z00_2197 =
													MAKE_YOUNG_PAIR(BGl_symbol2534z00zz__evobjectz00,
													BNIL);
												{	/* Eval/evobject.scm 708 */
													obj_t BgL_arg1978z00_2199;

													{	/* Eval/evobject.scm 708 */
														obj_t BgL_arg1979z00_2200;
														obj_t BgL_arg1980z00_2201;

														BgL_arg1979z00_2200 = CAR(CAR(BgL_clausesz00_111));
														BgL_arg1980z00_2201 =
															MAKE_YOUNG_PAIR(BGl_symbol2534z00zz__evobjectz00,
															BNIL);
														BgL_arg1978z00_2199 =
															MAKE_YOUNG_PAIR(BgL_arg1979z00_2200,
															BgL_arg1980z00_2201);
													}
													BgL_arg1977z00_2198 =
														MAKE_YOUNG_PAIR(BgL_arg1978z00_2199, BNIL);
												}
												BgL_arg1975z00_2196 =
													MAKE_YOUNG_PAIR(BgL_arg1976z00_2197,
													BgL_arg1977z00_2198);
											}
											BgL_val0_1143z00_2194 =
												MAKE_YOUNG_PAIR(BGl_symbol2453z00zz__evobjectz00,
												BgL_arg1975z00_2196);
										}
										BgL_val1_1144z00_2195 =
											BGl_zc3z04anonymousza31981ze3ze70z60zz__evobjectz00
											(BgL_locz00_2178, CDR(BgL_clausesz00_111));
										{	/* Eval/evobject.scm 708 */
											int BgL_tmpz00_6305;

											BgL_tmpz00_6305 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6305);
										}
										{	/* Eval/evobject.scm 708 */
											int BgL_tmpz00_6308;

											BgL_tmpz00_6308 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_6308,
												BgL_val1_1144z00_2195);
										}
										return BgL_val0_1143z00_2194;
									}
								else
									{	/* Eval/evobject.scm 712 */
										bool_t BgL_test2803z00_6311;

										{	/* Eval/evobject.scm 712 */
											obj_t BgL_ezd2282zd2_2256;

											BgL_ezd2282zd2_2256 = CAR(BgL_clausesz00_111);
											if (PAIRP(BgL_ezd2282zd2_2256))
												{	/* Eval/evobject.scm 712 */
													obj_t BgL_carzd2283zd2_2258;

													BgL_carzd2283zd2_2258 = CAR(BgL_ezd2282zd2_2256);
													if (PAIRP(BgL_carzd2283zd2_2258))
														{	/* Eval/evobject.scm 712 */
															if (
																(CAR(BgL_carzd2283zd2_2258) ==
																	BGl_symbol2453z00zz__evobjectz00))
																{	/* Eval/evobject.scm 712 */
																	BgL_test2803z00_6311 =
																		NULLP(CDR(BgL_ezd2282zd2_2256));
																}
															else
																{	/* Eval/evobject.scm 712 */
																	BgL_test2803z00_6311 = ((bool_t) 0);
																}
														}
													else
														{	/* Eval/evobject.scm 712 */
															BgL_test2803z00_6311 = ((bool_t) 0);
														}
												}
											else
												{	/* Eval/evobject.scm 712 */
													BgL_test2803z00_6311 = ((bool_t) 0);
												}
										}
										if (BgL_test2803z00_6311)
											{	/* Eval/evobject.scm 715 */
												obj_t BgL_val0_1148z00_2225;
												obj_t BgL_val1_1149z00_2226;

												{	/* Eval/evobject.scm 715 */
													obj_t BgL_arg1995z00_2227;

													{	/* Eval/evobject.scm 715 */
														obj_t BgL_arg1996z00_2228;
														obj_t BgL_arg1997z00_2229;

														BgL_arg1996z00_2228 =
															MAKE_YOUNG_PAIR(BGl_symbol2534z00zz__evobjectz00,
															BNIL);
														{	/* Eval/evobject.scm 715 */
															obj_t BgL_arg1998z00_2230;

															{	/* Eval/evobject.scm 715 */
																obj_t BgL_arg1999z00_2231;
																obj_t BgL_arg2000z00_2232;

																BgL_arg1999z00_2231 =
																	CAR(CAR(BgL_clausesz00_111));
																BgL_arg2000z00_2232 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2534z00zz__evobjectz00, BNIL);
																BgL_arg1998z00_2230 =
																	MAKE_YOUNG_PAIR(BgL_arg1999z00_2231,
																	BgL_arg2000z00_2232);
															}
															BgL_arg1997z00_2229 =
																MAKE_YOUNG_PAIR(BgL_arg1998z00_2230, BNIL);
														}
														BgL_arg1995z00_2227 =
															MAKE_YOUNG_PAIR(BgL_arg1996z00_2228,
															BgL_arg1997z00_2229);
													}
													BgL_val0_1148z00_2225 =
														MAKE_YOUNG_PAIR(BGl_symbol2453z00zz__evobjectz00,
														BgL_arg1995z00_2227);
												}
												BgL_val1_1149z00_2226 =
													BGl_zc3z04anonymousza32001ze3ze70z60zz__evobjectz00
													(BgL_locz00_2178, CDR(BgL_clausesz00_111));
												{	/* Eval/evobject.scm 715 */
													int BgL_tmpz00_6333;

													BgL_tmpz00_6333 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6333);
												}
												{	/* Eval/evobject.scm 715 */
													int BgL_tmpz00_6336;

													BgL_tmpz00_6336 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_6336,
														BgL_val1_1149z00_2226);
												}
												return BgL_val0_1148z00_2225;
											}
										else
											{	/* Eval/evobject.scm 720 */
												obj_t BgL_val1_1153z00_2244;

												BgL_val1_1153z00_2244 =
													BGl_zc3z04anonymousza32007ze3ze70z60zz__evobjectz00
													(BgL_locz00_2178, BgL_clausesz00_111);
												{	/* Eval/evobject.scm 720 */
													int BgL_tmpz00_6340;

													BgL_tmpz00_6340 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6340);
												}
												{	/* Eval/evobject.scm 720 */
													int BgL_tmpz00_6343;

													BgL_tmpz00_6343 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_6343,
														BgL_val1_1153z00_2244);
												}
												return BFALSE;
											}
									}
							}
						else
							{	/* Eval/evobject.scm 703 */
								obj_t BgL_arg2016z00_2272;

								{	/* Eval/evobject.scm 703 */
									obj_t BgL__ortest_1052z00_2273;

									BgL__ortest_1052z00_2273 =
										BGl_getzd2sourcezd2locationz00zz__readerz00
										(BgL_clausesz00_111);
									if (CBOOL(BgL__ortest_1052z00_2273))
										{	/* Eval/evobject.scm 703 */
											BgL_arg2016z00_2272 = BgL__ortest_1052z00_2273;
										}
									else
										{	/* Eval/evobject.scm 703 */
											BgL_arg2016z00_2272 = BgL_locz00_2178;
										}
								}
								return
									BGl_evcompilezd2errorzd2zz__evcompilez00(BgL_arg2016z00_2272,
									BGl_string2522z00zz__evobjectz00,
									BGl_string2536z00zz__evobjectz00, BgL_clausesz00_111);
							}
					}
			}
		}

	}



/* <@anonymous:2007>~0 */
	obj_t BGl_zc3z04anonymousza32007ze3ze70z60zz__evobjectz00(obj_t
		BgL_locz00_4427, obj_t BgL_l1151z00_2246)
	{
		{	/* Eval/evobject.scm 721 */
			if (NULLP(BgL_l1151z00_2246))
				{	/* Eval/evobject.scm 721 */
					return BNIL;
				}
			else
				{	/* Eval/evobject.scm 722 */
					obj_t BgL_arg2009z00_2249;
					obj_t BgL_arg2010z00_2250;

					{	/* Eval/evobject.scm 722 */
						obj_t BgL_fz00_2251;

						BgL_fz00_2251 = CAR(((obj_t) BgL_l1151z00_2246));
						BgL_arg2009z00_2249 =
							BGl_evalzd2parsezd2classzd2slotzd2zz__evobjectz00(BgL_locz00_4427,
							BgL_fz00_2251);
					}
					{	/* Eval/evobject.scm 721 */
						obj_t BgL_arg2011z00_2252;

						BgL_arg2011z00_2252 = CDR(((obj_t) BgL_l1151z00_2246));
						BgL_arg2010z00_2250 =
							BGl_zc3z04anonymousza32007ze3ze70z60zz__evobjectz00
							(BgL_locz00_4427, BgL_arg2011z00_2252);
					}
					return bgl_append2(BgL_arg2009z00_2249, BgL_arg2010z00_2250);
				}
		}

	}



/* <@anonymous:2001>~0 */
	obj_t BGl_zc3z04anonymousza32001ze3ze70z60zz__evobjectz00(obj_t
		BgL_locz00_4428, obj_t BgL_l1146z00_2235)
	{
		{	/* Eval/evobject.scm 718 */
			if (NULLP(BgL_l1146z00_2235))
				{	/* Eval/evobject.scm 718 */
					return BNIL;
				}
			else
				{	/* Eval/evobject.scm 717 */
					obj_t BgL_arg2003z00_2238;
					obj_t BgL_arg2004z00_2239;

					{	/* Eval/evobject.scm 717 */
						obj_t BgL_fz00_2240;

						BgL_fz00_2240 = CAR(((obj_t) BgL_l1146z00_2235));
						BgL_arg2003z00_2238 =
							BGl_evalzd2parsezd2classzd2slotzd2zz__evobjectz00(BgL_locz00_4428,
							BgL_fz00_2240);
					}
					{	/* Eval/evobject.scm 718 */
						obj_t BgL_arg2006z00_2241;

						BgL_arg2006z00_2241 = CDR(((obj_t) BgL_l1146z00_2235));
						BgL_arg2004z00_2239 =
							BGl_zc3z04anonymousza32001ze3ze70z60zz__evobjectz00
							(BgL_locz00_4428, BgL_arg2006z00_2241);
					}
					return bgl_append2(BgL_arg2003z00_2238, BgL_arg2004z00_2239);
				}
		}

	}



/* <@anonymous:1981>~0 */
	obj_t BGl_zc3z04anonymousza31981ze3ze70z60zz__evobjectz00(obj_t
		BgL_locz00_4429, obj_t BgL_l1141z00_2204)
	{
		{	/* Eval/evobject.scm 711 */
			if (NULLP(BgL_l1141z00_2204))
				{	/* Eval/evobject.scm 711 */
					return BNIL;
				}
			else
				{	/* Eval/evobject.scm 710 */
					obj_t BgL_arg1983z00_2207;
					obj_t BgL_arg1984z00_2208;

					{	/* Eval/evobject.scm 710 */
						obj_t BgL_fz00_2209;

						BgL_fz00_2209 = CAR(((obj_t) BgL_l1141z00_2204));
						BgL_arg1983z00_2207 =
							BGl_evalzd2parsezd2classzd2slotzd2zz__evobjectz00(BgL_locz00_4429,
							BgL_fz00_2209);
					}
					{	/* Eval/evobject.scm 711 */
						obj_t BgL_arg1985z00_2210;

						BgL_arg1985z00_2210 = CDR(((obj_t) BgL_l1141z00_2204));
						BgL_arg1984z00_2208 =
							BGl_zc3z04anonymousza31981ze3ze70z60zz__evobjectz00
							(BgL_locz00_4429, BgL_arg1985z00_2210);
					}
					return bgl_append2(BgL_arg1983z00_2207, BgL_arg1984z00_2208);
				}
		}

	}



/* eval-class */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2classzd2zz__evobjectz00(obj_t BgL_idz00_112,
		bool_t BgL_abstractz00_113, obj_t BgL_clausesz00_114, obj_t BgL_srcz00_115,
		obj_t BgL_modz00_116)
	{
		{	/* Eval/evobject.scm 728 */
			{	/* Eval/evobject.scm 729 */
				obj_t BgL_cidz00_2275;

				BgL_cidz00_2275 =
					BGl_decomposezd2identzd2zz__evobjectz00(BgL_idz00_112);
				{	/* Eval/evobject.scm 730 */
					obj_t BgL_sidz00_2276;

					{	/* Eval/evobject.scm 731 */
						obj_t BgL_tmpz00_3784;

						{	/* Eval/evobject.scm 731 */
							int BgL_tmpz00_6378;

							BgL_tmpz00_6378 = (int) (1L);
							BgL_tmpz00_3784 = BGL_MVALUES_VAL(BgL_tmpz00_6378);
						}
						{	/* Eval/evobject.scm 731 */
							int BgL_tmpz00_6381;

							BgL_tmpz00_6381 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6381, BUNSPEC);
						}
						BgL_sidz00_2276 = BgL_tmpz00_3784;
					}
					{	/* Eval/evobject.scm 731 */
						obj_t BgL_locz00_2277;

						BgL_locz00_2277 =
							BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_srcz00_115);
						{	/* Eval/evobject.scm 731 */
							obj_t BgL_sidz00_2278;

							if (CBOOL(BgL_sidz00_2276))
								{	/* Eval/evobject.scm 732 */
									BgL_sidz00_2278 = BgL_sidz00_2276;
								}
							else
								{	/* Eval/evobject.scm 732 */
									BgL_sidz00_2278 = BGl_symbol2537z00zz__evobjectz00;
								}
							{	/* Eval/evobject.scm 732 */
								obj_t BgL_superz00_2279;

								BgL_superz00_2279 =
									BGl_findzd2classzd2zz__objectz00(BgL_sidz00_2278);
								{	/* Eval/evobject.scm 733 */

									if (BGl_classzf3zf3zz__objectz00(BgL_superz00_2279))
										{	/* Eval/evobject.scm 736 */
											obj_t BgL_constructorz00_2281;

											BgL_constructorz00_2281 =
												BGl_evalzd2parsezd2classz00zz__evobjectz00
												(BgL_locz00_2277, BgL_clausesz00_114);
											{	/* Eval/evobject.scm 737 */
												obj_t BgL_slotsz00_2282;

												{	/* Eval/evobject.scm 739 */
													obj_t BgL_tmpz00_3785;

													{	/* Eval/evobject.scm 739 */
														int BgL_tmpz00_6391;

														BgL_tmpz00_6391 = (int) (1L);
														BgL_tmpz00_3785 = BGL_MVALUES_VAL(BgL_tmpz00_6391);
													}
													{	/* Eval/evobject.scm 739 */
														int BgL_tmpz00_6394;

														BgL_tmpz00_6394 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_6394, BUNSPEC);
													}
													BgL_slotsz00_2282 = BgL_tmpz00_3785;
												}
												{	/* Eval/evobject.scm 739 */
													obj_t BgL_claza7za7z00_2283;

													BgL_claza7za7z00_2283 =
														BGl_evalzd2registerzd2classz00zz__evobjectz00
														(BgL_cidz00_2275, BgL_modz00_116, BgL_superz00_2279,
														BgL_abstractz00_113, BgL_slotsz00_2282,
														BGl_getzd2classzd2hashz00zz__evobjectz00
														(BgL_srcz00_115),
														BGl_evalz12z12zz__evalz00(BgL_constructorz00_2281,
															BgL_modz00_116));
													{	/* Eval/evobject.scm 739 */

														{	/* Eval/evobject.scm 744 */
															obj_t BgL_arg2018z00_2284;

															{	/* Eval/evobject.scm 744 */
																obj_t BgL_arg2019z00_2287;

																{	/* Eval/evobject.scm 744 */
																	obj_t BgL_arg2020z00_2288;

																	BgL_arg2020z00_2288 =
																		MAKE_YOUNG_PAIR(BgL_claza7za7z00_2283,
																		BNIL);
																	BgL_arg2019z00_2287 =
																		MAKE_YOUNG_PAIR(BgL_cidz00_2275,
																		BgL_arg2020z00_2288);
																}
																BgL_arg2018z00_2284 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol2539z00zz__evobjectz00,
																	BgL_arg2019z00_2287);
															}
															{	/* Eval/eval.scm 82 */
																obj_t BgL_envz00_2286;

																BgL_envz00_2286 =
																	BGl_defaultzd2environmentzd2zz__evalz00();
																{	/* Eval/eval.scm 82 */

																	BGl_evalz12z12zz__evalz00(BgL_arg2018z00_2284,
																		BgL_envz00_2286);
														}}}
														BGl_evalzd2expandzd2withzd2accesszd2zz__evobjectz00
															(BgL_claza7za7z00_2283);
														{	/* Eval/evobject.scm 749 */
															obj_t BgL_fieldsz00_2289;

															BgL_fieldsz00_2289 =
																BGl_classzd2fieldszd2zz__objectz00
																(BgL_claza7za7z00_2283);
															BGl_patchzd2fieldzd2defaultzd2valuesz12zc0zz__evobjectz00
																(BgL_slotsz00_2282, BgL_fieldsz00_2289,
																BgL_modz00_116);
															{	/* Eval/evobject.scm 753 */
																obj_t BgL_arg2021z00_2290;

																BgL_arg2021z00_2290 =
																	BGL_CLASS_VIRTUAL_FIELDS
																	(BgL_claza7za7z00_2283);
																BGl_patchzd2vfieldzd2accessorsz12z12zz__evobjectz00
																	(BgL_slotsz00_2282, BgL_fieldsz00_2289,
																	BgL_arg2021z00_2290, BgL_modz00_116);
														}}
														if (BgL_abstractz00_113)
															{	/* Eval/evobject.scm 755 */
																BFALSE;
															}
														else
															{	/* Eval/evobject.scm 755 */
																BGl_evalzd2expandzd2instantiatez00zz__evobjectz00
																	(BgL_claza7za7z00_2283);
																BGl_evalzd2expandzd2duplicatez00zz__evobjectz00
																	(BgL_claza7za7z00_2283);
															}
														{	/* Eval/evobject.scm 758 */
															obj_t BgL_list2022z00_2291;

															BgL_list2022z00_2291 =
																MAKE_YOUNG_PAIR(BgL_cidz00_2275, BNIL);
															return BgL_list2022z00_2291;
														}
													}
												}
											}
										}
									else
										{	/* Eval/evobject.scm 734 */
											return
												BGl_evcompilezd2errorzd2zz__evcompilez00
												(BgL_locz00_2277, BGl_string2522z00zz__evobjectz00,
												BGl_string2541z00zz__evobjectz00, BgL_sidz00_2278);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &eval-class */
	obj_t BGl_z62evalzd2classzb0zz__evobjectz00(obj_t BgL_envz00_4417,
		obj_t BgL_idz00_4418, obj_t BgL_abstractz00_4419, obj_t BgL_clausesz00_4420,
		obj_t BgL_srcz00_4421, obj_t BgL_modz00_4422)
	{
		{	/* Eval/evobject.scm 728 */
			{	/* Eval/evobject.scm 729 */
				obj_t BgL_auxz00_6430;
				obj_t BgL_auxz00_6422;
				obj_t BgL_auxz00_6415;

				if (PAIRP(BgL_srcz00_4421))
					{	/* Eval/evobject.scm 729 */
						BgL_auxz00_6430 = BgL_srcz00_4421;
					}
				else
					{
						obj_t BgL_auxz00_6433;

						BgL_auxz00_6433 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(26399L), BGl_string2542z00zz__evobjectz00,
							BGl_string2545z00zz__evobjectz00, BgL_srcz00_4421);
						FAILURE(BgL_auxz00_6433, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_clausesz00_4420))
					{	/* Eval/evobject.scm 729 */
						BgL_auxz00_6422 = BgL_clausesz00_4420;
					}
				else
					{
						obj_t BgL_auxz00_6426;

						BgL_auxz00_6426 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(26399L), BGl_string2542z00zz__evobjectz00,
							BGl_string2544z00zz__evobjectz00, BgL_clausesz00_4420);
						FAILURE(BgL_auxz00_6426, BFALSE, BFALSE);
					}
				if (SYMBOLP(BgL_idz00_4418))
					{	/* Eval/evobject.scm 729 */
						BgL_auxz00_6415 = BgL_idz00_4418;
					}
				else
					{
						obj_t BgL_auxz00_6418;

						BgL_auxz00_6418 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(26399L), BGl_string2542z00zz__evobjectz00,
							BGl_string2543z00zz__evobjectz00, BgL_idz00_4418);
						FAILURE(BgL_auxz00_6418, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2classzd2zz__evobjectz00(BgL_auxz00_6415,
					CBOOL(BgL_abstractz00_4419), BgL_auxz00_6422, BgL_auxz00_6430,
					BgL_modz00_4422);
			}
		}

	}



/* eval-co-instantiate-expander */
	BGL_EXPORTED_DEF obj_t
		BGl_evalzd2cozd2instantiatezd2expanderzd2zz__evobjectz00(obj_t BgL_xz00_117,
		obj_t BgL_ez00_118)
	{
		{	/* Eval/evobject.scm 763 */
			if (NULLP(BgL_xz00_117))
				{	/* Eval/evobject.scm 764 */
					return
						BGl_expandzd2errorzd2zz__evobjectz00
						(BGl_string2546z00zz__evobjectz00, BGl_string2547z00zz__evobjectz00,
						BgL_xz00_117);
				}
			else
				{	/* Eval/evobject.scm 764 */
					obj_t BgL_cdrzd2295zd2_2301;

					BgL_cdrzd2295zd2_2301 = CDR(((obj_t) BgL_xz00_117));
					if ((CAR(((obj_t) BgL_xz00_117)) == BGl_symbol2548z00zz__evobjectz00))
						{	/* Eval/evobject.scm 764 */
							if (PAIRP(BgL_cdrzd2295zd2_2301))
								{	/* Eval/evobject.scm 764 */
									return
										BGl_cozd2instantiatezd2ze3letze3zz__evobjectz00(CAR
										(BgL_cdrzd2295zd2_2301), CDR(BgL_cdrzd2295zd2_2301),
										BgL_xz00_117, BgL_ez00_118);
								}
							else
								{	/* Eval/evobject.scm 764 */
									return
										BGl_expandzd2errorzd2zz__evobjectz00
										(BGl_string2546z00zz__evobjectz00,
										BGl_string2547z00zz__evobjectz00, BgL_xz00_117);
								}
						}
					else
						{	/* Eval/evobject.scm 764 */
							return
								BGl_expandzd2errorzd2zz__evobjectz00
								(BGl_string2546z00zz__evobjectz00,
								BGl_string2547z00zz__evobjectz00, BgL_xz00_117);
						}
				}
		}

	}



/* &eval-co-instantiate-expander */
	obj_t BGl_z62evalzd2cozd2instantiatezd2expanderzb0zz__evobjectz00(obj_t
		BgL_envz00_4423, obj_t BgL_xz00_4424, obj_t BgL_ez00_4425)
	{
		{	/* Eval/evobject.scm 763 */
			{	/* Eval/evobject.scm 764 */
				obj_t BgL_auxz00_6461;
				obj_t BgL_auxz00_6454;

				if (PROCEDUREP(BgL_ez00_4425))
					{	/* Eval/evobject.scm 764 */
						BgL_auxz00_6461 = BgL_ez00_4425;
					}
				else
					{
						obj_t BgL_auxz00_6464;

						BgL_auxz00_6464 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(27757L), BGl_string2549z00zz__evobjectz00,
							BGl_string2550z00zz__evobjectz00, BgL_ez00_4425);
						FAILURE(BgL_auxz00_6464, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_4424))
					{	/* Eval/evobject.scm 764 */
						BgL_auxz00_6454 = BgL_xz00_4424;
					}
				else
					{
						obj_t BgL_auxz00_6457;

						BgL_auxz00_6457 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2477z00zz__evobjectz00,
							BINT(27757L), BGl_string2549z00zz__evobjectz00,
							BGl_string2544z00zz__evobjectz00, BgL_xz00_4424);
						FAILURE(BgL_auxz00_6457, BFALSE, BFALSE);
					}
				return
					BGl_evalzd2cozd2instantiatezd2expanderzd2zz__evobjectz00
					(BgL_auxz00_6454, BgL_auxz00_6461);
			}
		}

	}



/* co-instantiate->let */
	obj_t BGl_cozd2instantiatezd2ze3letze3zz__evobjectz00(obj_t
		BgL_bindingsz00_121, obj_t BgL_bodyz00_122, obj_t BgL_xz00_123,
		obj_t BgL_ez00_124)
	{
		{	/* Eval/evobject.scm 779 */
			{
				obj_t BgL_exprz00_2406;
				obj_t BgL_bdgz00_2407;

				{	/* Eval/evobject.scm 805 */
					obj_t BgL_varsz00_2310;

					if (NULLP(BgL_bindingsz00_121))
						{	/* Eval/evobject.scm 805 */
							BgL_varsz00_2310 = BNIL;
						}
					else
						{	/* Eval/evobject.scm 805 */
							obj_t BgL_head1156z00_2365;

							BgL_head1156z00_2365 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1154z00_2367;
								obj_t BgL_tail1157z00_2368;

								BgL_l1154z00_2367 = BgL_bindingsz00_121;
								BgL_tail1157z00_2368 = BgL_head1156z00_2365;
							BgL_zc3z04anonymousza32071ze3z87_2369:
								if (NULLP(BgL_l1154z00_2367))
									{	/* Eval/evobject.scm 805 */
										BgL_varsz00_2310 = CDR(BgL_head1156z00_2365);
									}
								else
									{	/* Eval/evobject.scm 805 */
										obj_t BgL_newtail1158z00_2371;

										{	/* Eval/evobject.scm 805 */
											obj_t BgL_arg2075z00_2373;

											{	/* Eval/evobject.scm 805 */
												obj_t BgL_bdgz00_2374;

												BgL_bdgz00_2374 = CAR(((obj_t) BgL_l1154z00_2367));
												{
													obj_t BgL_varz00_2375;
													obj_t BgL_exprz00_2376;

													if (PAIRP(BgL_bdgz00_2374))
														{	/* Eval/evobject.scm 806 */
															obj_t BgL_carzd2320zd2_2381;
															obj_t BgL_cdrzd2321zd2_2382;

															BgL_carzd2320zd2_2381 =
																CAR(((obj_t) BgL_bdgz00_2374));
															BgL_cdrzd2321zd2_2382 =
																CDR(((obj_t) BgL_bdgz00_2374));
															if (SYMBOLP(BgL_carzd2320zd2_2381))
																{	/* Eval/evobject.scm 806 */
																	if (PAIRP(BgL_cdrzd2321zd2_2382))
																		{	/* Eval/evobject.scm 806 */
																			if (NULLP(CDR(BgL_cdrzd2321zd2_2382)))
																				{	/* Eval/evobject.scm 806 */
																					BgL_varz00_2375 =
																						BgL_carzd2320zd2_2381;
																					BgL_exprz00_2376 =
																						CAR(BgL_cdrzd2321zd2_2382);
																					{	/* Eval/evobject.scm 808 */
																						obj_t BgL_idzd2typezd2_2389;

																						{	/* Eval/evobject.scm 782 */
																							obj_t BgL_idz00_3929;

																							BgL_idz00_3929 =
																								BGl_decomposezd2identzd2zz__evobjectz00
																								(BgL_varz00_2375);
																							{	/* Eval/evobject.scm 783 */
																								obj_t BgL_typez00_3930;

																								{	/* Eval/evobject.scm 784 */
																									obj_t BgL_tmpz00_3931;

																									{	/* Eval/evobject.scm 784 */
																										int BgL_tmpz00_6491;

																										BgL_tmpz00_6491 =
																											(int) (1L);
																										BgL_tmpz00_3931 =
																											BGL_MVALUES_VAL
																											(BgL_tmpz00_6491);
																									}
																									{	/* Eval/evobject.scm 784 */
																										int BgL_tmpz00_6494;

																										BgL_tmpz00_6494 =
																											(int) (1L);
																										BGL_MVALUES_VAL_SET
																											(BgL_tmpz00_6494,
																											BUNSPEC);
																									}
																									BgL_typez00_3930 =
																										BgL_tmpz00_3931;
																								}
																								BgL_idzd2typezd2_2389 =
																									MAKE_YOUNG_PAIR
																									(BgL_idz00_3929,
																									BgL_typez00_3930);
																						}}
																						{	/* Eval/evobject.scm 808 */
																							obj_t BgL_idz00_2390;

																							BgL_idz00_2390 =
																								CAR(BgL_idzd2typezd2_2389);
																							{	/* Eval/evobject.scm 809 */
																								obj_t BgL_tz00_2391;

																								BgL_tz00_2391 =
																									CDR(BgL_idzd2typezd2_2389);
																								{	/* Eval/evobject.scm 810 */
																									obj_t BgL_klassz00_2392;

																									BgL_exprz00_2406 =
																										BgL_exprz00_2376;
																									BgL_bdgz00_2407 =
																										BgL_bdgz00_2374;
																									{
																										obj_t
																											BgL_instantiatez00_2409;
																										obj_t BgL_bodyz00_2410;

																										if (PAIRP(BgL_exprz00_2406))
																											{	/* Eval/evobject.scm 803 */
																												obj_t
																													BgL_arg2094z00_2415;
																												obj_t
																													BgL_arg2095z00_2416;
																												BgL_arg2094z00_2415 =
																													CAR(((obj_t)
																														BgL_exprz00_2406));
																												BgL_arg2095z00_2416 =
																													CDR(((obj_t)
																														BgL_exprz00_2406));
																												BgL_instantiatez00_2409
																													= BgL_arg2094z00_2415;
																												BgL_bodyz00_2410 =
																													BgL_arg2095z00_2416;
																												{	/* Eval/evobject.scm 789 */
																													obj_t
																														BgL_idzd2typezd2_2417;
																													{	/* Eval/evobject.scm 782 */
																														obj_t
																															BgL_idz00_3820;
																														BgL_idz00_3820 =
																															BGl_decomposezd2identzd2zz__evobjectz00
																															(BgL_instantiatez00_2409);
																														{	/* Eval/evobject.scm 783 */
																															obj_t
																																BgL_typez00_3821;
																															{	/* Eval/evobject.scm 784 */
																																obj_t
																																	BgL_tmpz00_3822;
																																{	/* Eval/evobject.scm 784 */
																																	int
																																		BgL_tmpz00_6507;
																																	BgL_tmpz00_6507
																																		=
																																		(int) (1L);
																																	BgL_tmpz00_3822
																																		=
																																		BGL_MVALUES_VAL
																																		(BgL_tmpz00_6507);
																																}
																																{	/* Eval/evobject.scm 784 */
																																	int
																																		BgL_tmpz00_6510;
																																	BgL_tmpz00_6510
																																		=
																																		(int) (1L);
																																	BGL_MVALUES_VAL_SET
																																		(BgL_tmpz00_6510,
																																		BUNSPEC);
																																}
																																BgL_typez00_3821
																																	=
																																	BgL_tmpz00_3822;
																															}
																															BgL_idzd2typezd2_2417
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_idz00_3820,
																																BgL_typez00_3821);
																													}}
																													{	/* Eval/evobject.scm 789 */
																														obj_t
																															BgL_kclassz00_2418;
																														BgL_kclassz00_2418 =
																															BGl_findzd2classzd2zz__objectz00
																															(CDR
																															(BgL_idzd2typezd2_2417));
																														{	/* Eval/evobject.scm 790 */

																															if (
																																(CAR
																																	(BgL_idzd2typezd2_2417)
																																	==
																																	BGl_symbol2551z00zz__evobjectz00))
																																{	/* Eval/evobject.scm 792 */
																																	if (BGl_classzf3zf3zz__objectz00(BgL_kclassz00_2418))
																																		{	/* Eval/evobject.scm 794 */
																																			if (BGl_classzd2abstractzf3z21zz__objectz00(BgL_kclassz00_2418))
																																				{	/* Eval/evobject.scm 72 */
																																					obj_t
																																						BgL_locz00_3825;
																																					if (EPAIRP(BgL_bdgz00_2407))
																																						{	/* Eval/evobject.scm 72 */
																																							BgL_locz00_3825
																																								=
																																								CER
																																								(
																																								((obj_t) BgL_bdgz00_2407));
																																						}
																																					else
																																						{	/* Eval/evobject.scm 72 */
																																							BgL_locz00_3825
																																								=
																																								BFALSE;
																																						}
																																					{	/* Eval/evobject.scm 73 */
																																						bool_t
																																							BgL_test2833z00_6527;
																																						if (PAIRP(BgL_locz00_3825))
																																							{	/* Eval/evobject.scm 73 */
																																								bool_t
																																									BgL_test2835z00_6530;
																																								{	/* Eval/evobject.scm 73 */
																																									obj_t
																																										BgL_tmpz00_6531;
																																									BgL_tmpz00_6531
																																										=
																																										CDR
																																										(BgL_locz00_3825);
																																									BgL_test2835z00_6530
																																										=
																																										PAIRP
																																										(BgL_tmpz00_6531);
																																								}
																																								if (BgL_test2835z00_6530)
																																									{	/* Eval/evobject.scm 73 */
																																										obj_t
																																											BgL_tmpz00_6534;
																																										BgL_tmpz00_6534
																																											=
																																											CDR
																																											(CDR
																																											(BgL_locz00_3825));
																																										BgL_test2833z00_6527
																																											=
																																											PAIRP
																																											(BgL_tmpz00_6534);
																																									}
																																								else
																																									{	/* Eval/evobject.scm 73 */
																																										BgL_test2833z00_6527
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																						else
																																							{	/* Eval/evobject.scm 73 */
																																								BgL_test2833z00_6527
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																						if (BgL_test2833z00_6527)
																																							{	/* Eval/evobject.scm 73 */
																																								BgL_klassz00_2392
																																									=
																																									BGl_errorzf2locationzf2zz__errorz00
																																									(BgL_instantiatez00_2409,
																																									BGl_string2553z00zz__evobjectz00,
																																									BgL_bdgz00_2407,
																																									CAR
																																									(CDR
																																										(BgL_locz00_3825)),
																																									CAR
																																									(CDR
																																										(CDR
																																											(BgL_locz00_3825))));
																																							}
																																						else
																																							{	/* Eval/evobject.scm 73 */
																																								BgL_klassz00_2392
																																									=
																																									BGl_errorz00zz__errorz00
																																									(BgL_instantiatez00_2409,
																																									BGl_string2553z00zz__evobjectz00,
																																									BgL_bdgz00_2407);
																																							}
																																					}
																																				}
																																			else
																																				{	/* Eval/evobject.scm 796 */
																																					BgL_klassz00_2392
																																						=
																																						BgL_kclassz00_2418;
																																				}
																																		}
																																	else
																																		{	/* Eval/evobject.scm 72 */
																																			obj_t
																																				BgL_locz00_3850;
																																			if (EPAIRP
																																				(BgL_bdgz00_2407))
																																				{	/* Eval/evobject.scm 72 */
																																					BgL_locz00_3850
																																						=
																																						CER(
																																						((obj_t) BgL_bdgz00_2407));
																																				}
																																			else
																																				{	/* Eval/evobject.scm 72 */
																																					BgL_locz00_3850
																																						=
																																						BFALSE;
																																				}
																																			{	/* Eval/evobject.scm 73 */
																																				bool_t
																																					BgL_test2837z00_6549;
																																				if (PAIRP(BgL_locz00_3850))
																																					{	/* Eval/evobject.scm 73 */
																																						bool_t
																																							BgL_test2839z00_6552;
																																						{	/* Eval/evobject.scm 73 */
																																							obj_t
																																								BgL_tmpz00_6553;
																																							BgL_tmpz00_6553
																																								=
																																								CDR
																																								(BgL_locz00_3850);
																																							BgL_test2839z00_6552
																																								=
																																								PAIRP
																																								(BgL_tmpz00_6553);
																																						}
																																						if (BgL_test2839z00_6552)
																																							{	/* Eval/evobject.scm 73 */
																																								obj_t
																																									BgL_tmpz00_6556;
																																								BgL_tmpz00_6556
																																									=
																																									CDR
																																									(CDR
																																									(BgL_locz00_3850));
																																								BgL_test2837z00_6549
																																									=
																																									PAIRP
																																									(BgL_tmpz00_6556);
																																							}
																																						else
																																							{	/* Eval/evobject.scm 73 */
																																								BgL_test2837z00_6549
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																				else
																																					{	/* Eval/evobject.scm 73 */
																																						BgL_test2837z00_6549
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																				if (BgL_test2837z00_6549)
																																					{	/* Eval/evobject.scm 73 */
																																						BgL_klassz00_2392
																																							=
																																							BGl_errorzf2locationzf2zz__errorz00
																																							(BgL_instantiatez00_2409,
																																							BGl_string2554z00zz__evobjectz00,
																																							BgL_bdgz00_2407,
																																							CAR
																																							(CDR
																																								(BgL_locz00_3850)),
																																							CAR
																																							(CDR
																																								(CDR
																																									(BgL_locz00_3850))));
																																					}
																																				else
																																					{	/* Eval/evobject.scm 73 */
																																						BgL_klassz00_2392
																																							=
																																							BGl_errorz00zz__errorz00
																																							(BgL_instantiatez00_2409,
																																							BGl_string2554z00zz__evobjectz00,
																																							BgL_bdgz00_2407);
																																					}
																																			}
																																		}
																																}
																															else
																																{	/* Eval/evobject.scm 72 */
																																	obj_t
																																		BgL_locz00_3875;
																																	if (EPAIRP
																																		(BgL_bdgz00_2407))
																																		{	/* Eval/evobject.scm 72 */
																																			BgL_locz00_3875
																																				=
																																				CER((
																																					(obj_t)
																																					BgL_bdgz00_2407));
																																		}
																																	else
																																		{	/* Eval/evobject.scm 72 */
																																			BgL_locz00_3875
																																				=
																																				BFALSE;
																																		}
																																	{	/* Eval/evobject.scm 73 */
																																		bool_t
																																			BgL_test2841z00_6571;
																																		if (PAIRP
																																			(BgL_locz00_3875))
																																			{	/* Eval/evobject.scm 73 */
																																				bool_t
																																					BgL_test2843z00_6574;
																																				{	/* Eval/evobject.scm 73 */
																																					obj_t
																																						BgL_tmpz00_6575;
																																					BgL_tmpz00_6575
																																						=
																																						CDR
																																						(BgL_locz00_3875);
																																					BgL_test2843z00_6574
																																						=
																																						PAIRP
																																						(BgL_tmpz00_6575);
																																				}
																																				if (BgL_test2843z00_6574)
																																					{	/* Eval/evobject.scm 73 */
																																						obj_t
																																							BgL_tmpz00_6578;
																																						BgL_tmpz00_6578
																																							=
																																							CDR
																																							(CDR
																																							(BgL_locz00_3875));
																																						BgL_test2841z00_6571
																																							=
																																							PAIRP
																																							(BgL_tmpz00_6578);
																																					}
																																				else
																																					{	/* Eval/evobject.scm 73 */
																																						BgL_test2841z00_6571
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		else
																																			{	/* Eval/evobject.scm 73 */
																																				BgL_test2841z00_6571
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																		if (BgL_test2841z00_6571)
																																			{	/* Eval/evobject.scm 73 */
																																				BgL_klassz00_2392
																																					=
																																					BGl_errorzf2locationzf2zz__errorz00
																																					(BgL_instantiatez00_2409,
																																					BGl_string2555z00zz__evobjectz00,
																																					BgL_bdgz00_2407,
																																					CAR
																																					(CDR
																																						(BgL_locz00_3875)),
																																					CAR
																																					(CDR
																																						(CDR
																																							(BgL_locz00_3875))));
																																			}
																																		else
																																			{	/* Eval/evobject.scm 73 */
																																				BgL_klassz00_2392
																																					=
																																					BGl_errorz00zz__errorz00
																																					(BgL_instantiatez00_2409,
																																					BGl_string2555z00zz__evobjectz00,
																																					BgL_bdgz00_2407);
																																			}
																																	}
																																}
																														}
																													}
																												}
																											}
																										else
																											{	/* Eval/evobject.scm 803 */
																												BgL_klassz00_2392 =
																													BGl_expandzd2errorzd2zz__evobjectz00
																													(BGl_string2546z00zz__evobjectz00,
																													BGl_string2555z00zz__evobjectz00,
																													BgL_bdgz00_2407);
																											}
																									}
																									{	/* Eval/evobject.scm 811 */

																										{	/* Eval/evobject.scm 812 */
																											bool_t
																												BgL_test2844z00_6590;
																											if (CBOOL(BgL_tz00_2391))
																												{	/* Eval/evobject.scm 812 */
																													BgL_test2844z00_6590 =
																														(BgL_tz00_2391 ==
																														BGl_classzd2namezd2zz__objectz00
																														(BgL_klassz00_2392));
																												}
																											else
																												{	/* Eval/evobject.scm 812 */
																													BgL_test2844z00_6590 =
																														((bool_t) 1);
																												}
																											if (BgL_test2844z00_6590)
																												{	/* Eval/evobject.scm 813 */
																													obj_t
																														BgL_list2085z00_2395;
																													{	/* Eval/evobject.scm 813 */
																														obj_t
																															BgL_arg2086z00_2396;
																														{	/* Eval/evobject.scm 813 */
																															obj_t
																																BgL_arg2087z00_2397;
																															BgL_arg2087z00_2397
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_exprz00_2376,
																																BNIL);
																															BgL_arg2086z00_2396
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_klassz00_2392,
																																BgL_arg2087z00_2397);
																														}
																														BgL_list2085z00_2395
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_idz00_2390,
																															BgL_arg2086z00_2396);
																													}
																													BgL_arg2075z00_2373 =
																														BgL_list2085z00_2395;
																												}
																											else
																												{	/* Eval/evobject.scm 814 */
																													obj_t
																														BgL_arg2088z00_2398;
																													BgL_arg2088z00_2398 =
																														CAR(BgL_xz00_123);
																													{	/* Eval/evobject.scm 72 */
																														obj_t
																															BgL_locz00_3936;
																														if (EPAIRP
																															(BgL_bdgz00_2374))
																															{	/* Eval/evobject.scm 72 */
																																BgL_locz00_3936
																																	=
																																	CER(((obj_t)
																																		BgL_bdgz00_2374));
																															}
																														else
																															{	/* Eval/evobject.scm 72 */
																																BgL_locz00_3936
																																	= BFALSE;
																															}
																														{	/* Eval/evobject.scm 73 */
																															bool_t
																																BgL_test2847z00_6603;
																															if (PAIRP
																																(BgL_locz00_3936))
																																{	/* Eval/evobject.scm 73 */
																																	bool_t
																																		BgL_test2849z00_6606;
																																	{	/* Eval/evobject.scm 73 */
																																		obj_t
																																			BgL_tmpz00_6607;
																																		BgL_tmpz00_6607
																																			=
																																			CDR
																																			(BgL_locz00_3936);
																																		BgL_test2849z00_6606
																																			=
																																			PAIRP
																																			(BgL_tmpz00_6607);
																																	}
																																	if (BgL_test2849z00_6606)
																																		{	/* Eval/evobject.scm 73 */
																																			obj_t
																																				BgL_tmpz00_6610;
																																			BgL_tmpz00_6610
																																				=
																																				CDR(CDR
																																				(BgL_locz00_3936));
																																			BgL_test2847z00_6603
																																				=
																																				PAIRP
																																				(BgL_tmpz00_6610);
																																		}
																																	else
																																		{	/* Eval/evobject.scm 73 */
																																			BgL_test2847z00_6603
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Eval/evobject.scm 73 */
																																	BgL_test2847z00_6603
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test2847z00_6603)
																																{	/* Eval/evobject.scm 73 */
																																	BgL_arg2075z00_2373
																																		=
																																		BGl_errorzf2locationzf2zz__errorz00
																																		(BgL_arg2088z00_2398,
																																		BGl_string2556z00zz__evobjectz00,
																																		BgL_bdgz00_2374,
																																		CAR(CDR
																																			(BgL_locz00_3936)),
																																		CAR(CDR(CDR
																																				(BgL_locz00_3936))));
																																}
																															else
																																{	/* Eval/evobject.scm 73 */
																																	BgL_arg2075z00_2373
																																		=
																																		BGl_errorz00zz__errorz00
																																		(BgL_arg2088z00_2398,
																																		BGl_string2556z00zz__evobjectz00,
																																		BgL_bdgz00_2374);
																																}
																														}
																													}
																												}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			else
																				{	/* Eval/evobject.scm 806 */
																					BgL_arg2075z00_2373 =
																						BGl_expandzd2errorzd2zz__evobjectz00
																						(CAR(BgL_xz00_123),
																						BGl_string2555z00zz__evobjectz00,
																						BgL_bdgz00_2374);
																				}
																		}
																	else
																		{	/* Eval/evobject.scm 806 */
																			BgL_arg2075z00_2373 =
																				BGl_expandzd2errorzd2zz__evobjectz00(CAR
																				(BgL_xz00_123),
																				BGl_string2555z00zz__evobjectz00,
																				BgL_bdgz00_2374);
																		}
																}
															else
																{	/* Eval/evobject.scm 806 */
																	BgL_arg2075z00_2373 =
																		BGl_expandzd2errorzd2zz__evobjectz00(CAR
																		(BgL_xz00_123),
																		BGl_string2555z00zz__evobjectz00,
																		BgL_bdgz00_2374);
																}
														}
													else
														{	/* Eval/evobject.scm 806 */
															BgL_arg2075z00_2373 =
																BGl_expandzd2errorzd2zz__evobjectz00(CAR
																(BgL_xz00_123),
																BGl_string2555z00zz__evobjectz00,
																BgL_bdgz00_2374);
														}
												}
											}
											BgL_newtail1158z00_2371 =
												MAKE_YOUNG_PAIR(BgL_arg2075z00_2373, BNIL);
										}
										SET_CDR(BgL_tail1157z00_2368, BgL_newtail1158z00_2371);
										{	/* Eval/evobject.scm 805 */
											obj_t BgL_arg2074z00_2372;

											BgL_arg2074z00_2372 = CDR(((obj_t) BgL_l1154z00_2367));
											{
												obj_t BgL_tail1157z00_6635;
												obj_t BgL_l1154z00_6634;

												BgL_l1154z00_6634 = BgL_arg2074z00_2372;
												BgL_tail1157z00_6635 = BgL_newtail1158z00_2371;
												BgL_tail1157z00_2368 = BgL_tail1157z00_6635;
												BgL_l1154z00_2367 = BgL_l1154z00_6634;
												goto BgL_zc3z04anonymousza32071ze3z87_2369;
											}
										}
									}
							}
						}
					{	/* Eval/evobject.scm 819 */
						obj_t BgL_arg2034z00_2311;

						{	/* Eval/evobject.scm 819 */
							obj_t BgL_arg2036z00_2312;
							obj_t BgL_arg2037z00_2313;

							if (NULLP(BgL_varsz00_2310))
								{	/* Eval/evobject.scm 819 */
									BgL_arg2036z00_2312 = BNIL;
								}
							else
								{	/* Eval/evobject.scm 819 */
									obj_t BgL_head1161z00_2316;

									BgL_head1161z00_2316 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1159z00_2318;
										obj_t BgL_tail1162z00_2319;

										BgL_l1159z00_2318 = BgL_varsz00_2310;
										BgL_tail1162z00_2319 = BgL_head1161z00_2316;
									BgL_zc3z04anonymousza32039ze3z87_2320:
										if (NULLP(BgL_l1159z00_2318))
											{	/* Eval/evobject.scm 819 */
												BgL_arg2036z00_2312 = CDR(BgL_head1161z00_2316);
											}
										else
											{	/* Eval/evobject.scm 819 */
												obj_t BgL_newtail1163z00_2322;

												{	/* Eval/evobject.scm 819 */
													obj_t BgL_arg2042z00_2324;

													{	/* Eval/evobject.scm 819 */
														obj_t BgL_varz00_2325;

														BgL_varz00_2325 = CAR(((obj_t) BgL_l1159z00_2318));
														{	/* Eval/evobject.scm 820 */
															obj_t BgL_idz00_2326;
															obj_t BgL_klassz00_2327;

															BgL_idz00_2326 = CAR(((obj_t) BgL_varz00_2325));
															{	/* Eval/evobject.scm 821 */
																obj_t BgL_pairz00_4008;

																BgL_pairz00_4008 =
																	CDR(((obj_t) BgL_varz00_2325));
																BgL_klassz00_2327 = CAR(BgL_pairz00_4008);
															}
															{	/* Eval/evobject.scm 822 */
																obj_t BgL_arg2044z00_2328;
																obj_t BgL_arg2045z00_2329;

																{	/* Eval/evobject.scm 822 */
																	obj_t BgL_arg2046z00_2330;

																	BgL_arg2046z00_2330 =
																		BGl_classzd2namezd2zz__objectz00
																		(BgL_klassz00_2327);
																	{	/* Eval/evobject.scm 822 */
																		obj_t BgL_list2047z00_2331;

																		{	/* Eval/evobject.scm 822 */
																			obj_t BgL_arg2048z00_2332;

																			{	/* Eval/evobject.scm 822 */
																				obj_t BgL_arg2049z00_2333;

																				BgL_arg2049z00_2333 =
																					MAKE_YOUNG_PAIR(BgL_arg2046z00_2330,
																					BNIL);
																				BgL_arg2048z00_2332 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2507z00zz__evobjectz00,
																					BgL_arg2049z00_2333);
																			}
																			BgL_list2047z00_2331 =
																				MAKE_YOUNG_PAIR(BgL_idz00_2326,
																				BgL_arg2048z00_2332);
																		}
																		BgL_arg2044z00_2328 =
																			BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																			(BgL_list2047z00_2331);
																	}
																}
																{	/* Eval/evobject.scm 823 */
																	obj_t BgL_arg2050z00_2334;

																	BgL_arg2050z00_2334 =
																		MAKE_YOUNG_PAIR
																		(BGl_classzd2allocatorzd2zz__objectz00
																		(BgL_klassz00_2327), BNIL);
																	BgL_arg2045z00_2329 =
																		MAKE_YOUNG_PAIR(BgL_arg2050z00_2334, BNIL);
																}
																BgL_arg2042z00_2324 =
																	MAKE_YOUNG_PAIR(BgL_arg2044z00_2328,
																	BgL_arg2045z00_2329);
															}
														}
													}
													BgL_newtail1163z00_2322 =
														MAKE_YOUNG_PAIR(BgL_arg2042z00_2324, BNIL);
												}
												SET_CDR(BgL_tail1162z00_2319, BgL_newtail1163z00_2322);
												{	/* Eval/evobject.scm 819 */
													obj_t BgL_arg2041z00_2323;

													BgL_arg2041z00_2323 =
														CDR(((obj_t) BgL_l1159z00_2318));
													{
														obj_t BgL_tail1162z00_6663;
														obj_t BgL_l1159z00_6662;

														BgL_l1159z00_6662 = BgL_arg2041z00_2323;
														BgL_tail1162z00_6663 = BgL_newtail1163z00_2322;
														BgL_tail1162z00_2319 = BgL_tail1162z00_6663;
														BgL_l1159z00_2318 = BgL_l1159z00_6662;
														goto BgL_zc3z04anonymousza32039ze3z87_2320;
													}
												}
											}
									}
								}
							{	/* Eval/evobject.scm 826 */
								obj_t BgL_arg2052z00_2337;

								{	/* Eval/evobject.scm 826 */
									obj_t BgL_arg2055z00_2338;

									{	/* Eval/evobject.scm 826 */
										obj_t BgL_arg2056z00_2339;
										obj_t BgL_arg2057z00_2340;

										if (NULLP(BgL_varsz00_2310))
											{	/* Eval/evobject.scm 826 */
												BgL_arg2056z00_2339 = BNIL;
											}
										else
											{	/* Eval/evobject.scm 826 */
												obj_t BgL_head1166z00_2343;

												BgL_head1166z00_2343 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1164z00_2345;
													obj_t BgL_tail1167z00_2346;

													BgL_l1164z00_2345 = BgL_varsz00_2310;
													BgL_tail1167z00_2346 = BgL_head1166z00_2343;
												BgL_zc3z04anonymousza32059ze3z87_2347:
													if (NULLP(BgL_l1164z00_2345))
														{	/* Eval/evobject.scm 826 */
															BgL_arg2056z00_2339 = CDR(BgL_head1166z00_2343);
														}
													else
														{	/* Eval/evobject.scm 826 */
															obj_t BgL_newtail1168z00_2349;

															{	/* Eval/evobject.scm 826 */
																obj_t BgL_arg2062z00_2351;

																{	/* Eval/evobject.scm 826 */
																	obj_t BgL_varz00_2352;

																	BgL_varz00_2352 =
																		CAR(((obj_t) BgL_l1164z00_2345));
																	{	/* Eval/evobject.scm 827 */
																		obj_t BgL_idz00_2353;
																		obj_t BgL_klassz00_2354;
																		obj_t BgL_exprz00_2355;

																		BgL_idz00_2353 =
																			CAR(((obj_t) BgL_varz00_2352));
																		{	/* Eval/evobject.scm 828 */
																			obj_t BgL_pairz00_4017;

																			BgL_pairz00_4017 =
																				CDR(((obj_t) BgL_varz00_2352));
																			BgL_klassz00_2354 = CAR(BgL_pairz00_4017);
																		}
																		{	/* Eval/evobject.scm 829 */
																			obj_t BgL_pairz00_4023;

																			{	/* Eval/evobject.scm 829 */
																				obj_t BgL_pairz00_4022;

																				BgL_pairz00_4022 =
																					CDR(((obj_t) BgL_varz00_2352));
																				BgL_pairz00_4023 =
																					CDR(BgL_pairz00_4022);
																			}
																			BgL_exprz00_2355 = CAR(BgL_pairz00_4023);
																		}
																		{	/* Eval/evobject.scm 830 */
																			obj_t BgL_arg2063z00_2356;
																			obj_t BgL_arg2064z00_2357;
																			obj_t BgL_arg2065z00_2358;

																			BgL_arg2063z00_2356 =
																				CAR(((obj_t) BgL_exprz00_2355));
																			BgL_arg2064z00_2357 =
																				CDR(((obj_t) BgL_exprz00_2355));
																			{	/* Eval/evobject.scm 831 */
																				obj_t BgL_tmpz00_6685;

																				BgL_tmpz00_6685 =
																					((obj_t) BgL_klassz00_2354);
																				BgL_arg2065z00_2358 =
																					BGL_CLASS_ALL_FIELDS(BgL_tmpz00_6685);
																			}
																			BgL_arg2062z00_2351 =
																				BGl_instantiatezd2fillzd2zz__evobjectz00
																				(BgL_arg2063z00_2356,
																				BgL_arg2064z00_2357, BgL_klassz00_2354,
																				BgL_arg2065z00_2358, BgL_idz00_2353,
																				BgL_exprz00_2355, BgL_ez00_124);
																		}
																	}
																}
																BgL_newtail1168z00_2349 =
																	MAKE_YOUNG_PAIR(BgL_arg2062z00_2351, BNIL);
															}
															SET_CDR(BgL_tail1167z00_2346,
																BgL_newtail1168z00_2349);
															{	/* Eval/evobject.scm 826 */
																obj_t BgL_arg2061z00_2350;

																BgL_arg2061z00_2350 =
																	CDR(((obj_t) BgL_l1164z00_2345));
																{
																	obj_t BgL_tail1167z00_6694;
																	obj_t BgL_l1164z00_6693;

																	BgL_l1164z00_6693 = BgL_arg2061z00_2350;
																	BgL_tail1167z00_6694 =
																		BgL_newtail1168z00_2349;
																	BgL_tail1167z00_2346 = BgL_tail1167z00_6694;
																	BgL_l1164z00_2345 = BgL_l1164z00_6693;
																	goto BgL_zc3z04anonymousza32059ze3z87_2347;
																}
															}
														}
												}
											}
										{	/* Eval/evobject.scm 833 */
											obj_t BgL_arg2067z00_2360;

											{	/* Eval/evobject.scm 833 */
												obj_t BgL_arg2068z00_2361;

												{	/* Eval/evobject.scm 833 */
													obj_t BgL_arg2069z00_2362;

													BgL_arg2069z00_2362 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_bodyz00_122, BNIL);
													BgL_arg2068z00_2361 =
														MAKE_YOUNG_PAIR(BGl_symbol2490z00zz__evobjectz00,
														BgL_arg2069z00_2362);
												}
												BgL_arg2067z00_2360 =
													BGL_PROCEDURE_CALL2(BgL_ez00_124, BgL_arg2068z00_2361,
													BgL_ez00_124);
											}
											BgL_arg2057z00_2340 =
												MAKE_YOUNG_PAIR(BgL_arg2067z00_2360, BNIL);
										}
										BgL_arg2055z00_2338 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg2056z00_2339, BgL_arg2057z00_2340);
									}
									BgL_arg2052z00_2337 =
										MAKE_YOUNG_PAIR(BGl_symbol2490z00zz__evobjectz00,
										BgL_arg2055z00_2338);
								}
								BgL_arg2037z00_2313 =
									MAKE_YOUNG_PAIR(BgL_arg2052z00_2337, BNIL);
							}
							BgL_arg2034z00_2311 =
								MAKE_YOUNG_PAIR(BgL_arg2036z00_2312, BgL_arg2037z00_2313);
						}
						return
							MAKE_YOUNG_PAIR(BGl_symbol2492z00zz__evobjectz00,
							BgL_arg2034z00_2311);
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evobjectz00(void)
	{
		{	/* Eval/evobject.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__evalz00(35539458L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__expander_definez00(380411245L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
			return BGl_modulezd2initializa7ationz75zz__macroz00(261397491L,
				BSTRING_TO_STRING(BGl_string2557z00zz__evobjectz00));
		}

	}

#ifdef __cplusplus
}
#endif
