/*===========================================================================*/
/*   (Eval/expdstruct.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdstruct.scm -indent -o objs/obj_u/Eval/expdstruct.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_STRUCT_TYPE_DEFINITIONS
#define BGL___EXPANDER_STRUCT_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_STRUCT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1911z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1913z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1915z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1918z00zz__expander_structz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1920z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1922z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1925z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_structz00 =
		BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2definezd2structzd2zz__expander_structz00(obj_t, obj_t);
	static obj_t BGl_list1909z00zz__expander_structz00 = BUNSPEC;
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_structz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_list1910z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1867z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1869z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1871z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1873z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1875z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1877z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1879z00zz__expander_structz00 = BUNSPEC;
	extern obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_symbol1881z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1883z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1885z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1887z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1889z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__expander_structz00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_structz00(void);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_symbol1891z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1893z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_structz00(void);
	static obj_t BGl_symbol1895z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1897z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__expander_structz00(void);
	static obj_t BGl_symbol1899z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_list1866z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__expander_structz00(void);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_structz00(void);
	extern obj_t string_append(obj_t, obj_t);
	extern obj_t
		BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7(obj_t);
	static obj_t
		BGl_z62expandzd2evalzd2definezd2structzb0zz__expander_structz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1901z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1903z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1905z00zz__expander_structz00 = BUNSPEC;
	static obj_t BGl_symbol1907z00zz__expander_structz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1870z00zz__expander_structz00,
		BgL_bgl_string1870za700za7za7_1928za7, "make-", 5);
	      DEFINE_STRING(BGl_string1872z00zz__expander_structz00,
		BgL_bgl_string1872za700za7za7_1929za7, "init", 4);
	      DEFINE_STRING(BGl_string1874z00zz__expander_structz00,
		BgL_bgl_string1874za700za7za7_1930za7, "pair?", 5);
	      DEFINE_STRING(BGl_string1876z00zz__expander_structz00,
		BgL_bgl_string1876za700za7za7_1931za7, "cdr", 3);
	      DEFINE_STRING(BGl_string1878z00zz__expander_structz00,
		BgL_bgl_string1878za700za7za7_1932za7, "null?", 5);
	      DEFINE_STRING(BGl_string1880z00zz__expander_structz00,
		BgL_bgl_string1880za700za7za7_1933za7, "not", 3);
	      DEFINE_STRING(BGl_string1882z00zz__expander_structz00,
		BgL_bgl_string1882za700za7za7_1934za7, "apply", 5);
	      DEFINE_STRING(BGl_string1884z00zz__expander_structz00,
		BgL_bgl_string1884za700za7za7_1935za7, "car", 3);
	      DEFINE_STRING(BGl_string1886z00zz__expander_structz00,
		BgL_bgl_string1886za700za7za7_1936za7, "make-struct", 11);
	      DEFINE_STRING(BGl_string1888z00zz__expander_structz00,
		BgL_bgl_string1888za700za7za7_1937za7, "if", 2);
	      DEFINE_STRING(BGl_string1890z00zz__expander_structz00,
		BgL_bgl_string1890za700za7za7_1938za7, "define-inline", 13);
	      DEFINE_STRING(BGl_string1892z00zz__expander_structz00,
		BgL_bgl_string1892za700za7za7_1939za7, "new", 3);
	      DEFINE_STRING(BGl_string1894z00zz__expander_structz00,
		BgL_bgl_string1894za700za7za7_1940za7, "-set!", 5);
	      DEFINE_STRING(BGl_string1896z00zz__expander_structz00,
		BgL_bgl_string1896za700za7za7_1941za7, "-", 1);
	      DEFINE_STRING(BGl_string1898z00zz__expander_structz00,
		BgL_bgl_string1898za700za7za7_1942za7, "let", 3);
	      DEFINE_STRING(BGl_string1900z00zz__expander_structz00,
		BgL_bgl_string1900za700za7za7_1943za7, "?", 1);
	      DEFINE_STRING(BGl_string1902z00zz__expander_structz00,
		BgL_bgl_string1902za700za7za7_1944za7, "o", 1);
	      DEFINE_STRING(BGl_string1904z00zz__expander_structz00,
		BgL_bgl_string1904za700za7za7_1945za7, "struct?", 7);
	      DEFINE_STRING(BGl_string1906z00zz__expander_structz00,
		BgL_bgl_string1906za700za7za7_1946za7, "struct-key", 10);
	      DEFINE_STRING(BGl_string1908z00zz__expander_structz00,
		BgL_bgl_string1908za700za7za7_1947za7, "eq?", 3);
	      DEFINE_STRING(BGl_string1912z00zz__expander_structz00,
		BgL_bgl_string1912za700za7za7_1948za7, "unspecified", 11);
	      DEFINE_STRING(BGl_string1914z00zz__expander_structz00,
		BgL_bgl_string1914za700za7za7_1949za7, "s", 1);
	      DEFINE_STRING(BGl_string1916z00zz__expander_structz00,
		BgL_bgl_string1916za700za7za7_1950za7, "struct-ref", 10);
	      DEFINE_STRING(BGl_string1917z00zz__expander_structz00,
		BgL_bgl_string1917za700za7za7_1951za7, "struct-ref:not an instance of", 29);
	      DEFINE_STRING(BGl_string1919z00zz__expander_structz00,
		BgL_bgl_string1919za700za7za7_1952za7, "expand-error", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2definezd2structzd2envz00zz__expander_structz00,
		BgL_bgl_za762expandza7d2eval1953z00,
		BGl_z62expandzd2evalzd2definezd2structzb0zz__expander_structz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1921z00zz__expander_structz00,
		BgL_bgl_string1921za700za7za7_1954za7, "v", 1);
	      DEFINE_STRING(BGl_string1923z00zz__expander_structz00,
		BgL_bgl_string1923za700za7za7_1955za7, "struct-set!", 11);
	      DEFINE_STRING(BGl_string1924z00zz__expander_structz00,
		BgL_bgl_string1924za700za7za7_1956za7, "struct-set!:not an instance of",
		30);
	      DEFINE_STRING(BGl_string1926z00zz__expander_structz00,
		BgL_bgl_string1926za700za7za7_1957za7, "begin", 5);
	      DEFINE_STRING(BGl_string1927z00zz__expander_structz00,
		BgL_bgl_string1927za700za7za7_1958za7, "__expander_struct", 17);
	      DEFINE_STRING(BGl_string1864z00zz__expander_structz00,
		BgL_bgl_string1864za700za7za7_1959za7, "define-struct", 13);
	      DEFINE_STRING(BGl_string1865z00zz__expander_structz00,
		BgL_bgl_string1865za700za7za7_1960za7, "Illegal `define-struct' form", 28);
	      DEFINE_STRING(BGl_string1868z00zz__expander_structz00,
		BgL_bgl_string1868za700za7za7_1961za7, "quote", 5);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1911z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1913z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1915z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1918z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1920z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1922z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1925z00zz__expander_structz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_list1909z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_list1910z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1867z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1869z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1871z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1873z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1875z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1877z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1879z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1881z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1883z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1885z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1887z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1889z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1891z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1893z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1895z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1897z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1899z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_list1866z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1901z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1903z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1905z00zz__expander_structz00));
		     ADD_ROOT((void *) (&BGl_symbol1907z00zz__expander_structz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_structz00(long
		BgL_checksumz00_2079, char *BgL_fromz00_2080)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_structz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_structz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_structz00();
					BGl_cnstzd2initzd2zz__expander_structz00();
					BGl_importedzd2moduleszd2initz00zz__expander_structz00();
					return BGl_methodzd2initzd2zz__expander_structz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_structz00(void)
	{
		{	/* Eval/expdstruct.scm 18 */
			BGl_symbol1867z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1868z00zz__expander_structz00);
			BGl_list1866z00zz__expander_structz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1867z00zz__expander_structz00,
				MAKE_YOUNG_PAIR(BNIL, BNIL));
			BGl_symbol1869z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1870z00zz__expander_structz00);
			BGl_symbol1871z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1872z00zz__expander_structz00);
			BGl_symbol1873z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1874z00zz__expander_structz00);
			BGl_symbol1875z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1876z00zz__expander_structz00);
			BGl_symbol1877z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1878z00zz__expander_structz00);
			BGl_symbol1879z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1880z00zz__expander_structz00);
			BGl_symbol1881z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1882z00zz__expander_structz00);
			BGl_symbol1883z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1884z00zz__expander_structz00);
			BGl_symbol1885z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1886z00zz__expander_structz00);
			BGl_symbol1887z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1888z00zz__expander_structz00);
			BGl_symbol1889z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1890z00zz__expander_structz00);
			BGl_symbol1891z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1892z00zz__expander_structz00);
			BGl_symbol1893z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1894z00zz__expander_structz00);
			BGl_symbol1895z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1896z00zz__expander_structz00);
			BGl_symbol1897z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1898z00zz__expander_structz00);
			BGl_symbol1899z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1900z00zz__expander_structz00);
			BGl_symbol1901z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1902z00zz__expander_structz00);
			BGl_symbol1903z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1904z00zz__expander_structz00);
			BGl_symbol1905z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1906z00zz__expander_structz00);
			BGl_symbol1907z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1908z00zz__expander_structz00);
			BGl_symbol1911z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1912z00zz__expander_structz00);
			BGl_list1910z00zz__expander_structz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1911z00zz__expander_structz00, BNIL);
			BGl_list1909z00zz__expander_structz00 =
				MAKE_YOUNG_PAIR(BGl_list1910z00zz__expander_structz00, BNIL);
			BGl_symbol1913z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1914z00zz__expander_structz00);
			BGl_symbol1915z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1916z00zz__expander_structz00);
			BGl_symbol1918z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1919z00zz__expander_structz00);
			BGl_symbol1920z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1921z00zz__expander_structz00);
			BGl_symbol1922z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1923z00zz__expander_structz00);
			return (BGl_symbol1925z00zz__expander_structz00 =
				bstring_to_symbol(BGl_string1926z00zz__expander_structz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_structz00(void)
	{
		{	/* Eval/expdstruct.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* expand-eval-define-struct */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2definezd2structzd2zz__expander_structz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expdstruct.scm 60 */
			{
				obj_t BgL_namez00_1118;
				obj_t BgL_slotsz00_1119;

				if (PAIRP(BgL_xz00_3))
					{	/* Eval/expdstruct.scm 61 */
						obj_t BgL_cdrzd2109zd2_1124;

						BgL_cdrzd2109zd2_1124 = CDR(((obj_t) BgL_xz00_3));
						if (PAIRP(BgL_cdrzd2109zd2_1124))
							{	/* Eval/expdstruct.scm 61 */
								BgL_namez00_1118 = CAR(BgL_cdrzd2109zd2_1124);
								BgL_slotsz00_1119 = CDR(BgL_cdrzd2109zd2_1124);
								BGl_matchzd2definezd2structurez12z12zz__match_normaliza7eza7
									(BgL_xz00_3);
								{	/* Eval/expdstruct.scm 64 */
									long BgL_lenz00_1128;

									BgL_lenz00_1128 = bgl_list_length(BgL_slotsz00_1119);
									{	/* Eval/expdstruct.scm 64 */
										obj_t BgL_slotszd2namezd2_1129;

										if (NULLP(BgL_slotsz00_1119))
											{	/* Eval/expdstruct.scm 65 */
												BgL_slotszd2namezd2_1129 = BNIL;
											}
										else
											{	/* Eval/expdstruct.scm 65 */
												obj_t BgL_head1084z00_1379;

												BgL_head1084z00_1379 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1082z00_1381;
													obj_t BgL_tail1085z00_1382;

													BgL_l1082z00_1381 = BgL_slotsz00_1119;
													BgL_tail1085z00_1382 = BgL_head1084z00_1379;
												BgL_zc3z04anonymousza31496ze3z87_1383:
													if (NULLP(BgL_l1082z00_1381))
														{	/* Eval/expdstruct.scm 65 */
															BgL_slotszd2namezd2_1129 =
																CDR(BgL_head1084z00_1379);
														}
													else
														{	/* Eval/expdstruct.scm 65 */
															obj_t BgL_newtail1086z00_1385;

															{	/* Eval/expdstruct.scm 65 */
																obj_t BgL_arg1499z00_1387;

																{	/* Eval/expdstruct.scm 65 */
																	obj_t BgL_sz00_1388;

																	BgL_sz00_1388 =
																		CAR(((obj_t) BgL_l1082z00_1381));
																	if (PAIRP(BgL_sz00_1388))
																		{	/* Eval/expdstruct.scm 66 */
																			obj_t BgL_cdrzd2125zd2_1396;

																			BgL_cdrzd2125zd2_1396 =
																				CDR(((obj_t) BgL_sz00_1388));
																			if (PAIRP(BgL_cdrzd2125zd2_1396))
																				{	/* Eval/expdstruct.scm 66 */
																					if (NULLP(CDR(BgL_cdrzd2125zd2_1396)))
																						{	/* Eval/expdstruct.scm 66 */
																							BgL_arg1499z00_1387 =
																								CAR(((obj_t) BgL_sz00_1388));
																						}
																					else
																						{	/* Eval/expdstruct.scm 66 */
																							BgL_arg1499z00_1387 =
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string1864z00zz__expander_structz00,
																								BGl_string1865z00zz__expander_structz00,
																								BgL_xz00_3);
																						}
																				}
																			else
																				{	/* Eval/expdstruct.scm 66 */
																					BgL_arg1499z00_1387 =
																						BGl_expandzd2errorzd2zz__expandz00
																						(BGl_string1864z00zz__expander_structz00,
																						BGl_string1865z00zz__expander_structz00,
																						BgL_xz00_3);
																				}
																		}
																	else
																		{	/* Eval/expdstruct.scm 66 */
																			if (SYMBOLP(BgL_sz00_1388))
																				{	/* Eval/expdstruct.scm 66 */
																					BgL_arg1499z00_1387 = BgL_sz00_1388;
																				}
																			else
																				{	/* Eval/expdstruct.scm 66 */
																					BgL_arg1499z00_1387 =
																						BGl_expandzd2errorzd2zz__expandz00
																						(BGl_string1864z00zz__expander_structz00,
																						BGl_string1865z00zz__expander_structz00,
																						BgL_xz00_3);
																				}
																		}
																}
																BgL_newtail1086z00_1385 =
																	MAKE_YOUNG_PAIR(BgL_arg1499z00_1387, BNIL);
															}
															SET_CDR(BgL_tail1085z00_1382,
																BgL_newtail1086z00_1385);
															{	/* Eval/expdstruct.scm 65 */
																obj_t BgL_arg1498z00_1386;

																BgL_arg1498z00_1386 =
																	CDR(((obj_t) BgL_l1082z00_1381));
																{
																	obj_t BgL_tail1085z00_2158;
																	obj_t BgL_l1082z00_2157;

																	BgL_l1082z00_2157 = BgL_arg1498z00_1386;
																	BgL_tail1085z00_2158 =
																		BgL_newtail1086z00_1385;
																	BgL_tail1085z00_1382 = BgL_tail1085z00_2158;
																	BgL_l1082z00_1381 = BgL_l1082z00_2157;
																	goto BgL_zc3z04anonymousza31496ze3z87_1383;
																}
															}
														}
												}
											}
										{	/* Eval/expdstruct.scm 65 */
											bool_t BgL_slotszd2valzf3z21_1130;

											BgL_slotszd2valzf3z21_1130 = ((bool_t) 0);
											{	/* Eval/expdstruct.scm 76 */
												obj_t BgL_slotszd2valzd2_1131;

												if (NULLP(BgL_slotsz00_1119))
													{	/* Eval/expdstruct.scm 77 */
														BgL_slotszd2valzd2_1131 = BNIL;
													}
												else
													{	/* Eval/expdstruct.scm 77 */
														obj_t BgL_head1089z00_1351;

														BgL_head1089z00_1351 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1087z00_1353;
															obj_t BgL_tail1090z00_1354;

															BgL_l1087z00_1353 = BgL_slotsz00_1119;
															BgL_tail1090z00_1354 = BgL_head1089z00_1351;
														BgL_zc3z04anonymousza31481ze3z87_1355:
															if (NULLP(BgL_l1087z00_1353))
																{	/* Eval/expdstruct.scm 77 */
																	BgL_slotszd2valzd2_1131 =
																		CDR(BgL_head1089z00_1351);
																}
															else
																{	/* Eval/expdstruct.scm 77 */
																	obj_t BgL_newtail1091z00_1357;

																	{	/* Eval/expdstruct.scm 77 */
																		obj_t BgL_arg1484z00_1359;

																		{	/* Eval/expdstruct.scm 77 */
																			obj_t BgL_sz00_1360;

																			BgL_sz00_1360 =
																				CAR(((obj_t) BgL_l1087z00_1353));
																			if (PAIRP(BgL_sz00_1360))
																				{	/* Eval/expdstruct.scm 78 */
																					obj_t BgL_cdrzd2138zd2_1367;

																					BgL_cdrzd2138zd2_1367 =
																						CDR(((obj_t) BgL_sz00_1360));
																					if (PAIRP(BgL_cdrzd2138zd2_1367))
																						{	/* Eval/expdstruct.scm 78 */
																							if (NULLP(CDR
																									(BgL_cdrzd2138zd2_1367)))
																								{	/* Eval/expdstruct.scm 78 */
																									obj_t BgL_arg1489z00_1371;

																									BgL_arg1489z00_1371 =
																										CAR(BgL_cdrzd2138zd2_1367);
																									BgL_slotszd2valzf3z21_1130 =
																										((bool_t) 1);
																									BgL_arg1484z00_1359 =
																										BgL_arg1489z00_1371;
																								}
																							else
																								{	/* Eval/expdstruct.scm 78 */
																									BgL_arg1484z00_1359 =
																										BGl_expandzd2errorzd2zz__expandz00
																										(BGl_string1864z00zz__expander_structz00,
																										BGl_string1865z00zz__expander_structz00,
																										BgL_xz00_3);
																								}
																						}
																					else
																						{	/* Eval/expdstruct.scm 78 */
																							BgL_arg1484z00_1359 =
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string1864z00zz__expander_structz00,
																								BGl_string1865z00zz__expander_structz00,
																								BgL_xz00_3);
																						}
																				}
																			else
																				{	/* Eval/expdstruct.scm 78 */
																					if (SYMBOLP(BgL_sz00_1360))
																						{	/* Eval/expdstruct.scm 78 */
																							BgL_arg1484z00_1359 =
																								BGl_list1866z00zz__expander_structz00;
																						}
																					else
																						{	/* Eval/expdstruct.scm 78 */
																							BgL_arg1484z00_1359 =
																								BGl_expandzd2errorzd2zz__expandz00
																								(BGl_string1864z00zz__expander_structz00,
																								BGl_string1865z00zz__expander_structz00,
																								BgL_xz00_3);
																						}
																				}
																		}
																		BgL_newtail1091z00_1357 =
																			MAKE_YOUNG_PAIR(BgL_arg1484z00_1359,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1090z00_1354,
																		BgL_newtail1091z00_1357);
																	{	/* Eval/expdstruct.scm 77 */
																		obj_t BgL_arg1483z00_1358;

																		BgL_arg1483z00_1358 =
																			CDR(((obj_t) BgL_l1087z00_1353));
																		{
																			obj_t BgL_tail1090z00_2187;
																			obj_t BgL_l1087z00_2186;

																			BgL_l1087z00_2186 = BgL_arg1483z00_1358;
																			BgL_tail1090z00_2187 =
																				BgL_newtail1091z00_1357;
																			BgL_tail1090z00_1354 =
																				BgL_tail1090z00_2187;
																			BgL_l1087z00_1353 = BgL_l1087z00_2186;
																			goto
																				BgL_zc3z04anonymousza31481ze3z87_1355;
																		}
																	}
																}
														}
													}
												{	/* Eval/expdstruct.scm 77 */

													{	/* Eval/expdstruct.scm 94 */
														obj_t BgL_arg1189z00_1132;

														{	/* Eval/expdstruct.scm 94 */
															obj_t BgL_arg1190z00_1133;
															obj_t BgL_arg1191z00_1134;

															{	/* Eval/expdstruct.scm 94 */
																obj_t BgL_arg1193z00_1135;

																{	/* Eval/expdstruct.scm 94 */
																	obj_t BgL_arg1194z00_1136;

																	{	/* Eval/expdstruct.scm 94 */
																		obj_t BgL_arg1196z00_1137;

																		{	/* Eval/expdstruct.scm 94 */
																			obj_t BgL_arg1197z00_1138;
																			obj_t BgL_arg1198z00_1139;

																			{	/* Eval/expdstruct.scm 94 */
																				obj_t BgL_arg1199z00_1140;

																				{	/* Eval/expdstruct.scm 94 */
																					obj_t BgL_arg1200z00_1141;

																					{	/* Eval/expdstruct.scm 94 */
																						obj_t BgL_arg1201z00_1142;
																						obj_t BgL_arg1202z00_1143;

																						{	/* Eval/expdstruct.scm 94 */
																							obj_t BgL_symbolz00_1836;

																							BgL_symbolz00_1836 =
																								BGl_symbol1869z00zz__expander_structz00;
																							{	/* Eval/expdstruct.scm 94 */
																								obj_t BgL_arg1664z00_1837;

																								BgL_arg1664z00_1837 =
																									SYMBOL_TO_STRING
																									(BgL_symbolz00_1836);
																								BgL_arg1201z00_1142 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1664z00_1837);
																							}
																						}
																						{	/* Eval/expdstruct.scm 94 */
																							obj_t BgL_arg1664z00_1839;

																							BgL_arg1664z00_1839 =
																								SYMBOL_TO_STRING(
																								((obj_t) BgL_namez00_1118));
																							BgL_arg1202z00_1143 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1664z00_1839);
																						}
																						BgL_arg1200z00_1141 =
																							string_append(BgL_arg1201z00_1142,
																							BgL_arg1202z00_1143);
																					}
																					BgL_arg1199z00_1140 =
																						bstring_to_symbol
																						(BgL_arg1200z00_1141);
																				}
																				BgL_arg1197z00_1138 =
																					MAKE_YOUNG_PAIR(BgL_arg1199z00_1140,
																					BGl_symbol1871z00zz__expander_structz00);
																			}
																			{	/* Eval/expdstruct.scm 95 */
																				obj_t BgL_arg1203z00_1144;

																				if (BgL_slotszd2valzf3z21_1130)
																					{	/* Eval/expdstruct.scm 96 */
																						obj_t BgL_arg1206z00_1145;

																						{	/* Eval/expdstruct.scm 96 */
																							obj_t BgL_arg1208z00_1146;
																							obj_t BgL_arg1209z00_1147;

																							{	/* Eval/expdstruct.scm 96 */
																								obj_t BgL_arg1210z00_1148;

																								BgL_arg1210z00_1148 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1871z00zz__expander_structz00,
																									BNIL);
																								BgL_arg1208z00_1146 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1873z00zz__expander_structz00,
																									BgL_arg1210z00_1148);
																							}
																							{	/* Eval/expdstruct.scm 97 */
																								obj_t BgL_arg1212z00_1149;
																								obj_t BgL_arg1215z00_1150;

																								{	/* Eval/expdstruct.scm 97 */
																									obj_t BgL_arg1216z00_1151;

																									{	/* Eval/expdstruct.scm 97 */
																										obj_t BgL_arg1218z00_1152;
																										obj_t BgL_arg1219z00_1153;

																										{	/* Eval/expdstruct.scm 97 */
																											obj_t BgL_arg1220z00_1154;

																											{	/* Eval/expdstruct.scm 97 */
																												obj_t
																													BgL_arg1221z00_1155;
																												{	/* Eval/expdstruct.scm 97 */
																													obj_t
																														BgL_arg1223z00_1156;
																													{	/* Eval/expdstruct.scm 97 */
																														obj_t
																															BgL_arg1225z00_1157;
																														{	/* Eval/expdstruct.scm 97 */
																															obj_t
																																BgL_arg1226z00_1158;
																															BgL_arg1226z00_1158
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1871z00zz__expander_structz00,
																																BNIL);
																															BgL_arg1225z00_1157
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1875z00zz__expander_structz00,
																																BgL_arg1226z00_1158);
																														}
																														BgL_arg1223z00_1156
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1225z00_1157,
																															BNIL);
																													}
																													BgL_arg1221z00_1155 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1877z00zz__expander_structz00,
																														BgL_arg1223z00_1156);
																												}
																												BgL_arg1220z00_1154 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1221z00_1155,
																													BNIL);
																											}
																											BgL_arg1218z00_1152 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol1879z00zz__expander_structz00,
																												BgL_arg1220z00_1154);
																										}
																										{	/* Eval/expdstruct.scm 98 */
																											obj_t BgL_arg1227z00_1159;
																											obj_t BgL_arg1228z00_1160;

																											{	/* Eval/expdstruct.scm 98 */
																												obj_t
																													BgL_arg1229z00_1161;
																												{	/* Eval/expdstruct.scm 98 */
																													obj_t
																														BgL_arg1230z00_1162;
																													BgL_arg1230z00_1162 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1871z00zz__expander_structz00,
																														BNIL);
																													BgL_arg1229z00_1161 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_1118,
																														BgL_arg1230z00_1162);
																												}
																												BgL_arg1227z00_1159 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1881z00zz__expander_structz00,
																													BgL_arg1229z00_1161);
																											}
																											{	/* Eval/expdstruct.scm 99 */
																												obj_t
																													BgL_arg1231z00_1163;
																												{	/* Eval/expdstruct.scm 99 */
																													obj_t
																														BgL_arg1232z00_1164;
																													{	/* Eval/expdstruct.scm 99 */
																														obj_t
																															BgL_arg1233z00_1165;
																														obj_t
																															BgL_arg1234z00_1166;
																														{	/* Eval/expdstruct.scm 99 */
																															obj_t
																																BgL_arg1236z00_1167;
																															BgL_arg1236z00_1167
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_namez00_1118,
																																BNIL);
																															BgL_arg1233z00_1165
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1867z00zz__expander_structz00,
																																BgL_arg1236z00_1167);
																														}
																														{	/* Eval/expdstruct.scm 99 */
																															obj_t
																																BgL_arg1238z00_1168;
																															{	/* Eval/expdstruct.scm 99 */
																																obj_t
																																	BgL_arg1239z00_1169;
																																{	/* Eval/expdstruct.scm 99 */
																																	obj_t
																																		BgL_arg1242z00_1170;
																																	BgL_arg1242z00_1170
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1871z00zz__expander_structz00,
																																		BNIL);
																																	BgL_arg1239z00_1169
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1883z00zz__expander_structz00,
																																		BgL_arg1242z00_1170);
																																}
																																BgL_arg1238z00_1168
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1239z00_1169,
																																	BNIL);
																															}
																															BgL_arg1234z00_1166
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_lenz00_1128),
																																BgL_arg1238z00_1168);
																														}
																														BgL_arg1232z00_1164
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1233z00_1165,
																															BgL_arg1234z00_1166);
																													}
																													BgL_arg1231z00_1163 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1885z00zz__expander_structz00,
																														BgL_arg1232z00_1164);
																												}
																												BgL_arg1228z00_1160 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1231z00_1163,
																													BNIL);
																											}
																											BgL_arg1219z00_1153 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1227z00_1159,
																												BgL_arg1228z00_1160);
																										}
																										BgL_arg1216z00_1151 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1218z00_1152,
																											BgL_arg1219z00_1153);
																									}
																									BgL_arg1212z00_1149 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1887z00zz__expander_structz00,
																										BgL_arg1216z00_1151);
																								}
																								{	/* Eval/expdstruct.scm 100 */
																									obj_t BgL_arg1244z00_1171;

																									BgL_arg1244z00_1171 =
																										MAKE_YOUNG_PAIR
																										(BgL_namez00_1118,
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_slotszd2valzd2_1131,
																											BNIL));
																									BgL_arg1215z00_1150 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1244z00_1171, BNIL);
																								}
																								BgL_arg1209z00_1147 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1212z00_1149,
																									BgL_arg1215z00_1150);
																							}
																							BgL_arg1206z00_1145 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1208z00_1146,
																								BgL_arg1209z00_1147);
																						}
																						BgL_arg1203z00_1144 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1887z00zz__expander_structz00,
																							BgL_arg1206z00_1145);
																					}
																				else
																					{	/* Eval/expdstruct.scm 101 */
																						obj_t BgL_arg1249z00_1173;

																						{	/* Eval/expdstruct.scm 101 */
																							obj_t BgL_arg1252z00_1174;
																							obj_t BgL_arg1268z00_1175;

																							{	/* Eval/expdstruct.scm 101 */
																								obj_t BgL_arg1272z00_1176;

																								BgL_arg1272z00_1176 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1871z00zz__expander_structz00,
																									BNIL);
																								BgL_arg1252z00_1174 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1873z00zz__expander_structz00,
																									BgL_arg1272z00_1176);
																							}
																							{	/* Eval/expdstruct.scm 102 */
																								obj_t BgL_arg1284z00_1177;
																								obj_t BgL_arg1304z00_1178;

																								{	/* Eval/expdstruct.scm 102 */
																									obj_t BgL_arg1305z00_1179;

																									{	/* Eval/expdstruct.scm 102 */
																										obj_t BgL_arg1306z00_1180;
																										obj_t BgL_arg1307z00_1181;

																										{	/* Eval/expdstruct.scm 102 */
																											obj_t BgL_arg1308z00_1182;

																											{	/* Eval/expdstruct.scm 102 */
																												obj_t
																													BgL_arg1309z00_1183;
																												{	/* Eval/expdstruct.scm 102 */
																													obj_t
																														BgL_arg1310z00_1184;
																													{	/* Eval/expdstruct.scm 102 */
																														obj_t
																															BgL_arg1311z00_1185;
																														{	/* Eval/expdstruct.scm 102 */
																															obj_t
																																BgL_arg1312z00_1186;
																															BgL_arg1312z00_1186
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1871z00zz__expander_structz00,
																																BNIL);
																															BgL_arg1311z00_1185
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1875z00zz__expander_structz00,
																																BgL_arg1312z00_1186);
																														}
																														BgL_arg1310z00_1184
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1311z00_1185,
																															BNIL);
																													}
																													BgL_arg1309z00_1183 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1877z00zz__expander_structz00,
																														BgL_arg1310z00_1184);
																												}
																												BgL_arg1308z00_1182 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1309z00_1183,
																													BNIL);
																											}
																											BgL_arg1306z00_1180 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol1879z00zz__expander_structz00,
																												BgL_arg1308z00_1182);
																										}
																										{	/* Eval/expdstruct.scm 103 */
																											obj_t BgL_arg1314z00_1187;
																											obj_t BgL_arg1315z00_1188;

																											{	/* Eval/expdstruct.scm 103 */
																												obj_t
																													BgL_arg1316z00_1189;
																												{	/* Eval/expdstruct.scm 103 */
																													obj_t
																														BgL_arg1317z00_1190;
																													BgL_arg1317z00_1190 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1871z00zz__expander_structz00,
																														BNIL);
																													BgL_arg1316z00_1189 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_1118,
																														BgL_arg1317z00_1190);
																												}
																												BgL_arg1314z00_1187 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1881z00zz__expander_structz00,
																													BgL_arg1316z00_1189);
																											}
																											{	/* Eval/expdstruct.scm 104 */
																												obj_t
																													BgL_arg1318z00_1191;
																												{	/* Eval/expdstruct.scm 104 */
																													obj_t
																														BgL_arg1319z00_1192;
																													{	/* Eval/expdstruct.scm 104 */
																														obj_t
																															BgL_arg1320z00_1193;
																														obj_t
																															BgL_arg1321z00_1194;
																														{	/* Eval/expdstruct.scm 104 */
																															obj_t
																																BgL_arg1322z00_1195;
																															BgL_arg1322z00_1195
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_namez00_1118,
																																BNIL);
																															BgL_arg1320z00_1193
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1867z00zz__expander_structz00,
																																BgL_arg1322z00_1195);
																														}
																														{	/* Eval/expdstruct.scm 104 */
																															obj_t
																																BgL_arg1323z00_1196;
																															{	/* Eval/expdstruct.scm 104 */
																																obj_t
																																	BgL_arg1325z00_1197;
																																{	/* Eval/expdstruct.scm 104 */
																																	obj_t
																																		BgL_arg1326z00_1198;
																																	BgL_arg1326z00_1198
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1871z00zz__expander_structz00,
																																		BNIL);
																																	BgL_arg1325z00_1197
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1883z00zz__expander_structz00,
																																		BgL_arg1326z00_1198);
																																}
																																BgL_arg1323z00_1196
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1325z00_1197,
																																	BNIL);
																															}
																															BgL_arg1321z00_1194
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_lenz00_1128),
																																BgL_arg1323z00_1196);
																														}
																														BgL_arg1319z00_1192
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1320z00_1193,
																															BgL_arg1321z00_1194);
																													}
																													BgL_arg1318z00_1191 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1885z00zz__expander_structz00,
																														BgL_arg1319z00_1192);
																												}
																												BgL_arg1315z00_1188 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1318z00_1191,
																													BNIL);
																											}
																											BgL_arg1307z00_1181 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1314z00_1187,
																												BgL_arg1315z00_1188);
																										}
																										BgL_arg1305z00_1179 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1306z00_1180,
																											BgL_arg1307z00_1181);
																									}
																									BgL_arg1284z00_1177 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1887z00zz__expander_structz00,
																										BgL_arg1305z00_1179);
																								}
																								{	/* Eval/expdstruct.scm 105 */
																									obj_t BgL_arg1327z00_1199;

																									{	/* Eval/expdstruct.scm 105 */
																										obj_t BgL_arg1328z00_1200;

																										{	/* Eval/expdstruct.scm 105 */
																											obj_t BgL_arg1329z00_1201;
																											obj_t BgL_arg1331z00_1202;

																											{	/* Eval/expdstruct.scm 105 */
																												obj_t
																													BgL_arg1332z00_1203;
																												BgL_arg1332z00_1203 =
																													MAKE_YOUNG_PAIR
																													(BgL_namez00_1118,
																													BNIL);
																												BgL_arg1329z00_1201 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1867z00zz__expander_structz00,
																													BgL_arg1332z00_1203);
																											}
																											{	/* Eval/expdstruct.scm 105 */
																												obj_t
																													BgL_arg1333z00_1204;
																												{	/* Eval/expdstruct.scm 105 */
																													obj_t
																														BgL_arg1334z00_1205;
																													{	/* Eval/expdstruct.scm 105 */
																														obj_t
																															BgL_arg1335z00_1206;
																														BgL_arg1335z00_1206
																															=
																															MAKE_YOUNG_PAIR
																															(BNIL, BNIL);
																														BgL_arg1334z00_1205
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol1867z00zz__expander_structz00,
																															BgL_arg1335z00_1206);
																													}
																													BgL_arg1333z00_1204 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1334z00_1205,
																														BNIL);
																												}
																												BgL_arg1331z00_1202 =
																													MAKE_YOUNG_PAIR(BINT
																													(BgL_lenz00_1128),
																													BgL_arg1333z00_1204);
																											}
																											BgL_arg1328z00_1200 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1329z00_1201,
																												BgL_arg1331z00_1202);
																										}
																										BgL_arg1327z00_1199 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol1885z00zz__expander_structz00,
																											BgL_arg1328z00_1200);
																									}
																									BgL_arg1304z00_1178 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1327z00_1199, BNIL);
																								}
																								BgL_arg1268z00_1175 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1284z00_1177,
																									BgL_arg1304z00_1178);
																							}
																							BgL_arg1249z00_1173 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1252z00_1174,
																								BgL_arg1268z00_1175);
																						}
																						BgL_arg1203z00_1144 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1887z00zz__expander_structz00,
																							BgL_arg1249z00_1173);
																					}
																				BgL_arg1198z00_1139 =
																					MAKE_YOUNG_PAIR(BgL_arg1203z00_1144,
																					BNIL);
																			}
																			BgL_arg1196z00_1137 =
																				MAKE_YOUNG_PAIR(BgL_arg1197z00_1138,
																				BgL_arg1198z00_1139);
																		}
																		BgL_arg1194z00_1136 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1889z00zz__expander_structz00,
																			BgL_arg1196z00_1137);
																	}
																	BgL_arg1193z00_1135 =
																		BGl_evepairifyz00zz__prognz00
																		(BgL_arg1194z00_1136, BgL_xz00_3);
																}
																BgL_arg1190z00_1133 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_4,
																	BgL_arg1193z00_1135, BgL_ez00_4);
															}
															{	/* Eval/expdstruct.scm 110 */
																obj_t BgL_arg1336z00_1207;
																obj_t BgL_arg1337z00_1208;

																{	/* Eval/expdstruct.scm 110 */
																	obj_t BgL_arg1338z00_1209;

																	{	/* Eval/expdstruct.scm 110 */
																		obj_t BgL_arg1339z00_1210;

																		{	/* Eval/expdstruct.scm 110 */
																			obj_t BgL_arg1340z00_1211;

																			{	/* Eval/expdstruct.scm 110 */
																				obj_t BgL_arg1341z00_1212;
																				obj_t BgL_arg1342z00_1213;

																				BgL_arg1341z00_1212 =
																					MAKE_YOUNG_PAIR(BgL_namez00_1118,
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_slotszd2namezd2_1129, BNIL));
																				{	/* Eval/expdstruct.scm 111 */
																					obj_t BgL_arg1344z00_1215;

																					{	/* Eval/expdstruct.scm 111 */
																						obj_t BgL_arg1346z00_1216;

																						{	/* Eval/expdstruct.scm 111 */
																							obj_t BgL_arg1347z00_1217;
																							obj_t BgL_arg1348z00_1218;

																							{	/* Eval/expdstruct.scm 111 */
																								obj_t BgL_arg1349z00_1219;

																								{	/* Eval/expdstruct.scm 111 */
																									obj_t BgL_arg1350z00_1220;

																									{	/* Eval/expdstruct.scm 111 */
																										obj_t BgL_arg1351z00_1221;

																										{	/* Eval/expdstruct.scm 111 */
																											obj_t BgL_arg1352z00_1222;

																											{	/* Eval/expdstruct.scm 111 */
																												obj_t
																													BgL_arg1354z00_1223;
																												obj_t
																													BgL_arg1356z00_1224;
																												{	/* Eval/expdstruct.scm 111 */
																													obj_t
																														BgL_arg1357z00_1225;
																													BgL_arg1357z00_1225 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_1118,
																														BNIL);
																													BgL_arg1354z00_1223 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1867z00zz__expander_structz00,
																														BgL_arg1357z00_1225);
																												}
																												{	/* Eval/expdstruct.scm 111 */
																													obj_t
																														BgL_arg1358z00_1226;
																													{	/* Eval/expdstruct.scm 111 */
																														obj_t
																															BgL_arg1359z00_1227;
																														{	/* Eval/expdstruct.scm 111 */
																															obj_t
																																BgL_arg1360z00_1228;
																															BgL_arg1360z00_1228
																																=
																																MAKE_YOUNG_PAIR
																																(BNIL, BNIL);
																															BgL_arg1359z00_1227
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1867z00zz__expander_structz00,
																																BgL_arg1360z00_1228);
																														}
																														BgL_arg1358z00_1226
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1359z00_1227,
																															BNIL);
																													}
																													BgL_arg1356z00_1224 =
																														MAKE_YOUNG_PAIR(BINT
																														(BgL_lenz00_1128),
																														BgL_arg1358z00_1226);
																												}
																												BgL_arg1352z00_1222 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1354z00_1223,
																													BgL_arg1356z00_1224);
																											}
																											BgL_arg1351z00_1221 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol1885z00zz__expander_structz00,
																												BgL_arg1352z00_1222);
																										}
																										BgL_arg1350z00_1220 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1351z00_1221,
																											BNIL);
																									}
																									BgL_arg1349z00_1219 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1891z00zz__expander_structz00,
																										BgL_arg1350z00_1220);
																								}
																								BgL_arg1347z00_1217 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1349z00_1219, BNIL);
																							}
																							{	/* Eval/expdstruct.scm 112 */
																								obj_t BgL_arg1361z00_1229;
																								obj_t BgL_arg1362z00_1230;

																								{
																									obj_t BgL_slotsz00_1233;
																									obj_t BgL_resz00_1234;

																									BgL_slotsz00_1233 =
																										BgL_slotszd2namezd2_1129;
																									BgL_resz00_1234 = BNIL;
																								BgL_zc3z04anonymousza31363ze3z87_1235:
																									if (NULLP
																										(BgL_slotsz00_1233))
																										{	/* Eval/expdstruct.scm 114 */
																											BgL_arg1361z00_1229 =
																												BgL_resz00_1234;
																										}
																									else
																										{	/* Eval/expdstruct.scm 116 */
																											obj_t BgL_arg1365z00_1237;
																											obj_t BgL_arg1366z00_1238;

																											BgL_arg1365z00_1237 =
																												CDR(
																												((obj_t)
																													BgL_slotsz00_1233));
																											{	/* Eval/expdstruct.scm 119 */
																												obj_t
																													BgL_arg1367z00_1239;
																												{	/* Eval/expdstruct.scm 119 */
																													obj_t
																														BgL_arg1368z00_1240;
																													obj_t
																														BgL_arg1369z00_1241;
																													{	/* Eval/expdstruct.scm 119 */
																														obj_t
																															BgL_arg1370z00_1242;
																														BgL_arg1370z00_1242
																															=
																															CAR(((obj_t)
																																BgL_slotsz00_1233));
																														{	/* Eval/expdstruct.scm 117 */
																															obj_t
																																BgL_list1371z00_1243;
																															{	/* Eval/expdstruct.scm 117 */
																																obj_t
																																	BgL_arg1372z00_1244;
																																{	/* Eval/expdstruct.scm 117 */
																																	obj_t
																																		BgL_arg1373z00_1245;
																																	{	/* Eval/expdstruct.scm 117 */
																																		obj_t
																																			BgL_arg1375z00_1246;
																																		BgL_arg1375z00_1246
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol1893z00zz__expander_structz00,
																																			BNIL);
																																		BgL_arg1373z00_1245
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1370z00_1242,
																																			BgL_arg1375z00_1246);
																																	}
																																	BgL_arg1372z00_1244
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1895z00zz__expander_structz00,
																																		BgL_arg1373z00_1245);
																																}
																																BgL_list1371z00_1243
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_namez00_1118,
																																	BgL_arg1372z00_1244);
																															}
																															BgL_arg1368z00_1240
																																=
																																BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																(BgL_list1371z00_1243);
																														}
																													}
																													{	/* Eval/expdstruct.scm 122 */
																														obj_t
																															BgL_arg1376z00_1247;
																														{	/* Eval/expdstruct.scm 122 */
																															obj_t
																																BgL_arg1377z00_1248;
																															BgL_arg1377z00_1248
																																=
																																CAR(((obj_t)
																																	BgL_slotsz00_1233));
																															BgL_arg1376z00_1247
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1377z00_1248,
																																BNIL);
																														}
																														BgL_arg1369z00_1241
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol1891z00zz__expander_structz00,
																															BgL_arg1376z00_1247);
																													}
																													BgL_arg1367z00_1239 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1368z00_1240,
																														BgL_arg1369z00_1241);
																												}
																												BgL_arg1366z00_1238 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1367z00_1239,
																													BgL_resz00_1234);
																											}
																											{
																												obj_t BgL_resz00_2305;
																												obj_t BgL_slotsz00_2304;

																												BgL_slotsz00_2304 =
																													BgL_arg1365z00_1237;
																												BgL_resz00_2305 =
																													BgL_arg1366z00_1238;
																												BgL_resz00_1234 =
																													BgL_resz00_2305;
																												BgL_slotsz00_1233 =
																													BgL_slotsz00_2304;
																												goto
																													BgL_zc3z04anonymousza31363ze3z87_1235;
																											}
																										}
																								}
																								BgL_arg1362z00_1230 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1891z00zz__expander_structz00,
																									BNIL);
																								BgL_arg1348z00_1218 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_arg1361z00_1229,
																									BgL_arg1362z00_1230);
																							}
																							BgL_arg1346z00_1216 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1347z00_1217,
																								BgL_arg1348z00_1218);
																						}
																						BgL_arg1344z00_1215 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1897z00zz__expander_structz00,
																							BgL_arg1346z00_1216);
																					}
																					BgL_arg1342z00_1213 =
																						MAKE_YOUNG_PAIR(BgL_arg1344z00_1215,
																						BNIL);
																				}
																				BgL_arg1340z00_1211 =
																					MAKE_YOUNG_PAIR(BgL_arg1341z00_1212,
																					BgL_arg1342z00_1213);
																			}
																			BgL_arg1339z00_1210 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1889z00zz__expander_structz00,
																				BgL_arg1340z00_1211);
																		}
																		BgL_arg1338z00_1209 =
																			BGl_evepairifyz00zz__prognz00
																			(BgL_arg1339z00_1210, BgL_xz00_3);
																	}
																	BgL_arg1336z00_1207 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_4,
																		BgL_arg1338z00_1209, BgL_ez00_4);
																}
																{	/* Eval/expdstruct.scm 130 */
																	obj_t BgL_arg1378z00_1250;
																	obj_t BgL_arg1379z00_1251;

																	{	/* Eval/expdstruct.scm 130 */
																		obj_t BgL_arg1380z00_1252;

																		{	/* Eval/expdstruct.scm 130 */
																			obj_t BgL_arg1382z00_1253;

																			{	/* Eval/expdstruct.scm 130 */
																				obj_t BgL_arg1383z00_1254;

																				{	/* Eval/expdstruct.scm 130 */
																					obj_t BgL_arg1384z00_1255;
																					obj_t BgL_arg1387z00_1256;

																					{	/* Eval/expdstruct.scm 130 */
																						obj_t BgL_arg1388z00_1257;
																						obj_t BgL_arg1389z00_1258;

																						{	/* Eval/expdstruct.scm 130 */
																							obj_t BgL_arg1390z00_1259;

																							{	/* Eval/expdstruct.scm 130 */
																								obj_t BgL_arg1391z00_1260;
																								obj_t BgL_arg1392z00_1261;

																								{	/* Eval/expdstruct.scm 130 */
																									obj_t BgL_arg1664z00_1845;

																									BgL_arg1664z00_1845 =
																										SYMBOL_TO_STRING(
																										((obj_t) BgL_namez00_1118));
																									BgL_arg1391z00_1260 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg1664z00_1845);
																								}
																								{	/* Eval/expdstruct.scm 130 */
																									obj_t BgL_symbolz00_1846;

																									BgL_symbolz00_1846 =
																										BGl_symbol1899z00zz__expander_structz00;
																									{	/* Eval/expdstruct.scm 130 */
																										obj_t BgL_arg1664z00_1847;

																										BgL_arg1664z00_1847 =
																											SYMBOL_TO_STRING
																											(BgL_symbolz00_1846);
																										BgL_arg1392z00_1261 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1664z00_1847);
																									}
																								}
																								BgL_arg1390z00_1259 =
																									string_append
																									(BgL_arg1391z00_1260,
																									BgL_arg1392z00_1261);
																							}
																							BgL_arg1388z00_1257 =
																								bstring_to_symbol
																								(BgL_arg1390z00_1259);
																						}
																						BgL_arg1389z00_1258 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1901z00zz__expander_structz00,
																							BNIL);
																						BgL_arg1384z00_1255 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1388z00_1257,
																							BgL_arg1389z00_1258);
																					}
																					{	/* Eval/expdstruct.scm 131 */
																						obj_t BgL_arg1393z00_1262;

																						{	/* Eval/expdstruct.scm 131 */
																							obj_t BgL_arg1394z00_1263;

																							{	/* Eval/expdstruct.scm 131 */
																								obj_t BgL_arg1395z00_1264;
																								obj_t BgL_arg1396z00_1265;

																								{	/* Eval/expdstruct.scm 131 */
																									obj_t BgL_arg1397z00_1266;

																									BgL_arg1397z00_1266 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1901z00zz__expander_structz00,
																										BNIL);
																									BgL_arg1395z00_1264 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1903z00zz__expander_structz00,
																										BgL_arg1397z00_1266);
																								}
																								{	/* Eval/expdstruct.scm 132 */
																									obj_t BgL_arg1399z00_1267;
																									obj_t BgL_arg1400z00_1268;

																									{	/* Eval/expdstruct.scm 132 */
																										obj_t BgL_arg1401z00_1269;

																										{	/* Eval/expdstruct.scm 132 */
																											obj_t BgL_arg1402z00_1270;
																											obj_t BgL_arg1403z00_1271;

																											{	/* Eval/expdstruct.scm 132 */
																												obj_t
																													BgL_arg1404z00_1272;
																												BgL_arg1404z00_1272 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1901z00zz__expander_structz00,
																													BNIL);
																												BgL_arg1402z00_1270 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1905z00zz__expander_structz00,
																													BgL_arg1404z00_1272);
																											}
																											{	/* Eval/expdstruct.scm 132 */
																												obj_t
																													BgL_arg1405z00_1273;
																												{	/* Eval/expdstruct.scm 132 */
																													obj_t
																														BgL_arg1406z00_1274;
																													BgL_arg1406z00_1274 =
																														MAKE_YOUNG_PAIR
																														(BgL_namez00_1118,
																														BNIL);
																													BgL_arg1405z00_1273 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1867z00zz__expander_structz00,
																														BgL_arg1406z00_1274);
																												}
																												BgL_arg1403z00_1271 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1405z00_1273,
																													BNIL);
																											}
																											BgL_arg1401z00_1269 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1402z00_1270,
																												BgL_arg1403z00_1271);
																										}
																										BgL_arg1399z00_1267 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol1907z00zz__expander_structz00,
																											BgL_arg1401z00_1269);
																									}
																									BgL_arg1400z00_1268 =
																										MAKE_YOUNG_PAIR(BFALSE,
																										BNIL);
																									BgL_arg1396z00_1265 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1399z00_1267,
																										BgL_arg1400z00_1268);
																								}
																								BgL_arg1394z00_1263 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1395z00_1264,
																									BgL_arg1396z00_1265);
																							}
																							BgL_arg1393z00_1262 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol1887z00zz__expander_structz00,
																								BgL_arg1394z00_1263);
																						}
																						BgL_arg1387z00_1256 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1393z00_1262, BNIL);
																					}
																					BgL_arg1383z00_1254 =
																						MAKE_YOUNG_PAIR(BgL_arg1384z00_1255,
																						BgL_arg1387z00_1256);
																				}
																				BgL_arg1382z00_1253 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol1889z00zz__expander_structz00,
																					BgL_arg1383z00_1254);
																			}
																			BgL_arg1380z00_1252 =
																				BGl_evepairifyz00zz__prognz00
																				(BgL_arg1382z00_1253, BgL_xz00_3);
																		}
																		BgL_arg1378z00_1250 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_4,
																			BgL_arg1380z00_1252, BgL_ez00_4);
																	}
																	{	/* Eval/expdstruct.scm 137 */
																		obj_t BgL_g1040z00_1275;

																		BgL_g1040z00_1275 =
																			BGl_list1909z00zz__expander_structz00;
																		{
																			long BgL_iz00_1277;
																			obj_t BgL_slotsz00_1278;
																			obj_t BgL_resz00_1279;

																			BgL_iz00_1277 = 0L;
																			BgL_slotsz00_1278 =
																				BgL_slotszd2namezd2_1129;
																			BgL_resz00_1279 = BgL_g1040z00_1275;
																		BgL_zc3z04anonymousza31407ze3z87_1280:
																			if ((BgL_iz00_1277 == BgL_lenz00_1128))
																				{	/* Eval/expdstruct.scm 140 */
																					BgL_arg1379z00_1251 = BgL_resz00_1279;
																				}
																			else
																				{	/* Eval/expdstruct.scm 142 */
																					obj_t BgL_prz00_1282;

																					BgL_prz00_1282 =
																						CAR(((obj_t) BgL_slotsz00_1278));
																					{	/* Eval/expdstruct.scm 143 */
																						long BgL_arg1410z00_1283;
																						obj_t BgL_arg1411z00_1284;
																						obj_t BgL_arg1412z00_1285;

																						BgL_arg1410z00_1283 =
																							(BgL_iz00_1277 + 1L);
																						BgL_arg1411z00_1284 =
																							CDR(((obj_t) BgL_slotsz00_1278));
																						{	/* Eval/expdstruct.scm 149 */
																							obj_t BgL_arg1413z00_1286;
																							obj_t BgL_arg1414z00_1287;

																							{	/* Eval/expdstruct.scm 149 */
																								obj_t BgL_arg1415z00_1288;

																								{	/* Eval/expdstruct.scm 149 */
																									obj_t BgL_arg1416z00_1289;

																									{	/* Eval/expdstruct.scm 149 */
																										obj_t BgL_arg1417z00_1290;

																										{	/* Eval/expdstruct.scm 149 */
																											obj_t BgL_arg1418z00_1291;
																											obj_t BgL_arg1419z00_1292;

																											{	/* Eval/expdstruct.scm 149 */
																												obj_t
																													BgL_arg1420z00_1293;
																												obj_t
																													BgL_arg1421z00_1294;
																												{	/* Eval/expdstruct.scm 149 */
																													obj_t
																														BgL_list1422z00_1295;
																													{	/* Eval/expdstruct.scm 149 */
																														obj_t
																															BgL_arg1423z00_1296;
																														{	/* Eval/expdstruct.scm 149 */
																															obj_t
																																BgL_arg1424z00_1297;
																															BgL_arg1424z00_1297
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_prz00_1282,
																																BNIL);
																															BgL_arg1423z00_1296
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1895z00zz__expander_structz00,
																																BgL_arg1424z00_1297);
																														}
																														BgL_list1422z00_1295
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_namez00_1118,
																															BgL_arg1423z00_1296);
																													}
																													BgL_arg1420z00_1293 =
																														BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																														(BgL_list1422z00_1295);
																												}
																												BgL_arg1421z00_1294 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol1913z00zz__expander_structz00,
																													BNIL);
																												BgL_arg1418z00_1291 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1420z00_1293,
																													BgL_arg1421z00_1294);
																											}
																											{	/* Eval/expdstruct.scm 150 */
																												obj_t
																													BgL_arg1425z00_1298;
																												{	/* Eval/expdstruct.scm 150 */
																													obj_t
																														BgL_arg1426z00_1299;
																													{	/* Eval/expdstruct.scm 150 */
																														obj_t
																															BgL_arg1427z00_1300;
																														obj_t
																															BgL_arg1428z00_1301;
																														{	/* Eval/expdstruct.scm 150 */
																															obj_t
																																BgL_arg1429z00_1302;
																															obj_t
																																BgL_arg1430z00_1303;
																															{	/* Eval/expdstruct.scm 150 */
																																obj_t
																																	BgL_arg1431z00_1304;
																																{	/* Eval/expdstruct.scm 150 */
																																	obj_t
																																		BgL_arg1434z00_1305;
																																	obj_t
																																		BgL_arg1435z00_1306;
																																	{	/* Eval/expdstruct.scm 150 */
																																		obj_t
																																			BgL_arg1664z00_1855;
																																		BgL_arg1664z00_1855
																																			=
																																			SYMBOL_TO_STRING
																																			(((obj_t)
																																				BgL_namez00_1118));
																																		BgL_arg1434z00_1305
																																			=
																																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																			(BgL_arg1664z00_1855);
																																	}
																																	{	/* Eval/expdstruct.scm 150 */
																																		obj_t
																																			BgL_symbolz00_1856;
																																		BgL_symbolz00_1856
																																			=
																																			BGl_symbol1899z00zz__expander_structz00;
																																		{	/* Eval/expdstruct.scm 150 */
																																			obj_t
																																				BgL_arg1664z00_1857;
																																			BgL_arg1664z00_1857
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_symbolz00_1856);
																																			BgL_arg1435z00_1306
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1664z00_1857);
																																	}}
																																	BgL_arg1431z00_1304
																																		=
																																		string_append
																																		(BgL_arg1434z00_1305,
																																		BgL_arg1435z00_1306);
																																}
																																BgL_arg1429z00_1302
																																	=
																																	bstring_to_symbol
																																	(BgL_arg1431z00_1304);
																															}
																															BgL_arg1430z00_1303
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol1913z00zz__expander_structz00,
																																BNIL);
																															BgL_arg1427z00_1300
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1429z00_1302,
																																BgL_arg1430z00_1303);
																														}
																														{	/* Eval/expdstruct.scm 151 */
																															obj_t
																																BgL_arg1436z00_1307;
																															obj_t
																																BgL_arg1437z00_1308;
																															{	/* Eval/expdstruct.scm 151 */
																																obj_t
																																	BgL_arg1438z00_1309;
																																{	/* Eval/expdstruct.scm 151 */
																																	obj_t
																																		BgL_arg1439z00_1310;
																																	BgL_arg1439z00_1310
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BINT
																																		(BgL_iz00_1277),
																																		BNIL);
																																	BgL_arg1438z00_1309
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1913z00zz__expander_structz00,
																																		BgL_arg1439z00_1310);
																																}
																																BgL_arg1436z00_1307
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol1915z00zz__expander_structz00,
																																	BgL_arg1438z00_1309);
																															}
																															{	/* Eval/expdstruct.scm 154 */
																																obj_t
																																	BgL_arg1440z00_1311;
																																{	/* Eval/expdstruct.scm 154 */
																																	obj_t
																																		BgL_arg1441z00_1312;
																																	{	/* Eval/expdstruct.scm 154 */
																																		obj_t
																																			BgL_arg1442z00_1313;
																																		{	/* Eval/expdstruct.scm 154 */
																																			obj_t
																																				BgL_arg1443z00_1314;
																																			obj_t
																																				BgL_arg1444z00_1315;
																																			{	/* Eval/expdstruct.scm 154 */
																																				obj_t
																																					BgL_arg1664z00_1860;
																																				BgL_arg1664z00_1860
																																					=
																																					SYMBOL_TO_STRING
																																					(((obj_t) BgL_namez00_1118));
																																				BgL_arg1443z00_1314
																																					=
																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																					(BgL_arg1664z00_1860);
																																			}
																																			BgL_arg1444z00_1315
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol1913z00zz__expander_structz00,
																																				BNIL);
																																			BgL_arg1442z00_1313
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1443z00_1314,
																																				BgL_arg1444z00_1315);
																																		}
																																		BgL_arg1441z00_1312
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_string1917z00zz__expander_structz00,
																																			BgL_arg1442z00_1313);
																																	}
																																	BgL_arg1440z00_1311
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1918z00zz__expander_structz00,
																																		BgL_arg1441z00_1312);
																																}
																																BgL_arg1437z00_1308
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1440z00_1311,
																																	BNIL);
																															}
																															BgL_arg1428z00_1301
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1436z00_1307,
																																BgL_arg1437z00_1308);
																														}
																														BgL_arg1426z00_1299
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1427z00_1300,
																															BgL_arg1428z00_1301);
																													}
																													BgL_arg1425z00_1298 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol1887z00zz__expander_structz00,
																														BgL_arg1426z00_1299);
																												}
																												BgL_arg1419z00_1292 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1425z00_1298,
																													BNIL);
																											}
																											BgL_arg1417z00_1290 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1418z00_1291,
																												BgL_arg1419z00_1292);
																										}
																										BgL_arg1416z00_1289 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol1889z00zz__expander_structz00,
																											BgL_arg1417z00_1290);
																									}
																									BgL_arg1415z00_1288 =
																										BGl_evepairifyz00zz__prognz00
																										(BgL_arg1416z00_1289,
																										BgL_xz00_3);
																								}
																								BgL_arg1413z00_1286 =
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_4,
																									BgL_arg1415z00_1288,
																									BgL_ez00_4);
																							}
																							{	/* Eval/expdstruct.scm 162 */
																								obj_t BgL_arg1445z00_1316;

																								{	/* Eval/expdstruct.scm 162 */
																									obj_t BgL_arg1446z00_1317;

																									{	/* Eval/expdstruct.scm 162 */
																										obj_t BgL_arg1447z00_1318;

																										{	/* Eval/expdstruct.scm 162 */
																											obj_t BgL_arg1448z00_1319;

																											{	/* Eval/expdstruct.scm 162 */
																												obj_t
																													BgL_arg1449z00_1320;
																												obj_t
																													BgL_arg1450z00_1321;
																												{	/* Eval/expdstruct.scm 162 */
																													obj_t
																														BgL_arg1451z00_1322;
																													obj_t
																														BgL_arg1452z00_1323;
																													{	/* Eval/expdstruct.scm 162 */
																														obj_t
																															BgL_list1453z00_1324;
																														{	/* Eval/expdstruct.scm 162 */
																															obj_t
																																BgL_arg1454z00_1325;
																															{	/* Eval/expdstruct.scm 162 */
																																obj_t
																																	BgL_arg1455z00_1326;
																																{	/* Eval/expdstruct.scm 162 */
																																	obj_t
																																		BgL_arg1456z00_1327;
																																	BgL_arg1456z00_1327
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1893z00zz__expander_structz00,
																																		BNIL);
																																	BgL_arg1455z00_1326
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_prz00_1282,
																																		BgL_arg1456z00_1327);
																																}
																																BgL_arg1454z00_1325
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol1895z00zz__expander_structz00,
																																	BgL_arg1455z00_1326);
																															}
																															BgL_list1453z00_1324
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_namez00_1118,
																																BgL_arg1454z00_1325);
																														}
																														BgL_arg1451z00_1322
																															=
																															BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																															(BgL_list1453z00_1324);
																													}
																													{	/* Eval/expdstruct.scm 162 */
																														obj_t
																															BgL_arg1457z00_1328;
																														BgL_arg1457z00_1328
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol1920z00zz__expander_structz00,
																															BNIL);
																														BgL_arg1452z00_1323
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol1913z00zz__expander_structz00,
																															BgL_arg1457z00_1328);
																													}
																													BgL_arg1449z00_1320 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1451z00_1322,
																														BgL_arg1452z00_1323);
																												}
																												{	/* Eval/expdstruct.scm 163 */
																													obj_t
																														BgL_arg1458z00_1329;
																													{	/* Eval/expdstruct.scm 163 */
																														obj_t
																															BgL_arg1459z00_1330;
																														{	/* Eval/expdstruct.scm 163 */
																															obj_t
																																BgL_arg1460z00_1331;
																															obj_t
																																BgL_arg1461z00_1332;
																															{	/* Eval/expdstruct.scm 163 */
																																obj_t
																																	BgL_arg1462z00_1333;
																																obj_t
																																	BgL_arg1463z00_1334;
																																{	/* Eval/expdstruct.scm 163 */
																																	obj_t
																																		BgL_arg1464z00_1335;
																																	{	/* Eval/expdstruct.scm 163 */
																																		obj_t
																																			BgL_arg1465z00_1336;
																																		obj_t
																																			BgL_arg1466z00_1337;
																																		{	/* Eval/expdstruct.scm 163 */
																																			obj_t
																																				BgL_arg1664z00_1862;
																																			BgL_arg1664z00_1862
																																				=
																																				SYMBOL_TO_STRING
																																				(((obj_t) BgL_namez00_1118));
																																			BgL_arg1465z00_1336
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1664z00_1862);
																																		}
																																		{	/* Eval/expdstruct.scm 163 */
																																			obj_t
																																				BgL_symbolz00_1863;
																																			BgL_symbolz00_1863
																																				=
																																				BGl_symbol1899z00zz__expander_structz00;
																																			{	/* Eval/expdstruct.scm 163 */
																																				obj_t
																																					BgL_arg1664z00_1864;
																																				BgL_arg1664z00_1864
																																					=
																																					SYMBOL_TO_STRING
																																					(BgL_symbolz00_1863);
																																				BgL_arg1466z00_1337
																																					=
																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																					(BgL_arg1664z00_1864);
																																		}}
																																		BgL_arg1464z00_1335
																																			=
																																			string_append
																																			(BgL_arg1465z00_1336,
																																			BgL_arg1466z00_1337);
																																	}
																																	BgL_arg1462z00_1333
																																		=
																																		bstring_to_symbol
																																		(BgL_arg1464z00_1335);
																																}
																																BgL_arg1463z00_1334
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol1913z00zz__expander_structz00,
																																	BNIL);
																																BgL_arg1460z00_1331
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1462z00_1333,
																																	BgL_arg1463z00_1334);
																															}
																															{	/* Eval/expdstruct.scm 164 */
																																obj_t
																																	BgL_arg1467z00_1338;
																																obj_t
																																	BgL_arg1468z00_1339;
																																{	/* Eval/expdstruct.scm 164 */
																																	obj_t
																																		BgL_arg1469z00_1340;
																																	{	/* Eval/expdstruct.scm 164 */
																																		obj_t
																																			BgL_arg1472z00_1341;
																																		{	/* Eval/expdstruct.scm 164 */
																																			obj_t
																																				BgL_arg1473z00_1342;
																																			BgL_arg1473z00_1342
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol1920z00zz__expander_structz00,
																																				BNIL);
																																			BgL_arg1472z00_1341
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BINT
																																				(BgL_iz00_1277),
																																				BgL_arg1473z00_1342);
																																		}
																																		BgL_arg1469z00_1340
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol1913z00zz__expander_structz00,
																																			BgL_arg1472z00_1341);
																																	}
																																	BgL_arg1467z00_1338
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1922z00zz__expander_structz00,
																																		BgL_arg1469z00_1340);
																																}
																																{	/* Eval/expdstruct.scm 167 */
																																	obj_t
																																		BgL_arg1474z00_1343;
																																	{	/* Eval/expdstruct.scm 167 */
																																		obj_t
																																			BgL_arg1476z00_1344;
																																		{	/* Eval/expdstruct.scm 167 */
																																			obj_t
																																				BgL_arg1477z00_1345;
																																			{	/* Eval/expdstruct.scm 167 */
																																				obj_t
																																					BgL_arg1478z00_1346;
																																				obj_t
																																					BgL_arg1479z00_1347;
																																				{	/* Eval/expdstruct.scm 167 */
																																					obj_t
																																						BgL_arg1664z00_1867;
																																					BgL_arg1664z00_1867
																																						=
																																						SYMBOL_TO_STRING
																																						(((obj_t) BgL_namez00_1118));
																																					BgL_arg1478z00_1346
																																						=
																																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																						(BgL_arg1664z00_1867);
																																				}
																																				BgL_arg1479z00_1347
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol1913z00zz__expander_structz00,
																																					BNIL);
																																				BgL_arg1477z00_1345
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1478z00_1346,
																																					BgL_arg1479z00_1347);
																																			}
																																			BgL_arg1476z00_1344
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string1924z00zz__expander_structz00,
																																				BgL_arg1477z00_1345);
																																		}
																																		BgL_arg1474z00_1343
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol1918z00zz__expander_structz00,
																																			BgL_arg1476z00_1344);
																																	}
																																	BgL_arg1468z00_1339
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1474z00_1343,
																																		BNIL);
																																}
																																BgL_arg1461z00_1332
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1467z00_1338,
																																	BgL_arg1468z00_1339);
																															}
																															BgL_arg1459z00_1330
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1460z00_1331,
																																BgL_arg1461z00_1332);
																														}
																														BgL_arg1458z00_1329
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol1887z00zz__expander_structz00,
																															BgL_arg1459z00_1330);
																													}
																													BgL_arg1450z00_1321 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1458z00_1329,
																														BNIL);
																												}
																												BgL_arg1448z00_1319 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1449z00_1320,
																													BgL_arg1450z00_1321);
																											}
																											BgL_arg1447z00_1318 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol1889z00zz__expander_structz00,
																												BgL_arg1448z00_1319);
																										}
																										BgL_arg1446z00_1317 =
																											BGl_evepairifyz00zz__prognz00
																											(BgL_arg1447z00_1318,
																											BgL_xz00_3);
																									}
																									BgL_arg1445z00_1316 =
																										BGL_PROCEDURE_CALL2
																										(BgL_ez00_4,
																										BgL_arg1446z00_1317,
																										BgL_ez00_4);
																								}
																								BgL_arg1414z00_1287 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1445z00_1316,
																									BgL_resz00_1279);
																							}
																							BgL_arg1412z00_1285 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1413z00_1286,
																								BgL_arg1414z00_1287);
																						}
																						{
																							obj_t BgL_resz00_2442;
																							obj_t BgL_slotsz00_2441;
																							long BgL_iz00_2440;

																							BgL_iz00_2440 =
																								BgL_arg1410z00_1283;
																							BgL_slotsz00_2441 =
																								BgL_arg1411z00_1284;
																							BgL_resz00_2442 =
																								BgL_arg1412z00_1285;
																							BgL_resz00_1279 = BgL_resz00_2442;
																							BgL_slotsz00_1278 =
																								BgL_slotsz00_2441;
																							BgL_iz00_1277 = BgL_iz00_2440;
																							goto
																								BgL_zc3z04anonymousza31407ze3z87_1280;
																						}
																					}
																				}
																		}
																	}
																	BgL_arg1337z00_1208 =
																		MAKE_YOUNG_PAIR(BgL_arg1378z00_1250,
																		BgL_arg1379z00_1251);
																}
																BgL_arg1191z00_1134 =
																	MAKE_YOUNG_PAIR(BgL_arg1336z00_1207,
																	BgL_arg1337z00_1208);
															}
															BgL_arg1189z00_1132 =
																MAKE_YOUNG_PAIR(BgL_arg1190z00_1133,
																BgL_arg1191z00_1134);
														}
														return
															MAKE_YOUNG_PAIR
															(BGl_symbol1925z00zz__expander_structz00,
															BgL_arg1189z00_1132);
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Eval/expdstruct.scm 61 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string1864z00zz__expander_structz00,
									BGl_string1865z00zz__expander_structz00, BgL_xz00_3);
							}
					}
				else
					{	/* Eval/expdstruct.scm 61 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string1864z00zz__expander_structz00,
							BGl_string1865z00zz__expander_structz00, BgL_xz00_3);
					}
			}
		}

	}



/* &expand-eval-define-struct */
	obj_t BGl_z62expandzd2evalzd2definezd2structzb0zz__expander_structz00(obj_t
		BgL_envz00_2048, obj_t BgL_xz00_2049, obj_t BgL_ez00_2050)
	{
		{	/* Eval/expdstruct.scm 60 */
			return
				BGl_expandzd2evalzd2definezd2structzd2zz__expander_structz00
				(BgL_xz00_2049, BgL_ez00_2050);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_structz00(void)
	{
		{	/* Eval/expdstruct.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_structz00(void)
	{
		{	/* Eval/expdstruct.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_structz00(void)
	{
		{	/* Eval/expdstruct.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_structz00(void)
	{
		{	/* Eval/expdstruct.scm 18 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(515155867L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1927z00zz__expander_structz00));
		}

	}

#ifdef __cplusplus
}
#endif
