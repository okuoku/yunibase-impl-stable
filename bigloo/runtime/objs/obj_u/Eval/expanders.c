/*===========================================================================*/
/*   (Eval/expanders.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expanders.scm -indent -o objs/obj_u/Eval/expanders.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___INSTALL_EXPANDERS_TYPE_DEFINITIONS
#define BGL___INSTALL_EXPANDERS_TYPE_DEFINITIONS
#endif													// BGL___INSTALL_EXPANDERS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2480z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2482z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_expandzd2matchzd2lambdaz00zz__match_expandz00(obj_t);
	static obj_t BGl_symbol2487z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2489z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_makezd2expandzd2withzd2tracezd2zz__expander_tracez00(obj_t);
	static obj_t
		BGl_z52installzd2allzd2expandersz12z40zz__install_expandersz00(void);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_expandzd2matchzd2casez00zz__match_expandz00(obj_t);
	static obj_t BGl_symbol2492z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2497z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_symbol2499z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31435ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__install_expandersz00 =
		BUNSPEC;
	static obj_t BGl_expandzd2errorzd2zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_makezd2ifze70z35zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static bool_t BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31371ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	extern obj_t BGl_expandzd2definezd2macroz00zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__install_expandersz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evobjectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_expandz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgc_expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__lalr_expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_tracez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_argsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_recordz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_structz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_tryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_doz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_definez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_casez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_boolz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_letz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expander_quotez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t, obj_t);
	extern obj_t BGl_installzd2compilerzd2expanderz00zz__macroz00(obj_t, obj_t);
	extern obj_t BGl_makezd2expandzd2tracezd2itemzd2zz__expander_tracez00(obj_t);
	extern obj_t BGl_makezd2expandzd2whenzd2tracezd2zz__expander_tracez00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31380ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_expandzd2condzd2zz__expander_boolz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31348ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_relativezd2filezd2namez00zz__osz00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31679ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2491z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__install_expandersz00(void);
	extern obj_t BGl_expandzd2tryzd2zz__expander_tryz00(obj_t, obj_t);
	static obj_t BGl_list2496z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2andzd2letza2zc0zz__install_expandersz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31803ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31390ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31374ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31447ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__install_expandersz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_genericzd2initzd2zz__install_expandersz00(void);
	static obj_t BGl_symbol2304z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_symbol2306z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2308z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__install_expandersz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__install_expandersz00(void);
	static obj_t
		BGl_z62zc3z04anonymousza31383ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__install_expandersz00(void);
	static obj_t
		BGl_z62installzd2allzd2expandersz12z70zz__install_expandersz00(obj_t);
	static obj_t BGl_symbol2310z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2313z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2316z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2319z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31401ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31392ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31627ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31376ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2400z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2403z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2322z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_expandzd2dozd2zz__expander_doz00(obj_t, obj_t);
	static obj_t BGl_symbol2406z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2325z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2409z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2328z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_pwdz00zz__osz00(void);
	static obj_t BGl_expandzd2testzd2zz__install_expandersz00(obj_t, obj_t);
	extern obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2tprintzb0zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2412z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2331z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2414z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2334z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2ifzb0zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2416z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2418z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2337z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31330ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31807ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31378ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31459ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_expandzd2quotezd2zz__expander_quotez00(obj_t, obj_t);
	extern obj_t BGl_expandzd2definezd2expanderz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_symbol2501z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2420z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_symbol2340z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2422z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2424z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2343z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2426z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2346z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2428z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2349z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_installzd2evalzd2expanderz00zz__macroz00(obj_t, obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31646ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31395ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2430z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2432z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2352z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__install_expandersz00(void);
	static obj_t BGl_symbol2434z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2436z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2355z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2438z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2357z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_quasiquotationz00zz__expander_quotez00(obj_t, obj_t);
	static obj_t BGl_symbol2359z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31477ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_keyword2484z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31388ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evmodulezd2staticzd2classz00zz__evmodulez00(obj_t);
	static obj_t BGl_symbol2441z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2361z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_expandzd2definezd2patternz00zz__evalz00(obj_t);
	static obj_t BGl_symbol2444z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2363z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2447z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2366z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2449z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2369z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_z62ez62zz__install_expandersz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(int,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_installzd2allzd2expandersz12z12zz__install_expandersz00(void);
	extern obj_t BGl_evepairifyzd2deepzd2zz__prognz00(obj_t, obj_t);
	static obj_t BGl_symbol2452z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2372z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2454z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2456z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2375z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2458z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2377z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2379z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31415ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31495ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2nilza2z00zz__evalz00;
	static obj_t
		BGl_z62zc3z04anonymousza31398ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2461z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2381z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2463z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2383z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2465z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2385z00zz__install_expandersz00 = BUNSPEC;
	static bool_t BGl_za2expanderszd2installedzd2pza2z00zz__install_expandersz00;
	static obj_t BGl_symbol2468z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2387z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2389z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_expandzd2definezd2hygienezd2macrozd2zz__evalz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2470z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2472z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2391z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2474z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2393z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2476z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2395z00zz__install_expandersz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2478z00zz__install_expandersz00 = BUNSPEC;
	static obj_t BGl_symbol2397z00zz__install_expandersz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04anonymousza31328ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31764ze3ze5zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2beginzb0zz__install_expandersz00(obj_t, obj_t,
		obj_t);
	static obj_t *__cnst;


	extern obj_t BGl_expandzd2evalzd2definezd2envzd2zz__expander_definez00;
	extern obj_t BGl_expandzd2regularzd2grammarzd2envzd2zz__rgc_expandz00;
	extern obj_t BGl_expandzd2evalzd2z42letzd2envz90zz__expander_letz00;
	extern obj_t
		BGl_expandzd2definezd2recordzd2typezd2envz00zz__expander_recordz00;
	      DEFINE_STRING(BGl_string2305z00zz__install_expandersz00,
		BgL_bgl_string2305za700za7za7_2508za7, "test-aux-for-nil", 16);
	      DEFINE_STRING(BGl_string2307z00zz__install_expandersz00,
		BgL_bgl_string2307za700za7za7_2509za7, "null?", 5);
	      DEFINE_STRING(BGl_string2309z00zz__install_expandersz00,
		BgL_bgl_string2309za700za7za7_2510za7, "if", 2);
	extern obj_t BGl_expandzd2evalzd2letza2zd2envz70zz__expander_letz00;
	extern obj_t BGl_expandzd2evalzd2labelszd2envzd2zz__expander_letz00;
	extern obj_t
		BGl_expandzd2evalzd2definezd2genericzd2envz00zz__expander_definez00;
	      DEFINE_STRING(BGl_string2311z00zz__install_expandersz00,
		BgL_bgl_string2311za700za7za7_2511za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2314z00zz__install_expandersz00,
		BgL_bgl_string2314za700za7za7_2512za7, "quote", 5);
	      DEFINE_STRING(BGl_string2317z00zz__install_expandersz00,
		BgL_bgl_string2317za700za7za7_2513za7, "@", 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_expandzd2andzd2letza2zd2envz70zz__install_expandersz00,
		BgL_bgl_za762expandza7d2andza72514za7,
		BGl_z62expandzd2andzd2letza2zc0zz__install_expandersz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2evalzd2casezd2envzd2zz__expander_casez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2515za7,
		BGl_z62zc3z04anonymousza31328ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2401z00zz__install_expandersz00,
		BgL_bgl_string2401za700za7za7_2516za7, "#meta", 5);
	      DEFINE_STRING(BGl_string2320z00zz__install_expandersz00,
		BgL_bgl_string2320za700za7za7_2517za7, "->", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2518za7,
		BGl_z62zc3z04anonymousza31330ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2404z00zz__install_expandersz00,
		BgL_bgl_string2404za700za7za7_2519za7, "bind-exit", 9);
	      DEFINE_STRING(BGl_string2323z00zz__install_expandersz00,
		BgL_bgl_string2323za700za7za7_2520za7, "quasiquote", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2521za7,
		BGl_z62zc3z04anonymousza31348ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2407z00zz__install_expandersz00,
		BgL_bgl_string2407za700za7za7_2522za7, "unwind-protect", 14);
	      DEFINE_STRING(BGl_string2326z00zz__install_expandersz00,
		BgL_bgl_string2326za700za7za7_2523za7, "module", 6);
	      DEFINE_STRING(BGl_string2329z00zz__install_expandersz00,
		BgL_bgl_string2329za700za7za7_2524za7, "define-macro", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2402z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2525za7,
		BGl_z62zc3z04anonymousza31495ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2526za7,
		BGl_z62zc3z04anonymousza31371ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2410z00zz__install_expandersz00,
		BgL_bgl_string2410za700za7za7_2527za7, "with-handler", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2405z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2528za7,
		BGl_z62zc3z04anonymousza31627ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2529za7,
		BGl_z62zc3z04anonymousza31374ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2413z00zz__install_expandersz00,
		BgL_bgl_string2413za700za7za7_2530za7, "multiple-value-bind", 19);
	      DEFINE_STRING(BGl_string2332z00zz__install_expandersz00,
		BgL_bgl_string2332za700za7za7_2531za7, "define-hygiene-macro", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2408z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2532za7,
		BGl_z62zc3z04anonymousza31646ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2533za7,
		BGl_z62zc3z04anonymousza31376ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	extern obj_t BGl_expandzd2lalrzd2grammarzd2envzd2zz__lalr_expandz00;
	   
		 
		DEFINE_STRING(BGl_string2415z00zz__install_expandersz00,
		BgL_bgl_string2415za700za7za7_2534za7, "let", 3);
	      DEFINE_STRING(BGl_string2335z00zz__install_expandersz00,
		BgL_bgl_string2335za700za7za7_2535za7, "define-expander", 15);
	      DEFINE_STRING(BGl_string2417z00zz__install_expandersz00,
		BgL_bgl_string2417za700za7za7_2536za7, "$let", 4);
	      DEFINE_STRING(BGl_string2419z00zz__install_expandersz00,
		BgL_bgl_string2419za700za7za7_2537za7, "let*", 4);
	      DEFINE_STRING(BGl_string2338z00zz__install_expandersz00,
		BgL_bgl_string2338za700za7za7_2538za7, "cond", 4);
	extern obj_t BGl_evalzd2cozd2instantiatezd2expanderzd2envz00zz__evobjectz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2411z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2539za7,
		BGl_z62zc3z04anonymousza31679ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2540za7,
		BGl_z62zc3z04anonymousza31378ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2500z00zz__install_expandersz00,
		BgL_bgl_string2500za700za7za7_2541za7, "at", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2542za7,
		BGl_z62zc3z04anonymousza31380ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2502z00zz__install_expandersz00,
		BgL_bgl_string2502za700za7za7_2543za7, "and", 3);
	      DEFINE_STRING(BGl_string2421z00zz__install_expandersz00,
		BgL_bgl_string2421za700za7za7_2544za7, "letrec", 6);
	      DEFINE_STRING(BGl_string2503z00zz__install_expandersz00,
		BgL_bgl_string2503za700za7za7_2545za7, "bindings must be a list", 23);
	      DEFINE_STRING(BGl_string2341z00zz__install_expandersz00,
		BgL_bgl_string2341za700za7za7_2546za7, "do", 2);
	      DEFINE_STRING(BGl_string2504z00zz__install_expandersz00,
		BgL_bgl_string2504za700za7za7_2547za7, "duplicate variable in the bindings",
		34);
	      DEFINE_STRING(BGl_string2423z00zz__install_expandersz00,
		BgL_bgl_string2423za700za7za7_2548za7, "letrec*", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2336z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2549za7,
		BGl_z62zc3z04anonymousza31383ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2505z00zz__install_expandersz00,
		BgL_bgl_string2505za700za7za7_2550za7, "ill-formed binding", 18);
	      DEFINE_STRING(BGl_string2506z00zz__install_expandersz00,
		BgL_bgl_string2506za700za7za7_2551za7, "Illegal `and-let*' form", 23);
	      DEFINE_STRING(BGl_string2425z00zz__install_expandersz00,
		BgL_bgl_string2425za700za7za7_2552za7, "labels", 6);
	      DEFINE_STRING(BGl_string2344z00zz__install_expandersz00,
		BgL_bgl_string2344za700za7za7_2553za7, "try", 3);
	      DEFINE_STRING(BGl_string2507z00zz__install_expandersz00,
		BgL_bgl_string2507za700za7za7_2554za7, "__install_expanders", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2555za7,
		BGl_z62zc3z04anonymousza31388ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2427z00zz__install_expandersz00,
		BgL_bgl_string2427za700za7za7_2556za7, "define", 6);
	      DEFINE_STRING(BGl_string2347z00zz__install_expandersz00,
		BgL_bgl_string2347za700za7za7_2557za7, "match-case", 10);
	      DEFINE_STRING(BGl_string2429z00zz__install_expandersz00,
		BgL_bgl_string2429za700za7za7_2558za7, "define-inline", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2559za7,
		BGl_z62zc3z04anonymousza31390ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2431z00zz__install_expandersz00,
		BgL_bgl_string2431za700za7za7_2560za7, "define-generic", 14);
	      DEFINE_STRING(BGl_string2350z00zz__install_expandersz00,
		BgL_bgl_string2350za700za7za7_2561za7, "match-lambda", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2562za7,
		BGl_z62zc3z04anonymousza31392ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	extern obj_t BGl_expandzd2definezd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00;
	   
		 
		DEFINE_STRING(BGl_string2433z00zz__install_expandersz00,
		BgL_bgl_string2433za700za7za7_2563za7, "define-method", 13);
	      DEFINE_STRING(BGl_string2353z00zz__install_expandersz00,
		BgL_bgl_string2353za700za7za7_2564za7, "define-pattern", 14);
	      DEFINE_STRING(BGl_string2435z00zz__install_expandersz00,
		BgL_bgl_string2435za700za7za7_2565za7, "define-struct", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2348z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2566za7,
		BGl_z62zc3z04anonymousza31395ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2437z00zz__install_expandersz00,
		BgL_bgl_string2437za700za7za7_2567za7, "case", 4);
	      DEFINE_STRING(BGl_string2356z00zz__install_expandersz00,
		BgL_bgl_string2356za700za7za7_2568za7, "delay", 5);
	      DEFINE_STRING(BGl_string2439z00zz__install_expandersz00,
		BgL_bgl_string2439za700za7za7_2569za7, "cond-expand", 11);
	      DEFINE_STRING(BGl_string2358z00zz__install_expandersz00,
		BgL_bgl_string2358za700za7za7_2570za7, "regular-grammar", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2571za7,
		BGl_z62zc3z04anonymousza31398ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2360z00zz__install_expandersz00,
		BgL_bgl_string2360za700za7za7_2572za7, "string-case", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2354z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2573za7,
		BGl_z62zc3z04anonymousza31401ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2442z00zz__install_expandersz00,
		BgL_bgl_string2442za700za7za7_2574za7, "profile", 7);
	      DEFINE_STRING(BGl_string2362z00zz__install_expandersz00,
		BgL_bgl_string2362za700za7za7_2575za7, "lalr-grammar", 12);
	      DEFINE_STRING(BGl_string2445z00zz__install_expandersz00,
		BgL_bgl_string2445za700za7za7_2576za7, "instantiate", 11);
	      DEFINE_STRING(BGl_string2364z00zz__install_expandersz00,
		BgL_bgl_string2364za700za7za7_2577za7, "begin", 5);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_expandzd2beginzd2envz00zz__install_expandersz00,
		BgL_bgl_za762expandza7d2begi2578z00,
		BGl_z62expandzd2beginzb0zz__install_expandersz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2448z00zz__install_expandersz00,
		BgL_bgl_string2448za700za7za7_2579za7, "with-access", 11);
	      DEFINE_STRING(BGl_string2367z00zz__install_expandersz00,
		BgL_bgl_string2367za700za7za7_2580za7, "failure", 7);
	extern obj_t BGl_expandzd2evalzd2condzd2expandzd2envz00zz__expander_srfi0z00;
	extern obj_t BGl_expandzd2letzd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2440z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2581za7,
		BGl_z62zc3z04anonymousza31764ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2443z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2582za7,
		BGl_z62zc3z04anonymousza31803ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2450z00zz__install_expandersz00,
		BgL_bgl_string2450za700za7za7_2583za7, "co-instantiate", 14);
	      DEFINE_STRING(BGl_string2370z00zz__install_expandersz00,
		BgL_bgl_string2370za700za7za7_2584za7, "receive", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2446z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2585za7,
		BGl_z62zc3z04anonymousza31807ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2365z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2586za7,
		BGl_z62zc3z04anonymousza31415ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2453z00zz__install_expandersz00,
		BgL_bgl_string2453za700za7za7_2587za7, "define-class", 12);
	      DEFINE_STRING(BGl_string2373z00zz__install_expandersz00,
		BgL_bgl_string2373za700za7za7_2588za7, "when", 4);
	      DEFINE_STRING(BGl_string2455z00zz__install_expandersz00,
		BgL_bgl_string2455za700za7za7_2589za7, "define-abstract-class", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2368z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2590za7,
		BGl_z62zc3z04anonymousza31435ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2457z00zz__install_expandersz00,
		BgL_bgl_string2457za700za7za7_2591za7, "define-final-class", 18);
	      DEFINE_STRING(BGl_string2376z00zz__install_expandersz00,
		BgL_bgl_string2376za700za7za7_2592za7, "unless", 6);
	      DEFINE_STRING(BGl_string2459z00zz__install_expandersz00,
		BgL_bgl_string2459za700za7za7_2593za7, "eval", 4);
	      DEFINE_STRING(BGl_string2378z00zz__install_expandersz00,
		BgL_bgl_string2378za700za7za7_2594za7, "define-record-type", 18);
	extern obj_t
		BGl_expandzd2evalzd2definezd2methodzd2envz00zz__expander_definez00;
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2451z00zz__install_expandersz00,
		BgL_bgl_za762eza762za7za7__insta2595z00,
		BGl_z62ez62zz__install_expandersz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2371z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2596za7,
		BGl_z62zc3z04anonymousza31447ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2460z00zz__install_expandersz00,
		BgL_bgl_string2460za700za7za7_2597za7, "Unknown class", 13);
	      DEFINE_STRING(BGl_string2380z00zz__install_expandersz00,
		BgL_bgl_string2380za700za7za7_2598za7, "args-parse", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2374z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2599za7,
		BGl_z62zc3z04anonymousza31459ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2462z00zz__install_expandersz00,
		BgL_bgl_string2462za700za7za7_2600za7, "value", 5);
	      DEFINE_STRING(BGl_string2382z00zz__install_expandersz00,
		BgL_bgl_string2382za700za7za7_2601za7, "tprint", 6);
	      DEFINE_STRING(BGl_string2464z00zz__install_expandersz00,
		BgL_bgl_string2464za700za7za7_2602za7, "GC-profile-push", 15);
	      DEFINE_STRING(BGl_string2384z00zz__install_expandersz00,
		BgL_bgl_string2384za700za7za7_2603za7, "and-let*", 8);
	      DEFINE_STRING(BGl_string2466z00zz__install_expandersz00,
		BgL_bgl_string2466za700za7za7_2604za7, "GC-profile-pop", 14);
	      DEFINE_STRING(BGl_string2467z00zz__install_expandersz00,
		BgL_bgl_string2467za700za7za7_2605za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2386z00zz__install_expandersz00,
		BgL_bgl_string2386za700za7za7_2606za7, "define-syntax", 13);
	      DEFINE_STRING(BGl_string2469z00zz__install_expandersz00,
		BgL_bgl_string2469za700za7za7_2607za7, "set!", 4);
	      DEFINE_STRING(BGl_string2388z00zz__install_expandersz00,
		BgL_bgl_string2388za700za7za7_2608za7, "letrec-syntax", 13);
	extern obj_t
		BGl_expandzd2evalzd2definezd2inlinezd2envz00zz__expander_definez00;
	extern obj_t BGl_expandzd2argszd2parsezd2envzd2zz__expander_argsz00;
	extern obj_t BGl_expandzd2evalzd2letrecza2zd2envz70zz__expander_letz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_expandzd2tprintzd2envz00zz__install_expandersz00,
		BgL_bgl_za762expandza7d2tpri2609z00,
		BGl_z62expandzd2tprintzb0zz__install_expandersz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2471z00zz__install_expandersz00,
		BgL_bgl_string2471za700za7za7_2610za7, "call-with-values", 16);
	      DEFINE_STRING(BGl_string2390z00zz__install_expandersz00,
		BgL_bgl_string2390za700za7za7_2611za7, "let-syntax", 10);
	      DEFINE_STRING(BGl_string2473z00zz__install_expandersz00,
		BgL_bgl_string2473za700za7za7_2612za7, "exn", 3);
	      DEFINE_STRING(BGl_string2392z00zz__install_expandersz00,
		BgL_bgl_string2392za700za7za7_2613za7, "compiler", 8);
	      DEFINE_STRING(BGl_string2475z00zz__install_expandersz00,
		BgL_bgl_string2475za700za7za7_2614za7, "sigsetmask", 10);
	      DEFINE_STRING(BGl_string2394z00zz__install_expandersz00,
		BgL_bgl_string2394za700za7za7_2615za7, "when-trace", 10);
	      DEFINE_STRING(BGl_string2477z00zz__install_expandersz00,
		BgL_bgl_string2477za700za7za7_2616za7, "exit", 4);
	      DEFINE_STRING(BGl_string2396z00zz__install_expandersz00,
		BgL_bgl_string2396za700za7za7_2617za7, "with-trace", 10);
	      DEFINE_STRING(BGl_string2479z00zz__install_expandersz00,
		BgL_bgl_string2479za700za7za7_2618za7, "flag", 4);
	      DEFINE_STRING(BGl_string2398z00zz__install_expandersz00,
		BgL_bgl_string2398za700za7za7_2619za7, "trace-item", 10);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_expandzd2ifzd2envz00zz__install_expandersz00,
		BgL_bgl_za762expandza7d2ifza7b2620za7,
		BGl_z62expandzd2ifzb0zz__install_expandersz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_expandzd2evalzd2letreczd2envzd2zz__expander_letz00;
	   
		 
		DEFINE_STRING(BGl_string2481z00zz__install_expandersz00,
		BgL_bgl_string2481za700za7za7_2621za7, "val", 3);
	      DEFINE_STRING(BGl_string2483z00zz__install_expandersz00,
		BgL_bgl_string2483za700za7za7_2622za7, "v", 1);
	      DEFINE_STRING(BGl_string2485z00zz__install_expandersz00,
		BgL_bgl_string2485za700za7za7_2623za7, "env", 3);
	      DEFINE_STRING(BGl_string2486z00zz__install_expandersz00,
		BgL_bgl_string2486za700za7za7_2624za7, "Illegal `failure' form", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2399z00zz__install_expandersz00,
		BgL_bgl_za762za7c3za704anonymo2625za7,
		BGl_z62zc3z04anonymousza31477ze3ze5zz__install_expandersz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2488z00zz__install_expandersz00,
		BgL_bgl_string2488za700za7za7_2626za7, "make-promise", 12);
	extern obj_t
		BGl_expandzd2evalzd2definezd2structzd2envz00zz__expander_structz00;
	extern obj_t BGl_expandzd2evalzd2lambdazd2envzd2zz__expander_definez00;
	extern obj_t BGl_expandzd2evalzd2letzd2envzd2zz__expander_letz00;
	   
		 
		DEFINE_STRING(BGl_string2490z00zz__install_expandersz00,
		BgL_bgl_string2490za700za7za7_2627za7, "not", 3);
	      DEFINE_STRING(BGl_string2493z00zz__install_expandersz00,
		BgL_bgl_string2493za700za7za7_2628za7, "__r4_output_6_10_3", 18);
	      DEFINE_STRING(BGl_string2494z00zz__install_expandersz00,
		BgL_bgl_string2494za700za7za7_2629za7, ":", 1);
	      DEFINE_STRING(BGl_string2495z00zz__install_expandersz00,
		BgL_bgl_string2495za700za7za7_2630za7, ",", 1);
	      DEFINE_STRING(BGl_string2498z00zz__install_expandersz00,
		BgL_bgl_string2498za700za7za7_2631za7, "current-error-port", 18);
	extern obj_t BGl_expandzd2letreczd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00;
	extern obj_t BGl_expandzd2stringzd2casezd2envzd2zz__rgc_expandz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2allzd2expandersz12zd2envzc0zz__install_expandersz00,
		BgL_bgl_za762installza7d2all2632z00,
		BGl_z62installzd2allzd2expandersz12z70zz__install_expandersz00, 0L, BUNSPEC,
		0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2480z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2482z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2487z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2489z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2492z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2497z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2499z00zz__install_expandersz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_list2491z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_list2496z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2304z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2306z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2308z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2310z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2313z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2316z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2319z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2400z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2403z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2322z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2406z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2325z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2409z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2328z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2412z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2331z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2414z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2334z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2416z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2418z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2337z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2501z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2420z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2340z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2422z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2424z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2343z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2426z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2346z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2428z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2349z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2430z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2432z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2352z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2434z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2436z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2355z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2438z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2357z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2359z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_keyword2484z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2441z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2361z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2444z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2363z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2447z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2366z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2449z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2369z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2452z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2372z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2454z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2456z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2375z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2458z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2377z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2379z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2461z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2381z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2463z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2383z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2465z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2385z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2468z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2387z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2389z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2470z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2472z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2391z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2474z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2393z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2476z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2395z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2478z00zz__install_expandersz00));
		     ADD_ROOT((void *) (&BGl_symbol2397z00zz__install_expandersz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__install_expandersz00(long
		BgL_checksumz00_4626, char *BgL_fromz00_4627)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__install_expandersz00))
				{
					BGl_requirezd2initializa7ationz75zz__install_expandersz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__install_expandersz00();
					BGl_cnstzd2initzd2zz__install_expandersz00();
					BGl_importedzd2moduleszd2initz00zz__install_expandersz00();
					return BGl_toplevelzd2initzd2zz__install_expandersz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			BGl_symbol2304z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2305z00zz__install_expandersz00);
			BGl_symbol2306z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2307z00zz__install_expandersz00);
			BGl_symbol2308z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2309z00zz__install_expandersz00);
			BGl_symbol2310z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2311z00zz__install_expandersz00);
			BGl_symbol2313z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2314z00zz__install_expandersz00);
			BGl_symbol2316z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2317z00zz__install_expandersz00);
			BGl_symbol2319z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2320z00zz__install_expandersz00);
			BGl_symbol2322z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2323z00zz__install_expandersz00);
			BGl_symbol2325z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2326z00zz__install_expandersz00);
			BGl_symbol2328z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2329z00zz__install_expandersz00);
			BGl_symbol2331z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2332z00zz__install_expandersz00);
			BGl_symbol2334z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2335z00zz__install_expandersz00);
			BGl_symbol2337z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2338z00zz__install_expandersz00);
			BGl_symbol2340z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2341z00zz__install_expandersz00);
			BGl_symbol2343z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2344z00zz__install_expandersz00);
			BGl_symbol2346z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2347z00zz__install_expandersz00);
			BGl_symbol2349z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2350z00zz__install_expandersz00);
			BGl_symbol2352z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2353z00zz__install_expandersz00);
			BGl_symbol2355z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2356z00zz__install_expandersz00);
			BGl_symbol2357z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2358z00zz__install_expandersz00);
			BGl_symbol2359z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2360z00zz__install_expandersz00);
			BGl_symbol2361z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2362z00zz__install_expandersz00);
			BGl_symbol2363z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2364z00zz__install_expandersz00);
			BGl_symbol2366z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2367z00zz__install_expandersz00);
			BGl_symbol2369z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2370z00zz__install_expandersz00);
			BGl_symbol2372z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2373z00zz__install_expandersz00);
			BGl_symbol2375z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2376z00zz__install_expandersz00);
			BGl_symbol2377z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2378z00zz__install_expandersz00);
			BGl_symbol2379z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2380z00zz__install_expandersz00);
			BGl_symbol2381z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2382z00zz__install_expandersz00);
			BGl_symbol2383z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2384z00zz__install_expandersz00);
			BGl_symbol2385z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2386z00zz__install_expandersz00);
			BGl_symbol2387z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2388z00zz__install_expandersz00);
			BGl_symbol2389z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2390z00zz__install_expandersz00);
			BGl_symbol2391z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2392z00zz__install_expandersz00);
			BGl_symbol2393z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2394z00zz__install_expandersz00);
			BGl_symbol2395z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2396z00zz__install_expandersz00);
			BGl_symbol2397z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2398z00zz__install_expandersz00);
			BGl_symbol2400z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2401z00zz__install_expandersz00);
			BGl_symbol2403z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2404z00zz__install_expandersz00);
			BGl_symbol2406z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2407z00zz__install_expandersz00);
			BGl_symbol2409z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2410z00zz__install_expandersz00);
			BGl_symbol2412z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2413z00zz__install_expandersz00);
			BGl_symbol2414z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2415z00zz__install_expandersz00);
			BGl_symbol2416z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2417z00zz__install_expandersz00);
			BGl_symbol2418z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2419z00zz__install_expandersz00);
			BGl_symbol2420z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2421z00zz__install_expandersz00);
			BGl_symbol2422z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2423z00zz__install_expandersz00);
			BGl_symbol2424z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2425z00zz__install_expandersz00);
			BGl_symbol2426z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2427z00zz__install_expandersz00);
			BGl_symbol2428z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2429z00zz__install_expandersz00);
			BGl_symbol2430z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2431z00zz__install_expandersz00);
			BGl_symbol2432z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2433z00zz__install_expandersz00);
			BGl_symbol2434z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2435z00zz__install_expandersz00);
			BGl_symbol2436z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2437z00zz__install_expandersz00);
			BGl_symbol2438z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2439z00zz__install_expandersz00);
			BGl_symbol2441z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2442z00zz__install_expandersz00);
			BGl_symbol2444z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2445z00zz__install_expandersz00);
			BGl_symbol2447z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2448z00zz__install_expandersz00);
			BGl_symbol2449z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2450z00zz__install_expandersz00);
			BGl_symbol2452z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2453z00zz__install_expandersz00);
			BGl_symbol2454z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2455z00zz__install_expandersz00);
			BGl_symbol2456z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2457z00zz__install_expandersz00);
			BGl_symbol2458z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2459z00zz__install_expandersz00);
			BGl_symbol2461z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2462z00zz__install_expandersz00);
			BGl_symbol2463z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2464z00zz__install_expandersz00);
			BGl_symbol2465z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2466z00zz__install_expandersz00);
			BGl_symbol2468z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2469z00zz__install_expandersz00);
			BGl_symbol2470z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2471z00zz__install_expandersz00);
			BGl_symbol2472z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2473z00zz__install_expandersz00);
			BGl_symbol2474z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2475z00zz__install_expandersz00);
			BGl_symbol2476z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2477z00zz__install_expandersz00);
			BGl_symbol2478z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2479z00zz__install_expandersz00);
			BGl_symbol2480z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2481z00zz__install_expandersz00);
			BGl_symbol2482z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2483z00zz__install_expandersz00);
			BGl_keyword2484z00zz__install_expandersz00 =
				bstring_to_keyword(BGl_string2485z00zz__install_expandersz00);
			BGl_symbol2487z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2488z00zz__install_expandersz00);
			BGl_symbol2489z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2490z00zz__install_expandersz00);
			BGl_symbol2492z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2493z00zz__install_expandersz00);
			BGl_list2491z00zz__install_expandersz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2316z00zz__install_expandersz00,
				MAKE_YOUNG_PAIR(BGl_symbol2381z00zz__install_expandersz00,
					MAKE_YOUNG_PAIR(BGl_symbol2492z00zz__install_expandersz00, BNIL)));
			BGl_symbol2497z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2498z00zz__install_expandersz00);
			BGl_list2496z00zz__install_expandersz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2497z00zz__install_expandersz00, BNIL);
			BGl_symbol2499z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2500z00zz__install_expandersz00);
			return (BGl_symbol2501z00zz__install_expandersz00 =
				bstring_to_symbol(BGl_string2502z00zz__install_expandersz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			BGl_za2expanderszd2installedzd2pza2z00zz__install_expandersz00 =
				((bool_t) 0);
			if (BGl_za2expanderszd2installedzd2pza2z00zz__install_expandersz00)
				{	/* Eval/expanders.scm 109 */
					return BFALSE;
				}
			else
				{	/* Eval/expanders.scm 109 */
					BGl_za2expanderszd2installedzd2pza2z00zz__install_expandersz00 =
						((bool_t) 1);
					BGL_TAIL return
						BGl_z52installzd2allzd2expandersz12z40zz__install_expandersz00();
				}
		}

	}



/* expand-test */
	obj_t BGl_expandzd2testzd2zz__install_expandersz00(obj_t BgL_xz00_3,
		obj_t BgL_ez00_4)
	{
		{	/* Eval/expanders.scm 80 */
			if (CBOOL(BGl_za2nilza2z00zz__evalz00))
				{	/* Eval/expanders.scm 81 */
					return BGL_PROCEDURE_CALL2(BgL_ez00_4, BgL_xz00_3, BgL_ez00_4);
				}
			else
				{	/* Eval/expanders.scm 83 */
					obj_t BgL_arg1248z00_1213;
					obj_t BgL_arg1249z00_1214;

					{	/* Eval/expanders.scm 83 */
						obj_t BgL_arg1252z00_1215;

						{	/* Eval/expanders.scm 83 */
							obj_t BgL_arg1268z00_1216;
							obj_t BgL_arg1272z00_1217;

							BgL_arg1268z00_1216 =
								MAKE_YOUNG_PAIR(BGl_symbol2304z00zz__install_expandersz00,
								BNIL);
							{	/* Eval/expanders.scm 85 */
								obj_t BgL_arg1284z00_1218;

								{	/* Eval/expanders.scm 85 */
									obj_t BgL_arg1304z00_1219;

									{	/* Eval/expanders.scm 85 */
										obj_t BgL_arg1305z00_1220;

										{	/* Eval/expanders.scm 85 */
											obj_t BgL_arg1306z00_1221;
											obj_t BgL_arg1307z00_1222;

											{	/* Eval/expanders.scm 85 */
												obj_t BgL_arg1308z00_1223;

												{	/* Eval/expanders.scm 85 */
													obj_t BgL_arg1309z00_1224;
													obj_t BgL_arg1310z00_1225;

													{	/* Eval/expanders.scm 85 */
														obj_t BgL_arg1311z00_1226;

														BgL_arg1311z00_1226 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2304z00zz__install_expandersz00, BNIL);
														BgL_arg1309z00_1224 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2306z00zz__install_expandersz00,
															BgL_arg1311z00_1226);
													}
													{	/* Eval/expanders.scm 85 */
														obj_t BgL_arg1312z00_1227;

														BgL_arg1312z00_1227 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
														BgL_arg1310z00_1225 =
															MAKE_YOUNG_PAIR(BFALSE, BgL_arg1312z00_1227);
													}
													BgL_arg1308z00_1223 =
														MAKE_YOUNG_PAIR(BgL_arg1309z00_1224,
														BgL_arg1310z00_1225);
												}
												BgL_arg1306z00_1221 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2308z00zz__install_expandersz00,
													BgL_arg1308z00_1223);
											}
											BgL_arg1307z00_1222 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											BgL_arg1305z00_1220 =
												MAKE_YOUNG_PAIR(BgL_arg1306z00_1221,
												BgL_arg1307z00_1222);
										}
										BgL_arg1304z00_1219 =
											MAKE_YOUNG_PAIR(BGl_symbol2304z00zz__install_expandersz00,
											BgL_arg1305z00_1220);
									}
									BgL_arg1284z00_1218 =
										MAKE_YOUNG_PAIR(BGl_symbol2308z00zz__install_expandersz00,
										BgL_arg1304z00_1219);
								}
								BgL_arg1272z00_1217 =
									MAKE_YOUNG_PAIR(BgL_arg1284z00_1218, BNIL);
							}
							BgL_arg1252z00_1215 =
								MAKE_YOUNG_PAIR(BgL_arg1268z00_1216, BgL_arg1272z00_1217);
						}
						BgL_arg1248z00_1213 =
							MAKE_YOUNG_PAIR(BGl_symbol2310z00zz__install_expandersz00,
							BgL_arg1252z00_1215);
					}
					{	/* Eval/expanders.scm 89 */
						obj_t BgL_arg1314z00_1228;

						BgL_arg1314z00_1228 =
							BGL_PROCEDURE_CALL2(BgL_ez00_4, BgL_xz00_3, BgL_ez00_4);
						BgL_arg1249z00_1214 = MAKE_YOUNG_PAIR(BgL_arg1314z00_1228, BNIL);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1248z00_1213, BgL_arg1249z00_1214);
				}
		}

	}



/* expand-error */
	obj_t BGl_expandzd2errorzd2zz__install_expandersz00(obj_t BgL_pz00_5,
		obj_t BgL_mz00_6, obj_t BgL_xz00_7)
	{
		{	/* Eval/expanders.scm 94 */
			{	/* Eval/expanders.scm 95 */
				obj_t BgL_locz00_1229;

				if (EPAIRP(BgL_xz00_7))
					{	/* Eval/expanders.scm 95 */
						BgL_locz00_1229 = CER(((obj_t) BgL_xz00_7));
					}
				else
					{	/* Eval/expanders.scm 95 */
						BgL_locz00_1229 = BFALSE;
					}
				{	/* Eval/expanders.scm 96 */
					bool_t BgL_test2637z00_4756;

					if (PAIRP(BgL_locz00_1229))
						{	/* Eval/expanders.scm 96 */
							bool_t BgL_test2639z00_4759;

							{	/* Eval/expanders.scm 96 */
								obj_t BgL_tmpz00_4760;

								BgL_tmpz00_4760 = CDR(BgL_locz00_1229);
								BgL_test2639z00_4759 = PAIRP(BgL_tmpz00_4760);
							}
							if (BgL_test2639z00_4759)
								{	/* Eval/expanders.scm 96 */
									obj_t BgL_tmpz00_4763;

									BgL_tmpz00_4763 = CDR(CDR(BgL_locz00_1229));
									BgL_test2637z00_4756 = PAIRP(BgL_tmpz00_4763);
								}
							else
								{	/* Eval/expanders.scm 96 */
									BgL_test2637z00_4756 = ((bool_t) 0);
								}
						}
					else
						{	/* Eval/expanders.scm 96 */
							BgL_test2637z00_4756 = ((bool_t) 0);
						}
					if (BgL_test2637z00_4756)
						{	/* Eval/expanders.scm 96 */
							return
								BGl_errorzf2locationzf2zz__errorz00(BgL_pz00_5, BgL_mz00_6,
								BgL_xz00_7, CAR(CDR(BgL_locz00_1229)),
								CAR(CDR(CDR(BgL_locz00_1229))));
						}
					else
						{	/* Eval/expanders.scm 96 */
							return
								BGl_errorz00zz__errorz00(BgL_pz00_5, BgL_mz00_6, BgL_xz00_7);
						}
				}
			}
		}

	}



/* install-all-expanders! */
	BGL_EXPORTED_DEF obj_t
		BGl_installzd2allzd2expandersz12z12zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 108 */
			if (BGl_za2expanderszd2installedzd2pza2z00zz__install_expandersz00)
				{	/* Eval/expanders.scm 109 */
					return BFALSE;
				}
			else
				{	/* Eval/expanders.scm 109 */
					BGl_za2expanderszd2installedzd2pza2z00zz__install_expandersz00 =
						((bool_t) 1);
					BGL_TAIL return
						BGl_z52installzd2allzd2expandersz12z40zz__install_expandersz00();
				}
		}

	}



/* &install-all-expanders! */
	obj_t BGl_z62installzd2allzd2expandersz12z70zz__install_expandersz00(obj_t
		BgL_envz00_3928)
	{
		{	/* Eval/expanders.scm 108 */
			return BGl_installzd2allzd2expandersz12z12zz__install_expandersz00();
		}

	}



/* %install-all-expanders! */
	obj_t BGl_z52installzd2allzd2expandersz12z40zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 122 */
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2313z00zz__install_expandersz00,
				BGl_proc2312z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2316z00zz__install_expandersz00,
				BGl_proc2315z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2319z00zz__install_expandersz00,
				BGl_proc2318z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2322z00zz__install_expandersz00,
				BGl_proc2321z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2325z00zz__install_expandersz00,
				BGl_proc2324z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2328z00zz__install_expandersz00,
				BGl_proc2327z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2331z00zz__install_expandersz00,
				BGl_proc2330z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2334z00zz__install_expandersz00,
				BGl_proc2333z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2337z00zz__install_expandersz00,
				BGl_proc2336z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2340z00zz__install_expandersz00,
				BGl_proc2339z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2343z00zz__install_expandersz00,
				BGl_proc2342z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2346z00zz__install_expandersz00,
				BGl_proc2345z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2349z00zz__install_expandersz00,
				BGl_proc2348z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2352z00zz__install_expandersz00,
				BGl_proc2351z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2355z00zz__install_expandersz00,
				BGl_proc2354z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2357z00zz__install_expandersz00,
				BGl_expandzd2regularzd2grammarzd2envzd2zz__rgc_expandz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2359z00zz__install_expandersz00,
				BGl_expandzd2stringzd2casezd2envzd2zz__rgc_expandz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2361z00zz__install_expandersz00,
				BGl_expandzd2lalrzd2grammarzd2envzd2zz__lalr_expandz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2363z00zz__install_expandersz00,
				BGl_expandzd2beginzd2envz00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2366z00zz__install_expandersz00,
				BGl_proc2365z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2369z00zz__install_expandersz00,
				BGl_proc2368z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2372z00zz__install_expandersz00,
				BGl_proc2371z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2375z00zz__install_expandersz00,
				BGl_proc2374z00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2377z00zz__install_expandersz00,
				BGl_expandzd2definezd2recordzd2typezd2envz00zz__expander_recordz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2379z00zz__install_expandersz00,
				BGl_expandzd2argszd2parsezd2envzd2zz__expander_argsz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2381z00zz__install_expandersz00,
				BGl_expandzd2tprintzd2envz00zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2383z00zz__install_expandersz00,
				BGl_expandzd2andzd2letza2zd2envz70zz__install_expandersz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2385z00zz__install_expandersz00,
				BGl_expandzd2definezd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2387z00zz__install_expandersz00,
				BGl_expandzd2letreczd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00);
			BGl_installzd2expanderzd2zz__macroz00
				(BGl_symbol2389z00zz__install_expandersz00,
				BGl_expandzd2letzd2syntaxzd2envzd2zz__r5_macro_4_3_syntaxz00);
			{	/* Eval/expanders.scm 276 */
				obj_t BgL_arg1472z00_1497;

				BgL_arg1472z00_1497 =
					BGl_makezd2expandzd2whenzd2tracezd2zz__expander_tracez00
					(BGl_symbol2391z00zz__install_expandersz00);
				BGl_installzd2compilerzd2expanderz00zz__macroz00
					(BGl_symbol2393z00zz__install_expandersz00, BgL_arg1472z00_1497);
			}
			{	/* Eval/expanders.scm 277 */
				obj_t BgL_arg1473z00_1498;

				BgL_arg1473z00_1498 =
					BGl_makezd2expandzd2withzd2tracezd2zz__expander_tracez00
					(BGl_symbol2391z00zz__install_expandersz00);
				BGl_installzd2compilerzd2expanderz00zz__macroz00
					(BGl_symbol2395z00zz__install_expandersz00, BgL_arg1473z00_1498);
			}
			{	/* Eval/expanders.scm 278 */
				obj_t BgL_arg1474z00_1499;

				BgL_arg1474z00_1499 =
					BGl_makezd2expandzd2tracezd2itemzd2zz__expander_tracez00
					(BGl_symbol2391z00zz__install_expandersz00);
				BGl_installzd2compilerzd2expanderz00zz__macroz00
					(BGl_symbol2397z00zz__install_expandersz00, BgL_arg1474z00_1499);
			}
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2400z00zz__install_expandersz00,
				BGl_proc2399z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2403z00zz__install_expandersz00,
				BGl_proc2402z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2406z00zz__install_expandersz00,
				BGl_proc2405z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2409z00zz__install_expandersz00,
				BGl_proc2408z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2412z00zz__install_expandersz00,
				BGl_proc2411z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2308z00zz__install_expandersz00,
				BGl_expandzd2ifzd2envz00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2310z00zz__install_expandersz00,
				BGl_expandzd2evalzd2lambdazd2envzd2zz__expander_definez00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2414z00zz__install_expandersz00,
				BGl_expandzd2evalzd2letzd2envzd2zz__expander_letz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2416z00zz__install_expandersz00,
				BGl_expandzd2evalzd2z42letzd2envz90zz__expander_letz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2418z00zz__install_expandersz00,
				BGl_expandzd2evalzd2letza2zd2envz70zz__expander_letz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2420z00zz__install_expandersz00,
				BGl_expandzd2evalzd2letreczd2envzd2zz__expander_letz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2422z00zz__install_expandersz00,
				BGl_expandzd2evalzd2letrecza2zd2envz70zz__expander_letz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2424z00zz__install_expandersz00,
				BGl_expandzd2evalzd2labelszd2envzd2zz__expander_letz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2426z00zz__install_expandersz00,
				BGl_expandzd2evalzd2definezd2envzd2zz__expander_definez00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2428z00zz__install_expandersz00,
				BGl_expandzd2evalzd2definezd2inlinezd2envz00zz__expander_definez00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2430z00zz__install_expandersz00,
				BGl_expandzd2evalzd2definezd2genericzd2envz00zz__expander_definez00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2432z00zz__install_expandersz00,
				BGl_expandzd2evalzd2definezd2methodzd2envz00zz__expander_definez00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2434z00zz__install_expandersz00,
				BGl_expandzd2evalzd2definezd2structzd2envz00zz__expander_structz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2436z00zz__install_expandersz00,
				BGl_expandzd2evalzd2casezd2envzd2zz__expander_casez00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2438z00zz__install_expandersz00,
				BGl_expandzd2evalzd2condzd2expandzd2envz00zz__expander_srfi0z00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2441z00zz__install_expandersz00,
				BGl_proc2440z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2444z00zz__install_expandersz00,
				BGl_proc2443z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2447z00zz__install_expandersz00,
				BGl_proc2446z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2449z00zz__install_expandersz00,
				BGl_evalzd2cozd2instantiatezd2expanderzd2envz00zz__evobjectz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2452z00zz__install_expandersz00,
				BGl_proc2451z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2454z00zz__install_expandersz00,
				BGl_proc2451z00zz__install_expandersz00);
			BGl_installzd2evalzd2expanderz00zz__macroz00
				(BGl_symbol2456z00zz__install_expandersz00,
				BGl_proc2451z00zz__install_expandersz00);
			{	/* Eval/expanders.scm 489 */
				obj_t BgL_arg1812z00_1968;

				BgL_arg1812z00_1968 =
					BGl_makezd2expandzd2whenzd2tracezd2zz__expander_tracez00
					(BGl_symbol2458z00zz__install_expandersz00);
				BGl_installzd2evalzd2expanderz00zz__macroz00
					(BGl_symbol2393z00zz__install_expandersz00, BgL_arg1812z00_1968);
			}
			{	/* Eval/expanders.scm 490 */
				obj_t BgL_arg1813z00_1969;

				BgL_arg1813z00_1969 =
					BGl_makezd2expandzd2withzd2tracezd2zz__expander_tracez00
					(BGl_symbol2458z00zz__install_expandersz00);
				BGl_installzd2evalzd2expanderz00zz__macroz00
					(BGl_symbol2395z00zz__install_expandersz00, BgL_arg1813z00_1969);
			}
			{	/* Eval/expanders.scm 491 */
				obj_t BgL_arg1814z00_1970;

				BgL_arg1814z00_1970 =
					BGl_makezd2expandzd2tracezd2itemzd2zz__expander_tracez00
					(BGl_symbol2458z00zz__install_expandersz00);
				return
					BGl_installzd2evalzd2expanderz00zz__macroz00
					(BGl_symbol2397z00zz__install_expandersz00, BgL_arg1814z00_1970);
			}
		}

	}



/* &e */
	obj_t BGl_z62ez62zz__install_expandersz00(obj_t BgL_envz00_3957,
		obj_t BgL_xz00_3958, obj_t BgL_ez00_3959)
	{
		{	/* Eval/expanders.scm 483 */
			{	/* Eval/expanders.scm 483 */
				obj_t BgL_arg1811z00_4204;

				BgL_arg1811z00_4204 =
					BGl_evmodulezd2staticzd2classz00zz__evmodulez00(BgL_xz00_3958);
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_3959, BgL_arg1811z00_4204,
					BgL_ez00_3959);
			}
		}

	}



/* &<@anonymous:1807> */
	obj_t BGl_z62zc3z04anonymousza31807ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3960, obj_t BgL_xz00_3961, obj_t BgL_ez00_3962)
	{
		{	/* Eval/expanders.scm 474 */
			if (PAIRP(BgL_xz00_3961))
				{	/* Eval/expanders.scm 474 */
					obj_t BgL_arg1809z00_4205;

					BgL_arg1809z00_4205 = CAR(((obj_t) BgL_xz00_3961));
					return
						BGl_expandzd2errorzd2zz__install_expandersz00(BgL_arg1809z00_4205,
						BGl_string2460z00zz__install_expandersz00, BgL_xz00_3961);
				}
			else
				{	/* Eval/expanders.scm 474 */
					return BFALSE;
				}
		}

	}



/* &<@anonymous:1803> */
	obj_t BGl_z62zc3z04anonymousza31803ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3963, obj_t BgL_xz00_3964, obj_t BgL_ez00_3965)
	{
		{	/* Eval/expanders.scm 467 */
			if (PAIRP(BgL_xz00_3964))
				{	/* Eval/expanders.scm 467 */
					obj_t BgL_arg1805z00_4206;

					BgL_arg1805z00_4206 = CAR(((obj_t) BgL_xz00_3964));
					return
						BGl_expandzd2errorzd2zz__install_expandersz00(BgL_arg1805z00_4206,
						BGl_string2460z00zz__install_expandersz00, BgL_xz00_3964);
				}
			else
				{	/* Eval/expanders.scm 467 */
					return BFALSE;
				}
		}

	}



/* &<@anonymous:1764> */
	obj_t BGl_z62zc3z04anonymousza31764ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3966, obj_t BgL_xz00_3967, obj_t BgL_ez00_3968)
	{
		{	/* Eval/expanders.scm 438 */
			{
				obj_t BgL_lblz00_4208;
				obj_t BgL_exprsz00_4209;

				if (PAIRP(BgL_xz00_3967))
					{	/* Eval/expanders.scm 438 */
						obj_t BgL_cdrzd2533zd2_4244;

						BgL_cdrzd2533zd2_4244 = CDR(((obj_t) BgL_xz00_3967));
						if (PAIRP(BgL_cdrzd2533zd2_4244))
							{	/* Eval/expanders.scm 438 */
								obj_t BgL_carzd2536zd2_4245;

								BgL_carzd2536zd2_4245 = CAR(BgL_cdrzd2533zd2_4244);
								if (SYMBOLP(BgL_carzd2536zd2_4245))
									{	/* Eval/expanders.scm 438 */
										BgL_lblz00_4208 = BgL_carzd2536zd2_4245;
										BgL_exprsz00_4209 = CDR(BgL_cdrzd2533zd2_4244);
										{	/* Eval/expanders.scm 441 */
											obj_t BgL_laz00_4210;

											{	/* Eval/expanders.scm 441 */
												obj_t BgL_arg1800z00_4211;

												BgL_arg1800z00_4211 =
													MAKE_YOUNG_PAIR(BNIL,
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_exprsz00_4209, BNIL));
												BgL_laz00_4210 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2310z00zz__install_expandersz00,
													BgL_arg1800z00_4211);
											}
											{	/* Eval/expanders.scm 441 */
												obj_t BgL_lamz00_4212;

												if (EPAIRP(BgL_xz00_3967))
													{	/* Eval/expanders.scm 443 */
														obj_t BgL_arg1797z00_4213;
														obj_t BgL_arg1798z00_4214;
														obj_t BgL_arg1799z00_4215;

														BgL_arg1797z00_4213 = CAR(BgL_laz00_4210);
														BgL_arg1798z00_4214 = CDR(BgL_laz00_4210);
														BgL_arg1799z00_4215 = CER(((obj_t) BgL_xz00_3967));
														{	/* Eval/expanders.scm 443 */
															obj_t BgL_res2256z00_4216;

															BgL_res2256z00_4216 =
																MAKE_YOUNG_EPAIR(BgL_arg1797z00_4213,
																BgL_arg1798z00_4214, BgL_arg1799z00_4215);
															BgL_lamz00_4212 = BgL_res2256z00_4216;
														}
													}
												else
													{	/* Eval/expanders.scm 442 */
														BgL_lamz00_4212 = BgL_laz00_4210;
													}
												{	/* Eval/expanders.scm 442 */
													obj_t BgL_valz00_4217;

													BgL_valz00_4217 =
														BGl_gensymz00zz__r4_symbols_6_4z00
														(BGl_symbol2461z00zz__install_expandersz00);
													{	/* Eval/expanders.scm 447 */
														obj_t BgL_auxz00_4218;

														{	/* Eval/expanders.scm 449 */
															obj_t BgL_arg1773z00_4219;

															{	/* Eval/expanders.scm 449 */
																obj_t BgL_arg1774z00_4220;
																obj_t BgL_arg1775z00_4221;

																{	/* Eval/expanders.scm 449 */
																	obj_t BgL_arg1777z00_4222;

																	{	/* Eval/expanders.scm 449 */
																		obj_t BgL_arg1779z00_4223;

																		BgL_arg1779z00_4223 =
																			MAKE_YOUNG_PAIR(BgL_lamz00_4212, BNIL);
																		BgL_arg1777z00_4222 =
																			MAKE_YOUNG_PAIR(BgL_lblz00_4208,
																			BgL_arg1779z00_4223);
																	}
																	BgL_arg1774z00_4220 =
																		MAKE_YOUNG_PAIR(BgL_arg1777z00_4222, BNIL);
																}
																{	/* Eval/expanders.scm 451 */
																	obj_t BgL_arg1781z00_4224;
																	obj_t BgL_arg1782z00_4225;

																	{	/* Eval/expanders.scm 451 */
																		obj_t BgL_arg1783z00_4226;

																		{	/* Eval/expanders.scm 451 */
																			obj_t BgL_arg1785z00_4227;
																			obj_t BgL_arg1786z00_4228;

																			{	/* Eval/expanders.scm 451 */
																				obj_t BgL_arg2077z00_4229;

																				BgL_arg2077z00_4229 =
																					SYMBOL_TO_STRING(BgL_lblz00_4208);
																				BgL_arg1785z00_4227 =
																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																					(BgL_arg2077z00_4229);
																			}
																			BgL_arg1786z00_4228 =
																				MAKE_YOUNG_PAIR(BgL_lblz00_4208, BNIL);
																			BgL_arg1783z00_4226 =
																				MAKE_YOUNG_PAIR(BgL_arg1785z00_4227,
																				BgL_arg1786z00_4228);
																		}
																		BgL_arg1781z00_4224 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2463z00zz__install_expandersz00,
																			BgL_arg1783z00_4226);
																	}
																	{	/* Eval/expanders.scm 453 */
																		obj_t BgL_arg1787z00_4230;

																		{	/* Eval/expanders.scm 453 */
																			obj_t BgL_arg1788z00_4231;

																			{	/* Eval/expanders.scm 453 */
																				obj_t BgL_arg1789z00_4232;
																				obj_t BgL_arg1790z00_4233;

																				{	/* Eval/expanders.scm 453 */
																					obj_t BgL_arg1791z00_4234;

																					{	/* Eval/expanders.scm 453 */
																						obj_t BgL_arg1792z00_4235;

																						{	/* Eval/expanders.scm 453 */
																							obj_t BgL_arg1793z00_4236;

																							BgL_arg1793z00_4236 =
																								MAKE_YOUNG_PAIR(BgL_lblz00_4208,
																								BNIL);
																							BgL_arg1792z00_4235 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1793z00_4236, BNIL);
																						}
																						BgL_arg1791z00_4234 =
																							MAKE_YOUNG_PAIR(BgL_valz00_4217,
																							BgL_arg1792z00_4235);
																					}
																					BgL_arg1789z00_4232 =
																						MAKE_YOUNG_PAIR(BgL_arg1791z00_4234,
																						BNIL);
																				}
																				{	/* Eval/expanders.scm 454 */
																					obj_t BgL_arg1794z00_4237;
																					obj_t BgL_arg1795z00_4238;

																					BgL_arg1794z00_4237 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2465z00zz__install_expandersz00,
																						BNIL);
																					BgL_arg1795z00_4238 =
																						MAKE_YOUNG_PAIR(BgL_valz00_4217,
																						BNIL);
																					BgL_arg1790z00_4233 =
																						MAKE_YOUNG_PAIR(BgL_arg1794z00_4237,
																						BgL_arg1795z00_4238);
																				}
																				BgL_arg1788z00_4231 =
																					MAKE_YOUNG_PAIR(BgL_arg1789z00_4232,
																					BgL_arg1790z00_4233);
																			}
																			BgL_arg1787z00_4230 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2414z00zz__install_expandersz00,
																				BgL_arg1788z00_4231);
																		}
																		BgL_arg1782z00_4225 =
																			MAKE_YOUNG_PAIR(BgL_arg1787z00_4230,
																			BNIL);
																	}
																	BgL_arg1775z00_4221 =
																		MAKE_YOUNG_PAIR(BgL_arg1781z00_4224,
																		BgL_arg1782z00_4225);
																}
																BgL_arg1773z00_4219 =
																	MAKE_YOUNG_PAIR(BgL_arg1774z00_4220,
																	BgL_arg1775z00_4221);
															}
															BgL_auxz00_4218 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2414z00zz__install_expandersz00,
																BgL_arg1773z00_4219);
														}
														{	/* Eval/expanders.scm 449 */
															obj_t BgL_resz00_4239;

															if (EPAIRP(BgL_xz00_3967))
																{	/* Eval/expanders.scm 457 */
																	obj_t BgL_arg1770z00_4240;
																	obj_t BgL_arg1771z00_4241;
																	obj_t BgL_arg1772z00_4242;

																	BgL_arg1770z00_4240 = CAR(BgL_auxz00_4218);
																	BgL_arg1771z00_4241 = CDR(BgL_auxz00_4218);
																	BgL_arg1772z00_4242 =
																		CER(((obj_t) BgL_xz00_3967));
																	{	/* Eval/expanders.scm 457 */
																		obj_t BgL_res2257z00_4243;

																		BgL_res2257z00_4243 =
																			MAKE_YOUNG_EPAIR(BgL_arg1770z00_4240,
																			BgL_arg1771z00_4241, BgL_arg1772z00_4242);
																		BgL_resz00_4239 = BgL_res2257z00_4243;
																	}
																}
															else
																{	/* Eval/expanders.scm 456 */
																	BgL_resz00_4239 = BgL_auxz00_4218;
																}
															{	/* Eval/expanders.scm 456 */

																return
																	BGL_PROCEDURE_CALL2(BgL_ez00_3968,
																	BgL_auxz00_4218, BgL_ez00_3968);
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Eval/expanders.scm 438 */
										return
											BGl_expandzd2errorzd2zz__install_expandersz00
											(BGl_string2442z00zz__install_expandersz00,
											BGl_string2467z00zz__install_expandersz00, BgL_xz00_3967);
									}
							}
						else
							{	/* Eval/expanders.scm 438 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2442z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3967);
							}
					}
				else
					{	/* Eval/expanders.scm 438 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2442z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3967);
					}
			}
		}

	}



/* &<@anonymous:1679> */
	obj_t BGl_z62zc3z04anonymousza31679ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3969, obj_t BgL_xz00_3970, obj_t BgL_ez00_3971)
	{
		{	/* Eval/expanders.scm 375 */
			{
				obj_t BgL_zf3zd2z21_4247;
				obj_t BgL_varsz00_4248;
				obj_t BgL_producerz00_4249;
				obj_t BgL_exprsz00_4250;

				if (PAIRP(BgL_xz00_3970))
					{	/* Eval/expanders.scm 375 */
						obj_t BgL_cdrzd2502zd2_4331;

						BgL_cdrzd2502zd2_4331 = CDR(((obj_t) BgL_xz00_3970));
						if (PAIRP(BgL_cdrzd2502zd2_4331))
							{	/* Eval/expanders.scm 375 */
								obj_t BgL_carzd2507zd2_4332;
								obj_t BgL_cdrzd2508zd2_4333;

								BgL_carzd2507zd2_4332 = CAR(BgL_cdrzd2502zd2_4331);
								BgL_cdrzd2508zd2_4333 = CDR(BgL_cdrzd2502zd2_4331);
								{	/* Eval/expanders.scm 375 */
									bool_t BgL_test2650z00_4927;

									{
										obj_t BgL_l1134z00_4335;

										BgL_l1134z00_4335 = BgL_carzd2507zd2_4332;
									BgL_zc3z04anonymousza31690ze3z87_4334:
										if (NULLP(BgL_l1134z00_4335))
											{	/* Eval/expanders.scm 375 */
												BgL_test2650z00_4927 = ((bool_t) 1);
											}
										else
											{	/* Eval/expanders.scm 375 */
												bool_t BgL_test2652z00_4930;

												{	/* Eval/expanders.scm 375 */
													obj_t BgL_tmpz00_4931;

													BgL_tmpz00_4931 = CAR(((obj_t) BgL_l1134z00_4335));
													BgL_test2652z00_4930 = SYMBOLP(BgL_tmpz00_4931);
												}
												if (BgL_test2652z00_4930)
													{
														obj_t BgL_l1134z00_4935;

														BgL_l1134z00_4935 =
															CDR(((obj_t) BgL_l1134z00_4335));
														BgL_l1134z00_4335 = BgL_l1134z00_4935;
														goto BgL_zc3z04anonymousza31690ze3z87_4334;
													}
												else
													{	/* Eval/expanders.scm 375 */
														BgL_test2650z00_4927 = ((bool_t) 0);
													}
											}
									}
									if (BgL_test2650z00_4927)
										{	/* Eval/expanders.scm 375 */
											if (PAIRP(BgL_cdrzd2508zd2_4333))
												{	/* Eval/expanders.scm 375 */
													BgL_zf3zd2z21_4247 = BgL_carzd2507zd2_4332;
													BgL_varsz00_4248 = BgL_carzd2507zd2_4332;
													BgL_producerz00_4249 = CAR(BgL_cdrzd2508zd2_4333);
													BgL_exprsz00_4250 = CDR(BgL_cdrzd2508zd2_4333);
													{	/* Eval/expanders.scm 379 */
														obj_t BgL_tmpsz00_4251;

														if (NULLP(BgL_varsz00_4248))
															{	/* Eval/expanders.scm 379 */
																BgL_tmpsz00_4251 = BNIL;
															}
														else
															{	/* Eval/expanders.scm 379 */
																obj_t BgL_head1107z00_4252;

																{	/* Eval/expanders.scm 379 */
																	obj_t BgL_arg1761z00_4253;

																	{	/* Eval/expanders.scm 379 */
																		obj_t BgL_arg1762z00_4254;

																		BgL_arg1762z00_4254 =
																			CAR(((obj_t) BgL_varsz00_4248));
																		BgL_arg1761z00_4253 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BgL_arg1762z00_4254);
																	}
																	BgL_head1107z00_4252 =
																		MAKE_YOUNG_PAIR(BgL_arg1761z00_4253, BNIL);
																}
																{	/* Eval/expanders.scm 379 */
																	obj_t BgL_g1110z00_4255;

																	BgL_g1110z00_4255 =
																		CDR(((obj_t) BgL_varsz00_4248));
																	{
																		obj_t BgL_l1105z00_4257;
																		obj_t BgL_tail1108z00_4258;

																		BgL_l1105z00_4257 = BgL_g1110z00_4255;
																		BgL_tail1108z00_4258 = BgL_head1107z00_4252;
																	BgL_zc3z04anonymousza31756ze3z87_4256:
																		if (NULLP(BgL_l1105z00_4257))
																			{	/* Eval/expanders.scm 379 */
																				BgL_tmpsz00_4251 = BgL_head1107z00_4252;
																			}
																		else
																			{	/* Eval/expanders.scm 379 */
																				obj_t BgL_newtail1109z00_4259;

																				{	/* Eval/expanders.scm 379 */
																					obj_t BgL_arg1759z00_4260;

																					{	/* Eval/expanders.scm 379 */
																						obj_t BgL_arg1760z00_4261;

																						BgL_arg1760z00_4261 =
																							CAR(((obj_t) BgL_l1105z00_4257));
																						BgL_arg1759z00_4260 =
																							BGl_gensymz00zz__r4_symbols_6_4z00
																							(BgL_arg1760z00_4261);
																					}
																					BgL_newtail1109z00_4259 =
																						MAKE_YOUNG_PAIR(BgL_arg1759z00_4260,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1108z00_4258,
																					BgL_newtail1109z00_4259);
																				{	/* Eval/expanders.scm 379 */
																					obj_t BgL_arg1758z00_4262;

																					BgL_arg1758z00_4262 =
																						CDR(((obj_t) BgL_l1105z00_4257));
																					{
																						obj_t BgL_tail1108z00_4958;
																						obj_t BgL_l1105z00_4957;

																						BgL_l1105z00_4957 =
																							BgL_arg1758z00_4262;
																						BgL_tail1108z00_4958 =
																							BgL_newtail1109z00_4259;
																						BgL_tail1108z00_4258 =
																							BgL_tail1108z00_4958;
																						BgL_l1105z00_4257 =
																							BgL_l1105z00_4957;
																						goto
																							BgL_zc3z04anonymousza31756ze3z87_4256;
																					}
																				}
																			}
																	}
																}
															}
														{	/* Eval/expanders.scm 379 */
															obj_t BgL_tmps2z00_4263;

															if (NULLP(BgL_varsz00_4248))
																{	/* Eval/expanders.scm 380 */
																	BgL_tmps2z00_4263 = BNIL;
																}
															else
																{	/* Eval/expanders.scm 380 */
																	obj_t BgL_head1113z00_4264;

																	{	/* Eval/expanders.scm 380 */
																		obj_t BgL_arg1753z00_4265;

																		{	/* Eval/expanders.scm 380 */
																			obj_t BgL_arg1754z00_4266;

																			BgL_arg1754z00_4266 =
																				CAR(((obj_t) BgL_varsz00_4248));
																			BgL_arg1753z00_4265 =
																				BGl_gensymz00zz__r4_symbols_6_4z00
																				(BgL_arg1754z00_4266);
																		}
																		BgL_head1113z00_4264 =
																			MAKE_YOUNG_PAIR(BgL_arg1753z00_4265,
																			BNIL);
																	}
																	{	/* Eval/expanders.scm 380 */
																		obj_t BgL_g1116z00_4267;

																		BgL_g1116z00_4267 =
																			CDR(((obj_t) BgL_varsz00_4248));
																		{
																			obj_t BgL_l1111z00_4269;
																			obj_t BgL_tail1114z00_4270;

																			BgL_l1111z00_4269 = BgL_g1116z00_4267;
																			BgL_tail1114z00_4270 =
																				BgL_head1113z00_4264;
																		BgL_zc3z04anonymousza31748ze3z87_4268:
																			if (NULLP(BgL_l1111z00_4269))
																				{	/* Eval/expanders.scm 380 */
																					BgL_tmps2z00_4263 =
																						BgL_head1113z00_4264;
																				}
																			else
																				{	/* Eval/expanders.scm 380 */
																					obj_t BgL_newtail1115z00_4271;

																					{	/* Eval/expanders.scm 380 */
																						obj_t BgL_arg1751z00_4272;

																						{	/* Eval/expanders.scm 380 */
																							obj_t BgL_arg1752z00_4273;

																							BgL_arg1752z00_4273 =
																								CAR(
																								((obj_t) BgL_l1111z00_4269));
																							BgL_arg1751z00_4272 =
																								BGl_gensymz00zz__r4_symbols_6_4z00
																								(BgL_arg1752z00_4273);
																						}
																						BgL_newtail1115z00_4271 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1751z00_4272, BNIL);
																					}
																					SET_CDR(BgL_tail1114z00_4270,
																						BgL_newtail1115z00_4271);
																					{	/* Eval/expanders.scm 380 */
																						obj_t BgL_arg1750z00_4274;

																						BgL_arg1750z00_4274 =
																							CDR(((obj_t) BgL_l1111z00_4269));
																						{
																							obj_t BgL_tail1114z00_4977;
																							obj_t BgL_l1111z00_4976;

																							BgL_l1111z00_4976 =
																								BgL_arg1750z00_4274;
																							BgL_tail1114z00_4977 =
																								BgL_newtail1115z00_4271;
																							BgL_tail1114z00_4270 =
																								BgL_tail1114z00_4977;
																							BgL_l1111z00_4269 =
																								BgL_l1111z00_4976;
																							goto
																								BgL_zc3z04anonymousza31748ze3z87_4268;
																						}
																					}
																				}
																		}
																	}
																}
															{	/* Eval/expanders.scm 380 */
																obj_t BgL_nxz00_4275;

																{	/* Eval/expanders.scm 381 */
																	obj_t BgL_arg1700z00_4276;

																	{	/* Eval/expanders.scm 381 */
																		obj_t BgL_arg1701z00_4277;
																		obj_t BgL_arg1702z00_4278;

																		{	/* Eval/expanders.scm 381 */
																			obj_t BgL_arg1703z00_4279;

																			if (NULLP(BgL_tmpsz00_4251))
																				{	/* Eval/expanders.scm 381 */
																					BgL_arg1703z00_4279 = BNIL;
																				}
																			else
																				{	/* Eval/expanders.scm 381 */
																					obj_t BgL_head1119z00_4280;

																					BgL_head1119z00_4280 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1117z00_4282;
																						obj_t BgL_tail1120z00_4283;

																						BgL_l1117z00_4282 =
																							BgL_tmpsz00_4251;
																						BgL_tail1120z00_4283 =
																							BgL_head1119z00_4280;
																					BgL_zc3z04anonymousza31705ze3z87_4281:
																						if (NULLP
																							(BgL_l1117z00_4282))
																							{	/* Eval/expanders.scm 381 */
																								BgL_arg1703z00_4279 =
																									CDR(BgL_head1119z00_4280);
																							}
																						else
																							{	/* Eval/expanders.scm 381 */
																								obj_t BgL_newtail1121z00_4284;

																								{	/* Eval/expanders.scm 381 */
																									obj_t BgL_arg1708z00_4285;

																									{	/* Eval/expanders.scm 381 */
																										obj_t BgL_vz00_4286;

																										BgL_vz00_4286 =
																											CAR(
																											((obj_t)
																												BgL_l1117z00_4282));
																										{	/* Eval/expanders.scm 381 */
																											obj_t BgL_arg1709z00_4287;

																											BgL_arg1709z00_4287 =
																												MAKE_YOUNG_PAIR(BUNSPEC,
																												BNIL);
																											BgL_arg1708z00_4285 =
																												MAKE_YOUNG_PAIR
																												(BgL_vz00_4286,
																												BgL_arg1709z00_4287);
																										}
																									}
																									BgL_newtail1121z00_4284 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1708z00_4285, BNIL);
																								}
																								SET_CDR(BgL_tail1120z00_4283,
																									BgL_newtail1121z00_4284);
																								{	/* Eval/expanders.scm 381 */
																									obj_t BgL_arg1707z00_4288;

																									BgL_arg1707z00_4288 =
																										CDR(
																										((obj_t)
																											BgL_l1117z00_4282));
																									{
																										obj_t BgL_tail1120z00_4993;
																										obj_t BgL_l1117z00_4992;

																										BgL_l1117z00_4992 =
																											BgL_arg1707z00_4288;
																										BgL_tail1120z00_4993 =
																											BgL_newtail1121z00_4284;
																										BgL_tail1120z00_4283 =
																											BgL_tail1120z00_4993;
																										BgL_l1117z00_4282 =
																											BgL_l1117z00_4992;
																										goto
																											BgL_zc3z04anonymousza31705ze3z87_4281;
																									}
																								}
																							}
																					}
																				}
																			BgL_arg1701z00_4277 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1703z00_4279, BNIL);
																		}
																		{	/* Eval/expanders.scm 383 */
																			obj_t BgL_arg1710z00_4289;
																			obj_t BgL_arg1711z00_4290;

																			{	/* Eval/expanders.scm 383 */
																				obj_t BgL_arg1714z00_4291;

																				{	/* Eval/expanders.scm 383 */
																					obj_t BgL_arg1715z00_4292;
																					obj_t BgL_arg1717z00_4293;

																					{	/* Eval/expanders.scm 383 */
																						obj_t BgL_arg1718z00_4294;

																						{	/* Eval/expanders.scm 383 */
																							obj_t BgL_arg1720z00_4295;

																							BgL_arg1720z00_4295 =
																								MAKE_YOUNG_PAIR
																								(BgL_producerz00_4249, BNIL);
																							BgL_arg1718z00_4294 =
																								MAKE_YOUNG_PAIR(BNIL,
																								BgL_arg1720z00_4295);
																						}
																						BgL_arg1715z00_4292 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2310z00zz__install_expandersz00,
																							BgL_arg1718z00_4294);
																					}
																					{	/* Eval/expanders.scm 385 */
																						obj_t BgL_arg1722z00_4296;

																						{	/* Eval/expanders.scm 385 */
																							obj_t BgL_arg1723z00_4297;

																							{	/* Eval/expanders.scm 385 */
																								obj_t BgL_arg1724z00_4298;

																								{	/* Eval/expanders.scm 385 */
																									obj_t BgL_arg1725z00_4299;

																									if (NULLP(BgL_tmpsz00_4251))
																										{	/* Eval/expanders.scm 385 */
																											BgL_arg1725z00_4299 =
																												BNIL;
																										}
																									else
																										{	/* Eval/expanders.scm 385 */
																											obj_t
																												BgL_head1124z00_4300;
																											BgL_head1124z00_4300 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t
																													BgL_ll1122z00_4302;
																												obj_t
																													BgL_ll1123z00_4303;
																												obj_t
																													BgL_tail1125z00_4304;
																												BgL_ll1122z00_4302 =
																													BgL_tmpsz00_4251;
																												BgL_ll1123z00_4303 =
																													BgL_tmps2z00_4263;
																												BgL_tail1125z00_4304 =
																													BgL_head1124z00_4300;
																											BgL_zc3z04anonymousza31727ze3z87_4301:
																												if (NULLP
																													(BgL_ll1122z00_4302))
																													{	/* Eval/expanders.scm 385 */
																														BgL_arg1725z00_4299
																															=
																															CDR
																															(BgL_head1124z00_4300);
																													}
																												else
																													{	/* Eval/expanders.scm 385 */
																														obj_t
																															BgL_newtail1126z00_4305;
																														{	/* Eval/expanders.scm 385 */
																															obj_t
																																BgL_arg1731z00_4306;
																															{	/* Eval/expanders.scm 385 */
																																obj_t
																																	BgL_vz00_4307;
																																obj_t
																																	BgL_tz00_4308;
																																BgL_vz00_4307 =
																																	CAR(((obj_t)
																																		BgL_ll1122z00_4302));
																																BgL_tz00_4308 =
																																	CAR(((obj_t)
																																		BgL_ll1123z00_4303));
																																{	/* Eval/expanders.scm 385 */
																																	obj_t
																																		BgL_arg1733z00_4309;
																																	{	/* Eval/expanders.scm 385 */
																																		obj_t
																																			BgL_arg1734z00_4310;
																																		BgL_arg1734z00_4310
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_tz00_4308,
																																			BNIL);
																																		BgL_arg1733z00_4309
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_vz00_4307,
																																			BgL_arg1734z00_4310);
																																	}
																																	BgL_arg1731z00_4306
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2468z00zz__install_expandersz00,
																																		BgL_arg1733z00_4309);
																																}
																															}
																															BgL_newtail1126z00_4305
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1731z00_4306,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1125z00_4304,
																															BgL_newtail1126z00_4305);
																														{	/* Eval/expanders.scm 385 */
																															obj_t
																																BgL_arg1729z00_4311;
																															obj_t
																																BgL_arg1730z00_4312;
																															BgL_arg1729z00_4311
																																=
																																CDR(((obj_t)
																																	BgL_ll1122z00_4302));
																															BgL_arg1730z00_4312
																																=
																																CDR(((obj_t)
																																	BgL_ll1123z00_4303));
																															{
																																obj_t
																																	BgL_tail1125z00_5019;
																																obj_t
																																	BgL_ll1123z00_5018;
																																obj_t
																																	BgL_ll1122z00_5017;
																																BgL_ll1122z00_5017
																																	=
																																	BgL_arg1729z00_4311;
																																BgL_ll1123z00_5018
																																	=
																																	BgL_arg1730z00_4312;
																																BgL_tail1125z00_5019
																																	=
																																	BgL_newtail1126z00_4305;
																																BgL_tail1125z00_4304
																																	=
																																	BgL_tail1125z00_5019;
																																BgL_ll1123z00_4303
																																	=
																																	BgL_ll1123z00_5018;
																																BgL_ll1122z00_4302
																																	=
																																	BgL_ll1122z00_5017;
																																goto
																																	BgL_zc3z04anonymousza31727ze3z87_4301;
																															}
																														}
																													}
																											}
																										}
																									BgL_arg1724z00_4298 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_arg1725z00_4299, BNIL);
																								}
																								BgL_arg1723z00_4297 =
																									MAKE_YOUNG_PAIR
																									(BgL_tmps2z00_4263,
																									BgL_arg1724z00_4298);
																							}
																							BgL_arg1722z00_4296 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2310z00zz__install_expandersz00,
																								BgL_arg1723z00_4297);
																						}
																						BgL_arg1717z00_4293 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1722z00_4296, BNIL);
																					}
																					BgL_arg1714z00_4291 =
																						MAKE_YOUNG_PAIR(BgL_arg1715z00_4292,
																						BgL_arg1717z00_4293);
																				}
																				BgL_arg1710z00_4289 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2470z00zz__install_expandersz00,
																					BgL_arg1714z00_4291);
																			}
																			{	/* Eval/expanders.scm 387 */
																				obj_t BgL_arg1735z00_4313;

																				{	/* Eval/expanders.scm 387 */
																					obj_t BgL_arg1736z00_4314;

																					{	/* Eval/expanders.scm 387 */
																						obj_t BgL_arg1737z00_4315;
																						obj_t BgL_arg1738z00_4316;

																						{	/* Eval/expanders.scm 387 */
																							obj_t BgL_arg1739z00_4317;

																							if (NULLP(BgL_varsz00_4248))
																								{	/* Eval/expanders.scm 387 */
																									BgL_arg1739z00_4317 = BNIL;
																								}
																							else
																								{	/* Eval/expanders.scm 387 */
																									obj_t BgL_head1130z00_4318;

																									BgL_head1130z00_4318 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_ll1128z00_4320;
																										obj_t BgL_ll1129z00_4321;
																										obj_t BgL_tail1131z00_4322;

																										BgL_ll1128z00_4320 =
																											BgL_varsz00_4248;
																										BgL_ll1129z00_4321 =
																											BgL_tmpsz00_4251;
																										BgL_tail1131z00_4322 =
																											BgL_head1130z00_4318;
																									BgL_zc3z04anonymousza31741ze3z87_4319:
																										if (NULLP
																											(BgL_ll1128z00_4320))
																											{	/* Eval/expanders.scm 387 */
																												BgL_arg1739z00_4317 =
																													CDR
																													(BgL_head1130z00_4318);
																											}
																										else
																											{	/* Eval/expanders.scm 387 */
																												obj_t
																													BgL_newtail1132z00_4323;
																												{	/* Eval/expanders.scm 387 */
																													obj_t
																														BgL_arg1745z00_4324;
																													{	/* Eval/expanders.scm 387 */
																														obj_t BgL_vz00_4325;
																														obj_t BgL_tz00_4326;

																														BgL_vz00_4325 =
																															CAR(
																															((obj_t)
																																BgL_ll1128z00_4320));
																														BgL_tz00_4326 =
																															CAR(((obj_t)
																																BgL_ll1129z00_4321));
																														{	/* Eval/expanders.scm 387 */
																															obj_t
																																BgL_arg1746z00_4327;
																															BgL_arg1746z00_4327
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_tz00_4326,
																																BNIL);
																															BgL_arg1745z00_4324
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_vz00_4325,
																																BgL_arg1746z00_4327);
																														}
																													}
																													BgL_newtail1132z00_4323
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1745z00_4324,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1131z00_4322,
																													BgL_newtail1132z00_4323);
																												{	/* Eval/expanders.scm 387 */
																													obj_t
																														BgL_arg1743z00_4328;
																													obj_t
																														BgL_arg1744z00_4329;
																													BgL_arg1743z00_4328 =
																														CDR(((obj_t)
																															BgL_ll1128z00_4320));
																													BgL_arg1744z00_4329 =
																														CDR(((obj_t)
																															BgL_ll1129z00_4321));
																													{
																														obj_t
																															BgL_tail1131z00_5046;
																														obj_t
																															BgL_ll1129z00_5045;
																														obj_t
																															BgL_ll1128z00_5044;
																														BgL_ll1128z00_5044 =
																															BgL_arg1743z00_4328;
																														BgL_ll1129z00_5045 =
																															BgL_arg1744z00_4329;
																														BgL_tail1131z00_5046
																															=
																															BgL_newtail1132z00_4323;
																														BgL_tail1131z00_4322
																															=
																															BgL_tail1131z00_5046;
																														BgL_ll1129z00_4321 =
																															BgL_ll1129z00_5045;
																														BgL_ll1128z00_4320 =
																															BgL_ll1128z00_5044;
																														goto
																															BgL_zc3z04anonymousza31741ze3z87_4319;
																													}
																												}
																											}
																									}
																								}
																							BgL_arg1737z00_4315 =
																								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																								(BgL_arg1739z00_4317, BNIL);
																						}
																						BgL_arg1738z00_4316 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_exprsz00_4250, BNIL);
																						BgL_arg1736z00_4314 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1737z00_4315,
																							BgL_arg1738z00_4316);
																					}
																					BgL_arg1735z00_4313 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2414z00zz__install_expandersz00,
																						BgL_arg1736z00_4314);
																				}
																				BgL_arg1711z00_4290 =
																					MAKE_YOUNG_PAIR(BgL_arg1735z00_4313,
																					BNIL);
																			}
																			BgL_arg1702z00_4278 =
																				MAKE_YOUNG_PAIR(BgL_arg1710z00_4289,
																				BgL_arg1711z00_4290);
																		}
																		BgL_arg1700z00_4276 =
																			MAKE_YOUNG_PAIR(BgL_arg1701z00_4277,
																			BgL_arg1702z00_4278);
																	}
																	BgL_nxz00_4275 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2414z00zz__install_expandersz00,
																		BgL_arg1700z00_4276);
																}
																{	/* Eval/expanders.scm 381 */

																	{	/* Eval/expanders.scm 390 */
																		obj_t BgL_arg1699z00_4330;

																		BgL_arg1699z00_4330 =
																			BGL_PROCEDURE_CALL2(BgL_ez00_3971,
																			BgL_nxz00_4275, BgL_ez00_3971);
																		return
																			BGl_evepairifyz00zz__prognz00
																			(BgL_arg1699z00_4330, BgL_xz00_3970);
																	}
																}
															}
														}
													}
												}
											else
												{	/* Eval/expanders.scm 375 */
													return
														BGl_expandzd2errorzd2zz__install_expandersz00
														(BGl_string2413z00zz__install_expandersz00,
														BGl_string2467z00zz__install_expandersz00,
														BgL_xz00_3970);
												}
										}
									else
										{	/* Eval/expanders.scm 375 */
											return
												BGl_expandzd2errorzd2zz__install_expandersz00
												(BGl_string2413z00zz__install_expandersz00,
												BGl_string2467z00zz__install_expandersz00,
												BgL_xz00_3970);
										}
								}
							}
						else
							{	/* Eval/expanders.scm 375 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2413z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3970);
							}
					}
				else
					{	/* Eval/expanders.scm 375 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2413z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3970);
					}
			}
		}

	}



/* &<@anonymous:1646> */
	obj_t BGl_z62zc3z04anonymousza31646ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3972, obj_t BgL_xz00_3973, obj_t BgL_ez00_3974)
	{
		{	/* Eval/expanders.scm 357 */
			{
				obj_t BgL_handlerz00_4337;
				obj_t BgL_bodyz00_4338;

				if (PAIRP(BgL_xz00_3973))
					{	/* Eval/expanders.scm 357 */
						obj_t BgL_cdrzd2483zd2_4362;

						BgL_cdrzd2483zd2_4362 = CDR(((obj_t) BgL_xz00_3973));
						if (PAIRP(BgL_cdrzd2483zd2_4362))
							{	/* Eval/expanders.scm 357 */
								BgL_handlerz00_4337 = CAR(BgL_cdrzd2483zd2_4362);
								BgL_bodyz00_4338 = CDR(BgL_cdrzd2483zd2_4362);
								{	/* Eval/expanders.scm 360 */
									obj_t BgL_exnz00_4339;

									BgL_exnz00_4339 =
										BGl_gensymz00zz__r4_symbols_6_4z00
										(BGl_symbol2472z00zz__install_expandersz00);
									{	/* Eval/expanders.scm 363 */
										obj_t BgL_arg1651z00_4340;

										{	/* Eval/expanders.scm 363 */
											obj_t BgL_arg1652z00_4341;

											{	/* Eval/expanders.scm 363 */
												obj_t BgL_arg1653z00_4342;
												obj_t BgL_arg1654z00_4343;

												{	/* Eval/expanders.scm 363 */
													obj_t BgL_arg1656z00_4344;

													{	/* Eval/expanders.scm 363 */
														obj_t BgL_arg1657z00_4345;

														{	/* Eval/expanders.scm 363 */
															obj_t BgL_arg1658z00_4346;
															obj_t BgL_arg1661z00_4347;

															BgL_arg1658z00_4346 =
																MAKE_YOUNG_PAIR(BgL_exnz00_4339, BNIL);
															{	/* Eval/expanders.scm 364 */
																obj_t BgL_arg1663z00_4348;
																obj_t BgL_arg1664z00_4349;

																{	/* Eval/expanders.scm 364 */
																	obj_t BgL_arg1667z00_4350;

																	BgL_arg1667z00_4350 =
																		MAKE_YOUNG_PAIR(BINT(0L), BNIL);
																	BgL_arg1663z00_4348 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2474z00zz__install_expandersz00,
																		BgL_arg1667z00_4350);
																}
																{	/* Eval/expanders.scm 365 */
																	obj_t BgL_arg1668z00_4351;

																	{	/* Eval/expanders.scm 365 */
																		obj_t BgL_arg1669z00_4352;

																		BgL_arg1669z00_4352 =
																			MAKE_YOUNG_PAIR(BgL_exnz00_4339, BNIL);
																		BgL_arg1668z00_4351 =
																			MAKE_YOUNG_PAIR(BgL_handlerz00_4337,
																			BgL_arg1669z00_4352);
																	}
																	BgL_arg1664z00_4349 =
																		MAKE_YOUNG_PAIR(BgL_arg1668z00_4351, BNIL);
																}
																BgL_arg1661z00_4347 =
																	MAKE_YOUNG_PAIR(BgL_arg1663z00_4348,
																	BgL_arg1664z00_4349);
															}
															BgL_arg1657z00_4345 =
																MAKE_YOUNG_PAIR(BgL_arg1658z00_4346,
																BgL_arg1661z00_4347);
														}
														BgL_arg1656z00_4344 =
															MAKE_YOUNG_PAIR
															(BGl_symbol2310z00zz__install_expandersz00,
															BgL_arg1657z00_4345);
													}
													BgL_arg1653z00_4342 =
														BGL_PROCEDURE_CALL2(BgL_ez00_3974,
														BgL_arg1656z00_4344, BgL_ez00_3974);
												}
												{	/* Eval/expanders.scm 367 */
													obj_t BgL_arg1670z00_4353;

													if (NULLP(BgL_bodyz00_4338))
														{	/* Eval/expanders.scm 367 */
															BgL_arg1670z00_4353 = BNIL;
														}
													else
														{	/* Eval/expanders.scm 367 */
															obj_t BgL_head1102z00_4354;

															BgL_head1102z00_4354 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1100z00_4356;
																obj_t BgL_tail1103z00_4357;

																BgL_l1100z00_4356 = BgL_bodyz00_4338;
																BgL_tail1103z00_4357 = BgL_head1102z00_4354;
															BgL_zc3z04anonymousza31672ze3z87_4355:
																if (NULLP(BgL_l1100z00_4356))
																	{	/* Eval/expanders.scm 367 */
																		BgL_arg1670z00_4353 =
																			CDR(BgL_head1102z00_4354);
																	}
																else
																	{	/* Eval/expanders.scm 367 */
																		obj_t BgL_newtail1104z00_4358;

																		{	/* Eval/expanders.scm 367 */
																			obj_t BgL_arg1676z00_4359;

																			{	/* Eval/expanders.scm 367 */
																				obj_t BgL_xz00_4360;

																				BgL_xz00_4360 =
																					CAR(((obj_t) BgL_l1100z00_4356));
																				BgL_arg1676z00_4359 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_3974,
																					BgL_xz00_4360, BgL_ez00_3974);
																			}
																			BgL_newtail1104z00_4358 =
																				MAKE_YOUNG_PAIR(BgL_arg1676z00_4359,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1103z00_4357,
																			BgL_newtail1104z00_4358);
																		{	/* Eval/expanders.scm 367 */
																			obj_t BgL_arg1675z00_4361;

																			BgL_arg1675z00_4361 =
																				CDR(((obj_t) BgL_l1100z00_4356));
																			{
																				obj_t BgL_tail1103z00_5107;
																				obj_t BgL_l1100z00_5106;

																				BgL_l1100z00_5106 = BgL_arg1675z00_4361;
																				BgL_tail1103z00_5107 =
																					BgL_newtail1104z00_4358;
																				BgL_tail1103z00_4357 =
																					BgL_tail1103z00_5107;
																				BgL_l1100z00_4356 = BgL_l1100z00_5106;
																				goto
																					BgL_zc3z04anonymousza31672ze3z87_4355;
																			}
																		}
																	}
															}
														}
													BgL_arg1654z00_4343 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1670z00_4353, BNIL);
												}
												BgL_arg1652z00_4341 =
													MAKE_YOUNG_PAIR(BgL_arg1653z00_4342,
													BgL_arg1654z00_4343);
											}
											BgL_arg1651z00_4340 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2409z00zz__install_expandersz00,
												BgL_arg1652z00_4341);
										}
										return
											BGl_evepairifyz00zz__prognz00(BgL_arg1651z00_4340,
											BgL_xz00_3973);
									}
								}
							}
						else
							{	/* Eval/expanders.scm 357 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2410z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3973);
							}
					}
				else
					{	/* Eval/expanders.scm 357 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2410z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3973);
					}
			}
		}

	}



/* &<@anonymous:1627> */
	obj_t BGl_z62zc3z04anonymousza31627ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3975, obj_t BgL_xz00_3976, obj_t BgL_ez00_3977)
	{
		{	/* Eval/expanders.scm 344 */
			{
				obj_t BgL_bodyz00_4364;
				obj_t BgL_expz00_4365;

				if (PAIRP(BgL_xz00_3976))
					{	/* Eval/expanders.scm 344 */
						obj_t BgL_cdrzd2468zd2_4379;

						BgL_cdrzd2468zd2_4379 = CDR(((obj_t) BgL_xz00_3976));
						if (PAIRP(BgL_cdrzd2468zd2_4379))
							{	/* Eval/expanders.scm 344 */
								BgL_bodyz00_4364 = CAR(BgL_cdrzd2468zd2_4379);
								BgL_expz00_4365 = CDR(BgL_cdrzd2468zd2_4379);
								{	/* Eval/expanders.scm 349 */
									obj_t BgL_arg1634z00_4366;

									{	/* Eval/expanders.scm 349 */
										obj_t BgL_arg1636z00_4367;

										{	/* Eval/expanders.scm 349 */
											obj_t BgL_arg1637z00_4368;
											obj_t BgL_arg1638z00_4369;

											BgL_arg1637z00_4368 =
												BGL_PROCEDURE_CALL2(BgL_ez00_3977, BgL_bodyz00_4364,
												BgL_ez00_3977);
											{	/* Eval/expanders.scm 350 */
												obj_t BgL_arg1639z00_4370;

												if (NULLP(BgL_expz00_4365))
													{	/* Eval/expanders.scm 350 */
														BgL_arg1639z00_4370 = BNIL;
													}
												else
													{	/* Eval/expanders.scm 350 */
														obj_t BgL_head1097z00_4371;

														BgL_head1097z00_4371 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1095z00_4373;
															obj_t BgL_tail1098z00_4374;

															BgL_l1095z00_4373 = BgL_expz00_4365;
															BgL_tail1098z00_4374 = BgL_head1097z00_4371;
														BgL_zc3z04anonymousza31641ze3z87_4372:
															if (NULLP(BgL_l1095z00_4373))
																{	/* Eval/expanders.scm 350 */
																	BgL_arg1639z00_4370 =
																		CDR(BgL_head1097z00_4371);
																}
															else
																{	/* Eval/expanders.scm 350 */
																	obj_t BgL_newtail1099z00_4375;

																	{	/* Eval/expanders.scm 350 */
																		obj_t BgL_arg1644z00_4376;

																		{	/* Eval/expanders.scm 350 */
																			obj_t BgL_xz00_4377;

																			BgL_xz00_4377 =
																				CAR(((obj_t) BgL_l1095z00_4373));
																			BgL_arg1644z00_4376 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_3977,
																				BgL_xz00_4377, BgL_ez00_3977);
																		}
																		BgL_newtail1099z00_4375 =
																			MAKE_YOUNG_PAIR(BgL_arg1644z00_4376,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1098z00_4374,
																		BgL_newtail1099z00_4375);
																	{	/* Eval/expanders.scm 350 */
																		obj_t BgL_arg1643z00_4378;

																		BgL_arg1643z00_4378 =
																			CDR(((obj_t) BgL_l1095z00_4373));
																		{
																			obj_t BgL_tail1098z00_5145;
																			obj_t BgL_l1095z00_5144;

																			BgL_l1095z00_5144 = BgL_arg1643z00_4378;
																			BgL_tail1098z00_5145 =
																				BgL_newtail1099z00_4375;
																			BgL_tail1098z00_4374 =
																				BgL_tail1098z00_5145;
																			BgL_l1095z00_4373 = BgL_l1095z00_5144;
																			goto
																				BgL_zc3z04anonymousza31641ze3z87_4372;
																		}
																	}
																}
														}
													}
												BgL_arg1638z00_4369 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1639z00_4370, BNIL);
											}
											BgL_arg1636z00_4367 =
												MAKE_YOUNG_PAIR(BgL_arg1637z00_4368,
												BgL_arg1638z00_4369);
										}
										BgL_arg1634z00_4366 =
											MAKE_YOUNG_PAIR(BGl_symbol2406z00zz__install_expandersz00,
											BgL_arg1636z00_4367);
									}
									return
										BGl_evepairifyz00zz__prognz00(BgL_arg1634z00_4366,
										BgL_xz00_3976);
								}
							}
						else
							{	/* Eval/expanders.scm 344 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2407z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3976);
							}
					}
				else
					{	/* Eval/expanders.scm 344 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2407z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3976);
					}
			}
		}

	}



/* &<@anonymous:1495> */
	obj_t BGl_z62zc3z04anonymousza31495ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3978, obj_t BgL_xz00_3979, obj_t BgL_ez00_3980)
	{
		{	/* Eval/expanders.scm 296 */
			{
				obj_t BgL_envz00_4404;
				obj_t BgL_exitz00_4405;
				obj_t BgL_bodyz00_4406;
				obj_t BgL_onexitz00_4407;
				obj_t BgL_envz00_4393;
				obj_t BgL_exitz00_4394;
				obj_t BgL_bodyz00_4395;
				obj_t BgL_exitz00_4383;
				obj_t BgL_bodyz00_4384;

				if (PAIRP(BgL_xz00_3979))
					{	/* Eval/expanders.scm 308 */
						obj_t BgL_cdrzd2246zd2_4448;

						BgL_cdrzd2246zd2_4448 = CDR(((obj_t) BgL_xz00_3979));
						if (PAIRP(BgL_cdrzd2246zd2_4448))
							{	/* Eval/expanders.scm 308 */
								obj_t BgL_carzd2249zd2_4449;
								obj_t BgL_cdrzd2250zd2_4450;

								BgL_carzd2249zd2_4449 = CAR(BgL_cdrzd2246zd2_4448);
								BgL_cdrzd2250zd2_4450 = CDR(BgL_cdrzd2246zd2_4448);
								if (PAIRP(BgL_carzd2249zd2_4449))
									{	/* Eval/expanders.scm 308 */
										if (NULLP(CDR(BgL_carzd2249zd2_4449)))
											{	/* Eval/expanders.scm 308 */
												if (NULLP(BgL_cdrzd2250zd2_4450))
													{	/* Eval/expanders.scm 308 */
														return
															BGl_expandzd2errorzd2zz__install_expandersz00
															(BGl_string2404z00zz__install_expandersz00,
															BGl_string2467z00zz__install_expandersz00,
															BgL_xz00_3979);
													}
												else
													{	/* Eval/expanders.scm 308 */
														BgL_exitz00_4383 = CAR(BgL_carzd2249zd2_4449);
														BgL_bodyz00_4384 = BgL_cdrzd2250zd2_4450;
														{	/* Eval/expanders.scm 311 */
															obj_t BgL_arg1539z00_4385;

															if (BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00(BgL_exitz00_4383, BgL_bodyz00_4384))
																{	/* Eval/expanders.scm 312 */
																	obj_t BgL_arg1543z00_4386;

																	{	/* Eval/expanders.scm 312 */
																		obj_t BgL_arg1544z00_4387;
																		obj_t BgL_arg1546z00_4388;

																		BgL_arg1544z00_4387 =
																			MAKE_YOUNG_PAIR(BgL_exitz00_4383, BNIL);
																		{	/* Eval/expanders.scm 313 */
																			obj_t BgL_arg1547z00_4389;

																			{	/* Eval/expanders.scm 313 */
																				obj_t BgL_arg1549z00_4390;

																				BgL_arg1549z00_4390 =
																					BGl_expandzd2prognzd2zz__prognz00
																					(BgL_bodyz00_4384);
																				BgL_arg1547z00_4389 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_3980,
																					BgL_arg1549z00_4390, BgL_ez00_3980);
																			}
																			BgL_arg1546z00_4388 =
																				MAKE_YOUNG_PAIR(BgL_arg1547z00_4389,
																				BNIL);
																		}
																		BgL_arg1543z00_4386 =
																			MAKE_YOUNG_PAIR(BgL_arg1544z00_4387,
																			BgL_arg1546z00_4388);
																	}
																	BgL_arg1539z00_4385 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2403z00zz__install_expandersz00,
																		BgL_arg1543z00_4386);
																}
															else
																{	/* Eval/expanders.scm 314 */
																	obj_t BgL_arg1552z00_4391;

																	{	/* Eval/expanders.scm 314 */
																		obj_t BgL_arg1553z00_4392;

																		BgL_arg1553z00_4392 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_bodyz00_4384, BNIL);
																		BgL_arg1552z00_4391 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2363z00zz__install_expandersz00,
																			BgL_arg1553z00_4392);
																	}
																	BgL_arg1539z00_4385 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_3980,
																		BgL_arg1552z00_4391, BgL_ez00_3980);
																}
															return
																BGl_evepairifyz00zz__prognz00
																(BgL_arg1539z00_4385, BgL_xz00_3979);
														}
													}
											}
										else
											{	/* Eval/expanders.scm 308 */
												return
													BGl_expandzd2errorzd2zz__install_expandersz00
													(BGl_string2404z00zz__install_expandersz00,
													BGl_string2467z00zz__install_expandersz00,
													BgL_xz00_3979);
											}
									}
								else
									{	/* Eval/expanders.scm 308 */
										obj_t BgL_cdrzd2300zd2_4451;

										BgL_cdrzd2300zd2_4451 =
											CDR(((obj_t) BgL_cdrzd2246zd2_4448));
										if (
											(CAR(
													((obj_t) BgL_cdrzd2246zd2_4448)) ==
												BGl_keyword2484z00zz__install_expandersz00))
											{	/* Eval/expanders.scm 308 */
												if (PAIRP(BgL_cdrzd2300zd2_4451))
													{	/* Eval/expanders.scm 308 */
														obj_t BgL_cdrzd2305zd2_4452;

														BgL_cdrzd2305zd2_4452 = CDR(BgL_cdrzd2300zd2_4451);
														if (PAIRP(BgL_cdrzd2305zd2_4452))
															{	/* Eval/expanders.scm 308 */
																obj_t BgL_carzd2309zd2_4453;
																obj_t BgL_cdrzd2310zd2_4454;

																BgL_carzd2309zd2_4453 =
																	CAR(BgL_cdrzd2305zd2_4452);
																BgL_cdrzd2310zd2_4454 =
																	CDR(BgL_cdrzd2305zd2_4452);
																if (PAIRP(BgL_carzd2309zd2_4453))
																	{	/* Eval/expanders.scm 308 */
																		if (NULLP(CDR(BgL_carzd2309zd2_4453)))
																			{	/* Eval/expanders.scm 308 */
																				if (PAIRP(BgL_cdrzd2310zd2_4454))
																					{	/* Eval/expanders.scm 308 */
																						if (NULLP(CDR
																								(BgL_cdrzd2310zd2_4454)))
																							{	/* Eval/expanders.scm 308 */
																								BgL_envz00_4393 =
																									CAR(BgL_cdrzd2300zd2_4451);
																								BgL_exitz00_4394 =
																									CAR(BgL_carzd2309zd2_4453);
																								BgL_bodyz00_4395 =
																									CAR(BgL_cdrzd2310zd2_4454);
																								{	/* Eval/expanders.scm 318 */
																									obj_t BgL_arg1554z00_4396;

																									if (BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00(BgL_exitz00_4394, BgL_bodyz00_4395))
																										{	/* Eval/expanders.scm 319 */
																											obj_t BgL_arg1556z00_4397;

																											{	/* Eval/expanders.scm 319 */
																												obj_t
																													BgL_arg1557z00_4398;
																												obj_t
																													BgL_arg1558z00_4399;
																												BgL_arg1557z00_4398 =
																													MAKE_YOUNG_PAIR
																													(BgL_exitz00_4394,
																													BNIL);
																												{	/* Eval/expanders.scm 320 */
																													obj_t
																														BgL_arg1559z00_4400;
																													{	/* Eval/expanders.scm 320 */
																														obj_t
																															BgL_arg1561z00_4401;
																														BgL_arg1561z00_4401
																															=
																															BGl_expandzd2prognzd2zz__prognz00
																															(BgL_bodyz00_4395);
																														BgL_arg1559z00_4400
																															=
																															BGL_PROCEDURE_CALL2
																															(BgL_ez00_3980,
																															BgL_arg1561z00_4401,
																															BgL_ez00_3980);
																													}
																													BgL_arg1558z00_4399 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1559z00_4400,
																														BNIL);
																												}
																												BgL_arg1556z00_4397 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1557z00_4398,
																													BgL_arg1558z00_4399);
																											}
																											BgL_arg1554z00_4396 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2403z00zz__install_expandersz00,
																												BgL_arg1556z00_4397);
																										}
																									else
																										{	/* Eval/expanders.scm 321 */
																											obj_t BgL_arg1562z00_4402;

																											{	/* Eval/expanders.scm 321 */
																												obj_t
																													BgL_arg1564z00_4403;
																												BgL_arg1564z00_4403 =
																													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																													(BgL_bodyz00_4395,
																													BNIL);
																												BgL_arg1562z00_4402 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2363z00zz__install_expandersz00,
																													BgL_arg1564z00_4403);
																											}
																											BgL_arg1554z00_4396 =
																												BGL_PROCEDURE_CALL2
																												(BgL_ez00_3980,
																												BgL_arg1562z00_4402,
																												BgL_ez00_3980);
																										}
																									return
																										BGl_evepairifyz00zz__prognz00
																										(BgL_arg1554z00_4396,
																										BgL_xz00_3979);
																								}
																							}
																						else
																							{	/* Eval/expanders.scm 308 */
																								obj_t BgL_cdrzd2340zd2_4455;

																								{	/* Eval/expanders.scm 308 */
																									obj_t BgL_pairz00_4456;

																									BgL_pairz00_4456 =
																										CDR(
																										((obj_t) BgL_xz00_3979));
																									BgL_cdrzd2340zd2_4455 =
																										CDR(BgL_pairz00_4456);
																								}
																								{	/* Eval/expanders.scm 308 */
																									obj_t BgL_cdrzd2349zd2_4457;

																									BgL_cdrzd2349zd2_4457 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2340zd2_4455));
																									{	/* Eval/expanders.scm 308 */
																										obj_t BgL_cdrzd2358zd2_4458;

																										BgL_cdrzd2358zd2_4458 =
																											CDR(
																											((obj_t)
																												BgL_cdrzd2349zd2_4457));
																										{	/* Eval/expanders.scm 308 */
																											obj_t
																												BgL_cdrzd2369zd2_4459;
																											BgL_cdrzd2369zd2_4459 =
																												CDR(((obj_t)
																													BgL_cdrzd2358zd2_4458));
																											if (PAIRP
																												(BgL_cdrzd2369zd2_4459))
																												{	/* Eval/expanders.scm 308 */
																													if (NULLP(CDR
																															(BgL_cdrzd2369zd2_4459)))
																														{	/* Eval/expanders.scm 308 */
																															obj_t
																																BgL_arg1525z00_4460;
																															obj_t
																																BgL_arg1526z00_4461;
																															obj_t
																																BgL_arg1527z00_4462;
																															obj_t
																																BgL_arg1528z00_4463;
																															BgL_arg1525z00_4460
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2340zd2_4455));
																															{	/* Eval/expanders.scm 308 */
																																obj_t
																																	BgL_pairz00_4464;
																																BgL_pairz00_4464
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd2349zd2_4457));
																																BgL_arg1526z00_4461
																																	=
																																	CAR
																																	(BgL_pairz00_4464);
																															}
																															BgL_arg1527z00_4462
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2358zd2_4458));
																															BgL_arg1528z00_4463
																																=
																																CAR
																																(BgL_cdrzd2369zd2_4459);
																															BgL_envz00_4404 =
																																BgL_arg1525z00_4460;
																															BgL_exitz00_4405 =
																																BgL_arg1526z00_4461;
																															BgL_bodyz00_4406 =
																																BgL_arg1527z00_4462;
																															BgL_onexitz00_4407
																																=
																																BgL_arg1528z00_4463;
																															{	/* Eval/expanders.scm 325 */
																																obj_t
																																	BgL_arg1565z00_4408;
																																{	/* Eval/expanders.scm 325 */
																																	obj_t
																																		BgL_exit2z00_4409;
																																	obj_t
																																		BgL_flagz00_4410;
																																	obj_t
																																		BgL_valz00_4411;
																																	BgL_exit2z00_4409
																																		=
																																		BGl_gensymz00zz__r4_symbols_6_4z00
																																		(BGl_symbol2476z00zz__install_expandersz00);
																																	BgL_flagz00_4410
																																		=
																																		BGl_gensymz00zz__r4_symbols_6_4z00
																																		(BGl_symbol2478z00zz__install_expandersz00);
																																	BgL_valz00_4411
																																		=
																																		BGl_gensymz00zz__r4_symbols_6_4z00
																																		(BGl_symbol2480z00zz__install_expandersz00);
																																	{	/* Eval/expanders.scm 328 */
																																		obj_t
																																			BgL_arg1567z00_4412;
																																		{	/* Eval/expanders.scm 328 */
																																			obj_t
																																				BgL_arg1571z00_4413;
																																			{	/* Eval/expanders.scm 328 */
																																				obj_t
																																					BgL_arg1573z00_4414;
																																				obj_t
																																					BgL_arg1575z00_4415;
																																				{	/* Eval/expanders.scm 328 */
																																					obj_t
																																						BgL_arg1576z00_4416;
																																					{	/* Eval/expanders.scm 328 */
																																						obj_t
																																							BgL_arg1578z00_4417;
																																						BgL_arg1578z00_4417
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BFALSE,
																																							BNIL);
																																						BgL_arg1576z00_4416
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_flagz00_4410,
																																							BgL_arg1578z00_4417);
																																					}
																																					BgL_arg1573z00_4414
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1576z00_4416,
																																						BNIL);
																																				}
																																				{	/* Eval/expanders.scm 329 */
																																					obj_t
																																						BgL_arg1579z00_4418;
																																					{	/* Eval/expanders.scm 329 */
																																						obj_t
																																							BgL_arg1580z00_4419;
																																						{	/* Eval/expanders.scm 329 */
																																							obj_t
																																								BgL_arg1582z00_4420;
																																							obj_t
																																								BgL_arg1583z00_4421;
																																							{	/* Eval/expanders.scm 329 */
																																								obj_t
																																									BgL_arg1584z00_4422;
																																								{	/* Eval/expanders.scm 329 */
																																									obj_t
																																										BgL_arg1585z00_4423;
																																									{	/* Eval/expanders.scm 329 */
																																										obj_t
																																											BgL_arg1586z00_4424;
																																										{	/* Eval/expanders.scm 329 */
																																											obj_t
																																												BgL_arg1587z00_4425;
																																											{	/* Eval/expanders.scm 329 */
																																												obj_t
																																													BgL_arg1589z00_4426;
																																												obj_t
																																													BgL_arg1591z00_4427;
																																												BgL_arg1589z00_4426
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_exit2z00_4409,
																																													BNIL);
																																												{	/* Eval/expanders.scm 330 */
																																													obj_t
																																														BgL_arg1593z00_4428;
																																													{	/* Eval/expanders.scm 330 */
																																														obj_t
																																															BgL_arg1594z00_4429;
																																														{	/* Eval/expanders.scm 330 */
																																															obj_t
																																																BgL_arg1595z00_4430;
																																															obj_t
																																																BgL_arg1598z00_4431;
																																															{	/* Eval/expanders.scm 330 */
																																																obj_t
																																																	BgL_arg1601z00_4432;
																																																{	/* Eval/expanders.scm 330 */
																																																	obj_t
																																																		BgL_arg1602z00_4433;
																																																	{	/* Eval/expanders.scm 330 */
																																																		obj_t
																																																			BgL_arg1603z00_4434;
																																																		{	/* Eval/expanders.scm 330 */
																																																			obj_t
																																																				BgL_arg1605z00_4435;
																																																			{	/* Eval/expanders.scm 330 */
																																																				obj_t
																																																					BgL_arg1606z00_4436;
																																																				obj_t
																																																					BgL_arg1607z00_4437;
																																																				BgL_arg1606z00_4436
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BGl_symbol2482z00zz__install_expandersz00,
																																																					BNIL);
																																																				{	/* Eval/expanders.scm 331 */
																																																					obj_t
																																																						BgL_arg1608z00_4438;
																																																					obj_t
																																																						BgL_arg1609z00_4439;
																																																					{	/* Eval/expanders.scm 331 */
																																																						obj_t
																																																							BgL_arg1610z00_4440;
																																																						{	/* Eval/expanders.scm 331 */
																																																							obj_t
																																																								BgL_arg1611z00_4441;
																																																							BgL_arg1611z00_4441
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BTRUE,
																																																								BNIL);
																																																							BgL_arg1610z00_4440
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BgL_flagz00_4410,
																																																								BgL_arg1611z00_4441);
																																																						}
																																																						BgL_arg1608z00_4438
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BGl_symbol2468z00zz__install_expandersz00,
																																																							BgL_arg1610z00_4440);
																																																					}
																																																					{	/* Eval/expanders.scm 332 */
																																																						obj_t
																																																							BgL_arg1612z00_4442;
																																																						{	/* Eval/expanders.scm 332 */
																																																							obj_t
																																																								BgL_arg1613z00_4443;
																																																							BgL_arg1613z00_4443
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BGl_symbol2482z00zz__install_expandersz00,
																																																								BNIL);
																																																							BgL_arg1612z00_4442
																																																								=
																																																								MAKE_YOUNG_PAIR
																																																								(BgL_exit2z00_4409,
																																																								BgL_arg1613z00_4443);
																																																						}
																																																						BgL_arg1609z00_4439
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_arg1612z00_4442,
																																																							BNIL);
																																																					}
																																																					BgL_arg1607z00_4437
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_arg1608z00_4438,
																																																						BgL_arg1609z00_4439);
																																																				}
																																																				BgL_arg1605z00_4435
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg1606z00_4436,
																																																					BgL_arg1607z00_4437);
																																																			}
																																																			BgL_arg1603z00_4434
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BGl_symbol2310z00zz__install_expandersz00,
																																																				BgL_arg1605z00_4435);
																																																		}
																																																		BgL_arg1602z00_4433
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg1603z00_4434,
																																																			BNIL);
																																																	}
																																																	BgL_arg1601z00_4432
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_exitz00_4405,
																																																		BgL_arg1602z00_4433);
																																																}
																																																BgL_arg1595z00_4430
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1601z00_4432,
																																																	BNIL);
																																															}
																																															BgL_arg1598z00_4431
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_bodyz00_4406,
																																																BNIL);
																																															BgL_arg1594z00_4429
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_arg1595z00_4430,
																																																BgL_arg1598z00_4431);
																																														}
																																														BgL_arg1593z00_4428
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_symbol2414z00zz__install_expandersz00,
																																															BgL_arg1594z00_4429);
																																													}
																																													BgL_arg1591z00_4427
																																														=
																																														MAKE_YOUNG_PAIR
																																														(BgL_arg1593z00_4428,
																																														BNIL);
																																												}
																																												BgL_arg1587z00_4425
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1589z00_4426,
																																													BgL_arg1591z00_4427);
																																											}
																																											BgL_arg1586z00_4424
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_symbol2403z00zz__install_expandersz00,
																																												BgL_arg1587z00_4425);
																																										}
																																										BgL_arg1585z00_4423
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg1586z00_4424,
																																											BNIL);
																																									}
																																									BgL_arg1584z00_4422
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_valz00_4411,
																																										BgL_arg1585z00_4423);
																																								}
																																								BgL_arg1582z00_4420
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1584z00_4422,
																																									BNIL);
																																							}
																																							{	/* Eval/expanders.scm 334 */
																																								obj_t
																																									BgL_arg1615z00_4444;
																																								{	/* Eval/expanders.scm 334 */
																																									obj_t
																																										BgL_arg1616z00_4445;
																																									{	/* Eval/expanders.scm 334 */
																																										obj_t
																																											BgL_arg1617z00_4446;
																																										{	/* Eval/expanders.scm 334 */
																																											obj_t
																																												BgL_arg1618z00_4447;
																																											BgL_arg1618z00_4447
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_valz00_4411,
																																												BNIL);
																																											BgL_arg1617z00_4446
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_onexitz00_4407,
																																												BgL_arg1618z00_4447);
																																										}
																																										BgL_arg1616z00_4445
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_flagz00_4410,
																																											BgL_arg1617z00_4446);
																																									}
																																									BgL_arg1615z00_4444
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol2308z00zz__install_expandersz00,
																																										BgL_arg1616z00_4445);
																																								}
																																								BgL_arg1583z00_4421
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1615z00_4444,
																																									BNIL);
																																							}
																																							BgL_arg1580z00_4419
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1582z00_4420,
																																								BgL_arg1583z00_4421);
																																						}
																																						BgL_arg1579z00_4418
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2414z00zz__install_expandersz00,
																																							BgL_arg1580z00_4419);
																																					}
																																					BgL_arg1575z00_4415
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1579z00_4418,
																																						BNIL);
																																				}
																																				BgL_arg1571z00_4413
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1573z00_4414,
																																					BgL_arg1575z00_4415);
																																			}
																																			BgL_arg1567z00_4412
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2414z00zz__install_expandersz00,
																																				BgL_arg1571z00_4413);
																																		}
																																		BgL_arg1565z00_4408
																																			=
																																			BGL_PROCEDURE_CALL2
																																			(BgL_ez00_3980,
																																			BgL_arg1567z00_4412,
																																			BgL_ez00_3980);
																																	}
																																}
																																return
																																	BGl_evepairifyz00zz__prognz00
																																	(BgL_arg1565z00_4408,
																																	BgL_xz00_3979);
																															}
																														}
																													else
																														{	/* Eval/expanders.scm 308 */
																															return
																																BGl_expandzd2errorzd2zz__install_expandersz00
																																(BGl_string2404z00zz__install_expandersz00,
																																BGl_string2467z00zz__install_expandersz00,
																																BgL_xz00_3979);
																														}
																												}
																											else
																												{	/* Eval/expanders.scm 308 */
																													return
																														BGl_expandzd2errorzd2zz__install_expandersz00
																														(BGl_string2404z00zz__install_expandersz00,
																														BGl_string2467z00zz__install_expandersz00,
																														BgL_xz00_3979);
																												}
																										}
																									}
																								}
																							}
																					}
																				else
																					{	/* Eval/expanders.scm 308 */
																						return
																							BGl_expandzd2errorzd2zz__install_expandersz00
																							(BGl_string2404z00zz__install_expandersz00,
																							BGl_string2467z00zz__install_expandersz00,
																							BgL_xz00_3979);
																					}
																			}
																		else
																			{	/* Eval/expanders.scm 308 */
																				return
																					BGl_expandzd2errorzd2zz__install_expandersz00
																					(BGl_string2404z00zz__install_expandersz00,
																					BGl_string2467z00zz__install_expandersz00,
																					BgL_xz00_3979);
																			}
																	}
																else
																	{	/* Eval/expanders.scm 308 */
																		return
																			BGl_expandzd2errorzd2zz__install_expandersz00
																			(BGl_string2404z00zz__install_expandersz00,
																			BGl_string2467z00zz__install_expandersz00,
																			BgL_xz00_3979);
																	}
															}
														else
															{	/* Eval/expanders.scm 308 */
																return
																	BGl_expandzd2errorzd2zz__install_expandersz00
																	(BGl_string2404z00zz__install_expandersz00,
																	BGl_string2467z00zz__install_expandersz00,
																	BgL_xz00_3979);
															}
													}
												else
													{	/* Eval/expanders.scm 308 */
														return
															BGl_expandzd2errorzd2zz__install_expandersz00
															(BGl_string2404z00zz__install_expandersz00,
															BGl_string2467z00zz__install_expandersz00,
															BgL_xz00_3979);
													}
											}
										else
											{	/* Eval/expanders.scm 308 */
												return
													BGl_expandzd2errorzd2zz__install_expandersz00
													(BGl_string2404z00zz__install_expandersz00,
													BGl_string2467z00zz__install_expandersz00,
													BgL_xz00_3979);
											}
									}
							}
						else
							{	/* Eval/expanders.scm 308 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2404z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3979);
							}
					}
				else
					{	/* Eval/expanders.scm 308 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2404z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3979);
					}
			}
		}

	}



/* find-in-body~0 */
	bool_t BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00(obj_t
		BgL_kz00_1655, obj_t BgL_bodyz00_1656)
	{
		{	/* Eval/expanders.scm 306 */
		BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00:
			if ((BgL_bodyz00_1656 == BgL_kz00_1655))
				{	/* Eval/expanders.scm 300 */
					return ((bool_t) 1);
				}
			else
				{	/* Eval/expanders.scm 300 */
					if (PAIRP(BgL_bodyz00_1656))
						{	/* Eval/expanders.scm 302 */
							if (
								(CAR(BgL_bodyz00_1656) ==
									BGl_symbol2313z00zz__install_expandersz00))
								{	/* Eval/expanders.scm 303 */
									return ((bool_t) 0);
								}
							else
								{	/* Eval/expanders.scm 304 */
									bool_t BgL__ortest_1039z00_1661;

									BgL__ortest_1039z00_1661 =
										BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00
										(BgL_kz00_1655, CAR(BgL_bodyz00_1656));
									if (BgL__ortest_1039z00_1661)
										{	/* Eval/expanders.scm 304 */
											return BgL__ortest_1039z00_1661;
										}
									else
										{
											obj_t BgL_bodyz00_5325;

											BgL_bodyz00_5325 = CDR(BgL_bodyz00_1656);
											BgL_bodyz00_1656 = BgL_bodyz00_5325;
											goto BGl_findzd2inzd2bodyze70ze7zz__install_expandersz00;
										}
								}
						}
					else
						{	/* Eval/expanders.scm 302 */
							return ((bool_t) 0);
						}
				}
		}

	}



/* &<@anonymous:1477> */
	obj_t BGl_z62zc3z04anonymousza31477ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3981, obj_t BgL_xz00_3982, obj_t BgL_ez00_3983)
	{
		{	/* Eval/expanders.scm 285 */
			{
				obj_t BgL_bodyz00_4466;

				if (PAIRP(BgL_xz00_3982))
					{	/* Eval/expanders.scm 285 */
						obj_t BgL_cdrzd2229zd2_4478;

						BgL_cdrzd2229zd2_4478 = CDR(((obj_t) BgL_xz00_3982));
						if (PAIRP(BgL_cdrzd2229zd2_4478))
							{	/* Eval/expanders.scm 285 */
								if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(CAR
										(BgL_cdrzd2229zd2_4478)))
									{	/* Eval/expanders.scm 285 */
										BgL_bodyz00_4466 = CDR(BgL_cdrzd2229zd2_4478);
										{	/* Eval/expanders.scm 289 */
											obj_t BgL_arg1484z00_4467;

											{	/* Eval/expanders.scm 289 */
												obj_t BgL_arg1485z00_4468;

												{	/* Eval/expanders.scm 289 */
													obj_t BgL_arg1486z00_4469;

													if (NULLP(BgL_bodyz00_4466))
														{	/* Eval/expanders.scm 289 */
															BgL_arg1486z00_4469 = BNIL;
														}
													else
														{	/* Eval/expanders.scm 289 */
															obj_t BgL_head1092z00_4470;

															BgL_head1092z00_4470 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1090z00_4472;
																obj_t BgL_tail1093z00_4473;

																BgL_l1090z00_4472 = BgL_bodyz00_4466;
																BgL_tail1093z00_4473 = BgL_head1092z00_4470;
															BgL_zc3z04anonymousza31488ze3z87_4471:
																if (NULLP(BgL_l1090z00_4472))
																	{	/* Eval/expanders.scm 289 */
																		BgL_arg1486z00_4469 =
																			CDR(BgL_head1092z00_4470);
																	}
																else
																	{	/* Eval/expanders.scm 289 */
																		obj_t BgL_newtail1094z00_4474;

																		{	/* Eval/expanders.scm 289 */
																			obj_t BgL_arg1492z00_4475;

																			{	/* Eval/expanders.scm 289 */
																				obj_t BgL_xz00_4476;

																				BgL_xz00_4476 =
																					CAR(((obj_t) BgL_l1090z00_4472));
																				BgL_arg1492z00_4475 =
																					BGL_PROCEDURE_CALL2(BgL_ez00_3983,
																					BgL_xz00_4476, BgL_ez00_3983);
																			}
																			BgL_newtail1094z00_4474 =
																				MAKE_YOUNG_PAIR(BgL_arg1492z00_4475,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1093z00_4473,
																			BgL_newtail1094z00_4474);
																		{	/* Eval/expanders.scm 289 */
																			obj_t BgL_arg1490z00_4477;

																			BgL_arg1490z00_4477 =
																				CDR(((obj_t) BgL_l1090z00_4472));
																			{
																				obj_t BgL_tail1093z00_5354;
																				obj_t BgL_l1090z00_5353;

																				BgL_l1090z00_5353 = BgL_arg1490z00_4477;
																				BgL_tail1093z00_5354 =
																					BgL_newtail1094z00_4474;
																				BgL_tail1093z00_4473 =
																					BgL_tail1093z00_5354;
																				BgL_l1090z00_4472 = BgL_l1090z00_5353;
																				goto
																					BgL_zc3z04anonymousza31488ze3z87_4471;
																			}
																		}
																	}
															}
														}
													BgL_arg1485z00_4468 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1486z00_4469, BNIL);
												}
												BgL_arg1484z00_4467 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2363z00zz__install_expandersz00,
													BgL_arg1485z00_4468);
											}
											return
												BGl_evepairifyz00zz__prognz00(BgL_arg1484z00_4467,
												BgL_xz00_3982);
										}
									}
								else
									{	/* Eval/expanders.scm 285 */
										return
											BGl_expandzd2errorzd2zz__install_expandersz00
											(BGl_string2401z00zz__install_expandersz00,
											BGl_string2467z00zz__install_expandersz00, BgL_xz00_3982);
									}
							}
						else
							{	/* Eval/expanders.scm 285 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2401z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3982);
							}
					}
				else
					{	/* Eval/expanders.scm 285 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2401z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3982);
					}
			}
		}

	}



/* &<@anonymous:1459> */
	obj_t BGl_z62zc3z04anonymousza31459ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3984, obj_t BgL_xz00_3985, obj_t BgL_ez00_3986)
	{
		{	/* Eval/expanders.scm 246 */
			if (PAIRP(BgL_xz00_3985))
				{	/* Eval/expanders.scm 246 */
					obj_t BgL_cdrzd2216zd2_4479;

					BgL_cdrzd2216zd2_4479 = CDR(((obj_t) BgL_xz00_3985));
					if (PAIRP(BgL_cdrzd2216zd2_4479))
						{	/* Eval/expanders.scm 246 */
							obj_t BgL_arg1462z00_4480;
							obj_t BgL_arg1463z00_4481;

							BgL_arg1462z00_4480 = CAR(BgL_cdrzd2216zd2_4479);
							BgL_arg1463z00_4481 = CDR(BgL_cdrzd2216zd2_4479);
							{	/* Eval/expanders.scm 251 */
								obj_t BgL_arg1464z00_4482;

								{	/* Eval/expanders.scm 251 */
									obj_t BgL_arg1465z00_4483;

									{	/* Eval/expanders.scm 251 */
										obj_t BgL_arg1466z00_4484;

										{	/* Eval/expanders.scm 251 */
											obj_t BgL_arg1467z00_4485;

											{	/* Eval/expanders.scm 251 */
												obj_t BgL_arg1468z00_4486;

												{	/* Eval/expanders.scm 251 */
													obj_t BgL_arg1469z00_4487;

													BgL_arg1469z00_4487 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1463z00_4481, BNIL);
													BgL_arg1468z00_4486 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2363z00zz__install_expandersz00,
														BgL_arg1469z00_4487);
												}
												BgL_arg1467z00_4485 =
													MAKE_YOUNG_PAIR(BgL_arg1468z00_4486, BNIL);
											}
											BgL_arg1466z00_4484 =
												MAKE_YOUNG_PAIR(BFALSE, BgL_arg1467z00_4485);
										}
										BgL_arg1465z00_4483 =
											MAKE_YOUNG_PAIR(BgL_arg1462z00_4480, BgL_arg1466z00_4484);
									}
									BgL_arg1464z00_4482 =
										MAKE_YOUNG_PAIR(BGl_symbol2308z00zz__install_expandersz00,
										BgL_arg1465z00_4483);
								}
								return
									BGL_PROCEDURE_CALL2(BgL_ez00_3986, BgL_arg1464z00_4482,
									BgL_ez00_3986);
							}
						}
					else
						{	/* Eval/expanders.scm 246 */
							return
								BGl_expandzd2errorzd2zz__install_expandersz00
								(BGl_string2376z00zz__install_expandersz00,
								BGl_string2467z00zz__install_expandersz00, BgL_xz00_3985);
						}
				}
			else
				{	/* Eval/expanders.scm 246 */
					return
						BGl_expandzd2errorzd2zz__install_expandersz00
						(BGl_string2376z00zz__install_expandersz00,
						BGl_string2467z00zz__install_expandersz00, BgL_xz00_3985);
				}
		}

	}



/* &<@anonymous:1447> */
	obj_t BGl_z62zc3z04anonymousza31447ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3987, obj_t BgL_xz00_3988, obj_t BgL_ez00_3989)
	{
		{	/* Eval/expanders.scm 234 */
			if (PAIRP(BgL_xz00_3988))
				{	/* Eval/expanders.scm 234 */
					obj_t BgL_cdrzd2201zd2_4488;

					BgL_cdrzd2201zd2_4488 = CDR(((obj_t) BgL_xz00_3988));
					if (PAIRP(BgL_cdrzd2201zd2_4488))
						{	/* Eval/expanders.scm 234 */
							obj_t BgL_arg1450z00_4489;
							obj_t BgL_arg1451z00_4490;

							BgL_arg1450z00_4489 = CAR(BgL_cdrzd2201zd2_4488);
							BgL_arg1451z00_4490 = CDR(BgL_cdrzd2201zd2_4488);
							{	/* Eval/expanders.scm 238 */
								obj_t BgL_arg1452z00_4491;

								{	/* Eval/expanders.scm 238 */
									obj_t BgL_arg1453z00_4492;

									{	/* Eval/expanders.scm 238 */
										obj_t BgL_arg1454z00_4493;

										{	/* Eval/expanders.scm 238 */
											obj_t BgL_arg1455z00_4494;
											obj_t BgL_arg1456z00_4495;

											{	/* Eval/expanders.scm 238 */
												obj_t BgL_arg1457z00_4496;

												BgL_arg1457z00_4496 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1451z00_4490, BNIL);
												BgL_arg1455z00_4494 =
													MAKE_YOUNG_PAIR
													(BGl_symbol2363z00zz__install_expandersz00,
													BgL_arg1457z00_4496);
											}
											BgL_arg1456z00_4495 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											BgL_arg1454z00_4493 =
												MAKE_YOUNG_PAIR(BgL_arg1455z00_4494,
												BgL_arg1456z00_4495);
										}
										BgL_arg1453z00_4492 =
											MAKE_YOUNG_PAIR(BgL_arg1450z00_4489, BgL_arg1454z00_4493);
									}
									BgL_arg1452z00_4491 =
										MAKE_YOUNG_PAIR(BGl_symbol2308z00zz__install_expandersz00,
										BgL_arg1453z00_4492);
								}
								return
									BGL_PROCEDURE_CALL2(BgL_ez00_3989, BgL_arg1452z00_4491,
									BgL_ez00_3989);
							}
						}
					else
						{	/* Eval/expanders.scm 234 */
							return
								BGl_expandzd2errorzd2zz__install_expandersz00
								(BGl_string2373z00zz__install_expandersz00,
								BGl_string2467z00zz__install_expandersz00, BgL_xz00_3988);
						}
				}
			else
				{	/* Eval/expanders.scm 234 */
					return
						BGl_expandzd2errorzd2zz__install_expandersz00
						(BGl_string2373z00zz__install_expandersz00,
						BGl_string2467z00zz__install_expandersz00, BgL_xz00_3988);
				}
		}

	}



/* &<@anonymous:1435> */
	obj_t BGl_z62zc3z04anonymousza31435ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3990, obj_t BgL_xz00_3991, obj_t BgL_ez00_3992)
	{
		{	/* Eval/expanders.scm 223 */
			if (PAIRP(BgL_xz00_3991))
				{	/* Eval/expanders.scm 223 */
					obj_t BgL_cdrzd2180zd2_4497;

					BgL_cdrzd2180zd2_4497 = CDR(((obj_t) BgL_xz00_3991));
					if (PAIRP(BgL_cdrzd2180zd2_4497))
						{	/* Eval/expanders.scm 223 */
							obj_t BgL_cdrzd2185zd2_4498;

							BgL_cdrzd2185zd2_4498 = CDR(BgL_cdrzd2180zd2_4497);
							if (PAIRP(BgL_cdrzd2185zd2_4498))
								{	/* Eval/expanders.scm 223 */
									obj_t BgL_arg1439z00_4499;
									obj_t BgL_arg1440z00_4500;
									obj_t BgL_arg1441z00_4501;

									BgL_arg1439z00_4499 = CAR(BgL_cdrzd2180zd2_4497);
									BgL_arg1440z00_4500 = CAR(BgL_cdrzd2185zd2_4498);
									BgL_arg1441z00_4501 = CDR(BgL_cdrzd2185zd2_4498);
									{	/* Eval/expanders.scm 226 */
										obj_t BgL_arg1442z00_4502;

										{	/* Eval/expanders.scm 226 */
											obj_t BgL_arg1443z00_4503;

											{	/* Eval/expanders.scm 226 */
												obj_t BgL_arg1444z00_4504;

												BgL_arg1444z00_4504 =
													MAKE_YOUNG_PAIR(BgL_arg1440z00_4500,
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1441z00_4501, BNIL));
												BgL_arg1443z00_4503 =
													MAKE_YOUNG_PAIR(BgL_arg1439z00_4499,
													BgL_arg1444z00_4504);
											}
											BgL_arg1442z00_4502 =
												MAKE_YOUNG_PAIR
												(BGl_symbol2412z00zz__install_expandersz00,
												BgL_arg1443z00_4503);
										}
										return
											BGL_PROCEDURE_CALL2(BgL_ez00_3992, BgL_arg1442z00_4502,
											BgL_ez00_3992);
									}
								}
							else
								{	/* Eval/expanders.scm 223 */
									return
										BGl_expandzd2errorzd2zz__install_expandersz00
										(BGl_string2370z00zz__install_expandersz00,
										BGl_string2467z00zz__install_expandersz00, BgL_xz00_3991);
								}
						}
					else
						{	/* Eval/expanders.scm 223 */
							return
								BGl_expandzd2errorzd2zz__install_expandersz00
								(BGl_string2370z00zz__install_expandersz00,
								BGl_string2467z00zz__install_expandersz00, BgL_xz00_3991);
						}
				}
			else
				{	/* Eval/expanders.scm 223 */
					return
						BGl_expandzd2errorzd2zz__install_expandersz00
						(BGl_string2370z00zz__install_expandersz00,
						BGl_string2467z00zz__install_expandersz00, BgL_xz00_3991);
				}
		}

	}



/* &<@anonymous:1415> */
	obj_t BGl_z62zc3z04anonymousza31415ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3993, obj_t BgL_xz00_3994, obj_t BgL_ez00_3995)
	{
		{	/* Eval/expanders.scm 210 */
			if (PAIRP(BgL_xz00_3994))
				{	/* Eval/expanders.scm 210 */
					obj_t BgL_cdrzd2154zd2_4505;

					BgL_cdrzd2154zd2_4505 = CDR(((obj_t) BgL_xz00_3994));
					if (PAIRP(BgL_cdrzd2154zd2_4505))
						{	/* Eval/expanders.scm 210 */
							obj_t BgL_cdrzd2159zd2_4506;

							BgL_cdrzd2159zd2_4506 = CDR(BgL_cdrzd2154zd2_4505);
							if (PAIRP(BgL_cdrzd2159zd2_4506))
								{	/* Eval/expanders.scm 210 */
									obj_t BgL_cdrzd2164zd2_4507;

									BgL_cdrzd2164zd2_4507 = CDR(BgL_cdrzd2159zd2_4506);
									if (PAIRP(BgL_cdrzd2164zd2_4507))
										{	/* Eval/expanders.scm 210 */
											if (NULLP(CDR(BgL_cdrzd2164zd2_4507)))
												{	/* Eval/expanders.scm 210 */
													obj_t BgL_arg1422z00_4508;
													obj_t BgL_arg1423z00_4509;
													obj_t BgL_arg1424z00_4510;

													BgL_arg1422z00_4508 = CAR(BgL_cdrzd2154zd2_4505);
													BgL_arg1423z00_4509 = CAR(BgL_cdrzd2159zd2_4506);
													BgL_arg1424z00_4510 = CAR(BgL_cdrzd2164zd2_4507);
													{	/* Eval/expanders.scm 213 */
														obj_t BgL_arg1426z00_4511;

														{	/* Eval/expanders.scm 213 */
															obj_t BgL_arg1427z00_4512;
															obj_t BgL_arg1428z00_4513;

															BgL_arg1427z00_4512 =
																BGL_PROCEDURE_CALL2(BgL_ez00_3995,
																BgL_arg1422z00_4508, BgL_ez00_3995);
															{	/* Eval/expanders.scm 214 */
																obj_t BgL_arg1429z00_4514;
																obj_t BgL_arg1430z00_4515;

																BgL_arg1429z00_4514 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_3995,
																	BgL_arg1423z00_4509, BgL_ez00_3995);
																{	/* Eval/expanders.scm 215 */
																	obj_t BgL_arg1431z00_4516;

																	BgL_arg1431z00_4516 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_3995,
																		BgL_arg1424z00_4510, BgL_ez00_3995);
																	BgL_arg1430z00_4515 =
																		MAKE_YOUNG_PAIR(BgL_arg1431z00_4516, BNIL);
																}
																BgL_arg1428z00_4513 =
																	MAKE_YOUNG_PAIR(BgL_arg1429z00_4514,
																	BgL_arg1430z00_4515);
															}
															BgL_arg1426z00_4511 =
																MAKE_YOUNG_PAIR(BgL_arg1427z00_4512,
																BgL_arg1428z00_4513);
														}
														return
															MAKE_YOUNG_PAIR
															(BGl_symbol2366z00zz__install_expandersz00,
															BgL_arg1426z00_4511);
													}
												}
											else
												{	/* Eval/expanders.scm 210 */
													return
														BGl_expandzd2errorzd2zz__install_expandersz00
														(BGl_string2367z00zz__install_expandersz00,
														BGl_string2486z00zz__install_expandersz00,
														BgL_xz00_3994);
												}
										}
									else
										{	/* Eval/expanders.scm 210 */
											return
												BGl_expandzd2errorzd2zz__install_expandersz00
												(BGl_string2367z00zz__install_expandersz00,
												BGl_string2486z00zz__install_expandersz00,
												BgL_xz00_3994);
										}
								}
							else
								{	/* Eval/expanders.scm 210 */
									return
										BGl_expandzd2errorzd2zz__install_expandersz00
										(BGl_string2367z00zz__install_expandersz00,
										BGl_string2486z00zz__install_expandersz00, BgL_xz00_3994);
								}
						}
					else
						{	/* Eval/expanders.scm 210 */
							return
								BGl_expandzd2errorzd2zz__install_expandersz00
								(BGl_string2367z00zz__install_expandersz00,
								BGl_string2486z00zz__install_expandersz00, BgL_xz00_3994);
						}
				}
			else
				{	/* Eval/expanders.scm 210 */
					return
						BGl_expandzd2errorzd2zz__install_expandersz00
						(BGl_string2367z00zz__install_expandersz00,
						BGl_string2486z00zz__install_expandersz00, BgL_xz00_3994);
				}
		}

	}



/* &<@anonymous:1401> */
	obj_t BGl_z62zc3z04anonymousza31401ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3996, obj_t BgL_xz00_3997, obj_t BgL_ez00_3998)
	{
		{	/* Eval/expanders.scm 191 */
			{
				obj_t BgL_expz00_4518;

				if (PAIRP(BgL_xz00_3997))
					{	/* Eval/expanders.scm 191 */
						obj_t BgL_cdrzd2139zd2_4524;

						BgL_cdrzd2139zd2_4524 = CDR(((obj_t) BgL_xz00_3997));
						if (PAIRP(BgL_cdrzd2139zd2_4524))
							{	/* Eval/expanders.scm 191 */
								if (NULLP(CDR(BgL_cdrzd2139zd2_4524)))
									{	/* Eval/expanders.scm 191 */
										BgL_expz00_4518 = CAR(BgL_cdrzd2139zd2_4524);
										{	/* Eval/expanders.scm 194 */
											obj_t BgL_arg1408z00_4519;

											{	/* Eval/expanders.scm 194 */
												obj_t BgL_arg1410z00_4520;

												{	/* Eval/expanders.scm 194 */
													obj_t BgL_arg1411z00_4521;

													{	/* Eval/expanders.scm 194 */
														obj_t BgL_arg1412z00_4522;

														{	/* Eval/expanders.scm 194 */
															obj_t BgL_arg1413z00_4523;

															BgL_arg1413z00_4523 =
																BGL_PROCEDURE_CALL2(BgL_ez00_3998,
																BgL_expz00_4518, BgL_ez00_3998);
															BgL_arg1412z00_4522 =
																MAKE_YOUNG_PAIR(BgL_arg1413z00_4523, BNIL);
														}
														BgL_arg1411z00_4521 =
															MAKE_YOUNG_PAIR(BNIL, BgL_arg1412z00_4522);
													}
													BgL_arg1410z00_4520 =
														MAKE_YOUNG_PAIR
														(BGl_symbol2310z00zz__install_expandersz00,
														BgL_arg1411z00_4521);
												}
												BgL_arg1408z00_4519 =
													MAKE_YOUNG_PAIR(BgL_arg1410z00_4520, BNIL);
											}
											return
												MAKE_YOUNG_PAIR
												(BGl_symbol2487z00zz__install_expandersz00,
												BgL_arg1408z00_4519);
										}
									}
								else
									{	/* Eval/expanders.scm 191 */
										return
											BGl_expandzd2errorzd2zz__install_expandersz00
											(BGl_string2356z00zz__install_expandersz00,
											BGl_string2467z00zz__install_expandersz00, BgL_xz00_3997);
									}
							}
						else
							{	/* Eval/expanders.scm 191 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2356z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_3997);
							}
					}
				else
					{	/* Eval/expanders.scm 191 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2356z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_3997);
					}
			}
		}

	}



/* &<@anonymous:1398> */
	obj_t BGl_z62zc3z04anonymousza31398ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_3999, obj_t BgL_xz00_4000, obj_t BgL_ez00_4001)
	{
		{	/* Eval/expanders.scm 187 */
			{	/* Eval/expanders.scm 188 */
				obj_t BgL_arg1399z00_4525;

				BgL_arg1399z00_4525 =
					BGl_expandzd2definezd2patternz00zz__evalz00(BgL_xz00_4000);
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4001, BgL_arg1399z00_4525,
					BgL_ez00_4001);
			}
		}

	}



/* &<@anonymous:1395> */
	obj_t BGl_z62zc3z04anonymousza31395ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4002, obj_t BgL_xz00_4003, obj_t BgL_ez00_4004)
	{
		{	/* Eval/expanders.scm 183 */
			{	/* Eval/expanders.scm 184 */
				obj_t BgL_arg1396z00_4526;

				BgL_arg1396z00_4526 =
					BGl_expandzd2matchzd2lambdaz00zz__match_expandz00(BgL_xz00_4003);
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4004, BgL_arg1396z00_4526,
					BgL_ez00_4004);
			}
		}

	}



/* &<@anonymous:1392> */
	obj_t BGl_z62zc3z04anonymousza31392ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4005, obj_t BgL_xz00_4006, obj_t BgL_ez00_4007)
	{
		{	/* Eval/expanders.scm 180 */
			{	/* Eval/expanders.scm 180 */
				obj_t BgL_arg1393z00_4527;

				BgL_arg1393z00_4527 =
					BGl_expandzd2matchzd2casez00zz__match_expandz00(BgL_xz00_4006);
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4007, BgL_arg1393z00_4527,
					BgL_ez00_4007);
			}
		}

	}



/* &<@anonymous:1390> */
	obj_t BGl_z62zc3z04anonymousza31390ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4008, obj_t BgL_xz00_4009, obj_t BgL_ez00_4010)
	{
		{	/* Eval/expanders.scm 177 */
			return
				BGl_expandzd2tryzd2zz__expander_tryz00(BgL_xz00_4009, BgL_ez00_4010);
		}

	}



/* &<@anonymous:1388> */
	obj_t BGl_z62zc3z04anonymousza31388ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4011, obj_t BgL_xz00_4012, obj_t BgL_ez00_4013)
	{
		{	/* Eval/expanders.scm 174 */
			return BGl_expandzd2dozd2zz__expander_doz00(BgL_xz00_4012, BgL_ez00_4013);
		}

	}



/* &<@anonymous:1383> */
	obj_t BGl_z62zc3z04anonymousza31383ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4014, obj_t BgL_xz00_4015, obj_t BgL_ez00_4016)
	{
		{	/* Eval/expanders.scm 171 */
			{	/* Eval/expanders.scm 171 */
				obj_t BgL_arg1384z00_4528;

				BgL_arg1384z00_4528 =
					BGl_expandzd2condzd2zz__expander_boolz00(BgL_xz00_4015);
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4016, BgL_arg1384z00_4528,
					BgL_ez00_4016);
			}
		}

	}



/* &<@anonymous:1380> */
	obj_t BGl_z62zc3z04anonymousza31380ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4017, obj_t BgL_xz00_4018, obj_t BgL_ez00_4019)
	{
		{	/* Eval/expanders.scm 167 */
			return
				BGl_expandzd2definezd2expanderz00zz__evalz00(BgL_xz00_4018,
				BgL_ez00_4019);
		}

	}



/* &<@anonymous:1378> */
	obj_t BGl_z62zc3z04anonymousza31378ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4020, obj_t BgL_xz00_4021, obj_t BgL_ez00_4022)
	{
		{	/* Eval/expanders.scm 163 */
			return
				BGl_expandzd2definezd2hygienezd2macrozd2zz__evalz00(BgL_xz00_4021,
				BgL_ez00_4022);
		}

	}



/* &<@anonymous:1376> */
	obj_t BGl_z62zc3z04anonymousza31376ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4023, obj_t BgL_xz00_4024, obj_t BgL_ez00_4025)
	{
		{	/* Eval/expanders.scm 159 */
			return
				BGl_expandzd2definezd2macroz00zz__evalz00(BgL_xz00_4024, BgL_ez00_4025);
		}

	}



/* &<@anonymous:1374> */
	obj_t BGl_z62zc3z04anonymousza31374ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4026, obj_t BgL_xz00_4027, obj_t BgL_ez00_4028)
	{
		{	/* Eval/expanders.scm 156 */
			return BgL_xz00_4027;
		}

	}



/* &<@anonymous:1371> */
	obj_t BGl_z62zc3z04anonymousza31371ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4029, obj_t BgL_xz00_4030, obj_t BgL_ez00_4031)
	{
		{	/* Eval/expanders.scm 153 */
			{	/* Eval/expanders.scm 153 */
				obj_t BgL_arg1372z00_4529;

				BgL_arg1372z00_4529 =
					BGl_quasiquotationz00zz__expander_quotez00(BINT(1L), BgL_xz00_4030);
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4031, BgL_arg1372z00_4529,
					BgL_ez00_4031);
			}
		}

	}



/* &<@anonymous:1348> */
	obj_t BGl_z62zc3z04anonymousza31348ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4032, obj_t BgL_xz00_4033, obj_t BgL_ez00_4034)
	{
		{	/* Eval/expanders.scm 138 */
			{	/* Eval/expanders.scm 139 */
				bool_t BgL_test2712z00_5529;

				{
					obj_t BgL_l1087z00_4531;

					BgL_l1087z00_4531 = BgL_xz00_4033;
				BgL_zc3z04anonymousza31367ze3z87_4530:
					if (NULLP(BgL_l1087z00_4531))
						{	/* Eval/expanders.scm 139 */
							BgL_test2712z00_5529 = ((bool_t) 1);
						}
					else
						{	/* Eval/expanders.scm 139 */
							bool_t BgL_test2714z00_5532;

							{	/* Eval/expanders.scm 139 */
								obj_t BgL_tmpz00_5533;

								BgL_tmpz00_5533 = CAR(((obj_t) BgL_l1087z00_4531));
								BgL_test2714z00_5532 = SYMBOLP(BgL_tmpz00_5533);
							}
							if (BgL_test2714z00_5532)
								{
									obj_t BgL_l1087z00_5537;

									BgL_l1087z00_5537 = CDR(((obj_t) BgL_l1087z00_4531));
									BgL_l1087z00_4531 = BgL_l1087z00_5537;
									goto BgL_zc3z04anonymousza31367ze3z87_4530;
								}
							else
								{	/* Eval/expanders.scm 139 */
									BgL_test2712z00_5529 = ((bool_t) 0);
								}
						}
				}
				if (BgL_test2712z00_5529)
					{
						obj_t BgL_vz00_4533;
						obj_t BgL_restz00_4534;

						if (PAIRP(BgL_xz00_4033))
							{	/* Eval/expanders.scm 140 */
								obj_t BgL_cdrzd2118zd2_4541;

								BgL_cdrzd2118zd2_4541 = CDR(((obj_t) BgL_xz00_4033));
								if (PAIRP(BgL_cdrzd2118zd2_4541))
									{	/* Eval/expanders.scm 140 */
										BgL_vz00_4533 = CAR(BgL_cdrzd2118zd2_4541);
										BgL_restz00_4534 = CDR(BgL_cdrzd2118zd2_4541);
										{	/* Eval/expanders.scm 142 */
											obj_t BgL_nvz00_4535;

											BgL_nvz00_4535 =
												BGL_PROCEDURE_CALL2(BgL_ez00_4034, BgL_vz00_4533,
												BgL_ez00_4034);
											if (PAIRP(BgL_nvz00_4535))
												{	/* Eval/expanders.scm 143 */
													if (
														(CAR(
																((obj_t) BgL_nvz00_4535)) ==
															BGl_symbol2319z00zz__install_expandersz00))
														{	/* Eval/expanders.scm 143 */
															obj_t BgL_arg1361z00_4536;

															BgL_arg1361z00_4536 =
																CDR(((obj_t) BgL_nvz00_4535));
															{	/* Eval/expanders.scm 145 */
																obj_t BgL_arg1363z00_4537;

																{	/* Eval/expanders.scm 145 */
																	obj_t BgL_arg1364z00_4538;

																	BgL_arg1364z00_4538 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_arg1361z00_4536,
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_restz00_4534, BNIL));
																	BgL_arg1363z00_4537 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2319z00zz__install_expandersz00,
																		BgL_arg1364z00_4538);
																}
																return
																	BGl_evepairifyz00zz__prognz00
																	(BgL_arg1363z00_4537, BgL_xz00_4033);
															}
														}
													else
														{	/* Eval/expanders.scm 143 */
															{	/* Eval/expanders.scm 147 */
																obj_t BgL_arg1366z00_4539;

																BgL_arg1366z00_4539 =
																	CDR(((obj_t) BgL_xz00_4033));
																{	/* Eval/expanders.scm 147 */
																	obj_t BgL_tmpz00_5565;

																	BgL_tmpz00_5565 =
																		((obj_t) BgL_arg1366z00_4539);
																	SET_CAR(BgL_tmpz00_5565, BgL_nvz00_4535);
																}
															}
															return BgL_xz00_4033;
														}
												}
											else
												{	/* Eval/expanders.scm 143 */
													{	/* Eval/expanders.scm 147 */
														obj_t BgL_arg1366z00_4540;

														BgL_arg1366z00_4540 = CDR(((obj_t) BgL_xz00_4033));
														{	/* Eval/expanders.scm 147 */
															obj_t BgL_tmpz00_5570;

															BgL_tmpz00_5570 = ((obj_t) BgL_arg1366z00_4540);
															SET_CAR(BgL_tmpz00_5570, BgL_nvz00_4535);
														}
													}
													return BgL_xz00_4033;
												}
										}
									}
								else
									{	/* Eval/expanders.scm 140 */
										return
											BGl_expandzd2errorzd2zz__install_expandersz00
											(BGl_string2320z00zz__install_expandersz00,
											BGl_string2467z00zz__install_expandersz00, BgL_xz00_4033);
									}
							}
						else
							{	/* Eval/expanders.scm 140 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2320z00zz__install_expandersz00,
									BGl_string2467z00zz__install_expandersz00, BgL_xz00_4033);
							}
					}
				else
					{	/* Eval/expanders.scm 95 */
						obj_t BgL_locz00_4542;

						if (EPAIRP(BgL_xz00_4033))
							{	/* Eval/expanders.scm 95 */
								BgL_locz00_4542 = CER(((obj_t) BgL_xz00_4033));
							}
						else
							{	/* Eval/expanders.scm 95 */
								BgL_locz00_4542 = BFALSE;
							}
						{	/* Eval/expanders.scm 96 */
							bool_t BgL_test2720z00_5581;

							if (PAIRP(BgL_locz00_4542))
								{	/* Eval/expanders.scm 96 */
									bool_t BgL_test2722z00_5584;

									{	/* Eval/expanders.scm 96 */
										obj_t BgL_tmpz00_5585;

										BgL_tmpz00_5585 = CDR(BgL_locz00_4542);
										BgL_test2722z00_5584 = PAIRP(BgL_tmpz00_5585);
									}
									if (BgL_test2722z00_5584)
										{	/* Eval/expanders.scm 96 */
											obj_t BgL_tmpz00_5588;

											BgL_tmpz00_5588 = CDR(CDR(BgL_locz00_4542));
											BgL_test2720z00_5581 = PAIRP(BgL_tmpz00_5588);
										}
									else
										{	/* Eval/expanders.scm 96 */
											BgL_test2720z00_5581 = ((bool_t) 0);
										}
								}
							else
								{	/* Eval/expanders.scm 96 */
									BgL_test2720z00_5581 = ((bool_t) 0);
								}
							if (BgL_test2720z00_5581)
								{	/* Eval/expanders.scm 96 */
									return
										BGl_errorzf2locationzf2zz__errorz00
										(BGl_string2320z00zz__install_expandersz00,
										BGl_string2467z00zz__install_expandersz00, BgL_xz00_4033,
										CAR(CDR(BgL_locz00_4542)), CAR(CDR(CDR(BgL_locz00_4542))));
								}
							else
								{	/* Eval/expanders.scm 96 */
									return
										BGl_errorz00zz__errorz00
										(BGl_string2320z00zz__install_expandersz00,
										BGl_string2467z00zz__install_expandersz00, BgL_xz00_4033);
								}
						}
					}
			}
		}

	}



/* &<@anonymous:1330> */
	obj_t BGl_z62zc3z04anonymousza31330ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4035, obj_t BgL_xz00_4036, obj_t BgL_ez00_4037)
	{
		{	/* Eval/expanders.scm 131 */
			if (PAIRP(BgL_xz00_4036))
				{	/* Eval/expanders.scm 131 */
					obj_t BgL_cdrzd2105zd2_4543;

					BgL_cdrzd2105zd2_4543 = CDR(((obj_t) BgL_xz00_4036));
					if (
						(CAR(
								((obj_t) BgL_xz00_4036)) ==
							BGl_symbol2316z00zz__install_expandersz00))
						{	/* Eval/expanders.scm 131 */
							if (PAIRP(BgL_cdrzd2105zd2_4543))
								{	/* Eval/expanders.scm 131 */
									obj_t BgL_cdrzd2107zd2_4544;

									BgL_cdrzd2107zd2_4544 = CDR(BgL_cdrzd2105zd2_4543);
									{	/* Eval/expanders.scm 131 */
										bool_t BgL_test2726z00_5610;

										{	/* Eval/expanders.scm 131 */
											obj_t BgL_tmpz00_5611;

											BgL_tmpz00_5611 = CAR(BgL_cdrzd2105zd2_4543);
											BgL_test2726z00_5610 = SYMBOLP(BgL_tmpz00_5611);
										}
										if (BgL_test2726z00_5610)
											{	/* Eval/expanders.scm 131 */
												if (PAIRP(BgL_cdrzd2107zd2_4544))
													{	/* Eval/expanders.scm 131 */
														bool_t BgL_test2728z00_5616;

														{	/* Eval/expanders.scm 131 */
															obj_t BgL_tmpz00_5617;

															BgL_tmpz00_5617 = CAR(BgL_cdrzd2107zd2_4544);
															BgL_test2728z00_5616 = SYMBOLP(BgL_tmpz00_5617);
														}
														if (BgL_test2728z00_5616)
															{	/* Eval/expanders.scm 131 */
																if (NULLP(CDR(BgL_cdrzd2107zd2_4544)))
																	{	/* Eval/expanders.scm 131 */
																		return BgL_xz00_4036;
																	}
																else
																	{	/* Eval/expanders.scm 131 */
																		return
																			BGl_expandzd2errorzd2zz__install_expandersz00
																			(BGl_string2317z00zz__install_expandersz00,
																			BGl_string2467z00zz__install_expandersz00,
																			BgL_xz00_4036);
																	}
															}
														else
															{	/* Eval/expanders.scm 131 */
																return
																	BGl_expandzd2errorzd2zz__install_expandersz00
																	(BGl_string2317z00zz__install_expandersz00,
																	BGl_string2467z00zz__install_expandersz00,
																	BgL_xz00_4036);
															}
													}
												else
													{	/* Eval/expanders.scm 131 */
														return
															BGl_expandzd2errorzd2zz__install_expandersz00
															(BGl_string2317z00zz__install_expandersz00,
															BGl_string2467z00zz__install_expandersz00,
															BgL_xz00_4036);
													}
											}
										else
											{	/* Eval/expanders.scm 131 */
												return
													BGl_expandzd2errorzd2zz__install_expandersz00
													(BGl_string2317z00zz__install_expandersz00,
													BGl_string2467z00zz__install_expandersz00,
													BgL_xz00_4036);
											}
									}
								}
							else
								{	/* Eval/expanders.scm 131 */
									return
										BGl_expandzd2errorzd2zz__install_expandersz00
										(BGl_string2317z00zz__install_expandersz00,
										BGl_string2467z00zz__install_expandersz00, BgL_xz00_4036);
								}
						}
					else
						{	/* Eval/expanders.scm 131 */
							return
								BGl_expandzd2errorzd2zz__install_expandersz00
								(BGl_string2317z00zz__install_expandersz00,
								BGl_string2467z00zz__install_expandersz00, BgL_xz00_4036);
						}
				}
			else
				{	/* Eval/expanders.scm 131 */
					return
						BGl_expandzd2errorzd2zz__install_expandersz00
						(BGl_string2317z00zz__install_expandersz00,
						BGl_string2467z00zz__install_expandersz00, BgL_xz00_4036);
				}
		}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zz__install_expandersz00(obj_t
		BgL_envz00_4038, obj_t BgL_xz00_4039, obj_t BgL_ez00_4040)
	{
		{	/* Eval/expanders.scm 128 */
			return
				BGl_expandzd2quotezd2zz__expander_quotez00(BgL_xz00_4039,
				BgL_ez00_4040);
		}

	}



/* make-if~0 */
	obj_t BGl_makezd2ifze70z35zz__install_expandersz00(obj_t BgL_testz00_2011,
		obj_t BgL_thenz00_2012, obj_t BgL_elsez00_2013)
	{
		{	/* Eval/expanders.scm 503 */
			{

				if (PAIRP(BgL_testz00_2011))
					{	/* Eval/expanders.scm 503 */
						obj_t BgL_cdrzd2562zd2_2020;

						BgL_cdrzd2562zd2_2020 = CDR(((obj_t) BgL_testz00_2011));
						if (
							(CAR(
									((obj_t) BgL_testz00_2011)) ==
								BGl_symbol2489z00zz__install_expandersz00))
							{	/* Eval/expanders.scm 503 */
								if (PAIRP(BgL_cdrzd2562zd2_2020))
									{	/* Eval/expanders.scm 503 */
										if (NULLP(CDR(BgL_cdrzd2562zd2_2020)))
											{	/* Eval/expanders.scm 503 */
												obj_t BgL_arg1850z00_2026;

												BgL_arg1850z00_2026 = CAR(BgL_cdrzd2562zd2_2020);
												{	/* Eval/expanders.scm 501 */
													obj_t BgL_arg1853z00_3419;

													{	/* Eval/expanders.scm 501 */
														obj_t BgL_arg1854z00_3420;

														{	/* Eval/expanders.scm 501 */
															obj_t BgL_arg1856z00_3421;

															BgL_arg1856z00_3421 =
																MAKE_YOUNG_PAIR(BgL_thenz00_2012, BNIL);
															BgL_arg1854z00_3420 =
																MAKE_YOUNG_PAIR(BgL_elsez00_2013,
																BgL_arg1856z00_3421);
														}
														BgL_arg1853z00_3419 =
															MAKE_YOUNG_PAIR(BgL_arg1850z00_2026,
															BgL_arg1854z00_3420);
													}
													return
														MAKE_YOUNG_PAIR
														(BGl_symbol2308z00zz__install_expandersz00,
														BgL_arg1853z00_3419);
												}
											}
										else
											{	/* Eval/expanders.scm 503 */
											BgL_tagzd2557zd2_2017:
												{	/* Eval/expanders.scm 503 */
													obj_t BgL_arg1857z00_2032;

													{	/* Eval/expanders.scm 503 */
														obj_t BgL_arg1858z00_2033;

														{	/* Eval/expanders.scm 503 */
															obj_t BgL_arg1859z00_2034;

															BgL_arg1859z00_2034 =
																MAKE_YOUNG_PAIR(BgL_elsez00_2013, BNIL);
															BgL_arg1858z00_2033 =
																MAKE_YOUNG_PAIR(BgL_thenz00_2012,
																BgL_arg1859z00_2034);
														}
														BgL_arg1857z00_2032 =
															MAKE_YOUNG_PAIR(BgL_testz00_2011,
															BgL_arg1858z00_2033);
													}
													return
														MAKE_YOUNG_PAIR
														(BGl_symbol2308z00zz__install_expandersz00,
														BgL_arg1857z00_2032);
												}
											}
									}
								else
									{	/* Eval/expanders.scm 503 */
										goto BgL_tagzd2557zd2_2017;
									}
							}
						else
							{	/* Eval/expanders.scm 503 */
								goto BgL_tagzd2557zd2_2017;
							}
					}
				else
					{	/* Eval/expanders.scm 503 */
						goto BgL_tagzd2557zd2_2017;
					}
			}
		}

	}



/* &expand-if */
	obj_t BGl_z62expandzd2ifzb0zz__install_expandersz00(obj_t BgL_envz00_4074,
		obj_t BgL_xz00_4075, obj_t BgL_ez00_4076)
	{
		{	/* Eval/expanders.scm 496 */
			if (PAIRP(BgL_xz00_4075))
				{	/* Eval/expanders.scm 505 */
					obj_t BgL_cdrzd2578zd2_4545;

					BgL_cdrzd2578zd2_4545 = CDR(((obj_t) BgL_xz00_4075));
					if (
						(CAR(
								((obj_t) BgL_xz00_4075)) ==
							BGl_symbol2308z00zz__install_expandersz00))
						{	/* Eval/expanders.scm 505 */
							if (PAIRP(BgL_cdrzd2578zd2_4545))
								{	/* Eval/expanders.scm 505 */
									obj_t BgL_cdrzd2583zd2_4546;

									BgL_cdrzd2583zd2_4546 = CDR(BgL_cdrzd2578zd2_4545);
									if (PAIRP(BgL_cdrzd2583zd2_4546))
										{	/* Eval/expanders.scm 505 */
											obj_t BgL_cdrzd2588zd2_4547;

											BgL_cdrzd2588zd2_4547 = CDR(BgL_cdrzd2583zd2_4546);
											if (PAIRP(BgL_cdrzd2588zd2_4547))
												{	/* Eval/expanders.scm 505 */
													if (NULLP(CDR(BgL_cdrzd2588zd2_4547)))
														{	/* Eval/expanders.scm 505 */
															obj_t BgL_arg1826z00_4548;
															obj_t BgL_arg1827z00_4549;
															obj_t BgL_arg1828z00_4550;

															BgL_arg1826z00_4548 = CAR(BgL_cdrzd2578zd2_4545);
															BgL_arg1827z00_4549 = CAR(BgL_cdrzd2583zd2_4546);
															BgL_arg1828z00_4550 = CAR(BgL_cdrzd2588zd2_4547);
															{	/* Eval/expanders.scm 507 */
																obj_t BgL_nxz00_4551;

																{	/* Eval/expanders.scm 507 */
																	obj_t BgL_arg1837z00_4552;
																	obj_t BgL_arg1838z00_4553;
																	obj_t BgL_arg1839z00_4554;

																	BgL_arg1837z00_4552 =
																		BGl_expandzd2testzd2zz__install_expandersz00
																		(BgL_arg1826z00_4548, BgL_ez00_4076);
																	BgL_arg1838z00_4553 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_4076,
																		BgL_arg1827z00_4549, BgL_ez00_4076);
																	BgL_arg1839z00_4554 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_4076,
																		BgL_arg1828z00_4550, BgL_ez00_4076);
																	BgL_nxz00_4551 =
																		BGl_makezd2ifze70z35zz__install_expandersz00
																		(BgL_arg1837z00_4552, BgL_arg1838z00_4553,
																		BgL_arg1839z00_4554);
																}
																return
																	BGl_evepairifyzd2deepzd2zz__prognz00
																	(BgL_nxz00_4551, BgL_xz00_4075);
															}
														}
													else
														{	/* Eval/expanders.scm 505 */
															return
																BGl_expandzd2errorzd2zz__install_expandersz00
																(BGl_string2309z00zz__install_expandersz00,
																BGl_string2467z00zz__install_expandersz00,
																BgL_xz00_4075);
														}
												}
											else
												{	/* Eval/expanders.scm 505 */
													obj_t BgL_cdrzd2613zd2_4555;

													BgL_cdrzd2613zd2_4555 =
														CDR(((obj_t) BgL_cdrzd2578zd2_4545));
													if (NULLP(CDR(((obj_t) BgL_cdrzd2613zd2_4555))))
														{	/* Eval/expanders.scm 505 */
															obj_t BgL_arg1833z00_4556;
															obj_t BgL_arg1834z00_4557;

															BgL_arg1833z00_4556 =
																CAR(((obj_t) BgL_cdrzd2578zd2_4545));
															BgL_arg1834z00_4557 =
																CAR(((obj_t) BgL_cdrzd2613zd2_4555));
															{	/* Eval/expanders.scm 510 */
																obj_t BgL_nxz00_4558;

																{	/* Eval/expanders.scm 510 */
																	obj_t BgL_arg1840z00_4559;
																	obj_t BgL_arg1842z00_4560;

																	BgL_arg1840z00_4559 =
																		BGl_expandzd2testzd2zz__install_expandersz00
																		(BgL_arg1833z00_4556, BgL_ez00_4076);
																	BgL_arg1842z00_4560 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_4076,
																		BgL_arg1834z00_4557, BgL_ez00_4076);
																	BgL_nxz00_4558 =
																		BGl_makezd2ifze70z35zz__install_expandersz00
																		(BgL_arg1840z00_4559, BgL_arg1842z00_4560,
																		BFALSE);
																}
																return
																	BGl_evepairifyzd2deepzd2zz__prognz00
																	(BgL_nxz00_4558, BgL_xz00_4075);
															}
														}
													else
														{	/* Eval/expanders.scm 505 */
															return
																BGl_expandzd2errorzd2zz__install_expandersz00
																(BGl_string2309z00zz__install_expandersz00,
																BGl_string2467z00zz__install_expandersz00,
																BgL_xz00_4075);
														}
												}
										}
									else
										{	/* Eval/expanders.scm 505 */
											return
												BGl_expandzd2errorzd2zz__install_expandersz00
												(BGl_string2309z00zz__install_expandersz00,
												BGl_string2467z00zz__install_expandersz00,
												BgL_xz00_4075);
										}
								}
							else
								{	/* Eval/expanders.scm 505 */
									return
										BGl_expandzd2errorzd2zz__install_expandersz00
										(BGl_string2309z00zz__install_expandersz00,
										BGl_string2467z00zz__install_expandersz00, BgL_xz00_4075);
								}
						}
					else
						{	/* Eval/expanders.scm 505 */
							return
								BGl_expandzd2errorzd2zz__install_expandersz00
								(BGl_string2309z00zz__install_expandersz00,
								BGl_string2467z00zz__install_expandersz00, BgL_xz00_4075);
						}
				}
			else
				{	/* Eval/expanders.scm 505 */
					return
						BGl_expandzd2errorzd2zz__install_expandersz00
						(BGl_string2309z00zz__install_expandersz00,
						BGl_string2467z00zz__install_expandersz00, BgL_xz00_4075);
				}
		}

	}



/* &expand-tprint */
	obj_t BGl_z62expandzd2tprintzb0zz__install_expandersz00(obj_t BgL_envz00_4059,
		obj_t BgL_xz00_4060, obj_t BgL_ez00_4061)
	{
		{	/* Eval/expanders.scm 518 */
			{	/* Eval/expanders.scm 519 */
				obj_t BgL_objz00_4561;

				BgL_objz00_4561 = BGl_list2491z00zz__install_expandersz00;
				{	/* Eval/expanders.scm 519 */
					obj_t BgL_tmpz00_5712;

					BgL_tmpz00_5712 = ((obj_t) BgL_xz00_4060);
					SET_CAR(BgL_tmpz00_5712, BgL_objz00_4561);
				}
			}
			{	/* Eval/expanders.scm 520 */
				obj_t BgL_arg1860z00_4562;

				if (EPAIRP(BgL_xz00_4060))
					{
						obj_t BgL_namez00_4565;
						obj_t BgL_posz00_4566;

						{	/* Eval/expanders.scm 521 */
							obj_t BgL_ezd2641zd2_4577;

							BgL_ezd2641zd2_4577 = CER(((obj_t) BgL_xz00_4060));
							if (PAIRP(BgL_ezd2641zd2_4577))
								{	/* Eval/expanders.scm 521 */
									obj_t BgL_cdrzd2647zd2_4578;

									BgL_cdrzd2647zd2_4578 = CDR(BgL_ezd2641zd2_4577);
									if (
										(CAR(BgL_ezd2641zd2_4577) ==
											BGl_symbol2499z00zz__install_expandersz00))
										{	/* Eval/expanders.scm 521 */
											if (PAIRP(BgL_cdrzd2647zd2_4578))
												{	/* Eval/expanders.scm 521 */
													obj_t BgL_cdrzd2651zd2_4579;

													BgL_cdrzd2651zd2_4579 = CDR(BgL_cdrzd2647zd2_4578);
													if (PAIRP(BgL_cdrzd2651zd2_4579))
														{	/* Eval/expanders.scm 521 */
															if (NULLP(CDR(BgL_cdrzd2651zd2_4579)))
																{	/* Eval/expanders.scm 521 */
																	BgL_namez00_4565 = CAR(BgL_cdrzd2647zd2_4578);
																	BgL_posz00_4566 = CAR(BgL_cdrzd2651zd2_4579);
																	{	/* Eval/expanders.scm 525 */
																		obj_t BgL_arg1875z00_4567;

																		{	/* Eval/expanders.scm 525 */
																			obj_t BgL_arg1876z00_4568;

																			{	/* Eval/expanders.scm 525 */
																				obj_t BgL_arg1877z00_4569;
																				obj_t BgL_arg1878z00_4570;

																				BgL_arg1877z00_4569 =
																					BGl_relativezd2filezd2namez00zz__osz00
																					(BgL_namez00_4565,
																					BGl_pwdz00zz__osz00());
																				{	/* Eval/expanders.scm 527 */
																					obj_t BgL_arg1880z00_4571;

																					{	/* Eval/expanders.scm 527 */
																						obj_t BgL_arg1882z00_4572;
																						obj_t BgL_arg1883z00_4573;

																						BgL_arg1882z00_4572 =
																							BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00
																							(CINT(BgL_posz00_4566),
																							BgL_namez00_4565);
																						{	/* Eval/expanders.scm 529 */
																							obj_t BgL_arg1884z00_4574;

																							BgL_arg1884z00_4574 =
																								CDR(((obj_t) BgL_xz00_4060));
																							BgL_arg1883z00_4573 =
																								MAKE_YOUNG_PAIR
																								(BGl_string2494z00zz__install_expandersz00,
																								BgL_arg1884z00_4574);
																						}
																						BgL_arg1880z00_4571 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1882z00_4572,
																							BgL_arg1883z00_4573);
																					}
																					BgL_arg1878z00_4570 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2495z00zz__install_expandersz00,
																						BgL_arg1880z00_4571);
																				}
																				BgL_arg1876z00_4568 =
																					MAKE_YOUNG_PAIR(BgL_arg1877z00_4569,
																					BgL_arg1878z00_4570);
																			}
																			BgL_arg1875z00_4567 =
																				MAKE_YOUNG_PAIR
																				(BGl_list2496z00zz__install_expandersz00,
																				BgL_arg1876z00_4568);
																		}
																		{	/* Eval/expanders.scm 523 */
																			obj_t BgL_tmpz00_5744;

																			BgL_tmpz00_5744 = ((obj_t) BgL_xz00_4060);
																			SET_CDR(BgL_tmpz00_5744,
																				BgL_arg1875z00_4567);
																		}
																	}
																	BgL_arg1860z00_4562 = BgL_xz00_4060;
																}
															else
																{	/* Eval/expanders.scm 521 */
																BgL_tagzd2640zd2_4564:
																	{	/* Eval/expanders.scm 532 */
																		obj_t BgL_arg1885z00_4575;

																		{	/* Eval/expanders.scm 532 */
																			obj_t BgL_arg1887z00_4576;

																			BgL_arg1887z00_4576 =
																				CDR(((obj_t) BgL_xz00_4060));
																			BgL_arg1885z00_4575 =
																				MAKE_YOUNG_PAIR
																				(BGl_list2496z00zz__install_expandersz00,
																				BgL_arg1887z00_4576);
																		}
																		{	/* Eval/expanders.scm 532 */
																			obj_t BgL_tmpz00_5752;

																			BgL_tmpz00_5752 = ((obj_t) BgL_xz00_4060);
																			SET_CDR(BgL_tmpz00_5752,
																				BgL_arg1885z00_4575);
																		}
																	}
																	BgL_arg1860z00_4562 = BgL_xz00_4060;
																}
														}
													else
														{	/* Eval/expanders.scm 521 */
															goto BgL_tagzd2640zd2_4564;
														}
												}
											else
												{	/* Eval/expanders.scm 521 */
													goto BgL_tagzd2640zd2_4564;
												}
										}
									else
										{	/* Eval/expanders.scm 521 */
											goto BgL_tagzd2640zd2_4564;
										}
								}
							else
								{	/* Eval/expanders.scm 521 */
									goto BgL_tagzd2640zd2_4564;
								}
						}
					}
				else
					{	/* Eval/expanders.scm 520 */
						{	/* Eval/expanders.scm 535 */
							obj_t BgL_arg1888z00_4580;

							{	/* Eval/expanders.scm 535 */
								obj_t BgL_arg1889z00_4581;

								BgL_arg1889z00_4581 = CDR(((obj_t) BgL_xz00_4060));
								BgL_arg1888z00_4580 =
									MAKE_YOUNG_PAIR(BGl_list2496z00zz__install_expandersz00,
									BgL_arg1889z00_4581);
							}
							{	/* Eval/expanders.scm 535 */
								obj_t BgL_tmpz00_5758;

								BgL_tmpz00_5758 = ((obj_t) BgL_xz00_4060);
								SET_CDR(BgL_tmpz00_5758, BgL_arg1888z00_4580);
							}
						}
						BgL_arg1860z00_4562 = BgL_xz00_4060;
					}
				return
					BGL_PROCEDURE_CALL2(BgL_ez00_4061, BgL_arg1860z00_4562,
					BgL_ez00_4061);
			}
		}

	}



/* &expand-begin */
	obj_t BGl_z62expandzd2beginzb0zz__install_expandersz00(obj_t BgL_envz00_4050,
		obj_t BgL_xz00_4051, obj_t BgL_ez00_4052)
	{
		{	/* Eval/expanders.scm 542 */
			{
				obj_t BgL_bodyz00_4583;

				if (PAIRP(BgL_xz00_4051))
					{	/* Eval/expanders.scm 543 */
						obj_t BgL_arg1891z00_4594;

						BgL_arg1891z00_4594 = CDR(((obj_t) BgL_xz00_4051));
						BgL_bodyz00_4583 = BgL_arg1891z00_4594;
						if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_bodyz00_4583))
							{	/* Eval/expanders.scm 547 */
								obj_t BgL_arg1893z00_4584;

								if (NULLP(BgL_bodyz00_4583))
									{	/* Eval/expanders.scm 547 */
										BgL_arg1893z00_4584 = BNIL;
									}
								else
									{	/* Eval/expanders.scm 547 */
										obj_t BgL_head1139z00_4585;

										BgL_head1139z00_4585 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1137z00_4587;
											obj_t BgL_tail1140z00_4588;

											BgL_l1137z00_4587 = BgL_bodyz00_4583;
											BgL_tail1140z00_4588 = BgL_head1139z00_4585;
										BgL_zc3z04anonymousza31895ze3z87_4586:
											if (NULLP(BgL_l1137z00_4587))
												{	/* Eval/expanders.scm 547 */
													BgL_arg1893z00_4584 = CDR(BgL_head1139z00_4585);
												}
											else
												{	/* Eval/expanders.scm 547 */
													obj_t BgL_newtail1141z00_4589;

													{	/* Eval/expanders.scm 547 */
														obj_t BgL_arg1898z00_4590;

														{	/* Eval/expanders.scm 547 */
															obj_t BgL_xz00_4591;

															BgL_xz00_4591 = CAR(((obj_t) BgL_l1137z00_4587));
															BgL_arg1898z00_4590 =
																BGL_PROCEDURE_CALL2(BgL_ez00_4052,
																BgL_xz00_4591, BgL_ez00_4052);
														}
														BgL_newtail1141z00_4589 =
															MAKE_YOUNG_PAIR(BgL_arg1898z00_4590, BNIL);
													}
													SET_CDR(BgL_tail1140z00_4588,
														BgL_newtail1141z00_4589);
													{	/* Eval/expanders.scm 547 */
														obj_t BgL_arg1897z00_4592;

														BgL_arg1897z00_4592 =
															CDR(((obj_t) BgL_l1137z00_4587));
														{
															obj_t BgL_tail1140z00_5790;
															obj_t BgL_l1137z00_5789;

															BgL_l1137z00_5789 = BgL_arg1897z00_4592;
															BgL_tail1140z00_5790 = BgL_newtail1141z00_4589;
															BgL_tail1140z00_4588 = BgL_tail1140z00_5790;
															BgL_l1137z00_4587 = BgL_l1137z00_5789;
															goto BgL_zc3z04anonymousza31895ze3z87_4586;
														}
													}
												}
										}
									}
								return BGl_expandzd2prognzd2zz__prognz00(BgL_arg1893z00_4584);
							}
						else
							{	/* Eval/expanders.scm 95 */
								obj_t BgL_locz00_4593;

								if (EPAIRP(BgL_xz00_4051))
									{	/* Eval/expanders.scm 95 */
										BgL_locz00_4593 = CER(((obj_t) BgL_xz00_4051));
									}
								else
									{	/* Eval/expanders.scm 95 */
										BgL_locz00_4593 = BFALSE;
									}
								{	/* Eval/expanders.scm 96 */
									bool_t BgL_test2752z00_5796;

									if (PAIRP(BgL_locz00_4593))
										{	/* Eval/expanders.scm 96 */
											bool_t BgL_test2754z00_5799;

											{	/* Eval/expanders.scm 96 */
												obj_t BgL_tmpz00_5800;

												BgL_tmpz00_5800 = CDR(BgL_locz00_4593);
												BgL_test2754z00_5799 = PAIRP(BgL_tmpz00_5800);
											}
											if (BgL_test2754z00_5799)
												{	/* Eval/expanders.scm 96 */
													obj_t BgL_tmpz00_5803;

													BgL_tmpz00_5803 = CDR(CDR(BgL_locz00_4593));
													BgL_test2752z00_5796 = PAIRP(BgL_tmpz00_5803);
												}
											else
												{	/* Eval/expanders.scm 96 */
													BgL_test2752z00_5796 = ((bool_t) 0);
												}
										}
									else
										{	/* Eval/expanders.scm 96 */
											BgL_test2752z00_5796 = ((bool_t) 0);
										}
									if (BgL_test2752z00_5796)
										{	/* Eval/expanders.scm 96 */
											return
												BGl_errorzf2locationzf2zz__errorz00
												(BGl_string2364z00zz__install_expandersz00,
												BGl_string2467z00zz__install_expandersz00,
												BgL_xz00_4051, CAR(CDR(BgL_locz00_4593)),
												CAR(CDR(CDR(BgL_locz00_4593))));
										}
									else
										{	/* Eval/expanders.scm 96 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string2364z00zz__install_expandersz00,
												BGl_string2467z00zz__install_expandersz00,
												BgL_xz00_4051);
										}
								}
							}
					}
				else
					{	/* Eval/expanders.scm 543 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2364z00zz__install_expandersz00,
							BGl_string2467z00zz__install_expandersz00, BgL_xz00_4051);
					}
			}
		}

	}



/* &expand-and-let* */
	obj_t BGl_z62expandzd2andzd2letza2zc0zz__install_expandersz00(obj_t
		BgL_envz00_4062, obj_t BgL_xz00_4063, obj_t BgL_ez00_4064)
	{
		{	/* Eval/expanders.scm 554 */
			{
				obj_t BgL_clawsz00_4596;
				obj_t BgL_bodyz00_4597;

				if (PAIRP(BgL_xz00_4063))
					{	/* Eval/expanders.scm 555 */
						obj_t BgL_cdrzd2673zd2_4625;

						BgL_cdrzd2673zd2_4625 = CDR(((obj_t) BgL_xz00_4063));
						if (PAIRP(BgL_cdrzd2673zd2_4625))
							{	/* Eval/expanders.scm 555 */
								BgL_clawsz00_4596 = CAR(BgL_cdrzd2673zd2_4625);
								BgL_bodyz00_4597 = CDR(BgL_cdrzd2673zd2_4625);
								{	/* Eval/expanders.scm 557 */
									obj_t BgL_newzd2varszd2_4598;

									BgL_newzd2varszd2_4598 = BNIL;
									{	/* Eval/expanders.scm 557 */
										obj_t BgL_resultz00_4599;

										BgL_resultz00_4599 =
											MAKE_YOUNG_PAIR(BGl_symbol2501z00zz__install_expandersz00,
											BNIL);
										{	/* Eval/expanders.scm 557 */
											obj_t BgL_growthzd2pointzd2_4600;

											BgL_growthzd2pointzd2_4600 = BgL_resultz00_4599;
											{	/* Eval/expanders.scm 557 */

												if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
													(BgL_clawsz00_4596))
													{	/* Eval/expanders.scm 562 */
														BFALSE;
													}
												else
													{	/* Eval/expanders.scm 95 */
														obj_t BgL_locz00_4601;

														if (EPAIRP(BgL_clawsz00_4596))
															{	/* Eval/expanders.scm 95 */
																BgL_locz00_4601 =
																	CER(((obj_t) BgL_clawsz00_4596));
															}
														else
															{	/* Eval/expanders.scm 95 */
																BgL_locz00_4601 = BFALSE;
															}
														{	/* Eval/expanders.scm 96 */
															bool_t BgL_test2759z00_5828;

															if (PAIRP(BgL_locz00_4601))
																{	/* Eval/expanders.scm 96 */
																	bool_t BgL_test2761z00_5831;

																	{	/* Eval/expanders.scm 96 */
																		obj_t BgL_tmpz00_5832;

																		BgL_tmpz00_5832 = CDR(BgL_locz00_4601);
																		BgL_test2761z00_5831 =
																			PAIRP(BgL_tmpz00_5832);
																	}
																	if (BgL_test2761z00_5831)
																		{	/* Eval/expanders.scm 96 */
																			obj_t BgL_tmpz00_5835;

																			BgL_tmpz00_5835 =
																				CDR(CDR(BgL_locz00_4601));
																			BgL_test2759z00_5828 =
																				PAIRP(BgL_tmpz00_5835);
																		}
																	else
																		{	/* Eval/expanders.scm 96 */
																			BgL_test2759z00_5828 = ((bool_t) 0);
																		}
																}
															else
																{	/* Eval/expanders.scm 96 */
																	BgL_test2759z00_5828 = ((bool_t) 0);
																}
															if (BgL_test2759z00_5828)
																{	/* Eval/expanders.scm 96 */
																	BGl_errorzf2locationzf2zz__errorz00(BFALSE,
																		BGl_string2503z00zz__install_expandersz00,
																		BgL_clawsz00_4596,
																		CAR(CDR(BgL_locz00_4601)),
																		CAR(CDR(CDR(BgL_locz00_4601))));
																}
															else
																{	/* Eval/expanders.scm 96 */
																	BGl_errorz00zz__errorz00(BFALSE,
																		BGl_string2503z00zz__install_expandersz00,
																		BgL_clawsz00_4596);
																}
														}
													}
												{
													obj_t BgL_l1142z00_4603;

													BgL_l1142z00_4603 = BgL_clawsz00_4596;
												BgL_zc3z04anonymousza31904ze3z87_4602:
													if (PAIRP(BgL_l1142z00_4603))
														{	/* Eval/expanders.scm 564 */
															{	/* Eval/expanders.scm 567 */
																obj_t BgL_clawz00_4604;

																BgL_clawz00_4604 = CAR(BgL_l1142z00_4603);
																if (SYMBOLP(BgL_clawz00_4604))
																	{	/* Eval/expanders.scm 559 */
																		obj_t BgL_prevzd2pointzd2_4605;
																		obj_t BgL_clausezd2cellzd2_4606;

																		BgL_prevzd2pointzd2_4605 =
																			BgL_growthzd2pointzd2_4600;
																		BgL_clausezd2cellzd2_4606 =
																			MAKE_YOUNG_PAIR(BgL_clawz00_4604, BNIL);
																		SET_CDR(BgL_growthzd2pointzd2_4600,
																			BgL_clausezd2cellzd2_4606);
																		BgL_growthzd2pointzd2_4600 =
																			BgL_clausezd2cellzd2_4606;
																	}
																else
																	{	/* Eval/expanders.scm 570 */
																		bool_t BgL_test2764z00_5853;

																		if (PAIRP(BgL_clawz00_4604))
																			{	/* Eval/expanders.scm 570 */
																				BgL_test2764z00_5853 =
																					NULLP(CDR(BgL_clawz00_4604));
																			}
																		else
																			{	/* Eval/expanders.scm 570 */
																				BgL_test2764z00_5853 = ((bool_t) 0);
																			}
																		if (BgL_test2764z00_5853)
																			{	/* Eval/expanders.scm 572 */
																				obj_t BgL_arg1912z00_4607;

																				BgL_arg1912z00_4607 =
																					CAR(BgL_clawz00_4604);
																				{	/* Eval/expanders.scm 559 */
																					obj_t BgL_prevzd2pointzd2_4608;
																					obj_t BgL_clausezd2cellzd2_4609;

																					BgL_prevzd2pointzd2_4608 =
																						BgL_growthzd2pointzd2_4600;
																					BgL_clausezd2cellzd2_4609 =
																						MAKE_YOUNG_PAIR(BgL_arg1912z00_4607,
																						BNIL);
																					SET_CDR(BgL_growthzd2pointzd2_4600,
																						BgL_clausezd2cellzd2_4609);
																					BgL_growthzd2pointzd2_4600 =
																						BgL_clausezd2cellzd2_4609;
																				}
																			}
																		else
																			{	/* Eval/expanders.scm 574 */
																				bool_t BgL_test2766z00_5861;

																				if (PAIRP(BgL_clawz00_4604))
																					{	/* Eval/expanders.scm 574 */
																						bool_t BgL_test2768z00_5864;

																						{	/* Eval/expanders.scm 574 */
																							obj_t BgL_tmpz00_5865;

																							BgL_tmpz00_5865 =
																								CAR(BgL_clawz00_4604);
																							BgL_test2768z00_5864 =
																								SYMBOLP(BgL_tmpz00_5865);
																						}
																						if (BgL_test2768z00_5864)
																							{	/* Eval/expanders.scm 575 */
																								bool_t BgL_test2769z00_5868;

																								{	/* Eval/expanders.scm 575 */
																									obj_t BgL_tmpz00_5869;

																									BgL_tmpz00_5869 =
																										CDR(BgL_clawz00_4604);
																									BgL_test2769z00_5868 =
																										PAIRP(BgL_tmpz00_5869);
																								}
																								if (BgL_test2769z00_5868)
																									{	/* Eval/expanders.scm 575 */
																										BgL_test2766z00_5861 =
																											NULLP(CDR(CDR
																												(BgL_clawz00_4604)));
																									}
																								else
																									{	/* Eval/expanders.scm 575 */
																										BgL_test2766z00_5861 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Eval/expanders.scm 574 */
																								BgL_test2766z00_5861 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Eval/expanders.scm 574 */
																						BgL_test2766z00_5861 = ((bool_t) 0);
																					}
																				if (BgL_test2766z00_5861)
																					{	/* Eval/expanders.scm 576 */
																						obj_t BgL_varz00_4610;

																						BgL_varz00_4610 =
																							CAR(BgL_clawz00_4604);
																						{	/* Eval/expanders.scm 576 */
																							obj_t BgL_varzd2cellzd2_4611;

																							BgL_varzd2cellzd2_4611 =
																								MAKE_YOUNG_PAIR(BgL_varz00_4610,
																								BNIL);
																							{	/* Eval/expanders.scm 576 */

																								if (CBOOL
																									(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																										(BgL_varz00_4610,
																											BgL_newzd2varszd2_4598)))
																									{	/* Eval/expanders.scm 95 */
																										obj_t BgL_locz00_4612;

																										if (EPAIRP(BgL_varz00_4610))
																											{	/* Eval/expanders.scm 95 */
																												BgL_locz00_4612 =
																													CER(
																													((obj_t)
																														BgL_varz00_4610));
																											}
																										else
																											{	/* Eval/expanders.scm 95 */
																												BgL_locz00_4612 =
																													BFALSE;
																											}
																										{	/* Eval/expanders.scm 96 */
																											bool_t
																												BgL_test2772z00_5884;
																											if (PAIRP
																												(BgL_locz00_4612))
																												{	/* Eval/expanders.scm 96 */
																													bool_t
																														BgL_test2774z00_5887;
																													{	/* Eval/expanders.scm 96 */
																														obj_t
																															BgL_tmpz00_5888;
																														BgL_tmpz00_5888 =
																															CDR
																															(BgL_locz00_4612);
																														BgL_test2774z00_5887
																															=
																															PAIRP
																															(BgL_tmpz00_5888);
																													}
																													if (BgL_test2774z00_5887)
																														{	/* Eval/expanders.scm 96 */
																															obj_t
																																BgL_tmpz00_5891;
																															BgL_tmpz00_5891 =
																																CDR(CDR
																																(BgL_locz00_4612));
																															BgL_test2772z00_5884
																																=
																																PAIRP
																																(BgL_tmpz00_5891);
																														}
																													else
																														{	/* Eval/expanders.scm 96 */
																															BgL_test2772z00_5884
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Eval/expanders.scm 96 */
																													BgL_test2772z00_5884 =
																														((bool_t) 0);
																												}
																											if (BgL_test2772z00_5884)
																												{	/* Eval/expanders.scm 96 */
																													BGl_errorzf2locationzf2zz__errorz00
																														(BGl_string2415z00zz__install_expandersz00,
																														BGl_string2504z00zz__install_expandersz00,
																														BgL_varz00_4610,
																														CAR(CDR
																															(BgL_locz00_4612)),
																														CAR(CDR(CDR
																																(BgL_locz00_4612))));
																												}
																											else
																												{	/* Eval/expanders.scm 96 */
																													BGl_errorz00zz__errorz00
																														(BGl_string2415z00zz__install_expandersz00,
																														BGl_string2504z00zz__install_expandersz00,
																														BgL_varz00_4610);
																												}
																										}
																									}
																								else
																									{	/* Eval/expanders.scm 577 */
																										BFALSE;
																									}
																								BgL_newzd2varszd2_4598 =
																									MAKE_YOUNG_PAIR
																									(BgL_varz00_4610,
																									BgL_newzd2varszd2_4598);
																								{	/* Eval/expanders.scm 582 */
																									obj_t BgL_arg1923z00_4613;

																									{	/* Eval/expanders.scm 582 */
																										obj_t BgL_arg1924z00_4614;

																										{	/* Eval/expanders.scm 582 */
																											obj_t BgL_arg1925z00_4615;

																											{	/* Eval/expanders.scm 582 */
																												obj_t
																													BgL_arg1926z00_4616;
																												obj_t
																													BgL_arg1927z00_4617;
																												BgL_arg1926z00_4616 =
																													MAKE_YOUNG_PAIR
																													(BgL_clawz00_4604,
																													BNIL);
																												{	/* Eval/expanders.scm 582 */
																													obj_t
																														BgL_arg1928z00_4618;
																													BgL_arg1928z00_4618 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2501z00zz__install_expandersz00,
																														BgL_varzd2cellzd2_4611);
																													BgL_arg1927z00_4617 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1928z00_4618,
																														BNIL);
																												}
																												BgL_arg1925z00_4615 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1926z00_4616,
																													BgL_arg1927z00_4617);
																											}
																											BgL_arg1924z00_4614 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2414z00zz__install_expandersz00,
																												BgL_arg1925z00_4615);
																										}
																										BgL_arg1923z00_4613 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1924z00_4614,
																											BNIL);
																									}
																									SET_CDR
																										(BgL_growthzd2pointzd2_4600,
																										BgL_arg1923z00_4613);
																								}
																								BgL_growthzd2pointzd2_4600 =
																									BgL_varzd2cellzd2_4611;
																							}
																						}
																					}
																				else
																					{	/* Eval/expanders.scm 95 */
																						obj_t BgL_locz00_4619;

																						if (EPAIRP(BgL_clawz00_4604))
																							{	/* Eval/expanders.scm 95 */
																								BgL_locz00_4619 =
																									CER(
																									((obj_t) BgL_clawz00_4604));
																							}
																						else
																							{	/* Eval/expanders.scm 95 */
																								BgL_locz00_4619 = BFALSE;
																							}
																						{	/* Eval/expanders.scm 96 */
																							bool_t BgL_test2776z00_5914;

																							if (PAIRP(BgL_locz00_4619))
																								{	/* Eval/expanders.scm 96 */
																									bool_t BgL_test2778z00_5917;

																									{	/* Eval/expanders.scm 96 */
																										obj_t BgL_tmpz00_5918;

																										BgL_tmpz00_5918 =
																											CDR(BgL_locz00_4619);
																										BgL_test2778z00_5917 =
																											PAIRP(BgL_tmpz00_5918);
																									}
																									if (BgL_test2778z00_5917)
																										{	/* Eval/expanders.scm 96 */
																											obj_t BgL_tmpz00_5921;

																											BgL_tmpz00_5921 =
																												CDR(CDR
																												(BgL_locz00_4619));
																											BgL_test2776z00_5914 =
																												PAIRP(BgL_tmpz00_5921);
																										}
																									else
																										{	/* Eval/expanders.scm 96 */
																											BgL_test2776z00_5914 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Eval/expanders.scm 96 */
																									BgL_test2776z00_5914 =
																										((bool_t) 0);
																								}
																							if (BgL_test2776z00_5914)
																								{	/* Eval/expanders.scm 96 */
																									BGl_errorzf2locationzf2zz__errorz00
																										(BGl_string2415z00zz__install_expandersz00,
																										BGl_string2505z00zz__install_expandersz00,
																										BgL_clawz00_4604,
																										CAR(CDR(BgL_locz00_4619)),
																										CAR(CDR(CDR
																												(BgL_locz00_4619))));
																								}
																							else
																								{	/* Eval/expanders.scm 96 */
																									BGl_errorz00zz__errorz00
																										(BGl_string2415z00zz__install_expandersz00,
																										BGl_string2505z00zz__install_expandersz00,
																										BgL_clawz00_4604);
																								}
																						}
																					}
																			}
																	}
															}
															{
																obj_t BgL_l1142z00_5932;

																BgL_l1142z00_5932 = CDR(BgL_l1142z00_4603);
																BgL_l1142z00_4603 = BgL_l1142z00_5932;
																goto BgL_zc3z04anonymousza31904ze3z87_4602;
															}
														}
													else
														{	/* Eval/expanders.scm 564 */
															((bool_t) 1);
														}
												}
												if (NULLP(BgL_bodyz00_4597))
													{	/* Eval/expanders.scm 587 */
														BFALSE;
													}
												else
													{	/* Eval/expanders.scm 587 */
														obj_t BgL_arg1935z00_4620;

														{	/* Eval/expanders.scm 587 */
															obj_t BgL_arg1936z00_4621;

															BgL_arg1936z00_4621 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_bodyz00_4597, BNIL);
															BgL_arg1935z00_4620 =
																MAKE_YOUNG_PAIR
																(BGl_symbol2363z00zz__install_expandersz00,
																BgL_arg1936z00_4621);
														}
														{	/* Eval/expanders.scm 559 */
															obj_t BgL_prevzd2pointzd2_4622;
															obj_t BgL_clausezd2cellzd2_4623;

															BgL_prevzd2pointzd2_4622 =
																BgL_growthzd2pointzd2_4600;
															BgL_clausezd2cellzd2_4623 =
																MAKE_YOUNG_PAIR(BgL_arg1935z00_4620, BNIL);
															SET_CDR(BgL_growthzd2pointzd2_4600,
																BgL_clausezd2cellzd2_4623);
															BgL_growthzd2pointzd2_4600 =
																BgL_clausezd2cellzd2_4623;
														}
													}
												{	/* Eval/expanders.scm 588 */
													obj_t BgL_arg1937z00_4624;

													BgL_arg1937z00_4624 =
														BGL_PROCEDURE_CALL2(BgL_ez00_4064,
														BgL_resultz00_4599, BgL_ez00_4064);
													return
														BGl_evepairifyz00zz__prognz00(BgL_arg1937z00_4624,
														BgL_xz00_4063);
												}
											}
										}
									}
								}
							}
						else
							{	/* Eval/expanders.scm 555 */
								return
									BGl_expandzd2errorzd2zz__install_expandersz00
									(BGl_string2415z00zz__install_expandersz00,
									BGl_string2506z00zz__install_expandersz00, BgL_xz00_4063);
							}
					}
				else
					{	/* Eval/expanders.scm 555 */
						return
							BGl_expandzd2errorzd2zz__install_expandersz00
							(BGl_string2415z00zz__install_expandersz00,
							BGl_string2506z00zz__install_expandersz00, BgL_xz00_4063);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__install_expandersz00(void)
	{
		{	/* Eval/expanders.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__macroz00(261397491L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_quotez00(289720124L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_letz00(467858524L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_boolz00(229205836L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_casez00(73646821L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_definez00(380411245L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_doz00(285051678L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_tryz00(364756758L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_structz00(90470135L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_recordz00(231151045L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(498124579L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_argsz00(506328475L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expander_tracez00(358612829L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__evalz00(35539458L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__lalr_expandz00(481153209L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__rgc_expandz00(402413976L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__match_expandz00(390935634L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			BGl_modulezd2initializa7ationz75zz__evobjectz00(481718039L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
			return
				BGl_modulezd2initializa7ationz75zz__r5_macro_4_3_syntaxz00(72617499L,
				BSTRING_TO_STRING(BGl_string2507z00zz__install_expandersz00));
		}

	}

#ifdef __cplusplus
}
#endif
