/*===========================================================================*/
/*   (Eval/expdsrfi0.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdsrfi0.scm -indent -o objs/obj_u/Eval/expdsrfi0.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_SRFI0_TYPE_DEFINITIONS
#define BGL___EXPANDER_SRFI0_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_SRFI0_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_bigloozd2intzd2siza7eza7zz__expander_srfi0z00(void);
	extern bool_t BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_symbol1911z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1913z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1915z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1917z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1919z00zz__expander_srfi0z00 = BUNSPEC;
	extern obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1921z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1923z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1925z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1927z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1929z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_srfizd2commonzd2listz00zz__expander_srfi0z00(void);
	extern obj_t bgl_remq_bang(obj_t, obj_t);
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1931z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
		BUNSPEC;
	static obj_t BGl_z62registerzd2srfiz12za2zz__expander_srfi0z00(obj_t, obj_t);
	static obj_t BGl_z62compilezd2srfizf3z43zz__expander_srfi0z00(obj_t, obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol1947z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1951z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1952z00zz__expander_srfi0z00 = BUNSPEC;
	extern bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_symbol1954z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1957z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__expander_srfi0z00(void);
	static obj_t BGl_expandzd2condzd2expandzd2orzd2zz__expander_srfi0z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1959z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1961z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1963z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1884z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1886z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__expander_srfi0z00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_srfi0z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_unregisterzd2evalzd2srfiz12z12zz__expander_srfi0z00(obj_t);
	static obj_t BGl_symbol1890z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_srfi0z00(void);
	static obj_t BGl_symbol1895z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1897z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__expander_srfi0z00(void);
	static obj_t BGl_symbol1899z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__expander_srfi0z00(void);
	BGL_EXPORTED_DECL obj_t bgl_register_eval_srfi(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_unregisterzd2compilezd2srfiz12z12zz__expander_srfi0z00(obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_list1956z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_expandzd2condzd2expandzd2andzd2zz__expander_srfi0z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	static obj_t BGl_bigloozd2elongzd2siza7eza7zz__expander_srfi0z00(void);
	BGL_EXPORTED_DECL obj_t BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_unregisterzd2srfiz12zc0zz__expander_srfi0z00(obj_t);
	static obj_t
		BGl_z62registerzd2compilezd2srfiz12z70zz__expander_srfi0z00(obj_t, obj_t);
	static obj_t BGl_list1894z00zz__expander_srfi0z00 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2condzd2expandz00zz__expander_srfi0z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62unregisterzd2evalzd2srfiz12z70zz__expander_srfi0z00(obj_t,
		obj_t);
	static obj_t BGl_srfizd2evalzd2listz00zz__expander_srfi0z00(void);
	extern obj_t BGl_libraryzd2existszf3z21zz__libraryz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_srfi0z00(void);
	static obj_t BGl_z62expandzd2condzd2expandz62zz__expander_srfi0z00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_evalzd2srfizf3z21zz__expander_srfi0z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2compilezd2condzd2expandzd2zz__expander_srfi0z00(obj_t, obj_t);
	static obj_t
		BGl_z62unregisterzd2compilezd2srfiz12z70zz__expander_srfi0z00(obj_t, obj_t);
	static obj_t BGl_prognz00zz__expander_srfi0z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2evalzd2condzd2expandzd2zz__expander_srfi0z00(obj_t, obj_t);
	static obj_t
		BGl_z62expandzd2evalzd2condzd2expandzb0zz__expander_srfi0z00(obj_t, obj_t,
		obj_t);
	extern obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_compilezd2srfizf3z21zz__expander_srfi0z00(obj_t);
	static obj_t BGl_z62evalzd2srfizf3z43zz__expander_srfi0z00(obj_t, obj_t);
	static obj_t BGl_z62unregisterzd2srfiz12za2zz__expander_srfi0z00(obj_t,
		obj_t);
	static obj_t BGl_srfizd2compilezd2listz00zz__expander_srfi0z00(void);
	static obj_t
		BGl_z62expandzd2compilezd2condzd2expandzb0zz__expander_srfi0z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2compilezd2srfiz12z12zz__expander_srfi0z00(obj_t);
	static obj_t BGl_symbol1901z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_z62registerzd2evalzd2srfiz12z70zz__expander_srfi0z00(obj_t,
		obj_t);
	static obj_t BGl_symbol1903z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1905z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1907z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t BGl_symbol1909z00zz__expander_srfi0z00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1950z00zz__expander_srfi0z00,
		BgL_bgl_string1950za700za7za7_1967za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1953z00zz__expander_srfi0z00,
		BgL_bgl_string1953za700za7za7_1968za7, "else", 4);
	      DEFINE_STRING(BGl_string1955z00zz__expander_srfi0z00,
		BgL_bgl_string1955za700za7za7_1969za7, "and", 3);
	      DEFINE_STRING(BGl_string1958z00zz__expander_srfi0z00,
		BgL_bgl_string1958za700za7za7_1970za7, "or", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2condzd2expandzd2envzd2zz__expander_srfi0z00,
		BgL_bgl_za762expandza7d2cond1971z00,
		BGl_z62expandzd2condzd2expandz62zz__expander_srfi0z00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unregisterzd2evalzd2srfiz12zd2envzc0zz__expander_srfi0z00,
		BgL_bgl_za762unregisterza7d21972z00,
		BGl_z62unregisterzd2evalzd2srfiz12z70zz__expander_srfi0z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_compilezd2srfizf3zd2envzf3zz__expander_srfi0z00,
		BgL_bgl_za762compileza7d2srf1973z00,
		BGl_z62compilezd2srfizf3z43zz__expander_srfi0z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1960z00zz__expander_srfi0z00,
		BgL_bgl_string1960za700za7za7_1974za7, "not", 3);
	      DEFINE_STRING(BGl_string1962z00zz__expander_srfi0z00,
		BgL_bgl_string1962za700za7za7_1975za7, "library", 7);
	      DEFINE_STRING(BGl_string1964z00zz__expander_srfi0z00,
		BgL_bgl_string1964za700za7za7_1976za7, "config", 6);
	      DEFINE_STRING(BGl_string1965z00zz__expander_srfi0z00,
		BgL_bgl_string1965za700za7za7_1977za7, "&expand-cond-expand", 19);
	      DEFINE_STRING(BGl_string1966z00zz__expander_srfi0z00,
		BgL_bgl_string1966za700za7za7_1978za7, "__expander_srfi0", 16);
	      DEFINE_STRING(BGl_string1885z00zz__expander_srfi0z00,
		BgL_bgl_string1885za700za7za7_1979za7, "srfi0", 5);
	      DEFINE_STRING(BGl_string1887z00zz__expander_srfi0z00,
		BgL_bgl_string1887za700za7za7_1980za7, "int-size", 8);
	      DEFINE_STRING(BGl_string1888z00zz__expander_srfi0z00,
		BgL_bgl_string1888za700za7za7_1981za7, "30", 2);
	      DEFINE_STRING(BGl_string1889z00zz__expander_srfi0z00,
		BgL_bgl_string1889za700za7za7_1982za7, "bint", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2compilezd2srfiz12zd2envzc0zz__expander_srfi0z00,
		BgL_bgl_za762registerza7d2co1983z00,
		BGl_z62registerzd2compilezd2srfiz12z70zz__expander_srfi0z00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1891z00zz__expander_srfi0z00,
		BgL_bgl_string1891za700za7za7_1984za7, "elong-size", 10);
	      DEFINE_STRING(BGl_string1892z00zz__expander_srfi0z00,
		BgL_bgl_string1892za700za7za7_1985za7, "32", 2);
	      DEFINE_STRING(BGl_string1893z00zz__expander_srfi0z00,
		BgL_bgl_string1893za700za7za7_1986za7, "elong", 5);
	      DEFINE_STRING(BGl_string1896z00zz__expander_srfi0z00,
		BgL_bgl_string1896za700za7za7_1987za7, "srfi-0", 6);
	      DEFINE_STRING(BGl_string1898z00zz__expander_srfi0z00,
		BgL_bgl_string1898za700za7za7_1988za7, "srfi-2", 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2srfiz12zd2envz12zz__expander_srfi0z00,
		BgL_bgl_za762registerza7d2sr1989z00,
		BGl_z62registerzd2srfiz12za2zz__expander_srfi0z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2evalzd2srfiz12zd2envzc0zz__expander_srfi0z00,
		BgL_bgl_za762registerza7d2ev1990z00,
		BGl_z62registerzd2evalzd2srfiz12z70zz__expander_srfi0z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unregisterzd2compilezd2srfiz12zd2envzc0zz__expander_srfi0z00,
		BgL_bgl_za762unregisterza7d21991z00,
		BGl_z62unregisterzd2compilezd2srfiz12z70zz__expander_srfi0z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2condzd2expandzd2envz00zz__expander_srfi0z00,
		BgL_bgl_za762expandza7d2eval1992z00,
		BGl_z62expandzd2evalzd2condzd2expandzb0zz__expander_srfi0z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2srfizf3zd2envzf3zz__expander_srfi0z00,
		BgL_bgl_za762evalza7d2srfiza7f1993za7,
		BGl_z62evalzd2srfizf3z43zz__expander_srfi0z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1900z00zz__expander_srfi0z00,
		BgL_bgl_string1900za700za7za7_1994za7, "srfi-4", 6);
	      DEFINE_STRING(BGl_string1902z00zz__expander_srfi0z00,
		BgL_bgl_string1902za700za7za7_1995za7, "srfi-6", 6);
	      DEFINE_STRING(BGl_string1904z00zz__expander_srfi0z00,
		BgL_bgl_string1904za700za7za7_1996za7, "srfi-8", 6);
	      DEFINE_STRING(BGl_string1906z00zz__expander_srfi0z00,
		BgL_bgl_string1906za700za7za7_1997za7, "srfi-9", 6);
	      DEFINE_STRING(BGl_string1908z00zz__expander_srfi0z00,
		BgL_bgl_string1908za700za7za7_1998za7, "srfi-10", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2compilezd2condzd2expandzd2envz00zz__expander_srfi0z00,
		BgL_bgl_za762expandza7d2comp1999z00,
		BGl_z62expandzd2compilezd2condzd2expandzb0zz__expander_srfi0z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1910z00zz__expander_srfi0z00,
		BgL_bgl_string1910za700za7za7_2000za7, "srfi-22", 7);
	      DEFINE_STRING(BGl_string1912z00zz__expander_srfi0z00,
		BgL_bgl_string1912za700za7za7_2001za7, "srfi-28", 7);
	      DEFINE_STRING(BGl_string1914z00zz__expander_srfi0z00,
		BgL_bgl_string1914za700za7za7_2002za7, "srfi-30", 7);
	      DEFINE_STRING(BGl_string1916z00zz__expander_srfi0z00,
		BgL_bgl_string1916za700za7za7_2003za7, "rlimit", 6);
	      DEFINE_STRING(BGl_string1918z00zz__expander_srfi0z00,
		BgL_bgl_string1918za700za7za7_2004za7, "gc", 2);
	      DEFINE_STRING(BGl_string1920z00zz__expander_srfi0z00,
		BgL_bgl_string1920za700za7za7_2005za7, "bigloo", 6);
	      DEFINE_STRING(BGl_string1922z00zz__expander_srfi0z00,
		BgL_bgl_string1922za700za7za7_2006za7, "bigloo4", 7);
	      DEFINE_STRING(BGl_string1924z00zz__expander_srfi0z00,
		BgL_bgl_string1924za700za7za7_2007za7, "bigloo4.6", 9);
	      DEFINE_STRING(BGl_string1926z00zz__expander_srfi0z00,
		BgL_bgl_string1926za700za7za7_2008za7, "bigloo4.6a", 10);
	      DEFINE_STRING(BGl_string1928z00zz__expander_srfi0z00,
		BgL_bgl_string1928za700za7za7_2009za7, "bigloo-weakptr", 14);
	      DEFINE_STRING(BGl_string1930z00zz__expander_srfi0z00,
		BgL_bgl_string1930za700za7za7_2010za7, "bigloo-finalizer", 16);
	      DEFINE_STRING(BGl_string1932z00zz__expander_srfi0z00,
		BgL_bgl_string1932za700za7za7_2011za7, "bigloo-eval", 11);
	      DEFINE_STRING(BGl_string1933z00zz__expander_srfi0z00,
		BgL_bgl_string1933za700za7za7_2012za7,
		"/tmp/bigloo/runtime/Eval/expdsrfi0.scm", 38);
	      DEFINE_STRING(BGl_string1934z00zz__expander_srfi0z00,
		BgL_bgl_string1934za700za7za7_2013za7, "&register-eval-srfi!", 20);
	      DEFINE_STRING(BGl_string1935z00zz__expander_srfi0z00,
		BgL_bgl_string1935za700za7za7_2014za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1936z00zz__expander_srfi0z00,
		BgL_bgl_string1936za700za7za7_2015za7, "&unregister-eval-srfi!", 22);
	      DEFINE_STRING(BGl_string1937z00zz__expander_srfi0z00,
		BgL_bgl_string1937za700za7za7_2016za7, "&register-compile-srfi!", 23);
	      DEFINE_STRING(BGl_string1938z00zz__expander_srfi0z00,
		BgL_bgl_string1938za700za7za7_2017za7, "&unregister-compile-srfi!", 25);
	      DEFINE_STRING(BGl_string1939z00zz__expander_srfi0z00,
		BgL_bgl_string1939za700za7za7_2018za7, "&register-srfi!", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unregisterzd2srfiz12zd2envz12zz__expander_srfi0z00,
		BgL_bgl_za762unregisterza7d22019z00,
		BGl_z62unregisterzd2srfiz12za2zz__expander_srfi0z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1940z00zz__expander_srfi0z00,
		BgL_bgl_string1940za700za7za7_2020za7, "&unregister-srfi!", 17);
	      DEFINE_STRING(BGl_string1941z00zz__expander_srfi0z00,
		BgL_bgl_string1941za700za7za7_2021za7, "&expand-eval-cond-expand", 24);
	      DEFINE_STRING(BGl_string1942z00zz__expander_srfi0z00,
		BgL_bgl_string1942za700za7za7_2022za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string1943z00zz__expander_srfi0z00,
		BgL_bgl_string1943za700za7za7_2023za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1944z00zz__expander_srfi0z00,
		BgL_bgl_string1944za700za7za7_2024za7, "&expand-compile-cond-expand", 27);
	      DEFINE_STRING(BGl_string1945z00zz__expander_srfi0z00,
		BgL_bgl_string1945za700za7za7_2025za7, "&compile-srfi?", 14);
	      DEFINE_STRING(BGl_string1946z00zz__expander_srfi0z00,
		BgL_bgl_string1946za700za7za7_2026za7, "&eval-srfi?", 11);
	      DEFINE_STRING(BGl_string1948z00zz__expander_srfi0z00,
		BgL_bgl_string1948za700za7za7_2027za7, "begin", 5);
	      DEFINE_STRING(BGl_string1949z00zz__expander_srfi0z00,
		BgL_bgl_string1949za700za7za7_2028za7, "cond-expand", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1911z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1913z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1915z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1917z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1919z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1921z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1923z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1925z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1927z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1929z00zz__expander_srfi0z00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1931z00zz__expander_srfi0z00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1947z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1951z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1952z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1954z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1957z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1959z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1961z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1963z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1884z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1886z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1890z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1895z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1897z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1899z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_list1956z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_list1894z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1901z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1903z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1905z00zz__expander_srfi0z00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1907z00zz__expander_srfi0z00));
		     ADD_ROOT((void *) (&BGl_symbol1909z00zz__expander_srfi0z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_srfi0z00(long
		BgL_checksumz00_2372, char *BgL_fromz00_2373)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_srfi0z00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_srfi0z00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_srfi0z00();
					BGl_cnstzd2initzd2zz__expander_srfi0z00();
					BGl_importedzd2moduleszd2initz00zz__expander_srfi0z00();
					return BGl_toplevelzd2initzd2zz__expander_srfi0z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			BGl_symbol1884z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1885z00zz__expander_srfi0z00);
			BGl_symbol1886z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1887z00zz__expander_srfi0z00);
			BGl_symbol1890z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1891z00zz__expander_srfi0z00);
			BGl_symbol1895z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1896z00zz__expander_srfi0z00);
			BGl_symbol1897z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1898z00zz__expander_srfi0z00);
			BGl_symbol1899z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1900z00zz__expander_srfi0z00);
			BGl_symbol1901z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1902z00zz__expander_srfi0z00);
			BGl_symbol1903z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1904z00zz__expander_srfi0z00);
			BGl_symbol1905z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1906z00zz__expander_srfi0z00);
			BGl_symbol1907z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1908z00zz__expander_srfi0z00);
			BGl_symbol1909z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1910z00zz__expander_srfi0z00);
			BGl_symbol1911z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1912z00zz__expander_srfi0z00);
			BGl_symbol1913z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1914z00zz__expander_srfi0z00);
			BGl_symbol1915z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1916z00zz__expander_srfi0z00);
			BGl_symbol1917z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1918z00zz__expander_srfi0z00);
			BGl_list1894z00zz__expander_srfi0z00 =
				MAKE_YOUNG_PAIR(BGl_symbol1895z00zz__expander_srfi0z00,
				MAKE_YOUNG_PAIR(BGl_symbol1897z00zz__expander_srfi0z00,
					MAKE_YOUNG_PAIR(BGl_symbol1899z00zz__expander_srfi0z00,
						MAKE_YOUNG_PAIR(BGl_symbol1901z00zz__expander_srfi0z00,
							MAKE_YOUNG_PAIR(BGl_symbol1903z00zz__expander_srfi0z00,
								MAKE_YOUNG_PAIR(BGl_symbol1905z00zz__expander_srfi0z00,
									MAKE_YOUNG_PAIR(BGl_symbol1907z00zz__expander_srfi0z00,
										MAKE_YOUNG_PAIR(BGl_symbol1909z00zz__expander_srfi0z00,
											MAKE_YOUNG_PAIR(BGl_symbol1911z00zz__expander_srfi0z00,
												MAKE_YOUNG_PAIR(BGl_symbol1913z00zz__expander_srfi0z00,
													MAKE_YOUNG_PAIR
													(BGl_symbol1915z00zz__expander_srfi0z00,
														MAKE_YOUNG_PAIR
														(BGl_symbol1917z00zz__expander_srfi0z00,
															BNIL))))))))))));
			BGl_symbol1919z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1920z00zz__expander_srfi0z00);
			BGl_symbol1921z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1922z00zz__expander_srfi0z00);
			BGl_symbol1923z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1924z00zz__expander_srfi0z00);
			BGl_symbol1925z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1926z00zz__expander_srfi0z00);
			BGl_symbol1927z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1928z00zz__expander_srfi0z00);
			BGl_symbol1929z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1930z00zz__expander_srfi0z00);
			BGl_symbol1931z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1932z00zz__expander_srfi0z00);
			BGl_symbol1947z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1948z00zz__expander_srfi0z00);
			BGl_symbol1951z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1949z00zz__expander_srfi0z00);
			BGl_symbol1952z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1953z00zz__expander_srfi0z00);
			BGl_symbol1954z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1955z00zz__expander_srfi0z00);
			BGl_list1956z00zz__expander_srfi0z00 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
			BGl_symbol1957z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1958z00zz__expander_srfi0z00);
			BGl_symbol1959z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1960z00zz__expander_srfi0z00);
			BGl_symbol1961z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1962z00zz__expander_srfi0z00);
			return (BGl_symbol1963z00zz__expander_srfi0z00 =
				bstring_to_symbol(BGl_string1964z00zz__expander_srfi0z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00 =
				bgl_make_mutex(BGl_symbol1884z00zz__expander_srfi0z00);
			BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 = BFALSE;
			return (BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
				BFALSE, BUNSPEC);
		}

	}



/* bigloo-int-size */
	obj_t BGl_bigloozd2intzd2siza7eza7zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 99 */
			{	/* Eval/expdsrfi0.scm 100 */
				obj_t BgL_isiza7eza7_1146;

				BgL_isiza7eza7_1146 =
					BGl_bigloozd2configzd2zz__configurez00
					(BGl_symbol1886z00zz__expander_srfi0z00);
				{	/* Eval/expdsrfi0.scm 103 */
					obj_t BgL_arg1182z00_1147;

					{	/* Eval/expdsrfi0.scm 103 */
						obj_t BgL_arg1183z00_1148;

						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_isiza7eza7_1146))
							{	/* Ieee/number.scm 174 */

								BgL_arg1183z00_1148 =
									BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
									(BgL_isiza7eza7_1146, BINT(10L));
							}
						else
							{	/* Eval/expdsrfi0.scm 103 */
								BgL_arg1183z00_1148 = BGl_string1888z00zz__expander_srfi0z00;
							}
						BgL_arg1182z00_1147 =
							string_append(BGl_string1889z00zz__expander_srfi0z00,
							BgL_arg1183z00_1148);
					}
					return bstring_to_symbol(BgL_arg1182z00_1147);
				}
			}
		}

	}



/* bigloo-elong-size */
	obj_t BGl_bigloozd2elongzd2siza7eza7zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 108 */
			{	/* Eval/expdsrfi0.scm 109 */
				obj_t BgL_esiza7eza7_1152;

				BgL_esiza7eza7_1152 =
					BGl_bigloozd2configzd2zz__configurez00
					(BGl_symbol1890z00zz__expander_srfi0z00);
				{	/* Eval/expdsrfi0.scm 112 */
					obj_t BgL_arg1187z00_1153;

					{	/* Eval/expdsrfi0.scm 112 */
						obj_t BgL_arg1188z00_1154;

						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_esiza7eza7_1152))
							{	/* Ieee/number.scm 174 */

								BgL_arg1188z00_1154 =
									BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
									(BgL_esiza7eza7_1152, BINT(10L));
							}
						else
							{	/* Eval/expdsrfi0.scm 112 */
								BgL_arg1188z00_1154 = BGl_string1892z00zz__expander_srfi0z00;
							}
						BgL_arg1187z00_1153 =
							string_append(BGl_string1893z00zz__expander_srfi0z00,
							BgL_arg1188z00_1154);
					}
					return bstring_to_symbol(BgL_arg1187z00_1153);
				}
			}
		}

	}



/* srfi-common-list */
	obj_t BGl_srfizd2commonzd2listz00zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 137 */
			{	/* Eval/expdsrfi0.scm 138 */
				obj_t BgL_lz00_1158;

				{	/* Eval/expdsrfi0.scm 142 */
					obj_t BgL_arg1191z00_1160;

					{	/* Eval/expdsrfi0.scm 142 */
						obj_t BgL_arg1193z00_1161;

						{	/* Eval/expdsrfi0.scm 142 */
							obj_t BgL_arg1194z00_1162;

							{	/* Eval/expdsrfi0.scm 142 */
								obj_t BgL_arg1196z00_1163;

								{	/* Eval/expdsrfi0.scm 142 */
									obj_t BgL_arg1197z00_1164;
									obj_t BgL_arg1198z00_1165;

									BgL_arg1197z00_1164 =
										BGl_bigloozd2intzd2siza7eza7zz__expander_srfi0z00();
									{	/* Eval/expdsrfi0.scm 143 */
										obj_t BgL_arg1199z00_1166;

										BgL_arg1199z00_1166 =
											BGl_bigloozd2elongzd2siza7eza7zz__expander_srfi0z00();
										BgL_arg1198z00_1165 =
											MAKE_YOUNG_PAIR(BgL_arg1199z00_1166,
											BGl_list1894z00zz__expander_srfi0z00);
									}
									BgL_arg1196z00_1163 =
										MAKE_YOUNG_PAIR(BgL_arg1197z00_1164, BgL_arg1198z00_1165);
								}
								BgL_arg1194z00_1162 =
									MAKE_YOUNG_PAIR(BGl_symbol1919z00zz__expander_srfi0z00,
									BgL_arg1196z00_1163);
							}
							BgL_arg1193z00_1161 =
								MAKE_YOUNG_PAIR(BGl_symbol1921z00zz__expander_srfi0z00,
								BgL_arg1194z00_1162);
						}
						BgL_arg1191z00_1160 =
							MAKE_YOUNG_PAIR(BGl_symbol1923z00zz__expander_srfi0z00,
							BgL_arg1193z00_1161);
					}
					BgL_lz00_1158 =
						MAKE_YOUNG_PAIR(BGl_symbol1925z00zz__expander_srfi0z00,
						BgL_arg1191z00_1160);
				}
				if (BGL_AUTO_FINALIZER)
					{	/* Eval/expdsrfi0.scm 157 */
						obj_t BgL_arg1190z00_1159;

						BgL_arg1190z00_1159 =
							MAKE_YOUNG_PAIR(BGl_symbol1927z00zz__expander_srfi0z00,
							BgL_lz00_1158);
						return MAKE_YOUNG_PAIR(BGl_symbol1929z00zz__expander_srfi0z00,
							BgL_arg1190z00_1159);
					}
				else
					{	/* Eval/expdsrfi0.scm 156 */
						return BgL_lz00_1158;
					}
			}
		}

	}



/* srfi-compile-list */
	obj_t BGl_srfizd2compilezd2listz00zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 169 */
			if (CBOOL(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00))
				{	/* Eval/expdsrfi0.scm 170 */
					BFALSE;
				}
			else
				{	/* Eval/expdsrfi0.scm 170 */
					BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
						BGl_srfizd2commonzd2listz00zz__expander_srfi0z00();
				}
			return BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00;
		}

	}



/* srfi-eval-list */
	obj_t BGl_srfizd2evalzd2listz00zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 177 */
			if (CBOOL(BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00))
				{	/* Eval/expdsrfi0.scm 178 */
					BFALSE;
				}
			else
				{	/* Eval/expdsrfi0.scm 179 */
					obj_t BgL_arg1200z00_1167;

					BgL_arg1200z00_1167 =
						BGl_srfizd2commonzd2listz00zz__expander_srfi0z00();
					BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 =
						MAKE_YOUNG_PAIR(BGl_symbol1931z00zz__expander_srfi0z00,
						BgL_arg1200z00_1167);
				}
			return BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00;
		}

	}



/* register-eval-srfi! */
	BGL_EXPORTED_DEF obj_t bgl_register_eval_srfi(obj_t BgL_srfiz00_3)
	{
		{	/* Eval/expdsrfi0.scm 185 */
			{	/* Eval/expdsrfi0.scm 186 */
				obj_t BgL_top2036z00_2459;

				BgL_top2036z00_2459 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2036z00_2459,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 186 */
					obj_t BgL_tmp2035z00_2458;

					BgL_tmp2035z00_2458 =
						(BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 =
						MAKE_YOUNG_PAIR(BgL_srfiz00_3,
							BGl_srfizd2evalzd2listz00zz__expander_srfi0z00()), BUNSPEC);
					BGL_EXITD_POP_PROTECT(BgL_top2036z00_2459);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2035z00_2458;
				}
			}
		}

	}



/* &register-eval-srfi! */
	obj_t BGl_z62registerzd2evalzd2srfiz12z70zz__expander_srfi0z00(obj_t
		BgL_envz00_2286, obj_t BgL_srfiz00_2287)
	{
		{	/* Eval/expdsrfi0.scm 185 */
			{	/* Eval/expdsrfi0.scm 186 */
				obj_t BgL_auxz00_2467;

				if (SYMBOLP(BgL_srfiz00_2287))
					{	/* Eval/expdsrfi0.scm 186 */
						BgL_auxz00_2467 = BgL_srfiz00_2287;
					}
				else
					{
						obj_t BgL_auxz00_2470;

						BgL_auxz00_2470 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(7622L),
							BGl_string1934z00zz__expander_srfi0z00,
							BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2287);
						FAILURE(BgL_auxz00_2470, BFALSE, BFALSE);
					}
				return bgl_register_eval_srfi(BgL_auxz00_2467);
			}
		}

	}



/* unregister-eval-srfi! */
	BGL_EXPORTED_DEF obj_t
		BGl_unregisterzd2evalzd2srfiz12z12zz__expander_srfi0z00(obj_t BgL_srfiz00_4)
	{
		{	/* Eval/expdsrfi0.scm 192 */
			{	/* Eval/expdsrfi0.scm 193 */
				obj_t BgL_top2039z00_2476;

				BgL_top2039z00_2476 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2039z00_2476,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 193 */
					obj_t BgL_tmp2038z00_2475;

					BgL_tmp2038z00_2475 =
						(BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 =
						bgl_remq_bang(BgL_srfiz00_4,
							BGl_srfizd2evalzd2listz00zz__expander_srfi0z00()), BUNSPEC);
					BGL_EXITD_POP_PROTECT(BgL_top2039z00_2476);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2038z00_2475;
				}
			}
		}

	}



/* &unregister-eval-srfi! */
	obj_t BGl_z62unregisterzd2evalzd2srfiz12z70zz__expander_srfi0z00(obj_t
		BgL_envz00_2288, obj_t BgL_srfiz00_2289)
	{
		{	/* Eval/expdsrfi0.scm 192 */
			{	/* Eval/expdsrfi0.scm 193 */
				obj_t BgL_auxz00_2484;

				if (SYMBOLP(BgL_srfiz00_2289))
					{	/* Eval/expdsrfi0.scm 193 */
						BgL_auxz00_2484 = BgL_srfiz00_2289;
					}
				else
					{
						obj_t BgL_auxz00_2487;

						BgL_auxz00_2487 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(7980L),
							BGl_string1936z00zz__expander_srfi0z00,
							BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2289);
						FAILURE(BgL_auxz00_2487, BFALSE, BFALSE);
					}
				return
					BGl_unregisterzd2evalzd2srfiz12z12zz__expander_srfi0z00
					(BgL_auxz00_2484);
			}
		}

	}



/* register-compile-srfi! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2compilezd2srfiz12z12zz__expander_srfi0z00(obj_t
		BgL_srfiz00_5)
	{
		{	/* Eval/expdsrfi0.scm 199 */
			{	/* Eval/expdsrfi0.scm 200 */
				obj_t BgL_top2042z00_2493;

				BgL_top2042z00_2493 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2042z00_2493,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 200 */
					obj_t BgL_tmp2041z00_2492;

					{	/* Eval/expdsrfi0.scm 201 */
						obj_t BgL_arg1203z00_1878;

						if (CBOOL(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00))
							{	/* Eval/expdsrfi0.scm 170 */
								BFALSE;
							}
						else
							{	/* Eval/expdsrfi0.scm 170 */
								BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
									BGl_srfizd2commonzd2listz00zz__expander_srfi0z00();
							}
						BgL_arg1203z00_1878 =
							BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00;
						BgL_tmp2041z00_2492 =
							(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
							MAKE_YOUNG_PAIR(BgL_srfiz00_5, BgL_arg1203z00_1878), BUNSPEC);
					}
					BGL_EXITD_POP_PROTECT(BgL_top2042z00_2493);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2041z00_2492;
				}
			}
		}

	}



/* &register-compile-srfi! */
	obj_t BGl_z62registerzd2compilezd2srfiz12z70zz__expander_srfi0z00(obj_t
		BgL_envz00_2290, obj_t BgL_srfiz00_2291)
	{
		{	/* Eval/expdsrfi0.scm 199 */
			{	/* Eval/expdsrfi0.scm 200 */
				obj_t BgL_auxz00_2503;

				if (SYMBOLP(BgL_srfiz00_2291))
					{	/* Eval/expdsrfi0.scm 200 */
						BgL_auxz00_2503 = BgL_srfiz00_2291;
					}
				else
					{
						obj_t BgL_auxz00_2506;

						BgL_auxz00_2506 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(8340L),
							BGl_string1937z00zz__expander_srfi0z00,
							BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2291);
						FAILURE(BgL_auxz00_2506, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2compilezd2srfiz12z12zz__expander_srfi0z00
					(BgL_auxz00_2503);
			}
		}

	}



/* unregister-compile-srfi! */
	BGL_EXPORTED_DEF obj_t
		BGl_unregisterzd2compilezd2srfiz12z12zz__expander_srfi0z00(obj_t
		BgL_srfiz00_6)
	{
		{	/* Eval/expdsrfi0.scm 206 */
			{	/* Eval/expdsrfi0.scm 207 */
				obj_t BgL_top2046z00_2512;

				BgL_top2046z00_2512 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2046z00_2512,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 207 */
					obj_t BgL_tmp2045z00_2511;

					{	/* Eval/expdsrfi0.scm 208 */
						obj_t BgL_arg1206z00_1879;

						if (CBOOL(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00))
							{	/* Eval/expdsrfi0.scm 170 */
								BFALSE;
							}
						else
							{	/* Eval/expdsrfi0.scm 170 */
								BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
									BGl_srfizd2commonzd2listz00zz__expander_srfi0z00();
							}
						BgL_arg1206z00_1879 =
							BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00;
						BgL_tmp2045z00_2511 =
							(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
							bgl_remq_bang(BgL_srfiz00_6, BgL_arg1206z00_1879), BUNSPEC);
					}
					BGL_EXITD_POP_PROTECT(BgL_top2046z00_2512);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2045z00_2511;
				}
			}
		}

	}



/* &unregister-compile-srfi! */
	obj_t BGl_z62unregisterzd2compilezd2srfiz12z70zz__expander_srfi0z00(obj_t
		BgL_envz00_2292, obj_t BgL_srfiz00_2293)
	{
		{	/* Eval/expdsrfi0.scm 206 */
			{	/* Eval/expdsrfi0.scm 207 */
				obj_t BgL_auxz00_2522;

				if (SYMBOLP(BgL_srfiz00_2293))
					{	/* Eval/expdsrfi0.scm 207 */
						BgL_auxz00_2522 = BgL_srfiz00_2293;
					}
				else
					{
						obj_t BgL_auxz00_2525;

						BgL_auxz00_2525 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(8707L),
							BGl_string1938z00zz__expander_srfi0z00,
							BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2293);
						FAILURE(BgL_auxz00_2525, BFALSE, BFALSE);
					}
				return
					BGl_unregisterzd2compilezd2srfiz12z12zz__expander_srfi0z00
					(BgL_auxz00_2522);
			}
		}

	}



/* register-srfi! */
	BGL_EXPORTED_DEF obj_t BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(obj_t
		BgL_srfiz00_7)
	{
		{	/* Eval/expdsrfi0.scm 213 */
			{	/* Eval/expdsrfi0.scm 186 */
				obj_t BgL_top2050z00_2531;

				BgL_top2050z00_2531 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2050z00_2531,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 186 */
					obj_t BgL_tmp2049z00_2530;

					BgL_tmp2049z00_2530 =
						(BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 =
						MAKE_YOUNG_PAIR(BgL_srfiz00_7,
							BGl_srfizd2evalzd2listz00zz__expander_srfi0z00()), BUNSPEC);
					BGL_EXITD_POP_PROTECT(BgL_top2050z00_2531);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					BgL_tmp2049z00_2530;
				}
			}
			{	/* Eval/expdsrfi0.scm 200 */
				obj_t BgL_top2052z00_2540;

				BgL_top2052z00_2540 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2052z00_2540,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 200 */
					obj_t BgL_tmp2051z00_2539;

					BgL_tmp2051z00_2539 =
						(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
						MAKE_YOUNG_PAIR(BgL_srfiz00_7,
							BGl_srfizd2compilezd2listz00zz__expander_srfi0z00()), BUNSPEC);
					BGL_EXITD_POP_PROTECT(BgL_top2052z00_2540);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2051z00_2539;
				}
			}
		}

	}



/* &register-srfi! */
	obj_t BGl_z62registerzd2srfiz12za2zz__expander_srfi0z00(obj_t BgL_envz00_2294,
		obj_t BgL_srfiz00_2295)
	{
		{	/* Eval/expdsrfi0.scm 213 */
			{	/* Eval/expdsrfi0.scm 214 */
				obj_t BgL_auxz00_2548;

				if (SYMBOLP(BgL_srfiz00_2295))
					{	/* Eval/expdsrfi0.scm 214 */
						BgL_auxz00_2548 = BgL_srfiz00_2295;
					}
				else
					{
						obj_t BgL_auxz00_2551;

						BgL_auxz00_2551 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(9090L),
							BGl_string1939z00zz__expander_srfi0z00,
							BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2295);
						FAILURE(BgL_auxz00_2551, BFALSE, BFALSE);
					}
				return BGl_registerzd2srfiz12zc0zz__expander_srfi0z00(BgL_auxz00_2548);
			}
		}

	}



/* unregister-srfi! */
	BGL_EXPORTED_DEF obj_t BGl_unregisterzd2srfiz12zc0zz__expander_srfi0z00(obj_t
		BgL_srfiz00_8)
	{
		{	/* Eval/expdsrfi0.scm 220 */
			{	/* Eval/expdsrfi0.scm 193 */
				obj_t BgL_top2055z00_2557;

				BgL_top2055z00_2557 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2055z00_2557,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 193 */
					obj_t BgL_tmp2054z00_2556;

					BgL_tmp2054z00_2556 =
						(BGl_za2srfizd2evalzd2listza2z00zz__expander_srfi0z00 =
						bgl_remq_bang(BgL_srfiz00_8,
							BGl_srfizd2evalzd2listz00zz__expander_srfi0z00()), BUNSPEC);
					BGL_EXITD_POP_PROTECT(BgL_top2055z00_2557);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					BgL_tmp2054z00_2556;
				}
			}
			{	/* Eval/expdsrfi0.scm 207 */
				obj_t BgL_top2057z00_2566;

				BgL_top2057z00_2566 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2057z00_2566,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 207 */
					obj_t BgL_tmp2056z00_2565;

					BgL_tmp2056z00_2565 =
						(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
						bgl_remq_bang(BgL_srfiz00_8,
							BGl_srfizd2compilezd2listz00zz__expander_srfi0z00()), BUNSPEC);
					BGL_EXITD_POP_PROTECT(BgL_top2057z00_2566);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2056z00_2565;
				}
			}
		}

	}



/* &unregister-srfi! */
	obj_t BGl_z62unregisterzd2srfiz12za2zz__expander_srfi0z00(obj_t
		BgL_envz00_2296, obj_t BgL_srfiz00_2297)
	{
		{	/* Eval/expdsrfi0.scm 220 */
			{	/* Eval/expdsrfi0.scm 221 */
				obj_t BgL_auxz00_2574;

				if (SYMBOLP(BgL_srfiz00_2297))
					{	/* Eval/expdsrfi0.scm 221 */
						BgL_auxz00_2574 = BgL_srfiz00_2297;
					}
				else
					{
						obj_t BgL_auxz00_2577;

						BgL_auxz00_2577 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(9419L),
							BGl_string1940z00zz__expander_srfi0z00,
							BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2297);
						FAILURE(BgL_auxz00_2577, BFALSE, BFALSE);
					}
				return
					BGl_unregisterzd2srfiz12zc0zz__expander_srfi0z00(BgL_auxz00_2574);
			}
		}

	}



/* expand-eval-cond-expand */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2evalzd2condzd2expandzd2zz__expander_srfi0z00(obj_t BgL_xz00_9,
		obj_t BgL_ez00_10)
	{
		{	/* Eval/expdsrfi0.scm 227 */
			return
				BGl_expandzd2condzd2expandz00zz__expander_srfi0z00(BgL_xz00_9,
				BgL_ez00_10, BGl_srfizd2evalzd2listz00zz__expander_srfi0z00());
		}

	}



/* &expand-eval-cond-expand */
	obj_t BGl_z62expandzd2evalzd2condzd2expandzb0zz__expander_srfi0z00(obj_t
		BgL_envz00_2298, obj_t BgL_xz00_2299, obj_t BgL_ez00_2300)
	{
		{	/* Eval/expdsrfi0.scm 227 */
			{	/* Eval/expdsrfi0.scm 228 */
				obj_t BgL_auxz00_2591;
				obj_t BgL_auxz00_2584;

				if (PROCEDUREP(BgL_ez00_2300))
					{	/* Eval/expdsrfi0.scm 228 */
						BgL_auxz00_2591 = BgL_ez00_2300;
					}
				else
					{
						obj_t BgL_auxz00_2594;

						BgL_auxz00_2594 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(9745L),
							BGl_string1941z00zz__expander_srfi0z00,
							BGl_string1943z00zz__expander_srfi0z00, BgL_ez00_2300);
						FAILURE(BgL_auxz00_2594, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_2299))
					{	/* Eval/expdsrfi0.scm 228 */
						BgL_auxz00_2584 = BgL_xz00_2299;
					}
				else
					{
						obj_t BgL_auxz00_2587;

						BgL_auxz00_2587 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(9745L),
							BGl_string1941z00zz__expander_srfi0z00,
							BGl_string1942z00zz__expander_srfi0z00, BgL_xz00_2299);
						FAILURE(BgL_auxz00_2587, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2evalzd2condzd2expandzd2zz__expander_srfi0z00
					(BgL_auxz00_2584, BgL_auxz00_2591);
			}
		}

	}



/* expand-compile-cond-expand */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2compilezd2condzd2expandzd2zz__expander_srfi0z00(obj_t
		BgL_xz00_11, obj_t BgL_ez00_12)
	{
		{	/* Eval/expdsrfi0.scm 233 */
			{	/* Eval/expdsrfi0.scm 234 */
				obj_t BgL_arg1209z00_1889;

				if (CBOOL(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00))
					{	/* Eval/expdsrfi0.scm 170 */
						BFALSE;
					}
				else
					{	/* Eval/expdsrfi0.scm 170 */
						BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
							BGl_srfizd2commonzd2listz00zz__expander_srfi0z00();
					}
				BgL_arg1209z00_1889 =
					BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00;
				return BGl_expandzd2condzd2expandz00zz__expander_srfi0z00(BgL_xz00_11,
					BgL_ez00_12, BgL_arg1209z00_1889);
			}
		}

	}



/* &expand-compile-cond-expand */
	obj_t BGl_z62expandzd2compilezd2condzd2expandzb0zz__expander_srfi0z00(obj_t
		BgL_envz00_2301, obj_t BgL_xz00_2302, obj_t BgL_ez00_2303)
	{
		{	/* Eval/expdsrfi0.scm 233 */
			{	/* Eval/expdsrfi0.scm 234 */
				obj_t BgL_auxz00_2610;
				obj_t BgL_auxz00_2603;

				if (PROCEDUREP(BgL_ez00_2303))
					{	/* Eval/expdsrfi0.scm 234 */
						BgL_auxz00_2610 = BgL_ez00_2303;
					}
				else
					{
						obj_t BgL_auxz00_2613;

						BgL_auxz00_2613 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(10055L),
							BGl_string1944z00zz__expander_srfi0z00,
							BGl_string1943z00zz__expander_srfi0z00, BgL_ez00_2303);
						FAILURE(BgL_auxz00_2613, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_2302))
					{	/* Eval/expdsrfi0.scm 234 */
						BgL_auxz00_2603 = BgL_xz00_2302;
					}
				else
					{
						obj_t BgL_auxz00_2606;

						BgL_auxz00_2606 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(10055L),
							BGl_string1944z00zz__expander_srfi0z00,
							BGl_string1942z00zz__expander_srfi0z00, BgL_xz00_2302);
						FAILURE(BgL_auxz00_2606, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2compilezd2condzd2expandzd2zz__expander_srfi0z00
					(BgL_auxz00_2603, BgL_auxz00_2610);
			}
		}

	}



/* compile-srfi? */
	BGL_EXPORTED_DEF bool_t BGl_compilezd2srfizf3z21zz__expander_srfi0z00(obj_t
		BgL_srfiz00_13)
	{
		{	/* Eval/expdsrfi0.scm 239 */
			{	/* Eval/expdsrfi0.scm 240 */
				obj_t BgL_top2065z00_2619;

				BgL_top2065z00_2619 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2065z00_2619,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 240 */
					bool_t BgL_tmp2064z00_2618;

					{	/* Eval/expdsrfi0.scm 241 */
						obj_t BgL_arg1210z00_1890;

						if (CBOOL(BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00))
							{	/* Eval/expdsrfi0.scm 170 */
								BFALSE;
							}
						else
							{	/* Eval/expdsrfi0.scm 170 */
								BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00 =
									BGl_srfizd2commonzd2listz00zz__expander_srfi0z00();
							}
						BgL_arg1210z00_1890 =
							BGl_za2srfizd2compilezd2listza2z00zz__expander_srfi0z00;
						BgL_tmp2064z00_2618 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_srfiz00_13,
								BgL_arg1210z00_1890));
					}
					BGL_EXITD_POP_PROTECT(BgL_top2065z00_2619);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2064z00_2618;
				}
			}
		}

	}



/* &compile-srfi? */
	obj_t BGl_z62compilezd2srfizf3z43zz__expander_srfi0z00(obj_t BgL_envz00_2304,
		obj_t BgL_srfiz00_2305)
	{
		{	/* Eval/expdsrfi0.scm 239 */
			{	/* Eval/expdsrfi0.scm 240 */
				bool_t BgL_tmpz00_2630;

				{	/* Eval/expdsrfi0.scm 240 */
					obj_t BgL_auxz00_2631;

					if (SYMBOLP(BgL_srfiz00_2305))
						{	/* Eval/expdsrfi0.scm 240 */
							BgL_auxz00_2631 = BgL_srfiz00_2305;
						}
					else
						{
							obj_t BgL_auxz00_2634;

							BgL_auxz00_2634 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1933z00zz__expander_srfi0z00, BINT(10332L),
								BGl_string1945z00zz__expander_srfi0z00,
								BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2305);
							FAILURE(BgL_auxz00_2634, BFALSE, BFALSE);
						}
					BgL_tmpz00_2630 =
						BGl_compilezd2srfizf3z21zz__expander_srfi0z00(BgL_auxz00_2631);
				}
				return BBOOL(BgL_tmpz00_2630);
			}
		}

	}



/* eval-srfi? */
	BGL_EXPORTED_DEF bool_t BGl_evalzd2srfizf3z21zz__expander_srfi0z00(obj_t
		BgL_srfiz00_14)
	{
		{	/* Eval/expdsrfi0.scm 246 */
			{	/* Eval/expdsrfi0.scm 247 */
				obj_t BgL_top2069z00_2641;

				BgL_top2069z00_2641 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BGL_EXITD_PUSH_PROTECT(BgL_top2069z00_2641,
					BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
				BUNSPEC;
				{	/* Eval/expdsrfi0.scm 247 */
					bool_t BgL_tmp2068z00_2640;

					{	/* Eval/expdsrfi0.scm 248 */
						obj_t BgL_arg1212z00_1891;

						BgL_arg1212z00_1891 =
							BGl_srfizd2evalzd2listz00zz__expander_srfi0z00();
						BgL_tmp2068z00_2640 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_srfiz00_14,
								BgL_arg1212z00_1891));
					}
					BGL_EXITD_POP_PROTECT(BgL_top2069z00_2641);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2srfizd2mutexza2zd2zz__expander_srfi0z00);
					return BgL_tmp2068z00_2640;
				}
			}
		}

	}



/* &eval-srfi? */
	obj_t BGl_z62evalzd2srfizf3z43zz__expander_srfi0z00(obj_t BgL_envz00_2306,
		obj_t BgL_srfiz00_2307)
	{
		{	/* Eval/expdsrfi0.scm 246 */
			{	/* Eval/expdsrfi0.scm 247 */
				bool_t BgL_tmpz00_2650;

				{	/* Eval/expdsrfi0.scm 247 */
					obj_t BgL_auxz00_2651;

					if (SYMBOLP(BgL_srfiz00_2307))
						{	/* Eval/expdsrfi0.scm 247 */
							BgL_auxz00_2651 = BgL_srfiz00_2307;
						}
					else
						{
							obj_t BgL_auxz00_2654;

							BgL_auxz00_2654 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1933z00zz__expander_srfi0z00, BINT(10653L),
								BGl_string1946z00zz__expander_srfi0z00,
								BGl_string1935z00zz__expander_srfi0z00, BgL_srfiz00_2307);
							FAILURE(BgL_auxz00_2654, BFALSE, BFALSE);
						}
					BgL_tmpz00_2650 =
						BGl_evalzd2srfizf3z21zz__expander_srfi0z00(BgL_auxz00_2651);
				}
				return BBOOL(BgL_tmpz00_2650);
			}
		}

	}



/* progn */
	obj_t BGl_prognz00zz__expander_srfi0z00(obj_t BgL_bodyz00_15)
	{
		{	/* Eval/expdsrfi0.scm 253 */
			if (PAIRP(BgL_bodyz00_15))
				{	/* Eval/expdsrfi0.scm 255 */
					if (NULLP(CDR(BgL_bodyz00_15)))
						{	/* Eval/expdsrfi0.scm 256 */
							return CAR(BgL_bodyz00_15);
						}
					else
						{	/* Eval/expdsrfi0.scm 257 */
							obj_t BgL_arg1218z00_1179;

							BgL_arg1218z00_1179 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_bodyz00_15,
								BNIL);
							return MAKE_YOUNG_PAIR(BGl_symbol1947z00zz__expander_srfi0z00,
								BgL_arg1218z00_1179);
						}
				}
			else
				{	/* Eval/expdsrfi0.scm 255 */
					obj_t BgL_arg1220z00_1181;

					{	/* Eval/expdsrfi0.scm 255 */
						obj_t BgL_arg1221z00_1182;

						if (NULLP(BgL_bodyz00_15))
							{	/* Eval/expdsrfi0.scm 255 */
								BgL_arg1221z00_1182 = BUNSPEC;
							}
						else
							{	/* Eval/expdsrfi0.scm 255 */
								BgL_arg1221z00_1182 = BgL_bodyz00_15;
							}
						BgL_arg1220z00_1181 = MAKE_YOUNG_PAIR(BgL_arg1221z00_1182, BNIL);
					}
					return
						MAKE_YOUNG_PAIR(BGl_symbol1947z00zz__expander_srfi0z00,
						BgL_arg1220z00_1181);
				}
		}

	}



/* expand-cond-expand */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2condzd2expandz00zz__expander_srfi0z00(obj_t BgL_xz00_16,
		obj_t BgL_ez00_17, obj_t BgL_featuresz00_18)
	{
		{	/* Eval/expdsrfi0.scm 262 */
			{
				obj_t BgL_clausez00_1185;
				obj_t BgL_elsez00_1186;

				if (NULLP(BgL_xz00_16))
					{	/* Eval/expdsrfi0.scm 266 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string1949z00zz__expander_srfi0z00,
							BGl_string1950z00zz__expander_srfi0z00, BgL_xz00_16);
					}
				else
					{	/* Eval/expdsrfi0.scm 266 */
						if (
							(CAR(
									((obj_t) BgL_xz00_16)) ==
								BGl_symbol1951z00zz__expander_srfi0z00))
							{	/* Eval/expdsrfi0.scm 266 */
								if (NULLP(CDR(((obj_t) BgL_xz00_16))))
									{	/* Eval/expdsrfi0.scm 266 */
										return BUNSPEC;
									}
								else
									{	/* Eval/expdsrfi0.scm 266 */
										obj_t BgL_cdrzd2165zd2_1195;

										BgL_cdrzd2165zd2_1195 = CDR(((obj_t) BgL_xz00_16));
										if (PAIRP(BgL_cdrzd2165zd2_1195))
											{	/* Eval/expdsrfi0.scm 266 */
												BgL_clausez00_1185 = CAR(BgL_cdrzd2165zd2_1195);
												BgL_elsez00_1186 = CDR(BgL_cdrzd2165zd2_1195);
											BgL_tagzd2153zd2_1187:
												{
													obj_t BgL_featurez00_1237;
													obj_t BgL_bodyz00_1238;
													obj_t BgL_reqz00_1227;
													obj_t BgL_bodyz00_1228;
													obj_t BgL_bodyz00_1205;

													if (PAIRP(BgL_clausez00_1185))
														{	/* Eval/expdsrfi0.scm 266 */
															if (
																(CAR(
																		((obj_t) BgL_clausez00_1185)) ==
																	BGl_symbol1952z00zz__expander_srfi0z00))
																{	/* Eval/expdsrfi0.scm 266 */
																	obj_t BgL_arg1244z00_1245;

																	BgL_arg1244z00_1245 =
																		CDR(((obj_t) BgL_clausez00_1185));
																	BgL_bodyz00_1205 = BgL_arg1244z00_1245;
																	if (NULLP(BgL_elsez00_1186))
																		{	/* Eval/expdsrfi0.scm 272 */
																			if (NULLP(BgL_bodyz00_1205))
																				{	/* Eval/expdsrfi0.scm 273 */
																					return BUNSPEC;
																				}
																			else
																				{	/* Eval/expdsrfi0.scm 275 */
																					obj_t BgL_arg1403z00_1377;

																					BgL_arg1403z00_1377 =
																						BGl_evepairifyz00zz__prognz00
																						(BGl_prognz00zz__expander_srfi0z00
																						(BgL_bodyz00_1205), BgL_xz00_16);
																					return
																						BGL_PROCEDURE_CALL2(BgL_ez00_17,
																						BgL_arg1403z00_1377, BgL_ez00_17);
																				}
																		}
																	else
																		{	/* Eval/expdsrfi0.scm 272 */
																			return
																				BGl_expandzd2errorzd2zz__expandz00
																				(BGl_string1949z00zz__expander_srfi0z00,
																				BGl_string1950z00zz__expander_srfi0z00,
																				BgL_xz00_16);
																		}
																}
															else
																{	/* Eval/expdsrfi0.scm 266 */
																	obj_t BgL_carzd2224zd2_1246;

																	BgL_carzd2224zd2_1246 =
																		CAR(((obj_t) BgL_clausez00_1185));
																	if (PAIRP(BgL_carzd2224zd2_1246))
																		{	/* Eval/expdsrfi0.scm 266 */
																			if (
																				(CAR(BgL_carzd2224zd2_1246) ==
																					BGl_symbol1954z00zz__expander_srfi0z00))
																				{	/* Eval/expdsrfi0.scm 266 */
																					if (NULLP(CDR(BgL_carzd2224zd2_1246)))
																						{	/* Eval/expdsrfi0.scm 266 */
																							obj_t BgL_arg1268z00_1252;

																							BgL_arg1268z00_1252 =
																								CDR(
																								((obj_t) BgL_clausez00_1185));
																							{	/* Eval/expdsrfi0.scm 278 */
																								obj_t BgL_arg1405z00_1963;

																								BgL_arg1405z00_1963 =
																									BGl_evepairifyz00zz__prognz00
																									(BGl_prognz00zz__expander_srfi0z00
																									(BgL_arg1268z00_1252),
																									BgL_xz00_16);
																								return
																									BGL_PROCEDURE_CALL2
																									(BgL_ez00_17,
																									BgL_arg1405z00_1963,
																									BgL_ez00_17);
																							}
																						}
																					else
																						{	/* Eval/expdsrfi0.scm 266 */
																							obj_t BgL_cdrzd2244zd2_1253;

																							BgL_cdrzd2244zd2_1253 =
																								CDR(
																								((obj_t)
																									BgL_carzd2224zd2_1246));
																							if (PAIRP(BgL_cdrzd2244zd2_1253))
																								{	/* Eval/expdsrfi0.scm 266 */
																									if (NULLP(CDR
																											(BgL_cdrzd2244zd2_1253)))
																										{	/* Eval/expdsrfi0.scm 266 */
																											obj_t BgL_arg1284z00_1257;
																											obj_t BgL_arg1304z00_1258;

																											BgL_arg1284z00_1257 =
																												CAR
																												(BgL_cdrzd2244zd2_1253);
																											BgL_arg1304z00_1258 =
																												CDR(((obj_t)
																													BgL_clausez00_1185));
																											{	/* Eval/expdsrfi0.scm 281 */
																												obj_t
																													BgL_arg1407z00_1970;
																												{	/* Eval/expdsrfi0.scm 281 */
																													obj_t
																														BgL_arg1408z00_1971;
																													{	/* Eval/expdsrfi0.scm 281 */
																														obj_t
																															BgL_arg1410z00_1972;
																														{	/* Eval/expdsrfi0.scm 281 */
																															obj_t
																																BgL_arg1411z00_1973;
																															obj_t
																																BgL_arg1412z00_1974;
																															BgL_arg1411z00_1973
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1284z00_1257,
																																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																(BgL_arg1304z00_1258,
																																	BNIL));
																															BgL_arg1412z00_1974
																																=
																																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																(BgL_elsez00_1186,
																																BNIL);
																															BgL_arg1410z00_1972
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1411z00_1973,
																																BgL_arg1412z00_1974);
																														}
																														BgL_arg1408z00_1971
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol1951z00zz__expander_srfi0z00,
																															BgL_arg1410z00_1972);
																													}
																													BgL_arg1407z00_1970 =
																														BGl_evepairifyz00zz__prognz00
																														(BgL_arg1408z00_1971,
																														BgL_xz00_16);
																												}
																												return
																													BGL_PROCEDURE_CALL2
																													(BgL_ez00_17,
																													BgL_arg1407z00_1970,
																													BgL_ez00_17);
																											}
																										}
																									else
																										{	/* Eval/expdsrfi0.scm 266 */
																											obj_t
																												BgL_cdrzd2271zd2_1259;
																											{	/* Eval/expdsrfi0.scm 266 */
																												obj_t BgL_pairz00_1977;

																												BgL_pairz00_1977 =
																													CAR(
																													((obj_t)
																														BgL_clausez00_1185));
																												BgL_cdrzd2271zd2_1259 =
																													CDR(BgL_pairz00_1977);
																											}
																											{	/* Eval/expdsrfi0.scm 266 */
																												obj_t
																													BgL_cdrzd2277zd2_1260;
																												BgL_cdrzd2277zd2_1260 =
																													CDR(((obj_t)
																														BgL_cdrzd2271zd2_1259));
																												if (PAIRP
																													(BgL_cdrzd2277zd2_1260))
																													{	/* Eval/expdsrfi0.scm 266 */
																														obj_t
																															BgL_arg1306z00_1262;
																														obj_t
																															BgL_arg1307z00_1263;
																														obj_t
																															BgL_arg1308z00_1264;
																														obj_t
																															BgL_arg1309z00_1265;
																														BgL_arg1306z00_1262
																															=
																															CAR(((obj_t)
																																BgL_cdrzd2271zd2_1259));
																														BgL_arg1307z00_1263
																															=
																															CAR
																															(BgL_cdrzd2277zd2_1260);
																														BgL_arg1308z00_1264
																															=
																															CDR
																															(BgL_cdrzd2277zd2_1260);
																														BgL_arg1309z00_1265
																															=
																															CDR(((obj_t)
																																BgL_clausez00_1185));
																														{	/* Eval/expdsrfi0.scm 287 */
																															obj_t
																																BgL_arg1414z00_1983;
																															if (NULLP
																																(BgL_arg1309z00_1265))
																																{	/* Eval/expdsrfi0.scm 287 */
																																	BgL_arg1414z00_1983
																																		=
																																		BGl_list1956z00zz__expander_srfi0z00;
																																}
																															else
																																{	/* Eval/expdsrfi0.scm 287 */
																																	BgL_arg1414z00_1983
																																		=
																																		BgL_arg1309z00_1265;
																																}
																															return
																																BGl_expandzd2condzd2expandzd2andzd2zz__expander_srfi0z00
																																(BgL_xz00_16,
																																BgL_ez00_17,
																																BgL_arg1306z00_1262,
																																BgL_arg1307z00_1263,
																																BgL_arg1308z00_1264,
																																BgL_arg1414z00_1983,
																																BgL_elsez00_1186);
																														}
																													}
																												else
																													{	/* Eval/expdsrfi0.scm 266 */
																														obj_t
																															BgL_carzd2347zd2_1266;
																														BgL_carzd2347zd2_1266
																															=
																															CAR(((obj_t)
																																BgL_clausez00_1185));
																														if (SYMBOLP
																															(BgL_carzd2347zd2_1266))
																															{	/* Eval/expdsrfi0.scm 266 */
																																obj_t
																																	BgL_arg1311z00_1268;
																																BgL_arg1311z00_1268
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_1185));
																																BgL_featurez00_1237
																																	=
																																	BgL_carzd2347zd2_1266;
																																BgL_bodyz00_1238
																																	=
																																	BgL_arg1311z00_1268;
																															BgL_tagzd2198zd2_1239:
																																{	/* Eval/expdsrfi0.scm 318 */
																																	obj_t
																																		BgL_arg1449z00_1420;
																																	{	/* Eval/expdsrfi0.scm 318 */
																																		obj_t
																																			BgL_arg1450z00_1421;
																																		if (CBOOL
																																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																				(BgL_featurez00_1237,
																																					BgL_featuresz00_18)))
																																			{	/* Eval/expdsrfi0.scm 318 */
																																				if (NULLP(BgL_bodyz00_1238))
																																					{	/* Eval/expdsrfi0.scm 319 */
																																						BgL_arg1450z00_1421
																																							=
																																							BUNSPEC;
																																					}
																																				else
																																					{	/* Eval/expdsrfi0.scm 319 */
																																						BgL_arg1450z00_1421
																																							=
																																							BGl_prognz00zz__expander_srfi0z00
																																							(BgL_bodyz00_1238);
																																					}
																																			}
																																		else
																																			{	/* Eval/expdsrfi0.scm 322 */
																																				obj_t
																																					BgL_arg1453z00_1424;
																																				BgL_arg1453z00_1424
																																					=
																																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																					(BgL_elsez00_1186,
																																					BNIL);
																																				BgL_arg1450z00_1421
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol1951z00zz__expander_srfi0z00,
																																					BgL_arg1453z00_1424);
																																			}
																																		BgL_arg1449z00_1420
																																			=
																																			BGl_evepairifyz00zz__prognz00
																																			(BgL_arg1450z00_1421,
																																			BgL_xz00_16);
																																	}
																																	return
																																		BGL_PROCEDURE_CALL2
																																		(BgL_ez00_17,
																																		BgL_arg1449z00_1420,
																																		BgL_ez00_17);
																																}
																															}
																														else
																															{	/* Eval/expdsrfi0.scm 266 */
																																return
																																	BGl_expandzd2errorzd2zz__expandz00
																																	(BGl_string1949z00zz__expander_srfi0z00,
																																	BGl_string1950z00zz__expander_srfi0z00,
																																	BgL_xz00_16);
																															}
																													}
																											}
																										}
																								}
																							else
																								{	/* Eval/expdsrfi0.scm 266 */
																									obj_t BgL_carzd2426zd2_1271;

																									BgL_carzd2426zd2_1271 =
																										CAR(
																										((obj_t)
																											BgL_clausez00_1185));
																									if (SYMBOLP
																										(BgL_carzd2426zd2_1271))
																										{	/* Eval/expdsrfi0.scm 266 */
																											obj_t BgL_arg1316z00_1273;

																											BgL_arg1316z00_1273 =
																												CDR(
																												((obj_t)
																													BgL_clausez00_1185));
																											{
																												obj_t BgL_bodyz00_2791;
																												obj_t
																													BgL_featurez00_2790;
																												BgL_featurez00_2790 =
																													BgL_carzd2426zd2_1271;
																												BgL_bodyz00_2791 =
																													BgL_arg1316z00_1273;
																												BgL_bodyz00_1238 =
																													BgL_bodyz00_2791;
																												BgL_featurez00_1237 =
																													BgL_featurez00_2790;
																												goto
																													BgL_tagzd2198zd2_1239;
																											}
																										}
																									else
																										{	/* Eval/expdsrfi0.scm 266 */
																											return
																												BGl_expandzd2errorzd2zz__expandz00
																												(BGl_string1949z00zz__expander_srfi0z00,
																												BGl_string1950z00zz__expander_srfi0z00,
																												BgL_xz00_16);
																										}
																								}
																						}
																				}
																			else
																				{	/* Eval/expdsrfi0.scm 266 */
																					if (
																						(CAR(
																								((obj_t) BgL_carzd2224zd2_1246))
																							==
																							BGl_symbol1957z00zz__expander_srfi0z00))
																						{	/* Eval/expdsrfi0.scm 266 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_carzd2224zd2_1246))))
																								{	/* Eval/expdsrfi0.scm 290 */
																									obj_t BgL_arg1416z00_1993;

																									{	/* Eval/expdsrfi0.scm 290 */
																										obj_t BgL_arg1417z00_1994;

																										{	/* Eval/expdsrfi0.scm 290 */
																											obj_t BgL_arg1418z00_1995;

																											BgL_arg1418z00_1995 =
																												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																												(BgL_elsez00_1186,
																												BNIL);
																											BgL_arg1417z00_1994 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol1951z00zz__expander_srfi0z00,
																												BgL_arg1418z00_1995);
																										}
																										BgL_arg1416z00_1993 =
																											BGl_evepairifyz00zz__prognz00
																											(BgL_arg1417z00_1994,
																											BgL_xz00_16);
																									}
																									return
																										BGL_PROCEDURE_CALL2
																										(BgL_ez00_17,
																										BgL_arg1416z00_1993,
																										BgL_ez00_17);
																								}
																							else
																								{	/* Eval/expdsrfi0.scm 266 */
																									obj_t BgL_cdrzd2486zd2_1282;

																									{	/* Eval/expdsrfi0.scm 266 */
																										obj_t BgL_pairz00_1997;

																										BgL_pairz00_1997 =
																											CAR(
																											((obj_t)
																												BgL_clausez00_1185));
																										BgL_cdrzd2486zd2_1282 =
																											CDR(BgL_pairz00_1997);
																									}
																									if (PAIRP
																										(BgL_cdrzd2486zd2_1282))
																										{	/* Eval/expdsrfi0.scm 266 */
																											if (NULLP(CDR
																													(BgL_cdrzd2486zd2_1282)))
																												{	/* Eval/expdsrfi0.scm 266 */
																													obj_t
																														BgL_arg1327z00_1286;
																													obj_t
																														BgL_arg1328z00_1287;
																													BgL_arg1327z00_1286 =
																														CAR
																														(BgL_cdrzd2486zd2_1282);
																													BgL_arg1328z00_1287 =
																														CDR(((obj_t)
																															BgL_clausez00_1185));
																													{	/* Eval/expdsrfi0.scm 293 */
																														obj_t
																															BgL_arg1419z00_2001;
																														{	/* Eval/expdsrfi0.scm 293 */
																															obj_t
																																BgL_arg1420z00_2002;
																															{	/* Eval/expdsrfi0.scm 293 */
																																obj_t
																																	BgL_arg1421z00_2003;
																																{	/* Eval/expdsrfi0.scm 293 */
																																	obj_t
																																		BgL_arg1422z00_2004;
																																	obj_t
																																		BgL_arg1423z00_2005;
																																	BgL_arg1422z00_2004
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1327z00_1286,
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_arg1328z00_1287,
																																			BNIL));
																																	BgL_arg1423z00_2005
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_elsez00_1186,
																																		BNIL);
																																	BgL_arg1421z00_2003
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1422z00_2004,
																																		BgL_arg1423z00_2005);
																																}
																																BgL_arg1420z00_2002
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_symbol1951z00zz__expander_srfi0z00,
																																	BgL_arg1421z00_2003);
																															}
																															BgL_arg1419z00_2001
																																=
																																BGl_evepairifyz00zz__prognz00
																																(BgL_arg1420z00_2002,
																																BgL_xz00_16);
																														}
																														return
																															BGL_PROCEDURE_CALL2
																															(BgL_ez00_17,
																															BgL_arg1419z00_2001,
																															BgL_ez00_17);
																													}
																												}
																											else
																												{	/* Eval/expdsrfi0.scm 266 */
																													obj_t
																														BgL_cdrzd2519zd2_1289;
																													BgL_cdrzd2519zd2_1289
																														=
																														CDR(((obj_t)
																															BgL_cdrzd2486zd2_1282));
																													if (PAIRP
																														(BgL_cdrzd2519zd2_1289))
																														{	/* Eval/expdsrfi0.scm 266 */
																															obj_t
																																BgL_arg1331z00_1291;
																															obj_t
																																BgL_arg1332z00_1292;
																															obj_t
																																BgL_arg1333z00_1293;
																															obj_t
																																BgL_arg1334z00_1294;
																															BgL_arg1331z00_1291
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd2486zd2_1282));
																															BgL_arg1332z00_1292
																																=
																																CAR
																																(BgL_cdrzd2519zd2_1289);
																															BgL_arg1333z00_1293
																																=
																																CDR
																																(BgL_cdrzd2519zd2_1289);
																															BgL_arg1334z00_1294
																																=
																																CDR(((obj_t)
																																	BgL_clausez00_1185));
																															return
																																BGl_expandzd2condzd2expandzd2orzd2zz__expander_srfi0z00
																																(BgL_xz00_16,
																																BgL_ez00_17,
																																BgL_arg1331z00_1291,
																																BgL_arg1332z00_1292,
																																BgL_arg1333z00_1293,
																																BgL_arg1334z00_1294,
																																BgL_elsez00_1186);
																														}
																													else
																														{	/* Eval/expdsrfi0.scm 266 */
																															obj_t
																																BgL_carzd2555zd2_1295;
																															BgL_carzd2555zd2_1295
																																=
																																CAR(((obj_t)
																																	BgL_clausez00_1185));
																															if (SYMBOLP
																																(BgL_carzd2555zd2_1295))
																																{	/* Eval/expdsrfi0.scm 266 */
																																	obj_t
																																		BgL_arg1336z00_1297;
																																	BgL_arg1336z00_1297
																																		=
																																		CDR(((obj_t)
																																			BgL_clausez00_1185));
																																	{
																																		obj_t
																																			BgL_bodyz00_2849;
																																		obj_t
																																			BgL_featurez00_2848;
																																		BgL_featurez00_2848
																																			=
																																			BgL_carzd2555zd2_1295;
																																		BgL_bodyz00_2849
																																			=
																																			BgL_arg1336z00_1297;
																																		BgL_bodyz00_1238
																																			=
																																			BgL_bodyz00_2849;
																																		BgL_featurez00_1237
																																			=
																																			BgL_featurez00_2848;
																																		goto
																																			BgL_tagzd2198zd2_1239;
																																	}
																																}
																															else
																																{	/* Eval/expdsrfi0.scm 266 */
																																	return
																																		BGl_expandzd2errorzd2zz__expandz00
																																		(BGl_string1949z00zz__expander_srfi0z00,
																																		BGl_string1950z00zz__expander_srfi0z00,
																																		BgL_xz00_16);
																																}
																														}
																												}
																										}
																									else
																										{	/* Eval/expdsrfi0.scm 266 */
																											obj_t
																												BgL_carzd2600zd2_1300;
																											BgL_carzd2600zd2_1300 =
																												CAR(((obj_t)
																													BgL_clausez00_1185));
																											if (SYMBOLP
																												(BgL_carzd2600zd2_1300))
																												{	/* Eval/expdsrfi0.scm 266 */
																													obj_t
																														BgL_arg1340z00_1302;
																													BgL_arg1340z00_1302 =
																														CDR(((obj_t)
																															BgL_clausez00_1185));
																													{
																														obj_t
																															BgL_bodyz00_2858;
																														obj_t
																															BgL_featurez00_2857;
																														BgL_featurez00_2857
																															=
																															BgL_carzd2600zd2_1300;
																														BgL_bodyz00_2858 =
																															BgL_arg1340z00_1302;
																														BgL_bodyz00_1238 =
																															BgL_bodyz00_2858;
																														BgL_featurez00_1237
																															=
																															BgL_featurez00_2857;
																														goto
																															BgL_tagzd2198zd2_1239;
																													}
																												}
																											else
																												{	/* Eval/expdsrfi0.scm 266 */
																													return
																														BGl_expandzd2errorzd2zz__expandz00
																														(BGl_string1949z00zz__expander_srfi0z00,
																														BGl_string1950z00zz__expander_srfi0z00,
																														BgL_xz00_16);
																												}
																										}
																								}
																						}
																					else
																						{	/* Eval/expdsrfi0.scm 266 */
																							obj_t BgL_carzd2638zd2_1305;

																							BgL_carzd2638zd2_1305 =
																								CAR(
																								((obj_t) BgL_clausez00_1185));
																							{	/* Eval/expdsrfi0.scm 266 */
																								obj_t BgL_cdrzd2642zd2_1306;

																								BgL_cdrzd2642zd2_1306 =
																									CDR(
																									((obj_t)
																										BgL_carzd2638zd2_1305));
																								if ((CAR(((obj_t)
																												BgL_carzd2638zd2_1305))
																										==
																										BGl_symbol1959z00zz__expander_srfi0z00))
																									{	/* Eval/expdsrfi0.scm 266 */
																										if (PAIRP
																											(BgL_cdrzd2642zd2_1306))
																											{	/* Eval/expdsrfi0.scm 266 */
																												if (NULLP(CDR
																														(BgL_cdrzd2642zd2_1306)))
																													{	/* Eval/expdsrfi0.scm 266 */
																														obj_t
																															BgL_arg1348z00_1312;
																														obj_t
																															BgL_arg1349z00_1313;
																														BgL_arg1348z00_1312
																															=
																															CAR
																															(BgL_cdrzd2642zd2_1306);
																														BgL_arg1349z00_1313
																															=
																															CDR(((obj_t)
																																BgL_clausez00_1185));
																														BgL_reqz00_1227 =
																															BgL_arg1348z00_1312;
																														BgL_bodyz00_1228 =
																															BgL_arg1349z00_1313;
																														{	/* Eval/expdsrfi0.scm 301 */
																															obj_t
																																BgL_arg1425z00_1398;
																															{	/* Eval/expdsrfi0.scm 301 */
																																obj_t
																																	BgL_arg1426z00_1399;
																																{	/* Eval/expdsrfi0.scm 301 */
																																	obj_t
																																		BgL_arg1427z00_1400;
																																	{	/* Eval/expdsrfi0.scm 301 */
																																		obj_t
																																			BgL_arg1428z00_1401;
																																		obj_t
																																			BgL_arg1429z00_1402;
																																		{	/* Eval/expdsrfi0.scm 301 */
																																			obj_t
																																				BgL_arg1430z00_1403;
																																			{	/* Eval/expdsrfi0.scm 301 */
																																				obj_t
																																					BgL_arg1431z00_1404;
																																				{	/* Eval/expdsrfi0.scm 301 */
																																					obj_t
																																						BgL_arg1434z00_1405;
																																					BgL_arg1434z00_1405
																																						=
																																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																						(BgL_elsez00_1186,
																																						BNIL);
																																					BgL_arg1431z00_1404
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol1951z00zz__expander_srfi0z00,
																																						BgL_arg1434z00_1405);
																																				}
																																				BgL_arg1430z00_1403
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1431z00_1404,
																																					BNIL);
																																			}
																																			BgL_arg1428z00_1401
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_reqz00_1227,
																																				BgL_arg1430z00_1403);
																																		}
																																		{	/* Eval/expdsrfi0.scm 302 */
																																			obj_t
																																				BgL_arg1435z00_1406;
																																			{	/* Eval/expdsrfi0.scm 302 */
																																				obj_t
																																					BgL_arg1436z00_1407;
																																				BgL_arg1436z00_1407
																																					=
																																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																					(BgL_bodyz00_1228,
																																					BNIL);
																																				BgL_arg1435z00_1406
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol1952z00zz__expander_srfi0z00,
																																					BgL_arg1436z00_1407);
																																			}
																																			BgL_arg1429z00_1402
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1435z00_1406,
																																				BNIL);
																																		}
																																		BgL_arg1427z00_1400
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1428z00_1401,
																																			BgL_arg1429z00_1402);
																																	}
																																	BgL_arg1426z00_1399
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol1951z00zz__expander_srfi0z00,
																																		BgL_arg1427z00_1400);
																																}
																																BgL_arg1425z00_1398
																																	=
																																	BGl_evepairifyz00zz__prognz00
																																	(BgL_arg1426z00_1399,
																																	BgL_xz00_16);
																															}
																															return
																																BGL_PROCEDURE_CALL2
																																(BgL_ez00_17,
																																BgL_arg1425z00_1398,
																																BgL_ez00_17);
																														}
																													}
																												else
																													{	/* Eval/expdsrfi0.scm 266 */
																														if (SYMBOLP
																															(BgL_carzd2638zd2_1305))
																															{	/* Eval/expdsrfi0.scm 266 */
																																obj_t
																																	BgL_arg1351z00_1316;
																																BgL_arg1351z00_1316
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_1185));
																																{
																																	obj_t
																																		BgL_bodyz00_2896;
																																	obj_t
																																		BgL_featurez00_2895;
																																	BgL_featurez00_2895
																																		=
																																		BgL_carzd2638zd2_1305;
																																	BgL_bodyz00_2896
																																		=
																																		BgL_arg1351z00_1316;
																																	BgL_bodyz00_1238
																																		=
																																		BgL_bodyz00_2896;
																																	BgL_featurez00_1237
																																		=
																																		BgL_featurez00_2895;
																																	goto
																																		BgL_tagzd2198zd2_1239;
																																}
																															}
																														else
																															{	/* Eval/expdsrfi0.scm 266 */
																																return
																																	BGl_expandzd2errorzd2zz__expandz00
																																	(BGl_string1949z00zz__expander_srfi0z00,
																																	BGl_string1950z00zz__expander_srfi0z00,
																																	BgL_xz00_16);
																															}
																													}
																											}
																										else
																											{	/* Eval/expdsrfi0.scm 266 */
																												if (SYMBOLP
																													(BgL_carzd2638zd2_1305))
																													{	/* Eval/expdsrfi0.scm 266 */
																														obj_t
																															BgL_arg1354z00_1320;
																														BgL_arg1354z00_1320
																															=
																															CDR(((obj_t)
																																BgL_clausez00_1185));
																														{
																															obj_t
																																BgL_bodyz00_2903;
																															obj_t
																																BgL_featurez00_2902;
																															BgL_featurez00_2902
																																=
																																BgL_carzd2638zd2_1305;
																															BgL_bodyz00_2903 =
																																BgL_arg1354z00_1320;
																															BgL_bodyz00_1238 =
																																BgL_bodyz00_2903;
																															BgL_featurez00_1237
																																=
																																BgL_featurez00_2902;
																															goto
																																BgL_tagzd2198zd2_1239;
																														}
																													}
																												else
																													{	/* Eval/expdsrfi0.scm 266 */
																														return
																															BGl_expandzd2errorzd2zz__expandz00
																															(BGl_string1949z00zz__expander_srfi0z00,
																															BGl_string1950z00zz__expander_srfi0z00,
																															BgL_xz00_16);
																													}
																											}
																									}
																								else
																									{	/* Eval/expdsrfi0.scm 266 */
																										obj_t BgL_cdrzd2707zd2_1322;

																										BgL_cdrzd2707zd2_1322 =
																											CDR(
																											((obj_t)
																												BgL_carzd2638zd2_1305));
																										if ((CAR(((obj_t)
																														BgL_carzd2638zd2_1305))
																												==
																												BGl_symbol1961z00zz__expander_srfi0z00))
																											{	/* Eval/expdsrfi0.scm 266 */
																												if (PAIRP
																													(BgL_cdrzd2707zd2_1322))
																													{	/* Eval/expdsrfi0.scm 266 */
																														obj_t
																															BgL_carzd2709zd2_1326;
																														BgL_carzd2709zd2_1326
																															=
																															CAR
																															(BgL_cdrzd2707zd2_1322);
																														if (SYMBOLP
																															(BgL_carzd2709zd2_1326))
																															{	/* Eval/expdsrfi0.scm 266 */
																																if (NULLP(CDR
																																		(BgL_cdrzd2707zd2_1322)))
																																	{	/* Eval/expdsrfi0.scm 266 */
																																		obj_t
																																			BgL_arg1362z00_1330;
																																		BgL_arg1362z00_1330
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_clausez00_1185));
																																		{	/* Eval/expdsrfi0.scm 306 */
																																			obj_t
																																				BgL_arg1437z00_2034;
																																			{	/* Eval/expdsrfi0.scm 306 */
																																				obj_t
																																					BgL_arg1438z00_2035;
																																				if (CBOOL(BGl_libraryzd2existszf3z21zz__libraryz00(BgL_carzd2709zd2_1326, BNIL)))
																																					{	/* Eval/expdsrfi0.scm 306 */
																																						BgL_arg1438z00_2035
																																							=
																																							BGl_prognz00zz__expander_srfi0z00
																																							(BgL_arg1362z00_1330);
																																					}
																																				else
																																					{	/* Eval/expdsrfi0.scm 308 */
																																						obj_t
																																							BgL_arg1441z00_2038;
																																						BgL_arg1441z00_2038
																																							=
																																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																							(BgL_elsez00_1186,
																																							BNIL);
																																						BgL_arg1438z00_2035
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol1951z00zz__expander_srfi0z00,
																																							BgL_arg1441z00_2038);
																																					}
																																				BgL_arg1437z00_2034
																																					=
																																					BGl_evepairifyz00zz__prognz00
																																					(BgL_arg1438z00_2035,
																																					BgL_xz00_16);
																																			}
																																			return
																																				BGL_PROCEDURE_CALL2
																																				(BgL_ez00_17,
																																				BgL_arg1437z00_2034,
																																				BgL_ez00_17);
																																		}
																																	}
																																else
																																	{	/* Eval/expdsrfi0.scm 266 */
																																		obj_t
																																			BgL_carzd2726zd2_1331;
																																		BgL_carzd2726zd2_1331
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_clausez00_1185));
																																		if (SYMBOLP
																																			(BgL_carzd2726zd2_1331))
																																			{	/* Eval/expdsrfi0.scm 266 */
																																				obj_t
																																					BgL_arg1364z00_1333;
																																				BgL_arg1364z00_1333
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_clausez00_1185));
																																				{
																																					obj_t
																																						BgL_bodyz00_2940;
																																					obj_t
																																						BgL_featurez00_2939;
																																					BgL_featurez00_2939
																																						=
																																						BgL_carzd2726zd2_1331;
																																					BgL_bodyz00_2940
																																						=
																																						BgL_arg1364z00_1333;
																																					BgL_bodyz00_1238
																																						=
																																						BgL_bodyz00_2940;
																																					BgL_featurez00_1237
																																						=
																																						BgL_featurez00_2939;
																																					goto
																																						BgL_tagzd2198zd2_1239;
																																				}
																																			}
																																		else
																																			{	/* Eval/expdsrfi0.scm 266 */
																																				return
																																					BGl_expandzd2errorzd2zz__expandz00
																																					(BGl_string1949z00zz__expander_srfi0z00,
																																					BGl_string1950z00zz__expander_srfi0z00,
																																					BgL_xz00_16);
																																			}
																																	}
																															}
																														else
																															{	/* Eval/expdsrfi0.scm 266 */
																																obj_t
																																	BgL_carzd2743zd2_1335;
																																BgL_carzd2743zd2_1335
																																	=
																																	CAR(((obj_t)
																																		BgL_clausez00_1185));
																																if (SYMBOLP
																																	(BgL_carzd2743zd2_1335))
																																	{	/* Eval/expdsrfi0.scm 266 */
																																		obj_t
																																			BgL_arg1367z00_1337;
																																		BgL_arg1367z00_1337
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_clausez00_1185));
																																		{
																																			obj_t
																																				BgL_bodyz00_2949;
																																			obj_t
																																				BgL_featurez00_2948;
																																			BgL_featurez00_2948
																																				=
																																				BgL_carzd2743zd2_1335;
																																			BgL_bodyz00_2949
																																				=
																																				BgL_arg1367z00_1337;
																																			BgL_bodyz00_1238
																																				=
																																				BgL_bodyz00_2949;
																																			BgL_featurez00_1237
																																				=
																																				BgL_featurez00_2948;
																																			goto
																																				BgL_tagzd2198zd2_1239;
																																		}
																																	}
																																else
																																	{	/* Eval/expdsrfi0.scm 266 */
																																		return
																																			BGl_expandzd2errorzd2zz__expandz00
																																			(BGl_string1949z00zz__expander_srfi0z00,
																																			BGl_string1950z00zz__expander_srfi0z00,
																																			BgL_xz00_16);
																																	}
																															}
																													}
																												else
																													{	/* Eval/expdsrfi0.scm 266 */
																														obj_t
																															BgL_carzd2760zd2_1338;
																														BgL_carzd2760zd2_1338
																															=
																															CAR(((obj_t)
																																BgL_clausez00_1185));
																														if (SYMBOLP
																															(BgL_carzd2760zd2_1338))
																															{	/* Eval/expdsrfi0.scm 266 */
																																obj_t
																																	BgL_arg1369z00_1340;
																																BgL_arg1369z00_1340
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_1185));
																																{
																																	obj_t
																																		BgL_bodyz00_2958;
																																	obj_t
																																		BgL_featurez00_2957;
																																	BgL_featurez00_2957
																																		=
																																		BgL_carzd2760zd2_1338;
																																	BgL_bodyz00_2958
																																		=
																																		BgL_arg1369z00_1340;
																																	BgL_bodyz00_1238
																																		=
																																		BgL_bodyz00_2958;
																																	BgL_featurez00_1237
																																		=
																																		BgL_featurez00_2957;
																																	goto
																																		BgL_tagzd2198zd2_1239;
																																}
																															}
																														else
																															{	/* Eval/expdsrfi0.scm 266 */
																																return
																																	BGl_expandzd2errorzd2zz__expandz00
																																	(BGl_string1949z00zz__expander_srfi0z00,
																																	BGl_string1950z00zz__expander_srfi0z00,
																																	BgL_xz00_16);
																															}
																													}
																											}
																										else
																											{	/* Eval/expdsrfi0.scm 266 */
																												obj_t
																													BgL_carzd2773zd2_1341;
																												BgL_carzd2773zd2_1341 =
																													CAR(((obj_t)
																														BgL_clausez00_1185));
																												{	/* Eval/expdsrfi0.scm 266 */
																													obj_t
																														BgL_cdrzd2778zd2_1342;
																													BgL_cdrzd2778zd2_1342
																														=
																														CDR(((obj_t)
																															BgL_carzd2773zd2_1341));
																													if ((CAR(((obj_t)
																																	BgL_carzd2773zd2_1341))
																															==
																															BGl_symbol1963z00zz__expander_srfi0z00))
																														{	/* Eval/expdsrfi0.scm 266 */
																															if (PAIRP
																																(BgL_cdrzd2778zd2_1342))
																																{	/* Eval/expdsrfi0.scm 266 */
																																	obj_t
																																		BgL_cdrzd2782zd2_1346;
																																	BgL_cdrzd2782zd2_1346
																																		=
																																		CDR
																																		(BgL_cdrzd2778zd2_1342);
																																	if (PAIRP
																																		(BgL_cdrzd2782zd2_1346))
																																		{	/* Eval/expdsrfi0.scm 266 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd2782zd2_1346)))
																																				{	/* Eval/expdsrfi0.scm 266 */
																																					obj_t
																																						BgL_arg1377z00_1350;
																																					obj_t
																																						BgL_arg1378z00_1351;
																																					obj_t
																																						BgL_arg1379z00_1352;
																																					BgL_arg1377z00_1350
																																						=
																																						CAR
																																						(BgL_cdrzd2778zd2_1342);
																																					BgL_arg1378z00_1351
																																						=
																																						CAR
																																						(BgL_cdrzd2782zd2_1346);
																																					BgL_arg1379z00_1352
																																						=
																																						CDR(
																																						((obj_t) BgL_clausez00_1185));
																																					{	/* Eval/expdsrfi0.scm 312 */
																																						obj_t
																																							BgL_arg1443z00_2053;
																																						{	/* Eval/expdsrfi0.scm 312 */
																																							obj_t
																																								BgL_arg1444z00_2054;
																																							if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BGl_bigloozd2configzd2zz__configurez00(BgL_arg1377z00_1350), BgL_arg1378z00_1351))
																																								{	/* Eval/expdsrfi0.scm 312 */
																																									BgL_arg1444z00_2054
																																										=
																																										BGl_prognz00zz__expander_srfi0z00
																																										(BgL_arg1379z00_1352);
																																								}
																																							else
																																								{	/* Eval/expdsrfi0.scm 314 */
																																									obj_t
																																										BgL_arg1447z00_2057;
																																									BgL_arg1447z00_2057
																																										=
																																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																										(BgL_elsez00_1186,
																																										BNIL);
																																									BgL_arg1444z00_2054
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_symbol1951z00zz__expander_srfi0z00,
																																										BgL_arg1447z00_2057);
																																								}
																																							BgL_arg1443z00_2053
																																								=
																																								BGl_evepairifyz00zz__prognz00
																																								(BgL_arg1444z00_2054,
																																								BgL_xz00_16);
																																						}
																																						return
																																							BGL_PROCEDURE_CALL2
																																							(BgL_ez00_17,
																																							BgL_arg1443z00_2053,
																																							BgL_ez00_17);
																																					}
																																				}
																																			else
																																				{	/* Eval/expdsrfi0.scm 266 */
																																					if (SYMBOLP(BgL_carzd2773zd2_1341))
																																						{	/* Eval/expdsrfi0.scm 266 */
																																							obj_t
																																								BgL_arg1382z00_1355;
																																							BgL_arg1382z00_1355
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_clausez00_1185));
																																							{
																																								obj_t
																																									BgL_bodyz00_2997;
																																								obj_t
																																									BgL_featurez00_2996;
																																								BgL_featurez00_2996
																																									=
																																									BgL_carzd2773zd2_1341;
																																								BgL_bodyz00_2997
																																									=
																																									BgL_arg1382z00_1355;
																																								BgL_bodyz00_1238
																																									=
																																									BgL_bodyz00_2997;
																																								BgL_featurez00_1237
																																									=
																																									BgL_featurez00_2996;
																																								goto
																																									BgL_tagzd2198zd2_1239;
																																							}
																																						}
																																					else
																																						{	/* Eval/expdsrfi0.scm 266 */
																																							return
																																								BGl_expandzd2errorzd2zz__expandz00
																																								(BGl_string1949z00zz__expander_srfi0z00,
																																								BGl_string1950z00zz__expander_srfi0z00,
																																								BgL_xz00_16);
																																						}
																																				}
																																		}
																																	else
																																		{	/* Eval/expdsrfi0.scm 266 */
																																			if (SYMBOLP(BgL_carzd2773zd2_1341))
																																				{	/* Eval/expdsrfi0.scm 266 */
																																					obj_t
																																						BgL_arg1387z00_1359;
																																					BgL_arg1387z00_1359
																																						=
																																						CDR(
																																						((obj_t) BgL_clausez00_1185));
																																					{
																																						obj_t
																																							BgL_bodyz00_3004;
																																						obj_t
																																							BgL_featurez00_3003;
																																						BgL_featurez00_3003
																																							=
																																							BgL_carzd2773zd2_1341;
																																						BgL_bodyz00_3004
																																							=
																																							BgL_arg1387z00_1359;
																																						BgL_bodyz00_1238
																																							=
																																							BgL_bodyz00_3004;
																																						BgL_featurez00_1237
																																							=
																																							BgL_featurez00_3003;
																																						goto
																																							BgL_tagzd2198zd2_1239;
																																					}
																																				}
																																			else
																																				{	/* Eval/expdsrfi0.scm 266 */
																																					return
																																						BGl_expandzd2errorzd2zz__expandz00
																																						(BGl_string1949z00zz__expander_srfi0z00,
																																						BGl_string1950z00zz__expander_srfi0z00,
																																						BgL_xz00_16);
																																				}
																																		}
																																}
																															else
																																{	/* Eval/expdsrfi0.scm 266 */
																																	if (SYMBOLP
																																		(BgL_carzd2773zd2_1341))
																																		{	/* Eval/expdsrfi0.scm 266 */
																																			obj_t
																																				BgL_arg1389z00_1362;
																																			BgL_arg1389z00_1362
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_clausez00_1185));
																																			{
																																				obj_t
																																					BgL_bodyz00_3011;
																																				obj_t
																																					BgL_featurez00_3010;
																																				BgL_featurez00_3010
																																					=
																																					BgL_carzd2773zd2_1341;
																																				BgL_bodyz00_3011
																																					=
																																					BgL_arg1389z00_1362;
																																				BgL_bodyz00_1238
																																					=
																																					BgL_bodyz00_3011;
																																				BgL_featurez00_1237
																																					=
																																					BgL_featurez00_3010;
																																				goto
																																					BgL_tagzd2198zd2_1239;
																																			}
																																		}
																																	else
																																		{	/* Eval/expdsrfi0.scm 266 */
																																			return
																																				BGl_expandzd2errorzd2zz__expandz00
																																				(BGl_string1949z00zz__expander_srfi0z00,
																																				BGl_string1950z00zz__expander_srfi0z00,
																																				BgL_xz00_16);
																																		}
																																}
																														}
																													else
																														{	/* Eval/expdsrfi0.scm 266 */
																															if (SYMBOLP
																																(BgL_carzd2773zd2_1341))
																																{	/* Eval/expdsrfi0.scm 266 */
																																	obj_t
																																		BgL_arg1391z00_1365;
																																	BgL_arg1391z00_1365
																																		=
																																		CDR(((obj_t)
																																			BgL_clausez00_1185));
																																	{
																																		obj_t
																																			BgL_bodyz00_3018;
																																		obj_t
																																			BgL_featurez00_3017;
																																		BgL_featurez00_3017
																																			=
																																			BgL_carzd2773zd2_1341;
																																		BgL_bodyz00_3018
																																			=
																																			BgL_arg1391z00_1365;
																																		BgL_bodyz00_1238
																																			=
																																			BgL_bodyz00_3018;
																																		BgL_featurez00_1237
																																			=
																																			BgL_featurez00_3017;
																																		goto
																																			BgL_tagzd2198zd2_1239;
																																	}
																																}
																															else
																																{	/* Eval/expdsrfi0.scm 266 */
																																	return
																																		BGl_expandzd2errorzd2zz__expandz00
																																		(BGl_string1949z00zz__expander_srfi0z00,
																																		BGl_string1950z00zz__expander_srfi0z00,
																																		BgL_xz00_16);
																																}
																														}
																												}
																											}
																									}
																							}
																						}
																				}
																		}
																	else
																		{	/* Eval/expdsrfi0.scm 266 */
																			if (SYMBOLP(BgL_carzd2224zd2_1246))
																				{	/* Eval/expdsrfi0.scm 266 */
																					obj_t BgL_arg1399z00_1373;

																					BgL_arg1399z00_1373 =
																						CDR(((obj_t) BgL_clausez00_1185));
																					{
																						obj_t BgL_bodyz00_3025;
																						obj_t BgL_featurez00_3024;

																						BgL_featurez00_3024 =
																							BgL_carzd2224zd2_1246;
																						BgL_bodyz00_3025 =
																							BgL_arg1399z00_1373;
																						BgL_bodyz00_1238 = BgL_bodyz00_3025;
																						BgL_featurez00_1237 =
																							BgL_featurez00_3024;
																						goto BgL_tagzd2198zd2_1239;
																					}
																				}
																			else
																				{	/* Eval/expdsrfi0.scm 266 */
																					return
																						BGl_expandzd2errorzd2zz__expandz00
																						(BGl_string1949z00zz__expander_srfi0z00,
																						BGl_string1950z00zz__expander_srfi0z00,
																						BgL_xz00_16);
																				}
																		}
																}
														}
													else
														{	/* Eval/expdsrfi0.scm 266 */
															return
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string1949z00zz__expander_srfi0z00,
																BGl_string1950z00zz__expander_srfi0z00,
																BgL_xz00_16);
														}
												}
											}
										else
											{	/* Eval/expdsrfi0.scm 266 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string1949z00zz__expander_srfi0z00,
													BGl_string1950z00zz__expander_srfi0z00, BgL_xz00_16);
											}
									}
							}
						else
							{	/* Eval/expdsrfi0.scm 266 */
								obj_t BgL_cdrzd2177zd2_1200;

								BgL_cdrzd2177zd2_1200 = CDR(((obj_t) BgL_xz00_16));
								if (PAIRP(BgL_cdrzd2177zd2_1200))
									{
										obj_t BgL_elsez00_3037;
										obj_t BgL_clausez00_3035;

										BgL_clausez00_3035 = CAR(BgL_cdrzd2177zd2_1200);
										BgL_elsez00_3037 = CDR(BgL_cdrzd2177zd2_1200);
										BgL_elsez00_1186 = BgL_elsez00_3037;
										BgL_clausez00_1185 = BgL_clausez00_3035;
										goto BgL_tagzd2153zd2_1187;
									}
								else
									{	/* Eval/expdsrfi0.scm 266 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string1949z00zz__expander_srfi0z00,
											BGl_string1950z00zz__expander_srfi0z00, BgL_xz00_16);
									}
							}
					}
			}
		}

	}



/* &expand-cond-expand */
	obj_t BGl_z62expandzd2condzd2expandz62zz__expander_srfi0z00(obj_t
		BgL_envz00_2308, obj_t BgL_xz00_2309, obj_t BgL_ez00_2310,
		obj_t BgL_featuresz00_2311)
	{
		{	/* Eval/expdsrfi0.scm 262 */
			{	/* Eval/expdsrfi0.scm 266 */
				obj_t BgL_auxz00_3054;
				obj_t BgL_auxz00_3047;
				obj_t BgL_auxz00_3040;

				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_featuresz00_2311))
					{	/* Eval/expdsrfi0.scm 266 */
						BgL_auxz00_3054 = BgL_featuresz00_2311;
					}
				else
					{
						obj_t BgL_auxz00_3057;

						BgL_auxz00_3057 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(11391L),
							BGl_string1965z00zz__expander_srfi0z00,
							BGl_string1942z00zz__expander_srfi0z00, BgL_featuresz00_2311);
						FAILURE(BgL_auxz00_3057, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_ez00_2310))
					{	/* Eval/expdsrfi0.scm 266 */
						BgL_auxz00_3047 = BgL_ez00_2310;
					}
				else
					{
						obj_t BgL_auxz00_3050;

						BgL_auxz00_3050 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(11391L),
							BGl_string1965z00zz__expander_srfi0z00,
							BGl_string1943z00zz__expander_srfi0z00, BgL_ez00_2310);
						FAILURE(BgL_auxz00_3050, BFALSE, BFALSE);
					}
				if (BGl_pairzd2orzd2nullzf3zf3zz__r4_pairs_and_lists_6_3z00
					(BgL_xz00_2309))
					{	/* Eval/expdsrfi0.scm 266 */
						BgL_auxz00_3040 = BgL_xz00_2309;
					}
				else
					{
						obj_t BgL_auxz00_3043;

						BgL_auxz00_3043 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1933z00zz__expander_srfi0z00, BINT(11391L),
							BGl_string1965z00zz__expander_srfi0z00,
							BGl_string1942z00zz__expander_srfi0z00, BgL_xz00_2309);
						FAILURE(BgL_auxz00_3043, BFALSE, BFALSE);
					}
				return
					BGl_expandzd2condzd2expandz00zz__expander_srfi0z00(BgL_auxz00_3040,
					BgL_auxz00_3047, BgL_auxz00_3054);
			}
		}

	}



/* expand-cond-expand-and */
	obj_t BGl_expandzd2condzd2expandzd2andzd2zz__expander_srfi0z00(obj_t
		BgL_xz00_19, obj_t BgL_ez00_20, obj_t BgL_req1z00_21, obj_t BgL_req2z00_22,
		obj_t BgL_reqsz00_23, obj_t BgL_bodyz00_24, obj_t BgL_elsez00_25)
	{
		{	/* Eval/expdsrfi0.scm 333 */
			{	/* Eval/expdsrfi0.scm 334 */
				obj_t BgL_ebodyz00_2076;

				BgL_ebodyz00_2076 =
					BGl_evepairifyz00zz__prognz00(BGl_prognz00zz__expander_srfi0z00
					(BgL_bodyz00_24), BgL_bodyz00_24);
				{	/* Eval/expdsrfi0.scm 337 */
					obj_t BgL_arg1454z00_2078;

					{	/* Eval/expdsrfi0.scm 337 */
						obj_t BgL_arg1455z00_2079;

						{	/* Eval/expdsrfi0.scm 337 */
							obj_t BgL_arg1456z00_2080;

							{	/* Eval/expdsrfi0.scm 337 */
								obj_t BgL_arg1457z00_2081;
								obj_t BgL_arg1458z00_2082;

								{	/* Eval/expdsrfi0.scm 337 */
									obj_t BgL_arg1459z00_2083;

									{	/* Eval/expdsrfi0.scm 337 */
										obj_t BgL_arg1460z00_2084;

										{	/* Eval/expdsrfi0.scm 337 */
											obj_t BgL_arg1461z00_2085;

											{	/* Eval/expdsrfi0.scm 337 */
												obj_t BgL_arg1462z00_2086;
												obj_t BgL_arg1463z00_2087;

												{	/* Eval/expdsrfi0.scm 337 */
													obj_t BgL_arg1464z00_2088;
													obj_t BgL_arg1465z00_2089;

													{	/* Eval/expdsrfi0.scm 337 */
														obj_t BgL_arg1466z00_2090;

														BgL_arg1466z00_2090 =
															MAKE_YOUNG_PAIR(BgL_req2z00_22,
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_reqsz00_23, BNIL));
														BgL_arg1464z00_2088 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1954z00zz__expander_srfi0z00,
															BgL_arg1466z00_2090);
													}
													BgL_arg1465z00_2089 =
														MAKE_YOUNG_PAIR(BgL_ebodyz00_2076, BNIL);
													BgL_arg1462z00_2086 =
														MAKE_YOUNG_PAIR(BgL_arg1464z00_2088,
														BgL_arg1465z00_2089);
												}
												BgL_arg1463z00_2087 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_elsez00_25, BNIL);
												BgL_arg1461z00_2085 =
													MAKE_YOUNG_PAIR(BgL_arg1462z00_2086,
													BgL_arg1463z00_2087);
											}
											BgL_arg1460z00_2084 =
												MAKE_YOUNG_PAIR(BGl_symbol1951z00zz__expander_srfi0z00,
												BgL_arg1461z00_2085);
										}
										BgL_arg1459z00_2083 =
											MAKE_YOUNG_PAIR(BgL_arg1460z00_2084, BNIL);
									}
									BgL_arg1457z00_2081 =
										MAKE_YOUNG_PAIR(BgL_req1z00_21, BgL_arg1459z00_2083);
								}
								BgL_arg1458z00_2082 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_elsez00_25, BNIL);
								BgL_arg1456z00_2080 =
									MAKE_YOUNG_PAIR(BgL_arg1457z00_2081, BgL_arg1458z00_2082);
							}
							BgL_arg1455z00_2079 =
								MAKE_YOUNG_PAIR(BGl_symbol1951z00zz__expander_srfi0z00,
								BgL_arg1456z00_2080);
						}
						BgL_arg1454z00_2078 =
							BGl_evepairifyz00zz__prognz00(BgL_arg1455z00_2079, BgL_xz00_19);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_ez00_20, BgL_arg1454z00_2078, BgL_ez00_20);
				}
			}
		}

	}



/* expand-cond-expand-or */
	obj_t BGl_expandzd2condzd2expandzd2orzd2zz__expander_srfi0z00(obj_t
		BgL_xz00_26, obj_t BgL_ez00_27, obj_t BgL_req1z00_28, obj_t BgL_req2z00_29,
		obj_t BgL_reqsz00_30, obj_t BgL_bodyz00_31, obj_t BgL_elsez00_32)
	{
		{	/* Eval/expdsrfi0.scm 346 */
			{	/* Eval/expdsrfi0.scm 347 */
				obj_t BgL_ebodyz00_2092;

				BgL_ebodyz00_2092 =
					BGl_evepairifyz00zz__prognz00(BGl_prognz00zz__expander_srfi0z00
					(BgL_bodyz00_31), BgL_bodyz00_31);
				{	/* Eval/expdsrfi0.scm 349 */
					obj_t BgL_arg1469z00_2094;

					{	/* Eval/expdsrfi0.scm 349 */
						obj_t BgL_arg1472z00_2095;

						{	/* Eval/expdsrfi0.scm 349 */
							obj_t BgL_arg1473z00_2096;

							{	/* Eval/expdsrfi0.scm 349 */
								obj_t BgL_arg1474z00_2097;
								obj_t BgL_arg1476z00_2098;

								{	/* Eval/expdsrfi0.scm 349 */
									obj_t BgL_arg1477z00_2099;

									BgL_arg1477z00_2099 =
										MAKE_YOUNG_PAIR(BgL_ebodyz00_2092, BNIL);
									BgL_arg1474z00_2097 =
										MAKE_YOUNG_PAIR(BgL_req1z00_28, BgL_arg1477z00_2099);
								}
								{	/* Eval/expdsrfi0.scm 352 */
									obj_t BgL_arg1478z00_2100;

									{	/* Eval/expdsrfi0.scm 352 */
										obj_t BgL_arg1479z00_2101;

										{	/* Eval/expdsrfi0.scm 352 */
											obj_t BgL_arg1480z00_2102;

											{	/* Eval/expdsrfi0.scm 352 */
												obj_t BgL_arg1481z00_2103;

												{	/* Eval/expdsrfi0.scm 352 */
													obj_t BgL_arg1482z00_2104;
													obj_t BgL_arg1483z00_2105;

													{	/* Eval/expdsrfi0.scm 352 */
														obj_t BgL_arg1484z00_2106;
														obj_t BgL_arg1485z00_2107;

														{	/* Eval/expdsrfi0.scm 352 */
															obj_t BgL_arg1486z00_2108;

															BgL_arg1486z00_2108 =
																MAKE_YOUNG_PAIR(BgL_req2z00_29,
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_reqsz00_30, BNIL));
															BgL_arg1484z00_2106 =
																MAKE_YOUNG_PAIR
																(BGl_symbol1957z00zz__expander_srfi0z00,
																BgL_arg1486z00_2108);
														}
														BgL_arg1485z00_2107 =
															MAKE_YOUNG_PAIR(BgL_ebodyz00_2092, BNIL);
														BgL_arg1482z00_2104 =
															MAKE_YOUNG_PAIR(BgL_arg1484z00_2106,
															BgL_arg1485z00_2107);
													}
													BgL_arg1483z00_2105 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_elsez00_32, BNIL);
													BgL_arg1481z00_2103 =
														MAKE_YOUNG_PAIR(BgL_arg1482z00_2104,
														BgL_arg1483z00_2105);
												}
												BgL_arg1480z00_2102 =
													MAKE_YOUNG_PAIR
													(BGl_symbol1951z00zz__expander_srfi0z00,
													BgL_arg1481z00_2103);
											}
											BgL_arg1479z00_2101 =
												MAKE_YOUNG_PAIR(BgL_arg1480z00_2102, BNIL);
										}
										BgL_arg1478z00_2100 =
											MAKE_YOUNG_PAIR(BGl_symbol1952z00zz__expander_srfi0z00,
											BgL_arg1479z00_2101);
									}
									BgL_arg1476z00_2098 =
										MAKE_YOUNG_PAIR(BgL_arg1478z00_2100, BNIL);
								}
								BgL_arg1473z00_2096 =
									MAKE_YOUNG_PAIR(BgL_arg1474z00_2097, BgL_arg1476z00_2098);
							}
							BgL_arg1472z00_2095 =
								MAKE_YOUNG_PAIR(BGl_symbol1951z00zz__expander_srfi0z00,
								BgL_arg1473z00_2096);
						}
						BgL_arg1469z00_2094 =
							BGl_evepairifyz00zz__prognz00(BgL_arg1472z00_2095, BgL_xz00_26);
					}
					return
						BGL_PROCEDURE_CALL2(BgL_ez00_27, BgL_arg1469z00_2094, BgL_ez00_27);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_srfi0z00(void)
	{
		{	/* Eval/expdsrfi0.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__libraryz00(143145005L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1966z00zz__expander_srfi0z00));
		}

	}

#ifdef __cplusplus
}
#endif
