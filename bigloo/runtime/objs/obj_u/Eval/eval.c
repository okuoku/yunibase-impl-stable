/*===========================================================================*/
/*   (Eval/eval.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/eval.scm -indent -o objs/obj_u/Eval/eval.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVAL_TYPE_DEFINITIONS
#define BGL___EVAL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;


#endif													// BGL___EVAL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_keyword2687z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2801z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2721z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2640z00zz__evalz00 = BUNSPEC;
	extern obj_t BGl_appendzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_symbol2804z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2724z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2726z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2808z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2728z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2definezd2patternz62zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nativezd2replzd2printerz00zz__evalz00(void);
	extern bool_t reset_eof(obj_t);
	static obj_t BGl_z62defaultzd2replzd2printerz62zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2730z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2732z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2734z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2737z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__evalz00 = BUNSPEC;
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04za2promptza2za31380ze3ze5zz__evalz00(obj_t, obj_t);
	static obj_t BGl_symbol2661z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2747z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2668z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_zc3z04exitza31447ze3ze70z60zz__evalz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_getzd2evalzd2readerz00zz__evalz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2definezd2macroz00zz__evalz00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_evalzd2modulezd2zz__evmodulez00(void);
	static obj_t BGl_za2transcriptza2z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_schemezd2reportzd2environmentz00zz__evalz00(obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04za2replzd2quitza2za31385ze3z37zz__evalz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2750z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2752z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2671z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2673z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2677z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2759z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2679z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2userzd2passza2zd2zz__evalz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2definezd2expanderz62zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bytezd2codezd2runz62zz__evalz00(obj_t, obj_t);
	static obj_t BGl_symbol2761z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2764z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2684z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2766z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zz__evalz00(void);
	static obj_t BGl_symbol2768z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2689z00zz__evalz00 = BUNSPEC;
	extern obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_symbol2773z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2775z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2777z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2698z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_evalzd2exceptionzd2handlerz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_notifyzd2interruptzd2zz__errorz00(int);
	extern obj_t BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00(obj_t, obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_internalzd2replzd2zz__evalz00(void);
	static obj_t BGl_genericzd2initzd2zz__evalz00(void);
	static obj_t BGl_symbol2781z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2783z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_z62expandzd2definezd2hygienezd2macrozb0zz__evalz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_identifierzd2syntaxzd2zz__evalz00(void);
	static obj_t BGl_objectzd2initzd2zz__evalz00(void);
	extern obj_t string_to_obj(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2userzd2passzd2nameza2z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2796z00zz__evalz00 = BUNSPEC;
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_z62exceptionz62zz__objectz00;
	static obj_t BGl_z62interactionzd2environmentzb0zz__evalz00(obj_t);
	extern obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31894ze3ze5zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62intrhdlz62zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2replzd2printerz12z12zz__evalz00(obj_t);
	extern obj_t reset_console(obj_t);
	static obj_t BGl__evalz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_evalzd2initzd2zz__evalz00(void);
	BGL_EXPORTED_DECL obj_t bgl_debug_repl(obj_t);
	extern obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	extern obj_t BGl_evmeaningz00zz__evmeaningz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__evalz00(obj_t);
	extern obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_identifierzd2syntaxzd2setz12z12zz__evalz00(obj_t);
	static obj_t BGl_z62transcriptzd2offzb0zz__evalz00(obj_t);
	extern char *BGl_datez00zz__osz00(void);
	static obj_t BGl_methodzd2initzd2zz__evalz00(void);
	static obj_t BGl_zc3z04exitza31610ze3ze70z60zz__evalz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_findzd2filezd2zz__evalz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_loadz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_everrorz00zz__everrorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2replzd2errorzd2notifierzb0zz__evalz00(obj_t);
	static obj_t BGl_z62getzd2prompterzb0zz__evalz00(obj_t);
	static obj_t BGl_evalzf2expanderzf2zz__evalz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nullzd2environmentzd2zz__evalz00(obj_t);
	extern obj_t obj_to_string(obj_t, obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31431ze3ze5zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62quitz62zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62evalzd2evaluatezd2setz12z70zz__evalz00(obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31558ze3ze70z60zz__evalz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04exitza31396ze3ze70z60zz__evalz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2replzd2quitza2zd2zz__evalz00 = BUNSPEC;
	static obj_t BGl_z62setzd2prompterz12za2zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_signalz00zz__osz00(int, obj_t);
	static obj_t BGl_z62notifyzd2assertzd2failz62zz__evalz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_close_output_port(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bytezd2codezd2runz00zz__evalz00(obj_t);
	static obj_t BGl__loadz00zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_quitz00zz__evalz00(void);
	static obj_t BGl_z62bytezd2codezd2evaluatez62zz__evalz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04exitza31441ze3ze70z60zz__evalz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_transcriptzd2offzd2zz__evalz00(void);
	static obj_t BGl_z62setzd2replzd2errorzd2notifierz12za2zz__evalz00(obj_t,
		obj_t);
	static obj_t BGl_z62transcriptzd2onzb0zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_bigloozd2loadzd2readerz00zz__paramz00(void);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl__bytezd2codezd2compilez00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_exceptionzd2notifyzd2zz__objectz00(obj_t);
	extern obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31428ze3ze5zz__evalz00(obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	extern obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evprimopz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmeaningz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__install_expandersz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__pp_circlez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62identifierzd2syntaxzb0zz__evalz00(obj_t);
	extern obj_t BGl_expandz00zz__expandz00(obj_t);
	static obj_t BGl_z62nullzd2environmentzb0zz__evalz00(obj_t, obj_t);
	static obj_t BGl_list2720z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_evalz12z12zz__evalz00(obj_t, obj_t);
	static obj_t BGl_defaultzd2evaluatezd2zz__evalz00 = BUNSPEC;
	extern obj_t BGl_z62errorz62zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31518ze3ze5zz__evalz00(obj_t);
	extern bool_t fexists(char *);
	extern obj_t BGl_extendzd2rzd2macrozd2envzd2zz__match_normaliza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_evcompilez00zz__evcompilez00(obj_t, obj_t, obj_t, obj_t,
		bool_t, obj_t, bool_t, bool_t);
	static obj_t BGl_za2replzd2numza2zd2zz__evalz00 = BUNSPEC;
	extern int bgl_debug(void);
	BGL_EXPORTED_DECL obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31608ze3ze5zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_loadvz00zz__evalz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62identifierzd2syntaxzd2setz12z70zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2replzd2printerz12z70zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31730ze3ze5zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2749z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2loadzd2verboseza2zd2zz__evalz00 = BUNSPEC;
	static obj_t BGl_za2replzd2printerza2zd2zz__evalz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__evalz00(void);
	static obj_t BGl__evalz12z12zz__evalz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__evalz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evalz00(void);
	static obj_t BGl_z62defaultzd2environmentzb0zz__evalz00(obj_t);
	static obj_t BGl_list2683z00zz__evalz00 = BUNSPEC;
	static obj_t BGl__loadqz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	static obj_t BGl_z62replz62zz__evalz00(obj_t);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62czd2debugzd2replz62zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_interactionzd2environmentzd2zz__evalz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2loadzd2pathza2zd2zz__evalz00 = BUNSPEC;
	static obj_t BGl_zc3z04exitza31891ze3ze70z60zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31555ze3ze5zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__evalz00(obj_t);
	static obj_t BGl_za2czd2debugzd2replzd2valueza2zd2zz__evalz00 = BUNSPEC;
	extern obj_t BGl_getzd2signalzd2handlerz00zz__osz00(int);
	static obj_t BGl_z62nativezd2replzd2printerz62zz__evalz00(obj_t);
	extern bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t);
	extern obj_t BGl_getzd2sourcezd2locationz00zz__readerz00(obj_t);
	static obj_t BGl_list2780z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_keyword2624z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_replz00zz__evalz00(void);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2definezd2expanderz00zz__evalz00(obj_t,
		obj_t);
	static obj_t BGl_zc3z04exitza31427ze3ze70z60zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2replzd2errorzd2notifierz12zc0zz__evalz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bytezd2codezd2compilez00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31905ze3ze5zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2prompterz12zc0zz__evalz00(obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_evcompilezd2errorzd2zz__evcompilez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2definezd2macroz62zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62quit2252z62zz__evalz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31469ze3ze5zz__evalz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2definezd2patternz00zz__evalz00(obj_t);
	static obj_t BGl_symbol2605z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_getzd2replzd2errorzd2notifierzd2zz__evalz00(void);
	BGL_EXPORTED_DECL obj_t BGl_evalzd2evaluatezd2setz12z12zz__evalz00(obj_t);
	static obj_t BGl_symbol2608z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__evalz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_transcriptzd2onzd2zz__evalz00(obj_t);
	extern obj_t
		BGl_installzd2allzd2expandersz12z12zz__install_expandersz00(void);
	static obj_t BGl_errze70ze7zz__evalz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2610z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_notifyzd2assertzd2failz00zz__evalz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2612z00zz__evalz00 = BUNSPEC;
	extern obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
	static obj_t BGl_symbol2616z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2nilza2z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2700z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2702z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2704z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2706z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_getzd2prompterzd2zz__evalz00(void);
	static obj_t BGl_symbol2708z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_zc3z04exitza31732ze3ze70z60zz__evalz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2identifierzd2syntaxza2zd2zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_expandzd2definezd2hygienezd2macrozd2zz__evalz00(obj_t, obj_t);
	static obj_t BGl_za2promptza2z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2710z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2630z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2712z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2714z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2716z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_z62schemezd2reportzd2environmentz62zz__evalz00(obj_t, obj_t);
	static obj_t BGl_symbol2636z00zz__evalz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t BGl_symbol2718z00zz__evalz00 = BUNSPEC;
	static obj_t BGl_symbol2639z00zz__evalz00 = BUNSPEC;
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bytezd2codezd2compilezd2envzd2zz__evalz00,
		BgL_bgl__byteza7d2codeza7d2c2811z00, opt_generic_entry,
		BGl__bytezd2codezd2compilez00zz__evalz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_identifierzd2syntaxzd2envz00zz__evalz00,
		BgL_bgl_za762identifierza7d22812z00,
		BGl_z62identifierzd2syntaxzb0zz__evalz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_schemezd2reportzd2environmentzd2envzd2zz__evalz00,
		BgL_bgl_za762schemeza7d2repo2813z00,
		BGl_z62schemezd2reportzd2environmentz62zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalzd2envzd2zz__evalz00,
		BgL_bgl__evalza700za7za7__eval2814za7, opt_generic_entry,
		BGl__evalz00zz__evalz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_replzd2envzd2zz__evalz00,
		BgL_bgl_za762replza762za7za7__ev2815z00, BGl_z62replz62zz__evalz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_czd2debugzd2replzd2envzd2zz__evalz00,
		BgL_bgl_za762cza7d2debugza7d2r2816za7,
		BGl_z62czd2debugzd2replz62zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2prompterz12zd2envz12zz__evalz00,
		BgL_bgl_za762setza7d2prompte2817z00, BGl_z62setzd2prompterz12za2zz__evalz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bytezd2codezd2runzd2envzd2zz__evalz00,
		BgL_bgl_za762byteza7d2codeza7d2818za7,
		BGl_z62bytezd2codezd2runz62zz__evalz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2definezd2patternzd2envzd2zz__evalz00,
		BgL_bgl_za762expandza7d2defi2819z00,
		BGl_z62expandzd2definezd2patternz62zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2604z00zz__evalz00,
		BgL_bgl_string2604za700za7za7_2820za7, "User", 4);
	      DEFINE_STRING(BGl_string2606z00zz__evalz00,
		BgL_bgl_string2606za700za7za7_2821za7, "bigloo-r5rs", 11);
	      DEFINE_STRING(BGl_string2607z00zz__evalz00,
		BgL_bgl_string2607za700za7za7_2822za7, ":=> ", 4);
	      DEFINE_STRING(BGl_string2609z00zz__evalz00,
		BgL_bgl_string2609za700za7za7_2823za7, "_", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2definezd2expanderzd2envzd2zz__evalz00,
		BgL_bgl_za762expandza7d2defi2824z00,
		BGl_z62expandzd2definezd2expanderz62zz__evalz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2602z00zz__evalz00,
		BgL_bgl_za762za7c3za704za7a2prom2825z00,
		BGl_z62zc3z04za2promptza2za31380ze3ze5zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2603z00zz__evalz00,
		BgL_bgl_za762za7c3za704za7a2repl2826z00,
		BGl_z62zc3z04za2replzd2quitza2za31385ze3z37zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2611z00zz__evalz00,
		BgL_bgl_string2611za700za7za7_2827za7, "classic", 7);
	      DEFINE_STRING(BGl_string2613z00zz__evalz00,
		BgL_bgl_string2613za700za7za7_2828za7, "new", 3);
	      DEFINE_STRING(BGl_string2614z00zz__evalz00,
		BgL_bgl_string2614za700za7za7_2829za7, "eval-evaluate-set!", 18);
	      DEFINE_STRING(BGl_string2615z00zz__evalz00,
		BgL_bgl_string2615za700za7za7_2830za7, "Illegal compiler", 16);
	      DEFINE_STRING(BGl_string2617z00zz__evalz00,
		BgL_bgl_string2617za700za7za7_2831za7, "interaction-environment", 23);
	      DEFINE_STRING(BGl_string2618z00zz__evalz00,
		BgL_bgl_string2618za700za7za7_2832za7, "/tmp/bigloo/runtime/Eval/eval.scm",
		33);
	      DEFINE_STRING(BGl_string2619z00zz__evalz00,
		BgL_bgl_string2619za700za7za7_2833za7, "_eval", 5);
	      DEFINE_STRING(BGl_string2701z00zz__evalz00,
		BgL_bgl_string2701za700za7za7_2834za7, "x1", 2);
	      DEFINE_STRING(BGl_string2620z00zz__evalz00,
		BgL_bgl_string2620za700za7za7_2835za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2621z00zz__evalz00,
		BgL_bgl_string2621za700za7za7_2836za7, "eval", 4);
	      DEFINE_STRING(BGl_string2703z00zz__evalz00,
		BgL_bgl_string2703za700za7za7_2837za7, "epair?", 6);
	      DEFINE_STRING(BGl_string2622z00zz__evalz00,
		BgL_bgl_string2622za700za7za7_2838za7, "_eval!", 6);
	      DEFINE_STRING(BGl_string2623z00zz__evalz00,
		BgL_bgl_string2623za700za7za7_2839za7, "eval!", 5);
	      DEFINE_STRING(BGl_string2705z00zz__evalz00,
		BgL_bgl_string2705za700za7za7_2840za7, "cer", 3);
	      DEFINE_STRING(BGl_string2625z00zz__evalz00,
		BgL_bgl_string2625za700za7za7_2841za7, "<@exit:1396>~0", 14);
	      DEFINE_STRING(BGl_string2707z00zz__evalz00,
		BgL_bgl_string2707za700za7za7_2842za7, "?l", 2);
	      DEFINE_STRING(BGl_string2626z00zz__evalz00,
		BgL_bgl_string2626za700za7za7_2843za7, "pair", 4);
	      DEFINE_STRING(BGl_string2627z00zz__evalz00,
		BgL_bgl_string2627za700za7za7_2844za7, "eval-exception-handler", 22);
	      DEFINE_STRING(BGl_string2709z00zz__evalz00,
		BgL_bgl_string2709za700za7za7_2845za7, "?f", 2);
	      DEFINE_STRING(BGl_string2628z00zz__evalz00,
		BgL_bgl_string2628za700za7za7_2846za7, "vector", 6);
	      DEFINE_STRING(BGl_string2629z00zz__evalz00,
		BgL_bgl_string2629za700za7za7_2847za7, "&exception", 10);
	extern obj_t BGl_displayzd2circlezd2envz00zz__pp_circlez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_loadzd2envzd2zz__evalz00,
		BgL_bgl__loadza700za7za7__eval2848za7, opt_generic_entry,
		BGl__loadz00zz__evalz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2replzd2errorzd2notifierzd2envz00zz__evalz00,
		BgL_bgl_za762getza7d2replza7d22849za7,
		BGl_z62getzd2replzd2errorzd2notifierzb0zz__evalz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2711z00zz__evalz00,
		BgL_bgl_string2711za700za7za7_2850za7, "f", 1);
	      DEFINE_STRING(BGl_string2631z00zz__evalz00,
		BgL_bgl_string2631za700za7za7_2851za7, "at", 2);
	      DEFINE_STRING(BGl_string2713z00zz__evalz00,
		BgL_bgl_string2713za700za7za7_2852za7, "set!", 4);
	      DEFINE_STRING(BGl_string2632z00zz__evalz00,
		BgL_bgl_string2632za700za7za7_2853za7, "&byte-code-run", 14);
	      DEFINE_STRING(BGl_string2633z00zz__evalz00,
		BgL_bgl_string2633za700za7za7_2854za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2715z00zz__evalz00,
		BgL_bgl_string2715za700za7za7_2855za7, "l", 1);
	      DEFINE_STRING(BGl_string2634z00zz__evalz00,
		BgL_bgl_string2634za700za7za7_2856za7, "scheme-report-environment", 25);
	      DEFINE_STRING(BGl_string2635z00zz__evalz00,
		BgL_bgl_string2635za700za7za7_2857za7, "bint", 4);
	      DEFINE_STRING(BGl_string2717z00zz__evalz00,
		BgL_bgl_string2717za700za7za7_2858za7, "match-case", 10);
	      DEFINE_STRING(BGl_string2637z00zz__evalz00,
		BgL_bgl_string2637za700za7za7_2859za7, "Version not supported", 21);
	      DEFINE_STRING(BGl_string2719z00zz__evalz00,
		BgL_bgl_string2719za700za7za7_2860za7, "when", 4);
	      DEFINE_STRING(BGl_string2638z00zz__evalz00,
		BgL_bgl_string2638za700za7za7_2861za7, "null-environment", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_transcriptzd2onzd2envz00zz__evalz00,
		BgL_bgl_za762transcriptza7d22862z00, BGl_z62transcriptzd2onzb0zz__evalz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2800z00zz__evalz00,
		BgL_bgl_string2800za700za7za7_2863za7, ";; session started on ", 22);
	      DEFINE_STRING(BGl_string2802z00zz__evalz00,
		BgL_bgl_string2802za700za7za7_2864za7, "A transcript is already in use",
		30);
	      DEFINE_STRING(BGl_string2803z00zz__evalz00,
		BgL_bgl_string2803za700za7za7_2865za7, "&transcript-on", 14);
	      DEFINE_STRING(BGl_string2722z00zz__evalz00,
		BgL_bgl_string2722za700za7za7_2866za7, "cdr", 3);
	      DEFINE_STRING(BGl_string2641z00zz__evalz00,
		BgL_bgl_string2641za700za7za7_2867za7, "set-prompter!", 13);
	      DEFINE_STRING(BGl_string2723z00zz__evalz00,
		BgL_bgl_string2723za700za7za7_2868za7, "TAG-254", 7);
	      DEFINE_STRING(BGl_string2642z00zz__evalz00,
		BgL_bgl_string2642za700za7za7_2869za7,
		"argument has to be a procedure of 1 argument", 44);
	      DEFINE_STRING(BGl_string2805z00zz__evalz00,
		BgL_bgl_string2805za700za7za7_2870za7, "transcript-off", 14);
	      DEFINE_STRING(BGl_string2643z00zz__evalz00,
		BgL_bgl_string2643za700za7za7_2871za7, "&set-prompter!", 14);
	      DEFINE_STRING(BGl_string2806z00zz__evalz00,
		BgL_bgl_string2806za700za7za7_2872za7, "No transcript is currently in use",
		33);
	      DEFINE_STRING(BGl_string2725z00zz__evalz00,
		BgL_bgl_string2725za700za7za7_2873za7, "let*", 4);
	      DEFINE_STRING(BGl_string2644z00zz__evalz00,
		BgL_bgl_string2644za700za7za7_2874za7, "get-prompter", 12);
	      DEFINE_STRING(BGl_string2807z00zz__evalz00,
		BgL_bgl_string2807za700za7za7_2875za7, "__eval", 6);
	      DEFINE_STRING(BGl_string2645z00zz__evalz00,
		BgL_bgl_string2645za700za7za7_2876za7, "<@exit:1427>~0", 14);
	      DEFINE_STRING(BGl_string2727z00zz__evalz00,
		BgL_bgl_string2727za700za7za7_2877za7, "n", 1);
	      DEFINE_STRING(BGl_string2809z00zz__evalz00,
		BgL_bgl_string2809za700za7za7_2878za7, "*c-debug-repl-value*", 20);
	      DEFINE_STRING(BGl_string2647z00zz__evalz00,
		BgL_bgl_string2647za700za7za7_2879za7, "<@anonymous:1431>", 17);
	      DEFINE_STRING(BGl_string2729z00zz__evalz00,
		BgL_bgl_string2729za700za7za7_2880za7, "ne", 2);
	      DEFINE_STRING(BGl_string2648z00zz__evalz00,
		BgL_bgl_string2648za700za7za7_2881za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2649z00zz__evalz00,
		BgL_bgl_string2649za700za7za7_2882za7, "set-repl-error-notifier!", 24);
	      DEFINE_STRING(BGl_string2810z00zz__evalz00,
		BgL_bgl_string2810za700za7za7_2883za7, "Eval/eval.scm", 13);
	      DEFINE_STRING(BGl_string2731z00zz__evalz00,
		BgL_bgl_string2731za700za7za7_2884za7, "evepairify*", 11);
	      DEFINE_STRING(BGl_string2650z00zz__evalz00,
		BgL_bgl_string2650za700za7za7_2885za7, "procedure of 1 argument expected",
		32);
	      DEFINE_STRING(BGl_string2651z00zz__evalz00,
		BgL_bgl_string2651za700za7za7_2886za7, "get-repl-error-notifier", 23);
	      DEFINE_STRING(BGl_string2733z00zz__evalz00,
		BgL_bgl_string2733za700za7za7_2887za7, "let", 3);
	      DEFINE_STRING(BGl_string2652z00zz__evalz00,
		BgL_bgl_string2652za700za7za7_2888za7, "internal-repl", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2646z00zz__evalz00,
		BgL_bgl_za762za7c3za704anonymo2889za7,
		BGl_z62zc3z04anonymousza31431ze3ze5zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2653z00zz__evalz00,
		BgL_bgl_string2653za700za7za7_2890za7, "liip", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_identifierzd2syntaxzd2setz12zd2envzc0zz__evalz00,
		BgL_bgl_za762identifierza7d22891z00,
		BGl_z62identifierzd2syntaxzd2setz12z70zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2735z00zz__evalz00,
		BgL_bgl_string2735za700za7za7_2892za7, "lambda", 6);
	      DEFINE_STRING(BGl_string2654z00zz__evalz00,
		BgL_bgl_string2654za700za7za7_2893za7, "output-port", 11);
	      DEFINE_STRING(BGl_string2736z00zz__evalz00,
		BgL_bgl_string2736za700za7za7_2894za7, "expand-define-macro", 19);
	      DEFINE_STRING(BGl_string2655z00zz__evalz00,
		BgL_bgl_string2655za700za7za7_2895za7, ";; ", 3);
	      DEFINE_STRING(BGl_string2656z00zz__evalz00,
		BgL_bgl_string2656za700za7za7_2896za7, "<@exit:1447>~0", 14);
	      DEFINE_STRING(BGl_string2738z00zz__evalz00,
		BgL_bgl_string2738za700za7za7_2897za7, "define-macro", 12);
	      DEFINE_STRING(BGl_string2657z00zz__evalz00,
		BgL_bgl_string2657za700za7za7_2898za7, "handler1083", 11);
	      DEFINE_STRING(BGl_string2739z00zz__evalz00,
		BgL_bgl_string2739za700za7za7_2899za7, "Illegal `define-macro' syntax", 29);
	      DEFINE_STRING(BGl_string2658z00zz__evalz00,
		BgL_bgl_string2658za700za7za7_2900za7, "&error", 6);
	      DEFINE_STRING(BGl_string2659z00zz__evalz00,
		BgL_bgl_string2659za700za7za7_2901za7, "intrhdl", 7);
	      DEFINE_STRING(BGl_string2740z00zz__evalz00,
		BgL_bgl_string2740za700za7za7_2902za7, "handler1141", 11);
	      DEFINE_STRING(BGl_string2741z00zz__evalz00,
		BgL_bgl_string2741za700za7za7_2903za7, "TAG-312", 7);
	      DEFINE_STRING(BGl_string2742z00zz__evalz00,
		BgL_bgl_string2742za700za7za7_2904za7, "<@exit:1610>~0", 14);
	      DEFINE_STRING(BGl_string2743z00zz__evalz00,
		BgL_bgl_string2743za700za7za7_2905za7, "TAG-330", 7);
	      DEFINE_STRING(BGl_string2662z00zz__evalz00,
		BgL_bgl_string2662za700za7za7_2906za7, "set-repl-printer!", 17);
	      DEFINE_STRING(BGl_string2744z00zz__evalz00,
		BgL_bgl_string2744za700za7za7_2907za7, "<@anonymous:1799>", 17);
	      DEFINE_STRING(BGl_string2663z00zz__evalz00,
		BgL_bgl_string2663za700za7za7_2908za7, "Illegal repl-printer (wrong arity)",
		34);
	      DEFINE_STRING(BGl_string2745z00zz__evalz00,
		BgL_bgl_string2745za700za7za7_2909za7, "map", 3);
	      DEFINE_STRING(BGl_string2664z00zz__evalz00,
		BgL_bgl_string2664za700za7za7_2910za7, "&set-repl-printer!", 18);
	      DEFINE_STRING(BGl_string2746z00zz__evalz00,
		BgL_bgl_string2746za700za7za7_2911za7, "list", 4);
	      DEFINE_STRING(BGl_string2665z00zz__evalz00,
		BgL_bgl_string2665za700za7za7_2912za7, "?* ", 3);
	      DEFINE_STRING(BGl_string2666z00zz__evalz00,
		BgL_bgl_string2666za700za7za7_2913za7, "loop", 4);
	      DEFINE_STRING(BGl_string2748z00zz__evalz00,
		BgL_bgl_string2748za700za7za7_2914za7, "x", 1);
	      DEFINE_STRING(BGl_string2667z00zz__evalz00,
		BgL_bgl_string2667za700za7za7_2915za7, "quit", 4);
	      DEFINE_STRING(BGl_string2669z00zz__evalz00,
		BgL_bgl_string2669za700za7za7_2916za7, "find-file", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2660z00zz__evalz00,
		BgL_bgl_za762za7c3za704anonymo2917za7,
		BGl_z62zc3z04anonymousza31469ze3ze5zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2751z00zz__evalz00,
		BgL_bgl_string2751za700za7za7_2918za7, "quote", 5);
	      DEFINE_STRING(BGl_string2670z00zz__evalz00,
		BgL_bgl_string2670za700za7za7_2919za7, "Illegal file name", 17);
	      DEFINE_STRING(BGl_string2753z00zz__evalz00,
		BgL_bgl_string2753za700za7za7_2920za7, "define-hygiene-macro", 20);
	      DEFINE_STRING(BGl_string2672z00zz__evalz00,
		BgL_bgl_string2672za700za7za7_2921za7, "load", 4);
	      DEFINE_STRING(BGl_string2754z00zz__evalz00,
		BgL_bgl_string2754za700za7za7_2922za7, "expand-define-hygiene-macro", 27);
	      DEFINE_STRING(BGl_string2755z00zz__evalz00,
		BgL_bgl_string2755za700za7za7_2923za7,
		"Illegal `define-hygiene-macro' syntax", 37);
	      DEFINE_STRING(BGl_string2674z00zz__evalz00,
		BgL_bgl_string2674za700za7za7_2924za7, "loadq", 5);
	      DEFINE_STRING(BGl_string2756z00zz__evalz00,
		BgL_bgl_string2756za700za7za7_2925za7, "handler1166", 11);
	      DEFINE_STRING(BGl_string2675z00zz__evalz00,
		BgL_bgl_string2675za700za7za7_2926za7, "loadv", 5);
	      DEFINE_STRING(BGl_string2757z00zz__evalz00,
		BgL_bgl_string2757za700za7za7_2927za7, "TAG-361", 7);
	      DEFINE_STRING(BGl_string2676z00zz__evalz00,
		BgL_bgl_string2676za700za7za7_2928za7, "epair", 5);
	      DEFINE_STRING(BGl_string2758z00zz__evalz00,
		BgL_bgl_string2758za700za7za7_2929za7, "<@exit:1732>~0", 14);
	      DEFINE_STRING(BGl_string2678z00zz__evalz00,
		BgL_bgl_string2678za700za7za7_2930za7, "module", 6);
	      DEFINE_STRING(BGl_string2760z00zz__evalz00,
		BgL_bgl_string2760za700za7za7_2931za7, "null?", 5);
	      DEFINE_STRING(BGl_string2680z00zz__evalz00,
		BgL_bgl_string2680za700za7za7_2932za7, "main", 4);
	      DEFINE_STRING(BGl_string2762z00zz__evalz00,
		BgL_bgl_string2762za700za7za7_2933za7, "not", 3);
	      DEFINE_STRING(BGl_string2681z00zz__evalz00,
		BgL_bgl_string2681za700za7za7_2934za7, "pair-nil", 8);
	      DEFINE_STRING(BGl_string2763z00zz__evalz00,
		BgL_bgl_string2763za700za7za7_2935za7, "Too many arguments provided", 27);
	      DEFINE_STRING(BGl_string2682z00zz__evalz00,
		BgL_bgl_string2682za700za7za7_2936za7, "Illegal main clause", 19);
	      DEFINE_STRING(BGl_string2765z00zz__evalz00,
		BgL_bgl_string2765za700za7za7_2937za7, "if", 2);
	      DEFINE_STRING(BGl_string2685z00zz__evalz00,
		BgL_bgl_string2685za700za7za7_2938za7, "command-line", 12);
	      DEFINE_STRING(BGl_string2767z00zz__evalz00,
		BgL_bgl_string2767za700za7za7_2939za7, "pair?", 5);
	      DEFINE_STRING(BGl_string2686z00zz__evalz00,
		BgL_bgl_string2686za700za7za7_2940za7, "Can't open file", 15);
	      DEFINE_STRING(BGl_string2769z00zz__evalz00,
		BgL_bgl_string2769za700za7za7_2941za7, "car", 3);
	      DEFINE_STRING(BGl_string2688z00zz__evalz00,
		BgL_bgl_string2688za700za7za7_2942za7, "expand-define-expander", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2definezd2hygienezd2macrozd2envz00zz__evalz00,
		BgL_bgl_za762expandza7d2defi2943z00,
		BGl_z62expandzd2definezd2hygienezd2macrozb0zz__evalz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2770z00zz__evalz00,
		BgL_bgl_string2770za700za7za7_2944za7, "Missing value for argument", 26);
	      DEFINE_STRING(BGl_string2771z00zz__evalz00,
		BgL_bgl_string2771za700za7za7_2945za7, "loop~0", 6);
	      DEFINE_STRING(BGl_string2690z00zz__evalz00,
		BgL_bgl_string2690za700za7za7_2946za7, "define-expander", 15);
	      DEFINE_STRING(BGl_string2772z00zz__evalz00,
		BgL_bgl_string2772za700za7za7_2947za7, "Illegal macro parameter", 23);
	      DEFINE_STRING(BGl_string2691z00zz__evalz00,
		BgL_bgl_string2691za700za7za7_2948za7, "Illegal `define-expander' syntax",
		32);
	      DEFINE_STRING(BGl_string2692z00zz__evalz00,
		BgL_bgl_string2692za700za7za7_2949za7, "handler1115", 11);
	      DEFINE_STRING(BGl_string2774z00zz__evalz00,
		BgL_bgl_string2774za700za7za7_2950za7, "string?", 7);
	      DEFINE_STRING(BGl_string2693z00zz__evalz00,
		BgL_bgl_string2693za700za7za7_2951za7, "TAG-118", 7);
	      DEFINE_STRING(BGl_string2694z00zz__evalz00,
		BgL_bgl_string2694za700za7za7_2952za7, "<@anonymous:1555>", 17);
	      DEFINE_STRING(BGl_string2776z00zz__evalz00,
		BgL_bgl_string2776za700za7za7_2953za7, "error/location", 14);
	      DEFINE_STRING(BGl_string2695z00zz__evalz00,
		BgL_bgl_string2695za700za7za7_2954za7,
		"wrong number of argument for expand", 35);
	      DEFINE_STRING(BGl_string2696z00zz__evalz00,
		BgL_bgl_string2696za700za7za7_2955za7, "illegal expander", 16);
	      DEFINE_STRING(BGl_string2778z00zz__evalz00,
		BgL_bgl_string2778za700za7za7_2956za7, "error", 5);
	      DEFINE_STRING(BGl_string2697z00zz__evalz00,
		BgL_bgl_string2697za700za7za7_2957za7, "<@exit:1558>~0", 14);
	      DEFINE_STRING(BGl_string2779z00zz__evalz00,
		BgL_bgl_string2779za700za7za7_2958za7, "expand-define-pattern", 21);
	      DEFINE_STRING(BGl_string2699z00zz__evalz00,
		BgL_bgl_string2699za700za7za7_2959za7, "e", 1);
	extern obj_t BGl_expandzd2envzd2zz__expandz00;
	   
		 
		DEFINE_STRING(BGl_string2782z00zz__evalz00,
		BgL_bgl_string2782za700za7za7_2960za7, "dummy", 5);
	      DEFINE_STRING(BGl_string2784z00zz__evalz00,
		BgL_bgl_string2784za700za7za7_2961za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string2785z00zz__evalz00,
		BgL_bgl_string2785za700za7za7_2962za7, "-----------------------", 23);
	      DEFINE_STRING(BGl_string2786z00zz__evalz00,
		BgL_bgl_string2786za700za7za7_2963za7, "Variables' value are : ", 23);
	      DEFINE_STRING(BGl_string2787z00zz__evalz00,
		BgL_bgl_string2787za700za7za7_2964za7, "   ", 3);
	      DEFINE_STRING(BGl_string2788z00zz__evalz00,
		BgL_bgl_string2788za700za7za7_2965za7, " : ", 3);
	      DEFINE_STRING(BGl_string2789z00zz__evalz00,
		BgL_bgl_string2789za700za7za7_2966za7, "<@anonymous:1899>", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_evalzd2evaluatezd2setz12zd2envzc0zz__evalz00,
		BgL_bgl_za762evalza7d2evalua2967z00,
		BGl_z62evalzd2evaluatezd2setz12z70zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2790z00zz__evalz00,
		BgL_bgl_string2790za700za7za7_2968za7, "for-each", 8);
	      DEFINE_STRING(BGl_string2791z00zz__evalz00,
		BgL_bgl_string2791za700za7za7_2969za7, "notify-assert-fail", 18);
	      DEFINE_STRING(BGl_string2793z00zz__evalz00,
		BgL_bgl_string2793za700za7za7_2970za7, "*:=> ", 5);
	      DEFINE_STRING(BGl_string2794z00zz__evalz00,
		BgL_bgl_string2794za700za7za7_2971za7, "assert", 6);
	      DEFINE_STRING(BGl_string2795z00zz__evalz00,
		BgL_bgl_string2795za700za7za7_2972za7, "assertion failed", 16);
	      DEFINE_STRING(BGl_string2797z00zz__evalz00,
		BgL_bgl_string2797za700za7za7_2973za7, "&identifier-syntax-set!", 23);
	      DEFINE_STRING(BGl_string2798z00zz__evalz00,
		BgL_bgl_string2798za700za7za7_2974za7, "symbol", 6);
	      DEFINE_STRING(BGl_string2799z00zz__evalz00,
		BgL_bgl_string2799za700za7za7_2975za7, "transcript-on", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2792z00zz__evalz00,
		BgL_bgl_za762za7c3za704anonymo2976za7,
		BGl_z62zc3z04anonymousza31905ze3ze5zz__evalz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2prompterzd2envz00zz__evalz00,
		BgL_bgl_za762getza7d2prompte2977z00, BGl_z62getzd2prompterzb0zz__evalz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evalz12zd2envzc0zz__evalz00,
		BgL_bgl__evalza712za712za7za7__e2978z00, opt_generic_entry,
		BGl__evalz12z12zz__evalz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_loadqzd2envzd2zz__evalz00,
		BgL_bgl__loadqza700za7za7__eva2979za7, opt_generic_entry,
		BGl__loadqz00zz__evalz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_transcriptzd2offzd2envz00zz__evalz00,
		BgL_bgl_za762transcriptza7d22980z00, BGl_z62transcriptzd2offzb0zz__evalz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_defaultzd2replzd2printerzd2envzd2zz__evalz00,
		BgL_bgl_za762defaultza7d2rep2981z00, va_generic_entry,
		BGl_z62defaultzd2replzd2printerz62zz__evalz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_defaultzd2environmentzd2envz00zz__evalz00,
		BgL_bgl_za762defaultza7d2env2982z00,
		BGl_z62defaultzd2environmentzb0zz__evalz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_quitzd2envzd2zz__evalz00,
		BgL_bgl_za762quit2252za762za7za72983z00, BGl_z62quit2252z62zz__evalz00, 0L,
		BUNSPEC, 0);
	extern obj_t BGl_expandz12zd2envzc0zz__expandz00;
	extern obj_t BGl_evaluate2zd2envzd2zz__evaluatez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00,
		BgL_bgl_za762byteza7d2codeza7d2984za7,
		BGl_z62bytezd2codezd2evaluatez62zz__evalz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2replzd2errorzd2notifierz12zd2envz12zz__evalz00,
		BgL_bgl_za762setza7d2replza7d22985za7,
		BGl_z62setzd2replzd2errorzd2notifierz12za2zz__evalz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2definezd2macrozd2envzd2zz__evalz00,
		BgL_bgl_za762expandza7d2defi2986z00,
		BGl_z62expandzd2definezd2macroz62zz__evalz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_interactionzd2environmentzd2envz00zz__evalz00,
		BgL_bgl_za762interactionza7d2987z00,
		BGl_z62interactionzd2environmentzb0zz__evalz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_nativezd2replzd2printerzd2envzd2zz__evalz00,
		BgL_bgl_za762nativeza7d2repl2988z00,
		BGl_z62nativezd2replzd2printerz62zz__evalz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nullzd2environmentzd2envz00zz__evalz00,
		BgL_bgl_za762nullza7d2enviro2989z00,
		BGl_z62nullzd2environmentzb0zz__evalz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2replzd2printerz12zd2envzc0zz__evalz00,
		BgL_bgl_za762setza7d2replza7d22990za7,
		BGl_z62setzd2replzd2printerz12z70zz__evalz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_notifyzd2assertzd2failzd2envzd2zz__evalz00,
		BgL_bgl_za762notifyza7d2asse2991z00,
		BGl_z62notifyzd2assertzd2failz62zz__evalz00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_keyword2687z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2801z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2721z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2640z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2804z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2724z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2726z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2808z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2728z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2730z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2732z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2734z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2737z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2661z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2747z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2668z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2transcriptza2z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2750z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2752z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2671z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2673z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2677z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2759z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2679z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2userzd2passza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2761z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2764z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2684z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2766z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2768z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2689z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2773z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2775z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2777z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2698z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2781z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2783z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2userzd2passzd2nameza2z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2796z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2replzd2quitza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_list2720z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_defaultzd2evaluatezd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2replzd2numza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_list2749z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2loadzd2verboseza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2replzd2printerza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_list2683z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2loadzd2pathza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2czd2debugzd2replzd2valueza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_list2780z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_keyword2624z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2605z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2608z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2610z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2612z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2616z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2nilza2z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2700z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2702z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2704z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2706z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2708z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2identifierzd2syntaxza2zd2zz__evalz00));
		     ADD_ROOT((void *) (&BGl_za2promptza2z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2710z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2630z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2712z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2714z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2716z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2636z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2718z00zz__evalz00));
		     ADD_ROOT((void *) (&BGl_symbol2639z00zz__evalz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long
		BgL_checksumz00_5697, char *BgL_fromz00_5698)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evalz00))
				{
					BGl_requirezd2initializa7ationz75zz__evalz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evalz00();
					BGl_cnstzd2initzd2zz__evalz00();
					BGl_importedzd2moduleszd2initz00zz__evalz00();
					BGl_evalzd2initzd2zz__evalz00();
					return BGl_toplevelzd2initzd2zz__evalz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			BGl_symbol2605z00zz__evalz00 =
				bstring_to_symbol(BGl_string2606z00zz__evalz00);
			BGl_symbol2608z00zz__evalz00 =
				bstring_to_symbol(BGl_string2609z00zz__evalz00);
			BGl_symbol2610z00zz__evalz00 =
				bstring_to_symbol(BGl_string2611z00zz__evalz00);
			BGl_symbol2612z00zz__evalz00 =
				bstring_to_symbol(BGl_string2613z00zz__evalz00);
			BGl_symbol2616z00zz__evalz00 =
				bstring_to_symbol(BGl_string2617z00zz__evalz00);
			BGl_keyword2624z00zz__evalz00 =
				bstring_to_keyword(BGl_string2621z00zz__evalz00);
			BGl_symbol2630z00zz__evalz00 =
				bstring_to_symbol(BGl_string2631z00zz__evalz00);
			BGl_symbol2636z00zz__evalz00 =
				bstring_to_symbol(BGl_string2634z00zz__evalz00);
			BGl_symbol2639z00zz__evalz00 =
				bstring_to_symbol(BGl_string2638z00zz__evalz00);
			BGl_symbol2640z00zz__evalz00 =
				bstring_to_symbol(BGl_string2641z00zz__evalz00);
			BGl_symbol2661z00zz__evalz00 =
				bstring_to_symbol(BGl_string2662z00zz__evalz00);
			BGl_symbol2668z00zz__evalz00 =
				bstring_to_symbol(BGl_string2669z00zz__evalz00);
			BGl_symbol2671z00zz__evalz00 =
				bstring_to_symbol(BGl_string2672z00zz__evalz00);
			BGl_symbol2673z00zz__evalz00 =
				bstring_to_symbol(BGl_string2674z00zz__evalz00);
			BGl_symbol2677z00zz__evalz00 =
				bstring_to_symbol(BGl_string2678z00zz__evalz00);
			BGl_symbol2679z00zz__evalz00 =
				bstring_to_symbol(BGl_string2680z00zz__evalz00);
			BGl_symbol2684z00zz__evalz00 =
				bstring_to_symbol(BGl_string2685z00zz__evalz00);
			BGl_list2683z00zz__evalz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2684z00zz__evalz00, BNIL);
			BGl_keyword2687z00zz__evalz00 =
				bstring_to_keyword(BGl_string2623z00zz__evalz00);
			BGl_symbol2689z00zz__evalz00 =
				bstring_to_symbol(BGl_string2690z00zz__evalz00);
			BGl_symbol2698z00zz__evalz00 =
				bstring_to_symbol(BGl_string2699z00zz__evalz00);
			BGl_symbol2700z00zz__evalz00 =
				bstring_to_symbol(BGl_string2701z00zz__evalz00);
			BGl_symbol2702z00zz__evalz00 =
				bstring_to_symbol(BGl_string2703z00zz__evalz00);
			BGl_symbol2704z00zz__evalz00 =
				bstring_to_symbol(BGl_string2705z00zz__evalz00);
			BGl_symbol2706z00zz__evalz00 =
				bstring_to_symbol(BGl_string2707z00zz__evalz00);
			BGl_symbol2708z00zz__evalz00 =
				bstring_to_symbol(BGl_string2709z00zz__evalz00);
			BGl_symbol2710z00zz__evalz00 =
				bstring_to_symbol(BGl_string2711z00zz__evalz00);
			BGl_symbol2712z00zz__evalz00 =
				bstring_to_symbol(BGl_string2713z00zz__evalz00);
			BGl_symbol2714z00zz__evalz00 =
				bstring_to_symbol(BGl_string2715z00zz__evalz00);
			BGl_symbol2716z00zz__evalz00 =
				bstring_to_symbol(BGl_string2717z00zz__evalz00);
			BGl_symbol2718z00zz__evalz00 =
				bstring_to_symbol(BGl_string2719z00zz__evalz00);
			BGl_symbol2721z00zz__evalz00 =
				bstring_to_symbol(BGl_string2722z00zz__evalz00);
			BGl_list2720z00zz__evalz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2721z00zz__evalz00,
				MAKE_YOUNG_PAIR(BGl_symbol2700z00zz__evalz00, BNIL));
			BGl_symbol2724z00zz__evalz00 =
				bstring_to_symbol(BGl_string2725z00zz__evalz00);
			BGl_symbol2726z00zz__evalz00 =
				bstring_to_symbol(BGl_string2727z00zz__evalz00);
			BGl_symbol2728z00zz__evalz00 =
				bstring_to_symbol(BGl_string2729z00zz__evalz00);
			BGl_symbol2730z00zz__evalz00 =
				bstring_to_symbol(BGl_string2731z00zz__evalz00);
			BGl_symbol2732z00zz__evalz00 =
				bstring_to_symbol(BGl_string2733z00zz__evalz00);
			BGl_symbol2734z00zz__evalz00 =
				bstring_to_symbol(BGl_string2735z00zz__evalz00);
			BGl_symbol2737z00zz__evalz00 =
				bstring_to_symbol(BGl_string2738z00zz__evalz00);
			BGl_symbol2747z00zz__evalz00 =
				bstring_to_symbol(BGl_string2748z00zz__evalz00);
			BGl_list2749z00zz__evalz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2721z00zz__evalz00,
				MAKE_YOUNG_PAIR(BGl_symbol2747z00zz__evalz00, BNIL));
			BGl_symbol2750z00zz__evalz00 =
				bstring_to_symbol(BGl_string2751z00zz__evalz00);
			BGl_symbol2752z00zz__evalz00 =
				bstring_to_symbol(BGl_string2753z00zz__evalz00);
			BGl_symbol2759z00zz__evalz00 =
				bstring_to_symbol(BGl_string2760z00zz__evalz00);
			BGl_symbol2761z00zz__evalz00 =
				bstring_to_symbol(BGl_string2762z00zz__evalz00);
			BGl_symbol2764z00zz__evalz00 =
				bstring_to_symbol(BGl_string2765z00zz__evalz00);
			BGl_symbol2766z00zz__evalz00 =
				bstring_to_symbol(BGl_string2767z00zz__evalz00);
			BGl_symbol2768z00zz__evalz00 =
				bstring_to_symbol(BGl_string2769z00zz__evalz00);
			BGl_symbol2773z00zz__evalz00 =
				bstring_to_symbol(BGl_string2774z00zz__evalz00);
			BGl_symbol2775z00zz__evalz00 =
				bstring_to_symbol(BGl_string2776z00zz__evalz00);
			BGl_symbol2777z00zz__evalz00 =
				bstring_to_symbol(BGl_string2778z00zz__evalz00);
			BGl_symbol2781z00zz__evalz00 =
				bstring_to_symbol(BGl_string2782z00zz__evalz00);
			BGl_list2780z00zz__evalz00 =
				MAKE_YOUNG_PAIR(BGl_symbol2750z00zz__evalz00,
				MAKE_YOUNG_PAIR(BGl_symbol2781z00zz__evalz00, BNIL));
			BGl_symbol2783z00zz__evalz00 =
				bstring_to_symbol(BGl_string2779z00zz__evalz00);
			BGl_symbol2796z00zz__evalz00 =
				bstring_to_symbol(BGl_string2794z00zz__evalz00);
			BGl_symbol2801z00zz__evalz00 =
				bstring_to_symbol(BGl_string2799z00zz__evalz00);
			BGl_symbol2804z00zz__evalz00 =
				bstring_to_symbol(BGl_string2805z00zz__evalz00);
			return (BGl_symbol2808z00zz__evalz00 =
				bstring_to_symbol(BGl_string2809z00zz__evalz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			BGl_defaultzd2evaluatezd2zz__evalz00 =
				BGl_evaluate2zd2envzd2zz__evaluatez00;
			BGl_installzd2allzd2expandersz12z12zz__install_expandersz00();
			BGl_za2promptza2z00zz__evalz00 = BGl_proc2602z00zz__evalz00;
			BGl_za2replzd2numza2zd2zz__evalz00 = BINT(0L);
			BGl_za2replzd2quitza2zd2zz__evalz00 = BGl_proc2603z00zz__evalz00;
			BGl_za2replzd2printerza2zd2zz__evalz00 =
				BGl_defaultzd2replzd2printerzd2envzd2zz__evalz00;
			BGl_za2czd2debugzd2replzd2valueza2zd2zz__evalz00 = BUNSPEC;
			BGl_za2loadzd2pathza2zd2zz__evalz00 = BNIL;
			BGl_za2loadzd2verboseza2zd2zz__evalz00 = BTRUE;
			BGl_za2nilza2z00zz__evalz00 = BTRUE;
			BGl_za2userzd2passza2zd2zz__evalz00 = BUNSPEC;
			BGl_za2userzd2passzd2nameza2z00zz__evalz00 = BGl_string2604z00zz__evalz00;
			BGl_za2identifierzd2syntaxza2zd2zz__evalz00 =
				BGl_symbol2605z00zz__evalz00;
			{	/* Eval/eval.scm 794 */
				obj_t BgL_tmpz00_5772;

				BgL_tmpz00_5772 = BGL_CURRENT_DYNAMIC_ENV();
				return (BGl_za2transcriptza2z00zz__evalz00 =
					BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5772), BUNSPEC);
			}
		}

	}



/* &<@*repl-quit*:1385> */
	obj_t BGl_z62zc3z04za2replzd2quitza2za31385ze3z37zz__evalz00(obj_t
		BgL_envz00_3609, obj_t BgL_xz00_3610)
	{
		{	/* Eval/eval.scm 290 */
			return BIGLOO_EXIT(BgL_xz00_3610);
		}

	}



/* &<@*prompt*:1380> */
	obj_t BGl_z62zc3z04za2promptza2za31380ze3ze5zz__evalz00(obj_t BgL_envz00_3611,
		obj_t BgL_numz00_3612)
	{
		{	/* Eval/eval.scm 265 */
			{	/* Eval/eval.scm 266 */
				obj_t BgL_arg1382z00_4203;

				{	/* Eval/eval.scm 266 */
					obj_t BgL_tmpz00_5776;

					BgL_tmpz00_5776 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1382z00_4203 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5776);
				}
				bgl_display_obj(BgL_numz00_3612, BgL_arg1382z00_4203);
			}
			{	/* Eval/eval.scm 267 */
				obj_t BgL_arg1383z00_4204;

				{	/* Eval/eval.scm 267 */
					obj_t BgL_tmpz00_5780;

					BgL_tmpz00_5780 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1383z00_4204 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5780);
				}
				bgl_display_string(BGl_string2607z00zz__evalz00, BgL_arg1383z00_4204);
			}
			{	/* Eval/eval.scm 268 */
				obj_t BgL_arg1384z00_4205;

				{	/* Eval/eval.scm 268 */
					obj_t BgL_tmpz00_5784;

					BgL_tmpz00_5784 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1384z00_4205 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_5784);
				}
				return bgl_flush_output_port(BgL_arg1384z00_4205);
			}
		}

	}



/* &byte-code-evaluate */
	obj_t BGl_z62bytezd2codezd2evaluatez62zz__evalz00(obj_t BgL_envz00_3620,
		obj_t BgL_eexpz00_3621, obj_t BgL_envz00_3622, obj_t BgL_locz00_3623)
	{
		{	/* Eval/eval.scm 121 */
			{	/* Eval/eval.scm 122 */
				obj_t BgL_cexpz00_4206;
				obj_t BgL_denvz00_4207;

				BgL_cexpz00_4206 =
					BGl_evcompilez00zz__evcompilez00(BgL_eexpz00_3621, BNIL,
					BgL_envz00_3622, BGl_symbol2608z00zz__evalz00, ((bool_t) 0),
					BgL_locz00_3623, ((bool_t) 1), ((bool_t) 1));
				BgL_denvz00_4207 = BGL_CURRENT_DYNAMIC_ENV();
				{	/* Eval/eval.scm 124 */

					BGL_ENV_PUSH_TRACE(BgL_denvz00_4207, BUNSPEC, BUNSPEC);
					{	/* Eval/eval.scm 127 */
						obj_t BgL_tmpz00_4208;

						BgL_tmpz00_4208 =
							BGl_evmeaningz00zz__evmeaningz00(BgL_cexpz00_4206, BNIL,
							BgL_denvz00_4207);
						BGL_ENV_POP_TRACE(BgL_denvz00_4207);
						return BgL_tmpz00_4208;
					}
				}
			}
		}

	}



/* eval-evaluate-set! */
	BGL_EXPORTED_DEF obj_t BGl_evalzd2evaluatezd2setz12z12zz__evalz00(obj_t
		BgL_compz00_6)
	{
		{	/* Eval/eval.scm 145 */
			if ((BgL_compz00_6 == BGl_symbol2610z00zz__evalz00))
				{	/* Eval/eval.scm 146 */
					return (BGl_defaultzd2evaluatezd2zz__evalz00 =
						BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00, BUNSPEC);
				}
			else
				{	/* Eval/eval.scm 146 */
					if ((BgL_compz00_6 == BGl_symbol2612z00zz__evalz00))
						{	/* Eval/eval.scm 146 */
							return (BGl_defaultzd2evaluatezd2zz__evalz00 =
								BGl_evaluate2zd2envzd2zz__evaluatez00, BUNSPEC);
						}
					else
						{	/* Eval/eval.scm 146 */
							if (PROCEDUREP(BgL_compz00_6))
								{	/* Eval/eval.scm 152 */
									return (BGl_defaultzd2evaluatezd2zz__evalz00 =
										BgL_compz00_6, BUNSPEC);
								}
							else
								{	/* Eval/eval.scm 152 */
									return
										BGl_errorz00zz__errorz00(BGl_string2614z00zz__evalz00,
										BGl_string2615z00zz__evalz00, BgL_compz00_6);
								}
						}
				}
		}

	}



/* &eval-evaluate-set! */
	obj_t BGl_z62evalzd2evaluatezd2setz12z70zz__evalz00(obj_t BgL_envz00_3624,
		obj_t BgL_compz00_3625)
	{
		{	/* Eval/eval.scm 145 */
			return BGl_evalzd2evaluatezd2setz12z12zz__evalz00(BgL_compz00_3625);
		}

	}



/* _eval */
	obj_t BGl__evalz00zz__evalz00(obj_t BgL_env1277z00_10, obj_t BgL_opt1276z00_9)
	{
		{	/* Eval/eval.scm 169 */
			{	/* Eval/eval.scm 169 */
				obj_t BgL_g1278z00_1275;

				BgL_g1278z00_1275 = VECTOR_REF(BgL_opt1276z00_9, 0L);
				switch (VECTOR_LENGTH(BgL_opt1276z00_9))
					{
					case 1L:

						{	/* Eval/eval.scm 169 */
							obj_t BgL_envz00_1278;

							{	/* Eval/eval.scm 257 */
								obj_t BgL_mz00_2583;

								BgL_mz00_2583 = BGl_evalzd2modulezd2zz__evmodulez00();
								if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2583))
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1278 = BgL_mz00_2583;
									}
								else
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1278 = BGl_symbol2616z00zz__evalz00;
									}
							}
							{	/* Eval/eval.scm 169 */

								{	/* Eval/eval.scm 170 */
									obj_t BgL_auxz00_5805;

									{	/* Eval/eval.scm 170 */
										obj_t BgL_aux2253z00_3763;

										BgL_aux2253z00_3763 = BGl_defaultzd2evaluatezd2zz__evalz00;
										if (PROCEDUREP(BgL_aux2253z00_3763))
											{	/* Eval/eval.scm 170 */
												BgL_auxz00_5805 = BgL_aux2253z00_3763;
											}
										else
											{
												obj_t BgL_auxz00_5808;

												BgL_auxz00_5808 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2618z00zz__evalz00, BINT(6408L),
													BGl_string2619z00zz__evalz00,
													BGl_string2620z00zz__evalz00, BgL_aux2253z00_3763);
												FAILURE(BgL_auxz00_5808, BFALSE, BFALSE);
											}
									}
									return
										BGl_evalzf2expanderzf2zz__evalz00(BgL_g1278z00_1275,
										BgL_envz00_1278, BGl_expandzd2envzd2zz__expandz00,
										BgL_auxz00_5805);
								}
							}
						}
						break;
					case 2L:

						{	/* Eval/eval.scm 169 */
							obj_t BgL_envz00_1279;

							BgL_envz00_1279 = VECTOR_REF(BgL_opt1276z00_9, 1L);
							{	/* Eval/eval.scm 169 */

								{	/* Eval/eval.scm 170 */
									obj_t BgL_auxz00_5814;

									{	/* Eval/eval.scm 170 */
										obj_t BgL_aux2255z00_3765;

										BgL_aux2255z00_3765 = BGl_defaultzd2evaluatezd2zz__evalz00;
										if (PROCEDUREP(BgL_aux2255z00_3765))
											{	/* Eval/eval.scm 170 */
												BgL_auxz00_5814 = BgL_aux2255z00_3765;
											}
										else
											{
												obj_t BgL_auxz00_5817;

												BgL_auxz00_5817 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2618z00zz__evalz00, BINT(6408L),
													BGl_string2619z00zz__evalz00,
													BGl_string2620z00zz__evalz00, BgL_aux2255z00_3765);
												FAILURE(BgL_auxz00_5817, BFALSE, BFALSE);
											}
									}
									return
										BGl_evalzf2expanderzf2zz__evalz00(BgL_g1278z00_1275,
										BgL_envz00_1279, BGl_expandzd2envzd2zz__expandz00,
										BgL_auxz00_5814);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* eval */
	BGL_EXPORTED_DEF obj_t BGl_evalz00zz__evalz00(obj_t BgL_expz00_7,
		obj_t BgL_envz00_8)
	{
		{	/* Eval/eval.scm 169 */
			{	/* Eval/eval.scm 170 */
				obj_t BgL_auxz00_5824;

				{	/* Eval/eval.scm 170 */
					obj_t BgL_aux2257z00_3767;

					BgL_aux2257z00_3767 = BGl_defaultzd2evaluatezd2zz__evalz00;
					if (PROCEDUREP(BgL_aux2257z00_3767))
						{	/* Eval/eval.scm 170 */
							BgL_auxz00_5824 = BgL_aux2257z00_3767;
						}
					else
						{
							obj_t BgL_auxz00_5827;

							BgL_auxz00_5827 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
								BINT(6408L), BGl_string2621z00zz__evalz00,
								BGl_string2620z00zz__evalz00, BgL_aux2257z00_3767);
							FAILURE(BgL_auxz00_5827, BFALSE, BFALSE);
						}
				}
				return
					BGl_evalzf2expanderzf2zz__evalz00(BgL_expz00_7, BgL_envz00_8,
					BGl_expandzd2envzd2zz__expandz00, BgL_auxz00_5824);
			}
		}

	}



/* _eval! */
	obj_t BGl__evalz12z12zz__evalz00(obj_t BgL_env1282z00_14,
		obj_t BgL_opt1281z00_13)
	{
		{	/* Eval/eval.scm 175 */
			{	/* Eval/eval.scm 175 */
				obj_t BgL_g1283z00_1280;

				BgL_g1283z00_1280 = VECTOR_REF(BgL_opt1281z00_13, 0L);
				switch (VECTOR_LENGTH(BgL_opt1281z00_13))
					{
					case 1L:

						{	/* Eval/eval.scm 175 */
							obj_t BgL_envz00_1283;

							{	/* Eval/eval.scm 257 */
								obj_t BgL_mz00_2585;

								BgL_mz00_2585 = BGl_evalzd2modulezd2zz__evmodulez00();
								if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2585))
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1283 = BgL_mz00_2585;
									}
								else
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1283 = BGl_symbol2616z00zz__evalz00;
									}
							}
							{	/* Eval/eval.scm 175 */

								{	/* Eval/eval.scm 176 */
									obj_t BgL_evaluatez00_2587;

									if (PROCEDUREP(BGl_defaultzd2evaluatezd2zz__evalz00))
										{	/* Eval/eval.scm 176 */
											BgL_evaluatez00_2587 =
												BGl_defaultzd2evaluatezd2zz__evalz00;
										}
									else
										{	/* Eval/eval.scm 176 */
											BgL_evaluatez00_2587 =
												BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
										}
									{	/* Eval/eval.scm 179 */
										obj_t BgL_auxz00_5838;

										if (PROCEDUREP(BgL_evaluatez00_2587))
											{	/* Eval/eval.scm 179 */
												BgL_auxz00_5838 = BgL_evaluatez00_2587;
											}
										else
											{
												obj_t BgL_auxz00_5841;

												BgL_auxz00_5841 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2618z00zz__evalz00, BINT(6857L),
													BGl_string2622z00zz__evalz00,
													BGl_string2620z00zz__evalz00, BgL_evaluatez00_2587);
												FAILURE(BgL_auxz00_5841, BFALSE, BFALSE);
											}
										return
											BGl_evalzf2expanderzf2zz__evalz00(BgL_g1283z00_1280,
											BgL_envz00_1283, BGl_expandz12zd2envzc0zz__expandz00,
											BgL_auxz00_5838);
									}
								}
							}
						}
						break;
					case 2L:

						{	/* Eval/eval.scm 175 */
							obj_t BgL_envz00_1284;

							BgL_envz00_1284 = VECTOR_REF(BgL_opt1281z00_13, 1L);
							{	/* Eval/eval.scm 175 */

								{	/* Eval/eval.scm 176 */
									obj_t BgL_evaluatez00_2590;

									if (PROCEDUREP(BGl_defaultzd2evaluatezd2zz__evalz00))
										{	/* Eval/eval.scm 176 */
											BgL_evaluatez00_2590 =
												BGl_defaultzd2evaluatezd2zz__evalz00;
										}
									else
										{	/* Eval/eval.scm 176 */
											BgL_evaluatez00_2590 =
												BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
										}
									{	/* Eval/eval.scm 179 */
										obj_t BgL_auxz00_5849;

										if (PROCEDUREP(BgL_evaluatez00_2590))
											{	/* Eval/eval.scm 179 */
												BgL_auxz00_5849 = BgL_evaluatez00_2590;
											}
										else
											{
												obj_t BgL_auxz00_5852;

												BgL_auxz00_5852 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2618z00zz__evalz00, BINT(6857L),
													BGl_string2622z00zz__evalz00,
													BGl_string2620z00zz__evalz00, BgL_evaluatez00_2590);
												FAILURE(BgL_auxz00_5852, BFALSE, BFALSE);
											}
										return
											BGl_evalzf2expanderzf2zz__evalz00(BgL_g1283z00_1280,
											BgL_envz00_1284, BGl_expandz12zd2envzc0zz__expandz00,
											BgL_auxz00_5849);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* eval! */
	BGL_EXPORTED_DEF obj_t BGl_evalz12z12zz__evalz00(obj_t BgL_expz00_11,
		obj_t BgL_envz00_12)
	{
		{	/* Eval/eval.scm 175 */
			{	/* Eval/eval.scm 176 */
				obj_t BgL_evaluatez00_2593;

				if (PROCEDUREP(BGl_defaultzd2evaluatezd2zz__evalz00))
					{	/* Eval/eval.scm 176 */
						BgL_evaluatez00_2593 = BGl_defaultzd2evaluatezd2zz__evalz00;
					}
				else
					{	/* Eval/eval.scm 176 */
						BgL_evaluatez00_2593 =
							BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
					}
				{	/* Eval/eval.scm 179 */
					obj_t BgL_auxz00_5861;

					if (PROCEDUREP(BgL_evaluatez00_2593))
						{	/* Eval/eval.scm 179 */
							BgL_auxz00_5861 = BgL_evaluatez00_2593;
						}
					else
						{
							obj_t BgL_auxz00_5864;

							BgL_auxz00_5864 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
								BINT(6857L), BGl_string2623z00zz__evalz00,
								BGl_string2620z00zz__evalz00, BgL_evaluatez00_2593);
							FAILURE(BgL_auxz00_5864, BFALSE, BFALSE);
						}
					return
						BGl_evalzf2expanderzf2zz__evalz00(BgL_expz00_11, BgL_envz00_12,
						BGl_expandz12zd2envzc0zz__expandz00, BgL_auxz00_5861);
				}
			}
		}

	}



/* eval/expander */
	obj_t BGl_evalzf2expanderzf2zz__evalz00(obj_t BgL_expz00_15,
		obj_t BgL_envz00_16, obj_t BgL_expandz00_17, obj_t BgL_evaluatez00_18)
	{
		{	/* Eval/eval.scm 184 */
			{	/* Eval/eval.scm 185 */
				obj_t BgL_denvz00_1287;

				BgL_denvz00_1287 = BGL_CURRENT_DYNAMIC_ENV();
				{	/* Eval/eval.scm 186 */

					{	/* Eval/eval.scm 187 */
						obj_t BgL_locz00_1288;
						obj_t BgL_sexpz00_1289;

						BgL_locz00_1288 =
							BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_expz00_15);
						if (PROCEDUREP(BGl_za2userzd2passza2zd2zz__evalz00))
							{	/* Eval/eval.scm 188 */
								BgL_sexpz00_1289 =
									BGL_PROCEDURE_CALL1(BGl_za2userzd2passza2zd2zz__evalz00,
									BgL_expz00_15);
							}
						else
							{	/* Eval/eval.scm 188 */
								BgL_sexpz00_1289 = BgL_expz00_15;
							}
						BGL_ENV_PUSH_TRACE(BgL_denvz00_1287, BGl_keyword2624z00zz__evalz00,
							BFALSE);
						{	/* Eval/eval.scm 190 */
							obj_t BgL_tmpz00_1290;

							{	/* Eval/eval.scm 190 */
								bool_t BgL_test3008z00_5878;

								if (CBOOL(BgL_locz00_1288))
									{	/* Eval/eval.scm 190 */
										int BgL_a1241z00_1315;

										BgL_a1241z00_1315 = bgl_debug();
										{	/* Eval/eval.scm 190 */

											BgL_test3008z00_5878 = ((long) (BgL_a1241z00_1315) > 0L);
									}}
								else
									{	/* Eval/eval.scm 190 */
										BgL_test3008z00_5878 = ((bool_t) 0);
									}
								if (BgL_test3008z00_5878)
									{	/* Eval/eval.scm 191 */
										obj_t BgL_env1049z00_1296;

										BgL_env1049z00_1296 = BGL_CURRENT_DYNAMIC_ENV();
										{	/* Eval/eval.scm 191 */
											obj_t BgL_cell1044z00_1297;

											BgL_cell1044z00_1297 = MAKE_STACK_CELL(BUNSPEC);
											{	/* Eval/eval.scm 191 */
												obj_t BgL_val1048z00_1298;

												BgL_val1048z00_1298 =
													BGl_zc3z04exitza31396ze3ze70z60zz__evalz00
													(BgL_locz00_1288, BgL_envz00_16, BgL_evaluatez00_18,
													BgL_sexpz00_1289, BgL_expandz00_17,
													BgL_cell1044z00_1297, BgL_env1049z00_1296);
												if ((BgL_val1048z00_1298 == BgL_cell1044z00_1297))
													{	/* Eval/eval.scm 191 */
														{	/* Eval/eval.scm 191 */
															int BgL_tmpz00_5889;

															BgL_tmpz00_5889 = (int) (0L);
															BGL_SIGSETMASK(BgL_tmpz00_5889);
														}
														{	/* Eval/eval.scm 191 */
															obj_t BgL_arg1395z00_1299;

															BgL_arg1395z00_1299 =
																CELL_REF(((obj_t) BgL_val1048z00_1298));
															BgL_tmpz00_1290 =
																BGl_evalzd2exceptionzd2handlerz00zz__evalz00
																(BgL_arg1395z00_1299, BgL_locz00_1288);
													}}
												else
													{	/* Eval/eval.scm 191 */
														BgL_tmpz00_1290 = BgL_val1048z00_1298;
													}
											}
										}
									}
								else
									{	/* Eval/eval.scm 195 */
										obj_t BgL_arg1401z00_1314;

										BgL_arg1401z00_1314 =
											BGL_PROCEDURE_CALL1(BgL_expandz00_17, BgL_sexpz00_1289);
										BgL_tmpz00_1290 =
											BGL_PROCEDURE_CALL3(BgL_evaluatez00_18,
											BgL_arg1401z00_1314, BgL_envz00_16, BgL_locz00_1288);
									}
							}
							BGL_ENV_POP_TRACE(BgL_denvz00_1287);
							return BgL_tmpz00_1290;
						}
					}
				}
			}
		}

	}



/* <@exit:1396>~0 */
	obj_t BGl_zc3z04exitza31396ze3ze70z60zz__evalz00(obj_t BgL_locz00_3761,
		obj_t BgL_envz00_3760, obj_t BgL_evaluatez00_3759, obj_t BgL_sexpz00_3758,
		obj_t BgL_expandz00_3757, obj_t BgL_cell1044z00_3756,
		obj_t BgL_env1049z00_3755)
	{
		{	/* Eval/eval.scm 191 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1053z00_1301;

			if (SET_EXIT(BgL_an_exit1053z00_1301))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1053z00_1301 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1049z00_3755, BgL_an_exit1053z00_1301, 1L);
					{	/* Eval/eval.scm 191 */
						obj_t BgL_escape1045z00_1302;

						BgL_escape1045z00_1302 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1049z00_3755);
						{	/* Eval/eval.scm 191 */
							obj_t BgL_res1056z00_1303;

							{	/* Eval/eval.scm 191 */
								obj_t BgL_ohs1041z00_1304;

								BgL_ohs1041z00_1304 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1049z00_3755);
								{	/* Eval/eval.scm 191 */
									obj_t BgL_hds1042z00_1305;

									BgL_hds1042z00_1305 =
										MAKE_STACK_PAIR(BgL_escape1045z00_1302,
										BgL_cell1044z00_3756);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1049z00_3755,
										BgL_hds1042z00_1305);
									BUNSPEC;
									{	/* Eval/eval.scm 191 */
										obj_t BgL_exitd1050z00_1306;

										BgL_exitd1050z00_1306 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1049z00_3755);
										{	/* Eval/eval.scm 191 */
											obj_t BgL_tmp1052z00_1307;

											{	/* Eval/eval.scm 191 */
												obj_t BgL_arg1399z00_1310;

												BgL_arg1399z00_1310 =
													BGL_EXITD_PROTECT(BgL_exitd1050z00_1306);
												BgL_tmp1052z00_1307 =
													MAKE_YOUNG_PAIR(BgL_ohs1041z00_1304,
													BgL_arg1399z00_1310);
											}
											{	/* Eval/eval.scm 191 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_1306,
													BgL_tmp1052z00_1307);
												BUNSPEC;
												{	/* Eval/eval.scm 194 */
													obj_t BgL_tmp1051z00_1308;

													{	/* Eval/eval.scm 194 */
														obj_t BgL_arg1397z00_1309;

														BgL_arg1397z00_1309 =
															BGL_PROCEDURE_CALL1(BgL_expandz00_3757,
															BgL_sexpz00_3758);
														BgL_tmp1051z00_1308 =
															BGL_PROCEDURE_CALL3(BgL_evaluatez00_3759,
															BgL_arg1397z00_1309, BgL_envz00_3760,
															BgL_locz00_3761);
													}
													{	/* Eval/eval.scm 191 */
														bool_t BgL_test3011z00_5927;

														{	/* Eval/eval.scm 191 */
															obj_t BgL_arg2210z00_2600;

															BgL_arg2210z00_2600 =
																BGL_EXITD_PROTECT(BgL_exitd1050z00_1306);
															BgL_test3011z00_5927 = PAIRP(BgL_arg2210z00_2600);
														}
														if (BgL_test3011z00_5927)
															{	/* Eval/eval.scm 191 */
																obj_t BgL_arg2208z00_2601;

																{	/* Eval/eval.scm 191 */
																	obj_t BgL_arg2209z00_2602;

																	BgL_arg2209z00_2602 =
																		BGL_EXITD_PROTECT(BgL_exitd1050z00_1306);
																	{	/* Eval/eval.scm 191 */
																		obj_t BgL_pairz00_2603;

																		if (PAIRP(BgL_arg2209z00_2602))
																			{	/* Eval/eval.scm 191 */
																				BgL_pairz00_2603 = BgL_arg2209z00_2602;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5933;

																				BgL_auxz00_5933 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(7435L),
																					BGl_string2625z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_arg2209z00_2602);
																				FAILURE(BgL_auxz00_5933, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2208z00_2601 = CDR(BgL_pairz00_2603);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_1306,
																	BgL_arg2208z00_2601);
																BUNSPEC;
															}
														else
															{	/* Eval/eval.scm 191 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1049z00_3755,
														BgL_ohs1041z00_1304);
													BUNSPEC;
													BgL_res1056z00_1303 = BgL_tmp1051z00_1308;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1049z00_3755);
							return BgL_res1056z00_1303;
						}
					}
				}
		}

	}



/* eval-exception-handler */
	obj_t BGl_evalzd2exceptionzd2handlerz00zz__evalz00(obj_t BgL_ez00_19,
		obj_t BgL_locz00_20)
	{
		{	/* Eval/eval.scm 202 */
			{	/* Eval/eval.scm 203 */
				bool_t BgL_test3013z00_5941;

				{	/* Eval/eval.scm 203 */
					bool_t BgL_test3014z00_5942;

					{	/* Eval/eval.scm 203 */
						obj_t BgL_classz00_2605;

						BgL_classz00_2605 = BGl_z62exceptionz62zz__objectz00;
						if (BGL_OBJECTP(BgL_ez00_19))
							{	/* Eval/eval.scm 203 */
								BgL_objectz00_bglt BgL_arg2222z00_2607;

								BgL_arg2222z00_2607 = (BgL_objectz00_bglt) (BgL_ez00_19);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/eval.scm 203 */
										long BgL_idxz00_2613;

										BgL_idxz00_2613 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2222z00_2607);
										{	/* Eval/eval.scm 203 */
											obj_t BgL_arg2215z00_2614;

											{	/* Eval/eval.scm 203 */
												long BgL_arg2216z00_2615;

												BgL_arg2216z00_2615 = (BgL_idxz00_2613 + 2L);
												{	/* Eval/eval.scm 203 */
													obj_t BgL_vectorz00_2617;

													{	/* Eval/eval.scm 203 */
														obj_t BgL_aux2267z00_3777;

														BgL_aux2267z00_3777 =
															BGl_za2inheritancesza2z00zz__objectz00;
														if (VECTORP(BgL_aux2267z00_3777))
															{	/* Eval/eval.scm 203 */
																BgL_vectorz00_2617 = BgL_aux2267z00_3777;
															}
														else
															{
																obj_t BgL_auxz00_5952;

																BgL_auxz00_5952 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2618z00zz__evalz00, BINT(7915L),
																	BGl_string2627z00zz__evalz00,
																	BGl_string2628z00zz__evalz00,
																	BgL_aux2267z00_3777);
																FAILURE(BgL_auxz00_5952, BFALSE, BFALSE);
															}
													}
													BgL_arg2215z00_2614 =
														VECTOR_REF(BgL_vectorz00_2617, BgL_arg2216z00_2615);
												}
											}
											BgL_test3014z00_5942 =
												(BgL_arg2215z00_2614 == BgL_classz00_2605);
										}
									}
								else
									{	/* Eval/eval.scm 203 */
										bool_t BgL_res2235z00_2638;

										{	/* Eval/eval.scm 203 */
											obj_t BgL_oclassz00_2621;

											{	/* Eval/eval.scm 203 */
												obj_t BgL_arg2229z00_2629;
												long BgL_arg2230z00_2630;

												BgL_arg2229z00_2629 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/eval.scm 203 */
													long BgL_arg2231z00_2631;

													BgL_arg2231z00_2631 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2222z00_2607);
													BgL_arg2230z00_2630 =
														(BgL_arg2231z00_2631 - OBJECT_TYPE);
												}
												BgL_oclassz00_2621 =
													VECTOR_REF(BgL_arg2229z00_2629, BgL_arg2230z00_2630);
											}
											{	/* Eval/eval.scm 203 */
												bool_t BgL__ortest_1236z00_2622;

												BgL__ortest_1236z00_2622 =
													(BgL_classz00_2605 == BgL_oclassz00_2621);
												if (BgL__ortest_1236z00_2622)
													{	/* Eval/eval.scm 203 */
														BgL_res2235z00_2638 = BgL__ortest_1236z00_2622;
													}
												else
													{	/* Eval/eval.scm 203 */
														long BgL_odepthz00_2623;

														{	/* Eval/eval.scm 203 */
															obj_t BgL_arg2219z00_2624;

															BgL_arg2219z00_2624 = (BgL_oclassz00_2621);
															BgL_odepthz00_2623 =
																BGL_CLASS_DEPTH(BgL_arg2219z00_2624);
														}
														if ((2L < BgL_odepthz00_2623))
															{	/* Eval/eval.scm 203 */
																obj_t BgL_arg2217z00_2626;

																{	/* Eval/eval.scm 203 */
																	obj_t BgL_arg2218z00_2627;

																	BgL_arg2218z00_2627 = (BgL_oclassz00_2621);
																	BgL_arg2217z00_2626 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2218z00_2627,
																		2L);
																}
																BgL_res2235z00_2638 =
																	(BgL_arg2217z00_2626 == BgL_classz00_2605);
															}
														else
															{	/* Eval/eval.scm 203 */
																BgL_res2235z00_2638 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3014z00_5942 = BgL_res2235z00_2638;
									}
							}
						else
							{	/* Eval/eval.scm 203 */
								BgL_test3014z00_5942 = ((bool_t) 0);
							}
					}
					if (BgL_test3014z00_5942)
						{	/* Eval/eval.scm 204 */
							BgL_z62exceptionz62_bglt BgL_i1058z00_1342;

							{	/* Eval/eval.scm 204 */
								bool_t BgL_test3020z00_5971;

								{	/* Eval/eval.scm 204 */
									obj_t BgL_classz00_4209;

									BgL_classz00_4209 = BGl_z62exceptionz62zz__objectz00;
									if (BGL_OBJECTP(BgL_ez00_19))
										{	/* Eval/eval.scm 204 */
											BgL_objectz00_bglt BgL_arg2224z00_4211;
											long BgL_arg2225z00_4212;

											BgL_arg2224z00_4211 = (BgL_objectz00_bglt) (BgL_ez00_19);
											BgL_arg2225z00_4212 = BGL_CLASS_DEPTH(BgL_classz00_4209);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Eval/eval.scm 204 */
													long BgL_idxz00_4220;

													BgL_idxz00_4220 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg2224z00_4211);
													{	/* Eval/eval.scm 204 */
														obj_t BgL_arg2215z00_4221;

														{	/* Eval/eval.scm 204 */
															long BgL_arg2216z00_4222;

															BgL_arg2216z00_4222 =
																(BgL_idxz00_4220 + BgL_arg2225z00_4212);
															BgL_arg2215z00_4221 =
																VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																BgL_arg2216z00_4222);
														}
														BgL_test3020z00_5971 =
															(BgL_arg2215z00_4221 == BgL_classz00_4209);
												}}
											else
												{	/* Eval/eval.scm 204 */
													bool_t BgL_res2244z00_4227;

													{	/* Eval/eval.scm 204 */
														obj_t BgL_oclassz00_4231;

														{	/* Eval/eval.scm 204 */
															obj_t BgL_arg2229z00_4233;
															long BgL_arg2230z00_4234;

															BgL_arg2229z00_4233 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Eval/eval.scm 204 */
																long BgL_arg2231z00_4235;

																BgL_arg2231z00_4235 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg2224z00_4211);
																BgL_arg2230z00_4234 =
																	(BgL_arg2231z00_4235 - OBJECT_TYPE);
															}
															BgL_oclassz00_4231 =
																VECTOR_REF(BgL_arg2229z00_4233,
																BgL_arg2230z00_4234);
														}
														{	/* Eval/eval.scm 204 */
															bool_t BgL__ortest_1236z00_4241;

															BgL__ortest_1236z00_4241 =
																(BgL_classz00_4209 == BgL_oclassz00_4231);
															if (BgL__ortest_1236z00_4241)
																{	/* Eval/eval.scm 204 */
																	BgL_res2244z00_4227 =
																		BgL__ortest_1236z00_4241;
																}
															else
																{	/* Eval/eval.scm 204 */
																	long BgL_odepthz00_4242;

																	{	/* Eval/eval.scm 204 */
																		obj_t BgL_arg2219z00_4243;

																		BgL_arg2219z00_4243 = (BgL_oclassz00_4231);
																		BgL_odepthz00_4242 =
																			BGL_CLASS_DEPTH(BgL_arg2219z00_4243);
																	}
																	if (
																		(BgL_arg2225z00_4212 < BgL_odepthz00_4242))
																		{	/* Eval/eval.scm 204 */
																			obj_t BgL_arg2217z00_4247;

																			{	/* Eval/eval.scm 204 */
																				obj_t BgL_arg2218z00_4248;

																				BgL_arg2218z00_4248 =
																					(BgL_oclassz00_4231);
																				BgL_arg2217z00_4247 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg2218z00_4248,
																					BgL_arg2225z00_4212);
																			}
																			BgL_res2244z00_4227 =
																				(BgL_arg2217z00_4247 ==
																				BgL_classz00_4209);
																		}
																	else
																		{	/* Eval/eval.scm 204 */
																			BgL_res2244z00_4227 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3020z00_5971 = BgL_res2244z00_4227;
												}
										}
									else
										{	/* Eval/eval.scm 204 */
											BgL_test3020z00_5971 = ((bool_t) 0);
										}
								}
								if (BgL_test3020z00_5971)
									{	/* Eval/eval.scm 204 */
										BgL_i1058z00_1342 =
											((BgL_z62exceptionz62_bglt) BgL_ez00_19);
									}
								else
									{
										obj_t BgL_auxz00_5996;

										BgL_auxz00_5996 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2618z00zz__evalz00, BINT(7977L),
											BGl_string2627z00zz__evalz00,
											BGl_string2629z00zz__evalz00, BgL_ez00_19);
										FAILURE(BgL_auxz00_5996, BFALSE, BFALSE);
									}
							}
							if (CBOOL(
									(((BgL_z62exceptionz62_bglt) COBJECT(BgL_i1058z00_1342))->
										BgL_fnamez00)))
								{	/* Eval/eval.scm 204 */
									BgL_test3013z00_5941 = ((bool_t) 0);
								}
							else
								{	/* Eval/eval.scm 204 */
									BgL_test3013z00_5941 = ((bool_t) 1);
								}
						}
					else
						{	/* Eval/eval.scm 203 */
							BgL_test3013z00_5941 = ((bool_t) 0);
						}
				}
				if (BgL_test3013z00_5941)
					{	/* Eval/eval.scm 203 */
						if (PAIRP(BgL_locz00_20))
							{	/* Eval/eval.scm 205 */
								obj_t BgL_cdrzd2108zd2_1328;

								BgL_cdrzd2108zd2_1328 = CDR(((obj_t) BgL_locz00_20));
								if (
									(CAR(
											((obj_t) BgL_locz00_20)) == BGl_symbol2630z00zz__evalz00))
									{	/* Eval/eval.scm 205 */
										if (PAIRP(BgL_cdrzd2108zd2_1328))
											{	/* Eval/eval.scm 205 */
												obj_t BgL_cdrzd2112zd2_1332;

												BgL_cdrzd2112zd2_1332 = CDR(BgL_cdrzd2108zd2_1328);
												if (PAIRP(BgL_cdrzd2112zd2_1332))
													{	/* Eval/eval.scm 205 */
														if (NULLP(CDR(BgL_cdrzd2112zd2_1332)))
															{	/* Eval/eval.scm 205 */
																obj_t BgL_arg1413z00_1336;
																obj_t BgL_arg1414z00_1337;

																BgL_arg1413z00_1336 =
																	CAR(BgL_cdrzd2108zd2_1328);
																BgL_arg1414z00_1337 =
																	CAR(BgL_cdrzd2112zd2_1332);
																{	/* Eval/eval.scm 207 */
																	BgL_z62exceptionz62_bglt BgL_i1059z00_2646;

																	{	/* Eval/eval.scm 208 */
																		bool_t BgL_test3031z00_6021;

																		{	/* Eval/eval.scm 208 */
																			obj_t BgL_classz00_4249;

																			BgL_classz00_4249 =
																				BGl_z62exceptionz62zz__objectz00;
																			if (BGL_OBJECTP(BgL_ez00_19))
																				{	/* Eval/eval.scm 208 */
																					BgL_objectz00_bglt
																						BgL_arg2224z00_4251;
																					long BgL_arg2225z00_4252;

																					BgL_arg2224z00_4251 =
																						(BgL_objectz00_bglt) (BgL_ez00_19);
																					BgL_arg2225z00_4252 =
																						BGL_CLASS_DEPTH(BgL_classz00_4249);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Eval/eval.scm 208 */
																							long BgL_idxz00_4260;

																							BgL_idxz00_4260 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg2224z00_4251);
																							{	/* Eval/eval.scm 208 */
																								obj_t BgL_arg2215z00_4261;

																								{	/* Eval/eval.scm 208 */
																									long BgL_arg2216z00_4262;

																									BgL_arg2216z00_4262 =
																										(BgL_idxz00_4260 +
																										BgL_arg2225z00_4252);
																									BgL_arg2215z00_4261 =
																										VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																										BgL_arg2216z00_4262);
																								}
																								BgL_test3031z00_6021 =
																									(BgL_arg2215z00_4261 ==
																									BgL_classz00_4249);
																						}}
																					else
																						{	/* Eval/eval.scm 208 */
																							bool_t BgL_res2244z00_4267;

																							{	/* Eval/eval.scm 208 */
																								obj_t BgL_oclassz00_4271;

																								{	/* Eval/eval.scm 208 */
																									obj_t BgL_arg2229z00_4273;
																									long BgL_arg2230z00_4274;

																									BgL_arg2229z00_4273 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Eval/eval.scm 208 */
																										long BgL_arg2231z00_4275;

																										BgL_arg2231z00_4275 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg2224z00_4251);
																										BgL_arg2230z00_4274 =
																											(BgL_arg2231z00_4275 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_4271 =
																										VECTOR_REF
																										(BgL_arg2229z00_4273,
																										BgL_arg2230z00_4274);
																								}
																								{	/* Eval/eval.scm 208 */
																									bool_t
																										BgL__ortest_1236z00_4281;
																									BgL__ortest_1236z00_4281 =
																										(BgL_classz00_4249 ==
																										BgL_oclassz00_4271);
																									if (BgL__ortest_1236z00_4281)
																										{	/* Eval/eval.scm 208 */
																											BgL_res2244z00_4267 =
																												BgL__ortest_1236z00_4281;
																										}
																									else
																										{	/* Eval/eval.scm 208 */
																											long BgL_odepthz00_4282;

																											{	/* Eval/eval.scm 208 */
																												obj_t
																													BgL_arg2219z00_4283;
																												BgL_arg2219z00_4283 =
																													(BgL_oclassz00_4271);
																												BgL_odepthz00_4282 =
																													BGL_CLASS_DEPTH
																													(BgL_arg2219z00_4283);
																											}
																											if (
																												(BgL_arg2225z00_4252 <
																													BgL_odepthz00_4282))
																												{	/* Eval/eval.scm 208 */
																													obj_t
																														BgL_arg2217z00_4287;
																													{	/* Eval/eval.scm 208 */
																														obj_t
																															BgL_arg2218z00_4288;
																														BgL_arg2218z00_4288
																															=
																															(BgL_oclassz00_4271);
																														BgL_arg2217z00_4287
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg2218z00_4288,
																															BgL_arg2225z00_4252);
																													}
																													BgL_res2244z00_4267 =
																														(BgL_arg2217z00_4287
																														==
																														BgL_classz00_4249);
																												}
																											else
																												{	/* Eval/eval.scm 208 */
																													BgL_res2244z00_4267 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test3031z00_6021 =
																								BgL_res2244z00_4267;
																						}
																				}
																			else
																				{	/* Eval/eval.scm 208 */
																					BgL_test3031z00_6021 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test3031z00_6021)
																			{	/* Eval/eval.scm 208 */
																				BgL_i1059z00_2646 =
																					((BgL_z62exceptionz62_bglt)
																					BgL_ez00_19);
																			}
																		else
																			{
																				obj_t BgL_auxz00_6046;

																				BgL_auxz00_6046 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(8101L),
																					BGl_string2627z00zz__evalz00,
																					BGl_string2629z00zz__evalz00,
																					BgL_ez00_19);
																				FAILURE(BgL_auxz00_6046, BFALSE,
																					BFALSE);
																			}
																	}
																	((((BgL_z62exceptionz62_bglt)
																				COBJECT(BgL_i1059z00_2646))->
																			BgL_fnamez00) =
																		((obj_t) BgL_arg1413z00_1336), BUNSPEC);
																	((((BgL_z62exceptionz62_bglt)
																				COBJECT(BgL_i1059z00_2646))->
																			BgL_locationz00) =
																		((obj_t) BgL_arg1414z00_1337), BUNSPEC);
																}
															}
														else
															{	/* Eval/eval.scm 205 */
																BFALSE;
															}
													}
												else
													{	/* Eval/eval.scm 205 */
														BFALSE;
													}
											}
										else
											{	/* Eval/eval.scm 205 */
												BFALSE;
											}
									}
								else
									{	/* Eval/eval.scm 205 */
										BFALSE;
									}
							}
						else
							{	/* Eval/eval.scm 205 */
								BFALSE;
							}
					}
				else
					{	/* Eval/eval.scm 203 */
						BFALSE;
					}
			}
			return BGl_raisez00zz__errorz00(BgL_ez00_19);
		}

	}



/* _byte-code-compile */
	obj_t BGl__bytezd2codezd2compilez00zz__evalz00(obj_t BgL_env1287z00_24,
		obj_t BgL_opt1286z00_23)
	{
		{	/* Eval/eval.scm 215 */
			{	/* Eval/eval.scm 215 */
				obj_t BgL_g1288z00_1344;

				BgL_g1288z00_1344 = VECTOR_REF(BgL_opt1286z00_23, 0L);
				switch (VECTOR_LENGTH(BgL_opt1286z00_23))
					{
					case 1L:

						{	/* Eval/eval.scm 215 */
							obj_t BgL_envz00_1347;

							{	/* Eval/eval.scm 257 */
								obj_t BgL_mz00_2647;

								BgL_mz00_2647 = BGl_evalzd2modulezd2zz__evmodulez00();
								if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2647))
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1347 = BgL_mz00_2647;
									}
								else
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1347 = BGl_symbol2616z00zz__evalz00;
									}
							}
							{	/* Eval/eval.scm 215 */

								return
									BGl_bytezd2codezd2compilez00zz__evalz00(BgL_g1288z00_1344,
									BgL_envz00_1347);
							}
						}
						break;
					case 2L:

						{	/* Eval/eval.scm 215 */
							obj_t BgL_envz00_1348;

							BgL_envz00_1348 = VECTOR_REF(BgL_opt1286z00_23, 1L);
							{	/* Eval/eval.scm 215 */

								return
									BGl_bytezd2codezd2compilez00zz__evalz00(BgL_g1288z00_1344,
									BgL_envz00_1348);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* byte-code-compile */
	BGL_EXPORTED_DEF obj_t BGl_bytezd2codezd2compilez00zz__evalz00(obj_t
		BgL_expz00_21, obj_t BgL_envz00_22)
	{
		{	/* Eval/eval.scm 215 */
			{	/* Eval/eval.scm 216 */
				obj_t BgL_locz00_1349;

				BgL_locz00_1349 =
					BGl_getzd2sourcezd2locationz00zz__readerz00(BgL_expz00_21);
				{	/* Eval/eval.scm 216 */
					obj_t BgL_sexpz00_1350;

					if (PROCEDUREP(BGl_za2userzd2passza2zd2zz__evalz00))
						{	/* Eval/eval.scm 217 */
							BgL_sexpz00_1350 =
								BGL_PROCEDURE_CALL1(BGl_za2userzd2passza2zd2zz__evalz00,
								BgL_expz00_21);
						}
					else
						{	/* Eval/eval.scm 217 */
							BgL_sexpz00_1350 = BgL_expz00_21;
						}
					{	/* Eval/eval.scm 217 */

						{	/* Eval/eval.scm 219 */
							obj_t BgL_arg1417z00_1351;

							{	/* Eval/eval.scm 219 */
								obj_t BgL_arg1419z00_1354;

								BgL_arg1419z00_1354 =
									BGl_expandz00zz__expandz00(BgL_sexpz00_1350);
								BgL_arg1417z00_1351 =
									BGl_evcompilez00zz__evcompilez00(BgL_arg1419z00_1354, BNIL,
									BgL_envz00_22, BGl_symbol2608z00zz__evalz00, ((bool_t) 1),
									BgL_locz00_1349, ((bool_t) 0), ((bool_t) 1));
							}
							{	/* Eval/eval.scm 218 */

								BGL_TAIL return obj_to_string(BgL_arg1417z00_1351, BFALSE);
							}
						}
					}
				}
			}
		}

	}



/* byte-code-run */
	BGL_EXPORTED_DEF obj_t BGl_bytezd2codezd2runz00zz__evalz00(obj_t
		BgL_bytezd2codezd2_25)
	{
		{	/* Eval/eval.scm 224 */
			{	/* Eval/eval.scm 225 */
				obj_t BgL_arg1421z00_2650;
				obj_t BgL_arg1422z00_2651;

				{	/* Eval/eval.scm 225 */

					BgL_arg1421z00_2650 =
						string_to_obj(BgL_bytezd2codezd2_25, BFALSE, BFALSE);
				}
				BgL_arg1422z00_2651 = BGL_CURRENT_DYNAMIC_ENV();
				return
					BGl_evmeaningz00zz__evmeaningz00(BgL_arg1421z00_2650, BNIL,
					BgL_arg1422z00_2651);
			}
		}

	}



/* &byte-code-run */
	obj_t BGl_z62bytezd2codezd2runz62zz__evalz00(obj_t BgL_envz00_3630,
		obj_t BgL_bytezd2codezd2_3631)
	{
		{	/* Eval/eval.scm 224 */
			{	/* Eval/eval.scm 225 */
				obj_t BgL_auxz00_6075;

				if (STRINGP(BgL_bytezd2codezd2_3631))
					{	/* Eval/eval.scm 225 */
						BgL_auxz00_6075 = BgL_bytezd2codezd2_3631;
					}
				else
					{
						obj_t BgL_auxz00_6078;

						BgL_auxz00_6078 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(8919L), BGl_string2632z00zz__evalz00,
							BGl_string2633z00zz__evalz00, BgL_bytezd2codezd2_3631);
						FAILURE(BgL_auxz00_6078, BFALSE, BFALSE);
					}
				return BGl_bytezd2codezd2runz00zz__evalz00(BgL_auxz00_6075);
			}
		}

	}



/* scheme-report-environment */
	BGL_EXPORTED_DEF obj_t BGl_schemezd2reportzd2environmentz00zz__evalz00(obj_t
		BgL_versionz00_26)
	{
		{	/* Eval/eval.scm 230 */
			{	/* Eval/eval.scm 231 */
				bool_t BgL_test3039z00_6083;

				{	/* Eval/eval.scm 231 */
					long BgL_n1z00_2656;

					{	/* Eval/eval.scm 231 */
						obj_t BgL_tmpz00_6084;

						if (INTEGERP(BgL_versionz00_26))
							{	/* Eval/eval.scm 231 */
								BgL_tmpz00_6084 = BgL_versionz00_26;
							}
						else
							{
								obj_t BgL_auxz00_6087;

								BgL_auxz00_6087 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
									BINT(9237L), BGl_string2634z00zz__evalz00,
									BGl_string2635z00zz__evalz00, BgL_versionz00_26);
								FAILURE(BgL_auxz00_6087, BFALSE, BFALSE);
							}
						BgL_n1z00_2656 = (long) CINT(BgL_tmpz00_6084);
					}
					BgL_test3039z00_6083 = (BgL_n1z00_2656 == 5L);
				}
				if (BgL_test3039z00_6083)
					{	/* Eval/eval.scm 231 */
						return BGl_symbol2636z00zz__evalz00;
					}
				else
					{	/* Eval/eval.scm 231 */
						return
							BGl_errorz00zz__errorz00(BGl_symbol2636z00zz__evalz00,
							BGl_string2637z00zz__evalz00, BgL_versionz00_26);
					}
			}
		}

	}



/* &scheme-report-environment */
	obj_t BGl_z62schemezd2reportzd2environmentz62zz__evalz00(obj_t
		BgL_envz00_3632, obj_t BgL_versionz00_3633)
	{
		{	/* Eval/eval.scm 230 */
			return
				BGl_schemezd2reportzd2environmentz00zz__evalz00(BgL_versionz00_3633);
		}

	}



/* null-environment */
	BGL_EXPORTED_DEF obj_t BGl_nullzd2environmentzd2zz__evalz00(obj_t
		BgL_versionz00_27)
	{
		{	/* Eval/eval.scm 240 */
			{	/* Eval/eval.scm 241 */
				bool_t BgL_test3041z00_6095;

				{	/* Eval/eval.scm 241 */
					long BgL_n1z00_2658;

					{	/* Eval/eval.scm 241 */
						obj_t BgL_tmpz00_6096;

						if (INTEGERP(BgL_versionz00_27))
							{	/* Eval/eval.scm 241 */
								BgL_tmpz00_6096 = BgL_versionz00_27;
							}
						else
							{
								obj_t BgL_auxz00_6099;

								BgL_auxz00_6099 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
									BINT(9645L), BGl_string2638z00zz__evalz00,
									BGl_string2635z00zz__evalz00, BgL_versionz00_27);
								FAILURE(BgL_auxz00_6099, BFALSE, BFALSE);
							}
						BgL_n1z00_2658 = (long) CINT(BgL_tmpz00_6096);
					}
					BgL_test3041z00_6095 = (BgL_n1z00_2658 == 5L);
				}
				if (BgL_test3041z00_6095)
					{	/* Eval/eval.scm 241 */
						return BGl_symbol2639z00zz__evalz00;
					}
				else
					{	/* Eval/eval.scm 241 */
						return
							BGl_errorz00zz__errorz00(BGl_symbol2636z00zz__evalz00,
							BGl_string2637z00zz__evalz00, BgL_versionz00_27);
					}
			}
		}

	}



/* &null-environment */
	obj_t BGl_z62nullzd2environmentzb0zz__evalz00(obj_t BgL_envz00_3634,
		obj_t BgL_versionz00_3635)
	{
		{	/* Eval/eval.scm 240 */
			return BGl_nullzd2environmentzd2zz__evalz00(BgL_versionz00_3635);
		}

	}



/* interaction-environment */
	BGL_EXPORTED_DEF obj_t BGl_interactionzd2environmentzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 250 */
			return BGl_symbol2616z00zz__evalz00;
		}

	}



/* &interaction-environment */
	obj_t BGl_z62interactionzd2environmentzb0zz__evalz00(obj_t BgL_envz00_3636)
	{
		{	/* Eval/eval.scm 250 */
			return BGl_interactionzd2environmentzd2zz__evalz00();
		}

	}



/* default-environment */
	BGL_EXPORTED_DEF obj_t BGl_defaultzd2environmentzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 256 */
			{	/* Eval/eval.scm 257 */
				obj_t BgL_mz00_2659;

				BgL_mz00_2659 = BGl_evalzd2modulezd2zz__evmodulez00();
				if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2659))
					{	/* Eval/eval.scm 258 */
						return BgL_mz00_2659;
					}
				else
					{	/* Eval/eval.scm 258 */
						return BGl_symbol2616z00zz__evalz00;
					}
			}
		}

	}



/* &default-environment */
	obj_t BGl_z62defaultzd2environmentzb0zz__evalz00(obj_t BgL_envz00_3637)
	{
		{	/* Eval/eval.scm 256 */
			return BGl_defaultzd2environmentzd2zz__evalz00();
		}

	}



/* set-prompter! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2prompterz12zc0zz__evalz00(obj_t
		BgL_procz00_28)
	{
		{	/* Eval/eval.scm 273 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_28, (int) (1L)))
				{	/* Eval/eval.scm 274 */
					return (BGl_za2promptza2z00zz__evalz00 = BgL_procz00_28, BUNSPEC);
				}
			else
				{	/* Eval/eval.scm 274 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2640z00zz__evalz00,
						BGl_string2642z00zz__evalz00, BgL_procz00_28);
				}
		}

	}



/* &set-prompter! */
	obj_t BGl_z62setzd2prompterz12za2zz__evalz00(obj_t BgL_envz00_3638,
		obj_t BgL_procz00_3639)
	{
		{	/* Eval/eval.scm 273 */
			{	/* Eval/eval.scm 274 */
				obj_t BgL_auxz00_6116;

				if (PROCEDUREP(BgL_procz00_3639))
					{	/* Eval/eval.scm 274 */
						BgL_auxz00_6116 = BgL_procz00_3639;
					}
				else
					{
						obj_t BgL_auxz00_6119;

						BgL_auxz00_6119 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(11010L), BGl_string2643z00zz__evalz00,
							BGl_string2620z00zz__evalz00, BgL_procz00_3639);
						FAILURE(BgL_auxz00_6119, BFALSE, BFALSE);
					}
				return BGl_setzd2prompterz12zc0zz__evalz00(BgL_auxz00_6116);
			}
		}

	}



/* get-prompter */
	BGL_EXPORTED_DEF obj_t BGl_getzd2prompterzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 283 */
			{	/* Eval/eval.scm 283 */
				obj_t BgL_aux2283z00_3797;

				BgL_aux2283z00_3797 = BGl_za2promptza2z00zz__evalz00;
				if (PROCEDUREP(BgL_aux2283z00_3797))
					{	/* Eval/eval.scm 283 */
						return BgL_aux2283z00_3797;
					}
				else
					{
						obj_t BgL_auxz00_6126;

						BgL_auxz00_6126 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(11393L), BGl_string2644z00zz__evalz00,
							BGl_string2620z00zz__evalz00, BgL_aux2283z00_3797);
						FAILURE(BgL_auxz00_6126, BFALSE, BFALSE);
					}
			}
		}

	}



/* &get-prompter */
	obj_t BGl_z62getzd2prompterzb0zz__evalz00(obj_t BgL_envz00_3640)
	{
		{	/* Eval/eval.scm 283 */
			return BGl_getzd2prompterzd2zz__evalz00();
		}

	}



/* repl */
	BGL_EXPORTED_DEF obj_t BGl_replz00zz__evalz00(void)
	{
		{	/* Eval/eval.scm 295 */
			{	/* Eval/eval.scm 296 */
				obj_t BgL_replzd2quitzd2_1365;
				obj_t BgL_replzd2numzd2_1366;

				BgL_replzd2quitzd2_1365 = BGl_za2replzd2quitza2zd2zz__evalz00;
				BgL_replzd2numzd2_1366 = BGl_za2replzd2numza2zd2zz__evalz00;
				BGl_zc3z04exitza31427ze3ze70z60zz__evalz00(BgL_replzd2quitzd2_1365,
					BgL_replzd2numzd2_1366);
				{	/* Eval/eval.scm 306 */
					obj_t BgL_arg1429z00_1379;

					{	/* Eval/eval.scm 306 */
						obj_t BgL_tmpz00_6132;

						BgL_tmpz00_6132 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1429z00_1379 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6132);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1429z00_1379);
				}
				{	/* Eval/eval.scm 307 */
					obj_t BgL_arg1430z00_1380;

					{	/* Eval/eval.scm 307 */
						obj_t BgL_tmpz00_6136;

						BgL_tmpz00_6136 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1430z00_1380 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6136);
					}
					return bgl_flush_output_port(BgL_arg1430z00_1380);
				}
			}
		}

	}



/* <@exit:1427>~0 */
	obj_t BGl_zc3z04exitza31427ze3ze70z60zz__evalz00(obj_t
		BgL_replzd2quitzd2_3754, obj_t BgL_replzd2numzd2_3753)
	{
		{	/* Eval/eval.scm 298 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1065z00_1368;

			if (SET_EXIT(BgL_an_exit1065z00_1368))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1065z00_1368 = (void *) jmpbuf;
					{	/* Eval/eval.scm 298 */
						obj_t BgL_env1069z00_1369;

						BgL_env1069z00_1369 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1069z00_1369, BgL_an_exit1065z00_1368, 1L);
						{	/* Eval/eval.scm 298 */
							obj_t BgL_an_exitd1066z00_1370;

							BgL_an_exitd1066z00_1370 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1069z00_1369);
							{	/* Eval/eval.scm 298 */
								obj_t BgL_quitz00_3642;

								BgL_quitz00_3642 =
									MAKE_FX_PROCEDURE(BGl_z62quitz62zz__evalz00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_quitz00_3642,
									(int) (0L), BgL_an_exitd1066z00_1370);
								{	/* Eval/eval.scm 298 */
									obj_t BgL_res1068z00_1373;

									BGl_za2replzd2quitza2zd2zz__evalz00 = BgL_quitz00_3642;
									{
										obj_t BgL_tmpz00_6150;

										{	/* Eval/eval.scm 300 */
											obj_t BgL_aux2285z00_3799;

											BgL_aux2285z00_3799 = BGl_za2replzd2numza2zd2zz__evalz00;
											if (INTEGERP(BgL_aux2285z00_3799))
												{	/* Eval/eval.scm 300 */
													BgL_tmpz00_6150 = BgL_aux2285z00_3799;
												}
											else
												{
													obj_t BgL_auxz00_6154;

													BgL_auxz00_6154 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(12102L),
														BGl_string2645z00zz__evalz00,
														BGl_string2635z00zz__evalz00, BgL_aux2285z00_3799);
													FAILURE(BgL_auxz00_6154, BFALSE, BFALSE);
												}
										}
										BGl_za2replzd2numza2zd2zz__evalz00 =
											ADDFX(BINT(1L), BgL_tmpz00_6150);
									}
									{	/* Eval/eval.scm 301 */
										obj_t BgL_exitd1061z00_1374;

										BgL_exitd1061z00_1374 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Eval/eval.scm 304 */
											obj_t BgL_zc3z04anonymousza31428ze3z87_3641;

											BgL_zc3z04anonymousza31428ze3z87_3641 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31428ze3ze5zz__evalz00,
												(int) (0L), (int) (2L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31428ze3z87_3641,
												(int) (0L), BgL_replzd2numzd2_3753);
											PROCEDURE_SET(BgL_zc3z04anonymousza31428ze3z87_3641,
												(int) (1L), BgL_replzd2quitzd2_3754);
											{	/* Eval/eval.scm 301 */
												obj_t BgL_arg2211z00_2662;

												{	/* Eval/eval.scm 301 */
													obj_t BgL_arg2212z00_2663;

													BgL_arg2212z00_2663 =
														BGL_EXITD_PROTECT(BgL_exitd1061z00_1374);
													BgL_arg2211z00_2662 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31428ze3z87_3641,
														BgL_arg2212z00_2663);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1061z00_1374,
													BgL_arg2211z00_2662);
												BUNSPEC;
											}
											{	/* Eval/eval.scm 302 */
												obj_t BgL_tmp1063z00_1376;

												BgL_tmp1063z00_1376 =
													BGl_internalzd2replzd2zz__evalz00();
												{	/* Eval/eval.scm 301 */
													bool_t BgL_test3048z00_6171;

													{	/* Eval/eval.scm 301 */
														obj_t BgL_arg2210z00_2665;

														BgL_arg2210z00_2665 =
															BGL_EXITD_PROTECT(BgL_exitd1061z00_1374);
														BgL_test3048z00_6171 = PAIRP(BgL_arg2210z00_2665);
													}
													if (BgL_test3048z00_6171)
														{	/* Eval/eval.scm 301 */
															obj_t BgL_arg2208z00_2666;

															{	/* Eval/eval.scm 301 */
																obj_t BgL_arg2209z00_2667;

																BgL_arg2209z00_2667 =
																	BGL_EXITD_PROTECT(BgL_exitd1061z00_1374);
																{	/* Eval/eval.scm 301 */
																	obj_t BgL_pairz00_2668;

																	if (PAIRP(BgL_arg2209z00_2667))
																		{	/* Eval/eval.scm 301 */
																			BgL_pairz00_2668 = BgL_arg2209z00_2667;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6177;

																			BgL_auxz00_6177 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(12117L),
																				BGl_string2645z00zz__evalz00,
																				BGl_string2626z00zz__evalz00,
																				BgL_arg2209z00_2667);
																			FAILURE(BgL_auxz00_6177, BFALSE, BFALSE);
																		}
																	BgL_arg2208z00_2666 = CDR(BgL_pairz00_2668);
																}
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1061z00_1374,
																BgL_arg2208z00_2666);
															BUNSPEC;
														}
													else
														{	/* Eval/eval.scm 301 */
															BFALSE;
														}
												}
												BGl_za2replzd2numza2zd2zz__evalz00 =
													BgL_replzd2numzd2_3753;
												BGl_za2replzd2quitza2zd2zz__evalz00 =
													BgL_replzd2quitzd2_3754;
												BgL_res1068z00_1373 = BgL_tmp1063z00_1376;
											}
										}
									}
									POP_ENV_EXIT(BgL_env1069z00_1369);
									return BgL_res1068z00_1373;
								}
							}
						}
					}
				}
		}

	}



/* &repl */
	obj_t BGl_z62replz62zz__evalz00(obj_t BgL_envz00_3643)
	{
		{	/* Eval/eval.scm 295 */
			return BGl_replz00zz__evalz00();
		}

	}



/* &<@anonymous:1428> */
	obj_t BGl_z62zc3z04anonymousza31428ze3ze5zz__evalz00(obj_t BgL_envz00_3644)
	{
		{	/* Eval/eval.scm 301 */
			{	/* Eval/eval.scm 304 */
				obj_t BgL_replzd2numzd2_3645;
				obj_t BgL_replzd2quitzd2_3646;

				BgL_replzd2numzd2_3645 = PROCEDURE_REF(BgL_envz00_3644, (int) (0L));
				BgL_replzd2quitzd2_3646 = PROCEDURE_REF(BgL_envz00_3644, (int) (1L));
				BGl_za2replzd2numza2zd2zz__evalz00 = BgL_replzd2numzd2_3645;
				return (BGl_za2replzd2quitza2zd2zz__evalz00 =
					BgL_replzd2quitzd2_3646, BUNSPEC);
			}
		}

	}



/* &quit */
	obj_t BGl_z62quitz62zz__evalz00(obj_t BgL_envz00_3647,
		obj_t BgL_val1067z00_3649)
	{
		{	/* Eval/eval.scm 298 */
			return
				unwind_stack_until(PROCEDURE_REF(BgL_envz00_3647,
					(int) (0L)), BFALSE, BgL_val1067z00_3649, BFALSE, BFALSE);
		}

	}



/* get-eval-reader */
	obj_t BGl_getzd2evalzd2readerz00zz__evalz00(void)
	{
		{	/* Eval/eval.scm 312 */
			{	/* Eval/eval.scm 313 */
				obj_t BgL__ortest_1071z00_1381;

				BgL__ortest_1071z00_1381 = BGl_bigloozd2loadzd2readerz00zz__paramz00();
				if (CBOOL(BgL__ortest_1071z00_1381))
					{	/* Eval/eval.scm 313 */
						return BgL__ortest_1071z00_1381;
					}
				else
					{	/* Eval/eval.scm 313 */
						return BGl_proc2646z00zz__evalz00;
					}
			}
		}

	}



/* &<@anonymous:1431> */
	obj_t BGl_z62zc3z04anonymousza31431ze3ze5zz__evalz00(obj_t BgL_envz00_3651,
		obj_t BgL_pz00_3652)
	{
		{	/* Eval/eval.scm 314 */
			{	/* Eval/eval.scm 314 */
				obj_t BgL_auxz00_6195;

				if (INPUT_PORTP(BgL_pz00_3652))
					{	/* Eval/eval.scm 314 */
						BgL_auxz00_6195 = BgL_pz00_3652;
					}
				else
					{
						obj_t BgL_auxz00_6198;

						BgL_auxz00_6198 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(12603L), BGl_string2647z00zz__evalz00,
							BGl_string2648z00zz__evalz00, BgL_pz00_3652);
						FAILURE(BgL_auxz00_6198, BFALSE, BFALSE);
					}
				return BGl_readz00zz__readerz00(BgL_auxz00_6195, BTRUE);
			}
		}

	}



/* set-repl-error-notifier! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2replzd2errorzd2notifierz12zc0zz__evalz00(obj_t BgL_procz00_29)
	{
		{	/* Eval/eval.scm 319 */
			{	/* Eval/eval.scm 320 */
				bool_t BgL_test3052z00_6203;

				{	/* Eval/eval.scm 320 */
					obj_t BgL_tmpz00_6204;

					if (PROCEDUREP(BgL_procz00_29))
						{	/* Eval/eval.scm 320 */
							BgL_tmpz00_6204 = BgL_procz00_29;
						}
					else
						{
							obj_t BgL_auxz00_6207;

							BgL_auxz00_6207 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
								BINT(12904L), BGl_string2649z00zz__evalz00,
								BGl_string2620z00zz__evalz00, BgL_procz00_29);
							FAILURE(BgL_auxz00_6207, BFALSE, BFALSE);
						}
					BgL_test3052z00_6203 =
						PROCEDURE_CORRECT_ARITYP(BgL_tmpz00_6204, (int) (1L));
				}
				if (BgL_test3052z00_6203)
					{	/* Eval/eval.scm 321 */
						obj_t BgL_arg1434z00_2673;

						{	/* Eval/eval.scm 321 */
							obj_t BgL_list1435z00_2674;

							BgL_list1435z00_2674 = MAKE_YOUNG_PAIR(BgL_procz00_29, BNIL);
							BgL_arg1434z00_2673 = BgL_list1435z00_2674;
						}
						return BGL_ERROR_NOTIFIERS_SET(BgL_arg1434z00_2673);
					}
				else
					{	/* Eval/eval.scm 320 */
						return
							BGl_errorz00zz__errorz00(BGl_string2649z00zz__evalz00,
							BGl_string2650z00zz__evalz00, BgL_procz00_29);
					}
			}
		}

	}



/* &set-repl-error-notifier! */
	obj_t BGl_z62setzd2replzd2errorzd2notifierz12za2zz__evalz00(obj_t
		BgL_envz00_3653, obj_t BgL_procz00_3654)
	{
		{	/* Eval/eval.scm 319 */
			return
				BGl_setzd2replzd2errorzd2notifierz12zc0zz__evalz00(BgL_procz00_3654);
		}

	}



/* get-repl-error-notifier */
	BGL_EXPORTED_DEF obj_t BGl_getzd2replzd2errorzd2notifierzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 327 */
			{	/* Eval/eval.scm 328 */
				bool_t BgL_test3054z00_6217;

				{	/* Eval/eval.scm 328 */
					obj_t BgL_arg1439z00_1389;

					BgL_arg1439z00_1389 = BGL_ERROR_NOTIFIERS_GET();
					BgL_test3054z00_6217 = PAIRP(BgL_arg1439z00_1389);
				}
				if (BgL_test3054z00_6217)
					{	/* Eval/eval.scm 329 */
						obj_t BgL_arg1438z00_1388;

						BgL_arg1438z00_1388 = BGL_ERROR_NOTIFIERS_GET();
						{	/* Eval/eval.scm 329 */
							obj_t BgL_pairz00_2676;

							if (PAIRP(BgL_arg1438z00_1388))
								{	/* Eval/eval.scm 329 */
									BgL_pairz00_2676 = BgL_arg1438z00_1388;
								}
							else
								{
									obj_t BgL_auxz00_6223;

									BgL_auxz00_6223 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string2618z00zz__evalz00, BINT(13368L),
										BGl_string2651z00zz__evalz00, BGl_string2626z00zz__evalz00,
										BgL_arg1438z00_1388);
									FAILURE(BgL_auxz00_6223, BFALSE, BFALSE);
								}
							return CAR(BgL_pairz00_2676);
						}
					}
				else
					{	/* Eval/eval.scm 328 */
						return BFALSE;
					}
			}
		}

	}



/* &get-repl-error-notifier */
	obj_t BGl_z62getzd2replzd2errorzd2notifierzb0zz__evalz00(obj_t
		BgL_envz00_3655)
	{
		{	/* Eval/eval.scm 327 */
			return BGl_getzd2replzd2errorzd2notifierzd2zz__evalz00();
		}

	}



/* internal-repl */
	obj_t BGl_internalzd2replzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 334 */
			{	/* Eval/eval.scm 335 */
				obj_t BgL_oldzd2intrhdlzd2_1390;

				BgL_oldzd2intrhdlzd2_1390 =
					BGl_getzd2signalzd2handlerz00zz__osz00(SIGINT);
				{	/* Eval/eval.scm 338 */
					obj_t BgL_exitd1072z00_1392;

					BgL_exitd1072z00_1392 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Eval/eval.scm 379 */
						obj_t BgL_zc3z04anonymousza31466ze3z87_3658;

						BgL_zc3z04anonymousza31466ze3z87_3658 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31466ze3ze5zz__evalz00,
							(int) (0L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31466ze3z87_3658,
							(int) (0L), BgL_oldzd2intrhdlzd2_1390);
						{	/* Eval/eval.scm 338 */
							obj_t BgL_arg2211z00_2681;

							{	/* Eval/eval.scm 338 */
								obj_t BgL_arg2212z00_2682;

								BgL_arg2212z00_2682 = BGL_EXITD_PROTECT(BgL_exitd1072z00_1392);
								BgL_arg2211z00_2681 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31466ze3z87_3658,
									BgL_arg2212z00_2682);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1072z00_1392, BgL_arg2211z00_2681);
							BUNSPEC;
						}
						{	/* Eval/eval.scm 339 */
							obj_t BgL_tmp1074z00_1394;

							{	/* Eval/eval.scm 339 */
								obj_t BgL_g1077z00_1395;

								BgL_g1077z00_1395 = BGl_evalzd2modulezd2zz__evmodulez00();
								{
									obj_t BgL_modz00_1397;

									BgL_modz00_1397 = BgL_g1077z00_1395;
								BgL_zc3z04anonymousza31440ze3z87_1398:
									BGl_zc3z04exitza31441ze3ze70z60zz__evalz00(BgL_modz00_1397);
									{

										goto BgL_zc3z04anonymousza31440ze3z87_1398;
									}
								}
							}
							{	/* Eval/eval.scm 338 */
								bool_t BgL_test3056z00_6241;

								{	/* Eval/eval.scm 338 */
									obj_t BgL_arg2210z00_2707;

									BgL_arg2210z00_2707 =
										BGL_EXITD_PROTECT(BgL_exitd1072z00_1392);
									BgL_test3056z00_6241 = PAIRP(BgL_arg2210z00_2707);
								}
								if (BgL_test3056z00_6241)
									{	/* Eval/eval.scm 338 */
										obj_t BgL_arg2208z00_2708;

										{	/* Eval/eval.scm 338 */
											obj_t BgL_arg2209z00_2709;

											BgL_arg2209z00_2709 =
												BGL_EXITD_PROTECT(BgL_exitd1072z00_1392);
											{	/* Eval/eval.scm 338 */
												obj_t BgL_pairz00_2710;

												if (PAIRP(BgL_arg2209z00_2709))
													{	/* Eval/eval.scm 338 */
														BgL_pairz00_2710 = BgL_arg2209z00_2709;
													}
												else
													{
														obj_t BgL_auxz00_6247;

														BgL_auxz00_6247 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2618z00zz__evalz00, BINT(13748L),
															BGl_string2652z00zz__evalz00,
															BGl_string2626z00zz__evalz00,
															BgL_arg2209z00_2709);
														FAILURE(BgL_auxz00_6247, BFALSE, BFALSE);
													}
												BgL_arg2208z00_2708 = CDR(BgL_pairz00_2710);
											}
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1072z00_1392,
											BgL_arg2208z00_2708);
										BUNSPEC;
									}
								else
									{	/* Eval/eval.scm 338 */
										BFALSE;
									}
							}
							BGl_z62zc3z04anonymousza31466ze3ze5zz__evalz00
								(BgL_zc3z04anonymousza31466ze3z87_3658);
							return BgL_tmp1074z00_1394;
						}
					}
				}
			}
		}

	}



/* <@exit:1447>~0 */
	obj_t BGl_zc3z04exitza31447ze3ze70z60zz__evalz00(obj_t BgL_modz00_3751,
		obj_t BgL_cell1081z00_3750, obj_t BgL_env1086z00_3749)
	{
		{	/* Eval/eval.scm 353 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1092z00_1421;

			if (SET_EXIT(BgL_an_exit1092z00_1421))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1092z00_1421 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1086z00_3749, BgL_an_exit1092z00_1421, 1L);
					{	/* Eval/eval.scm 353 */
						obj_t BgL_escape1082z00_1422;

						BgL_escape1082z00_1422 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1086z00_3749);
						{	/* Eval/eval.scm 353 */
							obj_t BgL_res1095z00_1423;

							{	/* Eval/eval.scm 353 */
								obj_t BgL_ohs1078z00_1424;

								BgL_ohs1078z00_1424 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1086z00_3749);
								{	/* Eval/eval.scm 353 */
									obj_t BgL_hds1079z00_1425;

									BgL_hds1079z00_1425 =
										MAKE_STACK_PAIR(BgL_escape1082z00_1422,
										BgL_cell1081z00_3750);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1086z00_3749,
										BgL_hds1079z00_1425);
									BUNSPEC;
									{	/* Eval/eval.scm 353 */
										obj_t BgL_exitd1088z00_1426;

										BgL_exitd1088z00_1426 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1086z00_3749);
										{	/* Eval/eval.scm 353 */
											obj_t BgL_tmp1090z00_1427;

											{	/* Eval/eval.scm 353 */
												obj_t BgL_arg1455z00_1446;

												BgL_arg1455z00_1446 =
													BGL_EXITD_PROTECT(BgL_exitd1088z00_1426);
												BgL_tmp1090z00_1427 =
													MAKE_YOUNG_PAIR(BgL_ohs1078z00_1424,
													BgL_arg1455z00_1446);
											}
											{	/* Eval/eval.scm 353 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1088z00_1426,
													BgL_tmp1090z00_1427);
												BUNSPEC;
												{	/* Eval/eval.scm 364 */
													obj_t BgL_tmp1089z00_1428;

													{
														obj_t BgL_modz00_1430;

														BgL_modz00_1430 = BgL_modz00_3751;
													BgL_zc3z04anonymousza31448ze3z87_1431:
														{	/* Eval/eval.scm 364 */
															obj_t BgL_tmpfunz00_6268;

															{	/* Eval/eval.scm 364 */
																obj_t BgL_aux2296z00_3810;

																BgL_aux2296z00_3810 =
																	BGl_za2promptza2z00zz__evalz00;
																{	/* Eval/eval.scm 364 */
																	bool_t BgL_test2297z00_3811;

																	BgL_test2297z00_3811 =
																		PROCEDUREP(BgL_aux2296z00_3810);
																	if (BgL_test2297z00_3811)
																		{	/* Eval/eval.scm 364 */
																			BgL_tmpfunz00_6268 = BgL_aux2296z00_3810;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6271;

																			BgL_auxz00_6271 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(14554L),
																				BGl_string2653z00zz__evalz00,
																				BGl_string2620z00zz__evalz00,
																				BgL_aux2296z00_3810);
																			FAILURE(BgL_auxz00_6271, BFALSE, BFALSE);
																		}
																}
															}
															(VA_PROCEDUREP(BgL_tmpfunz00_6268)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_6268))
																(BGl_za2promptza2z00zz__evalz00,
																	BGl_za2replzd2numza2zd2zz__evalz00,
																	BEOA) : ((obj_t(*)(obj_t,
																			obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_6268))
																(BGl_za2promptza2z00zz__evalz00,
																	BGl_za2replzd2numza2zd2zz__evalz00));
														}
														{	/* Eval/eval.scm 365 */
															obj_t BgL_expz00_1432;

															{	/* Eval/eval.scm 337 */
																obj_t BgL_fun1473z00_2690;

																BgL_fun1473z00_2690 =
																	BGl_getzd2evalzd2readerz00zz__evalz00();
																{	/* Eval/eval.scm 337 */
																	obj_t BgL_arg1472z00_2691;

																	{	/* Eval/eval.scm 337 */
																		obj_t BgL_tmpz00_6276;

																		BgL_tmpz00_6276 = BGL_CURRENT_DYNAMIC_ENV();
																		BgL_arg1472z00_2691 =
																			BGL_ENV_CURRENT_INPUT_PORT
																			(BgL_tmpz00_6276);
																	}
																	{	/* Eval/eval.scm 337 */
																		obj_t BgL_tmpfunz00_6282;

																		{	/* Eval/eval.scm 337 */
																			bool_t BgL_test2299z00_3813;

																			BgL_test2299z00_3813 =
																				PROCEDUREP(BgL_fun1473z00_2690);
																			if (BgL_test2299z00_3813)
																				{	/* Eval/eval.scm 337 */
																					BgL_tmpfunz00_6282 =
																						BgL_fun1473z00_2690;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6285;

																					BgL_auxz00_6285 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(13698L),
																						BGl_string2653z00zz__evalz00,
																						BGl_string2620z00zz__evalz00,
																						BgL_fun1473z00_2690);
																					FAILURE(BgL_auxz00_6285, BFALSE,
																						BFALSE);
																				}
																		}
																		BgL_expz00_1432 =
																			(VA_PROCEDUREP(BgL_tmpfunz00_6282)
																			? ((obj_t(*)(obj_t,
																						...))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_6282))
																			(BgL_fun1473z00_2690, BgL_arg1472z00_2691,
																				BEOA) : ((obj_t(*)(obj_t,
																						obj_t))
																				PROCEDURE_ENTRY(BgL_tmpfunz00_6282))
																			(BgL_fun1473z00_2690,
																				BgL_arg1472z00_2691));
																	}
																}
															}
															if (EOF_OBJECTP(BgL_expz00_1432))
																{	/* Eval/eval.scm 431 */
																	obj_t BgL_tmpfunz00_6295;

																	{	/* Eval/eval.scm 431 */
																		obj_t BgL_aux2300z00_3814;

																		BgL_aux2300z00_3814 =
																			BGl_za2replzd2quitza2zd2zz__evalz00;
																		{	/* Eval/eval.scm 431 */
																			bool_t BgL_test2301z00_3815;

																			BgL_test2301z00_3815 =
																				PROCEDUREP(BgL_aux2300z00_3814);
																			if (BgL_test2301z00_3815)
																				{	/* Eval/eval.scm 431 */
																					BgL_tmpfunz00_6295 =
																						BgL_aux2300z00_3814;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6298;

																					BgL_auxz00_6298 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(17398L),
																						BGl_string2653z00zz__evalz00,
																						BGl_string2620z00zz__evalz00,
																						BgL_aux2300z00_3814);
																					FAILURE(BgL_auxz00_6298, BFALSE,
																						BFALSE);
																				}
																		}
																	}
																	BgL_tmp1089z00_1428 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_6295)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_6295))
																		(BGl_za2replzd2quitza2zd2zz__evalz00,
																			BINT(0L), BEOA) : ((obj_t(*)(obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_6295))
																		(BGl_za2replzd2quitza2zd2zz__evalz00,
																			BINT(0L)));
																}
															else
																{	/* Eval/eval.scm 368 */
																	obj_t BgL_vz00_1434;

																	{	/* Eval/eval.scm 81 */
																		obj_t BgL_envz00_1444;

																		{	/* Eval/eval.scm 257 */
																			obj_t BgL_mz00_2693;

																			BgL_mz00_2693 =
																				BGl_evalzd2modulezd2zz__evmodulez00();
																			if (BGl_evmodulezf3zf3zz__evmodulez00
																				(BgL_mz00_2693))
																				{	/* Eval/eval.scm 258 */
																					BgL_envz00_1444 = BgL_mz00_2693;
																				}
																			else
																				{	/* Eval/eval.scm 258 */
																					BgL_envz00_1444 =
																						BGl_symbol2616z00zz__evalz00;
																				}
																		}
																		{	/* Eval/eval.scm 81 */

																			{	/* Eval/eval.scm 170 */
																				obj_t BgL_auxz00_6305;

																				{	/* Eval/eval.scm 170 */
																					obj_t BgL_aux2302z00_3816;

																					BgL_aux2302z00_3816 =
																						BGl_defaultzd2evaluatezd2zz__evalz00;
																					if (PROCEDUREP(BgL_aux2302z00_3816))
																						{	/* Eval/eval.scm 170 */
																							BgL_auxz00_6305 =
																								BgL_aux2302z00_3816;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6308;

																							BgL_auxz00_6308 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(6408L),
																								BGl_string2653z00zz__evalz00,
																								BGl_string2620z00zz__evalz00,
																								BgL_aux2302z00_3816);
																							FAILURE(BgL_auxz00_6308, BFALSE,
																								BFALSE);
																						}
																				}
																				BgL_vz00_1434 =
																					BGl_evalzf2expanderzf2zz__evalz00
																					(BgL_expz00_1432, BgL_envz00_1444,
																					BGl_expandzd2envzd2zz__expandz00,
																					BgL_auxz00_6305);
																			}
																		}
																	}
																	{	/* Eval/eval.scm 368 */
																		obj_t BgL_nmodz00_1435;

																		BgL_nmodz00_1435 =
																			BGl_evalzd2modulezd2zz__evmodulez00();
																		{	/* Eval/eval.scm 369 */

																			{	/* Eval/eval.scm 370 */
																				bool_t BgL_test3064z00_6314;

																				if (
																					(BgL_nmodz00_1435 == BgL_modz00_1430))
																					{	/* Eval/eval.scm 370 */
																						BgL_test3064z00_6314 = ((bool_t) 0);
																					}
																				else
																					{	/* Eval/eval.scm 370 */
																						BgL_test3064z00_6314 =
																							BGl_evmodulezf3zf3zz__evmodulez00
																							(BgL_modz00_1430);
																					}
																				if (BgL_test3064z00_6314)
																					{	/* Eval/eval.scm 370 */
																						BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00
																							(BgL_modz00_1430, BFALSE);
																					}
																				else
																					{	/* Eval/eval.scm 370 */
																						BFALSE;
																					}
																			}
																			{	/* Eval/eval.scm 373 */
																				bool_t BgL_test3066z00_6319;

																				{	/* Eval/eval.scm 373 */
																					obj_t BgL_arg1453z00_1440;

																					{	/* Eval/eval.scm 373 */
																						obj_t BgL_tmpz00_6320;

																						BgL_tmpz00_6320 =
																							BGL_CURRENT_DYNAMIC_ENV();
																						BgL_arg1453z00_1440 =
																							BGL_ENV_CURRENT_OUTPUT_PORT
																							(BgL_tmpz00_6320);
																					}
																					BgL_test3066z00_6319 =
																						(BGl_za2transcriptza2z00zz__evalz00
																						== BgL_arg1453z00_1440);
																				}
																				if (BgL_test3066z00_6319)
																					{	/* Eval/eval.scm 373 */
																						BFALSE;
																					}
																				else
																					{	/* Eval/eval.scm 374 */
																						obj_t BgL_port1244z00_1439;

																						BgL_port1244z00_1439 =
																							BGl_za2transcriptza2z00zz__evalz00;
																						{	/* Eval/eval.scm 374 */
																							obj_t BgL_portz00_2697;

																							if (OUTPUT_PORTP
																								(BgL_port1244z00_1439))
																								{	/* Eval/eval.scm 374 */
																									BgL_portz00_2697 =
																										BgL_port1244z00_1439;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6326;

																									BgL_auxz00_6326 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(14884L),
																										BGl_string2653z00zz__evalz00,
																										BGl_string2654z00zz__evalz00,
																										BgL_port1244z00_1439);
																									FAILURE(BgL_auxz00_6326,
																										BFALSE, BFALSE);
																								}
																							bgl_display_string
																								(BGl_string2655z00zz__evalz00,
																								BgL_portz00_2697);
																						}
																						{	/* Eval/eval.scm 374 */
																							obj_t BgL_auxz00_6331;

																							if (OUTPUT_PORTP
																								(BgL_port1244z00_1439))
																								{	/* Eval/eval.scm 374 */
																									BgL_auxz00_6331 =
																										BgL_port1244z00_1439;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6334;

																									BgL_auxz00_6334 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(14884L),
																										BGl_string2653z00zz__evalz00,
																										BGl_string2654z00zz__evalz00,
																										BgL_port1244z00_1439);
																									FAILURE(BgL_auxz00_6334,
																										BFALSE, BFALSE);
																								}
																							bgl_display_obj(BgL_expz00_1432,
																								BgL_auxz00_6331);
																						}
																						{	/* Eval/eval.scm 374 */
																							obj_t BgL_portz00_2698;

																							if (OUTPUT_PORTP
																								(BgL_port1244z00_1439))
																								{	/* Eval/eval.scm 374 */
																									BgL_portz00_2698 =
																										BgL_port1244z00_1439;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6341;

																									BgL_auxz00_6341 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(14884L),
																										BGl_string2653z00zz__evalz00,
																										BGl_string2654z00zz__evalz00,
																										BgL_port1244z00_1439);
																									FAILURE(BgL_auxz00_6341,
																										BFALSE, BFALSE);
																								}
																							bgl_display_char(((unsigned char)
																									10), BgL_portz00_2698);
																			}}}
																			{	/* Eval/eval.scm 375 */
																				obj_t BgL_tmpfunz00_6350;

																				{	/* Eval/eval.scm 375 */
																					obj_t BgL_aux2310z00_3824;

																					BgL_aux2310z00_3824 =
																						BGl_za2replzd2printerza2zd2zz__evalz00;
																					{	/* Eval/eval.scm 375 */
																						bool_t BgL_test2311z00_3825;

																						BgL_test2311z00_3825 =
																							PROCEDUREP(BgL_aux2310z00_3824);
																						if (BgL_test2311z00_3825)
																							{	/* Eval/eval.scm 375 */
																								BgL_tmpfunz00_6350 =
																									BgL_aux2310z00_3824;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6353;

																								BgL_auxz00_6353 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(14923L),
																									BGl_string2653z00zz__evalz00,
																									BGl_string2620z00zz__evalz00,
																									BgL_aux2310z00_3824);
																								FAILURE(BgL_auxz00_6353, BFALSE,
																									BFALSE);
																							}
																					}
																				}
																				(VA_PROCEDUREP(BgL_tmpfunz00_6350)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_6350))
																					(BGl_za2replzd2printerza2zd2zz__evalz00,
																						BgL_vz00_1434,
																						BGl_za2transcriptza2z00zz__evalz00,
																						BEOA) : ((obj_t(*)(obj_t, obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_6350))
																					(BGl_za2replzd2printerza2zd2zz__evalz00,
																						BgL_vz00_1434,
																						BGl_za2transcriptza2z00zz__evalz00));
																			}
																			{	/* Eval/eval.scm 376 */
																				obj_t BgL_portz00_2699;

																				{	/* Eval/eval.scm 376 */
																					obj_t BgL_aux2312z00_3826;

																					BgL_aux2312z00_3826 =
																						BGl_za2transcriptza2z00zz__evalz00;
																					if (OUTPUT_PORTP(BgL_aux2312z00_3826))
																						{	/* Eval/eval.scm 376 */
																							BgL_portz00_2699 =
																								BgL_aux2312z00_3826;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6359;

																							BgL_auxz00_6359 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(14961L),
																								BGl_string2653z00zz__evalz00,
																								BGl_string2654z00zz__evalz00,
																								BgL_aux2312z00_3826);
																							FAILURE(BgL_auxz00_6359, BFALSE,
																								BFALSE);
																						}
																				}
																				bgl_display_char(((unsigned char) 10),
																					BgL_portz00_2699);
																			}
																			{	/* Eval/eval.scm 377 */
																				obj_t BgL_arg1454z00_1441;

																				if (CBOOL(BgL_nmodz00_1435))
																					{	/* Eval/eval.scm 377 */
																						BgL_arg1454z00_1441 =
																							BgL_nmodz00_1435;
																					}
																				else
																					{	/* Eval/eval.scm 377 */
																						BgL_arg1454z00_1441 =
																							BgL_modz00_1430;
																					}
																				{
																					obj_t BgL_modz00_6366;

																					BgL_modz00_6366 = BgL_arg1454z00_1441;
																					BgL_modz00_1430 = BgL_modz00_6366;
																					goto
																						BgL_zc3z04anonymousza31448ze3z87_1431;
																				}
																			}
																		}
																	}
																}
														}
													}
													{	/* Eval/eval.scm 353 */
														bool_t BgL_test3073z00_6367;

														{	/* Eval/eval.scm 353 */
															obj_t BgL_arg2210z00_2701;

															BgL_arg2210z00_2701 =
																BGL_EXITD_PROTECT(BgL_exitd1088z00_1426);
															BgL_test3073z00_6367 = PAIRP(BgL_arg2210z00_2701);
														}
														if (BgL_test3073z00_6367)
															{	/* Eval/eval.scm 353 */
																obj_t BgL_arg2208z00_2702;

																{	/* Eval/eval.scm 353 */
																	obj_t BgL_arg2209z00_2703;

																	BgL_arg2209z00_2703 =
																		BGL_EXITD_PROTECT(BgL_exitd1088z00_1426);
																	{	/* Eval/eval.scm 353 */
																		obj_t BgL_pairz00_2704;

																		if (PAIRP(BgL_arg2209z00_2703))
																			{	/* Eval/eval.scm 353 */
																				BgL_pairz00_2704 = BgL_arg2209z00_2703;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6373;

																				BgL_auxz00_6373 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(14247L),
																					BGl_string2656z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_arg2209z00_2703);
																				FAILURE(BgL_auxz00_6373, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2208z00_2702 = CDR(BgL_pairz00_2704);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1088z00_1426,
																	BgL_arg2208z00_2702);
																BUNSPEC;
															}
														else
															{	/* Eval/eval.scm 353 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1086z00_3749,
														BgL_ohs1078z00_1424);
													BUNSPEC;
													BgL_res1095z00_1423 = BgL_tmp1089z00_1428;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1086z00_3749);
							return BgL_res1095z00_1423;
						}
					}
				}
		}

	}



/* <@exit:1441>~0 */
	obj_t BGl_zc3z04exitza31441ze3ze70z60zz__evalz00(obj_t BgL_modz00_3752)
	{
		{	/* Eval/eval.scm 340 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1097z00_1400;

			if (SET_EXIT(BgL_an_exit1097z00_1400))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1097z00_1400 = (void *) jmpbuf;
					{	/* Eval/eval.scm 340 */
						obj_t BgL_env1101z00_1401;

						BgL_env1101z00_1401 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1101z00_1401, BgL_an_exit1097z00_1400, 1L);
						{	/* Eval/eval.scm 340 */
							obj_t BgL_an_exitd1098z00_1402;

							BgL_an_exitd1098z00_1402 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1101z00_1401);
							{	/* Eval/eval.scm 340 */
								obj_t BgL_res1100z00_1405;

								{	/* Eval/eval.scm 343 */
									obj_t BgL_intrhdlz00_3656;

									BgL_intrhdlz00_3656 =
										MAKE_FX_PROCEDURE(BGl_z62intrhdlz62zz__evalz00,
										(int) (1L), (int) (1L));
									PROCEDURE_SET(BgL_intrhdlz00_3656,
										(int) (0L), BgL_an_exitd1098z00_1402);
									BGl_signalz00zz__osz00(SIGINT, BgL_intrhdlz00_3656);
								}
								{	/* Eval/eval.scm 351 */
									obj_t BgL_arg1444z00_1411;

									{	/* Eval/eval.scm 351 */
										obj_t BgL_tmpz00_6392;

										BgL_tmpz00_6392 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1444z00_1411 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6392);
									}
									bgl_display_char(((unsigned char) 10), BgL_arg1444z00_1411);
								}
								{
									obj_t BgL_modz00_1413;

									BgL_modz00_1413 = BgL_modz00_3752;
								BgL_zc3z04anonymousza31445ze3z87_1414:
									{	/* Eval/eval.scm 353 */
										obj_t BgL_env1086z00_1416;

										BgL_env1086z00_1416 = BGL_CURRENT_DYNAMIC_ENV();
										{
											obj_t BgL_ez00_1447;

											{	/* Eval/eval.scm 353 */
												obj_t BgL_cell1081z00_1417;

												BgL_cell1081z00_1417 = MAKE_STACK_CELL(BUNSPEC);
												{	/* Eval/eval.scm 353 */
													obj_t BgL_val1085z00_1418;

													BgL_val1085z00_1418 =
														BGl_zc3z04exitza31447ze3ze70z60zz__evalz00
														(BgL_modz00_1413, BgL_cell1081z00_1417,
														BgL_env1086z00_1416);
													if ((BgL_val1085z00_1418 == BgL_cell1081z00_1417))
														{	/* Eval/eval.scm 353 */
															{	/* Eval/eval.scm 353 */
																int BgL_tmpz00_6401;

																BgL_tmpz00_6401 = (int) (0L);
																BGL_SIGSETMASK(BgL_tmpz00_6401);
															}
															{	/* Eval/eval.scm 353 */
																obj_t BgL_arg1446z00_1419;

																BgL_arg1446z00_1419 =
																	CELL_REF(((obj_t) BgL_val1085z00_1418));
																BgL_ez00_1447 = BgL_arg1446z00_1419;
																{	/* Eval/eval.scm 355 */
																	bool_t BgL_test3076z00_6406;

																	{	/* Eval/eval.scm 355 */
																		obj_t BgL_arg1461z00_1453;

																		BgL_arg1461z00_1453 =
																			BGL_ERROR_NOTIFIERS_GET();
																		BgL_test3076z00_6406 =
																			PAIRP(BgL_arg1461z00_1453);
																	}
																	if (BgL_test3076z00_6406)
																		{	/* Eval/eval.scm 356 */
																			obj_t BgL_fun1460z00_1452;

																			{	/* Eval/eval.scm 356 */
																				obj_t BgL_arg1459z00_1451;

																				BgL_arg1459z00_1451 =
																					BGL_ERROR_NOTIFIERS_GET();
																				{	/* Eval/eval.scm 356 */
																					obj_t BgL_pairz00_2687;

																					if (PAIRP(BgL_arg1459z00_1451))
																						{	/* Eval/eval.scm 356 */
																							BgL_pairz00_2687 =
																								BgL_arg1459z00_1451;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6412;

																							BgL_auxz00_6412 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(14352L),
																								BGl_string2657z00zz__evalz00,
																								BGl_string2626z00zz__evalz00,
																								BgL_arg1459z00_1451);
																							FAILURE(BgL_auxz00_6412, BFALSE,
																								BFALSE);
																						}
																					BgL_fun1460z00_1452 =
																						CAR(BgL_pairz00_2687);
																				}
																			}
																			{	/* Eval/eval.scm 356 */
																				obj_t BgL_tmpfunz00_6420;

																				{	/* Eval/eval.scm 356 */
																					bool_t BgL_test2319z00_3833;

																					BgL_test2319z00_3833 =
																						PROCEDUREP(BgL_fun1460z00_1452);
																					if (BgL_test2319z00_3833)
																						{	/* Eval/eval.scm 356 */
																							BgL_tmpfunz00_6420 =
																								BgL_fun1460z00_1452;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6423;

																							BgL_auxz00_6423 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(14325L),
																								BGl_string2657z00zz__evalz00,
																								BGl_string2620z00zz__evalz00,
																								BgL_fun1460z00_1452);
																							FAILURE(BgL_auxz00_6423, BFALSE,
																								BFALSE);
																						}
																				}
																				(VA_PROCEDUREP(BgL_tmpfunz00_6420)
																					? ((obj_t(*)(obj_t,
																								...))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_6420))
																					(BgL_fun1460z00_1452, BgL_ez00_1447,
																						BEOA) : ((obj_t(*)(obj_t,
																								obj_t))
																						PROCEDURE_ENTRY(BgL_tmpfunz00_6420))
																					(BgL_fun1460z00_1452, BgL_ez00_1447));
																			}
																		}
																	else
																		{	/* Eval/eval.scm 355 */
																			BGl_errorzd2notifyzd2zz__errorz00
																				(BgL_ez00_1447);
																		}
																}
																{	/* Eval/eval.scm 358 */
																	BgL_z62errorz62_bglt BgL_i1087z00_1454;

																	{	/* Eval/eval.scm 359 */
																		bool_t BgL_test3079z00_6428;

																		{	/* Eval/eval.scm 359 */
																			obj_t BgL_classz00_4289;

																			BgL_classz00_4289 =
																				BGl_z62errorz62zz__objectz00;
																			if (BGL_OBJECTP(BgL_ez00_1447))
																				{	/* Eval/eval.scm 359 */
																					BgL_objectz00_bglt
																						BgL_arg2224z00_4291;
																					long BgL_arg2225z00_4292;

																					BgL_arg2224z00_4291 =
																						(BgL_objectz00_bglt)
																						(BgL_ez00_1447);
																					BgL_arg2225z00_4292 =
																						BGL_CLASS_DEPTH(BgL_classz00_4289);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Eval/eval.scm 359 */
																							long BgL_idxz00_4300;

																							BgL_idxz00_4300 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg2224z00_4291);
																							{	/* Eval/eval.scm 359 */
																								obj_t BgL_arg2215z00_4301;

																								{	/* Eval/eval.scm 359 */
																									long BgL_arg2216z00_4302;

																									BgL_arg2216z00_4302 =
																										(BgL_idxz00_4300 +
																										BgL_arg2225z00_4292);
																									BgL_arg2215z00_4301 =
																										VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																										BgL_arg2216z00_4302);
																								}
																								BgL_test3079z00_6428 =
																									(BgL_arg2215z00_4301 ==
																									BgL_classz00_4289);
																						}}
																					else
																						{	/* Eval/eval.scm 359 */
																							bool_t BgL_res2244z00_4307;

																							{	/* Eval/eval.scm 359 */
																								obj_t BgL_oclassz00_4311;

																								{	/* Eval/eval.scm 359 */
																									obj_t BgL_arg2229z00_4313;
																									long BgL_arg2230z00_4314;

																									BgL_arg2229z00_4313 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Eval/eval.scm 359 */
																										long BgL_arg2231z00_4315;

																										BgL_arg2231z00_4315 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg2224z00_4291);
																										BgL_arg2230z00_4314 =
																											(BgL_arg2231z00_4315 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_4311 =
																										VECTOR_REF
																										(BgL_arg2229z00_4313,
																										BgL_arg2230z00_4314);
																								}
																								{	/* Eval/eval.scm 359 */
																									bool_t
																										BgL__ortest_1236z00_4321;
																									BgL__ortest_1236z00_4321 =
																										(BgL_classz00_4289 ==
																										BgL_oclassz00_4311);
																									if (BgL__ortest_1236z00_4321)
																										{	/* Eval/eval.scm 359 */
																											BgL_res2244z00_4307 =
																												BgL__ortest_1236z00_4321;
																										}
																									else
																										{	/* Eval/eval.scm 359 */
																											long BgL_odepthz00_4322;

																											{	/* Eval/eval.scm 359 */
																												obj_t
																													BgL_arg2219z00_4323;
																												BgL_arg2219z00_4323 =
																													(BgL_oclassz00_4311);
																												BgL_odepthz00_4322 =
																													BGL_CLASS_DEPTH
																													(BgL_arg2219z00_4323);
																											}
																											if (
																												(BgL_arg2225z00_4292 <
																													BgL_odepthz00_4322))
																												{	/* Eval/eval.scm 359 */
																													obj_t
																														BgL_arg2217z00_4327;
																													{	/* Eval/eval.scm 359 */
																														obj_t
																															BgL_arg2218z00_4328;
																														BgL_arg2218z00_4328
																															=
																															(BgL_oclassz00_4311);
																														BgL_arg2217z00_4327
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg2218z00_4328,
																															BgL_arg2225z00_4292);
																													}
																													BgL_res2244z00_4307 =
																														(BgL_arg2217z00_4327
																														==
																														BgL_classz00_4289);
																												}
																											else
																												{	/* Eval/eval.scm 359 */
																													BgL_res2244z00_4307 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test3079z00_6428 =
																								BgL_res2244z00_4307;
																						}
																				}
																			else
																				{	/* Eval/eval.scm 359 */
																					BgL_test3079z00_6428 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test3079z00_6428)
																			{	/* Eval/eval.scm 359 */
																				BgL_i1087z00_1454 =
																					((BgL_z62errorz62_bglt)
																					BgL_ez00_1447);
																			}
																		else
																			{
																				obj_t BgL_auxz00_6453;

																				BgL_auxz00_6453 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(14421L),
																					BGl_string2657z00zz__evalz00,
																					BGl_string2658z00zz__evalz00,
																					BgL_ez00_1447);
																				FAILURE(BgL_auxz00_6453, BFALSE,
																					BFALSE);
																			}
																	}
																	{	/* Eval/eval.scm 359 */
																		bool_t BgL_test3084z00_6457;

																		{	/* Eval/eval.scm 359 */
																			obj_t BgL_tmpz00_6458;

																			BgL_tmpz00_6458 =
																				(((BgL_z62errorz62_bglt)
																					COBJECT(BgL_i1087z00_1454))->
																				BgL_objz00);
																			BgL_test3084z00_6457 =
																				EOF_OBJECTP(BgL_tmpz00_6458);
																		}
																		if (BgL_test3084z00_6457)
																			{	/* Eval/eval.scm 360 */
																				obj_t BgL_arg1464z00_1457;

																				{	/* Eval/eval.scm 360 */
																					obj_t BgL_tmpz00_6461;

																					BgL_tmpz00_6461 =
																						BGL_CURRENT_DYNAMIC_ENV();
																					BgL_arg1464z00_1457 =
																						BGL_ENV_CURRENT_INPUT_PORT
																						(BgL_tmpz00_6461);
																				}
																				reset_eof(BgL_arg1464z00_1457);
																			}
																		else
																			{	/* Eval/eval.scm 359 */
																				((bool_t) 0);
																			}
																	}
																}
																{	/* Eval/eval.scm 361 */
																	int BgL_tmpz00_6465;

																	BgL_tmpz00_6465 = (int) (0L);
																	BGL_SIGSETMASK(BgL_tmpz00_6465);
																}
																{

																	goto BgL_zc3z04anonymousza31445ze3z87_1414;
																}
															}
														}
													else
														{	/* Eval/eval.scm 353 */
															BgL_res1100z00_1405 = BgL_val1085z00_1418;
														}
												}
											}
										}
									}
								}
								POP_ENV_EXIT(BgL_env1101z00_1401);
								return BgL_res1100z00_1405;
							}
						}
					}
				}
		}

	}



/* &intrhdl */
	obj_t BGl_z62intrhdlz62zz__evalz00(obj_t BgL_envz00_3659, obj_t BgL_nz00_3661)
	{
		{	/* Eval/eval.scm 342 */
			{	/* Eval/eval.scm 343 */
				obj_t BgL_an_exitd1098z00_3660;

				BgL_an_exitd1098z00_3660 = PROCEDURE_REF(BgL_envz00_3659, (int) (0L));
				{	/* Eval/eval.scm 343 */
					int BgL_auxz00_6471;

					{	/* Eval/eval.scm 343 */
						obj_t BgL_tmpz00_6472;

						if (INTEGERP(BgL_nz00_3661))
							{	/* Eval/eval.scm 343 */
								BgL_tmpz00_6472 = BgL_nz00_3661;
							}
						else
							{
								obj_t BgL_auxz00_6475;

								BgL_auxz00_6475 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
									BINT(13935L), BGl_string2659z00zz__evalz00,
									BGl_string2635z00zz__evalz00, BgL_nz00_3661);
								FAILURE(BgL_auxz00_6475, BFALSE, BFALSE);
							}
						BgL_auxz00_6471 = CINT(BgL_tmpz00_6472);
					}
					BGl_notifyzd2interruptzd2zz__errorz00(BgL_auxz00_6471);
				}
				{	/* Eval/eval.scm 345 */
					obj_t BgL_arg1443z00_4329;

					{	/* Eval/eval.scm 345 */
						obj_t BgL_tmpz00_6481;

						BgL_tmpz00_6481 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1443z00_4329 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_6481);
					}
					reset_console(BgL_arg1443z00_4329);
				}
				{	/* Eval/eval.scm 347 */
					int BgL_tmpz00_6485;

					BgL_tmpz00_6485 = (int) (0L);
					BGL_SIGSETMASK(BgL_tmpz00_6485);
				}
				return
					unwind_stack_until(BgL_an_exitd1098z00_3660, BFALSE, BUNSPEC, BFALSE,
					BFALSE);
			}
		}

	}



/* &<@anonymous:1466> */
	obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__evalz00(obj_t BgL_envz00_3662)
	{
		{	/* Eval/eval.scm 338 */
			{	/* Eval/eval.scm 379 */
				obj_t BgL_oldzd2intrhdlzd2_3663;

				BgL_oldzd2intrhdlzd2_3663 = PROCEDURE_REF(BgL_envz00_3662, (int) (0L));
				if (PROCEDUREP(BgL_oldzd2intrhdlzd2_3663))
					{	/* Eval/eval.scm 379 */
						return BGl_signalz00zz__osz00(SIGINT, BgL_oldzd2intrhdlzd2_3663);
					}
				else
					{	/* Eval/eval.scm 379 */
						return BGl_signalz00zz__osz00(SIGINT, BGl_proc2660z00zz__evalz00);
					}
			}
		}

	}



/* &<@anonymous:1469> */
	obj_t BGl_z62zc3z04anonymousza31469ze3ze5zz__evalz00(obj_t BgL_envz00_3664,
		obj_t BgL_nz00_3665)
	{
		{	/* Eval/eval.scm 381 */
			{	/* Eval/eval.scm 381 */
				obj_t BgL_list1470z00_4330;

				BgL_list1470z00_4330 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
				return BGl_exitz00zz__errorz00(BgL_list1470z00_4330);
			}
		}

	}



/* &default-repl-printer */
	obj_t BGl_z62defaultzd2replzd2printerz62zz__evalz00(obj_t BgL_envz00_3617,
		obj_t BgL_expz00_3618, obj_t BgL_portz00_3619)
	{
		{	/* Eval/eval.scm 386 */
			{	/* Eval/eval.scm 387 */
				obj_t BgL_auxz00_6498;

				{	/* Eval/eval.scm 387 */
					obj_t BgL_list1474z00_4331;

					BgL_list1474z00_4331 = MAKE_YOUNG_PAIR(BgL_portz00_3619, BNIL);
					BgL_auxz00_6498 =
						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_expz00_3618,
						BgL_list1474z00_4331);
				}
				return
					apply(BGl_displayzd2circlezd2envz00zz__pp_circlez00, BgL_auxz00_6498);
			}
		}

	}



/* set-repl-printer! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2replzd2printerz12z12zz__evalz00(obj_t
		BgL_dispz00_32)
	{
		{	/* Eval/eval.scm 397 */
			if (PROCEDURE_CORRECT_ARITYP(BgL_dispz00_32, (int) (-2L)))
				{	/* Eval/eval.scm 400 */
					obj_t BgL_oldz00_2712;

					BgL_oldz00_2712 = BGl_za2replzd2printerza2zd2zz__evalz00;
					BGl_za2replzd2printerza2zd2zz__evalz00 = BgL_dispz00_32;
					return BgL_oldz00_2712;
				}
			else
				{	/* Eval/eval.scm 398 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2661z00zz__evalz00,
						BGl_string2663z00zz__evalz00, BgL_dispz00_32);
				}
		}

	}



/* &set-repl-printer! */
	obj_t BGl_z62setzd2replzd2printerz12z70zz__evalz00(obj_t BgL_envz00_3669,
		obj_t BgL_dispz00_3670)
	{
		{	/* Eval/eval.scm 397 */
			{	/* Eval/eval.scm 398 */
				obj_t BgL_auxz00_6506;

				if (PROCEDUREP(BgL_dispz00_3670))
					{	/* Eval/eval.scm 398 */
						BgL_auxz00_6506 = BgL_dispz00_3670;
					}
				else
					{
						obj_t BgL_auxz00_6509;

						BgL_auxz00_6509 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(15976L), BGl_string2664z00zz__evalz00,
							BGl_string2620z00zz__evalz00, BgL_dispz00_3670);
						FAILURE(BgL_auxz00_6509, BFALSE, BFALSE);
					}
				return BGl_setzd2replzd2printerz12z12zz__evalz00(BgL_auxz00_6506);
			}
		}

	}



/* native-repl-printer */
	BGL_EXPORTED_DEF obj_t BGl_nativezd2replzd2printerz00zz__evalz00(void)
	{
		{	/* Eval/eval.scm 407 */
			return BGl_defaultzd2replzd2printerzd2envzd2zz__evalz00;
		}

	}



/* &native-repl-printer */
	obj_t BGl_z62nativezd2replzd2printerz62zz__evalz00(obj_t BgL_envz00_3671)
	{
		{	/* Eval/eval.scm 407 */
			return BGl_nativezd2replzd2printerz00zz__evalz00();
		}

	}



/* c-debug-repl */
	BGL_EXPORTED_DEF obj_t bgl_debug_repl(obj_t BgL_valz00_33)
	{
		{	/* Eval/eval.scm 418 */
			BGl_za2czd2debugzd2replzd2valueza2zd2zz__evalz00 = BgL_valz00_33;
			{

				{	/* Eval/eval.scm 420 */
					bool_t BgL_tmpz00_6515;

				BgL_zc3z04anonymousza31475ze3z87_1477:
					{	/* Eval/eval.scm 421 */
						obj_t BgL_arg1476z00_1478;

						{	/* Eval/eval.scm 421 */
							obj_t BgL_tmpz00_6516;

							BgL_tmpz00_6516 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1476z00_1478 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6516);
						}
						bgl_display_string(BGl_string2665z00zz__evalz00,
							BgL_arg1476z00_1478);
					}
					{	/* Eval/eval.scm 422 */
						obj_t BgL_expz00_1479;

						{	/* Eval/eval.scm 422 */
							obj_t BgL_fun1480z00_1485;

							BgL_fun1480z00_1485 = BGl_getzd2evalzd2readerz00zz__evalz00();
							{	/* Eval/eval.scm 422 */
								obj_t BgL_arg1479z00_1486;

								{	/* Eval/eval.scm 422 */
									obj_t BgL_tmpz00_6521;

									BgL_tmpz00_6521 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1479z00_1486 =
										BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_6521);
								}
								{	/* Eval/eval.scm 422 */
									obj_t BgL_tmpfunz00_6527;

									{	/* Eval/eval.scm 422 */
										bool_t BgL_test2326z00_3842;

										BgL_test2326z00_3842 = PROCEDUREP(BgL_fun1480z00_1485);
										if (BgL_test2326z00_3842)
											{	/* Eval/eval.scm 422 */
												BgL_tmpfunz00_6527 = BgL_fun1480z00_1485;
											}
										else
											{
												obj_t BgL_auxz00_6530;

												BgL_auxz00_6530 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2618z00zz__evalz00, BINT(17046L),
													BGl_string2666z00zz__evalz00,
													BGl_string2620z00zz__evalz00, BgL_fun1480z00_1485);
												FAILURE(BgL_auxz00_6530, BFALSE, BFALSE);
											}
									}
									BgL_expz00_1479 =
										(VA_PROCEDUREP(BgL_tmpfunz00_6527) ? ((obj_t(*)(obj_t,
													...))
											PROCEDURE_ENTRY(BgL_tmpfunz00_6527)) (BgL_fun1480z00_1485,
											BgL_arg1479z00_1486, BEOA) : ((obj_t(*)(obj_t,
													obj_t))
											PROCEDURE_ENTRY(BgL_tmpfunz00_6527)) (BgL_fun1480z00_1485,
											BgL_arg1479z00_1486));
								}
							}
						}
						if (EOF_OBJECTP(BgL_expz00_1479))
							{	/* Eval/eval.scm 423 */
								BgL_tmpz00_6515 = ((bool_t) 0);
							}
						else
							{	/* Eval/eval.scm 423 */
								{	/* Eval/eval.scm 424 */
									obj_t BgL_port1245z00_1481;

									{	/* Eval/eval.scm 424 */
										obj_t BgL_tmpz00_6536;

										BgL_tmpz00_6536 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_port1245z00_1481 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6536);
									}
									{	/* Eval/eval.scm 424 */
										obj_t BgL_arg1478z00_1482;

										{	/* Eval/eval.scm 81 */
											obj_t BgL_envz00_1484;

											{	/* Eval/eval.scm 257 */
												obj_t BgL_mz00_2718;

												BgL_mz00_2718 = BGl_evalzd2modulezd2zz__evmodulez00();
												if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2718))
													{	/* Eval/eval.scm 258 */
														BgL_envz00_1484 = BgL_mz00_2718;
													}
												else
													{	/* Eval/eval.scm 258 */
														BgL_envz00_1484 = BGl_symbol2616z00zz__evalz00;
													}
											}
											{	/* Eval/eval.scm 81 */

												{	/* Eval/eval.scm 170 */
													obj_t BgL_auxz00_6542;

													{	/* Eval/eval.scm 170 */
														obj_t BgL_aux2327z00_3843;

														BgL_aux2327z00_3843 =
															BGl_defaultzd2evaluatezd2zz__evalz00;
														if (PROCEDUREP(BgL_aux2327z00_3843))
															{	/* Eval/eval.scm 170 */
																BgL_auxz00_6542 = BgL_aux2327z00_3843;
															}
														else
															{
																obj_t BgL_auxz00_6545;

																BgL_auxz00_6545 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2618z00zz__evalz00, BINT(6408L),
																	BGl_string2666z00zz__evalz00,
																	BGl_string2620z00zz__evalz00,
																	BgL_aux2327z00_3843);
																FAILURE(BgL_auxz00_6545, BFALSE, BFALSE);
															}
													}
													BgL_arg1478z00_1482 =
														BGl_evalzf2expanderzf2zz__evalz00(BgL_expz00_1479,
														BgL_envz00_1484, BGl_expandzd2envzd2zz__expandz00,
														BgL_auxz00_6542);
												}
											}
										}
										bgl_display_obj(BgL_arg1478z00_1482, BgL_port1245z00_1481);
									}
									bgl_display_char(((unsigned char) 10), BgL_port1245z00_1481);
								}
								goto BgL_zc3z04anonymousza31475ze3z87_1477;
							}
					}
					return BBOOL(BgL_tmpz00_6515);
				}
			}
		}

	}



/* &c-debug-repl */
	obj_t BGl_z62czd2debugzd2replz62zz__evalz00(obj_t BgL_envz00_3672,
		obj_t BgL_valz00_3673)
	{
		{	/* Eval/eval.scm 418 */
			return bgl_debug_repl(BgL_valz00_3673);
		}

	}



/* quit */
	BGL_EXPORTED_DEF obj_t BGl_quitz00zz__evalz00(void)
	{
		{	/* Eval/eval.scm 430 */
			{	/* Eval/eval.scm 431 */
				obj_t BgL_tmpfunz00_6558;

				{	/* Eval/eval.scm 431 */
					obj_t BgL_aux2329z00_3845;

					BgL_aux2329z00_3845 = BGl_za2replzd2quitza2zd2zz__evalz00;
					{	/* Eval/eval.scm 431 */
						bool_t BgL_test2330z00_3846;

						BgL_test2330z00_3846 = PROCEDUREP(BgL_aux2329z00_3845);
						if (BgL_test2330z00_3846)
							{	/* Eval/eval.scm 431 */
								BgL_tmpfunz00_6558 = BgL_aux2329z00_3845;
							}
						else
							{
								obj_t BgL_auxz00_6561;

								BgL_auxz00_6561 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
									BINT(17398L), BGl_string2667z00zz__evalz00,
									BGl_string2620z00zz__evalz00, BgL_aux2329z00_3845);
								FAILURE(BgL_auxz00_6561, BFALSE, BFALSE);
							}
					}
				}
				return
					(VA_PROCEDUREP(BgL_tmpfunz00_6558) ? ((obj_t(*)(obj_t,
								...))
						PROCEDURE_ENTRY(BgL_tmpfunz00_6558))
					(BGl_za2replzd2quitza2zd2zz__evalz00, BINT(0L),
						BEOA) : ((obj_t(*)(obj_t,
								obj_t))
						PROCEDURE_ENTRY(BgL_tmpfunz00_6558))
					(BGl_za2replzd2quitza2zd2zz__evalz00, BINT(0L)));
			}
		}

	}



/* &quit2252 */
	obj_t BGl_z62quit2252z62zz__evalz00(obj_t BgL_envz00_3674)
	{
		{	/* Eval/eval.scm 430 */
			return BGl_quitz00zz__evalz00();
		}

	}



/* find-file */
	obj_t BGl_findzd2filezd2zz__evalz00(obj_t BgL_namez00_34)
	{
		{	/* Eval/eval.scm 441 */
			if (STRINGP(BgL_namez00_34))
				{	/* Eval/eval.scm 442 */
					if (fexists(BSTRING_TO_STRING(BgL_namez00_34)))
						{	/* Eval/eval.scm 444 */
							return BgL_namez00_34;
						}
					else
						{
							obj_t BgL_pathz00_1491;

							BgL_pathz00_1491 = BGl_za2loadzd2pathza2zd2zz__evalz00;
						BgL_zc3z04anonymousza31483ze3z87_1492:
							if (NULLP(BgL_pathz00_1491))
								{	/* Eval/eval.scm 447 */
									return BgL_namez00_34;
								}
							else
								{	/* Eval/eval.scm 449 */
									obj_t BgL_fz00_1494;

									{	/* Eval/eval.scm 449 */
										obj_t BgL_arg1487z00_1497;

										{	/* Eval/eval.scm 449 */
											obj_t BgL_pairz00_2722;

											if (PAIRP(BgL_pathz00_1491))
												{	/* Eval/eval.scm 449 */
													BgL_pairz00_2722 = BgL_pathz00_1491;
												}
											else
												{
													obj_t BgL_auxz00_6575;

													BgL_auxz00_6575 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(18132L),
														BGl_string2666z00zz__evalz00,
														BGl_string2626z00zz__evalz00, BgL_pathz00_1491);
													FAILURE(BgL_auxz00_6575, BFALSE, BFALSE);
												}
											BgL_arg1487z00_1497 = CAR(BgL_pairz00_2722);
										}
										{	/* Eval/eval.scm 449 */
											obj_t BgL_auxz00_6580;

											if (STRINGP(BgL_arg1487z00_1497))
												{	/* Eval/eval.scm 449 */
													BgL_auxz00_6580 = BgL_arg1487z00_1497;
												}
											else
												{
													obj_t BgL_auxz00_6583;

													BgL_auxz00_6583 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(18136L),
														BGl_string2666z00zz__evalz00,
														BGl_string2633z00zz__evalz00, BgL_arg1487z00_1497);
													FAILURE(BgL_auxz00_6583, BFALSE, BFALSE);
												}
											BgL_fz00_1494 =
												BGl_makezd2filezd2namez00zz__osz00(BgL_auxz00_6580,
												BgL_namez00_34);
										}
									}
									if (fexists(BSTRING_TO_STRING(BgL_fz00_1494)))
										{	/* Eval/eval.scm 450 */
											return BgL_fz00_1494;
										}
									else
										{	/* Eval/eval.scm 452 */
											obj_t BgL_arg1486z00_1496;

											{	/* Eval/eval.scm 452 */
												obj_t BgL_pairz00_2724;

												if (PAIRP(BgL_pathz00_1491))
													{	/* Eval/eval.scm 452 */
														BgL_pairz00_2724 = BgL_pathz00_1491;
													}
												else
													{
														obj_t BgL_auxz00_6593;

														BgL_auxz00_6593 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2618z00zz__evalz00, BINT(18195L),
															BGl_string2666z00zz__evalz00,
															BGl_string2626z00zz__evalz00, BgL_pathz00_1491);
														FAILURE(BgL_auxz00_6593, BFALSE, BFALSE);
													}
												BgL_arg1486z00_1496 = CDR(BgL_pairz00_2724);
											}
											{
												obj_t BgL_pathz00_6598;

												BgL_pathz00_6598 = BgL_arg1486z00_1496;
												BgL_pathz00_1491 = BgL_pathz00_6598;
												goto BgL_zc3z04anonymousza31483ze3z87_1492;
											}
										}
								}
						}
				}
			else
				{	/* Eval/eval.scm 442 */
					return
						BGl_errorz00zz__errorz00(BGl_symbol2668z00zz__evalz00,
						BGl_string2670z00zz__evalz00, BgL_namez00_34);
				}
		}

	}



/* _load */
	obj_t BGl__loadz00zz__evalz00(obj_t BgL_env1292z00_38,
		obj_t BgL_opt1291z00_37)
	{
		{	/* Eval/eval.scm 462 */
			{	/* Eval/eval.scm 462 */
				obj_t BgL_zc3stringze3z20_1499;

				BgL_zc3stringze3z20_1499 = VECTOR_REF(BgL_opt1291z00_37, 0L);
				switch (VECTOR_LENGTH(BgL_opt1291z00_37))
					{
					case 1L:

						{	/* Eval/eval.scm 462 */
							obj_t BgL_envz00_1502;

							{	/* Eval/eval.scm 257 */
								obj_t BgL_mz00_2725;

								BgL_mz00_2725 = BGl_evalzd2modulezd2zz__evmodulez00();
								if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2725))
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1502 = BgL_mz00_2725;
									}
								else
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1502 = BGl_symbol2616z00zz__evalz00;
									}
							}
							{	/* Eval/eval.scm 462 */

								return
									BGl_loadvz00zz__evalz00(BgL_zc3stringze3z20_1499,
									BGl_za2loadzd2verboseza2zd2zz__evalz00, BgL_envz00_1502,
									BGl_symbol2671z00zz__evalz00);
							}
						}
						break;
					case 2L:

						{	/* Eval/eval.scm 462 */
							obj_t BgL_envz00_1503;

							BgL_envz00_1503 = VECTOR_REF(BgL_opt1291z00_37, 1L);
							{	/* Eval/eval.scm 462 */

								return
									BGl_loadvz00zz__evalz00(BgL_zc3stringze3z20_1499,
									BGl_za2loadzd2verboseza2zd2zz__evalz00, BgL_envz00_1503,
									BGl_symbol2671z00zz__evalz00);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* load */
	BGL_EXPORTED_DEF obj_t BGl_loadz00zz__evalz00(obj_t BgL_filezd2namezd2_35,
		obj_t BgL_envz00_36)
	{
		{	/* Eval/eval.scm 462 */
			return
				BGl_loadvz00zz__evalz00(BgL_filezd2namezd2_35,
				BGl_za2loadzd2verboseza2zd2zz__evalz00, BgL_envz00_36,
				BGl_symbol2671z00zz__evalz00);
		}

	}



/* _loadq */
	obj_t BGl__loadqz00zz__evalz00(obj_t BgL_env1296z00_42,
		obj_t BgL_opt1295z00_41)
	{
		{	/* Eval/eval.scm 468 */
			{	/* Eval/eval.scm 468 */
				obj_t BgL_zc3stringze3z20_1504;

				BgL_zc3stringze3z20_1504 = VECTOR_REF(BgL_opt1295z00_41, 0L);
				switch (VECTOR_LENGTH(BgL_opt1295z00_41))
					{
					case 1L:

						{	/* Eval/eval.scm 468 */
							obj_t BgL_envz00_1507;

							{	/* Eval/eval.scm 257 */
								obj_t BgL_mz00_2727;

								BgL_mz00_2727 = BGl_evalzd2modulezd2zz__evmodulez00();
								if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_2727))
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1507 = BgL_mz00_2727;
									}
								else
									{	/* Eval/eval.scm 258 */
										BgL_envz00_1507 = BGl_symbol2616z00zz__evalz00;
									}
							}
							{	/* Eval/eval.scm 468 */

								return
									BGl_loadvz00zz__evalz00(BgL_zc3stringze3z20_1504, BFALSE,
									BgL_envz00_1507, BGl_symbol2673z00zz__evalz00);
							}
						}
						break;
					case 2L:

						{	/* Eval/eval.scm 468 */
							obj_t BgL_envz00_1508;

							BgL_envz00_1508 = VECTOR_REF(BgL_opt1295z00_41, 1L);
							{	/* Eval/eval.scm 468 */

								return
									BGl_loadvz00zz__evalz00(BgL_zc3stringze3z20_1504, BFALSE,
									BgL_envz00_1508, BGl_symbol2673z00zz__evalz00);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* loadq */
	BGL_EXPORTED_DEF obj_t BGl_loadqz00zz__evalz00(obj_t BgL_filezd2namezd2_39,
		obj_t BgL_envz00_40)
	{
		{	/* Eval/eval.scm 468 */
			return
				BGl_loadvz00zz__evalz00(BgL_filezd2namezd2_39, BFALSE, BgL_envz00_40,
				BGl_symbol2673z00zz__evalz00);
		}

	}



/* loadv */
	obj_t BGl_loadvz00zz__evalz00(obj_t BgL_filenamez00_43, obj_t BgL_vzf3zf3_44,
		obj_t BgL_envz00_45, obj_t BgL_traceidz00_46)
	{
		{	/* Eval/eval.scm 477 */
			{	/* Eval/eval.scm 485 */
				obj_t BgL_pathz00_1510;

				BgL_pathz00_1510 = BGl_findzd2filezd2zz__evalz00(BgL_filenamez00_43);
				{	/* Eval/eval.scm 485 */
					obj_t BgL_portz00_1511;

					{	/* Ieee/port.scm 466 */

						{	/* Ieee/port.scm 466 */
							obj_t BgL_auxz00_6621;

							if (STRINGP(BgL_pathz00_1510))
								{	/* Eval/eval.scm 486 */
									BgL_auxz00_6621 = BgL_pathz00_1510;
								}
							else
								{
									obj_t BgL_auxz00_6624;

									BgL_auxz00_6624 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string2618z00zz__evalz00, BINT(19798L),
										BGl_string2675z00zz__evalz00, BGl_string2633z00zz__evalz00,
										BgL_pathz00_1510);
									FAILURE(BgL_auxz00_6624, BFALSE, BFALSE);
								}
							BgL_portz00_1511 =
								BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_auxz00_6621, BTRUE, BINT(5000000L));
						}
					}
					{	/* Eval/eval.scm 486 */
						obj_t BgL_evreadz00_1512;

						BgL_evreadz00_1512 = BGl_getzd2evalzd2readerz00zz__evalz00();
						{	/* Eval/eval.scm 487 */
							obj_t BgL_denvz00_1513;

							BgL_denvz00_1513 = BGL_CURRENT_DYNAMIC_ENV();
							{	/* Eval/eval.scm 488 */
								obj_t BgL_modz00_1514;

								BgL_modz00_1514 = BGL_MODULE();
								{	/* Eval/eval.scm 489 */

									if (INPUT_PORTP(BgL_portz00_1511))
										{	/* Eval/eval.scm 491 */
											obj_t BgL_exitd1105z00_1516;

											BgL_exitd1105z00_1516 = BGL_EXITD_TOP_AS_OBJ();
											{	/* Eval/eval.scm 539 */
												obj_t BgL_zc3z04anonymousza31518ze3z87_3675;

												BgL_zc3z04anonymousza31518ze3z87_3675 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31518ze3ze5zz__evalz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31518ze3z87_3675,
													(int) (0L), BgL_modz00_1514);
												{	/* Eval/eval.scm 491 */
													obj_t BgL_arg2211z00_2739;

													{	/* Eval/eval.scm 491 */
														obj_t BgL_arg2212z00_2740;

														BgL_arg2212z00_2740 =
															BGL_EXITD_PROTECT(BgL_exitd1105z00_1516);
														BgL_arg2211z00_2739 =
															MAKE_YOUNG_PAIR
															(BgL_zc3z04anonymousza31518ze3z87_3675,
															BgL_arg2212z00_2740);
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1105z00_1516,
														BgL_arg2211z00_2739);
													BUNSPEC;
												}
												{	/* Eval/eval.scm 492 */
													obj_t BgL_tmp1107z00_1518;

													{	/* Eval/eval.scm 492 */

														BGL_ENV_PUSH_TRACE(BgL_denvz00_1513,
															BgL_traceidz00_46, BFALSE);
														{	/* Eval/eval.scm 494 */
															obj_t BgL_sexpz00_1519;
															obj_t BgL_locz00_1520;
															obj_t BgL_mainsymz00_1521;
															obj_t BgL_envz00_1522;

															{	/* Eval/eval.scm 494 */
																obj_t BgL_tmpfunz00_6648;

																{	/* Eval/eval.scm 494 */
																	bool_t BgL_test2342z00_3858;

																	BgL_test2342z00_3858 =
																		PROCEDUREP(BgL_evreadz00_1512);
																	if (BgL_test2342z00_3858)
																		{	/* Eval/eval.scm 494 */
																			BgL_tmpfunz00_6648 = BgL_evreadz00_1512;
																		}
																	else
																		{
																			obj_t BgL_auxz00_6651;

																			BgL_auxz00_6651 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(20004L),
																				BGl_string2675z00zz__evalz00,
																				BGl_string2620z00zz__evalz00,
																				BgL_evreadz00_1512);
																			FAILURE(BgL_auxz00_6651, BFALSE, BFALSE);
																		}
																}
																BgL_sexpz00_1519 =
																	(VA_PROCEDUREP(BgL_tmpfunz00_6648)
																	? ((obj_t(*)(obj_t,
																				...))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_6648))
																	(BgL_evreadz00_1512, BgL_portz00_1511,
																		BEOA) : ((obj_t(*)(obj_t,
																				obj_t))
																		PROCEDURE_ENTRY(BgL_tmpfunz00_6648))
																	(BgL_evreadz00_1512, BgL_portz00_1511));
															}
															BgL_locz00_1520 = BFALSE;
															BgL_mainsymz00_1521 = BFALSE;
															BgL_envz00_1522 = BgL_envz00_45;
															if (EPAIRP(BgL_sexpz00_1519))
																{	/* Eval/eval.scm 499 */
																	obj_t BgL_arg1490z00_1524;

																	{	/* Eval/eval.scm 499 */
																		obj_t BgL_objz00_2741;

																		if (EPAIRP(BgL_sexpz00_1519))
																			{	/* Eval/eval.scm 499 */
																				BgL_objz00_2741 = BgL_sexpz00_1519;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6659;

																				BgL_auxz00_6659 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(20144L),
																					BGl_string2675z00zz__evalz00,
																					BGl_string2676z00zz__evalz00,
																					BgL_sexpz00_1519);
																				FAILURE(BgL_auxz00_6659, BFALSE,
																					BFALSE);
																			}
																		BgL_arg1490z00_1524 = CER(BgL_objz00_2741);
																	}
																	BGL_ENV_SET_TRACE_LOCATION(BgL_denvz00_1513,
																		BgL_arg1490z00_1524);
																}
															else
																{	/* Eval/eval.scm 498 */
																	BFALSE;
																}
															{	/* Eval/eval.scm 501 */
																bool_t BgL_test3108z00_6665;

																if (PAIRP(BgL_sexpz00_1519))
																	{	/* Eval/eval.scm 501 */
																		BgL_test3108z00_6665 =
																			(CAR(BgL_sexpz00_1519) ==
																			BGl_symbol2677z00zz__evalz00);
																	}
																else
																	{	/* Eval/eval.scm 501 */
																		BgL_test3108z00_6665 = ((bool_t) 0);
																	}
																if (BgL_test3108z00_6665)
																	{	/* Eval/eval.scm 503 */
																		obj_t BgL_clausez00_1528;

																		{	/* Eval/eval.scm 503 */
																			obj_t BgL_auxz00_6670;

																			{	/* Eval/eval.scm 503 */
																				obj_t BgL_pairz00_2746;

																				{	/* Eval/eval.scm 503 */
																					obj_t BgL_aux2345z00_3861;

																					BgL_aux2345z00_3861 =
																						CDR(BgL_sexpz00_1519);
																					if (PAIRP(BgL_aux2345z00_3861))
																						{	/* Eval/eval.scm 503 */
																							BgL_pairz00_2746 =
																								BgL_aux2345z00_3861;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6674;

																							BgL_auxz00_6674 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(20340L),
																								BGl_string2675z00zz__evalz00,
																								BGl_string2626z00zz__evalz00,
																								BgL_aux2345z00_3861);
																							FAILURE(BgL_auxz00_6674, BFALSE,
																								BFALSE);
																						}
																				}
																				{	/* Eval/eval.scm 503 */
																					obj_t BgL_aux2347z00_3863;

																					BgL_aux2347z00_3863 =
																						CDR(BgL_pairz00_2746);
																					{	/* Eval/eval.scm 503 */
																						bool_t BgL_test3111z00_6679;

																						if (PAIRP(BgL_aux2347z00_3863))
																							{	/* Eval/eval.scm 503 */
																								BgL_test3111z00_6679 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Eval/eval.scm 503 */
																								BgL_test3111z00_6679 =
																									NULLP(BgL_aux2347z00_3863);
																							}
																						if (BgL_test3111z00_6679)
																							{	/* Eval/eval.scm 503 */
																								BgL_auxz00_6670 =
																									BgL_aux2347z00_3863;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6683;

																								BgL_auxz00_6683 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(20340L),
																									BGl_string2675z00zz__evalz00,
																									BGl_string2681z00zz__evalz00,
																									BgL_aux2347z00_3863);
																								FAILURE(BgL_auxz00_6683, BFALSE,
																									BFALSE);
																							}
																					}
																				}
																			}
																			BgL_clausez00_1528 =
																				BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																				(BGl_symbol2679z00zz__evalz00,
																				BgL_auxz00_6670);
																		}
																		BgL_locz00_1520 =
																			BGl_getzd2sourcezd2locationz00zz__readerz00
																			(BgL_sexpz00_1519);
																		if (PAIRP(BgL_clausez00_1528))
																			{	/* Eval/eval.scm 506 */
																				bool_t BgL_test3114z00_6691;

																				{	/* Eval/eval.scm 506 */
																					bool_t BgL_test3115z00_6692;

																					{	/* Eval/eval.scm 506 */
																						obj_t BgL_tmpz00_6693;

																						BgL_tmpz00_6693 =
																							CDR(BgL_clausez00_1528);
																						BgL_test3115z00_6692 =
																							PAIRP(BgL_tmpz00_6693);
																					}
																					if (BgL_test3115z00_6692)
																						{	/* Eval/eval.scm 507 */
																							bool_t BgL_test3116z00_6696;

																							{	/* Eval/eval.scm 507 */
																								obj_t BgL_tmpz00_6697;

																								{	/* Eval/eval.scm 507 */
																									obj_t BgL_pairz00_2751;

																									{	/* Eval/eval.scm 507 */
																										obj_t BgL_aux2349z00_3865;

																										BgL_aux2349z00_3865 =
																											CDR(BgL_clausez00_1528);
																										if (PAIRP
																											(BgL_aux2349z00_3865))
																											{	/* Eval/eval.scm 507 */
																												BgL_pairz00_2751 =
																													BgL_aux2349z00_3865;
																											}
																										else
																											{
																												obj_t BgL_auxz00_6701;

																												BgL_auxz00_6701 =
																													BGl_typezd2errorzd2zz__errorz00
																													(BGl_string2618z00zz__evalz00,
																													BINT(20479L),
																													BGl_string2675z00zz__evalz00,
																													BGl_string2626z00zz__evalz00,
																													BgL_aux2349z00_3865);
																												FAILURE(BgL_auxz00_6701,
																													BFALSE, BFALSE);
																											}
																									}
																									BgL_tmpz00_6697 =
																										CDR(BgL_pairz00_2751);
																								}
																								BgL_test3116z00_6696 =
																									NULLP(BgL_tmpz00_6697);
																							}
																							if (BgL_test3116z00_6696)
																								{	/* Eval/eval.scm 508 */
																									obj_t BgL_tmpz00_6707;

																									{	/* Eval/eval.scm 508 */
																										obj_t BgL_pairz00_2755;

																										{	/* Eval/eval.scm 508 */
																											obj_t BgL_aux2351z00_3867;

																											BgL_aux2351z00_3867 =
																												CDR(BgL_clausez00_1528);
																											if (PAIRP
																												(BgL_aux2351z00_3867))
																												{	/* Eval/eval.scm 508 */
																													BgL_pairz00_2755 =
																														BgL_aux2351z00_3867;
																												}
																											else
																												{
																													obj_t BgL_auxz00_6711;

																													BgL_auxz00_6711 =
																														BGl_typezd2errorzd2zz__errorz00
																														(BGl_string2618z00zz__evalz00,
																														BINT(20514L),
																														BGl_string2675z00zz__evalz00,
																														BGl_string2626z00zz__evalz00,
																														BgL_aux2351z00_3867);
																													FAILURE
																														(BgL_auxz00_6711,
																														BFALSE, BFALSE);
																												}
																										}
																										BgL_tmpz00_6707 =
																											CAR(BgL_pairz00_2755);
																									}
																									BgL_test3114z00_6691 =
																										SYMBOLP(BgL_tmpz00_6707);
																								}
																							else
																								{	/* Eval/eval.scm 507 */
																									BgL_test3114z00_6691 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Eval/eval.scm 506 */
																							BgL_test3114z00_6691 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test3114z00_6691)
																					{	/* Eval/eval.scm 509 */
																						obj_t BgL_pairz00_2759;

																						{	/* Eval/eval.scm 509 */
																							obj_t BgL_aux2353z00_3869;

																							BgL_aux2353z00_3869 =
																								CDR(BgL_clausez00_1528);
																							if (PAIRP(BgL_aux2353z00_3869))
																								{	/* Eval/eval.scm 509 */
																									BgL_pairz00_2759 =
																										BgL_aux2353z00_3869;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6720;

																									BgL_auxz00_6720 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(20550L),
																										BGl_string2675z00zz__evalz00,
																										BGl_string2626z00zz__evalz00,
																										BgL_aux2353z00_3869);
																									FAILURE(BgL_auxz00_6720,
																										BFALSE, BFALSE);
																								}
																						}
																						BgL_mainsymz00_1521 =
																							CAR(BgL_pairz00_2759);
																					}
																				else
																					{	/* Eval/eval.scm 506 */
																						BGl_evcompilezd2errorzd2zz__evcompilez00
																							(BGl_getzd2sourcezd2locationz00zz__readerz00
																							(BgL_sexpz00_1519),
																							BGl_string2672z00zz__evalz00,
																							BGl_string2682z00zz__evalz00,
																							BgL_clausez00_1528);
																					}
																			}
																		else
																			{	/* Eval/eval.scm 505 */
																				BFALSE;
																			}
																		{	/* Eval/eval.scm 515 */
																			obj_t BgL_envz00_2760;

																			BgL_envz00_2760 = BgL_envz00_1522;
																			{	/* Eval/eval.scm 480 */
																				obj_t BgL_vz00_2761;

																				{	/* Eval/eval.scm 176 */
																					obj_t BgL_evaluatez00_2765;

																					if (PROCEDUREP
																						(BGl_defaultzd2evaluatezd2zz__evalz00))
																						{	/* Eval/eval.scm 176 */
																							BgL_evaluatez00_2765 =
																								BGl_defaultzd2evaluatezd2zz__evalz00;
																						}
																					else
																						{	/* Eval/eval.scm 176 */
																							BgL_evaluatez00_2765 =
																								BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																						}
																					{	/* Eval/eval.scm 179 */
																						obj_t BgL_auxz00_6729;

																						if (PROCEDUREP
																							(BgL_evaluatez00_2765))
																							{	/* Eval/eval.scm 179 */
																								BgL_auxz00_6729 =
																									BgL_evaluatez00_2765;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6732;

																								BgL_auxz00_6732 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(6857L),
																									BGl_string2675z00zz__evalz00,
																									BGl_string2620z00zz__evalz00,
																									BgL_evaluatez00_2765);
																								FAILURE(BgL_auxz00_6732, BFALSE,
																									BFALSE);
																							}
																						BgL_vz00_2761 =
																							BGl_evalzf2expanderzf2zz__evalz00
																							(BgL_sexpz00_1519,
																							BgL_envz00_2760,
																							BGl_expandz12zd2envzc0zz__expandz00,
																							BgL_auxz00_6729);
																					}
																				}
																				if (CBOOL(BgL_vzf3zf3_44))
																					{	/* Eval/eval.scm 481 */
																						{	/* Eval/eval.scm 482 */
																							obj_t BgL_portz00_2763;

																							{	/* Eval/eval.scm 482 */
																								obj_t BgL_tmpz00_6739;

																								BgL_tmpz00_6739 =
																									BGL_CURRENT_DYNAMIC_ENV();
																								BgL_portz00_2763 =
																									BGL_ENV_CURRENT_OUTPUT_PORT
																									(BgL_tmpz00_6739);
																							}
																							{	/* Eval/eval.scm 482 */

																								BGl_displayzd2circlezd2zz__pp_circlez00
																									(BgL_vz00_2761,
																									BgL_portz00_2763);
																							}
																						}
																						{	/* Eval/eval.scm 483 */
																							obj_t BgL_arg1522z00_2764;

																							{	/* Eval/eval.scm 483 */
																								obj_t BgL_tmpz00_6743;

																								BgL_tmpz00_6743 =
																									BGL_CURRENT_DYNAMIC_ENV();
																								BgL_arg1522z00_2764 =
																									BGL_ENV_CURRENT_OUTPUT_PORT
																									(BgL_tmpz00_6743);
																							}
																							bgl_display_char(((unsigned char)
																									10), BgL_arg1522z00_2764);
																					}}
																				else
																					{	/* Eval/eval.scm 481 */
																						BFALSE;
																					}
																			}
																		}
																		BgL_envz00_1522 = BGL_MODULE();
																	}
																else
																	{	/* Eval/eval.scm 517 */
																		obj_t BgL_envz00_2771;

																		BgL_envz00_2771 = BgL_envz00_1522;
																		{	/* Eval/eval.scm 480 */
																			obj_t BgL_vz00_2772;

																			{	/* Eval/eval.scm 176 */
																				obj_t BgL_evaluatez00_2776;

																				if (PROCEDUREP
																					(BGl_defaultzd2evaluatezd2zz__evalz00))
																					{	/* Eval/eval.scm 176 */
																						BgL_evaluatez00_2776 =
																							BGl_defaultzd2evaluatezd2zz__evalz00;
																					}
																				else
																					{	/* Eval/eval.scm 176 */
																						BgL_evaluatez00_2776 =
																							BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																					}
																				{	/* Eval/eval.scm 179 */
																					obj_t BgL_auxz00_6750;

																					if (PROCEDUREP(BgL_evaluatez00_2776))
																						{	/* Eval/eval.scm 179 */
																							BgL_auxz00_6750 =
																								BgL_evaluatez00_2776;
																						}
																					else
																						{
																							obj_t BgL_auxz00_6753;

																							BgL_auxz00_6753 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(6857L),
																								BGl_string2675z00zz__evalz00,
																								BGl_string2620z00zz__evalz00,
																								BgL_evaluatez00_2776);
																							FAILURE(BgL_auxz00_6753, BFALSE,
																								BFALSE);
																						}
																					BgL_vz00_2772 =
																						BGl_evalzf2expanderzf2zz__evalz00
																						(BgL_sexpz00_1519, BgL_envz00_2771,
																						BGl_expandz12zd2envzc0zz__expandz00,
																						BgL_auxz00_6750);
																				}
																			}
																			if (CBOOL(BgL_vzf3zf3_44))
																				{	/* Eval/eval.scm 481 */
																					{	/* Eval/eval.scm 482 */
																						obj_t BgL_portz00_2774;

																						{	/* Eval/eval.scm 482 */
																							obj_t BgL_tmpz00_6760;

																							BgL_tmpz00_6760 =
																								BGL_CURRENT_DYNAMIC_ENV();
																							BgL_portz00_2774 =
																								BGL_ENV_CURRENT_OUTPUT_PORT
																								(BgL_tmpz00_6760);
																						}
																						{	/* Eval/eval.scm 482 */

																							BGl_displayzd2circlezd2zz__pp_circlez00
																								(BgL_vz00_2772,
																								BgL_portz00_2774);
																						}
																					}
																					{	/* Eval/eval.scm 483 */
																						obj_t BgL_arg1522z00_2775;

																						{	/* Eval/eval.scm 483 */
																							obj_t BgL_tmpz00_6764;

																							BgL_tmpz00_6764 =
																								BGL_CURRENT_DYNAMIC_ENV();
																							BgL_arg1522z00_2775 =
																								BGL_ENV_CURRENT_OUTPUT_PORT
																								(BgL_tmpz00_6764);
																						}
																						bgl_display_char(((unsigned char)
																								10), BgL_arg1522z00_2775);
																				}}
																			else
																				{	/* Eval/eval.scm 481 */
																					BFALSE;
																				}
																		}
																	}
															}
															{	/* Eval/eval.scm 518 */
																obj_t BgL_g1109z00_1547;

																{	/* Eval/eval.scm 518 */
																	obj_t BgL_tmpfunz00_6771;

																	{	/* Eval/eval.scm 518 */
																		bool_t BgL_test2360z00_3876;

																		BgL_test2360z00_3876 =
																			PROCEDUREP(BgL_evreadz00_1512);
																		if (BgL_test2360z00_3876)
																			{	/* Eval/eval.scm 518 */
																				BgL_tmpfunz00_6771 = BgL_evreadz00_1512;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6774;

																				BgL_auxz00_6774 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(20818L),
																					BGl_string2675z00zz__evalz00,
																					BGl_string2620z00zz__evalz00,
																					BgL_evreadz00_1512);
																				FAILURE(BgL_auxz00_6774, BFALSE,
																					BFALSE);
																			}
																	}
																	BgL_g1109z00_1547 =
																		(VA_PROCEDUREP(BgL_tmpfunz00_6771)
																		? ((obj_t(*)(obj_t,
																					...))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_6771))
																		(BgL_evreadz00_1512, BgL_portz00_1511,
																			BEOA) : ((obj_t(*)(obj_t,
																					obj_t))
																			PROCEDURE_ENTRY(BgL_tmpfunz00_6771))
																		(BgL_evreadz00_1512, BgL_portz00_1511));
																}
																{
																	obj_t BgL_sexpz00_1549;

																	BgL_sexpz00_1549 = BgL_g1109z00_1547;
																BgL_zc3z04anonymousza31509ze3z87_1550:
																	if (EOF_OBJECTP(BgL_sexpz00_1549))
																		{	/* Eval/eval.scm 520 */
																			bgl_close_input_port(
																				((obj_t) BgL_portz00_1511));
																			{	/* Eval/eval.scm 522 */
																				obj_t BgL_vz00_1552;

																				if (SYMBOLP(BgL_mainsymz00_1521))
																					{	/* Eval/eval.scm 523 */
																						obj_t BgL_iexpz00_1556;

																						{	/* Eval/eval.scm 524 */
																							obj_t BgL_arg1513z00_1557;

																							{	/* Eval/eval.scm 524 */
																								obj_t BgL_list1514z00_1558;

																								BgL_list1514z00_1558 =
																									MAKE_YOUNG_PAIR
																									(BGl_list2683z00zz__evalz00,
																									BNIL);
																								BgL_arg1513z00_1557 =
																									BgL_list1514z00_1558;
																							}
																							{	/* Eval/eval.scm 523 */
																								obj_t BgL_res2238z00_2787;

																								BgL_res2238z00_2787 =
																									MAKE_YOUNG_EPAIR
																									(BgL_mainsymz00_1521,
																									BgL_arg1513z00_1557,
																									BgL_locz00_1520);
																								BgL_iexpz00_1556 =
																									BgL_res2238z00_2787;
																							}
																						}
																						{	/* Eval/eval.scm 526 */
																							obj_t BgL_envz00_2788;

																							BgL_envz00_2788 = BgL_envz00_1522;
																							{	/* Eval/eval.scm 176 */
																								obj_t BgL_evaluatez00_2789;

																								if (PROCEDUREP
																									(BGl_defaultzd2evaluatezd2zz__evalz00))
																									{	/* Eval/eval.scm 176 */
																										BgL_evaluatez00_2789 =
																											BGl_defaultzd2evaluatezd2zz__evalz00;
																									}
																								else
																									{	/* Eval/eval.scm 176 */
																										BgL_evaluatez00_2789 =
																											BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																									}
																								{	/* Eval/eval.scm 179 */
																									obj_t BgL_auxz00_6788;

																									if (PROCEDUREP
																										(BgL_evaluatez00_2789))
																										{	/* Eval/eval.scm 179 */
																											BgL_auxz00_6788 =
																												BgL_evaluatez00_2789;
																										}
																									else
																										{
																											obj_t BgL_auxz00_6791;

																											BgL_auxz00_6791 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string2618z00zz__evalz00,
																												BINT(6857L),
																												BGl_string2666z00zz__evalz00,
																												BGl_string2620z00zz__evalz00,
																												BgL_evaluatez00_2789);
																											FAILURE(BgL_auxz00_6791,
																												BFALSE, BFALSE);
																										}
																									BgL_vz00_1552 =
																										BGl_evalzf2expanderzf2zz__evalz00
																										(BgL_iexpz00_1556,
																										BgL_envz00_2788,
																										BGl_expandz12zd2envzc0zz__expandz00,
																										BgL_auxz00_6788);
																								}
																							}
																						}
																					}
																				else
																					{	/* Eval/eval.scm 522 */
																						BgL_vz00_1552 = BINT(0L);
																					}
																				{	/* Eval/eval.scm 522 */
																					obj_t BgL_nenvz00_1553;

																					BgL_nenvz00_1553 = BGL_MODULE();
																					{	/* Eval/eval.scm 528 */

																						{	/* Eval/eval.scm 529 */
																							bool_t BgL_test3131z00_6798;

																							if (
																								(BgL_envz00_1522 ==
																									BgL_nenvz00_1553))
																								{	/* Eval/eval.scm 529 */
																									BgL_test3131z00_6798 =
																										((bool_t) 0);
																								}
																							else
																								{	/* Eval/eval.scm 529 */
																									BgL_test3131z00_6798 =
																										BGl_evmodulezf3zf3zz__evmodulez00
																										(BgL_envz00_1522);
																								}
																							if (BgL_test3131z00_6798)
																								{	/* Eval/eval.scm 529 */
																									BGl_evmodulezd2checkzd2unboundz00zz__evmodulez00
																										(BgL_envz00_1522, BFALSE);
																									BgL_envz00_1522 =
																										BgL_nenvz00_1553;
																								}
																							else
																								{	/* Eval/eval.scm 529 */
																									BFALSE;
																								}
																						}
																						BGL_ENV_POP_TRACE(BgL_denvz00_1513);
																						BgL_tmp1107z00_1518 =
																							BgL_pathz00_1510;
																					}
																				}
																			}
																		}
																	else
																		{	/* Eval/eval.scm 520 */
																			if (EPAIRP(BgL_sexpz00_1549))
																				{	/* Eval/eval.scm 536 */
																					obj_t BgL_arg1516z00_1560;

																					{	/* Eval/eval.scm 536 */
																						obj_t BgL_objz00_2792;

																						if (EPAIRP(BgL_sexpz00_1549))
																							{	/* Eval/eval.scm 536 */
																								BgL_objz00_2792 =
																									BgL_sexpz00_1549;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6808;

																								BgL_auxz00_6808 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(21326L),
																									BGl_string2666z00zz__evalz00,
																									BGl_string2676z00zz__evalz00,
																									BgL_sexpz00_1549);
																								FAILURE(BgL_auxz00_6808, BFALSE,
																									BFALSE);
																							}
																						BgL_arg1516z00_1560 =
																							CER(BgL_objz00_2792);
																					}
																					BGL_ENV_SET_TRACE_LOCATION
																						(BgL_denvz00_1513,
																						BgL_arg1516z00_1560);
																				}
																			else
																				{	/* Eval/eval.scm 535 */
																					BFALSE;
																				}
																			{	/* Eval/eval.scm 537 */
																				obj_t BgL_envz00_2793;

																				BgL_envz00_2793 = BgL_envz00_1522;
																				{	/* Eval/eval.scm 480 */
																					obj_t BgL_vz00_2794;

																					{	/* Eval/eval.scm 176 */
																						obj_t BgL_evaluatez00_2798;

																						if (PROCEDUREP
																							(BGl_defaultzd2evaluatezd2zz__evalz00))
																							{	/* Eval/eval.scm 176 */
																								BgL_evaluatez00_2798 =
																									BGl_defaultzd2evaluatezd2zz__evalz00;
																							}
																						else
																							{	/* Eval/eval.scm 176 */
																								BgL_evaluatez00_2798 =
																									BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																							}
																						{	/* Eval/eval.scm 179 */
																							obj_t BgL_auxz00_6816;

																							if (PROCEDUREP
																								(BgL_evaluatez00_2798))
																								{	/* Eval/eval.scm 179 */
																									BgL_auxz00_6816 =
																										BgL_evaluatez00_2798;
																								}
																							else
																								{
																									obj_t BgL_auxz00_6819;

																									BgL_auxz00_6819 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(6857L),
																										BGl_string2666z00zz__evalz00,
																										BGl_string2620z00zz__evalz00,
																										BgL_evaluatez00_2798);
																									FAILURE(BgL_auxz00_6819,
																										BFALSE, BFALSE);
																								}
																							BgL_vz00_2794 =
																								BGl_evalzf2expanderzf2zz__evalz00
																								(BgL_sexpz00_1549,
																								BgL_envz00_2793,
																								BGl_expandz12zd2envzc0zz__expandz00,
																								BgL_auxz00_6816);
																						}
																					}
																					if (CBOOL(BgL_vzf3zf3_44))
																						{	/* Eval/eval.scm 481 */
																							{	/* Eval/eval.scm 482 */
																								obj_t BgL_portz00_2796;

																								{	/* Eval/eval.scm 482 */
																									obj_t BgL_tmpz00_6826;

																									BgL_tmpz00_6826 =
																										BGL_CURRENT_DYNAMIC_ENV();
																									BgL_portz00_2796 =
																										BGL_ENV_CURRENT_OUTPUT_PORT
																										(BgL_tmpz00_6826);
																								}
																								{	/* Eval/eval.scm 482 */

																									BGl_displayzd2circlezd2zz__pp_circlez00
																										(BgL_vz00_2794,
																										BgL_portz00_2796);
																								}
																							}
																							{	/* Eval/eval.scm 483 */
																								obj_t BgL_arg1522z00_2797;

																								{	/* Eval/eval.scm 483 */
																									obj_t BgL_tmpz00_6830;

																									BgL_tmpz00_6830 =
																										BGL_CURRENT_DYNAMIC_ENV();
																									BgL_arg1522z00_2797 =
																										BGL_ENV_CURRENT_OUTPUT_PORT
																										(BgL_tmpz00_6830);
																								}
																								bgl_display_char(((unsigned
																											char) 10),
																									BgL_arg1522z00_2797);
																						}}
																					else
																						{	/* Eval/eval.scm 481 */
																							BFALSE;
																						}
																				}
																			}
																			{	/* Eval/eval.scm 538 */
																				obj_t BgL_arg1517z00_1561;

																				{	/* Eval/eval.scm 538 */
																					obj_t BgL_tmpfunz00_6837;

																					{	/* Eval/eval.scm 538 */
																						bool_t BgL_test2370z00_3886;

																						BgL_test2370z00_3886 =
																							PROCEDUREP(BgL_evreadz00_1512);
																						if (BgL_test2370z00_3886)
																							{	/* Eval/eval.scm 538 */
																								BgL_tmpfunz00_6837 =
																									BgL_evreadz00_1512;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6840;

																								BgL_auxz00_6840 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(21368L),
																									BGl_string2666z00zz__evalz00,
																									BGl_string2620z00zz__evalz00,
																									BgL_evreadz00_1512);
																								FAILURE(BgL_auxz00_6840, BFALSE,
																									BFALSE);
																							}
																					}
																					BgL_arg1517z00_1561 =
																						(VA_PROCEDUREP(BgL_tmpfunz00_6837)
																						? ((obj_t(*)(obj_t,
																									...))
																							PROCEDURE_ENTRY
																							(BgL_tmpfunz00_6837))
																						(BgL_evreadz00_1512,
																							BgL_portz00_1511,
																							BEOA) : ((obj_t(*)(obj_t,
																									obj_t))
																							PROCEDURE_ENTRY
																							(BgL_tmpfunz00_6837))
																						(BgL_evreadz00_1512,
																							BgL_portz00_1511));
																				}
																				{
																					obj_t BgL_sexpz00_6844;

																					BgL_sexpz00_6844 =
																						BgL_arg1517z00_1561;
																					BgL_sexpz00_1549 = BgL_sexpz00_6844;
																					goto
																						BgL_zc3z04anonymousza31509ze3z87_1550;
																				}
																			}
																		}
																}
															}
														}
													}
													{	/* Eval/eval.scm 491 */
														bool_t BgL_test3139z00_6845;

														{	/* Eval/eval.scm 491 */
															obj_t BgL_arg2210z00_2805;

															BgL_arg2210z00_2805 =
																BGL_EXITD_PROTECT(BgL_exitd1105z00_1516);
															BgL_test3139z00_6845 = PAIRP(BgL_arg2210z00_2805);
														}
														if (BgL_test3139z00_6845)
															{	/* Eval/eval.scm 491 */
																obj_t BgL_arg2208z00_2806;

																{	/* Eval/eval.scm 491 */
																	obj_t BgL_arg2209z00_2807;

																	BgL_arg2209z00_2807 =
																		BGL_EXITD_PROTECT(BgL_exitd1105z00_1516);
																	{	/* Eval/eval.scm 491 */
																		obj_t BgL_pairz00_2808;

																		if (PAIRP(BgL_arg2209z00_2807))
																			{	/* Eval/eval.scm 491 */
																				BgL_pairz00_2808 = BgL_arg2209z00_2807;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6851;

																				BgL_auxz00_6851 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(19924L),
																					BGl_string2675z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_arg2209z00_2807);
																				FAILURE(BgL_auxz00_6851, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2208z00_2806 = CDR(BgL_pairz00_2808);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1105z00_1516,
																	BgL_arg2208z00_2806);
																BUNSPEC;
															}
														else
															{	/* Eval/eval.scm 491 */
																BFALSE;
															}
													}
													BGL_MODULE_SET(BgL_modz00_1514);
													return BgL_tmp1107z00_1518;
												}
											}
										}
									else
										{	/* Eval/eval.scm 490 */
											return
												BGl_errorz00zz__errorz00(BGl_string2672z00zz__evalz00,
												BGl_string2686z00zz__evalz00, BgL_filenamez00_43);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1518> */
	obj_t BGl_z62zc3z04anonymousza31518ze3ze5zz__evalz00(obj_t BgL_envz00_3676)
	{
		{	/* Eval/eval.scm 491 */
			{	/* Eval/eval.scm 539 */
				obj_t BgL_tmpz00_6859;

				BgL_tmpz00_6859 = PROCEDURE_REF(BgL_envz00_3676, (int) (0L));
				return BGL_MODULE_SET(BgL_tmpz00_6859);
			}
		}

	}



/* expand-define-expander */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2definezd2expanderz00zz__evalz00(obj_t
		BgL_xz00_50, obj_t BgL_ez00_51)
	{
		{	/* Eval/eval.scm 553 */
			{
				obj_t BgL_namez00_1633;
				obj_t BgL_expdzd2lamzd2_1634;
				obj_t BgL_expdzd2lamzf2locz20_1635;
				obj_t BgL_expdzd2evalzd2_1636;

				if (PAIRP(BgL_xz00_50))
					{	/* Eval/eval.scm 583 */
						obj_t BgL_cdrzd2145zd2_1588;

						BgL_cdrzd2145zd2_1588 = CDR(((obj_t) BgL_xz00_50));
						if (PAIRP(BgL_cdrzd2145zd2_1588))
							{	/* Eval/eval.scm 583 */
								obj_t BgL_carzd2148zd2_1590;
								obj_t BgL_cdrzd2149zd2_1591;

								BgL_carzd2148zd2_1590 = CAR(BgL_cdrzd2145zd2_1588);
								BgL_cdrzd2149zd2_1591 = CDR(BgL_cdrzd2145zd2_1588);
								if (SYMBOLP(BgL_carzd2148zd2_1590))
									{	/* Eval/eval.scm 583 */
										if (PAIRP(BgL_cdrzd2149zd2_1591))
											{	/* Eval/eval.scm 583 */
												obj_t BgL_cdrzd2155zd2_1594;

												BgL_cdrzd2155zd2_1594 = CDR(BgL_cdrzd2149zd2_1591);
												if (
													(CAR(BgL_cdrzd2149zd2_1591) ==
														BGl_keyword2687z00zz__evalz00))
													{	/* Eval/eval.scm 583 */
														if (PAIRP(BgL_cdrzd2155zd2_1594))
															{	/* Eval/eval.scm 583 */
																if (NULLP(CDR(BgL_cdrzd2155zd2_1594)))
																	{	/* Eval/eval.scm 583 */
																		obj_t BgL_arg1537z00_1600;

																		BgL_arg1537z00_1600 =
																			CAR(BgL_cdrzd2155zd2_1594);
																		{	/* Eval/eval.scm 585 */
																			obj_t BgL_expdzd2lamzf2locz20_2897;

																			BgL_expdzd2lamzf2locz20_2897 =
																				BGl_evepairifyz00zz__prognz00
																				(BgL_arg1537z00_1600, BgL_xz00_50);
																			{	/* Eval/eval.scm 586 */
																				obj_t BgL_expdzd2evalzd2_2898;

																				{	/* Eval/eval.scm 82 */
																					obj_t BgL_envz00_2900;

																					BgL_envz00_2900 =
																						BGl_defaultzd2environmentzd2zz__evalz00
																						();
																					{	/* Eval/eval.scm 82 */

																						{	/* Eval/eval.scm 176 */
																							obj_t BgL_evaluatez00_2901;

																							if (PROCEDUREP
																								(BGl_defaultzd2evaluatezd2zz__evalz00))
																								{	/* Eval/eval.scm 176 */
																									BgL_evaluatez00_2901 =
																										BGl_defaultzd2evaluatezd2zz__evalz00;
																								}
																							else
																								{	/* Eval/eval.scm 176 */
																									BgL_evaluatez00_2901 =
																										BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																								}
																							{	/* Eval/eval.scm 179 */
																								obj_t BgL_auxz00_6889;

																								if (PROCEDUREP
																									(BgL_evaluatez00_2901))
																									{	/* Eval/eval.scm 179 */
																										BgL_auxz00_6889 =
																											BgL_evaluatez00_2901;
																									}
																								else
																									{
																										obj_t BgL_auxz00_6892;

																										BgL_auxz00_6892 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2618z00zz__evalz00,
																											BINT(6857L),
																											BGl_string2688z00zz__evalz00,
																											BGl_string2620z00zz__evalz00,
																											BgL_evaluatez00_2901);
																										FAILURE(BgL_auxz00_6892,
																											BFALSE, BFALSE);
																									}
																								BgL_expdzd2evalzd2_2898 =
																									BGl_evalzf2expanderzf2zz__evalz00
																									(BgL_expdzd2lamzf2locz20_2897,
																									BgL_envz00_2900,
																									BGl_expandz12zd2envzc0zz__expandz00,
																									BgL_auxz00_6889);
																							}
																						}
																					}
																				}
																				{	/* Eval/eval.scm 587 */

																					BgL_namez00_1633 =
																						BgL_carzd2148zd2_1590;
																					BgL_expdzd2lamzd2_1634 =
																						BgL_arg1537z00_1600;
																					BgL_expdzd2lamzf2locz20_1635 =
																						BgL_expdzd2lamzf2locz20_2897;
																					BgL_expdzd2evalzd2_1636 =
																						BgL_expdzd2evalzd2_2898;
																				BgL_zc3z04anonymousza31553ze3z87_1637:
																					{	/* Eval/eval.scm 559 */
																						obj_t
																							BgL_zc3z04anonymousza31555ze3z87_3678;
																						BgL_zc3z04anonymousza31555ze3z87_3678
																							=
																							MAKE_FX_PROCEDURE
																							(BGl_z62zc3z04anonymousza31555ze3ze5zz__evalz00,
																							(int) (2L), (int) (2L));
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31555ze3z87_3678,
																							(int) (0L), BgL_namez00_1633);
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31555ze3z87_3678,
																							(int) (1L),
																							BgL_expdzd2evalzd2_1636);
																						BGl_installzd2expanderzd2zz__macroz00
																							(BgL_namez00_1633,
																							BgL_zc3z04anonymousza31555ze3z87_3678);
																					}
																					return BUNSPEC;
																				}
																			}
																		}
																	}
																else
																	{	/* Eval/eval.scm 583 */
																		obj_t BgL_cdrzd2166zd2_1601;

																		BgL_cdrzd2166zd2_1601 =
																			CDR(((obj_t) BgL_xz00_50));
																		{	/* Eval/eval.scm 583 */
																			obj_t BgL_carzd2170zd2_1602;

																			{	/* Eval/eval.scm 583 */
																				obj_t BgL_pairz00_2905;

																				if (PAIRP(BgL_cdrzd2166zd2_1601))
																					{	/* Eval/eval.scm 583 */
																						BgL_pairz00_2905 =
																							BgL_cdrzd2166zd2_1601;
																					}
																				else
																					{
																						obj_t BgL_auxz00_6909;

																						BgL_auxz00_6909 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string2618z00zz__evalz00,
																							BINT(22842L),
																							BGl_string2688z00zz__evalz00,
																							BGl_string2626z00zz__evalz00,
																							BgL_cdrzd2166zd2_1601);
																						FAILURE(BgL_auxz00_6909, BFALSE,
																							BFALSE);
																					}
																				BgL_carzd2170zd2_1602 =
																					CAR(BgL_pairz00_2905);
																			}
																			if (SYMBOLP(BgL_carzd2170zd2_1602))
																				{	/* Eval/eval.scm 583 */
																					obj_t BgL_arg1539z00_1604;

																					{	/* Eval/eval.scm 583 */
																						obj_t BgL_pairz00_2906;

																						if (PAIRP(BgL_cdrzd2166zd2_1601))
																							{	/* Eval/eval.scm 583 */
																								BgL_pairz00_2906 =
																									BgL_cdrzd2166zd2_1601;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6918;

																								BgL_auxz00_6918 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(22842L),
																									BGl_string2688z00zz__evalz00,
																									BGl_string2626z00zz__evalz00,
																									BgL_cdrzd2166zd2_1601);
																								FAILURE(BgL_auxz00_6918, BFALSE,
																									BFALSE);
																							}
																						BgL_arg1539z00_1604 =
																							CDR(BgL_pairz00_2906);
																					}
																					{	/* Eval/eval.scm 590 */
																						obj_t BgL_expdzd2lamzd2_2907;

																						{	/* Eval/eval.scm 590 */
																							obj_t BgL_auxz00_6923;

																							{	/* Eval/eval.scm 590 */
																								bool_t BgL_test3153z00_6924;

																								if (PAIRP(BgL_arg1539z00_1604))
																									{	/* Eval/eval.scm 590 */
																										BgL_test3153z00_6924 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Eval/eval.scm 590 */
																										BgL_test3153z00_6924 =
																											NULLP
																											(BgL_arg1539z00_1604);
																									}
																								if (BgL_test3153z00_6924)
																									{	/* Eval/eval.scm 590 */
																										BgL_auxz00_6923 =
																											BgL_arg1539z00_1604;
																									}
																								else
																									{
																										obj_t BgL_auxz00_6928;

																										BgL_auxz00_6928 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2618z00zz__evalz00,
																											BINT(23175L),
																											BGl_string2688z00zz__evalz00,
																											BGl_string2681z00zz__evalz00,
																											BgL_arg1539z00_1604);
																										FAILURE(BgL_auxz00_6928,
																											BFALSE, BFALSE);
																									}
																							}
																							BgL_expdzd2lamzd2_2907 =
																								BGl_expandzd2prognzd2zz__prognz00
																								(BgL_auxz00_6923);
																						}
																						{	/* Eval/eval.scm 590 */
																							obj_t
																								BgL_expdzd2lamzf2locz20_2908;
																							BgL_expdzd2lamzf2locz20_2908 =
																								BGl_evepairifyz00zz__prognz00
																								(BgL_expdzd2lamzd2_2907,
																								BgL_xz00_50);
																							{	/* Eval/eval.scm 591 */
																								obj_t BgL_expdzd2evalzd2_2909;

																								{	/* Eval/eval.scm 81 */
																									obj_t BgL_envz00_2911;

																									BgL_envz00_2911 =
																										BGl_defaultzd2environmentzd2zz__evalz00
																										();
																									{	/* Eval/eval.scm 81 */

																										{	/* Eval/eval.scm 170 */
																											obj_t BgL_auxz00_6935;

																											{	/* Eval/eval.scm 170 */
																												obj_t
																													BgL_aux2385z00_3901;
																												BgL_aux2385z00_3901 =
																													BGl_defaultzd2evaluatezd2zz__evalz00;
																												if (PROCEDUREP
																													(BgL_aux2385z00_3901))
																													{	/* Eval/eval.scm 170 */
																														BgL_auxz00_6935 =
																															BgL_aux2385z00_3901;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_6938;
																														BgL_auxz00_6938 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string2618z00zz__evalz00,
																															BINT(6408L),
																															BGl_string2688z00zz__evalz00,
																															BGl_string2620z00zz__evalz00,
																															BgL_aux2385z00_3901);
																														FAILURE
																															(BgL_auxz00_6938,
																															BFALSE, BFALSE);
																													}
																											}
																											BgL_expdzd2evalzd2_2909 =
																												BGl_evalzf2expanderzf2zz__evalz00
																												(BgL_expdzd2lamzf2locz20_2908,
																												BgL_envz00_2911,
																												BGl_expandzd2envzd2zz__expandz00,
																												BgL_auxz00_6935);
																										}
																									}
																								}
																								{	/* Eval/eval.scm 592 */

																									{
																										obj_t
																											BgL_expdzd2evalzd2_6946;
																										obj_t
																											BgL_expdzd2lamzf2locz20_6945;
																										obj_t
																											BgL_expdzd2lamzd2_6944;
																										obj_t BgL_namez00_6943;

																										BgL_namez00_6943 =
																											BgL_carzd2170zd2_1602;
																										BgL_expdzd2lamzd2_6944 =
																											BgL_expdzd2lamzd2_2907;
																										BgL_expdzd2lamzf2locz20_6945
																											=
																											BgL_expdzd2lamzf2locz20_2908;
																										BgL_expdzd2evalzd2_6946 =
																											BgL_expdzd2evalzd2_2909;
																										BgL_expdzd2evalzd2_1636 =
																											BgL_expdzd2evalzd2_6946;
																										BgL_expdzd2lamzf2locz20_1635
																											=
																											BgL_expdzd2lamzf2locz20_6945;
																										BgL_expdzd2lamzd2_1634 =
																											BgL_expdzd2lamzd2_6944;
																										BgL_namez00_1633 =
																											BgL_namez00_6943;
																										goto
																											BgL_zc3z04anonymousza31553ze3z87_1637;
																									}
																								}
																							}
																						}
																					}
																				}
																			else
																				{	/* Eval/eval.scm 595 */
																					obj_t BgL_procz00_2912;

																					BgL_procz00_2912 =
																						BGl_symbol2689z00zz__evalz00;
																					if (EPAIRP(BgL_xz00_50))
																						{	/* Eval/eval.scm 547 */
																							obj_t BgL_arg1524z00_2914;

																							{	/* Eval/eval.scm 547 */
																								obj_t BgL_objz00_2915;

																								if (EPAIRP(BgL_xz00_50))
																									{	/* Eval/eval.scm 547 */
																										BgL_objz00_2915 =
																											BgL_xz00_50;
																									}
																								else
																									{
																										obj_t BgL_auxz00_6951;

																										BgL_auxz00_6951 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2618z00zz__evalz00,
																											BINT(21769L),
																											BGl_string2688z00zz__evalz00,
																											BGl_string2676z00zz__evalz00,
																											BgL_xz00_50);
																										FAILURE(BgL_auxz00_6951,
																											BFALSE, BFALSE);
																									}
																								BgL_arg1524z00_2914 =
																									CER(BgL_objz00_2915);
																							}
																							return
																								BGl_everrorz00zz__everrorz00
																								(BgL_arg1524z00_2914,
																								BgL_procz00_2912,
																								BGl_string2691z00zz__evalz00,
																								BgL_xz00_50);
																						}
																					else
																						{	/* Eval/eval.scm 546 */
																							return
																								BGl_errorz00zz__errorz00
																								(BgL_procz00_2912,
																								BGl_string2691z00zz__evalz00,
																								BgL_xz00_50);
																						}
																				}
																		}
																	}
															}
														else
															{	/* Eval/eval.scm 583 */
																obj_t BgL_cdrzd2184zd2_1606;

																BgL_cdrzd2184zd2_1606 =
																	CDR(((obj_t) BgL_xz00_50));
																{	/* Eval/eval.scm 583 */
																	obj_t BgL_carzd2188zd2_1607;

																	{	/* Eval/eval.scm 583 */
																		obj_t BgL_pairz00_2917;

																		if (PAIRP(BgL_cdrzd2184zd2_1606))
																			{	/* Eval/eval.scm 583 */
																				BgL_pairz00_2917 =
																					BgL_cdrzd2184zd2_1606;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6962;

																				BgL_auxz00_6962 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(22842L),
																					BGl_string2688z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_cdrzd2184zd2_1606);
																				FAILURE(BgL_auxz00_6962, BFALSE,
																					BFALSE);
																			}
																		BgL_carzd2188zd2_1607 =
																			CAR(BgL_pairz00_2917);
																	}
																	if (SYMBOLP(BgL_carzd2188zd2_1607))
																		{	/* Eval/eval.scm 583 */
																			obj_t BgL_arg1543z00_1609;

																			{	/* Eval/eval.scm 583 */
																				obj_t BgL_pairz00_2918;

																				if (PAIRP(BgL_cdrzd2184zd2_1606))
																					{	/* Eval/eval.scm 583 */
																						BgL_pairz00_2918 =
																							BgL_cdrzd2184zd2_1606;
																					}
																				else
																					{
																						obj_t BgL_auxz00_6971;

																						BgL_auxz00_6971 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string2618z00zz__evalz00,
																							BINT(22842L),
																							BGl_string2688z00zz__evalz00,
																							BGl_string2626z00zz__evalz00,
																							BgL_cdrzd2184zd2_1606);
																						FAILURE(BgL_auxz00_6971, BFALSE,
																							BFALSE);
																					}
																				BgL_arg1543z00_1609 =
																					CDR(BgL_pairz00_2918);
																			}
																			{	/* Eval/eval.scm 590 */
																				obj_t BgL_expdzd2lamzd2_2919;

																				{	/* Eval/eval.scm 590 */
																					obj_t BgL_auxz00_6976;

																					{	/* Eval/eval.scm 590 */
																						bool_t BgL_test3161z00_6977;

																						if (PAIRP(BgL_arg1543z00_1609))
																							{	/* Eval/eval.scm 590 */
																								BgL_test3161z00_6977 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Eval/eval.scm 590 */
																								BgL_test3161z00_6977 =
																									NULLP(BgL_arg1543z00_1609);
																							}
																						if (BgL_test3161z00_6977)
																							{	/* Eval/eval.scm 590 */
																								BgL_auxz00_6976 =
																									BgL_arg1543z00_1609;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6981;

																								BgL_auxz00_6981 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(23175L),
																									BGl_string2688z00zz__evalz00,
																									BGl_string2681z00zz__evalz00,
																									BgL_arg1543z00_1609);
																								FAILURE(BgL_auxz00_6981, BFALSE,
																									BFALSE);
																							}
																					}
																					BgL_expdzd2lamzd2_2919 =
																						BGl_expandzd2prognzd2zz__prognz00
																						(BgL_auxz00_6976);
																				}
																				{	/* Eval/eval.scm 590 */
																					obj_t BgL_expdzd2lamzf2locz20_2920;

																					BgL_expdzd2lamzf2locz20_2920 =
																						BGl_evepairifyz00zz__prognz00
																						(BgL_expdzd2lamzd2_2919,
																						BgL_xz00_50);
																					{	/* Eval/eval.scm 591 */
																						obj_t BgL_expdzd2evalzd2_2921;

																						{	/* Eval/eval.scm 81 */
																							obj_t BgL_envz00_2923;

																							BgL_envz00_2923 =
																								BGl_defaultzd2environmentzd2zz__evalz00
																								();
																							{	/* Eval/eval.scm 81 */

																								{	/* Eval/eval.scm 170 */
																									obj_t BgL_auxz00_6988;

																									{	/* Eval/eval.scm 170 */
																										obj_t BgL_aux2397z00_3913;

																										BgL_aux2397z00_3913 =
																											BGl_defaultzd2evaluatezd2zz__evalz00;
																										if (PROCEDUREP
																											(BgL_aux2397z00_3913))
																											{	/* Eval/eval.scm 170 */
																												BgL_auxz00_6988 =
																													BgL_aux2397z00_3913;
																											}
																										else
																											{
																												obj_t BgL_auxz00_6991;

																												BgL_auxz00_6991 =
																													BGl_typezd2errorzd2zz__errorz00
																													(BGl_string2618z00zz__evalz00,
																													BINT(6408L),
																													BGl_string2688z00zz__evalz00,
																													BGl_string2620z00zz__evalz00,
																													BgL_aux2397z00_3913);
																												FAILURE(BgL_auxz00_6991,
																													BFALSE, BFALSE);
																											}
																									}
																									BgL_expdzd2evalzd2_2921 =
																										BGl_evalzf2expanderzf2zz__evalz00
																										(BgL_expdzd2lamzf2locz20_2920,
																										BgL_envz00_2923,
																										BGl_expandzd2envzd2zz__expandz00,
																										BgL_auxz00_6988);
																								}
																							}
																						}
																						{	/* Eval/eval.scm 592 */

																							{
																								obj_t BgL_expdzd2evalzd2_6999;
																								obj_t
																									BgL_expdzd2lamzf2locz20_6998;
																								obj_t BgL_expdzd2lamzd2_6997;
																								obj_t BgL_namez00_6996;

																								BgL_namez00_6996 =
																									BgL_carzd2188zd2_1607;
																								BgL_expdzd2lamzd2_6997 =
																									BgL_expdzd2lamzd2_2919;
																								BgL_expdzd2lamzf2locz20_6998 =
																									BgL_expdzd2lamzf2locz20_2920;
																								BgL_expdzd2evalzd2_6999 =
																									BgL_expdzd2evalzd2_2921;
																								BgL_expdzd2evalzd2_1636 =
																									BgL_expdzd2evalzd2_6999;
																								BgL_expdzd2lamzf2locz20_1635 =
																									BgL_expdzd2lamzf2locz20_6998;
																								BgL_expdzd2lamzd2_1634 =
																									BgL_expdzd2lamzd2_6997;
																								BgL_namez00_1633 =
																									BgL_namez00_6996;
																								goto
																									BgL_zc3z04anonymousza31553ze3z87_1637;
																							}
																						}
																					}
																				}
																			}
																		}
																	else
																		{	/* Eval/eval.scm 595 */
																			obj_t BgL_procz00_2924;

																			BgL_procz00_2924 =
																				BGl_symbol2689z00zz__evalz00;
																			if (EPAIRP(BgL_xz00_50))
																				{	/* Eval/eval.scm 547 */
																					obj_t BgL_arg1524z00_2926;

																					{	/* Eval/eval.scm 547 */
																						obj_t BgL_objz00_2927;

																						if (EPAIRP(BgL_xz00_50))
																							{	/* Eval/eval.scm 547 */
																								BgL_objz00_2927 = BgL_xz00_50;
																							}
																						else
																							{
																								obj_t BgL_auxz00_7004;

																								BgL_auxz00_7004 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(21769L),
																									BGl_string2688z00zz__evalz00,
																									BGl_string2676z00zz__evalz00,
																									BgL_xz00_50);
																								FAILURE(BgL_auxz00_7004, BFALSE,
																									BFALSE);
																							}
																						BgL_arg1524z00_2926 =
																							CER(BgL_objz00_2927);
																					}
																					return
																						BGl_everrorz00zz__everrorz00
																						(BgL_arg1524z00_2926,
																						BgL_procz00_2924,
																						BGl_string2691z00zz__evalz00,
																						BgL_xz00_50);
																				}
																			else
																				{	/* Eval/eval.scm 546 */
																					return
																						BGl_errorz00zz__errorz00
																						(BgL_procz00_2924,
																						BGl_string2691z00zz__evalz00,
																						BgL_xz00_50);
																				}
																		}
																}
															}
													}
												else
													{	/* Eval/eval.scm 583 */
														obj_t BgL_cdrzd2202zd2_1610;

														BgL_cdrzd2202zd2_1610 = CDR(((obj_t) BgL_xz00_50));
														{	/* Eval/eval.scm 583 */
															obj_t BgL_carzd2206zd2_1611;

															{	/* Eval/eval.scm 583 */
																obj_t BgL_pairz00_2929;

																if (PAIRP(BgL_cdrzd2202zd2_1610))
																	{	/* Eval/eval.scm 583 */
																		BgL_pairz00_2929 = BgL_cdrzd2202zd2_1610;
																	}
																else
																	{
																		obj_t BgL_auxz00_7015;

																		BgL_auxz00_7015 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2618z00zz__evalz00,
																			BINT(22842L),
																			BGl_string2688z00zz__evalz00,
																			BGl_string2626z00zz__evalz00,
																			BgL_cdrzd2202zd2_1610);
																		FAILURE(BgL_auxz00_7015, BFALSE, BFALSE);
																	}
																BgL_carzd2206zd2_1611 = CAR(BgL_pairz00_2929);
															}
															if (SYMBOLP(BgL_carzd2206zd2_1611))
																{	/* Eval/eval.scm 583 */
																	obj_t BgL_arg1546z00_1613;

																	{	/* Eval/eval.scm 583 */
																		obj_t BgL_pairz00_2930;

																		if (PAIRP(BgL_cdrzd2202zd2_1610))
																			{	/* Eval/eval.scm 583 */
																				BgL_pairz00_2930 =
																					BgL_cdrzd2202zd2_1610;
																			}
																		else
																			{
																				obj_t BgL_auxz00_7024;

																				BgL_auxz00_7024 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(22842L),
																					BGl_string2688z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_cdrzd2202zd2_1610);
																				FAILURE(BgL_auxz00_7024, BFALSE,
																					BFALSE);
																			}
																		BgL_arg1546z00_1613 = CDR(BgL_pairz00_2930);
																	}
																	{	/* Eval/eval.scm 590 */
																		obj_t BgL_expdzd2lamzd2_2931;

																		{	/* Eval/eval.scm 590 */
																			obj_t BgL_auxz00_7029;

																			{	/* Eval/eval.scm 590 */
																				bool_t BgL_test3169z00_7030;

																				if (PAIRP(BgL_arg1546z00_1613))
																					{	/* Eval/eval.scm 590 */
																						BgL_test3169z00_7030 = ((bool_t) 1);
																					}
																				else
																					{	/* Eval/eval.scm 590 */
																						BgL_test3169z00_7030 =
																							NULLP(BgL_arg1546z00_1613);
																					}
																				if (BgL_test3169z00_7030)
																					{	/* Eval/eval.scm 590 */
																						BgL_auxz00_7029 =
																							BgL_arg1546z00_1613;
																					}
																				else
																					{
																						obj_t BgL_auxz00_7034;

																						BgL_auxz00_7034 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string2618z00zz__evalz00,
																							BINT(23175L),
																							BGl_string2688z00zz__evalz00,
																							BGl_string2681z00zz__evalz00,
																							BgL_arg1546z00_1613);
																						FAILURE(BgL_auxz00_7034, BFALSE,
																							BFALSE);
																					}
																			}
																			BgL_expdzd2lamzd2_2931 =
																				BGl_expandzd2prognzd2zz__prognz00
																				(BgL_auxz00_7029);
																		}
																		{	/* Eval/eval.scm 590 */
																			obj_t BgL_expdzd2lamzf2locz20_2932;

																			BgL_expdzd2lamzf2locz20_2932 =
																				BGl_evepairifyz00zz__prognz00
																				(BgL_expdzd2lamzd2_2931, BgL_xz00_50);
																			{	/* Eval/eval.scm 591 */
																				obj_t BgL_expdzd2evalzd2_2933;

																				{	/* Eval/eval.scm 81 */
																					obj_t BgL_envz00_2935;

																					BgL_envz00_2935 =
																						BGl_defaultzd2environmentzd2zz__evalz00
																						();
																					{	/* Eval/eval.scm 81 */

																						{	/* Eval/eval.scm 170 */
																							obj_t BgL_auxz00_7041;

																							{	/* Eval/eval.scm 170 */
																								obj_t BgL_aux2409z00_3925;

																								BgL_aux2409z00_3925 =
																									BGl_defaultzd2evaluatezd2zz__evalz00;
																								if (PROCEDUREP
																									(BgL_aux2409z00_3925))
																									{	/* Eval/eval.scm 170 */
																										BgL_auxz00_7041 =
																											BgL_aux2409z00_3925;
																									}
																								else
																									{
																										obj_t BgL_auxz00_7044;

																										BgL_auxz00_7044 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2618z00zz__evalz00,
																											BINT(6408L),
																											BGl_string2688z00zz__evalz00,
																											BGl_string2620z00zz__evalz00,
																											BgL_aux2409z00_3925);
																										FAILURE(BgL_auxz00_7044,
																											BFALSE, BFALSE);
																									}
																							}
																							BgL_expdzd2evalzd2_2933 =
																								BGl_evalzf2expanderzf2zz__evalz00
																								(BgL_expdzd2lamzf2locz20_2932,
																								BgL_envz00_2935,
																								BGl_expandzd2envzd2zz__expandz00,
																								BgL_auxz00_7041);
																						}
																					}
																				}
																				{	/* Eval/eval.scm 592 */

																					{
																						obj_t BgL_expdzd2evalzd2_7052;
																						obj_t BgL_expdzd2lamzf2locz20_7051;
																						obj_t BgL_expdzd2lamzd2_7050;
																						obj_t BgL_namez00_7049;

																						BgL_namez00_7049 =
																							BgL_carzd2206zd2_1611;
																						BgL_expdzd2lamzd2_7050 =
																							BgL_expdzd2lamzd2_2931;
																						BgL_expdzd2lamzf2locz20_7051 =
																							BgL_expdzd2lamzf2locz20_2932;
																						BgL_expdzd2evalzd2_7052 =
																							BgL_expdzd2evalzd2_2933;
																						BgL_expdzd2evalzd2_1636 =
																							BgL_expdzd2evalzd2_7052;
																						BgL_expdzd2lamzf2locz20_1635 =
																							BgL_expdzd2lamzf2locz20_7051;
																						BgL_expdzd2lamzd2_1634 =
																							BgL_expdzd2lamzd2_7050;
																						BgL_namez00_1633 = BgL_namez00_7049;
																						goto
																							BgL_zc3z04anonymousza31553ze3z87_1637;
																					}
																				}
																			}
																		}
																	}
																}
															else
																{	/* Eval/eval.scm 595 */
																	obj_t BgL_procz00_2936;

																	BgL_procz00_2936 =
																		BGl_symbol2689z00zz__evalz00;
																	if (EPAIRP(BgL_xz00_50))
																		{	/* Eval/eval.scm 547 */
																			obj_t BgL_arg1524z00_2938;

																			{	/* Eval/eval.scm 547 */
																				obj_t BgL_objz00_2939;

																				if (EPAIRP(BgL_xz00_50))
																					{	/* Eval/eval.scm 547 */
																						BgL_objz00_2939 = BgL_xz00_50;
																					}
																				else
																					{
																						obj_t BgL_auxz00_7057;

																						BgL_auxz00_7057 =
																							BGl_typezd2errorzd2zz__errorz00
																							(BGl_string2618z00zz__evalz00,
																							BINT(21769L),
																							BGl_string2688z00zz__evalz00,
																							BGl_string2676z00zz__evalz00,
																							BgL_xz00_50);
																						FAILURE(BgL_auxz00_7057, BFALSE,
																							BFALSE);
																					}
																				BgL_arg1524z00_2938 =
																					CER(BgL_objz00_2939);
																			}
																			return
																				BGl_everrorz00zz__everrorz00
																				(BgL_arg1524z00_2938, BgL_procz00_2936,
																				BGl_string2691z00zz__evalz00,
																				BgL_xz00_50);
																		}
																	else
																		{	/* Eval/eval.scm 546 */
																			return
																				BGl_errorz00zz__errorz00
																				(BgL_procz00_2936,
																				BGl_string2691z00zz__evalz00,
																				BgL_xz00_50);
																		}
																}
														}
													}
											}
										else
											{	/* Eval/eval.scm 583 */
												obj_t BgL_carzd2224zd2_1616;

												{	/* Eval/eval.scm 583 */
													obj_t BgL_pairz00_2941;

													BgL_pairz00_2941 = BgL_cdrzd2145zd2_1588;
													BgL_carzd2224zd2_1616 = CAR(BgL_pairz00_2941);
												}
												if (SYMBOLP(BgL_carzd2224zd2_1616))
													{	/* Eval/eval.scm 583 */
														obj_t BgL_arg1549z00_1618;

														{	/* Eval/eval.scm 583 */
															obj_t BgL_pairz00_2942;

															BgL_pairz00_2942 = BgL_cdrzd2145zd2_1588;
															BgL_arg1549z00_1618 = CDR(BgL_pairz00_2942);
														}
														{	/* Eval/eval.scm 590 */
															obj_t BgL_expdzd2lamzd2_2943;

															{	/* Eval/eval.scm 590 */
																obj_t BgL_auxz00_7068;

																{	/* Eval/eval.scm 590 */
																	bool_t BgL_test3175z00_7069;

																	if (PAIRP(BgL_arg1549z00_1618))
																		{	/* Eval/eval.scm 590 */
																			BgL_test3175z00_7069 = ((bool_t) 1);
																		}
																	else
																		{	/* Eval/eval.scm 590 */
																			BgL_test3175z00_7069 =
																				NULLP(BgL_arg1549z00_1618);
																		}
																	if (BgL_test3175z00_7069)
																		{	/* Eval/eval.scm 590 */
																			BgL_auxz00_7068 = BgL_arg1549z00_1618;
																		}
																	else
																		{
																			obj_t BgL_auxz00_7073;

																			BgL_auxz00_7073 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(23175L),
																				BGl_string2688z00zz__evalz00,
																				BGl_string2681z00zz__evalz00,
																				BgL_arg1549z00_1618);
																			FAILURE(BgL_auxz00_7073, BFALSE, BFALSE);
																		}
																}
																BgL_expdzd2lamzd2_2943 =
																	BGl_expandzd2prognzd2zz__prognz00
																	(BgL_auxz00_7068);
															}
															{	/* Eval/eval.scm 590 */
																obj_t BgL_expdzd2lamzf2locz20_2944;

																BgL_expdzd2lamzf2locz20_2944 =
																	BGl_evepairifyz00zz__prognz00
																	(BgL_expdzd2lamzd2_2943, BgL_xz00_50);
																{	/* Eval/eval.scm 591 */
																	obj_t BgL_expdzd2evalzd2_2945;

																	{	/* Eval/eval.scm 81 */
																		obj_t BgL_envz00_2947;

																		BgL_envz00_2947 =
																			BGl_defaultzd2environmentzd2zz__evalz00();
																		{	/* Eval/eval.scm 81 */

																			{	/* Eval/eval.scm 170 */
																				obj_t BgL_auxz00_7080;

																				{	/* Eval/eval.scm 170 */
																					obj_t BgL_aux2421z00_3937;

																					BgL_aux2421z00_3937 =
																						BGl_defaultzd2evaluatezd2zz__evalz00;
																					if (PROCEDUREP(BgL_aux2421z00_3937))
																						{	/* Eval/eval.scm 170 */
																							BgL_auxz00_7080 =
																								BgL_aux2421z00_3937;
																						}
																					else
																						{
																							obj_t BgL_auxz00_7083;

																							BgL_auxz00_7083 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(6408L),
																								BGl_string2688z00zz__evalz00,
																								BGl_string2620z00zz__evalz00,
																								BgL_aux2421z00_3937);
																							FAILURE(BgL_auxz00_7083, BFALSE,
																								BFALSE);
																						}
																				}
																				BgL_expdzd2evalzd2_2945 =
																					BGl_evalzf2expanderzf2zz__evalz00
																					(BgL_expdzd2lamzf2locz20_2944,
																					BgL_envz00_2947,
																					BGl_expandzd2envzd2zz__expandz00,
																					BgL_auxz00_7080);
																			}
																		}
																	}
																	{	/* Eval/eval.scm 592 */

																		{
																			obj_t BgL_expdzd2evalzd2_7091;
																			obj_t BgL_expdzd2lamzf2locz20_7090;
																			obj_t BgL_expdzd2lamzd2_7089;
																			obj_t BgL_namez00_7088;

																			BgL_namez00_7088 = BgL_carzd2224zd2_1616;
																			BgL_expdzd2lamzd2_7089 =
																				BgL_expdzd2lamzd2_2943;
																			BgL_expdzd2lamzf2locz20_7090 =
																				BgL_expdzd2lamzf2locz20_2944;
																			BgL_expdzd2evalzd2_7091 =
																				BgL_expdzd2evalzd2_2945;
																			BgL_expdzd2evalzd2_1636 =
																				BgL_expdzd2evalzd2_7091;
																			BgL_expdzd2lamzf2locz20_1635 =
																				BgL_expdzd2lamzf2locz20_7090;
																			BgL_expdzd2lamzd2_1634 =
																				BgL_expdzd2lamzd2_7089;
																			BgL_namez00_1633 = BgL_namez00_7088;
																			goto
																				BgL_zc3z04anonymousza31553ze3z87_1637;
																		}
																	}
																}
															}
														}
													}
												else
													{	/* Eval/eval.scm 595 */
														obj_t BgL_procz00_2948;

														BgL_procz00_2948 = BGl_symbol2689z00zz__evalz00;
														if (EPAIRP(BgL_xz00_50))
															{	/* Eval/eval.scm 547 */
																obj_t BgL_arg1524z00_2950;

																{	/* Eval/eval.scm 547 */
																	obj_t BgL_objz00_2951;

																	if (EPAIRP(BgL_xz00_50))
																		{	/* Eval/eval.scm 547 */
																			BgL_objz00_2951 = BgL_xz00_50;
																		}
																	else
																		{
																			obj_t BgL_auxz00_7096;

																			BgL_auxz00_7096 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(21769L),
																				BGl_string2688z00zz__evalz00,
																				BGl_string2676z00zz__evalz00,
																				BgL_xz00_50);
																			FAILURE(BgL_auxz00_7096, BFALSE, BFALSE);
																		}
																	BgL_arg1524z00_2950 = CER(BgL_objz00_2951);
																}
																return
																	BGl_everrorz00zz__everrorz00
																	(BgL_arg1524z00_2950, BgL_procz00_2948,
																	BGl_string2691z00zz__evalz00, BgL_xz00_50);
															}
														else
															{	/* Eval/eval.scm 546 */
																return
																	BGl_errorz00zz__errorz00(BgL_procz00_2948,
																	BGl_string2691z00zz__evalz00, BgL_xz00_50);
															}
													}
											}
									}
								else
									{	/* Eval/eval.scm 583 */
										obj_t BgL_carzd2240zd2_1620;

										{	/* Eval/eval.scm 583 */
											obj_t BgL_pairz00_2953;

											BgL_pairz00_2953 = BgL_cdrzd2145zd2_1588;
											BgL_carzd2240zd2_1620 = CAR(BgL_pairz00_2953);
										}
										if (SYMBOLP(BgL_carzd2240zd2_1620))
											{	/* Eval/eval.scm 583 */
												obj_t BgL_arg1552z00_1622;

												{	/* Eval/eval.scm 583 */
													obj_t BgL_pairz00_2954;

													BgL_pairz00_2954 = BgL_cdrzd2145zd2_1588;
													BgL_arg1552z00_1622 = CDR(BgL_pairz00_2954);
												}
												{	/* Eval/eval.scm 590 */
													obj_t BgL_expdzd2lamzd2_2955;

													{	/* Eval/eval.scm 590 */
														obj_t BgL_auxz00_7107;

														{	/* Eval/eval.scm 590 */
															bool_t BgL_test3181z00_7108;

															if (PAIRP(BgL_arg1552z00_1622))
																{	/* Eval/eval.scm 590 */
																	BgL_test3181z00_7108 = ((bool_t) 1);
																}
															else
																{	/* Eval/eval.scm 590 */
																	BgL_test3181z00_7108 =
																		NULLP(BgL_arg1552z00_1622);
																}
															if (BgL_test3181z00_7108)
																{	/* Eval/eval.scm 590 */
																	BgL_auxz00_7107 = BgL_arg1552z00_1622;
																}
															else
																{
																	obj_t BgL_auxz00_7112;

																	BgL_auxz00_7112 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(23175L),
																		BGl_string2688z00zz__evalz00,
																		BGl_string2681z00zz__evalz00,
																		BgL_arg1552z00_1622);
																	FAILURE(BgL_auxz00_7112, BFALSE, BFALSE);
																}
														}
														BgL_expdzd2lamzd2_2955 =
															BGl_expandzd2prognzd2zz__prognz00
															(BgL_auxz00_7107);
													}
													{	/* Eval/eval.scm 590 */
														obj_t BgL_expdzd2lamzf2locz20_2956;

														BgL_expdzd2lamzf2locz20_2956 =
															BGl_evepairifyz00zz__prognz00
															(BgL_expdzd2lamzd2_2955, BgL_xz00_50);
														{	/* Eval/eval.scm 591 */
															obj_t BgL_expdzd2evalzd2_2957;

															{	/* Eval/eval.scm 81 */
																obj_t BgL_envz00_2959;

																BgL_envz00_2959 =
																	BGl_defaultzd2environmentzd2zz__evalz00();
																{	/* Eval/eval.scm 81 */

																	{	/* Eval/eval.scm 170 */
																		obj_t BgL_auxz00_7119;

																		{	/* Eval/eval.scm 170 */
																			obj_t BgL_aux2433z00_3949;

																			BgL_aux2433z00_3949 =
																				BGl_defaultzd2evaluatezd2zz__evalz00;
																			if (PROCEDUREP(BgL_aux2433z00_3949))
																				{	/* Eval/eval.scm 170 */
																					BgL_auxz00_7119 = BgL_aux2433z00_3949;
																				}
																			else
																				{
																					obj_t BgL_auxz00_7122;

																					BgL_auxz00_7122 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(6408L),
																						BGl_string2688z00zz__evalz00,
																						BGl_string2620z00zz__evalz00,
																						BgL_aux2433z00_3949);
																					FAILURE(BgL_auxz00_7122, BFALSE,
																						BFALSE);
																				}
																		}
																		BgL_expdzd2evalzd2_2957 =
																			BGl_evalzf2expanderzf2zz__evalz00
																			(BgL_expdzd2lamzf2locz20_2956,
																			BgL_envz00_2959,
																			BGl_expandzd2envzd2zz__expandz00,
																			BgL_auxz00_7119);
																	}
																}
															}
															{	/* Eval/eval.scm 592 */

																{
																	obj_t BgL_expdzd2evalzd2_7130;
																	obj_t BgL_expdzd2lamzf2locz20_7129;
																	obj_t BgL_expdzd2lamzd2_7128;
																	obj_t BgL_namez00_7127;

																	BgL_namez00_7127 = BgL_carzd2240zd2_1620;
																	BgL_expdzd2lamzd2_7128 =
																		BgL_expdzd2lamzd2_2955;
																	BgL_expdzd2lamzf2locz20_7129 =
																		BgL_expdzd2lamzf2locz20_2956;
																	BgL_expdzd2evalzd2_7130 =
																		BgL_expdzd2evalzd2_2957;
																	BgL_expdzd2evalzd2_1636 =
																		BgL_expdzd2evalzd2_7130;
																	BgL_expdzd2lamzf2locz20_1635 =
																		BgL_expdzd2lamzf2locz20_7129;
																	BgL_expdzd2lamzd2_1634 =
																		BgL_expdzd2lamzd2_7128;
																	BgL_namez00_1633 = BgL_namez00_7127;
																	goto BgL_zc3z04anonymousza31553ze3z87_1637;
																}
															}
														}
													}
												}
											}
										else
											{	/* Eval/eval.scm 595 */
												obj_t BgL_procz00_2960;

												BgL_procz00_2960 = BGl_symbol2689z00zz__evalz00;
												if (EPAIRP(BgL_xz00_50))
													{	/* Eval/eval.scm 547 */
														obj_t BgL_arg1524z00_2962;

														{	/* Eval/eval.scm 547 */
															obj_t BgL_objz00_2963;

															if (EPAIRP(BgL_xz00_50))
																{	/* Eval/eval.scm 547 */
																	BgL_objz00_2963 = BgL_xz00_50;
																}
															else
																{
																	obj_t BgL_auxz00_7135;

																	BgL_auxz00_7135 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(21769L),
																		BGl_string2688z00zz__evalz00,
																		BGl_string2676z00zz__evalz00, BgL_xz00_50);
																	FAILURE(BgL_auxz00_7135, BFALSE, BFALSE);
																}
															BgL_arg1524z00_2962 = CER(BgL_objz00_2963);
														}
														return
															BGl_everrorz00zz__everrorz00(BgL_arg1524z00_2962,
															BgL_procz00_2960, BGl_string2691z00zz__evalz00,
															BgL_xz00_50);
													}
												else
													{	/* Eval/eval.scm 546 */
														return
															BGl_errorz00zz__errorz00(BgL_procz00_2960,
															BGl_string2691z00zz__evalz00, BgL_xz00_50);
													}
											}
									}
							}
						else
							{	/* Eval/eval.scm 595 */
								obj_t BgL_procz00_2964;

								BgL_procz00_2964 = BGl_symbol2689z00zz__evalz00;
								if (EPAIRP(BgL_xz00_50))
									{	/* Eval/eval.scm 547 */
										obj_t BgL_arg1524z00_2966;

										{	/* Eval/eval.scm 547 */
											obj_t BgL_objz00_2967;

											if (EPAIRP(BgL_xz00_50))
												{	/* Eval/eval.scm 547 */
													BgL_objz00_2967 = BgL_xz00_50;
												}
											else
												{
													obj_t BgL_auxz00_7146;

													BgL_auxz00_7146 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(21769L),
														BGl_string2688z00zz__evalz00,
														BGl_string2676z00zz__evalz00, BgL_xz00_50);
													FAILURE(BgL_auxz00_7146, BFALSE, BFALSE);
												}
											BgL_arg1524z00_2966 = CER(BgL_objz00_2967);
										}
										return
											BGl_everrorz00zz__everrorz00(BgL_arg1524z00_2966,
											BgL_procz00_2964, BGl_string2691z00zz__evalz00,
											BgL_xz00_50);
									}
								else
									{	/* Eval/eval.scm 546 */
										return
											BGl_errorz00zz__errorz00(BgL_procz00_2964,
											BGl_string2691z00zz__evalz00, BgL_xz00_50);
									}
							}
					}
				else
					{	/* Eval/eval.scm 595 */
						obj_t BgL_procz00_2968;

						BgL_procz00_2968 = BGl_symbol2689z00zz__evalz00;
						if (EPAIRP(BgL_xz00_50))
							{	/* Eval/eval.scm 547 */
								obj_t BgL_arg1524z00_2970;

								{	/* Eval/eval.scm 547 */
									obj_t BgL_objz00_2971;

									if (EPAIRP(BgL_xz00_50))
										{	/* Eval/eval.scm 547 */
											BgL_objz00_2971 = BgL_xz00_50;
										}
									else
										{
											obj_t BgL_auxz00_7157;

											BgL_auxz00_7157 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2618z00zz__evalz00, BINT(21769L),
												BGl_string2688z00zz__evalz00,
												BGl_string2676z00zz__evalz00, BgL_xz00_50);
											FAILURE(BgL_auxz00_7157, BFALSE, BFALSE);
										}
									BgL_arg1524z00_2970 = CER(BgL_objz00_2971);
								}
								return
									BGl_everrorz00zz__everrorz00(BgL_arg1524z00_2970,
									BgL_procz00_2968, BGl_string2691z00zz__evalz00, BgL_xz00_50);
							}
						else
							{	/* Eval/eval.scm 546 */
								return
									BGl_errorz00zz__errorz00(BgL_procz00_2968,
									BGl_string2691z00zz__evalz00, BgL_xz00_50);
							}
					}
			}
		}

	}



/* &expand-define-expander */
	obj_t BGl_z62expandzd2definezd2expanderz62zz__evalz00(obj_t BgL_envz00_3679,
		obj_t BgL_xz00_3680, obj_t BgL_ez00_3681)
	{
		{	/* Eval/eval.scm 553 */
			return
				BGl_expandzd2definezd2expanderz00zz__evalz00(BgL_xz00_3680,
				BgL_ez00_3681);
		}

	}



/* &<@anonymous:1555> */
	obj_t BGl_z62zc3z04anonymousza31555ze3ze5zz__evalz00(obj_t BgL_envz00_3682,
		obj_t BgL_xz00_3685, obj_t BgL_ez00_3686)
	{
		{	/* Eval/eval.scm 558 */
			{	/* Eval/eval.scm 559 */
				obj_t BgL_namez00_3683;
				obj_t BgL_expdzd2evalzd2_3684;

				BgL_namez00_3683 = ((obj_t) PROCEDURE_REF(BgL_envz00_3682, (int) (0L)));
				BgL_expdzd2evalzd2_3684 = PROCEDURE_REF(BgL_envz00_3682, (int) (1L));
				if (PROCEDUREP(BgL_expdzd2evalzd2_3684))
					{	/* Eval/eval.scm 559 */
						if (PROCEDURE_CORRECT_ARITYP(BgL_expdzd2evalzd2_3684, (int) (2L)))
							{	/* Eval/eval.scm 565 */
								obj_t BgL_env1118z00_4541;

								BgL_env1118z00_4541 = BGL_CURRENT_DYNAMIC_ENV();
								{
									obj_t BgL_excz00_4543;

									{	/* Eval/eval.scm 565 */
										obj_t BgL_cell1113z00_4783;

										BgL_cell1113z00_4783 = MAKE_STACK_CELL(BUNSPEC);
										{	/* Eval/eval.scm 565 */
											obj_t BgL_val1117z00_4784;

											BgL_val1117z00_4784 =
												BGl_zc3z04exitza31558ze3ze70z60zz__evalz00
												(BgL_ez00_3686, BgL_xz00_3685, BgL_expdzd2evalzd2_3684,
												BgL_cell1113z00_4783, BgL_env1118z00_4541);
											if ((BgL_val1117z00_4784 == BgL_cell1113z00_4783))
												{	/* Eval/eval.scm 565 */
													{	/* Eval/eval.scm 565 */
														int BgL_tmpz00_7180;

														BgL_tmpz00_7180 = (int) (0L);
														BGL_SIGSETMASK(BgL_tmpz00_7180);
													}
													{	/* Eval/eval.scm 565 */
														obj_t BgL_arg1557z00_4785;

														BgL_arg1557z00_4785 =
															CELL_REF(((obj_t) BgL_val1117z00_4784));
														BgL_excz00_4543 = BgL_arg1557z00_4785;
														{	/* Eval/eval.scm 567 */
															obj_t BgL_nexcz00_4544;

															{	/* Eval/eval.scm 567 */
																bool_t BgL_test3193z00_7185;

																{	/* Eval/eval.scm 567 */
																	obj_t BgL_classz00_4545;

																	BgL_classz00_4545 =
																		BGl_z62errorz62zz__objectz00;
																	if (BGL_OBJECTP(BgL_excz00_4543))
																		{	/* Eval/eval.scm 567 */
																			BgL_objectz00_bglt BgL_arg2222z00_4546;

																			BgL_arg2222z00_4546 =
																				(BgL_objectz00_bglt) (BgL_excz00_4543);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Eval/eval.scm 567 */
																					long BgL_idxz00_4547;

																					BgL_idxz00_4547 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg2222z00_4546);
																					{	/* Eval/eval.scm 567 */
																						obj_t BgL_arg2215z00_4548;

																						{	/* Eval/eval.scm 567 */
																							long BgL_arg2216z00_4549;

																							BgL_arg2216z00_4549 =
																								(BgL_idxz00_4547 + 3L);
																							{	/* Eval/eval.scm 567 */
																								obj_t BgL_vectorz00_4550;

																								{	/* Eval/eval.scm 567 */
																									obj_t BgL_aux2441z00_4551;

																									BgL_aux2441z00_4551 =
																										BGl_za2inheritancesza2z00zz__objectz00;
																									if (VECTORP
																										(BgL_aux2441z00_4551))
																										{	/* Eval/eval.scm 567 */
																											BgL_vectorz00_4550 =
																												BgL_aux2441z00_4551;
																										}
																									else
																										{
																											obj_t BgL_auxz00_7195;

																											BgL_auxz00_7195 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string2618z00zz__evalz00,
																												BINT(22481L),
																												BGl_string2692z00zz__evalz00,
																												BGl_string2628z00zz__evalz00,
																												BgL_aux2441z00_4551);
																											FAILURE(BgL_auxz00_7195,
																												BFALSE, BFALSE);
																										}
																								}
																								BgL_arg2215z00_4548 =
																									VECTOR_REF(BgL_vectorz00_4550,
																									BgL_arg2216z00_4549);
																							}
																						}
																						BgL_test3193z00_7185 =
																							(BgL_arg2215z00_4548 ==
																							BgL_classz00_4545);
																					}
																				}
																			else
																				{	/* Eval/eval.scm 567 */
																					bool_t BgL_res2239z00_4552;

																					{	/* Eval/eval.scm 567 */
																						obj_t BgL_oclassz00_4553;

																						{	/* Eval/eval.scm 567 */
																							obj_t BgL_arg2229z00_4554;
																							long BgL_arg2230z00_4555;

																							BgL_arg2229z00_4554 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Eval/eval.scm 567 */
																								long BgL_arg2231z00_4556;

																								BgL_arg2231z00_4556 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg2222z00_4546);
																								BgL_arg2230z00_4555 =
																									(BgL_arg2231z00_4556 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_4553 =
																								VECTOR_REF(BgL_arg2229z00_4554,
																								BgL_arg2230z00_4555);
																						}
																						{	/* Eval/eval.scm 567 */
																							bool_t BgL__ortest_1236z00_4557;

																							BgL__ortest_1236z00_4557 =
																								(BgL_classz00_4545 ==
																								BgL_oclassz00_4553);
																							if (BgL__ortest_1236z00_4557)
																								{	/* Eval/eval.scm 567 */
																									BgL_res2239z00_4552 =
																										BgL__ortest_1236z00_4557;
																								}
																							else
																								{	/* Eval/eval.scm 567 */
																									long BgL_odepthz00_4558;

																									{	/* Eval/eval.scm 567 */
																										obj_t BgL_arg2219z00_4559;

																										BgL_arg2219z00_4559 =
																											(BgL_oclassz00_4553);
																										BgL_odepthz00_4558 =
																											BGL_CLASS_DEPTH
																											(BgL_arg2219z00_4559);
																									}
																									if ((3L < BgL_odepthz00_4558))
																										{	/* Eval/eval.scm 567 */
																											obj_t BgL_arg2217z00_4560;

																											{	/* Eval/eval.scm 567 */
																												obj_t
																													BgL_arg2218z00_4561;
																												BgL_arg2218z00_4561 =
																													(BgL_oclassz00_4553);
																												BgL_arg2217z00_4560 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg2218z00_4561,
																													3L);
																											}
																											BgL_res2239z00_4552 =
																												(BgL_arg2217z00_4560 ==
																												BgL_classz00_4545);
																										}
																									else
																										{	/* Eval/eval.scm 567 */
																											BgL_res2239z00_4552 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test3193z00_7185 =
																						BgL_res2239z00_4552;
																				}
																		}
																	else
																		{	/* Eval/eval.scm 567 */
																			BgL_test3193z00_7185 = ((bool_t) 0);
																		}
																}
																if (BgL_test3193z00_7185)
																	{	/* Eval/eval.scm 568 */
																		BgL_z62errorz62_bglt BgL_i1119z00_4562;

																		{	/* Eval/eval.scm 569 */
																			bool_t BgL_test3199z00_7214;

																			{	/* Eval/eval.scm 569 */
																				obj_t BgL_classz00_4563;

																				BgL_classz00_4563 =
																					BGl_z62errorz62zz__objectz00;
																				if (BGL_OBJECTP(BgL_excz00_4543))
																					{	/* Eval/eval.scm 569 */
																						BgL_objectz00_bglt
																							BgL_arg2224z00_4565;
																						long BgL_arg2225z00_4566;

																						BgL_arg2224z00_4565 =
																							(BgL_objectz00_bglt)
																							(BgL_excz00_4543);
																						BgL_arg2225z00_4566 =
																							BGL_CLASS_DEPTH
																							(BgL_classz00_4563);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Eval/eval.scm 569 */
																								long BgL_idxz00_4574;

																								BgL_idxz00_4574 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg2224z00_4565);
																								{	/* Eval/eval.scm 569 */
																									obj_t BgL_arg2215z00_4575;

																									{	/* Eval/eval.scm 569 */
																										long BgL_arg2216z00_4576;

																										BgL_arg2216z00_4576 =
																											(BgL_idxz00_4574 +
																											BgL_arg2225z00_4566);
																										BgL_arg2215z00_4575 =
																											VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																											BgL_arg2216z00_4576);
																									}
																									BgL_test3199z00_7214 =
																										(BgL_arg2215z00_4575 ==
																										BgL_classz00_4563);
																							}}
																						else
																							{	/* Eval/eval.scm 569 */
																								bool_t BgL_res2244z00_4581;

																								{	/* Eval/eval.scm 569 */
																									obj_t BgL_oclassz00_4585;

																									{	/* Eval/eval.scm 569 */
																										obj_t BgL_arg2229z00_4587;
																										long BgL_arg2230z00_4588;

																										BgL_arg2229z00_4587 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Eval/eval.scm 569 */
																											long BgL_arg2231z00_4589;

																											BgL_arg2231z00_4589 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg2224z00_4565);
																											BgL_arg2230z00_4588 =
																												(BgL_arg2231z00_4589 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_4585 =
																											VECTOR_REF
																											(BgL_arg2229z00_4587,
																											BgL_arg2230z00_4588);
																									}
																									{	/* Eval/eval.scm 569 */
																										bool_t
																											BgL__ortest_1236z00_4595;
																										BgL__ortest_1236z00_4595 =
																											(BgL_classz00_4563 ==
																											BgL_oclassz00_4585);
																										if (BgL__ortest_1236z00_4595)
																											{	/* Eval/eval.scm 569 */
																												BgL_res2244z00_4581 =
																													BgL__ortest_1236z00_4595;
																											}
																										else
																											{	/* Eval/eval.scm 569 */
																												long BgL_odepthz00_4596;

																												{	/* Eval/eval.scm 569 */
																													obj_t
																														BgL_arg2219z00_4597;
																													BgL_arg2219z00_4597 =
																														(BgL_oclassz00_4585);
																													BgL_odepthz00_4596 =
																														BGL_CLASS_DEPTH
																														(BgL_arg2219z00_4597);
																												}
																												if (
																													(BgL_arg2225z00_4566 <
																														BgL_odepthz00_4596))
																													{	/* Eval/eval.scm 569 */
																														obj_t
																															BgL_arg2217z00_4601;
																														{	/* Eval/eval.scm 569 */
																															obj_t
																																BgL_arg2218z00_4602;
																															BgL_arg2218z00_4602
																																=
																																(BgL_oclassz00_4585);
																															BgL_arg2217z00_4601
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg2218z00_4602,
																																BgL_arg2225z00_4566);
																														}
																														BgL_res2244z00_4581
																															=
																															(BgL_arg2217z00_4601
																															==
																															BgL_classz00_4563);
																													}
																												else
																													{	/* Eval/eval.scm 569 */
																														BgL_res2244z00_4581
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test3199z00_7214 =
																									BgL_res2244z00_4581;
																							}
																					}
																				else
																					{	/* Eval/eval.scm 569 */
																						BgL_test3199z00_7214 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test3199z00_7214)
																				{	/* Eval/eval.scm 569 */
																					BgL_i1119z00_4562 =
																						((BgL_z62errorz62_bglt)
																						BgL_excz00_4543);
																				}
																			else
																				{
																					obj_t BgL_auxz00_7239;

																					BgL_auxz00_7239 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(22543L),
																						BGl_string2692z00zz__evalz00,
																						BGl_string2658z00zz__evalz00,
																						BgL_excz00_4543);
																					FAILURE(BgL_auxz00_7239, BFALSE,
																						BFALSE);
																				}
																		}
																		{	/* Eval/eval.scm 569 */
																			bool_t BgL_test3204z00_7243;

																			{	/* Eval/eval.scm 569 */
																				obj_t BgL_tmpz00_7244;

																				BgL_tmpz00_7244 =
																					(((BgL_z62errorz62_bglt)
																						COBJECT(BgL_i1119z00_4562))->
																					BgL_objz00);
																				BgL_test3204z00_7243 =
																					EPAIRP(BgL_tmpz00_7244);
																			}
																			if (BgL_test3204z00_7243)
																				{
																					obj_t BgL_fnamez00_4604;
																					obj_t BgL_locz00_4605;

																					{	/* Eval/eval.scm 570 */
																						obj_t BgL_ezd2120zd2_4776;

																						{	/* Eval/eval.scm 570 */
																							obj_t BgL_objz00_4777;

																							{	/* Eval/eval.scm 570 */
																								obj_t BgL_aux2456z00_4778;

																								BgL_aux2456z00_4778 =
																									(((BgL_z62errorz62_bglt)
																										COBJECT
																										(BgL_i1119z00_4562))->
																									BgL_objz00);
																								if (EPAIRP(BgL_aux2456z00_4778))
																									{	/* Eval/eval.scm 570 */
																										BgL_objz00_4777 =
																											BgL_aux2456z00_4778;
																									}
																								else
																									{
																										obj_t BgL_auxz00_7250;

																										BgL_auxz00_7250 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2618z00zz__evalz00,
																											BINT(22584L),
																											BGl_string2692z00zz__evalz00,
																											BGl_string2676z00zz__evalz00,
																											BgL_aux2456z00_4778);
																										FAILURE(BgL_auxz00_7250,
																											BFALSE, BFALSE);
																									}
																							}
																							BgL_ezd2120zd2_4776 =
																								CER(BgL_objz00_4777);
																						}
																						if (PAIRP(BgL_ezd2120zd2_4776))
																							{	/* Eval/eval.scm 570 */
																								obj_t BgL_cdrzd2126zd2_4779;

																								BgL_cdrzd2126zd2_4779 =
																									CDR(BgL_ezd2120zd2_4776);
																								if (
																									(CAR(BgL_ezd2120zd2_4776) ==
																										BGl_symbol2630z00zz__evalz00))
																									{	/* Eval/eval.scm 570 */
																										if (PAIRP
																											(BgL_cdrzd2126zd2_4779))
																											{	/* Eval/eval.scm 570 */
																												obj_t
																													BgL_cdrzd2130zd2_4780;
																												BgL_cdrzd2130zd2_4780 =
																													CDR
																													(BgL_cdrzd2126zd2_4779);
																												if (PAIRP
																													(BgL_cdrzd2130zd2_4780))
																													{	/* Eval/eval.scm 570 */
																														if (NULLP(CDR
																																(BgL_cdrzd2130zd2_4780)))
																															{	/* Eval/eval.scm 570 */
																																obj_t
																																	BgL_arg1573z00_4781;
																																obj_t
																																	BgL_arg1575z00_4782;
																																BgL_arg1573z00_4781
																																	=
																																	CAR
																																	(BgL_cdrzd2126zd2_4779);
																																BgL_arg1575z00_4782
																																	=
																																	CAR
																																	(BgL_cdrzd2130zd2_4780);
																																{
																																	BgL_z62errorz62_bglt
																																		BgL_auxz00_7271;
																																	BgL_fnamez00_4604
																																		=
																																		BgL_arg1573z00_4781;
																																	BgL_locz00_4605
																																		=
																																		BgL_arg1575z00_4782;
																																	{	/* Eval/eval.scm 572 */
																																		BgL_z62exceptionz62_bglt
																																			BgL_duplicated1122z00_4606;
																																		BgL_z62errorz62_bglt
																																			BgL_new1120z00_4607;
																																		{	/* Eval/eval.scm 573 */
																																			bool_t
																																				BgL_test3211z00_7272;
																																			{	/* Eval/eval.scm 573 */
																																				obj_t
																																					BgL_classz00_4608;
																																				BgL_classz00_4608
																																					=
																																					BGl_z62exceptionz62zz__objectz00;
																																				if (BGL_OBJECTP(BgL_excz00_4543))
																																					{	/* Eval/eval.scm 573 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_4610;
																																						long
																																							BgL_arg2225z00_4611;
																																						BgL_arg2224z00_4610
																																							=
																																							(BgL_objectz00_bglt)
																																							(BgL_excz00_4543);
																																						BgL_arg2225z00_4611
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_4608);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 573 */
																																								long
																																									BgL_idxz00_4619;
																																								BgL_idxz00_4619
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_4610);
																																								{	/* Eval/eval.scm 573 */
																																									obj_t
																																										BgL_arg2215z00_4620;
																																									{	/* Eval/eval.scm 573 */
																																										long
																																											BgL_arg2216z00_4621;
																																										BgL_arg2216z00_4621
																																											=
																																											(BgL_idxz00_4619
																																											+
																																											BgL_arg2225z00_4611);
																																										BgL_arg2215z00_4620
																																											=
																																											VECTOR_REF
																																											(BGl_za2inheritancesza2z00zz__objectz00,
																																											BgL_arg2216z00_4621);
																																									}
																																									BgL_test3211z00_7272
																																										=
																																										(BgL_arg2215z00_4620
																																										==
																																										BgL_classz00_4608);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 573 */
																																								bool_t
																																									BgL_res2244z00_4626;
																																								{	/* Eval/eval.scm 573 */
																																									obj_t
																																										BgL_oclassz00_4630;
																																									{	/* Eval/eval.scm 573 */
																																										obj_t
																																											BgL_arg2229z00_4632;
																																										long
																																											BgL_arg2230z00_4633;
																																										BgL_arg2229z00_4632
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 573 */
																																											long
																																												BgL_arg2231z00_4634;
																																											BgL_arg2231z00_4634
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_4610);
																																											BgL_arg2230z00_4633
																																												=
																																												(BgL_arg2231z00_4634
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_4630
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_4632,
																																											BgL_arg2230z00_4633);
																																									}
																																									{	/* Eval/eval.scm 573 */
																																										bool_t
																																											BgL__ortest_1236z00_4640;
																																										BgL__ortest_1236z00_4640
																																											=
																																											(BgL_classz00_4608
																																											==
																																											BgL_oclassz00_4630);
																																										if (BgL__ortest_1236z00_4640)
																																											{	/* Eval/eval.scm 573 */
																																												BgL_res2244z00_4626
																																													=
																																													BgL__ortest_1236z00_4640;
																																											}
																																										else
																																											{	/* Eval/eval.scm 573 */
																																												long
																																													BgL_odepthz00_4641;
																																												{	/* Eval/eval.scm 573 */
																																													obj_t
																																														BgL_arg2219z00_4642;
																																													BgL_arg2219z00_4642
																																														=
																																														(BgL_oclassz00_4630);
																																													BgL_odepthz00_4641
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_4642);
																																												}
																																												if ((BgL_arg2225z00_4611 < BgL_odepthz00_4641))
																																													{	/* Eval/eval.scm 573 */
																																														obj_t
																																															BgL_arg2217z00_4646;
																																														{	/* Eval/eval.scm 573 */
																																															obj_t
																																																BgL_arg2218z00_4647;
																																															BgL_arg2218z00_4647
																																																=
																																																(BgL_oclassz00_4630);
																																															BgL_arg2217z00_4646
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_4647,
																																																BgL_arg2225z00_4611);
																																														}
																																														BgL_res2244z00_4626
																																															=
																																															(BgL_arg2217z00_4646
																																															==
																																															BgL_classz00_4608);
																																													}
																																												else
																																													{	/* Eval/eval.scm 573 */
																																														BgL_res2244z00_4626
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3211z00_7272
																																									=
																																									BgL_res2244z00_4626;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 573 */
																																						BgL_test3211z00_7272
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																			if (BgL_test3211z00_7272)
																																				{	/* Eval/eval.scm 573 */
																																					BgL_duplicated1122z00_4606
																																						=
																																						(
																																						(BgL_z62exceptionz62_bglt)
																																						BgL_excz00_4543);
																																				}
																																			else
																																				{
																																					obj_t
																																						BgL_auxz00_7297;
																																					BgL_auxz00_7297
																																						=
																																						BGl_typezd2errorzd2zz__errorz00
																																						(BGl_string2618z00zz__evalz00,
																																						BINT
																																						(22664L),
																																						BGl_string2693z00zz__evalz00,
																																						BGl_string2629z00zz__evalz00,
																																						BgL_excz00_4543);
																																					FAILURE
																																						(BgL_auxz00_7297,
																																						BFALSE,
																																						BFALSE);
																																				}
																																		}
																																		{	/* Eval/eval.scm 573 */
																																			BgL_z62errorz62_bglt
																																				BgL_new1127z00_4648;
																																			BgL_new1127z00_4648
																																				=
																																				(
																																				(BgL_z62errorz62_bglt)
																																				BOBJECT
																																				(GC_MALLOC
																																					(sizeof
																																						(struct
																																							BgL_z62errorz62_bgl))));
																																			{	/* Eval/eval.scm 573 */
																																				long
																																					BgL_arg1580z00_4649;
																																				BgL_arg1580z00_4649
																																					=
																																					BGL_CLASS_NUM
																																					(BGl_z62errorz62zz__objectz00);
																																				BGL_OBJECT_CLASS_NUM_SET
																																					(((BgL_objectz00_bglt) BgL_new1127z00_4648), BgL_arg1580z00_4649);
																																			}
																																			BgL_new1120z00_4607
																																				=
																																				BgL_new1127z00_4648;
																																		}
																																		((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1120z00_4607)))->BgL_fnamez00) = ((obj_t) BgL_fnamez00_4604), BUNSPEC);
																																		((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1120z00_4607)))->BgL_locationz00) = ((obj_t) BgL_locz00_4605), BUNSPEC);
																																		((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1120z00_4607)))->BgL_stackz00) = ((obj_t) (((BgL_z62exceptionz62_bglt) COBJECT(BgL_duplicated1122z00_4606))->BgL_stackz00)), BUNSPEC);
																																		{
																																			obj_t
																																				BgL_auxz00_7312;
																																			{	/* Eval/eval.scm 574 */
																																				BgL_z62errorz62_bglt
																																					BgL_tmp1123z00_4650;
																																				{	/* Eval/eval.scm 574 */
																																					bool_t
																																						BgL_test3216z00_7313;
																																					{	/* Eval/eval.scm 574 */
																																						obj_t
																																							BgL_classz00_4652;
																																						BgL_classz00_4652
																																							=
																																							BGl_z62errorz62zz__objectz00;
																																						{	/* Eval/eval.scm 574 */
																																							bool_t
																																								BgL_test3217z00_7314;
																																							{	/* Eval/eval.scm 574 */
																																								obj_t
																																									BgL_tmpz00_7315;
																																								BgL_tmpz00_7315
																																									=
																																									(
																																									(obj_t)
																																									BgL_duplicated1122z00_4606);
																																								BgL_test3217z00_7314
																																									=
																																									BGL_OBJECTP
																																									(BgL_tmpz00_7315);
																																							}
																																							if (BgL_test3217z00_7314)
																																								{	/* Eval/eval.scm 574 */
																																									BgL_objectz00_bglt
																																										BgL_arg2224z00_4654;
																																									long
																																										BgL_arg2225z00_4655;
																																									{	/* Eval/eval.scm 574 */
																																										obj_t
																																											BgL_tmpz00_7318;
																																										BgL_tmpz00_7318
																																											=
																																											(
																																											(obj_t)
																																											BgL_duplicated1122z00_4606);
																																										BgL_arg2224z00_4654
																																											=
																																											(BgL_objectz00_bglt)
																																											(BgL_tmpz00_7318);
																																									}
																																									BgL_arg2225z00_4655
																																										=
																																										BGL_CLASS_DEPTH
																																										(BgL_classz00_4652);
																																									if (BGL_CONDEXPAND_ISA_ARCH64())
																																										{	/* Eval/eval.scm 574 */
																																											long
																																												BgL_idxz00_4663;
																																											BgL_idxz00_4663
																																												=
																																												BGL_OBJECT_INHERITANCE_NUM
																																												(BgL_arg2224z00_4654);
																																											{	/* Eval/eval.scm 574 */
																																												obj_t
																																													BgL_arg2215z00_4664;
																																												{	/* Eval/eval.scm 574 */
																																													long
																																														BgL_arg2216z00_4665;
																																													BgL_arg2216z00_4665
																																														=
																																														(BgL_idxz00_4663
																																														+
																																														BgL_arg2225z00_4655);
																																													{	/* Eval/eval.scm 574 */
																																														obj_t
																																															BgL_vectorz00_4668;
																																														BgL_vectorz00_4668
																																															=
																																															BGl_za2inheritancesza2z00zz__objectz00;
																																														BgL_arg2215z00_4664
																																															=
																																															VECTOR_REF
																																															(BgL_vectorz00_4668,
																																															BgL_arg2216z00_4665);
																																												}}
																																												BgL_test3216z00_7313
																																													=
																																													(BgL_arg2215z00_4664
																																													==
																																													BgL_classz00_4652);
																																										}}
																																									else
																																										{	/* Eval/eval.scm 574 */
																																											bool_t
																																												BgL_res2244z00_4670;
																																											{	/* Eval/eval.scm 574 */
																																												obj_t
																																													BgL_oclassz00_4674;
																																												{	/* Eval/eval.scm 574 */
																																													obj_t
																																														BgL_arg2229z00_4676;
																																													long
																																														BgL_arg2230z00_4677;
																																													BgL_arg2229z00_4676
																																														=
																																														(BGl_za2classesza2z00zz__objectz00);
																																													{	/* Eval/eval.scm 574 */
																																														long
																																															BgL_arg2231z00_4678;
																																														BgL_arg2231z00_4678
																																															=
																																															BGL_OBJECT_CLASS_NUM
																																															(BgL_arg2224z00_4654);
																																														BgL_arg2230z00_4677
																																															=
																																															(BgL_arg2231z00_4678
																																															-
																																															OBJECT_TYPE);
																																													}
																																													BgL_oclassz00_4674
																																														=
																																														VECTOR_REF
																																														(BgL_arg2229z00_4676,
																																														BgL_arg2230z00_4677);
																																												}
																																												{	/* Eval/eval.scm 574 */
																																													bool_t
																																														BgL__ortest_1236z00_4684;
																																													BgL__ortest_1236z00_4684
																																														=
																																														(BgL_classz00_4652
																																														==
																																														BgL_oclassz00_4674);
																																													if (BgL__ortest_1236z00_4684)
																																														{	/* Eval/eval.scm 574 */
																																															BgL_res2244z00_4670
																																																=
																																																BgL__ortest_1236z00_4684;
																																														}
																																													else
																																														{	/* Eval/eval.scm 574 */
																																															long
																																																BgL_odepthz00_4685;
																																															{	/* Eval/eval.scm 574 */
																																																obj_t
																																																	BgL_arg2219z00_4686;
																																																BgL_arg2219z00_4686
																																																	=
																																																	(BgL_oclassz00_4674);
																																																BgL_odepthz00_4685
																																																	=
																																																	BGL_CLASS_DEPTH
																																																	(BgL_arg2219z00_4686);
																																															}
																																															if ((BgL_arg2225z00_4655 < BgL_odepthz00_4685))
																																																{	/* Eval/eval.scm 574 */
																																																	obj_t
																																																		BgL_arg2217z00_4690;
																																																	{	/* Eval/eval.scm 574 */
																																																		obj_t
																																																			BgL_arg2218z00_4691;
																																																		BgL_arg2218z00_4691
																																																			=
																																																			(BgL_oclassz00_4674);
																																																		BgL_arg2217z00_4690
																																																			=
																																																			BGL_CLASS_ANCESTORS_REF
																																																			(BgL_arg2218z00_4691,
																																																			BgL_arg2225z00_4655);
																																																	}
																																																	BgL_res2244z00_4670
																																																		=
																																																		(BgL_arg2217z00_4690
																																																		==
																																																		BgL_classz00_4652);
																																																}
																																															else
																																																{	/* Eval/eval.scm 574 */
																																																	BgL_res2244z00_4670
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																												}
																																											}
																																											BgL_test3216z00_7313
																																												=
																																												BgL_res2244z00_4670;
																																										}
																																								}
																																							else
																																								{	/* Eval/eval.scm 574 */
																																									BgL_test3216z00_7313
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					}
																																					if (BgL_test3216z00_7313)
																																						{	/* Eval/eval.scm 574 */
																																							BgL_tmp1123z00_4650
																																								=
																																								(
																																								(BgL_z62errorz62_bglt)
																																								BgL_duplicated1122z00_4606);
																																						}
																																					else
																																						{
																																							obj_t
																																								BgL_auxz00_7342;
																																							BgL_auxz00_7342
																																								=
																																								BGl_typezd2errorzd2zz__errorz00
																																								(BGl_string2618z00zz__evalz00,
																																								BINT
																																								(22690L),
																																								BGl_string2693z00zz__evalz00,
																																								BGl_string2658z00zz__evalz00,
																																								((obj_t) BgL_duplicated1122z00_4606));
																																							FAILURE
																																								(BgL_auxz00_7342,
																																								BFALSE,
																																								BFALSE);
																																						}
																																				}
																																				BgL_auxz00_7312
																																					=
																																					(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1123z00_4650))->BgL_procz00);
																																			}
																																			((((BgL_z62errorz62_bglt) COBJECT(BgL_new1120z00_4607))->BgL_procz00) = ((obj_t) BgL_auxz00_7312), BUNSPEC);
																																		}
																																		{
																																			obj_t
																																				BgL_auxz00_7349;
																																			{	/* Eval/eval.scm 574 */
																																				BgL_z62errorz62_bglt
																																					BgL_tmp1124z00_4692;
																																				{	/* Eval/eval.scm 574 */
																																					bool_t
																																						BgL_test3221z00_7350;
																																					{	/* Eval/eval.scm 574 */
																																						obj_t
																																							BgL_classz00_4694;
																																						BgL_classz00_4694
																																							=
																																							BGl_z62errorz62zz__objectz00;
																																						{	/* Eval/eval.scm 574 */
																																							bool_t
																																								BgL_test3222z00_7351;
																																							{	/* Eval/eval.scm 574 */
																																								obj_t
																																									BgL_tmpz00_7352;
																																								BgL_tmpz00_7352
																																									=
																																									(
																																									(obj_t)
																																									BgL_duplicated1122z00_4606);
																																								BgL_test3222z00_7351
																																									=
																																									BGL_OBJECTP
																																									(BgL_tmpz00_7352);
																																							}
																																							if (BgL_test3222z00_7351)
																																								{	/* Eval/eval.scm 574 */
																																									BgL_objectz00_bglt
																																										BgL_arg2224z00_4696;
																																									long
																																										BgL_arg2225z00_4697;
																																									{	/* Eval/eval.scm 574 */
																																										obj_t
																																											BgL_tmpz00_7355;
																																										BgL_tmpz00_7355
																																											=
																																											(
																																											(obj_t)
																																											BgL_duplicated1122z00_4606);
																																										BgL_arg2224z00_4696
																																											=
																																											(BgL_objectz00_bglt)
																																											(BgL_tmpz00_7355);
																																									}
																																									BgL_arg2225z00_4697
																																										=
																																										BGL_CLASS_DEPTH
																																										(BgL_classz00_4694);
																																									if (BGL_CONDEXPAND_ISA_ARCH64())
																																										{	/* Eval/eval.scm 574 */
																																											long
																																												BgL_idxz00_4705;
																																											BgL_idxz00_4705
																																												=
																																												BGL_OBJECT_INHERITANCE_NUM
																																												(BgL_arg2224z00_4696);
																																											{	/* Eval/eval.scm 574 */
																																												obj_t
																																													BgL_arg2215z00_4706;
																																												{	/* Eval/eval.scm 574 */
																																													long
																																														BgL_arg2216z00_4707;
																																													BgL_arg2216z00_4707
																																														=
																																														(BgL_idxz00_4705
																																														+
																																														BgL_arg2225z00_4697);
																																													{	/* Eval/eval.scm 574 */
																																														obj_t
																																															BgL_vectorz00_4710;
																																														BgL_vectorz00_4710
																																															=
																																															BGl_za2inheritancesza2z00zz__objectz00;
																																														BgL_arg2215z00_4706
																																															=
																																															VECTOR_REF
																																															(BgL_vectorz00_4710,
																																															BgL_arg2216z00_4707);
																																												}}
																																												BgL_test3221z00_7350
																																													=
																																													(BgL_arg2215z00_4706
																																													==
																																													BgL_classz00_4694);
																																										}}
																																									else
																																										{	/* Eval/eval.scm 574 */
																																											bool_t
																																												BgL_res2244z00_4712;
																																											{	/* Eval/eval.scm 574 */
																																												obj_t
																																													BgL_oclassz00_4716;
																																												{	/* Eval/eval.scm 574 */
																																													obj_t
																																														BgL_arg2229z00_4718;
																																													long
																																														BgL_arg2230z00_4719;
																																													BgL_arg2229z00_4718
																																														=
																																														(BGl_za2classesza2z00zz__objectz00);
																																													{	/* Eval/eval.scm 574 */
																																														long
																																															BgL_arg2231z00_4720;
																																														BgL_arg2231z00_4720
																																															=
																																															BGL_OBJECT_CLASS_NUM
																																															(BgL_arg2224z00_4696);
																																														BgL_arg2230z00_4719
																																															=
																																															(BgL_arg2231z00_4720
																																															-
																																															OBJECT_TYPE);
																																													}
																																													BgL_oclassz00_4716
																																														=
																																														VECTOR_REF
																																														(BgL_arg2229z00_4718,
																																														BgL_arg2230z00_4719);
																																												}
																																												{	/* Eval/eval.scm 574 */
																																													bool_t
																																														BgL__ortest_1236z00_4726;
																																													BgL__ortest_1236z00_4726
																																														=
																																														(BgL_classz00_4694
																																														==
																																														BgL_oclassz00_4716);
																																													if (BgL__ortest_1236z00_4726)
																																														{	/* Eval/eval.scm 574 */
																																															BgL_res2244z00_4712
																																																=
																																																BgL__ortest_1236z00_4726;
																																														}
																																													else
																																														{	/* Eval/eval.scm 574 */
																																															long
																																																BgL_odepthz00_4727;
																																															{	/* Eval/eval.scm 574 */
																																																obj_t
																																																	BgL_arg2219z00_4728;
																																																BgL_arg2219z00_4728
																																																	=
																																																	(BgL_oclassz00_4716);
																																																BgL_odepthz00_4727
																																																	=
																																																	BGL_CLASS_DEPTH
																																																	(BgL_arg2219z00_4728);
																																															}
																																															if ((BgL_arg2225z00_4697 < BgL_odepthz00_4727))
																																																{	/* Eval/eval.scm 574 */
																																																	obj_t
																																																		BgL_arg2217z00_4732;
																																																	{	/* Eval/eval.scm 574 */
																																																		obj_t
																																																			BgL_arg2218z00_4733;
																																																		BgL_arg2218z00_4733
																																																			=
																																																			(BgL_oclassz00_4716);
																																																		BgL_arg2217z00_4732
																																																			=
																																																			BGL_CLASS_ANCESTORS_REF
																																																			(BgL_arg2218z00_4733,
																																																			BgL_arg2225z00_4697);
																																																	}
																																																	BgL_res2244z00_4712
																																																		=
																																																		(BgL_arg2217z00_4732
																																																		==
																																																		BgL_classz00_4694);
																																																}
																																															else
																																																{	/* Eval/eval.scm 574 */
																																																	BgL_res2244z00_4712
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																												}
																																											}
																																											BgL_test3221z00_7350
																																												=
																																												BgL_res2244z00_4712;
																																										}
																																								}
																																							else
																																								{	/* Eval/eval.scm 574 */
																																									BgL_test3221z00_7350
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					}
																																					if (BgL_test3221z00_7350)
																																						{	/* Eval/eval.scm 574 */
																																							BgL_tmp1124z00_4692
																																								=
																																								(
																																								(BgL_z62errorz62_bglt)
																																								BgL_duplicated1122z00_4606);
																																						}
																																					else
																																						{
																																							obj_t
																																								BgL_auxz00_7379;
																																							BgL_auxz00_7379
																																								=
																																								BGl_typezd2errorzd2zz__errorz00
																																								(BGl_string2618z00zz__evalz00,
																																								BINT
																																								(22690L),
																																								BGl_string2693z00zz__evalz00,
																																								BGl_string2658z00zz__evalz00,
																																								((obj_t) BgL_duplicated1122z00_4606));
																																							FAILURE
																																								(BgL_auxz00_7379,
																																								BFALSE,
																																								BFALSE);
																																						}
																																				}
																																				BgL_auxz00_7349
																																					=
																																					(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1124z00_4692))->BgL_msgz00);
																																			}
																																			((((BgL_z62errorz62_bglt) COBJECT(BgL_new1120z00_4607))->BgL_msgz00) = ((obj_t) BgL_auxz00_7349), BUNSPEC);
																																		}
																																		{
																																			obj_t
																																				BgL_auxz00_7386;
																																			{	/* Eval/eval.scm 574 */
																																				BgL_z62errorz62_bglt
																																					BgL_tmp1125z00_4734;
																																				{	/* Eval/eval.scm 574 */
																																					bool_t
																																						BgL_test3226z00_7387;
																																					{	/* Eval/eval.scm 574 */
																																						obj_t
																																							BgL_classz00_4736;
																																						BgL_classz00_4736
																																							=
																																							BGl_z62errorz62zz__objectz00;
																																						{	/* Eval/eval.scm 574 */
																																							bool_t
																																								BgL_test3227z00_7388;
																																							{	/* Eval/eval.scm 574 */
																																								obj_t
																																									BgL_tmpz00_7389;
																																								BgL_tmpz00_7389
																																									=
																																									(
																																									(obj_t)
																																									BgL_duplicated1122z00_4606);
																																								BgL_test3227z00_7388
																																									=
																																									BGL_OBJECTP
																																									(BgL_tmpz00_7389);
																																							}
																																							if (BgL_test3227z00_7388)
																																								{	/* Eval/eval.scm 574 */
																																									BgL_objectz00_bglt
																																										BgL_arg2224z00_4738;
																																									long
																																										BgL_arg2225z00_4739;
																																									{	/* Eval/eval.scm 574 */
																																										obj_t
																																											BgL_tmpz00_7392;
																																										BgL_tmpz00_7392
																																											=
																																											(
																																											(obj_t)
																																											BgL_duplicated1122z00_4606);
																																										BgL_arg2224z00_4738
																																											=
																																											(BgL_objectz00_bglt)
																																											(BgL_tmpz00_7392);
																																									}
																																									BgL_arg2225z00_4739
																																										=
																																										BGL_CLASS_DEPTH
																																										(BgL_classz00_4736);
																																									if (BGL_CONDEXPAND_ISA_ARCH64())
																																										{	/* Eval/eval.scm 574 */
																																											long
																																												BgL_idxz00_4747;
																																											BgL_idxz00_4747
																																												=
																																												BGL_OBJECT_INHERITANCE_NUM
																																												(BgL_arg2224z00_4738);
																																											{	/* Eval/eval.scm 574 */
																																												obj_t
																																													BgL_arg2215z00_4748;
																																												{	/* Eval/eval.scm 574 */
																																													long
																																														BgL_arg2216z00_4749;
																																													BgL_arg2216z00_4749
																																														=
																																														(BgL_idxz00_4747
																																														+
																																														BgL_arg2225z00_4739);
																																													{	/* Eval/eval.scm 574 */
																																														obj_t
																																															BgL_vectorz00_4752;
																																														BgL_vectorz00_4752
																																															=
																																															BGl_za2inheritancesza2z00zz__objectz00;
																																														BgL_arg2215z00_4748
																																															=
																																															VECTOR_REF
																																															(BgL_vectorz00_4752,
																																															BgL_arg2216z00_4749);
																																												}}
																																												BgL_test3226z00_7387
																																													=
																																													(BgL_arg2215z00_4748
																																													==
																																													BgL_classz00_4736);
																																										}}
																																									else
																																										{	/* Eval/eval.scm 574 */
																																											bool_t
																																												BgL_res2244z00_4754;
																																											{	/* Eval/eval.scm 574 */
																																												obj_t
																																													BgL_oclassz00_4758;
																																												{	/* Eval/eval.scm 574 */
																																													obj_t
																																														BgL_arg2229z00_4760;
																																													long
																																														BgL_arg2230z00_4761;
																																													BgL_arg2229z00_4760
																																														=
																																														(BGl_za2classesza2z00zz__objectz00);
																																													{	/* Eval/eval.scm 574 */
																																														long
																																															BgL_arg2231z00_4762;
																																														BgL_arg2231z00_4762
																																															=
																																															BGL_OBJECT_CLASS_NUM
																																															(BgL_arg2224z00_4738);
																																														BgL_arg2230z00_4761
																																															=
																																															(BgL_arg2231z00_4762
																																															-
																																															OBJECT_TYPE);
																																													}
																																													BgL_oclassz00_4758
																																														=
																																														VECTOR_REF
																																														(BgL_arg2229z00_4760,
																																														BgL_arg2230z00_4761);
																																												}
																																												{	/* Eval/eval.scm 574 */
																																													bool_t
																																														BgL__ortest_1236z00_4768;
																																													BgL__ortest_1236z00_4768
																																														=
																																														(BgL_classz00_4736
																																														==
																																														BgL_oclassz00_4758);
																																													if (BgL__ortest_1236z00_4768)
																																														{	/* Eval/eval.scm 574 */
																																															BgL_res2244z00_4754
																																																=
																																																BgL__ortest_1236z00_4768;
																																														}
																																													else
																																														{	/* Eval/eval.scm 574 */
																																															long
																																																BgL_odepthz00_4769;
																																															{	/* Eval/eval.scm 574 */
																																																obj_t
																																																	BgL_arg2219z00_4770;
																																																BgL_arg2219z00_4770
																																																	=
																																																	(BgL_oclassz00_4758);
																																																BgL_odepthz00_4769
																																																	=
																																																	BGL_CLASS_DEPTH
																																																	(BgL_arg2219z00_4770);
																																															}
																																															if ((BgL_arg2225z00_4739 < BgL_odepthz00_4769))
																																																{	/* Eval/eval.scm 574 */
																																																	obj_t
																																																		BgL_arg2217z00_4774;
																																																	{	/* Eval/eval.scm 574 */
																																																		obj_t
																																																			BgL_arg2218z00_4775;
																																																		BgL_arg2218z00_4775
																																																			=
																																																			(BgL_oclassz00_4758);
																																																		BgL_arg2217z00_4774
																																																			=
																																																			BGL_CLASS_ANCESTORS_REF
																																																			(BgL_arg2218z00_4775,
																																																			BgL_arg2225z00_4739);
																																																	}
																																																	BgL_res2244z00_4754
																																																		=
																																																		(BgL_arg2217z00_4774
																																																		==
																																																		BgL_classz00_4736);
																																																}
																																															else
																																																{	/* Eval/eval.scm 574 */
																																																	BgL_res2244z00_4754
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																												}
																																											}
																																											BgL_test3226z00_7387
																																												=
																																												BgL_res2244z00_4754;
																																										}
																																								}
																																							else
																																								{	/* Eval/eval.scm 574 */
																																									BgL_test3226z00_7387
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					}
																																					if (BgL_test3226z00_7387)
																																						{	/* Eval/eval.scm 574 */
																																							BgL_tmp1125z00_4734
																																								=
																																								(
																																								(BgL_z62errorz62_bglt)
																																								BgL_duplicated1122z00_4606);
																																						}
																																					else
																																						{
																																							obj_t
																																								BgL_auxz00_7416;
																																							BgL_auxz00_7416
																																								=
																																								BGl_typezd2errorzd2zz__errorz00
																																								(BGl_string2618z00zz__evalz00,
																																								BINT
																																								(22690L),
																																								BGl_string2693z00zz__evalz00,
																																								BGl_string2658z00zz__evalz00,
																																								((obj_t) BgL_duplicated1122z00_4606));
																																							FAILURE
																																								(BgL_auxz00_7416,
																																								BFALSE,
																																								BFALSE);
																																						}
																																				}
																																				BgL_auxz00_7386
																																					=
																																					(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1125z00_4734))->BgL_objz00);
																																			}
																																			((((BgL_z62errorz62_bglt) COBJECT(BgL_new1120z00_4607))->BgL_objz00) = ((obj_t) BgL_auxz00_7386), BUNSPEC);
																																		}
																																		BgL_auxz00_7271
																																			=
																																			BgL_new1120z00_4607;
																																	}
																																	BgL_nexcz00_4544
																																		=
																																		((obj_t)
																																		BgL_auxz00_7271);
																																}
																															}
																														else
																															{	/* Eval/eval.scm 570 */
																																BgL_nexcz00_4544
																																	=
																																	BgL_excz00_4543;
																															}
																													}
																												else
																													{	/* Eval/eval.scm 570 */
																														BgL_nexcz00_4544 =
																															BgL_excz00_4543;
																													}
																											}
																										else
																											{	/* Eval/eval.scm 570 */
																												BgL_nexcz00_4544 =
																													BgL_excz00_4543;
																											}
																									}
																								else
																									{	/* Eval/eval.scm 570 */
																										BgL_nexcz00_4544 =
																											BgL_excz00_4543;
																									}
																							}
																						else
																							{	/* Eval/eval.scm 570 */
																								BgL_nexcz00_4544 =
																									BgL_excz00_4543;
																							}
																					}
																				}
																			else
																				{	/* Eval/eval.scm 569 */
																					BgL_nexcz00_4544 = BgL_excz00_4543;
																				}
																		}
																	}
																else
																	{	/* Eval/eval.scm 567 */
																		BgL_nexcz00_4544 = BgL_excz00_4543;
																	}
															}
															return BGl_raisez00zz__errorz00(BgL_nexcz00_4544);
														}
													}
												}
											else
												{	/* Eval/eval.scm 565 */
													return BgL_val1117z00_4784;
												}
										}
									}
								}
							}
						else
							{	/* Eval/eval.scm 561 */
								if (EPAIRP(BgL_xz00_3685))
									{	/* Eval/eval.scm 547 */
										obj_t BgL_arg1524z00_4786;

										{	/* Eval/eval.scm 547 */
											obj_t BgL_objz00_4787;

											if (EPAIRP(BgL_xz00_3685))
												{	/* Eval/eval.scm 547 */
													BgL_objz00_4787 = BgL_xz00_3685;
												}
											else
												{
													obj_t BgL_auxz00_7429;

													BgL_auxz00_7429 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(21769L),
														BGl_string2694z00zz__evalz00,
														BGl_string2676z00zz__evalz00, BgL_xz00_3685);
													FAILURE(BgL_auxz00_7429, BFALSE, BFALSE);
												}
											BgL_arg1524z00_4786 = CER(BgL_objz00_4787);
										}
										return
											BGl_everrorz00zz__everrorz00(BgL_arg1524z00_4786,
											BgL_namez00_3683, BGl_string2695z00zz__evalz00,
											BgL_xz00_3685);
									}
								else
									{	/* Eval/eval.scm 546 */
										return
											BGl_errorz00zz__errorz00(BgL_namez00_3683,
											BGl_string2695z00zz__evalz00, BgL_xz00_3685);
									}
							}
					}
				else
					{	/* Eval/eval.scm 559 */
						if (EPAIRP(BgL_xz00_3685))
							{	/* Eval/eval.scm 547 */
								obj_t BgL_arg1524z00_4788;

								{	/* Eval/eval.scm 547 */
									obj_t BgL_objz00_4789;

									if (EPAIRP(BgL_xz00_3685))
										{	/* Eval/eval.scm 547 */
											BgL_objz00_4789 = BgL_xz00_3685;
										}
									else
										{
											obj_t BgL_auxz00_7440;

											BgL_auxz00_7440 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2618z00zz__evalz00, BINT(21769L),
												BGl_string2694z00zz__evalz00,
												BGl_string2676z00zz__evalz00, BgL_xz00_3685);
											FAILURE(BgL_auxz00_7440, BFALSE, BFALSE);
										}
									BgL_arg1524z00_4788 = CER(BgL_objz00_4789);
								}
								return
									BGl_everrorz00zz__everrorz00(BgL_arg1524z00_4788,
									BgL_namez00_3683, BGl_string2696z00zz__evalz00,
									BgL_xz00_3685);
							}
						else
							{	/* Eval/eval.scm 546 */
								return
									BGl_errorz00zz__errorz00(BgL_namez00_3683,
									BGl_string2696z00zz__evalz00, BgL_xz00_3685);
							}
					}
			}
		}

	}



/* <@exit:1558>~0 */
	obj_t BGl_zc3z04exitza31558ze3ze70z60zz__evalz00(obj_t BgL_ez00_3748,
		obj_t BgL_xz00_3747, obj_t BgL_expdzd2evalzd2_3746,
		obj_t BgL_cell1113z00_3745, obj_t BgL_env1118z00_3744)
	{
		{	/* Eval/eval.scm 565 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1131z00_1649;

			if (SET_EXIT(BgL_an_exit1131z00_1649))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1131z00_1649 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1118z00_3744, BgL_an_exit1131z00_1649, 1L);
					{	/* Eval/eval.scm 565 */
						obj_t BgL_escape1114z00_1650;

						BgL_escape1114z00_1650 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1118z00_3744);
						{	/* Eval/eval.scm 565 */
							obj_t BgL_res1134z00_1651;

							{	/* Eval/eval.scm 565 */
								obj_t BgL_ohs1110z00_1652;

								BgL_ohs1110z00_1652 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1118z00_3744);
								{	/* Eval/eval.scm 565 */
									obj_t BgL_hds1111z00_1653;

									BgL_hds1111z00_1653 =
										MAKE_STACK_PAIR(BgL_escape1114z00_1650,
										BgL_cell1113z00_3745);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1118z00_3744,
										BgL_hds1111z00_1653);
									BUNSPEC;
									{	/* Eval/eval.scm 565 */
										obj_t BgL_exitd1128z00_1654;

										BgL_exitd1128z00_1654 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1118z00_3744);
										{	/* Eval/eval.scm 565 */
											obj_t BgL_tmp1130z00_1655;

											{	/* Eval/eval.scm 565 */
												obj_t BgL_arg1559z00_1657;

												BgL_arg1559z00_1657 =
													BGL_EXITD_PROTECT(BgL_exitd1128z00_1654);
												BgL_tmp1130z00_1655 =
													MAKE_YOUNG_PAIR(BgL_ohs1110z00_1652,
													BgL_arg1559z00_1657);
											}
											{	/* Eval/eval.scm 565 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1128z00_1654,
													BgL_tmp1130z00_1655);
												BUNSPEC;
												{	/* Eval/eval.scm 580 */
													obj_t BgL_tmp1129z00_1656;

													{	/* Eval/eval.scm 580 */
														obj_t BgL_tmpfunz00_7462;

														{	/* Eval/eval.scm 580 */
															bool_t BgL_test2463z00_3989;

															BgL_test2463z00_3989 =
																PROCEDUREP(BgL_expdzd2evalzd2_3746);
															if (BgL_test2463z00_3989)
																{	/* Eval/eval.scm 580 */
																	BgL_tmpfunz00_7462 = BgL_expdzd2evalzd2_3746;
																}
															else
																{
																	obj_t BgL_auxz00_7465;

																	BgL_auxz00_7465 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(22794L),
																		BGl_string2697z00zz__evalz00,
																		BGl_string2620z00zz__evalz00,
																		BgL_expdzd2evalzd2_3746);
																	FAILURE(BgL_auxz00_7465, BFALSE, BFALSE);
																}
														}
														BgL_tmp1129z00_1656 =
															(VA_PROCEDUREP(BgL_tmpfunz00_7462)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7462))
															(BgL_expdzd2evalzd2_3746, BgL_xz00_3747,
																BgL_ez00_3748, BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7462))
															(BgL_expdzd2evalzd2_3746, BgL_xz00_3747,
																BgL_ez00_3748));
													}
													{	/* Eval/eval.scm 565 */
														bool_t BgL_test3236z00_7469;

														{	/* Eval/eval.scm 565 */
															obj_t BgL_arg2210z00_2857;

															BgL_arg2210z00_2857 =
																BGL_EXITD_PROTECT(BgL_exitd1128z00_1654);
															BgL_test3236z00_7469 = PAIRP(BgL_arg2210z00_2857);
														}
														if (BgL_test3236z00_7469)
															{	/* Eval/eval.scm 565 */
																obj_t BgL_arg2208z00_2858;

																{	/* Eval/eval.scm 565 */
																	obj_t BgL_arg2209z00_2859;

																	BgL_arg2209z00_2859 =
																		BGL_EXITD_PROTECT(BgL_exitd1128z00_1654);
																	{	/* Eval/eval.scm 565 */
																		obj_t BgL_pairz00_2860;

																		if (PAIRP(BgL_arg2209z00_2859))
																			{	/* Eval/eval.scm 565 */
																				BgL_pairz00_2860 = BgL_arg2209z00_2859;
																			}
																		else
																			{
																				obj_t BgL_auxz00_7475;

																				BgL_auxz00_7475 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(22427L),
																					BGl_string2697z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_arg2209z00_2859);
																				FAILURE(BgL_auxz00_7475, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2208z00_2858 = CDR(BgL_pairz00_2860);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1128z00_1654,
																	BgL_arg2208z00_2858);
																BUNSPEC;
															}
														else
															{	/* Eval/eval.scm 565 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1118z00_3744,
														BgL_ohs1110z00_1652);
													BUNSPEC;
													BgL_res1134z00_1651 = BgL_tmp1129z00_1656;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1118z00_3744);
							return BgL_res1134z00_1651;
						}
					}
				}
		}

	}



/* expand-define-macro */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2definezd2macroz00zz__evalz00(obj_t
		BgL_xz00_52, obj_t BgL_ez00_53)
	{
		{	/* Eval/eval.scm 602 */
			{
				obj_t BgL_namez00_1695;
				obj_t BgL_argsz00_1696;
				obj_t BgL_bodyz00_1697;

				if (PAIRP(BgL_xz00_52))
					{	/* Eval/eval.scm 603 */
						obj_t BgL_cdrzd2268zd2_1702;

						BgL_cdrzd2268zd2_1702 = CDR(((obj_t) BgL_xz00_52));
						if (PAIRP(BgL_cdrzd2268zd2_1702))
							{	/* Eval/eval.scm 603 */
								obj_t BgL_carzd2272zd2_1704;

								BgL_carzd2272zd2_1704 = CAR(BgL_cdrzd2268zd2_1702);
								if (PAIRP(BgL_carzd2272zd2_1704))
									{	/* Eval/eval.scm 603 */
										BgL_namez00_1695 = CAR(BgL_carzd2272zd2_1704);
										BgL_argsz00_1696 = CDR(BgL_carzd2272zd2_1704);
										BgL_bodyz00_1697 = CDR(BgL_cdrzd2268zd2_1702);
									BgL_tagzd2254zd2_1698:
										{	/* Eval/eval.scm 606 */
											obj_t BgL_fnamez00_1725;
											obj_t BgL_locz00_1726;

											{	/* Eval/eval.scm 606 */

												{	/* Eval/eval.scm 606 */

													BgL_fnamez00_1725 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
												}
											}
											{	/* Eval/eval.scm 607 */

												{	/* Eval/eval.scm 607 */

													BgL_locz00_1726 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
												}
											}
											{	/* Eval/eval.scm 610 */
												obj_t BgL_arg1607z00_1727;

												{	/* Eval/eval.scm 610 */
													obj_t BgL_evexpdz00_1728;

													{	/* Eval/eval.scm 610 */
														obj_t BgL_arg1631z00_1786;

														{	/* Eval/eval.scm 610 */
															obj_t BgL_arg1634z00_1787;
															obj_t BgL_arg1636z00_1788;

															{	/* Eval/eval.scm 610 */
																obj_t BgL_arg1637z00_1789;

																BgL_arg1637z00_1789 =
																	MAKE_YOUNG_PAIR(BGl_symbol2698z00zz__evalz00,
																	BNIL);
																BgL_arg1634z00_1787 =
																	MAKE_YOUNG_PAIR(BGl_symbol2700z00zz__evalz00,
																	BgL_arg1637z00_1789);
															}
															{	/* Eval/eval.scm 611 */
																obj_t BgL_arg1638z00_1790;

																{	/* Eval/eval.scm 611 */
																	obj_t BgL_arg1639z00_1791;

																	{	/* Eval/eval.scm 611 */
																		obj_t BgL_arg1640z00_1792;
																		obj_t BgL_arg1641z00_1793;

																		{	/* Eval/eval.scm 611 */
																			obj_t BgL_arg1642z00_1794;
																			obj_t BgL_arg1643z00_1795;

																			{	/* Eval/eval.scm 611 */
																				obj_t BgL_arg1644z00_1796;

																				BgL_arg1644z00_1796 =
																					MAKE_YOUNG_PAIR(BFALSE, BNIL);
																				BgL_arg1642z00_1794 =
																					MAKE_YOUNG_PAIR(BgL_fnamez00_1725,
																					BgL_arg1644z00_1796);
																			}
																			BgL_arg1643z00_1795 =
																				MAKE_YOUNG_PAIR(BgL_locz00_1726, BNIL);
																			BgL_arg1640z00_1792 =
																				MAKE_YOUNG_PAIR(BgL_arg1642z00_1794,
																				BgL_arg1643z00_1795);
																		}
																		{	/* Eval/eval.scm 612 */
																			obj_t BgL_arg1645z00_1797;
																			obj_t BgL_arg1646z00_1798;

																			{	/* Eval/eval.scm 612 */
																				obj_t BgL_arg1648z00_1799;

																				{	/* Eval/eval.scm 612 */
																					obj_t BgL_arg1649z00_1800;
																					obj_t BgL_arg1650z00_1801;

																					{	/* Eval/eval.scm 612 */
																						obj_t BgL_arg1651z00_1802;

																						BgL_arg1651z00_1802 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2700z00zz__evalz00,
																							BNIL);
																						BgL_arg1649z00_1800 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2702z00zz__evalz00,
																							BgL_arg1651z00_1802);
																					}
																					{	/* Eval/eval.scm 613 */
																						obj_t BgL_arg1652z00_1803;

																						{	/* Eval/eval.scm 613 */
																							obj_t BgL_arg1653z00_1804;

																							{	/* Eval/eval.scm 613 */
																								obj_t BgL_arg1654z00_1805;
																								obj_t BgL_arg1656z00_1806;

																								{	/* Eval/eval.scm 613 */
																									obj_t BgL_arg1657z00_1807;

																									BgL_arg1657z00_1807 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2700z00zz__evalz00,
																										BNIL);
																									BgL_arg1654z00_1805 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2704z00zz__evalz00,
																										BgL_arg1657z00_1807);
																								}
																								{	/* Eval/eval.scm 614 */
																									obj_t BgL_arg1658z00_1808;

																									{	/* Eval/eval.scm 614 */
																										obj_t BgL_arg1661z00_1809;
																										obj_t BgL_arg1663z00_1810;

																										{	/* Eval/eval.scm 614 */
																											obj_t BgL_arg1664z00_1811;

																											{	/* Eval/eval.scm 614 */
																												obj_t
																													BgL_arg1667z00_1812;
																												BgL_arg1667z00_1812 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2706z00zz__evalz00,
																													BNIL);
																												BgL_arg1664z00_1811 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2708z00zz__evalz00,
																													BgL_arg1667z00_1812);
																											}
																											BgL_arg1661z00_1809 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2630z00zz__evalz00,
																												BgL_arg1664z00_1811);
																										}
																										{	/* Eval/eval.scm 615 */
																											obj_t BgL_arg1668z00_1813;
																											obj_t BgL_arg1669z00_1814;

																											{	/* Eval/eval.scm 615 */
																												obj_t
																													BgL_arg1670z00_1815;
																												{	/* Eval/eval.scm 615 */
																													obj_t
																														BgL_arg1675z00_1816;
																													BgL_arg1675z00_1816 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2710z00zz__evalz00,
																														BNIL);
																													BgL_arg1670z00_1815 =
																														MAKE_YOUNG_PAIR
																														(BgL_fnamez00_1725,
																														BgL_arg1675z00_1816);
																												}
																												BgL_arg1668z00_1813 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2712z00zz__evalz00,
																													BgL_arg1670z00_1815);
																											}
																											{	/* Eval/eval.scm 616 */
																												obj_t
																													BgL_arg1676z00_1817;
																												{	/* Eval/eval.scm 616 */
																													obj_t
																														BgL_arg1678z00_1818;
																													{	/* Eval/eval.scm 616 */
																														obj_t
																															BgL_arg1681z00_1819;
																														BgL_arg1681z00_1819
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2714z00zz__evalz00,
																															BNIL);
																														BgL_arg1678z00_1818
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_locz00_1726,
																															BgL_arg1681z00_1819);
																													}
																													BgL_arg1676z00_1817 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2712z00zz__evalz00,
																														BgL_arg1678z00_1818);
																												}
																												BgL_arg1669z00_1814 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1676z00_1817,
																													BNIL);
																											}
																											BgL_arg1663z00_1810 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1668z00_1813,
																												BgL_arg1669z00_1814);
																										}
																										BgL_arg1658z00_1808 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1661z00_1809,
																											BgL_arg1663z00_1810);
																									}
																									BgL_arg1656z00_1806 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1658z00_1808, BNIL);
																								}
																								BgL_arg1653z00_1804 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1654z00_1805,
																									BgL_arg1656z00_1806);
																							}
																							BgL_arg1652z00_1803 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2716z00zz__evalz00,
																								BgL_arg1653z00_1804);
																						}
																						BgL_arg1650z00_1801 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1652z00_1803, BNIL);
																					}
																					BgL_arg1648z00_1799 =
																						MAKE_YOUNG_PAIR(BgL_arg1649z00_1800,
																						BgL_arg1650z00_1801);
																				}
																				BgL_arg1645z00_1797 =
																					MAKE_YOUNG_PAIR
																					(BGl_symbol2718z00zz__evalz00,
																					BgL_arg1648z00_1799);
																			}
																			{	/* Eval/eval.scm 617 */
																				obj_t BgL_arg1684z00_1820;

																				{	/* Eval/eval.scm 617 */
																					obj_t BgL_arg1685z00_1821;

																					{	/* Eval/eval.scm 617 */
																						obj_t BgL_arg1688z00_1822;
																						obj_t BgL_arg1689z00_1823;

																						{	/* Eval/eval.scm 617 */
																							obj_t BgL_arg1691z00_1824;
																							obj_t BgL_arg1692z00_1825;

																							{	/* Eval/eval.scm 617 */
																								obj_t BgL_arg1699z00_1826;

																								{	/* Eval/eval.scm 617 */
																									obj_t BgL_arg1700z00_1827;

																									{	/* Eval/eval.scm 617 */
																										obj_t BgL_arg1701z00_1828;

																										{	/* Eval/eval.scm 617 */
																											obj_t BgL_arg1702z00_1829;
																											obj_t BgL_arg1703z00_1830;

																											BgL_arg1702z00_1829 =
																												BGl_loopze70ze7zz__evalz00
																												(BgL_namez00_1695,
																												BgL_locz00_1726,
																												BgL_fnamez00_1725,
																												BgL_argsz00_1696,
																												BGl_list2720z00zz__evalz00,
																												BNIL);
																											{	/* Eval/eval.scm 620 */
																												obj_t
																													BgL_arg1704z00_1831;
																												{	/* Eval/eval.scm 620 */
																													obj_t BgL_auxz00_7523;

																													{	/* Eval/eval.scm 620 */
																														bool_t
																															BgL_test3241z00_7524;
																														if (PAIRP
																															(BgL_bodyz00_1697))
																															{	/* Eval/eval.scm 620 */
																																BgL_test3241z00_7524
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Eval/eval.scm 620 */
																																BgL_test3241z00_7524
																																	=
																																	NULLP
																																	(BgL_bodyz00_1697);
																															}
																														if (BgL_test3241z00_7524)
																															{	/* Eval/eval.scm 620 */
																																BgL_auxz00_7523
																																	=
																																	BgL_bodyz00_1697;
																															}
																														else
																															{
																																obj_t
																																	BgL_auxz00_7528;
																																BgL_auxz00_7528
																																	=
																																	BGl_typezd2errorzd2zz__errorz00
																																	(BGl_string2618z00zz__evalz00,
																																	BINT(24183L),
																																	BGl_string2723z00zz__evalz00,
																																	BGl_string2681z00zz__evalz00,
																																	BgL_bodyz00_1697);
																																FAILURE
																																	(BgL_auxz00_7528,
																																	BFALSE,
																																	BFALSE);
																															}
																													}
																													BgL_arg1704z00_1831 =
																														BGl_expandzd2prognzd2zz__prognz00
																														(BgL_auxz00_7523);
																												}
																												BgL_arg1703z00_1830 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1704z00_1831,
																													BNIL);
																											}
																											BgL_arg1701z00_1828 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1702z00_1829,
																												BgL_arg1703z00_1830);
																										}
																										BgL_arg1700z00_1827 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol2724z00zz__evalz00,
																											BgL_arg1701z00_1828);
																									}
																									BgL_arg1699z00_1826 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1700z00_1827, BNIL);
																								}
																								BgL_arg1691z00_1824 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2726z00zz__evalz00,
																									BgL_arg1699z00_1826);
																							}
																							{	/* Eval/eval.scm 621 */
																								obj_t BgL_arg1705z00_1832;

																								{	/* Eval/eval.scm 621 */
																									obj_t BgL_arg1706z00_1833;

																									{	/* Eval/eval.scm 621 */
																										obj_t BgL_arg1707z00_1834;

																										{	/* Eval/eval.scm 621 */
																											obj_t BgL_arg1708z00_1835;

																											{	/* Eval/eval.scm 621 */
																												obj_t
																													BgL_arg1709z00_1836;
																												BgL_arg1709z00_1836 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2698z00zz__evalz00,
																													BNIL);
																												BgL_arg1708z00_1835 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2726z00zz__evalz00,
																													BgL_arg1709z00_1836);
																											}
																											BgL_arg1707z00_1834 =
																												MAKE_YOUNG_PAIR
																												(BGl_symbol2698z00zz__evalz00,
																												BgL_arg1708z00_1835);
																										}
																										BgL_arg1706z00_1833 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1707z00_1834,
																											BNIL);
																									}
																									BgL_arg1705z00_1832 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2728z00zz__evalz00,
																										BgL_arg1706z00_1833);
																								}
																								BgL_arg1692z00_1825 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1705z00_1832, BNIL);
																							}
																							BgL_arg1688z00_1822 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1691z00_1824,
																								BgL_arg1692z00_1825);
																						}
																						{	/* Eval/eval.scm 622 */
																							obj_t BgL_arg1710z00_1837;

																							{	/* Eval/eval.scm 622 */
																								obj_t BgL_arg1711z00_1838;

																								{	/* Eval/eval.scm 622 */
																									obj_t BgL_arg1714z00_1839;

																									BgL_arg1714z00_1839 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2700z00zz__evalz00,
																										BNIL);
																									BgL_arg1711z00_1838 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2728z00zz__evalz00,
																										BgL_arg1714z00_1839);
																								}
																								BgL_arg1710z00_1837 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2730z00zz__evalz00,
																									BgL_arg1711z00_1838);
																							}
																							BgL_arg1689z00_1823 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1710z00_1837, BNIL);
																						}
																						BgL_arg1685z00_1821 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1688z00_1822,
																							BgL_arg1689z00_1823);
																					}
																					BgL_arg1684z00_1820 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2724z00zz__evalz00,
																						BgL_arg1685z00_1821);
																				}
																				BgL_arg1646z00_1798 =
																					MAKE_YOUNG_PAIR(BgL_arg1684z00_1820,
																					BNIL);
																			}
																			BgL_arg1641z00_1793 =
																				MAKE_YOUNG_PAIR(BgL_arg1645z00_1797,
																				BgL_arg1646z00_1798);
																		}
																		BgL_arg1639z00_1791 =
																			MAKE_YOUNG_PAIR(BgL_arg1640z00_1792,
																			BgL_arg1641z00_1793);
																	}
																	BgL_arg1638z00_1790 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2732z00zz__evalz00,
																		BgL_arg1639z00_1791);
																}
																BgL_arg1636z00_1788 =
																	MAKE_YOUNG_PAIR(BgL_arg1638z00_1790, BNIL);
															}
															BgL_arg1631z00_1786 =
																MAKE_YOUNG_PAIR(BgL_arg1634z00_1787,
																BgL_arg1636z00_1788);
														}
														BgL_evexpdz00_1728 =
															MAKE_YOUNG_PAIR(BGl_symbol2734z00zz__evalz00,
															BgL_arg1631z00_1786);
													}
													{	/* Eval/eval.scm 610 */
														obj_t BgL_evexpdzf2loczf2_1729;

														BgL_evexpdzf2loczf2_1729 =
															BGl_evepairifyz00zz__prognz00(BgL_evexpdz00_1728,
															BgL_xz00_52);
														{	/* Eval/eval.scm 623 */
															obj_t BgL_expdzd2evalzd2_1730;

															{	/* Eval/eval.scm 82 */
																obj_t BgL_envz00_1785;

																{	/* Eval/eval.scm 257 */
																	obj_t BgL_mz00_2972;

																	BgL_mz00_2972 =
																		BGl_evalzd2modulezd2zz__evmodulez00();
																	if (BGl_evmodulezf3zf3zz__evmodulez00
																		(BgL_mz00_2972))
																		{	/* Eval/eval.scm 258 */
																			BgL_envz00_1785 = BgL_mz00_2972;
																		}
																	else
																		{	/* Eval/eval.scm 258 */
																			BgL_envz00_1785 =
																				BGl_symbol2616z00zz__evalz00;
																		}
																}
																{	/* Eval/eval.scm 82 */

																	{	/* Eval/eval.scm 176 */
																		obj_t BgL_evaluatez00_2974;

																		if (PROCEDUREP
																			(BGl_defaultzd2evaluatezd2zz__evalz00))
																			{	/* Eval/eval.scm 176 */
																				BgL_evaluatez00_2974 =
																					BGl_defaultzd2evaluatezd2zz__evalz00;
																			}
																		else
																			{	/* Eval/eval.scm 176 */
																				BgL_evaluatez00_2974 =
																					BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																			}
																		{	/* Eval/eval.scm 179 */
																			obj_t BgL_auxz00_7564;

																			if (PROCEDUREP(BgL_evaluatez00_2974))
																				{	/* Eval/eval.scm 179 */
																					BgL_auxz00_7564 =
																						BgL_evaluatez00_2974;
																				}
																			else
																				{
																					obj_t BgL_auxz00_7567;

																					BgL_auxz00_7567 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(6857L),
																						BGl_string2723z00zz__evalz00,
																						BGl_string2620z00zz__evalz00,
																						BgL_evaluatez00_2974);
																					FAILURE(BgL_auxz00_7567, BFALSE,
																						BFALSE);
																				}
																			BgL_expdzd2evalzd2_1730 =
																				BGl_evalzf2expanderzf2zz__evalz00
																				(BgL_evexpdzf2loczf2_1729,
																				BgL_envz00_1785,
																				BGl_expandz12zd2envzc0zz__expandz00,
																				BgL_auxz00_7564);
																		}
																	}
																}
															}
															{	/* Eval/eval.scm 624 */

																{	/* Eval/eval.scm 625 */
																	obj_t BgL_zc3z04anonymousza31608ze3z87_3687;

																	BgL_zc3z04anonymousza31608ze3z87_3687 =
																		MAKE_FX_PROCEDURE
																		(BGl_z62zc3z04anonymousza31608ze3ze5zz__evalz00,
																		(int) (2L), (int) (1L));
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza31608ze3z87_3687,
																		(int) (0L), BgL_expdzd2evalzd2_1730);
																	BgL_arg1607z00_1727 =
																		BgL_zc3z04anonymousza31608ze3z87_3687;
												}}}}}
												BGl_installzd2expanderzd2zz__macroz00(BgL_namez00_1695,
													BgL_arg1607z00_1727);
										}}
										return BUNSPEC;
									}
								else
									{	/* Eval/eval.scm 603 */
										obj_t BgL_cdrzd2290zd2_1710;

										{	/* Eval/eval.scm 603 */
											obj_t BgL_pairz00_3037;

											BgL_pairz00_3037 = BgL_cdrzd2268zd2_1702;
											BgL_cdrzd2290zd2_1710 = CDR(BgL_pairz00_3037);
										}
										if (PAIRP(BgL_cdrzd2290zd2_1710))
											{	/* Eval/eval.scm 603 */
												obj_t BgL_carzd2294zd2_1712;

												BgL_carzd2294zd2_1712 = CAR(BgL_cdrzd2290zd2_1710);
												if (PAIRP(BgL_carzd2294zd2_1712))
													{	/* Eval/eval.scm 603 */
														obj_t BgL_cdrzd2299zd2_1714;

														BgL_cdrzd2299zd2_1714 = CDR(BgL_carzd2294zd2_1712);
														if (
															(CAR(BgL_carzd2294zd2_1712) ==
																BGl_symbol2734z00zz__evalz00))
															{	/* Eval/eval.scm 603 */
																if (PAIRP(BgL_cdrzd2299zd2_1714))
																	{	/* Eval/eval.scm 603 */
																		if (NULLP(CDR(BgL_cdrzd2290zd2_1710)))
																			{	/* Eval/eval.scm 603 */
																				obj_t BgL_arg1601z00_1720;
																				obj_t BgL_arg1602z00_1721;
																				obj_t BgL_arg1603z00_1722;

																				{	/* Eval/eval.scm 603 */
																					obj_t BgL_pairz00_3042;

																					BgL_pairz00_3042 =
																						BgL_cdrzd2268zd2_1702;
																					BgL_arg1601z00_1720 =
																						CAR(BgL_pairz00_3042);
																				}
																				BgL_arg1602z00_1721 =
																					CAR(BgL_cdrzd2299zd2_1714);
																				BgL_arg1603z00_1722 =
																					CDR(BgL_cdrzd2299zd2_1714);
																				{
																					obj_t BgL_bodyz00_7601;
																					obj_t BgL_argsz00_7600;
																					obj_t BgL_namez00_7599;

																					BgL_namez00_7599 =
																						BgL_arg1601z00_1720;
																					BgL_argsz00_7600 =
																						BgL_arg1602z00_1721;
																					BgL_bodyz00_7601 =
																						BgL_arg1603z00_1722;
																					BgL_bodyz00_1697 = BgL_bodyz00_7601;
																					BgL_argsz00_1696 = BgL_argsz00_7600;
																					BgL_namez00_1695 = BgL_namez00_7599;
																					goto BgL_tagzd2254zd2_1698;
																				}
																			}
																		else
																			{	/* Eval/eval.scm 644 */
																				obj_t BgL_procz00_3045;

																				BgL_procz00_3045 =
																					BGl_symbol2737z00zz__evalz00;
																				if (EPAIRP(BgL_xz00_52))
																					{	/* Eval/eval.scm 547 */
																						obj_t BgL_arg1524z00_3047;

																						{	/* Eval/eval.scm 547 */
																							obj_t BgL_objz00_3048;

																							if (EPAIRP(BgL_xz00_52))
																								{	/* Eval/eval.scm 547 */
																									BgL_objz00_3048 = BgL_xz00_52;
																								}
																							else
																								{
																									obj_t BgL_auxz00_7606;

																									BgL_auxz00_7606 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(21769L),
																										BGl_string2736z00zz__evalz00,
																										BGl_string2676z00zz__evalz00,
																										BgL_xz00_52);
																									FAILURE(BgL_auxz00_7606,
																										BFALSE, BFALSE);
																								}
																							BgL_arg1524z00_3047 =
																								CER(BgL_objz00_3048);
																						}
																						return
																							BGl_everrorz00zz__everrorz00
																							(BgL_arg1524z00_3047,
																							BgL_procz00_3045,
																							BGl_string2739z00zz__evalz00,
																							BgL_xz00_52);
																					}
																				else
																					{	/* Eval/eval.scm 546 */
																						return
																							BGl_errorz00zz__errorz00
																							(BgL_procz00_3045,
																							BGl_string2739z00zz__evalz00,
																							BgL_xz00_52);
																					}
																			}
																	}
																else
																	{	/* Eval/eval.scm 644 */
																		obj_t BgL_procz00_3049;

																		BgL_procz00_3049 =
																			BGl_symbol2737z00zz__evalz00;
																		if (EPAIRP(BgL_xz00_52))
																			{	/* Eval/eval.scm 547 */
																				obj_t BgL_arg1524z00_3051;

																				{	/* Eval/eval.scm 547 */
																					obj_t BgL_objz00_3052;

																					if (EPAIRP(BgL_xz00_52))
																						{	/* Eval/eval.scm 547 */
																							BgL_objz00_3052 = BgL_xz00_52;
																						}
																					else
																						{
																							obj_t BgL_auxz00_7617;

																							BgL_auxz00_7617 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(21769L),
																								BGl_string2736z00zz__evalz00,
																								BGl_string2676z00zz__evalz00,
																								BgL_xz00_52);
																							FAILURE(BgL_auxz00_7617, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1524z00_3051 =
																						CER(BgL_objz00_3052);
																				}
																				return
																					BGl_everrorz00zz__everrorz00
																					(BgL_arg1524z00_3051,
																					BgL_procz00_3049,
																					BGl_string2739z00zz__evalz00,
																					BgL_xz00_52);
																			}
																		else
																			{	/* Eval/eval.scm 546 */
																				return
																					BGl_errorz00zz__errorz00
																					(BgL_procz00_3049,
																					BGl_string2739z00zz__evalz00,
																					BgL_xz00_52);
																			}
																	}
															}
														else
															{	/* Eval/eval.scm 644 */
																obj_t BgL_procz00_3053;

																BgL_procz00_3053 = BGl_symbol2737z00zz__evalz00;
																if (EPAIRP(BgL_xz00_52))
																	{	/* Eval/eval.scm 547 */
																		obj_t BgL_arg1524z00_3055;

																		{	/* Eval/eval.scm 547 */
																			obj_t BgL_objz00_3056;

																			if (EPAIRP(BgL_xz00_52))
																				{	/* Eval/eval.scm 547 */
																					BgL_objz00_3056 = BgL_xz00_52;
																				}
																			else
																				{
																					obj_t BgL_auxz00_7628;

																					BgL_auxz00_7628 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(21769L),
																						BGl_string2736z00zz__evalz00,
																						BGl_string2676z00zz__evalz00,
																						BgL_xz00_52);
																					FAILURE(BgL_auxz00_7628, BFALSE,
																						BFALSE);
																				}
																			BgL_arg1524z00_3055 =
																				CER(BgL_objz00_3056);
																		}
																		return
																			BGl_everrorz00zz__everrorz00
																			(BgL_arg1524z00_3055, BgL_procz00_3053,
																			BGl_string2739z00zz__evalz00,
																			BgL_xz00_52);
																	}
																else
																	{	/* Eval/eval.scm 546 */
																		return
																			BGl_errorz00zz__errorz00(BgL_procz00_3053,
																			BGl_string2739z00zz__evalz00,
																			BgL_xz00_52);
																	}
															}
													}
												else
													{	/* Eval/eval.scm 644 */
														obj_t BgL_procz00_3057;

														BgL_procz00_3057 = BGl_symbol2737z00zz__evalz00;
														if (EPAIRP(BgL_xz00_52))
															{	/* Eval/eval.scm 547 */
																obj_t BgL_arg1524z00_3059;

																{	/* Eval/eval.scm 547 */
																	obj_t BgL_objz00_3060;

																	if (EPAIRP(BgL_xz00_52))
																		{	/* Eval/eval.scm 547 */
																			BgL_objz00_3060 = BgL_xz00_52;
																		}
																	else
																		{
																			obj_t BgL_auxz00_7639;

																			BgL_auxz00_7639 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(21769L),
																				BGl_string2736z00zz__evalz00,
																				BGl_string2676z00zz__evalz00,
																				BgL_xz00_52);
																			FAILURE(BgL_auxz00_7639, BFALSE, BFALSE);
																		}
																	BgL_arg1524z00_3059 = CER(BgL_objz00_3060);
																}
																return
																	BGl_everrorz00zz__everrorz00
																	(BgL_arg1524z00_3059, BgL_procz00_3057,
																	BGl_string2739z00zz__evalz00, BgL_xz00_52);
															}
														else
															{	/* Eval/eval.scm 546 */
																return
																	BGl_errorz00zz__errorz00(BgL_procz00_3057,
																	BGl_string2739z00zz__evalz00, BgL_xz00_52);
															}
													}
											}
										else
											{	/* Eval/eval.scm 644 */
												obj_t BgL_procz00_3061;

												BgL_procz00_3061 = BGl_symbol2737z00zz__evalz00;
												if (EPAIRP(BgL_xz00_52))
													{	/* Eval/eval.scm 547 */
														obj_t BgL_arg1524z00_3063;

														{	/* Eval/eval.scm 547 */
															obj_t BgL_objz00_3064;

															if (EPAIRP(BgL_xz00_52))
																{	/* Eval/eval.scm 547 */
																	BgL_objz00_3064 = BgL_xz00_52;
																}
															else
																{
																	obj_t BgL_auxz00_7650;

																	BgL_auxz00_7650 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(21769L),
																		BGl_string2736z00zz__evalz00,
																		BGl_string2676z00zz__evalz00, BgL_xz00_52);
																	FAILURE(BgL_auxz00_7650, BFALSE, BFALSE);
																}
															BgL_arg1524z00_3063 = CER(BgL_objz00_3064);
														}
														return
															BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3063,
															BgL_procz00_3061, BGl_string2739z00zz__evalz00,
															BgL_xz00_52);
													}
												else
													{	/* Eval/eval.scm 546 */
														return
															BGl_errorz00zz__errorz00(BgL_procz00_3061,
															BGl_string2739z00zz__evalz00, BgL_xz00_52);
													}
											}
									}
							}
						else
							{	/* Eval/eval.scm 644 */
								obj_t BgL_procz00_3065;

								BgL_procz00_3065 = BGl_symbol2737z00zz__evalz00;
								if (EPAIRP(BgL_xz00_52))
									{	/* Eval/eval.scm 547 */
										obj_t BgL_arg1524z00_3067;

										{	/* Eval/eval.scm 547 */
											obj_t BgL_objz00_3068;

											if (EPAIRP(BgL_xz00_52))
												{	/* Eval/eval.scm 547 */
													BgL_objz00_3068 = BgL_xz00_52;
												}
											else
												{
													obj_t BgL_auxz00_7661;

													BgL_auxz00_7661 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(21769L),
														BGl_string2736z00zz__evalz00,
														BGl_string2676z00zz__evalz00, BgL_xz00_52);
													FAILURE(BgL_auxz00_7661, BFALSE, BFALSE);
												}
											BgL_arg1524z00_3067 = CER(BgL_objz00_3068);
										}
										return
											BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3067,
											BgL_procz00_3065, BGl_string2739z00zz__evalz00,
											BgL_xz00_52);
									}
								else
									{	/* Eval/eval.scm 546 */
										return
											BGl_errorz00zz__errorz00(BgL_procz00_3065,
											BGl_string2739z00zz__evalz00, BgL_xz00_52);
									}
							}
					}
				else
					{	/* Eval/eval.scm 644 */
						obj_t BgL_procz00_3069;

						BgL_procz00_3069 = BGl_symbol2737z00zz__evalz00;
						if (EPAIRP(BgL_xz00_52))
							{	/* Eval/eval.scm 547 */
								obj_t BgL_arg1524z00_3071;

								{	/* Eval/eval.scm 547 */
									obj_t BgL_objz00_3072;

									if (EPAIRP(BgL_xz00_52))
										{	/* Eval/eval.scm 547 */
											BgL_objz00_3072 = BgL_xz00_52;
										}
									else
										{
											obj_t BgL_auxz00_7672;

											BgL_auxz00_7672 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2618z00zz__evalz00, BINT(21769L),
												BGl_string2736z00zz__evalz00,
												BGl_string2676z00zz__evalz00, BgL_xz00_52);
											FAILURE(BgL_auxz00_7672, BFALSE, BFALSE);
										}
									BgL_arg1524z00_3071 = CER(BgL_objz00_3072);
								}
								return
									BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3071,
									BgL_procz00_3069, BGl_string2739z00zz__evalz00, BgL_xz00_52);
							}
						else
							{	/* Eval/eval.scm 546 */
								return
									BGl_errorz00zz__errorz00(BgL_procz00_3069,
									BGl_string2739z00zz__evalz00, BgL_xz00_52);
							}
					}
			}
		}

	}



/* &expand-define-macro */
	obj_t BGl_z62expandzd2definezd2macroz62zz__evalz00(obj_t BgL_envz00_3688,
		obj_t BgL_xz00_3689, obj_t BgL_ez00_3690)
	{
		{	/* Eval/eval.scm 602 */
			return
				BGl_expandzd2definezd2macroz00zz__evalz00(BgL_xz00_3689, BgL_ez00_3690);
		}

	}



/* &<@anonymous:1608> */
	obj_t BGl_z62zc3z04anonymousza31608ze3ze5zz__evalz00(obj_t BgL_envz00_3691,
		obj_t BgL_xz00_3693, obj_t BgL_ez00_3694)
	{
		{	/* Eval/eval.scm 625 */
			{	/* Eval/eval.scm 626 */
				obj_t BgL_expdzd2evalzd2_3692;

				BgL_expdzd2evalzd2_3692 = PROCEDURE_REF(BgL_envz00_3691, (int) (0L));
				{	/* Eval/eval.scm 626 */
					obj_t BgL_env1144z00_4998;

					BgL_env1144z00_4998 = BGL_CURRENT_DYNAMIC_ENV();
					{
						obj_t BgL_ez00_5000;

						{	/* Eval/eval.scm 626 */
							obj_t BgL_cell1139z00_5240;

							BgL_cell1139z00_5240 = MAKE_STACK_CELL(BUNSPEC);
							{	/* Eval/eval.scm 626 */
								obj_t BgL_val1143z00_5241;

								BgL_val1143z00_5241 =
									BGl_zc3z04exitza31610ze3ze70z60zz__evalz00(BgL_ez00_3694,
									BgL_xz00_3693, BgL_expdzd2evalzd2_3692, BgL_cell1139z00_5240,
									BgL_env1144z00_4998);
								if ((BgL_val1143z00_5241 == BgL_cell1139z00_5240))
									{	/* Eval/eval.scm 626 */
										{	/* Eval/eval.scm 626 */
											int BgL_tmpz00_7687;

											BgL_tmpz00_7687 = (int) (0L);
											BGL_SIGSETMASK(BgL_tmpz00_7687);
										}
										{	/* Eval/eval.scm 626 */
											obj_t BgL_arg1609z00_5242;

											BgL_arg1609z00_5242 =
												CELL_REF(((obj_t) BgL_val1143z00_5241));
											BgL_ez00_5000 = BgL_arg1609z00_5242;
											{	/* Eval/eval.scm 628 */
												obj_t BgL_nez00_5001;

												{	/* Eval/eval.scm 628 */
													bool_t BgL_test3266z00_7692;

													{	/* Eval/eval.scm 628 */
														obj_t BgL_classz00_5002;

														BgL_classz00_5002 = BGl_z62errorz62zz__objectz00;
														if (BGL_OBJECTP(BgL_ez00_5000))
															{	/* Eval/eval.scm 628 */
																BgL_objectz00_bglt BgL_arg2222z00_5003;

																BgL_arg2222z00_5003 =
																	(BgL_objectz00_bglt) (BgL_ez00_5000);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Eval/eval.scm 628 */
																		long BgL_idxz00_5004;

																		BgL_idxz00_5004 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg2222z00_5003);
																		{	/* Eval/eval.scm 628 */
																			obj_t BgL_arg2215z00_5005;

																			{	/* Eval/eval.scm 628 */
																				long BgL_arg2216z00_5006;

																				BgL_arg2216z00_5006 =
																					(BgL_idxz00_5004 + 3L);
																				{	/* Eval/eval.scm 628 */
																					obj_t BgL_vectorz00_5007;

																					{	/* Eval/eval.scm 628 */
																						obj_t BgL_aux2492z00_5008;

																						BgL_aux2492z00_5008 =
																							BGl_za2inheritancesza2z00zz__objectz00;
																						if (VECTORP(BgL_aux2492z00_5008))
																							{	/* Eval/eval.scm 628 */
																								BgL_vectorz00_5007 =
																									BgL_aux2492z00_5008;
																							}
																						else
																							{
																								obj_t BgL_auxz00_7702;

																								BgL_auxz00_7702 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(24396L),
																									BGl_string2740z00zz__evalz00,
																									BGl_string2628z00zz__evalz00,
																									BgL_aux2492z00_5008);
																								FAILURE(BgL_auxz00_7702, BFALSE,
																									BFALSE);
																							}
																					}
																					BgL_arg2215z00_5005 =
																						VECTOR_REF(BgL_vectorz00_5007,
																						BgL_arg2216z00_5006);
																				}
																			}
																			BgL_test3266z00_7692 =
																				(BgL_arg2215z00_5005 ==
																				BgL_classz00_5002);
																		}
																	}
																else
																	{	/* Eval/eval.scm 628 */
																		bool_t BgL_res2240z00_5009;

																		{	/* Eval/eval.scm 628 */
																			obj_t BgL_oclassz00_5010;

																			{	/* Eval/eval.scm 628 */
																				obj_t BgL_arg2229z00_5011;
																				long BgL_arg2230z00_5012;

																				BgL_arg2229z00_5011 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Eval/eval.scm 628 */
																					long BgL_arg2231z00_5013;

																					BgL_arg2231z00_5013 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg2222z00_5003);
																					BgL_arg2230z00_5012 =
																						(BgL_arg2231z00_5013 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_5010 =
																					VECTOR_REF(BgL_arg2229z00_5011,
																					BgL_arg2230z00_5012);
																			}
																			{	/* Eval/eval.scm 628 */
																				bool_t BgL__ortest_1236z00_5014;

																				BgL__ortest_1236z00_5014 =
																					(BgL_classz00_5002 ==
																					BgL_oclassz00_5010);
																				if (BgL__ortest_1236z00_5014)
																					{	/* Eval/eval.scm 628 */
																						BgL_res2240z00_5009 =
																							BgL__ortest_1236z00_5014;
																					}
																				else
																					{	/* Eval/eval.scm 628 */
																						long BgL_odepthz00_5015;

																						{	/* Eval/eval.scm 628 */
																							obj_t BgL_arg2219z00_5016;

																							BgL_arg2219z00_5016 =
																								(BgL_oclassz00_5010);
																							BgL_odepthz00_5015 =
																								BGL_CLASS_DEPTH
																								(BgL_arg2219z00_5016);
																						}
																						if ((3L < BgL_odepthz00_5015))
																							{	/* Eval/eval.scm 628 */
																								obj_t BgL_arg2217z00_5017;

																								{	/* Eval/eval.scm 628 */
																									obj_t BgL_arg2218z00_5018;

																									BgL_arg2218z00_5018 =
																										(BgL_oclassz00_5010);
																									BgL_arg2217z00_5017 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg2218z00_5018, 3L);
																								}
																								BgL_res2240z00_5009 =
																									(BgL_arg2217z00_5017 ==
																									BgL_classz00_5002);
																							}
																						else
																							{	/* Eval/eval.scm 628 */
																								BgL_res2240z00_5009 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3266z00_7692 = BgL_res2240z00_5009;
																	}
															}
														else
															{	/* Eval/eval.scm 628 */
																BgL_test3266z00_7692 = ((bool_t) 0);
															}
													}
													if (BgL_test3266z00_7692)
														{	/* Eval/eval.scm 629 */
															BgL_z62errorz62_bglt BgL_i1145z00_5019;

															{	/* Eval/eval.scm 630 */
																bool_t BgL_test3272z00_7721;

																{	/* Eval/eval.scm 630 */
																	obj_t BgL_classz00_5020;

																	BgL_classz00_5020 =
																		BGl_z62errorz62zz__objectz00;
																	if (BGL_OBJECTP(BgL_ez00_5000))
																		{	/* Eval/eval.scm 630 */
																			BgL_objectz00_bglt BgL_arg2224z00_5022;
																			long BgL_arg2225z00_5023;

																			BgL_arg2224z00_5022 =
																				(BgL_objectz00_bglt) (BgL_ez00_5000);
																			BgL_arg2225z00_5023 =
																				BGL_CLASS_DEPTH(BgL_classz00_5020);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Eval/eval.scm 630 */
																					long BgL_idxz00_5031;

																					BgL_idxz00_5031 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg2224z00_5022);
																					{	/* Eval/eval.scm 630 */
																						obj_t BgL_arg2215z00_5032;

																						{	/* Eval/eval.scm 630 */
																							long BgL_arg2216z00_5033;

																							BgL_arg2216z00_5033 =
																								(BgL_idxz00_5031 +
																								BgL_arg2225z00_5023);
																							BgL_arg2215z00_5032 =
																								VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																								BgL_arg2216z00_5033);
																						}
																						BgL_test3272z00_7721 =
																							(BgL_arg2215z00_5032 ==
																							BgL_classz00_5020);
																				}}
																			else
																				{	/* Eval/eval.scm 630 */
																					bool_t BgL_res2244z00_5038;

																					{	/* Eval/eval.scm 630 */
																						obj_t BgL_oclassz00_5042;

																						{	/* Eval/eval.scm 630 */
																							obj_t BgL_arg2229z00_5044;
																							long BgL_arg2230z00_5045;

																							BgL_arg2229z00_5044 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Eval/eval.scm 630 */
																								long BgL_arg2231z00_5046;

																								BgL_arg2231z00_5046 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg2224z00_5022);
																								BgL_arg2230z00_5045 =
																									(BgL_arg2231z00_5046 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_5042 =
																								VECTOR_REF(BgL_arg2229z00_5044,
																								BgL_arg2230z00_5045);
																						}
																						{	/* Eval/eval.scm 630 */
																							bool_t BgL__ortest_1236z00_5052;

																							BgL__ortest_1236z00_5052 =
																								(BgL_classz00_5020 ==
																								BgL_oclassz00_5042);
																							if (BgL__ortest_1236z00_5052)
																								{	/* Eval/eval.scm 630 */
																									BgL_res2244z00_5038 =
																										BgL__ortest_1236z00_5052;
																								}
																							else
																								{	/* Eval/eval.scm 630 */
																									long BgL_odepthz00_5053;

																									{	/* Eval/eval.scm 630 */
																										obj_t BgL_arg2219z00_5054;

																										BgL_arg2219z00_5054 =
																											(BgL_oclassz00_5042);
																										BgL_odepthz00_5053 =
																											BGL_CLASS_DEPTH
																											(BgL_arg2219z00_5054);
																									}
																									if (
																										(BgL_arg2225z00_5023 <
																											BgL_odepthz00_5053))
																										{	/* Eval/eval.scm 630 */
																											obj_t BgL_arg2217z00_5058;

																											{	/* Eval/eval.scm 630 */
																												obj_t
																													BgL_arg2218z00_5059;
																												BgL_arg2218z00_5059 =
																													(BgL_oclassz00_5042);
																												BgL_arg2217z00_5058 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg2218z00_5059,
																													BgL_arg2225z00_5023);
																											}
																											BgL_res2244z00_5038 =
																												(BgL_arg2217z00_5058 ==
																												BgL_classz00_5020);
																										}
																									else
																										{	/* Eval/eval.scm 630 */
																											BgL_res2244z00_5038 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test3272z00_7721 =
																						BgL_res2244z00_5038;
																				}
																		}
																	else
																		{	/* Eval/eval.scm 630 */
																			BgL_test3272z00_7721 = ((bool_t) 0);
																		}
																}
																if (BgL_test3272z00_7721)
																	{	/* Eval/eval.scm 630 */
																		BgL_i1145z00_5019 =
																			((BgL_z62errorz62_bglt) BgL_ez00_5000);
																	}
																else
																	{
																		obj_t BgL_auxz00_7746;

																		BgL_auxz00_7746 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2618z00zz__evalz00,
																			BINT(24455L),
																			BGl_string2740z00zz__evalz00,
																			BGl_string2658z00zz__evalz00,
																			BgL_ez00_5000);
																		FAILURE(BgL_auxz00_7746, BFALSE, BFALSE);
																	}
															}
															{	/* Eval/eval.scm 630 */
																bool_t BgL_test3277z00_7750;

																{	/* Eval/eval.scm 630 */
																	obj_t BgL_tmpz00_7751;

																	BgL_tmpz00_7751 =
																		(((BgL_z62errorz62_bglt)
																			COBJECT(BgL_i1145z00_5019))->BgL_objz00);
																	BgL_test3277z00_7750 =
																		EPAIRP(BgL_tmpz00_7751);
																}
																if (BgL_test3277z00_7750)
																	{
																		obj_t BgL_fnamez00_5061;
																		obj_t BgL_locz00_5062;

																		{	/* Eval/eval.scm 631 */
																			obj_t BgL_ezd2314zd2_5233;

																			{	/* Eval/eval.scm 631 */
																				obj_t BgL_objz00_5234;

																				{	/* Eval/eval.scm 631 */
																					obj_t BgL_aux2507z00_5235;

																					BgL_aux2507z00_5235 =
																						(((BgL_z62errorz62_bglt)
																							COBJECT(BgL_i1145z00_5019))->
																						BgL_objz00);
																					if (EPAIRP(BgL_aux2507z00_5235))
																						{	/* Eval/eval.scm 631 */
																							BgL_objz00_5234 =
																								BgL_aux2507z00_5235;
																						}
																					else
																						{
																							obj_t BgL_auxz00_7757;

																							BgL_auxz00_7757 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(24493L),
																								BGl_string2740z00zz__evalz00,
																								BGl_string2676z00zz__evalz00,
																								BgL_aux2507z00_5235);
																							FAILURE(BgL_auxz00_7757, BFALSE,
																								BFALSE);
																						}
																				}
																				BgL_ezd2314zd2_5233 =
																					CER(BgL_objz00_5234);
																			}
																			if (PAIRP(BgL_ezd2314zd2_5233))
																				{	/* Eval/eval.scm 631 */
																					obj_t BgL_cdrzd2320zd2_5236;

																					BgL_cdrzd2320zd2_5236 =
																						CDR(BgL_ezd2314zd2_5233);
																					if (
																						(CAR(BgL_ezd2314zd2_5233) ==
																							BGl_symbol2630z00zz__evalz00))
																						{	/* Eval/eval.scm 631 */
																							if (PAIRP(BgL_cdrzd2320zd2_5236))
																								{	/* Eval/eval.scm 631 */
																									obj_t BgL_cdrzd2324zd2_5237;

																									BgL_cdrzd2324zd2_5237 =
																										CDR(BgL_cdrzd2320zd2_5236);
																									if (PAIRP
																										(BgL_cdrzd2324zd2_5237))
																										{	/* Eval/eval.scm 631 */
																											if (NULLP(CDR
																													(BgL_cdrzd2324zd2_5237)))
																												{	/* Eval/eval.scm 631 */
																													obj_t
																														BgL_arg1624z00_5238;
																													obj_t
																														BgL_arg1625z00_5239;
																													BgL_arg1624z00_5238 =
																														CAR
																														(BgL_cdrzd2320zd2_5236);
																													BgL_arg1625z00_5239 =
																														CAR
																														(BgL_cdrzd2324zd2_5237);
																													{
																														BgL_z62errorz62_bglt
																															BgL_auxz00_7778;
																														BgL_fnamez00_5061 =
																															BgL_arg1624z00_5238;
																														BgL_locz00_5062 =
																															BgL_arg1625z00_5239;
																														{	/* Eval/eval.scm 633 */
																															BgL_z62exceptionz62_bglt
																																BgL_duplicated1148z00_5063;
																															BgL_z62errorz62_bglt
																																BgL_new1146z00_5064;
																															{	/* Eval/eval.scm 634 */
																																bool_t
																																	BgL_test3284z00_7779;
																																{	/* Eval/eval.scm 634 */
																																	obj_t
																																		BgL_classz00_5065;
																																	BgL_classz00_5065
																																		=
																																		BGl_z62exceptionz62zz__objectz00;
																																	if (BGL_OBJECTP(BgL_ez00_5000))
																																		{	/* Eval/eval.scm 634 */
																																			BgL_objectz00_bglt
																																				BgL_arg2224z00_5067;
																																			long
																																				BgL_arg2225z00_5068;
																																			BgL_arg2224z00_5067
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_ez00_5000);
																																			BgL_arg2225z00_5068
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_classz00_5065);
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Eval/eval.scm 634 */
																																					long
																																						BgL_idxz00_5076;
																																					BgL_idxz00_5076
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg2224z00_5067);
																																					{	/* Eval/eval.scm 634 */
																																						obj_t
																																							BgL_arg2215z00_5077;
																																						{	/* Eval/eval.scm 634 */
																																							long
																																								BgL_arg2216z00_5078;
																																							BgL_arg2216z00_5078
																																								=
																																								(BgL_idxz00_5076
																																								+
																																								BgL_arg2225z00_5068);
																																							BgL_arg2215z00_5077
																																								=
																																								VECTOR_REF
																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																								BgL_arg2216z00_5078);
																																						}
																																						BgL_test3284z00_7779
																																							=
																																							(BgL_arg2215z00_5077
																																							==
																																							BgL_classz00_5065);
																																				}}
																																			else
																																				{	/* Eval/eval.scm 634 */
																																					bool_t
																																						BgL_res2244z00_5083;
																																					{	/* Eval/eval.scm 634 */
																																						obj_t
																																							BgL_oclassz00_5087;
																																						{	/* Eval/eval.scm 634 */
																																							obj_t
																																								BgL_arg2229z00_5089;
																																							long
																																								BgL_arg2230z00_5090;
																																							BgL_arg2229z00_5089
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Eval/eval.scm 634 */
																																								long
																																									BgL_arg2231z00_5091;
																																								BgL_arg2231z00_5091
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg2224z00_5067);
																																								BgL_arg2230z00_5090
																																									=
																																									(BgL_arg2231z00_5091
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_5087
																																								=
																																								VECTOR_REF
																																								(BgL_arg2229z00_5089,
																																								BgL_arg2230z00_5090);
																																						}
																																						{	/* Eval/eval.scm 634 */
																																							bool_t
																																								BgL__ortest_1236z00_5097;
																																							BgL__ortest_1236z00_5097
																																								=
																																								(BgL_classz00_5065
																																								==
																																								BgL_oclassz00_5087);
																																							if (BgL__ortest_1236z00_5097)
																																								{	/* Eval/eval.scm 634 */
																																									BgL_res2244z00_5083
																																										=
																																										BgL__ortest_1236z00_5097;
																																								}
																																							else
																																								{	/* Eval/eval.scm 634 */
																																									long
																																										BgL_odepthz00_5098;
																																									{	/* Eval/eval.scm 634 */
																																										obj_t
																																											BgL_arg2219z00_5099;
																																										BgL_arg2219z00_5099
																																											=
																																											(BgL_oclassz00_5087);
																																										BgL_odepthz00_5098
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg2219z00_5099);
																																									}
																																									if ((BgL_arg2225z00_5068 < BgL_odepthz00_5098))
																																										{	/* Eval/eval.scm 634 */
																																											obj_t
																																												BgL_arg2217z00_5103;
																																											{	/* Eval/eval.scm 634 */
																																												obj_t
																																													BgL_arg2218z00_5104;
																																												BgL_arg2218z00_5104
																																													=
																																													(BgL_oclassz00_5087);
																																												BgL_arg2217z00_5103
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg2218z00_5104,
																																													BgL_arg2225z00_5068);
																																											}
																																											BgL_res2244z00_5083
																																												=
																																												(BgL_arg2217z00_5103
																																												==
																																												BgL_classz00_5065);
																																										}
																																									else
																																										{	/* Eval/eval.scm 634 */
																																											BgL_res2244z00_5083
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test3284z00_7779
																																						=
																																						BgL_res2244z00_5083;
																																				}
																																		}
																																	else
																																		{	/* Eval/eval.scm 634 */
																																			BgL_test3284z00_7779
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test3284z00_7779)
																																	{	/* Eval/eval.scm 634 */
																																		BgL_duplicated1148z00_5063
																																			=
																																			(
																																			(BgL_z62exceptionz62_bglt)
																																			BgL_ez00_5000);
																																	}
																																else
																																	{
																																		obj_t
																																			BgL_auxz00_7804;
																																		BgL_auxz00_7804
																																			=
																																			BGl_typezd2errorzd2zz__errorz00
																																			(BGl_string2618z00zz__evalz00,
																																			BINT
																																			(24569L),
																																			BGl_string2741z00zz__evalz00,
																																			BGl_string2629z00zz__evalz00,
																																			BgL_ez00_5000);
																																		FAILURE
																																			(BgL_auxz00_7804,
																																			BFALSE,
																																			BFALSE);
																																	}
																															}
																															{	/* Eval/eval.scm 634 */
																																BgL_z62errorz62_bglt
																																	BgL_new1152z00_5105;
																																BgL_new1152z00_5105
																																	=
																																	(
																																	(BgL_z62errorz62_bglt)
																																	BOBJECT
																																	(GC_MALLOC
																																		(sizeof
																																			(struct
																																				BgL_z62errorz62_bgl))));
																																{	/* Eval/eval.scm 634 */
																																	long
																																		BgL_arg1629z00_5106;
																																	BgL_arg1629z00_5106
																																		=
																																		BGL_CLASS_NUM
																																		(BGl_z62errorz62zz__objectz00);
																																	BGL_OBJECT_CLASS_NUM_SET
																																		(((BgL_objectz00_bglt) BgL_new1152z00_5105), BgL_arg1629z00_5106);
																																}
																																BgL_new1146z00_5064
																																	=
																																	BgL_new1152z00_5105;
																															}
																															((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1146z00_5064)))->BgL_fnamez00) = ((obj_t) BgL_fnamez00_5061), BUNSPEC);
																															((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1146z00_5064)))->BgL_locationz00) = ((obj_t) BgL_locz00_5062), BUNSPEC);
																															((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1146z00_5064)))->BgL_stackz00) = ((obj_t) (((BgL_z62exceptionz62_bglt) COBJECT(BgL_duplicated1148z00_5063))->BgL_stackz00)), BUNSPEC);
																															{
																																obj_t
																																	BgL_auxz00_7819;
																																{	/* Eval/eval.scm 635 */
																																	BgL_z62errorz62_bglt
																																		BgL_tmp1149z00_5107;
																																	{	/* Eval/eval.scm 635 */
																																		bool_t
																																			BgL_test3289z00_7820;
																																		{	/* Eval/eval.scm 635 */
																																			obj_t
																																				BgL_classz00_5109;
																																			BgL_classz00_5109
																																				=
																																				BGl_z62errorz62zz__objectz00;
																																			{	/* Eval/eval.scm 635 */
																																				bool_t
																																					BgL_test3290z00_7821;
																																				{	/* Eval/eval.scm 635 */
																																					obj_t
																																						BgL_tmpz00_7822;
																																					BgL_tmpz00_7822
																																						=
																																						(
																																						(obj_t)
																																						BgL_duplicated1148z00_5063);
																																					BgL_test3290z00_7821
																																						=
																																						BGL_OBJECTP
																																						(BgL_tmpz00_7822);
																																				}
																																				if (BgL_test3290z00_7821)
																																					{	/* Eval/eval.scm 635 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_5111;
																																						long
																																							BgL_arg2225z00_5112;
																																						{	/* Eval/eval.scm 635 */
																																							obj_t
																																								BgL_tmpz00_7825;
																																							BgL_tmpz00_7825
																																								=
																																								(
																																								(obj_t)
																																								BgL_duplicated1148z00_5063);
																																							BgL_arg2224z00_5111
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_tmpz00_7825);
																																						}
																																						BgL_arg2225z00_5112
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_5109);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 635 */
																																								long
																																									BgL_idxz00_5120;
																																								BgL_idxz00_5120
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_5111);
																																								{	/* Eval/eval.scm 635 */
																																									obj_t
																																										BgL_arg2215z00_5121;
																																									{	/* Eval/eval.scm 635 */
																																										long
																																											BgL_arg2216z00_5122;
																																										BgL_arg2216z00_5122
																																											=
																																											(BgL_idxz00_5120
																																											+
																																											BgL_arg2225z00_5112);
																																										{	/* Eval/eval.scm 635 */
																																											obj_t
																																												BgL_vectorz00_5125;
																																											BgL_vectorz00_5125
																																												=
																																												BGl_za2inheritancesza2z00zz__objectz00;
																																											BgL_arg2215z00_5121
																																												=
																																												VECTOR_REF
																																												(BgL_vectorz00_5125,
																																												BgL_arg2216z00_5122);
																																									}}
																																									BgL_test3289z00_7820
																																										=
																																										(BgL_arg2215z00_5121
																																										==
																																										BgL_classz00_5109);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 635 */
																																								bool_t
																																									BgL_res2244z00_5127;
																																								{	/* Eval/eval.scm 635 */
																																									obj_t
																																										BgL_oclassz00_5131;
																																									{	/* Eval/eval.scm 635 */
																																										obj_t
																																											BgL_arg2229z00_5133;
																																										long
																																											BgL_arg2230z00_5134;
																																										BgL_arg2229z00_5133
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 635 */
																																											long
																																												BgL_arg2231z00_5135;
																																											BgL_arg2231z00_5135
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_5111);
																																											BgL_arg2230z00_5134
																																												=
																																												(BgL_arg2231z00_5135
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_5131
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_5133,
																																											BgL_arg2230z00_5134);
																																									}
																																									{	/* Eval/eval.scm 635 */
																																										bool_t
																																											BgL__ortest_1236z00_5141;
																																										BgL__ortest_1236z00_5141
																																											=
																																											(BgL_classz00_5109
																																											==
																																											BgL_oclassz00_5131);
																																										if (BgL__ortest_1236z00_5141)
																																											{	/* Eval/eval.scm 635 */
																																												BgL_res2244z00_5127
																																													=
																																													BgL__ortest_1236z00_5141;
																																											}
																																										else
																																											{	/* Eval/eval.scm 635 */
																																												long
																																													BgL_odepthz00_5142;
																																												{	/* Eval/eval.scm 635 */
																																													obj_t
																																														BgL_arg2219z00_5143;
																																													BgL_arg2219z00_5143
																																														=
																																														(BgL_oclassz00_5131);
																																													BgL_odepthz00_5142
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_5143);
																																												}
																																												if ((BgL_arg2225z00_5112 < BgL_odepthz00_5142))
																																													{	/* Eval/eval.scm 635 */
																																														obj_t
																																															BgL_arg2217z00_5147;
																																														{	/* Eval/eval.scm 635 */
																																															obj_t
																																																BgL_arg2218z00_5148;
																																															BgL_arg2218z00_5148
																																																=
																																																(BgL_oclassz00_5131);
																																															BgL_arg2217z00_5147
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_5148,
																																																BgL_arg2225z00_5112);
																																														}
																																														BgL_res2244z00_5127
																																															=
																																															(BgL_arg2217z00_5147
																																															==
																																															BgL_classz00_5109);
																																													}
																																												else
																																													{	/* Eval/eval.scm 635 */
																																														BgL_res2244z00_5127
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3289z00_7820
																																									=
																																									BgL_res2244z00_5127;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 635 */
																																						BgL_test3289z00_7820
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																		if (BgL_test3289z00_7820)
																																			{	/* Eval/eval.scm 635 */
																																				BgL_tmp1149z00_5107
																																					=
																																					(
																																					(BgL_z62errorz62_bglt)
																																					BgL_duplicated1148z00_5063);
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_auxz00_7849;
																																				BgL_auxz00_7849
																																					=
																																					BGl_typezd2errorzd2zz__errorz00
																																					(BGl_string2618z00zz__evalz00,
																																					BINT
																																					(24592L),
																																					BGl_string2741z00zz__evalz00,
																																					BGl_string2658z00zz__evalz00,
																																					((obj_t) BgL_duplicated1148z00_5063));
																																				FAILURE
																																					(BgL_auxz00_7849,
																																					BFALSE,
																																					BFALSE);
																																			}
																																	}
																																	BgL_auxz00_7819
																																		=
																																		(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1149z00_5107))->BgL_procz00);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(BgL_new1146z00_5064))->BgL_procz00) = ((obj_t) BgL_auxz00_7819), BUNSPEC);
																															}
																															{
																																obj_t
																																	BgL_auxz00_7856;
																																{	/* Eval/eval.scm 635 */
																																	BgL_z62errorz62_bglt
																																		BgL_tmp1150z00_5149;
																																	{	/* Eval/eval.scm 635 */
																																		bool_t
																																			BgL_test3294z00_7857;
																																		{	/* Eval/eval.scm 635 */
																																			obj_t
																																				BgL_classz00_5151;
																																			BgL_classz00_5151
																																				=
																																				BGl_z62errorz62zz__objectz00;
																																			{	/* Eval/eval.scm 635 */
																																				bool_t
																																					BgL_test3295z00_7858;
																																				{	/* Eval/eval.scm 635 */
																																					obj_t
																																						BgL_tmpz00_7859;
																																					BgL_tmpz00_7859
																																						=
																																						(
																																						(obj_t)
																																						BgL_duplicated1148z00_5063);
																																					BgL_test3295z00_7858
																																						=
																																						BGL_OBJECTP
																																						(BgL_tmpz00_7859);
																																				}
																																				if (BgL_test3295z00_7858)
																																					{	/* Eval/eval.scm 635 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_5153;
																																						long
																																							BgL_arg2225z00_5154;
																																						{	/* Eval/eval.scm 635 */
																																							obj_t
																																								BgL_tmpz00_7862;
																																							BgL_tmpz00_7862
																																								=
																																								(
																																								(obj_t)
																																								BgL_duplicated1148z00_5063);
																																							BgL_arg2224z00_5153
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_tmpz00_7862);
																																						}
																																						BgL_arg2225z00_5154
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_5151);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 635 */
																																								long
																																									BgL_idxz00_5162;
																																								BgL_idxz00_5162
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_5153);
																																								{	/* Eval/eval.scm 635 */
																																									obj_t
																																										BgL_arg2215z00_5163;
																																									{	/* Eval/eval.scm 635 */
																																										long
																																											BgL_arg2216z00_5164;
																																										BgL_arg2216z00_5164
																																											=
																																											(BgL_idxz00_5162
																																											+
																																											BgL_arg2225z00_5154);
																																										{	/* Eval/eval.scm 635 */
																																											obj_t
																																												BgL_vectorz00_5167;
																																											BgL_vectorz00_5167
																																												=
																																												BGl_za2inheritancesza2z00zz__objectz00;
																																											BgL_arg2215z00_5163
																																												=
																																												VECTOR_REF
																																												(BgL_vectorz00_5167,
																																												BgL_arg2216z00_5164);
																																									}}
																																									BgL_test3294z00_7857
																																										=
																																										(BgL_arg2215z00_5163
																																										==
																																										BgL_classz00_5151);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 635 */
																																								bool_t
																																									BgL_res2244z00_5169;
																																								{	/* Eval/eval.scm 635 */
																																									obj_t
																																										BgL_oclassz00_5173;
																																									{	/* Eval/eval.scm 635 */
																																										obj_t
																																											BgL_arg2229z00_5175;
																																										long
																																											BgL_arg2230z00_5176;
																																										BgL_arg2229z00_5175
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 635 */
																																											long
																																												BgL_arg2231z00_5177;
																																											BgL_arg2231z00_5177
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_5153);
																																											BgL_arg2230z00_5176
																																												=
																																												(BgL_arg2231z00_5177
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_5173
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_5175,
																																											BgL_arg2230z00_5176);
																																									}
																																									{	/* Eval/eval.scm 635 */
																																										bool_t
																																											BgL__ortest_1236z00_5183;
																																										BgL__ortest_1236z00_5183
																																											=
																																											(BgL_classz00_5151
																																											==
																																											BgL_oclassz00_5173);
																																										if (BgL__ortest_1236z00_5183)
																																											{	/* Eval/eval.scm 635 */
																																												BgL_res2244z00_5169
																																													=
																																													BgL__ortest_1236z00_5183;
																																											}
																																										else
																																											{	/* Eval/eval.scm 635 */
																																												long
																																													BgL_odepthz00_5184;
																																												{	/* Eval/eval.scm 635 */
																																													obj_t
																																														BgL_arg2219z00_5185;
																																													BgL_arg2219z00_5185
																																														=
																																														(BgL_oclassz00_5173);
																																													BgL_odepthz00_5184
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_5185);
																																												}
																																												if ((BgL_arg2225z00_5154 < BgL_odepthz00_5184))
																																													{	/* Eval/eval.scm 635 */
																																														obj_t
																																															BgL_arg2217z00_5189;
																																														{	/* Eval/eval.scm 635 */
																																															obj_t
																																																BgL_arg2218z00_5190;
																																															BgL_arg2218z00_5190
																																																=
																																																(BgL_oclassz00_5173);
																																															BgL_arg2217z00_5189
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_5190,
																																																BgL_arg2225z00_5154);
																																														}
																																														BgL_res2244z00_5169
																																															=
																																															(BgL_arg2217z00_5189
																																															==
																																															BgL_classz00_5151);
																																													}
																																												else
																																													{	/* Eval/eval.scm 635 */
																																														BgL_res2244z00_5169
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3294z00_7857
																																									=
																																									BgL_res2244z00_5169;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 635 */
																																						BgL_test3294z00_7857
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																		if (BgL_test3294z00_7857)
																																			{	/* Eval/eval.scm 635 */
																																				BgL_tmp1150z00_5149
																																					=
																																					(
																																					(BgL_z62errorz62_bglt)
																																					BgL_duplicated1148z00_5063);
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_auxz00_7886;
																																				BgL_auxz00_7886
																																					=
																																					BGl_typezd2errorzd2zz__errorz00
																																					(BGl_string2618z00zz__evalz00,
																																					BINT
																																					(24592L),
																																					BGl_string2741z00zz__evalz00,
																																					BGl_string2658z00zz__evalz00,
																																					((obj_t) BgL_duplicated1148z00_5063));
																																				FAILURE
																																					(BgL_auxz00_7886,
																																					BFALSE,
																																					BFALSE);
																																			}
																																	}
																																	BgL_auxz00_7856
																																		=
																																		(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1150z00_5149))->BgL_msgz00);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(BgL_new1146z00_5064))->BgL_msgz00) = ((obj_t) BgL_auxz00_7856), BUNSPEC);
																															}
																															{
																																obj_t
																																	BgL_auxz00_7893;
																																{	/* Eval/eval.scm 635 */
																																	BgL_z62errorz62_bglt
																																		BgL_tmp1151z00_5191;
																																	{	/* Eval/eval.scm 635 */
																																		bool_t
																																			BgL_test3299z00_7894;
																																		{	/* Eval/eval.scm 635 */
																																			obj_t
																																				BgL_classz00_5193;
																																			BgL_classz00_5193
																																				=
																																				BGl_z62errorz62zz__objectz00;
																																			{	/* Eval/eval.scm 635 */
																																				bool_t
																																					BgL_test3300z00_7895;
																																				{	/* Eval/eval.scm 635 */
																																					obj_t
																																						BgL_tmpz00_7896;
																																					BgL_tmpz00_7896
																																						=
																																						(
																																						(obj_t)
																																						BgL_duplicated1148z00_5063);
																																					BgL_test3300z00_7895
																																						=
																																						BGL_OBJECTP
																																						(BgL_tmpz00_7896);
																																				}
																																				if (BgL_test3300z00_7895)
																																					{	/* Eval/eval.scm 635 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_5195;
																																						long
																																							BgL_arg2225z00_5196;
																																						{	/* Eval/eval.scm 635 */
																																							obj_t
																																								BgL_tmpz00_7899;
																																							BgL_tmpz00_7899
																																								=
																																								(
																																								(obj_t)
																																								BgL_duplicated1148z00_5063);
																																							BgL_arg2224z00_5195
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_tmpz00_7899);
																																						}
																																						BgL_arg2225z00_5196
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_5193);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 635 */
																																								long
																																									BgL_idxz00_5204;
																																								BgL_idxz00_5204
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_5195);
																																								{	/* Eval/eval.scm 635 */
																																									obj_t
																																										BgL_arg2215z00_5205;
																																									{	/* Eval/eval.scm 635 */
																																										long
																																											BgL_arg2216z00_5206;
																																										BgL_arg2216z00_5206
																																											=
																																											(BgL_idxz00_5204
																																											+
																																											BgL_arg2225z00_5196);
																																										{	/* Eval/eval.scm 635 */
																																											obj_t
																																												BgL_vectorz00_5209;
																																											BgL_vectorz00_5209
																																												=
																																												BGl_za2inheritancesza2z00zz__objectz00;
																																											BgL_arg2215z00_5205
																																												=
																																												VECTOR_REF
																																												(BgL_vectorz00_5209,
																																												BgL_arg2216z00_5206);
																																									}}
																																									BgL_test3299z00_7894
																																										=
																																										(BgL_arg2215z00_5205
																																										==
																																										BgL_classz00_5193);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 635 */
																																								bool_t
																																									BgL_res2244z00_5211;
																																								{	/* Eval/eval.scm 635 */
																																									obj_t
																																										BgL_oclassz00_5215;
																																									{	/* Eval/eval.scm 635 */
																																										obj_t
																																											BgL_arg2229z00_5217;
																																										long
																																											BgL_arg2230z00_5218;
																																										BgL_arg2229z00_5217
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 635 */
																																											long
																																												BgL_arg2231z00_5219;
																																											BgL_arg2231z00_5219
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_5195);
																																											BgL_arg2230z00_5218
																																												=
																																												(BgL_arg2231z00_5219
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_5215
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_5217,
																																											BgL_arg2230z00_5218);
																																									}
																																									{	/* Eval/eval.scm 635 */
																																										bool_t
																																											BgL__ortest_1236z00_5225;
																																										BgL__ortest_1236z00_5225
																																											=
																																											(BgL_classz00_5193
																																											==
																																											BgL_oclassz00_5215);
																																										if (BgL__ortest_1236z00_5225)
																																											{	/* Eval/eval.scm 635 */
																																												BgL_res2244z00_5211
																																													=
																																													BgL__ortest_1236z00_5225;
																																											}
																																										else
																																											{	/* Eval/eval.scm 635 */
																																												long
																																													BgL_odepthz00_5226;
																																												{	/* Eval/eval.scm 635 */
																																													obj_t
																																														BgL_arg2219z00_5227;
																																													BgL_arg2219z00_5227
																																														=
																																														(BgL_oclassz00_5215);
																																													BgL_odepthz00_5226
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_5227);
																																												}
																																												if ((BgL_arg2225z00_5196 < BgL_odepthz00_5226))
																																													{	/* Eval/eval.scm 635 */
																																														obj_t
																																															BgL_arg2217z00_5231;
																																														{	/* Eval/eval.scm 635 */
																																															obj_t
																																																BgL_arg2218z00_5232;
																																															BgL_arg2218z00_5232
																																																=
																																																(BgL_oclassz00_5215);
																																															BgL_arg2217z00_5231
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_5232,
																																																BgL_arg2225z00_5196);
																																														}
																																														BgL_res2244z00_5211
																																															=
																																															(BgL_arg2217z00_5231
																																															==
																																															BgL_classz00_5193);
																																													}
																																												else
																																													{	/* Eval/eval.scm 635 */
																																														BgL_res2244z00_5211
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3299z00_7894
																																									=
																																									BgL_res2244z00_5211;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 635 */
																																						BgL_test3299z00_7894
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																		if (BgL_test3299z00_7894)
																																			{	/* Eval/eval.scm 635 */
																																				BgL_tmp1151z00_5191
																																					=
																																					(
																																					(BgL_z62errorz62_bglt)
																																					BgL_duplicated1148z00_5063);
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_auxz00_7923;
																																				BgL_auxz00_7923
																																					=
																																					BGl_typezd2errorzd2zz__errorz00
																																					(BGl_string2618z00zz__evalz00,
																																					BINT
																																					(24592L),
																																					BGl_string2741z00zz__evalz00,
																																					BGl_string2658z00zz__evalz00,
																																					((obj_t) BgL_duplicated1148z00_5063));
																																				FAILURE
																																					(BgL_auxz00_7923,
																																					BFALSE,
																																					BFALSE);
																																			}
																																	}
																																	BgL_auxz00_7893
																																		=
																																		(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1151z00_5191))->BgL_objz00);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(BgL_new1146z00_5064))->BgL_objz00) = ((obj_t) BgL_auxz00_7893), BUNSPEC);
																															}
																															BgL_auxz00_7778 =
																																BgL_new1146z00_5064;
																														}
																														BgL_nez00_5001 =
																															((obj_t)
																															BgL_auxz00_7778);
																													}
																												}
																											else
																												{	/* Eval/eval.scm 631 */
																													BgL_nez00_5001 =
																														BgL_ez00_5000;
																												}
																										}
																									else
																										{	/* Eval/eval.scm 631 */
																											BgL_nez00_5001 =
																												BgL_ez00_5000;
																										}
																								}
																							else
																								{	/* Eval/eval.scm 631 */
																									BgL_nez00_5001 =
																										BgL_ez00_5000;
																								}
																						}
																					else
																						{	/* Eval/eval.scm 631 */
																							BgL_nez00_5001 = BgL_ez00_5000;
																						}
																				}
																			else
																				{	/* Eval/eval.scm 631 */
																					BgL_nez00_5001 = BgL_ez00_5000;
																				}
																		}
																	}
																else
																	{	/* Eval/eval.scm 630 */
																		BgL_nez00_5001 = BgL_ez00_5000;
																	}
															}
														}
													else
														{	/* Eval/eval.scm 628 */
															BgL_nez00_5001 = BgL_ez00_5000;
														}
												}
												return BGl_raisez00zz__errorz00(BgL_ez00_5000);
											}
										}
									}
								else
									{	/* Eval/eval.scm 626 */
										return BgL_val1143z00_5241;
									}
							}
						}
					}
				}
			}
		}

	}



/* <@exit:1610>~0 */
	obj_t BGl_zc3z04exitza31610ze3ze70z60zz__evalz00(obj_t BgL_ez00_3743,
		obj_t BgL_xz00_3742, obj_t BgL_expdzd2evalzd2_3741,
		obj_t BgL_cell1139z00_3740, obj_t BgL_env1144z00_3739)
	{
		{	/* Eval/eval.scm 626 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1156z00_1740;

			if (SET_EXIT(BgL_an_exit1156z00_1740))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1156z00_1740 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1144z00_3739, BgL_an_exit1156z00_1740, 1L);
					{	/* Eval/eval.scm 626 */
						obj_t BgL_escape1140z00_1741;

						BgL_escape1140z00_1741 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1144z00_3739);
						{	/* Eval/eval.scm 626 */
							obj_t BgL_res1159z00_1742;

							{	/* Eval/eval.scm 626 */
								obj_t BgL_ohs1136z00_1743;

								BgL_ohs1136z00_1743 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1144z00_3739);
								{	/* Eval/eval.scm 626 */
									obj_t BgL_hds1137z00_1744;

									BgL_hds1137z00_1744 =
										MAKE_STACK_PAIR(BgL_escape1140z00_1741,
										BgL_cell1139z00_3740);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1144z00_3739,
										BgL_hds1137z00_1744);
									BUNSPEC;
									{	/* Eval/eval.scm 626 */
										obj_t BgL_exitd1153z00_1745;

										BgL_exitd1153z00_1745 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1144z00_3739);
										{	/* Eval/eval.scm 626 */
											obj_t BgL_tmp1155z00_1746;

											{	/* Eval/eval.scm 626 */
												obj_t BgL_arg1611z00_1748;

												BgL_arg1611z00_1748 =
													BGL_EXITD_PROTECT(BgL_exitd1153z00_1745);
												BgL_tmp1155z00_1746 =
													MAKE_YOUNG_PAIR(BgL_ohs1136z00_1743,
													BgL_arg1611z00_1748);
											}
											{	/* Eval/eval.scm 626 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1153z00_1745,
													BgL_tmp1155z00_1746);
												BUNSPEC;
												{	/* Eval/eval.scm 641 */
													obj_t BgL_tmp1154z00_1747;

													{	/* Eval/eval.scm 641 */
														obj_t BgL_tmpfunz00_7947;

														{	/* Eval/eval.scm 641 */
															bool_t BgL_test2510z00_4046;

															BgL_test2510z00_4046 =
																PROCEDUREP(BgL_expdzd2evalzd2_3741);
															if (BgL_test2510z00_4046)
																{	/* Eval/eval.scm 641 */
																	BgL_tmpfunz00_7947 = BgL_expdzd2evalzd2_3741;
																}
															else
																{
																	obj_t BgL_auxz00_7950;

																	BgL_auxz00_7950 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(24687L),
																		BGl_string2742z00zz__evalz00,
																		BGl_string2620z00zz__evalz00,
																		BgL_expdzd2evalzd2_3741);
																	FAILURE(BgL_auxz00_7950, BFALSE, BFALSE);
																}
														}
														BgL_tmp1154z00_1747 =
															(VA_PROCEDUREP(BgL_tmpfunz00_7947)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7947))
															(BgL_expdzd2evalzd2_3741, BgL_xz00_3742,
																BgL_ez00_3743, BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_7947))
															(BgL_expdzd2evalzd2_3741, BgL_xz00_3742,
																BgL_ez00_3743));
													}
													{	/* Eval/eval.scm 626 */
														bool_t BgL_test3305z00_7954;

														{	/* Eval/eval.scm 626 */
															obj_t BgL_arg2210z00_3022;

															BgL_arg2210z00_3022 =
																BGL_EXITD_PROTECT(BgL_exitd1153z00_1745);
															BgL_test3305z00_7954 = PAIRP(BgL_arg2210z00_3022);
														}
														if (BgL_test3305z00_7954)
															{	/* Eval/eval.scm 626 */
																obj_t BgL_arg2208z00_3023;

																{	/* Eval/eval.scm 626 */
																	obj_t BgL_arg2209z00_3024;

																	BgL_arg2209z00_3024 =
																		BGL_EXITD_PROTECT(BgL_exitd1153z00_1745);
																	{	/* Eval/eval.scm 626 */
																		obj_t BgL_pairz00_3025;

																		if (PAIRP(BgL_arg2209z00_3024))
																			{	/* Eval/eval.scm 626 */
																				BgL_pairz00_3025 = BgL_arg2209z00_3024;
																			}
																		else
																			{
																				obj_t BgL_auxz00_7960;

																				BgL_auxz00_7960 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(24341L),
																					BGl_string2742z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_arg2209z00_3024);
																				FAILURE(BgL_auxz00_7960, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2208z00_3023 = CDR(BgL_pairz00_3025);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1153z00_1745,
																	BgL_arg2208z00_3023);
																BUNSPEC;
															}
														else
															{	/* Eval/eval.scm 626 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1144z00_3739,
														BgL_ohs1136z00_1743);
													BUNSPEC;
													BgL_res1159z00_1742 = BgL_tmp1154z00_1747;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1144z00_3739);
							return BgL_res1159z00_1742;
						}
					}
				}
		}

	}



/* expand-define-hygiene-macro */
	BGL_EXPORTED_DEF obj_t
		BGl_expandzd2definezd2hygienezd2macrozd2zz__evalz00(obj_t BgL_xz00_54,
		obj_t BgL_ez00_55)
	{
		{	/* Eval/eval.scm 649 */
			{
				obj_t BgL_namez00_1842;
				obj_t BgL_argsz00_1843;
				obj_t BgL_bodyz00_1844;

				if (PAIRP(BgL_xz00_54))
					{	/* Eval/eval.scm 650 */
						obj_t BgL_cdrzd2340zd2_1849;

						BgL_cdrzd2340zd2_1849 = CDR(((obj_t) BgL_xz00_54));
						if (PAIRP(BgL_cdrzd2340zd2_1849))
							{	/* Eval/eval.scm 650 */
								obj_t BgL_carzd2344zd2_1851;

								BgL_carzd2344zd2_1851 = CAR(BgL_cdrzd2340zd2_1849);
								if (PAIRP(BgL_carzd2344zd2_1851))
									{	/* Eval/eval.scm 650 */
										obj_t BgL_cdrzd2349zd2_1853;

										BgL_cdrzd2349zd2_1853 = CDR(BgL_carzd2344zd2_1851);
										if (
											(CAR(BgL_carzd2344zd2_1851) ==
												BGl_symbol2750z00zz__evalz00))
											{	/* Eval/eval.scm 650 */
												if (PAIRP(BgL_cdrzd2349zd2_1853))
													{	/* Eval/eval.scm 650 */
														obj_t BgL_carzd2352zd2_1857;

														BgL_carzd2352zd2_1857 = CAR(BgL_cdrzd2349zd2_1853);
														if (PAIRP(BgL_carzd2352zd2_1857))
															{	/* Eval/eval.scm 650 */
																if (NULLP(CDR(BgL_cdrzd2349zd2_1853)))
																	{	/* Eval/eval.scm 650 */
																		BgL_namez00_1842 =
																			CAR(BgL_carzd2352zd2_1857);
																		BgL_argsz00_1843 =
																			CDR(BgL_carzd2352zd2_1857);
																		BgL_bodyz00_1844 =
																			CDR(BgL_cdrzd2340zd2_1849);
																		{	/* Eval/eval.scm 652 */
																			obj_t BgL_bodyz00_1866;
																			obj_t BgL_fnamez00_1867;
																			obj_t BgL_locz00_1868;

																			if (NULLP(BgL_bodyz00_1844))
																				{	/* Eval/eval.scm 652 */
																					BgL_bodyz00_1866 = BNIL;
																				}
																			else
																				{	/* Eval/eval.scm 652 */
																					obj_t BgL_head1250z00_1972;

																					{	/* Eval/eval.scm 652 */
																						obj_t BgL_arg1805z00_1985;

																						{	/* Eval/eval.scm 652 */
																							obj_t BgL_pairz00_3074;

																							{	/* Eval/eval.scm 652 */
																								obj_t BgL_pairz00_3073;

																								if (PAIRP(BgL_bodyz00_1844))
																									{	/* Eval/eval.scm 652 */
																										BgL_pairz00_3073 =
																											BgL_bodyz00_1844;
																									}
																								else
																									{
																										obj_t BgL_auxz00_7993;

																										BgL_auxz00_7993 =
																											BGl_typezd2errorzd2zz__errorz00
																											(BGl_string2618z00zz__evalz00,
																											BINT(25161L),
																											BGl_string2743z00zz__evalz00,
																											BGl_string2626z00zz__evalz00,
																											BgL_bodyz00_1844);
																										FAILURE(BgL_auxz00_7993,
																											BFALSE, BFALSE);
																									}
																								{	/* Eval/eval.scm 652 */
																									obj_t BgL_aux2515z00_4051;

																									BgL_aux2515z00_4051 =
																										CAR(BgL_pairz00_3073);
																									if (PAIRP
																										(BgL_aux2515z00_4051))
																										{	/* Eval/eval.scm 652 */
																											BgL_pairz00_3074 =
																												BgL_aux2515z00_4051;
																										}
																									else
																										{
																											obj_t BgL_auxz00_8000;

																											BgL_auxz00_8000 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string2618z00zz__evalz00,
																												BINT(25161L),
																												BGl_string2743z00zz__evalz00,
																												BGl_string2626z00zz__evalz00,
																												BgL_aux2515z00_4051);
																											FAILURE(BgL_auxz00_8000,
																												BFALSE, BFALSE);
																										}
																								}
																							}
																							{	/* Eval/eval.scm 652 */
																								obj_t BgL_pairz00_3077;

																								{	/* Eval/eval.scm 652 */
																									obj_t BgL_aux2517z00_4053;

																									BgL_aux2517z00_4053 =
																										CDR(BgL_pairz00_3074);
																									if (PAIRP
																										(BgL_aux2517z00_4053))
																										{	/* Eval/eval.scm 652 */
																											BgL_pairz00_3077 =
																												BgL_aux2517z00_4053;
																										}
																									else
																										{
																											obj_t BgL_auxz00_8007;

																											BgL_auxz00_8007 =
																												BGl_typezd2errorzd2zz__errorz00
																												(BGl_string2618z00zz__evalz00,
																												BINT(25161L),
																												BGl_string2743z00zz__evalz00,
																												BGl_string2626z00zz__evalz00,
																												BgL_aux2517z00_4053);
																											FAILURE(BgL_auxz00_8007,
																												BFALSE, BFALSE);
																										}
																								}
																								BgL_arg1805z00_1985 =
																									CAR(BgL_pairz00_3077);
																							}
																						}
																						BgL_head1250z00_1972 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1805z00_1985, BNIL);
																					}
																					{	/* Eval/eval.scm 652 */
																						obj_t BgL_g1253z00_1973;

																						{	/* Eval/eval.scm 652 */
																							obj_t BgL_pairz00_3078;

																							if (PAIRP(BgL_bodyz00_1844))
																								{	/* Eval/eval.scm 652 */
																									BgL_pairz00_3078 =
																										BgL_bodyz00_1844;
																								}
																							else
																								{
																									obj_t BgL_auxz00_8015;

																									BgL_auxz00_8015 =
																										BGl_typezd2errorzd2zz__errorz00
																										(BGl_string2618z00zz__evalz00,
																										BINT(25161L),
																										BGl_string2743z00zz__evalz00,
																										BGl_string2626z00zz__evalz00,
																										BgL_bodyz00_1844);
																									FAILURE(BgL_auxz00_8015,
																										BFALSE, BFALSE);
																								}
																							BgL_g1253z00_1973 =
																								CDR(BgL_pairz00_3078);
																						}
																						{
																							obj_t BgL_l1248z00_1975;
																							obj_t BgL_tail1251z00_1976;

																							BgL_l1248z00_1975 =
																								BgL_g1253z00_1973;
																							BgL_tail1251z00_1976 =
																								BgL_head1250z00_1972;
																						BgL_zc3z04anonymousza31799ze3z87_1977:
																							if (PAIRP
																								(BgL_l1248z00_1975))
																								{	/* Eval/eval.scm 652 */
																									obj_t BgL_newtail1252z00_1979;

																									{	/* Eval/eval.scm 652 */
																										obj_t BgL_arg1802z00_1981;

																										{	/* Eval/eval.scm 652 */
																											obj_t BgL_pairz00_3080;

																											{	/* Eval/eval.scm 652 */
																												obj_t
																													BgL_aux2521z00_4057;
																												BgL_aux2521z00_4057 =
																													CAR
																													(BgL_l1248z00_1975);
																												if (PAIRP
																													(BgL_aux2521z00_4057))
																													{	/* Eval/eval.scm 652 */
																														BgL_pairz00_3080 =
																															BgL_aux2521z00_4057;
																													}
																												else
																													{
																														obj_t
																															BgL_auxz00_8025;
																														BgL_auxz00_8025 =
																															BGl_typezd2errorzd2zz__errorz00
																															(BGl_string2618z00zz__evalz00,
																															BINT(25161L),
																															BGl_string2744z00zz__evalz00,
																															BGl_string2626z00zz__evalz00,
																															BgL_aux2521z00_4057);
																														FAILURE
																															(BgL_auxz00_8025,
																															BFALSE, BFALSE);
																													}
																											}
																											{	/* Eval/eval.scm 652 */
																												obj_t BgL_pairz00_3083;

																												{	/* Eval/eval.scm 652 */
																													obj_t
																														BgL_aux2523z00_4059;
																													BgL_aux2523z00_4059 =
																														CDR
																														(BgL_pairz00_3080);
																													if (PAIRP
																														(BgL_aux2523z00_4059))
																														{	/* Eval/eval.scm 652 */
																															BgL_pairz00_3083 =
																																BgL_aux2523z00_4059;
																														}
																													else
																														{
																															obj_t
																																BgL_auxz00_8032;
																															BgL_auxz00_8032 =
																																BGl_typezd2errorzd2zz__errorz00
																																(BGl_string2618z00zz__evalz00,
																																BINT(25161L),
																																BGl_string2744z00zz__evalz00,
																																BGl_string2626z00zz__evalz00,
																																BgL_aux2523z00_4059);
																															FAILURE
																																(BgL_auxz00_8032,
																																BFALSE, BFALSE);
																														}
																												}
																												BgL_arg1802z00_1981 =
																													CAR(BgL_pairz00_3083);
																											}
																										}
																										BgL_newtail1252z00_1979 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1802z00_1981,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1251z00_1976,
																										BgL_newtail1252z00_1979);
																									{
																										obj_t BgL_tail1251z00_8041;
																										obj_t BgL_l1248z00_8039;

																										BgL_l1248z00_8039 =
																											CDR(BgL_l1248z00_1975);
																										BgL_tail1251z00_8041 =
																											BgL_newtail1252z00_1979;
																										BgL_tail1251z00_1976 =
																											BgL_tail1251z00_8041;
																										BgL_l1248z00_1975 =
																											BgL_l1248z00_8039;
																										goto
																											BgL_zc3z04anonymousza31799ze3z87_1977;
																									}
																								}
																							else
																								{	/* Eval/eval.scm 652 */
																									if (NULLP(BgL_l1248z00_1975))
																										{	/* Eval/eval.scm 652 */
																											BgL_bodyz00_1866 =
																												BgL_head1250z00_1972;
																										}
																									else
																										{	/* Eval/eval.scm 652 */
																											BgL_bodyz00_1866 =
																												BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
																												(BGl_string2745z00zz__evalz00,
																												BGl_string2746z00zz__evalz00,
																												BgL_l1248z00_1975,
																												BGl_string2618z00zz__evalz00,
																												BINT(25161L));
																										}
																								}
																						}
																					}
																				}
																			{	/* Eval/eval.scm 653 */

																				{	/* Eval/eval.scm 653 */

																					BgL_fnamez00_1867 =
																						BGl_gensymz00zz__r4_symbols_6_4z00
																						(BFALSE);
																				}
																			}
																			{	/* Eval/eval.scm 654 */

																				{	/* Eval/eval.scm 654 */

																					BgL_locz00_1868 =
																						BGl_gensymz00zz__r4_symbols_6_4z00
																						(BFALSE);
																				}
																			}
																			{	/* Eval/eval.scm 657 */
																				obj_t BgL_arg1729z00_1869;

																				{	/* Eval/eval.scm 657 */
																					obj_t BgL_expdzd2lamzd2_1870;

																					{	/* Eval/eval.scm 657 */
																						obj_t BgL_arg1752z00_1928;

																						{	/* Eval/eval.scm 657 */
																							obj_t BgL_arg1753z00_1929;
																							obj_t BgL_arg1754z00_1930;

																							{	/* Eval/eval.scm 657 */
																								obj_t BgL_arg1755z00_1931;

																								BgL_arg1755z00_1931 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2698z00zz__evalz00,
																									BNIL);
																								BgL_arg1753z00_1929 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol2747z00zz__evalz00,
																									BgL_arg1755z00_1931);
																							}
																							{	/* Eval/eval.scm 658 */
																								obj_t BgL_arg1756z00_1932;

																								{	/* Eval/eval.scm 658 */
																									obj_t BgL_arg1757z00_1933;

																									{	/* Eval/eval.scm 658 */
																										obj_t BgL_arg1758z00_1934;
																										obj_t BgL_arg1759z00_1935;

																										{	/* Eval/eval.scm 658 */
																											obj_t BgL_arg1760z00_1936;
																											obj_t BgL_arg1761z00_1937;

																											{	/* Eval/eval.scm 658 */
																												obj_t
																													BgL_arg1762z00_1938;
																												BgL_arg1762z00_1938 =
																													MAKE_YOUNG_PAIR
																													(BFALSE, BNIL);
																												BgL_arg1760z00_1936 =
																													MAKE_YOUNG_PAIR
																													(BgL_fnamez00_1867,
																													BgL_arg1762z00_1938);
																											}
																											BgL_arg1761z00_1937 =
																												MAKE_YOUNG_PAIR
																												(BgL_locz00_1868, BNIL);
																											BgL_arg1758z00_1934 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1760z00_1936,
																												BgL_arg1761z00_1937);
																										}
																										{	/* Eval/eval.scm 659 */
																											obj_t BgL_arg1763z00_1939;
																											obj_t BgL_arg1764z00_1940;

																											{	/* Eval/eval.scm 659 */
																												obj_t
																													BgL_arg1765z00_1941;
																												{	/* Eval/eval.scm 659 */
																													obj_t
																														BgL_arg1766z00_1942;
																													obj_t
																														BgL_arg1767z00_1943;
																													{	/* Eval/eval.scm 659 */
																														obj_t
																															BgL_arg1768z00_1944;
																														BgL_arg1768z00_1944
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2747z00zz__evalz00,
																															BNIL);
																														BgL_arg1766z00_1942
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2702z00zz__evalz00,
																															BgL_arg1768z00_1944);
																													}
																													{	/* Eval/eval.scm 660 */
																														obj_t
																															BgL_arg1769z00_1945;
																														{	/* Eval/eval.scm 660 */
																															obj_t
																																BgL_arg1770z00_1946;
																															{	/* Eval/eval.scm 660 */
																																obj_t
																																	BgL_arg1771z00_1947;
																																obj_t
																																	BgL_arg1772z00_1948;
																																{	/* Eval/eval.scm 660 */
																																	obj_t
																																		BgL_arg1773z00_1949;
																																	BgL_arg1773z00_1949
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2747z00zz__evalz00,
																																		BNIL);
																																	BgL_arg1771z00_1947
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_symbol2704z00zz__evalz00,
																																		BgL_arg1773z00_1949);
																																}
																																{	/* Eval/eval.scm 661 */
																																	obj_t
																																		BgL_arg1774z00_1950;
																																	{	/* Eval/eval.scm 661 */
																																		obj_t
																																			BgL_arg1775z00_1951;
																																		obj_t
																																			BgL_arg1777z00_1952;
																																		{	/* Eval/eval.scm 661 */
																																			obj_t
																																				BgL_arg1779z00_1953;
																																			{	/* Eval/eval.scm 661 */
																																				obj_t
																																					BgL_arg1781z00_1954;
																																				BgL_arg1781z00_1954
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2706z00zz__evalz00,
																																					BNIL);
																																				BgL_arg1779z00_1953
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2708z00zz__evalz00,
																																					BgL_arg1781z00_1954);
																																			}
																																			BgL_arg1775z00_1951
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2630z00zz__evalz00,
																																				BgL_arg1779z00_1953);
																																		}
																																		{	/* Eval/eval.scm 662 */
																																			obj_t
																																				BgL_arg1782z00_1955;
																																			obj_t
																																				BgL_arg1783z00_1956;
																																			{	/* Eval/eval.scm 662 */
																																				obj_t
																																					BgL_arg1785z00_1957;
																																				{	/* Eval/eval.scm 662 */
																																					obj_t
																																						BgL_arg1786z00_1958;
																																					BgL_arg1786z00_1958
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2710z00zz__evalz00,
																																						BNIL);
																																					BgL_arg1785z00_1957
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_fnamez00_1867,
																																						BgL_arg1786z00_1958);
																																				}
																																				BgL_arg1782z00_1955
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_symbol2712z00zz__evalz00,
																																					BgL_arg1785z00_1957);
																																			}
																																			{	/* Eval/eval.scm 663 */
																																				obj_t
																																					BgL_arg1787z00_1959;
																																				{	/* Eval/eval.scm 663 */
																																					obj_t
																																						BgL_arg1788z00_1960;
																																					{	/* Eval/eval.scm 663 */
																																						obj_t
																																							BgL_arg1789z00_1961;
																																						BgL_arg1789z00_1961
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2714z00zz__evalz00,
																																							BNIL);
																																						BgL_arg1788z00_1960
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_locz00_1868,
																																							BgL_arg1789z00_1961);
																																					}
																																					BgL_arg1787z00_1959
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_symbol2712z00zz__evalz00,
																																						BgL_arg1788z00_1960);
																																				}
																																				BgL_arg1783z00_1956
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1787z00_1959,
																																					BNIL);
																																			}
																																			BgL_arg1777z00_1952
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1782z00_1955,
																																				BgL_arg1783z00_1956);
																																		}
																																		BgL_arg1774z00_1950
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1775z00_1951,
																																			BgL_arg1777z00_1952);
																																	}
																																	BgL_arg1772z00_1948
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1774z00_1950,
																																		BNIL);
																																}
																																BgL_arg1770z00_1946
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1771z00_1947,
																																	BgL_arg1772z00_1948);
																															}
																															BgL_arg1769z00_1945
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2716z00zz__evalz00,
																																BgL_arg1770z00_1946);
																														}
																														BgL_arg1767z00_1943
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1769z00_1945,
																															BNIL);
																													}
																													BgL_arg1765z00_1941 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1766z00_1942,
																														BgL_arg1767z00_1943);
																												}
																												BgL_arg1763z00_1939 =
																													MAKE_YOUNG_PAIR
																													(BGl_symbol2718z00zz__evalz00,
																													BgL_arg1765z00_1941);
																											}
																											{	/* Eval/eval.scm 664 */
																												obj_t
																													BgL_arg1790z00_1962;
																												{	/* Eval/eval.scm 664 */
																													obj_t
																														BgL_arg1791z00_1963;
																													{	/* Eval/eval.scm 664 */
																														obj_t
																															BgL_arg1792z00_1964;
																														obj_t
																															BgL_arg1793z00_1965;
																														{	/* Eval/eval.scm 664 */
																															obj_t
																																BgL_arg1794z00_1966;
																															{	/* Eval/eval.scm 664 */
																																obj_t
																																	BgL_arg1795z00_1967;
																																obj_t
																																	BgL_arg1796z00_1968;
																																BgL_arg1795z00_1967
																																	=
																																	BGl_loopze70ze7zz__evalz00
																																	(BgL_namez00_1842,
																																	BgL_locz00_1868,
																																	BgL_fnamez00_1867,
																																	BgL_argsz00_1843,
																																	BGl_list2749z00zz__evalz00,
																																	BNIL);
																																{	/* Eval/eval.scm 667 */
																																	obj_t
																																		BgL_arg1797z00_1969;
																																	{	/* Eval/eval.scm 667 */
																																		obj_t
																																			BgL_auxz00_8077;
																																		{	/* Eval/eval.scm 667 */
																																			bool_t
																																				BgL_test3323z00_8078;
																																			if (PAIRP
																																				(BgL_bodyz00_1866))
																																				{	/* Eval/eval.scm 667 */
																																					BgL_test3323z00_8078
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Eval/eval.scm 667 */
																																					BgL_test3323z00_8078
																																						=
																																						NULLP
																																						(BgL_bodyz00_1866);
																																				}
																																			if (BgL_test3323z00_8078)
																																				{	/* Eval/eval.scm 667 */
																																					BgL_auxz00_8077
																																						=
																																						BgL_bodyz00_1866;
																																				}
																																			else
																																				{
																																					obj_t
																																						BgL_auxz00_8082;
																																					BgL_auxz00_8082
																																						=
																																						BGl_typezd2errorzd2zz__errorz00
																																						(BGl_string2618z00zz__evalz00,
																																						BINT
																																						(25543L),
																																						BGl_string2743z00zz__evalz00,
																																						BGl_string2681z00zz__evalz00,
																																						BgL_bodyz00_1866);
																																					FAILURE
																																						(BgL_auxz00_8082,
																																						BFALSE,
																																						BFALSE);
																																				}
																																		}
																																		BgL_arg1797z00_1969
																																			=
																																			BGl_expandzd2prognzd2zz__prognz00
																																			(BgL_auxz00_8077);
																																	}
																																	BgL_arg1796z00_1968
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1797z00_1969,
																																		BNIL);
																																}
																																BgL_arg1794z00_1966
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1795z00_1967,
																																	BgL_arg1796z00_1968);
																															}
																															BgL_arg1792z00_1964
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2724z00zz__evalz00,
																																BgL_arg1794z00_1966);
																														}
																														BgL_arg1793z00_1965
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2698z00zz__evalz00,
																															BNIL);
																														BgL_arg1791z00_1963
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1792z00_1964,
																															BgL_arg1793z00_1965);
																													}
																													BgL_arg1790z00_1962 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2698z00zz__evalz00,
																														BgL_arg1791z00_1963);
																												}
																												BgL_arg1764z00_1940 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1790z00_1962,
																													BNIL);
																											}
																											BgL_arg1759z00_1935 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1763z00_1939,
																												BgL_arg1764z00_1940);
																										}
																										BgL_arg1757z00_1933 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1758z00_1934,
																											BgL_arg1759z00_1935);
																									}
																									BgL_arg1756z00_1932 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2732z00zz__evalz00,
																										BgL_arg1757z00_1933);
																								}
																								BgL_arg1754z00_1930 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1756z00_1932, BNIL);
																							}
																							BgL_arg1752z00_1928 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1753z00_1929,
																								BgL_arg1754z00_1930);
																						}
																						BgL_expdzd2lamzd2_1870 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2734z00zz__evalz00,
																							BgL_arg1752z00_1928);
																					}
																					{	/* Eval/eval.scm 657 */
																						obj_t BgL_expdzd2lamzf2locz20_1871;

																						BgL_expdzd2lamzf2locz20_1871 =
																							BGl_evepairifyz00zz__prognz00
																							(BgL_expdzd2lamzd2_1870,
																							BgL_xz00_54);
																						{	/* Eval/eval.scm 669 */
																							obj_t BgL_expdzd2evalzd2_1872;

																							{	/* Eval/eval.scm 82 */
																								obj_t BgL_envz00_1927;

																								{	/* Eval/eval.scm 257 */
																									obj_t BgL_mz00_3086;

																									BgL_mz00_3086 =
																										BGl_evalzd2modulezd2zz__evmodulez00
																										();
																									if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_3086))
																										{	/* Eval/eval.scm 258 */
																											BgL_envz00_1927 =
																												BgL_mz00_3086;
																										}
																									else
																										{	/* Eval/eval.scm 258 */
																											BgL_envz00_1927 =
																												BGl_symbol2616z00zz__evalz00;
																										}
																								}
																								{	/* Eval/eval.scm 82 */

																									{	/* Eval/eval.scm 176 */
																										obj_t BgL_evaluatez00_3088;

																										if (PROCEDUREP
																											(BGl_defaultzd2evaluatezd2zz__evalz00))
																											{	/* Eval/eval.scm 176 */
																												BgL_evaluatez00_3088 =
																													BGl_defaultzd2evaluatezd2zz__evalz00;
																											}
																										else
																											{	/* Eval/eval.scm 176 */
																												BgL_evaluatez00_3088 =
																													BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																											}
																										{	/* Eval/eval.scm 179 */
																											obj_t BgL_auxz00_8106;

																											if (PROCEDUREP
																												(BgL_evaluatez00_3088))
																												{	/* Eval/eval.scm 179 */
																													BgL_auxz00_8106 =
																														BgL_evaluatez00_3088;
																												}
																											else
																												{
																													obj_t BgL_auxz00_8109;

																													BgL_auxz00_8109 =
																														BGl_typezd2errorzd2zz__errorz00
																														(BGl_string2618z00zz__evalz00,
																														BINT(6857L),
																														BGl_string2743z00zz__evalz00,
																														BGl_string2620z00zz__evalz00,
																														BgL_evaluatez00_3088);
																													FAILURE
																														(BgL_auxz00_8109,
																														BFALSE, BFALSE);
																												}
																											BgL_expdzd2evalzd2_1872 =
																												BGl_evalzf2expanderzf2zz__evalz00
																												(BgL_expdzd2lamzf2locz20_1871,
																												BgL_envz00_1927,
																												BGl_expandz12zd2envzc0zz__expandz00,
																												BgL_auxz00_8106);
																										}
																									}
																								}
																							}
																							{	/* Eval/eval.scm 670 */

																								{	/* Eval/eval.scm 671 */
																									obj_t
																										BgL_zc3z04anonymousza31730ze3z87_3695;
																									BgL_zc3z04anonymousza31730ze3z87_3695
																										=
																										MAKE_FX_PROCEDURE
																										(BGl_z62zc3z04anonymousza31730ze3ze5zz__evalz00,
																										(int) (2L), (int) (1L));
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza31730ze3z87_3695,
																										(int) (0L),
																										BgL_expdzd2evalzd2_1872);
																									BgL_arg1729z00_1869 =
																										BgL_zc3z04anonymousza31730ze3z87_3695;
																				}}}}}
																				BGl_installzd2expanderzd2zz__macroz00
																					(BgL_namez00_1842,
																					BgL_arg1729z00_1869);
																			}
																			return BUNSPEC;
																		}
																	}
																else
																	{	/* Eval/eval.scm 691 */
																		obj_t BgL_procz00_3154;

																		BgL_procz00_3154 =
																			BGl_symbol2752z00zz__evalz00;
																		if (EPAIRP(BgL_xz00_54))
																			{	/* Eval/eval.scm 547 */
																				obj_t BgL_arg1524z00_3156;

																				{	/* Eval/eval.scm 547 */
																					obj_t BgL_objz00_3157;

																					if (EPAIRP(BgL_xz00_54))
																						{	/* Eval/eval.scm 547 */
																							BgL_objz00_3157 = BgL_xz00_54;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8127;

																							BgL_auxz00_8127 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(21769L),
																								BGl_string2754z00zz__evalz00,
																								BGl_string2676z00zz__evalz00,
																								BgL_xz00_54);
																							FAILURE(BgL_auxz00_8127, BFALSE,
																								BFALSE);
																						}
																					BgL_arg1524z00_3156 =
																						CER(BgL_objz00_3157);
																				}
																				return
																					BGl_everrorz00zz__everrorz00
																					(BgL_arg1524z00_3156,
																					BgL_procz00_3154,
																					BGl_string2755z00zz__evalz00,
																					BgL_xz00_54);
																			}
																		else
																			{	/* Eval/eval.scm 546 */
																				return
																					BGl_errorz00zz__errorz00
																					(BgL_procz00_3154,
																					BGl_string2755z00zz__evalz00,
																					BgL_xz00_54);
																			}
																	}
															}
														else
															{	/* Eval/eval.scm 691 */
																obj_t BgL_procz00_3158;

																BgL_procz00_3158 = BGl_symbol2752z00zz__evalz00;
																if (EPAIRP(BgL_xz00_54))
																	{	/* Eval/eval.scm 547 */
																		obj_t BgL_arg1524z00_3160;

																		{	/* Eval/eval.scm 547 */
																			obj_t BgL_objz00_3161;

																			if (EPAIRP(BgL_xz00_54))
																				{	/* Eval/eval.scm 547 */
																					BgL_objz00_3161 = BgL_xz00_54;
																				}
																			else
																				{
																					obj_t BgL_auxz00_8138;

																					BgL_auxz00_8138 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(21769L),
																						BGl_string2754z00zz__evalz00,
																						BGl_string2676z00zz__evalz00,
																						BgL_xz00_54);
																					FAILURE(BgL_auxz00_8138, BFALSE,
																						BFALSE);
																				}
																			BgL_arg1524z00_3160 =
																				CER(BgL_objz00_3161);
																		}
																		return
																			BGl_everrorz00zz__everrorz00
																			(BgL_arg1524z00_3160, BgL_procz00_3158,
																			BGl_string2755z00zz__evalz00,
																			BgL_xz00_54);
																	}
																else
																	{	/* Eval/eval.scm 546 */
																		return
																			BGl_errorz00zz__errorz00(BgL_procz00_3158,
																			BGl_string2755z00zz__evalz00,
																			BgL_xz00_54);
																	}
															}
													}
												else
													{	/* Eval/eval.scm 691 */
														obj_t BgL_procz00_3162;

														BgL_procz00_3162 = BGl_symbol2752z00zz__evalz00;
														if (EPAIRP(BgL_xz00_54))
															{	/* Eval/eval.scm 547 */
																obj_t BgL_arg1524z00_3164;

																{	/* Eval/eval.scm 547 */
																	obj_t BgL_objz00_3165;

																	if (EPAIRP(BgL_xz00_54))
																		{	/* Eval/eval.scm 547 */
																			BgL_objz00_3165 = BgL_xz00_54;
																		}
																	else
																		{
																			obj_t BgL_auxz00_8149;

																			BgL_auxz00_8149 =
																				BGl_typezd2errorzd2zz__errorz00
																				(BGl_string2618z00zz__evalz00,
																				BINT(21769L),
																				BGl_string2754z00zz__evalz00,
																				BGl_string2676z00zz__evalz00,
																				BgL_xz00_54);
																			FAILURE(BgL_auxz00_8149, BFALSE, BFALSE);
																		}
																	BgL_arg1524z00_3164 = CER(BgL_objz00_3165);
																}
																return
																	BGl_everrorz00zz__everrorz00
																	(BgL_arg1524z00_3164, BgL_procz00_3162,
																	BGl_string2755z00zz__evalz00, BgL_xz00_54);
															}
														else
															{	/* Eval/eval.scm 546 */
																return
																	BGl_errorz00zz__errorz00(BgL_procz00_3162,
																	BGl_string2755z00zz__evalz00, BgL_xz00_54);
															}
													}
											}
										else
											{	/* Eval/eval.scm 691 */
												obj_t BgL_procz00_3166;

												BgL_procz00_3166 = BGl_symbol2752z00zz__evalz00;
												if (EPAIRP(BgL_xz00_54))
													{	/* Eval/eval.scm 547 */
														obj_t BgL_arg1524z00_3168;

														{	/* Eval/eval.scm 547 */
															obj_t BgL_objz00_3169;

															if (EPAIRP(BgL_xz00_54))
																{	/* Eval/eval.scm 547 */
																	BgL_objz00_3169 = BgL_xz00_54;
																}
															else
																{
																	obj_t BgL_auxz00_8160;

																	BgL_auxz00_8160 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(21769L),
																		BGl_string2754z00zz__evalz00,
																		BGl_string2676z00zz__evalz00, BgL_xz00_54);
																	FAILURE(BgL_auxz00_8160, BFALSE, BFALSE);
																}
															BgL_arg1524z00_3168 = CER(BgL_objz00_3169);
														}
														return
															BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3168,
															BgL_procz00_3166, BGl_string2755z00zz__evalz00,
															BgL_xz00_54);
													}
												else
													{	/* Eval/eval.scm 546 */
														return
															BGl_errorz00zz__errorz00(BgL_procz00_3166,
															BGl_string2755z00zz__evalz00, BgL_xz00_54);
													}
											}
									}
								else
									{	/* Eval/eval.scm 691 */
										obj_t BgL_procz00_3170;

										BgL_procz00_3170 = BGl_symbol2752z00zz__evalz00;
										if (EPAIRP(BgL_xz00_54))
											{	/* Eval/eval.scm 547 */
												obj_t BgL_arg1524z00_3172;

												{	/* Eval/eval.scm 547 */
													obj_t BgL_objz00_3173;

													if (EPAIRP(BgL_xz00_54))
														{	/* Eval/eval.scm 547 */
															BgL_objz00_3173 = BgL_xz00_54;
														}
													else
														{
															obj_t BgL_auxz00_8171;

															BgL_auxz00_8171 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2618z00zz__evalz00, BINT(21769L),
																BGl_string2754z00zz__evalz00,
																BGl_string2676z00zz__evalz00, BgL_xz00_54);
															FAILURE(BgL_auxz00_8171, BFALSE, BFALSE);
														}
													BgL_arg1524z00_3172 = CER(BgL_objz00_3173);
												}
												return
													BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3172,
													BgL_procz00_3170, BGl_string2755z00zz__evalz00,
													BgL_xz00_54);
											}
										else
											{	/* Eval/eval.scm 546 */
												return
													BGl_errorz00zz__errorz00(BgL_procz00_3170,
													BGl_string2755z00zz__evalz00, BgL_xz00_54);
											}
									}
							}
						else
							{	/* Eval/eval.scm 691 */
								obj_t BgL_procz00_3174;

								BgL_procz00_3174 = BGl_symbol2752z00zz__evalz00;
								if (EPAIRP(BgL_xz00_54))
									{	/* Eval/eval.scm 547 */
										obj_t BgL_arg1524z00_3176;

										{	/* Eval/eval.scm 547 */
											obj_t BgL_objz00_3177;

											if (EPAIRP(BgL_xz00_54))
												{	/* Eval/eval.scm 547 */
													BgL_objz00_3177 = BgL_xz00_54;
												}
											else
												{
													obj_t BgL_auxz00_8182;

													BgL_auxz00_8182 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2618z00zz__evalz00, BINT(21769L),
														BGl_string2754z00zz__evalz00,
														BGl_string2676z00zz__evalz00, BgL_xz00_54);
													FAILURE(BgL_auxz00_8182, BFALSE, BFALSE);
												}
											BgL_arg1524z00_3176 = CER(BgL_objz00_3177);
										}
										return
											BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3176,
											BgL_procz00_3174, BGl_string2755z00zz__evalz00,
											BgL_xz00_54);
									}
								else
									{	/* Eval/eval.scm 546 */
										return
											BGl_errorz00zz__errorz00(BgL_procz00_3174,
											BGl_string2755z00zz__evalz00, BgL_xz00_54);
									}
							}
					}
				else
					{	/* Eval/eval.scm 691 */
						obj_t BgL_procz00_3178;

						BgL_procz00_3178 = BGl_symbol2752z00zz__evalz00;
						if (EPAIRP(BgL_xz00_54))
							{	/* Eval/eval.scm 547 */
								obj_t BgL_arg1524z00_3180;

								{	/* Eval/eval.scm 547 */
									obj_t BgL_objz00_3181;

									if (EPAIRP(BgL_xz00_54))
										{	/* Eval/eval.scm 547 */
											BgL_objz00_3181 = BgL_xz00_54;
										}
									else
										{
											obj_t BgL_auxz00_8193;

											BgL_auxz00_8193 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2618z00zz__evalz00, BINT(21769L),
												BGl_string2754z00zz__evalz00,
												BGl_string2676z00zz__evalz00, BgL_xz00_54);
											FAILURE(BgL_auxz00_8193, BFALSE, BFALSE);
										}
									BgL_arg1524z00_3180 = CER(BgL_objz00_3181);
								}
								return
									BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3180,
									BgL_procz00_3178, BGl_string2755z00zz__evalz00, BgL_xz00_54);
							}
						else
							{	/* Eval/eval.scm 546 */
								return
									BGl_errorz00zz__errorz00(BgL_procz00_3178,
									BGl_string2755z00zz__evalz00, BgL_xz00_54);
							}
					}
			}
		}

	}



/* &expand-define-hygiene-macro */
	obj_t BGl_z62expandzd2definezd2hygienezd2macrozb0zz__evalz00(obj_t
		BgL_envz00_3696, obj_t BgL_xz00_3697, obj_t BgL_ez00_3698)
	{
		{	/* Eval/eval.scm 649 */
			return
				BGl_expandzd2definezd2hygienezd2macrozd2zz__evalz00(BgL_xz00_3697,
				BgL_ez00_3698);
		}

	}



/* &<@anonymous:1730> */
	obj_t BGl_z62zc3z04anonymousza31730ze3ze5zz__evalz00(obj_t BgL_envz00_3699,
		obj_t BgL_xz00_3701, obj_t BgL_ez00_3702)
	{
		{	/* Eval/eval.scm 671 */
			{	/* Eval/eval.scm 672 */
				obj_t BgL_expdzd2evalzd2_3700;

				BgL_expdzd2evalzd2_3700 = PROCEDURE_REF(BgL_envz00_3699, (int) (0L));
				{	/* Eval/eval.scm 672 */
					obj_t BgL_env1169z00_5451;

					BgL_env1169z00_5451 = BGL_CURRENT_DYNAMIC_ENV();
					{
						obj_t BgL_ez00_5453;

						{	/* Eval/eval.scm 672 */
							obj_t BgL_cell1164z00_5693;

							BgL_cell1164z00_5693 = MAKE_STACK_CELL(BUNSPEC);
							{	/* Eval/eval.scm 672 */
								obj_t BgL_val1168z00_5694;

								BgL_val1168z00_5694 =
									BGl_zc3z04exitza31732ze3ze70z60zz__evalz00(BgL_ez00_3702,
									BgL_xz00_3701, BgL_expdzd2evalzd2_3700, BgL_cell1164z00_5693,
									BgL_env1169z00_5451);
								if ((BgL_val1168z00_5694 == BgL_cell1164z00_5693))
									{	/* Eval/eval.scm 672 */
										{	/* Eval/eval.scm 672 */
											int BgL_tmpz00_8208;

											BgL_tmpz00_8208 = (int) (0L);
											BGL_SIGSETMASK(BgL_tmpz00_8208);
										}
										{	/* Eval/eval.scm 672 */
											obj_t BgL_arg1731z00_5695;

											BgL_arg1731z00_5695 =
												CELL_REF(((obj_t) BgL_val1168z00_5694));
											BgL_ez00_5453 = BgL_arg1731z00_5695;
											{	/* Eval/eval.scm 674 */
												obj_t BgL_nez00_5454;

												{	/* Eval/eval.scm 674 */
													bool_t BgL_test3343z00_8213;

													{	/* Eval/eval.scm 674 */
														obj_t BgL_classz00_5455;

														BgL_classz00_5455 = BGl_z62errorz62zz__objectz00;
														if (BGL_OBJECTP(BgL_ez00_5453))
															{	/* Eval/eval.scm 674 */
																BgL_objectz00_bglt BgL_arg2222z00_5456;

																BgL_arg2222z00_5456 =
																	(BgL_objectz00_bglt) (BgL_ez00_5453);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Eval/eval.scm 674 */
																		long BgL_idxz00_5457;

																		BgL_idxz00_5457 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg2222z00_5456);
																		{	/* Eval/eval.scm 674 */
																			obj_t BgL_arg2215z00_5458;

																			{	/* Eval/eval.scm 674 */
																				long BgL_arg2216z00_5459;

																				BgL_arg2216z00_5459 =
																					(BgL_idxz00_5457 + 3L);
																				{	/* Eval/eval.scm 674 */
																					obj_t BgL_vectorz00_5460;

																					{	/* Eval/eval.scm 674 */
																						obj_t BgL_aux2545z00_5461;

																						BgL_aux2545z00_5461 =
																							BGl_za2inheritancesza2z00zz__objectz00;
																						if (VECTORP(BgL_aux2545z00_5461))
																							{	/* Eval/eval.scm 674 */
																								BgL_vectorz00_5460 =
																									BgL_aux2545z00_5461;
																							}
																						else
																							{
																								obj_t BgL_auxz00_8223;

																								BgL_auxz00_8223 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2618z00zz__evalz00,
																									BINT(25726L),
																									BGl_string2756z00zz__evalz00,
																									BGl_string2628z00zz__evalz00,
																									BgL_aux2545z00_5461);
																								FAILURE(BgL_auxz00_8223, BFALSE,
																									BFALSE);
																							}
																					}
																					BgL_arg2215z00_5458 =
																						VECTOR_REF(BgL_vectorz00_5460,
																						BgL_arg2216z00_5459);
																				}
																			}
																			BgL_test3343z00_8213 =
																				(BgL_arg2215z00_5458 ==
																				BgL_classz00_5455);
																		}
																	}
																else
																	{	/* Eval/eval.scm 674 */
																		bool_t BgL_res2241z00_5462;

																		{	/* Eval/eval.scm 674 */
																			obj_t BgL_oclassz00_5463;

																			{	/* Eval/eval.scm 674 */
																				obj_t BgL_arg2229z00_5464;
																				long BgL_arg2230z00_5465;

																				BgL_arg2229z00_5464 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Eval/eval.scm 674 */
																					long BgL_arg2231z00_5466;

																					BgL_arg2231z00_5466 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg2222z00_5456);
																					BgL_arg2230z00_5465 =
																						(BgL_arg2231z00_5466 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_5463 =
																					VECTOR_REF(BgL_arg2229z00_5464,
																					BgL_arg2230z00_5465);
																			}
																			{	/* Eval/eval.scm 674 */
																				bool_t BgL__ortest_1236z00_5467;

																				BgL__ortest_1236z00_5467 =
																					(BgL_classz00_5455 ==
																					BgL_oclassz00_5463);
																				if (BgL__ortest_1236z00_5467)
																					{	/* Eval/eval.scm 674 */
																						BgL_res2241z00_5462 =
																							BgL__ortest_1236z00_5467;
																					}
																				else
																					{	/* Eval/eval.scm 674 */
																						long BgL_odepthz00_5468;

																						{	/* Eval/eval.scm 674 */
																							obj_t BgL_arg2219z00_5469;

																							BgL_arg2219z00_5469 =
																								(BgL_oclassz00_5463);
																							BgL_odepthz00_5468 =
																								BGL_CLASS_DEPTH
																								(BgL_arg2219z00_5469);
																						}
																						if ((3L < BgL_odepthz00_5468))
																							{	/* Eval/eval.scm 674 */
																								obj_t BgL_arg2217z00_5470;

																								{	/* Eval/eval.scm 674 */
																									obj_t BgL_arg2218z00_5471;

																									BgL_arg2218z00_5471 =
																										(BgL_oclassz00_5463);
																									BgL_arg2217z00_5470 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg2218z00_5471, 3L);
																								}
																								BgL_res2241z00_5462 =
																									(BgL_arg2217z00_5470 ==
																									BgL_classz00_5455);
																							}
																						else
																							{	/* Eval/eval.scm 674 */
																								BgL_res2241z00_5462 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3343z00_8213 = BgL_res2241z00_5462;
																	}
															}
														else
															{	/* Eval/eval.scm 674 */
																BgL_test3343z00_8213 = ((bool_t) 0);
															}
													}
													if (BgL_test3343z00_8213)
														{	/* Eval/eval.scm 675 */
															BgL_z62errorz62_bglt BgL_i1170z00_5472;

															{	/* Eval/eval.scm 676 */
																bool_t BgL_test3349z00_8242;

																{	/* Eval/eval.scm 676 */
																	obj_t BgL_classz00_5473;

																	BgL_classz00_5473 =
																		BGl_z62errorz62zz__objectz00;
																	if (BGL_OBJECTP(BgL_ez00_5453))
																		{	/* Eval/eval.scm 676 */
																			BgL_objectz00_bglt BgL_arg2224z00_5475;
																			long BgL_arg2225z00_5476;

																			BgL_arg2224z00_5475 =
																				(BgL_objectz00_bglt) (BgL_ez00_5453);
																			BgL_arg2225z00_5476 =
																				BGL_CLASS_DEPTH(BgL_classz00_5473);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Eval/eval.scm 676 */
																					long BgL_idxz00_5484;

																					BgL_idxz00_5484 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg2224z00_5475);
																					{	/* Eval/eval.scm 676 */
																						obj_t BgL_arg2215z00_5485;

																						{	/* Eval/eval.scm 676 */
																							long BgL_arg2216z00_5486;

																							BgL_arg2216z00_5486 =
																								(BgL_idxz00_5484 +
																								BgL_arg2225z00_5476);
																							BgL_arg2215z00_5485 =
																								VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																								BgL_arg2216z00_5486);
																						}
																						BgL_test3349z00_8242 =
																							(BgL_arg2215z00_5485 ==
																							BgL_classz00_5473);
																				}}
																			else
																				{	/* Eval/eval.scm 676 */
																					bool_t BgL_res2244z00_5491;

																					{	/* Eval/eval.scm 676 */
																						obj_t BgL_oclassz00_5495;

																						{	/* Eval/eval.scm 676 */
																							obj_t BgL_arg2229z00_5497;
																							long BgL_arg2230z00_5498;

																							BgL_arg2229z00_5497 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Eval/eval.scm 676 */
																								long BgL_arg2231z00_5499;

																								BgL_arg2231z00_5499 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg2224z00_5475);
																								BgL_arg2230z00_5498 =
																									(BgL_arg2231z00_5499 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_5495 =
																								VECTOR_REF(BgL_arg2229z00_5497,
																								BgL_arg2230z00_5498);
																						}
																						{	/* Eval/eval.scm 676 */
																							bool_t BgL__ortest_1236z00_5505;

																							BgL__ortest_1236z00_5505 =
																								(BgL_classz00_5473 ==
																								BgL_oclassz00_5495);
																							if (BgL__ortest_1236z00_5505)
																								{	/* Eval/eval.scm 676 */
																									BgL_res2244z00_5491 =
																										BgL__ortest_1236z00_5505;
																								}
																							else
																								{	/* Eval/eval.scm 676 */
																									long BgL_odepthz00_5506;

																									{	/* Eval/eval.scm 676 */
																										obj_t BgL_arg2219z00_5507;

																										BgL_arg2219z00_5507 =
																											(BgL_oclassz00_5495);
																										BgL_odepthz00_5506 =
																											BGL_CLASS_DEPTH
																											(BgL_arg2219z00_5507);
																									}
																									if (
																										(BgL_arg2225z00_5476 <
																											BgL_odepthz00_5506))
																										{	/* Eval/eval.scm 676 */
																											obj_t BgL_arg2217z00_5511;

																											{	/* Eval/eval.scm 676 */
																												obj_t
																													BgL_arg2218z00_5512;
																												BgL_arg2218z00_5512 =
																													(BgL_oclassz00_5495);
																												BgL_arg2217z00_5511 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg2218z00_5512,
																													BgL_arg2225z00_5476);
																											}
																											BgL_res2244z00_5491 =
																												(BgL_arg2217z00_5511 ==
																												BgL_classz00_5473);
																										}
																									else
																										{	/* Eval/eval.scm 676 */
																											BgL_res2244z00_5491 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test3349z00_8242 =
																						BgL_res2244z00_5491;
																				}
																		}
																	else
																		{	/* Eval/eval.scm 676 */
																			BgL_test3349z00_8242 = ((bool_t) 0);
																		}
																}
																if (BgL_test3349z00_8242)
																	{	/* Eval/eval.scm 676 */
																		BgL_i1170z00_5472 =
																			((BgL_z62errorz62_bglt) BgL_ez00_5453);
																	}
																else
																	{
																		obj_t BgL_auxz00_8267;

																		BgL_auxz00_8267 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2618z00zz__evalz00,
																			BINT(25785L),
																			BGl_string2756z00zz__evalz00,
																			BGl_string2658z00zz__evalz00,
																			BgL_ez00_5453);
																		FAILURE(BgL_auxz00_8267, BFALSE, BFALSE);
																	}
															}
															{	/* Eval/eval.scm 676 */
																bool_t BgL_test3354z00_8271;

																{	/* Eval/eval.scm 676 */
																	obj_t BgL_tmpz00_8272;

																	BgL_tmpz00_8272 =
																		(((BgL_z62errorz62_bglt)
																			COBJECT(BgL_i1170z00_5472))->BgL_objz00);
																	BgL_test3354z00_8271 =
																		EPAIRP(BgL_tmpz00_8272);
																}
																if (BgL_test3354z00_8271)
																	{
																		obj_t BgL_fnamez00_5514;
																		obj_t BgL_locz00_5515;

																		{	/* Eval/eval.scm 677 */
																			obj_t BgL_ezd2363zd2_5686;

																			{	/* Eval/eval.scm 677 */
																				obj_t BgL_objz00_5687;

																				{	/* Eval/eval.scm 677 */
																					obj_t BgL_aux2560z00_5688;

																					BgL_aux2560z00_5688 =
																						(((BgL_z62errorz62_bglt)
																							COBJECT(BgL_i1170z00_5472))->
																						BgL_objz00);
																					if (EPAIRP(BgL_aux2560z00_5688))
																						{	/* Eval/eval.scm 677 */
																							BgL_objz00_5687 =
																								BgL_aux2560z00_5688;
																						}
																					else
																						{
																							obj_t BgL_auxz00_8278;

																							BgL_auxz00_8278 =
																								BGl_typezd2errorzd2zz__errorz00
																								(BGl_string2618z00zz__evalz00,
																								BINT(25823L),
																								BGl_string2756z00zz__evalz00,
																								BGl_string2676z00zz__evalz00,
																								BgL_aux2560z00_5688);
																							FAILURE(BgL_auxz00_8278, BFALSE,
																								BFALSE);
																						}
																				}
																				BgL_ezd2363zd2_5686 =
																					CER(BgL_objz00_5687);
																			}
																			if (PAIRP(BgL_ezd2363zd2_5686))
																				{	/* Eval/eval.scm 677 */
																					obj_t BgL_cdrzd2369zd2_5689;

																					BgL_cdrzd2369zd2_5689 =
																						CDR(BgL_ezd2363zd2_5686);
																					if (
																						(CAR(BgL_ezd2363zd2_5686) ==
																							BGl_symbol2630z00zz__evalz00))
																						{	/* Eval/eval.scm 677 */
																							if (PAIRP(BgL_cdrzd2369zd2_5689))
																								{	/* Eval/eval.scm 677 */
																									obj_t BgL_cdrzd2373zd2_5690;

																									BgL_cdrzd2373zd2_5690 =
																										CDR(BgL_cdrzd2369zd2_5689);
																									if (PAIRP
																										(BgL_cdrzd2373zd2_5690))
																										{	/* Eval/eval.scm 677 */
																											if (NULLP(CDR
																													(BgL_cdrzd2373zd2_5690)))
																												{	/* Eval/eval.scm 677 */
																													obj_t
																														BgL_arg1745z00_5691;
																													obj_t
																														BgL_arg1746z00_5692;
																													BgL_arg1745z00_5691 =
																														CAR
																														(BgL_cdrzd2369zd2_5689);
																													BgL_arg1746z00_5692 =
																														CAR
																														(BgL_cdrzd2373zd2_5690);
																													{
																														BgL_z62errorz62_bglt
																															BgL_auxz00_8299;
																														BgL_fnamez00_5514 =
																															BgL_arg1745z00_5691;
																														BgL_locz00_5515 =
																															BgL_arg1746z00_5692;
																														{	/* Eval/eval.scm 679 */
																															BgL_z62exceptionz62_bglt
																																BgL_duplicated1173z00_5516;
																															BgL_z62errorz62_bglt
																																BgL_new1171z00_5517;
																															{	/* Eval/eval.scm 680 */
																																bool_t
																																	BgL_test3361z00_8300;
																																{	/* Eval/eval.scm 680 */
																																	obj_t
																																		BgL_classz00_5518;
																																	BgL_classz00_5518
																																		=
																																		BGl_z62exceptionz62zz__objectz00;
																																	if (BGL_OBJECTP(BgL_ez00_5453))
																																		{	/* Eval/eval.scm 680 */
																																			BgL_objectz00_bglt
																																				BgL_arg2224z00_5520;
																																			long
																																				BgL_arg2225z00_5521;
																																			BgL_arg2224z00_5520
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_ez00_5453);
																																			BgL_arg2225z00_5521
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_classz00_5518);
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Eval/eval.scm 680 */
																																					long
																																						BgL_idxz00_5529;
																																					BgL_idxz00_5529
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg2224z00_5520);
																																					{	/* Eval/eval.scm 680 */
																																						obj_t
																																							BgL_arg2215z00_5530;
																																						{	/* Eval/eval.scm 680 */
																																							long
																																								BgL_arg2216z00_5531;
																																							BgL_arg2216z00_5531
																																								=
																																								(BgL_idxz00_5529
																																								+
																																								BgL_arg2225z00_5521);
																																							BgL_arg2215z00_5530
																																								=
																																								VECTOR_REF
																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																								BgL_arg2216z00_5531);
																																						}
																																						BgL_test3361z00_8300
																																							=
																																							(BgL_arg2215z00_5530
																																							==
																																							BgL_classz00_5518);
																																				}}
																																			else
																																				{	/* Eval/eval.scm 680 */
																																					bool_t
																																						BgL_res2244z00_5536;
																																					{	/* Eval/eval.scm 680 */
																																						obj_t
																																							BgL_oclassz00_5540;
																																						{	/* Eval/eval.scm 680 */
																																							obj_t
																																								BgL_arg2229z00_5542;
																																							long
																																								BgL_arg2230z00_5543;
																																							BgL_arg2229z00_5542
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Eval/eval.scm 680 */
																																								long
																																									BgL_arg2231z00_5544;
																																								BgL_arg2231z00_5544
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg2224z00_5520);
																																								BgL_arg2230z00_5543
																																									=
																																									(BgL_arg2231z00_5544
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_5540
																																								=
																																								VECTOR_REF
																																								(BgL_arg2229z00_5542,
																																								BgL_arg2230z00_5543);
																																						}
																																						{	/* Eval/eval.scm 680 */
																																							bool_t
																																								BgL__ortest_1236z00_5550;
																																							BgL__ortest_1236z00_5550
																																								=
																																								(BgL_classz00_5518
																																								==
																																								BgL_oclassz00_5540);
																																							if (BgL__ortest_1236z00_5550)
																																								{	/* Eval/eval.scm 680 */
																																									BgL_res2244z00_5536
																																										=
																																										BgL__ortest_1236z00_5550;
																																								}
																																							else
																																								{	/* Eval/eval.scm 680 */
																																									long
																																										BgL_odepthz00_5551;
																																									{	/* Eval/eval.scm 680 */
																																										obj_t
																																											BgL_arg2219z00_5552;
																																										BgL_arg2219z00_5552
																																											=
																																											(BgL_oclassz00_5540);
																																										BgL_odepthz00_5551
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg2219z00_5552);
																																									}
																																									if ((BgL_arg2225z00_5521 < BgL_odepthz00_5551))
																																										{	/* Eval/eval.scm 680 */
																																											obj_t
																																												BgL_arg2217z00_5556;
																																											{	/* Eval/eval.scm 680 */
																																												obj_t
																																													BgL_arg2218z00_5557;
																																												BgL_arg2218z00_5557
																																													=
																																													(BgL_oclassz00_5540);
																																												BgL_arg2217z00_5556
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg2218z00_5557,
																																													BgL_arg2225z00_5521);
																																											}
																																											BgL_res2244z00_5536
																																												=
																																												(BgL_arg2217z00_5556
																																												==
																																												BgL_classz00_5518);
																																										}
																																									else
																																										{	/* Eval/eval.scm 680 */
																																											BgL_res2244z00_5536
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test3361z00_8300
																																						=
																																						BgL_res2244z00_5536;
																																				}
																																		}
																																	else
																																		{	/* Eval/eval.scm 680 */
																																			BgL_test3361z00_8300
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test3361z00_8300)
																																	{	/* Eval/eval.scm 680 */
																																		BgL_duplicated1173z00_5516
																																			=
																																			(
																																			(BgL_z62exceptionz62_bglt)
																																			BgL_ez00_5453);
																																	}
																																else
																																	{
																																		obj_t
																																			BgL_auxz00_8325;
																																		BgL_auxz00_8325
																																			=
																																			BGl_typezd2errorzd2zz__errorz00
																																			(BGl_string2618z00zz__evalz00,
																																			BINT
																																			(25899L),
																																			BGl_string2757z00zz__evalz00,
																																			BGl_string2629z00zz__evalz00,
																																			BgL_ez00_5453);
																																		FAILURE
																																			(BgL_auxz00_8325,
																																			BFALSE,
																																			BFALSE);
																																	}
																															}
																															{	/* Eval/eval.scm 680 */
																																BgL_z62errorz62_bglt
																																	BgL_new1177z00_5558;
																																BgL_new1177z00_5558
																																	=
																																	(
																																	(BgL_z62errorz62_bglt)
																																	BOBJECT
																																	(GC_MALLOC
																																		(sizeof
																																			(struct
																																				BgL_z62errorz62_bgl))));
																																{	/* Eval/eval.scm 680 */
																																	long
																																		BgL_arg1750z00_5559;
																																	BgL_arg1750z00_5559
																																		=
																																		BGL_CLASS_NUM
																																		(BGl_z62errorz62zz__objectz00);
																																	BGL_OBJECT_CLASS_NUM_SET
																																		(((BgL_objectz00_bglt) BgL_new1177z00_5558), BgL_arg1750z00_5559);
																																}
																																BgL_new1171z00_5517
																																	=
																																	BgL_new1177z00_5558;
																															}
																															((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1171z00_5517)))->BgL_fnamez00) = ((obj_t) BgL_fnamez00_5514), BUNSPEC);
																															((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1171z00_5517)))->BgL_locationz00) = ((obj_t) BgL_locz00_5515), BUNSPEC);
																															((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1171z00_5517)))->BgL_stackz00) = ((obj_t) (((BgL_z62exceptionz62_bglt) COBJECT(BgL_duplicated1173z00_5516))->BgL_stackz00)), BUNSPEC);
																															{
																																obj_t
																																	BgL_auxz00_8340;
																																{	/* Eval/eval.scm 681 */
																																	BgL_z62errorz62_bglt
																																		BgL_tmp1174z00_5560;
																																	{	/* Eval/eval.scm 681 */
																																		bool_t
																																			BgL_test3366z00_8341;
																																		{	/* Eval/eval.scm 681 */
																																			obj_t
																																				BgL_classz00_5562;
																																			BgL_classz00_5562
																																				=
																																				BGl_z62errorz62zz__objectz00;
																																			{	/* Eval/eval.scm 681 */
																																				bool_t
																																					BgL_test3367z00_8342;
																																				{	/* Eval/eval.scm 681 */
																																					obj_t
																																						BgL_tmpz00_8343;
																																					BgL_tmpz00_8343
																																						=
																																						(
																																						(obj_t)
																																						BgL_duplicated1173z00_5516);
																																					BgL_test3367z00_8342
																																						=
																																						BGL_OBJECTP
																																						(BgL_tmpz00_8343);
																																				}
																																				if (BgL_test3367z00_8342)
																																					{	/* Eval/eval.scm 681 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_5564;
																																						long
																																							BgL_arg2225z00_5565;
																																						{	/* Eval/eval.scm 681 */
																																							obj_t
																																								BgL_tmpz00_8346;
																																							BgL_tmpz00_8346
																																								=
																																								(
																																								(obj_t)
																																								BgL_duplicated1173z00_5516);
																																							BgL_arg2224z00_5564
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_tmpz00_8346);
																																						}
																																						BgL_arg2225z00_5565
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_5562);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 681 */
																																								long
																																									BgL_idxz00_5573;
																																								BgL_idxz00_5573
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_5564);
																																								{	/* Eval/eval.scm 681 */
																																									obj_t
																																										BgL_arg2215z00_5574;
																																									{	/* Eval/eval.scm 681 */
																																										long
																																											BgL_arg2216z00_5575;
																																										BgL_arg2216z00_5575
																																											=
																																											(BgL_idxz00_5573
																																											+
																																											BgL_arg2225z00_5565);
																																										{	/* Eval/eval.scm 681 */
																																											obj_t
																																												BgL_vectorz00_5578;
																																											BgL_vectorz00_5578
																																												=
																																												BGl_za2inheritancesza2z00zz__objectz00;
																																											BgL_arg2215z00_5574
																																												=
																																												VECTOR_REF
																																												(BgL_vectorz00_5578,
																																												BgL_arg2216z00_5575);
																																									}}
																																									BgL_test3366z00_8341
																																										=
																																										(BgL_arg2215z00_5574
																																										==
																																										BgL_classz00_5562);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 681 */
																																								bool_t
																																									BgL_res2244z00_5580;
																																								{	/* Eval/eval.scm 681 */
																																									obj_t
																																										BgL_oclassz00_5584;
																																									{	/* Eval/eval.scm 681 */
																																										obj_t
																																											BgL_arg2229z00_5586;
																																										long
																																											BgL_arg2230z00_5587;
																																										BgL_arg2229z00_5586
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 681 */
																																											long
																																												BgL_arg2231z00_5588;
																																											BgL_arg2231z00_5588
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_5564);
																																											BgL_arg2230z00_5587
																																												=
																																												(BgL_arg2231z00_5588
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_5584
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_5586,
																																											BgL_arg2230z00_5587);
																																									}
																																									{	/* Eval/eval.scm 681 */
																																										bool_t
																																											BgL__ortest_1236z00_5594;
																																										BgL__ortest_1236z00_5594
																																											=
																																											(BgL_classz00_5562
																																											==
																																											BgL_oclassz00_5584);
																																										if (BgL__ortest_1236z00_5594)
																																											{	/* Eval/eval.scm 681 */
																																												BgL_res2244z00_5580
																																													=
																																													BgL__ortest_1236z00_5594;
																																											}
																																										else
																																											{	/* Eval/eval.scm 681 */
																																												long
																																													BgL_odepthz00_5595;
																																												{	/* Eval/eval.scm 681 */
																																													obj_t
																																														BgL_arg2219z00_5596;
																																													BgL_arg2219z00_5596
																																														=
																																														(BgL_oclassz00_5584);
																																													BgL_odepthz00_5595
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_5596);
																																												}
																																												if ((BgL_arg2225z00_5565 < BgL_odepthz00_5595))
																																													{	/* Eval/eval.scm 681 */
																																														obj_t
																																															BgL_arg2217z00_5600;
																																														{	/* Eval/eval.scm 681 */
																																															obj_t
																																																BgL_arg2218z00_5601;
																																															BgL_arg2218z00_5601
																																																=
																																																(BgL_oclassz00_5584);
																																															BgL_arg2217z00_5600
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_5601,
																																																BgL_arg2225z00_5565);
																																														}
																																														BgL_res2244z00_5580
																																															=
																																															(BgL_arg2217z00_5600
																																															==
																																															BgL_classz00_5562);
																																													}
																																												else
																																													{	/* Eval/eval.scm 681 */
																																														BgL_res2244z00_5580
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3366z00_8341
																																									=
																																									BgL_res2244z00_5580;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 681 */
																																						BgL_test3366z00_8341
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																		if (BgL_test3366z00_8341)
																																			{	/* Eval/eval.scm 681 */
																																				BgL_tmp1174z00_5560
																																					=
																																					(
																																					(BgL_z62errorz62_bglt)
																																					BgL_duplicated1173z00_5516);
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_auxz00_8370;
																																				BgL_auxz00_8370
																																					=
																																					BGl_typezd2errorzd2zz__errorz00
																																					(BGl_string2618z00zz__evalz00,
																																					BINT
																																					(25922L),
																																					BGl_string2757z00zz__evalz00,
																																					BGl_string2658z00zz__evalz00,
																																					((obj_t) BgL_duplicated1173z00_5516));
																																				FAILURE
																																					(BgL_auxz00_8370,
																																					BFALSE,
																																					BFALSE);
																																			}
																																	}
																																	BgL_auxz00_8340
																																		=
																																		(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1174z00_5560))->BgL_procz00);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(BgL_new1171z00_5517))->BgL_procz00) = ((obj_t) BgL_auxz00_8340), BUNSPEC);
																															}
																															{
																																obj_t
																																	BgL_auxz00_8377;
																																{	/* Eval/eval.scm 681 */
																																	BgL_z62errorz62_bglt
																																		BgL_tmp1175z00_5602;
																																	{	/* Eval/eval.scm 681 */
																																		bool_t
																																			BgL_test3371z00_8378;
																																		{	/* Eval/eval.scm 681 */
																																			obj_t
																																				BgL_classz00_5604;
																																			BgL_classz00_5604
																																				=
																																				BGl_z62errorz62zz__objectz00;
																																			{	/* Eval/eval.scm 681 */
																																				bool_t
																																					BgL_test3372z00_8379;
																																				{	/* Eval/eval.scm 681 */
																																					obj_t
																																						BgL_tmpz00_8380;
																																					BgL_tmpz00_8380
																																						=
																																						(
																																						(obj_t)
																																						BgL_duplicated1173z00_5516);
																																					BgL_test3372z00_8379
																																						=
																																						BGL_OBJECTP
																																						(BgL_tmpz00_8380);
																																				}
																																				if (BgL_test3372z00_8379)
																																					{	/* Eval/eval.scm 681 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_5606;
																																						long
																																							BgL_arg2225z00_5607;
																																						{	/* Eval/eval.scm 681 */
																																							obj_t
																																								BgL_tmpz00_8383;
																																							BgL_tmpz00_8383
																																								=
																																								(
																																								(obj_t)
																																								BgL_duplicated1173z00_5516);
																																							BgL_arg2224z00_5606
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_tmpz00_8383);
																																						}
																																						BgL_arg2225z00_5607
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_5604);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 681 */
																																								long
																																									BgL_idxz00_5615;
																																								BgL_idxz00_5615
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_5606);
																																								{	/* Eval/eval.scm 681 */
																																									obj_t
																																										BgL_arg2215z00_5616;
																																									{	/* Eval/eval.scm 681 */
																																										long
																																											BgL_arg2216z00_5617;
																																										BgL_arg2216z00_5617
																																											=
																																											(BgL_idxz00_5615
																																											+
																																											BgL_arg2225z00_5607);
																																										{	/* Eval/eval.scm 681 */
																																											obj_t
																																												BgL_vectorz00_5620;
																																											BgL_vectorz00_5620
																																												=
																																												BGl_za2inheritancesza2z00zz__objectz00;
																																											BgL_arg2215z00_5616
																																												=
																																												VECTOR_REF
																																												(BgL_vectorz00_5620,
																																												BgL_arg2216z00_5617);
																																									}}
																																									BgL_test3371z00_8378
																																										=
																																										(BgL_arg2215z00_5616
																																										==
																																										BgL_classz00_5604);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 681 */
																																								bool_t
																																									BgL_res2244z00_5622;
																																								{	/* Eval/eval.scm 681 */
																																									obj_t
																																										BgL_oclassz00_5626;
																																									{	/* Eval/eval.scm 681 */
																																										obj_t
																																											BgL_arg2229z00_5628;
																																										long
																																											BgL_arg2230z00_5629;
																																										BgL_arg2229z00_5628
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 681 */
																																											long
																																												BgL_arg2231z00_5630;
																																											BgL_arg2231z00_5630
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_5606);
																																											BgL_arg2230z00_5629
																																												=
																																												(BgL_arg2231z00_5630
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_5626
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_5628,
																																											BgL_arg2230z00_5629);
																																									}
																																									{	/* Eval/eval.scm 681 */
																																										bool_t
																																											BgL__ortest_1236z00_5636;
																																										BgL__ortest_1236z00_5636
																																											=
																																											(BgL_classz00_5604
																																											==
																																											BgL_oclassz00_5626);
																																										if (BgL__ortest_1236z00_5636)
																																											{	/* Eval/eval.scm 681 */
																																												BgL_res2244z00_5622
																																													=
																																													BgL__ortest_1236z00_5636;
																																											}
																																										else
																																											{	/* Eval/eval.scm 681 */
																																												long
																																													BgL_odepthz00_5637;
																																												{	/* Eval/eval.scm 681 */
																																													obj_t
																																														BgL_arg2219z00_5638;
																																													BgL_arg2219z00_5638
																																														=
																																														(BgL_oclassz00_5626);
																																													BgL_odepthz00_5637
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_5638);
																																												}
																																												if ((BgL_arg2225z00_5607 < BgL_odepthz00_5637))
																																													{	/* Eval/eval.scm 681 */
																																														obj_t
																																															BgL_arg2217z00_5642;
																																														{	/* Eval/eval.scm 681 */
																																															obj_t
																																																BgL_arg2218z00_5643;
																																															BgL_arg2218z00_5643
																																																=
																																																(BgL_oclassz00_5626);
																																															BgL_arg2217z00_5642
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_5643,
																																																BgL_arg2225z00_5607);
																																														}
																																														BgL_res2244z00_5622
																																															=
																																															(BgL_arg2217z00_5642
																																															==
																																															BgL_classz00_5604);
																																													}
																																												else
																																													{	/* Eval/eval.scm 681 */
																																														BgL_res2244z00_5622
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3371z00_8378
																																									=
																																									BgL_res2244z00_5622;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 681 */
																																						BgL_test3371z00_8378
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																		if (BgL_test3371z00_8378)
																																			{	/* Eval/eval.scm 681 */
																																				BgL_tmp1175z00_5602
																																					=
																																					(
																																					(BgL_z62errorz62_bglt)
																																					BgL_duplicated1173z00_5516);
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_auxz00_8407;
																																				BgL_auxz00_8407
																																					=
																																					BGl_typezd2errorzd2zz__errorz00
																																					(BGl_string2618z00zz__evalz00,
																																					BINT
																																					(25922L),
																																					BGl_string2757z00zz__evalz00,
																																					BGl_string2658z00zz__evalz00,
																																					((obj_t) BgL_duplicated1173z00_5516));
																																				FAILURE
																																					(BgL_auxz00_8407,
																																					BFALSE,
																																					BFALSE);
																																			}
																																	}
																																	BgL_auxz00_8377
																																		=
																																		(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1175z00_5602))->BgL_msgz00);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(BgL_new1171z00_5517))->BgL_msgz00) = ((obj_t) BgL_auxz00_8377), BUNSPEC);
																															}
																															{
																																obj_t
																																	BgL_auxz00_8414;
																																{	/* Eval/eval.scm 681 */
																																	BgL_z62errorz62_bglt
																																		BgL_tmp1176z00_5644;
																																	{	/* Eval/eval.scm 681 */
																																		bool_t
																																			BgL_test3376z00_8415;
																																		{	/* Eval/eval.scm 681 */
																																			obj_t
																																				BgL_classz00_5646;
																																			BgL_classz00_5646
																																				=
																																				BGl_z62errorz62zz__objectz00;
																																			{	/* Eval/eval.scm 681 */
																																				bool_t
																																					BgL_test3377z00_8416;
																																				{	/* Eval/eval.scm 681 */
																																					obj_t
																																						BgL_tmpz00_8417;
																																					BgL_tmpz00_8417
																																						=
																																						(
																																						(obj_t)
																																						BgL_duplicated1173z00_5516);
																																					BgL_test3377z00_8416
																																						=
																																						BGL_OBJECTP
																																						(BgL_tmpz00_8417);
																																				}
																																				if (BgL_test3377z00_8416)
																																					{	/* Eval/eval.scm 681 */
																																						BgL_objectz00_bglt
																																							BgL_arg2224z00_5648;
																																						long
																																							BgL_arg2225z00_5649;
																																						{	/* Eval/eval.scm 681 */
																																							obj_t
																																								BgL_tmpz00_8420;
																																							BgL_tmpz00_8420
																																								=
																																								(
																																								(obj_t)
																																								BgL_duplicated1173z00_5516);
																																							BgL_arg2224z00_5648
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_tmpz00_8420);
																																						}
																																						BgL_arg2225z00_5649
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_classz00_5646);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Eval/eval.scm 681 */
																																								long
																																									BgL_idxz00_5657;
																																								BgL_idxz00_5657
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg2224z00_5648);
																																								{	/* Eval/eval.scm 681 */
																																									obj_t
																																										BgL_arg2215z00_5658;
																																									{	/* Eval/eval.scm 681 */
																																										long
																																											BgL_arg2216z00_5659;
																																										BgL_arg2216z00_5659
																																											=
																																											(BgL_idxz00_5657
																																											+
																																											BgL_arg2225z00_5649);
																																										{	/* Eval/eval.scm 681 */
																																											obj_t
																																												BgL_vectorz00_5662;
																																											BgL_vectorz00_5662
																																												=
																																												BGl_za2inheritancesza2z00zz__objectz00;
																																											BgL_arg2215z00_5658
																																												=
																																												VECTOR_REF
																																												(BgL_vectorz00_5662,
																																												BgL_arg2216z00_5659);
																																									}}
																																									BgL_test3376z00_8415
																																										=
																																										(BgL_arg2215z00_5658
																																										==
																																										BgL_classz00_5646);
																																							}}
																																						else
																																							{	/* Eval/eval.scm 681 */
																																								bool_t
																																									BgL_res2244z00_5664;
																																								{	/* Eval/eval.scm 681 */
																																									obj_t
																																										BgL_oclassz00_5668;
																																									{	/* Eval/eval.scm 681 */
																																										obj_t
																																											BgL_arg2229z00_5670;
																																										long
																																											BgL_arg2230z00_5671;
																																										BgL_arg2229z00_5670
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Eval/eval.scm 681 */
																																											long
																																												BgL_arg2231z00_5672;
																																											BgL_arg2231z00_5672
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg2224z00_5648);
																																											BgL_arg2230z00_5671
																																												=
																																												(BgL_arg2231z00_5672
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_5668
																																											=
																																											VECTOR_REF
																																											(BgL_arg2229z00_5670,
																																											BgL_arg2230z00_5671);
																																									}
																																									{	/* Eval/eval.scm 681 */
																																										bool_t
																																											BgL__ortest_1236z00_5678;
																																										BgL__ortest_1236z00_5678
																																											=
																																											(BgL_classz00_5646
																																											==
																																											BgL_oclassz00_5668);
																																										if (BgL__ortest_1236z00_5678)
																																											{	/* Eval/eval.scm 681 */
																																												BgL_res2244z00_5664
																																													=
																																													BgL__ortest_1236z00_5678;
																																											}
																																										else
																																											{	/* Eval/eval.scm 681 */
																																												long
																																													BgL_odepthz00_5679;
																																												{	/* Eval/eval.scm 681 */
																																													obj_t
																																														BgL_arg2219z00_5680;
																																													BgL_arg2219z00_5680
																																														=
																																														(BgL_oclassz00_5668);
																																													BgL_odepthz00_5679
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg2219z00_5680);
																																												}
																																												if ((BgL_arg2225z00_5649 < BgL_odepthz00_5679))
																																													{	/* Eval/eval.scm 681 */
																																														obj_t
																																															BgL_arg2217z00_5684;
																																														{	/* Eval/eval.scm 681 */
																																															obj_t
																																																BgL_arg2218z00_5685;
																																															BgL_arg2218z00_5685
																																																=
																																																(BgL_oclassz00_5668);
																																															BgL_arg2217z00_5684
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg2218z00_5685,
																																																BgL_arg2225z00_5649);
																																														}
																																														BgL_res2244z00_5664
																																															=
																																															(BgL_arg2217z00_5684
																																															==
																																															BgL_classz00_5646);
																																													}
																																												else
																																													{	/* Eval/eval.scm 681 */
																																														BgL_res2244z00_5664
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test3376z00_8415
																																									=
																																									BgL_res2244z00_5664;
																																							}
																																					}
																																				else
																																					{	/* Eval/eval.scm 681 */
																																						BgL_test3376z00_8415
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																		if (BgL_test3376z00_8415)
																																			{	/* Eval/eval.scm 681 */
																																				BgL_tmp1176z00_5644
																																					=
																																					(
																																					(BgL_z62errorz62_bglt)
																																					BgL_duplicated1173z00_5516);
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_auxz00_8444;
																																				BgL_auxz00_8444
																																					=
																																					BGl_typezd2errorzd2zz__errorz00
																																					(BGl_string2618z00zz__evalz00,
																																					BINT
																																					(25922L),
																																					BGl_string2757z00zz__evalz00,
																																					BGl_string2658z00zz__evalz00,
																																					((obj_t) BgL_duplicated1173z00_5516));
																																				FAILURE
																																					(BgL_auxz00_8444,
																																					BFALSE,
																																					BFALSE);
																																			}
																																	}
																																	BgL_auxz00_8414
																																		=
																																		(((BgL_z62errorz62_bglt) COBJECT(BgL_tmp1176z00_5644))->BgL_objz00);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(BgL_new1171z00_5517))->BgL_objz00) = ((obj_t) BgL_auxz00_8414), BUNSPEC);
																															}
																															BgL_auxz00_8299 =
																																BgL_new1171z00_5517;
																														}
																														BgL_nez00_5454 =
																															((obj_t)
																															BgL_auxz00_8299);
																													}
																												}
																											else
																												{	/* Eval/eval.scm 677 */
																													BgL_nez00_5454 =
																														BgL_ez00_5453;
																												}
																										}
																									else
																										{	/* Eval/eval.scm 677 */
																											BgL_nez00_5454 =
																												BgL_ez00_5453;
																										}
																								}
																							else
																								{	/* Eval/eval.scm 677 */
																									BgL_nez00_5454 =
																										BgL_ez00_5453;
																								}
																						}
																					else
																						{	/* Eval/eval.scm 677 */
																							BgL_nez00_5454 = BgL_ez00_5453;
																						}
																				}
																			else
																				{	/* Eval/eval.scm 677 */
																					BgL_nez00_5454 = BgL_ez00_5453;
																				}
																		}
																	}
																else
																	{	/* Eval/eval.scm 676 */
																		BgL_nez00_5454 = BgL_ez00_5453;
																	}
															}
														}
													else
														{	/* Eval/eval.scm 674 */
															BgL_nez00_5454 = BgL_ez00_5453;
														}
												}
												BGl_exceptionzd2notifyzd2zz__objectz00(BgL_nez00_5454);
												return BGl_raisez00zz__errorz00(BgL_nez00_5454);
											}
										}
									}
								else
									{	/* Eval/eval.scm 672 */
										return BgL_val1168z00_5694;
									}
							}
						}
					}
				}
			}
		}

	}



/* <@exit:1732>~0 */
	obj_t BGl_zc3z04exitza31732ze3ze70z60zz__evalz00(obj_t BgL_ez00_3738,
		obj_t BgL_xz00_3737, obj_t BgL_expdzd2evalzd2_3736,
		obj_t BgL_cell1164z00_3735, obj_t BgL_env1169z00_3734)
	{
		{	/* Eval/eval.scm 672 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1181z00_1882;

			if (SET_EXIT(BgL_an_exit1181z00_1882))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1181z00_1882 = (void *) jmpbuf;
					PUSH_ENV_EXIT(BgL_env1169z00_3734, BgL_an_exit1181z00_1882, 1L);
					{	/* Eval/eval.scm 672 */
						obj_t BgL_escape1165z00_1883;

						BgL_escape1165z00_1883 =
							BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1169z00_3734);
						{	/* Eval/eval.scm 672 */
							obj_t BgL_res1184z00_1884;

							{	/* Eval/eval.scm 672 */
								obj_t BgL_ohs1161z00_1885;

								BgL_ohs1161z00_1885 =
									BGL_ENV_ERROR_HANDLER_GET(BgL_env1169z00_3734);
								{	/* Eval/eval.scm 672 */
									obj_t BgL_hds1162z00_1886;

									BgL_hds1162z00_1886 =
										MAKE_STACK_PAIR(BgL_escape1165z00_1883,
										BgL_cell1164z00_3735);
									BGL_ENV_ERROR_HANDLER_SET(BgL_env1169z00_3734,
										BgL_hds1162z00_1886);
									BUNSPEC;
									{	/* Eval/eval.scm 672 */
										obj_t BgL_exitd1178z00_1887;

										BgL_exitd1178z00_1887 =
											BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1169z00_3734);
										{	/* Eval/eval.scm 672 */
											obj_t BgL_tmp1180z00_1888;

											{	/* Eval/eval.scm 672 */
												obj_t BgL_arg1733z00_1890;

												BgL_arg1733z00_1890 =
													BGL_EXITD_PROTECT(BgL_exitd1178z00_1887);
												BgL_tmp1180z00_1888 =
													MAKE_YOUNG_PAIR(BgL_ohs1161z00_1885,
													BgL_arg1733z00_1890);
											}
											{	/* Eval/eval.scm 672 */

												BGL_EXITD_PROTECT_SET(BgL_exitd1178z00_1887,
													BgL_tmp1180z00_1888);
												BUNSPEC;
												{	/* Eval/eval.scm 688 */
													obj_t BgL_tmp1179z00_1889;

													{	/* Eval/eval.scm 688 */
														obj_t BgL_tmpfunz00_8469;

														{	/* Eval/eval.scm 688 */
															bool_t BgL_test2563z00_4109;

															BgL_test2563z00_4109 =
																PROCEDUREP(BgL_expdzd2evalzd2_3736);
															if (BgL_test2563z00_4109)
																{	/* Eval/eval.scm 688 */
																	BgL_tmpfunz00_8469 = BgL_expdzd2evalzd2_3736;
																}
															else
																{
																	obj_t BgL_auxz00_8472;

																	BgL_auxz00_8472 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2618z00zz__evalz00, BINT(26045L),
																		BGl_string2758z00zz__evalz00,
																		BGl_string2620z00zz__evalz00,
																		BgL_expdzd2evalzd2_3736);
																	FAILURE(BgL_auxz00_8472, BFALSE, BFALSE);
																}
														}
														BgL_tmp1179z00_1889 =
															(VA_PROCEDUREP(BgL_tmpfunz00_8469)
															? ((obj_t(*)(obj_t,
																		...))
																PROCEDURE_ENTRY(BgL_tmpfunz00_8469))
															(BgL_expdzd2evalzd2_3736, BgL_xz00_3737,
																BgL_ez00_3738, BEOA) : ((obj_t(*)(obj_t, obj_t,
																		obj_t))
																PROCEDURE_ENTRY(BgL_tmpfunz00_8469))
															(BgL_expdzd2evalzd2_3736, BgL_xz00_3737,
																BgL_ez00_3738));
													}
													{	/* Eval/eval.scm 672 */
														bool_t BgL_test3382z00_8476;

														{	/* Eval/eval.scm 672 */
															obj_t BgL_arg2210z00_3136;

															BgL_arg2210z00_3136 =
																BGL_EXITD_PROTECT(BgL_exitd1178z00_1887);
															BgL_test3382z00_8476 = PAIRP(BgL_arg2210z00_3136);
														}
														if (BgL_test3382z00_8476)
															{	/* Eval/eval.scm 672 */
																obj_t BgL_arg2208z00_3137;

																{	/* Eval/eval.scm 672 */
																	obj_t BgL_arg2209z00_3138;

																	BgL_arg2209z00_3138 =
																		BGL_EXITD_PROTECT(BgL_exitd1178z00_1887);
																	{	/* Eval/eval.scm 672 */
																		obj_t BgL_pairz00_3139;

																		if (PAIRP(BgL_arg2209z00_3138))
																			{	/* Eval/eval.scm 672 */
																				BgL_pairz00_3139 = BgL_arg2209z00_3138;
																			}
																		else
																			{
																				obj_t BgL_auxz00_8482;

																				BgL_auxz00_8482 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2618z00zz__evalz00,
																					BINT(25671L),
																					BGl_string2758z00zz__evalz00,
																					BGl_string2626z00zz__evalz00,
																					BgL_arg2209z00_3138);
																				FAILURE(BgL_auxz00_8482, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2208z00_3137 = CDR(BgL_pairz00_3139);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1178z00_1887,
																	BgL_arg2208z00_3137);
																BUNSPEC;
															}
														else
															{	/* Eval/eval.scm 672 */
																BFALSE;
															}
													}
													BGL_ENV_ERROR_HANDLER_SET(BgL_env1169z00_3734,
														BgL_ohs1161z00_1885);
													BUNSPEC;
													BgL_res1184z00_1884 = BgL_tmp1179z00_1889;
												}
											}
										}
									}
								}
							}
							POP_ENV_EXIT(BgL_env1169z00_3734);
							return BgL_res1184z00_1884;
						}
					}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__evalz00(obj_t BgL_idz00_3730, obj_t BgL_locz00_3729,
		obj_t BgL_fnamez00_3728, obj_t BgL_patz00_1991, obj_t BgL_argz00_1992,
		obj_t BgL_bindingsz00_1993)
	{
		{	/* Eval/eval.scm 703 */
		BGl_loopze70ze7zz__evalz00:
			if (NULLP(BgL_patz00_1991))
				{	/* Eval/eval.scm 708 */
					obj_t BgL_arg1809z00_1996;

					{	/* Eval/eval.scm 708 */
						obj_t BgL_arg1810z00_1997;
						obj_t BgL_arg1811z00_1998;

						{	/* Eval/eval.scm 708 */

							{	/* Eval/eval.scm 708 */

								BgL_arg1810z00_1997 =
									BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
							}
						}
						{	/* Eval/eval.scm 709 */
							obj_t BgL_arg1812z00_2000;

							{	/* Eval/eval.scm 709 */
								obj_t BgL_arg1813z00_2001;

								{	/* Eval/eval.scm 709 */
									obj_t BgL_arg1814z00_2002;
									obj_t BgL_arg1815z00_2003;

									{	/* Eval/eval.scm 709 */
										obj_t BgL_arg1816z00_2004;

										{	/* Eval/eval.scm 709 */
											obj_t BgL_arg1817z00_2005;

											{	/* Eval/eval.scm 709 */
												obj_t BgL_arg1818z00_2006;

												BgL_arg1818z00_2006 =
													MAKE_YOUNG_PAIR(BgL_argz00_1992, BNIL);
												BgL_arg1817z00_2005 =
													MAKE_YOUNG_PAIR(BGl_symbol2759z00zz__evalz00,
													BgL_arg1818z00_2006);
											}
											BgL_arg1816z00_2004 =
												MAKE_YOUNG_PAIR(BgL_arg1817z00_2005, BNIL);
										}
										BgL_arg1814z00_2002 =
											MAKE_YOUNG_PAIR(BGl_symbol2761z00zz__evalz00,
											BgL_arg1816z00_2004);
									}
									{	/* Eval/eval.scm 710 */
										obj_t BgL_arg1819z00_2007;
										obj_t BgL_arg1820z00_2008;

										BgL_arg1819z00_2007 =
											BGl_errze70ze7zz__evalz00(BgL_locz00_3729, BgL_idz00_3730,
											BgL_fnamez00_3728, BGl_string2763z00zz__evalz00,
											BgL_argz00_1992);
										{	/* Eval/eval.scm 711 */
											obj_t BgL_arg1822z00_2009;

											{	/* Eval/eval.scm 711 */
												obj_t BgL_arg1823z00_2010;

												BgL_arg1823z00_2010 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												BgL_arg1822z00_2009 =
													MAKE_YOUNG_PAIR(BGl_symbol2750z00zz__evalz00,
													BgL_arg1823z00_2010);
											}
											BgL_arg1820z00_2008 =
												MAKE_YOUNG_PAIR(BgL_arg1822z00_2009, BNIL);
										}
										BgL_arg1815z00_2003 =
											MAKE_YOUNG_PAIR(BgL_arg1819z00_2007, BgL_arg1820z00_2008);
									}
									BgL_arg1813z00_2001 =
										MAKE_YOUNG_PAIR(BgL_arg1814z00_2002, BgL_arg1815z00_2003);
								}
								BgL_arg1812z00_2000 =
									MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__evalz00,
									BgL_arg1813z00_2001);
							}
							BgL_arg1811z00_1998 = MAKE_YOUNG_PAIR(BgL_arg1812z00_2000, BNIL);
						}
						BgL_arg1809z00_1996 =
							MAKE_YOUNG_PAIR(BgL_arg1810z00_1997, BgL_arg1811z00_1998);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1809z00_1996, BgL_bindingsz00_1993);
				}
			else
				{	/* Eval/eval.scm 707 */
					if (SYMBOLP(BgL_patz00_1991))
						{	/* Eval/eval.scm 714 */
							obj_t BgL_arg1826z00_2012;

							{	/* Eval/eval.scm 714 */
								obj_t BgL_arg1827z00_2013;

								BgL_arg1827z00_2013 = MAKE_YOUNG_PAIR(BgL_argz00_1992, BNIL);
								BgL_arg1826z00_2012 =
									MAKE_YOUNG_PAIR(BgL_patz00_1991, BgL_arg1827z00_2013);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1826z00_2012, BgL_bindingsz00_1993);
						}
					else
						{	/* Eval/eval.scm 713 */
							if (PAIRP(BgL_patz00_1991))
								{	/* Eval/eval.scm 716 */
									obj_t BgL_arg1829z00_2015;
									obj_t BgL_arg1831z00_2016;
									obj_t BgL_arg1832z00_2017;

									BgL_arg1829z00_2015 = CAR(BgL_patz00_1991);
									{	/* Eval/eval.scm 717 */
										obj_t BgL_arg1833z00_2018;

										{	/* Eval/eval.scm 717 */
											obj_t BgL_arg1834z00_2019;
											obj_t BgL_arg1835z00_2020;

											{	/* Eval/eval.scm 717 */
												obj_t BgL_arg1836z00_2021;

												BgL_arg1836z00_2021 =
													MAKE_YOUNG_PAIR(BgL_argz00_1992, BNIL);
												BgL_arg1834z00_2019 =
													MAKE_YOUNG_PAIR(BGl_symbol2766z00zz__evalz00,
													BgL_arg1836z00_2021);
											}
											{	/* Eval/eval.scm 718 */
												obj_t BgL_arg1837z00_2022;
												obj_t BgL_arg1838z00_2023;

												{	/* Eval/eval.scm 718 */
													obj_t BgL_arg1839z00_2024;

													BgL_arg1839z00_2024 =
														MAKE_YOUNG_PAIR(BgL_argz00_1992, BNIL);
													BgL_arg1837z00_2022 =
														MAKE_YOUNG_PAIR(BGl_symbol2768z00zz__evalz00,
														BgL_arg1839z00_2024);
												}
												BgL_arg1838z00_2023 =
													MAKE_YOUNG_PAIR(BGl_errze70ze7zz__evalz00
													(BgL_locz00_3729, BgL_idz00_3730, BgL_fnamez00_3728,
														BGl_string2770z00zz__evalz00, CAR(BgL_patz00_1991)),
													BNIL);
												BgL_arg1835z00_2020 =
													MAKE_YOUNG_PAIR(BgL_arg1837z00_2022,
													BgL_arg1838z00_2023);
											}
											BgL_arg1833z00_2018 =
												MAKE_YOUNG_PAIR(BgL_arg1834z00_2019,
												BgL_arg1835z00_2020);
										}
										BgL_arg1831z00_2016 =
											MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__evalz00,
											BgL_arg1833z00_2018);
									}
									{	/* Eval/eval.scm 720 */
										obj_t BgL_arg1843z00_2027;
										obj_t BgL_arg1844z00_2028;

										BgL_arg1843z00_2027 = CDR(BgL_patz00_1991);
										{	/* Eval/eval.scm 720 */
											obj_t BgL_arg1845z00_2029;

											BgL_arg1845z00_2029 =
												MAKE_YOUNG_PAIR(BgL_argz00_1992, BNIL);
											BgL_arg1844z00_2028 =
												MAKE_YOUNG_PAIR(BGl_symbol2721z00zz__evalz00,
												BgL_arg1845z00_2029);
										}
										BgL_arg1832z00_2017 =
											BGl_loopze70ze7zz__evalz00(BgL_idz00_3730,
											BgL_locz00_3729, BgL_fnamez00_3728, BgL_arg1843z00_2027,
											BgL_arg1844z00_2028, BgL_bindingsz00_1993);
									}
									{
										obj_t BgL_bindingsz00_8531;
										obj_t BgL_argz00_8530;
										obj_t BgL_patz00_8529;

										BgL_patz00_8529 = BgL_arg1829z00_2015;
										BgL_argz00_8530 = BgL_arg1831z00_2016;
										BgL_bindingsz00_8531 = BgL_arg1832z00_2017;
										BgL_bindingsz00_1993 = BgL_bindingsz00_8531;
										BgL_argz00_1992 = BgL_argz00_8530;
										BgL_patz00_1991 = BgL_patz00_8529;
										goto BGl_loopze70ze7zz__evalz00;
									}
								}
							else
								{	/* Eval/eval.scm 715 */
									if (EPAIRP(BgL_patz00_1991))
										{	/* Eval/eval.scm 547 */
											obj_t BgL_arg1524z00_3186;

											{	/* Eval/eval.scm 547 */
												obj_t BgL_objz00_3187;

												if (EPAIRP(BgL_patz00_1991))
													{	/* Eval/eval.scm 547 */
														BgL_objz00_3187 = BgL_patz00_1991;
													}
												else
													{
														obj_t BgL_auxz00_8536;

														BgL_auxz00_8536 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2618z00zz__evalz00, BINT(21769L),
															BGl_string2771z00zz__evalz00,
															BGl_string2676z00zz__evalz00, BgL_patz00_1991);
														FAILURE(BgL_auxz00_8536, BFALSE, BFALSE);
													}
												BgL_arg1524z00_3186 = CER(BgL_objz00_3187);
											}
											return
												BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3186,
												BgL_idz00_3730, BGl_string2772z00zz__evalz00,
												BgL_patz00_1991);
										}
									else
										{	/* Eval/eval.scm 546 */
											return
												BGl_errorz00zz__errorz00(BgL_idz00_3730,
												BGl_string2772z00zz__evalz00, BgL_patz00_1991);
										}
								}
						}
				}
		}

	}



/* err~0 */
	obj_t BGl_errze70ze7zz__evalz00(obj_t BgL_locz00_3733, obj_t BgL_idz00_3732,
		obj_t BgL_fnamez00_3731, obj_t BgL_msgz00_2031, obj_t BgL_objz00_2032)
	{
		{	/* Eval/eval.scm 702 */
			{	/* Eval/eval.scm 700 */
				obj_t BgL_arg1847z00_2034;

				{	/* Eval/eval.scm 700 */
					obj_t BgL_arg1848z00_2035;
					obj_t BgL_arg1849z00_2036;

					{	/* Eval/eval.scm 700 */
						obj_t BgL_arg1850z00_2037;

						BgL_arg1850z00_2037 = MAKE_YOUNG_PAIR(BgL_fnamez00_3731, BNIL);
						BgL_arg1848z00_2035 =
							MAKE_YOUNG_PAIR(BGl_symbol2773z00zz__evalz00,
							BgL_arg1850z00_2037);
					}
					{	/* Eval/eval.scm 701 */
						obj_t BgL_arg1851z00_2038;
						obj_t BgL_arg1852z00_2039;

						{	/* Eval/eval.scm 701 */
							obj_t BgL_arg1853z00_2040;

							{	/* Eval/eval.scm 701 */
								obj_t BgL_arg1854z00_2041;
								obj_t BgL_arg1856z00_2042;

								{	/* Eval/eval.scm 701 */
									obj_t BgL_arg1857z00_2043;

									BgL_arg1857z00_2043 = MAKE_YOUNG_PAIR(BgL_idz00_3732, BNIL);
									BgL_arg1854z00_2041 =
										MAKE_YOUNG_PAIR(BGl_symbol2750z00zz__evalz00,
										BgL_arg1857z00_2043);
								}
								{	/* Eval/eval.scm 701 */
									obj_t BgL_arg1858z00_2044;

									{	/* Eval/eval.scm 701 */
										obj_t BgL_arg1859z00_2045;
										obj_t BgL_arg1860z00_2046;

										{	/* Eval/eval.scm 701 */
											obj_t BgL_arg1862z00_2047;

											BgL_arg1862z00_2047 =
												MAKE_YOUNG_PAIR(BgL_objz00_2032, BNIL);
											BgL_arg1859z00_2045 =
												MAKE_YOUNG_PAIR(BGl_symbol2750z00zz__evalz00,
												BgL_arg1862z00_2047);
										}
										{	/* Eval/eval.scm 701 */
											obj_t BgL_arg1863z00_2048;

											BgL_arg1863z00_2048 =
												MAKE_YOUNG_PAIR(BgL_locz00_3733, BNIL);
											BgL_arg1860z00_2046 =
												MAKE_YOUNG_PAIR(BgL_fnamez00_3731, BgL_arg1863z00_2048);
										}
										BgL_arg1858z00_2044 =
											MAKE_YOUNG_PAIR(BgL_arg1859z00_2045, BgL_arg1860z00_2046);
									}
									BgL_arg1856z00_2042 =
										MAKE_YOUNG_PAIR(BgL_msgz00_2031, BgL_arg1858z00_2044);
								}
								BgL_arg1853z00_2040 =
									MAKE_YOUNG_PAIR(BgL_arg1854z00_2041, BgL_arg1856z00_2042);
							}
							BgL_arg1851z00_2038 =
								MAKE_YOUNG_PAIR(BGl_symbol2775z00zz__evalz00,
								BgL_arg1853z00_2040);
						}
						{	/* Eval/eval.scm 702 */
							obj_t BgL_arg1864z00_2049;

							{	/* Eval/eval.scm 702 */
								obj_t BgL_arg1866z00_2050;

								{	/* Eval/eval.scm 702 */
									obj_t BgL_arg1868z00_2051;
									obj_t BgL_arg1869z00_2052;

									{	/* Eval/eval.scm 702 */
										obj_t BgL_arg1870z00_2053;

										BgL_arg1870z00_2053 = MAKE_YOUNG_PAIR(BgL_idz00_3732, BNIL);
										BgL_arg1868z00_2051 =
											MAKE_YOUNG_PAIR(BGl_symbol2750z00zz__evalz00,
											BgL_arg1870z00_2053);
									}
									{	/* Eval/eval.scm 702 */
										obj_t BgL_arg1872z00_2054;

										{	/* Eval/eval.scm 702 */
											obj_t BgL_arg1873z00_2055;

											{	/* Eval/eval.scm 702 */
												obj_t BgL_arg1874z00_2056;

												BgL_arg1874z00_2056 =
													MAKE_YOUNG_PAIR(BgL_objz00_2032, BNIL);
												BgL_arg1873z00_2055 =
													MAKE_YOUNG_PAIR(BGl_symbol2750z00zz__evalz00,
													BgL_arg1874z00_2056);
											}
											BgL_arg1872z00_2054 =
												MAKE_YOUNG_PAIR(BgL_arg1873z00_2055, BNIL);
										}
										BgL_arg1869z00_2052 =
											MAKE_YOUNG_PAIR(BgL_msgz00_2031, BgL_arg1872z00_2054);
									}
									BgL_arg1866z00_2050 =
										MAKE_YOUNG_PAIR(BgL_arg1868z00_2051, BgL_arg1869z00_2052);
								}
								BgL_arg1864z00_2049 =
									MAKE_YOUNG_PAIR(BGl_symbol2777z00zz__evalz00,
									BgL_arg1866z00_2050);
							}
							BgL_arg1852z00_2039 = MAKE_YOUNG_PAIR(BgL_arg1864z00_2049, BNIL);
						}
						BgL_arg1849z00_2036 =
							MAKE_YOUNG_PAIR(BgL_arg1851z00_2038, BgL_arg1852z00_2039);
					}
					BgL_arg1847z00_2034 =
						MAKE_YOUNG_PAIR(BgL_arg1848z00_2035, BgL_arg1849z00_2036);
				}
				return
					MAKE_YOUNG_PAIR(BGl_symbol2764z00zz__evalz00, BgL_arg1847z00_2034);
			}
		}

	}



/* expand-define-pattern */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2definezd2patternz00zz__evalz00(obj_t
		BgL_xz00_62)
	{
		{	/* Eval/eval.scm 727 */
			if (PAIRP(BgL_xz00_62))
				{	/* Eval/eval.scm 728 */
					obj_t BgL_cdrzd2389zd2_2065;

					BgL_cdrzd2389zd2_2065 = CDR(((obj_t) BgL_xz00_62));
					if (PAIRP(BgL_cdrzd2389zd2_2065))
						{	/* Eval/eval.scm 728 */
							obj_t BgL_cdrzd2394zd2_2067;

							BgL_cdrzd2394zd2_2067 = CDR(BgL_cdrzd2389zd2_2065);
							if (PAIRP(BgL_cdrzd2394zd2_2067))
								{	/* Eval/eval.scm 728 */
									obj_t BgL_cdrzd2399zd2_2069;

									BgL_cdrzd2399zd2_2069 = CDR(BgL_cdrzd2394zd2_2067);
									if (PAIRP(BgL_cdrzd2399zd2_2069))
										{	/* Eval/eval.scm 728 */
											if (NULLP(CDR(BgL_cdrzd2399zd2_2069)))
												{	/* Eval/eval.scm 728 */
													obj_t BgL_arg1882z00_2073;
													obj_t BgL_arg1883z00_2074;
													obj_t BgL_arg1884z00_2075;

													BgL_arg1882z00_2073 = CAR(BgL_cdrzd2389zd2_2065);
													BgL_arg1883z00_2074 = CAR(BgL_cdrzd2394zd2_2067);
													BgL_arg1884z00_2075 = CAR(BgL_cdrzd2399zd2_2069);
													{	/* Eval/eval.scm 730 */
														obj_t BgL_arg1887z00_3210;

														{	/* Eval/eval.scm 730 */
															obj_t BgL_arg1888z00_3211;

															{	/* Eval/eval.scm 730 */
																obj_t BgL_arg1889z00_3212;

																{	/* Eval/eval.scm 730 */
																	obj_t BgL_arg1890z00_3213;

																	BgL_arg1890z00_3213 =
																		MAKE_YOUNG_PAIR(BgL_arg1884z00_2075, BNIL);
																	BgL_arg1889z00_3212 =
																		MAKE_YOUNG_PAIR(BgL_arg1883z00_2074,
																		BgL_arg1890z00_3213);
																}
																BgL_arg1888z00_3211 =
																	MAKE_YOUNG_PAIR(BGl_symbol2734z00zz__evalz00,
																	BgL_arg1889z00_3212);
															}
															{	/* Eval/eval.scm 82 */
																obj_t BgL_envz00_3215;

																BgL_envz00_3215 =
																	BGl_defaultzd2environmentzd2zz__evalz00();
																{	/* Eval/eval.scm 82 */

																	{	/* Eval/eval.scm 176 */
																		obj_t BgL_evaluatez00_3216;

																		if (PROCEDUREP
																			(BGl_defaultzd2evaluatezd2zz__evalz00))
																			{	/* Eval/eval.scm 176 */
																				BgL_evaluatez00_3216 =
																					BGl_defaultzd2evaluatezd2zz__evalz00;
																			}
																		else
																			{	/* Eval/eval.scm 176 */
																				BgL_evaluatez00_3216 =
																					BGl_bytezd2codezd2evaluatezd2envzd2zz__evalz00;
																			}
																		{	/* Eval/eval.scm 179 */
																			obj_t BgL_auxz00_8591;

																			if (PROCEDUREP(BgL_evaluatez00_3216))
																				{	/* Eval/eval.scm 179 */
																					BgL_auxz00_8591 =
																						BgL_evaluatez00_3216;
																				}
																			else
																				{
																					obj_t BgL_auxz00_8594;

																					BgL_auxz00_8594 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2618z00zz__evalz00,
																						BINT(6857L),
																						BGl_string2779z00zz__evalz00,
																						BGl_string2620z00zz__evalz00,
																						BgL_evaluatez00_3216);
																					FAILURE(BgL_auxz00_8594, BFALSE,
																						BFALSE);
																				}
																			BgL_arg1887z00_3210 =
																				BGl_evalzf2expanderzf2zz__evalz00
																				(BgL_arg1888z00_3211, BgL_envz00_3215,
																				BGl_expandz12zd2envzc0zz__expandz00,
																				BgL_auxz00_8591);
																		}
																	}
																}
															}
														}
														BGl_extendzd2rzd2macrozd2envzd2zz__match_normaliza7eza7
															(BgL_arg1882z00_2073, BgL_arg1887z00_3210);
													}
													return BGl_list2780z00zz__evalz00;
												}
											else
												{	/* Eval/eval.scm 733 */
													obj_t BgL_procz00_3219;

													BgL_procz00_3219 = BGl_symbol2783z00zz__evalz00;
													if (EPAIRP(BgL_xz00_62))
														{	/* Eval/eval.scm 547 */
															obj_t BgL_arg1524z00_3221;

															{	/* Eval/eval.scm 547 */
																obj_t BgL_objz00_3222;

																if (EPAIRP(BgL_xz00_62))
																	{	/* Eval/eval.scm 547 */
																		BgL_objz00_3222 = BgL_xz00_62;
																	}
																else
																	{
																		obj_t BgL_auxz00_8604;

																		BgL_auxz00_8604 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2618z00zz__evalz00,
																			BINT(21769L),
																			BGl_string2779z00zz__evalz00,
																			BGl_string2676z00zz__evalz00,
																			BgL_xz00_62);
																		FAILURE(BgL_auxz00_8604, BFALSE, BFALSE);
																	}
																BgL_arg1524z00_3221 = CER(BgL_objz00_3222);
															}
															return
																BGl_everrorz00zz__everrorz00
																(BgL_arg1524z00_3221, BgL_procz00_3219,
																BGl_string2784z00zz__evalz00, BgL_xz00_62);
														}
													else
														{	/* Eval/eval.scm 546 */
															return
																BGl_errorz00zz__errorz00(BgL_procz00_3219,
																BGl_string2784z00zz__evalz00, BgL_xz00_62);
														}
												}
										}
									else
										{	/* Eval/eval.scm 733 */
											obj_t BgL_procz00_3223;

											BgL_procz00_3223 = BGl_symbol2783z00zz__evalz00;
											if (EPAIRP(BgL_xz00_62))
												{	/* Eval/eval.scm 547 */
													obj_t BgL_arg1524z00_3225;

													{	/* Eval/eval.scm 547 */
														obj_t BgL_objz00_3226;

														if (EPAIRP(BgL_xz00_62))
															{	/* Eval/eval.scm 547 */
																BgL_objz00_3226 = BgL_xz00_62;
															}
														else
															{
																obj_t BgL_auxz00_8615;

																BgL_auxz00_8615 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2618z00zz__evalz00, BINT(21769L),
																	BGl_string2779z00zz__evalz00,
																	BGl_string2676z00zz__evalz00, BgL_xz00_62);
																FAILURE(BgL_auxz00_8615, BFALSE, BFALSE);
															}
														BgL_arg1524z00_3225 = CER(BgL_objz00_3226);
													}
													return
														BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3225,
														BgL_procz00_3223, BGl_string2784z00zz__evalz00,
														BgL_xz00_62);
												}
											else
												{	/* Eval/eval.scm 546 */
													return
														BGl_errorz00zz__errorz00(BgL_procz00_3223,
														BGl_string2784z00zz__evalz00, BgL_xz00_62);
												}
										}
								}
							else
								{	/* Eval/eval.scm 733 */
									obj_t BgL_procz00_3227;

									BgL_procz00_3227 = BGl_symbol2783z00zz__evalz00;
									if (EPAIRP(BgL_xz00_62))
										{	/* Eval/eval.scm 547 */
											obj_t BgL_arg1524z00_3229;

											{	/* Eval/eval.scm 547 */
												obj_t BgL_objz00_3230;

												if (EPAIRP(BgL_xz00_62))
													{	/* Eval/eval.scm 547 */
														BgL_objz00_3230 = BgL_xz00_62;
													}
												else
													{
														obj_t BgL_auxz00_8626;

														BgL_auxz00_8626 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2618z00zz__evalz00, BINT(21769L),
															BGl_string2779z00zz__evalz00,
															BGl_string2676z00zz__evalz00, BgL_xz00_62);
														FAILURE(BgL_auxz00_8626, BFALSE, BFALSE);
													}
												BgL_arg1524z00_3229 = CER(BgL_objz00_3230);
											}
											return
												BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3229,
												BgL_procz00_3227, BGl_string2784z00zz__evalz00,
												BgL_xz00_62);
										}
									else
										{	/* Eval/eval.scm 546 */
											return
												BGl_errorz00zz__errorz00(BgL_procz00_3227,
												BGl_string2784z00zz__evalz00, BgL_xz00_62);
										}
								}
						}
					else
						{	/* Eval/eval.scm 733 */
							obj_t BgL_procz00_3231;

							BgL_procz00_3231 = BGl_symbol2783z00zz__evalz00;
							if (EPAIRP(BgL_xz00_62))
								{	/* Eval/eval.scm 547 */
									obj_t BgL_arg1524z00_3233;

									{	/* Eval/eval.scm 547 */
										obj_t BgL_objz00_3234;

										if (EPAIRP(BgL_xz00_62))
											{	/* Eval/eval.scm 547 */
												BgL_objz00_3234 = BgL_xz00_62;
											}
										else
											{
												obj_t BgL_auxz00_8637;

												BgL_auxz00_8637 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2618z00zz__evalz00, BINT(21769L),
													BGl_string2779z00zz__evalz00,
													BGl_string2676z00zz__evalz00, BgL_xz00_62);
												FAILURE(BgL_auxz00_8637, BFALSE, BFALSE);
											}
										BgL_arg1524z00_3233 = CER(BgL_objz00_3234);
									}
									return
										BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3233,
										BgL_procz00_3231, BGl_string2784z00zz__evalz00,
										BgL_xz00_62);
								}
							else
								{	/* Eval/eval.scm 546 */
									return
										BGl_errorz00zz__errorz00(BgL_procz00_3231,
										BGl_string2784z00zz__evalz00, BgL_xz00_62);
								}
						}
				}
			else
				{	/* Eval/eval.scm 733 */
					obj_t BgL_procz00_3235;

					BgL_procz00_3235 = BGl_symbol2783z00zz__evalz00;
					if (EPAIRP(BgL_xz00_62))
						{	/* Eval/eval.scm 547 */
							obj_t BgL_arg1524z00_3237;

							{	/* Eval/eval.scm 547 */
								obj_t BgL_objz00_3238;

								if (EPAIRP(BgL_xz00_62))
									{	/* Eval/eval.scm 547 */
										BgL_objz00_3238 = BgL_xz00_62;
									}
								else
									{
										obj_t BgL_auxz00_8648;

										BgL_auxz00_8648 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2618z00zz__evalz00, BINT(21769L),
											BGl_string2779z00zz__evalz00,
											BGl_string2676z00zz__evalz00, BgL_xz00_62);
										FAILURE(BgL_auxz00_8648, BFALSE, BFALSE);
									}
								BgL_arg1524z00_3237 = CER(BgL_objz00_3238);
							}
							return
								BGl_everrorz00zz__everrorz00(BgL_arg1524z00_3237,
								BgL_procz00_3235, BGl_string2784z00zz__evalz00, BgL_xz00_62);
						}
					else
						{	/* Eval/eval.scm 546 */
							return
								BGl_errorz00zz__errorz00(BgL_procz00_3235,
								BGl_string2784z00zz__evalz00, BgL_xz00_62);
						}
				}
		}

	}



/* &expand-define-pattern */
	obj_t BGl_z62expandzd2definezd2patternz62zz__evalz00(obj_t BgL_envz00_3703,
		obj_t BgL_xz00_3704)
	{
		{	/* Eval/eval.scm 727 */
			return BGl_expandzd2definezd2patternz00zz__evalz00(BgL_xz00_3704);
		}

	}



/* notify-assert-fail */
	BGL_EXPORTED_DEF obj_t BGl_notifyzd2assertzd2failz00zz__evalz00(obj_t
		BgL_varsz00_63, obj_t BgL_failzd2bodyzd2_64, obj_t BgL_locz00_65)
	{
		{	/* Eval/eval.scm 738 */
			{	/* Eval/eval.scm 739 */
				obj_t BgL_portz00_2083;

				{	/* Eval/eval.scm 739 */
					obj_t BgL_tmpz00_8656;

					BgL_tmpz00_8656 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_portz00_2083 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8656);
				}
				BGl_zc3z04exitza31891ze3ze70z60zz__evalz00(BgL_locz00_65,
					BgL_failzd2bodyzd2_64);
				bgl_display_string(BGl_string2785z00zz__evalz00, BgL_portz00_2083);
				bgl_display_char(((unsigned char) 10), BgL_portz00_2083);
				bgl_display_string(BGl_string2786z00zz__evalz00, BgL_portz00_2083);
				bgl_display_char(((unsigned char) 10), BgL_portz00_2083);
				{
					obj_t BgL_l1256z00_2104;

					BgL_l1256z00_2104 = BgL_varsz00_63;
				BgL_zc3z04anonymousza31899ze3z87_2105:
					if (PAIRP(BgL_l1256z00_2104))
						{	/* Eval/eval.scm 755 */
							{	/* Eval/eval.scm 756 */
								obj_t BgL_fz00_2107;

								BgL_fz00_2107 = CAR(BgL_l1256z00_2104);
								bgl_display_string(BGl_string2787z00zz__evalz00,
									BgL_portz00_2083);
								bgl_display_obj(BgL_fz00_2107, BgL_portz00_2083);
								bgl_display_string(BGl_string2788z00zz__evalz00,
									BgL_portz00_2083);
								{	/* Eval/eval.scm 759 */
									obj_t BgL_arg1901z00_2108;

									{	/* Eval/eval.scm 81 */
										obj_t BgL_envz00_2110;

										{	/* Eval/eval.scm 257 */
											obj_t BgL_mz00_3253;

											BgL_mz00_3253 = BGl_evalzd2modulezd2zz__evmodulez00();
											if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_3253))
												{	/* Eval/eval.scm 258 */
													BgL_envz00_2110 = BgL_mz00_3253;
												}
											else
												{	/* Eval/eval.scm 258 */
													BgL_envz00_2110 = BGl_symbol2616z00zz__evalz00;
												}
										}
										{	/* Eval/eval.scm 81 */

											{	/* Eval/eval.scm 170 */
												obj_t BgL_auxz00_8673;

												{	/* Eval/eval.scm 170 */
													obj_t BgL_aux2582z00_4128;

													BgL_aux2582z00_4128 =
														BGl_defaultzd2evaluatezd2zz__evalz00;
													if (PROCEDUREP(BgL_aux2582z00_4128))
														{	/* Eval/eval.scm 170 */
															BgL_auxz00_8673 = BgL_aux2582z00_4128;
														}
													else
														{
															obj_t BgL_auxz00_8676;

															BgL_auxz00_8676 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2618z00zz__evalz00, BINT(6408L),
																BGl_string2789z00zz__evalz00,
																BGl_string2620z00zz__evalz00,
																BgL_aux2582z00_4128);
															FAILURE(BgL_auxz00_8676, BFALSE, BFALSE);
														}
												}
												BgL_arg1901z00_2108 =
													BGl_evalzf2expanderzf2zz__evalz00(BgL_fz00_2107,
													BgL_envz00_2110, BGl_expandzd2envzd2zz__expandz00,
													BgL_auxz00_8673);
											}
										}
									}
									{	/* Eval/eval.scm 759 */
										obj_t BgL_tmpfunz00_8685;

										{	/* Eval/eval.scm 759 */
											obj_t BgL_aux2584z00_4130;

											BgL_aux2584z00_4130 =
												BGl_za2replzd2printerza2zd2zz__evalz00;
											{	/* Eval/eval.scm 759 */
												bool_t BgL_test2585z00_4131;

												BgL_test2585z00_4131 = PROCEDUREP(BgL_aux2584z00_4130);
												if (BgL_test2585z00_4131)
													{	/* Eval/eval.scm 759 */
														BgL_tmpfunz00_8685 = BgL_aux2584z00_4130;
													}
												else
													{
														obj_t BgL_auxz00_8688;

														BgL_auxz00_8688 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2618z00zz__evalz00, BINT(28403L),
															BGl_string2789z00zz__evalz00,
															BGl_string2620z00zz__evalz00,
															BgL_aux2584z00_4130);
														FAILURE(BgL_auxz00_8688, BFALSE, BFALSE);
													}
											}
										}
										(VA_PROCEDUREP(BgL_tmpfunz00_8685) ? ((obj_t(*)(obj_t,
														...))
												PROCEDURE_ENTRY(BgL_tmpfunz00_8685))
											(BGl_za2replzd2printerza2zd2zz__evalz00,
												BgL_arg1901z00_2108, BgL_portz00_2083,
												BEOA) : ((obj_t(*)(obj_t, obj_t,
														obj_t))
												PROCEDURE_ENTRY(BgL_tmpfunz00_8685))
											(BGl_za2replzd2printerza2zd2zz__evalz00,
												BgL_arg1901z00_2108, BgL_portz00_2083));
									}
								}
								bgl_display_char(((unsigned char) 10), BgL_portz00_2083);
							}
							{
								obj_t BgL_l1256z00_8693;

								BgL_l1256z00_8693 = CDR(BgL_l1256z00_2104);
								BgL_l1256z00_2104 = BgL_l1256z00_8693;
								goto BgL_zc3z04anonymousza31899ze3z87_2105;
							}
						}
					else
						{	/* Eval/eval.scm 755 */
							if (NULLP(BgL_l1256z00_2104))
								{	/* Eval/eval.scm 755 */
									BTRUE;
								}
							else
								{	/* Eval/eval.scm 755 */
									BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
										(BGl_string2790z00zz__evalz00, BGl_string2746z00zz__evalz00,
										BgL_l1256z00_2104, BGl_string2618z00zz__evalz00,
										BINT(28302L));
								}
						}
				}
				bgl_display_string(BGl_string2785z00zz__evalz00, BgL_portz00_2083);
				bgl_display_char(((unsigned char) 10), BgL_portz00_2083);
				{	/* Eval/eval.scm 763 */
					obj_t BgL_oldzd2prompterzd2_2115;

					{	/* Eval/eval.scm 763 */
						obj_t BgL_res2242z00_3260;

						{	/* Eval/eval.scm 283 */
							obj_t BgL_aux2586z00_4132;

							BgL_aux2586z00_4132 = BGl_za2promptza2z00zz__evalz00;
							if (PROCEDUREP(BgL_aux2586z00_4132))
								{	/* Eval/eval.scm 283 */
									BgL_res2242z00_3260 = BgL_aux2586z00_4132;
								}
							else
								{
									obj_t BgL_auxz00_8703;

									BgL_auxz00_8703 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string2618z00zz__evalz00, BINT(11393L),
										BGl_string2791z00zz__evalz00, BGl_string2620z00zz__evalz00,
										BgL_aux2586z00_4132);
									FAILURE(BgL_auxz00_8703, BFALSE, BFALSE);
								}
						}
						BgL_oldzd2prompterzd2_2115 = BgL_res2242z00_3260;
					}
					if (PROCEDURE_CORRECT_ARITYP(BGl_proc2792z00zz__evalz00, (int) (1L)))
						{	/* Eval/eval.scm 274 */
							BGl_za2promptza2z00zz__evalz00 = BGl_proc2792z00zz__evalz00;
						}
					else
						{	/* Eval/eval.scm 274 */
							BGl_errorz00zz__errorz00(BGl_symbol2640z00zz__evalz00,
								BGl_string2642z00zz__evalz00, BGl_proc2792z00zz__evalz00);
						}
					BGl_replz00zz__evalz00();
					{	/* Eval/eval.scm 766 */
						obj_t BgL_procz00_3265;

						if (PROCEDUREP(BgL_oldzd2prompterzd2_2115))
							{	/* Eval/eval.scm 766 */
								BgL_procz00_3265 = BgL_oldzd2prompterzd2_2115;
							}
						else
							{
								obj_t BgL_auxz00_8714;

								BgL_auxz00_8714 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
									BINT(28629L), BGl_string2791z00zz__evalz00,
									BGl_string2620z00zz__evalz00, BgL_oldzd2prompterzd2_2115);
								FAILURE(BgL_auxz00_8714, BFALSE, BFALSE);
							}
						if (PROCEDURE_CORRECT_ARITYP(BgL_procz00_3265, (int) (1L)))
							{	/* Eval/eval.scm 274 */
								return (BGl_za2promptza2z00zz__evalz00 =
									BgL_procz00_3265, BUNSPEC);
							}
						else
							{	/* Eval/eval.scm 274 */
								BGL_TAIL return
									BGl_errorz00zz__errorz00(BGl_symbol2640z00zz__evalz00,
									BGl_string2642z00zz__evalz00, BgL_procz00_3265);
							}
					}
				}
			}
		}

	}



/* <@exit:1891>~0 */
	obj_t BGl_zc3z04exitza31891ze3ze70z60zz__evalz00(obj_t BgL_locz00_3727,
		obj_t BgL_failzd2bodyzd2_3726)
	{
		{	/* Eval/eval.scm 740 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1186z00_2085;

			if (SET_EXIT(BgL_an_exit1186z00_2085))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1186z00_2085 = (void *) jmpbuf;
					{	/* Eval/eval.scm 740 */
						obj_t BgL_env1190z00_2086;

						BgL_env1190z00_2086 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1190z00_2086, BgL_an_exit1186z00_2085, 1L);
						{	/* Eval/eval.scm 740 */
							obj_t BgL_an_exitd1187z00_2087;

							BgL_an_exitd1187z00_2087 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1190z00_2086);
							{	/* Eval/eval.scm 740 */
								obj_t BgL_res1189z00_2090;

								{	/* Eval/eval.scm 743 */
									obj_t BgL_zc3z04anonymousza31895ze3z87_3706;
									obj_t BgL_zc3z04anonymousza31894ze3z87_3707;

									BgL_zc3z04anonymousza31895ze3z87_3706 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31895ze3ze5zz__evalz00, (int) (0L),
										(int) (2L));
									BgL_zc3z04anonymousza31894ze3z87_3707 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31894ze3ze5zz__evalz00, (int) (1L),
										(int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31895ze3z87_3706,
										(int) (0L), BgL_failzd2bodyzd2_3726);
									PROCEDURE_SET(BgL_zc3z04anonymousza31895ze3z87_3706,
										(int) (1L), BgL_locz00_3727);
									PROCEDURE_SET(BgL_zc3z04anonymousza31894ze3z87_3707,
										(int) (0L), BgL_an_exitd1187z00_2087);
									BgL_res1189z00_2090 =
										BGl_withzd2exceptionzd2handlerz00zz__errorz00
										(BgL_zc3z04anonymousza31894ze3z87_3707,
										BgL_zc3z04anonymousza31895ze3z87_3706);
								}
								POP_ENV_EXIT(BgL_env1190z00_2086);
								return BgL_res1189z00_2090;
							}
						}
					}
				}
		}

	}



/* &notify-assert-fail */
	obj_t BGl_z62notifyzd2assertzd2failz62zz__evalz00(obj_t BgL_envz00_3708,
		obj_t BgL_varsz00_3709, obj_t BgL_failzd2bodyzd2_3710,
		obj_t BgL_locz00_3711)
	{
		{	/* Eval/eval.scm 738 */
			return
				BGl_notifyzd2assertzd2failz00zz__evalz00(BgL_varsz00_3709,
				BgL_failzd2bodyzd2_3710, BgL_locz00_3711);
		}

	}



/* &<@anonymous:1905> */
	obj_t BGl_z62zc3z04anonymousza31905ze3ze5zz__evalz00(obj_t BgL_envz00_3712,
		obj_t BgL_numz00_3713)
	{
		{	/* Eval/eval.scm 764 */
			{	/* Eval/eval.scm 764 */
				obj_t BgL_arg1906z00_5696;

				{	/* Eval/eval.scm 764 */
					obj_t BgL_tmpz00_8742;

					BgL_tmpz00_8742 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1906z00_5696 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8742);
				}
				return
					bgl_display_string(BGl_string2793z00zz__evalz00, BgL_arg1906z00_5696);
			}
		}

	}



/* &<@anonymous:1895> */
	obj_t BGl_z62zc3z04anonymousza31895ze3ze5zz__evalz00(obj_t BgL_envz00_3714)
	{
		{	/* Eval/eval.scm 745 */
			{	/* Eval/eval.scm 746 */
				obj_t BgL_failzd2bodyzd2_3715;
				obj_t BgL_locz00_3716;

				BgL_failzd2bodyzd2_3715 = PROCEDURE_REF(BgL_envz00_3714, (int) (0L));
				BgL_locz00_3716 = PROCEDURE_REF(BgL_envz00_3714, (int) (1L));
				if (PAIRP(BgL_locz00_3716))
					{	/* Eval/eval.scm 746 */
						return
							BGl_errorzf2locationzf2zz__errorz00(BGl_string2794z00zz__evalz00,
							BGl_string2795z00zz__evalz00, BgL_failzd2bodyzd2_3715,
							CAR(BgL_locz00_3716), CDR(BgL_locz00_3716));
					}
				else
					{	/* Eval/eval.scm 746 */
						return
							BGl_errorz00zz__errorz00(BGl_symbol2796z00zz__evalz00,
							BGl_string2795z00zz__evalz00, BgL_failzd2bodyzd2_3715);
					}
			}
		}

	}



/* &<@anonymous:1894> */
	obj_t BGl_z62zc3z04anonymousza31894ze3ze5zz__evalz00(obj_t BgL_envz00_3717,
		obj_t BgL_ez00_3719)
	{
		{	/* Eval/eval.scm 742 */
			{	/* Eval/eval.scm 743 */
				obj_t BgL_an_exitd1187z00_3718;

				BgL_an_exitd1187z00_3718 = PROCEDURE_REF(BgL_envz00_3717, (int) (0L));
				BGl_errorzd2notifyzd2zz__errorz00(BgL_ez00_3719);
				return
					unwind_stack_until(BgL_an_exitd1187z00_3718, BFALSE, BUNSPEC, BFALSE,
					BFALSE);
			}
		}

	}



/* identifier-syntax */
	BGL_EXPORTED_DEF obj_t BGl_identifierzd2syntaxzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 788 */
			return BGl_za2identifierzd2syntaxza2zd2zz__evalz00;
		}

	}



/* &identifier-syntax */
	obj_t BGl_z62identifierzd2syntaxzb0zz__evalz00(obj_t BgL_envz00_3720)
	{
		{	/* Eval/eval.scm 788 */
			return BGl_identifierzd2syntaxzd2zz__evalz00();
		}

	}



/* identifier-syntax-set! */
	BGL_EXPORTED_DEF obj_t BGl_identifierzd2syntaxzd2setz12z12zz__evalz00(obj_t
		BgL_vz00_66)
	{
		{	/* Eval/eval.scm 789 */
			return (BGl_za2identifierzd2syntaxza2zd2zz__evalz00 =
				BgL_vz00_66, BUNSPEC);
		}

	}



/* &identifier-syntax-set! */
	obj_t BGl_z62identifierzd2syntaxzd2setz12z70zz__evalz00(obj_t BgL_envz00_3721,
		obj_t BgL_vz00_3722)
	{
		{	/* Eval/eval.scm 789 */
			{	/* Eval/eval.scm 789 */
				obj_t BgL_auxz00_8761;

				if (SYMBOLP(BgL_vz00_3722))
					{	/* Eval/eval.scm 789 */
						BgL_auxz00_8761 = BgL_vz00_3722;
					}
				else
					{
						obj_t BgL_auxz00_8764;

						BgL_auxz00_8764 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(29891L), BGl_string2797z00zz__evalz00,
							BGl_string2798z00zz__evalz00, BgL_vz00_3722);
						FAILURE(BgL_auxz00_8764, BFALSE, BFALSE);
					}
				return BGl_identifierzd2syntaxzd2setz12z12zz__evalz00(BgL_auxz00_8761);
			}
		}

	}



/* transcript-on */
	BGL_EXPORTED_DEF obj_t BGl_transcriptzd2onzd2zz__evalz00(obj_t BgL_filez00_67)
	{
		{	/* Eval/eval.scm 799 */
			{	/* Eval/eval.scm 800 */
				bool_t BgL_test3417z00_8769;

				{	/* Eval/eval.scm 800 */
					obj_t BgL_arg1914z00_2127;

					{	/* Eval/eval.scm 800 */
						obj_t BgL_tmpz00_8770;

						BgL_tmpz00_8770 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1914z00_2127 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8770);
					}
					BgL_test3417z00_8769 =
						(BGl_za2transcriptza2z00zz__evalz00 == BgL_arg1914z00_2127);
				}
				if (BgL_test3417z00_8769)
					{	/* Eval/eval.scm 800 */
						{	/* Ieee/port.scm 484 */

							BGl_za2transcriptza2z00zz__evalz00 =
								BGl_appendzd2outputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_filez00_67, BTRUE);
						}
						{	/* Eval/eval.scm 804 */
							obj_t BgL_port1259z00_2125;

							BgL_port1259z00_2125 = BGl_za2transcriptza2z00zz__evalz00;
							{	/* Eval/eval.scm 804 */
								obj_t BgL_portz00_3268;

								if (OUTPUT_PORTP(BgL_port1259z00_2125))
									{	/* Eval/eval.scm 804 */
										BgL_portz00_3268 = BgL_port1259z00_2125;
									}
								else
									{
										obj_t BgL_auxz00_8777;

										BgL_auxz00_8777 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2618z00zz__evalz00, BINT(30645L),
											BGl_string2799z00zz__evalz00,
											BGl_string2654z00zz__evalz00, BgL_port1259z00_2125);
										FAILURE(BgL_auxz00_8777, BFALSE, BFALSE);
									}
								bgl_display_string(BGl_string2800z00zz__evalz00,
									BgL_portz00_3268);
							}
							{	/* Eval/eval.scm 804 */
								char *BgL_arg1913z00_2126;

								BgL_arg1913z00_2126 = BGl_datez00zz__osz00();
								{	/* Eval/eval.scm 804 */
									obj_t BgL_auxz00_8783;

									if (OUTPUT_PORTP(BgL_port1259z00_2125))
										{	/* Eval/eval.scm 804 */
											BgL_auxz00_8783 = BgL_port1259z00_2125;
										}
									else
										{
											obj_t BgL_auxz00_8787;

											BgL_auxz00_8787 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2618z00zz__evalz00, BINT(30645L),
												BGl_string2799z00zz__evalz00,
												BGl_string2654z00zz__evalz00, BgL_port1259z00_2125);
											FAILURE(BgL_auxz00_8787, BFALSE, BFALSE);
										}
									bgl_display_obj(string_to_bstring(BgL_arg1913z00_2126),
										BgL_auxz00_8783);
								}
							}
							{	/* Eval/eval.scm 804 */
								obj_t BgL_portz00_3269;

								if (OUTPUT_PORTP(BgL_port1259z00_2125))
									{	/* Eval/eval.scm 804 */
										BgL_portz00_3269 = BgL_port1259z00_2125;
									}
								else
									{
										obj_t BgL_auxz00_8794;

										BgL_auxz00_8794 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2618z00zz__evalz00, BINT(30645L),
											BGl_string2799z00zz__evalz00,
											BGl_string2654z00zz__evalz00, BgL_port1259z00_2125);
										FAILURE(BgL_auxz00_8794, BFALSE, BFALSE);
									}
								bgl_display_char(((unsigned char) 10), BgL_portz00_3269);
						}}
						return BUNSPEC;
					}
				else
					{	/* Eval/eval.scm 800 */
						return
							BGl_errorz00zz__errorz00(BGl_symbol2801z00zz__evalz00,
							BGl_string2802z00zz__evalz00, BGl_za2transcriptza2z00zz__evalz00);
					}
			}
		}

	}



/* &transcript-on */
	obj_t BGl_z62transcriptzd2onzb0zz__evalz00(obj_t BgL_envz00_3723,
		obj_t BgL_filez00_3724)
	{
		{	/* Eval/eval.scm 799 */
			{	/* Eval/eval.scm 800 */
				obj_t BgL_auxz00_8800;

				if (STRINGP(BgL_filez00_3724))
					{	/* Eval/eval.scm 800 */
						BgL_auxz00_8800 = BgL_filez00_3724;
					}
				else
					{
						obj_t BgL_auxz00_8803;

						BgL_auxz00_8803 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2618z00zz__evalz00,
							BINT(30452L), BGl_string2803z00zz__evalz00,
							BGl_string2633z00zz__evalz00, BgL_filez00_3724);
						FAILURE(BgL_auxz00_8803, BFALSE, BFALSE);
					}
				return BGl_transcriptzd2onzd2zz__evalz00(BgL_auxz00_8800);
			}
		}

	}



/* transcript-off */
	BGL_EXPORTED_DEF obj_t BGl_transcriptzd2offzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 810 */
			{	/* Eval/eval.scm 811 */
				bool_t BgL_test3422z00_8808;

				{	/* Eval/eval.scm 811 */
					obj_t BgL_arg1918z00_2130;

					{	/* Eval/eval.scm 811 */
						obj_t BgL_tmpz00_8809;

						BgL_tmpz00_8809 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1918z00_2130 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8809);
					}
					BgL_test3422z00_8808 =
						(BGl_za2transcriptza2z00zz__evalz00 == BgL_arg1918z00_2130);
				}
				if (BgL_test3422z00_8808)
					{	/* Eval/eval.scm 811 */
						BGl_errorz00zz__errorz00(BGl_symbol2804z00zz__evalz00,
							BGl_string2806z00zz__evalz00, BGl_za2transcriptza2z00zz__evalz00);
					}
				else
					{	/* Eval/eval.scm 811 */
						{	/* Eval/eval.scm 816 */
							obj_t BgL_portz00_3271;

							{	/* Eval/eval.scm 816 */
								obj_t BgL_aux2600z00_4146;

								BgL_aux2600z00_4146 = BGl_za2transcriptza2z00zz__evalz00;
								if (OUTPUT_PORTP(BgL_aux2600z00_4146))
									{	/* Eval/eval.scm 816 */
										BgL_portz00_3271 = BgL_aux2600z00_4146;
									}
								else
									{
										obj_t BgL_auxz00_8816;

										BgL_auxz00_8816 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2618z00zz__evalz00, BINT(31144L),
											BGl_string2805z00zz__evalz00,
											BGl_string2654z00zz__evalz00, BgL_aux2600z00_4146);
										FAILURE(BgL_auxz00_8816, BFALSE, BFALSE);
									}
							}
							bgl_close_output_port(BgL_portz00_3271);
						}
						{	/* Eval/eval.scm 817 */
							obj_t BgL_tmpz00_8821;

							BgL_tmpz00_8821 = BGL_CURRENT_DYNAMIC_ENV();
							BGl_za2transcriptza2z00zz__evalz00 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8821);
						}
					}
			}
			return BUNSPEC;
		}

	}



/* &transcript-off */
	obj_t BGl_z62transcriptzd2offzb0zz__evalz00(obj_t BgL_envz00_3725)
	{
		{	/* Eval/eval.scm 810 */
			return BGl_transcriptzd2offzd2zz__evalz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__pp_circlez00(122355566L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__intextz00(6305717L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(323064722L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__macroz00(261397491L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__install_expandersz00(305094867L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__evmeaningz00(72314635L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__evaluatez00(398574045L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__evprimopz00(245680565L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
			return
				BGl_modulezd2initializa7ationz75zz__match_normaliza7eza7(515155867L,
				BSTRING_TO_STRING(BGl_string2807z00zz__evalz00));
		}

	}



/* eval-init */
	obj_t BGl_evalzd2initzd2zz__evalz00(void)
	{
		{	/* Eval/eval.scm 18 */
			BGl_definezd2primopzd2refzf2locz12ze0zz__evenvz00
				(BGl_symbol2808z00zz__evalz00,
				__EVMEANING_ADDRESS((BGl_za2czd2debugzd2replzd2valueza2zd2zz__evalz00)),
				BGl_string2810z00zz__evalz00, BINT(16663L));
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
