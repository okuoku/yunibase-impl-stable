/*===========================================================================*/
/*   (Eval/expdcase.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdcase.scm -indent -o objs/obj_u/Eval/expdcase.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_CASE_TYPE_DEFINITIONS
#define BGL___EXPANDER_CASE_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_CASE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__expander_casez00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_casez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62expandzd2evalzd2casez62zz__expander_casez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2casezd2zz__expander_casez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_casez00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_casez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_casez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_casez00(void);
	static obj_t BGl_objectzd2initzd2zz__expander_casez00(void);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_casez00(void);
	static obj_t BGl_loopze70ze7zz__expander_casez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2evalzd2casez00zz__expander_casez00(obj_t,
		obj_t);
	static obj_t BGl_symbol1655z00zz__expander_casez00 = BUNSPEC;
	static obj_t BGl_symbol1657z00zz__expander_casez00 = BUNSPEC;
	static obj_t BGl_symbol1660z00zz__expander_casez00 = BUNSPEC;
	static obj_t BGl_symbol1662z00zz__expander_casez00 = BUNSPEC;
	static obj_t BGl_symbol1664z00zz__expander_casez00 = BUNSPEC;
	static obj_t BGl_symbol1666z00zz__expander_casez00 = BUNSPEC;
	static obj_t BGl_symbol1668z00zz__expander_casez00 = BUNSPEC;
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2evalzd2casezd2envzd2zz__expander_casez00,
		BgL_bgl_za762expandza7d2eval1671z00,
		BGl_z62expandzd2evalzd2casez62zz__expander_casez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1653z00zz__expander_casez00,
		BgL_bgl_string1653za700za7za7_1672za7, "case", 4);
	      DEFINE_STRING(BGl_string1654z00zz__expander_casez00,
		BgL_bgl_string1654za700za7za7_1673za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1656z00zz__expander_casez00,
		BgL_bgl_string1656za700za7za7_1674za7, "case-value", 10);
	      DEFINE_STRING(BGl_string1658z00zz__expander_casez00,
		BgL_bgl_string1658za700za7za7_1675za7, "let", 3);
	      DEFINE_STRING(BGl_string1659z00zz__expander_casez00,
		BgL_bgl_string1659za700za7za7_1676za7, "Illegal `case' form", 19);
	      DEFINE_STRING(BGl_string1661z00zz__expander_casez00,
		BgL_bgl_string1661za700za7za7_1677za7, "quote", 5);
	      DEFINE_STRING(BGl_string1663z00zz__expander_casez00,
		BgL_bgl_string1663za700za7za7_1678za7, "eqv?", 4);
	      DEFINE_STRING(BGl_string1665z00zz__expander_casez00,
		BgL_bgl_string1665za700za7za7_1679za7, "if", 2);
	      DEFINE_STRING(BGl_string1667z00zz__expander_casez00,
		BgL_bgl_string1667za700za7za7_1680za7, "memv", 4);
	      DEFINE_STRING(BGl_string1669z00zz__expander_casez00,
		BgL_bgl_string1669za700za7za7_1681za7, "else", 4);
	      DEFINE_STRING(BGl_string1670z00zz__expander_casez00,
		BgL_bgl_string1670za700za7za7_1682za7, "__expander_case", 15);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1655z00zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1657z00zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1660z00zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1662z00zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1664z00zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1666z00zz__expander_casez00));
		     ADD_ROOT((void *) (&BGl_symbol1668z00zz__expander_casez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_casez00(long
		BgL_checksumz00_1837, char *BgL_fromz00_1838)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_casez00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_casez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_casez00();
					BGl_cnstzd2initzd2zz__expander_casez00();
					BGl_importedzd2moduleszd2initz00zz__expander_casez00();
					return BGl_methodzd2initzd2zz__expander_casez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_casez00(void)
	{
		{	/* Eval/expdcase.scm 14 */
			BGl_symbol1655z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1656z00zz__expander_casez00);
			BGl_symbol1657z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1658z00zz__expander_casez00);
			BGl_symbol1660z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1661z00zz__expander_casez00);
			BGl_symbol1662z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1663z00zz__expander_casez00);
			BGl_symbol1664z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1665z00zz__expander_casez00);
			BGl_symbol1666z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1667z00zz__expander_casez00);
			return (BGl_symbol1668z00zz__expander_casez00 =
				bstring_to_symbol(BGl_string1669z00zz__expander_casez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_casez00(void)
	{
		{	/* Eval/expdcase.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* expand-eval-case */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2evalzd2casez00zz__expander_casez00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expdcase.scm 54 */
			if (PAIRP(BgL_xz00_3))
				{	/* Eval/expdcase.scm 55 */
					obj_t BgL_cdrzd2109zd2_1128;

					BgL_cdrzd2109zd2_1128 = CDR(((obj_t) BgL_xz00_3));
					if (PAIRP(BgL_cdrzd2109zd2_1128))
						{	/* Eval/expdcase.scm 55 */
							return
								BGl_genericzd2casezd2zz__expander_casez00(BgL_xz00_3,
								CAR(BgL_cdrzd2109zd2_1128),
								CDR(BgL_cdrzd2109zd2_1128), BgL_ez00_4);
						}
					else
						{	/* Eval/expdcase.scm 55 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string1653z00zz__expander_casez00,
								BGl_string1654z00zz__expander_casez00, BgL_xz00_3);
						}
				}
			else
				{	/* Eval/expdcase.scm 55 */
					return
						BGl_expandzd2errorzd2zz__expandz00
						(BGl_string1653z00zz__expander_casez00,
						BGl_string1654z00zz__expander_casez00, BgL_xz00_3);
				}
		}

	}



/* &expand-eval-case */
	obj_t BGl_z62expandzd2evalzd2casez62zz__expander_casez00(obj_t
		BgL_envz00_1826, obj_t BgL_xz00_1827, obj_t BgL_ez00_1828)
	{
		{	/* Eval/expdcase.scm 54 */
			return
				BGl_expandzd2evalzd2casez00zz__expander_casez00(BgL_xz00_1827,
				BgL_ez00_1828);
		}

	}



/* generic-case */
	obj_t BGl_genericzd2casezd2zz__expander_casez00(obj_t BgL_xz00_5,
		obj_t BgL_valuez00_6, obj_t BgL_clausesz00_7, obj_t BgL_ez00_8)
	{
		{	/* Eval/expdcase.scm 65 */
			{	/* Eval/expdcase.scm 66 */
				obj_t BgL_arg1182z00_1132;

				{	/* Eval/expdcase.scm 66 */
					obj_t BgL_arg1183z00_1133;

					{	/* Eval/expdcase.scm 66 */
						obj_t BgL_arg1187z00_1134;
						obj_t BgL_arg1188z00_1135;

						{	/* Eval/expdcase.scm 66 */
							obj_t BgL_arg1189z00_1136;

							{	/* Eval/expdcase.scm 66 */
								obj_t BgL_arg1190z00_1137;

								BgL_arg1190z00_1137 = MAKE_YOUNG_PAIR(BgL_valuez00_6, BNIL);
								BgL_arg1189z00_1136 =
									MAKE_YOUNG_PAIR(BGl_symbol1655z00zz__expander_casez00,
									BgL_arg1190z00_1137);
							}
							BgL_arg1187z00_1134 = MAKE_YOUNG_PAIR(BgL_arg1189z00_1136, BNIL);
						}
						BgL_arg1188z00_1135 =
							MAKE_YOUNG_PAIR(BGl_loopze70ze7zz__expander_casez00(BgL_xz00_5,
								BgL_clausesz00_7), BNIL);
						BgL_arg1183z00_1133 =
							MAKE_YOUNG_PAIR(BgL_arg1187z00_1134, BgL_arg1188z00_1135);
					}
					BgL_arg1182z00_1132 =
						MAKE_YOUNG_PAIR(BGl_symbol1657z00zz__expander_casez00,
						BgL_arg1183z00_1133);
				}
				return BGL_PROCEDURE_CALL2(BgL_ez00_8, BgL_arg1182z00_1132, BgL_ez00_8);
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__expander_casez00(obj_t BgL_xz00_1829,
		obj_t BgL_clausesz00_1140)
	{
		{	/* Eval/expdcase.scm 67 */
			if (NULLP(BgL_clausesz00_1140))
				{	/* Eval/expdcase.scm 68 */
					return BUNSPEC;
				}
			else
				{
					obj_t BgL_bodyz00_1144;
					obj_t BgL_datumsz00_1146;
					obj_t BgL_bodyz00_1147;
					obj_t BgL_datumsz00_1149;
					obj_t BgL_bodyz00_1150;

					{	/* Eval/expdcase.scm 70 */
						obj_t BgL_ezd2121zd2_1153;

						BgL_ezd2121zd2_1153 = CAR(((obj_t) BgL_clausesz00_1140));
						if (NULLP(BgL_ezd2121zd2_1153))
							{	/* Eval/expdcase.scm 70 */
								return BUNSPEC;
							}
						else
							{	/* Eval/expdcase.scm 70 */
								if (PAIRP(BgL_ezd2121zd2_1153))
									{	/* Eval/expdcase.scm 70 */
										if (
											(CAR(BgL_ezd2121zd2_1153) ==
												BGl_symbol1668z00zz__expander_casez00))
											{	/* Eval/expdcase.scm 70 */
												BgL_bodyz00_1144 = CDR(BgL_ezd2121zd2_1153);
												{	/* Eval/expdcase.scm 74 */
													bool_t BgL_test1690z00_1889;

													if (NULLP(CDR(((obj_t) BgL_clausesz00_1140))))
														{	/* Eval/expdcase.scm 74 */
															BgL_test1690z00_1889 = NULLP(BgL_bodyz00_1144);
														}
													else
														{	/* Eval/expdcase.scm 74 */
															BgL_test1690z00_1889 = ((bool_t) 1);
														}
													if (BgL_test1690z00_1889)
														{	/* Eval/expdcase.scm 74 */
															return
																BGl_expandzd2errorzd2zz__expandz00
																(BGl_string1653z00zz__expander_casez00,
																BGl_string1659z00zz__expander_casez00,
																BgL_xz00_1829);
														}
													else
														{	/* Eval/expdcase.scm 74 */
															return
																BGl_expandzd2prognzd2zz__prognz00
																(BgL_bodyz00_1144);
														}
												}
											}
										else
											{	/* Eval/expdcase.scm 70 */
												obj_t BgL_carzd2134zd2_1159;

												BgL_carzd2134zd2_1159 = CAR(BgL_ezd2121zd2_1153);
												if (PAIRP(BgL_carzd2134zd2_1159))
													{	/* Eval/expdcase.scm 70 */
														bool_t BgL_test1693z00_1901;

														{	/* Eval/expdcase.scm 70 */
															obj_t BgL_tmpz00_1902;

															BgL_tmpz00_1902 = CDR(BgL_carzd2134zd2_1159);
															BgL_test1693z00_1901 = PAIRP(BgL_tmpz00_1902);
														}
														if (BgL_test1693z00_1901)
															{	/* Eval/expdcase.scm 70 */
																BgL_datumsz00_1146 = BgL_carzd2134zd2_1159;
																BgL_bodyz00_1147 = CDR(BgL_ezd2121zd2_1153);
																if (NULLP(BgL_bodyz00_1147))
																	{	/* Eval/expdcase.scm 78 */
																		return
																			BGl_expandzd2errorzd2zz__expandz00
																			(BGl_string1653z00zz__expander_casez00,
																			BGl_string1659z00zz__expander_casez00,
																			BgL_xz00_1829);
																	}
																else
																	{	/* Eval/expdcase.scm 81 */
																		obj_t BgL_arg1227z00_1186;
																		obj_t BgL_arg1228z00_1187;

																		{	/* Eval/expdcase.scm 81 */
																			obj_t BgL_arg1229z00_1188;

																			{	/* Eval/expdcase.scm 81 */
																				obj_t BgL_arg1230z00_1189;
																				obj_t BgL_arg1231z00_1190;

																				{	/* Eval/expdcase.scm 81 */
																					obj_t BgL_arg1232z00_1191;

																					{	/* Eval/expdcase.scm 81 */
																						obj_t BgL_arg1233z00_1192;

																						{	/* Eval/expdcase.scm 81 */
																							obj_t BgL_arg1234z00_1193;

																							{	/* Eval/expdcase.scm 81 */
																								obj_t BgL_arg1236z00_1194;

																								BgL_arg1236z00_1194 =
																									MAKE_YOUNG_PAIR
																									(BgL_datumsz00_1146, BNIL);
																								BgL_arg1234z00_1193 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1660z00zz__expander_casez00,
																									BgL_arg1236z00_1194);
																							}
																							BgL_arg1233z00_1192 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1234z00_1193, BNIL);
																						}
																						BgL_arg1232z00_1191 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1655z00zz__expander_casez00,
																							BgL_arg1233z00_1192);
																					}
																					BgL_arg1230z00_1189 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol1666z00zz__expander_casez00,
																						BgL_arg1232z00_1191);
																				}
																				{	/* Eval/expdcase.scm 82 */
																					obj_t BgL_arg1238z00_1195;
																					obj_t BgL_arg1239z00_1196;

																					BgL_arg1238z00_1195 =
																						BGl_expandzd2prognzd2zz__prognz00
																						(BgL_bodyz00_1147);
																					{	/* Eval/expdcase.scm 83 */
																						obj_t BgL_arg1242z00_1197;

																						{	/* Eval/expdcase.scm 83 */
																							obj_t BgL_arg1244z00_1198;

																							BgL_arg1244z00_1198 =
																								CDR(
																								((obj_t) BgL_clausesz00_1140));
																							BgL_arg1242z00_1197 =
																								BGl_loopze70ze7zz__expander_casez00
																								(BgL_xz00_1829,
																								BgL_arg1244z00_1198);
																						}
																						BgL_arg1239z00_1196 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1242z00_1197, BNIL);
																					}
																					BgL_arg1231z00_1190 =
																						MAKE_YOUNG_PAIR(BgL_arg1238z00_1195,
																						BgL_arg1239z00_1196);
																				}
																				BgL_arg1229z00_1188 =
																					MAKE_YOUNG_PAIR(BgL_arg1230z00_1189,
																					BgL_arg1231z00_1190);
																			}
																			BgL_arg1227z00_1186 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1664z00zz__expander_casez00,
																				BgL_arg1229z00_1188);
																		}
																		BgL_arg1228z00_1187 =
																			CAR(((obj_t) BgL_clausesz00_1140));
																		return
																			BGl_evepairifyz00zz__prognz00
																			(BgL_arg1227z00_1186,
																			BgL_arg1228z00_1187);
																	}
															}
														else
															{	/* Eval/expdcase.scm 70 */
																if (NULLP(CDR(((obj_t) BgL_carzd2134zd2_1159))))
																	{	/* Eval/expdcase.scm 70 */
																		obj_t BgL_arg1206z00_1167;
																		obj_t BgL_arg1208z00_1168;

																		BgL_arg1206z00_1167 =
																			CAR(((obj_t) BgL_carzd2134zd2_1159));
																		BgL_arg1208z00_1168 =
																			CDR(BgL_ezd2121zd2_1153);
																		BgL_datumsz00_1149 = BgL_arg1206z00_1167;
																		BgL_bodyz00_1150 = BgL_arg1208z00_1168;
																		if (NULLP(BgL_bodyz00_1150))
																			{	/* Eval/expdcase.scm 86 */
																				return
																					BGl_expandzd2errorzd2zz__expandz00
																					(BGl_string1653z00zz__expander_casez00,
																					BGl_string1659z00zz__expander_casez00,
																					BgL_xz00_1829);
																			}
																		else
																			{	/* Eval/expdcase.scm 89 */
																				obj_t BgL_arg1248z00_1200;
																				obj_t BgL_arg1249z00_1201;

																				{	/* Eval/expdcase.scm 89 */
																					obj_t BgL_arg1252z00_1202;

																					{	/* Eval/expdcase.scm 89 */
																						obj_t BgL_arg1268z00_1203;
																						obj_t BgL_arg1272z00_1204;

																						{	/* Eval/expdcase.scm 89 */
																							obj_t BgL_arg1284z00_1205;

																							{	/* Eval/expdcase.scm 89 */
																								obj_t BgL_arg1304z00_1206;

																								{	/* Eval/expdcase.scm 89 */
																									obj_t BgL_arg1305z00_1207;

																									{	/* Eval/expdcase.scm 89 */
																										obj_t BgL_arg1306z00_1208;

																										BgL_arg1306z00_1208 =
																											MAKE_YOUNG_PAIR
																											(BgL_datumsz00_1149,
																											BNIL);
																										BgL_arg1305z00_1207 =
																											MAKE_YOUNG_PAIR
																											(BGl_symbol1660z00zz__expander_casez00,
																											BgL_arg1306z00_1208);
																									}
																									BgL_arg1304z00_1206 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1305z00_1207, BNIL);
																								}
																								BgL_arg1284z00_1205 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1655z00zz__expander_casez00,
																									BgL_arg1304z00_1206);
																							}
																							BgL_arg1268z00_1203 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol1662z00zz__expander_casez00,
																								BgL_arg1284z00_1205);
																						}
																						{	/* Eval/expdcase.scm 90 */
																							obj_t BgL_arg1307z00_1209;
																							obj_t BgL_arg1308z00_1210;

																							BgL_arg1307z00_1209 =
																								BGl_expandzd2prognzd2zz__prognz00
																								(BgL_bodyz00_1150);
																							{	/* Eval/expdcase.scm 91 */
																								obj_t BgL_arg1309z00_1211;

																								{	/* Eval/expdcase.scm 91 */
																									obj_t BgL_arg1310z00_1212;

																									BgL_arg1310z00_1212 =
																										CDR(
																										((obj_t)
																											BgL_clausesz00_1140));
																									BgL_arg1309z00_1211 =
																										BGl_loopze70ze7zz__expander_casez00
																										(BgL_xz00_1829,
																										BgL_arg1310z00_1212);
																								}
																								BgL_arg1308z00_1210 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1309z00_1211, BNIL);
																							}
																							BgL_arg1272z00_1204 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1307z00_1209,
																								BgL_arg1308z00_1210);
																						}
																						BgL_arg1252z00_1202 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1268z00_1203,
																							BgL_arg1272z00_1204);
																					}
																					BgL_arg1248z00_1200 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol1664z00zz__expander_casez00,
																						BgL_arg1252z00_1202);
																				}
																				BgL_arg1249z00_1201 =
																					CAR(((obj_t) BgL_clausesz00_1140));
																				return
																					BGl_evepairifyz00zz__prognz00
																					(BgL_arg1248z00_1200,
																					BgL_arg1249z00_1201);
																			}
																	}
																else
																	{	/* Eval/expdcase.scm 70 */
																		return
																			BGl_expandzd2errorzd2zz__expandz00
																			(BGl_string1653z00zz__expander_casez00,
																			BGl_string1659z00zz__expander_casez00,
																			BgL_xz00_1829);
																	}
															}
													}
												else
													{	/* Eval/expdcase.scm 70 */
														return
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string1653z00zz__expander_casez00,
															BGl_string1659z00zz__expander_casez00,
															BgL_xz00_1829);
													}
											}
									}
								else
									{	/* Eval/expdcase.scm 70 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string1653z00zz__expander_casez00,
											BGl_string1659z00zz__expander_casez00, BgL_xz00_1829);
									}
							}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_casez00(void)
	{
		{	/* Eval/expdcase.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_casez00(void)
	{
		{	/* Eval/expdcase.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_casez00(void)
	{
		{	/* Eval/expdcase.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_casez00(void)
	{
		{	/* Eval/expdcase.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1670z00zz__expander_casez00));
		}

	}

#ifdef __cplusplus
}
#endif
