/*===========================================================================*/
/*   (Eval/macro.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/macro.scm -indent -o objs/obj_u/Eval/macro.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___MACRO_TYPE_DEFINITIONS
#define BGL___MACRO_TYPE_DEFINITIONS
#endif													// BGL___MACRO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__macroz00 = BUNSPEC;
	static obj_t BGl_za2compilerzd2macrozd2tableza2z00zz__macroz00 = BUNSPEC;
	extern obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_evalzd2modulezd2zz__evmodulez00(void);
	BGL_EXPORTED_DECL obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_installzd2compilerzd2expanderz00zz__macroz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2evalzd2expanderz62zz__macroz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__macroz00(void);
	BGL_EXPORTED_DECL obj_t BGl_getzd2compilerzd2expanderz00zz__macroz00(obj_t);
	static obj_t BGl_za2evalzd2macrozd2tableza2z00zz__macroz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zz__macroz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__macroz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__macroz00(void);
	extern obj_t BGl_evwarningz00zz__everrorz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__macroz00(void);
	BGL_EXPORTED_DECL obj_t BGl_getzd2evalzd2expanderz00zz__macroz00(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2compilerzd2expanderz62zz__macroz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31199ze3ze5zz__macroz00(obj_t, obj_t);
	extern bool_t BGl_evmodulezf3zf3zz__evmodulez00(obj_t);
	static obj_t BGl_z62installzd2expanderzb0zz__macroz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_installzd2evalzd2expanderz00zz__macroz00(obj_t,
		obj_t);
	extern obj_t BGl_evmodulezd2macrozd2tablez00zz__evmodulez00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__macroz00(void);
	static obj_t BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00 = BUNSPEC;
	extern obj_t BGl_hashtablezd2updatez12zc0zz__hashz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31199ze31608ze5zz__macroz00(obj_t,
		obj_t);
	static obj_t BGl_z62installzd2evalzd2expanderz62zz__macroz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_modulezd2macrozd2tablez00zz__macroz00(void);
	static obj_t BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00 = BUNSPEC;
	static obj_t BGl_z62installzd2compilerzd2expanderz62zz__macroz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2compilerzd2expanderzd2envzd2zz__macroz00,
		BgL_bgl_za762getza7d2compile1620z00,
		BGl_z62getzd2compilerzd2expanderz62zz__macroz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1609z00zz__macroz00,
		BgL_bgl_string1609za700za7za7_1621za7, "eval-macros", 11);
	      DEFINE_STRING(BGl_string1610z00zz__macroz00,
		BgL_bgl_string1610za700za7za7_1622za7, "compiler-macros", 15);
	      DEFINE_STRING(BGl_string1611z00zz__macroz00,
		BgL_bgl_string1611za700za7za7_1623za7, "install-eval-expander", 21);
	      DEFINE_STRING(BGl_string1612z00zz__macroz00,
		BgL_bgl_string1612za700za7za7_1624za7, "Illegal expander expander", 25);
	      DEFINE_STRING(BGl_string1613z00zz__macroz00,
		BgL_bgl_string1613za700za7za7_1625za7, "Illegal expander keyword", 24);
	      DEFINE_STRING(BGl_string1614z00zz__macroz00,
		BgL_bgl_string1614za700za7za7_1626za7, "Redefinition of ", 16);
	      DEFINE_STRING(BGl_string1615z00zz__macroz00,
		BgL_bgl_string1615za700za7za7_1627za7, "eval", 4);
	      DEFINE_STRING(BGl_string1616z00zz__macroz00,
		BgL_bgl_string1616za700za7za7_1628za7, " expander -- ", 13);
	      DEFINE_STRING(BGl_string1617z00zz__macroz00,
		BgL_bgl_string1617za700za7za7_1629za7, "install-expander", 16);
	      DEFINE_STRING(BGl_string1618z00zz__macroz00,
		BgL_bgl_string1618za700za7za7_1630za7, "compiler", 8);
	      DEFINE_STRING(BGl_string1619z00zz__macroz00,
		BgL_bgl_string1619za700za7za7_1631za7, "__macro", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2evalzd2expanderzd2envzd2zz__macroz00,
		BgL_bgl_za762installza7d2eva1632z00,
		BGl_z62installzd2evalzd2expanderz62zz__macroz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2evalzd2expanderzd2envzd2zz__macroz00,
		BgL_bgl_za762getza7d2evalza7d21633za7,
		BGl_z62getzd2evalzd2expanderz62zz__macroz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_installzd2expanderzd2envz00zz__macroz00,
		BgL_bgl_za762installza7d2exp1634z00,
		BGl_z62installzd2expanderzb0zz__macroz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_installzd2compilerzd2expanderzd2envzd2zz__macroz00,
		BgL_bgl_za762installza7d2com1635z00,
		BGl_z62installzd2compilerzd2expanderz62zz__macroz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__macroz00));
		   
			 ADD_ROOT((void *) (&BGl_za2compilerzd2macrozd2tableza2z00zz__macroz00));
		     ADD_ROOT((void *) (&BGl_za2evalzd2macrozd2tableza2z00zz__macroz00));
		   
			 ADD_ROOT((void *) (&BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00));
		     ADD_ROOT((void *) (&BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__macroz00(long
		BgL_checksumz00_1821, char *BgL_fromz00_1822)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__macroz00))
				{
					BGl_requirezd2initializa7ationz75zz__macroz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__macroz00();
					BGl_importedzd2moduleszd2initz00zz__macroz00();
					return BGl_toplevelzd2initzd2zz__macroz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__macroz00(void)
	{
		{	/* Eval/macro.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__macroz00(void)
	{
		{	/* Eval/macro.scm 14 */
			BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00 =
				bgl_make_spinlock(BGl_string1609z00zz__macroz00);
			BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00 =
				bgl_make_spinlock(BGl_string1610z00zz__macroz00);
			BGl_za2evalzd2macrozd2tableza2z00zz__macroz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL);
			return (BGl_za2compilerzd2macrozd2tableza2z00zz__macroz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL), BUNSPEC);
		}

	}



/* module-macro-table */
	obj_t BGl_modulezd2macrozd2tablez00zz__macroz00(void)
	{
		{	/* Eval/macro.scm 76 */
			{	/* Eval/macro.scm 77 */
				obj_t BgL_mz00_1159;

				BgL_mz00_1159 = BGl_evalzd2modulezd2zz__evmodulez00();
				if (BGl_evmodulezf3zf3zz__evmodulez00(BgL_mz00_1159))
					{	/* Eval/macro.scm 78 */
						return
							BGl_evmodulezd2macrozd2tablez00zz__evmodulez00(BgL_mz00_1159);
					}
				else
					{	/* Eval/macro.scm 78 */
						return BFALSE;
					}
			}
		}

	}



/* install-eval-expander */
	BGL_EXPORTED_DEF obj_t BGl_installzd2evalzd2expanderz00zz__macroz00(obj_t
		BgL_keywordz00_21, obj_t BgL_expanderz00_22)
	{
		{	/* Eval/macro.scm 102 */
			if (SYMBOLP(BgL_keywordz00_21))
				{	/* Eval/macro.scm 104 */
					if (PROCEDUREP(BgL_expanderz00_22))
						{	/* Eval/macro.scm 109 */
							obj_t BgL_top1641z00_1843;

							BgL_top1641z00_1843 = BGL_EXITD_TOP_AS_OBJ();
							BGL_MUTEX_LOCK(BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00);
							BGL_EXITD_PUSH_PROTECT(BgL_top1641z00_1843,
								BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00);
							BUNSPEC;
							{	/* Eval/macro.scm 109 */
								obj_t BgL_tmp1640z00_1842;

								{	/* Eval/macro.scm 110 */
									obj_t BgL_arg1206z00_1171;

									{	/* Eval/macro.scm 110 */
										obj_t BgL__ortest_1039z00_1172;

										BgL__ortest_1039z00_1172 =
											BGl_modulezd2macrozd2tablez00zz__macroz00();
										if (CBOOL(BgL__ortest_1039z00_1172))
											{	/* Eval/macro.scm 110 */
												BgL_arg1206z00_1171 = BgL__ortest_1039z00_1172;
											}
										else
											{	/* Eval/macro.scm 110 */
												BgL_arg1206z00_1171 =
													BGl_za2evalzd2macrozd2tableza2z00zz__macroz00;
											}
									}
									{	/* Eval/macro.scm 88 */
										obj_t BgL_zc3z04anonymousza31199ze3z87_1789;

										BgL_zc3z04anonymousza31199ze3z87_1789 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31199ze3ze5zz__macroz00,
											(int) (1L), (int) (2L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31199ze3z87_1789,
											(int) (0L), BgL_keywordz00_21);
										PROCEDURE_SET(BgL_zc3z04anonymousza31199ze3z87_1789,
											(int) (1L), BgL_expanderz00_22);
										BgL_tmp1640z00_1842 =
											BGl_hashtablezd2updatez12zc0zz__hashz00
											(BgL_arg1206z00_1171, BgL_keywordz00_21,
											BgL_zc3z04anonymousza31199ze3z87_1789,
											BgL_expanderz00_22);
								}}
								BGL_EXITD_POP_PROTECT(BgL_top1641z00_1843);
								BUNSPEC;
								BGL_MUTEX_UNLOCK(BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00);
								return BgL_tmp1640z00_1842;
							}
						}
					else
						{	/* Eval/macro.scm 106 */
							return
								BGl_errorz00zz__errorz00(BGl_string1611z00zz__macroz00,
								BGl_string1612z00zz__macroz00, BgL_expanderz00_22);
						}
				}
			else
				{	/* Eval/macro.scm 104 */
					return
						BGl_errorz00zz__errorz00(BGl_string1611z00zz__macroz00,
						BGl_string1613z00zz__macroz00, BgL_keywordz00_21);
				}
		}

	}



/* &install-eval-expander */
	obj_t BGl_z62installzd2evalzd2expanderz62zz__macroz00(obj_t BgL_envz00_1790,
		obj_t BgL_keywordz00_1791, obj_t BgL_expanderz00_1792)
	{
		{	/* Eval/macro.scm 102 */
			return
				BGl_installzd2evalzd2expanderz00zz__macroz00(BgL_keywordz00_1791,
				BgL_expanderz00_1792);
		}

	}



/* &<@anonymous:1199> */
	obj_t BGl_z62zc3z04anonymousza31199ze3ze5zz__macroz00(obj_t BgL_envz00_1793,
		obj_t BgL_xz00_1796)
	{
		{	/* Eval/macro.scm 87 */
			{	/* Eval/macro.scm 88 */
				obj_t BgL_keywordz00_1794;
				obj_t BgL_expanderz00_1795;

				BgL_keywordz00_1794 = PROCEDURE_REF(BgL_envz00_1793, (int) (0L));
				BgL_expanderz00_1795 = PROCEDURE_REF(BgL_envz00_1793, (int) (1L));
				{	/* Eval/macro.scm 90 */
					obj_t BgL_arg1200z00_1813;

					BgL_arg1200z00_1813 =
						string_append_3(BGl_string1614z00zz__macroz00,
						BGl_string1615z00zz__macroz00, BGl_string1616z00zz__macroz00);
					{	/* Eval/macro.scm 88 */
						obj_t BgL_list1201z00_1814;

						{	/* Eval/macro.scm 88 */
							obj_t BgL_arg1202z00_1815;

							{	/* Eval/macro.scm 88 */
								obj_t BgL_arg1203z00_1816;

								BgL_arg1203z00_1816 =
									MAKE_YOUNG_PAIR(BgL_keywordz00_1794, BNIL);
								BgL_arg1202z00_1815 =
									MAKE_YOUNG_PAIR(BgL_arg1200z00_1813, BgL_arg1203z00_1816);
							}
							BgL_list1201z00_1814 =
								MAKE_YOUNG_PAIR(BGl_string1617z00zz__macroz00,
								BgL_arg1202z00_1815);
						}
						BGl_evwarningz00zz__everrorz00(BFALSE, BgL_list1201z00_1814);
				}}
				return BgL_expanderz00_1795;
			}
		}

	}



/* install-compiler-expander */
	BGL_EXPORTED_DEF obj_t BGl_installzd2compilerzd2expanderz00zz__macroz00(obj_t
		BgL_keywordz00_23, obj_t BgL_expanderz00_24)
	{
		{	/* Eval/macro.scm 118 */
			if (SYMBOLP(BgL_keywordz00_23))
				{	/* Eval/macro.scm 120 */
					if (PROCEDUREP(BgL_expanderz00_24))
						{	/* Eval/macro.scm 125 */
							obj_t BgL_top1646z00_1877;

							BgL_top1646z00_1877 = BGL_EXITD_TOP_AS_OBJ();
							BGL_MUTEX_LOCK(BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00);
							BGL_EXITD_PUSH_PROTECT(BgL_top1646z00_1877,
								BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00);
							BUNSPEC;
							{	/* Eval/macro.scm 125 */
								obj_t BgL_tmp1645z00_1876;

								{	/* Eval/macro.scm 126 */
									obj_t BgL_tablez00_1606;

									BgL_tablez00_1606 =
										BGl_za2compilerzd2macrozd2tableza2z00zz__macroz00;
									{	/* Eval/macro.scm 88 */
										obj_t BgL_zc3z04anonymousza31199ze3z87_1797;

										BgL_zc3z04anonymousza31199ze3z87_1797 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31199ze31608ze5zz__macroz00,
											(int) (1L), (int) (2L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31199ze3z87_1797,
											(int) (0L), BgL_keywordz00_23);
										PROCEDURE_SET(BgL_zc3z04anonymousza31199ze3z87_1797,
											(int) (1L), BgL_expanderz00_24);
										BgL_tmp1645z00_1876 =
											BGl_hashtablezd2updatez12zc0zz__hashz00(BgL_tablez00_1606,
											BgL_keywordz00_23, BgL_zc3z04anonymousza31199ze3z87_1797,
											BgL_expanderz00_24);
								}}
								BGL_EXITD_POP_PROTECT(BgL_top1646z00_1877);
								BUNSPEC;
								BGL_MUTEX_UNLOCK
									(BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00);
								return BgL_tmp1645z00_1876;
							}
						}
					else
						{	/* Eval/macro.scm 122 */
							return
								BGl_errorz00zz__errorz00(BGl_string1611z00zz__macroz00,
								BGl_string1612z00zz__macroz00, BgL_expanderz00_24);
						}
				}
			else
				{	/* Eval/macro.scm 120 */
					return
						BGl_errorz00zz__errorz00(BGl_string1611z00zz__macroz00,
						BGl_string1613z00zz__macroz00, BgL_keywordz00_23);
				}
		}

	}



/* &install-compiler-expander */
	obj_t BGl_z62installzd2compilerzd2expanderz62zz__macroz00(obj_t
		BgL_envz00_1798, obj_t BgL_keywordz00_1799, obj_t BgL_expanderz00_1800)
	{
		{	/* Eval/macro.scm 118 */
			return
				BGl_installzd2compilerzd2expanderz00zz__macroz00(BgL_keywordz00_1799,
				BgL_expanderz00_1800);
		}

	}



/* &<@anonymous:1199>1608 */
	obj_t BGl_z62zc3z04anonymousza31199ze31608ze5zz__macroz00(obj_t
		BgL_envz00_1801, obj_t BgL_xz00_1804)
	{
		{	/* Eval/macro.scm 87 */
			{	/* Eval/macro.scm 88 */
				obj_t BgL_keywordz00_1802;
				obj_t BgL_expanderz00_1803;

				BgL_keywordz00_1802 = PROCEDURE_REF(BgL_envz00_1801, (int) (0L));
				BgL_expanderz00_1803 = PROCEDURE_REF(BgL_envz00_1801, (int) (1L));
				{	/* Eval/macro.scm 90 */
					obj_t BgL_arg1200z00_1817;

					BgL_arg1200z00_1817 =
						string_append_3(BGl_string1614z00zz__macroz00,
						BGl_string1618z00zz__macroz00, BGl_string1616z00zz__macroz00);
					{	/* Eval/macro.scm 88 */
						obj_t BgL_list1201z00_1818;

						{	/* Eval/macro.scm 88 */
							obj_t BgL_arg1202z00_1819;

							{	/* Eval/macro.scm 88 */
								obj_t BgL_arg1203z00_1820;

								BgL_arg1203z00_1820 =
									MAKE_YOUNG_PAIR(BgL_keywordz00_1802, BNIL);
								BgL_arg1202z00_1819 =
									MAKE_YOUNG_PAIR(BgL_arg1200z00_1817, BgL_arg1203z00_1820);
							}
							BgL_list1201z00_1818 =
								MAKE_YOUNG_PAIR(BGl_string1617z00zz__macroz00,
								BgL_arg1202z00_1819);
						}
						BGl_evwarningz00zz__everrorz00(BFALSE, BgL_list1201z00_1818);
				}}
				return BgL_expanderz00_1803;
			}
		}

	}



/* install-expander */
	BGL_EXPORTED_DEF obj_t BGl_installzd2expanderzd2zz__macroz00(obj_t
		BgL_keywordz00_25, obj_t BgL_expanderz00_26)
	{
		{	/* Eval/macro.scm 133 */
			BGl_installzd2evalzd2expanderz00zz__macroz00(BgL_keywordz00_25,
				BgL_expanderz00_26);
			BGL_TAIL return
				BGl_installzd2compilerzd2expanderz00zz__macroz00(BgL_keywordz00_25,
				BgL_expanderz00_26);
		}

	}



/* &install-expander */
	obj_t BGl_z62installzd2expanderzb0zz__macroz00(obj_t BgL_envz00_1805,
		obj_t BgL_keywordz00_1806, obj_t BgL_expanderz00_1807)
	{
		{	/* Eval/macro.scm 133 */
			return
				BGl_installzd2expanderzd2zz__macroz00(BgL_keywordz00_1806,
				BgL_expanderz00_1807);
		}

	}



/* get-eval-expander */
	BGL_EXPORTED_DEF obj_t BGl_getzd2evalzd2expanderz00zz__macroz00(obj_t
		BgL_keywordz00_27)
	{
		{	/* Eval/macro.scm 142 */
			{	/* Eval/macro.scm 143 */
				obj_t BgL_top1648z00_1907;

				BgL_top1648z00_1907 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1648z00_1907,
					BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00);
				BUNSPEC;
				{	/* Eval/macro.scm 143 */
					obj_t BgL_tmp1647z00_1906;

					{	/* Eval/macro.scm 144 */
						obj_t BgL_mtablez00_1175;

						BgL_mtablez00_1175 = BGl_modulezd2macrozd2tablez00zz__macroz00();
						{	/* Eval/macro.scm 145 */
							obj_t BgL__ortest_1040z00_1176;

							if (CBOOL(BgL_mtablez00_1175))
								{	/* Eval/macro.scm 145 */
									BgL__ortest_1040z00_1176 =
										BGl_hashtablezd2getzd2zz__hashz00(BgL_mtablez00_1175,
										BgL_keywordz00_27);
								}
							else
								{	/* Eval/macro.scm 145 */
									BgL__ortest_1040z00_1176 = BFALSE;
								}
							if (CBOOL(BgL__ortest_1040z00_1176))
								{	/* Eval/macro.scm 145 */
									BgL_tmp1647z00_1906 = BgL__ortest_1040z00_1176;
								}
							else
								{	/* Eval/macro.scm 145 */
									BgL_tmp1647z00_1906 =
										BGl_hashtablezd2getzd2zz__hashz00
										(BGl_za2evalzd2macrozd2tableza2z00zz__macroz00,
										BgL_keywordz00_27);
								}
						}
					}
					BGL_EXITD_POP_PROTECT(BgL_top1648z00_1907);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2evalzd2macrozd2mutexza2z00zz__macroz00);
					return BgL_tmp1647z00_1906;
				}
			}
		}

	}



/* &get-eval-expander */
	obj_t BGl_z62getzd2evalzd2expanderz62zz__macroz00(obj_t BgL_envz00_1808,
		obj_t BgL_keywordz00_1809)
	{
		{	/* Eval/macro.scm 142 */
			return BGl_getzd2evalzd2expanderz00zz__macroz00(BgL_keywordz00_1809);
		}

	}



/* get-compiler-expander */
	BGL_EXPORTED_DEF obj_t BGl_getzd2compilerzd2expanderz00zz__macroz00(obj_t
		BgL_keywordz00_28)
	{
		{	/* Eval/macro.scm 156 */
			{	/* Eval/macro.scm 157 */
				obj_t BgL_top1652z00_1922;

				BgL_top1652z00_1922 = BGL_EXITD_TOP_AS_OBJ();
				BGL_MUTEX_LOCK(BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00);
				BGL_EXITD_PUSH_PROTECT(BgL_top1652z00_1922,
					BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00);
				BUNSPEC;
				{	/* Eval/macro.scm 157 */
					obj_t BgL_tmp1651z00_1921;

					BgL_tmp1651z00_1921 =
						BGl_hashtablezd2getzd2zz__hashz00
						(BGl_za2compilerzd2macrozd2tableza2z00zz__macroz00,
						BgL_keywordz00_28);
					BGL_EXITD_POP_PROTECT(BgL_top1652z00_1922);
					BUNSPEC;
					BGL_MUTEX_UNLOCK(BGl_za2compilerzd2macrozd2mutexza2z00zz__macroz00);
					return BgL_tmp1651z00_1921;
				}
			}
		}

	}



/* &get-compiler-expander */
	obj_t BGl_z62getzd2compilerzd2expanderz62zz__macroz00(obj_t BgL_envz00_1810,
		obj_t BgL_keywordz00_1811)
	{
		{	/* Eval/macro.scm 156 */
			return BGl_getzd2compilerzd2expanderz00zz__macroz00(BgL_keywordz00_1811);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__macroz00(void)
	{
		{	/* Eval/macro.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__macroz00(void)
	{
		{	/* Eval/macro.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__macroz00(void)
	{
		{	/* Eval/macro.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__macroz00(void)
	{
		{	/* Eval/macro.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1619z00zz__macroz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string1619z00zz__macroz00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string1619z00zz__macroz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1619z00zz__macroz00));
			return
				BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1619z00zz__macroz00));
		}

	}

#ifdef __cplusplus
}
#endif
