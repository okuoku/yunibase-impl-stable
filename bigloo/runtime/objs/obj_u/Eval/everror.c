/*===========================================================================*/
/*   (Eval/everror.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/everror.scm -indent -o objs/obj_u/Eval/everror.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVERROR_TYPE_DEFINITIONS
#define BGL___EVERROR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62warningz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                    *BgL_z62warningz62_bglt;

	typedef struct BgL_z62evalzd2warningzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_argsz00;
	}                           *BgL_z62evalzd2warningzb0_bglt;


#endif													// BGL___EVERROR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_z62evalzd2warningzb0zz__objectz00;
	static obj_t BGl_requirezd2initializa7ationz75zz__everrorz00 = BUNSPEC;
	static obj_t BGl_z62evwarningz62zz__everrorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	static obj_t BGl_z62evtypezd2errorzb0zz__everrorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__everrorz00(void);
	BGL_EXPORTED_DECL obj_t BGl_evarityzd2errorzd2zz__everrorz00(obj_t, obj_t,
		int, int);
	static obj_t BGl_genericzd2initzd2zz__everrorz00(void);
	extern obj_t BGl_getzd2tracezd2stackz00zz__errorz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__everrorz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__everrorz00(void);
	BGL_EXPORTED_DECL obj_t BGl_evwarningz00zz__everrorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_evtypezd2errorzd2zz__everrorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__everrorz00(void);
	extern obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62evarityzd2errorzb0zz__everrorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_warningzd2notifyzd2zz__errorz00(obj_t);
	static obj_t BGl_symbol1623z00zz__everrorz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__everrorz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_everrorz00zz__everrorz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62everrorz62zz__everrorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_evarityzd2errorzd2envz00zz__everrorz00,
		BgL_bgl_za762evarityza7d2err1631z00,
		BGl_z62evarityzd2errorzb0zz__everrorz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evtypezd2errorzd2envz00zz__everrorz00,
		BgL_bgl_za762evtypeza7d2erro1632z00, BGl_z62evtypezd2errorzb0zz__everrorz00,
		0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1624z00zz__everrorz00,
		BgL_bgl_string1624za700za7za7_1633za7, "at", 2);
	      DEFINE_STRING(BGl_string1625z00zz__everrorz00,
		BgL_bgl_string1625za700za7za7_1634za7,
		"Wrong number of arguments: ~a expected, ~a provided ", 52);
	      DEFINE_STRING(BGl_string1626z00zz__everrorz00,
		BgL_bgl_string1626za700za7za7_1635za7, "eval", 4);
	      DEFINE_STRING(BGl_string1627z00zz__everrorz00,
		BgL_bgl_string1627za700za7za7_1636za7,
		"/tmp/bigloo/runtime/Eval/everror.scm", 36);
	      DEFINE_STRING(BGl_string1628z00zz__everrorz00,
		BgL_bgl_string1628za700za7za7_1637za7, "&evarity-error", 14);
	      DEFINE_STRING(BGl_string1629z00zz__everrorz00,
		BgL_bgl_string1629za700za7za7_1638za7, "bint", 4);
	      DEFINE_STRING(BGl_string1630z00zz__everrorz00,
		BgL_bgl_string1630za700za7za7_1639za7, "__everror", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_evwarningzd2envzd2zz__everrorz00,
		BgL_bgl_za762evwarningza762za71640za7, va_generic_entry,
		BGl_z62evwarningz62zz__everrorz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_everrorzd2envzd2zz__everrorz00,
		BgL_bgl_za762everrorza762za7za7_1641z00, BGl_z62everrorz62zz__everrorz00,
		0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__everrorz00));
		     ADD_ROOT((void *) (&BGl_symbol1623z00zz__everrorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long
		BgL_checksumz00_1827, char *BgL_fromz00_1828)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__everrorz00))
				{
					BGl_requirezd2initializa7ationz75zz__everrorz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__everrorz00();
					BGl_cnstzd2initzd2zz__everrorz00();
					BGl_importedzd2moduleszd2initz00zz__everrorz00();
					return BGl_methodzd2initzd2zz__everrorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__everrorz00(void)
	{
		{	/* Eval/everror.scm 15 */
			return (BGl_symbol1623z00zz__everrorz00 =
				bstring_to_symbol(BGl_string1624z00zz__everrorz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__everrorz00(void)
	{
		{	/* Eval/everror.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* everror */
	BGL_EXPORTED_DEF obj_t BGl_everrorz00zz__everrorz00(obj_t BgL_locz00_3,
		obj_t BgL_procz00_4, obj_t BgL_mesz00_5, obj_t BgL_objz00_6)
	{
		{	/* Eval/everror.scm 55 */
			if (PAIRP(BgL_locz00_3))
				{	/* Eval/everror.scm 56 */
					obj_t BgL_cdrzd2109zd2_1130;

					BgL_cdrzd2109zd2_1130 = CDR(((obj_t) BgL_locz00_3));
					if ((CAR(((obj_t) BgL_locz00_3)) == BGl_symbol1623z00zz__everrorz00))
						{	/* Eval/everror.scm 56 */
							if (PAIRP(BgL_cdrzd2109zd2_1130))
								{	/* Eval/everror.scm 56 */
									obj_t BgL_cdrzd2113zd2_1134;

									BgL_cdrzd2113zd2_1134 = CDR(BgL_cdrzd2109zd2_1130);
									if (PAIRP(BgL_cdrzd2113zd2_1134))
										{	/* Eval/everror.scm 56 */
											if (NULLP(CDR(BgL_cdrzd2113zd2_1134)))
												{	/* Eval/everror.scm 56 */
													return
														BGl_errorzf2locationzf2zz__errorz00(BgL_procz00_4,
														BgL_mesz00_5, BgL_objz00_6,
														CAR(BgL_cdrzd2109zd2_1130),
														CAR(BgL_cdrzd2113zd2_1134));
												}
											else
												{	/* Eval/everror.scm 56 */
													return
														BGl_errorz00zz__errorz00(BgL_procz00_4,
														BgL_mesz00_5, BgL_objz00_6);
												}
										}
									else
										{	/* Eval/everror.scm 56 */
											return
												BGl_errorz00zz__errorz00(BgL_procz00_4, BgL_mesz00_5,
												BgL_objz00_6);
										}
								}
							else
								{	/* Eval/everror.scm 56 */
									return
										BGl_errorz00zz__errorz00(BgL_procz00_4, BgL_mesz00_5,
										BgL_objz00_6);
								}
						}
					else
						{	/* Eval/everror.scm 56 */
							return
								BGl_errorz00zz__errorz00(BgL_procz00_4, BgL_mesz00_5,
								BgL_objz00_6);
						}
				}
			else
				{	/* Eval/everror.scm 56 */
					return
						BGl_errorz00zz__errorz00(BgL_procz00_4, BgL_mesz00_5, BgL_objz00_6);
				}
		}

	}



/* &everror */
	obj_t BGl_z62everrorz62zz__everrorz00(obj_t BgL_envz00_1806,
		obj_t BgL_locz00_1807, obj_t BgL_procz00_1808, obj_t BgL_mesz00_1809,
		obj_t BgL_objz00_1810)
	{
		{	/* Eval/everror.scm 55 */
			return
				BGl_everrorz00zz__everrorz00(BgL_locz00_1807, BgL_procz00_1808,
				BgL_mesz00_1809, BgL_objz00_1810);
		}

	}



/* evtype-error */
	BGL_EXPORTED_DEF obj_t BGl_evtypezd2errorzd2zz__everrorz00(obj_t BgL_locz00_7,
		obj_t BgL_procz00_8, obj_t BgL_mesz00_9, obj_t BgL_objz00_10)
	{
		{	/* Eval/everror.scm 65 */
			if (PAIRP(BgL_locz00_7))
				{	/* Eval/everror.scm 66 */
					obj_t BgL_cdrzd2127zd2_1148;

					BgL_cdrzd2127zd2_1148 = CDR(((obj_t) BgL_locz00_7));
					if ((CAR(((obj_t) BgL_locz00_7)) == BGl_symbol1623z00zz__everrorz00))
						{	/* Eval/everror.scm 66 */
							if (PAIRP(BgL_cdrzd2127zd2_1148))
								{	/* Eval/everror.scm 66 */
									obj_t BgL_cdrzd2131zd2_1152;

									BgL_cdrzd2131zd2_1152 = CDR(BgL_cdrzd2127zd2_1148);
									if (PAIRP(BgL_cdrzd2131zd2_1152))
										{	/* Eval/everror.scm 66 */
											if (NULLP(CDR(BgL_cdrzd2131zd2_1152)))
												{	/* Eval/everror.scm 66 */
													return
														BGl_bigloozd2typezd2errorzf2locationzf2zz__errorz00
														(BgL_procz00_8, BgL_mesz00_9, BgL_objz00_10,
														CAR(BgL_cdrzd2127zd2_1148),
														CAR(BgL_cdrzd2131zd2_1152));
												}
											else
												{	/* Eval/everror.scm 66 */
													return
														BGl_bigloozd2typezd2errorz00zz__errorz00
														(BgL_procz00_8, BgL_mesz00_9, BgL_objz00_10);
												}
										}
									else
										{	/* Eval/everror.scm 66 */
											return
												BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procz00_8,
												BgL_mesz00_9, BgL_objz00_10);
										}
								}
							else
								{	/* Eval/everror.scm 66 */
									return
										BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procz00_8,
										BgL_mesz00_9, BgL_objz00_10);
								}
						}
					else
						{	/* Eval/everror.scm 66 */
							return
								BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procz00_8,
								BgL_mesz00_9, BgL_objz00_10);
						}
				}
			else
				{	/* Eval/everror.scm 66 */
					return
						BGl_bigloozd2typezd2errorz00zz__errorz00(BgL_procz00_8,
						BgL_mesz00_9, BgL_objz00_10);
				}
		}

	}



/* &evtype-error */
	obj_t BGl_z62evtypezd2errorzb0zz__everrorz00(obj_t BgL_envz00_1811,
		obj_t BgL_locz00_1812, obj_t BgL_procz00_1813, obj_t BgL_mesz00_1814,
		obj_t BgL_objz00_1815)
	{
		{	/* Eval/everror.scm 65 */
			return
				BGl_evtypezd2errorzd2zz__everrorz00(BgL_locz00_1812, BgL_procz00_1813,
				BgL_mesz00_1814, BgL_objz00_1815);
		}

	}



/* evarity-error */
	BGL_EXPORTED_DEF obj_t BGl_evarityzd2errorzd2zz__everrorz00(obj_t
		BgL_locz00_11, obj_t BgL_namez00_12, int BgL_providez00_13,
		int BgL_expectz00_14)
	{
		{	/* Eval/everror.scm 75 */
			{	/* Eval/everror.scm 76 */
				obj_t BgL_msgz00_1615;

				{	/* Eval/everror.scm 76 */
					obj_t BgL_list1203z00_1616;

					{	/* Eval/everror.scm 76 */
						obj_t BgL_arg1206z00_1617;

						BgL_arg1206z00_1617 =
							MAKE_YOUNG_PAIR(BINT(BgL_providez00_13), BNIL);
						BgL_list1203z00_1616 =
							MAKE_YOUNG_PAIR(BINT(BgL_expectz00_14), BgL_arg1206z00_1617);
					}
					BgL_msgz00_1615 =
						BGl_formatz00zz__r4_output_6_10_3z00
						(BGl_string1625z00zz__everrorz00, BgL_list1203z00_1616);
				}
				return
					BGl_everrorz00zz__everrorz00(BgL_locz00_11,
					BGl_string1626z00zz__everrorz00, BgL_msgz00_1615, BgL_namez00_12);
			}
		}

	}



/* &evarity-error */
	obj_t BGl_z62evarityzd2errorzb0zz__everrorz00(obj_t BgL_envz00_1816,
		obj_t BgL_locz00_1817, obj_t BgL_namez00_1818, obj_t BgL_providez00_1819,
		obj_t BgL_expectz00_1820)
	{
		{	/* Eval/everror.scm 75 */
			{	/* Eval/everror.scm 76 */
				int BgL_auxz00_1903;
				int BgL_auxz00_1894;

				{	/* Eval/everror.scm 76 */
					obj_t BgL_tmpz00_1904;

					if (INTEGERP(BgL_expectz00_1820))
						{	/* Eval/everror.scm 76 */
							BgL_tmpz00_1904 = BgL_expectz00_1820;
						}
					else
						{
							obj_t BgL_auxz00_1907;

							BgL_auxz00_1907 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1627z00zz__everrorz00,
								BINT(2808L), BGl_string1628z00zz__everrorz00,
								BGl_string1629z00zz__everrorz00, BgL_expectz00_1820);
							FAILURE(BgL_auxz00_1907, BFALSE, BFALSE);
						}
					BgL_auxz00_1903 = CINT(BgL_tmpz00_1904);
				}
				{	/* Eval/everror.scm 76 */
					obj_t BgL_tmpz00_1895;

					if (INTEGERP(BgL_providez00_1819))
						{	/* Eval/everror.scm 76 */
							BgL_tmpz00_1895 = BgL_providez00_1819;
						}
					else
						{
							obj_t BgL_auxz00_1898;

							BgL_auxz00_1898 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1627z00zz__everrorz00,
								BINT(2808L), BGl_string1628z00zz__everrorz00,
								BGl_string1629z00zz__everrorz00, BgL_providez00_1819);
							FAILURE(BgL_auxz00_1898, BFALSE, BFALSE);
						}
					BgL_auxz00_1894 = CINT(BgL_tmpz00_1895);
				}
				return
					BGl_evarityzd2errorzd2zz__everrorz00(BgL_locz00_1817,
					BgL_namez00_1818, BgL_auxz00_1894, BgL_auxz00_1903);
			}
		}

	}



/* evwarning */
	BGL_EXPORTED_DEF obj_t BGl_evwarningz00zz__everrorz00(obj_t BgL_locz00_15,
		obj_t BgL_argsz00_16)
	{
		{	/* Eval/everror.scm 83 */
			{
				obj_t BgL_fnamez00_1163;
				obj_t BgL_locz00_1164;

				if (PAIRP(BgL_locz00_15))
					{	/* Eval/everror.scm 84 */
						obj_t BgL_cdrzd2145zd2_1169;

						BgL_cdrzd2145zd2_1169 = CDR(((obj_t) BgL_locz00_15));
						if (
							(CAR(((obj_t) BgL_locz00_15)) == BGl_symbol1623z00zz__everrorz00))
							{	/* Eval/everror.scm 84 */
								if (PAIRP(BgL_cdrzd2145zd2_1169))
									{	/* Eval/everror.scm 84 */
										obj_t BgL_cdrzd2149zd2_1173;

										BgL_cdrzd2149zd2_1173 = CDR(BgL_cdrzd2145zd2_1169);
										if (PAIRP(BgL_cdrzd2149zd2_1173))
											{	/* Eval/everror.scm 84 */
												if (NULLP(CDR(BgL_cdrzd2149zd2_1173)))
													{	/* Eval/everror.scm 84 */
														BgL_fnamez00_1163 = CAR(BgL_cdrzd2145zd2_1169);
														BgL_locz00_1164 = CAR(BgL_cdrzd2149zd2_1173);
														{	/* Eval/everror.scm 87 */
															BgL_z62evalzd2warningzb0_bglt BgL_arg1220z00_1181;

															{	/* Eval/everror.scm 87 */
																BgL_z62evalzd2warningzb0_bglt
																	BgL_new1040z00_1182;
																{	/* Eval/everror.scm 88 */
																	BgL_z62evalzd2warningzb0_bglt
																		BgL_new1039z00_1184;
																	BgL_new1039z00_1184 =
																		((BgL_z62evalzd2warningzb0_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_z62evalzd2warningzb0_bgl))));
																	{	/* Eval/everror.scm 88 */
																		long BgL_arg1221z00_1185;

																		BgL_arg1221z00_1185 =
																			BGL_CLASS_NUM
																			(BGl_z62evalzd2warningzb0zz__objectz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1039z00_1184),
																			BgL_arg1221z00_1185);
																	}
																	BgL_new1040z00_1182 = BgL_new1039z00_1184;
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1040z00_1182)))->
																		BgL_fnamez00) =
																	((obj_t) BgL_fnamez00_1163), BUNSPEC);
																((((BgL_z62exceptionz62_bglt)
																			COBJECT(((BgL_z62exceptionz62_bglt)
																					BgL_new1040z00_1182)))->
																		BgL_locationz00) =
																	((obj_t) BgL_locz00_1164), BUNSPEC);
																{
																	obj_t BgL_auxz00_1937;

																	{	/* Eval/everror.scm 90 */

																		{	/* Eval/everror.scm 90 */

																			BgL_auxz00_1937 =
																				BGl_getzd2tracezd2stackz00zz__errorz00
																				(BFALSE);
																	}}
																	((((BgL_z62exceptionz62_bglt) COBJECT(
																					((BgL_z62exceptionz62_bglt)
																						BgL_new1040z00_1182)))->
																			BgL_stackz00) =
																		((obj_t) BgL_auxz00_1937), BUNSPEC);
																}
																((((BgL_z62warningz62_bglt) COBJECT(
																				((BgL_z62warningz62_bglt)
																					BgL_new1040z00_1182)))->BgL_argsz00) =
																	((obj_t) BgL_argsz00_16), BUNSPEC);
																BgL_arg1220z00_1181 = BgL_new1040z00_1182;
															}
															return
																BGl_warningzd2notifyzd2zz__errorz00(
																((obj_t) BgL_arg1220z00_1181));
														}
													}
												else
													{	/* Eval/everror.scm 84 */
													BgL_tagzd2138zd2_1166:
														{	/* Eval/everror.scm 94 */
															BgL_z62evalzd2warningzb0_bglt BgL_arg1223z00_1186;

															{	/* Eval/everror.scm 94 */
																BgL_z62evalzd2warningzb0_bglt
																	BgL_new1042z00_1187;
																{	/* Eval/everror.scm 95 */
																	BgL_z62evalzd2warningzb0_bglt
																		BgL_new1041z00_1189;
																	BgL_new1041z00_1189 =
																		((BgL_z62evalzd2warningzb0_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_z62evalzd2warningzb0_bgl))));
																	{	/* Eval/everror.scm 95 */
																		long BgL_arg1225z00_1190;

																		BgL_arg1225z00_1190 =
																			BGL_CLASS_NUM
																			(BGl_z62evalzd2warningzb0zz__objectz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1041z00_1189),
																			BgL_arg1225z00_1190);
																	}
																	BgL_new1042z00_1187 = BgL_new1041z00_1189;
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1042z00_1187)))->
																		BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																((((BgL_z62exceptionz62_bglt)
																			COBJECT(((BgL_z62exceptionz62_bglt)
																					BgL_new1042z00_1187)))->
																		BgL_locationz00) =
																	((obj_t) BFALSE), BUNSPEC);
																{
																	obj_t BgL_auxz00_1955;

																	{	/* Eval/everror.scm 97 */

																		{	/* Eval/everror.scm 97 */

																			BgL_auxz00_1955 =
																				BGl_getzd2tracezd2stackz00zz__errorz00
																				(BFALSE);
																	}}
																	((((BgL_z62exceptionz62_bglt) COBJECT(
																					((BgL_z62exceptionz62_bglt)
																						BgL_new1042z00_1187)))->
																			BgL_stackz00) =
																		((obj_t) BgL_auxz00_1955), BUNSPEC);
																}
																((((BgL_z62warningz62_bglt) COBJECT(
																				((BgL_z62warningz62_bglt)
																					BgL_new1042z00_1187)))->BgL_argsz00) =
																	((obj_t) BgL_argsz00_16), BUNSPEC);
																BgL_arg1223z00_1186 = BgL_new1042z00_1187;
															}
															return
																BGl_warningzd2notifyzd2zz__errorz00(
																((obj_t) BgL_arg1223z00_1186));
														}
													}
											}
										else
											{	/* Eval/everror.scm 84 */
												goto BgL_tagzd2138zd2_1166;
											}
									}
								else
									{	/* Eval/everror.scm 84 */
										goto BgL_tagzd2138zd2_1166;
									}
							}
						else
							{	/* Eval/everror.scm 84 */
								goto BgL_tagzd2138zd2_1166;
							}
					}
				else
					{	/* Eval/everror.scm 84 */
						goto BgL_tagzd2138zd2_1166;
					}
			}
		}

	}



/* &evwarning */
	obj_t BGl_z62evwarningz62zz__everrorz00(obj_t BgL_envz00_1821,
		obj_t BgL_locz00_1822, obj_t BgL_argsz00_1823)
	{
		{	/* Eval/everror.scm 83 */
			return BGl_evwarningz00zz__everrorz00(BgL_locz00_1822, BgL_argsz00_1823);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__everrorz00(void)
	{
		{	/* Eval/everror.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__everrorz00(void)
	{
		{	/* Eval/everror.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__everrorz00(void)
	{
		{	/* Eval/everror.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__everrorz00(void)
	{
		{	/* Eval/everror.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
			return BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1630z00zz__everrorz00));
		}

	}

#ifdef __cplusplus
}
#endif
