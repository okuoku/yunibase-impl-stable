/*===========================================================================*/
/*   (Eval/evaluate_use.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/evaluate_use.scm -indent -o objs/obj_u/Eval/evaluate_use.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EVALUATE_USE_TYPE_DEFINITIONS
#define BGL___EVALUATE_USE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ev_exprz00_bgl
	{
		header_t header;
		obj_t widening;
	}                 *BgL_ev_exprz00_bglt;

	typedef struct BgL_ev_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		obj_t BgL_effz00;
		obj_t BgL_typez00;
	}                *BgL_ev_varz00_bglt;

	typedef struct BgL_ev_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_pz00;
		struct BgL_ev_exprz00_bgl *BgL_tz00;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}               *BgL_ev_ifz00_bglt;

	typedef struct BgL_ev_listz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_argsz00;
	}                 *BgL_ev_listz00_bglt;

	typedef struct BgL_ev_prog2z00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_e1z00;
		struct BgL_ev_exprz00_bgl *BgL_e2z00;
	}                  *BgL_ev_prog2z00_bglt;

	typedef struct BgL_ev_hookz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
	}                 *BgL_ev_hookz00_bglt;

	typedef struct BgL_ev_setlocalz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_varz00_bgl *BgL_vz00;
	}                     *BgL_ev_setlocalz00_bglt;

	typedef struct BgL_ev_bindzd2exitzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_varz00_bgl *BgL_varz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                        *BgL_ev_bindzd2exitzd2_bglt;

	typedef struct BgL_ev_unwindzd2protectzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_ez00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                             *BgL_ev_unwindzd2protectzd2_bglt;

	typedef struct BgL_ev_withzd2handlerzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_ev_exprz00_bgl *BgL_handlerz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                           *BgL_ev_withzd2handlerzd2_bglt;

	typedef struct BgL_ev_synchroniza7eza7_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_mutexz00;
		struct BgL_ev_exprz00_bgl *BgL_prelockz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                          *BgL_ev_synchroniza7eza7_bglt;

	typedef struct BgL_ev_binderz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
	}                   *BgL_ev_binderz00_bglt;

	typedef struct BgL_ev_labelsz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_varsz00;
		obj_t BgL_valsz00;
		obj_t BgL_envz00;
		obj_t BgL_stkz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		obj_t BgL_boxesz00;
	}                   *BgL_ev_labelsz00_bglt;

	typedef struct BgL_ev_gotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_varz00_bgl *BgL_labelz00;
		struct BgL_ev_labelsz00_bgl *BgL_labelsz00;
		obj_t BgL_argsz00;
	}                 *BgL_ev_gotoz00_bglt;

	typedef struct BgL_ev_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_ev_exprz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_tailzf3zf3;
	}                *BgL_ev_appz00_bglt;

	typedef struct BgL_ev_absz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_wherez00;
		obj_t BgL_arityz00;
		obj_t BgL_varsz00;
		struct BgL_ev_exprz00_bgl *BgL_bodyz00;
		int BgL_siza7eza7;
		obj_t BgL_bindz00;
		obj_t BgL_freez00;
		obj_t BgL_innerz00;
		obj_t BgL_boxesz00;
	}                *BgL_ev_absz00_bglt;


#endif													// BGL___EVALUATE_USE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62usezd2ev_synchroniza7e1159z17zz__evaluate_usez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62usezd2ev_prog21147zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__evaluate_usez00 = BUNSPEC;
	extern obj_t BGl_ev_gotoz00zz__evaluate_typesz00;
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_usez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evmodulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__everrorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evcompilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__typez00(long, char *);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_ev_globalz00zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_app1169zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_binderz00zz__evaluate_typesz00;
	extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_listz00zz__evaluate_typesz00;
	static obj_t BGl_toplevelzd2initzd2zz__evaluate_usez00(void);
	extern obj_t BGl_ev_littz00zz__evaluate_typesz00;
	static obj_t BGl_z62use1134z62zz__evaluate_usez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_ev_varz00zz__evaluate_typesz00;
	static obj_t BGl_cnstzd2initzd2zz__evaluate_usez00(void);
	static obj_t BGl_genericzd2initzd2zz__evaluate_usez00(void);
	extern obj_t BGl_ev_prog2z00zz__evaluate_typesz00;
	static obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_usez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__evaluate_usez00(void);
	static obj_t BGl_z62usez62zz__evaluate_usez00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__evaluate_usez00(void);
	static obj_t BGl_z62usezd2ev_hook1149zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_bindzd2exitzd2zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_list1145zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_withzd2handlerzd2zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_withzd2handler1157z62zz__evaluate_usez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62usezd2ev_unwindzd2protec1155z62zz__evaluate_usez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62usezd2ev_binder1161zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62usezd2ev_binder1163zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_global1139zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_exprz00zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_abs1171zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_appz00zz__evaluate_typesz00;
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_ev_labelsz00zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_goto1167zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__evaluate_usez00(void);
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62usezd2ev_bindzd2exit1153z62zz__evaluate_usez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62usezd2ev_var1137zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_ev_absz00zz__evaluate_typesz00;
	extern obj_t BGl_ev_synchroniza7eza7zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_labels1165zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62usezd2ev_litt1141zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62usezd2ev_if1143zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol1735z00zz__evaluate_usez00 = BUNSPEC;
	extern obj_t BGl_ev_ifz00zz__evaluate_typesz00;
	BGL_EXPORTED_DECL obj_t BGl_usez00zz__evaluate_usez00(BgL_ev_exprz00_bglt,
		obj_t);
	extern obj_t BGl_ev_setlocalz00zz__evaluate_typesz00;
	static obj_t BGl_z62usezd2ev_setlocal1151zb0zz__evaluate_usez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_ev_hookz00zz__evaluate_typesz00;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_GENERIC(BGl_usezd2envzd2zz__evaluate_usez00,
		BgL_bgl_za762useza762za7za7__eva1761z00, BGl_z62usez62zz__evaluate_usez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1734z00zz__evaluate_usez00,
		BgL_bgl_string1734za700za7za7_1762za7, "use1134", 7);
	      DEFINE_STRING(BGl_string1736z00zz__evaluate_usez00,
		BgL_bgl_string1736za700za7za7_1763za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1737z00zz__evaluate_usez00,
		BgL_bgl_string1737za700za7za7_1764za7,
		"/tmp/bigloo/runtime/Eval/evaluate_use.scm", 41);
	      DEFINE_STRING(BGl_string1738z00zz__evaluate_usez00,
		BgL_bgl_string1738za700za7za7_1765za7, "&use", 4);
	      DEFINE_STRING(BGl_string1739z00zz__evaluate_usez00,
		BgL_bgl_string1739za700za7za7_1766za7, "ev_expr", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1733z00zz__evaluate_usez00,
		BgL_bgl_za762use1134za762za7za7_1767z00,
		BGl_z62use1134z62zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1741z00zz__evaluate_usez00,
		BgL_bgl_string1741za700za7za7_1768za7, "use", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1740z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_var11769z00,
		BGl_z62usezd2ev_var1137zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1742z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_glob1770z00,
		BGl_z62usezd2ev_global1139zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1743z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_litt1771z00,
		BGl_z62usezd2ev_litt1141zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1744z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_if111772z00,
		BGl_z62usezd2ev_if1143zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1745z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_list1773z00,
		BGl_z62usezd2ev_list1145zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1746z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_prog1774z00,
		BGl_z62usezd2ev_prog21147zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1747z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_hook1775z00,
		BGl_z62usezd2ev_hook1149zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1748z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_setl1776z00,
		BGl_z62usezd2ev_setlocal1151zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1749z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_bind1777z00,
		BGl_z62usezd2ev_bindzd2exit1153z62zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1759z00zz__evaluate_usez00,
		BgL_bgl_string1759za700za7za7_1778za7, "__evaluate_use", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1750z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_unwi1779z00,
		BGl_z62usezd2ev_unwindzd2protec1155z62zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1751z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_with1780z00,
		BGl_z62usezd2ev_withzd2handler1157z62zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1752z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_sync1781z00,
		BGl_z62usezd2ev_synchroniza7e1159z17zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1753z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_bind1782z00,
		BGl_z62usezd2ev_binder1161zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1754z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_bind1783z00,
		BGl_z62usezd2ev_binder1163zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1755z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_labe1784z00,
		BGl_z62usezd2ev_labels1165zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1756z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_goto1785z00,
		BGl_z62usezd2ev_goto1167zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1757z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_app11786z00,
		BGl_z62usezd2ev_app1169zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1758z00zz__evaluate_usez00,
		BgL_bgl_za762useza7d2ev_abs11787z00,
		BGl_z62usezd2ev_abs1171zb0zz__evaluate_usez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__evaluate_usez00));
		     ADD_ROOT((void *) (&BGl_symbol1735z00zz__evaluate_usez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__evaluate_usez00(long
		BgL_checksumz00_2388, char *BgL_fromz00_2389)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__evaluate_usez00))
				{
					BGl_requirezd2initializa7ationz75zz__evaluate_usez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__evaluate_usez00();
					BGl_cnstzd2initzd2zz__evaluate_usez00();
					BGl_importedzd2moduleszd2initz00zz__evaluate_usez00();
					BGl_genericzd2initzd2zz__evaluate_usez00();
					BGl_methodzd2initzd2zz__evaluate_usez00();
					return BGl_toplevelzd2initzd2zz__evaluate_usez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			return (BGl_symbol1735z00zz__evaluate_usez00 =
				bstring_to_symbol(BGl_string1734z00zz__evaluate_usez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_proc1733z00zz__evaluate_usez00, BGl_ev_exprz00zz__evaluate_typesz00,
				BGl_string1734z00zz__evaluate_usez00);
		}

	}



/* &use1134 */
	obj_t BGl_z62use1134z62zz__evaluate_usez00(obj_t BgL_envz00_2192,
		obj_t BgL_ez00_2193, obj_t BgL_donez00_2194)
	{
		{	/* Eval/evaluate_use.scm 62 */
			return
				BGl_errorz00zz__errorz00(BGl_symbol1735z00zz__evaluate_usez00,
				BGl_string1736z00zz__evaluate_usez00,
				((obj_t) ((BgL_ev_exprz00_bglt) BgL_ez00_2193)));
		}

	}



/* use */
	BGL_EXPORTED_DEF obj_t BGl_usez00zz__evaluate_usez00(BgL_ev_exprz00_bglt
		BgL_ez00_3, obj_t BgL_donez00_4)
	{
		{	/* Eval/evaluate_use.scm 62 */
			{	/* Eval/evaluate_use.scm 62 */
				obj_t BgL_method1135z00_1178;

				{	/* Eval/evaluate_use.scm 62 */
					obj_t BgL_res1719z00_1792;

					{	/* Eval/evaluate_use.scm 62 */
						long BgL_objzd2classzd2numz00_1763;

						BgL_objzd2classzd2numz00_1763 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_3));
						{	/* Eval/evaluate_use.scm 62 */
							obj_t BgL_arg1691z00_1764;

							BgL_arg1691z00_1764 =
								PROCEDURE_REF(BGl_usezd2envzd2zz__evaluate_usez00, (int) (1L));
							{	/* Eval/evaluate_use.scm 62 */
								int BgL_offsetz00_1767;

								BgL_offsetz00_1767 = (int) (BgL_objzd2classzd2numz00_1763);
								{	/* Eval/evaluate_use.scm 62 */
									long BgL_offsetz00_1768;

									BgL_offsetz00_1768 =
										((long) (BgL_offsetz00_1767) - OBJECT_TYPE);
									{	/* Eval/evaluate_use.scm 62 */
										long BgL_modz00_1769;

										BgL_modz00_1769 =
											(BgL_offsetz00_1768 >> (int) ((long) ((int) (4L))));
										{	/* Eval/evaluate_use.scm 62 */
											long BgL_restz00_1771;

											BgL_restz00_1771 =
												(BgL_offsetz00_1768 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Eval/evaluate_use.scm 62 */

												{	/* Eval/evaluate_use.scm 62 */
													obj_t BgL_bucketz00_1773;

													BgL_bucketz00_1773 =
														VECTOR_REF(
														((obj_t) BgL_arg1691z00_1764), BgL_modz00_1769);
													BgL_res1719z00_1792 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1773), BgL_restz00_1771);
					}}}}}}}}
					BgL_method1135z00_1178 = BgL_res1719z00_1792;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1135z00_1178,
					((obj_t) BgL_ez00_3), BgL_donez00_4);
			}
		}

	}



/* &use */
	obj_t BGl_z62usez62zz__evaluate_usez00(obj_t BgL_envz00_2195,
		obj_t BgL_ez00_2196, obj_t BgL_donez00_2197)
	{
		{	/* Eval/evaluate_use.scm 62 */
			{	/* Eval/evaluate_use.scm 62 */
				BgL_ev_exprz00_bglt BgL_auxz00_2436;

				{	/* Eval/evaluate_use.scm 62 */
					bool_t BgL_test1789z00_2437;

					{	/* Eval/evaluate_use.scm 62 */
						obj_t BgL_classz00_2276;

						BgL_classz00_2276 = BGl_ev_exprz00zz__evaluate_typesz00;
						if (BGL_OBJECTP(BgL_ez00_2196))
							{	/* Eval/evaluate_use.scm 62 */
								BgL_objectz00_bglt BgL_arg1688z00_2278;
								long BgL_arg1689z00_2279;

								BgL_arg1688z00_2278 = (BgL_objectz00_bglt) (BgL_ez00_2196);
								BgL_arg1689z00_2279 = BGL_CLASS_DEPTH(BgL_classz00_2276);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Eval/evaluate_use.scm 62 */
										long BgL_idxz00_2287;

										BgL_idxz00_2287 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1688z00_2278);
										{	/* Eval/evaluate_use.scm 62 */
											obj_t BgL_arg1670z00_2288;

											{	/* Eval/evaluate_use.scm 62 */
												long BgL_arg1675z00_2289;

												BgL_arg1675z00_2289 =
													(BgL_idxz00_2287 + BgL_arg1689z00_2279);
												BgL_arg1670z00_2288 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg1675z00_2289);
											}
											BgL_test1789z00_2437 =
												(BgL_arg1670z00_2288 == BgL_classz00_2276);
									}}
								else
									{	/* Eval/evaluate_use.scm 62 */
										bool_t BgL_res1760z00_2315;

										{	/* Eval/evaluate_use.scm 62 */
											obj_t BgL_oclassz00_2297;

											{	/* Eval/evaluate_use.scm 62 */
												obj_t BgL_arg1700z00_2305;
												long BgL_arg1701z00_2306;

												BgL_arg1700z00_2305 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Eval/evaluate_use.scm 62 */
													long BgL_arg1702z00_2307;

													BgL_arg1702z00_2307 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1688z00_2278);
													BgL_arg1701z00_2306 =
														(BgL_arg1702z00_2307 - OBJECT_TYPE);
												}
												BgL_oclassz00_2297 =
													VECTOR_REF(BgL_arg1700z00_2305, BgL_arg1701z00_2306);
											}
											{	/* Eval/evaluate_use.scm 62 */
												bool_t BgL__ortest_1118z00_2298;

												BgL__ortest_1118z00_2298 =
													(BgL_classz00_2276 == BgL_oclassz00_2297);
												if (BgL__ortest_1118z00_2298)
													{	/* Eval/evaluate_use.scm 62 */
														BgL_res1760z00_2315 = BgL__ortest_1118z00_2298;
													}
												else
													{	/* Eval/evaluate_use.scm 62 */
														long BgL_odepthz00_2299;

														{	/* Eval/evaluate_use.scm 62 */
															obj_t BgL_arg1681z00_2300;

															BgL_arg1681z00_2300 = (BgL_oclassz00_2297);
															BgL_odepthz00_2299 =
																BGL_CLASS_DEPTH(BgL_arg1681z00_2300);
														}
														if ((BgL_arg1689z00_2279 < BgL_odepthz00_2299))
															{	/* Eval/evaluate_use.scm 62 */
																obj_t BgL_arg1676z00_2302;

																{	/* Eval/evaluate_use.scm 62 */
																	obj_t BgL_arg1678z00_2303;

																	BgL_arg1678z00_2303 = (BgL_oclassz00_2297);
																	BgL_arg1676z00_2302 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1678z00_2303,
																		BgL_arg1689z00_2279);
																}
																BgL_res1760z00_2315 =
																	(BgL_arg1676z00_2302 == BgL_classz00_2276);
															}
														else
															{	/* Eval/evaluate_use.scm 62 */
																BgL_res1760z00_2315 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1789z00_2437 = BgL_res1760z00_2315;
									}
							}
						else
							{	/* Eval/evaluate_use.scm 62 */
								BgL_test1789z00_2437 = ((bool_t) 0);
							}
					}
					if (BgL_test1789z00_2437)
						{	/* Eval/evaluate_use.scm 62 */
							BgL_auxz00_2436 = ((BgL_ev_exprz00_bglt) BgL_ez00_2196);
						}
					else
						{
							obj_t BgL_auxz00_2462;

							BgL_auxz00_2462 =
								BGl_typezd2errorzd2zz__errorz00
								(BGl_string1737z00zz__evaluate_usez00, BINT(1786L),
								BGl_string1738z00zz__evaluate_usez00,
								BGl_string1739z00zz__evaluate_usez00, BgL_ez00_2196);
							FAILURE(BgL_auxz00_2462, BFALSE, BFALSE);
						}
				}
				return BGl_usez00zz__evaluate_usez00(BgL_auxz00_2436, BgL_donez00_2197);
			}
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_varz00zz__evaluate_typesz00, BGl_proc1740z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_globalz00zz__evaluate_typesz00,
				BGl_proc1742z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_littz00zz__evaluate_typesz00, BGl_proc1743z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00, BGl_ev_ifz00zz__evaluate_typesz00,
				BGl_proc1744z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_listz00zz__evaluate_typesz00, BGl_proc1745z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_prog2z00zz__evaluate_typesz00,
				BGl_proc1746z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_hookz00zz__evaluate_typesz00, BGl_proc1747z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_setlocalz00zz__evaluate_typesz00,
				BGl_proc1748z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_bindzd2exitzd2zz__evaluate_typesz00,
				BGl_proc1749z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_unwindzd2protectzd2zz__evaluate_typesz00,
				BGl_proc1750z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_withzd2handlerzd2zz__evaluate_typesz00,
				BGl_proc1751z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_synchroniza7eza7zz__evaluate_typesz00,
				BGl_proc1752z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_binderz00zz__evaluate_typesz00,
				BGl_proc1753z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_binderz00zz__evaluate_typesz00,
				BGl_proc1754z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_labelsz00zz__evaluate_typesz00,
				BGl_proc1755z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_gotoz00zz__evaluate_typesz00, BGl_proc1756z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_appz00zz__evaluate_typesz00, BGl_proc1757z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2envzd2zz__evaluate_usez00,
				BGl_ev_absz00zz__evaluate_typesz00, BGl_proc1758z00zz__evaluate_usez00,
				BGl_string1741z00zz__evaluate_usez00);
		}

	}



/* &use-ev_abs1171 */
	obj_t BGl_z62usezd2ev_abs1171zb0zz__evaluate_usez00(obj_t BgL_envz00_2216,
		obj_t BgL_ez00_2217, obj_t BgL_donez00_2218)
	{
		{	/* Eval/evaluate_use.scm 139 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_absz00_bglt) COBJECT(
							((BgL_ev_absz00_bglt) BgL_ez00_2217)))->BgL_bodyz00),
				BgL_donez00_2218);
		}

	}



/* &use-ev_app1169 */
	obj_t BGl_z62usezd2ev_app1169zb0zz__evaluate_usez00(obj_t BgL_envz00_2219,
		obj_t BgL_ez00_2220, obj_t BgL_donez00_2221)
	{
		{	/* Eval/evaluate_use.scm 135 */
			{	/* Eval/evaluate_use.scm 137 */
				BgL_ev_exprz00_bglt BgL_arg1346z00_2318;
				obj_t BgL_arg1347z00_2319;

				BgL_arg1346z00_2318 =
					(((BgL_ev_appz00_bglt) COBJECT(
							((BgL_ev_appz00_bglt) BgL_ez00_2220)))->BgL_funz00);
				{	/* Eval/evaluate_use.scm 137 */
					obj_t BgL_arg1348z00_2320;

					BgL_arg1348z00_2320 =
						(((BgL_ev_appz00_bglt) COBJECT(
								((BgL_ev_appz00_bglt) BgL_ez00_2220)))->BgL_argsz00);
					{
						obj_t BgL_lz00_2322;
						obj_t BgL_donez00_2323;

						BgL_lz00_2322 = BgL_arg1348z00_2320;
						BgL_donez00_2323 = BgL_donez00_2221;
					BgL_useza2za2_2321:
						if (NULLP(BgL_lz00_2322))
							{	/* Eval/evaluate_use.scm 65 */
								BgL_arg1347z00_2319 = BgL_donez00_2323;
							}
						else
							{	/* Eval/evaluate_use.scm 67 */
								obj_t BgL_arg1244z00_2324;
								obj_t BgL_arg1248z00_2325;

								BgL_arg1244z00_2324 = CDR(((obj_t) BgL_lz00_2322));
								{	/* Eval/evaluate_use.scm 67 */
									obj_t BgL_arg1249z00_2326;

									BgL_arg1249z00_2326 = CAR(((obj_t) BgL_lz00_2322));
									BgL_arg1248z00_2325 =
										BGl_usez00zz__evaluate_usez00(
										((BgL_ev_exprz00_bglt) BgL_arg1249z00_2326),
										BgL_donez00_2323);
								}
								{
									obj_t BgL_donez00_2501;
									obj_t BgL_lz00_2500;

									BgL_lz00_2500 = BgL_arg1244z00_2324;
									BgL_donez00_2501 = BgL_arg1248z00_2325;
									BgL_donez00_2323 = BgL_donez00_2501;
									BgL_lz00_2322 = BgL_lz00_2500;
									goto BgL_useza2za2_2321;
								}
							}
					}
				}
				return
					BGl_usez00zz__evaluate_usez00(BgL_arg1346z00_2318,
					BgL_arg1347z00_2319);
			}
		}

	}



/* &use-ev_goto1167 */
	obj_t BGl_z62usezd2ev_goto1167zb0zz__evaluate_usez00(obj_t BgL_envz00_2222,
		obj_t BgL_ez00_2223, obj_t BgL_donez00_2224)
	{
		{	/* Eval/evaluate_use.scm 131 */
			{	/* Eval/evaluate_use.scm 133 */
				obj_t BgL_arg1344z00_2328;

				BgL_arg1344z00_2328 =
					(((BgL_ev_gotoz00_bglt) COBJECT(
							((BgL_ev_gotoz00_bglt) BgL_ez00_2223)))->BgL_argsz00);
				{
					obj_t BgL_lz00_2330;
					obj_t BgL_donez00_2331;

					BgL_lz00_2330 = BgL_arg1344z00_2328;
					BgL_donez00_2331 = BgL_donez00_2224;
				BgL_useza2za2_2329:
					if (NULLP(BgL_lz00_2330))
						{	/* Eval/evaluate_use.scm 65 */
							return BgL_donez00_2331;
						}
					else
						{	/* Eval/evaluate_use.scm 67 */
							obj_t BgL_arg1244z00_2332;
							obj_t BgL_arg1248z00_2333;

							BgL_arg1244z00_2332 = CDR(((obj_t) BgL_lz00_2330));
							{	/* Eval/evaluate_use.scm 67 */
								obj_t BgL_arg1249z00_2334;

								BgL_arg1249z00_2334 = CAR(((obj_t) BgL_lz00_2330));
								BgL_arg1248z00_2333 =
									BGl_usez00zz__evaluate_usez00(
									((BgL_ev_exprz00_bglt) BgL_arg1249z00_2334),
									BgL_donez00_2331);
							}
							{
								obj_t BgL_donez00_2514;
								obj_t BgL_lz00_2513;

								BgL_lz00_2513 = BgL_arg1244z00_2332;
								BgL_donez00_2514 = BgL_arg1248z00_2333;
								BgL_donez00_2331 = BgL_donez00_2514;
								BgL_lz00_2330 = BgL_lz00_2513;
								goto BgL_useza2za2_2329;
							}
						}
				}
			}
		}

	}



/* &use-ev_labels1165 */
	obj_t BGl_z62usezd2ev_labels1165zb0zz__evaluate_usez00(obj_t BgL_envz00_2225,
		obj_t BgL_ez00_2226, obj_t BgL_donez00_2227)
	{
		{	/* Eval/evaluate_use.scm 124 */
			{	/* Eval/evaluate_use.scm 126 */
				obj_t BgL_arg1337z00_2336;

				BgL_arg1337z00_2336 =
					(((BgL_ev_labelsz00_bglt) COBJECT(
							((BgL_ev_labelsz00_bglt) BgL_ez00_2226)))->BgL_valsz00);
				{
					obj_t BgL_lz00_2338;
					obj_t BgL_donez00_2339;

					BgL_lz00_2338 = BgL_arg1337z00_2336;
					BgL_donez00_2339 = BgL_donez00_2227;
				BgL_recz00_2337:
					if (NULLP(BgL_lz00_2338))
						{	/* Eval/evaluate_use.scm 127 */
							return
								BGl_usez00zz__evaluate_usez00(
								(((BgL_ev_labelsz00_bglt) COBJECT(
											((BgL_ev_labelsz00_bglt) BgL_ez00_2226)))->BgL_bodyz00),
								BgL_donez00_2339);
						}
					else
						{	/* Eval/evaluate_use.scm 129 */
							obj_t BgL_arg1341z00_2340;
							obj_t BgL_arg1342z00_2341;

							BgL_arg1341z00_2340 = CDR(((obj_t) BgL_lz00_2338));
							{	/* Eval/evaluate_use.scm 129 */
								obj_t BgL_arg1343z00_2342;

								{	/* Eval/evaluate_use.scm 129 */
									obj_t BgL_pairz00_2343;

									BgL_pairz00_2343 = CAR(((obj_t) BgL_lz00_2338));
									BgL_arg1343z00_2342 = CDR(BgL_pairz00_2343);
								}
								BgL_arg1342z00_2341 =
									BGl_usez00zz__evaluate_usez00(
									((BgL_ev_exprz00_bglt) BgL_arg1343z00_2342),
									BgL_donez00_2339);
							}
							{
								obj_t BgL_donez00_2530;
								obj_t BgL_lz00_2529;

								BgL_lz00_2529 = BgL_arg1341z00_2340;
								BgL_donez00_2530 = BgL_arg1342z00_2341;
								BgL_donez00_2339 = BgL_donez00_2530;
								BgL_lz00_2338 = BgL_lz00_2529;
								goto BgL_recz00_2337;
							}
						}
				}
			}
		}

	}



/* &use-ev_binder1163 */
	obj_t BGl_z62usezd2ev_binder1163zb0zz__evaluate_usez00(obj_t BgL_envz00_2228,
		obj_t BgL_ez00_2229, obj_t BgL_donez00_2230)
	{
		{	/* Eval/evaluate_use.scm 120 */
			{	/* Eval/evaluate_use.scm 122 */
				BgL_ev_exprz00_bglt BgL_arg1334z00_2345;
				obj_t BgL_arg1335z00_2346;

				BgL_arg1334z00_2345 =
					(((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt) BgL_ez00_2229)))->BgL_bodyz00);
				{	/* Eval/evaluate_use.scm 122 */
					obj_t BgL_arg1336z00_2347;

					BgL_arg1336z00_2347 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_ez00_2229)))->BgL_valsz00);
					{
						obj_t BgL_lz00_2349;
						obj_t BgL_donez00_2350;

						BgL_lz00_2349 = BgL_arg1336z00_2347;
						BgL_donez00_2350 = BgL_donez00_2230;
					BgL_useza2za2_2348:
						if (NULLP(BgL_lz00_2349))
							{	/* Eval/evaluate_use.scm 65 */
								BgL_arg1335z00_2346 = BgL_donez00_2350;
							}
						else
							{	/* Eval/evaluate_use.scm 67 */
								obj_t BgL_arg1244z00_2351;
								obj_t BgL_arg1248z00_2352;

								BgL_arg1244z00_2351 = CDR(((obj_t) BgL_lz00_2349));
								{	/* Eval/evaluate_use.scm 67 */
									obj_t BgL_arg1249z00_2353;

									BgL_arg1249z00_2353 = CAR(((obj_t) BgL_lz00_2349));
									BgL_arg1248z00_2352 =
										BGl_usez00zz__evaluate_usez00(
										((BgL_ev_exprz00_bglt) BgL_arg1249z00_2353),
										BgL_donez00_2350);
								}
								{
									obj_t BgL_donez00_2544;
									obj_t BgL_lz00_2543;

									BgL_lz00_2543 = BgL_arg1244z00_2351;
									BgL_donez00_2544 = BgL_arg1248z00_2352;
									BgL_donez00_2350 = BgL_donez00_2544;
									BgL_lz00_2349 = BgL_lz00_2543;
									goto BgL_useza2za2_2348;
								}
							}
					}
				}
				return
					BGl_usez00zz__evaluate_usez00(BgL_arg1334z00_2345,
					BgL_arg1335z00_2346);
			}
		}

	}



/* &use-ev_binder1161 */
	obj_t BGl_z62usezd2ev_binder1161zb0zz__evaluate_usez00(obj_t BgL_envz00_2231,
		obj_t BgL_ez00_2232, obj_t BgL_donez00_2233)
	{
		{	/* Eval/evaluate_use.scm 116 */
			{	/* Eval/evaluate_use.scm 118 */
				BgL_ev_exprz00_bglt BgL_arg1331z00_2355;
				obj_t BgL_arg1332z00_2356;

				BgL_arg1331z00_2355 =
					(((BgL_ev_binderz00_bglt) COBJECT(
							((BgL_ev_binderz00_bglt) BgL_ez00_2232)))->BgL_bodyz00);
				{	/* Eval/evaluate_use.scm 118 */
					obj_t BgL_arg1333z00_2357;

					BgL_arg1333z00_2357 =
						(((BgL_ev_binderz00_bglt) COBJECT(
								((BgL_ev_binderz00_bglt) BgL_ez00_2232)))->BgL_valsz00);
					{
						obj_t BgL_lz00_2359;
						obj_t BgL_donez00_2360;

						BgL_lz00_2359 = BgL_arg1333z00_2357;
						BgL_donez00_2360 = BgL_donez00_2233;
					BgL_useza2za2_2358:
						if (NULLP(BgL_lz00_2359))
							{	/* Eval/evaluate_use.scm 65 */
								BgL_arg1332z00_2356 = BgL_donez00_2360;
							}
						else
							{	/* Eval/evaluate_use.scm 67 */
								obj_t BgL_arg1244z00_2361;
								obj_t BgL_arg1248z00_2362;

								BgL_arg1244z00_2361 = CDR(((obj_t) BgL_lz00_2359));
								{	/* Eval/evaluate_use.scm 67 */
									obj_t BgL_arg1249z00_2363;

									BgL_arg1249z00_2363 = CAR(((obj_t) BgL_lz00_2359));
									BgL_arg1248z00_2362 =
										BGl_usez00zz__evaluate_usez00(
										((BgL_ev_exprz00_bglt) BgL_arg1249z00_2363),
										BgL_donez00_2360);
								}
								{
									obj_t BgL_donez00_2559;
									obj_t BgL_lz00_2558;

									BgL_lz00_2558 = BgL_arg1244z00_2361;
									BgL_donez00_2559 = BgL_arg1248z00_2362;
									BgL_donez00_2360 = BgL_donez00_2559;
									BgL_lz00_2359 = BgL_lz00_2558;
									goto BgL_useza2za2_2358;
								}
							}
					}
				}
				return
					BGl_usez00zz__evaluate_usez00(BgL_arg1331z00_2355,
					BgL_arg1332z00_2356);
			}
		}

	}



/* &use-ev_synchronize1159 */
	obj_t BGl_z62usezd2ev_synchroniza7e1159z17zz__evaluate_usez00(obj_t
		BgL_envz00_2234, obj_t BgL_ez00_2235, obj_t BgL_donez00_2236)
	{
		{	/* Eval/evaluate_use.scm 112 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
							((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2235)))->BgL_mutexz00),
				BGl_usez00zz__evaluate_usez00(
					(((BgL_ev_synchroniza7eza7_bglt) COBJECT(
								((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2235)))->
						BgL_prelockz00),
					BGl_usez00zz__evaluate_usez00((((BgL_ev_synchroniza7eza7_bglt)
								COBJECT(((BgL_ev_synchroniza7eza7_bglt) BgL_ez00_2235)))->
							BgL_bodyz00), BgL_donez00_2236)));
		}

	}



/* &use-ev_with-handler1157 */
	obj_t BGl_z62usezd2ev_withzd2handler1157z62zz__evaluate_usez00(obj_t
		BgL_envz00_2237, obj_t BgL_ez00_2238, obj_t BgL_donez00_2239)
	{
		{	/* Eval/evaluate_use.scm 108 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_withzd2handlerzd2_bglt) COBJECT(
							((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_2238)))->
					BgL_handlerz00),
				BGl_usez00zz__evaluate_usez00((((BgL_ev_withzd2handlerzd2_bglt)
							COBJECT(((BgL_ev_withzd2handlerzd2_bglt) BgL_ez00_2238)))->
						BgL_bodyz00), BgL_donez00_2239));
		}

	}



/* &use-ev_unwind-protec1155 */
	obj_t BGl_z62usezd2ev_unwindzd2protec1155z62zz__evaluate_usez00(obj_t
		BgL_envz00_2240, obj_t BgL_ez00_2241, obj_t BgL_donez00_2242)
	{
		{	/* Eval/evaluate_use.scm 104 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
							((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_2241)))->BgL_ez00),
				BGl_usez00zz__evaluate_usez00(
					(((BgL_ev_unwindzd2protectzd2_bglt) COBJECT(
								((BgL_ev_unwindzd2protectzd2_bglt) BgL_ez00_2241)))->
						BgL_bodyz00), BgL_donez00_2242));
		}

	}



/* &use-ev_bind-exit1153 */
	obj_t BGl_z62usezd2ev_bindzd2exit1153z62zz__evaluate_usez00(obj_t
		BgL_envz00_2243, obj_t BgL_ez00_2244, obj_t BgL_donez00_2245)
	{
		{	/* Eval/evaluate_use.scm 100 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_bindzd2exitzd2_bglt) COBJECT(
							((BgL_ev_bindzd2exitzd2_bglt) BgL_ez00_2244)))->BgL_bodyz00),
				BgL_donez00_2245);
		}

	}



/* &use-ev_setlocal1151 */
	obj_t BGl_z62usezd2ev_setlocal1151zb0zz__evaluate_usez00(obj_t
		BgL_envz00_2246, obj_t BgL_ez00_2247, obj_t BgL_donez00_2248)
	{
		{	/* Eval/evaluate_use.scm 95 */
			{	/* Eval/evaluate_use.scm 96 */
				obj_t BgL_donez00_2369;

				BgL_donez00_2369 = BgL_donez00_2248;
				{	/* Eval/evaluate_use.scm 97 */
					bool_t BgL_test1799z00_2585;

					{	/* Eval/evaluate_use.scm 97 */
						BgL_ev_varz00_bglt BgL_arg1315z00_2370;

						BgL_arg1315z00_2370 =
							(((BgL_ev_setlocalz00_bglt) COBJECT(
									((BgL_ev_setlocalz00_bglt) BgL_ez00_2247)))->BgL_vz00);
						BgL_test1799z00_2585 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
								((obj_t) BgL_arg1315z00_2370), BgL_donez00_2369));
					}
					if (BgL_test1799z00_2585)
						{	/* Eval/evaluate_use.scm 97 */
							BFALSE;
						}
					else
						{	/* Eval/evaluate_use.scm 97 */
							BgL_ev_varz00_bglt BgL_arg1314z00_2371;

							BgL_arg1314z00_2371 =
								(((BgL_ev_setlocalz00_bglt) COBJECT(
										((BgL_ev_setlocalz00_bglt) BgL_ez00_2247)))->BgL_vz00);
							BgL_donez00_2369 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg1314z00_2371), BgL_donez00_2369);
						}
				}
				{	/* Eval/evaluate_use.scm 98 */
					BgL_ev_exprz00_bglt BgL_arg1316z00_2372;

					BgL_arg1316z00_2372 =
						(((BgL_ev_hookz00_bglt) COBJECT(
								((BgL_ev_hookz00_bglt)
									((BgL_ev_setlocalz00_bglt) BgL_ez00_2247))))->BgL_ez00);
					return
						BGl_usez00zz__evaluate_usez00(BgL_arg1316z00_2372,
						BgL_donez00_2369);
				}
			}
		}

	}



/* &use-ev_hook1149 */
	obj_t BGl_z62usezd2ev_hook1149zb0zz__evaluate_usez00(obj_t BgL_envz00_2249,
		obj_t BgL_ez00_2250, obj_t BgL_donez00_2251)
	{
		{	/* Eval/evaluate_use.scm 91 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_hookz00_bglt) COBJECT(
							((BgL_ev_hookz00_bglt) BgL_ez00_2250)))->BgL_ez00),
				BgL_donez00_2251);
		}

	}



/* &use-ev_prog21147 */
	obj_t BGl_z62usezd2ev_prog21147zb0zz__evaluate_usez00(obj_t BgL_envz00_2252,
		obj_t BgL_ez00_2253, obj_t BgL_donez00_2254)
	{
		{	/* Eval/evaluate_use.scm 87 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_prog2z00_bglt) COBJECT(
							((BgL_ev_prog2z00_bglt) BgL_ez00_2253)))->BgL_e1z00),
				BGl_usez00zz__evaluate_usez00(
					(((BgL_ev_prog2z00_bglt) COBJECT(
								((BgL_ev_prog2z00_bglt) BgL_ez00_2253)))->BgL_e2z00),
					BgL_donez00_2254));
		}

	}



/* &use-ev_list1145 */
	obj_t BGl_z62usezd2ev_list1145zb0zz__evaluate_usez00(obj_t BgL_envz00_2255,
		obj_t BgL_ez00_2256, obj_t BgL_donez00_2257)
	{
		{	/* Eval/evaluate_use.scm 83 */
			{	/* Eval/evaluate_use.scm 85 */
				obj_t BgL_arg1306z00_2376;

				BgL_arg1306z00_2376 =
					(((BgL_ev_listz00_bglt) COBJECT(
							((BgL_ev_listz00_bglt) BgL_ez00_2256)))->BgL_argsz00);
				{
					obj_t BgL_lz00_2378;
					obj_t BgL_donez00_2379;

					BgL_lz00_2378 = BgL_arg1306z00_2376;
					BgL_donez00_2379 = BgL_donez00_2257;
				BgL_useza2za2_2377:
					if (NULLP(BgL_lz00_2378))
						{	/* Eval/evaluate_use.scm 65 */
							return BgL_donez00_2379;
						}
					else
						{	/* Eval/evaluate_use.scm 67 */
							obj_t BgL_arg1244z00_2380;
							obj_t BgL_arg1248z00_2381;

							BgL_arg1244z00_2380 = CDR(((obj_t) BgL_lz00_2378));
							{	/* Eval/evaluate_use.scm 67 */
								obj_t BgL_arg1249z00_2382;

								BgL_arg1249z00_2382 = CAR(((obj_t) BgL_lz00_2378));
								BgL_arg1248z00_2381 =
									BGl_usez00zz__evaluate_usez00(
									((BgL_ev_exprz00_bglt) BgL_arg1249z00_2382),
									BgL_donez00_2379);
							}
							{
								obj_t BgL_donez00_2619;
								obj_t BgL_lz00_2618;

								BgL_lz00_2618 = BgL_arg1244z00_2380;
								BgL_donez00_2619 = BgL_arg1248z00_2381;
								BgL_donez00_2379 = BgL_donez00_2619;
								BgL_lz00_2378 = BgL_lz00_2618;
								goto BgL_useza2za2_2377;
							}
						}
				}
			}
		}

	}



/* &use-ev_if1143 */
	obj_t BGl_z62usezd2ev_if1143zb0zz__evaluate_usez00(obj_t BgL_envz00_2258,
		obj_t BgL_ez00_2259, obj_t BgL_donez00_2260)
	{
		{	/* Eval/evaluate_use.scm 79 */
			return
				BGl_usez00zz__evaluate_usez00(
				(((BgL_ev_ifz00_bglt) COBJECT(
							((BgL_ev_ifz00_bglt) BgL_ez00_2259)))->BgL_pz00),
				BGl_usez00zz__evaluate_usez00(
					(((BgL_ev_ifz00_bglt) COBJECT(
								((BgL_ev_ifz00_bglt) BgL_ez00_2259)))->BgL_tz00),
					BGl_usez00zz__evaluate_usez00(
						(((BgL_ev_ifz00_bglt) COBJECT(
									((BgL_ev_ifz00_bglt) BgL_ez00_2259)))->BgL_ez00),
						BgL_donez00_2260)));
		}

	}



/* &use-ev_litt1141 */
	obj_t BGl_z62usezd2ev_litt1141zb0zz__evaluate_usez00(obj_t BgL_envz00_2261,
		obj_t BgL_ez00_2262, obj_t BgL_donez00_2263)
	{
		{	/* Eval/evaluate_use.scm 76 */
			return BgL_donez00_2263;
		}

	}



/* &use-ev_global1139 */
	obj_t BGl_z62usezd2ev_global1139zb0zz__evaluate_usez00(obj_t BgL_envz00_2264,
		obj_t BgL_varz00_2265, obj_t BgL_donez00_2266)
	{
		{	/* Eval/evaluate_use.scm 73 */
			return BgL_donez00_2266;
		}

	}



/* &use-ev_var1137 */
	obj_t BGl_z62usezd2ev_var1137zb0zz__evaluate_usez00(obj_t BgL_envz00_2267,
		obj_t BgL_varz00_2268, obj_t BgL_donez00_2269)
	{
		{	/* Eval/evaluate_use.scm 69 */
			{	/* Eval/evaluate_use.scm 70 */
				obj_t BgL_donez00_2387;

				BgL_donez00_2387 = BgL_donez00_2269;
				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							((obj_t)
								((BgL_ev_varz00_bglt) BgL_varz00_2268)), BgL_donez00_2387)))
					{	/* Eval/evaluate_use.scm 70 */
						BFALSE;
					}
				else
					{	/* Eval/evaluate_use.scm 70 */
						BgL_donez00_2387 =
							MAKE_YOUNG_PAIR(
							((obj_t)
								((BgL_ev_varz00_bglt) BgL_varz00_2268)), BgL_donez00_2387);
					}
				return BgL_donez00_2387;
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__evaluate_usez00(void)
	{
		{	/* Eval/evaluate_use.scm 15 */
			BGl_modulezd2initializa7ationz75zz__typez00(393254000L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__osz00(310000171L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__dssslz00(443936801L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__bitz00(377164741L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__ppz00(233942021L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__readerz00(220648206L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__evenvz00(528345299L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__evcompilez00(492754569L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__everrorz00(375872221L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			BGl_modulezd2initializa7ationz75zz__evmodulez00(505897955L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
			return BGl_modulezd2initializa7ationz75zz__evaluate_typesz00(0L,
				BSTRING_TO_STRING(BGl_string1759z00zz__evaluate_usez00));
		}

	}

#ifdef __cplusplus
}
#endif
