/*===========================================================================*/
/*   (Eval/expdtrace.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdtrace.scm -indent -o objs/obj_u/Eval/expdtrace.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_TRACE_TYPE_DEFINITIONS
#define BGL___EXPANDER_TRACE_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_TRACE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_makezd2expandzd2withzd2tracezd2zz__expander_tracez00(obj_t);
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_tracez00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2expandzd2tracezd2itemzd2zz__expander_tracez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2expandzd2whenzd2tracezd2zz__expander_tracez00(obj_t);
	extern int bgl_debug(void);
	static obj_t BGl_cnstzd2initzd2zz__expander_tracez00(void);
	extern int BGl_bigloozd2profilezd2zz__paramz00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_tracez00(void);
	static obj_t BGl_z62zc3z04anonymousza31311ze3ze5zz__expander_tracez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_tracez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_tracez00(void);
	static obj_t BGl_objectzd2initzd2zz__expander_tracez00(void);
	static obj_t BGl_z62zc3z04anonymousza31207ze3ze5zz__expander_tracez00(obj_t,
		obj_t, obj_t);
	extern int BGl_bigloozd2compilerzd2debugz00zz__paramz00(void);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t
		BGl_z62makezd2expandzd2withzd2tracezb0zz__expander_tracez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_tracez00(void);
	static obj_t BGl_symbol1709z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1711z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1713z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1715z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1720z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1722z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1724z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1726z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1728z00zz__expander_tracez00 = BUNSPEC;
	static obj_t
		BGl_z62makezd2expandzd2tracezd2itemzb0zz__expander_tracez00(obj_t, obj_t);
	static obj_t
		BGl_z62makezd2expandzd2whenzd2tracezb0zz__expander_tracez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31172ze3ze5zz__expander_tracez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol1730z00zz__expander_tracez00 = BUNSPEC;
	static obj_t BGl_symbol1734z00zz__expander_tracez00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1706z00zz__expander_tracez00,
		BgL_bgl_string1706za700za7za7_1737za7,
		"/tmp/bigloo/runtime/Eval/expdtrace.scm", 38);
	      DEFINE_STRING(BGl_string1707z00zz__expander_tracez00,
		BgL_bgl_string1707za700za7za7_1738za7, "&make-expand-when-trace", 23);
	      DEFINE_STRING(BGl_string1708z00zz__expander_tracez00,
		BgL_bgl_string1708za700za7za7_1739za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1710z00zz__expander_tracez00,
		BgL_bgl_string1710za700za7za7_1740za7, "compiler", 8);
	      DEFINE_STRING(BGl_string1712z00zz__expander_tracez00,
		BgL_bgl_string1712za700za7za7_1741za7, "trace-active?", 13);
	      DEFINE_STRING(BGl_string1714z00zz__expander_tracez00,
		BgL_bgl_string1714za700za7za7_1742za7, "begin", 5);
	      DEFINE_STRING(BGl_string1716z00zz__expander_tracez00,
		BgL_bgl_string1716za700za7za7_1743za7, "if", 2);
	      DEFINE_STRING(BGl_string1717z00zz__expander_tracez00,
		BgL_bgl_string1717za700za7za7_1744za7, "when-trace", 10);
	      DEFINE_STRING(BGl_string1718z00zz__expander_tracez00,
		BgL_bgl_string1718za700za7za7_1745za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1719z00zz__expander_tracez00,
		BgL_bgl_string1719za700za7za7_1746za7, "&make-expand-with-trace", 23);
	      DEFINE_STRING(BGl_string1721z00zz__expander_tracez00,
		BgL_bgl_string1721za700za7za7_1747za7, "f", 1);
	      DEFINE_STRING(BGl_string1723z00zz__expander_tracez00,
		BgL_bgl_string1723za700za7za7_1748za7, "lambda", 6);
	      DEFINE_STRING(BGl_string1725z00zz__expander_tracez00,
		BgL_bgl_string1725za700za7za7_1749za7, "bigloo-debug", 12);
	      DEFINE_STRING(BGl_string1727z00zz__expander_tracez00,
		BgL_bgl_string1727za700za7za7_1750za7, ">fx", 3);
	      DEFINE_STRING(BGl_string1729z00zz__expander_tracez00,
		BgL_bgl_string1729za700za7za7_1751za7, "%with-trace", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2expandzd2whenzd2tracezd2envz00zz__expander_tracez00,
		BgL_bgl_za762makeza7d2expand1752z00,
		BGl_z62makezd2expandzd2whenzd2tracezb0zz__expander_tracez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1731z00zz__expander_tracez00,
		BgL_bgl_string1731za700za7za7_1753za7, "let", 3);
	      DEFINE_STRING(BGl_string1732z00zz__expander_tracez00,
		BgL_bgl_string1732za700za7za7_1754za7, "with-trace", 10);
	      DEFINE_STRING(BGl_string1733z00zz__expander_tracez00,
		BgL_bgl_string1733za700za7za7_1755za7, "&make-expand-trace-item", 23);
	      DEFINE_STRING(BGl_string1735z00zz__expander_tracez00,
		BgL_bgl_string1735za700za7za7_1756za7, "trace-item", 10);
	      DEFINE_STRING(BGl_string1736z00zz__expander_tracez00,
		BgL_bgl_string1736za700za7za7_1757za7, "__expander_trace", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2expandzd2tracezd2itemzd2envz00zz__expander_tracez00,
		BgL_bgl_za762makeza7d2expand1758z00,
		BGl_z62makezd2expandzd2tracezd2itemzb0zz__expander_tracez00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2expandzd2withzd2tracezd2envz00zz__expander_tracez00,
		BgL_bgl_za762makeza7d2expand1759z00,
		BGl_z62makezd2expandzd2withzd2tracezb0zz__expander_tracez00, 0L, BUNSPEC,
		1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1709z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1711z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1713z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1715z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1720z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1722z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1724z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1726z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1728z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1730z00zz__expander_tracez00));
		     ADD_ROOT((void *) (&BGl_symbol1734z00zz__expander_tracez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_tracez00(long
		BgL_checksumz00_1994, char *BgL_fromz00_1995)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_tracez00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_tracez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_tracez00();
					BGl_cnstzd2initzd2zz__expander_tracez00();
					BGl_importedzd2moduleszd2initz00zz__expander_tracez00();
					return BGl_methodzd2initzd2zz__expander_tracez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_tracez00(void)
	{
		{	/* Eval/expdtrace.scm 14 */
			BGl_symbol1709z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1710z00zz__expander_tracez00);
			BGl_symbol1711z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1712z00zz__expander_tracez00);
			BGl_symbol1713z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1714z00zz__expander_tracez00);
			BGl_symbol1715z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1716z00zz__expander_tracez00);
			BGl_symbol1720z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1721z00zz__expander_tracez00);
			BGl_symbol1722z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1723z00zz__expander_tracez00);
			BGl_symbol1724z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1725z00zz__expander_tracez00);
			BGl_symbol1726z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1727z00zz__expander_tracez00);
			BGl_symbol1728z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1729z00zz__expander_tracez00);
			BGl_symbol1730z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1731z00zz__expander_tracez00);
			return (BGl_symbol1734z00zz__expander_tracez00 =
				bstring_to_symbol(BGl_string1735z00zz__expander_tracez00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_tracez00(void)
	{
		{	/* Eval/expdtrace.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* make-expand-when-trace */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2expandzd2whenzd2tracezd2zz__expander_tracez00(obj_t
		BgL_modez00_3)
	{
		{	/* Eval/expdtrace.scm 56 */
			{	/* Eval/expdtrace.scm 57 */
				obj_t BgL_zc3z04anonymousza31172ze3z87_1883;

				BgL_zc3z04anonymousza31172ze3z87_1883 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31172ze3ze5zz__expander_tracez00, (int) (2L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31172ze3z87_1883, (int) (0L),
					BgL_modez00_3);
				return BgL_zc3z04anonymousza31172ze3z87_1883;
			}
		}

	}



/* &make-expand-when-trace */
	obj_t BGl_z62makezd2expandzd2whenzd2tracezb0zz__expander_tracez00(obj_t
		BgL_envz00_1884, obj_t BgL_modez00_1885)
	{
		{	/* Eval/expdtrace.scm 56 */
			{	/* Eval/expdtrace.scm 57 */
				obj_t BgL_auxz00_2020;

				if (SYMBOLP(BgL_modez00_1885))
					{	/* Eval/expdtrace.scm 57 */
						BgL_auxz00_2020 = BgL_modez00_1885;
					}
				else
					{
						obj_t BgL_auxz00_2023;

						BgL_auxz00_2023 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1706z00zz__expander_tracez00, BINT(1944L),
							BGl_string1707z00zz__expander_tracez00,
							BGl_string1708z00zz__expander_tracez00, BgL_modez00_1885);
						FAILURE(BgL_auxz00_2023, BFALSE, BFALSE);
					}
				return
					BGl_makezd2expandzd2whenzd2tracezd2zz__expander_tracez00
					(BgL_auxz00_2020);
			}
		}

	}



/* &<@anonymous:1172> */
	obj_t BGl_z62zc3z04anonymousza31172ze3ze5zz__expander_tracez00(obj_t
		BgL_envz00_1886, obj_t BgL_xz00_1888, obj_t BgL_ez00_1889)
	{
		{	/* Eval/expdtrace.scm 57 */
			{	/* Eval/expdtrace.scm 57 */
				obj_t BgL_modez00_1887;

				BgL_modez00_1887 = ((obj_t) PROCEDURE_REF(BgL_envz00_1886, (int) (0L)));
				{
					obj_t BgL_levelz00_1922;
					obj_t BgL_expz00_1923;

					if (PAIRP(BgL_xz00_1888))
						{	/* Eval/expdtrace.scm 57 */
							obj_t BgL_cdrzd2109zd2_1935;

							BgL_cdrzd2109zd2_1935 = CDR(((obj_t) BgL_xz00_1888));
							if (PAIRP(BgL_cdrzd2109zd2_1935))
								{	/* Eval/expdtrace.scm 57 */
									BgL_levelz00_1922 = CAR(BgL_cdrzd2109zd2_1935);
									BgL_expz00_1923 = CDR(BgL_cdrzd2109zd2_1935);
									{	/* Eval/expdtrace.scm 60 */
										bool_t BgL_test1764z00_2037;

										{	/* Eval/expdtrace.scm 60 */
											bool_t BgL_test1765z00_2038;

											{	/* Eval/expdtrace.scm 60 */
												int BgL_arg1206z00_1924;

												BgL_arg1206z00_1924 =
													BGl_bigloozd2profilezd2zz__paramz00();
												BgL_test1765z00_2038 =
													((long) (BgL_arg1206z00_1924) == 0L);
											}
											if (BgL_test1765z00_2038)
												{	/* Eval/expdtrace.scm 60 */
													if (
														(BgL_modez00_1887 ==
															BGl_symbol1709z00zz__expander_tracez00))
														{	/* Eval/expdtrace.scm 62 */
															int BgL_arg1202z00_1925;

															BgL_arg1202z00_1925 =
																BGl_bigloozd2compilerzd2debugz00zz__paramz00();
															BgL_test1764z00_2037 =
																((long) (BgL_arg1202z00_1925) > 0L);
														}
													else
														{	/* Eval/expdtrace.scm 63 */
															int BgL_arg1203z00_1926;

															BgL_arg1203z00_1926 = bgl_debug();
															BgL_test1764z00_2037 =
																((long) (BgL_arg1203z00_1926) > 0L);
												}}
											else
												{	/* Eval/expdtrace.scm 60 */
													BgL_test1764z00_2037 = ((bool_t) 0);
												}
										}
										if (BgL_test1764z00_2037)
											{	/* Eval/expdtrace.scm 65 */
												obj_t BgL_arg1193z00_1927;

												{	/* Eval/expdtrace.scm 65 */
													obj_t BgL_arg1194z00_1928;

													{	/* Eval/expdtrace.scm 65 */
														obj_t BgL_arg1196z00_1929;
														obj_t BgL_arg1197z00_1930;

														{	/* Eval/expdtrace.scm 65 */
															obj_t BgL_arg1198z00_1931;

															BgL_arg1198z00_1931 =
																MAKE_YOUNG_PAIR(BgL_levelz00_1922, BNIL);
															BgL_arg1196z00_1929 =
																MAKE_YOUNG_PAIR
																(BGl_symbol1711z00zz__expander_tracez00,
																BgL_arg1198z00_1931);
														}
														{	/* Eval/expdtrace.scm 66 */
															obj_t BgL_arg1199z00_1932;
															obj_t BgL_arg1200z00_1933;

															{	/* Eval/expdtrace.scm 66 */
																obj_t BgL_arg1201z00_1934;

																BgL_arg1201z00_1934 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_expz00_1923, BNIL);
																BgL_arg1199z00_1932 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1713z00zz__expander_tracez00,
																	BgL_arg1201z00_1934);
															}
															BgL_arg1200z00_1933 =
																MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
															BgL_arg1197z00_1930 =
																MAKE_YOUNG_PAIR(BgL_arg1199z00_1932,
																BgL_arg1200z00_1933);
														}
														BgL_arg1194z00_1928 =
															MAKE_YOUNG_PAIR(BgL_arg1196z00_1929,
															BgL_arg1197z00_1930);
													}
													BgL_arg1193z00_1927 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1715z00zz__expander_tracez00,
														BgL_arg1194z00_1928);
												}
												return
													BGL_PROCEDURE_CALL2(BgL_ez00_1889,
													BgL_arg1193z00_1927, BgL_ez00_1889);
											}
										else
											{	/* Eval/expdtrace.scm 60 */
												return BUNSPEC;
											}
									}
								}
							else
								{	/* Eval/expdtrace.scm 57 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string1717z00zz__expander_tracez00,
										BGl_string1718z00zz__expander_tracez00, BgL_xz00_1888);
								}
						}
					else
						{	/* Eval/expdtrace.scm 57 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string1717z00zz__expander_tracez00,
								BGl_string1718z00zz__expander_tracez00, BgL_xz00_1888);
						}
				}
			}
		}

	}



/* make-expand-with-trace */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2expandzd2withzd2tracezd2zz__expander_tracez00(obj_t
		BgL_modez00_4)
	{
		{	/* Eval/expdtrace.scm 76 */
			{	/* Eval/expdtrace.scm 77 */
				obj_t BgL_zc3z04anonymousza31207ze3z87_1890;

				BgL_zc3z04anonymousza31207ze3z87_1890 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31207ze3ze5zz__expander_tracez00, (int) (2L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31207ze3z87_1890, (int) (0L),
					BgL_modez00_4);
				return BgL_zc3z04anonymousza31207ze3z87_1890;
			}
		}

	}



/* &make-expand-with-trace */
	obj_t BGl_z62makezd2expandzd2withzd2tracezb0zz__expander_tracez00(obj_t
		BgL_envz00_1891, obj_t BgL_modez00_1892)
	{
		{	/* Eval/expdtrace.scm 76 */
			{	/* Eval/expdtrace.scm 77 */
				obj_t BgL_auxz00_2072;

				if (SYMBOLP(BgL_modez00_1892))
					{	/* Eval/expdtrace.scm 77 */
						BgL_auxz00_2072 = BgL_modez00_1892;
					}
				else
					{
						obj_t BgL_auxz00_2075;

						BgL_auxz00_2075 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1706z00zz__expander_tracez00, BINT(2579L),
							BGl_string1719z00zz__expander_tracez00,
							BGl_string1708z00zz__expander_tracez00, BgL_modez00_1892);
						FAILURE(BgL_auxz00_2075, BFALSE, BFALSE);
					}
				return
					BGl_makezd2expandzd2withzd2tracezd2zz__expander_tracez00
					(BgL_auxz00_2072);
			}
		}

	}



/* &<@anonymous:1207> */
	obj_t BGl_z62zc3z04anonymousza31207ze3ze5zz__expander_tracez00(obj_t
		BgL_envz00_1893, obj_t BgL_xz00_1895, obj_t BgL_ez00_1896)
	{
		{	/* Eval/expdtrace.scm 77 */
			{	/* Eval/expdtrace.scm 77 */
				obj_t BgL_modez00_1894;

				BgL_modez00_1894 = ((obj_t) PROCEDURE_REF(BgL_envz00_1893, (int) (0L)));
				{
					obj_t BgL_levelz00_1937;
					obj_t BgL_lblz00_1938;
					obj_t BgL_argza2za2_1939;

					if (PAIRP(BgL_xz00_1895))
						{	/* Eval/expdtrace.scm 77 */
							obj_t BgL_cdrzd2126zd2_1970;

							BgL_cdrzd2126zd2_1970 = CDR(((obj_t) BgL_xz00_1895));
							if (PAIRP(BgL_cdrzd2126zd2_1970))
								{	/* Eval/expdtrace.scm 77 */
									obj_t BgL_cdrzd2131zd2_1971;

									BgL_cdrzd2131zd2_1971 = CDR(BgL_cdrzd2126zd2_1970);
									if (PAIRP(BgL_cdrzd2131zd2_1971))
										{	/* Eval/expdtrace.scm 77 */
											BgL_levelz00_1937 = CAR(BgL_cdrzd2126zd2_1970);
											BgL_lblz00_1938 = CAR(BgL_cdrzd2131zd2_1971);
											BgL_argza2za2_1939 = CDR(BgL_cdrzd2131zd2_1971);
											{	/* Eval/expdtrace.scm 80 */
												bool_t BgL_test1771z00_2092;

												{	/* Eval/expdtrace.scm 80 */
													bool_t BgL_test1772z00_2093;

													{	/* Eval/expdtrace.scm 80 */
														int BgL_arg1310z00_1940;

														BgL_arg1310z00_1940 =
															BGl_bigloozd2profilezd2zz__paramz00();
														BgL_test1772z00_2093 =
															((long) (BgL_arg1310z00_1940) == 0L);
													}
													if (BgL_test1772z00_2093)
														{	/* Eval/expdtrace.scm 80 */
															if (
																(BgL_modez00_1894 ==
																	BGl_symbol1709z00zz__expander_tracez00))
																{	/* Eval/expdtrace.scm 82 */
																	int BgL_arg1308z00_1941;

																	BgL_arg1308z00_1941 =
																		BGl_bigloozd2compilerzd2debugz00zz__paramz00
																		();
																	BgL_test1771z00_2092 =
																		((long) (BgL_arg1308z00_1941) > 0L);
																}
															else
																{	/* Eval/expdtrace.scm 83 */
																	int BgL_arg1309z00_1942;

																	BgL_arg1309z00_1942 = bgl_debug();
																	BgL_test1771z00_2092 =
																		((long) (BgL_arg1309z00_1942) > 0L);
														}}
													else
														{	/* Eval/expdtrace.scm 80 */
															BgL_test1771z00_2092 = ((bool_t) 0);
														}
												}
												if (BgL_test1771z00_2092)
													{	/* Eval/expdtrace.scm 85 */
														obj_t BgL_fz00_1943;

														BgL_fz00_1943 =
															BGl_gensymz00zz__r4_symbols_6_4z00
															(BGl_symbol1720z00zz__expander_tracez00);
														{	/* Eval/expdtrace.scm 85 */
															obj_t BgL_nxz00_1944;

															{	/* Eval/expdtrace.scm 86 */
																obj_t BgL_arg1225z00_1945;

																{	/* Eval/expdtrace.scm 86 */
																	obj_t BgL_arg1226z00_1946;
																	obj_t BgL_arg1227z00_1947;

																	{	/* Eval/expdtrace.scm 86 */
																		obj_t BgL_arg1228z00_1948;

																		{	/* Eval/expdtrace.scm 86 */
																			obj_t BgL_arg1229z00_1949;

																			{	/* Eval/expdtrace.scm 86 */
																				obj_t BgL_arg1230z00_1950;

																				{	/* Eval/expdtrace.scm 86 */
																					obj_t BgL_arg1231z00_1951;

																					{	/* Eval/expdtrace.scm 86 */
																						obj_t BgL_arg1232z00_1952;

																						{	/* Eval/expdtrace.scm 86 */
																							obj_t BgL_arg1233z00_1953;

																							{	/* Eval/expdtrace.scm 86 */
																								obj_t BgL_arg1234z00_1954;

																								BgL_arg1234z00_1954 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_argza2za2_1939, BNIL);
																								BgL_arg1233z00_1953 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1713z00zz__expander_tracez00,
																									BgL_arg1234z00_1954);
																							}
																							BgL_arg1232z00_1952 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1233z00_1953, BNIL);
																						}
																						BgL_arg1231z00_1951 =
																							MAKE_YOUNG_PAIR(BNIL,
																							BgL_arg1232z00_1952);
																					}
																					BgL_arg1230z00_1950 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol1722z00zz__expander_tracez00,
																						BgL_arg1231z00_1951);
																				}
																				BgL_arg1229z00_1949 =
																					MAKE_YOUNG_PAIR(BgL_arg1230z00_1950,
																					BNIL);
																			}
																			BgL_arg1228z00_1948 =
																				MAKE_YOUNG_PAIR(BgL_fz00_1943,
																				BgL_arg1229z00_1949);
																		}
																		BgL_arg1226z00_1946 =
																			MAKE_YOUNG_PAIR(BgL_arg1228z00_1948,
																			BNIL);
																	}
																	{	/* Eval/expdtrace.scm 87 */
																		obj_t BgL_arg1236z00_1955;

																		{	/* Eval/expdtrace.scm 87 */
																			obj_t BgL_arg1238z00_1956;

																			{	/* Eval/expdtrace.scm 87 */
																				obj_t BgL_arg1239z00_1957;
																				obj_t BgL_arg1242z00_1958;

																				{	/* Eval/expdtrace.scm 87 */
																					obj_t BgL_arg1244z00_1959;

																					{	/* Eval/expdtrace.scm 87 */
																						obj_t BgL_arg1248z00_1960;
																						obj_t BgL_arg1249z00_1961;

																						BgL_arg1248z00_1960 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1724z00zz__expander_tracez00,
																							BNIL);
																						BgL_arg1249z00_1961 =
																							MAKE_YOUNG_PAIR(BINT(0L), BNIL);
																						BgL_arg1244z00_1959 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1248z00_1960,
																							BgL_arg1249z00_1961);
																					}
																					BgL_arg1239z00_1957 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol1726z00zz__expander_tracez00,
																						BgL_arg1244z00_1959);
																				}
																				{	/* Eval/expdtrace.scm 88 */
																					obj_t BgL_arg1252z00_1962;
																					obj_t BgL_arg1268z00_1963;

																					{	/* Eval/expdtrace.scm 88 */
																						obj_t BgL_arg1272z00_1964;

																						{	/* Eval/expdtrace.scm 88 */
																							obj_t BgL_arg1284z00_1965;

																							{	/* Eval/expdtrace.scm 88 */
																								obj_t BgL_arg1304z00_1966;

																								BgL_arg1304z00_1966 =
																									MAKE_YOUNG_PAIR(BgL_fz00_1943,
																									BNIL);
																								BgL_arg1284z00_1965 =
																									MAKE_YOUNG_PAIR
																									(BgL_lblz00_1938,
																									BgL_arg1304z00_1966);
																							}
																							BgL_arg1272z00_1964 =
																								MAKE_YOUNG_PAIR
																								(BgL_levelz00_1937,
																								BgL_arg1284z00_1965);
																						}
																						BgL_arg1252z00_1962 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol1728z00zz__expander_tracez00,
																							BgL_arg1272z00_1964);
																					}
																					{	/* Eval/expdtrace.scm 89 */
																						obj_t BgL_arg1305z00_1967;

																						BgL_arg1305z00_1967 =
																							MAKE_YOUNG_PAIR(BgL_fz00_1943,
																							BNIL);
																						BgL_arg1268z00_1963 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1305z00_1967, BNIL);
																					}
																					BgL_arg1242z00_1958 =
																						MAKE_YOUNG_PAIR(BgL_arg1252z00_1962,
																						BgL_arg1268z00_1963);
																				}
																				BgL_arg1238z00_1956 =
																					MAKE_YOUNG_PAIR(BgL_arg1239z00_1957,
																					BgL_arg1242z00_1958);
																			}
																			BgL_arg1236z00_1955 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1715z00zz__expander_tracez00,
																				BgL_arg1238z00_1956);
																		}
																		BgL_arg1227z00_1947 =
																			MAKE_YOUNG_PAIR(BgL_arg1236z00_1955,
																			BNIL);
																	}
																	BgL_arg1225z00_1945 =
																		MAKE_YOUNG_PAIR(BgL_arg1226z00_1946,
																		BgL_arg1227z00_1947);
																}
																BgL_nxz00_1944 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1730z00zz__expander_tracez00,
																	BgL_arg1225z00_1945);
															}
															{	/* Eval/expdtrace.scm 86 */

																return
																	BGL_PROCEDURE_CALL2(BgL_ez00_1896,
																	BgL_nxz00_1944, BgL_ez00_1896);
															}
														}
													}
												else
													{	/* Eval/expdtrace.scm 91 */
														obj_t BgL_arg1306z00_1968;

														{	/* Eval/expdtrace.scm 91 */
															obj_t BgL_arg1307z00_1969;

															BgL_arg1307z00_1969 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_argza2za2_1939, BNIL);
															BgL_arg1306z00_1968 =
																MAKE_YOUNG_PAIR
																(BGl_symbol1713z00zz__expander_tracez00,
																BgL_arg1307z00_1969);
														}
														return
															BGL_PROCEDURE_CALL2(BgL_ez00_1896,
															BgL_arg1306z00_1968, BgL_ez00_1896);
													}
											}
										}
									else
										{	/* Eval/expdtrace.scm 77 */
											return
												BGl_expandzd2errorzd2zz__expandz00
												(BGl_string1732z00zz__expander_tracez00,
												BGl_string1718z00zz__expander_tracez00, BgL_xz00_1895);
										}
								}
							else
								{	/* Eval/expdtrace.scm 77 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string1732z00zz__expander_tracez00,
										BGl_string1718z00zz__expander_tracez00, BgL_xz00_1895);
								}
						}
					else
						{	/* Eval/expdtrace.scm 77 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string1732z00zz__expander_tracez00,
								BGl_string1718z00zz__expander_tracez00, BgL_xz00_1895);
						}
				}
			}
		}

	}



/* make-expand-trace-item */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2expandzd2tracezd2itemzd2zz__expander_tracez00(obj_t
		BgL_modez00_5)
	{
		{	/* Eval/expdtrace.scm 98 */
			{	/* Eval/expdtrace.scm 99 */
				obj_t BgL_zc3z04anonymousza31311ze3z87_1897;

				BgL_zc3z04anonymousza31311ze3z87_1897 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31311ze3ze5zz__expander_tracez00, (int) (2L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31311ze3z87_1897, (int) (0L),
					BgL_modez00_5);
				return BgL_zc3z04anonymousza31311ze3z87_1897;
			}
		}

	}



/* &make-expand-trace-item */
	obj_t BGl_z62makezd2expandzd2tracezd2itemzb0zz__expander_tracez00(obj_t
		BgL_envz00_1898, obj_t BgL_modez00_1899)
	{
		{	/* Eval/expdtrace.scm 98 */
			{	/* Eval/expdtrace.scm 99 */
				obj_t BgL_auxz00_2154;

				if (SYMBOLP(BgL_modez00_1899))
					{	/* Eval/expdtrace.scm 99 */
						BgL_auxz00_2154 = BgL_modez00_1899;
					}
				else
					{
						obj_t BgL_auxz00_2157;

						BgL_auxz00_2157 =
							BGl_typezd2errorzd2zz__errorz00
							(BGl_string1706z00zz__expander_tracez00, BINT(3318L),
							BGl_string1733z00zz__expander_tracez00,
							BGl_string1708z00zz__expander_tracez00, BgL_modez00_1899);
						FAILURE(BgL_auxz00_2157, BFALSE, BFALSE);
					}
				return
					BGl_makezd2expandzd2tracezd2itemzd2zz__expander_tracez00
					(BgL_auxz00_2154);
			}
		}

	}



/* &<@anonymous:1311> */
	obj_t BGl_z62zc3z04anonymousza31311ze3ze5zz__expander_tracez00(obj_t
		BgL_envz00_1900, obj_t BgL_xz00_1902, obj_t BgL_ez00_1903)
	{
		{	/* Eval/expdtrace.scm 99 */
			{	/* Eval/expdtrace.scm 100 */
				obj_t BgL_modez00_1901;

				BgL_modez00_1901 = ((obj_t) PROCEDURE_REF(BgL_envz00_1900, (int) (0L)));
				{	/* Eval/expdtrace.scm 100 */
					bool_t BgL_test1775z00_2165;

					{	/* Eval/expdtrace.scm 100 */
						bool_t BgL_test1776z00_2166;

						{	/* Eval/expdtrace.scm 100 */
							int BgL_arg1336z00_1972;

							BgL_arg1336z00_1972 = BGl_bigloozd2profilezd2zz__paramz00();
							BgL_test1776z00_2166 = ((long) (BgL_arg1336z00_1972) == 0L);
						}
						if (BgL_test1776z00_2166)
							{	/* Eval/expdtrace.scm 100 */
								if (
									(BgL_modez00_1901 == BGl_symbol1709z00zz__expander_tracez00))
									{	/* Eval/expdtrace.scm 102 */
										int BgL_arg1334z00_1973;

										BgL_arg1334z00_1973 =
											BGl_bigloozd2compilerzd2debugz00zz__paramz00();
										BgL_test1775z00_2165 = ((long) (BgL_arg1334z00_1973) > 0L);
									}
								else
									{	/* Eval/expdtrace.scm 103 */
										int BgL_arg1335z00_1974;

										BgL_arg1335z00_1974 = bgl_debug();
										BgL_test1775z00_2165 = ((long) (BgL_arg1335z00_1974) > 0L);
							}}
						else
							{	/* Eval/expdtrace.scm 100 */
								BgL_test1775z00_2165 = ((bool_t) 0);
							}
					}
					if (BgL_test1775z00_2165)
						{	/* Eval/expdtrace.scm 105 */
							obj_t BgL_arg1318z00_1975;

							{	/* Eval/expdtrace.scm 105 */
								obj_t BgL_arg1319z00_1976;
								obj_t BgL_arg1320z00_1977;

								{	/* Eval/expdtrace.scm 105 */
									obj_t BgL_arg1321z00_1978;

									{	/* Eval/expdtrace.scm 105 */
										obj_t BgL_arg1322z00_1979;
										obj_t BgL_arg1323z00_1980;

										BgL_arg1322z00_1979 =
											MAKE_YOUNG_PAIR(BGl_symbol1724z00zz__expander_tracez00,
											BNIL);
										BgL_arg1323z00_1980 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
										BgL_arg1321z00_1978 =
											MAKE_YOUNG_PAIR(BgL_arg1322z00_1979, BgL_arg1323z00_1980);
									}
									BgL_arg1319z00_1976 =
										MAKE_YOUNG_PAIR(BGl_symbol1726z00zz__expander_tracez00,
										BgL_arg1321z00_1978);
								}
								{	/* Eval/expdtrace.scm 106 */
									obj_t BgL_arg1325z00_1981;
									obj_t BgL_arg1326z00_1982;

									{	/* Eval/expdtrace.scm 106 */
										obj_t BgL_arg1327z00_1983;

										{	/* Eval/expdtrace.scm 106 */
											obj_t BgL_arg1328z00_1984;

											{	/* Eval/expdtrace.scm 106 */
												obj_t BgL_l1080z00_1985;

												BgL_l1080z00_1985 = CDR(((obj_t) BgL_xz00_1902));
												if (NULLP(BgL_l1080z00_1985))
													{	/* Eval/expdtrace.scm 106 */
														BgL_arg1328z00_1984 = BNIL;
													}
												else
													{	/* Eval/expdtrace.scm 106 */
														obj_t BgL_head1082z00_1986;

														BgL_head1082z00_1986 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1080z00_1988;
															obj_t BgL_tail1083z00_1989;

															BgL_l1080z00_1988 = BgL_l1080z00_1985;
															BgL_tail1083z00_1989 = BgL_head1082z00_1986;
														BgL_zc3z04anonymousza31330ze3z87_1987:
															if (NULLP(BgL_l1080z00_1988))
																{	/* Eval/expdtrace.scm 106 */
																	BgL_arg1328z00_1984 =
																		CDR(BgL_head1082z00_1986);
																}
															else
																{	/* Eval/expdtrace.scm 106 */
																	obj_t BgL_newtail1084z00_1990;

																	{	/* Eval/expdtrace.scm 106 */
																		obj_t BgL_arg1333z00_1991;

																		{	/* Eval/expdtrace.scm 106 */
																			obj_t BgL_xz00_1992;

																			BgL_xz00_1992 =
																				CAR(((obj_t) BgL_l1080z00_1988));
																			BgL_arg1333z00_1991 =
																				BGL_PROCEDURE_CALL2(BgL_ez00_1903,
																				BgL_xz00_1992, BgL_ez00_1903);
																		}
																		BgL_newtail1084z00_1990 =
																			MAKE_YOUNG_PAIR(BgL_arg1333z00_1991,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1083z00_1989,
																		BgL_newtail1084z00_1990);
																	{	/* Eval/expdtrace.scm 106 */
																		obj_t BgL_arg1332z00_1993;

																		BgL_arg1332z00_1993 =
																			CDR(((obj_t) BgL_l1080z00_1988));
																		{
																			obj_t BgL_tail1083z00_2203;
																			obj_t BgL_l1080z00_2202;

																			BgL_l1080z00_2202 = BgL_arg1332z00_1993;
																			BgL_tail1083z00_2203 =
																				BgL_newtail1084z00_1990;
																			BgL_tail1083z00_1989 =
																				BgL_tail1083z00_2203;
																			BgL_l1080z00_1988 = BgL_l1080z00_2202;
																			goto
																				BgL_zc3z04anonymousza31330ze3z87_1987;
																		}
																	}
																}
														}
													}
											}
											BgL_arg1327z00_1983 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1328z00_1984, BNIL);
										}
										BgL_arg1325z00_1981 =
											MAKE_YOUNG_PAIR(BGl_symbol1734z00zz__expander_tracez00,
											BgL_arg1327z00_1983);
									}
									BgL_arg1326z00_1982 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
									BgL_arg1320z00_1977 =
										MAKE_YOUNG_PAIR(BgL_arg1325z00_1981, BgL_arg1326z00_1982);
								}
								BgL_arg1318z00_1975 =
									MAKE_YOUNG_PAIR(BgL_arg1319z00_1976, BgL_arg1320z00_1977);
							}
							return
								MAKE_YOUNG_PAIR(BGl_symbol1715z00zz__expander_tracez00,
								BgL_arg1318z00_1975);
						}
					else
						{	/* Eval/expdtrace.scm 100 */
							return BUNSPEC;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_tracez00(void)
	{
		{	/* Eval/expdtrace.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_tracez00(void)
	{
		{	/* Eval/expdtrace.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_tracez00(void)
	{
		{	/* Eval/expdtrace.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_tracez00(void)
	{
		{	/* Eval/expdtrace.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1736z00zz__expander_tracez00));
		}

	}

#ifdef __cplusplus
}
#endif
