/*===========================================================================*/
/*   (Eval/expdtry.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdtry.scm -indent -o objs/obj_u/Eval/expdtry.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_TRY_TYPE_DEFINITIONS
#define BGL___EXPANDER_TRY_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_TRY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1594z00zz__expander_tryz00 = BUNSPEC;
	static obj_t BGl_symbol1596z00zz__expander_tryz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_tryz00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_evepairifyz00zz__prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_tryz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2tryzd2zz__expander_tryz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_tryz00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_tryz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_tryz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_tryz00(void);
	static obj_t BGl_z62expandzd2tryzb0zz__expander_tryz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__expander_tryz00(void);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_tryz00(void);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2tryzd2envz00zz__expander_tryz00,
		BgL_bgl_za762expandza7d2tryza71601za7,
		BGl_z62expandzd2tryzb0zz__expander_tryz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1600z00zz__expander_tryz00,
		BgL_bgl_string1600za700za7za7_1602za7, "__expander_try", 14);
	      DEFINE_STRING(BGl_string1595z00zz__expander_tryz00,
		BgL_bgl_string1595za700za7za7_1603za7, "lambda", 6);
	      DEFINE_STRING(BGl_string1597z00zz__expander_tryz00,
		BgL_bgl_string1597za700za7za7_1604za7, "&try", 4);
	      DEFINE_STRING(BGl_string1598z00zz__expander_tryz00,
		BgL_bgl_string1598za700za7za7_1605za7, "try", 3);
	      DEFINE_STRING(BGl_string1599z00zz__expander_tryz00,
		BgL_bgl_string1599za700za7za7_1606za7, "Illegal form", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1594z00zz__expander_tryz00));
		     ADD_ROOT((void *) (&BGl_symbol1596z00zz__expander_tryz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_tryz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_tryz00(long
		BgL_checksumz00_1741, char *BgL_fromz00_1742)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_tryz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_tryz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_tryz00();
					BGl_cnstzd2initzd2zz__expander_tryz00();
					BGl_importedzd2moduleszd2initz00zz__expander_tryz00();
					return BGl_methodzd2initzd2zz__expander_tryz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_tryz00(void)
	{
		{	/* Eval/expdtry.scm 14 */
			BGl_symbol1594z00zz__expander_tryz00 =
				bstring_to_symbol(BGl_string1595z00zz__expander_tryz00);
			return (BGl_symbol1596z00zz__expander_tryz00 =
				bstring_to_symbol(BGl_string1597z00zz__expander_tryz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_tryz00(void)
	{
		{	/* Eval/expdtry.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* expand-try */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2tryzd2zz__expander_tryz00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expdtry.scm 54 */
			{
				obj_t BgL_bodyz00_1118;
				obj_t BgL_handlerz00_1119;

				if (PAIRP(BgL_xz00_3))
					{	/* Eval/expdtry.scm 55 */
						obj_t BgL_cdrzd2109zd2_1124;

						BgL_cdrzd2109zd2_1124 = CDR(((obj_t) BgL_xz00_3));
						if (PAIRP(BgL_cdrzd2109zd2_1124))
							{	/* Eval/expdtry.scm 55 */
								obj_t BgL_carzd2112zd2_1126;
								obj_t BgL_cdrzd2113zd2_1127;

								BgL_carzd2112zd2_1126 = CAR(BgL_cdrzd2109zd2_1124);
								BgL_cdrzd2113zd2_1127 = CDR(BgL_cdrzd2109zd2_1124);
								if (NULLP(BgL_carzd2112zd2_1126))
									{	/* Eval/expdtry.scm 55 */
										return
											BGl_expandzd2errorzd2zz__expandz00
											(BGl_string1598z00zz__expander_tryz00,
											BGl_string1599z00zz__expander_tryz00, BgL_xz00_3);
									}
								else
									{	/* Eval/expdtry.scm 55 */
										if (PAIRP(BgL_cdrzd2113zd2_1127))
											{	/* Eval/expdtry.scm 55 */
												if (NULLP(CDR(BgL_cdrzd2113zd2_1127)))
													{	/* Eval/expdtry.scm 55 */
														BgL_bodyz00_1118 = BgL_carzd2112zd2_1126;
														BgL_handlerz00_1119 = CAR(BgL_cdrzd2113zd2_1127);
														{	/* Eval/expdtry.scm 57 */
															obj_t BgL_nz00_1134;

															{	/* Eval/expdtry.scm 57 */
																obj_t BgL_arg1187z00_1135;

																{	/* Eval/expdtry.scm 57 */
																	obj_t BgL_arg1188z00_1136;

																	{	/* Eval/expdtry.scm 57 */
																		obj_t BgL_arg1189z00_1137;
																		obj_t BgL_arg1190z00_1138;

																		{	/* Eval/expdtry.scm 57 */
																			obj_t BgL_arg1191z00_1139;

																			{	/* Eval/expdtry.scm 57 */
																				obj_t BgL_arg1193z00_1140;

																				BgL_arg1193z00_1140 =
																					MAKE_YOUNG_PAIR(BgL_bodyz00_1118,
																					BNIL);
																				BgL_arg1191z00_1139 =
																					MAKE_YOUNG_PAIR(BNIL,
																					BgL_arg1193z00_1140);
																			}
																			BgL_arg1189z00_1137 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1594z00zz__expander_tryz00,
																				BgL_arg1191z00_1139);
																		}
																		BgL_arg1190z00_1138 =
																			MAKE_YOUNG_PAIR(BgL_handlerz00_1119,
																			BNIL);
																		BgL_arg1188z00_1136 =
																			MAKE_YOUNG_PAIR(BgL_arg1189z00_1137,
																			BgL_arg1190z00_1138);
																	}
																	BgL_arg1187z00_1135 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol1596z00zz__expander_tryz00,
																		BgL_arg1188z00_1136);
																}
																BgL_nz00_1134 =
																	BGL_PROCEDURE_CALL2(BgL_ez00_4,
																	BgL_arg1187z00_1135, BgL_ez00_4);
															}
															BGL_TAIL return
																BGl_evepairifyz00zz__prognz00(BgL_nz00_1134,
																BgL_xz00_3);
														}
													}
												else
													{	/* Eval/expdtry.scm 55 */
														return
															BGl_expandzd2errorzd2zz__expandz00
															(BGl_string1598z00zz__expander_tryz00,
															BGl_string1599z00zz__expander_tryz00, BgL_xz00_3);
													}
											}
										else
											{	/* Eval/expdtry.scm 55 */
												return
													BGl_expandzd2errorzd2zz__expandz00
													(BGl_string1598z00zz__expander_tryz00,
													BGl_string1599z00zz__expander_tryz00, BgL_xz00_3);
											}
									}
							}
						else
							{	/* Eval/expdtry.scm 55 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string1598z00zz__expander_tryz00,
									BGl_string1599z00zz__expander_tryz00, BgL_xz00_3);
							}
					}
				else
					{	/* Eval/expdtry.scm 55 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string1598z00zz__expander_tryz00,
							BGl_string1599z00zz__expander_tryz00, BgL_xz00_3);
					}
			}
		}

	}



/* &expand-try */
	obj_t BGl_z62expandzd2tryzb0zz__expander_tryz00(obj_t BgL_envz00_1736,
		obj_t BgL_xz00_1737, obj_t BgL_ez00_1738)
	{
		{	/* Eval/expdtry.scm 54 */
			return
				BGl_expandzd2tryzd2zz__expander_tryz00(BgL_xz00_1737, BgL_ez00_1738);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_tryz00(void)
	{
		{	/* Eval/expdtry.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_tryz00(void)
	{
		{	/* Eval/expdtry.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_tryz00(void)
	{
		{	/* Eval/expdtry.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_tryz00(void)
	{
		{	/* Eval/expdtry.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1600z00zz__expander_tryz00));
		}

	}

#ifdef __cplusplus
}
#endif
