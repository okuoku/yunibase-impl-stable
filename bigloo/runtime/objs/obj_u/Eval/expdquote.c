/*===========================================================================*/
/*   (Eval/expdquote.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdquote.scm -indent -o objs/obj_u/Eval/expdquote.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_QUOTE_TYPE_DEFINITIONS
#define BGL___EXPANDER_QUOTE_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_QUOTE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_templatez00zz__expander_quotez00(obj_t, obj_t);
	static obj_t BGl_list1801z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_list1802z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_list1807z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_quotez00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_quotez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol1783z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1786z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1787z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1790z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1793z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1795z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1797z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1799z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_listzd2templatezd2zz__expander_quotez00(obj_t, obj_t);
	static obj_t BGl_z62quasiquotationz62zz__expander_quotez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_quotez00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_quotez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_quotez00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_quotez00(void);
	static obj_t BGl_list1785z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__expander_quotez00(void);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_list1792z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_z62expandzd2quotezb0zz__expander_quotez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_templatezd2orzd2splicez00zz__expander_quotez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2quotezd2zz__expander_quotez00(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_quotez00(void);
	BGL_EXPORTED_DECL obj_t BGl_quasiquotationz00zz__expander_quotez00(obj_t,
		obj_t);
	static obj_t BGl_symbol1803z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_symbol1805z00zz__expander_quotez00 = BUNSPEC;
	static obj_t BGl_vectorzd2templatezd2zz__expander_quotez00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1791z00zz__expander_quotez00,
		BgL_bgl_string1791za700za7za7_1810za7, "quasiquote", 10);
	      DEFINE_STRING(BGl_string1794z00zz__expander_quotez00,
		BgL_bgl_string1794za700za7za7_1811za7, "cons*", 5);
	      DEFINE_STRING(BGl_string1796z00zz__expander_quotez00,
		BgL_bgl_string1796za700za7za7_1812za7, "list->vector", 12);
	      DEFINE_STRING(BGl_string1798z00zz__expander_quotez00,
		BgL_bgl_string1798za700za7za7_1813za7, "vector-tag-set!", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_expandzd2quotezd2envz00zz__expander_quotez00,
		BgL_bgl_za762expandza7d2quot1814z00,
		BGl_z62expandzd2quotezb0zz__expander_quotez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_quasiquotationzd2envzd2zz__expander_quotez00,
		BgL_bgl_za762quasiquotatio1815za7,
		BGl_z62quasiquotationz62zz__expander_quotez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1800z00zz__expander_quotez00,
		BgL_bgl_string1800za700za7za7_1816za7, "let", 3);
	      DEFINE_STRING(BGl_string1804z00zz__expander_quotez00,
		BgL_bgl_string1804za700za7za7_1817za7, "unquote-splicing", 16);
	      DEFINE_STRING(BGl_string1806z00zz__expander_quotez00,
		BgL_bgl_string1806za700za7za7_1818za7, "eappend", 7);
	      DEFINE_STRING(BGl_string1808z00zz__expander_quotez00,
		BgL_bgl_string1808za700za7za7_1819za7, "Illegal `unquote-splicing' form",
		31);
	      DEFINE_STRING(BGl_string1809z00zz__expander_quotez00,
		BgL_bgl_string1809za700za7za7_1820za7, "__expander_quote", 16);
	      DEFINE_STRING(BGl_string1779z00zz__expander_quotez00,
		BgL_bgl_string1779za700za7za7_1821za7, "quote", 5);
	      DEFINE_STRING(BGl_string1780z00zz__expander_quotez00,
		BgL_bgl_string1780za700za7za7_1822za7, "Illegal `quote' form", 20);
	      DEFINE_STRING(BGl_string1781z00zz__expander_quotez00,
		BgL_bgl_string1781za700za7za7_1823za7, "quasiquotation", 14);
	      DEFINE_STRING(BGl_string1782z00zz__expander_quotez00,
		BgL_bgl_string1782za700za7za7_1824za7, "illegal `quasiquote' form", 25);
	      DEFINE_STRING(BGl_string1784z00zz__expander_quotez00,
		BgL_bgl_string1784za700za7za7_1825za7, "unquote", 7);
	      DEFINE_STRING(BGl_string1788z00zz__expander_quotez00,
		BgL_bgl_string1788za700za7za7_1826za7, "list", 4);
	      DEFINE_STRING(BGl_string1789z00zz__expander_quotez00,
		BgL_bgl_string1789za700za7za7_1827za7, "Illegal `unquote' form", 22);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list1801z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_list1802z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_list1807z00zz__expander_quotez00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1783z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1786z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1787z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1790z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1793z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1795z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1797z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1799z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_list1785z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_list1792z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1803z00zz__expander_quotez00));
		     ADD_ROOT((void *) (&BGl_symbol1805z00zz__expander_quotez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_quotez00(long
		BgL_checksumz00_2019, char *BgL_fromz00_2020)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_quotez00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_quotez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_quotez00();
					BGl_cnstzd2initzd2zz__expander_quotez00();
					BGl_importedzd2moduleszd2initz00zz__expander_quotez00();
					return BGl_methodzd2initzd2zz__expander_quotez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_quotez00(void)
	{
		{	/* Eval/expdquote.scm 14 */
			BGl_symbol1783z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1784z00zz__expander_quotez00);
			BGl_symbol1786z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1779z00zz__expander_quotez00);
			BGl_list1785z00zz__expander_quotez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1786z00zz__expander_quotez00,
				MAKE_YOUNG_PAIR(BGl_symbol1783z00zz__expander_quotez00, BNIL));
			BGl_symbol1787z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1788z00zz__expander_quotez00);
			BGl_symbol1790z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1791z00zz__expander_quotez00);
			BGl_list1792z00zz__expander_quotez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1786z00zz__expander_quotez00,
				MAKE_YOUNG_PAIR(BGl_symbol1790z00zz__expander_quotez00, BNIL));
			BGl_symbol1793z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1794z00zz__expander_quotez00);
			BGl_symbol1795z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1796z00zz__expander_quotez00);
			BGl_symbol1797z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1798z00zz__expander_quotez00);
			BGl_symbol1799z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1800z00zz__expander_quotez00);
			BGl_list1802z00zz__expander_quotez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1786z00zz__expander_quotez00,
				MAKE_YOUNG_PAIR(BNIL, BNIL));
			BGl_list1801z00zz__expander_quotez00 =
				MAKE_YOUNG_PAIR(BGl_list1802z00zz__expander_quotez00, BNIL);
			BGl_symbol1803z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1804z00zz__expander_quotez00);
			BGl_symbol1805z00zz__expander_quotez00 =
				bstring_to_symbol(BGl_string1806z00zz__expander_quotez00);
			return (BGl_list1807z00zz__expander_quotez00 =
				MAKE_YOUNG_PAIR(BGl_symbol1786z00zz__expander_quotez00,
					MAKE_YOUNG_PAIR(BGl_symbol1803z00zz__expander_quotez00, BNIL)),
				BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_quotez00(void)
	{
		{	/* Eval/expdquote.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* expand-quote */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2quotezd2zz__expander_quotez00(obj_t
		BgL_xz00_3, obj_t BgL_ez00_4)
	{
		{	/* Eval/expdquote.scm 54 */
			if (PAIRP(BgL_xz00_3))
				{	/* Eval/expdquote.scm 55 */
					obj_t BgL_cdrzd2107zd2_1136;

					BgL_cdrzd2107zd2_1136 = CDR(((obj_t) BgL_xz00_3));
					if (PAIRP(BgL_cdrzd2107zd2_1136))
						{	/* Eval/expdquote.scm 55 */
							if (NULLP(CDR(BgL_cdrzd2107zd2_1136)))
								{	/* Eval/expdquote.scm 55 */
									return BgL_xz00_3;
								}
							else
								{	/* Eval/expdquote.scm 55 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string1779z00zz__expander_quotez00,
										BGl_string1780z00zz__expander_quotez00, BgL_xz00_3);
								}
						}
					else
						{	/* Eval/expdquote.scm 55 */
							return
								BGl_expandzd2errorzd2zz__expandz00
								(BGl_string1779z00zz__expander_quotez00,
								BGl_string1780z00zz__expander_quotez00, BgL_xz00_3);
						}
				}
			else
				{	/* Eval/expdquote.scm 55 */
					return
						BGl_expandzd2errorzd2zz__expandz00
						(BGl_string1779z00zz__expander_quotez00,
						BGl_string1780z00zz__expander_quotez00, BgL_xz00_3);
				}
		}

	}



/* &expand-quote */
	obj_t BGl_z62expandzd2quotezb0zz__expander_quotez00(obj_t BgL_envz00_2003,
		obj_t BgL_xz00_2004, obj_t BgL_ez00_2005)
	{
		{	/* Eval/expdquote.scm 54 */
			return
				BGl_expandzd2quotezd2zz__expander_quotez00(BgL_xz00_2004,
				BgL_ez00_2005);
		}

	}



/* quasiquotation */
	BGL_EXPORTED_DEF obj_t BGl_quasiquotationz00zz__expander_quotez00(obj_t
		BgL_dz00_5, obj_t BgL_expz00_6)
	{
		{	/* Eval/expdquote.scm 82 */
			{	/* Eval/expdquote.scm 83 */
				bool_t BgL_test1832z00_2061;

				if (PAIRP(BgL_expz00_6))
					{	/* Eval/expdquote.scm 83 */
						bool_t BgL_test1834z00_2064;

						{	/* Eval/expdquote.scm 83 */
							obj_t BgL_tmpz00_2065;

							BgL_tmpz00_2065 = CDR(BgL_expz00_6);
							BgL_test1834z00_2064 = PAIRP(BgL_tmpz00_2065);
						}
						if (BgL_test1834z00_2064)
							{	/* Eval/expdquote.scm 83 */
								BgL_test1832z00_2061 = NULLP(CDR(CDR(BgL_expz00_6)));
							}
						else
							{	/* Eval/expdquote.scm 83 */
								BgL_test1832z00_2061 = ((bool_t) 0);
							}
					}
				else
					{	/* Eval/expdquote.scm 83 */
						BgL_test1832z00_2061 = ((bool_t) 0);
					}
				if (BgL_test1832z00_2061)
					{	/* Eval/expdquote.scm 83 */
						BGL_TAIL return
							BGl_templatez00zz__expander_quotez00(BgL_dz00_5,
							CAR(CDR(BgL_expz00_6)));
					}
				else
					{	/* Eval/expdquote.scm 83 */
						return
							BGl_expandzd2errorzd2zz__expandz00
							(BGl_string1781z00zz__expander_quotez00,
							BGl_string1782z00zz__expander_quotez00, BgL_expz00_6);
					}
			}
		}

	}



/* &quasiquotation */
	obj_t BGl_z62quasiquotationz62zz__expander_quotez00(obj_t BgL_envz00_2006,
		obj_t BgL_dz00_2007, obj_t BgL_expz00_2008)
	{
		{	/* Eval/expdquote.scm 82 */
			return
				BGl_quasiquotationz00zz__expander_quotez00(BgL_dz00_2007,
				BgL_expz00_2008);
		}

	}



/* template */
	obj_t BGl_templatez00zz__expander_quotez00(obj_t BgL_dz00_7,
		obj_t BgL_expz00_8)
	{
		{	/* Eval/expdquote.scm 90 */
		BGl_templatez00zz__expander_quotez00:
			if (((long) CINT(BgL_dz00_7) == 0L))
				{	/* Eval/expdquote.scm 91 */
					return BgL_expz00_8;
				}
			else
				{	/* Eval/expdquote.scm 93 */
					bool_t BgL_test1836z00_2079;

					if (PAIRP(BgL_expz00_8))
						{	/* Eval/expdquote.scm 93 */
							BgL_test1836z00_2079 =
								(CAR(BgL_expz00_8) == BGl_symbol1783z00zz__expander_quotez00);
						}
					else
						{	/* Eval/expdquote.scm 93 */
							BgL_test1836z00_2079 = ((bool_t) 0);
						}
					if (BgL_test1836z00_2079)
						{	/* Eval/expdquote.scm 94 */
							bool_t BgL_test1838z00_2084;

							{	/* Eval/expdquote.scm 94 */
								bool_t BgL_test1839z00_2085;

								{	/* Eval/expdquote.scm 94 */
									obj_t BgL_tmpz00_2086;

									BgL_tmpz00_2086 = CDR(BgL_expz00_8);
									BgL_test1839z00_2085 = PAIRP(BgL_tmpz00_2086);
								}
								if (BgL_test1839z00_2085)
									{	/* Eval/expdquote.scm 94 */
										BgL_test1838z00_2084 = NULLP(CDR(CDR(BgL_expz00_8)));
									}
								else
									{	/* Eval/expdquote.scm 94 */
										BgL_test1838z00_2084 = ((bool_t) 0);
									}
							}
							if (BgL_test1838z00_2084)
								{	/* Eval/expdquote.scm 94 */
									if ((BgL_dz00_7 == BINT(1L)))
										{	/* Eval/expdquote.scm 96 */
											long BgL_arg1210z00_1168;
											obj_t BgL_arg1212z00_1169;

											BgL_arg1210z00_1168 = ((long) CINT(BgL_dz00_7) - 1L);
											BgL_arg1212z00_1169 = CAR(CDR(BgL_expz00_8));
											{
												obj_t BgL_expz00_2101;
												obj_t BgL_dz00_2099;

												BgL_dz00_2099 = BINT(BgL_arg1210z00_1168);
												BgL_expz00_2101 = BgL_arg1212z00_1169;
												BgL_expz00_8 = BgL_expz00_2101;
												BgL_dz00_7 = BgL_dz00_2099;
												goto BGl_templatez00zz__expander_quotez00;
											}
										}
									else
										{	/* Eval/expdquote.scm 97 */
											obj_t BgL_arg1215z00_1170;

											{	/* Eval/expdquote.scm 97 */
												long BgL_arg1220z00_1174;
												obj_t BgL_arg1221z00_1175;

												BgL_arg1220z00_1174 = ((long) CINT(BgL_dz00_7) - 1L);
												BgL_arg1221z00_1175 = CAR(CDR(BgL_expz00_8));
												BgL_arg1215z00_1170 =
													BGl_templatez00zz__expander_quotez00(BINT
													(BgL_arg1220z00_1174), BgL_arg1221z00_1175);
											}
											{	/* Eval/expdquote.scm 97 */
												obj_t BgL_list1216z00_1171;

												{	/* Eval/expdquote.scm 97 */
													obj_t BgL_arg1218z00_1172;

													{	/* Eval/expdquote.scm 97 */
														obj_t BgL_arg1219z00_1173;

														BgL_arg1219z00_1173 =
															MAKE_YOUNG_PAIR(BgL_arg1215z00_1170, BNIL);
														BgL_arg1218z00_1172 =
															MAKE_YOUNG_PAIR
															(BGl_list1785z00zz__expander_quotez00,
															BgL_arg1219z00_1173);
													}
													BgL_list1216z00_1171 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1787z00zz__expander_quotez00,
														BgL_arg1218z00_1172);
												}
												return BgL_list1216z00_1171;
											}
										}
								}
							else
								{	/* Eval/expdquote.scm 94 */
									return
										BGl_expandzd2errorzd2zz__expandz00
										(BGl_string1784z00zz__expander_quotez00,
										BGl_string1789z00zz__expander_quotez00, BgL_expz00_8);
								}
						}
					else
						{	/* Eval/expdquote.scm 93 */
							if (VECTORP(BgL_expz00_8))
								{	/* Eval/expdquote.scm 99 */
									return
										BGl_vectorzd2templatezd2zz__expander_quotez00(BgL_dz00_7,
										BgL_expz00_8);
								}
							else
								{	/* Eval/expdquote.scm 99 */
									if (PAIRP(BgL_expz00_8))
										{	/* Eval/expdquote.scm 101 */
											return
												BGl_listzd2templatezd2zz__expander_quotez00(BgL_dz00_7,
												BgL_expz00_8);
										}
									else
										{	/* Eval/expdquote.scm 101 */
											if (NULLP(BgL_expz00_8))
												{	/* Eval/expdquote.scm 104 */
													obj_t BgL_list1229z00_1182;

													{	/* Eval/expdquote.scm 104 */
														obj_t BgL_arg1230z00_1183;

														BgL_arg1230z00_1183 =
															MAKE_YOUNG_PAIR(BgL_expz00_8, BNIL);
														BgL_list1229z00_1182 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1786z00zz__expander_quotez00,
															BgL_arg1230z00_1183);
													}
													return BgL_list1229z00_1182;
												}
											else
												{	/* Eval/expdquote.scm 105 */
													bool_t BgL_test1844z00_2122;

													if (CHARP(BgL_expz00_8))
														{	/* Eval/expdquote.scm 105 */
															BgL_test1844z00_2122 = ((bool_t) 1);
														}
													else
														{	/* Eval/expdquote.scm 105 */
															if (INTEGERP(BgL_expz00_8))
																{	/* Eval/expdquote.scm 105 */
																	BgL_test1844z00_2122 = ((bool_t) 1);
																}
															else
																{	/* Eval/expdquote.scm 105 */
																	if (STRINGP(BgL_expz00_8))
																		{	/* Eval/expdquote.scm 105 */
																			BgL_test1844z00_2122 = ((bool_t) 1);
																		}
																	else
																		{	/* Eval/expdquote.scm 105 */
																			BgL_test1844z00_2122 =
																				CNSTP(BgL_expz00_8);
																		}
																}
														}
													if (BgL_test1844z00_2122)
														{	/* Eval/expdquote.scm 105 */
															return BgL_expz00_8;
														}
													else
														{	/* Eval/expdquote.scm 108 */
															obj_t BgL_list1235z00_1188;

															{	/* Eval/expdquote.scm 108 */
																obj_t BgL_arg1236z00_1189;

																BgL_arg1236z00_1189 =
																	MAKE_YOUNG_PAIR(BgL_expz00_8, BNIL);
																BgL_list1235z00_1188 =
																	MAKE_YOUNG_PAIR
																	(BGl_symbol1786z00zz__expander_quotez00,
																	BgL_arg1236z00_1189);
															}
															return BgL_list1235z00_1188;
														}
												}
										}
								}
						}
				}
		}

	}



/* list-template */
	obj_t BGl_listzd2templatezd2zz__expander_quotez00(obj_t BgL_dz00_9,
		obj_t BgL_expz00_10)
	{
		{	/* Eval/expdquote.scm 113 */
			{	/* Eval/expdquote.scm 114 */
				bool_t BgL_test1848z00_2132;

				{	/* Eval/expdquote.scm 114 */
					bool_t BgL_test1849z00_2133;

					{	/* Eval/expdquote.scm 114 */
						bool_t BgL_test1850z00_2134;

						{	/* Eval/expdquote.scm 114 */
							obj_t BgL_tmpz00_2135;

							BgL_tmpz00_2135 = CDR(BgL_expz00_10);
							BgL_test1850z00_2134 = PAIRP(BgL_tmpz00_2135);
						}
						if (BgL_test1850z00_2134)
							{	/* Eval/expdquote.scm 114 */
								BgL_test1849z00_2133 = NULLP(CDR(CDR(BgL_expz00_10)));
							}
						else
							{	/* Eval/expdquote.scm 114 */
								BgL_test1849z00_2133 = ((bool_t) 0);
							}
					}
					if (BgL_test1849z00_2133)
						{	/* Eval/expdquote.scm 114 */
							if (
								(CAR(BgL_expz00_10) == BGl_symbol1786z00zz__expander_quotez00))
								{	/* Eval/expdquote.scm 115 */
									bool_t BgL_test1852z00_2144;

									{	/* Eval/expdquote.scm 115 */
										obj_t BgL_tmpz00_2145;

										BgL_tmpz00_2145 = CAR(CDR(BgL_expz00_10));
										BgL_test1852z00_2144 = PAIRP(BgL_tmpz00_2145);
									}
									if (BgL_test1852z00_2144)
										{	/* Eval/expdquote.scm 115 */
											BgL_test1848z00_2132 =
												(CAR(CAR(CDR(BgL_expz00_10))) ==
												BGl_symbol1790z00zz__expander_quotez00);
										}
									else
										{	/* Eval/expdquote.scm 115 */
											BgL_test1848z00_2132 = ((bool_t) 0);
										}
								}
							else
								{	/* Eval/expdquote.scm 115 */
									BgL_test1848z00_2132 = ((bool_t) 0);
								}
						}
					else
						{	/* Eval/expdquote.scm 114 */
							BgL_test1848z00_2132 = ((bool_t) 0);
						}
				}
				if (BgL_test1848z00_2132)
					{	/* Eval/expdquote.scm 114 */
						return
							BGl_quasiquotationz00zz__expander_quotez00(BgL_dz00_9,
							CAR(CDR(BgL_expz00_10)));
					}
				else
					{	/* Eval/expdquote.scm 114 */
						if ((CAR(BgL_expz00_10) == BGl_symbol1790z00zz__expander_quotez00))
							{	/* Eval/expdquote.scm 118 */
								if ((BgL_dz00_9 == BINT(0L)))
									{	/* Eval/expdquote.scm 120 */
										obj_t BgL_arg1312z00_1217;

										if (INTEGERP(BgL_dz00_9))
											{	/* Eval/expdquote.scm 120 */
												BgL_arg1312z00_1217 = ADDFX(BgL_dz00_9, BINT(1L));
											}
										else
											{	/* Eval/expdquote.scm 120 */
												BgL_arg1312z00_1217 =
													BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_dz00_9,
													BINT(1L));
											}
										return
											BGl_quasiquotationz00zz__expander_quotez00
											(BgL_arg1312z00_1217, BgL_expz00_10);
									}
								else
									{	/* Eval/expdquote.scm 121 */
										obj_t BgL_arg1314z00_1218;

										{	/* Eval/expdquote.scm 121 */
											obj_t BgL_arg1318z00_1222;

											if (INTEGERP(BgL_dz00_9))
												{	/* Eval/expdquote.scm 121 */
													BgL_arg1318z00_1222 = ADDFX(BgL_dz00_9, BINT(1L));
												}
											else
												{	/* Eval/expdquote.scm 121 */
													BgL_arg1318z00_1222 =
														BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_dz00_9,
														BINT(1L));
												}
											BgL_arg1314z00_1218 =
												BGl_quasiquotationz00zz__expander_quotez00
												(BgL_arg1318z00_1222, BgL_expz00_10);
										}
										{	/* Eval/expdquote.scm 121 */
											obj_t BgL_list1315z00_1219;

											{	/* Eval/expdquote.scm 121 */
												obj_t BgL_arg1316z00_1220;

												{	/* Eval/expdquote.scm 121 */
													obj_t BgL_arg1317z00_1221;

													BgL_arg1317z00_1221 =
														MAKE_YOUNG_PAIR(BgL_arg1314z00_1218, BNIL);
													BgL_arg1316z00_1220 =
														MAKE_YOUNG_PAIR
														(BGl_list1792z00zz__expander_quotez00,
														BgL_arg1317z00_1221);
												}
												BgL_list1315z00_1219 =
													MAKE_YOUNG_PAIR
													(BGl_symbol1787z00zz__expander_quotez00,
													BgL_arg1316z00_1220);
											}
											return BgL_list1315z00_1219;
										}
									}
							}
						else
							{	/* Eval/expdquote.scm 118 */
								if (EPAIRP(BgL_expz00_10))
									{	/* Eval/expdquote.scm 124 */
										obj_t BgL_erz00_1224;

										BgL_erz00_1224 = CER(BgL_expz00_10);
										{	/* Eval/expdquote.scm 125 */
											obj_t BgL_arg1320z00_1225;

											BgL_arg1320z00_1225 =
												BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00
												(BgL_dz00_9, BgL_expz00_10);
											{	/* Eval/expdquote.scm 125 */
												obj_t BgL_res1771z00_1787;

												BgL_res1771z00_1787 =
													MAKE_YOUNG_EPAIR
													(BGl_symbol1793z00zz__expander_quotez00,
													BgL_arg1320z00_1225, BgL_erz00_1224);
												return BgL_res1771z00_1787;
											}
										}
									}
								else
									{	/* Eval/expdquote.scm 126 */
										obj_t BgL_arg1321z00_1226;

										BgL_arg1321z00_1226 =
											BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00
											(BgL_dz00_9, BgL_expz00_10);
										return
											MAKE_YOUNG_PAIR(BGl_symbol1793z00zz__expander_quotez00,
											BgL_arg1321z00_1226);
									}
							}
					}
			}
		}

	}



/* vector-template */
	obj_t BGl_vectorzd2templatezd2zz__expander_quotez00(obj_t BgL_dz00_11,
		obj_t BgL_expz00_12)
	{
		{	/* Eval/expdquote.scm 131 */
			{	/* Eval/expdquote.scm 132 */
				int BgL_tagzd2valzd2_1239;
				obj_t BgL_reszd2valzd2_1240;

				BgL_tagzd2valzd2_1239 = VECTOR_TAG(BgL_expz00_12);
				{	/* Eval/expdquote.scm 136 */
					obj_t BgL_arg1340z00_1253;

					{	/* Eval/expdquote.scm 136 */
						obj_t BgL_arg1343z00_1256;

						BgL_arg1343z00_1256 =
							BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00
							(BgL_dz00_11,
							BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(BgL_expz00_12));
						BgL_arg1340z00_1253 =
							MAKE_YOUNG_PAIR(BGl_symbol1793z00zz__expander_quotez00,
							BgL_arg1343z00_1256);
					}
					{	/* Eval/expdquote.scm 133 */
						obj_t BgL_list1341z00_1254;

						{	/* Eval/expdquote.scm 133 */
							obj_t BgL_arg1342z00_1255;

							BgL_arg1342z00_1255 = MAKE_YOUNG_PAIR(BgL_arg1340z00_1253, BNIL);
							BgL_list1341z00_1254 =
								MAKE_YOUNG_PAIR(BGl_symbol1795z00zz__expander_quotez00,
								BgL_arg1342z00_1255);
						}
						BgL_reszd2valzd2_1240 = BgL_list1341z00_1254;
				}}
				if (((long) (BgL_tagzd2valzd2_1239) == 0L))
					{	/* Eval/expdquote.scm 137 */
						return BgL_reszd2valzd2_1240;
					}
				else
					{	/* Eval/expdquote.scm 139 */
						obj_t BgL_reszd2varzd2_1242;

						{	/* Eval/expdquote.scm 139 */

							{	/* Eval/expdquote.scm 139 */

								BgL_reszd2varzd2_1242 =
									BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
							}
						}
						{	/* Eval/expdquote.scm 140 */
							obj_t BgL_arg1331z00_1243;

							{	/* Eval/expdquote.scm 140 */
								obj_t BgL_arg1332z00_1244;
								obj_t BgL_arg1333z00_1245;

								{	/* Eval/expdquote.scm 140 */
									obj_t BgL_arg1334z00_1246;

									{	/* Eval/expdquote.scm 140 */
										obj_t BgL_arg1335z00_1247;

										BgL_arg1335z00_1247 =
											MAKE_YOUNG_PAIR(BgL_reszd2valzd2_1240, BNIL);
										BgL_arg1334z00_1246 =
											MAKE_YOUNG_PAIR(BgL_reszd2varzd2_1242,
											BgL_arg1335z00_1247);
									}
									BgL_arg1332z00_1244 =
										MAKE_YOUNG_PAIR(BgL_arg1334z00_1246, BNIL);
								}
								{	/* Eval/expdquote.scm 141 */
									obj_t BgL_arg1336z00_1248;
									obj_t BgL_arg1337z00_1249;

									{	/* Eval/expdquote.scm 141 */
										obj_t BgL_arg1338z00_1250;

										{	/* Eval/expdquote.scm 141 */
											obj_t BgL_arg1339z00_1251;

											BgL_arg1339z00_1251 =
												MAKE_YOUNG_PAIR(BINT(BgL_tagzd2valzd2_1239), BNIL);
											BgL_arg1338z00_1250 =
												MAKE_YOUNG_PAIR(BgL_reszd2varzd2_1242,
												BgL_arg1339z00_1251);
										}
										BgL_arg1336z00_1248 =
											MAKE_YOUNG_PAIR(BGl_symbol1797z00zz__expander_quotez00,
											BgL_arg1338z00_1250);
									}
									BgL_arg1337z00_1249 =
										MAKE_YOUNG_PAIR(BgL_reszd2varzd2_1242, BNIL);
									BgL_arg1333z00_1245 =
										MAKE_YOUNG_PAIR(BgL_arg1336z00_1248, BgL_arg1337z00_1249);
								}
								BgL_arg1331z00_1243 =
									MAKE_YOUNG_PAIR(BgL_arg1332z00_1244, BgL_arg1333z00_1245);
							}
							return
								MAKE_YOUNG_PAIR(BGl_symbol1799z00zz__expander_quotez00,
								BgL_arg1331z00_1243);
						}
					}
			}
		}

	}



/* template-or-splice-list */
	obj_t BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00(obj_t
		BgL_dz00_13, obj_t BgL_expz00_14)
	{
		{	/* Eval/expdquote.scm 147 */
			if (NULLP(BgL_expz00_14))
				{	/* Eval/expdquote.scm 148 */
					return BGl_list1801z00zz__expander_quotez00;
				}
			else
				{	/* Eval/expdquote.scm 148 */
					if (PAIRP(BgL_expz00_14))
						{	/* Eval/expdquote.scm 149 */
							if (
								(CAR(BgL_expz00_14) == BGl_symbol1783z00zz__expander_quotez00))
								{	/* Eval/expdquote.scm 151 */
									obj_t BgL_arg1349z00_1262;

									BgL_arg1349z00_1262 =
										BGl_templatez00zz__expander_quotez00(BgL_dz00_13,
										BgL_expz00_14);
									{	/* Eval/expdquote.scm 151 */
										obj_t BgL_list1350z00_1263;

										BgL_list1350z00_1263 =
											MAKE_YOUNG_PAIR(BgL_arg1349z00_1262, BNIL);
										return BgL_list1350z00_1263;
									}
								}
							else
								{	/* Eval/expdquote.scm 152 */
									bool_t BgL_test1862z00_2216;

									{	/* Eval/expdquote.scm 152 */
										bool_t BgL_test1863z00_2217;

										{	/* Eval/expdquote.scm 152 */
											obj_t BgL_tmpz00_2218;

											BgL_tmpz00_2218 = CAR(BgL_expz00_14);
											BgL_test1863z00_2217 = PAIRP(BgL_tmpz00_2218);
										}
										if (BgL_test1863z00_2217)
											{	/* Eval/expdquote.scm 152 */
												BgL_test1862z00_2216 =
													(CAR(CAR(BgL_expz00_14)) ==
													BGl_symbol1803z00zz__expander_quotez00);
											}
										else
											{	/* Eval/expdquote.scm 152 */
												BgL_test1862z00_2216 = ((bool_t) 0);
											}
									}
									if (BgL_test1862z00_2216)
										{	/* Eval/expdquote.scm 155 */
											obj_t BgL_arg1359z00_1270;

											{	/* Eval/expdquote.scm 155 */
												obj_t BgL_arg1361z00_1272;
												obj_t BgL_arg1362z00_1273;

												BgL_arg1361z00_1272 =
													BGl_templatezd2orzd2splicez00zz__expander_quotez00
													(BgL_dz00_13, CAR(BgL_expz00_14));
												{	/* Eval/expdquote.scm 157 */
													obj_t BgL_arg1367z00_1278;

													BgL_arg1367z00_1278 =
														BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00
														(BgL_dz00_13, CDR(BgL_expz00_14));
													BgL_arg1362z00_1273 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1793z00zz__expander_quotez00,
														BgL_arg1367z00_1278);
												}
												{	/* Eval/expdquote.scm 154 */
													obj_t BgL_list1363z00_1274;

													{	/* Eval/expdquote.scm 154 */
														obj_t BgL_arg1364z00_1275;

														{	/* Eval/expdquote.scm 154 */
															obj_t BgL_arg1365z00_1276;

															BgL_arg1365z00_1276 =
																MAKE_YOUNG_PAIR(BgL_arg1362z00_1273, BNIL);
															BgL_arg1364z00_1275 =
																MAKE_YOUNG_PAIR(BgL_arg1361z00_1272,
																BgL_arg1365z00_1276);
														}
														BgL_list1363z00_1274 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1805z00zz__expander_quotez00,
															BgL_arg1364z00_1275);
													}
													BgL_arg1359z00_1270 = BgL_list1363z00_1274;
												}
											}
											{	/* Eval/expdquote.scm 154 */
												obj_t BgL_list1360z00_1271;

												BgL_list1360z00_1271 =
													MAKE_YOUNG_PAIR(BgL_arg1359z00_1270, BNIL);
												return BgL_list1360z00_1271;
											}
										}
									else
										{	/* Eval/expdquote.scm 152 */
											return
												MAKE_YOUNG_PAIR
												(BGl_templatezd2orzd2splicez00zz__expander_quotez00
												(BgL_dz00_13, CAR(BgL_expz00_14)),
												BGl_templatezd2orzd2splicezd2listzd2zz__expander_quotez00
												(BgL_dz00_13, CDR(BgL_expz00_14)));
										}
								}
						}
					else
						{	/* Eval/expdquote.scm 160 */
							obj_t BgL_arg1378z00_1289;

							BgL_arg1378z00_1289 =
								BGl_templatezd2orzd2splicez00zz__expander_quotez00(BgL_dz00_13,
								BgL_expz00_14);
							{	/* Eval/expdquote.scm 160 */
								obj_t BgL_list1379z00_1290;

								BgL_list1379z00_1290 =
									MAKE_YOUNG_PAIR(BgL_arg1378z00_1289, BNIL);
								return BgL_list1379z00_1290;
							}
						}
				}
		}

	}



/* template-or-splice */
	obj_t BGl_templatezd2orzd2splicez00zz__expander_quotez00(obj_t BgL_dz00_15,
		obj_t BgL_expz00_16)
	{
		{	/* Eval/expdquote.scm 165 */
			{	/* Eval/expdquote.scm 166 */
				bool_t BgL_test1864z00_2240;

				if (PAIRP(BgL_expz00_16))
					{	/* Eval/expdquote.scm 166 */
						BgL_test1864z00_2240 =
							(CAR(BgL_expz00_16) == BGl_symbol1803z00zz__expander_quotez00);
					}
				else
					{	/* Eval/expdquote.scm 166 */
						BgL_test1864z00_2240 = ((bool_t) 0);
					}
				if (BgL_test1864z00_2240)
					{	/* Eval/expdquote.scm 167 */
						bool_t BgL_test1866z00_2245;

						{	/* Eval/expdquote.scm 167 */
							bool_t BgL_test1867z00_2246;

							{	/* Eval/expdquote.scm 167 */
								obj_t BgL_tmpz00_2247;

								BgL_tmpz00_2247 = CDR(BgL_expz00_16);
								BgL_test1867z00_2246 = PAIRP(BgL_tmpz00_2247);
							}
							if (BgL_test1867z00_2246)
								{	/* Eval/expdquote.scm 167 */
									BgL_test1866z00_2245 = NULLP(CDR(CDR(BgL_expz00_16)));
								}
							else
								{	/* Eval/expdquote.scm 167 */
									BgL_test1866z00_2245 = ((bool_t) 0);
								}
						}
						if (BgL_test1866z00_2245)
							{	/* Eval/expdquote.scm 167 */
								if ((BgL_dz00_15 == BINT(1L)))
									{	/* Eval/expdquote.scm 169 */
										long BgL_arg1392z00_1300;
										obj_t BgL_arg1393z00_1301;

										BgL_arg1392z00_1300 = ((long) CINT(BgL_dz00_15) - 1L);
										BgL_arg1393z00_1301 = CAR(CDR(BgL_expz00_16));
										BGL_TAIL return
											BGl_templatez00zz__expander_quotez00(BINT
											(BgL_arg1392z00_1300), BgL_arg1393z00_1301);
									}
								else
									{	/* Eval/expdquote.scm 171 */
										obj_t BgL_arg1394z00_1302;

										{	/* Eval/expdquote.scm 171 */
											obj_t BgL_arg1397z00_1305;

											{	/* Eval/expdquote.scm 171 */
												long BgL_arg1401z00_1309;
												obj_t BgL_arg1402z00_1310;

												BgL_arg1401z00_1309 = ((long) CINT(BgL_dz00_15) - 1L);
												BgL_arg1402z00_1310 = CAR(CDR(BgL_expz00_16));
												BgL_arg1397z00_1305 =
													BGl_templatez00zz__expander_quotez00(BINT
													(BgL_arg1401z00_1309), BgL_arg1402z00_1310);
											}
											{	/* Eval/expdquote.scm 170 */
												obj_t BgL_list1398z00_1306;

												{	/* Eval/expdquote.scm 170 */
													obj_t BgL_arg1399z00_1307;

													{	/* Eval/expdquote.scm 170 */
														obj_t BgL_arg1400z00_1308;

														BgL_arg1400z00_1308 =
															MAKE_YOUNG_PAIR(BgL_arg1397z00_1305, BNIL);
														BgL_arg1399z00_1307 =
															MAKE_YOUNG_PAIR
															(BGl_list1807z00zz__expander_quotez00,
															BgL_arg1400z00_1308);
													}
													BgL_list1398z00_1306 =
														MAKE_YOUNG_PAIR
														(BGl_symbol1787z00zz__expander_quotez00,
														BgL_arg1399z00_1307);
												}
												BgL_arg1394z00_1302 = BgL_list1398z00_1306;
										}}
										{	/* Eval/expdquote.scm 170 */
											obj_t BgL_list1395z00_1303;

											{	/* Eval/expdquote.scm 170 */
												obj_t BgL_arg1396z00_1304;

												BgL_arg1396z00_1304 =
													MAKE_YOUNG_PAIR(BgL_arg1394z00_1302, BNIL);
												BgL_list1395z00_1303 =
													MAKE_YOUNG_PAIR
													(BGl_symbol1787z00zz__expander_quotez00,
													BgL_arg1396z00_1304);
											}
											return BgL_list1395z00_1303;
										}
									}
							}
						else
							{	/* Eval/expdquote.scm 167 */
								return
									BGl_expandzd2errorzd2zz__expandz00
									(BGl_string1804z00zz__expander_quotez00,
									BGl_string1808z00zz__expander_quotez00, BgL_expz00_16);
							}
					}
				else
					{	/* Eval/expdquote.scm 166 */
						BGL_TAIL return
							BGl_templatez00zz__expander_quotez00(BgL_dz00_15, BgL_expz00_16);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_quotez00(void)
	{
		{	/* Eval/expdquote.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_quotez00(void)
	{
		{	/* Eval/expdquote.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_quotez00(void)
	{
		{	/* Eval/expdquote.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_quotez00(void)
	{
		{	/* Eval/expdquote.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1809z00zz__expander_quotez00));
		}

	}

#ifdef __cplusplus
}
#endif
