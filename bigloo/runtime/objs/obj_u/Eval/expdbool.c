/*===========================================================================*/
/*   (Eval/expdbool.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Eval/expdbool.scm -indent -o objs/obj_u/Eval/expdbool.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___EXPANDER_BOOL_TYPE_DEFINITIONS
#define BGL___EXPANDER_BOOL_TYPE_DEFINITIONS
#endif													// BGL___EXPANDER_BOOL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_list1726z00zz__expander_boolz00 = BUNSPEC;
	extern obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern int BGl_bigloozd2warningzd2zz__paramz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zz__expander_boolz00 = BUNSPEC;
	extern obj_t BGl_expandzd2errorzd2zz__expandz00(obj_t, obj_t, obj_t);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zz__expander_boolz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__expandz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bignumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2condzd2zz__expander_boolz00(obj_t);
	extern obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__expander_boolz00(void);
	static obj_t BGl_genericzd2initzd2zz__expander_boolz00(void);
	extern long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__expander_boolz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__expander_boolz00(void);
	static obj_t BGl_objectzd2initzd2zz__expander_boolz00(void);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__expander_boolz00(void);
	static obj_t BGl_z62expandzd2condzb0zz__expander_boolz00(obj_t, obj_t);
	extern obj_t BGl_evepairifyzd2deepzd2zz__prognz00(obj_t, obj_t);
	static obj_t BGl_symbol1727z00zz__expander_boolz00 = BUNSPEC;
	static obj_t BGl_symbol1731z00zz__expander_boolz00 = BUNSPEC;
	static obj_t BGl_symbol1732z00zz__expander_boolz00 = BUNSPEC;
	static obj_t BGl_symbol1734z00zz__expander_boolz00 = BUNSPEC;
	static obj_t BGl_symbol1736z00zz__expander_boolz00 = BUNSPEC;
	static obj_t BGl_symbol1738z00zz__expander_boolz00 = BUNSPEC;
	static obj_t BGl_symbol1740z00zz__expander_boolz00 = BUNSPEC;
	extern obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2condzd2envz00zz__expander_boolz00,
		BgL_bgl_za762expandza7d2cond1744z00,
		BGl_z62expandzd2condzb0zz__expander_boolz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1728z00zz__expander_boolz00,
		BgL_bgl_string1728za700za7za7_1745za7, "else", 4);
	      DEFINE_STRING(BGl_string1729z00zz__expander_boolz00,
		BgL_bgl_string1729za700za7za7_1746za7, "cond", 4);
	      DEFINE_STRING(BGl_string1730z00zz__expander_boolz00,
		BgL_bgl_string1730za700za7za7_1747za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1733z00zz__expander_boolz00,
		BgL_bgl_string1733za700za7za7_1748za7, "or", 2);
	      DEFINE_STRING(BGl_string1735z00zz__expander_boolz00,
		BgL_bgl_string1735za700za7za7_1749za7, "=>", 2);
	      DEFINE_STRING(BGl_string1737z00zz__expander_boolz00,
		BgL_bgl_string1737za700za7za7_1750za7, "non-user", 8);
	      DEFINE_STRING(BGl_string1739z00zz__expander_boolz00,
		BgL_bgl_string1739za700za7za7_1751za7, "let", 3);
	      DEFINE_STRING(BGl_string1741z00zz__expander_boolz00,
		BgL_bgl_string1741za700za7za7_1752za7, "if", 2);
	      DEFINE_STRING(BGl_string1742z00zz__expander_boolz00,
		BgL_bgl_string1742za700za7za7_1753za7, "ignored COND clauses -- ", 24);
	      DEFINE_STRING(BGl_string1743z00zz__expander_boolz00,
		BgL_bgl_string1743za700za7za7_1754za7, "__expander_bool", 15);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list1726z00zz__expander_boolz00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1727z00zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1731z00zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1732z00zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1734z00zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1736z00zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1738z00zz__expander_boolz00));
		     ADD_ROOT((void *) (&BGl_symbol1740z00zz__expander_boolz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zz__expander_boolz00(long
		BgL_checksumz00_1937, char *BgL_fromz00_1938)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__expander_boolz00))
				{
					BGl_requirezd2initializa7ationz75zz__expander_boolz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__expander_boolz00();
					BGl_cnstzd2initzd2zz__expander_boolz00();
					BGl_importedzd2moduleszd2initz00zz__expander_boolz00();
					return BGl_methodzd2initzd2zz__expander_boolz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__expander_boolz00(void)
	{
		{	/* Eval/expdbool.scm 14 */
			BGl_symbol1727z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1728z00zz__expander_boolz00);
			BGl_list1726z00zz__expander_boolz00 =
				MAKE_YOUNG_PAIR(BGl_symbol1727z00zz__expander_boolz00, BNIL);
			BGl_symbol1731z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1729z00zz__expander_boolz00);
			BGl_symbol1732z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1733z00zz__expander_boolz00);
			BGl_symbol1734z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1735z00zz__expander_boolz00);
			BGl_symbol1736z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1737z00zz__expander_boolz00);
			BGl_symbol1738z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1739z00zz__expander_boolz00);
			return (BGl_symbol1740z00zz__expander_boolz00 =
				bstring_to_symbol(BGl_string1741z00zz__expander_boolz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__expander_boolz00(void)
	{
		{	/* Eval/expdbool.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* expand-cond */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2condzd2zz__expander_boolz00(obj_t
		BgL_expz00_7)
	{
		{	/* Eval/expdbool.scm 74 */
			{	/* Eval/expdbool.scm 75 */
				obj_t BgL_clausesz00_1123;

				BgL_clausesz00_1123 = CDR(((obj_t) BgL_expz00_7));
				{	/* Eval/expdbool.scm 75 */
					obj_t BgL_clause1z00_1124;

					if (PAIRP(BgL_clausesz00_1123))
						{	/* Eval/expdbool.scm 76 */
							BgL_clause1z00_1124 = CAR(BgL_clausesz00_1123);
						}
					else
						{	/* Eval/expdbool.scm 76 */
							BgL_clause1z00_1124 = BNIL;
						}
					{	/* Eval/expdbool.scm 76 */
						obj_t BgL_clause2zb2zb2_1125;

						if (PAIRP(BgL_clause1z00_1124))
							{	/* Eval/expdbool.scm 77 */
								BgL_clause2zb2zb2_1125 = CDR(((obj_t) BgL_clausesz00_1123));
							}
						else
							{	/* Eval/expdbool.scm 77 */
								BgL_clause2zb2zb2_1125 = BFALSE;
							}
						{	/* Eval/expdbool.scm 77 */

							if (NULLP(BgL_clause1z00_1124))
								{	/* Eval/expdbool.scm 79 */
									return BFALSE;
								}
							else
								{	/* Eval/expdbool.scm 81 */
									bool_t BgL_test1759z00_1966;

									if (PAIRP(BgL_clause1z00_1124))
										{	/* Eval/expdbool.scm 81 */
											BgL_test1759z00_1966 =
												BGl_equalzf3zf3zz__r4_equivalence_6_2z00
												(BgL_clause1z00_1124,
												BGl_list1726z00zz__expander_boolz00);
										}
									else
										{	/* Eval/expdbool.scm 81 */
											BgL_test1759z00_1966 = ((bool_t) 1);
										}
									if (BgL_test1759z00_1966)
										{	/* Eval/expdbool.scm 81 */
											return
												BGl_expandzd2errorzd2zz__expandz00
												(BGl_string1729z00zz__expander_boolz00,
												BGl_string1730z00zz__expander_boolz00, BgL_expz00_7);
										}
									else
										{	/* Eval/expdbool.scm 81 */
											if (NULLP(CDR(((obj_t) BgL_clause1z00_1124))))
												{	/* Eval/expdbool.scm 84 */
													obj_t BgL_resz00_1131;

													{	/* Eval/expdbool.scm 84 */
														obj_t BgL_arg1196z00_1139;

														{	/* Eval/expdbool.scm 84 */
															obj_t BgL_arg1197z00_1140;
															obj_t BgL_arg1198z00_1141;

															BgL_arg1197z00_1140 =
																CAR(((obj_t) BgL_clause1z00_1124));
															{	/* Eval/expdbool.scm 84 */
																obj_t BgL_arg1199z00_1142;

																{	/* Eval/expdbool.scm 84 */
																	obj_t BgL_arg1200z00_1143;

																	BgL_arg1200z00_1143 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_clause2zb2zb2_1125, BNIL);
																	BgL_arg1199z00_1142 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol1731z00zz__expander_boolz00,
																		BgL_arg1200z00_1143);
																}
																BgL_arg1198z00_1141 =
																	MAKE_YOUNG_PAIR(BgL_arg1199z00_1142, BNIL);
															}
															BgL_arg1196z00_1139 =
																MAKE_YOUNG_PAIR(BgL_arg1197z00_1140,
																BgL_arg1198z00_1141);
														}
														BgL_resz00_1131 =
															MAKE_YOUNG_PAIR
															(BGl_symbol1732z00zz__expander_boolz00,
															BgL_arg1196z00_1139);
													}
													{	/* Eval/expdbool.scm 85 */
														bool_t BgL_test1762z00_1982;

														{	/* Eval/expdbool.scm 85 */
															obj_t BgL_arg1194z00_1138;

															BgL_arg1194z00_1138 =
																CAR(((obj_t) BgL_clause1z00_1124));
															BgL_test1762z00_1982 =
																EPAIRP(BgL_arg1194z00_1138);
														}
														if (BgL_test1762z00_1982)
															{	/* Eval/expdbool.scm 86 */
																obj_t BgL_arg1189z00_1134;
																obj_t BgL_arg1190z00_1135;
																obj_t BgL_arg1191z00_1136;

																BgL_arg1189z00_1134 = CAR(BgL_resz00_1131);
																BgL_arg1190z00_1135 = CDR(BgL_resz00_1131);
																{	/* Eval/expdbool.scm 86 */
																	obj_t BgL_objz00_1684;

																	BgL_objz00_1684 =
																		CAR(((obj_t) BgL_clause1z00_1124));
																	BgL_arg1191z00_1136 = CER(BgL_objz00_1684);
																}
																{	/* Eval/expdbool.scm 86 */
																	obj_t BgL_res1717z00_1685;

																	BgL_res1717z00_1685 =
																		MAKE_YOUNG_EPAIR(BgL_arg1189z00_1134,
																		BgL_arg1190z00_1135, BgL_arg1191z00_1136);
																	return BgL_res1717z00_1685;
																}
															}
														else
															{	/* Eval/expdbool.scm 85 */
																return
																	BGl_evepairifyzd2deepzd2zz__prognz00
																	(BgL_resz00_1131, BgL_expz00_7);
															}
													}
												}
											else
												{	/* Eval/expdbool.scm 88 */
													bool_t BgL_test1763z00_1993;

													{	/* Eval/expdbool.scm 88 */
														bool_t BgL_test1764z00_1994;

														{	/* Eval/expdbool.scm 88 */
															obj_t BgL_tmpz00_1995;

															{	/* Eval/expdbool.scm 88 */
																obj_t BgL_pairz00_1689;

																BgL_pairz00_1689 =
																	CDR(((obj_t) BgL_clause1z00_1124));
																BgL_tmpz00_1995 = CAR(BgL_pairz00_1689);
															}
															BgL_test1764z00_1994 =
																(BgL_tmpz00_1995 ==
																BGl_symbol1734z00zz__expander_boolz00);
														}
														if (BgL_test1764z00_1994)
															{	/* Eval/expdbool.scm 88 */
																BgL_test1763z00_1993 =
																	(bgl_list_length(BgL_clause1z00_1124) == 3L);
															}
														else
															{	/* Eval/expdbool.scm 88 */
																BgL_test1763z00_1993 = ((bool_t) 0);
															}
													}
													if (BgL_test1763z00_1993)
														{	/* Eval/expdbool.scm 89 */
															obj_t BgL_auxz00_1149;

															{	/* Eval/expdbool.scm 55 */
																obj_t BgL_symbolz00_1691;

																{	/* Eval/expdbool.scm 55 */

																	{	/* Eval/expdbool.scm 55 */

																		BgL_symbolz00_1691 =
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(BFALSE);
																	}
																}
																BGl_putpropz12z12zz__r4_symbols_6_4z00
																	(BgL_symbolz00_1691,
																	BGl_symbol1736z00zz__expander_boolz00, BTRUE);
																BgL_auxz00_1149 = BgL_symbolz00_1691;
															}
															{	/* Eval/expdbool.scm 89 */
																obj_t BgL_testz00_1150;

																{	/* Eval/expdbool.scm 55 */
																	obj_t BgL_symbolz00_1693;

																	{	/* Eval/expdbool.scm 55 */

																		{	/* Eval/expdbool.scm 55 */

																			BgL_symbolz00_1693 =
																				BGl_gensymz00zz__r4_symbols_6_4z00
																				(BFALSE);
																		}
																	}
																	BGl_putpropz12z12zz__r4_symbols_6_4z00
																		(BgL_symbolz00_1693,
																		BGl_symbol1736z00zz__expander_boolz00,
																		BTRUE);
																	BgL_testz00_1150 = BgL_symbolz00_1693;
																}
																{	/* Eval/expdbool.scm 90 */
																	obj_t BgL_resz00_1151;

																	{	/* Eval/expdbool.scm 91 */
																		obj_t BgL_arg1219z00_1159;

																		{	/* Eval/expdbool.scm 91 */
																			obj_t BgL_arg1220z00_1160;
																			obj_t BgL_arg1221z00_1161;

																			{	/* Eval/expdbool.scm 91 */
																				obj_t BgL_arg1223z00_1162;

																				{	/* Eval/expdbool.scm 91 */
																					obj_t BgL_arg1225z00_1163;

																					{	/* Eval/expdbool.scm 91 */
																						obj_t BgL_arg1226z00_1164;

																						BgL_arg1226z00_1164 =
																							CAR(
																							((obj_t) BgL_clause1z00_1124));
																						BgL_arg1225z00_1163 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1226z00_1164, BNIL);
																					}
																					BgL_arg1223z00_1162 =
																						MAKE_YOUNG_PAIR(BgL_testz00_1150,
																						BgL_arg1225z00_1163);
																				}
																				BgL_arg1220z00_1160 =
																					MAKE_YOUNG_PAIR(BgL_arg1223z00_1162,
																					BNIL);
																			}
																			{	/* Eval/expdbool.scm 93 */
																				obj_t BgL_arg1227z00_1165;

																				{	/* Eval/expdbool.scm 93 */
																					obj_t BgL_arg1228z00_1166;

																					{	/* Eval/expdbool.scm 93 */
																						obj_t BgL_arg1229z00_1167;

																						{	/* Eval/expdbool.scm 93 */
																							obj_t BgL_arg1230z00_1168;
																							obj_t BgL_arg1231z00_1169;

																							{	/* Eval/expdbool.scm 93 */
																								obj_t BgL_arg1232z00_1170;

																								{	/* Eval/expdbool.scm 93 */
																									obj_t BgL_arg1233z00_1171;
																									obj_t BgL_arg1234z00_1172;

																									{	/* Eval/expdbool.scm 93 */
																										obj_t BgL_arg1236z00_1173;

																										{	/* Eval/expdbool.scm 93 */
																											obj_t BgL_arg1238z00_1174;

																											BgL_arg1238z00_1174 =
																												MAKE_YOUNG_PAIR
																												(BgL_testz00_1150,
																												BNIL);
																											BgL_arg1236z00_1173 =
																												MAKE_YOUNG_PAIR
																												(BgL_auxz00_1149,
																												BgL_arg1238z00_1174);
																										}
																										BgL_arg1233z00_1171 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1236z00_1173,
																											BNIL);
																									}
																									{	/* Eval/expdbool.scm 94 */
																										obj_t BgL_arg1239z00_1175;

																										{	/* Eval/expdbool.scm 94 */
																											obj_t BgL_arg1242z00_1176;
																											obj_t BgL_arg1244z00_1177;

																											{	/* Eval/expdbool.scm 94 */
																												obj_t BgL_pairz00_1701;

																												{	/* Eval/expdbool.scm 94 */
																													obj_t
																														BgL_pairz00_1700;
																													BgL_pairz00_1700 =
																														CDR(((obj_t)
																															BgL_clause1z00_1124));
																													BgL_pairz00_1701 =
																														CDR
																														(BgL_pairz00_1700);
																												}
																												BgL_arg1242z00_1176 =
																													CAR(BgL_pairz00_1701);
																											}
																											BgL_arg1244z00_1177 =
																												MAKE_YOUNG_PAIR
																												(BgL_auxz00_1149, BNIL);
																											BgL_arg1239z00_1175 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1242z00_1176,
																												BgL_arg1244z00_1177);
																										}
																										BgL_arg1234z00_1172 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1239z00_1175,
																											BNIL);
																									}
																									BgL_arg1232z00_1170 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1233z00_1171,
																										BgL_arg1234z00_1172);
																								}
																								BgL_arg1230z00_1168 =
																									MAKE_YOUNG_PAIR
																									(BGl_symbol1738z00zz__expander_boolz00,
																									BgL_arg1232z00_1170);
																							}
																							{	/* Eval/expdbool.scm 95 */
																								obj_t BgL_arg1248z00_1178;

																								{	/* Eval/expdbool.scm 95 */
																									obj_t BgL_arg1249z00_1179;

																									BgL_arg1249z00_1179 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_clause2zb2zb2_1125,
																										BNIL);
																									BgL_arg1248z00_1178 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol1731z00zz__expander_boolz00,
																										BgL_arg1249z00_1179);
																								}
																								BgL_arg1231z00_1169 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1248z00_1178, BNIL);
																							}
																							BgL_arg1229z00_1167 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1230z00_1168,
																								BgL_arg1231z00_1169);
																						}
																						BgL_arg1228z00_1166 =
																							MAKE_YOUNG_PAIR(BgL_testz00_1150,
																							BgL_arg1229z00_1167);
																					}
																					BgL_arg1227z00_1165 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol1740z00zz__expander_boolz00,
																						BgL_arg1228z00_1166);
																				}
																				BgL_arg1221z00_1161 =
																					MAKE_YOUNG_PAIR(BgL_arg1227z00_1165,
																					BNIL);
																			}
																			BgL_arg1219z00_1159 =
																				MAKE_YOUNG_PAIR(BgL_arg1220z00_1160,
																				BgL_arg1221z00_1161);
																		}
																		BgL_resz00_1151 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol1738z00zz__expander_boolz00,
																			BgL_arg1219z00_1159);
																	}
																	{	/* Eval/expdbool.scm 91 */

																		{	/* Eval/expdbool.scm 96 */
																			bool_t BgL_test1765z00_2032;

																			{	/* Eval/expdbool.scm 96 */
																				obj_t BgL_arg1218z00_1158;

																				BgL_arg1218z00_1158 =
																					CAR(((obj_t) BgL_clause1z00_1124));
																				BgL_test1765z00_2032 =
																					EPAIRP(BgL_arg1218z00_1158);
																			}
																			if (BgL_test1765z00_2032)
																				{	/* Eval/expdbool.scm 97 */
																					obj_t BgL_arg1210z00_1154;
																					obj_t BgL_arg1212z00_1155;
																					obj_t BgL_arg1215z00_1156;

																					BgL_arg1210z00_1154 =
																						CAR(BgL_resz00_1151);
																					BgL_arg1212z00_1155 =
																						CDR(BgL_resz00_1151);
																					{	/* Eval/expdbool.scm 97 */
																						obj_t BgL_objz00_1706;

																						BgL_objz00_1706 =
																							CAR(
																							((obj_t) BgL_clause1z00_1124));
																						BgL_arg1215z00_1156 =
																							CER(BgL_objz00_1706);
																					}
																					{	/* Eval/expdbool.scm 97 */
																						obj_t BgL_res1718z00_1707;

																						BgL_res1718z00_1707 =
																							MAKE_YOUNG_EPAIR
																							(BgL_arg1210z00_1154,
																							BgL_arg1212z00_1155,
																							BgL_arg1215z00_1156);
																						return BgL_res1718z00_1707;
																					}
																				}
																			else
																				{	/* Eval/expdbool.scm 96 */
																					return
																						BGl_evepairifyzd2deepzd2zz__prognz00
																						(BgL_resz00_1151, BgL_expz00_7);
																				}
																		}
																	}
																}
															}
														}
													else
														{	/* Eval/expdbool.scm 88 */
															if (
																(CAR(
																		((obj_t) BgL_clause1z00_1124)) ==
																	BGl_symbol1727z00zz__expander_boolz00))
																{	/* Eval/expdbool.scm 99 */
																	{	/* Eval/expdbool.scm 100 */
																		bool_t BgL_test1767z00_2047;

																		if (PAIRP(BgL_clause2zb2zb2_1125))
																			{	/* Eval/expdbool.scm 100 */
																				int BgL_a1086z00_1191;

																				BgL_a1086z00_1191 =
																					BGl_bigloozd2warningzd2zz__paramz00();
																				{	/* Eval/expdbool.scm 100 */

																					BgL_test1767z00_2047 =
																						((long) (BgL_a1086z00_1191) > 0L);
																			}}
																		else
																			{	/* Eval/expdbool.scm 100 */
																				BgL_test1767z00_2047 = ((bool_t) 0);
																			}
																		if (BgL_test1767z00_2047)
																			{	/* Eval/expdbool.scm 101 */
																				obj_t BgL_list1257z00_1187;

																				{	/* Eval/expdbool.scm 101 */
																					obj_t BgL_arg1268z00_1188;

																					{	/* Eval/expdbool.scm 101 */
																						obj_t BgL_arg1272z00_1189;

																						BgL_arg1272z00_1189 =
																							MAKE_YOUNG_PAIR
																							(BgL_clause2zb2zb2_1125, BNIL);
																						BgL_arg1268z00_1188 =
																							MAKE_YOUNG_PAIR
																							(BGl_string1742z00zz__expander_boolz00,
																							BgL_arg1272z00_1189);
																					}
																					BgL_list1257z00_1187 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1729z00zz__expander_boolz00,
																						BgL_arg1268z00_1188);
																				}
																				BGl_warningz00zz__errorz00
																					(BgL_list1257z00_1187);
																			}
																		else
																			{	/* Eval/expdbool.scm 100 */
																				BFALSE;
																			}
																	}
																	{	/* Eval/expdbool.scm 102 */
																		obj_t BgL_arg1284z00_1194;

																		BgL_arg1284z00_1194 =
																			CDR(((obj_t) BgL_clause1z00_1124));
																		return
																			BGl_expandzd2prognzd2zz__prognz00
																			(BgL_arg1284z00_1194);
																	}
																}
															else
																{	/* Eval/expdbool.scm 104 */
																	obj_t BgL_ncondz00_1195;

																	{	/* Eval/expdbool.scm 104 */
																		obj_t BgL_ncz00_1228;

																		{	/* Eval/expdbool.scm 104 */
																			obj_t BgL_arg1347z00_1249;

																			BgL_arg1347z00_1249 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_clause2zb2zb2_1125, BNIL);
																			BgL_ncz00_1228 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol1731z00zz__expander_boolz00,
																				BgL_arg1347z00_1249);
																		}
																		{	/* Eval/expdbool.scm 106 */
																			bool_t BgL_test1769z00_2062;

																			if (PAIRP(BgL_clause2zb2zb2_1125))
																				{	/* Eval/expdbool.scm 106 */
																					obj_t BgL_tmpz00_2065;

																					BgL_tmpz00_2065 =
																						CAR(BgL_clause2zb2zb2_1125);
																					BgL_test1769z00_2062 =
																						EPAIRP(BgL_tmpz00_2065);
																				}
																			else
																				{	/* Eval/expdbool.scm 106 */
																					BgL_test1769z00_2062 = ((bool_t) 0);
																				}
																			if (BgL_test1769z00_2062)
																				{	/* Eval/expdbool.scm 107 */
																					obj_t BgL_arg1329z00_1232;
																					obj_t BgL_arg1331z00_1233;
																					obj_t BgL_arg1332z00_1234;

																					BgL_arg1329z00_1232 =
																						CAR(BgL_ncz00_1228);
																					BgL_arg1331z00_1233 =
																						CDR(BgL_ncz00_1228);
																					BgL_arg1332z00_1234 =
																						CER(CAR(BgL_clause2zb2zb2_1125));
																					{	/* Eval/expdbool.scm 107 */
																						obj_t BgL_res1719z00_1717;

																						BgL_res1719z00_1717 =
																							MAKE_YOUNG_EPAIR
																							(BgL_arg1329z00_1232,
																							BgL_arg1331z00_1233,
																							BgL_arg1332z00_1234);
																						BgL_ncondz00_1195 =
																							BgL_res1719z00_1717;
																					}
																				}
																			else
																				{	/* Eval/expdbool.scm 108 */
																					bool_t BgL_test1771z00_2073;

																					{	/* Eval/expdbool.scm 108 */
																						obj_t BgL_arg1344z00_1246;

																						BgL_arg1344z00_1246 =
																							CAR(
																							((obj_t) BgL_clausesz00_1123));
																						BgL_test1771z00_2073 =
																							EPAIRP(BgL_arg1344z00_1246);
																					}
																					if (BgL_test1771z00_2073)
																						{	/* Eval/expdbool.scm 109 */
																							obj_t BgL_arg1336z00_1238;
																							obj_t BgL_arg1337z00_1239;
																							obj_t BgL_arg1338z00_1240;

																							BgL_arg1336z00_1238 =
																								CAR(BgL_ncz00_1228);
																							BgL_arg1337z00_1239 =
																								CDR(BgL_ncz00_1228);
																							{	/* Eval/expdbool.scm 109 */
																								obj_t BgL_objz00_1722;

																								BgL_objz00_1722 =
																									CAR(
																									((obj_t)
																										BgL_clausesz00_1123));
																								BgL_arg1338z00_1240 =
																									CER(BgL_objz00_1722);
																							}
																							{	/* Eval/expdbool.scm 109 */
																								obj_t BgL_res1720z00_1723;

																								BgL_res1720z00_1723 =
																									MAKE_YOUNG_EPAIR
																									(BgL_arg1336z00_1238,
																									BgL_arg1337z00_1239,
																									BgL_arg1338z00_1240);
																								BgL_ncondz00_1195 =
																									BgL_res1720z00_1723;
																							}
																						}
																					else
																						{	/* Eval/expdbool.scm 108 */
																							if (EPAIRP(BgL_clausesz00_1123))
																								{	/* Eval/expdbool.scm 111 */
																									obj_t BgL_arg1341z00_1243;
																									obj_t BgL_arg1342z00_1244;
																									obj_t BgL_arg1343z00_1245;

																									BgL_arg1341z00_1243 =
																										CAR(BgL_ncz00_1228);
																									BgL_arg1342z00_1244 =
																										CDR(BgL_ncz00_1228);
																									BgL_arg1343z00_1245 =
																										CER(
																										((obj_t)
																											BgL_clausesz00_1123));
																									{	/* Eval/expdbool.scm 111 */
																										obj_t BgL_res1721z00_1727;

																										BgL_res1721z00_1727 =
																											MAKE_YOUNG_EPAIR
																											(BgL_arg1341z00_1243,
																											BgL_arg1342z00_1244,
																											BgL_arg1343z00_1245);
																										BgL_ncondz00_1195 =
																											BgL_res1721z00_1727;
																									}
																								}
																							else
																								{	/* Eval/expdbool.scm 110 */
																									BgL_ncondz00_1195 =
																										BgL_ncz00_1228;
																								}
																						}
																				}
																		}
																	}
																	{	/* Eval/expdbool.scm 104 */
																		obj_t BgL_locz00_1196;

																		if (EPAIRP(BgL_expz00_7))
																			{	/* Eval/expdbool.scm 114 */
																				BgL_locz00_1196 =
																					CER(((obj_t) BgL_expz00_7));
																			}
																		else
																			{	/* Eval/expdbool.scm 114 */
																				BgL_locz00_1196 = BFALSE;
																			}
																		{	/* Eval/expdbool.scm 114 */
																			obj_t BgL_loc1z00_1197;

																			if (EPAIRP(BgL_clause1z00_1124))
																				{	/* Eval/expdbool.scm 115 */
																					BgL_loc1z00_1197 =
																						CER(((obj_t) BgL_clause1z00_1124));
																				}
																			else
																				{	/* Eval/expdbool.scm 115 */
																					BgL_loc1z00_1197 = BFALSE;
																				}
																			{	/* Eval/expdbool.scm 115 */
																				obj_t BgL_loc1zd2carzd2_1198;

																				{	/* Eval/expdbool.scm 116 */
																					bool_t BgL_test1775z00_2098;

																					{	/* Eval/expdbool.scm 116 */
																						obj_t BgL_arg1323z00_1225;

																						BgL_arg1323z00_1225 =
																							CAR(
																							((obj_t) BgL_clause1z00_1124));
																						BgL_test1775z00_2098 =
																							EPAIRP(BgL_arg1323z00_1225);
																					}
																					if (BgL_test1775z00_2098)
																						{	/* Eval/expdbool.scm 116 */
																							obj_t BgL_objz00_1732;

																							BgL_objz00_1732 =
																								CAR(
																								((obj_t) BgL_clause1z00_1124));
																							BgL_loc1zd2carzd2_1198 =
																								CER(BgL_objz00_1732);
																						}
																					else
																						{	/* Eval/expdbool.scm 116 */
																							BgL_loc1zd2carzd2_1198 = BFALSE;
																						}
																				}
																				{	/* Eval/expdbool.scm 116 */
																					obj_t BgL_loc1zd2cdrzd2_1199;

																					{	/* Eval/expdbool.scm 117 */
																						bool_t BgL_test1776z00_2105;

																						{	/* Eval/expdbool.scm 117 */
																							obj_t BgL_arg1319z00_1221;

																							BgL_arg1319z00_1221 =
																								CDR(
																								((obj_t) BgL_clause1z00_1124));
																							BgL_test1776z00_2105 =
																								EPAIRP(BgL_arg1319z00_1221);
																						}
																						if (BgL_test1776z00_2105)
																							{	/* Eval/expdbool.scm 117 */
																								obj_t BgL_objz00_1735;

																								BgL_objz00_1735 =
																									CDR(
																									((obj_t)
																										BgL_clause1z00_1124));
																								BgL_loc1zd2cdrzd2_1199 =
																									CER(BgL_objz00_1735);
																							}
																						else
																							{	/* Eval/expdbool.scm 117 */
																								BgL_loc1zd2cdrzd2_1199 = BFALSE;
																							}
																					}
																					{	/* Eval/expdbool.scm 117 */
																						obj_t BgL_locnz00_1200;

																						if (EPAIRP(BgL_clause2zb2zb2_1125))
																							{	/* Eval/expdbool.scm 118 */
																								BgL_locnz00_1200 =
																									CER(
																									((obj_t)
																										BgL_clause2zb2zb2_1125));
																							}
																						else
																							{	/* Eval/expdbool.scm 118 */
																								BgL_locnz00_1200 = BFALSE;
																							}
																						{	/* Eval/expdbool.scm 118 */

																							{	/* Eval/expdbool.scm 120 */
																								obj_t BgL_arg1304z00_1201;
																								obj_t BgL_arg1305z00_1202;

																								{	/* Eval/expdbool.scm 120 */
																									obj_t BgL_arg1306z00_1203;
																									obj_t BgL_arg1307z00_1204;
																									obj_t BgL_arg1308z00_1205;

																									BgL_arg1306z00_1203 =
																										CAR(
																										((obj_t)
																											BgL_clause1z00_1124));
																									{	/* Eval/expdbool.scm 121 */
																										obj_t BgL_arg1309z00_1206;
																										obj_t BgL_arg1310z00_1207;
																										obj_t BgL_arg1311z00_1208;

																										{	/* Eval/expdbool.scm 121 */
																											obj_t BgL_arg1312z00_1209;

																											BgL_arg1312z00_1209 =
																												CDR(
																												((obj_t)
																													BgL_clause1z00_1124));
																											BgL_arg1309z00_1206 =
																												BGl_expandzd2prognzd2zz__prognz00
																												(BgL_arg1312z00_1209);
																										}
																										{	/* Eval/expdbool.scm 125 */
																											obj_t BgL_arg1314z00_1210;

																											if (CBOOL
																												(BgL_locnz00_1200))
																												{	/* Eval/expdbool.scm 125 */
																													BgL_arg1314z00_1210 =
																														BgL_locnz00_1200;
																												}
																											else
																												{	/* Eval/expdbool.scm 125 */
																													if (CBOOL
																														(BgL_loc1z00_1197))
																														{	/* Eval/expdbool.scm 125 */
																															BgL_arg1314z00_1210
																																=
																																BgL_loc1z00_1197;
																														}
																													else
																														{	/* Eval/expdbool.scm 125 */
																															BgL_arg1314z00_1210
																																=
																																BgL_locz00_1196;
																														}
																												}
																											if (CBOOL
																												(BgL_arg1314z00_1210))
																												{	/* Eval/expdbool.scm 68 */
																													obj_t
																														BgL_res1722z00_1739;
																													BgL_res1722z00_1739 =
																														MAKE_YOUNG_EPAIR
																														(BgL_ncondz00_1195,
																														BNIL,
																														BgL_arg1314z00_1210);
																													BgL_arg1310z00_1207 =
																														BgL_res1722z00_1739;
																												}
																											else
																												{	/* Eval/expdbool.scm 67 */
																													BgL_arg1310z00_1207 =
																														MAKE_YOUNG_PAIR
																														(BgL_ncondz00_1195,
																														BNIL);
																												}
																										}
																										if (CBOOL
																											(BgL_loc1zd2cdrzd2_1199))
																											{	/* Eval/expdbool.scm 126 */
																												BgL_arg1311z00_1208 =
																													BgL_loc1zd2cdrzd2_1199;
																											}
																										else
																											{	/* Eval/expdbool.scm 126 */
																												if (CBOOL
																													(BgL_loc1z00_1197))
																													{	/* Eval/expdbool.scm 126 */
																														BgL_arg1311z00_1208
																															=
																															BgL_loc1z00_1197;
																													}
																												else
																													{	/* Eval/expdbool.scm 126 */
																														BgL_arg1311z00_1208
																															= BgL_locz00_1196;
																													}
																											}
																										if (CBOOL
																											(BgL_arg1311z00_1208))
																											{	/* Eval/expdbool.scm 68 */
																												obj_t
																													BgL_res1723z00_1740;
																												BgL_res1723z00_1740 =
																													MAKE_YOUNG_EPAIR
																													(BgL_arg1309z00_1206,
																													BgL_arg1310z00_1207,
																													BgL_arg1311z00_1208);
																												BgL_arg1307z00_1204 =
																													BgL_res1723z00_1740;
																											}
																										else
																											{	/* Eval/expdbool.scm 67 */
																												BgL_arg1307z00_1204 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1309z00_1206,
																													BgL_arg1310z00_1207);
																											}
																									}
																									if (CBOOL
																										(BgL_loc1zd2carzd2_1198))
																										{	/* Eval/expdbool.scm 127 */
																											BgL_arg1308z00_1205 =
																												BgL_loc1zd2carzd2_1198;
																										}
																									else
																										{	/* Eval/expdbool.scm 127 */
																											BgL_arg1308z00_1205 =
																												BgL_locz00_1196;
																										}
																									if (CBOOL
																										(BgL_arg1308z00_1205))
																										{	/* Eval/expdbool.scm 68 */
																											obj_t BgL_res1724z00_1741;

																											BgL_res1724z00_1741 =
																												MAKE_YOUNG_EPAIR
																												(BgL_arg1306z00_1203,
																												BgL_arg1307z00_1204,
																												BgL_arg1308z00_1205);
																											BgL_arg1304z00_1201 =
																												BgL_res1724z00_1741;
																										}
																									else
																										{	/* Eval/expdbool.scm 67 */
																											BgL_arg1304z00_1201 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1306z00_1203,
																												BgL_arg1307z00_1204);
																										}
																								}
																								if (CBOOL(BgL_loc1z00_1197))
																									{	/* Eval/expdbool.scm 128 */
																										BgL_arg1305z00_1202 =
																											BgL_loc1z00_1197;
																									}
																								else
																									{	/* Eval/expdbool.scm 128 */
																										BgL_arg1305z00_1202 =
																											BgL_locz00_1196;
																									}
																								{	/* Eval/expdbool.scm 119 */
																									obj_t BgL_az00_1742;

																									BgL_az00_1742 =
																										BGl_symbol1740z00zz__expander_boolz00;
																									if (CBOOL
																										(BgL_arg1305z00_1202))
																										{	/* Eval/expdbool.scm 68 */
																											obj_t BgL_res1725z00_1743;

																											BgL_res1725z00_1743 =
																												MAKE_YOUNG_EPAIR
																												(BgL_az00_1742,
																												BgL_arg1304z00_1201,
																												BgL_arg1305z00_1202);
																											return
																												BgL_res1725z00_1743;
																										}
																									else
																										{	/* Eval/expdbool.scm 67 */
																											return
																												MAKE_YOUNG_PAIR
																												(BgL_az00_1742,
																												BgL_arg1304z00_1201);
																										}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
														}
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* &expand-cond */
	obj_t BGl_z62expandzd2condzb0zz__expander_boolz00(obj_t BgL_envz00_1928,
		obj_t BgL_expz00_1929)
	{
		{	/* Eval/expdbool.scm 74 */
			return BGl_expandzd2condzd2zz__expander_boolz00(BgL_expz00_1929);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__expander_boolz00(void)
	{
		{	/* Eval/expdbool.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__expander_boolz00(void)
	{
		{	/* Eval/expdbool.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__expander_boolz00(void)
	{
		{	/* Eval/expdbool.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__expander_boolz00(void)
	{
		{	/* Eval/expdbool.scm 14 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__biglooz00(536217185L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__structurez00(128218210L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__bignumz00(6519932L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(481261960L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(54297095L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(237870368L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00
				(268155843L, BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(198620800L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(473309430L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(459519430L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(-44273703L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(9614678L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(376927882L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(43892022L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			BGl_modulezd2initializa7ationz75zz__prognz00(177147622L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
			return BGl_modulezd2initializa7ationz75zz__expandz00(414007149L,
				BSTRING_TO_STRING(BGl_string1743z00zz__expander_boolz00));
		}

	}

#ifdef __cplusplus
}
#endif
