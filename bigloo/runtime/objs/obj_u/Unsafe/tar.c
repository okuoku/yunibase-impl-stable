/*===========================================================================*/
/*   (Unsafe/tar.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/tar.scm -indent -o objs/obj_u/Unsafe/tar.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___TAR_TYPE_DEFINITIONS
#define BGL___TAR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62iozd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                       *BgL_z62iozd2errorzb0_bglt;

	typedef struct BgL_z62iozd2parsezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2parsezd2errorz62_bglt;

	typedef struct BgL_tarzd2headerzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_namez00;
		long BgL_modez00;
		long BgL_uidz00;
		long BgL_gidz00;
		long BgL_siza7eza7;
		obj_t BgL_mtimez00;
		long BgL_checksumz00;
		obj_t BgL_typez00;
		obj_t BgL_linknamez00;
		obj_t BgL_magicz00;
		obj_t BgL_unamez00;
		obj_t BgL_gnamez00;
		long BgL_devmajorz00;
		long BgL_devminorz00;
	}                      *BgL_tarzd2headerzd2_bglt;


#endif													// BGL___TAR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2002z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2004z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2006z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2008z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_untarzd2directoryzd2zz__tarz00(obj_t, obj_t);
	extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	static long BGl_checksumz00zz__tarz00(obj_t);
	static obj_t BGl_symbol2010z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2012z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2014z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2016z00zz__tarz00 = BUNSPEC;
	extern obj_t bgl_directory_to_list(char *);
	static obj_t BGl_symbol2018z00zz__tarz00 = BUNSPEC;
	static obj_t BGl__tarzd2readzd2blockz00zz__tarz00(obj_t, obj_t);
	extern bool_t BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__tarz00 = BUNSPEC;
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	extern obj_t BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t string_for_read(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__tarz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_tarzd2errorzd2zz__tarz00(obj_t, obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_symbol2039z00zz__tarz00 = BUNSPEC;
	extern obj_t bgl_nanoseconds_to_date(BGL_LONGLONG_T);
	extern bool_t fexists(char *);
	extern obj_t bgl_seconds_to_date(long);
	static obj_t BGl__tarzd2readzd2headerz00zz__tarz00(obj_t, obj_t);
	static obj_t BGl_symbol2050z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2054z00zz__tarz00 = BUNSPEC;
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_symbol1966z00zz__tarz00 = BUNSPEC;
	static BgL_tarzd2headerzd2_bglt BGl_z62lambda1500z62zz__tarz00(obj_t);
	static obj_t BGl_symbol1968z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_z62lambda1507z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1508z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__tarz00(void);
	extern bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
	static obj_t BGl_genericzd2initzd2zz__tarz00(void);
	static obj_t BGl_symbol2061z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2065z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_list2032z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2069z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1970z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1972z00zz__tarz00 = BUNSPEC;
	extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_untarzd2fileszd2zz__tarz00(obj_t, obj_t);
	static obj_t BGl_symbol1974z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zz__tarz00(void);
	static obj_t BGl_symbol1976z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1978z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__tarz00(void);
	static obj_t BGl_z62lambda1512z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1513z62zz__tarz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_z62iozd2errorzb0zz__objectz00;
	static obj_t BGl_z62lambda1518z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1519z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__tarz00(void);
	static obj_t BGl_symbol2073z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2075z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1980z00zz__tarz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_tarzd2readzd2blockz00zz__tarz00(obj_t, obj_t);
	static obj_t BGl_extractze70ze7zz__tarz00(obj_t, long, obj_t, obj_t, obj_t,
		obj_t, long);
	static obj_t BGl_symbol1982z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1984z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1986z00zz__tarz00 = BUNSPEC;
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62lambda1524z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1525z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__tarz00(obj_t);
	extern obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_z62lambda1529z62zz__tarz00(obj_t, obj_t);
	static obj_t
		BGl_z62tarzd2roundzd2upzd2tozd2recordzd2siza7ez17zz__tarz00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_dirnamez00zz__osz00(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_pwdz00zz__osz00(void);
	static obj_t BGl_symbol1994z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol1996z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_z62lambda1530z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1998z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_z62lambda1537z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1538z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2092z00zz__tarz00 = BUNSPEC;
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static obj_t BGl_symbol2093z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_symbol2096z00zz__tarz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_untarz00zz__tarz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1544z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1545z62zz__tarz00(obj_t, obj_t, obj_t);
	extern bool_t bgl_directoryp(char *);
	extern obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	extern BGL_LONGLONG_T bgl_current_nanoseconds(void);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_tarzd2typezd2namez00zz__tarz00(unsigned char);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62lambda1550z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1551z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1555z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1556z62zz__tarz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_readzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__tarz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static bool_t BGl_rmzd2rfzd2zz__tarz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_tarzd2readzd2headerz00zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31502ze3ze5zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1560z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1561z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1566z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1567z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl__untarz00zz__tarz00(obj_t, obj_t);
	extern int bgl_symlink(char *, char *);
	static obj_t BGl_z62lambda1576z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1577z62zz__tarz00(obj_t, obj_t, obj_t);
	static BgL_tarzd2headerzd2_bglt BGl_z62lambda1498z62zz__tarz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_tarzd2headerzd2zz__tarz00 = BUNSPEC;
	extern obj_t bgl_file_type(char *);
	static obj_t BGl_z62lambda1581z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1582z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1586z62zz__tarz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1587z62zz__tarz00(obj_t, obj_t, obj_t);
	static obj_t BGl_keyword2033z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_keyword2035z00zz__tarz00 = BUNSPEC;
	static obj_t BGl_keyword2037z00zz__tarz00 = BUNSPEC;
	extern obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_tarzd2roundzd2upzd2tozd2recordzd2siza7ez75zz__tarz00(obj_t);
	extern long BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00(obj_t, long);
	static obj_t BGl_symbol2000z00zz__tarz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2040z00zz__tarz00,
		BgL_bgl_string2040za700za7za7_2098za7, "untar", 5);
	      DEFINE_STRING(BGl_string2041z00zz__tarz00,
		BgL_bgl_string2041za700za7za7_2099za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string2042z00zz__tarz00,
		BgL_bgl_string2042za700za7za7_2100za7,
		"wrong number of arguments: [1..4] expected, provided", 52);
	      DEFINE_STRING(BGl_string2043z00zz__tarz00,
		BgL_bgl_string2043za700za7za7_2101za7, "/tmp/bigloo/runtime/Unsafe/tar.scm",
		34);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_untarzd2envzd2zz__tarz00,
		BgL_bgl__untarza700za7za7__tar2102za7, opt_generic_entry,
		BGl__untarz00zz__tarz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2044z00zz__tarz00,
		BgL_bgl_string2044za700za7za7_2103za7, "_untar", 6);
	      DEFINE_STRING(BGl_string2045z00zz__tarz00,
		BgL_bgl_string2045za700za7za7_2104za7, "bint", 4);
	      DEFINE_STRING(BGl_string2046z00zz__tarz00,
		BgL_bgl_string2046za700za7za7_2105za7, "Cannot create directory", 23);
	      DEFINE_STRING(BGl_string2047z00zz__tarz00,
		BgL_bgl_string2047za700za7za7_2106za7, "Illegal file type `~a'", 22);
	      DEFINE_STRING(BGl_string2051z00zz__tarz00,
		BgL_bgl_string2051za700za7za7_2107za7, "bstring", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2048z00zz__tarz00,
		BgL_bgl_za762lambda1508za7622108z00, BGl_z62lambda1508z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zz__tarz00,
		BgL_bgl_za762lambda1507za7622109z00, BGl_z62lambda1507z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1967z00zz__tarz00,
		BgL_bgl_string1967za700za7za7_2110za7, "tar", 3);
	      DEFINE_STRING(BGl_string1969z00zz__tarz00,
		BgL_bgl_string1969za700za7za7_2111za7, "oldnormal", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tarzd2readzd2headerzd2envzd2zz__tarz00,
		BgL_bgl__tarza7d2readza7d2he2112z00, opt_generic_entry,
		BGl__tarzd2readzd2headerz00zz__tarz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zz__tarz00,
		BgL_bgl_za762lambda1513za7622113z00, BGl_z62lambda1513z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2053z00zz__tarz00,
		BgL_bgl_za762lambda1512za7622114z00, BGl_z62lambda1512z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zz__tarz00,
		BgL_bgl_za762lambda1519za7622115z00, BGl_z62lambda1519z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2062z00zz__tarz00,
		BgL_bgl_string2062za700za7za7_2116za7, "elong", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zz__tarz00,
		BgL_bgl_za762lambda1518za7622117z00, BGl_z62lambda1518z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zz__tarz00,
		BgL_bgl_za762lambda1525za7622118z00, BGl_z62lambda1525z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zz__tarz00,
		BgL_bgl_za762lambda1524za7622119z00, BGl_z62lambda1524z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2059z00zz__tarz00,
		BgL_bgl_za762lambda1530za7622120z00, BGl_z62lambda1530z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2066z00zz__tarz00,
		BgL_bgl_string2066za700za7za7_2121za7, "date", 4);
	      DEFINE_STRING(BGl_string1971z00zz__tarz00,
		BgL_bgl_string1971za700za7za7_2122za7, "normal", 6);
	      DEFINE_STRING(BGl_string1973z00zz__tarz00,
		BgL_bgl_string1973za700za7za7_2123za7, "link", 4);
	      DEFINE_STRING(BGl_string1975z00zz__tarz00,
		BgL_bgl_string1975za700za7za7_2124za7, "symlink", 7);
	      DEFINE_STRING(BGl_string1977z00zz__tarz00,
		BgL_bgl_string1977za700za7za7_2125za7, "chr", 3);
	      DEFINE_STRING(BGl_string1979z00zz__tarz00,
		BgL_bgl_string1979za700za7za7_2126za7, "blk", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zz__tarz00,
		BgL_bgl_za762lambda1529za7622127z00, BGl_z62lambda1529z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zz__tarz00,
		BgL_bgl_za762lambda1538za7622128z00, BGl_z62lambda1538z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2070z00zz__tarz00,
		BgL_bgl_string2070za700za7za7_2129za7, "checksum", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zz__tarz00,
		BgL_bgl_za762lambda1537za7622130z00, BGl_z62lambda1537z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2067z00zz__tarz00,
		BgL_bgl_za762lambda1545za7622131z00, BGl_z62lambda1545z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2074z00zz__tarz00,
		BgL_bgl_string2074za700za7za7_2132za7, "type", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2068z00zz__tarz00,
		BgL_bgl_za762lambda1544za7622133z00, BGl_z62lambda1544z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2076z00zz__tarz00,
		BgL_bgl_string2076za700za7za7_2134za7, "symbol", 6);
	      DEFINE_STRING(BGl_string1981z00zz__tarz00,
		BgL_bgl_string1981za700za7za7_2135za7, "dir", 3);
	      DEFINE_STRING(BGl_string1983z00zz__tarz00,
		BgL_bgl_string1983za700za7za7_2136za7, "fifo", 4);
	      DEFINE_STRING(BGl_string1985z00zz__tarz00,
		BgL_bgl_string1985za700za7za7_2137za7, "contig", 6);
	      DEFINE_STRING(BGl_string1987z00zz__tarz00,
		BgL_bgl_string1987za700za7za7_2138za7, "longname", 8);
	      DEFINE_STRING(BGl_string1988z00zz__tarz00,
		BgL_bgl_string1988za700za7za7_2139za7, "invalid file type", 17);
	      DEFINE_STRING(BGl_string1989z00zz__tarz00,
		BgL_bgl_string1989za700za7za7_2140za7, "invalid octal record item", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zz__tarz00,
		BgL_bgl_za762lambda1551za7622141z00, BGl_z62lambda1551z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2072z00zz__tarz00,
		BgL_bgl_za762lambda1550za7622142z00, BGl_z62lambda1550z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2077z00zz__tarz00,
		BgL_bgl_za762lambda1556za7622143z00, BGl_z62lambda1556z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2078z00zz__tarz00,
		BgL_bgl_za762lambda1555za7622144z00, BGl_z62lambda1555z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2079z00zz__tarz00,
		BgL_bgl_za762lambda1561za7622145z00, BGl_z62lambda1561z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1990z00zz__tarz00,
		BgL_bgl_string1990za700za7za7_2146za7, "        ", 8);
	      DEFINE_STRING(BGl_string1991z00zz__tarz00,
		BgL_bgl_string1991za700za7za7_2147za7, "tar-read-header", 15);
	      DEFINE_STRING(BGl_string1992z00zz__tarz00,
		BgL_bgl_string1992za700za7za7_2148za7, "input-port", 10);
	      DEFINE_STRING(BGl_string1993z00zz__tarz00,
		BgL_bgl_string1993za700za7za7_2149za7, "", 0);
	      DEFINE_STRING(BGl_string1995z00zz__tarz00,
		BgL_bgl_string1995za700za7za7_2150za7, "name", 4);
	      DEFINE_STRING(BGl_string1997z00zz__tarz00,
		BgL_bgl_string1997za700za7za7_2151za7, "mode", 4);
	      DEFINE_STRING(BGl_string1999z00zz__tarz00,
		BgL_bgl_string1999za700za7za7_2152za7, "uid", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2080z00zz__tarz00,
		BgL_bgl_za762lambda1560za7622153z00, BGl_z62lambda1560z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2081z00zz__tarz00,
		BgL_bgl_za762lambda1567za7622154z00, BGl_z62lambda1567z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2082z00zz__tarz00,
		BgL_bgl_za762lambda1566za7622155z00, BGl_z62lambda1566z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2083z00zz__tarz00,
		BgL_bgl_za762lambda1577za7622156z00, BGl_z62lambda1577z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2084z00zz__tarz00,
		BgL_bgl_za762lambda1576za7622157z00, BGl_z62lambda1576z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2085z00zz__tarz00,
		BgL_bgl_za762lambda1582za7622158z00, BGl_z62lambda1582z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2086z00zz__tarz00,
		BgL_bgl_za762lambda1581za7622159z00, BGl_z62lambda1581z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2087z00zz__tarz00,
		BgL_bgl_za762lambda1587za7622160z00, BGl_z62lambda1587z62zz__tarz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2094z00zz__tarz00,
		BgL_bgl_string2094za700za7za7_2161za7, "__tar", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2088z00zz__tarz00,
		BgL_bgl_za762lambda1586za7622162z00, BGl_z62lambda1586z62zz__tarz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2095z00zz__tarz00,
		BgL_bgl_string2095za700za7za7_2163za7, "0", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2089z00zz__tarz00,
		BgL_bgl_za762za7c3za704anonymo2164za7,
		BGl_z62zc3z04anonymousza31502ze3ze5zz__tarz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2097z00zz__tarz00,
		BgL_bgl_string2097za700za7za7_2165za7, "_", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tarzd2readzd2blockzd2envzd2zz__tarz00,
		BgL_bgl__tarza7d2readza7d2bl2166z00, opt_generic_entry,
		BGl__tarzd2readzd2blockz00zz__tarz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2090z00zz__tarz00,
		BgL_bgl_za762lambda1500za7622167z00, BGl_z62lambda1500z62zz__tarz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2091z00zz__tarz00,
		BgL_bgl_za762lambda1498za7622168z00, BGl_z62lambda1498z62zz__tarz00, 0L,
		BUNSPEC, 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tarzd2roundzd2upzd2tozd2recordzd2siza7ezd2envza7zz__tarz00,
		BgL_bgl_za762tarza7d2roundza7d2169za7,
		BGl_z62tarzd2roundzd2upzd2tozd2recordzd2siza7ez17zz__tarz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2001z00zz__tarz00,
		BgL_bgl_string2001za700za7za7_2170za7, "gid", 3);
	      DEFINE_STRING(BGl_string2003z00zz__tarz00,
		BgL_bgl_string2003za700za7za7_2171za7, "size", 4);
	      DEFINE_STRING(BGl_string2005z00zz__tarz00,
		BgL_bgl_string2005za700za7za7_2172za7, "mtime", 5);
	      DEFINE_STRING(BGl_string2007z00zz__tarz00,
		BgL_bgl_string2007za700za7za7_2173za7, "chksum", 6);
	      DEFINE_STRING(BGl_string2009z00zz__tarz00,
		BgL_bgl_string2009za700za7za7_2174za7, "linkname", 8);
	      DEFINE_STRING(BGl_string2011z00zz__tarz00,
		BgL_bgl_string2011za700za7za7_2175za7, "magic", 5);
	      DEFINE_STRING(BGl_string2013z00zz__tarz00,
		BgL_bgl_string2013za700za7za7_2176za7, "uname", 5);
	      DEFINE_STRING(BGl_string2015z00zz__tarz00,
		BgL_bgl_string2015za700za7za7_2177za7, "gname", 5);
	      DEFINE_STRING(BGl_string2017z00zz__tarz00,
		BgL_bgl_string2017za700za7za7_2178za7, "devmajor", 8);
	      DEFINE_STRING(BGl_string2019z00zz__tarz00,
		BgL_bgl_string2019za700za7za7_2179za7, "devminor", 8);
	      DEFINE_STRING(BGl_string2020z00zz__tarz00,
		BgL_bgl_string2020za700za7za7_2180za7, "ustar  ", 7);
	      DEFINE_STRING(BGl_string2021z00zz__tarz00,
		BgL_bgl_string2021za700za7za7_2181za7, "ustar", 5);
	      DEFINE_STRING(BGl_string2022z00zz__tarz00,
		BgL_bgl_string2022za700za7za7_2182za7, "GNUtar ", 7);
	      DEFINE_STRING(BGl_string2023z00zz__tarz00,
		BgL_bgl_string2023za700za7za7_2183za7, "invalid checksum (expected: ~s)",
		31);
	      DEFINE_STRING(BGl_string2024z00zz__tarz00,
		BgL_bgl_string2024za700za7za7_2184za7, "invalid magic number", 20);
	      DEFINE_STRING(BGl_string2025z00zz__tarz00,
		BgL_bgl_string2025za700za7za7_2185za7,
		"no terminator for zero-terminated `~a' found", 44);
	      DEFINE_STRING(BGl_string2026z00zz__tarz00,
		BgL_bgl_string2026za700za7za7_2186za7, "corrupted tar file", 18);
	      DEFINE_STRING(BGl_string2027z00zz__tarz00,
		BgL_bgl_string2027za700za7za7_2187za7, "tar-round-up-to-record-size", 27);
	      DEFINE_STRING(BGl_string2028z00zz__tarz00,
		BgL_bgl_string2028za700za7za7_2188za7, "long", 4);
	      DEFINE_STRING(BGl_string2029z00zz__tarz00,
		BgL_bgl_string2029za700za7za7_2189za7, "tar-read-block", 14);
	      DEFINE_STRING(BGl_string2030z00zz__tarz00,
		BgL_bgl_string2030za700za7za7_2190za7, "Illegal block", 13);
	      DEFINE_STRING(BGl_string2031z00zz__tarz00,
		BgL_bgl_string2031za700za7za7_2191za7, "tar-header", 10);
	      DEFINE_STRING(BGl_string2034z00zz__tarz00,
		BgL_bgl_string2034za700za7za7_2192za7, "directory", 9);
	      DEFINE_STRING(BGl_string2036z00zz__tarz00,
		BgL_bgl_string2036za700za7za7_2193za7, "file", 4);
	      DEFINE_STRING(BGl_string2038z00zz__tarz00,
		BgL_bgl_string2038za700za7za7_2194za7, "files", 5);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2002z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2004z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2006z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2008z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2010z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2012z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2014z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2016z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2018z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2039z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2050z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2054z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1966z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1968z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2061z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2065z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_list2032z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2069z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1970z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1972z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1974z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1976z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1978z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2073z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2075z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1980z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1982z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1984z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1986z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1994z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1996z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol1998z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2092z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2093z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2096z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_tarzd2headerzd2zz__tarz00));
		     ADD_ROOT((void *) (&BGl_keyword2033z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_keyword2035z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_keyword2037z00zz__tarz00));
		     ADD_ROOT((void *) (&BGl_symbol2000z00zz__tarz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__tarz00(long
		BgL_checksumz00_3152, char *BgL_fromz00_3153)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__tarz00))
				{
					BGl_requirezd2initializa7ationz75zz__tarz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__tarz00();
					BGl_cnstzd2initzd2zz__tarz00();
					BGl_importedzd2moduleszd2initz00zz__tarz00();
					BGl_objectzd2initzd2zz__tarz00();
					return BGl_methodzd2initzd2zz__tarz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__tarz00(void)
	{
		{	/* Unsafe/tar.scm 17 */
			BGl_symbol1966z00zz__tarz00 =
				bstring_to_symbol(BGl_string1967z00zz__tarz00);
			BGl_symbol1968z00zz__tarz00 =
				bstring_to_symbol(BGl_string1969z00zz__tarz00);
			BGl_symbol1970z00zz__tarz00 =
				bstring_to_symbol(BGl_string1971z00zz__tarz00);
			BGl_symbol1972z00zz__tarz00 =
				bstring_to_symbol(BGl_string1973z00zz__tarz00);
			BGl_symbol1974z00zz__tarz00 =
				bstring_to_symbol(BGl_string1975z00zz__tarz00);
			BGl_symbol1976z00zz__tarz00 =
				bstring_to_symbol(BGl_string1977z00zz__tarz00);
			BGl_symbol1978z00zz__tarz00 =
				bstring_to_symbol(BGl_string1979z00zz__tarz00);
			BGl_symbol1980z00zz__tarz00 =
				bstring_to_symbol(BGl_string1981z00zz__tarz00);
			BGl_symbol1982z00zz__tarz00 =
				bstring_to_symbol(BGl_string1983z00zz__tarz00);
			BGl_symbol1984z00zz__tarz00 =
				bstring_to_symbol(BGl_string1985z00zz__tarz00);
			BGl_symbol1986z00zz__tarz00 =
				bstring_to_symbol(BGl_string1987z00zz__tarz00);
			BGl_symbol1994z00zz__tarz00 =
				bstring_to_symbol(BGl_string1995z00zz__tarz00);
			BGl_symbol1996z00zz__tarz00 =
				bstring_to_symbol(BGl_string1997z00zz__tarz00);
			BGl_symbol1998z00zz__tarz00 =
				bstring_to_symbol(BGl_string1999z00zz__tarz00);
			BGl_symbol2000z00zz__tarz00 =
				bstring_to_symbol(BGl_string2001z00zz__tarz00);
			BGl_symbol2002z00zz__tarz00 =
				bstring_to_symbol(BGl_string2003z00zz__tarz00);
			BGl_symbol2004z00zz__tarz00 =
				bstring_to_symbol(BGl_string2005z00zz__tarz00);
			BGl_symbol2006z00zz__tarz00 =
				bstring_to_symbol(BGl_string2007z00zz__tarz00);
			BGl_symbol2008z00zz__tarz00 =
				bstring_to_symbol(BGl_string2009z00zz__tarz00);
			BGl_symbol2010z00zz__tarz00 =
				bstring_to_symbol(BGl_string2011z00zz__tarz00);
			BGl_symbol2012z00zz__tarz00 =
				bstring_to_symbol(BGl_string2013z00zz__tarz00);
			BGl_symbol2014z00zz__tarz00 =
				bstring_to_symbol(BGl_string2015z00zz__tarz00);
			BGl_symbol2016z00zz__tarz00 =
				bstring_to_symbol(BGl_string2017z00zz__tarz00);
			BGl_symbol2018z00zz__tarz00 =
				bstring_to_symbol(BGl_string2019z00zz__tarz00);
			BGl_keyword2033z00zz__tarz00 =
				bstring_to_keyword(BGl_string2034z00zz__tarz00);
			BGl_keyword2035z00zz__tarz00 =
				bstring_to_keyword(BGl_string2036z00zz__tarz00);
			BGl_keyword2037z00zz__tarz00 =
				bstring_to_keyword(BGl_string2038z00zz__tarz00);
			BGl_list2032z00zz__tarz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2033z00zz__tarz00,
				MAKE_YOUNG_PAIR(BGl_keyword2035z00zz__tarz00,
					MAKE_YOUNG_PAIR(BGl_keyword2037z00zz__tarz00, BNIL)));
			BGl_symbol2039z00zz__tarz00 =
				bstring_to_symbol(BGl_string2040z00zz__tarz00);
			BGl_symbol2050z00zz__tarz00 =
				bstring_to_symbol(BGl_string2051z00zz__tarz00);
			BGl_symbol2054z00zz__tarz00 =
				bstring_to_symbol(BGl_string2028z00zz__tarz00);
			BGl_symbol2061z00zz__tarz00 =
				bstring_to_symbol(BGl_string2062z00zz__tarz00);
			BGl_symbol2065z00zz__tarz00 =
				bstring_to_symbol(BGl_string2066z00zz__tarz00);
			BGl_symbol2069z00zz__tarz00 =
				bstring_to_symbol(BGl_string2070z00zz__tarz00);
			BGl_symbol2073z00zz__tarz00 =
				bstring_to_symbol(BGl_string2074z00zz__tarz00);
			BGl_symbol2075z00zz__tarz00 =
				bstring_to_symbol(BGl_string2076z00zz__tarz00);
			BGl_symbol2092z00zz__tarz00 =
				bstring_to_symbol(BGl_string2031z00zz__tarz00);
			BGl_symbol2093z00zz__tarz00 =
				bstring_to_symbol(BGl_string2094z00zz__tarz00);
			return (BGl_symbol2096z00zz__tarz00 =
				bstring_to_symbol(BGl_string2097z00zz__tarz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__tarz00(void)
	{
		{	/* Unsafe/tar.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* tar-error */
	obj_t BGl_tarzd2errorzd2zz__tarz00(obj_t BgL_msgz00_3, obj_t BgL_objz00_4)
	{
		{	/* Unsafe/tar.scm 79 */
			{	/* Unsafe/tar.scm 80 */
				BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1284z00_1257;

				{	/* Unsafe/tar.scm 80 */
					BgL_z62iozd2parsezd2errorz62_bglt BgL_new1057z00_1258;

					{	/* Unsafe/tar.scm 80 */
						BgL_z62iozd2parsezd2errorz62_bglt BgL_new1056z00_1261;

						BgL_new1056z00_1261 =
							((BgL_z62iozd2parsezd2errorz62_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62iozd2parsezd2errorz62_bgl))));
						{	/* Unsafe/tar.scm 80 */
							long BgL_arg1306z00_1262;

							BgL_arg1306z00_1262 =
								BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1056z00_1261),
								BgL_arg1306z00_1262);
						}
						BgL_new1057z00_1258 = BgL_new1056z00_1261;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1057z00_1258)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1057z00_1258)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_3212;

						{	/* Unsafe/tar.scm 80 */
							obj_t BgL_arg1304z00_1259;

							{	/* Unsafe/tar.scm 80 */
								obj_t BgL_arg1305z00_1260;

								{	/* Unsafe/tar.scm 80 */
									obj_t BgL_classz00_2203;

									BgL_classz00_2203 = BGl_z62iozd2parsezd2errorz62zz__objectz00;
									BgL_arg1305z00_1260 = BGL_CLASS_ALL_FIELDS(BgL_classz00_2203);
								}
								BgL_arg1304z00_1259 = VECTOR_REF(BgL_arg1305z00_1260, 2L);
							}
							BgL_auxz00_3212 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1304z00_1259);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1057z00_1258)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_3212), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(
									((BgL_z62errorz62_bglt) BgL_new1057z00_1258)))->BgL_procz00) =
						((obj_t) BGl_symbol1966z00zz__tarz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
										BgL_new1057z00_1258)))->BgL_msgz00) =
						((obj_t) BgL_msgz00_3), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
										BgL_new1057z00_1258)))->BgL_objz00) =
						((obj_t) BgL_objz00_4), BUNSPEC);
					BgL_arg1284z00_1257 = BgL_new1057z00_1258;
				}
				return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1284z00_1257));
			}
		}

	}



/* tar-type-name */
	obj_t BGl_tarzd2typezd2namez00zz__tarz00(unsigned char BgL_cz00_5)
	{
		{	/* Unsafe/tar.scm 99 */
			switch (BgL_cz00_5)
				{
				case ((unsigned char) '\000'):

					return BGl_symbol1968z00zz__tarz00;
					break;
				case ((unsigned char) '0'):

					return BGl_symbol1970z00zz__tarz00;
					break;
				case ((unsigned char) '1'):

					return BGl_symbol1972z00zz__tarz00;
					break;
				case ((unsigned char) '2'):

					return BGl_symbol1974z00zz__tarz00;
					break;
				case ((unsigned char) '3'):

					return BGl_symbol1976z00zz__tarz00;
					break;
				case ((unsigned char) '4'):

					return BGl_symbol1978z00zz__tarz00;
					break;
				case ((unsigned char) '5'):

					return BGl_symbol1980z00zz__tarz00;
					break;
				case ((unsigned char) '6'):

					return BGl_symbol1982z00zz__tarz00;
					break;
				case ((unsigned char) '7'):

					return BGl_symbol1984z00zz__tarz00;
					break;
				case ((unsigned char) 'L'):

					return BGl_symbol1986z00zz__tarz00;
					break;
				default:
					return
						BGl_tarzd2errorzd2zz__tarz00(BGl_string1988z00zz__tarz00,
						BCHAR(BgL_cz00_5));
				}
		}

	}



/* checksum */
	long BGl_checksumz00zz__tarz00(obj_t BgL_bufz00_10)
	{
		{	/* Unsafe/tar.scm 125 */
			{	/* Unsafe/tar.scm 126 */
				long BgL_pz00_1272;

				BgL_pz00_1272 = (100L + 48L);
				{	/* Unsafe/tar.scm 126 */
					obj_t BgL_b2z00_1273;

					{	/* Unsafe/tar.scm 128 */
						obj_t BgL_arg1317z00_1286;
						obj_t BgL_arg1318z00_1287;

						BgL_arg1317z00_1286 =
							c_substring(((obj_t) BgL_bufz00_10), 0L, BgL_pz00_1272);
						{	/* Unsafe/tar.scm 130 */
							long BgL_arg1319z00_1288;
							long BgL_arg1320z00_1289;

							BgL_arg1319z00_1288 = (BgL_pz00_1272 + 8L);
							BgL_arg1320z00_1289 = STRING_LENGTH(((obj_t) BgL_bufz00_10));
							BgL_arg1318z00_1287 =
								c_substring(
								((obj_t) BgL_bufz00_10), BgL_arg1319z00_1288,
								BgL_arg1320z00_1289);
						}
						BgL_b2z00_1273 =
							string_append_3(BgL_arg1317z00_1286, BGl_string1990z00zz__tarz00,
							BgL_arg1318z00_1287);
					}
					{	/* Unsafe/tar.scm 127 */

						{
							long BgL_iz00_2245;
							long BgL_sz00_2246;

							BgL_iz00_2245 = 0L;
							BgL_sz00_2246 = 0L;
						BgL_dozd2loopzd2zd21062zd2_2244:
							if ((BgL_iz00_2245 >= 512L))
								{	/* Unsafe/tar.scm 131 */
									return BgL_sz00_2246;
								}
							else
								{
									long BgL_sz00_3242;
									long BgL_iz00_3240;

									BgL_iz00_3240 = (1L + BgL_iz00_2245);
									BgL_sz00_3242 =
										(BgL_sz00_2246 +
										(STRING_REF(BgL_b2z00_1273, BgL_iz00_2245)));
									BgL_sz00_2246 = BgL_sz00_3242;
									BgL_iz00_2245 = BgL_iz00_3240;
									goto BgL_dozd2loopzd2zd21062zd2_2244;
								}
						}
					}
				}
			}
		}

	}



/* _tar-read-header */
	obj_t BGl__tarzd2readzd2headerz00zz__tarz00(obj_t BgL_env1161z00_14,
		obj_t BgL_opt1160z00_13)
	{
		{	/* Unsafe/tar.scm 138 */
			{	/* Unsafe/tar.scm 138 */

				switch (VECTOR_LENGTH(BgL_opt1160z00_13))
					{
					case 0L:

						{	/* Unsafe/tar.scm 138 */
							obj_t BgL_portz00_1293;

							{	/* Unsafe/tar.scm 138 */
								obj_t BgL_tmpz00_3246;

								BgL_tmpz00_3246 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_portz00_1293 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_3246);
							}
							{	/* Unsafe/tar.scm 138 */

								return
									BGl_tarzd2readzd2headerz00zz__tarz00(BgL_portz00_1293,
									BFALSE);
							}
						}
						break;
					case 1L:

						{	/* Unsafe/tar.scm 138 */
							obj_t BgL_portz00_1295;

							BgL_portz00_1295 = VECTOR_REF(BgL_opt1160z00_13, 0L);
							{	/* Unsafe/tar.scm 138 */

								return
									BGl_tarzd2readzd2headerz00zz__tarz00(BgL_portz00_1295,
									BFALSE);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/tar.scm 138 */
							obj_t BgL_portz00_1297;

							BgL_portz00_1297 = VECTOR_REF(BgL_opt1160z00_13, 0L);
							{	/* Unsafe/tar.scm 138 */
								obj_t BgL_longnamez00_1298;

								BgL_longnamez00_1298 = VECTOR_REF(BgL_opt1160z00_13, 1L);
								{	/* Unsafe/tar.scm 138 */

									return
										BGl_tarzd2readzd2headerz00zz__tarz00(BgL_portz00_1297,
										BgL_longnamez00_1298);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* tar-read-header */
	BGL_EXPORTED_DEF obj_t BGl_tarzd2readzd2headerz00zz__tarz00(obj_t
		BgL_portz00_11, obj_t BgL_longnamez00_12)
	{
		{	/* Unsafe/tar.scm 138 */
			if (INPUT_PORTP(BgL_portz00_11))
				{	/* Unsafe/tar.scm 139 */
					BFALSE;
				}
			else
				{	/* Unsafe/tar.scm 139 */
					BGl_bigloozd2typezd2errorz00zz__errorz00(BGl_string1991z00zz__tarz00,
						BGl_string1992z00zz__tarz00, BgL_portz00_11);
				}
			{	/* Unsafe/tar.scm 141 */
				struct bgl_cell BgL_box2198_3034z00;
				obj_t BgL_ptrz00_3034;

				BgL_ptrz00_3034 = MAKE_CELL_STACK(BINT(0L), BgL_box2198_3034z00);
				{	/* Unsafe/tar.scm 141 */
					obj_t BgL_dataz00_1301;

					BgL_dataz00_1301 =
						BGl_readzd2charszd2zz__r4_input_6_10_2z00(BINT(512L),
						BgL_portz00_11);
					{	/* Unsafe/tar.scm 142 */
						long BgL_lenz00_1302;

						BgL_lenz00_1302 = STRING_LENGTH(((obj_t) BgL_dataz00_1301));
						{	/* Unsafe/tar.scm 143 */

							{

								{	/* Unsafe/tar.scm 170 */
									obj_t BgL_namez00_1305;

									{	/* Unsafe/tar.scm 170 */
										bool_t BgL_test2199z00_3265;

										if (STRINGP(BgL_dataz00_1301))
											{	/* Unsafe/tar.scm 170 */
												BgL_test2199z00_3265 =
													(STRING_LENGTH(BgL_dataz00_1301) == 0L);
											}
										else
											{	/* Unsafe/tar.scm 170 */
												BgL_test2199z00_3265 = ((bool_t) 1);
											}
										if (BgL_test2199z00_3265)
											{	/* Unsafe/tar.scm 170 */
												BgL_namez00_1305 = BGl_string1993z00zz__tarz00;
											}
										else
											{	/* Unsafe/tar.scm 170 */
												BgL_namez00_1305 =
													BGl_extractze70ze7zz__tarz00(BgL_longnamez00_12,
													BgL_lenz00_1302, BgL_portz00_11, BgL_dataz00_1301,
													BgL_ptrz00_3034, BGl_symbol1994z00zz__tarz00, 100L);
											}
									}
									if ((STRING_LENGTH(((obj_t) BgL_namez00_1305)) > 0L))
										{	/* Unsafe/tar.scm 174 */
											obj_t BgL_modez00_1308;

											{	/* Unsafe/tar.scm 174 */
												obj_t BgL_arg1354z00_1358;

												BgL_arg1354z00_1358 =
													BGl_extractze70ze7zz__tarz00(BgL_longnamez00_12,
													BgL_lenz00_1302, BgL_portz00_11, BgL_dataz00_1301,
													BgL_ptrz00_3034, BGl_symbol1996z00zz__tarz00, 8L);
												{	/* Unsafe/tar.scm 174 */

													{	/* Unsafe/tar.scm 117 */
														long BgL__ortest_1061z00_2300;

														{	/* Unsafe/tar.scm 117 */
															char *BgL_tmpz00_3276;

															BgL_tmpz00_3276 =
																BSTRING_TO_STRING(BgL_arg1354z00_1358);
															BgL__ortest_1061z00_2300 =
																BGL_STRTOL(BgL_tmpz00_3276, 0L, 8L);
														}
														BgL_modez00_1308 = BINT(BgL__ortest_1061z00_2300);
											}}}
											{	/* Unsafe/tar.scm 174 */
												obj_t BgL_uidz00_1309;

												{	/* Unsafe/tar.scm 175 */
													obj_t BgL_arg1352z00_1355;

													BgL_arg1352z00_1355 =
														BGl_extractze70ze7zz__tarz00(BgL_longnamez00_12,
														BgL_lenz00_1302, BgL_portz00_11, BgL_dataz00_1301,
														BgL_ptrz00_3034, BGl_symbol1998z00zz__tarz00, 8L);
													{	/* Unsafe/tar.scm 175 */

														{	/* Unsafe/tar.scm 117 */
															long BgL__ortest_1061z00_2301;

															{	/* Unsafe/tar.scm 117 */
																char *BgL_tmpz00_3281;

																BgL_tmpz00_3281 =
																	BSTRING_TO_STRING(BgL_arg1352z00_1355);
																BgL__ortest_1061z00_2301 =
																	BGL_STRTOL(BgL_tmpz00_3281, 0L, 8L);
															}
															BgL_uidz00_1309 = BINT(BgL__ortest_1061z00_2301);
												}}}
												{	/* Unsafe/tar.scm 175 */
													obj_t BgL_gidz00_1310;

													{	/* Unsafe/tar.scm 176 */
														obj_t BgL_arg1351z00_1352;

														BgL_arg1351z00_1352 =
															BGl_extractze70ze7zz__tarz00(BgL_longnamez00_12,
															BgL_lenz00_1302, BgL_portz00_11, BgL_dataz00_1301,
															BgL_ptrz00_3034, BGl_symbol2000z00zz__tarz00, 8L);
														{	/* Unsafe/tar.scm 176 */

															{	/* Unsafe/tar.scm 117 */
																long BgL__ortest_1061z00_2302;

																{	/* Unsafe/tar.scm 117 */
																	char *BgL_tmpz00_3286;

																	BgL_tmpz00_3286 =
																		BSTRING_TO_STRING(BgL_arg1351z00_1352);
																	BgL__ortest_1061z00_2302 =
																		BGL_STRTOL(BgL_tmpz00_3286, 0L, 8L);
																}
																BgL_gidz00_1310 =
																	BINT(BgL__ortest_1061z00_2302);
													}}}
													{	/* Unsafe/tar.scm 176 */
														long BgL_siza7eza7_1311;

														BgL_siza7eza7_1311 =
															BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00
															(BGl_extractze70ze7zz__tarz00(BgL_longnamez00_12,
																BgL_lenz00_1302, BgL_portz00_11,
																BgL_dataz00_1301, BgL_ptrz00_3034,
																BGl_symbol2002z00zz__tarz00, 12L), 8L);
														{	/* Unsafe/tar.scm 177 */
															long BgL_mtimez00_1312;

															BgL_mtimez00_1312 =
																BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00
																(BGl_extractze70ze7zz__tarz00
																(BgL_longnamez00_12, BgL_lenz00_1302,
																	BgL_portz00_11, BgL_dataz00_1301,
																	BgL_ptrz00_3034, BGl_symbol2004z00zz__tarz00,
																	12L), 8L);
															{	/* Unsafe/tar.scm 178 */
																obj_t BgL_chksumz00_1313;

																{	/* Unsafe/tar.scm 179 */
																	obj_t BgL_arg1348z00_1347;

																	BgL_arg1348z00_1347 =
																		BGl_extractze70ze7zz__tarz00
																		(BgL_longnamez00_12, BgL_lenz00_1302,
																		BgL_portz00_11, BgL_dataz00_1301,
																		BgL_ptrz00_3034,
																		BGl_symbol2006z00zz__tarz00, 8L);
																	{	/* Unsafe/tar.scm 179 */

																		{	/* Unsafe/tar.scm 117 */
																			long BgL__ortest_1061z00_2303;

																			{	/* Unsafe/tar.scm 117 */
																				char *BgL_tmpz00_3295;

																				BgL_tmpz00_3295 =
																					BSTRING_TO_STRING
																					(BgL_arg1348z00_1347);
																				BgL__ortest_1061z00_2303 =
																					BGL_STRTOL(BgL_tmpz00_3295, 0L, 8L);
																			}
																			BgL_chksumz00_1313 =
																				BINT(BgL__ortest_1061z00_2303);
																}}}
																{	/* Unsafe/tar.scm 179 */
																	unsigned char BgL_linkflagz00_1314;

																	{	/* Unsafe/tar.scm 167 */
																		unsigned char BgL_cz00_1388;

																		BgL_cz00_1388 =
																			STRING_REF(
																			((obj_t) BgL_dataz00_1301),
																			(long) CINT(
																				((obj_t)
																					((obj_t)
																						CELL_REF(BgL_ptrz00_3034)))));
																		{	/* Unsafe/tar.scm 168 */
																			obj_t BgL_auxz00_3035;

																			BgL_auxz00_3035 =
																				ADDFX(BINT(1L),
																				((obj_t)
																					((obj_t) CELL_REF(BgL_ptrz00_3034))));
																			CELL_SET(BgL_ptrz00_3034,
																				BgL_auxz00_3035);
																		}
																		BgL_linkflagz00_1314 = BgL_cz00_1388;
																	}
																	{	/* Unsafe/tar.scm 180 */
																		obj_t BgL_linknamez00_1315;

																		BgL_linknamez00_1315 =
																			BGl_extractze70ze7zz__tarz00
																			(BgL_longnamez00_12, BgL_lenz00_1302,
																			BgL_portz00_11, BgL_dataz00_1301,
																			BgL_ptrz00_3034,
																			BGl_symbol2008z00zz__tarz00, 100L);
																		{	/* Unsafe/tar.scm 181 */
																			obj_t BgL_magicz00_1316;

																			BgL_magicz00_1316 =
																				BGl_extractze70ze7zz__tarz00
																				(BgL_longnamez00_12, BgL_lenz00_1302,
																				BgL_portz00_11, BgL_dataz00_1301,
																				BgL_ptrz00_3034,
																				BGl_symbol2010z00zz__tarz00, 8L);
																			{	/* Unsafe/tar.scm 182 */
																				obj_t BgL_unamez00_1317;

																				BgL_unamez00_1317 =
																					BGl_extractze70ze7zz__tarz00
																					(BgL_longnamez00_12, BgL_lenz00_1302,
																					BgL_portz00_11, BgL_dataz00_1301,
																					BgL_ptrz00_3034,
																					BGl_symbol2012z00zz__tarz00, 32L);
																				{	/* Unsafe/tar.scm 183 */
																					obj_t BgL_gnamez00_1318;

																					BgL_gnamez00_1318 =
																						BGl_extractze70ze7zz__tarz00
																						(BgL_longnamez00_12,
																						BgL_lenz00_1302, BgL_portz00_11,
																						BgL_dataz00_1301, BgL_ptrz00_3034,
																						BGl_symbol2014z00zz__tarz00, 32L);
																					{	/* Unsafe/tar.scm 184 */
																						long BgL_devmajorz00_1319;

																						{	/* Unsafe/tar.scm 185 */
																							obj_t BgL_arg1343z00_1343;

																							BgL_arg1343z00_1343 =
																								BGl_extractze70ze7zz__tarz00
																								(BgL_longnamez00_12,
																								BgL_lenz00_1302, BgL_portz00_11,
																								BgL_dataz00_1301,
																								BgL_ptrz00_3034,
																								BGl_symbol2016z00zz__tarz00,
																								8L);
																							{	/* Unsafe/tar.scm 117 */
																								long BgL__ortest_1061z00_2304;

																								{	/* Unsafe/tar.scm 117 */
																									char *BgL_tmpz00_3311;

																									BgL_tmpz00_3311 =
																										BSTRING_TO_STRING
																										(BgL_arg1343z00_1343);
																									BgL__ortest_1061z00_2304 =
																										BGL_STRTOL(BgL_tmpz00_3311,
																										0L, 8L);
																								}
																								BgL_devmajorz00_1319 =
																									BgL__ortest_1061z00_2304;
																						}}
																						{	/* Unsafe/tar.scm 185 */
																							long BgL_devminorz00_1320;

																							{	/* Unsafe/tar.scm 186 */
																								obj_t BgL_arg1342z00_1342;

																								BgL_arg1342z00_1342 =
																									BGl_extractze70ze7zz__tarz00
																									(BgL_longnamez00_12,
																									BgL_lenz00_1302,
																									BgL_portz00_11,
																									BgL_dataz00_1301,
																									BgL_ptrz00_3034,
																									BGl_symbol2018z00zz__tarz00,
																									8L);
																								{	/* Unsafe/tar.scm 117 */
																									long BgL__ortest_1061z00_2305;

																									{	/* Unsafe/tar.scm 117 */
																										char *BgL_tmpz00_3315;

																										BgL_tmpz00_3315 =
																											BSTRING_TO_STRING
																											(BgL_arg1342z00_1342);
																										BgL__ortest_1061z00_2305 =
																											BGL_STRTOL
																											(BgL_tmpz00_3315, 0L, 8L);
																									}
																									BgL_devminorz00_1320 =
																										BgL__ortest_1061z00_2305;
																							}}
																							{	/* Unsafe/tar.scm 186 */
																								long BgL_csum2z00_1321;

																								BgL_csum2z00_1321 =
																									BGl_checksumz00zz__tarz00
																									(BgL_dataz00_1301);
																								{	/* Unsafe/tar.scm 187 */

																									{	/* Unsafe/tar.scm 189 */
																										bool_t BgL_test2202z00_3319;

																										{	/* Unsafe/tar.scm 189 */
																											bool_t
																												BgL_test2203z00_3320;
																											if ((7L ==
																													STRING_LENGTH(((obj_t)
																															BgL_magicz00_1316))))
																												{	/* Unsafe/tar.scm 189 */
																													int
																														BgL_arg1704z00_2311;
																													{	/* Unsafe/tar.scm 189 */
																														char
																															*BgL_auxz00_3327;
																														char
																															*BgL_tmpz00_3325;
																														BgL_auxz00_3327 =
																															BSTRING_TO_STRING(
																															((obj_t)
																																BgL_magicz00_1316));
																														BgL_tmpz00_3325 =
																															BSTRING_TO_STRING
																															(BGl_string2020z00zz__tarz00);
																														BgL_arg1704z00_2311
																															=
																															memcmp
																															(BgL_tmpz00_3325,
																															BgL_auxz00_3327,
																															7L);
																													}
																													BgL_test2203z00_3320 =
																														(
																														(long)
																														(BgL_arg1704z00_2311)
																														== 0L);
																												}
																											else
																												{	/* Unsafe/tar.scm 189 */
																													BgL_test2203z00_3320 =
																														((bool_t) 0);
																												}
																											if (BgL_test2203z00_3320)
																												{	/* Unsafe/tar.scm 189 */
																													BgL_test2202z00_3319 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Unsafe/tar.scm 190 */
																													bool_t
																														BgL_test2205z00_3333;
																													if ((5L ==
																															STRING_LENGTH((
																																	(obj_t)
																																	BgL_magicz00_1316))))
																														{	/* Unsafe/tar.scm 190 */
																															int
																																BgL_arg1704z00_2322;
																															{	/* Unsafe/tar.scm 190 */
																																char
																																	*BgL_auxz00_3340;
																																char
																																	*BgL_tmpz00_3338;
																																BgL_auxz00_3340
																																	=
																																	BSTRING_TO_STRING
																																	(((obj_t)
																																		BgL_magicz00_1316));
																																BgL_tmpz00_3338
																																	=
																																	BSTRING_TO_STRING
																																	(BGl_string2021z00zz__tarz00);
																																BgL_arg1704z00_2322
																																	=
																																	memcmp
																																	(BgL_tmpz00_3338,
																																	BgL_auxz00_3340,
																																	5L);
																															}
																															BgL_test2205z00_3333
																																=
																																((long)
																																(BgL_arg1704z00_2322)
																																== 0L);
																														}
																													else
																														{	/* Unsafe/tar.scm 190 */
																															BgL_test2205z00_3333
																																= ((bool_t) 0);
																														}
																													if (BgL_test2205z00_3333)
																														{	/* Unsafe/tar.scm 190 */
																															BgL_test2202z00_3319
																																= ((bool_t) 1);
																														}
																													else
																														{	/* Unsafe/tar.scm 190 */
																															if (
																																(7L ==
																																	STRING_LENGTH(
																																		((obj_t)
																																			BgL_magicz00_1316))))
																																{	/* Unsafe/tar.scm 191 */
																																	int
																																		BgL_arg1704z00_2333;
																																	{	/* Unsafe/tar.scm 191 */
																																		char
																																			*BgL_auxz00_3352;
																																		char
																																			*BgL_tmpz00_3350;
																																		BgL_auxz00_3352
																																			=
																																			BSTRING_TO_STRING
																																			(((obj_t)
																																				BgL_magicz00_1316));
																																		BgL_tmpz00_3350
																																			=
																																			BSTRING_TO_STRING
																																			(BGl_string2022z00zz__tarz00);
																																		BgL_arg1704z00_2333
																																			=
																																			memcmp
																																			(BgL_tmpz00_3350,
																																			BgL_auxz00_3352,
																																			7L);
																																	}
																																	BgL_test2202z00_3319
																																		=
																																		((long)
																																		(BgL_arg1704z00_2333)
																																		== 0L);
																																}
																															else
																																{	/* Unsafe/tar.scm 191 */
																																	BgL_test2202z00_3319
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																										}
																										if (BgL_test2202z00_3319)
																											{	/* Unsafe/tar.scm 189 */
																												if (
																													(BgL_csum2z00_1321 ==
																														(long)
																														CINT
																														(BgL_chksumz00_1313)))
																													{	/* Unsafe/tar.scm 198 */
																														BgL_tarzd2headerzd2_bglt
																															BgL_new1065z00_1331;
																														{	/* Unsafe/tar.scm 199 */
																															BgL_tarzd2headerzd2_bglt
																																BgL_new1064z00_1332;
																															BgL_new1064z00_1332
																																=
																																(
																																(BgL_tarzd2headerzd2_bglt)
																																BOBJECT
																																(GC_MALLOC
																																	(sizeof(struct
																																			BgL_tarzd2headerzd2_bgl))));
																															{	/* Unsafe/tar.scm 199 */
																																long
																																	BgL_arg1335z00_1333;
																																BgL_arg1335z00_1333
																																	=
																																	BGL_CLASS_NUM
																																	(BGl_tarzd2headerzd2zz__tarz00);
																																BGL_OBJECT_CLASS_NUM_SET
																																	(((BgL_objectz00_bglt) BgL_new1064z00_1332), BgL_arg1335z00_1333);
																															}
																															BgL_new1065z00_1331
																																=
																																BgL_new1064z00_1332;
																														}
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_namez00) = ((obj_t) BgL_namez00_1305), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_modez00) = ((long) (long) CINT(BgL_modez00_1308)), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_uidz00) = ((long) (long) CINT(BgL_uidz00_1309)), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_gidz00) = ((long) (long) CINT(BgL_gidz00_1310)), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_siza7eza7) = ((long) BgL_siza7eza7_1311), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_mtimez00) = ((obj_t) bgl_seconds_to_date(BgL_mtimez00_1312)), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_checksumz00) = ((long) (long) CINT(BgL_chksumz00_1313)), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_typez00) = ((obj_t) BGl_tarzd2typezd2namez00zz__tarz00(BgL_linkflagz00_1314)), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_linknamez00) = ((obj_t) BgL_linknamez00_1315), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_magicz00) = ((obj_t) BgL_magicz00_1316), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_unamez00) = ((obj_t) BgL_unamez00_1317), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_gnamez00) = ((obj_t) BgL_gnamez00_1318), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_devmajorz00) = ((long) BgL_devmajorz00_1319), BUNSPEC);
																														((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1065z00_1331))->BgL_devminorz00) = ((long) BgL_devminorz00_1320), BUNSPEC);
																														return
																															((obj_t)
																															BgL_new1065z00_1331);
																													}
																												else
																													{	/* Unsafe/tar.scm 195 */
																														obj_t
																															BgL_arg1336z00_1334;
																														{	/* Unsafe/tar.scm 195 */
																															obj_t
																																BgL_list1337z00_1335;
																															BgL_list1337z00_1335
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_chksumz00_1313,
																																BNIL);
																															BgL_arg1336z00_1334
																																=
																																BGl_formatz00zz__r4_output_6_10_3z00
																																(BGl_string2023z00zz__tarz00,
																																BgL_list1337z00_1335);
																														}
																														return
																															BGl_tarzd2errorzd2zz__tarz00
																															(BgL_arg1336z00_1334,
																															BINT
																															(BgL_csum2z00_1321));
																													}
																											}
																										else
																											{	/* Unsafe/tar.scm 192 */
																												obj_t
																													BgL_arg1338z00_1336;
																												BgL_arg1338z00_1336 =
																													string_for_read((
																														(obj_t)
																														BgL_magicz00_1316));
																												return
																													BGl_tarzd2errorzd2zz__tarz00
																													(BGl_string2024z00zz__tarz00,
																													BgL_arg1338z00_1336);
																											}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									else
										{	/* Unsafe/tar.scm 173 */
											return BFALSE;
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* extract~0 */
	obj_t BGl_extractze70ze7zz__tarz00(obj_t BgL_longnamez00_3031,
		long BgL_lenz00_3030, obj_t BgL_portz00_3029, obj_t BgL_dataz00_3028,
		obj_t BgL_ptrz00_3027, obj_t BgL_whatz00_1368, long BgL_siza7eza7_1369)
	{
		{	/* Unsafe/tar.scm 165 */
			{
				long BgL_iz00_1372;

				BgL_iz00_1372 = 0L;
			BgL_zc3z04anonymousza31363ze3z87_1373:
				if ((BgL_iz00_1372 >= BgL_siza7eza7_1369))
					{	/* Unsafe/tar.scm 147 */
						if (CBOOL(BgL_longnamez00_3031))
							{	/* Unsafe/tar.scm 148 */
								{	/* Unsafe/tar.scm 150 */
									obj_t BgL_auxz00_3032;

									BgL_auxz00_3032 =
										ADDFX(
										((obj_t)
											((obj_t) CELL_REF(BgL_ptrz00_3027))),
										BINT(BgL_siza7eza7_1369));
									CELL_SET(BgL_ptrz00_3027, BgL_auxz00_3032);
								}
								{	/* Unsafe/tar.scm 152 */
									long BgL_arg1365z00_1375;

									BgL_arg1365z00_1375 =
										(STRING_LENGTH(((obj_t) BgL_longnamez00_3031)) - 1L);
									{	/* Unsafe/tar.scm 151 */
										obj_t BgL_tmpz00_3403;

										BgL_tmpz00_3403 = ((obj_t) BgL_longnamez00_3031);
										return
											bgl_string_shrink(BgL_tmpz00_3403, BgL_arg1365z00_1375);
									}
								}
							}
						else
							{	/* Unsafe/tar.scm 154 */
								obj_t BgL_arg1367z00_1377;

								{	/* Unsafe/tar.scm 154 */
									obj_t BgL_list1368z00_1378;

									BgL_list1368z00_1378 =
										MAKE_YOUNG_PAIR(BgL_whatz00_1368, BNIL);
									BgL_arg1367z00_1377 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2025z00zz__tarz00, BgL_list1368z00_1378);
								}
								return
									BGl_tarzd2errorzd2zz__tarz00(BgL_arg1367z00_1377,
									BINT(BgL_siza7eza7_1369));
							}
					}
				else
					{	/* Unsafe/tar.scm 147 */
						if ((BgL_iz00_1372 >= BgL_lenz00_3030))
							{	/* Unsafe/tar.scm 156 */
								return
									BGl_tarzd2errorzd2zz__tarz00(BGl_string2026z00zz__tarz00,
									BgL_portz00_3029);
							}
						else
							{	/* Unsafe/tar.scm 159 */
								unsigned char BgL_cz00_1380;

								{	/* Unsafe/tar.scm 159 */
									long BgL_tmpz00_3413;

									{	/* Unsafe/tar.scm 159 */
										long BgL_za71za7_2279;

										BgL_za71za7_2279 =
											(long) CINT(
											((obj_t) ((obj_t) CELL_REF(BgL_ptrz00_3027))));
										BgL_tmpz00_3413 = (BgL_za71za7_2279 + BgL_iz00_1372);
									}
									BgL_cz00_1380 =
										STRING_REF(((obj_t) BgL_dataz00_3028), BgL_tmpz00_3413);
								}
								if ((((unsigned char) '\000') == BgL_cz00_1380))
									{	/* Unsafe/tar.scm 161 */
										long BgL_nptrz00_1382;

										{	/* Unsafe/tar.scm 161 */
											long BgL_za71za7_2285;

											BgL_za71za7_2285 =
												(long) CINT(
												((obj_t) ((obj_t) CELL_REF(BgL_ptrz00_3027))));
											BgL_nptrz00_1382 = (BgL_za71za7_2285 + BgL_iz00_1372);
										}
										{	/* Unsafe/tar.scm 161 */
											obj_t BgL_subz00_1383;

											{	/* Unsafe/tar.scm 162 */
												long BgL_startz00_2288;

												BgL_startz00_2288 =
													(long) CINT(
													((obj_t) ((obj_t) CELL_REF(BgL_ptrz00_3027))));
												BgL_subz00_1383 =
													c_substring(
													((obj_t) BgL_dataz00_3028), BgL_startz00_2288,
													BgL_nptrz00_1382);
											}
											{	/* Unsafe/tar.scm 162 */

												{	/* Unsafe/tar.scm 163 */
													obj_t BgL_auxz00_3033;

													BgL_auxz00_3033 =
														ADDFX(
														((obj_t)
															((obj_t) CELL_REF(BgL_ptrz00_3027))),
														BINT(BgL_siza7eza7_1369));
													CELL_SET(BgL_ptrz00_3027, BgL_auxz00_3033);
												}
												return BgL_subz00_1383;
											}
										}
									}
								else
									{
										long BgL_iz00_3431;

										BgL_iz00_3431 = (1L + BgL_iz00_1372);
										BgL_iz00_1372 = BgL_iz00_3431;
										goto BgL_zc3z04anonymousza31363ze3z87_1373;
									}
							}
					}
			}
		}

	}



/* tar-round-up-to-record-size */
	BGL_EXPORTED_DEF long
		BGl_tarzd2roundzd2upzd2tozd2recordzd2siza7ez75zz__tarz00(obj_t BgL_nz00_15)
	{
		{	/* Unsafe/tar.scm 217 */
			if (INTEGERP(BgL_nz00_15))
				{	/* Unsafe/tar.scm 218 */
					return (512L * (((long) CINT(BgL_nz00_15) + (512L - 1L)) / 512L));
				}
			else
				{	/* Unsafe/tar.scm 218 */
					return
						(long)
						CINT(BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_string2027z00zz__tarz00, BGl_string2028z00zz__tarz00,
							BgL_nz00_15));
		}}

	}



/* &tar-round-up-to-record-size */
	obj_t BGl_z62tarzd2roundzd2upzd2tozd2recordzd2siza7ez17zz__tarz00(obj_t
		BgL_envz00_2902, obj_t BgL_nz00_2903)
	{
		{	/* Unsafe/tar.scm 217 */
			return
				BINT(BGl_tarzd2roundzd2upzd2tozd2recordzd2siza7ez75zz__tarz00
				(BgL_nz00_2903));
		}

	}



/* _tar-read-block */
	obj_t BGl__tarzd2readzd2blockz00zz__tarz00(obj_t BgL_env1165z00_19,
		obj_t BgL_opt1164z00_18)
	{
		{	/* Unsafe/tar.scm 226 */
			{	/* Unsafe/tar.scm 226 */
				obj_t BgL_g1166z00_1399;

				BgL_g1166z00_1399 = VECTOR_REF(BgL_opt1164z00_18, 0L);
				switch (VECTOR_LENGTH(BgL_opt1164z00_18))
					{
					case 1L:

						{	/* Unsafe/tar.scm 226 */
							obj_t BgL_pz00_1402;

							{	/* Unsafe/tar.scm 226 */
								obj_t BgL_tmpz00_3445;

								BgL_tmpz00_3445 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_pz00_1402 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_3445);
							}
							{	/* Unsafe/tar.scm 226 */

								return
									BGl_tarzd2readzd2blockz00zz__tarz00(BgL_g1166z00_1399,
									BgL_pz00_1402);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/tar.scm 226 */
							obj_t BgL_pz00_1403;

							BgL_pz00_1403 = VECTOR_REF(BgL_opt1164z00_18, 1L);
							{	/* Unsafe/tar.scm 226 */

								return
									BGl_tarzd2readzd2blockz00zz__tarz00(BgL_g1166z00_1399,
									BgL_pz00_1403);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* tar-read-block */
	BGL_EXPORTED_DEF obj_t BGl_tarzd2readzd2blockz00zz__tarz00(obj_t BgL_hz00_16,
		obj_t BgL_pz00_17)
	{
		{	/* Unsafe/tar.scm 226 */
			if (INPUT_PORTP(BgL_pz00_17))
				{	/* Unsafe/tar.scm 230 */
					bool_t BgL_test2215z00_3455;

					{	/* Unsafe/tar.scm 230 */
						obj_t BgL_classz00_2354;

						BgL_classz00_2354 = BGl_tarzd2headerzd2zz__tarz00;
						if (BGL_OBJECTP(BgL_hz00_16))
							{	/* Unsafe/tar.scm 230 */
								BgL_objectz00_bglt BgL_arg1899z00_2356;

								BgL_arg1899z00_2356 = (BgL_objectz00_bglt) (BgL_hz00_16);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Unsafe/tar.scm 230 */
										long BgL_idxz00_2362;

										BgL_idxz00_2362 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1899z00_2356);
										BgL_test2215z00_3455 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2362 + 1L)) == BgL_classz00_2354);
									}
								else
									{	/* Unsafe/tar.scm 230 */
										bool_t BgL_res1955z00_2387;

										{	/* Unsafe/tar.scm 230 */
											obj_t BgL_oclassz00_2370;

											{	/* Unsafe/tar.scm 230 */
												obj_t BgL_arg1910z00_2378;
												long BgL_arg1911z00_2379;

												BgL_arg1910z00_2378 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Unsafe/tar.scm 230 */
													long BgL_arg1912z00_2380;

													BgL_arg1912z00_2380 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1899z00_2356);
													BgL_arg1911z00_2379 =
														(BgL_arg1912z00_2380 - OBJECT_TYPE);
												}
												BgL_oclassz00_2370 =
													VECTOR_REF(BgL_arg1910z00_2378, BgL_arg1911z00_2379);
											}
											{	/* Unsafe/tar.scm 230 */
												bool_t BgL__ortest_1121z00_2371;

												BgL__ortest_1121z00_2371 =
													(BgL_classz00_2354 == BgL_oclassz00_2370);
												if (BgL__ortest_1121z00_2371)
													{	/* Unsafe/tar.scm 230 */
														BgL_res1955z00_2387 = BgL__ortest_1121z00_2371;
													}
												else
													{	/* Unsafe/tar.scm 230 */
														long BgL_odepthz00_2372;

														{	/* Unsafe/tar.scm 230 */
															obj_t BgL_arg1896z00_2373;

															BgL_arg1896z00_2373 = (BgL_oclassz00_2370);
															BgL_odepthz00_2372 =
																BGL_CLASS_DEPTH(BgL_arg1896z00_2373);
														}
														if ((1L < BgL_odepthz00_2372))
															{	/* Unsafe/tar.scm 230 */
																obj_t BgL_arg1893z00_2375;

																{	/* Unsafe/tar.scm 230 */
																	obj_t BgL_arg1894z00_2376;

																	BgL_arg1894z00_2376 = (BgL_oclassz00_2370);
																	BgL_arg1893z00_2375 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1894z00_2376,
																		1L);
																}
																BgL_res1955z00_2387 =
																	(BgL_arg1893z00_2375 == BgL_classz00_2354);
															}
														else
															{	/* Unsafe/tar.scm 230 */
																BgL_res1955z00_2387 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2215z00_3455 = BgL_res1955z00_2387;
									}
							}
						else
							{	/* Unsafe/tar.scm 230 */
								BgL_test2215z00_3455 = ((bool_t) 0);
							}
					}
					if (BgL_test2215z00_3455)
						{	/* Unsafe/tar.scm 232 */
							long BgL_nz00_1407;

							{	/* Unsafe/tar.scm 232 */
								long BgL_arg1393z00_1416;

								BgL_arg1393z00_1416 =
									(((BgL_tarzd2headerzd2_bglt) COBJECT(
											((BgL_tarzd2headerzd2_bglt) BgL_hz00_16)))->
									BgL_siza7eza7);
								BgL_nz00_1407 = (long) (BgL_arg1393z00_1416);
							}
							if ((BgL_nz00_1407 == 0L))
								{	/* Unsafe/tar.scm 233 */
									return BFALSE;
								}
							else
								{	/* Unsafe/tar.scm 235 */
									obj_t BgL_sz00_1409;

									BgL_sz00_1409 =
										BGl_readzd2charszd2zz__r4_input_6_10_2z00(BINT
										(BgL_nz00_1407), BgL_pz00_17);
									if ((STRING_LENGTH(((obj_t) BgL_sz00_1409)) < BgL_nz00_1407))
										{	/* Unsafe/tar.scm 237 */
											obj_t BgL_arg1389z00_1412;

											BgL_arg1389z00_1412 =
												(((BgL_tarzd2headerzd2_bglt) COBJECT(
														((BgL_tarzd2headerzd2_bglt) BgL_hz00_16)))->
												BgL_namez00);
											BGl_errorz00zz__errorz00(BGl_string2029z00zz__tarz00,
												BGl_string2030z00zz__tarz00, BgL_arg1389z00_1412);
										}
									else
										{	/* Unsafe/tar.scm 238 */
											long BgL_arg1390z00_1413;

											BgL_arg1390z00_1413 =
												(BGl_tarzd2roundzd2upzd2tozd2recordzd2siza7ez75zz__tarz00
												(BINT(BgL_nz00_1407)) - BgL_nz00_1407);
											BGl_readzd2charszd2zz__r4_input_6_10_2z00(BINT
												(BgL_arg1390z00_1413), BgL_pz00_17);
										}
									return BgL_sz00_1409;
								}
						}
					else
						{	/* Unsafe/tar.scm 230 */
							return
								BGl_bigloozd2typezd2errorz00zz__errorz00
								(BGl_string2029z00zz__tarz00, BGl_string2031z00zz__tarz00,
								BgL_hz00_16);
						}
				}
			else
				{	/* Unsafe/tar.scm 228 */
					return
						BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_string1991z00zz__tarz00, BGl_string1992z00zz__tarz00,
						BgL_pz00_17);
				}
		}

	}



/* rm-rf */
	bool_t BGl_rmzd2rfzd2zz__tarz00(obj_t BgL_pathz00_20)
	{
		{	/* Unsafe/tar.scm 246 */
			if (fexists(BSTRING_TO_STRING(BgL_pathz00_20)))
				{	/* Unsafe/tar.scm 248 */
					bool_t BgL_test2223z00_3502;

					if (bgl_directoryp(BSTRING_TO_STRING(BgL_pathz00_20)))
						{	/* Unsafe/tar.scm 248 */
							bool_t BgL_test2225z00_3506;

							{	/* Unsafe/tar.scm 248 */
								obj_t BgL_arg1405z00_1434;

								BgL_arg1405z00_1434 =
									bgl_file_type(BSTRING_TO_STRING(BgL_pathz00_20));
								BgL_test2225z00_3506 =
									(BgL_arg1405z00_1434 == BGl_symbol1972z00zz__tarz00);
							}
							if (BgL_test2225z00_3506)
								{	/* Unsafe/tar.scm 248 */
									BgL_test2223z00_3502 = ((bool_t) 0);
								}
							else
								{	/* Unsafe/tar.scm 248 */
									BgL_test2223z00_3502 = ((bool_t) 1);
								}
						}
					else
						{	/* Unsafe/tar.scm 248 */
							BgL_test2223z00_3502 = ((bool_t) 0);
						}
					if (BgL_test2223z00_3502)
						{	/* Unsafe/tar.scm 249 */
							obj_t BgL_filesz00_1423;

							BgL_filesz00_1423 =
								bgl_directory_to_list(BSTRING_TO_STRING(BgL_pathz00_20));
							{
								obj_t BgL_l1132z00_1425;

								BgL_l1132z00_1425 = BgL_filesz00_1423;
							BgL_zc3z04anonymousza31401ze3z87_1426:
								if (PAIRP(BgL_l1132z00_1425))
									{	/* Unsafe/tar.scm 250 */
										{	/* Unsafe/tar.scm 250 */
											obj_t BgL_fz00_1428;

											BgL_fz00_1428 = CAR(BgL_l1132z00_1425);
											BGl_rmzd2rfzd2zz__tarz00
												(BGl_makezd2filezd2namez00zz__osz00(BgL_pathz00_20,
													BgL_fz00_1428));
										}
										{
											obj_t BgL_l1132z00_3517;

											BgL_l1132z00_3517 = CDR(BgL_l1132z00_1425);
											BgL_l1132z00_1425 = BgL_l1132z00_3517;
											goto BgL_zc3z04anonymousza31401ze3z87_1426;
										}
									}
								else
									{	/* Unsafe/tar.scm 250 */
										((bool_t) 1);
									}
							}
							{	/* Unsafe/tar.scm 251 */
								char *BgL_stringz00_2401;

								BgL_stringz00_2401 = BSTRING_TO_STRING(BgL_pathz00_20);
								if (rmdir(BgL_stringz00_2401))
									{	/* Unsafe/tar.scm 251 */
										return ((bool_t) 0);
									}
								else
									{	/* Unsafe/tar.scm 251 */
										return ((bool_t) 1);
									}
							}
						}
					else
						{	/* Unsafe/tar.scm 252 */
							char *BgL_stringz00_2403;

							BgL_stringz00_2403 = BSTRING_TO_STRING(BgL_pathz00_20);
							if (unlink(BgL_stringz00_2403))
								{	/* Unsafe/tar.scm 252 */
									return ((bool_t) 0);
								}
							else
								{	/* Unsafe/tar.scm 252 */
									return ((bool_t) 1);
								}
						}
				}
			else
				{	/* Unsafe/tar.scm 247 */
					return ((bool_t) 0);
				}
		}

	}



/* _untar */
	obj_t BGl__untarz00zz__tarz00(obj_t BgL_env1170z00_26,
		obj_t BgL_opt1169z00_25)
	{
		{	/* Unsafe/tar.scm 257 */
			{	/* Unsafe/tar.scm 257 */
				obj_t BgL_g1178z00_1439;

				BgL_g1178z00_1439 = VECTOR_REF(BgL_opt1169z00_25, 0L);
				{	/* Unsafe/tar.scm 257 */
					obj_t BgL_directoryz00_1440;

					BgL_directoryz00_1440 = BGl_pwdz00zz__osz00();
					{	/* Unsafe/tar.scm 257 */
						obj_t BgL_filez00_1441;

						BgL_filez00_1441 = BFALSE;
						{	/* Unsafe/tar.scm 257 */
							obj_t BgL_filesz00_1442;

							BgL_filesz00_1442 = BNIL;
							{	/* Unsafe/tar.scm 257 */

								{
									long BgL_iz00_1443;

									BgL_iz00_1443 = 1L;
								BgL_check1173z00_1444:
									if ((BgL_iz00_1443 == VECTOR_LENGTH(BgL_opt1169z00_25)))
										{	/* Unsafe/tar.scm 257 */
											BNIL;
										}
									else
										{	/* Unsafe/tar.scm 257 */
											bool_t BgL_test2230z00_3530;

											{	/* Unsafe/tar.scm 257 */
												obj_t BgL_arg1412z00_1450;

												BgL_arg1412z00_1450 =
													VECTOR_REF(BgL_opt1169z00_25, BgL_iz00_1443);
												BgL_test2230z00_3530 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1412z00_1450, BGl_list2032z00zz__tarz00));
											}
											if (BgL_test2230z00_3530)
												{
													long BgL_iz00_3534;

													BgL_iz00_3534 = (BgL_iz00_1443 + 2L);
													BgL_iz00_1443 = BgL_iz00_3534;
													goto BgL_check1173z00_1444;
												}
											else
												{	/* Unsafe/tar.scm 257 */
													obj_t BgL_arg1411z00_1449;

													BgL_arg1411z00_1449 =
														VECTOR_REF(BgL_opt1169z00_25, BgL_iz00_1443);
													BGl_errorz00zz__errorz00(BGl_symbol2039z00zz__tarz00,
														BGl_string2041z00zz__tarz00, BgL_arg1411z00_1449);
												}
										}
								}
								{	/* Unsafe/tar.scm 257 */
									obj_t BgL_index1175z00_1451;

									{
										long BgL_iz00_2421;

										BgL_iz00_2421 = 1L;
									BgL_search1172z00_2420:
										if ((BgL_iz00_2421 == VECTOR_LENGTH(BgL_opt1169z00_25)))
											{	/* Unsafe/tar.scm 257 */
												BgL_index1175z00_1451 = BINT(-1L);
											}
										else
											{	/* Unsafe/tar.scm 257 */
												if (
													(BgL_iz00_2421 ==
														(VECTOR_LENGTH(BgL_opt1169z00_25) - 1L)))
													{	/* Unsafe/tar.scm 257 */
														BgL_index1175z00_1451 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2039z00zz__tarz00,
															BGl_string2042z00zz__tarz00,
															BINT(VECTOR_LENGTH(BgL_opt1169z00_25)));
													}
												else
													{	/* Unsafe/tar.scm 257 */
														obj_t BgL_vz00_2431;

														BgL_vz00_2431 =
															VECTOR_REF(BgL_opt1169z00_25, BgL_iz00_2421);
														if ((BgL_vz00_2431 == BGl_keyword2033z00zz__tarz00))
															{	/* Unsafe/tar.scm 257 */
																BgL_index1175z00_1451 =
																	BINT((BgL_iz00_2421 + 1L));
															}
														else
															{
																long BgL_iz00_3554;

																BgL_iz00_3554 = (BgL_iz00_2421 + 2L);
																BgL_iz00_2421 = BgL_iz00_3554;
																goto BgL_search1172z00_2420;
															}
													}
											}
									}
									{	/* Unsafe/tar.scm 257 */
										bool_t BgL_test2234z00_3556;

										{	/* Unsafe/tar.scm 257 */
											long BgL_n1z00_2435;

											{	/* Unsafe/tar.scm 257 */
												obj_t BgL_tmpz00_3557;

												if (INTEGERP(BgL_index1175z00_1451))
													{	/* Unsafe/tar.scm 257 */
														BgL_tmpz00_3557 = BgL_index1175z00_1451;
													}
												else
													{
														obj_t BgL_auxz00_3560;

														BgL_auxz00_3560 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2043z00zz__tarz00, BINT(9397L),
															BGl_string2044z00zz__tarz00,
															BGl_string2045z00zz__tarz00,
															BgL_index1175z00_1451);
														FAILURE(BgL_auxz00_3560, BFALSE, BFALSE);
													}
												BgL_n1z00_2435 = (long) CINT(BgL_tmpz00_3557);
											}
											BgL_test2234z00_3556 = (BgL_n1z00_2435 >= 0L);
										}
										if (BgL_test2234z00_3556)
											{
												long BgL_auxz00_3566;

												{	/* Unsafe/tar.scm 257 */
													obj_t BgL_tmpz00_3567;

													if (INTEGERP(BgL_index1175z00_1451))
														{	/* Unsafe/tar.scm 257 */
															BgL_tmpz00_3567 = BgL_index1175z00_1451;
														}
													else
														{
															obj_t BgL_auxz00_3570;

															BgL_auxz00_3570 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2043z00zz__tarz00, BINT(9397L),
																BGl_string2044z00zz__tarz00,
																BGl_string2045z00zz__tarz00,
																BgL_index1175z00_1451);
															FAILURE(BgL_auxz00_3570, BFALSE, BFALSE);
														}
													BgL_auxz00_3566 = (long) CINT(BgL_tmpz00_3567);
												}
												BgL_directoryz00_1440 =
													VECTOR_REF(BgL_opt1169z00_25, BgL_auxz00_3566);
											}
										else
											{	/* Unsafe/tar.scm 257 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/tar.scm 257 */
									obj_t BgL_index1176z00_1453;

									{
										long BgL_iz00_2437;

										BgL_iz00_2437 = 1L;
									BgL_search1172z00_2436:
										if ((BgL_iz00_2437 == VECTOR_LENGTH(BgL_opt1169z00_25)))
											{	/* Unsafe/tar.scm 257 */
												BgL_index1176z00_1453 = BINT(-1L);
											}
										else
											{	/* Unsafe/tar.scm 257 */
												if (
													(BgL_iz00_2437 ==
														(VECTOR_LENGTH(BgL_opt1169z00_25) - 1L)))
													{	/* Unsafe/tar.scm 257 */
														BgL_index1176z00_1453 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2039z00zz__tarz00,
															BGl_string2042z00zz__tarz00,
															BINT(VECTOR_LENGTH(BgL_opt1169z00_25)));
													}
												else
													{	/* Unsafe/tar.scm 257 */
														obj_t BgL_vz00_2447;

														BgL_vz00_2447 =
															VECTOR_REF(BgL_opt1169z00_25, BgL_iz00_2437);
														if ((BgL_vz00_2447 == BGl_keyword2035z00zz__tarz00))
															{	/* Unsafe/tar.scm 257 */
																BgL_index1176z00_1453 =
																	BINT((BgL_iz00_2437 + 1L));
															}
														else
															{
																long BgL_iz00_3592;

																BgL_iz00_3592 = (BgL_iz00_2437 + 2L);
																BgL_iz00_2437 = BgL_iz00_3592;
																goto BgL_search1172z00_2436;
															}
													}
											}
									}
									{	/* Unsafe/tar.scm 257 */
										bool_t BgL_test2240z00_3594;

										{	/* Unsafe/tar.scm 257 */
											long BgL_n1z00_2451;

											{	/* Unsafe/tar.scm 257 */
												obj_t BgL_tmpz00_3595;

												if (INTEGERP(BgL_index1176z00_1453))
													{	/* Unsafe/tar.scm 257 */
														BgL_tmpz00_3595 = BgL_index1176z00_1453;
													}
												else
													{
														obj_t BgL_auxz00_3598;

														BgL_auxz00_3598 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2043z00zz__tarz00, BINT(9397L),
															BGl_string2044z00zz__tarz00,
															BGl_string2045z00zz__tarz00,
															BgL_index1176z00_1453);
														FAILURE(BgL_auxz00_3598, BFALSE, BFALSE);
													}
												BgL_n1z00_2451 = (long) CINT(BgL_tmpz00_3595);
											}
											BgL_test2240z00_3594 = (BgL_n1z00_2451 >= 0L);
										}
										if (BgL_test2240z00_3594)
											{
												long BgL_auxz00_3604;

												{	/* Unsafe/tar.scm 257 */
													obj_t BgL_tmpz00_3605;

													if (INTEGERP(BgL_index1176z00_1453))
														{	/* Unsafe/tar.scm 257 */
															BgL_tmpz00_3605 = BgL_index1176z00_1453;
														}
													else
														{
															obj_t BgL_auxz00_3608;

															BgL_auxz00_3608 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2043z00zz__tarz00, BINT(9397L),
																BGl_string2044z00zz__tarz00,
																BGl_string2045z00zz__tarz00,
																BgL_index1176z00_1453);
															FAILURE(BgL_auxz00_3608, BFALSE, BFALSE);
														}
													BgL_auxz00_3604 = (long) CINT(BgL_tmpz00_3605);
												}
												BgL_filez00_1441 =
													VECTOR_REF(BgL_opt1169z00_25, BgL_auxz00_3604);
											}
										else
											{	/* Unsafe/tar.scm 257 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/tar.scm 257 */
									obj_t BgL_index1177z00_1455;

									{
										long BgL_iz00_2453;

										BgL_iz00_2453 = 1L;
									BgL_search1172z00_2452:
										if ((BgL_iz00_2453 == VECTOR_LENGTH(BgL_opt1169z00_25)))
											{	/* Unsafe/tar.scm 257 */
												BgL_index1177z00_1455 = BINT(-1L);
											}
										else
											{	/* Unsafe/tar.scm 257 */
												if (
													(BgL_iz00_2453 ==
														(VECTOR_LENGTH(BgL_opt1169z00_25) - 1L)))
													{	/* Unsafe/tar.scm 257 */
														BgL_index1177z00_1455 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2039z00zz__tarz00,
															BGl_string2042z00zz__tarz00,
															BINT(VECTOR_LENGTH(BgL_opt1169z00_25)));
													}
												else
													{	/* Unsafe/tar.scm 257 */
														obj_t BgL_vz00_2463;

														BgL_vz00_2463 =
															VECTOR_REF(BgL_opt1169z00_25, BgL_iz00_2453);
														if ((BgL_vz00_2463 == BGl_keyword2037z00zz__tarz00))
															{	/* Unsafe/tar.scm 257 */
																BgL_index1177z00_1455 =
																	BINT((BgL_iz00_2453 + 1L));
															}
														else
															{
																long BgL_iz00_3630;

																BgL_iz00_3630 = (BgL_iz00_2453 + 2L);
																BgL_iz00_2453 = BgL_iz00_3630;
																goto BgL_search1172z00_2452;
															}
													}
											}
									}
									{	/* Unsafe/tar.scm 257 */
										bool_t BgL_test2246z00_3632;

										{	/* Unsafe/tar.scm 257 */
											long BgL_n1z00_2467;

											{	/* Unsafe/tar.scm 257 */
												obj_t BgL_tmpz00_3633;

												if (INTEGERP(BgL_index1177z00_1455))
													{	/* Unsafe/tar.scm 257 */
														BgL_tmpz00_3633 = BgL_index1177z00_1455;
													}
												else
													{
														obj_t BgL_auxz00_3636;

														BgL_auxz00_3636 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2043z00zz__tarz00, BINT(9397L),
															BGl_string2044z00zz__tarz00,
															BGl_string2045z00zz__tarz00,
															BgL_index1177z00_1455);
														FAILURE(BgL_auxz00_3636, BFALSE, BFALSE);
													}
												BgL_n1z00_2467 = (long) CINT(BgL_tmpz00_3633);
											}
											BgL_test2246z00_3632 = (BgL_n1z00_2467 >= 0L);
										}
										if (BgL_test2246z00_3632)
											{
												long BgL_auxz00_3642;

												{	/* Unsafe/tar.scm 257 */
													obj_t BgL_tmpz00_3643;

													if (INTEGERP(BgL_index1177z00_1455))
														{	/* Unsafe/tar.scm 257 */
															BgL_tmpz00_3643 = BgL_index1177z00_1455;
														}
													else
														{
															obj_t BgL_auxz00_3646;

															BgL_auxz00_3646 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2043z00zz__tarz00, BINT(9397L),
																BGl_string2044z00zz__tarz00,
																BGl_string2045z00zz__tarz00,
																BgL_index1177z00_1455);
															FAILURE(BgL_auxz00_3646, BFALSE, BFALSE);
														}
													BgL_auxz00_3642 = (long) CINT(BgL_tmpz00_3643);
												}
												BgL_filesz00_1442 =
													VECTOR_REF(BgL_opt1169z00_25, BgL_auxz00_3642);
											}
										else
											{	/* Unsafe/tar.scm 257 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/tar.scm 257 */
									obj_t BgL_arg1416z00_1457;

									BgL_arg1416z00_1457 = VECTOR_REF(BgL_opt1169z00_25, 0L);
									{	/* Unsafe/tar.scm 257 */
										obj_t BgL_directoryz00_1458;

										BgL_directoryz00_1458 = BgL_directoryz00_1440;
										{	/* Unsafe/tar.scm 257 */
											obj_t BgL_filez00_1459;

											BgL_filez00_1459 = BgL_filez00_1441;
											return
												BGl_untarz00zz__tarz00(BgL_arg1416z00_1457,
												BgL_directoryz00_1458, BgL_filez00_1459,
												BgL_filesz00_1442);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* untar */
	BGL_EXPORTED_DEF obj_t BGl_untarz00zz__tarz00(obj_t BgL_ipz00_21,
		obj_t BgL_directoryz00_22, obj_t BgL_filez00_23, obj_t BgL_filesz00_24)
	{
		{	/* Unsafe/tar.scm 257 */
			if (INPUT_PORTP(BgL_ipz00_21))
				{	/* Unsafe/tar.scm 259 */
					if (STRINGP(BgL_filez00_23))
						{	/* Unsafe/tar.scm 262 */
							obj_t BgL_arg1425z00_1470;

							{	/* Unsafe/tar.scm 262 */
								obj_t BgL_list1426z00_1471;

								BgL_list1426z00_1471 = MAKE_YOUNG_PAIR(BgL_filez00_23, BNIL);
								BgL_arg1425z00_1470 = BgL_list1426z00_1471;
							}
							return
								BGl_untarzd2fileszd2zz__tarz00(BgL_ipz00_21,
								BgL_arg1425z00_1470);
						}
					else
						{	/* Unsafe/tar.scm 263 */
							bool_t BgL_test2251z00_3660;

							if (PAIRP(BgL_filesz00_24))
								{	/* Unsafe/tar.scm 263 */
									if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
										(BgL_filesz00_24))
										{
											obj_t BgL_l1135z00_1489;

											BgL_l1135z00_1489 = BgL_filesz00_24;
										BgL_zc3z04anonymousza31438ze3z87_1490:
											if (NULLP(BgL_l1135z00_1489))
												{	/* Unsafe/tar.scm 263 */
													BgL_test2251z00_3660 = ((bool_t) 1);
												}
											else
												{	/* Unsafe/tar.scm 263 */
													bool_t BgL_test2255z00_3667;

													{	/* Unsafe/tar.scm 263 */
														obj_t BgL_tmpz00_3668;

														BgL_tmpz00_3668 = CAR(((obj_t) BgL_l1135z00_1489));
														BgL_test2255z00_3667 = STRINGP(BgL_tmpz00_3668);
													}
													if (BgL_test2255z00_3667)
														{
															obj_t BgL_l1135z00_3672;

															BgL_l1135z00_3672 =
																CDR(((obj_t) BgL_l1135z00_1489));
															BgL_l1135z00_1489 = BgL_l1135z00_3672;
															goto BgL_zc3z04anonymousza31438ze3z87_1490;
														}
													else
														{	/* Unsafe/tar.scm 263 */
															BgL_test2251z00_3660 = ((bool_t) 0);
														}
												}
										}
									else
										{	/* Unsafe/tar.scm 263 */
											BgL_test2251z00_3660 = ((bool_t) 0);
										}
								}
							else
								{	/* Unsafe/tar.scm 263 */
									BgL_test2251z00_3660 = ((bool_t) 0);
								}
							if (BgL_test2251z00_3660)
								{	/* Unsafe/tar.scm 263 */
									return
										BGl_untarzd2fileszd2zz__tarz00(BgL_ipz00_21,
										BgL_filesz00_24);
								}
							else
								{	/* Unsafe/tar.scm 266 */
									obj_t BgL_arg1436z00_1484;

									if (STRINGP(BgL_directoryz00_22))
										{	/* Unsafe/tar.scm 266 */
											BgL_arg1436z00_1484 = BgL_directoryz00_22;
										}
									else
										{	/* Unsafe/tar.scm 266 */
											BgL_arg1436z00_1484 = BGl_pwdz00zz__osz00();
										}
									return
										BGl_untarzd2directoryzd2zz__tarz00(BgL_ipz00_21,
										BgL_arg1436z00_1484);
								}
						}
				}
			else
				{	/* Unsafe/tar.scm 259 */
					return
						BGl_bigloozd2typezd2errorz00zz__errorz00
						(BGl_string2040z00zz__tarz00, BGl_string1992z00zz__tarz00,
						BgL_ipz00_21);
				}
		}

	}



/* untar-directory */
	obj_t BGl_untarzd2directoryzd2zz__tarz00(obj_t BgL_ipz00_27,
		obj_t BgL_basez00_28)
	{
		{	/* Unsafe/tar.scm 271 */
			if (bgl_directoryp(BSTRING_TO_STRING(BgL_basez00_28)))
				{	/* Unsafe/tar.scm 272 */
					((bool_t) 0);
				}
			else
				{	/* Unsafe/tar.scm 272 */
					BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00(BgL_basez00_28);
				}
			{
				obj_t BgL_lstz00_1500;
				obj_t BgL_longnamez00_1501;

				BgL_lstz00_1500 = BNIL;
				BgL_longnamez00_1501 = BFALSE;
			BgL_zc3z04anonymousza31442ze3z87_1502:
				{	/* Unsafe/tar.scm 276 */
					obj_t BgL_hz00_1503;

					BgL_hz00_1503 =
						BGl_tarzd2readzd2headerz00zz__tarz00(BgL_ipz00_27,
						BgL_longnamez00_1501);
					if (CBOOL(BgL_hz00_1503))
						{	/* Unsafe/tar.scm 280 */
							obj_t BgL_casezd2valuezd2_1505;

							BgL_casezd2valuezd2_1505 =
								(((BgL_tarzd2headerzd2_bglt) COBJECT(
										((BgL_tarzd2headerzd2_bglt) BgL_hz00_1503)))->BgL_typez00);
							if ((BgL_casezd2valuezd2_1505 == BGl_symbol1980z00zz__tarz00))
								{	/* Unsafe/tar.scm 282 */
									obj_t BgL_pathz00_1507;

									{	/* Unsafe/tar.scm 282 */
										obj_t BgL_arg1450z00_1516;

										BgL_arg1450z00_1516 =
											(((BgL_tarzd2headerzd2_bglt) COBJECT(
													((BgL_tarzd2headerzd2_bglt) BgL_hz00_1503)))->
											BgL_namez00);
										BgL_pathz00_1507 =
											BGl_makezd2filezd2namez00zz__osz00(BgL_basez00_28,
											BgL_arg1450z00_1516);
									}
									BGl_rmzd2rfzd2zz__tarz00(BgL_pathz00_1507);
									if (BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00
										(BgL_pathz00_1507))
										{	/* Unsafe/tar.scm 285 */
											obj_t BgL_arg1445z00_1509;

											BgL_arg1445z00_1509 =
												MAKE_YOUNG_PAIR(BgL_pathz00_1507, BgL_lstz00_1500);
											{
												obj_t BgL_longnamez00_3700;
												obj_t BgL_lstz00_3699;

												BgL_lstz00_3699 = BgL_arg1445z00_1509;
												BgL_longnamez00_3700 = BFALSE;
												BgL_longnamez00_1501 = BgL_longnamez00_3700;
												BgL_lstz00_1500 = BgL_lstz00_3699;
												goto BgL_zc3z04anonymousza31442ze3z87_1502;
											}
										}
									else
										{	/* Unsafe/tar.scm 287 */
											BgL_z62iozd2errorzb0_bglt BgL_arg1446z00_1510;

											{	/* Unsafe/tar.scm 287 */
												BgL_z62iozd2errorzb0_bglt BgL_new1072z00_1511;

												{	/* Unsafe/tar.scm 287 */
													BgL_z62iozd2errorzb0_bglt BgL_new1071z00_1514;

													BgL_new1071z00_1514 =
														((BgL_z62iozd2errorzb0_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_z62iozd2errorzb0_bgl))));
													{	/* Unsafe/tar.scm 287 */
														long BgL_arg1449z00_1515;

														BgL_arg1449z00_1515 =
															BGL_CLASS_NUM(BGl_z62iozd2errorzb0zz__objectz00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1071z00_1514),
															BgL_arg1449z00_1515);
													}
													BgL_new1072z00_1511 = BgL_new1071z00_1514;
												}
												((((BgL_z62exceptionz62_bglt) COBJECT(
																((BgL_z62exceptionz62_bglt)
																	BgL_new1072z00_1511)))->BgL_fnamez00) =
													((obj_t) BFALSE), BUNSPEC);
												((((BgL_z62exceptionz62_bglt)
															COBJECT(((BgL_z62exceptionz62_bglt)
																	BgL_new1072z00_1511)))->BgL_locationz00) =
													((obj_t) BFALSE), BUNSPEC);
												{
													obj_t BgL_auxz00_3709;

													{	/* Unsafe/tar.scm 287 */
														obj_t BgL_arg1447z00_1512;

														{	/* Unsafe/tar.scm 287 */
															obj_t BgL_arg1448z00_1513;

															{	/* Unsafe/tar.scm 287 */
																obj_t BgL_classz00_2476;

																BgL_classz00_2476 =
																	BGl_z62iozd2errorzb0zz__objectz00;
																BgL_arg1448z00_1513 =
																	BGL_CLASS_ALL_FIELDS(BgL_classz00_2476);
															}
															BgL_arg1447z00_1512 =
																VECTOR_REF(BgL_arg1448z00_1513, 2L);
														}
														BgL_auxz00_3709 =
															BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
															(BgL_arg1447z00_1512);
													}
													((((BgL_z62exceptionz62_bglt) COBJECT(
																	((BgL_z62exceptionz62_bglt)
																		BgL_new1072z00_1511)))->BgL_stackz00) =
														((obj_t) BgL_auxz00_3709), BUNSPEC);
												}
												((((BgL_z62errorz62_bglt) COBJECT(
																((BgL_z62errorz62_bglt) BgL_new1072z00_1511)))->
														BgL_procz00) =
													((obj_t) BGl_symbol2039z00zz__tarz00), BUNSPEC);
												((((BgL_z62errorz62_bglt)
															COBJECT(((BgL_z62errorz62_bglt)
																	BgL_new1072z00_1511)))->BgL_msgz00) =
													((obj_t) BGl_string2046z00zz__tarz00), BUNSPEC);
												((((BgL_z62errorz62_bglt)
															COBJECT(((BgL_z62errorz62_bglt)
																	BgL_new1072z00_1511)))->BgL_objz00) =
													((obj_t) BgL_pathz00_1507), BUNSPEC);
												BgL_arg1446z00_1510 = BgL_new1072z00_1511;
											}
											return
												BGl_raisez00zz__errorz00(((obj_t) BgL_arg1446z00_1510));
										}
								}
							else
								{	/* Unsafe/tar.scm 280 */
									if ((BgL_casezd2valuezd2_1505 == BGl_symbol1970z00zz__tarz00))
										{	/* Unsafe/tar.scm 292 */
											obj_t BgL_pathz00_1518;

											{	/* Unsafe/tar.scm 292 */
												obj_t BgL_arg1461z00_1532;

												BgL_arg1461z00_1532 =
													(((BgL_tarzd2headerzd2_bglt) COBJECT(
															((BgL_tarzd2headerzd2_bglt) BgL_hz00_1503)))->
													BgL_namez00);
												BgL_pathz00_1518 =
													BGl_makezd2filezd2namez00zz__osz00(BgL_basez00_28,
													BgL_arg1461z00_1532);
											}
											{	/* Unsafe/tar.scm 292 */
												obj_t BgL_dirz00_1519;

												BgL_dirz00_1519 =
													BGl_dirnamez00zz__osz00(BgL_pathz00_1518);
												{	/* Unsafe/tar.scm 293 */

													{	/* Unsafe/tar.scm 294 */
														bool_t BgL_test2262z00_3729;

														if (fexists(BSTRING_TO_STRING(BgL_dirz00_1519)))
															{	/* Unsafe/tar.scm 294 */
																if (bgl_directoryp(BSTRING_TO_STRING
																		(BgL_dirz00_1519)))
																	{	/* Unsafe/tar.scm 294 */
																		BgL_test2262z00_3729 = ((bool_t) 0);
																	}
																else
																	{	/* Unsafe/tar.scm 294 */
																		BgL_test2262z00_3729 = ((bool_t) 1);
																	}
															}
														else
															{	/* Unsafe/tar.scm 294 */
																BgL_test2262z00_3729 = ((bool_t) 0);
															}
														if (BgL_test2262z00_3729)
															{	/* Unsafe/tar.scm 295 */
																char *BgL_stringz00_2481;

																BgL_stringz00_2481 =
																	BSTRING_TO_STRING(BgL_dirz00_1519);
																if (unlink(BgL_stringz00_2481))
																	{	/* Unsafe/tar.scm 295 */
																		((bool_t) 0);
																	}
																else
																	{	/* Unsafe/tar.scm 295 */
																		((bool_t) 1);
																	}
															}
														else
															{	/* Unsafe/tar.scm 294 */
																((bool_t) 0);
															}
													}
													if (fexists(BSTRING_TO_STRING(BgL_dirz00_1519)))
														{	/* Unsafe/tar.scm 296 */
															BFALSE;
														}
													else
														{	/* Unsafe/tar.scm 296 */
															BGl_makezd2directorieszd2zz__r4_ports_6_10_1z00
																(BgL_dirz00_1519);
															BgL_lstz00_1500 =
																MAKE_YOUNG_PAIR(BgL_dirz00_1519,
																BgL_lstz00_1500);
														}
													{	/* Unsafe/tar.scm 301 */
														obj_t BgL_zc3z04anonymousza31457ze3z87_2904;

														BgL_zc3z04anonymousza31457ze3z87_2904 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31457ze3ze5zz__tarz00,
															(int) (0L), (int) (2L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_2904,
															(int) (0L), BgL_hz00_1503);
														PROCEDURE_SET(BgL_zc3z04anonymousza31457ze3z87_2904,
															(int) (1L), BgL_ipz00_27);
														BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
															(BgL_pathz00_1518,
															BgL_zc3z04anonymousza31457ze3z87_2904);
													}
													{	/* Unsafe/tar.scm 302 */
														obj_t BgL_arg1460z00_1531;

														BgL_arg1460z00_1531 =
															MAKE_YOUNG_PAIR(BgL_pathz00_1518,
															BgL_lstz00_1500);
														{
															obj_t BgL_longnamez00_3754;
															obj_t BgL_lstz00_3753;

															BgL_lstz00_3753 = BgL_arg1460z00_1531;
															BgL_longnamez00_3754 = BFALSE;
															BgL_longnamez00_1501 = BgL_longnamez00_3754;
															BgL_lstz00_1500 = BgL_lstz00_3753;
															goto BgL_zc3z04anonymousza31442ze3z87_1502;
														}
													}
												}
											}
										}
									else
										{	/* Unsafe/tar.scm 280 */
											if (
												(BgL_casezd2valuezd2_1505 ==
													BGl_symbol1974z00zz__tarz00))
												{	/* Unsafe/tar.scm 305 */
													obj_t BgL_pathz00_1535;

													{	/* Unsafe/tar.scm 305 */
														obj_t BgL_arg1466z00_1539;

														BgL_arg1466z00_1539 =
															(((BgL_tarzd2headerzd2_bglt) COBJECT(
																	((BgL_tarzd2headerzd2_bglt) BgL_hz00_1503)))->
															BgL_namez00);
														BgL_pathz00_1535 =
															BGl_makezd2filezd2namez00zz__osz00(BgL_basez00_28,
															BgL_arg1466z00_1539);
													}
													if (fexists(BSTRING_TO_STRING(BgL_pathz00_1535)))
														{	/* Unsafe/tar.scm 307 */
															char *BgL_stringz00_2489;

															BgL_stringz00_2489 =
																BSTRING_TO_STRING(BgL_pathz00_1535);
															if (unlink(BgL_stringz00_2489))
																{	/* Unsafe/tar.scm 307 */
																	((bool_t) 0);
																}
															else
																{	/* Unsafe/tar.scm 307 */
																	((bool_t) 1);
																}
														}
													else
														{	/* Unsafe/tar.scm 306 */
															((bool_t) 0);
														}
													{	/* Unsafe/tar.scm 308 */
														obj_t BgL_arg1464z00_1537;

														BgL_arg1464z00_1537 =
															(((BgL_tarzd2headerzd2_bglt) COBJECT(
																	((BgL_tarzd2headerzd2_bglt) BgL_hz00_1503)))->
															BgL_linknamez00);
														bgl_symlink(BSTRING_TO_STRING(BgL_arg1464z00_1537),
															BSTRING_TO_STRING(BgL_pathz00_1535));
													}
													{	/* Unsafe/tar.scm 309 */
														obj_t BgL_arg1465z00_1538;

														BgL_arg1465z00_1538 =
															MAKE_YOUNG_PAIR(BgL_pathz00_1535,
															BgL_lstz00_1500);
														{
															obj_t BgL_longnamez00_3773;
															obj_t BgL_lstz00_3772;

															BgL_lstz00_3772 = BgL_arg1465z00_1538;
															BgL_longnamez00_3773 = BFALSE;
															BgL_longnamez00_1501 = BgL_longnamez00_3773;
															BgL_lstz00_1500 = BgL_lstz00_3772;
															goto BgL_zc3z04anonymousza31442ze3z87_1502;
														}
													}
												}
											else
												{	/* Unsafe/tar.scm 280 */
													if (
														(BgL_casezd2valuezd2_1505 ==
															BGl_symbol1986z00zz__tarz00))
														{	/* Unsafe/tar.scm 312 */
															long BgL_nz00_1542;

															{	/* Unsafe/tar.scm 312 */
																long BgL_arg1477z00_1553;

																BgL_arg1477z00_1553 =
																	(((BgL_tarzd2headerzd2_bglt) COBJECT(
																			((BgL_tarzd2headerzd2_bglt)
																				BgL_hz00_1503)))->BgL_siza7eza7);
																BgL_nz00_1542 =
																	BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
																	(long) (BgL_arg1477z00_1553), 512L);
															}
															{	/* Unsafe/tar.scm 317 */

																{	/* Unsafe/tar.scm 318 */
																	obj_t BgL_arg1468z00_1546;

																	BgL_arg1468z00_1546 =
																		BGl_tarzd2readzd2blockz00zz__tarz00
																		(BgL_hz00_1503, BgL_ipz00_27);
																	{
																		obj_t BgL_longnamez00_3781;

																		BgL_longnamez00_3781 = BgL_arg1468z00_1546;
																		BgL_longnamez00_1501 = BgL_longnamez00_3781;
																		goto BgL_zc3z04anonymousza31442ze3z87_1502;
																	}
																}
															}
														}
													else
														{	/* Unsafe/tar.scm 321 */
															BgL_z62iozd2parsezd2errorz62_bglt
																BgL_arg1479z00_1555;
															{	/* Unsafe/tar.scm 321 */
																BgL_z62iozd2parsezd2errorz62_bglt
																	BgL_new1076z00_1556;
																{	/* Unsafe/tar.scm 321 */
																	BgL_z62iozd2parsezd2errorz62_bglt
																		BgL_new1075z00_1561;
																	BgL_new1075z00_1561 =
																		((BgL_z62iozd2parsezd2errorz62_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_z62iozd2parsezd2errorz62_bgl))));
																	{	/* Unsafe/tar.scm 321 */
																		long BgL_arg1484z00_1562;

																		BgL_arg1484z00_1562 =
																			BGL_CLASS_NUM
																			(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1075z00_1561),
																			BgL_arg1484z00_1562);
																	}
																	BgL_new1076z00_1556 = BgL_new1075z00_1561;
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1076z00_1556)))->
																		BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																((((BgL_z62exceptionz62_bglt)
																			COBJECT(((BgL_z62exceptionz62_bglt)
																					BgL_new1076z00_1556)))->
																		BgL_locationz00) =
																	((obj_t) BFALSE), BUNSPEC);
																{
																	obj_t BgL_auxz00_3790;

																	{	/* Unsafe/tar.scm 321 */
																		obj_t BgL_arg1480z00_1557;

																		{	/* Unsafe/tar.scm 321 */
																			obj_t BgL_arg1481z00_1558;

																			{	/* Unsafe/tar.scm 321 */
																				obj_t BgL_classz00_2506;

																				BgL_classz00_2506 =
																					BGl_z62iozd2parsezd2errorz62zz__objectz00;
																				BgL_arg1481z00_1558 =
																					BGL_CLASS_ALL_FIELDS
																					(BgL_classz00_2506);
																			}
																			BgL_arg1480z00_1557 =
																				VECTOR_REF(BgL_arg1481z00_1558, 2L);
																		}
																		BgL_auxz00_3790 =
																			BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																			(BgL_arg1480z00_1557);
																	}
																	((((BgL_z62exceptionz62_bglt) COBJECT(
																					((BgL_z62exceptionz62_bglt)
																						BgL_new1076z00_1556)))->
																			BgL_stackz00) =
																		((obj_t) BgL_auxz00_3790), BUNSPEC);
																}
																((((BgL_z62errorz62_bglt) COBJECT(
																				((BgL_z62errorz62_bglt)
																					BgL_new1076z00_1556)))->BgL_procz00) =
																	((obj_t) BGl_symbol2039z00zz__tarz00),
																	BUNSPEC);
																{
																	obj_t BgL_auxz00_3798;

																	{	/* Unsafe/tar.scm 323 */
																		obj_t BgL_arg1482z00_1559;

																		BgL_arg1482z00_1559 =
																			(((BgL_tarzd2headerzd2_bglt) COBJECT(
																					((BgL_tarzd2headerzd2_bglt)
																						BgL_hz00_1503)))->BgL_typez00);
																		{	/* Unsafe/tar.scm 323 */
																			obj_t BgL_list1483z00_1560;

																			BgL_list1483z00_1560 =
																				MAKE_YOUNG_PAIR(BgL_arg1482z00_1559,
																				BNIL);
																			BgL_auxz00_3798 =
																				BGl_formatz00zz__r4_output_6_10_3z00
																				(BGl_string2047z00zz__tarz00,
																				BgL_list1483z00_1560);
																	}}
																	((((BgL_z62errorz62_bglt) COBJECT(
																					((BgL_z62errorz62_bglt)
																						BgL_new1076z00_1556)))->
																			BgL_msgz00) =
																		((obj_t) BgL_auxz00_3798), BUNSPEC);
																}
																((((BgL_z62errorz62_bglt) COBJECT(
																				((BgL_z62errorz62_bglt)
																					BgL_new1076z00_1556)))->BgL_objz00) =
																	((obj_t) (((BgL_tarzd2headerzd2_bglt)
																				COBJECT(((BgL_tarzd2headerzd2_bglt)
																						BgL_hz00_1503)))->BgL_namez00)),
																	BUNSPEC);
																BgL_arg1479z00_1555 = BgL_new1076z00_1556;
															}
															return
																BGl_raisez00zz__errorz00(
																((obj_t) BgL_arg1479z00_1555));
														}
												}
										}
								}
						}
					else
						{	/* Unsafe/tar.scm 277 */
							return bgl_reverse_bang(BgL_lstz00_1500);
						}
				}
			}
		}

	}



/* &<@anonymous:1457> */
	obj_t BGl_z62zc3z04anonymousza31457ze3ze5zz__tarz00(obj_t BgL_envz00_2905)
	{
		{	/* Unsafe/tar.scm 300 */
			{	/* Unsafe/tar.scm 301 */
				obj_t BgL_hz00_2906;
				obj_t BgL_ipz00_2907;

				BgL_hz00_2906 = PROCEDURE_REF(BgL_envz00_2905, (int) (0L));
				BgL_ipz00_2907 = ((obj_t) PROCEDURE_REF(BgL_envz00_2905, (int) (1L)));
				{	/* Unsafe/tar.scm 301 */
					obj_t BgL_arg1458z00_3087;
					obj_t BgL_arg1459z00_3088;

					BgL_arg1458z00_3087 =
						BGl_tarzd2readzd2blockz00zz__tarz00(BgL_hz00_2906, BgL_ipz00_2907);
					{	/* Unsafe/tar.scm 301 */
						obj_t BgL_tmpz00_3818;

						BgL_tmpz00_3818 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1459z00_3088 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3818);
					}
					return bgl_display_obj(BgL_arg1458z00_3087, BgL_arg1459z00_3088);
				}
			}
		}

	}



/* untar-files */
	obj_t BGl_untarzd2fileszd2zz__tarz00(obj_t BgL_ipz00_29,
		obj_t BgL_filesz00_30)
	{
		{	/* Unsafe/tar.scm 329 */
			{

			BgL_zc3z04anonymousza31485ze3z87_1565:
				{	/* Unsafe/tar.scm 331 */
					obj_t BgL_hz00_1566;

					{	/* Unsafe/tar.scm 331 */

						{	/* Unsafe/tar.scm 331 */

							BgL_hz00_1566 =
								BGl_tarzd2readzd2headerz00zz__tarz00(BgL_ipz00_29, BFALSE);
						}
					}
					{	/* Unsafe/tar.scm 332 */
						bool_t BgL_test2271z00_3823;

						{	/* Unsafe/tar.scm 332 */
							obj_t BgL_classz00_2508;

							BgL_classz00_2508 = BGl_tarzd2headerzd2zz__tarz00;
							if (BGL_OBJECTP(BgL_hz00_1566))
								{	/* Unsafe/tar.scm 332 */
									BgL_objectz00_bglt BgL_arg1899z00_2510;

									BgL_arg1899z00_2510 = (BgL_objectz00_bglt) (BgL_hz00_1566);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Unsafe/tar.scm 332 */
											long BgL_idxz00_2516;

											BgL_idxz00_2516 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1899z00_2510);
											BgL_test2271z00_3823 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2516 + 1L)) == BgL_classz00_2508);
										}
									else
										{	/* Unsafe/tar.scm 332 */
											bool_t BgL_res1957z00_2541;

											{	/* Unsafe/tar.scm 332 */
												obj_t BgL_oclassz00_2524;

												{	/* Unsafe/tar.scm 332 */
													obj_t BgL_arg1910z00_2532;
													long BgL_arg1911z00_2533;

													BgL_arg1910z00_2532 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Unsafe/tar.scm 332 */
														long BgL_arg1912z00_2534;

														BgL_arg1912z00_2534 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1899z00_2510);
														BgL_arg1911z00_2533 =
															(BgL_arg1912z00_2534 - OBJECT_TYPE);
													}
													BgL_oclassz00_2524 =
														VECTOR_REF(BgL_arg1910z00_2532,
														BgL_arg1911z00_2533);
												}
												{	/* Unsafe/tar.scm 332 */
													bool_t BgL__ortest_1121z00_2525;

													BgL__ortest_1121z00_2525 =
														(BgL_classz00_2508 == BgL_oclassz00_2524);
													if (BgL__ortest_1121z00_2525)
														{	/* Unsafe/tar.scm 332 */
															BgL_res1957z00_2541 = BgL__ortest_1121z00_2525;
														}
													else
														{	/* Unsafe/tar.scm 332 */
															long BgL_odepthz00_2526;

															{	/* Unsafe/tar.scm 332 */
																obj_t BgL_arg1896z00_2527;

																BgL_arg1896z00_2527 = (BgL_oclassz00_2524);
																BgL_odepthz00_2526 =
																	BGL_CLASS_DEPTH(BgL_arg1896z00_2527);
															}
															if ((1L < BgL_odepthz00_2526))
																{	/* Unsafe/tar.scm 332 */
																	obj_t BgL_arg1893z00_2529;

																	{	/* Unsafe/tar.scm 332 */
																		obj_t BgL_arg1894z00_2530;

																		BgL_arg1894z00_2530 = (BgL_oclassz00_2524);
																		BgL_arg1893z00_2529 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1894z00_2530, 1L);
																	}
																	BgL_res1957z00_2541 =
																		(BgL_arg1893z00_2529 == BgL_classz00_2508);
																}
															else
																{	/* Unsafe/tar.scm 332 */
																	BgL_res1957z00_2541 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2271z00_3823 = BgL_res1957z00_2541;
										}
								}
							else
								{	/* Unsafe/tar.scm 332 */
									BgL_test2271z00_3823 = ((bool_t) 0);
								}
						}
						if (BgL_test2271z00_3823)
							{	/* Unsafe/tar.scm 334 */
								obj_t BgL_casezd2valuezd2_1569;

								BgL_casezd2valuezd2_1569 =
									(((BgL_tarzd2headerzd2_bglt) COBJECT(
											((BgL_tarzd2headerzd2_bglt) BgL_hz00_1566)))->
									BgL_typez00);
								if ((BgL_casezd2valuezd2_1569 == BGl_symbol1980z00zz__tarz00))
									{	/* Unsafe/tar.scm 334 */
										goto BgL_zc3z04anonymousza31485ze3z87_1565;
									}
								else
									{	/* Unsafe/tar.scm 334 */
										if (
											(BgL_casezd2valuezd2_1569 == BGl_symbol1970z00zz__tarz00))
											{	/* Unsafe/tar.scm 338 */
												obj_t BgL_bz00_1572;
												obj_t BgL_nz00_1573;

												BgL_bz00_1572 =
													BGl_tarzd2readzd2blockz00zz__tarz00(BgL_hz00_1566,
													BgL_ipz00_29);
												BgL_nz00_1573 =
													(((BgL_tarzd2headerzd2_bglt)
														COBJECT(((BgL_tarzd2headerzd2_bglt)
																BgL_hz00_1566)))->BgL_namez00);
												if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
														(BgL_nz00_1573, BgL_filesz00_30)))
													{	/* Unsafe/tar.scm 340 */
														return BgL_bz00_1572;
													}
												else
													{	/* Unsafe/tar.scm 340 */
														goto BgL_zc3z04anonymousza31485ze3z87_1565;
													}
											}
										else
											{	/* Unsafe/tar.scm 334 */
												return BFALSE;
											}
									}
							}
						else
							{	/* Unsafe/tar.scm 332 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__tarz00(void)
	{
		{	/* Unsafe/tar.scm 17 */
			{	/* Unsafe/tar.scm 55 */
				obj_t BgL_arg1495z00_1581;
				obj_t BgL_arg1497z00_1582;

				{	/* Unsafe/tar.scm 55 */
					obj_t BgL_v1138z00_1608;

					BgL_v1138z00_1608 = create_vector(14L);
					VECTOR_SET(BgL_v1138z00_1608, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol1994z00zz__tarz00, BGl_proc2049z00zz__tarz00,
							BGl_proc2048z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2050z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol1996z00zz__tarz00, BGl_proc2053z00zz__tarz00,
							BGl_proc2052z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2054z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol1998z00zz__tarz00, BGl_proc2056z00zz__tarz00,
							BGl_proc2055z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2054z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2000z00zz__tarz00, BGl_proc2058z00zz__tarz00,
							BGl_proc2057z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2054z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 4L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2002z00zz__tarz00, BGl_proc2060z00zz__tarz00,
							BGl_proc2059z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2061z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 5L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2004z00zz__tarz00, BGl_proc2064z00zz__tarz00,
							BGl_proc2063z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2065z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 6L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2069z00zz__tarz00, BGl_proc2068z00zz__tarz00,
							BGl_proc2067z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2054z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 7L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2073z00zz__tarz00, BGl_proc2072z00zz__tarz00,
							BGl_proc2071z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2075z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 8L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2008z00zz__tarz00, BGl_proc2078z00zz__tarz00,
							BGl_proc2077z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2050z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 9L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2010z00zz__tarz00, BGl_proc2080z00zz__tarz00,
							BGl_proc2079z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2050z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 10L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2012z00zz__tarz00, BGl_proc2082z00zz__tarz00,
							BGl_proc2081z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2050z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 11L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2014z00zz__tarz00, BGl_proc2084z00zz__tarz00,
							BGl_proc2083z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2050z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 12L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2016z00zz__tarz00, BGl_proc2086z00zz__tarz00,
							BGl_proc2085z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2054z00zz__tarz00));
					VECTOR_SET(BgL_v1138z00_1608, 13L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2018z00zz__tarz00, BGl_proc2088z00zz__tarz00,
							BGl_proc2087z00zz__tarz00, ((bool_t) 1), ((bool_t) 0), BFALSE,
							BFALSE, BGl_symbol2054z00zz__tarz00));
					BgL_arg1495z00_1581 = BgL_v1138z00_1608;
				}
				{	/* Unsafe/tar.scm 55 */
					obj_t BgL_v1139z00_1749;

					BgL_v1139z00_1749 = create_vector(0L);
					BgL_arg1497z00_1582 = BgL_v1139z00_1749;
				}
				return (BGl_tarzd2headerzd2zz__tarz00 =
					BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2092z00zz__tarz00,
						BGl_symbol2093z00zz__tarz00, BGl_objectz00zz__objectz00, 38086L,
						BGl_proc2091z00zz__tarz00, BGl_proc2090z00zz__tarz00, BFALSE,
						BGl_proc2089z00zz__tarz00, BFALSE, BgL_arg1495z00_1581,
						BgL_arg1497z00_1582), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1502> */
	obj_t BGl_z62zc3z04anonymousza31502ze3ze5zz__tarz00(obj_t BgL_envz00_2939,
		obj_t BgL_new1055z00_2940)
	{
		{	/* Unsafe/tar.scm 55 */
			{
				BgL_tarzd2headerzd2_bglt BgL_auxz00_3889;

				((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_new1055z00_2940)))->
						BgL_namez00) = ((obj_t) BGl_string1993z00zz__tarz00), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_modez00) = ((long) 0L), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_uidz00) = ((long) 0L), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_gidz00) = ((long) 0L), BUNSPEC);
				{
					long BgL_auxz00_3898;

					{	/* Unsafe/tar.scm 55 */

						BgL_auxz00_3898 =
							BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00
							(BGl_string2095z00zz__tarz00, 10L);
					}
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
									((BgL_tarzd2headerzd2_bglt) BgL_new1055z00_2940)))->
							BgL_siza7eza7) = ((long) BgL_auxz00_3898), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_3902;

					{	/* Unsafe/tar.scm 55 */
						BGL_LONGLONG_T BgL_arg1884z00_3090;

						BgL_arg1884z00_3090 = bgl_current_nanoseconds();
						BgL_auxz00_3902 = bgl_nanoseconds_to_date(BgL_arg1884z00_3090);
					}
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
									((BgL_tarzd2headerzd2_bglt) BgL_new1055z00_2940)))->
							BgL_mtimez00) = ((obj_t) BgL_auxz00_3902), BUNSPEC);
				}
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_new1055z00_2940)))->
						BgL_checksumz00) = ((long) 0L), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_typez00) =
					((obj_t) BGl_symbol2096z00zz__tarz00), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_linknamez00) =
					((obj_t) BGl_string1993z00zz__tarz00), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_magicz00) =
					((obj_t) BGl_string1993z00zz__tarz00), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_unamez00) =
					((obj_t) BGl_string1993z00zz__tarz00), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_gnamez00) =
					((obj_t) BGl_string1993z00zz__tarz00), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_devmajorz00) =
					((long) 0L), BUNSPEC);
				((((BgL_tarzd2headerzd2_bglt) COBJECT(((BgL_tarzd2headerzd2_bglt)
									BgL_new1055z00_2940)))->BgL_devminorz00) =
					((long) 0L), BUNSPEC);
				BgL_auxz00_3889 = ((BgL_tarzd2headerzd2_bglt) BgL_new1055z00_2940);
				return ((obj_t) BgL_auxz00_3889);
			}
		}

	}



/* &lambda1500 */
	BgL_tarzd2headerzd2_bglt BGl_z62lambda1500z62zz__tarz00(obj_t BgL_envz00_2941)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				BgL_tarzd2headerzd2_bglt BgL_new1054z00_3091;

				BgL_new1054z00_3091 =
					((BgL_tarzd2headerzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_tarzd2headerzd2_bgl))));
				{	/* Unsafe/tar.scm 55 */
					long BgL_arg1501z00_3092;

					BgL_arg1501z00_3092 = BGL_CLASS_NUM(BGl_tarzd2headerzd2zz__tarz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1054z00_3091), BgL_arg1501z00_3092);
				}
				return BgL_new1054z00_3091;
			}
		}

	}



/* &lambda1498 */
	BgL_tarzd2headerzd2_bglt BGl_z62lambda1498z62zz__tarz00(obj_t BgL_envz00_2942,
		obj_t BgL_name1040z00_2943, obj_t BgL_mode1041z00_2944,
		obj_t BgL_uid1042z00_2945, obj_t BgL_gid1043z00_2946,
		obj_t BgL_siza7e1044za7_2947, obj_t BgL_mtime1045z00_2948,
		obj_t BgL_checksum1046z00_2949, obj_t BgL_type1047z00_2950,
		obj_t BgL_linkname1048z00_2951, obj_t BgL_magic1049z00_2952,
		obj_t BgL_uname1050z00_2953, obj_t BgL_gname1051z00_2954,
		obj_t BgL_devmajor1052z00_2955, obj_t BgL_devminor1053z00_2956)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_mode1041z00_3094;
				long BgL_uid1042z00_3095;
				long BgL_gid1043z00_3096;
				long BgL_siza7e1044za7_3097;
				long BgL_checksum1046z00_3099;
				long BgL_devmajor1052z00_3105;
				long BgL_devminor1053z00_3106;

				BgL_mode1041z00_3094 = (long) CINT(BgL_mode1041z00_2944);
				BgL_uid1042z00_3095 = (long) CINT(BgL_uid1042z00_2945);
				BgL_gid1043z00_3096 = (long) CINT(BgL_gid1043z00_2946);
				BgL_siza7e1044za7_3097 = BELONG_TO_LONG(BgL_siza7e1044za7_2947);
				BgL_checksum1046z00_3099 = (long) CINT(BgL_checksum1046z00_2949);
				BgL_devmajor1052z00_3105 = (long) CINT(BgL_devmajor1052z00_2955);
				BgL_devminor1053z00_3106 = (long) CINT(BgL_devminor1053z00_2956);
				{	/* Unsafe/tar.scm 55 */
					BgL_tarzd2headerzd2_bglt BgL_new1079z00_3107;

					{	/* Unsafe/tar.scm 55 */
						BgL_tarzd2headerzd2_bglt BgL_new1078z00_3108;

						BgL_new1078z00_3108 =
							((BgL_tarzd2headerzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_tarzd2headerzd2_bgl))));
						{	/* Unsafe/tar.scm 55 */
							long BgL_arg1499z00_3109;

							BgL_arg1499z00_3109 =
								BGL_CLASS_NUM(BGl_tarzd2headerzd2zz__tarz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1078z00_3108),
								BgL_arg1499z00_3109);
						}
						BgL_new1079z00_3107 = BgL_new1078z00_3108;
					}
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_namez00) = ((obj_t) ((obj_t) BgL_name1040z00_2943)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_modez00) = ((long) BgL_mode1041z00_3094), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_uidz00) = ((long) BgL_uid1042z00_3095), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_gidz00) = ((long) BgL_gid1043z00_3096), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_siza7eza7) = ((long) BgL_siza7e1044za7_3097), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_mtimez00) =
						((obj_t) ((obj_t) BgL_mtime1045z00_2948)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_checksumz00) = ((long) BgL_checksum1046z00_3099), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_typez00) = ((obj_t) ((obj_t) BgL_type1047z00_2950)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_linknamez00) =
						((obj_t) ((obj_t) BgL_linkname1048z00_2951)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_magicz00) =
						((obj_t) ((obj_t) BgL_magic1049z00_2952)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_unamez00) =
						((obj_t) ((obj_t) BgL_uname1050z00_2953)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_gnamez00) =
						((obj_t) ((obj_t) BgL_gname1051z00_2954)), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_devmajorz00) = ((long) BgL_devmajor1052z00_3105), BUNSPEC);
					((((BgL_tarzd2headerzd2_bglt) COBJECT(BgL_new1079z00_3107))->
							BgL_devminorz00) = ((long) BgL_devminor1053z00_3106), BUNSPEC);
					return BgL_new1079z00_3107;
				}
			}
		}

	}



/* &lambda1587 */
	obj_t BGl_z62lambda1587z62zz__tarz00(obj_t BgL_envz00_2957,
		obj_t BgL_oz00_2958, obj_t BgL_vz00_2959)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3111;

				BgL_vz00_3111 = (long) CINT(BgL_vz00_2959);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_2958)))->BgL_devminorz00) =
					((long) BgL_vz00_3111), BUNSPEC);
		}}

	}



/* &lambda1586 */
	obj_t BGl_z62lambda1586z62zz__tarz00(obj_t BgL_envz00_2960,
		obj_t BgL_oz00_2961)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				BINT(
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2961)))->BgL_devminorz00));
		}

	}



/* &lambda1582 */
	obj_t BGl_z62lambda1582z62zz__tarz00(obj_t BgL_envz00_2962,
		obj_t BgL_oz00_2963, obj_t BgL_vz00_2964)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3114;

				BgL_vz00_3114 = (long) CINT(BgL_vz00_2964);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_2963)))->BgL_devmajorz00) =
					((long) BgL_vz00_3114), BUNSPEC);
		}}

	}



/* &lambda1581 */
	obj_t BGl_z62lambda1581z62zz__tarz00(obj_t BgL_envz00_2965,
		obj_t BgL_oz00_2966)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				BINT(
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2966)))->BgL_devmajorz00));
		}

	}



/* &lambda1577 */
	obj_t BGl_z62lambda1577z62zz__tarz00(obj_t BgL_envz00_2967,
		obj_t BgL_oz00_2968, obj_t BgL_vz00_2969)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2968)))->BgL_gnamez00) =
				((obj_t) ((obj_t) BgL_vz00_2969)), BUNSPEC);
		}

	}



/* &lambda1576 */
	obj_t BGl_z62lambda1576z62zz__tarz00(obj_t BgL_envz00_2970,
		obj_t BgL_oz00_2971)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_2971)))->BgL_gnamez00);
		}

	}



/* &lambda1567 */
	obj_t BGl_z62lambda1567z62zz__tarz00(obj_t BgL_envz00_2972,
		obj_t BgL_oz00_2973, obj_t BgL_vz00_2974)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2973)))->BgL_unamez00) =
				((obj_t) ((obj_t) BgL_vz00_2974)), BUNSPEC);
		}

	}



/* &lambda1566 */
	obj_t BGl_z62lambda1566z62zz__tarz00(obj_t BgL_envz00_2975,
		obj_t BgL_oz00_2976)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_2976)))->BgL_unamez00);
		}

	}



/* &lambda1561 */
	obj_t BGl_z62lambda1561z62zz__tarz00(obj_t BgL_envz00_2977,
		obj_t BgL_oz00_2978, obj_t BgL_vz00_2979)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2978)))->BgL_magicz00) =
				((obj_t) ((obj_t) BgL_vz00_2979)), BUNSPEC);
		}

	}



/* &lambda1560 */
	obj_t BGl_z62lambda1560z62zz__tarz00(obj_t BgL_envz00_2980,
		obj_t BgL_oz00_2981)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_2981)))->BgL_magicz00);
		}

	}



/* &lambda1556 */
	obj_t BGl_z62lambda1556z62zz__tarz00(obj_t BgL_envz00_2982,
		obj_t BgL_oz00_2983, obj_t BgL_vz00_2984)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2983)))->BgL_linknamez00) =
				((obj_t) ((obj_t) BgL_vz00_2984)), BUNSPEC);
		}

	}



/* &lambda1555 */
	obj_t BGl_z62lambda1555z62zz__tarz00(obj_t BgL_envz00_2985,
		obj_t BgL_oz00_2986)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_2986)))->BgL_linknamez00);
		}

	}



/* &lambda1551 */
	obj_t BGl_z62lambda1551z62zz__tarz00(obj_t BgL_envz00_2987,
		obj_t BgL_oz00_2988, obj_t BgL_vz00_2989)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2988)))->BgL_typez00) =
				((obj_t) ((obj_t) BgL_vz00_2989)), BUNSPEC);
		}

	}



/* &lambda1550 */
	obj_t BGl_z62lambda1550z62zz__tarz00(obj_t BgL_envz00_2990,
		obj_t BgL_oz00_2991)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_2991)))->BgL_typez00);
		}

	}



/* &lambda1545 */
	obj_t BGl_z62lambda1545z62zz__tarz00(obj_t BgL_envz00_2992,
		obj_t BgL_oz00_2993, obj_t BgL_vz00_2994)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3132;

				BgL_vz00_3132 = (long) CINT(BgL_vz00_2994);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_2993)))->BgL_checksumz00) =
					((long) BgL_vz00_3132), BUNSPEC);
		}}

	}



/* &lambda1544 */
	obj_t BGl_z62lambda1544z62zz__tarz00(obj_t BgL_envz00_2995,
		obj_t BgL_oz00_2996)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				BINT(
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2996)))->BgL_checksumz00));
		}

	}



/* &lambda1538 */
	obj_t BGl_z62lambda1538z62zz__tarz00(obj_t BgL_envz00_2997,
		obj_t BgL_oz00_2998, obj_t BgL_vz00_2999)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_2998)))->BgL_mtimez00) =
				((obj_t) ((obj_t) BgL_vz00_2999)), BUNSPEC);
		}

	}



/* &lambda1537 */
	obj_t BGl_z62lambda1537z62zz__tarz00(obj_t BgL_envz00_3000,
		obj_t BgL_oz00_3001)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_3001)))->BgL_mtimez00);
		}

	}



/* &lambda1530 */
	obj_t BGl_z62lambda1530z62zz__tarz00(obj_t BgL_envz00_3002,
		obj_t BgL_oz00_3003, obj_t BgL_vz00_3004)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3138;

				BgL_vz00_3138 = BELONG_TO_LONG(BgL_vz00_3004);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_3003)))->BgL_siza7eza7) =
					((long) BgL_vz00_3138), BUNSPEC);
		}}

	}



/* &lambda1529 */
	obj_t BGl_z62lambda1529z62zz__tarz00(obj_t BgL_envz00_3005,
		obj_t BgL_oz00_3006)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_tmpz00_4012;

				BgL_tmpz00_4012 =
					(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_3006)))->BgL_siza7eza7);
				return make_belong(BgL_tmpz00_4012);
			}
		}

	}



/* &lambda1525 */
	obj_t BGl_z62lambda1525z62zz__tarz00(obj_t BgL_envz00_3007,
		obj_t BgL_oz00_3008, obj_t BgL_vz00_3009)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3141;

				BgL_vz00_3141 = (long) CINT(BgL_vz00_3009);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_3008)))->BgL_gidz00) =
					((long) BgL_vz00_3141), BUNSPEC);
		}}

	}



/* &lambda1524 */
	obj_t BGl_z62lambda1524z62zz__tarz00(obj_t BgL_envz00_3010,
		obj_t BgL_oz00_3011)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				BINT(
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_3011)))->BgL_gidz00));
		}

	}



/* &lambda1519 */
	obj_t BGl_z62lambda1519z62zz__tarz00(obj_t BgL_envz00_3012,
		obj_t BgL_oz00_3013, obj_t BgL_vz00_3014)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3144;

				BgL_vz00_3144 = (long) CINT(BgL_vz00_3014);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_3013)))->BgL_uidz00) =
					((long) BgL_vz00_3144), BUNSPEC);
		}}

	}



/* &lambda1518 */
	obj_t BGl_z62lambda1518z62zz__tarz00(obj_t BgL_envz00_3015,
		obj_t BgL_oz00_3016)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				BINT(
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_3016)))->BgL_uidz00));
		}

	}



/* &lambda1513 */
	obj_t BGl_z62lambda1513z62zz__tarz00(obj_t BgL_envz00_3017,
		obj_t BgL_oz00_3018, obj_t BgL_vz00_3019)
	{
		{	/* Unsafe/tar.scm 55 */
			{	/* Unsafe/tar.scm 55 */
				long BgL_vz00_3147;

				BgL_vz00_3147 = (long) CINT(BgL_vz00_3019);
				return
					((((BgL_tarzd2headerzd2_bglt) COBJECT(
								((BgL_tarzd2headerzd2_bglt) BgL_oz00_3018)))->BgL_modez00) =
					((long) BgL_vz00_3147), BUNSPEC);
		}}

	}



/* &lambda1512 */
	obj_t BGl_z62lambda1512z62zz__tarz00(obj_t BgL_envz00_3020,
		obj_t BgL_oz00_3021)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				BINT(
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_3021)))->BgL_modez00));
		}

	}



/* &lambda1508 */
	obj_t BGl_z62lambda1508z62zz__tarz00(obj_t BgL_envz00_3022,
		obj_t BgL_oz00_3023, obj_t BgL_vz00_3024)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				((((BgL_tarzd2headerzd2_bglt) COBJECT(
							((BgL_tarzd2headerzd2_bglt) BgL_oz00_3023)))->BgL_namez00) =
				((obj_t) ((obj_t) BgL_vz00_3024)), BUNSPEC);
		}

	}



/* &lambda1507 */
	obj_t BGl_z62lambda1507z62zz__tarz00(obj_t BgL_envz00_3025,
		obj_t BgL_oz00_3026)
	{
		{	/* Unsafe/tar.scm 55 */
			return
				(((BgL_tarzd2headerzd2_bglt) COBJECT(
						((BgL_tarzd2headerzd2_bglt) BgL_oz00_3026)))->BgL_namez00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__tarz00(void)
	{
		{	/* Unsafe/tar.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__tarz00(void)
	{
		{	/* Unsafe/tar.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__tarz00(void)
	{
		{	/* Unsafe/tar.scm 17 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2094z00zz__tarz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2094z00zz__tarz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2094z00zz__tarz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2094z00zz__tarz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2094z00zz__tarz00));
			return
				BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2094z00zz__tarz00));
		}

	}

#ifdef __cplusplus
}
#endif
