/*===========================================================================*/
/*   (Unsafe/crc.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/crc.scm -indent -o objs/obj_u/Unsafe/crc.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___CRC_TYPE_DEFINITIONS
#define BGL___CRC_TYPE_DEFINITIONS
#endif													// BGL___CRC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62crczd2nameszb0zz__crcz00(obj_t);
	static obj_t BGl_crczd2fastzd2mmapz00zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62crczd2longzd2lez62zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__crcz00 = BUNSPEC;
	extern obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_getzd2crczd2zz__crcz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__crcz00(long,
		char *);
	static obj_t BGl_z62crczd2longzb0zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2crcsza2z00zz__crcz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_crczd2polynomialzd2lez00zz__crcz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_crczd2lengthzd2zz__crcz00(obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_crczd2nameszd2zz__crcz00(void);
	static obj_t BGl_toplevelzd2initzd2zz__crcz00(void);
	static obj_t BGl_z62zc3z04anonymousza31646ze32187ze5zz__crcz00(obj_t);
	static obj_t BGl_z62crczd2lengthzb0zz__crcz00(obj_t, obj_t);
	static obj_t BGl_z62crczd2polynomialzb0zz__crcz00(obj_t, obj_t);
	static obj_t BGl_keyword2349z00zz__crcz00 = BUNSPEC;
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__crcz00(void);
	static obj_t BGl_symbol2301z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2303z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zz__crcz00(void);
	static obj_t BGl_symbol2305z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2307z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2309z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_z62crczd2llongzb0zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_registerzd2crcz12zc0zz__crcz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__crcz00(void);
	static obj_t BGl_keyword2351z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_keyword2353z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__crcz00(void);
	static obj_t BGl_symbol2311z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_z62crczd2elongzd2lez62zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2315z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2318z00zz__crcz00 = BUNSPEC;
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_symbol2322z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_z62crczd2llongzd2lez62zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long BGl_crczd2longzd2lez00zz__crcz00(unsigned char, long,
		long, long);
	BGL_EXPORTED_DECL long BGl_crczd2elongzd2lez00zz__crcz00(unsigned char, long,
		long, long);
	BGL_EXPORTED_DECL obj_t BGl_crcz00zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2335z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2337z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2339z00zz__crcz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_crczd2polynomialzd2bezd2ze3lez31zz__crcz00(obj_t,
		obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_crczd2llongzd2lez00zz__crcz00(unsigned
		char, BGL_LONGLONG_T, BGL_LONGLONG_T, long);
	static obj_t BGl_symbol2341z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_z62crczd2elongzb0zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BGL_LONGLONG_T BGl_crczd2llongzd2zz__crcz00(unsigned char,
		BGL_LONGLONG_T, BGL_LONGLONG_T, long);
	static obj_t BGl_z62registerzd2crcz12za2zz__crcz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31646ze3ze5zz__crcz00(obj_t);
	static obj_t BGl__crczd2stringzd2zz__crcz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__crcz00(void);
	static obj_t BGl_symbol2273z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2355z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2275z00zz__crcz00 = BUNSPEC;
	extern obj_t BGl_readzd2charzd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_symbol2277z00zz__crcz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_crczd2polynomialzd2zz__crcz00(obj_t);
	static obj_t BGl_symbol2279z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2362z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2281z00zz__crcz00 = BUNSPEC;
	static obj_t BGl__crczd2filezd2zz__crcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_crczd2filezd2zz__crcz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2286z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2369z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2289z00zz__crcz00 = BUNSPEC;
	BGL_EXPORTED_DECL long BGl_crczd2longzd2zz__crcz00(unsigned char, long, long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_crczd2stringzd2zz__crcz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2372z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2291z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2293z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2375z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_symbol2295z00zz__crcz00 = BUNSPEC;
	static obj_t BGl__crczd2portzd2zz__crcz00(obj_t, obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_symbol2297z00zz__crcz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_crczd2portzd2zz__crcz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_symbol2299z00zz__crcz00 = BUNSPEC;
	static obj_t BGl_list2348z00zz__crcz00 = BUNSPEC;
	static obj_t BGl__crcz00zz__crcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_crczd2elongzd2zz__crcz00(unsigned char, long, long,
		long);
	static obj_t BGl_z62crczd2polynomialzd2bezd2ze3lez53zz__crcz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__crczd2mmapzd2zz__crcz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_crczd2mmapzd2zz__crcz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_crczd2fastzd2portz00zz__crcz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62crczd2polynomialzd2lez62zz__crcz00(obj_t, obj_t);
	static obj_t *__cnst;


	extern obj_t BGl_bitzd2orelongzd2envz00zz__bitz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_registerzd2crcz12zd2envz12zz__crcz00,
		BgL_bgl_za762registerza7d2cr2380z00, BGl_z62registerzd2crcz12za2zz__crcz00,
		0L, BUNSPEC, 3);
	extern obj_t BGl_bitzd2rshllongzd2envz00zz__bitz00;
	   
		 
		DEFINE_STRING(BGl_string2300z00zz__crcz00,
		BgL_bgl_string2300za700za7za7_2381za7, "usb-5", 5);
	      DEFINE_STRING(BGl_string2302z00zz__crcz00,
		BgL_bgl_string2302za700za7za7_2382za7, "itu-6", 5);
	      DEFINE_STRING(BGl_string2304z00zz__crcz00,
		BgL_bgl_string2304za700za7za7_2383za7, "atm-8", 5);
	      DEFINE_STRING(BGl_string2306z00zz__crcz00,
		BgL_bgl_string2306za700za7za7_2384za7, "ccitt-8", 7);
	      DEFINE_STRING(BGl_string2308z00zz__crcz00,
		BgL_bgl_string2308za700za7za7_2385za7, "dallas/maxim-8", 14);
	      DEFINE_STRING(BGl_string2310z00zz__crcz00,
		BgL_bgl_string2310za700za7za7_2386za7, "sae-j1850-8", 11);
	      DEFINE_STRING(BGl_string2312z00zz__crcz00,
		BgL_bgl_string2312za700za7za7_2387za7, "can-15", 6);
	      DEFINE_STRING(BGl_string2316z00zz__crcz00,
		BgL_bgl_string2316za700za7za7_2388za7, "bit-andellong", 13);
	      DEFINE_STRING(BGl_string2319z00zz__crcz00,
		BgL_bgl_string2319za700za7za7_2389za7, "iso-64", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2longzd2lezd2envzd2zz__crcz00,
		BgL_bgl_za762crcza7d2longza7d22390za7, BGl_z62crczd2longzd2lez62zz__crcz00,
		0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2portzd2envz00zz__crcz00,
		BgL_bgl__crcza7d2portza7d2za7za72391z00, opt_generic_entry,
		BGl__crczd2portzd2zz__crcz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2323z00zz__crcz00,
		BgL_bgl_string2323za700za7za7_2392za7, "ecma-182-64", 11);
	      DEFINE_ELONG(BGl_elong2344z00zz__crcz00,
		BgL_bgl_elong2344za700za7za7__2393za7, 1);
	      DEFINE_STRING(BGl_string2324z00zz__crcz00,
		BgL_bgl_string2324za700za7za7_2394za7, "/tmp/bigloo/runtime/Unsafe/crc.scm",
		34);
	      DEFINE_STRING(BGl_string2325z00zz__crcz00,
		BgL_bgl_string2325za700za7za7_2395za7, "&crc-long", 9);
	      DEFINE_ELONG(BGl_elong2346z00zz__crcz00,
		BgL_bgl_elong2346za700za7za7__2396za7, 0);
	      DEFINE_STRING(BGl_string2326z00zz__crcz00,
		BgL_bgl_string2326za700za7za7_2397za7, "bchar", 5);
	      DEFINE_STRING(BGl_string2327z00zz__crcz00,
		BgL_bgl_string2327za700za7za7_2398za7, "bint", 4);
	      DEFINE_STRING(BGl_string2328z00zz__crcz00,
		BgL_bgl_string2328za700za7za7_2399za7, "&crc-elong", 10);
	      DEFINE_STRING(BGl_string2329z00zz__crcz00,
		BgL_bgl_string2329za700za7za7_2400za7, "belong", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2elongzd2envz00zz__crcz00,
		BgL_bgl_za762crcza7d2elongza7b2401za7, BGl_z62crczd2elongzb0zz__crcz00, 0L,
		BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2llongzd2lezd2envzd2zz__crcz00,
		BgL_bgl_za762crcza7d2llongza7d2402za7, BGl_z62crczd2llongzd2lez62zz__crcz00,
		0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2330z00zz__crcz00,
		BgL_bgl_string2330za700za7za7_2403za7, "&crc-llong", 10);
	      DEFINE_STRING(BGl_string2331z00zz__crcz00,
		BgL_bgl_string2331za700za7za7_2404za7, "bllong", 6);
	      DEFINE_STRING(BGl_string2332z00zz__crcz00,
		BgL_bgl_string2332za700za7za7_2405za7, "&crc-long-le", 12);
	      DEFINE_STRING(BGl_string2333z00zz__crcz00,
		BgL_bgl_string2333za700za7za7_2406za7, "&crc-elong-le", 13);
	      DEFINE_ELONG(BGl_elong2272z00zz__crcz00,
		BgL_bgl_elong2272za700za7za7__2407za7, 79764919);
	      DEFINE_STRING(BGl_string2334z00zz__crcz00,
		BgL_bgl_string2334za700za7za7_2408za7, "&crc-llong-le", 13);
	      DEFINE_STRING(BGl_string2336z00zz__crcz00,
		BgL_bgl_string2336za700za7za7_2409za7, "long", 4);
	      DEFINE_STRING(BGl_string2338z00zz__crcz00,
		BgL_bgl_string2338za700za7za7_2410za7, "elong", 5);
	      DEFINE_STRING(BGl_string2340z00zz__crcz00,
		BgL_bgl_string2340za700za7za7_2411za7, "llong", 5);
	      DEFINE_STRING(BGl_string2342z00zz__crcz00,
		BgL_bgl_string2342za700za7za7_2412za7, "crc-gen", 7);
	      DEFINE_STRING(BGl_string2343z00zz__crcz00,
		BgL_bgl_string2343za700za7za7_2413za7, "could not determine type", 24);
	      DEFINE_ELONG(BGl_elong2283z00zz__crcz00,
		BgL_bgl_elong2283za700za7za7__2414za7, 1021788929);
	      DEFINE_ELONG(BGl_elong2284z00zz__crcz00,
		BgL_bgl_elong2284za700za7za7__2415za7, 540064207);
	      DEFINE_ELONG(BGl_elong2285z00zz__crcz00,
		BgL_bgl_elong2285za700za7za7__2416za7, 517762881);
	      DEFINE_ELONG(BGl_elong2288z00zz__crcz00,
		BgL_bgl_elong2288za700za7za7__2417za7, 1947962583);
	extern obj_t BGl_bitzd2lshzd2envz00zz__bitz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2nameszd2envz00zz__crcz00,
		BgL_bgl_za762crcza7d2namesza7b2418za7, BGl_z62crczd2nameszb0zz__crcz00, 0L,
		BUNSPEC, 0);
	extern obj_t BGl_bitzd2lshelongzd2envz00zz__bitz00;
	   
		 
		DEFINE_STRING(BGl_string2350z00zz__crcz00,
		BgL_bgl_string2350za700za7za7_2419za7, "big-endian?", 11);
	      DEFINE_STRING(BGl_string2352z00zz__crcz00,
		BgL_bgl_string2352za700za7za7_2420za7, "final-xor", 9);
	      DEFINE_STRING(BGl_string2354z00zz__crcz00,
		BgL_bgl_string2354za700za7za7_2421za7, "init", 4);
	      DEFINE_STRING(BGl_string2274z00zz__crcz00,
		BgL_bgl_string2274za700za7za7_2422za7, "ieee-32", 7);
	      DEFINE_STRING(BGl_string2356z00zz__crcz00,
		BgL_bgl_string2356za700za7za7_2423za7, "crc", 3);
	      DEFINE_STRING(BGl_string2357z00zz__crcz00,
		BgL_bgl_string2357za700za7za7_2424za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string2276z00zz__crcz00,
		BgL_bgl_string2276za700za7za7_2425za7, "radix-64-24", 11);
	      DEFINE_STRING(BGl_string2358z00zz__crcz00,
		BgL_bgl_string2358za700za7za7_2426za7,
		"wrong number of arguments: [2..5] expected, provided", 52);
	      DEFINE_STRING(BGl_string2359z00zz__crcz00,
		BgL_bgl_string2359za700za7za7_2427za7, "_crc", 4);
	      DEFINE_STRING(BGl_string2278z00zz__crcz00,
		BgL_bgl_string2278za700za7za7_2428za7, "ccitt-16", 8);
	extern obj_t BGl_bitzd2andelongzd2envz00zz__bitz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2envzd2zz__crcz00,
		BgL_bgl__crcza700za7za7__crcza702429z00, opt_generic_entry,
		BGl__crcz00zz__crcz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2elongzd2lezd2envzd2zz__crcz00,
		BgL_bgl_za762crcza7d2elongza7d2430za7, BGl_z62crczd2elongzd2lez62zz__crcz00,
		0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2360z00zz__crcz00,
		BgL_bgl_string2360za700za7za7_2431za7, "mmap", 4);
	      DEFINE_STRING(BGl_string2361z00zz__crcz00,
		BgL_bgl_string2361za700za7za7_2432za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string2280z00zz__crcz00,
		BgL_bgl_string2280za700za7za7_2433za7, "dnp-16", 6);
	      DEFINE_STRING(BGl_string2363z00zz__crcz00,
		BgL_bgl_string2363za700za7za7_2434za7, "crc-file", 8);
	      DEFINE_STRING(BGl_string2282z00zz__crcz00,
		BgL_bgl_string2282za700za7za7_2435za7, "ibm-16", 6);
	      DEFINE_STRING(BGl_string2364z00zz__crcz00,
		BgL_bgl_string2364za700za7za7_2436za7, "_crc-file", 9);
	      DEFINE_STRING(BGl_string2365z00zz__crcz00,
		BgL_bgl_string2365za700za7za7_2437za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2366z00zz__crcz00,
		BgL_bgl_string2366za700za7za7_2438za7, "Could not open file", 19);
	      DEFINE_STRING(BGl_string2367z00zz__crcz00,
		BgL_bgl_string2367za700za7za7_2439za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2368z00zz__crcz00,
		BgL_bgl_string2368za700za7za7_2440za7, "pair", 4);
	      DEFINE_STRING(BGl_string2287z00zz__crcz00,
		BgL_bgl_string2287za700za7za7_2441za7, "c-32", 4);
	extern obj_t BGl_bitzd2orllongzd2envz00zz__bitz00;
	   
		 
		DEFINE_STRING(BGl_string2370z00zz__crcz00,
		BgL_bgl_string2370za700za7za7_2442za7, "crc-string", 10);
	      DEFINE_STRING(BGl_string2371z00zz__crcz00,
		BgL_bgl_string2371za700za7za7_2443za7, "_crc-string", 11);
	      DEFINE_STRING(BGl_string2290z00zz__crcz00,
		BgL_bgl_string2290za700za7za7_2444za7, "k-32", 4);
	extern obj_t BGl_bitzd2rshelongzd2envz00zz__bitz00;
	   
		 
		DEFINE_LLONG(BGl_llong2313z00zz__crcz00,
		BgL_bgl_llong2313za700za7za7__2445za7,
		(-(0 + ((BGL_LONGLONG_T) 65536 * ((0 + ((BGL_LONGLONG_T) 65536 * ((0 +
										((BGL_LONGLONG_T) 65536 *
											(((BGL_LONGLONG_T) 10240))))))))))));
	      DEFINE_STRING(BGl_string2373z00zz__crcz00,
		BgL_bgl_string2373za700za7za7_2446za7, "crc-port", 8);
	      DEFINE_LLONG(BGl_llong2314z00zz__crcz00,
		BgL_bgl_llong2314za700za7za7__2447za7, (-((BGL_LONGLONG_T) 1)));
	      DEFINE_STRING(BGl_string2292z00zz__crcz00,
		BgL_bgl_string2292za700za7za7_2448za7, "q-32", 4);
	      DEFINE_STRING(BGl_string2374z00zz__crcz00,
		BgL_bgl_string2374za700za7za7_2449za7, "_crc-port", 9);
	      DEFINE_STRING(BGl_string2294z00zz__crcz00,
		BgL_bgl_string2294za700za7za7_2450za7, "itu-4", 5);
	      DEFINE_STRING(BGl_string2376z00zz__crcz00,
		BgL_bgl_string2376za700za7za7_2451za7, "crc-mmap", 8);
	      DEFINE_LLONG(BGl_llong2317z00zz__crcz00,
		BgL_bgl_llong2317za700za7za7__2452za7, ((BGL_LONGLONG_T) 27));
	      DEFINE_STRING(BGl_string2377z00zz__crcz00,
		BgL_bgl_string2377za700za7za7_2453za7, "_crc-mmap", 9);
	      DEFINE_STRING(BGl_string2296z00zz__crcz00,
		BgL_bgl_string2296za700za7za7_2454za7, "epc-5", 5);
	      DEFINE_STRING(BGl_string2378z00zz__crcz00,
		BgL_bgl_string2378za700za7za7_2455za7, "Could not find crc", 18);
	      DEFINE_STRING(BGl_string2379z00zz__crcz00,
		BgL_bgl_string2379za700za7za7_2456za7, "bad polynomial", 14);
	      DEFINE_STRING(BGl_string2298z00zz__crcz00,
		BgL_bgl_string2298za700za7za7_2457za7, "itu-5", 5);
	      DEFINE_LLONG(BGl_llong2320z00zz__crcz00,
		BgL_bgl_llong2320za700za7za7__2458za7,
		(-(61630 + ((BGL_LONGLONG_T) 65536 * ((10360 +
							((BGL_LONGLONG_T) 65536 * ((43114 +
										((BGL_LONGLONG_T) 65536 *
											(((BGL_LONGLONG_T) 13971))))))))))));
	      DEFINE_LLONG(BGl_llong2321z00zz__crcz00,
		BgL_bgl_llong2321za700za7za7__2459za7,
		(13971 + ((BGL_LONGLONG_T) 65536 * ((43498 +
						((BGL_LONGLONG_T) 65536 * ((57835 +
									((BGL_LONGLONG_T) 65536 * (((BGL_LONGLONG_T) 17136)))))))))));
	extern obj_t BGl_bitzd2rshzd2envz00zz__bitz00;
	extern obj_t BGl_bitzd2andzd2envz00zz__bitz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2stringzd2envz00zz__crcz00,
		BgL_bgl__crcza7d2stringza7d22460z00, opt_generic_entry,
		BGl__crczd2stringzd2zz__crcz00, BFALSE, -1);
	      DEFINE_LLONG(BGl_llong2345z00zz__crcz00,
		BgL_bgl_llong2345za700za7za7__2461za7, ((BGL_LONGLONG_T) 1));
	      DEFINE_LLONG(BGl_llong2347z00zz__crcz00,
		BgL_bgl_llong2347za700za7za7__2462za7, ((BGL_LONGLONG_T) 0));
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_crczd2polynomialzd2bezd2ze3lezd2envze3zz__crcz00,
		BgL_bgl_za762crcza7d2polynom2463z00,
		BGl_z62crczd2polynomialzd2bezd2ze3lez53zz__crcz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2llongzd2envz00zz__crcz00,
		BgL_bgl_za762crcza7d2llongza7b2464za7, BGl_z62crczd2llongzb0zz__crcz00, 0L,
		BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2mmapzd2envz00zz__crcz00,
		BgL_bgl__crcza7d2mmapza7d2za7za72465z00, opt_generic_entry,
		BGl__crczd2mmapzd2zz__crcz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2lengthzd2envz00zz__crcz00,
		BgL_bgl_za762crcza7d2lengthza72466za7, BGl_z62crczd2lengthzb0zz__crcz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2polynomialzd2lezd2envzd2zz__crcz00,
		BgL_bgl_za762crcza7d2polynom2467z00,
		BGl_z62crczd2polynomialzd2lez62zz__crcz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2polynomialzd2envz00zz__crcz00,
		BgL_bgl_za762crcza7d2polynom2468z00, BGl_z62crczd2polynomialzb0zz__crcz00,
		0L, BUNSPEC, 1);
	extern obj_t BGl_bitzd2orzd2envz00zz__bitz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2filezd2envz00zz__crcz00,
		BgL_bgl__crcza7d2fileza7d2za7za72469z00, opt_generic_entry,
		BGl__crczd2filezd2zz__crcz00, BFALSE, -1);
	extern obj_t BGl_bitzd2lshllongzd2envz00zz__bitz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crczd2longzd2envz00zz__crcz00,
		BgL_bgl_za762crcza7d2longza7b02470za7, BGl_z62crczd2longzb0zz__crcz00, 0L,
		BUNSPEC, 4);
	extern obj_t BGl_bitzd2andllongzd2envz00zz__bitz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__crcz00));
		     ADD_ROOT((void *) (&BGl_za2crcsza2z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_keyword2349z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2301z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2303z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2305z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2307z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2309z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_keyword2351z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_keyword2353z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2311z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2315z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2318z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2322z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2335z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2337z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2339z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2341z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2273z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2355z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2275z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2277z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2279z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2362z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2281z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2286z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2369z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2289z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2372z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2291z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2293z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2375z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2295z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2297z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_symbol2299z00zz__crcz00));
		     ADD_ROOT((void *) (&BGl_list2348z00zz__crcz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__crcz00(long
		BgL_checksumz00_5558, char *BgL_fromz00_5559)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__crcz00))
				{
					BGl_requirezd2initializa7ationz75zz__crcz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__crcz00();
					BGl_cnstzd2initzd2zz__crcz00();
					return BGl_toplevelzd2initzd2zz__crcz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 15 */
			BGl_symbol2273z00zz__crcz00 =
				bstring_to_symbol(BGl_string2274z00zz__crcz00);
			BGl_symbol2275z00zz__crcz00 =
				bstring_to_symbol(BGl_string2276z00zz__crcz00);
			BGl_symbol2277z00zz__crcz00 =
				bstring_to_symbol(BGl_string2278z00zz__crcz00);
			BGl_symbol2279z00zz__crcz00 =
				bstring_to_symbol(BGl_string2280z00zz__crcz00);
			BGl_symbol2281z00zz__crcz00 =
				bstring_to_symbol(BGl_string2282z00zz__crcz00);
			BGl_symbol2286z00zz__crcz00 =
				bstring_to_symbol(BGl_string2287z00zz__crcz00);
			BGl_symbol2289z00zz__crcz00 =
				bstring_to_symbol(BGl_string2290z00zz__crcz00);
			BGl_symbol2291z00zz__crcz00 =
				bstring_to_symbol(BGl_string2292z00zz__crcz00);
			BGl_symbol2293z00zz__crcz00 =
				bstring_to_symbol(BGl_string2294z00zz__crcz00);
			BGl_symbol2295z00zz__crcz00 =
				bstring_to_symbol(BGl_string2296z00zz__crcz00);
			BGl_symbol2297z00zz__crcz00 =
				bstring_to_symbol(BGl_string2298z00zz__crcz00);
			BGl_symbol2299z00zz__crcz00 =
				bstring_to_symbol(BGl_string2300z00zz__crcz00);
			BGl_symbol2301z00zz__crcz00 =
				bstring_to_symbol(BGl_string2302z00zz__crcz00);
			BGl_symbol2303z00zz__crcz00 =
				bstring_to_symbol(BGl_string2304z00zz__crcz00);
			BGl_symbol2305z00zz__crcz00 =
				bstring_to_symbol(BGl_string2306z00zz__crcz00);
			BGl_symbol2307z00zz__crcz00 =
				bstring_to_symbol(BGl_string2308z00zz__crcz00);
			BGl_symbol2309z00zz__crcz00 =
				bstring_to_symbol(BGl_string2310z00zz__crcz00);
			BGl_symbol2311z00zz__crcz00 =
				bstring_to_symbol(BGl_string2312z00zz__crcz00);
			BGl_symbol2315z00zz__crcz00 =
				bstring_to_symbol(BGl_string2316z00zz__crcz00);
			BGl_symbol2318z00zz__crcz00 =
				bstring_to_symbol(BGl_string2319z00zz__crcz00);
			BGl_symbol2322z00zz__crcz00 =
				bstring_to_symbol(BGl_string2323z00zz__crcz00);
			BGl_symbol2335z00zz__crcz00 =
				bstring_to_symbol(BGl_string2336z00zz__crcz00);
			BGl_symbol2337z00zz__crcz00 =
				bstring_to_symbol(BGl_string2338z00zz__crcz00);
			BGl_symbol2339z00zz__crcz00 =
				bstring_to_symbol(BGl_string2340z00zz__crcz00);
			BGl_symbol2341z00zz__crcz00 =
				bstring_to_symbol(BGl_string2342z00zz__crcz00);
			BGl_keyword2349z00zz__crcz00 =
				bstring_to_keyword(BGl_string2350z00zz__crcz00);
			BGl_keyword2351z00zz__crcz00 =
				bstring_to_keyword(BGl_string2352z00zz__crcz00);
			BGl_keyword2353z00zz__crcz00 =
				bstring_to_keyword(BGl_string2354z00zz__crcz00);
			BGl_list2348z00zz__crcz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2349z00zz__crcz00,
				MAKE_YOUNG_PAIR(BGl_keyword2351z00zz__crcz00,
					MAKE_YOUNG_PAIR(BGl_keyword2353z00zz__crcz00, BNIL)));
			BGl_symbol2355z00zz__crcz00 =
				bstring_to_symbol(BGl_string2356z00zz__crcz00);
			BGl_symbol2362z00zz__crcz00 =
				bstring_to_symbol(BGl_string2363z00zz__crcz00);
			BGl_symbol2369z00zz__crcz00 =
				bstring_to_symbol(BGl_string2370z00zz__crcz00);
			BGl_symbol2372z00zz__crcz00 =
				bstring_to_symbol(BGl_string2373z00zz__crcz00);
			return (BGl_symbol2375z00zz__crcz00 =
				bstring_to_symbol(BGl_string2376z00zz__crcz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 15 */
			{	/* Unsafe/crc.scm 242 */
				obj_t BgL_arg1268z00_1386;
				obj_t BgL_arg1272z00_1387;

				{	/* Unsafe/crc.scm 242 */
					obj_t BgL_arg1284z00_1388;

					{	/* Unsafe/crc.scm 242 */
						obj_t BgL_arg1304z00_1389;

						{	/* Unsafe/crc.scm 242 */
							obj_t BgL_arg1305z00_1390;

							{	/* Unsafe/crc.scm 242 */
								long BgL_arg1306z00_1391;

								BgL_arg1306z00_1391 =
									(((long) 4294967295) & ((long) -306674912));
								BgL_arg1305z00_1390 =
									MAKE_YOUNG_PAIR(make_belong(BgL_arg1306z00_1391), BNIL);
							}
							BgL_arg1304z00_1389 =
								MAKE_YOUNG_PAIR(BGl_elong2272z00zz__crcz00,
								BgL_arg1305z00_1390);
						}
						BgL_arg1284z00_1388 =
							MAKE_YOUNG_PAIR(BINT(32L), BgL_arg1304z00_1389);
					}
					BgL_arg1268z00_1386 =
						MAKE_YOUNG_PAIR(BGl_symbol2273z00zz__crcz00, BgL_arg1284z00_1388);
				}
				{	/* Unsafe/crc.scm 243 */
					obj_t BgL_arg1307z00_1392;
					obj_t BgL_arg1308z00_1393;

					{	/* Unsafe/crc.scm 243 */
						obj_t BgL_arg1309z00_1394;

						{	/* Unsafe/crc.scm 243 */
							obj_t BgL_arg1310z00_1395;

							{	/* Unsafe/crc.scm 243 */
								obj_t BgL_arg1311z00_1396;

								BgL_arg1311z00_1396 = MAKE_YOUNG_PAIR(BINT(14627425L), BNIL);
								BgL_arg1310z00_1395 =
									MAKE_YOUNG_PAIR(BINT(8801531L), BgL_arg1311z00_1396);
							}
							BgL_arg1309z00_1394 =
								MAKE_YOUNG_PAIR(BINT(24L), BgL_arg1310z00_1395);
						}
						BgL_arg1307z00_1392 =
							MAKE_YOUNG_PAIR(BGl_symbol2275z00zz__crcz00, BgL_arg1309z00_1394);
					}
					{	/* Unsafe/crc.scm 244 */
						obj_t BgL_arg1312z00_1397;
						obj_t BgL_arg1314z00_1398;

						{	/* Unsafe/crc.scm 244 */
							obj_t BgL_arg1315z00_1399;

							{	/* Unsafe/crc.scm 244 */
								obj_t BgL_arg1316z00_1400;

								{	/* Unsafe/crc.scm 244 */
									obj_t BgL_arg1317z00_1401;

									BgL_arg1317z00_1401 = MAKE_YOUNG_PAIR(BINT(33800L), BNIL);
									BgL_arg1316z00_1400 =
										MAKE_YOUNG_PAIR(BINT(4129L), BgL_arg1317z00_1401);
								}
								BgL_arg1315z00_1399 =
									MAKE_YOUNG_PAIR(BINT(16L), BgL_arg1316z00_1400);
							}
							BgL_arg1312z00_1397 =
								MAKE_YOUNG_PAIR(BGl_symbol2277z00zz__crcz00,
								BgL_arg1315z00_1399);
						}
						{	/* Unsafe/crc.scm 245 */
							obj_t BgL_arg1318z00_1402;
							obj_t BgL_arg1319z00_1403;

							{	/* Unsafe/crc.scm 245 */
								obj_t BgL_arg1320z00_1404;

								{	/* Unsafe/crc.scm 245 */
									obj_t BgL_arg1321z00_1405;

									{	/* Unsafe/crc.scm 245 */
										obj_t BgL_arg1322z00_1406;

										BgL_arg1322z00_1406 = MAKE_YOUNG_PAIR(BINT(42684L), BNIL);
										BgL_arg1321z00_1405 =
											MAKE_YOUNG_PAIR(BINT(15717L), BgL_arg1322z00_1406);
									}
									BgL_arg1320z00_1404 =
										MAKE_YOUNG_PAIR(BINT(16L), BgL_arg1321z00_1405);
								}
								BgL_arg1318z00_1402 =
									MAKE_YOUNG_PAIR(BGl_symbol2279z00zz__crcz00,
									BgL_arg1320z00_1404);
							}
							{	/* Unsafe/crc.scm 246 */
								obj_t BgL_arg1323z00_1407;
								obj_t BgL_arg1325z00_1408;

								{	/* Unsafe/crc.scm 246 */
									obj_t BgL_arg1326z00_1409;

									{	/* Unsafe/crc.scm 246 */
										obj_t BgL_arg1327z00_1410;

										{	/* Unsafe/crc.scm 246 */
											obj_t BgL_arg1328z00_1411;

											BgL_arg1328z00_1411 = MAKE_YOUNG_PAIR(BINT(40961L), BNIL);
											BgL_arg1327z00_1410 =
												MAKE_YOUNG_PAIR(BINT(32773L), BgL_arg1328z00_1411);
										}
										BgL_arg1326z00_1409 =
											MAKE_YOUNG_PAIR(BINT(16L), BgL_arg1327z00_1410);
									}
									BgL_arg1323z00_1407 =
										MAKE_YOUNG_PAIR(BGl_symbol2281z00zz__crcz00,
										BgL_arg1326z00_1409);
								}
								{	/* Unsafe/crc.scm 247 */
									obj_t BgL_arg1329z00_1412;
									obj_t BgL_arg1331z00_1413;

									{	/* Unsafe/crc.scm 247 */
										obj_t BgL_arg1332z00_1414;

										{	/* Unsafe/crc.scm 247 */
											obj_t BgL_arg1333z00_1415;

											{	/* Unsafe/crc.scm 247 */
												obj_t BgL_arg1334z00_1416;

												BgL_arg1334z00_1416 =
													MAKE_YOUNG_PAIR(BINT(13874874L), BNIL);
												BgL_arg1333z00_1415 =
													MAKE_YOUNG_PAIR(BINT(6122955L), BgL_arg1334z00_1416);
											}
											BgL_arg1332z00_1414 =
												MAKE_YOUNG_PAIR(BINT(24L), BgL_arg1333z00_1415);
										}
										BgL_arg1329z00_1412 =
											MAKE_YOUNG_PAIR(BINT(24L), BgL_arg1332z00_1414);
									}
									{	/* Unsafe/crc.scm 248 */
										obj_t BgL_arg1335z00_1417;
										obj_t BgL_arg1336z00_1418;

										{	/* Unsafe/crc.scm 248 */
											obj_t BgL_arg1337z00_1419;

											{	/* Unsafe/crc.scm 248 */
												obj_t BgL_arg1338z00_1420;

												{	/* Unsafe/crc.scm 248 */
													obj_t BgL_arg1339z00_1421;

													BgL_arg1339z00_1421 =
														MAKE_YOUNG_PAIR(BGl_elong2283z00zz__crcz00, BNIL);
													BgL_arg1338z00_1420 =
														MAKE_YOUNG_PAIR(BGl_elong2284z00zz__crcz00,
														BgL_arg1339z00_1421);
												}
												BgL_arg1337z00_1419 =
													MAKE_YOUNG_PAIR(BINT(30L), BgL_arg1338z00_1420);
											}
											BgL_arg1335z00_1417 =
												MAKE_YOUNG_PAIR(BINT(30L), BgL_arg1337z00_1419);
										}
										{	/* Unsafe/crc.scm 249 */
											obj_t BgL_arg1340z00_1422;
											obj_t BgL_arg1341z00_1423;

											{	/* Unsafe/crc.scm 249 */
												obj_t BgL_arg1342z00_1424;

												{	/* Unsafe/crc.scm 249 */
													obj_t BgL_arg1343z00_1425;

													{	/* Unsafe/crc.scm 249 */
														obj_t BgL_arg1344z00_1426;

														{	/* Unsafe/crc.scm 249 */
															long BgL_arg1346z00_1427;

															BgL_arg1346z00_1427 =
																(((long) 4294967295) & ((long) -2097792136));
															BgL_arg1344z00_1426 =
																MAKE_YOUNG_PAIR(make_belong
																(BgL_arg1346z00_1427), BNIL);
														}
														BgL_arg1343z00_1425 =
															MAKE_YOUNG_PAIR(BGl_elong2285z00zz__crcz00,
															BgL_arg1344z00_1426);
													}
													BgL_arg1342z00_1424 =
														MAKE_YOUNG_PAIR(BINT(32L), BgL_arg1343z00_1425);
												}
												BgL_arg1340z00_1422 =
													MAKE_YOUNG_PAIR(BGl_symbol2286z00zz__crcz00,
													BgL_arg1342z00_1424);
											}
											{	/* Unsafe/crc.scm 250 */
												obj_t BgL_arg1347z00_1428;
												obj_t BgL_arg1348z00_1429;

												{	/* Unsafe/crc.scm 250 */
													obj_t BgL_arg1349z00_1430;

													{	/* Unsafe/crc.scm 250 */
														obj_t BgL_arg1350z00_1431;

														{	/* Unsafe/crc.scm 250 */
															obj_t BgL_arg1351z00_1432;

															{	/* Unsafe/crc.scm 250 */
																long BgL_arg1352z00_1433;

																BgL_arg1352z00_1433 =
																	(((long) 4294967295) & ((long) -349054930));
																BgL_arg1351z00_1432 =
																	MAKE_YOUNG_PAIR(make_belong
																	(BgL_arg1352z00_1433), BNIL);
															}
															BgL_arg1350z00_1431 =
																MAKE_YOUNG_PAIR(BGl_elong2288z00zz__crcz00,
																BgL_arg1351z00_1432);
														}
														BgL_arg1349z00_1430 =
															MAKE_YOUNG_PAIR(BINT(32L), BgL_arg1350z00_1431);
													}
													BgL_arg1347z00_1428 =
														MAKE_YOUNG_PAIR(BGl_symbol2289z00zz__crcz00,
														BgL_arg1349z00_1430);
												}
												{	/* Unsafe/crc.scm 252 */
													obj_t BgL_arg1354z00_1434;
													obj_t BgL_arg1356z00_1435;

													{	/* Unsafe/crc.scm 252 */
														obj_t BgL_arg1357z00_1436;

														{	/* Unsafe/crc.scm 252 */
															obj_t BgL_arg1358z00_1437;

															{	/* Unsafe/crc.scm 252 */
																long BgL_arg1359z00_1438;
																obj_t BgL_arg1360z00_1439;

																BgL_arg1359z00_1438 =
																	(((long) 4294967295) & ((long) -2126429781));
																{	/* Unsafe/crc.scm 253 */
																	long BgL_arg1361z00_1440;

																	BgL_arg1361z00_1440 =
																		(((long) 4294967295) & ((long) -712867199));
																	BgL_arg1360z00_1439 =
																		MAKE_YOUNG_PAIR(make_belong
																		(BgL_arg1361z00_1440), BNIL);
																}
																BgL_arg1358z00_1437 =
																	MAKE_YOUNG_PAIR(make_belong
																	(BgL_arg1359z00_1438), BgL_arg1360z00_1439);
															}
															BgL_arg1357z00_1436 =
																MAKE_YOUNG_PAIR(BINT(32L), BgL_arg1358z00_1437);
														}
														BgL_arg1354z00_1434 =
															MAKE_YOUNG_PAIR(BGl_symbol2291z00zz__crcz00,
															BgL_arg1357z00_1436);
													}
													{	/* Unsafe/crc.scm 254 */
														obj_t BgL_arg1362z00_1441;
														obj_t BgL_arg1363z00_1442;

														{	/* Unsafe/crc.scm 254 */
															obj_t BgL_arg1364z00_1443;

															{	/* Unsafe/crc.scm 254 */
																obj_t BgL_arg1365z00_1444;

																{	/* Unsafe/crc.scm 254 */
																	obj_t BgL_arg1366z00_1445;

																	BgL_arg1366z00_1445 =
																		MAKE_YOUNG_PAIR(BINT(12L), BNIL);
																	BgL_arg1365z00_1444 =
																		MAKE_YOUNG_PAIR(BINT(3L),
																		BgL_arg1366z00_1445);
																}
																BgL_arg1364z00_1443 =
																	MAKE_YOUNG_PAIR(BINT(4L),
																	BgL_arg1365z00_1444);
															}
															BgL_arg1362z00_1441 =
																MAKE_YOUNG_PAIR(BGl_symbol2293z00zz__crcz00,
																BgL_arg1364z00_1443);
														}
														{	/* Unsafe/crc.scm 255 */
															obj_t BgL_arg1367z00_1446;
															obj_t BgL_arg1368z00_1447;

															{	/* Unsafe/crc.scm 255 */
																obj_t BgL_arg1369z00_1448;

																{	/* Unsafe/crc.scm 255 */
																	obj_t BgL_arg1370z00_1449;

																	{	/* Unsafe/crc.scm 255 */
																		obj_t BgL_arg1371z00_1450;

																		BgL_arg1371z00_1450 =
																			MAKE_YOUNG_PAIR(BINT(18L), BNIL);
																		BgL_arg1370z00_1449 =
																			MAKE_YOUNG_PAIR(BINT(9L),
																			BgL_arg1371z00_1450);
																	}
																	BgL_arg1369z00_1448 =
																		MAKE_YOUNG_PAIR(BINT(5L),
																		BgL_arg1370z00_1449);
																}
																BgL_arg1367z00_1446 =
																	MAKE_YOUNG_PAIR(BGl_symbol2295z00zz__crcz00,
																	BgL_arg1369z00_1448);
															}
															{	/* Unsafe/crc.scm 256 */
																obj_t BgL_arg1372z00_1451;
																obj_t BgL_arg1373z00_1452;

																{	/* Unsafe/crc.scm 256 */
																	obj_t BgL_arg1375z00_1453;

																	{	/* Unsafe/crc.scm 256 */
																		obj_t BgL_arg1376z00_1454;

																		{	/* Unsafe/crc.scm 256 */
																			obj_t BgL_arg1377z00_1455;

																			BgL_arg1377z00_1455 =
																				MAKE_YOUNG_PAIR(BINT(21L), BNIL);
																			BgL_arg1376z00_1454 =
																				MAKE_YOUNG_PAIR(BINT(21L),
																				BgL_arg1377z00_1455);
																		}
																		BgL_arg1375z00_1453 =
																			MAKE_YOUNG_PAIR(BINT(5L),
																			BgL_arg1376z00_1454);
																	}
																	BgL_arg1372z00_1451 =
																		MAKE_YOUNG_PAIR(BGl_symbol2297z00zz__crcz00,
																		BgL_arg1375z00_1453);
																}
																{	/* Unsafe/crc.scm 257 */
																	obj_t BgL_arg1378z00_1456;
																	obj_t BgL_arg1379z00_1457;

																	{	/* Unsafe/crc.scm 257 */
																		obj_t BgL_arg1380z00_1458;

																		{	/* Unsafe/crc.scm 257 */
																			obj_t BgL_arg1382z00_1459;

																			{	/* Unsafe/crc.scm 257 */
																				obj_t BgL_arg1383z00_1460;

																				BgL_arg1383z00_1460 =
																					MAKE_YOUNG_PAIR(BINT(20L), BNIL);
																				BgL_arg1382z00_1459 =
																					MAKE_YOUNG_PAIR(BINT(5L),
																					BgL_arg1383z00_1460);
																			}
																			BgL_arg1380z00_1458 =
																				MAKE_YOUNG_PAIR(BINT(5L),
																				BgL_arg1382z00_1459);
																		}
																		BgL_arg1378z00_1456 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2299z00zz__crcz00,
																			BgL_arg1380z00_1458);
																	}
																	{	/* Unsafe/crc.scm 258 */
																		obj_t BgL_arg1384z00_1461;
																		obj_t BgL_arg1387z00_1462;

																		{	/* Unsafe/crc.scm 258 */
																			obj_t BgL_arg1388z00_1463;

																			{	/* Unsafe/crc.scm 258 */
																				obj_t BgL_arg1389z00_1464;

																				{	/* Unsafe/crc.scm 258 */
																					obj_t BgL_arg1390z00_1465;

																					BgL_arg1390z00_1465 =
																						MAKE_YOUNG_PAIR(BINT(48L), BNIL);
																					BgL_arg1389z00_1464 =
																						MAKE_YOUNG_PAIR(BINT(3L),
																						BgL_arg1390z00_1465);
																				}
																				BgL_arg1388z00_1463 =
																					MAKE_YOUNG_PAIR(BINT(6L),
																					BgL_arg1389z00_1464);
																			}
																			BgL_arg1384z00_1461 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2301z00zz__crcz00,
																				BgL_arg1388z00_1463);
																		}
																		{	/* Unsafe/crc.scm 259 */
																			obj_t BgL_arg1391z00_1466;
																			obj_t BgL_arg1392z00_1467;

																			{	/* Unsafe/crc.scm 259 */
																				obj_t BgL_arg1393z00_1468;

																				{	/* Unsafe/crc.scm 259 */
																					obj_t BgL_arg1394z00_1469;

																					{	/* Unsafe/crc.scm 259 */
																						obj_t BgL_arg1395z00_1470;

																						BgL_arg1395z00_1470 =
																							MAKE_YOUNG_PAIR(BINT(72L), BNIL);
																						BgL_arg1394z00_1469 =
																							MAKE_YOUNG_PAIR(BINT(9L),
																							BgL_arg1395z00_1470);
																					}
																					BgL_arg1393z00_1468 =
																						MAKE_YOUNG_PAIR(BINT(7L),
																						BgL_arg1394z00_1469);
																				}
																				BgL_arg1391z00_1466 =
																					MAKE_YOUNG_PAIR(BINT(7L),
																					BgL_arg1393z00_1468);
																			}
																			{	/* Unsafe/crc.scm 260 */
																				obj_t BgL_arg1396z00_1471;
																				obj_t BgL_arg1397z00_1472;

																				{	/* Unsafe/crc.scm 260 */
																					obj_t BgL_arg1399z00_1473;

																					{	/* Unsafe/crc.scm 260 */
																						obj_t BgL_arg1400z00_1474;

																						{	/* Unsafe/crc.scm 260 */
																							obj_t BgL_arg1401z00_1475;

																							BgL_arg1401z00_1475 =
																								MAKE_YOUNG_PAIR(BINT(224L),
																								BNIL);
																							BgL_arg1400z00_1474 =
																								MAKE_YOUNG_PAIR(BINT(7L),
																								BgL_arg1401z00_1475);
																						}
																						BgL_arg1399z00_1473 =
																							MAKE_YOUNG_PAIR(BINT(8L),
																							BgL_arg1400z00_1474);
																					}
																					BgL_arg1396z00_1471 =
																						MAKE_YOUNG_PAIR
																						(BGl_symbol2303z00zz__crcz00,
																						BgL_arg1399z00_1473);
																				}
																				{	/* Unsafe/crc.scm 261 */
																					obj_t BgL_arg1402z00_1476;
																					obj_t BgL_arg1403z00_1477;

																					{	/* Unsafe/crc.scm 261 */
																						obj_t BgL_arg1404z00_1478;

																						{	/* Unsafe/crc.scm 261 */
																							obj_t BgL_arg1405z00_1479;

																							{	/* Unsafe/crc.scm 261 */
																								obj_t BgL_arg1406z00_1480;

																								BgL_arg1406z00_1480 =
																									MAKE_YOUNG_PAIR(BINT(177L),
																									BNIL);
																								BgL_arg1405z00_1479 =
																									MAKE_YOUNG_PAIR(BINT(141L),
																									BgL_arg1406z00_1480);
																							}
																							BgL_arg1404z00_1478 =
																								MAKE_YOUNG_PAIR(BINT(8L),
																								BgL_arg1405z00_1479);
																						}
																						BgL_arg1402z00_1476 =
																							MAKE_YOUNG_PAIR
																							(BGl_symbol2305z00zz__crcz00,
																							BgL_arg1404z00_1478);
																					}
																					{	/* Unsafe/crc.scm 262 */
																						obj_t BgL_arg1407z00_1481;
																						obj_t BgL_arg1408z00_1482;

																						{	/* Unsafe/crc.scm 262 */
																							obj_t BgL_arg1410z00_1483;

																							{	/* Unsafe/crc.scm 262 */
																								obj_t BgL_arg1411z00_1484;

																								{	/* Unsafe/crc.scm 262 */
																									obj_t BgL_arg1412z00_1485;

																									BgL_arg1412z00_1485 =
																										MAKE_YOUNG_PAIR(BINT(140L),
																										BNIL);
																									BgL_arg1411z00_1484 =
																										MAKE_YOUNG_PAIR(BINT(49L),
																										BgL_arg1412z00_1485);
																								}
																								BgL_arg1410z00_1483 =
																									MAKE_YOUNG_PAIR(BINT(8L),
																									BgL_arg1411z00_1484);
																							}
																							BgL_arg1407z00_1481 =
																								MAKE_YOUNG_PAIR
																								(BGl_symbol2307z00zz__crcz00,
																								BgL_arg1410z00_1483);
																						}
																						{	/* Unsafe/crc.scm 263 */
																							obj_t BgL_arg1413z00_1486;
																							obj_t BgL_arg1414z00_1487;

																							{	/* Unsafe/crc.scm 263 */
																								obj_t BgL_arg1415z00_1488;

																								{	/* Unsafe/crc.scm 263 */
																									obj_t BgL_arg1416z00_1489;

																									{	/* Unsafe/crc.scm 263 */
																										obj_t BgL_arg1417z00_1490;

																										BgL_arg1417z00_1490 =
																											MAKE_YOUNG_PAIR(BINT
																											(171L), BNIL);
																										BgL_arg1416z00_1489 =
																											MAKE_YOUNG_PAIR(BINT
																											(213L),
																											BgL_arg1417z00_1490);
																									}
																									BgL_arg1415z00_1488 =
																										MAKE_YOUNG_PAIR(BINT(8L),
																										BgL_arg1416z00_1489);
																								}
																								BgL_arg1413z00_1486 =
																									MAKE_YOUNG_PAIR(BINT(8L),
																									BgL_arg1415z00_1488);
																							}
																							{	/* Unsafe/crc.scm 264 */
																								obj_t BgL_arg1418z00_1491;
																								obj_t BgL_arg1419z00_1492;

																								{	/* Unsafe/crc.scm 264 */
																									obj_t BgL_arg1420z00_1493;

																									{	/* Unsafe/crc.scm 264 */
																										obj_t BgL_arg1421z00_1494;

																										{	/* Unsafe/crc.scm 264 */
																											obj_t BgL_arg1422z00_1495;

																											BgL_arg1422z00_1495 =
																												MAKE_YOUNG_PAIR(BINT
																												(184L), BNIL);
																											BgL_arg1421z00_1494 =
																												MAKE_YOUNG_PAIR(BINT
																												(29L),
																												BgL_arg1422z00_1495);
																										}
																										BgL_arg1420z00_1493 =
																											MAKE_YOUNG_PAIR(BINT(8L),
																											BgL_arg1421z00_1494);
																									}
																									BgL_arg1418z00_1491 =
																										MAKE_YOUNG_PAIR
																										(BGl_symbol2309z00zz__crcz00,
																										BgL_arg1420z00_1493);
																								}
																								{	/* Unsafe/crc.scm 265 */
																									obj_t BgL_arg1423z00_1496;
																									obj_t BgL_arg1424z00_1497;

																									{	/* Unsafe/crc.scm 265 */
																										obj_t BgL_arg1425z00_1498;

																										{	/* Unsafe/crc.scm 265 */
																											obj_t BgL_arg1426z00_1499;

																											{	/* Unsafe/crc.scm 265 */
																												obj_t
																													BgL_arg1427z00_1500;
																												BgL_arg1427z00_1500 =
																													MAKE_YOUNG_PAIR(BINT
																													(817L), BNIL);
																												BgL_arg1426z00_1499 =
																													MAKE_YOUNG_PAIR(BINT
																													(563L),
																													BgL_arg1427z00_1500);
																											}
																											BgL_arg1425z00_1498 =
																												MAKE_YOUNG_PAIR(BINT
																												(10L),
																												BgL_arg1426z00_1499);
																										}
																										BgL_arg1423z00_1496 =
																											MAKE_YOUNG_PAIR(BINT(10L),
																											BgL_arg1425z00_1498);
																									}
																									{	/* Unsafe/crc.scm 266 */
																										obj_t BgL_arg1428z00_1501;
																										obj_t BgL_arg1429z00_1502;

																										{	/* Unsafe/crc.scm 266 */
																											obj_t BgL_arg1430z00_1503;

																											{	/* Unsafe/crc.scm 266 */
																												obj_t
																													BgL_arg1431z00_1504;
																												{	/* Unsafe/crc.scm 266 */
																													obj_t
																														BgL_arg1434z00_1505;
																													BgL_arg1434z00_1505 =
																														MAKE_YOUNG_PAIR(BINT
																														(1294L), BNIL);
																													BgL_arg1431z00_1504 =
																														MAKE_YOUNG_PAIR(BINT
																														(901L),
																														BgL_arg1434z00_1505);
																												}
																												BgL_arg1430z00_1503 =
																													MAKE_YOUNG_PAIR(BINT
																													(11L),
																													BgL_arg1431z00_1504);
																											}
																											BgL_arg1428z00_1501 =
																												MAKE_YOUNG_PAIR(BINT
																												(11L),
																												BgL_arg1430z00_1503);
																										}
																										{	/* Unsafe/crc.scm 267 */
																											obj_t BgL_arg1435z00_1506;
																											obj_t BgL_arg1436z00_1507;

																											{	/* Unsafe/crc.scm 267 */
																												obj_t
																													BgL_arg1437z00_1508;
																												{	/* Unsafe/crc.scm 267 */
																													obj_t
																														BgL_arg1438z00_1509;
																													{	/* Unsafe/crc.scm 267 */
																														obj_t
																															BgL_arg1439z00_1510;
																														BgL_arg1439z00_1510
																															=
																															MAKE_YOUNG_PAIR
																															(BINT(3841L),
																															BNIL);
																														BgL_arg1438z00_1509
																															=
																															MAKE_YOUNG_PAIR
																															(BINT(2063L),
																															BgL_arg1439z00_1510);
																													}
																													BgL_arg1437z00_1508 =
																														MAKE_YOUNG_PAIR(BINT
																														(12L),
																														BgL_arg1438z00_1509);
																												}
																												BgL_arg1435z00_1506 =
																													MAKE_YOUNG_PAIR(BINT
																													(12L),
																													BgL_arg1437z00_1508);
																											}
																											{	/* Unsafe/crc.scm 268 */
																												obj_t
																													BgL_arg1440z00_1511;
																												obj_t
																													BgL_arg1441z00_1512;
																												{	/* Unsafe/crc.scm 268 */
																													obj_t
																														BgL_arg1442z00_1513;
																													{	/* Unsafe/crc.scm 268 */
																														obj_t
																															BgL_arg1443z00_1514;
																														{	/* Unsafe/crc.scm 268 */
																															obj_t
																																BgL_arg1444z00_1515;
																															BgL_arg1444z00_1515
																																=
																																MAKE_YOUNG_PAIR
																																(BINT(19665L),
																																BNIL);
																															BgL_arg1443z00_1514
																																=
																																MAKE_YOUNG_PAIR
																																(BINT(17817L),
																																BgL_arg1444z00_1515);
																														}
																														BgL_arg1442z00_1513
																															=
																															MAKE_YOUNG_PAIR
																															(BINT(15L),
																															BgL_arg1443z00_1514);
																													}
																													BgL_arg1440z00_1511 =
																														MAKE_YOUNG_PAIR
																														(BGl_symbol2311z00zz__crcz00,
																														BgL_arg1442z00_1513);
																												}
																												{	/* Unsafe/crc.scm 269 */
																													obj_t
																														BgL_arg1445z00_1516;
																													obj_t
																														BgL_arg1446z00_1517;
																													{	/* Unsafe/crc.scm 269 */
																														obj_t
																															BgL_arg1447z00_1518;
																														{	/* Unsafe/crc.scm 269 */
																															obj_t
																																BgL_arg1448z00_1519;
																															{	/* Unsafe/crc.scm 269 */
																																obj_t
																																	BgL_arg1449z00_1520;
																																{	/* Unsafe/crc.scm 269 */
																																	obj_t
																																		BgL_arg1450z00_1521;
																																	{	/* Unsafe/crc.scm 269 */
																																		obj_t
																																			BgL_arg1451z00_1522;
																																		{	/* Unsafe/crc.scm 269 */
																																			obj_t
																																				BgL_arg1452z00_1523;
																																			BgL_arg1452z00_1523
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_llong2313z00zz__crcz00,
																																				BNIL);
																																			BgL_arg1451z00_1522
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_llong2314z00zz__crcz00,
																																				BgL_arg1452z00_1523);
																																		}
																																		BgL_arg1450z00_1521
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_symbol2315z00zz__crcz00,
																																			BgL_arg1451z00_1522);
																																	}
																																	BgL_arg1449z00_1520
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1450z00_1521,
																																		BNIL);
																																}
																																BgL_arg1448z00_1519
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_llong2317z00zz__crcz00,
																																	BgL_arg1449z00_1520);
																															}
																															BgL_arg1447z00_1518
																																=
																																MAKE_YOUNG_PAIR
																																(BINT(64L),
																																BgL_arg1448z00_1519);
																														}
																														BgL_arg1445z00_1516
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_symbol2318z00zz__crcz00,
																															BgL_arg1447z00_1518);
																													}
																													{	/* Unsafe/crc.scm 272 */
																														obj_t
																															BgL_arg1453z00_1524;
																														{	/* Unsafe/crc.scm 272 */
																															obj_t
																																BgL_arg1454z00_1525;
																															{	/* Unsafe/crc.scm 272 */
																																obj_t
																																	BgL_arg1455z00_1526;
																																{	/* Unsafe/crc.scm 272 */
																																	obj_t
																																		BgL_arg1456z00_1527;
																																	{	/* Unsafe/crc.scm 272 */
																																		obj_t
																																			BgL_arg1457z00_1528;
																																		{	/* Unsafe/crc.scm 272 */
																																			obj_t
																																				BgL_arg1458z00_1529;
																																			{	/* Unsafe/crc.scm 272 */
																																				obj_t
																																					BgL_arg1459z00_1530;
																																				BgL_arg1459z00_1530
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_llong2320z00zz__crcz00,
																																					BNIL);
																																				BgL_arg1458z00_1529
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_llong2314z00zz__crcz00,
																																					BgL_arg1459z00_1530);
																																			}
																																			BgL_arg1457z00_1528
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_symbol2315z00zz__crcz00,
																																				BgL_arg1458z00_1529);
																																		}
																																		BgL_arg1456z00_1527
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1457z00_1528,
																																			BNIL);
																																	}
																																	BgL_arg1455z00_1526
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_llong2321z00zz__crcz00,
																																		BgL_arg1456z00_1527);
																																}
																																BgL_arg1454z00_1525
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BINT(64L),
																																	BgL_arg1455z00_1526);
																															}
																															BgL_arg1453z00_1524
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_symbol2322z00zz__crcz00,
																																BgL_arg1454z00_1525);
																														}
																														BgL_arg1446z00_1517
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1453z00_1524,
																															BNIL);
																													}
																													BgL_arg1441z00_1512 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1445z00_1516,
																														BgL_arg1446z00_1517);
																												}
																												BgL_arg1436z00_1507 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1440z00_1511,
																													BgL_arg1441z00_1512);
																											}
																											BgL_arg1429z00_1502 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1435z00_1506,
																												BgL_arg1436z00_1507);
																										}
																										BgL_arg1424z00_1497 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1428z00_1501,
																											BgL_arg1429z00_1502);
																									}
																									BgL_arg1419z00_1492 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1423z00_1496,
																										BgL_arg1424z00_1497);
																								}
																								BgL_arg1414z00_1487 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1418z00_1491,
																									BgL_arg1419z00_1492);
																							}
																							BgL_arg1408z00_1482 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1413z00_1486,
																								BgL_arg1414z00_1487);
																						}
																						BgL_arg1403z00_1477 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1407z00_1481,
																							BgL_arg1408z00_1482);
																					}
																					BgL_arg1397z00_1472 =
																						MAKE_YOUNG_PAIR(BgL_arg1402z00_1476,
																						BgL_arg1403z00_1477);
																				}
																				BgL_arg1392z00_1467 =
																					MAKE_YOUNG_PAIR(BgL_arg1396z00_1471,
																					BgL_arg1397z00_1472);
																			}
																			BgL_arg1387z00_1462 =
																				MAKE_YOUNG_PAIR(BgL_arg1391z00_1466,
																				BgL_arg1392z00_1467);
																		}
																		BgL_arg1379z00_1457 =
																			MAKE_YOUNG_PAIR(BgL_arg1384z00_1461,
																			BgL_arg1387z00_1462);
																	}
																	BgL_arg1373z00_1452 =
																		MAKE_YOUNG_PAIR(BgL_arg1378z00_1456,
																		BgL_arg1379z00_1457);
																}
																BgL_arg1368z00_1447 =
																	MAKE_YOUNG_PAIR(BgL_arg1372z00_1451,
																	BgL_arg1373z00_1452);
															}
															BgL_arg1363z00_1442 =
																MAKE_YOUNG_PAIR(BgL_arg1367z00_1446,
																BgL_arg1368z00_1447);
														}
														BgL_arg1356z00_1435 =
															MAKE_YOUNG_PAIR(BgL_arg1362z00_1441,
															BgL_arg1363z00_1442);
													}
													BgL_arg1348z00_1429 =
														MAKE_YOUNG_PAIR(BgL_arg1354z00_1434,
														BgL_arg1356z00_1435);
												}
												BgL_arg1341z00_1423 =
													MAKE_YOUNG_PAIR(BgL_arg1347z00_1428,
													BgL_arg1348z00_1429);
											}
											BgL_arg1336z00_1418 =
												MAKE_YOUNG_PAIR(BgL_arg1340z00_1422,
												BgL_arg1341z00_1423);
										}
										BgL_arg1331z00_1413 =
											MAKE_YOUNG_PAIR(BgL_arg1335z00_1417, BgL_arg1336z00_1418);
									}
									BgL_arg1325z00_1408 =
										MAKE_YOUNG_PAIR(BgL_arg1329z00_1412, BgL_arg1331z00_1413);
								}
								BgL_arg1319z00_1403 =
									MAKE_YOUNG_PAIR(BgL_arg1323z00_1407, BgL_arg1325z00_1408);
							}
							BgL_arg1314z00_1398 =
								MAKE_YOUNG_PAIR(BgL_arg1318z00_1402, BgL_arg1319z00_1403);
						}
						BgL_arg1308z00_1393 =
							MAKE_YOUNG_PAIR(BgL_arg1312z00_1397, BgL_arg1314z00_1398);
					}
					BgL_arg1272z00_1387 =
						MAKE_YOUNG_PAIR(BgL_arg1307z00_1392, BgL_arg1308z00_1393);
				}
				return (BGl_za2crcsza2z00zz__crcz00 =
					MAKE_YOUNG_PAIR(BgL_arg1268z00_1386, BgL_arg1272z00_1387), BUNSPEC);
			}
		}

	}



/* crc-long */
	BGL_EXPORTED_DEF long BGl_crczd2longzd2zz__crcz00(unsigned char BgL_cz00_3,
		long BgL_crcz00_4, long BgL_polyz00_5, long BgL_lenz00_6)
	{
		{	/* Unsafe/crc.scm 69 */
			{

				if ((BgL_lenz00_6 >= 8L))
					{	/* Unsafe/crc.scm 111 */
						{	/* Unsafe/crc.scm 76 */
							long BgL_mz00_5481;

							BgL_mz00_5481 = (1L << (int) ((BgL_lenz00_6 - 1L)));
							{	/* Unsafe/crc.scm 77 */

								{
									long BgL_iz00_5483;
									long BgL_crcz00_5484;

									BgL_iz00_5483 = 0L;
									BgL_crcz00_5484 =
										(BgL_crcz00_4 ^
										(((unsigned char) (BgL_cz00_3)) <<
											(int) ((BgL_lenz00_6 - 8L))));
								BgL_loopz00_5482:
									if ((BgL_iz00_5483 == 8L))
										{	/* Unsafe/crc.scm 80 */
											return BgL_crcz00_5484;
										}
									else
										{
											long BgL_crcz00_5837;
											long BgL_iz00_5835;

											BgL_iz00_5835 = (BgL_iz00_5483 + 1L);
											BgL_crcz00_5837 =
												(
												(((BgL_mz00_5481 & BgL_crcz00_5484) >>
														(int) (
															(BgL_lenz00_6 - 1L))) * BgL_polyz00_5) ^
												(BgL_crcz00_5484 << (int) (1L)));
											BgL_crcz00_5484 = BgL_crcz00_5837;
											BgL_iz00_5483 = BgL_iz00_5835;
											goto BgL_loopz00_5482;
										}
								}
							}
						}
					}
				else
					{	/* Unsafe/crc.scm 111 */
						{	/* Unsafe/crc.scm 92 */
							long BgL_mz00_5485;

							BgL_mz00_5485 = (1L << (int) ((BgL_lenz00_6 - 1L)));
							{	/* Unsafe/crc.scm 94 */

								{
									long BgL_iz00_5487;
									long BgL_crcz00_5488;
									long BgL_shiftedzd2valuezd2_5489;

									BgL_iz00_5487 = 0L;
									BgL_crcz00_5488 = BgL_crcz00_4;
									BgL_shiftedzd2valuezd2_5489 =
										(((unsigned char) (BgL_cz00_3)) << (int) (BgL_lenz00_6));
								BgL_loopz00_5486:
									if ((BgL_iz00_5487 == 8L))
										{	/* Unsafe/crc.scm 98 */
											return BgL_crcz00_5488;
										}
									else
										{	/* Unsafe/crc.scm 100 */
											long BgL_crc2z00_5490;

											BgL_crc2z00_5490 =
												(BgL_crcz00_5488 ^
												(BgL_mz00_5485 &
													(BgL_shiftedzd2valuezd2_5489 >> (int) (8L))));
											{	/* Unsafe/crc.scm 104 */

												{
													long BgL_shiftedzd2valuezd2_5872;
													long BgL_crcz00_5863;
													long BgL_iz00_5861;

													BgL_iz00_5861 = (BgL_iz00_5487 + 1L);
													BgL_crcz00_5863 =
														(
														(((BgL_mz00_5485 & BgL_crc2z00_5490) >>
																(int) (
																	(BgL_lenz00_6 - 1L))) * BgL_polyz00_5) ^
														(BgL_crc2z00_5490 << (int) (1L)));
													BgL_shiftedzd2valuezd2_5872 =
														(BgL_shiftedzd2valuezd2_5489 << (int) (1L));
													BgL_shiftedzd2valuezd2_5489 =
														BgL_shiftedzd2valuezd2_5872;
													BgL_crcz00_5488 = BgL_crcz00_5863;
													BgL_iz00_5487 = BgL_iz00_5861;
													goto BgL_loopz00_5486;
												}
											}
										}
								}
							}
						}
					}
			}
		}

	}



/* &crc-long */
	obj_t BGl_z62crczd2longzb0zz__crcz00(obj_t BgL_envz00_5276,
		obj_t BgL_cz00_5277, obj_t BgL_crcz00_5278, obj_t BgL_polyz00_5279,
		obj_t BgL_lenz00_5280)
	{
		{	/* Unsafe/crc.scm 69 */
			{	/* Unsafe/crc.scm 75 */
				long BgL_tmpz00_5879;

				{	/* Unsafe/crc.scm 75 */
					long BgL_auxz00_5907;
					long BgL_auxz00_5898;
					long BgL_auxz00_5889;
					unsigned char BgL_auxz00_5880;

					{	/* Unsafe/crc.scm 75 */
						obj_t BgL_tmpz00_5908;

						if (INTEGERP(BgL_lenz00_5280))
							{	/* Unsafe/crc.scm 75 */
								BgL_tmpz00_5908 = BgL_lenz00_5280;
							}
						else
							{
								obj_t BgL_auxz00_5911;

								BgL_auxz00_5911 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(2995L), BGl_string2325z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_lenz00_5280);
								FAILURE(BgL_auxz00_5911, BFALSE, BFALSE);
							}
						BgL_auxz00_5907 = (long) CINT(BgL_tmpz00_5908);
					}
					{	/* Unsafe/crc.scm 75 */
						obj_t BgL_tmpz00_5899;

						if (INTEGERP(BgL_polyz00_5279))
							{	/* Unsafe/crc.scm 75 */
								BgL_tmpz00_5899 = BgL_polyz00_5279;
							}
						else
							{
								obj_t BgL_auxz00_5902;

								BgL_auxz00_5902 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(2995L), BGl_string2325z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_polyz00_5279);
								FAILURE(BgL_auxz00_5902, BFALSE, BFALSE);
							}
						BgL_auxz00_5898 = (long) CINT(BgL_tmpz00_5899);
					}
					{	/* Unsafe/crc.scm 75 */
						obj_t BgL_tmpz00_5890;

						if (INTEGERP(BgL_crcz00_5278))
							{	/* Unsafe/crc.scm 75 */
								BgL_tmpz00_5890 = BgL_crcz00_5278;
							}
						else
							{
								obj_t BgL_auxz00_5893;

								BgL_auxz00_5893 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(2995L), BGl_string2325z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_crcz00_5278);
								FAILURE(BgL_auxz00_5893, BFALSE, BFALSE);
							}
						BgL_auxz00_5889 = (long) CINT(BgL_tmpz00_5890);
					}
					{	/* Unsafe/crc.scm 75 */
						obj_t BgL_tmpz00_5881;

						if (CHARP(BgL_cz00_5277))
							{	/* Unsafe/crc.scm 75 */
								BgL_tmpz00_5881 = BgL_cz00_5277;
							}
						else
							{
								obj_t BgL_auxz00_5884;

								BgL_auxz00_5884 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(2995L), BGl_string2325z00zz__crcz00,
									BGl_string2326z00zz__crcz00, BgL_cz00_5277);
								FAILURE(BgL_auxz00_5884, BFALSE, BFALSE);
							}
						BgL_auxz00_5880 = CCHAR(BgL_tmpz00_5881);
					}
					BgL_tmpz00_5879 =
						BGl_crczd2longzd2zz__crcz00(BgL_auxz00_5880, BgL_auxz00_5889,
						BgL_auxz00_5898, BgL_auxz00_5907);
				}
				return BINT(BgL_tmpz00_5879);
			}
		}

	}



/* crc-elong */
	BGL_EXPORTED_DEF long BGl_crczd2elongzd2zz__crcz00(unsigned char BgL_cz00_7,
		long BgL_crcz00_8, long BgL_polyz00_9, long BgL_lenz00_10)
	{
		{	/* Unsafe/crc.scm 120 */
			if ((BgL_lenz00_10 >= 8L))
				{	/* Unsafe/crc.scm 122 */
					long BgL_octetz00_5491;

					BgL_octetz00_5491 = ((unsigned char) (BgL_cz00_7));
					{	/* Unsafe/crc.scm 123 */
						long BgL_valuez00_5492;
						long BgL_mz00_5493;

						{	/* Unsafe/crc.scm 123 */
							long BgL_arg1501z00_5494;
							long BgL_arg1502z00_5495;

							BgL_arg1501z00_5494 = (long) (BgL_octetz00_5491);
							BgL_arg1502z00_5495 = (BgL_lenz00_10 - 8L);
							BgL_valuez00_5492 =
								(BgL_arg1501z00_5494 << (int) (BgL_arg1502z00_5495));
						}
						BgL_mz00_5493 = (((long) 1) << (int) ((BgL_lenz00_10 - 1L)));
						{
							long BgL_iz00_5497;
							long BgL_crcz00_5498;

							BgL_iz00_5497 = 0L;
							BgL_crcz00_5498 = (BgL_crcz00_8 ^ BgL_valuez00_5492);
						BgL_loopz00_5496:
							if ((BgL_iz00_5497 == 8L))
								{	/* Unsafe/crc.scm 127 */
									return BgL_crcz00_5498;
								}
							else
								{	/* Unsafe/crc.scm 129 */
									long BgL_newzd2crczd2_5499;

									BgL_newzd2crczd2_5499 = (BgL_crcz00_5498 << (int) (1L));
									if ((((long) 0) == (BgL_mz00_5493 & BgL_crcz00_5498)))
										{
											long BgL_crcz00_5938;
											long BgL_iz00_5936;

											BgL_iz00_5936 = (BgL_iz00_5497 + 1L);
											BgL_crcz00_5938 = BgL_newzd2crczd2_5499;
											BgL_crcz00_5498 = BgL_crcz00_5938;
											BgL_iz00_5497 = BgL_iz00_5936;
											goto BgL_loopz00_5496;
										}
									else
										{
											long BgL_crcz00_5941;
											long BgL_iz00_5939;

											BgL_iz00_5939 = (BgL_iz00_5497 + 1L);
											BgL_crcz00_5941 = (BgL_newzd2crczd2_5499 ^ BgL_polyz00_9);
											BgL_crcz00_5498 = BgL_crcz00_5941;
											BgL_iz00_5497 = BgL_iz00_5939;
											goto BgL_loopz00_5496;
										}
								}
						}
					}
				}
			else
				{	/* Unsafe/crc.scm 133 */
					long BgL_arg1504z00_5500;

					{	/* Unsafe/crc.scm 133 */
						long BgL_arg1505z00_5501;
						long BgL_arg1506z00_5502;

						BgL_arg1505z00_5501 = (long) (BgL_crcz00_8);
						BgL_arg1506z00_5502 = (long) (BgL_polyz00_9);
						{

							if ((BgL_lenz00_10 >= 8L))
								{	/* Unsafe/crc.scm 111 */
									{	/* Unsafe/crc.scm 76 */
										long BgL_mz00_5505;

										BgL_mz00_5505 = (1L << (int) ((BgL_lenz00_10 - 1L)));
										{	/* Unsafe/crc.scm 77 */

											{
												long BgL_iz00_5507;
												long BgL_crcz00_5508;

												BgL_iz00_5507 = 0L;
												BgL_crcz00_5508 =
													(BgL_arg1505z00_5501 ^
													(((unsigned char) (BgL_cz00_7)) <<
														(int) ((BgL_lenz00_10 - 8L))));
											BgL_loopz00_5506:
												if ((BgL_iz00_5507 == 8L))
													{	/* Unsafe/crc.scm 80 */
														BgL_arg1504z00_5500 = BgL_crcz00_5508;
													}
												else
													{
														long BgL_crcz00_5955;
														long BgL_iz00_5953;

														BgL_iz00_5953 = (BgL_iz00_5507 + 1L);
														BgL_crcz00_5955 =
															(
															(((BgL_mz00_5505 & BgL_crcz00_5508) >>
																	(int) (
																		(BgL_lenz00_10 -
																			1L))) *
																BgL_arg1506z00_5502) ^ (BgL_crcz00_5508 <<
																(int) (1L)));
														BgL_crcz00_5508 = BgL_crcz00_5955;
														BgL_iz00_5507 = BgL_iz00_5953;
														goto BgL_loopz00_5506;
													}
											}
										}
									}
								}
							else
								{	/* Unsafe/crc.scm 111 */
									{	/* Unsafe/crc.scm 92 */
										long BgL_mz00_5509;

										BgL_mz00_5509 = (1L << (int) ((BgL_lenz00_10 - 1L)));
										{	/* Unsafe/crc.scm 94 */

											{
												long BgL_iz00_5511;
												long BgL_crcz00_5512;
												long BgL_shiftedzd2valuezd2_5513;

												BgL_iz00_5511 = 0L;
												BgL_crcz00_5512 = BgL_arg1505z00_5501;
												BgL_shiftedzd2valuezd2_5513 =
													(
													((unsigned char) (BgL_cz00_7)) <<
													(int) (BgL_lenz00_10));
											BgL_loopz00_5510:
												if ((BgL_iz00_5511 == 8L))
													{	/* Unsafe/crc.scm 98 */
														BgL_arg1504z00_5500 = BgL_crcz00_5512;
													}
												else
													{	/* Unsafe/crc.scm 100 */
														long BgL_crc2z00_5514;

														BgL_crc2z00_5514 =
															(BgL_crcz00_5512 ^
															(BgL_mz00_5509 &
																(BgL_shiftedzd2valuezd2_5513 >> (int) (8L))));
														{	/* Unsafe/crc.scm 104 */

															{
																long BgL_shiftedzd2valuezd2_5990;
																long BgL_crcz00_5981;
																long BgL_iz00_5979;

																BgL_iz00_5979 = (BgL_iz00_5511 + 1L);
																BgL_crcz00_5981 =
																	(
																	(((BgL_mz00_5509 & BgL_crc2z00_5514) >>
																			(int) (
																				(BgL_lenz00_10 -
																					1L))) *
																		BgL_arg1506z00_5502) ^ (BgL_crc2z00_5514 <<
																		(int) (1L)));
																BgL_shiftedzd2valuezd2_5990 =
																	(BgL_shiftedzd2valuezd2_5513 << (int) (1L));
																BgL_shiftedzd2valuezd2_5513 =
																	BgL_shiftedzd2valuezd2_5990;
																BgL_crcz00_5512 = BgL_crcz00_5981;
																BgL_iz00_5511 = BgL_iz00_5979;
																goto BgL_loopz00_5510;
															}
														}
													}
											}
										}
									}
								}
						}
					}
					return (long) (BgL_arg1504z00_5500);
		}}

	}



/* &crc-elong */
	obj_t BGl_z62crczd2elongzb0zz__crcz00(obj_t BgL_envz00_5281,
		obj_t BgL_cz00_5282, obj_t BgL_crcz00_5283, obj_t BgL_polyz00_5284,
		obj_t BgL_lenz00_5285)
	{
		{	/* Unsafe/crc.scm 120 */
			{	/* Unsafe/crc.scm 121 */
				long BgL_tmpz00_5998;

				{	/* Unsafe/crc.scm 121 */
					long BgL_auxz00_6026;
					long BgL_auxz00_6017;
					long BgL_auxz00_6008;
					unsigned char BgL_auxz00_5999;

					{	/* Unsafe/crc.scm 121 */
						obj_t BgL_tmpz00_6027;

						if (INTEGERP(BgL_lenz00_5285))
							{	/* Unsafe/crc.scm 121 */
								BgL_tmpz00_6027 = BgL_lenz00_5285;
							}
						else
							{
								obj_t BgL_auxz00_6030;

								BgL_auxz00_6030 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(4747L), BGl_string2328z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_lenz00_5285);
								FAILURE(BgL_auxz00_6030, BFALSE, BFALSE);
							}
						BgL_auxz00_6026 = (long) CINT(BgL_tmpz00_6027);
					}
					{	/* Unsafe/crc.scm 121 */
						obj_t BgL_tmpz00_6018;

						if (ELONGP(BgL_polyz00_5284))
							{	/* Unsafe/crc.scm 121 */
								BgL_tmpz00_6018 = BgL_polyz00_5284;
							}
						else
							{
								obj_t BgL_auxz00_6021;

								BgL_auxz00_6021 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(4747L), BGl_string2328z00zz__crcz00,
									BGl_string2329z00zz__crcz00, BgL_polyz00_5284);
								FAILURE(BgL_auxz00_6021, BFALSE, BFALSE);
							}
						BgL_auxz00_6017 = BELONG_TO_LONG(BgL_tmpz00_6018);
					}
					{	/* Unsafe/crc.scm 121 */
						obj_t BgL_tmpz00_6009;

						if (ELONGP(BgL_crcz00_5283))
							{	/* Unsafe/crc.scm 121 */
								BgL_tmpz00_6009 = BgL_crcz00_5283;
							}
						else
							{
								obj_t BgL_auxz00_6012;

								BgL_auxz00_6012 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(4747L), BGl_string2328z00zz__crcz00,
									BGl_string2329z00zz__crcz00, BgL_crcz00_5283);
								FAILURE(BgL_auxz00_6012, BFALSE, BFALSE);
							}
						BgL_auxz00_6008 = BELONG_TO_LONG(BgL_tmpz00_6009);
					}
					{	/* Unsafe/crc.scm 121 */
						obj_t BgL_tmpz00_6000;

						if (CHARP(BgL_cz00_5282))
							{	/* Unsafe/crc.scm 121 */
								BgL_tmpz00_6000 = BgL_cz00_5282;
							}
						else
							{
								obj_t BgL_auxz00_6003;

								BgL_auxz00_6003 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(4747L), BGl_string2328z00zz__crcz00,
									BGl_string2326z00zz__crcz00, BgL_cz00_5282);
								FAILURE(BgL_auxz00_6003, BFALSE, BFALSE);
							}
						BgL_auxz00_5999 = CCHAR(BgL_tmpz00_6000);
					}
					BgL_tmpz00_5998 =
						BGl_crczd2elongzd2zz__crcz00(BgL_auxz00_5999, BgL_auxz00_6008,
						BgL_auxz00_6017, BgL_auxz00_6026);
				}
				return make_belong(BgL_tmpz00_5998);
			}
		}

	}



/* crc-llong */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_crczd2llongzd2zz__crcz00(unsigned char
		BgL_cz00_11, BGL_LONGLONG_T BgL_crcz00_12, BGL_LONGLONG_T BgL_polyz00_13,
		long BgL_lenz00_14)
	{
		{	/* Unsafe/crc.scm 140 */
			if ((BgL_lenz00_14 >= 8L))
				{	/* Unsafe/crc.scm 142 */
					long BgL_octetz00_5515;

					BgL_octetz00_5515 = ((unsigned char) (BgL_cz00_11));
					{	/* Unsafe/crc.scm 143 */
						BGL_LONGLONG_T BgL_valuez00_5516;
						BGL_LONGLONG_T BgL_mz00_5517;

						{	/* Unsafe/crc.scm 143 */
							BGL_LONGLONG_T BgL_arg1521z00_5518;
							long BgL_arg1522z00_5519;

							BgL_arg1521z00_5518 = LONG_TO_LLONG(BgL_octetz00_5515);
							BgL_arg1522z00_5519 = (BgL_lenz00_14 - 8L);
							BgL_valuez00_5516 =
								(BgL_arg1521z00_5518 << (int) (BgL_arg1522z00_5519));
						}
						BgL_mz00_5517 =
							(((BGL_LONGLONG_T) 1) << (int) ((BgL_lenz00_14 - 1L)));
						{
							long BgL_iz00_5521;
							BGL_LONGLONG_T BgL_crcz00_5522;

							BgL_iz00_5521 = 0L;
							BgL_crcz00_5522 = (BgL_crcz00_12 ^ BgL_valuez00_5516);
						BgL_loopz00_5520:
							if ((BgL_iz00_5521 == 8L))
								{	/* Unsafe/crc.scm 147 */
									return BgL_crcz00_5522;
								}
							else
								{	/* Unsafe/crc.scm 149 */
									BGL_LONGLONG_T BgL_newzd2crczd2_5523;

									BgL_newzd2crczd2_5523 = (BgL_crcz00_5522 << (int) (1L));
									if (
										(((BGL_LONGLONG_T) 0) == (BgL_mz00_5517 & BgL_crcz00_5522)))
										{
											BGL_LONGLONG_T BgL_crcz00_6057;
											long BgL_iz00_6055;

											BgL_iz00_6055 = (BgL_iz00_5521 + 1L);
											BgL_crcz00_6057 = BgL_newzd2crczd2_5523;
											BgL_crcz00_5522 = BgL_crcz00_6057;
											BgL_iz00_5521 = BgL_iz00_6055;
											goto BgL_loopz00_5520;
										}
									else
										{
											BGL_LONGLONG_T BgL_crcz00_6060;
											long BgL_iz00_6058;

											BgL_iz00_6058 = (BgL_iz00_5521 + 1L);
											BgL_crcz00_6060 =
												(BgL_newzd2crczd2_5523 ^ BgL_polyz00_13);
											BgL_crcz00_5522 = BgL_crcz00_6060;
											BgL_iz00_5521 = BgL_iz00_6058;
											goto BgL_loopz00_5520;
										}
								}
						}
					}
				}
			else
				{	/* Unsafe/crc.scm 153 */
					long BgL_arg1524z00_5524;

					{	/* Unsafe/crc.scm 153 */
						long BgL_arg1525z00_5525;
						long BgL_arg1526z00_5526;

						BgL_arg1525z00_5525 = LLONG_TO_LONG(BgL_crcz00_12);
						BgL_arg1526z00_5526 = LLONG_TO_LONG(BgL_polyz00_13);
						{

							if ((BgL_lenz00_14 >= 8L))
								{	/* Unsafe/crc.scm 111 */
									{	/* Unsafe/crc.scm 76 */
										long BgL_mz00_5529;

										BgL_mz00_5529 = (1L << (int) ((BgL_lenz00_14 - 1L)));
										{	/* Unsafe/crc.scm 77 */

											{
												long BgL_iz00_5531;
												long BgL_crcz00_5532;

												BgL_iz00_5531 = 0L;
												BgL_crcz00_5532 =
													(BgL_arg1525z00_5525 ^
													(((unsigned char) (BgL_cz00_11)) <<
														(int) ((BgL_lenz00_14 - 8L))));
											BgL_loopz00_5530:
												if ((BgL_iz00_5531 == 8L))
													{	/* Unsafe/crc.scm 80 */
														BgL_arg1524z00_5524 = BgL_crcz00_5532;
													}
												else
													{
														long BgL_crcz00_6074;
														long BgL_iz00_6072;

														BgL_iz00_6072 = (BgL_iz00_5531 + 1L);
														BgL_crcz00_6074 =
															(
															(((BgL_mz00_5529 & BgL_crcz00_5532) >>
																	(int) (
																		(BgL_lenz00_14 -
																			1L))) *
																BgL_arg1526z00_5526) ^ (BgL_crcz00_5532 <<
																(int) (1L)));
														BgL_crcz00_5532 = BgL_crcz00_6074;
														BgL_iz00_5531 = BgL_iz00_6072;
														goto BgL_loopz00_5530;
													}
											}
										}
									}
								}
							else
								{	/* Unsafe/crc.scm 111 */
									{	/* Unsafe/crc.scm 92 */
										long BgL_mz00_5533;

										BgL_mz00_5533 = (1L << (int) ((BgL_lenz00_14 - 1L)));
										{	/* Unsafe/crc.scm 94 */

											{
												long BgL_iz00_5535;
												long BgL_crcz00_5536;
												long BgL_shiftedzd2valuezd2_5537;

												BgL_iz00_5535 = 0L;
												BgL_crcz00_5536 = BgL_arg1525z00_5525;
												BgL_shiftedzd2valuezd2_5537 =
													(
													((unsigned char) (BgL_cz00_11)) <<
													(int) (BgL_lenz00_14));
											BgL_loopz00_5534:
												if ((BgL_iz00_5535 == 8L))
													{	/* Unsafe/crc.scm 98 */
														BgL_arg1524z00_5524 = BgL_crcz00_5536;
													}
												else
													{	/* Unsafe/crc.scm 100 */
														long BgL_crc2z00_5538;

														BgL_crc2z00_5538 =
															(BgL_crcz00_5536 ^
															(BgL_mz00_5533 &
																(BgL_shiftedzd2valuezd2_5537 >> (int) (8L))));
														{	/* Unsafe/crc.scm 104 */

															{
																long BgL_shiftedzd2valuezd2_6109;
																long BgL_crcz00_6100;
																long BgL_iz00_6098;

																BgL_iz00_6098 = (BgL_iz00_5535 + 1L);
																BgL_crcz00_6100 =
																	(
																	(((BgL_mz00_5533 & BgL_crc2z00_5538) >>
																			(int) (
																				(BgL_lenz00_14 -
																					1L))) *
																		BgL_arg1526z00_5526) ^ (BgL_crc2z00_5538 <<
																		(int) (1L)));
																BgL_shiftedzd2valuezd2_6109 =
																	(BgL_shiftedzd2valuezd2_5537 << (int) (1L));
																BgL_shiftedzd2valuezd2_5537 =
																	BgL_shiftedzd2valuezd2_6109;
																BgL_crcz00_5536 = BgL_crcz00_6100;
																BgL_iz00_5535 = BgL_iz00_6098;
																goto BgL_loopz00_5534;
															}
														}
													}
											}
										}
									}
								}
						}
					}
					return LONG_TO_LLONG(BgL_arg1524z00_5524);
				}
		}

	}



/* &crc-llong */
	obj_t BGl_z62crczd2llongzb0zz__crcz00(obj_t BgL_envz00_5286,
		obj_t BgL_cz00_5287, obj_t BgL_crcz00_5288, obj_t BgL_polyz00_5289,
		obj_t BgL_lenz00_5290)
	{
		{	/* Unsafe/crc.scm 140 */
			{	/* Unsafe/crc.scm 141 */
				BGL_LONGLONG_T BgL_tmpz00_6117;

				{	/* Unsafe/crc.scm 141 */
					long BgL_auxz00_6145;
					BGL_LONGLONG_T BgL_auxz00_6136;
					BGL_LONGLONG_T BgL_auxz00_6127;
					unsigned char BgL_auxz00_6118;

					{	/* Unsafe/crc.scm 141 */
						obj_t BgL_tmpz00_6146;

						if (INTEGERP(BgL_lenz00_5290))
							{	/* Unsafe/crc.scm 141 */
								BgL_tmpz00_6146 = BgL_lenz00_5290;
							}
						else
							{
								obj_t BgL_auxz00_6149;

								BgL_auxz00_6149 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(5724L), BGl_string2330z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_lenz00_5290);
								FAILURE(BgL_auxz00_6149, BFALSE, BFALSE);
							}
						BgL_auxz00_6145 = (long) CINT(BgL_tmpz00_6146);
					}
					{	/* Unsafe/crc.scm 141 */
						obj_t BgL_tmpz00_6137;

						if (LLONGP(BgL_polyz00_5289))
							{	/* Unsafe/crc.scm 141 */
								BgL_tmpz00_6137 = BgL_polyz00_5289;
							}
						else
							{
								obj_t BgL_auxz00_6140;

								BgL_auxz00_6140 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(5724L), BGl_string2330z00zz__crcz00,
									BGl_string2331z00zz__crcz00, BgL_polyz00_5289);
								FAILURE(BgL_auxz00_6140, BFALSE, BFALSE);
							}
						BgL_auxz00_6136 = BLLONG_TO_LLONG(BgL_tmpz00_6137);
					}
					{	/* Unsafe/crc.scm 141 */
						obj_t BgL_tmpz00_6128;

						if (LLONGP(BgL_crcz00_5288))
							{	/* Unsafe/crc.scm 141 */
								BgL_tmpz00_6128 = BgL_crcz00_5288;
							}
						else
							{
								obj_t BgL_auxz00_6131;

								BgL_auxz00_6131 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(5724L), BGl_string2330z00zz__crcz00,
									BGl_string2331z00zz__crcz00, BgL_crcz00_5288);
								FAILURE(BgL_auxz00_6131, BFALSE, BFALSE);
							}
						BgL_auxz00_6127 = BLLONG_TO_LLONG(BgL_tmpz00_6128);
					}
					{	/* Unsafe/crc.scm 141 */
						obj_t BgL_tmpz00_6119;

						if (CHARP(BgL_cz00_5287))
							{	/* Unsafe/crc.scm 141 */
								BgL_tmpz00_6119 = BgL_cz00_5287;
							}
						else
							{
								obj_t BgL_auxz00_6122;

								BgL_auxz00_6122 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(5724L), BGl_string2330z00zz__crcz00,
									BGl_string2326z00zz__crcz00, BgL_cz00_5287);
								FAILURE(BgL_auxz00_6122, BFALSE, BFALSE);
							}
						BgL_auxz00_6118 = CCHAR(BgL_tmpz00_6119);
					}
					BgL_tmpz00_6117 =
						BGl_crczd2llongzd2zz__crcz00(BgL_auxz00_6118, BgL_auxz00_6127,
						BgL_auxz00_6136, BgL_auxz00_6145);
				}
				return make_bllong(BgL_tmpz00_6117);
			}
		}

	}



/* crc-long-le */
	BGL_EXPORTED_DEF long BGl_crczd2longzd2lez00zz__crcz00(unsigned char
		BgL_cz00_15, long BgL_crcz00_16, long BgL_polyz00_17, long BgL_lenz00_18)
	{
		{	/* Unsafe/crc.scm 160 */
			{
				long BgL_iz00_5540;
				long BgL_crcz00_5541;

				BgL_iz00_5540 = 0L;
				BgL_crcz00_5541 = (BgL_crcz00_16 ^ ((unsigned char) (BgL_cz00_15)));
			BgL_loopz00_5539:
				if ((BgL_iz00_5540 == 8L))
					{	/* Unsafe/crc.scm 164 */
						return BgL_crcz00_5541;
					}
				else
					{
						long BgL_crcz00_6160;
						long BgL_iz00_6158;

						BgL_iz00_6158 = (BgL_iz00_5540 + 1L);
						BgL_crcz00_6160 =
							(
							((1L & BgL_crcz00_5541) * BgL_polyz00_17) ^
							(long) (((unsigned long) (BgL_crcz00_5541) >> (int) (1L))));
						BgL_crcz00_5541 = BgL_crcz00_6160;
						BgL_iz00_5540 = BgL_iz00_6158;
						goto BgL_loopz00_5539;
					}
			}
		}

	}



/* &crc-long-le */
	obj_t BGl_z62crczd2longzd2lez62zz__crcz00(obj_t BgL_envz00_5291,
		obj_t BgL_cz00_5292, obj_t BgL_crcz00_5293, obj_t BgL_polyz00_5294,
		obj_t BgL_lenz00_5295)
	{
		{	/* Unsafe/crc.scm 160 */
			{	/* Unsafe/crc.scm 161 */
				long BgL_tmpz00_6171;

				{	/* Unsafe/crc.scm 161 */
					long BgL_auxz00_6199;
					long BgL_auxz00_6190;
					long BgL_auxz00_6181;
					unsigned char BgL_auxz00_6172;

					{	/* Unsafe/crc.scm 161 */
						obj_t BgL_tmpz00_6200;

						if (INTEGERP(BgL_lenz00_5295))
							{	/* Unsafe/crc.scm 161 */
								BgL_tmpz00_6200 = BgL_lenz00_5295;
							}
						else
							{
								obj_t BgL_auxz00_6203;

								BgL_auxz00_6203 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(6700L), BGl_string2332z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_lenz00_5295);
								FAILURE(BgL_auxz00_6203, BFALSE, BFALSE);
							}
						BgL_auxz00_6199 = (long) CINT(BgL_tmpz00_6200);
					}
					{	/* Unsafe/crc.scm 161 */
						obj_t BgL_tmpz00_6191;

						if (INTEGERP(BgL_polyz00_5294))
							{	/* Unsafe/crc.scm 161 */
								BgL_tmpz00_6191 = BgL_polyz00_5294;
							}
						else
							{
								obj_t BgL_auxz00_6194;

								BgL_auxz00_6194 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(6700L), BGl_string2332z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_polyz00_5294);
								FAILURE(BgL_auxz00_6194, BFALSE, BFALSE);
							}
						BgL_auxz00_6190 = (long) CINT(BgL_tmpz00_6191);
					}
					{	/* Unsafe/crc.scm 161 */
						obj_t BgL_tmpz00_6182;

						if (INTEGERP(BgL_crcz00_5293))
							{	/* Unsafe/crc.scm 161 */
								BgL_tmpz00_6182 = BgL_crcz00_5293;
							}
						else
							{
								obj_t BgL_auxz00_6185;

								BgL_auxz00_6185 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(6700L), BGl_string2332z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_crcz00_5293);
								FAILURE(BgL_auxz00_6185, BFALSE, BFALSE);
							}
						BgL_auxz00_6181 = (long) CINT(BgL_tmpz00_6182);
					}
					{	/* Unsafe/crc.scm 161 */
						obj_t BgL_tmpz00_6173;

						if (CHARP(BgL_cz00_5292))
							{	/* Unsafe/crc.scm 161 */
								BgL_tmpz00_6173 = BgL_cz00_5292;
							}
						else
							{
								obj_t BgL_auxz00_6176;

								BgL_auxz00_6176 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(6700L), BGl_string2332z00zz__crcz00,
									BGl_string2326z00zz__crcz00, BgL_cz00_5292);
								FAILURE(BgL_auxz00_6176, BFALSE, BFALSE);
							}
						BgL_auxz00_6172 = CCHAR(BgL_tmpz00_6173);
					}
					BgL_tmpz00_6171 =
						BGl_crczd2longzd2lez00zz__crcz00(BgL_auxz00_6172, BgL_auxz00_6181,
						BgL_auxz00_6190, BgL_auxz00_6199);
				}
				return BINT(BgL_tmpz00_6171);
			}
		}

	}



/* crc-elong-le */
	BGL_EXPORTED_DEF long BGl_crczd2elongzd2lez00zz__crcz00(unsigned char
		BgL_cz00_19, long BgL_crcz00_20, long BgL_polyz00_21, long BgL_lenz00_22)
	{
		{	/* Unsafe/crc.scm 174 */
			{	/* Unsafe/crc.scm 175 */
				long BgL_octetz00_5542;

				BgL_octetz00_5542 = ((unsigned char) (BgL_cz00_19));
				{	/* Unsafe/crc.scm 176 */
					long BgL_g1043z00_5543;

					{	/* Unsafe/crc.scm 177 */
						long BgL_arg1546z00_5544;

						BgL_arg1546z00_5544 = (long) (BgL_octetz00_5542);
						BgL_g1043z00_5543 = (BgL_crcz00_20 ^ BgL_arg1546z00_5544);
					}
					{
						long BgL_iz00_5546;
						long BgL_crcz00_5547;

						BgL_iz00_5546 = 0L;
						BgL_crcz00_5547 = BgL_g1043z00_5543;
					BgL_loopz00_5545:
						if ((BgL_iz00_5546 == 8L))
							{	/* Unsafe/crc.scm 178 */
								return BgL_crcz00_5547;
							}
						else
							{	/* Unsafe/crc.scm 180 */
								long BgL_newzd2crczd2_5548;

								{	/* Unsafe/crc.scm 180 */
									unsigned long BgL_xz00_5549;

									BgL_xz00_5549 = (unsigned long) (BgL_crcz00_5547);
									{	/* Unsafe/crc.scm 180 */
										unsigned long BgL_tmpz00_6217;

										BgL_tmpz00_6217 = (BgL_xz00_5549 >> (int) (1L));
										BgL_newzd2crczd2_5548 = (long) (BgL_tmpz00_6217);
								}}
								{
									long BgL_crcz00_6223;
									long BgL_iz00_6221;

									BgL_iz00_6221 = (BgL_iz00_5546 + 1L);
									BgL_crcz00_6223 =
										(
										((((long) 1) & BgL_crcz00_5547) *
											BgL_polyz00_21) ^ BgL_newzd2crczd2_5548);
									BgL_crcz00_5547 = BgL_crcz00_6223;
									BgL_iz00_5546 = BgL_iz00_6221;
									goto BgL_loopz00_5545;
								}
							}
					}
				}
			}
		}

	}



/* &crc-elong-le */
	obj_t BGl_z62crczd2elongzd2lez62zz__crcz00(obj_t BgL_envz00_5296,
		obj_t BgL_cz00_5297, obj_t BgL_crcz00_5298, obj_t BgL_polyz00_5299,
		obj_t BgL_lenz00_5300)
	{
		{	/* Unsafe/crc.scm 174 */
			{	/* Unsafe/crc.scm 175 */
				long BgL_tmpz00_6227;

				{	/* Unsafe/crc.scm 175 */
					long BgL_auxz00_6255;
					long BgL_auxz00_6246;
					long BgL_auxz00_6237;
					unsigned char BgL_auxz00_6228;

					{	/* Unsafe/crc.scm 175 */
						obj_t BgL_tmpz00_6256;

						if (INTEGERP(BgL_lenz00_5300))
							{	/* Unsafe/crc.scm 175 */
								BgL_tmpz00_6256 = BgL_lenz00_5300;
							}
						else
							{
								obj_t BgL_auxz00_6259;

								BgL_auxz00_6259 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7255L), BGl_string2333z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_lenz00_5300);
								FAILURE(BgL_auxz00_6259, BFALSE, BFALSE);
							}
						BgL_auxz00_6255 = (long) CINT(BgL_tmpz00_6256);
					}
					{	/* Unsafe/crc.scm 175 */
						obj_t BgL_tmpz00_6247;

						if (ELONGP(BgL_polyz00_5299))
							{	/* Unsafe/crc.scm 175 */
								BgL_tmpz00_6247 = BgL_polyz00_5299;
							}
						else
							{
								obj_t BgL_auxz00_6250;

								BgL_auxz00_6250 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7255L), BGl_string2333z00zz__crcz00,
									BGl_string2329z00zz__crcz00, BgL_polyz00_5299);
								FAILURE(BgL_auxz00_6250, BFALSE, BFALSE);
							}
						BgL_auxz00_6246 = BELONG_TO_LONG(BgL_tmpz00_6247);
					}
					{	/* Unsafe/crc.scm 175 */
						obj_t BgL_tmpz00_6238;

						if (ELONGP(BgL_crcz00_5298))
							{	/* Unsafe/crc.scm 175 */
								BgL_tmpz00_6238 = BgL_crcz00_5298;
							}
						else
							{
								obj_t BgL_auxz00_6241;

								BgL_auxz00_6241 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7255L), BGl_string2333z00zz__crcz00,
									BGl_string2329z00zz__crcz00, BgL_crcz00_5298);
								FAILURE(BgL_auxz00_6241, BFALSE, BFALSE);
							}
						BgL_auxz00_6237 = BELONG_TO_LONG(BgL_tmpz00_6238);
					}
					{	/* Unsafe/crc.scm 175 */
						obj_t BgL_tmpz00_6229;

						if (CHARP(BgL_cz00_5297))
							{	/* Unsafe/crc.scm 175 */
								BgL_tmpz00_6229 = BgL_cz00_5297;
							}
						else
							{
								obj_t BgL_auxz00_6232;

								BgL_auxz00_6232 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7255L), BGl_string2333z00zz__crcz00,
									BGl_string2326z00zz__crcz00, BgL_cz00_5297);
								FAILURE(BgL_auxz00_6232, BFALSE, BFALSE);
							}
						BgL_auxz00_6228 = CCHAR(BgL_tmpz00_6229);
					}
					BgL_tmpz00_6227 =
						BGl_crczd2elongzd2lez00zz__crcz00(BgL_auxz00_6228, BgL_auxz00_6237,
						BgL_auxz00_6246, BgL_auxz00_6255);
				}
				return make_belong(BgL_tmpz00_6227);
			}
		}

	}



/* crc-llong-le */
	BGL_EXPORTED_DEF BGL_LONGLONG_T BGl_crczd2llongzd2lez00zz__crcz00(unsigned
		char BgL_cz00_23, BGL_LONGLONG_T BgL_crcz00_24,
		BGL_LONGLONG_T BgL_polyz00_25, long BgL_lenz00_26)
	{
		{	/* Unsafe/crc.scm 188 */
			{	/* Unsafe/crc.scm 189 */
				long BgL_octetz00_5550;

				BgL_octetz00_5550 = ((unsigned char) (BgL_cz00_23));
				{	/* Unsafe/crc.scm 190 */
					BGL_LONGLONG_T BgL_g1044z00_5551;

					{	/* Unsafe/crc.scm 191 */
						BGL_LONGLONG_T BgL_arg1555z00_5552;

						BgL_arg1555z00_5552 = LONG_TO_LLONG(BgL_octetz00_5550);
						BgL_g1044z00_5551 = (BgL_crcz00_24 ^ BgL_arg1555z00_5552);
					}
					{
						long BgL_iz00_5554;
						BGL_LONGLONG_T BgL_crcz00_5555;

						BgL_iz00_5554 = 0L;
						BgL_crcz00_5555 = BgL_g1044z00_5551;
					BgL_loopz00_5553:
						if ((BgL_iz00_5554 == 8L))
							{	/* Unsafe/crc.scm 192 */
								return BgL_crcz00_5555;
							}
						else
							{	/* Unsafe/crc.scm 194 */
								BGL_LONGLONG_T BgL_newzd2crczd2_5556;

								{	/* Unsafe/crc.scm 194 */
									unsigned BGL_LONGLONG_T BgL_xz00_5557;

									BgL_xz00_5557 = (unsigned BGL_LONGLONG_T) (BgL_crcz00_5555);
									{	/* Unsafe/crc.scm 194 */
										unsigned BGL_LONGLONG_T BgL_tmpz00_6273;

										BgL_tmpz00_6273 = (BgL_xz00_5557 >> (int) (1L));
										BgL_newzd2crczd2_5556 = (BGL_LONGLONG_T) (BgL_tmpz00_6273);
								}}
								{
									BGL_LONGLONG_T BgL_crcz00_6279;
									long BgL_iz00_6277;

									BgL_iz00_6277 = (BgL_iz00_5554 + 1L);
									BgL_crcz00_6279 =
										(
										((((BGL_LONGLONG_T) 1) & BgL_crcz00_5555) *
											BgL_polyz00_25) ^ BgL_newzd2crczd2_5556);
									BgL_crcz00_5555 = BgL_crcz00_6279;
									BgL_iz00_5554 = BgL_iz00_6277;
									goto BgL_loopz00_5553;
								}
							}
					}
				}
			}
		}

	}



/* &crc-llong-le */
	obj_t BGl_z62crczd2llongzd2lez62zz__crcz00(obj_t BgL_envz00_5301,
		obj_t BgL_cz00_5302, obj_t BgL_crcz00_5303, obj_t BgL_polyz00_5304,
		obj_t BgL_lenz00_5305)
	{
		{	/* Unsafe/crc.scm 188 */
			{	/* Unsafe/crc.scm 189 */
				BGL_LONGLONG_T BgL_tmpz00_6283;

				{	/* Unsafe/crc.scm 189 */
					long BgL_auxz00_6311;
					BGL_LONGLONG_T BgL_auxz00_6302;
					BGL_LONGLONG_T BgL_auxz00_6293;
					unsigned char BgL_auxz00_6284;

					{	/* Unsafe/crc.scm 189 */
						obj_t BgL_tmpz00_6312;

						if (INTEGERP(BgL_lenz00_5305))
							{	/* Unsafe/crc.scm 189 */
								BgL_tmpz00_6312 = BgL_lenz00_5305;
							}
						else
							{
								obj_t BgL_auxz00_6315;

								BgL_auxz00_6315 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7851L), BGl_string2334z00zz__crcz00,
									BGl_string2327z00zz__crcz00, BgL_lenz00_5305);
								FAILURE(BgL_auxz00_6315, BFALSE, BFALSE);
							}
						BgL_auxz00_6311 = (long) CINT(BgL_tmpz00_6312);
					}
					{	/* Unsafe/crc.scm 189 */
						obj_t BgL_tmpz00_6303;

						if (LLONGP(BgL_polyz00_5304))
							{	/* Unsafe/crc.scm 189 */
								BgL_tmpz00_6303 = BgL_polyz00_5304;
							}
						else
							{
								obj_t BgL_auxz00_6306;

								BgL_auxz00_6306 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7851L), BGl_string2334z00zz__crcz00,
									BGl_string2331z00zz__crcz00, BgL_polyz00_5304);
								FAILURE(BgL_auxz00_6306, BFALSE, BFALSE);
							}
						BgL_auxz00_6302 = BLLONG_TO_LLONG(BgL_tmpz00_6303);
					}
					{	/* Unsafe/crc.scm 189 */
						obj_t BgL_tmpz00_6294;

						if (LLONGP(BgL_crcz00_5303))
							{	/* Unsafe/crc.scm 189 */
								BgL_tmpz00_6294 = BgL_crcz00_5303;
							}
						else
							{
								obj_t BgL_auxz00_6297;

								BgL_auxz00_6297 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7851L), BGl_string2334z00zz__crcz00,
									BGl_string2331z00zz__crcz00, BgL_crcz00_5303);
								FAILURE(BgL_auxz00_6297, BFALSE, BFALSE);
							}
						BgL_auxz00_6293 = BLLONG_TO_LLONG(BgL_tmpz00_6294);
					}
					{	/* Unsafe/crc.scm 189 */
						obj_t BgL_tmpz00_6285;

						if (CHARP(BgL_cz00_5302))
							{	/* Unsafe/crc.scm 189 */
								BgL_tmpz00_6285 = BgL_cz00_5302;
							}
						else
							{
								obj_t BgL_auxz00_6288;

								BgL_auxz00_6288 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string2324z00zz__crcz00,
									BINT(7851L), BGl_string2334z00zz__crcz00,
									BGl_string2326z00zz__crcz00, BgL_cz00_5302);
								FAILURE(BgL_auxz00_6288, BFALSE, BFALSE);
							}
						BgL_auxz00_6284 = CCHAR(BgL_tmpz00_6285);
					}
					BgL_tmpz00_6283 =
						BGl_crczd2llongzd2lez00zz__crcz00(BgL_auxz00_6284, BgL_auxz00_6293,
						BgL_auxz00_6302, BgL_auxz00_6311);
				}
				return make_bllong(BgL_tmpz00_6283);
			}
		}

	}



/* crc-polynomial-be->le */
	BGL_EXPORTED_DEF obj_t BGl_crczd2polynomialzd2bezd2ze3lez31zz__crcz00(obj_t
		BgL_lenz00_27, obj_t BgL_polyz00_28)
	{
		{	/* Unsafe/crc.scm 202 */
			{	/* Unsafe/crc.scm 203 */
				obj_t BgL_typez00_1676;

				if (INTEGERP(BgL_polyz00_28))
					{	/* Unsafe/crc.scm 205 */
						BgL_typez00_1676 = BGl_symbol2335z00zz__crcz00;
					}
				else
					{	/* Unsafe/crc.scm 205 */
						if (ELONGP(BgL_polyz00_28))
							{	/* Unsafe/crc.scm 206 */
								BgL_typez00_1676 = BGl_symbol2337z00zz__crcz00;
							}
						else
							{	/* Unsafe/crc.scm 206 */
								if (LLONGP(BgL_polyz00_28))
									{	/* Unsafe/crc.scm 207 */
										BgL_typez00_1676 = BGl_symbol2339z00zz__crcz00;
									}
								else
									{	/* Unsafe/crc.scm 207 */
										BgL_typez00_1676 =
											BGl_errorz00zz__errorz00(BGl_symbol2341z00zz__crcz00,
											BGl_string2343z00zz__crcz00, BgL_polyz00_28);
									}
							}
					}
				{	/* Unsafe/crc.scm 203 */
					obj_t BgL_onez00_1677;

					if ((BgL_typez00_1676 == BGl_symbol2335z00zz__crcz00))
						{	/* Unsafe/crc.scm 210 */
							BgL_onez00_1677 = BINT(1L);
						}
					else
						{	/* Unsafe/crc.scm 210 */
							if ((BgL_typez00_1676 == BGl_symbol2337z00zz__crcz00))
								{	/* Unsafe/crc.scm 210 */
									BgL_onez00_1677 = BGl_elong2344z00zz__crcz00;
								}
							else
								{	/* Unsafe/crc.scm 210 */
									if ((BgL_typez00_1676 == BGl_symbol2339z00zz__crcz00))
										{	/* Unsafe/crc.scm 210 */
											BgL_onez00_1677 = BGl_llong2345z00zz__crcz00;
										}
									else
										{	/* Unsafe/crc.scm 210 */
											BgL_onez00_1677 = BUNSPEC;
										}
								}
						}
					{	/* Unsafe/crc.scm 210 */
						obj_t BgL_za7eroza7_1678;

						if ((BgL_typez00_1676 == BGl_symbol2335z00zz__crcz00))
							{	/* Unsafe/crc.scm 211 */
								BgL_za7eroza7_1678 = BINT(0L);
							}
						else
							{	/* Unsafe/crc.scm 211 */
								if ((BgL_typez00_1676 == BGl_symbol2337z00zz__crcz00))
									{	/* Unsafe/crc.scm 211 */
										BgL_za7eroza7_1678 = BGl_elong2346z00zz__crcz00;
									}
								else
									{	/* Unsafe/crc.scm 211 */
										if ((BgL_typez00_1676 == BGl_symbol2339z00zz__crcz00))
											{	/* Unsafe/crc.scm 211 */
												BgL_za7eroza7_1678 = BGl_llong2347z00zz__crcz00;
											}
										else
											{	/* Unsafe/crc.scm 211 */
												BgL_za7eroza7_1678 = BUNSPEC;
											}
									}
							}
						{	/* Unsafe/crc.scm 211 */
							obj_t BgL_lshz00_1679;

							if ((BgL_typez00_1676 == BGl_symbol2335z00zz__crcz00))
								{	/* Unsafe/crc.scm 212 */
									BgL_lshz00_1679 = BGl_bitzd2lshzd2envz00zz__bitz00;
								}
							else
								{	/* Unsafe/crc.scm 212 */
									if ((BgL_typez00_1676 == BGl_symbol2337z00zz__crcz00))
										{	/* Unsafe/crc.scm 212 */
											BgL_lshz00_1679 = BGl_bitzd2lshelongzd2envz00zz__bitz00;
										}
									else
										{	/* Unsafe/crc.scm 212 */
											if ((BgL_typez00_1676 == BGl_symbol2339z00zz__crcz00))
												{	/* Unsafe/crc.scm 212 */
													BgL_lshz00_1679 =
														BGl_bitzd2lshllongzd2envz00zz__bitz00;
												}
											else
												{	/* Unsafe/crc.scm 212 */
													BgL_lshz00_1679 = BUNSPEC;
												}
										}
								}
							{	/* Unsafe/crc.scm 212 */
								obj_t BgL_rshz00_1680;

								if ((BgL_typez00_1676 == BGl_symbol2335z00zz__crcz00))
									{	/* Unsafe/crc.scm 215 */
										BgL_rshz00_1680 = BGl_bitzd2rshzd2envz00zz__bitz00;
									}
								else
									{	/* Unsafe/crc.scm 215 */
										if ((BgL_typez00_1676 == BGl_symbol2337z00zz__crcz00))
											{	/* Unsafe/crc.scm 215 */
												BgL_rshz00_1680 = BGl_bitzd2rshelongzd2envz00zz__bitz00;
											}
										else
											{	/* Unsafe/crc.scm 215 */
												if ((BgL_typez00_1676 == BGl_symbol2339z00zz__crcz00))
													{	/* Unsafe/crc.scm 215 */
														BgL_rshz00_1680 =
															BGl_bitzd2rshllongzd2envz00zz__bitz00;
													}
												else
													{	/* Unsafe/crc.scm 215 */
														BgL_rshz00_1680 = BUNSPEC;
													}
											}
									}
								{	/* Unsafe/crc.scm 215 */
									obj_t BgL_bzd2andzd2_1681;

									if ((BgL_typez00_1676 == BGl_symbol2335z00zz__crcz00))
										{	/* Unsafe/crc.scm 218 */
											BgL_bzd2andzd2_1681 = BGl_bitzd2andzd2envz00zz__bitz00;
										}
									else
										{	/* Unsafe/crc.scm 218 */
											if ((BgL_typez00_1676 == BGl_symbol2337z00zz__crcz00))
												{	/* Unsafe/crc.scm 218 */
													BgL_bzd2andzd2_1681 =
														BGl_bitzd2andelongzd2envz00zz__bitz00;
												}
											else
												{	/* Unsafe/crc.scm 218 */
													if ((BgL_typez00_1676 == BGl_symbol2339z00zz__crcz00))
														{	/* Unsafe/crc.scm 218 */
															BgL_bzd2andzd2_1681 =
																BGl_bitzd2andllongzd2envz00zz__bitz00;
														}
													else
														{	/* Unsafe/crc.scm 218 */
															BgL_bzd2andzd2_1681 = BUNSPEC;
														}
												}
										}
									{	/* Unsafe/crc.scm 218 */
										obj_t BgL_bzd2orzd2_1682;

										if ((BgL_typez00_1676 == BGl_symbol2335z00zz__crcz00))
											{	/* Unsafe/crc.scm 221 */
												BgL_bzd2orzd2_1682 = BGl_bitzd2orzd2envz00zz__bitz00;
											}
										else
											{	/* Unsafe/crc.scm 221 */
												if ((BgL_typez00_1676 == BGl_symbol2337z00zz__crcz00))
													{	/* Unsafe/crc.scm 221 */
														BgL_bzd2orzd2_1682 =
															BGl_bitzd2orelongzd2envz00zz__bitz00;
													}
												else
													{	/* Unsafe/crc.scm 221 */
														if (
															(BgL_typez00_1676 == BGl_symbol2339z00zz__crcz00))
															{	/* Unsafe/crc.scm 221 */
																BgL_bzd2orzd2_1682 =
																	BGl_bitzd2orllongzd2envz00zz__bitz00;
															}
														else
															{	/* Unsafe/crc.scm 221 */
																BgL_bzd2orzd2_1682 = BUNSPEC;
															}
													}
											}
										{	/* Unsafe/crc.scm 221 */

											{
												long BgL_iz00_3005;
												obj_t BgL_polyz00_3006;
												obj_t BgL_resz00_3007;

												BgL_iz00_3005 = 0L;
												BgL_polyz00_3006 = BgL_polyz00_28;
												BgL_resz00_3007 = BgL_za7eroza7_1678;
											BgL_loopz00_3004:
												if ((BgL_iz00_3005 >= (long) CINT(BgL_lenz00_27)))
													{	/* Unsafe/crc.scm 227 */
														return BgL_resz00_3007;
													}
												else
													{	/* Unsafe/crc.scm 229 */
														long BgL_arg1558z00_3015;
														obj_t BgL_arg1559z00_3016;
														obj_t BgL_arg1561z00_3017;

														BgL_arg1558z00_3015 = (BgL_iz00_3005 + 1L);
														BgL_arg1559z00_3016 =
															BGL_PROCEDURE_CALL2(BgL_rshz00_1680,
															BgL_polyz00_3006, BINT(1L));
														{	/* Unsafe/crc.scm 231 */
															obj_t BgL_arg1562z00_3018;
															obj_t BgL_arg1564z00_3019;

															BgL_arg1562z00_3018 =
																BGL_PROCEDURE_CALL2(BgL_lshz00_1679,
																BgL_resz00_3007, BINT(1L));
															BgL_arg1564z00_3019 =
																BGL_PROCEDURE_CALL2(BgL_bzd2andzd2_1681,
																BgL_onez00_1677, BgL_polyz00_3006);
															BgL_arg1561z00_3017 =
																BGL_PROCEDURE_CALL2(BgL_bzd2orzd2_1682,
																BgL_arg1562z00_3018, BgL_arg1564z00_3019);
														}
														{
															obj_t BgL_resz00_6395;
															obj_t BgL_polyz00_6394;
															long BgL_iz00_6393;

															BgL_iz00_6393 = BgL_arg1558z00_3015;
															BgL_polyz00_6394 = BgL_arg1559z00_3016;
															BgL_resz00_6395 = BgL_arg1561z00_3017;
															BgL_resz00_3007 = BgL_resz00_6395;
															BgL_polyz00_3006 = BgL_polyz00_6394;
															BgL_iz00_3005 = BgL_iz00_6393;
															goto BgL_loopz00_3004;
														}
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &crc-polynomial-be->le */
	obj_t BGl_z62crczd2polynomialzd2bezd2ze3lez53zz__crcz00(obj_t BgL_envz00_5306,
		obj_t BgL_lenz00_5307, obj_t BgL_polyz00_5308)
	{
		{	/* Unsafe/crc.scm 202 */
			return
				BGl_crczd2polynomialzd2bezd2ze3lez31zz__crcz00(BgL_lenz00_5307,
				BgL_polyz00_5308);
		}

	}



/* register-crc! */
	BGL_EXPORTED_DEF obj_t BGl_registerzd2crcz12zc0zz__crcz00(obj_t
		BgL_namez00_29, obj_t BgL_polyz00_30, obj_t BgL_lenz00_31)
	{
		{	/* Unsafe/crc.scm 277 */
			{	/* Unsafe/crc.scm 279 */
				obj_t BgL_arg1587z00_3023;

				{	/* Unsafe/crc.scm 279 */
					obj_t BgL_arg1589z00_3024;

					BgL_arg1589z00_3024 =
						BGl_crczd2polynomialzd2bezd2ze3lez31zz__crcz00(BgL_polyz00_30,
						BgL_lenz00_31);
					{	/* Unsafe/crc.scm 279 */
						obj_t BgL_list1590z00_3025;

						{	/* Unsafe/crc.scm 279 */
							obj_t BgL_arg1591z00_3026;

							{	/* Unsafe/crc.scm 279 */
								obj_t BgL_arg1593z00_3027;

								{	/* Unsafe/crc.scm 279 */
									obj_t BgL_arg1594z00_3028;

									BgL_arg1594z00_3028 =
										MAKE_YOUNG_PAIR(BgL_arg1589z00_3024, BNIL);
									BgL_arg1593z00_3027 =
										MAKE_YOUNG_PAIR(BgL_polyz00_30, BgL_arg1594z00_3028);
								}
								BgL_arg1591z00_3026 =
									MAKE_YOUNG_PAIR(BgL_lenz00_31, BgL_arg1593z00_3027);
							}
							BgL_list1590z00_3025 =
								MAKE_YOUNG_PAIR(BgL_namez00_29, BgL_arg1591z00_3026);
						}
						BgL_arg1587z00_3023 = BgL_list1590z00_3025;
					}
				}
				return (BGl_za2crcsza2z00zz__crcz00 =
					MAKE_YOUNG_PAIR(BgL_arg1587z00_3023, BGl_za2crcsza2z00zz__crcz00),
					BUNSPEC);
			}
		}

	}



/* &register-crc! */
	obj_t BGl_z62registerzd2crcz12za2zz__crcz00(obj_t BgL_envz00_5345,
		obj_t BgL_namez00_5346, obj_t BgL_polyz00_5347, obj_t BgL_lenz00_5348)
	{
		{	/* Unsafe/crc.scm 277 */
			return
				BGl_registerzd2crcz12zc0zz__crcz00(BgL_namez00_5346, BgL_polyz00_5347,
				BgL_lenz00_5348);
		}

	}



/* crc-polynomial */
	BGL_EXPORTED_DEF obj_t BGl_crczd2polynomialzd2zz__crcz00(obj_t BgL_namez00_32)
	{
		{	/* Unsafe/crc.scm 284 */
			{	/* Unsafe/crc.scm 285 */
				obj_t BgL_tz00_3030;

				BgL_tz00_3030 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_namez00_32,
					BGl_za2crcsza2z00zz__crcz00);
				if (CBOOL(BgL_tz00_3030))
					{	/* Unsafe/crc.scm 286 */
						obj_t BgL_pairz00_3037;

						{	/* Unsafe/crc.scm 286 */
							obj_t BgL_pairz00_3036;

							BgL_pairz00_3036 = CDR(((obj_t) BgL_tz00_3030));
							BgL_pairz00_3037 = CDR(BgL_pairz00_3036);
						}
						return CAR(BgL_pairz00_3037);
					}
				else
					{	/* Unsafe/crc.scm 286 */
						return BFALSE;
					}
			}
		}

	}



/* &crc-polynomial */
	obj_t BGl_z62crczd2polynomialzb0zz__crcz00(obj_t BgL_envz00_5349,
		obj_t BgL_namez00_5350)
	{
		{	/* Unsafe/crc.scm 284 */
			return BGl_crczd2polynomialzd2zz__crcz00(BgL_namez00_5350);
		}

	}



/* crc-polynomial-le */
	BGL_EXPORTED_DEF obj_t BGl_crczd2polynomialzd2lez00zz__crcz00(obj_t
		BgL_namez00_33)
	{
		{	/* Unsafe/crc.scm 291 */
			{	/* Unsafe/crc.scm 292 */
				obj_t BgL_tz00_3038;

				BgL_tz00_3038 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_namez00_33,
					BGl_za2crcsza2z00zz__crcz00);
				if (CBOOL(BgL_tz00_3038))
					{	/* Unsafe/crc.scm 293 */
						obj_t BgL_pairz00_3047;

						{	/* Unsafe/crc.scm 293 */
							obj_t BgL_pairz00_3046;

							{	/* Unsafe/crc.scm 293 */
								obj_t BgL_pairz00_3045;

								BgL_pairz00_3045 = CDR(((obj_t) BgL_tz00_3038));
								BgL_pairz00_3046 = CDR(BgL_pairz00_3045);
							}
							BgL_pairz00_3047 = CDR(BgL_pairz00_3046);
						}
						return CAR(BgL_pairz00_3047);
					}
				else
					{	/* Unsafe/crc.scm 293 */
						return BFALSE;
					}
			}
		}

	}



/* &crc-polynomial-le */
	obj_t BGl_z62crczd2polynomialzd2lez62zz__crcz00(obj_t BgL_envz00_5351,
		obj_t BgL_namez00_5352)
	{
		{	/* Unsafe/crc.scm 291 */
			return BGl_crczd2polynomialzd2lez00zz__crcz00(BgL_namez00_5352);
		}

	}



/* crc-length */
	BGL_EXPORTED_DEF obj_t BGl_crczd2lengthzd2zz__crcz00(obj_t BgL_namez00_34)
	{
		{	/* Unsafe/crc.scm 298 */
			{	/* Unsafe/crc.scm 299 */
				obj_t BgL_tz00_3048;

				BgL_tz00_3048 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_namez00_34,
					BGl_za2crcsza2z00zz__crcz00);
				if (CBOOL(BgL_tz00_3048))
					{	/* Unsafe/crc.scm 300 */
						obj_t BgL_pairz00_3053;

						BgL_pairz00_3053 = CDR(((obj_t) BgL_tz00_3048));
						return CAR(BgL_pairz00_3053);
					}
				else
					{	/* Unsafe/crc.scm 300 */
						return BFALSE;
					}
			}
		}

	}



/* &crc-length */
	obj_t BGl_z62crczd2lengthzb0zz__crcz00(obj_t BgL_envz00_5353,
		obj_t BgL_namez00_5354)
	{
		{	/* Unsafe/crc.scm 298 */
			return BGl_crczd2lengthzd2zz__crcz00(BgL_namez00_5354);
		}

	}



/* crc-names */
	BGL_EXPORTED_DEF obj_t BGl_crczd2nameszd2zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 305 */
			{	/* Unsafe/crc.scm 306 */
				obj_t BgL_l1104z00_1734;

				BgL_l1104z00_1734 = BGl_za2crcsza2z00zz__crcz00;
				{	/* Unsafe/crc.scm 306 */
					obj_t BgL_head1106z00_1736;

					BgL_head1106z00_1736 =
						MAKE_YOUNG_PAIR(CAR(CAR(BgL_l1104z00_1734)), BNIL);
					{	/* Unsafe/crc.scm 306 */
						obj_t BgL_g1109z00_1737;

						BgL_g1109z00_1737 = CDR(BgL_l1104z00_1734);
						{
							obj_t BgL_l1104z00_3076;
							obj_t BgL_tail1107z00_3077;

							BgL_l1104z00_3076 = BgL_g1109z00_1737;
							BgL_tail1107z00_3077 = BgL_head1106z00_1736;
						BgL_zc3z04anonymousza31596ze3z87_3075:
							if (NULLP(BgL_l1104z00_3076))
								{	/* Unsafe/crc.scm 306 */
									return BgL_head1106z00_1736;
								}
							else
								{	/* Unsafe/crc.scm 306 */
									obj_t BgL_newtail1108z00_3084;

									{	/* Unsafe/crc.scm 306 */
										obj_t BgL_arg1601z00_3085;

										{	/* Unsafe/crc.scm 306 */
											obj_t BgL_pairz00_3089;

											BgL_pairz00_3089 = CAR(((obj_t) BgL_l1104z00_3076));
											BgL_arg1601z00_3085 = CAR(BgL_pairz00_3089);
										}
										BgL_newtail1108z00_3084 =
											MAKE_YOUNG_PAIR(BgL_arg1601z00_3085, BNIL);
									}
									SET_CDR(BgL_tail1107z00_3077, BgL_newtail1108z00_3084);
									{	/* Unsafe/crc.scm 306 */
										obj_t BgL_arg1598z00_3087;

										BgL_arg1598z00_3087 = CDR(((obj_t) BgL_l1104z00_3076));
										{
											obj_t BgL_tail1107z00_6442;
											obj_t BgL_l1104z00_6441;

											BgL_l1104z00_6441 = BgL_arg1598z00_3087;
											BgL_tail1107z00_6442 = BgL_newtail1108z00_3084;
											BgL_tail1107z00_3077 = BgL_tail1107z00_6442;
											BgL_l1104z00_3076 = BgL_l1104z00_6441;
											goto BgL_zc3z04anonymousza31596ze3z87_3075;
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &crc-names */
	obj_t BGl_z62crczd2nameszb0zz__crcz00(obj_t BgL_envz00_5355)
	{
		{	/* Unsafe/crc.scm 305 */
			return BGl_crczd2nameszd2zz__crcz00();
		}

	}



/* _crc */
	obj_t BGl__crcz00zz__crcz00(obj_t BgL_env1139z00_41, obj_t BgL_opt1138z00_40)
	{
		{	/* Unsafe/crc.scm 311 */
			{	/* Unsafe/crc.scm 311 */
				obj_t BgL_namez00_1754;
				obj_t BgL_objz00_1755;

				BgL_namez00_1754 = VECTOR_REF(BgL_opt1138z00_40, 0L);
				BgL_objz00_1755 = VECTOR_REF(BgL_opt1138z00_40, 1L);
				{	/* Unsafe/crc.scm 311 */
					obj_t BgL_bigzd2endianzf3z21_1756;

					BgL_bigzd2endianzf3z21_1756 = BTRUE;
					{	/* Unsafe/crc.scm 311 */
						obj_t BgL_finalzd2xorzd2_1757;

						BgL_finalzd2xorzd2_1757 = BINT(0L);
						{	/* Unsafe/crc.scm 311 */
							obj_t BgL_initz00_1758;

							BgL_initz00_1758 = BINT(0L);
							{	/* Unsafe/crc.scm 311 */

								{
									long BgL_iz00_1759;

									BgL_iz00_1759 = 2L;
								BgL_check1142z00_1760:
									if ((BgL_iz00_1759 == VECTOR_LENGTH(BgL_opt1138z00_40)))
										{	/* Unsafe/crc.scm 311 */
											BNIL;
										}
									else
										{	/* Unsafe/crc.scm 311 */
											bool_t BgL_test2541z00_6451;

											{	/* Unsafe/crc.scm 311 */
												obj_t BgL_arg1611z00_1766;

												BgL_arg1611z00_1766 =
													VECTOR_REF(BgL_opt1138z00_40, BgL_iz00_1759);
												BgL_test2541z00_6451 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1611z00_1766, BGl_list2348z00zz__crcz00));
											}
											if (BgL_test2541z00_6451)
												{
													long BgL_iz00_6455;

													BgL_iz00_6455 = (BgL_iz00_1759 + 2L);
													BgL_iz00_1759 = BgL_iz00_6455;
													goto BgL_check1142z00_1760;
												}
											else
												{	/* Unsafe/crc.scm 311 */
													obj_t BgL_arg1610z00_1765;

													BgL_arg1610z00_1765 =
														VECTOR_REF(BgL_opt1138z00_40, BgL_iz00_1759);
													BGl_errorz00zz__errorz00(BGl_symbol2355z00zz__crcz00,
														BGl_string2357z00zz__crcz00, BgL_arg1610z00_1765);
												}
										}
								}
								{	/* Unsafe/crc.scm 311 */
									obj_t BgL_index1144z00_1767;

									{
										long BgL_iz00_3108;

										BgL_iz00_3108 = 2L;
									BgL_search1141z00_3107:
										if ((BgL_iz00_3108 == VECTOR_LENGTH(BgL_opt1138z00_40)))
											{	/* Unsafe/crc.scm 311 */
												BgL_index1144z00_1767 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 311 */
												if (
													(BgL_iz00_3108 ==
														(VECTOR_LENGTH(BgL_opt1138z00_40) - 1L)))
													{	/* Unsafe/crc.scm 311 */
														BgL_index1144z00_1767 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2355z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1138z00_40)));
													}
												else
													{	/* Unsafe/crc.scm 311 */
														obj_t BgL_vz00_3118;

														BgL_vz00_3118 =
															VECTOR_REF(BgL_opt1138z00_40, BgL_iz00_3108);
														if ((BgL_vz00_3118 == BGl_keyword2349z00zz__crcz00))
															{	/* Unsafe/crc.scm 311 */
																BgL_index1144z00_1767 =
																	BINT((BgL_iz00_3108 + 1L));
															}
														else
															{
																long BgL_iz00_6475;

																BgL_iz00_6475 = (BgL_iz00_3108 + 2L);
																BgL_iz00_3108 = BgL_iz00_6475;
																goto BgL_search1141z00_3107;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 311 */
										bool_t BgL_test2545z00_6477;

										{	/* Unsafe/crc.scm 311 */
											long BgL_n1z00_3122;

											{	/* Unsafe/crc.scm 311 */
												obj_t BgL_tmpz00_6478;

												if (INTEGERP(BgL_index1144z00_1767))
													{	/* Unsafe/crc.scm 311 */
														BgL_tmpz00_6478 = BgL_index1144z00_1767;
													}
												else
													{
														obj_t BgL_auxz00_6481;

														BgL_auxz00_6481 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(12674L),
															BGl_string2359z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1144z00_1767);
														FAILURE(BgL_auxz00_6481, BFALSE, BFALSE);
													}
												BgL_n1z00_3122 = (long) CINT(BgL_tmpz00_6478);
											}
											BgL_test2545z00_6477 = (BgL_n1z00_3122 >= 0L);
										}
										if (BgL_test2545z00_6477)
											{
												long BgL_auxz00_6487;

												{	/* Unsafe/crc.scm 311 */
													obj_t BgL_tmpz00_6488;

													if (INTEGERP(BgL_index1144z00_1767))
														{	/* Unsafe/crc.scm 311 */
															BgL_tmpz00_6488 = BgL_index1144z00_1767;
														}
													else
														{
															obj_t BgL_auxz00_6491;

															BgL_auxz00_6491 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(12674L),
																BGl_string2359z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1144z00_1767);
															FAILURE(BgL_auxz00_6491, BFALSE, BFALSE);
														}
													BgL_auxz00_6487 = (long) CINT(BgL_tmpz00_6488);
												}
												BgL_bigzd2endianzf3z21_1756 =
													VECTOR_REF(BgL_opt1138z00_40, BgL_auxz00_6487);
											}
										else
											{	/* Unsafe/crc.scm 311 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 311 */
									obj_t BgL_index1145z00_1769;

									{
										long BgL_iz00_3124;

										BgL_iz00_3124 = 2L;
									BgL_search1141z00_3123:
										if ((BgL_iz00_3124 == VECTOR_LENGTH(BgL_opt1138z00_40)))
											{	/* Unsafe/crc.scm 311 */
												BgL_index1145z00_1769 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 311 */
												if (
													(BgL_iz00_3124 ==
														(VECTOR_LENGTH(BgL_opt1138z00_40) - 1L)))
													{	/* Unsafe/crc.scm 311 */
														BgL_index1145z00_1769 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2355z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1138z00_40)));
													}
												else
													{	/* Unsafe/crc.scm 311 */
														obj_t BgL_vz00_3134;

														BgL_vz00_3134 =
															VECTOR_REF(BgL_opt1138z00_40, BgL_iz00_3124);
														if ((BgL_vz00_3134 == BGl_keyword2351z00zz__crcz00))
															{	/* Unsafe/crc.scm 311 */
																BgL_index1145z00_1769 =
																	BINT((BgL_iz00_3124 + 1L));
															}
														else
															{
																long BgL_iz00_6513;

																BgL_iz00_6513 = (BgL_iz00_3124 + 2L);
																BgL_iz00_3124 = BgL_iz00_6513;
																goto BgL_search1141z00_3123;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 311 */
										bool_t BgL_test2551z00_6515;

										{	/* Unsafe/crc.scm 311 */
											long BgL_n1z00_3138;

											{	/* Unsafe/crc.scm 311 */
												obj_t BgL_tmpz00_6516;

												if (INTEGERP(BgL_index1145z00_1769))
													{	/* Unsafe/crc.scm 311 */
														BgL_tmpz00_6516 = BgL_index1145z00_1769;
													}
												else
													{
														obj_t BgL_auxz00_6519;

														BgL_auxz00_6519 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(12674L),
															BGl_string2359z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1145z00_1769);
														FAILURE(BgL_auxz00_6519, BFALSE, BFALSE);
													}
												BgL_n1z00_3138 = (long) CINT(BgL_tmpz00_6516);
											}
											BgL_test2551z00_6515 = (BgL_n1z00_3138 >= 0L);
										}
										if (BgL_test2551z00_6515)
											{
												long BgL_auxz00_6525;

												{	/* Unsafe/crc.scm 311 */
													obj_t BgL_tmpz00_6526;

													if (INTEGERP(BgL_index1145z00_1769))
														{	/* Unsafe/crc.scm 311 */
															BgL_tmpz00_6526 = BgL_index1145z00_1769;
														}
													else
														{
															obj_t BgL_auxz00_6529;

															BgL_auxz00_6529 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(12674L),
																BGl_string2359z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1145z00_1769);
															FAILURE(BgL_auxz00_6529, BFALSE, BFALSE);
														}
													BgL_auxz00_6525 = (long) CINT(BgL_tmpz00_6526);
												}
												BgL_finalzd2xorzd2_1757 =
													VECTOR_REF(BgL_opt1138z00_40, BgL_auxz00_6525);
											}
										else
											{	/* Unsafe/crc.scm 311 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 311 */
									obj_t BgL_index1146z00_1771;

									{
										long BgL_iz00_3140;

										BgL_iz00_3140 = 2L;
									BgL_search1141z00_3139:
										if ((BgL_iz00_3140 == VECTOR_LENGTH(BgL_opt1138z00_40)))
											{	/* Unsafe/crc.scm 311 */
												BgL_index1146z00_1771 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 311 */
												if (
													(BgL_iz00_3140 ==
														(VECTOR_LENGTH(BgL_opt1138z00_40) - 1L)))
													{	/* Unsafe/crc.scm 311 */
														BgL_index1146z00_1771 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2355z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1138z00_40)));
													}
												else
													{	/* Unsafe/crc.scm 311 */
														obj_t BgL_vz00_3150;

														BgL_vz00_3150 =
															VECTOR_REF(BgL_opt1138z00_40, BgL_iz00_3140);
														if ((BgL_vz00_3150 == BGl_keyword2353z00zz__crcz00))
															{	/* Unsafe/crc.scm 311 */
																BgL_index1146z00_1771 =
																	BINT((BgL_iz00_3140 + 1L));
															}
														else
															{
																long BgL_iz00_6551;

																BgL_iz00_6551 = (BgL_iz00_3140 + 2L);
																BgL_iz00_3140 = BgL_iz00_6551;
																goto BgL_search1141z00_3139;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 311 */
										bool_t BgL_test2557z00_6553;

										{	/* Unsafe/crc.scm 311 */
											long BgL_n1z00_3154;

											{	/* Unsafe/crc.scm 311 */
												obj_t BgL_tmpz00_6554;

												if (INTEGERP(BgL_index1146z00_1771))
													{	/* Unsafe/crc.scm 311 */
														BgL_tmpz00_6554 = BgL_index1146z00_1771;
													}
												else
													{
														obj_t BgL_auxz00_6557;

														BgL_auxz00_6557 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(12674L),
															BGl_string2359z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1146z00_1771);
														FAILURE(BgL_auxz00_6557, BFALSE, BFALSE);
													}
												BgL_n1z00_3154 = (long) CINT(BgL_tmpz00_6554);
											}
											BgL_test2557z00_6553 = (BgL_n1z00_3154 >= 0L);
										}
										if (BgL_test2557z00_6553)
											{
												long BgL_auxz00_6563;

												{	/* Unsafe/crc.scm 311 */
													obj_t BgL_tmpz00_6564;

													if (INTEGERP(BgL_index1146z00_1771))
														{	/* Unsafe/crc.scm 311 */
															BgL_tmpz00_6564 = BgL_index1146z00_1771;
														}
													else
														{
															obj_t BgL_auxz00_6567;

															BgL_auxz00_6567 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(12674L),
																BGl_string2359z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1146z00_1771);
															FAILURE(BgL_auxz00_6567, BFALSE, BFALSE);
														}
													BgL_auxz00_6563 = (long) CINT(BgL_tmpz00_6564);
												}
												BgL_initz00_1758 =
													VECTOR_REF(BgL_opt1138z00_40, BgL_auxz00_6563);
											}
										else
											{	/* Unsafe/crc.scm 311 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 311 */
									obj_t BgL_arg1615z00_1773;
									obj_t BgL_arg1616z00_1774;

									BgL_arg1615z00_1773 = VECTOR_REF(BgL_opt1138z00_40, 0L);
									BgL_arg1616z00_1774 = VECTOR_REF(BgL_opt1138z00_40, 1L);
									{	/* Unsafe/crc.scm 311 */
										obj_t BgL_bigzd2endianzf3z21_1775;

										BgL_bigzd2endianzf3z21_1775 = BgL_bigzd2endianzf3z21_1756;
										{	/* Unsafe/crc.scm 311 */
											obj_t BgL_finalzd2xorzd2_1776;

											BgL_finalzd2xorzd2_1776 = BgL_finalzd2xorzd2_1757;
											{	/* Unsafe/crc.scm 311 */
												obj_t BgL_initz00_1777;

												BgL_initz00_1777 = BgL_initz00_1758;
												if (STRINGP(BgL_arg1616z00_1774))
													{	/* Unsafe/crc.scm 314 */
														obj_t BgL_arg1624z00_3156;

														{	/* Unsafe/crc.scm 314 */
															long BgL_endz00_3159;

															BgL_endz00_3159 =
																STRING_LENGTH(BgL_arg1616z00_1774);
															{	/* Unsafe/crc.scm 314 */

																BgL_arg1624z00_3156 =
																	BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
																	(BgL_arg1616z00_1774, BINT(0L),
																	BINT(BgL_endz00_3159));
														}}
														return
															BGl_crczd2fastzd2portz00zz__crcz00
															(BgL_arg1615z00_1773, BgL_arg1624z00_3156,
															BgL_initz00_1777, BgL_finalzd2xorzd2_1776,
															BgL_bigzd2endianzf3z21_1775);
													}
												else
													{	/* Unsafe/crc.scm 313 */
														if (INPUT_PORTP(BgL_arg1616z00_1774))
															{	/* Unsafe/crc.scm 315 */
																return
																	BGl_crczd2fastzd2portz00zz__crcz00
																	(BgL_arg1615z00_1773, BgL_arg1616z00_1774,
																	BgL_initz00_1777, BgL_finalzd2xorzd2_1776,
																	BgL_bigzd2endianzf3z21_1775);
															}
														else
															{	/* Unsafe/crc.scm 315 */
																if (BGL_MMAPP(BgL_arg1616z00_1774))
																	{	/* Unsafe/crc.scm 318 */
																		obj_t BgL_auxz00_6587;

																		if (BGL_MMAPP(BgL_arg1616z00_1774))
																			{	/* Unsafe/crc.scm 318 */
																				BgL_auxz00_6587 = BgL_arg1616z00_1774;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6590;

																				BgL_auxz00_6590 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2324z00zz__crcz00,
																					BINT(12984L),
																					BGl_string2359z00zz__crcz00,
																					BGl_string2360z00zz__crcz00,
																					BgL_arg1616z00_1774);
																				FAILURE(BgL_auxz00_6590, BFALSE,
																					BFALSE);
																			}
																		return
																			BGl_crczd2fastzd2mmapz00zz__crcz00
																			(BgL_arg1615z00_1773, BgL_auxz00_6587,
																			BgL_initz00_1777, BgL_finalzd2xorzd2_1776,
																			BgL_bigzd2endianzf3z21_1775);
																	}
																else
																	{	/* Unsafe/crc.scm 317 */
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_symbol2355z00zz__crcz00,
																			BGl_string2361z00zz__crcz00,
																			BgL_arg1616z00_1774);
																	}
															}
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* crc */
	BGL_EXPORTED_DEF obj_t BGl_crcz00zz__crcz00(obj_t BgL_namez00_35,
		obj_t BgL_objz00_36, obj_t BgL_bigzd2endianzf3z21_37,
		obj_t BgL_finalzd2xorzd2_38, obj_t BgL_initz00_39)
	{
		{	/* Unsafe/crc.scm 311 */
			if (STRINGP(BgL_objz00_36))
				{	/* Unsafe/crc.scm 314 */
					obj_t BgL_arg1624z00_3164;

					{	/* Unsafe/crc.scm 314 */
						long BgL_endz00_3167;

						BgL_endz00_3167 = STRING_LENGTH(BgL_objz00_36);
						{	/* Unsafe/crc.scm 314 */

							BgL_arg1624z00_3164 =
								BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
								(BgL_objz00_36, BINT(0L), BINT(BgL_endz00_3167));
					}}
					return
						BGl_crczd2fastzd2portz00zz__crcz00(BgL_namez00_35,
						BgL_arg1624z00_3164, BgL_initz00_39, BgL_finalzd2xorzd2_38,
						BgL_bigzd2endianzf3z21_37);
				}
			else
				{	/* Unsafe/crc.scm 313 */
					if (INPUT_PORTP(BgL_objz00_36))
						{	/* Unsafe/crc.scm 315 */
							return
								BGl_crczd2fastzd2portz00zz__crcz00(BgL_namez00_35,
								BgL_objz00_36, BgL_initz00_39, BgL_finalzd2xorzd2_38,
								BgL_bigzd2endianzf3z21_37);
						}
					else
						{	/* Unsafe/crc.scm 315 */
							if (BGL_MMAPP(BgL_objz00_36))
								{	/* Unsafe/crc.scm 317 */
									return
										BGl_crczd2fastzd2mmapz00zz__crcz00(BgL_namez00_35,
										BgL_objz00_36, BgL_initz00_39, BgL_finalzd2xorzd2_38,
										BgL_bigzd2endianzf3z21_37);
								}
							else
								{	/* Unsafe/crc.scm 317 */
									return
										BGl_errorz00zz__errorz00(BGl_symbol2355z00zz__crcz00,
										BGl_string2361z00zz__crcz00, BgL_objz00_36);
								}
						}
				}
		}

	}



/* _crc-file */
	obj_t BGl__crczd2filezd2zz__crcz00(obj_t BgL_env1148z00_48,
		obj_t BgL_opt1147z00_47)
	{
		{	/* Unsafe/crc.scm 325 */
			{	/* Unsafe/crc.scm 325 */
				obj_t BgL_namez00_1796;
				obj_t BgL_fz00_1797;

				BgL_namez00_1796 = VECTOR_REF(BgL_opt1147z00_47, 0L);
				BgL_fz00_1797 = VECTOR_REF(BgL_opt1147z00_47, 1L);
				{	/* Unsafe/crc.scm 325 */
					obj_t BgL_bigzd2endianzf3z21_1798;

					BgL_bigzd2endianzf3z21_1798 = BTRUE;
					{	/* Unsafe/crc.scm 325 */
						obj_t BgL_finalzd2xorzd2_1799;

						BgL_finalzd2xorzd2_1799 = BINT(0L);
						{	/* Unsafe/crc.scm 325 */
							obj_t BgL_initz00_1800;

							BgL_initz00_1800 = BINT(0L);
							{	/* Unsafe/crc.scm 325 */

								{
									long BgL_iz00_1801;

									BgL_iz00_1801 = 2L;
								BgL_check1151z00_1802:
									if ((BgL_iz00_1801 == VECTOR_LENGTH(BgL_opt1147z00_47)))
										{	/* Unsafe/crc.scm 325 */
											BNIL;
										}
									else
										{	/* Unsafe/crc.scm 325 */
											bool_t BgL_test2568z00_6617;

											{	/* Unsafe/crc.scm 325 */
												obj_t BgL_arg1634z00_1808;

												BgL_arg1634z00_1808 =
													VECTOR_REF(BgL_opt1147z00_47, BgL_iz00_1801);
												BgL_test2568z00_6617 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1634z00_1808, BGl_list2348z00zz__crcz00));
											}
											if (BgL_test2568z00_6617)
												{
													long BgL_iz00_6621;

													BgL_iz00_6621 = (BgL_iz00_1801 + 2L);
													BgL_iz00_1801 = BgL_iz00_6621;
													goto BgL_check1151z00_1802;
												}
											else
												{	/* Unsafe/crc.scm 325 */
													obj_t BgL_arg1631z00_1807;

													BgL_arg1631z00_1807 =
														VECTOR_REF(BgL_opt1147z00_47, BgL_iz00_1801);
													BGl_errorz00zz__errorz00(BGl_symbol2362z00zz__crcz00,
														BGl_string2357z00zz__crcz00, BgL_arg1631z00_1807);
												}
										}
								}
								{	/* Unsafe/crc.scm 325 */
									obj_t BgL_index1153z00_1809;

									{
										long BgL_iz00_3187;

										BgL_iz00_3187 = 2L;
									BgL_search1150z00_3186:
										if ((BgL_iz00_3187 == VECTOR_LENGTH(BgL_opt1147z00_47)))
											{	/* Unsafe/crc.scm 325 */
												BgL_index1153z00_1809 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 325 */
												if (
													(BgL_iz00_3187 ==
														(VECTOR_LENGTH(BgL_opt1147z00_47) - 1L)))
													{	/* Unsafe/crc.scm 325 */
														BgL_index1153z00_1809 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2362z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1147z00_47)));
													}
												else
													{	/* Unsafe/crc.scm 325 */
														obj_t BgL_vz00_3197;

														BgL_vz00_3197 =
															VECTOR_REF(BgL_opt1147z00_47, BgL_iz00_3187);
														if ((BgL_vz00_3197 == BGl_keyword2349z00zz__crcz00))
															{	/* Unsafe/crc.scm 325 */
																BgL_index1153z00_1809 =
																	BINT((BgL_iz00_3187 + 1L));
															}
														else
															{
																long BgL_iz00_6641;

																BgL_iz00_6641 = (BgL_iz00_3187 + 2L);
																BgL_iz00_3187 = BgL_iz00_6641;
																goto BgL_search1150z00_3186;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 325 */
										bool_t BgL_test2572z00_6643;

										{	/* Unsafe/crc.scm 325 */
											long BgL_n1z00_3201;

											{	/* Unsafe/crc.scm 325 */
												obj_t BgL_tmpz00_6644;

												if (INTEGERP(BgL_index1153z00_1809))
													{	/* Unsafe/crc.scm 325 */
														BgL_tmpz00_6644 = BgL_index1153z00_1809;
													}
												else
													{
														obj_t BgL_auxz00_6647;

														BgL_auxz00_6647 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(13298L),
															BGl_string2364z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1153z00_1809);
														FAILURE(BgL_auxz00_6647, BFALSE, BFALSE);
													}
												BgL_n1z00_3201 = (long) CINT(BgL_tmpz00_6644);
											}
											BgL_test2572z00_6643 = (BgL_n1z00_3201 >= 0L);
										}
										if (BgL_test2572z00_6643)
											{
												long BgL_auxz00_6653;

												{	/* Unsafe/crc.scm 325 */
													obj_t BgL_tmpz00_6654;

													if (INTEGERP(BgL_index1153z00_1809))
														{	/* Unsafe/crc.scm 325 */
															BgL_tmpz00_6654 = BgL_index1153z00_1809;
														}
													else
														{
															obj_t BgL_auxz00_6657;

															BgL_auxz00_6657 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13298L),
																BGl_string2364z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1153z00_1809);
															FAILURE(BgL_auxz00_6657, BFALSE, BFALSE);
														}
													BgL_auxz00_6653 = (long) CINT(BgL_tmpz00_6654);
												}
												BgL_bigzd2endianzf3z21_1798 =
													VECTOR_REF(BgL_opt1147z00_47, BgL_auxz00_6653);
											}
										else
											{	/* Unsafe/crc.scm 325 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 325 */
									obj_t BgL_index1154z00_1811;

									{
										long BgL_iz00_3203;

										BgL_iz00_3203 = 2L;
									BgL_search1150z00_3202:
										if ((BgL_iz00_3203 == VECTOR_LENGTH(BgL_opt1147z00_47)))
											{	/* Unsafe/crc.scm 325 */
												BgL_index1154z00_1811 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 325 */
												if (
													(BgL_iz00_3203 ==
														(VECTOR_LENGTH(BgL_opt1147z00_47) - 1L)))
													{	/* Unsafe/crc.scm 325 */
														BgL_index1154z00_1811 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2362z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1147z00_47)));
													}
												else
													{	/* Unsafe/crc.scm 325 */
														obj_t BgL_vz00_3213;

														BgL_vz00_3213 =
															VECTOR_REF(BgL_opt1147z00_47, BgL_iz00_3203);
														if ((BgL_vz00_3213 == BGl_keyword2351z00zz__crcz00))
															{	/* Unsafe/crc.scm 325 */
																BgL_index1154z00_1811 =
																	BINT((BgL_iz00_3203 + 1L));
															}
														else
															{
																long BgL_iz00_6679;

																BgL_iz00_6679 = (BgL_iz00_3203 + 2L);
																BgL_iz00_3203 = BgL_iz00_6679;
																goto BgL_search1150z00_3202;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 325 */
										bool_t BgL_test2578z00_6681;

										{	/* Unsafe/crc.scm 325 */
											long BgL_n1z00_3217;

											{	/* Unsafe/crc.scm 325 */
												obj_t BgL_tmpz00_6682;

												if (INTEGERP(BgL_index1154z00_1811))
													{	/* Unsafe/crc.scm 325 */
														BgL_tmpz00_6682 = BgL_index1154z00_1811;
													}
												else
													{
														obj_t BgL_auxz00_6685;

														BgL_auxz00_6685 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(13298L),
															BGl_string2364z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1154z00_1811);
														FAILURE(BgL_auxz00_6685, BFALSE, BFALSE);
													}
												BgL_n1z00_3217 = (long) CINT(BgL_tmpz00_6682);
											}
											BgL_test2578z00_6681 = (BgL_n1z00_3217 >= 0L);
										}
										if (BgL_test2578z00_6681)
											{
												long BgL_auxz00_6691;

												{	/* Unsafe/crc.scm 325 */
													obj_t BgL_tmpz00_6692;

													if (INTEGERP(BgL_index1154z00_1811))
														{	/* Unsafe/crc.scm 325 */
															BgL_tmpz00_6692 = BgL_index1154z00_1811;
														}
													else
														{
															obj_t BgL_auxz00_6695;

															BgL_auxz00_6695 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13298L),
																BGl_string2364z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1154z00_1811);
															FAILURE(BgL_auxz00_6695, BFALSE, BFALSE);
														}
													BgL_auxz00_6691 = (long) CINT(BgL_tmpz00_6692);
												}
												BgL_finalzd2xorzd2_1799 =
													VECTOR_REF(BgL_opt1147z00_47, BgL_auxz00_6691);
											}
										else
											{	/* Unsafe/crc.scm 325 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 325 */
									obj_t BgL_index1155z00_1813;

									{
										long BgL_iz00_3219;

										BgL_iz00_3219 = 2L;
									BgL_search1150z00_3218:
										if ((BgL_iz00_3219 == VECTOR_LENGTH(BgL_opt1147z00_47)))
											{	/* Unsafe/crc.scm 325 */
												BgL_index1155z00_1813 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 325 */
												if (
													(BgL_iz00_3219 ==
														(VECTOR_LENGTH(BgL_opt1147z00_47) - 1L)))
													{	/* Unsafe/crc.scm 325 */
														BgL_index1155z00_1813 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2362z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1147z00_47)));
													}
												else
													{	/* Unsafe/crc.scm 325 */
														obj_t BgL_vz00_3229;

														BgL_vz00_3229 =
															VECTOR_REF(BgL_opt1147z00_47, BgL_iz00_3219);
														if ((BgL_vz00_3229 == BGl_keyword2353z00zz__crcz00))
															{	/* Unsafe/crc.scm 325 */
																BgL_index1155z00_1813 =
																	BINT((BgL_iz00_3219 + 1L));
															}
														else
															{
																long BgL_iz00_6717;

																BgL_iz00_6717 = (BgL_iz00_3219 + 2L);
																BgL_iz00_3219 = BgL_iz00_6717;
																goto BgL_search1150z00_3218;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 325 */
										bool_t BgL_test2584z00_6719;

										{	/* Unsafe/crc.scm 325 */
											long BgL_n1z00_3233;

											{	/* Unsafe/crc.scm 325 */
												obj_t BgL_tmpz00_6720;

												if (INTEGERP(BgL_index1155z00_1813))
													{	/* Unsafe/crc.scm 325 */
														BgL_tmpz00_6720 = BgL_index1155z00_1813;
													}
												else
													{
														obj_t BgL_auxz00_6723;

														BgL_auxz00_6723 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(13298L),
															BGl_string2364z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1155z00_1813);
														FAILURE(BgL_auxz00_6723, BFALSE, BFALSE);
													}
												BgL_n1z00_3233 = (long) CINT(BgL_tmpz00_6720);
											}
											BgL_test2584z00_6719 = (BgL_n1z00_3233 >= 0L);
										}
										if (BgL_test2584z00_6719)
											{
												long BgL_auxz00_6729;

												{	/* Unsafe/crc.scm 325 */
													obj_t BgL_tmpz00_6730;

													if (INTEGERP(BgL_index1155z00_1813))
														{	/* Unsafe/crc.scm 325 */
															BgL_tmpz00_6730 = BgL_index1155z00_1813;
														}
													else
														{
															obj_t BgL_auxz00_6733;

															BgL_auxz00_6733 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13298L),
																BGl_string2364z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1155z00_1813);
															FAILURE(BgL_auxz00_6733, BFALSE, BFALSE);
														}
													BgL_auxz00_6729 = (long) CINT(BgL_tmpz00_6730);
												}
												BgL_initz00_1800 =
													VECTOR_REF(BgL_opt1147z00_47, BgL_auxz00_6729);
											}
										else
											{	/* Unsafe/crc.scm 325 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 325 */
									obj_t BgL_arg1638z00_1815;
									obj_t BgL_arg1639z00_1816;

									BgL_arg1638z00_1815 = VECTOR_REF(BgL_opt1147z00_47, 0L);
									BgL_arg1639z00_1816 = VECTOR_REF(BgL_opt1147z00_47, 1L);
									{	/* Unsafe/crc.scm 325 */
										obj_t BgL_bigzd2endianzf3z21_1817;

										BgL_bigzd2endianzf3z21_1817 = BgL_bigzd2endianzf3z21_1798;
										{	/* Unsafe/crc.scm 325 */
											obj_t BgL_finalzd2xorzd2_1818;

											BgL_finalzd2xorzd2_1818 = BgL_finalzd2xorzd2_1799;
											{	/* Unsafe/crc.scm 325 */
												obj_t BgL_initz00_1819;

												BgL_initz00_1819 = BgL_initz00_1800;
												{	/* Unsafe/crc.scm 325 */
													obj_t BgL_fz00_3234;

													if (STRINGP(BgL_arg1639z00_1816))
														{	/* Unsafe/crc.scm 325 */
															BgL_fz00_3234 = BgL_arg1639z00_1816;
														}
													else
														{
															obj_t BgL_auxz00_6743;

															BgL_auxz00_6743 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13298L),
																BGl_string2364z00zz__crcz00,
																BGl_string2365z00zz__crcz00,
																BgL_arg1639z00_1816);
															FAILURE(BgL_auxz00_6743, BFALSE, BFALSE);
														}
													{	/* Unsafe/crc.scm 326 */
														obj_t BgL_pz00_3235;

														{	/* Unsafe/crc.scm 326 */

															BgL_pz00_3235 =
																BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
																(BgL_fz00_3234, BTRUE, BINT(5000000L));
														}
														if (CBOOL(BgL_pz00_3235))
															{	/* Unsafe/crc.scm 327 */
																BFALSE;
															}
														else
															{	/* Unsafe/crc.scm 327 */
																BGl_errorz00zz__errorz00
																	(BGl_symbol2362z00zz__crcz00,
																	BGl_string2366z00zz__crcz00, BgL_fz00_3234);
															}
														{	/* Unsafe/crc.scm 328 */
															obj_t BgL_exitd1050z00_3239;

															BgL_exitd1050z00_3239 = BGL_EXITD_TOP_AS_OBJ();
															{	/* Unsafe/crc.scm 330 */
																obj_t BgL_zc3z04anonymousza31646ze3z87_5356;

																BgL_zc3z04anonymousza31646ze3z87_5356 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza31646ze3ze5zz__crcz00,
																	(int) (0L), (int) (1L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31646ze3z87_5356,
																	(int) (0L), BgL_pz00_3235);
																{	/* Unsafe/crc.scm 328 */
																	obj_t BgL_arg2148z00_3243;

																	{	/* Unsafe/crc.scm 328 */
																		obj_t BgL_arg2149z00_3244;

																		BgL_arg2149z00_3244 =
																			BGL_EXITD_PROTECT(BgL_exitd1050z00_3239);
																		BgL_arg2148z00_3243 =
																			MAKE_YOUNG_PAIR
																			(BgL_zc3z04anonymousza31646ze3z87_5356,
																			BgL_arg2149z00_3244);
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_3239,
																		BgL_arg2148z00_3243);
																	BUNSPEC;
																}
																{	/* Unsafe/crc.scm 329 */
																	obj_t BgL_tmp1052z00_3241;

																	{	/* Unsafe/crc.scm 329 */
																		obj_t BgL_auxz00_6761;

																		if (INPUT_PORTP(BgL_pz00_3235))
																			{	/* Unsafe/crc.scm 329 */
																				BgL_auxz00_6761 = BgL_pz00_3235;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6764;

																				BgL_auxz00_6764 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2324z00zz__crcz00,
																					BINT(13516L),
																					BGl_string2364z00zz__crcz00,
																					BGl_string2367z00zz__crcz00,
																					BgL_pz00_3235);
																				FAILURE(BgL_auxz00_6764, BFALSE,
																					BFALSE);
																			}
																		BgL_tmp1052z00_3241 =
																			BGl_crczd2fastzd2portz00zz__crcz00
																			(BgL_arg1638z00_1815, BgL_auxz00_6761,
																			BgL_initz00_1819, BgL_finalzd2xorzd2_1818,
																			BgL_bigzd2endianzf3z21_1817);
																	}
																	{	/* Unsafe/crc.scm 328 */
																		bool_t BgL_test2590z00_6769;

																		{	/* Unsafe/crc.scm 328 */
																			obj_t BgL_arg2147z00_3246;

																			BgL_arg2147z00_3246 =
																				BGL_EXITD_PROTECT
																				(BgL_exitd1050z00_3239);
																			BgL_test2590z00_6769 =
																				PAIRP(BgL_arg2147z00_3246);
																		}
																		if (BgL_test2590z00_6769)
																			{	/* Unsafe/crc.scm 328 */
																				obj_t BgL_arg2145z00_3247;

																				{	/* Unsafe/crc.scm 328 */
																					obj_t BgL_arg2146z00_3248;

																					BgL_arg2146z00_3248 =
																						BGL_EXITD_PROTECT
																						(BgL_exitd1050z00_3239);
																					{	/* Unsafe/crc.scm 328 */
																						obj_t BgL_pairz00_3249;

																						if (PAIRP(BgL_arg2146z00_3248))
																							{	/* Unsafe/crc.scm 328 */
																								BgL_pairz00_3249 =
																									BgL_arg2146z00_3248;
																							}
																						else
																							{
																								obj_t BgL_auxz00_6775;

																								BgL_auxz00_6775 =
																									BGl_typezd2errorzd2zz__errorz00
																									(BGl_string2324z00zz__crcz00,
																									BINT(13478L),
																									BGl_string2364z00zz__crcz00,
																									BGl_string2368z00zz__crcz00,
																									BgL_arg2146z00_3248);
																								FAILURE(BgL_auxz00_6775, BFALSE,
																									BFALSE);
																							}
																						BgL_arg2145z00_3247 =
																							CDR(BgL_pairz00_3249);
																					}
																				}
																				BGL_EXITD_PROTECT_SET
																					(BgL_exitd1050z00_3239,
																					BgL_arg2145z00_3247);
																				BUNSPEC;
																			}
																		else
																			{	/* Unsafe/crc.scm 328 */
																				BFALSE;
																			}
																	}
																	{	/* Unsafe/crc.scm 330 */
																		obj_t BgL_portz00_3250;

																		if (INPUT_PORTP(BgL_pz00_3235))
																			{	/* Unsafe/crc.scm 330 */
																				BgL_portz00_3250 = BgL_pz00_3235;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6783;

																				BgL_auxz00_6783 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2324z00zz__crcz00,
																					BINT(13566L),
																					BGl_string2364z00zz__crcz00,
																					BGl_string2367z00zz__crcz00,
																					BgL_pz00_3235);
																				FAILURE(BgL_auxz00_6783, BFALSE,
																					BFALSE);
																			}
																		bgl_close_input_port(BgL_portz00_3250);
																	}
																	return BgL_tmp1052z00_3241;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1646> */
	obj_t BGl_z62zc3z04anonymousza31646ze3ze5zz__crcz00(obj_t BgL_envz00_5357)
	{
		{	/* Unsafe/crc.scm 328 */
			{	/* Unsafe/crc.scm 330 */
				obj_t BgL_pz00_5358;

				BgL_pz00_5358 = PROCEDURE_REF(BgL_envz00_5357, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_5358));
			}
		}

	}



/* crc-file */
	BGL_EXPORTED_DEF obj_t BGl_crczd2filezd2zz__crcz00(obj_t BgL_namez00_42,
		obj_t BgL_fz00_43, obj_t BgL_bigzd2endianzf3z21_44,
		obj_t BgL_finalzd2xorzd2_45, obj_t BgL_initz00_46)
	{
		{	/* Unsafe/crc.scm 325 */
			{	/* Unsafe/crc.scm 326 */
				obj_t BgL_pz00_3251;

				{	/* Unsafe/crc.scm 326 */

					BgL_pz00_3251 =
						BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_fz00_43, BTRUE,
						BINT(5000000L));
				}
				if (CBOOL(BgL_pz00_3251))
					{	/* Unsafe/crc.scm 327 */
						BFALSE;
					}
				else
					{	/* Unsafe/crc.scm 327 */
						BGl_errorz00zz__errorz00(BGl_symbol2362z00zz__crcz00,
							BGl_string2366z00zz__crcz00, BgL_fz00_43);
					}
				{	/* Unsafe/crc.scm 328 */
					obj_t BgL_exitd1050z00_3255;

					BgL_exitd1050z00_3255 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Unsafe/crc.scm 330 */
						obj_t BgL_zc3z04anonymousza31646ze3z87_5359;

						BgL_zc3z04anonymousza31646ze3z87_5359 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31646ze32187ze5zz__crcz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31646ze3z87_5359, (int) (0L),
							BgL_pz00_3251);
						{	/* Unsafe/crc.scm 328 */
							obj_t BgL_arg2148z00_3259;

							{	/* Unsafe/crc.scm 328 */
								obj_t BgL_arg2149z00_3260;

								BgL_arg2149z00_3260 = BGL_EXITD_PROTECT(BgL_exitd1050z00_3255);
								BgL_arg2148z00_3259 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31646ze3z87_5359,
									BgL_arg2149z00_3260);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_3255, BgL_arg2148z00_3259);
							BUNSPEC;
						}
						{	/* Unsafe/crc.scm 329 */
							obj_t BgL_tmp1052z00_3257;

							BgL_tmp1052z00_3257 =
								BGl_crczd2fastzd2portz00zz__crcz00(BgL_namez00_42,
								BgL_pz00_3251, BgL_initz00_46, BgL_finalzd2xorzd2_45,
								BgL_bigzd2endianzf3z21_44);
							{	/* Unsafe/crc.scm 328 */
								bool_t BgL_test2594z00_6807;

								{	/* Unsafe/crc.scm 328 */
									obj_t BgL_arg2147z00_3262;

									BgL_arg2147z00_3262 =
										BGL_EXITD_PROTECT(BgL_exitd1050z00_3255);
									BgL_test2594z00_6807 = PAIRP(BgL_arg2147z00_3262);
								}
								if (BgL_test2594z00_6807)
									{	/* Unsafe/crc.scm 328 */
										obj_t BgL_arg2145z00_3263;

										{	/* Unsafe/crc.scm 328 */
											obj_t BgL_arg2146z00_3264;

											BgL_arg2146z00_3264 =
												BGL_EXITD_PROTECT(BgL_exitd1050z00_3255);
											BgL_arg2145z00_3263 = CDR(((obj_t) BgL_arg2146z00_3264));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1050z00_3255,
											BgL_arg2145z00_3263);
										BUNSPEC;
									}
								else
									{	/* Unsafe/crc.scm 328 */
										BFALSE;
									}
							}
							bgl_close_input_port(((obj_t) BgL_pz00_3251));
							return BgL_tmp1052z00_3257;
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1646>2187 */
	obj_t BGl_z62zc3z04anonymousza31646ze32187ze5zz__crcz00(obj_t BgL_envz00_5360)
	{
		{	/* Unsafe/crc.scm 328 */
			{	/* Unsafe/crc.scm 330 */
				obj_t BgL_pz00_5361;

				BgL_pz00_5361 = PROCEDURE_REF(BgL_envz00_5360, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_5361));
			}
		}

	}



/* _crc-string */
	obj_t BGl__crczd2stringzd2zz__crcz00(obj_t BgL_env1157z00_55,
		obj_t BgL_opt1156z00_54)
	{
		{	/* Unsafe/crc.scm 335 */
			{	/* Unsafe/crc.scm 335 */
				obj_t BgL_namez00_1840;
				obj_t BgL_strz00_1841;

				BgL_namez00_1840 = VECTOR_REF(BgL_opt1156z00_54, 0L);
				BgL_strz00_1841 = VECTOR_REF(BgL_opt1156z00_54, 1L);
				{	/* Unsafe/crc.scm 335 */
					obj_t BgL_bigzd2endianzf3z21_1842;

					BgL_bigzd2endianzf3z21_1842 = BTRUE;
					{	/* Unsafe/crc.scm 335 */
						obj_t BgL_finalzd2xorzd2_1843;

						BgL_finalzd2xorzd2_1843 = BINT(0L);
						{	/* Unsafe/crc.scm 335 */
							obj_t BgL_initz00_1844;

							BgL_initz00_1844 = BINT(0L);
							{	/* Unsafe/crc.scm 335 */

								{
									long BgL_iz00_1845;

									BgL_iz00_1845 = 2L;
								BgL_check1160z00_1846:
									if ((BgL_iz00_1845 == VECTOR_LENGTH(BgL_opt1156z00_54)))
										{	/* Unsafe/crc.scm 335 */
											BNIL;
										}
									else
										{	/* Unsafe/crc.scm 335 */
											bool_t BgL_test2596z00_6827;

											{	/* Unsafe/crc.scm 335 */
												obj_t BgL_arg1653z00_1852;

												BgL_arg1653z00_1852 =
													VECTOR_REF(BgL_opt1156z00_54, BgL_iz00_1845);
												BgL_test2596z00_6827 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1653z00_1852, BGl_list2348z00zz__crcz00));
											}
											if (BgL_test2596z00_6827)
												{
													long BgL_iz00_6831;

													BgL_iz00_6831 = (BgL_iz00_1845 + 2L);
													BgL_iz00_1845 = BgL_iz00_6831;
													goto BgL_check1160z00_1846;
												}
											else
												{	/* Unsafe/crc.scm 335 */
													obj_t BgL_arg1652z00_1851;

													BgL_arg1652z00_1851 =
														VECTOR_REF(BgL_opt1156z00_54, BgL_iz00_1845);
													BGl_errorz00zz__errorz00(BGl_symbol2369z00zz__crcz00,
														BGl_string2357z00zz__crcz00, BgL_arg1652z00_1851);
												}
										}
								}
								{	/* Unsafe/crc.scm 335 */
									obj_t BgL_index1162z00_1853;

									{
										long BgL_iz00_3283;

										BgL_iz00_3283 = 2L;
									BgL_search1159z00_3282:
										if ((BgL_iz00_3283 == VECTOR_LENGTH(BgL_opt1156z00_54)))
											{	/* Unsafe/crc.scm 335 */
												BgL_index1162z00_1853 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 335 */
												if (
													(BgL_iz00_3283 ==
														(VECTOR_LENGTH(BgL_opt1156z00_54) - 1L)))
													{	/* Unsafe/crc.scm 335 */
														BgL_index1162z00_1853 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2369z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1156z00_54)));
													}
												else
													{	/* Unsafe/crc.scm 335 */
														obj_t BgL_vz00_3293;

														BgL_vz00_3293 =
															VECTOR_REF(BgL_opt1156z00_54, BgL_iz00_3283);
														if ((BgL_vz00_3293 == BGl_keyword2349z00zz__crcz00))
															{	/* Unsafe/crc.scm 335 */
																BgL_index1162z00_1853 =
																	BINT((BgL_iz00_3283 + 1L));
															}
														else
															{
																long BgL_iz00_6851;

																BgL_iz00_6851 = (BgL_iz00_3283 + 2L);
																BgL_iz00_3283 = BgL_iz00_6851;
																goto BgL_search1159z00_3282;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 335 */
										bool_t BgL_test2600z00_6853;

										{	/* Unsafe/crc.scm 335 */
											long BgL_n1z00_3297;

											{	/* Unsafe/crc.scm 335 */
												obj_t BgL_tmpz00_6854;

												if (INTEGERP(BgL_index1162z00_1853))
													{	/* Unsafe/crc.scm 335 */
														BgL_tmpz00_6854 = BgL_index1162z00_1853;
													}
												else
													{
														obj_t BgL_auxz00_6857;

														BgL_auxz00_6857 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(13795L),
															BGl_string2371z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1162z00_1853);
														FAILURE(BgL_auxz00_6857, BFALSE, BFALSE);
													}
												BgL_n1z00_3297 = (long) CINT(BgL_tmpz00_6854);
											}
											BgL_test2600z00_6853 = (BgL_n1z00_3297 >= 0L);
										}
										if (BgL_test2600z00_6853)
											{
												long BgL_auxz00_6863;

												{	/* Unsafe/crc.scm 335 */
													obj_t BgL_tmpz00_6864;

													if (INTEGERP(BgL_index1162z00_1853))
														{	/* Unsafe/crc.scm 335 */
															BgL_tmpz00_6864 = BgL_index1162z00_1853;
														}
													else
														{
															obj_t BgL_auxz00_6867;

															BgL_auxz00_6867 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13795L),
																BGl_string2371z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1162z00_1853);
															FAILURE(BgL_auxz00_6867, BFALSE, BFALSE);
														}
													BgL_auxz00_6863 = (long) CINT(BgL_tmpz00_6864);
												}
												BgL_bigzd2endianzf3z21_1842 =
													VECTOR_REF(BgL_opt1156z00_54, BgL_auxz00_6863);
											}
										else
											{	/* Unsafe/crc.scm 335 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 335 */
									obj_t BgL_index1163z00_1855;

									{
										long BgL_iz00_3299;

										BgL_iz00_3299 = 2L;
									BgL_search1159z00_3298:
										if ((BgL_iz00_3299 == VECTOR_LENGTH(BgL_opt1156z00_54)))
											{	/* Unsafe/crc.scm 335 */
												BgL_index1163z00_1855 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 335 */
												if (
													(BgL_iz00_3299 ==
														(VECTOR_LENGTH(BgL_opt1156z00_54) - 1L)))
													{	/* Unsafe/crc.scm 335 */
														BgL_index1163z00_1855 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2369z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1156z00_54)));
													}
												else
													{	/* Unsafe/crc.scm 335 */
														obj_t BgL_vz00_3309;

														BgL_vz00_3309 =
															VECTOR_REF(BgL_opt1156z00_54, BgL_iz00_3299);
														if ((BgL_vz00_3309 == BGl_keyword2351z00zz__crcz00))
															{	/* Unsafe/crc.scm 335 */
																BgL_index1163z00_1855 =
																	BINT((BgL_iz00_3299 + 1L));
															}
														else
															{
																long BgL_iz00_6889;

																BgL_iz00_6889 = (BgL_iz00_3299 + 2L);
																BgL_iz00_3299 = BgL_iz00_6889;
																goto BgL_search1159z00_3298;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 335 */
										bool_t BgL_test2606z00_6891;

										{	/* Unsafe/crc.scm 335 */
											long BgL_n1z00_3313;

											{	/* Unsafe/crc.scm 335 */
												obj_t BgL_tmpz00_6892;

												if (INTEGERP(BgL_index1163z00_1855))
													{	/* Unsafe/crc.scm 335 */
														BgL_tmpz00_6892 = BgL_index1163z00_1855;
													}
												else
													{
														obj_t BgL_auxz00_6895;

														BgL_auxz00_6895 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(13795L),
															BGl_string2371z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1163z00_1855);
														FAILURE(BgL_auxz00_6895, BFALSE, BFALSE);
													}
												BgL_n1z00_3313 = (long) CINT(BgL_tmpz00_6892);
											}
											BgL_test2606z00_6891 = (BgL_n1z00_3313 >= 0L);
										}
										if (BgL_test2606z00_6891)
											{
												long BgL_auxz00_6901;

												{	/* Unsafe/crc.scm 335 */
													obj_t BgL_tmpz00_6902;

													if (INTEGERP(BgL_index1163z00_1855))
														{	/* Unsafe/crc.scm 335 */
															BgL_tmpz00_6902 = BgL_index1163z00_1855;
														}
													else
														{
															obj_t BgL_auxz00_6905;

															BgL_auxz00_6905 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13795L),
																BGl_string2371z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1163z00_1855);
															FAILURE(BgL_auxz00_6905, BFALSE, BFALSE);
														}
													BgL_auxz00_6901 = (long) CINT(BgL_tmpz00_6902);
												}
												BgL_finalzd2xorzd2_1843 =
													VECTOR_REF(BgL_opt1156z00_54, BgL_auxz00_6901);
											}
										else
											{	/* Unsafe/crc.scm 335 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 335 */
									obj_t BgL_index1164z00_1857;

									{
										long BgL_iz00_3315;

										BgL_iz00_3315 = 2L;
									BgL_search1159z00_3314:
										if ((BgL_iz00_3315 == VECTOR_LENGTH(BgL_opt1156z00_54)))
											{	/* Unsafe/crc.scm 335 */
												BgL_index1164z00_1857 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 335 */
												if (
													(BgL_iz00_3315 ==
														(VECTOR_LENGTH(BgL_opt1156z00_54) - 1L)))
													{	/* Unsafe/crc.scm 335 */
														BgL_index1164z00_1857 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2369z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1156z00_54)));
													}
												else
													{	/* Unsafe/crc.scm 335 */
														obj_t BgL_vz00_3325;

														BgL_vz00_3325 =
															VECTOR_REF(BgL_opt1156z00_54, BgL_iz00_3315);
														if ((BgL_vz00_3325 == BGl_keyword2353z00zz__crcz00))
															{	/* Unsafe/crc.scm 335 */
																BgL_index1164z00_1857 =
																	BINT((BgL_iz00_3315 + 1L));
															}
														else
															{
																long BgL_iz00_6927;

																BgL_iz00_6927 = (BgL_iz00_3315 + 2L);
																BgL_iz00_3315 = BgL_iz00_6927;
																goto BgL_search1159z00_3314;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 335 */
										bool_t BgL_test2612z00_6929;

										{	/* Unsafe/crc.scm 335 */
											long BgL_n1z00_3329;

											{	/* Unsafe/crc.scm 335 */
												obj_t BgL_tmpz00_6930;

												if (INTEGERP(BgL_index1164z00_1857))
													{	/* Unsafe/crc.scm 335 */
														BgL_tmpz00_6930 = BgL_index1164z00_1857;
													}
												else
													{
														obj_t BgL_auxz00_6933;

														BgL_auxz00_6933 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(13795L),
															BGl_string2371z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1164z00_1857);
														FAILURE(BgL_auxz00_6933, BFALSE, BFALSE);
													}
												BgL_n1z00_3329 = (long) CINT(BgL_tmpz00_6930);
											}
											BgL_test2612z00_6929 = (BgL_n1z00_3329 >= 0L);
										}
										if (BgL_test2612z00_6929)
											{
												long BgL_auxz00_6939;

												{	/* Unsafe/crc.scm 335 */
													obj_t BgL_tmpz00_6940;

													if (INTEGERP(BgL_index1164z00_1857))
														{	/* Unsafe/crc.scm 335 */
															BgL_tmpz00_6940 = BgL_index1164z00_1857;
														}
													else
														{
															obj_t BgL_auxz00_6943;

															BgL_auxz00_6943 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13795L),
																BGl_string2371z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1164z00_1857);
															FAILURE(BgL_auxz00_6943, BFALSE, BFALSE);
														}
													BgL_auxz00_6939 = (long) CINT(BgL_tmpz00_6940);
												}
												BgL_initz00_1844 =
													VECTOR_REF(BgL_opt1156z00_54, BgL_auxz00_6939);
											}
										else
											{	/* Unsafe/crc.scm 335 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 335 */
									obj_t BgL_arg1657z00_1859;
									obj_t BgL_arg1658z00_1860;

									BgL_arg1657z00_1859 = VECTOR_REF(BgL_opt1156z00_54, 0L);
									BgL_arg1658z00_1860 = VECTOR_REF(BgL_opt1156z00_54, 1L);
									{	/* Unsafe/crc.scm 335 */
										obj_t BgL_bigzd2endianzf3z21_1861;

										BgL_bigzd2endianzf3z21_1861 = BgL_bigzd2endianzf3z21_1842;
										{	/* Unsafe/crc.scm 335 */
											obj_t BgL_finalzd2xorzd2_1862;

											BgL_finalzd2xorzd2_1862 = BgL_finalzd2xorzd2_1843;
											{	/* Unsafe/crc.scm 335 */
												obj_t BgL_initz00_1863;

												BgL_initz00_1863 = BgL_initz00_1844;
												{	/* Unsafe/crc.scm 335 */
													obj_t BgL_strz00_3330;

													if (STRINGP(BgL_arg1658z00_1860))
														{	/* Unsafe/crc.scm 335 */
															BgL_strz00_3330 = BgL_arg1658z00_1860;
														}
													else
														{
															obj_t BgL_auxz00_6953;

															BgL_auxz00_6953 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(13795L),
																BGl_string2371z00zz__crcz00,
																BGl_string2365z00zz__crcz00,
																BgL_arg1658z00_1860);
															FAILURE(BgL_auxz00_6953, BFALSE, BFALSE);
														}
													{	/* Unsafe/crc.scm 336 */
														obj_t BgL_arg1668z00_3331;

														{	/* Unsafe/crc.scm 336 */
															long BgL_endz00_3334;

															BgL_endz00_3334 = STRING_LENGTH(BgL_strz00_3330);
															{	/* Unsafe/crc.scm 336 */

																BgL_arg1668z00_3331 =
																	BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
																	(BgL_strz00_3330, BINT(0L),
																	BINT(BgL_endz00_3334));
														}}
														return
															BGl_crczd2fastzd2portz00zz__crcz00
															(BgL_arg1657z00_1859, BgL_arg1668z00_3331,
															BgL_initz00_1863, BgL_finalzd2xorzd2_1862,
															BgL_bigzd2endianzf3z21_1861);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* crc-string */
	BGL_EXPORTED_DEF obj_t BGl_crczd2stringzd2zz__crcz00(obj_t BgL_namez00_49,
		obj_t BgL_strz00_50, obj_t BgL_bigzd2endianzf3z21_51,
		obj_t BgL_finalzd2xorzd2_52, obj_t BgL_initz00_53)
	{
		{	/* Unsafe/crc.scm 335 */
			{	/* Unsafe/crc.scm 336 */
				obj_t BgL_arg1668z00_3336;

				{	/* Unsafe/crc.scm 336 */
					long BgL_endz00_3339;

					BgL_endz00_3339 = STRING_LENGTH(BgL_strz00_50);
					{	/* Unsafe/crc.scm 336 */

						BgL_arg1668z00_3336 =
							BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_strz00_50,
							BINT(0L), BINT(BgL_endz00_3339));
				}}
				return
					BGl_crczd2fastzd2portz00zz__crcz00(BgL_namez00_49,
					BgL_arg1668z00_3336, BgL_initz00_53, BgL_finalzd2xorzd2_52,
					BgL_bigzd2endianzf3z21_51);
			}
		}

	}



/* _crc-port */
	obj_t BGl__crczd2portzd2zz__crcz00(obj_t BgL_env1166z00_62,
		obj_t BgL_opt1165z00_61)
	{
		{	/* Unsafe/crc.scm 341 */
			{	/* Unsafe/crc.scm 341 */
				obj_t BgL_namez00_1879;
				obj_t BgL_pz00_1880;

				BgL_namez00_1879 = VECTOR_REF(BgL_opt1165z00_61, 0L);
				BgL_pz00_1880 = VECTOR_REF(BgL_opt1165z00_61, 1L);
				{	/* Unsafe/crc.scm 341 */
					obj_t BgL_bigzd2endianzf3z21_1881;

					BgL_bigzd2endianzf3z21_1881 = BTRUE;
					{	/* Unsafe/crc.scm 341 */
						obj_t BgL_finalzd2xorzd2_1882;

						BgL_finalzd2xorzd2_1882 = BINT(0L);
						{	/* Unsafe/crc.scm 341 */
							obj_t BgL_initz00_1883;

							BgL_initz00_1883 = BINT(0L);
							{	/* Unsafe/crc.scm 341 */

								{
									long BgL_iz00_1884;

									BgL_iz00_1884 = 2L;
								BgL_check1169z00_1885:
									if ((BgL_iz00_1884 == VECTOR_LENGTH(BgL_opt1165z00_61)))
										{	/* Unsafe/crc.scm 341 */
											BNIL;
										}
									else
										{	/* Unsafe/crc.scm 341 */
											bool_t BgL_test2617z00_6974;

											{	/* Unsafe/crc.scm 341 */
												obj_t BgL_arg1678z00_1891;

												BgL_arg1678z00_1891 =
													VECTOR_REF(BgL_opt1165z00_61, BgL_iz00_1884);
												BgL_test2617z00_6974 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1678z00_1891, BGl_list2348z00zz__crcz00));
											}
											if (BgL_test2617z00_6974)
												{
													long BgL_iz00_6978;

													BgL_iz00_6978 = (BgL_iz00_1884 + 2L);
													BgL_iz00_1884 = BgL_iz00_6978;
													goto BgL_check1169z00_1885;
												}
											else
												{	/* Unsafe/crc.scm 341 */
													obj_t BgL_arg1676z00_1890;

													BgL_arg1676z00_1890 =
														VECTOR_REF(BgL_opt1165z00_61, BgL_iz00_1884);
													BGl_errorz00zz__errorz00(BGl_symbol2372z00zz__crcz00,
														BGl_string2357z00zz__crcz00, BgL_arg1676z00_1890);
												}
										}
								}
								{	/* Unsafe/crc.scm 341 */
									obj_t BgL_index1171z00_1892;

									{
										long BgL_iz00_3357;

										BgL_iz00_3357 = 2L;
									BgL_search1168z00_3356:
										if ((BgL_iz00_3357 == VECTOR_LENGTH(BgL_opt1165z00_61)))
											{	/* Unsafe/crc.scm 341 */
												BgL_index1171z00_1892 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 341 */
												if (
													(BgL_iz00_3357 ==
														(VECTOR_LENGTH(BgL_opt1165z00_61) - 1L)))
													{	/* Unsafe/crc.scm 341 */
														BgL_index1171z00_1892 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2372z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1165z00_61)));
													}
												else
													{	/* Unsafe/crc.scm 341 */
														obj_t BgL_vz00_3367;

														BgL_vz00_3367 =
															VECTOR_REF(BgL_opt1165z00_61, BgL_iz00_3357);
														if ((BgL_vz00_3367 == BGl_keyword2349z00zz__crcz00))
															{	/* Unsafe/crc.scm 341 */
																BgL_index1171z00_1892 =
																	BINT((BgL_iz00_3357 + 1L));
															}
														else
															{
																long BgL_iz00_6998;

																BgL_iz00_6998 = (BgL_iz00_3357 + 2L);
																BgL_iz00_3357 = BgL_iz00_6998;
																goto BgL_search1168z00_3356;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 341 */
										bool_t BgL_test2621z00_7000;

										{	/* Unsafe/crc.scm 341 */
											long BgL_n1z00_3371;

											{	/* Unsafe/crc.scm 341 */
												obj_t BgL_tmpz00_7001;

												if (INTEGERP(BgL_index1171z00_1892))
													{	/* Unsafe/crc.scm 341 */
														BgL_tmpz00_7001 = BgL_index1171z00_1892;
													}
												else
													{
														obj_t BgL_auxz00_7004;

														BgL_auxz00_7004 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(14180L),
															BGl_string2374z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1171z00_1892);
														FAILURE(BgL_auxz00_7004, BFALSE, BFALSE);
													}
												BgL_n1z00_3371 = (long) CINT(BgL_tmpz00_7001);
											}
											BgL_test2621z00_7000 = (BgL_n1z00_3371 >= 0L);
										}
										if (BgL_test2621z00_7000)
											{
												long BgL_auxz00_7010;

												{	/* Unsafe/crc.scm 341 */
													obj_t BgL_tmpz00_7011;

													if (INTEGERP(BgL_index1171z00_1892))
														{	/* Unsafe/crc.scm 341 */
															BgL_tmpz00_7011 = BgL_index1171z00_1892;
														}
													else
														{
															obj_t BgL_auxz00_7014;

															BgL_auxz00_7014 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14180L),
																BGl_string2374z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1171z00_1892);
															FAILURE(BgL_auxz00_7014, BFALSE, BFALSE);
														}
													BgL_auxz00_7010 = (long) CINT(BgL_tmpz00_7011);
												}
												BgL_bigzd2endianzf3z21_1881 =
													VECTOR_REF(BgL_opt1165z00_61, BgL_auxz00_7010);
											}
										else
											{	/* Unsafe/crc.scm 341 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 341 */
									obj_t BgL_index1172z00_1894;

									{
										long BgL_iz00_3373;

										BgL_iz00_3373 = 2L;
									BgL_search1168z00_3372:
										if ((BgL_iz00_3373 == VECTOR_LENGTH(BgL_opt1165z00_61)))
											{	/* Unsafe/crc.scm 341 */
												BgL_index1172z00_1894 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 341 */
												if (
													(BgL_iz00_3373 ==
														(VECTOR_LENGTH(BgL_opt1165z00_61) - 1L)))
													{	/* Unsafe/crc.scm 341 */
														BgL_index1172z00_1894 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2372z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1165z00_61)));
													}
												else
													{	/* Unsafe/crc.scm 341 */
														obj_t BgL_vz00_3383;

														BgL_vz00_3383 =
															VECTOR_REF(BgL_opt1165z00_61, BgL_iz00_3373);
														if ((BgL_vz00_3383 == BGl_keyword2351z00zz__crcz00))
															{	/* Unsafe/crc.scm 341 */
																BgL_index1172z00_1894 =
																	BINT((BgL_iz00_3373 + 1L));
															}
														else
															{
																long BgL_iz00_7036;

																BgL_iz00_7036 = (BgL_iz00_3373 + 2L);
																BgL_iz00_3373 = BgL_iz00_7036;
																goto BgL_search1168z00_3372;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 341 */
										bool_t BgL_test2627z00_7038;

										{	/* Unsafe/crc.scm 341 */
											long BgL_n1z00_3387;

											{	/* Unsafe/crc.scm 341 */
												obj_t BgL_tmpz00_7039;

												if (INTEGERP(BgL_index1172z00_1894))
													{	/* Unsafe/crc.scm 341 */
														BgL_tmpz00_7039 = BgL_index1172z00_1894;
													}
												else
													{
														obj_t BgL_auxz00_7042;

														BgL_auxz00_7042 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(14180L),
															BGl_string2374z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1172z00_1894);
														FAILURE(BgL_auxz00_7042, BFALSE, BFALSE);
													}
												BgL_n1z00_3387 = (long) CINT(BgL_tmpz00_7039);
											}
											BgL_test2627z00_7038 = (BgL_n1z00_3387 >= 0L);
										}
										if (BgL_test2627z00_7038)
											{
												long BgL_auxz00_7048;

												{	/* Unsafe/crc.scm 341 */
													obj_t BgL_tmpz00_7049;

													if (INTEGERP(BgL_index1172z00_1894))
														{	/* Unsafe/crc.scm 341 */
															BgL_tmpz00_7049 = BgL_index1172z00_1894;
														}
													else
														{
															obj_t BgL_auxz00_7052;

															BgL_auxz00_7052 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14180L),
																BGl_string2374z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1172z00_1894);
															FAILURE(BgL_auxz00_7052, BFALSE, BFALSE);
														}
													BgL_auxz00_7048 = (long) CINT(BgL_tmpz00_7049);
												}
												BgL_finalzd2xorzd2_1882 =
													VECTOR_REF(BgL_opt1165z00_61, BgL_auxz00_7048);
											}
										else
											{	/* Unsafe/crc.scm 341 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 341 */
									obj_t BgL_index1173z00_1896;

									{
										long BgL_iz00_3389;

										BgL_iz00_3389 = 2L;
									BgL_search1168z00_3388:
										if ((BgL_iz00_3389 == VECTOR_LENGTH(BgL_opt1165z00_61)))
											{	/* Unsafe/crc.scm 341 */
												BgL_index1173z00_1896 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 341 */
												if (
													(BgL_iz00_3389 ==
														(VECTOR_LENGTH(BgL_opt1165z00_61) - 1L)))
													{	/* Unsafe/crc.scm 341 */
														BgL_index1173z00_1896 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2372z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1165z00_61)));
													}
												else
													{	/* Unsafe/crc.scm 341 */
														obj_t BgL_vz00_3399;

														BgL_vz00_3399 =
															VECTOR_REF(BgL_opt1165z00_61, BgL_iz00_3389);
														if ((BgL_vz00_3399 == BGl_keyword2353z00zz__crcz00))
															{	/* Unsafe/crc.scm 341 */
																BgL_index1173z00_1896 =
																	BINT((BgL_iz00_3389 + 1L));
															}
														else
															{
																long BgL_iz00_7074;

																BgL_iz00_7074 = (BgL_iz00_3389 + 2L);
																BgL_iz00_3389 = BgL_iz00_7074;
																goto BgL_search1168z00_3388;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 341 */
										bool_t BgL_test2633z00_7076;

										{	/* Unsafe/crc.scm 341 */
											long BgL_n1z00_3403;

											{	/* Unsafe/crc.scm 341 */
												obj_t BgL_tmpz00_7077;

												if (INTEGERP(BgL_index1173z00_1896))
													{	/* Unsafe/crc.scm 341 */
														BgL_tmpz00_7077 = BgL_index1173z00_1896;
													}
												else
													{
														obj_t BgL_auxz00_7080;

														BgL_auxz00_7080 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(14180L),
															BGl_string2374z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1173z00_1896);
														FAILURE(BgL_auxz00_7080, BFALSE, BFALSE);
													}
												BgL_n1z00_3403 = (long) CINT(BgL_tmpz00_7077);
											}
											BgL_test2633z00_7076 = (BgL_n1z00_3403 >= 0L);
										}
										if (BgL_test2633z00_7076)
											{
												long BgL_auxz00_7086;

												{	/* Unsafe/crc.scm 341 */
													obj_t BgL_tmpz00_7087;

													if (INTEGERP(BgL_index1173z00_1896))
														{	/* Unsafe/crc.scm 341 */
															BgL_tmpz00_7087 = BgL_index1173z00_1896;
														}
													else
														{
															obj_t BgL_auxz00_7090;

															BgL_auxz00_7090 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14180L),
																BGl_string2374z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1173z00_1896);
															FAILURE(BgL_auxz00_7090, BFALSE, BFALSE);
														}
													BgL_auxz00_7086 = (long) CINT(BgL_tmpz00_7087);
												}
												BgL_initz00_1883 =
													VECTOR_REF(BgL_opt1165z00_61, BgL_auxz00_7086);
											}
										else
											{	/* Unsafe/crc.scm 341 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 341 */
									obj_t BgL_arg1684z00_1898;
									obj_t BgL_arg1685z00_1899;

									BgL_arg1684z00_1898 = VECTOR_REF(BgL_opt1165z00_61, 0L);
									BgL_arg1685z00_1899 = VECTOR_REF(BgL_opt1165z00_61, 1L);
									{	/* Unsafe/crc.scm 341 */
										obj_t BgL_bigzd2endianzf3z21_1900;

										BgL_bigzd2endianzf3z21_1900 = BgL_bigzd2endianzf3z21_1881;
										{	/* Unsafe/crc.scm 341 */
											obj_t BgL_finalzd2xorzd2_1901;

											BgL_finalzd2xorzd2_1901 = BgL_finalzd2xorzd2_1882;
											{	/* Unsafe/crc.scm 341 */
												obj_t BgL_initz00_1902;

												BgL_initz00_1902 = BgL_initz00_1883;
												{	/* Unsafe/crc.scm 341 */
													obj_t BgL_pz00_3404;

													if (INPUT_PORTP(BgL_arg1685z00_1899))
														{	/* Unsafe/crc.scm 341 */
															BgL_pz00_3404 = BgL_arg1685z00_1899;
														}
													else
														{
															obj_t BgL_auxz00_7100;

															BgL_auxz00_7100 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14180L),
																BGl_string2374z00zz__crcz00,
																BGl_string2367z00zz__crcz00,
																BgL_arg1685z00_1899);
															FAILURE(BgL_auxz00_7100, BFALSE, BFALSE);
														}
													return
														BGl_crczd2fastzd2portz00zz__crcz00
														(BgL_arg1684z00_1898, BgL_pz00_3404,
														BgL_initz00_1902, BgL_finalzd2xorzd2_1901,
														BgL_bigzd2endianzf3z21_1900);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* crc-port */
	BGL_EXPORTED_DEF obj_t BGl_crczd2portzd2zz__crcz00(obj_t BgL_namez00_56,
		obj_t BgL_pz00_57, obj_t BgL_bigzd2endianzf3z21_58,
		obj_t BgL_finalzd2xorzd2_59, obj_t BgL_initz00_60)
	{
		{	/* Unsafe/crc.scm 341 */
			BGL_TAIL return
				BGl_crczd2fastzd2portz00zz__crcz00(BgL_namez00_56, BgL_pz00_57,
				BgL_initz00_60, BgL_finalzd2xorzd2_59, BgL_bigzd2endianzf3z21_58);
		}

	}



/* _crc-mmap */
	obj_t BGl__crczd2mmapzd2zz__crcz00(obj_t BgL_env1175z00_69,
		obj_t BgL_opt1174z00_68)
	{
		{	/* Unsafe/crc.scm 347 */
			{	/* Unsafe/crc.scm 347 */
				obj_t BgL_namez00_1914;
				obj_t BgL_mz00_1915;

				BgL_namez00_1914 = VECTOR_REF(BgL_opt1174z00_68, 0L);
				BgL_mz00_1915 = VECTOR_REF(BgL_opt1174z00_68, 1L);
				{	/* Unsafe/crc.scm 347 */
					obj_t BgL_bigzd2endianzf3z21_1916;

					BgL_bigzd2endianzf3z21_1916 = BTRUE;
					{	/* Unsafe/crc.scm 347 */
						obj_t BgL_finalzd2xorzd2_1917;

						BgL_finalzd2xorzd2_1917 = BINT(0L);
						{	/* Unsafe/crc.scm 347 */
							obj_t BgL_initz00_1918;

							BgL_initz00_1918 = BINT(0L);
							{	/* Unsafe/crc.scm 347 */

								{
									long BgL_iz00_1919;

									BgL_iz00_1919 = 2L;
								BgL_check1178z00_1920:
									if ((BgL_iz00_1919 == VECTOR_LENGTH(BgL_opt1174z00_68)))
										{	/* Unsafe/crc.scm 347 */
											BNIL;
										}
									else
										{	/* Unsafe/crc.scm 347 */
											bool_t BgL_test2638z00_7113;

											{	/* Unsafe/crc.scm 347 */
												obj_t BgL_arg1705z00_1926;

												BgL_arg1705z00_1926 =
													VECTOR_REF(BgL_opt1174z00_68, BgL_iz00_1919);
												BgL_test2638z00_7113 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1705z00_1926, BGl_list2348z00zz__crcz00));
											}
											if (BgL_test2638z00_7113)
												{
													long BgL_iz00_7117;

													BgL_iz00_7117 = (BgL_iz00_1919 + 2L);
													BgL_iz00_1919 = BgL_iz00_7117;
													goto BgL_check1178z00_1920;
												}
											else
												{	/* Unsafe/crc.scm 347 */
													obj_t BgL_arg1704z00_1925;

													BgL_arg1704z00_1925 =
														VECTOR_REF(BgL_opt1174z00_68, BgL_iz00_1919);
													BGl_errorz00zz__errorz00(BGl_symbol2375z00zz__crcz00,
														BGl_string2357z00zz__crcz00, BgL_arg1704z00_1925);
												}
										}
								}
								{	/* Unsafe/crc.scm 347 */
									obj_t BgL_index1180z00_1927;

									{
										long BgL_iz00_3421;

										BgL_iz00_3421 = 2L;
									BgL_search1177z00_3420:
										if ((BgL_iz00_3421 == VECTOR_LENGTH(BgL_opt1174z00_68)))
											{	/* Unsafe/crc.scm 347 */
												BgL_index1180z00_1927 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 347 */
												if (
													(BgL_iz00_3421 ==
														(VECTOR_LENGTH(BgL_opt1174z00_68) - 1L)))
													{	/* Unsafe/crc.scm 347 */
														BgL_index1180z00_1927 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2375z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1174z00_68)));
													}
												else
													{	/* Unsafe/crc.scm 347 */
														obj_t BgL_vz00_3431;

														BgL_vz00_3431 =
															VECTOR_REF(BgL_opt1174z00_68, BgL_iz00_3421);
														if ((BgL_vz00_3431 == BGl_keyword2349z00zz__crcz00))
															{	/* Unsafe/crc.scm 347 */
																BgL_index1180z00_1927 =
																	BINT((BgL_iz00_3421 + 1L));
															}
														else
															{
																long BgL_iz00_7137;

																BgL_iz00_7137 = (BgL_iz00_3421 + 2L);
																BgL_iz00_3421 = BgL_iz00_7137;
																goto BgL_search1177z00_3420;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 347 */
										bool_t BgL_test2642z00_7139;

										{	/* Unsafe/crc.scm 347 */
											long BgL_n1z00_3435;

											{	/* Unsafe/crc.scm 347 */
												obj_t BgL_tmpz00_7140;

												if (INTEGERP(BgL_index1180z00_1927))
													{	/* Unsafe/crc.scm 347 */
														BgL_tmpz00_7140 = BgL_index1180z00_1927;
													}
												else
													{
														obj_t BgL_auxz00_7143;

														BgL_auxz00_7143 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(14542L),
															BGl_string2377z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1180z00_1927);
														FAILURE(BgL_auxz00_7143, BFALSE, BFALSE);
													}
												BgL_n1z00_3435 = (long) CINT(BgL_tmpz00_7140);
											}
											BgL_test2642z00_7139 = (BgL_n1z00_3435 >= 0L);
										}
										if (BgL_test2642z00_7139)
											{
												long BgL_auxz00_7149;

												{	/* Unsafe/crc.scm 347 */
													obj_t BgL_tmpz00_7150;

													if (INTEGERP(BgL_index1180z00_1927))
														{	/* Unsafe/crc.scm 347 */
															BgL_tmpz00_7150 = BgL_index1180z00_1927;
														}
													else
														{
															obj_t BgL_auxz00_7153;

															BgL_auxz00_7153 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14542L),
																BGl_string2377z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1180z00_1927);
															FAILURE(BgL_auxz00_7153, BFALSE, BFALSE);
														}
													BgL_auxz00_7149 = (long) CINT(BgL_tmpz00_7150);
												}
												BgL_bigzd2endianzf3z21_1916 =
													VECTOR_REF(BgL_opt1174z00_68, BgL_auxz00_7149);
											}
										else
											{	/* Unsafe/crc.scm 347 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 347 */
									obj_t BgL_index1181z00_1929;

									{
										long BgL_iz00_3437;

										BgL_iz00_3437 = 2L;
									BgL_search1177z00_3436:
										if ((BgL_iz00_3437 == VECTOR_LENGTH(BgL_opt1174z00_68)))
											{	/* Unsafe/crc.scm 347 */
												BgL_index1181z00_1929 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 347 */
												if (
													(BgL_iz00_3437 ==
														(VECTOR_LENGTH(BgL_opt1174z00_68) - 1L)))
													{	/* Unsafe/crc.scm 347 */
														BgL_index1181z00_1929 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2375z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1174z00_68)));
													}
												else
													{	/* Unsafe/crc.scm 347 */
														obj_t BgL_vz00_3447;

														BgL_vz00_3447 =
															VECTOR_REF(BgL_opt1174z00_68, BgL_iz00_3437);
														if ((BgL_vz00_3447 == BGl_keyword2351z00zz__crcz00))
															{	/* Unsafe/crc.scm 347 */
																BgL_index1181z00_1929 =
																	BINT((BgL_iz00_3437 + 1L));
															}
														else
															{
																long BgL_iz00_7175;

																BgL_iz00_7175 = (BgL_iz00_3437 + 2L);
																BgL_iz00_3437 = BgL_iz00_7175;
																goto BgL_search1177z00_3436;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 347 */
										bool_t BgL_test2648z00_7177;

										{	/* Unsafe/crc.scm 347 */
											long BgL_n1z00_3451;

											{	/* Unsafe/crc.scm 347 */
												obj_t BgL_tmpz00_7178;

												if (INTEGERP(BgL_index1181z00_1929))
													{	/* Unsafe/crc.scm 347 */
														BgL_tmpz00_7178 = BgL_index1181z00_1929;
													}
												else
													{
														obj_t BgL_auxz00_7181;

														BgL_auxz00_7181 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(14542L),
															BGl_string2377z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1181z00_1929);
														FAILURE(BgL_auxz00_7181, BFALSE, BFALSE);
													}
												BgL_n1z00_3451 = (long) CINT(BgL_tmpz00_7178);
											}
											BgL_test2648z00_7177 = (BgL_n1z00_3451 >= 0L);
										}
										if (BgL_test2648z00_7177)
											{
												long BgL_auxz00_7187;

												{	/* Unsafe/crc.scm 347 */
													obj_t BgL_tmpz00_7188;

													if (INTEGERP(BgL_index1181z00_1929))
														{	/* Unsafe/crc.scm 347 */
															BgL_tmpz00_7188 = BgL_index1181z00_1929;
														}
													else
														{
															obj_t BgL_auxz00_7191;

															BgL_auxz00_7191 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14542L),
																BGl_string2377z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1181z00_1929);
															FAILURE(BgL_auxz00_7191, BFALSE, BFALSE);
														}
													BgL_auxz00_7187 = (long) CINT(BgL_tmpz00_7188);
												}
												BgL_finalzd2xorzd2_1917 =
													VECTOR_REF(BgL_opt1174z00_68, BgL_auxz00_7187);
											}
										else
											{	/* Unsafe/crc.scm 347 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 347 */
									obj_t BgL_index1182z00_1931;

									{
										long BgL_iz00_3453;

										BgL_iz00_3453 = 2L;
									BgL_search1177z00_3452:
										if ((BgL_iz00_3453 == VECTOR_LENGTH(BgL_opt1174z00_68)))
											{	/* Unsafe/crc.scm 347 */
												BgL_index1182z00_1931 = BINT(-1L);
											}
										else
											{	/* Unsafe/crc.scm 347 */
												if (
													(BgL_iz00_3453 ==
														(VECTOR_LENGTH(BgL_opt1174z00_68) - 1L)))
													{	/* Unsafe/crc.scm 347 */
														BgL_index1182z00_1931 =
															BGl_errorz00zz__errorz00
															(BGl_symbol2375z00zz__crcz00,
															BGl_string2358z00zz__crcz00,
															BINT(VECTOR_LENGTH(BgL_opt1174z00_68)));
													}
												else
													{	/* Unsafe/crc.scm 347 */
														obj_t BgL_vz00_3463;

														BgL_vz00_3463 =
															VECTOR_REF(BgL_opt1174z00_68, BgL_iz00_3453);
														if ((BgL_vz00_3463 == BGl_keyword2353z00zz__crcz00))
															{	/* Unsafe/crc.scm 347 */
																BgL_index1182z00_1931 =
																	BINT((BgL_iz00_3453 + 1L));
															}
														else
															{
																long BgL_iz00_7213;

																BgL_iz00_7213 = (BgL_iz00_3453 + 2L);
																BgL_iz00_3453 = BgL_iz00_7213;
																goto BgL_search1177z00_3452;
															}
													}
											}
									}
									{	/* Unsafe/crc.scm 347 */
										bool_t BgL_test2654z00_7215;

										{	/* Unsafe/crc.scm 347 */
											long BgL_n1z00_3467;

											{	/* Unsafe/crc.scm 347 */
												obj_t BgL_tmpz00_7216;

												if (INTEGERP(BgL_index1182z00_1931))
													{	/* Unsafe/crc.scm 347 */
														BgL_tmpz00_7216 = BgL_index1182z00_1931;
													}
												else
													{
														obj_t BgL_auxz00_7219;

														BgL_auxz00_7219 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2324z00zz__crcz00, BINT(14542L),
															BGl_string2377z00zz__crcz00,
															BGl_string2327z00zz__crcz00,
															BgL_index1182z00_1931);
														FAILURE(BgL_auxz00_7219, BFALSE, BFALSE);
													}
												BgL_n1z00_3467 = (long) CINT(BgL_tmpz00_7216);
											}
											BgL_test2654z00_7215 = (BgL_n1z00_3467 >= 0L);
										}
										if (BgL_test2654z00_7215)
											{
												long BgL_auxz00_7225;

												{	/* Unsafe/crc.scm 347 */
													obj_t BgL_tmpz00_7226;

													if (INTEGERP(BgL_index1182z00_1931))
														{	/* Unsafe/crc.scm 347 */
															BgL_tmpz00_7226 = BgL_index1182z00_1931;
														}
													else
														{
															obj_t BgL_auxz00_7229;

															BgL_auxz00_7229 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14542L),
																BGl_string2377z00zz__crcz00,
																BGl_string2327z00zz__crcz00,
																BgL_index1182z00_1931);
															FAILURE(BgL_auxz00_7229, BFALSE, BFALSE);
														}
													BgL_auxz00_7225 = (long) CINT(BgL_tmpz00_7226);
												}
												BgL_initz00_1918 =
													VECTOR_REF(BgL_opt1174z00_68, BgL_auxz00_7225);
											}
										else
											{	/* Unsafe/crc.scm 347 */
												BFALSE;
											}
									}
								}
								{	/* Unsafe/crc.scm 347 */
									obj_t BgL_arg1709z00_1933;
									obj_t BgL_arg1710z00_1934;

									BgL_arg1709z00_1933 = VECTOR_REF(BgL_opt1174z00_68, 0L);
									BgL_arg1710z00_1934 = VECTOR_REF(BgL_opt1174z00_68, 1L);
									{	/* Unsafe/crc.scm 347 */
										obj_t BgL_bigzd2endianzf3z21_1935;

										BgL_bigzd2endianzf3z21_1935 = BgL_bigzd2endianzf3z21_1916;
										{	/* Unsafe/crc.scm 347 */
											obj_t BgL_finalzd2xorzd2_1936;

											BgL_finalzd2xorzd2_1936 = BgL_finalzd2xorzd2_1917;
											{	/* Unsafe/crc.scm 347 */
												obj_t BgL_initz00_1937;

												BgL_initz00_1937 = BgL_initz00_1918;
												{	/* Unsafe/crc.scm 347 */
													obj_t BgL_mz00_3468;

													if (BGL_MMAPP(BgL_arg1710z00_1934))
														{	/* Unsafe/crc.scm 347 */
															BgL_mz00_3468 = BgL_arg1710z00_1934;
														}
													else
														{
															obj_t BgL_auxz00_7239;

															BgL_auxz00_7239 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string2324z00zz__crcz00, BINT(14542L),
																BGl_string2377z00zz__crcz00,
																BGl_string2360z00zz__crcz00,
																BgL_arg1710z00_1934);
															FAILURE(BgL_auxz00_7239, BFALSE, BFALSE);
														}
													return
														BGl_crczd2fastzd2mmapz00zz__crcz00
														(BgL_arg1709z00_1933, BgL_mz00_3468,
														BgL_initz00_1937, BgL_finalzd2xorzd2_1936,
														BgL_bigzd2endianzf3z21_1935);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* crc-mmap */
	BGL_EXPORTED_DEF obj_t BGl_crczd2mmapzd2zz__crcz00(obj_t BgL_namez00_63,
		obj_t BgL_mz00_64, obj_t BgL_bigzd2endianzf3z21_65,
		obj_t BgL_finalzd2xorzd2_66, obj_t BgL_initz00_67)
	{
		{	/* Unsafe/crc.scm 347 */
			BGL_TAIL return
				BGl_crczd2fastzd2mmapz00zz__crcz00(BgL_namez00_63, BgL_mz00_64,
				BgL_initz00_67, BgL_finalzd2xorzd2_66, BgL_bigzd2endianzf3z21_65);
		}

	}



/* get-crc */
	obj_t BGl_getzd2crczd2zz__crcz00(obj_t BgL_namez00_70)
	{
		{	/* Unsafe/crc.scm 353 */
			{	/* Unsafe/crc.scm 354 */
				obj_t BgL_crczd2desczd2_1945;

				BgL_crczd2desczd2_1945 =
					BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_namez00_70,
					BGl_za2crcsza2z00zz__crcz00);
				if (CBOOL(BgL_crczd2desczd2_1945))
					{	/* Unsafe/crc.scm 355 */
						BFALSE;
					}
				else
					{	/* Unsafe/crc.scm 355 */
						BGl_errorz00zz__errorz00(BGl_symbol2355z00zz__crcz00,
							BGl_string2378z00zz__crcz00, BgL_namez00_70);
					}
				{	/* Unsafe/crc.scm 357 */
					obj_t BgL_lenz00_1946;
					obj_t BgL_polyz00_1947;
					obj_t BgL_lsbzd2polyzd2_1948;

					{	/* Unsafe/crc.scm 357 */
						obj_t BgL_pairz00_3472;

						BgL_pairz00_3472 = CDR(((obj_t) BgL_crczd2desczd2_1945));
						BgL_lenz00_1946 = CAR(BgL_pairz00_3472);
					}
					{	/* Unsafe/crc.scm 358 */
						obj_t BgL_pairz00_3478;

						{	/* Unsafe/crc.scm 358 */
							obj_t BgL_pairz00_3477;

							BgL_pairz00_3477 = CDR(((obj_t) BgL_crczd2desczd2_1945));
							BgL_pairz00_3478 = CDR(BgL_pairz00_3477);
						}
						BgL_polyz00_1947 = CAR(BgL_pairz00_3478);
					}
					{	/* Unsafe/crc.scm 359 */
						obj_t BgL_pairz00_3486;

						{	/* Unsafe/crc.scm 359 */
							obj_t BgL_pairz00_3485;

							{	/* Unsafe/crc.scm 359 */
								obj_t BgL_pairz00_3484;

								BgL_pairz00_3484 = CDR(((obj_t) BgL_crczd2desczd2_1945));
								BgL_pairz00_3485 = CDR(BgL_pairz00_3484);
							}
							BgL_pairz00_3486 = CDR(BgL_pairz00_3485);
						}
						BgL_lsbzd2polyzd2_1948 = CAR(BgL_pairz00_3486);
					}
					{	/* Unsafe/crc.scm 360 */
						int BgL_tmpz00_7261;

						BgL_tmpz00_7261 = (int) (3L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7261);
					}
					{	/* Unsafe/crc.scm 360 */
						int BgL_tmpz00_7264;

						BgL_tmpz00_7264 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_7264, BgL_polyz00_1947);
					}
					{	/* Unsafe/crc.scm 360 */
						int BgL_tmpz00_7267;

						BgL_tmpz00_7267 = (int) (2L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_7267, BgL_lsbzd2polyzd2_1948);
					}
					return BgL_lenz00_1946;
				}
			}
		}

	}



/* crc-fast-port */
	obj_t BGl_crczd2fastzd2portz00zz__crcz00(obj_t BgL_namez00_73,
		obj_t BgL_pz00_74, obj_t BgL_initz00_75, obj_t BgL_finalzd2xorzd2_76,
		obj_t BgL_bigzd2endianzf3z21_77)
	{
		{	/* Unsafe/crc.scm 382 */
			{	/* Unsafe/crc.scm 383 */
				obj_t BgL_lenz00_1955;

				BgL_lenz00_1955 = BGl_getzd2crczd2zz__crcz00(BgL_namez00_73);
				{	/* Unsafe/crc.scm 384 */
					obj_t BgL_polyz00_1956;
					obj_t BgL_lsbzd2polyzd2_1957;

					{	/* Unsafe/crc.scm 386 */
						obj_t BgL_tmpz00_3493;

						{	/* Unsafe/crc.scm 386 */
							int BgL_tmpz00_7271;

							BgL_tmpz00_7271 = (int) (1L);
							BgL_tmpz00_3493 = BGL_MVALUES_VAL(BgL_tmpz00_7271);
						}
						{	/* Unsafe/crc.scm 386 */
							int BgL_tmpz00_7274;

							BgL_tmpz00_7274 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7274, BUNSPEC);
						}
						BgL_polyz00_1956 = BgL_tmpz00_3493;
					}
					{	/* Unsafe/crc.scm 386 */
						obj_t BgL_tmpz00_3494;

						{	/* Unsafe/crc.scm 386 */
							int BgL_tmpz00_7277;

							BgL_tmpz00_7277 = (int) (2L);
							BgL_tmpz00_3494 = BGL_MVALUES_VAL(BgL_tmpz00_7277);
						}
						{	/* Unsafe/crc.scm 386 */
							int BgL_tmpz00_7280;

							BgL_tmpz00_7280 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7280, BUNSPEC);
						}
						BgL_lsbzd2polyzd2_1957 = BgL_tmpz00_3494;
					}
					if (INTEGERP(BgL_polyz00_1956))
						{	/* Unsafe/crc.scm 386 */
							if (CBOOL(BgL_bigzd2endianzf3z21_77))
								{	/* Unsafe/crc.scm 388 */
									long BgL_res2154z00_3612;

									{	/* Unsafe/crc.scm 388 */
										long BgL_initz00_3496;
										long BgL_finalzd2xorzd2_3497;
										long BgL_polyz00_3498;
										long BgL_crczd2lenzd2_3499;

										BgL_initz00_3496 = (long) CINT(BgL_initz00_75);
										BgL_finalzd2xorzd2_3497 =
											(long) CINT(BgL_finalzd2xorzd2_76);
										BgL_polyz00_3498 = (long) CINT(BgL_polyz00_1956);
										BgL_crczd2lenzd2_3499 = (long) CINT(BgL_lenz00_1955);
										{	/* Unsafe/crc.scm 497 */
											long BgL_mz00_3500;

											BgL_mz00_3500 =
												(
												(1L <<
													(int) (
														(BgL_crczd2lenzd2_3499 - 1L))) +
												((1L << (int) ((BgL_crczd2lenzd2_3499 - 1L))) - 1L));
											{
												long BgL_crcz00_3507;

												BgL_crcz00_3507 = BgL_initz00_3496;
											BgL_loopz00_3506:
												{	/* Unsafe/crc.scm 504 */
													obj_t BgL_cz00_3508;

													BgL_cz00_3508 =
														BGl_readzd2charzd2zz__r4_input_6_10_2z00
														(BgL_pz00_74);
													if (EOF_OBJECTP(BgL_cz00_3508))
														{	/* Unsafe/crc.scm 505 */
															BgL_res2154z00_3612 =
																(
																(BgL_finalzd2xorzd2_3497 ^ BgL_crcz00_3507) &
																BgL_mz00_3500);
														}
													else
														{	/* Unsafe/crc.scm 507 */
															long BgL_arg1786z00_3511;

															{	/* Unsafe/crc.scm 507 */
																unsigned char BgL_cz00_3523;

																BgL_cz00_3523 = CCHAR(BgL_cz00_3508);
																{

																	if ((BgL_crczd2lenzd2_3499 >= 8L))
																		{	/* Unsafe/crc.scm 111 */
																			{	/* Unsafe/crc.scm 76 */
																				long BgL_mz00_3532;

																				BgL_mz00_3532 =
																					(1L <<
																					(int) ((BgL_crczd2lenzd2_3499 - 1L)));
																				{	/* Unsafe/crc.scm 77 */

																					{
																						long BgL_iz00_3536;
																						long BgL_crcz00_3537;

																						BgL_iz00_3536 = 0L;
																						BgL_crcz00_3537 =
																							(BgL_crcz00_3507 ^
																							(((unsigned char) (BgL_cz00_3523))
																								<< (int) ((BgL_crczd2lenzd2_3499
																										- 8L))));
																					BgL_loopz00_3535:
																						if ((BgL_iz00_3536 == 8L))
																							{	/* Unsafe/crc.scm 80 */
																								BgL_arg1786z00_3511 =
																									BgL_crcz00_3537;
																							}
																						else
																							{
																								long BgL_crcz00_7314;
																								long BgL_iz00_7312;

																								BgL_iz00_7312 =
																									(BgL_iz00_3536 + 1L);
																								BgL_crcz00_7314 =
																									(
																									(((BgL_mz00_3532 &
																												BgL_crcz00_3537) >>
																											(int) (
																												(BgL_crczd2lenzd2_3499 -
																													1L))) *
																										BgL_polyz00_3498) ^
																									(BgL_crcz00_3537 <<
																										(int) (1L)));
																								BgL_crcz00_3537 =
																									BgL_crcz00_7314;
																								BgL_iz00_3536 = BgL_iz00_7312;
																								goto BgL_loopz00_3535;
																							}
																					}
																				}
																			}
																		}
																	else
																		{	/* Unsafe/crc.scm 111 */
																			{	/* Unsafe/crc.scm 92 */
																				long BgL_mz00_3547;

																				BgL_mz00_3547 =
																					(1L <<
																					(int) ((BgL_crczd2lenzd2_3499 - 1L)));
																				{	/* Unsafe/crc.scm 94 */

																					{
																						long BgL_iz00_3551;
																						long BgL_crcz00_3552;
																						long BgL_shiftedzd2valuezd2_3553;

																						BgL_iz00_3551 = 0L;
																						BgL_crcz00_3552 = BgL_crcz00_3507;
																						BgL_shiftedzd2valuezd2_3553 =
																							(
																							((unsigned char) (BgL_cz00_3523))
																							<< (int) (BgL_crczd2lenzd2_3499));
																					BgL_loopz00_3550:
																						if ((BgL_iz00_3551 == 8L))
																							{	/* Unsafe/crc.scm 98 */
																								BgL_arg1786z00_3511 =
																									BgL_crcz00_3552;
																							}
																						else
																							{	/* Unsafe/crc.scm 100 */
																								long BgL_crc2z00_3555;

																								BgL_crc2z00_3555 =
																									(BgL_crcz00_3552 ^
																									(BgL_mz00_3547 &
																										(BgL_shiftedzd2valuezd2_3553
																											>> (int) (8L))));
																								{	/* Unsafe/crc.scm 104 */

																									{
																										long
																											BgL_shiftedzd2valuezd2_7349;
																										long BgL_crcz00_7340;
																										long BgL_iz00_7338;

																										BgL_iz00_7338 =
																											(BgL_iz00_3551 + 1L);
																										BgL_crcz00_7340 =
																											(
																											(((BgL_mz00_3547 &
																														BgL_crc2z00_3555) >>
																													(int) (
																														(BgL_crczd2lenzd2_3499
																															-
																															1L))) *
																												BgL_polyz00_3498) ^
																											(BgL_crc2z00_3555 <<
																												(int) (1L)));
																										BgL_shiftedzd2valuezd2_7349
																											=
																											(BgL_shiftedzd2valuezd2_3553
																											<< (int) (1L));
																										BgL_shiftedzd2valuezd2_3553
																											=
																											BgL_shiftedzd2valuezd2_7349;
																										BgL_crcz00_3552 =
																											BgL_crcz00_7340;
																										BgL_iz00_3551 =
																											BgL_iz00_7338;
																										goto BgL_loopz00_3550;
																									}
																								}
																							}
																					}
																				}
																			}
																		}
																}
															}
															{
																long BgL_crcz00_7356;

																BgL_crcz00_7356 = BgL_arg1786z00_3511;
																BgL_crcz00_3507 = BgL_crcz00_7356;
																goto BgL_loopz00_3506;
															}
														}
												}
											}
										}
									}
									return BINT(BgL_res2154z00_3612);
								}
							else
								{	/* Unsafe/crc.scm 389 */
									long BgL_res2156z00_3668;

									{	/* Unsafe/crc.scm 389 */
										long BgL_initz00_3614;
										long BgL_finalzd2xorzd2_3615;
										long BgL_polyz00_3616;
										long BgL_crczd2lenzd2_3617;

										BgL_initz00_3614 = (long) CINT(BgL_initz00_75);
										BgL_finalzd2xorzd2_3615 =
											(long) CINT(BgL_finalzd2xorzd2_76);
										BgL_polyz00_3616 = (long) CINT(BgL_lsbzd2polyzd2_1957);
										BgL_crczd2lenzd2_3617 = (long) CINT(BgL_lenz00_1955);
										{	/* Unsafe/crc.scm 497 */
											long BgL_mz00_3618;

											BgL_mz00_3618 =
												(
												(1L <<
													(int) (
														(BgL_crczd2lenzd2_3617 - 1L))) +
												((1L << (int) ((BgL_crczd2lenzd2_3617 - 1L))) - 1L));
											{
												long BgL_crcz00_3625;

												BgL_crcz00_3625 = BgL_initz00_3614;
											BgL_loopz00_3624:
												{	/* Unsafe/crc.scm 504 */
													obj_t BgL_cz00_3626;

													BgL_cz00_3626 =
														BGl_readzd2charzd2zz__r4_input_6_10_2z00
														(BgL_pz00_74);
													if (EOF_OBJECTP(BgL_cz00_3626))
														{	/* Unsafe/crc.scm 505 */
															BgL_res2156z00_3668 =
																(
																(BgL_finalzd2xorzd2_3615 ^ BgL_crcz00_3625) &
																BgL_mz00_3618);
														}
													else
														{	/* Unsafe/crc.scm 507 */
															long BgL_arg1786z00_3629;

															{
																long BgL_iz00_3648;
																long BgL_crcz00_3649;

																BgL_iz00_3648 = 0L;
																BgL_crcz00_3649 =
																	(BgL_crcz00_3625 ^
																	((unsigned char) (CCHAR(BgL_cz00_3626))));
															BgL_loopz00_3647:
																if ((BgL_iz00_3648 == 8L))
																	{	/* Unsafe/crc.scm 164 */
																		BgL_arg1786z00_3629 = BgL_crcz00_3649;
																	}
																else
																	{
																		long BgL_crcz00_7379;
																		long BgL_iz00_7377;

																		BgL_iz00_7377 = (BgL_iz00_3648 + 1L);
																		BgL_crcz00_7379 =
																			(
																			((1L & BgL_crcz00_3649) *
																				BgL_polyz00_3616) ^ (long) (((unsigned
																						long) (BgL_crcz00_3649) >>
																					(int) (1L))));
																		BgL_crcz00_3649 = BgL_crcz00_7379;
																		BgL_iz00_3648 = BgL_iz00_7377;
																		goto BgL_loopz00_3647;
																	}
															}
															{
																long BgL_crcz00_7391;

																BgL_crcz00_7391 = BgL_arg1786z00_3629;
																BgL_crcz00_3625 = BgL_crcz00_7391;
																goto BgL_loopz00_3624;
															}
														}
												}
											}
										}
									}
									return BINT(BgL_res2156z00_3668);
								}
						}
					else
						{	/* Unsafe/crc.scm 386 */
							if (ELONGP(BgL_polyz00_1956))
								{	/* Unsafe/crc.scm 390 */
									if (CBOOL(BgL_bigzd2endianzf3z21_77))
										{	/* Unsafe/crc.scm 392 */
											obj_t BgL_arg1726z00_1960;
											obj_t BgL_arg1727z00_1961;

											if (INTEGERP(BgL_initz00_75))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_3670;

													BgL_xz00_3670 = (long) CINT(BgL_initz00_75);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_7400;

														BgL_tmpz00_7400 = (long) (BgL_xz00_3670);
														BgL_arg1726z00_1960 = make_belong(BgL_tmpz00_7400);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1726z00_1960 = BgL_initz00_75;
												}
											if (INTEGERP(BgL_finalzd2xorzd2_76))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_3672;

													BgL_xz00_3672 = (long) CINT(BgL_finalzd2xorzd2_76);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_7406;

														BgL_tmpz00_7406 = (long) (BgL_xz00_3672);
														BgL_arg1727z00_1961 = make_belong(BgL_tmpz00_7406);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1727z00_1961 = BgL_finalzd2xorzd2_76;
												}
											{	/* Unsafe/crc.scm 392 */
												long BgL_res2160z00_3844;

												{	/* Unsafe/crc.scm 392 */
													long BgL_initz00_3674;
													long BgL_finalzd2xorzd2_3675;
													long BgL_polyz00_3676;
													long BgL_crczd2lenzd2_3677;

													BgL_initz00_3674 =
														BELONG_TO_LONG(BgL_arg1726z00_1960);
													BgL_finalzd2xorzd2_3675 =
														BELONG_TO_LONG(BgL_arg1727z00_1961);
													BgL_polyz00_3676 = BELONG_TO_LONG(BgL_polyz00_1956);
													BgL_crczd2lenzd2_3677 = (long) CINT(BgL_lenz00_1955);
													{	/* Unsafe/crc.scm 514 */
														long BgL_mz00_3678;

														{	/* Unsafe/crc.scm 517 */
															long BgL_tmpz00_7413;

															{	/* Unsafe/crc.scm 518 */
																long BgL_res2157z00_3698;

																{	/* Unsafe/crc.scm 518 */
																	long BgL_tmpz00_7417;

																	BgL_tmpz00_7417 =
																		(
																		(((long) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_3677 - 1L))) -
																		((long) 1));
																	BgL_res2157z00_3698 =
																		(long) (BgL_tmpz00_7417);
																}
																BgL_tmpz00_7413 = BgL_res2157z00_3698;
															}
															BgL_mz00_3678 =
																(
																(((long) 1) <<
																	(int) (
																		(BgL_crczd2lenzd2_3677 - 1L))) +
																BgL_tmpz00_7413);
														}
														{
															long BgL_crcz00_3685;

															BgL_crcz00_3685 = BgL_initz00_3674;
														BgL_loopz00_3684:
															{	/* Unsafe/crc.scm 521 */
																obj_t BgL_cz00_3686;

																BgL_cz00_3686 =
																	BGl_readzd2charzd2zz__r4_input_6_10_2z00
																	(BgL_pz00_74);
																if (EOF_OBJECTP(BgL_cz00_3686))
																	{	/* Unsafe/crc.scm 522 */
																		BgL_res2160z00_3844 =
																			(
																			(BgL_finalzd2xorzd2_3675 ^
																				BgL_crcz00_3685) & BgL_mz00_3678);
																	}
																else
																	{	/* Unsafe/crc.scm 524 */
																		long BgL_arg1795z00_3689;

																		{	/* Unsafe/crc.scm 524 */
																			long BgL_res2159z00_3843;

																			{	/* Unsafe/crc.scm 524 */
																				unsigned char BgL_cz00_3705;

																				BgL_cz00_3705 = CCHAR(BgL_cz00_3686);
																				if ((BgL_crczd2lenzd2_3677 >= 8L))
																					{	/* Unsafe/crc.scm 122 */
																						long BgL_octetz00_3710;

																						BgL_octetz00_3710 =
																							((unsigned char) (BgL_cz00_3705));
																						{	/* Unsafe/crc.scm 123 */
																							long BgL_valuez00_3711;
																							long BgL_mz00_3712;

																							{	/* Unsafe/crc.scm 123 */
																								long BgL_arg1501z00_3713;
																								long BgL_arg1502z00_3714;

																								BgL_arg1501z00_3713 =
																									(long) (BgL_octetz00_3710);
																								BgL_arg1502z00_3714 =
																									(BgL_crczd2lenzd2_3677 - 8L);
																								BgL_valuez00_3711 =
																									(BgL_arg1501z00_3713 <<
																									(int) (BgL_arg1502z00_3714));
																							}
																							BgL_mz00_3712 =
																								(((long) 1) <<
																								(int) (
																									(BgL_crczd2lenzd2_3677 -
																										1L)));
																							{
																								long BgL_iz00_3718;
																								long BgL_crcz00_3719;

																								BgL_iz00_3718 = 0L;
																								BgL_crcz00_3719 =
																									(BgL_crcz00_3685 ^
																									BgL_valuez00_3711);
																							BgL_loopz00_3717:
																								if ((BgL_iz00_3718 == 8L))
																									{	/* Unsafe/crc.scm 127 */
																										BgL_res2159z00_3843 =
																											BgL_crcz00_3719;
																									}
																								else
																									{	/* Unsafe/crc.scm 129 */
																										long BgL_newzd2crczd2_3721;

																										BgL_newzd2crczd2_3721 =
																											(BgL_crcz00_3719 <<
																											(int) (1L));
																										if (
																											(((long) 0) ==
																												(BgL_mz00_3712 &
																													BgL_crcz00_3719)))
																											{
																												long BgL_crcz00_7450;
																												long BgL_iz00_7448;

																												BgL_iz00_7448 =
																													(BgL_iz00_3718 + 1L);
																												BgL_crcz00_7450 =
																													BgL_newzd2crczd2_3721;
																												BgL_crcz00_3719 =
																													BgL_crcz00_7450;
																												BgL_iz00_3718 =
																													BgL_iz00_7448;
																												goto BgL_loopz00_3717;
																											}
																										else
																											{
																												long BgL_crcz00_7453;
																												long BgL_iz00_7451;

																												BgL_iz00_7451 =
																													(BgL_iz00_3718 + 1L);
																												BgL_crcz00_7453 =
																													(BgL_newzd2crczd2_3721
																													^ BgL_polyz00_3676);
																												BgL_crcz00_3719 =
																													BgL_crcz00_7453;
																												BgL_iz00_3718 =
																													BgL_iz00_7451;
																												goto BgL_loopz00_3717;
																											}
																									}
																							}
																						}
																					}
																				else
																					{	/* Unsafe/crc.scm 133 */
																						long BgL_arg1504z00_3727;

																						{	/* Unsafe/crc.scm 133 */
																							long BgL_arg1505z00_3728;
																							long BgL_arg1506z00_3729;

																							BgL_arg1505z00_3728 =
																								(long) (BgL_crcz00_3685);
																							BgL_arg1506z00_3729 =
																								(long) (BgL_polyz00_3676);
																							{

																								if (
																									(BgL_crczd2lenzd2_3677 >= 8L))
																									{	/* Unsafe/crc.scm 111 */
																										{	/* Unsafe/crc.scm 76 */
																											long BgL_mz00_3762;

																											BgL_mz00_3762 =
																												(1L <<
																												(int) (
																													(BgL_crczd2lenzd2_3677
																														- 1L)));
																											{	/* Unsafe/crc.scm 77 */

																												{
																													long BgL_iz00_3766;
																													long BgL_crcz00_3767;

																													BgL_iz00_3766 = 0L;
																													BgL_crcz00_3767 =
																														(BgL_arg1505z00_3728
																														^ (((unsigned
																																	char)
																																(BgL_cz00_3705))
																															<<
																															(int) (
																																(BgL_crczd2lenzd2_3677
																																	- 8L))));
																												BgL_loopz00_3765:
																													if (
																														(BgL_iz00_3766 ==
																															8L))
																														{	/* Unsafe/crc.scm 80 */
																															BgL_arg1504z00_3727
																																=
																																BgL_crcz00_3767;
																														}
																													else
																														{
																															long
																																BgL_crcz00_7467;
																															long
																																BgL_iz00_7465;
																															BgL_iz00_7465 =
																																(BgL_iz00_3766 +
																																1L);
																															BgL_crcz00_7467 =
																																((((BgL_mz00_3762 & BgL_crcz00_3767) >> (int) ((BgL_crczd2lenzd2_3677 - 1L))) * BgL_arg1506z00_3729) ^ (BgL_crcz00_3767 << (int) (1L)));
																															BgL_crcz00_3767 =
																																BgL_crcz00_7467;
																															BgL_iz00_3766 =
																																BgL_iz00_7465;
																															goto
																																BgL_loopz00_3765;
																														}
																												}
																											}
																										}
																									}
																								else
																									{	/* Unsafe/crc.scm 111 */
																										{	/* Unsafe/crc.scm 92 */
																											long BgL_mz00_3777;

																											BgL_mz00_3777 =
																												(1L <<
																												(int) (
																													(BgL_crczd2lenzd2_3677
																														- 1L)));
																											{	/* Unsafe/crc.scm 94 */

																												{
																													long BgL_iz00_3781;
																													long BgL_crcz00_3782;
																													long
																														BgL_shiftedzd2valuezd2_3783;
																													BgL_iz00_3781 = 0L;
																													BgL_crcz00_3782 =
																														BgL_arg1505z00_3728;
																													BgL_shiftedzd2valuezd2_3783
																														=
																														(((unsigned
																																char)
																															(BgL_cz00_3705))
																														<<
																														(int)
																														(BgL_crczd2lenzd2_3677));
																												BgL_loopz00_3780:
																													if (
																														(BgL_iz00_3781 ==
																															8L))
																														{	/* Unsafe/crc.scm 98 */
																															BgL_arg1504z00_3727
																																=
																																BgL_crcz00_3782;
																														}
																													else
																														{	/* Unsafe/crc.scm 100 */
																															long
																																BgL_crc2z00_3785;
																															BgL_crc2z00_3785 =
																																(BgL_crcz00_3782
																																^ (BgL_mz00_3777
																																	&
																																	(BgL_shiftedzd2valuezd2_3783
																																		>>
																																		(int)
																																		(8L))));
																															{	/* Unsafe/crc.scm 104 */

																																{
																																	long
																																		BgL_shiftedzd2valuezd2_7502;
																																	long
																																		BgL_crcz00_7493;
																																	long
																																		BgL_iz00_7491;
																																	BgL_iz00_7491
																																		=
																																		(BgL_iz00_3781
																																		+ 1L);
																																	BgL_crcz00_7493
																																		=
																																		((((BgL_mz00_3777 & BgL_crc2z00_3785) >> (int) ((BgL_crczd2lenzd2_3677 - 1L))) * BgL_arg1506z00_3729) ^ (BgL_crc2z00_3785 << (int) (1L)));
																																	BgL_shiftedzd2valuezd2_7502
																																		=
																																		(BgL_shiftedzd2valuezd2_3783
																																		<<
																																		(int) (1L));
																																	BgL_shiftedzd2valuezd2_3783
																																		=
																																		BgL_shiftedzd2valuezd2_7502;
																																	BgL_crcz00_3782
																																		=
																																		BgL_crcz00_7493;
																																	BgL_iz00_3781
																																		=
																																		BgL_iz00_7491;
																																	goto
																																		BgL_loopz00_3780;
																																}
																															}
																														}
																												}
																											}
																										}
																									}
																							}
																						}
																						BgL_res2159z00_3843 =
																							(long) (BgL_arg1504z00_3727);
																			}}
																			BgL_arg1795z00_3689 = BgL_res2159z00_3843;
																		}
																		{
																			long BgL_crcz00_7510;

																			BgL_crcz00_7510 = BgL_arg1795z00_3689;
																			BgL_crcz00_3685 = BgL_crcz00_7510;
																			goto BgL_loopz00_3684;
																		}
																	}
															}
														}
													}
												}
												return make_belong(BgL_res2160z00_3844);
											}
										}
									else
										{	/* Unsafe/crc.scm 394 */
											obj_t BgL_arg1728z00_1962;
											obj_t BgL_arg1729z00_1963;

											if (INTEGERP(BgL_initz00_75))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_3846;

													BgL_xz00_3846 = (long) CINT(BgL_initz00_75);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_7515;

														BgL_tmpz00_7515 = (long) (BgL_xz00_3846);
														BgL_arg1728z00_1962 = make_belong(BgL_tmpz00_7515);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1728z00_1962 = BgL_initz00_75;
												}
											if (INTEGERP(BgL_finalzd2xorzd2_76))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_3848;

													BgL_xz00_3848 = (long) CINT(BgL_finalzd2xorzd2_76);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_7521;

														BgL_tmpz00_7521 = (long) (BgL_xz00_3848);
														BgL_arg1729z00_1963 = make_belong(BgL_tmpz00_7521);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1729z00_1963 = BgL_finalzd2xorzd2_76;
												}
											{	/* Unsafe/crc.scm 394 */
												long BgL_res2163z00_3911;

												{	/* Unsafe/crc.scm 394 */
													long BgL_initz00_3850;
													long BgL_finalzd2xorzd2_3851;
													long BgL_polyz00_3852;
													long BgL_crczd2lenzd2_3853;

													BgL_initz00_3850 =
														BELONG_TO_LONG(BgL_arg1728z00_1962);
													BgL_finalzd2xorzd2_3851 =
														BELONG_TO_LONG(BgL_arg1729z00_1963);
													BgL_polyz00_3852 =
														BELONG_TO_LONG(BgL_lsbzd2polyzd2_1957);
													BgL_crczd2lenzd2_3853 = (long) CINT(BgL_lenz00_1955);
													{	/* Unsafe/crc.scm 514 */
														long BgL_mz00_3854;

														{	/* Unsafe/crc.scm 517 */
															long BgL_tmpz00_7528;

															{	/* Unsafe/crc.scm 518 */
																long BgL_res2161z00_3874;

																{	/* Unsafe/crc.scm 518 */
																	long BgL_tmpz00_7532;

																	BgL_tmpz00_7532 =
																		(
																		(((long) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_3853 - 1L))) -
																		((long) 1));
																	BgL_res2161z00_3874 =
																		(long) (BgL_tmpz00_7532);
																}
																BgL_tmpz00_7528 = BgL_res2161z00_3874;
															}
															BgL_mz00_3854 =
																(
																(((long) 1) <<
																	(int) (
																		(BgL_crczd2lenzd2_3853 - 1L))) +
																BgL_tmpz00_7528);
														}
														{
															long BgL_crcz00_3861;

															BgL_crcz00_3861 = BgL_initz00_3850;
														BgL_loopz00_3860:
															{	/* Unsafe/crc.scm 521 */
																obj_t BgL_cz00_3862;

																BgL_cz00_3862 =
																	BGl_readzd2charzd2zz__r4_input_6_10_2z00
																	(BgL_pz00_74);
																if (EOF_OBJECTP(BgL_cz00_3862))
																	{	/* Unsafe/crc.scm 522 */
																		BgL_res2163z00_3911 =
																			(
																			(BgL_finalzd2xorzd2_3851 ^
																				BgL_crcz00_3861) & BgL_mz00_3854);
																	}
																else
																	{	/* Unsafe/crc.scm 524 */
																		long BgL_arg1795z00_3865;

																		{	/* Unsafe/crc.scm 524 */
																			long BgL_res2162z00_3910;

																			{	/* Unsafe/crc.scm 524 */
																				unsigned char BgL_cz00_3881;

																				BgL_cz00_3881 = CCHAR(BgL_cz00_3862);
																				{	/* Unsafe/crc.scm 175 */
																					long BgL_octetz00_3885;

																					BgL_octetz00_3885 =
																						((unsigned char) (BgL_cz00_3881));
																					{	/* Unsafe/crc.scm 176 */
																						long BgL_g1043z00_3886;

																						{	/* Unsafe/crc.scm 177 */
																							long BgL_arg1546z00_3887;

																							BgL_arg1546z00_3887 =
																								(long) (BgL_octetz00_3885);
																							BgL_g1043z00_3886 =
																								(BgL_crcz00_3861 ^
																								BgL_arg1546z00_3887);
																						}
																						{
																							long BgL_iz00_3889;
																							long BgL_crcz00_3890;

																							BgL_iz00_3889 = 0L;
																							BgL_crcz00_3890 =
																								BgL_g1043z00_3886;
																						BgL_loopz00_3888:
																							if ((BgL_iz00_3889 == 8L))
																								{	/* Unsafe/crc.scm 178 */
																									BgL_res2162z00_3910 =
																										BgL_crcz00_3890;
																								}
																							else
																								{	/* Unsafe/crc.scm 180 */
																									long BgL_newzd2crczd2_3892;

																									{	/* Unsafe/crc.scm 180 */
																										unsigned long BgL_xz00_3902;

																										BgL_xz00_3902 =
																											(unsigned
																											long) (BgL_crcz00_3890);
																										{	/* Unsafe/crc.scm 180 */
																											unsigned long
																												BgL_tmpz00_7552;
																											BgL_tmpz00_7552 =
																												(BgL_xz00_3902 >>
																												(int) (1L));
																											BgL_newzd2crczd2_3892 =
																												(long)
																												(BgL_tmpz00_7552);
																									}}
																									{
																										long BgL_crcz00_7558;
																										long BgL_iz00_7556;

																										BgL_iz00_7556 =
																											(BgL_iz00_3889 + 1L);
																										BgL_crcz00_7558 =
																											(
																											((((long) 1) &
																													BgL_crcz00_3890) *
																												BgL_polyz00_3852) ^
																											BgL_newzd2crczd2_3892);
																										BgL_crcz00_3890 =
																											BgL_crcz00_7558;
																										BgL_iz00_3889 =
																											BgL_iz00_7556;
																										goto BgL_loopz00_3888;
																									}
																								}
																						}
																					}
																				}
																			}
																			BgL_arg1795z00_3865 = BgL_res2162z00_3910;
																		}
																		{
																			long BgL_crcz00_7562;

																			BgL_crcz00_7562 = BgL_arg1795z00_3865;
																			BgL_crcz00_3861 = BgL_crcz00_7562;
																			goto BgL_loopz00_3860;
																		}
																	}
															}
														}
													}
												}
												return make_belong(BgL_res2163z00_3911);
											}
										}
								}
							else
								{	/* Unsafe/crc.scm 390 */
									if (LLONGP(BgL_polyz00_1956))
										{	/* Unsafe/crc.scm 396 */
											if (CBOOL(BgL_bigzd2endianzf3z21_77))
												{	/* Unsafe/crc.scm 398 */
													obj_t BgL_arg1731z00_1965;
													obj_t BgL_arg1733z00_1966;

													if (INTEGERP(BgL_initz00_75))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_3914;

															BgL_xz00_3914 = (long) CINT(BgL_initz00_75);
															BgL_arg1731z00_1965 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_3914));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_initz00_75))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_3915;

																	BgL_xz00_3915 =
																		BELONG_TO_LONG(BgL_initz00_75);
																	BgL_arg1731z00_1965 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_3915));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1731z00_1965 = BgL_initz00_75;
																}
														}
													if (INTEGERP(BgL_finalzd2xorzd2_76))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_3918;

															BgL_xz00_3918 =
																(long) CINT(BgL_finalzd2xorzd2_76);
															BgL_arg1733z00_1966 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_3918));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_finalzd2xorzd2_76))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_3919;

																	BgL_xz00_3919 =
																		BELONG_TO_LONG(BgL_finalzd2xorzd2_76);
																	BgL_arg1733z00_1966 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_3919));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1733z00_1966 = BgL_finalzd2xorzd2_76;
																}
														}
													{	/* Unsafe/crc.scm 398 */
														BGL_LONGLONG_T BgL_res2166z00_4090;

														{	/* Unsafe/crc.scm 398 */
															BGL_LONGLONG_T BgL_initz00_3921;
															BGL_LONGLONG_T BgL_finalzd2xorzd2_3922;
															BGL_LONGLONG_T BgL_polyz00_3923;
															long BgL_crczd2lenzd2_3924;

															BgL_initz00_3921 =
																BLLONG_TO_LLONG(BgL_arg1731z00_1965);
															BgL_finalzd2xorzd2_3922 =
																BLLONG_TO_LLONG(BgL_arg1733z00_1966);
															BgL_polyz00_3923 =
																BLLONG_TO_LLONG(BgL_polyz00_1956);
															BgL_crczd2lenzd2_3924 =
																(long) CINT(BgL_lenz00_1955);
															{	/* Unsafe/crc.scm 531 */
																BGL_LONGLONG_T BgL_mz00_3925;

																BgL_mz00_3925 =
																	(
																	(((BGL_LONGLONG_T) 1) <<
																		(int) (
																			(BgL_crczd2lenzd2_3924 - 1L))) +
																	((((BGL_LONGLONG_T) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_3924 - 1L))) -
																		((BGL_LONGLONG_T) 1)));
																{
																	BGL_LONGLONG_T BgL_crcz00_3932;

																	BgL_crcz00_3932 = BgL_initz00_3921;
																BgL_loopz00_3931:
																	{	/* Unsafe/crc.scm 538 */
																		obj_t BgL_cz00_3933;

																		BgL_cz00_3933 =
																			BGl_readzd2charzd2zz__r4_input_6_10_2z00
																			(BgL_pz00_74);
																		if (EOF_OBJECTP(BgL_cz00_3933))
																			{	/* Unsafe/crc.scm 539 */
																				BgL_res2166z00_4090 =
																					(
																					(BgL_finalzd2xorzd2_3922 ^
																						BgL_crcz00_3932) & BgL_mz00_3925);
																			}
																		else
																			{	/* Unsafe/crc.scm 541 */
																				BGL_LONGLONG_T BgL_arg1804z00_3936;

																				{	/* Unsafe/crc.scm 541 */
																					BGL_LONGLONG_T BgL_res2165z00_4089;

																					{	/* Unsafe/crc.scm 541 */
																						unsigned char BgL_cz00_3951;

																						BgL_cz00_3951 =
																							CCHAR(BgL_cz00_3933);
																						if ((BgL_crczd2lenzd2_3924 >= 8L))
																							{	/* Unsafe/crc.scm 142 */
																								long BgL_octetz00_3956;

																								BgL_octetz00_3956 =
																									(
																									(unsigned
																										char) (BgL_cz00_3951));
																								{	/* Unsafe/crc.scm 143 */
																									BGL_LONGLONG_T
																										BgL_valuez00_3957;
																									BGL_LONGLONG_T BgL_mz00_3958;

																									{	/* Unsafe/crc.scm 143 */
																										BGL_LONGLONG_T
																											BgL_arg1521z00_3959;
																										long BgL_arg1522z00_3960;

																										BgL_arg1521z00_3959 =
																											LONG_TO_LLONG
																											(BgL_octetz00_3956);
																										BgL_arg1522z00_3960 =
																											(BgL_crczd2lenzd2_3924 -
																											8L);
																										BgL_valuez00_3957 =
																											(BgL_arg1521z00_3959 <<
																											(int)
																											(BgL_arg1522z00_3960));
																									}
																									BgL_mz00_3958 =
																										(((BGL_LONGLONG_T) 1) <<
																										(int) (
																											(BgL_crczd2lenzd2_3924 -
																												1L)));
																									{
																										long BgL_iz00_3964;
																										BGL_LONGLONG_T
																											BgL_crcz00_3965;
																										BgL_iz00_3964 = 0L;
																										BgL_crcz00_3965 =
																											(BgL_crcz00_3932 ^
																											BgL_valuez00_3957);
																									BgL_loopz00_3963:
																										if ((BgL_iz00_3964 == 8L))
																											{	/* Unsafe/crc.scm 147 */
																												BgL_res2165z00_4089 =
																													BgL_crcz00_3965;
																											}
																										else
																											{	/* Unsafe/crc.scm 149 */
																												BGL_LONGLONG_T
																													BgL_newzd2crczd2_3967;
																												BgL_newzd2crczd2_3967 =
																													(BgL_crcz00_3965 <<
																													(int) (1L));
																												if ((((BGL_LONGLONG_T)
																															0) ==
																														(BgL_mz00_3958 &
																															BgL_crcz00_3965)))
																													{
																														BGL_LONGLONG_T
																															BgL_crcz00_7626;
																														long BgL_iz00_7624;

																														BgL_iz00_7624 =
																															(BgL_iz00_3964 +
																															1L);
																														BgL_crcz00_7626 =
																															BgL_newzd2crczd2_3967;
																														BgL_crcz00_3965 =
																															BgL_crcz00_7626;
																														BgL_iz00_3964 =
																															BgL_iz00_7624;
																														goto
																															BgL_loopz00_3963;
																													}
																												else
																													{
																														BGL_LONGLONG_T
																															BgL_crcz00_7629;
																														long BgL_iz00_7627;

																														BgL_iz00_7627 =
																															(BgL_iz00_3964 +
																															1L);
																														BgL_crcz00_7629 =
																															(BgL_newzd2crczd2_3967
																															^
																															BgL_polyz00_3923);
																														BgL_crcz00_3965 =
																															BgL_crcz00_7629;
																														BgL_iz00_3964 =
																															BgL_iz00_7627;
																														goto
																															BgL_loopz00_3963;
																													}
																											}
																									}
																								}
																							}
																						else
																							{	/* Unsafe/crc.scm 153 */
																								long BgL_arg1524z00_3973;

																								{	/* Unsafe/crc.scm 153 */
																									long BgL_arg1525z00_3974;
																									long BgL_arg1526z00_3975;

																									BgL_arg1525z00_3974 =
																										LLONG_TO_LONG
																										(BgL_crcz00_3932);
																									BgL_arg1526z00_3975 =
																										LLONG_TO_LONG
																										(BgL_polyz00_3923);
																									{

																										if (
																											(BgL_crczd2lenzd2_3924 >=
																												8L))
																											{	/* Unsafe/crc.scm 111 */
																												{	/* Unsafe/crc.scm 76 */
																													long BgL_mz00_4008;

																													BgL_mz00_4008 =
																														(1L <<
																														(int) (
																															(BgL_crczd2lenzd2_3924
																																- 1L)));
																													{	/* Unsafe/crc.scm 77 */

																														{
																															long
																																BgL_iz00_4012;
																															long
																																BgL_crcz00_4013;
																															BgL_iz00_4012 =
																																0L;
																															BgL_crcz00_4013 =
																																(BgL_arg1525z00_3974
																																^ (((unsigned
																																			char)
																																		(BgL_cz00_3951))
																																	<<
																																	(int) (
																																		(BgL_crczd2lenzd2_3924
																																			- 8L))));
																														BgL_loopz00_4011:
																															if (
																																(BgL_iz00_4012
																																	== 8L))
																																{	/* Unsafe/crc.scm 80 */
																																	BgL_arg1524z00_3973
																																		=
																																		BgL_crcz00_4013;
																																}
																															else
																																{
																																	long
																																		BgL_crcz00_7643;
																																	long
																																		BgL_iz00_7641;
																																	BgL_iz00_7641
																																		=
																																		(BgL_iz00_4012
																																		+ 1L);
																																	BgL_crcz00_7643
																																		=
																																		((((BgL_mz00_4008 & BgL_crcz00_4013) >> (int) ((BgL_crczd2lenzd2_3924 - 1L))) * BgL_arg1526z00_3975) ^ (BgL_crcz00_4013 << (int) (1L)));
																																	BgL_crcz00_4013
																																		=
																																		BgL_crcz00_7643;
																																	BgL_iz00_4012
																																		=
																																		BgL_iz00_7641;
																																	goto
																																		BgL_loopz00_4011;
																																}
																														}
																													}
																												}
																											}
																										else
																											{	/* Unsafe/crc.scm 111 */
																												{	/* Unsafe/crc.scm 92 */
																													long BgL_mz00_4023;

																													BgL_mz00_4023 =
																														(1L <<
																														(int) (
																															(BgL_crczd2lenzd2_3924
																																- 1L)));
																													{	/* Unsafe/crc.scm 94 */

																														{
																															long
																																BgL_iz00_4027;
																															long
																																BgL_crcz00_4028;
																															long
																																BgL_shiftedzd2valuezd2_4029;
																															BgL_iz00_4027 =
																																0L;
																															BgL_crcz00_4028 =
																																BgL_arg1525z00_3974;
																															BgL_shiftedzd2valuezd2_4029
																																=
																																(((unsigned
																																		char)
																																	(BgL_cz00_3951))
																																<<
																																(int)
																																(BgL_crczd2lenzd2_3924));
																														BgL_loopz00_4026:
																															if (
																																(BgL_iz00_4027
																																	== 8L))
																																{	/* Unsafe/crc.scm 98 */
																																	BgL_arg1524z00_3973
																																		=
																																		BgL_crcz00_4028;
																																}
																															else
																																{	/* Unsafe/crc.scm 100 */
																																	long
																																		BgL_crc2z00_4031;
																																	BgL_crc2z00_4031
																																		=
																																		(BgL_crcz00_4028
																																		^
																																		(BgL_mz00_4023
																																			&
																																			(BgL_shiftedzd2valuezd2_4029
																																				>>
																																				(int)
																																				(8L))));
																																	{	/* Unsafe/crc.scm 104 */

																																		{
																																			long
																																				BgL_shiftedzd2valuezd2_7678;
																																			long
																																				BgL_crcz00_7669;
																																			long
																																				BgL_iz00_7667;
																																			BgL_iz00_7667
																																				=
																																				(BgL_iz00_4027
																																				+ 1L);
																																			BgL_crcz00_7669
																																				=
																																				((((BgL_mz00_4023 & BgL_crc2z00_4031) >> (int) ((BgL_crczd2lenzd2_3924 - 1L))) * BgL_arg1526z00_3975) ^ (BgL_crc2z00_4031 << (int) (1L)));
																																			BgL_shiftedzd2valuezd2_7678
																																				=
																																				(BgL_shiftedzd2valuezd2_4029
																																				<<
																																				(int)
																																				(1L));
																																			BgL_shiftedzd2valuezd2_4029
																																				=
																																				BgL_shiftedzd2valuezd2_7678;
																																			BgL_crcz00_4028
																																				=
																																				BgL_crcz00_7669;
																																			BgL_iz00_4027
																																				=
																																				BgL_iz00_7667;
																																			goto
																																				BgL_loopz00_4026;
																																		}
																																	}
																																}
																														}
																													}
																												}
																											}
																									}
																								}
																								BgL_res2165z00_4089 =
																									LONG_TO_LLONG
																									(BgL_arg1524z00_3973);
																							}
																					}
																					BgL_arg1804z00_3936 =
																						BgL_res2165z00_4089;
																				}
																				{
																					BGL_LONGLONG_T BgL_crcz00_7686;

																					BgL_crcz00_7686 = BgL_arg1804z00_3936;
																					BgL_crcz00_3932 = BgL_crcz00_7686;
																					goto BgL_loopz00_3931;
																				}
																			}
																	}
																}
															}
														}
														return make_bllong(BgL_res2166z00_4090);
													}
												}
											else
												{	/* Unsafe/crc.scm 400 */
													obj_t BgL_arg1734z00_1967;
													obj_t BgL_arg1735z00_1968;

													if (INTEGERP(BgL_initz00_75))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_4093;

															BgL_xz00_4093 = (long) CINT(BgL_initz00_75);
															BgL_arg1734z00_1967 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_4093));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_initz00_75))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_4094;

																	BgL_xz00_4094 =
																		BELONG_TO_LONG(BgL_initz00_75);
																	BgL_arg1734z00_1967 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_4094));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1734z00_1967 = BgL_initz00_75;
																}
														}
													if (INTEGERP(BgL_finalzd2xorzd2_76))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_4097;

															BgL_xz00_4097 =
																(long) CINT(BgL_finalzd2xorzd2_76);
															BgL_arg1735z00_1968 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_4097));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_finalzd2xorzd2_76))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_4098;

																	BgL_xz00_4098 =
																		BELONG_TO_LONG(BgL_finalzd2xorzd2_76);
																	BgL_arg1735z00_1968 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_4098));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1735z00_1968 = BgL_finalzd2xorzd2_76;
																}
														}
													{	/* Unsafe/crc.scm 400 */
														BGL_LONGLONG_T BgL_res2168z00_4160;

														{	/* Unsafe/crc.scm 400 */
															BGL_LONGLONG_T BgL_initz00_4100;
															BGL_LONGLONG_T BgL_finalzd2xorzd2_4101;
															BGL_LONGLONG_T BgL_polyz00_4102;
															long BgL_crczd2lenzd2_4103;

															BgL_initz00_4100 =
																BLLONG_TO_LLONG(BgL_arg1734z00_1967);
															BgL_finalzd2xorzd2_4101 =
																BLLONG_TO_LLONG(BgL_arg1735z00_1968);
															BgL_polyz00_4102 =
																BLLONG_TO_LLONG(BgL_lsbzd2polyzd2_1957);
															BgL_crczd2lenzd2_4103 =
																(long) CINT(BgL_lenz00_1955);
															{	/* Unsafe/crc.scm 531 */
																BGL_LONGLONG_T BgL_mz00_4104;

																BgL_mz00_4104 =
																	(
																	(((BGL_LONGLONG_T) 1) <<
																		(int) (
																			(BgL_crczd2lenzd2_4103 - 1L))) +
																	((((BGL_LONGLONG_T) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_4103 - 1L))) -
																		((BGL_LONGLONG_T) 1)));
																{
																	BGL_LONGLONG_T BgL_crcz00_4111;

																	BgL_crcz00_4111 = BgL_initz00_4100;
																BgL_loopz00_4110:
																	{	/* Unsafe/crc.scm 538 */
																		obj_t BgL_cz00_4112;

																		BgL_cz00_4112 =
																			BGl_readzd2charzd2zz__r4_input_6_10_2z00
																			(BgL_pz00_74);
																		if (EOF_OBJECTP(BgL_cz00_4112))
																			{	/* Unsafe/crc.scm 539 */
																				BgL_res2168z00_4160 =
																					(
																					(BgL_finalzd2xorzd2_4101 ^
																						BgL_crcz00_4111) & BgL_mz00_4104);
																			}
																		else
																			{	/* Unsafe/crc.scm 541 */
																				BGL_LONGLONG_T BgL_arg1804z00_4115;

																				{	/* Unsafe/crc.scm 541 */
																					BGL_LONGLONG_T BgL_res2167z00_4159;

																					{	/* Unsafe/crc.scm 541 */
																						unsigned char BgL_cz00_4130;

																						BgL_cz00_4130 =
																							CCHAR(BgL_cz00_4112);
																						{	/* Unsafe/crc.scm 189 */
																							long BgL_octetz00_4134;

																							BgL_octetz00_4134 =
																								(
																								(unsigned
																									char) (BgL_cz00_4130));
																							{	/* Unsafe/crc.scm 190 */
																								BGL_LONGLONG_T
																									BgL_g1044z00_4135;
																								{	/* Unsafe/crc.scm 191 */
																									BGL_LONGLONG_T
																										BgL_arg1555z00_4136;
																									BgL_arg1555z00_4136 =
																										LONG_TO_LLONG
																										(BgL_octetz00_4134);
																									BgL_g1044z00_4135 =
																										(BgL_crcz00_4111 ^
																										BgL_arg1555z00_4136);
																								}
																								{
																									long BgL_iz00_4138;
																									BGL_LONGLONG_T
																										BgL_crcz00_4139;
																									BgL_iz00_4138 = 0L;
																									BgL_crcz00_4139 =
																										BgL_g1044z00_4135;
																								BgL_loopz00_4137:
																									if ((BgL_iz00_4138 == 8L))
																										{	/* Unsafe/crc.scm 192 */
																											BgL_res2167z00_4159 =
																												BgL_crcz00_4139;
																										}
																									else
																										{	/* Unsafe/crc.scm 194 */
																											BGL_LONGLONG_T
																												BgL_newzd2crczd2_4141;
																											{	/* Unsafe/crc.scm 194 */
																												unsigned BGL_LONGLONG_T
																													BgL_xz00_4151;
																												BgL_xz00_4151 =
																													(unsigned
																													BGL_LONGLONG_T)
																													(BgL_crcz00_4139);
																												{	/* Unsafe/crc.scm 194 */
																													unsigned
																														BGL_LONGLONG_T
																														BgL_tmpz00_7733;
																													BgL_tmpz00_7733 =
																														(BgL_xz00_4151 >>
																														(int) (1L));
																													BgL_newzd2crczd2_4141
																														=
																														(BGL_LONGLONG_T)
																														(BgL_tmpz00_7733);
																											}}
																											{
																												BGL_LONGLONG_T
																													BgL_crcz00_7739;
																												long BgL_iz00_7737;

																												BgL_iz00_7737 =
																													(BgL_iz00_4138 + 1L);
																												BgL_crcz00_7739 =
																													(
																													((((BGL_LONGLONG_T) 1)
																															& BgL_crcz00_4139)
																														*
																														BgL_polyz00_4102) ^
																													BgL_newzd2crczd2_4141);
																												BgL_crcz00_4139 =
																													BgL_crcz00_7739;
																												BgL_iz00_4138 =
																													BgL_iz00_7737;
																												goto BgL_loopz00_4137;
																											}
																										}
																								}
																							}
																						}
																					}
																					BgL_arg1804z00_4115 =
																						BgL_res2167z00_4159;
																				}
																				{
																					BGL_LONGLONG_T BgL_crcz00_7743;

																					BgL_crcz00_7743 = BgL_arg1804z00_4115;
																					BgL_crcz00_4111 = BgL_crcz00_7743;
																					goto BgL_loopz00_4110;
																				}
																			}
																	}
																}
															}
														}
														return make_bllong(BgL_res2168z00_4160);
													}
												}
										}
									else
										{	/* Unsafe/crc.scm 396 */
											return
												BGl_errorz00zz__errorz00(BGl_symbol2355z00zz__crcz00,
												BGl_string2379z00zz__crcz00, BgL_polyz00_1956);
										}
								}
						}
				}
			}
		}

	}



/* crc-fast-mmap */
	obj_t BGl_crczd2fastzd2mmapz00zz__crcz00(obj_t BgL_namez00_78,
		obj_t BgL_mz00_79, obj_t BgL_initz00_80, obj_t BgL_finalzd2xorzd2_81,
		obj_t BgL_bigzd2endianzf3z21_82)
	{
		{	/* Unsafe/crc.scm 408 */
			{	/* Unsafe/crc.scm 409 */
				obj_t BgL_lenz00_1969;

				BgL_lenz00_1969 = BGl_getzd2crczd2zz__crcz00(BgL_namez00_78);
				{	/* Unsafe/crc.scm 410 */
					obj_t BgL_polyz00_1970;
					obj_t BgL_lsbzd2polyzd2_1971;

					{	/* Unsafe/crc.scm 412 */
						obj_t BgL_tmpz00_4161;

						{	/* Unsafe/crc.scm 412 */
							int BgL_tmpz00_7747;

							BgL_tmpz00_7747 = (int) (1L);
							BgL_tmpz00_4161 = BGL_MVALUES_VAL(BgL_tmpz00_7747);
						}
						{	/* Unsafe/crc.scm 412 */
							int BgL_tmpz00_7750;

							BgL_tmpz00_7750 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7750, BUNSPEC);
						}
						BgL_polyz00_1970 = BgL_tmpz00_4161;
					}
					{	/* Unsafe/crc.scm 412 */
						obj_t BgL_tmpz00_4162;

						{	/* Unsafe/crc.scm 412 */
							int BgL_tmpz00_7753;

							BgL_tmpz00_7753 = (int) (2L);
							BgL_tmpz00_4162 = BGL_MVALUES_VAL(BgL_tmpz00_7753);
						}
						{	/* Unsafe/crc.scm 412 */
							int BgL_tmpz00_7756;

							BgL_tmpz00_7756 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7756, BUNSPEC);
						}
						BgL_lsbzd2polyzd2_1971 = BgL_tmpz00_4162;
					}
					if (INTEGERP(BgL_polyz00_1970))
						{	/* Unsafe/crc.scm 412 */
							if (CBOOL(BgL_bigzd2endianzf3z21_82))
								{	/* Unsafe/crc.scm 414 */
									long BgL_res2170z00_4286;

									{	/* Unsafe/crc.scm 414 */
										long BgL_initz00_4163;
										long BgL_finalzd2xorzd2_4164;
										long BgL_polyz00_4165;
										long BgL_crczd2lenzd2_4166;

										BgL_initz00_4163 = (long) CINT(BgL_initz00_80);
										BgL_finalzd2xorzd2_4164 =
											(long) CINT(BgL_finalzd2xorzd2_81);
										BgL_polyz00_4165 = (long) CINT(BgL_polyz00_1970);
										BgL_crczd2lenzd2_4166 = (long) CINT(BgL_lenz00_1969);
										{	/* Unsafe/crc.scm 434 */
											long BgL_mz00_4167;

											BgL_mz00_4167 =
												(
												(1L <<
													(int) (
														(BgL_crczd2lenzd2_4166 - 1L))) +
												((1L << (int) ((BgL_crczd2lenzd2_4166 - 1L))) - 1L));
											{	/* Unsafe/crc.scm 443 */
												long BgL_lenz00_4173;

												BgL_lenz00_4173 = BGL_MMAP_LENGTH(BgL_mz00_79);
												{
													long BgL_iz00_4175;
													long BgL_crcz00_4176;

													BgL_iz00_4175 = 0L;
													BgL_crcz00_4176 = BgL_initz00_4163;
												BgL_loopz00_4174:
													{	/* Unsafe/crc.scm 446 */
														bool_t BgL_test2703z00_7776;

														{	/* Unsafe/crc.scm 446 */
															long BgL_n2z00_4191;

															BgL_n2z00_4191 = (long) (BgL_lenz00_4173);
															BgL_test2703z00_7776 =
																(BgL_iz00_4175 == BgL_n2z00_4191);
														}
														if (BgL_test2703z00_7776)
															{	/* Unsafe/crc.scm 446 */
																BgL_res2170z00_4286 =
																	(
																	(BgL_finalzd2xorzd2_4164 ^ BgL_crcz00_4176) &
																	BgL_mz00_4167);
															}
														else
															{	/* Unsafe/crc.scm 448 */
																long BgL_arg1750z00_4179;
																long BgL_arg1751z00_4180;

																BgL_arg1750z00_4179 = (BgL_iz00_4175 + 1L);
																{	/* Unsafe/crc.scm 449 */
																	unsigned char BgL_arg1752z00_4181;

																	{	/* Unsafe/crc.scm 449 */
																		long BgL_tmpz00_7782;

																		BgL_tmpz00_7782 = (long) (BgL_iz00_4175);
																		BgL_arg1752z00_4181 =
																			BGL_MMAP_REF(BgL_mz00_79,
																			BgL_tmpz00_7782);
																	}
																	{	/* Unsafe/crc.scm 449 */
																		unsigned char BgL_cz00_4197;

																		BgL_cz00_4197 =
																			(char) (BgL_arg1752z00_4181);
																		{

																			if ((BgL_crczd2lenzd2_4166 >= 8L))
																				{	/* Unsafe/crc.scm 111 */
																					{	/* Unsafe/crc.scm 76 */
																						long BgL_mz00_4206;

																						BgL_mz00_4206 =
																							(1L <<
																							(int) (
																								(BgL_crczd2lenzd2_4166 - 1L)));
																						{	/* Unsafe/crc.scm 77 */

																							{
																								long BgL_iz00_4210;
																								long BgL_crcz00_4211;

																								BgL_iz00_4210 = 0L;
																								BgL_crcz00_4211 =
																									(BgL_crcz00_4176 ^
																									(((unsigned
																												char) (BgL_cz00_4197))
																										<<
																										(int) (
																											(BgL_crczd2lenzd2_4166 -
																												8L))));
																							BgL_loopz00_4209:
																								if ((BgL_iz00_4210 == 8L))
																									{	/* Unsafe/crc.scm 80 */
																										BgL_arg1751z00_4180 =
																											BgL_crcz00_4211;
																									}
																								else
																									{
																										long BgL_crcz00_7795;
																										long BgL_iz00_7793;

																										BgL_iz00_7793 =
																											(BgL_iz00_4210 + 1L);
																										BgL_crcz00_7795 =
																											(
																											(((BgL_mz00_4206 &
																														BgL_crcz00_4211) >>
																													(int) (
																														(BgL_crczd2lenzd2_4166
																															-
																															1L))) *
																												BgL_polyz00_4165) ^
																											(BgL_crcz00_4211 <<
																												(int) (1L)));
																										BgL_crcz00_4211 =
																											BgL_crcz00_7795;
																										BgL_iz00_4210 =
																											BgL_iz00_7793;
																										goto BgL_loopz00_4209;
																									}
																							}
																						}
																					}
																				}
																			else
																				{	/* Unsafe/crc.scm 111 */
																					{	/* Unsafe/crc.scm 92 */
																						long BgL_mz00_4221;

																						BgL_mz00_4221 =
																							(1L <<
																							(int) (
																								(BgL_crczd2lenzd2_4166 - 1L)));
																						{	/* Unsafe/crc.scm 94 */

																							{
																								long BgL_iz00_4225;
																								long BgL_crcz00_4226;
																								long
																									BgL_shiftedzd2valuezd2_4227;
																								BgL_iz00_4225 = 0L;
																								BgL_crcz00_4226 =
																									BgL_crcz00_4176;
																								BgL_shiftedzd2valuezd2_4227 =
																									(((unsigned
																											char) (BgL_cz00_4197)) <<
																									(int)
																									(BgL_crczd2lenzd2_4166));
																							BgL_loopz00_4224:
																								if ((BgL_iz00_4225 == 8L))
																									{	/* Unsafe/crc.scm 98 */
																										BgL_arg1751z00_4180 =
																											BgL_crcz00_4226;
																									}
																								else
																									{	/* Unsafe/crc.scm 100 */
																										long BgL_crc2z00_4229;

																										BgL_crc2z00_4229 =
																											(BgL_crcz00_4226 ^
																											(BgL_mz00_4221 &
																												(BgL_shiftedzd2valuezd2_4227
																													>> (int) (8L))));
																										{	/* Unsafe/crc.scm 104 */

																											{
																												long
																													BgL_shiftedzd2valuezd2_7830;
																												long BgL_crcz00_7821;
																												long BgL_iz00_7819;

																												BgL_iz00_7819 =
																													(BgL_iz00_4225 + 1L);
																												BgL_crcz00_7821 =
																													(
																													(((BgL_mz00_4221 &
																																BgL_crc2z00_4229)
																															>>
																															(int) (
																																(BgL_crczd2lenzd2_4166
																																	-
																																	1L))) *
																														BgL_polyz00_4165) ^
																													(BgL_crc2z00_4229 <<
																														(int) (1L)));
																												BgL_shiftedzd2valuezd2_7830
																													=
																													(BgL_shiftedzd2valuezd2_4227
																													<< (int) (1L));
																												BgL_shiftedzd2valuezd2_4227
																													=
																													BgL_shiftedzd2valuezd2_7830;
																												BgL_crcz00_4226 =
																													BgL_crcz00_7821;
																												BgL_iz00_4225 =
																													BgL_iz00_7819;
																												goto BgL_loopz00_4224;
																											}
																										}
																									}
																							}
																						}
																					}
																				}
																		}
																	}
																}
																{
																	long BgL_crcz00_7838;
																	long BgL_iz00_7837;

																	BgL_iz00_7837 = BgL_arg1750z00_4179;
																	BgL_crcz00_7838 = BgL_arg1751z00_4180;
																	BgL_crcz00_4176 = BgL_crcz00_7838;
																	BgL_iz00_4175 = BgL_iz00_7837;
																	goto BgL_loopz00_4174;
																}
															}
													}
												}
											}
										}
									}
									return BINT(BgL_res2170z00_4286);
								}
							else
								{	/* Unsafe/crc.scm 415 */
									long BgL_res2172z00_4348;

									{	/* Unsafe/crc.scm 415 */
										long BgL_initz00_4287;
										long BgL_finalzd2xorzd2_4288;
										long BgL_polyz00_4289;
										long BgL_crczd2lenzd2_4290;

										BgL_initz00_4287 = (long) CINT(BgL_initz00_80);
										BgL_finalzd2xorzd2_4288 =
											(long) CINT(BgL_finalzd2xorzd2_81);
										BgL_polyz00_4289 = (long) CINT(BgL_lsbzd2polyzd2_1971);
										BgL_crczd2lenzd2_4290 = (long) CINT(BgL_lenz00_1969);
										{	/* Unsafe/crc.scm 434 */
											long BgL_mz00_4291;

											BgL_mz00_4291 =
												(
												(1L <<
													(int) (
														(BgL_crczd2lenzd2_4290 - 1L))) +
												((1L << (int) ((BgL_crczd2lenzd2_4290 - 1L))) - 1L));
											{	/* Unsafe/crc.scm 443 */
												long BgL_lenz00_4297;

												BgL_lenz00_4297 = BGL_MMAP_LENGTH(BgL_mz00_79);
												{
													long BgL_iz00_4299;
													long BgL_crcz00_4300;

													BgL_iz00_4299 = 0L;
													BgL_crcz00_4300 = BgL_initz00_4287;
												BgL_loopz00_4298:
													{	/* Unsafe/crc.scm 446 */
														bool_t BgL_test2707z00_7853;

														{	/* Unsafe/crc.scm 446 */
															long BgL_n2z00_4315;

															BgL_n2z00_4315 = (long) (BgL_lenz00_4297);
															BgL_test2707z00_7853 =
																(BgL_iz00_4299 == BgL_n2z00_4315);
														}
														if (BgL_test2707z00_7853)
															{	/* Unsafe/crc.scm 446 */
																BgL_res2172z00_4348 =
																	(
																	(BgL_finalzd2xorzd2_4288 ^ BgL_crcz00_4300) &
																	BgL_mz00_4291);
															}
														else
															{	/* Unsafe/crc.scm 448 */
																long BgL_arg1750z00_4303;
																long BgL_arg1751z00_4304;

																BgL_arg1750z00_4303 = (BgL_iz00_4299 + 1L);
																{	/* Unsafe/crc.scm 449 */
																	unsigned char BgL_arg1752z00_4305;

																	{	/* Unsafe/crc.scm 449 */
																		long BgL_tmpz00_7859;

																		BgL_tmpz00_7859 = (long) (BgL_iz00_4299);
																		BgL_arg1752z00_4305 =
																			BGL_MMAP_REF(BgL_mz00_79,
																			BgL_tmpz00_7859);
																	}
																	{
																		long BgL_iz00_4328;
																		long BgL_crcz00_4329;

																		BgL_iz00_4328 = 0L;
																		BgL_crcz00_4329 =
																			(BgL_crcz00_4300 ^
																			((unsigned char) (
																					(char) (BgL_arg1752z00_4305))));
																	BgL_loopz00_4327:
																		if ((BgL_iz00_4328 == 8L))
																			{	/* Unsafe/crc.scm 164 */
																				BgL_arg1751z00_4304 = BgL_crcz00_4329;
																			}
																		else
																			{
																				long BgL_crcz00_7866;
																				long BgL_iz00_7864;

																				BgL_iz00_7864 = (BgL_iz00_4328 + 1L);
																				BgL_crcz00_7866 =
																					(
																					((1L & BgL_crcz00_4329) *
																						BgL_polyz00_4289) ^
																					(long) (((unsigned
																								long) (BgL_crcz00_4329) >>
																							(int) (1L))));
																				BgL_crcz00_4329 = BgL_crcz00_7866;
																				BgL_iz00_4328 = BgL_iz00_7864;
																				goto BgL_loopz00_4327;
																			}
																	}
																}
																{
																	long BgL_crcz00_7879;
																	long BgL_iz00_7878;

																	BgL_iz00_7878 = BgL_arg1750z00_4303;
																	BgL_crcz00_7879 = BgL_arg1751z00_4304;
																	BgL_crcz00_4300 = BgL_crcz00_7879;
																	BgL_iz00_4299 = BgL_iz00_7878;
																	goto BgL_loopz00_4298;
																}
															}
													}
												}
											}
										}
									}
									return BINT(BgL_res2172z00_4348);
								}
						}
					else
						{	/* Unsafe/crc.scm 412 */
							if (ELONGP(BgL_polyz00_1970))
								{	/* Unsafe/crc.scm 416 */
									if (CBOOL(BgL_bigzd2endianzf3z21_82))
										{	/* Unsafe/crc.scm 418 */
											obj_t BgL_arg1738z00_1974;
											obj_t BgL_arg1739z00_1975;

											if (INTEGERP(BgL_initz00_80))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_4350;

													BgL_xz00_4350 = (long) CINT(BgL_initz00_80);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_7888;

														BgL_tmpz00_7888 = (long) (BgL_xz00_4350);
														BgL_arg1738z00_1974 = make_belong(BgL_tmpz00_7888);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1738z00_1974 = BgL_initz00_80;
												}
											if (INTEGERP(BgL_finalzd2xorzd2_81))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_4352;

													BgL_xz00_4352 = (long) CINT(BgL_finalzd2xorzd2_81);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_7894;

														BgL_tmpz00_7894 = (long) (BgL_xz00_4352);
														BgL_arg1739z00_1975 = make_belong(BgL_tmpz00_7894);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1739z00_1975 = BgL_finalzd2xorzd2_81;
												}
											{	/* Unsafe/crc.scm 418 */
												long BgL_res2176z00_4530;

												{	/* Unsafe/crc.scm 418 */
													long BgL_initz00_4353;
													long BgL_finalzd2xorzd2_4354;
													long BgL_polyz00_4355;
													long BgL_crczd2lenzd2_4356;

													BgL_initz00_4353 =
														BELONG_TO_LONG(BgL_arg1738z00_1974);
													BgL_finalzd2xorzd2_4354 =
														BELONG_TO_LONG(BgL_arg1739z00_1975);
													BgL_polyz00_4355 = BELONG_TO_LONG(BgL_polyz00_1970);
													BgL_crczd2lenzd2_4356 = (long) CINT(BgL_lenz00_1969);
													{	/* Unsafe/crc.scm 457 */
														long BgL_mz00_4357;

														{	/* Unsafe/crc.scm 460 */
															long BgL_tmpz00_7901;

															{	/* Unsafe/crc.scm 461 */
																long BgL_res2173z00_4380;

																{	/* Unsafe/crc.scm 461 */
																	long BgL_tmpz00_7905;

																	BgL_tmpz00_7905 =
																		(
																		(((long) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_4356 - 1L))) -
																		((long) 1));
																	BgL_res2173z00_4380 =
																		(long) (BgL_tmpz00_7905);
																}
																BgL_tmpz00_7901 = BgL_res2173z00_4380;
															}
															BgL_mz00_4357 =
																(
																(((long) 1) <<
																	(int) (
																		(BgL_crczd2lenzd2_4356 - 1L))) +
																BgL_tmpz00_7901);
														}
														{	/* Unsafe/crc.scm 463 */
															long BgL_lenz00_4363;

															BgL_lenz00_4363 = BGL_MMAP_LENGTH(BgL_mz00_79);
															{
																long BgL_iz00_4365;
																long BgL_crcz00_4366;

																BgL_iz00_4365 = 0L;
																BgL_crcz00_4366 = BgL_initz00_4353;
															BgL_loopz00_4364:
																{	/* Unsafe/crc.scm 466 */
																	bool_t BgL_test2713z00_7913;

																	{	/* Unsafe/crc.scm 466 */
																		long BgL_n2z00_4385;

																		BgL_n2z00_4385 = (long) (BgL_lenz00_4363);
																		BgL_test2713z00_7913 =
																			(BgL_iz00_4365 == BgL_n2z00_4385);
																	}
																	if (BgL_test2713z00_7913)
																		{	/* Unsafe/crc.scm 466 */
																			BgL_res2176z00_4530 =
																				(
																				(BgL_finalzd2xorzd2_4354 ^
																					BgL_crcz00_4366) & BgL_mz00_4357);
																		}
																	else
																		{	/* Unsafe/crc.scm 468 */
																			long BgL_arg1761z00_4369;
																			long BgL_arg1762z00_4370;

																			BgL_arg1761z00_4369 =
																				(BgL_iz00_4365 + 1L);
																			{	/* Unsafe/crc.scm 469 */
																				unsigned char BgL_arg1763z00_4371;

																				{	/* Unsafe/crc.scm 469 */
																					long BgL_tmpz00_7919;

																					BgL_tmpz00_7919 =
																						(long) (BgL_iz00_4365);
																					BgL_arg1763z00_4371 =
																						BGL_MMAP_REF(BgL_mz00_79,
																						BgL_tmpz00_7919);
																				}
																				{	/* Unsafe/crc.scm 469 */
																					long BgL_res2175z00_4529;

																					{	/* Unsafe/crc.scm 469 */
																						unsigned char BgL_cz00_4391;

																						BgL_cz00_4391 =
																							(char) (BgL_arg1763z00_4371);
																						if ((BgL_crczd2lenzd2_4356 >= 8L))
																							{	/* Unsafe/crc.scm 122 */
																								long BgL_octetz00_4396;

																								BgL_octetz00_4396 =
																									(
																									(unsigned
																										char) (BgL_cz00_4391));
																								{	/* Unsafe/crc.scm 123 */
																									long BgL_valuez00_4397;
																									long BgL_mz00_4398;

																									{	/* Unsafe/crc.scm 123 */
																										long BgL_arg1501z00_4399;
																										long BgL_arg1502z00_4400;

																										BgL_arg1501z00_4399 =
																											(long)
																											(BgL_octetz00_4396);
																										BgL_arg1502z00_4400 =
																											(BgL_crczd2lenzd2_4356 -
																											8L);
																										BgL_valuez00_4397 =
																											(BgL_arg1501z00_4399 <<
																											(int)
																											(BgL_arg1502z00_4400));
																									}
																									BgL_mz00_4398 =
																										(((long) 1) <<
																										(int) (
																											(BgL_crczd2lenzd2_4356 -
																												1L)));
																									{
																										long BgL_iz00_4404;
																										long BgL_crcz00_4405;

																										BgL_iz00_4404 = 0L;
																										BgL_crcz00_4405 =
																											(BgL_crcz00_4366 ^
																											BgL_valuez00_4397);
																									BgL_loopz00_4403:
																										if ((BgL_iz00_4404 == 8L))
																											{	/* Unsafe/crc.scm 127 */
																												BgL_res2175z00_4529 =
																													BgL_crcz00_4405;
																											}
																										else
																											{	/* Unsafe/crc.scm 129 */
																												long
																													BgL_newzd2crczd2_4407;
																												BgL_newzd2crczd2_4407 =
																													(BgL_crcz00_4405 <<
																													(int) (1L));
																												if ((((long) 0) ==
																														(BgL_mz00_4398 &
																															BgL_crcz00_4405)))
																													{
																														long
																															BgL_crcz00_7943;
																														long BgL_iz00_7941;

																														BgL_iz00_7941 =
																															(BgL_iz00_4404 +
																															1L);
																														BgL_crcz00_7943 =
																															BgL_newzd2crczd2_4407;
																														BgL_crcz00_4405 =
																															BgL_crcz00_7943;
																														BgL_iz00_4404 =
																															BgL_iz00_7941;
																														goto
																															BgL_loopz00_4403;
																													}
																												else
																													{
																														long
																															BgL_crcz00_7946;
																														long BgL_iz00_7944;

																														BgL_iz00_7944 =
																															(BgL_iz00_4404 +
																															1L);
																														BgL_crcz00_7946 =
																															(BgL_newzd2crczd2_4407
																															^
																															BgL_polyz00_4355);
																														BgL_crcz00_4405 =
																															BgL_crcz00_7946;
																														BgL_iz00_4404 =
																															BgL_iz00_7944;
																														goto
																															BgL_loopz00_4403;
																													}
																											}
																									}
																								}
																							}
																						else
																							{	/* Unsafe/crc.scm 133 */
																								long BgL_arg1504z00_4413;

																								{	/* Unsafe/crc.scm 133 */
																									long BgL_arg1505z00_4414;
																									long BgL_arg1506z00_4415;

																									BgL_arg1505z00_4414 =
																										(long) (BgL_crcz00_4366);
																									BgL_arg1506z00_4415 =
																										(long) (BgL_polyz00_4355);
																									{

																										if (
																											(BgL_crczd2lenzd2_4356 >=
																												8L))
																											{	/* Unsafe/crc.scm 111 */
																												{	/* Unsafe/crc.scm 76 */
																													long BgL_mz00_4448;

																													BgL_mz00_4448 =
																														(1L <<
																														(int) (
																															(BgL_crczd2lenzd2_4356
																																- 1L)));
																													{	/* Unsafe/crc.scm 77 */

																														{
																															long
																																BgL_iz00_4452;
																															long
																																BgL_crcz00_4453;
																															BgL_iz00_4452 =
																																0L;
																															BgL_crcz00_4453 =
																																(BgL_arg1505z00_4414
																																^ (((unsigned
																																			char)
																																		(BgL_cz00_4391))
																																	<<
																																	(int) (
																																		(BgL_crczd2lenzd2_4356
																																			- 8L))));
																														BgL_loopz00_4451:
																															if (
																																(BgL_iz00_4452
																																	== 8L))
																																{	/* Unsafe/crc.scm 80 */
																																	BgL_arg1504z00_4413
																																		=
																																		BgL_crcz00_4453;
																																}
																															else
																																{
																																	long
																																		BgL_crcz00_7960;
																																	long
																																		BgL_iz00_7958;
																																	BgL_iz00_7958
																																		=
																																		(BgL_iz00_4452
																																		+ 1L);
																																	BgL_crcz00_7960
																																		=
																																		((((BgL_mz00_4448 & BgL_crcz00_4453) >> (int) ((BgL_crczd2lenzd2_4356 - 1L))) * BgL_arg1506z00_4415) ^ (BgL_crcz00_4453 << (int) (1L)));
																																	BgL_crcz00_4453
																																		=
																																		BgL_crcz00_7960;
																																	BgL_iz00_4452
																																		=
																																		BgL_iz00_7958;
																																	goto
																																		BgL_loopz00_4451;
																																}
																														}
																													}
																												}
																											}
																										else
																											{	/* Unsafe/crc.scm 111 */
																												{	/* Unsafe/crc.scm 92 */
																													long BgL_mz00_4463;

																													BgL_mz00_4463 =
																														(1L <<
																														(int) (
																															(BgL_crczd2lenzd2_4356
																																- 1L)));
																													{	/* Unsafe/crc.scm 94 */

																														{
																															long
																																BgL_iz00_4467;
																															long
																																BgL_crcz00_4468;
																															long
																																BgL_shiftedzd2valuezd2_4469;
																															BgL_iz00_4467 =
																																0L;
																															BgL_crcz00_4468 =
																																BgL_arg1505z00_4414;
																															BgL_shiftedzd2valuezd2_4469
																																=
																																(((unsigned
																																		char)
																																	(BgL_cz00_4391))
																																<<
																																(int)
																																(BgL_crczd2lenzd2_4356));
																														BgL_loopz00_4466:
																															if (
																																(BgL_iz00_4467
																																	== 8L))
																																{	/* Unsafe/crc.scm 98 */
																																	BgL_arg1504z00_4413
																																		=
																																		BgL_crcz00_4468;
																																}
																															else
																																{	/* Unsafe/crc.scm 100 */
																																	long
																																		BgL_crc2z00_4471;
																																	BgL_crc2z00_4471
																																		=
																																		(BgL_crcz00_4468
																																		^
																																		(BgL_mz00_4463
																																			&
																																			(BgL_shiftedzd2valuezd2_4469
																																				>>
																																				(int)
																																				(8L))));
																																	{	/* Unsafe/crc.scm 104 */

																																		{
																																			long
																																				BgL_shiftedzd2valuezd2_7995;
																																			long
																																				BgL_crcz00_7986;
																																			long
																																				BgL_iz00_7984;
																																			BgL_iz00_7984
																																				=
																																				(BgL_iz00_4467
																																				+ 1L);
																																			BgL_crcz00_7986
																																				=
																																				((((BgL_mz00_4463 & BgL_crc2z00_4471) >> (int) ((BgL_crczd2lenzd2_4356 - 1L))) * BgL_arg1506z00_4415) ^ (BgL_crc2z00_4471 << (int) (1L)));
																																			BgL_shiftedzd2valuezd2_7995
																																				=
																																				(BgL_shiftedzd2valuezd2_4469
																																				<<
																																				(int)
																																				(1L));
																																			BgL_shiftedzd2valuezd2_4469
																																				=
																																				BgL_shiftedzd2valuezd2_7995;
																																			BgL_crcz00_4468
																																				=
																																				BgL_crcz00_7986;
																																			BgL_iz00_4467
																																				=
																																				BgL_iz00_7984;
																																			goto
																																				BgL_loopz00_4466;
																																		}
																																	}
																																}
																														}
																													}
																												}
																											}
																									}
																								}
																								BgL_res2175z00_4529 =
																									(long) (BgL_arg1504z00_4413);
																					}}
																					BgL_arg1762z00_4370 =
																						BgL_res2175z00_4529;
																			}}
																			{
																				long BgL_crcz00_8004;
																				long BgL_iz00_8003;

																				BgL_iz00_8003 = BgL_arg1761z00_4369;
																				BgL_crcz00_8004 = BgL_arg1762z00_4370;
																				BgL_crcz00_4366 = BgL_crcz00_8004;
																				BgL_iz00_4365 = BgL_iz00_8003;
																				goto BgL_loopz00_4364;
																			}
																		}
																}
															}
														}
													}
												}
												return make_belong(BgL_res2176z00_4530);
											}
										}
									else
										{	/* Unsafe/crc.scm 420 */
											obj_t BgL_arg1740z00_1976;
											obj_t BgL_arg1741z00_1977;

											if (INTEGERP(BgL_initz00_80))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_4532;

													BgL_xz00_4532 = (long) CINT(BgL_initz00_80);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_8009;

														BgL_tmpz00_8009 = (long) (BgL_xz00_4532);
														BgL_arg1740z00_1976 = make_belong(BgL_tmpz00_8009);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1740z00_1976 = BgL_initz00_80;
												}
											if (INTEGERP(BgL_finalzd2xorzd2_81))
												{	/* Unsafe/crc.scm 367 */
													long BgL_xz00_4534;

													BgL_xz00_4534 = (long) CINT(BgL_finalzd2xorzd2_81);
													{	/* Unsafe/crc.scm 367 */
														long BgL_tmpz00_8015;

														BgL_tmpz00_8015 = (long) (BgL_xz00_4534);
														BgL_arg1741z00_1977 = make_belong(BgL_tmpz00_8015);
												}}
											else
												{	/* Unsafe/crc.scm 366 */
													BgL_arg1741z00_1977 = BgL_finalzd2xorzd2_81;
												}
											{	/* Unsafe/crc.scm 420 */
												long BgL_res2179z00_4603;

												{	/* Unsafe/crc.scm 420 */
													long BgL_initz00_4535;
													long BgL_finalzd2xorzd2_4536;
													long BgL_polyz00_4537;
													long BgL_crczd2lenzd2_4538;

													BgL_initz00_4535 =
														BELONG_TO_LONG(BgL_arg1740z00_1976);
													BgL_finalzd2xorzd2_4536 =
														BELONG_TO_LONG(BgL_arg1741z00_1977);
													BgL_polyz00_4537 =
														BELONG_TO_LONG(BgL_lsbzd2polyzd2_1971);
													BgL_crczd2lenzd2_4538 = (long) CINT(BgL_lenz00_1969);
													{	/* Unsafe/crc.scm 457 */
														long BgL_mz00_4539;

														{	/* Unsafe/crc.scm 460 */
															long BgL_tmpz00_8022;

															{	/* Unsafe/crc.scm 461 */
																long BgL_res2177z00_4562;

																{	/* Unsafe/crc.scm 461 */
																	long BgL_tmpz00_8026;

																	BgL_tmpz00_8026 =
																		(
																		(((long) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_4538 - 1L))) -
																		((long) 1));
																	BgL_res2177z00_4562 =
																		(long) (BgL_tmpz00_8026);
																}
																BgL_tmpz00_8022 = BgL_res2177z00_4562;
															}
															BgL_mz00_4539 =
																(
																(((long) 1) <<
																	(int) (
																		(BgL_crczd2lenzd2_4538 - 1L))) +
																BgL_tmpz00_8022);
														}
														{	/* Unsafe/crc.scm 463 */
															long BgL_lenz00_4545;

															BgL_lenz00_4545 = BGL_MMAP_LENGTH(BgL_mz00_79);
															{
																long BgL_iz00_4547;
																long BgL_crcz00_4548;

																BgL_iz00_4547 = 0L;
																BgL_crcz00_4548 = BgL_initz00_4535;
															BgL_loopz00_4546:
																{	/* Unsafe/crc.scm 466 */
																	bool_t BgL_test2722z00_8034;

																	{	/* Unsafe/crc.scm 466 */
																		long BgL_n2z00_4567;

																		BgL_n2z00_4567 = (long) (BgL_lenz00_4545);
																		BgL_test2722z00_8034 =
																			(BgL_iz00_4547 == BgL_n2z00_4567);
																	}
																	if (BgL_test2722z00_8034)
																		{	/* Unsafe/crc.scm 466 */
																			BgL_res2179z00_4603 =
																				(
																				(BgL_finalzd2xorzd2_4536 ^
																					BgL_crcz00_4548) & BgL_mz00_4539);
																		}
																	else
																		{	/* Unsafe/crc.scm 468 */
																			long BgL_arg1761z00_4551;
																			long BgL_arg1762z00_4552;

																			BgL_arg1761z00_4551 =
																				(BgL_iz00_4547 + 1L);
																			{	/* Unsafe/crc.scm 469 */
																				unsigned char BgL_arg1763z00_4553;

																				{	/* Unsafe/crc.scm 469 */
																					long BgL_tmpz00_8040;

																					BgL_tmpz00_8040 =
																						(long) (BgL_iz00_4547);
																					BgL_arg1763z00_4553 =
																						BGL_MMAP_REF(BgL_mz00_79,
																						BgL_tmpz00_8040);
																				}
																				{	/* Unsafe/crc.scm 469 */
																					long BgL_res2178z00_4602;

																					{	/* Unsafe/crc.scm 469 */
																						unsigned char BgL_cz00_4573;

																						BgL_cz00_4573 =
																							(char) (BgL_arg1763z00_4553);
																						{	/* Unsafe/crc.scm 175 */
																							long BgL_octetz00_4577;

																							BgL_octetz00_4577 =
																								(
																								(unsigned
																									char) (BgL_cz00_4573));
																							{	/* Unsafe/crc.scm 176 */
																								long BgL_g1043z00_4578;

																								{	/* Unsafe/crc.scm 177 */
																									long BgL_arg1546z00_4579;

																									BgL_arg1546z00_4579 =
																										(long) (BgL_octetz00_4577);
																									BgL_g1043z00_4578 =
																										(BgL_crcz00_4548 ^
																										BgL_arg1546z00_4579);
																								}
																								{
																									long BgL_iz00_4581;
																									long BgL_crcz00_4582;

																									BgL_iz00_4581 = 0L;
																									BgL_crcz00_4582 =
																										BgL_g1043z00_4578;
																								BgL_loopz00_4580:
																									if ((BgL_iz00_4581 == 8L))
																										{	/* Unsafe/crc.scm 178 */
																											BgL_res2178z00_4602 =
																												BgL_crcz00_4582;
																										}
																									else
																										{	/* Unsafe/crc.scm 180 */
																											long
																												BgL_newzd2crczd2_4584;
																											{	/* Unsafe/crc.scm 180 */
																												unsigned long
																													BgL_xz00_4594;
																												BgL_xz00_4594 =
																													(unsigned
																													long)
																													(BgL_crcz00_4582);
																												{	/* Unsafe/crc.scm 180 */
																													unsigned long
																														BgL_tmpz00_8051;
																													BgL_tmpz00_8051 =
																														(BgL_xz00_4594 >>
																														(int) (1L));
																													BgL_newzd2crczd2_4584
																														=
																														(long)
																														(BgL_tmpz00_8051);
																											}}
																											{
																												long BgL_crcz00_8057;
																												long BgL_iz00_8055;

																												BgL_iz00_8055 =
																													(BgL_iz00_4581 + 1L);
																												BgL_crcz00_8057 =
																													(
																													((((long) 1) &
																															BgL_crcz00_4582) *
																														BgL_polyz00_4537) ^
																													BgL_newzd2crczd2_4584);
																												BgL_crcz00_4582 =
																													BgL_crcz00_8057;
																												BgL_iz00_4581 =
																													BgL_iz00_8055;
																												goto BgL_loopz00_4580;
																											}
																										}
																								}
																							}
																						}
																					}
																					BgL_arg1762z00_4552 =
																						BgL_res2178z00_4602;
																				}
																			}
																			{
																				long BgL_crcz00_8062;
																				long BgL_iz00_8061;

																				BgL_iz00_8061 = BgL_arg1761z00_4551;
																				BgL_crcz00_8062 = BgL_arg1762z00_4552;
																				BgL_crcz00_4548 = BgL_crcz00_8062;
																				BgL_iz00_4547 = BgL_iz00_8061;
																				goto BgL_loopz00_4546;
																			}
																		}
																}
															}
														}
													}
												}
												return make_belong(BgL_res2179z00_4603);
											}
										}
								}
							else
								{	/* Unsafe/crc.scm 416 */
									if (LLONGP(BgL_polyz00_1970))
										{	/* Unsafe/crc.scm 422 */
											if (CBOOL(BgL_bigzd2endianzf3z21_82))
												{	/* Unsafe/crc.scm 424 */
													obj_t BgL_arg1743z00_1979;
													obj_t BgL_arg1744z00_1980;

													if (INTEGERP(BgL_initz00_80))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_4606;

															BgL_xz00_4606 = (long) CINT(BgL_initz00_80);
															BgL_arg1743z00_1979 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_4606));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_initz00_80))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_4607;

																	BgL_xz00_4607 =
																		BELONG_TO_LONG(BgL_initz00_80);
																	BgL_arg1743z00_1979 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_4607));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1743z00_1979 = BgL_initz00_80;
																}
														}
													if (INTEGERP(BgL_finalzd2xorzd2_81))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_4610;

															BgL_xz00_4610 =
																(long) CINT(BgL_finalzd2xorzd2_81);
															BgL_arg1744z00_1980 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_4610));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_finalzd2xorzd2_81))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_4611;

																	BgL_xz00_4611 =
																		BELONG_TO_LONG(BgL_finalzd2xorzd2_81);
																	BgL_arg1744z00_1980 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_4611));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1744z00_1980 = BgL_finalzd2xorzd2_81;
																}
														}
													{	/* Unsafe/crc.scm 424 */
														BGL_LONGLONG_T BgL_res2182z00_4788;

														{	/* Unsafe/crc.scm 424 */
															BGL_LONGLONG_T BgL_initz00_4612;
															BGL_LONGLONG_T BgL_finalzd2xorzd2_4613;
															BGL_LONGLONG_T BgL_polyz00_4614;
															long BgL_crczd2lenzd2_4615;

															BgL_initz00_4612 =
																BLLONG_TO_LLONG(BgL_arg1743z00_1979);
															BgL_finalzd2xorzd2_4613 =
																BLLONG_TO_LLONG(BgL_arg1744z00_1980);
															BgL_polyz00_4614 =
																BLLONG_TO_LLONG(BgL_polyz00_1970);
															BgL_crczd2lenzd2_4615 =
																(long) CINT(BgL_lenz00_1969);
															{	/* Unsafe/crc.scm 477 */
																BGL_LONGLONG_T BgL_mz00_4616;

																BgL_mz00_4616 =
																	(
																	(((BGL_LONGLONG_T) 1) <<
																		(int) (
																			(BgL_crczd2lenzd2_4615 - 1L))) +
																	((((BGL_LONGLONG_T) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_4615 - 1L))) -
																		((BGL_LONGLONG_T) 1)));
																{	/* Unsafe/crc.scm 483 */
																	long BgL_lenz00_4622;

																	BgL_lenz00_4622 =
																		BGL_MMAP_LENGTH(BgL_mz00_79);
																	{
																		long BgL_iz00_4624;
																		BGL_LONGLONG_T BgL_crcz00_4625;

																		BgL_iz00_4624 = 0L;
																		BgL_crcz00_4625 = BgL_initz00_4612;
																	BgL_loopz00_4623:
																		{	/* Unsafe/crc.scm 486 */
																			bool_t BgL_test2730z00_8101;

																			{	/* Unsafe/crc.scm 486 */
																				long BgL_n2z00_4643;

																				BgL_n2z00_4643 =
																					(long) (BgL_lenz00_4622);
																				BgL_test2730z00_8101 =
																					(BgL_iz00_4624 == BgL_n2z00_4643);
																			}
																			if (BgL_test2730z00_8101)
																				{	/* Unsafe/crc.scm 486 */
																					BgL_res2182z00_4788 =
																						(
																						(BgL_finalzd2xorzd2_4613 ^
																							BgL_crcz00_4625) & BgL_mz00_4616);
																				}
																			else
																				{	/* Unsafe/crc.scm 488 */
																					long BgL_arg1772z00_4628;
																					BGL_LONGLONG_T BgL_arg1773z00_4629;

																					BgL_arg1772z00_4628 =
																						(BgL_iz00_4624 + 1L);
																					{	/* Unsafe/crc.scm 489 */
																						unsigned char BgL_arg1774z00_4630;

																						{	/* Unsafe/crc.scm 489 */
																							long BgL_tmpz00_8107;

																							BgL_tmpz00_8107 =
																								(long) (BgL_iz00_4624);
																							BgL_arg1774z00_4630 =
																								BGL_MMAP_REF(BgL_mz00_79,
																								BgL_tmpz00_8107);
																						}
																						{	/* Unsafe/crc.scm 489 */
																							BGL_LONGLONG_T
																								BgL_res2181z00_4787;
																							{	/* Unsafe/crc.scm 489 */
																								unsigned char BgL_cz00_4649;

																								BgL_cz00_4649 =
																									(char) (BgL_arg1774z00_4630);
																								if (
																									(BgL_crczd2lenzd2_4615 >= 8L))
																									{	/* Unsafe/crc.scm 142 */
																										long BgL_octetz00_4654;

																										BgL_octetz00_4654 =
																											(
																											(unsigned
																												char) (BgL_cz00_4649));
																										{	/* Unsafe/crc.scm 143 */
																											BGL_LONGLONG_T
																												BgL_valuez00_4655;
																											BGL_LONGLONG_T
																												BgL_mz00_4656;
																											{	/* Unsafe/crc.scm 143 */
																												BGL_LONGLONG_T
																													BgL_arg1521z00_4657;
																												long
																													BgL_arg1522z00_4658;
																												BgL_arg1521z00_4657 =
																													LONG_TO_LLONG
																													(BgL_octetz00_4654);
																												BgL_arg1522z00_4658 =
																													(BgL_crczd2lenzd2_4615
																													- 8L);
																												BgL_valuez00_4655 =
																													(BgL_arg1521z00_4657
																													<<
																													(int)
																													(BgL_arg1522z00_4658));
																											}
																											BgL_mz00_4656 =
																												(((BGL_LONGLONG_T) 1) <<
																												(int) (
																													(BgL_crczd2lenzd2_4615
																														- 1L)));
																											{
																												long BgL_iz00_4662;
																												BGL_LONGLONG_T
																													BgL_crcz00_4663;
																												BgL_iz00_4662 = 0L;
																												BgL_crcz00_4663 =
																													(BgL_crcz00_4625 ^
																													BgL_valuez00_4655);
																											BgL_loopz00_4661:
																												if (
																													(BgL_iz00_4662 == 8L))
																													{	/* Unsafe/crc.scm 147 */
																														BgL_res2181z00_4787
																															= BgL_crcz00_4663;
																													}
																												else
																													{	/* Unsafe/crc.scm 149 */
																														BGL_LONGLONG_T
																															BgL_newzd2crczd2_4665;
																														BgL_newzd2crczd2_4665
																															=
																															(BgL_crcz00_4663
																															<< (int) (1L));
																														if ((((BGL_LONGLONG_T) 0) == (BgL_mz00_4656 & BgL_crcz00_4663)))
																															{
																																BGL_LONGLONG_T
																																	BgL_crcz00_8131;
																																long
																																	BgL_iz00_8129;
																																BgL_iz00_8129 =
																																	(BgL_iz00_4662
																																	+ 1L);
																																BgL_crcz00_8131
																																	=
																																	BgL_newzd2crczd2_4665;
																																BgL_crcz00_4663
																																	=
																																	BgL_crcz00_8131;
																																BgL_iz00_4662 =
																																	BgL_iz00_8129;
																																goto
																																	BgL_loopz00_4661;
																															}
																														else
																															{
																																BGL_LONGLONG_T
																																	BgL_crcz00_8134;
																																long
																																	BgL_iz00_8132;
																																BgL_iz00_8132 =
																																	(BgL_iz00_4662
																																	+ 1L);
																																BgL_crcz00_8134
																																	=
																																	(BgL_newzd2crczd2_4665
																																	^
																																	BgL_polyz00_4614);
																																BgL_crcz00_4663
																																	=
																																	BgL_crcz00_8134;
																																BgL_iz00_4662 =
																																	BgL_iz00_8132;
																																goto
																																	BgL_loopz00_4661;
																															}
																													}
																											}
																										}
																									}
																								else
																									{	/* Unsafe/crc.scm 153 */
																										long BgL_arg1524z00_4671;

																										{	/* Unsafe/crc.scm 153 */
																											long BgL_arg1525z00_4672;
																											long BgL_arg1526z00_4673;

																											BgL_arg1525z00_4672 =
																												LLONG_TO_LONG
																												(BgL_crcz00_4625);
																											BgL_arg1526z00_4673 =
																												LLONG_TO_LONG
																												(BgL_polyz00_4614);
																											{

																												if (
																													(BgL_crczd2lenzd2_4615
																														>= 8L))
																													{	/* Unsafe/crc.scm 111 */
																														{	/* Unsafe/crc.scm 76 */
																															long
																																BgL_mz00_4706;
																															BgL_mz00_4706 =
																																(1L <<
																																(int) (
																																	(BgL_crczd2lenzd2_4615
																																		- 1L)));
																															{	/* Unsafe/crc.scm 77 */

																																{
																																	long
																																		BgL_iz00_4710;
																																	long
																																		BgL_crcz00_4711;
																																	BgL_iz00_4710
																																		= 0L;
																																	BgL_crcz00_4711
																																		=
																																		(BgL_arg1525z00_4672
																																		^
																																		(((unsigned
																																					char)
																																				(BgL_cz00_4649))
																																			<<
																																			(int) (
																																				(BgL_crczd2lenzd2_4615
																																					-
																																					8L))));
																																BgL_loopz00_4709:
																																	if (
																																		(BgL_iz00_4710
																																			== 8L))
																																		{	/* Unsafe/crc.scm 80 */
																																			BgL_arg1524z00_4671
																																				=
																																				BgL_crcz00_4711;
																																		}
																																	else
																																		{
																																			long
																																				BgL_crcz00_8148;
																																			long
																																				BgL_iz00_8146;
																																			BgL_iz00_8146
																																				=
																																				(BgL_iz00_4710
																																				+ 1L);
																																			BgL_crcz00_8148
																																				=
																																				((((BgL_mz00_4706 & BgL_crcz00_4711) >> (int) ((BgL_crczd2lenzd2_4615 - 1L))) * BgL_arg1526z00_4673) ^ (BgL_crcz00_4711 << (int) (1L)));
																																			BgL_crcz00_4711
																																				=
																																				BgL_crcz00_8148;
																																			BgL_iz00_4710
																																				=
																																				BgL_iz00_8146;
																																			goto
																																				BgL_loopz00_4709;
																																		}
																																}
																															}
																														}
																													}
																												else
																													{	/* Unsafe/crc.scm 111 */
																														{	/* Unsafe/crc.scm 92 */
																															long
																																BgL_mz00_4721;
																															BgL_mz00_4721 =
																																(1L <<
																																(int) (
																																	(BgL_crczd2lenzd2_4615
																																		- 1L)));
																															{	/* Unsafe/crc.scm 94 */

																																{
																																	long
																																		BgL_iz00_4725;
																																	long
																																		BgL_crcz00_4726;
																																	long
																																		BgL_shiftedzd2valuezd2_4727;
																																	BgL_iz00_4725
																																		= 0L;
																																	BgL_crcz00_4726
																																		=
																																		BgL_arg1525z00_4672;
																																	BgL_shiftedzd2valuezd2_4727
																																		=
																																		(((unsigned
																																				char)
																																			(BgL_cz00_4649))
																																		<<
																																		(int)
																																		(BgL_crczd2lenzd2_4615));
																																BgL_loopz00_4724:
																																	if (
																																		(BgL_iz00_4725
																																			== 8L))
																																		{	/* Unsafe/crc.scm 98 */
																																			BgL_arg1524z00_4671
																																				=
																																				BgL_crcz00_4726;
																																		}
																																	else
																																		{	/* Unsafe/crc.scm 100 */
																																			long
																																				BgL_crc2z00_4729;
																																			BgL_crc2z00_4729
																																				=
																																				(BgL_crcz00_4726
																																				^
																																				(BgL_mz00_4721
																																					&
																																					(BgL_shiftedzd2valuezd2_4727
																																						>>
																																						(int)
																																						(8L))));
																																			{	/* Unsafe/crc.scm 104 */

																																				{
																																					long
																																						BgL_shiftedzd2valuezd2_8183;
																																					long
																																						BgL_crcz00_8174;
																																					long
																																						BgL_iz00_8172;
																																					BgL_iz00_8172
																																						=
																																						(BgL_iz00_4725
																																						+
																																						1L);
																																					BgL_crcz00_8174
																																						=
																																						((((BgL_mz00_4721 & BgL_crc2z00_4729) >> (int) ((BgL_crczd2lenzd2_4615 - 1L))) * BgL_arg1526z00_4673) ^ (BgL_crc2z00_4729 << (int) (1L)));
																																					BgL_shiftedzd2valuezd2_8183
																																						=
																																						(BgL_shiftedzd2valuezd2_4727
																																						<<
																																						(int)
																																						(1L));
																																					BgL_shiftedzd2valuezd2_4727
																																						=
																																						BgL_shiftedzd2valuezd2_8183;
																																					BgL_crcz00_4726
																																						=
																																						BgL_crcz00_8174;
																																					BgL_iz00_4725
																																						=
																																						BgL_iz00_8172;
																																					goto
																																						BgL_loopz00_4724;
																																				}
																																			}
																																		}
																																}
																															}
																														}
																													}
																											}
																										}
																										BgL_res2181z00_4787 =
																											LONG_TO_LLONG
																											(BgL_arg1524z00_4671);
																									}
																							}
																							BgL_arg1773z00_4629 =
																								BgL_res2181z00_4787;
																						}
																					}
																					{
																						BGL_LONGLONG_T BgL_crcz00_8192;
																						long BgL_iz00_8191;

																						BgL_iz00_8191 = BgL_arg1772z00_4628;
																						BgL_crcz00_8192 =
																							BgL_arg1773z00_4629;
																						BgL_crcz00_4625 = BgL_crcz00_8192;
																						BgL_iz00_4624 = BgL_iz00_8191;
																						goto BgL_loopz00_4623;
																					}
																				}
																		}
																	}
																}
															}
														}
														return make_bllong(BgL_res2182z00_4788);
													}
												}
											else
												{	/* Unsafe/crc.scm 426 */
													obj_t BgL_arg1745z00_1981;
													obj_t BgL_arg1746z00_1982;

													if (INTEGERP(BgL_initz00_80))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_4791;

															BgL_xz00_4791 = (long) CINT(BgL_initz00_80);
															BgL_arg1745z00_1981 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_4791));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_initz00_80))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_4792;

																	BgL_xz00_4792 =
																		BELONG_TO_LONG(BgL_initz00_80);
																	BgL_arg1745z00_1981 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_4792));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1745z00_1981 = BgL_initz00_80;
																}
														}
													if (INTEGERP(BgL_finalzd2xorzd2_81))
														{	/* Unsafe/crc.scm 375 */
															long BgL_xz00_4795;

															BgL_xz00_4795 =
																(long) CINT(BgL_finalzd2xorzd2_81);
															BgL_arg1746z00_1982 =
																make_bllong(LONG_TO_LLONG(BgL_xz00_4795));
														}
													else
														{	/* Unsafe/crc.scm 375 */
															if (ELONGP(BgL_finalzd2xorzd2_81))
																{	/* Unsafe/crc.scm 376 */
																	long BgL_xz00_4796;

																	BgL_xz00_4796 =
																		BELONG_TO_LONG(BgL_finalzd2xorzd2_81);
																	BgL_arg1746z00_1982 =
																		make_bllong(
																		(BGL_LONGLONG_T) (BgL_xz00_4796));
																}
															else
																{	/* Unsafe/crc.scm 376 */
																	BgL_arg1746z00_1982 = BgL_finalzd2xorzd2_81;
																}
														}
													{	/* Unsafe/crc.scm 426 */
														BGL_LONGLONG_T BgL_res2184z00_4864;

														{	/* Unsafe/crc.scm 426 */
															BGL_LONGLONG_T BgL_initz00_4797;
															BGL_LONGLONG_T BgL_finalzd2xorzd2_4798;
															BGL_LONGLONG_T BgL_polyz00_4799;
															long BgL_crczd2lenzd2_4800;

															BgL_initz00_4797 =
																BLLONG_TO_LLONG(BgL_arg1745z00_1981);
															BgL_finalzd2xorzd2_4798 =
																BLLONG_TO_LLONG(BgL_arg1746z00_1982);
															BgL_polyz00_4799 =
																BLLONG_TO_LLONG(BgL_lsbzd2polyzd2_1971);
															BgL_crczd2lenzd2_4800 =
																(long) CINT(BgL_lenz00_1969);
															{	/* Unsafe/crc.scm 477 */
																BGL_LONGLONG_T BgL_mz00_4801;

																BgL_mz00_4801 =
																	(
																	(((BGL_LONGLONG_T) 1) <<
																		(int) (
																			(BgL_crczd2lenzd2_4800 - 1L))) +
																	((((BGL_LONGLONG_T) 1) <<
																			(int) (
																				(BgL_crczd2lenzd2_4800 - 1L))) -
																		((BGL_LONGLONG_T) 1)));
																{	/* Unsafe/crc.scm 483 */
																	long BgL_lenz00_4807;

																	BgL_lenz00_4807 =
																		BGL_MMAP_LENGTH(BgL_mz00_79);
																	{
																		long BgL_iz00_4809;
																		BGL_LONGLONG_T BgL_crcz00_4810;

																		BgL_iz00_4809 = 0L;
																		BgL_crcz00_4810 = BgL_initz00_4797;
																	BgL_loopz00_4808:
																		{	/* Unsafe/crc.scm 486 */
																			bool_t BgL_test2741z00_8227;

																			{	/* Unsafe/crc.scm 486 */
																				long BgL_n2z00_4828;

																				BgL_n2z00_4828 =
																					(long) (BgL_lenz00_4807);
																				BgL_test2741z00_8227 =
																					(BgL_iz00_4809 == BgL_n2z00_4828);
																			}
																			if (BgL_test2741z00_8227)
																				{	/* Unsafe/crc.scm 486 */
																					BgL_res2184z00_4864 =
																						(
																						(BgL_finalzd2xorzd2_4798 ^
																							BgL_crcz00_4810) & BgL_mz00_4801);
																				}
																			else
																				{	/* Unsafe/crc.scm 488 */
																					long BgL_arg1772z00_4813;
																					BGL_LONGLONG_T BgL_arg1773z00_4814;

																					BgL_arg1772z00_4813 =
																						(BgL_iz00_4809 + 1L);
																					{	/* Unsafe/crc.scm 489 */
																						unsigned char BgL_arg1774z00_4815;

																						{	/* Unsafe/crc.scm 489 */
																							long BgL_tmpz00_8233;

																							BgL_tmpz00_8233 =
																								(long) (BgL_iz00_4809);
																							BgL_arg1774z00_4815 =
																								BGL_MMAP_REF(BgL_mz00_79,
																								BgL_tmpz00_8233);
																						}
																						{	/* Unsafe/crc.scm 489 */
																							BGL_LONGLONG_T
																								BgL_res2183z00_4863;
																							{	/* Unsafe/crc.scm 489 */
																								unsigned char BgL_cz00_4834;

																								BgL_cz00_4834 =
																									(char) (BgL_arg1774z00_4815);
																								{	/* Unsafe/crc.scm 189 */
																									long BgL_octetz00_4838;

																									BgL_octetz00_4838 =
																										(
																										(unsigned
																											char) (BgL_cz00_4834));
																									{	/* Unsafe/crc.scm 190 */
																										BGL_LONGLONG_T
																											BgL_g1044z00_4839;
																										{	/* Unsafe/crc.scm 191 */
																											BGL_LONGLONG_T
																												BgL_arg1555z00_4840;
																											BgL_arg1555z00_4840 =
																												LONG_TO_LLONG
																												(BgL_octetz00_4838);
																											BgL_g1044z00_4839 =
																												(BgL_crcz00_4810 ^
																												BgL_arg1555z00_4840);
																										}
																										{
																											long BgL_iz00_4842;
																											BGL_LONGLONG_T
																												BgL_crcz00_4843;
																											BgL_iz00_4842 = 0L;
																											BgL_crcz00_4843 =
																												BgL_g1044z00_4839;
																										BgL_loopz00_4841:
																											if ((BgL_iz00_4842 == 8L))
																												{	/* Unsafe/crc.scm 192 */
																													BgL_res2183z00_4863 =
																														BgL_crcz00_4843;
																												}
																											else
																												{	/* Unsafe/crc.scm 194 */
																													BGL_LONGLONG_T
																														BgL_newzd2crczd2_4845;
																													{	/* Unsafe/crc.scm 194 */
																														unsigned
																															BGL_LONGLONG_T
																															BgL_xz00_4855;
																														BgL_xz00_4855 =
																															(unsigned
																															BGL_LONGLONG_T)
																															(BgL_crcz00_4843);
																														{	/* Unsafe/crc.scm 194 */
																															unsigned
																																BGL_LONGLONG_T
																																BgL_tmpz00_8244;
																															BgL_tmpz00_8244 =
																																(BgL_xz00_4855
																																>> (int) (1L));
																															BgL_newzd2crczd2_4845
																																=
																																(BGL_LONGLONG_T)
																																(BgL_tmpz00_8244);
																													}}
																													{
																														BGL_LONGLONG_T
																															BgL_crcz00_8250;
																														long BgL_iz00_8248;

																														BgL_iz00_8248 =
																															(BgL_iz00_4842 +
																															1L);
																														BgL_crcz00_8250 =
																															(((((BGL_LONGLONG_T) 1) & BgL_crcz00_4843) * BgL_polyz00_4799) ^ BgL_newzd2crczd2_4845);
																														BgL_crcz00_4843 =
																															BgL_crcz00_8250;
																														BgL_iz00_4842 =
																															BgL_iz00_8248;
																														goto
																															BgL_loopz00_4841;
																													}
																												}
																										}
																									}
																								}
																							}
																							BgL_arg1773z00_4814 =
																								BgL_res2183z00_4863;
																						}
																					}
																					{
																						BGL_LONGLONG_T BgL_crcz00_8255;
																						long BgL_iz00_8254;

																						BgL_iz00_8254 = BgL_arg1772z00_4813;
																						BgL_crcz00_8255 =
																							BgL_arg1773z00_4814;
																						BgL_crcz00_4810 = BgL_crcz00_8255;
																						BgL_iz00_4809 = BgL_iz00_8254;
																						goto BgL_loopz00_4808;
																					}
																				}
																		}
																	}
																}
															}
														}
														return make_bllong(BgL_res2184z00_4864);
													}
												}
										}
									else
										{	/* Unsafe/crc.scm 422 */
											return
												BGl_errorz00zz__errorz00(BGl_symbol2355z00zz__crcz00,
												BGl_string2379z00zz__crcz00, BgL_polyz00_1970);
										}
								}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__crcz00(void)
	{
		{	/* Unsafe/crc.scm 15 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
