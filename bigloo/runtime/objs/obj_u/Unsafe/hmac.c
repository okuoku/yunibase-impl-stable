/*===========================================================================*/
/*   (Unsafe/hmac.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/hmac.scm -indent -o objs/obj_u/Unsafe/hmac.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___HMAC_TYPE_DEFINITIONS
#define BGL___HMAC_TYPE_DEFINITIONS
#endif													// BGL___HMAC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zz__hmacz00 = BUNSPEC;
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__hmacz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__base64z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	static obj_t BGl_genericzd2initzd2zz__hmacz00(void);
	extern obj_t BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__hmacz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__hmacz00(void);
	static obj_t BGl_objectzd2initzd2zz__hmacz00(void);
	BGL_EXPORTED_DECL obj_t BGl_hmaczd2stringzd2zz__hmacz00(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__hmacz00(void);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	extern obj_t make_string(long, unsigned char);
	extern obj_t string_append(obj_t, obj_t);
	extern obj_t make_string_sans_fill(long);
	static obj_t BGl_z62hmaczd2stringzb0zz__hmacz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1631z00zz__hmacz00,
		BgL_bgl_string1631za700za7za7_1636za7,
		"/tmp/bigloo/runtime/Unsafe/hmac.scm", 35);
	      DEFINE_STRING(BGl_string1632z00zz__hmacz00,
		BgL_bgl_string1632za700za7za7_1637za7, "&hmac-string", 12);
	      DEFINE_STRING(BGl_string1633z00zz__hmacz00,
		BgL_bgl_string1633za700za7za7_1638za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1634z00zz__hmacz00,
		BgL_bgl_string1634za700za7za7_1639za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1635z00zz__hmacz00,
		BgL_bgl_string1635za700za7za7_1640za7, "__hmac", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_hmaczd2stringzd2envz00zz__hmacz00,
		BgL_bgl_za762hmacza7d2string1641z00, BGl_z62hmaczd2stringzb0zz__hmacz00, 0L,
		BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__hmacz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__hmacz00(long
		BgL_checksumz00_2016, char *BgL_fromz00_2017)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__hmacz00))
				{
					BGl_requirezd2initializa7ationz75zz__hmacz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__hmacz00();
					BGl_importedzd2moduleszd2initz00zz__hmacz00();
					return BGl_methodzd2initzd2zz__hmacz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__hmacz00(void)
	{
		{	/* Unsafe/hmac.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* hmac-string */
	BGL_EXPORTED_DEF obj_t BGl_hmaczd2stringzd2zz__hmacz00(obj_t BgL_keyz00_3,
		obj_t BgL_messagez00_4, obj_t BgL_hashz00_5)
	{
		{	/* Unsafe/hmac.scm 57 */
			{	/* Unsafe/hmac.scm 61 */
				obj_t BgL_keybz00_1284;

				BgL_keybz00_1284 = make_string(64L, ((unsigned char) '\000'));
				{	/* Unsafe/hmac.scm 63 */
					obj_t BgL_ixorz00_1285;

					BgL_ixorz00_1285 = make_string_sans_fill(64L);
					{	/* Unsafe/hmac.scm 64 */
						obj_t BgL_oxorz00_1286;

						BgL_oxorz00_1286 = make_string_sans_fill(64L);
						{	/* Unsafe/hmac.scm 65 */

							{	/* Unsafe/hmac.scm 67 */
								long BgL_keyzd2lengthzd2_1287;

								BgL_keyzd2lengthzd2_1287 = STRING_LENGTH(BgL_keyz00_3);
								if ((BgL_keyzd2lengthzd2_1287 > 64L))
									{	/* Unsafe/hmac.scm 69 */
										obj_t BgL_arg1191z00_1289;

										{	/* Unsafe/hmac.scm 69 */
											obj_t BgL_arg1193z00_1290;

											BgL_arg1193z00_1290 =
												BGL_PROCEDURE_CALL1(BgL_hashz00_5, BgL_keyz00_3);
											BgL_arg1191z00_1289 =
												BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00
												(BgL_arg1193z00_1290);
										}
										blit_string(BgL_arg1191z00_1289, 0L, BgL_keybz00_1284, 0L,
											16L);
									}
								else
									{	/* Unsafe/hmac.scm 68 */
										blit_string(BgL_keyz00_3, 0L, BgL_keybz00_1284, 0L,
											BgL_keyzd2lengthzd2_1287);
									}
							}
							{
								long BgL_iz00_1292;

								BgL_iz00_1292 = 0L;
							BgL_zc3z04anonymousza31194ze3z87_1293:
								if ((BgL_iz00_1292 < 64L))
									{	/* Unsafe/hmac.scm 74 */
										long BgL_xiz00_1295;

										BgL_xiz00_1295 =
											(STRING_REF(BgL_keybz00_1284, BgL_iz00_1292));
										{	/* Unsafe/hmac.scm 75 */
											unsigned char BgL_tmpz00_2042;

											BgL_tmpz00_2042 = ((54L ^ BgL_xiz00_1295));
											STRING_SET(BgL_ixorz00_1285, BgL_iz00_1292,
												BgL_tmpz00_2042);
										}
										{	/* Unsafe/hmac.scm 76 */
											unsigned char BgL_tmpz00_2046;

											BgL_tmpz00_2046 = ((92L ^ BgL_xiz00_1295));
											STRING_SET(BgL_oxorz00_1286, BgL_iz00_1292,
												BgL_tmpz00_2046);
										}
										{
											long BgL_iz00_2050;

											BgL_iz00_2050 = (BgL_iz00_1292 + 1L);
											BgL_iz00_1292 = BgL_iz00_2050;
											goto BgL_zc3z04anonymousza31194ze3z87_1293;
										}
									}
								else
									{	/* Unsafe/hmac.scm 73 */
										((bool_t) 0);
									}
							}
							{	/* Unsafe/hmac.scm 82 */
								obj_t BgL_arg1202z00_1303;

								{	/* Unsafe/hmac.scm 82 */
									obj_t BgL_arg1203z00_1304;

									{	/* Unsafe/hmac.scm 82 */
										obj_t BgL_arg1206z00_1305;

										{	/* Unsafe/hmac.scm 82 */
											obj_t BgL_arg1208z00_1306;

											BgL_arg1208z00_1306 =
												string_append(BgL_ixorz00_1285, BgL_messagez00_4);
											BgL_arg1206z00_1305 =
												BGL_PROCEDURE_CALL1(BgL_hashz00_5, BgL_arg1208z00_1306);
										}
										BgL_arg1203z00_1304 =
											BGl_stringzd2hexzd2internz12z12zz__r4_strings_6_7z00
											(BgL_arg1206z00_1305);
									}
									BgL_arg1202z00_1303 =
										string_append(BgL_oxorz00_1286, BgL_arg1203z00_1304);
								}
								return BGL_PROCEDURE_CALL1(BgL_hashz00_5, BgL_arg1202z00_1303);
							}
						}
					}
				}
			}
		}

	}



/* &hmac-string */
	obj_t BGl_z62hmaczd2stringzb0zz__hmacz00(obj_t BgL_envz00_2006,
		obj_t BgL_keyz00_2007, obj_t BgL_messagez00_2008, obj_t BgL_hashz00_2009)
	{
		{	/* Unsafe/hmac.scm 57 */
			{	/* Unsafe/hmac.scm 59 */
				obj_t BgL_auxz00_2077;
				obj_t BgL_auxz00_2070;
				obj_t BgL_auxz00_2063;

				if (PROCEDUREP(BgL_hashz00_2009))
					{	/* Unsafe/hmac.scm 59 */
						BgL_auxz00_2077 = BgL_hashz00_2009;
					}
				else
					{
						obj_t BgL_auxz00_2080;

						BgL_auxz00_2080 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1631z00zz__hmacz00,
							BINT(2036L), BGl_string1632z00zz__hmacz00,
							BGl_string1634z00zz__hmacz00, BgL_hashz00_2009);
						FAILURE(BgL_auxz00_2080, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_messagez00_2008))
					{	/* Unsafe/hmac.scm 59 */
						BgL_auxz00_2070 = BgL_messagez00_2008;
					}
				else
					{
						obj_t BgL_auxz00_2073;

						BgL_auxz00_2073 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1631z00zz__hmacz00,
							BINT(2036L), BGl_string1632z00zz__hmacz00,
							BGl_string1633z00zz__hmacz00, BgL_messagez00_2008);
						FAILURE(BgL_auxz00_2073, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_keyz00_2007))
					{	/* Unsafe/hmac.scm 59 */
						BgL_auxz00_2063 = BgL_keyz00_2007;
					}
				else
					{
						obj_t BgL_auxz00_2066;

						BgL_auxz00_2066 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1631z00zz__hmacz00,
							BINT(2036L), BGl_string1632z00zz__hmacz00,
							BGl_string1633z00zz__hmacz00, BgL_keyz00_2007);
						FAILURE(BgL_auxz00_2066, BFALSE, BFALSE);
					}
				return
					BGl_hmaczd2stringzd2zz__hmacz00(BgL_auxz00_2063, BgL_auxz00_2070,
					BgL_auxz00_2077);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__hmacz00(void)
	{
		{	/* Unsafe/hmac.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__hmacz00(void)
	{
		{	/* Unsafe/hmac.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__hmacz00(void)
	{
		{	/* Unsafe/hmac.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__hmacz00(void)
	{
		{	/* Unsafe/hmac.scm 15 */
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1635z00zz__hmacz00));
			return
				BGl_modulezd2initializa7ationz75zz__base64z00(67813413L,
				BSTRING_TO_STRING(BGl_string1635z00zz__hmacz00));
		}

	}

#ifdef __cplusplus
}
#endif
