/*===========================================================================*/
/*   (Unsafe/uuid.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/uuid.scm -indent -o objs/obj_u/Unsafe/uuid.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___UUID_TYPE_DEFINITIONS
#define BGL___UUID_TYPE_DEFINITIONS
#endif													// BGL___UUID_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	static obj_t BGl_list1735z00zz__uuidz00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__uuidz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__uuidz00(long,
		char *);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__uuidz00(void);
	static obj_t BGl_genericzd2initzd2zz__uuidz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__uuidz00(void);
	static obj_t BGl_objectzd2initzd2zz__uuidz00(void);
	static obj_t BGl_vector1734z00zz__uuidz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__uuidz00(void);
	extern long bgl_current_seconds(void);
	extern obj_t make_string_sans_fill(long);
	BGL_EXPORTED_DECL obj_t BGl_genuuidz00zz__uuidz00(void);
	static obj_t BGl_z62genuuidz62zz__uuidz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_genuuidzd2envzd2zz__uuidz00,
		BgL_bgl_za762genuuidza762za7za7_1736z00, BGl_z62genuuidz62zz__uuidz00, 0L,
		BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list1735z00zz__uuidz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__uuidz00));
		     ADD_ROOT((void *) (&BGl_vector1734z00zz__uuidz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__uuidz00(long
		BgL_checksumz00_2488, char *BgL_fromz00_2489)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__uuidz00))
				{
					BGl_requirezd2initializa7ationz75zz__uuidz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__uuidz00();
					BGl_cnstzd2initzd2zz__uuidz00();
					return BGl_methodzd2initzd2zz__uuidz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__uuidz00(void)
	{
		{	/* Unsafe/uuid.scm 15 */
			BGl_list1735z00zz__uuidz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '0')),
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '1')),
					MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '2')),
						MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '3')),
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '4')),
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '5')),
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '6')),
										MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '7')),
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '8')),
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '9')),
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'a')),
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'b')),
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'c')),
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'd')),
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 'e')),
																		MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																					'f')), BNIL))))))))))))))));
			return (BGl_vector1734z00zz__uuidz00 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list1735z00zz__uuidz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__uuidz00(void)
	{
		{	/* Unsafe/uuid.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* genuuid */
	BGL_EXPORTED_DEF obj_t BGl_genuuidz00zz__uuidz00(void)
	{
		{	/* Unsafe/uuid.scm 83 */
			{	/* Unsafe/uuid.scm 86 */
				obj_t BgL_hexz00_1236;

				BgL_hexz00_1236 = BGl_vector1734z00zz__uuidz00;
				{	/* Unsafe/uuid.scm 91 */
					long BgL_n1z00_1238;
					long BgL_n2z00_1239;
					long BgL_n3z00_1240;
					long BgL_n4z00_1241;
					long BgL_n5z00_1242;
					long BgL_n6z00_1243;
					long BgL_n7z00_1244;
					long BgL_n8z00_1245;

					{	/* Unsafe/uuid.scm 91 */
						long BgL_arg1332z00_1311;
						long BgL_arg1333z00_1312;

						{	/* Unsafe/uuid.scm 91 */
							long BgL_tmpz00_2530;

							BgL_tmpz00_2530 = bgl_current_seconds();
							BgL_arg1332z00_1311 = (long) (BgL_tmpz00_2530);
						}
						{	/* Unsafe/uuid.scm 91 */
							int BgL_arg1530z00_1806;

							BgL_arg1530z00_1806 = rand();
							BgL_arg1333z00_1312 =
								BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
								(long) (BgL_arg1530z00_1806), 65536L);
						}
						BgL_n1z00_1238 = (BgL_arg1332z00_1311 ^ BgL_arg1333z00_1312);
					}
					{	/* Unsafe/uuid.scm 92 */
						int BgL_arg1530z00_1810;

						BgL_arg1530z00_1810 = rand();
						BgL_n2z00_1239 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1810), 65536L);
					}
					{	/* Unsafe/uuid.scm 93 */
						int BgL_arg1530z00_1812;

						BgL_arg1530z00_1812 = rand();
						BgL_n3z00_1240 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1812), 65536L);
					}
					{	/* Unsafe/uuid.scm 94 */
						int BgL_arg1530z00_1814;

						BgL_arg1530z00_1814 = rand();
						BgL_n4z00_1241 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1814), 65536L);
					}
					{	/* Unsafe/uuid.scm 95 */
						int BgL_arg1530z00_1816;

						BgL_arg1530z00_1816 = rand();
						BgL_n5z00_1242 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1816), 65536L);
					}
					{	/* Unsafe/uuid.scm 96 */
						int BgL_arg1530z00_1818;

						BgL_arg1530z00_1818 = rand();
						BgL_n6z00_1243 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1818), 65536L);
					}
					{	/* Unsafe/uuid.scm 97 */
						int BgL_arg1530z00_1820;

						BgL_arg1530z00_1820 = rand();
						BgL_n7z00_1244 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1820), 65536L);
					}
					{	/* Unsafe/uuid.scm 98 */
						int BgL_arg1530z00_1822;

						BgL_arg1530z00_1822 = rand();
						BgL_n8z00_1245 =
							BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
							(long) (BgL_arg1530z00_1822), 65536L);
					}
					{	/* Unsafe/uuid.scm 99 */
						obj_t BgL_g1041z00_1246;

						BgL_g1041z00_1246 = make_string_sans_fill(36L);
						{	/* Unsafe/uuid.scm 101 */
							unsigned char BgL_arg1198z00_1247;

							{	/* Unsafe/uuid.scm 101 */
								long BgL_arg1199z00_1248;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2559;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2559 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1199z00_1248 =
										((BgL_n1z00_1238 >> (int) (12L)) & BgL_tmpz00_2559);
								}
								BgL_arg1198z00_1247 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1199z00_1248));
							}
							STRING_SET(BgL_g1041z00_1246, 0L, BgL_arg1198z00_1247);
						}
						{	/* Unsafe/uuid.scm 102 */
							unsigned char BgL_arg1200z00_1249;

							{	/* Unsafe/uuid.scm 102 */
								long BgL_arg1201z00_1250;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2569;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2569 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1201z00_1250 =
										((BgL_n1z00_1238 >> (int) (8L)) & BgL_tmpz00_2569);
								}
								BgL_arg1200z00_1249 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1201z00_1250));
							}
							STRING_SET(BgL_g1041z00_1246, 1L, BgL_arg1200z00_1249);
						}
						{	/* Unsafe/uuid.scm 103 */
							unsigned char BgL_arg1202z00_1251;

							{	/* Unsafe/uuid.scm 103 */
								long BgL_arg1203z00_1252;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2579;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2579 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1203z00_1252 =
										((BgL_n1z00_1238 >> (int) (4L)) & BgL_tmpz00_2579);
								}
								BgL_arg1202z00_1251 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1203z00_1252));
							}
							STRING_SET(BgL_g1041z00_1246, 2L, BgL_arg1202z00_1251);
						}
						{	/* Unsafe/uuid.scm 104 */
							unsigned char BgL_arg1206z00_1253;

							{	/* Unsafe/uuid.scm 104 */
								long BgL_arg1208z00_1254;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2589;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2589 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1208z00_1254 =
										((BgL_n1z00_1238 >> (int) (0L)) & BgL_tmpz00_2589);
								}
								BgL_arg1206z00_1253 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1208z00_1254));
							}
							STRING_SET(BgL_g1041z00_1246, 3L, BgL_arg1206z00_1253);
						}
						{	/* Unsafe/uuid.scm 105 */
							unsigned char BgL_arg1209z00_1255;

							{	/* Unsafe/uuid.scm 105 */
								long BgL_arg1210z00_1256;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2599;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2599 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1210z00_1256 =
										((BgL_n2z00_1239 >> (int) (12L)) & BgL_tmpz00_2599);
								}
								BgL_arg1209z00_1255 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1210z00_1256));
							}
							STRING_SET(BgL_g1041z00_1246, 4L, BgL_arg1209z00_1255);
						}
						{	/* Unsafe/uuid.scm 106 */
							unsigned char BgL_arg1212z00_1257;

							{	/* Unsafe/uuid.scm 106 */
								long BgL_arg1215z00_1258;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2609;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2609 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1215z00_1258 =
										((BgL_n2z00_1239 >> (int) (8L)) & BgL_tmpz00_2609);
								}
								BgL_arg1212z00_1257 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1215z00_1258));
							}
							STRING_SET(BgL_g1041z00_1246, 5L, BgL_arg1212z00_1257);
						}
						{	/* Unsafe/uuid.scm 107 */
							unsigned char BgL_arg1216z00_1259;

							{	/* Unsafe/uuid.scm 107 */
								long BgL_arg1218z00_1260;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2619;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2619 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1218z00_1260 =
										((BgL_n2z00_1239 >> (int) (4L)) & BgL_tmpz00_2619);
								}
								BgL_arg1216z00_1259 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1218z00_1260));
							}
							STRING_SET(BgL_g1041z00_1246, 6L, BgL_arg1216z00_1259);
						}
						{	/* Unsafe/uuid.scm 108 */
							unsigned char BgL_arg1219z00_1261;

							{	/* Unsafe/uuid.scm 108 */
								long BgL_arg1220z00_1262;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2629;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2629 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1220z00_1262 =
										((BgL_n2z00_1239 >> (int) (0L)) & BgL_tmpz00_2629);
								}
								BgL_arg1219z00_1261 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1220z00_1262));
							}
							STRING_SET(BgL_g1041z00_1246, 7L, BgL_arg1219z00_1261);
						}
						STRING_SET(BgL_g1041z00_1246, 8L, ((unsigned char) '-'));
						{	/* Unsafe/uuid.scm 111 */
							unsigned char BgL_arg1221z00_1263;

							{	/* Unsafe/uuid.scm 111 */
								long BgL_arg1223z00_1264;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2640;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2640 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1223z00_1264 =
										((BgL_n3z00_1240 >> (int) (12L)) & BgL_tmpz00_2640);
								}
								BgL_arg1221z00_1263 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1223z00_1264));
							}
							STRING_SET(BgL_g1041z00_1246, 9L, BgL_arg1221z00_1263);
						}
						{	/* Unsafe/uuid.scm 112 */
							unsigned char BgL_arg1225z00_1265;

							{	/* Unsafe/uuid.scm 112 */
								long BgL_arg1226z00_1266;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2650;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2650 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1226z00_1266 =
										((BgL_n3z00_1240 >> (int) (8L)) & BgL_tmpz00_2650);
								}
								BgL_arg1225z00_1265 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1226z00_1266));
							}
							STRING_SET(BgL_g1041z00_1246, 10L, BgL_arg1225z00_1265);
						}
						{	/* Unsafe/uuid.scm 113 */
							unsigned char BgL_arg1227z00_1267;

							{	/* Unsafe/uuid.scm 113 */
								long BgL_arg1228z00_1268;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2660;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2660 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1228z00_1268 =
										((BgL_n3z00_1240 >> (int) (4L)) & BgL_tmpz00_2660);
								}
								BgL_arg1227z00_1267 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1228z00_1268));
							}
							STRING_SET(BgL_g1041z00_1246, 11L, BgL_arg1227z00_1267);
						}
						{	/* Unsafe/uuid.scm 114 */
							unsigned char BgL_arg1229z00_1269;

							{	/* Unsafe/uuid.scm 114 */
								long BgL_arg1230z00_1270;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2670;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2670 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1230z00_1270 =
										((BgL_n3z00_1240 >> (int) (0L)) & BgL_tmpz00_2670);
								}
								BgL_arg1229z00_1269 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1230z00_1270));
							}
							STRING_SET(BgL_g1041z00_1246, 12L, BgL_arg1229z00_1269);
						}
						STRING_SET(BgL_g1041z00_1246, 13L, ((unsigned char) '-'));
						{	/* Unsafe/uuid.scm 99 */
							unsigned char BgL_tmpz00_2681;

							BgL_tmpz00_2681 = CCHAR(VECTOR_REF(BgL_hexz00_1236, 4L));
							STRING_SET(BgL_g1041z00_1246, 14L, BgL_tmpz00_2681);
						}
						{	/* Unsafe/uuid.scm 118 */
							unsigned char BgL_arg1232z00_1272;

							{	/* Unsafe/uuid.scm 118 */
								long BgL_arg1233z00_1273;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2685;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2685 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1233z00_1273 =
										((BgL_n4z00_1241 >> (int) (8L)) & BgL_tmpz00_2685);
								}
								BgL_arg1232z00_1272 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1233z00_1273));
							}
							STRING_SET(BgL_g1041z00_1246, 15L, BgL_arg1232z00_1272);
						}
						{	/* Unsafe/uuid.scm 119 */
							unsigned char BgL_arg1234z00_1274;

							{	/* Unsafe/uuid.scm 119 */
								long BgL_arg1236z00_1275;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2695;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2695 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1236z00_1275 =
										((BgL_n4z00_1241 >> (int) (4L)) & BgL_tmpz00_2695);
								}
								BgL_arg1234z00_1274 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1236z00_1275));
							}
							STRING_SET(BgL_g1041z00_1246, 16L, BgL_arg1234z00_1274);
						}
						{	/* Unsafe/uuid.scm 120 */
							unsigned char BgL_arg1238z00_1276;

							{	/* Unsafe/uuid.scm 120 */
								long BgL_arg1239z00_1277;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2705;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2705 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1239z00_1277 =
										((BgL_n4z00_1241 >> (int) (0L)) & BgL_tmpz00_2705);
								}
								BgL_arg1238z00_1276 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1239z00_1277));
							}
							STRING_SET(BgL_g1041z00_1246, 17L, BgL_arg1238z00_1276);
						}
						STRING_SET(BgL_g1041z00_1246, 18L, ((unsigned char) '-'));
						{	/* Unsafe/uuid.scm 123 */
							unsigned char BgL_arg1242z00_1278;

							{	/* Unsafe/uuid.scm 123 */
								long BgL_arg1244z00_1279;

								{	/* Unsafe/uuid.scm 123 */
									long BgL_tmpz00_2716;

									{	/* Unsafe/uuid.scm 89 */
										long BgL_tmpz00_2717;

										{	/* Unsafe/uuid.scm 89 */

											BgL_tmpz00_2717 = ((1L << (int) (2L)) - 1L);
										}
										BgL_tmpz00_2716 =
											((BgL_n5z00_1242 >> (int) (12L)) & BgL_tmpz00_2717);
									}
									BgL_arg1244z00_1279 = (BgL_tmpz00_2716 | 8L);
								}
								BgL_arg1242z00_1278 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1244z00_1279));
							}
							STRING_SET(BgL_g1041z00_1246, 19L, BgL_arg1242z00_1278);
						}
						{	/* Unsafe/uuid.scm 124 */
							unsigned char BgL_arg1249z00_1281;

							{	/* Unsafe/uuid.scm 124 */
								long BgL_arg1252z00_1282;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2728;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2728 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1252z00_1282 =
										((BgL_n5z00_1242 >> (int) (8L)) & BgL_tmpz00_2728);
								}
								BgL_arg1249z00_1281 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1252z00_1282));
							}
							STRING_SET(BgL_g1041z00_1246, 20L, BgL_arg1249z00_1281);
						}
						{	/* Unsafe/uuid.scm 126 */
							unsigned char BgL_arg1268z00_1283;

							{	/* Unsafe/uuid.scm 126 */
								long BgL_arg1272z00_1284;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2738;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2738 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1272z00_1284 =
										((BgL_n5z00_1242 >> (int) (4L)) & BgL_tmpz00_2738);
								}
								BgL_arg1268z00_1283 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1272z00_1284));
							}
							STRING_SET(BgL_g1041z00_1246, 21L, BgL_arg1268z00_1283);
						}
						{	/* Unsafe/uuid.scm 127 */
							unsigned char BgL_arg1284z00_1285;

							{	/* Unsafe/uuid.scm 127 */
								long BgL_arg1304z00_1286;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2748;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2748 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1304z00_1286 =
										((BgL_n5z00_1242 >> (int) (0L)) & BgL_tmpz00_2748);
								}
								BgL_arg1284z00_1285 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1304z00_1286));
							}
							STRING_SET(BgL_g1041z00_1246, 22L, BgL_arg1284z00_1285);
						}
						STRING_SET(BgL_g1041z00_1246, 23L, ((unsigned char) '-'));
						{	/* Unsafe/uuid.scm 130 */
							unsigned char BgL_arg1305z00_1287;

							{	/* Unsafe/uuid.scm 130 */
								long BgL_arg1306z00_1288;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2759;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2759 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1306z00_1288 =
										((BgL_n6z00_1243 >> (int) (12L)) & BgL_tmpz00_2759);
								}
								BgL_arg1305z00_1287 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1306z00_1288));
							}
							STRING_SET(BgL_g1041z00_1246, 24L, BgL_arg1305z00_1287);
						}
						{	/* Unsafe/uuid.scm 131 */
							unsigned char BgL_arg1307z00_1289;

							{	/* Unsafe/uuid.scm 131 */
								long BgL_arg1308z00_1290;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2769;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2769 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1308z00_1290 =
										((BgL_n6z00_1243 >> (int) (8L)) & BgL_tmpz00_2769);
								}
								BgL_arg1307z00_1289 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1308z00_1290));
							}
							STRING_SET(BgL_g1041z00_1246, 25L, BgL_arg1307z00_1289);
						}
						{	/* Unsafe/uuid.scm 132 */
							unsigned char BgL_arg1309z00_1291;

							{	/* Unsafe/uuid.scm 132 */
								long BgL_arg1310z00_1292;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2779;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2779 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1310z00_1292 =
										((BgL_n6z00_1243 >> (int) (4L)) & BgL_tmpz00_2779);
								}
								BgL_arg1309z00_1291 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1310z00_1292));
							}
							STRING_SET(BgL_g1041z00_1246, 26L, BgL_arg1309z00_1291);
						}
						{	/* Unsafe/uuid.scm 133 */
							unsigned char BgL_arg1311z00_1293;

							{	/* Unsafe/uuid.scm 133 */
								long BgL_arg1312z00_1294;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2789;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2789 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1312z00_1294 =
										((BgL_n6z00_1243 >> (int) (0L)) & BgL_tmpz00_2789);
								}
								BgL_arg1311z00_1293 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1312z00_1294));
							}
							STRING_SET(BgL_g1041z00_1246, 27L, BgL_arg1311z00_1293);
						}
						{	/* Unsafe/uuid.scm 134 */
							unsigned char BgL_arg1314z00_1295;

							{	/* Unsafe/uuid.scm 134 */
								long BgL_arg1315z00_1296;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2799;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2799 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1315z00_1296 =
										((BgL_n7z00_1244 >> (int) (12L)) & BgL_tmpz00_2799);
								}
								BgL_arg1314z00_1295 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1315z00_1296));
							}
							STRING_SET(BgL_g1041z00_1246, 28L, BgL_arg1314z00_1295);
						}
						{	/* Unsafe/uuid.scm 135 */
							unsigned char BgL_arg1316z00_1297;

							{	/* Unsafe/uuid.scm 135 */
								long BgL_arg1317z00_1298;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2809;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2809 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1317z00_1298 =
										((BgL_n7z00_1244 >> (int) (8L)) & BgL_tmpz00_2809);
								}
								BgL_arg1316z00_1297 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1317z00_1298));
							}
							STRING_SET(BgL_g1041z00_1246, 29L, BgL_arg1316z00_1297);
						}
						{	/* Unsafe/uuid.scm 136 */
							unsigned char BgL_arg1318z00_1299;

							{	/* Unsafe/uuid.scm 136 */
								long BgL_arg1319z00_1300;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2819;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2819 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1319z00_1300 =
										((BgL_n7z00_1244 >> (int) (4L)) & BgL_tmpz00_2819);
								}
								BgL_arg1318z00_1299 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1319z00_1300));
							}
							STRING_SET(BgL_g1041z00_1246, 30L, BgL_arg1318z00_1299);
						}
						{	/* Unsafe/uuid.scm 137 */
							unsigned char BgL_arg1320z00_1301;

							{	/* Unsafe/uuid.scm 137 */
								long BgL_arg1321z00_1302;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2829;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2829 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1321z00_1302 =
										((BgL_n7z00_1244 >> (int) (0L)) & BgL_tmpz00_2829);
								}
								BgL_arg1320z00_1301 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1321z00_1302));
							}
							STRING_SET(BgL_g1041z00_1246, 31L, BgL_arg1320z00_1301);
						}
						{	/* Unsafe/uuid.scm 138 */
							unsigned char BgL_arg1322z00_1303;

							{	/* Unsafe/uuid.scm 138 */
								long BgL_arg1323z00_1304;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2839;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2839 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1323z00_1304 =
										((BgL_n8z00_1245 >> (int) (12L)) & BgL_tmpz00_2839);
								}
								BgL_arg1322z00_1303 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1323z00_1304));
							}
							STRING_SET(BgL_g1041z00_1246, 32L, BgL_arg1322z00_1303);
						}
						{	/* Unsafe/uuid.scm 139 */
							unsigned char BgL_arg1325z00_1305;

							{	/* Unsafe/uuid.scm 139 */
								long BgL_arg1326z00_1306;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2849;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2849 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1326z00_1306 =
										((BgL_n8z00_1245 >> (int) (8L)) & BgL_tmpz00_2849);
								}
								BgL_arg1325z00_1305 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1326z00_1306));
							}
							STRING_SET(BgL_g1041z00_1246, 33L, BgL_arg1325z00_1305);
						}
						{	/* Unsafe/uuid.scm 140 */
							unsigned char BgL_arg1327z00_1307;

							{	/* Unsafe/uuid.scm 140 */
								long BgL_arg1328z00_1308;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2859;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2859 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1328z00_1308 =
										((BgL_n8z00_1245 >> (int) (4L)) & BgL_tmpz00_2859);
								}
								BgL_arg1327z00_1307 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1328z00_1308));
							}
							STRING_SET(BgL_g1041z00_1246, 34L, BgL_arg1327z00_1307);
						}
						{	/* Unsafe/uuid.scm 141 */
							unsigned char BgL_arg1329z00_1309;

							{	/* Unsafe/uuid.scm 141 */
								long BgL_arg1331z00_1310;

								{	/* Unsafe/uuid.scm 89 */
									long BgL_tmpz00_2869;

									{	/* Unsafe/uuid.scm 89 */

										BgL_tmpz00_2869 = ((1L << (int) (4L)) - 1L);
									}
									BgL_arg1331z00_1310 =
										((BgL_n8z00_1245 >> (int) (0L)) & BgL_tmpz00_2869);
								}
								BgL_arg1329z00_1309 =
									CCHAR(VECTOR_REF(BgL_hexz00_1236, BgL_arg1331z00_1310));
							}
							STRING_SET(BgL_g1041z00_1246, 35L, BgL_arg1329z00_1309);
						}
						return BgL_g1041z00_1246;
					}
				}
			}
		}

	}



/* &genuuid */
	obj_t BGl_z62genuuidz62zz__uuidz00(obj_t BgL_envz00_2486)
	{
		{	/* Unsafe/uuid.scm 83 */
			return BGl_genuuidz00zz__uuidz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__uuidz00(void)
	{
		{	/* Unsafe/uuid.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__uuidz00(void)
	{
		{	/* Unsafe/uuid.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__uuidz00(void)
	{
		{	/* Unsafe/uuid.scm 15 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
