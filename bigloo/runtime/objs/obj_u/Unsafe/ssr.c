/*===========================================================================*/
/*   (Unsafe/ssr.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/ssr.scm -indent -o objs/obj_u/Unsafe/ssr.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SSR_TYPE_DEFINITIONS
#define BGL___SSR_TYPE_DEFINITIONS
#endif													// BGL___SSR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_cleanzd2edgezf3z21zz__ssrz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__ssrz00 = BUNSPEC;
	static obj_t BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl__ssrzd2makezd2graphz00zz__ssrz00(obj_t, obj_t);
	static obj_t BGl_list2008z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_zc3z04exitza31375ze3ze70z60zz__ssrz00(obj_t, obj_t);
	static bool_t BGl_setzd2emptyzf3z21zz__ssrz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__ssrz00(void);
	extern obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_list2021z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_list2029z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_setzd2ze3listz31zz__ssrz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ssrzd2removezd2edgez12z12zz__ssrz00(obj_t, long,
		long, obj_t);
	static obj_t BGl_genericzd2initzd2zz__ssrz00(void);
	static obj_t BGl_list2039z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_makezd2setzd2zz__ssrz00(void);
	static obj_t BGl_objectzd2initzd2zz__ssrz00(void);
	static obj_t BGl_childrenzd2forzd2eachz00zz__ssrz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_hashtablezd2removez12zc0zz__hashz00(obj_t, obj_t);
	extern obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_hashtablezd2mapzd2zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ssrzd2makezd2graphz00zz__ssrz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_friendszd2forzd2eachz00zz__ssrz00(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__ssrz00(void);
	static obj_t BGl_keyword2009z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_z62cmpz62zz__ssrz00(obj_t, obj_t, obj_t);
	static obj_t BGl_removezd2parentzd2edgez12z12zz__ssrz00(obj_t, long, obj_t);
	static obj_t BGl_keyword2022z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_keyword2030z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_z62ssrzd2connectedzf3z43zz__ssrz00(obj_t, obj_t, obj_t);
	static bool_t BGl_removezd2childz12zc0zz__ssrz00(obj_t, obj_t, long);
	static obj_t BGl_z62catchz62zz__ssrz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31522ze3ze5zz__ssrz00(obj_t, obj_t);
	static obj_t BGl_symbol2005z00zz__ssrz00 = BUNSPEC;
	static double BGl_infinityz00zz__ssrz00;
	static obj_t BGl_symbol2011z00zz__ssrz00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_friendzf3zf3zz__ssrz00(obj_t, long, long);
	static obj_t BGl_symbol2024z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_setzd2parentz12zc0zz__ssrz00(obj_t, obj_t, obj_t);
	static obj_t BGl__ssrzd2redirectz12zc0zz__ssrz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__ssrz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol2032z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_symbol2035z00zz__ssrz00 = BUNSPEC;
	static bool_t BGl_z62loosezf3z91zz__ssrz00(obj_t, obj_t);
	extern obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	extern long BGl_hashtablezd2siza7ez75zz__hashz00(obj_t);
	static obj_t BGl_symbol2040z00zz__ssrz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31374ze31975ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31374ze31976ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31374ze31977ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31374ze31978ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31374ze31979ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ssrzd2connectedzf3z21zz__ssrz00(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31536ze3ze5zz__ssrz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31374ze3ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__ssrz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__ssrz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__ssrz00(void);
	static obj_t BGl__ssrzd2addzd2edgez12z12zz__ssrz00(obj_t, obj_t);
	extern obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ssrzd2redirectz12zc0zz__ssrz00(obj_t, long, long,
		obj_t, obj_t);
	static bool_t BGl_updatezd2rankz12zc0zz__ssrz00(obj_t, obj_t);
	static obj_t BGl_addzd2friendz12zc0zz__ssrz00(obj_t, obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__ssrz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31377ze3ze5zz__ssrz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_queuezd2getz12zc0zz__ssrz00(obj_t);
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ssrzd2addzd2edgez12z12zz__ssrz00(obj_t, long,
		long, obj_t);
	static obj_t BGl_getzd2parentzd2zz__ssrz00(obj_t, long);
	static bool_t BGl_removezd2friendz12zc0zz__ssrz00(obj_t, obj_t, obj_t);
	static obj_t BGl__ssrzd2removezd2edgez12z12zz__ssrz00(obj_t, obj_t);
	extern bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_hoistze70ze7zz__ssrz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string2041z00zz__ssrz00,
		BgL_bgl_string2041za700za7za7_2046za7, "ssr-redirect!", 13);
	      DEFINE_STRING(BGl_string2042z00zz__ssrz00,
		BgL_bgl_string2042za700za7za7_2047za7,
		"wrong number of arguments: [3..5] expected, provided", 52);
	      DEFINE_STRING(BGl_string2043z00zz__ssrz00,
		BgL_bgl_string2043za700za7za7_2048za7, "_ssr-redirect!", 14);
	      DEFINE_STRING(BGl_string2044z00zz__ssrz00,
		BgL_bgl_string2044za700za7za7_2049za7, "&ssr-connected?", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2038z00zz__ssrz00,
		BgL_bgl_za762cmpza762za7za7__ssr2050z00, BGl_z62cmpz62zz__ssrz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2045z00zz__ssrz00,
		BgL_bgl_string2045za700za7za7_2051za7, "__ssr", 5);
	extern obj_t BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00;
	extern obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ssrzd2connectedzf3zd2envzf3zz__ssrz00,
		BgL_bgl_za762ssrza7d2connect2052z00, BGl_z62ssrzd2connectedzf3z43zz__ssrz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ssrzd2makezd2graphzd2envzd2zz__ssrz00,
		BgL_bgl__ssrza7d2makeza7d2gr2053z00, opt_generic_entry,
		BGl__ssrzd2makezd2graphz00zz__ssrz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ssrzd2redirectz12zd2envz12zz__ssrz00,
		BgL_bgl__ssrza7d2redirectza72054z00, opt_generic_entry,
		BGl__ssrzd2redirectz12zc0zz__ssrz00, BFALSE, -1);
	      DEFINE_REAL(BGl_real2007z00zz__ssrz00,
		BgL_bgl_real2007za700za7za7__s2055za7, 1.2);
#define BGl_real2018z00zz__ssrz00 bigloo_infinity
	   
		 
		DEFINE_STRING(BGl_string2006z00zz__ssrz00,
		BgL_bgl_string2006za700za7za7_2056za7, "none", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ssrzd2removezd2edgez12zd2envzc0zz__ssrz00,
		BgL_bgl__ssrza7d2removeza7d22057z00, opt_generic_entry,
		BGl__ssrzd2removezd2edgez12z12zz__ssrz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2010z00zz__ssrz00,
		BgL_bgl_string2010za700za7za7_2058za7, "source", 6);
	      DEFINE_STRING(BGl_string2012z00zz__ssrz00,
		BgL_bgl_string2012za700za7za7_2059za7, "ssr-make-graph", 14);
	      DEFINE_STRING(BGl_string2013z00zz__ssrz00,
		BgL_bgl_string2013za700za7za7_2060za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string2014z00zz__ssrz00,
		BgL_bgl_string2014za700za7za7_2061za7,
		"wrong number of arguments: [0..1] expected, provided", 52);
	      DEFINE_STRING(BGl_string2015z00zz__ssrz00,
		BgL_bgl_string2015za700za7za7_2062za7, "/tmp/bigloo/runtime/Unsafe/ssr.scm",
		34);
	      DEFINE_STRING(BGl_string2016z00zz__ssrz00,
		BgL_bgl_string2016za700za7za7_2063za7, "_ssr-make-graph", 15);
	      DEFINE_STRING(BGl_string2017z00zz__ssrz00,
		BgL_bgl_string2017za700za7za7_2064za7, "bint", 4);
	      DEFINE_STRING(BGl_string2019z00zz__ssrz00,
		BgL_bgl_string2019za700za7za7_2065za7, "table-ref", 9);
	      DEFINE_STRING(BGl_string2020z00zz__ssrz00,
		BgL_bgl_string2020za700za7za7_2066za7, "key unbound", 11);
	      DEFINE_STRING(BGl_string2023z00zz__ssrz00,
		BgL_bgl_string2023za700za7za7_2067za7, "onconnect", 9);
	      DEFINE_STRING(BGl_string2025z00zz__ssrz00,
		BgL_bgl_string2025za700za7za7_2068za7, "ssr-add-edge!", 13);
	      DEFINE_STRING(BGl_string2026z00zz__ssrz00,
		BgL_bgl_string2026za700za7za7_2069za7,
		"wrong number of arguments: [3..4] expected, provided", 52);
	      DEFINE_STRING(BGl_string2027z00zz__ssrz00,
		BgL_bgl_string2027za700za7za7_2070za7, "_ssr-add-edge!", 14);
	      DEFINE_STRING(BGl_string2028z00zz__ssrz00,
		BgL_bgl_string2028za700za7za7_2071za7, "vector", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ssrzd2addzd2edgez12zd2envzc0zz__ssrz00,
		BgL_bgl__ssrza7d2addza7d2edg2072z00, opt_generic_entry,
		BGl__ssrzd2addzd2edgez12z12zz__ssrz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2031z00zz__ssrz00,
		BgL_bgl_string2031za700za7za7_2073za7, "ondisconnect", 12);
	      DEFINE_STRING(BGl_string2033z00zz__ssrz00,
		BgL_bgl_string2033za700za7za7_2074za7, "ssr-remove-edge!", 16);
	      DEFINE_STRING(BGl_string2034z00zz__ssrz00,
		BgL_bgl_string2034za700za7za7_2075za7, "_ssr-remove-edge!", 17);
	      DEFINE_STRING(BGl_string2036z00zz__ssrz00,
		BgL_bgl_string2036za700za7za7_2076za7, "remove-parent-edge!", 19);
	      DEFINE_STRING(BGl_string2037z00zz__ssrz00,
		BgL_bgl_string2037za700za7za7_2077za7,
		"wrong number of arguments: [2..3] expected, provided", 52);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_list2008z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_list2021z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_list2029z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_list2039z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_keyword2009z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_keyword2022z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_keyword2030z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_symbol2005z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_symbol2011z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_symbol2024z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_symbol2032z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_symbol2035z00zz__ssrz00));
		     ADD_ROOT((void *) (&BGl_symbol2040z00zz__ssrz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__ssrz00(long
		BgL_checksumz00_3518, char *BgL_fromz00_3519)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__ssrz00))
				{
					BGl_requirezd2initializa7ationz75zz__ssrz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__ssrz00();
					BGl_cnstzd2initzd2zz__ssrz00();
					BGl_importedzd2moduleszd2initz00zz__ssrz00();
					return BGl_toplevelzd2initzd2zz__ssrz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			BGl_symbol2005z00zz__ssrz00 =
				bstring_to_symbol(BGl_string2006z00zz__ssrz00);
			BGl_keyword2009z00zz__ssrz00 =
				bstring_to_keyword(BGl_string2010z00zz__ssrz00);
			BGl_list2008z00zz__ssrz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2009z00zz__ssrz00, BNIL);
			BGl_symbol2011z00zz__ssrz00 =
				bstring_to_symbol(BGl_string2012z00zz__ssrz00);
			BGl_keyword2022z00zz__ssrz00 =
				bstring_to_keyword(BGl_string2023z00zz__ssrz00);
			BGl_list2021z00zz__ssrz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2022z00zz__ssrz00, BNIL);
			BGl_symbol2024z00zz__ssrz00 =
				bstring_to_symbol(BGl_string2025z00zz__ssrz00);
			BGl_keyword2030z00zz__ssrz00 =
				bstring_to_keyword(BGl_string2031z00zz__ssrz00);
			BGl_list2029z00zz__ssrz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2030z00zz__ssrz00, BNIL);
			BGl_symbol2032z00zz__ssrz00 =
				bstring_to_symbol(BGl_string2033z00zz__ssrz00);
			BGl_symbol2035z00zz__ssrz00 =
				bstring_to_symbol(BGl_string2036z00zz__ssrz00);
			BGl_list2039z00zz__ssrz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2022z00zz__ssrz00,
				MAKE_YOUNG_PAIR(BGl_keyword2030z00zz__ssrz00, BNIL));
			return (BGl_symbol2040z00zz__ssrz00 =
				bstring_to_symbol(BGl_string2041z00zz__ssrz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			return (BGl_infinityz00zz__ssrz00 = BGL_INFINITY, BUNSPEC);
		}

	}



/* queue-get! */
	obj_t BGl_queuezd2getz12zc0zz__ssrz00(obj_t BgL_queuez00_4)
	{
		{	/* Unsafe/ssr.scm 144 */
			{	/* Unsafe/ssr.scm 145 */
				obj_t BgL_xz00_1345;

				BgL_xz00_1345 = CAR(CAR(BgL_queuez00_4));
				{	/* Unsafe/ssr.scm 146 */
					obj_t BgL_tmpz00_3544;

					BgL_tmpz00_3544 = CDR(CAR(BgL_queuez00_4));
					SET_CAR(BgL_queuez00_4, BgL_tmpz00_3544);
				}
				if (NULLP(CAR(BgL_queuez00_4)))
					{	/* Unsafe/ssr.scm 147 */
						SET_CDR(BgL_queuez00_4, BNIL);
					}
				else
					{	/* Unsafe/ssr.scm 147 */
						BFALSE;
					}
				return BgL_xz00_1345;
			}
		}

	}



/* make-set */
	obj_t BGl_makezd2setzd2zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 158 */
			return
				BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
				(BGl_real2007z00zz__ssrz00),
				BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
				BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00);
		}

	}



/* set-empty? */
	bool_t BGl_setzd2emptyzf3z21zz__ssrz00(obj_t BgL_setz00_15)
	{
		{	/* Unsafe/ssr.scm 163 */
			{	/* Unsafe/ssr.scm 163 */
				long BgL_a1183z00_1362;

				BgL_a1183z00_1362 = BGl_hashtablezd2siza7ez75zz__hashz00(BgL_setz00_15);
				{	/* Unsafe/ssr.scm 163 */

					return (BgL_a1183z00_1362 == 0L);
				}
			}
		}

	}



/* <@exit:1375>~0 */
	obj_t BGl_zc3z04exitza31375ze3ze70z60zz__ssrz00(obj_t BgL_setz00_3459,
		obj_t BgL_fz00_3458)
	{
		{	/* Unsafe/ssr.scm 165 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1083z00_1371;

			if (SET_EXIT(BgL_an_exit1083z00_1371))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1083z00_1371 = (void *) jmpbuf;
					{	/* Unsafe/ssr.scm 165 */
						obj_t BgL_env1087z00_1372;

						BgL_env1087z00_1372 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1087z00_1372, BgL_an_exit1083z00_1371, 1L);
						{	/* Unsafe/ssr.scm 165 */
							obj_t BgL_an_exitd1084z00_1373;

							BgL_an_exitd1084z00_1373 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1087z00_1372);
							{	/* Unsafe/ssr.scm 165 */
								bool_t BgL_res1086z00_1376;

								{	/* Unsafe/ssr.scm 165 */
									obj_t BgL_zc3z04anonymousza31377ze3z87_3382;

									BgL_zc3z04anonymousza31377ze3z87_3382 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31377ze3ze5zz__ssrz00, (int) (2L),
										(int) (2L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_3382,
										(int) (0L), BgL_fz00_3458);
									PROCEDURE_SET(BgL_zc3z04anonymousza31377ze3z87_3382,
										(int) (1L), BgL_an_exitd1084z00_1373);
									BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_setz00_3459,
										BgL_zc3z04anonymousza31377ze3z87_3382);
								}
								BgL_res1086z00_1376 = ((bool_t) 0);
								POP_ENV_EXIT(BgL_env1087z00_1372);
								return BBOOL(BgL_res1086z00_1376);
							}
						}
					}
				}
		}

	}



/* &<@anonymous:1377> */
	obj_t BGl_z62zc3z04anonymousza31377ze3ze5zz__ssrz00(obj_t BgL_envz00_3383,
		obj_t BgL_k1078z00_3386, obj_t BgL_v1079z00_3387)
	{
		{	/* Unsafe/ssr.scm 165 */
			{	/* Unsafe/ssr.scm 165 */
				obj_t BgL_fz00_3384;
				obj_t BgL_an_exitd1084z00_3385;

				BgL_fz00_3384 = ((obj_t) PROCEDURE_REF(BgL_envz00_3383, (int) (0L)));
				BgL_an_exitd1084z00_3385 = PROCEDURE_REF(BgL_envz00_3383, (int) (1L));
				{	/* Unsafe/ssr.scm 165 */
					obj_t BgL_t1081z00_3494;

					{	/* Unsafe/ssr.scm 165 */
						obj_t BgL__andtest_1082z00_3495;

						BgL__andtest_1082z00_3495 =
							BGl_z62zc3z04anonymousza31536ze3ze5zz__ssrz00(BgL_fz00_3384,
							BgL_k1078z00_3386);
						if (CBOOL(BgL__andtest_1082z00_3495))
							{	/* Unsafe/ssr.scm 165 */
								BgL_t1081z00_3494 = BgL_k1078z00_3386;
							}
						else
							{	/* Unsafe/ssr.scm 165 */
								BgL_t1081z00_3494 = BFALSE;
							}
					}
					if (CBOOL(BgL_t1081z00_3494))
						{	/* Unsafe/ssr.scm 165 */
							return
								unwind_stack_until(BgL_an_exitd1084z00_3385, BFALSE,
								BgL_t1081z00_3494, BFALSE, BFALSE);
						}
					else
						{	/* Unsafe/ssr.scm 165 */
							return BFALSE;
						}
				}
			}
		}

	}



/* set->list */
	obj_t BGl_setzd2ze3listz31zz__ssrz00(obj_t BgL_setz00_20)
	{
		{	/* Unsafe/ssr.scm 166 */
			{	/* Unsafe/ssr.scm 166 */
				obj_t BgL_l1185z00_1386;

				BgL_l1185z00_1386 =
					BGl_hashtablezd2mapzd2zz__hashz00(BgL_setz00_20,
					BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00);
				if (NULLP(BgL_l1185z00_1386))
					{	/* Unsafe/ssr.scm 166 */
						return BNIL;
					}
				else
					{	/* Unsafe/ssr.scm 166 */
						obj_t BgL_head1187z00_1388;

						{	/* Unsafe/ssr.scm 166 */
							obj_t BgL_arg1387z00_1400;

							{	/* Unsafe/ssr.scm 166 */
								obj_t BgL_pairz00_2407;

								BgL_pairz00_2407 = CAR(((obj_t) BgL_l1185z00_1386));
								BgL_arg1387z00_1400 = CAR(BgL_pairz00_2407);
							}
							BgL_head1187z00_1388 = MAKE_YOUNG_PAIR(BgL_arg1387z00_1400, BNIL);
						}
						{	/* Unsafe/ssr.scm 166 */
							obj_t BgL_g1190z00_1389;

							BgL_g1190z00_1389 = CDR(((obj_t) BgL_l1185z00_1386));
							{
								obj_t BgL_l1185z00_2428;
								obj_t BgL_tail1188z00_2429;

								BgL_l1185z00_2428 = BgL_g1190z00_1389;
								BgL_tail1188z00_2429 = BgL_head1187z00_1388;
							BgL_zc3z04anonymousza31379ze3z87_2427:
								if (NULLP(BgL_l1185z00_2428))
									{	/* Unsafe/ssr.scm 166 */
										return BgL_head1187z00_1388;
									}
								else
									{	/* Unsafe/ssr.scm 166 */
										obj_t BgL_newtail1189z00_2436;

										{	/* Unsafe/ssr.scm 166 */
											obj_t BgL_arg1383z00_2437;

											{	/* Unsafe/ssr.scm 166 */
												obj_t BgL_pairz00_2441;

												BgL_pairz00_2441 = CAR(((obj_t) BgL_l1185z00_2428));
												BgL_arg1383z00_2437 = CAR(BgL_pairz00_2441);
											}
											BgL_newtail1189z00_2436 =
												MAKE_YOUNG_PAIR(BgL_arg1383z00_2437, BNIL);
										}
										SET_CDR(BgL_tail1188z00_2429, BgL_newtail1189z00_2436);
										{	/* Unsafe/ssr.scm 166 */
											obj_t BgL_arg1382z00_2439;

											BgL_arg1382z00_2439 = CDR(((obj_t) BgL_l1185z00_2428));
											{
												obj_t BgL_tail1188z00_3606;
												obj_t BgL_l1185z00_3605;

												BgL_l1185z00_3605 = BgL_arg1382z00_2439;
												BgL_tail1188z00_3606 = BgL_newtail1189z00_2436;
												BgL_tail1188z00_2429 = BgL_tail1188z00_3606;
												BgL_l1185z00_2428 = BgL_l1185z00_3605;
												goto BgL_zc3z04anonymousza31379ze3z87_2427;
											}
										}
									}
							}
						}
					}
			}
		}

	}



/* table-ref-or-set-default! */
	obj_t BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00(obj_t
		BgL_tablez00_21, obj_t BgL_xz00_22)
	{
		{	/* Unsafe/ssr.scm 168 */
			{	/* Unsafe/ssr.scm 169 */
				obj_t BgL_valuez00_2444;

				{	/* Unsafe/ssr.scm 169 */
					obj_t BgL__ortest_1090z00_2446;

					BgL__ortest_1090z00_2446 =
						BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_21, BgL_xz00_22);
					if (CBOOL(BgL__ortest_1090z00_2446))
						{	/* Unsafe/ssr.scm 169 */
							BgL_valuez00_2444 = BgL__ortest_1090z00_2446;
						}
					else
						{	/* Unsafe/ssr.scm 169 */
							BgL_valuez00_2444 = BFALSE;
						}
				}
				if (CBOOL(BgL_valuez00_2444))
					{	/* Unsafe/ssr.scm 170 */
						return BgL_valuez00_2444;
					}
				else
					{	/* Unsafe/ssr.scm 171 */
						obj_t BgL_defaultz00_2448;

						BgL_defaultz00_2448 = BGl_makezd2setzd2zz__ssrz00();
						BGl_hashtablezd2putz12zc0zz__hashz00(BgL_tablez00_21, BgL_xz00_22,
							BgL_defaultz00_2448);
						return BgL_defaultz00_2448;
					}
			}
		}

	}



/* _ssr-make-graph */
	obj_t BGl__ssrzd2makezd2graphz00zz__ssrz00(obj_t BgL_env1247z00_25,
		obj_t BgL_opt1246z00_24)
	{
		{	/* Unsafe/ssr.scm 175 */
			{	/* Unsafe/ssr.scm 175 */

				{	/* Unsafe/ssr.scm 175 */
					obj_t BgL_sourcez00_1411;

					BgL_sourcez00_1411 = BINT(0L);
					{	/* Unsafe/ssr.scm 175 */

						{
							long BgL_iz00_1412;

							BgL_iz00_1412 = 0L;
						BgL_check1250z00_1413:
							if ((BgL_iz00_1412 == VECTOR_LENGTH(BgL_opt1246z00_24)))
								{	/* Unsafe/ssr.scm 175 */
									BNIL;
								}
							else
								{	/* Unsafe/ssr.scm 175 */
									bool_t BgL_test2087z00_3618;

									{	/* Unsafe/ssr.scm 175 */
										obj_t BgL_arg1394z00_1419;

										BgL_arg1394z00_1419 =
											VECTOR_REF(BgL_opt1246z00_24, BgL_iz00_1412);
										BgL_test2087z00_3618 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1394z00_1419, BGl_list2008z00zz__ssrz00));
									}
									if (BgL_test2087z00_3618)
										{
											long BgL_iz00_3622;

											BgL_iz00_3622 = (BgL_iz00_1412 + 2L);
											BgL_iz00_1412 = BgL_iz00_3622;
											goto BgL_check1250z00_1413;
										}
									else
										{	/* Unsafe/ssr.scm 175 */
											obj_t BgL_arg1393z00_1418;

											BgL_arg1393z00_1418 =
												VECTOR_REF(BgL_opt1246z00_24, BgL_iz00_1412);
											BGl_errorz00zz__errorz00(BGl_symbol2011z00zz__ssrz00,
												BGl_string2013z00zz__ssrz00, BgL_arg1393z00_1418);
										}
								}
						}
						{	/* Unsafe/ssr.scm 175 */
							obj_t BgL_index1252z00_1420;

							{
								long BgL_iz00_2465;

								BgL_iz00_2465 = 0L;
							BgL_search1249z00_2464:
								if ((BgL_iz00_2465 == VECTOR_LENGTH(BgL_opt1246z00_24)))
									{	/* Unsafe/ssr.scm 175 */
										BgL_index1252z00_1420 = BINT(-1L);
									}
								else
									{	/* Unsafe/ssr.scm 175 */
										if (
											(BgL_iz00_2465 ==
												(VECTOR_LENGTH(BgL_opt1246z00_24) - 1L)))
											{	/* Unsafe/ssr.scm 175 */
												BgL_index1252z00_1420 =
													BGl_errorz00zz__errorz00(BGl_symbol2011z00zz__ssrz00,
													BGl_string2014z00zz__ssrz00,
													BINT(VECTOR_LENGTH(BgL_opt1246z00_24)));
											}
										else
											{	/* Unsafe/ssr.scm 175 */
												obj_t BgL_vz00_2475;

												BgL_vz00_2475 =
													VECTOR_REF(BgL_opt1246z00_24, BgL_iz00_2465);
												if ((BgL_vz00_2475 == BGl_keyword2009z00zz__ssrz00))
													{	/* Unsafe/ssr.scm 175 */
														BgL_index1252z00_1420 = BINT((BgL_iz00_2465 + 1L));
													}
												else
													{
														long BgL_iz00_3642;

														BgL_iz00_3642 = (BgL_iz00_2465 + 2L);
														BgL_iz00_2465 = BgL_iz00_3642;
														goto BgL_search1249z00_2464;
													}
											}
									}
							}
							{	/* Unsafe/ssr.scm 175 */
								bool_t BgL_test2091z00_3644;

								{	/* Unsafe/ssr.scm 175 */
									long BgL_n1z00_2479;

									{	/* Unsafe/ssr.scm 175 */
										obj_t BgL_tmpz00_3645;

										if (INTEGERP(BgL_index1252z00_1420))
											{	/* Unsafe/ssr.scm 175 */
												BgL_tmpz00_3645 = BgL_index1252z00_1420;
											}
										else
											{
												obj_t BgL_auxz00_3648;

												BgL_auxz00_3648 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(6402L),
													BGl_string2016z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_index1252z00_1420);
												FAILURE(BgL_auxz00_3648, BFALSE, BFALSE);
											}
										BgL_n1z00_2479 = (long) CINT(BgL_tmpz00_3645);
									}
									BgL_test2091z00_3644 = (BgL_n1z00_2479 >= 0L);
								}
								if (BgL_test2091z00_3644)
									{
										long BgL_auxz00_3654;

										{	/* Unsafe/ssr.scm 175 */
											obj_t BgL_tmpz00_3655;

											if (INTEGERP(BgL_index1252z00_1420))
												{	/* Unsafe/ssr.scm 175 */
													BgL_tmpz00_3655 = BgL_index1252z00_1420;
												}
											else
												{
													obj_t BgL_auxz00_3658;

													BgL_auxz00_3658 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2015z00zz__ssrz00, BINT(6402L),
														BGl_string2016z00zz__ssrz00,
														BGl_string2017z00zz__ssrz00, BgL_index1252z00_1420);
													FAILURE(BgL_auxz00_3658, BFALSE, BFALSE);
												}
											BgL_auxz00_3654 = (long) CINT(BgL_tmpz00_3655);
										}
										BgL_sourcez00_1411 =
											VECTOR_REF(BgL_opt1246z00_24, BgL_auxz00_3654);
									}
								else
									{	/* Unsafe/ssr.scm 175 */
										BFALSE;
									}
							}
						}
						return BGl_ssrzd2makezd2graphz00zz__ssrz00(BgL_sourcez00_1411);
					}
				}
			}
		}

	}



/* ssr-make-graph */
	BGL_EXPORTED_DEF obj_t BGl_ssrzd2makezd2graphz00zz__ssrz00(obj_t
		BgL_sourcez00_23)
	{
		{	/* Unsafe/ssr.scm 175 */
			{	/* Unsafe/ssr.scm 176 */
				obj_t BgL_v1195z00_1430;

				BgL_v1195z00_1430 = create_vector(6L);
				VECTOR_SET(BgL_v1195z00_1430, 0L, BgL_sourcez00_23);
				{	/* Unsafe/ssr.scm 178 */
					obj_t BgL_arg1402z00_1431;

					{	/* Unsafe/ssr.scm 178 */
						obj_t BgL_table1093z00_1432;

						BgL_table1093z00_1432 =
							BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
							(BGl_real2007z00zz__ssrz00),
							BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
							BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00);
						{	/* Unsafe/ssr.scm 178 */
							obj_t BgL_g1194z00_1433;

							{	/* Unsafe/ssr.scm 178 */
								obj_t BgL_arg1408z00_1443;

								BgL_arg1408z00_1443 =
									MAKE_YOUNG_PAIR(BgL_sourcez00_23, BINT(0L));
								{	/* Unsafe/ssr.scm 178 */
									obj_t BgL_list1409z00_1444;

									BgL_list1409z00_1444 =
										MAKE_YOUNG_PAIR(BgL_arg1408z00_1443, BNIL);
									BgL_g1194z00_1433 = BgL_list1409z00_1444;
								}
							}
							{
								obj_t BgL_l1192z00_1435;

								BgL_l1192z00_1435 = BgL_g1194z00_1433;
							BgL_zc3z04anonymousza31403ze3z87_1436:
								if (PAIRP(BgL_l1192z00_1435))
									{	/* Unsafe/ssr.scm 178 */
										{	/* Unsafe/ssr.scm 178 */
											obj_t BgL_pairz00_1438;

											BgL_pairz00_1438 = CAR(BgL_l1192z00_1435);
											{	/* Unsafe/ssr.scm 178 */
												obj_t BgL_arg1405z00_1439;
												obj_t BgL_arg1406z00_1440;

												BgL_arg1405z00_1439 = CAR(((obj_t) BgL_pairz00_1438));
												BgL_arg1406z00_1440 = CDR(((obj_t) BgL_pairz00_1438));
												BGl_hashtablezd2putz12zc0zz__hashz00
													(BgL_table1093z00_1432, BgL_arg1405z00_1439,
													BgL_arg1406z00_1440);
											}
										}
										{
											obj_t BgL_l1192z00_3682;

											BgL_l1192z00_3682 = CDR(BgL_l1192z00_1435);
											BgL_l1192z00_1435 = BgL_l1192z00_3682;
											goto BgL_zc3z04anonymousza31403ze3z87_1436;
										}
									}
								else
									{	/* Unsafe/ssr.scm 178 */
										((bool_t) 1);
									}
							}
						}
						BgL_arg1402z00_1431 = BgL_table1093z00_1432;
					}
					VECTOR_SET(BgL_v1195z00_1430, 1L, BgL_arg1402z00_1431);
				}
				VECTOR_SET(BgL_v1195z00_1430, 2L,
					BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
						(BGl_real2007z00zz__ssrz00),
						BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
						BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00));
				VECTOR_SET(BgL_v1195z00_1430, 3L,
					BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
						(BGl_real2007z00zz__ssrz00),
						BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
						BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00));
				VECTOR_SET(BgL_v1195z00_1430, 4L,
					BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
						(BGl_real2007z00zz__ssrz00),
						BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
						BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00));
				VECTOR_SET(BgL_v1195z00_1430, 5L,
					BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
						(BGl_real2007z00zz__ssrz00),
						BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
						BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00));
				return BgL_v1195z00_1430;
			}
		}

	}



/* update-rank! */
	bool_t BGl_updatezd2rankz12zc0zz__ssrz00(obj_t BgL_graphz00_37,
		obj_t BgL_xz00_38)
	{
		{	/* Unsafe/ssr.scm 217 */
			{	/* Unsafe/ssr.scm 218 */
				obj_t BgL_parentz00_1493;

				{	/* Unsafe/ssr.scm 235 */
					obj_t BgL__ortest_1101z00_2503;

					{	/* Unsafe/ssr.scm 235 */
						obj_t BgL_arg1424z00_2504;

						BgL_arg1424z00_2504 = VECTOR_REF(((obj_t) BgL_graphz00_37), 2L);
						BgL__ortest_1101z00_2503 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1424z00_2504,
							BgL_xz00_38);
					}
					if (CBOOL(BgL__ortest_1101z00_2503))
						{	/* Unsafe/ssr.scm 235 */
							BgL_parentz00_1493 = BgL__ortest_1101z00_2503;
						}
					else
						{	/* Unsafe/ssr.scm 235 */
							BgL_parentz00_1493 = BFALSE;
						}
				}
				{	/* Unsafe/ssr.scm 218 */
					obj_t BgL_parentzd2rankzd2_1494;

					if (CBOOL(BgL_parentz00_1493))
						{	/* Unsafe/ssr.scm 214 */
							obj_t BgL__ortest_1095z00_2507;

							{	/* Unsafe/ssr.scm 214 */
								obj_t BgL_arg1414z00_2508;

								BgL_arg1414z00_2508 = VECTOR_REF(((obj_t) BgL_graphz00_37), 1L);
								BgL__ortest_1095z00_2507 =
									BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1414z00_2508,
									BgL_parentz00_1493);
							}
							if (CBOOL(BgL__ortest_1095z00_2507))
								{	/* Unsafe/ssr.scm 214 */
									BgL_parentzd2rankzd2_1494 = BgL__ortest_1095z00_2507;
								}
							else
								{	/* Unsafe/ssr.scm 214 */
									BgL_parentzd2rankzd2_1494 =
										BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
								}
						}
					else
						{	/* Unsafe/ssr.scm 219 */
							BgL_parentzd2rankzd2_1494 =
								BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
						}
					{	/* Unsafe/ssr.scm 219 */
						obj_t BgL_oldzd2rankzd2_1495;

						{	/* Unsafe/ssr.scm 214 */
							obj_t BgL__ortest_1095z00_2511;

							{	/* Unsafe/ssr.scm 214 */
								obj_t BgL_arg1414z00_2512;

								BgL_arg1414z00_2512 = VECTOR_REF(((obj_t) BgL_graphz00_37), 1L);
								BgL__ortest_1095z00_2511 =
									BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1414z00_2512,
									BgL_xz00_38);
							}
							if (CBOOL(BgL__ortest_1095z00_2511))
								{	/* Unsafe/ssr.scm 214 */
									BgL_oldzd2rankzd2_1495 = BgL__ortest_1095z00_2511;
								}
							else
								{	/* Unsafe/ssr.scm 214 */
									BgL_oldzd2rankzd2_1495 =
										BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
								}
						}
						{	/* Unsafe/ssr.scm 220 */
							obj_t BgL_newzd2rankzd2_1496;

							if (INTEGERP(BgL_parentzd2rankzd2_1494))
								{	/* Unsafe/ssr.scm 221 */
									BgL_newzd2rankzd2_1496 =
										ADDFX(BgL_parentzd2rankzd2_1494, BINT(1L));
								}
							else
								{	/* Unsafe/ssr.scm 221 */
									BgL_newzd2rankzd2_1496 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_parentzd2rankzd2_1494,
										BINT(1L));
								}
							{	/* Unsafe/ssr.scm 221 */

								{	/* Unsafe/ssr.scm 222 */
									bool_t BgL_test2100z00_3728;

									{	/* Unsafe/ssr.scm 222 */
										bool_t BgL_test2101z00_3729;

										if (INTEGERP(BgL_newzd2rankzd2_1496))
											{	/* Unsafe/ssr.scm 222 */
												BgL_test2101z00_3729 = INTEGERP(BgL_oldzd2rankzd2_1495);
											}
										else
											{	/* Unsafe/ssr.scm 222 */
												BgL_test2101z00_3729 = ((bool_t) 0);
											}
										if (BgL_test2101z00_3729)
											{	/* Unsafe/ssr.scm 222 */
												BgL_test2100z00_3728 =
													(
													(long) CINT(BgL_newzd2rankzd2_1496) ==
													(long) CINT(BgL_oldzd2rankzd2_1495));
											}
										else
											{	/* Unsafe/ssr.scm 222 */
												BgL_test2100z00_3728 =
													BGl_2zd3zd3zz__r4_numbers_6_5z00
													(BgL_newzd2rankzd2_1496, BgL_oldzd2rankzd2_1495);
											}
									}
									if (BgL_test2100z00_3728)
										{	/* Unsafe/ssr.scm 222 */
											return ((bool_t) 0);
										}
									else
										{	/* Unsafe/ssr.scm 222 */
											{	/* Unsafe/ssr.scm 216 */
												obj_t BgL_arg1415z00_2517;

												BgL_arg1415z00_2517 =
													VECTOR_REF(((obj_t) BgL_graphz00_37), 1L);
												BGl_hashtablezd2putz12zc0zz__hashz00
													(BgL_arg1415z00_2517, BgL_xz00_38,
													BgL_newzd2rankzd2_1496);
											}
											return ((bool_t) 1);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* add-friend! */
	obj_t BGl_addzd2friendz12zc0zz__ssrz00(obj_t BgL_graphz00_39,
		obj_t BgL_nodez00_40, long BgL_friendz00_41)
	{
		{	/* Unsafe/ssr.scm 226 */
			{	/* Unsafe/ssr.scm 227 */
				obj_t BgL_arg1418z00_2519;

				{	/* Unsafe/ssr.scm 227 */
					obj_t BgL_arg1419z00_2520;

					BgL_arg1419z00_2520 = VECTOR_REF(BgL_graphz00_39, 4L);
					{	/* Unsafe/ssr.scm 169 */
						obj_t BgL_valuez00_2524;

						{	/* Unsafe/ssr.scm 169 */
							obj_t BgL__ortest_1090z00_2526;

							BgL__ortest_1090z00_2526 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1419z00_2520,
								BgL_nodez00_40);
							if (CBOOL(BgL__ortest_1090z00_2526))
								{	/* Unsafe/ssr.scm 169 */
									BgL_valuez00_2524 = BgL__ortest_1090z00_2526;
								}
							else
								{	/* Unsafe/ssr.scm 169 */
									BgL_valuez00_2524 = BFALSE;
								}
						}
						if (CBOOL(BgL_valuez00_2524))
							{	/* Unsafe/ssr.scm 170 */
								BgL_arg1418z00_2519 = BgL_valuez00_2524;
							}
						else
							{	/* Unsafe/ssr.scm 171 */
								obj_t BgL_defaultz00_2528;

								BgL_defaultz00_2528 = BGl_makezd2setzd2zz__ssrz00();
								BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1419z00_2520,
									BgL_nodez00_40, BgL_defaultz00_2528);
								BgL_arg1418z00_2519 = BgL_defaultz00_2528;
							}
					}
				}
				BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1418z00_2519,
					BINT(BgL_friendz00_41), BTRUE);
			}
			{	/* Unsafe/ssr.scm 228 */
				obj_t BgL_arg1420z00_2521;

				{	/* Unsafe/ssr.scm 228 */
					obj_t BgL_arg1421z00_2522;

					BgL_arg1421z00_2522 = VECTOR_REF(BgL_graphz00_39, 5L);
					{	/* Unsafe/ssr.scm 169 */
						obj_t BgL_valuez00_2530;

						{	/* Unsafe/ssr.scm 169 */
							obj_t BgL__ortest_1090z00_2532;

							BgL__ortest_1090z00_2532 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1421z00_2522,
								BINT(BgL_friendz00_41));
							if (CBOOL(BgL__ortest_1090z00_2532))
								{	/* Unsafe/ssr.scm 169 */
									BgL_valuez00_2530 = BgL__ortest_1090z00_2532;
								}
							else
								{	/* Unsafe/ssr.scm 169 */
									BgL_valuez00_2530 = BFALSE;
								}
						}
						if (CBOOL(BgL_valuez00_2530))
							{	/* Unsafe/ssr.scm 170 */
								BgL_arg1420z00_2521 = BgL_valuez00_2530;
							}
						else
							{	/* Unsafe/ssr.scm 171 */
								obj_t BgL_defaultz00_2534;

								BgL_defaultz00_2534 = BGl_makezd2setzd2zz__ssrz00();
								BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1421z00_2522,
									BINT(BgL_friendz00_41), BgL_defaultz00_2534);
								BgL_arg1420z00_2521 = BgL_defaultz00_2534;
							}
					}
				}
				return
					BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1420z00_2521,
					BgL_nodez00_40, BTRUE);
			}
		}

	}



/* remove-friend! */
	bool_t BGl_removezd2friendz12zc0zz__ssrz00(obj_t BgL_graphz00_42,
		obj_t BgL_nodez00_43, obj_t BgL_friendz00_44)
	{
		{	/* Unsafe/ssr.scm 229 */
			{	/* Unsafe/ssr.scm 230 */
				obj_t BgL_friendsz00_1504;
				obj_t BgL_friendliesz00_1505;

				{	/* Unsafe/ssr.scm 230 */
					obj_t BgL__ortest_1097z00_1507;

					{	/* Unsafe/ssr.scm 230 */
						obj_t BgL_arg1422z00_1508;

						BgL_arg1422z00_1508 = VECTOR_REF(((obj_t) BgL_graphz00_42), 4L);
						BgL__ortest_1097z00_1507 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1422z00_1508,
							BgL_nodez00_43);
					}
					if (CBOOL(BgL__ortest_1097z00_1507))
						{	/* Unsafe/ssr.scm 230 */
							BgL_friendsz00_1504 = BgL__ortest_1097z00_1507;
						}
					else
						{	/* Unsafe/ssr.scm 230 */
							BgL_friendsz00_1504 = BFALSE;
						}
				}
				{	/* Unsafe/ssr.scm 231 */
					obj_t BgL__ortest_1099z00_1510;

					{	/* Unsafe/ssr.scm 231 */
						obj_t BgL_arg1423z00_1511;

						BgL_arg1423z00_1511 = VECTOR_REF(((obj_t) BgL_graphz00_42), 5L);
						BgL__ortest_1099z00_1510 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1423z00_1511,
							BgL_friendz00_44);
					}
					if (CBOOL(BgL__ortest_1099z00_1510))
						{	/* Unsafe/ssr.scm 231 */
							BgL_friendliesz00_1505 = BgL__ortest_1099z00_1510;
						}
					else
						{	/* Unsafe/ssr.scm 231 */
							BgL_friendliesz00_1505 = BFALSE;
						}
				}
				if (CBOOL(BgL_friendsz00_1504))
					{	/* Unsafe/ssr.scm 232 */
						BGl_hashtablezd2removez12zc0zz__hashz00(BgL_friendsz00_1504,
							BgL_friendz00_44);
					}
				else
					{	/* Unsafe/ssr.scm 232 */
						((bool_t) 0);
					}
				if (CBOOL(BgL_friendliesz00_1505))
					{	/* Unsafe/ssr.scm 233 */
						return
							BGl_hashtablezd2removez12zc0zz__hashz00(BgL_friendliesz00_1505,
							BgL_nodez00_43);
					}
				else
					{	/* Unsafe/ssr.scm 233 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* get-parent */
	obj_t BGl_getzd2parentzd2zz__ssrz00(obj_t BgL_graphz00_45, long BgL_xz00_46)
	{
		{	/* Unsafe/ssr.scm 234 */
			{	/* Unsafe/ssr.scm 235 */
				obj_t BgL__ortest_1101z00_2538;

				BgL__ortest_1101z00_2538 =
					BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_45, 2L),
					BINT(BgL_xz00_46));
				if (CBOOL(BgL__ortest_1101z00_2538))
					{	/* Unsafe/ssr.scm 235 */
						return BgL__ortest_1101z00_2538;
					}
				else
					{	/* Unsafe/ssr.scm 235 */
						return BFALSE;
					}
			}
		}

	}



/* set-parent! */
	obj_t BGl_setzd2parentz12zc0zz__ssrz00(obj_t BgL_graphz00_47,
		obj_t BgL_childz00_48, obj_t BgL_parentz00_49)
	{
		{	/* Unsafe/ssr.scm 236 */
			{	/* Unsafe/ssr.scm 238 */
				obj_t BgL_oldzd2parentzd2_1515;

				{	/* Unsafe/ssr.scm 235 */
					obj_t BgL__ortest_1101z00_2542;

					{	/* Unsafe/ssr.scm 235 */
						obj_t BgL_arg1424z00_2543;

						BgL_arg1424z00_2543 = VECTOR_REF(((obj_t) BgL_graphz00_47), 2L);
						BgL__ortest_1101z00_2542 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1424z00_2543,
							BgL_childz00_48);
					}
					if (CBOOL(BgL__ortest_1101z00_2542))
						{	/* Unsafe/ssr.scm 235 */
							BgL_oldzd2parentzd2_1515 = BgL__ortest_1101z00_2542;
						}
					else
						{	/* Unsafe/ssr.scm 235 */
							BgL_oldzd2parentzd2_1515 = BFALSE;
						}
				}
				if (CBOOL(BgL_oldzd2parentzd2_1515))
					{	/* Unsafe/ssr.scm 239 */
						{	/* Unsafe/ssr.scm 227 */
							obj_t BgL_arg1418z00_2545;

							{	/* Unsafe/ssr.scm 227 */
								obj_t BgL_arg1419z00_2546;

								BgL_arg1419z00_2546 = VECTOR_REF(((obj_t) BgL_graphz00_47), 4L);
								BgL_arg1418z00_2545 =
									BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
									(BgL_arg1419z00_2546, BgL_oldzd2parentzd2_1515);
							}
							BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1418z00_2545,
								BgL_childz00_48, BTRUE);
						}
						{	/* Unsafe/ssr.scm 228 */
							obj_t BgL_arg1420z00_2547;

							{	/* Unsafe/ssr.scm 228 */
								obj_t BgL_arg1421z00_2548;

								BgL_arg1421z00_2548 = VECTOR_REF(((obj_t) BgL_graphz00_47), 5L);
								BgL_arg1420z00_2547 =
									BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
									(BgL_arg1421z00_2548, BgL_childz00_48);
							}
							BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1420z00_2547,
								BgL_oldzd2parentzd2_1515, BTRUE);
						}
						{	/* Unsafe/ssr.scm 254 */
							obj_t BgL_arg1429z00_2551;

							{	/* Unsafe/ssr.scm 254 */
								obj_t BgL__ortest_1103z00_2553;

								{	/* Unsafe/ssr.scm 254 */
									obj_t BgL_arg1430z00_2554;

									BgL_arg1430z00_2554 =
										VECTOR_REF(((obj_t) BgL_graphz00_47), 3L);
									BgL__ortest_1103z00_2553 =
										BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1430z00_2554,
										BgL_oldzd2parentzd2_1515);
								}
								if (CBOOL(BgL__ortest_1103z00_2553))
									{	/* Unsafe/ssr.scm 254 */
										BgL_arg1429z00_2551 = BgL__ortest_1103z00_2553;
									}
								else
									{	/* Unsafe/ssr.scm 254 */
										BgL_arg1429z00_2551 =
											BGl_errorz00zz__errorz00(BGl_string2019z00zz__ssrz00,
											BGl_string2020z00zz__ssrz00, BgL_oldzd2parentzd2_1515);
									}
							}
							BGl_hashtablezd2removez12zc0zz__hashz00(BgL_arg1429z00_2551,
								BgL_childz00_48);
						}
					}
				else
					{	/* Unsafe/ssr.scm 239 */
						((bool_t) 0);
					}
			}
			BGl_removezd2friendz12zc0zz__ssrz00(BgL_graphz00_47, BgL_parentz00_49,
				BgL_childz00_48);
			{	/* Unsafe/ssr.scm 245 */
				obj_t BgL_arg1425z00_1516;

				BgL_arg1425z00_1516 = VECTOR_REF(((obj_t) BgL_graphz00_47), 2L);
				BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1425z00_1516,
					BgL_childz00_48, BgL_parentz00_49);
			}
			{	/* Unsafe/ssr.scm 247 */
				obj_t BgL_arg1426z00_1517;

				{	/* Unsafe/ssr.scm 247 */
					obj_t BgL_arg1427z00_1518;

					BgL_arg1427z00_1518 = VECTOR_REF(((obj_t) BgL_graphz00_47), 3L);
					{	/* Unsafe/ssr.scm 169 */
						obj_t BgL_valuez00_2558;

						{	/* Unsafe/ssr.scm 169 */
							obj_t BgL__ortest_1090z00_2560;

							BgL__ortest_1090z00_2560 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1427z00_1518,
								BgL_parentz00_49);
							if (CBOOL(BgL__ortest_1090z00_2560))
								{	/* Unsafe/ssr.scm 169 */
									BgL_valuez00_2558 = BgL__ortest_1090z00_2560;
								}
							else
								{	/* Unsafe/ssr.scm 169 */
									BgL_valuez00_2558 = BFALSE;
								}
						}
						if (CBOOL(BgL_valuez00_2558))
							{	/* Unsafe/ssr.scm 170 */
								BgL_arg1426z00_1517 = BgL_valuez00_2558;
							}
						else
							{	/* Unsafe/ssr.scm 171 */
								obj_t BgL_defaultz00_2562;

								BgL_defaultz00_2562 = BGl_makezd2setzd2zz__ssrz00();
								BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1427z00_1518,
									BgL_parentz00_49, BgL_defaultz00_2562);
								BgL_arg1426z00_1517 = BgL_defaultz00_2562;
							}
					}
				}
				return
					BGl_hashtablezd2putz12zc0zz__hashz00(BgL_arg1426z00_1517,
					BgL_childz00_48, BTRUE);
			}
		}

	}



/* remove-child! */
	bool_t BGl_removezd2childz12zc0zz__ssrz00(obj_t BgL_graphz00_52,
		obj_t BgL_parentz00_53, long BgL_childz00_54)
	{
		{	/* Unsafe/ssr.scm 252 */
			{	/* Unsafe/ssr.scm 254 */
				obj_t BgL_arg1429z00_2575;

				{	/* Unsafe/ssr.scm 254 */
					obj_t BgL__ortest_1103z00_2577;

					BgL__ortest_1103z00_2577 =
						BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_52, 3L),
						BgL_parentz00_53);
					if (CBOOL(BgL__ortest_1103z00_2577))
						{	/* Unsafe/ssr.scm 254 */
							BgL_arg1429z00_2575 = BgL__ortest_1103z00_2577;
						}
					else
						{	/* Unsafe/ssr.scm 254 */
							BgL_arg1429z00_2575 =
								BGl_errorz00zz__errorz00(BGl_string2019z00zz__ssrz00,
								BGl_string2020z00zz__ssrz00, BgL_parentz00_53);
						}
				}
				return
					BGl_hashtablezd2removez12zc0zz__hashz00(BgL_arg1429z00_2575,
					BINT(BgL_childz00_54));
			}
		}

	}



/* clean-edge? */
	bool_t BGl_cleanzd2edgezf3z21zz__ssrz00(obj_t BgL_graphz00_55,
		obj_t BgL_fromz00_56, obj_t BgL_toz00_57)
	{
		{	/* Unsafe/ssr.scm 257 */
			{	/* Unsafe/ssr.scm 258 */
				obj_t BgL_a1198z00_1525;

				{	/* Unsafe/ssr.scm 214 */
					obj_t BgL__ortest_1095z00_2581;

					BgL__ortest_1095z00_2581 =
						BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_55, 1L),
						BgL_fromz00_56);
					if (CBOOL(BgL__ortest_1095z00_2581))
						{	/* Unsafe/ssr.scm 214 */
							BgL_a1198z00_1525 = BgL__ortest_1095z00_2581;
						}
					else
						{	/* Unsafe/ssr.scm 214 */
							BgL_a1198z00_1525 = BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
						}
				}
				{	/* Unsafe/ssr.scm 258 */
					obj_t BgL_b1199z00_1526;

					{	/* Unsafe/ssr.scm 258 */
						obj_t BgL_a1196z00_1528;

						{	/* Unsafe/ssr.scm 214 */
							obj_t BgL__ortest_1095z00_2585;

							BgL__ortest_1095z00_2585 =
								BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_55,
									1L), BgL_toz00_57);
							if (CBOOL(BgL__ortest_1095z00_2585))
								{	/* Unsafe/ssr.scm 214 */
									BgL_a1196z00_1528 = BgL__ortest_1095z00_2585;
								}
							else
								{	/* Unsafe/ssr.scm 214 */
									BgL_a1196z00_1528 = BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
								}
						}
						{	/* Unsafe/ssr.scm 258 */

							if (INTEGERP(BgL_a1196z00_1528))
								{	/* Unsafe/ssr.scm 258 */
									BgL_b1199z00_1526 = SUBFX(BgL_a1196z00_1528, BINT(1L));
								}
							else
								{	/* Unsafe/ssr.scm 258 */
									BgL_b1199z00_1526 =
										BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_a1196z00_1528,
										BINT(1L));
								}
						}
					}
					{	/* Unsafe/ssr.scm 258 */

						{	/* Unsafe/ssr.scm 258 */
							bool_t BgL_test2121z00_3839;

							if (INTEGERP(BgL_a1198z00_1525))
								{	/* Unsafe/ssr.scm 258 */
									BgL_test2121z00_3839 = INTEGERP(BgL_b1199z00_1526);
								}
							else
								{	/* Unsafe/ssr.scm 258 */
									BgL_test2121z00_3839 = ((bool_t) 0);
								}
							if (BgL_test2121z00_3839)
								{	/* Unsafe/ssr.scm 258 */
									return
										(
										(long) CINT(BgL_a1198z00_1525) >=
										(long) CINT(BgL_b1199z00_1526));
								}
							else
								{	/* Unsafe/ssr.scm 258 */
									return
										BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_a1198z00_1525,
										BgL_b1199z00_1526);
								}
						}
					}
				}
			}
		}

	}



/* children-for-each */
	obj_t BGl_childrenzd2forzd2eachz00zz__ssrz00(obj_t BgL_fz00_64,
		obj_t BgL_graphz00_65, obj_t BgL_xz00_66)
	{
		{	/* Unsafe/ssr.scm 264 */
			{	/* Unsafe/ssr.scm 265 */
				obj_t BgL_childrenz00_2606;

				{	/* Unsafe/ssr.scm 265 */
					obj_t BgL__ortest_1106z00_2608;

					{	/* Unsafe/ssr.scm 265 */
						obj_t BgL_arg1434z00_2609;

						BgL_arg1434z00_2609 = VECTOR_REF(((obj_t) BgL_graphz00_65), 3L);
						BgL__ortest_1106z00_2608 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1434z00_2609,
							BgL_xz00_66);
					}
					if (CBOOL(BgL__ortest_1106z00_2608))
						{	/* Unsafe/ssr.scm 265 */
							BgL_childrenz00_2606 = BgL__ortest_1106z00_2608;
						}
					else
						{	/* Unsafe/ssr.scm 265 */
							BgL_childrenz00_2606 = BFALSE;
						}
				}
				if (CBOOL(BgL_childrenz00_2606))
					{	/* Unsafe/ssr.scm 164 */
						obj_t BgL_zc3z04anonymousza31374ze3z87_3391;

						BgL_zc3z04anonymousza31374ze3z87_3391 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31374ze3ze5zz__ssrz00,
							(int) (2L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31374ze3z87_3391,
							(int) (0L), BgL_fz00_64);
						return
							BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_childrenz00_2606,
							BgL_zc3z04anonymousza31374ze3z87_3391);
					}
				else
					{	/* Unsafe/ssr.scm 266 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1374> */
	obj_t BGl_z62zc3z04anonymousza31374ze3ze5zz__ssrz00(obj_t BgL_envz00_3392,
		obj_t BgL_kz00_3394, obj_t BgL__z00_3395)
	{
		{	/* Unsafe/ssr.scm 164 */
			{	/* Unsafe/ssr.scm 164 */
				obj_t BgL_fz00_3393;

				BgL_fz00_3393 = ((obj_t) PROCEDURE_REF(BgL_envz00_3392, (int) (0L)));
				return
					((obj_t(*)(obj_t,
							obj_t)) PROCEDURE_L_ENTRY(BgL_fz00_3393)) (BgL_fz00_3393,
					BgL_kz00_3394);
			}
		}

	}



/* friends-for-each */
	obj_t BGl_friendszd2forzd2eachz00zz__ssrz00(obj_t BgL_fz00_67,
		obj_t BgL_graphz00_68, obj_t BgL_xz00_69)
	{
		{	/* Unsafe/ssr.scm 267 */
			{	/* Unsafe/ssr.scm 268 */
				obj_t BgL_friendsz00_2614;

				{	/* Unsafe/ssr.scm 268 */
					obj_t BgL__ortest_1108z00_2616;

					{	/* Unsafe/ssr.scm 268 */
						obj_t BgL_arg1435z00_2617;

						BgL_arg1435z00_2617 = VECTOR_REF(((obj_t) BgL_graphz00_68), 4L);
						BgL__ortest_1108z00_2616 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_arg1435z00_2617,
							BgL_xz00_69);
					}
					if (CBOOL(BgL__ortest_1108z00_2616))
						{	/* Unsafe/ssr.scm 268 */
							BgL_friendsz00_2614 = BgL__ortest_1108z00_2616;
						}
					else
						{	/* Unsafe/ssr.scm 268 */
							BgL_friendsz00_2614 = BFALSE;
						}
				}
				if (CBOOL(BgL_friendsz00_2614))
					{	/* Unsafe/ssr.scm 164 */
						obj_t BgL_zc3z04anonymousza31374ze3z87_3396;

						BgL_zc3z04anonymousza31374ze3z87_3396 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31374ze31975ze5zz__ssrz00, (int) (2L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31374ze3z87_3396, (int) (0L),
							BgL_fz00_67);
						return BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_friendsz00_2614,
							BgL_zc3z04anonymousza31374ze3z87_3396);
					}
				else
					{	/* Unsafe/ssr.scm 269 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1374>1975 */
	obj_t BGl_z62zc3z04anonymousza31374ze31975ze5zz__ssrz00(obj_t BgL_envz00_3397,
		obj_t BgL_kz00_3399, obj_t BgL__z00_3400)
	{
		{	/* Unsafe/ssr.scm 164 */
			{	/* Unsafe/ssr.scm 164 */
				obj_t BgL_fz00_3398;

				BgL_fz00_3398 = ((obj_t) PROCEDURE_REF(BgL_envz00_3397, (int) (0L)));
				return
					((obj_t(*)(obj_t,
							obj_t)) PROCEDURE_L_ENTRY(BgL_fz00_3398)) (BgL_fz00_3398,
					BgL_kz00_3399);
			}
		}

	}



/* friend? */
	obj_t BGl_friendzf3zf3zz__ssrz00(obj_t BgL_graphz00_86, long BgL_xz00_87,
		long BgL_fz00_88)
	{
		{	/* Unsafe/ssr.scm 288 */
			{	/* Unsafe/ssr.scm 289 */
				obj_t BgL_friendsz00_2661;

				{	/* Unsafe/ssr.scm 289 */
					obj_t BgL__ortest_1116z00_2663;

					BgL__ortest_1116z00_2663 =
						BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_86, 4L),
						BINT(BgL_xz00_87));
					if (CBOOL(BgL__ortest_1116z00_2663))
						{	/* Unsafe/ssr.scm 289 */
							BgL_friendsz00_2661 = BgL__ortest_1116z00_2663;
						}
					else
						{	/* Unsafe/ssr.scm 289 */
							BgL_friendsz00_2661 = BFALSE;
						}
				}
				if (CBOOL(BgL_friendsz00_2661))
					{	/* Unsafe/ssr.scm 161 */
						obj_t BgL__ortest_1077z00_2667;

						BgL__ortest_1077z00_2667 =
							BGl_hashtablezd2getzd2zz__hashz00(BgL_friendsz00_2661,
							BINT(BgL_fz00_88));
						if (CBOOL(BgL__ortest_1077z00_2667))
							{	/* Unsafe/ssr.scm 161 */
								return BgL__ortest_1077z00_2667;
							}
						else
							{	/* Unsafe/ssr.scm 161 */
								return BFALSE;
							}
					}
				else
					{	/* Unsafe/ssr.scm 290 */
						return BFALSE;
					}
			}
		}

	}



/* _ssr-add-edge! */
	obj_t BGl__ssrzd2addzd2edgez12z12zz__ssrz00(obj_t BgL_env1254z00_94,
		obj_t BgL_opt1253z00_93)
	{
		{	/* Unsafe/ssr.scm 292 */
			{	/* Unsafe/ssr.scm 292 */
				obj_t BgL_graphz00_1564;
				obj_t BgL_fromz00_1565;
				obj_t BgL_toz00_1566;

				BgL_graphz00_1564 = VECTOR_REF(BgL_opt1253z00_93, 0L);
				BgL_fromz00_1565 = VECTOR_REF(BgL_opt1253z00_93, 1L);
				BgL_toz00_1566 = VECTOR_REF(BgL_opt1253z00_93, 2L);
				{	/* Unsafe/ssr.scm 292 */
					obj_t BgL_onconnectz00_1567;

					BgL_onconnectz00_1567 = BFALSE;
					{	/* Unsafe/ssr.scm 292 */

						{
							long BgL_iz00_1568;

							BgL_iz00_1568 = 3L;
						BgL_check1257z00_1569:
							if ((BgL_iz00_1568 == VECTOR_LENGTH(BgL_opt1253z00_93)))
								{	/* Unsafe/ssr.scm 292 */
									BNIL;
								}
							else
								{	/* Unsafe/ssr.scm 292 */
									bool_t BgL_test2131z00_3904;

									{	/* Unsafe/ssr.scm 292 */
										obj_t BgL_arg1447z00_1575;

										BgL_arg1447z00_1575 =
											VECTOR_REF(BgL_opt1253z00_93, BgL_iz00_1568);
										BgL_test2131z00_3904 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1447z00_1575, BGl_list2021z00zz__ssrz00));
									}
									if (BgL_test2131z00_3904)
										{
											long BgL_iz00_3908;

											BgL_iz00_3908 = (BgL_iz00_1568 + 2L);
											BgL_iz00_1568 = BgL_iz00_3908;
											goto BgL_check1257z00_1569;
										}
									else
										{	/* Unsafe/ssr.scm 292 */
											obj_t BgL_arg1446z00_1574;

											BgL_arg1446z00_1574 =
												VECTOR_REF(BgL_opt1253z00_93, BgL_iz00_1568);
											BGl_errorz00zz__errorz00(BGl_symbol2024z00zz__ssrz00,
												BGl_string2013z00zz__ssrz00, BgL_arg1446z00_1574);
										}
								}
						}
						{	/* Unsafe/ssr.scm 292 */
							obj_t BgL_index1259z00_1576;

							{
								long BgL_iz00_2684;

								BgL_iz00_2684 = 3L;
							BgL_search1256z00_2683:
								if ((BgL_iz00_2684 == VECTOR_LENGTH(BgL_opt1253z00_93)))
									{	/* Unsafe/ssr.scm 292 */
										BgL_index1259z00_1576 = BINT(-1L);
									}
								else
									{	/* Unsafe/ssr.scm 292 */
										if (
											(BgL_iz00_2684 ==
												(VECTOR_LENGTH(BgL_opt1253z00_93) - 1L)))
											{	/* Unsafe/ssr.scm 292 */
												BgL_index1259z00_1576 =
													BGl_errorz00zz__errorz00(BGl_symbol2024z00zz__ssrz00,
													BGl_string2026z00zz__ssrz00,
													BINT(VECTOR_LENGTH(BgL_opt1253z00_93)));
											}
										else
											{	/* Unsafe/ssr.scm 292 */
												obj_t BgL_vz00_2694;

												BgL_vz00_2694 =
													VECTOR_REF(BgL_opt1253z00_93, BgL_iz00_2684);
												if ((BgL_vz00_2694 == BGl_keyword2022z00zz__ssrz00))
													{	/* Unsafe/ssr.scm 292 */
														BgL_index1259z00_1576 = BINT((BgL_iz00_2684 + 1L));
													}
												else
													{
														long BgL_iz00_3928;

														BgL_iz00_3928 = (BgL_iz00_2684 + 2L);
														BgL_iz00_2684 = BgL_iz00_3928;
														goto BgL_search1256z00_2683;
													}
											}
									}
							}
							{	/* Unsafe/ssr.scm 292 */
								bool_t BgL_test2135z00_3930;

								{	/* Unsafe/ssr.scm 292 */
									long BgL_n1z00_2698;

									{	/* Unsafe/ssr.scm 292 */
										obj_t BgL_tmpz00_3931;

										if (INTEGERP(BgL_index1259z00_1576))
											{	/* Unsafe/ssr.scm 292 */
												BgL_tmpz00_3931 = BgL_index1259z00_1576;
											}
										else
											{
												obj_t BgL_auxz00_3934;

												BgL_auxz00_3934 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(10751L),
													BGl_string2027z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_index1259z00_1576);
												FAILURE(BgL_auxz00_3934, BFALSE, BFALSE);
											}
										BgL_n1z00_2698 = (long) CINT(BgL_tmpz00_3931);
									}
									BgL_test2135z00_3930 = (BgL_n1z00_2698 >= 0L);
								}
								if (BgL_test2135z00_3930)
									{
										long BgL_auxz00_3940;

										{	/* Unsafe/ssr.scm 292 */
											obj_t BgL_tmpz00_3941;

											if (INTEGERP(BgL_index1259z00_1576))
												{	/* Unsafe/ssr.scm 292 */
													BgL_tmpz00_3941 = BgL_index1259z00_1576;
												}
											else
												{
													obj_t BgL_auxz00_3944;

													BgL_auxz00_3944 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2015z00zz__ssrz00, BINT(10751L),
														BGl_string2027z00zz__ssrz00,
														BGl_string2017z00zz__ssrz00, BgL_index1259z00_1576);
													FAILURE(BgL_auxz00_3944, BFALSE, BFALSE);
												}
											BgL_auxz00_3940 = (long) CINT(BgL_tmpz00_3941);
										}
										BgL_onconnectz00_1567 =
											VECTOR_REF(BgL_opt1253z00_93, BgL_auxz00_3940);
									}
								else
									{	/* Unsafe/ssr.scm 292 */
										BFALSE;
									}
							}
						}
						{	/* Unsafe/ssr.scm 292 */
							obj_t BgL_arg1449z00_1578;
							obj_t BgL_arg1450z00_1579;
							obj_t BgL_arg1451z00_1580;

							BgL_arg1449z00_1578 = VECTOR_REF(BgL_opt1253z00_93, 0L);
							BgL_arg1450z00_1579 = VECTOR_REF(BgL_opt1253z00_93, 1L);
							BgL_arg1451z00_1580 = VECTOR_REF(BgL_opt1253z00_93, 2L);
							{	/* Unsafe/ssr.scm 292 */
								obj_t BgL_onconnectz00_1581;

								BgL_onconnectz00_1581 = BgL_onconnectz00_1567;
								{	/* Unsafe/ssr.scm 292 */
									long BgL_auxz00_3969;
									long BgL_auxz00_3960;
									obj_t BgL_auxz00_3953;

									{	/* Unsafe/ssr.scm 292 */
										obj_t BgL_tmpz00_3970;

										if (INTEGERP(BgL_arg1451z00_1580))
											{	/* Unsafe/ssr.scm 292 */
												BgL_tmpz00_3970 = BgL_arg1451z00_1580;
											}
										else
											{
												obj_t BgL_auxz00_3973;

												BgL_auxz00_3973 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(10751L),
													BGl_string2027z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_arg1451z00_1580);
												FAILURE(BgL_auxz00_3973, BFALSE, BFALSE);
											}
										BgL_auxz00_3969 = (long) CINT(BgL_tmpz00_3970);
									}
									{	/* Unsafe/ssr.scm 292 */
										obj_t BgL_tmpz00_3961;

										if (INTEGERP(BgL_arg1450z00_1579))
											{	/* Unsafe/ssr.scm 292 */
												BgL_tmpz00_3961 = BgL_arg1450z00_1579;
											}
										else
											{
												obj_t BgL_auxz00_3964;

												BgL_auxz00_3964 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(10751L),
													BGl_string2027z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_arg1450z00_1579);
												FAILURE(BgL_auxz00_3964, BFALSE, BFALSE);
											}
										BgL_auxz00_3960 = (long) CINT(BgL_tmpz00_3961);
									}
									if (VECTORP(BgL_arg1449z00_1578))
										{	/* Unsafe/ssr.scm 292 */
											BgL_auxz00_3953 = BgL_arg1449z00_1578;
										}
									else
										{
											obj_t BgL_auxz00_3956;

											BgL_auxz00_3956 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2015z00zz__ssrz00, BINT(10751L),
												BGl_string2027z00zz__ssrz00,
												BGl_string2028z00zz__ssrz00, BgL_arg1449z00_1578);
											FAILURE(BgL_auxz00_3956, BFALSE, BFALSE);
										}
									return
										BGl_ssrzd2addzd2edgez12z12zz__ssrz00(BgL_auxz00_3953,
										BgL_auxz00_3960, BgL_auxz00_3969, BgL_onconnectz00_1581);
								}
							}
						}
					}
				}
			}
		}

	}



/* ssr-add-edge! */
	BGL_EXPORTED_DEF obj_t BGl_ssrzd2addzd2edgez12z12zz__ssrz00(obj_t
		BgL_graphz00_89, long BgL_fromz00_90, long BgL_toz00_91,
		obj_t BgL_onconnectz00_92)
	{
		{	/* Unsafe/ssr.scm 292 */
			{	/* Unsafe/ssr.scm 293 */
				obj_t BgL_queuez00_1589;

				BgL_queuez00_1589 = MAKE_YOUNG_PAIR(BNIL, BNIL);
				{	/* Unsafe/ssr.scm 307 */
					bool_t BgL_test2141z00_3980;

					{	/* Unsafe/ssr.scm 262 */
						bool_t BgL__ortest_1104z00_2724;

						BgL__ortest_1104z00_2724 =
							(BINT(BgL_fromz00_90) ==
							BGl_getzd2parentzd2zz__ssrz00(BgL_graphz00_89, BgL_toz00_91));
						if (BgL__ortest_1104z00_2724)
							{	/* Unsafe/ssr.scm 262 */
								BgL_test2141z00_3980 = BgL__ortest_1104z00_2724;
							}
						else
							{	/* Unsafe/ssr.scm 262 */
								BgL_test2141z00_3980 =
									CBOOL(BGl_friendzf3zf3zz__ssrz00(BgL_graphz00_89,
										BgL_fromz00_90, BgL_toz00_91));
							}
					}
					if (BgL_test2141z00_3980)
						{	/* Unsafe/ssr.scm 307 */
							return BFALSE;
						}
					else
						{	/* Unsafe/ssr.scm 307 */
							BGl_hashtablezd2putz12zc0zz__hashz00
								(BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00(VECTOR_REF
									(BgL_graphz00_89, 4L), BINT(BgL_fromz00_90)),
								BINT(BgL_toz00_91), BTRUE);
							BGl_hashtablezd2putz12zc0zz__hashz00
								(BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00(VECTOR_REF
									(BgL_graphz00_89, 5L), BINT(BgL_toz00_91)),
								BINT(BgL_fromz00_90), BTRUE);
							BGl_hoistze70ze7zz__ssrz00(BgL_queuez00_1589, BgL_onconnectz00_92,
								BgL_graphz00_89, BINT(BgL_fromz00_90), BINT(BgL_toz00_91));
							{

								{	/* Unsafe/ssr.scm 310 */
									bool_t BgL_tmpz00_4000;

								BgL_zc3z04anonymousza31459ze3z87_1593:
									if (NULLP(CAR(BgL_queuez00_1589)))
										{	/* Unsafe/ssr.scm 310 */
											BgL_tmpz00_4000 = ((bool_t) 0);
										}
									else
										{	/* Unsafe/ssr.scm 310 */
											{	/* Unsafe/ssr.scm 311 */
												obj_t BgL_hz00_1595;

												BgL_hz00_1595 =
													BGl_queuezd2getz12zc0zz__ssrz00(BgL_queuez00_1589);
												{	/* Unsafe/ssr.scm 311 */
													obj_t BgL_nz00_1596;

													BgL_nz00_1596 =
														BGl_queuezd2getz12zc0zz__ssrz00(BgL_queuez00_1589);
													{	/* Unsafe/ssr.scm 312 */

														BGl_hoistze70ze7zz__ssrz00(BgL_queuez00_1589,
															BgL_onconnectz00_92, BgL_graphz00_89,
															BgL_hz00_1595, BgL_nz00_1596);
													}
												}
											}
											goto BgL_zc3z04anonymousza31459ze3z87_1593;
										}
									return BBOOL(BgL_tmpz00_4000);
								}
							}
						}
				}
			}
		}

	}



/* hoist~0 */
	obj_t BGl_hoistze70ze7zz__ssrz00(obj_t BgL_queuez00_3457,
		obj_t BgL_onconnectz00_3456, obj_t BgL_graphz00_3455,
		obj_t BgL_hoisterz00_1598, obj_t BgL_nodez00_1599)
	{
		{	/* Unsafe/ssr.scm 305 */
			{	/* Unsafe/ssr.scm 296 */
				bool_t BgL_test2144z00_4008;

				if (BGl_cleanzd2edgezf3z21zz__ssrz00(BgL_graphz00_3455,
						BgL_hoisterz00_1598, BgL_nodez00_1599))
					{	/* Unsafe/ssr.scm 260 */
						BgL_test2144z00_4008 = ((bool_t) 0);
					}
				else
					{	/* Unsafe/ssr.scm 260 */
						BgL_test2144z00_4008 = ((bool_t) 1);
					}
				if (BgL_test2144z00_4008)
					{	/* Unsafe/ssr.scm 296 */
						BGl_setzd2parentz12zc0zz__ssrz00(BgL_graphz00_3455,
							BgL_nodez00_1599, BgL_hoisterz00_1598);
						{	/* Unsafe/ssr.scm 298 */
							bool_t BgL_test2146z00_4012;

							if (CBOOL(BgL_onconnectz00_3456))
								{	/* Unsafe/ssr.scm 298 */
									obj_t BgL_a1202z00_1605;

									{	/* Unsafe/ssr.scm 214 */
										obj_t BgL__ortest_1095z00_2701;

										BgL__ortest_1095z00_2701 =
											BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
											(BgL_graphz00_3455, 1L), BgL_nodez00_1599);
										if (CBOOL(BgL__ortest_1095z00_2701))
											{	/* Unsafe/ssr.scm 214 */
												BgL_a1202z00_1605 = BgL__ortest_1095z00_2701;
											}
										else
											{	/* Unsafe/ssr.scm 214 */
												BgL_a1202z00_1605 =
													BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
											}
									}
									BgL_test2146z00_4012 =
										BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_a1202z00_1605,
										BGL_REAL_CNST(BGl_real2018z00zz__ssrz00));
								}
							else
								{	/* Unsafe/ssr.scm 298 */
									BgL_test2146z00_4012 = ((bool_t) 0);
								}
							if (BgL_test2146z00_4012)
								{	/* Unsafe/ssr.scm 298 */
									BGL_PROCEDURE_CALL1(BgL_onconnectz00_3456, BgL_nodez00_1599);
								}
							else
								{	/* Unsafe/ssr.scm 298 */
									BFALSE;
								}
						}
						BGl_updatezd2rankz12zc0zz__ssrz00(BgL_graphz00_3455,
							BgL_nodez00_1599);
						{	/* Unsafe/ssr.scm 302 */
							obj_t BgL_zc3z04anonymousza31466ze3z87_3401;

							{
								int BgL_tmpz00_4025;

								BgL_tmpz00_4025 = (int) (2L);
								BgL_zc3z04anonymousza31466ze3z87_3401 =
									MAKE_L_PROCEDURE
									(BGl_z62zc3z04anonymousza31466ze3ze5zz__ssrz00,
									BgL_tmpz00_4025);
							}
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31466ze3z87_3401,
								(int) (0L), BgL_nodez00_1599);
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31466ze3z87_3401,
								(int) (1L), BgL_queuez00_3457);
							BGl_friendszd2forzd2eachz00zz__ssrz00
								(BgL_zc3z04anonymousza31466ze3z87_3401, BgL_graphz00_3455,
								BgL_nodez00_1599);
							return
								BGl_childrenzd2forzd2eachz00zz__ssrz00
								(BgL_zc3z04anonymousza31466ze3z87_3401, BgL_graphz00_3455,
								BgL_nodez00_1599);
						}
					}
				else
					{	/* Unsafe/ssr.scm 296 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1466> */
	obj_t BGl_z62zc3z04anonymousza31466ze3ze5zz__ssrz00(obj_t BgL_envz00_3402,
		obj_t BgL_nz00_3405)
	{
		{	/* Unsafe/ssr.scm 301 */
			{	/* Unsafe/ssr.scm 302 */
				obj_t BgL_nodez00_3403;
				obj_t BgL_queuez00_3404;

				BgL_nodez00_3403 = PROCEDURE_L_REF(BgL_envz00_3402, (int) (0L));
				BgL_queuez00_3404 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_3402, (int) (1L)));
				{	/* Unsafe/ssr.scm 150 */
					obj_t BgL_entryz00_3496;

					BgL_entryz00_3496 = MAKE_YOUNG_PAIR(BgL_nodez00_3403, BNIL);
					if (NULLP(CAR(BgL_queuez00_3404)))
						{	/* Unsafe/ssr.scm 151 */
							SET_CAR(BgL_queuez00_3404, BgL_entryz00_3496);
						}
					else
						{	/* Unsafe/ssr.scm 153 */
							obj_t BgL_arg1370z00_3497;

							BgL_arg1370z00_3497 = CDR(BgL_queuez00_3404);
							{	/* Unsafe/ssr.scm 153 */
								obj_t BgL_tmpz00_4045;

								BgL_tmpz00_4045 = ((obj_t) BgL_arg1370z00_3497);
								SET_CDR(BgL_tmpz00_4045, BgL_entryz00_3496);
							}
						}
					SET_CDR(BgL_queuez00_3404, BgL_entryz00_3496);
					BgL_nodez00_3403;
				}
				{	/* Unsafe/ssr.scm 150 */
					obj_t BgL_entryz00_3498;

					BgL_entryz00_3498 = MAKE_YOUNG_PAIR(BgL_nz00_3405, BNIL);
					if (NULLP(CAR(BgL_queuez00_3404)))
						{	/* Unsafe/ssr.scm 151 */
							SET_CAR(BgL_queuez00_3404, BgL_entryz00_3498);
						}
					else
						{	/* Unsafe/ssr.scm 153 */
							obj_t BgL_arg1370z00_3499;

							BgL_arg1370z00_3499 = CDR(BgL_queuez00_3404);
							{	/* Unsafe/ssr.scm 153 */
								obj_t BgL_tmpz00_4055;

								BgL_tmpz00_4055 = ((obj_t) BgL_arg1370z00_3499);
								SET_CDR(BgL_tmpz00_4055, BgL_entryz00_3498);
							}
						}
					SET_CDR(BgL_queuez00_3404, BgL_entryz00_3498);
					return BgL_nz00_3405;
				}
			}
		}

	}



/* _ssr-remove-edge! */
	obj_t BGl__ssrzd2removezd2edgez12z12zz__ssrz00(obj_t BgL_env1261z00_100,
		obj_t BgL_opt1260z00_99)
	{
		{	/* Unsafe/ssr.scm 315 */
			{	/* Unsafe/ssr.scm 315 */
				obj_t BgL_graphz00_1616;
				obj_t BgL_fromz00_1617;
				obj_t BgL_toz00_1618;

				BgL_graphz00_1616 = VECTOR_REF(BgL_opt1260z00_99, 0L);
				BgL_fromz00_1617 = VECTOR_REF(BgL_opt1260z00_99, 1L);
				BgL_toz00_1618 = VECTOR_REF(BgL_opt1260z00_99, 2L);
				{	/* Unsafe/ssr.scm 315 */
					obj_t BgL_ondisconnectz00_1619;

					BgL_ondisconnectz00_1619 = BFALSE;
					{	/* Unsafe/ssr.scm 315 */

						{
							long BgL_iz00_1620;

							BgL_iz00_1620 = 3L;
						BgL_check1264z00_1621:
							if ((BgL_iz00_1620 == VECTOR_LENGTH(BgL_opt1260z00_99)))
								{	/* Unsafe/ssr.scm 315 */
									BNIL;
								}
							else
								{	/* Unsafe/ssr.scm 315 */
									bool_t BgL_test2152z00_4065;

									{	/* Unsafe/ssr.scm 315 */
										obj_t BgL_arg1474z00_1627;

										BgL_arg1474z00_1627 =
											VECTOR_REF(BgL_opt1260z00_99, BgL_iz00_1620);
										BgL_test2152z00_4065 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1474z00_1627, BGl_list2029z00zz__ssrz00));
									}
									if (BgL_test2152z00_4065)
										{
											long BgL_iz00_4069;

											BgL_iz00_4069 = (BgL_iz00_1620 + 2L);
											BgL_iz00_1620 = BgL_iz00_4069;
											goto BgL_check1264z00_1621;
										}
									else
										{	/* Unsafe/ssr.scm 315 */
											obj_t BgL_arg1473z00_1626;

											BgL_arg1473z00_1626 =
												VECTOR_REF(BgL_opt1260z00_99, BgL_iz00_1620);
											BGl_errorz00zz__errorz00(BGl_symbol2032z00zz__ssrz00,
												BGl_string2013z00zz__ssrz00, BgL_arg1473z00_1626);
										}
								}
						}
						{	/* Unsafe/ssr.scm 315 */
							obj_t BgL_index1266z00_1628;

							{
								long BgL_iz00_2750;

								BgL_iz00_2750 = 3L;
							BgL_search1263z00_2749:
								if ((BgL_iz00_2750 == VECTOR_LENGTH(BgL_opt1260z00_99)))
									{	/* Unsafe/ssr.scm 315 */
										BgL_index1266z00_1628 = BINT(-1L);
									}
								else
									{	/* Unsafe/ssr.scm 315 */
										if (
											(BgL_iz00_2750 ==
												(VECTOR_LENGTH(BgL_opt1260z00_99) - 1L)))
											{	/* Unsafe/ssr.scm 315 */
												BgL_index1266z00_1628 =
													BGl_errorz00zz__errorz00(BGl_symbol2032z00zz__ssrz00,
													BGl_string2026z00zz__ssrz00,
													BINT(VECTOR_LENGTH(BgL_opt1260z00_99)));
											}
										else
											{	/* Unsafe/ssr.scm 315 */
												obj_t BgL_vz00_2760;

												BgL_vz00_2760 =
													VECTOR_REF(BgL_opt1260z00_99, BgL_iz00_2750);
												if ((BgL_vz00_2760 == BGl_keyword2030z00zz__ssrz00))
													{	/* Unsafe/ssr.scm 315 */
														BgL_index1266z00_1628 = BINT((BgL_iz00_2750 + 1L));
													}
												else
													{
														long BgL_iz00_4089;

														BgL_iz00_4089 = (BgL_iz00_2750 + 2L);
														BgL_iz00_2750 = BgL_iz00_4089;
														goto BgL_search1263z00_2749;
													}
											}
									}
							}
							{	/* Unsafe/ssr.scm 315 */
								bool_t BgL_test2156z00_4091;

								{	/* Unsafe/ssr.scm 315 */
									long BgL_n1z00_2764;

									{	/* Unsafe/ssr.scm 315 */
										obj_t BgL_tmpz00_4092;

										if (INTEGERP(BgL_index1266z00_1628))
											{	/* Unsafe/ssr.scm 315 */
												BgL_tmpz00_4092 = BgL_index1266z00_1628;
											}
										else
											{
												obj_t BgL_auxz00_4095;

												BgL_auxz00_4095 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(11438L),
													BGl_string2034z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_index1266z00_1628);
												FAILURE(BgL_auxz00_4095, BFALSE, BFALSE);
											}
										BgL_n1z00_2764 = (long) CINT(BgL_tmpz00_4092);
									}
									BgL_test2156z00_4091 = (BgL_n1z00_2764 >= 0L);
								}
								if (BgL_test2156z00_4091)
									{
										long BgL_auxz00_4101;

										{	/* Unsafe/ssr.scm 315 */
											obj_t BgL_tmpz00_4102;

											if (INTEGERP(BgL_index1266z00_1628))
												{	/* Unsafe/ssr.scm 315 */
													BgL_tmpz00_4102 = BgL_index1266z00_1628;
												}
											else
												{
													obj_t BgL_auxz00_4105;

													BgL_auxz00_4105 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2015z00zz__ssrz00, BINT(11438L),
														BGl_string2034z00zz__ssrz00,
														BGl_string2017z00zz__ssrz00, BgL_index1266z00_1628);
													FAILURE(BgL_auxz00_4105, BFALSE, BFALSE);
												}
											BgL_auxz00_4101 = (long) CINT(BgL_tmpz00_4102);
										}
										BgL_ondisconnectz00_1619 =
											VECTOR_REF(BgL_opt1260z00_99, BgL_auxz00_4101);
									}
								else
									{	/* Unsafe/ssr.scm 315 */
										BFALSE;
									}
							}
						}
						{	/* Unsafe/ssr.scm 315 */
							obj_t BgL_arg1476z00_1630;
							obj_t BgL_arg1477z00_1631;
							obj_t BgL_arg1478z00_1632;

							BgL_arg1476z00_1630 = VECTOR_REF(BgL_opt1260z00_99, 0L);
							BgL_arg1477z00_1631 = VECTOR_REF(BgL_opt1260z00_99, 1L);
							BgL_arg1478z00_1632 = VECTOR_REF(BgL_opt1260z00_99, 2L);
							{	/* Unsafe/ssr.scm 315 */
								obj_t BgL_ondisconnectz00_1633;

								BgL_ondisconnectz00_1633 = BgL_ondisconnectz00_1619;
								{	/* Unsafe/ssr.scm 315 */
									obj_t BgL_graphz00_2765;
									long BgL_fromz00_2766;
									long BgL_toz00_2767;

									if (VECTORP(BgL_arg1476z00_1630))
										{	/* Unsafe/ssr.scm 315 */
											BgL_graphz00_2765 = BgL_arg1476z00_1630;
										}
									else
										{
											obj_t BgL_auxz00_4116;

											BgL_auxz00_4116 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2015z00zz__ssrz00, BINT(11438L),
												BGl_string2034z00zz__ssrz00,
												BGl_string2028z00zz__ssrz00, BgL_arg1476z00_1630);
											FAILURE(BgL_auxz00_4116, BFALSE, BFALSE);
										}
									{	/* Unsafe/ssr.scm 315 */
										obj_t BgL_tmpz00_4120;

										if (INTEGERP(BgL_arg1477z00_1631))
											{	/* Unsafe/ssr.scm 315 */
												BgL_tmpz00_4120 = BgL_arg1477z00_1631;
											}
										else
											{
												obj_t BgL_auxz00_4123;

												BgL_auxz00_4123 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(11438L),
													BGl_string2034z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_arg1477z00_1631);
												FAILURE(BgL_auxz00_4123, BFALSE, BFALSE);
											}
										BgL_fromz00_2766 = (long) CINT(BgL_tmpz00_4120);
									}
									{	/* Unsafe/ssr.scm 315 */
										obj_t BgL_tmpz00_4128;

										if (INTEGERP(BgL_arg1478z00_1632))
											{	/* Unsafe/ssr.scm 315 */
												BgL_tmpz00_4128 = BgL_arg1478z00_1632;
											}
										else
											{
												obj_t BgL_auxz00_4131;

												BgL_auxz00_4131 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2015z00zz__ssrz00, BINT(11438L),
													BGl_string2034z00zz__ssrz00,
													BGl_string2017z00zz__ssrz00, BgL_arg1478z00_1632);
												FAILURE(BgL_auxz00_4131, BFALSE, BFALSE);
											}
										BgL_toz00_2767 = (long) CINT(BgL_tmpz00_4128);
									}
									if (
										(BINT(BgL_fromz00_2766) ==
											BGl_getzd2parentzd2zz__ssrz00(BgL_graphz00_2765,
												BgL_toz00_2767)))
										{	/* Unsafe/ssr.scm 317 */
											return
												BGl_removezd2parentzd2edgez12z12zz__ssrz00
												(BgL_graphz00_2765, BgL_toz00_2767,
												BgL_ondisconnectz00_1633);
										}
									else
										{	/* Unsafe/ssr.scm 317 */
											return
												BBOOL(BGl_removezd2friendz12zc0zz__ssrz00
												(BgL_graphz00_2765, BINT(BgL_fromz00_2766),
													BINT(BgL_toz00_2767)));
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* ssr-remove-edge! */
	BGL_EXPORTED_DEF obj_t BGl_ssrzd2removezd2edgez12z12zz__ssrz00(obj_t
		BgL_graphz00_95, long BgL_fromz00_96, long BgL_toz00_97,
		obj_t BgL_ondisconnectz00_98)
	{
		{	/* Unsafe/ssr.scm 315 */
			{	/* Unsafe/ssr.scm 317 */
				bool_t BgL_test2163z00_4145;

				{	/* Unsafe/ssr.scm 287 */
					obj_t BgL_arg1440z00_2773;

					{	/* Unsafe/ssr.scm 235 */
						obj_t BgL__ortest_1101z00_2775;

						BgL__ortest_1101z00_2775 =
							BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_95, 2L),
							BINT(BgL_toz00_97));
						if (CBOOL(BgL__ortest_1101z00_2775))
							{	/* Unsafe/ssr.scm 235 */
								BgL_arg1440z00_2773 = BgL__ortest_1101z00_2775;
							}
						else
							{	/* Unsafe/ssr.scm 235 */
								BgL_arg1440z00_2773 = BFALSE;
							}
					}
					BgL_test2163z00_4145 = (BINT(BgL_fromz00_96) == BgL_arg1440z00_2773);
				}
				if (BgL_test2163z00_4145)
					{	/* Unsafe/ssr.scm 317 */
						return
							BGl_removezd2parentzd2edgez12z12zz__ssrz00(BgL_graphz00_95,
							BgL_toz00_97, BgL_ondisconnectz00_98);
					}
				else
					{	/* Unsafe/ssr.scm 317 */
						return
							BBOOL(BGl_removezd2friendz12zc0zz__ssrz00(BgL_graphz00_95,
								BINT(BgL_fromz00_96), BINT(BgL_toz00_97)));
					}
			}
		}

	}



/* remove-parent-edge! */
	obj_t BGl_removezd2parentzd2edgez12z12zz__ssrz00(obj_t BgL_graphz00_101,
		long BgL_toz00_102, obj_t BgL_ondisconnectz00_103)
	{
		{	/* Unsafe/ssr.scm 322 */
			{	/* Unsafe/ssr.scm 323 */
				obj_t BgL_loosezd2queuezd2_1670;

				BgL_loosezd2queuezd2_1670 = MAKE_YOUNG_PAIR(BNIL, BNIL);
				{	/* Unsafe/ssr.scm 324 */
					obj_t BgL_catchzd2queuezd2_1671;

					BgL_catchzd2queuezd2_1671 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{	/* Unsafe/ssr.scm 325 */
						obj_t BgL_loosezd2setzd2_1672;

						BgL_loosezd2setzd2_1672 = BGl_makezd2setzd2zz__ssrz00();
						{	/* Unsafe/ssr.scm 326 */
							obj_t BgL_anchorzd2tablezd2_1673;

							BgL_anchorzd2tablezd2_1673 =
								BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
								(BGl_real2007z00zz__ssrz00),
								BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BFALSE, BINT(10L),
								BINT(16384L), BFALSE, BINT(128L), BGl_symbol2005z00zz__ssrz00);
							{	/* Unsafe/ssr.scm 327 */
								obj_t BgL_disconnectedz00_1674;

								BgL_disconnectedz00_1674 = BGl_makezd2setzd2zz__ssrz00();
								{
									obj_t BgL_nodez00_1772;

									{
										long BgL_toz00_1725;

										BgL_toz00_1725 = BgL_toz00_102;
										{	/* Unsafe/ssr.scm 349 */
											obj_t BgL_dropzd2queuezd2_1727;

											BgL_dropzd2queuezd2_1727 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{	/* Unsafe/ssr.scm 150 */
												obj_t BgL_entryz00_2832;

												BgL_entryz00_2832 =
													MAKE_YOUNG_PAIR(BINT(BgL_toz00_1725), BNIL);
												if (NULLP(CAR(BgL_dropzd2queuezd2_1727)))
													{	/* Unsafe/ssr.scm 151 */
														SET_CAR(BgL_dropzd2queuezd2_1727,
															BgL_entryz00_2832);
													}
												else
													{	/* Unsafe/ssr.scm 153 */
														obj_t BgL_arg1370z00_2834;

														BgL_arg1370z00_2834 = CDR(BgL_dropzd2queuezd2_1727);
														{	/* Unsafe/ssr.scm 153 */
															obj_t BgL_tmpz00_4174;

															BgL_tmpz00_4174 = ((obj_t) BgL_arg1370z00_2834);
															SET_CDR(BgL_tmpz00_4174, BgL_entryz00_2832);
														}
													}
												SET_CDR(BgL_dropzd2queuezd2_1727, BgL_entryz00_2832);
												BgL_toz00_1725;
											}
											{	/* Unsafe/ssr.scm 352 */
												obj_t BgL_arg1525z00_1728;

												{	/* Unsafe/ssr.scm 352 */
													obj_t BgL_arg1526z00_1729;

													{	/* Unsafe/ssr.scm 235 */
														obj_t BgL__ortest_1101z00_2842;

														BgL__ortest_1101z00_2842 =
															BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
															(BgL_graphz00_101, 2L), BINT(BgL_toz00_1725));
														if (CBOOL(BgL__ortest_1101z00_2842))
															{	/* Unsafe/ssr.scm 235 */
																BgL_arg1526z00_1729 = BgL__ortest_1101z00_2842;
															}
														else
															{	/* Unsafe/ssr.scm 235 */
																BgL_arg1526z00_1729 = BFALSE;
															}
													}
													{	/* Unsafe/ssr.scm 214 */
														obj_t BgL__ortest_1095z00_2846;

														BgL__ortest_1095z00_2846 =
															BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
															(BgL_graphz00_101, 1L), BgL_arg1526z00_1729);
														if (CBOOL(BgL__ortest_1095z00_2846))
															{	/* Unsafe/ssr.scm 214 */
																BgL_arg1525z00_1728 = BgL__ortest_1095z00_2846;
															}
														else
															{	/* Unsafe/ssr.scm 214 */
																BgL_arg1525z00_1728 =
																	BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
															}
													}
												}
												{	/* Unsafe/ssr.scm 150 */
													obj_t BgL_entryz00_2849;

													BgL_entryz00_2849 =
														MAKE_YOUNG_PAIR(BgL_arg1525z00_1728, BNIL);
													if (NULLP(CAR(BgL_dropzd2queuezd2_1727)))
														{	/* Unsafe/ssr.scm 151 */
															SET_CAR(BgL_dropzd2queuezd2_1727,
																BgL_entryz00_2849);
														}
													else
														{	/* Unsafe/ssr.scm 153 */
															obj_t BgL_arg1370z00_2851;

															BgL_arg1370z00_2851 =
																CDR(BgL_dropzd2queuezd2_1727);
															{	/* Unsafe/ssr.scm 153 */
																obj_t BgL_tmpz00_4193;

																BgL_tmpz00_4193 = ((obj_t) BgL_arg1370z00_2851);
																SET_CDR(BgL_tmpz00_4193, BgL_entryz00_2849);
															}
														}
													SET_CDR(BgL_dropzd2queuezd2_1727, BgL_entryz00_2849);
													BgL_arg1525z00_1728;
												}
											}
											{	/* Unsafe/ssr.scm 249 */
												obj_t BgL_parentz00_2858;

												{	/* Unsafe/ssr.scm 235 */
													obj_t BgL__ortest_1101z00_2861;

													BgL__ortest_1101z00_2861 =
														BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
														(BgL_graphz00_101, 2L), BINT(BgL_toz00_1725));
													if (CBOOL(BgL__ortest_1101z00_2861))
														{	/* Unsafe/ssr.scm 235 */
															BgL_parentz00_2858 = BgL__ortest_1101z00_2861;
														}
													else
														{	/* Unsafe/ssr.scm 235 */
															BgL_parentz00_2858 = BFALSE;
														}
												}
												if (CBOOL(BgL_parentz00_2858))
													{	/* Unsafe/ssr.scm 250 */
														BGl_removezd2childz12zc0zz__ssrz00(BgL_graphz00_101,
															BgL_parentz00_2858, BgL_toz00_1725);
													}
												else
													{	/* Unsafe/ssr.scm 250 */
														((bool_t) 0);
													}
											}
											BGl_hashtablezd2removez12zc0zz__hashz00(VECTOR_REF
												(BgL_graphz00_101, 2L), BINT(BgL_toz00_1725));
											{

											BgL_zc3z04anonymousza31527ze3z87_1731:
												if (NULLP(CAR(BgL_dropzd2queuezd2_1727)))
													{	/* Unsafe/ssr.scm 356 */
														((bool_t) 0);
													}
												else
													{	/* Unsafe/ssr.scm 356 */
														{	/* Unsafe/ssr.scm 357 */
															obj_t BgL_nodez00_1733;

															BgL_nodez00_1733 =
																BGl_queuezd2getz12zc0zz__ssrz00
																(BgL_dropzd2queuezd2_1727);
															{	/* Unsafe/ssr.scm 357 */
																obj_t BgL_parentzd2rankzd2_1734;

																BgL_parentzd2rankzd2_1734 =
																	BGl_queuezd2getz12zc0zz__ssrz00
																	(BgL_dropzd2queuezd2_1727);
																{	/* Unsafe/ssr.scm 358 */
																	obj_t BgL_adopterz00_1735;

																	{	/* Unsafe/ssr.scm 361 */
																		obj_t BgL_zc3z04anonymousza31536ze3z87_3414;

																		{
																			int BgL_tmpz00_4213;

																			BgL_tmpz00_4213 = (int) (2L);
																			BgL_zc3z04anonymousza31536ze3z87_3414 =
																				MAKE_EL_PROCEDURE(BgL_tmpz00_4213);
																		}
																		PROCEDURE_EL_SET
																			(BgL_zc3z04anonymousza31536ze3z87_3414,
																			(int) (0L), BgL_graphz00_101);
																		PROCEDURE_EL_SET
																			(BgL_zc3z04anonymousza31536ze3z87_3414,
																			(int) (1L), BgL_parentzd2rankzd2_1734);
																		{	/* Unsafe/ssr.scm 277 */
																			obj_t BgL_friendliesz00_2873;

																			{	/* Unsafe/ssr.scm 277 */
																				obj_t BgL__ortest_1112z00_2875;

																				BgL__ortest_1112z00_2875 =
																					BGl_hashtablezd2getzd2zz__hashz00
																					(VECTOR_REF(BgL_graphz00_101, 5L),
																					BgL_nodez00_1733);
																				if (CBOOL(BgL__ortest_1112z00_2875))
																					{	/* Unsafe/ssr.scm 277 */
																						BgL_friendliesz00_2873 =
																							BgL__ortest_1112z00_2875;
																					}
																				else
																					{	/* Unsafe/ssr.scm 277 */
																						BgL_friendliesz00_2873 = BFALSE;
																					}
																			}
																			if (CBOOL(BgL_friendliesz00_2873))
																				{	/* Unsafe/ssr.scm 278 */
																					BgL_adopterz00_1735 =
																						BGl_zc3z04exitza31375ze3ze70z60zz__ssrz00
																						(BgL_friendliesz00_2873,
																						BgL_zc3z04anonymousza31536ze3z87_3414);
																				}
																			else
																				{	/* Unsafe/ssr.scm 278 */
																					BgL_adopterz00_1735 = BFALSE;
																				}
																		}
																	}
																	{	/* Unsafe/ssr.scm 359 */

																		if (CBOOL(BgL_adopterz00_1735))
																			{	/* Unsafe/ssr.scm 364 */
																				BGl_setzd2parentz12zc0zz__ssrz00
																					(BgL_graphz00_101, BgL_nodez00_1733,
																					BgL_adopterz00_1735);
																			}
																		else
																			{	/* Unsafe/ssr.scm 369 */
																				obj_t BgL_rankz00_1736;

																				{	/* Unsafe/ssr.scm 214 */
																					obj_t BgL__ortest_1095z00_2879;

																					BgL__ortest_1095z00_2879 =
																						BGl_hashtablezd2getzd2zz__hashz00
																						(VECTOR_REF(BgL_graphz00_101, 1L),
																						BgL_nodez00_1733);
																					if (CBOOL(BgL__ortest_1095z00_2879))
																						{	/* Unsafe/ssr.scm 214 */
																							BgL_rankz00_1736 =
																								BgL__ortest_1095z00_2879;
																						}
																					else
																						{	/* Unsafe/ssr.scm 214 */
																							BgL_rankz00_1736 =
																								BGL_REAL_CNST
																								(BGl_real2018z00zz__ssrz00);
																						}
																				}
																				BgL_nodez00_1772 = BgL_nodez00_1733;
																				{	/* Unsafe/ssr.scm 330 */
																					obj_t BgL_rankz00_1774;

																					{	/* Unsafe/ssr.scm 214 */
																						obj_t BgL__ortest_1095z00_2810;

																						BgL__ortest_1095z00_2810 =
																							BGl_hashtablezd2getzd2zz__hashz00
																							(VECTOR_REF(BgL_graphz00_101, 1L),
																							BgL_nodez00_1772);
																						if (CBOOL(BgL__ortest_1095z00_2810))
																							{	/* Unsafe/ssr.scm 214 */
																								BgL_rankz00_1774 =
																									BgL__ortest_1095z00_2810;
																							}
																						else
																							{	/* Unsafe/ssr.scm 214 */
																								BgL_rankz00_1774 =
																									BGL_REAL_CNST
																									(BGl_real2018z00zz__ssrz00);
																							}
																					}
																					{	/* Unsafe/ssr.scm 330 */
																						obj_t BgL_bucketz00_1775;

																						{	/* Unsafe/ssr.scm 331 */
																							obj_t BgL__ortest_1122z00_1778;

																							BgL__ortest_1122z00_1778 =
																								BGl_hashtablezd2getzd2zz__hashz00
																								(BgL_anchorzd2tablezd2_1673,
																								BgL_rankz00_1774);
																							if (CBOOL
																								(BgL__ortest_1122z00_1778))
																								{	/* Unsafe/ssr.scm 331 */
																									BgL_bucketz00_1775 =
																										BgL__ortest_1122z00_1778;
																								}
																							else
																								{	/* Unsafe/ssr.scm 331 */
																									BgL_bucketz00_1775 = BFALSE;
																								}
																						}
																						{	/* Unsafe/ssr.scm 331 */

																							if (CBOOL(BgL_bucketz00_1775))
																								{	/* Unsafe/ssr.scm 332 */
																									BGl_hashtablezd2removez12zc0zz__hashz00
																										(BgL_bucketz00_1775,
																										BgL_nodez00_1772);
																									if (BGl_setzd2emptyzf3z21zz__ssrz00(BgL_bucketz00_1775))
																										{	/* Unsafe/ssr.scm 334 */
																											BGl_hashtablezd2removez12zc0zz__hashz00
																												(BgL_anchorzd2tablezd2_1673,
																												BgL_rankz00_1774);
																										}
																									else
																										{	/* Unsafe/ssr.scm 334 */
																											((bool_t) 0);
																										}
																								}
																							else
																								{	/* Unsafe/ssr.scm 332 */
																									((bool_t) 0);
																								}
																							BGl_hashtablezd2putz12zc0zz__hashz00
																								(BgL_disconnectedz00_1674,
																								BgL_nodez00_1772, BTRUE);
																							BGl_hashtablezd2putz12zc0zz__hashz00
																								(VECTOR_REF(BgL_graphz00_101,
																									1L), BgL_nodez00_1772,
																								BGL_REAL_CNST
																								(BGl_real2018z00zz__ssrz00));
																						}
																					}
																				}
																				{	/* Unsafe/ssr.scm 265 */
																					obj_t BgL_childrenz00_2900;

																					{	/* Unsafe/ssr.scm 265 */
																						obj_t BgL__ortest_1106z00_2902;

																						BgL__ortest_1106z00_2902 =
																							BGl_hashtablezd2getzd2zz__hashz00
																							(VECTOR_REF(BgL_graphz00_101, 3L),
																							BgL_nodez00_1733);
																						if (CBOOL(BgL__ortest_1106z00_2902))
																							{	/* Unsafe/ssr.scm 265 */
																								BgL_childrenz00_2900 =
																									BgL__ortest_1106z00_2902;
																							}
																						else
																							{	/* Unsafe/ssr.scm 265 */
																								BgL_childrenz00_2900 = BFALSE;
																							}
																					}
																					if (CBOOL(BgL_childrenz00_2900))
																						{	/* Unsafe/ssr.scm 164 */
																							obj_t
																								BgL_zc3z04anonymousza31374ze3z87_3413;
																							BgL_zc3z04anonymousza31374ze3z87_3413
																								=
																								MAKE_FX_PROCEDURE
																								(BGl_z62zc3z04anonymousza31374ze31979ze5zz__ssrz00,
																								(int) (2L), (int) (2L));
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31374ze3z87_3413,
																								(int) (0L),
																								BgL_dropzd2queuezd2_1727);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31374ze3z87_3413,
																								(int) (1L), BgL_rankz00_1736);
																							BGl_hashtablezd2forzd2eachz00zz__hashz00
																								(BgL_childrenz00_2900,
																								BgL_zc3z04anonymousza31374ze3z87_3413);
																						}
																					else
																						{	/* Unsafe/ssr.scm 266 */
																							BFALSE;
																						}
																				}
																				{	/* Unsafe/ssr.scm 274 */
																					obj_t BgL_friendliesz00_2915;

																					{	/* Unsafe/ssr.scm 274 */
																						obj_t BgL__ortest_1110z00_2917;

																						BgL__ortest_1110z00_2917 =
																							BGl_hashtablezd2getzd2zz__hashz00
																							(VECTOR_REF(BgL_graphz00_101, 5L),
																							BgL_nodez00_1733);
																						if (CBOOL(BgL__ortest_1110z00_2917))
																							{	/* Unsafe/ssr.scm 274 */
																								BgL_friendliesz00_2915 =
																									BgL__ortest_1110z00_2917;
																							}
																						else
																							{	/* Unsafe/ssr.scm 274 */
																								BgL_friendliesz00_2915 = BFALSE;
																							}
																					}
																					if (CBOOL(BgL_friendliesz00_2915))
																						{	/* Unsafe/ssr.scm 164 */
																							obj_t
																								BgL_zc3z04anonymousza31374ze3z87_3412;
																							BgL_zc3z04anonymousza31374ze3z87_3412
																								=
																								MAKE_FX_PROCEDURE
																								(BGl_z62zc3z04anonymousza31374ze31978ze5zz__ssrz00,
																								(int) (2L), (int) (2L));
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31374ze3z87_3412,
																								(int) (0L), BgL_graphz00_101);
																							PROCEDURE_SET
																								(BgL_zc3z04anonymousza31374ze3z87_3412,
																								(int) (1L),
																								BgL_anchorzd2tablezd2_1673);
																							BGl_hashtablezd2forzd2eachz00zz__hashz00
																								(BgL_friendliesz00_2915,
																								BgL_zc3z04anonymousza31374ze3z87_3412);
																						}
																					else
																						{	/* Unsafe/ssr.scm 275 */
																							BFALSE;
																						}
																				}
																			}
																	}
																}
															}
														}
														goto BgL_zc3z04anonymousza31527ze3z87_1731;
													}
											}
										}
										{	/* Unsafe/ssr.scm 398 */
											obj_t BgL_bucketsz00_1682;

											BgL_bucketsz00_1682 =
												BGl_sortz00zz__r4_vectors_6_8z00
												(BGl_proc2038z00zz__ssrz00,
												BGl_hashtablezd2mapzd2zz__hashz00
												(BgL_anchorzd2tablezd2_1673,
													BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00));
											{	/* Unsafe/ssr.scm 399 */

												{
													obj_t BgL_l1210z00_1684;

													BgL_l1210z00_1684 = BgL_bucketsz00_1682;
												BgL_zc3z04anonymousza31502ze3z87_1685:
													if (PAIRP(BgL_l1210z00_1684))
														{	/* Unsafe/ssr.scm 400 */
															{	/* Unsafe/ssr.scm 406 */
																obj_t BgL_bucketz00_1687;

																BgL_bucketz00_1687 = CAR(BgL_l1210z00_1684);
																{	/* Unsafe/ssr.scm 402 */
																	obj_t BgL_rankz00_1688;
																	obj_t BgL_headz00_1689;

																	BgL_rankz00_1688 =
																		CAR(((obj_t) BgL_bucketz00_1687));
																	if (NULLP(CAR(BgL_catchzd2queuezd2_1671)))
																		{	/* Unsafe/ssr.scm 156 */
																			BgL_headz00_1689 = BFALSE;
																		}
																	else
																		{	/* Unsafe/ssr.scm 156 */
																			BgL_headz00_1689 =
																				CAR(CAR(BgL_catchzd2queuezd2_1671));
																		}
																	{
																		obj_t BgL_nextz00_1691;

																		BgL_nextz00_1691 = BgL_headz00_1689;
																	BgL_zc3z04anonymousza31504ze3z87_1692:
																		{	/* Unsafe/ssr.scm 404 */
																			bool_t BgL_test2186z00_4290;

																			if (CBOOL(BgL_nextz00_1691))
																				{	/* Unsafe/ssr.scm 405 */
																					obj_t BgL_a1209z00_1698;

																					{	/* Unsafe/ssr.scm 214 */
																						obj_t BgL__ortest_1095z00_2949;

																						BgL__ortest_1095z00_2949 =
																							BGl_hashtablezd2getzd2zz__hashz00
																							(VECTOR_REF(BgL_graphz00_101, 1L),
																							BgL_nextz00_1691);
																						if (CBOOL(BgL__ortest_1095z00_2949))
																							{	/* Unsafe/ssr.scm 214 */
																								BgL_a1209z00_1698 =
																									BgL__ortest_1095z00_2949;
																							}
																						else
																							{	/* Unsafe/ssr.scm 214 */
																								BgL_a1209z00_1698 =
																									BGL_REAL_CNST
																									(BGl_real2018z00zz__ssrz00);
																							}
																					}
																					{	/* Unsafe/ssr.scm 405 */
																						bool_t BgL_test2189z00_4297;

																						if (INTEGERP(BgL_a1209z00_1698))
																							{	/* Unsafe/ssr.scm 405 */
																								BgL_test2189z00_4297 =
																									INTEGERP(BgL_rankz00_1688);
																							}
																						else
																							{	/* Unsafe/ssr.scm 405 */
																								BgL_test2189z00_4297 =
																									((bool_t) 0);
																							}
																						if (BgL_test2189z00_4297)
																							{	/* Unsafe/ssr.scm 405 */
																								BgL_test2186z00_4290 =
																									(
																									(long) CINT(BgL_a1209z00_1698)
																									>
																									(long)
																									CINT(BgL_rankz00_1688));
																							}
																						else
																							{	/* Unsafe/ssr.scm 405 */
																								BgL_test2186z00_4290 =
																									BGl_2ze3ze3zz__r4_numbers_6_5z00
																									(BgL_a1209z00_1698,
																									BgL_rankz00_1688);
																							}
																					}
																				}
																			else
																				{	/* Unsafe/ssr.scm 405 */
																					BgL_test2186z00_4290 = ((bool_t) 1);
																				}
																			if (BgL_test2186z00_4290)
																				{	/* Unsafe/ssr.scm 404 */
																					((bool_t) 0);
																				}
																			else
																				{	/* Unsafe/ssr.scm 404 */
																					BGl_z62catchz62zz__ssrz00
																						(BgL_catchzd2queuezd2_1671,
																						BgL_disconnectedz00_1674,
																						BgL_graphz00_101,
																						BGl_queuezd2getz12zc0zz__ssrz00
																						(BgL_catchzd2queuezd2_1671));
																					{	/* Unsafe/ssr.scm 404 */
																						obj_t BgL_arg1508z00_1697;

																						if (NULLP(CAR
																								(BgL_catchzd2queuezd2_1671)))
																							{	/* Unsafe/ssr.scm 156 */
																								BgL_arg1508z00_1697 = BFALSE;
																							}
																						else
																							{	/* Unsafe/ssr.scm 156 */
																								BgL_arg1508z00_1697 =
																									CAR(CAR
																									(BgL_catchzd2queuezd2_1671));
																							}
																						{
																							obj_t BgL_nextz00_4312;

																							BgL_nextz00_4312 =
																								BgL_arg1508z00_1697;
																							BgL_nextz00_1691 =
																								BgL_nextz00_4312;
																							goto
																								BgL_zc3z04anonymousza31504ze3z87_1692;
																						}
																					}
																				}
																		}
																	}
																}
																{	/* Unsafe/ssr.scm 407 */
																	obj_t BgL_arg1509z00_1701;

																	BgL_arg1509z00_1701 =
																		CDR(((obj_t) BgL_bucketz00_1687));
																	{	/* Unsafe/ssr.scm 164 */
																		obj_t BgL_zc3z04anonymousza31374ze3z87_3410;

																		BgL_zc3z04anonymousza31374ze3z87_3410 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31374ze31977ze5zz__ssrz00,
																			(int) (2L), (int) (3L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31374ze3z87_3410,
																			(int) (0L), BgL_graphz00_101);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31374ze3z87_3410,
																			(int) (1L), BgL_disconnectedz00_1674);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31374ze3z87_3410,
																			(int) (2L), BgL_catchzd2queuezd2_1671);
																		BGl_hashtablezd2forzd2eachz00zz__hashz00
																			(BgL_arg1509z00_1701,
																			BgL_zc3z04anonymousza31374ze3z87_3410);
															}}}
															{
																obj_t BgL_l1210z00_4325;

																BgL_l1210z00_4325 = CDR(BgL_l1210z00_1684);
																BgL_l1210z00_1684 = BgL_l1210z00_4325;
																goto BgL_zc3z04anonymousza31502ze3z87_1685;
															}
														}
													else
														{	/* Unsafe/ssr.scm 400 */
															((bool_t) 1);
														}
												}
												{

												BgL_zc3z04anonymousza31511ze3z87_1705:
													if (NULLP(CAR(BgL_catchzd2queuezd2_1671)))
														{	/* Unsafe/ssr.scm 410 */
															((bool_t) 0);
														}
													else
														{	/* Unsafe/ssr.scm 410 */
															BGl_z62catchz62zz__ssrz00
																(BgL_catchzd2queuezd2_1671,
																BgL_disconnectedz00_1674, BgL_graphz00_101,
																BGl_queuezd2getz12zc0zz__ssrz00
																(BgL_catchzd2queuezd2_1671));
															goto BgL_zc3z04anonymousza31511ze3z87_1705;
														}
												}
												if (CBOOL(BgL_ondisconnectz00_103))
													{	/* Unsafe/ssr.scm 164 */
														obj_t BgL_zc3z04anonymousza31374ze3z87_3409;

														BgL_zc3z04anonymousza31374ze3z87_3409 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31374ze31976ze5zz__ssrz00,
															(int) (2L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31374ze3z87_3409,
															(int) (0L), BgL_ondisconnectz00_103);
														return
															BGl_hashtablezd2forzd2eachz00zz__hashz00
															(BgL_disconnectedz00_1674,
															BgL_zc3z04anonymousza31374ze3z87_3409);
													}
												else
													{	/* Unsafe/ssr.scm 412 */
														return BFALSE;
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &catch */
	obj_t BGl_z62catchz62zz__ssrz00(obj_t BgL_catchzd2queuezd2_3417,
		obj_t BgL_disconnectedz00_3416, obj_t BgL_graphz00_3415,
		obj_t BgL_nodez00_1717)
	{
		{	/* Unsafe/ssr.scm 394 */
			{	/* Unsafe/ssr.scm 388 */
				obj_t BgL_zc3z04anonymousza31522ze3z87_3407;

				{
					int BgL_tmpz00_4340;

					BgL_tmpz00_4340 = (int) (4L);
					BgL_zc3z04anonymousza31522ze3z87_3407 =
						MAKE_L_PROCEDURE(BGl_z62zc3z04anonymousza31522ze3ze5zz__ssrz00,
						BgL_tmpz00_4340);
				}
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31522ze3z87_3407,
					(int) (0L), BgL_graphz00_3415);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31522ze3z87_3407,
					(int) (1L), BgL_nodez00_1717);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31522ze3z87_3407,
					(int) (2L), BgL_disconnectedz00_3416);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31522ze3z87_3407,
					(int) (3L), BgL_catchzd2queuezd2_3417);
				BGl_friendszd2forzd2eachz00zz__ssrz00
					(BgL_zc3z04anonymousza31522ze3z87_3407, BgL_graphz00_3415,
					BgL_nodez00_1717);
				return
					BGl_childrenzd2forzd2eachz00zz__ssrz00
					(BgL_zc3z04anonymousza31522ze3z87_3407, BgL_graphz00_3415,
					BgL_nodez00_1717);
			}
		}

	}



/* &loose? */
	bool_t BGl_z62loosezf3z91zz__ssrz00(obj_t BgL_graphz00_3418,
		obj_t BgL_nodez00_1767)
	{
		{	/* Unsafe/ssr.scm 337 */
			{	/* Unsafe/ssr.scm 337 */
				obj_t BgL_a1205z00_1769;

				{	/* Unsafe/ssr.scm 214 */
					obj_t BgL__ortest_1095z00_2816;

					BgL__ortest_1095z00_2816 =
						BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_3418, 1L),
						BgL_nodez00_1767);
					if (CBOOL(BgL__ortest_1095z00_2816))
						{	/* Unsafe/ssr.scm 214 */
							BgL_a1205z00_1769 = BgL__ortest_1095z00_2816;
						}
					else
						{	/* Unsafe/ssr.scm 214 */
							BgL_a1205z00_1769 = BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
						}
				}
				return
					BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_a1205z00_1769,
					BGL_REAL_CNST(BGl_real2018z00zz__ssrz00));
			}
		}

	}



/* &<@anonymous:1374>1976 */
	obj_t BGl_z62zc3z04anonymousza31374ze31976ze5zz__ssrz00(obj_t BgL_envz00_3419,
		obj_t BgL_kz00_3421, obj_t BgL__z00_3422)
	{
		{	/* Unsafe/ssr.scm 164 */
			{	/* Unsafe/ssr.scm 164 */
				obj_t BgL_ondisconnectz00_3420;

				BgL_ondisconnectz00_3420 = PROCEDURE_REF(BgL_envz00_3419, (int) (0L));
				return BGL_PROCEDURE_CALL1(BgL_ondisconnectz00_3420, BgL_kz00_3421);
			}
		}

	}



/* &<@anonymous:1374>1977 */
	obj_t BGl_z62zc3z04anonymousza31374ze31977ze5zz__ssrz00(obj_t BgL_envz00_3423,
		obj_t BgL_kz00_3427, obj_t BgL__z00_3428)
	{
		{	/* Unsafe/ssr.scm 164 */
			{	/* Unsafe/ssr.scm 164 */
				obj_t BgL_graphz00_3424;
				obj_t BgL_disconnectedz00_3425;
				obj_t BgL_catchzd2queuezd2_3426;

				BgL_graphz00_3424 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3423, (int) (0L)));
				BgL_disconnectedz00_3425 = PROCEDURE_REF(BgL_envz00_3423, (int) (1L));
				BgL_catchzd2queuezd2_3426 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3423, (int) (2L)));
				return
					BGl_z62catchz62zz__ssrz00(BgL_catchzd2queuezd2_3426,
					BgL_disconnectedz00_3425, BgL_graphz00_3424, BgL_kz00_3427);
			}
		}

	}



/* &cmp */
	obj_t BGl_z62cmpz62zz__ssrz00(obj_t BgL_envz00_3429, obj_t BgL_xz00_3430,
		obj_t BgL_yz00_3431)
	{
		{	/* Unsafe/ssr.scm 398 */
			{	/* Unsafe/ssr.scm 398 */
				bool_t BgL_tmpz00_4373;

				{	/* Unsafe/ssr.scm 398 */
					obj_t BgL_a1207z00_3501;

					BgL_a1207z00_3501 = CAR(((obj_t) BgL_xz00_3430));
					{	/* Unsafe/ssr.scm 398 */
						obj_t BgL_b1208z00_3502;

						BgL_b1208z00_3502 = CAR(((obj_t) BgL_yz00_3431));
						{	/* Unsafe/ssr.scm 398 */

							{	/* Unsafe/ssr.scm 398 */
								bool_t BgL_test2195z00_4378;

								if (INTEGERP(BgL_a1207z00_3501))
									{	/* Unsafe/ssr.scm 398 */
										BgL_test2195z00_4378 = INTEGERP(BgL_b1208z00_3502);
									}
								else
									{	/* Unsafe/ssr.scm 398 */
										BgL_test2195z00_4378 = ((bool_t) 0);
									}
								if (BgL_test2195z00_4378)
									{	/* Unsafe/ssr.scm 398 */
										BgL_tmpz00_4373 =
											(
											(long) CINT(BgL_a1207z00_3501) <
											(long) CINT(BgL_b1208z00_3502));
									}
								else
									{	/* Unsafe/ssr.scm 398 */
										BgL_tmpz00_4373 =
											BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_a1207z00_3501,
											BgL_b1208z00_3502);
									}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_4373);
			}
		}

	}



/* &<@anonymous:1522> */
	obj_t BGl_z62zc3z04anonymousza31522ze3ze5zz__ssrz00(obj_t BgL_envz00_3432,
		obj_t BgL_neighborz00_3437)
	{
		{	/* Unsafe/ssr.scm 387 */
			{	/* Unsafe/ssr.scm 388 */
				obj_t BgL_graphz00_3433;
				obj_t BgL_nodez00_3434;
				obj_t BgL_disconnectedz00_3435;
				obj_t BgL_catchzd2queuezd2_3436;

				BgL_graphz00_3433 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_3432, (int) (0L)));
				BgL_nodez00_3434 = PROCEDURE_L_REF(BgL_envz00_3432, (int) (1L));
				BgL_disconnectedz00_3435 = PROCEDURE_L_REF(BgL_envz00_3432, (int) (2L));
				BgL_catchzd2queuezd2_3436 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_3432, (int) (3L)));
				if (BGl_z62loosezf3z91zz__ssrz00(BgL_graphz00_3433,
						BgL_neighborz00_3437))
					{	/* Unsafe/ssr.scm 388 */
						BGl_setzd2parentz12zc0zz__ssrz00(BgL_graphz00_3433,
							BgL_neighborz00_3437, BgL_nodez00_3434);
						BGl_hashtablezd2removez12zc0zz__hashz00(BgL_disconnectedz00_3435,
							BgL_neighborz00_3437);
						BGl_updatezd2rankz12zc0zz__ssrz00(BgL_graphz00_3433,
							BgL_neighborz00_3437);
						{	/* Unsafe/ssr.scm 150 */
							obj_t BgL_entryz00_3503;

							BgL_entryz00_3503 = MAKE_YOUNG_PAIR(BgL_neighborz00_3437, BNIL);
							if (NULLP(CAR(BgL_catchzd2queuezd2_3436)))
								{	/* Unsafe/ssr.scm 151 */
									SET_CAR(BgL_catchzd2queuezd2_3436, BgL_entryz00_3503);
								}
							else
								{	/* Unsafe/ssr.scm 153 */
									obj_t BgL_arg1370z00_3504;

									BgL_arg1370z00_3504 = CDR(BgL_catchzd2queuezd2_3436);
									{	/* Unsafe/ssr.scm 153 */
										obj_t BgL_tmpz00_4408;

										BgL_tmpz00_4408 = ((obj_t) BgL_arg1370z00_3504);
										SET_CDR(BgL_tmpz00_4408, BgL_entryz00_3503);
									}
								}
							SET_CDR(BgL_catchzd2queuezd2_3436, BgL_entryz00_3503);
							return BgL_neighborz00_3437;
						}
					}
				else
					{	/* Unsafe/ssr.scm 388 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1374>1978 */
	obj_t BGl_z62zc3z04anonymousza31374ze31978ze5zz__ssrz00(obj_t BgL_envz00_3438,
		obj_t BgL_kz00_3441, obj_t BgL__z00_3442)
	{
		{	/* Unsafe/ssr.scm 164 */
			{	/* Unsafe/ssr.scm 164 */
				obj_t BgL_graphz00_3439;
				obj_t BgL_anchorzd2tablezd2_3440;

				BgL_graphz00_3439 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3438, (int) (0L)));
				BgL_anchorzd2tablezd2_3440 = PROCEDURE_REF(BgL_envz00_3438, (int) (1L));
				{
					obj_t BgL_friendlyz00_3506;

					BgL_friendlyz00_3506 = BgL_kz00_3441;
					if (BGl_z62loosezf3z91zz__ssrz00(BgL_graphz00_3439,
							BgL_friendlyz00_3506))
						{	/* Unsafe/ssr.scm 381 */
							return BFALSE;
						}
					else
						{	/* Unsafe/ssr.scm 340 */
							obj_t BgL_rankz00_3507;

							{	/* Unsafe/ssr.scm 214 */
								obj_t BgL__ortest_1095z00_3508;

								BgL__ortest_1095z00_3508 =
									BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
									(BgL_graphz00_3439, 1L), BgL_friendlyz00_3506);
								if (CBOOL(BgL__ortest_1095z00_3508))
									{	/* Unsafe/ssr.scm 214 */
										BgL_rankz00_3507 = BgL__ortest_1095z00_3508;
									}
								else
									{	/* Unsafe/ssr.scm 214 */
										BgL_rankz00_3507 = BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
									}
							}
							{	/* Unsafe/ssr.scm 340 */
								obj_t BgL_bucketz00_3509;

								BgL_bucketz00_3509 =
									BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
									(BgL_anchorzd2tablezd2_3440, BgL_rankz00_3507);
								{	/* Unsafe/ssr.scm 341 */

									return
										BGl_hashtablezd2putz12zc0zz__hashz00(BgL_bucketz00_3509,
										BgL_friendlyz00_3506, BTRUE);
								}
							}
						}
				}
			}
		}

	}



/* &<@anonymous:1374>1979 */
	obj_t BGl_z62zc3z04anonymousza31374ze31979ze5zz__ssrz00(obj_t BgL_envz00_3443,
		obj_t BgL_kz00_3446, obj_t BgL__z00_3447)
	{
		{	/* Unsafe/ssr.scm 164 */
			{	/* Unsafe/ssr.scm 164 */
				obj_t BgL_dropzd2queuezd2_3444;
				obj_t BgL_rankz00_3445;

				BgL_dropzd2queuezd2_3444 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3443, (int) (0L)));
				BgL_rankz00_3445 = PROCEDURE_REF(BgL_envz00_3443, (int) (1L));
				{
					obj_t BgL_childz00_3511;

					BgL_childz00_3511 = BgL_kz00_3446;
					{	/* Unsafe/ssr.scm 150 */
						obj_t BgL_entryz00_3512;

						BgL_entryz00_3512 = MAKE_YOUNG_PAIR(BgL_childz00_3511, BNIL);
						if (NULLP(CAR(BgL_dropzd2queuezd2_3444)))
							{	/* Unsafe/ssr.scm 151 */
								SET_CAR(BgL_dropzd2queuezd2_3444, BgL_entryz00_3512);
							}
						else
							{	/* Unsafe/ssr.scm 153 */
								obj_t BgL_arg1370z00_3513;

								BgL_arg1370z00_3513 = CDR(BgL_dropzd2queuezd2_3444);
								{	/* Unsafe/ssr.scm 153 */
									obj_t BgL_tmpz00_4436;

									BgL_tmpz00_4436 = ((obj_t) BgL_arg1370z00_3513);
									SET_CDR(BgL_tmpz00_4436, BgL_entryz00_3512);
								}
							}
						SET_CDR(BgL_dropzd2queuezd2_3444, BgL_entryz00_3512);
						BgL_childz00_3511;
					}
					{	/* Unsafe/ssr.scm 150 */
						obj_t BgL_entryz00_3514;

						BgL_entryz00_3514 = MAKE_YOUNG_PAIR(BgL_rankz00_3445, BNIL);
						if (NULLP(CAR(BgL_dropzd2queuezd2_3444)))
							{	/* Unsafe/ssr.scm 151 */
								SET_CAR(BgL_dropzd2queuezd2_3444, BgL_entryz00_3514);
							}
						else
							{	/* Unsafe/ssr.scm 153 */
								obj_t BgL_arg1370z00_3515;

								BgL_arg1370z00_3515 = CDR(BgL_dropzd2queuezd2_3444);
								{	/* Unsafe/ssr.scm 153 */
									obj_t BgL_tmpz00_4446;

									BgL_tmpz00_4446 = ((obj_t) BgL_arg1370z00_3515);
									SET_CDR(BgL_tmpz00_4446, BgL_entryz00_3514);
								}
							}
						SET_CDR(BgL_dropzd2queuezd2_3444, BgL_entryz00_3514);
						return BgL_rankz00_3445;
					}
				}
			}
		}

	}



/* &<@anonymous:1536> */
	obj_t BGl_z62zc3z04anonymousza31536ze3ze5zz__ssrz00(obj_t BgL_envz00_3448,
		obj_t BgL_fz00_3451)
	{
		{	/* Unsafe/ssr.scm 361 */
			{	/* Unsafe/ssr.scm 361 */
				obj_t BgL_graphz00_3449;
				obj_t BgL_parentzd2rankzd2_3450;

				BgL_graphz00_3449 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_3448, (int) (0L)));
				BgL_parentzd2rankzd2_3450 =
					PROCEDURE_EL_REF(BgL_envz00_3448, (int) (1L));
				{	/* Unsafe/ssr.scm 361 */
					bool_t BgL_tmpz00_4455;

					{	/* Unsafe/ssr.scm 361 */
						obj_t BgL_a1206z00_3516;

						{	/* Unsafe/ssr.scm 214 */
							obj_t BgL__ortest_1095z00_3517;

							BgL__ortest_1095z00_3517 =
								BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_3449,
									1L), BgL_fz00_3451);
							if (CBOOL(BgL__ortest_1095z00_3517))
								{	/* Unsafe/ssr.scm 214 */
									BgL_a1206z00_3516 = BgL__ortest_1095z00_3517;
								}
							else
								{	/* Unsafe/ssr.scm 214 */
									BgL_a1206z00_3516 = BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
								}
						}
						{	/* Unsafe/ssr.scm 361 */
							bool_t BgL_test2204z00_4460;

							if (INTEGERP(BgL_a1206z00_3516))
								{	/* Unsafe/ssr.scm 361 */
									BgL_test2204z00_4460 = INTEGERP(BgL_parentzd2rankzd2_3450);
								}
							else
								{	/* Unsafe/ssr.scm 361 */
									BgL_test2204z00_4460 = ((bool_t) 0);
								}
							if (BgL_test2204z00_4460)
								{	/* Unsafe/ssr.scm 361 */
									BgL_tmpz00_4455 =
										(
										(long) CINT(BgL_a1206z00_3516) ==
										(long) CINT(BgL_parentzd2rankzd2_3450));
								}
							else
								{	/* Unsafe/ssr.scm 361 */
									BgL_tmpz00_4455 =
										BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_a1206z00_3516,
										BgL_parentzd2rankzd2_3450);
								}
						}
					}
					return BBOOL(BgL_tmpz00_4455);
				}
			}
		}

	}



/* _ssr-redirect! */
	obj_t BGl__ssrzd2redirectz12zc0zz__ssrz00(obj_t BgL_env1275z00_112,
		obj_t BgL_opt1274z00_111)
	{
		{	/* Unsafe/ssr.scm 414 */
			{	/* Unsafe/ssr.scm 414 */
				obj_t BgL_graphz00_1792;
				obj_t BgL_nodez00_1793;
				obj_t BgL_otherz00_1794;

				BgL_graphz00_1792 = VECTOR_REF(BgL_opt1274z00_111, 0L);
				BgL_nodez00_1793 = VECTOR_REF(BgL_opt1274z00_111, 1L);
				BgL_otherz00_1794 = VECTOR_REF(BgL_opt1274z00_111, 2L);
				{	/* Unsafe/ssr.scm 414 */
					obj_t BgL_onconnectz00_1795;

					BgL_onconnectz00_1795 = BFALSE;
					{	/* Unsafe/ssr.scm 414 */
						obj_t BgL_ondisconnectz00_1796;

						BgL_ondisconnectz00_1796 = BFALSE;
						{	/* Unsafe/ssr.scm 414 */

							{
								long BgL_iz00_1797;

								BgL_iz00_1797 = 3L;
							BgL_check1278z00_1798:
								if ((BgL_iz00_1797 == VECTOR_LENGTH(BgL_opt1274z00_111)))
									{	/* Unsafe/ssr.scm 414 */
										BNIL;
									}
								else
									{	/* Unsafe/ssr.scm 414 */
										bool_t BgL_test2207z00_4475;

										{	/* Unsafe/ssr.scm 414 */
											obj_t BgL_arg1553z00_1804;

											BgL_arg1553z00_1804 =
												VECTOR_REF(BgL_opt1274z00_111, BgL_iz00_1797);
											BgL_test2207z00_4475 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1553z00_1804, BGl_list2039z00zz__ssrz00));
										}
										if (BgL_test2207z00_4475)
											{
												long BgL_iz00_4479;

												BgL_iz00_4479 = (BgL_iz00_1797 + 2L);
												BgL_iz00_1797 = BgL_iz00_4479;
												goto BgL_check1278z00_1798;
											}
										else
											{	/* Unsafe/ssr.scm 414 */
												obj_t BgL_arg1552z00_1803;

												BgL_arg1552z00_1803 =
													VECTOR_REF(BgL_opt1274z00_111, BgL_iz00_1797);
												BGl_errorz00zz__errorz00(BGl_symbol2040z00zz__ssrz00,
													BGl_string2013z00zz__ssrz00, BgL_arg1552z00_1803);
											}
									}
							}
							{	/* Unsafe/ssr.scm 414 */
								obj_t BgL_index1280z00_1805;

								{
									long BgL_iz00_2987;

									BgL_iz00_2987 = 3L;
								BgL_search1277z00_2986:
									if ((BgL_iz00_2987 == VECTOR_LENGTH(BgL_opt1274z00_111)))
										{	/* Unsafe/ssr.scm 414 */
											BgL_index1280z00_1805 = BINT(-1L);
										}
									else
										{	/* Unsafe/ssr.scm 414 */
											if (
												(BgL_iz00_2987 ==
													(VECTOR_LENGTH(BgL_opt1274z00_111) - 1L)))
												{	/* Unsafe/ssr.scm 414 */
													BgL_index1280z00_1805 =
														BGl_errorz00zz__errorz00
														(BGl_symbol2040z00zz__ssrz00,
														BGl_string2042z00zz__ssrz00,
														BINT(VECTOR_LENGTH(BgL_opt1274z00_111)));
												}
											else
												{	/* Unsafe/ssr.scm 414 */
													obj_t BgL_vz00_2997;

													BgL_vz00_2997 =
														VECTOR_REF(BgL_opt1274z00_111, BgL_iz00_2987);
													if ((BgL_vz00_2997 == BGl_keyword2022z00zz__ssrz00))
														{	/* Unsafe/ssr.scm 414 */
															BgL_index1280z00_1805 =
																BINT((BgL_iz00_2987 + 1L));
														}
													else
														{
															long BgL_iz00_4499;

															BgL_iz00_4499 = (BgL_iz00_2987 + 2L);
															BgL_iz00_2987 = BgL_iz00_4499;
															goto BgL_search1277z00_2986;
														}
												}
										}
								}
								{	/* Unsafe/ssr.scm 414 */
									bool_t BgL_test2211z00_4501;

									{	/* Unsafe/ssr.scm 414 */
										long BgL_n1z00_3001;

										{	/* Unsafe/ssr.scm 414 */
											obj_t BgL_tmpz00_4502;

											if (INTEGERP(BgL_index1280z00_1805))
												{	/* Unsafe/ssr.scm 414 */
													BgL_tmpz00_4502 = BgL_index1280z00_1805;
												}
											else
												{
													obj_t BgL_auxz00_4505;

													BgL_auxz00_4505 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2015z00zz__ssrz00, BINT(14850L),
														BGl_string2043z00zz__ssrz00,
														BGl_string2017z00zz__ssrz00, BgL_index1280z00_1805);
													FAILURE(BgL_auxz00_4505, BFALSE, BFALSE);
												}
											BgL_n1z00_3001 = (long) CINT(BgL_tmpz00_4502);
										}
										BgL_test2211z00_4501 = (BgL_n1z00_3001 >= 0L);
									}
									if (BgL_test2211z00_4501)
										{
											long BgL_auxz00_4511;

											{	/* Unsafe/ssr.scm 414 */
												obj_t BgL_tmpz00_4512;

												if (INTEGERP(BgL_index1280z00_1805))
													{	/* Unsafe/ssr.scm 414 */
														BgL_tmpz00_4512 = BgL_index1280z00_1805;
													}
												else
													{
														obj_t BgL_auxz00_4515;

														BgL_auxz00_4515 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2015z00zz__ssrz00, BINT(14850L),
															BGl_string2043z00zz__ssrz00,
															BGl_string2017z00zz__ssrz00,
															BgL_index1280z00_1805);
														FAILURE(BgL_auxz00_4515, BFALSE, BFALSE);
													}
												BgL_auxz00_4511 = (long) CINT(BgL_tmpz00_4512);
											}
											BgL_onconnectz00_1795 =
												VECTOR_REF(BgL_opt1274z00_111, BgL_auxz00_4511);
										}
									else
										{	/* Unsafe/ssr.scm 414 */
											BFALSE;
										}
								}
							}
							{	/* Unsafe/ssr.scm 414 */
								obj_t BgL_index1281z00_1807;

								{
									long BgL_iz00_3003;

									BgL_iz00_3003 = 3L;
								BgL_search1277z00_3002:
									if ((BgL_iz00_3003 == VECTOR_LENGTH(BgL_opt1274z00_111)))
										{	/* Unsafe/ssr.scm 414 */
											BgL_index1281z00_1807 = BINT(-1L);
										}
									else
										{	/* Unsafe/ssr.scm 414 */
											if (
												(BgL_iz00_3003 ==
													(VECTOR_LENGTH(BgL_opt1274z00_111) - 1L)))
												{	/* Unsafe/ssr.scm 414 */
													BgL_index1281z00_1807 =
														BGl_errorz00zz__errorz00
														(BGl_symbol2040z00zz__ssrz00,
														BGl_string2042z00zz__ssrz00,
														BINT(VECTOR_LENGTH(BgL_opt1274z00_111)));
												}
											else
												{	/* Unsafe/ssr.scm 414 */
													obj_t BgL_vz00_3013;

													BgL_vz00_3013 =
														VECTOR_REF(BgL_opt1274z00_111, BgL_iz00_3003);
													if ((BgL_vz00_3013 == BGl_keyword2030z00zz__ssrz00))
														{	/* Unsafe/ssr.scm 414 */
															BgL_index1281z00_1807 =
																BINT((BgL_iz00_3003 + 1L));
														}
													else
														{
															long BgL_iz00_4537;

															BgL_iz00_4537 = (BgL_iz00_3003 + 2L);
															BgL_iz00_3003 = BgL_iz00_4537;
															goto BgL_search1277z00_3002;
														}
												}
										}
								}
								{	/* Unsafe/ssr.scm 414 */
									bool_t BgL_test2217z00_4539;

									{	/* Unsafe/ssr.scm 414 */
										long BgL_n1z00_3017;

										{	/* Unsafe/ssr.scm 414 */
											obj_t BgL_tmpz00_4540;

											if (INTEGERP(BgL_index1281z00_1807))
												{	/* Unsafe/ssr.scm 414 */
													BgL_tmpz00_4540 = BgL_index1281z00_1807;
												}
											else
												{
													obj_t BgL_auxz00_4543;

													BgL_auxz00_4543 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2015z00zz__ssrz00, BINT(14850L),
														BGl_string2043z00zz__ssrz00,
														BGl_string2017z00zz__ssrz00, BgL_index1281z00_1807);
													FAILURE(BgL_auxz00_4543, BFALSE, BFALSE);
												}
											BgL_n1z00_3017 = (long) CINT(BgL_tmpz00_4540);
										}
										BgL_test2217z00_4539 = (BgL_n1z00_3017 >= 0L);
									}
									if (BgL_test2217z00_4539)
										{
											long BgL_auxz00_4549;

											{	/* Unsafe/ssr.scm 414 */
												obj_t BgL_tmpz00_4550;

												if (INTEGERP(BgL_index1281z00_1807))
													{	/* Unsafe/ssr.scm 414 */
														BgL_tmpz00_4550 = BgL_index1281z00_1807;
													}
												else
													{
														obj_t BgL_auxz00_4553;

														BgL_auxz00_4553 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2015z00zz__ssrz00, BINT(14850L),
															BGl_string2043z00zz__ssrz00,
															BGl_string2017z00zz__ssrz00,
															BgL_index1281z00_1807);
														FAILURE(BgL_auxz00_4553, BFALSE, BFALSE);
													}
												BgL_auxz00_4549 = (long) CINT(BgL_tmpz00_4550);
											}
											BgL_ondisconnectz00_1796 =
												VECTOR_REF(BgL_opt1274z00_111, BgL_auxz00_4549);
										}
									else
										{	/* Unsafe/ssr.scm 414 */
											BFALSE;
										}
								}
							}
							{	/* Unsafe/ssr.scm 414 */
								obj_t BgL_arg1556z00_1809;
								obj_t BgL_arg1557z00_1810;
								obj_t BgL_arg1558z00_1811;

								BgL_arg1556z00_1809 = VECTOR_REF(BgL_opt1274z00_111, 0L);
								BgL_arg1557z00_1810 = VECTOR_REF(BgL_opt1274z00_111, 1L);
								BgL_arg1558z00_1811 = VECTOR_REF(BgL_opt1274z00_111, 2L);
								{	/* Unsafe/ssr.scm 414 */
									obj_t BgL_onconnectz00_1812;

									BgL_onconnectz00_1812 = BgL_onconnectz00_1795;
									{	/* Unsafe/ssr.scm 414 */
										obj_t BgL_ondisconnectz00_1813;

										BgL_ondisconnectz00_1813 = BgL_ondisconnectz00_1796;
										{	/* Unsafe/ssr.scm 414 */
											long BgL_auxz00_4578;
											long BgL_auxz00_4569;
											obj_t BgL_auxz00_4562;

											{	/* Unsafe/ssr.scm 414 */
												obj_t BgL_tmpz00_4579;

												if (INTEGERP(BgL_arg1558z00_1811))
													{	/* Unsafe/ssr.scm 414 */
														BgL_tmpz00_4579 = BgL_arg1558z00_1811;
													}
												else
													{
														obj_t BgL_auxz00_4582;

														BgL_auxz00_4582 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2015z00zz__ssrz00, BINT(14850L),
															BGl_string2043z00zz__ssrz00,
															BGl_string2017z00zz__ssrz00, BgL_arg1558z00_1811);
														FAILURE(BgL_auxz00_4582, BFALSE, BFALSE);
													}
												BgL_auxz00_4578 = (long) CINT(BgL_tmpz00_4579);
											}
											{	/* Unsafe/ssr.scm 414 */
												obj_t BgL_tmpz00_4570;

												if (INTEGERP(BgL_arg1557z00_1810))
													{	/* Unsafe/ssr.scm 414 */
														BgL_tmpz00_4570 = BgL_arg1557z00_1810;
													}
												else
													{
														obj_t BgL_auxz00_4573;

														BgL_auxz00_4573 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2015z00zz__ssrz00, BINT(14850L),
															BGl_string2043z00zz__ssrz00,
															BGl_string2017z00zz__ssrz00, BgL_arg1557z00_1810);
														FAILURE(BgL_auxz00_4573, BFALSE, BFALSE);
													}
												BgL_auxz00_4569 = (long) CINT(BgL_tmpz00_4570);
											}
											if (VECTORP(BgL_arg1556z00_1809))
												{	/* Unsafe/ssr.scm 414 */
													BgL_auxz00_4562 = BgL_arg1556z00_1809;
												}
											else
												{
													obj_t BgL_auxz00_4565;

													BgL_auxz00_4565 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2015z00zz__ssrz00, BINT(14850L),
														BGl_string2043z00zz__ssrz00,
														BGl_string2028z00zz__ssrz00, BgL_arg1556z00_1809);
													FAILURE(BgL_auxz00_4565, BFALSE, BFALSE);
												}
											return
												BGl_ssrzd2redirectz12zc0zz__ssrz00(BgL_auxz00_4562,
												BgL_auxz00_4569, BgL_auxz00_4578, BgL_onconnectz00_1812,
												BgL_ondisconnectz00_1813);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* ssr-redirect! */
	BGL_EXPORTED_DEF obj_t BGl_ssrzd2redirectz12zc0zz__ssrz00(obj_t
		BgL_graphz00_106, long BgL_nodez00_107, long BgL_otherz00_108,
		obj_t BgL_onconnectz00_109, obj_t BgL_ondisconnectz00_110)
	{
		{	/* Unsafe/ssr.scm 414 */
			{
				obj_t BgL_nodesz00_1892;

				if ((BgL_nodez00_107 == BgL_otherz00_108))
					{	/* Unsafe/ssr.scm 431 */
						return BFALSE;
					}
				else
					{	/* Unsafe/ssr.scm 432 */
						obj_t BgL_parentz00_1824;
						obj_t BgL_friendliesz00_1825;

						{	/* Unsafe/ssr.scm 235 */
							obj_t BgL__ortest_1101z00_3037;

							BgL__ortest_1101z00_3037 =
								BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_106,
									2L), BINT(BgL_nodez00_107));
							if (CBOOL(BgL__ortest_1101z00_3037))
								{	/* Unsafe/ssr.scm 235 */
									BgL_parentz00_1824 = BgL__ortest_1101z00_3037;
								}
							else
								{	/* Unsafe/ssr.scm 235 */
									BgL_parentz00_1824 = BFALSE;
								}
						}
						{	/* Unsafe/ssr.scm 434 */
							obj_t BgL_hook1217z00_1871;

							BgL_hook1217z00_1871 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							{	/* Unsafe/ssr.scm 436 */
								obj_t BgL_g1218z00_1872;

								{	/* Unsafe/ssr.scm 280 */
									obj_t BgL_friendliesz00_3040;

									{	/* Unsafe/ssr.scm 280 */
										obj_t BgL__ortest_1114z00_3042;

										BgL__ortest_1114z00_3042 =
											BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
											(BgL_graphz00_106, 5L), BINT(BgL_nodez00_107));
										if (CBOOL(BgL__ortest_1114z00_3042))
											{	/* Unsafe/ssr.scm 280 */
												BgL_friendliesz00_3040 = BgL__ortest_1114z00_3042;
											}
										else
											{	/* Unsafe/ssr.scm 280 */
												BgL_friendliesz00_3040 = BFALSE;
											}
									}
									if (CBOOL(BgL_friendliesz00_3040))
										{	/* Unsafe/ssr.scm 281 */
											BgL_g1218z00_1872 =
												BGl_setzd2ze3listz31zz__ssrz00(BgL_friendliesz00_3040);
										}
									else
										{	/* Unsafe/ssr.scm 281 */
											BgL_g1218z00_1872 = BNIL;
										}
								}
								{
									obj_t BgL_l1214z00_1874;
									obj_t BgL_h1215z00_1875;

									BgL_l1214z00_1874 = BgL_g1218z00_1872;
									BgL_h1215z00_1875 = BgL_hook1217z00_1871;
								BgL_zc3z04anonymousza31588ze3z87_1876:
									if (NULLP(BgL_l1214z00_1874))
										{	/* Unsafe/ssr.scm 436 */
											BgL_friendliesz00_1825 = CDR(BgL_hook1217z00_1871);
										}
									else
										{	/* Unsafe/ssr.scm 436 */
											bool_t BgL_test2228z00_4607;

											{	/* Unsafe/ssr.scm 435 */
												obj_t BgL_fz00_1887;

												BgL_fz00_1887 = CAR(((obj_t) BgL_l1214z00_1874));
												{	/* Unsafe/ssr.scm 435 */
													bool_t BgL_test2229z00_4610;

													if (INTEGERP(BgL_fz00_1887))
														{	/* Unsafe/ssr.scm 435 */
															BgL_test2229z00_4610 =
																((long) CINT(BgL_fz00_1887) == BgL_nodez00_107);
														}
													else
														{	/* Unsafe/ssr.scm 435 */
															BgL_test2229z00_4610 =
																BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_fz00_1887,
																BINT(BgL_nodez00_107));
														}
													if (BgL_test2229z00_4610)
														{	/* Unsafe/ssr.scm 435 */
															BgL_test2228z00_4607 = ((bool_t) 0);
														}
													else
														{	/* Unsafe/ssr.scm 435 */
															BgL_test2228z00_4607 = ((bool_t) 1);
														}
												}
											}
											if (BgL_test2228z00_4607)
												{	/* Unsafe/ssr.scm 436 */
													obj_t BgL_nh1216z00_1883;

													{	/* Unsafe/ssr.scm 436 */
														obj_t BgL_arg1595z00_1885;

														BgL_arg1595z00_1885 =
															CAR(((obj_t) BgL_l1214z00_1874));
														BgL_nh1216z00_1883 =
															MAKE_YOUNG_PAIR(BgL_arg1595z00_1885, BNIL);
													}
													SET_CDR(BgL_h1215z00_1875, BgL_nh1216z00_1883);
													{	/* Unsafe/ssr.scm 436 */
														obj_t BgL_arg1594z00_1884;

														BgL_arg1594z00_1884 =
															CDR(((obj_t) BgL_l1214z00_1874));
														{
															obj_t BgL_h1215z00_4624;
															obj_t BgL_l1214z00_4623;

															BgL_l1214z00_4623 = BgL_arg1594z00_1884;
															BgL_h1215z00_4624 = BgL_nh1216z00_1883;
															BgL_h1215z00_1875 = BgL_h1215z00_4624;
															BgL_l1214z00_1874 = BgL_l1214z00_4623;
															goto BgL_zc3z04anonymousza31588ze3z87_1876;
														}
													}
												}
											else
												{	/* Unsafe/ssr.scm 436 */
													obj_t BgL_arg1598z00_1886;

													BgL_arg1598z00_1886 =
														CDR(((obj_t) BgL_l1214z00_1874));
													{
														obj_t BgL_l1214z00_4627;

														BgL_l1214z00_4627 = BgL_arg1598z00_1886;
														BgL_l1214z00_1874 = BgL_l1214z00_4627;
														goto BgL_zc3z04anonymousza31588ze3z87_1876;
													}
												}
										}
								}
							}
						}
						{	/* Unsafe/ssr.scm 438 */
							bool_t BgL_test2231z00_4628;

							if (CBOOL(BgL_parentz00_1824))
								{	/* Unsafe/ssr.scm 438 */
									BgL_test2231z00_4628 = ((bool_t) 0);
								}
							else
								{	/* Unsafe/ssr.scm 438 */
									BgL_test2231z00_4628 = NULLP(BgL_friendliesz00_1825);
								}
							if (BgL_test2231z00_4628)
								{	/* Unsafe/ssr.scm 438 */
									return BFALSE;
								}
							else
								{	/* Unsafe/ssr.scm 439 */
									bool_t BgL_test2233z00_4632;

									{	/* Unsafe/ssr.scm 439 */
										obj_t BgL_a1219z00_1868;

										{	/* Unsafe/ssr.scm 214 */
											obj_t BgL__ortest_1095z00_3054;

											BgL__ortest_1095z00_3054 =
												BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
												(BgL_graphz00_106, 1L), BINT(BgL_nodez00_107));
											if (CBOOL(BgL__ortest_1095z00_3054))
												{	/* Unsafe/ssr.scm 214 */
													BgL_a1219z00_1868 = BgL__ortest_1095z00_3054;
												}
											else
												{	/* Unsafe/ssr.scm 214 */
													BgL_a1219z00_1868 =
														BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
												}
										}
										{	/* Unsafe/ssr.scm 439 */
											obj_t BgL_b1220z00_1869;

											{	/* Unsafe/ssr.scm 214 */
												obj_t BgL__ortest_1095z00_3058;

												BgL__ortest_1095z00_3058 =
													BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
													(BgL_graphz00_106, 1L), BINT(BgL_otherz00_108));
												if (CBOOL(BgL__ortest_1095z00_3058))
													{	/* Unsafe/ssr.scm 214 */
														BgL_b1220z00_1869 = BgL__ortest_1095z00_3058;
													}
												else
													{	/* Unsafe/ssr.scm 214 */
														BgL_b1220z00_1869 =
															BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
													}
											}
											{	/* Unsafe/ssr.scm 439 */

												{	/* Unsafe/ssr.scm 439 */
													bool_t BgL_test2236z00_4643;

													if (INTEGERP(BgL_a1219z00_1868))
														{	/* Unsafe/ssr.scm 439 */
															BgL_test2236z00_4643 =
																INTEGERP(BgL_b1220z00_1869);
														}
													else
														{	/* Unsafe/ssr.scm 439 */
															BgL_test2236z00_4643 = ((bool_t) 0);
														}
													if (BgL_test2236z00_4643)
														{	/* Unsafe/ssr.scm 439 */
															BgL_test2233z00_4632 =
																(
																(long) CINT(BgL_a1219z00_1868) >=
																(long) CINT(BgL_b1220z00_1869));
														}
													else
														{	/* Unsafe/ssr.scm 439 */
															BgL_test2233z00_4632 =
																BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																(BgL_a1219z00_1868, BgL_b1220z00_1869);
														}
												}
											}
										}
									}
									if (BgL_test2233z00_4632)
										{	/* Unsafe/ssr.scm 439 */
											{
												obj_t BgL_l1221z00_3074;

												BgL_l1221z00_3074 = BgL_friendliesz00_1825;
											BgL_zc3z04anonymousza31573ze3z87_3073:
												if (PAIRP(BgL_l1221z00_3074))
													{	/* Unsafe/ssr.scm 441 */
														BGl_removezd2friendz12zc0zz__ssrz00
															(BgL_graphz00_106, CAR(BgL_l1221z00_3074),
															BINT(BgL_nodez00_107));
														{
															obj_t BgL_l1221z00_4656;

															BgL_l1221z00_4656 = CDR(BgL_l1221z00_3074);
															BgL_l1221z00_3074 = BgL_l1221z00_4656;
															goto BgL_zc3z04anonymousza31573ze3z87_3073;
														}
													}
												else
													{	/* Unsafe/ssr.scm 441 */
														((bool_t) 1);
													}
											}
											if (CBOOL(BgL_parentz00_1824))
												{	/* Unsafe/ssr.scm 443 */
													BGl_removezd2parentzd2edgez12z12zz__ssrz00
														(BgL_graphz00_106, BgL_nodez00_107,
														BgL_ondisconnectz00_110);
													{	/* Unsafe/ssr.scm 445 */
														bool_t BgL_test2240z00_4661;

														{	/* Unsafe/ssr.scm 287 */
															obj_t BgL_arg1440z00_3083;

															{	/* Unsafe/ssr.scm 235 */
																obj_t BgL__ortest_1101z00_3085;

																BgL__ortest_1101z00_3085 =
																	BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
																	(BgL_graphz00_106, 2L),
																	BINT(BgL_otherz00_108));
																if (CBOOL(BgL__ortest_1101z00_3085))
																	{	/* Unsafe/ssr.scm 235 */
																		BgL_arg1440z00_3083 =
																			BgL__ortest_1101z00_3085;
																	}
																else
																	{	/* Unsafe/ssr.scm 235 */
																		BgL_arg1440z00_3083 = BFALSE;
																	}
															}
															BgL_test2240z00_4661 =
																(BgL_parentz00_1824 == BgL_arg1440z00_3083);
														}
														if (BgL_test2240z00_4661)
															{	/* Unsafe/ssr.scm 445 */
																BFALSE;
															}
														else
															{	/* Unsafe/ssr.scm 445 */
																BGl_hashtablezd2putz12zc0zz__hashz00
																	(BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
																	(VECTOR_REF(BgL_graphz00_106, 4L),
																		BgL_parentz00_1824), BINT(BgL_otherz00_108),
																	BTRUE);
																BGl_hashtablezd2putz12zc0zz__hashz00
																	(BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
																	(VECTOR_REF(BgL_graphz00_106, 5L),
																		BINT(BgL_otherz00_108)), BgL_parentz00_1824,
																	BTRUE);
															}
													}
												}
											else
												{	/* Unsafe/ssr.scm 443 */
													BFALSE;
												}
											{
												obj_t BgL_l1223z00_3111;

												{	/* Unsafe/ssr.scm 448 */
													bool_t BgL_tmpz00_4676;

													BgL_l1223z00_3111 = BgL_friendliesz00_1825;
												BgL_zc3z04anonymousza31577ze3z87_3110:
													if (PAIRP(BgL_l1223z00_3111))
														{	/* Unsafe/ssr.scm 448 */
															BGl_addzd2friendz12zc0zz__ssrz00(BgL_graphz00_106,
																CAR(BgL_l1223z00_3111), BgL_otherz00_108);
															{
																obj_t BgL_l1223z00_4681;

																BgL_l1223z00_4681 = CDR(BgL_l1223z00_3111);
																BgL_l1223z00_3111 = BgL_l1223z00_4681;
																goto BgL_zc3z04anonymousza31577ze3z87_3110;
															}
														}
													else
														{	/* Unsafe/ssr.scm 448 */
															BgL_tmpz00_4676 = ((bool_t) 1);
														}
													return BBOOL(BgL_tmpz00_4676);
												}
											}
										}
									else
										{	/* Unsafe/ssr.scm 452 */
											obj_t BgL_adopterz00_1847;

											if (CBOOL(BgL_parentz00_1824))
												{	/* Unsafe/ssr.scm 452 */
													BgL_adopterz00_1847 = BgL_parentz00_1824;
												}
											else
												{	/* Unsafe/ssr.scm 452 */
													BgL_nodesz00_1892 = BgL_friendliesz00_1825;
													{	/* Unsafe/ssr.scm 416 */
														obj_t BgL_g1130z00_1894;
														obj_t BgL_g1131z00_1895;
														obj_t BgL_g1132z00_1896;

														BgL_g1130z00_1894 =
															CAR(((obj_t) BgL_nodesz00_1892));
														{	/* Unsafe/ssr.scm 417 */
															obj_t BgL_arg1607z00_1911;

															BgL_arg1607z00_1911 =
																CAR(((obj_t) BgL_nodesz00_1892));
															{	/* Unsafe/ssr.scm 214 */
																obj_t BgL__ortest_1095z00_3021;

																BgL__ortest_1095z00_3021 =
																	BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF
																	(BgL_graphz00_106, 1L), BgL_arg1607z00_1911);
																if (CBOOL(BgL__ortest_1095z00_3021))
																	{	/* Unsafe/ssr.scm 214 */
																		BgL_g1131z00_1895 =
																			BgL__ortest_1095z00_3021;
																	}
																else
																	{	/* Unsafe/ssr.scm 214 */
																		BgL_g1131z00_1895 =
																			BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
																	}
															}
														}
														BgL_g1132z00_1896 =
															CDR(((obj_t) BgL_nodesz00_1892));
														{
															obj_t BgL_bestz00_1898;
															obj_t BgL_rankz00_1899;
															obj_t BgL_nodesz00_1900;

															BgL_bestz00_1898 = BgL_g1130z00_1894;
															BgL_rankz00_1899 = BgL_g1131z00_1895;
															BgL_nodesz00_1900 = BgL_g1132z00_1896;
														BgL_zc3z04anonymousza31600ze3z87_1901:
															if (NULLP(BgL_nodesz00_1900))
																{	/* Unsafe/ssr.scm 419 */
																	BgL_adopterz00_1847 = BgL_bestz00_1898;
																}
															else
																{	/* Unsafe/ssr.scm 421 */
																	obj_t BgL_nextz00_1903;

																	BgL_nextz00_1903 =
																		CAR(((obj_t) BgL_nodesz00_1900));
																	{	/* Unsafe/ssr.scm 421 */
																		obj_t BgL_nextzd2rankzd2_1904;

																		{	/* Unsafe/ssr.scm 214 */
																			obj_t BgL__ortest_1095z00_3027;

																			BgL__ortest_1095z00_3027 =
																				BGl_hashtablezd2getzd2zz__hashz00
																				(VECTOR_REF(BgL_graphz00_106, 1L),
																				BgL_nextz00_1903);
																			if (CBOOL(BgL__ortest_1095z00_3027))
																				{	/* Unsafe/ssr.scm 214 */
																					BgL_nextzd2rankzd2_1904 =
																						BgL__ortest_1095z00_3027;
																				}
																			else
																				{	/* Unsafe/ssr.scm 214 */
																					BgL_nextzd2rankzd2_1904 =
																						BGL_REAL_CNST
																						(BGl_real2018z00zz__ssrz00);
																				}
																		}
																		{	/* Unsafe/ssr.scm 422 */

																			{	/* Unsafe/ssr.scm 423 */
																				bool_t BgL_test2247z00_4704;

																				{	/* Unsafe/ssr.scm 423 */
																					bool_t BgL_test2248z00_4705;

																					if (INTEGERP(BgL_nextzd2rankzd2_1904))
																						{	/* Unsafe/ssr.scm 423 */
																							BgL_test2248z00_4705 =
																								INTEGERP(BgL_rankz00_1899);
																						}
																					else
																						{	/* Unsafe/ssr.scm 423 */
																							BgL_test2248z00_4705 =
																								((bool_t) 0);
																						}
																					if (BgL_test2248z00_4705)
																						{	/* Unsafe/ssr.scm 423 */
																							BgL_test2247z00_4704 =
																								(
																								(long)
																								CINT(BgL_nextzd2rankzd2_1904) <
																								(long) CINT(BgL_rankz00_1899));
																						}
																					else
																						{	/* Unsafe/ssr.scm 423 */
																							BgL_test2247z00_4704 =
																								BGl_2zc3zc3zz__r4_numbers_6_5z00
																								(BgL_nextzd2rankzd2_1904,
																								BgL_rankz00_1899);
																						}
																				}
																				if (BgL_test2247z00_4704)
																					{
																						obj_t BgL_nodesz00_4715;
																						obj_t BgL_rankz00_4714;
																						obj_t BgL_bestz00_4713;

																						BgL_bestz00_4713 = BgL_nextz00_1903;
																						BgL_rankz00_4714 =
																							BgL_nextzd2rankzd2_1904;
																						BgL_nodesz00_4715 =
																							CDR(((obj_t) BgL_nodesz00_1900));
																						BgL_nodesz00_1900 =
																							BgL_nodesz00_4715;
																						BgL_rankz00_1899 = BgL_rankz00_4714;
																						BgL_bestz00_1898 = BgL_bestz00_4713;
																						goto
																							BgL_zc3z04anonymousza31600ze3z87_1901;
																					}
																				else
																					{
																						obj_t BgL_nodesz00_4718;

																						BgL_nodesz00_4718 =
																							CDR(((obj_t) BgL_nodesz00_1900));
																						BgL_nodesz00_1900 =
																							BgL_nodesz00_4718;
																						goto
																							BgL_zc3z04anonymousza31600ze3z87_1901;
																					}
																			}
																		}
																	}
																}
														}
													}
												}
											{
												obj_t BgL_l1225z00_3131;

												BgL_l1225z00_3131 = BgL_friendliesz00_1825;
											BgL_zc3z04anonymousza31580ze3z87_3130:
												if (PAIRP(BgL_l1225z00_3131))
													{	/* Unsafe/ssr.scm 454 */
														BGl_removezd2friendz12zc0zz__ssrz00
															(BgL_graphz00_106, CAR(BgL_l1225z00_3131),
															BINT(BgL_nodez00_107));
														{
															obj_t BgL_l1225z00_4726;

															BgL_l1225z00_4726 = CDR(BgL_l1225z00_3131);
															BgL_l1225z00_3131 = BgL_l1225z00_4726;
															goto BgL_zc3z04anonymousza31580ze3z87_3130;
														}
													}
												else
													{	/* Unsafe/ssr.scm 454 */
														((bool_t) 1);
													}
											}
											if (CBOOL(BgL_parentz00_1824))
												{	/* Unsafe/ssr.scm 455 */
													BGl_removezd2parentzd2edgez12z12zz__ssrz00
														(BgL_graphz00_106, BgL_nodez00_107,
														BgL_ondisconnectz00_110);
												}
											else
												{	/* Unsafe/ssr.scm 455 */
													BFALSE;
												}
											BGl_ssrzd2addzd2edgez12z12zz__ssrz00(BgL_graphz00_106,
												(long) CINT(BgL_adopterz00_1847), BgL_otherz00_108,
												BgL_onconnectz00_109);
											{
												obj_t BgL_l1227z00_1858;

												{	/* Unsafe/ssr.scm 458 */
													bool_t BgL_tmpz00_4733;

													BgL_l1227z00_1858 = BgL_friendliesz00_1825;
												BgL_zc3z04anonymousza31583ze3z87_1859:
													if (PAIRP(BgL_l1227z00_1858))
														{	/* Unsafe/ssr.scm 458 */
															{	/* Unsafe/ssr.scm 458 */
																obj_t BgL_fz00_1861;

																BgL_fz00_1861 = CAR(BgL_l1227z00_1858);
																{	/* Unsafe/ssr.scm 458 */
																	bool_t BgL_test2253z00_4737;

																	{	/* Unsafe/ssr.scm 458 */
																		bool_t BgL_test2254z00_4738;

																		if (INTEGERP(BgL_fz00_1861))
																			{	/* Unsafe/ssr.scm 458 */
																				BgL_test2254z00_4738 =
																					INTEGERP(BgL_adopterz00_1847);
																			}
																		else
																			{	/* Unsafe/ssr.scm 458 */
																				BgL_test2254z00_4738 = ((bool_t) 0);
																			}
																		if (BgL_test2254z00_4738)
																			{	/* Unsafe/ssr.scm 458 */
																				BgL_test2253z00_4737 =
																					(
																					(long) CINT(BgL_fz00_1861) ==
																					(long) CINT(BgL_adopterz00_1847));
																			}
																		else
																			{	/* Unsafe/ssr.scm 458 */
																				BgL_test2253z00_4737 =
																					BGl_2zd3zd3zz__r4_numbers_6_5z00
																					(BgL_fz00_1861, BgL_adopterz00_1847);
																			}
																	}
																	if (BgL_test2253z00_4737)
																		{	/* Unsafe/ssr.scm 458 */
																			BFALSE;
																		}
																	else
																		{	/* Unsafe/ssr.scm 458 */
																			BGl_hashtablezd2putz12zc0zz__hashz00
																				(BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
																				(VECTOR_REF(BgL_graphz00_106, 4L),
																					BgL_fz00_1861),
																				BINT(BgL_otherz00_108), BTRUE);
																			BGl_hashtablezd2putz12zc0zz__hashz00
																				(BGl_tablezd2refzd2orzd2setzd2defaultz12z12zz__ssrz00
																				(VECTOR_REF(BgL_graphz00_106, 5L),
																					BINT(BgL_otherz00_108)),
																				BgL_fz00_1861, BTRUE);
																		}
																}
															}
															{
																obj_t BgL_l1227z00_4754;

																BgL_l1227z00_4754 = CDR(BgL_l1227z00_1858);
																BgL_l1227z00_1858 = BgL_l1227z00_4754;
																goto BgL_zc3z04anonymousza31583ze3z87_1859;
															}
														}
													else
														{	/* Unsafe/ssr.scm 458 */
															BgL_tmpz00_4733 = ((bool_t) 1);
														}
													return BBOOL(BgL_tmpz00_4733);
												}
											}
										}
								}
						}
					}
			}
		}

	}



/* ssr-connected? */
	BGL_EXPORTED_DEF obj_t BGl_ssrzd2connectedzf3z21zz__ssrz00(obj_t
		BgL_graphz00_113, long BgL_nodez00_114)
	{
		{	/* Unsafe/ssr.scm 460 */
			{	/* Unsafe/ssr.scm 461 */
				bool_t BgL_test2256z00_4757;

				{	/* Unsafe/ssr.scm 461 */
					obj_t BgL_a1229z00_3151;

					{	/* Unsafe/ssr.scm 214 */
						obj_t BgL__ortest_1095z00_3154;

						BgL__ortest_1095z00_3154 =
							BGl_hashtablezd2getzd2zz__hashz00(VECTOR_REF(BgL_graphz00_113,
								1L), BINT(BgL_nodez00_114));
						if (CBOOL(BgL__ortest_1095z00_3154))
							{	/* Unsafe/ssr.scm 214 */
								BgL_a1229z00_3151 = BgL__ortest_1095z00_3154;
							}
						else
							{	/* Unsafe/ssr.scm 214 */
								BgL_a1229z00_3151 = BGL_REAL_CNST(BGl_real2018z00zz__ssrz00);
							}
					}
					BgL_test2256z00_4757 =
						BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_a1229z00_3151,
						BGL_REAL_CNST(BGl_real2018z00zz__ssrz00));
				}
				if (BgL_test2256z00_4757)
					{	/* Unsafe/ssr.scm 461 */
						return BFALSE;
					}
				else
					{	/* Unsafe/ssr.scm 461 */
						return BTRUE;
					}
			}
		}

	}



/* &ssr-connected? */
	obj_t BGl_z62ssrzd2connectedzf3z43zz__ssrz00(obj_t BgL_envz00_3452,
		obj_t BgL_graphz00_3453, obj_t BgL_nodez00_3454)
	{
		{	/* Unsafe/ssr.scm 460 */
			{	/* Unsafe/ssr.scm 461 */
				long BgL_auxz00_4771;
				obj_t BgL_auxz00_4764;

				{	/* Unsafe/ssr.scm 461 */
					obj_t BgL_tmpz00_4772;

					if (INTEGERP(BgL_nodez00_3454))
						{	/* Unsafe/ssr.scm 461 */
							BgL_tmpz00_4772 = BgL_nodez00_3454;
						}
					else
						{
							obj_t BgL_auxz00_4775;

							BgL_auxz00_4775 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2015z00zz__ssrz00,
								BINT(17306L), BGl_string2044z00zz__ssrz00,
								BGl_string2017z00zz__ssrz00, BgL_nodez00_3454);
							FAILURE(BgL_auxz00_4775, BFALSE, BFALSE);
						}
					BgL_auxz00_4771 = (long) CINT(BgL_tmpz00_4772);
				}
				if (VECTORP(BgL_graphz00_3453))
					{	/* Unsafe/ssr.scm 461 */
						BgL_auxz00_4764 = BgL_graphz00_3453;
					}
				else
					{
						obj_t BgL_auxz00_4767;

						BgL_auxz00_4767 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2015z00zz__ssrz00,
							BINT(17306L), BGl_string2044z00zz__ssrz00,
							BGl_string2028z00zz__ssrz00, BgL_graphz00_3453);
						FAILURE(BgL_auxz00_4767, BFALSE, BFALSE);
					}
				return
					BGl_ssrzd2connectedzf3z21zz__ssrz00(BgL_auxz00_4764, BgL_auxz00_4771);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__ssrz00(void)
	{
		{	/* Unsafe/ssr.scm 28 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2045z00zz__ssrz00));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2045z00zz__ssrz00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2045z00zz__ssrz00));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2045z00zz__ssrz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2045z00zz__ssrz00));
			return
				BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2045z00zz__ssrz00));
		}

	}

#ifdef __cplusplus
}
#endif
