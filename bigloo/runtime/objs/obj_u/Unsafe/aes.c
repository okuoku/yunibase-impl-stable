/*===========================================================================*/
/*   (Unsafe/aes.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/aes.scm -indent -o objs/obj_u/Unsafe/aes.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___AES_TYPE_DEFINITIONS
#define BGL___AES_TYPE_DEFINITIONS
#endif													// BGL___AES_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
	static obj_t BGl_z62zc3z04anonymousza31329ze3ze5zz__aesz00(obj_t);
	static obj_t BGl_aeszd2keyzd2expansionz00zz__aesz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__aesz00 = BUNSPEC;
	static bool_t BGl_addroundkeyz12z12zz__aesz00(obj_t, obj_t, long, long);
	static obj_t BGl__aeszd2ctrzd2encryptz00zz__aesz00(obj_t, obj_t);
	static bool_t BGl_shiftrowsz12z12zz__aesz00(obj_t, long);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__aesz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__datez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__sha1z00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2decryptzd2stringzd2zz__aesz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2decryptzd2filezd2zz__aesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__aesz00(void);
	static obj_t BGl__aeszd2ctrzd2decryptzd2stringzd2zz__aesz00(obj_t, obj_t);
	extern obj_t BGl_ceilingz00zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_Sboxz00zz__aesz00 = BUNSPEC;
	static obj_t BGl_aeszd2cipherzd2zz__aesz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2decryptzd2portzd2zz__aesz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2decryptz00zz__aesz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zz__aesz00(void);
	static obj_t BGl_genericzd2initzd2zz__aesz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__aesz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__aesz00(void);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2decryptzd2mmapzd2zz__aesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_rotwordz12z12zz__aesz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__aesz00(void);
	static obj_t BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(obj_t, obj_t, obj_t);
	extern obj_t string_to_obj(obj_t, obj_t, obj_t);
	extern obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_sha1sumzd2stringzd2zz__sha1z00(obj_t);
	static obj_t BGl_subwordz12z12zz__aesz00(obj_t);
	extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl__aeszd2ctrzd2decryptzd2filezd2zz__aesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2encryptz00zz__aesz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31329ze32568ze5zz__aesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31329ze32569ze5zz__aesz00(obj_t);
	static obj_t BGl__aeszd2ctrzd2decryptzd2portzd2zz__aesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2encryptzd2filezd2zz__aesz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(obj_t, obj_t, obj_t);
	static obj_t BGl__aeszd2ctrzd2decryptzd2mmapzd2zz__aesz00(obj_t, obj_t);
	static obj_t BGl_mixcolumnsz12z12zz__aesz00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2encryptzd2portzd2zz__aesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__aesz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2encryptzd2mmapzd2zz__aesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31335ze32570ze5zz__aesz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31335ze32571ze5zz__aesz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_aeszd2ctrzd2encryptzd2stringzd2zz__aesz00(obj_t,
		obj_t, obj_t);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_aeszd2passwordzd2ze3keyze3zz__aesz00(obj_t, int, obj_t);
	static obj_t BGl__aeszd2ctrzd2encryptzd2filezd2zz__aesz00(obj_t, obj_t);
	static obj_t BGl_Rconnz00zz__aesz00 = BUNSPEC;
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl__aeszd2ctrzd2encryptzd2stringzd2zz__aesz00(obj_t, obj_t);
	extern long bgl_current_seconds(void);
	static obj_t BGl_z62zc3z04anonymousza31335ze3ze5zz__aesz00(obj_t);
	static obj_t BGl__aeszd2ctrzd2encryptzd2portzd2zz__aesz00(obj_t, obj_t);
	static obj_t BGl__aeszd2ctrzd2decryptz00zz__aesz00(obj_t, obj_t);
	extern obj_t BGl_u8vectorz00zz__srfi4z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static bool_t BGl_subbytesz12z12zz__aesz00(obj_t, int);
	static int BGl_z62u8stringzd2refzb0zz__aesz00(obj_t, obj_t, long);
	static obj_t BGl__aeszd2ctrzd2encryptzd2mmapzd2zz__aesz00(obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2encryptzd2filezd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2enc2707z00, opt_generic_entry,
		BGl__aeszd2ctrzd2encryptzd2filezd2zz__aesz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2684z00zz__aesz00,
		BgL_bgl_string2684za700za7za7_2708za7, "/tmp/bigloo/runtime/Unsafe/aes.scm",
		34);
	      DEFINE_STRING(BGl_string2685z00zz__aesz00,
		BgL_bgl_string2685za700za7za7_2709za7, "_aes-ctr-encrypt", 16);
	      DEFINE_STRING(BGl_string2686z00zz__aesz00,
		BgL_bgl_string2686za700za7za7_2710za7, "bstring", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_aeszd2ctrzd2decryptzd2envzd2zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2dec2711z00, opt_generic_entry,
		BGl__aeszd2ctrzd2decryptz00zz__aesz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2687z00zz__aesz00,
		BgL_bgl_string2687za700za7za7_2712za7, "mmap", 4);
	      DEFINE_STRING(BGl_string2688z00zz__aesz00,
		BgL_bgl_string2688za700za7za7_2713za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string2689z00zz__aesz00,
		BgL_bgl_string2689za700za7za7_2714za7, "_aes-ctr-encrypt-string", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2decryptzd2mmapzd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2dec2715z00, opt_generic_entry,
		BGl__aeszd2ctrzd2decryptzd2mmapzd2zz__aesz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2690z00zz__aesz00,
		BgL_bgl_string2690za700za7za7_2716za7, "_aes-ctr-encrypt-mmap", 21);
	      DEFINE_STRING(BGl_string2691z00zz__aesz00,
		BgL_bgl_string2691za700za7za7_2717za7, "_aes-ctr-encrypt-file", 21);
	      DEFINE_STRING(BGl_string2692z00zz__aesz00,
		BgL_bgl_string2692za700za7za7_2718za7, "pair", 4);
	      DEFINE_STRING(BGl_string2693z00zz__aesz00,
		BgL_bgl_string2693za700za7za7_2719za7, "_aes-ctr-encrypt-port", 21);
	      DEFINE_STRING(BGl_string2694z00zz__aesz00,
		BgL_bgl_string2694za700za7za7_2720za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2695z00zz__aesz00,
		BgL_bgl_string2695za700za7za7_2721za7, "_aes-ctr-decrypt", 16);
	      DEFINE_STRING(BGl_string2696z00zz__aesz00,
		BgL_bgl_string2696za700za7za7_2722za7, "_aes-ctr-decrypt-string", 23);
	      DEFINE_STRING(BGl_string2697z00zz__aesz00,
		BgL_bgl_string2697za700za7za7_2723za7, "_aes-ctr-decrypt-mmap", 21);
	      DEFINE_STRING(BGl_string2698z00zz__aesz00,
		BgL_bgl_string2698za700za7za7_2724za7, "_aes-ctr-decrypt-file", 21);
	extern obj_t BGl_mmapzd2lengthzd2envz00zz__mmapz00;
	   
		 
		DEFINE_STRING(BGl_string2699z00zz__aesz00,
		BgL_bgl_string2699za700za7za7_2725za7, "_aes-ctr-decrypt-port", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2decryptzd2filezd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2dec2726z00, opt_generic_entry,
		BGl__aeszd2ctrzd2decryptzd2filezd2zz__aesz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2encryptzd2portzd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2enc2727z00, opt_generic_entry,
		BGl__aeszd2ctrzd2encryptzd2portzd2zz__aesz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2decryptzd2portzd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2dec2728z00, opt_generic_entry,
		BGl__aeszd2ctrzd2decryptzd2portzd2zz__aesz00, BFALSE, -1);
	static obj_t BGl_u8stringzd2refzd2envz00zz__aesz00;
	extern obj_t BGl_mmapzd2refzd2envz00zz__mmapz00;
	extern obj_t BGl_stringzd2lengthzd2envz00zz__r4_strings_6_7z00;
	   
		 
		DEFINE_STRING(BGl_string2700z00zz__aesz00,
		BgL_bgl_string2700za700za7za7_2729za7, "Illegal bit keys", 16);
	      DEFINE_STRING(BGl_string2701z00zz__aesz00,
		BgL_bgl_string2701za700za7za7_2730za7, "string or mmap", 14);
	      DEFINE_STRING(BGl_string2702z00zz__aesz00,
		BgL_bgl_string2702za700za7za7_2731za7, "&u8string-ref", 13);
	      DEFINE_STRING(BGl_string2703z00zz__aesz00,
		BgL_bgl_string2703za700za7za7_2732za7, "__aes", 5);
	      DEFINE_STRING(BGl_string2704z00zz__aesz00,
		BgL_bgl_string2704za700za7za7_2733za7,
		"c\001\002[\001\005[\001\vh\001\004\001\001\"\001\002u8\000\000\000\000h\001\004\001\001\"\001\002u8\001\000\000\000h\001\004\001\001\"\001\002u8\002\000\000\000h\001\004\001\001\"\001\002u8\004\000\000\000h\001\004\001\001\"\001\002u8\b\000\000\000h\001\004\001\001\"\001\002u8\020\000\000\000h\001\004\001\001\"\001\002u8 \000\000\000h\001\004\001\001\"\001\002u8@\000\000\000h\001\004\001\001\"\001\002u8�\000\000\000h\001\004\001\001\"\001\002u8\033\000\000\000h\001\004\001\001\"\001\002u86\000\000\000h\002\001\000\001\001\"\001\002u8c|w{�ko�0\001g+�׫vʂ�}�YG�Ԣ���r����&6?��4���q�1\025\004�#�\030�\005�\007\022���'�u\t�,\032\033nZ�R;ֳ)�/�S�\000� ��[j˾9JLX����CM3�E�\002P<��Q�@���8����!\020����\f\023�_�D\027ħ~=d]\031s`�O�\"*��F�\024�^\v��2:\nI\006$\\�Ӭb���y��7m��N�lV��ez�\b�x%.\034�����t\037K���p>�fH\003�\016a5W���\035����\021iَ��\036���U(ߌ��\r��BhA�-\017�T�\026'\"\001\017aes-ctr-encrypt'\"\001\017aes-ctr-decrypt^\001\004\001�(\001\004'=\000\"\001\002at=\001\001\"\001\016Unsafe/aes.scm\002(�.\001�(\001"
		"\004'#\000#\001\001\002(�.\002\001\000(\001\004'#\000#\001\001\002(�..",
		540);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_aeszd2ctrzd2encryptzd2envzd2zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2enc2734z00, opt_generic_entry,
		BGl__aeszd2ctrzd2encryptz00zz__aesz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2decryptzd2stringzd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2dec2735z00, opt_generic_entry,
		BGl__aeszd2ctrzd2decryptzd2stringzd2zz__aesz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2encryptzd2mmapzd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2enc2736z00, opt_generic_entry,
		BGl__aeszd2ctrzd2encryptzd2mmapzd2zz__aesz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_aeszd2ctrzd2encryptzd2stringzd2envz00zz__aesz00,
		BgL_bgl__aesza7d2ctrza7d2enc2737z00, opt_generic_entry,
		BGl__aeszd2ctrzd2encryptzd2stringzd2zz__aesz00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__aesz00));
		     ADD_ROOT((void *) (&BGl_Sboxz00zz__aesz00));
		     ADD_ROOT((void *) (&__cnst[5]));
		     ADD_ROOT((void *) (&BGl_Rconnz00zz__aesz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__aesz00(long
		BgL_checksumz00_5592, char *BgL_fromz00_5593)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__aesz00))
				{
					BGl_requirezd2initializa7ationz75zz__aesz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__aesz00();
					BGl_cnstzd2initzd2zz__aesz00();
					BGl_importedzd2moduleszd2initz00zz__aesz00();
					return BGl_toplevelzd2initzd2zz__aesz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			{	/* Unsafe/aes.scm 20 */
				obj_t BgL_cnstzd2tmpzd2_5582;

				{	/* Unsafe/aes.scm 20 */
					obj_t BgL_g2706z00_5587;

					BgL_g2706z00_5587 = BGl_string2704z00zz__aesz00;
					{	/* Unsafe/aes.scm 20 */
						obj_t BgL_extensionz00_5588;

						BgL_extensionz00_5588 = BBOOL(((bool_t) 0));
						{	/* Unsafe/aes.scm 20 */
							obj_t BgL_unserializa7ezd2argz75_5589;

							BgL_unserializa7ezd2argz75_5589 = BgL_extensionz00_5588;
							{	/* Unsafe/aes.scm 20 */

								BgL_cnstzd2tmpzd2_5582 =
									string_to_obj(BgL_g2706z00_5587, BgL_extensionz00_5588,
									BgL_unserializa7ezd2argz75_5589);
				}}}}
				{
					int BgL_iz00_5583;

					BgL_iz00_5583 = (int) (4L);
				BgL_loopz00_5584:
					if (((long) (BgL_iz00_5583) == -1L))
						{	/* Unsafe/aes.scm 20 */
							return BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 20 */
							{	/* Unsafe/aes.scm 20 */
								obj_t BgL_tmpz00_5606;

								BgL_tmpz00_5606 =
									VECTOR_REF(BgL_cnstzd2tmpzd2_5582, (long) (BgL_iz00_5583));
								CNST_TABLE_SET(BgL_iz00_5583, BgL_tmpz00_5606);
							}
							{
								int BgL_iz00_5610;

								BgL_iz00_5610 = (int) (((long) (BgL_iz00_5583) - 1L));
								BgL_iz00_5583 = BgL_iz00_5610;
								goto BgL_loopz00_5584;
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			BGl_Rconnz00zz__aesz00 = CNST_TABLE_REF(0);
			return (BGl_Sboxz00zz__aesz00 = CNST_TABLE_REF(1), BUNSPEC);
		}

	}



/* _aes-ctr-encrypt */
	obj_t BGl__aeszd2ctrzd2encryptz00zz__aesz00(obj_t BgL_env1174z00_7,
		obj_t BgL_opt1173z00_6)
	{
		{	/* Unsafe/aes.scm 91 */
			{	/* Unsafe/aes.scm 91 */
				obj_t BgL_g1175z00_1402;
				obj_t BgL_g1176z00_1403;

				BgL_g1175z00_1402 = VECTOR_REF(BgL_opt1173z00_6, 0L);
				BgL_g1176z00_1403 = VECTOR_REF(BgL_opt1173z00_6, 1L);
				switch (VECTOR_LENGTH(BgL_opt1173z00_6))
					{
					case 2L:

						{	/* Unsafe/aes.scm 91 */

							{	/* Unsafe/aes.scm 91 */
								obj_t BgL_res2450z00_2924;

								{	/* Unsafe/aes.scm 91 */
									obj_t BgL_passwordz00_2908;

									if (STRINGP(BgL_g1176z00_1403))
										{	/* Unsafe/aes.scm 91 */
											BgL_passwordz00_2908 = BgL_g1176z00_1403;
										}
									else
										{
											obj_t BgL_auxz00_5622;

											BgL_auxz00_5622 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(3526L),
												BGl_string2685z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1176z00_1403);
											FAILURE(BgL_auxz00_5622, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1175z00_1402))
										{	/* Unsafe/aes.scm 93 */
											BgL_res2450z00_2924 =
												BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
												(BgL_g1175z00_1402, BgL_passwordz00_2908, BINT(128L));
										}
									else
										{	/* Unsafe/aes.scm 93 */
											if (BGL_MMAPP(BgL_g1175z00_1402))
												{	/* Unsafe/aes.scm 96 */
													obj_t BgL_res2448z00_2917;

													{	/* Unsafe/aes.scm 96 */
														obj_t BgL_plaintextz00_2915;

														if (BGL_MMAPP(BgL_g1175z00_1402))
															{	/* Unsafe/aes.scm 96 */
																BgL_plaintextz00_2915 = BgL_g1175z00_1402;
															}
														else
															{
																obj_t BgL_auxz00_5634;

																BgL_auxz00_5634 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2684z00zz__aesz00, BINT(3742L),
																	BGl_string2685z00zz__aesz00,
																	BGl_string2687z00zz__aesz00,
																	BgL_g1175z00_1402);
																FAILURE(BgL_auxz00_5634, BFALSE, BFALSE);
															}
														BgL_res2448z00_2917 =
															BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
															(BgL_plaintextz00_2915, BgL_passwordz00_2908,
															BINT(128L));
													}
													BgL_res2450z00_2924 = BgL_res2448z00_2917;
												}
											else
												{	/* Unsafe/aes.scm 95 */
													if (INPUT_PORTP(BgL_g1175z00_1402))
														{	/* Unsafe/aes.scm 97 */
															BgL_res2450z00_2924 =
																BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
																(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
																(BgL_g1175z00_1402), BgL_passwordz00_2908,
																BINT(128L));
														}
													else
														{	/* Unsafe/aes.scm 100 */
															obj_t BgL_aux2590z00_5485;

															BgL_aux2590z00_5485 =
																BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
																BGl_string2688z00zz__aesz00, BgL_g1175z00_1402);
															if (STRINGP(BgL_aux2590z00_5485))
																{	/* Unsafe/aes.scm 100 */
																	BgL_res2450z00_2924 = BgL_aux2590z00_5485;
																}
															else
																{
																	obj_t BgL_auxz00_5649;

																	BgL_auxz00_5649 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2684z00zz__aesz00, BINT(3875L),
																		BGl_string2685z00zz__aesz00,
																		BGl_string2686z00zz__aesz00,
																		BgL_aux2590z00_5485);
																	FAILURE(BgL_auxz00_5649, BFALSE, BFALSE);
																}
														}
												}
										}
								}
								return BgL_res2450z00_2924;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 91 */
							obj_t BgL_nbitsz00_1407;

							BgL_nbitsz00_1407 = VECTOR_REF(BgL_opt1173z00_6, 2L);
							{	/* Unsafe/aes.scm 91 */

								{	/* Unsafe/aes.scm 91 */
									obj_t BgL_res2454z00_2941;

									{	/* Unsafe/aes.scm 91 */
										obj_t BgL_passwordz00_2925;

										if (STRINGP(BgL_g1176z00_1403))
											{	/* Unsafe/aes.scm 91 */
												BgL_passwordz00_2925 = BgL_g1176z00_1403;
											}
										else
											{
												obj_t BgL_auxz00_5656;

												BgL_auxz00_5656 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(3526L),
													BGl_string2685z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1176z00_1403);
												FAILURE(BgL_auxz00_5656, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1175z00_1402))
											{	/* Unsafe/aes.scm 93 */
												BgL_res2454z00_2941 =
													BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
													(BgL_g1175z00_1402, BgL_passwordz00_2925,
													BgL_nbitsz00_1407);
											}
										else
											{	/* Unsafe/aes.scm 93 */
												if (BGL_MMAPP(BgL_g1175z00_1402))
													{	/* Unsafe/aes.scm 96 */
														obj_t BgL_res2452z00_2934;

														{	/* Unsafe/aes.scm 96 */
															obj_t BgL_plaintextz00_2932;

															if (BGL_MMAPP(BgL_g1175z00_1402))
																{	/* Unsafe/aes.scm 96 */
																	BgL_plaintextz00_2932 = BgL_g1175z00_1402;
																}
															else
																{
																	obj_t BgL_auxz00_5667;

																	BgL_auxz00_5667 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2684z00zz__aesz00, BINT(3742L),
																		BGl_string2685z00zz__aesz00,
																		BGl_string2687z00zz__aesz00,
																		BgL_g1175z00_1402);
																	FAILURE(BgL_auxz00_5667, BFALSE, BFALSE);
																}
															BgL_res2452z00_2934 =
																BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
																(BgL_plaintextz00_2932, BgL_passwordz00_2925,
																BgL_nbitsz00_1407);
														}
														BgL_res2454z00_2941 = BgL_res2452z00_2934;
													}
												else
													{	/* Unsafe/aes.scm 95 */
														if (INPUT_PORTP(BgL_g1175z00_1402))
															{	/* Unsafe/aes.scm 97 */
																BgL_res2454z00_2941 =
																	BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
																	(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
																	(BgL_g1175z00_1402), BgL_passwordz00_2925,
																	BgL_nbitsz00_1407);
															}
														else
															{	/* Unsafe/aes.scm 100 */
																obj_t BgL_aux2596z00_5491;

																BgL_aux2596z00_5491 =
																	BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
																	BGl_string2688z00zz__aesz00,
																	BgL_g1175z00_1402);
																if (STRINGP(BgL_aux2596z00_5491))
																	{	/* Unsafe/aes.scm 100 */
																		BgL_res2454z00_2941 = BgL_aux2596z00_5491;
																	}
																else
																	{
																		obj_t BgL_auxz00_5680;

																		BgL_auxz00_5680 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2684z00zz__aesz00, BINT(3875L),
																			BGl_string2685z00zz__aesz00,
																			BGl_string2686z00zz__aesz00,
																			BgL_aux2596z00_5491);
																		FAILURE(BgL_auxz00_5680, BFALSE, BFALSE);
																	}
															}
													}
											}
									}
									return BgL_res2454z00_2941;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-encrypt */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2encryptz00zz__aesz00(obj_t
		BgL_plaintextz00_3, obj_t BgL_passwordz00_4, obj_t BgL_nbitsz00_5)
	{
		{	/* Unsafe/aes.scm 91 */
			if (STRINGP(BgL_plaintextz00_3))
				{	/* Unsafe/aes.scm 93 */
					BGL_TAIL return
						BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(BgL_plaintextz00_3,
						BgL_passwordz00_4, BgL_nbitsz00_5);
				}
			else
				{	/* Unsafe/aes.scm 93 */
					if (BGL_MMAPP(BgL_plaintextz00_3))
						{	/* Unsafe/aes.scm 96 */
							obj_t BgL_res2456z00_2950;

							BgL_res2456z00_2950 =
								BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(
								((obj_t) BgL_plaintextz00_3), BgL_passwordz00_4,
								BgL_nbitsz00_5);
							return BgL_res2456z00_2950;
						}
					else
						{	/* Unsafe/aes.scm 95 */
							if (INPUT_PORTP(BgL_plaintextz00_3))
								{	/* Unsafe/aes.scm 97 */
									BGL_TAIL return
										BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
										(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
										(BgL_plaintextz00_3), BgL_passwordz00_4, BgL_nbitsz00_5);
								}
							else
								{	/* Unsafe/aes.scm 97 */
									return
										BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
										BGl_string2688z00zz__aesz00, BgL_plaintextz00_3);
								}
						}
				}
		}

	}



/* _aes-ctr-encrypt-string */
	obj_t BGl__aeszd2ctrzd2encryptzd2stringzd2zz__aesz00(obj_t BgL_env1180z00_12,
		obj_t BgL_opt1179z00_11)
	{
		{	/* Unsafe/aes.scm 105 */
			{	/* Unsafe/aes.scm 105 */
				obj_t BgL_g1181z00_1411;
				obj_t BgL_g1182z00_1412;

				BgL_g1181z00_1411 = VECTOR_REF(BgL_opt1179z00_11, 0L);
				BgL_g1182z00_1412 = VECTOR_REF(BgL_opt1179z00_11, 1L);
				switch (VECTOR_LENGTH(BgL_opt1179z00_11))
					{
					case 2L:

						{	/* Unsafe/aes.scm 105 */

							{	/* Unsafe/aes.scm 105 */
								obj_t BgL_res2458z00_2959;

								{	/* Unsafe/aes.scm 105 */
									obj_t BgL_plaintextz00_2957;
									obj_t BgL_passwordz00_2958;

									if (STRINGP(BgL_g1181z00_1411))
										{	/* Unsafe/aes.scm 105 */
											BgL_plaintextz00_2957 = BgL_g1181z00_1411;
										}
									else
										{
											obj_t BgL_auxz00_5703;

											BgL_auxz00_5703 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(4155L),
												BGl_string2689z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1181z00_1411);
											FAILURE(BgL_auxz00_5703, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1182z00_1412))
										{	/* Unsafe/aes.scm 105 */
											BgL_passwordz00_2958 = BgL_g1182z00_1412;
										}
									else
										{
											obj_t BgL_auxz00_5709;

											BgL_auxz00_5709 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(4155L),
												BGl_string2689z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1182z00_1412);
											FAILURE(BgL_auxz00_5709, BFALSE, BFALSE);
										}
									BgL_res2458z00_2959 =
										BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
										(BgL_plaintextz00_2957, BgL_passwordz00_2958, BINT(128L));
								}
								return BgL_res2458z00_2959;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 105 */
							obj_t BgL_nbitsz00_1416;

							BgL_nbitsz00_1416 = VECTOR_REF(BgL_opt1179z00_11, 2L);
							{	/* Unsafe/aes.scm 105 */

								{	/* Unsafe/aes.scm 105 */
									obj_t BgL_res2459z00_2962;

									{	/* Unsafe/aes.scm 105 */
										obj_t BgL_plaintextz00_2960;
										obj_t BgL_passwordz00_2961;

										if (STRINGP(BgL_g1181z00_1411))
											{	/* Unsafe/aes.scm 105 */
												BgL_plaintextz00_2960 = BgL_g1181z00_1411;
											}
										else
											{
												obj_t BgL_auxz00_5718;

												BgL_auxz00_5718 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(4155L),
													BGl_string2689z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1181z00_1411);
												FAILURE(BgL_auxz00_5718, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1182z00_1412))
											{	/* Unsafe/aes.scm 105 */
												BgL_passwordz00_2961 = BgL_g1182z00_1412;
											}
										else
											{
												obj_t BgL_auxz00_5724;

												BgL_auxz00_5724 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(4155L),
													BGl_string2689z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1182z00_1412);
												FAILURE(BgL_auxz00_5724, BFALSE, BFALSE);
											}
										BgL_res2459z00_2962 =
											BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
											(BgL_plaintextz00_2960, BgL_passwordz00_2961,
											BgL_nbitsz00_1416);
									}
									return BgL_res2459z00_2962;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-encrypt-string */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2encryptzd2stringzd2zz__aesz00(obj_t
		BgL_plaintextz00_8, obj_t BgL_passwordz00_9, obj_t BgL_nbitsz00_10)
	{
		{	/* Unsafe/aes.scm 105 */
			return
				BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(BgL_plaintextz00_8,
				BgL_passwordz00_9, BgL_nbitsz00_10);
		}

	}



/* _aes-ctr-encrypt-mmap */
	obj_t BGl__aeszd2ctrzd2encryptzd2mmapzd2zz__aesz00(obj_t BgL_env1186z00_17,
		obj_t BgL_opt1185z00_16)
	{
		{	/* Unsafe/aes.scm 111 */
			{	/* Unsafe/aes.scm 111 */
				obj_t BgL_g1187z00_1417;
				obj_t BgL_g1188z00_1418;

				BgL_g1187z00_1417 = VECTOR_REF(BgL_opt1185z00_16, 0L);
				BgL_g1188z00_1418 = VECTOR_REF(BgL_opt1185z00_16, 1L);
				switch (VECTOR_LENGTH(BgL_opt1185z00_16))
					{
					case 2L:

						{	/* Unsafe/aes.scm 111 */

							{	/* Unsafe/aes.scm 111 */
								obj_t BgL_res2460z00_2965;

								{	/* Unsafe/aes.scm 111 */
									obj_t BgL_plaintextz00_2963;
									obj_t BgL_passwordz00_2964;

									if (BGL_MMAPP(BgL_g1187z00_1417))
										{	/* Unsafe/aes.scm 111 */
											BgL_plaintextz00_2963 = BgL_g1187z00_1417;
										}
									else
										{
											obj_t BgL_auxz00_5736;

											BgL_auxz00_5736 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(4504L),
												BGl_string2690z00zz__aesz00,
												BGl_string2687z00zz__aesz00, BgL_g1187z00_1417);
											FAILURE(BgL_auxz00_5736, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1188z00_1418))
										{	/* Unsafe/aes.scm 111 */
											BgL_passwordz00_2964 = BgL_g1188z00_1418;
										}
									else
										{
											obj_t BgL_auxz00_5742;

											BgL_auxz00_5742 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(4504L),
												BGl_string2690z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1188z00_1418);
											FAILURE(BgL_auxz00_5742, BFALSE, BFALSE);
										}
									BgL_res2460z00_2965 =
										BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
										(BgL_plaintextz00_2963, BgL_passwordz00_2964, BINT(128L));
								}
								return BgL_res2460z00_2965;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 111 */
							obj_t BgL_nbitsz00_1422;

							BgL_nbitsz00_1422 = VECTOR_REF(BgL_opt1185z00_16, 2L);
							{	/* Unsafe/aes.scm 111 */

								{	/* Unsafe/aes.scm 111 */
									obj_t BgL_res2461z00_2968;

									{	/* Unsafe/aes.scm 111 */
										obj_t BgL_plaintextz00_2966;
										obj_t BgL_passwordz00_2967;

										if (BGL_MMAPP(BgL_g1187z00_1417))
											{	/* Unsafe/aes.scm 111 */
												BgL_plaintextz00_2966 = BgL_g1187z00_1417;
											}
										else
											{
												obj_t BgL_auxz00_5751;

												BgL_auxz00_5751 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(4504L),
													BGl_string2690z00zz__aesz00,
													BGl_string2687z00zz__aesz00, BgL_g1187z00_1417);
												FAILURE(BgL_auxz00_5751, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1188z00_1418))
											{	/* Unsafe/aes.scm 111 */
												BgL_passwordz00_2967 = BgL_g1188z00_1418;
											}
										else
											{
												obj_t BgL_auxz00_5757;

												BgL_auxz00_5757 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(4504L),
													BGl_string2690z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1188z00_1418);
												FAILURE(BgL_auxz00_5757, BFALSE, BFALSE);
											}
										BgL_res2461z00_2968 =
											BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
											(BgL_plaintextz00_2966, BgL_passwordz00_2967,
											BgL_nbitsz00_1422);
									}
									return BgL_res2461z00_2968;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-encrypt-mmap */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2encryptzd2mmapzd2zz__aesz00(obj_t
		BgL_plaintextz00_13, obj_t BgL_passwordz00_14, obj_t BgL_nbitsz00_15)
	{
		{	/* Unsafe/aes.scm 111 */
			return
				BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(BgL_plaintextz00_13,
				BgL_passwordz00_14, BgL_nbitsz00_15);
		}

	}



/* _aes-ctr-encrypt-file */
	obj_t BGl__aeszd2ctrzd2encryptzd2filezd2zz__aesz00(obj_t BgL_env1192z00_22,
		obj_t BgL_opt1191z00_21)
	{
		{	/* Unsafe/aes.scm 117 */
			{	/* Unsafe/aes.scm 117 */
				obj_t BgL_g1193z00_1423;
				obj_t BgL_g1194z00_1424;

				BgL_g1193z00_1423 = VECTOR_REF(BgL_opt1191z00_21, 0L);
				BgL_g1194z00_1424 = VECTOR_REF(BgL_opt1191z00_21, 1L);
				switch (VECTOR_LENGTH(BgL_opt1191z00_21))
					{
					case 2L:

						{	/* Unsafe/aes.scm 117 */

							{	/* Unsafe/aes.scm 117 */
								obj_t BgL_res2463z00_2989;

								{	/* Unsafe/aes.scm 117 */
									obj_t BgL_fnamez00_2969;
									obj_t BgL_passwordz00_2970;

									if (STRINGP(BgL_g1193z00_1423))
										{	/* Unsafe/aes.scm 117 */
											BgL_fnamez00_2969 = BgL_g1193z00_1423;
										}
									else
										{
											obj_t BgL_auxz00_5769;

											BgL_auxz00_5769 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(4851L),
												BGl_string2691z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1193z00_1423);
											FAILURE(BgL_auxz00_5769, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1194z00_1424))
										{	/* Unsafe/aes.scm 117 */
											BgL_passwordz00_2970 = BgL_g1194z00_1424;
										}
									else
										{
											obj_t BgL_auxz00_5775;

											BgL_auxz00_5775 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(4851L),
												BGl_string2691z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1194z00_1424);
											FAILURE(BgL_auxz00_5775, BFALSE, BFALSE);
										}
									{	/* Unsafe/aes.scm 118 */
										obj_t BgL_mmz00_2971;

										BgL_mmz00_2971 =
											BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_2969, BTRUE,
											BFALSE);
										{	/* Unsafe/aes.scm 119 */
											obj_t BgL_exitd1046z00_2974;

											BgL_exitd1046z00_2974 = BGL_EXITD_TOP_AS_OBJ();
											{	/* Unsafe/aes.scm 121 */
												obj_t BgL_zc3z04anonymousza31329ze3z87_5449;

												BgL_zc3z04anonymousza31329ze3z87_5449 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31329ze32568ze5zz__aesz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31329ze3z87_5449,
													(int) (0L), BgL_mmz00_2971);
												{	/* Unsafe/aes.scm 119 */
													obj_t BgL_arg2445z00_2978;

													{	/* Unsafe/aes.scm 119 */
														obj_t BgL_arg2446z00_2979;

														BgL_arg2446z00_2979 =
															BGL_EXITD_PROTECT(BgL_exitd1046z00_2974);
														BgL_arg2445z00_2978 =
															MAKE_YOUNG_PAIR
															(BgL_zc3z04anonymousza31329ze3z87_5449,
															BgL_arg2446z00_2979);
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_2974,
														BgL_arg2445z00_2978);
													BUNSPEC;
												}
												{	/* Unsafe/aes.scm 120 */
													obj_t BgL_tmp1048z00_2976;

													BgL_tmp1048z00_2976 =
														BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
														(BgL_mmz00_2971, BgL_passwordz00_2970, BINT(128L));
													{	/* Unsafe/aes.scm 119 */
														bool_t BgL_test2765z00_5791;

														{	/* Unsafe/aes.scm 119 */
															obj_t BgL_arg2444z00_2984;

															BgL_arg2444z00_2984 =
																BGL_EXITD_PROTECT(BgL_exitd1046z00_2974);
															BgL_test2765z00_5791 = PAIRP(BgL_arg2444z00_2984);
														}
														if (BgL_test2765z00_5791)
															{	/* Unsafe/aes.scm 119 */
																obj_t BgL_arg2442z00_2985;

																{	/* Unsafe/aes.scm 119 */
																	obj_t BgL_arg2443z00_2986;

																	BgL_arg2443z00_2986 =
																		BGL_EXITD_PROTECT(BgL_exitd1046z00_2974);
																	{	/* Unsafe/aes.scm 119 */
																		obj_t BgL_pairz00_2987;

																		if (PAIRP(BgL_arg2443z00_2986))
																			{	/* Unsafe/aes.scm 119 */
																				BgL_pairz00_2987 = BgL_arg2443z00_2986;
																			}
																		else
																			{
																				obj_t BgL_auxz00_5797;

																				BgL_auxz00_5797 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2684z00zz__aesz00,
																					BINT(4969L),
																					BGl_string2691z00zz__aesz00,
																					BGl_string2692z00zz__aesz00,
																					BgL_arg2443z00_2986);
																				FAILURE(BgL_auxz00_5797, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2442z00_2985 = CDR(BgL_pairz00_2987);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_2974,
																	BgL_arg2442z00_2985);
																BUNSPEC;
															}
														else
															{	/* Unsafe/aes.scm 119 */
																BFALSE;
															}
													}
													bgl_close_mmap(BgL_mmz00_2971);
													BgL_res2463z00_2989 = BgL_tmp1048z00_2976;
												}
											}
										}
									}
								}
								return BgL_res2463z00_2989;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 117 */
							obj_t BgL_nbitsz00_1428;

							BgL_nbitsz00_1428 = VECTOR_REF(BgL_opt1191z00_21, 2L);
							{	/* Unsafe/aes.scm 117 */

								{	/* Unsafe/aes.scm 117 */
									obj_t BgL_res2465z00_3010;

									{	/* Unsafe/aes.scm 117 */
										obj_t BgL_fnamez00_2990;
										obj_t BgL_passwordz00_2991;

										if (STRINGP(BgL_g1193z00_1423))
											{	/* Unsafe/aes.scm 117 */
												BgL_fnamez00_2990 = BgL_g1193z00_1423;
											}
										else
											{
												obj_t BgL_auxz00_5807;

												BgL_auxz00_5807 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(4851L),
													BGl_string2691z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1193z00_1423);
												FAILURE(BgL_auxz00_5807, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1194z00_1424))
											{	/* Unsafe/aes.scm 117 */
												BgL_passwordz00_2991 = BgL_g1194z00_1424;
											}
										else
											{
												obj_t BgL_auxz00_5813;

												BgL_auxz00_5813 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(4851L),
													BGl_string2691z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1194z00_1424);
												FAILURE(BgL_auxz00_5813, BFALSE, BFALSE);
											}
										{	/* Unsafe/aes.scm 118 */
											obj_t BgL_mmz00_2992;

											BgL_mmz00_2992 =
												BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_2990, BTRUE,
												BFALSE);
											{	/* Unsafe/aes.scm 119 */
												obj_t BgL_exitd1046z00_2995;

												BgL_exitd1046z00_2995 = BGL_EXITD_TOP_AS_OBJ();
												{	/* Unsafe/aes.scm 121 */
													obj_t BgL_zc3z04anonymousza31329ze3z87_5448;

													BgL_zc3z04anonymousza31329ze3z87_5448 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31329ze3ze5zz__aesz00,
														(int) (0L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31329ze3z87_5448,
														(int) (0L), BgL_mmz00_2992);
													{	/* Unsafe/aes.scm 119 */
														obj_t BgL_arg2445z00_2999;

														{	/* Unsafe/aes.scm 119 */
															obj_t BgL_arg2446z00_3000;

															BgL_arg2446z00_3000 =
																BGL_EXITD_PROTECT(BgL_exitd1046z00_2995);
															BgL_arg2445z00_2999 =
																MAKE_YOUNG_PAIR
																(BgL_zc3z04anonymousza31329ze3z87_5448,
																BgL_arg2446z00_3000);
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_2995,
															BgL_arg2445z00_2999);
														BUNSPEC;
													}
													{	/* Unsafe/aes.scm 120 */
														obj_t BgL_tmp1048z00_2997;

														BgL_tmp1048z00_2997 =
															BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
															(BgL_mmz00_2992, BgL_passwordz00_2991,
															BgL_nbitsz00_1428);
														{	/* Unsafe/aes.scm 119 */
															bool_t BgL_test2769z00_5828;

															{	/* Unsafe/aes.scm 119 */
																obj_t BgL_arg2444z00_3005;

																BgL_arg2444z00_3005 =
																	BGL_EXITD_PROTECT(BgL_exitd1046z00_2995);
																BgL_test2769z00_5828 =
																	PAIRP(BgL_arg2444z00_3005);
															}
															if (BgL_test2769z00_5828)
																{	/* Unsafe/aes.scm 119 */
																	obj_t BgL_arg2442z00_3006;

																	{	/* Unsafe/aes.scm 119 */
																		obj_t BgL_arg2443z00_3007;

																		BgL_arg2443z00_3007 =
																			BGL_EXITD_PROTECT(BgL_exitd1046z00_2995);
																		{	/* Unsafe/aes.scm 119 */
																			obj_t BgL_pairz00_3008;

																			if (PAIRP(BgL_arg2443z00_3007))
																				{	/* Unsafe/aes.scm 119 */
																					BgL_pairz00_3008 =
																						BgL_arg2443z00_3007;
																				}
																			else
																				{
																					obj_t BgL_auxz00_5834;

																					BgL_auxz00_5834 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2684z00zz__aesz00,
																						BINT(4969L),
																						BGl_string2691z00zz__aesz00,
																						BGl_string2692z00zz__aesz00,
																						BgL_arg2443z00_3007);
																					FAILURE(BgL_auxz00_5834, BFALSE,
																						BFALSE);
																				}
																			BgL_arg2442z00_3006 =
																				CDR(BgL_pairz00_3008);
																		}
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_2995,
																		BgL_arg2442z00_3006);
																	BUNSPEC;
																}
															else
																{	/* Unsafe/aes.scm 119 */
																	BFALSE;
																}
														}
														bgl_close_mmap(BgL_mmz00_2992);
														BgL_res2465z00_3010 = BgL_tmp1048z00_2997;
													}
												}
											}
										}
									}
									return BgL_res2465z00_3010;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:1329> */
	obj_t BGl_z62zc3z04anonymousza31329ze3ze5zz__aesz00(obj_t BgL_envz00_5450)
	{
		{	/* Unsafe/aes.scm 119 */
			{	/* Unsafe/aes.scm 121 */
				obj_t BgL_mmz00_5451;

				BgL_mmz00_5451 = ((obj_t) PROCEDURE_REF(BgL_envz00_5450, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_5451);
			}
		}

	}



/* &<@anonymous:1329>2568 */
	obj_t BGl_z62zc3z04anonymousza31329ze32568ze5zz__aesz00(obj_t BgL_envz00_5452)
	{
		{	/* Unsafe/aes.scm 119 */
			{	/* Unsafe/aes.scm 121 */
				obj_t BgL_mmz00_5453;

				BgL_mmz00_5453 = ((obj_t) PROCEDURE_REF(BgL_envz00_5452, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_5453);
			}
		}

	}



/* aes-ctr-encrypt-file */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2encryptzd2filezd2zz__aesz00(obj_t
		BgL_fnamez00_18, obj_t BgL_passwordz00_19, obj_t BgL_nbitsz00_20)
	{
		{	/* Unsafe/aes.scm 117 */
			{	/* Unsafe/aes.scm 118 */
				obj_t BgL_mmz00_3011;

				BgL_mmz00_3011 =
					BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_18, BTRUE, BFALSE);
				{	/* Unsafe/aes.scm 119 */
					obj_t BgL_exitd1046z00_3014;

					BgL_exitd1046z00_3014 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Unsafe/aes.scm 121 */
						obj_t BgL_zc3z04anonymousza31329ze3z87_5454;

						BgL_zc3z04anonymousza31329ze3z87_5454 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31329ze32569ze5zz__aesz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31329ze3z87_5454, (int) (0L),
							BgL_mmz00_3011);
						{	/* Unsafe/aes.scm 119 */
							obj_t BgL_arg2445z00_3018;

							{	/* Unsafe/aes.scm 119 */
								obj_t BgL_arg2446z00_3019;

								BgL_arg2446z00_3019 = BGL_EXITD_PROTECT(BgL_exitd1046z00_3014);
								BgL_arg2445z00_3018 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31329ze3z87_5454,
									BgL_arg2446z00_3019);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_3014, BgL_arg2445z00_3018);
							BUNSPEC;
						}
						{	/* Unsafe/aes.scm 120 */
							obj_t BgL_tmp1048z00_3016;

							BgL_tmp1048z00_3016 =
								BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(BgL_mmz00_3011,
								BgL_passwordz00_19, BgL_nbitsz00_20);
							{	/* Unsafe/aes.scm 119 */
								bool_t BgL_test2771z00_5862;

								{	/* Unsafe/aes.scm 119 */
									obj_t BgL_arg2444z00_3024;

									BgL_arg2444z00_3024 =
										BGL_EXITD_PROTECT(BgL_exitd1046z00_3014);
									BgL_test2771z00_5862 = PAIRP(BgL_arg2444z00_3024);
								}
								if (BgL_test2771z00_5862)
									{	/* Unsafe/aes.scm 119 */
										obj_t BgL_arg2442z00_3025;

										{	/* Unsafe/aes.scm 119 */
											obj_t BgL_arg2443z00_3026;

											BgL_arg2443z00_3026 =
												BGL_EXITD_PROTECT(BgL_exitd1046z00_3014);
											BgL_arg2442z00_3025 = CDR(((obj_t) BgL_arg2443z00_3026));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1046z00_3014,
											BgL_arg2442z00_3025);
										BUNSPEC;
									}
								else
									{	/* Unsafe/aes.scm 119 */
										BFALSE;
									}
							}
							bgl_close_mmap(BgL_mmz00_3011);
							return BgL_tmp1048z00_3016;
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1329>2569 */
	obj_t BGl_z62zc3z04anonymousza31329ze32569ze5zz__aesz00(obj_t BgL_envz00_5455)
	{
		{	/* Unsafe/aes.scm 119 */
			{	/* Unsafe/aes.scm 121 */
				obj_t BgL_mmz00_5456;

				BgL_mmz00_5456 = ((obj_t) PROCEDURE_REF(BgL_envz00_5455, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_5456);
			}
		}

	}



/* _aes-ctr-encrypt-port */
	obj_t BGl__aeszd2ctrzd2encryptzd2portzd2zz__aesz00(obj_t BgL_env1198z00_27,
		obj_t BgL_opt1197z00_26)
	{
		{	/* Unsafe/aes.scm 126 */
			{	/* Unsafe/aes.scm 126 */
				obj_t BgL_g1199z00_1437;
				obj_t BgL_g1200z00_1438;

				BgL_g1199z00_1437 = VECTOR_REF(BgL_opt1197z00_26, 0L);
				BgL_g1200z00_1438 = VECTOR_REF(BgL_opt1197z00_26, 1L);
				switch (VECTOR_LENGTH(BgL_opt1197z00_26))
					{
					case 2L:

						{	/* Unsafe/aes.scm 126 */

							{	/* Unsafe/aes.scm 126 */
								obj_t BgL_inputzd2portzd2_3029;
								obj_t BgL_passwordz00_3030;

								if (INPUT_PORTP(BgL_g1199z00_1437))
									{	/* Unsafe/aes.scm 126 */
										BgL_inputzd2portzd2_3029 = BgL_g1199z00_1437;
									}
								else
									{
										obj_t BgL_auxz00_5878;

										BgL_auxz00_5878 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2684z00zz__aesz00, BINT(5272L),
											BGl_string2693z00zz__aesz00, BGl_string2694z00zz__aesz00,
											BgL_g1199z00_1437);
										FAILURE(BgL_auxz00_5878, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1200z00_1438))
									{	/* Unsafe/aes.scm 126 */
										BgL_passwordz00_3030 = BgL_g1200z00_1438;
									}
								else
									{
										obj_t BgL_auxz00_5884;

										BgL_auxz00_5884 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2684z00zz__aesz00, BINT(5272L),
											BGl_string2693z00zz__aesz00, BGl_string2686z00zz__aesz00,
											BgL_g1200z00_1438);
										FAILURE(BgL_auxz00_5884, BFALSE, BFALSE);
									}
								return
									BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
									(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
									(BgL_inputzd2portzd2_3029), BgL_passwordz00_3030, BINT(128L));
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 126 */
							obj_t BgL_nbitsz00_1442;

							BgL_nbitsz00_1442 = VECTOR_REF(BgL_opt1197z00_26, 2L);
							{	/* Unsafe/aes.scm 126 */

								{	/* Unsafe/aes.scm 126 */
									obj_t BgL_inputzd2portzd2_3035;
									obj_t BgL_passwordz00_3036;

									if (INPUT_PORTP(BgL_g1199z00_1437))
										{	/* Unsafe/aes.scm 126 */
											BgL_inputzd2portzd2_3035 = BgL_g1199z00_1437;
										}
									else
										{
											obj_t BgL_auxz00_5894;

											BgL_auxz00_5894 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(5272L),
												BGl_string2693z00zz__aesz00,
												BGl_string2694z00zz__aesz00, BgL_g1199z00_1437);
											FAILURE(BgL_auxz00_5894, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1200z00_1438))
										{	/* Unsafe/aes.scm 126 */
											BgL_passwordz00_3036 = BgL_g1200z00_1438;
										}
									else
										{
											obj_t BgL_auxz00_5900;

											BgL_auxz00_5900 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(5272L),
												BGl_string2693z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1200z00_1438);
											FAILURE(BgL_auxz00_5900, BFALSE, BFALSE);
										}
									return
										BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
										(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
										(BgL_inputzd2portzd2_3035), BgL_passwordz00_3036,
										BgL_nbitsz00_1442);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-encrypt-port */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2encryptzd2portzd2zz__aesz00(obj_t
		BgL_inputzd2portzd2_23, obj_t BgL_passwordz00_24, obj_t BgL_nbitsz00_25)
	{
		{	/* Unsafe/aes.scm 126 */
			return
				BGl_z52aeszd2ctrzd2encryptz52zz__aesz00
				(BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_inputzd2portzd2_23),
				BgL_passwordz00_24, BgL_nbitsz00_25);
		}

	}



/* _aes-ctr-decrypt */
	obj_t BGl__aeszd2ctrzd2decryptz00zz__aesz00(obj_t BgL_env1204z00_32,
		obj_t BgL_opt1203z00_31)
	{
		{	/* Unsafe/aes.scm 135 */
			{	/* Unsafe/aes.scm 135 */
				obj_t BgL_g1205z00_1444;
				obj_t BgL_g1206z00_1445;

				BgL_g1205z00_1444 = VECTOR_REF(BgL_opt1203z00_31, 0L);
				BgL_g1206z00_1445 = VECTOR_REF(BgL_opt1203z00_31, 1L);
				switch (VECTOR_LENGTH(BgL_opt1203z00_31))
					{
					case 2L:

						{	/* Unsafe/aes.scm 135 */

							{	/* Unsafe/aes.scm 135 */
								obj_t BgL_res2473z00_3061;

								{	/* Unsafe/aes.scm 135 */
									obj_t BgL_passwordz00_3045;

									if (STRINGP(BgL_g1206z00_1445))
										{	/* Unsafe/aes.scm 135 */
											BgL_passwordz00_3045 = BgL_g1206z00_1445;
										}
									else
										{
											obj_t BgL_auxz00_5914;

											BgL_auxz00_5914 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(5860L),
												BGl_string2695z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1206z00_1445);
											FAILURE(BgL_auxz00_5914, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1205z00_1444))
										{	/* Unsafe/aes.scm 137 */
											BgL_res2473z00_3061 =
												BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
												(BgL_g1205z00_1444, BgL_passwordz00_3045, BINT(128L));
										}
									else
										{	/* Unsafe/aes.scm 137 */
											if (BGL_MMAPP(BgL_g1205z00_1444))
												{	/* Unsafe/aes.scm 140 */
													obj_t BgL_res2471z00_3054;

													{	/* Unsafe/aes.scm 140 */
														obj_t BgL_plaintextz00_3052;

														if (BGL_MMAPP(BgL_g1205z00_1444))
															{	/* Unsafe/aes.scm 140 */
																BgL_plaintextz00_3052 = BgL_g1205z00_1444;
															}
														else
															{
																obj_t BgL_auxz00_5926;

																BgL_auxz00_5926 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string2684z00zz__aesz00, BINT(6080L),
																	BGl_string2695z00zz__aesz00,
																	BGl_string2687z00zz__aesz00,
																	BgL_g1205z00_1444);
																FAILURE(BgL_auxz00_5926, BFALSE, BFALSE);
															}
														BgL_res2471z00_3054 =
															BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
															(BgL_plaintextz00_3052, BgL_passwordz00_3045,
															BINT(128L));
													}
													BgL_res2473z00_3061 = BgL_res2471z00_3054;
												}
											else
												{	/* Unsafe/aes.scm 139 */
													if (INPUT_PORTP(BgL_g1205z00_1444))
														{	/* Unsafe/aes.scm 141 */
															BgL_res2473z00_3061 =
																BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
																(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
																(BgL_g1205z00_1444), BgL_passwordz00_3045,
																BINT(128L));
														}
													else
														{	/* Unsafe/aes.scm 144 */
															obj_t BgL_aux2638z00_5533;

															BgL_aux2638z00_5533 =
																BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
																BGl_string2688z00zz__aesz00, BgL_g1205z00_1444);
															if (STRINGP(BgL_aux2638z00_5533))
																{	/* Unsafe/aes.scm 144 */
																	BgL_res2473z00_3061 = BgL_aux2638z00_5533;
																}
															else
																{
																	obj_t BgL_auxz00_5941;

																	BgL_auxz00_5941 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2684z00zz__aesz00, BINT(6216L),
																		BGl_string2695z00zz__aesz00,
																		BGl_string2686z00zz__aesz00,
																		BgL_aux2638z00_5533);
																	FAILURE(BgL_auxz00_5941, BFALSE, BFALSE);
																}
														}
												}
										}
								}
								return BgL_res2473z00_3061;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 135 */
							obj_t BgL_nbitsz00_1449;

							BgL_nbitsz00_1449 = VECTOR_REF(BgL_opt1203z00_31, 2L);
							{	/* Unsafe/aes.scm 135 */

								{	/* Unsafe/aes.scm 135 */
									obj_t BgL_res2477z00_3078;

									{	/* Unsafe/aes.scm 135 */
										obj_t BgL_passwordz00_3062;

										if (STRINGP(BgL_g1206z00_1445))
											{	/* Unsafe/aes.scm 135 */
												BgL_passwordz00_3062 = BgL_g1206z00_1445;
											}
										else
											{
												obj_t BgL_auxz00_5948;

												BgL_auxz00_5948 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(5860L),
													BGl_string2695z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1206z00_1445);
												FAILURE(BgL_auxz00_5948, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1205z00_1444))
											{	/* Unsafe/aes.scm 137 */
												BgL_res2477z00_3078 =
													BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
													(BgL_g1205z00_1444, BgL_passwordz00_3062,
													BgL_nbitsz00_1449);
											}
										else
											{	/* Unsafe/aes.scm 137 */
												if (BGL_MMAPP(BgL_g1205z00_1444))
													{	/* Unsafe/aes.scm 140 */
														obj_t BgL_res2475z00_3071;

														{	/* Unsafe/aes.scm 140 */
															obj_t BgL_plaintextz00_3069;

															if (BGL_MMAPP(BgL_g1205z00_1444))
																{	/* Unsafe/aes.scm 140 */
																	BgL_plaintextz00_3069 = BgL_g1205z00_1444;
																}
															else
																{
																	obj_t BgL_auxz00_5959;

																	BgL_auxz00_5959 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string2684z00zz__aesz00, BINT(6080L),
																		BGl_string2695z00zz__aesz00,
																		BGl_string2687z00zz__aesz00,
																		BgL_g1205z00_1444);
																	FAILURE(BgL_auxz00_5959, BFALSE, BFALSE);
																}
															BgL_res2475z00_3071 =
																BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
																(BgL_plaintextz00_3069, BgL_passwordz00_3062,
																BgL_nbitsz00_1449);
														}
														BgL_res2477z00_3078 = BgL_res2475z00_3071;
													}
												else
													{	/* Unsafe/aes.scm 139 */
														if (INPUT_PORTP(BgL_g1205z00_1444))
															{	/* Unsafe/aes.scm 141 */
																BgL_res2477z00_3078 =
																	BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
																	(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
																	(BgL_g1205z00_1444), BgL_passwordz00_3062,
																	BgL_nbitsz00_1449);
															}
														else
															{	/* Unsafe/aes.scm 144 */
																obj_t BgL_aux2644z00_5539;

																BgL_aux2644z00_5539 =
																	BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
																	BGl_string2688z00zz__aesz00,
																	BgL_g1205z00_1444);
																if (STRINGP(BgL_aux2644z00_5539))
																	{	/* Unsafe/aes.scm 144 */
																		BgL_res2477z00_3078 = BgL_aux2644z00_5539;
																	}
																else
																	{
																		obj_t BgL_auxz00_5972;

																		BgL_auxz00_5972 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string2684z00zz__aesz00, BINT(6216L),
																			BGl_string2695z00zz__aesz00,
																			BGl_string2686z00zz__aesz00,
																			BgL_aux2644z00_5539);
																		FAILURE(BgL_auxz00_5972, BFALSE, BFALSE);
																	}
															}
													}
											}
									}
									return BgL_res2477z00_3078;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-decrypt */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2decryptz00zz__aesz00(obj_t
		BgL_ciphertextz00_28, obj_t BgL_passwordz00_29, obj_t BgL_nbitsz00_30)
	{
		{	/* Unsafe/aes.scm 135 */
			if (STRINGP(BgL_ciphertextz00_28))
				{	/* Unsafe/aes.scm 137 */
					BGL_TAIL return
						BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(BgL_ciphertextz00_28,
						BgL_passwordz00_29, BgL_nbitsz00_30);
				}
			else
				{	/* Unsafe/aes.scm 137 */
					if (BGL_MMAPP(BgL_ciphertextz00_28))
						{	/* Unsafe/aes.scm 140 */
							obj_t BgL_res2479z00_3087;

							BgL_res2479z00_3087 =
								BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(
								((obj_t) BgL_ciphertextz00_28), BgL_passwordz00_29,
								BgL_nbitsz00_30);
							return BgL_res2479z00_3087;
						}
					else
						{	/* Unsafe/aes.scm 139 */
							if (INPUT_PORTP(BgL_ciphertextz00_28))
								{	/* Unsafe/aes.scm 141 */
									BGL_TAIL return
										BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
										(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
										(BgL_ciphertextz00_28), BgL_passwordz00_29,
										BgL_nbitsz00_30);
								}
							else
								{	/* Unsafe/aes.scm 141 */
									return
										BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
										BGl_string2688z00zz__aesz00, BgL_ciphertextz00_28);
								}
						}
				}
		}

	}



/* _aes-ctr-decrypt-string */
	obj_t BGl__aeszd2ctrzd2decryptzd2stringzd2zz__aesz00(obj_t BgL_env1210z00_37,
		obj_t BgL_opt1209z00_36)
	{
		{	/* Unsafe/aes.scm 149 */
			{	/* Unsafe/aes.scm 149 */
				obj_t BgL_g1211z00_1453;
				obj_t BgL_g1212z00_1454;

				BgL_g1211z00_1453 = VECTOR_REF(BgL_opt1209z00_36, 0L);
				BgL_g1212z00_1454 = VECTOR_REF(BgL_opt1209z00_36, 1L);
				switch (VECTOR_LENGTH(BgL_opt1209z00_36))
					{
					case 2L:

						{	/* Unsafe/aes.scm 149 */

							{	/* Unsafe/aes.scm 149 */
								obj_t BgL_res2481z00_3096;

								{	/* Unsafe/aes.scm 149 */
									obj_t BgL_plaintextz00_3094;
									obj_t BgL_passwordz00_3095;

									if (STRINGP(BgL_g1211z00_1453))
										{	/* Unsafe/aes.scm 149 */
											BgL_plaintextz00_3094 = BgL_g1211z00_1453;
										}
									else
										{
											obj_t BgL_auxz00_5995;

											BgL_auxz00_5995 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(6498L),
												BGl_string2696z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1211z00_1453);
											FAILURE(BgL_auxz00_5995, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1212z00_1454))
										{	/* Unsafe/aes.scm 149 */
											BgL_passwordz00_3095 = BgL_g1212z00_1454;
										}
									else
										{
											obj_t BgL_auxz00_6001;

											BgL_auxz00_6001 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(6498L),
												BGl_string2696z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1212z00_1454);
											FAILURE(BgL_auxz00_6001, BFALSE, BFALSE);
										}
									BgL_res2481z00_3096 =
										BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
										(BgL_plaintextz00_3094, BgL_passwordz00_3095, BINT(128L));
								}
								return BgL_res2481z00_3096;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 149 */
							obj_t BgL_nbitsz00_1458;

							BgL_nbitsz00_1458 = VECTOR_REF(BgL_opt1209z00_36, 2L);
							{	/* Unsafe/aes.scm 149 */

								{	/* Unsafe/aes.scm 149 */
									obj_t BgL_res2482z00_3099;

									{	/* Unsafe/aes.scm 149 */
										obj_t BgL_plaintextz00_3097;
										obj_t BgL_passwordz00_3098;

										if (STRINGP(BgL_g1211z00_1453))
											{	/* Unsafe/aes.scm 149 */
												BgL_plaintextz00_3097 = BgL_g1211z00_1453;
											}
										else
											{
												obj_t BgL_auxz00_6010;

												BgL_auxz00_6010 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(6498L),
													BGl_string2696z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1211z00_1453);
												FAILURE(BgL_auxz00_6010, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1212z00_1454))
											{	/* Unsafe/aes.scm 149 */
												BgL_passwordz00_3098 = BgL_g1212z00_1454;
											}
										else
											{
												obj_t BgL_auxz00_6016;

												BgL_auxz00_6016 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(6498L),
													BGl_string2696z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1212z00_1454);
												FAILURE(BgL_auxz00_6016, BFALSE, BFALSE);
											}
										BgL_res2482z00_3099 =
											BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
											(BgL_plaintextz00_3097, BgL_passwordz00_3098,
											BgL_nbitsz00_1458);
									}
									return BgL_res2482z00_3099;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-decrypt-string */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2decryptzd2stringzd2zz__aesz00(obj_t
		BgL_plaintextz00_33, obj_t BgL_passwordz00_34, obj_t BgL_nbitsz00_35)
	{
		{	/* Unsafe/aes.scm 149 */
			return
				BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(BgL_plaintextz00_33,
				BgL_passwordz00_34, BgL_nbitsz00_35);
		}

	}



/* _aes-ctr-decrypt-mmap */
	obj_t BGl__aeszd2ctrzd2decryptzd2mmapzd2zz__aesz00(obj_t BgL_env1216z00_42,
		obj_t BgL_opt1215z00_41)
	{
		{	/* Unsafe/aes.scm 155 */
			{	/* Unsafe/aes.scm 155 */
				obj_t BgL_g1217z00_1459;
				obj_t BgL_g1218z00_1460;

				BgL_g1217z00_1459 = VECTOR_REF(BgL_opt1215z00_41, 0L);
				BgL_g1218z00_1460 = VECTOR_REF(BgL_opt1215z00_41, 1L);
				switch (VECTOR_LENGTH(BgL_opt1215z00_41))
					{
					case 2L:

						{	/* Unsafe/aes.scm 155 */

							{	/* Unsafe/aes.scm 155 */
								obj_t BgL_res2483z00_3102;

								{	/* Unsafe/aes.scm 155 */
									obj_t BgL_plaintextz00_3100;
									obj_t BgL_passwordz00_3101;

									if (BGL_MMAPP(BgL_g1217z00_1459))
										{	/* Unsafe/aes.scm 155 */
											BgL_plaintextz00_3100 = BgL_g1217z00_1459;
										}
									else
										{
											obj_t BgL_auxz00_6028;

											BgL_auxz00_6028 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(6847L),
												BGl_string2697z00zz__aesz00,
												BGl_string2687z00zz__aesz00, BgL_g1217z00_1459);
											FAILURE(BgL_auxz00_6028, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1218z00_1460))
										{	/* Unsafe/aes.scm 155 */
											BgL_passwordz00_3101 = BgL_g1218z00_1460;
										}
									else
										{
											obj_t BgL_auxz00_6034;

											BgL_auxz00_6034 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(6847L),
												BGl_string2697z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1218z00_1460);
											FAILURE(BgL_auxz00_6034, BFALSE, BFALSE);
										}
									BgL_res2483z00_3102 =
										BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
										(BgL_plaintextz00_3100, BgL_passwordz00_3101, BINT(128L));
								}
								return BgL_res2483z00_3102;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 155 */
							obj_t BgL_nbitsz00_1464;

							BgL_nbitsz00_1464 = VECTOR_REF(BgL_opt1215z00_41, 2L);
							{	/* Unsafe/aes.scm 155 */

								{	/* Unsafe/aes.scm 155 */
									obj_t BgL_res2484z00_3105;

									{	/* Unsafe/aes.scm 155 */
										obj_t BgL_plaintextz00_3103;
										obj_t BgL_passwordz00_3104;

										if (BGL_MMAPP(BgL_g1217z00_1459))
											{	/* Unsafe/aes.scm 155 */
												BgL_plaintextz00_3103 = BgL_g1217z00_1459;
											}
										else
											{
												obj_t BgL_auxz00_6043;

												BgL_auxz00_6043 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(6847L),
													BGl_string2697z00zz__aesz00,
													BGl_string2687z00zz__aesz00, BgL_g1217z00_1459);
												FAILURE(BgL_auxz00_6043, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1218z00_1460))
											{	/* Unsafe/aes.scm 155 */
												BgL_passwordz00_3104 = BgL_g1218z00_1460;
											}
										else
											{
												obj_t BgL_auxz00_6049;

												BgL_auxz00_6049 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(6847L),
													BGl_string2697z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1218z00_1460);
												FAILURE(BgL_auxz00_6049, BFALSE, BFALSE);
											}
										BgL_res2484z00_3105 =
											BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
											(BgL_plaintextz00_3103, BgL_passwordz00_3104,
											BgL_nbitsz00_1464);
									}
									return BgL_res2484z00_3105;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-decrypt-mmap */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2decryptzd2mmapzd2zz__aesz00(obj_t
		BgL_plaintextz00_38, obj_t BgL_passwordz00_39, obj_t BgL_nbitsz00_40)
	{
		{	/* Unsafe/aes.scm 155 */
			return
				BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(BgL_plaintextz00_38,
				BgL_passwordz00_39, BgL_nbitsz00_40);
		}

	}



/* _aes-ctr-decrypt-file */
	obj_t BGl__aeszd2ctrzd2decryptzd2filezd2zz__aesz00(obj_t BgL_env1222z00_47,
		obj_t BgL_opt1221z00_46)
	{
		{	/* Unsafe/aes.scm 161 */
			{	/* Unsafe/aes.scm 161 */
				obj_t BgL_g1223z00_1465;
				obj_t BgL_g1224z00_1466;

				BgL_g1223z00_1465 = VECTOR_REF(BgL_opt1221z00_46, 0L);
				BgL_g1224z00_1466 = VECTOR_REF(BgL_opt1221z00_46, 1L);
				switch (VECTOR_LENGTH(BgL_opt1221z00_46))
					{
					case 2L:

						{	/* Unsafe/aes.scm 161 */

							{	/* Unsafe/aes.scm 161 */
								obj_t BgL_res2486z00_3126;

								{	/* Unsafe/aes.scm 161 */
									obj_t BgL_fnamez00_3106;
									obj_t BgL_passwordz00_3107;

									if (STRINGP(BgL_g1223z00_1465))
										{	/* Unsafe/aes.scm 161 */
											BgL_fnamez00_3106 = BgL_g1223z00_1465;
										}
									else
										{
											obj_t BgL_auxz00_6061;

											BgL_auxz00_6061 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(7194L),
												BGl_string2698z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1223z00_1465);
											FAILURE(BgL_auxz00_6061, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1224z00_1466))
										{	/* Unsafe/aes.scm 161 */
											BgL_passwordz00_3107 = BgL_g1224z00_1466;
										}
									else
										{
											obj_t BgL_auxz00_6067;

											BgL_auxz00_6067 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(7194L),
												BGl_string2698z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1224z00_1466);
											FAILURE(BgL_auxz00_6067, BFALSE, BFALSE);
										}
									{	/* Unsafe/aes.scm 162 */
										obj_t BgL_mmz00_3108;

										BgL_mmz00_3108 =
											BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_3106, BTRUE,
											BFALSE);
										{	/* Unsafe/aes.scm 163 */
											obj_t BgL_exitd1055z00_3111;

											BgL_exitd1055z00_3111 = BGL_EXITD_TOP_AS_OBJ();
											{	/* Unsafe/aes.scm 165 */
												obj_t BgL_zc3z04anonymousza31335ze3z87_5458;

												BgL_zc3z04anonymousza31335ze3z87_5458 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31335ze32570ze5zz__aesz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31335ze3z87_5458,
													(int) (0L), BgL_mmz00_3108);
												{	/* Unsafe/aes.scm 163 */
													obj_t BgL_arg2445z00_3115;

													{	/* Unsafe/aes.scm 163 */
														obj_t BgL_arg2446z00_3116;

														BgL_arg2446z00_3116 =
															BGL_EXITD_PROTECT(BgL_exitd1055z00_3111);
														BgL_arg2445z00_3115 =
															MAKE_YOUNG_PAIR
															(BgL_zc3z04anonymousza31335ze3z87_5458,
															BgL_arg2446z00_3116);
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_3111,
														BgL_arg2445z00_3115);
													BUNSPEC;
												}
												{	/* Unsafe/aes.scm 164 */
													obj_t BgL_tmp1057z00_3113;

													BgL_tmp1057z00_3113 =
														BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
														(BgL_mmz00_3108, BgL_passwordz00_3107, BINT(128L));
													{	/* Unsafe/aes.scm 163 */
														bool_t BgL_test2801z00_6083;

														{	/* Unsafe/aes.scm 163 */
															obj_t BgL_arg2444z00_3121;

															BgL_arg2444z00_3121 =
																BGL_EXITD_PROTECT(BgL_exitd1055z00_3111);
															BgL_test2801z00_6083 = PAIRP(BgL_arg2444z00_3121);
														}
														if (BgL_test2801z00_6083)
															{	/* Unsafe/aes.scm 163 */
																obj_t BgL_arg2442z00_3122;

																{	/* Unsafe/aes.scm 163 */
																	obj_t BgL_arg2443z00_3123;

																	BgL_arg2443z00_3123 =
																		BGL_EXITD_PROTECT(BgL_exitd1055z00_3111);
																	{	/* Unsafe/aes.scm 163 */
																		obj_t BgL_pairz00_3124;

																		if (PAIRP(BgL_arg2443z00_3123))
																			{	/* Unsafe/aes.scm 163 */
																				BgL_pairz00_3124 = BgL_arg2443z00_3123;
																			}
																		else
																			{
																				obj_t BgL_auxz00_6089;

																				BgL_auxz00_6089 =
																					BGl_typezd2errorzd2zz__errorz00
																					(BGl_string2684z00zz__aesz00,
																					BINT(7312L),
																					BGl_string2698z00zz__aesz00,
																					BGl_string2692z00zz__aesz00,
																					BgL_arg2443z00_3123);
																				FAILURE(BgL_auxz00_6089, BFALSE,
																					BFALSE);
																			}
																		BgL_arg2442z00_3122 = CDR(BgL_pairz00_3124);
																	}
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_3111,
																	BgL_arg2442z00_3122);
																BUNSPEC;
															}
														else
															{	/* Unsafe/aes.scm 163 */
																BFALSE;
															}
													}
													bgl_close_mmap(BgL_mmz00_3108);
													BgL_res2486z00_3126 = BgL_tmp1057z00_3113;
												}
											}
										}
									}
								}
								return BgL_res2486z00_3126;
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 161 */
							obj_t BgL_nbitsz00_1470;

							BgL_nbitsz00_1470 = VECTOR_REF(BgL_opt1221z00_46, 2L);
							{	/* Unsafe/aes.scm 161 */

								{	/* Unsafe/aes.scm 161 */
									obj_t BgL_res2488z00_3147;

									{	/* Unsafe/aes.scm 161 */
										obj_t BgL_fnamez00_3127;
										obj_t BgL_passwordz00_3128;

										if (STRINGP(BgL_g1223z00_1465))
											{	/* Unsafe/aes.scm 161 */
												BgL_fnamez00_3127 = BgL_g1223z00_1465;
											}
										else
											{
												obj_t BgL_auxz00_6099;

												BgL_auxz00_6099 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(7194L),
													BGl_string2698z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1223z00_1465);
												FAILURE(BgL_auxz00_6099, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1224z00_1466))
											{	/* Unsafe/aes.scm 161 */
												BgL_passwordz00_3128 = BgL_g1224z00_1466;
											}
										else
											{
												obj_t BgL_auxz00_6105;

												BgL_auxz00_6105 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2684z00zz__aesz00, BINT(7194L),
													BGl_string2698z00zz__aesz00,
													BGl_string2686z00zz__aesz00, BgL_g1224z00_1466);
												FAILURE(BgL_auxz00_6105, BFALSE, BFALSE);
											}
										{	/* Unsafe/aes.scm 162 */
											obj_t BgL_mmz00_3129;

											BgL_mmz00_3129 =
												BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_3127, BTRUE,
												BFALSE);
											{	/* Unsafe/aes.scm 163 */
												obj_t BgL_exitd1055z00_3132;

												BgL_exitd1055z00_3132 = BGL_EXITD_TOP_AS_OBJ();
												{	/* Unsafe/aes.scm 165 */
													obj_t BgL_zc3z04anonymousza31335ze3z87_5457;

													BgL_zc3z04anonymousza31335ze3z87_5457 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31335ze3ze5zz__aesz00,
														(int) (0L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31335ze3z87_5457,
														(int) (0L), BgL_mmz00_3129);
													{	/* Unsafe/aes.scm 163 */
														obj_t BgL_arg2445z00_3136;

														{	/* Unsafe/aes.scm 163 */
															obj_t BgL_arg2446z00_3137;

															BgL_arg2446z00_3137 =
																BGL_EXITD_PROTECT(BgL_exitd1055z00_3132);
															BgL_arg2445z00_3136 =
																MAKE_YOUNG_PAIR
																(BgL_zc3z04anonymousza31335ze3z87_5457,
																BgL_arg2446z00_3137);
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_3132,
															BgL_arg2445z00_3136);
														BUNSPEC;
													}
													{	/* Unsafe/aes.scm 164 */
														obj_t BgL_tmp1057z00_3134;

														BgL_tmp1057z00_3134 =
															BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
															(BgL_mmz00_3129, BgL_passwordz00_3128,
															BgL_nbitsz00_1470);
														{	/* Unsafe/aes.scm 163 */
															bool_t BgL_test2805z00_6120;

															{	/* Unsafe/aes.scm 163 */
																obj_t BgL_arg2444z00_3142;

																BgL_arg2444z00_3142 =
																	BGL_EXITD_PROTECT(BgL_exitd1055z00_3132);
																BgL_test2805z00_6120 =
																	PAIRP(BgL_arg2444z00_3142);
															}
															if (BgL_test2805z00_6120)
																{	/* Unsafe/aes.scm 163 */
																	obj_t BgL_arg2442z00_3143;

																	{	/* Unsafe/aes.scm 163 */
																		obj_t BgL_arg2443z00_3144;

																		BgL_arg2443z00_3144 =
																			BGL_EXITD_PROTECT(BgL_exitd1055z00_3132);
																		{	/* Unsafe/aes.scm 163 */
																			obj_t BgL_pairz00_3145;

																			if (PAIRP(BgL_arg2443z00_3144))
																				{	/* Unsafe/aes.scm 163 */
																					BgL_pairz00_3145 =
																						BgL_arg2443z00_3144;
																				}
																			else
																				{
																					obj_t BgL_auxz00_6126;

																					BgL_auxz00_6126 =
																						BGl_typezd2errorzd2zz__errorz00
																						(BGl_string2684z00zz__aesz00,
																						BINT(7312L),
																						BGl_string2698z00zz__aesz00,
																						BGl_string2692z00zz__aesz00,
																						BgL_arg2443z00_3144);
																					FAILURE(BgL_auxz00_6126, BFALSE,
																						BFALSE);
																				}
																			BgL_arg2442z00_3143 =
																				CDR(BgL_pairz00_3145);
																		}
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_3132,
																		BgL_arg2442z00_3143);
																	BUNSPEC;
																}
															else
																{	/* Unsafe/aes.scm 163 */
																	BFALSE;
																}
														}
														bgl_close_mmap(BgL_mmz00_3129);
														BgL_res2488z00_3147 = BgL_tmp1057z00_3134;
													}
												}
											}
										}
									}
									return BgL_res2488z00_3147;
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:1335> */
	obj_t BGl_z62zc3z04anonymousza31335ze3ze5zz__aesz00(obj_t BgL_envz00_5459)
	{
		{	/* Unsafe/aes.scm 163 */
			{	/* Unsafe/aes.scm 165 */
				obj_t BgL_mmz00_5460;

				BgL_mmz00_5460 = ((obj_t) PROCEDURE_REF(BgL_envz00_5459, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_5460);
			}
		}

	}



/* &<@anonymous:1335>2570 */
	obj_t BGl_z62zc3z04anonymousza31335ze32570ze5zz__aesz00(obj_t BgL_envz00_5461)
	{
		{	/* Unsafe/aes.scm 163 */
			{	/* Unsafe/aes.scm 165 */
				obj_t BgL_mmz00_5462;

				BgL_mmz00_5462 = ((obj_t) PROCEDURE_REF(BgL_envz00_5461, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_5462);
			}
		}

	}



/* aes-ctr-decrypt-file */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2decryptzd2filezd2zz__aesz00(obj_t
		BgL_fnamez00_43, obj_t BgL_passwordz00_44, obj_t BgL_nbitsz00_45)
	{
		{	/* Unsafe/aes.scm 161 */
			{	/* Unsafe/aes.scm 162 */
				obj_t BgL_mmz00_3148;

				BgL_mmz00_3148 =
					BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_43, BTRUE, BFALSE);
				{	/* Unsafe/aes.scm 163 */
					obj_t BgL_exitd1055z00_3151;

					BgL_exitd1055z00_3151 = BGL_EXITD_TOP_AS_OBJ();
					{	/* Unsafe/aes.scm 165 */
						obj_t BgL_zc3z04anonymousza31335ze3z87_5463;

						BgL_zc3z04anonymousza31335ze3z87_5463 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31335ze32571ze5zz__aesz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31335ze3z87_5463, (int) (0L),
							BgL_mmz00_3148);
						{	/* Unsafe/aes.scm 163 */
							obj_t BgL_arg2445z00_3155;

							{	/* Unsafe/aes.scm 163 */
								obj_t BgL_arg2446z00_3156;

								BgL_arg2446z00_3156 = BGL_EXITD_PROTECT(BgL_exitd1055z00_3151);
								BgL_arg2445z00_3155 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31335ze3z87_5463,
									BgL_arg2446z00_3156);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_3151, BgL_arg2445z00_3155);
							BUNSPEC;
						}
						{	/* Unsafe/aes.scm 164 */
							obj_t BgL_tmp1057z00_3153;

							BgL_tmp1057z00_3153 =
								BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(BgL_mmz00_3148,
								BgL_passwordz00_44, BgL_nbitsz00_45);
							{	/* Unsafe/aes.scm 163 */
								bool_t BgL_test2807z00_6154;

								{	/* Unsafe/aes.scm 163 */
									obj_t BgL_arg2444z00_3161;

									BgL_arg2444z00_3161 =
										BGL_EXITD_PROTECT(BgL_exitd1055z00_3151);
									BgL_test2807z00_6154 = PAIRP(BgL_arg2444z00_3161);
								}
								if (BgL_test2807z00_6154)
									{	/* Unsafe/aes.scm 163 */
										obj_t BgL_arg2442z00_3162;

										{	/* Unsafe/aes.scm 163 */
											obj_t BgL_arg2443z00_3163;

											BgL_arg2443z00_3163 =
												BGL_EXITD_PROTECT(BgL_exitd1055z00_3151);
											BgL_arg2442z00_3162 = CDR(((obj_t) BgL_arg2443z00_3163));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1055z00_3151,
											BgL_arg2442z00_3162);
										BUNSPEC;
									}
								else
									{	/* Unsafe/aes.scm 163 */
										BFALSE;
									}
							}
							bgl_close_mmap(BgL_mmz00_3148);
							return BgL_tmp1057z00_3153;
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1335>2571 */
	obj_t BGl_z62zc3z04anonymousza31335ze32571ze5zz__aesz00(obj_t BgL_envz00_5464)
	{
		{	/* Unsafe/aes.scm 163 */
			{	/* Unsafe/aes.scm 165 */
				obj_t BgL_mmz00_5465;

				BgL_mmz00_5465 = ((obj_t) PROCEDURE_REF(BgL_envz00_5464, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_5465);
			}
		}

	}



/* _aes-ctr-decrypt-port */
	obj_t BGl__aeszd2ctrzd2decryptzd2portzd2zz__aesz00(obj_t BgL_env1228z00_52,
		obj_t BgL_opt1227z00_51)
	{
		{	/* Unsafe/aes.scm 170 */
			{	/* Unsafe/aes.scm 170 */
				obj_t BgL_g1229z00_1479;
				obj_t BgL_g1230z00_1480;

				BgL_g1229z00_1479 = VECTOR_REF(BgL_opt1227z00_51, 0L);
				BgL_g1230z00_1480 = VECTOR_REF(BgL_opt1227z00_51, 1L);
				switch (VECTOR_LENGTH(BgL_opt1227z00_51))
					{
					case 2L:

						{	/* Unsafe/aes.scm 170 */

							{	/* Unsafe/aes.scm 170 */
								obj_t BgL_inputzd2portzd2_3166;
								obj_t BgL_passwordz00_3167;

								if (INPUT_PORTP(BgL_g1229z00_1479))
									{	/* Unsafe/aes.scm 170 */
										BgL_inputzd2portzd2_3166 = BgL_g1229z00_1479;
									}
								else
									{
										obj_t BgL_auxz00_6170;

										BgL_auxz00_6170 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2684z00zz__aesz00, BINT(7615L),
											BGl_string2699z00zz__aesz00, BGl_string2694z00zz__aesz00,
											BgL_g1229z00_1479);
										FAILURE(BgL_auxz00_6170, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1230z00_1480))
									{	/* Unsafe/aes.scm 170 */
										BgL_passwordz00_3167 = BgL_g1230z00_1480;
									}
								else
									{
										obj_t BgL_auxz00_6176;

										BgL_auxz00_6176 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2684z00zz__aesz00, BINT(7615L),
											BGl_string2699z00zz__aesz00, BGl_string2686z00zz__aesz00,
											BgL_g1230z00_1480);
										FAILURE(BgL_auxz00_6176, BFALSE, BFALSE);
									}
								return
									BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
									(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
									(BgL_inputzd2portzd2_3166), BgL_passwordz00_3167, BINT(128L));
							}
						}
						break;
					case 3L:

						{	/* Unsafe/aes.scm 170 */
							obj_t BgL_nbitsz00_1484;

							BgL_nbitsz00_1484 = VECTOR_REF(BgL_opt1227z00_51, 2L);
							{	/* Unsafe/aes.scm 170 */

								{	/* Unsafe/aes.scm 170 */
									obj_t BgL_inputzd2portzd2_3172;
									obj_t BgL_passwordz00_3173;

									if (INPUT_PORTP(BgL_g1229z00_1479))
										{	/* Unsafe/aes.scm 170 */
											BgL_inputzd2portzd2_3172 = BgL_g1229z00_1479;
										}
									else
										{
											obj_t BgL_auxz00_6186;

											BgL_auxz00_6186 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(7615L),
												BGl_string2699z00zz__aesz00,
												BGl_string2694z00zz__aesz00, BgL_g1229z00_1479);
											FAILURE(BgL_auxz00_6186, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1230z00_1480))
										{	/* Unsafe/aes.scm 170 */
											BgL_passwordz00_3173 = BgL_g1230z00_1480;
										}
									else
										{
											obj_t BgL_auxz00_6192;

											BgL_auxz00_6192 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2684z00zz__aesz00, BINT(7615L),
												BGl_string2699z00zz__aesz00,
												BGl_string2686z00zz__aesz00, BgL_g1230z00_1480);
											FAILURE(BgL_auxz00_6192, BFALSE, BFALSE);
										}
									return
										BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
										(BGl_readzd2stringzd2zz__r4_input_6_10_2z00
										(BgL_inputzd2portzd2_3172), BgL_passwordz00_3173,
										BgL_nbitsz00_1484);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* aes-ctr-decrypt-port */
	BGL_EXPORTED_DEF obj_t BGl_aeszd2ctrzd2decryptzd2portzd2zz__aesz00(obj_t
		BgL_inputzd2portzd2_48, obj_t BgL_passwordz00_49, obj_t BgL_nbitsz00_50)
	{
		{	/* Unsafe/aes.scm 170 */
			return
				BGl_z52aeszd2ctrzd2decryptz52zz__aesz00
				(BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_inputzd2portzd2_48),
				BgL_passwordz00_49, BgL_nbitsz00_50);
		}

	}



/* %aes-ctr-encrypt */
	obj_t BGl_z52aeszd2ctrzd2encryptz52zz__aesz00(obj_t BgL_plaintextz00_55,
		obj_t BgL_passwordz00_56, obj_t BgL_nbitsz00_57)
	{
		{	/* Unsafe/aes.scm 236 */
			if (CBOOL(BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BgL_nbitsz00_57,
						CNST_TABLE_REF(4))))
				{	/* Unsafe/aes.scm 239 */
					BFALSE;
				}
			else
				{	/* Unsafe/aes.scm 239 */
					BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
						BGl_string2700z00zz__aesz00, BgL_nbitsz00_57);
				}
			{	/* Unsafe/aes.scm 243 */
				bool_t BgL_test2813z00_6208;

				if (STRINGP(BgL_plaintextz00_55))
					{	/* Unsafe/aes.scm 243 */
						BgL_test2813z00_6208 = ((bool_t) 1);
					}
				else
					{	/* Unsafe/aes.scm 243 */
						BgL_test2813z00_6208 = BGL_MMAPP(BgL_plaintextz00_55);
					}
				if (BgL_test2813z00_6208)
					{	/* Unsafe/aes.scm 243 */
						BFALSE;
					}
				else
					{	/* Unsafe/aes.scm 243 */
						BGl_bigloozd2typezd2errorz00zz__errorz00(CNST_TABLE_REF(2),
							BGl_string2701z00zz__aesz00, BgL_plaintextz00_55);
					}
			}
			{	/* Unsafe/aes.scm 247 */
				obj_t BgL_statez00_1499;

				{	/* Unsafe/aes.scm 247 */
					obj_t BgL_res2494z00_3198;

					{	/* Unsafe/aes.scm 411 */
						obj_t BgL_vz00_3186;

						BgL_vz00_3186 = make_vector(4L, BUNSPEC);
						{
							long BgL_iz00_3189;

							BgL_iz00_3189 = 0L;
						BgL_loopzd2izd21075z00_3188:
							if ((BgL_iz00_3189 < 4L))
								{	/* Unsafe/aes.scm 412 */
									{	/* Unsafe/aes.scm 413 */
										obj_t BgL_arg1490z00_3191;

										BgL_arg1490z00_3191 =
											BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0L));
										VECTOR_SET(BgL_vz00_3186, BgL_iz00_3189,
											BgL_arg1490z00_3191);
									}
									{
										long BgL_iz00_6220;

										BgL_iz00_6220 = (BgL_iz00_3189 + 1L);
										BgL_iz00_3189 = BgL_iz00_6220;
										goto BgL_loopzd2izd21075z00_3188;
									}
								}
							else
								{	/* Unsafe/aes.scm 412 */
									((bool_t) 0);
								}
						}
						BgL_res2494z00_3198 = BgL_vz00_3186;
					}
					BgL_statez00_1499 = BgL_res2494z00_3198;
				}
				{	/* Unsafe/aes.scm 247 */
					int BgL_lenz00_1500;

					if (STRINGP(BgL_plaintextz00_55))
						{	/* Unsafe/aes.scm 248 */
							BgL_lenz00_1500 =
								CINT(BGL_PROCEDURE_CALL1
								(BGl_stringzd2lengthzd2envz00zz__r4_strings_6_7z00,
									BgL_plaintextz00_55));
						}
					else
						{	/* Unsafe/aes.scm 248 */
							if (BGL_MMAPP(BgL_plaintextz00_55))
								{	/* Unsafe/aes.scm 248 */
									BgL_lenz00_1500 =
										CINT(BGL_PROCEDURE_CALL1
										(BGl_mmapzd2lengthzd2envz00zz__mmapz00,
											BgL_plaintextz00_55));
								}
							else
								{	/* Unsafe/aes.scm 248 */
									BgL_lenz00_1500 = (int) (0L);
						}}
					{	/* Unsafe/aes.scm 248 */
						obj_t BgL_keyz00_1501;

						BgL_keyz00_1501 =
							BGl_aeszd2passwordzd2ze3keyze3zz__aesz00(BgL_passwordz00_56,
							CINT(BgL_nbitsz00_57), BgL_statez00_1499);
						{	/* Unsafe/aes.scm 251 */
							long BgL_blockcountz00_1502;

							{	/* Unsafe/aes.scm 226 */
								obj_t BgL_rz00_3200;

								BgL_rz00_3200 =
									BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(BgL_lenz00_1500),
									BINT((int) (16L)));
								if (INTEGERP(BgL_rz00_3200))
									{	/* Unsafe/aes.scm 227 */
										BgL_blockcountz00_1502 = (long) CINT(BgL_rz00_3200);
									}
								else
									{	/* Unsafe/aes.scm 227 */
										BgL_blockcountz00_1502 =
											(long) (REAL_TO_DOUBLE(BGl_ceilingz00zz__r4_numbers_6_5z00
												(BgL_rz00_3200)));
							}}
							{	/* Unsafe/aes.scm 256 */
								obj_t BgL_counterblockz00_1503;

								{	/* Unsafe/aes.scm 257 */
									int BgL_arg1430z00_1635;

									BgL_arg1430z00_1635 = (int) (16L);
									{	/* Llib/srfi4.scm 447 */

										BgL_counterblockz00_1503 =
											BGl_makezd2u8vectorzd2zz__srfi4z00(
											(long) (BgL_arg1430z00_1635), (uint8_t) (0));
								}}
								{	/* Unsafe/aes.scm 257 */
									long BgL_noncez00_1504;

									BgL_noncez00_1504 = bgl_current_seconds();
									{	/* Unsafe/aes.scm 258 */
										obj_t BgL_keyschedulez00_1505;

										BgL_keyschedulez00_1505 =
											BGl_aeszd2keyzd2expansionz00zz__aesz00(BgL_keyz00_1501);
										{	/* Unsafe/aes.scm 262 */
											obj_t BgL_ciphertextz00_1506;

											{	/* Unsafe/aes.scm 265 */
												long BgL_arg1428z00_1631;

												BgL_arg1428z00_1631 =
													((long) ((int) (8L)) + (long) (BgL_lenz00_1500));
												{	/* Ieee/string.scm 172 */

													BgL_ciphertextz00_1506 =
														make_string(BgL_arg1428z00_1631,
														((unsigned char) ' '));
											}}
											{	/* Unsafe/aes.scm 265 */

												{	/* Unsafe/aes.scm 269 */
													long BgL_sz00_1508;

													{	/* Unsafe/aes.scm 269 */
														long BgL_arg1343z00_1510;

														{	/* Unsafe/aes.scm 269 */
															long BgL_nz00_3213;

															BgL_nz00_3213 =
																(BgL_noncez00_1504 >> (int) ((0L * 8L)));
															if ((BgL_nz00_3213 < ((long) 0)))
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1343z00_1510 = NEG(BgL_nz00_3213);
																}
															else
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1343z00_1510 = BgL_nz00_3213;
																}
														}
														BgL_sz00_1508 = (long) (BgL_arg1343z00_1510);
													}
													{	/* Unsafe/aes.scm 270 */
														long BgL_arg1342z00_1509;

														BgL_arg1342z00_1509 = (BgL_sz00_1508 & 255L);
														{	/* Unsafe/aes.scm 270 */
															uint8_t BgL_tmpz00_6267;

															BgL_tmpz00_6267 = (uint8_t) (BgL_arg1342z00_1509);
															BGL_U8VSET(BgL_counterblockz00_1503, 0L,
																BgL_tmpz00_6267);
														} BUNSPEC;
												}}
												{	/* Unsafe/aes.scm 275 */
													long BgL_arg1347z00_1514;
													long BgL_arg1348z00_1515;

													BgL_arg1347z00_1514 = (0L + 4L);
													BgL_arg1348z00_1515 = (0L & 255L);
													{	/* Unsafe/aes.scm 275 */
														uint8_t BgL_tmpz00_6272;

														BgL_tmpz00_6272 = (uint8_t) (BgL_arg1348z00_1515);
														BGL_U8VSET(BgL_counterblockz00_1503,
															BgL_arg1347z00_1514, BgL_tmpz00_6272);
													} BUNSPEC;
												}
												{	/* Unsafe/aes.scm 269 */
													long BgL_sz00_1517;

													{	/* Unsafe/aes.scm 269 */
														long BgL_arg1350z00_1519;

														{	/* Unsafe/aes.scm 269 */
															long BgL_nz00_3225;

															BgL_nz00_3225 =
																(BgL_noncez00_1504 >> (int) ((1L * 8L)));
															if ((BgL_nz00_3225 < ((long) 0)))
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1350z00_1519 = NEG(BgL_nz00_3225);
																}
															else
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1350z00_1519 = BgL_nz00_3225;
																}
														}
														BgL_sz00_1517 = (long) (BgL_arg1350z00_1519);
													}
													{	/* Unsafe/aes.scm 270 */
														long BgL_arg1349z00_1518;

														BgL_arg1349z00_1518 = (BgL_sz00_1517 & 255L);
														{	/* Unsafe/aes.scm 270 */
															uint8_t BgL_tmpz00_6283;

															BgL_tmpz00_6283 = (uint8_t) (BgL_arg1349z00_1518);
															BGL_U8VSET(BgL_counterblockz00_1503, 1L,
																BgL_tmpz00_6283);
														} BUNSPEC;
												}}
												{	/* Unsafe/aes.scm 275 */
													long BgL_arg1354z00_1523;
													long BgL_arg1356z00_1524;

													BgL_arg1354z00_1523 = (1L + 4L);
													BgL_arg1356z00_1524 = (0L & 255L);
													{	/* Unsafe/aes.scm 275 */
														uint8_t BgL_tmpz00_6288;

														BgL_tmpz00_6288 = (uint8_t) (BgL_arg1356z00_1524);
														BGL_U8VSET(BgL_counterblockz00_1503,
															BgL_arg1354z00_1523, BgL_tmpz00_6288);
													} BUNSPEC;
												}
												{	/* Unsafe/aes.scm 269 */
													long BgL_sz00_1526;

													{	/* Unsafe/aes.scm 269 */
														long BgL_arg1358z00_1528;

														{	/* Unsafe/aes.scm 269 */
															long BgL_nz00_3237;

															BgL_nz00_3237 =
																(BgL_noncez00_1504 >> (int) ((2L * 8L)));
															if ((BgL_nz00_3237 < ((long) 0)))
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1358z00_1528 = NEG(BgL_nz00_3237);
																}
															else
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1358z00_1528 = BgL_nz00_3237;
																}
														}
														BgL_sz00_1526 = (long) (BgL_arg1358z00_1528);
													}
													{	/* Unsafe/aes.scm 270 */
														long BgL_arg1357z00_1527;

														BgL_arg1357z00_1527 = (BgL_sz00_1526 & 255L);
														{	/* Unsafe/aes.scm 270 */
															uint8_t BgL_tmpz00_6299;

															BgL_tmpz00_6299 = (uint8_t) (BgL_arg1357z00_1527);
															BGL_U8VSET(BgL_counterblockz00_1503, 2L,
																BgL_tmpz00_6299);
														} BUNSPEC;
												}}
												{	/* Unsafe/aes.scm 275 */
													long BgL_arg1361z00_1532;
													long BgL_arg1362z00_1533;

													BgL_arg1361z00_1532 = (2L + 4L);
													BgL_arg1362z00_1533 = (0L & 255L);
													{	/* Unsafe/aes.scm 275 */
														uint8_t BgL_tmpz00_6304;

														BgL_tmpz00_6304 = (uint8_t) (BgL_arg1362z00_1533);
														BGL_U8VSET(BgL_counterblockz00_1503,
															BgL_arg1361z00_1532, BgL_tmpz00_6304);
													} BUNSPEC;
												}
												{	/* Unsafe/aes.scm 269 */
													long BgL_sz00_1535;

													{	/* Unsafe/aes.scm 269 */
														long BgL_arg1364z00_1537;

														{	/* Unsafe/aes.scm 269 */
															long BgL_nz00_3249;

															BgL_nz00_3249 =
																(BgL_noncez00_1504 >> (int) ((3L * 8L)));
															if ((BgL_nz00_3249 < ((long) 0)))
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1364z00_1537 = NEG(BgL_nz00_3249);
																}
															else
																{	/* Unsafe/aes.scm 269 */
																	BgL_arg1364z00_1537 = BgL_nz00_3249;
																}
														}
														BgL_sz00_1535 = (long) (BgL_arg1364z00_1537);
													}
													{	/* Unsafe/aes.scm 270 */
														long BgL_arg1363z00_1536;

														BgL_arg1363z00_1536 = (BgL_sz00_1535 & 255L);
														{	/* Unsafe/aes.scm 270 */
															uint8_t BgL_tmpz00_6315;

															BgL_tmpz00_6315 = (uint8_t) (BgL_arg1363z00_1536);
															BGL_U8VSET(BgL_counterblockz00_1503, 3L,
																BgL_tmpz00_6315);
														} BUNSPEC;
												}}
												{	/* Unsafe/aes.scm 275 */
													long BgL_arg1367z00_1541;
													long BgL_arg1368z00_1542;

													BgL_arg1367z00_1541 = (3L + 4L);
													BgL_arg1368z00_1542 = (0L & 255L);
													{	/* Unsafe/aes.scm 275 */
														uint8_t BgL_tmpz00_6320;

														BgL_tmpz00_6320 = (uint8_t) (BgL_arg1368z00_1542);
														BGL_U8VSET(BgL_counterblockz00_1503,
															BgL_arg1367z00_1541, BgL_tmpz00_6320);
													} BUNSPEC;
												}
												{
													long BgL_bz00_1544;

													BgL_bz00_1544 = 0L;
												BgL_zc3z04anonymousza31369ze3z87_1545:
													if ((BgL_bz00_1544 < BgL_blockcountz00_1502))
														{	/* Unsafe/aes.scm 277 */
															{	/* Unsafe/aes.scm 282 */
																long BgL_jz00_1548;

																BgL_jz00_1548 = (0L * 8L);
																{	/* Unsafe/aes.scm 283 */
																	long BgL_sz00_1549;

																	BgL_sz00_1549 =
																		(
																		(long) (
																			((unsigned long) (BgL_bz00_1544) >>
																				(int) (BgL_jz00_1548))) & 255L);
																	{	/* Unsafe/aes.scm 284 */
																		long BgL_arg1371z00_1550;

																		BgL_arg1371z00_1550 = (15L - 0L);
																		{	/* Unsafe/aes.scm 284 */
																			uint8_t BgL_tmpz00_6332;

																			BgL_tmpz00_6332 =
																				(uint8_t) (BgL_sz00_1549);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1371z00_1550, BgL_tmpz00_6332);
																		} BUNSPEC;
																	}
																	{	/* Unsafe/aes.scm 286 */
																		long BgL_arg1372z00_1551;

																		BgL_arg1372z00_1551 = (15L - (0L + 4L));
																		{	/* Unsafe/aes.scm 286 */
																			uint8_t BgL_tmpz00_6337;

																			BgL_tmpz00_6337 = (uint8_t) (0L);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1372z00_1551, BgL_tmpz00_6337);
																		} BUNSPEC;
															}}}
															{	/* Unsafe/aes.scm 282 */
																long BgL_jz00_1555;

																BgL_jz00_1555 = (1L * 8L);
																{	/* Unsafe/aes.scm 283 */
																	long BgL_sz00_1556;

																	BgL_sz00_1556 =
																		(
																		(long) (
																			((unsigned long) (BgL_bz00_1544) >>
																				(int) (BgL_jz00_1555))) & 255L);
																	{	/* Unsafe/aes.scm 284 */
																		long BgL_arg1376z00_1557;

																		BgL_arg1376z00_1557 = (15L - 1L);
																		{	/* Unsafe/aes.scm 284 */
																			uint8_t BgL_tmpz00_6347;

																			BgL_tmpz00_6347 =
																				(uint8_t) (BgL_sz00_1556);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1376z00_1557, BgL_tmpz00_6347);
																		} BUNSPEC;
																	}
																	{	/* Unsafe/aes.scm 286 */
																		long BgL_arg1377z00_1558;

																		BgL_arg1377z00_1558 = (15L - (1L + 4L));
																		{	/* Unsafe/aes.scm 286 */
																			uint8_t BgL_tmpz00_6352;

																			BgL_tmpz00_6352 = (uint8_t) (0L);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1377z00_1558, BgL_tmpz00_6352);
																		} BUNSPEC;
															}}}
															{	/* Unsafe/aes.scm 282 */
																long BgL_jz00_1562;

																BgL_jz00_1562 = (2L * 8L);
																{	/* Unsafe/aes.scm 283 */
																	long BgL_sz00_1563;

																	BgL_sz00_1563 =
																		(
																		(long) (
																			((unsigned long) (BgL_bz00_1544) >>
																				(int) (BgL_jz00_1562))) & 255L);
																	{	/* Unsafe/aes.scm 284 */
																		long BgL_arg1380z00_1564;

																		BgL_arg1380z00_1564 = (15L - 2L);
																		{	/* Unsafe/aes.scm 284 */
																			uint8_t BgL_tmpz00_6362;

																			BgL_tmpz00_6362 =
																				(uint8_t) (BgL_sz00_1563);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1380z00_1564, BgL_tmpz00_6362);
																		} BUNSPEC;
																	}
																	{	/* Unsafe/aes.scm 286 */
																		long BgL_arg1382z00_1565;

																		BgL_arg1382z00_1565 = (15L - (2L + 4L));
																		{	/* Unsafe/aes.scm 286 */
																			uint8_t BgL_tmpz00_6367;

																			BgL_tmpz00_6367 = (uint8_t) (0L);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1382z00_1565, BgL_tmpz00_6367);
																		} BUNSPEC;
															}}}
															{	/* Unsafe/aes.scm 282 */
																long BgL_jz00_1569;

																BgL_jz00_1569 = (3L * 8L);
																{	/* Unsafe/aes.scm 283 */
																	long BgL_sz00_1570;

																	BgL_sz00_1570 =
																		(
																		(long) (
																			((unsigned long) (BgL_bz00_1544) >>
																				(int) (BgL_jz00_1569))) & 255L);
																	{	/* Unsafe/aes.scm 284 */
																		long BgL_arg1387z00_1571;

																		BgL_arg1387z00_1571 = (15L - 3L);
																		{	/* Unsafe/aes.scm 284 */
																			uint8_t BgL_tmpz00_6377;

																			BgL_tmpz00_6377 =
																				(uint8_t) (BgL_sz00_1570);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1387z00_1571, BgL_tmpz00_6377);
																		} BUNSPEC;
																	}
																	{	/* Unsafe/aes.scm 286 */
																		long BgL_arg1388z00_1572;

																		BgL_arg1388z00_1572 = (15L - (3L + 4L));
																		{	/* Unsafe/aes.scm 286 */
																			uint8_t BgL_tmpz00_6382;

																			BgL_tmpz00_6382 = (uint8_t) (0L);
																			BGL_U8VSET(BgL_counterblockz00_1503,
																				BgL_arg1388z00_1572, BgL_tmpz00_6382);
																		} BUNSPEC;
															}}}
															{	/* Unsafe/aes.scm 288 */
																obj_t BgL_ciphercntrz00_1575;

																BgL_ciphercntrz00_1575 =
																	BGl_aeszd2cipherzd2zz__aesz00
																	(BgL_counterblockz00_1503,
																	BgL_keyschedulez00_1505, BgL_statez00_1499);
																{	/* Unsafe/aes.scm 288 */
																	int BgL_blocklengthz00_1576;

																	if (
																		(BgL_bz00_1544 <
																			(BgL_blockcountz00_1502 - 1L)))
																		{	/* Unsafe/aes.scm 289 */
																			BgL_blocklengthz00_1576 = (int) (16L);
																		}
																	else
																		{	/* Unsafe/aes.scm 291 */
																			obj_t BgL_arg1416z00_1615;

																			{	/* Unsafe/aes.scm 291 */
																				long BgL_arg1417z00_1616;
																				int BgL_arg1418z00_1617;

																				BgL_arg1417z00_1616 =
																					((long) (BgL_lenz00_1500) - 1L);
																				BgL_arg1418z00_1617 = (int) (16L);
																				BgL_arg1416z00_1615 =
																					BGl_remainderz00zz__r4_numbers_6_5_fixnumz00
																					(BINT(BgL_arg1417z00_1616),
																					BINT(BgL_arg1418z00_1617));
																			}
																			BgL_blocklengthz00_1576 =
																				(int) (
																				((long) CINT(BgL_arg1416z00_1615) +
																					1L));
																		}
																	{	/* Unsafe/aes.scm 289 */
																		long BgL_startz00_1577;

																		BgL_startz00_1577 =
																			(
																			(long) (
																				(int) (8L)) +
																			(BgL_bz00_1544 * (long) ((int) (16L))));
																		{	/* Unsafe/aes.scm 292 */

																			if (STRINGP(BgL_plaintextz00_55))
																				{	/* Unsafe/aes.scm 294 */
																					obj_t BgL_refz00_1579;

																					BgL_refz00_1579 =
																						BGl_u8stringzd2refzd2envz00zz__aesz00;
																					{
																						long BgL_iz00_1581;

																						{	/* Unsafe/aes.scm 296 */
																							bool_t BgL_tmpz00_6407;

																							BgL_iz00_1581 = 0L;
																						BgL_zc3z04anonymousza31392ze3z87_1582:
																							if (
																								(BgL_iz00_1581 <
																									(long)
																									(BgL_blocklengthz00_1576)))
																								{	/* Unsafe/aes.scm 296 */
																									{	/* Unsafe/aes.scm 297 */
																										long BgL_jz00_1584;

																										BgL_jz00_1584 =
																											(
																											(BgL_bz00_1544 *
																												(long) (
																													(int) (16L))) +
																											BgL_iz00_1581);
																										{	/* Unsafe/aes.scm 297 */
																											int BgL_pz00_1585;

																											BgL_pz00_1585 =
																												BGl_z62u8stringzd2refzb0zz__aesz00
																												(BgL_refz00_1579,
																												BgL_plaintextz00_55,
																												BgL_jz00_1584);
																											{	/* Unsafe/aes.scm 298 */
																												long BgL_bytez00_1586;

																												{	/* Unsafe/aes.scm 299 */
																													long
																														BgL_arg1395z00_1588;
																													{	/* Unsafe/aes.scm 299 */
																														uint8_t
																															BgL_arg1396z00_1589;
																														BgL_arg1396z00_1589
																															=
																															BGL_U8VREF
																															(BgL_ciphercntrz00_1575,
																															BgL_iz00_1581);
																														{	/* Unsafe/aes.scm 299 */
																															long
																																BgL_arg2398z00_3311;
																															BgL_arg2398z00_3311
																																=
																																(long)
																																(BgL_arg1396z00_1589);
																															BgL_arg1395z00_1588
																																=
																																(long)
																																(BgL_arg2398z00_3311);
																													}}
																													BgL_bytez00_1586 =
																														(
																														(long)
																														(BgL_pz00_1585) ^
																														BgL_arg1395z00_1588);
																												}
																												{	/* Unsafe/aes.scm 299 */

																													{	/* Unsafe/aes.scm 300 */
																														long
																															BgL_arg1394z00_1587;
																														BgL_arg1394z00_1587
																															=
																															(BgL_iz00_1581 +
																															BgL_startz00_1577);
																														{	/* Unsafe/aes.scm 300 */
																															int BgL_iz00_3318;
																															int BgL_vz00_3319;

																															BgL_iz00_3318 =
																																(int)
																																(BgL_arg1394z00_1587);
																															BgL_vz00_3319 =
																																(int)
																																(BgL_bytez00_1586);
																															{	/* Unsafe/aes.scm 438 */
																																unsigned char
																																	BgL_auxz00_6430;
																																long
																																	BgL_tmpz00_6428;
																																BgL_auxz00_6430
																																	=
																																	((long)
																																	(BgL_vz00_3319));
																																BgL_tmpz00_6428
																																	=
																																	(long)
																																	(BgL_iz00_3318);
																																STRING_SET
																																	(BgL_ciphertextz00_1506,
																																	BgL_tmpz00_6428,
																																	BgL_auxz00_6430);
																									}}}}}}}
																									{
																										long BgL_iz00_6434;

																										BgL_iz00_6434 =
																											(BgL_iz00_1581 + 1L);
																										BgL_iz00_1581 =
																											BgL_iz00_6434;
																										goto
																											BgL_zc3z04anonymousza31392ze3z87_1582;
																									}
																								}
																							else
																								{	/* Unsafe/aes.scm 296 */
																									BgL_tmpz00_6407 =
																										((bool_t) 0);
																								}
																							BBOOL(BgL_tmpz00_6407);
																						}
																					}
																				}
																			else
																				{	/* Unsafe/aes.scm 294 */
																					if (BGL_MMAPP(BgL_plaintextz00_55))
																						{
																							long BgL_iz00_1597;

																							{	/* Unsafe/aes.scm 296 */
																								bool_t BgL_tmpz00_6439;

																								BgL_iz00_1597 = 0L;
																							BgL_zc3z04anonymousza31402ze3z87_1598:
																								if (
																									(BgL_iz00_1597 <
																										(long)
																										(BgL_blocklengthz00_1576)))
																									{	/* Unsafe/aes.scm 296 */
																										{	/* Unsafe/aes.scm 297 */
																											long BgL_jz00_1600;

																											BgL_jz00_1600 =
																												(
																												(BgL_bz00_1544 *
																													(long) (
																														(int) (16L))) +
																												BgL_iz00_1597);
																											{	/* Unsafe/aes.scm 297 */
																												obj_t BgL_pz00_1601;

																												BgL_pz00_1601 =
																													BGL_PROCEDURE_CALL2
																													(BGl_mmapzd2refzd2envz00zz__mmapz00,
																													BgL_plaintextz00_55,
																													BINT(BgL_jz00_1600));
																												{	/* Unsafe/aes.scm 298 */
																													long BgL_bytez00_1602;

																													{	/* Unsafe/aes.scm 299 */
																														long
																															BgL_arg1405z00_1604;
																														{	/* Unsafe/aes.scm 299 */
																															uint8_t
																																BgL_arg1406z00_1605;
																															BgL_arg1406z00_1605
																																=
																																BGL_U8VREF
																																(BgL_ciphercntrz00_1575,
																																BgL_iz00_1597);
																															{	/* Unsafe/aes.scm 299 */
																																long
																																	BgL_arg2398z00_3336;
																																BgL_arg2398z00_3336
																																	=
																																	(long)
																																	(BgL_arg1406z00_1605);
																																BgL_arg1405z00_1604
																																	=
																																	(long)
																																	(BgL_arg2398z00_3336);
																														}}
																														BgL_bytez00_1602 =
																															(
																															(long)
																															CINT
																															(BgL_pz00_1601) ^
																															BgL_arg1405z00_1604);
																													}
																													{	/* Unsafe/aes.scm 299 */

																														{	/* Unsafe/aes.scm 300 */
																															long
																																BgL_arg1404z00_1603;
																															BgL_arg1404z00_1603
																																=
																																(BgL_iz00_1597 +
																																BgL_startz00_1577);
																															{	/* Unsafe/aes.scm 300 */
																																int
																																	BgL_iz00_3343;
																																int
																																	BgL_vz00_3344;
																																BgL_iz00_3343 =
																																	(int)
																																	(BgL_arg1404z00_1603);
																																BgL_vz00_3344 =
																																	(int)
																																	(BgL_bytez00_1602);
																																{	/* Unsafe/aes.scm 438 */
																																	unsigned char
																																		BgL_auxz00_6463;
																																	long
																																		BgL_tmpz00_6461;
																																	BgL_auxz00_6463
																																		=
																																		((long)
																																		(BgL_vz00_3344));
																																	BgL_tmpz00_6461
																																		=
																																		(long)
																																		(BgL_iz00_3343);
																																	STRING_SET
																																		(BgL_ciphertextz00_1506,
																																		BgL_tmpz00_6461,
																																		BgL_auxz00_6463);
																										}}}}}}}
																										{
																											long BgL_iz00_6467;

																											BgL_iz00_6467 =
																												(BgL_iz00_1597 + 1L);
																											BgL_iz00_1597 =
																												BgL_iz00_6467;
																											goto
																												BgL_zc3z04anonymousza31402ze3z87_1598;
																										}
																									}
																								else
																									{	/* Unsafe/aes.scm 296 */
																										BgL_tmpz00_6439 =
																											((bool_t) 0);
																									}
																								BBOOL(BgL_tmpz00_6439);
																							}
																						}
																					else
																						{	/* Unsafe/aes.scm 294 */
																							BINT(0L);
																						}
																				}
																		}
																	}
																}
															}
															{
																long BgL_bz00_6471;

																BgL_bz00_6471 = (BgL_bz00_1544 + 1L);
																BgL_bz00_1544 = BgL_bz00_6471;
																goto BgL_zc3z04anonymousza31369ze3z87_1545;
															}
														}
													else
														{	/* Unsafe/aes.scm 277 */
															((bool_t) 0);
														}
												}
												{
													long BgL_iz00_3370;

													BgL_iz00_3370 = 0L;
												BgL_loopzd2izd21068z00_3369:
													if ((BgL_iz00_3370 < (long) ((int) (8L))))
														{	/* Unsafe/aes.scm 303 */
															{	/* Unsafe/aes.scm 304 */
																long BgL_arg1424z00_3376;

																{	/* Unsafe/aes.scm 304 */
																	uint8_t BgL_arg1425z00_3377;

																	BgL_arg1425z00_3377 =
																		BGL_U8VREF(BgL_counterblockz00_1503,
																		BgL_iz00_3370);
																	{	/* Unsafe/aes.scm 304 */
																		long BgL_arg2398z00_3381;

																		BgL_arg2398z00_3381 =
																			(long) (BgL_arg1425z00_3377);
																		BgL_arg1424z00_3376 =
																			(long) (BgL_arg2398z00_3381);
																}}
																{	/* Unsafe/aes.scm 304 */
																	int BgL_iz00_3384;
																	int BgL_vz00_3385;

																	BgL_iz00_3384 = (int) (BgL_iz00_3370);
																	BgL_vz00_3385 = (int) (BgL_arg1424z00_3376);
																	{	/* Unsafe/aes.scm 438 */
																		unsigned char BgL_auxz00_6484;
																		long BgL_tmpz00_6482;

																		BgL_auxz00_6484 = ((long) (BgL_vz00_3385));
																		BgL_tmpz00_6482 = (long) (BgL_iz00_3384);
																		STRING_SET(BgL_ciphertextz00_1506,
																			BgL_tmpz00_6482, BgL_auxz00_6484);
															}}}
															{
																long BgL_iz00_6488;

																BgL_iz00_6488 = (BgL_iz00_3370 + 1L);
																BgL_iz00_3370 = BgL_iz00_6488;
																goto BgL_loopzd2izd21068z00_3369;
															}
														}
													else
														{	/* Unsafe/aes.scm 303 */
															((bool_t) 0);
														}
												}
												return BgL_ciphertextz00_1506;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* %aes-ctr-decrypt */
	obj_t BGl_z52aeszd2ctrzd2decryptz52zz__aesz00(obj_t BgL_ciphertextz00_60,
		obj_t BgL_passwordz00_61, obj_t BgL_nbitsz00_62)
	{
		{	/* Unsafe/aes.scm 314 */
			if (CBOOL(BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BgL_nbitsz00_62,
						CNST_TABLE_REF(4))))
				{	/* Unsafe/aes.scm 317 */
					BFALSE;
				}
			else
				{	/* Unsafe/aes.scm 317 */
					BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
						BGl_string2700z00zz__aesz00, BgL_nbitsz00_62);
				}
			{	/* Unsafe/aes.scm 320 */
				obj_t BgL_statez00_1650;

				{	/* Unsafe/aes.scm 320 */
					obj_t BgL_res2507z00_3405;

					{	/* Unsafe/aes.scm 411 */
						obj_t BgL_vz00_3393;

						BgL_vz00_3393 = make_vector(4L, BUNSPEC);
						{
							long BgL_iz00_3396;

							BgL_iz00_3396 = 0L;
						BgL_loopzd2izd21075z00_3395:
							if ((BgL_iz00_3396 < 4L))
								{	/* Unsafe/aes.scm 412 */
									{	/* Unsafe/aes.scm 413 */
										obj_t BgL_arg1490z00_3398;

										BgL_arg1490z00_3398 =
											BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0L));
										VECTOR_SET(BgL_vz00_3393, BgL_iz00_3396,
											BgL_arg1490z00_3398);
									}
									{
										long BgL_iz00_6502;

										BgL_iz00_6502 = (BgL_iz00_3396 + 1L);
										BgL_iz00_3396 = BgL_iz00_6502;
										goto BgL_loopzd2izd21075z00_3395;
									}
								}
							else
								{	/* Unsafe/aes.scm 412 */
									((bool_t) 0);
								}
						}
						BgL_res2507z00_3405 = BgL_vz00_3393;
					}
					BgL_statez00_1650 = BgL_res2507z00_3405;
				}
				{	/* Unsafe/aes.scm 320 */
					obj_t BgL_keyz00_1651;

					BgL_keyz00_1651 =
						BGl_aeszd2passwordzd2ze3keyze3zz__aesz00(BgL_passwordz00_61,
						CINT(BgL_nbitsz00_62), BgL_statez00_1650);
					{	/* Unsafe/aes.scm 321 */
						obj_t BgL_keyschedulez00_1652;

						BgL_keyschedulez00_1652 =
							BGl_aeszd2keyzd2expansionz00zz__aesz00(BgL_keyz00_1651);
						{	/* Unsafe/aes.scm 322 */
							int BgL_lenz00_1653;

							BgL_lenz00_1653 =
								(int) (
								(STRING_LENGTH(
										((obj_t) BgL_ciphertextz00_60)) - (long) ((int) (8L))));
							{	/* Unsafe/aes.scm 323 */
								obj_t BgL_blockcountz00_1654;

								{	/* Unsafe/aes.scm 226 */
									obj_t BgL_rz00_3411;

									BgL_rz00_3411 =
										BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(BgL_lenz00_1653),
										BINT((int) (16L)));
									if (INTEGERP(BgL_rz00_3411))
										{	/* Unsafe/aes.scm 227 */
											BgL_blockcountz00_1654 = BgL_rz00_3411;
										}
									else
										{	/* Unsafe/aes.scm 227 */
											BgL_blockcountz00_1654 =
												BINT(
												(long) (REAL_TO_DOUBLE
													(BGl_ceilingz00zz__r4_numbers_6_5z00
														(BgL_rz00_3411))));
								}}
								{	/* Unsafe/aes.scm 324 */
									obj_t BgL_counterblockz00_1655;

									{	/* Unsafe/aes.scm 325 */
										int BgL_arg1484z00_1730;

										BgL_arg1484z00_1730 = (int) (16L);
										{	/* Llib/srfi4.scm 447 */

											BgL_counterblockz00_1655 =
												BGl_makezd2u8vectorzd2zz__srfi4z00(
												(long) (BgL_arg1484z00_1730), (uint8_t) (0));
									}}
									{	/* Unsafe/aes.scm 325 */
										obj_t BgL_plaintextz00_1656;

										{	/* Ieee/string.scm 172 */

											BgL_plaintextz00_1656 =
												make_string(
												(long) (BgL_lenz00_1653), ((unsigned char) ' '));
										}
										{	/* Unsafe/aes.scm 326 */

											{
												long BgL_iz00_3430;

												BgL_iz00_3430 = 0L;
											BgL_loopzd2izd21070z00_3429:
												if ((BgL_iz00_3430 < (long) ((int) (8L))))
													{	/* Unsafe/aes.scm 329 */
														{	/* Unsafe/aes.scm 330 */
															int BgL_arg1441z00_3436;

															BgL_arg1441z00_3436 =
																(int) (
																(STRING_REF(
																		((obj_t) BgL_ciphertextz00_60),
																		(long) ((int) (BgL_iz00_3430)))));
															{	/* Unsafe/aes.scm 330 */
																uint8_t BgL_tmpz00_6538;

																BgL_tmpz00_6538 =
																	(uint8_t) (BgL_arg1441z00_3436);
																BGL_U8VSET(BgL_counterblockz00_1655,
																	BgL_iz00_3430, BgL_tmpz00_6538);
															} BUNSPEC;
														}
														{
															long BgL_iz00_6541;

															BgL_iz00_6541 = (BgL_iz00_3430 + 1L);
															BgL_iz00_3430 = BgL_iz00_6541;
															goto BgL_loopzd2izd21070z00_3429;
														}
													}
												else
													{	/* Unsafe/aes.scm 329 */
														((bool_t) 0);
													}
											}
											{
												long BgL_bz00_1667;

												BgL_bz00_1667 = 0L;
											BgL_zc3z04anonymousza31444ze3z87_1668:
												if (
													(BgL_bz00_1667 < (long) CINT(BgL_blockcountz00_1654)))
													{	/* Unsafe/aes.scm 332 */
														{	/* Unsafe/aes.scm 334 */
															long BgL_vz00_1671;

															BgL_vz00_1671 =
																(
																(long) (
																	((unsigned long) (BgL_bz00_1667) >>
																		(int) ((0L * 8L)))) & 255L);
															{	/* Unsafe/aes.scm 335 */
																long BgL_arg1446z00_1672;

																BgL_arg1446z00_1672 = (15L - 0L);
																{	/* Unsafe/aes.scm 335 */
																	uint8_t BgL_tmpz00_6553;

																	BgL_tmpz00_6553 = (uint8_t) (BgL_vz00_1671);
																	BGL_U8VSET(BgL_counterblockz00_1655,
																		BgL_arg1446z00_1672, BgL_tmpz00_6553);
																} BUNSPEC;
														}}
														{	/* Unsafe/aes.scm 338 */
															long BgL_arg1449z00_1676;

															BgL_arg1449z00_1676 = (15L - (0L + 4L));
															{	/* Unsafe/aes.scm 338 */
																uint8_t BgL_tmpz00_6558;

																BgL_tmpz00_6558 = (uint8_t) (0L);
																BGL_U8VSET(BgL_counterblockz00_1655,
																	BgL_arg1449z00_1676, BgL_tmpz00_6558);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 334 */
															long BgL_vz00_1679;

															BgL_vz00_1679 =
																(
																(long) (
																	((unsigned long) (BgL_bz00_1667) >>
																		(int) ((1L * 8L)))) & 255L);
															{	/* Unsafe/aes.scm 335 */
																long BgL_arg1451z00_1680;

																BgL_arg1451z00_1680 = (15L - 1L);
																{	/* Unsafe/aes.scm 335 */
																	uint8_t BgL_tmpz00_6568;

																	BgL_tmpz00_6568 = (uint8_t) (BgL_vz00_1679);
																	BGL_U8VSET(BgL_counterblockz00_1655,
																		BgL_arg1451z00_1680, BgL_tmpz00_6568);
																} BUNSPEC;
														}}
														{	/* Unsafe/aes.scm 338 */
															long BgL_arg1454z00_1684;

															BgL_arg1454z00_1684 = (15L - (1L + 4L));
															{	/* Unsafe/aes.scm 338 */
																uint8_t BgL_tmpz00_6573;

																BgL_tmpz00_6573 = (uint8_t) (0L);
																BGL_U8VSET(BgL_counterblockz00_1655,
																	BgL_arg1454z00_1684, BgL_tmpz00_6573);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 334 */
															long BgL_vz00_1687;

															BgL_vz00_1687 =
																(
																(long) (
																	((unsigned long) (BgL_bz00_1667) >>
																		(int) ((2L * 8L)))) & 255L);
															{	/* Unsafe/aes.scm 335 */
																long BgL_arg1456z00_1688;

																BgL_arg1456z00_1688 = (15L - 2L);
																{	/* Unsafe/aes.scm 335 */
																	uint8_t BgL_tmpz00_6583;

																	BgL_tmpz00_6583 = (uint8_t) (BgL_vz00_1687);
																	BGL_U8VSET(BgL_counterblockz00_1655,
																		BgL_arg1456z00_1688, BgL_tmpz00_6583);
																} BUNSPEC;
														}}
														{	/* Unsafe/aes.scm 338 */
															long BgL_arg1459z00_1692;

															BgL_arg1459z00_1692 = (15L - (2L + 4L));
															{	/* Unsafe/aes.scm 338 */
																uint8_t BgL_tmpz00_6588;

																BgL_tmpz00_6588 = (uint8_t) (0L);
																BGL_U8VSET(BgL_counterblockz00_1655,
																	BgL_arg1459z00_1692, BgL_tmpz00_6588);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 334 */
															long BgL_vz00_1695;

															BgL_vz00_1695 =
																(
																(long) (
																	((unsigned long) (BgL_bz00_1667) >>
																		(int) ((3L * 8L)))) & 255L);
															{	/* Unsafe/aes.scm 335 */
																long BgL_arg1461z00_1696;

																BgL_arg1461z00_1696 = (15L - 3L);
																{	/* Unsafe/aes.scm 335 */
																	uint8_t BgL_tmpz00_6598;

																	BgL_tmpz00_6598 = (uint8_t) (BgL_vz00_1695);
																	BGL_U8VSET(BgL_counterblockz00_1655,
																		BgL_arg1461z00_1696, BgL_tmpz00_6598);
																} BUNSPEC;
														}}
														{	/* Unsafe/aes.scm 338 */
															long BgL_arg1464z00_1700;

															BgL_arg1464z00_1700 = (15L - (3L + 4L));
															{	/* Unsafe/aes.scm 338 */
																uint8_t BgL_tmpz00_6603;

																BgL_tmpz00_6603 = (uint8_t) (0L);
																BGL_U8VSET(BgL_counterblockz00_1655,
																	BgL_arg1464z00_1700, BgL_tmpz00_6603);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 339 */
															obj_t BgL_ciphercntrz00_1702;

															BgL_ciphercntrz00_1702 =
																BGl_aeszd2cipherzd2zz__aesz00
																(BgL_counterblockz00_1655,
																BgL_keyschedulez00_1652, BgL_statez00_1650);
															{	/* Unsafe/aes.scm 339 */
																int BgL_blocklengthz00_1703;

																if (
																	(BgL_bz00_1667 <
																		((long) CINT(BgL_blockcountz00_1654) - 1L)))
																	{	/* Unsafe/aes.scm 340 */
																		BgL_blocklengthz00_1703 = (int) (16L);
																	}
																else
																	{	/* Unsafe/aes.scm 342 */
																		obj_t BgL_arg1479z00_1722;

																		{	/* Unsafe/aes.scm 342 */
																			long BgL_arg1480z00_1723;
																			int BgL_arg1481z00_1724;

																			BgL_arg1480z00_1723 =
																				((long) (BgL_lenz00_1653) - 1L);
																			BgL_arg1481z00_1724 = (int) (16L);
																			BgL_arg1479z00_1722 =
																				BGl_remainderz00zz__r4_numbers_6_5_fixnumz00
																				(BINT(BgL_arg1480z00_1723),
																				BINT(BgL_arg1481z00_1724));
																		}
																		BgL_blocklengthz00_1703 =
																			(int) (
																			((long) CINT(BgL_arg1479z00_1722) + 1L));
																	}
																{	/* Unsafe/aes.scm 340 */
																	long BgL_startz00_1704;

																	BgL_startz00_1704 =
																		(BgL_bz00_1667 * (long) ((int) (16L)));
																	{	/* Unsafe/aes.scm 343 */
																		long BgL_endz00_1705;

																		BgL_endz00_1705 =
																			(BgL_startz00_1704 +
																			(long) (BgL_blocklengthz00_1703));
																		{	/* Unsafe/aes.scm 344 */

																			{
																				long BgL_iz00_1707;

																				BgL_iz00_1707 = BgL_startz00_1704;
																			BgL_zc3z04anonymousza31466ze3z87_1708:
																				if ((BgL_iz00_1707 < BgL_endz00_1705))
																					{	/* Unsafe/aes.scm 345 */
																						{	/* Unsafe/aes.scm 346 */
																							int BgL_cipherz00_1710;

																							BgL_cipherz00_1710 =
																								(int) (
																								(STRING_REF(
																										((obj_t)
																											BgL_ciphertextz00_60),
																										(long) ((int) (
																												(BgL_iz00_1707 +
																													(long) ((int)
																														(8L))))))));
																							{	/* Unsafe/aes.scm 346 */
																								long BgL_cntrbytez00_1711;

																								{	/* Unsafe/aes.scm 347 */
																									uint8_t BgL_arg1468z00_1713;

																									{	/* Unsafe/aes.scm 347 */
																										long BgL_tmpz00_6637;

																										BgL_tmpz00_6637 =
																											(BgL_iz00_1707 -
																											BgL_startz00_1704);
																										BgL_arg1468z00_1713 =
																											BGL_U8VREF
																											(BgL_ciphercntrz00_1702,
																											BgL_tmpz00_6637);
																									}
																									{	/* Unsafe/aes.scm 347 */
																										long BgL_arg2398z00_3505;

																										BgL_arg2398z00_3505 =
																											(long)
																											(BgL_arg1468z00_1713);
																										BgL_cntrbytez00_1711 =
																											(long)
																											(BgL_arg2398z00_3505);
																								}}
																								{	/* Unsafe/aes.scm 347 */
																									long BgL_plainbytez00_1712;

																									BgL_plainbytez00_1712 =
																										(
																										(long) (BgL_cipherz00_1710)
																										^ BgL_cntrbytez00_1711);
																									{	/* Unsafe/aes.scm 348 */

																										{	/* Unsafe/aes.scm 349 */
																											int BgL_iz00_3510;
																											int BgL_vz00_3511;

																											BgL_iz00_3510 =
																												(int) (BgL_iz00_1707);
																											BgL_vz00_3511 =
																												(int)
																												(BgL_plainbytez00_1712);
																											{	/* Unsafe/aes.scm 438 */
																												unsigned char
																													BgL_auxz00_6648;
																												long BgL_tmpz00_6646;

																												BgL_auxz00_6648 =
																													(
																													(long)
																													(BgL_vz00_3511));
																												BgL_tmpz00_6646 =
																													(long)
																													(BgL_iz00_3510);
																												STRING_SET
																													(BgL_plaintextz00_1656,
																													BgL_tmpz00_6646,
																													BgL_auxz00_6648);
																						}}}}}}
																						{
																							long BgL_iz00_6652;

																							BgL_iz00_6652 =
																								(BgL_iz00_1707 + 1L);
																							BgL_iz00_1707 = BgL_iz00_6652;
																							goto
																								BgL_zc3z04anonymousza31466ze3z87_1708;
																						}
																					}
																				else
																					{	/* Unsafe/aes.scm 345 */
																						((bool_t) 0);
																					}
																			}
																		}
																	}
																}
															}
														}
														{
															long BgL_bz00_6654;

															BgL_bz00_6654 = (BgL_bz00_1667 + 1L);
															BgL_bz00_1667 = BgL_bz00_6654;
															goto BgL_zc3z04anonymousza31444ze3z87_1668;
														}
													}
												else
													{	/* Unsafe/aes.scm 332 */
														((bool_t) 0);
													}
											}
											return BgL_plaintextz00_1656;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &u8string-ref */
	int BGl_z62u8stringzd2refzb0zz__aesz00(obj_t BgL_envz00_5470,
		obj_t BgL_sz00_5471, long BgL_iz00_5472)
	{
		{	/* Unsafe/aes.scm 431 */
			{	/* Unsafe/aes.scm 432 */
				obj_t BgL_sz00_5590;
				int BgL_iz00_5591;

				if (STRINGP(BgL_sz00_5471))
					{	/* Unsafe/aes.scm 432 */
						BgL_sz00_5590 = BgL_sz00_5471;
					}
				else
					{
						obj_t BgL_auxz00_6658;

						BgL_auxz00_6658 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2684z00zz__aesz00,
							BINT(19447L), BGl_string2702z00zz__aesz00,
							BGl_string2686z00zz__aesz00, BgL_sz00_5471);
						FAILURE(BgL_auxz00_6658, BFALSE, BFALSE);
					}
				BgL_iz00_5591 = (int) (BgL_iz00_5472);
				return (int) ((STRING_REF(BgL_sz00_5590, (long) (BgL_iz00_5591))));
		}}

	}



/* aes-password->key */
	obj_t BGl_aeszd2passwordzd2ze3keyze3zz__aesz00(obj_t BgL_passwordz00_79,
		int BgL_nbitsz00_80, obj_t BgL_statez00_81)
	{
		{	/* Unsafe/aes.scm 443 */
			{	/* Unsafe/aes.scm 444 */
				long BgL_nbytesz00_1750;

				BgL_nbytesz00_1750 = ((long) (BgL_nbitsz00_80) / 8L);
				{	/* Unsafe/aes.scm 444 */
					obj_t BgL_uvecz00_1751;

					BgL_uvecz00_1751 =
						BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_nbytesz00_1750,
						(uint8_t) (0L));
					{	/* Unsafe/aes.scm 445 */
						obj_t BgL_shapassz00_1752;

						if ((STRING_LENGTH(BgL_passwordz00_79) < BgL_nbytesz00_1750))
							{	/* Unsafe/aes.scm 446 */
								BgL_shapassz00_1752 =
									string_append(BgL_passwordz00_79,
									BGl_sha1sumzd2stringzd2zz__sha1z00(BgL_passwordz00_79));
							}
						else
							{	/* Unsafe/aes.scm 446 */
								BgL_shapassz00_1752 = BgL_passwordz00_79;
							}
						{	/* Unsafe/aes.scm 449 */

							{
								long BgL_iz00_3572;

								BgL_iz00_3572 = 0L;
							BgL_loopzd2izd21076z00_3571:
								if ((BgL_iz00_3572 < BgL_nbytesz00_1750))
									{	/* Unsafe/aes.scm 450 */
										{	/* Unsafe/aes.scm 451 */
											int BgL_arg1503z00_3576;

											BgL_arg1503z00_3576 =
												(int) (
												(STRING_REF(BgL_shapassz00_1752,
														(long) ((int) (BgL_iz00_3572)))));
											{	/* Unsafe/aes.scm 451 */
												uint8_t BgL_tmpz00_6683;

												BgL_tmpz00_6683 = (uint8_t) (BgL_arg1503z00_3576);
												BGL_U8VSET(BgL_uvecz00_1751, BgL_iz00_3572,
													BgL_tmpz00_6683);
											} BUNSPEC;
										}
										{
											long BgL_iz00_6686;

											BgL_iz00_6686 = (BgL_iz00_3572 + 1L);
											BgL_iz00_3572 = BgL_iz00_6686;
											goto BgL_loopzd2izd21076z00_3571;
										}
									}
								else
									{	/* Unsafe/aes.scm 450 */
										((bool_t) 0);
									}
							}
							return
								BGl_aeszd2cipherzd2zz__aesz00(BgL_uvecz00_1751,
								BGl_aeszd2keyzd2expansionz00zz__aesz00(BgL_uvecz00_1751),
								BgL_statez00_81);
						}
					}
				}
			}
		}

	}



/* aes-key-expansion */
	obj_t BGl_aeszd2keyzd2expansionz00zz__aesz00(obj_t BgL_keyz00_82)
	{
		{	/* Unsafe/aes.scm 459 */
			{	/* Unsafe/aes.scm 461 */
				long BgL_nkz00_1767;

				{	/* Unsafe/aes.scm 462 */
					long BgL_arg1619z00_1881;

					BgL_arg1619z00_1881 = BGL_HVECTOR_LENGTH(BgL_keyz00_82);
					BgL_nkz00_1767 = (BgL_arg1619z00_1881 / 4L);
				}
				{	/* Unsafe/aes.scm 462 */
					long BgL_nrz00_1768;

					BgL_nrz00_1768 = (BgL_nkz00_1767 + 6L);
					{	/* Unsafe/aes.scm 463 */
						obj_t BgL_wz00_1769;

						BgL_wz00_1769 = make_vector((4L * (BgL_nrz00_1768 + 1L)), BUNSPEC);
						{	/* Unsafe/aes.scm 464 */
							obj_t BgL_tempz00_1770;

							{	/* Llib/srfi4.scm 447 */

								BgL_tempz00_1770 =
									BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
							}
							{	/* Unsafe/aes.scm 465 */

								{
									long BgL_iz00_1772;

									BgL_iz00_1772 = 0L;
								BgL_zc3z04anonymousza31510ze3z87_1773:
									if ((BgL_iz00_1772 < BgL_nkz00_1767))
										{	/* Unsafe/aes.scm 467 */
											{	/* Unsafe/aes.scm 468 */
												long BgL_jz00_1775;

												BgL_jz00_1775 = (4L * BgL_iz00_1772);
												{	/* Unsafe/aes.scm 468 */
													obj_t BgL_rz00_1776;

													{	/* Unsafe/aes.scm 469 */
														long BgL_arg1513z00_1777;
														long BgL_arg1514z00_1778;
														long BgL_arg1516z00_1779;
														long BgL_arg1517z00_1780;

														{	/* Unsafe/aes.scm 469 */
															uint8_t BgL_arg1524z00_1785;

															BgL_arg1524z00_1785 =
																BGL_U8VREF(BgL_keyz00_82, BgL_jz00_1775);
															{	/* Unsafe/aes.scm 469 */
																long BgL_arg2398z00_3599;

																BgL_arg2398z00_3599 =
																	(long) (BgL_arg1524z00_1785);
																BgL_arg1513z00_1777 =
																	(long) (BgL_arg2398z00_3599);
														}}
														{	/* Unsafe/aes.scm 470 */
															uint8_t BgL_arg1525z00_1786;

															{	/* Unsafe/aes.scm 470 */
																long BgL_tmpz00_6703;

																BgL_tmpz00_6703 = (BgL_jz00_1775 + 1L);
																BgL_arg1525z00_1786 =
																	BGL_U8VREF(BgL_keyz00_82, BgL_tmpz00_6703);
															}
															{	/* Unsafe/aes.scm 470 */
																long BgL_arg2398z00_3605;

																BgL_arg2398z00_3605 =
																	(long) (BgL_arg1525z00_1786);
																BgL_arg1514z00_1778 =
																	(long) (BgL_arg2398z00_3605);
														}}
														{	/* Unsafe/aes.scm 471 */
															uint8_t BgL_arg1527z00_1788;

															{	/* Unsafe/aes.scm 471 */
																long BgL_tmpz00_6708;

																BgL_tmpz00_6708 = (BgL_jz00_1775 + 2L);
																BgL_arg1527z00_1788 =
																	BGL_U8VREF(BgL_keyz00_82, BgL_tmpz00_6708);
															}
															{	/* Unsafe/aes.scm 471 */
																long BgL_arg2398z00_3611;

																BgL_arg2398z00_3611 =
																	(long) (BgL_arg1527z00_1788);
																BgL_arg1516z00_1779 =
																	(long) (BgL_arg2398z00_3611);
														}}
														{	/* Unsafe/aes.scm 472 */
															uint8_t BgL_arg1529z00_1790;

															{	/* Unsafe/aes.scm 472 */
																long BgL_tmpz00_6713;

																BgL_tmpz00_6713 = (BgL_jz00_1775 + 3L);
																BgL_arg1529z00_1790 =
																	BGL_U8VREF(BgL_keyz00_82, BgL_tmpz00_6713);
															}
															{	/* Unsafe/aes.scm 472 */
																long BgL_arg2398z00_3617;

																BgL_arg2398z00_3617 =
																	(long) (BgL_arg1529z00_1790);
																BgL_arg1517z00_1780 =
																	(long) (BgL_arg2398z00_3617);
														}}
														{	/* Unsafe/aes.scm 469 */
															obj_t BgL_list1518z00_1781;

															{	/* Unsafe/aes.scm 469 */
																obj_t BgL_arg1521z00_1782;

																{	/* Unsafe/aes.scm 469 */
																	obj_t BgL_arg1522z00_1783;

																	{	/* Unsafe/aes.scm 469 */
																		obj_t BgL_arg1523z00_1784;

																		BgL_arg1523z00_1784 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1517z00_1780),
																			BNIL);
																		BgL_arg1522z00_1783 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1516z00_1779),
																			BgL_arg1523z00_1784);
																	}
																	BgL_arg1521z00_1782 =
																		MAKE_YOUNG_PAIR(BINT(BgL_arg1514z00_1778),
																		BgL_arg1522z00_1783);
																}
																BgL_list1518z00_1781 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg1513z00_1777),
																	BgL_arg1521z00_1782);
															}
															BgL_rz00_1776 =
																BGl_u8vectorz00zz__srfi4z00
																(BgL_list1518z00_1781);
													}}
													{	/* Unsafe/aes.scm 469 */

														VECTOR_SET(BgL_wz00_1769, BgL_iz00_1772,
															BgL_rz00_1776);
											}}}
											{
												long BgL_iz00_6728;

												BgL_iz00_6728 = (BgL_iz00_1772 + 1L);
												BgL_iz00_1772 = BgL_iz00_6728;
												goto BgL_zc3z04anonymousza31510ze3z87_1773;
											}
										}
									else
										{	/* Unsafe/aes.scm 467 */
											((bool_t) 0);
										}
								}
								{
									long BgL_iz00_1795;

									BgL_iz00_1795 = BgL_nkz00_1767;
								BgL_zc3z04anonymousza31532ze3z87_1796:
									if ((BgL_iz00_1795 < (4L * (BgL_nrz00_1768 + 1L))))
										{	/* Unsafe/aes.scm 475 */
											{	/* Unsafe/aes.scm 476 */
												obj_t BgL_arg1539z00_1800;

												{	/* Llib/srfi4.scm 447 */

													BgL_arg1539z00_1800 =
														BGl_makezd2u8vectorzd2zz__srfi4z00(4L,
														(uint8_t) (0));
												}
												VECTOR_SET(BgL_wz00_1769, BgL_iz00_1795,
													BgL_arg1539z00_1800);
											}
											{	/* Unsafe/aes.scm 478 */
												unsigned long BgL_arg1540z00_1804;

												{	/* Unsafe/aes.scm 478 */
													long BgL_arg1543z00_1805;

													BgL_arg1543z00_1805 = (BgL_iz00_1795 - 1L);
													{	/* Unsafe/aes.scm 478 */
														unsigned long BgL_res2522z00_3642;

														{	/* Unsafe/aes.scm 478 */
															int BgL_iz00_3631;
															int BgL_jz00_3632;

															BgL_iz00_3631 = (int) (BgL_arg1543z00_1805);
															BgL_jz00_3632 = (int) (0L);
															{	/* Unsafe/aes.scm 420 */
																uint8_t BgL_arg1495z00_3633;

																{	/* Unsafe/aes.scm 420 */
																	obj_t BgL_arg1497z00_3634;

																	BgL_arg1497z00_3634 =
																		VECTOR_REF(BgL_wz00_1769,
																		(long) (BgL_iz00_3631));
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_kz00_3638;

																		BgL_kz00_3638 = (long) (BgL_jz00_3632);
																		{	/* Unsafe/aes.scm 420 */
																			obj_t BgL_tmpz00_6742;

																			BgL_tmpz00_6742 =
																				((obj_t) BgL_arg1497z00_3634);
																			BgL_arg1495z00_3633 =
																				BGL_U8VREF(BgL_tmpz00_6742,
																				BgL_kz00_3638);
																}}}
																{	/* Unsafe/aes.scm 420 */
																	long BgL_arg2398z00_3640;

																	BgL_arg2398z00_3640 =
																		(long) (BgL_arg1495z00_3633);
																	BgL_res2522z00_3642 =
																		(unsigned long) (
																		(long) (BgL_arg2398z00_3640));
														}}}
														BgL_arg1540z00_1804 = BgL_res2522z00_3642;
												}}
												{	/* Unsafe/aes.scm 478 */
													uint8_t BgL_tmpz00_6748;

													BgL_tmpz00_6748 = (uint8_t) (BgL_arg1540z00_1804);
													BGL_U8VSET(BgL_tempz00_1770, 0L, BgL_tmpz00_6748);
												} BUNSPEC;
											}
											{	/* Unsafe/aes.scm 478 */
												unsigned long BgL_arg1544z00_1807;

												{	/* Unsafe/aes.scm 478 */
													long BgL_arg1546z00_1808;

													BgL_arg1546z00_1808 = (BgL_iz00_1795 - 1L);
													{	/* Unsafe/aes.scm 478 */
														unsigned long BgL_res2523z00_3656;

														{	/* Unsafe/aes.scm 478 */
															int BgL_iz00_3645;
															int BgL_jz00_3646;

															BgL_iz00_3645 = (int) (BgL_arg1546z00_1808);
															BgL_jz00_3646 = (int) (1L);
															{	/* Unsafe/aes.scm 420 */
																uint8_t BgL_arg1495z00_3647;

																{	/* Unsafe/aes.scm 420 */
																	obj_t BgL_arg1497z00_3648;

																	BgL_arg1497z00_3648 =
																		VECTOR_REF(BgL_wz00_1769,
																		(long) (BgL_iz00_3645));
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_kz00_3652;

																		BgL_kz00_3652 = (long) (BgL_jz00_3646);
																		{	/* Unsafe/aes.scm 420 */
																			obj_t BgL_tmpz00_6757;

																			BgL_tmpz00_6757 =
																				((obj_t) BgL_arg1497z00_3648);
																			BgL_arg1495z00_3647 =
																				BGL_U8VREF(BgL_tmpz00_6757,
																				BgL_kz00_3652);
																}}}
																{	/* Unsafe/aes.scm 420 */
																	long BgL_arg2398z00_3654;

																	BgL_arg2398z00_3654 =
																		(long) (BgL_arg1495z00_3647);
																	BgL_res2523z00_3656 =
																		(unsigned long) (
																		(long) (BgL_arg2398z00_3654));
														}}}
														BgL_arg1544z00_1807 = BgL_res2523z00_3656;
												}}
												{	/* Unsafe/aes.scm 478 */
													uint8_t BgL_tmpz00_6763;

													BgL_tmpz00_6763 = (uint8_t) (BgL_arg1544z00_1807);
													BGL_U8VSET(BgL_tempz00_1770, 1L, BgL_tmpz00_6763);
												} BUNSPEC;
											}
											{	/* Unsafe/aes.scm 478 */
												unsigned long BgL_arg1547z00_1810;

												{	/* Unsafe/aes.scm 478 */
													long BgL_arg1549z00_1811;

													BgL_arg1549z00_1811 = (BgL_iz00_1795 - 1L);
													{	/* Unsafe/aes.scm 478 */
														unsigned long BgL_res2524z00_3670;

														{	/* Unsafe/aes.scm 478 */
															int BgL_iz00_3659;
															int BgL_jz00_3660;

															BgL_iz00_3659 = (int) (BgL_arg1549z00_1811);
															BgL_jz00_3660 = (int) (2L);
															{	/* Unsafe/aes.scm 420 */
																uint8_t BgL_arg1495z00_3661;

																{	/* Unsafe/aes.scm 420 */
																	obj_t BgL_arg1497z00_3662;

																	BgL_arg1497z00_3662 =
																		VECTOR_REF(BgL_wz00_1769,
																		(long) (BgL_iz00_3659));
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_kz00_3666;

																		BgL_kz00_3666 = (long) (BgL_jz00_3660);
																		{	/* Unsafe/aes.scm 420 */
																			obj_t BgL_tmpz00_6772;

																			BgL_tmpz00_6772 =
																				((obj_t) BgL_arg1497z00_3662);
																			BgL_arg1495z00_3661 =
																				BGL_U8VREF(BgL_tmpz00_6772,
																				BgL_kz00_3666);
																}}}
																{	/* Unsafe/aes.scm 420 */
																	long BgL_arg2398z00_3668;

																	BgL_arg2398z00_3668 =
																		(long) (BgL_arg1495z00_3661);
																	BgL_res2524z00_3670 =
																		(unsigned long) (
																		(long) (BgL_arg2398z00_3668));
														}}}
														BgL_arg1547z00_1810 = BgL_res2524z00_3670;
												}}
												{	/* Unsafe/aes.scm 478 */
													uint8_t BgL_tmpz00_6778;

													BgL_tmpz00_6778 = (uint8_t) (BgL_arg1547z00_1810);
													BGL_U8VSET(BgL_tempz00_1770, 2L, BgL_tmpz00_6778);
												} BUNSPEC;
											}
											{	/* Unsafe/aes.scm 478 */
												unsigned long BgL_arg1552z00_1813;

												{	/* Unsafe/aes.scm 478 */
													long BgL_arg1553z00_1814;

													BgL_arg1553z00_1814 = (BgL_iz00_1795 - 1L);
													{	/* Unsafe/aes.scm 478 */
														unsigned long BgL_res2525z00_3684;

														{	/* Unsafe/aes.scm 478 */
															int BgL_iz00_3673;
															int BgL_jz00_3674;

															BgL_iz00_3673 = (int) (BgL_arg1553z00_1814);
															BgL_jz00_3674 = (int) (3L);
															{	/* Unsafe/aes.scm 420 */
																uint8_t BgL_arg1495z00_3675;

																{	/* Unsafe/aes.scm 420 */
																	obj_t BgL_arg1497z00_3676;

																	BgL_arg1497z00_3676 =
																		VECTOR_REF(BgL_wz00_1769,
																		(long) (BgL_iz00_3673));
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_kz00_3680;

																		BgL_kz00_3680 = (long) (BgL_jz00_3674);
																		{	/* Unsafe/aes.scm 420 */
																			obj_t BgL_tmpz00_6787;

																			BgL_tmpz00_6787 =
																				((obj_t) BgL_arg1497z00_3676);
																			BgL_arg1495z00_3675 =
																				BGL_U8VREF(BgL_tmpz00_6787,
																				BgL_kz00_3680);
																}}}
																{	/* Unsafe/aes.scm 420 */
																	long BgL_arg2398z00_3682;

																	BgL_arg2398z00_3682 =
																		(long) (BgL_arg1495z00_3675);
																	BgL_res2525z00_3684 =
																		(unsigned long) (
																		(long) (BgL_arg2398z00_3682));
														}}}
														BgL_arg1552z00_1813 = BgL_res2525z00_3684;
												}}
												{	/* Unsafe/aes.scm 478 */
													uint8_t BgL_tmpz00_6793;

													BgL_tmpz00_6793 = (uint8_t) (BgL_arg1552z00_1813);
													BGL_U8VSET(BgL_tempz00_1770, 3L, BgL_tmpz00_6793);
												} BUNSPEC;
											}
											{	/* Unsafe/aes.scm 479 */
												bool_t BgL_test2842z00_6796;

												{	/* Unsafe/aes.scm 479 */
													obj_t BgL_arg1587z00_1847;

													BgL_arg1587z00_1847 =
														BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
														(BgL_iz00_1795), BINT(BgL_nkz00_1767));
													BgL_test2842z00_6796 =
														((long) CINT(BgL_arg1587z00_1847) == 0L);
												}
												if (BgL_test2842z00_6796)
													{	/* Unsafe/aes.scm 479 */
														BGl_subwordz12z12zz__aesz00
															(BGl_rotwordz12z12zz__aesz00(BgL_tempz00_1770));
														{	/* Unsafe/aes.scm 483 */
															long BgL_vz00_1819;

															{	/* Unsafe/aes.scm 483 */
																long BgL_arg1557z00_1820;
																unsigned long BgL_arg1558z00_1821;

																{	/* Unsafe/aes.scm 483 */
																	uint8_t BgL_arg1559z00_1822;

																	BgL_arg1559z00_1822 =
																		BGL_U8VREF(BgL_tempz00_1770, 0L);
																	{	/* Unsafe/aes.scm 483 */
																		long BgL_arg2398z00_3690;

																		BgL_arg2398z00_3690 =
																			(long) (BgL_arg1559z00_1822);
																		BgL_arg1557z00_1820 =
																			(long) (BgL_arg2398z00_3690);
																}}
																{	/* Unsafe/aes.scm 484 */
																	long BgL_arg1561z00_1823;

																	BgL_arg1561z00_1823 =
																		(BgL_iz00_1795 / BgL_nkz00_1767);
																	{	/* Unsafe/aes.scm 484 */
																		unsigned long BgL_res2526z00_3706;

																		{	/* Unsafe/aes.scm 484 */
																			obj_t BgL_mz00_3694;
																			int BgL_iz00_3695;
																			int BgL_jz00_3696;

																			BgL_mz00_3694 = CNST_TABLE_REF(0);
																			BgL_iz00_3695 =
																				(int) (BgL_arg1561z00_1823);
																			BgL_jz00_3696 = (int) (0L);
																			{	/* Unsafe/aes.scm 420 */
																				uint8_t BgL_arg1495z00_3697;

																				{	/* Unsafe/aes.scm 420 */
																					obj_t BgL_arg1497z00_3698;

																					BgL_arg1497z00_3698 =
																						VECTOR_REF(BgL_mz00_3694,
																						(long) (BgL_iz00_3695));
																					{	/* Unsafe/aes.scm 420 */
																						long BgL_kz00_3702;

																						BgL_kz00_3702 =
																							(long) (BgL_jz00_3696);
																						{	/* Unsafe/aes.scm 420 */
																							obj_t BgL_tmpz00_6814;

																							BgL_tmpz00_6814 =
																								((obj_t) BgL_arg1497z00_3698);
																							BgL_arg1495z00_3697 =
																								BGL_U8VREF(BgL_tmpz00_6814,
																								BgL_kz00_3702);
																				}}}
																				{	/* Unsafe/aes.scm 420 */
																					long BgL_arg2398z00_3704;

																					BgL_arg2398z00_3704 =
																						(long) (BgL_arg1495z00_3697);
																					BgL_res2526z00_3706 =
																						(unsigned long) (
																						(long) (BgL_arg2398z00_3704));
																		}}}
																		BgL_arg1558z00_1821 = BgL_res2526z00_3706;
																}}
																BgL_vz00_1819 =
																	(BgL_arg1557z00_1820 ^
																	(long) (BgL_arg1558z00_1821));
															}
															{	/* Unsafe/aes.scm 485 */
																uint8_t BgL_tmpz00_6822;

																BgL_tmpz00_6822 = (uint8_t) (BgL_vz00_1819);
																BGL_U8VSET(BgL_tempz00_1770, 0L,
																	BgL_tmpz00_6822);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 483 */
															long BgL_vz00_1825;

															{	/* Unsafe/aes.scm 483 */
																long BgL_arg1562z00_1826;
																unsigned long BgL_arg1564z00_1827;

																{	/* Unsafe/aes.scm 483 */
																	uint8_t BgL_arg1565z00_1828;

																	BgL_arg1565z00_1828 =
																		BGL_U8VREF(BgL_tempz00_1770, 1L);
																	{	/* Unsafe/aes.scm 483 */
																		long BgL_arg2398z00_3712;

																		BgL_arg2398z00_3712 =
																			(long) (BgL_arg1565z00_1828);
																		BgL_arg1562z00_1826 =
																			(long) (BgL_arg2398z00_3712);
																}}
																{	/* Unsafe/aes.scm 484 */
																	long BgL_arg1567z00_1829;

																	BgL_arg1567z00_1829 =
																		(BgL_iz00_1795 / BgL_nkz00_1767);
																	{	/* Unsafe/aes.scm 484 */
																		unsigned long BgL_res2527z00_3728;

																		{	/* Unsafe/aes.scm 484 */
																			obj_t BgL_mz00_3716;
																			int BgL_iz00_3717;
																			int BgL_jz00_3718;

																			BgL_mz00_3716 = CNST_TABLE_REF(0);
																			BgL_iz00_3717 =
																				(int) (BgL_arg1567z00_1829);
																			BgL_jz00_3718 = (int) (1L);
																			{	/* Unsafe/aes.scm 420 */
																				uint8_t BgL_arg1495z00_3719;

																				{	/* Unsafe/aes.scm 420 */
																					obj_t BgL_arg1497z00_3720;

																					BgL_arg1497z00_3720 =
																						VECTOR_REF(BgL_mz00_3716,
																						(long) (BgL_iz00_3717));
																					{	/* Unsafe/aes.scm 420 */
																						long BgL_kz00_3724;

																						BgL_kz00_3724 =
																							(long) (BgL_jz00_3718);
																						{	/* Unsafe/aes.scm 420 */
																							obj_t BgL_tmpz00_6835;

																							BgL_tmpz00_6835 =
																								((obj_t) BgL_arg1497z00_3720);
																							BgL_arg1495z00_3719 =
																								BGL_U8VREF(BgL_tmpz00_6835,
																								BgL_kz00_3724);
																				}}}
																				{	/* Unsafe/aes.scm 420 */
																					long BgL_arg2398z00_3726;

																					BgL_arg2398z00_3726 =
																						(long) (BgL_arg1495z00_3719);
																					BgL_res2527z00_3728 =
																						(unsigned long) (
																						(long) (BgL_arg2398z00_3726));
																		}}}
																		BgL_arg1564z00_1827 = BgL_res2527z00_3728;
																}}
																BgL_vz00_1825 =
																	(BgL_arg1562z00_1826 ^
																	(long) (BgL_arg1564z00_1827));
															}
															{	/* Unsafe/aes.scm 485 */
																uint8_t BgL_tmpz00_6843;

																BgL_tmpz00_6843 = (uint8_t) (BgL_vz00_1825);
																BGL_U8VSET(BgL_tempz00_1770, 1L,
																	BgL_tmpz00_6843);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 483 */
															long BgL_vz00_1831;

															{	/* Unsafe/aes.scm 483 */
																long BgL_arg1571z00_1832;
																unsigned long BgL_arg1573z00_1833;

																{	/* Unsafe/aes.scm 483 */
																	uint8_t BgL_arg1575z00_1834;

																	BgL_arg1575z00_1834 =
																		BGL_U8VREF(BgL_tempz00_1770, 2L);
																	{	/* Unsafe/aes.scm 483 */
																		long BgL_arg2398z00_3734;

																		BgL_arg2398z00_3734 =
																			(long) (BgL_arg1575z00_1834);
																		BgL_arg1571z00_1832 =
																			(long) (BgL_arg2398z00_3734);
																}}
																{	/* Unsafe/aes.scm 484 */
																	long BgL_arg1576z00_1835;

																	BgL_arg1576z00_1835 =
																		(BgL_iz00_1795 / BgL_nkz00_1767);
																	{	/* Unsafe/aes.scm 484 */
																		unsigned long BgL_res2528z00_3750;

																		{	/* Unsafe/aes.scm 484 */
																			obj_t BgL_mz00_3738;
																			int BgL_iz00_3739;
																			int BgL_jz00_3740;

																			BgL_mz00_3738 = CNST_TABLE_REF(0);
																			BgL_iz00_3739 =
																				(int) (BgL_arg1576z00_1835);
																			BgL_jz00_3740 = (int) (2L);
																			{	/* Unsafe/aes.scm 420 */
																				uint8_t BgL_arg1495z00_3741;

																				{	/* Unsafe/aes.scm 420 */
																					obj_t BgL_arg1497z00_3742;

																					BgL_arg1497z00_3742 =
																						VECTOR_REF(BgL_mz00_3738,
																						(long) (BgL_iz00_3739));
																					{	/* Unsafe/aes.scm 420 */
																						long BgL_kz00_3746;

																						BgL_kz00_3746 =
																							(long) (BgL_jz00_3740);
																						{	/* Unsafe/aes.scm 420 */
																							obj_t BgL_tmpz00_6856;

																							BgL_tmpz00_6856 =
																								((obj_t) BgL_arg1497z00_3742);
																							BgL_arg1495z00_3741 =
																								BGL_U8VREF(BgL_tmpz00_6856,
																								BgL_kz00_3746);
																				}}}
																				{	/* Unsafe/aes.scm 420 */
																					long BgL_arg2398z00_3748;

																					BgL_arg2398z00_3748 =
																						(long) (BgL_arg1495z00_3741);
																					BgL_res2528z00_3750 =
																						(unsigned long) (
																						(long) (BgL_arg2398z00_3748));
																		}}}
																		BgL_arg1573z00_1833 = BgL_res2528z00_3750;
																}}
																BgL_vz00_1831 =
																	(BgL_arg1571z00_1832 ^
																	(long) (BgL_arg1573z00_1833));
															}
															{	/* Unsafe/aes.scm 485 */
																uint8_t BgL_tmpz00_6864;

																BgL_tmpz00_6864 = (uint8_t) (BgL_vz00_1831);
																BGL_U8VSET(BgL_tempz00_1770, 2L,
																	BgL_tmpz00_6864);
															} BUNSPEC;
														}
														{	/* Unsafe/aes.scm 483 */
															long BgL_vz00_1837;

															{	/* Unsafe/aes.scm 483 */
																long BgL_arg1578z00_1838;
																unsigned long BgL_arg1579z00_1839;

																{	/* Unsafe/aes.scm 483 */
																	uint8_t BgL_arg1580z00_1840;

																	BgL_arg1580z00_1840 =
																		BGL_U8VREF(BgL_tempz00_1770, 3L);
																	{	/* Unsafe/aes.scm 483 */
																		long BgL_arg2398z00_3756;

																		BgL_arg2398z00_3756 =
																			(long) (BgL_arg1580z00_1840);
																		BgL_arg1578z00_1838 =
																			(long) (BgL_arg2398z00_3756);
																}}
																{	/* Unsafe/aes.scm 484 */
																	long BgL_arg1582z00_1841;

																	BgL_arg1582z00_1841 =
																		(BgL_iz00_1795 / BgL_nkz00_1767);
																	{	/* Unsafe/aes.scm 484 */
																		unsigned long BgL_res2529z00_3772;

																		{	/* Unsafe/aes.scm 484 */
																			obj_t BgL_mz00_3760;
																			int BgL_iz00_3761;
																			int BgL_jz00_3762;

																			BgL_mz00_3760 = CNST_TABLE_REF(0);
																			BgL_iz00_3761 =
																				(int) (BgL_arg1582z00_1841);
																			BgL_jz00_3762 = (int) (3L);
																			{	/* Unsafe/aes.scm 420 */
																				uint8_t BgL_arg1495z00_3763;

																				{	/* Unsafe/aes.scm 420 */
																					obj_t BgL_arg1497z00_3764;

																					BgL_arg1497z00_3764 =
																						VECTOR_REF(BgL_mz00_3760,
																						(long) (BgL_iz00_3761));
																					{	/* Unsafe/aes.scm 420 */
																						long BgL_kz00_3768;

																						BgL_kz00_3768 =
																							(long) (BgL_jz00_3762);
																						{	/* Unsafe/aes.scm 420 */
																							obj_t BgL_tmpz00_6877;

																							BgL_tmpz00_6877 =
																								((obj_t) BgL_arg1497z00_3764);
																							BgL_arg1495z00_3763 =
																								BGL_U8VREF(BgL_tmpz00_6877,
																								BgL_kz00_3768);
																				}}}
																				{	/* Unsafe/aes.scm 420 */
																					long BgL_arg2398z00_3770;

																					BgL_arg2398z00_3770 =
																						(long) (BgL_arg1495z00_3763);
																					BgL_res2529z00_3772 =
																						(unsigned long) (
																						(long) (BgL_arg2398z00_3770));
																		}}}
																		BgL_arg1579z00_1839 = BgL_res2529z00_3772;
																}}
																BgL_vz00_1837 =
																	(BgL_arg1578z00_1838 ^
																	(long) (BgL_arg1579z00_1839));
															}
															{	/* Unsafe/aes.scm 485 */
																uint8_t BgL_tmpz00_6885;

																BgL_tmpz00_6885 = (uint8_t) (BgL_vz00_1837);
																BGL_U8VSET(BgL_tempz00_1770, 3L,
																	BgL_tmpz00_6885);
															} BUNSPEC;
													}}
												else
													{	/* Unsafe/aes.scm 486 */
														bool_t BgL_test2843z00_6888;

														if ((BgL_nkz00_1767 > 6L))
															{	/* Unsafe/aes.scm 486 */
																obj_t BgL_arg1586z00_1846;

																BgL_arg1586z00_1846 =
																	BGl_remainderz00zz__r4_numbers_6_5_fixnumz00
																	(BINT(BgL_iz00_1795), BINT(BgL_nkz00_1767));
																BgL_test2843z00_6888 =
																	((long) CINT(BgL_arg1586z00_1846) == 4L);
															}
														else
															{	/* Unsafe/aes.scm 486 */
																BgL_test2843z00_6888 = ((bool_t) 0);
															}
														if (BgL_test2843z00_6888)
															{	/* Unsafe/aes.scm 486 */
																BGl_subwordz12z12zz__aesz00(BgL_tempz00_1770);
															}
														else
															{	/* Unsafe/aes.scm 486 */
																BFALSE;
															}
													}
											}
											{	/* Unsafe/aes.scm 489 */
												long BgL_vz00_1849;

												{	/* Unsafe/aes.scm 489 */
													unsigned long BgL_arg1589z00_1850;
													long BgL_arg1591z00_1851;

													{	/* Unsafe/aes.scm 489 */
														long BgL_arg1593z00_1852;

														BgL_arg1593z00_1852 =
															(BgL_iz00_1795 - BgL_nkz00_1767);
														{	/* Unsafe/aes.scm 489 */
															unsigned long BgL_res2530z00_3791;

															{	/* Unsafe/aes.scm 489 */
																int BgL_iz00_3780;
																int BgL_jz00_3781;

																BgL_iz00_3780 = (int) (BgL_arg1593z00_1852);
																BgL_jz00_3781 = (int) (0L);
																{	/* Unsafe/aes.scm 420 */
																	uint8_t BgL_arg1495z00_3782;

																	{	/* Unsafe/aes.scm 420 */
																		obj_t BgL_arg1497z00_3783;

																		BgL_arg1497z00_3783 =
																			VECTOR_REF(BgL_wz00_1769,
																			(long) (BgL_iz00_3780));
																		{	/* Unsafe/aes.scm 420 */
																			long BgL_kz00_3787;

																			BgL_kz00_3787 = (long) (BgL_jz00_3781);
																			{	/* Unsafe/aes.scm 420 */
																				obj_t BgL_tmpz00_6903;

																				BgL_tmpz00_6903 =
																					((obj_t) BgL_arg1497z00_3783);
																				BgL_arg1495z00_3782 =
																					BGL_U8VREF(BgL_tmpz00_6903,
																					BgL_kz00_3787);
																	}}}
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_arg2398z00_3789;

																		BgL_arg2398z00_3789 =
																			(long) (BgL_arg1495z00_3782);
																		BgL_res2530z00_3791 =
																			(unsigned long) (
																			(long) (BgL_arg2398z00_3789));
															}}}
															BgL_arg1589z00_1850 = BgL_res2530z00_3791;
													}}
													{	/* Unsafe/aes.scm 490 */
														uint8_t BgL_arg1594z00_1853;

														BgL_arg1594z00_1853 =
															BGL_U8VREF(BgL_tempz00_1770, 0L);
														{	/* Unsafe/aes.scm 490 */
															long BgL_arg2398z00_3795;

															BgL_arg2398z00_3795 =
																(long) (BgL_arg1594z00_1853);
															BgL_arg1591z00_1851 =
																(long) (BgL_arg2398z00_3795);
													}}
													BgL_vz00_1849 =
														(
														(long) (BgL_arg1589z00_1850) ^ BgL_arg1591z00_1851);
												}
												{	/* Unsafe/aes.scm 491 */
													int BgL_iz00_3800;
													int BgL_jz00_3801;
													unsigned long BgL_vz00_3802;

													BgL_iz00_3800 = (int) (BgL_iz00_1795);
													BgL_jz00_3801 = (int) (0L);
													BgL_vz00_3802 = (unsigned long) (BgL_vz00_1849);
													{	/* Unsafe/aes.scm 426 */
														obj_t BgL_arg1498z00_3803;

														BgL_arg1498z00_3803 =
															VECTOR_REF(BgL_wz00_1769, (long) (BgL_iz00_3800));
														{	/* Unsafe/aes.scm 426 */
															uint8_t BgL_auxz00_6921;
															long BgL_tmpz00_6919;

															BgL_auxz00_6921 = (uint8_t) (BgL_vz00_3802);
															BgL_tmpz00_6919 = (long) (BgL_jz00_3801);
															BGL_U8VSET(BgL_arg1498z00_3803, BgL_tmpz00_6919,
																BgL_auxz00_6921);
														} BUNSPEC;
											}}}
											{	/* Unsafe/aes.scm 489 */
												long BgL_vz00_1855;

												{	/* Unsafe/aes.scm 489 */
													unsigned long BgL_arg1595z00_1856;
													long BgL_arg1598z00_1857;

													{	/* Unsafe/aes.scm 489 */
														long BgL_arg1601z00_1858;

														BgL_arg1601z00_1858 =
															(BgL_iz00_1795 - BgL_nkz00_1767);
														{	/* Unsafe/aes.scm 489 */
															unsigned long BgL_res2531z00_3820;

															{	/* Unsafe/aes.scm 489 */
																int BgL_iz00_3809;
																int BgL_jz00_3810;

																BgL_iz00_3809 = (int) (BgL_arg1601z00_1858);
																BgL_jz00_3810 = (int) (1L);
																{	/* Unsafe/aes.scm 420 */
																	uint8_t BgL_arg1495z00_3811;

																	{	/* Unsafe/aes.scm 420 */
																		obj_t BgL_arg1497z00_3812;

																		BgL_arg1497z00_3812 =
																			VECTOR_REF(BgL_wz00_1769,
																			(long) (BgL_iz00_3809));
																		{	/* Unsafe/aes.scm 420 */
																			long BgL_kz00_3816;

																			BgL_kz00_3816 = (long) (BgL_jz00_3810);
																			{	/* Unsafe/aes.scm 420 */
																				obj_t BgL_tmpz00_6930;

																				BgL_tmpz00_6930 =
																					((obj_t) BgL_arg1497z00_3812);
																				BgL_arg1495z00_3811 =
																					BGL_U8VREF(BgL_tmpz00_6930,
																					BgL_kz00_3816);
																	}}}
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_arg2398z00_3818;

																		BgL_arg2398z00_3818 =
																			(long) (BgL_arg1495z00_3811);
																		BgL_res2531z00_3820 =
																			(unsigned long) (
																			(long) (BgL_arg2398z00_3818));
															}}}
															BgL_arg1595z00_1856 = BgL_res2531z00_3820;
													}}
													{	/* Unsafe/aes.scm 490 */
														uint8_t BgL_arg1602z00_1859;

														BgL_arg1602z00_1859 =
															BGL_U8VREF(BgL_tempz00_1770, 1L);
														{	/* Unsafe/aes.scm 490 */
															long BgL_arg2398z00_3824;

															BgL_arg2398z00_3824 =
																(long) (BgL_arg1602z00_1859);
															BgL_arg1598z00_1857 =
																(long) (BgL_arg2398z00_3824);
													}}
													BgL_vz00_1855 =
														(
														(long) (BgL_arg1595z00_1856) ^ BgL_arg1598z00_1857);
												}
												{	/* Unsafe/aes.scm 491 */
													int BgL_iz00_3829;
													int BgL_jz00_3830;
													unsigned long BgL_vz00_3831;

													BgL_iz00_3829 = (int) (BgL_iz00_1795);
													BgL_jz00_3830 = (int) (1L);
													BgL_vz00_3831 = (unsigned long) (BgL_vz00_1855);
													{	/* Unsafe/aes.scm 426 */
														obj_t BgL_arg1498z00_3832;

														BgL_arg1498z00_3832 =
															VECTOR_REF(BgL_wz00_1769, (long) (BgL_iz00_3829));
														{	/* Unsafe/aes.scm 426 */
															uint8_t BgL_auxz00_6948;
															long BgL_tmpz00_6946;

															BgL_auxz00_6948 = (uint8_t) (BgL_vz00_3831);
															BgL_tmpz00_6946 = (long) (BgL_jz00_3830);
															BGL_U8VSET(BgL_arg1498z00_3832, BgL_tmpz00_6946,
																BgL_auxz00_6948);
														} BUNSPEC;
											}}}
											{	/* Unsafe/aes.scm 489 */
												long BgL_vz00_1861;

												{	/* Unsafe/aes.scm 489 */
													unsigned long BgL_arg1603z00_1862;
													long BgL_arg1605z00_1863;

													{	/* Unsafe/aes.scm 489 */
														long BgL_arg1606z00_1864;

														BgL_arg1606z00_1864 =
															(BgL_iz00_1795 - BgL_nkz00_1767);
														{	/* Unsafe/aes.scm 489 */
															unsigned long BgL_res2532z00_3849;

															{	/* Unsafe/aes.scm 489 */
																int BgL_iz00_3838;
																int BgL_jz00_3839;

																BgL_iz00_3838 = (int) (BgL_arg1606z00_1864);
																BgL_jz00_3839 = (int) (2L);
																{	/* Unsafe/aes.scm 420 */
																	uint8_t BgL_arg1495z00_3840;

																	{	/* Unsafe/aes.scm 420 */
																		obj_t BgL_arg1497z00_3841;

																		BgL_arg1497z00_3841 =
																			VECTOR_REF(BgL_wz00_1769,
																			(long) (BgL_iz00_3838));
																		{	/* Unsafe/aes.scm 420 */
																			long BgL_kz00_3845;

																			BgL_kz00_3845 = (long) (BgL_jz00_3839);
																			{	/* Unsafe/aes.scm 420 */
																				obj_t BgL_tmpz00_6957;

																				BgL_tmpz00_6957 =
																					((obj_t) BgL_arg1497z00_3841);
																				BgL_arg1495z00_3840 =
																					BGL_U8VREF(BgL_tmpz00_6957,
																					BgL_kz00_3845);
																	}}}
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_arg2398z00_3847;

																		BgL_arg2398z00_3847 =
																			(long) (BgL_arg1495z00_3840);
																		BgL_res2532z00_3849 =
																			(unsigned long) (
																			(long) (BgL_arg2398z00_3847));
															}}}
															BgL_arg1603z00_1862 = BgL_res2532z00_3849;
													}}
													{	/* Unsafe/aes.scm 490 */
														uint8_t BgL_arg1607z00_1865;

														BgL_arg1607z00_1865 =
															BGL_U8VREF(BgL_tempz00_1770, 2L);
														{	/* Unsafe/aes.scm 490 */
															long BgL_arg2398z00_3853;

															BgL_arg2398z00_3853 =
																(long) (BgL_arg1607z00_1865);
															BgL_arg1605z00_1863 =
																(long) (BgL_arg2398z00_3853);
													}}
													BgL_vz00_1861 =
														(
														(long) (BgL_arg1603z00_1862) ^ BgL_arg1605z00_1863);
												}
												{	/* Unsafe/aes.scm 491 */
													int BgL_iz00_3858;
													int BgL_jz00_3859;
													unsigned long BgL_vz00_3860;

													BgL_iz00_3858 = (int) (BgL_iz00_1795);
													BgL_jz00_3859 = (int) (2L);
													BgL_vz00_3860 = (unsigned long) (BgL_vz00_1861);
													{	/* Unsafe/aes.scm 426 */
														obj_t BgL_arg1498z00_3861;

														BgL_arg1498z00_3861 =
															VECTOR_REF(BgL_wz00_1769, (long) (BgL_iz00_3858));
														{	/* Unsafe/aes.scm 426 */
															uint8_t BgL_auxz00_6975;
															long BgL_tmpz00_6973;

															BgL_auxz00_6975 = (uint8_t) (BgL_vz00_3860);
															BgL_tmpz00_6973 = (long) (BgL_jz00_3859);
															BGL_U8VSET(BgL_arg1498z00_3861, BgL_tmpz00_6973,
																BgL_auxz00_6975);
														} BUNSPEC;
											}}}
											{	/* Unsafe/aes.scm 489 */
												long BgL_vz00_1867;

												{	/* Unsafe/aes.scm 489 */
													unsigned long BgL_arg1608z00_1868;
													long BgL_arg1609z00_1869;

													{	/* Unsafe/aes.scm 489 */
														long BgL_arg1610z00_1870;

														BgL_arg1610z00_1870 =
															(BgL_iz00_1795 - BgL_nkz00_1767);
														{	/* Unsafe/aes.scm 489 */
															unsigned long BgL_res2533z00_3878;

															{	/* Unsafe/aes.scm 489 */
																int BgL_iz00_3867;
																int BgL_jz00_3868;

																BgL_iz00_3867 = (int) (BgL_arg1610z00_1870);
																BgL_jz00_3868 = (int) (3L);
																{	/* Unsafe/aes.scm 420 */
																	uint8_t BgL_arg1495z00_3869;

																	{	/* Unsafe/aes.scm 420 */
																		obj_t BgL_arg1497z00_3870;

																		BgL_arg1497z00_3870 =
																			VECTOR_REF(BgL_wz00_1769,
																			(long) (BgL_iz00_3867));
																		{	/* Unsafe/aes.scm 420 */
																			long BgL_kz00_3874;

																			BgL_kz00_3874 = (long) (BgL_jz00_3868);
																			{	/* Unsafe/aes.scm 420 */
																				obj_t BgL_tmpz00_6984;

																				BgL_tmpz00_6984 =
																					((obj_t) BgL_arg1497z00_3870);
																				BgL_arg1495z00_3869 =
																					BGL_U8VREF(BgL_tmpz00_6984,
																					BgL_kz00_3874);
																	}}}
																	{	/* Unsafe/aes.scm 420 */
																		long BgL_arg2398z00_3876;

																		BgL_arg2398z00_3876 =
																			(long) (BgL_arg1495z00_3869);
																		BgL_res2533z00_3878 =
																			(unsigned long) (
																			(long) (BgL_arg2398z00_3876));
															}}}
															BgL_arg1608z00_1868 = BgL_res2533z00_3878;
													}}
													{	/* Unsafe/aes.scm 490 */
														uint8_t BgL_arg1611z00_1871;

														BgL_arg1611z00_1871 =
															BGL_U8VREF(BgL_tempz00_1770, 3L);
														{	/* Unsafe/aes.scm 490 */
															long BgL_arg2398z00_3882;

															BgL_arg2398z00_3882 =
																(long) (BgL_arg1611z00_1871);
															BgL_arg1609z00_1869 =
																(long) (BgL_arg2398z00_3882);
													}}
													BgL_vz00_1867 =
														(
														(long) (BgL_arg1608z00_1868) ^ BgL_arg1609z00_1869);
												}
												{	/* Unsafe/aes.scm 491 */
													int BgL_iz00_3887;
													int BgL_jz00_3888;
													unsigned long BgL_vz00_3889;

													BgL_iz00_3887 = (int) (BgL_iz00_1795);
													BgL_jz00_3888 = (int) (3L);
													BgL_vz00_3889 = (unsigned long) (BgL_vz00_1867);
													{	/* Unsafe/aes.scm 426 */
														obj_t BgL_arg1498z00_3890;

														BgL_arg1498z00_3890 =
															VECTOR_REF(BgL_wz00_1769, (long) (BgL_iz00_3887));
														{	/* Unsafe/aes.scm 426 */
															uint8_t BgL_auxz00_7002;
															long BgL_tmpz00_7000;

															BgL_auxz00_7002 = (uint8_t) (BgL_vz00_3889);
															BgL_tmpz00_7000 = (long) (BgL_jz00_3888);
															BGL_U8VSET(BgL_arg1498z00_3890, BgL_tmpz00_7000,
																BgL_auxz00_7002);
														} BUNSPEC;
											}}}
											{
												long BgL_iz00_7005;

												BgL_iz00_7005 = (BgL_iz00_1795 + 1L);
												BgL_iz00_1795 = BgL_iz00_7005;
												goto BgL_zc3z04anonymousza31532ze3z87_1796;
											}
										}
									else
										{	/* Unsafe/aes.scm 475 */
											((bool_t) 0);
										}
								}
								return BgL_wz00_1769;
							}
						}
					}
				}
			}
		}

	}



/* aes-cipher */
	obj_t BGl_aeszd2cipherzd2zz__aesz00(obj_t BgL_inputz00_83, obj_t BgL_wz00_84,
		obj_t BgL_statez00_85)
	{
		{	/* Unsafe/aes.scm 498 */
			{	/* Unsafe/aes.scm 499 */
				long BgL_nrz00_1883;

				BgL_nrz00_1883 = ((VECTOR_LENGTH(BgL_wz00_84) / 4L) - 1L);
				{	/* Unsafe/aes.scm 500 */

					{
						long BgL_iz00_1885;

						BgL_iz00_1885 = 0L;
					BgL_zc3z04anonymousza31620ze3z87_1886:
						if ((BgL_iz00_1885 < (4L * 4L)))
							{	/* Unsafe/aes.scm 502 */
								{	/* Unsafe/aes.scm 503 */
									long BgL_vz00_1889;

									{	/* Unsafe/aes.scm 503 */
										uint8_t BgL_arg1625z00_1892;

										BgL_arg1625z00_1892 =
											BGL_U8VREF(BgL_inputz00_83, BgL_iz00_1885);
										{	/* Unsafe/aes.scm 503 */
											long BgL_arg2398z00_3904;

											BgL_arg2398z00_3904 = (long) (BgL_arg1625z00_1892);
											BgL_vz00_1889 = (long) (BgL_arg2398z00_3904);
									}}
									{	/* Unsafe/aes.scm 504 */
										obj_t BgL_arg1623z00_1890;
										long BgL_arg1624z00_1891;

										BgL_arg1623z00_1890 =
											BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_iz00_1885), BINT(4L));
										BgL_arg1624z00_1891 = (BgL_iz00_1885 / 4L);
										{	/* Unsafe/aes.scm 504 */
											int BgL_iz00_3908;
											int BgL_jz00_3909;
											unsigned long BgL_vz00_3910;

											BgL_iz00_3908 = CINT(BgL_arg1623z00_1890);
											BgL_jz00_3909 = (int) (BgL_arg1624z00_1891);
											BgL_vz00_3910 = (unsigned long) (BgL_vz00_1889);
											{	/* Unsafe/aes.scm 426 */
												obj_t BgL_arg1498z00_3911;

												BgL_arg1498z00_3911 =
													VECTOR_REF(BgL_statez00_85, (long) (BgL_iz00_3908));
												{	/* Unsafe/aes.scm 426 */
													uint8_t BgL_auxz00_7027;
													long BgL_tmpz00_7025;

													BgL_auxz00_7027 = (uint8_t) (BgL_vz00_3910);
													BgL_tmpz00_7025 = (long) (BgL_jz00_3909);
													BGL_U8VSET(BgL_arg1498z00_3911, BgL_tmpz00_7025,
														BgL_auxz00_7027);
												} BUNSPEC;
								}}}}
								{
									long BgL_iz00_7030;

									BgL_iz00_7030 = (BgL_iz00_1885 + 1L);
									BgL_iz00_1885 = BgL_iz00_7030;
									goto BgL_zc3z04anonymousza31620ze3z87_1886;
								}
							}
						else
							{	/* Unsafe/aes.scm 502 */
								((bool_t) 0);
							}
					}
					BGl_addroundkeyz12z12zz__aesz00(BgL_statez00_85, BgL_wz00_84, 0L, 4L);
					{
						long BgL_roundz00_1897;

						BgL_roundz00_1897 = 1L;
					BgL_zc3z04anonymousza31628ze3z87_1898:
						if ((BgL_roundz00_1897 < BgL_nrz00_1883))
							{	/* Unsafe/aes.scm 508 */
								BGl_subbytesz12z12zz__aesz00(BgL_statez00_85, (int) (4L));
								BGl_shiftrowsz12z12zz__aesz00(BgL_statez00_85, 4L);
								BGl_mixcolumnsz12z12zz__aesz00(BgL_statez00_85, 4L);
								BGl_addroundkeyz12z12zz__aesz00(BgL_statez00_85, BgL_wz00_84,
									BgL_roundz00_1897, 4L);
								{
									long BgL_roundz00_7040;

									BgL_roundz00_7040 = (BgL_roundz00_1897 + 1L);
									BgL_roundz00_1897 = BgL_roundz00_7040;
									goto BgL_zc3z04anonymousza31628ze3z87_1898;
								}
							}
						else
							{	/* Unsafe/aes.scm 508 */
								((bool_t) 0);
							}
					}
					BGl_subbytesz12z12zz__aesz00(BgL_statez00_85, (int) (4L));
					BGl_shiftrowsz12z12zz__aesz00(BgL_statez00_85, 4L);
					BGl_addroundkeyz12z12zz__aesz00(BgL_statez00_85, BgL_wz00_84,
						BgL_nrz00_1883, 4L);
					{	/* Unsafe/aes.scm 518 */
						obj_t BgL_outputz00_1902;

						{	/* Unsafe/aes.scm 518 */
							long BgL_arg1640z00_1914;

							BgL_arg1640z00_1914 = (4L * 4L);
							{	/* Llib/srfi4.scm 447 */

								BgL_outputz00_1902 =
									BGl_makezd2u8vectorzd2zz__srfi4z00(BgL_arg1640z00_1914,
									(uint8_t) (0));
						}}
						{
							long BgL_iz00_1904;

							BgL_iz00_1904 = 0L;
						BgL_zc3z04anonymousza31631ze3z87_1905:
							if ((BgL_iz00_1904 < (4L * 4L)))
								{	/* Unsafe/aes.scm 519 */
									{	/* Unsafe/aes.scm 520 */
										unsigned long BgL_vz00_1908;

										{	/* Unsafe/aes.scm 520 */
											obj_t BgL_arg1636z00_1909;
											long BgL_arg1637z00_1910;

											BgL_arg1636z00_1909 =
												BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
												(BgL_iz00_1904), BINT(4L));
											BgL_arg1637z00_1910 = (BgL_iz00_1904 / 4L);
											{	/* Unsafe/aes.scm 520 */
												unsigned long BgL_res2534z00_3935;

												{	/* Unsafe/aes.scm 520 */
													int BgL_iz00_3924;
													int BgL_jz00_3925;

													BgL_iz00_3924 = CINT(BgL_arg1636z00_1909);
													BgL_jz00_3925 = (int) (BgL_arg1637z00_1910);
													{	/* Unsafe/aes.scm 420 */
														uint8_t BgL_arg1495z00_3926;

														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_arg1497z00_3927;

															BgL_arg1497z00_3927 =
																VECTOR_REF(BgL_statez00_85,
																(long) (BgL_iz00_3924));
															{	/* Unsafe/aes.scm 420 */
																long BgL_kz00_3931;

																BgL_kz00_3931 = (long) (BgL_jz00_3925);
																{	/* Unsafe/aes.scm 420 */
																	obj_t BgL_tmpz00_7060;

																	BgL_tmpz00_7060 =
																		((obj_t) BgL_arg1497z00_3927);
																	BgL_arg1495z00_3926 =
																		BGL_U8VREF(BgL_tmpz00_7060, BgL_kz00_3931);
														}}}
														{	/* Unsafe/aes.scm 420 */
															long BgL_arg2398z00_3933;

															BgL_arg2398z00_3933 =
																(long) (BgL_arg1495z00_3926);
															BgL_res2534z00_3935 =
																(unsigned long) ((long) (BgL_arg2398z00_3933));
												}}}
												BgL_vz00_1908 = BgL_res2534z00_3935;
										}}
										{	/* Unsafe/aes.scm 521 */
											uint8_t BgL_tmpz00_7066;

											BgL_tmpz00_7066 = (uint8_t) (BgL_vz00_1908);
											BGL_U8VSET(BgL_outputz00_1902, BgL_iz00_1904,
												BgL_tmpz00_7066);
										} BUNSPEC;
									}
									{
										long BgL_iz00_7069;

										BgL_iz00_7069 = (BgL_iz00_1904 + 1L);
										BgL_iz00_1904 = BgL_iz00_7069;
										goto BgL_zc3z04anonymousza31631ze3z87_1905;
									}
								}
							else
								{	/* Unsafe/aes.scm 519 */
									((bool_t) 0);
								}
						}
						return BgL_outputz00_1902;
					}
				}
			}
		}

	}



/* subbytes! */
	bool_t BGl_subbytesz12z12zz__aesz00(obj_t BgL_sz00_86, int BgL_nbz00_87)
	{
		{	/* Unsafe/aes.scm 529 */
			{
				long BgL_cz00_1921;

				BgL_cz00_1921 = 0L;
			BgL_zc3z04anonymousza31643ze3z87_1922:
				if ((BgL_cz00_1921 < (long) (BgL_nbz00_87)))
					{	/* Unsafe/aes.scm 531 */
						{	/* Unsafe/aes.scm 532 */
							long BgL_arg1645z00_1924;

							{	/* Unsafe/aes.scm 532 */
								uint8_t BgL_arg1646z00_1925;

								{	/* Unsafe/aes.scm 532 */
									unsigned long BgL_arg1648z00_1926;

									{	/* Unsafe/aes.scm 532 */
										unsigned long BgL_res2535z00_3951;

										{	/* Unsafe/aes.scm 532 */
											int BgL_iz00_3940;
											int BgL_jz00_3941;

											BgL_iz00_3940 = (int) (0L);
											BgL_jz00_3941 = (int) (BgL_cz00_1921);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_3942;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_3943;

													BgL_arg1497z00_3943 =
														VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_3940));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_3947;

														BgL_kz00_3947 = (long) (BgL_jz00_3941);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7079;

															BgL_tmpz00_7079 = ((obj_t) BgL_arg1497z00_3943);
															BgL_arg1495z00_3942 =
																BGL_U8VREF(BgL_tmpz00_7079, BgL_kz00_3947);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_3949;

													BgL_arg2398z00_3949 = (long) (BgL_arg1495z00_3942);
													BgL_res2535z00_3951 =
														(unsigned long) ((long) (BgL_arg2398z00_3949));
										}}}
										BgL_arg1648z00_1926 = BgL_res2535z00_3951;
									}
									{	/* Unsafe/aes.scm 532 */
										obj_t BgL_vz00_3952;
										long BgL_kz00_3953;

										BgL_vz00_3952 = CNST_TABLE_REF(1);
										BgL_kz00_3953 = (long) (BgL_arg1648z00_1926);
										BgL_arg1646z00_1925 =
											BGL_U8VREF(BgL_vz00_3952, BgL_kz00_3953);
								}}
								{	/* Unsafe/aes.scm 532 */
									long BgL_arg2398z00_3955;

									BgL_arg2398z00_3955 = (long) (BgL_arg1646z00_1925);
									BgL_arg1645z00_1924 = (long) (BgL_arg2398z00_3955);
							}}
							{	/* Unsafe/aes.scm 532 */
								int BgL_iz00_3958;
								int BgL_jz00_3959;
								unsigned long BgL_vz00_3960;

								BgL_iz00_3958 = (int) (0L);
								BgL_jz00_3959 = (int) (BgL_cz00_1921);
								BgL_vz00_3960 = (unsigned long) (BgL_arg1645z00_1924);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_3961;

									BgL_arg1498z00_3961 =
										VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_3958));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_7097;
										long BgL_tmpz00_7095;

										BgL_auxz00_7097 = (uint8_t) (BgL_vz00_3960);
										BgL_tmpz00_7095 = (long) (BgL_jz00_3959);
										BGL_U8VSET(BgL_arg1498z00_3961, BgL_tmpz00_7095,
											BgL_auxz00_7097);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_7100;

							BgL_cz00_7100 = (BgL_cz00_1921 + 1L);
							BgL_cz00_1921 = BgL_cz00_7100;
							goto BgL_zc3z04anonymousza31643ze3z87_1922;
						}
					}
				else
					{	/* Unsafe/aes.scm 531 */
						((bool_t) 0);
					}
			}
			{
				long BgL_cz00_1931;

				BgL_cz00_1931 = 0L;
			BgL_zc3z04anonymousza31650ze3z87_1932:
				if ((BgL_cz00_1931 < (long) (BgL_nbz00_87)))
					{	/* Unsafe/aes.scm 531 */
						{	/* Unsafe/aes.scm 532 */
							long BgL_arg1652z00_1934;

							{	/* Unsafe/aes.scm 532 */
								uint8_t BgL_arg1653z00_1935;

								{	/* Unsafe/aes.scm 532 */
									unsigned long BgL_arg1654z00_1936;

									{	/* Unsafe/aes.scm 532 */
										unsigned long BgL_res2536z00_3979;

										{	/* Unsafe/aes.scm 532 */
											int BgL_iz00_3968;
											int BgL_jz00_3969;

											BgL_iz00_3968 = (int) (1L);
											BgL_jz00_3969 = (int) (BgL_cz00_1931);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_3970;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_3971;

													BgL_arg1497z00_3971 =
														VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_3968));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_3975;

														BgL_kz00_3975 = (long) (BgL_jz00_3969);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7110;

															BgL_tmpz00_7110 = ((obj_t) BgL_arg1497z00_3971);
															BgL_arg1495z00_3970 =
																BGL_U8VREF(BgL_tmpz00_7110, BgL_kz00_3975);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_3977;

													BgL_arg2398z00_3977 = (long) (BgL_arg1495z00_3970);
													BgL_res2536z00_3979 =
														(unsigned long) ((long) (BgL_arg2398z00_3977));
										}}}
										BgL_arg1654z00_1936 = BgL_res2536z00_3979;
									}
									{	/* Unsafe/aes.scm 532 */
										obj_t BgL_vz00_3980;
										long BgL_kz00_3981;

										BgL_vz00_3980 = CNST_TABLE_REF(1);
										BgL_kz00_3981 = (long) (BgL_arg1654z00_1936);
										BgL_arg1653z00_1935 =
											BGL_U8VREF(BgL_vz00_3980, BgL_kz00_3981);
								}}
								{	/* Unsafe/aes.scm 532 */
									long BgL_arg2398z00_3983;

									BgL_arg2398z00_3983 = (long) (BgL_arg1653z00_1935);
									BgL_arg1652z00_1934 = (long) (BgL_arg2398z00_3983);
							}}
							{	/* Unsafe/aes.scm 532 */
								int BgL_iz00_3986;
								int BgL_jz00_3987;
								unsigned long BgL_vz00_3988;

								BgL_iz00_3986 = (int) (1L);
								BgL_jz00_3987 = (int) (BgL_cz00_1931);
								BgL_vz00_3988 = (unsigned long) (BgL_arg1652z00_1934);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_3989;

									BgL_arg1498z00_3989 =
										VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_3986));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_7128;
										long BgL_tmpz00_7126;

										BgL_auxz00_7128 = (uint8_t) (BgL_vz00_3988);
										BgL_tmpz00_7126 = (long) (BgL_jz00_3987);
										BGL_U8VSET(BgL_arg1498z00_3989, BgL_tmpz00_7126,
											BgL_auxz00_7128);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_7131;

							BgL_cz00_7131 = (BgL_cz00_1931 + 1L);
							BgL_cz00_1931 = BgL_cz00_7131;
							goto BgL_zc3z04anonymousza31650ze3z87_1932;
						}
					}
				else
					{	/* Unsafe/aes.scm 531 */
						((bool_t) 0);
					}
			}
			{
				long BgL_cz00_1941;

				BgL_cz00_1941 = 0L;
			BgL_zc3z04anonymousza31657ze3z87_1942:
				if ((BgL_cz00_1941 < (long) (BgL_nbz00_87)))
					{	/* Unsafe/aes.scm 531 */
						{	/* Unsafe/aes.scm 532 */
							long BgL_arg1661z00_1944;

							{	/* Unsafe/aes.scm 532 */
								uint8_t BgL_arg1663z00_1945;

								{	/* Unsafe/aes.scm 532 */
									unsigned long BgL_arg1664z00_1946;

									{	/* Unsafe/aes.scm 532 */
										unsigned long BgL_res2537z00_4007;

										{	/* Unsafe/aes.scm 532 */
											int BgL_iz00_3996;
											int BgL_jz00_3997;

											BgL_iz00_3996 = (int) (2L);
											BgL_jz00_3997 = (int) (BgL_cz00_1941);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_3998;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_3999;

													BgL_arg1497z00_3999 =
														VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_3996));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_4003;

														BgL_kz00_4003 = (long) (BgL_jz00_3997);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7141;

															BgL_tmpz00_7141 = ((obj_t) BgL_arg1497z00_3999);
															BgL_arg1495z00_3998 =
																BGL_U8VREF(BgL_tmpz00_7141, BgL_kz00_4003);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_4005;

													BgL_arg2398z00_4005 = (long) (BgL_arg1495z00_3998);
													BgL_res2537z00_4007 =
														(unsigned long) ((long) (BgL_arg2398z00_4005));
										}}}
										BgL_arg1664z00_1946 = BgL_res2537z00_4007;
									}
									{	/* Unsafe/aes.scm 532 */
										obj_t BgL_vz00_4008;
										long BgL_kz00_4009;

										BgL_vz00_4008 = CNST_TABLE_REF(1);
										BgL_kz00_4009 = (long) (BgL_arg1664z00_1946);
										BgL_arg1663z00_1945 =
											BGL_U8VREF(BgL_vz00_4008, BgL_kz00_4009);
								}}
								{	/* Unsafe/aes.scm 532 */
									long BgL_arg2398z00_4011;

									BgL_arg2398z00_4011 = (long) (BgL_arg1663z00_1945);
									BgL_arg1661z00_1944 = (long) (BgL_arg2398z00_4011);
							}}
							{	/* Unsafe/aes.scm 532 */
								int BgL_iz00_4014;
								int BgL_jz00_4015;
								unsigned long BgL_vz00_4016;

								BgL_iz00_4014 = (int) (2L);
								BgL_jz00_4015 = (int) (BgL_cz00_1941);
								BgL_vz00_4016 = (unsigned long) (BgL_arg1661z00_1944);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_4017;

									BgL_arg1498z00_4017 =
										VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_4014));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_7159;
										long BgL_tmpz00_7157;

										BgL_auxz00_7159 = (uint8_t) (BgL_vz00_4016);
										BgL_tmpz00_7157 = (long) (BgL_jz00_4015);
										BGL_U8VSET(BgL_arg1498z00_4017, BgL_tmpz00_7157,
											BgL_auxz00_7159);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_7162;

							BgL_cz00_7162 = (BgL_cz00_1941 + 1L);
							BgL_cz00_1941 = BgL_cz00_7162;
							goto BgL_zc3z04anonymousza31657ze3z87_1942;
						}
					}
				else
					{	/* Unsafe/aes.scm 531 */
						((bool_t) 0);
					}
			}
			{
				long BgL_cz00_1951;

				BgL_cz00_1951 = 0L;
			BgL_zc3z04anonymousza31668ze3z87_1952:
				if ((BgL_cz00_1951 < (long) (BgL_nbz00_87)))
					{	/* Unsafe/aes.scm 531 */
						{	/* Unsafe/aes.scm 532 */
							long BgL_arg1670z00_1954;

							{	/* Unsafe/aes.scm 532 */
								uint8_t BgL_arg1675z00_1955;

								{	/* Unsafe/aes.scm 532 */
									unsigned long BgL_arg1676z00_1956;

									{	/* Unsafe/aes.scm 532 */
										unsigned long BgL_res2538z00_4035;

										{	/* Unsafe/aes.scm 532 */
											int BgL_iz00_4024;
											int BgL_jz00_4025;

											BgL_iz00_4024 = (int) (3L);
											BgL_jz00_4025 = (int) (BgL_cz00_1951);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_4026;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_4027;

													BgL_arg1497z00_4027 =
														VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_4024));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_4031;

														BgL_kz00_4031 = (long) (BgL_jz00_4025);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7172;

															BgL_tmpz00_7172 = ((obj_t) BgL_arg1497z00_4027);
															BgL_arg1495z00_4026 =
																BGL_U8VREF(BgL_tmpz00_7172, BgL_kz00_4031);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_4033;

													BgL_arg2398z00_4033 = (long) (BgL_arg1495z00_4026);
													BgL_res2538z00_4035 =
														(unsigned long) ((long) (BgL_arg2398z00_4033));
										}}}
										BgL_arg1676z00_1956 = BgL_res2538z00_4035;
									}
									{	/* Unsafe/aes.scm 532 */
										obj_t BgL_vz00_4036;
										long BgL_kz00_4037;

										BgL_vz00_4036 = CNST_TABLE_REF(1);
										BgL_kz00_4037 = (long) (BgL_arg1676z00_1956);
										BgL_arg1675z00_1955 =
											BGL_U8VREF(BgL_vz00_4036, BgL_kz00_4037);
								}}
								{	/* Unsafe/aes.scm 532 */
									long BgL_arg2398z00_4039;

									BgL_arg2398z00_4039 = (long) (BgL_arg1675z00_1955);
									BgL_arg1670z00_1954 = (long) (BgL_arg2398z00_4039);
							}}
							{	/* Unsafe/aes.scm 532 */
								int BgL_iz00_4042;
								int BgL_jz00_4043;
								unsigned long BgL_vz00_4044;

								BgL_iz00_4042 = (int) (3L);
								BgL_jz00_4043 = (int) (BgL_cz00_1951);
								BgL_vz00_4044 = (unsigned long) (BgL_arg1670z00_1954);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_4045;

									BgL_arg1498z00_4045 =
										VECTOR_REF(BgL_sz00_86, (long) (BgL_iz00_4042));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_7190;
										long BgL_tmpz00_7188;

										BgL_auxz00_7190 = (uint8_t) (BgL_vz00_4044);
										BgL_tmpz00_7188 = (long) (BgL_jz00_4043);
										BGL_U8VSET(BgL_arg1498z00_4045, BgL_tmpz00_7188,
											BgL_auxz00_7190);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_7193;

							BgL_cz00_7193 = (BgL_cz00_1951 + 1L);
							BgL_cz00_1951 = BgL_cz00_7193;
							goto BgL_zc3z04anonymousza31668ze3z87_1952;
						}
					}
				else
					{	/* Unsafe/aes.scm 531 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* shiftrows! */
	bool_t BGl_shiftrowsz12z12zz__aesz00(obj_t BgL_sz00_88, long BgL_nbz00_89)
	{
		{	/* Unsafe/aes.scm 539 */
			{	/* Unsafe/aes.scm 540 */
				obj_t BgL_tz00_1959;

				{	/* Llib/srfi4.scm 447 */

					BgL_tz00_1959 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{
					long BgL_rz00_1961;

					BgL_rz00_1961 = 1L;
				BgL_zc3z04anonymousza31679ze3z87_1962:
					if ((BgL_rz00_1961 < 4L))
						{	/* Unsafe/aes.scm 541 */
							{	/* Unsafe/aes.scm 543 */
								unsigned long BgL_arg1681z00_1965;

								{	/* Unsafe/aes.scm 543 */
									obj_t BgL_arg1684z00_1966;

									{	/* Unsafe/aes.scm 543 */
										long BgL_arg1685z00_1967;

										BgL_arg1685z00_1967 = (0L + BgL_rz00_1961);
										BgL_arg1684z00_1966 =
											BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_arg1685z00_1967), BINT(BgL_nbz00_89));
									}
									{	/* Unsafe/aes.scm 543 */
										unsigned long BgL_res2539z00_4064;

										{	/* Unsafe/aes.scm 543 */
											int BgL_iz00_4053;
											int BgL_jz00_4054;

											BgL_iz00_4053 = (int) (BgL_rz00_1961);
											BgL_jz00_4054 = CINT(BgL_arg1684z00_1966);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_4055;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_4056;

													BgL_arg1497z00_4056 =
														VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4053));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_4060;

														BgL_kz00_4060 = (long) (BgL_jz00_4054);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7207;

															BgL_tmpz00_7207 = ((obj_t) BgL_arg1497z00_4056);
															BgL_arg1495z00_4055 =
																BGL_U8VREF(BgL_tmpz00_7207, BgL_kz00_4060);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_4062;

													BgL_arg2398z00_4062 = (long) (BgL_arg1495z00_4055);
													BgL_res2539z00_4064 =
														(unsigned long) ((long) (BgL_arg2398z00_4062));
										}}}
										BgL_arg1681z00_1965 = BgL_res2539z00_4064;
								}}
								{	/* Unsafe/aes.scm 543 */
									uint8_t BgL_tmpz00_7213;

									BgL_tmpz00_7213 = (uint8_t) (BgL_arg1681z00_1965);
									BGL_U8VSET(BgL_tz00_1959, 0L, BgL_tmpz00_7213);
								} BUNSPEC;
							}
							{	/* Unsafe/aes.scm 543 */
								unsigned long BgL_arg1688z00_1969;

								{	/* Unsafe/aes.scm 543 */
									obj_t BgL_arg1689z00_1970;

									{	/* Unsafe/aes.scm 543 */
										long BgL_arg1691z00_1971;

										BgL_arg1691z00_1971 = (1L + BgL_rz00_1961);
										BgL_arg1689z00_1970 =
											BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_arg1691z00_1971), BINT(BgL_nbz00_89));
									}
									{	/* Unsafe/aes.scm 543 */
										unsigned long BgL_res2540z00_4079;

										{	/* Unsafe/aes.scm 543 */
											int BgL_iz00_4068;
											int BgL_jz00_4069;

											BgL_iz00_4068 = (int) (BgL_rz00_1961);
											BgL_jz00_4069 = CINT(BgL_arg1689z00_1970);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_4070;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_4071;

													BgL_arg1497z00_4071 =
														VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4068));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_4075;

														BgL_kz00_4075 = (long) (BgL_jz00_4069);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7225;

															BgL_tmpz00_7225 = ((obj_t) BgL_arg1497z00_4071);
															BgL_arg1495z00_4070 =
																BGL_U8VREF(BgL_tmpz00_7225, BgL_kz00_4075);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_4077;

													BgL_arg2398z00_4077 = (long) (BgL_arg1495z00_4070);
													BgL_res2540z00_4079 =
														(unsigned long) ((long) (BgL_arg2398z00_4077));
										}}}
										BgL_arg1688z00_1969 = BgL_res2540z00_4079;
								}}
								{	/* Unsafe/aes.scm 543 */
									uint8_t BgL_tmpz00_7231;

									BgL_tmpz00_7231 = (uint8_t) (BgL_arg1688z00_1969);
									BGL_U8VSET(BgL_tz00_1959, 1L, BgL_tmpz00_7231);
								} BUNSPEC;
							}
							{	/* Unsafe/aes.scm 543 */
								unsigned long BgL_arg1692z00_1973;

								{	/* Unsafe/aes.scm 543 */
									obj_t BgL_arg1699z00_1974;

									{	/* Unsafe/aes.scm 543 */
										long BgL_arg1700z00_1975;

										BgL_arg1700z00_1975 = (2L + BgL_rz00_1961);
										BgL_arg1699z00_1974 =
											BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_arg1700z00_1975), BINT(BgL_nbz00_89));
									}
									{	/* Unsafe/aes.scm 543 */
										unsigned long BgL_res2541z00_4094;

										{	/* Unsafe/aes.scm 543 */
											int BgL_iz00_4083;
											int BgL_jz00_4084;

											BgL_iz00_4083 = (int) (BgL_rz00_1961);
											BgL_jz00_4084 = CINT(BgL_arg1699z00_1974);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_4085;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_4086;

													BgL_arg1497z00_4086 =
														VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4083));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_4090;

														BgL_kz00_4090 = (long) (BgL_jz00_4084);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7243;

															BgL_tmpz00_7243 = ((obj_t) BgL_arg1497z00_4086);
															BgL_arg1495z00_4085 =
																BGL_U8VREF(BgL_tmpz00_7243, BgL_kz00_4090);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_4092;

													BgL_arg2398z00_4092 = (long) (BgL_arg1495z00_4085);
													BgL_res2541z00_4094 =
														(unsigned long) ((long) (BgL_arg2398z00_4092));
										}}}
										BgL_arg1692z00_1973 = BgL_res2541z00_4094;
								}}
								{	/* Unsafe/aes.scm 543 */
									uint8_t BgL_tmpz00_7249;

									BgL_tmpz00_7249 = (uint8_t) (BgL_arg1692z00_1973);
									BGL_U8VSET(BgL_tz00_1959, 2L, BgL_tmpz00_7249);
								} BUNSPEC;
							}
							{	/* Unsafe/aes.scm 543 */
								unsigned long BgL_arg1701z00_1977;

								{	/* Unsafe/aes.scm 543 */
									obj_t BgL_arg1702z00_1978;

									{	/* Unsafe/aes.scm 543 */
										long BgL_arg1703z00_1979;

										BgL_arg1703z00_1979 = (3L + BgL_rz00_1961);
										BgL_arg1702z00_1978 =
											BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
											(BgL_arg1703z00_1979), BINT(BgL_nbz00_89));
									}
									{	/* Unsafe/aes.scm 543 */
										unsigned long BgL_res2542z00_4109;

										{	/* Unsafe/aes.scm 543 */
											int BgL_iz00_4098;
											int BgL_jz00_4099;

											BgL_iz00_4098 = (int) (BgL_rz00_1961);
											BgL_jz00_4099 = CINT(BgL_arg1702z00_1978);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_4100;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_4101;

													BgL_arg1497z00_4101 =
														VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4098));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_4105;

														BgL_kz00_4105 = (long) (BgL_jz00_4099);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_7261;

															BgL_tmpz00_7261 = ((obj_t) BgL_arg1497z00_4101);
															BgL_arg1495z00_4100 =
																BGL_U8VREF(BgL_tmpz00_7261, BgL_kz00_4105);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_4107;

													BgL_arg2398z00_4107 = (long) (BgL_arg1495z00_4100);
													BgL_res2542z00_4109 =
														(unsigned long) ((long) (BgL_arg2398z00_4107));
										}}}
										BgL_arg1701z00_1977 = BgL_res2542z00_4109;
								}}
								{	/* Unsafe/aes.scm 543 */
									uint8_t BgL_tmpz00_7267;

									BgL_tmpz00_7267 = (uint8_t) (BgL_arg1701z00_1977);
									BGL_U8VSET(BgL_tz00_1959, 3L, BgL_tmpz00_7267);
								} BUNSPEC;
							}
							{	/* Unsafe/aes.scm 545 */
								long BgL_arg1704z00_1981;

								{	/* Unsafe/aes.scm 545 */
									uint8_t BgL_arg1705z00_1982;

									BgL_arg1705z00_1982 = BGL_U8VREF(BgL_tz00_1959, 0L);
									{	/* Unsafe/aes.scm 545 */
										long BgL_arg2398z00_4113;

										BgL_arg2398z00_4113 = (long) (BgL_arg1705z00_1982);
										BgL_arg1704z00_1981 = (long) (BgL_arg2398z00_4113);
								}}
								{	/* Unsafe/aes.scm 545 */
									int BgL_iz00_4116;
									int BgL_jz00_4117;
									unsigned long BgL_vz00_4118;

									BgL_iz00_4116 = (int) (BgL_rz00_1961);
									BgL_jz00_4117 = (int) (0L);
									BgL_vz00_4118 = (unsigned long) (BgL_arg1704z00_1981);
									{	/* Unsafe/aes.scm 426 */
										obj_t BgL_arg1498z00_4119;

										BgL_arg1498z00_4119 =
											VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4116));
										{	/* Unsafe/aes.scm 426 */
											uint8_t BgL_auxz00_7280;
											long BgL_tmpz00_7278;

											BgL_auxz00_7280 = (uint8_t) (BgL_vz00_4118);
											BgL_tmpz00_7278 = (long) (BgL_jz00_4117);
											BGL_U8VSET(BgL_arg1498z00_4119, BgL_tmpz00_7278,
												BgL_auxz00_7280);
										} BUNSPEC;
							}}}
							{	/* Unsafe/aes.scm 545 */
								long BgL_arg1706z00_1984;

								{	/* Unsafe/aes.scm 545 */
									uint8_t BgL_arg1707z00_1985;

									BgL_arg1707z00_1985 = BGL_U8VREF(BgL_tz00_1959, 1L);
									{	/* Unsafe/aes.scm 545 */
										long BgL_arg2398z00_4125;

										BgL_arg2398z00_4125 = (long) (BgL_arg1707z00_1985);
										BgL_arg1706z00_1984 = (long) (BgL_arg2398z00_4125);
								}}
								{	/* Unsafe/aes.scm 545 */
									int BgL_iz00_4128;
									int BgL_jz00_4129;
									unsigned long BgL_vz00_4130;

									BgL_iz00_4128 = (int) (BgL_rz00_1961);
									BgL_jz00_4129 = (int) (1L);
									BgL_vz00_4130 = (unsigned long) (BgL_arg1706z00_1984);
									{	/* Unsafe/aes.scm 426 */
										obj_t BgL_arg1498z00_4131;

										BgL_arg1498z00_4131 =
											VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4128));
										{	/* Unsafe/aes.scm 426 */
											uint8_t BgL_auxz00_7293;
											long BgL_tmpz00_7291;

											BgL_auxz00_7293 = (uint8_t) (BgL_vz00_4130);
											BgL_tmpz00_7291 = (long) (BgL_jz00_4129);
											BGL_U8VSET(BgL_arg1498z00_4131, BgL_tmpz00_7291,
												BgL_auxz00_7293);
										} BUNSPEC;
							}}}
							{	/* Unsafe/aes.scm 545 */
								long BgL_arg1708z00_1987;

								{	/* Unsafe/aes.scm 545 */
									uint8_t BgL_arg1709z00_1988;

									BgL_arg1709z00_1988 = BGL_U8VREF(BgL_tz00_1959, 2L);
									{	/* Unsafe/aes.scm 545 */
										long BgL_arg2398z00_4137;

										BgL_arg2398z00_4137 = (long) (BgL_arg1709z00_1988);
										BgL_arg1708z00_1987 = (long) (BgL_arg2398z00_4137);
								}}
								{	/* Unsafe/aes.scm 545 */
									int BgL_iz00_4140;
									int BgL_jz00_4141;
									unsigned long BgL_vz00_4142;

									BgL_iz00_4140 = (int) (BgL_rz00_1961);
									BgL_jz00_4141 = (int) (2L);
									BgL_vz00_4142 = (unsigned long) (BgL_arg1708z00_1987);
									{	/* Unsafe/aes.scm 426 */
										obj_t BgL_arg1498z00_4143;

										BgL_arg1498z00_4143 =
											VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4140));
										{	/* Unsafe/aes.scm 426 */
											uint8_t BgL_auxz00_7306;
											long BgL_tmpz00_7304;

											BgL_auxz00_7306 = (uint8_t) (BgL_vz00_4142);
											BgL_tmpz00_7304 = (long) (BgL_jz00_4141);
											BGL_U8VSET(BgL_arg1498z00_4143, BgL_tmpz00_7304,
												BgL_auxz00_7306);
										} BUNSPEC;
							}}}
							{	/* Unsafe/aes.scm 545 */
								long BgL_arg1710z00_1990;

								{	/* Unsafe/aes.scm 545 */
									uint8_t BgL_arg1711z00_1991;

									BgL_arg1711z00_1991 = BGL_U8VREF(BgL_tz00_1959, 3L);
									{	/* Unsafe/aes.scm 545 */
										long BgL_arg2398z00_4149;

										BgL_arg2398z00_4149 = (long) (BgL_arg1711z00_1991);
										BgL_arg1710z00_1990 = (long) (BgL_arg2398z00_4149);
								}}
								{	/* Unsafe/aes.scm 545 */
									int BgL_iz00_4152;
									int BgL_jz00_4153;
									unsigned long BgL_vz00_4154;

									BgL_iz00_4152 = (int) (BgL_rz00_1961);
									BgL_jz00_4153 = (int) (3L);
									BgL_vz00_4154 = (unsigned long) (BgL_arg1710z00_1990);
									{	/* Unsafe/aes.scm 426 */
										obj_t BgL_arg1498z00_4155;

										BgL_arg1498z00_4155 =
											VECTOR_REF(BgL_sz00_88, (long) (BgL_iz00_4152));
										{	/* Unsafe/aes.scm 426 */
											uint8_t BgL_auxz00_7319;
											long BgL_tmpz00_7317;

											BgL_auxz00_7319 = (uint8_t) (BgL_vz00_4154);
											BgL_tmpz00_7317 = (long) (BgL_jz00_4153);
											BGL_U8VSET(BgL_arg1498z00_4155, BgL_tmpz00_7317,
												BgL_auxz00_7319);
										} BUNSPEC;
							}}}
							{
								long BgL_rz00_7322;

								BgL_rz00_7322 = (BgL_rz00_1961 + 1L);
								BgL_rz00_1961 = BgL_rz00_7322;
								goto BgL_zc3z04anonymousza31679ze3z87_1962;
							}
						}
					else
						{	/* Unsafe/aes.scm 541 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* mixcolumns! */
	obj_t BGl_mixcolumnsz12z12zz__aesz00(obj_t BgL_sz00_90, long BgL_nbz00_91)
	{
		{	/* Unsafe/aes.scm 552 */
			{	/* Unsafe/aes.scm 554 */
				obj_t BgL_az00_1997;
				obj_t BgL_bz00_1998;

				{	/* Llib/srfi4.scm 447 */

					BgL_az00_1997 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Llib/srfi4.scm 447 */

					BgL_bz00_1998 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2000;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2543z00_4171;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4160;
							int BgL_jz00_4161;

							BgL_iz00_4160 = (int) (0L);
							BgL_jz00_4161 = (int) (0L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4162;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4163;

									BgL_arg1497z00_4163 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4160));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4167;

										BgL_kz00_4167 = (long) (BgL_jz00_4161);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7331;

											BgL_tmpz00_7331 = ((obj_t) BgL_arg1497z00_4163);
											BgL_arg1495z00_4162 =
												BGL_U8VREF(BgL_tmpz00_7331, BgL_kz00_4167);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4169;

									BgL_arg2398z00_4169 = (long) (BgL_arg1495z00_4162);
									BgL_res2543z00_4171 =
										(unsigned long) ((long) (BgL_arg2398z00_4169));
						}}}
						BgL_vz00_2000 = BgL_res2543z00_4171;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7337;

						BgL_tmpz00_7337 = (uint8_t) (BgL_vz00_2000);
						BGL_U8VSET(BgL_az00_1997, 0L, BgL_tmpz00_7337);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2000) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1717z00_2003;

							BgL_arg1717z00_2003 = ((long) (BgL_vz00_2000) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7347;

								BgL_tmpz00_7347 = (uint8_t) (BgL_arg1717z00_2003);
								BGL_U8VSET(BgL_bz00_1998, 0L, BgL_tmpz00_7347);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1718z00_2004;

							BgL_arg1718z00_2004 =
								(((long) (BgL_vz00_2000) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7354;

								BgL_tmpz00_7354 = (uint8_t) (BgL_arg1718z00_2004);
								BGL_U8VSET(BgL_bz00_1998, 0L, BgL_tmpz00_7354);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2008;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2544z00_4189;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4178;
							int BgL_jz00_4179;

							BgL_iz00_4178 = (int) (1L);
							BgL_jz00_4179 = (int) (0L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4180;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4181;

									BgL_arg1497z00_4181 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4178));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4185;

										BgL_kz00_4185 = (long) (BgL_jz00_4179);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7362;

											BgL_tmpz00_7362 = ((obj_t) BgL_arg1497z00_4181);
											BgL_arg1495z00_4180 =
												BGL_U8VREF(BgL_tmpz00_7362, BgL_kz00_4185);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4187;

									BgL_arg2398z00_4187 = (long) (BgL_arg1495z00_4180);
									BgL_res2544z00_4189 =
										(unsigned long) ((long) (BgL_arg2398z00_4187));
						}}}
						BgL_vz00_2008 = BgL_res2544z00_4189;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7368;

						BgL_tmpz00_7368 = (uint8_t) (BgL_vz00_2008);
						BGL_U8VSET(BgL_az00_1997, 1L, BgL_tmpz00_7368);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2008) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1723z00_2010;

							BgL_arg1723z00_2010 = ((long) (BgL_vz00_2008) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7378;

								BgL_tmpz00_7378 = (uint8_t) (BgL_arg1723z00_2010);
								BGL_U8VSET(BgL_bz00_1998, 1L, BgL_tmpz00_7378);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1724z00_2011;

							BgL_arg1724z00_2011 =
								(((long) (BgL_vz00_2008) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7385;

								BgL_tmpz00_7385 = (uint8_t) (BgL_arg1724z00_2011);
								BGL_U8VSET(BgL_bz00_1998, 1L, BgL_tmpz00_7385);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2015;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2545z00_4207;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4196;
							int BgL_jz00_4197;

							BgL_iz00_4196 = (int) (2L);
							BgL_jz00_4197 = (int) (0L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4198;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4199;

									BgL_arg1497z00_4199 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4196));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4203;

										BgL_kz00_4203 = (long) (BgL_jz00_4197);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7393;

											BgL_tmpz00_7393 = ((obj_t) BgL_arg1497z00_4199);
											BgL_arg1495z00_4198 =
												BGL_U8VREF(BgL_tmpz00_7393, BgL_kz00_4203);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4205;

									BgL_arg2398z00_4205 = (long) (BgL_arg1495z00_4198);
									BgL_res2545z00_4207 =
										(unsigned long) ((long) (BgL_arg2398z00_4205));
						}}}
						BgL_vz00_2015 = BgL_res2545z00_4207;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7399;

						BgL_tmpz00_7399 = (uint8_t) (BgL_vz00_2015);
						BGL_U8VSET(BgL_az00_1997, 2L, BgL_tmpz00_7399);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2015) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1727z00_2017;

							BgL_arg1727z00_2017 = ((long) (BgL_vz00_2015) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7409;

								BgL_tmpz00_7409 = (uint8_t) (BgL_arg1727z00_2017);
								BGL_U8VSET(BgL_bz00_1998, 2L, BgL_tmpz00_7409);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1728z00_2018;

							BgL_arg1728z00_2018 =
								(((long) (BgL_vz00_2015) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7416;

								BgL_tmpz00_7416 = (uint8_t) (BgL_arg1728z00_2018);
								BGL_U8VSET(BgL_bz00_1998, 2L, BgL_tmpz00_7416);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2022;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2546z00_4225;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4214;
							int BgL_jz00_4215;

							BgL_iz00_4214 = (int) (3L);
							BgL_jz00_4215 = (int) (0L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4216;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4217;

									BgL_arg1497z00_4217 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4214));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4221;

										BgL_kz00_4221 = (long) (BgL_jz00_4215);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7424;

											BgL_tmpz00_7424 = ((obj_t) BgL_arg1497z00_4217);
											BgL_arg1495z00_4216 =
												BGL_U8VREF(BgL_tmpz00_7424, BgL_kz00_4221);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4223;

									BgL_arg2398z00_4223 = (long) (BgL_arg1495z00_4216);
									BgL_res2546z00_4225 =
										(unsigned long) ((long) (BgL_arg2398z00_4223));
						}}}
						BgL_vz00_2022 = BgL_res2546z00_4225;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7430;

						BgL_tmpz00_7430 = (uint8_t) (BgL_vz00_2022);
						BGL_U8VSET(BgL_az00_1997, 3L, BgL_tmpz00_7430);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2022) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1731z00_2024;

							BgL_arg1731z00_2024 = ((long) (BgL_vz00_2022) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7440;

								BgL_tmpz00_7440 = (uint8_t) (BgL_arg1731z00_2024);
								BGL_U8VSET(BgL_bz00_1998, 3L, BgL_tmpz00_7440);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1733z00_2025;

							BgL_arg1733z00_2025 =
								(((long) (BgL_vz00_2022) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7447;

								BgL_tmpz00_7447 = (uint8_t) (BgL_arg1733z00_2025);
								BGL_U8VSET(BgL_bz00_1998, 3L, BgL_tmpz00_7447);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 562 */
					long BgL_arg1736z00_2028;

					{	/* Unsafe/aes.scm 562 */
						long BgL_arg1737z00_2029;
						long BgL_arg1738z00_2030;

						{	/* Unsafe/aes.scm 562 */
							uint8_t BgL_arg1739z00_2031;

							BgL_arg1739z00_2031 = BGL_U8VREF(BgL_bz00_1998, 0L);
							{	/* Unsafe/aes.scm 562 */
								long BgL_arg2398z00_4233;

								BgL_arg2398z00_4233 = (long) (BgL_arg1739z00_2031);
								BgL_arg1737z00_2029 = (long) (BgL_arg2398z00_4233);
						}}
						{	/* Unsafe/aes.scm 563 */
							long BgL_arg1740z00_2032;
							long BgL_arg1741z00_2033;

							{	/* Unsafe/aes.scm 563 */
								uint8_t BgL_arg1743z00_2034;

								BgL_arg1743z00_2034 = BGL_U8VREF(BgL_az00_1997, 1L);
								{	/* Unsafe/aes.scm 563 */
									long BgL_arg2398z00_4237;

									BgL_arg2398z00_4237 = (long) (BgL_arg1743z00_2034);
									BgL_arg1740z00_2032 = (long) (BgL_arg2398z00_4237);
							}}
							{	/* Unsafe/aes.scm 564 */
								long BgL_arg1744z00_2035;
								long BgL_arg1745z00_2036;

								{	/* Unsafe/aes.scm 564 */
									uint8_t BgL_arg1746z00_2037;

									BgL_arg1746z00_2037 = BGL_U8VREF(BgL_bz00_1998, 1L);
									{	/* Unsafe/aes.scm 564 */
										long BgL_arg2398z00_4241;

										BgL_arg2398z00_4241 = (long) (BgL_arg1746z00_2037);
										BgL_arg1744z00_2035 = (long) (BgL_arg2398z00_4241);
								}}
								{	/* Unsafe/aes.scm 565 */
									long BgL_arg1747z00_2038;
									long BgL_arg1748z00_2039;

									{	/* Unsafe/aes.scm 565 */
										uint8_t BgL_arg1749z00_2040;

										BgL_arg1749z00_2040 = BGL_U8VREF(BgL_az00_1997, 2L);
										{	/* Unsafe/aes.scm 565 */
											long BgL_arg2398z00_4245;

											BgL_arg2398z00_4245 = (long) (BgL_arg1749z00_2040);
											BgL_arg1747z00_2038 = (long) (BgL_arg2398z00_4245);
									}}
									{	/* Unsafe/aes.scm 566 */
										uint8_t BgL_arg1750z00_2041;

										BgL_arg1750z00_2041 = BGL_U8VREF(BgL_az00_1997, 3L);
										{	/* Unsafe/aes.scm 566 */
											long BgL_arg2398z00_4249;

											BgL_arg2398z00_4249 = (long) (BgL_arg1750z00_2041);
											BgL_arg1748z00_2039 = (long) (BgL_arg2398z00_4249);
									}}
									BgL_arg1745z00_2036 =
										(BgL_arg1747z00_2038 ^ BgL_arg1748z00_2039);
								}
								BgL_arg1741z00_2033 =
									(BgL_arg1744z00_2035 ^ BgL_arg1745z00_2036);
							}
							BgL_arg1738z00_2030 = (BgL_arg1740z00_2032 ^ BgL_arg1741z00_2033);
						}
						BgL_arg1736z00_2028 = (BgL_arg1737z00_2029 ^ BgL_arg1738z00_2030);
					}
					{	/* Unsafe/aes.scm 562 */
						int BgL_jz00_4260;
						unsigned long BgL_vz00_4261;

						BgL_jz00_4260 = (int) (0L);
						BgL_vz00_4261 = (unsigned long) (BgL_arg1736z00_2028);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4262;

							BgL_arg1498z00_4262 = VECTOR_REF(BgL_sz00_90, 0L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7474;
								long BgL_tmpz00_7472;

								BgL_auxz00_7474 = (uint8_t) (BgL_vz00_4261);
								BgL_tmpz00_7472 = (long) (BgL_jz00_4260);
								BGL_U8VSET(BgL_arg1498z00_4262, BgL_tmpz00_7472,
									BgL_auxz00_7474);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 567 */
					long BgL_arg1751z00_2042;

					{	/* Unsafe/aes.scm 567 */
						long BgL_arg1752z00_2043;
						long BgL_arg1753z00_2044;

						{	/* Unsafe/aes.scm 567 */
							uint8_t BgL_arg1754z00_2045;

							BgL_arg1754z00_2045 = BGL_U8VREF(BgL_az00_1997, 0L);
							{	/* Unsafe/aes.scm 567 */
								long BgL_arg2398z00_4266;

								BgL_arg2398z00_4266 = (long) (BgL_arg1754z00_2045);
								BgL_arg1752z00_2043 = (long) (BgL_arg2398z00_4266);
						}}
						{	/* Unsafe/aes.scm 568 */
							long BgL_arg1755z00_2046;
							long BgL_arg1756z00_2047;

							{	/* Unsafe/aes.scm 568 */
								uint8_t BgL_arg1757z00_2048;

								BgL_arg1757z00_2048 = BGL_U8VREF(BgL_bz00_1998, 1L);
								{	/* Unsafe/aes.scm 568 */
									long BgL_arg2398z00_4270;

									BgL_arg2398z00_4270 = (long) (BgL_arg1757z00_2048);
									BgL_arg1755z00_2046 = (long) (BgL_arg2398z00_4270);
							}}
							{	/* Unsafe/aes.scm 569 */
								long BgL_arg1758z00_2049;
								long BgL_arg1759z00_2050;

								{	/* Unsafe/aes.scm 569 */
									uint8_t BgL_arg1760z00_2051;

									BgL_arg1760z00_2051 = BGL_U8VREF(BgL_az00_1997, 2L);
									{	/* Unsafe/aes.scm 569 */
										long BgL_arg2398z00_4274;

										BgL_arg2398z00_4274 = (long) (BgL_arg1760z00_2051);
										BgL_arg1758z00_2049 = (long) (BgL_arg2398z00_4274);
								}}
								{	/* Unsafe/aes.scm 570 */
									long BgL_arg1761z00_2052;
									long BgL_arg1762z00_2053;

									{	/* Unsafe/aes.scm 570 */
										uint8_t BgL_arg1763z00_2054;

										BgL_arg1763z00_2054 = BGL_U8VREF(BgL_bz00_1998, 2L);
										{	/* Unsafe/aes.scm 570 */
											long BgL_arg2398z00_4278;

											BgL_arg2398z00_4278 = (long) (BgL_arg1763z00_2054);
											BgL_arg1761z00_2052 = (long) (BgL_arg2398z00_4278);
									}}
									{	/* Unsafe/aes.scm 571 */
										uint8_t BgL_arg1764z00_2055;

										BgL_arg1764z00_2055 = BGL_U8VREF(BgL_az00_1997, 3L);
										{	/* Unsafe/aes.scm 571 */
											long BgL_arg2398z00_4282;

											BgL_arg2398z00_4282 = (long) (BgL_arg1764z00_2055);
											BgL_arg1762z00_2053 = (long) (BgL_arg2398z00_4282);
									}}
									BgL_arg1759z00_2050 =
										(BgL_arg1761z00_2052 ^ BgL_arg1762z00_2053);
								}
								BgL_arg1756z00_2047 =
									(BgL_arg1758z00_2049 ^ BgL_arg1759z00_2050);
							}
							BgL_arg1753z00_2044 = (BgL_arg1755z00_2046 ^ BgL_arg1756z00_2047);
						}
						BgL_arg1751z00_2042 = (BgL_arg1752z00_2043 ^ BgL_arg1753z00_2044);
					}
					{	/* Unsafe/aes.scm 567 */
						int BgL_jz00_4293;
						unsigned long BgL_vz00_4294;

						BgL_jz00_4293 = (int) (0L);
						BgL_vz00_4294 = (unsigned long) (BgL_arg1751z00_2042);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4295;

							BgL_arg1498z00_4295 = VECTOR_REF(BgL_sz00_90, 1L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7501;
								long BgL_tmpz00_7499;

								BgL_auxz00_7501 = (uint8_t) (BgL_vz00_4294);
								BgL_tmpz00_7499 = (long) (BgL_jz00_4293);
								BGL_U8VSET(BgL_arg1498z00_4295, BgL_tmpz00_7499,
									BgL_auxz00_7501);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 572 */
					long BgL_arg1765z00_2056;

					{	/* Unsafe/aes.scm 572 */
						long BgL_arg1766z00_2057;
						long BgL_arg1767z00_2058;

						{	/* Unsafe/aes.scm 572 */
							uint8_t BgL_arg1768z00_2059;

							BgL_arg1768z00_2059 = BGL_U8VREF(BgL_az00_1997, 0L);
							{	/* Unsafe/aes.scm 572 */
								long BgL_arg2398z00_4299;

								BgL_arg2398z00_4299 = (long) (BgL_arg1768z00_2059);
								BgL_arg1766z00_2057 = (long) (BgL_arg2398z00_4299);
						}}
						{	/* Unsafe/aes.scm 573 */
							long BgL_arg1769z00_2060;
							long BgL_arg1770z00_2061;

							{	/* Unsafe/aes.scm 573 */
								uint8_t BgL_arg1771z00_2062;

								BgL_arg1771z00_2062 = BGL_U8VREF(BgL_az00_1997, 1L);
								{	/* Unsafe/aes.scm 573 */
									long BgL_arg2398z00_4303;

									BgL_arg2398z00_4303 = (long) (BgL_arg1771z00_2062);
									BgL_arg1769z00_2060 = (long) (BgL_arg2398z00_4303);
							}}
							{	/* Unsafe/aes.scm 574 */
								long BgL_arg1772z00_2063;
								long BgL_arg1773z00_2064;

								{	/* Unsafe/aes.scm 574 */
									uint8_t BgL_arg1774z00_2065;

									BgL_arg1774z00_2065 = BGL_U8VREF(BgL_bz00_1998, 2L);
									{	/* Unsafe/aes.scm 574 */
										long BgL_arg2398z00_4307;

										BgL_arg2398z00_4307 = (long) (BgL_arg1774z00_2065);
										BgL_arg1772z00_2063 = (long) (BgL_arg2398z00_4307);
								}}
								{	/* Unsafe/aes.scm 575 */
									long BgL_arg1775z00_2066;
									long BgL_arg1777z00_2067;

									{	/* Unsafe/aes.scm 575 */
										uint8_t BgL_arg1779z00_2068;

										BgL_arg1779z00_2068 = BGL_U8VREF(BgL_az00_1997, 3L);
										{	/* Unsafe/aes.scm 575 */
											long BgL_arg2398z00_4311;

											BgL_arg2398z00_4311 = (long) (BgL_arg1779z00_2068);
											BgL_arg1775z00_2066 = (long) (BgL_arg2398z00_4311);
									}}
									{	/* Unsafe/aes.scm 576 */
										uint8_t BgL_arg1781z00_2069;

										BgL_arg1781z00_2069 = BGL_U8VREF(BgL_bz00_1998, 3L);
										{	/* Unsafe/aes.scm 576 */
											long BgL_arg2398z00_4315;

											BgL_arg2398z00_4315 = (long) (BgL_arg1781z00_2069);
											BgL_arg1777z00_2067 = (long) (BgL_arg2398z00_4315);
									}}
									BgL_arg1773z00_2064 =
										(BgL_arg1775z00_2066 ^ BgL_arg1777z00_2067);
								}
								BgL_arg1770z00_2061 =
									(BgL_arg1772z00_2063 ^ BgL_arg1773z00_2064);
							}
							BgL_arg1767z00_2058 = (BgL_arg1769z00_2060 ^ BgL_arg1770z00_2061);
						}
						BgL_arg1765z00_2056 = (BgL_arg1766z00_2057 ^ BgL_arg1767z00_2058);
					}
					{	/* Unsafe/aes.scm 572 */
						int BgL_jz00_4326;
						unsigned long BgL_vz00_4327;

						BgL_jz00_4326 = (int) (0L);
						BgL_vz00_4327 = (unsigned long) (BgL_arg1765z00_2056);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4328;

							BgL_arg1498z00_4328 = VECTOR_REF(BgL_sz00_90, 2L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7528;
								long BgL_tmpz00_7526;

								BgL_auxz00_7528 = (uint8_t) (BgL_vz00_4327);
								BgL_tmpz00_7526 = (long) (BgL_jz00_4326);
								BGL_U8VSET(BgL_arg1498z00_4328, BgL_tmpz00_7526,
									BgL_auxz00_7528);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 577 */
					long BgL_arg1782z00_2070;

					{	/* Unsafe/aes.scm 577 */
						long BgL_arg1783z00_2071;
						long BgL_arg1785z00_2072;

						{	/* Unsafe/aes.scm 577 */
							uint8_t BgL_arg1786z00_2073;

							BgL_arg1786z00_2073 = BGL_U8VREF(BgL_az00_1997, 0L);
							{	/* Unsafe/aes.scm 577 */
								long BgL_arg2398z00_4332;

								BgL_arg2398z00_4332 = (long) (BgL_arg1786z00_2073);
								BgL_arg1783z00_2071 = (long) (BgL_arg2398z00_4332);
						}}
						{	/* Unsafe/aes.scm 578 */
							long BgL_arg1787z00_2074;
							long BgL_arg1788z00_2075;

							{	/* Unsafe/aes.scm 578 */
								uint8_t BgL_arg1789z00_2076;

								BgL_arg1789z00_2076 = BGL_U8VREF(BgL_bz00_1998, 0L);
								{	/* Unsafe/aes.scm 578 */
									long BgL_arg2398z00_4336;

									BgL_arg2398z00_4336 = (long) (BgL_arg1789z00_2076);
									BgL_arg1787z00_2074 = (long) (BgL_arg2398z00_4336);
							}}
							{	/* Unsafe/aes.scm 579 */
								long BgL_arg1790z00_2077;
								long BgL_arg1791z00_2078;

								{	/* Unsafe/aes.scm 579 */
									uint8_t BgL_arg1792z00_2079;

									BgL_arg1792z00_2079 = BGL_U8VREF(BgL_az00_1997, 1L);
									{	/* Unsafe/aes.scm 579 */
										long BgL_arg2398z00_4340;

										BgL_arg2398z00_4340 = (long) (BgL_arg1792z00_2079);
										BgL_arg1790z00_2077 = (long) (BgL_arg2398z00_4340);
								}}
								{	/* Unsafe/aes.scm 580 */
									long BgL_arg1793z00_2080;
									long BgL_arg1794z00_2081;

									{	/* Unsafe/aes.scm 580 */
										uint8_t BgL_arg1795z00_2082;

										BgL_arg1795z00_2082 = BGL_U8VREF(BgL_az00_1997, 2L);
										{	/* Unsafe/aes.scm 580 */
											long BgL_arg2398z00_4344;

											BgL_arg2398z00_4344 = (long) (BgL_arg1795z00_2082);
											BgL_arg1793z00_2080 = (long) (BgL_arg2398z00_4344);
									}}
									{	/* Unsafe/aes.scm 581 */
										uint8_t BgL_arg1796z00_2083;

										BgL_arg1796z00_2083 = BGL_U8VREF(BgL_bz00_1998, 3L);
										{	/* Unsafe/aes.scm 581 */
											long BgL_arg2398z00_4348;

											BgL_arg2398z00_4348 = (long) (BgL_arg1796z00_2083);
											BgL_arg1794z00_2081 = (long) (BgL_arg2398z00_4348);
									}}
									BgL_arg1791z00_2078 =
										(BgL_arg1793z00_2080 ^ BgL_arg1794z00_2081);
								}
								BgL_arg1788z00_2075 =
									(BgL_arg1790z00_2077 ^ BgL_arg1791z00_2078);
							}
							BgL_arg1785z00_2072 = (BgL_arg1787z00_2074 ^ BgL_arg1788z00_2075);
						}
						BgL_arg1782z00_2070 = (BgL_arg1783z00_2071 ^ BgL_arg1785z00_2072);
					}
					{	/* Unsafe/aes.scm 577 */
						int BgL_jz00_4359;
						unsigned long BgL_vz00_4360;

						BgL_jz00_4359 = (int) (0L);
						BgL_vz00_4360 = (unsigned long) (BgL_arg1782z00_2070);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4361;

							BgL_arg1498z00_4361 = VECTOR_REF(BgL_sz00_90, 3L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7555;
								long BgL_tmpz00_7553;

								BgL_auxz00_7555 = (uint8_t) (BgL_vz00_4360);
								BgL_tmpz00_7553 = (long) (BgL_jz00_4359);
								BGL_U8VSET(BgL_arg1498z00_4361, BgL_tmpz00_7553,
									BgL_auxz00_7555);
							} BUNSPEC;
			}}}}
			{	/* Unsafe/aes.scm 554 */
				obj_t BgL_az00_2089;
				obj_t BgL_bz00_2090;

				{	/* Llib/srfi4.scm 447 */

					BgL_az00_2089 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Llib/srfi4.scm 447 */

					BgL_bz00_2090 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2092;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2547z00_4375;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4364;
							int BgL_jz00_4365;

							BgL_iz00_4364 = (int) (0L);
							BgL_jz00_4365 = (int) (1L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4366;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4367;

									BgL_arg1497z00_4367 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4364));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4371;

										BgL_kz00_4371 = (long) (BgL_jz00_4365);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7565;

											BgL_tmpz00_7565 = ((obj_t) BgL_arg1497z00_4367);
											BgL_arg1495z00_4366 =
												BGL_U8VREF(BgL_tmpz00_7565, BgL_kz00_4371);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4373;

									BgL_arg2398z00_4373 = (long) (BgL_arg1495z00_4366);
									BgL_res2547z00_4375 =
										(unsigned long) ((long) (BgL_arg2398z00_4373));
						}}}
						BgL_vz00_2092 = BgL_res2547z00_4375;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7571;

						BgL_tmpz00_7571 = (uint8_t) (BgL_vz00_2092);
						BGL_U8VSET(BgL_az00_2089, 0L, BgL_tmpz00_7571);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2092) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1797z00_2094;

							BgL_arg1797z00_2094 = ((long) (BgL_vz00_2092) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7581;

								BgL_tmpz00_7581 = (uint8_t) (BgL_arg1797z00_2094);
								BGL_U8VSET(BgL_bz00_2090, 0L, BgL_tmpz00_7581);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1798z00_2095;

							BgL_arg1798z00_2095 =
								(((long) (BgL_vz00_2092) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7588;

								BgL_tmpz00_7588 = (uint8_t) (BgL_arg1798z00_2095);
								BGL_U8VSET(BgL_bz00_2090, 0L, BgL_tmpz00_7588);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2099;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2548z00_4393;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4382;
							int BgL_jz00_4383;

							BgL_iz00_4382 = (int) (1L);
							BgL_jz00_4383 = (int) (1L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4384;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4385;

									BgL_arg1497z00_4385 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4382));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4389;

										BgL_kz00_4389 = (long) (BgL_jz00_4383);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7596;

											BgL_tmpz00_7596 = ((obj_t) BgL_arg1497z00_4385);
											BgL_arg1495z00_4384 =
												BGL_U8VREF(BgL_tmpz00_7596, BgL_kz00_4389);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4391;

									BgL_arg2398z00_4391 = (long) (BgL_arg1495z00_4384);
									BgL_res2548z00_4393 =
										(unsigned long) ((long) (BgL_arg2398z00_4391));
						}}}
						BgL_vz00_2099 = BgL_res2548z00_4393;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7602;

						BgL_tmpz00_7602 = (uint8_t) (BgL_vz00_2099);
						BGL_U8VSET(BgL_az00_2089, 1L, BgL_tmpz00_7602);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2099) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1801z00_2101;

							BgL_arg1801z00_2101 = ((long) (BgL_vz00_2099) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7612;

								BgL_tmpz00_7612 = (uint8_t) (BgL_arg1801z00_2101);
								BGL_U8VSET(BgL_bz00_2090, 1L, BgL_tmpz00_7612);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1802z00_2102;

							BgL_arg1802z00_2102 =
								(((long) (BgL_vz00_2099) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7619;

								BgL_tmpz00_7619 = (uint8_t) (BgL_arg1802z00_2102);
								BGL_U8VSET(BgL_bz00_2090, 1L, BgL_tmpz00_7619);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2106;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2549z00_4411;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4400;
							int BgL_jz00_4401;

							BgL_iz00_4400 = (int) (2L);
							BgL_jz00_4401 = (int) (1L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4402;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4403;

									BgL_arg1497z00_4403 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4400));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4407;

										BgL_kz00_4407 = (long) (BgL_jz00_4401);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7627;

											BgL_tmpz00_7627 = ((obj_t) BgL_arg1497z00_4403);
											BgL_arg1495z00_4402 =
												BGL_U8VREF(BgL_tmpz00_7627, BgL_kz00_4407);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4409;

									BgL_arg2398z00_4409 = (long) (BgL_arg1495z00_4402);
									BgL_res2549z00_4411 =
										(unsigned long) ((long) (BgL_arg2398z00_4409));
						}}}
						BgL_vz00_2106 = BgL_res2549z00_4411;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7633;

						BgL_tmpz00_7633 = (uint8_t) (BgL_vz00_2106);
						BGL_U8VSET(BgL_az00_2089, 2L, BgL_tmpz00_7633);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2106) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1805z00_2108;

							BgL_arg1805z00_2108 = ((long) (BgL_vz00_2106) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7643;

								BgL_tmpz00_7643 = (uint8_t) (BgL_arg1805z00_2108);
								BGL_U8VSET(BgL_bz00_2090, 2L, BgL_tmpz00_7643);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1806z00_2109;

							BgL_arg1806z00_2109 =
								(((long) (BgL_vz00_2106) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7650;

								BgL_tmpz00_7650 = (uint8_t) (BgL_arg1806z00_2109);
								BGL_U8VSET(BgL_bz00_2090, 2L, BgL_tmpz00_7650);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2113;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2550z00_4429;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4418;
							int BgL_jz00_4419;

							BgL_iz00_4418 = (int) (3L);
							BgL_jz00_4419 = (int) (1L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4420;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4421;

									BgL_arg1497z00_4421 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4418));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4425;

										BgL_kz00_4425 = (long) (BgL_jz00_4419);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7658;

											BgL_tmpz00_7658 = ((obj_t) BgL_arg1497z00_4421);
											BgL_arg1495z00_4420 =
												BGL_U8VREF(BgL_tmpz00_7658, BgL_kz00_4425);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4427;

									BgL_arg2398z00_4427 = (long) (BgL_arg1495z00_4420);
									BgL_res2550z00_4429 =
										(unsigned long) ((long) (BgL_arg2398z00_4427));
						}}}
						BgL_vz00_2113 = BgL_res2550z00_4429;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7664;

						BgL_tmpz00_7664 = (uint8_t) (BgL_vz00_2113);
						BGL_U8VSET(BgL_az00_2089, 3L, BgL_tmpz00_7664);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2113) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1809z00_2115;

							BgL_arg1809z00_2115 = ((long) (BgL_vz00_2113) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7674;

								BgL_tmpz00_7674 = (uint8_t) (BgL_arg1809z00_2115);
								BGL_U8VSET(BgL_bz00_2090, 3L, BgL_tmpz00_7674);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1810z00_2116;

							BgL_arg1810z00_2116 =
								(((long) (BgL_vz00_2113) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7681;

								BgL_tmpz00_7681 = (uint8_t) (BgL_arg1810z00_2116);
								BGL_U8VSET(BgL_bz00_2090, 3L, BgL_tmpz00_7681);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 562 */
					long BgL_arg1813z00_2119;

					{	/* Unsafe/aes.scm 562 */
						long BgL_arg1814z00_2120;
						long BgL_arg1815z00_2121;

						{	/* Unsafe/aes.scm 562 */
							uint8_t BgL_arg1816z00_2122;

							BgL_arg1816z00_2122 = BGL_U8VREF(BgL_bz00_2090, 0L);
							{	/* Unsafe/aes.scm 562 */
								long BgL_arg2398z00_4437;

								BgL_arg2398z00_4437 = (long) (BgL_arg1816z00_2122);
								BgL_arg1814z00_2120 = (long) (BgL_arg2398z00_4437);
						}}
						{	/* Unsafe/aes.scm 563 */
							long BgL_arg1817z00_2123;
							long BgL_arg1818z00_2124;

							{	/* Unsafe/aes.scm 563 */
								uint8_t BgL_arg1819z00_2125;

								BgL_arg1819z00_2125 = BGL_U8VREF(BgL_az00_2089, 1L);
								{	/* Unsafe/aes.scm 563 */
									long BgL_arg2398z00_4441;

									BgL_arg2398z00_4441 = (long) (BgL_arg1819z00_2125);
									BgL_arg1817z00_2123 = (long) (BgL_arg2398z00_4441);
							}}
							{	/* Unsafe/aes.scm 564 */
								long BgL_arg1820z00_2126;
								long BgL_arg1822z00_2127;

								{	/* Unsafe/aes.scm 564 */
									uint8_t BgL_arg1823z00_2128;

									BgL_arg1823z00_2128 = BGL_U8VREF(BgL_bz00_2090, 1L);
									{	/* Unsafe/aes.scm 564 */
										long BgL_arg2398z00_4445;

										BgL_arg2398z00_4445 = (long) (BgL_arg1823z00_2128);
										BgL_arg1820z00_2126 = (long) (BgL_arg2398z00_4445);
								}}
								{	/* Unsafe/aes.scm 565 */
									long BgL_arg1826z00_2129;
									long BgL_arg1827z00_2130;

									{	/* Unsafe/aes.scm 565 */
										uint8_t BgL_arg1828z00_2131;

										BgL_arg1828z00_2131 = BGL_U8VREF(BgL_az00_2089, 2L);
										{	/* Unsafe/aes.scm 565 */
											long BgL_arg2398z00_4449;

											BgL_arg2398z00_4449 = (long) (BgL_arg1828z00_2131);
											BgL_arg1826z00_2129 = (long) (BgL_arg2398z00_4449);
									}}
									{	/* Unsafe/aes.scm 566 */
										uint8_t BgL_arg1829z00_2132;

										BgL_arg1829z00_2132 = BGL_U8VREF(BgL_az00_2089, 3L);
										{	/* Unsafe/aes.scm 566 */
											long BgL_arg2398z00_4453;

											BgL_arg2398z00_4453 = (long) (BgL_arg1829z00_2132);
											BgL_arg1827z00_2130 = (long) (BgL_arg2398z00_4453);
									}}
									BgL_arg1822z00_2127 =
										(BgL_arg1826z00_2129 ^ BgL_arg1827z00_2130);
								}
								BgL_arg1818z00_2124 =
									(BgL_arg1820z00_2126 ^ BgL_arg1822z00_2127);
							}
							BgL_arg1815z00_2121 = (BgL_arg1817z00_2123 ^ BgL_arg1818z00_2124);
						}
						BgL_arg1813z00_2119 = (BgL_arg1814z00_2120 ^ BgL_arg1815z00_2121);
					}
					{	/* Unsafe/aes.scm 562 */
						int BgL_jz00_4464;
						unsigned long BgL_vz00_4465;

						BgL_jz00_4464 = (int) (1L);
						BgL_vz00_4465 = (unsigned long) (BgL_arg1813z00_2119);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4466;

							BgL_arg1498z00_4466 = VECTOR_REF(BgL_sz00_90, 0L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7708;
								long BgL_tmpz00_7706;

								BgL_auxz00_7708 = (uint8_t) (BgL_vz00_4465);
								BgL_tmpz00_7706 = (long) (BgL_jz00_4464);
								BGL_U8VSET(BgL_arg1498z00_4466, BgL_tmpz00_7706,
									BgL_auxz00_7708);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 567 */
					long BgL_arg1831z00_2133;

					{	/* Unsafe/aes.scm 567 */
						long BgL_arg1832z00_2134;
						long BgL_arg1833z00_2135;

						{	/* Unsafe/aes.scm 567 */
							uint8_t BgL_arg1834z00_2136;

							BgL_arg1834z00_2136 = BGL_U8VREF(BgL_az00_2089, 0L);
							{	/* Unsafe/aes.scm 567 */
								long BgL_arg2398z00_4470;

								BgL_arg2398z00_4470 = (long) (BgL_arg1834z00_2136);
								BgL_arg1832z00_2134 = (long) (BgL_arg2398z00_4470);
						}}
						{	/* Unsafe/aes.scm 568 */
							long BgL_arg1835z00_2137;
							long BgL_arg1836z00_2138;

							{	/* Unsafe/aes.scm 568 */
								uint8_t BgL_arg1837z00_2139;

								BgL_arg1837z00_2139 = BGL_U8VREF(BgL_bz00_2090, 1L);
								{	/* Unsafe/aes.scm 568 */
									long BgL_arg2398z00_4474;

									BgL_arg2398z00_4474 = (long) (BgL_arg1837z00_2139);
									BgL_arg1835z00_2137 = (long) (BgL_arg2398z00_4474);
							}}
							{	/* Unsafe/aes.scm 569 */
								long BgL_arg1838z00_2140;
								long BgL_arg1839z00_2141;

								{	/* Unsafe/aes.scm 569 */
									uint8_t BgL_arg1840z00_2142;

									BgL_arg1840z00_2142 = BGL_U8VREF(BgL_az00_2089, 2L);
									{	/* Unsafe/aes.scm 569 */
										long BgL_arg2398z00_4478;

										BgL_arg2398z00_4478 = (long) (BgL_arg1840z00_2142);
										BgL_arg1838z00_2140 = (long) (BgL_arg2398z00_4478);
								}}
								{	/* Unsafe/aes.scm 570 */
									long BgL_arg1842z00_2143;
									long BgL_arg1843z00_2144;

									{	/* Unsafe/aes.scm 570 */
										uint8_t BgL_arg1844z00_2145;

										BgL_arg1844z00_2145 = BGL_U8VREF(BgL_bz00_2090, 2L);
										{	/* Unsafe/aes.scm 570 */
											long BgL_arg2398z00_4482;

											BgL_arg2398z00_4482 = (long) (BgL_arg1844z00_2145);
											BgL_arg1842z00_2143 = (long) (BgL_arg2398z00_4482);
									}}
									{	/* Unsafe/aes.scm 571 */
										uint8_t BgL_arg1845z00_2146;

										BgL_arg1845z00_2146 = BGL_U8VREF(BgL_az00_2089, 3L);
										{	/* Unsafe/aes.scm 571 */
											long BgL_arg2398z00_4486;

											BgL_arg2398z00_4486 = (long) (BgL_arg1845z00_2146);
											BgL_arg1843z00_2144 = (long) (BgL_arg2398z00_4486);
									}}
									BgL_arg1839z00_2141 =
										(BgL_arg1842z00_2143 ^ BgL_arg1843z00_2144);
								}
								BgL_arg1836z00_2138 =
									(BgL_arg1838z00_2140 ^ BgL_arg1839z00_2141);
							}
							BgL_arg1833z00_2135 = (BgL_arg1835z00_2137 ^ BgL_arg1836z00_2138);
						}
						BgL_arg1831z00_2133 = (BgL_arg1832z00_2134 ^ BgL_arg1833z00_2135);
					}
					{	/* Unsafe/aes.scm 567 */
						int BgL_jz00_4497;
						unsigned long BgL_vz00_4498;

						BgL_jz00_4497 = (int) (1L);
						BgL_vz00_4498 = (unsigned long) (BgL_arg1831z00_2133);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4499;

							BgL_arg1498z00_4499 = VECTOR_REF(BgL_sz00_90, 1L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7735;
								long BgL_tmpz00_7733;

								BgL_auxz00_7735 = (uint8_t) (BgL_vz00_4498);
								BgL_tmpz00_7733 = (long) (BgL_jz00_4497);
								BGL_U8VSET(BgL_arg1498z00_4499, BgL_tmpz00_7733,
									BgL_auxz00_7735);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 572 */
					long BgL_arg1846z00_2147;

					{	/* Unsafe/aes.scm 572 */
						long BgL_arg1847z00_2148;
						long BgL_arg1848z00_2149;

						{	/* Unsafe/aes.scm 572 */
							uint8_t BgL_arg1849z00_2150;

							BgL_arg1849z00_2150 = BGL_U8VREF(BgL_az00_2089, 0L);
							{	/* Unsafe/aes.scm 572 */
								long BgL_arg2398z00_4503;

								BgL_arg2398z00_4503 = (long) (BgL_arg1849z00_2150);
								BgL_arg1847z00_2148 = (long) (BgL_arg2398z00_4503);
						}}
						{	/* Unsafe/aes.scm 573 */
							long BgL_arg1850z00_2151;
							long BgL_arg1851z00_2152;

							{	/* Unsafe/aes.scm 573 */
								uint8_t BgL_arg1852z00_2153;

								BgL_arg1852z00_2153 = BGL_U8VREF(BgL_az00_2089, 1L);
								{	/* Unsafe/aes.scm 573 */
									long BgL_arg2398z00_4507;

									BgL_arg2398z00_4507 = (long) (BgL_arg1852z00_2153);
									BgL_arg1850z00_2151 = (long) (BgL_arg2398z00_4507);
							}}
							{	/* Unsafe/aes.scm 574 */
								long BgL_arg1853z00_2154;
								long BgL_arg1854z00_2155;

								{	/* Unsafe/aes.scm 574 */
									uint8_t BgL_arg1856z00_2156;

									BgL_arg1856z00_2156 = BGL_U8VREF(BgL_bz00_2090, 2L);
									{	/* Unsafe/aes.scm 574 */
										long BgL_arg2398z00_4511;

										BgL_arg2398z00_4511 = (long) (BgL_arg1856z00_2156);
										BgL_arg1853z00_2154 = (long) (BgL_arg2398z00_4511);
								}}
								{	/* Unsafe/aes.scm 575 */
									long BgL_arg1857z00_2157;
									long BgL_arg1858z00_2158;

									{	/* Unsafe/aes.scm 575 */
										uint8_t BgL_arg1859z00_2159;

										BgL_arg1859z00_2159 = BGL_U8VREF(BgL_az00_2089, 3L);
										{	/* Unsafe/aes.scm 575 */
											long BgL_arg2398z00_4515;

											BgL_arg2398z00_4515 = (long) (BgL_arg1859z00_2159);
											BgL_arg1857z00_2157 = (long) (BgL_arg2398z00_4515);
									}}
									{	/* Unsafe/aes.scm 576 */
										uint8_t BgL_arg1860z00_2160;

										BgL_arg1860z00_2160 = BGL_U8VREF(BgL_bz00_2090, 3L);
										{	/* Unsafe/aes.scm 576 */
											long BgL_arg2398z00_4519;

											BgL_arg2398z00_4519 = (long) (BgL_arg1860z00_2160);
											BgL_arg1858z00_2158 = (long) (BgL_arg2398z00_4519);
									}}
									BgL_arg1854z00_2155 =
										(BgL_arg1857z00_2157 ^ BgL_arg1858z00_2158);
								}
								BgL_arg1851z00_2152 =
									(BgL_arg1853z00_2154 ^ BgL_arg1854z00_2155);
							}
							BgL_arg1848z00_2149 = (BgL_arg1850z00_2151 ^ BgL_arg1851z00_2152);
						}
						BgL_arg1846z00_2147 = (BgL_arg1847z00_2148 ^ BgL_arg1848z00_2149);
					}
					{	/* Unsafe/aes.scm 572 */
						int BgL_jz00_4530;
						unsigned long BgL_vz00_4531;

						BgL_jz00_4530 = (int) (1L);
						BgL_vz00_4531 = (unsigned long) (BgL_arg1846z00_2147);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4532;

							BgL_arg1498z00_4532 = VECTOR_REF(BgL_sz00_90, 2L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7762;
								long BgL_tmpz00_7760;

								BgL_auxz00_7762 = (uint8_t) (BgL_vz00_4531);
								BgL_tmpz00_7760 = (long) (BgL_jz00_4530);
								BGL_U8VSET(BgL_arg1498z00_4532, BgL_tmpz00_7760,
									BgL_auxz00_7762);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 577 */
					long BgL_arg1862z00_2161;

					{	/* Unsafe/aes.scm 577 */
						long BgL_arg1863z00_2162;
						long BgL_arg1864z00_2163;

						{	/* Unsafe/aes.scm 577 */
							uint8_t BgL_arg1866z00_2164;

							BgL_arg1866z00_2164 = BGL_U8VREF(BgL_az00_2089, 0L);
							{	/* Unsafe/aes.scm 577 */
								long BgL_arg2398z00_4536;

								BgL_arg2398z00_4536 = (long) (BgL_arg1866z00_2164);
								BgL_arg1863z00_2162 = (long) (BgL_arg2398z00_4536);
						}}
						{	/* Unsafe/aes.scm 578 */
							long BgL_arg1868z00_2165;
							long BgL_arg1869z00_2166;

							{	/* Unsafe/aes.scm 578 */
								uint8_t BgL_arg1870z00_2167;

								BgL_arg1870z00_2167 = BGL_U8VREF(BgL_bz00_2090, 0L);
								{	/* Unsafe/aes.scm 578 */
									long BgL_arg2398z00_4540;

									BgL_arg2398z00_4540 = (long) (BgL_arg1870z00_2167);
									BgL_arg1868z00_2165 = (long) (BgL_arg2398z00_4540);
							}}
							{	/* Unsafe/aes.scm 579 */
								long BgL_arg1872z00_2168;
								long BgL_arg1873z00_2169;

								{	/* Unsafe/aes.scm 579 */
									uint8_t BgL_arg1874z00_2170;

									BgL_arg1874z00_2170 = BGL_U8VREF(BgL_az00_2089, 1L);
									{	/* Unsafe/aes.scm 579 */
										long BgL_arg2398z00_4544;

										BgL_arg2398z00_4544 = (long) (BgL_arg1874z00_2170);
										BgL_arg1872z00_2168 = (long) (BgL_arg2398z00_4544);
								}}
								{	/* Unsafe/aes.scm 580 */
									long BgL_arg1875z00_2171;
									long BgL_arg1876z00_2172;

									{	/* Unsafe/aes.scm 580 */
										uint8_t BgL_arg1877z00_2173;

										BgL_arg1877z00_2173 = BGL_U8VREF(BgL_az00_2089, 2L);
										{	/* Unsafe/aes.scm 580 */
											long BgL_arg2398z00_4548;

											BgL_arg2398z00_4548 = (long) (BgL_arg1877z00_2173);
											BgL_arg1875z00_2171 = (long) (BgL_arg2398z00_4548);
									}}
									{	/* Unsafe/aes.scm 581 */
										uint8_t BgL_arg1878z00_2174;

										BgL_arg1878z00_2174 = BGL_U8VREF(BgL_bz00_2090, 3L);
										{	/* Unsafe/aes.scm 581 */
											long BgL_arg2398z00_4552;

											BgL_arg2398z00_4552 = (long) (BgL_arg1878z00_2174);
											BgL_arg1876z00_2172 = (long) (BgL_arg2398z00_4552);
									}}
									BgL_arg1873z00_2169 =
										(BgL_arg1875z00_2171 ^ BgL_arg1876z00_2172);
								}
								BgL_arg1869z00_2166 =
									(BgL_arg1872z00_2168 ^ BgL_arg1873z00_2169);
							}
							BgL_arg1864z00_2163 = (BgL_arg1868z00_2165 ^ BgL_arg1869z00_2166);
						}
						BgL_arg1862z00_2161 = (BgL_arg1863z00_2162 ^ BgL_arg1864z00_2163);
					}
					{	/* Unsafe/aes.scm 577 */
						int BgL_jz00_4563;
						unsigned long BgL_vz00_4564;

						BgL_jz00_4563 = (int) (1L);
						BgL_vz00_4564 = (unsigned long) (BgL_arg1862z00_2161);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4565;

							BgL_arg1498z00_4565 = VECTOR_REF(BgL_sz00_90, 3L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7789;
								long BgL_tmpz00_7787;

								BgL_auxz00_7789 = (uint8_t) (BgL_vz00_4564);
								BgL_tmpz00_7787 = (long) (BgL_jz00_4563);
								BGL_U8VSET(BgL_arg1498z00_4565, BgL_tmpz00_7787,
									BgL_auxz00_7789);
							} BUNSPEC;
			}}}}
			{	/* Unsafe/aes.scm 554 */
				obj_t BgL_az00_2180;
				obj_t BgL_bz00_2181;

				{	/* Llib/srfi4.scm 447 */

					BgL_az00_2180 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Llib/srfi4.scm 447 */

					BgL_bz00_2181 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2183;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2551z00_4579;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4568;
							int BgL_jz00_4569;

							BgL_iz00_4568 = (int) (0L);
							BgL_jz00_4569 = (int) (2L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4570;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4571;

									BgL_arg1497z00_4571 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4568));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4575;

										BgL_kz00_4575 = (long) (BgL_jz00_4569);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7799;

											BgL_tmpz00_7799 = ((obj_t) BgL_arg1497z00_4571);
											BgL_arg1495z00_4570 =
												BGL_U8VREF(BgL_tmpz00_7799, BgL_kz00_4575);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4577;

									BgL_arg2398z00_4577 = (long) (BgL_arg1495z00_4570);
									BgL_res2551z00_4579 =
										(unsigned long) ((long) (BgL_arg2398z00_4577));
						}}}
						BgL_vz00_2183 = BgL_res2551z00_4579;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7805;

						BgL_tmpz00_7805 = (uint8_t) (BgL_vz00_2183);
						BGL_U8VSET(BgL_az00_2180, 0L, BgL_tmpz00_7805);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2183) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1879z00_2185;

							BgL_arg1879z00_2185 = ((long) (BgL_vz00_2183) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7815;

								BgL_tmpz00_7815 = (uint8_t) (BgL_arg1879z00_2185);
								BGL_U8VSET(BgL_bz00_2181, 0L, BgL_tmpz00_7815);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1880z00_2186;

							BgL_arg1880z00_2186 =
								(((long) (BgL_vz00_2183) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7822;

								BgL_tmpz00_7822 = (uint8_t) (BgL_arg1880z00_2186);
								BGL_U8VSET(BgL_bz00_2181, 0L, BgL_tmpz00_7822);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2190;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2552z00_4597;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4586;
							int BgL_jz00_4587;

							BgL_iz00_4586 = (int) (1L);
							BgL_jz00_4587 = (int) (2L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4588;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4589;

									BgL_arg1497z00_4589 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4586));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4593;

										BgL_kz00_4593 = (long) (BgL_jz00_4587);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7830;

											BgL_tmpz00_7830 = ((obj_t) BgL_arg1497z00_4589);
											BgL_arg1495z00_4588 =
												BGL_U8VREF(BgL_tmpz00_7830, BgL_kz00_4593);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4595;

									BgL_arg2398z00_4595 = (long) (BgL_arg1495z00_4588);
									BgL_res2552z00_4597 =
										(unsigned long) ((long) (BgL_arg2398z00_4595));
						}}}
						BgL_vz00_2190 = BgL_res2552z00_4597;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7836;

						BgL_tmpz00_7836 = (uint8_t) (BgL_vz00_2190);
						BGL_U8VSET(BgL_az00_2180, 1L, BgL_tmpz00_7836);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2190) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1884z00_2192;

							BgL_arg1884z00_2192 = ((long) (BgL_vz00_2190) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7846;

								BgL_tmpz00_7846 = (uint8_t) (BgL_arg1884z00_2192);
								BGL_U8VSET(BgL_bz00_2181, 1L, BgL_tmpz00_7846);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1885z00_2193;

							BgL_arg1885z00_2193 =
								(((long) (BgL_vz00_2190) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7853;

								BgL_tmpz00_7853 = (uint8_t) (BgL_arg1885z00_2193);
								BGL_U8VSET(BgL_bz00_2181, 1L, BgL_tmpz00_7853);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2197;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2553z00_4615;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4604;
							int BgL_jz00_4605;

							BgL_iz00_4604 = (int) (2L);
							BgL_jz00_4605 = (int) (2L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4606;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4607;

									BgL_arg1497z00_4607 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4604));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4611;

										BgL_kz00_4611 = (long) (BgL_jz00_4605);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7861;

											BgL_tmpz00_7861 = ((obj_t) BgL_arg1497z00_4607);
											BgL_arg1495z00_4606 =
												BGL_U8VREF(BgL_tmpz00_7861, BgL_kz00_4611);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4613;

									BgL_arg2398z00_4613 = (long) (BgL_arg1495z00_4606);
									BgL_res2553z00_4615 =
										(unsigned long) ((long) (BgL_arg2398z00_4613));
						}}}
						BgL_vz00_2197 = BgL_res2553z00_4615;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7867;

						BgL_tmpz00_7867 = (uint8_t) (BgL_vz00_2197);
						BGL_U8VSET(BgL_az00_2180, 2L, BgL_tmpz00_7867);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2197) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1889z00_2199;

							BgL_arg1889z00_2199 = ((long) (BgL_vz00_2197) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7877;

								BgL_tmpz00_7877 = (uint8_t) (BgL_arg1889z00_2199);
								BGL_U8VSET(BgL_bz00_2181, 2L, BgL_tmpz00_7877);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1890z00_2200;

							BgL_arg1890z00_2200 =
								(((long) (BgL_vz00_2197) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7884;

								BgL_tmpz00_7884 = (uint8_t) (BgL_arg1890z00_2200);
								BGL_U8VSET(BgL_bz00_2181, 2L, BgL_tmpz00_7884);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2204;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2554z00_4633;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4622;
							int BgL_jz00_4623;

							BgL_iz00_4622 = (int) (3L);
							BgL_jz00_4623 = (int) (2L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4624;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4625;

									BgL_arg1497z00_4625 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4622));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4629;

										BgL_kz00_4629 = (long) (BgL_jz00_4623);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_7892;

											BgL_tmpz00_7892 = ((obj_t) BgL_arg1497z00_4625);
											BgL_arg1495z00_4624 =
												BGL_U8VREF(BgL_tmpz00_7892, BgL_kz00_4629);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4631;

									BgL_arg2398z00_4631 = (long) (BgL_arg1495z00_4624);
									BgL_res2554z00_4633 =
										(unsigned long) ((long) (BgL_arg2398z00_4631));
						}}}
						BgL_vz00_2204 = BgL_res2554z00_4633;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_7898;

						BgL_tmpz00_7898 = (uint8_t) (BgL_vz00_2204);
						BGL_U8VSET(BgL_az00_2180, 3L, BgL_tmpz00_7898);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2204) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1893z00_2206;

							BgL_arg1893z00_2206 = ((long) (BgL_vz00_2204) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_7908;

								BgL_tmpz00_7908 = (uint8_t) (BgL_arg1893z00_2206);
								BGL_U8VSET(BgL_bz00_2181, 3L, BgL_tmpz00_7908);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1894z00_2207;

							BgL_arg1894z00_2207 =
								(((long) (BgL_vz00_2204) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_7915;

								BgL_tmpz00_7915 = (uint8_t) (BgL_arg1894z00_2207);
								BGL_U8VSET(BgL_bz00_2181, 3L, BgL_tmpz00_7915);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 562 */
					long BgL_arg1898z00_2210;

					{	/* Unsafe/aes.scm 562 */
						long BgL_arg1899z00_2211;
						long BgL_arg1901z00_2212;

						{	/* Unsafe/aes.scm 562 */
							uint8_t BgL_arg1902z00_2213;

							BgL_arg1902z00_2213 = BGL_U8VREF(BgL_bz00_2181, 0L);
							{	/* Unsafe/aes.scm 562 */
								long BgL_arg2398z00_4641;

								BgL_arg2398z00_4641 = (long) (BgL_arg1902z00_2213);
								BgL_arg1899z00_2211 = (long) (BgL_arg2398z00_4641);
						}}
						{	/* Unsafe/aes.scm 563 */
							long BgL_arg1903z00_2214;
							long BgL_arg1904z00_2215;

							{	/* Unsafe/aes.scm 563 */
								uint8_t BgL_arg1906z00_2216;

								BgL_arg1906z00_2216 = BGL_U8VREF(BgL_az00_2180, 1L);
								{	/* Unsafe/aes.scm 563 */
									long BgL_arg2398z00_4645;

									BgL_arg2398z00_4645 = (long) (BgL_arg1906z00_2216);
									BgL_arg1903z00_2214 = (long) (BgL_arg2398z00_4645);
							}}
							{	/* Unsafe/aes.scm 564 */
								long BgL_arg1910z00_2217;
								long BgL_arg1911z00_2218;

								{	/* Unsafe/aes.scm 564 */
									uint8_t BgL_arg1912z00_2219;

									BgL_arg1912z00_2219 = BGL_U8VREF(BgL_bz00_2181, 1L);
									{	/* Unsafe/aes.scm 564 */
										long BgL_arg2398z00_4649;

										BgL_arg2398z00_4649 = (long) (BgL_arg1912z00_2219);
										BgL_arg1910z00_2217 = (long) (BgL_arg2398z00_4649);
								}}
								{	/* Unsafe/aes.scm 565 */
									long BgL_arg1913z00_2220;
									long BgL_arg1914z00_2221;

									{	/* Unsafe/aes.scm 565 */
										uint8_t BgL_arg1916z00_2222;

										BgL_arg1916z00_2222 = BGL_U8VREF(BgL_az00_2180, 2L);
										{	/* Unsafe/aes.scm 565 */
											long BgL_arg2398z00_4653;

											BgL_arg2398z00_4653 = (long) (BgL_arg1916z00_2222);
											BgL_arg1913z00_2220 = (long) (BgL_arg2398z00_4653);
									}}
									{	/* Unsafe/aes.scm 566 */
										uint8_t BgL_arg1917z00_2223;

										BgL_arg1917z00_2223 = BGL_U8VREF(BgL_az00_2180, 3L);
										{	/* Unsafe/aes.scm 566 */
											long BgL_arg2398z00_4657;

											BgL_arg2398z00_4657 = (long) (BgL_arg1917z00_2223);
											BgL_arg1914z00_2221 = (long) (BgL_arg2398z00_4657);
									}}
									BgL_arg1911z00_2218 =
										(BgL_arg1913z00_2220 ^ BgL_arg1914z00_2221);
								}
								BgL_arg1904z00_2215 =
									(BgL_arg1910z00_2217 ^ BgL_arg1911z00_2218);
							}
							BgL_arg1901z00_2212 = (BgL_arg1903z00_2214 ^ BgL_arg1904z00_2215);
						}
						BgL_arg1898z00_2210 = (BgL_arg1899z00_2211 ^ BgL_arg1901z00_2212);
					}
					{	/* Unsafe/aes.scm 562 */
						int BgL_jz00_4668;
						unsigned long BgL_vz00_4669;

						BgL_jz00_4668 = (int) (2L);
						BgL_vz00_4669 = (unsigned long) (BgL_arg1898z00_2210);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4670;

							BgL_arg1498z00_4670 = VECTOR_REF(BgL_sz00_90, 0L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7942;
								long BgL_tmpz00_7940;

								BgL_auxz00_7942 = (uint8_t) (BgL_vz00_4669);
								BgL_tmpz00_7940 = (long) (BgL_jz00_4668);
								BGL_U8VSET(BgL_arg1498z00_4670, BgL_tmpz00_7940,
									BgL_auxz00_7942);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 567 */
					long BgL_arg1918z00_2224;

					{	/* Unsafe/aes.scm 567 */
						long BgL_arg1919z00_2225;
						long BgL_arg1920z00_2226;

						{	/* Unsafe/aes.scm 567 */
							uint8_t BgL_arg1923z00_2227;

							BgL_arg1923z00_2227 = BGL_U8VREF(BgL_az00_2180, 0L);
							{	/* Unsafe/aes.scm 567 */
								long BgL_arg2398z00_4674;

								BgL_arg2398z00_4674 = (long) (BgL_arg1923z00_2227);
								BgL_arg1919z00_2225 = (long) (BgL_arg2398z00_4674);
						}}
						{	/* Unsafe/aes.scm 568 */
							long BgL_arg1924z00_2228;
							long BgL_arg1925z00_2229;

							{	/* Unsafe/aes.scm 568 */
								uint8_t BgL_arg1926z00_2230;

								BgL_arg1926z00_2230 = BGL_U8VREF(BgL_bz00_2181, 1L);
								{	/* Unsafe/aes.scm 568 */
									long BgL_arg2398z00_4678;

									BgL_arg2398z00_4678 = (long) (BgL_arg1926z00_2230);
									BgL_arg1924z00_2228 = (long) (BgL_arg2398z00_4678);
							}}
							{	/* Unsafe/aes.scm 569 */
								long BgL_arg1927z00_2231;
								long BgL_arg1928z00_2232;

								{	/* Unsafe/aes.scm 569 */
									uint8_t BgL_arg1929z00_2233;

									BgL_arg1929z00_2233 = BGL_U8VREF(BgL_az00_2180, 2L);
									{	/* Unsafe/aes.scm 569 */
										long BgL_arg2398z00_4682;

										BgL_arg2398z00_4682 = (long) (BgL_arg1929z00_2233);
										BgL_arg1927z00_2231 = (long) (BgL_arg2398z00_4682);
								}}
								{	/* Unsafe/aes.scm 570 */
									long BgL_arg1930z00_2234;
									long BgL_arg1931z00_2235;

									{	/* Unsafe/aes.scm 570 */
										uint8_t BgL_arg1932z00_2236;

										BgL_arg1932z00_2236 = BGL_U8VREF(BgL_bz00_2181, 2L);
										{	/* Unsafe/aes.scm 570 */
											long BgL_arg2398z00_4686;

											BgL_arg2398z00_4686 = (long) (BgL_arg1932z00_2236);
											BgL_arg1930z00_2234 = (long) (BgL_arg2398z00_4686);
									}}
									{	/* Unsafe/aes.scm 571 */
										uint8_t BgL_arg1933z00_2237;

										BgL_arg1933z00_2237 = BGL_U8VREF(BgL_az00_2180, 3L);
										{	/* Unsafe/aes.scm 571 */
											long BgL_arg2398z00_4690;

											BgL_arg2398z00_4690 = (long) (BgL_arg1933z00_2237);
											BgL_arg1931z00_2235 = (long) (BgL_arg2398z00_4690);
									}}
									BgL_arg1928z00_2232 =
										(BgL_arg1930z00_2234 ^ BgL_arg1931z00_2235);
								}
								BgL_arg1925z00_2229 =
									(BgL_arg1927z00_2231 ^ BgL_arg1928z00_2232);
							}
							BgL_arg1920z00_2226 = (BgL_arg1924z00_2228 ^ BgL_arg1925z00_2229);
						}
						BgL_arg1918z00_2224 = (BgL_arg1919z00_2225 ^ BgL_arg1920z00_2226);
					}
					{	/* Unsafe/aes.scm 567 */
						int BgL_jz00_4701;
						unsigned long BgL_vz00_4702;

						BgL_jz00_4701 = (int) (2L);
						BgL_vz00_4702 = (unsigned long) (BgL_arg1918z00_2224);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4703;

							BgL_arg1498z00_4703 = VECTOR_REF(BgL_sz00_90, 1L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7969;
								long BgL_tmpz00_7967;

								BgL_auxz00_7969 = (uint8_t) (BgL_vz00_4702);
								BgL_tmpz00_7967 = (long) (BgL_jz00_4701);
								BGL_U8VSET(BgL_arg1498z00_4703, BgL_tmpz00_7967,
									BgL_auxz00_7969);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 572 */
					long BgL_arg1934z00_2238;

					{	/* Unsafe/aes.scm 572 */
						long BgL_arg1935z00_2239;
						long BgL_arg1936z00_2240;

						{	/* Unsafe/aes.scm 572 */
							uint8_t BgL_arg1937z00_2241;

							BgL_arg1937z00_2241 = BGL_U8VREF(BgL_az00_2180, 0L);
							{	/* Unsafe/aes.scm 572 */
								long BgL_arg2398z00_4707;

								BgL_arg2398z00_4707 = (long) (BgL_arg1937z00_2241);
								BgL_arg1935z00_2239 = (long) (BgL_arg2398z00_4707);
						}}
						{	/* Unsafe/aes.scm 573 */
							long BgL_arg1938z00_2242;
							long BgL_arg1939z00_2243;

							{	/* Unsafe/aes.scm 573 */
								uint8_t BgL_arg1940z00_2244;

								BgL_arg1940z00_2244 = BGL_U8VREF(BgL_az00_2180, 1L);
								{	/* Unsafe/aes.scm 573 */
									long BgL_arg2398z00_4711;

									BgL_arg2398z00_4711 = (long) (BgL_arg1940z00_2244);
									BgL_arg1938z00_2242 = (long) (BgL_arg2398z00_4711);
							}}
							{	/* Unsafe/aes.scm 574 */
								long BgL_arg1941z00_2245;
								long BgL_arg1942z00_2246;

								{	/* Unsafe/aes.scm 574 */
									uint8_t BgL_arg1943z00_2247;

									BgL_arg1943z00_2247 = BGL_U8VREF(BgL_bz00_2181, 2L);
									{	/* Unsafe/aes.scm 574 */
										long BgL_arg2398z00_4715;

										BgL_arg2398z00_4715 = (long) (BgL_arg1943z00_2247);
										BgL_arg1941z00_2245 = (long) (BgL_arg2398z00_4715);
								}}
								{	/* Unsafe/aes.scm 575 */
									long BgL_arg1944z00_2248;
									long BgL_arg1945z00_2249;

									{	/* Unsafe/aes.scm 575 */
										uint8_t BgL_arg1946z00_2250;

										BgL_arg1946z00_2250 = BGL_U8VREF(BgL_az00_2180, 3L);
										{	/* Unsafe/aes.scm 575 */
											long BgL_arg2398z00_4719;

											BgL_arg2398z00_4719 = (long) (BgL_arg1946z00_2250);
											BgL_arg1944z00_2248 = (long) (BgL_arg2398z00_4719);
									}}
									{	/* Unsafe/aes.scm 576 */
										uint8_t BgL_arg1947z00_2251;

										BgL_arg1947z00_2251 = BGL_U8VREF(BgL_bz00_2181, 3L);
										{	/* Unsafe/aes.scm 576 */
											long BgL_arg2398z00_4723;

											BgL_arg2398z00_4723 = (long) (BgL_arg1947z00_2251);
											BgL_arg1945z00_2249 = (long) (BgL_arg2398z00_4723);
									}}
									BgL_arg1942z00_2246 =
										(BgL_arg1944z00_2248 ^ BgL_arg1945z00_2249);
								}
								BgL_arg1939z00_2243 =
									(BgL_arg1941z00_2245 ^ BgL_arg1942z00_2246);
							}
							BgL_arg1936z00_2240 = (BgL_arg1938z00_2242 ^ BgL_arg1939z00_2243);
						}
						BgL_arg1934z00_2238 = (BgL_arg1935z00_2239 ^ BgL_arg1936z00_2240);
					}
					{	/* Unsafe/aes.scm 572 */
						int BgL_jz00_4734;
						unsigned long BgL_vz00_4735;

						BgL_jz00_4734 = (int) (2L);
						BgL_vz00_4735 = (unsigned long) (BgL_arg1934z00_2238);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4736;

							BgL_arg1498z00_4736 = VECTOR_REF(BgL_sz00_90, 2L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_7996;
								long BgL_tmpz00_7994;

								BgL_auxz00_7996 = (uint8_t) (BgL_vz00_4735);
								BgL_tmpz00_7994 = (long) (BgL_jz00_4734);
								BGL_U8VSET(BgL_arg1498z00_4736, BgL_tmpz00_7994,
									BgL_auxz00_7996);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 577 */
					long BgL_arg1948z00_2252;

					{	/* Unsafe/aes.scm 577 */
						long BgL_arg1949z00_2253;
						long BgL_arg1950z00_2254;

						{	/* Unsafe/aes.scm 577 */
							uint8_t BgL_arg1951z00_2255;

							BgL_arg1951z00_2255 = BGL_U8VREF(BgL_az00_2180, 0L);
							{	/* Unsafe/aes.scm 577 */
								long BgL_arg2398z00_4740;

								BgL_arg2398z00_4740 = (long) (BgL_arg1951z00_2255);
								BgL_arg1949z00_2253 = (long) (BgL_arg2398z00_4740);
						}}
						{	/* Unsafe/aes.scm 578 */
							long BgL_arg1952z00_2256;
							long BgL_arg1953z00_2257;

							{	/* Unsafe/aes.scm 578 */
								uint8_t BgL_arg1954z00_2258;

								BgL_arg1954z00_2258 = BGL_U8VREF(BgL_bz00_2181, 0L);
								{	/* Unsafe/aes.scm 578 */
									long BgL_arg2398z00_4744;

									BgL_arg2398z00_4744 = (long) (BgL_arg1954z00_2258);
									BgL_arg1952z00_2256 = (long) (BgL_arg2398z00_4744);
							}}
							{	/* Unsafe/aes.scm 579 */
								long BgL_arg1955z00_2259;
								long BgL_arg1956z00_2260;

								{	/* Unsafe/aes.scm 579 */
									uint8_t BgL_arg1957z00_2261;

									BgL_arg1957z00_2261 = BGL_U8VREF(BgL_az00_2180, 1L);
									{	/* Unsafe/aes.scm 579 */
										long BgL_arg2398z00_4748;

										BgL_arg2398z00_4748 = (long) (BgL_arg1957z00_2261);
										BgL_arg1955z00_2259 = (long) (BgL_arg2398z00_4748);
								}}
								{	/* Unsafe/aes.scm 580 */
									long BgL_arg1958z00_2262;
									long BgL_arg1959z00_2263;

									{	/* Unsafe/aes.scm 580 */
										uint8_t BgL_arg1960z00_2264;

										BgL_arg1960z00_2264 = BGL_U8VREF(BgL_az00_2180, 2L);
										{	/* Unsafe/aes.scm 580 */
											long BgL_arg2398z00_4752;

											BgL_arg2398z00_4752 = (long) (BgL_arg1960z00_2264);
											BgL_arg1958z00_2262 = (long) (BgL_arg2398z00_4752);
									}}
									{	/* Unsafe/aes.scm 581 */
										uint8_t BgL_arg1961z00_2265;

										BgL_arg1961z00_2265 = BGL_U8VREF(BgL_bz00_2181, 3L);
										{	/* Unsafe/aes.scm 581 */
											long BgL_arg2398z00_4756;

											BgL_arg2398z00_4756 = (long) (BgL_arg1961z00_2265);
											BgL_arg1959z00_2263 = (long) (BgL_arg2398z00_4756);
									}}
									BgL_arg1956z00_2260 =
										(BgL_arg1958z00_2262 ^ BgL_arg1959z00_2263);
								}
								BgL_arg1953z00_2257 =
									(BgL_arg1955z00_2259 ^ BgL_arg1956z00_2260);
							}
							BgL_arg1950z00_2254 = (BgL_arg1952z00_2256 ^ BgL_arg1953z00_2257);
						}
						BgL_arg1948z00_2252 = (BgL_arg1949z00_2253 ^ BgL_arg1950z00_2254);
					}
					{	/* Unsafe/aes.scm 577 */
						int BgL_jz00_4767;
						unsigned long BgL_vz00_4768;

						BgL_jz00_4767 = (int) (2L);
						BgL_vz00_4768 = (unsigned long) (BgL_arg1948z00_2252);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4769;

							BgL_arg1498z00_4769 = VECTOR_REF(BgL_sz00_90, 3L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_8023;
								long BgL_tmpz00_8021;

								BgL_auxz00_8023 = (uint8_t) (BgL_vz00_4768);
								BgL_tmpz00_8021 = (long) (BgL_jz00_4767);
								BGL_U8VSET(BgL_arg1498z00_4769, BgL_tmpz00_8021,
									BgL_auxz00_8023);
							} BUNSPEC;
			}}}}
			{	/* Unsafe/aes.scm 554 */
				obj_t BgL_az00_2271;
				obj_t BgL_bz00_2272;

				{	/* Llib/srfi4.scm 447 */

					BgL_az00_2271 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Llib/srfi4.scm 447 */

					BgL_bz00_2272 = BGl_makezd2u8vectorzd2zz__srfi4z00(4L, (uint8_t) (0));
				}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2274;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2555z00_4783;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4772;
							int BgL_jz00_4773;

							BgL_iz00_4772 = (int) (0L);
							BgL_jz00_4773 = (int) (3L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4774;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4775;

									BgL_arg1497z00_4775 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4772));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4779;

										BgL_kz00_4779 = (long) (BgL_jz00_4773);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_8033;

											BgL_tmpz00_8033 = ((obj_t) BgL_arg1497z00_4775);
											BgL_arg1495z00_4774 =
												BGL_U8VREF(BgL_tmpz00_8033, BgL_kz00_4779);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4781;

									BgL_arg2398z00_4781 = (long) (BgL_arg1495z00_4774);
									BgL_res2555z00_4783 =
										(unsigned long) ((long) (BgL_arg2398z00_4781));
						}}}
						BgL_vz00_2274 = BgL_res2555z00_4783;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_8039;

						BgL_tmpz00_8039 = (uint8_t) (BgL_vz00_2274);
						BGL_U8VSET(BgL_az00_2271, 0L, BgL_tmpz00_8039);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2274) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1962z00_2276;

							BgL_arg1962z00_2276 = ((long) (BgL_vz00_2274) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_8049;

								BgL_tmpz00_8049 = (uint8_t) (BgL_arg1962z00_2276);
								BGL_U8VSET(BgL_bz00_2272, 0L, BgL_tmpz00_8049);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1963z00_2277;

							BgL_arg1963z00_2277 =
								(((long) (BgL_vz00_2274) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_8056;

								BgL_tmpz00_8056 = (uint8_t) (BgL_arg1963z00_2277);
								BGL_U8VSET(BgL_bz00_2272, 0L, BgL_tmpz00_8056);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2281;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2556z00_4801;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4790;
							int BgL_jz00_4791;

							BgL_iz00_4790 = (int) (1L);
							BgL_jz00_4791 = (int) (3L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4792;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4793;

									BgL_arg1497z00_4793 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4790));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4797;

										BgL_kz00_4797 = (long) (BgL_jz00_4791);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_8064;

											BgL_tmpz00_8064 = ((obj_t) BgL_arg1497z00_4793);
											BgL_arg1495z00_4792 =
												BGL_U8VREF(BgL_tmpz00_8064, BgL_kz00_4797);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4799;

									BgL_arg2398z00_4799 = (long) (BgL_arg1495z00_4792);
									BgL_res2556z00_4801 =
										(unsigned long) ((long) (BgL_arg2398z00_4799));
						}}}
						BgL_vz00_2281 = BgL_res2556z00_4801;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_8070;

						BgL_tmpz00_8070 = (uint8_t) (BgL_vz00_2281);
						BGL_U8VSET(BgL_az00_2271, 1L, BgL_tmpz00_8070);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2281) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1966z00_2283;

							BgL_arg1966z00_2283 = ((long) (BgL_vz00_2281) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_8080;

								BgL_tmpz00_8080 = (uint8_t) (BgL_arg1966z00_2283);
								BGL_U8VSET(BgL_bz00_2272, 1L, BgL_tmpz00_8080);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1967z00_2284;

							BgL_arg1967z00_2284 =
								(((long) (BgL_vz00_2281) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_8087;

								BgL_tmpz00_8087 = (uint8_t) (BgL_arg1967z00_2284);
								BGL_U8VSET(BgL_bz00_2272, 1L, BgL_tmpz00_8087);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2288;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2557z00_4819;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4808;
							int BgL_jz00_4809;

							BgL_iz00_4808 = (int) (2L);
							BgL_jz00_4809 = (int) (3L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4810;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4811;

									BgL_arg1497z00_4811 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4808));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4815;

										BgL_kz00_4815 = (long) (BgL_jz00_4809);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_8095;

											BgL_tmpz00_8095 = ((obj_t) BgL_arg1497z00_4811);
											BgL_arg1495z00_4810 =
												BGL_U8VREF(BgL_tmpz00_8095, BgL_kz00_4815);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4817;

									BgL_arg2398z00_4817 = (long) (BgL_arg1495z00_4810);
									BgL_res2557z00_4819 =
										(unsigned long) ((long) (BgL_arg2398z00_4817));
						}}}
						BgL_vz00_2288 = BgL_res2557z00_4819;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_8101;

						BgL_tmpz00_8101 = (uint8_t) (BgL_vz00_2288);
						BGL_U8VSET(BgL_az00_2271, 2L, BgL_tmpz00_8101);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2288) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1970z00_2290;

							BgL_arg1970z00_2290 = ((long) (BgL_vz00_2288) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_8111;

								BgL_tmpz00_8111 = (uint8_t) (BgL_arg1970z00_2290);
								BGL_U8VSET(BgL_bz00_2272, 2L, BgL_tmpz00_8111);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1971z00_2291;

							BgL_arg1971z00_2291 =
								(((long) (BgL_vz00_2288) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_8118;

								BgL_tmpz00_8118 = (uint8_t) (BgL_arg1971z00_2291);
								BGL_U8VSET(BgL_bz00_2272, 2L, BgL_tmpz00_8118);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 557 */
					unsigned long BgL_vz00_2295;

					{	/* Unsafe/aes.scm 557 */
						unsigned long BgL_res2558z00_4837;

						{	/* Unsafe/aes.scm 557 */
							int BgL_iz00_4826;
							int BgL_jz00_4827;

							BgL_iz00_4826 = (int) (3L);
							BgL_jz00_4827 = (int) (3L);
							{	/* Unsafe/aes.scm 420 */
								uint8_t BgL_arg1495z00_4828;

								{	/* Unsafe/aes.scm 420 */
									obj_t BgL_arg1497z00_4829;

									BgL_arg1497z00_4829 =
										VECTOR_REF(BgL_sz00_90, (long) (BgL_iz00_4826));
									{	/* Unsafe/aes.scm 420 */
										long BgL_kz00_4833;

										BgL_kz00_4833 = (long) (BgL_jz00_4827);
										{	/* Unsafe/aes.scm 420 */
											obj_t BgL_tmpz00_8126;

											BgL_tmpz00_8126 = ((obj_t) BgL_arg1497z00_4829);
											BgL_arg1495z00_4828 =
												BGL_U8VREF(BgL_tmpz00_8126, BgL_kz00_4833);
								}}}
								{	/* Unsafe/aes.scm 420 */
									long BgL_arg2398z00_4835;

									BgL_arg2398z00_4835 = (long) (BgL_arg1495z00_4828);
									BgL_res2558z00_4837 =
										(unsigned long) ((long) (BgL_arg2398z00_4835));
						}}}
						BgL_vz00_2295 = BgL_res2558z00_4837;
					}
					{	/* Unsafe/aes.scm 558 */
						uint8_t BgL_tmpz00_8132;

						BgL_tmpz00_8132 = (uint8_t) (BgL_vz00_2295);
						BGL_U8VSET(BgL_az00_2271, 3L, BgL_tmpz00_8132);
					} BUNSPEC;
					if ((((long) (BgL_vz00_2295) & 128L) == 0L))
						{	/* Unsafe/aes.scm 560 */
							long BgL_arg1974z00_2297;

							BgL_arg1974z00_2297 = ((long) (BgL_vz00_2295) << (int) (1L));
							{	/* Unsafe/aes.scm 560 */
								uint8_t BgL_tmpz00_8142;

								BgL_tmpz00_8142 = (uint8_t) (BgL_arg1974z00_2297);
								BGL_U8VSET(BgL_bz00_2272, 3L, BgL_tmpz00_8142);
							} BUNSPEC;
						}
					else
						{	/* Unsafe/aes.scm 561 */
							long BgL_arg1975z00_2298;

							BgL_arg1975z00_2298 =
								(((long) (BgL_vz00_2295) << (int) (1L)) ^ 283L);
							{	/* Unsafe/aes.scm 561 */
								uint8_t BgL_tmpz00_8149;

								BgL_tmpz00_8149 = (uint8_t) (BgL_arg1975z00_2298);
								BGL_U8VSET(BgL_bz00_2272, 3L, BgL_tmpz00_8149);
							} BUNSPEC;
				}}
				{	/* Unsafe/aes.scm 562 */
					long BgL_arg1978z00_2301;

					{	/* Unsafe/aes.scm 562 */
						long BgL_arg1979z00_2302;
						long BgL_arg1980z00_2303;

						{	/* Unsafe/aes.scm 562 */
							uint8_t BgL_arg1981z00_2304;

							BgL_arg1981z00_2304 = BGL_U8VREF(BgL_bz00_2272, 0L);
							{	/* Unsafe/aes.scm 562 */
								long BgL_arg2398z00_4845;

								BgL_arg2398z00_4845 = (long) (BgL_arg1981z00_2304);
								BgL_arg1979z00_2302 = (long) (BgL_arg2398z00_4845);
						}}
						{	/* Unsafe/aes.scm 563 */
							long BgL_arg1982z00_2305;
							long BgL_arg1983z00_2306;

							{	/* Unsafe/aes.scm 563 */
								uint8_t BgL_arg1984z00_2307;

								BgL_arg1984z00_2307 = BGL_U8VREF(BgL_az00_2271, 1L);
								{	/* Unsafe/aes.scm 563 */
									long BgL_arg2398z00_4849;

									BgL_arg2398z00_4849 = (long) (BgL_arg1984z00_2307);
									BgL_arg1982z00_2305 = (long) (BgL_arg2398z00_4849);
							}}
							{	/* Unsafe/aes.scm 564 */
								long BgL_arg1985z00_2308;
								long BgL_arg1986z00_2309;

								{	/* Unsafe/aes.scm 564 */
									uint8_t BgL_arg1987z00_2310;

									BgL_arg1987z00_2310 = BGL_U8VREF(BgL_bz00_2272, 1L);
									{	/* Unsafe/aes.scm 564 */
										long BgL_arg2398z00_4853;

										BgL_arg2398z00_4853 = (long) (BgL_arg1987z00_2310);
										BgL_arg1985z00_2308 = (long) (BgL_arg2398z00_4853);
								}}
								{	/* Unsafe/aes.scm 565 */
									long BgL_arg1988z00_2311;
									long BgL_arg1989z00_2312;

									{	/* Unsafe/aes.scm 565 */
										uint8_t BgL_arg1990z00_2313;

										BgL_arg1990z00_2313 = BGL_U8VREF(BgL_az00_2271, 2L);
										{	/* Unsafe/aes.scm 565 */
											long BgL_arg2398z00_4857;

											BgL_arg2398z00_4857 = (long) (BgL_arg1990z00_2313);
											BgL_arg1988z00_2311 = (long) (BgL_arg2398z00_4857);
									}}
									{	/* Unsafe/aes.scm 566 */
										uint8_t BgL_arg1991z00_2314;

										BgL_arg1991z00_2314 = BGL_U8VREF(BgL_az00_2271, 3L);
										{	/* Unsafe/aes.scm 566 */
											long BgL_arg2398z00_4861;

											BgL_arg2398z00_4861 = (long) (BgL_arg1991z00_2314);
											BgL_arg1989z00_2312 = (long) (BgL_arg2398z00_4861);
									}}
									BgL_arg1986z00_2309 =
										(BgL_arg1988z00_2311 ^ BgL_arg1989z00_2312);
								}
								BgL_arg1983z00_2306 =
									(BgL_arg1985z00_2308 ^ BgL_arg1986z00_2309);
							}
							BgL_arg1980z00_2303 = (BgL_arg1982z00_2305 ^ BgL_arg1983z00_2306);
						}
						BgL_arg1978z00_2301 = (BgL_arg1979z00_2302 ^ BgL_arg1980z00_2303);
					}
					{	/* Unsafe/aes.scm 562 */
						int BgL_jz00_4872;
						unsigned long BgL_vz00_4873;

						BgL_jz00_4872 = (int) (3L);
						BgL_vz00_4873 = (unsigned long) (BgL_arg1978z00_2301);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4874;

							BgL_arg1498z00_4874 = VECTOR_REF(BgL_sz00_90, 0L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_8176;
								long BgL_tmpz00_8174;

								BgL_auxz00_8176 = (uint8_t) (BgL_vz00_4873);
								BgL_tmpz00_8174 = (long) (BgL_jz00_4872);
								BGL_U8VSET(BgL_arg1498z00_4874, BgL_tmpz00_8174,
									BgL_auxz00_8176);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 567 */
					long BgL_arg1992z00_2315;

					{	/* Unsafe/aes.scm 567 */
						long BgL_arg1993z00_2316;
						long BgL_arg1994z00_2317;

						{	/* Unsafe/aes.scm 567 */
							uint8_t BgL_arg1995z00_2318;

							BgL_arg1995z00_2318 = BGL_U8VREF(BgL_az00_2271, 0L);
							{	/* Unsafe/aes.scm 567 */
								long BgL_arg2398z00_4878;

								BgL_arg2398z00_4878 = (long) (BgL_arg1995z00_2318);
								BgL_arg1993z00_2316 = (long) (BgL_arg2398z00_4878);
						}}
						{	/* Unsafe/aes.scm 568 */
							long BgL_arg1996z00_2319;
							long BgL_arg1997z00_2320;

							{	/* Unsafe/aes.scm 568 */
								uint8_t BgL_arg1998z00_2321;

								BgL_arg1998z00_2321 = BGL_U8VREF(BgL_bz00_2272, 1L);
								{	/* Unsafe/aes.scm 568 */
									long BgL_arg2398z00_4882;

									BgL_arg2398z00_4882 = (long) (BgL_arg1998z00_2321);
									BgL_arg1996z00_2319 = (long) (BgL_arg2398z00_4882);
							}}
							{	/* Unsafe/aes.scm 569 */
								long BgL_arg1999z00_2322;
								long BgL_arg2000z00_2323;

								{	/* Unsafe/aes.scm 569 */
									uint8_t BgL_arg2001z00_2324;

									BgL_arg2001z00_2324 = BGL_U8VREF(BgL_az00_2271, 2L);
									{	/* Unsafe/aes.scm 569 */
										long BgL_arg2398z00_4886;

										BgL_arg2398z00_4886 = (long) (BgL_arg2001z00_2324);
										BgL_arg1999z00_2322 = (long) (BgL_arg2398z00_4886);
								}}
								{	/* Unsafe/aes.scm 570 */
									long BgL_arg2002z00_2325;
									long BgL_arg2003z00_2326;

									{	/* Unsafe/aes.scm 570 */
										uint8_t BgL_arg2004z00_2327;

										BgL_arg2004z00_2327 = BGL_U8VREF(BgL_bz00_2272, 2L);
										{	/* Unsafe/aes.scm 570 */
											long BgL_arg2398z00_4890;

											BgL_arg2398z00_4890 = (long) (BgL_arg2004z00_2327);
											BgL_arg2002z00_2325 = (long) (BgL_arg2398z00_4890);
									}}
									{	/* Unsafe/aes.scm 571 */
										uint8_t BgL_arg2006z00_2328;

										BgL_arg2006z00_2328 = BGL_U8VREF(BgL_az00_2271, 3L);
										{	/* Unsafe/aes.scm 571 */
											long BgL_arg2398z00_4894;

											BgL_arg2398z00_4894 = (long) (BgL_arg2006z00_2328);
											BgL_arg2003z00_2326 = (long) (BgL_arg2398z00_4894);
									}}
									BgL_arg2000z00_2323 =
										(BgL_arg2002z00_2325 ^ BgL_arg2003z00_2326);
								}
								BgL_arg1997z00_2320 =
									(BgL_arg1999z00_2322 ^ BgL_arg2000z00_2323);
							}
							BgL_arg1994z00_2317 = (BgL_arg1996z00_2319 ^ BgL_arg1997z00_2320);
						}
						BgL_arg1992z00_2315 = (BgL_arg1993z00_2316 ^ BgL_arg1994z00_2317);
					}
					{	/* Unsafe/aes.scm 567 */
						int BgL_jz00_4905;
						unsigned long BgL_vz00_4906;

						BgL_jz00_4905 = (int) (3L);
						BgL_vz00_4906 = (unsigned long) (BgL_arg1992z00_2315);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4907;

							BgL_arg1498z00_4907 = VECTOR_REF(BgL_sz00_90, 1L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_8203;
								long BgL_tmpz00_8201;

								BgL_auxz00_8203 = (uint8_t) (BgL_vz00_4906);
								BgL_tmpz00_8201 = (long) (BgL_jz00_4905);
								BGL_U8VSET(BgL_arg1498z00_4907, BgL_tmpz00_8201,
									BgL_auxz00_8203);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 572 */
					long BgL_arg2007z00_2329;

					{	/* Unsafe/aes.scm 572 */
						long BgL_arg2008z00_2330;
						long BgL_arg2009z00_2331;

						{	/* Unsafe/aes.scm 572 */
							uint8_t BgL_arg2010z00_2332;

							BgL_arg2010z00_2332 = BGL_U8VREF(BgL_az00_2271, 0L);
							{	/* Unsafe/aes.scm 572 */
								long BgL_arg2398z00_4911;

								BgL_arg2398z00_4911 = (long) (BgL_arg2010z00_2332);
								BgL_arg2008z00_2330 = (long) (BgL_arg2398z00_4911);
						}}
						{	/* Unsafe/aes.scm 573 */
							long BgL_arg2011z00_2333;
							long BgL_arg2012z00_2334;

							{	/* Unsafe/aes.scm 573 */
								uint8_t BgL_arg2013z00_2335;

								BgL_arg2013z00_2335 = BGL_U8VREF(BgL_az00_2271, 1L);
								{	/* Unsafe/aes.scm 573 */
									long BgL_arg2398z00_4915;

									BgL_arg2398z00_4915 = (long) (BgL_arg2013z00_2335);
									BgL_arg2011z00_2333 = (long) (BgL_arg2398z00_4915);
							}}
							{	/* Unsafe/aes.scm 574 */
								long BgL_arg2014z00_2336;
								long BgL_arg2015z00_2337;

								{	/* Unsafe/aes.scm 574 */
									uint8_t BgL_arg2016z00_2338;

									BgL_arg2016z00_2338 = BGL_U8VREF(BgL_bz00_2272, 2L);
									{	/* Unsafe/aes.scm 574 */
										long BgL_arg2398z00_4919;

										BgL_arg2398z00_4919 = (long) (BgL_arg2016z00_2338);
										BgL_arg2014z00_2336 = (long) (BgL_arg2398z00_4919);
								}}
								{	/* Unsafe/aes.scm 575 */
									long BgL_arg2017z00_2339;
									long BgL_arg2018z00_2340;

									{	/* Unsafe/aes.scm 575 */
										uint8_t BgL_arg2019z00_2341;

										BgL_arg2019z00_2341 = BGL_U8VREF(BgL_az00_2271, 3L);
										{	/* Unsafe/aes.scm 575 */
											long BgL_arg2398z00_4923;

											BgL_arg2398z00_4923 = (long) (BgL_arg2019z00_2341);
											BgL_arg2017z00_2339 = (long) (BgL_arg2398z00_4923);
									}}
									{	/* Unsafe/aes.scm 576 */
										uint8_t BgL_arg2020z00_2342;

										BgL_arg2020z00_2342 = BGL_U8VREF(BgL_bz00_2272, 3L);
										{	/* Unsafe/aes.scm 576 */
											long BgL_arg2398z00_4927;

											BgL_arg2398z00_4927 = (long) (BgL_arg2020z00_2342);
											BgL_arg2018z00_2340 = (long) (BgL_arg2398z00_4927);
									}}
									BgL_arg2015z00_2337 =
										(BgL_arg2017z00_2339 ^ BgL_arg2018z00_2340);
								}
								BgL_arg2012z00_2334 =
									(BgL_arg2014z00_2336 ^ BgL_arg2015z00_2337);
							}
							BgL_arg2009z00_2331 = (BgL_arg2011z00_2333 ^ BgL_arg2012z00_2334);
						}
						BgL_arg2007z00_2329 = (BgL_arg2008z00_2330 ^ BgL_arg2009z00_2331);
					}
					{	/* Unsafe/aes.scm 572 */
						int BgL_jz00_4938;
						unsigned long BgL_vz00_4939;

						BgL_jz00_4938 = (int) (3L);
						BgL_vz00_4939 = (unsigned long) (BgL_arg2007z00_2329);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4940;

							BgL_arg1498z00_4940 = VECTOR_REF(BgL_sz00_90, 2L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_8230;
								long BgL_tmpz00_8228;

								BgL_auxz00_8230 = (uint8_t) (BgL_vz00_4939);
								BgL_tmpz00_8228 = (long) (BgL_jz00_4938);
								BGL_U8VSET(BgL_arg1498z00_4940, BgL_tmpz00_8228,
									BgL_auxz00_8230);
							} BUNSPEC;
				}}}
				{	/* Unsafe/aes.scm 577 */
					long BgL_arg2021z00_2343;

					{	/* Unsafe/aes.scm 577 */
						long BgL_arg2022z00_2344;
						long BgL_arg2024z00_2345;

						{	/* Unsafe/aes.scm 577 */
							uint8_t BgL_arg2025z00_2346;

							BgL_arg2025z00_2346 = BGL_U8VREF(BgL_az00_2271, 0L);
							{	/* Unsafe/aes.scm 577 */
								long BgL_arg2398z00_4944;

								BgL_arg2398z00_4944 = (long) (BgL_arg2025z00_2346);
								BgL_arg2022z00_2344 = (long) (BgL_arg2398z00_4944);
						}}
						{	/* Unsafe/aes.scm 578 */
							long BgL_arg2026z00_2347;
							long BgL_arg2027z00_2348;

							{	/* Unsafe/aes.scm 578 */
								uint8_t BgL_arg2029z00_2349;

								BgL_arg2029z00_2349 = BGL_U8VREF(BgL_bz00_2272, 0L);
								{	/* Unsafe/aes.scm 578 */
									long BgL_arg2398z00_4948;

									BgL_arg2398z00_4948 = (long) (BgL_arg2029z00_2349);
									BgL_arg2026z00_2347 = (long) (BgL_arg2398z00_4948);
							}}
							{	/* Unsafe/aes.scm 579 */
								long BgL_arg2030z00_2350;
								long BgL_arg2031z00_2351;

								{	/* Unsafe/aes.scm 579 */
									uint8_t BgL_arg2033z00_2352;

									BgL_arg2033z00_2352 = BGL_U8VREF(BgL_az00_2271, 1L);
									{	/* Unsafe/aes.scm 579 */
										long BgL_arg2398z00_4952;

										BgL_arg2398z00_4952 = (long) (BgL_arg2033z00_2352);
										BgL_arg2030z00_2350 = (long) (BgL_arg2398z00_4952);
								}}
								{	/* Unsafe/aes.scm 580 */
									long BgL_arg2034z00_2353;
									long BgL_arg2036z00_2354;

									{	/* Unsafe/aes.scm 580 */
										uint8_t BgL_arg2037z00_2355;

										BgL_arg2037z00_2355 = BGL_U8VREF(BgL_az00_2271, 2L);
										{	/* Unsafe/aes.scm 580 */
											long BgL_arg2398z00_4956;

											BgL_arg2398z00_4956 = (long) (BgL_arg2037z00_2355);
											BgL_arg2034z00_2353 = (long) (BgL_arg2398z00_4956);
									}}
									{	/* Unsafe/aes.scm 581 */
										uint8_t BgL_arg2038z00_2356;

										BgL_arg2038z00_2356 = BGL_U8VREF(BgL_bz00_2272, 3L);
										{	/* Unsafe/aes.scm 581 */
											long BgL_arg2398z00_4960;

											BgL_arg2398z00_4960 = (long) (BgL_arg2038z00_2356);
											BgL_arg2036z00_2354 = (long) (BgL_arg2398z00_4960);
									}}
									BgL_arg2031z00_2351 =
										(BgL_arg2034z00_2353 ^ BgL_arg2036z00_2354);
								}
								BgL_arg2027z00_2348 =
									(BgL_arg2030z00_2350 ^ BgL_arg2031z00_2351);
							}
							BgL_arg2024z00_2345 = (BgL_arg2026z00_2347 ^ BgL_arg2027z00_2348);
						}
						BgL_arg2021z00_2343 = (BgL_arg2022z00_2344 ^ BgL_arg2024z00_2345);
					}
					{	/* Unsafe/aes.scm 577 */
						int BgL_jz00_4971;
						unsigned long BgL_vz00_4972;

						BgL_jz00_4971 = (int) (3L);
						BgL_vz00_4972 = (unsigned long) (BgL_arg2021z00_2343);
						{	/* Unsafe/aes.scm 426 */
							obj_t BgL_arg1498z00_4973;

							BgL_arg1498z00_4973 = VECTOR_REF(BgL_sz00_90, 3L);
							{	/* Unsafe/aes.scm 426 */
								uint8_t BgL_auxz00_8257;
								long BgL_tmpz00_8255;

								BgL_auxz00_8257 = (uint8_t) (BgL_vz00_4972);
								BgL_tmpz00_8255 = (long) (BgL_jz00_4971);
								BGL_U8VSET(BgL_arg1498z00_4973, BgL_tmpz00_8255,
									BgL_auxz00_8257);
							}
							return BUNSPEC;
						}
					}
				}
			}
		}

	}



/* addroundkey! */
	bool_t BGl_addroundkeyz12z12zz__aesz00(obj_t BgL_statez00_92,
		obj_t BgL_wz00_93, long BgL_rndz00_94, long BgL_nbz00_95)
	{
		{	/* Unsafe/aes.scm 588 */
			{
				long BgL_cz00_2363;

				BgL_cz00_2363 = 0L;
			BgL_zc3z04anonymousza32039ze3z87_2364:
				if ((BgL_cz00_2363 < BgL_nbz00_95))
					{	/* Unsafe/aes.scm 590 */
						{	/* Unsafe/aes.scm 591 */
							long BgL_vz00_2366;

							{	/* Unsafe/aes.scm 591 */
								unsigned long BgL_arg2041z00_2367;
								unsigned long BgL_arg2042z00_2368;

								{	/* Unsafe/aes.scm 591 */
									unsigned long BgL_res2559z00_4989;

									{	/* Unsafe/aes.scm 591 */
										int BgL_iz00_4978;
										int BgL_jz00_4979;

										BgL_iz00_4978 = (int) (0L);
										BgL_jz00_4979 = (int) (BgL_cz00_2363);
										{	/* Unsafe/aes.scm 420 */
											uint8_t BgL_arg1495z00_4980;

											{	/* Unsafe/aes.scm 420 */
												obj_t BgL_arg1497z00_4981;

												BgL_arg1497z00_4981 =
													VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_4978));
												{	/* Unsafe/aes.scm 420 */
													long BgL_kz00_4985;

													BgL_kz00_4985 = (long) (BgL_jz00_4979);
													{	/* Unsafe/aes.scm 420 */
														obj_t BgL_tmpz00_8267;

														BgL_tmpz00_8267 = ((obj_t) BgL_arg1497z00_4981);
														BgL_arg1495z00_4980 =
															BGL_U8VREF(BgL_tmpz00_8267, BgL_kz00_4985);
											}}}
											{	/* Unsafe/aes.scm 420 */
												long BgL_arg2398z00_4987;

												BgL_arg2398z00_4987 = (long) (BgL_arg1495z00_4980);
												BgL_res2559z00_4989 =
													(unsigned long) ((long) (BgL_arg2398z00_4987));
									}}}
									BgL_arg2041z00_2367 = BgL_res2559z00_4989;
								}
								{	/* Unsafe/aes.scm 592 */
									long BgL_arg2044z00_2369;

									BgL_arg2044z00_2369 = ((BgL_rndz00_94 * 4L) + BgL_cz00_2363);
									{	/* Unsafe/aes.scm 592 */
										unsigned long BgL_res2560z00_5005;

										{	/* Unsafe/aes.scm 592 */
											int BgL_iz00_4994;
											int BgL_jz00_4995;

											BgL_iz00_4994 = (int) (BgL_arg2044z00_2369);
											BgL_jz00_4995 = (int) (0L);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_4996;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_4997;

													BgL_arg1497z00_4997 =
														VECTOR_REF(BgL_wz00_93, (long) (BgL_iz00_4994));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_5001;

														BgL_kz00_5001 = (long) (BgL_jz00_4995);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_8280;

															BgL_tmpz00_8280 = ((obj_t) BgL_arg1497z00_4997);
															BgL_arg1495z00_4996 =
																BGL_U8VREF(BgL_tmpz00_8280, BgL_kz00_5001);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_5003;

													BgL_arg2398z00_5003 = (long) (BgL_arg1495z00_4996);
													BgL_res2560z00_5005 =
														(unsigned long) ((long) (BgL_arg2398z00_5003));
										}}}
										BgL_arg2042z00_2368 = BgL_res2560z00_5005;
								}}
								BgL_vz00_2366 =
									((long) (BgL_arg2041z00_2367) ^ (long) (BgL_arg2042z00_2368));
							}
							{	/* Unsafe/aes.scm 593 */
								int BgL_iz00_5009;
								int BgL_jz00_5010;
								unsigned long BgL_vz00_5011;

								BgL_iz00_5009 = (int) (0L);
								BgL_jz00_5010 = (int) (BgL_cz00_2363);
								BgL_vz00_5011 = (unsigned long) (BgL_vz00_2366);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_5012;

									BgL_arg1498z00_5012 =
										VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5009));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_8296;
										long BgL_tmpz00_8294;

										BgL_auxz00_8296 = (uint8_t) (BgL_vz00_5011);
										BgL_tmpz00_8294 = (long) (BgL_jz00_5010);
										BGL_U8VSET(BgL_arg1498z00_5012, BgL_tmpz00_8294,
											BgL_auxz00_8296);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_8299;

							BgL_cz00_8299 = (BgL_cz00_2363 + 1L);
							BgL_cz00_2363 = BgL_cz00_8299;
							goto BgL_zc3z04anonymousza32039ze3z87_2364;
						}
					}
				else
					{	/* Unsafe/aes.scm 590 */
						((bool_t) 0);
					}
			}
			{
				long BgL_cz00_2375;

				BgL_cz00_2375 = 0L;
			BgL_zc3z04anonymousza32047ze3z87_2376:
				if ((BgL_cz00_2375 < BgL_nbz00_95))
					{	/* Unsafe/aes.scm 590 */
						{	/* Unsafe/aes.scm 591 */
							long BgL_vz00_2378;

							{	/* Unsafe/aes.scm 591 */
								unsigned long BgL_arg2049z00_2379;
								unsigned long BgL_arg2050z00_2380;

								{	/* Unsafe/aes.scm 591 */
									unsigned long BgL_res2561z00_5030;

									{	/* Unsafe/aes.scm 591 */
										int BgL_iz00_5019;
										int BgL_jz00_5020;

										BgL_iz00_5019 = (int) (1L);
										BgL_jz00_5020 = (int) (BgL_cz00_2375);
										{	/* Unsafe/aes.scm 420 */
											uint8_t BgL_arg1495z00_5021;

											{	/* Unsafe/aes.scm 420 */
												obj_t BgL_arg1497z00_5022;

												BgL_arg1497z00_5022 =
													VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5019));
												{	/* Unsafe/aes.scm 420 */
													long BgL_kz00_5026;

													BgL_kz00_5026 = (long) (BgL_jz00_5020);
													{	/* Unsafe/aes.scm 420 */
														obj_t BgL_tmpz00_8308;

														BgL_tmpz00_8308 = ((obj_t) BgL_arg1497z00_5022);
														BgL_arg1495z00_5021 =
															BGL_U8VREF(BgL_tmpz00_8308, BgL_kz00_5026);
											}}}
											{	/* Unsafe/aes.scm 420 */
												long BgL_arg2398z00_5028;

												BgL_arg2398z00_5028 = (long) (BgL_arg1495z00_5021);
												BgL_res2561z00_5030 =
													(unsigned long) ((long) (BgL_arg2398z00_5028));
									}}}
									BgL_arg2049z00_2379 = BgL_res2561z00_5030;
								}
								{	/* Unsafe/aes.scm 592 */
									long BgL_arg2051z00_2381;

									BgL_arg2051z00_2381 = ((BgL_rndz00_94 * 4L) + BgL_cz00_2375);
									{	/* Unsafe/aes.scm 592 */
										unsigned long BgL_res2562z00_5046;

										{	/* Unsafe/aes.scm 592 */
											int BgL_iz00_5035;
											int BgL_jz00_5036;

											BgL_iz00_5035 = (int) (BgL_arg2051z00_2381);
											BgL_jz00_5036 = (int) (1L);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_5037;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_5038;

													BgL_arg1497z00_5038 =
														VECTOR_REF(BgL_wz00_93, (long) (BgL_iz00_5035));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_5042;

														BgL_kz00_5042 = (long) (BgL_jz00_5036);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_8321;

															BgL_tmpz00_8321 = ((obj_t) BgL_arg1497z00_5038);
															BgL_arg1495z00_5037 =
																BGL_U8VREF(BgL_tmpz00_8321, BgL_kz00_5042);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_5044;

													BgL_arg2398z00_5044 = (long) (BgL_arg1495z00_5037);
													BgL_res2562z00_5046 =
														(unsigned long) ((long) (BgL_arg2398z00_5044));
										}}}
										BgL_arg2050z00_2380 = BgL_res2562z00_5046;
								}}
								BgL_vz00_2378 =
									((long) (BgL_arg2049z00_2379) ^ (long) (BgL_arg2050z00_2380));
							}
							{	/* Unsafe/aes.scm 593 */
								int BgL_iz00_5050;
								int BgL_jz00_5051;
								unsigned long BgL_vz00_5052;

								BgL_iz00_5050 = (int) (1L);
								BgL_jz00_5051 = (int) (BgL_cz00_2375);
								BgL_vz00_5052 = (unsigned long) (BgL_vz00_2378);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_5053;

									BgL_arg1498z00_5053 =
										VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5050));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_8337;
										long BgL_tmpz00_8335;

										BgL_auxz00_8337 = (uint8_t) (BgL_vz00_5052);
										BgL_tmpz00_8335 = (long) (BgL_jz00_5051);
										BGL_U8VSET(BgL_arg1498z00_5053, BgL_tmpz00_8335,
											BgL_auxz00_8337);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_8340;

							BgL_cz00_8340 = (BgL_cz00_2375 + 1L);
							BgL_cz00_2375 = BgL_cz00_8340;
							goto BgL_zc3z04anonymousza32047ze3z87_2376;
						}
					}
				else
					{	/* Unsafe/aes.scm 590 */
						((bool_t) 0);
					}
			}
			{
				long BgL_cz00_2387;

				BgL_cz00_2387 = 0L;
			BgL_zc3z04anonymousza32056ze3z87_2388:
				if ((BgL_cz00_2387 < BgL_nbz00_95))
					{	/* Unsafe/aes.scm 590 */
						{	/* Unsafe/aes.scm 591 */
							long BgL_vz00_2390;

							{	/* Unsafe/aes.scm 591 */
								unsigned long BgL_arg2058z00_2391;
								unsigned long BgL_arg2059z00_2392;

								{	/* Unsafe/aes.scm 591 */
									unsigned long BgL_res2563z00_5071;

									{	/* Unsafe/aes.scm 591 */
										int BgL_iz00_5060;
										int BgL_jz00_5061;

										BgL_iz00_5060 = (int) (2L);
										BgL_jz00_5061 = (int) (BgL_cz00_2387);
										{	/* Unsafe/aes.scm 420 */
											uint8_t BgL_arg1495z00_5062;

											{	/* Unsafe/aes.scm 420 */
												obj_t BgL_arg1497z00_5063;

												BgL_arg1497z00_5063 =
													VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5060));
												{	/* Unsafe/aes.scm 420 */
													long BgL_kz00_5067;

													BgL_kz00_5067 = (long) (BgL_jz00_5061);
													{	/* Unsafe/aes.scm 420 */
														obj_t BgL_tmpz00_8349;

														BgL_tmpz00_8349 = ((obj_t) BgL_arg1497z00_5063);
														BgL_arg1495z00_5062 =
															BGL_U8VREF(BgL_tmpz00_8349, BgL_kz00_5067);
											}}}
											{	/* Unsafe/aes.scm 420 */
												long BgL_arg2398z00_5069;

												BgL_arg2398z00_5069 = (long) (BgL_arg1495z00_5062);
												BgL_res2563z00_5071 =
													(unsigned long) ((long) (BgL_arg2398z00_5069));
									}}}
									BgL_arg2058z00_2391 = BgL_res2563z00_5071;
								}
								{	/* Unsafe/aes.scm 592 */
									long BgL_arg2060z00_2393;

									BgL_arg2060z00_2393 = ((BgL_rndz00_94 * 4L) + BgL_cz00_2387);
									{	/* Unsafe/aes.scm 592 */
										unsigned long BgL_res2564z00_5087;

										{	/* Unsafe/aes.scm 592 */
											int BgL_iz00_5076;
											int BgL_jz00_5077;

											BgL_iz00_5076 = (int) (BgL_arg2060z00_2393);
											BgL_jz00_5077 = (int) (2L);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_5078;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_5079;

													BgL_arg1497z00_5079 =
														VECTOR_REF(BgL_wz00_93, (long) (BgL_iz00_5076));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_5083;

														BgL_kz00_5083 = (long) (BgL_jz00_5077);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_8362;

															BgL_tmpz00_8362 = ((obj_t) BgL_arg1497z00_5079);
															BgL_arg1495z00_5078 =
																BGL_U8VREF(BgL_tmpz00_8362, BgL_kz00_5083);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_5085;

													BgL_arg2398z00_5085 = (long) (BgL_arg1495z00_5078);
													BgL_res2564z00_5087 =
														(unsigned long) ((long) (BgL_arg2398z00_5085));
										}}}
										BgL_arg2059z00_2392 = BgL_res2564z00_5087;
								}}
								BgL_vz00_2390 =
									((long) (BgL_arg2058z00_2391) ^ (long) (BgL_arg2059z00_2392));
							}
							{	/* Unsafe/aes.scm 593 */
								int BgL_iz00_5091;
								int BgL_jz00_5092;
								unsigned long BgL_vz00_5093;

								BgL_iz00_5091 = (int) (2L);
								BgL_jz00_5092 = (int) (BgL_cz00_2387);
								BgL_vz00_5093 = (unsigned long) (BgL_vz00_2390);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_5094;

									BgL_arg1498z00_5094 =
										VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5091));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_8378;
										long BgL_tmpz00_8376;

										BgL_auxz00_8378 = (uint8_t) (BgL_vz00_5093);
										BgL_tmpz00_8376 = (long) (BgL_jz00_5092);
										BGL_U8VSET(BgL_arg1498z00_5094, BgL_tmpz00_8376,
											BgL_auxz00_8378);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_8381;

							BgL_cz00_8381 = (BgL_cz00_2387 + 1L);
							BgL_cz00_2387 = BgL_cz00_8381;
							goto BgL_zc3z04anonymousza32056ze3z87_2388;
						}
					}
				else
					{	/* Unsafe/aes.scm 590 */
						((bool_t) 0);
					}
			}
			{
				long BgL_cz00_2399;

				BgL_cz00_2399 = 0L;
			BgL_zc3z04anonymousza32063ze3z87_2400:
				if ((BgL_cz00_2399 < BgL_nbz00_95))
					{	/* Unsafe/aes.scm 590 */
						{	/* Unsafe/aes.scm 591 */
							long BgL_vz00_2402;

							{	/* Unsafe/aes.scm 591 */
								unsigned long BgL_arg2065z00_2403;
								unsigned long BgL_arg2067z00_2404;

								{	/* Unsafe/aes.scm 591 */
									unsigned long BgL_res2565z00_5112;

									{	/* Unsafe/aes.scm 591 */
										int BgL_iz00_5101;
										int BgL_jz00_5102;

										BgL_iz00_5101 = (int) (3L);
										BgL_jz00_5102 = (int) (BgL_cz00_2399);
										{	/* Unsafe/aes.scm 420 */
											uint8_t BgL_arg1495z00_5103;

											{	/* Unsafe/aes.scm 420 */
												obj_t BgL_arg1497z00_5104;

												BgL_arg1497z00_5104 =
													VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5101));
												{	/* Unsafe/aes.scm 420 */
													long BgL_kz00_5108;

													BgL_kz00_5108 = (long) (BgL_jz00_5102);
													{	/* Unsafe/aes.scm 420 */
														obj_t BgL_tmpz00_8390;

														BgL_tmpz00_8390 = ((obj_t) BgL_arg1497z00_5104);
														BgL_arg1495z00_5103 =
															BGL_U8VREF(BgL_tmpz00_8390, BgL_kz00_5108);
											}}}
											{	/* Unsafe/aes.scm 420 */
												long BgL_arg2398z00_5110;

												BgL_arg2398z00_5110 = (long) (BgL_arg1495z00_5103);
												BgL_res2565z00_5112 =
													(unsigned long) ((long) (BgL_arg2398z00_5110));
									}}}
									BgL_arg2065z00_2403 = BgL_res2565z00_5112;
								}
								{	/* Unsafe/aes.scm 592 */
									long BgL_arg2068z00_2405;

									BgL_arg2068z00_2405 = ((BgL_rndz00_94 * 4L) + BgL_cz00_2399);
									{	/* Unsafe/aes.scm 592 */
										unsigned long BgL_res2566z00_5128;

										{	/* Unsafe/aes.scm 592 */
											int BgL_iz00_5117;
											int BgL_jz00_5118;

											BgL_iz00_5117 = (int) (BgL_arg2068z00_2405);
											BgL_jz00_5118 = (int) (3L);
											{	/* Unsafe/aes.scm 420 */
												uint8_t BgL_arg1495z00_5119;

												{	/* Unsafe/aes.scm 420 */
													obj_t BgL_arg1497z00_5120;

													BgL_arg1497z00_5120 =
														VECTOR_REF(BgL_wz00_93, (long) (BgL_iz00_5117));
													{	/* Unsafe/aes.scm 420 */
														long BgL_kz00_5124;

														BgL_kz00_5124 = (long) (BgL_jz00_5118);
														{	/* Unsafe/aes.scm 420 */
															obj_t BgL_tmpz00_8403;

															BgL_tmpz00_8403 = ((obj_t) BgL_arg1497z00_5120);
															BgL_arg1495z00_5119 =
																BGL_U8VREF(BgL_tmpz00_8403, BgL_kz00_5124);
												}}}
												{	/* Unsafe/aes.scm 420 */
													long BgL_arg2398z00_5126;

													BgL_arg2398z00_5126 = (long) (BgL_arg1495z00_5119);
													BgL_res2566z00_5128 =
														(unsigned long) ((long) (BgL_arg2398z00_5126));
										}}}
										BgL_arg2067z00_2404 = BgL_res2566z00_5128;
								}}
								BgL_vz00_2402 =
									((long) (BgL_arg2065z00_2403) ^ (long) (BgL_arg2067z00_2404));
							}
							{	/* Unsafe/aes.scm 593 */
								int BgL_iz00_5132;
								int BgL_jz00_5133;
								unsigned long BgL_vz00_5134;

								BgL_iz00_5132 = (int) (3L);
								BgL_jz00_5133 = (int) (BgL_cz00_2399);
								BgL_vz00_5134 = (unsigned long) (BgL_vz00_2402);
								{	/* Unsafe/aes.scm 426 */
									obj_t BgL_arg1498z00_5135;

									BgL_arg1498z00_5135 =
										VECTOR_REF(BgL_statez00_92, (long) (BgL_iz00_5132));
									{	/* Unsafe/aes.scm 426 */
										uint8_t BgL_auxz00_8419;
										long BgL_tmpz00_8417;

										BgL_auxz00_8419 = (uint8_t) (BgL_vz00_5134);
										BgL_tmpz00_8417 = (long) (BgL_jz00_5133);
										BGL_U8VSET(BgL_arg1498z00_5135, BgL_tmpz00_8417,
											BgL_auxz00_8419);
									} BUNSPEC;
						}}}
						{
							long BgL_cz00_8422;

							BgL_cz00_8422 = (BgL_cz00_2399 + 1L);
							BgL_cz00_2399 = BgL_cz00_8422;
							goto BgL_zc3z04anonymousza32063ze3z87_2400;
						}
					}
				else
					{	/* Unsafe/aes.scm 590 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* subword! */
	obj_t BGl_subwordz12z12zz__aesz00(obj_t BgL_wz00_96)
	{
		{	/* Unsafe/aes.scm 600 */
			{	/* Unsafe/aes.scm 602 */
				long BgL_arg2072z00_2410;

				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_arg2074z00_2411;

					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2075z00_2412;

						{	/* Unsafe/aes.scm 602 */
							uint8_t BgL_arg2076z00_2413;

							BgL_arg2076z00_2413 = BGL_U8VREF(BgL_wz00_96, 0L);
							{	/* Unsafe/aes.scm 602 */
								long BgL_arg2398z00_5142;

								BgL_arg2398z00_5142 = (long) (BgL_arg2076z00_2413);
								BgL_arg2075z00_2412 = (long) (BgL_arg2398z00_5142);
						}}
						{	/* Unsafe/aes.scm 602 */
							obj_t BgL_vz00_5144;

							BgL_vz00_5144 = CNST_TABLE_REF(1);
							BgL_arg2074z00_2411 =
								BGL_U8VREF(BgL_vz00_5144, BgL_arg2075z00_2412);
					}}
					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2398z00_5147;

						BgL_arg2398z00_5147 = (long) (BgL_arg2074z00_2411);
						BgL_arg2072z00_2410 = (long) (BgL_arg2398z00_5147);
				}}
				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_tmpz00_8431;

					BgL_tmpz00_8431 = (uint8_t) (BgL_arg2072z00_2410);
					BGL_U8VSET(BgL_wz00_96, 0L, BgL_tmpz00_8431);
				} BUNSPEC;
			}
			{	/* Unsafe/aes.scm 602 */
				long BgL_arg2077z00_2415;

				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_arg2078z00_2416;

					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2079z00_2417;

						{	/* Unsafe/aes.scm 602 */
							uint8_t BgL_arg2080z00_2418;

							BgL_arg2080z00_2418 = BGL_U8VREF(BgL_wz00_96, 1L);
							{	/* Unsafe/aes.scm 602 */
								long BgL_arg2398z00_5152;

								BgL_arg2398z00_5152 = (long) (BgL_arg2080z00_2418);
								BgL_arg2079z00_2417 = (long) (BgL_arg2398z00_5152);
						}}
						{	/* Unsafe/aes.scm 602 */
							obj_t BgL_vz00_5154;

							BgL_vz00_5154 = CNST_TABLE_REF(1);
							BgL_arg2078z00_2416 =
								BGL_U8VREF(BgL_vz00_5154, BgL_arg2079z00_2417);
					}}
					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2398z00_5157;

						BgL_arg2398z00_5157 = (long) (BgL_arg2078z00_2416);
						BgL_arg2077z00_2415 = (long) (BgL_arg2398z00_5157);
				}}
				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_tmpz00_8441;

					BgL_tmpz00_8441 = (uint8_t) (BgL_arg2077z00_2415);
					BGL_U8VSET(BgL_wz00_96, 1L, BgL_tmpz00_8441);
				} BUNSPEC;
			}
			{	/* Unsafe/aes.scm 602 */
				long BgL_arg2081z00_2420;

				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_arg2082z00_2421;

					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2083z00_2422;

						{	/* Unsafe/aes.scm 602 */
							uint8_t BgL_arg2084z00_2423;

							BgL_arg2084z00_2423 = BGL_U8VREF(BgL_wz00_96, 2L);
							{	/* Unsafe/aes.scm 602 */
								long BgL_arg2398z00_5162;

								BgL_arg2398z00_5162 = (long) (BgL_arg2084z00_2423);
								BgL_arg2083z00_2422 = (long) (BgL_arg2398z00_5162);
						}}
						{	/* Unsafe/aes.scm 602 */
							obj_t BgL_vz00_5164;

							BgL_vz00_5164 = CNST_TABLE_REF(1);
							BgL_arg2082z00_2421 =
								BGL_U8VREF(BgL_vz00_5164, BgL_arg2083z00_2422);
					}}
					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2398z00_5167;

						BgL_arg2398z00_5167 = (long) (BgL_arg2082z00_2421);
						BgL_arg2081z00_2420 = (long) (BgL_arg2398z00_5167);
				}}
				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_tmpz00_8451;

					BgL_tmpz00_8451 = (uint8_t) (BgL_arg2081z00_2420);
					BGL_U8VSET(BgL_wz00_96, 2L, BgL_tmpz00_8451);
				} BUNSPEC;
			}
			{	/* Unsafe/aes.scm 602 */
				long BgL_arg2086z00_2425;

				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_arg2087z00_2426;

					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2088z00_2427;

						{	/* Unsafe/aes.scm 602 */
							uint8_t BgL_arg2089z00_2428;

							BgL_arg2089z00_2428 = BGL_U8VREF(BgL_wz00_96, 3L);
							{	/* Unsafe/aes.scm 602 */
								long BgL_arg2398z00_5172;

								BgL_arg2398z00_5172 = (long) (BgL_arg2089z00_2428);
								BgL_arg2088z00_2427 = (long) (BgL_arg2398z00_5172);
						}}
						{	/* Unsafe/aes.scm 602 */
							obj_t BgL_vz00_5174;

							BgL_vz00_5174 = CNST_TABLE_REF(1);
							BgL_arg2087z00_2426 =
								BGL_U8VREF(BgL_vz00_5174, BgL_arg2088z00_2427);
					}}
					{	/* Unsafe/aes.scm 602 */
						long BgL_arg2398z00_5177;

						BgL_arg2398z00_5177 = (long) (BgL_arg2087z00_2426);
						BgL_arg2086z00_2425 = (long) (BgL_arg2398z00_5177);
				}}
				{	/* Unsafe/aes.scm 602 */
					uint8_t BgL_tmpz00_8461;

					BgL_tmpz00_8461 = (uint8_t) (BgL_arg2086z00_2425);
					BGL_U8VSET(BgL_wz00_96, 3L, BgL_tmpz00_8461);
				} BUNSPEC;
			}
			return BgL_wz00_96;
		}

	}



/* rotword! */
	obj_t BGl_rotwordz12z12zz__aesz00(obj_t BgL_wz00_97)
	{
		{	/* Unsafe/aes.scm 610 */
			{	/* Unsafe/aes.scm 611 */
				long BgL_tmpz00_2429;

				{	/* Unsafe/aes.scm 611 */
					uint8_t BgL_arg2097z00_2439;

					BgL_arg2097z00_2439 = BGL_U8VREF(BgL_wz00_97, 0L);
					{	/* Unsafe/aes.scm 611 */
						long BgL_arg2398z00_5181;

						BgL_arg2398z00_5181 = (long) (BgL_arg2097z00_2439);
						BgL_tmpz00_2429 = (long) (BgL_arg2398z00_5181);
				}}
				{
					long BgL_iz00_2431;

					BgL_iz00_2431 = 0L;
				BgL_zc3z04anonymousza32090ze3z87_2432:
					if ((BgL_iz00_2431 < 3L))
						{	/* Unsafe/aes.scm 612 */
							{	/* Unsafe/aes.scm 613 */
								long BgL_arg2093z00_2434;

								{	/* Unsafe/aes.scm 613 */
									uint8_t BgL_arg2094z00_2435;

									{	/* Unsafe/aes.scm 613 */
										long BgL_tmpz00_8469;

										BgL_tmpz00_8469 = (BgL_iz00_2431 + 1L);
										BgL_arg2094z00_2435 =
											BGL_U8VREF(BgL_wz00_97, BgL_tmpz00_8469);
									}
									{	/* Unsafe/aes.scm 613 */
										long BgL_arg2398z00_5188;

										BgL_arg2398z00_5188 = (long) (BgL_arg2094z00_2435);
										BgL_arg2093z00_2434 = (long) (BgL_arg2398z00_5188);
								}}
								{	/* Unsafe/aes.scm 613 */
									uint8_t BgL_tmpz00_8474;

									BgL_tmpz00_8474 = (uint8_t) (BgL_arg2093z00_2434);
									BGL_U8VSET(BgL_wz00_97, BgL_iz00_2431, BgL_tmpz00_8474);
								} BUNSPEC;
							}
							{
								long BgL_iz00_8477;

								BgL_iz00_8477 = (BgL_iz00_2431 + 1L);
								BgL_iz00_2431 = BgL_iz00_8477;
								goto BgL_zc3z04anonymousza32090ze3z87_2432;
							}
						}
					else
						{	/* Unsafe/aes.scm 612 */
							((bool_t) 0);
						}
				}
				{	/* Unsafe/aes.scm 614 */
					uint8_t BgL_tmpz00_8479;

					BgL_tmpz00_8479 = (uint8_t) (BgL_tmpz00_2429);
					BGL_U8VSET(BgL_wz00_97, 3L, BgL_tmpz00_8479);
				}
				BUNSPEC;
				return BgL_wz00_97;
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__aesz00(void)
	{
		{	/* Unsafe/aes.scm 20 */
			BGl_modulezd2initializa7ationz75zz__sha1z00(331991421L,
				BSTRING_TO_STRING(BGl_string2703z00zz__aesz00));
			BGl_modulezd2initializa7ationz75zz__datez00(-445947232L,
				BSTRING_TO_STRING(BGl_string2703z00zz__aesz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2703z00zz__aesz00));
		}

	}

#ifdef __cplusplus
}
#endif
