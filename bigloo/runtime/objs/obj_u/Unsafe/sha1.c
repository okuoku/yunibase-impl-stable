/*===========================================================================*/
/*   (Unsafe/sha1.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/sha1.scm -indent -o objs/obj_u/Unsafe/sha1.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SHA1_TYPE_DEFINITIONS
#define BGL___SHA1_TYPE_DEFINITIONS
#endif													// BGL___SHA1_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static uint32_t BGl_fz00zz__sha1z00(int, uint32_t, uint32_t, uint32_t);
	static obj_t BGl_z62sha1sumzd2filezb0zz__sha1z00(obj_t, obj_t);
	static obj_t BGl_z62sha1sumzd2stringzb0zz__sha1z00(obj_t, obj_t);
	static obj_t BGl_sha1z00zz__sha1z00(long, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__sha1z00 = BUNSPEC;
	static obj_t BGl_z62sha1sumzd2portzb0zz__sha1z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha1sumzd2filezd2zz__sha1z00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__sha1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hmacz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sha1sumzd2mmapzb0zz__sha1z00(obj_t, obj_t);
	extern obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha1sumzd2portzd2zz__sha1z00(obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__sha1z00(void);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_sha1sumzd2mmapzd2zz__sha1z00(obj_t);
	extern obj_t BGl_ceilingz00zz__r4_numbers_6_5z00(obj_t);
	extern bool_t rgc_buffer_eof_p(obj_t);
	static obj_t BGl_genericzd2initzd2zz__sha1z00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__sha1z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__sha1z00(void);
	static obj_t BGl_z62hmaczd2sha1sumzd2stringz62zz__sha1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zz__sha1z00(void);
	extern obj_t BGl_hmaczd2stringzd2zz__hmacz00(obj_t, obj_t, obj_t);
	extern long bgl_rgc_blit_string(obj_t, char *, long, long);
	extern obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha1sumzd2stringzd2zz__sha1z00(obj_t);
	extern obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long, uint32_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_methodzd2initzd2zz__sha1z00(void);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	static obj_t BGl_u32zd2fillz12zc0zz__sha1z00(obj_t, long, uint32_t);
	BGL_EXPORTED_DECL obj_t BGl_sha1sumz00zz__sha1z00(obj_t);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_z62sha1sumz62zz__sha1z00(obj_t, obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31504ze3ze5zz__sha1z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hmaczd2sha1sumzd2stringz00zz__sha1z00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31505ze3ze5zz__sha1z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_Kz00zz__sha1z00 = BUNSPEC;
	extern obj_t BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(obj_t,
		unsigned char);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha1sumzd2portzd2envz00zz__sha1z00,
		BgL_bgl_za762sha1sumza7d2por1929z00, BGl_z62sha1sumzd2portzb0zz__sha1z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha1sumzd2envzd2zz__sha1z00,
		BgL_bgl_za762sha1sumza762za7za7_1930z00, BGl_z62sha1sumz62zz__sha1z00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha1sumzd2mmapzd2envz00zz__sha1z00,
		BgL_bgl_za762sha1sumza7d2mma1931z00, BGl_z62sha1sumzd2mmapzb0zz__sha1z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1917z00zz__sha1z00,
		BgL_bgl_string1917za700za7za7_1932za7,
		"/tmp/bigloo/runtime/Unsafe/sha1.scm", 35);
	      DEFINE_STRING(BGl_string1918z00zz__sha1z00,
		BgL_bgl_string1918za700za7za7_1933za7, "&sha1sum-string", 15);
	      DEFINE_STRING(BGl_string1919z00zz__sha1z00,
		BgL_bgl_string1919za700za7za7_1934za7, "bstring", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hmaczd2sha1sumzd2stringzd2envzd2zz__sha1z00,
		BgL_bgl_za762hmacza7d2sha1su1935z00,
		BGl_z62hmaczd2sha1sumzd2stringz62zz__sha1z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1920z00zz__sha1z00,
		BgL_bgl_string1920za700za7za7_1936za7, "&sha1sum-mmap", 13);
	      DEFINE_STRING(BGl_string1921z00zz__sha1z00,
		BgL_bgl_string1921za700za7za7_1937za7, "mmap", 4);
	      DEFINE_STRING(BGl_string1922z00zz__sha1z00,
		BgL_bgl_string1922za700za7za7_1938za7, "&sha1sum-port", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha1sumzd2filezd2envz00zz__sha1z00,
		BgL_bgl_za762sha1sumza7d2fil1939z00, BGl_z62sha1sumzd2filezb0zz__sha1z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1923z00zz__sha1z00,
		BgL_bgl_string1923za700za7za7_1940za7, "input-port", 10);
	      DEFINE_STRING(BGl_string1924z00zz__sha1z00,
		BgL_bgl_string1924za700za7za7_1941za7, "&sha1sum-file", 13);
	      DEFINE_STRING(BGl_string1925z00zz__sha1z00,
		BgL_bgl_string1925za700za7za7_1942za7, "sha1sum", 7);
	      DEFINE_STRING(BGl_string1926z00zz__sha1z00,
		BgL_bgl_string1926za700za7za7_1943za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string1927z00zz__sha1z00,
		BgL_bgl_string1927za700za7za7_1944za7, "&hmac-sha1sum-string", 20);
	      DEFINE_STRING(BGl_string1928z00zz__sha1z00,
		BgL_bgl_string1928za700za7za7_1945za7, "__sha1", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha1sumzd2stringzd2envz00zz__sha1z00,
		BgL_bgl_za762sha1sumza7d2str1946z00, BGl_z62sha1sumzd2stringzb0zz__sha1z00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__sha1z00));
		     ADD_ROOT((void *) (&BGl_Kz00zz__sha1z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__sha1z00(long
		BgL_checksumz00_2938, char *BgL_fromz00_2939)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__sha1z00))
				{
					BGl_requirezd2initializa7ationz75zz__sha1z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__sha1z00();
					BGl_importedzd2moduleszd2initz00zz__sha1z00();
					return BGl_toplevelzd2initzd2zz__sha1z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__sha1z00(void)
	{
		{	/* Unsafe/sha1.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__sha1z00(void)
	{
		{	/* Unsafe/sha1.scm 17 */
			{	/* Unsafe/sha1.scm 129 */
				obj_t BgL_vz00_1302;

				{	/* Llib/srfi4.scm 451 */

					BgL_vz00_1302 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(4L, (uint32_t) (0));
				}
				{	/* Unsafe/sha1.scm 130 */
					uint32_t BgL_arg1230z00_1303;

					{	/* Unsafe/sha1.scm 130 */
						uint32_t BgL_arg1231z00_1304;
						uint32_t BgL_arg1232z00_1305;

						{	/* Unsafe/sha1.scm 130 */
							uint32_t BgL_arg1233z00_1306;

							BgL_arg1233z00_1306 = (uint32_t) (23170L);
							BgL_arg1231z00_1304 = (BgL_arg1233z00_1306 << (int) (16L));
						}
						BgL_arg1232z00_1305 = (uint32_t) (31129L);
						BgL_arg1230z00_1303 = (BgL_arg1231z00_1304 | BgL_arg1232z00_1305);
					}
					BGL_U32VSET(BgL_vz00_1302, 0L, BgL_arg1230z00_1303);
					BUNSPEC;
				}
				{	/* Unsafe/sha1.scm 131 */
					uint32_t BgL_arg1234z00_1307;

					{	/* Unsafe/sha1.scm 131 */
						uint32_t BgL_arg1236z00_1308;
						uint32_t BgL_arg1238z00_1309;

						{	/* Unsafe/sha1.scm 131 */
							uint32_t BgL_arg1239z00_1310;

							BgL_arg1239z00_1310 = (uint32_t) (28377L);
							BgL_arg1236z00_1308 = (BgL_arg1239z00_1310 << (int) (16L));
						}
						BgL_arg1238z00_1309 = (uint32_t) (60321L);
						BgL_arg1234z00_1307 = (BgL_arg1236z00_1308 | BgL_arg1238z00_1309);
					}
					BGL_U32VSET(BgL_vz00_1302, 1L, BgL_arg1234z00_1307);
					BUNSPEC;
				}
				{	/* Unsafe/sha1.scm 132 */
					uint32_t BgL_arg1242z00_1311;

					{	/* Unsafe/sha1.scm 132 */
						uint32_t BgL_arg1244z00_1312;
						uint32_t BgL_arg1248z00_1313;

						{	/* Unsafe/sha1.scm 132 */
							uint32_t BgL_arg1249z00_1314;

							BgL_arg1249z00_1314 = (uint32_t) (36635L);
							BgL_arg1244z00_1312 = (BgL_arg1249z00_1314 << (int) (16L));
						}
						BgL_arg1248z00_1313 = (uint32_t) (48348L);
						BgL_arg1242z00_1311 = (BgL_arg1244z00_1312 | BgL_arg1248z00_1313);
					}
					BGL_U32VSET(BgL_vz00_1302, 2L, BgL_arg1242z00_1311);
					BUNSPEC;
				}
				{	/* Unsafe/sha1.scm 133 */
					uint32_t BgL_arg1252z00_1315;

					{	/* Unsafe/sha1.scm 133 */
						uint32_t BgL_arg1268z00_1316;
						uint32_t BgL_arg1272z00_1317;

						{	/* Unsafe/sha1.scm 133 */
							uint32_t BgL_arg1284z00_1318;

							BgL_arg1284z00_1318 = (uint32_t) (51810L);
							BgL_arg1268z00_1316 = (BgL_arg1284z00_1318 << (int) (16L));
						}
						BgL_arg1272z00_1317 = (uint32_t) (49622L);
						BgL_arg1252z00_1315 = (BgL_arg1268z00_1316 | BgL_arg1272z00_1317);
					}
					BGL_U32VSET(BgL_vz00_1302, 3L, BgL_arg1252z00_1315);
					BUNSPEC;
				}
				return (BGl_Kz00zz__sha1z00 = BgL_vz00_1302, BUNSPEC);
			}
		}

	}



/* u32-fill! */
	obj_t BGl_u32zd2fillz12zc0zz__sha1z00(obj_t BgL_strz00_5,
		long BgL_offsetz00_6, uint32_t BgL_wz00_7)
	{
		{	/* Unsafe/sha1.scm 105 */
			{	/* Unsafe/sha1.scm 106 */
				obj_t BgL_s1z00_1332;

				{	/* Unsafe/sha1.scm 106 */
					long BgL_arg1317z00_1343;

					{	/* Unsafe/sha1.scm 106 */
						uint32_t BgL_tmpz00_2972;

						BgL_tmpz00_2972 = (BgL_wz00_7 >> (int) (16L));
						BgL_arg1317z00_1343 = (long) (BgL_tmpz00_2972);
					}
					BgL_s1z00_1332 =
						BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
						(BgL_arg1317z00_1343, 16L);
				}
				{	/* Unsafe/sha1.scm 106 */
					long BgL_l1z00_1333;

					BgL_l1z00_1333 = STRING_LENGTH(BgL_s1z00_1332);
					{	/* Unsafe/sha1.scm 107 */
						obj_t BgL_s2z00_1334;

						{	/* Unsafe/sha1.scm 108 */
							long BgL_arg1315z00_1341;

							{	/* Unsafe/sha1.scm 108 */
								uint32_t BgL_tmpz00_2978;

								BgL_tmpz00_2978 = (BgL_wz00_7 & (uint32_t) (65535));
								BgL_arg1315z00_1341 = (long) (BgL_tmpz00_2978);
							}
							BgL_s2z00_1334 =
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
								(BgL_arg1315z00_1341, 16L);
						}
						{	/* Unsafe/sha1.scm 108 */
							long BgL_l2z00_1335;

							BgL_l2z00_1335 = STRING_LENGTH(BgL_s2z00_1334);
							{	/* Unsafe/sha1.scm 109 */

								blit_string(BgL_s1z00_1332, 0L, BgL_strz00_5,
									(BgL_offsetz00_6 + (4L - BgL_l1z00_1333)), BgL_l1z00_1333);
								return
									blit_string(BgL_s2z00_1334, 0L, BgL_strz00_5,
									(BgL_offsetz00_6 +
										(4L + (4L - BgL_l2z00_1335))), BgL_l2z00_1335);
							}
						}
					}
				}
			}
		}

	}



/* f */
	uint32_t BGl_fz00zz__sha1z00(int BgL_sz00_20, uint32_t BgL_xz00_21,
		uint32_t BgL_yz00_22, uint32_t BgL_za7za7_23)
	{
		{	/* Unsafe/sha1.scm 165 */
			{

				switch ((long) (BgL_sz00_20))
					{
					case 0L:

						return
							((BgL_xz00_21 & BgL_yz00_22) ^ (~(BgL_xz00_21) & BgL_za7za7_23));
						break;
					case 1L:
					case 3L:

						return (BgL_xz00_21 ^ (BgL_yz00_22 ^ BgL_za7za7_23));
						break;
					default:
						return
							(
							((BgL_xz00_21 & BgL_yz00_22) ^
								(BgL_xz00_21 & BgL_za7za7_23)) ^ (BgL_yz00_22 & BgL_za7za7_23));
					}
			}
		}

	}



/* sha1 */
	obj_t BGl_sha1z00zz__sha1z00(long BgL_lenz00_24, obj_t BgL_mz00_25)
	{
		{	/* Unsafe/sha1.scm 178 */
			{	/* Unsafe/sha1.scm 180 */
				obj_t BgL_wz00_1367;

				BgL_wz00_1367 =
					BGl_makezd2u32vectorzd2zz__srfi4z00(80L, (uint32_t) (0L));
				{	/* Unsafe/sha1.scm 184 */
					obj_t BgL_vecz00_1369;

					{	/* Unsafe/sha1.scm 184 */
						long BgL_arg1338z00_1372;

						BgL_arg1338z00_1372 = (VECTOR_LENGTH(BgL_mz00_25) - 1L);
						BgL_vecz00_1369 = VECTOR_REF(BgL_mz00_25, BgL_arg1338z00_1372);
					}
					{	/* Unsafe/sha1.scm 185 */
						long BgL_v15z00_1371;

						BgL_v15z00_1371 = (BgL_lenz00_24 * 8L);
						{	/* Unsafe/sha1.scm 186 */

							{	/* Unsafe/sha1.scm 187 */
								uint32_t BgL_tmpz00_3009;

								BgL_tmpz00_3009 = (uint32_t) (0L);
								BGL_U32VSET(BgL_vecz00_1369, 14L, BgL_tmpz00_3009);
							} BUNSPEC;
							{	/* Unsafe/sha1.scm 188 */
								uint32_t BgL_tmpz00_3012;

								BgL_tmpz00_3012 = (uint32_t) (BgL_v15z00_1371);
								BGL_U32VSET(BgL_vecz00_1369, 15L, BgL_tmpz00_3012);
							} BUNSPEC;
				}}}
				{	/* Unsafe/sha1.scm 191 */
					uint32_t BgL_h0z00_1373;

					{	/* Unsafe/sha1.scm 191 */
						uint32_t BgL_arg1411z00_1474;
						uint32_t BgL_arg1412z00_1475;

						{	/* Unsafe/sha1.scm 191 */
							uint32_t BgL_arg1413z00_1476;

							BgL_arg1413z00_1476 = (uint32_t) (26437L);
							BgL_arg1411z00_1474 = (BgL_arg1413z00_1476 << (int) (16L));
						}
						BgL_arg1412z00_1475 = (uint32_t) (8961L);
						BgL_h0z00_1373 = (BgL_arg1411z00_1474 | BgL_arg1412z00_1475);
					}
					{	/* Unsafe/sha1.scm 191 */
						uint32_t BgL_h1z00_1374;

						{	/* Unsafe/sha1.scm 192 */
							uint32_t BgL_arg1407z00_1471;
							uint32_t BgL_arg1408z00_1472;

							{	/* Unsafe/sha1.scm 192 */
								uint32_t BgL_arg1410z00_1473;

								BgL_arg1410z00_1473 = (uint32_t) (61389L);
								BgL_arg1407z00_1471 = (BgL_arg1410z00_1473 << (int) (16L));
							}
							BgL_arg1408z00_1472 = (uint32_t) (43913L);
							BgL_h1z00_1374 = (BgL_arg1407z00_1471 | BgL_arg1408z00_1472);
						}
						{	/* Unsafe/sha1.scm 192 */
							uint32_t BgL_h2z00_1375;

							{	/* Unsafe/sha1.scm 193 */
								uint32_t BgL_arg1404z00_1468;
								uint32_t BgL_arg1405z00_1469;

								{	/* Unsafe/sha1.scm 193 */
									uint32_t BgL_arg1406z00_1470;

									BgL_arg1406z00_1470 = (uint32_t) (39098L);
									BgL_arg1404z00_1468 = (BgL_arg1406z00_1470 << (int) (16L));
								}
								BgL_arg1405z00_1469 = (uint32_t) (56574L);
								BgL_h2z00_1375 = (BgL_arg1404z00_1468 | BgL_arg1405z00_1469);
							}
							{	/* Unsafe/sha1.scm 193 */
								uint32_t BgL_h3z00_1376;

								{	/* Unsafe/sha1.scm 194 */
									uint32_t BgL_arg1401z00_1465;
									uint32_t BgL_arg1402z00_1466;

									{	/* Unsafe/sha1.scm 194 */
										uint32_t BgL_arg1403z00_1467;

										BgL_arg1403z00_1467 = (uint32_t) (4146L);
										BgL_arg1401z00_1465 = (BgL_arg1403z00_1467 << (int) (16L));
									}
									BgL_arg1402z00_1466 = (uint32_t) (21622L);
									BgL_h3z00_1376 = (BgL_arg1401z00_1465 | BgL_arg1402z00_1466);
								}
								{	/* Unsafe/sha1.scm 194 */
									uint32_t BgL_h4z00_1377;

									{	/* Unsafe/sha1.scm 195 */
										uint32_t BgL_arg1397z00_1462;
										uint32_t BgL_arg1399z00_1463;

										{	/* Unsafe/sha1.scm 195 */
											uint32_t BgL_arg1400z00_1464;

											BgL_arg1400z00_1464 = (uint32_t) (50130L);
											BgL_arg1397z00_1462 =
												(BgL_arg1400z00_1464 << (int) (16L));
										}
										BgL_arg1399z00_1463 = (uint32_t) (57840L);
										BgL_h4z00_1377 =
											(BgL_arg1397z00_1462 | BgL_arg1399z00_1463);
									}
									{	/* Unsafe/sha1.scm 195 */

										{
											long BgL_iz00_1380;

											BgL_iz00_1380 = 0L;
										BgL_zc3z04anonymousza31339ze3z87_1381:
											if ((BgL_iz00_1380 < VECTOR_LENGTH(BgL_mz00_25)))
												{	/* Unsafe/sha1.scm 198 */
													{
														long BgL_tz00_2243;

														BgL_tz00_2243 = 0L;
													BgL_for1057z00_2242:
														if ((BgL_tz00_2243 < 16L))
															{	/* Unsafe/sha1.scm 201 */
																{	/* Unsafe/sha1.scm 202 */
																	uint32_t BgL_arg1343z00_2247;

																	{	/* Unsafe/sha1.scm 202 */
																		int BgL_iz00_2249;
																		int BgL_jz00_2250;

																		BgL_iz00_2249 = (int) (BgL_iz00_1380);
																		BgL_jz00_2250 = (int) (BgL_tz00_2243);
																		{	/* Unsafe/sha1.scm 160 */
																			long BgL_auxz00_3050;
																			obj_t BgL_tmpz00_3047;

																			BgL_auxz00_3050 = (long) (BgL_jz00_2250);
																			BgL_tmpz00_3047 =
																				VECTOR_REF(BgL_mz00_25,
																				(long) (BgL_iz00_2249));
																			BgL_arg1343z00_2247 =
																				BGL_U32VREF(BgL_tmpz00_3047,
																				BgL_auxz00_3050);
																	}}
																	BGL_U32VSET(BgL_wz00_1367, BgL_tz00_2243,
																		BgL_arg1343z00_2247);
																	BUNSPEC;
																}
																{
																	long BgL_tz00_3054;

																	BgL_tz00_3054 = (BgL_tz00_2243 + 1L);
																	BgL_tz00_2243 = BgL_tz00_3054;
																	goto BgL_for1057z00_2242;
																}
															}
														else
															{	/* Unsafe/sha1.scm 201 */
																((bool_t) 0);
															}
													}
													{
														long BgL_tz00_1393;

														BgL_tz00_1393 = 16L;
													BgL_zc3z04anonymousza31345ze3z87_1394:
														if ((BgL_tz00_1393 < 80L))
															{	/* Unsafe/sha1.scm 203 */
																{	/* Unsafe/sha1.scm 204 */
																	uint32_t BgL_w0z00_1396;

																	{	/* Unsafe/sha1.scm 204 */
																		long BgL_tmpz00_3058;

																		BgL_tmpz00_3058 = (BgL_tz00_1393 - 3L);
																		BgL_w0z00_1396 =
																			BGL_U32VREF(BgL_wz00_1367,
																			BgL_tmpz00_3058);
																	}
																	{	/* Unsafe/sha1.scm 204 */
																		uint32_t BgL_w1z00_1397;

																		{	/* Unsafe/sha1.scm 205 */
																			long BgL_tmpz00_3061;

																			BgL_tmpz00_3061 = (BgL_tz00_1393 - 8L);
																			BgL_w1z00_1397 =
																				BGL_U32VREF(BgL_wz00_1367,
																				BgL_tmpz00_3061);
																		}
																		{	/* Unsafe/sha1.scm 205 */
																			uint32_t BgL_w2z00_1398;

																			{	/* Unsafe/sha1.scm 206 */
																				long BgL_tmpz00_3064;

																				BgL_tmpz00_3064 = (BgL_tz00_1393 - 14L);
																				BgL_w2z00_1398 =
																					BGL_U32VREF(BgL_wz00_1367,
																					BgL_tmpz00_3064);
																			}
																			{	/* Unsafe/sha1.scm 206 */
																				uint32_t BgL_w3z00_1399;

																				{	/* Unsafe/sha1.scm 207 */
																					long BgL_tmpz00_3067;

																					BgL_tmpz00_3067 =
																						(BgL_tz00_1393 - 16L);
																					BgL_w3z00_1399 =
																						BGL_U32VREF(BgL_wz00_1367,
																						BgL_tmpz00_3067);
																				}
																				{	/* Unsafe/sha1.scm 207 */
																					uint32_t BgL_vz00_1400;

																					BgL_vz00_1400 =
																						(BgL_w0z00_1396 ^
																						(BgL_w1z00_1397 ^
																							(BgL_w2z00_1398 ^
																								BgL_w3z00_1399)));
																					{	/* Unsafe/sha1.scm 208 */

																						{	/* Unsafe/sha1.scm 209 */
																							uint32_t BgL_arg1347z00_1401;

																							BgL_arg1347z00_1401 =
																								(
																								(BgL_vz00_1400 <<
																									(int) (1L)) |
																								(BgL_vz00_1400 >>
																									(int) ((32L - 1L))));
																							BGL_U32VSET(BgL_wz00_1367,
																								BgL_tz00_1393,
																								BgL_arg1347z00_1401);
																							BUNSPEC;
																}}}}}}}
																{
																	long BgL_tz00_3080;

																	BgL_tz00_3080 = (BgL_tz00_1393 + 1L);
																	BgL_tz00_1393 = BgL_tz00_3080;
																	goto BgL_zc3z04anonymousza31345ze3z87_1394;
																}
															}
														else
															{	/* Unsafe/sha1.scm 203 */
																((bool_t) 0);
															}
													}
													{	/* Unsafe/sha1.scm 213 */
														uint32_t BgL_az00_1410;
														uint32_t BgL_bz00_1411;
														uint32_t BgL_cz00_1412;
														uint32_t BgL_dz00_1413;
														uint32_t BgL_ez00_1414;

														BgL_az00_1410 = BgL_h0z00_1373;
														BgL_bz00_1411 = BgL_h1z00_1374;
														BgL_cz00_1412 = BgL_h2z00_1375;
														BgL_dz00_1413 = BgL_h3z00_1376;
														BgL_ez00_1414 = BgL_h4z00_1377;
														{
															long BgL_tz00_1417;

															BgL_tz00_1417 = 0L;
														BgL_zc3z04anonymousza31357ze3z87_1418:
															if ((BgL_tz00_1417 < 80L))
																{	/* Unsafe/sha1.scm 220 */
																	{	/* Unsafe/sha1.scm 221 */
																		long BgL_sz00_1420;

																		BgL_sz00_1420 = (BgL_tz00_1417 / 20L);
																		{	/* Unsafe/sha1.scm 221 */
																			uint32_t BgL_a5z00_1421;

																			{	/* Unsafe/sha1.scm 222 */
																				uint32_t BgL_xz00_2281;

																				BgL_xz00_2281 = BgL_az00_1410;
																				BgL_a5z00_1421 =
																					(
																					(BgL_xz00_2281 <<
																						(int) (5L)) |
																					(BgL_xz00_2281 >>
																						(int) ((32L - 5L))));
																			}
																			{	/* Unsafe/sha1.scm 222 */
																				uint32_t BgL_fz00_1422;

																				BgL_fz00_1422 =
																					BGl_fz00zz__sha1z00(
																					(int) (BgL_sz00_1420), BgL_bz00_1411,
																					BgL_cz00_1412, BgL_dz00_1413);
																				{	/* Unsafe/sha1.scm 223 */
																					uint32_t BgL_kz00_1423;

																					BgL_kz00_1423 =
																						BGL_U32VREF(BGl_Kz00zz__sha1z00,
																						BgL_sz00_1420);
																					{	/* Unsafe/sha1.scm 224 */
																						uint32_t BgL_wz00_1424;

																						BgL_wz00_1424 =
																							BGL_U32VREF(BgL_wz00_1367,
																							BgL_tz00_1417);
																						{	/* Unsafe/sha1.scm 225 */
																							uint32_t BgL_yz00_1425;

																							BgL_yz00_1425 =
																								(BgL_a5z00_1421 +
																								(BgL_fz00_1422 +
																									(BgL_ez00_1414 +
																										(BgL_kz00_1423 +
																											BgL_wz00_1424))));
																							{	/* Unsafe/sha1.scm 226 */

																								BgL_ez00_1414 = BgL_dz00_1413;
																								BgL_dz00_1413 = BgL_cz00_1412;
																								{	/* Unsafe/sha1.scm 229 */
																									uint32_t BgL_xz00_2298;

																									BgL_xz00_2298 = BgL_bz00_1411;
																									BgL_cz00_1412 =
																										(
																										(BgL_xz00_2298 <<
																											(int) (30L)) |
																										(BgL_xz00_2298 >>
																											(int) ((32L - 30L))));
																								}
																								BgL_bz00_1411 = BgL_az00_1410;
																								{	/* Unsafe/sha1.scm 231 */
																									uint32_t BgL_arg1359z00_1426;

																									{	/* Unsafe/sha1.scm 231 */
																										uint32_t
																											BgL_arg1360z00_1427;
																										uint32_t
																											BgL_arg1361z00_1428;
																										{	/* Unsafe/sha1.scm 231 */
																											uint32_t
																												BgL_arg1362z00_1429;
																											BgL_arg1362z00_1429 =
																												(uint32_t) (65535L);
																											BgL_arg1360z00_1427 =
																												(BgL_arg1362z00_1429 <<
																												(int) (16L));
																										}
																										BgL_arg1361z00_1428 =
																											(uint32_t) (65535L);
																										BgL_arg1359z00_1426 =
																											(BgL_arg1360z00_1427 |
																											BgL_arg1361z00_1428);
																									}
																									BgL_az00_1410 =
																										(BgL_yz00_1425 &
																										BgL_arg1359z00_1426);
																	}}}}}}}}
																	{
																		long BgL_tz00_3111;

																		BgL_tz00_3111 = (BgL_tz00_1417 + 1L);
																		BgL_tz00_1417 = BgL_tz00_3111;
																		goto BgL_zc3z04anonymousza31357ze3z87_1418;
																	}
																}
															else
																{	/* Unsafe/sha1.scm 220 */
																	((bool_t) 0);
																}
														}
														{	/* Unsafe/sha1.scm 233 */
															uint32_t BgL_arg1367z00_1435;
															uint32_t BgL_arg1368z00_1436;

															BgL_arg1367z00_1435 =
																(BgL_h0z00_1373 + BgL_az00_1410);
															{	/* Unsafe/sha1.scm 233 */
																uint32_t BgL_arg1369z00_1437;
																uint32_t BgL_arg1370z00_1438;

																{	/* Unsafe/sha1.scm 233 */
																	uint32_t BgL_arg1371z00_1439;

																	BgL_arg1371z00_1439 = (uint32_t) (65535L);
																	BgL_arg1369z00_1437 =
																		(BgL_arg1371z00_1439 << (int) (16L));
																}
																BgL_arg1370z00_1438 = (uint32_t) (65535L);
																BgL_arg1368z00_1436 =
																	(BgL_arg1369z00_1437 | BgL_arg1370z00_1438);
															}
															BgL_h0z00_1373 =
																(BgL_arg1367z00_1435 & BgL_arg1368z00_1436);
														}
														{	/* Unsafe/sha1.scm 234 */
															uint32_t BgL_arg1372z00_1440;
															uint32_t BgL_arg1373z00_1441;

															BgL_arg1372z00_1440 =
																(BgL_h1z00_1374 + BgL_bz00_1411);
															{	/* Unsafe/sha1.scm 234 */
																uint32_t BgL_arg1375z00_1442;
																uint32_t BgL_arg1376z00_1443;

																{	/* Unsafe/sha1.scm 234 */
																	uint32_t BgL_arg1377z00_1444;

																	BgL_arg1377z00_1444 = (uint32_t) (65535L);
																	BgL_arg1375z00_1442 =
																		(BgL_arg1377z00_1444 << (int) (16L));
																}
																BgL_arg1376z00_1443 = (uint32_t) (65535L);
																BgL_arg1373z00_1441 =
																	(BgL_arg1375z00_1442 | BgL_arg1376z00_1443);
															}
															BgL_h1z00_1374 =
																(BgL_arg1372z00_1440 & BgL_arg1373z00_1441);
														}
														{	/* Unsafe/sha1.scm 235 */
															uint32_t BgL_arg1378z00_1445;
															uint32_t BgL_arg1379z00_1446;

															BgL_arg1378z00_1445 =
																(BgL_h2z00_1375 + BgL_cz00_1412);
															{	/* Unsafe/sha1.scm 235 */
																uint32_t BgL_arg1380z00_1447;
																uint32_t BgL_arg1382z00_1448;

																{	/* Unsafe/sha1.scm 235 */
																	uint32_t BgL_arg1383z00_1449;

																	BgL_arg1383z00_1449 = (uint32_t) (65535L);
																	BgL_arg1380z00_1447 =
																		(BgL_arg1383z00_1449 << (int) (16L));
																}
																BgL_arg1382z00_1448 = (uint32_t) (65535L);
																BgL_arg1379z00_1446 =
																	(BgL_arg1380z00_1447 | BgL_arg1382z00_1448);
															}
															BgL_h2z00_1375 =
																(BgL_arg1378z00_1445 & BgL_arg1379z00_1446);
														}
														{	/* Unsafe/sha1.scm 236 */
															uint32_t BgL_arg1384z00_1450;
															uint32_t BgL_arg1387z00_1451;

															BgL_arg1384z00_1450 =
																(BgL_h3z00_1376 + BgL_dz00_1413);
															{	/* Unsafe/sha1.scm 236 */
																uint32_t BgL_arg1388z00_1452;
																uint32_t BgL_arg1389z00_1453;

																{	/* Unsafe/sha1.scm 236 */
																	uint32_t BgL_arg1390z00_1454;

																	BgL_arg1390z00_1454 = (uint32_t) (65535L);
																	BgL_arg1388z00_1452 =
																		(BgL_arg1390z00_1454 << (int) (16L));
																}
																BgL_arg1389z00_1453 = (uint32_t) (65535L);
																BgL_arg1387z00_1451 =
																	(BgL_arg1388z00_1452 | BgL_arg1389z00_1453);
															}
															BgL_h3z00_1376 =
																(BgL_arg1384z00_1450 & BgL_arg1387z00_1451);
														}
														{	/* Unsafe/sha1.scm 237 */
															uint32_t BgL_arg1391z00_1455;
															uint32_t BgL_arg1392z00_1456;

															BgL_arg1391z00_1455 =
																(BgL_h4z00_1377 + BgL_ez00_1414);
															{	/* Unsafe/sha1.scm 237 */
																uint32_t BgL_arg1393z00_1457;
																uint32_t BgL_arg1394z00_1458;

																{	/* Unsafe/sha1.scm 237 */
																	uint32_t BgL_arg1395z00_1459;

																	BgL_arg1395z00_1459 = (uint32_t) (65535L);
																	BgL_arg1393z00_1457 =
																		(BgL_arg1395z00_1459 << (int) (16L));
																}
																BgL_arg1394z00_1458 = (uint32_t) (65535L);
																BgL_arg1392z00_1456 =
																	(BgL_arg1393z00_1457 | BgL_arg1394z00_1458);
															}
															BgL_h4z00_1377 =
																(BgL_arg1391z00_1455 & BgL_arg1392z00_1456);
													}}
													{
														long BgL_iz00_3148;

														BgL_iz00_3148 = (BgL_iz00_1380 + 1L);
														BgL_iz00_1380 = BgL_iz00_3148;
														goto BgL_zc3z04anonymousza31339ze3z87_1381;
													}
												}
											else
												{	/* Unsafe/sha1.scm 198 */
													((bool_t) 0);
												}
										}
										{	/* Unsafe/sha1.scm 239 */
											uint32_t BgL_w0z00_2349;
											uint32_t BgL_w1z00_2350;
											uint32_t BgL_w2z00_2351;
											uint32_t BgL_w3z00_2352;
											uint32_t BgL_w4z00_2353;

											BgL_w0z00_2349 = BgL_h0z00_1373;
											BgL_w1z00_2350 = BgL_h1z00_1374;
											BgL_w2z00_2351 = BgL_h2z00_1375;
											BgL_w3z00_2352 = BgL_h3z00_1376;
											BgL_w4z00_2353 = BgL_h4z00_1377;
											{	/* Unsafe/sha1.scm 117 */
												obj_t BgL_rz00_2354;

												BgL_rz00_2354 = make_string(40L, ((unsigned char) '0'));
												BGl_u32zd2fillz12zc0zz__sha1z00(BgL_rz00_2354, 0L,
													BgL_w0z00_2349);
												BGl_u32zd2fillz12zc0zz__sha1z00(BgL_rz00_2354, 8L,
													BgL_w1z00_2350);
												BGl_u32zd2fillz12zc0zz__sha1z00(BgL_rz00_2354, 16L,
													BgL_w2z00_2351);
												BGl_u32zd2fillz12zc0zz__sha1z00(BgL_rz00_2354, 24L,
													BgL_w3z00_2352);
												BGl_u32zd2fillz12zc0zz__sha1z00(BgL_rz00_2354, 32L,
													BgL_w4z00_2353);
												return BgL_rz00_2354;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* sha1sum-string */
	BGL_EXPORTED_DEF obj_t BGl_sha1sumzd2stringzd2zz__sha1z00(obj_t BgL_strz00_28)
	{
		{	/* Unsafe/sha1.scm 253 */
			{	/* Unsafe/sha1.scm 256 */
				long BgL_lenz00_1480;

				BgL_lenz00_1480 = STRING_LENGTH(BgL_strz00_28);
				{	/* Unsafe/sha1.scm 256 */
					long BgL_lz00_1481;

					{	/* Unsafe/sha1.scm 257 */
						long BgL_tmpz00_3157;

						{	/* Unsafe/sha1.scm 257 */
							obj_t BgL_tmpz00_3158;

							{	/* Unsafe/sha1.scm 245 */
								obj_t BgL_rz00_2361;

								BgL_rz00_2361 =
									BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(
										(BgL_lenz00_1480 + 1L)), BINT(4L));
								if (INTEGERP(BgL_rz00_2361))
									{	/* Unsafe/sha1.scm 246 */
										BgL_tmpz00_3158 = BgL_rz00_2361;
									}
								else
									{	/* Unsafe/sha1.scm 246 */
										BgL_tmpz00_3158 =
											BINT(
											(long) (REAL_TO_DOUBLE(BGl_ceilingz00zz__r4_numbers_6_5z00
													(BgL_rz00_2361))));
							}}
							BgL_tmpz00_3157 = (long) CINT(BgL_tmpz00_3158);
						}
						BgL_lz00_1481 = (BgL_tmpz00_3157 + 2L);
					}
					{	/* Unsafe/sha1.scm 257 */
						long BgL_nz00_1482;

						{	/* Unsafe/sha1.scm 245 */
							obj_t BgL_rz00_2367;

							BgL_rz00_2367 =
								BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(BgL_lz00_1481),
								BINT(16L));
							if (INTEGERP(BgL_rz00_2367))
								{	/* Unsafe/sha1.scm 246 */
									BgL_nz00_1482 = (long) CINT(BgL_rz00_2367);
								}
							else
								{	/* Unsafe/sha1.scm 246 */
									BgL_nz00_1482 =
										(long) (REAL_TO_DOUBLE(BGl_ceilingz00zz__r4_numbers_6_5z00
											(BgL_rz00_2367)));
						}}
						{	/* Unsafe/sha1.scm 258 */
							obj_t BgL_mz00_1483;

							BgL_mz00_1483 = make_vector(BgL_nz00_1482, BUNSPEC);
							{	/* Unsafe/sha1.scm 259 */

								{
									long BgL_iz00_1486;

									BgL_iz00_1486 = 0L;
								BgL_zc3z04anonymousza31416ze3z87_1487:
									if ((BgL_iz00_1486 < BgL_nz00_1482))
										{	/* Unsafe/sha1.scm 261 */
											{	/* Unsafe/sha1.scm 262 */
												obj_t BgL_vecz00_1489;

												{	/* Llib/srfi4.scm 451 */

													BgL_vecz00_1489 =
														BGl_makezd2u32vectorzd2zz__srfi4z00(16L,
														(uint32_t) (0));
												}
												{
													long BgL_jz00_1492;

													BgL_jz00_1492 = 0L;
												BgL_zc3z04anonymousza31418ze3z87_1493:
													if ((BgL_jz00_1492 < 16L))
														{	/* Unsafe/sha1.scm 263 */
															{	/* Unsafe/sha1.scm 264 */
																int BgL_nz00_1495;

																BgL_nz00_1495 =
																	(int) (
																	((BgL_iz00_1486 * 64L) +
																		(BgL_jz00_1492 * 4L)));
																{	/* Unsafe/sha1.scm 264 */
																	uint32_t BgL_v0z00_1496;

																	{	/* Unsafe/sha1.scm 140 */
																		long BgL_lenz00_2383;

																		BgL_lenz00_2383 =
																			STRING_LENGTH(BgL_strz00_28);
																		if (
																			((long) (BgL_nz00_1495) <
																				BgL_lenz00_2383))
																			{	/* Unsafe/sha1.scm 142 */
																				long BgL_tmpz00_3194;

																				BgL_tmpz00_3194 =
																					(STRING_REF(BgL_strz00_28,
																						(long) (BgL_nz00_1495)));
																				BgL_v0z00_1496 =
																					(uint32_t) (BgL_tmpz00_3194);
																			}
																		else
																			{	/* Unsafe/sha1.scm 142 */
																				if (
																					((long) (BgL_nz00_1495) ==
																						BgL_lenz00_2383))
																					{	/* Unsafe/sha1.scm 143 */
																						BgL_v0z00_1496 = (uint32_t) (128);
																					}
																				else
																					{	/* Unsafe/sha1.scm 143 */
																						BgL_v0z00_1496 = (uint32_t) (0);
																					}
																			}
																	}
																	{	/* Unsafe/sha1.scm 265 */
																		uint32_t BgL_v1z00_1497;

																		{	/* Unsafe/sha1.scm 266 */
																			long BgL_arg1431z00_1512;

																			BgL_arg1431z00_1512 =
																				((long) (BgL_nz00_1495) + 1L);
																			{	/* Unsafe/sha1.scm 266 */
																				int BgL_iz00_2399;

																				BgL_iz00_2399 =
																					(int) (BgL_arg1431z00_1512);
																				{	/* Unsafe/sha1.scm 140 */
																					long BgL_lenz00_2400;

																					BgL_lenz00_2400 =
																						STRING_LENGTH(BgL_strz00_28);
																					if (
																						((long) (BgL_iz00_2399) <
																							BgL_lenz00_2400))
																						{	/* Unsafe/sha1.scm 142 */
																							long BgL_tmpz00_3209;

																							BgL_tmpz00_3209 =
																								(STRING_REF(BgL_strz00_28,
																									(long) (BgL_iz00_2399)));
																							BgL_v1z00_1497 =
																								(uint32_t) (BgL_tmpz00_3209);
																						}
																					else
																						{	/* Unsafe/sha1.scm 142 */
																							if (
																								((long) (BgL_iz00_2399) ==
																									BgL_lenz00_2400))
																								{	/* Unsafe/sha1.scm 143 */
																									BgL_v1z00_1497 =
																										(uint32_t) (128);
																								}
																							else
																								{	/* Unsafe/sha1.scm 143 */
																									BgL_v1z00_1497 =
																										(uint32_t) (0);
																								}
																						}
																				}
																			}
																		}
																		{	/* Unsafe/sha1.scm 266 */
																			uint32_t BgL_v2z00_1498;

																			{	/* Unsafe/sha1.scm 267 */
																				long BgL_arg1430z00_1511;

																				BgL_arg1430z00_1511 =
																					((long) (BgL_nz00_1495) + 2L);
																				{	/* Unsafe/sha1.scm 267 */
																					int BgL_iz00_2416;

																					BgL_iz00_2416 =
																						(int) (BgL_arg1430z00_1511);
																					{	/* Unsafe/sha1.scm 140 */
																						long BgL_lenz00_2417;

																						BgL_lenz00_2417 =
																							STRING_LENGTH(BgL_strz00_28);
																						if (
																							((long) (BgL_iz00_2416) <
																								BgL_lenz00_2417))
																							{	/* Unsafe/sha1.scm 142 */
																								long BgL_tmpz00_3224;

																								BgL_tmpz00_3224 =
																									(STRING_REF(BgL_strz00_28,
																										(long) (BgL_iz00_2416)));
																								BgL_v2z00_1498 =
																									(uint32_t) (BgL_tmpz00_3224);
																							}
																						else
																							{	/* Unsafe/sha1.scm 142 */
																								if (
																									((long) (BgL_iz00_2416) ==
																										BgL_lenz00_2417))
																									{	/* Unsafe/sha1.scm 143 */
																										BgL_v2z00_1498 =
																											(uint32_t) (128);
																									}
																								else
																									{	/* Unsafe/sha1.scm 143 */
																										BgL_v2z00_1498 =
																											(uint32_t) (0);
																									}
																							}
																					}
																				}
																			}
																			{	/* Unsafe/sha1.scm 267 */
																				uint32_t BgL_v3z00_1499;

																				{	/* Unsafe/sha1.scm 268 */
																					long BgL_arg1429z00_1510;

																					BgL_arg1429z00_1510 =
																						((long) (BgL_nz00_1495) + 3L);
																					{	/* Unsafe/sha1.scm 268 */
																						int BgL_iz00_2433;

																						BgL_iz00_2433 =
																							(int) (BgL_arg1429z00_1510);
																						{	/* Unsafe/sha1.scm 140 */
																							long BgL_lenz00_2434;

																							BgL_lenz00_2434 =
																								STRING_LENGTH(BgL_strz00_28);
																							if (
																								((long) (BgL_iz00_2433) <
																									BgL_lenz00_2434))
																								{	/* Unsafe/sha1.scm 142 */
																									long BgL_tmpz00_3239;

																									BgL_tmpz00_3239 =
																										(STRING_REF(BgL_strz00_28,
																											(long) (BgL_iz00_2433)));
																									BgL_v3z00_1499 =
																										(uint32_t)
																										(BgL_tmpz00_3239);
																								}
																							else
																								{	/* Unsafe/sha1.scm 142 */
																									if (
																										((long) (BgL_iz00_2433) ==
																											BgL_lenz00_2434))
																										{	/* Unsafe/sha1.scm 143 */
																											BgL_v3z00_1499 =
																												(uint32_t) (128);
																										}
																									else
																										{	/* Unsafe/sha1.scm 143 */
																											BgL_v3z00_1499 =
																												(uint32_t) (0);
																										}
																								}
																						}
																					}
																				}
																				{	/* Unsafe/sha1.scm 268 */
																					uint32_t BgL_vz00_1500;

																					{	/* Unsafe/sha1.scm 269 */
																						uint32_t BgL_arg1420z00_1501;
																						uint32_t BgL_arg1421z00_1502;

																						{	/* Unsafe/sha1.scm 269 */
																							uint32_t BgL_arg1422z00_1503;

																							{	/* Unsafe/sha1.scm 269 */
																								long BgL_arg1423z00_1504;

																								{	/* Unsafe/sha1.scm 269 */
																									uint32_t BgL_tmpz00_3247;

																									BgL_tmpz00_3247 =
																										(
																										(BgL_v0z00_1496 <<
																											(int) (8L)) |
																										BgL_v1z00_1497);
																									BgL_arg1423z00_1504 =
																										(long) (BgL_tmpz00_3247);
																								}
																								BgL_arg1422z00_1503 =
																									(uint32_t)
																									(BgL_arg1423z00_1504);
																							}
																							BgL_arg1420z00_1501 =
																								(BgL_arg1422z00_1503 <<
																								(int) (16L));
																						}
																						{	/* Unsafe/sha1.scm 269 */
																							long BgL_arg1426z00_1507;

																							{	/* Unsafe/sha1.scm 269 */
																								uint32_t BgL_tmpz00_3255;

																								BgL_tmpz00_3255 =
																									(
																									(BgL_v2z00_1498 <<
																										(int) (8L)) |
																									BgL_v3z00_1499);
																								BgL_arg1426z00_1507 =
																									(long) (BgL_tmpz00_3255);
																							}
																							BgL_arg1421z00_1502 =
																								(uint32_t)
																								(BgL_arg1426z00_1507);
																						}
																						BgL_vz00_1500 =
																							(BgL_arg1420z00_1501 |
																							BgL_arg1421z00_1502);
																					}
																					{	/* Unsafe/sha1.scm 269 */

																						BGL_U32VSET(BgL_vecz00_1489,
																							BgL_jz00_1492, BgL_vz00_1500);
																						BUNSPEC;
															}}}}}}}
															VECTOR_SET(BgL_mz00_1483, BgL_iz00_1486,
																BgL_vecz00_1489);
															{
																long BgL_jz00_3264;

																BgL_jz00_3264 = (BgL_jz00_1492 + 1L);
																BgL_jz00_1492 = BgL_jz00_3264;
																goto BgL_zc3z04anonymousza31418ze3z87_1493;
															}
														}
													else
														{	/* Unsafe/sha1.scm 263 */
															((bool_t) 0);
														}
												}
											}
											{
												long BgL_iz00_3266;

												BgL_iz00_3266 = (BgL_iz00_1486 + 1L);
												BgL_iz00_1486 = BgL_iz00_3266;
												goto BgL_zc3z04anonymousza31416ze3z87_1487;
											}
										}
									else
										{	/* Unsafe/sha1.scm 261 */
											((bool_t) 0);
										}
								}
								return BGl_sha1z00zz__sha1z00(BgL_lenz00_1480, BgL_mz00_1483);
							}
						}
					}
				}
			}
		}

	}



/* &sha1sum-string */
	obj_t BGl_z62sha1sumzd2stringzb0zz__sha1z00(obj_t BgL_envz00_2905,
		obj_t BgL_strz00_2906)
	{
		{	/* Unsafe/sha1.scm 253 */
			{	/* Unsafe/sha1.scm 256 */
				obj_t BgL_auxz00_3269;

				if (STRINGP(BgL_strz00_2906))
					{	/* Unsafe/sha1.scm 256 */
						BgL_auxz00_3269 = BgL_strz00_2906;
					}
				else
					{
						obj_t BgL_auxz00_3272;

						BgL_auxz00_3272 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__sha1z00,
							BINT(10118L), BGl_string1918z00zz__sha1z00,
							BGl_string1919z00zz__sha1z00, BgL_strz00_2906);
						FAILURE(BgL_auxz00_3272, BFALSE, BFALSE);
					}
				return BGl_sha1sumzd2stringzd2zz__sha1z00(BgL_auxz00_3269);
			}
		}

	}



/* sha1sum-mmap */
	BGL_EXPORTED_DEF obj_t BGl_sha1sumzd2mmapzd2zz__sha1z00(obj_t BgL_strz00_29)
	{
		{	/* Unsafe/sha1.scm 278 */
			{	/* Unsafe/sha1.scm 281 */
				long BgL_lenz00_1524;

				{	/* Unsafe/sha1.scm 281 */
					long BgL_tmpz00_3277;

					BgL_tmpz00_3277 = BGL_MMAP_LENGTH(BgL_strz00_29);
					BgL_lenz00_1524 = (long) (BgL_tmpz00_3277);
				}
				{	/* Unsafe/sha1.scm 281 */
					long BgL_lz00_1525;

					{	/* Unsafe/sha1.scm 282 */
						long BgL_tmpz00_3280;

						{	/* Unsafe/sha1.scm 282 */
							obj_t BgL_tmpz00_3281;

							{	/* Unsafe/sha1.scm 245 */
								obj_t BgL_rz00_2467;

								BgL_rz00_2467 =
									BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(
										(BgL_lenz00_1524 + 1L)), BINT(4L));
								if (INTEGERP(BgL_rz00_2467))
									{	/* Unsafe/sha1.scm 246 */
										BgL_tmpz00_3281 = BgL_rz00_2467;
									}
								else
									{	/* Unsafe/sha1.scm 246 */
										BgL_tmpz00_3281 =
											BINT(
											(long) (REAL_TO_DOUBLE(BGl_ceilingz00zz__r4_numbers_6_5z00
													(BgL_rz00_2467))));
							}}
							BgL_tmpz00_3280 = (long) CINT(BgL_tmpz00_3281);
						}
						BgL_lz00_1525 = (BgL_tmpz00_3280 + 2L);
					}
					{	/* Unsafe/sha1.scm 282 */
						long BgL_nz00_1526;

						{	/* Unsafe/sha1.scm 245 */
							obj_t BgL_rz00_2473;

							BgL_rz00_2473 =
								BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(BgL_lz00_1525),
								BINT(16L));
							if (INTEGERP(BgL_rz00_2473))
								{	/* Unsafe/sha1.scm 246 */
									BgL_nz00_1526 = (long) CINT(BgL_rz00_2473);
								}
							else
								{	/* Unsafe/sha1.scm 246 */
									BgL_nz00_1526 =
										(long) (REAL_TO_DOUBLE(BGl_ceilingz00zz__r4_numbers_6_5z00
											(BgL_rz00_2473)));
						}}
						{	/* Unsafe/sha1.scm 283 */
							obj_t BgL_mz00_1527;

							BgL_mz00_1527 = make_vector(BgL_nz00_1526, BUNSPEC);
							{	/* Unsafe/sha1.scm 284 */

								{
									long BgL_iz00_1530;

									BgL_iz00_1530 = 0L;
								BgL_zc3z04anonymousza31441ze3z87_1531:
									if ((BgL_iz00_1530 < BgL_nz00_1526))
										{	/* Unsafe/sha1.scm 286 */
											{	/* Unsafe/sha1.scm 287 */
												obj_t BgL_vecz00_1533;

												{	/* Llib/srfi4.scm 451 */

													BgL_vecz00_1533 =
														BGl_makezd2u32vectorzd2zz__srfi4z00(16L,
														(uint32_t) (0));
												}
												{
													long BgL_jz00_1536;

													BgL_jz00_1536 = 0L;
												BgL_zc3z04anonymousza31443ze3z87_1537:
													if ((BgL_jz00_1536 < 16L))
														{	/* Unsafe/sha1.scm 288 */
															{	/* Unsafe/sha1.scm 289 */
																long BgL_nz00_1539;

																{	/* Unsafe/sha1.scm 289 */
																	long BgL_tmpz00_3309;

																	BgL_tmpz00_3309 =
																		(
																		(BgL_iz00_1530 * 64L) +
																		(BgL_jz00_1536 * 4L));
																	BgL_nz00_1539 = (long) (BgL_tmpz00_3309);
																}
																{	/* Unsafe/sha1.scm 289 */
																	uint32_t BgL_v0z00_1540;

																	{	/* Unsafe/sha1.scm 150 */
																		long BgL_lenz00_2489;

																		BgL_lenz00_2489 =
																			BGL_MMAP_LENGTH(BgL_strz00_29);
																		{	/* Unsafe/sha1.scm 152 */
																			bool_t BgL_test1969z00_3315;

																			{	/* Unsafe/sha1.scm 152 */
																				long BgL_n1z00_2495;
																				long BgL_n2z00_2496;

																				BgL_n1z00_2495 = (long) (BgL_nz00_1539);
																				BgL_n2z00_2496 =
																					(long) (BgL_lenz00_2489);
																				BgL_test1969z00_3315 =
																					(BgL_n1z00_2495 < BgL_n2z00_2496);
																			}
																			if (BgL_test1969z00_3315)
																				{	/* Unsafe/sha1.scm 152 */
																					long BgL_arg1325z00_2491;

																					{	/* Unsafe/sha1.scm 152 */
																						unsigned char BgL_arg1326z00_2492;

																						{	/* Unsafe/sha1.scm 152 */
																							unsigned char BgL_res1899z00_2505;

																							{	/* Unsafe/sha1.scm 152 */
																								unsigned char BgL_cz00_2499;

																								BgL_cz00_2499 =
																									BGL_MMAP_REF(BgL_strz00_29,
																									BgL_nz00_1539);
																								{	/* Unsafe/sha1.scm 152 */
																									long BgL_arg1546z00_2500;

																									BgL_arg1546z00_2500 =
																										(BgL_nz00_1539 +
																										((long) 1));
																									BGL_MMAP_RP_SET(BgL_strz00_29,
																										BgL_arg1546z00_2500);
																									BUNSPEC;
																									BgL_arg1546z00_2500;
																								}
																								BgL_res1899z00_2505 =
																									BgL_cz00_2499;
																							}
																							BgL_arg1326z00_2492 =
																								BgL_res1899z00_2505;
																						}
																						BgL_arg1325z00_2491 =
																							(BgL_arg1326z00_2492);
																					}
																					BgL_v0z00_1540 =
																						(uint32_t) (BgL_arg1325z00_2491);
																				}
																			else
																				{	/* Unsafe/sha1.scm 153 */
																					bool_t BgL_test1970z00_3324;

																					{	/* Unsafe/sha1.scm 153 */
																						long BgL_n1z00_2508;
																						long BgL_n2z00_2509;

																						BgL_n1z00_2508 =
																							(long) (BgL_nz00_1539);
																						BgL_n2z00_2509 =
																							(long) (BgL_lenz00_2489);
																						BgL_test1970z00_3324 =
																							(BgL_n1z00_2508 ==
																							BgL_n2z00_2509);
																					}
																					if (BgL_test1970z00_3324)
																						{	/* Unsafe/sha1.scm 153 */
																							BgL_v0z00_1540 = (uint32_t) (128);
																						}
																					else
																						{	/* Unsafe/sha1.scm 153 */
																							BgL_v0z00_1540 = (uint32_t) (0);
																						}
																				}
																		}
																	}
																	{	/* Unsafe/sha1.scm 290 */
																		uint32_t BgL_v1z00_1541;

																		{	/* Unsafe/sha1.scm 291 */
																			long BgL_arg1456z00_1556;

																			{	/* Unsafe/sha1.scm 291 */
																				long BgL_za71za7_2510;

																				BgL_za71za7_2510 =
																					(long) (BgL_nz00_1539);
																				BgL_arg1456z00_1556 =
																					(BgL_za71za7_2510 + 1L);
																			}
																			{	/* Unsafe/sha1.scm 291 */
																				long BgL_iz00_2512;

																				BgL_iz00_2512 =
																					(long) (BgL_arg1456z00_1556);
																				{	/* Unsafe/sha1.scm 150 */
																					long BgL_lenz00_2513;

																					BgL_lenz00_2513 =
																						BGL_MMAP_LENGTH(BgL_strz00_29);
																					{	/* Unsafe/sha1.scm 152 */
																						bool_t BgL_test1971z00_3332;

																						{	/* Unsafe/sha1.scm 152 */
																							long BgL_n1z00_2519;
																							long BgL_n2z00_2520;

																							BgL_n1z00_2519 =
																								(long) (BgL_iz00_2512);
																							BgL_n2z00_2520 =
																								(long) (BgL_lenz00_2513);
																							BgL_test1971z00_3332 =
																								(BgL_n1z00_2519 <
																								BgL_n2z00_2520);
																						}
																						if (BgL_test1971z00_3332)
																							{	/* Unsafe/sha1.scm 152 */
																								long BgL_arg1325z00_2515;

																								{	/* Unsafe/sha1.scm 152 */
																									unsigned char
																										BgL_arg1326z00_2516;
																									{	/* Unsafe/sha1.scm 152 */
																										unsigned char
																											BgL_res1900z00_2529;
																										{	/* Unsafe/sha1.scm 152 */
																											unsigned char
																												BgL_cz00_2523;
																											BgL_cz00_2523 =
																												BGL_MMAP_REF
																												(BgL_strz00_29,
																												BgL_iz00_2512);
																											{	/* Unsafe/sha1.scm 152 */
																												long
																													BgL_arg1546z00_2524;
																												BgL_arg1546z00_2524 =
																													(BgL_iz00_2512 +
																													((long) 1));
																												BGL_MMAP_RP_SET
																													(BgL_strz00_29,
																													BgL_arg1546z00_2524);
																												BUNSPEC;
																												BgL_arg1546z00_2524;
																											}
																											BgL_res1900z00_2529 =
																												BgL_cz00_2523;
																										}
																										BgL_arg1326z00_2516 =
																											BgL_res1900z00_2529;
																									}
																									BgL_arg1325z00_2515 =
																										(BgL_arg1326z00_2516);
																								}
																								BgL_v1z00_1541 =
																									(uint32_t)
																									(BgL_arg1325z00_2515);
																							}
																						else
																							{	/* Unsafe/sha1.scm 153 */
																								bool_t BgL_test1972z00_3341;

																								{	/* Unsafe/sha1.scm 153 */
																									long BgL_n1z00_2532;
																									long BgL_n2z00_2533;

																									BgL_n1z00_2532 =
																										(long) (BgL_iz00_2512);
																									BgL_n2z00_2533 =
																										(long) (BgL_lenz00_2513);
																									BgL_test1972z00_3341 =
																										(BgL_n1z00_2532 ==
																										BgL_n2z00_2533);
																								}
																								if (BgL_test1972z00_3341)
																									{	/* Unsafe/sha1.scm 153 */
																										BgL_v1z00_1541 =
																											(uint32_t) (128);
																									}
																								else
																									{	/* Unsafe/sha1.scm 153 */
																										BgL_v1z00_1541 =
																											(uint32_t) (0);
																									}
																							}
																					}
																				}
																			}
																		}
																		{	/* Unsafe/sha1.scm 291 */
																			uint32_t BgL_v2z00_1542;

																			{	/* Unsafe/sha1.scm 292 */
																				long BgL_arg1455z00_1555;

																				{	/* Unsafe/sha1.scm 292 */
																					long BgL_za71za7_2534;

																					BgL_za71za7_2534 =
																						(long) (BgL_nz00_1539);
																					BgL_arg1455z00_1555 =
																						(BgL_za71za7_2534 + 2L);
																				}
																				{	/* Unsafe/sha1.scm 292 */
																					long BgL_iz00_2536;

																					BgL_iz00_2536 =
																						(long) (BgL_arg1455z00_1555);
																					{	/* Unsafe/sha1.scm 150 */
																						long BgL_lenz00_2537;

																						BgL_lenz00_2537 =
																							BGL_MMAP_LENGTH(BgL_strz00_29);
																						{	/* Unsafe/sha1.scm 152 */
																							bool_t BgL_test1973z00_3349;

																							{	/* Unsafe/sha1.scm 152 */
																								long BgL_n1z00_2543;
																								long BgL_n2z00_2544;

																								BgL_n1z00_2543 =
																									(long) (BgL_iz00_2536);
																								BgL_n2z00_2544 =
																									(long) (BgL_lenz00_2537);
																								BgL_test1973z00_3349 =
																									(BgL_n1z00_2543 <
																									BgL_n2z00_2544);
																							}
																							if (BgL_test1973z00_3349)
																								{	/* Unsafe/sha1.scm 152 */
																									long BgL_arg1325z00_2539;

																									{	/* Unsafe/sha1.scm 152 */
																										unsigned char
																											BgL_arg1326z00_2540;
																										{	/* Unsafe/sha1.scm 152 */
																											unsigned char
																												BgL_res1901z00_2553;
																											{	/* Unsafe/sha1.scm 152 */
																												unsigned char
																													BgL_cz00_2547;
																												BgL_cz00_2547 =
																													BGL_MMAP_REF
																													(BgL_strz00_29,
																													BgL_iz00_2536);
																												{	/* Unsafe/sha1.scm 152 */
																													long
																														BgL_arg1546z00_2548;
																													BgL_arg1546z00_2548 =
																														(BgL_iz00_2536 +
																														((long) 1));
																													BGL_MMAP_RP_SET
																														(BgL_strz00_29,
																														BgL_arg1546z00_2548);
																													BUNSPEC;
																													BgL_arg1546z00_2548;
																												}
																												BgL_res1901z00_2553 =
																													BgL_cz00_2547;
																											}
																											BgL_arg1326z00_2540 =
																												BgL_res1901z00_2553;
																										}
																										BgL_arg1325z00_2539 =
																											(BgL_arg1326z00_2540);
																									}
																									BgL_v2z00_1542 =
																										(uint32_t)
																										(BgL_arg1325z00_2539);
																								}
																							else
																								{	/* Unsafe/sha1.scm 153 */
																									bool_t BgL_test1974z00_3358;

																									{	/* Unsafe/sha1.scm 153 */
																										long BgL_n1z00_2556;
																										long BgL_n2z00_2557;

																										BgL_n1z00_2556 =
																											(long) (BgL_iz00_2536);
																										BgL_n2z00_2557 =
																											(long) (BgL_lenz00_2537);
																										BgL_test1974z00_3358 =
																											(BgL_n1z00_2556 ==
																											BgL_n2z00_2557);
																									}
																									if (BgL_test1974z00_3358)
																										{	/* Unsafe/sha1.scm 153 */
																											BgL_v2z00_1542 =
																												(uint32_t) (128);
																										}
																									else
																										{	/* Unsafe/sha1.scm 153 */
																											BgL_v2z00_1542 =
																												(uint32_t) (0);
																										}
																								}
																						}
																					}
																				}
																			}
																			{	/* Unsafe/sha1.scm 292 */
																				uint32_t BgL_v3z00_1543;

																				{	/* Unsafe/sha1.scm 293 */
																					long BgL_arg1454z00_1554;

																					{	/* Unsafe/sha1.scm 293 */
																						long BgL_za71za7_2558;

																						BgL_za71za7_2558 =
																							(long) (BgL_nz00_1539);
																						BgL_arg1454z00_1554 =
																							(BgL_za71za7_2558 + 3L);
																					}
																					{	/* Unsafe/sha1.scm 293 */
																						long BgL_iz00_2560;

																						BgL_iz00_2560 =
																							(long) (BgL_arg1454z00_1554);
																						{	/* Unsafe/sha1.scm 150 */
																							long BgL_lenz00_2561;

																							BgL_lenz00_2561 =
																								BGL_MMAP_LENGTH(BgL_strz00_29);
																							{	/* Unsafe/sha1.scm 152 */
																								bool_t BgL_test1975z00_3366;

																								{	/* Unsafe/sha1.scm 152 */
																									long BgL_n1z00_2567;
																									long BgL_n2z00_2568;

																									BgL_n1z00_2567 =
																										(long) (BgL_iz00_2560);
																									BgL_n2z00_2568 =
																										(long) (BgL_lenz00_2561);
																									BgL_test1975z00_3366 =
																										(BgL_n1z00_2567 <
																										BgL_n2z00_2568);
																								}
																								if (BgL_test1975z00_3366)
																									{	/* Unsafe/sha1.scm 152 */
																										long BgL_arg1325z00_2563;

																										{	/* Unsafe/sha1.scm 152 */
																											unsigned char
																												BgL_arg1326z00_2564;
																											{	/* Unsafe/sha1.scm 152 */
																												unsigned char
																													BgL_res1902z00_2577;
																												{	/* Unsafe/sha1.scm 152 */
																													unsigned char
																														BgL_cz00_2571;
																													BgL_cz00_2571 =
																														BGL_MMAP_REF
																														(BgL_strz00_29,
																														BgL_iz00_2560);
																													{	/* Unsafe/sha1.scm 152 */
																														long
																															BgL_arg1546z00_2572;
																														BgL_arg1546z00_2572
																															=
																															(BgL_iz00_2560 +
																															((long) 1));
																														BGL_MMAP_RP_SET
																															(BgL_strz00_29,
																															BgL_arg1546z00_2572);
																														BUNSPEC;
																														BgL_arg1546z00_2572;
																													}
																													BgL_res1902z00_2577 =
																														BgL_cz00_2571;
																												}
																												BgL_arg1326z00_2564 =
																													BgL_res1902z00_2577;
																											}
																											BgL_arg1325z00_2563 =
																												(BgL_arg1326z00_2564);
																										}
																										BgL_v3z00_1543 =
																											(uint32_t)
																											(BgL_arg1325z00_2563);
																									}
																								else
																									{	/* Unsafe/sha1.scm 153 */
																										bool_t BgL_test1976z00_3375;

																										{	/* Unsafe/sha1.scm 153 */
																											long BgL_n1z00_2580;
																											long BgL_n2z00_2581;

																											BgL_n1z00_2580 =
																												(long) (BgL_iz00_2560);
																											BgL_n2z00_2581 =
																												(long)
																												(BgL_lenz00_2561);
																											BgL_test1976z00_3375 =
																												(BgL_n1z00_2580 ==
																												BgL_n2z00_2581);
																										}
																										if (BgL_test1976z00_3375)
																											{	/* Unsafe/sha1.scm 153 */
																												BgL_v3z00_1543 =
																													(uint32_t) (128);
																											}
																										else
																											{	/* Unsafe/sha1.scm 153 */
																												BgL_v3z00_1543 =
																													(uint32_t) (0);
																											}
																									}
																							}
																						}
																					}
																				}
																				{	/* Unsafe/sha1.scm 293 */
																					uint32_t BgL_vz00_1544;

																					{	/* Unsafe/sha1.scm 294 */
																						uint32_t BgL_arg1445z00_1545;
																						uint32_t BgL_arg1446z00_1546;

																						{	/* Unsafe/sha1.scm 294 */
																							uint32_t BgL_arg1447z00_1547;

																							{	/* Unsafe/sha1.scm 294 */
																								long BgL_arg1448z00_1548;

																								{	/* Unsafe/sha1.scm 294 */
																									uint32_t BgL_tmpz00_3379;

																									BgL_tmpz00_3379 =
																										(
																										(BgL_v0z00_1540 <<
																											(int) (8L)) |
																										BgL_v1z00_1541);
																									BgL_arg1448z00_1548 =
																										(long) (BgL_tmpz00_3379);
																								}
																								BgL_arg1447z00_1547 =
																									(uint32_t)
																									(BgL_arg1448z00_1548);
																							}
																							BgL_arg1445z00_1545 =
																								(BgL_arg1447z00_1547 <<
																								(int) (16L));
																						}
																						{	/* Unsafe/sha1.scm 294 */
																							long BgL_arg1451z00_1551;

																							{	/* Unsafe/sha1.scm 294 */
																								uint32_t BgL_tmpz00_3387;

																								BgL_tmpz00_3387 =
																									(
																									(BgL_v2z00_1542 <<
																										(int) (8L)) |
																									BgL_v3z00_1543);
																								BgL_arg1451z00_1551 =
																									(long) (BgL_tmpz00_3387);
																							}
																							BgL_arg1446z00_1546 =
																								(uint32_t)
																								(BgL_arg1451z00_1551);
																						}
																						BgL_vz00_1544 =
																							(BgL_arg1445z00_1545 |
																							BgL_arg1446z00_1546);
																					}
																					{	/* Unsafe/sha1.scm 294 */

																						BGL_U32VSET(BgL_vecz00_1533,
																							BgL_jz00_1536, BgL_vz00_1544);
																						BUNSPEC;
															}}}}}}}
															VECTOR_SET(BgL_mz00_1527, BgL_iz00_1530,
																BgL_vecz00_1533);
															{
																long BgL_jz00_3396;

																BgL_jz00_3396 = (BgL_jz00_1536 + 1L);
																BgL_jz00_1536 = BgL_jz00_3396;
																goto BgL_zc3z04anonymousza31443ze3z87_1537;
															}
														}
													else
														{	/* Unsafe/sha1.scm 288 */
															((bool_t) 0);
														}
												}
											}
											{
												long BgL_iz00_3398;

												BgL_iz00_3398 = (BgL_iz00_1530 + 1L);
												BgL_iz00_1530 = BgL_iz00_3398;
												goto BgL_zc3z04anonymousza31441ze3z87_1531;
											}
										}
									else
										{	/* Unsafe/sha1.scm 286 */
											((bool_t) 0);
										}
								}
								return BGl_sha1z00zz__sha1z00(BgL_lenz00_1524, BgL_mz00_1527);
							}
						}
					}
				}
			}
		}

	}



/* &sha1sum-mmap */
	obj_t BGl_z62sha1sumzd2mmapzb0zz__sha1z00(obj_t BgL_envz00_2907,
		obj_t BgL_strz00_2908)
	{
		{	/* Unsafe/sha1.scm 278 */
			{	/* Unsafe/sha1.scm 281 */
				obj_t BgL_auxz00_3401;

				if (BGL_MMAPP(BgL_strz00_2908))
					{	/* Unsafe/sha1.scm 281 */
						BgL_auxz00_3401 = BgL_strz00_2908;
					}
				else
					{
						obj_t BgL_auxz00_3404;

						BgL_auxz00_3404 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__sha1z00,
							BINT(11050L), BGl_string1920z00zz__sha1z00,
							BGl_string1921z00zz__sha1z00, BgL_strz00_2908);
						FAILURE(BgL_auxz00_3404, BFALSE, BFALSE);
					}
				return BGl_sha1sumzd2mmapzd2zz__sha1z00(BgL_auxz00_3401);
			}
		}

	}



/* sha1sum-port */
	BGL_EXPORTED_DEF obj_t BGl_sha1sumzd2portzd2zz__sha1z00(obj_t BgL_ipz00_30)
	{
		{	/* Unsafe/sha1.scm 303 */
			{	/* Unsafe/sha1.scm 306 */
				obj_t BgL_bufz00_1568;

				{	/* Ieee/string.scm 172 */

					BgL_bufz00_1568 = make_string(64L, ((unsigned char) ' '));
				}
				{
					long BgL_lenz00_1571;
					obj_t BgL_lz00_1572;
					long BgL_nlz00_1573;

					BgL_lenz00_1571 = 0L;
					BgL_lz00_1572 = BNIL;
					BgL_nlz00_1573 = 0L;
				BgL_zc3z04anonymousza31464ze3z87_1574:
					BGl_stringzd2fillz12zc0zz__r4_strings_6_7z00(BgL_bufz00_1568,
						((unsigned char) '\000'));
					{	/* Unsafe/sha1.scm 312 */
						obj_t BgL_cz00_1575;

						{	/* Unsafe/sha1.scm 312 */
							long BgL_nz00_2601;

							BgL_nz00_2601 =
								bgl_rgc_blit_string(BgL_ipz00_30,
								BSTRING_TO_STRING(BgL_bufz00_1568), 0L, 64L);
							{	/* Unsafe/sha1.scm 312 */
								bool_t BgL_test1978z00_3413;

								if ((BgL_nz00_2601 == 0L))
									{	/* Unsafe/sha1.scm 312 */
										BgL_test1978z00_3413 = rgc_buffer_eof_p(BgL_ipz00_30);
									}
								else
									{	/* Unsafe/sha1.scm 312 */
										BgL_test1978z00_3413 = ((bool_t) 0);
									}
								if (BgL_test1978z00_3413)
									{	/* Unsafe/sha1.scm 312 */
										BgL_cz00_1575 = BEOF;
									}
								else
									{	/* Unsafe/sha1.scm 312 */
										BgL_cz00_1575 = BINT(BgL_nz00_2601);
									}
							}
						}
						{	/* Unsafe/sha1.scm 312 */
							obj_t BgL_lz00_1576;

							if (EOF_OBJECTP(BgL_cz00_1575))
								{	/* Unsafe/sha1.scm 313 */
									BgL_lz00_1576 = BINT(0L);
								}
							else
								{	/* Unsafe/sha1.scm 313 */
									BgL_lz00_1576 = BgL_cz00_1575;
								}
							{	/* Unsafe/sha1.scm 313 */
								long BgL_nlenz00_1577;

								BgL_nlenz00_1577 =
									((long) CINT(BgL_lz00_1576) + BgL_lenz00_1571);
								{	/* Unsafe/sha1.scm 314 */
									obj_t BgL_vecz00_1578;

									BgL_vecz00_1578 =
										BGl_makezd2u32vectorzd2zz__srfi4z00(16L, (uint32_t) (0L));
									{	/* Unsafe/sha1.scm 315 */

										if (((long) CINT(BgL_lz00_1576) < 64L))
											{	/* Unsafe/sha1.scm 316 */
												long BgL_tmpz00_3428;

												BgL_tmpz00_3428 = (long) CINT(BgL_lz00_1576);
												STRING_SET(BgL_bufz00_1568, BgL_tmpz00_3428,
													((unsigned char) 128));
											}
										else
											{	/* Unsafe/sha1.scm 316 */
												BFALSE;
											}
										{
											long BgL_jz00_1582;

											BgL_jz00_1582 = 0L;
										BgL_zc3z04anonymousza31466ze3z87_1583:
											if ((BgL_jz00_1582 < 16L))
												{	/* Unsafe/sha1.scm 317 */
													{	/* Unsafe/sha1.scm 318 */
														int BgL_nz00_1585;

														BgL_nz00_1585 = (int) ((BgL_jz00_1582 * 4L));
														{	/* Unsafe/sha1.scm 318 */
															uint32_t BgL_v0z00_1586;

															{	/* Unsafe/sha1.scm 319 */
																long BgL_tmpz00_3435;

																BgL_tmpz00_3435 =
																	(STRING_REF(BgL_bufz00_1568,
																		(long) (BgL_nz00_1585)));
																BgL_v0z00_1586 = (uint32_t) (BgL_tmpz00_3435);
															}
															{	/* Unsafe/sha1.scm 319 */
																uint32_t BgL_v1z00_1587;

																{	/* Unsafe/sha1.scm 320 */
																	long BgL_tmpz00_3440;

																	BgL_tmpz00_3440 =
																		(STRING_REF(BgL_bufz00_1568,
																			((long) (BgL_nz00_1585) + 1L)));
																	BgL_v1z00_1587 = (uint32_t) (BgL_tmpz00_3440);
																}
																{	/* Unsafe/sha1.scm 320 */
																	uint32_t BgL_v2z00_1588;

																	{	/* Unsafe/sha1.scm 321 */
																		long BgL_tmpz00_3446;

																		BgL_tmpz00_3446 =
																			(STRING_REF(BgL_bufz00_1568,
																				((long) (BgL_nz00_1585) + 2L)));
																		BgL_v2z00_1588 =
																			(uint32_t) (BgL_tmpz00_3446);
																	}
																	{	/* Unsafe/sha1.scm 321 */
																		uint32_t BgL_v3z00_1589;

																		{	/* Unsafe/sha1.scm 322 */
																			long BgL_tmpz00_3452;

																			BgL_tmpz00_3452 =
																				(STRING_REF(BgL_bufz00_1568,
																					((long) (BgL_nz00_1585) + 3L)));
																			BgL_v3z00_1589 =
																				(uint32_t) (BgL_tmpz00_3452);
																		}
																		{	/* Unsafe/sha1.scm 322 */
																			uint32_t BgL_vz00_1590;

																			{	/* Unsafe/sha1.scm 323 */
																				uint32_t BgL_arg1468z00_1591;
																				uint32_t BgL_arg1469z00_1592;

																				{	/* Unsafe/sha1.scm 323 */
																					uint32_t BgL_arg1472z00_1593;

																					{	/* Unsafe/sha1.scm 323 */
																						long BgL_arg1473z00_1594;

																						{	/* Unsafe/sha1.scm 323 */
																							uint32_t BgL_tmpz00_3458;

																							BgL_tmpz00_3458 =
																								(
																								(BgL_v0z00_1586 <<
																									(int) (8L)) | BgL_v1z00_1587);
																							BgL_arg1473z00_1594 =
																								(long) (BgL_tmpz00_3458);
																						}
																						BgL_arg1472z00_1593 =
																							(uint32_t) (BgL_arg1473z00_1594);
																					}
																					BgL_arg1468z00_1591 =
																						(BgL_arg1472z00_1593 <<
																						(int) (16L));
																				}
																				{	/* Unsafe/sha1.scm 323 */
																					long BgL_arg1477z00_1597;

																					{	/* Unsafe/sha1.scm 323 */
																						uint32_t BgL_tmpz00_3466;

																						BgL_tmpz00_3466 =
																							(
																							(BgL_v2z00_1588 <<
																								(int) (8L)) | BgL_v3z00_1589);
																						BgL_arg1477z00_1597 =
																							(long) (BgL_tmpz00_3466);
																					}
																					BgL_arg1469z00_1592 =
																						(uint32_t) (BgL_arg1477z00_1597);
																				}
																				BgL_vz00_1590 =
																					(BgL_arg1468z00_1591 |
																					BgL_arg1469z00_1592);
																			}
																			{	/* Unsafe/sha1.scm 323 */

																				BGL_U32VSET(BgL_vecz00_1578,
																					BgL_jz00_1582, BgL_vz00_1590);
																				BUNSPEC;
													}}}}}}}
													{
														long BgL_jz00_3474;

														BgL_jz00_3474 = (BgL_jz00_1582 + 1L);
														BgL_jz00_1582 = BgL_jz00_3474;
														goto BgL_zc3z04anonymousza31466ze3z87_1583;
													}
												}
											else
												{	/* Unsafe/sha1.scm 317 */
													((bool_t) 0);
												}
										}
										if (((long) CINT(BgL_lz00_1576) < 64L))
											{	/* Unsafe/sha1.scm 327 */
												long BgL_flz00_1610;

												{	/* Unsafe/sha1.scm 327 */
													long BgL_tmpz00_3479;

													{	/* Unsafe/sha1.scm 327 */
														obj_t BgL_tmpz00_3480;

														{	/* Unsafe/sha1.scm 245 */
															obj_t BgL_rz00_2645;

															BgL_rz00_2645 =
																BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT(
																	(BgL_nlenz00_1577 + 1L)), BINT(4L));
															if (INTEGERP(BgL_rz00_2645))
																{	/* Unsafe/sha1.scm 246 */
																	BgL_tmpz00_3480 = BgL_rz00_2645;
																}
															else
																{	/* Unsafe/sha1.scm 246 */
																	BgL_tmpz00_3480 =
																		BINT(
																		(long) (REAL_TO_DOUBLE
																			(BGl_ceilingz00zz__r4_numbers_6_5z00
																				(BgL_rz00_2645))));
														}}
														BgL_tmpz00_3479 = (long) CINT(BgL_tmpz00_3480);
													}
													BgL_flz00_1610 = (BgL_tmpz00_3479 + 2L);
												}
												{	/* Unsafe/sha1.scm 327 */
													long BgL_nz00_1611;

													{	/* Unsafe/sha1.scm 245 */
														obj_t BgL_rz00_2651;

														BgL_rz00_2651 =
															BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT
															(BgL_flz00_1610), BINT(16L));
														if (INTEGERP(BgL_rz00_2651))
															{	/* Unsafe/sha1.scm 246 */
																BgL_nz00_1611 = (long) CINT(BgL_rz00_2651);
															}
														else
															{	/* Unsafe/sha1.scm 246 */
																BgL_nz00_1611 =
																	(long) (REAL_TO_DOUBLE
																	(BGl_ceilingz00zz__r4_numbers_6_5z00
																		(BgL_rz00_2651)));
													}}
													{	/* Unsafe/sha1.scm 328 */
														obj_t BgL_mz00_1612;

														{	/* Unsafe/sha1.scm 331 */
															obj_t BgL_arg1489z00_1613;

															{	/* Unsafe/sha1.scm 331 */
																obj_t BgL_arg1490z00_1614;

																if ((BgL_nz00_1611 > (1L + BgL_nlz00_1573)))
																	{	/* Unsafe/sha1.scm 332 */
																		obj_t BgL_arg1494z00_1617;
																		obj_t BgL_arg1495z00_1618;

																		BgL_arg1494z00_1617 =
																			BGl_makezd2u32vectorzd2zz__srfi4z00(16L,
																			(uint32_t) (0));
																		BgL_arg1495z00_1618 =
																			MAKE_YOUNG_PAIR(BgL_vecz00_1578,
																			BgL_lz00_1572);
																		BgL_arg1490z00_1614 =
																			MAKE_YOUNG_PAIR(BgL_arg1494z00_1617,
																			BgL_arg1495z00_1618);
																	}
																else
																	{	/* Unsafe/sha1.scm 331 */
																		BgL_arg1490z00_1614 =
																			MAKE_YOUNG_PAIR(BgL_vecz00_1578,
																			BgL_lz00_1572);
																	}
																BgL_arg1489z00_1613 =
																	bgl_reverse_bang(BgL_arg1490z00_1614);
															}
															BgL_mz00_1612 =
																BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
																(BgL_arg1489z00_1613);
														}
														{	/* Unsafe/sha1.scm 329 */

															return
																BGl_sha1z00zz__sha1z00(BgL_nlenz00_1577,
																BgL_mz00_1612);
														}
													}
												}
											}
										else
											{	/* Unsafe/sha1.scm 335 */
												obj_t BgL_arg1500z00_1622;
												long BgL_arg1501z00_1623;

												BgL_arg1500z00_1622 =
													MAKE_YOUNG_PAIR(BgL_vecz00_1578, BgL_lz00_1572);
												BgL_arg1501z00_1623 = (BgL_nlz00_1573 + 1L);
												{
													long BgL_nlz00_3516;
													obj_t BgL_lz00_3515;
													long BgL_lenz00_3514;

													BgL_lenz00_3514 = BgL_nlenz00_1577;
													BgL_lz00_3515 = BgL_arg1500z00_1622;
													BgL_nlz00_3516 = BgL_arg1501z00_1623;
													BgL_nlz00_1573 = BgL_nlz00_3516;
													BgL_lz00_1572 = BgL_lz00_3515;
													BgL_lenz00_1571 = BgL_lenz00_3514;
													goto BgL_zc3z04anonymousza31464ze3z87_1574;
												}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &sha1sum-port */
	obj_t BGl_z62sha1sumzd2portzb0zz__sha1z00(obj_t BgL_envz00_2909,
		obj_t BgL_ipz00_2910)
	{
		{	/* Unsafe/sha1.scm 303 */
			{	/* Unsafe/sha1.scm 306 */
				obj_t BgL_auxz00_3517;

				if (INPUT_PORTP(BgL_ipz00_2910))
					{	/* Unsafe/sha1.scm 306 */
						BgL_auxz00_3517 = BgL_ipz00_2910;
					}
				else
					{
						obj_t BgL_auxz00_3520;

						BgL_auxz00_3520 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__sha1z00,
							BINT(11985L), BGl_string1922z00zz__sha1z00,
							BGl_string1923z00zz__sha1z00, BgL_ipz00_2910);
						FAILURE(BgL_auxz00_3520, BFALSE, BFALSE);
					}
				return BGl_sha1sumzd2portzd2zz__sha1z00(BgL_auxz00_3517);
			}
		}

	}



/* sha1sum-file */
	BGL_EXPORTED_DEF obj_t BGl_sha1sumzd2filezd2zz__sha1z00(obj_t BgL_fnamez00_31)
	{
		{	/* Unsafe/sha1.scm 340 */
			{	/* Unsafe/sha1.scm 341 */
				obj_t BgL_mmz00_1628;

				BgL_mmz00_1628 =
					BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_31, BTRUE, BFALSE);
				if (BGL_MMAPP(BgL_mmz00_1628))
					{	/* Unsafe/sha1.scm 343 */
						obj_t BgL_exitd1074z00_1630;

						BgL_exitd1074z00_1630 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Unsafe/sha1.scm 345 */
							obj_t BgL_zc3z04anonymousza31504ze3z87_2911;

							BgL_zc3z04anonymousza31504ze3z87_2911 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31504ze3ze5zz__sha1z00, (int) (0L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31504ze3z87_2911, (int) (0L),
								BgL_mmz00_1628);
							{	/* Unsafe/sha1.scm 343 */
								obj_t BgL_arg1890z00_2661;

								{	/* Unsafe/sha1.scm 343 */
									obj_t BgL_arg1891z00_2662;

									BgL_arg1891z00_2662 =
										BGL_EXITD_PROTECT(BgL_exitd1074z00_1630);
									BgL_arg1890z00_2661 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31504ze3z87_2911,
										BgL_arg1891z00_2662);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1074z00_1630,
									BgL_arg1890z00_2661);
								BUNSPEC;
							}
							{	/* Unsafe/sha1.scm 344 */
								obj_t BgL_tmp1076z00_1632;

								BgL_tmp1076z00_1632 =
									BGl_sha1sumzd2mmapzd2zz__sha1z00(BgL_mmz00_1628);
								{	/* Unsafe/sha1.scm 343 */
									bool_t BgL_test1989z00_3538;

									{	/* Unsafe/sha1.scm 343 */
										obj_t BgL_arg1889z00_2664;

										BgL_arg1889z00_2664 =
											BGL_EXITD_PROTECT(BgL_exitd1074z00_1630);
										BgL_test1989z00_3538 = PAIRP(BgL_arg1889z00_2664);
									}
									if (BgL_test1989z00_3538)
										{	/* Unsafe/sha1.scm 343 */
											obj_t BgL_arg1887z00_2665;

											{	/* Unsafe/sha1.scm 343 */
												obj_t BgL_arg1888z00_2666;

												BgL_arg1888z00_2666 =
													BGL_EXITD_PROTECT(BgL_exitd1074z00_1630);
												BgL_arg1887z00_2665 =
													CDR(((obj_t) BgL_arg1888z00_2666));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1074z00_1630,
												BgL_arg1887z00_2665);
											BUNSPEC;
										}
									else
										{	/* Unsafe/sha1.scm 343 */
											BFALSE;
										}
								}
								bgl_close_mmap(BgL_mmz00_1628);
								return BgL_tmp1076z00_1632;
							}
						}
					}
				else
					{	/* Unsafe/sha1.scm 346 */
						obj_t BgL_pz00_1635;

						{	/* Ieee/port.scm 466 */

							BgL_pz00_1635 =
								BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_fnamez00_31, BTRUE, BINT(5000000L));
						}
						{	/* Unsafe/sha1.scm 347 */
							obj_t BgL_exitd1078z00_1636;

							BgL_exitd1078z00_1636 = BGL_EXITD_TOP_AS_OBJ();
							{	/* Unsafe/sha1.scm 349 */
								obj_t BgL_zc3z04anonymousza31505ze3z87_2912;

								BgL_zc3z04anonymousza31505ze3z87_2912 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31505ze3ze5zz__sha1z00, (int) (0L),
									(int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31505ze3z87_2912, (int) (0L),
									BgL_pz00_1635);
								{	/* Unsafe/sha1.scm 347 */
									obj_t BgL_arg1890z00_2670;

									{	/* Unsafe/sha1.scm 347 */
										obj_t BgL_arg1891z00_2671;

										BgL_arg1891z00_2671 =
											BGL_EXITD_PROTECT(BgL_exitd1078z00_1636);
										BgL_arg1890z00_2670 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31505ze3z87_2912,
											BgL_arg1891z00_2671);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1078z00_1636,
										BgL_arg1890z00_2670);
									BUNSPEC;
								}
								{	/* Unsafe/sha1.scm 348 */
									obj_t BgL_tmp1080z00_1638;

									BgL_tmp1080z00_1638 =
										BGl_sha1sumzd2portzd2zz__sha1z00(BgL_pz00_1635);
									{	/* Unsafe/sha1.scm 347 */
										bool_t BgL_test1990z00_3558;

										{	/* Unsafe/sha1.scm 347 */
											obj_t BgL_arg1889z00_2673;

											BgL_arg1889z00_2673 =
												BGL_EXITD_PROTECT(BgL_exitd1078z00_1636);
											BgL_test1990z00_3558 = PAIRP(BgL_arg1889z00_2673);
										}
										if (BgL_test1990z00_3558)
											{	/* Unsafe/sha1.scm 347 */
												obj_t BgL_arg1887z00_2674;

												{	/* Unsafe/sha1.scm 347 */
													obj_t BgL_arg1888z00_2675;

													BgL_arg1888z00_2675 =
														BGL_EXITD_PROTECT(BgL_exitd1078z00_1636);
													BgL_arg1887z00_2674 =
														CDR(((obj_t) BgL_arg1888z00_2675));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1078z00_1636,
													BgL_arg1887z00_2674);
												BUNSPEC;
											}
										else
											{	/* Unsafe/sha1.scm 347 */
												BFALSE;
											}
									}
									bgl_close_input_port(((obj_t) BgL_pz00_1635));
									return BgL_tmp1080z00_1638;
								}
							}
						}
					}
			}
		}

	}



/* &sha1sum-file */
	obj_t BGl_z62sha1sumzd2filezb0zz__sha1z00(obj_t BgL_envz00_2913,
		obj_t BgL_fnamez00_2914)
	{
		{	/* Unsafe/sha1.scm 340 */
			{	/* Unsafe/sha1.scm 341 */
				obj_t BgL_auxz00_3567;

				if (STRINGP(BgL_fnamez00_2914))
					{	/* Unsafe/sha1.scm 341 */
						BgL_auxz00_3567 = BgL_fnamez00_2914;
					}
				else
					{
						obj_t BgL_auxz00_3570;

						BgL_auxz00_3570 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__sha1z00,
							BINT(13262L), BGl_string1924z00zz__sha1z00,
							BGl_string1919z00zz__sha1z00, BgL_fnamez00_2914);
						FAILURE(BgL_auxz00_3570, BFALSE, BFALSE);
					}
				return BGl_sha1sumzd2filezd2zz__sha1z00(BgL_auxz00_3567);
			}
		}

	}



/* &<@anonymous:1504> */
	obj_t BGl_z62zc3z04anonymousza31504ze3ze5zz__sha1z00(obj_t BgL_envz00_2915)
	{
		{	/* Unsafe/sha1.scm 343 */
			{	/* Unsafe/sha1.scm 345 */
				obj_t BgL_mmz00_2916;

				BgL_mmz00_2916 = ((obj_t) PROCEDURE_REF(BgL_envz00_2915, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_2916);
			}
		}

	}



/* &<@anonymous:1505> */
	obj_t BGl_z62zc3z04anonymousza31505ze3ze5zz__sha1z00(obj_t BgL_envz00_2917)
	{
		{	/* Unsafe/sha1.scm 347 */
			{	/* Unsafe/sha1.scm 349 */
				obj_t BgL_pz00_2918;

				BgL_pz00_2918 = PROCEDURE_REF(BgL_envz00_2917, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_2918));
			}
		}

	}



/* sha1sum */
	BGL_EXPORTED_DEF obj_t BGl_sha1sumz00zz__sha1z00(obj_t BgL_objz00_32)
	{
		{	/* Unsafe/sha1.scm 354 */
			if (BGL_MMAPP(BgL_objz00_32))
				{	/* Unsafe/sha1.scm 356 */
					return BGl_sha1sumzd2mmapzd2zz__sha1z00(BgL_objz00_32);
				}
			else
				{	/* Unsafe/sha1.scm 356 */
					if (STRINGP(BgL_objz00_32))
						{	/* Unsafe/sha1.scm 358 */
							return BGl_sha1sumzd2stringzd2zz__sha1z00(BgL_objz00_32);
						}
					else
						{	/* Unsafe/sha1.scm 358 */
							if (INPUT_PORTP(BgL_objz00_32))
								{	/* Unsafe/sha1.scm 360 */
									return BGl_sha1sumzd2portzd2zz__sha1z00(BgL_objz00_32);
								}
							else
								{	/* Unsafe/sha1.scm 360 */
									return
										BGl_errorz00zz__errorz00(BGl_string1925z00zz__sha1z00,
										BGl_string1926z00zz__sha1z00, BgL_objz00_32);
								}
						}
				}
		}

	}



/* &sha1sum */
	obj_t BGl_z62sha1sumz62zz__sha1z00(obj_t BgL_envz00_2919,
		obj_t BgL_objz00_2920)
	{
		{	/* Unsafe/sha1.scm 354 */
			return BGl_sha1sumz00zz__sha1z00(BgL_objz00_2920);
		}

	}



/* hmac-sha1sum-string */
	BGL_EXPORTED_DEF obj_t BGl_hmaczd2sha1sumzd2stringz00zz__sha1z00(obj_t
		BgL_keyz00_33, obj_t BgL_messagez00_34)
	{
		{	/* Unsafe/sha1.scm 368 */
			return
				BGl_hmaczd2stringzd2zz__hmacz00(BgL_keyz00_33, BgL_messagez00_34,
				BGl_sha1sumzd2stringzd2envz00zz__sha1z00);
		}

	}



/* &hmac-sha1sum-string */
	obj_t BGl_z62hmaczd2sha1sumzd2stringz62zz__sha1z00(obj_t BgL_envz00_2921,
		obj_t BgL_keyz00_2922, obj_t BgL_messagez00_2923)
	{
		{	/* Unsafe/sha1.scm 368 */
			{	/* Unsafe/sha1.scm 369 */
				obj_t BgL_auxz00_3602;
				obj_t BgL_auxz00_3595;

				if (STRINGP(BgL_messagez00_2923))
					{	/* Unsafe/sha1.scm 369 */
						BgL_auxz00_3602 = BgL_messagez00_2923;
					}
				else
					{
						obj_t BgL_auxz00_3605;

						BgL_auxz00_3605 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__sha1z00,
							BINT(14229L), BGl_string1927z00zz__sha1z00,
							BGl_string1919z00zz__sha1z00, BgL_messagez00_2923);
						FAILURE(BgL_auxz00_3605, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_keyz00_2922))
					{	/* Unsafe/sha1.scm 369 */
						BgL_auxz00_3595 = BgL_keyz00_2922;
					}
				else
					{
						obj_t BgL_auxz00_3598;

						BgL_auxz00_3598 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__sha1z00,
							BINT(14229L), BGl_string1927z00zz__sha1z00,
							BGl_string1919z00zz__sha1z00, BgL_keyz00_2922);
						FAILURE(BgL_auxz00_3598, BFALSE, BFALSE);
					}
				return
					BGl_hmaczd2sha1sumzd2stringz00zz__sha1z00(BgL_auxz00_3595,
					BgL_auxz00_3602);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__sha1z00(void)
	{
		{	/* Unsafe/sha1.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__sha1z00(void)
	{
		{	/* Unsafe/sha1.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__sha1z00(void)
	{
		{	/* Unsafe/sha1.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__sha1z00(void)
	{
		{	/* Unsafe/sha1.scm 17 */
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1928z00zz__sha1z00));
			return
				BGl_modulezd2initializa7ationz75zz__hmacz00(285132844L,
				BSTRING_TO_STRING(BGl_string1928z00zz__sha1z00));
		}

	}

#ifdef __cplusplus
}
#endif
