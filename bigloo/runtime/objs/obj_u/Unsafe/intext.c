/*===========================================================================*/
/*   (Unsafe/intext.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/intext.scm -indent -o objs/obj_u/Unsafe/intext.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___INTEXT_TYPE_DEFINITIONS
#define BGL___INTEXT_TYPE_DEFINITIONS

/* object type definitions */

#endif													// BGL___INTEXT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol2641z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2643z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2645z00zz__intextz00 = BUNSPEC;
	extern obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
	static obj_t BGl_z62zc3z04anonymousza31951ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_checkzd2siza7ez12ze70z80zz__intextz00(obj_t, obj_t, long,
		long, obj_t);
	static obj_t
		BGl_z62zc3z04za2stringzd2ze3processza2za31315ze3zd4zz__intextz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2serializa7erz75zz__intextz00(BgL_objectz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2opaquezd2serializa7ationza7zz__intextz00(void);
	static obj_t BGl_symbol2656z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2659z00zz__intextz00 = BUNSPEC;
	static BGL_LONGLONG_T BGl_readzd2longzd2wordze70ze7zz__intextz00(obj_t, obj_t,
		long, obj_t, int);
	static obj_t BGl_requirezd2initializa7ationz75zz__intextz00 = BUNSPEC;
	extern obj_t BGl_makezd2s16vectorzd2zz__srfi4z00(long, int16_t);
	static long BGl_siza7ezd2ofzd2wordza7zz__intextz00(long);
	static obj_t BGl_symbol2664z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2667z00zz__intextz00 = BUNSPEC;
	static long BGl_za2maxzd2siza7ezd2wordza2za7zz__intextz00 = 0L;
	static obj_t BGl_z62printzd2pairzb0zz__intextz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2classzd2serializa7ationz12zb5zz__intextz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_printzd2objzd2zz__intextz00(obj_t, long, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31945ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern ucs2_t BGl_integerzd2ze3ucs2z31zz__ucs2z00(int);
	extern bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2procedurezd2serializa7ationza7zz__intextz00(void);
	extern bool_t BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(obj_t);
	extern obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl__stringzd2ze3objz31zz__intextz00(obj_t, obj_t);
	extern obj_t bgl_seconds_to_date(long);
	extern obj_t BGl_vectorzd2ze3tvectorz31zz__tvectorz00(obj_t, obj_t);
	static obj_t BGl_stringzd2guardz12ze70z27zz__intextz00(obj_t, long, obj_t,
		long);
	static obj_t BGl_toplevelzd2initzd2zz__intextz00(void);
	static obj_t BGl_z62zc3z04anonymousza31955ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2classzd2serializa7ationza7zz__intextz00(obj_t);
	static obj_t BGl_z62setzd2objzd2stringzd2modez12za2zz__intextz00(obj_t,
		obj_t);
	extern obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62getzd2customzd2serializa7ationzc5zz__intextz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31948ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__intextz00(void);
	extern obj_t
		BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(BGL_LONGLONG_T, obj_t);
	extern obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62printzd2cellzb0zz__intextz00(obj_t, obj_t, obj_t);
	extern void bgl_weakptr_data_set(obj_t, obj_t);
	extern obj_t BGl_tvectorzd2ze3vectorz31zz__tvectorz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__intextz00(void);
	BGL_EXPORTED_DECL obj_t string_to_obj(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2procedurezd2serializa7ationz12zb5zz__intextz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2objzd2stringzd2modez12zc0zz__intextz00(obj_t);
	extern obj_t BGl_createzd2hashtablezd2zz__hashz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_readzd2stringze70z35zz__intextz00(obj_t, obj_t, obj_t, obj_t,
		long, obj_t);
	extern bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long, uint32_t);
	static obj_t BGl_markze70ze7zz__intextz00(obj_t, obj_t, obj_t, obj_t);
	extern BGL_LONGLONG_T bgl_date_to_nanoseconds(obj_t);
	static obj_t BGl_excerptz00zz__intextz00(obj_t);
	extern obj_t BGl_pregexpz00zz__regexpz00(obj_t, obj_t);
	static long BGl_markedzd2pairzd2lengthz00zz__intextz00(obj_t, obj_t);
	extern obj_t bgl_weakptr_data(obj_t);
	extern obj_t BGl_makezd2u64vectorzd2zz__srfi4z00(long, uint64_t);
	static obj_t
		BGl_z62zc3z04za2opaquezd2ze3stringza2za31317ze3zd4zz__intextz00(obj_t,
		obj_t);
	static double BGl_readzd2floatze70z35zz__intextz00(obj_t, obj_t, long, obj_t);
	static obj_t BGl__objzd2ze3stringz31zz__intextz00(obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_classzd2fieldzd2mutatorz00zz__objectz00(obj_t);
	extern obj_t make_vector(long, obj_t);
	extern bool_t BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31888ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zz__intextz00(void);
	static obj_t BGl_z62getzd2procedurezd2serializa7ationzc5zz__intextz00(obj_t);
	static obj_t
		BGl_z62zc3z04za2stringzd2ze3opaqueza2za31319ze3zd4zz__intextz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32061ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_classzd2fieldzd2namez00zz__objectz00(obj_t);
	static obj_t BGl_z62printzd2customzb0zz__intextz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_tvectorzd2idzd2zz__tvectorz00(obj_t);
	BGL_EXPORTED_DECL obj_t obj_to_string(obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04za2procedurezd2ze3string1309ze3zd5zz__intextz00(obj_t, obj_t);
	static obj_t BGl_z62objectzd2serializa7erz17zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t make_string(long, unsigned char);
	extern long BGl_classzd2hashzd2zz__objectz00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_classzd2fieldzd2accessorz00zz__objectz00(obj_t);
	extern obj_t BGl_makezd2f32vectorzd2zz__srfi4z00(long, float);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62printzd2itemzb0zz__intextz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2customzd2serializa7ationza7zz__intextz00(obj_t);
	static long BGl_markzd2objz12zc0zz__intextz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2serializa7ationzd2substringza7zz__intextz00(obj_t, long, long);
	extern obj_t BGl_classzd2fieldzd2infoz00zz__objectz00(obj_t);
	extern obj_t BGl_makezd2f64vectorzd2zz__srfi4z00(long, double);
	static obj_t
		BGl_z62registerzd2customzd2serializa7ationz12zd7zz__intextz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_za2classzd2serializa7ationza2z75zz__intextz00 = BUNSPEC;
	extern obj_t BGl_classzd2fieldzd2typez00zz__objectz00(obj_t);
	extern obj_t BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		obj_t);
	extern obj_t BGl_makezd2u16vectorzd2zz__srfi4z00(long, uint16_t);
	extern obj_t BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(obj_t,
		long);
	static bool_t BGl_z62printzd2wordzf2siza7eze5zz__intextz00(obj_t, obj_t,
		obj_t, long);
	extern BgL_objectz00_bglt BGl_allocatezd2instancezd2zz__objectz00(obj_t);
	extern obj_t BGl_makezd2s8vectorzd2zz__srfi4z00(long, int8_t);
	static obj_t
		BGl_z62registerzd2opaquezd2serializa7ationz12zd7zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t string_for_read(obj_t);
	static obj_t BGl_z62printzd2hvectorzb0zz__intextz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__urlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__threadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t ucs2_string_to_utf8_string(obj_t);
	extern obj_t bgl_nanoseconds_to_date(BGL_LONGLONG_T);
	static obj_t
		BGl_z62registerzd2procedurezd2serializa7ationz12zd7zz__intextz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32085ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	static obj_t BGl_za2stringzd2ze3procedureza2z31zz__intextz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31624ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern int bgl_debug(void);
	static obj_t BGl_z62zc3z04anonymousza32086ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62printzd2classzb0zz__intextz00(obj_t, obj_t, obj_t);
	extern obj_t utf8_string_to_ucs2_string(obj_t);
	extern obj_t create_struct(obj_t, int);
	static obj_t BGl_findzd2classzd2unserializa7erza7zz__intextz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2classzd2serializa7ationzc5zz__intextz00(obj_t,
		obj_t);
	static obj_t BGl_z62printzd2objectzb0zz__intextz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__intextz00(void);
	static obj_t BGl_vector2610z00zz__intextz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2processzd2serializa7ationz12zb5zz__intextz00(obj_t, obj_t);
	static obj_t
		BGl_z62registerzd2processzd2serializa7ationz12zd7zz__intextz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__intextz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__intextz00(void);
	static obj_t BGl_z62z12printzd2markupza2zz__intextz00(obj_t, obj_t,
		unsigned char);
	static obj_t BGl_z62zc3z04anonymousza32088ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2customzd2serializa7ationz12zb5zz__intextz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62registerzd2classzd2serializa7ationz12zd7zz__intextz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(obj_t);
	extern obj_t BGl_findzd2classzd2zz__objectz00(obj_t);
	static obj_t BGl_readzd2itemze70z35zz__intextz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, long);
	static obj_t BGl_z62getzd2opaquezd2serializa7ationzc5zz__intextz00(obj_t);
	extern obj_t BGl_makezd2s32vectorzd2zz__srfi4z00(long, int32_t);
	extern obj_t BGl_objectz00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2opaquezd2serializa7ationz12zb5zz__intextz00(obj_t, obj_t);
	static obj_t BGl_za2processzd2ze3stringza2z31zz__intextz00 = BUNSPEC;
	extern obj_t BGl_makezd2s64vectorzd2zz__srfi4z00(long, int64_t);
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	extern obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31920ze3ze5zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_stringzd2ze3bignumz31zz__r4_numbers_6_5_fixnumz00(obj_t,
		long);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62checkzd2bufferz12za2zz__intextz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62printzd2vectorzb0zz__intextz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	extern obj_t make_struct(obj_t, int, obj_t);
	static obj_t BGl_za2stringzd2ze3processza2z31zz__intextz00 = BUNSPEC;
	static obj_t BGl_za2stringzd2ze3opaqueza2z31zz__intextz00 = BUNSPEC;
	static obj_t BGl_za2opaquezd2ze3stringza2z31zz__intextz00 = BUNSPEC;
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static bool_t BGl_za2epairzf3za2zf3zz__intextz00;
	extern obj_t BGl_urlzd2decodezd2zz__urlz00(obj_t);
	static obj_t BGl_z62printzd2tvectorzb0zz__intextz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_make_weakptr(obj_t, obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	extern BGL_LONGLONG_T
		BGl_stringzd2ze3llongz31zz__r4_numbers_6_5_fixnumz00(obj_t, long);
	static obj_t BGl_symbol2603z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2605z00zz__intextz00 = BUNSPEC;
	static obj_t
		BGl_z62zc3z04za2processzd2ze3stringza2za31313ze3zd4zz__intextz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04za2stringzd2ze3procedure1311ze3zd5zz__intextz00(obj_t, obj_t);
	static obj_t BGl_z62printzd2wordzb0zz__intextz00(obj_t, obj_t, long);
	static obj_t BGl_z62printzd2weakptrzb0zz__intextz00(obj_t, obj_t, obj_t);
	static obj_t BGl_keyword2662z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_za2customzd2serializa7ationza2z75zz__intextz00 = BUNSPEC;
	static obj_t BGl_z62makezd2serializa7ationzd2substringzc5zz__intextz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_za2procedurezd2ze3stringza2z31zz__intextz00 = BUNSPEC;
	extern obj_t bgl_real_to_string(double);
	static obj_t BGl_symbol2627z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2629z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_z62objectzd2serializa7er1207z17zz__intextz00(obj_t, obj_t,
		obj_t);
	extern obj_t make_string_sans_fill(long);
	static obj_t BGl_z62printzd2epairzb0zz__intextz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62z12printzd2charsza2zz__intextz00(obj_t, obj_t, obj_t,
		long);
	static long BGl_readzd2wordze70z35zz__intextz00(obj_t, obj_t, long, obj_t,
		int);
	static obj_t BGl_symbol2631z00zz__intextz00 = BUNSPEC;
	static long BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00(obj_t, obj_t, long,
		obj_t);
	static obj_t BGl_symbol2633z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2635z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2637z00zz__intextz00 = BUNSPEC;
	static obj_t BGl_symbol2639z00zz__intextz00 = BUNSPEC;
	extern long BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00(obj_t, long);
	extern obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2procedurezd2serializa7ationz12zd2envz67zz__intextz00,
		BgL_bgl_za762registerza7d2pr2690z00,
		BGl_z62registerzd2procedurezd2serializa7ationz12zd7zz__intextz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2procedurezd2serializa7ationzd2envz75zz__intextz00,
		BgL_bgl_za762getza7d2procedu2691z00,
		BGl_z62getzd2procedurezd2serializa7ationzc5zz__intextz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2600z00zz__intextz00,
		BgL_bgl_string2600za700za7za7_2692za7, "cannot extern process", 21);
	      DEFINE_STRING(BGl_string2601z00zz__intextz00,
		BgL_bgl_string2601za700za7za7_2693za7, "Cannot intern procedure item", 28);
	      DEFINE_STRING(BGl_string2602z00zz__intextz00,
		BgL_bgl_string2602za700za7za7_2694za7, "Cannot extern procedure", 23);
	      DEFINE_STRING(BGl_string2604z00zz__intextz00,
		BgL_bgl_string2604za700za7za7_2695za7, "epair", 5);
	      DEFINE_STRING(BGl_string2606z00zz__intextz00,
		BgL_bgl_string2606za700za7za7_2696za7, "pair", 4);
	      DEFINE_STRING(BGl_string2607z00zz__intextz00,
		BgL_bgl_string2607za700za7za7_2697za7,
		"/tmp/bigloo/runtime/Unsafe/intext.scm", 37);
	      DEFINE_STRING(BGl_string2608z00zz__intextz00,
		BgL_bgl_string2608za700za7za7_2698za7, "_string->obj", 12);
	      DEFINE_STRING(BGl_string2609z00zz__intextz00,
		BgL_bgl_string2609za700za7za7_2699za7, "bstring", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2processzd2serializa7ationz12zd2envz67zz__intextz00,
		BgL_bgl_za762registerza7d2pr2700z00,
		BGl_z62registerzd2processzd2serializa7ationz12zd7zz__intextz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2611z00zz__intextz00,
		BgL_bgl_string2611za700za7za7_2701za7, "definitions", 11);
	      DEFINE_STRING(BGl_string2612z00zz__intextz00,
		BgL_bgl_string2612za700za7za7_2702za7, "float", 5);
	      DEFINE_STRING(BGl_string2613z00zz__intextz00,
		BgL_bgl_string2613za700za7za7_2703za7, "+nan.0", 6);
	      DEFINE_STRING(BGl_string2614z00zz__intextz00,
		BgL_bgl_string2614za700za7za7_2704za7, "+inf.0", 6);
	      DEFINE_STRING(BGl_string2615z00zz__intextz00,
		BgL_bgl_string2615za700za7za7_2705za7, "-inf.0", 6);
	      DEFINE_STRING(BGl_string2616z00zz__intextz00,
		BgL_bgl_string2616za700za7za7_2706za7, "string", 6);
	      DEFINE_STRING(BGl_string2617z00zz__intextz00,
		BgL_bgl_string2617za700za7za7_2707za7, "Cannot find class unserializer",
		30);
	      DEFINE_STRING(BGl_string2618z00zz__intextz00,
		BgL_bgl_string2618za700za7za7_2708za7, "object", 6);
	      DEFINE_STRING(BGl_string2619z00zz__intextz00,
		BgL_bgl_string2619za700za7za7_2709za7, "corrupted class, wrong fields", 29);
	      DEFINE_STRING(BGl_string2620z00zz__intextz00,
		BgL_bgl_string2620za700za7za7_2710za7, "corrupted class, bad signature",
		30);
	      DEFINE_STRING(BGl_string2621z00zz__intextz00,
		BgL_bgl_string2621za700za7za7_2711za7, "structure", 9);
	      DEFINE_STRING(BGl_string2622z00zz__intextz00,
		BgL_bgl_string2622za700za7za7_2712za7, "extended-list", 13);
	      DEFINE_STRING(BGl_string2623z00zz__intextz00,
		BgL_bgl_string2623za700za7za7_2713za7, "list", 4);
	      DEFINE_STRING(BGl_string2624z00zz__intextz00,
		BgL_bgl_string2624za700za7za7_2714za7, "tagged-vector", 13);
	      DEFINE_STRING(BGl_string2625z00zz__intextz00,
		BgL_bgl_string2625za700za7za7_2715za7, "hvector", 7);
	      DEFINE_STRING(BGl_string2626z00zz__intextz00,
		BgL_bgl_string2626za700za7za7_2716za7, "hvector-size", 12);
	      DEFINE_STRING(BGl_string2628z00zz__intextz00,
		BgL_bgl_string2628za700za7za7_2717za7, "s8", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2serializa7ationzd2substringzd2envz75zz__intextz00,
		BgL_bgl_za762makeza7d2serial2718z00,
		BGl_z62makezd2serializa7ationzd2substringzc5zz__intextz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2630z00zz__intextz00,
		BgL_bgl_string2630za700za7za7_2719za7, "u8", 2);
	      DEFINE_STRING(BGl_string2632z00zz__intextz00,
		BgL_bgl_string2632za700za7za7_2720za7, "s16", 3);
	      DEFINE_STRING(BGl_string2634z00zz__intextz00,
		BgL_bgl_string2634za700za7za7_2721za7, "u16", 3);
	      DEFINE_STRING(BGl_string2636z00zz__intextz00,
		BgL_bgl_string2636za700za7za7_2722za7, "s32", 3);
	      DEFINE_STRING(BGl_string2638z00zz__intextz00,
		BgL_bgl_string2638za700za7za7_2723za7, "u32", 3);
	      DEFINE_REAL(BGl_real2658z00zz__intextz00,
		BgL_bgl_real2658za700za7za7__i2724za7, 1.2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2opaquezd2serializa7ationzd2envz75zz__intextz00,
		BgL_bgl_za762getza7d2opaqueza72725za7,
		BGl_z62getzd2opaquezd2serializa7ationzc5zz__intextz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2640z00zz__intextz00,
		BgL_bgl_string2640za700za7za7_2726za7, "s64", 3);
	      DEFINE_STRING(BGl_string2642z00zz__intextz00,
		BgL_bgl_string2642za700za7za7_2727za7, "u64", 3);
	      DEFINE_STRING(BGl_string2644z00zz__intextz00,
		BgL_bgl_string2644za700za7za7_2728za7, "f32", 3);
	      DEFINE_STRING(BGl_string2646z00zz__intextz00,
		BgL_bgl_string2646za700za7za7_2729za7, "f64", 3);
	      DEFINE_STRING(BGl_string2647z00zz__intextz00,
		BgL_bgl_string2647za700za7za7_2730za7, "vector", 6);
	      DEFINE_STRING(BGl_string2648z00zz__intextz00,
		BgL_bgl_string2648za700za7za7_2731za7, "llong", 5);
	      DEFINE_STRING(BGl_string2649z00zz__intextz00,
		BgL_bgl_string2649za700za7za7_2732za7, "elong", 5);
	      DEFINE_STRING(BGl_string2650z00zz__intextz00,
		BgL_bgl_string2650za700za7za7_2733za7, "Cannot find custom unserializer",
		31);
	      DEFINE_STRING(BGl_string2651z00zz__intextz00,
		BgL_bgl_string2651za700za7za7_2734za7, "Cannot unserialize custom object",
		32);
	      DEFINE_STRING(BGl_string2652z00zz__intextz00,
		BgL_bgl_string2652za700za7za7_2735za7, "bignum", 6);
	      DEFINE_STRING(BGl_string2653z00zz__intextz00,
		BgL_bgl_string2653za700za7za7_2736za7,
		"Corrupted string (~a) at index ~a/~a", 36);
	      DEFINE_STRING(BGl_string2654z00zz__intextz00,
		BgL_bgl_string2654za700za7za7_2737za7,
		"Corrupted string (~a) at index ~a/~a ~a", 39);
	      DEFINE_STRING(BGl_string2657z00zz__intextz00,
		BgL_bgl_string2657za700za7za7_2738za7, "none", 4);
	      DEFINE_STRING(BGl_string2660z00zz__intextz00,
		BgL_bgl_string2660za700za7za7_2739za7, "__serialization-substring", 25);
	      DEFINE_STRING(BGl_string2661z00zz__intextz00,
		BgL_bgl_string2661za700za7za7_2740za7, "Unknown object", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2655z00zz__intextz00,
		BgL_bgl_za762za7c3za704anonymo2741za7,
		BGl_z62zc3z04anonymousza31624ze3ze5zz__intextz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2663z00zz__intextz00,
		BgL_bgl_string2663za700za7za7_2742za7, "serialize", 9);
	      DEFINE_STRING(BGl_string2665z00zz__intextz00,
		BgL_bgl_string2665za700za7za7_2743za7, "obj", 3);
	      DEFINE_STRING(BGl_string2666z00zz__intextz00,
		BgL_bgl_string2666za700za7za7_2744za7,
		"Bad type \"~a\" for unserialized field", 36);
	      DEFINE_STRING(BGl_string2668z00zz__intextz00,
		BgL_bgl_string2668za700za7za7_2745za7, "mark", 4);
	      DEFINE_STRING(BGl_string2669z00zz__intextz00,
		BgL_bgl_string2669za700za7za7_2746za7, "Cannot find custom serializer", 29);
	      DEFINE_STRING(BGl_string2670z00zz__intextz00,
		BgL_bgl_string2670za700za7za7_2747za7, "&make-serialization-substring", 29);
	      DEFINE_STRING(BGl_string2671z00zz__intextz00,
		BgL_bgl_string2671za700za7za7_2748za7, "bint", 4);
	      DEFINE_STRING(BGl_string2672z00zz__intextz00,
		BgL_bgl_string2672za700za7za7_2749za7, "register-custom-serialization!",
		30);
	      DEFINE_STRING(BGl_string2673z00zz__intextz00,
		BgL_bgl_string2673za700za7za7_2750za7, "bad arity", 9);
	      DEFINE_STRING(BGl_string2674z00zz__intextz00,
		BgL_bgl_string2674za700za7za7_2751za7, "&register-custom-serialization!",
		31);
	      DEFINE_STRING(BGl_string2675z00zz__intextz00,
		BgL_bgl_string2675za700za7za7_2752za7, "procedure", 9);
	      DEFINE_STRING(BGl_string2676z00zz__intextz00,
		BgL_bgl_string2676za700za7za7_2753za7, "...", 3);
	      DEFINE_STRING(BGl_string2595z00zz__intextz00,
		BgL_bgl_string2595za700za7za7_2754za7, "string->obj", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2589z00zz__intextz00,
		BgL_bgl_za762za7c3za704za7a2proc2755z00,
		BGl_z62zc3z04za2procedurezd2ze3string1309ze3zd5zz__intextz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2677z00zz__intextz00,
		BgL_bgl_string2677za700za7za7_2756za7, "&get-custom-serialization", 25);
	      DEFINE_STRING(BGl_string2596z00zz__intextz00,
		BgL_bgl_string2596za700za7za7_2757za7, "Cannot intern opaque item", 25);
	      DEFINE_STRING(BGl_string2678z00zz__intextz00,
		BgL_bgl_string2678za700za7za7_2758za7, "&register-procedure-serialization!",
		34);
	      DEFINE_STRING(BGl_string2597z00zz__intextz00,
		BgL_bgl_string2597za700za7za7_2759za7, "obj->string", 11);
	      DEFINE_STRING(BGl_string2679z00zz__intextz00,
		BgL_bgl_string2679za700za7za7_2760za7, "&register-process-serialization!",
		32);
	      DEFINE_STRING(BGl_string2598z00zz__intextz00,
		BgL_bgl_string2598za700za7za7_2761za7, "Cannot extern opaque", 20);
	      DEFINE_STRING(BGl_string2599z00zz__intextz00,
		BgL_bgl_string2599za700za7za7_2762za7, "Cannot intern process item", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2590z00zz__intextz00,
		BgL_bgl_za762za7c3za704za7a2stri2763z00,
		BGl_z62zc3z04za2stringzd2ze3procedure1311ze3zd5zz__intextz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2591z00zz__intextz00,
		BgL_bgl_za762za7c3za704za7a2proc2764z00,
		BGl_z62zc3z04za2processzd2ze3stringza2za31313ze3zd4zz__intextz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2592z00zz__intextz00,
		BgL_bgl_za762za7c3za704za7a2stri2765z00,
		BGl_z62zc3z04za2stringzd2ze3processza2za31315ze3zd4zz__intextz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2680z00zz__intextz00,
		BgL_bgl_string2680za700za7za7_2766za7, "&register-opaque-serialization!",
		31);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2593z00zz__intextz00,
		BgL_bgl_za762za7c3za704za7a2opaq2767z00,
		BGl_z62zc3z04za2opaquezd2ze3stringza2za31317ze3zd4zz__intextz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2681z00zz__intextz00,
		BgL_bgl_string2681za700za7za7_2768za7, "register-class-serialization!", 29);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2594z00zz__intextz00,
		BgL_bgl_za762za7c3za704za7a2stri2769z00,
		BGl_z62zc3z04za2stringzd2ze3opaqueza2za31319ze3zd4zz__intextz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2682z00zz__intextz00,
		BgL_bgl_string2682za700za7za7_2770za7, "-serializer", 11);
	      DEFINE_STRING(BGl_string2683z00zz__intextz00,
		BgL_bgl_string2683za700za7za7_2771za7, "&register-class-serialization!",
		30);
	      DEFINE_STRING(BGl_string2684z00zz__intextz00,
		BgL_bgl_string2684za700za7za7_2772za7, "Cannot find ~s class unserializer",
		33);
	      DEFINE_STRING(BGl_string2686z00zz__intextz00,
		BgL_bgl_string2686za700za7za7_2773za7, "object-serializer1207", 21);
	      DEFINE_STRING(BGl_string2687z00zz__intextz00,
		BgL_bgl_string2687za700za7za7_2774za7, "&object-serializer", 18);
	      DEFINE_STRING(BGl_string2688z00zz__intextz00,
		BgL_bgl_string2688za700za7za7_2775za7, "__intext", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2685z00zz__intextz00,
		BgL_bgl_za762objectza7d2seri2776z00,
		BGl_z62objectzd2serializa7er1207z17zz__intextz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2classzd2serializa7ationz12zd2envz67zz__intextz00,
		BgL_bgl_za762registerza7d2cl2777z00,
		BGl_z62registerzd2classzd2serializa7ationz12zd7zz__intextz00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2objzd2stringzd2modez12zd2envz12zz__intextz00,
		BgL_bgl_za762setza7d2objza7d2s2778za7,
		BGl_z62setzd2objzd2stringzd2modez12za2zz__intextz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_objectzd2serializa7erzd2envza7zz__intextz00,
		BgL_bgl_za762objectza7d2seri2779z00,
		BGl_z62objectzd2serializa7erz17zz__intextz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2customzd2serializa7ationz12zd2envz67zz__intextz00,
		BgL_bgl_za762registerza7d2cu2780z00,
		BGl_z62registerzd2customzd2serializa7ationz12zd7zz__intextz00, 0L, BUNSPEC,
		3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stringzd2ze3objzd2envze3zz__intextz00,
		BgL_bgl__stringza7d2za7e3obj2781z00, opt_generic_entry,
		BGl__stringzd2ze3objz31zz__intextz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2customzd2serializa7ationzd2envz75zz__intextz00,
		BgL_bgl_za762getza7d2customza72782za7,
		BGl_z62getzd2customzd2serializa7ationzc5zz__intextz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2opaquezd2serializa7ationz12zd2envz67zz__intextz00,
		BgL_bgl_za762registerza7d2op2783z00,
		BGl_z62registerzd2opaquezd2serializa7ationz12zd7zz__intextz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2classzd2serializa7ationzd2envz75zz__intextz00,
		BgL_bgl_za762getza7d2classza7d2784za7,
		BGl_z62getzd2classzd2serializa7ationzc5zz__intextz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_objzd2ze3stringzd2envze3zz__intextz00,
		BgL_bgl__objza7d2za7e3string2785z00, opt_generic_entry,
		BGl__objzd2ze3stringz31zz__intextz00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2641z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2643z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2645z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2656z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2659z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2664z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2667z00zz__intextz00));
		   
			 ADD_ROOT((void *) (&BGl_za2classzd2serializa7ationza2z75zz__intextz00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2ze3procedureza2z31zz__intextz00));
		     ADD_ROOT((void *) (&BGl_vector2610z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_za2processzd2ze3stringza2z31zz__intextz00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2ze3processza2z31zz__intextz00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2ze3opaqueza2z31zz__intextz00));
		     ADD_ROOT((void *) (&BGl_za2opaquezd2ze3stringza2z31zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2603z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2605z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_keyword2662z00zz__intextz00));
		   
			 ADD_ROOT((void *) (&BGl_za2customzd2serializa7ationza2z75zz__intextz00));
		     ADD_ROOT((void *) (&BGl_za2procedurezd2ze3stringza2z31zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2627z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2629z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2631z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2633z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2635z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2637z00zz__intextz00));
		     ADD_ROOT((void *) (&BGl_symbol2639z00zz__intextz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__intextz00(long
		BgL_checksumz00_7118, char *BgL_fromz00_7119)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__intextz00))
				{
					BGl_requirezd2initializa7ationz75zz__intextz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__intextz00();
					BGl_cnstzd2initzd2zz__intextz00();
					BGl_importedzd2moduleszd2initz00zz__intextz00();
					BGl_genericzd2initzd2zz__intextz00();
					return BGl_toplevelzd2initzd2zz__intextz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			BGl_symbol2603z00zz__intextz00 =
				bstring_to_symbol(BGl_string2604z00zz__intextz00);
			BGl_symbol2605z00zz__intextz00 =
				bstring_to_symbol(BGl_string2606z00zz__intextz00);
			BGl_vector2610z00zz__intextz00 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BNIL);
			BGl_symbol2627z00zz__intextz00 =
				bstring_to_symbol(BGl_string2628z00zz__intextz00);
			BGl_symbol2629z00zz__intextz00 =
				bstring_to_symbol(BGl_string2630z00zz__intextz00);
			BGl_symbol2631z00zz__intextz00 =
				bstring_to_symbol(BGl_string2632z00zz__intextz00);
			BGl_symbol2633z00zz__intextz00 =
				bstring_to_symbol(BGl_string2634z00zz__intextz00);
			BGl_symbol2635z00zz__intextz00 =
				bstring_to_symbol(BGl_string2636z00zz__intextz00);
			BGl_symbol2637z00zz__intextz00 =
				bstring_to_symbol(BGl_string2638z00zz__intextz00);
			BGl_symbol2639z00zz__intextz00 =
				bstring_to_symbol(BGl_string2640z00zz__intextz00);
			BGl_symbol2641z00zz__intextz00 =
				bstring_to_symbol(BGl_string2642z00zz__intextz00);
			BGl_symbol2643z00zz__intextz00 =
				bstring_to_symbol(BGl_string2644z00zz__intextz00);
			BGl_symbol2645z00zz__intextz00 =
				bstring_to_symbol(BGl_string2646z00zz__intextz00);
			BGl_symbol2656z00zz__intextz00 =
				bstring_to_symbol(BGl_string2657z00zz__intextz00);
			BGl_symbol2659z00zz__intextz00 =
				bstring_to_symbol(BGl_string2660z00zz__intextz00);
			BGl_keyword2662z00zz__intextz00 =
				bstring_to_keyword(BGl_string2663z00zz__intextz00);
			BGl_symbol2664z00zz__intextz00 =
				bstring_to_symbol(BGl_string2665z00zz__intextz00);
			return (BGl_symbol2667z00zz__intextz00 =
				bstring_to_symbol(BGl_string2668z00zz__intextz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			BGl_za2epairzf3za2zf3zz__intextz00 = ((bool_t) 1);
			BGl_za2maxzd2siza7ezd2wordza2za7zz__intextz00 = sizeof(long);
			BGl_za2customzd2serializa7ationza2z75zz__intextz00 = BNIL;
			BGl_za2procedurezd2ze3stringza2z31zz__intextz00 =
				BGl_proc2589z00zz__intextz00;
			BGl_za2stringzd2ze3procedureza2z31zz__intextz00 =
				BGl_proc2590z00zz__intextz00;
			BGl_za2processzd2ze3stringza2z31zz__intextz00 =
				BGl_proc2591z00zz__intextz00;
			BGl_za2stringzd2ze3processza2z31zz__intextz00 =
				BGl_proc2592z00zz__intextz00;
			BGl_za2opaquezd2ze3stringza2z31zz__intextz00 =
				BGl_proc2593z00zz__intextz00;
			BGl_za2stringzd2ze3opaqueza2z31zz__intextz00 =
				BGl_proc2594z00zz__intextz00;
			return (BGl_za2classzd2serializa7ationza2z75zz__intextz00 =
				BNIL, BUNSPEC);
		}

	}



/* &<@*string->opaque*:1319> */
	obj_t BGl_z62zc3z04za2stringzd2ze3opaqueza2za31319ze3zd4zz__intextz00(obj_t
		BgL_envz00_6252, obj_t BgL_stringz00_6253)
	{
		{	/* Unsafe/intext.scm 1496 */
			return
				BGl_errorz00zz__errorz00(BGl_string2595z00zz__intextz00,
				BGl_string2596z00zz__intextz00, BgL_stringz00_6253);
		}

	}



/* &<@*opaque->string*:1317> */
	obj_t BGl_z62zc3z04za2opaquezd2ze3stringza2za31317ze3zd4zz__intextz00(obj_t
		BgL_envz00_6254, obj_t BgL_itemz00_6255)
	{
		{	/* Unsafe/intext.scm 1489 */
			return
				BGl_errorz00zz__errorz00(BGl_string2597z00zz__intextz00,
				BGl_string2598z00zz__intextz00,
				BGl_excerptz00zz__intextz00(BgL_itemz00_6255));
		}

	}



/* &<@*string->process*:1315> */
	obj_t BGl_z62zc3z04za2stringzd2ze3processza2za31315ze3zd4zz__intextz00(obj_t
		BgL_envz00_6256, obj_t BgL_stringz00_6257)
	{
		{	/* Unsafe/intext.scm 1469 */
			return
				BGl_errorz00zz__errorz00(BGl_string2595z00zz__intextz00,
				BGl_string2599z00zz__intextz00,
				BGl_excerptz00zz__intextz00(BgL_stringz00_6257));
		}

	}



/* &<@*process->string*:1313> */
	obj_t BGl_z62zc3z04za2processzd2ze3stringza2za31313ze3zd4zz__intextz00(obj_t
		BgL_envz00_6258, obj_t BgL_itemz00_6259)
	{
		{	/* Unsafe/intext.scm 1462 */
			return
				BGl_errorz00zz__errorz00(BGl_string2597z00zz__intextz00,
				BGl_string2600z00zz__intextz00,
				BGl_excerptz00zz__intextz00(BgL_itemz00_6259));
		}

	}



/* &<@*string->procedure1311> */
	obj_t BGl_z62zc3z04za2stringzd2ze3procedure1311ze3zd5zz__intextz00(obj_t
		BgL_envz00_6260, obj_t BgL_stringz00_6261)
	{
		{	/* Unsafe/intext.scm 1442 */
			return
				BGl_errorz00zz__errorz00(BGl_string2595z00zz__intextz00,
				BGl_string2601z00zz__intextz00,
				BGl_excerptz00zz__intextz00(BgL_stringz00_6261));
		}

	}



/* &<@*procedure->string1309> */
	obj_t BGl_z62zc3z04za2procedurezd2ze3string1309ze3zd5zz__intextz00(obj_t
		BgL_envz00_6262, obj_t BgL_itemz00_6263)
	{
		{	/* Unsafe/intext.scm 1435 */
			return
				BGl_errorz00zz__errorz00(BGl_string2597z00zz__intextz00,
				BGl_string2602z00zz__intextz00,
				BGl_excerptz00zz__intextz00(BgL_itemz00_6263));
		}

	}



/* set-obj-string-mode! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2objzd2stringzd2modez12zc0zz__intextz00(obj_t
		BgL_modez00_17)
	{
		{	/* Unsafe/intext.scm 143 */
			if ((BgL_modez00_17 == BGl_symbol2603z00zz__intextz00))
				{	/* Unsafe/intext.scm 144 */
					return (BGl_za2epairzf3za2zf3zz__intextz00 = ((bool_t) 1), BUNSPEC);
				}
			else
				{	/* Unsafe/intext.scm 144 */
					if ((BgL_modez00_17 == BGl_symbol2605z00zz__intextz00))
						{	/* Unsafe/intext.scm 144 */
							return (BGl_za2epairzf3za2zf3zz__intextz00 =
								((bool_t) 0), BUNSPEC);
						}
					else
						{	/* Unsafe/intext.scm 144 */
							return BUNSPEC;
						}
				}
		}

	}



/* &set-obj-string-mode! */
	obj_t BGl_z62setzd2objzd2stringzd2modez12za2zz__intextz00(obj_t
		BgL_envz00_6264, obj_t BgL_modez00_6265)
	{
		{	/* Unsafe/intext.scm 143 */
			return BGl_setzd2objzd2stringzd2modez12zc0zz__intextz00(BgL_modez00_6265);
		}

	}



/* _string->obj */
	obj_t BGl__stringzd2ze3objz31zz__intextz00(obj_t BgL_env1198z00_22,
		obj_t BgL_opt1197z00_21)
	{
		{	/* Unsafe/intext.scm 153 */
			{	/* Unsafe/intext.scm 153 */
				obj_t BgL_g1199z00_1539;

				BgL_g1199z00_1539 = VECTOR_REF(BgL_opt1197z00_21, 0L);
				switch (VECTOR_LENGTH(BgL_opt1197z00_21))
					{
					case 1L:

						{	/* Unsafe/intext.scm 153 */

							{	/* Unsafe/intext.scm 153 */
								obj_t BgL_auxz00_7164;

								if (STRINGP(BgL_g1199z00_1539))
									{	/* Unsafe/intext.scm 153 */
										BgL_auxz00_7164 = BgL_g1199z00_1539;
									}
								else
									{
										obj_t BgL_auxz00_7167;

										BgL_auxz00_7167 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2607z00zz__intextz00, BINT(5579L),
											BGl_string2608z00zz__intextz00,
											BGl_string2609z00zz__intextz00, BgL_g1199z00_1539);
										FAILURE(BgL_auxz00_7167, BFALSE, BFALSE);
									}
								return string_to_obj(BgL_auxz00_7164, BFALSE, BFALSE);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/intext.scm 153 */
							obj_t BgL_extensionz00_1544;

							BgL_extensionz00_1544 = VECTOR_REF(BgL_opt1197z00_21, 1L);
							{	/* Unsafe/intext.scm 153 */

								{	/* Unsafe/intext.scm 153 */
									obj_t BgL_auxz00_7173;

									if (STRINGP(BgL_g1199z00_1539))
										{	/* Unsafe/intext.scm 153 */
											BgL_auxz00_7173 = BgL_g1199z00_1539;
										}
									else
										{
											obj_t BgL_auxz00_7176;

											BgL_auxz00_7176 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2607z00zz__intextz00, BINT(5579L),
												BGl_string2608z00zz__intextz00,
												BGl_string2609z00zz__intextz00, BgL_g1199z00_1539);
											FAILURE(BgL_auxz00_7176, BFALSE, BFALSE);
										}
									return
										string_to_obj(BgL_auxz00_7173, BgL_extensionz00_1544,
										BFALSE);
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/intext.scm 153 */
							obj_t BgL_extensionz00_1546;

							BgL_extensionz00_1546 = VECTOR_REF(BgL_opt1197z00_21, 1L);
							{	/* Unsafe/intext.scm 153 */
								obj_t BgL_unserializa7ezd2argz75_1547;

								BgL_unserializa7ezd2argz75_1547 =
									VECTOR_REF(BgL_opt1197z00_21, 2L);
								{	/* Unsafe/intext.scm 153 */

									{	/* Unsafe/intext.scm 153 */
										obj_t BgL_auxz00_7183;

										if (STRINGP(BgL_g1199z00_1539))
											{	/* Unsafe/intext.scm 153 */
												BgL_auxz00_7183 = BgL_g1199z00_1539;
											}
										else
											{
												obj_t BgL_auxz00_7186;

												BgL_auxz00_7186 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2607z00zz__intextz00, BINT(5579L),
													BGl_string2608z00zz__intextz00,
													BGl_string2609z00zz__intextz00, BgL_g1199z00_1539);
												FAILURE(BgL_auxz00_7186, BFALSE, BFALSE);
											}
										return
											string_to_obj(BgL_auxz00_7183, BgL_extensionz00_1546,
											BgL_unserializa7ezd2argz75_1547);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* string->obj */
	BGL_EXPORTED_DEF obj_t string_to_obj(obj_t BgL_sz00_18,
		obj_t BgL_extensionz00_19, obj_t BgL_unserializa7ezd2argz75_20)
	{
		{	/* Unsafe/intext.scm 153 */
			{	/* Unsafe/intext.scm 87 */
				struct bgl_cell BgL_box2792_6730z00;
				obj_t BgL_za2pointerza2z00_6730;

				BgL_za2pointerza2z00_6730 =
					MAKE_CELL_STACK(BINT(0L), BgL_box2792_6730z00);
				{	/* Unsafe/intext.scm 162 */
					struct bgl_cell BgL_box2793_6731z00;
					obj_t BgL_za2definitionsza2z00_6731;

					BgL_za2definitionsza2z00_6731 =
						MAKE_CELL_STACK(BGl_vector2610z00zz__intextz00,
						BgL_box2793_6731z00);
					{	/* Unsafe/intext.scm 87 */
						struct bgl_cell BgL_box2794_6732z00;
						obj_t BgL_za2definingza2z00_6732;

						BgL_za2definingza2z00_6732 =
							MAKE_CELL_STACK(BFALSE, BgL_box2794_6732z00);
						{	/* Unsafe/intext.scm 159 */
							long BgL_za2strlenza2z00_1551;

							BgL_za2strlenza2z00_1551 = STRING_LENGTH(BgL_sz00_18);
							BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_18,
								BgL_za2strlenza2z00_1551, BgL_za2pointerza2z00_6730, 1L);
							{	/* Unsafe/intext.scm 636 */
								unsigned char BgL_dz00_1597;

								BgL_dz00_1597 =
									STRING_REF(BgL_sz00_18,
									(long) CINT(
										((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6730)))));
								if ((BgL_dz00_1597 == ((unsigned char) 'c')))
									{	/* Unsafe/intext.scm 637 */
										{	/* Unsafe/intext.scm 638 */
											obj_t BgL_auxz00_6733;

											BgL_auxz00_6733 =
												ADDFX(
												((obj_t)
													((obj_t) CELL_REF(BgL_za2pointerza2z00_6730))),
												BINT(1L));
											CELL_SET(BgL_za2pointerza2z00_6730, BgL_auxz00_6733);
										}
										{	/* Unsafe/intext.scm 639 */
											obj_t BgL_auxz00_6734;

											{	/* Unsafe/intext.scm 639 */
												long BgL_arg1338z00_1599;

												{	/* Unsafe/intext.scm 234 */
													long BgL_siza7eza7_4065;

													BgL_siza7eza7_4065 =
														BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
														(BgL_za2pointerza2z00_6730, BgL_sz00_18,
														BgL_za2strlenza2z00_1551, BgL_sz00_18);
													BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_18,
														BgL_za2pointerza2z00_6730, BgL_za2strlenza2z00_1551,
														BgL_siza7eza7_4065, BGl_string2611z00zz__intextz00);
													BgL_arg1338z00_1599 = BgL_siza7eza7_4065;
												}
												BgL_auxz00_6734 =
													make_vector(BgL_arg1338z00_1599, BUNSPEC);
											}
											CELL_SET(BgL_za2definitionsza2z00_6731, BgL_auxz00_6734);
									}}
								else
									{	/* Unsafe/intext.scm 637 */
										BFALSE;
									}
							}
							return
								BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_19,
								BgL_sz00_18, BgL_za2definitionsza2z00_6731,
								BgL_unserializa7ezd2argz75_20, BgL_za2definingza2z00_6732,
								BgL_za2pointerza2z00_6730, BgL_za2strlenza2z00_1551);
						}
					}
				}
			}
		}

	}



/* read-float~0 */
	double BGl_readzd2floatze70z35zz__intextz00(obj_t BgL_za2pointerza2z00_6676,
		obj_t BgL_sz00_6675, long BgL_za2strlenza2z00_6674, obj_t BgL_sz00_1626)
	{
		{	/* Unsafe/intext.scm 191 */
			{	/* Unsafe/intext.scm 188 */
				long BgL_sza7za7_1628;

				{	/* Unsafe/intext.scm 234 */
					long BgL_siza7eza7_3566;

					BgL_siza7eza7_3566 =
						BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
						(BgL_za2pointerza2z00_6676, BgL_sz00_6675, BgL_za2strlenza2z00_6674,
						BgL_sz00_1626);
					BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6675,
						BgL_za2pointerza2z00_6676, BgL_za2strlenza2z00_6674,
						BgL_siza7eza7_3566, BGl_string2612z00zz__intextz00);
					BgL_sza7za7_1628 = BgL_siza7eza7_3566;
				}
				{	/* Unsafe/intext.scm 188 */
					double BgL_resz00_1629;

					{	/* Unsafe/intext.scm 189 */
						obj_t BgL_arg1362z00_1630;

						{	/* Unsafe/intext.scm 189 */
							long BgL_arg1363z00_1631;

							{	/* Unsafe/intext.scm 189 */
								long BgL_za71za7_3567;

								BgL_za71za7_3567 =
									(long) CINT(
									((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6676))));
								BgL_arg1363z00_1631 = (BgL_za71za7_3567 + BgL_sza7za7_1628);
							}
							{	/* Unsafe/intext.scm 189 */
								long BgL_startz00_3570;

								BgL_startz00_3570 =
									(long) CINT(
									((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6676))));
								BgL_arg1362z00_1630 =
									c_substring(BgL_sz00_1626, BgL_startz00_3570,
									BgL_arg1363z00_1631);
						}}
						{	/* Unsafe/intext.scm 189 */
							char *BgL_stringz00_3572;

							BgL_stringz00_3572 = BSTRING_TO_STRING(BgL_arg1362z00_1630);
							{	/* Unsafe/intext.scm 189 */
								bool_t BgL_test2796z00_7217;

								{	/* Unsafe/intext.scm 189 */
									obj_t BgL_string1z00_3576;

									BgL_string1z00_3576 = string_to_bstring(BgL_stringz00_3572);
									{	/* Unsafe/intext.scm 189 */
										long BgL_l1z00_3578;

										BgL_l1z00_3578 = STRING_LENGTH(BgL_string1z00_3576);
										if ((BgL_l1z00_3578 == 6L))
											{	/* Unsafe/intext.scm 189 */
												int BgL_arg2219z00_3581;

												{	/* Unsafe/intext.scm 189 */
													char *BgL_auxz00_7224;
													char *BgL_tmpz00_7222;

													BgL_auxz00_7224 =
														BSTRING_TO_STRING(BGl_string2613z00zz__intextz00);
													BgL_tmpz00_7222 =
														BSTRING_TO_STRING(BgL_string1z00_3576);
													BgL_arg2219z00_3581 =
														memcmp(BgL_tmpz00_7222, BgL_auxz00_7224,
														BgL_l1z00_3578);
												}
												BgL_test2796z00_7217 =
													((long) (BgL_arg2219z00_3581) == 0L);
											}
										else
											{	/* Unsafe/intext.scm 189 */
												BgL_test2796z00_7217 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2796z00_7217)
									{	/* Unsafe/intext.scm 189 */
										BgL_resz00_1629 = BGL_NAN;
									}
								else
									{	/* Unsafe/intext.scm 189 */
										bool_t BgL_test2798z00_7229;

										{	/* Unsafe/intext.scm 189 */
											obj_t BgL_string1z00_3587;

											BgL_string1z00_3587 =
												string_to_bstring(BgL_stringz00_3572);
											{	/* Unsafe/intext.scm 189 */
												long BgL_l1z00_3589;

												BgL_l1z00_3589 = STRING_LENGTH(BgL_string1z00_3587);
												if ((BgL_l1z00_3589 == 6L))
													{	/* Unsafe/intext.scm 189 */
														int BgL_arg2219z00_3592;

														{	/* Unsafe/intext.scm 189 */
															char *BgL_auxz00_7236;
															char *BgL_tmpz00_7234;

															BgL_auxz00_7236 =
																BSTRING_TO_STRING
																(BGl_string2614z00zz__intextz00);
															BgL_tmpz00_7234 =
																BSTRING_TO_STRING(BgL_string1z00_3587);
															BgL_arg2219z00_3592 =
																memcmp(BgL_tmpz00_7234, BgL_auxz00_7236,
																BgL_l1z00_3589);
														}
														BgL_test2798z00_7229 =
															((long) (BgL_arg2219z00_3592) == 0L);
													}
												else
													{	/* Unsafe/intext.scm 189 */
														BgL_test2798z00_7229 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2798z00_7229)
											{	/* Unsafe/intext.scm 189 */
												BgL_resz00_1629 = BGL_INFINITY;
											}
										else
											{	/* Unsafe/intext.scm 189 */
												bool_t BgL_test2800z00_7241;

												{	/* Unsafe/intext.scm 189 */
													obj_t BgL_string1z00_3598;

													BgL_string1z00_3598 =
														string_to_bstring(BgL_stringz00_3572);
													{	/* Unsafe/intext.scm 189 */
														long BgL_l1z00_3600;

														BgL_l1z00_3600 = STRING_LENGTH(BgL_string1z00_3598);
														if ((BgL_l1z00_3600 == 6L))
															{	/* Unsafe/intext.scm 189 */
																int BgL_arg2219z00_3603;

																{	/* Unsafe/intext.scm 189 */
																	char *BgL_auxz00_7248;
																	char *BgL_tmpz00_7246;

																	BgL_auxz00_7248 =
																		BSTRING_TO_STRING
																		(BGl_string2615z00zz__intextz00);
																	BgL_tmpz00_7246 =
																		BSTRING_TO_STRING(BgL_string1z00_3598);
																	BgL_arg2219z00_3603 =
																		memcmp(BgL_tmpz00_7246, BgL_auxz00_7248,
																		BgL_l1z00_3600);
																}
																BgL_test2800z00_7241 =
																	((long) (BgL_arg2219z00_3603) == 0L);
															}
														else
															{	/* Unsafe/intext.scm 189 */
																BgL_test2800z00_7241 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2800z00_7241)
													{	/* Unsafe/intext.scm 189 */
														BgL_resz00_1629 = (-BGL_INFINITY);
													}
												else
													{	/* Unsafe/intext.scm 189 */
														BgL_resz00_1629 = STRTOD(BgL_stringz00_3572);
													}
											}
									}
							}
						}
					}
					{	/* Unsafe/intext.scm 189 */

						{	/* Unsafe/intext.scm 190 */
							obj_t BgL_auxz00_6677;

							BgL_auxz00_6677 =
								ADDFX(
								((obj_t)
									((obj_t) CELL_REF(BgL_za2pointerza2z00_6676))),
								BINT(BgL_sza7za7_1628));
							CELL_SET(BgL_za2pointerza2z00_6676, BgL_auxz00_6677);
						}
						return BgL_resz00_1629;
					}
				}
			}
		}

	}



/* read-long-word~0 */
	BGL_LONGLONG_T BGl_readzd2longzd2wordze70ze7zz__intextz00(obj_t
		BgL_za2pointerza2z00_6680, obj_t BgL_sz00_6679,
		long BgL_za2strlenza2z00_6678, obj_t BgL_sz00_1656, int BgL_sza7za7_1657)
	{
		{	/* Unsafe/intext.scm 223 */
			{	/* Unsafe/intext.scm 216 */
				BGL_LONGLONG_T BgL_accz00_1659;

				BgL_accz00_1659 = LONG_TO_LLONG(0L);
				BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_6679,
					BgL_za2strlenza2z00_6678, BgL_za2pointerza2z00_6680,
					(long) (BgL_sza7za7_1657));
				{
					long BgL_iz00_1661;

					BgL_iz00_1661 = 0L;
				BgL_zc3z04anonymousza31378ze3z87_1662:
					if ((BgL_iz00_1661 < (long) (BgL_sza7za7_1657)))
						{	/* Unsafe/intext.scm 218 */
							{	/* Unsafe/intext.scm 219 */
								unsigned char BgL_dz00_1664;

								{	/* Unsafe/intext.scm 219 */
									long BgL_kz00_3634;

									BgL_kz00_3634 =
										(long) CINT(
										((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6680))));
									BgL_dz00_1664 = STRING_REF(BgL_sz00_1656, BgL_kz00_3634);
								}
								{	/* Unsafe/intext.scm 220 */
									BGL_LONGLONG_T BgL_arg1380z00_1665;
									BGL_LONGLONG_T BgL_arg1382z00_1666;

									BgL_arg1380z00_1665 =
										(((BGL_LONGLONG_T) 256) * BgL_accz00_1659);
									{	/* Unsafe/intext.scm 221 */
										long BgL_tmpz00_7267;

										BgL_tmpz00_7267 = (BgL_dz00_1664);
										BgL_arg1382z00_1666 = LONG_TO_LLONG(BgL_tmpz00_7267);
									}
									BgL_accz00_1659 = (BgL_arg1380z00_1665 + BgL_arg1382z00_1666);
								}
								{	/* Unsafe/intext.scm 222 */
									obj_t BgL_auxz00_6681;

									BgL_auxz00_6681 =
										ADDFX(
										((obj_t)
											((obj_t) CELL_REF(BgL_za2pointerza2z00_6680))), BINT(1L));
									CELL_SET(BgL_za2pointerza2z00_6680, BgL_auxz00_6681);
							}}
							{
								long BgL_iz00_7274;

								BgL_iz00_7274 = (BgL_iz00_1661 + 1L);
								BgL_iz00_1661 = BgL_iz00_7274;
								goto BgL_zc3z04anonymousza31378ze3z87_1662;
							}
						}
					else
						{	/* Unsafe/intext.scm 218 */
							((bool_t) 0);
						}
				}
				return BgL_accz00_1659;
			}
		}

	}



/* read-string~0 */
	obj_t BGl_readzd2stringze70z35zz__intextz00(obj_t BgL_za2definingza2z00_6686,
		obj_t BgL_za2definitionsza2z00_6685, obj_t BgL_za2pointerza2z00_6684,
		obj_t BgL_sz00_6683, long BgL_za2strlenza2z00_6682, obj_t BgL_sz00_1678)
	{
		{	/* Unsafe/intext.scm 246 */
			{	/* Unsafe/intext.scm 240 */
				long BgL_sza7za7_1680;

				{	/* Unsafe/intext.scm 234 */
					long BgL_siza7eza7_3656;

					BgL_siza7eza7_3656 =
						BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
						(BgL_za2pointerza2z00_6684, BgL_sz00_6683, BgL_za2strlenza2z00_6682,
						BgL_sz00_1678);
					BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6683,
						BgL_za2pointerza2z00_6684, BgL_za2strlenza2z00_6682,
						BgL_siza7eza7_3656, BGl_string2616z00zz__intextz00);
					BgL_sza7za7_1680 = BgL_siza7eza7_3656;
				}
				{	/* Unsafe/intext.scm 240 */
					obj_t BgL_resz00_1681;

					{	/* Unsafe/intext.scm 241 */
						long BgL_arg1391z00_1683;

						{	/* Unsafe/intext.scm 241 */
							long BgL_za71za7_3657;

							BgL_za71za7_3657 =
								(long) CINT(
								((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6684))));
							BgL_arg1391z00_1683 = (BgL_za71za7_3657 + BgL_sza7za7_1680);
						}
						{	/* Unsafe/intext.scm 241 */
							long BgL_startz00_3660;

							BgL_startz00_3660 =
								(long) CINT(
								((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6684))));
							BgL_resz00_1681 =
								c_substring(BgL_sz00_1678, BgL_startz00_3660,
								BgL_arg1391z00_1683);
					}}
					{	/* Unsafe/intext.scm 241 */

						{	/* Unsafe/intext.scm 242 */
							bool_t BgL_test2803z00_7284;

							{	/* Unsafe/intext.scm 242 */
								obj_t BgL_objz00_3662;

								BgL_objz00_3662 = CELL_REF(BgL_za2definingza2z00_6686);
								BgL_test2803z00_7284 = INTEGERP(BgL_objz00_3662);
							}
							if (BgL_test2803z00_7284)
								{	/* Unsafe/intext.scm 242 */
									{	/* Unsafe/intext.scm 243 */
										obj_t BgL_vectorz00_3663;
										long BgL_kz00_3664;

										BgL_vectorz00_3663 =
											((obj_t)
											((obj_t) CELL_REF(BgL_za2definitionsza2z00_6685)));
										BgL_kz00_3664 =
											(long) CINT(CELL_REF(BgL_za2definingza2z00_6686));
										VECTOR_SET(BgL_vectorz00_3663, BgL_kz00_3664,
											BgL_resz00_1681);
									}
									{	/* Unsafe/intext.scm 244 */
										obj_t BgL_auxz00_6687;

										BgL_auxz00_6687 = BFALSE;
										CELL_SET(BgL_za2definingza2z00_6686, BgL_auxz00_6687);
								}}
							else
								{	/* Unsafe/intext.scm 242 */
									BFALSE;
								}
						}
						{	/* Unsafe/intext.scm 245 */
							obj_t BgL_auxz00_6688;

							BgL_auxz00_6688 =
								ADDFX(
								((obj_t)
									((obj_t) CELL_REF(BgL_za2pointerza2z00_6684))),
								BINT(BgL_sza7za7_1680));
							CELL_SET(BgL_za2pointerza2z00_6684, BgL_auxz00_6688);
						}
						return BgL_resz00_1681;
					}
				}
			}
		}

	}



/* read-item~0 */
	obj_t BGl_readzd2itemze70z35zz__intextz00(obj_t BgL_extensionz00_6695,
		obj_t BgL_sz00_6694, obj_t BgL_za2definitionsza2z00_6693,
		obj_t BgL_unserializa7ezd2argz75_6692, obj_t BgL_za2definingza2z00_6691,
		obj_t BgL_za2pointerza2z00_6690, long BgL_za2strlenza2z00_6689)
	{
		{	/* Unsafe/intext.scm 580 */
		BGl_readzd2itemze70z35zz__intextz00:
			{
				obj_t BgL_sz00_1632;
				obj_t BgL_sz00_1929;
				obj_t BgL_sz00_2016;
				obj_t BgL_sz00_2029;

				BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_6694,
					BgL_za2strlenza2z00_6689, BgL_za2pointerza2z00_6690, 1L);
				{	/* Unsafe/intext.scm 581 */
					unsigned char BgL_dz00_2043;

					{	/* Unsafe/intext.scm 581 */
						long BgL_kz00_4019;

						BgL_kz00_4019 =
							(long) CINT(
							((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
						BgL_dz00_2043 = (char) (STRING_REF(BgL_sz00_6694, BgL_kz00_4019));
					}
					{	/* Unsafe/intext.scm 582 */
						obj_t BgL_auxz00_6696;

						BgL_auxz00_6696 =
							ADDFX(
							((obj_t)
								((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))), BINT(1L));
						CELL_SET(BgL_za2pointerza2z00_6690, BgL_auxz00_6696);
					}
					switch ((unsigned char) (BgL_dz00_2043))
						{
						case ((unsigned char) '='):

							{	/* Unsafe/intext.scm 250 */
								obj_t BgL_auxz00_6697;

								BgL_auxz00_6697 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6697);
							}
							{

								goto BGl_readzd2itemze70z35zz__intextz00;
							}
							break;
						case ((unsigned char) '#'):

							{	/* Unsafe/intext.scm 255 */
								obj_t BgL_arg1394z00_4022;

								BgL_arg1394z00_4022 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								{	/* Unsafe/intext.scm 255 */
									obj_t BgL_vectorz00_4023;
									long BgL_kz00_4024;

									BgL_vectorz00_4023 =
										((obj_t) ((obj_t) CELL_REF(BgL_za2definitionsza2z00_6693)));
									BgL_kz00_4024 = (long) CINT(BgL_arg1394z00_4022);
									return VECTOR_REF(BgL_vectorz00_4023, BgL_kz00_4024);
								}
							}
							break;
						case ((unsigned char) '\''):

							{	/* Unsafe/intext.scm 259 */
								obj_t BgL_arg1396z00_4025;

								BgL_arg1396z00_4025 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								return bstring_to_symbol(((obj_t) BgL_arg1396z00_4025));
							}
							break;
						case ((unsigned char) ':'):

							{	/* Unsafe/intext.scm 263 */
								obj_t BgL_arg1399z00_4027;

								BgL_arg1399z00_4027 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								return bstring_to_keyword(((obj_t) BgL_arg1399z00_4027));
							}
							break;
						case ((unsigned char) 'a'):

							{	/* Unsafe/intext.scm 202 */
								long BgL_arg1369z00_4029;

								BgL_arg1369z00_4029 =
									BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
									(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694);
								return BCHAR((BgL_arg1369z00_4029));
							}
							break;
						case ((unsigned char) 'F'):

							return BFALSE;
							break;
						case ((unsigned char) 'T'):

							return BTRUE;
							break;
						case ((unsigned char) ';'):

							return BUNSPEC;
							break;
						case ((unsigned char) '.'):

							return BNIL;
							break;
						case ((unsigned char) '<'):

							{	/* Unsafe/intext.scm 267 */
								long BgL_tmpz00_7314;

								BgL_tmpz00_7314 =
									BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
									(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694);
								return BCNST(BgL_tmpz00_7314);
							}
							break;
						case ((unsigned char) '"'):

							return
								BGl_readzd2stringze70z35zz__intextz00
								(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
								BgL_za2pointerza2z00_6690, BgL_sz00_6694,
								BgL_za2strlenza2z00_6689, BgL_sz00_6694);
							break;
						case ((unsigned char) '`'):

							return
								BGl_readzd2stringze70z35zz__intextz00
								(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
								BgL_za2pointerza2z00_6690, BgL_sz00_6694,
								BgL_za2strlenza2z00_6689, BgL_sz00_6694);
							break;
						case ((unsigned char) '%'):

							return
								BGl_urlzd2decodezd2zz__urlz00
								(BGl_readzd2stringze70z35zz__intextz00
								(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
									BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694));
							break;
						case ((unsigned char) 'U'):

							return
								utf8_string_to_ucs2_string(BGl_readzd2stringze70z35zz__intextz00
								(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
									BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694));
							break;
						case ((unsigned char) '['):

							{	/* Unsafe/intext.scm 305 */
								long BgL_sza7za7_1718;

								{	/* Unsafe/intext.scm 234 */
									long BgL_siza7eza7_3701;

									BgL_siza7eza7_3701 =
										BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
										(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
										BgL_siza7eza7_3701, BGl_string2647z00zz__intextz00);
									BgL_sza7za7_1718 = BgL_siza7eza7_3701;
								}
								{	/* Unsafe/intext.scm 305 */
									obj_t BgL_resz00_1719;

									BgL_resz00_1719 = create_vector(BgL_sza7za7_1718);
									{	/* Unsafe/intext.scm 306 */

										{	/* Unsafe/intext.scm 307 */
											bool_t BgL_test2804z00_7326;

											{	/* Unsafe/intext.scm 307 */
												obj_t BgL_objz00_3702;

												BgL_objz00_3702 = CELL_REF(BgL_za2definingza2z00_6691);
												BgL_test2804z00_7326 = INTEGERP(BgL_objz00_3702);
											}
											if (BgL_test2804z00_7326)
												{	/* Unsafe/intext.scm 307 */
													{	/* Unsafe/intext.scm 308 */
														obj_t BgL_vectorz00_3703;
														long BgL_kz00_3704;

														BgL_vectorz00_3703 =
															((obj_t)
															((obj_t)
																CELL_REF(BgL_za2definitionsza2z00_6693)));
														BgL_kz00_3704 =
															(long) CINT(CELL_REF(BgL_za2definingza2z00_6691));
														VECTOR_SET(BgL_vectorz00_3703, BgL_kz00_3704,
															BgL_resz00_1719);
													}
													{	/* Unsafe/intext.scm 309 */
														obj_t BgL_auxz00_6712;

														BgL_auxz00_6712 = BFALSE;
														CELL_SET(BgL_za2definingza2z00_6691,
															BgL_auxz00_6712);
												}}
											else
												{	/* Unsafe/intext.scm 307 */
													BFALSE;
												}
										}
										{
											long BgL_iz00_3711;

											BgL_iz00_3711 = 0L;
										BgL_for1044z00_3710:
											if ((BgL_iz00_3711 < BgL_sza7za7_1718))
												{	/* Unsafe/intext.scm 310 */
													VECTOR_SET(BgL_resz00_1719, BgL_iz00_3711,
														BGl_readzd2itemze70z35zz__intextz00
														(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689));
													{
														long BgL_iz00_7335;

														BgL_iz00_7335 = (BgL_iz00_3711 + 1L);
														BgL_iz00_3711 = BgL_iz00_7335;
														goto BgL_for1044z00_3710;
													}
												}
											else
												{	/* Unsafe/intext.scm 310 */
													((bool_t) 0);
												}
										}
										return BgL_resz00_1719;
									}
								}
							}
							break;
						case ((unsigned char) 't'):

							{	/* Unsafe/intext.scm 363 */
								obj_t BgL_tagz00_1845;

								BgL_tagz00_1845 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								{	/* Unsafe/intext.scm 363 */
									long BgL_sza7za7_1846;

									{	/* Unsafe/intext.scm 234 */
										long BgL_siza7eza7_3850;

										BgL_siza7eza7_3850 =
											BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
											(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
											BgL_za2strlenza2z00_6689, BgL_sz00_6694);
										BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
											BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
											BgL_siza7eza7_3850, BGl_string2624z00zz__intextz00);
										BgL_sza7za7_1846 = BgL_siza7eza7_3850;
									}
									{	/* Unsafe/intext.scm 364 */
										obj_t BgL_resz00_1847;

										BgL_resz00_1847 = create_vector(BgL_sza7za7_1846);
										{	/* Unsafe/intext.scm 365 */

											{	/* Unsafe/intext.scm 366 */
												int BgL_tmpz00_7341;

												BgL_tmpz00_7341 = CINT(BgL_tagz00_1845);
												VECTOR_TAG_SET(BgL_resz00_1847, BgL_tmpz00_7341);
											}
											{	/* Unsafe/intext.scm 367 */
												bool_t BgL_test2806z00_7344;

												{	/* Unsafe/intext.scm 367 */
													obj_t BgL_objz00_3853;

													BgL_objz00_3853 =
														CELL_REF(BgL_za2definingza2z00_6691);
													BgL_test2806z00_7344 = INTEGERP(BgL_objz00_3853);
												}
												if (BgL_test2806z00_7344)
													{	/* Unsafe/intext.scm 367 */
														{	/* Unsafe/intext.scm 368 */
															obj_t BgL_vectorz00_3854;
															long BgL_kz00_3855;

															BgL_vectorz00_3854 =
																((obj_t)
																((obj_t)
																	CELL_REF(BgL_za2definitionsza2z00_6693)));
															BgL_kz00_3855 =
																(long)
																CINT(CELL_REF(BgL_za2definingza2z00_6691));
															VECTOR_SET(BgL_vectorz00_3854, BgL_kz00_3855,
																BgL_resz00_1847);
														}
														{	/* Unsafe/intext.scm 369 */
															obj_t BgL_auxz00_6711;

															BgL_auxz00_6711 = BFALSE;
															CELL_SET(BgL_za2definingza2z00_6691,
																BgL_auxz00_6711);
													}}
												else
													{	/* Unsafe/intext.scm 367 */
														BFALSE;
													}
											}
											{
												long BgL_iz00_3862;

												BgL_iz00_3862 = 0L;
											BgL_for1055z00_3861:
												if ((BgL_iz00_3862 < BgL_sza7za7_1846))
													{	/* Unsafe/intext.scm 370 */
														VECTOR_SET(BgL_resz00_1847, BgL_iz00_3862,
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
																BgL_za2definitionsza2z00_6693,
																BgL_unserializa7ezd2argz75_6692,
																BgL_za2definingza2z00_6691,
																BgL_za2pointerza2z00_6690,
																BgL_za2strlenza2z00_6689));
														{
															long BgL_iz00_7353;

															BgL_iz00_7353 = (BgL_iz00_3862 + 1L);
															BgL_iz00_3862 = BgL_iz00_7353;
															goto BgL_for1055z00_3861;
														}
													}
												else
													{	/* Unsafe/intext.scm 370 */
														((bool_t) 0);
													}
											}
											return BgL_resz00_1847;
										}
									}
								}
							}
							break;
						case ((unsigned char) 'V'):

							{	/* Unsafe/intext.scm 375 */
								obj_t BgL_idz00_1857;

								BgL_idz00_1857 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								{	/* Unsafe/intext.scm 375 */
									obj_t BgL_vz00_1858;

									BgL_vz00_1858 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									{	/* Unsafe/intext.scm 376 */
										obj_t BgL_tvz00_1859;

										BgL_tvz00_1859 =
											BGl_vectorzd2ze3tvectorz31zz__tvectorz00(BgL_idz00_1857,
											BgL_vz00_1858);
										{	/* Unsafe/intext.scm 377 */

											{	/* Unsafe/intext.scm 378 */
												bool_t BgL_test2808z00_7358;

												{	/* Unsafe/intext.scm 378 */
													obj_t BgL_objz00_3871;

													BgL_objz00_3871 =
														CELL_REF(BgL_za2definingza2z00_6691);
													BgL_test2808z00_7358 = INTEGERP(BgL_objz00_3871);
												}
												if (BgL_test2808z00_7358)
													{	/* Unsafe/intext.scm 378 */
														{	/* Unsafe/intext.scm 379 */
															obj_t BgL_vectorz00_3872;
															long BgL_kz00_3873;

															BgL_vectorz00_3872 =
																((obj_t)
																((obj_t)
																	CELL_REF(BgL_za2definitionsza2z00_6693)));
															BgL_kz00_3873 =
																(long)
																CINT(CELL_REF(BgL_za2definingza2z00_6691));
															VECTOR_SET(BgL_vectorz00_3872, BgL_kz00_3873,
																BgL_tvz00_1859);
														}
														{	/* Unsafe/intext.scm 380 */
															obj_t BgL_auxz00_6710;

															BgL_auxz00_6710 = BFALSE;
															CELL_SET(BgL_za2definingza2z00_6691,
																BgL_auxz00_6710);
													}}
												else
													{	/* Unsafe/intext.scm 378 */
														BFALSE;
													}
											}
											return BgL_tvz00_1859;
										}
									}
								}
							}
							break;
						case ((unsigned char) 'h'):

							{	/* Unsafe/intext.scm 315 */
								long BgL_lenz00_1729;
								long BgL_bsiza7eza7_1730;

								{	/* Unsafe/intext.scm 234 */
									long BgL_siza7eza7_3720;

									BgL_siza7eza7_3720 =
										BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
										(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
										BgL_siza7eza7_3720, BGl_string2625z00zz__intextz00);
									BgL_lenz00_1729 = BgL_siza7eza7_3720;
								}
								{	/* Unsafe/intext.scm 234 */
									long BgL_siza7eza7_3721;

									BgL_siza7eza7_3721 =
										BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
										(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
										BgL_siza7eza7_3721, BGl_string2626z00zz__intextz00);
									BgL_bsiza7eza7_1730 = BgL_siza7eza7_3721;
								}
								{	/* Unsafe/intext.scm 317 */
									obj_t BgL_casezd2valuezd2_1731;

									{	/* Unsafe/intext.scm 259 */
										obj_t BgL_arg1396z00_3722;

										BgL_arg1396z00_3722 =
											BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
											BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
											BgL_unserializa7ezd2argz75_6692,
											BgL_za2definingza2z00_6691, BgL_za2pointerza2z00_6690,
											BgL_za2strlenza2z00_6689);
										BgL_casezd2valuezd2_1731 =
											bstring_to_symbol(((obj_t) BgL_arg1396z00_3722));
									}
									if (
										(BgL_casezd2valuezd2_1731 ==
											BGl_symbol2627z00zz__intextz00))
										{	/* Unsafe/intext.scm 319 */
											obj_t BgL_resz00_1733;

											{	/* Llib/srfi4.scm 446 */

												BgL_resz00_1733 =
													BGl_makezd2s8vectorzd2zz__srfi4z00(BgL_lenz00_1729,
													(int8_t) (0));
											}
											{
												long BgL_iz00_3730;

												BgL_iz00_3730 = 0L;
											BgL_for1045z00_3729:
												if ((BgL_iz00_3730 < BgL_lenz00_1729))
													{	/* Unsafe/intext.scm 320 */
														{	/* Unsafe/intext.scm 321 */
															int8_t BgL_arg1427z00_3734;

															{	/* Unsafe/intext.scm 321 */
																long BgL_tmpz00_7375;

																BgL_tmpz00_7375 =
																	BGl_readzd2wordze70z35zz__intextz00
																	(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
																	BgL_za2strlenza2z00_6689, BgL_sz00_6694,
																	(int) (BgL_bsiza7eza7_1730));
																BgL_arg1427z00_3734 =
																	(int8_t) (BgL_tmpz00_7375);
															}
															BGL_S8VSET(BgL_resz00_1733, BgL_iz00_3730,
																BgL_arg1427z00_3734);
															BUNSPEC;
														}
														{
															long BgL_iz00_7380;

															BgL_iz00_7380 = (BgL_iz00_3730 + 1L);
															BgL_iz00_3730 = BgL_iz00_7380;
															goto BgL_for1045z00_3729;
														}
													}
												else
													{	/* Unsafe/intext.scm 320 */
														((bool_t) 0);
													}
											}
											return BgL_resz00_1733;
										}
									else
										{	/* Unsafe/intext.scm 317 */
											if (
												(BgL_casezd2valuezd2_1731 ==
													BGl_symbol2629z00zz__intextz00))
												{	/* Unsafe/intext.scm 324 */
													obj_t BgL_resz00_1745;

													{	/* Llib/srfi4.scm 447 */

														BgL_resz00_1745 =
															BGl_makezd2u8vectorzd2zz__srfi4z00
															(BgL_lenz00_1729, (uint8_t) (0));
													}
													{
														long BgL_iz00_3745;

														BgL_iz00_3745 = 0L;
													BgL_for1046z00_3744:
														if ((BgL_iz00_3745 < BgL_lenz00_1729))
															{	/* Unsafe/intext.scm 325 */
																{	/* Unsafe/intext.scm 326 */
																	uint8_t BgL_arg1434z00_3749;

																	{	/* Unsafe/intext.scm 326 */
																		long BgL_tmpz00_7387;

																		BgL_tmpz00_7387 =
																			BGl_readzd2wordze70z35zz__intextz00
																			(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
																			BgL_za2strlenza2z00_6689, BgL_sz00_6694,
																			(int) (BgL_bsiza7eza7_1730));
																		BgL_arg1434z00_3749 =
																			(uint8_t) (BgL_tmpz00_7387);
																	}
																	BGL_U8VSET(BgL_resz00_1745, BgL_iz00_3745,
																		BgL_arg1434z00_3749);
																	BUNSPEC;
																}
																{
																	long BgL_iz00_7392;

																	BgL_iz00_7392 = (BgL_iz00_3745 + 1L);
																	BgL_iz00_3745 = BgL_iz00_7392;
																	goto BgL_for1046z00_3744;
																}
															}
														else
															{	/* Unsafe/intext.scm 325 */
																((bool_t) 0);
															}
													}
													return BgL_resz00_1745;
												}
											else
												{	/* Unsafe/intext.scm 317 */
													if (
														(BgL_casezd2valuezd2_1731 ==
															BGl_symbol2631z00zz__intextz00))
														{	/* Unsafe/intext.scm 329 */
															obj_t BgL_resz00_1757;

															{	/* Llib/srfi4.scm 448 */

																BgL_resz00_1757 =
																	BGl_makezd2s16vectorzd2zz__srfi4z00
																	(BgL_lenz00_1729, (int16_t) (0));
															}
															{
																long BgL_iz00_3759;

																BgL_iz00_3759 = 0L;
															BgL_for1047z00_3758:
																if ((BgL_iz00_3759 < BgL_lenz00_1729))
																	{	/* Unsafe/intext.scm 330 */
																		{	/* Unsafe/intext.scm 330 */
																			long BgL_arg1441z00_3763;

																			BgL_arg1441z00_3763 =
																				BGl_readzd2wordze70z35zz__intextz00
																				(BgL_za2pointerza2z00_6690,
																				BgL_sz00_6694, BgL_za2strlenza2z00_6689,
																				BgL_sz00_6694,
																				(int) (BgL_bsiza7eza7_1730));
																			{	/* Unsafe/intext.scm 330 */
																				int16_t BgL_tmpz00_7401;

																				BgL_tmpz00_7401 =
																					(int16_t) (BgL_arg1441z00_3763);
																				BGL_S16VSET(BgL_resz00_1757,
																					BgL_iz00_3759, BgL_tmpz00_7401);
																			} BUNSPEC;
																		}
																		{
																			long BgL_iz00_7404;

																			BgL_iz00_7404 = (BgL_iz00_3759 + 1L);
																			BgL_iz00_3759 = BgL_iz00_7404;
																			goto BgL_for1047z00_3758;
																		}
																	}
																else
																	{	/* Unsafe/intext.scm 330 */
																		((bool_t) 0);
																	}
															}
															return BgL_resz00_1757;
														}
													else
														{	/* Unsafe/intext.scm 317 */
															if (
																(BgL_casezd2valuezd2_1731 ==
																	BGl_symbol2633z00zz__intextz00))
																{	/* Unsafe/intext.scm 333 */
																	obj_t BgL_resz00_1768;

																	{	/* Llib/srfi4.scm 449 */

																		BgL_resz00_1768 =
																			BGl_makezd2u16vectorzd2zz__srfi4z00
																			(BgL_lenz00_1729, (uint16_t) (0));
																	}
																	{
																		long BgL_iz00_3771;

																		BgL_iz00_3771 = 0L;
																	BgL_for1048z00_3770:
																		if ((BgL_iz00_3771 < BgL_lenz00_1729))
																			{	/* Unsafe/intext.scm 334 */
																				{	/* Unsafe/intext.scm 334 */
																					long BgL_arg1447z00_3775;

																					BgL_arg1447z00_3775 =
																						BGl_readzd2wordze70z35zz__intextz00
																						(BgL_za2pointerza2z00_6690,
																						BgL_sz00_6694,
																						BgL_za2strlenza2z00_6689,
																						BgL_sz00_6694,
																						(int) (BgL_bsiza7eza7_1730));
																					{	/* Unsafe/intext.scm 334 */
																						uint16_t BgL_tmpz00_7413;

																						BgL_tmpz00_7413 =
																							(uint16_t) (BgL_arg1447z00_3775);
																						BGL_U16VSET(BgL_resz00_1768,
																							BgL_iz00_3771, BgL_tmpz00_7413);
																					} BUNSPEC;
																				}
																				{
																					long BgL_iz00_7416;

																					BgL_iz00_7416 = (BgL_iz00_3771 + 1L);
																					BgL_iz00_3771 = BgL_iz00_7416;
																					goto BgL_for1048z00_3770;
																				}
																			}
																		else
																			{	/* Unsafe/intext.scm 334 */
																				((bool_t) 0);
																			}
																	}
																	return BgL_resz00_1768;
																}
															else
																{	/* Unsafe/intext.scm 317 */
																	if (
																		(BgL_casezd2valuezd2_1731 ==
																			BGl_symbol2635z00zz__intextz00))
																		{	/* Unsafe/intext.scm 337 */
																			obj_t BgL_resz00_1779;

																			{	/* Llib/srfi4.scm 450 */

																				BgL_resz00_1779 =
																					BGl_makezd2s32vectorzd2zz__srfi4z00
																					(BgL_lenz00_1729, (int32_t) (0));
																			}
																			{
																				long BgL_iz00_3783;

																				BgL_iz00_3783 = 0L;
																			BgL_for1049z00_3782:
																				if ((BgL_iz00_3783 < BgL_lenz00_1729))
																					{	/* Unsafe/intext.scm 338 */
																						{	/* Unsafe/intext.scm 338 */
																							long BgL_arg1453z00_3787;

																							BgL_arg1453z00_3787 =
																								BGl_readzd2wordze70z35zz__intextz00
																								(BgL_za2pointerza2z00_6690,
																								BgL_sz00_6694,
																								BgL_za2strlenza2z00_6689,
																								BgL_sz00_6694,
																								(int) (BgL_bsiza7eza7_1730));
																							{	/* Unsafe/intext.scm 338 */
																								int32_t BgL_tmpz00_7425;

																								BgL_tmpz00_7425 =
																									(int32_t)
																									(BgL_arg1453z00_3787);
																								BGL_S32VSET(BgL_resz00_1779,
																									BgL_iz00_3783,
																									BgL_tmpz00_7425);
																							} BUNSPEC;
																						}
																						{
																							long BgL_iz00_7428;

																							BgL_iz00_7428 =
																								(BgL_iz00_3783 + 1L);
																							BgL_iz00_3783 = BgL_iz00_7428;
																							goto BgL_for1049z00_3782;
																						}
																					}
																				else
																					{	/* Unsafe/intext.scm 338 */
																						((bool_t) 0);
																					}
																			}
																			return BgL_resz00_1779;
																		}
																	else
																		{	/* Unsafe/intext.scm 317 */
																			if (
																				(BgL_casezd2valuezd2_1731 ==
																					BGl_symbol2637z00zz__intextz00))
																				{	/* Unsafe/intext.scm 341 */
																					obj_t BgL_resz00_1790;

																					{	/* Llib/srfi4.scm 451 */

																						BgL_resz00_1790 =
																							BGl_makezd2u32vectorzd2zz__srfi4z00
																							(BgL_lenz00_1729, (uint32_t) (0));
																					}
																					{
																						long BgL_iz00_3795;

																						BgL_iz00_3795 = 0L;
																					BgL_for1050z00_3794:
																						if (
																							(BgL_iz00_3795 < BgL_lenz00_1729))
																							{	/* Unsafe/intext.scm 342 */
																								{	/* Unsafe/intext.scm 342 */
																									long BgL_arg1459z00_3799;

																									BgL_arg1459z00_3799 =
																										BGl_readzd2wordze70z35zz__intextz00
																										(BgL_za2pointerza2z00_6690,
																										BgL_sz00_6694,
																										BgL_za2strlenza2z00_6689,
																										BgL_sz00_6694,
																										(int)
																										(BgL_bsiza7eza7_1730));
																									{	/* Unsafe/intext.scm 342 */
																										uint32_t BgL_tmpz00_7437;

																										BgL_tmpz00_7437 =
																											(uint32_t)
																											(BgL_arg1459z00_3799);
																										BGL_U32VSET(BgL_resz00_1790,
																											BgL_iz00_3795,
																											BgL_tmpz00_7437);
																									} BUNSPEC;
																								}
																								{
																									long BgL_iz00_7440;

																									BgL_iz00_7440 =
																										(BgL_iz00_3795 + 1L);
																									BgL_iz00_3795 = BgL_iz00_7440;
																									goto BgL_for1050z00_3794;
																								}
																							}
																						else
																							{	/* Unsafe/intext.scm 342 */
																								((bool_t) 0);
																							}
																					}
																					return BgL_resz00_1790;
																				}
																			else
																				{	/* Unsafe/intext.scm 317 */
																					if (
																						(BgL_casezd2valuezd2_1731 ==
																							BGl_symbol2639z00zz__intextz00))
																						{	/* Unsafe/intext.scm 345 */
																							obj_t BgL_resz00_1801;

																							{	/* Llib/srfi4.scm 452 */

																								BgL_resz00_1801 =
																									BGl_makezd2s64vectorzd2zz__srfi4z00
																									(BgL_lenz00_1729,
																									(int64_t) (0));
																							}
																							{
																								long BgL_iz00_3807;

																								BgL_iz00_3807 = 0L;
																							BgL_for1051z00_3806:
																								if (
																									(BgL_iz00_3807 <
																										BgL_lenz00_1729))
																									{	/* Unsafe/intext.scm 346 */
																										{	/* Unsafe/intext.scm 346 */
																											BGL_LONGLONG_T
																												BgL_arg1465z00_3811;
																											BgL_arg1465z00_3811 =
																												BGl_readzd2longzd2wordze70ze7zz__intextz00
																												(BgL_za2pointerza2z00_6690,
																												BgL_sz00_6694,
																												BgL_za2strlenza2z00_6689,
																												BgL_sz00_6694,
																												(int)
																												(BgL_bsiza7eza7_1730));
																											{	/* Unsafe/intext.scm 346 */
																												int64_t BgL_tmpz00_7449;

																												BgL_tmpz00_7449 =
																													(int64_t)
																													(BgL_arg1465z00_3811);
																												BGL_S64VSET
																													(BgL_resz00_1801,
																													BgL_iz00_3807,
																													BgL_tmpz00_7449);
																											} BUNSPEC;
																										}
																										{
																											long BgL_iz00_7452;

																											BgL_iz00_7452 =
																												(BgL_iz00_3807 + 1L);
																											BgL_iz00_3807 =
																												BgL_iz00_7452;
																											goto BgL_for1051z00_3806;
																										}
																									}
																								else
																									{	/* Unsafe/intext.scm 346 */
																										((bool_t) 0);
																									}
																							}
																							return BgL_resz00_1801;
																						}
																					else
																						{	/* Unsafe/intext.scm 317 */
																							if (
																								(BgL_casezd2valuezd2_1731 ==
																									BGl_symbol2641z00zz__intextz00))
																								{	/* Unsafe/intext.scm 349 */
																									obj_t BgL_resz00_1812;

																									{	/* Llib/srfi4.scm 453 */

																										BgL_resz00_1812 =
																											BGl_makezd2u64vectorzd2zz__srfi4z00
																											(BgL_lenz00_1729,
																											(uint64_t) (0));
																									}
																									{
																										long BgL_iz00_3819;

																										BgL_iz00_3819 = 0L;
																									BgL_for1052z00_3818:
																										if (
																											(BgL_iz00_3819 <
																												BgL_lenz00_1729))
																											{	/* Unsafe/intext.scm 350 */
																												{	/* Unsafe/intext.scm 350 */
																													BGL_LONGLONG_T
																														BgL_arg1472z00_3823;
																													BgL_arg1472z00_3823 =
																														BGl_readzd2longzd2wordze70ze7zz__intextz00
																														(BgL_za2pointerza2z00_6690,
																														BgL_sz00_6694,
																														BgL_za2strlenza2z00_6689,
																														BgL_sz00_6694,
																														(int)
																														(BgL_bsiza7eza7_1730));
																													{	/* Unsafe/intext.scm 350 */
																														uint64_t
																															BgL_tmpz00_7461;
																														BgL_tmpz00_7461 =
																															(uint64_t)
																															(BgL_arg1472z00_3823);
																														BGL_U64VSET
																															(BgL_resz00_1812,
																															BgL_iz00_3819,
																															BgL_tmpz00_7461);
																													} BUNSPEC;
																												}
																												{
																													long BgL_iz00_7464;

																													BgL_iz00_7464 =
																														(BgL_iz00_3819 +
																														1L);
																													BgL_iz00_3819 =
																														BgL_iz00_7464;
																													goto
																														BgL_for1052z00_3818;
																												}
																											}
																										else
																											{	/* Unsafe/intext.scm 350 */
																												((bool_t) 0);
																											}
																									}
																									return BgL_resz00_1812;
																								}
																							else
																								{	/* Unsafe/intext.scm 317 */
																									if (
																										(BgL_casezd2valuezd2_1731 ==
																											BGl_symbol2643z00zz__intextz00))
																										{	/* Unsafe/intext.scm 353 */
																											obj_t BgL_resz00_1823;

																											{	/* Llib/srfi4.scm 454 */

																												BgL_resz00_1823 =
																													BGl_makezd2f32vectorzd2zz__srfi4z00
																													(BgL_lenz00_1729,
																													(float) (((double)
																															0.0)));
																											}
																											{
																												long BgL_iz00_3831;

																												BgL_iz00_3831 = 0L;
																											BgL_for1053z00_3830:
																												if (
																													(BgL_iz00_3831 <
																														BgL_lenz00_1729))
																													{	/* Unsafe/intext.scm 354 */
																														{	/* Unsafe/intext.scm 354 */
																															double
																																BgL_arg1478z00_3835;
																															BgL_arg1478z00_3835
																																=
																																BGl_readzd2floatze70z35zz__intextz00
																																(BgL_za2pointerza2z00_6690,
																																BgL_sz00_6694,
																																BgL_za2strlenza2z00_6689,
																																BgL_sz00_6694);
																															{	/* Unsafe/intext.scm 354 */
																																float
																																	BgL_tmpz00_7473;
																																BgL_tmpz00_7473
																																	=
																																	(float)
																																	(BgL_arg1478z00_3835);
																																BGL_F32VSET
																																	(BgL_resz00_1823,
																																	BgL_iz00_3831,
																																	BgL_tmpz00_7473);
																															} BUNSPEC;
																														}
																														{
																															long
																																BgL_iz00_7476;
																															BgL_iz00_7476 =
																																(BgL_iz00_3831 +
																																1L);
																															BgL_iz00_3831 =
																																BgL_iz00_7476;
																															goto
																																BgL_for1053z00_3830;
																														}
																													}
																												else
																													{	/* Unsafe/intext.scm 354 */
																														((bool_t) 0);
																													}
																											}
																											return BgL_resz00_1823;
																										}
																									else
																										{	/* Unsafe/intext.scm 317 */
																											if (
																												(BgL_casezd2valuezd2_1731
																													==
																													BGl_symbol2645z00zz__intextz00))
																												{	/* Unsafe/intext.scm 357 */
																													obj_t BgL_resz00_1834;

																													{	/* Llib/srfi4.scm 455 */

																														BgL_resz00_1834 =
																															BGl_makezd2f64vectorzd2zz__srfi4z00
																															(BgL_lenz00_1729,
																															((double) 0.0));
																													}
																													{
																														long BgL_iz00_3843;

																														BgL_iz00_3843 = 0L;
																													BgL_for1054z00_3842:
																														if (
																															(BgL_iz00_3843 <
																																BgL_lenz00_1729))
																															{	/* Unsafe/intext.scm 358 */
																																{	/* Unsafe/intext.scm 358 */
																																	double
																																		BgL_arg1484z00_3847;
																																	BgL_arg1484z00_3847
																																		=
																																		BGl_readzd2floatze70z35zz__intextz00
																																		(BgL_za2pointerza2z00_6690,
																																		BgL_sz00_6694,
																																		BgL_za2strlenza2z00_6689,
																																		BgL_sz00_6694);
																																	BGL_F64VSET
																																		(BgL_resz00_1834,
																																		BgL_iz00_3843,
																																		BgL_arg1484z00_3847);
																																	BUNSPEC;
																																}
																																{
																																	long
																																		BgL_iz00_7485;
																																	BgL_iz00_7485
																																		=
																																		(BgL_iz00_3843
																																		+ 1L);
																																	BgL_iz00_3843
																																		=
																																		BgL_iz00_7485;
																																	goto
																																		BgL_for1054z00_3842;
																																}
																															}
																														else
																															{	/* Unsafe/intext.scm 358 */
																																((bool_t) 0);
																															}
																													}
																													return
																														BgL_resz00_1834;
																												}
																											else
																												{	/* Unsafe/intext.scm 317 */
																													return BUNSPEC;
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
							}
							break;
						case ((unsigned char) '('):

							{	/* Unsafe/intext.scm 385 */
								long BgL_sza7za7_1862;

								{	/* Unsafe/intext.scm 234 */
									long BgL_siza7eza7_3874;

									BgL_siza7eza7_3874 =
										BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
										(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
										BgL_siza7eza7_3874, BGl_string2623z00zz__intextz00);
									BgL_sza7za7_1862 = BgL_siza7eza7_3874;
								}
								{	/* Unsafe/intext.scm 385 */
									obj_t BgL_resz00_1863;

									BgL_resz00_1863 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{	/* Unsafe/intext.scm 386 */

										{	/* Unsafe/intext.scm 387 */
											bool_t BgL_test2829z00_7490;

											{	/* Unsafe/intext.scm 387 */
												obj_t BgL_objz00_3875;

												BgL_objz00_3875 = CELL_REF(BgL_za2definingza2z00_6691);
												BgL_test2829z00_7490 = INTEGERP(BgL_objz00_3875);
											}
											if (BgL_test2829z00_7490)
												{	/* Unsafe/intext.scm 387 */
													{	/* Unsafe/intext.scm 388 */
														obj_t BgL_vectorz00_3876;
														long BgL_kz00_3877;

														BgL_vectorz00_3876 =
															((obj_t)
															((obj_t)
																CELL_REF(BgL_za2definitionsza2z00_6693)));
														BgL_kz00_3877 =
															(long) CINT(CELL_REF(BgL_za2definingza2z00_6691));
														VECTOR_SET(BgL_vectorz00_3876, BgL_kz00_3877,
															BgL_resz00_1863);
													}
													{	/* Unsafe/intext.scm 389 */
														obj_t BgL_auxz00_6709;

														BgL_auxz00_6709 = BFALSE;
														CELL_SET(BgL_za2definingza2z00_6691,
															BgL_auxz00_6709);
												}}
											else
												{	/* Unsafe/intext.scm 387 */
													BFALSE;
												}
										}
										{
											long BgL_iz00_1866;
											obj_t BgL_hdz00_1867;

											BgL_iz00_1866 = 0L;
											BgL_hdz00_1867 = BgL_resz00_1863;
										BgL_zc3z04anonymousza31499ze3z87_1868:
											if ((BgL_iz00_1866 == (BgL_sza7za7_1862 - 2L)))
												{	/* Unsafe/intext.scm 392 */
													{	/* Unsafe/intext.scm 394 */
														obj_t BgL_arg1502z00_1871;

														BgL_arg1502z00_1871 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 394 */
															obj_t BgL_tmpz00_7499;

															BgL_tmpz00_7499 = ((obj_t) BgL_hdz00_1867);
															SET_CAR(BgL_tmpz00_7499, BgL_arg1502z00_1871);
														}
													}
													{	/* Unsafe/intext.scm 395 */
														obj_t BgL_arg1503z00_1872;

														BgL_arg1503z00_1872 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 395 */
															obj_t BgL_tmpz00_7503;

															BgL_tmpz00_7503 = ((obj_t) BgL_hdz00_1867);
															SET_CDR(BgL_tmpz00_7503, BgL_arg1503z00_1872);
														}
													}
												}
											else
												{	/* Unsafe/intext.scm 392 */
													{	/* Unsafe/intext.scm 397 */
														obj_t BgL_arg1504z00_1873;

														BgL_arg1504z00_1873 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 397 */
															obj_t BgL_tmpz00_7507;

															BgL_tmpz00_7507 = ((obj_t) BgL_hdz00_1867);
															SET_CAR(BgL_tmpz00_7507, BgL_arg1504z00_1873);
														}
													}
													{	/* Unsafe/intext.scm 398 */
														obj_t BgL_arg1505z00_1874;

														BgL_arg1505z00_1874 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{	/* Unsafe/intext.scm 398 */
															obj_t BgL_tmpz00_7511;

															BgL_tmpz00_7511 = ((obj_t) BgL_hdz00_1867);
															SET_CDR(BgL_tmpz00_7511, BgL_arg1505z00_1874);
														}
													}
													{	/* Unsafe/intext.scm 399 */
														long BgL_arg1506z00_1875;
														obj_t BgL_arg1507z00_1876;

														BgL_arg1506z00_1875 = (BgL_iz00_1866 + 1L);
														BgL_arg1507z00_1876 = CDR(((obj_t) BgL_hdz00_1867));
														{
															obj_t BgL_hdz00_7518;
															long BgL_iz00_7517;

															BgL_iz00_7517 = BgL_arg1506z00_1875;
															BgL_hdz00_7518 = BgL_arg1507z00_1876;
															BgL_hdz00_1867 = BgL_hdz00_7518;
															BgL_iz00_1866 = BgL_iz00_7517;
															goto BgL_zc3z04anonymousza31499ze3z87_1868;
														}
													}
												}
										}
										return BgL_resz00_1863;
									}
								}
							}
							break;
						case ((unsigned char) '^'):

							{	/* Unsafe/intext.scm 404 */
								long BgL_sza7za7_1880;

								{	/* Unsafe/intext.scm 234 */
									long BgL_siza7eza7_3887;

									BgL_siza7eza7_3887 =
										BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
										(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
										BgL_siza7eza7_3887, BGl_string2622z00zz__intextz00);
									BgL_sza7za7_1880 = BgL_siza7eza7_3887;
								}
								{	/* Unsafe/intext.scm 404 */
									obj_t BgL_resz00_1881;

									{	/* Unsafe/intext.scm 405 */
										obj_t BgL_res2507z00_3888;

										BgL_res2507z00_3888 = MAKE_YOUNG_EPAIR(BNIL, BNIL, BUNSPEC);
										BgL_resz00_1881 = BgL_res2507z00_3888;
									}
									{	/* Unsafe/intext.scm 405 */

										{	/* Unsafe/intext.scm 406 */
											bool_t BgL_test2831z00_7522;

											{	/* Unsafe/intext.scm 406 */
												obj_t BgL_objz00_3889;

												BgL_objz00_3889 = CELL_REF(BgL_za2definingza2z00_6691);
												BgL_test2831z00_7522 = INTEGERP(BgL_objz00_3889);
											}
											if (BgL_test2831z00_7522)
												{	/* Unsafe/intext.scm 406 */
													{	/* Unsafe/intext.scm 407 */
														obj_t BgL_vectorz00_3890;
														long BgL_kz00_3891;

														BgL_vectorz00_3890 =
															((obj_t)
															((obj_t)
																CELL_REF(BgL_za2definitionsza2z00_6693)));
														BgL_kz00_3891 =
															(long) CINT(CELL_REF(BgL_za2definingza2z00_6691));
														VECTOR_SET(BgL_vectorz00_3890, BgL_kz00_3891,
															BgL_resz00_1881);
													}
													{	/* Unsafe/intext.scm 408 */
														obj_t BgL_auxz00_6708;

														BgL_auxz00_6708 = BFALSE;
														CELL_SET(BgL_za2definingza2z00_6691,
															BgL_auxz00_6708);
												}}
											else
												{	/* Unsafe/intext.scm 406 */
													BFALSE;
												}
										}
										{
											long BgL_iz00_1884;
											obj_t BgL_hdz00_1885;

											BgL_iz00_1884 = 0L;
											BgL_hdz00_1885 = BgL_resz00_1881;
										BgL_zc3z04anonymousza31511ze3z87_1886:
											if ((BgL_iz00_1884 == (BgL_sza7za7_1880 - 2L)))
												{	/* Unsafe/intext.scm 411 */
													{	/* Unsafe/intext.scm 413 */
														obj_t BgL_arg1516z00_1889;

														BgL_arg1516z00_1889 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 413 */
															obj_t BgL_tmpz00_7531;

															BgL_tmpz00_7531 = ((obj_t) BgL_hdz00_1885);
															SET_CAR(BgL_tmpz00_7531, BgL_arg1516z00_1889);
														}
													}
													{	/* Unsafe/intext.scm 414 */
														obj_t BgL_arg1517z00_1890;

														BgL_arg1517z00_1890 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 414 */
															obj_t BgL_tmpz00_7535;

															BgL_tmpz00_7535 = ((obj_t) BgL_hdz00_1885);
															SET_CER(BgL_tmpz00_7535, BgL_arg1517z00_1890);
														}
													}
													{	/* Unsafe/intext.scm 415 */
														obj_t BgL_arg1521z00_1891;

														BgL_arg1521z00_1891 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 415 */
															obj_t BgL_tmpz00_7539;

															BgL_tmpz00_7539 = ((obj_t) BgL_hdz00_1885);
															SET_CDR(BgL_tmpz00_7539, BgL_arg1521z00_1891);
														}
													}
												}
											else
												{	/* Unsafe/intext.scm 411 */
													{	/* Unsafe/intext.scm 417 */
														obj_t BgL_arg1522z00_1892;

														BgL_arg1522z00_1892 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 417 */
															obj_t BgL_tmpz00_7543;

															BgL_tmpz00_7543 = ((obj_t) BgL_hdz00_1885);
															SET_CAR(BgL_tmpz00_7543, BgL_arg1522z00_1892);
														}
													}
													{	/* Unsafe/intext.scm 418 */
														obj_t BgL_arg1523z00_1893;

														BgL_arg1523z00_1893 =
															BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{	/* Unsafe/intext.scm 418 */
															obj_t BgL_tmpz00_7547;

															BgL_tmpz00_7547 = ((obj_t) BgL_hdz00_1885);
															SET_CER(BgL_tmpz00_7547, BgL_arg1523z00_1893);
														}
													}
													{	/* Unsafe/intext.scm 419 */
														obj_t BgL_arg1524z00_1894;

														{	/* Unsafe/intext.scm 419 */
															obj_t BgL_res2508z00_3900;

															BgL_res2508z00_3900 =
																MAKE_YOUNG_EPAIR(BNIL, BNIL, BUNSPEC);
															BgL_arg1524z00_1894 = BgL_res2508z00_3900;
														}
														{	/* Unsafe/intext.scm 419 */
															obj_t BgL_tmpz00_7551;

															BgL_tmpz00_7551 = ((obj_t) BgL_hdz00_1885);
															SET_CDR(BgL_tmpz00_7551, BgL_arg1524z00_1894);
														}
													}
													{	/* Unsafe/intext.scm 420 */
														long BgL_arg1525z00_1895;
														obj_t BgL_arg1526z00_1896;

														BgL_arg1525z00_1895 = (BgL_iz00_1884 + 1L);
														BgL_arg1526z00_1896 = CDR(((obj_t) BgL_hdz00_1885));
														{
															obj_t BgL_hdz00_7558;
															long BgL_iz00_7557;

															BgL_iz00_7557 = BgL_arg1525z00_1895;
															BgL_hdz00_7558 = BgL_arg1526z00_1896;
															BgL_hdz00_1885 = BgL_hdz00_7558;
															BgL_iz00_1884 = BgL_iz00_7557;
															goto BgL_zc3z04anonymousza31511ze3z87_1886;
														}
													}
												}
										}
										return BgL_resz00_1881;
									}
								}
							}
							break;
						case ((unsigned char) '{'):

							{	/* Unsafe/intext.scm 458 */
								obj_t BgL_definingz00_1916;

								{	/* Unsafe/intext.scm 458 */
									obj_t BgL_oldz00_1928;

									BgL_oldz00_1928 = CELL_REF(BgL_za2definingza2z00_6691);
									{	/* Unsafe/intext.scm 459 */
										obj_t BgL_auxz00_6705;

										BgL_auxz00_6705 = BFALSE;
										CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6705);
									}
									BgL_definingz00_1916 = BgL_oldz00_1928;
								}
								{	/* Unsafe/intext.scm 458 */
									obj_t BgL_keyz00_1917;

									BgL_keyz00_1917 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									{	/* Unsafe/intext.scm 461 */
										long BgL_sza7za7_1918;

										{	/* Unsafe/intext.scm 234 */
											long BgL_siza7eza7_3915;

											BgL_siza7eza7_3915 =
												BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
												(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
												BgL_za2strlenza2z00_6689, BgL_sz00_6694);
											BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
												BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
												BgL_siza7eza7_3915, BGl_string2621z00zz__intextz00);
											BgL_sza7za7_1918 = BgL_siza7eza7_3915;
										}
										{	/* Unsafe/intext.scm 462 */
											obj_t BgL_resz00_1919;

											{	/* Unsafe/intext.scm 463 */
												int BgL_lenz00_3917;

												BgL_lenz00_3917 = (int) (BgL_sza7za7_1918);
												BgL_resz00_1919 =
													make_struct(
													((obj_t) BgL_keyz00_1917), BgL_lenz00_3917, BUNSPEC);
											}
											{	/* Unsafe/intext.scm 463 */

												if (INTEGERP(BgL_definingz00_1916))
													{	/* Unsafe/intext.scm 465 */
														obj_t BgL_vectorz00_3918;
														long BgL_kz00_3919;

														BgL_vectorz00_3918 =
															((obj_t)
															((obj_t)
																CELL_REF(BgL_za2definitionsza2z00_6693)));
														BgL_kz00_3919 = (long) CINT(BgL_definingz00_1916);
														VECTOR_SET(BgL_vectorz00_3918, BgL_kz00_3919,
															BgL_resz00_1919);
													}
												else
													{	/* Unsafe/intext.scm 464 */
														BFALSE;
													}
												{
													long BgL_iz00_3926;

													BgL_iz00_3926 = 0L;
												BgL_for1056z00_3925:
													if ((BgL_iz00_3926 < BgL_sza7za7_1918))
														{	/* Unsafe/intext.scm 466 */
															{	/* Unsafe/intext.scm 466 */
																obj_t BgL_auxz00_7574;
																int BgL_tmpz00_7572;

																BgL_auxz00_7574 =
																	BGl_readzd2itemze70z35zz__intextz00
																	(BgL_extensionz00_6695, BgL_sz00_6694,
																	BgL_za2definitionsza2z00_6693,
																	BgL_unserializa7ezd2argz75_6692,
																	BgL_za2definingza2z00_6691,
																	BgL_za2pointerza2z00_6690,
																	BgL_za2strlenza2z00_6689);
																BgL_tmpz00_7572 = (int) (BgL_iz00_3926);
																STRUCT_SET(BgL_resz00_1919, BgL_tmpz00_7572,
																	BgL_auxz00_7574);
															}
															{
																long BgL_iz00_7577;

																BgL_iz00_7577 = (BgL_iz00_3926 + 1L);
																BgL_iz00_3926 = BgL_iz00_7577;
																goto BgL_for1056z00_3925;
															}
														}
													else
														{	/* Unsafe/intext.scm 466 */
															((bool_t) 0);
														}
												}
												return BgL_resz00_1919;
											}
										}
									}
								}
							}
							break;
						case ((unsigned char) '|'):

							BgL_sz00_1929 = BgL_sz00_6694;
							{	/* Unsafe/intext.scm 471 */
								obj_t BgL_definingz00_1931;

								{	/* Unsafe/intext.scm 471 */
									obj_t BgL_oldz00_1959;

									BgL_oldz00_1959 = CELL_REF(BgL_za2definingza2z00_6691);
									{	/* Unsafe/intext.scm 472 */
										obj_t BgL_auxz00_6704;

										BgL_auxz00_6704 = BFALSE;
										CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6704);
									}
									BgL_definingz00_1931 = BgL_oldz00_1959;
								}
								{	/* Unsafe/intext.scm 471 */
									obj_t BgL_cnamez00_1932;

									BgL_cnamez00_1932 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									{	/* Unsafe/intext.scm 474 */
										long BgL_sza7za7_1933;

										{	/* Unsafe/intext.scm 234 */
											long BgL_siza7eza7_3935;

											BgL_siza7eza7_3935 =
												BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
												(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
												BgL_za2strlenza2z00_6689, BgL_sz00_1929);
											BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
												BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
												BgL_siza7eza7_3935, BGl_string2618z00zz__intextz00);
											BgL_sza7za7_1933 = BgL_siza7eza7_3935;
										}
										{	/* Unsafe/intext.scm 475 */
											BgL_objectz00_bglt BgL_objz00_1934;

											BgL_objz00_1934 =
												BGl_allocatezd2instancezd2zz__objectz00
												(BgL_cnamez00_1932);
											{	/* Unsafe/intext.scm 476 */
												obj_t BgL_klassz00_1935;

												{	/* Unsafe/intext.scm 477 */
													obj_t BgL_arg2495z00_3937;
													long BgL_arg2497z00_3938;

													BgL_arg2495z00_3937 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Unsafe/intext.scm 477 */
														long BgL_arg2500z00_3939;

														BgL_arg2500z00_3939 =
															BGL_OBJECT_CLASS_NUM(BgL_objz00_1934);
														BgL_arg2497z00_3938 =
															(BgL_arg2500z00_3939 - OBJECT_TYPE);
													}
													BgL_klassz00_1935 =
														VECTOR_REF(BgL_arg2495z00_3937,
														BgL_arg2497z00_3938);
												}
												{	/* Unsafe/intext.scm 477 */
													obj_t BgL_fieldsz00_1936;

													{	/* Unsafe/intext.scm 478 */
														obj_t BgL_tmpz00_7587;

														BgL_tmpz00_7587 = ((obj_t) BgL_klassz00_1935);
														BgL_fieldsz00_1936 =
															BGL_CLASS_ALL_FIELDS(BgL_tmpz00_7587);
													}
													{	/* Unsafe/intext.scm 478 */

														if (
															((BgL_sza7za7_1933 - 1L) ==
																VECTOR_LENGTH(BgL_fieldsz00_1936)))
															{	/* Unsafe/intext.scm 479 */
																BFALSE;
															}
														else
															{	/* Unsafe/intext.scm 479 */
																BGl_errorz00zz__errorz00
																	(BGl_string2595z00zz__intextz00,
																	BGl_string2619z00zz__intextz00,
																	BgL_cnamez00_1932);
															}
														if (INTEGERP(BgL_definingz00_1931))
															{	/* Unsafe/intext.scm 482 */
																obj_t BgL_vectorz00_3950;
																long BgL_kz00_3951;

																BgL_vectorz00_3950 =
																	((obj_t)
																	((obj_t)
																		CELL_REF(BgL_za2definitionsza2z00_6693)));
																BgL_kz00_3951 =
																	(long) CINT(BgL_definingz00_1931);
																VECTOR_SET(BgL_vectorz00_3950, BgL_kz00_3951,
																	((obj_t) BgL_objz00_1934));
															}
														else
															{	/* Unsafe/intext.scm 481 */
																BFALSE;
															}
														BGl_readzd2itemze70z35zz__intextz00
															(BgL_extensionz00_6695, BgL_sz00_6694,
															BgL_za2definitionsza2z00_6693,
															BgL_unserializa7ezd2argz75_6692,
															BgL_za2definingza2z00_6691,
															BgL_za2pointerza2z00_6690,
															BgL_za2strlenza2z00_6689);
														{
															long BgL_iz00_1944;

															BgL_iz00_1944 = 0L;
														BgL_zc3z04anonymousza31554ze3z87_1945:
															if ((BgL_iz00_1944 < (BgL_sza7za7_1933 - 1L)))
																{	/* Unsafe/intext.scm 486 */
																	{	/* Unsafe/intext.scm 488 */
																		obj_t BgL_fz00_1948;

																		BgL_fz00_1948 =
																			VECTOR_REF(BgL_fieldsz00_1936,
																			BgL_iz00_1944);
																		if (BGl_classzd2fieldzd2virtualzf3zf3zz__objectz00(BgL_fz00_1948))
																			{	/* Unsafe/intext.scm 489 */
																				BFALSE;
																			}
																		else
																			{	/* Unsafe/intext.scm 490 */
																				obj_t BgL_fun1559z00_1950;

																				BgL_fun1559z00_1950 =
																					BGl_classzd2fieldzd2mutatorz00zz__objectz00
																					(BgL_fz00_1948);
																				{	/* Unsafe/intext.scm 490 */
																					obj_t BgL_arg1558z00_1951;

																					BgL_arg1558z00_1951 =
																						BGl_readzd2itemze70z35zz__intextz00
																						(BgL_extensionz00_6695,
																						BgL_sz00_6694,
																						BgL_za2definitionsza2z00_6693,
																						BgL_unserializa7ezd2argz75_6692,
																						BgL_za2definingza2z00_6691,
																						BgL_za2pointerza2z00_6690,
																						BgL_za2strlenza2z00_6689);
																					BGL_PROCEDURE_CALL2
																						(BgL_fun1559z00_1950,
																						((obj_t) BgL_objz00_1934),
																						BgL_arg1558z00_1951);
																				}
																			}
																	}
																	{
																		long BgL_iz00_7616;

																		BgL_iz00_7616 = (BgL_iz00_1944 + 1L);
																		BgL_iz00_1944 = BgL_iz00_7616;
																		goto BgL_zc3z04anonymousza31554ze3z87_1945;
																	}
																}
															else
																{	/* Unsafe/intext.scm 486 */
																	((bool_t) 0);
																}
														}
														{	/* Unsafe/intext.scm 491 */
															long BgL_hashz00_1955;

															BgL_hashz00_1955 =
																BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
																(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
																BgL_za2strlenza2z00_6689, BgL_sz00_1929);
															if ((BgL_hashz00_1955 ==
																	BGl_classzd2hashzd2zz__objectz00
																	(BgL_klassz00_1935)))
																{	/* Unsafe/intext.scm 492 */
																	return ((obj_t) BgL_objz00_1934);
																}
															else
																{	/* Unsafe/intext.scm 492 */
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2595z00zz__intextz00,
																		BGl_string2620z00zz__intextz00,
																		BgL_cnamez00_1932);
																}
														}
													}
												}
											}
										}
									}
								}
							}
							break;
						case ((unsigned char) 'O'):

							{	/* Unsafe/intext.scm 498 */
								obj_t BgL_definingz00_1961;

								{	/* Unsafe/intext.scm 498 */
									obj_t BgL_oldz00_1969;

									BgL_oldz00_1969 = CELL_REF(BgL_za2definingza2z00_6691);
									{	/* Unsafe/intext.scm 499 */
										obj_t BgL_auxz00_6703;

										BgL_auxz00_6703 = BFALSE;
										CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6703);
									}
									BgL_definingz00_1961 = BgL_oldz00_1969;
								}
								{	/* Unsafe/intext.scm 498 */
									obj_t BgL_hashobjz00_1962;

									BgL_hashobjz00_1962 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									{	/* Unsafe/intext.scm 501 */
										obj_t BgL_hash_z00_1963;

										BgL_hash_z00_1963 =
											BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
											BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
											BgL_unserializa7ezd2argz75_6692,
											BgL_za2definingza2z00_6691, BgL_za2pointerza2z00_6690,
											BgL_za2strlenza2z00_6689);
										{	/* Unsafe/intext.scm 502 */
											obj_t BgL_hashz00_1964;

											BgL_hashz00_1964 = CAR(((obj_t) BgL_hashobjz00_1962));
											{	/* Unsafe/intext.scm 503 */
												obj_t BgL_objz00_1965;

												BgL_objz00_1965 = CDR(((obj_t) BgL_hashobjz00_1962));
												{	/* Unsafe/intext.scm 504 */
													obj_t BgL_unserializa7erza7_1966;

													{	/* Unsafe/intext.scm 1569 */
														obj_t BgL_hz00_3962;

														if (((long) CINT(BgL_hashz00_1964) == 0L))
															{	/* Unsafe/intext.scm 1569 */
																BgL_hz00_3962 =
																	BINT(BGl_classzd2hashzd2zz__objectz00
																	(BGl_objectz00zz__objectz00));
															}
														else
															{	/* Unsafe/intext.scm 1569 */
																BgL_hz00_3962 = BgL_hashz00_1964;
															}
														{	/* Unsafe/intext.scm 1569 */
															obj_t BgL_cellz00_3964;

															BgL_cellz00_3964 =
																BGl_assvz00zz__r4_pairs_and_lists_6_3z00
																(BgL_hz00_3962,
																BGl_za2classzd2serializa7ationza2z75zz__intextz00);
															{	/* Unsafe/intext.scm 1570 */

																if (PAIRP(BgL_cellz00_3964))
																	{	/* Unsafe/intext.scm 1571 */
																		BgL_unserializa7erza7_1966 =
																			CAR(CDR(CDR(BgL_cellz00_3964)));
																	}
																else
																	{	/* Unsafe/intext.scm 1571 */
																		BgL_unserializa7erza7_1966 =
																			BGl_errorz00zz__errorz00
																			(BGl_string2595z00zz__intextz00,
																			BGl_string2617z00zz__intextz00,
																			BgL_hashz00_1964);
																	}
															}
														}
													}
													{	/* Unsafe/intext.scm 505 */
														obj_t BgL_valz00_1967;

														BgL_valz00_1967 =
															BGL_PROCEDURE_CALL2(BgL_unserializa7erza7_1966,
															BgL_objz00_1965, BgL_unserializa7ezd2argz75_6692);
														{	/* Unsafe/intext.scm 506 */

															if (INTEGERP(BgL_definingz00_1961))
																{	/* Unsafe/intext.scm 508 */
																	obj_t BgL_vectorz00_3975;
																	long BgL_kz00_3976;

																	BgL_vectorz00_3975 =
																		((obj_t)
																		((obj_t)
																			CELL_REF(BgL_za2definitionsza2z00_6693)));
																	BgL_kz00_3976 =
																		(long) CINT(BgL_definingz00_1961);
																	VECTOR_SET(BgL_vectorz00_3975, BgL_kz00_3976,
																		BgL_valz00_1967);
																}
															else
																{	/* Unsafe/intext.scm 507 */
																	BFALSE;
																}
															return BgL_valz00_1967;
														}
													}
												}
											}
										}
									}
								}
							}
							break;
						case ((unsigned char) 'G'):

							{	/* Unsafe/intext.scm 513 */
								obj_t BgL_definingz00_1971;

								{	/* Unsafe/intext.scm 513 */
									obj_t BgL_oldz00_1980;

									BgL_oldz00_1980 = CELL_REF(BgL_za2definingza2z00_6691);
									{	/* Unsafe/intext.scm 514 */
										obj_t BgL_auxz00_6702;

										BgL_auxz00_6702 = BFALSE;
										CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6702);
									}
									BgL_definingz00_1971 = BgL_oldz00_1980;
								}
								{	/* Unsafe/intext.scm 513 */
									obj_t BgL_claza7za7z00_1972;

									BgL_claza7za7z00_1972 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									{	/* Unsafe/intext.scm 516 */
										obj_t BgL_hashobjz00_1973;

										BgL_hashobjz00_1973 =
											BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
											BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
											BgL_unserializa7ezd2argz75_6692,
											BgL_za2definingza2z00_6691, BgL_za2pointerza2z00_6690,
											BgL_za2strlenza2z00_6689);
										{	/* Unsafe/intext.scm 517 */
											obj_t BgL_hash_z00_1974;

											BgL_hash_z00_1974 =
												BGl_readzd2itemze70z35zz__intextz00
												(BgL_extensionz00_6695, BgL_sz00_6694,
												BgL_za2definitionsza2z00_6693,
												BgL_unserializa7ezd2argz75_6692,
												BgL_za2definingza2z00_6691, BgL_za2pointerza2z00_6690,
												BgL_za2strlenza2z00_6689);
											{	/* Unsafe/intext.scm 518 */
												obj_t BgL_hashz00_1975;

												BgL_hashz00_1975 = CAR(((obj_t) BgL_hashobjz00_1973));
												{	/* Unsafe/intext.scm 519 */
													obj_t BgL_objz00_1976;

													BgL_objz00_1976 = CDR(((obj_t) BgL_hashobjz00_1973));
													{	/* Unsafe/intext.scm 520 */
														obj_t BgL_unserializa7erza7_1977;

														BgL_unserializa7erza7_1977 =
															BGl_findzd2classzd2unserializa7erza7zz__intextz00
															(BgL_hashz00_1975, BgL_claza7za7z00_1972);
														{	/* Unsafe/intext.scm 521 */
															obj_t BgL_valz00_1978;

															BgL_valz00_1978 =
																BGL_PROCEDURE_CALL2(BgL_unserializa7erza7_1977,
																BgL_objz00_1976,
																BgL_unserializa7ezd2argz75_6692);
															{	/* Unsafe/intext.scm 522 */

																if (INTEGERP(BgL_definingz00_1971))
																	{	/* Unsafe/intext.scm 524 */
																		obj_t BgL_vectorz00_3979;
																		long BgL_kz00_3980;

																		BgL_vectorz00_3979 =
																			((obj_t)
																			((obj_t)
																				CELL_REF
																				(BgL_za2definitionsza2z00_6693)));
																		BgL_kz00_3980 =
																			(long) CINT(BgL_definingz00_1971);
																		VECTOR_SET(BgL_vectorz00_3979,
																			BgL_kz00_3980, BgL_valz00_1978);
																	}
																else
																	{	/* Unsafe/intext.scm 523 */
																		BFALSE;
																	}
																return BgL_valz00_1978;
															}
														}
													}
												}
											}
										}
									}
								}
							}
							break;
						case ((unsigned char) 'f'):

							return
								DOUBLE_TO_REAL(BGl_readzd2floatze70z35zz__intextz00
								(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694));
							break;
						case ((unsigned char) 'z'):

							BgL_sz00_1632 = BgL_sz00_6694;
							{	/* Unsafe/intext.scm 195 */
								long BgL_sza7za7_1634;

								{	/* Unsafe/intext.scm 234 */
									long BgL_siza7eza7_3611;

									BgL_siza7eza7_3611 =
										BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
										(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_1632);
									BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
										BgL_siza7eza7_3611, BGl_string2652z00zz__intextz00);
									BgL_sza7za7_1634 = BgL_siza7eza7_3611;
								}
								{	/* Unsafe/intext.scm 195 */
									obj_t BgL_resz00_1635;

									{	/* Unsafe/intext.scm 196 */
										obj_t BgL_arg1365z00_1636;

										{	/* Unsafe/intext.scm 196 */
											long BgL_arg1367z00_1639;

											{	/* Unsafe/intext.scm 196 */
												long BgL_za71za7_3612;

												BgL_za71za7_3612 =
													(long) CINT(
													((obj_t)
														((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
												BgL_arg1367z00_1639 =
													(BgL_za71za7_3612 + BgL_sza7za7_1634);
											}
											{	/* Unsafe/intext.scm 196 */
												long BgL_startz00_3615;

												BgL_startz00_3615 =
													(long) CINT(
													((obj_t)
														((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
												BgL_arg1365z00_1636 =
													c_substring(BgL_sz00_1632, BgL_startz00_3615,
													BgL_arg1367z00_1639);
										}}
										{	/* Ieee/fixnum.scm 1011 */

											BgL_resz00_1635 =
												BGl_stringzd2ze3bignumz31zz__r4_numbers_6_5_fixnumz00
												(BgL_arg1365z00_1636, 10L);
									}}
									{	/* Unsafe/intext.scm 196 */

										{	/* Unsafe/intext.scm 197 */
											obj_t BgL_auxz00_6715;

											BgL_auxz00_6715 =
												ADDFX(
												((obj_t)
													((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))),
												BINT(BgL_sza7za7_1634));
											CELL_SET(BgL_za2pointerza2z00_6690, BgL_auxz00_6715);
										}
										return BgL_resz00_1635;
									}
								}
							}
							break;
						case ((unsigned char) '-'):

							{	/* Unsafe/intext.scm 610 */
								long BgL_arg1616z00_2049;

								BgL_arg1616z00_2049 =
									BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
									(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694);
								return BINT(NEG(BgL_arg1616z00_2049));
							}
							break;
						case ((unsigned char) '!'):

							{	/* Unsafe/intext.scm 425 */
								obj_t BgL_resz00_1900;

								BgL_resz00_1900 = MAKE_YOUNG_CELL(BUNSPEC);
								{	/* Unsafe/intext.scm 426 */
									bool_t BgL_test2844z00_7688;

									{	/* Unsafe/intext.scm 426 */
										obj_t BgL_objz00_3905;

										BgL_objz00_3905 = CELL_REF(BgL_za2definingza2z00_6691);
										BgL_test2844z00_7688 = INTEGERP(BgL_objz00_3905);
									}
									if (BgL_test2844z00_7688)
										{	/* Unsafe/intext.scm 426 */
											{	/* Unsafe/intext.scm 427 */
												obj_t BgL_vectorz00_3906;
												long BgL_kz00_3907;

												BgL_vectorz00_3906 =
													((obj_t)
													((obj_t) CELL_REF(BgL_za2definitionsza2z00_6693)));
												BgL_kz00_3907 =
													(long) CINT(CELL_REF(BgL_za2definingza2z00_6691));
												VECTOR_SET(BgL_vectorz00_3906, BgL_kz00_3907,
													BgL_resz00_1900);
											}
											{	/* Unsafe/intext.scm 428 */
												obj_t BgL_auxz00_6707;

												BgL_auxz00_6707 = BFALSE;
												CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6707);
										}}
									else
										{	/* Unsafe/intext.scm 426 */
											BFALSE;
										}
								}
								CELL_SET(BgL_resz00_1900,
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689));
								return BgL_resz00_1900;
							}
							break;
						case ((unsigned char) 'w'):

							{	/* Unsafe/intext.scm 434 */
								obj_t BgL_resz00_1905;

								{	/* Unsafe/intext.scm 434 */

									BgL_resz00_1905 = bgl_make_weakptr(BUNSPEC, BFALSE);
								}
								{	/* Unsafe/intext.scm 435 */
									bool_t BgL_test2845z00_7696;

									{	/* Unsafe/intext.scm 435 */
										obj_t BgL_objz00_3910;

										BgL_objz00_3910 = CELL_REF(BgL_za2definingza2z00_6691);
										BgL_test2845z00_7696 = INTEGERP(BgL_objz00_3910);
									}
									if (BgL_test2845z00_7696)
										{	/* Unsafe/intext.scm 435 */
											{	/* Unsafe/intext.scm 436 */
												obj_t BgL_vectorz00_3911;
												long BgL_kz00_3912;

												BgL_vectorz00_3911 =
													((obj_t)
													((obj_t) CELL_REF(BgL_za2definitionsza2z00_6693)));
												BgL_kz00_3912 =
													(long) CINT(CELL_REF(BgL_za2definingza2z00_6691));
												VECTOR_SET(BgL_vectorz00_3911, BgL_kz00_3912,
													BgL_resz00_1905);
											}
											{	/* Unsafe/intext.scm 437 */
												obj_t BgL_auxz00_6706;

												BgL_auxz00_6706 = BFALSE;
												CELL_SET(BgL_za2definingza2z00_6691, BgL_auxz00_6706);
										}}
									else
										{	/* Unsafe/intext.scm 435 */
											BFALSE;
										}
								}
								{	/* Unsafe/intext.scm 438 */
									obj_t BgL_arg1535z00_1907;

									BgL_arg1535z00_1907 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									bgl_weakptr_data_set(BgL_resz00_1905, BgL_arg1535z00_1907);
									BUNSPEC;
									BUNSPEC;
								}
								return BgL_resz00_1905;
							}
							break;
						case ((unsigned char) '+'):

							{	/* Unsafe/intext.scm 275 */
								obj_t BgL_strz00_1696;

								BgL_strz00_1696 =
									BGl_readzd2stringze70z35zz__intextz00
									(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
									BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694);
								{	/* Unsafe/intext.scm 275 */
									obj_t BgL_str2z00_1697;

									BgL_str2z00_1697 =
										BGl_readzd2stringze70z35zz__intextz00
										(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
										BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									{	/* Unsafe/intext.scm 276 */
										obj_t BgL_unserializa7erza7_1698;

										{	/* Unsafe/intext.scm 1418 */
											obj_t BgL_cellz00_3676;

											BgL_cellz00_3676 =
												BGl_assocz00zz__r4_pairs_and_lists_6_3z00
												(BgL_strz00_1696,
												BGl_za2customzd2serializa7ationza2z75zz__intextz00);
											if (PAIRP(BgL_cellz00_3676))
												{	/* Unsafe/intext.scm 1419 */
													BgL_unserializa7erza7_1698 =
														CAR(CDR(CDR(BgL_cellz00_3676)));
												}
											else
												{	/* Unsafe/intext.scm 1419 */
													BgL_unserializa7erza7_1698 =
														BGl_errorz00zz__errorz00
														(BGl_string2595z00zz__intextz00,
														BGl_string2650z00zz__intextz00,
														BGl_excerptz00zz__intextz00(BgL_strz00_1696));
												}
										}
										{	/* Unsafe/intext.scm 277 */

											if (PROCEDUREP(BgL_unserializa7erza7_1698))
												{	/* Unsafe/intext.scm 278 */
													return
														BGL_PROCEDURE_CALL1(BgL_unserializa7erza7_1698,
														BgL_str2z00_1697);
												}
											else
												{	/* Unsafe/intext.scm 278 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string2595z00zz__intextz00,
														BGl_string2651z00zz__intextz00, BgL_strz00_1696);
												}
										}
									}
								}
							}
							break;
						case ((unsigned char) 'E'):

							{	/* Unsafe/intext.scm 614 */
								long BgL_tmpz00_7720;

								{	/* Unsafe/intext.scm 291 */
									long BgL_sza7za7_1704;

									{	/* Unsafe/intext.scm 234 */
										long BgL_siza7eza7_3685;

										BgL_siza7eza7_3685 =
											BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
											(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
											BgL_za2strlenza2z00_6689, BgL_sz00_6694);
										BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
											BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
											BgL_siza7eza7_3685, BGl_string2649z00zz__intextz00);
										BgL_sza7za7_1704 = BgL_siza7eza7_3685;
									}
									{	/* Unsafe/intext.scm 291 */
										long BgL_resz00_1705;

										{	/* Unsafe/intext.scm 292 */
											obj_t BgL_arg1410z00_1706;

											{	/* Unsafe/intext.scm 292 */
												long BgL_arg1412z00_1709;

												{	/* Unsafe/intext.scm 292 */
													long BgL_za71za7_3686;

													BgL_za71za7_3686 =
														(long) CINT(
														((obj_t)
															((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
													BgL_arg1412z00_1709 =
														(BgL_za71za7_3686 + BgL_sza7za7_1704);
												}
												{	/* Unsafe/intext.scm 292 */
													long BgL_startz00_3689;

													BgL_startz00_3689 =
														(long) CINT(
														((obj_t)
															((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
													BgL_arg1410z00_1706 =
														c_substring(BgL_sz00_6694, BgL_startz00_3689,
														BgL_arg1412z00_1709);
											}}
											{	/* Ieee/fixnum.scm 1007 */

												BgL_resz00_1705 =
													BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00
													(BgL_arg1410z00_1706, 10L);
										}}
										{	/* Unsafe/intext.scm 292 */

											{	/* Unsafe/intext.scm 293 */
												obj_t BgL_auxz00_6714;

												BgL_auxz00_6714 =
													ADDFX(
													((obj_t)
														((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))),
													BINT(BgL_sza7za7_1704));
												CELL_SET(BgL_za2pointerza2z00_6690, BgL_auxz00_6714);
											}
											BgL_tmpz00_7720 = BgL_resz00_1705;
								}}}
								return make_belong(BgL_tmpz00_7720);
							}
							break;
						case ((unsigned char) 'L'):

							{	/* Unsafe/intext.scm 615 */
								BGL_LONGLONG_T BgL_tmpz00_7734;

								{	/* Unsafe/intext.scm 298 */
									long BgL_sza7za7_1711;

									{	/* Unsafe/intext.scm 234 */
										long BgL_siza7eza7_3693;

										BgL_siza7eza7_3693 =
											BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
											(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
											BgL_za2strlenza2z00_6689, BgL_sz00_6694);
										BGl_checkzd2siza7ez12ze70z80zz__intextz00(BgL_sz00_6694,
											BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689,
											BgL_siza7eza7_3693, BGl_string2648z00zz__intextz00);
										BgL_sza7za7_1711 = BgL_siza7eza7_3693;
									}
									{	/* Unsafe/intext.scm 298 */
										BGL_LONGLONG_T BgL_resz00_1712;

										{	/* Unsafe/intext.scm 299 */
											obj_t BgL_arg1414z00_1713;

											{	/* Unsafe/intext.scm 299 */
												long BgL_arg1416z00_1716;

												{	/* Unsafe/intext.scm 299 */
													long BgL_za71za7_3694;

													BgL_za71za7_3694 =
														(long) CINT(
														((obj_t)
															((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
													BgL_arg1416z00_1716 =
														(BgL_za71za7_3694 + BgL_sza7za7_1711);
												}
												{	/* Unsafe/intext.scm 299 */
													long BgL_startz00_3697;

													BgL_startz00_3697 =
														(long) CINT(
														((obj_t)
															((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
													BgL_arg1414z00_1713 =
														c_substring(BgL_sz00_6694, BgL_startz00_3697,
														BgL_arg1416z00_1716);
											}}
											{	/* Ieee/fixnum.scm 1009 */

												BgL_resz00_1712 =
													BGl_stringzd2ze3llongz31zz__r4_numbers_6_5_fixnumz00
													(BgL_arg1414z00_1713, 10L);
										}}
										{	/* Unsafe/intext.scm 299 */

											{	/* Unsafe/intext.scm 300 */
												obj_t BgL_auxz00_6713;

												BgL_auxz00_6713 =
													ADDFX(
													((obj_t)
														((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))),
													BINT(BgL_sza7za7_1711));
												CELL_SET(BgL_za2pointerza2z00_6690, BgL_auxz00_6713);
											}
											BgL_tmpz00_7734 = BgL_resz00_1712;
								}}}
								return make_bllong(BgL_tmpz00_7734);
							}
							break;
						case ((unsigned char) 'd'):

							{	/* Unsafe/intext.scm 616 */
								long BgL_arg1617z00_2050;

								{	/* Unsafe/intext.scm 616 */
									obj_t BgL_arg1618z00_2051;

									BgL_arg1618z00_2051 =
										BGl_readzd2stringze70z35zz__intextz00
										(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
										BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									{	/* Ieee/fixnum.scm 1007 */

										BgL_arg1617z00_2050 =
											BGl_stringzd2ze3elongz31zz__r4_numbers_6_5_fixnumz00
											(BgL_arg1618z00_2051, 10L);
								}}
								return bgl_seconds_to_date(BgL_arg1617z00_2050);
							}
							break;
						case ((unsigned char) 'D'):

							{	/* Unsafe/intext.scm 617 */
								BGL_LONGLONG_T BgL_arg1619z00_2054;

								{	/* Unsafe/intext.scm 617 */
									obj_t BgL_arg1620z00_2055;

									BgL_arg1620z00_2055 =
										BGl_readzd2stringze70z35zz__intextz00
										(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
										BgL_za2pointerza2z00_6690, BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_sz00_6694);
									{	/* Ieee/fixnum.scm 1009 */

										BgL_arg1619z00_2054 =
											BGl_stringzd2ze3llongz31zz__r4_numbers_6_5_fixnumz00
											(BgL_arg1620z00_2055, 10L);
								}}
								return bgl_nanoseconds_to_date(BgL_arg1619z00_2054);
							}
							break;
						case ((unsigned char) 'k'):

							{	/* Unsafe/intext.scm 528 */
								obj_t BgL_namez00_4036;

								{	/* Unsafe/intext.scm 259 */
									obj_t BgL_arg1396z00_4037;

									BgL_arg1396z00_4037 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									BgL_namez00_4036 =
										bstring_to_symbol(((obj_t) BgL_arg1396z00_4037));
								}
								BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								return BGl_findzd2classzd2zz__objectz00(BgL_namez00_4036);
							}
							break;
						case ((unsigned char) 'r'):

							return
								BGl_pregexpz00zz__regexpz00
								(BGl_readzd2stringze70z35zz__intextz00
								(BgL_za2definingza2z00_6691, BgL_za2definitionsza2z00_6693,
									BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694), BNIL);
							break;
						case ((unsigned char) 'u'):

							{	/* Unsafe/intext.scm 271 */
								long BgL_arg1403z00_4039;

								BgL_arg1403z00_4039 =
									BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
									(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694);
								return
									BUCS2(BGl_integerzd2ze3ucs2z31zz__ucs2z00((int)
										(BgL_arg1403z00_4039)));
							} break;
						case ((unsigned char) 'p'):

							{	/* Unsafe/intext.scm 621 */
								obj_t BgL_converterz00_4040;

								BgL_converterz00_4040 =
									BGl_za2stringzd2ze3procedureza2z31zz__intextz00;
								{	/* Unsafe/intext.scm 445 */
									obj_t BgL_sz00_4041;

									BgL_sz00_4041 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									return BGL_PROCEDURE_CALL1(BgL_converterz00_4040,
										BgL_sz00_4041);
								}
							}
							break;
						case ((unsigned char) 'e'):

							{	/* Unsafe/intext.scm 622 */
								obj_t BgL_converterz00_4042;

								BgL_converterz00_4042 =
									BGl_za2stringzd2ze3processza2z31zz__intextz00;
								{	/* Unsafe/intext.scm 445 */
									obj_t BgL_sz00_4043;

									BgL_sz00_4043 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									return BGL_PROCEDURE_CALL1(BgL_converterz00_4042,
										BgL_sz00_4043);
								}
							}
							break;
						case ((unsigned char) 'o'):

							{	/* Unsafe/intext.scm 623 */
								obj_t BgL_converterz00_4044;

								BgL_converterz00_4044 =
									BGl_za2stringzd2ze3opaqueza2z31zz__intextz00;
								{	/* Unsafe/intext.scm 445 */
									obj_t BgL_sz00_4045;

									BgL_sz00_4045 =
										BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
										BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
										BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
										BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
									return BGL_PROCEDURE_CALL1(BgL_converterz00_4044,
										BgL_sz00_4045);
								}
							}
							break;
						case ((unsigned char) 'X'):

							{	/* Unsafe/intext.scm 284 */
								obj_t BgL_itemz00_1701;

								BgL_itemz00_1701 =
									BGl_readzd2itemze70z35zz__intextz00(BgL_extensionz00_6695,
									BgL_sz00_6694, BgL_za2definitionsza2z00_6693,
									BgL_unserializa7ezd2argz75_6692, BgL_za2definingza2z00_6691,
									BgL_za2pointerza2z00_6690, BgL_za2strlenza2z00_6689);
								if (PROCEDUREP(BgL_extensionz00_6695))
									{	/* Unsafe/intext.scm 285 */
										return
											BGL_PROCEDURE_CALL1(BgL_extensionz00_6695,
											BgL_itemz00_1701);
									}
								else
									{	/* Unsafe/intext.scm 285 */
										return BgL_itemz00_1701;
									}
							}
							break;
						case ((unsigned char) 'b'):

							{	/* Unsafe/intext.scm 544 */
								long BgL_arg1582z00_4046;

								BgL_arg1582z00_4046 =
									BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6690,
									BgL_sz00_6694, BgL_za2strlenza2z00_6689, BgL_sz00_6694,
									(int) (1L));
								{	/* Unsafe/intext.scm 544 */
									int8_t BgL_tmpz00_7789;

									BgL_tmpz00_7789 = (int8_t) (BgL_arg1582z00_4046);
									return BGL_INT8_TO_BINT8(BgL_tmpz00_7789);
								}
							}
							break;
						case ((unsigned char) 'B'):

							{	/* Unsafe/intext.scm 546 */
								long BgL_arg1584z00_4048;

								BgL_arg1584z00_4048 =
									BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6690,
									BgL_sz00_6694, BgL_za2strlenza2z00_6689, BgL_sz00_6694,
									(int) (1L));
								{	/* Unsafe/intext.scm 546 */
									uint8_t BgL_tmpz00_7794;

									BgL_tmpz00_7794 = (uint8_t) (BgL_arg1584z00_4048);
									return BGL_UINT8_TO_BUINT8(BgL_tmpz00_7794);
								}
							}
							break;
						case ((unsigned char) 's'):

							{	/* Unsafe/intext.scm 549 */
								long BgL_arg1586z00_4050;

								BgL_arg1586z00_4050 =
									BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6690,
									BgL_sz00_6694, BgL_za2strlenza2z00_6689, BgL_sz00_6694,
									(int) (2L));
								{	/* Unsafe/intext.scm 549 */
									int16_t BgL_tmpz00_7799;

									BgL_tmpz00_7799 = (int16_t) (BgL_arg1586z00_4050);
									return BGL_INT16_TO_BINT16(BgL_tmpz00_7799);
								}
							}
							break;
						case ((unsigned char) 'S'):

							{	/* Unsafe/intext.scm 551 */
								long BgL_arg1589z00_4052;

								BgL_arg1589z00_4052 =
									BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6690,
									BgL_sz00_6694, BgL_za2strlenza2z00_6689, BgL_sz00_6694,
									(int) (2L));
								{	/* Unsafe/intext.scm 551 */
									uint16_t BgL_tmpz00_7804;

									BgL_tmpz00_7804 = (uint16_t) (BgL_arg1589z00_4052);
									return BGL_UINT16_TO_BUINT16(BgL_tmpz00_7804);
								}
							}
							break;
						case ((unsigned char) 'i'):

							{	/* Unsafe/intext.scm 554 */
								long BgL_arg1591z00_4054;

								BgL_arg1591z00_4054 =
									BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6690,
									BgL_sz00_6694, BgL_za2strlenza2z00_6689, BgL_sz00_6694,
									(int) (4L));
								{	/* Unsafe/intext.scm 554 */
									int32_t BgL_tmpz00_7809;

									BgL_tmpz00_7809 = (int32_t) (BgL_arg1591z00_4054);
									return BGL_INT32_TO_BINT32(BgL_tmpz00_7809);
								}
							}
							break;
						case ((unsigned char) 'I'):

							{	/* Unsafe/intext.scm 556 */
								long BgL_arg1593z00_4056;

								BgL_arg1593z00_4056 =
									BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6690,
									BgL_sz00_6694, BgL_za2strlenza2z00_6689, BgL_sz00_6694,
									(int) (4L));
								{	/* Unsafe/intext.scm 556 */
									uint32_t BgL_tmpz00_7814;

									BgL_tmpz00_7814 = (uint32_t) (BgL_arg1593z00_4056);
									return BGL_UINT32_TO_BUINT32(BgL_tmpz00_7814);
								}
							}
							break;
						case ((unsigned char) 'l'):

							{	/* Unsafe/intext.scm 631 */
								int64_t BgL_tmpz00_7817;

								BgL_sz00_2016 = BgL_sz00_6694;
								{	/* Unsafe/intext.scm 559 */
									int64_t BgL_accz00_2018;

									BgL_accz00_2018 = (int64_t) (0);
									BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_za2pointerza2z00_6690, 8L);
									{
										long BgL_iz00_2020;

										BgL_iz00_2020 = 0L;
									BgL_zc3z04anonymousza31595ze3z87_2021:
										if ((BgL_iz00_2020 < 8L))
											{	/* Unsafe/intext.scm 561 */
												{	/* Unsafe/intext.scm 562 */
													unsigned char BgL_dz00_2023;

													{	/* Unsafe/intext.scm 562 */
														long BgL_kz00_3998;

														BgL_kz00_3998 =
															(long) CINT(
															((obj_t)
																((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
														BgL_dz00_2023 =
															STRING_REF(BgL_sz00_2016, BgL_kz00_3998);
													}
													{	/* Unsafe/intext.scm 563 */
														int64_t BgL_arg1598z00_2024;
														int64_t BgL_arg1601z00_2025;

														BgL_arg1598z00_2024 =
															((int64_t) (256) * BgL_accz00_2018);
														{	/* Unsafe/intext.scm 564 */
															long BgL_tmpz00_7825;

															BgL_tmpz00_7825 = (BgL_dz00_2023);
															BgL_arg1601z00_2025 = (int64_t) (BgL_tmpz00_7825);
														}
														BgL_accz00_2018 =
															(BgL_arg1598z00_2024 + BgL_arg1601z00_2025);
													}
													{	/* Unsafe/intext.scm 565 */
														obj_t BgL_auxz00_6701;

														BgL_auxz00_6701 =
															ADDFX(
															((obj_t)
																((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))),
															BINT(1L));
														CELL_SET(BgL_za2pointerza2z00_6690,
															BgL_auxz00_6701);
												}}
												{
													long BgL_iz00_7832;

													BgL_iz00_7832 = (BgL_iz00_2020 + 1L);
													BgL_iz00_2020 = BgL_iz00_7832;
													goto BgL_zc3z04anonymousza31595ze3z87_2021;
												}
											}
										else
											{	/* Unsafe/intext.scm 561 */
												((bool_t) 0);
											}
									}
									BgL_tmpz00_7817 = BgL_accz00_2018;
								}
								return BGL_INT64_TO_BINT64(BgL_tmpz00_7817);
							}
							break;
						case ((unsigned char) 'W'):

							{	/* Unsafe/intext.scm 632 */
								uint64_t BgL_tmpz00_7835;

								BgL_sz00_2029 = BgL_sz00_6694;
								{	/* Unsafe/intext.scm 569 */
									uint64_t BgL_accz00_2031;

									BgL_accz00_2031 = (uint64_t) (0);
									BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_6694,
										BgL_za2strlenza2z00_6689, BgL_za2pointerza2z00_6690, 8L);
									{
										long BgL_iz00_2033;

										BgL_iz00_2033 = 0L;
									BgL_zc3z04anonymousza31605ze3z87_2034:
										if ((BgL_iz00_2033 < 8L))
											{	/* Unsafe/intext.scm 571 */
												{	/* Unsafe/intext.scm 572 */
													unsigned char BgL_dz00_2036;

													{	/* Unsafe/intext.scm 572 */
														long BgL_kz00_4009;

														BgL_kz00_4009 =
															(long) CINT(
															((obj_t)
																((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))));
														BgL_dz00_2036 =
															STRING_REF(BgL_sz00_2029, BgL_kz00_4009);
													}
													{	/* Unsafe/intext.scm 573 */
														uint64_t BgL_arg1607z00_2037;
														int64_t BgL_arg1608z00_2038;

														BgL_arg1607z00_2037 =
															((uint64_t) (256) * BgL_accz00_2031);
														{	/* Unsafe/intext.scm 574 */
															long BgL_tmpz00_7843;

															BgL_tmpz00_7843 = (BgL_dz00_2036);
															BgL_arg1608z00_2038 = (int64_t) (BgL_tmpz00_7843);
														}
														{	/* Unsafe/intext.scm 573 */
															uint64_t BgL_za72za7_4015;

															BgL_za72za7_4015 =
																(uint64_t) (BgL_arg1608z00_2038);
															BgL_accz00_2031 =
																(BgL_arg1607z00_2037 + BgL_za72za7_4015);
													}}
													{	/* Unsafe/intext.scm 575 */
														obj_t BgL_auxz00_6700;

														BgL_auxz00_6700 =
															ADDFX(
															((obj_t)
																((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))),
															BINT(1L));
														CELL_SET(BgL_za2pointerza2z00_6690,
															BgL_auxz00_6700);
												}}
												{
													long BgL_iz00_7851;

													BgL_iz00_7851 = (BgL_iz00_2033 + 1L);
													BgL_iz00_2033 = BgL_iz00_7851;
													goto BgL_zc3z04anonymousza31605ze3z87_2034;
												}
											}
										else
											{	/* Unsafe/intext.scm 571 */
												((bool_t) 0);
											}
									}
									BgL_tmpz00_7835 = BgL_accz00_2031;
								}
								return BGL_UINT64_TO_BUINT64(BgL_tmpz00_7835);
							}
							break;
						default:
							{	/* Unsafe/intext.scm 633 */
								obj_t BgL_auxz00_6698;

								BgL_auxz00_6698 =
									SUBFX(
									((obj_t)
										((obj_t) CELL_REF(BgL_za2pointerza2z00_6690))), BINT(1L));
								CELL_SET(BgL_za2pointerza2z00_6690, BgL_auxz00_6698);
							}
							return
								BINT(BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00
								(BgL_za2pointerza2z00_6690, BgL_sz00_6694,
									BgL_za2strlenza2z00_6689, BgL_sz00_6694));
						}
				}
			}
		}

	}



/* read-word~0 */
	long BGl_readzd2wordze70z35zz__intextz00(obj_t BgL_za2pointerza2z00_6718,
		obj_t BgL_sz00_6717, long BgL_za2strlenza2z00_6716, obj_t BgL_sz00_1643,
		int BgL_sza7za7_1644)
	{
		{	/* Unsafe/intext.scm 212 */
			{	/* Unsafe/intext.scm 206 */
				long BgL_accz00_1646;

				BgL_accz00_1646 = 0L;
				BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_6717,
					BgL_za2strlenza2z00_6716, BgL_za2pointerza2z00_6718,
					(long) (BgL_sza7za7_1644));
				{
					long BgL_iz00_1648;

					BgL_iz00_1648 = 0L;
				BgL_zc3z04anonymousza31371ze3z87_1649:
					if ((BgL_iz00_1648 < (long) (BgL_sza7za7_1644)))
						{	/* Unsafe/intext.scm 208 */
							{	/* Unsafe/intext.scm 209 */
								unsigned char BgL_dz00_1651;

								{	/* Unsafe/intext.scm 209 */
									long BgL_kz00_3624;

									BgL_kz00_3624 =
										(long) CINT(
										((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6718))));
									BgL_dz00_1651 = STRING_REF(BgL_sz00_1643, BgL_kz00_3624);
								}
								BgL_accz00_1646 = ((256L * BgL_accz00_1646) + (BgL_dz00_1651));
								{	/* Unsafe/intext.scm 211 */
									obj_t BgL_auxz00_6719;

									BgL_auxz00_6719 =
										ADDFX(
										((obj_t)
											((obj_t) CELL_REF(BgL_za2pointerza2z00_6718))), BINT(1L));
									CELL_SET(BgL_za2pointerza2z00_6718, BgL_auxz00_6719);
							}}
							{
								long BgL_iz00_7875;

								BgL_iz00_7875 = (BgL_iz00_1648 + 1L);
								BgL_iz00_1648 = BgL_iz00_7875;
								goto BgL_zc3z04anonymousza31371ze3z87_1649;
							}
						}
					else
						{	/* Unsafe/intext.scm 208 */
							((bool_t) 0);
						}
				}
				return BgL_accz00_1646;
			}
		}

	}



/* string-guard!~0 */
	obj_t BGl_stringzd2guardz12ze70z27zz__intextz00(obj_t BgL_sz00_6722,
		long BgL_za2strlenza2z00_6721, obj_t BgL_za2pointerza2z00_6720,
		long BgL_sza7za7_1601)
	{
		{	/* Unsafe/intext.scm 173 */
			{	/* Unsafe/intext.scm 169 */
				bool_t BgL_test2852z00_7877;

				{	/* Unsafe/intext.scm 169 */
					long BgL_tmpz00_7878;

					{	/* Unsafe/intext.scm 169 */
						long BgL_za72za7_3550;

						BgL_za72za7_3550 =
							(long) CINT(
							((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6720))));
						BgL_tmpz00_7878 = (BgL_sza7za7_1601 + BgL_za72za7_3550);
					}
					BgL_test2852z00_7877 = (BgL_tmpz00_7878 > BgL_za2strlenza2z00_6721);
				}
				if (BgL_test2852z00_7877)
					{	/* Unsafe/intext.scm 171 */
						obj_t BgL_arg1343z00_1605;

						{	/* Unsafe/intext.scm 171 */
							long BgL_arg1344z00_1606;

							{	/* Unsafe/intext.scm 171 */
								long BgL_za72za7_3554;

								BgL_za72za7_3554 =
									(long) CINT(
									((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6720))));
								BgL_arg1344z00_1606 = (BgL_sza7za7_1601 + BgL_za72za7_3554);
							}
							{	/* Unsafe/intext.scm 171 */
								obj_t BgL_list1345z00_1607;

								{	/* Unsafe/intext.scm 171 */
									obj_t BgL_arg1346z00_1608;

									{	/* Unsafe/intext.scm 171 */
										obj_t BgL_arg1347z00_1609;

										BgL_arg1347z00_1609 =
											MAKE_YOUNG_PAIR(BINT(BgL_za2strlenza2z00_6721), BNIL);
										BgL_arg1346z00_1608 =
											MAKE_YOUNG_PAIR(
											((obj_t)
												((obj_t) CELL_REF(BgL_za2pointerza2z00_6720))),
											BgL_arg1347z00_1609);
									}
									BgL_list1345z00_1607 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1344z00_1606),
										BgL_arg1346z00_1608);
								}
								BgL_arg1343z00_1605 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2653z00zz__intextz00, BgL_list1345z00_1607);
						}}
						return
							BGl_errorz00zz__errorz00(BGl_string2595z00zz__intextz00,
							BgL_arg1343z00_1605, BgL_sz00_6722);
					}
				else
					{	/* Unsafe/intext.scm 169 */
						return BFALSE;
					}
			}
		}

	}



/* read-size/unsafe~0 */
	long BGl_readzd2siza7ezf2unsafeze70z60zz__intextz00(obj_t
		BgL_za2pointerza2z00_6725, obj_t BgL_sz00_6724,
		long BgL_za2strlenza2z00_6723, obj_t BgL_sz00_1670)
	{
		{	/* Unsafe/intext.scm 227 */
			BGl_stringzd2guardz12ze70z27zz__intextz00(BgL_sz00_6724,
				BgL_za2strlenza2z00_6723, BgL_za2pointerza2z00_6725, 1L);
			{	/* Unsafe/intext.scm 228 */
				long BgL_sza7za7_3643;

				{	/* Unsafe/intext.scm 228 */
					unsigned char BgL_tmpz00_7895;

					{	/* Unsafe/intext.scm 228 */
						long BgL_kz00_3646;

						BgL_kz00_3646 =
							(long) CINT(
							((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6725))));
						BgL_tmpz00_7895 = STRING_REF(BgL_sz00_1670, BgL_kz00_3646);
					}
					BgL_sza7za7_3643 = (BgL_tmpz00_7895);
				}
				{	/* Unsafe/intext.scm 229 */
					obj_t BgL_auxz00_6726;

					BgL_auxz00_6726 =
						ADDFX(
						((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6725))), BINT(1L));
					CELL_SET(BgL_za2pointerza2z00_6725, BgL_auxz00_6726);
				}
				return
					BGl_readzd2wordze70z35zz__intextz00(BgL_za2pointerza2z00_6725,
					BgL_sz00_6724, BgL_za2strlenza2z00_6723, BgL_sz00_1670,
					(int) (BgL_sza7za7_3643));
		}}

	}



/* check-size!~0 */
	obj_t BGl_checkzd2siza7ez12ze70z80zz__intextz00(obj_t BgL_sz00_6729,
		obj_t BgL_za2pointerza2z00_6728, long BgL_za2strlenza2z00_6727,
		long BgL_sza7za7_1611, obj_t BgL_lblz00_1612)
	{
		{	/* Unsafe/intext.scm 180 */
			{	/* Unsafe/intext.scm 177 */
				bool_t BgL_test2853z00_7905;

				if ((BgL_sza7za7_1611 < 0L))
					{	/* Unsafe/intext.scm 177 */
						BgL_test2853z00_7905 = ((bool_t) 1);
					}
				else
					{	/* Unsafe/intext.scm 177 */
						long BgL_tmpz00_7908;

						{	/* Unsafe/intext.scm 177 */
							long BgL_za72za7_3557;

							BgL_za72za7_3557 =
								(long) CINT(
								((obj_t) ((obj_t) CELL_REF(BgL_za2pointerza2z00_6728))));
							BgL_tmpz00_7908 = (BgL_za2strlenza2z00_6727 - BgL_za72za7_3557);
						}
						BgL_test2853z00_7905 = (BgL_sza7za7_1611 > BgL_tmpz00_7908);
					}
				if (BgL_test2853z00_7905)
					{	/* Unsafe/intext.scm 179 */
						obj_t BgL_arg1354z00_1617;

						{	/* Unsafe/intext.scm 179 */
							obj_t BgL_list1355z00_1618;

							{	/* Unsafe/intext.scm 179 */
								obj_t BgL_arg1356z00_1619;

								{	/* Unsafe/intext.scm 179 */
									obj_t BgL_arg1357z00_1620;

									{	/* Unsafe/intext.scm 179 */
										obj_t BgL_arg1358z00_1621;

										BgL_arg1358z00_1621 =
											MAKE_YOUNG_PAIR(BINT(BgL_sza7za7_1611), BNIL);
										BgL_arg1357z00_1620 =
											MAKE_YOUNG_PAIR(BINT(BgL_za2strlenza2z00_6727),
											BgL_arg1358z00_1621);
									}
									BgL_arg1356z00_1619 =
										MAKE_YOUNG_PAIR(
										((obj_t)
											((obj_t) CELL_REF(BgL_za2pointerza2z00_6728))),
										BgL_arg1357z00_1620);
								}
								BgL_list1355z00_1618 =
									MAKE_YOUNG_PAIR(BgL_lblz00_1612, BgL_arg1356z00_1619);
							}
							BgL_arg1354z00_1617 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2654z00zz__intextz00, BgL_list1355z00_1618);
						}
						return
							BGl_errorz00zz__errorz00(BGl_string2595z00zz__intextz00,
							BgL_arg1354z00_1617, BgL_sz00_6729);
					}
				else
					{	/* Unsafe/intext.scm 177 */
						return BFALSE;
					}
			}
		}

	}



/* _obj->string */
	obj_t BGl__objzd2ze3stringz31zz__intextz00(obj_t BgL_env1203z00_26,
		obj_t BgL_opt1202z00_25)
	{
		{	/* Unsafe/intext.scm 646 */
			{	/* Unsafe/intext.scm 646 */
				obj_t BgL_g1204z00_2105;

				BgL_g1204z00_2105 = VECTOR_REF(BgL_opt1202z00_25, 0L);
				switch (VECTOR_LENGTH(BgL_opt1202z00_25))
					{
					case 1L:

						{	/* Unsafe/intext.scm 646 */

							return obj_to_string(BgL_g1204z00_2105, BFALSE);
						}
						break;
					case 2L:

						{	/* Unsafe/intext.scm 646 */
							obj_t BgL_markzd2argzd2_2109;

							BgL_markzd2argzd2_2109 = VECTOR_REF(BgL_opt1202z00_25, 1L);
							{	/* Unsafe/intext.scm 646 */

								return obj_to_string(BgL_g1204z00_2105, BgL_markzd2argzd2_2109);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* obj->string */
	BGL_EXPORTED_DEF obj_t obj_to_string(obj_t BgL_objz00_23,
		obj_t BgL_markzd2argzd2_24)
	{
		{	/* Unsafe/intext.scm 646 */
			{	/* Unsafe/intext.scm 647 */
				obj_t BgL_tablez00_2110;

				BgL_tablez00_2110 =
					BGl_createzd2hashtablezd2zz__hashz00(BGL_REAL_CNST
					(BGl_real2658z00zz__intextz00), BGl_proc2655z00zz__intextz00, BFALSE,
					BINT(10L), BINT(-1L), BFALSE, BINT(128L),
					BGl_symbol2656z00zz__intextz00);
				{	/* Unsafe/intext.scm 647 */
					long BgL_nbrefz00_2111;

					BgL_nbrefz00_2111 =
						BGl_markzd2objz12zc0zz__intextz00(BgL_tablez00_2110, BgL_objz00_23,
						BgL_markzd2argzd2_24);
					{	/* Unsafe/intext.scm 657 */

						return
							BGl_printzd2objzd2zz__intextz00(BgL_tablez00_2110,
							BgL_nbrefz00_2111, BgL_objz00_23);
					}
				}
			}
		}

	}



/* &<@anonymous:1624> */
	obj_t BGl_z62zc3z04anonymousza31624ze3ze5zz__intextz00(obj_t BgL_envz00_6267,
		obj_t BgL_az00_6268, obj_t BgL_bz00_6269)
	{
		{	/* Unsafe/intext.scm 648 */
			{	/* Unsafe/intext.scm 650 */
				bool_t BgL_tmpz00_7934;

				if (STRINGP(BgL_az00_6268))
					{	/* Unsafe/intext.scm 650 */
						if (STRINGP(BgL_bz00_6269))
							{	/* Unsafe/intext.scm 651 */
								long BgL_l1z00_6797;

								BgL_l1z00_6797 = STRING_LENGTH(BgL_az00_6268);
								if ((BgL_l1z00_6797 == STRING_LENGTH(BgL_bz00_6269)))
									{	/* Unsafe/intext.scm 651 */
										int BgL_arg2219z00_6798;

										{	/* Unsafe/intext.scm 651 */
											char *BgL_auxz00_7945;
											char *BgL_tmpz00_7943;

											BgL_auxz00_7945 = BSTRING_TO_STRING(BgL_bz00_6269);
											BgL_tmpz00_7943 = BSTRING_TO_STRING(BgL_az00_6268);
											BgL_arg2219z00_6798 =
												memcmp(BgL_tmpz00_7943, BgL_auxz00_7945,
												BgL_l1z00_6797);
										}
										BgL_tmpz00_7934 = ((long) (BgL_arg2219z00_6798) == 0L);
									}
								else
									{	/* Unsafe/intext.scm 651 */
										BgL_tmpz00_7934 = ((bool_t) 0);
									}
							}
						else
							{	/* Unsafe/intext.scm 651 */
								BgL_tmpz00_7934 = ((bool_t) 0);
							}
					}
				else
					{	/* Unsafe/intext.scm 650 */
						if (UCS2_STRINGP(BgL_az00_6268))
							{	/* Unsafe/intext.scm 652 */
								BgL_tmpz00_7934 =
									BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_az00_6268,
									BgL_bz00_6269);
							}
						else
							{	/* Unsafe/intext.scm 652 */
								BgL_tmpz00_7934 = (BgL_az00_6268 == BgL_bz00_6269);
							}
					}
				return BBOOL(BgL_tmpz00_7934);
			}
		}

	}



/* size-of-word */
	long BGl_siza7ezd2ofzd2wordza7zz__intextz00(long BgL_mz00_51)
	{
		{	/* Unsafe/intext.scm 691 */
			{
				long BgL_siza7eza7_4119;
				long BgL_mz00_4120;

				BgL_siza7eza7_4119 = 0L;
				BgL_mz00_4120 = BgL_mz00_51;
			BgL_loopz00_4118:
				if ((BgL_mz00_4120 == 0L))
					{	/* Unsafe/intext.scm 694 */
						return BgL_siza7eza7_4119;
					}
				else
					{
						long BgL_mz00_7959;
						long BgL_siza7eza7_7957;

						BgL_siza7eza7_7957 = (BgL_siza7eza7_4119 + 1L);
						BgL_mz00_7959 = (BgL_mz00_4120 >> (int) (8L));
						BgL_mz00_4120 = BgL_mz00_7959;
						BgL_siza7eza7_4119 = BgL_siza7eza7_7957;
						goto BgL_loopz00_4118;
					}
			}
		}

	}



/* print-obj */
	obj_t BGl_printzd2objzd2zz__intextz00(obj_t BgL_tablez00_52,
		long BgL_nbrefz00_53, obj_t BgL_objz00_54)
	{
		{	/* Unsafe/intext.scm 701 */
			{	/* Unsafe/intext.scm 701 */
				obj_t BgL_ptrz00_6603;
				obj_t BgL_refz00_6604;

				BgL_ptrz00_6603 = MAKE_CELL(BINT(0L));
				BgL_refz00_6604 = MAKE_CELL(BINT(0L));
				{	/* Unsafe/intext.scm 703 */
					obj_t BgL_bufferz00_6605;

					{	/* Unsafe/intext.scm 703 */
						obj_t BgL_cellvalz00_7964;

						BgL_cellvalz00_7964 = make_string_sans_fill(100L);
						BgL_bufferz00_6605 = MAKE_CELL(BgL_cellvalz00_7964);
					}
					{	/* Unsafe/intext.scm 709 */
						obj_t BgL_printzd2customzd2_6278;
						obj_t BgL_printzd2tvectorzd2_6279;
						obj_t BgL_printzd2hvectorzd2_6281;
						obj_t BgL_printzd2vectorzd2_6282;
						obj_t BgL_printzd2weakptrzd2_6283;
						obj_t BgL_printzd2cellzd2_6284;
						obj_t BgL_printzd2objectzd2_6287;
						obj_t BgL_printzd2classzd2_6286;
						obj_t BgL_printzd2pairzd2_6289;
						obj_t BgL_printzd2epairzd2_6290;

						{
							int BgL_tmpz00_7966;

							BgL_tmpz00_7966 = (int) (2L);
							BgL_printzd2customzd2_6278 =
								MAKE_L_PROCEDURE(BGl_z62printzd2customzb0zz__intextz00,
								BgL_tmpz00_7966);
						}
						{
							int BgL_tmpz00_7969;

							BgL_tmpz00_7969 = (int) (13L);
							BgL_printzd2tvectorzd2_6279 =
								MAKE_L_PROCEDURE(BGl_z62printzd2tvectorzb0zz__intextz00,
								BgL_tmpz00_7969);
						}
						{
							int BgL_tmpz00_7972;

							BgL_tmpz00_7972 = (int) (2L);
							BgL_printzd2hvectorzd2_6281 =
								MAKE_L_PROCEDURE(BGl_z62printzd2hvectorzb0zz__intextz00,
								BgL_tmpz00_7972);
						}
						{
							int BgL_tmpz00_7975;

							BgL_tmpz00_7975 = (int) (13L);
							BgL_printzd2vectorzd2_6282 =
								MAKE_L_PROCEDURE(BGl_z62printzd2vectorzb0zz__intextz00,
								BgL_tmpz00_7975);
						}
						{
							int BgL_tmpz00_7978;

							BgL_tmpz00_7978 = (int) (13L);
							BgL_printzd2weakptrzd2_6283 =
								MAKE_L_PROCEDURE(BGl_z62printzd2weakptrzb0zz__intextz00,
								BgL_tmpz00_7978);
						}
						{
							int BgL_tmpz00_7981;

							BgL_tmpz00_7981 = (int) (13L);
							BgL_printzd2cellzd2_6284 =
								MAKE_L_PROCEDURE(BGl_z62printzd2cellzb0zz__intextz00,
								BgL_tmpz00_7981);
						}
						{
							int BgL_tmpz00_7984;

							BgL_tmpz00_7984 = (int) (13L);
							BgL_printzd2objectzd2_6287 =
								MAKE_L_PROCEDURE(BGl_z62printzd2objectzb0zz__intextz00,
								BgL_tmpz00_7984);
						}
						{
							int BgL_tmpz00_7987;

							BgL_tmpz00_7987 = (int) (13L);
							BgL_printzd2classzd2_6286 =
								MAKE_L_PROCEDURE(BGl_z62printzd2classzb0zz__intextz00,
								BgL_tmpz00_7987);
						}
						{
							int BgL_tmpz00_7990;

							BgL_tmpz00_7990 = (int) (13L);
							BgL_printzd2pairzd2_6289 =
								MAKE_L_PROCEDURE(BGl_z62printzd2pairzb0zz__intextz00,
								BgL_tmpz00_7990);
						}
						{
							int BgL_tmpz00_7993;

							BgL_tmpz00_7993 = (int) (13L);
							BgL_printzd2epairzd2_6290 =
								MAKE_L_PROCEDURE(BGl_z62printzd2epairzb0zz__intextz00,
								BgL_tmpz00_7993);
						}
						PROCEDURE_L_SET(BgL_printzd2customzd2_6278,
							(int) (0L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2customzd2_6278,
							(int) (1L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (0L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (1L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (2L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (3L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (4L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (5L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (6L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (7L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (10L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (11L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2tvectorzd2_6279,
							(int) (12L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2hvectorzd2_6281,
							(int) (0L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2hvectorzd2_6281,
							(int) (1L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (0L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (1L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (2L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (3L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (4L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (5L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (6L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (7L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (10L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (11L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2vectorzd2_6282,
							(int) (12L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (0L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (1L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (2L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (3L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (4L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (5L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (6L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (7L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (10L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (11L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2weakptrzd2_6283,
							(int) (12L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (0L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (1L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (2L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (3L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (4L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (5L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (6L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (7L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (10L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (11L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2cellzd2_6284,
							(int) (12L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (0L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (1L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (2L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (3L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (4L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (5L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (6L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (7L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (8L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (10L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (11L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2objectzd2_6287,
							(int) (12L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (0L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (1L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (2L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (3L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (4L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (5L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (6L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (7L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (10L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (11L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2classzd2_6286,
							(int) (12L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (0L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (1L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (2L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (3L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (4L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (5L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (6L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (7L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (9L), BgL_printzd2epairzd2_6290);
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (10L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (11L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2pairzd2_6289,
							(int) (12L), BgL_tablez00_52);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (0L), ((obj_t) BgL_refz00_6604));
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (1L), BgL_printzd2customzd2_6278);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (2L), BgL_printzd2tvectorzd2_6279);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (3L), BgL_printzd2hvectorzd2_6281);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (4L), BgL_printzd2vectorzd2_6282);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (5L), BgL_printzd2weakptrzd2_6283);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (6L), BgL_printzd2cellzd2_6284);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (7L), BgL_printzd2classzd2_6286);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (8L), BgL_printzd2objectzd2_6287);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (9L), BgL_printzd2pairzd2_6289);
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (10L), ((obj_t) BgL_bufferz00_6605));
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (11L), ((obj_t) BgL_ptrz00_6603));
						PROCEDURE_L_SET(BgL_printzd2epairzd2_6290,
							(int) (12L), BgL_tablez00_52);
						if ((BgL_nbrefz00_53 > 0L))
							{	/* Unsafe/intext.scm 1157 */
								BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6605,
									BgL_ptrz00_6603, BINT(1L));
								{	/* Unsafe/intext.scm 719 */
									obj_t BgL_stringz00_5412;
									long BgL_kz00_5413;

									BgL_stringz00_5412 = CELL_REF(BgL_bufferz00_6605);
									BgL_kz00_5413 = (long) CINT(CELL_REF(BgL_ptrz00_6603));
									STRING_SET(BgL_stringz00_5412, BgL_kz00_5413,
										((unsigned char) 'c'));
								}
								{	/* Unsafe/intext.scm 720 */
									obj_t BgL_auxz00_6606;

									BgL_auxz00_6606 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6603));
									CELL_SET(BgL_ptrz00_6603, BgL_auxz00_6606);
								}
								if ((BgL_nbrefz00_53 < 0L))
									{	/* Unsafe/intext.scm 802 */
										BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6605,
											BgL_ptrz00_6603, BINT(1L));
										{	/* Unsafe/intext.scm 719 */
											obj_t BgL_stringz00_5420;
											long BgL_kz00_5421;

											BgL_stringz00_5420 = CELL_REF(BgL_bufferz00_6605);
											BgL_kz00_5421 = (long) CINT(CELL_REF(BgL_ptrz00_6603));
											STRING_SET(BgL_stringz00_5420, BgL_kz00_5421,
												((unsigned char) '-'));
										}
										{	/* Unsafe/intext.scm 720 */
											obj_t BgL_auxz00_6607;

											BgL_auxz00_6607 =
												ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6603));
											CELL_SET(BgL_ptrz00_6603, BgL_auxz00_6607);
										}
										BGl_z62printzd2wordzb0zz__intextz00(BgL_ptrz00_6603,
											BgL_bufferz00_6605, NEG(BgL_nbrefz00_53));
									}
								else
									{	/* Unsafe/intext.scm 802 */
										BGl_z62printzd2wordzb0zz__intextz00(BgL_ptrz00_6603,
											BgL_bufferz00_6605, BgL_nbrefz00_53);
									}
							}
						else
							{	/* Unsafe/intext.scm 1157 */
								BFALSE;
							}
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6290,
							BgL_printzd2pairzd2_6289, BgL_printzd2objectzd2_6287,
							BgL_printzd2classzd2_6286, BgL_printzd2cellzd2_6284,
							BgL_printzd2weakptrzd2_6283, BgL_printzd2vectorzd2_6282,
							BgL_printzd2hvectorzd2_6281, BgL_printzd2tvectorzd2_6279,
							BgL_printzd2customzd2_6278, BgL_refz00_6604, BgL_tablez00_52,
							BgL_ptrz00_6603, BgL_bufferz00_6605, BgL_objz00_54);
						{	/* Unsafe/intext.scm 1163 */
							obj_t BgL_sz00_5425;
							long BgL_lz00_5426;

							BgL_sz00_5425 = CELL_REF(BgL_bufferz00_6605);
							BgL_lz00_5426 = (long) CINT(CELL_REF(BgL_ptrz00_6603));
							return bgl_string_shrink(BgL_sz00_5425, BgL_lz00_5426);
						}
					}
				}
			}
		}

	}



/* &print-word/size */
	bool_t BGl_z62printzd2wordzf2siza7eze5zz__intextz00(obj_t BgL_ptrz00_6293,
		obj_t BgL_bufferz00_6292, obj_t BgL_mz00_2214, long BgL_siza7eza7_2215)
	{
		{	/* Unsafe/intext.scm 756 */
			{	/* Unsafe/intext.scm 752 */
				long BgL_g1065z00_2217;

				BgL_g1065z00_2217 = (BgL_siza7eza7_2215 - 1L);
				{
					long BgL_iz00_2219;

					BgL_iz00_2219 = BgL_g1065z00_2217;
				BgL_zc3z04anonymousza31658ze3z87_2220:
					if ((BgL_iz00_2219 >= 0L))
						{	/* Unsafe/intext.scm 754 */
							long BgL_dz00_2222;

							BgL_dz00_2222 =
								(
								((long) CINT(BgL_mz00_2214) >>
									(int) ((8L * BgL_iz00_2219))) & 255L);
							{	/* Unsafe/intext.scm 748 */
								unsigned char BgL_arg1656z00_4187;

								BgL_arg1656z00_4187 = (BgL_dz00_2222);
								BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6293,
									BgL_bufferz00_6292, BgL_arg1656z00_4187);
							}
							{
								long BgL_iz00_8272;

								BgL_iz00_8272 = (BgL_iz00_2219 - 1L);
								BgL_iz00_2219 = BgL_iz00_8272;
								goto BgL_zc3z04anonymousza31658ze3z87_2220;
							}
						}
					else
						{	/* Unsafe/intext.scm 753 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* &!print-markup */
	obj_t BGl_z62z12printzd2markupza2zz__intextz00(obj_t BgL_ptrz00_6295,
		obj_t BgL_bufferz00_6294, unsigned char BgL_cz00_2197)
	{
		{	/* Unsafe/intext.scm 718 */
			BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6294,
				BgL_ptrz00_6295, BINT(1L));
			{	/* Unsafe/intext.scm 719 */
				obj_t BgL_stringz00_4143;
				long BgL_kz00_4144;

				BgL_stringz00_4143 = CELL_REF(BgL_bufferz00_6294);
				BgL_kz00_4144 = (long) CINT(CELL_REF(BgL_ptrz00_6295));
				STRING_SET(BgL_stringz00_4143, BgL_kz00_4144, BgL_cz00_2197);
			}
			{	/* Unsafe/intext.scm 720 */
				obj_t BgL_auxz00_6296;

				BgL_auxz00_6296 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6295));
				return CELL_SET(BgL_ptrz00_6295, BgL_auxz00_6296);
			}
		}

	}



/* &check-buffer! */
	obj_t BGl_z62checkzd2bufferz12za2zz__intextz00(obj_t BgL_bufferz00_6298,
		obj_t BgL_ptrz00_6297, obj_t BgL_siza7eza7_2186)
	{
		{	/* Unsafe/intext.scm 714 */
			{	/* Unsafe/intext.scm 709 */
				long BgL_lz00_2188;
				long BgL_blz00_2189;

				{	/* Unsafe/intext.scm 709 */
					long BgL_za71za7_4132;

					BgL_za71za7_4132 = (long) CINT(CELL_REF(BgL_ptrz00_6297));
					BgL_lz00_2188 =
						(BgL_za71za7_4132 +
						((long) CINT(BgL_siza7eza7_2186) +
							BGl_za2maxzd2siza7ezd2wordza2za7zz__intextz00));
				}
				{	/* Unsafe/intext.scm 710 */
					obj_t BgL_stringz00_4134;

					BgL_stringz00_4134 = CELL_REF(BgL_bufferz00_6298);
					BgL_blz00_2189 = STRING_LENGTH(BgL_stringz00_4134);
				}
				if ((BgL_lz00_2188 >= BgL_blz00_2189))
					{	/* Unsafe/intext.scm 712 */
						obj_t BgL_nbufz00_2191;

						{	/* Unsafe/intext.scm 712 */
							long BgL_arg1648z00_2192;

							BgL_arg1648z00_2192 = (2L * (BgL_lz00_2188 + 100L));
							{	/* Ieee/string.scm 172 */

								BgL_nbufz00_2191 =
									make_string(BgL_arg1648z00_2192, ((unsigned char) ' '));
						}}
						{	/* Unsafe/intext.scm 713 */
							obj_t BgL_s1z00_4140;

							BgL_s1z00_4140 = CELL_REF(BgL_bufferz00_6298);
							blit_string(BgL_s1z00_4140, 0L, BgL_nbufz00_2191, 0L,
								BgL_blz00_2189);
						}
						{	/* Unsafe/intext.scm 714 */
							obj_t BgL_auxz00_6299;

							BgL_auxz00_6299 = BgL_nbufz00_2191;
							return CELL_SET(BgL_bufferz00_6298, BgL_auxz00_6299);
						}
					}
				else
					{	/* Unsafe/intext.scm 711 */
						return BFALSE;
					}
			}
		}

	}



/* &print-item */
	obj_t BGl_z62printzd2itemzb0zz__intextz00(obj_t BgL_printzd2epairzd2_6313,
		obj_t BgL_printzd2pairzd2_6312, obj_t BgL_printzd2objectzd2_6311,
		obj_t BgL_printzd2classzd2_6310, obj_t BgL_printzd2cellzd2_6309,
		obj_t BgL_printzd2weakptrzd2_6308, obj_t BgL_printzd2vectorzd2_6307,
		obj_t BgL_printzd2hvectorzd2_6306, obj_t BgL_printzd2tvectorzd2_6305,
		obj_t BgL_printzd2customzd2_6304, obj_t BgL_refz00_6303,
		obj_t BgL_tablez00_6302, obj_t BgL_ptrz00_6301, obj_t BgL_bufferz00_6300,
		obj_t BgL_itemz00_2565)
	{
		{	/* Unsafe/intext.scm 1155 */
		BGl_z62printzd2itemzb0zz__intextz00:
			{
				obj_t BgL_mz00_2227;
				obj_t BgL_mz00_2240;
				obj_t BgL_itemz00_2202;
				obj_t BgL_itemz00_2293;
				obj_t BgL_printerz00_2294;

				{	/* Unsafe/intext.scm 1045 */
					bool_t BgL_test2864z00_8291;

					if (EPAIRP(BgL_itemz00_2565))
						{	/* Unsafe/intext.scm 1045 */
							BgL_test2864z00_8291 = BGl_za2epairzf3za2zf3zz__intextz00;
						}
					else
						{	/* Unsafe/intext.scm 1045 */
							BgL_test2864z00_8291 = ((bool_t) 0);
						}
					if (BgL_test2864z00_8291)
						{	/* Unsafe/intext.scm 1045 */
							BgL_itemz00_2293 = BgL_itemz00_2565;
							BgL_printerz00_2294 = BgL_printzd2epairzd2_6313;
						BgL_zc3z04anonymousza31718ze3z87_2295:
							{	/* Unsafe/intext.scm 815 */
								obj_t BgL_markz00_2296;

								BgL_markz00_2296 =
									BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6302,
									BgL_itemz00_2293);
								if (((long) CINT(STRUCT_REF(((obj_t) BgL_markz00_2296),
												(int) (3L))) >= 0L))
									{	/* Unsafe/intext.scm 817 */
										BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6300,
											BgL_ptrz00_6301, BINT(1L));
										{	/* Unsafe/intext.scm 719 */
											obj_t BgL_stringz00_4281;
											long BgL_kz00_4282;

											BgL_stringz00_4281 = CELL_REF(BgL_bufferz00_6300);
											BgL_kz00_4282 = (long) CINT(CELL_REF(BgL_ptrz00_6301));
											STRING_SET(BgL_stringz00_4281, BgL_kz00_4282,
												((unsigned char) '#'));
										}
										{	/* Unsafe/intext.scm 720 */
											obj_t BgL_auxz00_6349;

											BgL_auxz00_6349 =
												ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6301));
											CELL_SET(BgL_ptrz00_6301, BgL_auxz00_6349);
										}
										{	/* Unsafe/intext.scm 819 */
											obj_t BgL_arg1720z00_2298;

											BgL_arg1720z00_2298 =
												STRUCT_REF(((obj_t) BgL_markz00_2296), (int) (3L));
											{	/* Unsafe/intext.scm 819 */
												long BgL_iz00_4286;

												BgL_iz00_4286 = (long) CINT(BgL_arg1720z00_2298);
												if ((BgL_iz00_4286 < 0L))
													{	/* Unsafe/intext.scm 802 */
														BGl_z62checkzd2bufferz12za2zz__intextz00
															(BgL_bufferz00_6300, BgL_ptrz00_6301, BINT(1L));
														{	/* Unsafe/intext.scm 719 */
															obj_t BgL_stringz00_4290;
															long BgL_kz00_4291;

															BgL_stringz00_4290 = CELL_REF(BgL_bufferz00_6300);
															BgL_kz00_4291 =
																(long) CINT(CELL_REF(BgL_ptrz00_6301));
															STRING_SET(BgL_stringz00_4290, BgL_kz00_4291,
																((unsigned char) '-'));
														}
														{	/* Unsafe/intext.scm 720 */
															obj_t BgL_auxz00_6350;

															BgL_auxz00_6350 =
																ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6301));
															CELL_SET(BgL_ptrz00_6301, BgL_auxz00_6350);
														}
														{	/* Unsafe/intext.scm 805 */
															long BgL_arg1714z00_4288;

															BgL_arg1714z00_4288 = NEG(BgL_iz00_4286);
															return
																BGl_z62printzd2wordzb0zz__intextz00
																(BgL_ptrz00_6301, BgL_bufferz00_6300,
																BgL_arg1714z00_4288);
														}
													}
												else
													{	/* Unsafe/intext.scm 802 */
														return
															BGl_z62printzd2wordzb0zz__intextz00
															(BgL_ptrz00_6301, BgL_bufferz00_6300,
															BgL_iz00_4286);
													}
											}
										}
									}
								else
									{	/* Unsafe/intext.scm 817 */
										if (
											((long) CINT(STRUCT_REF(
														((obj_t) BgL_markz00_2296), (int) (2L))) == 0L))
											{	/* Unsafe/intext.scm 820 */
												return
													((obj_t(*)(obj_t, obj_t,
															obj_t))
													PROCEDURE_L_ENTRY(BgL_printerz00_2294))
													(BgL_printerz00_2294, BgL_itemz00_2293,
													BgL_markz00_2296);
											}
										else
											{	/* Unsafe/intext.scm 820 */
												{	/* Unsafe/intext.scm 823 */
													obj_t BgL_vz00_4297;

													BgL_vz00_4297 = CELL_REF(BgL_refz00_6303);
													{	/* Unsafe/intext.scm 663 */
														int BgL_auxz00_8335;
														obj_t BgL_tmpz00_8333;

														BgL_auxz00_8335 = (int) (3L);
														BgL_tmpz00_8333 = ((obj_t) BgL_markz00_2296);
														STRUCT_SET(BgL_tmpz00_8333, BgL_auxz00_8335,
															BgL_vz00_4297);
												}}
												BGl_z62checkzd2bufferz12za2zz__intextz00
													(BgL_bufferz00_6300, BgL_ptrz00_6301, BINT(1L));
												{	/* Unsafe/intext.scm 719 */
													obj_t BgL_stringz00_4299;
													long BgL_kz00_4300;

													BgL_stringz00_4299 = CELL_REF(BgL_bufferz00_6300);
													BgL_kz00_4300 =
														(long) CINT(CELL_REF(BgL_ptrz00_6301));
													STRING_SET(BgL_stringz00_4299, BgL_kz00_4300,
														((unsigned char) '='));
												}
												{	/* Unsafe/intext.scm 720 */
													obj_t BgL_auxz00_6351;

													BgL_auxz00_6351 =
														ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6301));
													CELL_SET(BgL_ptrz00_6301, BgL_auxz00_6351);
												}
												{	/* Unsafe/intext.scm 825 */
													long BgL_iz00_4303;

													BgL_iz00_4303 =
														(long) CINT(CELL_REF(BgL_refz00_6303));
													if ((BgL_iz00_4303 < 0L))
														{	/* Unsafe/intext.scm 802 */
															BGl_z62checkzd2bufferz12za2zz__intextz00
																(BgL_bufferz00_6300, BgL_ptrz00_6301, BINT(1L));
															{	/* Unsafe/intext.scm 719 */
																obj_t BgL_stringz00_4307;
																long BgL_kz00_4308;

																BgL_stringz00_4307 =
																	CELL_REF(BgL_bufferz00_6300);
																BgL_kz00_4308 =
																	(long) CINT(CELL_REF(BgL_ptrz00_6301));
																STRING_SET(BgL_stringz00_4307, BgL_kz00_4308,
																	((unsigned char) '-'));
															}
															{	/* Unsafe/intext.scm 720 */
																obj_t BgL_auxz00_6352;

																BgL_auxz00_6352 =
																	ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6301));
																CELL_SET(BgL_ptrz00_6301, BgL_auxz00_6352);
															}
															{	/* Unsafe/intext.scm 805 */
																long BgL_arg1714z00_4305;

																BgL_arg1714z00_4305 = NEG(BgL_iz00_4303);
																BGl_z62printzd2wordzb0zz__intextz00
																	(BgL_ptrz00_6301, BgL_bufferz00_6300,
																	BgL_arg1714z00_4305);
														}}
													else
														{	/* Unsafe/intext.scm 802 */
															BGl_z62printzd2wordzb0zz__intextz00
																(BgL_ptrz00_6301, BgL_bufferz00_6300,
																BgL_iz00_4303);
														}
												}
												{	/* Unsafe/intext.scm 826 */
													obj_t BgL_auxz00_6353;

													BgL_auxz00_6353 =
														ADDFX(BINT(1L), CELL_REF(BgL_refz00_6303));
													CELL_SET(BgL_refz00_6303, BgL_auxz00_6353);
												}
												return
													((obj_t(*)(obj_t, obj_t,
															obj_t))
													PROCEDURE_L_ENTRY(BgL_printerz00_2294))
													(BgL_printerz00_2294, BgL_itemz00_2293,
													BgL_markz00_2296);
											}
									}
							}
						}
					else
						{	/* Unsafe/intext.scm 1045 */
							if (PAIRP(BgL_itemz00_2565))
								{
									obj_t BgL_printerz00_8366;
									obj_t BgL_itemz00_8365;

									BgL_itemz00_8365 = BgL_itemz00_2565;
									BgL_printerz00_8366 = BgL_printzd2pairzd2_6312;
									BgL_printerz00_2294 = BgL_printerz00_8366;
									BgL_itemz00_2293 = BgL_itemz00_8365;
									goto BgL_zc3z04anonymousza31718ze3z87_2295;
								}
							else
								{	/* Unsafe/intext.scm 1047 */
									if (SYMBOLP(BgL_itemz00_2565))
										{	/* Unsafe/intext.scm 1049 */
											BGl_z62checkzd2bufferz12za2zz__intextz00
												(BgL_bufferz00_6300, BgL_ptrz00_6301, BINT(1L));
											{	/* Unsafe/intext.scm 719 */
												obj_t BgL_stringz00_5041;
												long BgL_kz00_5042;

												BgL_stringz00_5041 = CELL_REF(BgL_bufferz00_6300);
												BgL_kz00_5042 = (long) CINT(CELL_REF(BgL_ptrz00_6301));
												STRING_SET(BgL_stringz00_5041, BgL_kz00_5042,
													((unsigned char) '\''));
											}
											{	/* Unsafe/intext.scm 720 */
												obj_t BgL_auxz00_6314;

												BgL_auxz00_6314 =
													ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6301));
												CELL_SET(BgL_ptrz00_6301, BgL_auxz00_6314);
											}
											{	/* Unsafe/intext.scm 1051 */
												obj_t BgL_arg1883z00_2571;

												BgL_arg1883z00_2571 =
													SYMBOL_TO_STRING(BgL_itemz00_2565);
												{
													obj_t BgL_itemz00_8376;

													BgL_itemz00_8376 = BgL_arg1883z00_2571;
													BgL_itemz00_2565 = BgL_itemz00_8376;
													goto BGl_z62printzd2itemzb0zz__intextz00;
												}
											}
										}
									else
										{	/* Unsafe/intext.scm 1049 */
											if (KEYWORDP(BgL_itemz00_2565))
												{	/* Unsafe/intext.scm 1052 */
													BGl_z62checkzd2bufferz12za2zz__intextz00
														(BgL_bufferz00_6300, BgL_ptrz00_6301, BINT(1L));
													{	/* Unsafe/intext.scm 719 */
														obj_t BgL_stringz00_5046;
														long BgL_kz00_5047;

														BgL_stringz00_5046 = CELL_REF(BgL_bufferz00_6300);
														BgL_kz00_5047 =
															(long) CINT(CELL_REF(BgL_ptrz00_6301));
														STRING_SET(BgL_stringz00_5046, BgL_kz00_5047,
															((unsigned char) ':'));
													}
													{	/* Unsafe/intext.scm 720 */
														obj_t BgL_auxz00_6315;

														BgL_auxz00_6315 =
															ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6301));
														CELL_SET(BgL_ptrz00_6301, BgL_auxz00_6315);
													}
													{	/* Unsafe/intext.scm 1054 */
														obj_t BgL_arg1885z00_2573;

														{	/* Unsafe/intext.scm 1054 */
															obj_t BgL_arg2460z00_5051;

															BgL_arg2460z00_5051 =
																KEYWORD_TO_STRING(BgL_itemz00_2565);
															BgL_arg1885z00_2573 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg2460z00_5051);
														}
														{
															obj_t BgL_itemz00_8387;

															BgL_itemz00_8387 = BgL_arg1885z00_2573;
															BgL_itemz00_2565 = BgL_itemz00_8387;
															goto BGl_z62printzd2itemzb0zz__intextz00;
														}
													}
												}
											else
												{	/* Unsafe/intext.scm 1052 */
													if (STRINGP(BgL_itemz00_2565))
														{	/* Unsafe/intext.scm 1056 */
															obj_t BgL_zc3z04anonymousza31888ze3z87_6288;

															{
																int BgL_tmpz00_8390;

																BgL_tmpz00_8390 = (int) (2L);
																BgL_zc3z04anonymousza31888ze3z87_6288 =
																	MAKE_L_PROCEDURE
																	(BGl_z62zc3z04anonymousza31888ze3ze5zz__intextz00,
																	BgL_tmpz00_8390);
															}
															PROCEDURE_L_SET
																(BgL_zc3z04anonymousza31888ze3z87_6288,
																(int) (0L), ((obj_t) BgL_bufferz00_6300));
															PROCEDURE_L_SET
																(BgL_zc3z04anonymousza31888ze3z87_6288,
																(int) (1L), ((obj_t) BgL_ptrz00_6301));
															{
																obj_t BgL_printerz00_8400;
																obj_t BgL_itemz00_8399;

																BgL_itemz00_8399 = BgL_itemz00_2565;
																BgL_printerz00_8400 =
																	BgL_zc3z04anonymousza31888ze3z87_6288;
																BgL_printerz00_2294 = BgL_printerz00_8400;
																BgL_itemz00_2293 = BgL_itemz00_8399;
																goto BgL_zc3z04anonymousza31718ze3z87_2295;
															}
														}
													else
														{	/* Unsafe/intext.scm 1055 */
															if (BGL_OBJECTP(BgL_itemz00_2565))
																{
																	obj_t BgL_printerz00_8404;
																	obj_t BgL_itemz00_8403;

																	BgL_itemz00_8403 = BgL_itemz00_2565;
																	BgL_printerz00_8404 =
																		BgL_printzd2objectzd2_6311;
																	BgL_printerz00_2294 = BgL_printerz00_8404;
																	BgL_itemz00_2293 = BgL_itemz00_8403;
																	goto BgL_zc3z04anonymousza31718ze3z87_2295;
																}
															else
																{	/* Unsafe/intext.scm 1057 */
																	if (BGl_classzf3zf3zz__objectz00
																		(BgL_itemz00_2565))
																		{
																			obj_t BgL_printerz00_8408;
																			obj_t BgL_itemz00_8407;

																			BgL_itemz00_8407 = BgL_itemz00_2565;
																			BgL_printerz00_8408 =
																				BgL_printzd2classzd2_6310;
																			BgL_printerz00_2294 = BgL_printerz00_8408;
																			BgL_itemz00_2293 = BgL_itemz00_8407;
																			goto
																				BgL_zc3z04anonymousza31718ze3z87_2295;
																		}
																	else
																		{	/* Unsafe/intext.scm 1059 */
																			if (CHARP(BgL_itemz00_2565))
																				{	/* Unsafe/intext.scm 1061 */
																					BGl_z62checkzd2bufferz12za2zz__intextz00
																						(BgL_bufferz00_6300,
																						BgL_ptrz00_6301, BINT(1L));
																					{	/* Unsafe/intext.scm 719 */
																						obj_t BgL_stringz00_5058;
																						long BgL_kz00_5059;

																						BgL_stringz00_5058 =
																							CELL_REF(BgL_bufferz00_6300);
																						BgL_kz00_5059 =
																							(long)
																							CINT(CELL_REF(BgL_ptrz00_6301));
																						STRING_SET(BgL_stringz00_5058,
																							BgL_kz00_5059,
																							((unsigned char) 'a'));
																					}
																					{	/* Unsafe/intext.scm 720 */
																						obj_t BgL_auxz00_6316;

																						BgL_auxz00_6316 =
																							ADDFX(BINT(1L),
																							CELL_REF(BgL_ptrz00_6301));
																						CELL_SET(BgL_ptrz00_6301,
																							BgL_auxz00_6316);
																					}
																					{	/* Unsafe/intext.scm 1063 */
																						long BgL_arg1892z00_2583;

																						BgL_arg1892z00_2583 =
																							(CCHAR(BgL_itemz00_2565));
																						if ((BgL_arg1892z00_2583 < 0L))
																							{	/* Unsafe/intext.scm 802 */
																								BGl_z62checkzd2bufferz12za2zz__intextz00
																									(BgL_bufferz00_6300,
																									BgL_ptrz00_6301, BINT(1L));
																								{	/* Unsafe/intext.scm 719 */
																									obj_t BgL_stringz00_5067;
																									long BgL_kz00_5068;

																									BgL_stringz00_5067 =
																										CELL_REF
																										(BgL_bufferz00_6300);
																									BgL_kz00_5068 =
																										(long)
																										CINT(CELL_REF
																										(BgL_ptrz00_6301));
																									STRING_SET(BgL_stringz00_5067,
																										BgL_kz00_5068,
																										((unsigned char) '-'));
																								}
																								{	/* Unsafe/intext.scm 720 */
																									obj_t BgL_auxz00_6317;

																									BgL_auxz00_6317 =
																										ADDFX(BINT(1L),
																										CELL_REF(BgL_ptrz00_6301));
																									CELL_SET(BgL_ptrz00_6301,
																										BgL_auxz00_6317);
																								}
																								{	/* Unsafe/intext.scm 805 */
																									long BgL_arg1714z00_5065;

																									BgL_arg1714z00_5065 =
																										NEG(BgL_arg1892z00_2583);
																									return
																										BGl_z62printzd2wordzb0zz__intextz00
																										(BgL_ptrz00_6301,
																										BgL_bufferz00_6300,
																										BgL_arg1714z00_5065);
																								}
																							}
																						else
																							{	/* Unsafe/intext.scm 802 */
																								return
																									BGl_z62printzd2wordzb0zz__intextz00
																									(BgL_ptrz00_6301,
																									BgL_bufferz00_6300,
																									BgL_arg1892z00_2583);
																							}
																					}
																				}
																			else
																				{	/* Unsafe/intext.scm 1061 */
																					if (UCS2P(BgL_itemz00_2565))
																						{	/* Unsafe/intext.scm 1064 */
																							BGl_z62checkzd2bufferz12za2zz__intextz00
																								(BgL_bufferz00_6300,
																								BgL_ptrz00_6301, BINT(1L));
																							{	/* Unsafe/intext.scm 719 */
																								obj_t BgL_stringz00_5072;
																								long BgL_kz00_5073;

																								BgL_stringz00_5072 =
																									CELL_REF(BgL_bufferz00_6300);
																								BgL_kz00_5073 =
																									(long)
																									CINT(CELL_REF
																									(BgL_ptrz00_6301));
																								STRING_SET(BgL_stringz00_5072,
																									BgL_kz00_5073,
																									((unsigned char) 'u'));
																							}
																							{	/* Unsafe/intext.scm 720 */
																								obj_t BgL_auxz00_6318;

																								BgL_auxz00_6318 =
																									ADDFX(BINT(1L),
																									CELL_REF(BgL_ptrz00_6301));
																								CELL_SET(BgL_ptrz00_6301,
																									BgL_auxz00_6318);
																							}
																							{	/* Unsafe/intext.scm 1066 */
																								int BgL_arg1894z00_2585;

																								{	/* Unsafe/intext.scm 1066 */
																									ucs2_t BgL_ucs2z00_5076;

																									BgL_ucs2z00_5076 =
																										CUCS2(BgL_itemz00_2565);
																									{	/* Unsafe/intext.scm 1066 */
																										obj_t BgL_tmpz00_8439;

																										BgL_tmpz00_8439 =
																											BUCS2(BgL_ucs2z00_5076);
																										BgL_arg1894z00_2585 =
																											CUCS2(BgL_tmpz00_8439);
																								}}
																								{	/* Unsafe/intext.scm 1066 */
																									long BgL_iz00_5077;

																									BgL_iz00_5077 =
																										(long)
																										(BgL_arg1894z00_2585);
																									if ((BgL_iz00_5077 < 0L))
																										{	/* Unsafe/intext.scm 802 */
																											BGl_z62checkzd2bufferz12za2zz__intextz00
																												(BgL_bufferz00_6300,
																												BgL_ptrz00_6301,
																												BINT(1L));
																											{	/* Unsafe/intext.scm 719 */
																												obj_t
																													BgL_stringz00_5081;
																												long BgL_kz00_5082;

																												BgL_stringz00_5081 =
																													CELL_REF
																													(BgL_bufferz00_6300);
																												BgL_kz00_5082 =
																													(long)
																													CINT(CELL_REF
																													(BgL_ptrz00_6301));
																												STRING_SET
																													(BgL_stringz00_5081,
																													BgL_kz00_5082,
																													((unsigned char)
																														'-'));
																											}
																											{	/* Unsafe/intext.scm 720 */
																												obj_t BgL_auxz00_6319;

																												BgL_auxz00_6319 =
																													ADDFX(BINT(1L),
																													CELL_REF
																													(BgL_ptrz00_6301));
																												CELL_SET
																													(BgL_ptrz00_6301,
																													BgL_auxz00_6319);
																											}
																											{	/* Unsafe/intext.scm 805 */
																												long
																													BgL_arg1714z00_5079;
																												BgL_arg1714z00_5079 =
																													NEG(BgL_iz00_5077);
																												return
																													BGl_z62printzd2wordzb0zz__intextz00
																													(BgL_ptrz00_6301,
																													BgL_bufferz00_6300,
																													BgL_arg1714z00_5079);
																											}
																										}
																									else
																										{	/* Unsafe/intext.scm 802 */
																											return
																												BGl_z62printzd2wordzb0zz__intextz00
																												(BgL_ptrz00_6301,
																												BgL_bufferz00_6300,
																												BgL_iz00_5077);
																										}
																								}
																							}
																						}
																					else
																						{	/* Unsafe/intext.scm 1064 */
																							if ((BgL_itemz00_2565 == BUNSPEC))
																								{	/* Unsafe/intext.scm 1067 */
																									BGl_z62checkzd2bufferz12za2zz__intextz00
																										(BgL_bufferz00_6300,
																										BgL_ptrz00_6301, BINT(1L));
																									{	/* Unsafe/intext.scm 719 */
																										obj_t BgL_stringz00_5086;
																										long BgL_kz00_5087;

																										BgL_stringz00_5086 =
																											CELL_REF
																											(BgL_bufferz00_6300);
																										BgL_kz00_5087 =
																											(long)
																											CINT(CELL_REF
																											(BgL_ptrz00_6301));
																										STRING_SET
																											(BgL_stringz00_5086,
																											BgL_kz00_5087,
																											((unsigned char) ';'));
																									}
																									{	/* Unsafe/intext.scm 720 */
																										obj_t BgL_auxz00_6320;

																										BgL_auxz00_6320 =
																											ADDFX(BINT(1L),
																											CELL_REF
																											(BgL_ptrz00_6301));
																										return
																											CELL_SET(BgL_ptrz00_6301,
																											BgL_auxz00_6320);
																									}
																								}
																							else
																								{	/* Unsafe/intext.scm 1067 */
																									if (
																										(BgL_itemz00_2565 == BNIL))
																										{	/* Unsafe/intext.scm 1069 */
																											BGl_z62checkzd2bufferz12za2zz__intextz00
																												(BgL_bufferz00_6300,
																												BgL_ptrz00_6301,
																												BINT(1L));
																											{	/* Unsafe/intext.scm 719 */
																												obj_t
																													BgL_stringz00_5090;
																												long BgL_kz00_5091;

																												BgL_stringz00_5090 =
																													CELL_REF
																													(BgL_bufferz00_6300);
																												BgL_kz00_5091 =
																													(long)
																													CINT(CELL_REF
																													(BgL_ptrz00_6301));
																												STRING_SET
																													(BgL_stringz00_5090,
																													BgL_kz00_5091,
																													((unsigned char)
																														'.'));
																											}
																											{	/* Unsafe/intext.scm 720 */
																												obj_t BgL_auxz00_6321;

																												BgL_auxz00_6321 =
																													ADDFX(BINT(1L),
																													CELL_REF
																													(BgL_ptrz00_6301));
																												return
																													CELL_SET
																													(BgL_ptrz00_6301,
																													BgL_auxz00_6321);
																											}
																										}
																									else
																										{	/* Unsafe/intext.scm 1069 */
																											if (
																												(BgL_itemz00_2565 ==
																													BTRUE))
																												{	/* Unsafe/intext.scm 1071 */
																													BGl_z62checkzd2bufferz12za2zz__intextz00
																														(BgL_bufferz00_6300,
																														BgL_ptrz00_6301,
																														BINT(1L));
																													{	/* Unsafe/intext.scm 719 */
																														obj_t
																															BgL_stringz00_5094;
																														long BgL_kz00_5095;

																														BgL_stringz00_5094 =
																															CELL_REF
																															(BgL_bufferz00_6300);
																														BgL_kz00_5095 =
																															(long)
																															CINT(CELL_REF
																															(BgL_ptrz00_6301));
																														STRING_SET
																															(BgL_stringz00_5094,
																															BgL_kz00_5095,
																															((unsigned char)
																																'T'));
																													}
																													{	/* Unsafe/intext.scm 720 */
																														obj_t
																															BgL_auxz00_6322;
																														BgL_auxz00_6322 =
																															ADDFX(BINT(1L),
																															CELL_REF
																															(BgL_ptrz00_6301));
																														return
																															CELL_SET
																															(BgL_ptrz00_6301,
																															BgL_auxz00_6322);
																													}
																												}
																											else
																												{	/* Unsafe/intext.scm 1071 */
																													if (
																														(BgL_itemz00_2565 ==
																															BFALSE))
																														{	/* Unsafe/intext.scm 1073 */
																															BGl_z62checkzd2bufferz12za2zz__intextz00
																																(BgL_bufferz00_6300,
																																BgL_ptrz00_6301,
																																BINT(1L));
																															{	/* Unsafe/intext.scm 719 */
																																obj_t
																																	BgL_stringz00_5098;
																																long
																																	BgL_kz00_5099;
																																BgL_stringz00_5098
																																	=
																																	CELL_REF
																																	(BgL_bufferz00_6300);
																																BgL_kz00_5099 =
																																	(long)
																																	CINT(CELL_REF
																																	(BgL_ptrz00_6301));
																																STRING_SET
																																	(BgL_stringz00_5098,
																																	BgL_kz00_5099,
																																	((unsigned
																																			char)
																																		'F'));
																															}
																															{	/* Unsafe/intext.scm 720 */
																																obj_t
																																	BgL_auxz00_6323;
																																BgL_auxz00_6323
																																	=
																																	ADDFX(BINT
																																	(1L),
																																	CELL_REF
																																	(BgL_ptrz00_6301));
																																return
																																	CELL_SET
																																	(BgL_ptrz00_6301,
																																	BgL_auxz00_6323);
																															}
																														}
																													else
																														{	/* Unsafe/intext.scm 1073 */
																															if (BGL_INT8P
																																(BgL_itemz00_2565))
																																{	/* Unsafe/intext.scm 1075 */
																																	BGl_z62checkzd2bufferz12za2zz__intextz00
																																		(BgL_bufferz00_6300,
																																		BgL_ptrz00_6301,
																																		BINT(1L));
																																	{	/* Unsafe/intext.scm 719 */
																																		obj_t
																																			BgL_stringz00_5102;
																																		long
																																			BgL_kz00_5103;
																																		BgL_stringz00_5102
																																			=
																																			CELL_REF
																																			(BgL_bufferz00_6300);
																																		BgL_kz00_5103
																																			=
																																			(long)
																																			CINT
																																			(CELL_REF
																																			(BgL_ptrz00_6301));
																																		STRING_SET
																																			(BgL_stringz00_5102,
																																			BgL_kz00_5103,
																																			((unsigned
																																					char)
																																				'b'));
																																	}
																																	{	/* Unsafe/intext.scm 720 */
																																		obj_t
																																			BgL_auxz00_6324;
																																		BgL_auxz00_6324
																																			=
																																			ADDFX(BINT
																																			(1L),
																																			CELL_REF
																																			(BgL_ptrz00_6301));
																																		CELL_SET
																																			(BgL_ptrz00_6301,
																																			BgL_auxz00_6324);
																																	}
																																	{	/* Unsafe/intext.scm 1077 */
																																		long
																																			BgL_arg1896z00_2587;
																																		{	/* Unsafe/intext.scm 1077 */
																																			int8_t
																																				BgL_xz00_5106;
																																			BgL_xz00_5106
																																				=
																																				BGL_BINT8_TO_INT8
																																				(BgL_itemz00_2565);
																																			{	/* Unsafe/intext.scm 1077 */
																																				long
																																					BgL_arg2367z00_5107;
																																				BgL_arg2367z00_5107
																																					=
																																					(long)
																																					(BgL_xz00_5106);
																																				BgL_arg1896z00_2587
																																					=
																																					(long)
																																					(BgL_arg2367z00_5107);
																																		}}
																																		{	/* Unsafe/intext.scm 752 */
																																			long
																																				BgL_g1065z00_5109;
																																			BgL_g1065z00_5109
																																				=
																																				(1L -
																																				1L);
																																			{
																																				long
																																					BgL_iz00_5112;
																																				{	/* Unsafe/intext.scm 752 */
																																					bool_t
																																						BgL_tmpz00_8498;
																																					BgL_iz00_5112
																																						=
																																						BgL_g1065z00_5109;
																																				BgL_loopz00_5111:
																																					if (
																																						(BgL_iz00_5112
																																							>=
																																							0L))
																																						{	/* Unsafe/intext.scm 754 */
																																							long
																																								BgL_dz00_5115;
																																							BgL_dz00_5115
																																								=
																																								(
																																								(BgL_arg1896z00_2587
																																									>>
																																									(int)
																																									((8L * BgL_iz00_5112))) & 255L);
																																							{	/* Unsafe/intext.scm 748 */
																																								unsigned
																																									char
																																									BgL_arg1656z00_5122;
																																								BgL_arg1656z00_5122
																																									=
																																									(BgL_dz00_5115);
																																								BGl_z62z12printzd2markupza2zz__intextz00
																																									(BgL_ptrz00_6301,
																																									BgL_bufferz00_6300,
																																									BgL_arg1656z00_5122);
																																							}
																																							{
																																								long
																																									BgL_iz00_8507;
																																								BgL_iz00_8507
																																									=
																																									(BgL_iz00_5112
																																									-
																																									1L);
																																								BgL_iz00_5112
																																									=
																																									BgL_iz00_8507;
																																								goto
																																									BgL_loopz00_5111;
																																							}
																																						}
																																					else
																																						{	/* Unsafe/intext.scm 753 */
																																							BgL_tmpz00_8498
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																					return
																																						BBOOL
																																						(BgL_tmpz00_8498);
																																				}
																																			}
																																		}
																																	}
																																}
																															else
																																{	/* Unsafe/intext.scm 1075 */
																																	if (BGL_UINT8P
																																		(BgL_itemz00_2565))
																																		{	/* Unsafe/intext.scm 1078 */
																																			BGl_z62checkzd2bufferz12za2zz__intextz00
																																				(BgL_bufferz00_6300,
																																				BgL_ptrz00_6301,
																																				BINT
																																				(1L));
																																			{	/* Unsafe/intext.scm 719 */
																																				obj_t
																																					BgL_stringz00_5126;
																																				long
																																					BgL_kz00_5127;
																																				BgL_stringz00_5126
																																					=
																																					CELL_REF
																																					(BgL_bufferz00_6300);
																																				BgL_kz00_5127
																																					=
																																					(long)
																																					CINT
																																					(CELL_REF
																																					(BgL_ptrz00_6301));
																																				STRING_SET
																																					(BgL_stringz00_5126,
																																					BgL_kz00_5127,
																																					((unsigned char) 'B'));
																																			}
																																			{	/* Unsafe/intext.scm 720 */
																																				obj_t
																																					BgL_auxz00_6325;
																																				BgL_auxz00_6325
																																					=
																																					ADDFX
																																					(BINT
																																					(1L),
																																					CELL_REF
																																					(BgL_ptrz00_6301));
																																				CELL_SET
																																					(BgL_ptrz00_6301,
																																					BgL_auxz00_6325);
																																			}
																																			{	/* Unsafe/intext.scm 1080 */
																																				long
																																					BgL_arg1898z00_2589;
																																				{	/* Unsafe/intext.scm 1080 */
																																					uint8_t
																																						BgL_xz00_5130;
																																					BgL_xz00_5130
																																						=
																																						BGL_BUINT8_TO_UINT8
																																						(BgL_itemz00_2565);
																																					{	/* Unsafe/intext.scm 1080 */
																																						long
																																							BgL_arg2366z00_5131;
																																						BgL_arg2366z00_5131
																																							=
																																							(long)
																																							(BgL_xz00_5130);
																																						BgL_arg1898z00_2589
																																							=
																																							(long)
																																							(BgL_arg2366z00_5131);
																																				}}
																																				{	/* Unsafe/intext.scm 752 */
																																					long
																																						BgL_g1065z00_5133;
																																					BgL_g1065z00_5133
																																						=
																																						(1L
																																						-
																																						1L);
																																					{
																																						long
																																							BgL_iz00_5136;
																																						{	/* Unsafe/intext.scm 752 */
																																							bool_t
																																								BgL_tmpz00_8522;
																																							BgL_iz00_5136
																																								=
																																								BgL_g1065z00_5133;
																																						BgL_loopz00_5135:
																																							if ((BgL_iz00_5136 >= 0L))
																																								{	/* Unsafe/intext.scm 754 */
																																									long
																																										BgL_dz00_5139;
																																									BgL_dz00_5139
																																										=
																																										(
																																										(BgL_arg1898z00_2589
																																											>>
																																											(int)
																																											((8L * BgL_iz00_5136))) & 255L);
																																									{	/* Unsafe/intext.scm 748 */
																																										unsigned
																																											char
																																											BgL_arg1656z00_5146;
																																										BgL_arg1656z00_5146
																																											=
																																											(BgL_dz00_5139);
																																										BGl_z62z12printzd2markupza2zz__intextz00
																																											(BgL_ptrz00_6301,
																																											BgL_bufferz00_6300,
																																											BgL_arg1656z00_5146);
																																									}
																																									{
																																										long
																																											BgL_iz00_8531;
																																										BgL_iz00_8531
																																											=
																																											(BgL_iz00_5136
																																											-
																																											1L);
																																										BgL_iz00_5136
																																											=
																																											BgL_iz00_8531;
																																										goto
																																											BgL_loopz00_5135;
																																									}
																																								}
																																							else
																																								{	/* Unsafe/intext.scm 753 */
																																									BgL_tmpz00_8522
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																							return
																																								BBOOL
																																								(BgL_tmpz00_8522);
																																						}
																																					}
																																				}
																																			}
																																		}
																																	else
																																		{	/* Unsafe/intext.scm 1078 */
																																			if (BGL_INT16P(BgL_itemz00_2565))
																																				{	/* Unsafe/intext.scm 1081 */
																																					BGl_z62checkzd2bufferz12za2zz__intextz00
																																						(BgL_bufferz00_6300,
																																						BgL_ptrz00_6301,
																																						BINT
																																						(1L));
																																					{	/* Unsafe/intext.scm 719 */
																																						obj_t
																																							BgL_stringz00_5150;
																																						long
																																							BgL_kz00_5151;
																																						BgL_stringz00_5150
																																							=
																																							CELL_REF
																																							(BgL_bufferz00_6300);
																																						BgL_kz00_5151
																																							=
																																							(long)
																																							CINT
																																							(CELL_REF
																																							(BgL_ptrz00_6301));
																																						STRING_SET
																																							(BgL_stringz00_5150,
																																							BgL_kz00_5151,
																																							((unsigned char) 's'));
																																					}
																																					{	/* Unsafe/intext.scm 720 */
																																						obj_t
																																							BgL_auxz00_6326;
																																						BgL_auxz00_6326
																																							=
																																							ADDFX
																																							(BINT
																																							(1L),
																																							CELL_REF
																																							(BgL_ptrz00_6301));
																																						CELL_SET
																																							(BgL_ptrz00_6301,
																																							BgL_auxz00_6326);
																																					}
																																					{	/* Unsafe/intext.scm 1083 */
																																						long
																																							BgL_arg1901z00_2591;
																																						{	/* Unsafe/intext.scm 1083 */
																																							int16_t
																																								BgL_xz00_5154;
																																							BgL_xz00_5154
																																								=
																																								BGL_BINT16_TO_INT16
																																								(BgL_itemz00_2565);
																																							{	/* Unsafe/intext.scm 1083 */
																																								long
																																									BgL_arg2365z00_5155;
																																								BgL_arg2365z00_5155
																																									=
																																									(long)
																																									(BgL_xz00_5154);
																																								BgL_arg1901z00_2591
																																									=
																																									(long)
																																									(BgL_arg2365z00_5155);
																																						}}
																																						{	/* Unsafe/intext.scm 752 */
																																							long
																																								BgL_g1065z00_5157;
																																							BgL_g1065z00_5157
																																								=
																																								(2L
																																								-
																																								1L);
																																							{
																																								long
																																									BgL_iz00_5160;
																																								{	/* Unsafe/intext.scm 752 */
																																									bool_t
																																										BgL_tmpz00_8546;
																																									BgL_iz00_5160
																																										=
																																										BgL_g1065z00_5157;
																																								BgL_loopz00_5159:
																																									if ((BgL_iz00_5160 >= 0L))
																																										{	/* Unsafe/intext.scm 754 */
																																											long
																																												BgL_dz00_5163;
																																											BgL_dz00_5163
																																												=
																																												(
																																												(BgL_arg1901z00_2591
																																													>>
																																													(int)
																																													((8L * BgL_iz00_5160))) & 255L);
																																											{	/* Unsafe/intext.scm 748 */
																																												unsigned
																																													char
																																													BgL_arg1656z00_5170;
																																												BgL_arg1656z00_5170
																																													=
																																													(BgL_dz00_5163);
																																												BGl_z62z12printzd2markupza2zz__intextz00
																																													(BgL_ptrz00_6301,
																																													BgL_bufferz00_6300,
																																													BgL_arg1656z00_5170);
																																											}
																																											{
																																												long
																																													BgL_iz00_8555;
																																												BgL_iz00_8555
																																													=
																																													(BgL_iz00_5160
																																													-
																																													1L);
																																												BgL_iz00_5160
																																													=
																																													BgL_iz00_8555;
																																												goto
																																													BgL_loopz00_5159;
																																											}
																																										}
																																									else
																																										{	/* Unsafe/intext.scm 753 */
																																											BgL_tmpz00_8546
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																									return
																																										BBOOL
																																										(BgL_tmpz00_8546);
																																								}
																																							}
																																						}
																																					}
																																				}
																																			else
																																				{	/* Unsafe/intext.scm 1081 */
																																					if (BGL_UINT16P(BgL_itemz00_2565))
																																						{	/* Unsafe/intext.scm 1084 */
																																							BGl_z62checkzd2bufferz12za2zz__intextz00
																																								(BgL_bufferz00_6300,
																																								BgL_ptrz00_6301,
																																								BINT
																																								(1L));
																																							{	/* Unsafe/intext.scm 719 */
																																								obj_t
																																									BgL_stringz00_5174;
																																								long
																																									BgL_kz00_5175;
																																								BgL_stringz00_5174
																																									=
																																									CELL_REF
																																									(BgL_bufferz00_6300);
																																								BgL_kz00_5175
																																									=
																																									(long)
																																									CINT
																																									(CELL_REF
																																									(BgL_ptrz00_6301));
																																								STRING_SET
																																									(BgL_stringz00_5174,
																																									BgL_kz00_5175,
																																									((unsigned char) 'S'));
																																							}
																																							{	/* Unsafe/intext.scm 720 */
																																								obj_t
																																									BgL_auxz00_6327;
																																								BgL_auxz00_6327
																																									=
																																									ADDFX
																																									(BINT
																																									(1L),
																																									CELL_REF
																																									(BgL_ptrz00_6301));
																																								CELL_SET
																																									(BgL_ptrz00_6301,
																																									BgL_auxz00_6327);
																																							}
																																							{	/* Unsafe/intext.scm 1086 */
																																								long
																																									BgL_arg1903z00_2593;
																																								{	/* Unsafe/intext.scm 1086 */
																																									uint16_t
																																										BgL_xz00_5178;
																																									BgL_xz00_5178
																																										=
																																										BGL_BUINT16_TO_UINT16
																																										(BgL_itemz00_2565);
																																									{	/* Unsafe/intext.scm 1086 */
																																										long
																																											BgL_arg2364z00_5179;
																																										BgL_arg2364z00_5179
																																											=
																																											(long)
																																											(BgL_xz00_5178);
																																										BgL_arg1903z00_2593
																																											=
																																											(long)
																																											(BgL_arg2364z00_5179);
																																								}}
																																								{	/* Unsafe/intext.scm 752 */
																																									long
																																										BgL_g1065z00_5181;
																																									BgL_g1065z00_5181
																																										=
																																										(2L
																																										-
																																										1L);
																																									{
																																										long
																																											BgL_iz00_5184;
																																										{	/* Unsafe/intext.scm 752 */
																																											bool_t
																																												BgL_tmpz00_8570;
																																											BgL_iz00_5184
																																												=
																																												BgL_g1065z00_5181;
																																										BgL_loopz00_5183:
																																											if ((BgL_iz00_5184 >= 0L))
																																												{	/* Unsafe/intext.scm 754 */
																																													long
																																														BgL_dz00_5187;
																																													BgL_dz00_5187
																																														=
																																														(
																																														(BgL_arg1903z00_2593
																																															>>
																																															(int)
																																															((8L * BgL_iz00_5184))) & 255L);
																																													{	/* Unsafe/intext.scm 748 */
																																														unsigned
																																															char
																																															BgL_arg1656z00_5194;
																																														BgL_arg1656z00_5194
																																															=
																																															(BgL_dz00_5187);
																																														BGl_z62z12printzd2markupza2zz__intextz00
																																															(BgL_ptrz00_6301,
																																															BgL_bufferz00_6300,
																																															BgL_arg1656z00_5194);
																																													}
																																													{
																																														long
																																															BgL_iz00_8579;
																																														BgL_iz00_8579
																																															=
																																															(BgL_iz00_5184
																																															-
																																															1L);
																																														BgL_iz00_5184
																																															=
																																															BgL_iz00_8579;
																																														goto
																																															BgL_loopz00_5183;
																																													}
																																												}
																																											else
																																												{	/* Unsafe/intext.scm 753 */
																																													BgL_tmpz00_8570
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																											return
																																												BBOOL
																																												(BgL_tmpz00_8570);
																																										}
																																									}
																																								}
																																							}
																																						}
																																					else
																																						{	/* Unsafe/intext.scm 1084 */
																																							if (BGL_INT32P(BgL_itemz00_2565))
																																								{	/* Unsafe/intext.scm 1087 */
																																									BGl_z62checkzd2bufferz12za2zz__intextz00
																																										(BgL_bufferz00_6300,
																																										BgL_ptrz00_6301,
																																										BINT
																																										(1L));
																																									{	/* Unsafe/intext.scm 719 */
																																										obj_t
																																											BgL_stringz00_5198;
																																										long
																																											BgL_kz00_5199;
																																										BgL_stringz00_5198
																																											=
																																											CELL_REF
																																											(BgL_bufferz00_6300);
																																										BgL_kz00_5199
																																											=
																																											(long)
																																											CINT
																																											(CELL_REF
																																											(BgL_ptrz00_6301));
																																										STRING_SET
																																											(BgL_stringz00_5198,
																																											BgL_kz00_5199,
																																											((unsigned char) 'i'));
																																									}
																																									{	/* Unsafe/intext.scm 720 */
																																										obj_t
																																											BgL_auxz00_6328;
																																										BgL_auxz00_6328
																																											=
																																											ADDFX
																																											(BINT
																																											(1L),
																																											CELL_REF
																																											(BgL_ptrz00_6301));
																																										CELL_SET
																																											(BgL_ptrz00_6301,
																																											BgL_auxz00_6328);
																																									}
																																									{	/* Unsafe/intext.scm 1089 */
																																										long
																																											BgL_arg1906z00_2595;
																																										{	/* Unsafe/intext.scm 1089 */
																																											int32_t
																																												BgL_xz00_5202;
																																											BgL_xz00_5202
																																												=
																																												BGL_BINT32_TO_INT32
																																												(BgL_itemz00_2565);
																																											{	/* Unsafe/intext.scm 1089 */
																																												long
																																													BgL_arg2363z00_5203;
																																												BgL_arg2363z00_5203
																																													=
																																													(long)
																																													(BgL_xz00_5202);
																																												BgL_arg1906z00_2595
																																													=
																																													(long)
																																													(BgL_arg2363z00_5203);
																																										}}
																																										{	/* Unsafe/intext.scm 752 */
																																											long
																																												BgL_g1065z00_5205;
																																											BgL_g1065z00_5205
																																												=
																																												(4L
																																												-
																																												1L);
																																											{
																																												long
																																													BgL_iz00_5208;
																																												{	/* Unsafe/intext.scm 752 */
																																													bool_t
																																														BgL_tmpz00_8594;
																																													BgL_iz00_5208
																																														=
																																														BgL_g1065z00_5205;
																																												BgL_loopz00_5207:
																																													if ((BgL_iz00_5208 >= 0L))
																																														{	/* Unsafe/intext.scm 754 */
																																															long
																																																BgL_dz00_5211;
																																															BgL_dz00_5211
																																																=
																																																(
																																																(BgL_arg1906z00_2595
																																																	>>
																																																	(int)
																																																	((8L * BgL_iz00_5208))) & 255L);
																																															{	/* Unsafe/intext.scm 748 */
																																																unsigned
																																																	char
																																																	BgL_arg1656z00_5218;
																																																BgL_arg1656z00_5218
																																																	=
																																																	(BgL_dz00_5211);
																																																BGl_z62z12printzd2markupza2zz__intextz00
																																																	(BgL_ptrz00_6301,
																																																	BgL_bufferz00_6300,
																																																	BgL_arg1656z00_5218);
																																															}
																																															{
																																																long
																																																	BgL_iz00_8603;
																																																BgL_iz00_8603
																																																	=
																																																	(BgL_iz00_5208
																																																	-
																																																	1L);
																																																BgL_iz00_5208
																																																	=
																																																	BgL_iz00_8603;
																																																goto
																																																	BgL_loopz00_5207;
																																															}
																																														}
																																													else
																																														{	/* Unsafe/intext.scm 753 */
																																															BgL_tmpz00_8594
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																													return
																																														BBOOL
																																														(BgL_tmpz00_8594);
																																												}
																																											}
																																										}
																																									}
																																								}
																																							else
																																								{	/* Unsafe/intext.scm 1087 */
																																									if (BGL_UINT32P(BgL_itemz00_2565))
																																										{	/* Unsafe/intext.scm 1090 */
																																											BGl_z62checkzd2bufferz12za2zz__intextz00
																																												(BgL_bufferz00_6300,
																																												BgL_ptrz00_6301,
																																												BINT
																																												(1L));
																																											{	/* Unsafe/intext.scm 719 */
																																												obj_t
																																													BgL_stringz00_5222;
																																												long
																																													BgL_kz00_5223;
																																												BgL_stringz00_5222
																																													=
																																													CELL_REF
																																													(BgL_bufferz00_6300);
																																												BgL_kz00_5223
																																													=
																																													(long)
																																													CINT
																																													(CELL_REF
																																													(BgL_ptrz00_6301));
																																												STRING_SET
																																													(BgL_stringz00_5222,
																																													BgL_kz00_5223,
																																													((unsigned char) 'I'));
																																											}
																																											{	/* Unsafe/intext.scm 720 */
																																												obj_t
																																													BgL_auxz00_6329;
																																												BgL_auxz00_6329
																																													=
																																													ADDFX
																																													(BINT
																																													(1L),
																																													CELL_REF
																																													(BgL_ptrz00_6301));
																																												CELL_SET
																																													(BgL_ptrz00_6301,
																																													BgL_auxz00_6329);
																																											}
																																											{	/* Unsafe/intext.scm 1092 */
																																												long
																																													BgL_arg1910z00_2597;
																																												{	/* Unsafe/intext.scm 1092 */
																																													uint32_t
																																														BgL_xz00_5226;
																																													BgL_xz00_5226
																																														=
																																														BGL_BUINT32_TO_UINT32
																																														(BgL_itemz00_2565);
																																													BgL_arg1910z00_2597
																																														=
																																														(long)
																																														(BgL_xz00_5226);
																																												}
																																												{	/* Unsafe/intext.scm 752 */
																																													long
																																														BgL_g1065z00_5227;
																																													BgL_g1065z00_5227
																																														=
																																														(4L
																																														-
																																														1L);
																																													{
																																														long
																																															BgL_iz00_5230;
																																														{	/* Unsafe/intext.scm 752 */
																																															bool_t
																																																BgL_tmpz00_8617;
																																															BgL_iz00_5230
																																																=
																																																BgL_g1065z00_5227;
																																														BgL_loopz00_5229:
																																															if ((BgL_iz00_5230 >= 0L))
																																																{	/* Unsafe/intext.scm 754 */
																																																	long
																																																		BgL_dz00_5233;
																																																	BgL_dz00_5233
																																																		=
																																																		(
																																																		(BgL_arg1910z00_2597
																																																			>>
																																																			(int)
																																																			((8L * BgL_iz00_5230))) & 255L);
																																																	{	/* Unsafe/intext.scm 748 */
																																																		unsigned
																																																			char
																																																			BgL_arg1656z00_5240;
																																																		BgL_arg1656z00_5240
																																																			=
																																																			(BgL_dz00_5233);
																																																		BGl_z62z12printzd2markupza2zz__intextz00
																																																			(BgL_ptrz00_6301,
																																																			BgL_bufferz00_6300,
																																																			BgL_arg1656z00_5240);
																																																	}
																																																	{
																																																		long
																																																			BgL_iz00_8626;
																																																		BgL_iz00_8626
																																																			=
																																																			(BgL_iz00_5230
																																																			-
																																																			1L);
																																																		BgL_iz00_5230
																																																			=
																																																			BgL_iz00_8626;
																																																		goto
																																																			BgL_loopz00_5229;
																																																	}
																																																}
																																															else
																																																{	/* Unsafe/intext.scm 753 */
																																																	BgL_tmpz00_8617
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																															return
																																																BBOOL
																																																(BgL_tmpz00_8617);
																																														}
																																													}
																																												}
																																											}
																																										}
																																									else
																																										{	/* Unsafe/intext.scm 1090 */
																																											if (BGL_INT64P(BgL_itemz00_2565))
																																												{	/* Unsafe/intext.scm 1093 */
																																													BGl_z62checkzd2bufferz12za2zz__intextz00
																																														(BgL_bufferz00_6300,
																																														BgL_ptrz00_6301,
																																														BINT
																																														(1L));
																																													{	/* Unsafe/intext.scm 719 */
																																														obj_t
																																															BgL_stringz00_5244;
																																														long
																																															BgL_kz00_5245;
																																														BgL_stringz00_5244
																																															=
																																															CELL_REF
																																															(BgL_bufferz00_6300);
																																														BgL_kz00_5245
																																															=
																																															(long)
																																															CINT
																																															(CELL_REF
																																															(BgL_ptrz00_6301));
																																														STRING_SET
																																															(BgL_stringz00_5244,
																																															BgL_kz00_5245,
																																															((unsigned char) 'l'));
																																													}
																																													{	/* Unsafe/intext.scm 720 */
																																														obj_t
																																															BgL_auxz00_6330;
																																														BgL_auxz00_6330
																																															=
																																															ADDFX
																																															(BINT
																																															(1L),
																																															CELL_REF
																																															(BgL_ptrz00_6301));
																																														CELL_SET
																																															(BgL_ptrz00_6301,
																																															BgL_auxz00_6330);
																																													}
																																													{	/* Unsafe/intext.scm 1095 */
																																														bool_t
																																															BgL_tmpz00_8637;
																																														BgL_mz00_2227
																																															=
																																															BgL_itemz00_2565;
																																														{
																																															long
																																																BgL_iz00_2231;
																																															BgL_iz00_2231
																																																=
																																																7L;
																																														BgL_zc3z04anonymousza31666ze3z87_2232:
																																															if ((BgL_iz00_2231 >= 0L))
																																																{	/* Unsafe/intext.scm 762 */
																																																	long
																																																		BgL_dz00_2234;
																																																	{	/* Unsafe/intext.scm 762 */
																																																		int64_t
																																																			BgL_arg1669z00_2236;
																																																		{	/* Unsafe/intext.scm 762 */
																																																			int64_t
																																																				BgL_tmpz00_8640;
																																																			{	/* Unsafe/intext.scm 762 */
																																																				int64_t
																																																					BgL_xz00_4192;
																																																				BgL_xz00_4192
																																																					=
																																																					BGL_BINT64_TO_INT64
																																																					(BgL_mz00_2227);
																																																				BgL_tmpz00_8640
																																																					=
																																																					(BgL_xz00_4192
																																																					>>
																																																					(int)
																																																					((8L * BgL_iz00_2231)));
																																																			}
																																																			BgL_arg1669z00_2236
																																																				=
																																																				(BgL_tmpz00_8640
																																																				&
																																																				(int64_t)
																																																				(255L));
																																																		}
																																																		{	/* Unsafe/intext.scm 762 */
																																																			long
																																																				BgL_arg2361z00_4197;
																																																			BgL_arg2361z00_4197
																																																				=
																																																				(long)
																																																				(BgL_arg1669z00_2236);
																																																			BgL_dz00_2234
																																																				=
																																																				(long)
																																																				(BgL_arg2361z00_4197);
																																																	}}
																																																	{	/* Unsafe/intext.scm 748 */
																																																		unsigned
																																																			char
																																																			BgL_arg1656z00_4199;
																																																		BgL_arg1656z00_4199
																																																			=
																																																			(BgL_dz00_2234);
																																																		BGl_z62z12printzd2markupza2zz__intextz00
																																																			(BgL_ptrz00_6301,
																																																			BgL_bufferz00_6300,
																																																			BgL_arg1656z00_4199);
																																																	}
																																																	{
																																																		long
																																																			BgL_iz00_8651;
																																																		BgL_iz00_8651
																																																			=
																																																			(BgL_iz00_2231
																																																			-
																																																			1L);
																																																		BgL_iz00_2231
																																																			=
																																																			BgL_iz00_8651;
																																																		goto
																																																			BgL_zc3z04anonymousza31666ze3z87_2232;
																																																	}
																																																}
																																															else
																																																{	/* Unsafe/intext.scm 761 */
																																																	BgL_tmpz00_8637
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																														return
																																															BBOOL
																																															(BgL_tmpz00_8637);
																																													}
																																												}
																																											else
																																												{	/* Unsafe/intext.scm 1093 */
																																													if (BGL_UINT64P(BgL_itemz00_2565))
																																														{	/* Unsafe/intext.scm 1096 */
																																															BGl_z62checkzd2bufferz12za2zz__intextz00
																																																(BgL_bufferz00_6300,
																																																BgL_ptrz00_6301,
																																																BINT
																																																(1L));
																																															{	/* Unsafe/intext.scm 719 */
																																																obj_t
																																																	BgL_stringz00_5248;
																																																long
																																																	BgL_kz00_5249;
																																																BgL_stringz00_5248
																																																	=
																																																	CELL_REF
																																																	(BgL_bufferz00_6300);
																																																BgL_kz00_5249
																																																	=
																																																	(long)
																																																	CINT
																																																	(CELL_REF
																																																	(BgL_ptrz00_6301));
																																																STRING_SET
																																																	(BgL_stringz00_5248,
																																																	BgL_kz00_5249,
																																																	((unsigned char) 'W'));
																																															}
																																															{	/* Unsafe/intext.scm 720 */
																																																obj_t
																																																	BgL_auxz00_6331;
																																																BgL_auxz00_6331
																																																	=
																																																	ADDFX
																																																	(BINT
																																																	(1L),
																																																	CELL_REF
																																																	(BgL_ptrz00_6301));
																																																CELL_SET
																																																	(BgL_ptrz00_6301,
																																																	BgL_auxz00_6331);
																																															}
																																															{	/* Unsafe/intext.scm 1098 */
																																																bool_t
																																																	BgL_tmpz00_8662;
																																																BgL_mz00_2240
																																																	=
																																																	BgL_itemz00_2565;
																																																{
																																																	long
																																																		BgL_iz00_2244;
																																																	BgL_iz00_2244
																																																		=
																																																		7L;
																																																BgL_zc3z04anonymousza31677ze3z87_2245:
																																																	if ((BgL_iz00_2244 >= 0L))
																																																		{	/* Unsafe/intext.scm 770 */
																																																			long
																																																				BgL_dz00_2247;
																																																			{	/* Unsafe/intext.scm 770 */
																																																				uint64_t
																																																					BgL_arg1684z00_2249;
																																																				{	/* Unsafe/intext.scm 770 */
																																																					uint64_t
																																																						BgL_tmpz00_8665;
																																																					{	/* Unsafe/intext.scm 770 */
																																																						uint64_t
																																																							BgL_xz00_4204;
																																																						BgL_xz00_4204
																																																							=
																																																							BGL_BINT64_TO_INT64
																																																							(BgL_mz00_2240);
																																																						BgL_tmpz00_8665
																																																							=
																																																							(BgL_xz00_4204
																																																							>>
																																																							(int)
																																																							((8L * BgL_iz00_2244)));
																																																					}
																																																					BgL_arg1684z00_2249
																																																						=
																																																						(BgL_tmpz00_8665
																																																						&
																																																						(uint64_t)
																																																						(255L));
																																																				}
																																																				{	/* Unsafe/intext.scm 770 */
																																																					long
																																																						BgL_arg2358z00_4209;
																																																					BgL_arg2358z00_4209
																																																						=
																																																						(long)
																																																						(BgL_arg1684z00_2249);
																																																					BgL_dz00_2247
																																																						=
																																																						(long)
																																																						(BgL_arg2358z00_4209);
																																																			}}
																																																			{	/* Unsafe/intext.scm 748 */
																																																				unsigned
																																																					char
																																																					BgL_arg1656z00_4211;
																																																				BgL_arg1656z00_4211
																																																					=
																																																					(BgL_dz00_2247);
																																																				BGl_z62z12printzd2markupza2zz__intextz00
																																																					(BgL_ptrz00_6301,
																																																					BgL_bufferz00_6300,
																																																					BgL_arg1656z00_4211);
																																																			}
																																																			{
																																																				long
																																																					BgL_iz00_8676;
																																																				BgL_iz00_8676
																																																					=
																																																					(BgL_iz00_2244
																																																					-
																																																					1L);
																																																				BgL_iz00_2244
																																																					=
																																																					BgL_iz00_8676;
																																																				goto
																																																					BgL_zc3z04anonymousza31677ze3z87_2245;
																																																			}
																																																		}
																																																	else
																																																		{	/* Unsafe/intext.scm 769 */
																																																			BgL_tmpz00_8662
																																																				=
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																																return
																																																	BBOOL
																																																	(BgL_tmpz00_8662);
																																															}
																																														}
																																													else
																																														{	/* Unsafe/intext.scm 1096 */
																																															if (CNSTP(BgL_itemz00_2565))
																																																{	/* Unsafe/intext.scm 1099 */
																																																	BGl_z62checkzd2bufferz12za2zz__intextz00
																																																		(BgL_bufferz00_6300,
																																																		BgL_ptrz00_6301,
																																																		BINT
																																																		(1L));
																																																	{	/* Unsafe/intext.scm 719 */
																																																		obj_t
																																																			BgL_stringz00_5252;
																																																		long
																																																			BgL_kz00_5253;
																																																		BgL_stringz00_5252
																																																			=
																																																			CELL_REF
																																																			(BgL_bufferz00_6300);
																																																		BgL_kz00_5253
																																																			=
																																																			(long)
																																																			CINT
																																																			(CELL_REF
																																																			(BgL_ptrz00_6301));
																																																		STRING_SET
																																																			(BgL_stringz00_5252,
																																																			BgL_kz00_5253,
																																																			((unsigned char) '<'));
																																																	}
																																																	{	/* Unsafe/intext.scm 720 */
																																																		obj_t
																																																			BgL_auxz00_6332;
																																																		BgL_auxz00_6332
																																																			=
																																																			ADDFX
																																																			(BINT
																																																			(1L),
																																																			CELL_REF
																																																			(BgL_ptrz00_6301));
																																																		CELL_SET
																																																			(BgL_ptrz00_6301,
																																																			BgL_auxz00_6332);
																																																	}
																																																	{	/* Unsafe/intext.scm 1101 */
																																																		long
																																																			BgL_arg1914z00_2601;
																																																		BgL_arg1914z00_2601
																																																			=
																																																			CCNST
																																																			(BgL_itemz00_2565);
																																																		if ((BgL_arg1914z00_2601 < 0L))
																																																			{	/* Unsafe/intext.scm 802 */
																																																				BGl_z62checkzd2bufferz12za2zz__intextz00
																																																					(BgL_bufferz00_6300,
																																																					BgL_ptrz00_6301,
																																																					BINT
																																																					(1L));
																																																				{	/* Unsafe/intext.scm 719 */
																																																					obj_t
																																																						BgL_stringz00_5260;
																																																					long
																																																						BgL_kz00_5261;
																																																					BgL_stringz00_5260
																																																						=
																																																						CELL_REF
																																																						(BgL_bufferz00_6300);
																																																					BgL_kz00_5261
																																																						=
																																																						(long)
																																																						CINT
																																																						(CELL_REF
																																																						(BgL_ptrz00_6301));
																																																					STRING_SET
																																																						(BgL_stringz00_5260,
																																																						BgL_kz00_5261,
																																																						((unsigned char) '-'));
																																																				}
																																																				{	/* Unsafe/intext.scm 720 */
																																																					obj_t
																																																						BgL_auxz00_6333;
																																																					BgL_auxz00_6333
																																																						=
																																																						ADDFX
																																																						(BINT
																																																						(1L),
																																																						CELL_REF
																																																						(BgL_ptrz00_6301));
																																																					CELL_SET
																																																						(BgL_ptrz00_6301,
																																																						BgL_auxz00_6333);
																																																				}
																																																				{	/* Unsafe/intext.scm 805 */
																																																					long
																																																						BgL_arg1714z00_5258;
																																																					BgL_arg1714z00_5258
																																																						=
																																																						NEG
																																																						(BgL_arg1914z00_2601);
																																																					return
																																																						BGl_z62printzd2wordzb0zz__intextz00
																																																						(BgL_ptrz00_6301,
																																																						BgL_bufferz00_6300,
																																																						BgL_arg1714z00_5258);
																																																				}
																																																			}
																																																		else
																																																			{	/* Unsafe/intext.scm 802 */
																																																				return
																																																					BGl_z62printzd2wordzb0zz__intextz00
																																																					(BgL_ptrz00_6301,
																																																					BgL_bufferz00_6300,
																																																					BgL_arg1914z00_2601);
																																																			}
																																																	}
																																																}
																																															else
																																																{	/* Unsafe/intext.scm 1099 */
																																																	if (INTEGERP(BgL_itemz00_2565))
																																																		{	/* Unsafe/intext.scm 1103 */
																																																			long
																																																				BgL_iz00_5265;
																																																			BgL_iz00_5265
																																																				=
																																																				(long)
																																																				CINT
																																																				(BgL_itemz00_2565);
																																																			if ((BgL_iz00_5265 < 0L))
																																																				{	/* Unsafe/intext.scm 802 */
																																																					BGl_z62checkzd2bufferz12za2zz__intextz00
																																																						(BgL_bufferz00_6300,
																																																						BgL_ptrz00_6301,
																																																						BINT
																																																						(1L));
																																																					{	/* Unsafe/intext.scm 719 */
																																																						obj_t
																																																							BgL_stringz00_5269;
																																																						long
																																																							BgL_kz00_5270;
																																																						BgL_stringz00_5269
																																																							=
																																																							CELL_REF
																																																							(BgL_bufferz00_6300);
																																																						BgL_kz00_5270
																																																							=
																																																							(long)
																																																							CINT
																																																							(CELL_REF
																																																							(BgL_ptrz00_6301));
																																																						STRING_SET
																																																							(BgL_stringz00_5269,
																																																							BgL_kz00_5270,
																																																							((unsigned char) '-'));
																																																					}
																																																					{	/* Unsafe/intext.scm 720 */
																																																						obj_t
																																																							BgL_auxz00_6334;
																																																						BgL_auxz00_6334
																																																							=
																																																							ADDFX
																																																							(BINT
																																																							(1L),
																																																							CELL_REF
																																																							(BgL_ptrz00_6301));
																																																						CELL_SET
																																																							(BgL_ptrz00_6301,
																																																							BgL_auxz00_6334);
																																																					}
																																																					{	/* Unsafe/intext.scm 805 */
																																																						long
																																																							BgL_arg1714z00_5267;
																																																						BgL_arg1714z00_5267
																																																							=
																																																							NEG
																																																							(BgL_iz00_5265);
																																																						return
																																																							BGl_z62printzd2wordzb0zz__intextz00
																																																							(BgL_ptrz00_6301,
																																																							BgL_bufferz00_6300,
																																																							BgL_arg1714z00_5267);
																																																					}
																																																				}
																																																			else
																																																				{	/* Unsafe/intext.scm 802 */
																																																					return
																																																						BGl_z62printzd2wordzb0zz__intextz00
																																																						(BgL_ptrz00_6301,
																																																						BgL_bufferz00_6300,
																																																						BgL_iz00_5265);
																																																				}
																																																		}
																																																	else
																																																		{	/* Unsafe/intext.scm 1102 */
																																																			if (REALP(BgL_itemz00_2565))
																																																				{	/* Unsafe/intext.scm 1104 */
																																																					BGl_z62checkzd2bufferz12za2zz__intextz00
																																																						(BgL_bufferz00_6300,
																																																						BgL_ptrz00_6301,
																																																						BINT
																																																						(1L));
																																																					{	/* Unsafe/intext.scm 719 */
																																																						obj_t
																																																							BgL_stringz00_5274;
																																																						long
																																																							BgL_kz00_5275;
																																																						BgL_stringz00_5274
																																																							=
																																																							CELL_REF
																																																							(BgL_bufferz00_6300);
																																																						BgL_kz00_5275
																																																							=
																																																							(long)
																																																							CINT
																																																							(CELL_REF
																																																							(BgL_ptrz00_6301));
																																																						STRING_SET
																																																							(BgL_stringz00_5274,
																																																							BgL_kz00_5275,
																																																							((unsigned char) 'f'));
																																																					}
																																																					{	/* Unsafe/intext.scm 720 */
																																																						obj_t
																																																							BgL_auxz00_6335;
																																																						BgL_auxz00_6335
																																																							=
																																																							ADDFX
																																																							(BINT
																																																							(1L),
																																																							CELL_REF
																																																							(BgL_ptrz00_6301));
																																																						CELL_SET
																																																							(BgL_ptrz00_6301,
																																																							BgL_auxz00_6335);
																																																					}
																																																					{	/* Unsafe/intext.scm 1106 */
																																																						obj_t
																																																							BgL_sz00_2604;
																																																						BgL_sz00_2604
																																																							=
																																																							bgl_real_to_string
																																																							(REAL_TO_DOUBLE
																																																							(BgL_itemz00_2565));
																																																						{	/* Unsafe/intext.scm 1107 */
																																																							long
																																																								BgL_arg1917z00_2605;
																																																							BgL_arg1917z00_2605
																																																								=
																																																								STRING_LENGTH
																																																								(BgL_sz00_2604);
																																																							BGl_z62printzd2wordzb0zz__intextz00
																																																								(BgL_ptrz00_6301,
																																																								BgL_bufferz00_6300,
																																																								BgL_arg1917z00_2605);
																																																							BGl_z62checkzd2bufferz12za2zz__intextz00
																																																								(BgL_bufferz00_6300,
																																																								BgL_ptrz00_6301,
																																																								BINT
																																																								(BgL_arg1917z00_2605));
																																																							{	/* Unsafe/intext.scm 726 */
																																																								obj_t
																																																									BgL_s2z00_5280;
																																																								long
																																																									BgL_o2z00_5281;
																																																								BgL_s2z00_5280
																																																									=
																																																									CELL_REF
																																																									(BgL_bufferz00_6300);
																																																								BgL_o2z00_5281
																																																									=
																																																									(long)
																																																									CINT
																																																									(CELL_REF
																																																									(BgL_ptrz00_6301));
																																																								blit_string
																																																									(BgL_sz00_2604,
																																																									0L,
																																																									BgL_s2z00_5280,
																																																									BgL_o2z00_5281,
																																																									BgL_arg1917z00_2605);
																																																							}
																																																							{	/* Unsafe/intext.scm 727 */
																																																								obj_t
																																																									BgL_auxz00_6336;
																																																								BgL_auxz00_6336
																																																									=
																																																									ADDFX
																																																									(CELL_REF
																																																									(BgL_ptrz00_6301),
																																																									BINT
																																																									(BgL_arg1917z00_2605));
																																																								return
																																																									CELL_SET
																																																									(BgL_ptrz00_6301,
																																																									BgL_auxz00_6336);
																																																							}
																																																						}
																																																					}
																																																				}
																																																			else
																																																				{	/* Unsafe/intext.scm 1104 */
																																																					if (UCS2_STRINGP(BgL_itemz00_2565))
																																																						{	/* Unsafe/intext.scm 1110 */
																																																							obj_t
																																																								BgL_zc3z04anonymousza31920ze3z87_6285;
																																																							{
																																																								int
																																																									BgL_tmpz00_8733;
																																																								BgL_tmpz00_8733
																																																									=
																																																									(int)
																																																									(2L);
																																																								BgL_zc3z04anonymousza31920ze3z87_6285
																																																									=
																																																									MAKE_L_PROCEDURE
																																																									(BGl_z62zc3z04anonymousza31920ze3ze5zz__intextz00,
																																																									BgL_tmpz00_8733);
																																																							}
																																																							PROCEDURE_L_SET
																																																								(BgL_zc3z04anonymousza31920ze3z87_6285,
																																																								(int)
																																																								(0L),
																																																								((obj_t) BgL_bufferz00_6300));
																																																							PROCEDURE_L_SET
																																																								(BgL_zc3z04anonymousza31920ze3z87_6285,
																																																								(int)
																																																								(1L),
																																																								((obj_t) BgL_ptrz00_6301));
																																																							{
																																																								obj_t
																																																									BgL_printerz00_8743;
																																																								obj_t
																																																									BgL_itemz00_8742;
																																																								BgL_itemz00_8742
																																																									=
																																																									BgL_itemz00_2565;
																																																								BgL_printerz00_8743
																																																									=
																																																									BgL_zc3z04anonymousza31920ze3z87_6285;
																																																								BgL_printerz00_2294
																																																									=
																																																									BgL_printerz00_8743;
																																																								BgL_itemz00_2293
																																																									=
																																																									BgL_itemz00_8742;
																																																								goto
																																																									BgL_zc3z04anonymousza31718ze3z87_2295;
																																																							}
																																																						}
																																																					else
																																																						{	/* Unsafe/intext.scm 1108 */
																																																							if (CELLP(BgL_itemz00_2565))
																																																								{
																																																									obj_t
																																																										BgL_printerz00_8747;
																																																									obj_t
																																																										BgL_itemz00_8746;
																																																									BgL_itemz00_8746
																																																										=
																																																										BgL_itemz00_2565;
																																																									BgL_printerz00_8747
																																																										=
																																																										BgL_printzd2cellzd2_6309;
																																																									BgL_printerz00_2294
																																																										=
																																																										BgL_printerz00_8747;
																																																									BgL_itemz00_2293
																																																										=
																																																										BgL_itemz00_8746;
																																																									goto
																																																										BgL_zc3z04anonymousza31718ze3z87_2295;
																																																								}
																																																							else
																																																								{	/* Unsafe/intext.scm 1111 */
																																																									if (BGL_WEAKPTRP(BgL_itemz00_2565))
																																																										{
																																																											obj_t
																																																												BgL_printerz00_8751;
																																																											obj_t
																																																												BgL_itemz00_8750;
																																																											BgL_itemz00_8750
																																																												=
																																																												BgL_itemz00_2565;
																																																											BgL_printerz00_8751
																																																												=
																																																												BgL_printzd2weakptrzd2_6308;
																																																											BgL_printerz00_2294
																																																												=
																																																												BgL_printerz00_8751;
																																																											BgL_itemz00_2293
																																																												=
																																																												BgL_itemz00_8750;
																																																											goto
																																																												BgL_zc3z04anonymousza31718ze3z87_2295;
																																																										}
																																																									else
																																																										{	/* Unsafe/intext.scm 1113 */
																																																											if (VECTORP(BgL_itemz00_2565))
																																																												{
																																																													obj_t
																																																														BgL_printerz00_8755;
																																																													obj_t
																																																														BgL_itemz00_8754;
																																																													BgL_itemz00_8754
																																																														=
																																																														BgL_itemz00_2565;
																																																													BgL_printerz00_8755
																																																														=
																																																														BgL_printzd2vectorzd2_6307;
																																																													BgL_printerz00_2294
																																																														=
																																																														BgL_printerz00_8755;
																																																													BgL_itemz00_2293
																																																														=
																																																														BgL_itemz00_8754;
																																																													goto
																																																														BgL_zc3z04anonymousza31718ze3z87_2295;
																																																												}
																																																											else
																																																												{	/* Unsafe/intext.scm 1115 */
																																																													if (BGL_HVECTORP(BgL_itemz00_2565))
																																																														{
																																																															obj_t
																																																																BgL_printerz00_8759;
																																																															obj_t
																																																																BgL_itemz00_8758;
																																																															BgL_itemz00_8758
																																																																=
																																																																BgL_itemz00_2565;
																																																															BgL_printerz00_8759
																																																																=
																																																																BgL_printzd2hvectorzd2_6306;
																																																															BgL_printerz00_2294
																																																																=
																																																																BgL_printerz00_8759;
																																																															BgL_itemz00_2293
																																																																=
																																																																BgL_itemz00_8758;
																																																															goto
																																																																BgL_zc3z04anonymousza31718ze3z87_2295;
																																																														}
																																																													else
																																																														{	/* Unsafe/intext.scm 1117 */
																																																															if (TVECTORP(BgL_itemz00_2565))
																																																																{
																																																																	obj_t
																																																																		BgL_printerz00_8763;
																																																																	obj_t
																																																																		BgL_itemz00_8762;
																																																																	BgL_itemz00_8762
																																																																		=
																																																																		BgL_itemz00_2565;
																																																																	BgL_printerz00_8763
																																																																		=
																																																																		BgL_printzd2tvectorzd2_6305;
																																																																	BgL_printerz00_2294
																																																																		=
																																																																		BgL_printerz00_8763;
																																																																	BgL_itemz00_2293
																																																																		=
																																																																		BgL_itemz00_8762;
																																																																	goto
																																																																		BgL_zc3z04anonymousza31718ze3z87_2295;
																																																																}
																																																															else
																																																																{	/* Unsafe/intext.scm 1119 */
																																																																	if (ELONGP(BgL_itemz00_2565))
																																																																		{	/* Unsafe/intext.scm 1121 */
																																																																			BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																				(BgL_bufferz00_6300,
																																																																				BgL_ptrz00_6301,
																																																																				BINT
																																																																				(1L));
																																																																			{	/* Unsafe/intext.scm 719 */
																																																																				obj_t
																																																																					BgL_stringz00_5293;
																																																																				long
																																																																					BgL_kz00_5294;
																																																																				BgL_stringz00_5293
																																																																					=
																																																																					CELL_REF
																																																																					(BgL_bufferz00_6300);
																																																																				BgL_kz00_5294
																																																																					=
																																																																					(long)
																																																																					CINT
																																																																					(CELL_REF
																																																																					(BgL_ptrz00_6301));
																																																																				STRING_SET
																																																																					(BgL_stringz00_5293,
																																																																					BgL_kz00_5294,
																																																																					((unsigned char) 'E'));
																																																																			}
																																																																			{	/* Unsafe/intext.scm 720 */
																																																																				obj_t
																																																																					BgL_auxz00_6337;
																																																																				BgL_auxz00_6337
																																																																					=
																																																																					ADDFX
																																																																					(BINT
																																																																					(1L),
																																																																					CELL_REF
																																																																					(BgL_ptrz00_6301));
																																																																				CELL_SET
																																																																					(BgL_ptrz00_6301,
																																																																					BgL_auxz00_6337);
																																																																			}
																																																																			{	/* Unsafe/intext.scm 1123 */
																																																																				obj_t
																																																																					BgL_sz00_2619;
																																																																				BgL_sz00_2619
																																																																					=
																																																																					BGl_elongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																																																					(BELONG_TO_LONG
																																																																					(BgL_itemz00_2565),
																																																																					BNIL);
																																																																				{	/* Unsafe/intext.scm 1124 */
																																																																					long
																																																																						BgL_arg1930z00_2620;
																																																																					BgL_arg1930z00_2620
																																																																						=
																																																																						STRING_LENGTH
																																																																						(BgL_sz00_2619);
																																																																					BGl_z62printzd2wordzb0zz__intextz00
																																																																						(BgL_ptrz00_6301,
																																																																						BgL_bufferz00_6300,
																																																																						BgL_arg1930z00_2620);
																																																																					BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																						(BgL_bufferz00_6300,
																																																																						BgL_ptrz00_6301,
																																																																						BINT
																																																																						(BgL_arg1930z00_2620));
																																																																					{	/* Unsafe/intext.scm 726 */
																																																																						obj_t
																																																																							BgL_s2z00_5299;
																																																																						long
																																																																							BgL_o2z00_5300;
																																																																						BgL_s2z00_5299
																																																																							=
																																																																							CELL_REF
																																																																							(BgL_bufferz00_6300);
																																																																						BgL_o2z00_5300
																																																																							=
																																																																							(long)
																																																																							CINT
																																																																							(CELL_REF
																																																																							(BgL_ptrz00_6301));
																																																																						blit_string
																																																																							(BgL_sz00_2619,
																																																																							0L,
																																																																							BgL_s2z00_5299,
																																																																							BgL_o2z00_5300,
																																																																							BgL_arg1930z00_2620);
																																																																					}
																																																																					{	/* Unsafe/intext.scm 727 */
																																																																						obj_t
																																																																							BgL_auxz00_6338;
																																																																						BgL_auxz00_6338
																																																																							=
																																																																							ADDFX
																																																																							(CELL_REF
																																																																							(BgL_ptrz00_6301),
																																																																							BINT
																																																																							(BgL_arg1930z00_2620));
																																																																						return
																																																																							CELL_SET
																																																																							(BgL_ptrz00_6301,
																																																																							BgL_auxz00_6338);
																																																																					}
																																																																				}
																																																																			}
																																																																		}
																																																																	else
																																																																		{	/* Unsafe/intext.scm 1121 */
																																																																			if (LLONGP(BgL_itemz00_2565))
																																																																				{	/* Unsafe/intext.scm 1125 */
																																																																					BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																						(BgL_bufferz00_6300,
																																																																						BgL_ptrz00_6301,
																																																																						BINT
																																																																						(1L));
																																																																					{	/* Unsafe/intext.scm 719 */
																																																																						obj_t
																																																																							BgL_stringz00_5304;
																																																																						long
																																																																							BgL_kz00_5305;
																																																																						BgL_stringz00_5304
																																																																							=
																																																																							CELL_REF
																																																																							(BgL_bufferz00_6300);
																																																																						BgL_kz00_5305
																																																																							=
																																																																							(long)
																																																																							CINT
																																																																							(CELL_REF
																																																																							(BgL_ptrz00_6301));
																																																																						STRING_SET
																																																																							(BgL_stringz00_5304,
																																																																							BgL_kz00_5305,
																																																																							((unsigned char) 'L'));
																																																																					}
																																																																					{	/* Unsafe/intext.scm 720 */
																																																																						obj_t
																																																																							BgL_auxz00_6339;
																																																																						BgL_auxz00_6339
																																																																							=
																																																																							ADDFX
																																																																							(BINT
																																																																							(1L),
																																																																							CELL_REF
																																																																							(BgL_ptrz00_6301));
																																																																						CELL_SET
																																																																							(BgL_ptrz00_6301,
																																																																							BgL_auxz00_6339);
																																																																					}
																																																																					{	/* Unsafe/intext.scm 1127 */
																																																																						obj_t
																																																																							BgL_sz00_2623;
																																																																						BgL_sz00_2623
																																																																							=
																																																																							BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																																																							(BLLONG_TO_LLONG
																																																																							(BgL_itemz00_2565),
																																																																							BNIL);
																																																																						{	/* Unsafe/intext.scm 1128 */
																																																																							long
																																																																								BgL_arg1933z00_2624;
																																																																							BgL_arg1933z00_2624
																																																																								=
																																																																								STRING_LENGTH
																																																																								(BgL_sz00_2623);
																																																																							BGl_z62printzd2wordzb0zz__intextz00
																																																																								(BgL_ptrz00_6301,
																																																																								BgL_bufferz00_6300,
																																																																								BgL_arg1933z00_2624);
																																																																							BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																								(BgL_bufferz00_6300,
																																																																								BgL_ptrz00_6301,
																																																																								BINT
																																																																								(BgL_arg1933z00_2624));
																																																																							{	/* Unsafe/intext.scm 726 */
																																																																								obj_t
																																																																									BgL_s2z00_5310;
																																																																								long
																																																																									BgL_o2z00_5311;
																																																																								BgL_s2z00_5310
																																																																									=
																																																																									CELL_REF
																																																																									(BgL_bufferz00_6300);
																																																																								BgL_o2z00_5311
																																																																									=
																																																																									(long)
																																																																									CINT
																																																																									(CELL_REF
																																																																									(BgL_ptrz00_6301));
																																																																								blit_string
																																																																									(BgL_sz00_2623,
																																																																									0L,
																																																																									BgL_s2z00_5310,
																																																																									BgL_o2z00_5311,
																																																																									BgL_arg1933z00_2624);
																																																																							}
																																																																							{	/* Unsafe/intext.scm 727 */
																																																																								obj_t
																																																																									BgL_auxz00_6340;
																																																																								BgL_auxz00_6340
																																																																									=
																																																																									ADDFX
																																																																									(CELL_REF
																																																																									(BgL_ptrz00_6301),
																																																																									BINT
																																																																									(BgL_arg1933z00_2624));
																																																																								return
																																																																									CELL_SET
																																																																									(BgL_ptrz00_6301,
																																																																									BgL_auxz00_6340);
																																																																							}
																																																																						}
																																																																					}
																																																																				}
																																																																			else
																																																																				{	/* Unsafe/intext.scm 1125 */
																																																																					if (BGL_DATEP(BgL_itemz00_2565))
																																																																						{	/* Unsafe/intext.scm 1129 */
																																																																							BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																								(BgL_bufferz00_6300,
																																																																								BgL_ptrz00_6301,
																																																																								BINT
																																																																								(1L));
																																																																							{	/* Unsafe/intext.scm 719 */
																																																																								obj_t
																																																																									BgL_stringz00_5315;
																																																																								long
																																																																									BgL_kz00_5316;
																																																																								BgL_stringz00_5315
																																																																									=
																																																																									CELL_REF
																																																																									(BgL_bufferz00_6300);
																																																																								BgL_kz00_5316
																																																																									=
																																																																									(long)
																																																																									CINT
																																																																									(CELL_REF
																																																																									(BgL_ptrz00_6301));
																																																																								STRING_SET
																																																																									(BgL_stringz00_5315,
																																																																									BgL_kz00_5316,
																																																																									((unsigned char) 'D'));
																																																																							}
																																																																							{	/* Unsafe/intext.scm 720 */
																																																																								obj_t
																																																																									BgL_auxz00_6341;
																																																																								BgL_auxz00_6341
																																																																									=
																																																																									ADDFX
																																																																									(BINT
																																																																									(1L),
																																																																									CELL_REF
																																																																									(BgL_ptrz00_6301));
																																																																								CELL_SET
																																																																									(BgL_ptrz00_6301,
																																																																									BgL_auxz00_6341);
																																																																							}
																																																																							{	/* Unsafe/intext.scm 1131 */
																																																																								obj_t
																																																																									BgL_sz00_2627;
																																																																								BgL_sz00_2627
																																																																									=
																																																																									BGl_llongzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																																																									(bgl_date_to_nanoseconds
																																																																									(BgL_itemz00_2565),
																																																																									BNIL);
																																																																								{	/* Unsafe/intext.scm 1132 */
																																																																									long
																																																																										BgL_arg1936z00_2628;
																																																																									BgL_arg1936z00_2628
																																																																										=
																																																																										STRING_LENGTH
																																																																										(BgL_sz00_2627);
																																																																									BGl_z62printzd2wordzb0zz__intextz00
																																																																										(BgL_ptrz00_6301,
																																																																										BgL_bufferz00_6300,
																																																																										BgL_arg1936z00_2628);
																																																																									BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																										(BgL_bufferz00_6300,
																																																																										BgL_ptrz00_6301,
																																																																										BINT
																																																																										(BgL_arg1936z00_2628));
																																																																									{	/* Unsafe/intext.scm 726 */
																																																																										obj_t
																																																																											BgL_s2z00_5322;
																																																																										long
																																																																											BgL_o2z00_5323;
																																																																										BgL_s2z00_5322
																																																																											=
																																																																											CELL_REF
																																																																											(BgL_bufferz00_6300);
																																																																										BgL_o2z00_5323
																																																																											=
																																																																											(long)
																																																																											CINT
																																																																											(CELL_REF
																																																																											(BgL_ptrz00_6301));
																																																																										blit_string
																																																																											(BgL_sz00_2627,
																																																																											0L,
																																																																											BgL_s2z00_5322,
																																																																											BgL_o2z00_5323,
																																																																											BgL_arg1936z00_2628);
																																																																									}
																																																																									{	/* Unsafe/intext.scm 727 */
																																																																										obj_t
																																																																											BgL_auxz00_6342;
																																																																										BgL_auxz00_6342
																																																																											=
																																																																											ADDFX
																																																																											(CELL_REF
																																																																											(BgL_ptrz00_6301),
																																																																											BINT
																																																																											(BgL_arg1936z00_2628));
																																																																										return
																																																																											CELL_SET
																																																																											(BgL_ptrz00_6301,
																																																																											BgL_auxz00_6342);
																																																																									}
																																																																								}
																																																																							}
																																																																						}
																																																																					else
																																																																						{	/* Unsafe/intext.scm 1129 */
																																																																							if (BIGNUMP(BgL_itemz00_2565))
																																																																								{	/* Unsafe/intext.scm 1133 */
																																																																									BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																										(BgL_bufferz00_6300,
																																																																										BgL_ptrz00_6301,
																																																																										BINT
																																																																										(1L));
																																																																									{	/* Unsafe/intext.scm 719 */
																																																																										obj_t
																																																																											BgL_stringz00_5327;
																																																																										long
																																																																											BgL_kz00_5328;
																																																																										BgL_stringz00_5327
																																																																											=
																																																																											CELL_REF
																																																																											(BgL_bufferz00_6300);
																																																																										BgL_kz00_5328
																																																																											=
																																																																											(long)
																																																																											CINT
																																																																											(CELL_REF
																																																																											(BgL_ptrz00_6301));
																																																																										STRING_SET
																																																																											(BgL_stringz00_5327,
																																																																											BgL_kz00_5328,
																																																																											((unsigned char) 'z'));
																																																																									}
																																																																									{	/* Unsafe/intext.scm 720 */
																																																																										obj_t
																																																																											BgL_auxz00_6343;
																																																																										BgL_auxz00_6343
																																																																											=
																																																																											ADDFX
																																																																											(BINT
																																																																											(1L),
																																																																											CELL_REF
																																																																											(BgL_ptrz00_6301));
																																																																										CELL_SET
																																																																											(BgL_ptrz00_6301,
																																																																											BgL_auxz00_6343);
																																																																									}
																																																																									{	/* Unsafe/intext.scm 1135 */
																																																																										obj_t
																																																																											BgL_sz00_2632;
																																																																										{	/* Ieee/fixnum.scm 1010 */

																																																																											BgL_sz00_2632
																																																																												=
																																																																												BGl_bignumzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																																																												(BgL_itemz00_2565,
																																																																												10L);
																																																																										}
																																																																										{	/* Unsafe/intext.scm 1136 */
																																																																											long
																																																																												BgL_arg1940z00_2633;
																																																																											BgL_arg1940z00_2633
																																																																												=
																																																																												STRING_LENGTH
																																																																												(BgL_sz00_2632);
																																																																											BGl_z62printzd2wordzb0zz__intextz00
																																																																												(BgL_ptrz00_6301,
																																																																												BgL_bufferz00_6300,
																																																																												BgL_arg1940z00_2633);
																																																																											BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																												(BgL_bufferz00_6300,
																																																																												BgL_ptrz00_6301,
																																																																												BINT
																																																																												(BgL_arg1940z00_2633));
																																																																											{	/* Unsafe/intext.scm 726 */
																																																																												obj_t
																																																																													BgL_s2z00_5333;
																																																																												long
																																																																													BgL_o2z00_5334;
																																																																												BgL_s2z00_5333
																																																																													=
																																																																													CELL_REF
																																																																													(BgL_bufferz00_6300);
																																																																												BgL_o2z00_5334
																																																																													=
																																																																													(long)
																																																																													CINT
																																																																													(CELL_REF
																																																																													(BgL_ptrz00_6301));
																																																																												blit_string
																																																																													(BgL_sz00_2632,
																																																																													0L,
																																																																													BgL_s2z00_5333,
																																																																													BgL_o2z00_5334,
																																																																													BgL_arg1940z00_2633);
																																																																											}
																																																																											{	/* Unsafe/intext.scm 727 */
																																																																												obj_t
																																																																													BgL_auxz00_6344;
																																																																												BgL_auxz00_6344
																																																																													=
																																																																													ADDFX
																																																																													(CELL_REF
																																																																													(BgL_ptrz00_6301),
																																																																													BINT
																																																																													(BgL_arg1940z00_2633));
																																																																												return
																																																																													CELL_SET
																																																																													(BgL_ptrz00_6301,
																																																																													BgL_auxz00_6344);
																																																																											}
																																																																										}
																																																																									}
																																																																								}
																																																																							else
																																																																								{	/* Unsafe/intext.scm 1133 */
																																																																									if (CUSTOMP(BgL_itemz00_2565))
																																																																										{
																																																																											obj_t
																																																																												BgL_printerz00_8838;
																																																																											obj_t
																																																																												BgL_itemz00_8837;
																																																																											BgL_itemz00_8837
																																																																												=
																																																																												BgL_itemz00_2565;
																																																																											BgL_printerz00_8838
																																																																												=
																																																																												BgL_printzd2customzd2_6304;
																																																																											BgL_printerz00_2294
																																																																												=
																																																																												BgL_printerz00_8838;
																																																																											BgL_itemz00_2293
																																																																												=
																																																																												BgL_itemz00_8837;
																																																																											goto
																																																																												BgL_zc3z04anonymousza31718ze3z87_2295;
																																																																										}
																																																																									else
																																																																										{	/* Unsafe/intext.scm 1137 */
																																																																											if (PROCEDUREP(BgL_itemz00_2565))
																																																																												{	/* Unsafe/intext.scm 1140 */
																																																																													obj_t
																																																																														BgL_zc3z04anonymousza31945ze3z87_6276;
																																																																													{
																																																																														int
																																																																															BgL_tmpz00_8841;
																																																																														BgL_tmpz00_8841
																																																																															=
																																																																															(int)
																																																																															(14L);
																																																																														BgL_zc3z04anonymousza31945ze3z87_6276
																																																																															=
																																																																															MAKE_L_PROCEDURE
																																																																															(BGl_z62zc3z04anonymousza31945ze3ze5zz__intextz00,
																																																																															BgL_tmpz00_8841);
																																																																													}
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(0L),
																																																																														BgL_tablez00_6302);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(1L),
																																																																														((obj_t) BgL_refz00_6303));
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(2L),
																																																																														BgL_printzd2customzd2_6304);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(3L),
																																																																														BgL_printzd2tvectorzd2_6305);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(4L),
																																																																														BgL_printzd2hvectorzd2_6306);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(5L),
																																																																														BgL_printzd2vectorzd2_6307);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(6L),
																																																																														BgL_printzd2weakptrzd2_6308);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(7L),
																																																																														BgL_printzd2cellzd2_6309);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(8L),
																																																																														BgL_printzd2classzd2_6310);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(9L),
																																																																														BgL_printzd2objectzd2_6311);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(10L),
																																																																														BgL_printzd2pairzd2_6312);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(11L),
																																																																														BgL_printzd2epairzd2_6313);
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(12L),
																																																																														((obj_t) BgL_bufferz00_6300));
																																																																													PROCEDURE_L_SET
																																																																														(BgL_zc3z04anonymousza31945ze3z87_6276,
																																																																														(int)
																																																																														(13L),
																																																																														((obj_t) BgL_ptrz00_6301));
																																																																													{
																																																																														obj_t
																																																																															BgL_printerz00_8876;
																																																																														obj_t
																																																																															BgL_itemz00_8875;
																																																																														BgL_itemz00_8875
																																																																															=
																																																																															BgL_itemz00_2565;
																																																																														BgL_printerz00_8876
																																																																															=
																																																																															BgL_zc3z04anonymousza31945ze3z87_6276;
																																																																														BgL_printerz00_2294
																																																																															=
																																																																															BgL_printerz00_8876;
																																																																														BgL_itemz00_2293
																																																																															=
																																																																															BgL_itemz00_8875;
																																																																														goto
																																																																															BgL_zc3z04anonymousza31718ze3z87_2295;
																																																																													}
																																																																												}
																																																																											else
																																																																												{	/* Unsafe/intext.scm 1139 */
																																																																													if (PROCESSP(BgL_itemz00_2565))
																																																																														{	/* Unsafe/intext.scm 1142 */
																																																																															obj_t
																																																																																BgL_zc3z04anonymousza31948ze3z87_6275;
																																																																															{
																																																																																int
																																																																																	BgL_tmpz00_8879;
																																																																																BgL_tmpz00_8879
																																																																																	=
																																																																																	(int)
																																																																																	(14L);
																																																																																BgL_zc3z04anonymousza31948ze3z87_6275
																																																																																	=
																																																																																	MAKE_L_PROCEDURE
																																																																																	(BGl_z62zc3z04anonymousza31948ze3ze5zz__intextz00,
																																																																																	BgL_tmpz00_8879);
																																																																															}
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(0L),
																																																																																BgL_tablez00_6302);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(1L),
																																																																																((obj_t) BgL_refz00_6303));
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(2L),
																																																																																BgL_printzd2customzd2_6304);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(3L),
																																																																																BgL_printzd2tvectorzd2_6305);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(4L),
																																																																																BgL_printzd2hvectorzd2_6306);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(5L),
																																																																																BgL_printzd2vectorzd2_6307);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(6L),
																																																																																BgL_printzd2weakptrzd2_6308);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(7L),
																																																																																BgL_printzd2cellzd2_6309);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(8L),
																																																																																BgL_printzd2classzd2_6310);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(9L),
																																																																																BgL_printzd2objectzd2_6311);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(10L),
																																																																																BgL_printzd2pairzd2_6312);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(11L),
																																																																																BgL_printzd2epairzd2_6313);
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(12L),
																																																																																((obj_t) BgL_bufferz00_6300));
																																																																															PROCEDURE_L_SET
																																																																																(BgL_zc3z04anonymousza31948ze3z87_6275,
																																																																																(int)
																																																																																(13L),
																																																																																((obj_t) BgL_ptrz00_6301));
																																																																															{
																																																																																obj_t
																																																																																	BgL_printerz00_8914;
																																																																																obj_t
																																																																																	BgL_itemz00_8913;
																																																																																BgL_itemz00_8913
																																																																																	=
																																																																																	BgL_itemz00_2565;
																																																																																BgL_printerz00_8914
																																																																																	=
																																																																																	BgL_zc3z04anonymousza31948ze3z87_6275;
																																																																																BgL_printerz00_2294
																																																																																	=
																																																																																	BgL_printerz00_8914;
																																																																																BgL_itemz00_2293
																																																																																	=
																																																																																	BgL_itemz00_8913;
																																																																																goto
																																																																																	BgL_zc3z04anonymousza31718ze3z87_2295;
																																																																															}
																																																																														}
																																																																													else
																																																																														{	/* Unsafe/intext.scm 1141 */
																																																																															if (OPAQUEP(BgL_itemz00_2565))
																																																																																{	/* Unsafe/intext.scm 1144 */
																																																																																	obj_t
																																																																																		BgL_zc3z04anonymousza31951ze3z87_6274;
																																																																																	{
																																																																																		int
																																																																																			BgL_tmpz00_8917;
																																																																																		BgL_tmpz00_8917
																																																																																			=
																																																																																			(int)
																																																																																			(14L);
																																																																																		BgL_zc3z04anonymousza31951ze3z87_6274
																																																																																			=
																																																																																			MAKE_L_PROCEDURE
																																																																																			(BGl_z62zc3z04anonymousza31951ze3ze5zz__intextz00,
																																																																																			BgL_tmpz00_8917);
																																																																																	}
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(0L),
																																																																																		BgL_tablez00_6302);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(1L),
																																																																																		((obj_t) BgL_refz00_6303));
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(2L),
																																																																																		BgL_printzd2customzd2_6304);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(3L),
																																																																																		BgL_printzd2tvectorzd2_6305);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(4L),
																																																																																		BgL_printzd2hvectorzd2_6306);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(5L),
																																																																																		BgL_printzd2vectorzd2_6307);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(6L),
																																																																																		BgL_printzd2weakptrzd2_6308);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(7L),
																																																																																		BgL_printzd2cellzd2_6309);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(8L),
																																																																																		BgL_printzd2classzd2_6310);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(9L),
																																																																																		BgL_printzd2objectzd2_6311);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(10L),
																																																																																		BgL_printzd2pairzd2_6312);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(11L),
																																																																																		BgL_printzd2epairzd2_6313);
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(12L),
																																																																																		((obj_t) BgL_bufferz00_6300));
																																																																																	PROCEDURE_L_SET
																																																																																		(BgL_zc3z04anonymousza31951ze3z87_6274,
																																																																																		(int)
																																																																																		(13L),
																																																																																		((obj_t) BgL_ptrz00_6301));
																																																																																	{
																																																																																		obj_t
																																																																																			BgL_printerz00_8952;
																																																																																		obj_t
																																																																																			BgL_itemz00_8951;
																																																																																		BgL_itemz00_8951
																																																																																			=
																																																																																			BgL_itemz00_2565;
																																																																																		BgL_printerz00_8952
																																																																																			=
																																																																																			BgL_zc3z04anonymousza31951ze3z87_6274;
																																																																																		BgL_printerz00_2294
																																																																																			=
																																																																																			BgL_printerz00_8952;
																																																																																		BgL_itemz00_2293
																																																																																			=
																																																																																			BgL_itemz00_8951;
																																																																																		goto
																																																																																			BgL_zc3z04anonymousza31718ze3z87_2295;
																																																																																	}
																																																																																}
																																																																															else
																																																																																{	/* Unsafe/intext.scm 1145 */
																																																																																	bool_t
																																																																																		BgL_test2919z00_8953;
																																																																																	if (STRUCTP(BgL_itemz00_2565))
																																																																																		{	/* Unsafe/intext.scm 129 */
																																																																																			BgL_test2919z00_8953
																																																																																				=
																																																																																				(STRUCT_KEY
																																																																																				(BgL_itemz00_2565)
																																																																																				==
																																																																																				BGl_symbol2659z00zz__intextz00);
																																																																																		}
																																																																																	else
																																																																																		{	/* Unsafe/intext.scm 129 */
																																																																																			BgL_test2919z00_8953
																																																																																				=
																																																																																				(
																																																																																				(bool_t)
																																																																																				0);
																																																																																		}
																																																																																	if (BgL_test2919z00_8953)
																																																																																		{	/* Unsafe/intext.scm 1145 */
																																																																																			BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																																				(BgL_bufferz00_6300,
																																																																																				BgL_ptrz00_6301,
																																																																																				BINT
																																																																																				(1L));
																																																																																			{	/* Unsafe/intext.scm 719 */
																																																																																				obj_t
																																																																																					BgL_stringz00_5360;
																																																																																				long
																																																																																					BgL_kz00_5361;
																																																																																				BgL_stringz00_5360
																																																																																					=
																																																																																					CELL_REF
																																																																																					(BgL_bufferz00_6300);
																																																																																				BgL_kz00_5361
																																																																																					=
																																																																																					(long)
																																																																																					CINT
																																																																																					(CELL_REF
																																																																																					(BgL_ptrz00_6301));
																																																																																				STRING_SET
																																																																																					(BgL_stringz00_5360,
																																																																																					BgL_kz00_5361,
																																																																																					((unsigned char) '"'));
																																																																																			}
																																																																																			{	/* Unsafe/intext.scm 720 */
																																																																																				obj_t
																																																																																					BgL_auxz00_6345;
																																																																																				BgL_auxz00_6345
																																																																																					=
																																																																																					ADDFX
																																																																																					(BINT
																																																																																					(1L),
																																																																																					CELL_REF
																																																																																					(BgL_ptrz00_6301));
																																																																																				CELL_SET
																																																																																					(BgL_ptrz00_6301,
																																																																																					BgL_auxz00_6345);
																																																																																			}
																																																																																			BgL_itemz00_2202
																																																																																				=
																																																																																				BgL_itemz00_2565;
																																																																																			{	/* Unsafe/intext.scm 731 */
																																																																																				obj_t
																																																																																					BgL_sz00_2204;
																																																																																				obj_t
																																																																																					BgL_offsetz00_2205;
																																																																																				obj_t
																																																																																					BgL_siza7eza7_2206;
																																																																																				BgL_sz00_2204
																																																																																					=
																																																																																					STRUCT_REF
																																																																																					(BgL_itemz00_2202,
																																																																																					(int)
																																																																																					(0L));
																																																																																				BgL_offsetz00_2205
																																																																																					=
																																																																																					STRUCT_REF
																																																																																					(BgL_itemz00_2202,
																																																																																					(int)
																																																																																					(1L));
																																																																																				BgL_siza7eza7_2206
																																																																																					=
																																																																																					STRUCT_REF
																																																																																					(BgL_itemz00_2202,
																																																																																					(int)
																																																																																					(2L));
																																																																																				{	/* Unsafe/intext.scm 793 */
																																																																																					long
																																																																																						BgL_siza7eza7_4162;
																																																																																					BgL_siza7eza7_4162
																																																																																						=
																																																																																						BGl_siza7ezd2ofzd2wordza7zz__intextz00
																																																																																						(
																																																																																						(long)
																																																																																						CINT
																																																																																						(BgL_siza7eza7_2206));
																																																																																					if ((BgL_siza7eza7_4162 == 0L))
																																																																																						{	/* Unsafe/intext.scm 748 */
																																																																																							unsigned
																																																																																								char
																																																																																								BgL_arg1656z00_4165;
																																																																																							BgL_arg1656z00_4165
																																																																																								=
																																																																																								(0L);
																																																																																							BGl_z62z12printzd2markupza2zz__intextz00
																																																																																								(BgL_ptrz00_6301,
																																																																																								BgL_bufferz00_6300,
																																																																																								BgL_arg1656z00_4165);
																																																																																						}
																																																																																					else
																																																																																						{	/* Unsafe/intext.scm 794 */
																																																																																							{	/* Unsafe/intext.scm 748 */
																																																																																								unsigned
																																																																																									char
																																																																																									BgL_arg1656z00_4166;
																																																																																								BgL_arg1656z00_4166
																																																																																									=
																																																																																									(BgL_siza7eza7_4162);
																																																																																								BGl_z62z12printzd2markupza2zz__intextz00
																																																																																									(BgL_ptrz00_6301,
																																																																																									BgL_bufferz00_6300,
																																																																																									BgL_arg1656z00_4166);
																																																																																							}
																																																																																							BBOOL
																																																																																								(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
																																																																																								(BgL_ptrz00_6301,
																																																																																									BgL_bufferz00_6300,
																																																																																									BgL_siza7eza7_2206,
																																																																																									BgL_siza7eza7_4162));
																																																																																				}}
																																																																																				BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																																					(BgL_bufferz00_6300,
																																																																																					BgL_ptrz00_6301,
																																																																																					BgL_siza7eza7_2206);
																																																																																				{	/* Unsafe/intext.scm 736 */
																																																																																					long
																																																																																						BgL_o1z00_4169;
																																																																																					obj_t
																																																																																						BgL_s2z00_4170;
																																																																																					long
																																																																																						BgL_o2z00_4171;
																																																																																					long
																																																																																						BgL_lz00_4172;
																																																																																					BgL_o1z00_4169
																																																																																						=
																																																																																						(long)
																																																																																						CINT
																																																																																						(BgL_offsetz00_2205);
																																																																																					BgL_s2z00_4170
																																																																																						=
																																																																																						CELL_REF
																																																																																						(BgL_bufferz00_6300);
																																																																																					BgL_o2z00_4171
																																																																																						=
																																																																																						(long)
																																																																																						CINT
																																																																																						(CELL_REF
																																																																																						(BgL_ptrz00_6301));
																																																																																					BgL_lz00_4172
																																																																																						=
																																																																																						(long)
																																																																																						CINT
																																																																																						(BgL_siza7eza7_2206);
																																																																																					blit_string
																																																																																						(
																																																																																						((obj_t) BgL_sz00_2204), BgL_o1z00_4169, BgL_s2z00_4170, BgL_o2z00_4171, BgL_lz00_4172);
																																																																																				}
																																																																																				{	/* Unsafe/intext.scm 737 */
																																																																																					obj_t
																																																																																						BgL_auxz00_6348;
																																																																																					BgL_auxz00_6348
																																																																																						=
																																																																																						ADDFX
																																																																																						(CELL_REF
																																																																																						(BgL_ptrz00_6301),
																																																																																						BgL_siza7eza7_2206);
																																																																																					return
																																																																																						CELL_SET
																																																																																						(BgL_ptrz00_6301,
																																																																																						BgL_auxz00_6348);
																																																																																				}
																																																																																			}
																																																																																		}
																																																																																	else
																																																																																		{	/* Unsafe/intext.scm 1145 */
																																																																																			if (STRUCTP(BgL_itemz00_2565))
																																																																																				{	/* Unsafe/intext.scm 1149 */
																																																																																					obj_t
																																																																																						BgL_zc3z04anonymousza31955ze3z87_6273;
																																																																																					{
																																																																																						int
																																																																																							BgL_tmpz00_8989;
																																																																																						BgL_tmpz00_8989
																																																																																							=
																																																																																							(int)
																																																																																							(14L);
																																																																																						BgL_zc3z04anonymousza31955ze3z87_6273
																																																																																							=
																																																																																							MAKE_L_PROCEDURE
																																																																																							(BGl_z62zc3z04anonymousza31955ze3ze5zz__intextz00,
																																																																																							BgL_tmpz00_8989);
																																																																																					}
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(0L),
																																																																																						BgL_tablez00_6302);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(1L),
																																																																																						((obj_t) BgL_refz00_6303));
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(2L),
																																																																																						BgL_printzd2customzd2_6304);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(3L),
																																																																																						BgL_printzd2tvectorzd2_6305);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(4L),
																																																																																						BgL_printzd2hvectorzd2_6306);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(5L),
																																																																																						BgL_printzd2vectorzd2_6307);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(6L),
																																																																																						BgL_printzd2weakptrzd2_6308);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(7L),
																																																																																						BgL_printzd2cellzd2_6309);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(8L),
																																																																																						BgL_printzd2classzd2_6310);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(9L),
																																																																																						BgL_printzd2objectzd2_6311);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(10L),
																																																																																						BgL_printzd2pairzd2_6312);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(11L),
																																																																																						BgL_printzd2epairzd2_6313);
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(12L),
																																																																																						((obj_t) BgL_bufferz00_6300));
																																																																																					PROCEDURE_L_SET
																																																																																						(BgL_zc3z04anonymousza31955ze3z87_6273,
																																																																																						(int)
																																																																																						(13L),
																																																																																						((obj_t) BgL_ptrz00_6301));
																																																																																					{
																																																																																						obj_t
																																																																																							BgL_printerz00_9024;
																																																																																						obj_t
																																																																																							BgL_itemz00_9023;
																																																																																						BgL_itemz00_9023
																																																																																							=
																																																																																							BgL_itemz00_2565;
																																																																																						BgL_printerz00_9024
																																																																																							=
																																																																																							BgL_zc3z04anonymousza31955ze3z87_6273;
																																																																																						BgL_printerz00_2294
																																																																																							=
																																																																																							BgL_printerz00_9024;
																																																																																						BgL_itemz00_2293
																																																																																							=
																																																																																							BgL_itemz00_9023;
																																																																																						goto
																																																																																							BgL_zc3z04anonymousza31718ze3z87_2295;
																																																																																					}
																																																																																				}
																																																																																			else
																																																																																				{	/* Unsafe/intext.scm 1148 */
																																																																																					if (BGL_REGEXPP(BgL_itemz00_2565))
																																																																																						{	/* Unsafe/intext.scm 1150 */
																																																																																							BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																																								(BgL_bufferz00_6300,
																																																																																								BgL_ptrz00_6301,
																																																																																								BINT
																																																																																								(1L));
																																																																																							{	/* Unsafe/intext.scm 719 */
																																																																																								obj_t
																																																																																									BgL_stringz00_5399;
																																																																																								long
																																																																																									BgL_kz00_5400;
																																																																																								BgL_stringz00_5399
																																																																																									=
																																																																																									CELL_REF
																																																																																									(BgL_bufferz00_6300);
																																																																																								BgL_kz00_5400
																																																																																									=
																																																																																									(long)
																																																																																									CINT
																																																																																									(CELL_REF
																																																																																									(BgL_ptrz00_6301));
																																																																																								STRING_SET
																																																																																									(BgL_stringz00_5399,
																																																																																									BgL_kz00_5400,
																																																																																									((unsigned char) 'r'));
																																																																																							}
																																																																																							{	/* Unsafe/intext.scm 720 */
																																																																																								obj_t
																																																																																									BgL_auxz00_6346;
																																																																																								BgL_auxz00_6346
																																																																																									=
																																																																																									ADDFX
																																																																																									(BINT
																																																																																									(1L),
																																																																																									CELL_REF
																																																																																									(BgL_ptrz00_6301));
																																																																																								CELL_SET
																																																																																									(BgL_ptrz00_6301,
																																																																																									BgL_auxz00_6346);
																																																																																							}
																																																																																							{	/* Unsafe/intext.scm 1152 */
																																																																																								obj_t
																																																																																									BgL_sz00_2663;
																																																																																								{	/* Unsafe/intext.scm 1152 */
																																																																																									obj_t
																																																																																										BgL_tmpz00_9033;
																																																																																									BgL_tmpz00_9033
																																																																																										=
																																																																																										(
																																																																																										(obj_t)
																																																																																										BgL_itemz00_2565);
																																																																																									BgL_sz00_2663
																																																																																										=
																																																																																										BGL_REGEXP_PAT
																																																																																										(BgL_tmpz00_9033);
																																																																																								}
																																																																																								{	/* Unsafe/intext.scm 1153 */
																																																																																									long
																																																																																										BgL_arg1957z00_2664;
																																																																																									BgL_arg1957z00_2664
																																																																																										=
																																																																																										STRING_LENGTH
																																																																																										(BgL_sz00_2663);
																																																																																									BGl_z62printzd2wordzb0zz__intextz00
																																																																																										(BgL_ptrz00_6301,
																																																																																										BgL_bufferz00_6300,
																																																																																										BgL_arg1957z00_2664);
																																																																																									BGl_z62checkzd2bufferz12za2zz__intextz00
																																																																																										(BgL_bufferz00_6300,
																																																																																										BgL_ptrz00_6301,
																																																																																										BINT
																																																																																										(BgL_arg1957z00_2664));
																																																																																									{	/* Unsafe/intext.scm 726 */
																																																																																										obj_t
																																																																																											BgL_s2z00_5406;
																																																																																										long
																																																																																											BgL_o2z00_5407;
																																																																																										BgL_s2z00_5406
																																																																																											=
																																																																																											CELL_REF
																																																																																											(BgL_bufferz00_6300);
																																																																																										BgL_o2z00_5407
																																																																																											=
																																																																																											(long)
																																																																																											CINT
																																																																																											(CELL_REF
																																																																																											(BgL_ptrz00_6301));
																																																																																										blit_string
																																																																																											(BgL_sz00_2663,
																																																																																											0L,
																																																																																											BgL_s2z00_5406,
																																																																																											BgL_o2z00_5407,
																																																																																											BgL_arg1957z00_2664);
																																																																																									}
																																																																																									{	/* Unsafe/intext.scm 727 */
																																																																																										obj_t
																																																																																											BgL_auxz00_6347;
																																																																																										BgL_auxz00_6347
																																																																																											=
																																																																																											ADDFX
																																																																																											(CELL_REF
																																																																																											(BgL_ptrz00_6301),
																																																																																											BINT
																																																																																											(BgL_arg1957z00_2664));
																																																																																										return
																																																																																											CELL_SET
																																																																																											(BgL_ptrz00_6301,
																																																																																											BgL_auxz00_6347);
																																																																																									}
																																																																																								}
																																																																																							}
																																																																																						}
																																																																																					else
																																																																																						{	/* Unsafe/intext.scm 1150 */
																																																																																							return
																																																																																								BGl_errorz00zz__errorz00
																																																																																								(BGl_string2597z00zz__intextz00,
																																																																																								BGl_string2661z00zz__intextz00,
																																																																																								BgL_itemz00_2565);
																																																																																						}
																																																																																				}
																																																																																		}
																																																																																}
																																																																														}
																																																																												}
																																																																										}
																																																																								}
																																																																						}
																																																																				}
																																																																		}
																																																																}
																																																														}
																																																												}
																																																										}
																																																								}
																																																						}
																																																				}
																																																		}
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			}
		}

	}



/* &print-word */
	obj_t BGl_z62printzd2wordzb0zz__intextz00(obj_t BgL_ptrz00_6355,
		obj_t BgL_bufferz00_6354, long BgL_mz00_2281)
	{
		{	/* Unsafe/intext.scm 798 */
			{	/* Unsafe/intext.scm 793 */
				long BgL_siza7eza7_4240;

				BgL_siza7eza7_4240 =
					BGl_siza7ezd2ofzd2wordza7zz__intextz00(BgL_mz00_2281);
				if ((BgL_siza7eza7_4240 == 0L))
					{	/* Unsafe/intext.scm 748 */
						unsigned char BgL_arg1656z00_4243;

						BgL_arg1656z00_4243 = (0L);
						return
							BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6355,
							BgL_bufferz00_6354, BgL_arg1656z00_4243);
					}
				else
					{	/* Unsafe/intext.scm 794 */
						{	/* Unsafe/intext.scm 748 */
							unsigned char BgL_arg1656z00_4244;

							BgL_arg1656z00_4244 = (BgL_siza7eza7_4240);
							BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6355,
								BgL_bufferz00_6354, BgL_arg1656z00_4244);
						}
						return
							BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
							(BgL_ptrz00_6355, BgL_bufferz00_6354, BINT(BgL_mz00_2281),
								BgL_siza7eza7_4240));
					}
			}
		}

	}



/* &!print-chars */
	obj_t BGl_z62z12printzd2charsza2zz__intextz00(obj_t BgL_ptrz00_6357,
		obj_t BgL_bufferz00_6356, obj_t BgL_sz00_2199, long BgL_siza7eza7_2200)
	{
		{	/* Unsafe/intext.scm 724 */
			{	/* Unsafe/intext.scm 793 */
				long BgL_siza7eza7_4147;

				BgL_siza7eza7_4147 =
					BGl_siza7ezd2ofzd2wordza7zz__intextz00(BgL_siza7eza7_2200);
				if ((BgL_siza7eza7_4147 == 0L))
					{	/* Unsafe/intext.scm 748 */
						unsigned char BgL_arg1656z00_4150;

						BgL_arg1656z00_4150 = (0L);
						BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6357,
							BgL_bufferz00_6356, BgL_arg1656z00_4150);
					}
				else
					{	/* Unsafe/intext.scm 794 */
						{	/* Unsafe/intext.scm 748 */
							unsigned char BgL_arg1656z00_4151;

							BgL_arg1656z00_4151 = (BgL_siza7eza7_4147);
							BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6357,
								BgL_bufferz00_6356, BgL_arg1656z00_4151);
						}
						BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00(BgL_ptrz00_6357,
								BgL_bufferz00_6356, BINT(BgL_siza7eza7_2200),
								BgL_siza7eza7_4147));
			}}
			BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6356,
				BgL_ptrz00_6357, BINT(BgL_siza7eza7_2200));
			{	/* Unsafe/intext.scm 726 */
				obj_t BgL_s2z00_4154;
				long BgL_o2z00_4155;

				BgL_s2z00_4154 = CELL_REF(BgL_bufferz00_6356);
				BgL_o2z00_4155 = (long) CINT(CELL_REF(BgL_ptrz00_6357));
				blit_string(
					((obj_t) BgL_sz00_2199), 0L, BgL_s2z00_4154, BgL_o2z00_4155,
					BgL_siza7eza7_2200);
			}
			{	/* Unsafe/intext.scm 727 */
				obj_t BgL_auxz00_6358;

				BgL_auxz00_6358 =
					ADDFX(CELL_REF(BgL_ptrz00_6357), BINT(BgL_siza7eza7_2200));
				return CELL_SET(BgL_ptrz00_6357, BgL_auxz00_6358);
			}
		}

	}



/* &print-epair */
	obj_t BGl_z62printzd2epairzb0zz__intextz00(obj_t BgL_envz00_6359,
		obj_t BgL_itemz00_6373, obj_t BgL_markz00_6374)
	{
		{	/* Unsafe/intext.scm 831 */
			{	/* Unsafe/intext.scm 831 */
				obj_t BgL_refz00_6360;
				obj_t BgL_printzd2customzd2_6361;
				obj_t BgL_printzd2tvectorzd2_6362;
				obj_t BgL_printzd2hvectorzd2_6363;
				obj_t BgL_printzd2vectorzd2_6364;
				obj_t BgL_printzd2weakptrzd2_6365;
				obj_t BgL_printzd2cellzd2_6366;
				obj_t BgL_printzd2classzd2_6367;
				obj_t BgL_printzd2objectzd2_6368;
				obj_t BgL_printzd2pairzd2_6369;
				obj_t BgL_bufferz00_6370;
				obj_t BgL_ptrz00_6371;
				obj_t BgL_tablez00_6372;

				BgL_refz00_6360 = PROCEDURE_L_REF(BgL_envz00_6359, (int) (0L));
				BgL_printzd2customzd2_6361 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (1L)));
				BgL_printzd2tvectorzd2_6362 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (2L)));
				BgL_printzd2hvectorzd2_6363 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (3L)));
				BgL_printzd2vectorzd2_6364 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (4L)));
				BgL_printzd2weakptrzd2_6365 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (5L)));
				BgL_printzd2cellzd2_6366 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (6L)));
				BgL_printzd2classzd2_6367 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (7L)));
				BgL_printzd2objectzd2_6368 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (8L)));
				BgL_printzd2pairzd2_6369 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6359, (int) (9L)));
				BgL_bufferz00_6370 = PROCEDURE_L_REF(BgL_envz00_6359, (int) (10L));
				BgL_ptrz00_6371 = PROCEDURE_L_REF(BgL_envz00_6359, (int) (11L));
				BgL_tablez00_6372 = PROCEDURE_L_REF(BgL_envz00_6359, (int) (12L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6370,
					BgL_ptrz00_6371, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_6799;
					long BgL_kz00_6800;

					BgL_stringz00_6799 = CELL_REF(BgL_bufferz00_6370);
					BgL_kz00_6800 = (long) CINT(CELL_REF(BgL_ptrz00_6371));
					STRING_SET(BgL_stringz00_6799, BgL_kz00_6800, ((unsigned char) '^'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_6801;

					BgL_auxz00_6801 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6371));
					CELL_SET(BgL_ptrz00_6371, BgL_auxz00_6801);
				}
				{	/* Unsafe/intext.scm 832 */
					long BgL_lenz00_6802;

					BgL_lenz00_6802 =
						BGl_markedzd2pairzd2lengthz00zz__intextz00(BgL_tablez00_6372,
						BgL_itemz00_6373);
					{	/* Unsafe/intext.scm 793 */
						long BgL_siza7eza7_6803;

						BgL_siza7eza7_6803 =
							BGl_siza7ezd2ofzd2wordza7zz__intextz00(BgL_lenz00_6802);
						if ((BgL_siza7eza7_6803 == 0L))
							{	/* Unsafe/intext.scm 794 */
								BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6371,
									BgL_bufferz00_6370, (0L));
							}
						else
							{	/* Unsafe/intext.scm 794 */
								BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6371,
									BgL_bufferz00_6370, (BgL_siza7eza7_6803));
								BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
									(BgL_ptrz00_6371, BgL_bufferz00_6370, BINT(BgL_lenz00_6802),
										BgL_siza7eza7_6803));
							}
					}
					{
						long BgL_iz00_6805;
						obj_t BgL_pz00_6806;

						BgL_iz00_6805 = 0L;
						BgL_pz00_6806 = BgL_itemz00_6373;
					BgL_loopz00_6804:
						if ((BgL_iz00_6805 == (BgL_lenz00_6802 - 1L)))
							{	/* Unsafe/intext.scm 837 */
								if (PAIRP(BgL_pz00_6806))
									{	/* Unsafe/intext.scm 838 */
										BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
											BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
											BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
											BgL_printzd2weakptrzd2_6365, BgL_printzd2vectorzd2_6364,
											BgL_printzd2hvectorzd2_6363, BgL_printzd2tvectorzd2_6362,
											BgL_printzd2customzd2_6361, BgL_refz00_6360,
											BgL_tablez00_6372, BgL_ptrz00_6371, BgL_bufferz00_6370,
											CAR(BgL_pz00_6806));
										if (EPAIRP(BgL_pz00_6806))
											{	/* Unsafe/intext.scm 841 */
												BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
													BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
													BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
													BgL_printzd2weakptrzd2_6365,
													BgL_printzd2vectorzd2_6364,
													BgL_printzd2hvectorzd2_6363,
													BgL_printzd2tvectorzd2_6362,
													BgL_printzd2customzd2_6361, BgL_refz00_6360,
													BgL_tablez00_6372, BgL_ptrz00_6371,
													BgL_bufferz00_6370, CER(BgL_pz00_6806));
											}
										else
											{	/* Unsafe/intext.scm 841 */
												BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
													BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
													BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
													BgL_printzd2weakptrzd2_6365,
													BgL_printzd2vectorzd2_6364,
													BgL_printzd2hvectorzd2_6363,
													BgL_printzd2tvectorzd2_6362,
													BgL_printzd2customzd2_6361, BgL_refz00_6360,
													BgL_tablez00_6372, BgL_ptrz00_6371,
													BgL_bufferz00_6370, BUNSPEC);
											}
										return
											BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
											BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
											BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
											BgL_printzd2weakptrzd2_6365, BgL_printzd2vectorzd2_6364,
											BgL_printzd2hvectorzd2_6363, BgL_printzd2tvectorzd2_6362,
											BgL_printzd2customzd2_6361, BgL_refz00_6360,
											BgL_tablez00_6372, BgL_ptrz00_6371, BgL_bufferz00_6370,
											BNIL);
									}
								else
									{	/* Unsafe/intext.scm 838 */
										return
											BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
											BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
											BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
											BgL_printzd2weakptrzd2_6365, BgL_printzd2vectorzd2_6364,
											BgL_printzd2hvectorzd2_6363, BgL_printzd2tvectorzd2_6362,
											BgL_printzd2customzd2_6361, BgL_refz00_6360,
											BgL_tablez00_6372, BgL_ptrz00_6371, BgL_bufferz00_6370,
											BgL_pz00_6806);
									}
							}
						else
							{	/* Unsafe/intext.scm 837 */
								{	/* Unsafe/intext.scm 847 */
									obj_t BgL_arg1734z00_6807;

									BgL_arg1734z00_6807 = CAR(((obj_t) BgL_pz00_6806));
									BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
										BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
										BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
										BgL_printzd2weakptrzd2_6365, BgL_printzd2vectorzd2_6364,
										BgL_printzd2hvectorzd2_6363, BgL_printzd2tvectorzd2_6362,
										BgL_printzd2customzd2_6361, BgL_refz00_6360,
										BgL_tablez00_6372, BgL_ptrz00_6371, BgL_bufferz00_6370,
										BgL_arg1734z00_6807);
								}
								if (EPAIRP(BgL_pz00_6806))
									{	/* Unsafe/intext.scm 849 */
										obj_t BgL_arg1736z00_6808;

										BgL_arg1736z00_6808 = CER(((obj_t) BgL_pz00_6806));
										BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
											BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
											BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
											BgL_printzd2weakptrzd2_6365, BgL_printzd2vectorzd2_6364,
											BgL_printzd2hvectorzd2_6363, BgL_printzd2tvectorzd2_6362,
											BgL_printzd2customzd2_6361, BgL_refz00_6360,
											BgL_tablez00_6372, BgL_ptrz00_6371, BgL_bufferz00_6370,
											BgL_arg1736z00_6808);
									}
								else
									{	/* Unsafe/intext.scm 848 */
										BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
											BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
											BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
											BgL_printzd2weakptrzd2_6365, BgL_printzd2vectorzd2_6364,
											BgL_printzd2hvectorzd2_6363, BgL_printzd2tvectorzd2_6362,
											BgL_printzd2customzd2_6361, BgL_refz00_6360,
											BgL_tablez00_6372, BgL_ptrz00_6371, BgL_bufferz00_6370,
											BUNSPEC);
									}
								{	/* Unsafe/intext.scm 851 */
									obj_t BgL_vcdrz00_6809;

									BgL_vcdrz00_6809 = CDR(((obj_t) BgL_pz00_6806));
									{	/* Unsafe/intext.scm 852 */
										bool_t BgL_test2931z00_9149;

										if (PAIRP(BgL_vcdrz00_6809))
											{	/* Unsafe/intext.scm 853 */
												obj_t BgL_markz00_6810;

												BgL_markz00_6810 =
													BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6372,
													BgL_vcdrz00_6809);
												{	/* Unsafe/intext.scm 854 */
													bool_t BgL__ortest_1070z00_6811;

													{	/* Unsafe/intext.scm 854 */
														obj_t BgL_a1159z00_6812;

														BgL_a1159z00_6812 =
															STRUCT_REF(
															((obj_t) BgL_markz00_6810), (int) (2L));
														{	/* Unsafe/intext.scm 854 */

															if (INTEGERP(BgL_a1159z00_6812))
																{	/* Unsafe/intext.scm 854 */
																	BgL__ortest_1070z00_6811 =
																		((long) CINT(BgL_a1159z00_6812) > 0L);
																}
															else
																{	/* Unsafe/intext.scm 854 */
																	BgL__ortest_1070z00_6811 =
																		BGl_2ze3ze3zz__r4_numbers_6_5z00
																		(BgL_a1159z00_6812, BINT(0L));
																}
														}
													}
													if (BgL__ortest_1070z00_6811)
														{	/* Unsafe/intext.scm 854 */
															BgL_test2931z00_9149 = BgL__ortest_1070z00_6811;
														}
													else
														{	/* Unsafe/intext.scm 854 */
															BgL_test2931z00_9149 =
																(
																(long) CINT(STRUCT_REF(
																		((obj_t) BgL_markz00_6810),
																		(int) (3L))) >= 0L);
											}}}
										else
											{	/* Unsafe/intext.scm 852 */
												BgL_test2931z00_9149 = ((bool_t) 0);
											}
										if (BgL_test2931z00_9149)
											{	/* Unsafe/intext.scm 852 */
												return
													BGl_z62printzd2itemzb0zz__intextz00(BgL_envz00_6359,
													BgL_printzd2pairzd2_6369, BgL_printzd2objectzd2_6368,
													BgL_printzd2classzd2_6367, BgL_printzd2cellzd2_6366,
													BgL_printzd2weakptrzd2_6365,
													BgL_printzd2vectorzd2_6364,
													BgL_printzd2hvectorzd2_6363,
													BgL_printzd2tvectorzd2_6362,
													BgL_printzd2customzd2_6361, BgL_refz00_6360,
													BgL_tablez00_6372, BgL_ptrz00_6371,
													BgL_bufferz00_6370, BgL_vcdrz00_6809);
											}
										else
											{
												obj_t BgL_pz00_9171;
												long BgL_iz00_9169;

												BgL_iz00_9169 = (BgL_iz00_6805 + 1L);
												BgL_pz00_9171 = BgL_vcdrz00_6809;
												BgL_pz00_6806 = BgL_pz00_9171;
												BgL_iz00_6805 = BgL_iz00_9169;
												goto BgL_loopz00_6804;
											}
									}
								}
							}
					}
				}
			}
		}

	}



/* &print-pair */
	obj_t BGl_z62printzd2pairzb0zz__intextz00(obj_t BgL_envz00_6376,
		obj_t BgL_itemz00_6390, obj_t BgL_markz00_6391)
	{
		{	/* Unsafe/intext.scm 861 */
			{	/* Unsafe/intext.scm 861 */
				obj_t BgL_refz00_6377;
				obj_t BgL_printzd2customzd2_6378;
				obj_t BgL_printzd2tvectorzd2_6379;
				obj_t BgL_printzd2hvectorzd2_6380;
				obj_t BgL_printzd2vectorzd2_6381;
				obj_t BgL_printzd2weakptrzd2_6382;
				obj_t BgL_printzd2cellzd2_6383;
				obj_t BgL_printzd2classzd2_6384;
				obj_t BgL_printzd2objectzd2_6385;
				obj_t BgL_printzd2epairzd2_6386;
				obj_t BgL_bufferz00_6387;
				obj_t BgL_ptrz00_6388;
				obj_t BgL_tablez00_6389;

				BgL_refz00_6377 = PROCEDURE_L_REF(BgL_envz00_6376, (int) (0L));
				BgL_printzd2customzd2_6378 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (1L)));
				BgL_printzd2tvectorzd2_6379 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (2L)));
				BgL_printzd2hvectorzd2_6380 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (3L)));
				BgL_printzd2vectorzd2_6381 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (4L)));
				BgL_printzd2weakptrzd2_6382 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (5L)));
				BgL_printzd2cellzd2_6383 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (6L)));
				BgL_printzd2classzd2_6384 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (7L)));
				BgL_printzd2objectzd2_6385 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (8L)));
				BgL_printzd2epairzd2_6386 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6376, (int) (9L)));
				BgL_bufferz00_6387 = PROCEDURE_L_REF(BgL_envz00_6376, (int) (10L));
				BgL_ptrz00_6388 = PROCEDURE_L_REF(BgL_envz00_6376, (int) (11L));
				BgL_tablez00_6389 = PROCEDURE_L_REF(BgL_envz00_6376, (int) (12L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6387,
					BgL_ptrz00_6388, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_6813;
					long BgL_kz00_6814;

					BgL_stringz00_6813 = CELL_REF(BgL_bufferz00_6387);
					BgL_kz00_6814 = (long) CINT(CELL_REF(BgL_ptrz00_6388));
					STRING_SET(BgL_stringz00_6813, BgL_kz00_6814, ((unsigned char) '('));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_6815;

					BgL_auxz00_6815 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6388));
					CELL_SET(BgL_ptrz00_6388, BgL_auxz00_6815);
				}
				{	/* Unsafe/intext.scm 862 */
					long BgL_lenz00_6816;

					BgL_lenz00_6816 =
						BGl_markedzd2pairzd2lengthz00zz__intextz00(BgL_tablez00_6389,
						BgL_itemz00_6390);
					{	/* Unsafe/intext.scm 793 */
						long BgL_siza7eza7_6817;

						BgL_siza7eza7_6817 =
							BGl_siza7ezd2ofzd2wordza7zz__intextz00(BgL_lenz00_6816);
						if ((BgL_siza7eza7_6817 == 0L))
							{	/* Unsafe/intext.scm 794 */
								BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6388,
									BgL_bufferz00_6387, (0L));
							}
						else
							{	/* Unsafe/intext.scm 794 */
								BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6388,
									BgL_bufferz00_6387, (BgL_siza7eza7_6817));
								BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
									(BgL_ptrz00_6388, BgL_bufferz00_6387, BINT(BgL_lenz00_6816),
										BgL_siza7eza7_6817));
							}
					}
					{
						long BgL_iz00_6819;
						obj_t BgL_pz00_6820;

						BgL_iz00_6819 = 0L;
						BgL_pz00_6820 = BgL_itemz00_6390;
					BgL_loopz00_6818:
						if ((BgL_iz00_6819 == (BgL_lenz00_6816 - 1L)))
							{	/* Unsafe/intext.scm 867 */
								if (PAIRP(BgL_pz00_6820))
									{	/* Unsafe/intext.scm 868 */
										BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6386, BgL_envz00_6376,
											BgL_printzd2objectzd2_6385, BgL_printzd2classzd2_6384,
											BgL_printzd2cellzd2_6383, BgL_printzd2weakptrzd2_6382,
											BgL_printzd2vectorzd2_6381, BgL_printzd2hvectorzd2_6380,
											BgL_printzd2tvectorzd2_6379, BgL_printzd2customzd2_6378,
											BgL_refz00_6377, BgL_tablez00_6389, BgL_ptrz00_6388,
											BgL_bufferz00_6387, CAR(BgL_pz00_6820));
										return
											BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6386, BgL_envz00_6376,
											BgL_printzd2objectzd2_6385, BgL_printzd2classzd2_6384,
											BgL_printzd2cellzd2_6383, BgL_printzd2weakptrzd2_6382,
											BgL_printzd2vectorzd2_6381, BgL_printzd2hvectorzd2_6380,
											BgL_printzd2tvectorzd2_6379, BgL_printzd2customzd2_6378,
											BgL_refz00_6377, BgL_tablez00_6389, BgL_ptrz00_6388,
											BgL_bufferz00_6387, BNIL);
									}
								else
									{	/* Unsafe/intext.scm 868 */
										return
											BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6386, BgL_envz00_6376,
											BgL_printzd2objectzd2_6385, BgL_printzd2classzd2_6384,
											BgL_printzd2cellzd2_6383, BgL_printzd2weakptrzd2_6382,
											BgL_printzd2vectorzd2_6381, BgL_printzd2hvectorzd2_6380,
											BgL_printzd2tvectorzd2_6379, BgL_printzd2customzd2_6378,
											BgL_refz00_6377, BgL_tablez00_6389, BgL_ptrz00_6388,
											BgL_bufferz00_6387, BgL_pz00_6820);
									}
							}
						else
							{	/* Unsafe/intext.scm 867 */
								{	/* Unsafe/intext.scm 874 */
									obj_t BgL_arg1748z00_6821;

									BgL_arg1748z00_6821 = CAR(((obj_t) BgL_pz00_6820));
									BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6386,
										BgL_envz00_6376, BgL_printzd2objectzd2_6385,
										BgL_printzd2classzd2_6384, BgL_printzd2cellzd2_6383,
										BgL_printzd2weakptrzd2_6382, BgL_printzd2vectorzd2_6381,
										BgL_printzd2hvectorzd2_6380, BgL_printzd2tvectorzd2_6379,
										BgL_printzd2customzd2_6378, BgL_refz00_6377,
										BgL_tablez00_6389, BgL_ptrz00_6388, BgL_bufferz00_6387,
										BgL_arg1748z00_6821);
								}
								{	/* Unsafe/intext.scm 875 */
									obj_t BgL_vcdrz00_6822;

									BgL_vcdrz00_6822 = CDR(((obj_t) BgL_pz00_6820));
									{	/* Unsafe/intext.scm 876 */
										bool_t BgL_test2938z00_9238;

										if (PAIRP(BgL_vcdrz00_6822))
											{	/* Unsafe/intext.scm 877 */
												obj_t BgL_markz00_6823;

												BgL_markz00_6823 =
													BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6389,
													BgL_vcdrz00_6822);
												{	/* Unsafe/intext.scm 878 */
													bool_t BgL__ortest_1071z00_6824;

													BgL__ortest_1071z00_6824 =
														(
														(long) CINT(STRUCT_REF(
																((obj_t) BgL_markz00_6823), (int) (2L))) > 0L);
													if (BgL__ortest_1071z00_6824)
														{	/* Unsafe/intext.scm 878 */
															BgL_test2938z00_9238 = BgL__ortest_1071z00_6824;
														}
													else
														{	/* Unsafe/intext.scm 878 */
															BgL_test2938z00_9238 =
																(
																(long) CINT(STRUCT_REF(
																		((obj_t) BgL_markz00_6823),
																		(int) (3L))) >= 0L);
											}}}
										else
											{	/* Unsafe/intext.scm 876 */
												BgL_test2938z00_9238 = ((bool_t) 0);
											}
										if (BgL_test2938z00_9238)
											{	/* Unsafe/intext.scm 876 */
												return
													BGl_z62printzd2itemzb0zz__intextz00
													(BgL_printzd2epairzd2_6386, BgL_envz00_6376,
													BgL_printzd2objectzd2_6385, BgL_printzd2classzd2_6384,
													BgL_printzd2cellzd2_6383, BgL_printzd2weakptrzd2_6382,
													BgL_printzd2vectorzd2_6381,
													BgL_printzd2hvectorzd2_6380,
													BgL_printzd2tvectorzd2_6379,
													BgL_printzd2customzd2_6378, BgL_refz00_6377,
													BgL_tablez00_6389, BgL_ptrz00_6388,
													BgL_bufferz00_6387, BgL_vcdrz00_6822);
											}
										else
											{
												obj_t BgL_pz00_9256;
												long BgL_iz00_9254;

												BgL_iz00_9254 = (BgL_iz00_6819 + 1L);
												BgL_pz00_9256 = BgL_vcdrz00_6822;
												BgL_pz00_6820 = BgL_pz00_9256;
												BgL_iz00_6819 = BgL_iz00_9254;
												goto BgL_loopz00_6818;
											}
									}
								}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1888> */
	obj_t BGl_z62zc3z04anonymousza31888ze3ze5zz__intextz00(obj_t BgL_envz00_6393,
		obj_t BgL_itemz00_6396, obj_t BgL_markz00_6397)
	{
		{	/* Unsafe/intext.scm 1056 */
			{	/* Unsafe/intext.scm 810 */
				obj_t BgL_bufferz00_6394;
				obj_t BgL_ptrz00_6395;

				BgL_bufferz00_6394 = PROCEDURE_L_REF(BgL_envz00_6393, (int) (0L));
				BgL_ptrz00_6395 = PROCEDURE_L_REF(BgL_envz00_6393, (int) (1L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6394,
					BgL_ptrz00_6395, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_6825;
					long BgL_kz00_6826;

					BgL_stringz00_6825 = CELL_REF(BgL_bufferz00_6394);
					BgL_kz00_6826 = (long) CINT(CELL_REF(BgL_ptrz00_6395));
					STRING_SET(BgL_stringz00_6825, BgL_kz00_6826, ((unsigned char) '"'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_6827;

					BgL_auxz00_6827 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6395));
					CELL_SET(BgL_ptrz00_6395, BgL_auxz00_6827);
				}
				{	/* Unsafe/intext.scm 811 */
					long BgL_arg1717z00_6828;

					BgL_arg1717z00_6828 = STRING_LENGTH(((obj_t) BgL_itemz00_6396));
					return
						BGl_z62z12printzd2charsza2zz__intextz00(BgL_ptrz00_6395,
						BgL_bufferz00_6394, BgL_itemz00_6396, BgL_arg1717z00_6828);
				}
			}
		}

	}



/* &print-object */
	obj_t BGl_z62printzd2objectzb0zz__intextz00(obj_t BgL_envz00_6399,
		obj_t BgL_itemz00_6413, obj_t BgL_markz00_6414)
	{
		{	/* Unsafe/intext.scm 898 */
			{	/* Unsafe/intext.scm 891 */
				obj_t BgL_tablez00_6400;
				obj_t BgL_refz00_6401;
				obj_t BgL_printzd2customzd2_6402;
				obj_t BgL_printzd2tvectorzd2_6403;
				obj_t BgL_printzd2hvectorzd2_6404;
				obj_t BgL_printzd2vectorzd2_6405;
				obj_t BgL_printzd2weakptrzd2_6406;
				obj_t BgL_printzd2cellzd2_6407;
				obj_t BgL_printzd2classzd2_6408;
				obj_t BgL_printzd2pairzd2_6409;
				obj_t BgL_printzd2epairzd2_6410;
				obj_t BgL_bufferz00_6411;
				obj_t BgL_ptrz00_6412;

				BgL_tablez00_6400 = PROCEDURE_L_REF(BgL_envz00_6399, (int) (0L));
				BgL_refz00_6401 = PROCEDURE_L_REF(BgL_envz00_6399, (int) (1L));
				BgL_printzd2customzd2_6402 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (2L)));
				BgL_printzd2tvectorzd2_6403 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (3L)));
				BgL_printzd2hvectorzd2_6404 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (4L)));
				BgL_printzd2vectorzd2_6405 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (5L)));
				BgL_printzd2weakptrzd2_6406 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (6L)));
				BgL_printzd2cellzd2_6407 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (7L)));
				BgL_printzd2classzd2_6408 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (8L)));
				BgL_printzd2pairzd2_6409 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (9L)));
				BgL_printzd2epairzd2_6410 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6399, (int) (10L)));
				BgL_bufferz00_6411 = PROCEDURE_L_REF(BgL_envz00_6399, (int) (11L));
				BgL_ptrz00_6412 = PROCEDURE_L_REF(BgL_envz00_6399, (int) (12L));
				{
					obj_t BgL_itemz00_6838;
					obj_t BgL_oz00_6839;
					long BgL_iz00_6831;

					{	/* Unsafe/intext.scm 891 */
						obj_t BgL_vz00_6868;

						BgL_vz00_6868 = STRUCT_REF(((obj_t) BgL_markz00_6414), (int) (1L));
						if ((BgL_itemz00_6413 == BgL_vz00_6868))
							{	/* Unsafe/intext.scm 893 */
								BgL_itemz00_6838 = BgL_itemz00_6413;
								BgL_oz00_6839 = BgL_vz00_6868;
								{	/* Unsafe/intext.scm 902 */
									obj_t BgL_klassz00_6840;

									{	/* Unsafe/intext.scm 902 */
										obj_t BgL_arg2495z00_6841;
										long BgL_arg2497z00_6842;

										BgL_arg2495z00_6841 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Unsafe/intext.scm 902 */
											long BgL_arg2500z00_6843;

											BgL_arg2500z00_6843 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_itemz00_6838));
											BgL_arg2497z00_6842 = (BgL_arg2500z00_6843 - OBJECT_TYPE);
										}
										BgL_klassz00_6840 =
											VECTOR_REF(BgL_arg2495z00_6841, BgL_arg2497z00_6842);
									}
									{	/* Unsafe/intext.scm 902 */
										obj_t BgL_fieldsz00_6844;

										{	/* Unsafe/intext.scm 903 */
											obj_t BgL_tmpz00_9315;

											BgL_tmpz00_9315 = ((obj_t) BgL_klassz00_6840);
											BgL_fieldsz00_6844 =
												BGL_CLASS_ALL_FIELDS(BgL_tmpz00_9315);
										}
										{	/* Unsafe/intext.scm 904 */

											BGl_z62checkzd2bufferz12za2zz__intextz00
												(BgL_bufferz00_6411, BgL_ptrz00_6412, BINT(1L));
											{	/* Unsafe/intext.scm 719 */
												obj_t BgL_stringz00_6845;
												long BgL_kz00_6846;

												BgL_stringz00_6845 = CELL_REF(BgL_bufferz00_6411);
												BgL_kz00_6846 = (long) CINT(CELL_REF(BgL_ptrz00_6412));
												STRING_SET(BgL_stringz00_6845, BgL_kz00_6846,
													((unsigned char) '|'));
											}
											{	/* Unsafe/intext.scm 720 */
												obj_t BgL_auxz00_6847;

												BgL_auxz00_6847 =
													ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
												CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6847);
											}
											BGl_z62printzd2itemzb0zz__intextz00
												(BgL_printzd2epairzd2_6410, BgL_printzd2pairzd2_6409,
												BgL_envz00_6399, BgL_printzd2classzd2_6408,
												BgL_printzd2cellzd2_6407, BgL_printzd2weakptrzd2_6406,
												BgL_printzd2vectorzd2_6405, BgL_printzd2hvectorzd2_6404,
												BgL_printzd2tvectorzd2_6403, BgL_printzd2customzd2_6402,
												BgL_refz00_6401, BgL_tablez00_6400, BgL_ptrz00_6412,
												BgL_bufferz00_6411,
												BGl_classzd2namezd2zz__objectz00(BgL_klassz00_6840));
											{	/* Unsafe/intext.scm 907 */
												long BgL_arg1765z00_6848;

												BgL_arg1765z00_6848 =
													(1L + VECTOR_LENGTH(BgL_fieldsz00_6844));
												{	/* Unsafe/intext.scm 793 */
													long BgL_siza7eza7_6849;

													BgL_siza7eza7_6849 =
														BGl_siza7ezd2ofzd2wordza7zz__intextz00
														(BgL_arg1765z00_6848);
													if ((BgL_siza7eza7_6849 == 0L))
														{	/* Unsafe/intext.scm 794 */
															BGl_z62z12printzd2markupza2zz__intextz00
																(BgL_ptrz00_6412, BgL_bufferz00_6411, (0L));
														}
													else
														{	/* Unsafe/intext.scm 794 */
															BGl_z62z12printzd2markupza2zz__intextz00
																(BgL_ptrz00_6412, BgL_bufferz00_6411,
																(BgL_siza7eza7_6849));
															BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
																(BgL_ptrz00_6412, BgL_bufferz00_6411,
																	BINT(BgL_arg1765z00_6848),
																	BgL_siza7eza7_6849));
														}
												}
											}
											BGl_z62printzd2itemzb0zz__intextz00
												(BgL_printzd2epairzd2_6410, BgL_printzd2pairzd2_6409,
												BgL_envz00_6399, BgL_printzd2classzd2_6408,
												BgL_printzd2cellzd2_6407, BgL_printzd2weakptrzd2_6406,
												BgL_printzd2vectorzd2_6405, BgL_printzd2hvectorzd2_6404,
												BgL_printzd2tvectorzd2_6403, BgL_printzd2customzd2_6402,
												BgL_refz00_6401, BgL_tablez00_6400, BgL_ptrz00_6412,
												BgL_bufferz00_6411, BgL_klassz00_6840);
											{
												long BgL_iz00_6851;

												BgL_iz00_6851 = 0L;
											BgL_for1072z00_6850:
												if ((BgL_iz00_6851 < VECTOR_LENGTH(BgL_fieldsz00_6844)))
													{	/* Unsafe/intext.scm 909 */
														{	/* Unsafe/intext.scm 910 */
															obj_t BgL_fz00_6852;

															BgL_fz00_6852 =
																VECTOR_REF(BgL_fieldsz00_6844, BgL_iz00_6851);
															{	/* Unsafe/intext.scm 910 */
																obj_t BgL_ivz00_6853;

																BgL_ivz00_6853 =
																	BGl_classzd2fieldzd2infoz00zz__objectz00
																	(BgL_fz00_6852);
																{	/* Unsafe/intext.scm 911 */

																	{	/* Unsafe/intext.scm 913 */
																		obj_t BgL_g1074z00_6854;

																		if (PAIRP(BgL_ivz00_6853))
																			{	/* Unsafe/intext.scm 913 */
																				BgL_g1074z00_6854 =
																					BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																					(BGl_keyword2662z00zz__intextz00,
																					BgL_ivz00_6853);
																			}
																		else
																			{	/* Unsafe/intext.scm 913 */
																				BgL_g1074z00_6854 = BFALSE;
																			}
																		if (CBOOL(BgL_g1074z00_6854))
																			{	/* Unsafe/intext.scm 918 */
																				obj_t BgL_arg1768z00_6855;

																				{	/* Unsafe/intext.scm 918 */
																					bool_t BgL_test2946z00_9349;

																					{	/* Unsafe/intext.scm 918 */
																						obj_t BgL_tmpz00_9350;

																						BgL_tmpz00_9350 =
																							CDR(((obj_t) BgL_g1074z00_6854));
																						BgL_test2946z00_9349 =
																							PAIRP(BgL_tmpz00_9350);
																					}
																					if (BgL_test2946z00_9349)
																						{	/* Unsafe/intext.scm 919 */
																							obj_t BgL_pairz00_6856;

																							BgL_pairz00_6856 =
																								CDR(
																								((obj_t) BgL_g1074z00_6854));
																							BgL_arg1768z00_6855 =
																								CAR(BgL_pairz00_6856);
																						}
																					else
																						{	/* Unsafe/intext.scm 918 */
																							if (BGl_classzd2fieldzd2defaultzd2valuezf3z21zz__objectz00(BgL_fz00_6852))
																								{	/* Unsafe/intext.scm 920 */
																									BgL_arg1768z00_6855 =
																										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																										(BgL_fz00_6852);
																								}
																							else
																								{	/* Unsafe/intext.scm 922 */
																									bool_t BgL_test2948z00_9360;

																									{	/* Unsafe/intext.scm 922 */
																										obj_t BgL_arg1779z00_6857;

																										BgL_arg1779z00_6857 =
																											BGl_classzd2fieldzd2typez00zz__objectz00
																											(BgL_fz00_6852);
																										BgL_test2948z00_9360 =
																											(BgL_arg1779z00_6857 ==
																											BGl_symbol2664z00zz__intextz00);
																									}
																									if (BgL_test2948z00_9360)
																										{	/* Unsafe/intext.scm 922 */
																											BgL_arg1768z00_6855 =
																												BFALSE;
																										}
																									else
																										{	/* Unsafe/intext.scm 926 */
																											obj_t BgL_arg1774z00_6858;
																											obj_t BgL_arg1775z00_6859;

																											{	/* Unsafe/intext.scm 926 */
																												obj_t
																													BgL_arg1777z00_6860;
																												BgL_arg1777z00_6860 =
																													BGl_classzd2fieldzd2typez00zz__objectz00
																													(BgL_fz00_6852);
																												{	/* Unsafe/intext.scm 924 */
																													obj_t
																														BgL_list1778z00_6861;
																													BgL_list1778z00_6861 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1777z00_6860,
																														BNIL);
																													BgL_arg1774z00_6858 =
																														BGl_formatz00zz__r4_output_6_10_3z00
																														(BGl_string2666z00zz__intextz00,
																														BgL_list1778z00_6861);
																												}
																											}
																											BgL_arg1775z00_6859 =
																												BGl_classzd2fieldzd2namez00zz__objectz00
																												(BgL_fz00_6852);
																											BgL_arg1768z00_6855 =
																												BGl_errorz00zz__errorz00
																												(BGl_string2597z00zz__intextz00,
																												BgL_arg1774z00_6858,
																												BgL_arg1775z00_6859);
																										}
																								}
																						}
																				}
																				BGl_z62printzd2itemzb0zz__intextz00
																					(BgL_printzd2epairzd2_6410,
																					BgL_printzd2pairzd2_6409,
																					BgL_envz00_6399,
																					BgL_printzd2classzd2_6408,
																					BgL_printzd2cellzd2_6407,
																					BgL_printzd2weakptrzd2_6406,
																					BgL_printzd2vectorzd2_6405,
																					BgL_printzd2hvectorzd2_6404,
																					BgL_printzd2tvectorzd2_6403,
																					BgL_printzd2customzd2_6402,
																					BgL_refz00_6401, BgL_tablez00_6400,
																					BgL_ptrz00_6412, BgL_bufferz00_6411,
																					BgL_arg1768z00_6855);
																			}
																		else
																			{	/* Unsafe/intext.scm 931 */
																				obj_t BgL_arg1782z00_6862;

																				{	/* Unsafe/intext.scm 931 */
																					obj_t BgL_fun1783z00_6863;

																					BgL_fun1783z00_6863 =
																						BGl_classzd2fieldzd2accessorz00zz__objectz00
																						(BgL_fz00_6852);
																					BgL_arg1782z00_6862 =
																						BGL_PROCEDURE_CALL1
																						(BgL_fun1783z00_6863,
																						BgL_itemz00_6838);
																				}
																				BGl_z62printzd2itemzb0zz__intextz00
																					(BgL_printzd2epairzd2_6410,
																					BgL_printzd2pairzd2_6409,
																					BgL_envz00_6399,
																					BgL_printzd2classzd2_6408,
																					BgL_printzd2cellzd2_6407,
																					BgL_printzd2weakptrzd2_6406,
																					BgL_printzd2vectorzd2_6405,
																					BgL_printzd2hvectorzd2_6404,
																					BgL_printzd2tvectorzd2_6403,
																					BgL_printzd2customzd2_6402,
																					BgL_refz00_6401, BgL_tablez00_6400,
																					BgL_ptrz00_6412, BgL_bufferz00_6411,
																					BgL_arg1782z00_6862);
																			}
																	}
																}
															}
														}
														{
															long BgL_iz00_9375;

															BgL_iz00_9375 = (BgL_iz00_6851 + 1L);
															BgL_iz00_6851 = BgL_iz00_9375;
															goto BgL_for1072z00_6850;
														}
													}
												else
													{	/* Unsafe/intext.scm 909 */
														((bool_t) 0);
													}
											}
											{	/* Unsafe/intext.scm 932 */
												long BgL_arg1786z00_6864;

												BgL_arg1786z00_6864 =
													BGl_classzd2hashzd2zz__objectz00(BgL_klassz00_6840);
												if ((BgL_arg1786z00_6864 < 0L))
													{	/* Unsafe/intext.scm 802 */
														BGl_z62checkzd2bufferz12za2zz__intextz00
															(BgL_bufferz00_6411, BgL_ptrz00_6412, BINT(1L));
														{	/* Unsafe/intext.scm 719 */
															obj_t BgL_stringz00_6865;
															long BgL_kz00_6866;

															BgL_stringz00_6865 = CELL_REF(BgL_bufferz00_6411);
															BgL_kz00_6866 =
																(long) CINT(CELL_REF(BgL_ptrz00_6412));
															STRING_SET(BgL_stringz00_6865, BgL_kz00_6866,
																((unsigned char) '-'));
														}
														{	/* Unsafe/intext.scm 720 */
															obj_t BgL_auxz00_6867;

															BgL_auxz00_6867 =
																ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
															CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6867);
														}
														return
															BGl_z62printzd2wordzb0zz__intextz00
															(BgL_ptrz00_6412, BgL_bufferz00_6411,
															NEG(BgL_arg1786z00_6864));
													}
												else
													{	/* Unsafe/intext.scm 802 */
														return
															BGl_z62printzd2wordzb0zz__intextz00
															(BgL_ptrz00_6412, BgL_bufferz00_6411,
															BgL_arg1786z00_6864);
													}
											}
										}
									}
								}
							}
						else
							{	/* Unsafe/intext.scm 895 */
								bool_t BgL_test2950z00_9389;

								{	/* Unsafe/intext.scm 895 */
									int BgL_arg1762z00_6869;

									BgL_arg1762z00_6869 = bgl_debug();
									BgL_test2950z00_9389 = ((long) (BgL_arg1762z00_6869) >= 1L);
								}
								if (BgL_test2950z00_9389)
									{	/* Unsafe/intext.scm 895 */
										BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6411,
											BgL_ptrz00_6412, BINT(1L));
										{	/* Unsafe/intext.scm 719 */
											obj_t BgL_stringz00_6870;
											long BgL_kz00_6871;

											BgL_stringz00_6870 = CELL_REF(BgL_bufferz00_6411);
											BgL_kz00_6871 = (long) CINT(CELL_REF(BgL_ptrz00_6412));
											STRING_SET(BgL_stringz00_6870, BgL_kz00_6871,
												((unsigned char) 'X'));
										}
										{	/* Unsafe/intext.scm 720 */
											obj_t BgL_auxz00_6872;

											BgL_auxz00_6872 =
												ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
											CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6872);
										}
										BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6411,
											BgL_ptrz00_6412, BINT(1L));
										{	/* Unsafe/intext.scm 719 */
											obj_t BgL_stringz00_6873;
											long BgL_kz00_6874;

											BgL_stringz00_6873 = CELL_REF(BgL_bufferz00_6411);
											BgL_kz00_6874 = (long) CINT(CELL_REF(BgL_ptrz00_6412));
											STRING_SET(BgL_stringz00_6873, BgL_kz00_6874,
												((unsigned char) 'G'));
										}
										{	/* Unsafe/intext.scm 720 */
											obj_t BgL_auxz00_6875;

											BgL_auxz00_6875 =
												ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
											CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6875);
										}
										{	/* Unsafe/intext.scm 949 */
											obj_t BgL_arg1791z00_6876;

											{	/* Unsafe/intext.scm 949 */
												obj_t BgL_arg1792z00_6877;

												{	/* Unsafe/intext.scm 949 */
													obj_t BgL_arg2495z00_6878;
													long BgL_arg2497z00_6879;

													BgL_arg2495z00_6878 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Unsafe/intext.scm 949 */
														long BgL_arg2500z00_6880;

														BgL_arg2500z00_6880 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_itemz00_6413));
														BgL_arg2497z00_6879 =
															(BgL_arg2500z00_6880 - OBJECT_TYPE);
													}
													BgL_arg1792z00_6877 =
														VECTOR_REF(BgL_arg2495z00_6878,
														BgL_arg2497z00_6879);
												}
												BgL_arg1791z00_6876 =
													BGl_classzd2namezd2zz__objectz00(BgL_arg1792z00_6877);
											}
											BGl_z62printzd2itemzb0zz__intextz00
												(BgL_printzd2epairzd2_6410, BgL_printzd2pairzd2_6409,
												BgL_envz00_6399, BgL_printzd2classzd2_6408,
												BgL_printzd2cellzd2_6407, BgL_printzd2weakptrzd2_6406,
												BgL_printzd2vectorzd2_6405, BgL_printzd2hvectorzd2_6404,
												BgL_printzd2tvectorzd2_6403, BgL_printzd2customzd2_6402,
												BgL_refz00_6401, BgL_tablez00_6400, BgL_ptrz00_6412,
												BgL_bufferz00_6411, BgL_arg1791z00_6876);
										}
										BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6410, BgL_printzd2pairzd2_6409,
											BgL_envz00_6399, BgL_printzd2classzd2_6408,
											BgL_printzd2cellzd2_6407, BgL_printzd2weakptrzd2_6406,
											BgL_printzd2vectorzd2_6405, BgL_printzd2hvectorzd2_6404,
											BgL_printzd2tvectorzd2_6403, BgL_printzd2customzd2_6402,
											BgL_refz00_6401, BgL_tablez00_6400, BgL_ptrz00_6412,
											BgL_bufferz00_6411, BgL_vz00_6868);
										{	/* Unsafe/intext.scm 951 */
											long BgL_arg1793z00_6881;

											{	/* Unsafe/intext.scm 951 */
												obj_t BgL_arg1794z00_6882;

												{	/* Unsafe/intext.scm 951 */
													obj_t BgL_arg2495z00_6883;
													long BgL_arg2497z00_6884;

													BgL_arg2495z00_6883 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Unsafe/intext.scm 951 */
														long BgL_arg2500z00_6885;

														BgL_arg2500z00_6885 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_itemz00_6413));
														BgL_arg2497z00_6884 =
															(BgL_arg2500z00_6885 - OBJECT_TYPE);
													}
													BgL_arg1794z00_6882 =
														VECTOR_REF(BgL_arg2495z00_6883,
														BgL_arg2497z00_6884);
												}
												BgL_arg1793z00_6881 =
													BGl_classzd2hashzd2zz__objectz00(BgL_arg1794z00_6882);
											}
											BgL_iz00_6831 = BgL_arg1793z00_6881;
										BgL_printzd2fixnumzd2_6829:
											if ((BgL_iz00_6831 < 0L))
												{	/* Unsafe/intext.scm 802 */
													BGl_z62checkzd2bufferz12za2zz__intextz00
														(BgL_bufferz00_6411, BgL_ptrz00_6412, BINT(1L));
													{	/* Unsafe/intext.scm 719 */
														obj_t BgL_stringz00_6832;
														long BgL_kz00_6833;

														BgL_stringz00_6832 = CELL_REF(BgL_bufferz00_6411);
														BgL_kz00_6833 =
															(long) CINT(CELL_REF(BgL_ptrz00_6412));
														STRING_SET(BgL_stringz00_6832, BgL_kz00_6833,
															((unsigned char) '-'));
													}
													{	/* Unsafe/intext.scm 720 */
														obj_t BgL_auxz00_6834;

														BgL_auxz00_6834 =
															ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
														CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6834);
													}
													{	/* Unsafe/intext.scm 805 */
														long BgL_arg1714z00_6835;

														BgL_arg1714z00_6835 = NEG(BgL_iz00_6831);
														{	/* Unsafe/intext.scm 793 */
															long BgL_siza7eza7_6836;

															BgL_siza7eza7_6836 =
																BGl_siza7ezd2ofzd2wordza7zz__intextz00
																(BgL_arg1714z00_6835);
															if ((BgL_siza7eza7_6836 == 0L))
																{	/* Unsafe/intext.scm 794 */
																	return
																		BGl_z62z12printzd2markupza2zz__intextz00
																		(BgL_ptrz00_6412, BgL_bufferz00_6411, (0L));
																}
															else
																{	/* Unsafe/intext.scm 794 */
																	BGl_z62z12printzd2markupza2zz__intextz00
																		(BgL_ptrz00_6412, BgL_bufferz00_6411,
																		(BgL_siza7eza7_6836));
																	return
																		BBOOL
																		(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
																		(BgL_ptrz00_6412, BgL_bufferz00_6411,
																			BINT(BgL_arg1714z00_6835),
																			BgL_siza7eza7_6836));
																}
														}
													}
												}
											else
												{	/* Unsafe/intext.scm 793 */
													long BgL_siza7eza7_6837;

													BgL_siza7eza7_6837 =
														BGl_siza7ezd2ofzd2wordza7zz__intextz00
														(BgL_iz00_6831);
													if ((BgL_siza7eza7_6837 == 0L))
														{	/* Unsafe/intext.scm 794 */
															return
																BGl_z62z12printzd2markupza2zz__intextz00
																(BgL_ptrz00_6412, BgL_bufferz00_6411, (0L));
														}
													else
														{	/* Unsafe/intext.scm 794 */
															BGl_z62z12printzd2markupza2zz__intextz00
																(BgL_ptrz00_6412, BgL_bufferz00_6411,
																(BgL_siza7eza7_6837));
															return
																BBOOL
																(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
																(BgL_ptrz00_6412, BgL_bufferz00_6411,
																	BINT(BgL_iz00_6831), BgL_siza7eza7_6837));
														}
												}
										}
									}
								else
									{	/* Unsafe/intext.scm 895 */
										BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6411,
											BgL_ptrz00_6412, BINT(1L));
										{	/* Unsafe/intext.scm 719 */
											obj_t BgL_stringz00_6886;
											long BgL_kz00_6887;

											BgL_stringz00_6886 = CELL_REF(BgL_bufferz00_6411);
											BgL_kz00_6887 = (long) CINT(CELL_REF(BgL_ptrz00_6412));
											STRING_SET(BgL_stringz00_6886, BgL_kz00_6887,
												((unsigned char) 'X'));
										}
										{	/* Unsafe/intext.scm 720 */
											obj_t BgL_auxz00_6888;

											BgL_auxz00_6888 =
												ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
											CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6888);
										}
										BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6411,
											BgL_ptrz00_6412, BINT(1L));
										{	/* Unsafe/intext.scm 719 */
											obj_t BgL_stringz00_6889;
											long BgL_kz00_6890;

											BgL_stringz00_6889 = CELL_REF(BgL_bufferz00_6411);
											BgL_kz00_6890 = (long) CINT(CELL_REF(BgL_ptrz00_6412));
											STRING_SET(BgL_stringz00_6889, BgL_kz00_6890,
												((unsigned char) 'O'));
										}
										{	/* Unsafe/intext.scm 720 */
											obj_t BgL_auxz00_6891;

											BgL_auxz00_6891 =
												ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6412));
											CELL_SET(BgL_ptrz00_6412, BgL_auxz00_6891);
										}
										BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6410, BgL_printzd2pairzd2_6409,
											BgL_envz00_6399, BgL_printzd2classzd2_6408,
											BgL_printzd2cellzd2_6407, BgL_printzd2weakptrzd2_6406,
											BgL_printzd2vectorzd2_6405, BgL_printzd2hvectorzd2_6404,
											BgL_printzd2tvectorzd2_6403, BgL_printzd2customzd2_6402,
											BgL_refz00_6401, BgL_tablez00_6400, BgL_ptrz00_6412,
											BgL_bufferz00_6411, BgL_vz00_6868);
										{	/* Unsafe/intext.scm 941 */
											long BgL_arg1788z00_6892;

											{	/* Unsafe/intext.scm 941 */
												obj_t BgL_arg1789z00_6893;

												{	/* Unsafe/intext.scm 941 */
													obj_t BgL_arg2495z00_6894;
													long BgL_arg2497z00_6895;

													BgL_arg2495z00_6894 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Unsafe/intext.scm 941 */
														long BgL_arg2500z00_6896;

														BgL_arg2500z00_6896 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_itemz00_6413));
														BgL_arg2497z00_6895 =
															(BgL_arg2500z00_6896 - OBJECT_TYPE);
													}
													BgL_arg1789z00_6893 =
														VECTOR_REF(BgL_arg2495z00_6894,
														BgL_arg2497z00_6895);
												}
												BgL_arg1788z00_6892 =
													BGl_classzd2hashzd2zz__objectz00(BgL_arg1789z00_6893);
											}
											{
												long BgL_iz00_9467;

												BgL_iz00_9467 = BgL_arg1788z00_6892;
												BgL_iz00_6831 = BgL_iz00_9467;
												goto BgL_printzd2fixnumzd2_6829;
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* &print-class */
	obj_t BGl_z62printzd2classzb0zz__intextz00(obj_t BgL_envz00_6422,
		obj_t BgL_itemz00_6436, obj_t BgL_markz00_6437)
	{
		{	/* Unsafe/intext.scm 885 */
			{	/* Unsafe/intext.scm 885 */
				obj_t BgL_tablez00_6423;
				obj_t BgL_refz00_6424;
				obj_t BgL_printzd2customzd2_6425;
				obj_t BgL_printzd2tvectorzd2_6426;
				obj_t BgL_printzd2hvectorzd2_6427;
				obj_t BgL_printzd2vectorzd2_6428;
				obj_t BgL_printzd2weakptrzd2_6429;
				obj_t BgL_printzd2cellzd2_6430;
				obj_t BgL_printzd2objectzd2_6431;
				obj_t BgL_printzd2pairzd2_6432;
				obj_t BgL_printzd2epairzd2_6433;
				obj_t BgL_bufferz00_6434;
				obj_t BgL_ptrz00_6435;

				BgL_tablez00_6423 = PROCEDURE_L_REF(BgL_envz00_6422, (int) (0L));
				BgL_refz00_6424 = PROCEDURE_L_REF(BgL_envz00_6422, (int) (1L));
				BgL_printzd2customzd2_6425 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (2L)));
				BgL_printzd2tvectorzd2_6426 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (3L)));
				BgL_printzd2hvectorzd2_6427 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (4L)));
				BgL_printzd2vectorzd2_6428 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (5L)));
				BgL_printzd2weakptrzd2_6429 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (6L)));
				BgL_printzd2cellzd2_6430 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (7L)));
				BgL_printzd2objectzd2_6431 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (8L)));
				BgL_printzd2pairzd2_6432 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (9L)));
				BgL_printzd2epairzd2_6433 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6422, (int) (10L)));
				BgL_bufferz00_6434 = PROCEDURE_L_REF(BgL_envz00_6422, (int) (11L));
				BgL_ptrz00_6435 = PROCEDURE_L_REF(BgL_envz00_6422, (int) (12L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6434,
					BgL_ptrz00_6435, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_6897;
					long BgL_kz00_6898;

					BgL_stringz00_6897 = CELL_REF(BgL_bufferz00_6434);
					BgL_kz00_6898 = (long) CINT(CELL_REF(BgL_ptrz00_6435));
					STRING_SET(BgL_stringz00_6897, BgL_kz00_6898, ((unsigned char) 'k'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_6899;

					BgL_auxz00_6899 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6435));
					CELL_SET(BgL_ptrz00_6435, BgL_auxz00_6899);
				}
				{	/* Unsafe/intext.scm 886 */
					obj_t BgL_arg1756z00_6900;

					BgL_arg1756z00_6900 =
						SYMBOL_TO_STRING(BGl_classzd2namezd2zz__objectz00
						(BgL_itemz00_6436));
					BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6433,
						BgL_printzd2pairzd2_6432, BgL_printzd2objectzd2_6431,
						BgL_envz00_6422, BgL_printzd2cellzd2_6430,
						BgL_printzd2weakptrzd2_6429, BgL_printzd2vectorzd2_6428,
						BgL_printzd2hvectorzd2_6427, BgL_printzd2tvectorzd2_6426,
						BgL_printzd2customzd2_6425, BgL_refz00_6424, BgL_tablez00_6423,
						BgL_ptrz00_6435, BgL_bufferz00_6434, BgL_arg1756z00_6900);
				}
				{	/* Unsafe/intext.scm 887 */
					obj_t BgL_arg1758z00_6901;

					BgL_arg1758z00_6901 =
						STRUCT_REF(((obj_t) BgL_markz00_6437), (int) (1L));
					return
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6433,
						BgL_printzd2pairzd2_6432, BgL_printzd2objectzd2_6431,
						BgL_envz00_6422, BgL_printzd2cellzd2_6430,
						BgL_printzd2weakptrzd2_6429, BgL_printzd2vectorzd2_6428,
						BgL_printzd2hvectorzd2_6427, BgL_printzd2tvectorzd2_6426,
						BgL_printzd2customzd2_6425, BgL_refz00_6424, BgL_tablez00_6423,
						BgL_ptrz00_6435, BgL_bufferz00_6434, BgL_arg1758z00_6901);
				}
			}
		}

	}



/* &<@anonymous:1920> */
	obj_t BGl_z62zc3z04anonymousza31920ze3ze5zz__intextz00(obj_t BgL_envz00_6439,
		obj_t BgL_itemz00_6442, obj_t BgL_markz00_6443)
	{
		{	/* Unsafe/intext.scm 1109 */
			{	/* Unsafe/intext.scm 1110 */
				obj_t BgL_bufferz00_6440;
				obj_t BgL_ptrz00_6441;

				BgL_bufferz00_6440 = PROCEDURE_L_REF(BgL_envz00_6439, (int) (0L));
				BgL_ptrz00_6441 = PROCEDURE_L_REF(BgL_envz00_6439, (int) (1L));
				{	/* Unsafe/intext.scm 1110 */
					obj_t BgL_arg1923z00_6902;

					BgL_arg1923z00_6902 =
						STRUCT_REF(((obj_t) BgL_markz00_6443), (int) (1L));
					BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6440,
						BgL_ptrz00_6441, BINT(1L));
					{	/* Unsafe/intext.scm 719 */
						obj_t BgL_stringz00_6903;
						long BgL_kz00_6904;

						BgL_stringz00_6903 = CELL_REF(BgL_bufferz00_6440);
						BgL_kz00_6904 = (long) CINT(CELL_REF(BgL_ptrz00_6441));
						STRING_SET(BgL_stringz00_6903, BgL_kz00_6904,
							((unsigned char) 'U'));
					}
					{	/* Unsafe/intext.scm 720 */
						obj_t BgL_auxz00_6905;

						BgL_auxz00_6905 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6441));
						CELL_SET(BgL_ptrz00_6441, BgL_auxz00_6905);
					}
					{	/* Unsafe/intext.scm 811 */
						long BgL_arg1717z00_6906;

						BgL_arg1717z00_6906 = STRING_LENGTH(((obj_t) BgL_arg1923z00_6902));
						return
							BGl_z62z12printzd2charsza2zz__intextz00(BgL_ptrz00_6441,
							BgL_bufferz00_6440, BgL_arg1923z00_6902, BgL_arg1717z00_6906);
					}
				}
			}
		}

	}



/* &print-cell */
	obj_t BGl_z62printzd2cellzb0zz__intextz00(obj_t BgL_envz00_6445,
		obj_t BgL_itemz00_6459, obj_t BgL_markz00_6460)
	{
		{	/* Unsafe/intext.scm 963 */
			{	/* Unsafe/intext.scm 963 */
				obj_t BgL_tablez00_6446;
				obj_t BgL_refz00_6447;
				obj_t BgL_printzd2customzd2_6448;
				obj_t BgL_printzd2tvectorzd2_6449;
				obj_t BgL_printzd2hvectorzd2_6450;
				obj_t BgL_printzd2vectorzd2_6451;
				obj_t BgL_printzd2weakptrzd2_6452;
				obj_t BgL_printzd2classzd2_6453;
				obj_t BgL_printzd2objectzd2_6454;
				obj_t BgL_printzd2pairzd2_6455;
				obj_t BgL_printzd2epairzd2_6456;
				obj_t BgL_bufferz00_6457;
				obj_t BgL_ptrz00_6458;

				BgL_tablez00_6446 = PROCEDURE_L_REF(BgL_envz00_6445, (int) (0L));
				BgL_refz00_6447 = PROCEDURE_L_REF(BgL_envz00_6445, (int) (1L));
				BgL_printzd2customzd2_6448 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (2L)));
				BgL_printzd2tvectorzd2_6449 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (3L)));
				BgL_printzd2hvectorzd2_6450 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (4L)));
				BgL_printzd2vectorzd2_6451 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (5L)));
				BgL_printzd2weakptrzd2_6452 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (6L)));
				BgL_printzd2classzd2_6453 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (7L)));
				BgL_printzd2objectzd2_6454 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (8L)));
				BgL_printzd2pairzd2_6455 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (9L)));
				BgL_printzd2epairzd2_6456 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6445, (int) (10L)));
				BgL_bufferz00_6457 = PROCEDURE_L_REF(BgL_envz00_6445, (int) (11L));
				BgL_ptrz00_6458 = PROCEDURE_L_REF(BgL_envz00_6445, (int) (12L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6457,
					BgL_ptrz00_6458, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_6907;
					long BgL_kz00_6908;

					BgL_stringz00_6907 = CELL_REF(BgL_bufferz00_6457);
					BgL_kz00_6908 = (long) CINT(CELL_REF(BgL_ptrz00_6458));
					STRING_SET(BgL_stringz00_6907, BgL_kz00_6908, ((unsigned char) '!'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_6909;

					BgL_auxz00_6909 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6458));
					CELL_SET(BgL_ptrz00_6458, BgL_auxz00_6909);
				}
				{	/* Unsafe/intext.scm 964 */
					obj_t BgL_arg1802z00_6910;

					BgL_arg1802z00_6910 = CELL_REF(((obj_t) BgL_itemz00_6459));
					return
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6456,
						BgL_printzd2pairzd2_6455, BgL_printzd2objectzd2_6454,
						BgL_printzd2classzd2_6453, BgL_envz00_6445,
						BgL_printzd2weakptrzd2_6452, BgL_printzd2vectorzd2_6451,
						BgL_printzd2hvectorzd2_6450, BgL_printzd2tvectorzd2_6449,
						BgL_printzd2customzd2_6448, BgL_refz00_6447, BgL_tablez00_6446,
						BgL_ptrz00_6458, BgL_bufferz00_6457, BgL_arg1802z00_6910);
				}
			}
		}

	}



/* &print-weakptr */
	obj_t BGl_z62printzd2weakptrzb0zz__intextz00(obj_t BgL_envz00_6462,
		obj_t BgL_oz00_6476, obj_t BgL_markz00_6477)
	{
		{	/* Unsafe/intext.scm 968 */
			{	/* Unsafe/intext.scm 968 */
				obj_t BgL_tablez00_6463;
				obj_t BgL_refz00_6464;
				obj_t BgL_printzd2customzd2_6465;
				obj_t BgL_printzd2tvectorzd2_6466;
				obj_t BgL_printzd2hvectorzd2_6467;
				obj_t BgL_printzd2vectorzd2_6468;
				obj_t BgL_printzd2cellzd2_6469;
				obj_t BgL_printzd2classzd2_6470;
				obj_t BgL_printzd2objectzd2_6471;
				obj_t BgL_printzd2pairzd2_6472;
				obj_t BgL_printzd2epairzd2_6473;
				obj_t BgL_bufferz00_6474;
				obj_t BgL_ptrz00_6475;

				BgL_tablez00_6463 = PROCEDURE_L_REF(BgL_envz00_6462, (int) (0L));
				BgL_refz00_6464 = PROCEDURE_L_REF(BgL_envz00_6462, (int) (1L));
				BgL_printzd2customzd2_6465 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (2L)));
				BgL_printzd2tvectorzd2_6466 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (3L)));
				BgL_printzd2hvectorzd2_6467 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (4L)));
				BgL_printzd2vectorzd2_6468 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (5L)));
				BgL_printzd2cellzd2_6469 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (6L)));
				BgL_printzd2classzd2_6470 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (7L)));
				BgL_printzd2objectzd2_6471 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (8L)));
				BgL_printzd2pairzd2_6472 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (9L)));
				BgL_printzd2epairzd2_6473 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6462, (int) (10L)));
				BgL_bufferz00_6474 = PROCEDURE_L_REF(BgL_envz00_6462, (int) (11L));
				BgL_ptrz00_6475 = PROCEDURE_L_REF(BgL_envz00_6462, (int) (12L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6474,
					BgL_ptrz00_6475, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_6911;
					long BgL_kz00_6912;

					BgL_stringz00_6911 = CELL_REF(BgL_bufferz00_6474);
					BgL_kz00_6912 = (long) CINT(CELL_REF(BgL_ptrz00_6475));
					STRING_SET(BgL_stringz00_6911, BgL_kz00_6912, ((unsigned char) 'w'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_6913;

					BgL_auxz00_6913 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6475));
					CELL_SET(BgL_ptrz00_6475, BgL_auxz00_6913);
				}
				{	/* Unsafe/intext.scm 969 */
					obj_t BgL_arg1804z00_6914;

					BgL_arg1804z00_6914 = bgl_weakptr_data(((obj_t) BgL_oz00_6476));
					return
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6473,
						BgL_printzd2pairzd2_6472, BgL_printzd2objectzd2_6471,
						BgL_printzd2classzd2_6470, BgL_printzd2cellzd2_6469,
						BgL_envz00_6462, BgL_printzd2vectorzd2_6468,
						BgL_printzd2hvectorzd2_6467, BgL_printzd2tvectorzd2_6466,
						BgL_printzd2customzd2_6465, BgL_refz00_6464, BgL_tablez00_6463,
						BgL_ptrz00_6475, BgL_bufferz00_6474, BgL_arg1804z00_6914);
				}
			}
		}

	}



/* &print-vector */
	obj_t BGl_z62printzd2vectorzb0zz__intextz00(obj_t BgL_envz00_6479,
		obj_t BgL_itemz00_6493, obj_t BgL_markz00_6494)
	{
		{	/* Unsafe/intext.scm 981 */
			{	/* Unsafe/intext.scm 973 */
				obj_t BgL_tablez00_6480;
				obj_t BgL_refz00_6481;
				obj_t BgL_printzd2customzd2_6482;
				obj_t BgL_printzd2tvectorzd2_6483;
				obj_t BgL_printzd2hvectorzd2_6484;
				obj_t BgL_printzd2weakptrzd2_6485;
				obj_t BgL_printzd2cellzd2_6486;
				obj_t BgL_printzd2classzd2_6487;
				obj_t BgL_printzd2objectzd2_6488;
				obj_t BgL_printzd2pairzd2_6489;
				obj_t BgL_printzd2epairzd2_6490;
				obj_t BgL_bufferz00_6491;
				obj_t BgL_ptrz00_6492;

				BgL_tablez00_6480 = PROCEDURE_L_REF(BgL_envz00_6479, (int) (0L));
				BgL_refz00_6481 = PROCEDURE_L_REF(BgL_envz00_6479, (int) (1L));
				BgL_printzd2customzd2_6482 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (2L)));
				BgL_printzd2tvectorzd2_6483 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (3L)));
				BgL_printzd2hvectorzd2_6484 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (4L)));
				BgL_printzd2weakptrzd2_6485 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (5L)));
				BgL_printzd2cellzd2_6486 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (6L)));
				BgL_printzd2classzd2_6487 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (7L)));
				BgL_printzd2objectzd2_6488 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (8L)));
				BgL_printzd2pairzd2_6489 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (9L)));
				BgL_printzd2epairzd2_6490 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6479, (int) (10L)));
				BgL_bufferz00_6491 = PROCEDURE_L_REF(BgL_envz00_6479, (int) (11L));
				BgL_ptrz00_6492 = PROCEDURE_L_REF(BgL_envz00_6479, (int) (12L));
				{	/* Unsafe/intext.scm 973 */
					bool_t BgL_tmpz00_9655;

					{	/* Unsafe/intext.scm 973 */
						int BgL_tagz00_6915;

						{	/* Unsafe/intext.scm 973 */
							obj_t BgL_tmpz00_9656;

							BgL_tmpz00_9656 = ((obj_t) BgL_itemz00_6493);
							BgL_tagz00_6915 = VECTOR_TAG(BgL_tmpz00_9656);
						}
						if (((long) (BgL_tagz00_6915) > 0L))
							{	/* Unsafe/intext.scm 975 */
								BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6491,
									BgL_ptrz00_6492, BINT(1L));
								{	/* Unsafe/intext.scm 719 */
									obj_t BgL_stringz00_6916;
									long BgL_kz00_6917;

									BgL_stringz00_6916 = CELL_REF(BgL_bufferz00_6491);
									BgL_kz00_6917 = (long) CINT(CELL_REF(BgL_ptrz00_6492));
									STRING_SET(BgL_stringz00_6916, BgL_kz00_6917,
										((unsigned char) 't'));
								}
								{	/* Unsafe/intext.scm 720 */
									obj_t BgL_auxz00_6918;

									BgL_auxz00_6918 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6492));
									CELL_SET(BgL_ptrz00_6492, BgL_auxz00_6918);
								}
								{	/* Unsafe/intext.scm 978 */
									long BgL_iz00_6919;

									BgL_iz00_6919 = (long) (BgL_tagz00_6915);
									if ((BgL_iz00_6919 < 0L))
										{	/* Unsafe/intext.scm 802 */
											BGl_z62checkzd2bufferz12za2zz__intextz00
												(BgL_bufferz00_6491, BgL_ptrz00_6492, BINT(1L));
											{	/* Unsafe/intext.scm 719 */
												obj_t BgL_stringz00_6920;
												long BgL_kz00_6921;

												BgL_stringz00_6920 = CELL_REF(BgL_bufferz00_6491);
												BgL_kz00_6921 = (long) CINT(CELL_REF(BgL_ptrz00_6492));
												STRING_SET(BgL_stringz00_6920, BgL_kz00_6921,
													((unsigned char) '-'));
											}
											{	/* Unsafe/intext.scm 720 */
												obj_t BgL_auxz00_6922;

												BgL_auxz00_6922 =
													ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6492));
												CELL_SET(BgL_ptrz00_6492, BgL_auxz00_6922);
											}
											BGl_z62printzd2wordzb0zz__intextz00(BgL_ptrz00_6492,
												BgL_bufferz00_6491, NEG(BgL_iz00_6919));
										}
									else
										{	/* Unsafe/intext.scm 802 */
											BGl_z62printzd2wordzb0zz__intextz00(BgL_ptrz00_6492,
												BgL_bufferz00_6491, BgL_iz00_6919);
										}
								}
							}
						else
							{	/* Unsafe/intext.scm 975 */
								BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6491,
									BgL_ptrz00_6492, BINT(1L));
								{	/* Unsafe/intext.scm 719 */
									obj_t BgL_stringz00_6923;
									long BgL_kz00_6924;

									BgL_stringz00_6923 = CELL_REF(BgL_bufferz00_6491);
									BgL_kz00_6924 = (long) CINT(CELL_REF(BgL_ptrz00_6492));
									STRING_SET(BgL_stringz00_6923, BgL_kz00_6924,
										((unsigned char) '['));
								}
								{	/* Unsafe/intext.scm 720 */
									obj_t BgL_auxz00_6925;

									BgL_auxz00_6925 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6492));
									CELL_SET(BgL_ptrz00_6492, BgL_auxz00_6925);
							}}
						{	/* Unsafe/intext.scm 793 */
							long BgL_siza7eza7_6926;

							BgL_siza7eza7_6926 =
								BGl_siza7ezd2ofzd2wordza7zz__intextz00(VECTOR_LENGTH(
									((obj_t) BgL_itemz00_6493)));
							if ((BgL_siza7eza7_6926 == 0L))
								{	/* Unsafe/intext.scm 794 */
									BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6492,
										BgL_bufferz00_6491, (0L));
								}
							else
								{	/* Unsafe/intext.scm 794 */
									BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6492,
										BgL_bufferz00_6491, (BgL_siza7eza7_6926));
									BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
										(BgL_ptrz00_6492, BgL_bufferz00_6491,
											BINT(VECTOR_LENGTH(((obj_t) BgL_itemz00_6493))),
											BgL_siza7eza7_6926));
								}
						}
						{
							long BgL_iz00_6928;

							BgL_iz00_6928 = 0L;
						BgL_for1077z00_6927:
							if ((BgL_iz00_6928 < VECTOR_LENGTH(((obj_t) BgL_itemz00_6493))))
								{	/* Unsafe/intext.scm 981 */
									{	/* Unsafe/intext.scm 981 */
										obj_t BgL_arg1809z00_6929;

										BgL_arg1809z00_6929 =
											VECTOR_REF(((obj_t) BgL_itemz00_6493), BgL_iz00_6928);
										BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6490, BgL_printzd2pairzd2_6489,
											BgL_printzd2objectzd2_6488, BgL_printzd2classzd2_6487,
											BgL_printzd2cellzd2_6486, BgL_printzd2weakptrzd2_6485,
											BgL_envz00_6479, BgL_printzd2hvectorzd2_6484,
											BgL_printzd2tvectorzd2_6483, BgL_printzd2customzd2_6482,
											BgL_refz00_6481, BgL_tablez00_6480, BgL_ptrz00_6492,
											BgL_bufferz00_6491, BgL_arg1809z00_6929);
									}
									{
										long BgL_iz00_9707;

										BgL_iz00_9707 = (BgL_iz00_6928 + 1L);
										BgL_iz00_6928 = BgL_iz00_9707;
										goto BgL_for1077z00_6927;
									}
								}
							else
								{	/* Unsafe/intext.scm 981 */
									BgL_tmpz00_9655 = ((bool_t) 0);
								}
						}
					}
					return BBOOL(BgL_tmpz00_9655);
				}
			}
		}

	}



/* &print-hvector */
	obj_t BGl_z62printzd2hvectorzb0zz__intextz00(obj_t BgL_envz00_6498,
		obj_t BgL_itemz00_6501, obj_t BgL_markz00_6502)
	{
		{	/* Unsafe/intext.scm 1020 */
			{	/* Unsafe/intext.scm 985 */
				obj_t BgL_bufferz00_6499;
				obj_t BgL_ptrz00_6500;

				BgL_bufferz00_6499 = PROCEDURE_L_REF(BgL_envz00_6498, (int) (0L));
				BgL_ptrz00_6500 = PROCEDURE_L_REF(BgL_envz00_6498, (int) (1L));
				{
					uint64_t BgL_mz00_6940;
					long BgL_siza7eza7_6941;
					int64_t BgL_mz00_6932;
					long BgL_siza7eza7_6933;

					{	/* Unsafe/intext.scm 985 */
						obj_t BgL_tagz00_6948;

						BgL_tagz00_6948 =
							BGl_homogeneouszd2vectorzd2infoz00zz__srfi4z00(BgL_itemz00_6501);
						{	/* Unsafe/intext.scm 986 */
							obj_t BgL_bsiza7eza7_6949;
							obj_t BgL_refz00_6950;
							obj_t BgL__z00_6951;
							obj_t BgL__z00_6952;

							{	/* Unsafe/intext.scm 987 */
								obj_t BgL_tmpz00_6953;

								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9715;

									BgL_tmpz00_9715 = (int) (1L);
									BgL_tmpz00_6953 = BGL_MVALUES_VAL(BgL_tmpz00_9715);
								}
								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9718;

									BgL_tmpz00_9718 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_9718, BUNSPEC);
								}
								BgL_bsiza7eza7_6949 = BgL_tmpz00_6953;
							}
							{	/* Unsafe/intext.scm 987 */
								obj_t BgL_tmpz00_6954;

								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9721;

									BgL_tmpz00_9721 = (int) (2L);
									BgL_tmpz00_6954 = BGL_MVALUES_VAL(BgL_tmpz00_9721);
								}
								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9724;

									BgL_tmpz00_9724 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_9724, BUNSPEC);
								}
								BgL_refz00_6950 = BgL_tmpz00_6954;
							}
							{	/* Unsafe/intext.scm 987 */
								obj_t BgL_tmpz00_6955;

								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9727;

									BgL_tmpz00_9727 = (int) (3L);
									BgL_tmpz00_6955 = BGL_MVALUES_VAL(BgL_tmpz00_9727);
								}
								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9730;

									BgL_tmpz00_9730 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_9730, BUNSPEC);
								}
								BgL__z00_6951 = BgL_tmpz00_6955;
							}
							{	/* Unsafe/intext.scm 987 */
								obj_t BgL_tmpz00_6956;

								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9733;

									BgL_tmpz00_9733 = (int) (4L);
									BgL_tmpz00_6956 = BGL_MVALUES_VAL(BgL_tmpz00_9733);
								}
								{	/* Unsafe/intext.scm 987 */
									int BgL_tmpz00_9736;

									BgL_tmpz00_9736 = (int) (4L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_9736, BUNSPEC);
								}
								BgL__z00_6952 = BgL_tmpz00_6956;
							}
							{	/* Unsafe/intext.scm 987 */
								long BgL_lenz00_6957;

								BgL_lenz00_6957 = BGL_HVECTOR_LENGTH(BgL_itemz00_6501);
								BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6499,
									BgL_ptrz00_6500, BINT(1L));
								{	/* Unsafe/intext.scm 719 */
									obj_t BgL_stringz00_6958;
									long BgL_kz00_6959;

									BgL_stringz00_6958 = CELL_REF(BgL_bufferz00_6499);
									BgL_kz00_6959 = (long) CINT(CELL_REF(BgL_ptrz00_6500));
									STRING_SET(BgL_stringz00_6958, BgL_kz00_6959,
										((unsigned char) 'h'));
								}
								{	/* Unsafe/intext.scm 720 */
									obj_t BgL_auxz00_6960;

									BgL_auxz00_6960 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6500));
									CELL_SET(BgL_ptrz00_6500, BgL_auxz00_6960);
								}
								{	/* Unsafe/intext.scm 793 */
									long BgL_siza7eza7_6961;

									BgL_siza7eza7_6961 =
										BGl_siza7ezd2ofzd2wordza7zz__intextz00(BgL_lenz00_6957);
									if ((BgL_siza7eza7_6961 == 0L))
										{	/* Unsafe/intext.scm 794 */
											BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6500,
												BgL_bufferz00_6499, (0L));
										}
									else
										{	/* Unsafe/intext.scm 794 */
											BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6500,
												BgL_bufferz00_6499, (BgL_siza7eza7_6961));
											BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
												(BgL_ptrz00_6500, BgL_bufferz00_6499,
													BINT(BgL_lenz00_6957), BgL_siza7eza7_6961));
										}
								}
								{	/* Unsafe/intext.scm 793 */
									long BgL_siza7eza7_6962;

									BgL_siza7eza7_6962 =
										BGl_siza7ezd2ofzd2wordza7zz__intextz00(
										(long) CINT(BgL_bsiza7eza7_6949));
									if ((BgL_siza7eza7_6962 == 0L))
										{	/* Unsafe/intext.scm 794 */
											BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6500,
												BgL_bufferz00_6499, (0L));
										}
									else
										{	/* Unsafe/intext.scm 794 */
											BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6500,
												BgL_bufferz00_6499, (BgL_siza7eza7_6962));
											BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
												(BgL_ptrz00_6500, BgL_bufferz00_6499,
													BgL_bsiza7eza7_6949, BgL_siza7eza7_6962));
										}
								}
								{	/* Unsafe/intext.scm 991 */
									obj_t BgL_arg1812z00_6963;

									BgL_arg1812z00_6963 =
										SYMBOL_TO_STRING(((obj_t) BgL_tagz00_6948));
									BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6499,
										BgL_ptrz00_6500, BINT(1L));
									{	/* Unsafe/intext.scm 719 */
										obj_t BgL_stringz00_6964;
										long BgL_kz00_6965;

										BgL_stringz00_6964 = CELL_REF(BgL_bufferz00_6499);
										BgL_kz00_6965 = (long) CINT(CELL_REF(BgL_ptrz00_6500));
										STRING_SET(BgL_stringz00_6964, BgL_kz00_6965,
											((unsigned char) '"'));
									}
									{	/* Unsafe/intext.scm 720 */
										obj_t BgL_auxz00_6966;

										BgL_auxz00_6966 =
											ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6500));
										CELL_SET(BgL_ptrz00_6500, BgL_auxz00_6966);
									}
									BGl_z62z12printzd2charsza2zz__intextz00(BgL_ptrz00_6500,
										BgL_bufferz00_6499, BgL_arg1812z00_6963,
										STRING_LENGTH(BgL_arg1812z00_6963));
								}
								if ((BgL_tagz00_6948 == BGl_symbol2627z00zz__intextz00))
									{
										long BgL_iz00_6968;

										{	/* Unsafe/intext.scm 994 */
											bool_t BgL_tmpz00_9778;

											BgL_iz00_6968 = 0L;
										BgL_for1078z00_6967:
											if ((BgL_iz00_6968 < BgL_lenz00_6957))
												{	/* Unsafe/intext.scm 994 */
													{	/* Unsafe/intext.scm 995 */
														long BgL_arg1816z00_6969;

														{	/* Unsafe/intext.scm 995 */
															int8_t BgL_arg1817z00_6970;

															BgL_arg1817z00_6970 =
																BGL_S8VREF(BgL_itemz00_6501, BgL_iz00_6968);
															{	/* Unsafe/intext.scm 995 */
																long BgL_arg2367z00_6971;

																BgL_arg2367z00_6971 =
																	(long) (BgL_arg1817z00_6970);
																BgL_arg1816z00_6969 =
																	(long) (BgL_arg2367z00_6971);
														}}
														{	/* Unsafe/intext.scm 752 */
															long BgL_g1065z00_6972;

															BgL_g1065z00_6972 = (1L - 1L);
															{
																long BgL_iz00_6974;

																BgL_iz00_6974 = BgL_g1065z00_6972;
															BgL_loopz00_6973:
																if ((BgL_iz00_6974 >= 0L))
																	{	/* Unsafe/intext.scm 754 */
																		long BgL_dz00_6975;

																		BgL_dz00_6975 =
																			(
																			(BgL_arg1816z00_6969 >>
																				(int) ((8L * BgL_iz00_6974))) & 255L);
																		BGl_z62z12printzd2markupza2zz__intextz00
																			(BgL_ptrz00_6500, BgL_bufferz00_6499,
																			(BgL_dz00_6975));
																		{
																			long BgL_iz00_9793;

																			BgL_iz00_9793 = (BgL_iz00_6974 - 1L);
																			BgL_iz00_6974 = BgL_iz00_9793;
																			goto BgL_loopz00_6973;
																		}
																	}
																else
																	{	/* Unsafe/intext.scm 753 */
																		((bool_t) 0);
																	}
															}
														}
													}
													{
														long BgL_iz00_9795;

														BgL_iz00_9795 = (BgL_iz00_6968 + 1L);
														BgL_iz00_6968 = BgL_iz00_9795;
														goto BgL_for1078z00_6967;
													}
												}
											else
												{	/* Unsafe/intext.scm 994 */
													BgL_tmpz00_9778 = ((bool_t) 0);
												}
											return BBOOL(BgL_tmpz00_9778);
										}
									}
								else
									{	/* Unsafe/intext.scm 992 */
										if ((BgL_tagz00_6948 == BGl_symbol2629z00zz__intextz00))
											{
												long BgL_iz00_6977;

												{	/* Unsafe/intext.scm 997 */
													bool_t BgL_tmpz00_9800;

													BgL_iz00_6977 = 0L;
												BgL_for1079z00_6976:
													if ((BgL_iz00_6977 < BgL_lenz00_6957))
														{	/* Unsafe/intext.scm 997 */
															{	/* Unsafe/intext.scm 998 */
																long BgL_arg1822z00_6978;

																{	/* Unsafe/intext.scm 998 */
																	uint8_t BgL_arg1823z00_6979;

																	BgL_arg1823z00_6979 =
																		BGL_U8VREF(BgL_itemz00_6501, BgL_iz00_6977);
																	{	/* Unsafe/intext.scm 998 */
																		long BgL_arg2366z00_6980;

																		BgL_arg2366z00_6980 =
																			(long) (BgL_arg1823z00_6979);
																		BgL_arg1822z00_6978 =
																			(long) (BgL_arg2366z00_6980);
																}}
																{	/* Unsafe/intext.scm 752 */
																	long BgL_g1065z00_6981;

																	BgL_g1065z00_6981 = (1L - 1L);
																	{
																		long BgL_iz00_6983;

																		BgL_iz00_6983 = BgL_g1065z00_6981;
																	BgL_loopz00_6982:
																		if ((BgL_iz00_6983 >= 0L))
																			{	/* Unsafe/intext.scm 754 */
																				long BgL_dz00_6984;

																				BgL_dz00_6984 =
																					(
																					(BgL_arg1822z00_6978 >>
																						(int) (
																							(8L * BgL_iz00_6983))) & 255L);
																				BGl_z62z12printzd2markupza2zz__intextz00
																					(BgL_ptrz00_6500, BgL_bufferz00_6499,
																					(BgL_dz00_6984));
																				{
																					long BgL_iz00_9815;

																					BgL_iz00_9815 = (BgL_iz00_6983 - 1L);
																					BgL_iz00_6983 = BgL_iz00_9815;
																					goto BgL_loopz00_6982;
																				}
																			}
																		else
																			{	/* Unsafe/intext.scm 753 */
																				((bool_t) 0);
																			}
																	}
																}
															}
															{
																long BgL_iz00_9817;

																BgL_iz00_9817 = (BgL_iz00_6977 + 1L);
																BgL_iz00_6977 = BgL_iz00_9817;
																goto BgL_for1079z00_6976;
															}
														}
													else
														{	/* Unsafe/intext.scm 997 */
															BgL_tmpz00_9800 = ((bool_t) 0);
														}
													return BBOOL(BgL_tmpz00_9800);
												}
											}
										else
											{	/* Unsafe/intext.scm 992 */
												if ((BgL_tagz00_6948 == BGl_symbol2631z00zz__intextz00))
													{
														long BgL_iz00_6986;

														{	/* Unsafe/intext.scm 1000 */
															bool_t BgL_tmpz00_9822;

															BgL_iz00_6986 = 0L;
														BgL_for1080z00_6985:
															if ((BgL_iz00_6986 < BgL_lenz00_6957))
																{	/* Unsafe/intext.scm 1000 */
																	{	/* Unsafe/intext.scm 1001 */
																		long BgL_arg1831z00_6987;

																		{	/* Unsafe/intext.scm 1001 */
																			int16_t BgL_arg1832z00_6988;

																			BgL_arg1832z00_6988 =
																				BGL_S16VREF(BgL_itemz00_6501,
																				BgL_iz00_6986);
																			{	/* Unsafe/intext.scm 1001 */
																				long BgL_arg2365z00_6989;

																				BgL_arg2365z00_6989 =
																					(long) (BgL_arg1832z00_6988);
																				BgL_arg1831z00_6987 =
																					(long) (BgL_arg2365z00_6989);
																		}}
																		{	/* Unsafe/intext.scm 752 */
																			long BgL_g1065z00_6990;

																			BgL_g1065z00_6990 = (2L - 1L);
																			{
																				long BgL_iz00_6992;

																				BgL_iz00_6992 = BgL_g1065z00_6990;
																			BgL_loopz00_6991:
																				if ((BgL_iz00_6992 >= 0L))
																					{	/* Unsafe/intext.scm 754 */
																						long BgL_dz00_6993;

																						BgL_dz00_6993 =
																							(
																							(BgL_arg1831z00_6987 >>
																								(int) (
																									(8L *
																										BgL_iz00_6992))) & 255L);
																						BGl_z62z12printzd2markupza2zz__intextz00
																							(BgL_ptrz00_6500,
																							BgL_bufferz00_6499,
																							(BgL_dz00_6993));
																						{
																							long BgL_iz00_9837;

																							BgL_iz00_9837 =
																								(BgL_iz00_6992 - 1L);
																							BgL_iz00_6992 = BgL_iz00_9837;
																							goto BgL_loopz00_6991;
																						}
																					}
																				else
																					{	/* Unsafe/intext.scm 753 */
																						((bool_t) 0);
																					}
																			}
																		}
																	}
																	{
																		long BgL_iz00_9839;

																		BgL_iz00_9839 = (BgL_iz00_6986 + 1L);
																		BgL_iz00_6986 = BgL_iz00_9839;
																		goto BgL_for1080z00_6985;
																	}
																}
															else
																{	/* Unsafe/intext.scm 1000 */
																	BgL_tmpz00_9822 = ((bool_t) 0);
																}
															return BBOOL(BgL_tmpz00_9822);
														}
													}
												else
													{	/* Unsafe/intext.scm 992 */
														if (
															(BgL_tagz00_6948 ==
																BGl_symbol2633z00zz__intextz00))
															{
																long BgL_iz00_6995;

																{	/* Unsafe/intext.scm 1003 */
																	bool_t BgL_tmpz00_9844;

																	BgL_iz00_6995 = 0L;
																BgL_for1081z00_6994:
																	if ((BgL_iz00_6995 < BgL_lenz00_6957))
																		{	/* Unsafe/intext.scm 1003 */
																			{	/* Unsafe/intext.scm 1004 */
																				long BgL_arg1837z00_6996;

																				{	/* Unsafe/intext.scm 1004 */
																					uint16_t BgL_arg1838z00_6997;

																					BgL_arg1838z00_6997 =
																						BGL_U16VREF(BgL_itemz00_6501,
																						BgL_iz00_6995);
																					{	/* Unsafe/intext.scm 1004 */
																						long BgL_arg2364z00_6998;

																						BgL_arg2364z00_6998 =
																							(long) (BgL_arg1838z00_6997);
																						BgL_arg1837z00_6996 =
																							(long) (BgL_arg2364z00_6998);
																				}}
																				{	/* Unsafe/intext.scm 752 */
																					long BgL_g1065z00_6999;

																					BgL_g1065z00_6999 = (2L - 1L);
																					{
																						long BgL_iz00_7001;

																						BgL_iz00_7001 = BgL_g1065z00_6999;
																					BgL_loopz00_7000:
																						if ((BgL_iz00_7001 >= 0L))
																							{	/* Unsafe/intext.scm 754 */
																								long BgL_dz00_7002;

																								BgL_dz00_7002 =
																									(
																									(BgL_arg1837z00_6996 >>
																										(int) (
																											(8L *
																												BgL_iz00_7001))) &
																									255L);
																								BGl_z62z12printzd2markupza2zz__intextz00
																									(BgL_ptrz00_6500,
																									BgL_bufferz00_6499,
																									(BgL_dz00_7002));
																								{
																									long BgL_iz00_9859;

																									BgL_iz00_9859 =
																										(BgL_iz00_7001 - 1L);
																									BgL_iz00_7001 = BgL_iz00_9859;
																									goto BgL_loopz00_7000;
																								}
																							}
																						else
																							{	/* Unsafe/intext.scm 753 */
																								((bool_t) 0);
																							}
																					}
																				}
																			}
																			{
																				long BgL_iz00_9861;

																				BgL_iz00_9861 = (BgL_iz00_6995 + 1L);
																				BgL_iz00_6995 = BgL_iz00_9861;
																				goto BgL_for1081z00_6994;
																			}
																		}
																	else
																		{	/* Unsafe/intext.scm 1003 */
																			BgL_tmpz00_9844 = ((bool_t) 0);
																		}
																	return BBOOL(BgL_tmpz00_9844);
																}
															}
														else
															{	/* Unsafe/intext.scm 992 */
																if (
																	(BgL_tagz00_6948 ==
																		BGl_symbol2635z00zz__intextz00))
																	{
																		long BgL_iz00_7004;

																		{	/* Unsafe/intext.scm 1006 */
																			bool_t BgL_tmpz00_9866;

																			BgL_iz00_7004 = 0L;
																		BgL_for1082z00_7003:
																			if ((BgL_iz00_7004 < BgL_lenz00_6957))
																				{	/* Unsafe/intext.scm 1006 */
																					{	/* Unsafe/intext.scm 1007 */
																						long BgL_arg1843z00_7005;

																						{	/* Unsafe/intext.scm 1007 */
																							int32_t BgL_arg1844z00_7006;

																							BgL_arg1844z00_7006 =
																								BGL_S32VREF(BgL_itemz00_6501,
																								BgL_iz00_7004);
																							{	/* Unsafe/intext.scm 1007 */
																								long BgL_arg2363z00_7007;

																								BgL_arg2363z00_7007 =
																									(long) (BgL_arg1844z00_7006);
																								BgL_arg1843z00_7005 =
																									(long) (BgL_arg2363z00_7007);
																						}}
																						{	/* Unsafe/intext.scm 752 */
																							long BgL_g1065z00_7008;

																							BgL_g1065z00_7008 = (4L - 1L);
																							{
																								long BgL_iz00_7010;

																								BgL_iz00_7010 =
																									BgL_g1065z00_7008;
																							BgL_loopz00_7009:
																								if ((BgL_iz00_7010 >= 0L))
																									{	/* Unsafe/intext.scm 754 */
																										long BgL_dz00_7011;

																										BgL_dz00_7011 =
																											(
																											(BgL_arg1843z00_7005 >>
																												(int) (
																													(8L *
																														BgL_iz00_7010))) &
																											255L);
																										BGl_z62z12printzd2markupza2zz__intextz00
																											(BgL_ptrz00_6500,
																											BgL_bufferz00_6499,
																											(BgL_dz00_7011));
																										{
																											long BgL_iz00_9881;

																											BgL_iz00_9881 =
																												(BgL_iz00_7010 - 1L);
																											BgL_iz00_7010 =
																												BgL_iz00_9881;
																											goto BgL_loopz00_7009;
																										}
																									}
																								else
																									{	/* Unsafe/intext.scm 753 */
																										((bool_t) 0);
																									}
																							}
																						}
																					}
																					{
																						long BgL_iz00_9883;

																						BgL_iz00_9883 =
																							(BgL_iz00_7004 + 1L);
																						BgL_iz00_7004 = BgL_iz00_9883;
																						goto BgL_for1082z00_7003;
																					}
																				}
																			else
																				{	/* Unsafe/intext.scm 1006 */
																					BgL_tmpz00_9866 = ((bool_t) 0);
																				}
																			return BBOOL(BgL_tmpz00_9866);
																		}
																	}
																else
																	{	/* Unsafe/intext.scm 992 */
																		if (
																			(BgL_tagz00_6948 ==
																				BGl_symbol2637z00zz__intextz00))
																			{
																				long BgL_iz00_7013;

																				{	/* Unsafe/intext.scm 1009 */
																					bool_t BgL_tmpz00_9888;

																					BgL_iz00_7013 = 0L;
																				BgL_for1083z00_7012:
																					if ((BgL_iz00_7013 < BgL_lenz00_6957))
																						{	/* Unsafe/intext.scm 1009 */
																							{	/* Unsafe/intext.scm 1010 */
																								long BgL_arg1849z00_7014;

																								{	/* Unsafe/intext.scm 1010 */
																									uint32_t BgL_arg1850z00_7015;

																									BgL_arg1850z00_7015 =
																										BGL_U32VREF
																										(BgL_itemz00_6501,
																										BgL_iz00_7013);
																									BgL_arg1849z00_7014 =
																										(long)
																										(BgL_arg1850z00_7015);
																								}
																								{	/* Unsafe/intext.scm 752 */
																									long BgL_g1065z00_7016;

																									BgL_g1065z00_7016 = (4L - 1L);
																									{
																										long BgL_iz00_7018;

																										BgL_iz00_7018 =
																											BgL_g1065z00_7016;
																									BgL_loopz00_7017:
																										if ((BgL_iz00_7018 >= 0L))
																											{	/* Unsafe/intext.scm 754 */
																												long BgL_dz00_7019;

																												BgL_dz00_7019 =
																													(
																													(BgL_arg1849z00_7014
																														>> (int) ((8L *
																																BgL_iz00_7018)))
																													& 255L);
																												BGl_z62z12printzd2markupza2zz__intextz00
																													(BgL_ptrz00_6500,
																													BgL_bufferz00_6499,
																													(BgL_dz00_7019));
																												{
																													long BgL_iz00_9902;

																													BgL_iz00_9902 =
																														(BgL_iz00_7018 -
																														1L);
																													BgL_iz00_7018 =
																														BgL_iz00_9902;
																													goto BgL_loopz00_7017;
																												}
																											}
																										else
																											{	/* Unsafe/intext.scm 753 */
																												((bool_t) 0);
																											}
																									}
																								}
																							}
																							{
																								long BgL_iz00_9904;

																								BgL_iz00_9904 =
																									(BgL_iz00_7013 + 1L);
																								BgL_iz00_7013 = BgL_iz00_9904;
																								goto BgL_for1083z00_7012;
																							}
																						}
																					else
																						{	/* Unsafe/intext.scm 1009 */
																							BgL_tmpz00_9888 = ((bool_t) 0);
																						}
																					return BBOOL(BgL_tmpz00_9888);
																				}
																			}
																		else
																			{	/* Unsafe/intext.scm 992 */
																				if (
																					(BgL_tagz00_6948 ==
																						BGl_symbol2639z00zz__intextz00))
																					{
																						long BgL_iz00_7021;

																						{	/* Unsafe/intext.scm 1012 */
																							bool_t BgL_tmpz00_9909;

																							BgL_iz00_7021 = 0L;
																						BgL_for1084z00_7020:
																							if (
																								(BgL_iz00_7021 <
																									BgL_lenz00_6957))
																								{	/* Unsafe/intext.scm 1012 */
																									{	/* Unsafe/intext.scm 1013 */
																										int64_t BgL_arg1856z00_7022;

																										BgL_arg1856z00_7022 =
																											BGL_S64VREF
																											(BgL_itemz00_6501,
																											BgL_iz00_7021);
																										BgL_mz00_6932 =
																											BgL_arg1856z00_7022;
																										BgL_siza7eza7_6933 = 8L;
																										{	/* Unsafe/intext.scm 776 */
																											long BgL_g1068z00_6934;

																											BgL_g1068z00_6934 =
																												(BgL_siza7eza7_6933 -
																												1L);
																											{
																												long BgL_iz00_6936;

																												BgL_iz00_6936 =
																													BgL_g1068z00_6934;
																											BgL_loopz00_6935:
																												if (
																													(BgL_iz00_6936 >= 0L))
																													{	/* Unsafe/intext.scm 778 */
																														long BgL_dz00_6937;

																														{	/* Unsafe/intext.scm 779 */
																															int64_t
																																BgL_arg1699z00_6938;
																															BgL_arg1699z00_6938
																																=
																																((BgL_mz00_6932
																																	>> (int) ((8L
																																			*
																																			BgL_iz00_6936)))
																																&
																																(int64_t)
																																(255));
																															{	/* Unsafe/intext.scm 778 */
																																long
																																	BgL_arg2361z00_6939;
																																BgL_arg2361z00_6939
																																	=
																																	(long)
																																	(BgL_arg1699z00_6938);
																																BgL_dz00_6937 =
																																	(long)
																																	(BgL_arg2361z00_6939);
																														}}
																														BGl_z62z12printzd2markupza2zz__intextz00
																															(BgL_ptrz00_6500,
																															BgL_bufferz00_6499,
																															(BgL_dz00_6937));
																														{
																															long
																																BgL_iz00_9924;
																															BgL_iz00_9924 =
																																(BgL_iz00_6936 -
																																1L);
																															BgL_iz00_6936 =
																																BgL_iz00_9924;
																															goto
																																BgL_loopz00_6935;
																														}
																													}
																												else
																													{	/* Unsafe/intext.scm 777 */
																														((bool_t) 0);
																													}
																											}
																										}
																									}
																									{
																										long BgL_iz00_9926;

																										BgL_iz00_9926 =
																											(BgL_iz00_7021 + 1L);
																										BgL_iz00_7021 =
																											BgL_iz00_9926;
																										goto BgL_for1084z00_7020;
																									}
																								}
																							else
																								{	/* Unsafe/intext.scm 1012 */
																									BgL_tmpz00_9909 =
																										((bool_t) 0);
																								}
																							return BBOOL(BgL_tmpz00_9909);
																						}
																					}
																				else
																					{	/* Unsafe/intext.scm 992 */
																						if (
																							(BgL_tagz00_6948 ==
																								BGl_symbol2641z00zz__intextz00))
																							{
																								long BgL_iz00_7024;

																								{	/* Unsafe/intext.scm 1015 */
																									bool_t BgL_tmpz00_9931;

																									BgL_iz00_7024 = 0L;
																								BgL_for1085z00_7023:
																									if (
																										(BgL_iz00_7024 <
																											BgL_lenz00_6957))
																										{	/* Unsafe/intext.scm 1015 */
																											{	/* Unsafe/intext.scm 1016 */
																												uint64_t
																													BgL_arg1862z00_7025;
																												BgL_arg1862z00_7025 =
																													BGL_U64VREF
																													(BgL_itemz00_6501,
																													BgL_iz00_7024);
																												BgL_mz00_6940 =
																													BgL_arg1862z00_7025;
																												BgL_siza7eza7_6941 = 8L;
																												{	/* Unsafe/intext.scm 784 */
																													long
																														BgL_g1069z00_6942;
																													BgL_g1069z00_6942 =
																														(BgL_siza7eza7_6941
																														- 1L);
																													{
																														long BgL_iz00_6944;

																														BgL_iz00_6944 =
																															BgL_g1069z00_6942;
																													BgL_loopz00_6943:
																														if (
																															(BgL_iz00_6944 >=
																																0L))
																															{	/* Unsafe/intext.scm 786 */
																																long
																																	BgL_dz00_6945;
																																{	/* Unsafe/intext.scm 787 */
																																	uint64_t
																																		BgL_arg1706z00_6946;
																																	BgL_arg1706z00_6946
																																		=
																																		(
																																		(BgL_mz00_6940
																																			>>
																																			(int) ((8L
																																					*
																																					BgL_iz00_6944)))
																																		&
																																		(uint64_t)
																																		(255));
																																	{	/* Unsafe/intext.scm 786 */
																																		long
																																			BgL_arg2358z00_6947;
																																		BgL_arg2358z00_6947
																																			=
																																			(long)
																																			(BgL_arg1706z00_6946);
																																		BgL_dz00_6945
																																			=
																																			(long)
																																			(BgL_arg2358z00_6947);
																																}}
																																BGl_z62z12printzd2markupza2zz__intextz00
																																	(BgL_ptrz00_6500,
																																	BgL_bufferz00_6499,
																																	(BgL_dz00_6945));
																																{
																																	long
																																		BgL_iz00_9946;
																																	BgL_iz00_9946
																																		=
																																		(BgL_iz00_6944
																																		- 1L);
																																	BgL_iz00_6944
																																		=
																																		BgL_iz00_9946;
																																	goto
																																		BgL_loopz00_6943;
																																}
																															}
																														else
																															{	/* Unsafe/intext.scm 785 */
																																((bool_t) 0);
																															}
																													}
																												}
																											}
																											{
																												long BgL_iz00_9948;

																												BgL_iz00_9948 =
																													(BgL_iz00_7024 + 1L);
																												BgL_iz00_7024 =
																													BgL_iz00_9948;
																												goto
																													BgL_for1085z00_7023;
																											}
																										}
																									else
																										{	/* Unsafe/intext.scm 1015 */
																											BgL_tmpz00_9931 =
																												((bool_t) 0);
																										}
																									return BBOOL(BgL_tmpz00_9931);
																								}
																							}
																						else
																							{	/* Unsafe/intext.scm 992 */
																								bool_t BgL_test2984z00_9951;

																								{	/* Unsafe/intext.scm 992 */
																									bool_t
																										BgL__ortest_1086z00_7026;
																									BgL__ortest_1086z00_7026 =
																										(BgL_tagz00_6948 ==
																										BGl_symbol2643z00zz__intextz00);
																									if (BgL__ortest_1086z00_7026)
																										{	/* Unsafe/intext.scm 992 */
																											BgL_test2984z00_9951 =
																												BgL__ortest_1086z00_7026;
																										}
																									else
																										{	/* Unsafe/intext.scm 992 */
																											BgL_test2984z00_9951 =
																												(BgL_tagz00_6948 ==
																												BGl_symbol2645z00zz__intextz00);
																										}
																								}
																								if (BgL_test2984z00_9951)
																									{
																										long BgL_iz00_7028;

																										{	/* Unsafe/intext.scm 1018 */
																											bool_t BgL_tmpz00_9955;

																											BgL_iz00_7028 = 0L;
																										BgL_for1087z00_7027:
																											if (
																												(BgL_iz00_7028 <
																													BgL_lenz00_6957))
																												{	/* Unsafe/intext.scm 1018 */
																													{	/* Unsafe/intext.scm 1019 */
																														obj_t BgL_sz00_7029;

																														{	/* Unsafe/intext.scm 1019 */
																															obj_t
																																BgL_arg1869z00_7030;
																															BgL_arg1869z00_7030
																																=
																																BGL_PROCEDURE_CALL2
																																(BgL_refz00_6950,
																																BgL_itemz00_6501,
																																BINT
																																(BgL_iz00_7028));
																															BgL_sz00_7029 =
																																bgl_real_to_string
																																(REAL_TO_DOUBLE
																																(BgL_arg1869z00_7030));
																														}
																														{	/* Unsafe/intext.scm 1020 */
																															long
																																BgL_arg1868z00_7031;
																															BgL_arg1868z00_7031
																																=
																																STRING_LENGTH
																																(BgL_sz00_7029);
																															BGl_z62printzd2wordzb0zz__intextz00
																																(BgL_ptrz00_6500,
																																BgL_bufferz00_6499,
																																BgL_arg1868z00_7031);
																															BGl_z62checkzd2bufferz12za2zz__intextz00
																																(BgL_bufferz00_6499,
																																BgL_ptrz00_6500,
																																BINT
																																(BgL_arg1868z00_7031));
																															{	/* Unsafe/intext.scm 726 */
																																obj_t
																																	BgL_s2z00_7032;
																																long
																																	BgL_o2z00_7033;
																																BgL_s2z00_7032 =
																																	CELL_REF
																																	(BgL_bufferz00_6499);
																																BgL_o2z00_7033 =
																																	(long)
																																	CINT(CELL_REF
																																	(BgL_ptrz00_6500));
																																blit_string
																																	(BgL_sz00_7029,
																																	0L,
																																	BgL_s2z00_7032,
																																	BgL_o2z00_7033,
																																	BgL_arg1868z00_7031);
																															}
																															{	/* Unsafe/intext.scm 727 */
																																obj_t
																																	BgL_auxz00_7034;
																																BgL_auxz00_7034
																																	=
																																	ADDFX(CELL_REF
																																	(BgL_ptrz00_6500),
																																	BINT
																																	(BgL_arg1868z00_7031));
																																CELL_SET
																																	(BgL_ptrz00_6500,
																																	BgL_auxz00_7034);
																													}}}
																													{
																														long BgL_iz00_9974;

																														BgL_iz00_9974 =
																															(BgL_iz00_7028 +
																															1L);
																														BgL_iz00_7028 =
																															BgL_iz00_9974;
																														goto
																															BgL_for1087z00_7027;
																													}
																												}
																											else
																												{	/* Unsafe/intext.scm 1018 */
																													BgL_tmpz00_9955 =
																														((bool_t) 0);
																												}
																											return
																												BBOOL(BgL_tmpz00_9955);
																										}
																									}
																								else
																									{	/* Unsafe/intext.scm 992 */
																										return BUNSPEC;
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &print-tvector */
	obj_t BGl_z62printzd2tvectorzb0zz__intextz00(obj_t BgL_envz00_6506,
		obj_t BgL_itemz00_6520, obj_t BgL_markz00_6521)
	{
		{	/* Unsafe/intext.scm 1027 */
			{	/* Unsafe/intext.scm 1024 */
				obj_t BgL_tablez00_6507;
				obj_t BgL_refz00_6508;
				obj_t BgL_printzd2customzd2_6509;
				obj_t BgL_printzd2hvectorzd2_6510;
				obj_t BgL_printzd2vectorzd2_6511;
				obj_t BgL_printzd2weakptrzd2_6512;
				obj_t BgL_printzd2cellzd2_6513;
				obj_t BgL_printzd2classzd2_6514;
				obj_t BgL_printzd2objectzd2_6515;
				obj_t BgL_printzd2pairzd2_6516;
				obj_t BgL_printzd2epairzd2_6517;
				obj_t BgL_bufferz00_6518;
				obj_t BgL_ptrz00_6519;

				BgL_tablez00_6507 = PROCEDURE_L_REF(BgL_envz00_6506, (int) (0L));
				BgL_refz00_6508 = PROCEDURE_L_REF(BgL_envz00_6506, (int) (1L));
				BgL_printzd2customzd2_6509 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (2L)));
				BgL_printzd2hvectorzd2_6510 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (3L)));
				BgL_printzd2vectorzd2_6511 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (4L)));
				BgL_printzd2weakptrzd2_6512 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (5L)));
				BgL_printzd2cellzd2_6513 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (6L)));
				BgL_printzd2classzd2_6514 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (7L)));
				BgL_printzd2objectzd2_6515 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (8L)));
				BgL_printzd2pairzd2_6516 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (9L)));
				BgL_printzd2epairzd2_6517 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6506, (int) (10L)));
				BgL_bufferz00_6518 = PROCEDURE_L_REF(BgL_envz00_6506, (int) (11L));
				BgL_ptrz00_6519 = PROCEDURE_L_REF(BgL_envz00_6506, (int) (12L));
				{	/* Unsafe/intext.scm 1024 */
					obj_t BgL_vz00_7035;

					BgL_vz00_7035 = STRUCT_REF(((obj_t) BgL_itemz00_6520), (int) (1L));
					BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6518,
						BgL_ptrz00_6519, BINT(1L));
					{	/* Unsafe/intext.scm 719 */
						obj_t BgL_stringz00_7036;
						long BgL_kz00_7037;

						BgL_stringz00_7036 = CELL_REF(BgL_bufferz00_6518);
						BgL_kz00_7037 = (long) CINT(CELL_REF(BgL_ptrz00_6519));
						STRING_SET(BgL_stringz00_7036, BgL_kz00_7037,
							((unsigned char) 'V'));
					}
					{	/* Unsafe/intext.scm 720 */
						obj_t BgL_auxz00_7038;

						BgL_auxz00_7038 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6519));
						CELL_SET(BgL_ptrz00_6519, BgL_auxz00_7038);
					}
					BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6517,
						BgL_printzd2pairzd2_6516, BgL_printzd2objectzd2_6515,
						BgL_printzd2classzd2_6514, BgL_printzd2cellzd2_6513,
						BgL_printzd2weakptrzd2_6512, BgL_printzd2vectorzd2_6511,
						BgL_printzd2hvectorzd2_6510, BgL_envz00_6506,
						BgL_printzd2customzd2_6509, BgL_refz00_6508, BgL_tablez00_6507,
						BgL_ptrz00_6519, BgL_bufferz00_6518,
						BGl_tvectorzd2idzd2zz__tvectorz00(BgL_itemz00_6520));
					return BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6517,
						BgL_printzd2pairzd2_6516, BgL_printzd2objectzd2_6515,
						BgL_printzd2classzd2_6514, BgL_printzd2cellzd2_6513,
						BgL_printzd2weakptrzd2_6512, BgL_printzd2vectorzd2_6511,
						BgL_printzd2hvectorzd2_6510, BgL_envz00_6506,
						BgL_printzd2customzd2_6509, BgL_refz00_6508, BgL_tablez00_6507,
						BgL_ptrz00_6519, BgL_bufferz00_6518, BgL_vz00_7035);
				}
			}
		}

	}



/* &print-custom */
	obj_t BGl_z62printzd2customzb0zz__intextz00(obj_t BgL_envz00_6523,
		obj_t BgL_itemz00_6526, obj_t BgL_markz00_6527)
	{
		{	/* Unsafe/intext.scm 1031 */
			{	/* Unsafe/intext.scm 1031 */
				obj_t BgL_bufferz00_6524;
				obj_t BgL_ptrz00_6525;

				BgL_bufferz00_6524 = PROCEDURE_L_REF(BgL_envz00_6523, (int) (0L));
				BgL_ptrz00_6525 = PROCEDURE_L_REF(BgL_envz00_6523, (int) (1L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6524,
					BgL_ptrz00_6525, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_7039;
					long BgL_kz00_7040;

					BgL_stringz00_7039 = CELL_REF(BgL_bufferz00_6524);
					BgL_kz00_7040 = (long) CINT(CELL_REF(BgL_ptrz00_6525));
					STRING_SET(BgL_stringz00_7039, BgL_kz00_7040, ((unsigned char) '+'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_7041;

					BgL_auxz00_7041 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6525));
					CELL_SET(BgL_ptrz00_6525, BgL_auxz00_7041);
				}
				{	/* Unsafe/intext.scm 1032 */
					char *BgL_identz00_7042;

					BgL_identz00_7042 = CUSTOM_IDENTIFIER(((obj_t) BgL_itemz00_6526));
					{	/* Unsafe/intext.scm 1032 */

						{	/* Unsafe/intext.scm 1033 */
							long BgL_arg1874z00_7043;

							BgL_arg1874z00_7043 =
								STRING_LENGTH(string_to_bstring(BgL_identz00_7042));
							BGl_z62printzd2wordzb0zz__intextz00(BgL_ptrz00_6525,
								BgL_bufferz00_6524, BgL_arg1874z00_7043);
							BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6524,
								BgL_ptrz00_6525, BINT(BgL_arg1874z00_7043));
							{	/* Unsafe/intext.scm 726 */
								obj_t BgL_s1z00_7044;
								obj_t BgL_s2z00_7045;
								long BgL_o2z00_7046;

								BgL_s1z00_7044 = string_to_bstring(BgL_identz00_7042);
								BgL_s2z00_7045 = CELL_REF(BgL_bufferz00_6524);
								BgL_o2z00_7046 = (long) CINT(CELL_REF(BgL_ptrz00_6525));
								blit_string(BgL_s1z00_7044, 0L, BgL_s2z00_7045, BgL_o2z00_7046,
									BgL_arg1874z00_7043);
							}
							{	/* Unsafe/intext.scm 727 */
								obj_t BgL_auxz00_7047;

								BgL_auxz00_7047 =
									ADDFX(CELL_REF(BgL_ptrz00_6525), BINT(BgL_arg1874z00_7043));
								CELL_SET(BgL_ptrz00_6525, BgL_auxz00_7047);
						}}
						{	/* Unsafe/intext.scm 1034 */
							obj_t BgL_sz00_7048;

							BgL_sz00_7048 =
								STRUCT_REF(((obj_t) BgL_markz00_6527), (int) (1L));
							{	/* Unsafe/intext.scm 1035 */
								long BgL_arg1875z00_7049;

								BgL_arg1875z00_7049 = STRING_LENGTH(((obj_t) BgL_sz00_7048));
								BGl_z62printzd2wordzb0zz__intextz00(BgL_ptrz00_6525,
									BgL_bufferz00_6524, BgL_arg1875z00_7049);
								BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6524,
									BgL_ptrz00_6525, BINT(BgL_arg1875z00_7049));
								{	/* Unsafe/intext.scm 726 */
									obj_t BgL_s2z00_7050;
									long BgL_o2z00_7051;

									BgL_s2z00_7050 = CELL_REF(BgL_bufferz00_6524);
									BgL_o2z00_7051 = (long) CINT(CELL_REF(BgL_ptrz00_6525));
									blit_string(
										((obj_t) BgL_sz00_7048), 0L, BgL_s2z00_7050, BgL_o2z00_7051,
										BgL_arg1875z00_7049);
								}
								{	/* Unsafe/intext.scm 727 */
									obj_t BgL_auxz00_7052;

									BgL_auxz00_7052 =
										ADDFX(CELL_REF(BgL_ptrz00_6525), BINT(BgL_arg1875z00_7049));
									return CELL_SET(BgL_ptrz00_6525, BgL_auxz00_7052);
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1945> */
	obj_t BGl_z62zc3z04anonymousza31945ze3ze5zz__intextz00(obj_t BgL_envz00_6531,
		obj_t BgL_iz00_6546, obj_t BgL_mz00_6547)
	{
		{	/* Unsafe/intext.scm 1140 */
			{	/* Unsafe/intext.scm 1039 */
				obj_t BgL_tablez00_6532;
				obj_t BgL_refz00_6533;
				obj_t BgL_printzd2customzd2_6534;
				obj_t BgL_printzd2tvectorzd2_6535;
				obj_t BgL_printzd2hvectorzd2_6536;
				obj_t BgL_printzd2vectorzd2_6537;
				obj_t BgL_printzd2weakptrzd2_6538;
				obj_t BgL_printzd2cellzd2_6539;
				obj_t BgL_printzd2classzd2_6540;
				obj_t BgL_printzd2objectzd2_6541;
				obj_t BgL_printzd2pairzd2_6542;
				obj_t BgL_printzd2epairzd2_6543;
				obj_t BgL_bufferz00_6544;
				obj_t BgL_ptrz00_6545;

				BgL_tablez00_6532 = PROCEDURE_L_REF(BgL_envz00_6531, (int) (0L));
				BgL_refz00_6533 = PROCEDURE_L_REF(BgL_envz00_6531, (int) (1L));
				BgL_printzd2customzd2_6534 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (2L)));
				BgL_printzd2tvectorzd2_6535 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (3L)));
				BgL_printzd2hvectorzd2_6536 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (4L)));
				BgL_printzd2vectorzd2_6537 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (5L)));
				BgL_printzd2weakptrzd2_6538 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (6L)));
				BgL_printzd2cellzd2_6539 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (7L)));
				BgL_printzd2classzd2_6540 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (8L)));
				BgL_printzd2objectzd2_6541 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (9L)));
				BgL_printzd2pairzd2_6542 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (10L)));
				BgL_printzd2epairzd2_6543 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6531, (int) (11L)));
				BgL_bufferz00_6544 = PROCEDURE_L_REF(BgL_envz00_6531, (int) (12L));
				BgL_ptrz00_6545 = PROCEDURE_L_REF(BgL_envz00_6531, (int) (13L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6544,
					BgL_ptrz00_6545, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_7053;
					long BgL_kz00_7054;

					BgL_stringz00_7053 = CELL_REF(BgL_bufferz00_6544);
					BgL_kz00_7054 = (long) CINT(CELL_REF(BgL_ptrz00_6545));
					STRING_SET(BgL_stringz00_7053, BgL_kz00_7054, ((unsigned char) 'p'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_7055;

					BgL_auxz00_7055 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6545));
					CELL_SET(BgL_ptrz00_6545, BgL_auxz00_7055);
				}
				{	/* Unsafe/intext.scm 1040 */
					obj_t BgL_arg1877z00_7056;

					BgL_arg1877z00_7056 = STRUCT_REF(((obj_t) BgL_mz00_6547), (int) (1L));
					return
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6543,
						BgL_printzd2pairzd2_6542, BgL_printzd2objectzd2_6541,
						BgL_printzd2classzd2_6540, BgL_printzd2cellzd2_6539,
						BgL_printzd2weakptrzd2_6538, BgL_printzd2vectorzd2_6537,
						BgL_printzd2hvectorzd2_6536, BgL_printzd2tvectorzd2_6535,
						BgL_printzd2customzd2_6534, BgL_refz00_6533, BgL_tablez00_6532,
						BgL_ptrz00_6545, BgL_bufferz00_6544, BgL_arg1877z00_7056);
				}
			}
		}

	}



/* &<@anonymous:1948> */
	obj_t BGl_z62zc3z04anonymousza31948ze3ze5zz__intextz00(obj_t BgL_envz00_6549,
		obj_t BgL_iz00_6564, obj_t BgL_mz00_6565)
	{
		{	/* Unsafe/intext.scm 1142 */
			{	/* Unsafe/intext.scm 1039 */
				obj_t BgL_tablez00_6550;
				obj_t BgL_refz00_6551;
				obj_t BgL_printzd2customzd2_6552;
				obj_t BgL_printzd2tvectorzd2_6553;
				obj_t BgL_printzd2hvectorzd2_6554;
				obj_t BgL_printzd2vectorzd2_6555;
				obj_t BgL_printzd2weakptrzd2_6556;
				obj_t BgL_printzd2cellzd2_6557;
				obj_t BgL_printzd2classzd2_6558;
				obj_t BgL_printzd2objectzd2_6559;
				obj_t BgL_printzd2pairzd2_6560;
				obj_t BgL_printzd2epairzd2_6561;
				obj_t BgL_bufferz00_6562;
				obj_t BgL_ptrz00_6563;

				BgL_tablez00_6550 = PROCEDURE_L_REF(BgL_envz00_6549, (int) (0L));
				BgL_refz00_6551 = PROCEDURE_L_REF(BgL_envz00_6549, (int) (1L));
				BgL_printzd2customzd2_6552 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (2L)));
				BgL_printzd2tvectorzd2_6553 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (3L)));
				BgL_printzd2hvectorzd2_6554 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (4L)));
				BgL_printzd2vectorzd2_6555 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (5L)));
				BgL_printzd2weakptrzd2_6556 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (6L)));
				BgL_printzd2cellzd2_6557 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (7L)));
				BgL_printzd2classzd2_6558 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (8L)));
				BgL_printzd2objectzd2_6559 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (9L)));
				BgL_printzd2pairzd2_6560 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (10L)));
				BgL_printzd2epairzd2_6561 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6549, (int) (11L)));
				BgL_bufferz00_6562 = PROCEDURE_L_REF(BgL_envz00_6549, (int) (12L));
				BgL_ptrz00_6563 = PROCEDURE_L_REF(BgL_envz00_6549, (int) (13L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6562,
					BgL_ptrz00_6563, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_7057;
					long BgL_kz00_7058;

					BgL_stringz00_7057 = CELL_REF(BgL_bufferz00_6562);
					BgL_kz00_7058 = (long) CINT(CELL_REF(BgL_ptrz00_6563));
					STRING_SET(BgL_stringz00_7057, BgL_kz00_7058, ((unsigned char) 'e'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_7059;

					BgL_auxz00_7059 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6563));
					CELL_SET(BgL_ptrz00_6563, BgL_auxz00_7059);
				}
				{	/* Unsafe/intext.scm 1040 */
					obj_t BgL_arg1877z00_7060;

					BgL_arg1877z00_7060 = STRUCT_REF(((obj_t) BgL_mz00_6565), (int) (1L));
					return
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6561,
						BgL_printzd2pairzd2_6560, BgL_printzd2objectzd2_6559,
						BgL_printzd2classzd2_6558, BgL_printzd2cellzd2_6557,
						BgL_printzd2weakptrzd2_6556, BgL_printzd2vectorzd2_6555,
						BgL_printzd2hvectorzd2_6554, BgL_printzd2tvectorzd2_6553,
						BgL_printzd2customzd2_6552, BgL_refz00_6551, BgL_tablez00_6550,
						BgL_ptrz00_6563, BgL_bufferz00_6562, BgL_arg1877z00_7060);
				}
			}
		}

	}



/* &<@anonymous:1951> */
	obj_t BGl_z62zc3z04anonymousza31951ze3ze5zz__intextz00(obj_t BgL_envz00_6567,
		obj_t BgL_iz00_6582, obj_t BgL_mz00_6583)
	{
		{	/* Unsafe/intext.scm 1144 */
			{	/* Unsafe/intext.scm 1039 */
				obj_t BgL_tablez00_6568;
				obj_t BgL_refz00_6569;
				obj_t BgL_printzd2customzd2_6570;
				obj_t BgL_printzd2tvectorzd2_6571;
				obj_t BgL_printzd2hvectorzd2_6572;
				obj_t BgL_printzd2vectorzd2_6573;
				obj_t BgL_printzd2weakptrzd2_6574;
				obj_t BgL_printzd2cellzd2_6575;
				obj_t BgL_printzd2classzd2_6576;
				obj_t BgL_printzd2objectzd2_6577;
				obj_t BgL_printzd2pairzd2_6578;
				obj_t BgL_printzd2epairzd2_6579;
				obj_t BgL_bufferz00_6580;
				obj_t BgL_ptrz00_6581;

				BgL_tablez00_6568 = PROCEDURE_L_REF(BgL_envz00_6567, (int) (0L));
				BgL_refz00_6569 = PROCEDURE_L_REF(BgL_envz00_6567, (int) (1L));
				BgL_printzd2customzd2_6570 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (2L)));
				BgL_printzd2tvectorzd2_6571 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (3L)));
				BgL_printzd2hvectorzd2_6572 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (4L)));
				BgL_printzd2vectorzd2_6573 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (5L)));
				BgL_printzd2weakptrzd2_6574 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (6L)));
				BgL_printzd2cellzd2_6575 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (7L)));
				BgL_printzd2classzd2_6576 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (8L)));
				BgL_printzd2objectzd2_6577 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (9L)));
				BgL_printzd2pairzd2_6578 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (10L)));
				BgL_printzd2epairzd2_6579 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6567, (int) (11L)));
				BgL_bufferz00_6580 = PROCEDURE_L_REF(BgL_envz00_6567, (int) (12L));
				BgL_ptrz00_6581 = PROCEDURE_L_REF(BgL_envz00_6567, (int) (13L));
				BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6580,
					BgL_ptrz00_6581, BINT(1L));
				{	/* Unsafe/intext.scm 719 */
					obj_t BgL_stringz00_7061;
					long BgL_kz00_7062;

					BgL_stringz00_7061 = CELL_REF(BgL_bufferz00_6580);
					BgL_kz00_7062 = (long) CINT(CELL_REF(BgL_ptrz00_6581));
					STRING_SET(BgL_stringz00_7061, BgL_kz00_7062, ((unsigned char) 'o'));
				}
				{	/* Unsafe/intext.scm 720 */
					obj_t BgL_auxz00_7063;

					BgL_auxz00_7063 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6581));
					CELL_SET(BgL_ptrz00_6581, BgL_auxz00_7063);
				}
				{	/* Unsafe/intext.scm 1040 */
					obj_t BgL_arg1877z00_7064;

					BgL_arg1877z00_7064 = STRUCT_REF(((obj_t) BgL_mz00_6583), (int) (1L));
					return
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6579,
						BgL_printzd2pairzd2_6578, BgL_printzd2objectzd2_6577,
						BgL_printzd2classzd2_6576, BgL_printzd2cellzd2_6575,
						BgL_printzd2weakptrzd2_6574, BgL_printzd2vectorzd2_6573,
						BgL_printzd2hvectorzd2_6572, BgL_printzd2tvectorzd2_6571,
						BgL_printzd2customzd2_6570, BgL_refz00_6569, BgL_tablez00_6568,
						BgL_ptrz00_6581, BgL_bufferz00_6580, BgL_arg1877z00_7064);
				}
			}
		}

	}



/* &<@anonymous:1955> */
	obj_t BGl_z62zc3z04anonymousza31955ze3ze5zz__intextz00(obj_t BgL_envz00_6585,
		obj_t BgL_itemz00_6600, obj_t BgL_markz00_6601)
	{
		{	/* Unsafe/intext.scm 1149 */
			{	/* Unsafe/intext.scm 955 */
				obj_t BgL_tablez00_6586;
				obj_t BgL_refz00_6587;
				obj_t BgL_printzd2customzd2_6588;
				obj_t BgL_printzd2tvectorzd2_6589;
				obj_t BgL_printzd2hvectorzd2_6590;
				obj_t BgL_printzd2vectorzd2_6591;
				obj_t BgL_printzd2weakptrzd2_6592;
				obj_t BgL_printzd2cellzd2_6593;
				obj_t BgL_printzd2classzd2_6594;
				obj_t BgL_printzd2objectzd2_6595;
				obj_t BgL_printzd2pairzd2_6596;
				obj_t BgL_printzd2epairzd2_6597;
				obj_t BgL_bufferz00_6598;
				obj_t BgL_ptrz00_6599;

				BgL_tablez00_6586 = PROCEDURE_L_REF(BgL_envz00_6585, (int) (0L));
				BgL_refz00_6587 = PROCEDURE_L_REF(BgL_envz00_6585, (int) (1L));
				BgL_printzd2customzd2_6588 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (2L)));
				BgL_printzd2tvectorzd2_6589 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (3L)));
				BgL_printzd2hvectorzd2_6590 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (4L)));
				BgL_printzd2vectorzd2_6591 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (5L)));
				BgL_printzd2weakptrzd2_6592 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (6L)));
				BgL_printzd2cellzd2_6593 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (7L)));
				BgL_printzd2classzd2_6594 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (8L)));
				BgL_printzd2objectzd2_6595 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (9L)));
				BgL_printzd2pairzd2_6596 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (10L)));
				BgL_printzd2epairzd2_6597 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6585, (int) (11L)));
				BgL_bufferz00_6598 = PROCEDURE_L_REF(BgL_envz00_6585, (int) (12L));
				BgL_ptrz00_6599 = PROCEDURE_L_REF(BgL_envz00_6585, (int) (13L));
				{	/* Unsafe/intext.scm 955 */
					bool_t BgL_tmpz00_10241;

					BGl_z62checkzd2bufferz12za2zz__intextz00(BgL_bufferz00_6598,
						BgL_ptrz00_6599, BINT(1L));
					{	/* Unsafe/intext.scm 719 */
						obj_t BgL_stringz00_7065;
						long BgL_kz00_7066;

						BgL_stringz00_7065 = CELL_REF(BgL_bufferz00_6598);
						BgL_kz00_7066 = (long) CINT(CELL_REF(BgL_ptrz00_6599));
						STRING_SET(BgL_stringz00_7065, BgL_kz00_7066,
							((unsigned char) '{'));
					}
					{	/* Unsafe/intext.scm 720 */
						obj_t BgL_auxz00_7067;

						BgL_auxz00_7067 = ADDFX(BINT(1L), CELL_REF(BgL_ptrz00_6599));
						CELL_SET(BgL_ptrz00_6599, BgL_auxz00_7067);
					}
					{	/* Unsafe/intext.scm 956 */
						obj_t BgL_arg1796z00_7068;

						{	/* Unsafe/intext.scm 956 */
							obj_t BgL_res2513z00_7069;

							BgL_res2513z00_7069 = STRUCT_KEY(((obj_t) BgL_itemz00_6600));
							BgL_arg1796z00_7068 = BgL_res2513z00_7069;
						}
						BGl_z62printzd2itemzb0zz__intextz00(BgL_printzd2epairzd2_6597,
							BgL_printzd2pairzd2_6596, BgL_printzd2objectzd2_6595,
							BgL_printzd2classzd2_6594, BgL_printzd2cellzd2_6593,
							BgL_printzd2weakptrzd2_6592, BgL_printzd2vectorzd2_6591,
							BgL_printzd2hvectorzd2_6590, BgL_printzd2tvectorzd2_6589,
							BgL_printzd2customzd2_6588, BgL_refz00_6587, BgL_tablez00_6586,
							BgL_ptrz00_6599, BgL_bufferz00_6598, BgL_arg1796z00_7068);
					}
					{	/* Unsafe/intext.scm 957 */
						int BgL_lenz00_7070;

						BgL_lenz00_7070 = STRUCT_LENGTH(((obj_t) BgL_itemz00_6600));
						{	/* Unsafe/intext.scm 793 */
							long BgL_siza7eza7_7071;

							BgL_siza7eza7_7071 =
								BGl_siza7ezd2ofzd2wordza7zz__intextz00(
								(long) (BgL_lenz00_7070));
							if ((BgL_siza7eza7_7071 == 0L))
								{	/* Unsafe/intext.scm 794 */
									BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6599,
										BgL_bufferz00_6598, (0L));
								}
							else
								{	/* Unsafe/intext.scm 794 */
									BGl_z62z12printzd2markupza2zz__intextz00(BgL_ptrz00_6599,
										BgL_bufferz00_6598, (BgL_siza7eza7_7071));
									BBOOL(BGl_z62printzd2wordzf2siza7eze5zz__intextz00
										(BgL_ptrz00_6599, BgL_bufferz00_6598, BINT(BgL_lenz00_7070),
											BgL_siza7eza7_7071));
								}
						}
						{
							long BgL_iz00_7073;

							BgL_iz00_7073 = 0L;
						BgL_for1076z00_7072:
							if ((BgL_iz00_7073 < (long) (BgL_lenz00_7070)))
								{	/* Unsafe/intext.scm 959 */
									{	/* Unsafe/intext.scm 959 */
										obj_t BgL_arg1799z00_7074;

										BgL_arg1799z00_7074 =
											STRUCT_REF(
											((obj_t) BgL_itemz00_6600), (int) (BgL_iz00_7073));
										BGl_z62printzd2itemzb0zz__intextz00
											(BgL_printzd2epairzd2_6597, BgL_printzd2pairzd2_6596,
											BgL_printzd2objectzd2_6595, BgL_printzd2classzd2_6594,
											BgL_printzd2cellzd2_6593, BgL_printzd2weakptrzd2_6592,
											BgL_printzd2vectorzd2_6591, BgL_printzd2hvectorzd2_6590,
											BgL_printzd2tvectorzd2_6589, BgL_printzd2customzd2_6588,
											BgL_refz00_6587, BgL_tablez00_6586, BgL_ptrz00_6599,
											BgL_bufferz00_6598, BgL_arg1799z00_7074);
									}
									{
										long BgL_iz00_10271;

										BgL_iz00_10271 = (BgL_iz00_7073 + 1L);
										BgL_iz00_7073 = BgL_iz00_10271;
										goto BgL_for1076z00_7072;
									}
								}
							else
								{	/* Unsafe/intext.scm 959 */
									BgL_tmpz00_10241 = ((bool_t) 0);
								}
						}
					}
					return BBOOL(BgL_tmpz00_10241);
				}
			}
		}

	}



/* mark-obj! */
	long BGl_markzd2objz12zc0zz__intextz00(obj_t BgL_tablez00_55,
		obj_t BgL_objz00_56, obj_t BgL_markzd2argzd2_57)
	{
		{	/* Unsafe/intext.scm 1168 */
			{	/* Unsafe/intext.scm 1168 */
				struct bgl_cell BgL_box2989_6672z00;
				obj_t BgL_nbrefz00_6672;

				BgL_nbrefz00_6672 = MAKE_CELL_STACK(BINT(0L), BgL_box2989_6672z00);
				BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6672, BgL_markzd2argzd2_57,
					BgL_tablez00_55, BgL_objz00_56);
				return (long) CINT(((obj_t) ((obj_t) CELL_REF(BgL_nbrefz00_6672))));
		}}

	}



/* mark~0 */
	obj_t BGl_markze70ze7zz__intextz00(obj_t BgL_nbrefz00_6670,
		obj_t BgL_markzd2argzd2_6669, obj_t BgL_tablez00_6668,
		obj_t BgL_objz00_2833)
	{
		{	/* Unsafe/intext.scm 1346 */
		BGl_markze70ze7zz__intextz00:
			{
				obj_t BgL_mz00_2718;
				obj_t BgL_objz00_2728;
				obj_t BgL_objz00_2735;
				obj_t BgL_objz00_2741;
				obj_t BgL_objz00_2767;
				obj_t BgL_objz00_2783;
				obj_t BgL_objz00_2794;
				obj_t BgL_objz00_2797;
				obj_t BgL_objz00_2800;
				obj_t BgL_objz00_2810;
				obj_t BgL_objz00_2818;
				obj_t BgL_objz00_2821;
				obj_t BgL_objz00_2826;

				{	/* Unsafe/intext.scm 1308 */
					bool_t BgL_test2990z00_10278;

					if (EPAIRP(BgL_objz00_2833))
						{	/* Unsafe/intext.scm 1308 */
							BgL_test2990z00_10278 = BGl_za2epairzf3za2zf3zz__intextz00;
						}
					else
						{	/* Unsafe/intext.scm 1308 */
							BgL_test2990z00_10278 = ((bool_t) 0);
						}
					if (BgL_test2990z00_10278)
						{	/* Unsafe/intext.scm 1184 */
							obj_t BgL_mz00_5633;

							BgL_mz00_5633 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6668,
								BgL_objz00_2833);
							{	/* Unsafe/intext.scm 1185 */
								bool_t BgL_test2992z00_10282;

								if (STRUCTP(BgL_mz00_5633))
									{	/* Unsafe/intext.scm 663 */
										BgL_test2992z00_10282 =
											(STRUCT_KEY(BgL_mz00_5633) ==
											BGl_symbol2667z00zz__intextz00);
									}
								else
									{	/* Unsafe/intext.scm 663 */
										BgL_test2992z00_10282 = ((bool_t) 0);
									}
								if (BgL_test2992z00_10282)
									{	/* Unsafe/intext.scm 1185 */
										BgL_mz00_2718 = BgL_mz00_5633;
									BgL_zc3z04anonymousza31958ze3z87_2719:
										{	/* Unsafe/intext.scm 1178 */
											obj_t BgL_omz00_2720;

											BgL_omz00_2720 = STRUCT_REF(BgL_mz00_2718, (int) (2L));
											{	/* Unsafe/intext.scm 1179 */
												long BgL_arg1959z00_2721;

												BgL_arg1959z00_2721 =
													(1L + (long) CINT(BgL_omz00_2720));
												{	/* Unsafe/intext.scm 663 */
													obj_t BgL_auxz00_10293;
													int BgL_tmpz00_10291;

													BgL_auxz00_10293 = BINT(BgL_arg1959z00_2721);
													BgL_tmpz00_10291 = (int) (2L);
													STRUCT_SET(BgL_mz00_2718, BgL_tmpz00_10291,
														BgL_auxz00_10293);
											}}
											if (((long) CINT(BgL_omz00_2720) == 0L))
												{	/* Unsafe/intext.scm 1180 */
													obj_t BgL_auxz00_6671;

													BgL_auxz00_6671 =
														ADDFX(
														((obj_t)
															((obj_t) CELL_REF(BgL_nbrefz00_6670))), BINT(1L));
													return CELL_SET(BgL_nbrefz00_6670, BgL_auxz00_6671);
												}
											else
												{	/* Unsafe/intext.scm 1180 */
													return BFALSE;
												}
										}
									}
								else
									{	/* Unsafe/intext.scm 1185 */
										BgL_objz00_2728 = BgL_objz00_2833;
										{	/* Unsafe/intext.scm 1191 */
											obj_t BgL_mz00_2730;

											BgL_mz00_2730 =
												BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6668,
												BgL_objz00_2728);
											{	/* Unsafe/intext.scm 1192 */
												bool_t BgL_test2995z00_10303;

												if (STRUCTP(BgL_mz00_2730))
													{	/* Unsafe/intext.scm 663 */
														BgL_test2995z00_10303 =
															(STRUCT_KEY(BgL_mz00_2730) ==
															BGl_symbol2667z00zz__intextz00);
													}
												else
													{	/* Unsafe/intext.scm 663 */
														BgL_test2995z00_10303 = ((bool_t) 0);
													}
												if (BgL_test2995z00_10303)
													{
														obj_t BgL_mz00_10308;

														BgL_mz00_10308 = BgL_mz00_2730;
														BgL_mz00_2718 = BgL_mz00_10308;
														goto BgL_zc3z04anonymousza31958ze3z87_2719;
													}
												else
													{	/* Unsafe/intext.scm 1192 */
														{	/* Unsafe/intext.scm 675 */
															obj_t BgL_arg1640z00_5442;

															{	/* Unsafe/intext.scm 663 */
																obj_t BgL_newz00_5443;

																BgL_newz00_5443 =
																	create_struct(BGl_symbol2667z00zz__intextz00,
																	(int) (4L));
																{	/* Unsafe/intext.scm 663 */
																	obj_t BgL_auxz00_10313;
																	int BgL_tmpz00_10311;

																	BgL_auxz00_10313 = BINT(-1L);
																	BgL_tmpz00_10311 = (int) (3L);
																	STRUCT_SET(BgL_newz00_5443, BgL_tmpz00_10311,
																		BgL_auxz00_10313);
																}
																{	/* Unsafe/intext.scm 663 */
																	obj_t BgL_auxz00_10318;
																	int BgL_tmpz00_10316;

																	BgL_auxz00_10318 = BINT(0L);
																	BgL_tmpz00_10316 = (int) (2L);
																	STRUCT_SET(BgL_newz00_5443, BgL_tmpz00_10316,
																		BgL_auxz00_10318);
																}
																{	/* Unsafe/intext.scm 663 */
																	int BgL_tmpz00_10321;

																	BgL_tmpz00_10321 = (int) (1L);
																	STRUCT_SET(BgL_newz00_5443, BgL_tmpz00_10321,
																		BFALSE);
																}
																{	/* Unsafe/intext.scm 663 */
																	int BgL_tmpz00_10324;

																	BgL_tmpz00_10324 = (int) (0L);
																	STRUCT_SET(BgL_newz00_5443, BgL_tmpz00_10324,
																		BgL_objz00_2728);
																}
																BgL_arg1640z00_5442 = BgL_newz00_5443;
															}
															BGl_hashtablezd2putz12zc0zz__hashz00
																(BgL_tablez00_6668, BgL_objz00_2728,
																BgL_arg1640z00_5442);
														}
														{	/* Unsafe/intext.scm 1196 */
															obj_t BgL_arg1965z00_2732;

															BgL_arg1965z00_2732 =
																CAR(((obj_t) BgL_objz00_2728));
															BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6670,
																BgL_markzd2argzd2_6669, BgL_tablez00_6668,
																BgL_arg1965z00_2732);
														}
														{	/* Unsafe/intext.scm 1197 */
															obj_t BgL_arg1966z00_2733;

															BgL_arg1966z00_2733 =
																CDR(((obj_t) BgL_objz00_2728));
															BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6670,
																BgL_markzd2argzd2_6669, BgL_tablez00_6668,
																BgL_arg1966z00_2733);
														}
														{	/* Unsafe/intext.scm 1198 */
															obj_t BgL_arg1967z00_2734;

															BgL_arg1967z00_2734 =
																CER(((obj_t) BgL_objz00_2728));
															{
																obj_t BgL_objz00_10336;

																BgL_objz00_10336 = BgL_arg1967z00_2734;
																BgL_objz00_2833 = BgL_objz00_10336;
																goto BGl_markze70ze7zz__intextz00;
															}
														}
													}
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/intext.scm 1308 */
							if (PAIRP(BgL_objz00_2833))
								{	/* Unsafe/intext.scm 1184 */
									obj_t BgL_mz00_5639;

									BgL_mz00_5639 =
										BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6668,
										BgL_objz00_2833);
									{	/* Unsafe/intext.scm 1185 */
										bool_t BgL_test2998z00_10340;

										if (STRUCTP(BgL_mz00_5639))
											{	/* Unsafe/intext.scm 663 */
												BgL_test2998z00_10340 =
													(STRUCT_KEY(BgL_mz00_5639) ==
													BGl_symbol2667z00zz__intextz00);
											}
										else
											{	/* Unsafe/intext.scm 663 */
												BgL_test2998z00_10340 = ((bool_t) 0);
											}
										if (BgL_test2998z00_10340)
											{
												obj_t BgL_mz00_10345;

												BgL_mz00_10345 = BgL_mz00_5639;
												BgL_mz00_2718 = BgL_mz00_10345;
												goto BgL_zc3z04anonymousza31958ze3z87_2719;
											}
										else
											{	/* Unsafe/intext.scm 1185 */
												BgL_objz00_2735 = BgL_objz00_2833;
												{	/* Unsafe/intext.scm 1202 */
													obj_t BgL_mz00_2737;

													BgL_mz00_2737 =
														BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6668,
														BgL_objz00_2735);
													{	/* Unsafe/intext.scm 1203 */
														bool_t BgL_test3000z00_10347;

														if (STRUCTP(BgL_mz00_2737))
															{	/* Unsafe/intext.scm 663 */
																BgL_test3000z00_10347 =
																	(STRUCT_KEY(BgL_mz00_2737) ==
																	BGl_symbol2667z00zz__intextz00);
															}
														else
															{	/* Unsafe/intext.scm 663 */
																BgL_test3000z00_10347 = ((bool_t) 0);
															}
														if (BgL_test3000z00_10347)
															{
																obj_t BgL_mz00_10352;

																BgL_mz00_10352 = BgL_mz00_2737;
																BgL_mz00_2718 = BgL_mz00_10352;
																goto BgL_zc3z04anonymousza31958ze3z87_2719;
															}
														else
															{	/* Unsafe/intext.scm 1203 */
																{	/* Unsafe/intext.scm 675 */
																	obj_t BgL_arg1640z00_5455;

																	{	/* Unsafe/intext.scm 663 */
																		obj_t BgL_newz00_5456;

																		BgL_newz00_5456 =
																			create_struct
																			(BGl_symbol2667z00zz__intextz00,
																			(int) (4L));
																		{	/* Unsafe/intext.scm 663 */
																			obj_t BgL_auxz00_10357;
																			int BgL_tmpz00_10355;

																			BgL_auxz00_10357 = BINT(-1L);
																			BgL_tmpz00_10355 = (int) (3L);
																			STRUCT_SET(BgL_newz00_5456,
																				BgL_tmpz00_10355, BgL_auxz00_10357);
																		}
																		{	/* Unsafe/intext.scm 663 */
																			obj_t BgL_auxz00_10362;
																			int BgL_tmpz00_10360;

																			BgL_auxz00_10362 = BINT(0L);
																			BgL_tmpz00_10360 = (int) (2L);
																			STRUCT_SET(BgL_newz00_5456,
																				BgL_tmpz00_10360, BgL_auxz00_10362);
																		}
																		{	/* Unsafe/intext.scm 663 */
																			int BgL_tmpz00_10365;

																			BgL_tmpz00_10365 = (int) (1L);
																			STRUCT_SET(BgL_newz00_5456,
																				BgL_tmpz00_10365, BFALSE);
																		}
																		{	/* Unsafe/intext.scm 663 */
																			int BgL_tmpz00_10368;

																			BgL_tmpz00_10368 = (int) (0L);
																			STRUCT_SET(BgL_newz00_5456,
																				BgL_tmpz00_10368, BgL_objz00_2735);
																		}
																		BgL_arg1640z00_5455 = BgL_newz00_5456;
																	}
																	BGl_hashtablezd2putz12zc0zz__hashz00
																		(BgL_tablez00_6668, BgL_objz00_2735,
																		BgL_arg1640z00_5455);
																}
																{	/* Unsafe/intext.scm 1207 */
																	obj_t BgL_arg1970z00_2739;

																	BgL_arg1970z00_2739 =
																		CAR(((obj_t) BgL_objz00_2735));
																	BGl_markze70ze7zz__intextz00
																		(BgL_nbrefz00_6670, BgL_markzd2argzd2_6669,
																		BgL_tablez00_6668, BgL_arg1970z00_2739);
																}
																{	/* Unsafe/intext.scm 1208 */
																	obj_t BgL_arg1971z00_2740;

																	BgL_arg1971z00_2740 =
																		CDR(((obj_t) BgL_objz00_2735));
																	{
																		obj_t BgL_objz00_10377;

																		BgL_objz00_10377 = BgL_arg1971z00_2740;
																		BgL_objz00_2833 = BgL_objz00_10377;
																		goto BGl_markze70ze7zz__intextz00;
																	}
																}
															}
													}
												}
											}
									}
								}
							else
								{	/* Unsafe/intext.scm 1310 */
									if (BGL_OBJECTP(BgL_objz00_2833))
										{	/* Unsafe/intext.scm 1184 */
											obj_t BgL_mz00_5645;

											BgL_mz00_5645 =
												BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_6668,
												BgL_objz00_2833);
											{	/* Unsafe/intext.scm 1185 */
												bool_t BgL_test3003z00_10381;

												if (STRUCTP(BgL_mz00_5645))
													{	/* Unsafe/intext.scm 663 */
														BgL_test3003z00_10381 =
															(STRUCT_KEY(BgL_mz00_5645) ==
															BGl_symbol2667z00zz__intextz00);
													}
												else
													{	/* Unsafe/intext.scm 663 */
														BgL_test3003z00_10381 = ((bool_t) 0);
													}
												if (BgL_test3003z00_10381)
													{
														obj_t BgL_mz00_10386;

														BgL_mz00_10386 = BgL_mz00_5645;
														BgL_mz00_2718 = BgL_mz00_10386;
														goto BgL_zc3z04anonymousza31958ze3z87_2719;
													}
												else
													{	/* Unsafe/intext.scm 1185 */
														BgL_objz00_2741 = BgL_objz00_2833;
														{	/* Unsafe/intext.scm 1212 */
															obj_t BgL_klassz00_2743;
															obj_t BgL_customz00_2744;

															{	/* Unsafe/intext.scm 1212 */
																obj_t BgL_arg2495z00_5464;
																long BgL_arg2497z00_5465;

																BgL_arg2495z00_5464 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Unsafe/intext.scm 1212 */
																	long BgL_arg2500z00_5466;

																	BgL_arg2500z00_5466 =
																		BGL_OBJECT_CLASS_NUM(
																		((BgL_objectz00_bglt) BgL_objz00_2741));
																	BgL_arg2497z00_5465 =
																		(BgL_arg2500z00_5466 - OBJECT_TYPE);
																}
																BgL_klassz00_2743 =
																	VECTOR_REF(BgL_arg2495z00_5464,
																	BgL_arg2497z00_5465);
															}
															BgL_customz00_2744 =
																BGl_objectzd2serializa7erz75zz__intextz00(
																((BgL_objectz00_bglt) BgL_objz00_2741),
																BgL_markzd2argzd2_6669);
															{	/* Unsafe/intext.scm 675 */
																obj_t BgL_arg1640z00_5472;

																{	/* Unsafe/intext.scm 663 */
																	obj_t BgL_newz00_5473;

																	BgL_newz00_5473 =
																		create_struct
																		(BGl_symbol2667z00zz__intextz00,
																		(int) (4L));
																	{	/* Unsafe/intext.scm 663 */
																		obj_t BgL_auxz00_10398;
																		int BgL_tmpz00_10396;

																		BgL_auxz00_10398 = BINT(-1L);
																		BgL_tmpz00_10396 = (int) (3L);
																		STRUCT_SET(BgL_newz00_5473,
																			BgL_tmpz00_10396, BgL_auxz00_10398);
																	}
																	{	/* Unsafe/intext.scm 663 */
																		obj_t BgL_auxz00_10403;
																		int BgL_tmpz00_10401;

																		BgL_auxz00_10403 = BINT(0L);
																		BgL_tmpz00_10401 = (int) (2L);
																		STRUCT_SET(BgL_newz00_5473,
																			BgL_tmpz00_10401, BgL_auxz00_10403);
																	}
																	{	/* Unsafe/intext.scm 663 */
																		int BgL_tmpz00_10406;

																		BgL_tmpz00_10406 = (int) (1L);
																		STRUCT_SET(BgL_newz00_5473,
																			BgL_tmpz00_10406, BgL_customz00_2744);
																	}
																	{	/* Unsafe/intext.scm 663 */
																		int BgL_tmpz00_10409;

																		BgL_tmpz00_10409 = (int) (0L);
																		STRUCT_SET(BgL_newz00_5473,
																			BgL_tmpz00_10409, BgL_objz00_2741);
																	}
																	BgL_arg1640z00_5472 = BgL_newz00_5473;
																}
																BGl_hashtablezd2putz12zc0zz__hashz00
																	(BgL_tablez00_6668, BgL_objz00_2741,
																	BgL_arg1640z00_5472);
															}
															BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6670,
																BgL_markzd2argzd2_6669, BgL_tablez00_6668,
																BgL_klassz00_2743);
															{	/* Unsafe/intext.scm 1216 */
																long BgL_arg1973z00_2745;

																BgL_arg1973z00_2745 =
																	BGl_classzd2hashzd2zz__objectz00
																	(BgL_klassz00_2743);
																BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6670,
																	BgL_markzd2argzd2_6669, BgL_tablez00_6668,
																	BINT(BgL_arg1973z00_2745));
															}
															{	/* Unsafe/intext.scm 1217 */
																obj_t BgL_arg1974z00_2746;

																BgL_arg1974z00_2746 =
																	BGl_classzd2namezd2zz__objectz00
																	(BgL_klassz00_2743);
																BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6670,
																	BgL_markzd2argzd2_6669, BgL_tablez00_6668,
																	BgL_arg1974z00_2746);
															}
															if ((BgL_objz00_2741 == BgL_customz00_2744))
																{	/* Unsafe/intext.scm 1220 */
																	obj_t BgL_fieldsz00_2747;

																	{	/* Unsafe/intext.scm 1220 */
																		obj_t BgL_tmpz00_10421;

																		BgL_tmpz00_10421 =
																			((obj_t) BgL_klassz00_2743);
																		BgL_fieldsz00_2747 =
																			BGL_CLASS_ALL_FIELDS(BgL_tmpz00_10421);
																	}
																	{	/* Unsafe/intext.scm 1221 */

																		{
																			long BgL_iz00_2750;

																			{	/* Unsafe/intext.scm 1222 */
																				bool_t BgL_tmpz00_10424;

																				BgL_iz00_2750 = 0L;
																			BgL_zc3z04anonymousza31975ze3z87_2751:
																				if (
																					(BgL_iz00_2750 <
																						VECTOR_LENGTH(BgL_fieldsz00_2747)))
																					{	/* Unsafe/intext.scm 1222 */
																						{	/* Unsafe/intext.scm 1223 */
																							obj_t BgL_fz00_2753;

																							BgL_fz00_2753 =
																								VECTOR_REF(BgL_fieldsz00_2747,
																								BgL_iz00_2750);
																							{	/* Unsafe/intext.scm 1223 */
																								obj_t BgL_ivz00_2754;

																								BgL_ivz00_2754 =
																									BGl_classzd2fieldzd2infoz00zz__objectz00
																									(BgL_fz00_2753);
																								{	/* Unsafe/intext.scm 1224 */

																									{	/* Unsafe/intext.scm 1226 */
																										obj_t BgL_g1090z00_2755;

																										if (PAIRP(BgL_ivz00_2754))
																											{	/* Unsafe/intext.scm 1226 */
																												BgL_g1090z00_2755 =
																													BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																													(BGl_keyword2662z00zz__intextz00,
																													BgL_ivz00_2754);
																											}
																										else
																											{	/* Unsafe/intext.scm 1226 */
																												BgL_g1090z00_2755 =
																													BFALSE;
																											}
																										if (CBOOL
																											(BgL_g1090z00_2755))
																											{	/* Unsafe/intext.scm 1229 */
																												bool_t
																													BgL_test3009z00_10435;
																												{	/* Unsafe/intext.scm 1229 */
																													obj_t
																														BgL_tmpz00_10436;
																													BgL_tmpz00_10436 =
																														CDR(((obj_t)
																															BgL_g1090z00_2755));
																													BgL_test3009z00_10435
																														=
																														PAIRP
																														(BgL_tmpz00_10436);
																												}
																												if (BgL_test3009z00_10435)
																													{	/* Unsafe/intext.scm 1229 */
																														obj_t
																															BgL_arg1979z00_2760;
																														{	/* Unsafe/intext.scm 1229 */
																															obj_t
																																BgL_pairz00_5488;
																															BgL_pairz00_5488 =
																																CDR(((obj_t)
																																	BgL_g1090z00_2755));
																															BgL_arg1979z00_2760
																																=
																																CAR
																																(BgL_pairz00_5488);
																														}
																														BGl_markze70ze7zz__intextz00
																															(BgL_nbrefz00_6670,
																															BgL_markzd2argzd2_6669,
																															BgL_tablez00_6668,
																															BgL_arg1979z00_2760);
																													}
																												else
																													{	/* Unsafe/intext.scm 1229 */
																														BFALSE;
																													}
																											}
																										else
																											{	/* Unsafe/intext.scm 1231 */
																												obj_t
																													BgL_arg1981z00_2762;
																												{	/* Unsafe/intext.scm 1231 */
																													obj_t
																														BgL_fun1982z00_2763;
																													BgL_fun1982z00_2763 =
																														BGl_classzd2fieldzd2accessorz00zz__objectz00
																														(BgL_fz00_2753);
																													BgL_arg1981z00_2762 =
																														BGL_PROCEDURE_CALL1
																														(BgL_fun1982z00_2763,
																														BgL_objz00_2741);
																												}
																												BGl_markze70ze7zz__intextz00
																													(BgL_nbrefz00_6670,
																													BgL_markzd2argzd2_6669,
																													BgL_tablez00_6668,
																													BgL_arg1981z00_2762);
																											}
																									}
																								}
																							}
																						}
																						{
																							long BgL_iz00_10450;

																							BgL_iz00_10450 =
																								(BgL_iz00_2750 + 1L);
																							BgL_iz00_2750 = BgL_iz00_10450;
																							goto
																								BgL_zc3z04anonymousza31975ze3z87_2751;
																						}
																					}
																				else
																					{	/* Unsafe/intext.scm 1222 */
																						BgL_tmpz00_10424 = ((bool_t) 0);
																					}
																				return BBOOL(BgL_tmpz00_10424);
																			}
																		}
																	}
																}
															else
																{
																	obj_t BgL_objz00_10453;

																	BgL_objz00_10453 = BgL_customz00_2744;
																	BgL_objz00_2833 = BgL_objz00_10453;
																	goto BGl_markze70ze7zz__intextz00;
																}
														}
													}
											}
										}
									else
										{	/* Unsafe/intext.scm 1312 */
											if (BGl_classzf3zf3zz__objectz00(BgL_objz00_2833))
												{	/* Unsafe/intext.scm 1314 */
													{	/* Unsafe/intext.scm 1315 */
														obj_t BgL_arg2022z00_2840;

														BgL_arg2022z00_2840 =
															SYMBOL_TO_STRING(BGl_classzd2namezd2zz__objectz00
															(BgL_objz00_2833));
														BGl_markze70ze7zz__intextz00(BgL_nbrefz00_6670,
															BgL_markzd2argzd2_6669, BgL_tablez00_6668,
															BgL_arg2022z00_2840);
													}
													{	/* Unsafe/intext.scm 1184 */
														obj_t BgL_mz00_5652;

														BgL_mz00_5652 =
															BGl_hashtablezd2getzd2zz__hashz00
															(BgL_tablez00_6668, BgL_objz00_2833);
														{	/* Unsafe/intext.scm 1185 */
															bool_t BgL_test3011z00_10460;

															if (STRUCTP(BgL_mz00_5652))
																{	/* Unsafe/intext.scm 663 */
																	BgL_test3011z00_10460 =
																		(STRUCT_KEY(BgL_mz00_5652) ==
																		BGl_symbol2667z00zz__intextz00);
																}
															else
																{	/* Unsafe/intext.scm 663 */
																	BgL_test3011z00_10460 = ((bool_t) 0);
																}
															if (BgL_test3011z00_10460)
																{
																	obj_t BgL_mz00_10465;

																	BgL_mz00_10465 = BgL_mz00_5652;
																	BgL_mz00_2718 = BgL_mz00_10465;
																	goto BgL_zc3z04anonymousza31958ze3z87_2719;
																}
															else
																{	/* Unsafe/intext.scm 1185 */
																	BgL_objz00_2767 = BgL_objz00_2833;
																	{	/* Unsafe/intext.scm 1236 */
																		obj_t BgL_fz00_2769;

																		{	/* Unsafe/intext.scm 1236 */
																			obj_t BgL_v1162z00_2770;

																			{	/* Unsafe/intext.scm 1236 */
																				obj_t BgL_tmpz00_10466;

																				BgL_tmpz00_10466 =
																					((obj_t) BgL_objz00_2767);
																				BgL_v1162z00_2770 =
																					BGL_CLASS_ALL_FIELDS
																					(BgL_tmpz00_10466);
																			}
																			{	/* Unsafe/intext.scm 1236 */
																				obj_t BgL_nv1163z00_2772;

																				{	/* Ieee/vector.scm 95 */

																					BgL_nv1163z00_2772 =
																						make_vector(VECTOR_LENGTH
																						(BgL_v1162z00_2770), BUNSPEC);
																				}
																				{	/* Unsafe/intext.scm 1236 */

																					{
																						long BgL_i1161z00_5501;

																						BgL_i1161z00_5501 = 0L;
																					BgL_zc3z04anonymousza31985ze3z87_5500:
																						if (
																							(BgL_i1161z00_5501 ==
																								VECTOR_LENGTH
																								(BgL_v1162z00_2770)))
																							{	/* Unsafe/intext.scm 1236 */
																								BgL_fz00_2769 =
																									BgL_nv1163z00_2772;
																							}
																						else
																							{	/* Unsafe/intext.scm 1236 */
																								VECTOR_SET(BgL_nv1163z00_2772,
																									BgL_i1161z00_5501,
																									BGl_classzd2fieldzd2namez00zz__objectz00
																									(VECTOR_REF(BgL_v1162z00_2770,
																											BgL_i1161z00_5501)));
																								{
																									long BgL_i1161z00_10477;

																									BgL_i1161z00_10477 =
																										(BgL_i1161z00_5501 + 1L);
																									BgL_i1161z00_5501 =
																										BgL_i1161z00_10477;
																									goto
																										BgL_zc3z04anonymousza31985ze3z87_5500;
																								}
																							}
																					}
																				}
																			}
																		}
																		{	/* Unsafe/intext.scm 675 */
																			obj_t BgL_arg1640z00_5513;

																			{	/* Unsafe/intext.scm 663 */
																				obj_t BgL_newz00_5514;

																				BgL_newz00_5514 =
																					create_struct
																					(BGl_symbol2667z00zz__intextz00,
																					(int) (4L));
																				{	/* Unsafe/intext.scm 663 */
																					obj_t BgL_auxz00_10483;
																					int BgL_tmpz00_10481;

																					BgL_auxz00_10483 = BINT(-1L);
																					BgL_tmpz00_10481 = (int) (3L);
																					STRUCT_SET(BgL_newz00_5514,
																						BgL_tmpz00_10481, BgL_auxz00_10483);
																				}
																				{	/* Unsafe/intext.scm 663 */
																					obj_t BgL_auxz00_10488;
																					int BgL_tmpz00_10486;

																					BgL_auxz00_10488 = BINT(0L);
																					BgL_tmpz00_10486 = (int) (2L);
																					STRUCT_SET(BgL_newz00_5514,
																						BgL_tmpz00_10486, BgL_auxz00_10488);
																				}
																				{	/* Unsafe/intext.scm 663 */
																					int BgL_tmpz00_10491;

																					BgL_tmpz00_10491 = (int) (1L);
																					STRUCT_SET(BgL_newz00_5514,
																						BgL_tmpz00_10491, BgL_fz00_2769);
																				}
																				{	/* Unsafe/intext.scm 663 */
																					int BgL_tmpz00_10494;

																					BgL_tmpz00_10494 = (int) (0L);
																					STRUCT_SET(BgL_newz00_5514,
																						BgL_tmpz00_10494, BgL_objz00_2767);
																				}
																				BgL_arg1640z00_5513 = BgL_newz00_5514;
																			}
																			BGl_hashtablezd2putz12zc0zz__hashz00
																				(BgL_tablez00_6668, BgL_objz00_2767,
																				BgL_arg1640z00_5513);
																		}
																		{
																			obj_t BgL_objz00_10498;

																			BgL_objz00_10498 = BgL_fz00_2769;
																			BgL_objz00_2833 = BgL_objz00_10498;
																			goto BGl_markze70ze7zz__intextz00;
																		}
																	}
																}
														}
													}
												}
											else
												{	/* Unsafe/intext.scm 1314 */
													if (STRUCTP(BgL_objz00_2833))
														{	/* Unsafe/intext.scm 1184 */
															obj_t BgL_mz00_5658;

															BgL_mz00_5658 =
																BGl_hashtablezd2getzd2zz__hashz00
																(BgL_tablez00_6668, BgL_objz00_2833);
															{	/* Unsafe/intext.scm 1185 */
																bool_t BgL_test3015z00_10502;

																if (STRUCTP(BgL_mz00_5658))
																	{	/* Unsafe/intext.scm 663 */
																		BgL_test3015z00_10502 =
																			(STRUCT_KEY(BgL_mz00_5658) ==
																			BGl_symbol2667z00zz__intextz00);
																	}
																else
																	{	/* Unsafe/intext.scm 663 */
																		BgL_test3015z00_10502 = ((bool_t) 0);
																	}
																if (BgL_test3015z00_10502)
																	{
																		obj_t BgL_mz00_10507;

																		BgL_mz00_10507 = BgL_mz00_5658;
																		BgL_mz00_2718 = BgL_mz00_10507;
																		goto BgL_zc3z04anonymousza31958ze3z87_2719;
																	}
																else
																	{	/* Unsafe/intext.scm 1187 */
																		bool_t BgL_tmpz00_10508;

																		BgL_objz00_2783 = BgL_objz00_2833;
																		{	/* Unsafe/intext.scm 675 */
																			obj_t BgL_arg1640z00_5519;

																			{	/* Unsafe/intext.scm 663 */
																				obj_t BgL_newz00_5520;

																				BgL_newz00_5520 =
																					create_struct
																					(BGl_symbol2667z00zz__intextz00,
																					(int) (4L));
																				{	/* Unsafe/intext.scm 663 */
																					obj_t BgL_auxz00_10513;
																					int BgL_tmpz00_10511;

																					BgL_auxz00_10513 = BINT(-1L);
																					BgL_tmpz00_10511 = (int) (3L);
																					STRUCT_SET(BgL_newz00_5520,
																						BgL_tmpz00_10511, BgL_auxz00_10513);
																				}
																				{	/* Unsafe/intext.scm 663 */
																					obj_t BgL_auxz00_10518;
																					int BgL_tmpz00_10516;

																					BgL_auxz00_10518 = BINT(0L);
																					BgL_tmpz00_10516 = (int) (2L);
																					STRUCT_SET(BgL_newz00_5520,
																						BgL_tmpz00_10516, BgL_auxz00_10518);
																				}
																				{	/* Unsafe/intext.scm 663 */
																					int BgL_tmpz00_10521;

																					BgL_tmpz00_10521 = (int) (1L);
																					STRUCT_SET(BgL_newz00_5520,
																						BgL_tmpz00_10521, BFALSE);
																				}
																				{	/* Unsafe/intext.scm 663 */
																					int BgL_tmpz00_10524;

																					BgL_tmpz00_10524 = (int) (0L);
																					STRUCT_SET(BgL_newz00_5520,
																						BgL_tmpz00_10524, BgL_objz00_2783);
																				}
																				BgL_arg1640z00_5519 = BgL_newz00_5520;
																			}
																			BGl_hashtablezd2putz12zc0zz__hashz00
																				(BgL_tablez00_6668, BgL_objz00_2783,
																				BgL_arg1640z00_5519);
																		}
																		{	/* Unsafe/intext.scm 1243 */
																			obj_t BgL_keyz00_2785;
																			int BgL_lenz00_2786;

																			{	/* Unsafe/intext.scm 1243 */
																				obj_t BgL_res2518z00_5526;

																				BgL_res2518z00_5526 =
																					STRUCT_KEY(((obj_t) BgL_objz00_2783));
																				BgL_keyz00_2785 = BgL_res2518z00_5526;
																			}
																			BgL_lenz00_2786 =
																				STRUCT_LENGTH(
																				((obj_t) BgL_objz00_2783));
																			BGl_markze70ze7zz__intextz00
																				(BgL_nbrefz00_6670,
																				BgL_markzd2argzd2_6669,
																				BgL_tablez00_6668, BgL_keyz00_2785);
																			{
																				long BgL_iz00_5534;

																				BgL_iz00_5534 = 0L;
																			BgL_for1092z00_5533:
																				if (
																					(BgL_iz00_5534 <
																						(long) (BgL_lenz00_2786)))
																					{	/* Unsafe/intext.scm 1246 */
																						{	/* Unsafe/intext.scm 1246 */
																							obj_t BgL_arg1993z00_5538;

																							BgL_arg1993z00_5538 =
																								STRUCT_REF(
																								((obj_t) BgL_objz00_2783),
																								(int) (BgL_iz00_5534));
																							BGl_markze70ze7zz__intextz00
																								(BgL_nbrefz00_6670,
																								BgL_markzd2argzd2_6669,
																								BgL_tablez00_6668,
																								BgL_arg1993z00_5538);
																						}
																						{
																							long BgL_iz00_10540;

																							BgL_iz00_10540 =
																								(BgL_iz00_5534 + 1L);
																							BgL_iz00_5534 = BgL_iz00_10540;
																							goto BgL_for1092z00_5533;
																						}
																					}
																				else
																					{	/* Unsafe/intext.scm 1246 */
																						BgL_tmpz00_10508 = ((bool_t) 0);
																					}
																			}
																		}
																		return BBOOL(BgL_tmpz00_10508);
																	}
															}
														}
													else
														{	/* Unsafe/intext.scm 1317 */
															if (CELLP(BgL_objz00_2833))
																{	/* Unsafe/intext.scm 1184 */
																	obj_t BgL_mz00_5664;

																	BgL_mz00_5664 =
																		BGl_hashtablezd2getzd2zz__hashz00
																		(BgL_tablez00_6668, BgL_objz00_2833);
																	{	/* Unsafe/intext.scm 1185 */
																		bool_t BgL_test3019z00_10546;

																		if (STRUCTP(BgL_mz00_5664))
																			{	/* Unsafe/intext.scm 663 */
																				BgL_test3019z00_10546 =
																					(STRUCT_KEY(BgL_mz00_5664) ==
																					BGl_symbol2667z00zz__intextz00);
																			}
																		else
																			{	/* Unsafe/intext.scm 663 */
																				BgL_test3019z00_10546 = ((bool_t) 0);
																			}
																		if (BgL_test3019z00_10546)
																			{
																				obj_t BgL_mz00_10551;

																				BgL_mz00_10551 = BgL_mz00_5664;
																				BgL_mz00_2718 = BgL_mz00_10551;
																				goto
																					BgL_zc3z04anonymousza31958ze3z87_2719;
																			}
																		else
																			{	/* Unsafe/intext.scm 1185 */
																				BgL_objz00_2794 = BgL_objz00_2833;
																				{	/* Unsafe/intext.scm 675 */
																					obj_t BgL_arg1640z00_5544;

																					{	/* Unsafe/intext.scm 663 */
																						obj_t BgL_newz00_5545;

																						BgL_newz00_5545 =
																							create_struct
																							(BGl_symbol2667z00zz__intextz00,
																							(int) (4L));
																						{	/* Unsafe/intext.scm 663 */
																							obj_t BgL_auxz00_10556;
																							int BgL_tmpz00_10554;

																							BgL_auxz00_10556 = BINT(-1L);
																							BgL_tmpz00_10554 = (int) (3L);
																							STRUCT_SET(BgL_newz00_5545,
																								BgL_tmpz00_10554,
																								BgL_auxz00_10556);
																						}
																						{	/* Unsafe/intext.scm 663 */
																							obj_t BgL_auxz00_10561;
																							int BgL_tmpz00_10559;

																							BgL_auxz00_10561 = BINT(0L);
																							BgL_tmpz00_10559 = (int) (2L);
																							STRUCT_SET(BgL_newz00_5545,
																								BgL_tmpz00_10559,
																								BgL_auxz00_10561);
																						}
																						{	/* Unsafe/intext.scm 663 */
																							int BgL_tmpz00_10564;

																							BgL_tmpz00_10564 = (int) (1L);
																							STRUCT_SET(BgL_newz00_5545,
																								BgL_tmpz00_10564, BFALSE);
																						}
																						{	/* Unsafe/intext.scm 663 */
																							int BgL_tmpz00_10567;

																							BgL_tmpz00_10567 = (int) (0L);
																							STRUCT_SET(BgL_newz00_5545,
																								BgL_tmpz00_10567,
																								BgL_objz00_2794);
																						}
																						BgL_arg1640z00_5544 =
																							BgL_newz00_5545;
																					}
																					BGl_hashtablezd2putz12zc0zz__hashz00
																						(BgL_tablez00_6668, BgL_objz00_2794,
																						BgL_arg1640z00_5544);
																				}
																				{	/* Unsafe/intext.scm 1251 */
																					obj_t BgL_arg1996z00_5543;

																					BgL_arg1996z00_5543 =
																						CELL_REF(((obj_t) BgL_objz00_2794));
																					{
																						obj_t BgL_objz00_10573;

																						BgL_objz00_10573 =
																							BgL_arg1996z00_5543;
																						BgL_objz00_2833 = BgL_objz00_10573;
																						goto BGl_markze70ze7zz__intextz00;
																					}
																				}
																			}
																	}
																}
															else
																{	/* Unsafe/intext.scm 1319 */
																	if (BGL_WEAKPTRP(BgL_objz00_2833))
																		{	/* Unsafe/intext.scm 1184 */
																			obj_t BgL_mz00_5670;

																			BgL_mz00_5670 =
																				BGl_hashtablezd2getzd2zz__hashz00
																				(BgL_tablez00_6668, BgL_objz00_2833);
																			{	/* Unsafe/intext.scm 1185 */
																				bool_t BgL_test3022z00_10577;

																				if (STRUCTP(BgL_mz00_5670))
																					{	/* Unsafe/intext.scm 663 */
																						BgL_test3022z00_10577 =
																							(STRUCT_KEY(BgL_mz00_5670) ==
																							BGl_symbol2667z00zz__intextz00);
																					}
																				else
																					{	/* Unsafe/intext.scm 663 */
																						BgL_test3022z00_10577 =
																							((bool_t) 0);
																					}
																				if (BgL_test3022z00_10577)
																					{
																						obj_t BgL_mz00_10582;

																						BgL_mz00_10582 = BgL_mz00_5670;
																						BgL_mz00_2718 = BgL_mz00_10582;
																						goto
																							BgL_zc3z04anonymousza31958ze3z87_2719;
																					}
																				else
																					{	/* Unsafe/intext.scm 1185 */
																						BgL_objz00_2797 = BgL_objz00_2833;
																						{	/* Unsafe/intext.scm 675 */
																							obj_t BgL_arg1640z00_5552;

																							{	/* Unsafe/intext.scm 663 */
																								obj_t BgL_newz00_5553;

																								BgL_newz00_5553 =
																									create_struct
																									(BGl_symbol2667z00zz__intextz00,
																									(int) (4L));
																								{	/* Unsafe/intext.scm 663 */
																									obj_t BgL_auxz00_10587;
																									int BgL_tmpz00_10585;

																									BgL_auxz00_10587 = BINT(-1L);
																									BgL_tmpz00_10585 = (int) (3L);
																									STRUCT_SET(BgL_newz00_5553,
																										BgL_tmpz00_10585,
																										BgL_auxz00_10587);
																								}
																								{	/* Unsafe/intext.scm 663 */
																									obj_t BgL_auxz00_10592;
																									int BgL_tmpz00_10590;

																									BgL_auxz00_10592 = BINT(0L);
																									BgL_tmpz00_10590 = (int) (2L);
																									STRUCT_SET(BgL_newz00_5553,
																										BgL_tmpz00_10590,
																										BgL_auxz00_10592);
																								}
																								{	/* Unsafe/intext.scm 663 */
																									int BgL_tmpz00_10595;

																									BgL_tmpz00_10595 = (int) (1L);
																									STRUCT_SET(BgL_newz00_5553,
																										BgL_tmpz00_10595, BFALSE);
																								}
																								{	/* Unsafe/intext.scm 663 */
																									int BgL_tmpz00_10598;

																									BgL_tmpz00_10598 = (int) (0L);
																									STRUCT_SET(BgL_newz00_5553,
																										BgL_tmpz00_10598,
																										BgL_objz00_2797);
																								}
																								BgL_arg1640z00_5552 =
																									BgL_newz00_5553;
																							}
																							BGl_hashtablezd2putz12zc0zz__hashz00
																								(BgL_tablez00_6668,
																								BgL_objz00_2797,
																								BgL_arg1640z00_5552);
																						}
																						{	/* Unsafe/intext.scm 1256 */
																							obj_t BgL_arg1998z00_5551;

																							BgL_arg1998z00_5551 =
																								bgl_weakptr_data(
																								((obj_t) BgL_objz00_2797));
																							{
																								obj_t BgL_objz00_10604;

																								BgL_objz00_10604 =
																									BgL_arg1998z00_5551;
																								BgL_objz00_2833 =
																									BgL_objz00_10604;
																								goto
																									BGl_markze70ze7zz__intextz00;
																							}
																						}
																					}
																			}
																		}
																	else
																		{	/* Unsafe/intext.scm 1321 */
																			if (STRINGP(BgL_objz00_2833))
																				{	/* Unsafe/intext.scm 1184 */
																					obj_t BgL_mz00_5676;

																					BgL_mz00_5676 =
																						BGl_hashtablezd2getzd2zz__hashz00
																						(BgL_tablez00_6668,
																						BgL_objz00_2833);
																					{	/* Unsafe/intext.scm 1185 */
																						bool_t BgL_test3025z00_10608;

																						if (STRUCTP(BgL_mz00_5676))
																							{	/* Unsafe/intext.scm 663 */
																								BgL_test3025z00_10608 =
																									(STRUCT_KEY(BgL_mz00_5676) ==
																									BGl_symbol2667z00zz__intextz00);
																							}
																						else
																							{	/* Unsafe/intext.scm 663 */
																								BgL_test3025z00_10608 =
																									((bool_t) 0);
																							}
																						if (BgL_test3025z00_10608)
																							{
																								obj_t BgL_mz00_10613;

																								BgL_mz00_10613 = BgL_mz00_5676;
																								BgL_mz00_2718 = BgL_mz00_10613;
																								goto
																									BgL_zc3z04anonymousza31958ze3z87_2719;
																							}
																						else
																							{	/* Unsafe/intext.scm 675 */
																								obj_t BgL_arg1640z00_5682;

																								{	/* Unsafe/intext.scm 663 */
																									obj_t BgL_newz00_5683;

																									BgL_newz00_5683 =
																										create_struct
																										(BGl_symbol2667z00zz__intextz00,
																										(int) (4L));
																									{	/* Unsafe/intext.scm 663 */
																										obj_t BgL_auxz00_10618;
																										int BgL_tmpz00_10616;

																										BgL_auxz00_10618 =
																											BINT(-1L);
																										BgL_tmpz00_10616 =
																											(int) (3L);
																										STRUCT_SET(BgL_newz00_5683,
																											BgL_tmpz00_10616,
																											BgL_auxz00_10618);
																									}
																									{	/* Unsafe/intext.scm 663 */
																										obj_t BgL_auxz00_10623;
																										int BgL_tmpz00_10621;

																										BgL_auxz00_10623 = BINT(0L);
																										BgL_tmpz00_10621 =
																											(int) (2L);
																										STRUCT_SET(BgL_newz00_5683,
																											BgL_tmpz00_10621,
																											BgL_auxz00_10623);
																									}
																									{	/* Unsafe/intext.scm 663 */
																										int BgL_tmpz00_10626;

																										BgL_tmpz00_10626 =
																											(int) (1L);
																										STRUCT_SET(BgL_newz00_5683,
																											BgL_tmpz00_10626, BFALSE);
																									}
																									{	/* Unsafe/intext.scm 663 */
																										int BgL_tmpz00_10629;

																										BgL_tmpz00_10629 =
																											(int) (0L);
																										STRUCT_SET(BgL_newz00_5683,
																											BgL_tmpz00_10629,
																											BgL_objz00_2833);
																									}
																									BgL_arg1640z00_5682 =
																										BgL_newz00_5683;
																								}
																								return
																									BGl_hashtablezd2putz12zc0zz__hashz00
																									(BgL_tablez00_6668,
																									BgL_objz00_2833,
																									BgL_arg1640z00_5682);
																							}
																					}
																				}
																			else
																				{	/* Unsafe/intext.scm 1323 */
																					if (SYMBOLP(BgL_objz00_2833))
																						{	/* Unsafe/intext.scm 1326 */
																							obj_t BgL_arg2030z00_2847;

																							BgL_arg2030z00_2847 =
																								SYMBOL_TO_STRING
																								(BgL_objz00_2833);
																							{
																								obj_t BgL_objz00_10636;

																								BgL_objz00_10636 =
																									BgL_arg2030z00_2847;
																								BgL_objz00_2833 =
																									BgL_objz00_10636;
																								goto
																									BGl_markze70ze7zz__intextz00;
																							}
																						}
																					else
																						{	/* Unsafe/intext.scm 1325 */
																							if (KEYWORDP(BgL_objz00_2833))
																								{	/* Unsafe/intext.scm 1328 */
																									obj_t BgL_arg2033z00_2849;

																									BgL_arg2033z00_2849 =
																										KEYWORD_TO_STRING
																										(BgL_objz00_2833);
																									{
																										obj_t BgL_objz00_10640;

																										BgL_objz00_10640 =
																											BgL_arg2033z00_2849;
																										BgL_objz00_2833 =
																											BgL_objz00_10640;
																										goto
																											BGl_markze70ze7zz__intextz00;
																									}
																								}
																							else
																								{	/* Unsafe/intext.scm 1327 */
																									if (UCS2_STRINGP
																										(BgL_objz00_2833))
																										{	/* Unsafe/intext.scm 1184 */
																											obj_t BgL_mz00_5690;

																											BgL_mz00_5690 =
																												BGl_hashtablezd2getzd2zz__hashz00
																												(BgL_tablez00_6668,
																												BgL_objz00_2833);
																											{	/* Unsafe/intext.scm 1185 */
																												bool_t
																													BgL_test3030z00_10644;
																												if (STRUCTP
																													(BgL_mz00_5690))
																													{	/* Unsafe/intext.scm 663 */
																														BgL_test3030z00_10644
																															=
																															(STRUCT_KEY
																															(BgL_mz00_5690) ==
																															BGl_symbol2667z00zz__intextz00);
																													}
																												else
																													{	/* Unsafe/intext.scm 663 */
																														BgL_test3030z00_10644
																															= ((bool_t) 0);
																													}
																												if (BgL_test3030z00_10644)
																													{
																														obj_t
																															BgL_mz00_10649;
																														BgL_mz00_10649 =
																															BgL_mz00_5690;
																														BgL_mz00_2718 =
																															BgL_mz00_10649;
																														goto
																															BgL_zc3z04anonymousza31958ze3z87_2719;
																													}
																												else
																													{	/* Unsafe/intext.scm 1185 */
																														BgL_objz00_2818 =
																															BgL_objz00_2833;
																														{	/* Unsafe/intext.scm 1281 */
																															obj_t
																																BgL_strz00_5601;
																															BgL_strz00_5601 =
																																ucs2_string_to_utf8_string
																																(((obj_t)
																																	BgL_objz00_2818));
																															{	/* Unsafe/intext.scm 675 */
																																obj_t
																																	BgL_arg1640z00_5603;
																																{	/* Unsafe/intext.scm 663 */
																																	obj_t
																																		BgL_newz00_5604;
																																	BgL_newz00_5604
																																		=
																																		create_struct
																																		(BGl_symbol2667z00zz__intextz00,
																																		(int) (4L));
																																	{	/* Unsafe/intext.scm 663 */
																																		obj_t
																																			BgL_auxz00_10656;
																																		int
																																			BgL_tmpz00_10654;
																																		BgL_auxz00_10656
																																			=
																																			BINT(-1L);
																																		BgL_tmpz00_10654
																																			=
																																			(int)
																																			(3L);
																																		STRUCT_SET
																																			(BgL_newz00_5604,
																																			BgL_tmpz00_10654,
																																			BgL_auxz00_10656);
																																	}
																																	{	/* Unsafe/intext.scm 663 */
																																		obj_t
																																			BgL_auxz00_10661;
																																		int
																																			BgL_tmpz00_10659;
																																		BgL_auxz00_10661
																																			=
																																			BINT(0L);
																																		BgL_tmpz00_10659
																																			=
																																			(int)
																																			(2L);
																																		STRUCT_SET
																																			(BgL_newz00_5604,
																																			BgL_tmpz00_10659,
																																			BgL_auxz00_10661);
																																	}
																																	{	/* Unsafe/intext.scm 663 */
																																		int
																																			BgL_tmpz00_10664;
																																		BgL_tmpz00_10664
																																			=
																																			(int)
																																			(1L);
																																		STRUCT_SET
																																			(BgL_newz00_5604,
																																			BgL_tmpz00_10664,
																																			BgL_strz00_5601);
																																	}
																																	{	/* Unsafe/intext.scm 663 */
																																		int
																																			BgL_tmpz00_10667;
																																		BgL_tmpz00_10667
																																			=
																																			(int)
																																			(0L);
																																		STRUCT_SET
																																			(BgL_newz00_5604,
																																			BgL_tmpz00_10667,
																																			BgL_objz00_2818);
																																	}
																																	BgL_arg1640z00_5603
																																		=
																																		BgL_newz00_5604;
																																}
																																BGl_hashtablezd2putz12zc0zz__hashz00
																																	(BgL_tablez00_6668,
																																	BgL_objz00_2818,
																																	BgL_arg1640z00_5603);
																															}
																															{
																																obj_t
																																	BgL_objz00_10671;
																																BgL_objz00_10671
																																	=
																																	BgL_strz00_5601;
																																BgL_objz00_2833
																																	=
																																	BgL_objz00_10671;
																																goto
																																	BGl_markze70ze7zz__intextz00;
																															}
																														}
																													}
																											}
																										}
																									else
																										{	/* Unsafe/intext.scm 1329 */
																											if (VECTORP
																												(BgL_objz00_2833))
																												{	/* Unsafe/intext.scm 1184 */
																													obj_t BgL_mz00_5696;

																													BgL_mz00_5696 =
																														BGl_hashtablezd2getzd2zz__hashz00
																														(BgL_tablez00_6668,
																														BgL_objz00_2833);
																													{	/* Unsafe/intext.scm 1185 */
																														bool_t
																															BgL_test3033z00_10675;
																														if (STRUCTP
																															(BgL_mz00_5696))
																															{	/* Unsafe/intext.scm 663 */
																																BgL_test3033z00_10675
																																	=
																																	(STRUCT_KEY
																																	(BgL_mz00_5696)
																																	==
																																	BGl_symbol2667z00zz__intextz00);
																															}
																														else
																															{	/* Unsafe/intext.scm 663 */
																																BgL_test3033z00_10675
																																	=
																																	((bool_t) 0);
																															}
																														if (BgL_test3033z00_10675)
																															{
																																obj_t
																																	BgL_mz00_10680;
																																BgL_mz00_10680 =
																																	BgL_mz00_5696;
																																BgL_mz00_2718 =
																																	BgL_mz00_10680;
																																goto
																																	BgL_zc3z04anonymousza31958ze3z87_2719;
																															}
																														else
																															{	/* Unsafe/intext.scm 1187 */
																																bool_t
																																	BgL_tmpz00_10681;
																																BgL_objz00_2800
																																	=
																																	BgL_objz00_2833;
																																{	/* Unsafe/intext.scm 675 */
																																	obj_t
																																		BgL_arg1640z00_5559;
																																	{	/* Unsafe/intext.scm 663 */
																																		obj_t
																																			BgL_newz00_5560;
																																		BgL_newz00_5560
																																			=
																																			create_struct
																																			(BGl_symbol2667z00zz__intextz00,
																																			(int)
																																			(4L));
																																		{	/* Unsafe/intext.scm 663 */
																																			obj_t
																																				BgL_auxz00_10686;
																																			int
																																				BgL_tmpz00_10684;
																																			BgL_auxz00_10686
																																				=
																																				BINT
																																				(-1L);
																																			BgL_tmpz00_10684
																																				=
																																				(int)
																																				(3L);
																																			STRUCT_SET
																																				(BgL_newz00_5560,
																																				BgL_tmpz00_10684,
																																				BgL_auxz00_10686);
																																		}
																																		{	/* Unsafe/intext.scm 663 */
																																			obj_t
																																				BgL_auxz00_10691;
																																			int
																																				BgL_tmpz00_10689;
																																			BgL_auxz00_10691
																																				=
																																				BINT
																																				(0L);
																																			BgL_tmpz00_10689
																																				=
																																				(int)
																																				(2L);
																																			STRUCT_SET
																																				(BgL_newz00_5560,
																																				BgL_tmpz00_10689,
																																				BgL_auxz00_10691);
																																		}
																																		{	/* Unsafe/intext.scm 663 */
																																			int
																																				BgL_tmpz00_10694;
																																			BgL_tmpz00_10694
																																				=
																																				(int)
																																				(1L);
																																			STRUCT_SET
																																				(BgL_newz00_5560,
																																				BgL_tmpz00_10694,
																																				BFALSE);
																																		}
																																		{	/* Unsafe/intext.scm 663 */
																																			int
																																				BgL_tmpz00_10697;
																																			BgL_tmpz00_10697
																																				=
																																				(int)
																																				(0L);
																																			STRUCT_SET
																																				(BgL_newz00_5560,
																																				BgL_tmpz00_10697,
																																				BgL_objz00_2800);
																																		}
																																		BgL_arg1640z00_5559
																																			=
																																			BgL_newz00_5560;
																																	}
																																	BGl_hashtablezd2putz12zc0zz__hashz00
																																		(BgL_tablez00_6668,
																																		BgL_objz00_2800,
																																		BgL_arg1640z00_5559);
																																}
																																{
																																	long
																																		BgL_iz00_5572;
																																	BgL_iz00_5572
																																		= 0L;
																																BgL_for1093z00_5571:
																																	if (
																																		(BgL_iz00_5572
																																			<
																																			VECTOR_LENGTH
																																			(((obj_t)
																																					BgL_objz00_2800))))
																																		{	/* Unsafe/intext.scm 1262 */
																																			{	/* Unsafe/intext.scm 1262 */
																																				obj_t
																																					BgL_arg2002z00_5576;
																																				BgL_arg2002z00_5576
																																					=
																																					VECTOR_REF
																																					(((obj_t) BgL_objz00_2800), BgL_iz00_5572);
																																				BGl_markze70ze7zz__intextz00
																																					(BgL_nbrefz00_6670,
																																					BgL_markzd2argzd2_6669,
																																					BgL_tablez00_6668,
																																					BgL_arg2002z00_5576);
																																			}
																																			{
																																				long
																																					BgL_iz00_10708;
																																				BgL_iz00_10708
																																					=
																																					(BgL_iz00_5572
																																					+ 1L);
																																				BgL_iz00_5572
																																					=
																																					BgL_iz00_10708;
																																				goto
																																					BgL_for1093z00_5571;
																																			}
																																		}
																																	else
																																		{	/* Unsafe/intext.scm 1262 */
																																			BgL_tmpz00_10681
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																return
																																	BBOOL
																																	(BgL_tmpz00_10681);
																															}
																													}
																												}
																											else
																												{	/* Unsafe/intext.scm 1331 */
																													if (BGL_HVECTORP
																														(BgL_objz00_2833))
																														{	/* Unsafe/intext.scm 1184 */
																															obj_t
																																BgL_mz00_5702;
																															BgL_mz00_5702 =
																																BGl_hashtablezd2getzd2zz__hashz00
																																(BgL_tablez00_6668,
																																BgL_objz00_2833);
																															{	/* Unsafe/intext.scm 1185 */
																																bool_t
																																	BgL_test3037z00_10714;
																																if (STRUCTP
																																	(BgL_mz00_5702))
																																	{	/* Unsafe/intext.scm 663 */
																																		BgL_test3037z00_10714
																																			=
																																			(STRUCT_KEY
																																			(BgL_mz00_5702)
																																			==
																																			BGl_symbol2667z00zz__intextz00);
																																	}
																																else
																																	{	/* Unsafe/intext.scm 663 */
																																		BgL_test3037z00_10714
																																			=
																																			((bool_t)
																																			0);
																																	}
																																if (BgL_test3037z00_10714)
																																	{
																																		obj_t
																																			BgL_mz00_10719;
																																		BgL_mz00_10719
																																			=
																																			BgL_mz00_5702;
																																		BgL_mz00_2718
																																			=
																																			BgL_mz00_10719;
																																		goto
																																			BgL_zc3z04anonymousza31958ze3z87_2719;
																																	}
																																else
																																	{	/* Unsafe/intext.scm 675 */
																																		obj_t
																																			BgL_arg1640z00_5708;
																																		{	/* Unsafe/intext.scm 663 */
																																			obj_t
																																				BgL_newz00_5709;
																																			BgL_newz00_5709
																																				=
																																				create_struct
																																				(BGl_symbol2667z00zz__intextz00,
																																				(int)
																																				(4L));
																																			{	/* Unsafe/intext.scm 663 */
																																				obj_t
																																					BgL_auxz00_10724;
																																				int
																																					BgL_tmpz00_10722;
																																				BgL_auxz00_10724
																																					=
																																					BINT
																																					(-1L);
																																				BgL_tmpz00_10722
																																					=
																																					(int)
																																					(3L);
																																				STRUCT_SET
																																					(BgL_newz00_5709,
																																					BgL_tmpz00_10722,
																																					BgL_auxz00_10724);
																																			}
																																			{	/* Unsafe/intext.scm 663 */
																																				obj_t
																																					BgL_auxz00_10729;
																																				int
																																					BgL_tmpz00_10727;
																																				BgL_auxz00_10729
																																					=
																																					BINT
																																					(0L);
																																				BgL_tmpz00_10727
																																					=
																																					(int)
																																					(2L);
																																				STRUCT_SET
																																					(BgL_newz00_5709,
																																					BgL_tmpz00_10727,
																																					BgL_auxz00_10729);
																																			}
																																			{	/* Unsafe/intext.scm 663 */
																																				int
																																					BgL_tmpz00_10732;
																																				BgL_tmpz00_10732
																																					=
																																					(int)
																																					(1L);
																																				STRUCT_SET
																																					(BgL_newz00_5709,
																																					BgL_tmpz00_10732,
																																					BFALSE);
																																			}
																																			{	/* Unsafe/intext.scm 663 */
																																				int
																																					BgL_tmpz00_10735;
																																				BgL_tmpz00_10735
																																					=
																																					(int)
																																					(0L);
																																				STRUCT_SET
																																					(BgL_newz00_5709,
																																					BgL_tmpz00_10735,
																																					BgL_objz00_2833);
																																			}
																																			BgL_arg1640z00_5708
																																				=
																																				BgL_newz00_5709;
																																		}
																																		return
																																			BGl_hashtablezd2putz12zc0zz__hashz00
																																			(BgL_tablez00_6668,
																																			BgL_objz00_2833,
																																			BgL_arg1640z00_5708);
																																	}
																															}
																														}
																													else
																														{	/* Unsafe/intext.scm 1333 */
																															if (TVECTORP
																																(BgL_objz00_2833))
																																{	/* Unsafe/intext.scm 1184 */
																																	obj_t
																																		BgL_mz00_5714;
																																	BgL_mz00_5714
																																		=
																																		BGl_hashtablezd2getzd2zz__hashz00
																																		(BgL_tablez00_6668,
																																		BgL_objz00_2833);
																																	{	/* Unsafe/intext.scm 1185 */
																																		bool_t
																																			BgL_test3040z00_10742;
																																		if (STRUCTP
																																			(BgL_mz00_5714))
																																			{	/* Unsafe/intext.scm 663 */
																																				BgL_test3040z00_10742
																																					=
																																					(STRUCT_KEY
																																					(BgL_mz00_5714)
																																					==
																																					BGl_symbol2667z00zz__intextz00);
																																			}
																																		else
																																			{	/* Unsafe/intext.scm 663 */
																																				BgL_test3040z00_10742
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																		if (BgL_test3040z00_10742)
																																			{
																																				obj_t
																																					BgL_mz00_10747;
																																				BgL_mz00_10747
																																					=
																																					BgL_mz00_5714;
																																				BgL_mz00_2718
																																					=
																																					BgL_mz00_10747;
																																				goto
																																					BgL_zc3z04anonymousza31958ze3z87_2719;
																																			}
																																		else
																																			{	/* Unsafe/intext.scm 1185 */
																																				BgL_objz00_2810
																																					=
																																					BgL_objz00_2833;
																																				{	/* Unsafe/intext.scm 1266 */
																																					obj_t
																																						BgL_vz00_5581;
																																					BgL_vz00_5581
																																						=
																																						BGl_tvectorzd2ze3vectorz31zz__tvectorz00
																																						(BgL_objz00_2810);
																																					{	/* Unsafe/intext.scm 675 */
																																						obj_t
																																							BgL_arg1640z00_5583;
																																						{	/* Unsafe/intext.scm 663 */
																																							obj_t
																																								BgL_newz00_5584;
																																							BgL_newz00_5584
																																								=
																																								create_struct
																																								(BGl_symbol2667z00zz__intextz00,
																																								(int)
																																								(4L));
																																							{	/* Unsafe/intext.scm 663 */
																																								obj_t
																																									BgL_auxz00_10753;
																																								int
																																									BgL_tmpz00_10751;
																																								BgL_auxz00_10753
																																									=
																																									BINT
																																									(-1L);
																																								BgL_tmpz00_10751
																																									=
																																									(int)
																																									(3L);
																																								STRUCT_SET
																																									(BgL_newz00_5584,
																																									BgL_tmpz00_10751,
																																									BgL_auxz00_10753);
																																							}
																																							{	/* Unsafe/intext.scm 663 */
																																								obj_t
																																									BgL_auxz00_10758;
																																								int
																																									BgL_tmpz00_10756;
																																								BgL_auxz00_10758
																																									=
																																									BINT
																																									(0L);
																																								BgL_tmpz00_10756
																																									=
																																									(int)
																																									(2L);
																																								STRUCT_SET
																																									(BgL_newz00_5584,
																																									BgL_tmpz00_10756,
																																									BgL_auxz00_10758);
																																							}
																																							{	/* Unsafe/intext.scm 663 */
																																								int
																																									BgL_tmpz00_10761;
																																								BgL_tmpz00_10761
																																									=
																																									(int)
																																									(1L);
																																								STRUCT_SET
																																									(BgL_newz00_5584,
																																									BgL_tmpz00_10761,
																																									BgL_vz00_5581);
																																							}
																																							{	/* Unsafe/intext.scm 663 */
																																								int
																																									BgL_tmpz00_10764;
																																								BgL_tmpz00_10764
																																									=
																																									(int)
																																									(0L);
																																								STRUCT_SET
																																									(BgL_newz00_5584,
																																									BgL_tmpz00_10764,
																																									BgL_objz00_2810);
																																							}
																																							BgL_arg1640z00_5583
																																								=
																																								BgL_newz00_5584;
																																						}
																																						BGl_hashtablezd2putz12zc0zz__hashz00
																																							(BgL_tablez00_6668,
																																							BgL_objz00_2810,
																																							BgL_arg1640z00_5583);
																																					}
																																					{	/* Unsafe/intext.scm 1268 */
																																						obj_t
																																							BgL_arg2006z00_5582;
																																						BgL_arg2006z00_5582
																																							=
																																							BGl_tvectorzd2idzd2zz__tvectorz00
																																							(BgL_objz00_2810);
																																						BGl_markze70ze7zz__intextz00
																																							(BgL_nbrefz00_6670,
																																							BgL_markzd2argzd2_6669,
																																							BgL_tablez00_6668,
																																							BgL_arg2006z00_5582);
																																					}
																																					{
																																						obj_t
																																							BgL_objz00_10770;
																																						BgL_objz00_10770
																																							=
																																							BgL_vz00_5581;
																																						BgL_objz00_2833
																																							=
																																							BgL_objz00_10770;
																																						goto
																																							BGl_markze70ze7zz__intextz00;
																																					}
																																				}
																																			}
																																	}
																																}
																															else
																																{	/* Unsafe/intext.scm 1335 */
																																	if (PROCEDUREP
																																		(BgL_objz00_2833))
																																		{	/* Unsafe/intext.scm 1184 */
																																			obj_t
																																				BgL_mz00_5720;
																																			BgL_mz00_5720
																																				=
																																				BGl_hashtablezd2getzd2zz__hashz00
																																				(BgL_tablez00_6668,
																																				BgL_objz00_2833);
																																			{	/* Unsafe/intext.scm 1185 */
																																				bool_t
																																					BgL_test3043z00_10774;
																																				if (STRUCTP(BgL_mz00_5720))
																																					{	/* Unsafe/intext.scm 663 */
																																						BgL_test3043z00_10774
																																							=
																																							(STRUCT_KEY
																																							(BgL_mz00_5720)
																																							==
																																							BGl_symbol2667z00zz__intextz00);
																																					}
																																				else
																																					{	/* Unsafe/intext.scm 663 */
																																						BgL_test3043z00_10774
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																				if (BgL_test3043z00_10774)
																																					{
																																						obj_t
																																							BgL_mz00_10779;
																																						BgL_mz00_10779
																																							=
																																							BgL_mz00_5720;
																																						BgL_mz00_2718
																																							=
																																							BgL_mz00_10779;
																																						goto
																																							BgL_zc3z04anonymousza31958ze3z87_2719;
																																					}
																																				else
																																					{	/* Unsafe/intext.scm 1185 */
																																						BgL_objz00_2826
																																							=
																																							BgL_objz00_2833;
																																						{	/* Unsafe/intext.scm 1293 */
																																							obj_t
																																								BgL_customz00_5626;
																																							BgL_customz00_5626
																																								=
																																								BGL_PROCEDURE_CALL1
																																								(BGl_za2procedurezd2ze3stringza2z31zz__intextz00,
																																								BgL_objz00_2826);
																																							{	/* Unsafe/intext.scm 675 */
																																								obj_t
																																									BgL_arg1640z00_5627;
																																								{	/* Unsafe/intext.scm 663 */
																																									obj_t
																																										BgL_newz00_5628;
																																									BgL_newz00_5628
																																										=
																																										create_struct
																																										(BGl_symbol2667z00zz__intextz00,
																																										(int)
																																										(4L));
																																									{	/* Unsafe/intext.scm 663 */
																																										obj_t
																																											BgL_auxz00_10788;
																																										int
																																											BgL_tmpz00_10786;
																																										BgL_auxz00_10788
																																											=
																																											BINT
																																											(-1L);
																																										BgL_tmpz00_10786
																																											=
																																											(int)
																																											(3L);
																																										STRUCT_SET
																																											(BgL_newz00_5628,
																																											BgL_tmpz00_10786,
																																											BgL_auxz00_10788);
																																									}
																																									{	/* Unsafe/intext.scm 663 */
																																										obj_t
																																											BgL_auxz00_10793;
																																										int
																																											BgL_tmpz00_10791;
																																										BgL_auxz00_10793
																																											=
																																											BINT
																																											(0L);
																																										BgL_tmpz00_10791
																																											=
																																											(int)
																																											(2L);
																																										STRUCT_SET
																																											(BgL_newz00_5628,
																																											BgL_tmpz00_10791,
																																											BgL_auxz00_10793);
																																									}
																																									{	/* Unsafe/intext.scm 663 */
																																										int
																																											BgL_tmpz00_10796;
																																										BgL_tmpz00_10796
																																											=
																																											(int)
																																											(1L);
																																										STRUCT_SET
																																											(BgL_newz00_5628,
																																											BgL_tmpz00_10796,
																																											BgL_customz00_5626);
																																									}
																																									{	/* Unsafe/intext.scm 663 */
																																										int
																																											BgL_tmpz00_10799;
																																										BgL_tmpz00_10799
																																											=
																																											(int)
																																											(0L);
																																										STRUCT_SET
																																											(BgL_newz00_5628,
																																											BgL_tmpz00_10799,
																																											BgL_objz00_2826);
																																									}
																																									BgL_arg1640z00_5627
																																										=
																																										BgL_newz00_5628;
																																								}
																																								BGl_hashtablezd2putz12zc0zz__hashz00
																																									(BgL_tablez00_6668,
																																									BgL_objz00_2826,
																																									BgL_arg1640z00_5627);
																																							}
																																							{
																																								obj_t
																																									BgL_objz00_10803;
																																								BgL_objz00_10803
																																									=
																																									BgL_customz00_5626;
																																								BgL_objz00_2833
																																									=
																																									BgL_objz00_10803;
																																								goto
																																									BGl_markze70ze7zz__intextz00;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	else
																																		{	/* Unsafe/intext.scm 1337 */
																																			if (CUSTOMP(BgL_objz00_2833))
																																				{	/* Unsafe/intext.scm 1184 */
																																					obj_t
																																						BgL_mz00_5726;
																																					BgL_mz00_5726
																																						=
																																						BGl_hashtablezd2getzd2zz__hashz00
																																						(BgL_tablez00_6668,
																																						BgL_objz00_2833);
																																					{	/* Unsafe/intext.scm 1185 */
																																						bool_t
																																							BgL_test3046z00_10807;
																																						if (STRUCTP(BgL_mz00_5726))
																																							{	/* Unsafe/intext.scm 663 */
																																								BgL_test3046z00_10807
																																									=
																																									(STRUCT_KEY
																																									(BgL_mz00_5726)
																																									==
																																									BGl_symbol2667z00zz__intextz00);
																																							}
																																						else
																																							{	/* Unsafe/intext.scm 663 */
																																								BgL_test3046z00_10807
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																						if (BgL_test3046z00_10807)
																																							{
																																								obj_t
																																									BgL_mz00_10812;
																																								BgL_mz00_10812
																																									=
																																									BgL_mz00_5726;
																																								BgL_mz00_2718
																																									=
																																									BgL_mz00_10812;
																																								goto
																																									BgL_zc3z04anonymousza31958ze3z87_2719;
																																							}
																																						else
																																							{	/* Unsafe/intext.scm 1185 */
																																								BgL_objz00_2821
																																									=
																																									BgL_objz00_2833;
																																								{	/* Unsafe/intext.scm 1287 */
																																									obj_t
																																										BgL_vz00_5609;
																																									{	/* Unsafe/intext.scm 1287 */
																																										obj_t
																																											BgL_fun2012z00_5610;
																																										{	/* Unsafe/intext.scm 1287 */
																																											char
																																												*BgL_arg2011z00_5611;
																																											BgL_arg2011z00_5611
																																												=
																																												CUSTOM_IDENTIFIER
																																												(
																																												((obj_t) BgL_objz00_2821));
																																											{	/* Unsafe/intext.scm 1409 */
																																												obj_t
																																													BgL_cellz00_5613;
																																												BgL_cellz00_5613
																																													=
																																													BGl_assocz00zz__r4_pairs_and_lists_6_3z00
																																													(string_to_bstring
																																													(BgL_arg2011z00_5611),
																																													BGl_za2customzd2serializa7ationza2z75zz__intextz00);
																																												if (PAIRP(BgL_cellz00_5613))
																																													{	/* Unsafe/intext.scm 1410 */
																																														BgL_fun2012z00_5610
																																															=
																																															CAR
																																															(CDR
																																															(BgL_cellz00_5613));
																																													}
																																												else
																																													{	/* Unsafe/intext.scm 1410 */
																																														BgL_fun2012z00_5610
																																															=
																																															BGl_errorz00zz__errorz00
																																															(BGl_string2597z00zz__intextz00,
																																															BGl_string2669z00zz__intextz00,
																																															BGl_excerptz00zz__intextz00
																																															(string_to_bstring
																																																(BgL_arg2011z00_5611)));
																																													}
																																											}
																																										}
																																										BgL_vz00_5609
																																											=
																																											BGL_PROCEDURE_CALL2
																																											(BgL_fun2012z00_5610,
																																											BgL_objz00_2821,
																																											BgL_markzd2argzd2_6669);
																																									}
																																									{	/* Unsafe/intext.scm 675 */
																																										obj_t
																																											BgL_arg1640z00_5620;
																																										{	/* Unsafe/intext.scm 663 */
																																											obj_t
																																												BgL_newz00_5621;
																																											BgL_newz00_5621
																																												=
																																												create_struct
																																												(BGl_symbol2667z00zz__intextz00,
																																												(int)
																																												(4L));
																																											{	/* Unsafe/intext.scm 663 */
																																												obj_t
																																													BgL_auxz00_10833;
																																												int
																																													BgL_tmpz00_10831;
																																												BgL_auxz00_10833
																																													=
																																													BINT
																																													(-1L);
																																												BgL_tmpz00_10831
																																													=
																																													(int)
																																													(3L);
																																												STRUCT_SET
																																													(BgL_newz00_5621,
																																													BgL_tmpz00_10831,
																																													BgL_auxz00_10833);
																																											}
																																											{	/* Unsafe/intext.scm 663 */
																																												obj_t
																																													BgL_auxz00_10838;
																																												int
																																													BgL_tmpz00_10836;
																																												BgL_auxz00_10838
																																													=
																																													BINT
																																													(0L);
																																												BgL_tmpz00_10836
																																													=
																																													(int)
																																													(2L);
																																												STRUCT_SET
																																													(BgL_newz00_5621,
																																													BgL_tmpz00_10836,
																																													BgL_auxz00_10838);
																																											}
																																											{	/* Unsafe/intext.scm 663 */
																																												int
																																													BgL_tmpz00_10841;
																																												BgL_tmpz00_10841
																																													=
																																													(int)
																																													(1L);
																																												STRUCT_SET
																																													(BgL_newz00_5621,
																																													BgL_tmpz00_10841,
																																													BgL_vz00_5609);
																																											}
																																											{	/* Unsafe/intext.scm 663 */
																																												int
																																													BgL_tmpz00_10844;
																																												BgL_tmpz00_10844
																																													=
																																													(int)
																																													(0L);
																																												STRUCT_SET
																																													(BgL_newz00_5621,
																																													BgL_tmpz00_10844,
																																													BgL_objz00_2821);
																																											}
																																											BgL_arg1640z00_5620
																																												=
																																												BgL_newz00_5621;
																																										}
																																										BGl_hashtablezd2putz12zc0zz__hashz00
																																											(BgL_tablez00_6668,
																																											BgL_objz00_2821,
																																											BgL_arg1640z00_5620);
																																									}
																																									{
																																										obj_t
																																											BgL_objz00_10848;
																																										BgL_objz00_10848
																																											=
																																											BgL_vz00_5609;
																																										BgL_objz00_2833
																																											=
																																											BgL_objz00_10848;
																																										goto
																																											BGl_markze70ze7zz__intextz00;
																																									}
																																								}
																																							}
																																					}
																																				}
																																			else
																																				{	/* Unsafe/intext.scm 1339 */
																																					if (PROCESSP(BgL_objz00_2833))
																																						{	/* Unsafe/intext.scm 1184 */
																																							obj_t
																																								BgL_mz00_5732;
																																							BgL_mz00_5732
																																								=
																																								BGl_hashtablezd2getzd2zz__hashz00
																																								(BgL_tablez00_6668,
																																								BgL_objz00_2833);
																																							{	/* Unsafe/intext.scm 1185 */
																																								bool_t
																																									BgL_test3050z00_10852;
																																								if (STRUCTP(BgL_mz00_5732))
																																									{	/* Unsafe/intext.scm 663 */
																																										BgL_test3050z00_10852
																																											=
																																											(STRUCT_KEY
																																											(BgL_mz00_5732)
																																											==
																																											BGl_symbol2667z00zz__intextz00);
																																									}
																																								else
																																									{	/* Unsafe/intext.scm 663 */
																																										BgL_test3050z00_10852
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																								if (BgL_test3050z00_10852)
																																									{
																																										obj_t
																																											BgL_mz00_10857;
																																										BgL_mz00_10857
																																											=
																																											BgL_mz00_5732;
																																										BgL_mz00_2718
																																											=
																																											BgL_mz00_10857;
																																										goto
																																											BgL_zc3z04anonymousza31958ze3z87_2719;
																																									}
																																								else
																																									{	/* Unsafe/intext.scm 1185 */
																																										return
																																											BGL_PROCEDURE_CALL1
																																											(BGl_za2processzd2ze3stringza2z31zz__intextz00,
																																											BgL_objz00_2833);
																																									}
																																							}
																																						}
																																					else
																																						{	/* Unsafe/intext.scm 1341 */
																																							if (OPAQUEP(BgL_objz00_2833))
																																								{	/* Unsafe/intext.scm 1184 */
																																									obj_t
																																										BgL_mz00_5738;
																																									BgL_mz00_5738
																																										=
																																										BGl_hashtablezd2getzd2zz__hashz00
																																										(BgL_tablez00_6668,
																																										BgL_objz00_2833);
																																									{	/* Unsafe/intext.scm 1185 */
																																										bool_t
																																											BgL_test3053z00_10865;
																																										if (STRUCTP(BgL_mz00_5738))
																																											{	/* Unsafe/intext.scm 663 */
																																												BgL_test3053z00_10865
																																													=
																																													(STRUCT_KEY
																																													(BgL_mz00_5738)
																																													==
																																													BGl_symbol2667z00zz__intextz00);
																																											}
																																										else
																																											{	/* Unsafe/intext.scm 663 */
																																												BgL_test3053z00_10865
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																										if (BgL_test3053z00_10865)
																																											{
																																												obj_t
																																													BgL_mz00_10870;
																																												BgL_mz00_10870
																																													=
																																													BgL_mz00_5738;
																																												BgL_mz00_2718
																																													=
																																													BgL_mz00_10870;
																																												goto
																																													BgL_zc3z04anonymousza31958ze3z87_2719;
																																											}
																																										else
																																											{	/* Unsafe/intext.scm 1185 */
																																												return
																																													BGL_PROCEDURE_CALL1
																																													(BGl_za2opaquezd2ze3stringza2z31zz__intextz00,
																																													BgL_objz00_2833);
																																											}
																																									}
																																								}
																																							else
																																								{	/* Unsafe/intext.scm 1343 */
																																									if (POINTERP(BgL_objz00_2833))
																																										{	/* Unsafe/intext.scm 1184 */
																																											obj_t
																																												BgL_mz00_5744;
																																											BgL_mz00_5744
																																												=
																																												BGl_hashtablezd2getzd2zz__hashz00
																																												(BgL_tablez00_6668,
																																												BgL_objz00_2833);
																																											{	/* Unsafe/intext.scm 1185 */
																																												bool_t
																																													BgL_test3056z00_10878;
																																												if (STRUCTP(BgL_mz00_5744))
																																													{	/* Unsafe/intext.scm 663 */
																																														BgL_test3056z00_10878
																																															=
																																															(STRUCT_KEY
																																															(BgL_mz00_5744)
																																															==
																																															BGl_symbol2667z00zz__intextz00);
																																													}
																																												else
																																													{	/* Unsafe/intext.scm 663 */
																																														BgL_test3056z00_10878
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																												if (BgL_test3056z00_10878)
																																													{
																																														obj_t
																																															BgL_mz00_10883;
																																														BgL_mz00_10883
																																															=
																																															BgL_mz00_5744;
																																														BgL_mz00_2718
																																															=
																																															BgL_mz00_10883;
																																														goto
																																															BgL_zc3z04anonymousza31958ze3z87_2719;
																																													}
																																												else
																																													{	/* Unsafe/intext.scm 1185 */
																																														return
																																															BFALSE;
																																													}
																																											}
																																										}
																																									else
																																										{	/* Unsafe/intext.scm 1345 */
																																											return
																																												BFALSE;
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			}
		}

	}



/* marked-pair-length */
	long BGl_markedzd2pairzd2lengthz00zz__intextz00(obj_t BgL_tablez00_58,
		obj_t BgL_lz00_59)
	{
		{	/* Unsafe/intext.scm 1355 */
			{
				obj_t BgL_lz00_2884;
				long BgL_rz00_2885;

				BgL_lz00_2884 = BgL_lz00_59;
				BgL_rz00_2885 = 1L;
			BgL_zc3z04anonymousza32046ze3z87_2886:
				{	/* Unsafe/intext.scm 1358 */
					obj_t BgL_vcdrz00_2887;

					BgL_vcdrz00_2887 = CDR(((obj_t) BgL_lz00_2884));
					if (PAIRP(BgL_vcdrz00_2887))
						{	/* Unsafe/intext.scm 1360 */
							obj_t BgL_markz00_2889;

							BgL_markz00_2889 =
								BGl_hashtablezd2getzd2zz__hashz00(BgL_tablez00_58,
								BgL_vcdrz00_2887);
							{	/* Unsafe/intext.scm 1361 */
								bool_t BgL_test3059z00_10889;

								if (
									((long) CINT(STRUCT_REF(
												((obj_t) BgL_markz00_2889), (int) (2L))) > 0L))
									{	/* Unsafe/intext.scm 1361 */
										BgL_test3059z00_10889 = ((bool_t) 1);
									}
								else
									{	/* Unsafe/intext.scm 1361 */
										BgL_test3059z00_10889 =
											(
											(long) CINT(STRUCT_REF(
													((obj_t) BgL_markz00_2889), (int) (3L))) >= 0L);
									}
								if (BgL_test3059z00_10889)
									{	/* Unsafe/intext.scm 1361 */
										return (BgL_rz00_2885 + 1L);
									}
								else
									{
										long BgL_rz00_10903;
										obj_t BgL_lz00_10902;

										BgL_lz00_10902 = BgL_vcdrz00_2887;
										BgL_rz00_10903 = (BgL_rz00_2885 + 1L);
										BgL_rz00_2885 = BgL_rz00_10903;
										BgL_lz00_2884 = BgL_lz00_10902;
										goto BgL_zc3z04anonymousza32046ze3z87_2886;
									}
							}
						}
					else
						{	/* Unsafe/intext.scm 1359 */
							return (BgL_rz00_2885 + 1L);
						}
				}
			}
		}

	}



/* make-serialization-substring */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2serializa7ationzd2substringza7zz__intextz00(obj_t BgL_strz00_60,
		long BgL_offsetz00_61, long BgL_siza7eza7_62)
	{
		{	/* Unsafe/intext.scm 1370 */
			{	/* Unsafe/intext.scm 129 */
				obj_t BgL_newz00_5759;

				BgL_newz00_5759 =
					create_struct(BGl_symbol2659z00zz__intextz00, (int) (3L));
				{	/* Unsafe/intext.scm 129 */
					obj_t BgL_auxz00_10910;
					int BgL_tmpz00_10908;

					BgL_auxz00_10910 = BINT(BgL_siza7eza7_62);
					BgL_tmpz00_10908 = (int) (2L);
					STRUCT_SET(BgL_newz00_5759, BgL_tmpz00_10908, BgL_auxz00_10910);
				}
				{	/* Unsafe/intext.scm 129 */
					obj_t BgL_auxz00_10915;
					int BgL_tmpz00_10913;

					BgL_auxz00_10915 = BINT(BgL_offsetz00_61);
					BgL_tmpz00_10913 = (int) (1L);
					STRUCT_SET(BgL_newz00_5759, BgL_tmpz00_10913, BgL_auxz00_10915);
				}
				{	/* Unsafe/intext.scm 129 */
					int BgL_tmpz00_10918;

					BgL_tmpz00_10918 = (int) (0L);
					STRUCT_SET(BgL_newz00_5759, BgL_tmpz00_10918, BgL_strz00_60);
				}
				return BgL_newz00_5759;
			}
		}

	}



/* &make-serialization-substring */
	obj_t BGl_z62makezd2serializa7ationzd2substringzc5zz__intextz00(obj_t
		BgL_envz00_6611, obj_t BgL_strz00_6612, obj_t BgL_offsetz00_6613,
		obj_t BgL_siza7eza7_6614)
	{
		{	/* Unsafe/intext.scm 1370 */
			{	/* Unsafe/intext.scm 129 */
				long BgL_auxz00_10937;
				long BgL_auxz00_10928;
				obj_t BgL_auxz00_10921;

				{	/* Unsafe/intext.scm 129 */
					obj_t BgL_tmpz00_10938;

					if (INTEGERP(BgL_siza7eza7_6614))
						{	/* Unsafe/intext.scm 129 */
							BgL_tmpz00_10938 = BgL_siza7eza7_6614;
						}
					else
						{
							obj_t BgL_auxz00_10941;

							BgL_auxz00_10941 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
								BINT(4400L), BGl_string2670z00zz__intextz00,
								BGl_string2671z00zz__intextz00, BgL_siza7eza7_6614);
							FAILURE(BgL_auxz00_10941, BFALSE, BFALSE);
						}
					BgL_auxz00_10937 = (long) CINT(BgL_tmpz00_10938);
				}
				{	/* Unsafe/intext.scm 129 */
					obj_t BgL_tmpz00_10929;

					if (INTEGERP(BgL_offsetz00_6613))
						{	/* Unsafe/intext.scm 129 */
							BgL_tmpz00_10929 = BgL_offsetz00_6613;
						}
					else
						{
							obj_t BgL_auxz00_10932;

							BgL_auxz00_10932 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
								BINT(4400L), BGl_string2670z00zz__intextz00,
								BGl_string2671z00zz__intextz00, BgL_offsetz00_6613);
							FAILURE(BgL_auxz00_10932, BFALSE, BFALSE);
						}
					BgL_auxz00_10928 = (long) CINT(BgL_tmpz00_10929);
				}
				if (STRINGP(BgL_strz00_6612))
					{	/* Unsafe/intext.scm 129 */
						BgL_auxz00_10921 = BgL_strz00_6612;
					}
				else
					{
						obj_t BgL_auxz00_10924;

						BgL_auxz00_10924 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(4400L), BGl_string2670z00zz__intextz00,
							BGl_string2609z00zz__intextz00, BgL_strz00_6612);
						FAILURE(BgL_auxz00_10924, BFALSE, BFALSE);
					}
				return
					BGl_makezd2serializa7ationzd2substringza7zz__intextz00
					(BgL_auxz00_10921, BgL_auxz00_10928, BgL_auxz00_10937);
			}
		}

	}



/* register-custom-serialization! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2customzd2serializa7ationz12zb5zz__intextz00(obj_t
		BgL_identz00_63, obj_t BgL_serializa7erza7_64,
		obj_t BgL_unserializa7erza7_65)
	{
		{	/* Unsafe/intext.scm 1381 */
			{	/* Unsafe/intext.scm 1382 */
				obj_t BgL_cellz00_2898;

				BgL_cellz00_2898 =
					BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_identz00_63,
					BGl_za2customzd2serializa7ationza2z75zz__intextz00);
				if (PAIRP(BgL_cellz00_2898))
					{	/* Unsafe/intext.scm 1383 */
						return BFALSE;
					}
				else
					{	/* Unsafe/intext.scm 1384 */
						obj_t BgL_procz00_2900;

						{	/* Unsafe/intext.scm 1384 */
							int BgL_aux1095z00_2906;

							BgL_aux1095z00_2906 = PROCEDURE_ARITY(BgL_serializa7erza7_64);
							switch ((long) (BgL_aux1095z00_2906))
								{
								case 1L:

									{	/* Unsafe/intext.scm 1386 */
										obj_t BgL_zc3z04anonymousza32061ze3z87_6615;

										BgL_zc3z04anonymousza32061ze3z87_6615 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza32061ze3ze5zz__intextz00,
											(int) (2L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza32061ze3z87_6615,
											(int) (0L), BgL_serializa7erza7_64);
										BgL_procz00_2900 = BgL_zc3z04anonymousza32061ze3z87_6615;
									} break;
								case 2L:

									BgL_procz00_2900 = BgL_serializa7erza7_64;
									break;
								default:
									BgL_procz00_2900 =
										BGl_errorz00zz__errorz00(BGl_string2672z00zz__intextz00,
										BGl_string2673z00zz__intextz00, BgL_serializa7erza7_64);
								}
						}
						{	/* Unsafe/intext.scm 1393 */
							obj_t BgL_arg2057z00_2901;

							{	/* Unsafe/intext.scm 1393 */
								obj_t BgL_list2058z00_2902;

								{	/* Unsafe/intext.scm 1393 */
									obj_t BgL_arg2059z00_2903;

									{	/* Unsafe/intext.scm 1393 */
										obj_t BgL_arg2060z00_2904;

										BgL_arg2060z00_2904 =
											MAKE_YOUNG_PAIR(BgL_unserializa7erza7_65, BNIL);
										BgL_arg2059z00_2903 =
											MAKE_YOUNG_PAIR(BgL_procz00_2900, BgL_arg2060z00_2904);
									}
									BgL_list2058z00_2902 =
										MAKE_YOUNG_PAIR(BgL_identz00_63, BgL_arg2059z00_2903);
								}
								BgL_arg2057z00_2901 = BgL_list2058z00_2902;
							}
							return (BGl_za2customzd2serializa7ationza2z75zz__intextz00 =
								MAKE_YOUNG_PAIR(BgL_arg2057z00_2901,
									BGl_za2customzd2serializa7ationza2z75zz__intextz00), BUNSPEC);
						}
					}
			}
		}

	}



/* &register-custom-serialization! */
	obj_t BGl_z62registerzd2customzd2serializa7ationz12zd7zz__intextz00(obj_t
		BgL_envz00_6616, obj_t BgL_identz00_6617, obj_t BgL_serializa7erza7_6618,
		obj_t BgL_unserializa7erza7_6619)
	{
		{	/* Unsafe/intext.scm 1381 */
			{	/* Unsafe/intext.scm 1382 */
				obj_t BgL_auxz00_10977;
				obj_t BgL_auxz00_10970;
				obj_t BgL_auxz00_10963;

				if (PROCEDUREP(BgL_unserializa7erza7_6619))
					{	/* Unsafe/intext.scm 1382 */
						BgL_auxz00_10977 = BgL_unserializa7erza7_6619;
					}
				else
					{
						obj_t BgL_auxz00_10980;

						BgL_auxz00_10980 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(41267L), BGl_string2674z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_unserializa7erza7_6619);
						FAILURE(BgL_auxz00_10980, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_serializa7erza7_6618))
					{	/* Unsafe/intext.scm 1382 */
						BgL_auxz00_10970 = BgL_serializa7erza7_6618;
					}
				else
					{
						obj_t BgL_auxz00_10973;

						BgL_auxz00_10973 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(41267L), BGl_string2674z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_serializa7erza7_6618);
						FAILURE(BgL_auxz00_10973, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_identz00_6617))
					{	/* Unsafe/intext.scm 1382 */
						BgL_auxz00_10963 = BgL_identz00_6617;
					}
				else
					{
						obj_t BgL_auxz00_10966;

						BgL_auxz00_10966 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(41267L), BGl_string2674z00zz__intextz00,
							BGl_string2609z00zz__intextz00, BgL_identz00_6617);
						FAILURE(BgL_auxz00_10966, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2customzd2serializa7ationz12zb5zz__intextz00
					(BgL_auxz00_10963, BgL_auxz00_10970, BgL_auxz00_10977);
			}
		}

	}



/* &<@anonymous:2061> */
	obj_t BGl_z62zc3z04anonymousza32061ze3ze5zz__intextz00(obj_t BgL_envz00_6620,
		obj_t BgL_oz00_6622, obj_t BgL_markzd2argzd2_6623)
	{
		{	/* Unsafe/intext.scm 1386 */
			{	/* Unsafe/intext.scm 1386 */
				obj_t BgL_serializa7erza7_6621;

				BgL_serializa7erza7_6621 =
					((obj_t) PROCEDURE_REF(BgL_envz00_6620, (int) (0L)));
				return BGL_PROCEDURE_CALL1(BgL_serializa7erza7_6621, BgL_oz00_6622);
			}
		}

	}



/* excerpt */
	obj_t BGl_excerptz00zz__intextz00(obj_t BgL_objz00_66)
	{
		{	/* Unsafe/intext.scm 1399 */
			if (STRINGP(BgL_objz00_66))
				{	/* Unsafe/intext.scm 1401 */
					if ((STRING_LENGTH(BgL_objz00_66) <= 80L))
						{	/* Unsafe/intext.scm 1402 */
							return string_for_read(BgL_objz00_66);
						}
					else
						{	/* Unsafe/intext.scm 1402 */
							return
								string_append(string_for_read(c_substring(BgL_objz00_66, 0L,
										80L)), BGl_string2676z00zz__intextz00);
						}
				}
			else
				{	/* Unsafe/intext.scm 1401 */
					return BgL_objz00_66;
				}
		}

	}



/* get-custom-serialization */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2customzd2serializa7ationza7zz__intextz00(obj_t BgL_identz00_69)
	{
		{	/* Unsafe/intext.scm 1426 */
			{	/* Unsafe/intext.scm 1427 */
				obj_t BgL_cellz00_2922;

				BgL_cellz00_2922 =
					BGl_assocz00zz__r4_pairs_and_lists_6_3z00(BgL_identz00_69,
					BGl_za2customzd2serializa7ationza2z75zz__intextz00);
				if (PAIRP(BgL_cellz00_2922))
					{	/* Unsafe/intext.scm 1429 */
						obj_t BgL_val0_1166z00_2924;
						obj_t BgL_val1_1167z00_2925;

						BgL_val0_1166z00_2924 = CAR(CDR(BgL_cellz00_2922));
						BgL_val1_1167z00_2925 = CAR(CDR(CDR(BgL_cellz00_2922)));
						{	/* Unsafe/intext.scm 1429 */
							int BgL_tmpz00_11009;

							BgL_tmpz00_11009 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11009);
						}
						{	/* Unsafe/intext.scm 1429 */
							int BgL_tmpz00_11012;

							BgL_tmpz00_11012 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11012, BgL_val1_1167z00_2925);
						}
						return BgL_val0_1166z00_2924;
					}
				else
					{	/* Unsafe/intext.scm 1428 */
						{	/* Unsafe/intext.scm 1430 */
							int BgL_tmpz00_11015;

							BgL_tmpz00_11015 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11015);
						}
						{	/* Unsafe/intext.scm 1430 */
							int BgL_tmpz00_11018;

							BgL_tmpz00_11018 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11018, BFALSE);
						}
						return BFALSE;
					}
			}
		}

	}



/* &get-custom-serialization */
	obj_t BGl_z62getzd2customzd2serializa7ationzc5zz__intextz00(obj_t
		BgL_envz00_6624, obj_t BgL_identz00_6625)
	{
		{	/* Unsafe/intext.scm 1426 */
			{	/* Unsafe/intext.scm 1427 */
				obj_t BgL_auxz00_11021;

				if (STRINGP(BgL_identz00_6625))
					{	/* Unsafe/intext.scm 1427 */
						BgL_auxz00_11021 = BgL_identz00_6625;
					}
				else
					{
						obj_t BgL_auxz00_11024;

						BgL_auxz00_11024 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(43232L), BGl_string2677z00zz__intextz00,
							BGl_string2609z00zz__intextz00, BgL_identz00_6625);
						FAILURE(BgL_auxz00_11024, BFALSE, BFALSE);
					}
				return
					BGl_getzd2customzd2serializa7ationza7zz__intextz00(BgL_auxz00_11021);
			}
		}

	}



/* register-procedure-serialization! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2procedurezd2serializa7ationz12zb5zz__intextz00(obj_t
		BgL_serializa7erza7_70, obj_t BgL_unserializa7erza7_71)
	{
		{	/* Unsafe/intext.scm 1449 */
			BGl_za2procedurezd2ze3stringza2z31zz__intextz00 = BgL_serializa7erza7_70;
			return (BGl_za2stringzd2ze3procedureza2z31zz__intextz00 =
				BgL_unserializa7erza7_71, BUNSPEC);
		}

	}



/* &register-procedure-serialization! */
	obj_t BGl_z62registerzd2procedurezd2serializa7ationz12zd7zz__intextz00(obj_t
		BgL_envz00_6626, obj_t BgL_serializa7erza7_6627,
		obj_t BgL_unserializa7erza7_6628)
	{
		{	/* Unsafe/intext.scm 1449 */
			{	/* Unsafe/intext.scm 1450 */
				obj_t BgL_auxz00_11036;
				obj_t BgL_auxz00_11029;

				if (PROCEDUREP(BgL_unserializa7erza7_6628))
					{	/* Unsafe/intext.scm 1450 */
						BgL_auxz00_11036 = BgL_unserializa7erza7_6628;
					}
				else
					{
						obj_t BgL_auxz00_11039;

						BgL_auxz00_11039 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(44386L), BGl_string2678z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_unserializa7erza7_6628);
						FAILURE(BgL_auxz00_11039, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_serializa7erza7_6627))
					{	/* Unsafe/intext.scm 1450 */
						BgL_auxz00_11029 = BgL_serializa7erza7_6627;
					}
				else
					{
						obj_t BgL_auxz00_11032;

						BgL_auxz00_11032 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(44386L), BGl_string2678z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_serializa7erza7_6627);
						FAILURE(BgL_auxz00_11032, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2procedurezd2serializa7ationz12zb5zz__intextz00
					(BgL_auxz00_11029, BgL_auxz00_11036);
			}
		}

	}



/* get-procedure-serialization */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2procedurezd2serializa7ationza7zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 1456 */
			return
				MAKE_YOUNG_PAIR(BGl_za2procedurezd2ze3stringza2z31zz__intextz00,
				BGl_za2stringzd2ze3procedureza2z31zz__intextz00);
		}

	}



/* &get-procedure-serialization */
	obj_t BGl_z62getzd2procedurezd2serializa7ationzc5zz__intextz00(obj_t
		BgL_envz00_6629)
	{
		{	/* Unsafe/intext.scm 1456 */
			return BGl_getzd2procedurezd2serializa7ationza7zz__intextz00();
		}

	}



/* register-process-serialization! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2processzd2serializa7ationz12zb5zz__intextz00(obj_t
		BgL_serializa7erza7_72, obj_t BgL_unserializa7erza7_73)
	{
		{	/* Unsafe/intext.scm 1476 */
			BGl_za2processzd2ze3stringza2z31zz__intextz00 = BgL_serializa7erza7_72;
			return (BGl_za2stringzd2ze3processza2z31zz__intextz00 =
				BgL_unserializa7erza7_73, BUNSPEC);
		}

	}



/* &register-process-serialization! */
	obj_t BGl_z62registerzd2processzd2serializa7ationz12zd7zz__intextz00(obj_t
		BgL_envz00_6630, obj_t BgL_serializa7erza7_6631,
		obj_t BgL_unserializa7erza7_6632)
	{
		{	/* Unsafe/intext.scm 1476 */
			{	/* Unsafe/intext.scm 1477 */
				obj_t BgL_auxz00_11053;
				obj_t BgL_auxz00_11046;

				if (PROCEDUREP(BgL_unserializa7erza7_6632))
					{	/* Unsafe/intext.scm 1477 */
						BgL_auxz00_11053 = BgL_unserializa7erza7_6632;
					}
				else
					{
						obj_t BgL_auxz00_11056;

						BgL_auxz00_11056 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(45757L), BGl_string2679z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_unserializa7erza7_6632);
						FAILURE(BgL_auxz00_11056, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_serializa7erza7_6631))
					{	/* Unsafe/intext.scm 1477 */
						BgL_auxz00_11046 = BgL_serializa7erza7_6631;
					}
				else
					{
						obj_t BgL_auxz00_11049;

						BgL_auxz00_11049 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(45757L), BGl_string2679z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_serializa7erza7_6631);
						FAILURE(BgL_auxz00_11049, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2processzd2serializa7ationz12zb5zz__intextz00
					(BgL_auxz00_11046, BgL_auxz00_11053);
			}
		}

	}



/* register-opaque-serialization! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2opaquezd2serializa7ationz12zb5zz__intextz00(obj_t
		BgL_serializa7erza7_74, obj_t BgL_unserializa7erza7_75)
	{
		{	/* Unsafe/intext.scm 1503 */
			BGl_za2opaquezd2ze3stringza2z31zz__intextz00 = BgL_serializa7erza7_74;
			return (BGl_za2stringzd2ze3opaqueza2z31zz__intextz00 =
				BgL_unserializa7erza7_75, BUNSPEC);
		}

	}



/* &register-opaque-serialization! */
	obj_t BGl_z62registerzd2opaquezd2serializa7ationz12zd7zz__intextz00(obj_t
		BgL_envz00_6633, obj_t BgL_serializa7erza7_6634,
		obj_t BgL_unserializa7erza7_6635)
	{
		{	/* Unsafe/intext.scm 1503 */
			{	/* Unsafe/intext.scm 1504 */
				obj_t BgL_auxz00_11068;
				obj_t BgL_auxz00_11061;

				if (PROCEDUREP(BgL_unserializa7erza7_6635))
					{	/* Unsafe/intext.scm 1504 */
						BgL_auxz00_11068 = BgL_unserializa7erza7_6635;
					}
				else
					{
						obj_t BgL_auxz00_11071;

						BgL_auxz00_11071 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(47103L), BGl_string2680z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_unserializa7erza7_6635);
						FAILURE(BgL_auxz00_11071, BFALSE, BFALSE);
					}
				if (PROCEDUREP(BgL_serializa7erza7_6634))
					{	/* Unsafe/intext.scm 1504 */
						BgL_auxz00_11061 = BgL_serializa7erza7_6634;
					}
				else
					{
						obj_t BgL_auxz00_11064;

						BgL_auxz00_11064 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(47103L), BGl_string2680z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_serializa7erza7_6634);
						FAILURE(BgL_auxz00_11064, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2opaquezd2serializa7ationz12zb5zz__intextz00
					(BgL_auxz00_11061, BgL_auxz00_11068);
			}
		}

	}



/* get-opaque-serialization */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2opaquezd2serializa7ationza7zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 1510 */
			{	/* Unsafe/intext.scm 1511 */
				obj_t BgL_val0_1172z00_2930;
				obj_t BgL_val1_1173z00_2931;

				BgL_val0_1172z00_2930 = BGl_za2opaquezd2ze3stringza2z31zz__intextz00;
				BgL_val1_1173z00_2931 = BGl_za2stringzd2ze3opaqueza2z31zz__intextz00;
				{	/* Unsafe/intext.scm 1511 */
					int BgL_tmpz00_11076;

					BgL_tmpz00_11076 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11076);
				}
				{	/* Unsafe/intext.scm 1511 */
					int BgL_tmpz00_11079;

					BgL_tmpz00_11079 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_11079, BgL_val1_1173z00_2931);
				}
				return BgL_val0_1172z00_2930;
			}
		}

	}



/* &get-opaque-serialization */
	obj_t BGl_z62getzd2opaquezd2serializa7ationzc5zz__intextz00(obj_t
		BgL_envz00_6636)
	{
		{	/* Unsafe/intext.scm 1510 */
			return BGl_getzd2opaquezd2serializa7ationza7zz__intextz00();
		}

	}



/* register-class-serialization! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2classzd2serializa7ationz12zb5zz__intextz00(obj_t
		BgL_classz00_78, obj_t BgL_serializa7erza7_79,
		obj_t BgL_unserializa7erza7_80)
	{
		{	/* Unsafe/intext.scm 1527 */
			{
				obj_t BgL_unserializa7erza7_2959;
				long BgL_hashz00_2946;
				obj_t BgL_serializa7erza7_2947;

				{	/* Unsafe/intext.scm 1552 */
					long BgL_hashz00_2934;

					BgL_hashz00_2934 = BGl_classzd2hashzd2zz__objectz00(BgL_classz00_78);
					{	/* Unsafe/intext.scm 1552 */
						obj_t BgL_cellz00_2935;

						BgL_cellz00_2935 =
							BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_hashz00_2934),
							BGl_za2classzd2serializa7ationza2z75zz__intextz00);
						{	/* Unsafe/intext.scm 1553 */

							if (CBOOL(BgL_serializa7erza7_79))
								{	/* Unsafe/intext.scm 1558 */
									obj_t BgL_arg2074z00_2936;
									obj_t BgL_arg2075z00_2937;

									BgL_hashz00_2946 = BgL_hashz00_2934;
									BgL_serializa7erza7_2947 = BgL_serializa7erza7_79;
									{	/* Unsafe/intext.scm 1530 */
										int BgL_aux1097z00_2950;

										BgL_aux1097z00_2950 =
											PROCEDURE_ARITY(((obj_t) BgL_serializa7erza7_2947));
										switch ((long) (BgL_aux1097z00_2950))
											{
											case 1L:

												{	/* Unsafe/intext.scm 1532 */
													obj_t BgL_zc3z04anonymousza32085ze3z87_6639;

													BgL_zc3z04anonymousza32085ze3z87_6639 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza32085ze3ze5zz__intextz00,
														(int) (2L), (int) (2L));
													PROCEDURE_SET(BgL_zc3z04anonymousza32085ze3z87_6639,
														(int) (0L), BgL_serializa7erza7_2947);
													PROCEDURE_SET(BgL_zc3z04anonymousza32085ze3z87_6639,
														(int) (1L), BINT(BgL_hashz00_2946));
													BgL_arg2074z00_2936 =
														BgL_zc3z04anonymousza32085ze3z87_6639;
												} break;
											case 2L:

												{	/* Unsafe/intext.scm 1536 */
													obj_t BgL_zc3z04anonymousza32086ze3z87_6638;

													BgL_zc3z04anonymousza32086ze3z87_6638 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza32086ze3ze5zz__intextz00,
														(int) (2L), (int) (2L));
													PROCEDURE_SET(BgL_zc3z04anonymousza32086ze3z87_6638,
														(int) (0L), BgL_serializa7erza7_2947);
													PROCEDURE_SET(BgL_zc3z04anonymousza32086ze3z87_6638,
														(int) (1L), BINT(BgL_hashz00_2946));
													BgL_arg2074z00_2936 =
														BgL_zc3z04anonymousza32086ze3z87_6638;
												} break;
											default:
												BgL_arg2074z00_2936 =
													BGl_errorz00zz__errorz00
													(BGl_string2681z00zz__intextz00,
													BGl_string2673z00zz__intextz00,
													BgL_serializa7erza7_2947);
											}
									}
									{	/* Unsafe/intext.scm 1559 */
										obj_t BgL_arg2076z00_2938;

										BgL_arg2076z00_2938 =
											SYMBOL_TO_STRING(BGl_classzd2namezd2zz__objectz00
											(BgL_classz00_78));
										BgL_arg2075z00_2937 =
											string_append(BgL_arg2076z00_2938,
											BGl_string2682z00zz__intextz00);
									}
									BGl_genericzd2addzd2methodz12z12zz__objectz00
										(BGl_objectzd2serializa7erzd2envza7zz__intextz00,
										BgL_classz00_78, BgL_arg2074z00_2936, BgL_arg2075z00_2937);
								}
							else
								{	/* Unsafe/intext.scm 1554 */
									BFALSE;
								}
							if (PAIRP(BgL_cellz00_2935))
								{	/* Unsafe/intext.scm 1560 */
									return BFALSE;
								}
							else
								{	/* Unsafe/intext.scm 1562 */
									obj_t BgL_arg2079z00_2941;

									{	/* Unsafe/intext.scm 1562 */
										obj_t BgL_arg2080z00_2942;

										BgL_unserializa7erza7_2959 = BgL_unserializa7erza7_80;
										{	/* Unsafe/intext.scm 1543 */
											int BgL_aux1099z00_2962;

											BgL_aux1099z00_2962 =
												PROCEDURE_ARITY(BgL_unserializa7erza7_2959);
											switch ((long) (BgL_aux1099z00_2962))
												{
												case 1L:

													{	/* Unsafe/intext.scm 1545 */
														obj_t BgL_zc3z04anonymousza32088ze3z87_6637;

														BgL_zc3z04anonymousza32088ze3z87_6637 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza32088ze3ze5zz__intextz00,
															(int) (2L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza32088ze3z87_6637,
															(int) (0L), BgL_unserializa7erza7_2959);
														BgL_arg2080z00_2942 =
															BgL_zc3z04anonymousza32088ze3z87_6637;
													} break;
												case 2L:

													BgL_arg2080z00_2942 = BgL_unserializa7erza7_2959;
													break;
												default:
													BgL_arg2080z00_2942 =
														BGl_errorz00zz__errorz00
														(BGl_string2681z00zz__intextz00,
														BGl_string2673z00zz__intextz00,
														BgL_unserializa7erza7_2959);
												}
										}
										{	/* Unsafe/intext.scm 1562 */
											obj_t BgL_list2081z00_2943;

											{	/* Unsafe/intext.scm 1562 */
												obj_t BgL_arg2082z00_2944;

												{	/* Unsafe/intext.scm 1562 */
													obj_t BgL_arg2083z00_2945;

													BgL_arg2083z00_2945 =
														MAKE_YOUNG_PAIR(BgL_arg2080z00_2942, BNIL);
													BgL_arg2082z00_2944 =
														MAKE_YOUNG_PAIR(BgL_serializa7erza7_79,
														BgL_arg2083z00_2945);
												}
												BgL_list2081z00_2943 =
													MAKE_YOUNG_PAIR(BINT(BgL_hashz00_2934),
													BgL_arg2082z00_2944);
											}
											BgL_arg2079z00_2941 = BgL_list2081z00_2943;
										}
									}
									return (BGl_za2classzd2serializa7ationza2z75zz__intextz00 =
										MAKE_YOUNG_PAIR(BgL_arg2079z00_2941,
											BGl_za2classzd2serializa7ationza2z75zz__intextz00),
										BUNSPEC);
								}
						}
					}
				}
			}
		}

	}



/* &register-class-serialization! */
	obj_t BGl_z62registerzd2classzd2serializa7ationz12zd7zz__intextz00(obj_t
		BgL_envz00_6640, obj_t BgL_classz00_6641, obj_t BgL_serializa7erza7_6642,
		obj_t BgL_unserializa7erza7_6643)
	{
		{	/* Unsafe/intext.scm 1527 */
			{	/* Unsafe/intext.scm 1530 */
				obj_t BgL_auxz00_11129;

				if (PROCEDUREP(BgL_unserializa7erza7_6643))
					{	/* Unsafe/intext.scm 1530 */
						BgL_auxz00_11129 = BgL_unserializa7erza7_6643;
					}
				else
					{
						obj_t BgL_auxz00_11132;

						BgL_auxz00_11132 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
							BINT(48348L), BGl_string2683z00zz__intextz00,
							BGl_string2675z00zz__intextz00, BgL_unserializa7erza7_6643);
						FAILURE(BgL_auxz00_11132, BFALSE, BFALSE);
					}
				return
					BGl_registerzd2classzd2serializa7ationz12zb5zz__intextz00
					(BgL_classz00_6641, BgL_serializa7erza7_6642, BgL_auxz00_11129);
			}
		}

	}



/* &<@anonymous:2088> */
	obj_t BGl_z62zc3z04anonymousza32088ze3ze5zz__intextz00(obj_t BgL_envz00_6644,
		obj_t BgL_oz00_6646, obj_t BgL_argz00_6647)
	{
		{	/* Unsafe/intext.scm 1545 */
			{	/* Unsafe/intext.scm 1546 */
				obj_t BgL_unserializa7erza7_6645;

				BgL_unserializa7erza7_6645 =
					((obj_t) PROCEDURE_REF(BgL_envz00_6644, (int) (0L)));
				return BGL_PROCEDURE_CALL1(BgL_unserializa7erza7_6645, BgL_oz00_6646);
			}
		}

	}



/* &<@anonymous:2086> */
	obj_t BGl_z62zc3z04anonymousza32086ze3ze5zz__intextz00(obj_t BgL_envz00_6648,
		obj_t BgL_oz00_6651, obj_t BgL_markzd2argzd2_6652)
	{
		{	/* Unsafe/intext.scm 1536 */
			{	/* Unsafe/intext.scm 1537 */
				obj_t BgL_serializa7erza7_6649;
				long BgL_hashz00_6650;

				BgL_serializa7erza7_6649 = PROCEDURE_REF(BgL_envz00_6648, (int) (0L));
				BgL_hashz00_6650 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_6648, (int) (1L)));
				{	/* Unsafe/intext.scm 1537 */
					obj_t BgL_soz00_7075;

					BgL_soz00_7075 =
						BGL_PROCEDURE_CALL2(BgL_serializa7erza7_6649, BgL_oz00_6651,
						BgL_markzd2argzd2_6652);
					if ((BgL_soz00_7075 == BgL_oz00_6651))
						{	/* Unsafe/intext.scm 1538 */
							return BgL_soz00_7075;
						}
					else
						{	/* Unsafe/intext.scm 1538 */
							return MAKE_YOUNG_PAIR(BINT(BgL_hashz00_6650), BgL_soz00_7075);
						}
				}
			}
		}

	}



/* &<@anonymous:2085> */
	obj_t BGl_z62zc3z04anonymousza32085ze3ze5zz__intextz00(obj_t BgL_envz00_6653,
		obj_t BgL_oz00_6656, obj_t BgL_markzd2argzd2_6657)
	{
		{	/* Unsafe/intext.scm 1532 */
			{	/* Unsafe/intext.scm 1533 */
				obj_t BgL_serializa7erza7_6654;
				long BgL_hashz00_6655;

				BgL_serializa7erza7_6654 = PROCEDURE_REF(BgL_envz00_6653, (int) (0L));
				BgL_hashz00_6655 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_6653, (int) (1L)));
				{	/* Unsafe/intext.scm 1533 */
					obj_t BgL_soz00_7076;

					BgL_soz00_7076 =
						BGL_PROCEDURE_CALL1(BgL_serializa7erza7_6654, BgL_oz00_6656);
					if ((BgL_soz00_7076 == BgL_oz00_6656))
						{	/* Unsafe/intext.scm 1534 */
							return BgL_oz00_6656;
						}
					else
						{	/* Unsafe/intext.scm 1534 */
							return MAKE_YOUNG_PAIR(BINT(BgL_hashz00_6655), BgL_soz00_7076);
						}
				}
			}
		}

	}



/* find-class-unserializer */
	obj_t BGl_findzd2classzd2unserializa7erza7zz__intextz00(obj_t BgL_hashz00_81,
		obj_t BgL_namez00_82)
	{
		{	/* Unsafe/intext.scm 1568 */
			{	/* Unsafe/intext.scm 1569 */
				obj_t BgL_hz00_2968;

				if (((long) CINT(BgL_hashz00_81) == 0L))
					{	/* Unsafe/intext.scm 1569 */
						BgL_hz00_2968 =
							BINT(BGl_classzd2hashzd2zz__objectz00
							(BGl_objectz00zz__objectz00));
					}
				else
					{	/* Unsafe/intext.scm 1569 */
						BgL_hz00_2968 = BgL_hashz00_81;
					}
				{	/* Unsafe/intext.scm 1569 */
					obj_t BgL_cellz00_2969;

					BgL_cellz00_2969 =
						BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BgL_hz00_2968,
						BGl_za2classzd2serializa7ationza2z75zz__intextz00);
					{	/* Unsafe/intext.scm 1570 */

						if (PAIRP(BgL_cellz00_2969))
							{	/* Unsafe/intext.scm 1571 */
								return CAR(CDR(CDR(BgL_cellz00_2969)));
							}
						else
							{	/* Unsafe/intext.scm 1574 */
								obj_t BgL_arg2090z00_2971;

								if (CBOOL(BgL_namez00_82))
									{	/* Unsafe/intext.scm 1575 */
										obj_t BgL_list2091z00_2972;

										BgL_list2091z00_2972 =
											MAKE_YOUNG_PAIR(BgL_namez00_82, BNIL);
										BgL_arg2090z00_2971 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string2684z00zz__intextz00, BgL_list2091z00_2972);
									}
								else
									{	/* Unsafe/intext.scm 1574 */
										BgL_arg2090z00_2971 = BGl_string2617z00zz__intextz00;
									}
								return
									BGl_errorz00zz__errorz00(BGl_string2595z00zz__intextz00,
									BgL_arg2090z00_2971, BgL_hashz00_81);
							}
					}
				}
			}
		}

	}



/* get-class-serialization */
	BGL_EXPORTED_DEF obj_t BGl_getzd2classzd2serializa7ationza7zz__intextz00(obj_t
		BgL_classz00_83)
	{
		{	/* Unsafe/intext.scm 1582 */
			{	/* Unsafe/intext.scm 1583 */
				long BgL_hashz00_2974;

				BgL_hashz00_2974 = BGl_classzd2hashzd2zz__objectz00(BgL_classz00_83);
				{	/* Unsafe/intext.scm 1583 */
					obj_t BgL_cellz00_2975;

					BgL_cellz00_2975 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BINT(BgL_hashz00_2974),
						BGl_za2classzd2serializa7ationza2z75zz__intextz00);
					{	/* Unsafe/intext.scm 1584 */

						if (PAIRP(BgL_cellz00_2975))
							{	/* Unsafe/intext.scm 1586 */
								obj_t BgL_val0_1174z00_2977;
								obj_t BgL_val1_1175z00_2978;

								BgL_val0_1174z00_2977 = CAR(CDR(BgL_cellz00_2975));
								BgL_val1_1175z00_2978 = CAR(CDR(CDR(BgL_cellz00_2975)));
								{	/* Unsafe/intext.scm 1586 */
									int BgL_tmpz00_11197;

									BgL_tmpz00_11197 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11197);
								}
								{	/* Unsafe/intext.scm 1586 */
									int BgL_tmpz00_11200;

									BgL_tmpz00_11200 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_11200, BgL_val1_1175z00_2978);
								}
								return BgL_val0_1174z00_2977;
							}
						else
							{	/* Unsafe/intext.scm 1585 */
								{	/* Unsafe/intext.scm 1587 */
									int BgL_tmpz00_11203;

									BgL_tmpz00_11203 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11203);
								}
								{	/* Unsafe/intext.scm 1587 */
									int BgL_tmpz00_11206;

									BgL_tmpz00_11206 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_11206, BFALSE);
								}
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* &get-class-serialization */
	obj_t BGl_z62getzd2classzd2serializa7ationzc5zz__intextz00(obj_t
		BgL_envz00_6661, obj_t BgL_classz00_6662)
	{
		{	/* Unsafe/intext.scm 1582 */
			return
				BGl_getzd2classzd2serializa7ationza7zz__intextz00(BgL_classz00_6662);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_objectzd2serializa7erzd2envza7zz__intextz00,
				BGl_proc2685z00zz__intextz00, BGl_objectz00zz__objectz00,
				BGl_string2686z00zz__intextz00);
		}

	}



/* &object-serializer1207 */
	obj_t BGl_z62objectzd2serializa7er1207z17zz__intextz00(obj_t BgL_envz00_6664,
		obj_t BgL_objz00_6665, obj_t BgL_markzd2argzd2_6666)
	{
		{	/* Unsafe/intext.scm 1516 */
			return ((obj_t) ((BgL_objectz00_bglt) BgL_objz00_6665));
		}

	}



/* object-serializer */
	obj_t BGl_objectzd2serializa7erz75zz__intextz00(BgL_objectz00_bglt
		BgL_objz00_76, obj_t BgL_markzd2argzd2_77)
	{
		{	/* Unsafe/intext.scm 1516 */
			{	/* Unsafe/intext.scm 1516 */
				obj_t BgL_method1208z00_2986;

				{	/* Unsafe/intext.scm 1516 */
					obj_t BgL_res2542z00_5849;

					{	/* Unsafe/intext.scm 1516 */
						long BgL_objzd2classzd2numz00_5820;

						BgL_objzd2classzd2numz00_5820 = BGL_OBJECT_CLASS_NUM(BgL_objz00_76);
						{	/* Unsafe/intext.scm 1516 */
							obj_t BgL_arg2491z00_5821;

							BgL_arg2491z00_5821 =
								PROCEDURE_REF(BGl_objectzd2serializa7erzd2envza7zz__intextz00,
								(int) (1L));
							{	/* Unsafe/intext.scm 1516 */
								int BgL_offsetz00_5824;

								BgL_offsetz00_5824 = (int) (BgL_objzd2classzd2numz00_5820);
								{	/* Unsafe/intext.scm 1516 */
									long BgL_offsetz00_5825;

									BgL_offsetz00_5825 =
										((long) (BgL_offsetz00_5824) - OBJECT_TYPE);
									{	/* Unsafe/intext.scm 1516 */
										long BgL_modz00_5826;

										BgL_modz00_5826 =
											(BgL_offsetz00_5825 >> (int) ((long) ((int) (4L))));
										{	/* Unsafe/intext.scm 1516 */
											long BgL_restz00_5828;

											BgL_restz00_5828 =
												(BgL_offsetz00_5825 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Unsafe/intext.scm 1516 */

												{	/* Unsafe/intext.scm 1516 */
													obj_t BgL_bucketz00_5830;

													BgL_bucketz00_5830 =
														VECTOR_REF(
														((obj_t) BgL_arg2491z00_5821), BgL_modz00_5826);
													BgL_res2542z00_5849 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5830), BgL_restz00_5828);
					}}}}}}}}
					BgL_method1208z00_2986 = BgL_res2542z00_5849;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1208z00_2986,
					((obj_t) BgL_objz00_76), BgL_markzd2argzd2_77);
			}
		}

	}



/* &object-serializer */
	obj_t BGl_z62objectzd2serializa7erz17zz__intextz00(obj_t BgL_envz00_6658,
		obj_t BgL_objz00_6659, obj_t BgL_markzd2argzd2_6660)
	{
		{	/* Unsafe/intext.scm 1516 */
			{	/* Unsafe/intext.scm 1516 */
				BgL_objectz00_bglt BgL_auxz00_11243;

				{	/* Unsafe/intext.scm 1516 */
					bool_t BgL_test3087z00_11244;

					{	/* Unsafe/intext.scm 1516 */
						obj_t BgL_classz00_7078;

						BgL_classz00_7078 = BGl_objectz00zz__objectz00;
						if (BGL_OBJECTP(BgL_objz00_6659))
							{	/* Unsafe/intext.scm 1516 */
								BgL_objectz00_bglt BgL_arg2488z00_7080;
								long BgL_arg2490z00_7081;

								BgL_arg2488z00_7080 = (BgL_objectz00_bglt) (BgL_objz00_6659);
								BgL_arg2490z00_7081 = BGL_CLASS_DEPTH(BgL_classz00_7078);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Unsafe/intext.scm 1516 */
										long BgL_idxz00_7089;

										BgL_idxz00_7089 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg2488z00_7080);
										{	/* Unsafe/intext.scm 1516 */
											obj_t BgL_arg2479z00_7090;

											{	/* Unsafe/intext.scm 1516 */
												long BgL_arg2480z00_7091;

												BgL_arg2480z00_7091 =
													(BgL_idxz00_7089 + BgL_arg2490z00_7081);
												BgL_arg2479z00_7090 =
													VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													BgL_arg2480z00_7091);
											}
											BgL_test3087z00_11244 =
												(BgL_arg2479z00_7090 == BgL_classz00_7078);
									}}
								else
									{	/* Unsafe/intext.scm 1516 */
										bool_t BgL_res2689z00_7117;

										{	/* Unsafe/intext.scm 1516 */
											obj_t BgL_oclassz00_7099;

											{	/* Unsafe/intext.scm 1516 */
												obj_t BgL_arg2495z00_7107;
												long BgL_arg2497z00_7108;

												BgL_arg2495z00_7107 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Unsafe/intext.scm 1516 */
													long BgL_arg2500z00_7109;

													BgL_arg2500z00_7109 =
														BGL_OBJECT_CLASS_NUM(BgL_arg2488z00_7080);
													BgL_arg2497z00_7108 =
														(BgL_arg2500z00_7109 - OBJECT_TYPE);
												}
												BgL_oclassz00_7099 =
													VECTOR_REF(BgL_arg2495z00_7107, BgL_arg2497z00_7108);
											}
											{	/* Unsafe/intext.scm 1516 */
												bool_t BgL__ortest_1154z00_7100;

												BgL__ortest_1154z00_7100 =
													(BgL_classz00_7078 == BgL_oclassz00_7099);
												if (BgL__ortest_1154z00_7100)
													{	/* Unsafe/intext.scm 1516 */
														BgL_res2689z00_7117 = BgL__ortest_1154z00_7100;
													}
												else
													{	/* Unsafe/intext.scm 1516 */
														long BgL_odepthz00_7101;

														{	/* Unsafe/intext.scm 1516 */
															obj_t BgL_arg2483z00_7102;

															BgL_arg2483z00_7102 = (BgL_oclassz00_7099);
															BgL_odepthz00_7101 =
																BGL_CLASS_DEPTH(BgL_arg2483z00_7102);
														}
														if ((BgL_arg2490z00_7081 < BgL_odepthz00_7101))
															{	/* Unsafe/intext.scm 1516 */
																obj_t BgL_arg2481z00_7104;

																{	/* Unsafe/intext.scm 1516 */
																	obj_t BgL_arg2482z00_7105;

																	BgL_arg2482z00_7105 = (BgL_oclassz00_7099);
																	BgL_arg2481z00_7104 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg2482z00_7105,
																		BgL_arg2490z00_7081);
																}
																BgL_res2689z00_7117 =
																	(BgL_arg2481z00_7104 == BgL_classz00_7078);
															}
														else
															{	/* Unsafe/intext.scm 1516 */
																BgL_res2689z00_7117 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3087z00_11244 = BgL_res2689z00_7117;
									}
							}
						else
							{	/* Unsafe/intext.scm 1516 */
								BgL_test3087z00_11244 = ((bool_t) 0);
							}
					}
					if (BgL_test3087z00_11244)
						{	/* Unsafe/intext.scm 1516 */
							BgL_auxz00_11243 = ((BgL_objectz00_bglt) BgL_objz00_6659);
						}
					else
						{
							obj_t BgL_auxz00_11269;

							BgL_auxz00_11269 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2607z00zz__intextz00,
								BINT(47677L), BGl_string2687z00zz__intextz00,
								BGl_string2618z00zz__intextz00, BgL_objz00_6659);
							FAILURE(BgL_auxz00_11269, BFALSE, BFALSE);
						}
				}
				return
					BGl_objectzd2serializa7erz75zz__intextz00(BgL_auxz00_11243,
					BgL_markzd2argzd2_6660);
			}
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__intextz00(void)
	{
		{	/* Unsafe/intext.scm 20 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			BGl_modulezd2initializa7ationz75zz__hashz00(482391669L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			BGl_modulezd2initializa7ationz75zz__threadz00(149516032L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(34245437L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			BGl_modulezd2initializa7ationz75zz__bexitz00(443005284L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
			return
				BGl_modulezd2initializa7ationz75zz__urlz00(337061926L,
				BSTRING_TO_STRING(BGl_string2688z00zz__intextz00));
		}

	}

#ifdef __cplusplus
}
#endif
