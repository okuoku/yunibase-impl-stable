/*===========================================================================*/
/*   (Unsafe/ftp.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/ftp.scm -indent -o objs/obj_u/Unsafe/ftp.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___FTP_TYPE_DEFINITIONS
#define BGL___FTP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z52ftpz52_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_cmdz00;
		obj_t BgL_dtpz00;
		bool_t BgL_passivezf3zf3;
	}                *BgL_z52ftpz52_bglt;

	typedef struct BgL_ftpz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_cmdz00;
		obj_t BgL_dtpz00;
		bool_t BgL_passivezf3zf3;
		obj_t BgL_hostz00;
		obj_t BgL_portz00;
		obj_t BgL_motdz00;
		obj_t BgL_userz00;
		obj_t BgL_passz00;
		obj_t BgL_acctz00;
	}             *BgL_ftpz00_bglt;

	typedef struct BgL_z62ftpzd2errorzb0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                        *BgL_z62ftpzd2errorzb0_bglt;

	typedef struct BgL_z62ftpzd2parsezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                                *BgL_z62ftpzd2parsezd2errorz62_bglt;


#endif													// BGL___FTP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1927z62zz__ftpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2datazd2typez00zz__ftpz00(BgL_ftpz00_bglt,
		obj_t, obj_t);
	extern obj_t bgl_socket_host_addr(obj_t);
	static obj_t BGl_z62ftpzd2statuszb0zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2allocatezb0zz__ftpz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2641z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z62ftpzd2copyzd2filez62zz__ftpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2646z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z52ftpzd2readzd2dtpz52zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31935ze3ze5zz__ftpz00(obj_t);
	static obj_t BGl_z62lambda1933z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31919ze3ze5zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1934z62zz__ftpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2putzd2filez00zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	static obj_t BGl_symbol2651z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2491z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z62ftpzd2reinitializa7ez17zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_symbol2493z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2656z00zz__ftpz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_ftpz00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_ftpzd2connectzd2toz00zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__ftpz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_ftpzd2directoryzd2ze3listze3zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31952ze3ze5zz__ftpz00(obj_t, obj_t);
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	static obj_t BGl_z62lambda1940z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1941z62zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__ftpz00(obj_t);
	extern obj_t BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_z62ftpzd2cdzd2parentz62zz__ftpz00(obj_t, obj_t);
	static BgL_ftpz00_bglt BGl_z62lambda1948z62zz__ftpz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2datazd2portz00zz__ftpz00(BgL_ftpz00_bglt,
		obj_t, obj_t);
	static obj_t BGl_symbol2660z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2665z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32004ze3ze5zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z52ftpzd2closez80zz__ftpz00(obj_t);
	extern obj_t bgl_display_obj(obj_t, obj_t);
	static BgL_ftpz00_bglt BGl_z62lambda1950z62zz__ftpz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1957z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1958z62zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2storezb0zz__ftpz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62ftpzd2parsezd2errorz62zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z52ftpzd2closezd2cmdz52zz__ftpz00(obj_t);
	static obj_t BGl_z62lambda1964z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1965z62zz__ftpz00(obj_t, obj_t, obj_t);
	extern long bgl_file_size(char *);
	static obj_t BGl_toplevelzd2initzd2zz__ftpz00(void);
	static obj_t BGl_z62ftpzd2mountzb0zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2abortzb0zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1971z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31858ze3ze5zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1972z62zz__ftpz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00(obj_t, obj_t,
		int);
	static obj_t BGl_z62lambda1978z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1979z62zz__ftpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2namezd2listz00zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	static obj_t BGl_z52ftpzd2dtpzd2initz52zz__ftpz00(obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32015ze3ze5zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31980ze3ze5zz__ftpz00(obj_t);
	extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
	static obj_t BGl_z62lambda1985z62zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__ftpz00(void);
	static obj_t BGl_z62lambda1986z62zz__ftpz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2noopzd2zz__ftpz00(BgL_ftpz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31973ze3ze5zz__ftpz00(obj_t);
	static obj_t BGl_objectzd2initzd2zz__ftpz00(void);
	static obj_t BGl_z62lambda1992z62zz__ftpz00(obj_t, obj_t);
	extern obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1993z62zz__ftpz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2pwdzd2zz__ftpz00(BgL_ftpz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2storezd2zz__ftpz00(BgL_ftpz00_bglt, obj_t,
		obj_t);
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62ftpzd2passivezb0zz__ftpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31966ze3ze5zz__ftpz00(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_z62ftpzd2errorzb0zz__ftpz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2mountzd2zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2abortzd2zz__ftpz00(BgL_ftpz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_openzd2inputzd2ftpzd2filezd2zz__ftpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62ftpzd2transferzd2modez62zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31959ze3ze5zz__ftpz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_ftpzd2filezd2structurez00zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	extern obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	extern bool_t BGl_isazf3zf3zz__objectz00(obj_t, obj_t);
	static obj_t BGl_thezd2substringze70z35zz__ftpz00(obj_t, int, int);
	static obj_t BGl_z62ftpzd2makezd2directoryz62zz__ftpz00(obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62ftpzd2rmdirzb0zz__ftpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2renamezd2filez00zz__ftpz00(BgL_ftpz00_bglt,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__ftpz00(void);
	static obj_t BGl_z62ftpzd2renamezd2filez62zz__ftpz00(obj_t, obj_t, obj_t,
		obj_t);
	extern int BGl_socketzd2shutdownzd2zz__socketz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31994ze3ze5zz__ftpz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2copyzd2filez00zz__ftpz00(BgL_ftpz00_bglt,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza31858ze3ze70z60zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	static obj_t BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31987ze3ze5zz__ftpz00(obj_t);
	extern obj_t BGl_urlzd2sanszd2protocolzd2parsezd2zz__urlz00(obj_t, obj_t);
	static obj_t BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(obj_t);
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_list2509z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z62ftpzd2directoryzd2ze3pathzd2listz53zz__ftpz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2cdzd2parentz00zz__ftpz00(BgL_ftpz00_bglt);
	static BgL_z62ftpzd2errorzb0_bglt BGl_z62lambda2000z62zz__ftpz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_z62ftpzd2errorzb0_bglt BGl_z62lambda2002z62zz__ftpz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2logoutzd2zz__ftpz00(BgL_ftpz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2rmdirzd2zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	extern obj_t rgc_buffer_substring(obj_t, long, long);
	static obj_t BGl_z62ftpzd2listzb0zz__ftpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2restartzd2zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	static obj_t BGl_z62ftpzd2putzd2filez62zz__ftpz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_readzd2lineszd2zz__r4_input_6_10_2z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_z62ftpzd2parsezd2errorz62_bglt
		BGl_z62lambda2011z62zz__ftpz00(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_z62ftpzd2parsezd2errorz62_bglt
		BGl_z62lambda2013z62zz__ftpz00(obj_t);
	static obj_t BGl_z62ftpzd2sitezd2parametersz62zz__ftpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2retrievezd2zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
	extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62ftpzd2logoutzb0zz__ftpz00(obj_t, obj_t);
	extern obj_t bstring_to_keyword(obj_t);
	extern obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2allocatezd2zz__ftpz00(BgL_ftpz00_bglt,
		obj_t, obj_t);
	extern obj_t bgl_list_ref(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31711ze3ze5zz__ftpz00(obj_t);
	extern bool_t rgc_buffer_bol_p(obj_t);
	static obj_t BGl_ignoreze70ze7zz__ftpz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__ftpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza31711ze32366ze5zz__ftpz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2cdzd2zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	extern obj_t BGl_makezd2clientzd2socketz00zz__socketz00(obj_t, int, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2helpzb0zz__ftpz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_z62errorz62zz__objectz00;
	extern bool_t fexists(char *);
	static obj_t BGl_z62ftpzd2datazd2typez62zz__ftpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31641ze3ze5zz__ftpz00(obj_t);
	extern obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_cnstzd2initzd2zz__ftpz00(void);
	extern obj_t BGl_stringzd2indexzd2zz__r4_strings_6_7z00(obj_t, obj_t, obj_t);
	static obj_t BGl__ftpzd2connectzd2zz__ftpz00(obj_t, obj_t);
	extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z52ftpzd2dtpzd2pasvzd2setupz80zz__ftpz00(obj_t);
	static obj_t BGl_z62ftpzd2datazd2portz62zz__ftpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__ftpz00(void);
	static obj_t BGl_keyword2510z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_keyword2512z00zz__ftpz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_z52ftpz52zz__ftpz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zz__ftpz00(void);
	static obj_t BGl_z52ftpzd2readzd2cmdz52zz__ftpz00(obj_t);
	static obj_t BGl_z62ftpzd2restartzb0zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2pwdzb0zz__ftpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2deletezd2zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ftpzd2sitezd2parametersz00zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ftpzd2directoryzd2ze3pathzd2listz31zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	extern obj_t socket_close(obj_t);
	extern obj_t BGl_objectz00zz__objectz00;
	BGL_EXPORTED_DECL bool_t
		BGl_ftpzd2makezd2directoryz00zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	extern long BGl_sendzd2filezd2zz__r4_input_6_10_2z00(obj_t, obj_t, long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2systemzd2zz__ftpz00(BgL_ftpz00_bglt);
	extern obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2listzd2zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	static obj_t BGl_ftpportzd2ze3hostportz31zz__ftpz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2connectzd2zz__ftpz00(BgL_ftpz00_bglt,
		obj_t);
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static obj_t BGl_z62ftpzd2namezd2listz62zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2deletezb0zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2directoryzd2ze3listz81zz__ftpz00(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t BGl_readzd2stringzd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_z62ftpzd2systemzb0zz__ftpz00(obj_t, obj_t);
	extern obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2appendzd2zz__ftpz00(BgL_ftpz00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_ftpzd2reinitializa7ez75zz__ftpz00(BgL_ftpz00_bglt);
	static obj_t BGl_symbol2513z00zz__ftpz00 = BUNSPEC;
	extern obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62ftpzd2filezd2structurez62zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62objectzd2printzd2ftp1283z62zz__ftpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_symbol2520z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2602z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2604z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2609z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_loopze70ze7zz__ftpz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_ftpzd2transferzd2modez00zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__ftpz00(obj_t);
	static obj_t BGl_z62ftpzd2appendzb0zz__ftpz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62ftpzd2noopzb0zz__ftpz00(obj_t, obj_t);
	extern bool_t rgc_fill_buffer(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2helpzd2zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	static obj_t BGl_symbol2614z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z62ftpzd2cdzb0zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2616z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl__openzd2inputzd2ftpzd2filezd2zz__ftpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ftpzd2statuszd2zz__ftpz00(BgL_ftpz00_bglt, obj_t);
	static obj_t BGl_symbol2620z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2622z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_z62ftpzd2retrievezb0zz__ftpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol2627z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2628z00zz__ftpz00 = BUNSPEC;
	static BgL_z52ftpz52_bglt BGl_z62lambda1917z62zz__ftpz00(obj_t);
	static obj_t BGl_symbol2632z00zz__ftpz00 = BUNSPEC;
	static obj_t BGl_symbol2636z00zz__ftpz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_ftpzd2passivezd2zz__ftpz00(BgL_ftpz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31942ze3ze5zz__ftpz00(obj_t);
	static obj_t BGl_z62lambda1926z62zz__ftpz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2cdzd2parentzd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2cdza7d2pa2674za7,
		BGl_z62ftpzd2cdzd2parentz62zz__ftpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2allocatezd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2allocat2675z00, va_generic_entry,
		BGl_z62ftpzd2allocatezb0zz__ftpz00, BUNSPEC, -3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2rmdirzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2rmdirza7b2676za7, BGl_z62ftpzd2rmdirzb0zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2retrievezd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2retriev2677z00, BGl_z62ftpzd2retrievezb0zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2abortzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2abortza7b2678za7, BGl_z62ftpzd2abortzb0zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2listzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2listza7b02679za7, va_generic_entry,
		BGl_z62ftpzd2listzb0zz__ftpz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2500z00zz__ftpz00,
		BgL_bgl_string2500za700za7za7_2680za7, "RETR", 4);
	      DEFINE_STRING(BGl_string2501z00zz__ftpz00,
		BgL_bgl_string2501za700za7za7_2681za7, "USER", 4);
	      DEFINE_STRING(BGl_string2502z00zz__ftpz00,
		BgL_bgl_string2502za700za7za7_2682za7, " ABOR ", 6);
	      DEFINE_STRING(BGl_string2503z00zz__ftpz00,
		BgL_bgl_string2503za700za7za7_2683za7, ",", 1);
	      DEFINE_STRING(BGl_string2504z00zz__ftpz00,
		BgL_bgl_string2504za700za7za7_2684za7, "PASS", 4);
	      DEFINE_STRING(BGl_string2505z00zz__ftpz00,
		BgL_bgl_string2505za700za7za7_2685za7, "ACCT", 4);
	      DEFINE_STRING(BGl_string2506z00zz__ftpz00,
		BgL_bgl_string2506za700za7za7_2686za7, "Remote host closed socket", 25);
	      DEFINE_STRING(BGl_string2507z00zz__ftpz00,
		BgL_bgl_string2507za700za7za7_2687za7, "Unknown return code", 19);
	      DEFINE_STRING(BGl_string2508z00zz__ftpz00,
		BgL_bgl_string2508za700za7za7_2688za7, "localhost", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2passivezd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2passive2689z00, BGl_z62ftpzd2passivezb0zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2systemzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2systemza72690za7, BGl_z62ftpzd2systemzb0zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2copyzd2filezd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2copyza7d22691za7,
		BGl_z62ftpzd2copyzd2filez62zz__ftpz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2511z00zz__ftpz00,
		BgL_bgl_string2511za700za7za7_2692za7, "host", 4);
	      DEFINE_STRING(BGl_string2514z00zz__ftpz00,
		BgL_bgl_string2514za700za7za7_2693za7, "ftp-connect-to", 14);
	      DEFINE_STRING(BGl_string2515z00zz__ftpz00,
		BgL_bgl_string2515za700za7za7_2694za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string2516z00zz__ftpz00,
		BgL_bgl_string2516za700za7za7_2695za7,
		"wrong number of arguments: [0..2] expected, provided", 52);
	      DEFINE_STRING(BGl_string2517z00zz__ftpz00,
		BgL_bgl_string2517za700za7za7_2696za7, "", 0);
	      DEFINE_STRING(BGl_string2518z00zz__ftpz00,
		BgL_bgl_string2518za700za7za7_2697za7, "anonymous", 9);
	      DEFINE_STRING(BGl_string2519z00zz__ftpz00,
		BgL_bgl_string2519za700za7za7_2698za7, "foo@bar.net", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2namezd2listzd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2nameza7d22699za7, va_generic_entry,
		BGl_z62ftpzd2namezd2listz62zz__ftpz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2521z00zz__ftpz00,
		BgL_bgl_string2521za700za7za7_2700za7, "inet", 4);
	      DEFINE_STRING(BGl_string2603z00zz__ftpz00,
		BgL_bgl_string2603za700za7za7_2701za7, "cmd", 3);
	      DEFINE_STRING(BGl_string2522z00zz__ftpz00,
		BgL_bgl_string2522za700za7za7_2702za7, "/tmp/bigloo/runtime/Unsafe/ftp.scm",
		34);
	      DEFINE_STRING(BGl_string2523z00zz__ftpz00,
		BgL_bgl_string2523za700za7za7_2703za7, "_ftp-connect", 12);
	      DEFINE_STRING(BGl_string2605z00zz__ftpz00,
		BgL_bgl_string2605za700za7za7_2704za7, "obj", 3);
	      DEFINE_STRING(BGl_string2524z00zz__ftpz00,
		BgL_bgl_string2524za700za7za7_2705za7, "PASV", 4);
	      DEFINE_STRING(BGl_string2525z00zz__ftpz00,
		BgL_bgl_string2525za700za7za7_2706za7, "ftp-data-port", 13);
	      DEFINE_STRING(BGl_string2526z00zz__ftpz00,
		BgL_bgl_string2526za700za7za7_2707za7,
		"PORT functionality is not yet implemented", 41);
	      DEFINE_STRING(BGl_string2527z00zz__ftpz00,
		BgL_bgl_string2527za700za7za7_2708za7, "~a.~a.~a.~a", 11);
	      DEFINE_STRING(BGl_string2528z00zz__ftpz00,
		BgL_bgl_string2528za700za7za7_2709za7, "QUIT", 4);
	      DEFINE_STRING(BGl_string2529z00zz__ftpz00,
		BgL_bgl_string2529za700za7za7_2710za7, "&ftp-logout", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2statuszd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2statusza72711za7, va_generic_entry,
		BGl_z62ftpzd2statuszb0zz__ftpz00, BUNSPEC, -2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2600z00zz__ftpz00,
		BgL_bgl_za762lambda1927za7622712z00, BGl_z62lambda1927z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2601z00zz__ftpz00,
		BgL_bgl_za762lambda1926za7622713z00, BGl_z62lambda1926z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2610z00zz__ftpz00,
		BgL_bgl_string2610za700za7za7_2714za7, "dtp", 3);
	      DEFINE_STRING(BGl_string2530z00zz__ftpz00,
		BgL_bgl_string2530za700za7za7_2715za7, "CWD", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2606z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2716za7,
		BGl_z62zc3z04anonymousza31935ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2531z00zz__ftpz00,
		BgL_bgl_string2531za700za7za7_2717za7, "&ftp-cd", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2607z00zz__ftpz00,
		BgL_bgl_za762lambda1934za7622718z00, BGl_z62lambda1934z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2532z00zz__ftpz00,
		BgL_bgl_string2532za700za7za7_2719za7, "bstring", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2608z00zz__ftpz00,
		BgL_bgl_za762lambda1933za7622720z00, BGl_z62lambda1933z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2533z00zz__ftpz00,
		BgL_bgl_string2533za700za7za7_2721za7, "CDUP", 4);
	      DEFINE_STRING(BGl_string2615z00zz__ftpz00,
		BgL_bgl_string2615za700za7za7_2722za7, "passive?", 8);
	      DEFINE_STRING(BGl_string2534z00zz__ftpz00,
		BgL_bgl_string2534za700za7za7_2723za7, "&ftp-cd-parent", 14);
	      DEFINE_STRING(BGl_string2535z00zz__ftpz00,
		BgL_bgl_string2535za700za7za7_2724za7, "SMNT", 4);
	      DEFINE_STRING(BGl_string2617z00zz__ftpz00,
		BgL_bgl_string2617za700za7za7_2725za7, "bool", 4);
	      DEFINE_STRING(BGl_string2536z00zz__ftpz00,
		BgL_bgl_string2536za700za7za7_2726za7, "&ftp-mount", 10);
	      DEFINE_STRING(BGl_string2537z00zz__ftpz00,
		BgL_bgl_string2537za700za7za7_2727za7, "REIN", 4);
	      DEFINE_STRING(BGl_string2538z00zz__ftpz00,
		BgL_bgl_string2538za700za7za7_2728za7, "&ftp-reinitialize", 17);
	      DEFINE_STRING(BGl_string2539z00zz__ftpz00,
		BgL_bgl_string2539za700za7za7_2729za7,
		"PORT functionality is not implemented", 37);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2cdzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2cdza7b0za7za72730za7, BGl_z62ftpzd2cdzb0zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2611z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2731za7,
		BGl_z62zc3z04anonymousza31942ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2612z00zz__ftpz00,
		BgL_bgl_za762lambda1941za7622732z00, BGl_z62lambda1941z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2613z00zz__ftpz00,
		BgL_bgl_za762lambda1940za7622733z00, BGl_z62lambda1940z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2621z00zz__ftpz00,
		BgL_bgl_string2621za700za7za7_2734za7, "%ftp", 4);
	      DEFINE_STRING(BGl_string2540z00zz__ftpz00,
		BgL_bgl_string2540za700za7za7_2735za7, "&ftp-data-port", 14);
	      DEFINE_STRING(BGl_string2541z00zz__ftpz00,
		BgL_bgl_string2541za700za7za7_2736za7, "bint", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ftpzd2directoryzd2ze3pathzd2listzd2envze3zz__ftpz00,
		BgL_bgl_za762ftpza7d2directo2737z00,
		BGl_z62ftpzd2directoryzd2ze3pathzd2listz53zz__ftpz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2sitezd2parameterszd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2siteza7d22738za7, va_generic_entry,
		BGl_z62ftpzd2sitezd2parametersz62zz__ftpz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2623z00zz__ftpz00,
		BgL_bgl_string2623za700za7za7_2739za7, "__ftp", 5);
	      DEFINE_STRING(BGl_string2542z00zz__ftpz00,
		BgL_bgl_string2542za700za7za7_2740za7, "&ftp-passive", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2618z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2741za7,
		BGl_z62zc3z04anonymousza31919ze3ze5zz__ftpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2543z00zz__ftpz00,
		BgL_bgl_string2543za700za7za7_2742za7, "ftp-data-type", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2619z00zz__ftpz00,
		BgL_bgl_za762lambda1917za7622743z00, BGl_z62lambda1917z62zz__ftpz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2544z00zz__ftpz00,
		BgL_bgl_string2544za700za7za7_2744za7,
		"Illegal type value, should be on of '(ascii image)", 50);
	      DEFINE_STRING(BGl_string2545z00zz__ftpz00,
		BgL_bgl_string2545za700za7za7_2745za7, "A", 1);
	      DEFINE_STRING(BGl_string2546z00zz__ftpz00,
		BgL_bgl_string2546za700za7za7_2746za7, "TYPE", 4);
	      DEFINE_STRING(BGl_string2547z00zz__ftpz00,
		BgL_bgl_string2547za700za7za7_2747za7, "I", 1);
	      DEFINE_STRING(BGl_string2548z00zz__ftpz00,
		BgL_bgl_string2548za700za7za7_2748za7, "&ftp-data-type", 14);
	      DEFINE_STRING(BGl_string2549z00zz__ftpz00,
		BgL_bgl_string2549za700za7za7_2749za7, "symbol", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2624z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2750za7,
		BGl_z62zc3z04anonymousza31959ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2625z00zz__ftpz00,
		BgL_bgl_za762lambda1958za7622751z00, BGl_z62lambda1958z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2550z00zz__ftpz00,
		BgL_bgl_string2550za700za7za7_2752za7, "F", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2626z00zz__ftpz00,
		BgL_bgl_za762lambda1957za7622753z00, BGl_z62lambda1957z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2551z00zz__ftpz00,
		BgL_bgl_string2551za700za7za7_2754za7, "STRU", 4);
	      DEFINE_STRING(BGl_string2552z00zz__ftpz00,
		BgL_bgl_string2552za700za7za7_2755za7, "&ftp-file-structure", 19);
	      DEFINE_STRING(BGl_string2553z00zz__ftpz00,
		BgL_bgl_string2553za700za7za7_2756za7, "S", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2629z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2757za7,
		BGl_z62zc3z04anonymousza31966ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2554z00zz__ftpz00,
		BgL_bgl_string2554za700za7za7_2758za7, "MODE", 4);
	      DEFINE_STRING(BGl_string2555z00zz__ftpz00,
		BgL_bgl_string2555za700za7za7_2759za7, "&ftp-transfer-mode", 18);
	      DEFINE_STRING(BGl_string2637z00zz__ftpz00,
		BgL_bgl_string2637za700za7za7_2760za7, "motd", 4);
	      DEFINE_STRING(BGl_string2556z00zz__ftpz00,
		BgL_bgl_string2556za700za7za7_2761za7, "&ftp-retrieve", 13);
	      DEFINE_STRING(BGl_string2557z00zz__ftpz00,
		BgL_bgl_string2557za700za7za7_2762za7, "STOR", 4);
	      DEFINE_STRING(BGl_string2558z00zz__ftpz00,
		BgL_bgl_string2558za700za7za7_2763za7, "STOU", 4);
	      DEFINE_STRING(BGl_string2559z00zz__ftpz00,
		BgL_bgl_string2559za700za7za7_2764za7, "&ftp-store", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2630z00zz__ftpz00,
		BgL_bgl_za762lambda1965za7622765z00, BGl_z62lambda1965z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2631z00zz__ftpz00,
		BgL_bgl_za762lambda1964za7622766z00, BGl_z62lambda1964z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2633z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2767za7,
		BGl_z62zc3z04anonymousza31973ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2634z00zz__ftpz00,
		BgL_bgl_za762lambda1972za7622768z00, BGl_z62lambda1972z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2635z00zz__ftpz00,
		BgL_bgl_za762lambda1971za7622769z00, BGl_z62lambda1971z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2560z00zz__ftpz00,
		BgL_bgl_string2560za700za7za7_2770za7, "APPE", 4);
	      DEFINE_STRING(BGl_string2642z00zz__ftpz00,
		BgL_bgl_string2642za700za7za7_2771za7, "user", 4);
	      DEFINE_STRING(BGl_string2561z00zz__ftpz00,
		BgL_bgl_string2561za700za7za7_2772za7, "&ftp-append", 11);
	      DEFINE_STRING(BGl_string2562z00zz__ftpz00,
		BgL_bgl_string2562za700za7za7_2773za7, "R", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2638z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2774za7,
		BGl_z62zc3z04anonymousza31980ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2563z00zz__ftpz00,
		BgL_bgl_string2563za700za7za7_2775za7, "ALLO", 4);
	extern obj_t BGl_objectzd2printzd2envz00zz__objectz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2639z00zz__ftpz00,
		BgL_bgl_za762lambda1979za7622776z00, BGl_z62lambda1979z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2564z00zz__ftpz00,
		BgL_bgl_string2564za700za7za7_2777za7, "&ftp-allocate", 13);
	      DEFINE_STRING(BGl_string2483z00zz__ftpz00,
		BgL_bgl_string2483za700za7za7_2778za7, "EOF", 3);
	      DEFINE_STRING(BGl_string2565z00zz__ftpz00,
		BgL_bgl_string2565za700za7za7_2779za7, "REST", 4);
	      DEFINE_STRING(BGl_string2484z00zz__ftpz00,
		BgL_bgl_string2484za700za7za7_2780za7, "Illegal range `~a'", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ftpzd2directoryzd2ze3listzd2envz31zz__ftpz00,
		BgL_bgl_za762ftpza7d2directo2781z00,
		BGl_z62ftpzd2directoryzd2ze3listz81zz__ftpz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2647z00zz__ftpz00,
		BgL_bgl_string2647za700za7za7_2782za7, "pass", 4);
	      DEFINE_STRING(BGl_string2566z00zz__ftpz00,
		BgL_bgl_string2566za700za7za7_2783za7, "&ftp-restart", 12);
	      DEFINE_STRING(BGl_string2485z00zz__ftpz00,
		BgL_bgl_string2485za700za7za7_2784za7, "the-substring", 13);
	      DEFINE_STRING(BGl_string2567z00zz__ftpz00,
		BgL_bgl_string2567za700za7za7_2785za7, "RNFR", 4);
	      DEFINE_STRING(BGl_string2486z00zz__ftpz00,
		BgL_bgl_string2486za700za7za7_2786za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string2568z00zz__ftpz00,
		BgL_bgl_string2568za700za7za7_2787za7, "RNTO", 4);
	      DEFINE_STRING(BGl_string2487z00zz__ftpz00,
		BgL_bgl_string2487za700za7za7_2788za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string2569z00zz__ftpz00,
		BgL_bgl_string2569za700za7za7_2789za7, "&ftp-rename-file", 16);
	      DEFINE_STRING(BGl_string2488z00zz__ftpz00,
		BgL_bgl_string2488za700za7za7_2790za7, "%ftp-read-cmd", 13);
	      DEFINE_STRING(BGl_string2489z00zz__ftpz00,
		BgL_bgl_string2489za700za7za7_2791za7, "Unrecognized output format", 26);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2datazd2portzd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2dataza7d22792za7,
		BGl_z62ftpzd2datazd2portz62zz__ftpz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2640z00zz__ftpz00,
		BgL_bgl_za762lambda1978za7622793z00, BGl_z62lambda1978z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2643z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2794za7,
		BGl_z62zc3z04anonymousza31987ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2644z00zz__ftpz00,
		BgL_bgl_za762lambda1986za7622795z00, BGl_z62lambda1986z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2645z00zz__ftpz00,
		BgL_bgl_za762lambda1985za7622796z00, BGl_z62lambda1985z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2570z00zz__ftpz00,
		BgL_bgl_string2570za700za7za7_2797za7, "ABOR", 4);
	      DEFINE_STRING(BGl_string2652z00zz__ftpz00,
		BgL_bgl_string2652za700za7za7_2798za7, "acct", 4);
	      DEFINE_STRING(BGl_string2571z00zz__ftpz00,
		BgL_bgl_string2571za700za7za7_2799za7, "&ftp-abort", 10);
	      DEFINE_STRING(BGl_string2490z00zz__ftpz00,
		BgL_bgl_string2490za700za7za7_2800za7, "\n", 1);
	      DEFINE_STRING(BGl_string2572z00zz__ftpz00,
		BgL_bgl_string2572za700za7za7_2801za7, "DELE", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2648z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2802za7,
		BGl_z62zc3z04anonymousza31994ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2573z00zz__ftpz00,
		BgL_bgl_string2573za700za7za7_2803za7, "&ftp-delete", 11);
	      DEFINE_STRING(BGl_string2492z00zz__ftpz00,
		BgL_bgl_string2492za700za7za7_2804za7, "list", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2649z00zz__ftpz00,
		BgL_bgl_za762lambda1993za7622805z00, BGl_z62lambda1993z62zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2574z00zz__ftpz00,
		BgL_bgl_string2574za700za7za7_2806za7, "RMD", 3);
	      DEFINE_STRING(BGl_string2575z00zz__ftpz00,
		BgL_bgl_string2575za700za7za7_2807za7, "&ftp-rmdir", 10);
	      DEFINE_STRING(BGl_string2494z00zz__ftpz00,
		BgL_bgl_string2494za700za7za7_2808za7, "port", 4);
	      DEFINE_STRING(BGl_string2576z00zz__ftpz00,
		BgL_bgl_string2576za700za7za7_2809za7, "MKD", 3);
	      DEFINE_STRING(BGl_string2495z00zz__ftpz00,
		BgL_bgl_string2495za700za7za7_2810za7, "%ftp-read-dtp", 13);
	      DEFINE_STRING(BGl_string2577z00zz__ftpz00,
		BgL_bgl_string2577za700za7za7_2811za7, "&ftp-make-directory", 19);
	      DEFINE_STRING(BGl_string2496z00zz__ftpz00,
		BgL_bgl_string2496za700za7za7_2812za7, "Dunno what to read", 18);
	      DEFINE_STRING(BGl_string2578z00zz__ftpz00,
		BgL_bgl_string2578za700za7za7_2813za7, "PWD", 3);
	      DEFINE_STRING(BGl_string2497z00zz__ftpz00,
		BgL_bgl_string2497za700za7za7_2814za7, "ftp", 3);
	      DEFINE_STRING(BGl_string2579z00zz__ftpz00,
		BgL_bgl_string2579za700za7za7_2815za7, "&ftp-pwd", 8);
	      DEFINE_STRING(BGl_string2498z00zz__ftpz00,
		BgL_bgl_string2498za700za7za7_2816za7, "Socket not connected", 20);
	      DEFINE_STRING(BGl_string2499z00zz__ftpz00,
		BgL_bgl_string2499za700za7za7_2817za7, "~l\r\n", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2pwdzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2pwdza7b0za72818z00, BGl_z62ftpzd2pwdzb0zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2650z00zz__ftpz00,
		BgL_bgl_za762lambda1992za7622819z00, BGl_z62lambda1992z62zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2653z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2820za7,
		BGl_z62zc3z04anonymousza31952ze3ze5zz__ftpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2654z00zz__ftpz00,
		BgL_bgl_za762lambda1950za7622821z00, BGl_z62lambda1950z62zz__ftpz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2661z00zz__ftpz00,
		BgL_bgl_string2661za700za7za7_2822za7, "&ftp-error", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2655z00zz__ftpz00,
		BgL_bgl_za762lambda1948za7622823z00, BGl_z62lambda1948z62zz__ftpz00, 0L,
		BUNSPEC, 9);
	      DEFINE_STRING(BGl_string2580z00zz__ftpz00,
		BgL_bgl_string2580za700za7za7_2824za7, "LIST", 4);
	      DEFINE_STRING(BGl_string2581z00zz__ftpz00,
		BgL_bgl_string2581za700za7za7_2825za7, "&ftp-list", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2657z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2826za7,
		BGl_z62zc3z04anonymousza32004ze3ze5zz__ftpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2582z00zz__ftpz00,
		BgL_bgl_string2582za700za7za7_2827za7, "NLST", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2658z00zz__ftpz00,
		BgL_bgl_za762lambda2002za7622828z00, BGl_z62lambda2002z62zz__ftpz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2583z00zz__ftpz00,
		BgL_bgl_string2583za700za7za7_2829za7, "&ftp-name-list", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2659z00zz__ftpz00,
		BgL_bgl_za762lambda2000za7622830z00, BGl_z62lambda2000z62zz__ftpz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string2584z00zz__ftpz00,
		BgL_bgl_string2584za700za7za7_2831za7, "SITE", 4);
	      DEFINE_STRING(BGl_string2666z00zz__ftpz00,
		BgL_bgl_string2666za700za7za7_2832za7, "&ftp-parse-error", 16);
	      DEFINE_STRING(BGl_string2585z00zz__ftpz00,
		BgL_bgl_string2585za700za7za7_2833za7, "&ftp-site-parameters", 20);
	      DEFINE_STRING(BGl_string2586z00zz__ftpz00,
		BgL_bgl_string2586za700za7za7_2834za7, "SYST", 4);
	      DEFINE_STRING(BGl_string2668z00zz__ftpz00,
		BgL_bgl_string2668za700za7za7_2835za7, "object-print", 12);
	      DEFINE_STRING(BGl_string2587z00zz__ftpz00,
		BgL_bgl_string2587za700za7za7_2836za7, "&ftp-system", 11);
	      DEFINE_STRING(BGl_string2669z00zz__ftpz00,
		BgL_bgl_string2669za700za7za7_2837za7, "#<%ftp host=", 12);
	      DEFINE_STRING(BGl_string2588z00zz__ftpz00,
		BgL_bgl_string2588za700za7za7_2838za7, "STAT", 4);
	      DEFINE_STRING(BGl_string2589z00zz__ftpz00,
		BgL_bgl_string2589za700za7za7_2839za7, "&ftp-status", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2mountzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2mountza7b2840za7, BGl_z62ftpzd2mountzb0zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2662z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2841za7,
		BGl_z62zc3z04anonymousza32015ze3ze5zz__ftpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2663z00zz__ftpz00,
		BgL_bgl_za762lambda2013za7622842z00, BGl_z62lambda2013z62zz__ftpz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2670z00zz__ftpz00,
		BgL_bgl_string2670za700za7za7_2843za7, " user=", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2664z00zz__ftpz00,
		BgL_bgl_za762lambda2011za7622844z00, BGl_z62lambda2011z62zz__ftpz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string2671z00zz__ftpz00,
		BgL_bgl_string2671za700za7za7_2845za7, " data-transfer-port=", 20);
	      DEFINE_STRING(BGl_string2590z00zz__ftpz00,
		BgL_bgl_string2590za700za7za7_2846za7, "HELP", 4);
	      DEFINE_STRING(BGl_string2672z00zz__ftpz00,
		BgL_bgl_string2672za700za7za7_2847za7, " passive-mode=", 14);
	      DEFINE_STRING(BGl_string2591z00zz__ftpz00,
		BgL_bgl_string2591za700za7za7_2848za7, "&ftp-help", 9);
	      DEFINE_STRING(BGl_string2673z00zz__ftpz00,
		BgL_bgl_string2673za700za7za7_2849za7, ">", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2667z00zz__ftpz00,
		BgL_bgl_za762objectza7d2prin2850z00,
		BGl_z62objectzd2printzd2ftp1283z62zz__ftpz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2592z00zz__ftpz00,
		BgL_bgl_string2592za700za7za7_2851za7, "NOOP", 4);
	      DEFINE_STRING(BGl_string2593z00zz__ftpz00,
		BgL_bgl_string2593za700za7za7_2852za7, "&ftp-noop", 9);
	      DEFINE_STRING(BGl_string2594z00zz__ftpz00,
		BgL_bgl_string2594za700za7za7_2853za7, "_open-input-ftp-file", 20);
	      DEFINE_STRING(BGl_string2595z00zz__ftpz00,
		BgL_bgl_string2595za700za7za7_2854za7, "&ftp-directory->list", 20);
	      DEFINE_STRING(BGl_string2596z00zz__ftpz00,
		BgL_bgl_string2596za700za7za7_2855za7, "&ftp-directory->path-list", 25);
	      DEFINE_STRING(BGl_string2597z00zz__ftpz00,
		BgL_bgl_string2597za700za7za7_2856za7, "&ftp-copy-file", 14);
	      DEFINE_STRING(BGl_string2598z00zz__ftpz00,
		BgL_bgl_string2598za700za7za7_2857za7, "&ftp-put-file", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2599z00zz__ftpz00,
		BgL_bgl_za762za7c3za704anonymo2858za7,
		BGl_z62zc3z04anonymousza31928ze3ze5zz__ftpz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2logoutzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2logoutza72859za7, BGl_z62ftpzd2logoutzb0zz__ftpz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2noopzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2noopza7b02860za7, BGl_z62ftpzd2noopzb0zz__ftpz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2makezd2directoryzd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2makeza7d22861za7,
		BGl_z62ftpzd2makezd2directoryz62zz__ftpz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2transferzd2modezd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2transfe2862z00, va_generic_entry,
		BGl_z62ftpzd2transferzd2modez62zz__ftpz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2ftpzd2filezd2envz00zz__ftpz00,
		BgL_bgl__openza7d2inputza7d22863z00, opt_generic_entry,
		BGl__openzd2inputzd2ftpzd2filezd2zz__ftpz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2putzd2filezd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2putza7d2f2864za7, BGl_z62ftpzd2putzd2filez62zz__ftpz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2appendzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2appendza72865za7, BGl_z62ftpzd2appendzb0zz__ftpz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2storezd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2storeza7b2866za7, BGl_z62ftpzd2storezb0zz__ftpz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2renamezd2filezd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2renameza72867za7,
		BGl_z62ftpzd2renamezd2filez62zz__ftpz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2reinitializa7ezd2envza7zz__ftpz00,
		BgL_bgl_za762ftpza7d2reiniti2868z00,
		BGl_z62ftpzd2reinitializa7ez17zz__ftpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2helpzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2helpza7b02869za7, va_generic_entry,
		BGl_z62ftpzd2helpzb0zz__ftpz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2connectzd2envz00zz__ftpz00,
		BgL_bgl__ftpza7d2connectza7d2870z00, opt_generic_entry,
		BGl__ftpzd2connectzd2zz__ftpz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2filezd2structurezd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2fileza7d22871za7, va_generic_entry,
		BGl_z62ftpzd2filezd2structurez62zz__ftpz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2restartzd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2restart2872z00, BGl_z62ftpzd2restartzb0zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2deletezd2envz00zz__ftpz00,
		BgL_bgl_za762ftpza7d2deleteza72873za7, BGl_z62ftpzd2deletezb0zz__ftpz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ftpzd2datazd2typezd2envzd2zz__ftpz00,
		BgL_bgl_za762ftpza7d2dataza7d22874za7, va_generic_entry,
		BGl_z62ftpzd2datazd2typez62zz__ftpz00, BUNSPEC, -3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol2641z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2646z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2651z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2491z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2493z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2656z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_ftpz00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2660z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2665z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_z62ftpzd2parsezd2errorz62zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_z62ftpzd2errorzb0zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_list2509z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_keyword2510z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_keyword2512z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_z52ftpz52zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2513z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2520z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2602z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2604z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2609z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2614z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2616z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2620z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2622z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2627z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2628z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2632z00zz__ftpz00));
		     ADD_ROOT((void *) (&BGl_symbol2636z00zz__ftpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__ftpz00(long
		BgL_checksumz00_4537, char *BgL_fromz00_4538)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__ftpz00))
				{
					BGl_requirezd2initializa7ationz75zz__ftpz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__ftpz00();
					BGl_cnstzd2initzd2zz__ftpz00();
					BGl_importedzd2moduleszd2initz00zz__ftpz00();
					BGl_objectzd2initzd2zz__ftpz00();
					BGl_methodzd2initzd2zz__ftpz00();
					return BGl_toplevelzd2initzd2zz__ftpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			BGl_symbol2491z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2492z00zz__ftpz00);
			BGl_symbol2493z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2494z00zz__ftpz00);
			BGl_keyword2510z00zz__ftpz00 =
				bstring_to_keyword(BGl_string2511z00zz__ftpz00);
			BGl_keyword2512z00zz__ftpz00 =
				bstring_to_keyword(BGl_string2494z00zz__ftpz00);
			BGl_list2509z00zz__ftpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2510z00zz__ftpz00,
				MAKE_YOUNG_PAIR(BGl_keyword2512z00zz__ftpz00, BNIL));
			BGl_symbol2513z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2514z00zz__ftpz00);
			BGl_symbol2520z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2521z00zz__ftpz00);
			BGl_symbol2602z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2603z00zz__ftpz00);
			BGl_symbol2604z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2605z00zz__ftpz00);
			BGl_symbol2609z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2610z00zz__ftpz00);
			BGl_symbol2614z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2615z00zz__ftpz00);
			BGl_symbol2616z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2617z00zz__ftpz00);
			BGl_symbol2620z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2621z00zz__ftpz00);
			BGl_symbol2622z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2623z00zz__ftpz00);
			BGl_symbol2627z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2511z00zz__ftpz00);
			BGl_symbol2628z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2532z00zz__ftpz00);
			BGl_symbol2632z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2541z00zz__ftpz00);
			BGl_symbol2636z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2637z00zz__ftpz00);
			BGl_symbol2641z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2642z00zz__ftpz00);
			BGl_symbol2646z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2647z00zz__ftpz00);
			BGl_symbol2651z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2652z00zz__ftpz00);
			BGl_symbol2656z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2497z00zz__ftpz00);
			BGl_symbol2660z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2661z00zz__ftpz00);
			return (BGl_symbol2665z00zz__ftpz00 =
				bstring_to_symbol(BGl_string2666z00zz__ftpz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			return BUNSPEC;
		}

	}



/* %ftp-read-cmd */
	obj_t BGl_z52ftpzd2readzd2cmdz52zz__ftpz00(obj_t BgL_ftpz00_8)
	{
		{	/* Unsafe/ftp.scm 137 */
			{	/* Unsafe/ftp.scm 139 */
				obj_t BgL_ipz00_1364;

				{	/* Unsafe/ftp.scm 139 */
					obj_t BgL_arg1645z00_1806;

					BgL_arg1645z00_1806 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_8)))->BgL_cmdz00);
					{	/* Unsafe/ftp.scm 139 */
						obj_t BgL_tmpz00_4576;

						BgL_tmpz00_4576 = ((obj_t) BgL_arg1645z00_1806);
						BgL_ipz00_1364 = SOCKET_INPUT(BgL_tmpz00_4576);
					}
				}
				{	/* Unsafe/ftp.scm 139 */
					obj_t BgL_msgz00_1365;

					BgL_msgz00_1365 =
						BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_1364);
					{	/* Unsafe/ftp.scm 140 */

						if (EOF_OBJECTP(BgL_msgz00_1365))
							{	/* Unsafe/ftp.scm 141 */
								{	/* Unsafe/ftp.scm 142 */
									int BgL_tmpz00_4582;

									BgL_tmpz00_4582 = (int) (3L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4582);
								}
								{	/* Unsafe/ftp.scm 142 */
									int BgL_tmpz00_4585;

									BgL_tmpz00_4585 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4585,
										BGl_string2483z00zz__ftpz00);
								}
								{	/* Unsafe/ftp.scm 142 */
									int BgL_tmpz00_4588;

									BgL_tmpz00_4588 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4588, BFALSE);
								}
								return BINT(999L);
							}
						else
							{	/* Unsafe/ftp.scm 143 */
								struct bgl_cell BgL_box2877_4166z00;
								obj_t BgL_codez00_1370;
								obj_t BgL_mesgz00_4166;
								bool_t BgL_morezf3zf3_1372;

								{	/* Unsafe/ftp.scm 143 */
									obj_t BgL_arg1642z00_1799;

									BgL_arg1642z00_1799 =
										c_substring(((obj_t) BgL_msgz00_1365), 0L, 3L);
									{	/* Ieee/number.scm 175 */

										BgL_codez00_1370 =
											BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
											(BgL_arg1642z00_1799, BINT(10L));
								}}
								{	/* Unsafe/ftp.scm 144 */
									obj_t BgL_cellvalz00_4596;

									{	/* Ieee/string.scm 194 */
										long BgL_endz00_1804;

										BgL_endz00_1804 = STRING_LENGTH(((obj_t) BgL_msgz00_1365));
										{	/* Ieee/string.scm 194 */

											BgL_cellvalz00_4596 =
												BGl_substringz00zz__r4_strings_6_7z00(BgL_msgz00_1365,
												4L, BgL_endz00_1804);
									}}
									BgL_mesgz00_4166 =
										MAKE_CELL_STACK(BgL_cellvalz00_4596, BgL_box2877_4166z00);
								}
								BgL_morezf3zf3_1372 =
									(((unsigned char) '-') ==
									STRING_REF(((obj_t) BgL_msgz00_1365), 3L));
								if (BgL_morezf3zf3_1372)
									{	/* Unsafe/ftp.scm 146 */
										BGl_loopze70ze7zz__ftpz00(BgL_codez00_1370, BgL_ipz00_1364,
											BgL_mesgz00_4166,
											BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_1364));
									}
								else
									{	/* Unsafe/ftp.scm 146 */
										BFALSE;
									}
								{	/* Unsafe/ftp.scm 170 */
									obj_t BgL_val1_1230z00_1798;

									BgL_val1_1230z00_1798 =
										((obj_t) ((obj_t) CELL_REF(BgL_mesgz00_4166)));
									{	/* Unsafe/ftp.scm 170 */
										int BgL_tmpz00_4607;

										BgL_tmpz00_4607 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4607);
									}
									{	/* Unsafe/ftp.scm 170 */
										int BgL_tmpz00_4610;

										BgL_tmpz00_4610 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_4610, BgL_val1_1230z00_1798);
									}
									return BgL_codez00_1370;
								}
							}
					}
				}
			}
		}

	}



/* the-substring~0 */
	obj_t BGl_thezd2substringze70z35zz__ftpz00(obj_t BgL_iportz00_4152,
		int BgL_minz00_1615, int BgL_maxz00_1616)
	{
		{	/* Unsafe/ftp.scm 149 */
			if (((long) (BgL_maxz00_1616) < (long) (BgL_minz00_1615)))
				{	/* Unsafe/ftp.scm 149 */
					long BgL_arg1524z00_1619;

					BgL_arg1524z00_1619 = RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4152);
					{	/* Unsafe/ftp.scm 149 */
						long BgL_za72za7_3111;

						BgL_za72za7_3111 = (long) (BgL_maxz00_1616);
						BgL_maxz00_1616 = (int) ((BgL_arg1524z00_1619 + BgL_za72za7_3111));
				}}
			else
				{	/* Unsafe/ftp.scm 149 */
					BFALSE;
				}
			{	/* Unsafe/ftp.scm 149 */
				bool_t BgL_test2880z00_4621;

				if (((long) (BgL_minz00_1615) >= 0L))
					{	/* Unsafe/ftp.scm 149 */
						if (((long) (BgL_maxz00_1616) >= (long) (BgL_minz00_1615)))
							{	/* Unsafe/ftp.scm 149 */
								long BgL_arg1535z00_1630;

								BgL_arg1535z00_1630 =
									RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4152);
								BgL_test2880z00_4621 =
									((long) (BgL_maxz00_1616) <= BgL_arg1535z00_1630);
							}
						else
							{	/* Unsafe/ftp.scm 149 */
								BgL_test2880z00_4621 = ((bool_t) 0);
							}
					}
				else
					{	/* Unsafe/ftp.scm 149 */
						BgL_test2880z00_4621 = ((bool_t) 0);
					}
				if (BgL_test2880z00_4621)
					{	/* Unsafe/ftp.scm 149 */
						return
							rgc_buffer_substring(BgL_iportz00_4152,
							(long) (BgL_minz00_1615), (long) (BgL_maxz00_1616));
					}
				else
					{	/* Unsafe/ftp.scm 149 */
						obj_t BgL_arg1529z00_1624;
						obj_t BgL_arg1530z00_1625;

						{	/* Unsafe/ftp.scm 149 */
							obj_t BgL_arg1531z00_1626;

							{	/* Unsafe/ftp.scm 149 */
								long BgL_arg1521z00_3121;

								BgL_arg1521z00_3121 =
									RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4152);
								BgL_arg1531z00_1626 =
									rgc_buffer_substring(BgL_iportz00_4152, 0L,
									BgL_arg1521z00_3121);
							}
							{	/* Unsafe/ftp.scm 149 */
								obj_t BgL_list1532z00_1627;

								BgL_list1532z00_1627 =
									MAKE_YOUNG_PAIR(BgL_arg1531z00_1626, BNIL);
								BgL_arg1529z00_1624 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2484z00zz__ftpz00, BgL_list1532z00_1627);
						}}
						BgL_arg1530z00_1625 =
							MAKE_YOUNG_PAIR(BINT(BgL_minz00_1615), BINT(BgL_maxz00_1616));
						return
							BGl_errorz00zz__errorz00(BGl_string2485z00zz__ftpz00,
							BgL_arg1529z00_1624, BgL_arg1530z00_1625);
					}
			}
		}

	}



/* ignore~0 */
	obj_t BGl_ignoreze70ze7zz__ftpz00(obj_t BgL_codez00_4158,
		obj_t BgL_ipz00_4157, obj_t BgL_mesgz00_4156, obj_t BgL_msgz00_4155,
		obj_t BgL_port1076z00_4154, obj_t BgL_iportz00_4153)
	{
		{	/* Unsafe/ftp.scm 149 */
			{
				obj_t BgL_iportz00_1428;
				long BgL_lastzd2matchzd2_1429;
				long BgL_forwardz00_1430;
				long BgL_bufposz00_1431;
				obj_t BgL_iportz00_1450;
				long BgL_lastzd2matchzd2_1451;
				long BgL_forwardz00_1452;
				long BgL_bufposz00_1453;
				obj_t BgL_iportz00_1465;
				long BgL_lastzd2matchzd2_1466;
				long BgL_forwardz00_1467;
				long BgL_bufposz00_1468;
				obj_t BgL_iportz00_1487;
				long BgL_lastzd2matchzd2_1488;
				long BgL_forwardz00_1489;
				long BgL_bufposz00_1490;
				obj_t BgL_iportz00_1509;
				long BgL_lastzd2matchzd2_1510;
				long BgL_forwardz00_1511;
				long BgL_bufposz00_1512;
				obj_t BgL_iportz00_1524;
				long BgL_lastzd2matchzd2_1525;
				long BgL_forwardz00_1526;
				long BgL_bufposz00_1527;
				obj_t BgL_iportz00_1545;
				long BgL_lastzd2matchzd2_1546;
				long BgL_forwardz00_1547;
				long BgL_bufposz00_1548;
				obj_t BgL_iportz00_1559;
				long BgL_lastzd2matchzd2_1560;
				long BgL_forwardz00_1561;
				long BgL_bufposz00_1562;
				obj_t BgL_iportz00_1573;
				long BgL_lastzd2matchzd2_1574;
				long BgL_forwardz00_1575;
				long BgL_bufposz00_1576;
				obj_t BgL_iportz00_1593;
				long BgL_lastzd2matchzd2_1594;
				long BgL_forwardz00_1595;
				long BgL_bufposz00_1596;

				RGC_START_MATCH(BgL_iportz00_4153);
				{	/* Unsafe/ftp.scm 149 */
					long BgL_matchz00_1722;

					{	/* Unsafe/ftp.scm 149 */
						long BgL_arg1639z00_1751;
						long BgL_arg1640z00_1752;

						BgL_arg1639z00_1751 = RGC_BUFFER_FORWARD(BgL_iportz00_4153);
						BgL_arg1640z00_1752 = RGC_BUFFER_BUFPOS(BgL_iportz00_4153);
						BgL_iportz00_1524 = BgL_port1076z00_4154;
						BgL_lastzd2matchzd2_1525 = 4L;
						BgL_forwardz00_1526 = BgL_arg1639z00_1751;
						BgL_bufposz00_1527 = BgL_arg1640z00_1752;
					BgL_zc3z04anonymousza31464ze3z87_1528:
						{	/* Unsafe/ftp.scm 149 */
							long BgL_newzd2matchzd2_1529;

							if (rgc_buffer_bol_p(BgL_iportz00_1524))
								{	/* Unsafe/ftp.scm 149 */
									RGC_STOP_MATCH(BgL_iportz00_1524, BgL_forwardz00_1526);
									BgL_newzd2matchzd2_1529 = 3L;
								}
							else
								{	/* Unsafe/ftp.scm 149 */
									BgL_newzd2matchzd2_1529 = BgL_lastzd2matchzd2_1525;
								}
							if ((BgL_forwardz00_1526 == BgL_bufposz00_1527))
								{	/* Unsafe/ftp.scm 149 */
									if (rgc_fill_buffer(BgL_iportz00_1524))
										{	/* Unsafe/ftp.scm 149 */
											long BgL_arg1467z00_1532;
											long BgL_arg1468z00_1533;

											BgL_arg1467z00_1532 =
												RGC_BUFFER_FORWARD(BgL_iportz00_1524);
											BgL_arg1468z00_1533 =
												RGC_BUFFER_BUFPOS(BgL_iportz00_1524);
											{
												long BgL_bufposz00_4656;
												long BgL_forwardz00_4655;

												BgL_forwardz00_4655 = BgL_arg1467z00_1532;
												BgL_bufposz00_4656 = BgL_arg1468z00_1533;
												BgL_bufposz00_1527 = BgL_bufposz00_4656;
												BgL_forwardz00_1526 = BgL_forwardz00_4655;
												goto BgL_zc3z04anonymousza31464ze3z87_1528;
											}
										}
									else
										{	/* Unsafe/ftp.scm 149 */
											BgL_matchz00_1722 = BgL_newzd2matchzd2_1529;
										}
								}
							else
								{	/* Unsafe/ftp.scm 149 */
									int BgL_curz00_1534;

									BgL_curz00_1534 =
										RGC_BUFFER_GET_CHAR(BgL_iportz00_1524, BgL_forwardz00_1526);
									{	/* Unsafe/ftp.scm 149 */

										{	/* Unsafe/ftp.scm 149 */
											bool_t BgL_test2886z00_4658;

											if (((long) (BgL_curz00_1534) >= 48L))
												{	/* Unsafe/ftp.scm 149 */
													BgL_test2886z00_4658 =
														((long) (BgL_curz00_1534) < 58L);
												}
											else
												{	/* Unsafe/ftp.scm 149 */
													BgL_test2886z00_4658 = ((bool_t) 0);
												}
											if (BgL_test2886z00_4658)
												{	/* Unsafe/ftp.scm 149 */
													BgL_iportz00_1465 = BgL_iportz00_1524;
													BgL_lastzd2matchzd2_1466 = BgL_newzd2matchzd2_1529;
													BgL_forwardz00_1467 = (1L + BgL_forwardz00_1526);
													BgL_bufposz00_1468 = BgL_bufposz00_1527;
												BgL_zc3z04anonymousza31430ze3z87_1469:
													{	/* Unsafe/ftp.scm 149 */
														long BgL_newzd2matchzd2_1470;

														if (rgc_buffer_bol_p(BgL_iportz00_1465))
															{	/* Unsafe/ftp.scm 149 */
																RGC_STOP_MATCH(BgL_iportz00_1465,
																	BgL_forwardz00_1467);
																BgL_newzd2matchzd2_1470 = 3L;
															}
														else
															{	/* Unsafe/ftp.scm 149 */
																RGC_STOP_MATCH(BgL_iportz00_1465,
																	BgL_forwardz00_1467);
																BgL_newzd2matchzd2_1470 = 4L;
															}
														if ((BgL_forwardz00_1467 == BgL_bufposz00_1468))
															{	/* Unsafe/ftp.scm 149 */
																if (rgc_fill_buffer(BgL_iportz00_1465))
																	{	/* Unsafe/ftp.scm 149 */
																		long BgL_arg1434z00_1473;
																		long BgL_arg1435z00_1474;

																		BgL_arg1434z00_1473 =
																			RGC_BUFFER_FORWARD(BgL_iportz00_1465);
																		BgL_arg1435z00_1474 =
																			RGC_BUFFER_BUFPOS(BgL_iportz00_1465);
																		{
																			long BgL_bufposz00_4675;
																			long BgL_forwardz00_4674;

																			BgL_forwardz00_4674 = BgL_arg1434z00_1473;
																			BgL_bufposz00_4675 = BgL_arg1435z00_1474;
																			BgL_bufposz00_1468 = BgL_bufposz00_4675;
																			BgL_forwardz00_1467 = BgL_forwardz00_4674;
																			goto
																				BgL_zc3z04anonymousza31430ze3z87_1469;
																		}
																	}
																else
																	{	/* Unsafe/ftp.scm 149 */
																		BgL_matchz00_1722 = BgL_newzd2matchzd2_1470;
																	}
															}
														else
															{	/* Unsafe/ftp.scm 149 */
																int BgL_curz00_1475;

																BgL_curz00_1475 =
																	RGC_BUFFER_GET_CHAR(BgL_iportz00_1465,
																	BgL_forwardz00_1467);
																{	/* Unsafe/ftp.scm 149 */

																	{	/* Unsafe/ftp.scm 149 */
																		bool_t BgL_test2891z00_4677;

																		if (((long) (BgL_curz00_1475) >= 48L))
																			{	/* Unsafe/ftp.scm 149 */
																				BgL_test2891z00_4677 =
																					((long) (BgL_curz00_1475) < 58L);
																			}
																		else
																			{	/* Unsafe/ftp.scm 149 */
																				BgL_test2891z00_4677 = ((bool_t) 0);
																			}
																		if (BgL_test2891z00_4677)
																			{	/* Unsafe/ftp.scm 149 */
																				BgL_iportz00_1487 = BgL_iportz00_1465;
																				BgL_lastzd2matchzd2_1488 =
																					BgL_newzd2matchzd2_1470;
																				BgL_forwardz00_1489 =
																					(1L + BgL_forwardz00_1467);
																				BgL_bufposz00_1490 = BgL_bufposz00_1468;
																			BgL_zc3z04anonymousza31443ze3z87_1491:
																				{	/* Unsafe/ftp.scm 149 */
																					long BgL_newzd2matchzd2_1492;

																					if (rgc_buffer_bol_p
																						(BgL_iportz00_1487))
																						{	/* Unsafe/ftp.scm 149 */
																							RGC_STOP_MATCH(BgL_iportz00_1487,
																								BgL_forwardz00_1489);
																							BgL_newzd2matchzd2_1492 = 3L;
																						}
																					else
																						{	/* Unsafe/ftp.scm 149 */
																							BgL_newzd2matchzd2_1492 =
																								BgL_lastzd2matchzd2_1488;
																						}
																					if (
																						(BgL_forwardz00_1489 ==
																							BgL_bufposz00_1490))
																						{	/* Unsafe/ftp.scm 149 */
																							if (rgc_fill_buffer
																								(BgL_iportz00_1487))
																								{	/* Unsafe/ftp.scm 149 */
																									long BgL_arg1446z00_1495;
																									long BgL_arg1447z00_1496;

																									BgL_arg1446z00_1495 =
																										RGC_BUFFER_FORWARD
																										(BgL_iportz00_1487);
																									BgL_arg1447z00_1496 =
																										RGC_BUFFER_BUFPOS
																										(BgL_iportz00_1487);
																									{
																										long BgL_bufposz00_4693;
																										long BgL_forwardz00_4692;

																										BgL_forwardz00_4692 =
																											BgL_arg1446z00_1495;
																										BgL_bufposz00_4693 =
																											BgL_arg1447z00_1496;
																										BgL_bufposz00_1490 =
																											BgL_bufposz00_4693;
																										BgL_forwardz00_1489 =
																											BgL_forwardz00_4692;
																										goto
																											BgL_zc3z04anonymousza31443ze3z87_1491;
																									}
																								}
																							else
																								{	/* Unsafe/ftp.scm 149 */
																									BgL_matchz00_1722 =
																										BgL_newzd2matchzd2_1492;
																								}
																						}
																					else
																						{	/* Unsafe/ftp.scm 149 */
																							int BgL_curz00_1497;

																							BgL_curz00_1497 =
																								RGC_BUFFER_GET_CHAR
																								(BgL_iportz00_1487,
																								BgL_forwardz00_1489);
																							{	/* Unsafe/ftp.scm 149 */

																								{	/* Unsafe/ftp.scm 149 */
																									bool_t BgL_test2896z00_4695;

																									if (
																										((long) (BgL_curz00_1497) >=
																											48L))
																										{	/* Unsafe/ftp.scm 149 */
																											BgL_test2896z00_4695 =
																												(
																												(long) (BgL_curz00_1497)
																												< 58L);
																										}
																									else
																										{	/* Unsafe/ftp.scm 149 */
																											BgL_test2896z00_4695 =
																												((bool_t) 0);
																										}
																									if (BgL_test2896z00_4695)
																										{	/* Unsafe/ftp.scm 149 */
																											BgL_iportz00_1428 =
																												BgL_iportz00_1487;
																											BgL_lastzd2matchzd2_1429 =
																												BgL_newzd2matchzd2_1492;
																											BgL_forwardz00_1430 =
																												(1L +
																												BgL_forwardz00_1489);
																											BgL_bufposz00_1431 =
																												BgL_bufposz00_1490;
																										BgL_zc3z04anonymousza31407ze3z87_1432:
																											{	/* Unsafe/ftp.scm 149 */
																												long
																													BgL_newzd2matchzd2_1433;
																												if (rgc_buffer_bol_p
																													(BgL_iportz00_1428))
																													{	/* Unsafe/ftp.scm 149 */
																														RGC_STOP_MATCH
																															(BgL_iportz00_1428,
																															BgL_forwardz00_1430);
																														BgL_newzd2matchzd2_1433
																															= 3L;
																													}
																												else
																													{	/* Unsafe/ftp.scm 149 */
																														BgL_newzd2matchzd2_1433
																															=
																															BgL_lastzd2matchzd2_1429;
																													}
																												if (
																													(BgL_forwardz00_1430
																														==
																														BgL_bufposz00_1431))
																													{	/* Unsafe/ftp.scm 149 */
																														if (rgc_fill_buffer
																															(BgL_iportz00_1428))
																															{	/* Unsafe/ftp.scm 149 */
																																long
																																	BgL_arg1410z00_1436;
																																long
																																	BgL_arg1411z00_1437;
																																BgL_arg1410z00_1436
																																	=
																																	RGC_BUFFER_FORWARD
																																	(BgL_iportz00_1428);
																																BgL_arg1411z00_1437
																																	=
																																	RGC_BUFFER_BUFPOS
																																	(BgL_iportz00_1428);
																																{
																																	long
																																		BgL_bufposz00_4711;
																																	long
																																		BgL_forwardz00_4710;
																																	BgL_forwardz00_4710
																																		=
																																		BgL_arg1410z00_1436;
																																	BgL_bufposz00_4711
																																		=
																																		BgL_arg1411z00_1437;
																																	BgL_bufposz00_1431
																																		=
																																		BgL_bufposz00_4711;
																																	BgL_forwardz00_1430
																																		=
																																		BgL_forwardz00_4710;
																																	goto
																																		BgL_zc3z04anonymousza31407ze3z87_1432;
																																}
																															}
																														else
																															{	/* Unsafe/ftp.scm 149 */
																																BgL_matchz00_1722
																																	=
																																	BgL_newzd2matchzd2_1433;
																															}
																													}
																												else
																													{	/* Unsafe/ftp.scm 149 */
																														int BgL_curz00_1438;

																														BgL_curz00_1438 =
																															RGC_BUFFER_GET_CHAR
																															(BgL_iportz00_1428,
																															BgL_forwardz00_1430);
																														{	/* Unsafe/ftp.scm 149 */

																															if (
																																((long)
																																	(BgL_curz00_1438)
																																	== 45L))
																																{	/* Unsafe/ftp.scm 149 */
																																	BgL_iportz00_1450
																																		=
																																		BgL_iportz00_1428;
																																	BgL_lastzd2matchzd2_1451
																																		=
																																		BgL_newzd2matchzd2_1433;
																																	BgL_forwardz00_1452
																																		=
																																		(1L +
																																		BgL_forwardz00_1430);
																																	BgL_bufposz00_1453
																																		=
																																		BgL_bufposz00_1431;
																																BgL_zc3z04anonymousza31421ze3z87_1454:
																																	{	/* Unsafe/ftp.scm 149 */
																																		long
																																			BgL_newzd2matchzd2_1455;
																																		if (rgc_buffer_bol_p(BgL_iportz00_1450))
																																			{	/* Unsafe/ftp.scm 149 */
																																				RGC_STOP_MATCH
																																					(BgL_iportz00_1450,
																																					BgL_forwardz00_1452);
																																				BgL_newzd2matchzd2_1455
																																					= 0L;
																																			}
																																		else
																																			{	/* Unsafe/ftp.scm 149 */
																																				if (rgc_buffer_bol_p(BgL_iportz00_1450))
																																					{	/* Unsafe/ftp.scm 149 */
																																						RGC_STOP_MATCH
																																							(BgL_iportz00_1450,
																																							BgL_forwardz00_1452);
																																						BgL_newzd2matchzd2_1455
																																							=
																																							3L;
																																					}
																																				else
																																					{	/* Unsafe/ftp.scm 149 */
																																						BgL_newzd2matchzd2_1455
																																							=
																																							BgL_lastzd2matchzd2_1451;
																																					}
																																			}
																																		if (
																																			(BgL_forwardz00_1452
																																				==
																																				BgL_bufposz00_1453))
																																			{	/* Unsafe/ftp.scm 149 */
																																				if (rgc_fill_buffer(BgL_iportz00_1450))
																																					{	/* Unsafe/ftp.scm 149 */
																																						long
																																							BgL_arg1424z00_1458;
																																						long
																																							BgL_arg1425z00_1459;
																																						BgL_arg1424z00_1458
																																							=
																																							RGC_BUFFER_FORWARD
																																							(BgL_iportz00_1450);
																																						BgL_arg1425z00_1459
																																							=
																																							RGC_BUFFER_BUFPOS
																																							(BgL_iportz00_1450);
																																						{
																																							long
																																								BgL_bufposz00_4729;
																																							long
																																								BgL_forwardz00_4728;
																																							BgL_forwardz00_4728
																																								=
																																								BgL_arg1424z00_1458;
																																							BgL_bufposz00_4729
																																								=
																																								BgL_arg1425z00_1459;
																																							BgL_bufposz00_1453
																																								=
																																								BgL_bufposz00_4729;
																																							BgL_forwardz00_1452
																																								=
																																								BgL_forwardz00_4728;
																																							goto
																																								BgL_zc3z04anonymousza31421ze3z87_1454;
																																						}
																																					}
																																				else
																																					{	/* Unsafe/ftp.scm 149 */
																																						BgL_matchz00_1722
																																							=
																																							BgL_newzd2matchzd2_1455;
																																					}
																																			}
																																		else
																																			{	/* Unsafe/ftp.scm 149 */
																																				int
																																					BgL_curz00_1460;
																																				BgL_curz00_1460
																																					=
																																					RGC_BUFFER_GET_CHAR
																																					(BgL_iportz00_1450,
																																					BgL_forwardz00_1452);
																																				{	/* Unsafe/ftp.scm 149 */

																																					if (
																																						((long) (BgL_curz00_1460) == 10L))
																																						{	/* Unsafe/ftp.scm 149 */
																																							BgL_matchz00_1722
																																								=
																																								BgL_newzd2matchzd2_1455;
																																						}
																																					else
																																						{
																																							long
																																								BgL_forwardz00_4735;
																																							long
																																								BgL_lastzd2matchzd2_4734;
																																							BgL_lastzd2matchzd2_4734
																																								=
																																								BgL_newzd2matchzd2_1455;
																																							BgL_forwardz00_4735
																																								=
																																								(1L
																																								+
																																								BgL_forwardz00_1452);
																																							BgL_forwardz00_1452
																																								=
																																								BgL_forwardz00_4735;
																																							BgL_lastzd2matchzd2_1451
																																								=
																																								BgL_lastzd2matchzd2_4734;
																																							goto
																																								BgL_zc3z04anonymousza31421ze3z87_1454;
																																						}
																																				}
																																			}
																																	}
																																}
																															else
																																{	/* Unsafe/ftp.scm 149 */
																																	if (
																																		((long)
																																			(BgL_curz00_1438)
																																			== 32L))
																																		{	/* Unsafe/ftp.scm 149 */
																																			BgL_iportz00_1509
																																				=
																																				BgL_iportz00_1428;
																																			BgL_lastzd2matchzd2_1510
																																				=
																																				BgL_newzd2matchzd2_1433;
																																			BgL_forwardz00_1511
																																				=
																																				(1L +
																																				BgL_forwardz00_1430);
																																			BgL_bufposz00_1512
																																				=
																																				BgL_bufposz00_1431;
																																		BgL_zc3z04anonymousza31455ze3z87_1513:
																																			{	/* Unsafe/ftp.scm 149 */
																																				long
																																					BgL_newzd2matchzd2_1514;
																																				if (rgc_buffer_bol_p(BgL_iportz00_1509))
																																					{	/* Unsafe/ftp.scm 149 */
																																						RGC_STOP_MATCH
																																							(BgL_iportz00_1509,
																																							BgL_forwardz00_1511);
																																						BgL_newzd2matchzd2_1514
																																							=
																																							1L;
																																					}
																																				else
																																					{	/* Unsafe/ftp.scm 149 */
																																						if (rgc_buffer_bol_p(BgL_iportz00_1509))
																																							{	/* Unsafe/ftp.scm 149 */
																																								RGC_STOP_MATCH
																																									(BgL_iportz00_1509,
																																									BgL_forwardz00_1511);
																																								BgL_newzd2matchzd2_1514
																																									=
																																									3L;
																																							}
																																						else
																																							{	/* Unsafe/ftp.scm 149 */
																																								BgL_newzd2matchzd2_1514
																																									=
																																									BgL_lastzd2matchzd2_1510;
																																							}
																																					}
																																				if (
																																					(BgL_forwardz00_1511
																																						==
																																						BgL_bufposz00_1512))
																																					{	/* Unsafe/ftp.scm 149 */
																																						if (rgc_fill_buffer(BgL_iportz00_1509))
																																							{	/* Unsafe/ftp.scm 149 */
																																								long
																																									BgL_arg1458z00_1517;
																																								long
																																									BgL_arg1459z00_1518;
																																								BgL_arg1458z00_1517
																																									=
																																									RGC_BUFFER_FORWARD
																																									(BgL_iportz00_1509);
																																								BgL_arg1459z00_1518
																																									=
																																									RGC_BUFFER_BUFPOS
																																									(BgL_iportz00_1509);
																																								{
																																									long
																																										BgL_bufposz00_4754;
																																									long
																																										BgL_forwardz00_4753;
																																									BgL_forwardz00_4753
																																										=
																																										BgL_arg1458z00_1517;
																																									BgL_bufposz00_4754
																																										=
																																										BgL_arg1459z00_1518;
																																									BgL_bufposz00_1512
																																										=
																																										BgL_bufposz00_4754;
																																									BgL_forwardz00_1511
																																										=
																																										BgL_forwardz00_4753;
																																									goto
																																										BgL_zc3z04anonymousza31455ze3z87_1513;
																																								}
																																							}
																																						else
																																							{	/* Unsafe/ftp.scm 149 */
																																								BgL_matchz00_1722
																																									=
																																									BgL_newzd2matchzd2_1514;
																																							}
																																					}
																																				else
																																					{	/* Unsafe/ftp.scm 149 */
																																						int
																																							BgL_curz00_1519;
																																						BgL_curz00_1519
																																							=
																																							RGC_BUFFER_GET_CHAR
																																							(BgL_iportz00_1509,
																																							BgL_forwardz00_1511);
																																						{	/* Unsafe/ftp.scm 149 */

																																							if (((long) (BgL_curz00_1519) == 10L))
																																								{	/* Unsafe/ftp.scm 149 */
																																									BgL_matchz00_1722
																																										=
																																										BgL_newzd2matchzd2_1514;
																																								}
																																							else
																																								{
																																									long
																																										BgL_forwardz00_4760;
																																									long
																																										BgL_lastzd2matchzd2_4759;
																																									BgL_lastzd2matchzd2_4759
																																										=
																																										BgL_newzd2matchzd2_1514;
																																									BgL_forwardz00_4760
																																										=
																																										(1L
																																										+
																																										BgL_forwardz00_1511);
																																									BgL_forwardz00_1511
																																										=
																																										BgL_forwardz00_4760;
																																									BgL_lastzd2matchzd2_1510
																																										=
																																										BgL_lastzd2matchzd2_4759;
																																									goto
																																										BgL_zc3z04anonymousza31455ze3z87_1513;
																																								}
																																						}
																																					}
																																			}
																																		}
																																	else
																																		{	/* Unsafe/ftp.scm 149 */
																																			bool_t
																																				BgL_test2913z00_4763;
																																			if ((
																																					(long)
																																					(BgL_curz00_1438)
																																					==
																																					10L))
																																				{	/* Unsafe/ftp.scm 149 */
																																					BgL_test2913z00_4763
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Unsafe/ftp.scm 149 */
																																					if (
																																						((long) (BgL_curz00_1438) == 32L))
																																						{	/* Unsafe/ftp.scm 149 */
																																							BgL_test2913z00_4763
																																								=
																																								(
																																								(bool_t)
																																								1);
																																						}
																																					else
																																						{	/* Unsafe/ftp.scm 149 */
																																							BgL_test2913z00_4763
																																								=
																																								(
																																								(long)
																																								(BgL_curz00_1438)
																																								==
																																								45L);
																																				}}
																																			if (BgL_test2913z00_4763)
																																				{	/* Unsafe/ftp.scm 149 */
																																					BgL_matchz00_1722
																																						=
																																						BgL_newzd2matchzd2_1433;
																																				}
																																			else
																																				{	/* Unsafe/ftp.scm 149 */
																																					BgL_iportz00_1559
																																						=
																																						BgL_iportz00_1428;
																																					BgL_lastzd2matchzd2_1560
																																						=
																																						BgL_newzd2matchzd2_1433;
																																					BgL_forwardz00_1561
																																						=
																																						(1L
																																						+
																																						BgL_forwardz00_1430);
																																					BgL_bufposz00_1562
																																						=
																																						BgL_bufposz00_1431;
																																				BgL_zc3z04anonymousza31487ze3z87_1563:
																																					{	/* Unsafe/ftp.scm 149 */
																																						long
																																							BgL_newzd2matchzd2_1564;
																																						if (rgc_buffer_bol_p(BgL_iportz00_1559))
																																							{	/* Unsafe/ftp.scm 149 */
																																								RGC_STOP_MATCH
																																									(BgL_iportz00_1559,
																																									BgL_forwardz00_1561);
																																								BgL_newzd2matchzd2_1564
																																									=
																																									3L;
																																							}
																																						else
																																							{	/* Unsafe/ftp.scm 149 */
																																								BgL_newzd2matchzd2_1564
																																									=
																																									BgL_lastzd2matchzd2_1560;
																																							}
																																						if (
																																							(BgL_forwardz00_1561
																																								==
																																								BgL_bufposz00_1562))
																																							{	/* Unsafe/ftp.scm 149 */
																																								if (rgc_fill_buffer(BgL_iportz00_1559))
																																									{	/* Unsafe/ftp.scm 149 */
																																										long
																																											BgL_arg1490z00_1567;
																																										long
																																											BgL_arg1492z00_1568;
																																										BgL_arg1490z00_1567
																																											=
																																											RGC_BUFFER_FORWARD
																																											(BgL_iportz00_1559);
																																										BgL_arg1492z00_1568
																																											=
																																											RGC_BUFFER_BUFPOS
																																											(BgL_iportz00_1559);
																																										{
																																											long
																																												BgL_bufposz00_4782;
																																											long
																																												BgL_forwardz00_4781;
																																											BgL_forwardz00_4781
																																												=
																																												BgL_arg1490z00_1567;
																																											BgL_bufposz00_4782
																																												=
																																												BgL_arg1492z00_1568;
																																											BgL_bufposz00_1562
																																												=
																																												BgL_bufposz00_4782;
																																											BgL_forwardz00_1561
																																												=
																																												BgL_forwardz00_4781;
																																											goto
																																												BgL_zc3z04anonymousza31487ze3z87_1563;
																																										}
																																									}
																																								else
																																									{	/* Unsafe/ftp.scm 149 */
																																										BgL_matchz00_1722
																																											=
																																											BgL_newzd2matchzd2_1564;
																																									}
																																							}
																																						else
																																							{	/* Unsafe/ftp.scm 149 */
																																								int
																																									BgL_curz00_1569;
																																								BgL_curz00_1569
																																									=
																																									RGC_BUFFER_GET_CHAR
																																									(BgL_iportz00_1559,
																																									BgL_forwardz00_1561);
																																								{	/* Unsafe/ftp.scm 149 */

																																									if (((long) (BgL_curz00_1569) == 10L))
																																										{	/* Unsafe/ftp.scm 149 */
																																											BgL_matchz00_1722
																																												=
																																												BgL_newzd2matchzd2_1564;
																																										}
																																									else
																																										{
																																											long
																																												BgL_forwardz00_4788;
																																											long
																																												BgL_lastzd2matchzd2_4787;
																																											BgL_lastzd2matchzd2_4787
																																												=
																																												BgL_newzd2matchzd2_1564;
																																											BgL_forwardz00_4788
																																												=
																																												(1L
																																												+
																																												BgL_forwardz00_1561);
																																											BgL_forwardz00_1561
																																												=
																																												BgL_forwardz00_4788;
																																											BgL_lastzd2matchzd2_1560
																																												=
																																												BgL_lastzd2matchzd2_4787;
																																											goto
																																												BgL_zc3z04anonymousza31487ze3z87_1563;
																																										}
																																								}
																																							}
																																					}
																																				}
																																		}
																																}
																														}
																													}
																											}
																										}
																									else
																										{	/* Unsafe/ftp.scm 149 */
																											bool_t
																												BgL_test2920z00_4792;
																											if (((long)
																													(BgL_curz00_1497) ==
																													10L))
																												{	/* Unsafe/ftp.scm 149 */
																													BgL_test2920z00_4792 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Unsafe/ftp.scm 149 */
																													if (
																														((long)
																															(BgL_curz00_1497)
																															>= 48L))
																														{	/* Unsafe/ftp.scm 149 */
																															BgL_test2920z00_4792
																																=
																																((long)
																																(BgL_curz00_1497)
																																< 58L);
																														}
																													else
																														{	/* Unsafe/ftp.scm 149 */
																															BgL_test2920z00_4792
																																= ((bool_t) 0);
																														}
																												}
																											if (BgL_test2920z00_4792)
																												{	/* Unsafe/ftp.scm 149 */
																													BgL_matchz00_1722 =
																														BgL_newzd2matchzd2_1492;
																												}
																											else
																												{
																													long
																														BgL_bufposz00_4805;
																													long
																														BgL_forwardz00_4803;
																													long
																														BgL_lastzd2matchzd2_4802;
																													obj_t
																														BgL_iportz00_4801;
																													BgL_iportz00_4801 =
																														BgL_iportz00_1487;
																													BgL_lastzd2matchzd2_4802
																														=
																														BgL_newzd2matchzd2_1492;
																													BgL_forwardz00_4803 =
																														(1L +
																														BgL_forwardz00_1489);
																													BgL_bufposz00_4805 =
																														BgL_bufposz00_1490;
																													BgL_bufposz00_1562 =
																														BgL_bufposz00_4805;
																													BgL_forwardz00_1561 =
																														BgL_forwardz00_4803;
																													BgL_lastzd2matchzd2_1560
																														=
																														BgL_lastzd2matchzd2_4802;
																													BgL_iportz00_1559 =
																														BgL_iportz00_4801;
																													goto
																														BgL_zc3z04anonymousza31487ze3z87_1563;
																												}
																										}
																								}
																							}
																						}
																				}
																			}
																		else
																			{	/* Unsafe/ftp.scm 149 */
																				bool_t BgL_test2923z00_4807;

																				if (((long) (BgL_curz00_1475) == 10L))
																					{	/* Unsafe/ftp.scm 149 */
																						BgL_test2923z00_4807 = ((bool_t) 1);
																					}
																				else
																					{	/* Unsafe/ftp.scm 149 */
																						if (
																							((long) (BgL_curz00_1475) >= 48L))
																							{	/* Unsafe/ftp.scm 149 */
																								BgL_test2923z00_4807 =
																									(
																									(long) (BgL_curz00_1475) <
																									58L);
																							}
																						else
																							{	/* Unsafe/ftp.scm 149 */
																								BgL_test2923z00_4807 =
																									((bool_t) 0);
																							}
																					}
																				if (BgL_test2923z00_4807)
																					{	/* Unsafe/ftp.scm 149 */
																						BgL_matchz00_1722 =
																							BgL_newzd2matchzd2_1470;
																					}
																				else
																					{
																						long BgL_bufposz00_4820;
																						long BgL_forwardz00_4818;
																						long BgL_lastzd2matchzd2_4817;
																						obj_t BgL_iportz00_4816;

																						BgL_iportz00_4816 =
																							BgL_iportz00_1465;
																						BgL_lastzd2matchzd2_4817 =
																							BgL_newzd2matchzd2_1470;
																						BgL_forwardz00_4818 =
																							(1L + BgL_forwardz00_1467);
																						BgL_bufposz00_4820 =
																							BgL_bufposz00_1468;
																						BgL_bufposz00_1562 =
																							BgL_bufposz00_4820;
																						BgL_forwardz00_1561 =
																							BgL_forwardz00_4818;
																						BgL_lastzd2matchzd2_1560 =
																							BgL_lastzd2matchzd2_4817;
																						BgL_iportz00_1559 =
																							BgL_iportz00_4816;
																						goto
																							BgL_zc3z04anonymousza31487ze3z87_1563;
																					}
																			}
																	}
																}
															}
													}
												}
											else
												{	/* Unsafe/ftp.scm 149 */
													if (((long) (BgL_curz00_1534) == 32L))
														{	/* Unsafe/ftp.scm 149 */
															BgL_iportz00_1573 = BgL_iportz00_1524;
															BgL_lastzd2matchzd2_1574 =
																BgL_newzd2matchzd2_1529;
															BgL_forwardz00_1575 = (1L + BgL_forwardz00_1526);
															BgL_bufposz00_1576 = BgL_bufposz00_1527;
														BgL_zc3z04anonymousza31496ze3z87_1577:
															{	/* Unsafe/ftp.scm 149 */
																long BgL_newzd2matchzd2_1578;

																if (rgc_buffer_bol_p(BgL_iportz00_1573))
																	{	/* Unsafe/ftp.scm 149 */
																		RGC_STOP_MATCH(BgL_iportz00_1573,
																			BgL_forwardz00_1575);
																		BgL_newzd2matchzd2_1578 = 2L;
																	}
																else
																	{	/* Unsafe/ftp.scm 149 */
																		if (rgc_buffer_bol_p(BgL_iportz00_1573))
																			{	/* Unsafe/ftp.scm 149 */
																				RGC_STOP_MATCH(BgL_iportz00_1573,
																					BgL_forwardz00_1575);
																				BgL_newzd2matchzd2_1578 = 3L;
																			}
																		else
																			{	/* Unsafe/ftp.scm 149 */
																				RGC_STOP_MATCH(BgL_iportz00_1573,
																					BgL_forwardz00_1575);
																				BgL_newzd2matchzd2_1578 = 4L;
																			}
																	}
																if ((BgL_forwardz00_1575 == BgL_bufposz00_1576))
																	{	/* Unsafe/ftp.scm 149 */
																		if (rgc_fill_buffer(BgL_iportz00_1573))
																			{	/* Unsafe/ftp.scm 149 */
																				long BgL_arg1499z00_1581;
																				long BgL_arg1500z00_1582;

																				BgL_arg1499z00_1581 =
																					RGC_BUFFER_FORWARD(BgL_iportz00_1573);
																				BgL_arg1500z00_1582 =
																					RGC_BUFFER_BUFPOS(BgL_iportz00_1573);
																				{
																					long BgL_bufposz00_4839;
																					long BgL_forwardz00_4838;

																					BgL_forwardz00_4838 =
																						BgL_arg1499z00_1581;
																					BgL_bufposz00_4839 =
																						BgL_arg1500z00_1582;
																					BgL_bufposz00_1576 =
																						BgL_bufposz00_4839;
																					BgL_forwardz00_1575 =
																						BgL_forwardz00_4838;
																					goto
																						BgL_zc3z04anonymousza31496ze3z87_1577;
																				}
																			}
																		else
																			{	/* Unsafe/ftp.scm 149 */
																				BgL_matchz00_1722 =
																					BgL_newzd2matchzd2_1578;
																			}
																	}
																else
																	{	/* Unsafe/ftp.scm 149 */
																		int BgL_curz00_1583;

																		BgL_curz00_1583 =
																			RGC_BUFFER_GET_CHAR(BgL_iportz00_1573,
																			BgL_forwardz00_1575);
																		{	/* Unsafe/ftp.scm 149 */

																			if (((long) (BgL_curz00_1583) == 10L))
																				{	/* Unsafe/ftp.scm 149 */
																					BgL_matchz00_1722 =
																						BgL_newzd2matchzd2_1578;
																				}
																			else
																				{	/* Unsafe/ftp.scm 149 */
																					BgL_iportz00_1593 = BgL_iportz00_1573;
																					BgL_lastzd2matchzd2_1594 =
																						BgL_newzd2matchzd2_1578;
																					BgL_forwardz00_1595 =
																						(1L + BgL_forwardz00_1575);
																					BgL_bufposz00_1596 =
																						BgL_bufposz00_1576;
																				BgL_zc3z04anonymousza31506ze3z87_1597:
																					{	/* Unsafe/ftp.scm 149 */
																						long BgL_newzd2matchzd2_1598;

																						if (rgc_buffer_bol_p
																							(BgL_iportz00_1593))
																							{	/* Unsafe/ftp.scm 149 */
																								RGC_STOP_MATCH
																									(BgL_iportz00_1593,
																									BgL_forwardz00_1595);
																								BgL_newzd2matchzd2_1598 = 2L;
																							}
																						else
																							{	/* Unsafe/ftp.scm 149 */
																								if (rgc_buffer_bol_p
																									(BgL_iportz00_1593))
																									{	/* Unsafe/ftp.scm 149 */
																										RGC_STOP_MATCH
																											(BgL_iportz00_1593,
																											BgL_forwardz00_1595);
																										BgL_newzd2matchzd2_1598 =
																											3L;
																									}
																								else
																									{	/* Unsafe/ftp.scm 149 */
																										BgL_newzd2matchzd2_1598 =
																											BgL_lastzd2matchzd2_1594;
																									}
																							}
																						if (
																							(BgL_forwardz00_1595 ==
																								BgL_bufposz00_1596))
																							{	/* Unsafe/ftp.scm 149 */
																								if (rgc_fill_buffer
																									(BgL_iportz00_1593))
																									{	/* Unsafe/ftp.scm 149 */
																										long BgL_arg1509z00_1601;
																										long BgL_arg1510z00_1602;

																										BgL_arg1509z00_1601 =
																											RGC_BUFFER_FORWARD
																											(BgL_iportz00_1593);
																										BgL_arg1510z00_1602 =
																											RGC_BUFFER_BUFPOS
																											(BgL_iportz00_1593);
																										{
																											long BgL_bufposz00_4857;
																											long BgL_forwardz00_4856;

																											BgL_forwardz00_4856 =
																												BgL_arg1509z00_1601;
																											BgL_bufposz00_4857 =
																												BgL_arg1510z00_1602;
																											BgL_bufposz00_1596 =
																												BgL_bufposz00_4857;
																											BgL_forwardz00_1595 =
																												BgL_forwardz00_4856;
																											goto
																												BgL_zc3z04anonymousza31506ze3z87_1597;
																										}
																									}
																								else
																									{	/* Unsafe/ftp.scm 149 */
																										BgL_matchz00_1722 =
																											BgL_newzd2matchzd2_1598;
																									}
																							}
																						else
																							{	/* Unsafe/ftp.scm 149 */
																								int BgL_curz00_1603;

																								BgL_curz00_1603 =
																									RGC_BUFFER_GET_CHAR
																									(BgL_iportz00_1593,
																									BgL_forwardz00_1595);
																								{	/* Unsafe/ftp.scm 149 */

																									if (
																										((long) (BgL_curz00_1603) ==
																											10L))
																										{	/* Unsafe/ftp.scm 149 */
																											BgL_matchz00_1722 =
																												BgL_newzd2matchzd2_1598;
																										}
																									else
																										{
																											long BgL_forwardz00_4863;
																											long
																												BgL_lastzd2matchzd2_4862;
																											BgL_lastzd2matchzd2_4862 =
																												BgL_newzd2matchzd2_1598;
																											BgL_forwardz00_4863 =
																												(1L +
																												BgL_forwardz00_1595);
																											BgL_forwardz00_1595 =
																												BgL_forwardz00_4863;
																											BgL_lastzd2matchzd2_1594 =
																												BgL_lastzd2matchzd2_4862;
																											goto
																												BgL_zc3z04anonymousza31506ze3z87_1597;
																										}
																								}
																							}
																					}
																				}
																		}
																	}
															}
														}
													else
														{	/* Unsafe/ftp.scm 149 */
															if (((long) (BgL_curz00_1534) == 10L))
																{	/* Unsafe/ftp.scm 149 */
																	long BgL_arg1476z00_1541;

																	BgL_arg1476z00_1541 =
																		(1L + BgL_forwardz00_1526);
																	{	/* Unsafe/ftp.scm 149 */
																		long BgL_newzd2matchzd2_3041;

																		RGC_STOP_MATCH(BgL_iportz00_1524,
																			BgL_arg1476z00_1541);
																		BgL_newzd2matchzd2_3041 = 4L;
																		BgL_matchz00_1722 = BgL_newzd2matchzd2_3041;
																}}
															else
																{	/* Unsafe/ftp.scm 149 */
																	BgL_iportz00_1545 = BgL_iportz00_1524;
																	BgL_lastzd2matchzd2_1546 =
																		BgL_newzd2matchzd2_1529;
																	BgL_forwardz00_1547 =
																		(1L + BgL_forwardz00_1526);
																	BgL_bufposz00_1548 = BgL_bufposz00_1527;
																BgL_zc3z04anonymousza31479ze3z87_1549:
																	{	/* Unsafe/ftp.scm 149 */
																		long BgL_newzd2matchzd2_1550;

																		if (rgc_buffer_bol_p(BgL_iportz00_1545))
																			{	/* Unsafe/ftp.scm 149 */
																				RGC_STOP_MATCH(BgL_iportz00_1545,
																					BgL_forwardz00_1547);
																				BgL_newzd2matchzd2_1550 = 3L;
																			}
																		else
																			{	/* Unsafe/ftp.scm 149 */
																				RGC_STOP_MATCH(BgL_iportz00_1545,
																					BgL_forwardz00_1547);
																				BgL_newzd2matchzd2_1550 = 4L;
																			}
																		if (
																			(BgL_forwardz00_1547 ==
																				BgL_bufposz00_1548))
																			{	/* Unsafe/ftp.scm 149 */
																				if (rgc_fill_buffer(BgL_iportz00_1545))
																					{	/* Unsafe/ftp.scm 149 */
																						long BgL_arg1482z00_1553;
																						long BgL_arg1483z00_1554;

																						BgL_arg1482z00_1553 =
																							RGC_BUFFER_FORWARD
																							(BgL_iportz00_1545);
																						BgL_arg1483z00_1554 =
																							RGC_BUFFER_BUFPOS
																							(BgL_iportz00_1545);
																						{
																							long BgL_bufposz00_4883;
																							long BgL_forwardz00_4882;

																							BgL_forwardz00_4882 =
																								BgL_arg1482z00_1553;
																							BgL_bufposz00_4883 =
																								BgL_arg1483z00_1554;
																							BgL_bufposz00_1548 =
																								BgL_bufposz00_4883;
																							BgL_forwardz00_1547 =
																								BgL_forwardz00_4882;
																							goto
																								BgL_zc3z04anonymousza31479ze3z87_1549;
																						}
																					}
																				else
																					{	/* Unsafe/ftp.scm 149 */
																						BgL_matchz00_1722 =
																							BgL_newzd2matchzd2_1550;
																					}
																			}
																		else
																			{	/* Unsafe/ftp.scm 149 */
																				int BgL_curz00_1555;

																				BgL_curz00_1555 =
																					RGC_BUFFER_GET_CHAR(BgL_iportz00_1545,
																					BgL_forwardz00_1547);
																				{	/* Unsafe/ftp.scm 149 */

																					if (((long) (BgL_curz00_1555) == 10L))
																						{	/* Unsafe/ftp.scm 149 */
																							BgL_matchz00_1722 =
																								BgL_newzd2matchzd2_1550;
																						}
																					else
																						{
																							long BgL_bufposz00_4892;
																							long BgL_forwardz00_4890;
																							long BgL_lastzd2matchzd2_4889;
																							obj_t BgL_iportz00_4888;

																							BgL_iportz00_4888 =
																								BgL_iportz00_1545;
																							BgL_lastzd2matchzd2_4889 =
																								BgL_newzd2matchzd2_1550;
																							BgL_forwardz00_4890 =
																								(1L + BgL_forwardz00_1547);
																							BgL_bufposz00_4892 =
																								BgL_bufposz00_1548;
																							BgL_bufposz00_1562 =
																								BgL_bufposz00_4892;
																							BgL_forwardz00_1561 =
																								BgL_forwardz00_4890;
																							BgL_lastzd2matchzd2_1560 =
																								BgL_lastzd2matchzd2_4889;
																							BgL_iportz00_1559 =
																								BgL_iportz00_4888;
																							goto
																								BgL_zc3z04anonymousza31487ze3z87_1563;
																						}
																				}
																			}
																	}
																}
														}
												}
										}
									}
								}
						}
					}
					RGC_SET_FILEPOS(BgL_iportz00_4153);
					{

						switch (BgL_matchz00_1722)
							{
							case 4L:

								{	/* Unsafe/ftp.scm 166 */
									BgL_z62ftpzd2parsezd2errorz62_bglt BgL_arg1619z00_1727;

									{	/* Unsafe/ftp.scm 166 */
										BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1098z00_1728;

										{	/* Unsafe/ftp.scm 169 */
											BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1097z00_1731;

											BgL_new1097z00_1731 =
												((BgL_z62ftpzd2parsezd2errorz62_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_z62ftpzd2parsezd2errorz62_bgl))));
											{	/* Unsafe/ftp.scm 169 */
												long BgL_arg1622z00_1732;

												BgL_arg1622z00_1732 =
													BGL_CLASS_NUM
													(BGl_z62ftpzd2parsezd2errorz62zz__ftpz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1097z00_1731), BgL_arg1622z00_1732);
											}
											BgL_new1098z00_1728 = BgL_new1097z00_1731;
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1098z00_1728)))->
												BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_z62exceptionz62_bglt)
													COBJECT(((BgL_z62exceptionz62_bglt)
															BgL_new1098z00_1728)))->BgL_locationz00) =
											((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_4903;

											{	/* Unsafe/ftp.scm 169 */
												obj_t BgL_arg1620z00_1729;

												{	/* Unsafe/ftp.scm 169 */
													obj_t BgL_arg1621z00_1730;

													{	/* Unsafe/ftp.scm 169 */
														obj_t BgL_classz00_3133;

														BgL_classz00_3133 =
															BGl_z62ftpzd2parsezd2errorz62zz__ftpz00;
														BgL_arg1621z00_1730 =
															BGL_CLASS_ALL_FIELDS(BgL_classz00_3133);
													}
													BgL_arg1620z00_1729 =
														VECTOR_REF(BgL_arg1621z00_1730, 2L);
												}
												BgL_auxz00_4903 =
													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
													(BgL_arg1620z00_1729);
											}
											((((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																BgL_new1098z00_1728)))->BgL_stackz00) =
												((obj_t) BgL_auxz00_4903), BUNSPEC);
										}
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_new1098z00_1728)))->
												BgL_procz00) =
											((obj_t) BGl_string2488z00zz__ftpz00), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1098z00_1728)))->BgL_msgz00) =
											((obj_t) BGl_string2489z00zz__ftpz00), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1098z00_1728)))->BgL_objz00) =
											((obj_t) BgL_msgz00_4155), BUNSPEC);
										BgL_arg1619z00_1727 = BgL_new1098z00_1728;
									}
									return
										BGl_raisez00zz__errorz00(((obj_t) BgL_arg1619z00_1727));
								}
								break;
							case 3L:

								{	/* Unsafe/ftp.scm 163 */
									obj_t BgL_auxz00_4159;

									{	/* Unsafe/ftp.scm 163 */
										obj_t BgL_arg1623z00_1733;

										{	/* Unsafe/ftp.scm 149 */
											long BgL_arg1521z00_3135;

											BgL_arg1521z00_3135 =
												RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4153);
											BgL_arg1623z00_1733 =
												rgc_buffer_substring(BgL_iportz00_4153, 0L,
												BgL_arg1521z00_3135);
										}
										BgL_auxz00_4159 =
											string_append_3(
											((obj_t)
												((obj_t) CELL_REF(BgL_mesgz00_4156))),
											BGl_string2490z00zz__ftpz00, BgL_arg1623z00_1733);
									}
									CELL_SET(BgL_mesgz00_4156, BgL_auxz00_4159);
								}
								{	/* Unsafe/ftp.scm 164 */
									obj_t BgL_arg1624z00_1734;

									BgL_arg1624z00_1734 =
										BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_4157);
									return
										BGl_loopze70ze7zz__ftpz00(BgL_codez00_4158, BgL_ipz00_4157,
										BgL_mesgz00_4156, BgL_arg1624z00_1734);
								}
								break;
							case 2L:

								{	/* Unsafe/ftp.scm 160 */
									obj_t BgL_auxz00_4160;

									{	/* Unsafe/ftp.scm 160 */
										obj_t BgL_arg1625z00_1735;

										{	/* Unsafe/ftp.scm 160 */
											long BgL_arg1626z00_1736;

											BgL_arg1626z00_1736 =
												RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4153);
											BgL_arg1625z00_1735 =
												BGl_thezd2substringze70z35zz__ftpz00(BgL_iportz00_4153,
												(int) (1L), (int) (BgL_arg1626z00_1736));
										}
										BgL_auxz00_4160 =
											string_append_3(
											((obj_t)
												((obj_t) CELL_REF(BgL_mesgz00_4156))),
											BGl_string2490z00zz__ftpz00, BgL_arg1625z00_1735);
									}
									CELL_SET(BgL_mesgz00_4156, BgL_auxz00_4160);
								}
								{	/* Unsafe/ftp.scm 161 */
									obj_t BgL_arg1627z00_1737;

									BgL_arg1627z00_1737 =
										BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_4157);
									return
										BGl_loopze70ze7zz__ftpz00(BgL_codez00_4158, BgL_ipz00_4157,
										BgL_mesgz00_4156, BgL_arg1627z00_1737);
								}
								break;
							case 1L:

								{	/* Unsafe/ftp.scm 154 */
									obj_t BgL_code1z00_1738;
									obj_t BgL_mesg1z00_1739;

									{	/* Unsafe/ftp.scm 154 */
										obj_t BgL_arg1631z00_1744;

										BgL_arg1631z00_1744 =
											BGl_thezd2substringze70z35zz__ftpz00(BgL_iportz00_4153,
											(int) (0L), (int) (3L));
										{	/* Ieee/number.scm 175 */

											BgL_code1z00_1738 =
												BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
												(BgL_arg1631z00_1744, BINT(10L));
									}}
									{	/* Unsafe/ftp.scm 155 */
										long BgL_arg1634z00_1747;

										BgL_arg1634z00_1747 =
											RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4153);
										BgL_mesg1z00_1739 =
											BGl_thezd2substringze70z35zz__ftpz00(BgL_iportz00_4153,
											(int) (4L), (int) (BgL_arg1634z00_1747));
									}
									{	/* Unsafe/ftp.scm 156 */
										obj_t BgL_auxz00_4161;

										BgL_auxz00_4161 =
											string_append_3(
											((obj_t)
												((obj_t) CELL_REF(BgL_mesgz00_4156))),
											BGl_string2490z00zz__ftpz00, BgL_mesg1z00_1739);
										CELL_SET(BgL_mesgz00_4156, BgL_auxz00_4161);
									}
									{	/* Unsafe/ftp.scm 157 */
										bool_t BgL_test2942z00_4942;

										{	/* Unsafe/ftp.scm 157 */
											bool_t BgL_test2943z00_4943;

											if (INTEGERP(BgL_codez00_4158))
												{	/* Unsafe/ftp.scm 157 */
													BgL_test2943z00_4943 = INTEGERP(BgL_code1z00_1738);
												}
											else
												{	/* Unsafe/ftp.scm 157 */
													BgL_test2943z00_4943 = ((bool_t) 0);
												}
											if (BgL_test2943z00_4943)
												{	/* Unsafe/ftp.scm 157 */
													BgL_test2942z00_4942 =
														(
														(long) CINT(BgL_codez00_4158) ==
														(long) CINT(BgL_code1z00_1738));
												}
											else
												{	/* Unsafe/ftp.scm 157 */
													BgL_test2942z00_4942 =
														BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_codez00_4158,
														BgL_code1z00_1738);
												}
										}
										if (BgL_test2942z00_4942)
											{	/* Unsafe/ftp.scm 157 */
												return BFALSE;
											}
										else
											{	/* Unsafe/ftp.scm 158 */
												obj_t BgL_arg1630z00_1742;

												BgL_arg1630z00_1742 =
													BGl_readzd2linezd2zz__r4_input_6_10_2z00
													(BgL_ipz00_4157);
												return BGl_loopze70ze7zz__ftpz00(BgL_codez00_4158,
													BgL_ipz00_4157, BgL_mesgz00_4156,
													BgL_arg1630z00_1742);
											}
									}
								}
								break;
							case 0L:

								{	/* Unsafe/ftp.scm 151 */
									obj_t BgL_auxz00_4162;

									{	/* Unsafe/ftp.scm 151 */
										obj_t BgL_arg1636z00_1748;

										{	/* Unsafe/ftp.scm 151 */
											long BgL_arg1637z00_1749;

											BgL_arg1637z00_1749 =
												RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4153);
											BgL_arg1636z00_1748 =
												BGl_thezd2substringze70z35zz__ftpz00(BgL_iportz00_4153,
												(int) (4L), (int) (BgL_arg1637z00_1749));
										}
										BgL_auxz00_4162 =
											string_append_3(
											((obj_t)
												((obj_t) CELL_REF(BgL_mesgz00_4156))),
											BGl_string2490z00zz__ftpz00, BgL_arg1636z00_1748);
									}
									CELL_SET(BgL_mesgz00_4156, BgL_auxz00_4162);
								}
								{	/* Unsafe/ftp.scm 152 */
									obj_t BgL_arg1638z00_1750;

									BgL_arg1638z00_1750 =
										BGl_readzd2linezd2zz__r4_input_6_10_2z00(BgL_ipz00_4157);
									return
										BGl_loopze70ze7zz__ftpz00(BgL_codez00_4158, BgL_ipz00_4157,
										BgL_mesgz00_4156, BgL_arg1638z00_1750);
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00(BGl_string2486z00zz__ftpz00,
									BGl_string2487z00zz__ftpz00, BINT(BgL_matchz00_1722));
							}
					}
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zz__ftpz00(obj_t BgL_codez00_4165, obj_t BgL_ipz00_4164,
		obj_t BgL_mesgz00_4163, obj_t BgL_msgz00_1375)
	{
		{	/* Unsafe/ftp.scm 147 */
			if (EOF_OBJECTP(BgL_msgz00_1375))
				{	/* Unsafe/ftp.scm 148 */
					return BFALSE;
				}
			else
				{	/* Unsafe/ftp.scm 149 */
					obj_t BgL_port1076z00_1378;

					{	/* Ieee/port.scm 469 */
						long BgL_endz00_1795;

						BgL_endz00_1795 = STRING_LENGTH(((obj_t) BgL_msgz00_1375));
						{	/* Ieee/port.scm 469 */

							BgL_port1076z00_1378 =
								BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
								(BgL_msgz00_1375, BINT(0L), BINT(BgL_endz00_1795));
					}}
					{	/* Unsafe/ftp.scm 149 */
						obj_t BgL_exitd1077z00_1379;

						BgL_exitd1077z00_1379 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Unsafe/ftp.scm 149 */
							obj_t BgL_zc3z04anonymousza31641ze3z87_3903;

							BgL_zc3z04anonymousza31641ze3z87_3903 =
								MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31641ze3ze5zz__ftpz00,
								(int) (0L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31641ze3z87_3903,
								(int) (0L), BgL_port1076z00_1378);
							{	/* Unsafe/ftp.scm 149 */
								obj_t BgL_arg2321z00_2935;

								{	/* Unsafe/ftp.scm 149 */
									obj_t BgL_arg2323z00_2936;

									BgL_arg2323z00_2936 =
										BGL_EXITD_PROTECT(BgL_exitd1077z00_1379);
									BgL_arg2321z00_2935 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31641ze3z87_3903,
										BgL_arg2323z00_2936);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1077z00_1379,
									BgL_arg2321z00_2935);
								BUNSPEC;
							}
							{	/* Unsafe/ftp.scm 149 */
								obj_t BgL_tmp1079z00_1381;

								BgL_tmp1079z00_1381 =
									BGl_ignoreze70ze7zz__ftpz00(BgL_codez00_4165, BgL_ipz00_4164,
									BgL_mesgz00_4163, BgL_msgz00_1375, BgL_port1076z00_1378,
									BgL_port1076z00_1378);
								{	/* Unsafe/ftp.scm 149 */
									bool_t BgL_test2946z00_4981;

									{	/* Unsafe/ftp.scm 149 */
										obj_t BgL_arg2320z00_3145;

										BgL_arg2320z00_3145 =
											BGL_EXITD_PROTECT(BgL_exitd1077z00_1379);
										BgL_test2946z00_4981 = PAIRP(BgL_arg2320z00_3145);
									}
									if (BgL_test2946z00_4981)
										{	/* Unsafe/ftp.scm 149 */
											obj_t BgL_arg2318z00_3146;

											{	/* Unsafe/ftp.scm 149 */
												obj_t BgL_arg2319z00_3147;

												BgL_arg2319z00_3147 =
													BGL_EXITD_PROTECT(BgL_exitd1077z00_1379);
												BgL_arg2318z00_3146 =
													CDR(((obj_t) BgL_arg2319z00_3147));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1077z00_1379,
												BgL_arg2318z00_3146);
											BUNSPEC;
										}
									else
										{	/* Unsafe/ftp.scm 149 */
											BFALSE;
										}
								}
								bgl_close_input_port(BgL_port1076z00_1378);
								return BgL_tmp1079z00_1381;
							}
						}
					}
				}
		}

	}



/* &<@anonymous:1641> */
	obj_t BGl_z62zc3z04anonymousza31641ze3ze5zz__ftpz00(obj_t BgL_envz00_3904)
	{
		{	/* Unsafe/ftp.scm 149 */
			{	/* Unsafe/ftp.scm 149 */
				obj_t BgL_port1076z00_3905;

				BgL_port1076z00_3905 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3904, (int) (0L)));
				return bgl_close_input_port(BgL_port1076z00_3905);
			}
		}

	}



/* %ftp-read-dtp */
	obj_t BGl_z52ftpzd2readzd2dtpz52zz__ftpz00(obj_t BgL_ftpz00_9,
		obj_t BgL_whatz00_10)
	{
		{	/* Unsafe/ftp.scm 175 */
			{	/* Unsafe/ftp.scm 177 */
				obj_t BgL_ipz00_1808;

				{	/* Unsafe/ftp.scm 177 */
					obj_t BgL_arg1652z00_1818;

					BgL_arg1652z00_1818 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_9)))->BgL_dtpz00);
					{	/* Unsafe/ftp.scm 177 */
						obj_t BgL_tmpz00_4995;

						BgL_tmpz00_4995 = ((obj_t) BgL_arg1652z00_1818);
						BgL_ipz00_1808 = SOCKET_INPUT(BgL_tmpz00_4995);
					}
				}
				if ((BgL_whatz00_10 == BGl_symbol2491z00zz__ftpz00))
					{	/* Unsafe/ftp.scm 178 */
						return BGl_readzd2lineszd2zz__r4_input_6_10_2z00(BgL_ipz00_1808);
					}
				else
					{	/* Unsafe/ftp.scm 178 */
						if ((BgL_whatz00_10 == BGl_symbol2493z00zz__ftpz00))
							{	/* Unsafe/ftp.scm 178 */
								return BgL_ipz00_1808;
							}
						else
							{	/* Unsafe/ftp.scm 185 */
								BgL_z62ftpzd2errorzb0_bglt BgL_arg1648z00_1812;

								{	/* Unsafe/ftp.scm 185 */
									BgL_z62ftpzd2errorzb0_bglt BgL_new1101z00_1813;

									{	/* Unsafe/ftp.scm 185 */
										BgL_z62ftpzd2errorzb0_bglt BgL_new1100z00_1816;

										BgL_new1100z00_1816 =
											((BgL_z62ftpzd2errorzb0_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62ftpzd2errorzb0_bgl))));
										{	/* Unsafe/ftp.scm 185 */
											long BgL_arg1651z00_1817;

											BgL_arg1651z00_1817 =
												BGL_CLASS_NUM(BGl_z62ftpzd2errorzb0zz__ftpz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1100z00_1816),
												BgL_arg1651z00_1817);
										}
										BgL_new1101z00_1813 = BgL_new1100z00_1816;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1101z00_1813)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1101z00_1813)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_5011;

										{	/* Unsafe/ftp.scm 185 */
											obj_t BgL_arg1649z00_1814;

											{	/* Unsafe/ftp.scm 185 */
												obj_t BgL_arg1650z00_1815;

												{	/* Unsafe/ftp.scm 185 */
													obj_t BgL_classz00_3156;

													BgL_classz00_3156 = BGl_z62ftpzd2errorzb0zz__ftpz00;
													BgL_arg1650z00_1815 =
														BGL_CLASS_ALL_FIELDS(BgL_classz00_3156);
												}
												BgL_arg1649z00_1814 =
													VECTOR_REF(BgL_arg1650z00_1815, 2L);
											}
											BgL_auxz00_5011 =
												BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
												(BgL_arg1649z00_1814);
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1101z00_1813)))->
												BgL_stackz00) = ((obj_t) BgL_auxz00_5011), BUNSPEC);
									}
									((((BgL_z62errorz62_bglt) COBJECT(
													((BgL_z62errorz62_bglt) BgL_new1101z00_1813)))->
											BgL_procz00) =
										((obj_t) BGl_string2495z00zz__ftpz00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1101z00_1813)))->BgL_msgz00) =
										((obj_t) BGl_string2496z00zz__ftpz00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1101z00_1813)))->BgL_objz00) =
										((obj_t) BgL_whatz00_10), BUNSPEC);
									BgL_arg1648z00_1812 = BgL_new1101z00_1813;
								}
								return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1648z00_1812));
							}
					}
			}
		}

	}



/* %ftp-engine-cmd */
	obj_t BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(obj_t BgL_ftpz00_11,
		obj_t BgL_cmdz00_12, obj_t BgL_cmdsz00_13)
	{
		{	/* Unsafe/ftp.scm 193 */
			{	/* Unsafe/ftp.scm 195 */
				bool_t BgL_test2949z00_5025;

				{	/* Unsafe/ftp.scm 195 */
					obj_t BgL_arg1656z00_1822;

					BgL_arg1656z00_1822 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt)
									((BgL_ftpz00_bglt) BgL_ftpz00_11))))->BgL_cmdz00);
					BgL_test2949z00_5025 = SOCKETP(BgL_arg1656z00_1822);
				}
				if (BgL_test2949z00_5025)
					{	/* Unsafe/ftp.scm 195 */
						BFALSE;
					}
				else
					{	/* Unsafe/ftp.scm 195 */
						BGl_errorz00zz__errorz00(BGl_string2497z00zz__ftpz00,
							BGl_string2498z00zz__ftpz00, BgL_cmdz00_12);
					}
			}
			if (CBOOL(BgL_cmdz00_12))
				{	/* Unsafe/ftp.scm 197 */
					obj_t BgL_runner1660z00_1826;

					{	/* Unsafe/ftp.scm 197 */
						obj_t BgL_list1657z00_1823;

						{	/* Unsafe/ftp.scm 197 */
							obj_t BgL_arg1658z00_1824;

							BgL_arg1658z00_1824 = MAKE_YOUNG_PAIR(BgL_cmdsz00_13, BNIL);
							BgL_list1657z00_1823 =
								MAKE_YOUNG_PAIR(BgL_cmdz00_12, BgL_arg1658z00_1824);
						}
						BgL_runner1660z00_1826 =
							BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(BgL_ftpz00_11,
							BgL_list1657z00_1823);
					}
					{	/* Unsafe/ftp.scm 197 */
						obj_t BgL_aux1659z00_1825;

						BgL_aux1659z00_1825 = CAR(BgL_runner1660z00_1826);
						BgL_runner1660z00_1826 = CDR(BgL_runner1660z00_1826);
						{	/* Unsafe/ftp.scm 197 */
							obj_t BgL_msgsz00_3160;

							BgL_msgsz00_3160 = BgL_runner1660z00_1826;
							{	/* Unsafe/ftp.scm 130 */
								obj_t BgL_opz00_3162;

								{	/* Unsafe/ftp.scm 130 */
									obj_t BgL_arg1402z00_3163;

									BgL_arg1402z00_3163 =
										(((BgL_z52ftpz52_bglt) COBJECT(
												((BgL_z52ftpz52_bglt) BgL_aux1659z00_1825)))->
										BgL_cmdz00);
									{	/* Unsafe/ftp.scm 130 */
										obj_t BgL_tmpz00_5040;

										BgL_tmpz00_5040 = ((obj_t) BgL_arg1402z00_3163);
										BgL_opz00_3162 = SOCKET_OUTPUT(BgL_tmpz00_5040);
									}
								}
								{	/* Unsafe/ftp.scm 131 */
									obj_t BgL_list1401z00_3164;

									BgL_list1401z00_3164 =
										MAKE_YOUNG_PAIR(BgL_msgsz00_3160, BNIL);
									BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_opz00_3162,
										BGl_string2499z00zz__ftpz00, BgL_list1401z00_3164);
								}
								bgl_flush_output_port(BgL_opz00_3162);
							}
						}
					}
				}
			else
				{	/* Unsafe/ftp.scm 197 */
					BFALSE;
				}
			{	/* Unsafe/ftp.scm 198 */
				obj_t BgL_codez00_1827;

				BgL_codez00_1827 = BGl_z52ftpzd2readzd2cmdz52zz__ftpz00(BgL_ftpz00_11);
				{	/* Unsafe/ftp.scm 199 */
					obj_t BgL_mesgz00_1828;

					{	/* Unsafe/ftp.scm 200 */
						obj_t BgL_tmpz00_3167;

						{	/* Unsafe/ftp.scm 200 */
							int BgL_tmpz00_5047;

							BgL_tmpz00_5047 = (int) (1L);
							BgL_tmpz00_3167 = BGL_MVALUES_VAL(BgL_tmpz00_5047);
						}
						{	/* Unsafe/ftp.scm 200 */
							int BgL_tmpz00_5050;

							BgL_tmpz00_5050 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_5050, BUNSPEC);
						}
						BgL_mesgz00_1828 = BgL_tmpz00_3167;
					}
					{	/* Unsafe/ftp.scm 200 */
						obj_t BgL___retval2951z00_5053;

						{	/* Unsafe/ftp.scm 200 */
							obj_t BgL_res1109z00_4410;

							if (INTEGERP(BgL_codez00_1827))
								{	/* Unsafe/ftp.scm 205 */
									switch ((long) CINT(BgL_codez00_1827))
										{
										case 110L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 120L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 125L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 150L:

											{	/* Unsafe/ftp.scm 223 */
												obj_t BgL_resz00_4411;

												{	/* Unsafe/ftp.scm 225 */
													obj_t BgL_arg1663z00_4412;

													{	/* Unsafe/ftp.scm 225 */
														bool_t BgL_test2954z00_5056;

														{	/* Unsafe/ftp.scm 225 */
															long BgL_l1z00_4413;

															BgL_l1z00_4413 =
																STRING_LENGTH(((obj_t) BgL_cmdz00_12));
															if ((BgL_l1z00_4413 == 4L))
																{	/* Unsafe/ftp.scm 225 */
																	int BgL_arg2089z00_4414;

																	{	/* Unsafe/ftp.scm 225 */
																		char *BgL_auxz00_5064;
																		char *BgL_tmpz00_5061;

																		BgL_auxz00_5064 =
																			BSTRING_TO_STRING
																			(BGl_string2500z00zz__ftpz00);
																		BgL_tmpz00_5061 =
																			BSTRING_TO_STRING(((obj_t)
																				BgL_cmdz00_12));
																		BgL_arg2089z00_4414 =
																			memcmp(BgL_tmpz00_5061, BgL_auxz00_5064,
																			BgL_l1z00_4413);
																	}
																	BgL_test2954z00_5056 =
																		((long) (BgL_arg2089z00_4414) == 0L);
																}
															else
																{	/* Unsafe/ftp.scm 225 */
																	BgL_test2954z00_5056 = ((bool_t) 0);
																}
														}
														if (BgL_test2954z00_5056)
															{	/* Unsafe/ftp.scm 225 */
																BgL_arg1663z00_4412 =
																	BGl_symbol2493z00zz__ftpz00;
															}
														else
															{	/* Unsafe/ftp.scm 225 */
																BgL_arg1663z00_4412 =
																	BGl_symbol2491z00zz__ftpz00;
															}
													}
													BgL_resz00_4411 =
														BGl_z52ftpzd2readzd2dtpz52zz__ftpz00(BgL_ftpz00_11,
														BgL_arg1663z00_4412);
												}
												BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(BgL_ftpz00_11,
													BFALSE, BNIL);
												BgL_res1109z00_4410 = BgL_resz00_4411;
											}
											break;
										case 200L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 202L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 211L:

											BgL_res1109z00_4410 = BgL_mesgz00_1828;
											break;
										case 212L:

											BgL_res1109z00_4410 = BgL_mesgz00_1828;
											break;
										case 213L:

											BgL_res1109z00_4410 = BgL_mesgz00_1828;
											break;
										case 214L:

											BgL_res1109z00_4410 = BgL_mesgz00_1828;
											break;
										case 215L:

											BgL_res1109z00_4410 = BgL_mesgz00_1828;
											break;
										case 220L:

											((((BgL_ftpz00_bglt) COBJECT(
															((BgL_ftpz00_bglt) BgL_ftpz00_11)))->
													BgL_motdz00) = ((obj_t) BgL_mesgz00_1828), BUNSPEC);
											{	/* Unsafe/ftp.scm 260 */
												obj_t BgL_arg1667z00_4415;

												BgL_arg1667z00_4415 =
													(((BgL_ftpz00_bglt) COBJECT(
															((BgL_ftpz00_bglt) BgL_ftpz00_11)))->BgL_userz00);
												{	/* Unsafe/ftp.scm 260 */
													obj_t BgL_list1668z00_4416;

													BgL_list1668z00_4416 =
														MAKE_YOUNG_PAIR(BgL_arg1667z00_4415, BNIL);
													BgL_res1109z00_4410 =
														BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00
														(BgL_ftpz00_11, BGl_string2501z00zz__ftpz00,
														BgL_list1668z00_4416);
												}
											}
											break;
										case 221L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 225L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 226L:

											{	/* Unsafe/ftp.scm 272 */
												bool_t BgL_test2956z00_5077;

												{	/* Unsafe/ftp.scm 200 */

													BgL_test2956z00_5077 =
														CBOOL
														(BGl_stringzd2containszd2ciz00zz__r4_strings_6_7z00
														(BgL_mesgz00_1828, BGl_string2502z00zz__ftpz00,
															(int) (0L)));
												}
												if (BgL_test2956z00_5077)
													{	/* Unsafe/ftp.scm 272 */
														BgL_res1109z00_4410 = BFALSE;
													}
												else
													{	/* Unsafe/ftp.scm 272 */
														BgL_res1109z00_4410 =
															BGl_z52ftpzd2dtpzd2pasvzd2setupz80zz__ftpz00
															(BgL_ftpz00_11);
													}
											}
											break;
										case 227L:

											{	/* Unsafe/ftp.scm 276 */
												obj_t BgL_hostz00_4417;

												{	/* Unsafe/ftp.scm 278 */
													obj_t BgL_arg1675z00_4418;

													{	/* Unsafe/ftp.scm 278 */
														obj_t BgL_l1231z00_4419;

														{	/* Unsafe/ftp.scm 280 */
															obj_t BgL_arg1691z00_4420;

															{	/* Unsafe/ftp.scm 280 */
																long BgL_arg1699z00_4421;

																BgL_arg1699z00_4421 =
																	(STRING_LENGTH(
																		((obj_t) BgL_mesgz00_1828)) - 1L);
																BgL_arg1691z00_4420 =
																	c_substring(
																	((obj_t) BgL_mesgz00_1828), 1L,
																	BgL_arg1699z00_4421);
															}
															{	/* Unsafe/ftp.scm 279 */
																obj_t BgL_list1692z00_4422;

																BgL_list1692z00_4422 =
																	MAKE_YOUNG_PAIR(BGl_string2503z00zz__ftpz00,
																	BNIL);
																BgL_l1231z00_4419 =
																	BGl_stringzd2splitzd2zz__r4_strings_6_7z00
																	(BgL_arg1691z00_4420, BgL_list1692z00_4422);
														}}
														if (NULLP(BgL_l1231z00_4419))
															{	/* Unsafe/ftp.scm 278 */
																BgL_arg1675z00_4418 = BNIL;
															}
														else
															{	/* Unsafe/ftp.scm 278 */
																obj_t BgL_head1233z00_4423;

																{	/* Unsafe/ftp.scm 278 */
																	obj_t BgL_arg1688z00_4424;

																	{	/* Unsafe/ftp.scm 278 */

																		BgL_arg1688z00_4424 =
																			BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																			(CAR(BgL_l1231z00_4419), BINT(10L));
																	}
																	BgL_head1233z00_4423 =
																		MAKE_YOUNG_PAIR(BgL_arg1688z00_4424, BNIL);
																}
																{	/* Unsafe/ftp.scm 278 */
																	obj_t BgL_g1236z00_4425;

																	BgL_g1236z00_4425 = CDR(BgL_l1231z00_4419);
																	{
																		obj_t BgL_l1231z00_4427;
																		obj_t BgL_tail1234z00_4428;

																		BgL_l1231z00_4427 = BgL_g1236z00_4425;
																		BgL_tail1234z00_4428 = BgL_head1233z00_4423;
																	BgL_zc3z04anonymousza31677ze3z87_4426:
																		if (NULLP(BgL_l1231z00_4427))
																			{	/* Unsafe/ftp.scm 278 */
																				BgL_arg1675z00_4418 =
																					BgL_head1233z00_4423;
																			}
																		else
																			{	/* Unsafe/ftp.scm 278 */
																				obj_t BgL_newtail1235z00_4429;

																				{	/* Unsafe/ftp.scm 278 */
																					obj_t BgL_arg1684z00_4430;

																					{	/* Unsafe/ftp.scm 278 */

																						BgL_arg1684z00_4430 =
																							BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																							(CAR(((obj_t) BgL_l1231z00_4427)),
																							BINT(10L));
																					}
																					BgL_newtail1235z00_4429 =
																						MAKE_YOUNG_PAIR(BgL_arg1684z00_4430,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1234z00_4428,
																					BgL_newtail1235z00_4429);
																				{	/* Unsafe/ftp.scm 278 */
																					obj_t BgL_arg1681z00_4431;

																					BgL_arg1681z00_4431 =
																						CDR(((obj_t) BgL_l1231z00_4427));
																					{
																						obj_t BgL_tail1234z00_5107;
																						obj_t BgL_l1231z00_5106;

																						BgL_l1231z00_5106 =
																							BgL_arg1681z00_4431;
																						BgL_tail1234z00_5107 =
																							BgL_newtail1235z00_4429;
																						BgL_tail1234z00_4428 =
																							BgL_tail1234z00_5107;
																						BgL_l1231z00_4427 =
																							BgL_l1231z00_5106;
																						goto
																							BgL_zc3z04anonymousza31677ze3z87_4426;
																					}
																				}
																			}
																	}
																}
															}
													}
													BgL_hostz00_4417 =
														BGl_ftpportzd2ze3hostportz31zz__ftpz00
														(BgL_arg1675z00_4418);
												}
												{	/* Unsafe/ftp.scm 277 */
													obj_t BgL_portz00_4432;

													{	/* Unsafe/ftp.scm 281 */
														obj_t BgL_tmpz00_4433;

														{	/* Unsafe/ftp.scm 281 */
															int BgL_tmpz00_5109;

															BgL_tmpz00_5109 = (int) (1L);
															BgL_tmpz00_4433 =
																BGL_MVALUES_VAL(BgL_tmpz00_5109);
														}
														{	/* Unsafe/ftp.scm 281 */
															int BgL_tmpz00_5112;

															BgL_tmpz00_5112 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_5112, BUNSPEC);
														}
														BgL_portz00_4432 = BgL_tmpz00_4433;
													}
													BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(BgL_ftpz00_11);
													{
														obj_t BgL_auxz00_5116;

														{	/* Unsafe/ftp.scm 431 */
															obj_t BgL_list1754z00_4434;

															{	/* Unsafe/ftp.scm 431 */
																obj_t BgL_arg1755z00_4435;

																BgL_arg1755z00_4435 =
																	MAKE_YOUNG_PAIR(BgL_portz00_4432, BNIL);
																BgL_list1754z00_4434 =
																	MAKE_YOUNG_PAIR(BgL_hostz00_4417,
																	BgL_arg1755z00_4435);
															}
															BgL_auxz00_5116 = BgL_list1754z00_4434;
														}
														((((BgL_z52ftpz52_bglt) COBJECT(
																		((BgL_z52ftpz52_bglt) BgL_ftpz00_11)))->
																BgL_dtpz00) =
															((obj_t) BgL_auxz00_5116), BUNSPEC);
													}
													BgL_res1109z00_4410 =
														BGl_z52ftpzd2dtpzd2pasvzd2setupz80zz__ftpz00
														(BgL_ftpz00_11);
											}} break;
										case 230L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 250L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 257L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 331L:

											{	/* Unsafe/ftp.scm 296 */
												obj_t BgL_arg1701z00_4436;

												BgL_arg1701z00_4436 =
													(((BgL_ftpz00_bglt) COBJECT(
															((BgL_ftpz00_bglt) BgL_ftpz00_11)))->BgL_passz00);
												{	/* Unsafe/ftp.scm 296 */
													obj_t BgL_list1702z00_4437;

													BgL_list1702z00_4437 =
														MAKE_YOUNG_PAIR(BgL_arg1701z00_4436, BNIL);
													BgL_res1109z00_4410 =
														BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00
														(BgL_ftpz00_11, BGl_string2504z00zz__ftpz00,
														BgL_list1702z00_4437);
												}
											}
											break;
										case 332L:

											{	/* Unsafe/ftp.scm 299 */
												obj_t BgL_arg1703z00_4438;

												BgL_arg1703z00_4438 =
													(((BgL_ftpz00_bglt) COBJECT(
															((BgL_ftpz00_bglt) BgL_ftpz00_11)))->BgL_acctz00);
												{	/* Unsafe/ftp.scm 299 */
													obj_t BgL_list1704z00_4439;

													BgL_list1704z00_4439 =
														MAKE_YOUNG_PAIR(BgL_arg1703z00_4438, BNIL);
													BgL_res1109z00_4410 =
														BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00
														(BgL_ftpz00_11, BGl_string2505z00zz__ftpz00,
														BgL_list1704z00_4439);
												}
											}
											break;
										case 350L:

											BgL_res1109z00_4410 = BTRUE;
											break;
										case 421L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 425L:

											BGl_z52ftpzd2dtpzd2initz52zz__ftpz00(BgL_ftpz00_11);
											{	/* Unsafe/ftp.scm 314 */
												obj_t BgL_runner1709z00_4440;

												{	/* Unsafe/ftp.scm 314 */
													obj_t BgL_list1705z00_4441;

													{	/* Unsafe/ftp.scm 314 */
														obj_t BgL_arg1706z00_4442;

														BgL_arg1706z00_4442 =
															MAKE_YOUNG_PAIR(BgL_cmdsz00_13, BNIL);
														BgL_list1705z00_4441 =
															MAKE_YOUNG_PAIR(BgL_cmdz00_12,
															BgL_arg1706z00_4442);
													}
													BgL_runner1709z00_4440 =
														BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
														(BgL_ftpz00_11, BgL_list1705z00_4441);
												}
												{	/* Unsafe/ftp.scm 314 */
													obj_t BgL_aux1707z00_4443;

													BgL_aux1707z00_4443 = CAR(BgL_runner1709z00_4440);
													BgL_runner1709z00_4440 = CDR(BgL_runner1709z00_4440);
													{	/* Unsafe/ftp.scm 314 */
														obj_t BgL_aux1708z00_4444;

														BgL_aux1708z00_4444 = CAR(BgL_runner1709z00_4440);
														BgL_runner1709z00_4440 =
															CDR(BgL_runner1709z00_4440);
														BgL_res1109z00_4410 =
															BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00
															(BgL_aux1707z00_4443, BgL_aux1708z00_4444,
															BgL_runner1709z00_4440);
													}
												}
											}
											break;
										case 426L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 450L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 451L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 452L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 500L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 501L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 502L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 503L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 504L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 530L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 532L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 550L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 551L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 552L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 553L:

											BgL_res1109z00_4410 = BFALSE;
											break;
										case 999L:

											BGl_z52ftpzd2closez80zz__ftpz00(BgL_ftpz00_11);
											BgL___retval2951z00_5053 = BGl_string2506z00zz__ftpz00;
											goto __return2952;
											break;
										default:
											BgL_res1109z00_4410 =
												BGl_errorz00zz__errorz00(BGl_string2497z00zz__ftpz00,
												BGl_string2507z00zz__ftpz00, BgL_codez00_1827);
										}
								}
							else
								{	/* Unsafe/ftp.scm 205 */
									BgL_res1109z00_4410 =
										BGl_errorz00zz__errorz00(BGl_string2497z00zz__ftpz00,
										BGl_string2507z00zz__ftpz00, BgL_codez00_1827);
								}
							BgL___retval2951z00_5053 = BgL_res1109z00_4410;
						}
					__return2952:
						return BgL___retval2951z00_5053;
					}
				}
			}
		}

	}



/* %ftp-close */
	obj_t BGl_z52ftpzd2closez80zz__ftpz00(obj_t BgL_ftpz00_14)
	{
		{	/* Unsafe/ftp.scm 376 */
			{	/* Unsafe/ftp.scm 377 */
				obj_t BgL_exitd1112z00_3244;

				BgL_exitd1112z00_3244 = BGL_EXITD_TOP_AS_OBJ();
				{	/* Unsafe/ftp.scm 379 */
					obj_t BgL_zc3z04anonymousza31711ze3z87_3906;

					BgL_zc3z04anonymousza31711ze3z87_3906 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31711ze3ze5zz__ftpz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3906,
						(int) (0L), BgL_ftpz00_14);
					{	/* Unsafe/ftp.scm 377 */
						obj_t BgL_arg2321z00_3247;

						{	/* Unsafe/ftp.scm 377 */
							obj_t BgL_arg2323z00_3248;

							BgL_arg2323z00_3248 = BGL_EXITD_PROTECT(BgL_exitd1112z00_3244);
							BgL_arg2321z00_3247 =
								MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31711ze3z87_3906,
								BgL_arg2323z00_3248);
						}
						BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_3244, BgL_arg2321z00_3247);
						BUNSPEC;
					}
					{	/* Unsafe/ftp.scm 378 */
						obj_t BgL_tmp1114z00_3246;

						{	/* Unsafe/ftp.scm 386 */
							bool_t BgL_test2959z00_5153;

							{	/* Unsafe/ftp.scm 386 */
								obj_t BgL_arg1718z00_3251;

								BgL_arg1718z00_3251 =
									(((BgL_z52ftpz52_bglt) COBJECT(
											((BgL_z52ftpz52_bglt) BgL_ftpz00_14)))->BgL_cmdz00);
								BgL_test2959z00_5153 = SOCKETP(BgL_arg1718z00_3251);
							}
							if (BgL_test2959z00_5153)
								{	/* Unsafe/ftp.scm 387 */
									obj_t BgL_arg1717z00_3252;

									BgL_arg1717z00_3252 =
										(((BgL_z52ftpz52_bglt) COBJECT(
												((BgL_z52ftpz52_bglt) BgL_ftpz00_14)))->BgL_cmdz00);
									BgL_tmp1114z00_3246 =
										socket_close(((obj_t) BgL_arg1717z00_3252));
								}
							else
								{	/* Unsafe/ftp.scm 386 */
									BgL_tmp1114z00_3246 = BFALSE;
								}
						}
						{	/* Unsafe/ftp.scm 377 */
							bool_t BgL_test2960z00_5161;

							{	/* Unsafe/ftp.scm 377 */
								obj_t BgL_arg2320z00_3255;

								BgL_arg2320z00_3255 = BGL_EXITD_PROTECT(BgL_exitd1112z00_3244);
								BgL_test2960z00_5161 = PAIRP(BgL_arg2320z00_3255);
							}
							if (BgL_test2960z00_5161)
								{	/* Unsafe/ftp.scm 377 */
									obj_t BgL_arg2318z00_3256;

									{	/* Unsafe/ftp.scm 377 */
										obj_t BgL_arg2319z00_3257;

										BgL_arg2319z00_3257 =
											BGL_EXITD_PROTECT(BgL_exitd1112z00_3244);
										BgL_arg2318z00_3256 = CDR(((obj_t) BgL_arg2319z00_3257));
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_3244,
										BgL_arg2318z00_3256);
									BUNSPEC;
								}
							else
								{	/* Unsafe/ftp.scm 377 */
									BFALSE;
								}
						}
						BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(BgL_ftpz00_14);
						return BgL_tmp1114z00_3246;
					}
				}
			}
		}

	}



/* &<@anonymous:1711> */
	obj_t BGl_z62zc3z04anonymousza31711ze3ze5zz__ftpz00(obj_t BgL_envz00_3907)
	{
		{	/* Unsafe/ftp.scm 377 */
			return
				BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(PROCEDURE_REF(BgL_envz00_3907,
					(int) (0L)));
		}

	}



/* %ftp-close-cmd */
	obj_t BGl_z52ftpzd2closezd2cmdz52zz__ftpz00(obj_t BgL_ftpz00_15)
	{
		{	/* Unsafe/ftp.scm 384 */
			{	/* Unsafe/ftp.scm 386 */
				bool_t BgL_test2961z00_5172;

				{	/* Unsafe/ftp.scm 386 */
					obj_t BgL_arg1718z00_3261;

					BgL_arg1718z00_3261 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_15)))->BgL_cmdz00);
					BgL_test2961z00_5172 = SOCKETP(BgL_arg1718z00_3261);
				}
				if (BgL_test2961z00_5172)
					{	/* Unsafe/ftp.scm 387 */
						obj_t BgL_arg1717z00_3262;

						BgL_arg1717z00_3262 =
							(((BgL_z52ftpz52_bglt) COBJECT(
									((BgL_z52ftpz52_bglt) BgL_ftpz00_15)))->BgL_cmdz00);
						return socket_close(((obj_t) BgL_arg1717z00_3262));
					}
				else
					{	/* Unsafe/ftp.scm 386 */
						return BFALSE;
					}
			}
		}

	}



/* %ftp-close-dtp */
	obj_t BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(obj_t BgL_ftpz00_16)
	{
		{	/* Unsafe/ftp.scm 392 */
			{	/* Unsafe/ftp.scm 394 */
				bool_t BgL_test2962z00_5180;

				{	/* Unsafe/ftp.scm 394 */
					obj_t BgL_arg1726z00_1910;

					BgL_arg1726z00_1910 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_16)))->BgL_dtpz00);
					BgL_test2962z00_5180 = SOCKETP(BgL_arg1726z00_1910);
				}
				if (BgL_test2962z00_5180)
					{	/* Unsafe/ftp.scm 394 */
						if (
							(((BgL_z52ftpz52_bglt) COBJECT(
										((BgL_z52ftpz52_bglt) BgL_ftpz00_16)))->BgL_passivezf3zf3))
							{	/* Unsafe/ftp.scm 396 */
								obj_t BgL_arg1723z00_1906;

								BgL_arg1723z00_1906 =
									(((BgL_z52ftpz52_bglt) COBJECT(
											((BgL_z52ftpz52_bglt) BgL_ftpz00_16)))->BgL_dtpz00);
								return socket_close(((obj_t) BgL_arg1723z00_1906));
							}
						else
							{	/* Unsafe/ftp.scm 397 */
								obj_t BgL_arg1724z00_1907;

								BgL_arg1724z00_1907 =
									(((BgL_z52ftpz52_bglt) COBJECT(
											((BgL_z52ftpz52_bglt) BgL_ftpz00_16)))->BgL_dtpz00);
								{	/* Llib/socket.scm 233 */

									return
										BINT(BGl_socketzd2shutdownzd2zz__socketz00
										(BgL_arg1724z00_1907, BTRUE));
								}
							}
					}
				else
					{	/* Unsafe/ftp.scm 394 */
						return BFALSE;
					}
			}
		}

	}



/* ftp-connect-to */
	obj_t BGl_ftpzd2connectzd2toz00zz__ftpz00(obj_t BgL_hostz00_17,
		obj_t BgL_portz00_18)
	{
		{	/* Unsafe/ftp.scm 402 */
			{	/* Unsafe/ftp.scm 403 */
				obj_t BgL_arg1743z00_1938;

				{	/* Unsafe/ftp.scm 403 */
					BgL_ftpz00_bglt BgL_new1120z00_1941;

					{	/* Unsafe/ftp.scm 403 */
						BgL_ftpz00_bglt BgL_new1119z00_1942;

						BgL_new1119z00_1942 =
							((BgL_ftpz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_ftpz00_bgl))));
						{	/* Unsafe/ftp.scm 403 */
							obj_t BgL_arg1744z00_1943;

							{	/* Unsafe/ftp.scm 403 */
								obj_t BgL_classz00_3312;

								BgL_classz00_3312 = BGl_ftpz00zz__ftpz00;
								BgL_arg1744z00_1943 = BINT(BGL_CLASS_NUM(BgL_classz00_3312));
							}
							{	/* Unsafe/ftp.scm 403 */
								long BgL_numz00_3314;

								BgL_numz00_3314 = (long) CINT(BgL_arg1744z00_1943);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1119z00_1942), BgL_numz00_3314);
						}}
						BgL_new1120z00_1941 = BgL_new1119z00_1942;
					}
					((((BgL_z52ftpz52_bglt) COBJECT(
									((BgL_z52ftpz52_bglt) BgL_new1120z00_1941)))->BgL_cmdz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
										BgL_new1120z00_1941)))->BgL_dtpz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
										BgL_new1120z00_1941)))->BgL_passivezf3zf3) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1120z00_1941))->BgL_hostz00) =
						((obj_t) BgL_hostz00_17), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1120z00_1941))->BgL_portz00) =
						((obj_t) BgL_portz00_18), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1120z00_1941))->BgL_motdz00) =
						((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1120z00_1941))->BgL_userz00) =
						((obj_t) BGl_string2518z00zz__ftpz00), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1120z00_1941))->BgL_passz00) =
						((obj_t) BGl_string2519z00zz__ftpz00), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1120z00_1941))->BgL_acctz00) =
						((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
					BgL_arg1743z00_1938 = ((obj_t) BgL_new1120z00_1941);
				}
				{	/* Unsafe/ftp.scm 67 */

					{	/* Unsafe/ftp.scm 67 */
						bool_t BgL_res2351z00_3324;

						{
							obj_t BgL_auxz00_5214;

							{	/* Unsafe/ftp.scm 410 */
								obj_t BgL_arg1745z00_3317;
								obj_t BgL_arg1746z00_3318;

								BgL_arg1745z00_3317 =
									(((BgL_ftpz00_bglt) COBJECT(
											((BgL_ftpz00_bglt) BgL_arg1743z00_1938)))->BgL_hostz00);
								BgL_arg1746z00_3318 =
									(((BgL_ftpz00_bglt) COBJECT(
											((BgL_ftpz00_bglt) BgL_arg1743z00_1938)))->BgL_portz00);
								{	/* Unsafe/ftp.scm 410 */
									obj_t BgL_domainz00_3319;

									BgL_domainz00_3319 = BGl_symbol2520z00zz__ftpz00;
									BgL_auxz00_5214 =
										BGl_makezd2clientzd2socketz00zz__socketz00
										(BgL_arg1745z00_3317, CINT(BgL_arg1746z00_3318),
										BgL_domainz00_3319, BTRUE, BTRUE, BINT(0L));
							}}
							((((BgL_z52ftpz52_bglt) COBJECT(
											((BgL_z52ftpz52_bglt)
												((BgL_ftpz00_bglt) BgL_arg1743z00_1938))))->
									BgL_cmdz00) = ((obj_t) BgL_auxz00_5214), BUNSPEC);
						}
						BgL_res2351z00_3324 =
							CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
								((obj_t)
									((BgL_ftpz00_bglt) BgL_arg1743z00_1938)), BFALSE, BNIL));
						return BBOOL(BgL_res2351z00_3324);
					}
				}
			}
		}

	}



/* _ftp-connect */
	obj_t BGl__ftpzd2connectzd2zz__ftpz00(obj_t BgL_env1293z00_24,
		obj_t BgL_opt1292z00_23)
	{
		{	/* Unsafe/ftp.scm 408 */
			{	/* Unsafe/ftp.scm 408 */
				obj_t BgL_g1294z00_1944;

				BgL_g1294z00_1944 = VECTOR_REF(BgL_opt1292z00_23, 0L);
				switch (VECTOR_LENGTH(BgL_opt1292z00_23))
					{
					case 1L:

						{	/* Unsafe/ftp.scm 408 */

							{	/* Unsafe/ftp.scm 408 */
								bool_t BgL_res2352z00_3334;

								{	/* Unsafe/ftp.scm 408 */
									BgL_ftpz00_bglt BgL_ftpz00_3325;

									if (BGl_isazf3zf3zz__objectz00(BgL_g1294z00_1944,
											BGl_ftpz00zz__ftpz00))
										{	/* Unsafe/ftp.scm 408 */
											BgL_ftpz00_3325 = ((BgL_ftpz00_bglt) BgL_g1294z00_1944);
										}
									else
										{
											obj_t BgL_auxz00_5234;

											BgL_auxz00_5234 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2522z00zz__ftpz00, BINT(13299L),
												BGl_string2523z00zz__ftpz00,
												BGl_string2497z00zz__ftpz00, BgL_g1294z00_1944);
											FAILURE(BgL_auxz00_5234, BFALSE, BFALSE);
										}
									{
										obj_t BgL_auxz00_5238;

										{	/* Unsafe/ftp.scm 410 */
											obj_t BgL_arg1745z00_3327;
											obj_t BgL_arg1746z00_3328;

											BgL_arg1745z00_3327 =
												(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_3325))->
												BgL_hostz00);
											BgL_arg1746z00_3328 =
												(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_3325))->
												BgL_portz00);
											{	/* Unsafe/ftp.scm 410 */
												obj_t BgL_domainz00_3329;

												BgL_domainz00_3329 = BGl_symbol2520z00zz__ftpz00;
												BgL_auxz00_5238 =
													BGl_makezd2clientzd2socketz00zz__socketz00
													(BgL_arg1745z00_3327, CINT(BgL_arg1746z00_3328),
													BgL_domainz00_3329, BTRUE, BTRUE, BINT(0L));
											}
										}
										((((BgL_z52ftpz52_bglt) COBJECT(
														((BgL_z52ftpz52_bglt) BgL_ftpz00_3325)))->
												BgL_cmdz00) = ((obj_t) BgL_auxz00_5238), BUNSPEC);
									}
									BgL_res2352z00_3334 =
										CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
											((obj_t) BgL_ftpz00_3325), BFALSE, BNIL));
								}
								return BBOOL(BgL_res2352z00_3334);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/ftp.scm 408 */
							obj_t BgL_timeoutz00_1948;

							BgL_timeoutz00_1948 = VECTOR_REF(BgL_opt1292z00_23, 1L);
							{	/* Unsafe/ftp.scm 408 */

								{	/* Unsafe/ftp.scm 408 */
									bool_t BgL_res2353z00_3344;

									{	/* Unsafe/ftp.scm 408 */
										BgL_ftpz00_bglt BgL_ftpz00_3335;

										if (BGl_isazf3zf3zz__objectz00(BgL_g1294z00_1944,
												BGl_ftpz00zz__ftpz00))
											{	/* Unsafe/ftp.scm 408 */
												BgL_ftpz00_3335 = ((BgL_ftpz00_bglt) BgL_g1294z00_1944);
											}
										else
											{
												obj_t BgL_auxz00_5254;

												BgL_auxz00_5254 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2522z00zz__ftpz00, BINT(13299L),
													BGl_string2523z00zz__ftpz00,
													BGl_string2497z00zz__ftpz00, BgL_g1294z00_1944);
												FAILURE(BgL_auxz00_5254, BFALSE, BFALSE);
											}
										{
											obj_t BgL_auxz00_5258;

											{	/* Unsafe/ftp.scm 410 */
												obj_t BgL_arg1745z00_3337;
												obj_t BgL_arg1746z00_3338;

												BgL_arg1745z00_3337 =
													(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_3335))->
													BgL_hostz00);
												BgL_arg1746z00_3338 =
													(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_3335))->
													BgL_portz00);
												{	/* Unsafe/ftp.scm 410 */
													obj_t BgL_domainz00_3339;

													BgL_domainz00_3339 = BGl_symbol2520z00zz__ftpz00;
													BgL_auxz00_5258 =
														BGl_makezd2clientzd2socketz00zz__socketz00
														(BgL_arg1745z00_3337, CINT(BgL_arg1746z00_3338),
														BgL_domainz00_3339, BTRUE, BTRUE,
														BgL_timeoutz00_1948);
												}
											}
											((((BgL_z52ftpz52_bglt) COBJECT(
															((BgL_z52ftpz52_bglt) BgL_ftpz00_3335)))->
													BgL_cmdz00) = ((obj_t) BgL_auxz00_5258), BUNSPEC);
										}
										BgL_res2353z00_3344 =
											CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
												((obj_t) BgL_ftpz00_3335), BFALSE, BNIL));
									}
									return BBOOL(BgL_res2353z00_3344);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* ftp-connect */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2connectzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_21, obj_t BgL_timeoutz00_22)
	{
		{	/* Unsafe/ftp.scm 408 */
			{
				obj_t BgL_auxz00_5271;

				{	/* Unsafe/ftp.scm 410 */
					obj_t BgL_arg1745z00_3346;
					obj_t BgL_arg1746z00_3347;

					BgL_arg1745z00_3346 =
						(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_21))->BgL_hostz00);
					BgL_arg1746z00_3347 =
						(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_21))->BgL_portz00);
					{	/* Unsafe/ftp.scm 410 */
						obj_t BgL_domainz00_3348;

						BgL_domainz00_3348 = BGl_symbol2520z00zz__ftpz00;
						BgL_auxz00_5271 =
							BGl_makezd2clientzd2socketz00zz__socketz00(BgL_arg1745z00_3346,
							CINT(BgL_arg1746z00_3347), BgL_domainz00_3348, BTRUE, BTRUE,
							BgL_timeoutz00_22);
					}
				}
				((((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_21)))->BgL_cmdz00) =
					((obj_t) BgL_auxz00_5271), BUNSPEC);
			}
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_21), BFALSE, BNIL));
		}

	}



/* %ftp-dtp-init */
	obj_t BGl_z52ftpzd2dtpzd2initz52zz__ftpz00(obj_t BgL_ftpz00_25)
	{
		{	/* Unsafe/ftp.scm 416 */
			if (
				(((BgL_z52ftpz52_bglt) COBJECT(
							((BgL_z52ftpz52_bglt) BgL_ftpz00_25)))->BgL_passivezf3zf3))
				{	/* Unsafe/ftp.scm 418 */
					return
						BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(BgL_ftpz00_25,
						BGl_string2524z00zz__ftpz00, BNIL);
				}
			else
				{	/* Unsafe/ftp.scm 420 */
					BgL_z62ftpzd2errorzb0_bglt BgL_arg1750z00_1960;

					{	/* Unsafe/ftp.scm 420 */
						BgL_z62ftpzd2errorzb0_bglt BgL_new1125z00_1961;

						{	/* Unsafe/ftp.scm 420 */
							BgL_z62ftpzd2errorzb0_bglt BgL_new1124z00_1964;

							BgL_new1124z00_1964 =
								((BgL_z62ftpzd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_z62ftpzd2errorzb0_bgl))));
							{	/* Unsafe/ftp.scm 420 */
								long BgL_arg1753z00_1965;

								BgL_arg1753z00_1965 =
									BGL_CLASS_NUM(BGl_z62ftpzd2errorzb0zz__ftpz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1124z00_1964),
									BgL_arg1753z00_1965);
							}
							BgL_new1125z00_1961 = BgL_new1124z00_1964;
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1125z00_1961)))->
								BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
						((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
											BgL_new1125z00_1961)))->BgL_locationz00) =
							((obj_t) BFALSE), BUNSPEC);
						{
							obj_t BgL_auxz00_5293;

							{	/* Unsafe/ftp.scm 420 */
								obj_t BgL_arg1751z00_1962;

								{	/* Unsafe/ftp.scm 420 */
									obj_t BgL_arg1752z00_1963;

									{	/* Unsafe/ftp.scm 420 */
										obj_t BgL_classz00_3356;

										BgL_classz00_3356 = BGl_z62ftpzd2errorzb0zz__ftpz00;
										BgL_arg1752z00_1963 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_3356);
									}
									BgL_arg1751z00_1962 = VECTOR_REF(BgL_arg1752z00_1963, 2L);
								}
								BgL_auxz00_5293 =
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg1751z00_1962);
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1125z00_1961)))->
									BgL_stackz00) = ((obj_t) BgL_auxz00_5293), BUNSPEC);
						}
						((((BgL_z62errorz62_bglt) COBJECT(
										((BgL_z62errorz62_bglt) BgL_new1125z00_1961)))->
								BgL_procz00) = ((obj_t) BGl_string2525z00zz__ftpz00), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1125z00_1961)))->BgL_msgz00) =
							((obj_t) BGl_string2526z00zz__ftpz00), BUNSPEC);
						((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
											BgL_new1125z00_1961)))->BgL_objz00) =
							((obj_t) BgL_ftpz00_25), BUNSPEC);
						BgL_arg1750z00_1960 = BgL_new1125z00_1961;
					}
					BGL_TAIL return
						BGl_raisez00zz__errorz00(((obj_t) BgL_arg1750z00_1960));
				}
		}

	}



/* %ftp-dtp-pasv-setup */
	obj_t BGl_z52ftpzd2dtpzd2pasvzd2setupz80zz__ftpz00(obj_t BgL_ftpz00_29)
	{
		{	/* Unsafe/ftp.scm 436 */
			{	/* Unsafe/ftp.scm 438 */
				obj_t BgL___retval2967z00_5307;

				{	/* Unsafe/ftp.scm 438 */
					bool_t BgL_res1131z00_4461;

					{	/* Unsafe/ftp.scm 439 */
						obj_t BgL_hostz00_4462;

						{	/* Unsafe/ftp.scm 441 */
							bool_t BgL_test2969z00_5308;

							{	/* Unsafe/ftp.scm 441 */
								obj_t BgL_arg1767z00_4463;

								BgL_arg1767z00_4463 =
									(((BgL_z52ftpz52_bglt) COBJECT(
											((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->BgL_dtpz00);
								BgL_test2969z00_5308 = SOCKETP(BgL_arg1767z00_4463);
							}
							if (BgL_test2969z00_5308)
								{	/* Unsafe/ftp.scm 442 */
									obj_t BgL_val0_1239z00_4464;
									obj_t BgL_val1_1240z00_4465;

									{	/* Unsafe/ftp.scm 442 */
										obj_t BgL_arg1760z00_4466;

										BgL_arg1760z00_4466 =
											(((BgL_z52ftpz52_bglt) COBJECT(
													((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->BgL_dtpz00);
										BgL_val0_1239z00_4464 =
											bgl_socket_host_addr(((obj_t) BgL_arg1760z00_4466));
									}
									{	/* Unsafe/ftp.scm 443 */
										obj_t BgL_arg1761z00_4467;

										BgL_arg1761z00_4467 =
											(((BgL_z52ftpz52_bglt) COBJECT(
													((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->BgL_dtpz00);
										{	/* Unsafe/ftp.scm 443 */
											obj_t BgL_res2355z00_4468;

											{	/* Unsafe/ftp.scm 443 */
												int BgL_tmpz00_5318;

												{	/* Unsafe/ftp.scm 443 */
													obj_t BgL_tmpz00_5319;

													BgL_tmpz00_5319 = ((obj_t) BgL_arg1761z00_4467);
													BgL_tmpz00_5318 = SOCKET_PORT(BgL_tmpz00_5319);
												}
												BgL_res2355z00_4468 = BINT(BgL_tmpz00_5318);
											}
											BgL_val1_1240z00_4465 = BgL_res2355z00_4468;
									}}
									{	/* Unsafe/ftp.scm 442 */
										int BgL_tmpz00_5323;

										BgL_tmpz00_5323 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5323);
									}
									{	/* Unsafe/ftp.scm 442 */
										int BgL_tmpz00_5326;

										BgL_tmpz00_5326 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_5326, BgL_val1_1240z00_4465);
									}
									BgL_hostz00_4462 = BgL_val0_1239z00_4464;
								}
							else
								{	/* Unsafe/ftp.scm 444 */
									bool_t BgL_test2970z00_5329;

									{	/* Unsafe/ftp.scm 444 */
										obj_t BgL_tmpz00_5330;

										BgL_tmpz00_5330 =
											(((BgL_z52ftpz52_bglt) COBJECT(
													((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->BgL_dtpz00);
										BgL_test2970z00_5329 = PAIRP(BgL_tmpz00_5330);
									}
									if (BgL_test2970z00_5329)
										{	/* Unsafe/ftp.scm 445 */
											obj_t BgL_val0_1241z00_4469;
											obj_t BgL_val1_1242z00_4470;

											{	/* Unsafe/ftp.scm 445 */
												obj_t BgL_pairz00_4471;

												BgL_pairz00_4471 =
													(((BgL_z52ftpz52_bglt) COBJECT(
															((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->
													BgL_dtpz00);
												BgL_val0_1241z00_4469 = CAR(BgL_pairz00_4471);
											}
											{	/* Unsafe/ftp.scm 445 */
												obj_t BgL_pairz00_4472;

												BgL_pairz00_4472 =
													(((BgL_z52ftpz52_bglt) COBJECT(
															((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->
													BgL_dtpz00);
												BgL_val1_1242z00_4470 = CAR(CDR(BgL_pairz00_4472));
											}
											{	/* Unsafe/ftp.scm 445 */
												int BgL_tmpz00_5341;

												BgL_tmpz00_5341 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5341);
											}
											{	/* Unsafe/ftp.scm 445 */
												int BgL_tmpz00_5344;

												BgL_tmpz00_5344 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_5344,
													BgL_val1_1242z00_4470);
											}
											BgL_hostz00_4462 = BgL_val0_1241z00_4469;
										}
									else
										{	/* Unsafe/ftp.scm 444 */
											BgL___retval2967z00_5307 = BFALSE;
											goto __return2968;
										}
								}
						}
						{	/* Unsafe/ftp.scm 440 */
							obj_t BgL_portz00_4473;

							{	/* Unsafe/ftp.scm 448 */
								obj_t BgL_tmpz00_4474;

								{	/* Unsafe/ftp.scm 448 */
									int BgL_tmpz00_5347;

									BgL_tmpz00_5347 = (int) (1L);
									BgL_tmpz00_4474 = BGL_MVALUES_VAL(BgL_tmpz00_5347);
								}
								{	/* Unsafe/ftp.scm 448 */
									int BgL_tmpz00_5350;

									BgL_tmpz00_5350 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_5350, BUNSPEC);
								}
								BgL_portz00_4473 = BgL_tmpz00_4474;
							}
							{
								obj_t BgL_auxz00_5353;

								{	/* Unsafe/ftp.scm 448 */
									obj_t BgL_domainz00_4475;

									BgL_domainz00_4475 = BGl_symbol2520z00zz__ftpz00;
									BgL_auxz00_5353 =
										BGl_makezd2clientzd2socketz00zz__socketz00(BgL_hostz00_4462,
										CINT(BgL_portz00_4473), BgL_domainz00_4475, BTRUE, BTRUE,
										BINT(0L));
								}
								((((BgL_z52ftpz52_bglt) COBJECT(
												((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->BgL_dtpz00) =
									((obj_t) BgL_auxz00_5353), BUNSPEC);
							}
							{	/* Unsafe/ftp.scm 449 */
								obj_t BgL_arg1757z00_4476;

								BgL_arg1757z00_4476 =
									(((BgL_z52ftpz52_bglt) COBJECT(
											((BgL_z52ftpz52_bglt) BgL_ftpz00_29)))->BgL_dtpz00);
								BgL_res1131z00_4461 = SOCKETP(BgL_arg1757z00_4476);
					}}}
					BgL___retval2967z00_5307 = BBOOL(BgL_res1131z00_4461);
				}
			__return2968:
				return BgL___retval2967z00_5307;
			}
		}

	}



/* ftpport->hostport */
	obj_t BGl_ftpportzd2ze3hostportz31zz__ftpz00(obj_t BgL_plistz00_32)
	{
		{	/* Unsafe/ftp.scm 462 */
			{	/* Unsafe/ftp.scm 463 */
				obj_t BgL_addrz00_2006;
				obj_t BgL_portz00_2007;

				{	/* Unsafe/ftp.scm 464 */
					obj_t BgL_arg1775z00_2010;
					obj_t BgL_arg1777z00_2011;
					obj_t BgL_arg1779z00_2012;
					obj_t BgL_arg1781z00_2013;

					BgL_arg1775z00_2010 = bgl_list_ref(BgL_plistz00_32, 0L);
					BgL_arg1777z00_2011 = bgl_list_ref(BgL_plistz00_32, 1L);
					BgL_arg1779z00_2012 = bgl_list_ref(BgL_plistz00_32, 2L);
					BgL_arg1781z00_2013 = bgl_list_ref(BgL_plistz00_32, 3L);
					{	/* Unsafe/ftp.scm 463 */
						obj_t BgL_list1782z00_2014;

						{	/* Unsafe/ftp.scm 463 */
							obj_t BgL_arg1783z00_2015;

							{	/* Unsafe/ftp.scm 463 */
								obj_t BgL_arg1785z00_2016;

								{	/* Unsafe/ftp.scm 463 */
									obj_t BgL_arg1786z00_2017;

									BgL_arg1786z00_2017 =
										MAKE_YOUNG_PAIR(BgL_arg1781z00_2013, BNIL);
									BgL_arg1785z00_2016 =
										MAKE_YOUNG_PAIR(BgL_arg1779z00_2012, BgL_arg1786z00_2017);
								}
								BgL_arg1783z00_2015 =
									MAKE_YOUNG_PAIR(BgL_arg1777z00_2011, BgL_arg1785z00_2016);
							}
							BgL_list1782z00_2014 =
								MAKE_YOUNG_PAIR(BgL_arg1775z00_2010, BgL_arg1783z00_2015);
						}
						BgL_addrz00_2006 =
							BGl_formatz00zz__r4_output_6_10_3z00(BGl_string2527z00zz__ftpz00,
							BgL_list1782z00_2014);
					}
				}
				{	/* Unsafe/ftp.scm 468 */
					obj_t BgL_a1245z00_2018;

					{	/* Unsafe/ftp.scm 468 */
						obj_t BgL_a1243z00_2021;

						BgL_a1243z00_2021 = bgl_list_ref(BgL_plistz00_32, 4L);
						{	/* Unsafe/ftp.scm 468 */

							if (INTEGERP(BgL_a1243z00_2021))
								{	/* Unsafe/ftp.scm 468 */
									BgL_a1245z00_2018 =
										BINT(((long) CINT(BgL_a1243z00_2021) * 256L));
								}
							else
								{	/* Unsafe/ftp.scm 468 */
									BgL_a1245z00_2018 =
										BGl_2za2za2zz__r4_numbers_6_5z00(BgL_a1243z00_2021,
										BINT(256L));
								}
						}
					}
					{	/* Unsafe/ftp.scm 468 */
						obj_t BgL_b1246z00_2019;

						BgL_b1246z00_2019 = bgl_list_ref(BgL_plistz00_32, 5L);
						{	/* Unsafe/ftp.scm 468 */

							{	/* Unsafe/ftp.scm 468 */
								bool_t BgL_test2972z00_5381;

								if (INTEGERP(BgL_a1245z00_2018))
									{	/* Unsafe/ftp.scm 468 */
										BgL_test2972z00_5381 = INTEGERP(BgL_b1246z00_2019);
									}
								else
									{	/* Unsafe/ftp.scm 468 */
										BgL_test2972z00_5381 = ((bool_t) 0);
									}
								if (BgL_test2972z00_5381)
									{	/* Unsafe/ftp.scm 468 */
										BgL_portz00_2007 =
											ADDFX(BgL_a1245z00_2018, BgL_b1246z00_2019);
									}
								else
									{	/* Unsafe/ftp.scm 468 */
										BgL_portz00_2007 =
											BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_a1245z00_2018,
											BgL_b1246z00_2019);
									}
							}
						}
					}
				}
				{	/* Unsafe/ftp.scm 469 */
					int BgL_tmpz00_5387;

					BgL_tmpz00_5387 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5387);
				}
				{	/* Unsafe/ftp.scm 469 */
					int BgL_tmpz00_5390;

					BgL_tmpz00_5390 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_5390, BgL_portz00_2007);
				}
				return BgL_addrz00_2006;
			}
		}

	}



/* ftp-logout */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2logoutzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_33)
	{
		{	/* Unsafe/ftp.scm 474 */
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_33), BGl_string2528z00zz__ftpz00, BNIL));
		}

	}



/* &ftp-logout */
	obj_t BGl_z62ftpzd2logoutzb0zz__ftpz00(obj_t BgL_envz00_3909,
		obj_t BgL_ftpz00_3910)
	{
		{	/* Unsafe/ftp.scm 474 */
			{	/* Unsafe/ftp.scm 475 */
				bool_t BgL_tmpz00_5396;

				{	/* Unsafe/ftp.scm 475 */
					BgL_ftpz00_bglt BgL_auxz00_5397;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3910, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 475 */
							BgL_auxz00_5397 = ((BgL_ftpz00_bglt) BgL_ftpz00_3910);
						}
					else
						{
							obj_t BgL_auxz00_5401;

							BgL_auxz00_5401 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(16109L), BGl_string2529z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3910);
							FAILURE(BgL_auxz00_5401, BFALSE, BFALSE);
						}
					BgL_tmpz00_5396 = BGl_ftpzd2logoutzd2zz__ftpz00(BgL_auxz00_5397);
				}
				return BBOOL(BgL_tmpz00_5396);
			}
		}

	}



/* ftp-cd */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2cdzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_34, obj_t BgL_directoryz00_35)
	{
		{	/* Unsafe/ftp.scm 480 */
			{	/* Unsafe/ftp.scm 481 */
				obj_t BgL_list1790z00_3376;

				BgL_list1790z00_3376 = MAKE_YOUNG_PAIR(BgL_directoryz00_35, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_34), BGl_string2530z00zz__ftpz00,
						BgL_list1790z00_3376));
			}
		}

	}



/* &ftp-cd */
	obj_t BGl_z62ftpzd2cdzb0zz__ftpz00(obj_t BgL_envz00_3911,
		obj_t BgL_ftpz00_3912, obj_t BgL_directoryz00_3913)
	{
		{	/* Unsafe/ftp.scm 480 */
			{	/* Unsafe/ftp.scm 481 */
				bool_t BgL_tmpz00_5411;

				{	/* Unsafe/ftp.scm 481 */
					obj_t BgL_auxz00_5420;
					BgL_ftpz00_bglt BgL_auxz00_5412;

					if (STRINGP(BgL_directoryz00_3913))
						{	/* Unsafe/ftp.scm 481 */
							BgL_auxz00_5420 = BgL_directoryz00_3913;
						}
					else
						{
							obj_t BgL_auxz00_5423;

							BgL_auxz00_5423 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(16396L), BGl_string2531z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_directoryz00_3913);
							FAILURE(BgL_auxz00_5423, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3912, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 481 */
							BgL_auxz00_5412 = ((BgL_ftpz00_bglt) BgL_ftpz00_3912);
						}
					else
						{
							obj_t BgL_auxz00_5416;

							BgL_auxz00_5416 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(16396L), BGl_string2531z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3912);
							FAILURE(BgL_auxz00_5416, BFALSE, BFALSE);
						}
					BgL_tmpz00_5411 =
						BGl_ftpzd2cdzd2zz__ftpz00(BgL_auxz00_5412, BgL_auxz00_5420);
				}
				return BBOOL(BgL_tmpz00_5411);
			}
		}

	}



/* ftp-cd-parent */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2cdzd2parentz00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_36)
	{
		{	/* Unsafe/ftp.scm 486 */
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_36), BGl_string2533z00zz__ftpz00, BNIL));
		}

	}



/* &ftp-cd-parent */
	obj_t BGl_z62ftpzd2cdzd2parentz62zz__ftpz00(obj_t BgL_envz00_3914,
		obj_t BgL_ftpz00_3915)
	{
		{	/* Unsafe/ftp.scm 486 */
			{	/* Unsafe/ftp.scm 487 */
				bool_t BgL_tmpz00_5432;

				{	/* Unsafe/ftp.scm 487 */
					BgL_ftpz00_bglt BgL_auxz00_5433;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3915, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 487 */
							BgL_auxz00_5433 = ((BgL_ftpz00_bglt) BgL_ftpz00_3915);
						}
					else
						{
							obj_t BgL_auxz00_5437;

							BgL_auxz00_5437 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(16689L), BGl_string2534z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3915);
							FAILURE(BgL_auxz00_5437, BFALSE, BFALSE);
						}
					BgL_tmpz00_5432 = BGl_ftpzd2cdzd2parentz00zz__ftpz00(BgL_auxz00_5433);
				}
				return BBOOL(BgL_tmpz00_5432);
			}
		}

	}



/* ftp-mount */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2mountzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_37, obj_t BgL_mountzd2pointzd2_38)
	{
		{	/* Unsafe/ftp.scm 492 */
			{	/* Unsafe/ftp.scm 493 */
				obj_t BgL_list1792z00_3378;

				BgL_list1792z00_3378 = MAKE_YOUNG_PAIR(BgL_mountzd2pointzd2_38, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_37), BGl_string2535z00zz__ftpz00,
						BgL_list1792z00_3378));
			}
		}

	}



/* &ftp-mount */
	obj_t BGl_z62ftpzd2mountzb0zz__ftpz00(obj_t BgL_envz00_3916,
		obj_t BgL_ftpz00_3917, obj_t BgL_mountzd2pointzd2_3918)
	{
		{	/* Unsafe/ftp.scm 492 */
			{	/* Unsafe/ftp.scm 493 */
				bool_t BgL_tmpz00_5447;

				{	/* Unsafe/ftp.scm 493 */
					obj_t BgL_auxz00_5456;
					BgL_ftpz00_bglt BgL_auxz00_5448;

					if (STRINGP(BgL_mountzd2pointzd2_3918))
						{	/* Unsafe/ftp.scm 493 */
							BgL_auxz00_5456 = BgL_mountzd2pointzd2_3918;
						}
					else
						{
							obj_t BgL_auxz00_5459;

							BgL_auxz00_5459 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(16981L), BGl_string2536z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_mountzd2pointzd2_3918);
							FAILURE(BgL_auxz00_5459, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3917, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 493 */
							BgL_auxz00_5448 = ((BgL_ftpz00_bglt) BgL_ftpz00_3917);
						}
					else
						{
							obj_t BgL_auxz00_5452;

							BgL_auxz00_5452 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(16981L), BGl_string2536z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3917);
							FAILURE(BgL_auxz00_5452, BFALSE, BFALSE);
						}
					BgL_tmpz00_5447 =
						BGl_ftpzd2mountzd2zz__ftpz00(BgL_auxz00_5448, BgL_auxz00_5456);
				}
				return BBOOL(BgL_tmpz00_5447);
			}
		}

	}



/* ftp-reinitialize */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2reinitializa7ez75zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_39)
	{
		{	/* Unsafe/ftp.scm 498 */
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_39), BGl_string2537z00zz__ftpz00, BNIL));
		}

	}



/* &ftp-reinitialize */
	obj_t BGl_z62ftpzd2reinitializa7ez17zz__ftpz00(obj_t BgL_envz00_3919,
		obj_t BgL_ftpz00_3920)
	{
		{	/* Unsafe/ftp.scm 498 */
			{	/* Unsafe/ftp.scm 499 */
				bool_t BgL_tmpz00_5468;

				{	/* Unsafe/ftp.scm 499 */
					BgL_ftpz00_bglt BgL_auxz00_5469;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3920, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 499 */
							BgL_auxz00_5469 = ((BgL_ftpz00_bglt) BgL_ftpz00_3920);
						}
					else
						{
							obj_t BgL_auxz00_5473;

							BgL_auxz00_5473 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(17280L), BGl_string2538z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3920);
							FAILURE(BgL_auxz00_5473, BFALSE, BFALSE);
						}
					BgL_tmpz00_5468 =
						BGl_ftpzd2reinitializa7ez75zz__ftpz00(BgL_auxz00_5469);
				}
				return BBOOL(BgL_tmpz00_5468);
			}
		}

	}



/* ftp-data-port */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2datazd2portz00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_40, obj_t BgL_hostz00_41, obj_t BgL_portz00_42)
	{
		{	/* Unsafe/ftp.scm 504 */
			{	/* Unsafe/ftp.scm 505 */
				BgL_z62ftpzd2errorzb0_bglt BgL_arg1794z00_2029;

				{	/* Unsafe/ftp.scm 505 */
					BgL_z62ftpzd2errorzb0_bglt BgL_new1135z00_2030;

					{	/* Unsafe/ftp.scm 505 */
						BgL_z62ftpzd2errorzb0_bglt BgL_new1134z00_2033;

						BgL_new1134z00_2033 =
							((BgL_z62ftpzd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_z62ftpzd2errorzb0_bgl))));
						{	/* Unsafe/ftp.scm 505 */
							long BgL_arg1797z00_2034;

							BgL_arg1797z00_2034 =
								BGL_CLASS_NUM(BGl_z62ftpzd2errorzb0zz__ftpz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1134z00_2033),
								BgL_arg1797z00_2034);
						}
						BgL_new1135z00_2030 = BgL_new1134z00_2033;
					}
					((((BgL_z62exceptionz62_bglt) COBJECT(
									((BgL_z62exceptionz62_bglt) BgL_new1135z00_2030)))->
							BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
					((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
										BgL_new1135z00_2030)))->BgL_locationz00) =
						((obj_t) BFALSE), BUNSPEC);
					{
						obj_t BgL_auxz00_5487;

						{	/* Unsafe/ftp.scm 505 */
							obj_t BgL_arg1795z00_2031;

							{	/* Unsafe/ftp.scm 505 */
								obj_t BgL_arg1796z00_2032;

								{	/* Unsafe/ftp.scm 505 */
									obj_t BgL_classz00_3383;

									BgL_classz00_3383 = BGl_z62ftpzd2errorzb0zz__ftpz00;
									BgL_arg1796z00_2032 = BGL_CLASS_ALL_FIELDS(BgL_classz00_3383);
								}
								BgL_arg1795z00_2031 = VECTOR_REF(BgL_arg1796z00_2032, 2L);
							}
							BgL_auxz00_5487 =
								BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1795z00_2031);
						}
						((((BgL_z62exceptionz62_bglt) COBJECT(
										((BgL_z62exceptionz62_bglt) BgL_new1135z00_2030)))->
								BgL_stackz00) = ((obj_t) BgL_auxz00_5487), BUNSPEC);
					}
					((((BgL_z62errorz62_bglt) COBJECT(
									((BgL_z62errorz62_bglt) BgL_new1135z00_2030)))->BgL_procz00) =
						((obj_t) BGl_string2525z00zz__ftpz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
										BgL_new1135z00_2030)))->BgL_msgz00) =
						((obj_t) BGl_string2539z00zz__ftpz00), BUNSPEC);
					((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
										BgL_new1135z00_2030)))->BgL_objz00) =
						((obj_t) ((obj_t) BgL_ftpz00_40)), BUNSPEC);
					BgL_arg1794z00_2029 = BgL_new1135z00_2030;
				}
				return CBOOL(BGl_raisez00zz__errorz00(((obj_t) BgL_arg1794z00_2029)));
			}
		}

	}



/* &ftp-data-port */
	obj_t BGl_z62ftpzd2datazd2portz62zz__ftpz00(obj_t BgL_envz00_3921,
		obj_t BgL_ftpz00_3922, obj_t BgL_hostz00_3923, obj_t BgL_portz00_3924)
	{
		{	/* Unsafe/ftp.scm 504 */
			{	/* Unsafe/ftp.scm 505 */
				bool_t BgL_tmpz00_5503;

				{	/* Unsafe/ftp.scm 505 */
					obj_t BgL_auxz00_5519;
					obj_t BgL_auxz00_5512;
					BgL_ftpz00_bglt BgL_auxz00_5504;

					if (INTEGERP(BgL_portz00_3924))
						{	/* Unsafe/ftp.scm 505 */
							BgL_auxz00_5519 = BgL_portz00_3924;
						}
					else
						{
							obj_t BgL_auxz00_5522;

							BgL_auxz00_5522 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(17581L), BGl_string2540z00zz__ftpz00,
								BGl_string2541z00zz__ftpz00, BgL_portz00_3924);
							FAILURE(BgL_auxz00_5522, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_hostz00_3923))
						{	/* Unsafe/ftp.scm 505 */
							BgL_auxz00_5512 = BgL_hostz00_3923;
						}
					else
						{
							obj_t BgL_auxz00_5515;

							BgL_auxz00_5515 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(17581L), BGl_string2540z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_hostz00_3923);
							FAILURE(BgL_auxz00_5515, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3922, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 505 */
							BgL_auxz00_5504 = ((BgL_ftpz00_bglt) BgL_ftpz00_3922);
						}
					else
						{
							obj_t BgL_auxz00_5508;

							BgL_auxz00_5508 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(17581L), BGl_string2540z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3922);
							FAILURE(BgL_auxz00_5508, BFALSE, BFALSE);
						}
					BgL_tmpz00_5503 =
						BGl_ftpzd2datazd2portz00zz__ftpz00(BgL_auxz00_5504, BgL_auxz00_5512,
						BgL_auxz00_5519);
				}
				return BBOOL(BgL_tmpz00_5503);
			}
		}

	}



/* ftp-passive */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2passivezd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_43)
	{
		{	/* Unsafe/ftp.scm 513 */
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_43), BGl_string2524z00zz__ftpz00, BNIL));
		}

	}



/* &ftp-passive */
	obj_t BGl_z62ftpzd2passivezb0zz__ftpz00(obj_t BgL_envz00_3925,
		obj_t BgL_ftpz00_3926)
	{
		{	/* Unsafe/ftp.scm 513 */
			{	/* Unsafe/ftp.scm 514 */
				bool_t BgL_tmpz00_5531;

				{	/* Unsafe/ftp.scm 514 */
					BgL_ftpz00_bglt BgL_auxz00_5532;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3926, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 514 */
							BgL_auxz00_5532 = ((BgL_ftpz00_bglt) BgL_ftpz00_3926);
						}
					else
						{
							obj_t BgL_auxz00_5536;

							BgL_auxz00_5536 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(17960L), BGl_string2542z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3926);
							FAILURE(BgL_auxz00_5536, BFALSE, BFALSE);
						}
					BgL_tmpz00_5531 = BGl_ftpzd2passivezd2zz__ftpz00(BgL_auxz00_5532);
				}
				return BBOOL(BgL_tmpz00_5531);
			}
		}

	}



/* ftp-data-type */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2datazd2typez00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_44, obj_t BgL_typez00_45, obj_t BgL_localzd2bytezd2siza7eza7_46)
	{
		{	/* Unsafe/ftp.scm 519 */
			{

				{	/* Unsafe/ftp.scm 520 */
					unsigned char BgL_aux1139z00_2037;

					{	/* Unsafe/ftp.scm 520 */
						obj_t BgL_arg1802z00_2041;

						BgL_arg1802z00_2041 = SYMBOL_TO_STRING(BgL_typez00_45);
						BgL_aux1139z00_2037 = STRING_REF(BgL_arg1802z00_2041, 0L);
					}
					switch (BgL_aux1139z00_2037)
						{
						case ((unsigned char) 'a'):
						case ((unsigned char) 'A'):

							{	/* Unsafe/ftp.scm 522 */
								obj_t BgL_list1800z00_2039;

								BgL_list1800z00_2039 =
									MAKE_YOUNG_PAIR(BGl_string2545z00zz__ftpz00, BNIL);
								return
									CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
										((obj_t) BgL_ftpz00_44), BGl_string2546z00zz__ftpz00,
										BgL_list1800z00_2039));
							}
							break;
						case ((unsigned char) 'i'):
						case ((unsigned char) 'I'):

							{	/* Unsafe/ftp.scm 524 */
								obj_t BgL_list1801z00_2040;

								BgL_list1801z00_2040 =
									MAKE_YOUNG_PAIR(BGl_string2547z00zz__ftpz00, BNIL);
								return
									CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
										((obj_t) BgL_ftpz00_44), BGl_string2546z00zz__ftpz00,
										BgL_list1801z00_2040));
							}
							break;
						default:
							{	/* Unsafe/ftp.scm 520 */
								obj_t BgL_tmpz00_5552;

								{	/* Unsafe/ftp.scm 526 */
									BgL_z62ftpzd2parsezd2errorz62_bglt BgL_arg1803z00_2042;

									{	/* Unsafe/ftp.scm 526 */
										BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1137z00_2043;

										{	/* Unsafe/ftp.scm 526 */
											BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1136z00_2046;

											BgL_new1136z00_2046 =
												((BgL_z62ftpzd2parsezd2errorz62_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_z62ftpzd2parsezd2errorz62_bgl))));
											{	/* Unsafe/ftp.scm 526 */
												long BgL_arg1806z00_2047;

												BgL_arg1806z00_2047 =
													BGL_CLASS_NUM
													(BGl_z62ftpzd2parsezd2errorz62zz__ftpz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1136z00_2046), BgL_arg1806z00_2047);
											}
											BgL_new1137z00_2043 = BgL_new1136z00_2046;
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1137z00_2043)))->
												BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_z62exceptionz62_bglt)
													COBJECT(((BgL_z62exceptionz62_bglt)
															BgL_new1137z00_2043)))->BgL_locationz00) =
											((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_5561;

											{	/* Unsafe/ftp.scm 526 */
												obj_t BgL_arg1804z00_2044;

												{	/* Unsafe/ftp.scm 526 */
													obj_t BgL_arg1805z00_2045;

													{	/* Unsafe/ftp.scm 526 */
														obj_t BgL_classz00_3389;

														BgL_classz00_3389 =
															BGl_z62ftpzd2parsezd2errorz62zz__ftpz00;
														BgL_arg1805z00_2045 =
															BGL_CLASS_ALL_FIELDS(BgL_classz00_3389);
													}
													BgL_arg1804z00_2044 =
														VECTOR_REF(BgL_arg1805z00_2045, 2L);
												}
												BgL_auxz00_5561 =
													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
													(BgL_arg1804z00_2044);
											}
											((((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																BgL_new1137z00_2043)))->BgL_stackz00) =
												((obj_t) BgL_auxz00_5561), BUNSPEC);
										}
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_new1137z00_2043)))->
												BgL_procz00) =
											((obj_t) BGl_string2543z00zz__ftpz00), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1137z00_2043)))->BgL_msgz00) =
											((obj_t) BGl_string2544z00zz__ftpz00), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1137z00_2043)))->BgL_objz00) =
											((obj_t) BgL_typez00_45), BUNSPEC);
										BgL_arg1803z00_2042 = BgL_new1137z00_2043;
									}
									BgL_tmpz00_5552 =
										BGl_raisez00zz__errorz00(((obj_t) BgL_arg1803z00_2042));
								}
								return CBOOL(BgL_tmpz00_5552);
							}
						}
				}
			}
		}

	}



/* &ftp-data-type */
	obj_t BGl_z62ftpzd2datazd2typez62zz__ftpz00(obj_t BgL_envz00_3927,
		obj_t BgL_ftpz00_3928, obj_t BgL_typez00_3929,
		obj_t BgL_localzd2bytezd2siza7eza7_3930)
	{
		{	/* Unsafe/ftp.scm 519 */
			{	/* Unsafe/ftp.scm 520 */
				bool_t BgL_tmpz00_5577;

				{	/* Unsafe/ftp.scm 520 */
					obj_t BgL_auxz00_5586;
					BgL_ftpz00_bglt BgL_auxz00_5578;

					if (SYMBOLP(BgL_typez00_3929))
						{	/* Unsafe/ftp.scm 520 */
							BgL_auxz00_5586 = BgL_typez00_3929;
						}
					else
						{
							obj_t BgL_auxz00_5589;

							BgL_auxz00_5589 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(18267L), BGl_string2548z00zz__ftpz00,
								BGl_string2549z00zz__ftpz00, BgL_typez00_3929);
							FAILURE(BgL_auxz00_5589, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3928, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 520 */
							BgL_auxz00_5578 = ((BgL_ftpz00_bglt) BgL_ftpz00_3928);
						}
					else
						{
							obj_t BgL_auxz00_5582;

							BgL_auxz00_5582 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(18267L), BGl_string2548z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3928);
							FAILURE(BgL_auxz00_5582, BFALSE, BFALSE);
						}
					BgL_tmpz00_5577 =
						BGl_ftpzd2datazd2typez00zz__ftpz00(BgL_auxz00_5578, BgL_auxz00_5586,
						BgL_localzd2bytezd2siza7eza7_3930);
				}
				return BBOOL(BgL_tmpz00_5577);
			}
		}

	}



/* ftp-file-structure */
	BGL_EXPORTED_DEF bool_t
		BGl_ftpzd2filezd2structurez00zz__ftpz00(BgL_ftpz00_bglt BgL_ftpz00_47,
		obj_t BgL_structurez00_48)
	{
		{	/* Unsafe/ftp.scm 534 */
			{	/* Unsafe/ftp.scm 535 */
				obj_t BgL_list1807z00_3393;

				BgL_list1807z00_3393 =
					MAKE_YOUNG_PAIR(BGl_string2550z00zz__ftpz00, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_47), BGl_string2551z00zz__ftpz00,
						BgL_list1807z00_3393));
			}
		}

	}



/* &ftp-file-structure */
	obj_t BGl_z62ftpzd2filezd2structurez62zz__ftpz00(obj_t BgL_envz00_3931,
		obj_t BgL_ftpz00_3932, obj_t BgL_structurez00_3933)
	{
		{	/* Unsafe/ftp.scm 534 */
			{	/* Unsafe/ftp.scm 535 */
				bool_t BgL_tmpz00_5599;

				{	/* Unsafe/ftp.scm 535 */
					BgL_ftpz00_bglt BgL_auxz00_5600;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3932, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 535 */
							BgL_auxz00_5600 = ((BgL_ftpz00_bglt) BgL_ftpz00_3932);
						}
					else
						{
							obj_t BgL_auxz00_5604;

							BgL_auxz00_5604 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(18864L), BGl_string2552z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3932);
							FAILURE(BgL_auxz00_5604, BFALSE, BFALSE);
						}
					BgL_tmpz00_5599 =
						BGl_ftpzd2filezd2structurez00zz__ftpz00(BgL_auxz00_5600,
						BgL_structurez00_3933);
				}
				return BBOOL(BgL_tmpz00_5599);
			}
		}

	}



/* ftp-transfer-mode */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2transferzd2modez00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_49, obj_t BgL_modez00_50)
	{
		{	/* Unsafe/ftp.scm 540 */
			{	/* Unsafe/ftp.scm 541 */
				obj_t BgL_list1808z00_3394;

				BgL_list1808z00_3394 =
					MAKE_YOUNG_PAIR(BGl_string2553z00zz__ftpz00, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_49), BGl_string2554z00zz__ftpz00,
						BgL_list1808z00_3394));
			}
		}

	}



/* &ftp-transfer-mode */
	obj_t BGl_z62ftpzd2transferzd2modez62zz__ftpz00(obj_t BgL_envz00_3934,
		obj_t BgL_ftpz00_3935, obj_t BgL_modez00_3936)
	{
		{	/* Unsafe/ftp.scm 540 */
			{	/* Unsafe/ftp.scm 541 */
				bool_t BgL_tmpz00_5614;

				{	/* Unsafe/ftp.scm 541 */
					BgL_ftpz00_bglt BgL_auxz00_5615;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3935, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 541 */
							BgL_auxz00_5615 = ((BgL_ftpz00_bglt) BgL_ftpz00_3935);
						}
					else
						{
							obj_t BgL_auxz00_5619;

							BgL_auxz00_5619 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(19163L), BGl_string2555z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3935);
							FAILURE(BgL_auxz00_5619, BFALSE, BFALSE);
						}
					BgL_tmpz00_5614 =
						BGl_ftpzd2transferzd2modez00zz__ftpz00(BgL_auxz00_5615,
						BgL_modez00_3936);
				}
				return BBOOL(BgL_tmpz00_5614);
			}
		}

	}



/* ftp-retrieve */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2retrievezd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_51, obj_t BgL_filenamez00_52)
	{
		{	/* Unsafe/ftp.scm 546 */
			{	/* Unsafe/ftp.scm 547 */
				obj_t BgL_list1809z00_3395;

				BgL_list1809z00_3395 = MAKE_YOUNG_PAIR(BgL_filenamez00_52, BNIL);
				return
					BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_51), BGl_string2500z00zz__ftpz00,
					BgL_list1809z00_3395);
			}
		}

	}



/* &ftp-retrieve */
	obj_t BGl_z62ftpzd2retrievezb0zz__ftpz00(obj_t BgL_envz00_3937,
		obj_t BgL_ftpz00_3938, obj_t BgL_filenamez00_3939)
	{
		{	/* Unsafe/ftp.scm 546 */
			{	/* Unsafe/ftp.scm 547 */
				obj_t BgL_auxz00_5636;
				BgL_ftpz00_bglt BgL_auxz00_5628;

				if (STRINGP(BgL_filenamez00_3939))
					{	/* Unsafe/ftp.scm 547 */
						BgL_auxz00_5636 = BgL_filenamez00_3939;
					}
				else
					{
						obj_t BgL_auxz00_5639;

						BgL_auxz00_5639 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(19459L), BGl_string2556z00zz__ftpz00,
							BGl_string2532z00zz__ftpz00, BgL_filenamez00_3939);
						FAILURE(BgL_auxz00_5639, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3938, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 547 */
						BgL_auxz00_5628 = ((BgL_ftpz00_bglt) BgL_ftpz00_3938);
					}
				else
					{
						obj_t BgL_auxz00_5632;

						BgL_auxz00_5632 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(19459L), BGl_string2556z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3938);
						FAILURE(BgL_auxz00_5632, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2retrievezd2zz__ftpz00(BgL_auxz00_5628, BgL_auxz00_5636);
			}
		}

	}



/* ftp-store */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2storezd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_53, obj_t BgL_sourcez00_54, obj_t BgL_destinationz00_55)
	{
		{	/* Unsafe/ftp.scm 552 */
			{	/* Unsafe/ftp.scm 554 */
				obj_t BgL_opz00_2052;

				{	/* Unsafe/ftp.scm 554 */
					obj_t BgL_arg1813z00_2058;

					BgL_arg1813z00_2058 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_53)))->BgL_dtpz00);
					{	/* Unsafe/ftp.scm 554 */
						obj_t BgL_tmpz00_5646;

						BgL_tmpz00_5646 = ((obj_t) BgL_arg1813z00_2058);
						BgL_opz00_2052 = SOCKET_OUTPUT(BgL_tmpz00_5646);
					}
				}
				if (fexists(BSTRING_TO_STRING(BgL_sourcez00_54)))
					{	/* Unsafe/ftp.scm 556 */
						obj_t BgL__andtest_1142z00_2054;

						if (CBOOL(BgL_destinationz00_55))
							{	/* Unsafe/ftp.scm 557 */
								obj_t BgL_list1811z00_2056;

								BgL_list1811z00_2056 =
									MAKE_YOUNG_PAIR(BgL_destinationz00_55, BNIL);
								BgL__andtest_1142z00_2054 =
									BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
									((obj_t) BgL_ftpz00_53), BGl_string2557z00zz__ftpz00,
									BgL_list1811z00_2056);
							}
						else
							{	/* Unsafe/ftp.scm 556 */
								BgL__andtest_1142z00_2054 =
									BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
									((obj_t) BgL_ftpz00_53), BGl_string2558z00zz__ftpz00, BNIL);
							}
						if (CBOOL(BgL__andtest_1142z00_2054))
							{	/* Unsafe/ftp.scm 559 */
								long BgL_arg1810z00_2055;

								BgL_arg1810z00_2055 =
									bgl_file_size(BSTRING_TO_STRING(BgL_sourcez00_54));
								{	/* Unsafe/ftp.scm 559 */
									long BgL_xz00_4231;

									BgL_xz00_4231 =
										BGl_sendzd2filezd2zz__r4_input_6_10_2z00(BgL_sourcez00_54,
										BgL_opz00_2052, BgL_arg1810z00_2055, ((long) 0));
									return ((bool_t) 1);
								}
							}
						else
							{	/* Unsafe/ftp.scm 556 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Unsafe/ftp.scm 555 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &ftp-store */
	obj_t BGl_z62ftpzd2storezb0zz__ftpz00(obj_t BgL_envz00_3940,
		obj_t BgL_ftpz00_3941, obj_t BgL_sourcez00_3942,
		obj_t BgL_destinationz00_3943)
	{
		{	/* Unsafe/ftp.scm 552 */
			{	/* Unsafe/ftp.scm 553 */
				bool_t BgL_tmpz00_5664;

				{	/* Unsafe/ftp.scm 553 */
					obj_t BgL_auxz00_5673;
					BgL_ftpz00_bglt BgL_auxz00_5665;

					if (STRINGP(BgL_sourcez00_3942))
						{	/* Unsafe/ftp.scm 553 */
							BgL_auxz00_5673 = BgL_sourcez00_3942;
						}
					else
						{
							obj_t BgL_auxz00_5676;

							BgL_auxz00_5676 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(19767L), BGl_string2559z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_sourcez00_3942);
							FAILURE(BgL_auxz00_5676, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3941, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 553 */
							BgL_auxz00_5665 = ((BgL_ftpz00_bglt) BgL_ftpz00_3941);
						}
					else
						{
							obj_t BgL_auxz00_5669;

							BgL_auxz00_5669 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(19767L), BGl_string2559z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3941);
							FAILURE(BgL_auxz00_5669, BFALSE, BFALSE);
						}
					BgL_tmpz00_5664 =
						BGl_ftpzd2storezd2zz__ftpz00(BgL_auxz00_5665, BgL_auxz00_5673,
						BgL_destinationz00_3943);
				}
				return BBOOL(BgL_tmpz00_5664);
			}
		}

	}



/* ftp-append */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2appendzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_56, obj_t BgL_sourcez00_57, obj_t BgL_destinationz00_58)
	{
		{	/* Unsafe/ftp.scm 564 */
			{	/* Unsafe/ftp.scm 566 */
				obj_t BgL_opz00_3400;

				{	/* Unsafe/ftp.scm 566 */
					obj_t BgL_arg1817z00_3401;

					BgL_arg1817z00_3401 =
						(((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_ftpz00_56)))->BgL_dtpz00);
					{	/* Unsafe/ftp.scm 566 */
						obj_t BgL_tmpz00_5684;

						BgL_tmpz00_5684 = ((obj_t) BgL_arg1817z00_3401);
						BgL_opz00_3400 = SOCKET_OUTPUT(BgL_tmpz00_5684);
					}
				}
				if (fexists(BSTRING_TO_STRING(BgL_sourcez00_57)))
					{	/* Unsafe/ftp.scm 568 */
						obj_t BgL__andtest_1145z00_3403;

						{	/* Unsafe/ftp.scm 568 */
							obj_t BgL_list1815z00_3404;

							{	/* Unsafe/ftp.scm 568 */
								obj_t BgL_arg1816z00_3405;

								BgL_arg1816z00_3405 =
									MAKE_YOUNG_PAIR(BgL_destinationz00_58, BNIL);
								BgL_list1815z00_3404 =
									MAKE_YOUNG_PAIR(BgL_sourcez00_57, BgL_arg1816z00_3405);
							}
							BgL__andtest_1145z00_3403 =
								BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
								((obj_t) BgL_ftpz00_56), BGl_string2560z00zz__ftpz00,
								BgL_list1815z00_3404);
						}
						if (CBOOL(BgL__andtest_1145z00_3403))
							{	/* Unsafe/ftp.scm 569 */
								long BgL_arg1814z00_3406;

								BgL_arg1814z00_3406 =
									bgl_file_size(BSTRING_TO_STRING(BgL_sourcez00_57));
								{	/* Unsafe/ftp.scm 569 */
									long BgL_xz00_4238;

									BgL_xz00_4238 =
										BGl_sendzd2filezd2zz__r4_input_6_10_2z00(BgL_sourcez00_57,
										BgL_opz00_3400, BgL_arg1814z00_3406, ((long) 0));
									return ((bool_t) 1);
								}
							}
						else
							{	/* Unsafe/ftp.scm 568 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Unsafe/ftp.scm 567 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &ftp-append */
	obj_t BGl_z62ftpzd2appendzb0zz__ftpz00(obj_t BgL_envz00_3944,
		obj_t BgL_ftpz00_3945, obj_t BgL_sourcez00_3946,
		obj_t BgL_destinationz00_3947)
	{
		{	/* Unsafe/ftp.scm 564 */
			{	/* Unsafe/ftp.scm 565 */
				bool_t BgL_tmpz00_5699;

				{	/* Unsafe/ftp.scm 565 */
					obj_t BgL_auxz00_5715;
					obj_t BgL_auxz00_5708;
					BgL_ftpz00_bglt BgL_auxz00_5700;

					if (STRINGP(BgL_destinationz00_3947))
						{	/* Unsafe/ftp.scm 565 */
							BgL_auxz00_5715 = BgL_destinationz00_3947;
						}
					else
						{
							obj_t BgL_auxz00_5718;

							BgL_auxz00_5718 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(20291L), BGl_string2561z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_destinationz00_3947);
							FAILURE(BgL_auxz00_5718, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_sourcez00_3946))
						{	/* Unsafe/ftp.scm 565 */
							BgL_auxz00_5708 = BgL_sourcez00_3946;
						}
					else
						{
							obj_t BgL_auxz00_5711;

							BgL_auxz00_5711 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(20291L), BGl_string2561z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_sourcez00_3946);
							FAILURE(BgL_auxz00_5711, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3945, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 565 */
							BgL_auxz00_5700 = ((BgL_ftpz00_bglt) BgL_ftpz00_3945);
						}
					else
						{
							obj_t BgL_auxz00_5704;

							BgL_auxz00_5704 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(20291L), BGl_string2561z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3945);
							FAILURE(BgL_auxz00_5704, BFALSE, BFALSE);
						}
					BgL_tmpz00_5699 =
						BGl_ftpzd2appendzd2zz__ftpz00(BgL_auxz00_5700, BgL_auxz00_5708,
						BgL_auxz00_5715);
				}
				return BBOOL(BgL_tmpz00_5699);
			}
		}

	}



/* ftp-allocate */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2allocatezd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_59, obj_t BgL_siza7eza7_60,
		obj_t BgL_pagezd2orzd2recordzd2siza7ez75_61)
	{
		{	/* Unsafe/ftp.scm 574 */
			{	/* Unsafe/ftp.scm 575 */
				obj_t BgL_prsiza7eza7_3410;

				if (PAIRP(BgL_pagezd2orzd2recordzd2siza7ez75_61))
					{	/* Unsafe/ftp.scm 575 */
						BgL_prsiza7eza7_3410 = CAR(BgL_pagezd2orzd2recordzd2siza7ez75_61);
					}
				else
					{	/* Unsafe/ftp.scm 575 */
						BgL_prsiza7eza7_3410 = BFALSE;
					}
				if (CBOOL(BgL_prsiza7eza7_3410))
					{	/* Unsafe/ftp.scm 577 */
						obj_t BgL_list1818z00_3412;

						{	/* Unsafe/ftp.scm 577 */
							obj_t BgL_arg1819z00_3413;

							{	/* Unsafe/ftp.scm 577 */
								obj_t BgL_arg1820z00_3414;

								BgL_arg1820z00_3414 =
									MAKE_YOUNG_PAIR(BgL_prsiza7eza7_3410, BNIL);
								BgL_arg1819z00_3413 =
									MAKE_YOUNG_PAIR(BGl_string2562z00zz__ftpz00,
									BgL_arg1820z00_3414);
							}
							BgL_list1818z00_3412 =
								MAKE_YOUNG_PAIR(BgL_siza7eza7_60, BgL_arg1819z00_3413);
						}
						return
							CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
								((obj_t) BgL_ftpz00_59), BGl_string2563z00zz__ftpz00,
								BgL_list1818z00_3412));
					}
				else
					{	/* Unsafe/ftp.scm 578 */
						obj_t BgL_list1821z00_3415;

						BgL_list1821z00_3415 = MAKE_YOUNG_PAIR(BgL_siza7eza7_60, BNIL);
						return
							CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
								((obj_t) BgL_ftpz00_59), BGl_string2563z00zz__ftpz00,
								BgL_list1821z00_3415));
					}
			}
		}

	}



/* &ftp-allocate */
	obj_t BGl_z62ftpzd2allocatezb0zz__ftpz00(obj_t BgL_envz00_3948,
		obj_t BgL_ftpz00_3949, obj_t BgL_siza7eza7_3950,
		obj_t BgL_pagezd2orzd2recordzd2siza7ez75_3951)
	{
		{	/* Unsafe/ftp.scm 574 */
			{	/* Unsafe/ftp.scm 575 */
				bool_t BgL_tmpz00_5739;

				{	/* Unsafe/ftp.scm 575 */
					obj_t BgL_auxz00_5748;
					BgL_ftpz00_bglt BgL_auxz00_5740;

					if (INTEGERP(BgL_siza7eza7_3950))
						{	/* Unsafe/ftp.scm 575 */
							BgL_auxz00_5748 = BgL_siza7eza7_3950;
						}
					else
						{
							obj_t BgL_auxz00_5751;

							BgL_auxz00_5751 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(20778L), BGl_string2564z00zz__ftpz00,
								BGl_string2541z00zz__ftpz00, BgL_siza7eza7_3950);
							FAILURE(BgL_auxz00_5751, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3949, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 575 */
							BgL_auxz00_5740 = ((BgL_ftpz00_bglt) BgL_ftpz00_3949);
						}
					else
						{
							obj_t BgL_auxz00_5744;

							BgL_auxz00_5744 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(20778L), BGl_string2564z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3949);
							FAILURE(BgL_auxz00_5744, BFALSE, BFALSE);
						}
					BgL_tmpz00_5739 =
						BGl_ftpzd2allocatezd2zz__ftpz00(BgL_auxz00_5740, BgL_auxz00_5748,
						BgL_pagezd2orzd2recordzd2siza7ez75_3951);
				}
				return BBOOL(BgL_tmpz00_5739);
			}
		}

	}



/* ftp-restart */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2restartzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_62, obj_t BgL_positionz00_63)
	{
		{	/* Unsafe/ftp.scm 583 */
			{	/* Unsafe/ftp.scm 584 */
				obj_t BgL_list1822z00_3417;

				BgL_list1822z00_3417 = MAKE_YOUNG_PAIR(BgL_positionz00_63, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_62), BGl_string2565z00zz__ftpz00,
						BgL_list1822z00_3417));
			}
		}

	}



/* &ftp-restart */
	obj_t BGl_z62ftpzd2restartzb0zz__ftpz00(obj_t BgL_envz00_3952,
		obj_t BgL_ftpz00_3953, obj_t BgL_positionz00_3954)
	{
		{	/* Unsafe/ftp.scm 583 */
			{	/* Unsafe/ftp.scm 584 */
				bool_t BgL_tmpz00_5761;

				{	/* Unsafe/ftp.scm 584 */
					obj_t BgL_auxz00_5770;
					BgL_ftpz00_bglt BgL_auxz00_5762;

					if (INTEGERP(BgL_positionz00_3954))
						{	/* Unsafe/ftp.scm 584 */
							BgL_auxz00_5770 = BgL_positionz00_3954;
						}
					else
						{
							obj_t BgL_auxz00_5773;

							BgL_auxz00_5773 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(21220L), BGl_string2566z00zz__ftpz00,
								BGl_string2541z00zz__ftpz00, BgL_positionz00_3954);
							FAILURE(BgL_auxz00_5773, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3953, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 584 */
							BgL_auxz00_5762 = ((BgL_ftpz00_bglt) BgL_ftpz00_3953);
						}
					else
						{
							obj_t BgL_auxz00_5766;

							BgL_auxz00_5766 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(21220L), BGl_string2566z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3953);
							FAILURE(BgL_auxz00_5766, BFALSE, BFALSE);
						}
					BgL_tmpz00_5761 =
						BGl_ftpzd2restartzd2zz__ftpz00(BgL_auxz00_5762, BgL_auxz00_5770);
				}
				return BBOOL(BgL_tmpz00_5761);
			}
		}

	}



/* ftp-rename-file */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2renamezd2filez00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_64, obj_t BgL_fromz00_65, obj_t BgL_toz00_66)
	{
		{	/* Unsafe/ftp.scm 589 */
			{	/* Unsafe/ftp.scm 590 */
				obj_t BgL__andtest_1147z00_3418;

				{	/* Unsafe/ftp.scm 590 */
					obj_t BgL_list1824z00_3419;

					BgL_list1824z00_3419 = MAKE_YOUNG_PAIR(BgL_fromz00_65, BNIL);
					BgL__andtest_1147z00_3418 =
						BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_64), BGl_string2567z00zz__ftpz00,
						BgL_list1824z00_3419);
				}
				if (CBOOL(BgL__andtest_1147z00_3418))
					{	/* Unsafe/ftp.scm 591 */
						obj_t BgL_list1823z00_3420;

						BgL_list1823z00_3420 = MAKE_YOUNG_PAIR(BgL_toz00_66, BNIL);
						return
							CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
								((obj_t) BgL_ftpz00_64), BGl_string2568z00zz__ftpz00,
								BgL_list1823z00_3420));
					}
				else
					{	/* Unsafe/ftp.scm 590 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &ftp-rename-file */
	obj_t BGl_z62ftpzd2renamezd2filez62zz__ftpz00(obj_t BgL_envz00_3955,
		obj_t BgL_ftpz00_3956, obj_t BgL_fromz00_3957, obj_t BgL_toz00_3958)
	{
		{	/* Unsafe/ftp.scm 589 */
			{	/* Unsafe/ftp.scm 590 */
				bool_t BgL_tmpz00_5788;

				{	/* Unsafe/ftp.scm 590 */
					obj_t BgL_auxz00_5804;
					obj_t BgL_auxz00_5797;
					BgL_ftpz00_bglt BgL_auxz00_5789;

					if (STRINGP(BgL_toz00_3958))
						{	/* Unsafe/ftp.scm 590 */
							BgL_auxz00_5804 = BgL_toz00_3958;
						}
					else
						{
							obj_t BgL_auxz00_5807;

							BgL_auxz00_5807 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(21528L), BGl_string2569z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_toz00_3958);
							FAILURE(BgL_auxz00_5807, BFALSE, BFALSE);
						}
					if (STRINGP(BgL_fromz00_3957))
						{	/* Unsafe/ftp.scm 590 */
							BgL_auxz00_5797 = BgL_fromz00_3957;
						}
					else
						{
							obj_t BgL_auxz00_5800;

							BgL_auxz00_5800 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(21528L), BGl_string2569z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_fromz00_3957);
							FAILURE(BgL_auxz00_5800, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3956, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 590 */
							BgL_auxz00_5789 = ((BgL_ftpz00_bglt) BgL_ftpz00_3956);
						}
					else
						{
							obj_t BgL_auxz00_5793;

							BgL_auxz00_5793 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(21528L), BGl_string2569z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3956);
							FAILURE(BgL_auxz00_5793, BFALSE, BFALSE);
						}
					BgL_tmpz00_5788 =
						BGl_ftpzd2renamezd2filez00zz__ftpz00(BgL_auxz00_5789,
						BgL_auxz00_5797, BgL_auxz00_5804);
				}
				return BBOOL(BgL_tmpz00_5788);
			}
		}

	}



/* ftp-abort */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2abortzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_67)
	{
		{	/* Unsafe/ftp.scm 596 */
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_67), BGl_string2570z00zz__ftpz00, BNIL));
		}

	}



/* &ftp-abort */
	obj_t BGl_z62ftpzd2abortzb0zz__ftpz00(obj_t BgL_envz00_3959,
		obj_t BgL_ftpz00_3960)
	{
		{	/* Unsafe/ftp.scm 596 */
			{	/* Unsafe/ftp.scm 597 */
				bool_t BgL_tmpz00_5816;

				{	/* Unsafe/ftp.scm 597 */
					BgL_ftpz00_bglt BgL_auxz00_5817;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3960, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 597 */
							BgL_auxz00_5817 = ((BgL_ftpz00_bglt) BgL_ftpz00_3960);
						}
					else
						{
							obj_t BgL_auxz00_5821;

							BgL_auxz00_5821 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(21847L), BGl_string2571z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3960);
							FAILURE(BgL_auxz00_5821, BFALSE, BFALSE);
						}
					BgL_tmpz00_5816 = BGl_ftpzd2abortzd2zz__ftpz00(BgL_auxz00_5817);
				}
				return BBOOL(BgL_tmpz00_5816);
			}
		}

	}



/* ftp-delete */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2deletezd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_68, obj_t BgL_filez00_69)
	{
		{	/* Unsafe/ftp.scm 602 */
			{	/* Unsafe/ftp.scm 603 */
				obj_t BgL_list1826z00_3422;

				BgL_list1826z00_3422 = MAKE_YOUNG_PAIR(BgL_filez00_69, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_68), BGl_string2572z00zz__ftpz00,
						BgL_list1826z00_3422));
			}
		}

	}



/* &ftp-delete */
	obj_t BGl_z62ftpzd2deletezb0zz__ftpz00(obj_t BgL_envz00_3961,
		obj_t BgL_ftpz00_3962, obj_t BgL_filez00_3963)
	{
		{	/* Unsafe/ftp.scm 602 */
			{	/* Unsafe/ftp.scm 603 */
				bool_t BgL_tmpz00_5831;

				{	/* Unsafe/ftp.scm 603 */
					obj_t BgL_auxz00_5840;
					BgL_ftpz00_bglt BgL_auxz00_5832;

					if (STRINGP(BgL_filez00_3963))
						{	/* Unsafe/ftp.scm 603 */
							BgL_auxz00_5840 = BgL_filez00_3963;
						}
					else
						{
							obj_t BgL_auxz00_5843;

							BgL_auxz00_5843 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(22133L), BGl_string2573z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_filez00_3963);
							FAILURE(BgL_auxz00_5843, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3962, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 603 */
							BgL_auxz00_5832 = ((BgL_ftpz00_bglt) BgL_ftpz00_3962);
						}
					else
						{
							obj_t BgL_auxz00_5836;

							BgL_auxz00_5836 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(22133L), BGl_string2573z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3962);
							FAILURE(BgL_auxz00_5836, BFALSE, BFALSE);
						}
					BgL_tmpz00_5831 =
						BGl_ftpzd2deletezd2zz__ftpz00(BgL_auxz00_5832, BgL_auxz00_5840);
				}
				return BBOOL(BgL_tmpz00_5831);
			}
		}

	}



/* ftp-rmdir */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2rmdirzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_70, obj_t BgL_directoryz00_71)
	{
		{	/* Unsafe/ftp.scm 608 */
			{	/* Unsafe/ftp.scm 609 */
				obj_t BgL_list1827z00_3423;

				BgL_list1827z00_3423 = MAKE_YOUNG_PAIR(BgL_directoryz00_71, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_70), BGl_string2574z00zz__ftpz00,
						BgL_list1827z00_3423));
			}
		}

	}



/* &ftp-rmdir */
	obj_t BGl_z62ftpzd2rmdirzb0zz__ftpz00(obj_t BgL_envz00_3964,
		obj_t BgL_ftpz00_3965, obj_t BgL_directoryz00_3966)
	{
		{	/* Unsafe/ftp.scm 608 */
			{	/* Unsafe/ftp.scm 609 */
				bool_t BgL_tmpz00_5853;

				{	/* Unsafe/ftp.scm 609 */
					obj_t BgL_auxz00_5862;
					BgL_ftpz00_bglt BgL_auxz00_5854;

					if (STRINGP(BgL_directoryz00_3966))
						{	/* Unsafe/ftp.scm 609 */
							BgL_auxz00_5862 = BgL_directoryz00_3966;
						}
					else
						{
							obj_t BgL_auxz00_5865;

							BgL_auxz00_5865 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(22428L), BGl_string2575z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_directoryz00_3966);
							FAILURE(BgL_auxz00_5865, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3965, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 609 */
							BgL_auxz00_5854 = ((BgL_ftpz00_bglt) BgL_ftpz00_3965);
						}
					else
						{
							obj_t BgL_auxz00_5858;

							BgL_auxz00_5858 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(22428L), BGl_string2575z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3965);
							FAILURE(BgL_auxz00_5858, BFALSE, BFALSE);
						}
					BgL_tmpz00_5853 =
						BGl_ftpzd2rmdirzd2zz__ftpz00(BgL_auxz00_5854, BgL_auxz00_5862);
				}
				return BBOOL(BgL_tmpz00_5853);
			}
		}

	}



/* ftp-make-directory */
	BGL_EXPORTED_DEF bool_t
		BGl_ftpzd2makezd2directoryz00zz__ftpz00(BgL_ftpz00_bglt BgL_ftpz00_72,
		obj_t BgL_directoryz00_73)
	{
		{	/* Unsafe/ftp.scm 614 */
			{	/* Unsafe/ftp.scm 615 */
				obj_t BgL_list1828z00_3424;

				BgL_list1828z00_3424 = MAKE_YOUNG_PAIR(BgL_directoryz00_73, BNIL);
				return
					CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_72), BGl_string2576z00zz__ftpz00,
						BgL_list1828z00_3424));
			}
		}

	}



/* &ftp-make-directory */
	obj_t BGl_z62ftpzd2makezd2directoryz62zz__ftpz00(obj_t BgL_envz00_3967,
		obj_t BgL_ftpz00_3968, obj_t BgL_directoryz00_3969)
	{
		{	/* Unsafe/ftp.scm 614 */
			{	/* Unsafe/ftp.scm 615 */
				bool_t BgL_tmpz00_5875;

				{	/* Unsafe/ftp.scm 615 */
					obj_t BgL_auxz00_5884;
					BgL_ftpz00_bglt BgL_auxz00_5876;

					if (STRINGP(BgL_directoryz00_3969))
						{	/* Unsafe/ftp.scm 615 */
							BgL_auxz00_5884 = BgL_directoryz00_3969;
						}
					else
						{
							obj_t BgL_auxz00_5887;

							BgL_auxz00_5887 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(22736L), BGl_string2577z00zz__ftpz00,
								BGl_string2532z00zz__ftpz00, BgL_directoryz00_3969);
							FAILURE(BgL_auxz00_5887, BFALSE, BFALSE);
						}
					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3968, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 615 */
							BgL_auxz00_5876 = ((BgL_ftpz00_bglt) BgL_ftpz00_3968);
						}
					else
						{
							obj_t BgL_auxz00_5880;

							BgL_auxz00_5880 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(22736L), BGl_string2577z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3968);
							FAILURE(BgL_auxz00_5880, BFALSE, BFALSE);
						}
					BgL_tmpz00_5875 =
						BGl_ftpzd2makezd2directoryz00zz__ftpz00(BgL_auxz00_5876,
						BgL_auxz00_5884);
				}
				return BBOOL(BgL_tmpz00_5875);
			}
		}

	}



/* ftp-pwd */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2pwdzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_74)
	{
		{	/* Unsafe/ftp.scm 620 */
			return
				BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
				((obj_t) BgL_ftpz00_74), BGl_string2578z00zz__ftpz00, BNIL);
		}

	}



/* &ftp-pwd */
	obj_t BGl_z62ftpzd2pwdzb0zz__ftpz00(obj_t BgL_envz00_3970,
		obj_t BgL_ftpz00_3971)
	{
		{	/* Unsafe/ftp.scm 620 */
			{	/* Unsafe/ftp.scm 621 */
				BgL_ftpz00_bglt BgL_auxz00_5895;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3971, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 621 */
						BgL_auxz00_5895 = ((BgL_ftpz00_bglt) BgL_ftpz00_3971);
					}
				else
					{
						obj_t BgL_auxz00_5899;

						BgL_auxz00_5899 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(23023L), BGl_string2579z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3971);
						FAILURE(BgL_auxz00_5899, BFALSE, BFALSE);
					}
				return BGl_ftpzd2pwdzd2zz__ftpz00(BgL_auxz00_5895);
			}
		}

	}



/* ftp-list */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2listzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_75, obj_t BgL_argsz00_76)
	{
		{	/* Unsafe/ftp.scm 626 */
			{	/* Unsafe/ftp.scm 627 */
				obj_t BgL_runner1834z00_2086;

				{	/* Unsafe/ftp.scm 627 */
					obj_t BgL_list1830z00_2082;

					{	/* Unsafe/ftp.scm 627 */
						obj_t BgL_arg1831z00_2083;

						BgL_arg1831z00_2083 = MAKE_YOUNG_PAIR(BgL_argsz00_76, BNIL);
						BgL_list1830z00_2082 =
							MAKE_YOUNG_PAIR(BGl_string2580z00zz__ftpz00, BgL_arg1831z00_2083);
					}
					BgL_runner1834z00_2086 =
						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_ftpz00_75), BgL_list1830z00_2082);
				}
				{	/* Unsafe/ftp.scm 627 */
					obj_t BgL_aux1832z00_2084;

					BgL_aux1832z00_2084 = CAR(BgL_runner1834z00_2086);
					BgL_runner1834z00_2086 = CDR(BgL_runner1834z00_2086);
					{	/* Unsafe/ftp.scm 627 */
						obj_t BgL_aux1833z00_2085;

						BgL_aux1833z00_2085 = CAR(BgL_runner1834z00_2086);
						BgL_runner1834z00_2086 = CDR(BgL_runner1834z00_2086);
						return
							BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(BgL_aux1832z00_2084,
							BgL_aux1833z00_2085, BgL_runner1834z00_2086);
					}
				}
			}
		}

	}



/* &ftp-list */
	obj_t BGl_z62ftpzd2listzb0zz__ftpz00(obj_t BgL_envz00_3972,
		obj_t BgL_ftpz00_3973, obj_t BgL_argsz00_3974)
	{
		{	/* Unsafe/ftp.scm 626 */
			{	/* Unsafe/ftp.scm 627 */
				BgL_ftpz00_bglt BgL_auxz00_5913;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3973, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 627 */
						BgL_auxz00_5913 = ((BgL_ftpz00_bglt) BgL_ftpz00_3973);
					}
				else
					{
						obj_t BgL_auxz00_5917;

						BgL_auxz00_5917 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(23308L), BGl_string2581z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3973);
						FAILURE(BgL_auxz00_5917, BFALSE, BFALSE);
					}
				return BGl_ftpzd2listzd2zz__ftpz00(BgL_auxz00_5913, BgL_argsz00_3974);
			}
		}

	}



/* ftp-name-list */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2namezd2listz00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_77, obj_t BgL_argsz00_78)
	{
		{	/* Unsafe/ftp.scm 632 */
			{	/* Unsafe/ftp.scm 633 */
				obj_t BgL_runner1839z00_2091;

				{	/* Unsafe/ftp.scm 633 */
					obj_t BgL_list1835z00_2087;

					{	/* Unsafe/ftp.scm 633 */
						obj_t BgL_arg1836z00_2088;

						BgL_arg1836z00_2088 = MAKE_YOUNG_PAIR(BgL_argsz00_78, BNIL);
						BgL_list1835z00_2087 =
							MAKE_YOUNG_PAIR(BGl_string2582z00zz__ftpz00, BgL_arg1836z00_2088);
					}
					BgL_runner1839z00_2091 =
						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_ftpz00_77), BgL_list1835z00_2087);
				}
				{	/* Unsafe/ftp.scm 633 */
					obj_t BgL_aux1837z00_2089;

					BgL_aux1837z00_2089 = CAR(BgL_runner1839z00_2091);
					BgL_runner1839z00_2091 = CDR(BgL_runner1839z00_2091);
					{	/* Unsafe/ftp.scm 633 */
						obj_t BgL_aux1838z00_2090;

						BgL_aux1838z00_2090 = CAR(BgL_runner1839z00_2091);
						BgL_runner1839z00_2091 = CDR(BgL_runner1839z00_2091);
						return
							BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(BgL_aux1837z00_2089,
							BgL_aux1838z00_2090, BgL_runner1839z00_2091);
					}
				}
			}
		}

	}



/* &ftp-name-list */
	obj_t BGl_z62ftpzd2namezd2listz62zz__ftpz00(obj_t BgL_envz00_3975,
		obj_t BgL_ftpz00_3976, obj_t BgL_argsz00_3977)
	{
		{	/* Unsafe/ftp.scm 632 */
			{	/* Unsafe/ftp.scm 633 */
				BgL_ftpz00_bglt BgL_auxz00_5931;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3976, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 633 */
						BgL_auxz00_5931 = ((BgL_ftpz00_bglt) BgL_ftpz00_3976);
					}
				else
					{
						obj_t BgL_auxz00_5935;

						BgL_auxz00_5935 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(23610L), BGl_string2583z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3976);
						FAILURE(BgL_auxz00_5935, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2namezd2listz00zz__ftpz00(BgL_auxz00_5931, BgL_argsz00_3977);
			}
		}

	}



/* ftp-site-parameters */
	BGL_EXPORTED_DEF obj_t
		BGl_ftpzd2sitezd2parametersz00zz__ftpz00(BgL_ftpz00_bglt BgL_ftpz00_79,
		obj_t BgL_argsz00_80)
	{
		{	/* Unsafe/ftp.scm 638 */
			{	/* Unsafe/ftp.scm 639 */
				obj_t BgL_runner1845z00_2096;

				{	/* Unsafe/ftp.scm 639 */
					obj_t BgL_list1840z00_2092;

					{	/* Unsafe/ftp.scm 639 */
						obj_t BgL_arg1842z00_2093;

						BgL_arg1842z00_2093 = MAKE_YOUNG_PAIR(BgL_argsz00_80, BNIL);
						BgL_list1840z00_2092 =
							MAKE_YOUNG_PAIR(BGl_string2584z00zz__ftpz00, BgL_arg1842z00_2093);
					}
					BgL_runner1845z00_2096 =
						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_ftpz00_79), BgL_list1840z00_2092);
				}
				{	/* Unsafe/ftp.scm 639 */
					obj_t BgL_aux1843z00_2094;

					BgL_aux1843z00_2094 = CAR(BgL_runner1845z00_2096);
					BgL_runner1845z00_2096 = CDR(BgL_runner1845z00_2096);
					{	/* Unsafe/ftp.scm 639 */
						obj_t BgL_aux1844z00_2095;

						BgL_aux1844z00_2095 = CAR(BgL_runner1845z00_2096);
						BgL_runner1845z00_2096 = CDR(BgL_runner1845z00_2096);
						return
							BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(BgL_aux1843z00_2094,
							BgL_aux1844z00_2095, BgL_runner1845z00_2096);
					}
				}
			}
		}

	}



/* &ftp-site-parameters */
	obj_t BGl_z62ftpzd2sitezd2parametersz62zz__ftpz00(obj_t BgL_envz00_3978,
		obj_t BgL_ftpz00_3979, obj_t BgL_argsz00_3980)
	{
		{	/* Unsafe/ftp.scm 638 */
			{	/* Unsafe/ftp.scm 639 */
				BgL_ftpz00_bglt BgL_auxz00_5949;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3979, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 639 */
						BgL_auxz00_5949 = ((BgL_ftpz00_bglt) BgL_ftpz00_3979);
					}
				else
					{
						obj_t BgL_auxz00_5953;

						BgL_auxz00_5953 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(23918L), BGl_string2585z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3979);
						FAILURE(BgL_auxz00_5953, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2sitezd2parametersz00zz__ftpz00(BgL_auxz00_5949,
					BgL_argsz00_3980);
			}
		}

	}



/* ftp-system */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2systemzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_81)
	{
		{	/* Unsafe/ftp.scm 644 */
			return
				BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
				((obj_t) BgL_ftpz00_81), BGl_string2586z00zz__ftpz00, BNIL);
		}

	}



/* &ftp-system */
	obj_t BGl_z62ftpzd2systemzb0zz__ftpz00(obj_t BgL_envz00_3981,
		obj_t BgL_ftpz00_3982)
	{
		{	/* Unsafe/ftp.scm 644 */
			{	/* Unsafe/ftp.scm 645 */
				BgL_ftpz00_bglt BgL_auxz00_5960;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3982, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 645 */
						BgL_auxz00_5960 = ((BgL_ftpz00_bglt) BgL_ftpz00_3982);
					}
				else
					{
						obj_t BgL_auxz00_5964;

						BgL_auxz00_5964 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(24210L), BGl_string2587z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3982);
						FAILURE(BgL_auxz00_5964, BFALSE, BFALSE);
					}
				return BGl_ftpzd2systemzd2zz__ftpz00(BgL_auxz00_5960);
			}
		}

	}



/* ftp-status */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2statuszd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_82, obj_t BgL_argsz00_83)
	{
		{	/* Unsafe/ftp.scm 650 */
			{	/* Unsafe/ftp.scm 651 */
				obj_t BgL_runner1851z00_2102;

				{	/* Unsafe/ftp.scm 651 */
					obj_t BgL_list1847z00_2098;

					{	/* Unsafe/ftp.scm 651 */
						obj_t BgL_arg1848z00_2099;

						BgL_arg1848z00_2099 = MAKE_YOUNG_PAIR(BgL_argsz00_83, BNIL);
						BgL_list1847z00_2098 =
							MAKE_YOUNG_PAIR(BGl_string2588z00zz__ftpz00, BgL_arg1848z00_2099);
					}
					BgL_runner1851z00_2102 =
						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_ftpz00_82), BgL_list1847z00_2098);
				}
				{	/* Unsafe/ftp.scm 651 */
					obj_t BgL_aux1849z00_2100;

					BgL_aux1849z00_2100 = CAR(BgL_runner1851z00_2102);
					BgL_runner1851z00_2102 = CDR(BgL_runner1851z00_2102);
					{	/* Unsafe/ftp.scm 651 */
						obj_t BgL_aux1850z00_2101;

						BgL_aux1850z00_2101 = CAR(BgL_runner1851z00_2102);
						BgL_runner1851z00_2102 = CDR(BgL_runner1851z00_2102);
						return
							BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(BgL_aux1849z00_2100,
							BgL_aux1850z00_2101, BgL_runner1851z00_2102);
					}
				}
			}
		}

	}



/* &ftp-status */
	obj_t BGl_z62ftpzd2statuszb0zz__ftpz00(obj_t BgL_envz00_3983,
		obj_t BgL_ftpz00_3984, obj_t BgL_argsz00_3985)
	{
		{	/* Unsafe/ftp.scm 650 */
			{	/* Unsafe/ftp.scm 651 */
				BgL_ftpz00_bglt BgL_auxz00_5978;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3984, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 651 */
						BgL_auxz00_5978 = ((BgL_ftpz00_bglt) BgL_ftpz00_3984);
					}
				else
					{
						obj_t BgL_auxz00_5982;

						BgL_auxz00_5982 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(24498L), BGl_string2589z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3984);
						FAILURE(BgL_auxz00_5982, BFALSE, BFALSE);
					}
				return BGl_ftpzd2statuszd2zz__ftpz00(BgL_auxz00_5978, BgL_argsz00_3985);
			}
		}

	}



/* ftp-help */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2helpzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_84, obj_t BgL_argsz00_85)
	{
		{	/* Unsafe/ftp.scm 656 */
			{	/* Unsafe/ftp.scm 657 */
				obj_t BgL_argz00_3443;

				if (PAIRP(BgL_argsz00_85))
					{	/* Unsafe/ftp.scm 657 */
						BgL_argz00_3443 = CAR(BgL_argsz00_85);
					}
				else
					{	/* Unsafe/ftp.scm 657 */
						BgL_argz00_3443 = BFALSE;
					}
				if (CBOOL(BgL_argz00_3443))
					{	/* Unsafe/ftp.scm 659 */
						obj_t BgL_list1852z00_3445;

						BgL_list1852z00_3445 = MAKE_YOUNG_PAIR(BgL_argz00_3443, BNIL);
						return
							BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
							((obj_t) BgL_ftpz00_84), BGl_string2590z00zz__ftpz00,
							BgL_list1852z00_3445);
					}
				else
					{	/* Unsafe/ftp.scm 658 */
						return
							BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
							((obj_t) BgL_ftpz00_84), BGl_string2590z00zz__ftpz00, BNIL);
					}
			}
		}

	}



/* &ftp-help */
	obj_t BGl_z62ftpzd2helpzb0zz__ftpz00(obj_t BgL_envz00_3986,
		obj_t BgL_ftpz00_3987, obj_t BgL_argsz00_3988)
	{
		{	/* Unsafe/ftp.scm 656 */
			{	/* Unsafe/ftp.scm 657 */
				BgL_ftpz00_bglt BgL_auxz00_5997;

				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3987, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 657 */
						BgL_auxz00_5997 = ((BgL_ftpz00_bglt) BgL_ftpz00_3987);
					}
				else
					{
						obj_t BgL_auxz00_6001;

						BgL_auxz00_6001 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(24795L), BGl_string2591z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3987);
						FAILURE(BgL_auxz00_6001, BFALSE, BFALSE);
					}
				return BGl_ftpzd2helpzd2zz__ftpz00(BgL_auxz00_5997, BgL_argsz00_3988);
			}
		}

	}



/* ftp-noop */
	BGL_EXPORTED_DEF bool_t BGl_ftpzd2noopzd2zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_86)
	{
		{	/* Unsafe/ftp.scm 665 */
			return
				CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
					((obj_t) BgL_ftpz00_86), BGl_string2592z00zz__ftpz00, BNIL));
		}

	}



/* &ftp-noop */
	obj_t BGl_z62ftpzd2noopzb0zz__ftpz00(obj_t BgL_envz00_3989,
		obj_t BgL_ftpz00_3990)
	{
		{	/* Unsafe/ftp.scm 665 */
			{	/* Unsafe/ftp.scm 666 */
				bool_t BgL_tmpz00_6009;

				{	/* Unsafe/ftp.scm 666 */
					BgL_ftpz00_bglt BgL_auxz00_6010;

					if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3990, BGl_ftpz00zz__ftpz00))
						{	/* Unsafe/ftp.scm 666 */
							BgL_auxz00_6010 = ((BgL_ftpz00_bglt) BgL_ftpz00_3990);
						}
					else
						{
							obj_t BgL_auxz00_6014;

							BgL_auxz00_6014 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
								BINT(25172L), BGl_string2593z00zz__ftpz00,
								BGl_string2497z00zz__ftpz00, BgL_ftpz00_3990);
							FAILURE(BgL_auxz00_6014, BFALSE, BFALSE);
						}
					BgL_tmpz00_6009 = BGl_ftpzd2noopzd2zz__ftpz00(BgL_auxz00_6010);
				}
				return BBOOL(BgL_tmpz00_6009);
			}
		}

	}



/* _open-input-ftp-file */
	obj_t BGl__openzd2inputzd2ftpzd2filezd2zz__ftpz00(obj_t BgL_env1298z00_91,
		obj_t BgL_opt1297z00_90)
	{
		{	/* Unsafe/ftp.scm 671 */
			{	/* Unsafe/ftp.scm 671 */
				obj_t BgL_g1299z00_2108;

				BgL_g1299z00_2108 = VECTOR_REF(BgL_opt1297z00_90, 0L);
				switch (VECTOR_LENGTH(BgL_opt1297z00_90))
					{
					case 1L:

						{	/* Unsafe/ftp.scm 671 */

							{	/* Unsafe/ftp.scm 671 */
								obj_t BgL_auxz00_6021;

								if (STRINGP(BgL_g1299z00_2108))
									{	/* Unsafe/ftp.scm 671 */
										BgL_auxz00_6021 = BgL_g1299z00_2108;
									}
								else
									{
										obj_t BgL_auxz00_6024;

										BgL_auxz00_6024 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2522z00zz__ftpz00, BINT(25425L),
											BGl_string2594z00zz__ftpz00, BGl_string2532z00zz__ftpz00,
											BgL_g1299z00_2108);
										FAILURE(BgL_auxz00_6024, BFALSE, BFALSE);
									}
								return
									BGl_openzd2inputzd2ftpzd2filezd2zz__ftpz00(BgL_auxz00_6021,
									BTRUE, BINT(1000000L));
							}
						}
						break;
					case 2L:

						{	/* Unsafe/ftp.scm 671 */
							obj_t BgL_bufinfoz00_2113;

							BgL_bufinfoz00_2113 = VECTOR_REF(BgL_opt1297z00_90, 1L);
							{	/* Unsafe/ftp.scm 671 */

								{	/* Unsafe/ftp.scm 671 */
									obj_t BgL_auxz00_6031;

									if (STRINGP(BgL_g1299z00_2108))
										{	/* Unsafe/ftp.scm 671 */
											BgL_auxz00_6031 = BgL_g1299z00_2108;
										}
									else
										{
											obj_t BgL_auxz00_6034;

											BgL_auxz00_6034 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2522z00zz__ftpz00, BINT(25425L),
												BGl_string2594z00zz__ftpz00,
												BGl_string2532z00zz__ftpz00, BgL_g1299z00_2108);
											FAILURE(BgL_auxz00_6034, BFALSE, BFALSE);
										}
									return
										BGl_openzd2inputzd2ftpzd2filezd2zz__ftpz00(BgL_auxz00_6031,
										BgL_bufinfoz00_2113, BINT(1000000L));
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/ftp.scm 671 */
							obj_t BgL_bufinfoz00_2115;

							BgL_bufinfoz00_2115 = VECTOR_REF(BgL_opt1297z00_90, 1L);
							{	/* Unsafe/ftp.scm 671 */
								obj_t BgL_timeoutz00_2116;

								BgL_timeoutz00_2116 = VECTOR_REF(BgL_opt1297z00_90, 2L);
								{	/* Unsafe/ftp.scm 671 */

									{	/* Unsafe/ftp.scm 671 */
										obj_t BgL_auxz00_6042;

										if (STRINGP(BgL_g1299z00_2108))
											{	/* Unsafe/ftp.scm 671 */
												BgL_auxz00_6042 = BgL_g1299z00_2108;
											}
										else
											{
												obj_t BgL_auxz00_6045;

												BgL_auxz00_6045 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2522z00zz__ftpz00, BINT(25425L),
													BGl_string2594z00zz__ftpz00,
													BGl_string2532z00zz__ftpz00, BgL_g1299z00_2108);
												FAILURE(BgL_auxz00_6045, BFALSE, BFALSE);
											}
										return
											BGl_openzd2inputzd2ftpzd2filezd2zz__ftpz00
											(BgL_auxz00_6042, BgL_bufinfoz00_2115,
											BgL_timeoutz00_2116);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* open-input-ftp-file */
	BGL_EXPORTED_DEF obj_t BGl_openzd2inputzd2ftpzd2filezd2zz__ftpz00(obj_t
		BgL_stringz00_87, obj_t BgL_bufinfoz00_88, obj_t BgL_timeoutz00_89)
	{
		{	/* Unsafe/ftp.scm 671 */
			{	/* Unsafe/ftp.scm 701 */
				obj_t BgL_protocolz00_2118;

				BgL_protocolz00_2118 =
					BGl_urlzd2sanszd2protocolzd2parsezd2zz__urlz00(BgL_stringz00_87,
					BGl_string2497z00zz__ftpz00);
				{	/* Unsafe/ftp.scm 702 */
					obj_t BgL_loginz00_2119;
					obj_t BgL_hostz00_2120;
					obj_t BgL_portz00_2121;
					obj_t BgL_abspathz00_2122;

					{	/* Unsafe/ftp.scm 703 */
						obj_t BgL_tmpz00_3449;

						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6053;

							BgL_tmpz00_6053 = (int) (1L);
							BgL_tmpz00_3449 = BGL_MVALUES_VAL(BgL_tmpz00_6053);
						}
						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6056;

							BgL_tmpz00_6056 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6056, BUNSPEC);
						}
						BgL_loginz00_2119 = BgL_tmpz00_3449;
					}
					{	/* Unsafe/ftp.scm 703 */
						obj_t BgL_tmpz00_3450;

						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6059;

							BgL_tmpz00_6059 = (int) (2L);
							BgL_tmpz00_3450 = BGL_MVALUES_VAL(BgL_tmpz00_6059);
						}
						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6062;

							BgL_tmpz00_6062 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6062, BUNSPEC);
						}
						BgL_hostz00_2120 = BgL_tmpz00_3450;
					}
					{	/* Unsafe/ftp.scm 703 */
						obj_t BgL_tmpz00_3451;

						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6065;

							BgL_tmpz00_6065 = (int) (3L);
							BgL_tmpz00_3451 = BGL_MVALUES_VAL(BgL_tmpz00_6065);
						}
						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6068;

							BgL_tmpz00_6068 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6068, BUNSPEC);
						}
						BgL_portz00_2121 = BgL_tmpz00_3451;
					}
					{	/* Unsafe/ftp.scm 703 */
						obj_t BgL_tmpz00_3452;

						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6071;

							BgL_tmpz00_6071 = (int) (4L);
							BgL_tmpz00_3452 = BGL_MVALUES_VAL(BgL_tmpz00_6071);
						}
						{	/* Unsafe/ftp.scm 703 */
							int BgL_tmpz00_6074;

							BgL_tmpz00_6074 = (int) (4L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_6074, BUNSPEC);
						}
						BgL_abspathz00_2122 = BgL_tmpz00_3452;
					}
					{	/* Unsafe/ftp.scm 703 */
						obj_t BgL_iz00_2123;

						if (STRINGP(BgL_loginz00_2119))
							{	/* Ieee/string.scm 223 */

								BgL_iz00_2123 =
									BGl_stringzd2indexzd2zz__r4_strings_6_7z00(BgL_loginz00_2119,
									BCHAR(((unsigned char) ':')), BINT(0L));
							}
						else
							{	/* Unsafe/ftp.scm 703 */
								BgL_iz00_2123 = BFALSE;
							}
						{	/* Unsafe/ftp.scm 703 */
							BgL_ftpz00_bglt BgL_ftpz00_2124;

							{	/* Unsafe/ftp.scm 704 */
								BgL_ftpz00_bglt BgL_new1160z00_2132;

								{	/* Unsafe/ftp.scm 704 */
									BgL_ftpz00_bglt BgL_new1159z00_2138;

									BgL_new1159z00_2138 =
										((BgL_ftpz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_ftpz00_bgl))));
									{	/* Unsafe/ftp.scm 704 */
										long BgL_arg1862z00_2139;

										BgL_arg1862z00_2139 = BGL_CLASS_NUM(BGl_ftpz00zz__ftpz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1159z00_2138),
											BgL_arg1862z00_2139);
									}
									BgL_new1160z00_2132 = BgL_new1159z00_2138;
								}
								((((BgL_z52ftpz52_bglt) COBJECT(
												((BgL_z52ftpz52_bglt) BgL_new1160z00_2132)))->
										BgL_cmdz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
													BgL_new1160z00_2132)))->BgL_dtpz00) =
									((obj_t) BFALSE), BUNSPEC);
								((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
													BgL_new1160z00_2132)))->BgL_passivezf3zf3) =
									((bool_t) ((bool_t) 1)), BUNSPEC);
								((((BgL_ftpz00_bglt) COBJECT(BgL_new1160z00_2132))->
										BgL_hostz00) = ((obj_t) BgL_hostz00_2120), BUNSPEC);
								((((BgL_ftpz00_bglt) COBJECT(BgL_new1160z00_2132))->
										BgL_portz00) = ((obj_t) BINT(21L)), BUNSPEC);
								((((BgL_ftpz00_bglt) COBJECT(BgL_new1160z00_2132))->
										BgL_motdz00) =
									((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
								{
									obj_t BgL_auxz00_6096;

									if (CBOOL(BgL_iz00_2123))
										{	/* Unsafe/ftp.scm 707 */
											long BgL_endz00_3457;

											BgL_endz00_3457 = (long) CINT(BgL_iz00_2123);
											BgL_auxz00_6096 =
												c_substring(
												((obj_t) BgL_loginz00_2119), 0L, BgL_endz00_3457);
										}
									else
										{	/* Unsafe/ftp.scm 707 */
											if (STRINGP(BgL_loginz00_2119))
												{	/* Unsafe/ftp.scm 708 */
													BgL_auxz00_6096 = BgL_loginz00_2119;
												}
											else
												{	/* Unsafe/ftp.scm 708 */
													BgL_auxz00_6096 = BGl_string2518z00zz__ftpz00;
												}
										}
									((((BgL_ftpz00_bglt) COBJECT(BgL_new1160z00_2132))->
											BgL_userz00) = ((obj_t) BgL_auxz00_6096), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_6105;

									if (CBOOL(BgL_iz00_2123))
										{	/* Unsafe/ftp.scm 711 */
											long BgL_arg1860z00_2134;

											BgL_arg1860z00_2134 = ((long) CINT(BgL_iz00_2123) + 1L);
											{	/* Ieee/string.scm 194 */
												long BgL_endz00_2137;

												BgL_endz00_2137 =
													STRING_LENGTH(((obj_t) BgL_loginz00_2119));
												{	/* Ieee/string.scm 194 */

													BgL_auxz00_6105 =
														BGl_substringz00zz__r4_strings_6_7z00
														(BgL_loginz00_2119, BgL_arg1860z00_2134,
														BgL_endz00_2137);
										}}}
									else
										{	/* Unsafe/ftp.scm 711 */
											BgL_auxz00_6105 = BGl_string2519z00zz__ftpz00;
										}
									((((BgL_ftpz00_bglt) COBJECT(BgL_new1160z00_2132))->
											BgL_passz00) = ((obj_t) BgL_auxz00_6105), BUNSPEC);
								}
								((((BgL_ftpz00_bglt) COBJECT(BgL_new1160z00_2132))->
										BgL_acctz00) =
									((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
								BgL_ftpz00_2124 = BgL_new1160z00_2132;
							}
							{	/* Unsafe/ftp.scm 704 */

								{	/* Unsafe/ftp.scm 713 */
									bool_t BgL_test3035z00_6115;

									{	/* Unsafe/ftp.scm 713 */
										bool_t BgL_res2356z00_3469;

										{
											obj_t BgL_auxz00_6116;

											{	/* Unsafe/ftp.scm 410 */
												obj_t BgL_arg1745z00_3462;
												obj_t BgL_arg1746z00_3463;

												BgL_arg1745z00_3462 =
													(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_2124))->
													BgL_hostz00);
												BgL_arg1746z00_3463 =
													(((BgL_ftpz00_bglt) COBJECT(BgL_ftpz00_2124))->
													BgL_portz00);
												{	/* Unsafe/ftp.scm 410 */
													obj_t BgL_domainz00_3464;

													BgL_domainz00_3464 = BGl_symbol2520z00zz__ftpz00;
													BgL_auxz00_6116 =
														BGl_makezd2clientzd2socketz00zz__socketz00
														(BgL_arg1745z00_3462, CINT(BgL_arg1746z00_3463),
														BgL_domainz00_3464, BTRUE, BTRUE,
														BgL_timeoutz00_89);
												}
											}
											((((BgL_z52ftpz52_bglt) COBJECT(
															((BgL_z52ftpz52_bglt) BgL_ftpz00_2124)))->
													BgL_cmdz00) = ((obj_t) BgL_auxz00_6116), BUNSPEC);
										}
										BgL_res2356z00_3469 =
											CBOOL(BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
												((obj_t) BgL_ftpz00_2124), BFALSE, BNIL));
										BgL_test3035z00_6115 = BgL_res2356z00_3469;
									}
									if (BgL_test3035z00_6115)
										{	/* Unsafe/ftp.scm 714 */
											obj_t BgL_piz00_2126;

											{	/* Unsafe/ftp.scm 547 */
												obj_t BgL_list1809z00_3472;

												BgL_list1809z00_3472 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_abspathz00_2122), BNIL);
												BgL_piz00_2126 =
													BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
													((obj_t) BgL_ftpz00_2124),
													BGl_string2500z00zz__ftpz00, BgL_list1809z00_3472);
											}
											if (INPUT_PORTP(BgL_piz00_2126))
												{	/* Unsafe/ftp.scm 715 */
													{	/* Unsafe/ftp.scm 716 */
														obj_t BgL_zc3z04anonymousza31858ze3z87_3992;

														BgL_zc3z04anonymousza31858ze3z87_3992 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31858ze3ze5zz__ftpz00,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31858ze3z87_3992,
															(int) (0L), ((obj_t) BgL_ftpz00_2124));
														BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
															(BgL_piz00_2126,
															BgL_zc3z04anonymousza31858ze3z87_3992);
													}
													return BgL_piz00_2126;
												}
											else
												{	/* Unsafe/ftp.scm 715 */
													return BFALSE;
												}
										}
									else
										{	/* Unsafe/ftp.scm 713 */
											return BFALSE;
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1858> */
	obj_t BGl_z62zc3z04anonymousza31858ze3ze5zz__ftpz00(obj_t BgL_envz00_3993,
		obj_t BgL_vz00_3995)
	{
		{	/* Unsafe/ftp.scm 716 */
			{	/* Unsafe/ftp.scm 377 */
				BgL_ftpz00_bglt BgL_ftpz00_3994;

				BgL_ftpz00_3994 =
					((BgL_ftpz00_bglt) PROCEDURE_REF(BgL_envz00_3993, (int) (0L)));
				return
					BGl_zc3z04anonymousza31858ze3ze70z60zz__ftpz00(BgL_ftpz00_3994,
					BgL_vz00_3995);
			}
		}

	}



/* <@anonymous:1858>~0 */
	obj_t BGl_zc3z04anonymousza31858ze3ze70z60zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_4151, obj_t BgL_vz00_2129)
	{
		{	/* Unsafe/ftp.scm 716 */
			{	/* Unsafe/ftp.scm 377 */
				obj_t BgL_exitd1112z00_3473;

				BgL_exitd1112z00_3473 = BGL_EXITD_TOP_AS_OBJ();
				{	/* Unsafe/ftp.scm 379 */
					obj_t BgL_zc3z04anonymousza31711ze3z87_3991;

					BgL_zc3z04anonymousza31711ze3z87_3991 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31711ze32366ze5zz__ftpz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31711ze3z87_3991,
						(int) (0L), ((obj_t) BgL_ftpz00_4151));
					{	/* Unsafe/ftp.scm 377 */
						obj_t BgL_arg2321z00_3476;

						{	/* Unsafe/ftp.scm 377 */
							obj_t BgL_arg2323z00_3477;

							BgL_arg2323z00_3477 = BGL_EXITD_PROTECT(BgL_exitd1112z00_3473);
							BgL_arg2321z00_3476 =
								MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31711ze3z87_3991,
								BgL_arg2323z00_3477);
						}
						BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_3473, BgL_arg2321z00_3476);
						BUNSPEC;
					}
					{	/* Unsafe/ftp.scm 378 */
						obj_t BgL_tmp1114z00_3475;

						BgL_tmp1114z00_3475 =
							BGl_z52ftpzd2closezd2cmdz52zz__ftpz00(((obj_t) BgL_ftpz00_4151));
						{	/* Unsafe/ftp.scm 377 */
							bool_t BgL_test3037z00_6155;

							{	/* Unsafe/ftp.scm 377 */
								obj_t BgL_arg2320z00_3479;

								BgL_arg2320z00_3479 = BGL_EXITD_PROTECT(BgL_exitd1112z00_3473);
								BgL_test3037z00_6155 = PAIRP(BgL_arg2320z00_3479);
							}
							if (BgL_test3037z00_6155)
								{	/* Unsafe/ftp.scm 377 */
									obj_t BgL_arg2318z00_3480;

									{	/* Unsafe/ftp.scm 377 */
										obj_t BgL_arg2319z00_3481;

										BgL_arg2319z00_3481 =
											BGL_EXITD_PROTECT(BgL_exitd1112z00_3473);
										BgL_arg2318z00_3480 = CDR(((obj_t) BgL_arg2319z00_3481));
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_3473,
										BgL_arg2318z00_3480);
									BUNSPEC;
								}
							else
								{	/* Unsafe/ftp.scm 377 */
									BFALSE;
								}
						}
						BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(((obj_t) BgL_ftpz00_4151));
						return BgL_tmp1114z00_3475;
					}
				}
			}
		}

	}



/* &<@anonymous:1711>2366 */
	obj_t BGl_z62zc3z04anonymousza31711ze32366ze5zz__ftpz00(obj_t BgL_envz00_3996)
	{
		{	/* Unsafe/ftp.scm 377 */
			{	/* Unsafe/ftp.scm 379 */
				BgL_ftpz00_bglt BgL_ftpz00_3997;

				BgL_ftpz00_3997 =
					((BgL_ftpz00_bglt) PROCEDURE_REF(BgL_envz00_3996, (int) (0L)));
				return BGl_z52ftpzd2closezd2dtpz52zz__ftpz00(((obj_t) BgL_ftpz00_3997));
			}
		}

	}



/* ftp-directory->list */
	BGL_EXPORTED_DEF obj_t
		BGl_ftpzd2directoryzd2ze3listze3zz__ftpz00(BgL_ftpz00_bglt BgL_ftpz00_92,
		obj_t BgL_dirz00_93)
	{
		{	/* Unsafe/ftp.scm 722 */
			{	/* Unsafe/ftp.scm 723 */
				long BgL_lenz00_2180;
				obj_t BgL_lstz00_2181;

				BgL_lenz00_2180 = STRING_LENGTH(BgL_dirz00_93);
				{	/* Unsafe/ftp.scm 724 */
					obj_t BgL_list1903z00_2213;

					BgL_list1903z00_2213 = MAKE_YOUNG_PAIR(BgL_dirz00_93, BNIL);
					BgL_lstz00_2181 =
						BGl_ftpzd2namezd2listz00zz__ftpz00(BgL_ftpz00_92,
						BgL_list1903z00_2213);
				}
				{	/* Unsafe/ftp.scm 726 */
					bool_t BgL_test3038z00_6172;

					if (NULLP(BgL_lstz00_2181))
						{	/* Unsafe/ftp.scm 726 */
							BgL_test3038z00_6172 = ((bool_t) 0);
						}
					else
						{	/* Unsafe/ftp.scm 726 */
							obj_t BgL_tmpz00_6175;

							BgL_tmpz00_6175 = CDR(BgL_lstz00_2181);
							BgL_test3038z00_6172 = PAIRP(BgL_tmpz00_6175);
						}
					if (BgL_test3038z00_6172)
						{	/* Unsafe/ftp.scm 727 */
							obj_t BgL_head1252z00_2187;

							BgL_head1252z00_2187 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1250z00_2189;
								obj_t BgL_tail1253z00_2190;

								BgL_l1250z00_2189 = BgL_lstz00_2181;
								BgL_tail1253z00_2190 = BgL_head1252z00_2187;
							BgL_zc3z04anonymousza31890ze3z87_2191:
								if (NULLP(BgL_l1250z00_2189))
									{	/* Unsafe/ftp.scm 727 */
										return CDR(BgL_head1252z00_2187);
									}
								else
									{	/* Unsafe/ftp.scm 727 */
										obj_t BgL_newtail1254z00_2193;

										{	/* Unsafe/ftp.scm 727 */
											obj_t BgL_arg1893z00_2195;

											{	/* Unsafe/ftp.scm 727 */
												obj_t BgL_filez00_2196;

												BgL_filez00_2196 = CAR(((obj_t) BgL_l1250z00_2189));
												{	/* Unsafe/ftp.scm 727 */
													long BgL_arg1894z00_2197;

													BgL_arg1894z00_2197 = (BgL_lenz00_2180 + 1L);
													{	/* Ieee/string.scm 194 */
														long BgL_endz00_2200;

														BgL_endz00_2200 =
															STRING_LENGTH(((obj_t) BgL_filez00_2196));
														{	/* Ieee/string.scm 194 */

															BgL_arg1893z00_2195 =
																BGl_substringz00zz__r4_strings_6_7z00
																(BgL_filez00_2196, BgL_arg1894z00_2197,
																BgL_endz00_2200);
											}}}}
											BgL_newtail1254z00_2193 =
												MAKE_YOUNG_PAIR(BgL_arg1893z00_2195, BNIL);
										}
										SET_CDR(BgL_tail1253z00_2190, BgL_newtail1254z00_2193);
										{	/* Unsafe/ftp.scm 727 */
											obj_t BgL_arg1892z00_2194;

											BgL_arg1892z00_2194 = CDR(((obj_t) BgL_l1250z00_2189));
											{
												obj_t BgL_tail1253z00_6193;
												obj_t BgL_l1250z00_6192;

												BgL_l1250z00_6192 = BgL_arg1892z00_2194;
												BgL_tail1253z00_6193 = BgL_newtail1254z00_2193;
												BgL_tail1253z00_2190 = BgL_tail1253z00_6193;
												BgL_l1250z00_2189 = BgL_l1250z00_6192;
												goto BgL_zc3z04anonymousza31890ze3z87_2191;
											}
										}
									}
							}
						}
					else
						{	/* Unsafe/ftp.scm 726 */
							if (NULLP(BgL_lstz00_2181))
								{	/* Unsafe/ftp.scm 728 */
									return BNIL;
								}
							else
								{	/* Unsafe/ftp.scm 730 */
									bool_t BgL_test3042z00_6196;

									{	/* Unsafe/ftp.scm 730 */
										obj_t BgL_arg1901z00_2210;

										BgL_arg1901z00_2210 = CAR(BgL_lstz00_2181);
										{	/* Unsafe/ftp.scm 730 */
											long BgL_l1z00_3494;

											BgL_l1z00_3494 =
												STRING_LENGTH(((obj_t) BgL_arg1901z00_2210));
											if ((BgL_l1z00_3494 == STRING_LENGTH(BgL_dirz00_93)))
												{	/* Unsafe/ftp.scm 730 */
													int BgL_arg2089z00_3497;

													{	/* Unsafe/ftp.scm 730 */
														char *BgL_auxz00_6206;
														char *BgL_tmpz00_6203;

														BgL_auxz00_6206 = BSTRING_TO_STRING(BgL_dirz00_93);
														BgL_tmpz00_6203 =
															BSTRING_TO_STRING(((obj_t) BgL_arg1901z00_2210));
														BgL_arg2089z00_3497 =
															memcmp(BgL_tmpz00_6203, BgL_auxz00_6206,
															BgL_l1z00_3494);
													}
													BgL_test3042z00_6196 =
														((long) (BgL_arg2089z00_3497) == 0L);
												}
											else
												{	/* Unsafe/ftp.scm 730 */
													BgL_test3042z00_6196 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test3042z00_6196)
										{	/* Unsafe/ftp.scm 730 */
											return BgL_dirz00_93;
										}
									else
										{	/* Unsafe/ftp.scm 733 */
											obj_t BgL_arg1898z00_2205;
											long BgL_arg1899z00_2206;

											BgL_arg1898z00_2205 = CAR(BgL_lstz00_2181);
											BgL_arg1899z00_2206 = (BgL_lenz00_2180 + 1L);
											{	/* Ieee/string.scm 194 */
												long BgL_endz00_2209;

												BgL_endz00_2209 =
													STRING_LENGTH(((obj_t) BgL_arg1898z00_2205));
												{	/* Ieee/string.scm 194 */

													return
														BGl_substringz00zz__r4_strings_6_7z00
														(BgL_arg1898z00_2205, BgL_arg1899z00_2206,
														BgL_endz00_2209);
												}
											}
										}
								}
						}
				}
			}
		}

	}



/* &ftp-directory->list */
	obj_t BGl_z62ftpzd2directoryzd2ze3listz81zz__ftpz00(obj_t BgL_envz00_3998,
		obj_t BgL_ftpz00_3999, obj_t BgL_dirz00_4000)
	{
		{	/* Unsafe/ftp.scm 722 */
			{	/* Unsafe/ftp.scm 723 */
				obj_t BgL_auxz00_6224;
				BgL_ftpz00_bglt BgL_auxz00_6216;

				if (STRINGP(BgL_dirz00_4000))
					{	/* Unsafe/ftp.scm 723 */
						BgL_auxz00_6224 = BgL_dirz00_4000;
					}
				else
					{
						obj_t BgL_auxz00_6227;

						BgL_auxz00_6227 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(27285L), BGl_string2595z00zz__ftpz00,
							BGl_string2532z00zz__ftpz00, BgL_dirz00_4000);
						FAILURE(BgL_auxz00_6227, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_3999, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 723 */
						BgL_auxz00_6216 = ((BgL_ftpz00_bglt) BgL_ftpz00_3999);
					}
				else
					{
						obj_t BgL_auxz00_6220;

						BgL_auxz00_6220 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(27285L), BGl_string2595z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_3999);
						FAILURE(BgL_auxz00_6220, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2directoryzd2ze3listze3zz__ftpz00(BgL_auxz00_6216,
					BgL_auxz00_6224);
			}
		}

	}



/* ftp-directory->path-list */
	BGL_EXPORTED_DEF obj_t
		BGl_ftpzd2directoryzd2ze3pathzd2listz31zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_94, obj_t BgL_dirz00_95)
	{
		{	/* Unsafe/ftp.scm 738 */
			{	/* Unsafe/ftp.scm 739 */
				obj_t BgL_list1904z00_3506;

				BgL_list1904z00_3506 = MAKE_YOUNG_PAIR(BgL_dirz00_95, BNIL);
				return
					BGl_ftpzd2namezd2listz00zz__ftpz00(BgL_ftpz00_94,
					BgL_list1904z00_3506);
			}
		}

	}



/* &ftp-directory->path-list */
	obj_t BGl_z62ftpzd2directoryzd2ze3pathzd2listz53zz__ftpz00(obj_t
		BgL_envz00_4001, obj_t BgL_ftpz00_4002, obj_t BgL_dirz00_4003)
	{
		{	/* Unsafe/ftp.scm 738 */
			{	/* Unsafe/ftp.scm 739 */
				obj_t BgL_auxz00_6242;
				BgL_ftpz00_bglt BgL_auxz00_6234;

				if (STRINGP(BgL_dirz00_4003))
					{	/* Unsafe/ftp.scm 739 */
						BgL_auxz00_6242 = BgL_dirz00_4003;
					}
				else
					{
						obj_t BgL_auxz00_6245;

						BgL_auxz00_6245 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(27837L), BGl_string2596z00zz__ftpz00,
							BGl_string2532z00zz__ftpz00, BgL_dirz00_4003);
						FAILURE(BgL_auxz00_6245, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_4002, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 739 */
						BgL_auxz00_6234 = ((BgL_ftpz00_bglt) BgL_ftpz00_4002);
					}
				else
					{
						obj_t BgL_auxz00_6238;

						BgL_auxz00_6238 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(27837L), BGl_string2596z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_4002);
						FAILURE(BgL_auxz00_6238, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2directoryzd2ze3pathzd2listz31zz__ftpz00(BgL_auxz00_6234,
					BgL_auxz00_6242);
			}
		}

	}



/* ftp-copy-file */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2copyzd2filez00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_96, obj_t BgL_fromz00_97, obj_t BgL_dstz00_98)
	{
		{	/* Unsafe/ftp.scm 744 */
			{	/* Unsafe/ftp.scm 745 */
				obj_t BgL_rz00_3507;

				{	/* Unsafe/ftp.scm 547 */
					obj_t BgL_list1809z00_3514;

					BgL_list1809z00_3514 = MAKE_YOUNG_PAIR(BgL_fromz00_97, BNIL);
					BgL_rz00_3507 =
						BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
						((obj_t) BgL_ftpz00_96), BGl_string2500z00zz__ftpz00,
						BgL_list1809z00_3514);
				}
				if (INPUT_PORTP(BgL_rz00_3507))
					{	/* Unsafe/ftp.scm 749 */
						obj_t BgL_zc3z04anonymousza31907ze3z87_4004;

						BgL_zc3z04anonymousza31907ze3z87_4004 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31907ze3ze5zz__ftpz00,
							(int) (0L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4004,
							(int) (0L), BgL_rz00_3507);
						return
							BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
							(BgL_dstz00_98, BgL_zc3z04anonymousza31907ze3z87_4004);
					}
				else
					{	/* Unsafe/ftp.scm 746 */
						return BFALSE;
					}
			}
		}

	}



/* &ftp-copy-file */
	obj_t BGl_z62ftpzd2copyzd2filez62zz__ftpz00(obj_t BgL_envz00_4005,
		obj_t BgL_ftpz00_4006, obj_t BgL_fromz00_4007, obj_t BgL_dstz00_4008)
	{
		{	/* Unsafe/ftp.scm 744 */
			{	/* Unsafe/ftp.scm 745 */
				obj_t BgL_auxz00_6276;
				obj_t BgL_auxz00_6269;
				BgL_ftpz00_bglt BgL_auxz00_6261;

				if (STRINGP(BgL_dstz00_4008))
					{	/* Unsafe/ftp.scm 745 */
						BgL_auxz00_6276 = BgL_dstz00_4008;
					}
				else
					{
						obj_t BgL_auxz00_6279;

						BgL_auxz00_6279 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(28125L), BGl_string2597z00zz__ftpz00,
							BGl_string2532z00zz__ftpz00, BgL_dstz00_4008);
						FAILURE(BgL_auxz00_6279, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_fromz00_4007))
					{	/* Unsafe/ftp.scm 745 */
						BgL_auxz00_6269 = BgL_fromz00_4007;
					}
				else
					{
						obj_t BgL_auxz00_6272;

						BgL_auxz00_6272 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(28125L), BGl_string2597z00zz__ftpz00,
							BGl_string2532z00zz__ftpz00, BgL_fromz00_4007);
						FAILURE(BgL_auxz00_6272, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_4006, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 745 */
						BgL_auxz00_6261 = ((BgL_ftpz00_bglt) BgL_ftpz00_4006);
					}
				else
					{
						obj_t BgL_auxz00_6265;

						BgL_auxz00_6265 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(28125L), BGl_string2597z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_4006);
						FAILURE(BgL_auxz00_6265, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2copyzd2filez00zz__ftpz00(BgL_auxz00_6261, BgL_auxz00_6269,
					BgL_auxz00_6276);
			}
		}

	}



/* &<@anonymous:1907> */
	obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__ftpz00(obj_t BgL_envz00_4009)
	{
		{	/* Unsafe/ftp.scm 748 */
			{	/* Unsafe/ftp.scm 749 */
				obj_t BgL_rz00_4010;

				BgL_rz00_4010 = PROCEDURE_REF(BgL_envz00_4009, (int) (0L));
				{	/* Unsafe/ftp.scm 749 */
					bool_t BgL_tmpz00_6286;

					{	/* Unsafe/ftp.scm 749 */
						obj_t BgL_arg1910z00_4477;
						obj_t BgL_arg1911z00_4478;

						BgL_arg1910z00_4477 =
							BGl_readzd2stringzd2zz__r4_input_6_10_2z00(BgL_rz00_4010);
						{	/* Unsafe/ftp.scm 749 */
							obj_t BgL_tmpz00_6288;

							BgL_tmpz00_6288 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1911z00_4478 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6288);
						}
						bgl_display_obj(BgL_arg1910z00_4477, BgL_arg1911z00_4478);
					}
					BgL_tmpz00_6286 = ((bool_t) 1);
					return BBOOL(BgL_tmpz00_6286);
				}
			}
		}

	}



/* ftp-put-file */
	BGL_EXPORTED_DEF obj_t BGl_ftpzd2putzd2filez00zz__ftpz00(BgL_ftpz00_bglt
		BgL_ftpz00_99, obj_t BgL_pathz00_100)
	{
		{	/* Unsafe/ftp.scm 755 */
			{	/* Unsafe/ftp.scm 756 */
				bool_t BgL_res2357z00_3531;

				{	/* Unsafe/ftp.scm 554 */
					obj_t BgL_opz00_3521;

					{	/* Unsafe/ftp.scm 554 */
						obj_t BgL_arg1813z00_3522;

						BgL_arg1813z00_3522 =
							(((BgL_z52ftpz52_bglt) COBJECT(
									((BgL_z52ftpz52_bglt) BgL_ftpz00_99)))->BgL_dtpz00);
						{	/* Unsafe/ftp.scm 554 */
							obj_t BgL_tmpz00_6295;

							BgL_tmpz00_6295 = ((obj_t) BgL_arg1813z00_3522);
							BgL_opz00_3521 = SOCKET_OUTPUT(BgL_tmpz00_6295);
						}
					}
					if (fexists(BSTRING_TO_STRING(BgL_pathz00_100)))
						{	/* Unsafe/ftp.scm 556 */
							obj_t BgL__andtest_1142z00_3526;

							{	/* Unsafe/ftp.scm 557 */
								obj_t BgL_list1811z00_3527;

								BgL_list1811z00_3527 = MAKE_YOUNG_PAIR(BTRUE, BNIL);
								BgL__andtest_1142z00_3526 =
									BGl_z52ftpzd2enginezd2cmdz52zz__ftpz00(
									((obj_t) BgL_ftpz00_99), BGl_string2557z00zz__ftpz00,
									BgL_list1811z00_3527);
							}
							if (CBOOL(BgL__andtest_1142z00_3526))
								{	/* Unsafe/ftp.scm 559 */
									long BgL_arg1810z00_3529;

									BgL_arg1810z00_3529 =
										bgl_file_size(BSTRING_TO_STRING(BgL_pathz00_100));
									{	/* Unsafe/ftp.scm 559 */
										long BgL_xz00_4345;

										BgL_xz00_4345 =
											BGl_sendzd2filezd2zz__r4_input_6_10_2z00(BgL_pathz00_100,
											BgL_opz00_3521, BgL_arg1810z00_3529, ((long) 0));
										BgL_res2357z00_3531 = ((bool_t) 1);
								}}
							else
								{	/* Unsafe/ftp.scm 556 */
									BgL_res2357z00_3531 = ((bool_t) 0);
								}
						}
					else
						{	/* Unsafe/ftp.scm 555 */
							BgL_res2357z00_3531 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_res2357z00_3531);
			}
		}

	}



/* &ftp-put-file */
	obj_t BGl_z62ftpzd2putzd2filez62zz__ftpz00(obj_t BgL_envz00_4011,
		obj_t BgL_ftpz00_4012, obj_t BgL_pathz00_4013)
	{
		{	/* Unsafe/ftp.scm 755 */
			{	/* Unsafe/ftp.scm 756 */
				obj_t BgL_auxz00_6318;
				BgL_ftpz00_bglt BgL_auxz00_6310;

				if (STRINGP(BgL_pathz00_4013))
					{	/* Unsafe/ftp.scm 756 */
						BgL_auxz00_6318 = BgL_pathz00_4013;
					}
				else
					{
						obj_t BgL_auxz00_6321;

						BgL_auxz00_6321 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(28539L), BGl_string2598z00zz__ftpz00,
							BGl_string2532z00zz__ftpz00, BgL_pathz00_4013);
						FAILURE(BgL_auxz00_6321, BFALSE, BFALSE);
					}
				if (BGl_isazf3zf3zz__objectz00(BgL_ftpz00_4012, BGl_ftpz00zz__ftpz00))
					{	/* Unsafe/ftp.scm 756 */
						BgL_auxz00_6310 = ((BgL_ftpz00_bglt) BgL_ftpz00_4012);
					}
				else
					{
						obj_t BgL_auxz00_6314;

						BgL_auxz00_6314 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2522z00zz__ftpz00,
							BINT(28539L), BGl_string2598z00zz__ftpz00,
							BGl_string2497z00zz__ftpz00, BgL_ftpz00_4012);
						FAILURE(BgL_auxz00_6314, BFALSE, BFALSE);
					}
				return
					BGl_ftpzd2putzd2filez00zz__ftpz00(BgL_auxz00_6310, BgL_auxz00_6318);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			{	/* Unsafe/ftp.scm 52 */
				obj_t BgL_arg1914z00_2224;
				obj_t BgL_arg1916z00_2225;

				{	/* Unsafe/ftp.scm 52 */
					obj_t BgL_v1255z00_2231;

					BgL_v1255z00_2231 = create_vector(3L);
					VECTOR_SET(BgL_v1255z00_2231, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2602z00zz__ftpz00, BGl_proc2601z00zz__ftpz00,
							BGl_proc2600z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2599z00zz__ftpz00, BGl_symbol2604z00zz__ftpz00));
					VECTOR_SET(BgL_v1255z00_2231, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2609z00zz__ftpz00, BGl_proc2608z00zz__ftpz00,
							BGl_proc2607z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2606z00zz__ftpz00, BGl_symbol2604z00zz__ftpz00));
					VECTOR_SET(BgL_v1255z00_2231, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2614z00zz__ftpz00, BGl_proc2613z00zz__ftpz00,
							BGl_proc2612z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2611z00zz__ftpz00, BGl_symbol2616z00zz__ftpz00));
					BgL_arg1914z00_2224 = BgL_v1255z00_2231;
				}
				{	/* Unsafe/ftp.scm 52 */
					obj_t BgL_v1256z00_2271;

					BgL_v1256z00_2271 = create_vector(0L);
					BgL_arg1916z00_2225 = BgL_v1256z00_2271;
				}
				BGl_z52ftpz52zz__ftpz00 =
					BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2620z00zz__ftpz00,
					BGl_symbol2622z00zz__ftpz00, BGl_objectz00zz__objectz00, 39100L,
					BFALSE, BGl_proc2619z00zz__ftpz00, BFALSE, BGl_proc2618z00zz__ftpz00,
					BFALSE, BgL_arg1914z00_2224, BgL_arg1916z00_2225);
			}
			{	/* Unsafe/ftp.scm 57 */
				obj_t BgL_arg1946z00_2277;
				obj_t BgL_arg1947z00_2278;

				{	/* Unsafe/ftp.scm 57 */
					obj_t BgL_v1257z00_2297;

					BgL_v1257z00_2297 = create_vector(6L);
					VECTOR_SET(BgL_v1257z00_2297, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2627z00zz__ftpz00, BGl_proc2626z00zz__ftpz00,
							BGl_proc2625z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2624z00zz__ftpz00, BGl_symbol2628z00zz__ftpz00));
					VECTOR_SET(BgL_v1257z00_2297, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2493z00zz__ftpz00, BGl_proc2631z00zz__ftpz00,
							BGl_proc2630z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2629z00zz__ftpz00, BGl_symbol2632z00zz__ftpz00));
					VECTOR_SET(BgL_v1257z00_2297, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2636z00zz__ftpz00, BGl_proc2635z00zz__ftpz00,
							BGl_proc2634z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2633z00zz__ftpz00, BGl_symbol2628z00zz__ftpz00));
					VECTOR_SET(BgL_v1257z00_2297, 3L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2641z00zz__ftpz00, BGl_proc2640z00zz__ftpz00,
							BGl_proc2639z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2638z00zz__ftpz00, BGl_symbol2628z00zz__ftpz00));
					VECTOR_SET(BgL_v1257z00_2297, 4L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2646z00zz__ftpz00, BGl_proc2645z00zz__ftpz00,
							BGl_proc2644z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2643z00zz__ftpz00, BGl_symbol2628z00zz__ftpz00));
					VECTOR_SET(BgL_v1257z00_2297, 5L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2651z00zz__ftpz00, BGl_proc2650z00zz__ftpz00,
							BGl_proc2649z00zz__ftpz00, ((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2648z00zz__ftpz00, BGl_symbol2628z00zz__ftpz00));
					BgL_arg1946z00_2277 = BgL_v1257z00_2297;
				}
				{	/* Unsafe/ftp.scm 57 */
					obj_t BgL_v1258z00_2376;

					BgL_v1258z00_2376 = create_vector(0L);
					BgL_arg1947z00_2278 = BgL_v1258z00_2376;
				}
				BGl_ftpz00zz__ftpz00 =
					BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2656z00zz__ftpz00,
					BGl_symbol2622z00zz__ftpz00, BGl_z52ftpz52zz__ftpz00, 36300L,
					BGl_proc2655z00zz__ftpz00, BGl_proc2654z00zz__ftpz00, BFALSE,
					BGl_proc2653z00zz__ftpz00, BFALSE, BgL_arg1946z00_2277,
					BgL_arg1947z00_2278);
			}
			{	/* Unsafe/ftp.scm 64 */
				obj_t BgL_arg1998z00_2383;
				obj_t BgL_arg1999z00_2384;

				{	/* Unsafe/ftp.scm 64 */
					obj_t BgL_v1259z00_2400;

					BgL_v1259z00_2400 = create_vector(0L);
					BgL_arg1998z00_2383 = BgL_v1259z00_2400;
				}
				{	/* Unsafe/ftp.scm 64 */
					obj_t BgL_v1260z00_2401;

					BgL_v1260z00_2401 = create_vector(0L);
					BgL_arg1999z00_2384 = BgL_v1260z00_2401;
				}
				BGl_z62ftpzd2errorzb0zz__ftpz00 =
					BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2660z00zz__ftpz00,
					BGl_symbol2622z00zz__ftpz00, BGl_z62errorz62zz__objectz00, 27908L,
					BGl_proc2659z00zz__ftpz00, BGl_proc2658z00zz__ftpz00, BFALSE,
					BGl_proc2657z00zz__ftpz00, BFALSE, BgL_arg1998z00_2383,
					BgL_arg1999z00_2384);
			}
			{	/* Unsafe/ftp.scm 65 */
				obj_t BgL_arg2009z00_2408;
				obj_t BgL_arg2010z00_2409;

				{	/* Unsafe/ftp.scm 65 */
					obj_t BgL_v1261z00_2425;

					BgL_v1261z00_2425 = create_vector(0L);
					BgL_arg2009z00_2408 = BgL_v1261z00_2425;
				}
				{	/* Unsafe/ftp.scm 65 */
					obj_t BgL_v1262z00_2426;

					BgL_v1262z00_2426 = create_vector(0L);
					BgL_arg2010z00_2409 = BgL_v1262z00_2426;
				}
				return (BGl_z62ftpzd2parsezd2errorz62zz__ftpz00 =
					BGl_registerzd2classz12zc0zz__objectz00(BGl_symbol2665z00zz__ftpz00,
						BGl_symbol2622z00zz__ftpz00,
						BGl_z62iozd2parsezd2errorz62zz__objectz00, 61171L,
						BGl_proc2664z00zz__ftpz00, BGl_proc2663z00zz__ftpz00, BFALSE,
						BGl_proc2662z00zz__ftpz00, BFALSE, BgL_arg2009z00_2408,
						BgL_arg2010z00_2409), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2015> */
	obj_t BGl_z62zc3z04anonymousza32015ze3ze5zz__ftpz00(obj_t BgL_envz00_4052,
		obj_t BgL_new1071z00_4053)
	{
		{	/* Unsafe/ftp.scm 65 */
			{
				BgL_z62ftpzd2parsezd2errorz62_bglt BgL_auxz00_6356;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62ftpzd2parsezd2errorz62_bglt)
										BgL_new1071z00_4053))))->BgL_fnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62ftpzd2parsezd2errorz62_bglt)
										BgL_new1071z00_4053))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) (
										(BgL_z62ftpzd2parsezd2errorz62_bglt)
										BgL_new1071z00_4053))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62ftpzd2parsezd2errorz62_bglt)
										BgL_new1071z00_4053))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62ftpzd2parsezd2errorz62_bglt)
										BgL_new1071z00_4053))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) (
										(BgL_z62ftpzd2parsezd2errorz62_bglt)
										BgL_new1071z00_4053))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6356 =
					((BgL_z62ftpzd2parsezd2errorz62_bglt) BgL_new1071z00_4053);
				return ((obj_t) BgL_auxz00_6356);
			}
		}

	}



/* &lambda2013 */
	BgL_z62ftpzd2parsezd2errorz62_bglt BGl_z62lambda2013z62zz__ftpz00(obj_t
		BgL_envz00_4054)
	{
		{	/* Unsafe/ftp.scm 65 */
			{	/* Unsafe/ftp.scm 65 */
				BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1070z00_4480;

				BgL_new1070z00_4480 =
					((BgL_z62ftpzd2parsezd2errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62ftpzd2parsezd2errorz62_bgl))));
				{	/* Unsafe/ftp.scm 65 */
					long BgL_arg2014z00_4481;

					BgL_arg2014z00_4481 =
						BGL_CLASS_NUM(BGl_z62ftpzd2parsezd2errorz62zz__ftpz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1070z00_4480), BgL_arg2014z00_4481);
				}
				return BgL_new1070z00_4480;
			}
		}

	}



/* &lambda2011 */
	BgL_z62ftpzd2parsezd2errorz62_bglt BGl_z62lambda2011z62zz__ftpz00(obj_t
		BgL_envz00_4055, obj_t BgL_fname1064z00_4056,
		obj_t BgL_location1065z00_4057, obj_t BgL_stack1066z00_4058,
		obj_t BgL_proc1067z00_4059, obj_t BgL_msg1068z00_4060,
		obj_t BgL_obj1069z00_4061)
	{
		{	/* Unsafe/ftp.scm 65 */
			{	/* Unsafe/ftp.scm 65 */
				BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1166z00_4482;

				{	/* Unsafe/ftp.scm 65 */
					BgL_z62ftpzd2parsezd2errorz62_bglt BgL_new1165z00_4483;

					BgL_new1165z00_4483 =
						((BgL_z62ftpzd2parsezd2errorz62_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62ftpzd2parsezd2errorz62_bgl))));
					{	/* Unsafe/ftp.scm 65 */
						long BgL_arg2012z00_4484;

						BgL_arg2012z00_4484 =
							BGL_CLASS_NUM(BGl_z62ftpzd2parsezd2errorz62zz__ftpz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1165z00_4483), BgL_arg2012z00_4484);
					}
					BgL_new1166z00_4482 = BgL_new1165z00_4483;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1166z00_4482)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1064z00_4056), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1166z00_4482)))->BgL_locationz00) =
					((obj_t) BgL_location1065z00_4057), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1166z00_4482)))->BgL_stackz00) =
					((obj_t) BgL_stack1066z00_4058), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1166z00_4482)))->BgL_procz00) =
					((obj_t) BgL_proc1067z00_4059), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1166z00_4482)))->BgL_msgz00) =
					((obj_t) BgL_msg1068z00_4060), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1166z00_4482)))->BgL_objz00) =
					((obj_t) BgL_obj1069z00_4061), BUNSPEC);
				return BgL_new1166z00_4482;
			}
		}

	}



/* &<@anonymous:2004> */
	obj_t BGl_z62zc3z04anonymousza32004ze3ze5zz__ftpz00(obj_t BgL_envz00_4062,
		obj_t BgL_new1062z00_4063)
	{
		{	/* Unsafe/ftp.scm 64 */
			{
				BgL_z62ftpzd2errorzb0_bglt BgL_auxz00_6397;

				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt)
									((BgL_z62ftpzd2errorzb0_bglt) BgL_new1062z00_4063))))->
						BgL_fnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62ftpzd2errorzb0_bglt)
										BgL_new1062z00_4063))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62exceptionz62_bglt)
							COBJECT(((BgL_z62exceptionz62_bglt) ((BgL_z62ftpzd2errorzb0_bglt)
										BgL_new1062z00_4063))))->BgL_stackz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62ftpzd2errorzb0_bglt)
										BgL_new1062z00_4063))))->BgL_procz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62ftpzd2errorzb0_bglt)
										BgL_new1062z00_4063))))->BgL_msgz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z62errorz62_bglt)
							COBJECT(((BgL_z62errorz62_bglt) ((BgL_z62ftpzd2errorzb0_bglt)
										BgL_new1062z00_4063))))->BgL_objz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6397 = ((BgL_z62ftpzd2errorzb0_bglt) BgL_new1062z00_4063);
				return ((obj_t) BgL_auxz00_6397);
			}
		}

	}



/* &lambda2002 */
	BgL_z62ftpzd2errorzb0_bglt BGl_z62lambda2002z62zz__ftpz00(obj_t
		BgL_envz00_4064)
	{
		{	/* Unsafe/ftp.scm 64 */
			{	/* Unsafe/ftp.scm 64 */
				BgL_z62ftpzd2errorzb0_bglt BgL_new1061z00_4486;

				BgL_new1061z00_4486 =
					((BgL_z62ftpzd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z62ftpzd2errorzb0_bgl))));
				{	/* Unsafe/ftp.scm 64 */
					long BgL_arg2003z00_4487;

					BgL_arg2003z00_4487 = BGL_CLASS_NUM(BGl_z62ftpzd2errorzb0zz__ftpz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1061z00_4486), BgL_arg2003z00_4487);
				}
				return BgL_new1061z00_4486;
			}
		}

	}



/* &lambda2000 */
	BgL_z62ftpzd2errorzb0_bglt BGl_z62lambda2000z62zz__ftpz00(obj_t
		BgL_envz00_4065, obj_t BgL_fname1055z00_4066,
		obj_t BgL_location1056z00_4067, obj_t BgL_stack1057z00_4068,
		obj_t BgL_proc1058z00_4069, obj_t BgL_msg1059z00_4070,
		obj_t BgL_obj1060z00_4071)
	{
		{	/* Unsafe/ftp.scm 64 */
			{	/* Unsafe/ftp.scm 64 */
				BgL_z62ftpzd2errorzb0_bglt BgL_new1164z00_4488;

				{	/* Unsafe/ftp.scm 64 */
					BgL_z62ftpzd2errorzb0_bglt BgL_new1163z00_4489;

					BgL_new1163z00_4489 =
						((BgL_z62ftpzd2errorzb0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_z62ftpzd2errorzb0_bgl))));
					{	/* Unsafe/ftp.scm 64 */
						long BgL_arg2001z00_4490;

						BgL_arg2001z00_4490 =
							BGL_CLASS_NUM(BGl_z62ftpzd2errorzb0zz__ftpz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1163z00_4489), BgL_arg2001z00_4490);
					}
					BgL_new1164z00_4488 = BgL_new1163z00_4489;
				}
				((((BgL_z62exceptionz62_bglt) COBJECT(
								((BgL_z62exceptionz62_bglt) BgL_new1164z00_4488)))->
						BgL_fnamez00) = ((obj_t) BgL_fname1055z00_4066), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1164z00_4488)))->BgL_locationz00) =
					((obj_t) BgL_location1056z00_4067), BUNSPEC);
				((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
									BgL_new1164z00_4488)))->BgL_stackz00) =
					((obj_t) BgL_stack1057z00_4068), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1164z00_4488)))->BgL_procz00) =
					((obj_t) BgL_proc1058z00_4069), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1164z00_4488)))->BgL_msgz00) =
					((obj_t) BgL_msg1059z00_4070), BUNSPEC);
				((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
									BgL_new1164z00_4488)))->BgL_objz00) =
					((obj_t) BgL_obj1060z00_4071), BUNSPEC);
				return BgL_new1164z00_4488;
			}
		}

	}



/* &<@anonymous:1952> */
	obj_t BGl_z62zc3z04anonymousza31952ze3ze5zz__ftpz00(obj_t BgL_envz00_4072,
		obj_t BgL_new1053z00_4073)
	{
		{	/* Unsafe/ftp.scm 57 */
			{
				BgL_ftpz00_bglt BgL_auxz00_6438;

				((((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt)
									((BgL_ftpz00_bglt) BgL_new1053z00_4073))))->BgL_cmdz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt) ((BgL_ftpz00_bglt)
										BgL_new1053z00_4073))))->BgL_dtpz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt) ((BgL_ftpz00_bglt)
										BgL_new1053z00_4073))))->BgL_passivezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_ftpz00_bglt) COBJECT(((BgL_ftpz00_bglt) BgL_new1053z00_4073)))->
						BgL_hostz00) = ((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
				((((BgL_ftpz00_bglt) COBJECT(((BgL_ftpz00_bglt) BgL_new1053z00_4073)))->
						BgL_portz00) = ((obj_t) BINT(0L)), BUNSPEC);
				((((BgL_ftpz00_bglt) COBJECT(((BgL_ftpz00_bglt) BgL_new1053z00_4073)))->
						BgL_motdz00) = ((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
				((((BgL_ftpz00_bglt) COBJECT(((BgL_ftpz00_bglt) BgL_new1053z00_4073)))->
						BgL_userz00) = ((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
				((((BgL_ftpz00_bglt) COBJECT(((BgL_ftpz00_bglt) BgL_new1053z00_4073)))->
						BgL_passz00) = ((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
				((((BgL_ftpz00_bglt) COBJECT(((BgL_ftpz00_bglt) BgL_new1053z00_4073)))->
						BgL_acctz00) = ((obj_t) BGl_string2517z00zz__ftpz00), BUNSPEC);
				BgL_auxz00_6438 = ((BgL_ftpz00_bglt) BgL_new1053z00_4073);
				return ((obj_t) BgL_auxz00_6438);
			}
		}

	}



/* &lambda1950 */
	BgL_ftpz00_bglt BGl_z62lambda1950z62zz__ftpz00(obj_t BgL_envz00_4074)
	{
		{	/* Unsafe/ftp.scm 57 */
			{	/* Unsafe/ftp.scm 57 */
				BgL_ftpz00_bglt BgL_new1052z00_4492;

				BgL_new1052z00_4492 =
					((BgL_ftpz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_ftpz00_bgl))));
				{	/* Unsafe/ftp.scm 57 */
					long BgL_arg1951z00_4493;

					BgL_arg1951z00_4493 = BGL_CLASS_NUM(BGl_ftpz00zz__ftpz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1052z00_4492), BgL_arg1951z00_4493);
				}
				return BgL_new1052z00_4492;
			}
		}

	}



/* &lambda1948 */
	BgL_ftpz00_bglt BGl_z62lambda1948z62zz__ftpz00(obj_t BgL_envz00_4075,
		obj_t BgL_cmd1043z00_4076, obj_t BgL_dtp1044z00_4077,
		obj_t BgL_passivezf31045zf3_4078, obj_t BgL_host1046z00_4079,
		obj_t BgL_port1047z00_4080, obj_t BgL_motd1048z00_4081,
		obj_t BgL_user1049z00_4082, obj_t BgL_pass1050z00_4083,
		obj_t BgL_acct1051z00_4084)
	{
		{	/* Unsafe/ftp.scm 57 */
			{	/* Unsafe/ftp.scm 57 */
				bool_t BgL_passivezf31045zf3_4494;

				BgL_passivezf31045zf3_4494 = CBOOL(BgL_passivezf31045zf3_4078);
				{	/* Unsafe/ftp.scm 57 */
					BgL_ftpz00_bglt BgL_new1162z00_4501;

					{	/* Unsafe/ftp.scm 57 */
						BgL_ftpz00_bglt BgL_new1161z00_4502;

						BgL_new1161z00_4502 =
							((BgL_ftpz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_ftpz00_bgl))));
						{	/* Unsafe/ftp.scm 57 */
							long BgL_arg1949z00_4503;

							BgL_arg1949z00_4503 = BGL_CLASS_NUM(BGl_ftpz00zz__ftpz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1161z00_4502),
								BgL_arg1949z00_4503);
						}
						BgL_new1162z00_4501 = BgL_new1161z00_4502;
					}
					((((BgL_z52ftpz52_bglt) COBJECT(
									((BgL_z52ftpz52_bglt) BgL_new1162z00_4501)))->BgL_cmdz00) =
						((obj_t) BgL_cmd1043z00_4076), BUNSPEC);
					((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
										BgL_new1162z00_4501)))->BgL_dtpz00) =
						((obj_t) BgL_dtp1044z00_4077), BUNSPEC);
					((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
										BgL_new1162z00_4501)))->BgL_passivezf3zf3) =
						((bool_t) BgL_passivezf31045zf3_4494), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1162z00_4501))->BgL_hostz00) =
						((obj_t) ((obj_t) BgL_host1046z00_4079)), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1162z00_4501))->BgL_portz00) =
						((obj_t) ((obj_t) BgL_port1047z00_4080)), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1162z00_4501))->BgL_motdz00) =
						((obj_t) ((obj_t) BgL_motd1048z00_4081)), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1162z00_4501))->BgL_userz00) =
						((obj_t) ((obj_t) BgL_user1049z00_4082)), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1162z00_4501))->BgL_passz00) =
						((obj_t) ((obj_t) BgL_pass1050z00_4083)), BUNSPEC);
					((((BgL_ftpz00_bglt) COBJECT(BgL_new1162z00_4501))->BgL_acctz00) =
						((obj_t) ((obj_t) BgL_acct1051z00_4084)), BUNSPEC);
					return BgL_new1162z00_4501;
				}
			}
		}

	}



/* &<@anonymous:1994> */
	obj_t BGl_z62zc3z04anonymousza31994ze3ze5zz__ftpz00(obj_t BgL_envz00_4085)
	{
		{	/* Unsafe/ftp.scm 57 */
			return BGl_string2517z00zz__ftpz00;
		}

	}



/* &lambda1993 */
	obj_t BGl_z62lambda1993z62zz__ftpz00(obj_t BgL_envz00_4086,
		obj_t BgL_oz00_4087, obj_t BgL_vz00_4088)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				((((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4087)))->BgL_acctz00) = ((obj_t)
					((obj_t) BgL_vz00_4088)), BUNSPEC);
		}

	}



/* &lambda1992 */
	obj_t BGl_z62lambda1992z62zz__ftpz00(obj_t BgL_envz00_4089,
		obj_t BgL_oz00_4090)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				(((BgL_ftpz00_bglt) COBJECT(
						((BgL_ftpz00_bglt) BgL_oz00_4090)))->BgL_acctz00);
		}

	}



/* &<@anonymous:1987> */
	obj_t BGl_z62zc3z04anonymousza31987ze3ze5zz__ftpz00(obj_t BgL_envz00_4091)
	{
		{	/* Unsafe/ftp.scm 57 */
			return BGl_string2519z00zz__ftpz00;
		}

	}



/* &lambda1986 */
	obj_t BGl_z62lambda1986z62zz__ftpz00(obj_t BgL_envz00_4092,
		obj_t BgL_oz00_4093, obj_t BgL_vz00_4094)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				((((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4093)))->BgL_passz00) = ((obj_t)
					((obj_t) BgL_vz00_4094)), BUNSPEC);
		}

	}



/* &lambda1985 */
	obj_t BGl_z62lambda1985z62zz__ftpz00(obj_t BgL_envz00_4095,
		obj_t BgL_oz00_4096)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				(((BgL_ftpz00_bglt) COBJECT(
						((BgL_ftpz00_bglt) BgL_oz00_4096)))->BgL_passz00);
		}

	}



/* &<@anonymous:1980> */
	obj_t BGl_z62zc3z04anonymousza31980ze3ze5zz__ftpz00(obj_t BgL_envz00_4097)
	{
		{	/* Unsafe/ftp.scm 57 */
			return BGl_string2518z00zz__ftpz00;
		}

	}



/* &lambda1979 */
	obj_t BGl_z62lambda1979z62zz__ftpz00(obj_t BgL_envz00_4098,
		obj_t BgL_oz00_4099, obj_t BgL_vz00_4100)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				((((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4099)))->BgL_userz00) = ((obj_t)
					((obj_t) BgL_vz00_4100)), BUNSPEC);
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zz__ftpz00(obj_t BgL_envz00_4101,
		obj_t BgL_oz00_4102)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				(((BgL_ftpz00_bglt) COBJECT(
						((BgL_ftpz00_bglt) BgL_oz00_4102)))->BgL_userz00);
		}

	}



/* &<@anonymous:1973> */
	obj_t BGl_z62zc3z04anonymousza31973ze3ze5zz__ftpz00(obj_t BgL_envz00_4103)
	{
		{	/* Unsafe/ftp.scm 57 */
			return BGl_string2517z00zz__ftpz00;
		}

	}



/* &lambda1972 */
	obj_t BGl_z62lambda1972z62zz__ftpz00(obj_t BgL_envz00_4104,
		obj_t BgL_oz00_4105, obj_t BgL_vz00_4106)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				((((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4105)))->BgL_motdz00) = ((obj_t)
					((obj_t) BgL_vz00_4106)), BUNSPEC);
		}

	}



/* &lambda1971 */
	obj_t BGl_z62lambda1971z62zz__ftpz00(obj_t BgL_envz00_4107,
		obj_t BgL_oz00_4108)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				(((BgL_ftpz00_bglt) COBJECT(
						((BgL_ftpz00_bglt) BgL_oz00_4108)))->BgL_motdz00);
		}

	}



/* &<@anonymous:1966> */
	obj_t BGl_z62zc3z04anonymousza31966ze3ze5zz__ftpz00(obj_t BgL_envz00_4109)
	{
		{	/* Unsafe/ftp.scm 57 */
			return BINT(21L);
		}

	}



/* &lambda1965 */
	obj_t BGl_z62lambda1965z62zz__ftpz00(obj_t BgL_envz00_4110,
		obj_t BgL_oz00_4111, obj_t BgL_vz00_4112)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				((((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4111)))->BgL_portz00) = ((obj_t)
					((obj_t) BgL_vz00_4112)), BUNSPEC);
		}

	}



/* &lambda1964 */
	obj_t BGl_z62lambda1964z62zz__ftpz00(obj_t BgL_envz00_4113,
		obj_t BgL_oz00_4114)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				(((BgL_ftpz00_bglt) COBJECT(
						((BgL_ftpz00_bglt) BgL_oz00_4114)))->BgL_portz00);
		}

	}



/* &<@anonymous:1959> */
	obj_t BGl_z62zc3z04anonymousza31959ze3ze5zz__ftpz00(obj_t BgL_envz00_4115)
	{
		{	/* Unsafe/ftp.scm 57 */
			return BGl_string2508z00zz__ftpz00;
		}

	}



/* &lambda1958 */
	obj_t BGl_z62lambda1958z62zz__ftpz00(obj_t BgL_envz00_4116,
		obj_t BgL_oz00_4117, obj_t BgL_vz00_4118)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				((((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4117)))->BgL_hostz00) = ((obj_t)
					((obj_t) BgL_vz00_4118)), BUNSPEC);
		}

	}



/* &lambda1957 */
	obj_t BGl_z62lambda1957z62zz__ftpz00(obj_t BgL_envz00_4119,
		obj_t BgL_oz00_4120)
	{
		{	/* Unsafe/ftp.scm 57 */
			return
				(((BgL_ftpz00_bglt) COBJECT(
						((BgL_ftpz00_bglt) BgL_oz00_4120)))->BgL_hostz00);
		}

	}



/* &<@anonymous:1919> */
	obj_t BGl_z62zc3z04anonymousza31919ze3ze5zz__ftpz00(obj_t BgL_envz00_4121,
		obj_t BgL_new1041z00_4122)
	{
		{	/* Unsafe/ftp.scm 52 */
			{
				BgL_z52ftpz52_bglt BgL_auxz00_6521;

				((((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_new1041z00_4122)))->BgL_cmdz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
									BgL_new1041z00_4122)))->BgL_dtpz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_z52ftpz52_bglt) COBJECT(((BgL_z52ftpz52_bglt)
									BgL_new1041z00_4122)))->BgL_passivezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_6521 = ((BgL_z52ftpz52_bglt) BgL_new1041z00_4122);
				return ((obj_t) BgL_auxz00_6521);
			}
		}

	}



/* &lambda1917 */
	BgL_z52ftpz52_bglt BGl_z62lambda1917z62zz__ftpz00(obj_t BgL_envz00_4123)
	{
		{	/* Unsafe/ftp.scm 52 */
			{	/* Unsafe/ftp.scm 52 */
				BgL_z52ftpz52_bglt BgL_new1040z00_4523;

				BgL_new1040z00_4523 =
					((BgL_z52ftpz52_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_z52ftpz52_bgl))));
				{	/* Unsafe/ftp.scm 52 */
					long BgL_arg1918z00_4524;

					BgL_arg1918z00_4524 = BGL_CLASS_NUM(BGl_z52ftpz52zz__ftpz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1040z00_4523), BgL_arg1918z00_4524);
				}
				return BgL_new1040z00_4523;
			}
		}

	}



/* &<@anonymous:1942> */
	obj_t BGl_z62zc3z04anonymousza31942ze3ze5zz__ftpz00(obj_t BgL_envz00_4124)
	{
		{	/* Unsafe/ftp.scm 52 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1941 */
	obj_t BGl_z62lambda1941z62zz__ftpz00(obj_t BgL_envz00_4125,
		obj_t BgL_oz00_4126, obj_t BgL_vz00_4127)
	{
		{	/* Unsafe/ftp.scm 52 */
			{	/* Unsafe/ftp.scm 52 */
				bool_t BgL_vz00_4526;

				BgL_vz00_4526 = CBOOL(BgL_vz00_4127);
				return
					((((BgL_z52ftpz52_bglt) COBJECT(
								((BgL_z52ftpz52_bglt) BgL_oz00_4126)))->BgL_passivezf3zf3) =
					((bool_t) BgL_vz00_4526), BUNSPEC);
			}
		}

	}



/* &lambda1940 */
	obj_t BGl_z62lambda1940z62zz__ftpz00(obj_t BgL_envz00_4128,
		obj_t BgL_oz00_4129)
	{
		{	/* Unsafe/ftp.scm 52 */
			return
				BBOOL(
				(((BgL_z52ftpz52_bglt) COBJECT(
							((BgL_z52ftpz52_bglt) BgL_oz00_4129)))->BgL_passivezf3zf3));
		}

	}



/* &<@anonymous:1935> */
	obj_t BGl_z62zc3z04anonymousza31935ze3ze5zz__ftpz00(obj_t BgL_envz00_4130)
	{
		{	/* Unsafe/ftp.scm 52 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1934 */
	obj_t BGl_z62lambda1934z62zz__ftpz00(obj_t BgL_envz00_4131,
		obj_t BgL_oz00_4132, obj_t BgL_vz00_4133)
	{
		{	/* Unsafe/ftp.scm 52 */
			return
				((((BgL_z52ftpz52_bglt) COBJECT(
							((BgL_z52ftpz52_bglt) BgL_oz00_4132)))->BgL_dtpz00) =
				((obj_t) BgL_vz00_4133), BUNSPEC);
		}

	}



/* &lambda1933 */
	obj_t BGl_z62lambda1933z62zz__ftpz00(obj_t BgL_envz00_4134,
		obj_t BgL_oz00_4135)
	{
		{	/* Unsafe/ftp.scm 52 */
			return
				(((BgL_z52ftpz52_bglt) COBJECT(
						((BgL_z52ftpz52_bglt) BgL_oz00_4135)))->BgL_dtpz00);
		}

	}



/* &<@anonymous:1928> */
	obj_t BGl_z62zc3z04anonymousza31928ze3ze5zz__ftpz00(obj_t BgL_envz00_4136)
	{
		{	/* Unsafe/ftp.scm 52 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1927 */
	obj_t BGl_z62lambda1927z62zz__ftpz00(obj_t BgL_envz00_4137,
		obj_t BgL_oz00_4138, obj_t BgL_vz00_4139)
	{
		{	/* Unsafe/ftp.scm 52 */
			return
				((((BgL_z52ftpz52_bglt) COBJECT(
							((BgL_z52ftpz52_bglt) BgL_oz00_4138)))->BgL_cmdz00) =
				((obj_t) BgL_vz00_4139), BUNSPEC);
		}

	}



/* &lambda1926 */
	obj_t BGl_z62lambda1926z62zz__ftpz00(obj_t BgL_envz00_4140,
		obj_t BgL_oz00_4141)
	{
		{	/* Unsafe/ftp.scm 52 */
			return
				(((BgL_z52ftpz52_bglt) COBJECT(
						((BgL_z52ftpz52_bglt) BgL_oz00_4141)))->BgL_cmdz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2printzd2envz00zz__objectz00, BGl_ftpz00zz__ftpz00,
				BGl_proc2667z00zz__ftpz00, BGl_string2668z00zz__ftpz00);
		}

	}



/* &object-print-ftp1283 */
	obj_t BGl_z62objectzd2printzd2ftp1283z62zz__ftpz00(obj_t BgL_envz00_4143,
		obj_t BgL_oz00_4144, obj_t BgL_pz00_4145, obj_t BgL_printzd2slotzd2_4146)
	{
		{	/* Unsafe/ftp.scm 113 */
			{	/* Unsafe/ftp.scm 115 */
				obj_t BgL_tmpz00_6552;

				BgL_tmpz00_6552 = ((obj_t) BgL_pz00_4145);
				bgl_display_string(BGl_string2669z00zz__ftpz00, BgL_tmpz00_6552);
			}
			{	/* Unsafe/ftp.scm 116 */
				obj_t BgL_arg2016z00_4533;

				BgL_arg2016z00_4533 =
					(((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4144)))->BgL_hostz00);
				BGL_PROCEDURE_CALL2(BgL_printzd2slotzd2_4146, BgL_arg2016z00_4533,
					BgL_pz00_4145);
			}
			{	/* Unsafe/ftp.scm 117 */
				obj_t BgL_tmpz00_6562;

				BgL_tmpz00_6562 = ((obj_t) BgL_pz00_4145);
				bgl_display_string(BGl_string2670z00zz__ftpz00, BgL_tmpz00_6562);
			}
			{	/* Unsafe/ftp.scm 118 */
				obj_t BgL_arg2017z00_4534;

				BgL_arg2017z00_4534 =
					(((BgL_ftpz00_bglt) COBJECT(
							((BgL_ftpz00_bglt) BgL_oz00_4144)))->BgL_userz00);
				BGL_PROCEDURE_CALL2(BgL_printzd2slotzd2_4146, BgL_arg2017z00_4534,
					BgL_pz00_4145);
			}
			{	/* Unsafe/ftp.scm 119 */
				obj_t BgL_tmpz00_6572;

				BgL_tmpz00_6572 = ((obj_t) BgL_pz00_4145);
				bgl_display_string(BGl_string2671z00zz__ftpz00, BgL_tmpz00_6572);
			}
			{	/* Unsafe/ftp.scm 120 */
				obj_t BgL_arg2018z00_4535;

				BgL_arg2018z00_4535 =
					(((BgL_z52ftpz52_bglt) COBJECT(
							((BgL_z52ftpz52_bglt)
								((BgL_ftpz00_bglt) BgL_oz00_4144))))->BgL_dtpz00);
				BGL_PROCEDURE_CALL2(BgL_printzd2slotzd2_4146, BgL_arg2018z00_4535,
					BgL_pz00_4145);
			}
			{	/* Unsafe/ftp.scm 121 */
				obj_t BgL_tmpz00_6583;

				BgL_tmpz00_6583 = ((obj_t) BgL_pz00_4145);
				bgl_display_string(BGl_string2672z00zz__ftpz00, BgL_tmpz00_6583);
			}
			{	/* Unsafe/ftp.scm 122 */
				bool_t BgL_arg2019z00_4536;

				BgL_arg2019z00_4536 =
					(((BgL_z52ftpz52_bglt) COBJECT(
							((BgL_z52ftpz52_bglt)
								((BgL_ftpz00_bglt) BgL_oz00_4144))))->BgL_passivezf3zf3);
				bgl_display_obj(BBOOL(BgL_arg2019z00_4536), BgL_pz00_4145);
			}
			{	/* Unsafe/ftp.scm 123 */
				obj_t BgL_tmpz00_6591;

				BgL_tmpz00_6591 = ((obj_t) BgL_pz00_4145);
				return bgl_display_string(BGl_string2673z00zz__ftpz00, BgL_tmpz00_6591);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__ftpz00(void)
	{
		{	/* Unsafe/ftp.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2623z00zz__ftpz00));
			return
				BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2623z00zz__ftpz00));
		}

	}

#ifdef __cplusplus
}
#endif
