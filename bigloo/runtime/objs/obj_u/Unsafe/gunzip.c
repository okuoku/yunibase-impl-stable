/*===========================================================================*/
/*   (Unsafe/gunzip.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/gunzip.scm -indent -o objs/obj_u/Unsafe/gunzip.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___GUNZIP_TYPE_DEFINITIONS
#define BGL___GUNZIP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62iozd2parsezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2parsezd2errorz62_bglt;

	typedef struct BgL_huftz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_ez00;
		long BgL_bz00;
		obj_t BgL_vz00;
	}              *BgL_huftz00_bglt;


#endif													// BGL___GUNZIP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_inflatez00zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62loop2313z62zz__gunza7ipza7(long, obj_t, obj_t, obj_t);
	static obj_t BGl_z62loop2314z62zz__gunza7ipza7(obj_t, obj_t, obj_t, obj_t,
		obj_t, long, long);
	extern long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	BGL_EXPORTED_DECL obj_t BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_portzd2ze3portz31zz__gunza7ipza7(obj_t, obj_t, obj_t, long,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_list2380z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_list2382z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_list2384z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_list2386z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__gunza7ipza7 = BUNSPEC;
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31944ze3ze5zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_huftz00zz__gunza7ipza7 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_portzd2ze3inflatezd2portze3zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_list2391z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_z62gunza7ipzd2sendcharsz17zz__gunza7ipza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list2394z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_z62loopzd2inflatezb0zz__gunza7ipza7(obj_t, long, obj_t,
		obj_t, long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, long, obj_t);
	static obj_t BGl__openzd2inputzd2za7libzd2filez75zz__gunza7ipza7(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__gunza7ipza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl__openzd2inputzd2gza7ipzd2filez75zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31550ze3ze5zz__gunza7ipza7(obj_t);
	extern obj_t BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(obj_t, obj_t);
	extern obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static BgL_huftz00_bglt BGl_z62lambda1969z62zz__gunza7ipza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__gunza7ipza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2inflatezd2filezd2zz__gunza7ipza7(obj_t, obj_t, obj_t);
	static BgL_huftz00_bglt
		BGl_z62zc3z04anonymousza31640ze3ze5zz__gunza7ipza7(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31519ze3ze5zz__gunza7ipza7(obj_t);
	static long BGl_readzd2int2ze70z35zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31963ze3ze5zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31947ze3ze5zz__gunza7ipza7(obj_t, obj_t);
	static BgL_huftz00_bglt BGl_z62lambda1971z62zz__gunza7ipza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31963ze32321ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1977z62zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31963ze32322ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1978z62zz__gunza7ipza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31963ze32323ze5zz__gunza7ipza7(obj_t,
		obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl__openzd2inputzd2inflatezd2filezd2zz__gunza7ipza7(obj_t,
		obj_t);
	static bool_t BGl_setzd2litze70z35zz__gunza7ipza7(obj_t, obj_t, obj_t, long,
		long, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__gunza7ipza7(void);
	extern obj_t BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		int);
	static obj_t BGl_z62lambda1982z62zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1983z62zz__gunza7ipza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
	static obj_t BGl_genericzd2initzd2zz__gunza7ipza7(void);
	static obj_t BGl_z62lambda1987z62zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1988z62zz__gunza7ipza7(obj_t, obj_t, obj_t);
	static obj_t BGl_vector2379z00zz__gunza7ipza7 = BUNSPEC;
	extern obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__gunza7ipza7(void);
	extern obj_t bgl_open_input_gzip_port(obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__gunza7ipza7(void);
	static obj_t BGl_z62zc3z04anonymousza31561ze3ze5zz__gunza7ipza7(obj_t);
	extern bool_t BGl_positivezf3zf3zz__r4_numbers_6_5z00(obj_t);
	extern obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31973ze3ze5zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zz__gunza7ipza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_vector2381z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_huftzd2buildze70z35zz__gunza7ipza7(obj_t, obj_t, long, long,
		obj_t, obj_t, long, bool_t);
	static obj_t BGl_vector2383z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_vector2385z00zz__gunza7ipza7 = BUNSPEC;
	extern bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_objectz00zz__objectz00;
	extern obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_vector2390z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_vector2392z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_vector2393z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl__portzd2ze3inflatezd2portze3zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62checkzd2flushzb0zz__gunza7ipza7(long, obj_t);
	static obj_t BGl_z62loopz62zz__gunza7ipza7(obj_t, obj_t, obj_t, obj_t, obj_t,
		long, obj_t);
	static obj_t BGl_symbol2414z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_symbol2416z00zz__gunza7ipza7 = BUNSPEC;
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static obj_t BGl_symbol2418z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl__portzd2ze3za7libzd2portz44zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31564ze3ze5zz__gunza7ipza7(obj_t);
	static obj_t BGl__portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(obj_t, obj_t);
	extern obj_t
		BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_symbol2420z00zz__gunza7ipza7 = BUNSPEC;
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t make_vector(long, obj_t);
	extern obj_t c_substring(obj_t, long, long);
	static bool_t BGl_jumpzd2tozd2nextze70ze7zz__gunza7ipza7(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31420ze3ze5zz__gunza7ipza7(obj_t);
	static obj_t BGl_z62z52dozd2copy2ze2zz__gunza7ipza7(obj_t, obj_t, obj_t,
		obj_t, obj_t, long, obj_t, long, obj_t, obj_t, long, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_readzd2charszd2zz__r4_input_6_10_2z00(obj_t, obj_t);
	extern long default_io_bufsiz;
	extern obj_t BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_za7erozf3z54zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__gunza7ipza7(void);
	static obj_t BGl_symbol2437z00zz__gunza7ipza7 = BUNSPEC;
	extern obj_t BGl_readzd2charzd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_symbol2439z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_z62inflatezd2sendcharszb0zz__gunza7ipza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_inflatezd2entryzd2zz__gunza7ipza7(obj_t, obj_t);
	static obj_t BGl_symbol2443z00zz__gunza7ipza7 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31947ze32318ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_symbol2447z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31947ze32319ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_symbol2449z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_symbol2368z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_readzd2nullzd2termzd2stringze70z35zz__gunza7ipza7(obj_t);
	extern bool_t rgc_fill_buffer(obj_t);
	static obj_t BGl_subvectorz00zz__gunza7ipza7(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31947ze32320ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31944ze32315ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31944ze32316ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_symbol2370z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31944ze32317ze5zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_symbol2372z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_symbol2454z00zz__gunza7ipza7 = BUNSPEC;
	static obj_t BGl_symbol2374z00zz__gunza7ipza7 = BUNSPEC;
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_symbol2456z00zz__gunza7ipza7 = BUNSPEC;
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31415ze3ze5zz__gunza7ipza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31932ze3ze5zz__gunza7ipza7(obj_t);
	static bool_t BGl_z62NEEDBITSz62zz__gunza7ipza7(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_z62checkzd2adler32zb0zz__gunza7ipza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62laapz62zz__gunza7ipza7(long, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, long, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2za7libzd2filez75zz__gunza7ipza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62gunza7ipzd2parsezd2headerzc5zz__gunza7ipza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_openzd2inputzd2gza7ipzd2filez75zz__gunza7ipza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inflatezd2sendcharszd2zz__gunza7ipza7(obj_t,
		obj_t);
	static obj_t BGl_buildzd2vectorzd2zz__gunza7ipza7(long, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_gunza7ipzd2parsezd2headerzd2envz75zz__gunza7ipza7,
		BgL_bgl_za762gunza7a7ipza7d2pa2458za7,
		BGl_z62gunza7ipzd2parsezd2headerzc5zz__gunza7ipza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2inflatezd2filezd2envz00zz__gunza7ipza7,
		BgL_bgl__openza7d2inputza7d22459z00, opt_generic_entry,
		BGl__openzd2inputzd2inflatezd2filezd2zz__gunza7ipza7, BFALSE, -1);
	      DEFINE_STRING(BGl_string2400z00zz__gunza7ipza7,
		BgL_bgl_string2400za700za7za7_2460za7, "premature end of file", 21);
	      DEFINE_STRING(BGl_string2401z00zz__gunza7ipza7,
		BgL_bgl_string2401za700za7za7_2461za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string2402z00zz__gunza7ipza7,
		BgL_bgl_string2402za700za7za7_2462za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string2403z00zz__gunza7ipza7,
		BgL_bgl_string2403za700za7za7_2463za7, "bad inflate code `~a'", 21);
	      DEFINE_STRING(BGl_string2404z00zz__gunza7ipza7,
		BgL_bgl_string2404za700za7za7_2464za7, "bad header `~a'", 15);
	      DEFINE_STRING(BGl_string2405z00zz__gunza7ipza7,
		BgL_bgl_string2405za700za7za7_2465za7, "gunzip", 6);
	      DEFINE_STRING(BGl_string2406z00zz__gunza7ipza7,
		BgL_bgl_string2406za700za7za7_2466za7, "unknown compression type `~a'", 29);
	      DEFINE_STRING(BGl_string2407z00zz__gunza7ipza7,
		BgL_bgl_string2407za700za7za7_2467za7, "cannot unzip encrypted file", 27);
	      DEFINE_STRING(BGl_string2408z00zz__gunza7ipza7,
		BgL_bgl_string2408za700za7za7_2468za7, "cannot handle multi-part files",
		30);
	      DEFINE_STRING(BGl_string2409z00zz__gunza7ipza7,
		BgL_bgl_string2409za700za7za7_2469za7,
		"/tmp/bigloo/runtime/Unsafe/gunzip.scm", 37);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_portzd2ze3inflatezd2portzd2envz31zz__gunza7ipza7,
		BgL_bgl__portza7d2za7e3infla2470z00, opt_generic_entry,
		BGl__portzd2ze3inflatezd2portze3zz__gunza7ipza7, BFALSE, -1);
	      DEFINE_STRING(BGl_string2410z00zz__gunza7ipza7,
		BgL_bgl_string2410za700za7za7_2471za7, "&gunzip-sendchars", 17);
	      DEFINE_STRING(BGl_string2411z00zz__gunza7ipza7,
		BgL_bgl_string2411za700za7za7_2472za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2412z00zz__gunza7ipza7,
		BgL_bgl_string2412za700za7za7_2473za7, "output-port", 11);
	      DEFINE_STRING(BGl_string2413z00zz__gunza7ipza7,
		BgL_bgl_string2413za700za7za7_2474za7, "&inflate-sendchars", 18);
	      DEFINE_STRING(BGl_string2415z00zz__gunza7ipza7,
		BgL_bgl_string2415za700za7za7_2475za7, "eof", 3);
	      DEFINE_STRING(BGl_string2417z00zz__gunza7ipza7,
		BgL_bgl_string2417za700za7za7_2476za7, "resume", 6);
	      DEFINE_STRING(BGl_string2419z00zz__gunza7ipza7,
		BgL_bgl_string2419za700za7za7_2477za7, "port->gzip-port", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2za7libzd2filezd2envza7zz__gunza7ipza7,
		BgL_bgl__openza7d2inputza7d22478z00, opt_generic_entry,
		BGl__openzd2inputzd2za7libzd2filez75zz__gunza7ipza7, BFALSE, -1);
	      DEFINE_STRING(BGl_string2421z00zz__gunza7ipza7,
		BgL_bgl_string2421za700za7za7_2479za7, "port->inflate-port", 18);
	      DEFINE_STRING(BGl_string2422z00zz__gunza7ipza7,
		BgL_bgl_string2422za700za7za7_2480za7, "port->port", 10);
	      DEFINE_STRING(BGl_string2423z00zz__gunza7ipza7,
		BgL_bgl_string2423za700za7za7_2481za7, "_port->inflate-port", 19);
	      DEFINE_STRING(BGl_string2424z00zz__gunza7ipza7,
		BgL_bgl_string2424za700za7za7_2482za7, "_port->gzip-port", 16);
	      DEFINE_STRING(BGl_string2425z00zz__gunza7ipza7,
		BgL_bgl_string2425za700za7za7_2483za7, "_open-input-gzip-file", 21);
	      DEFINE_STRING(BGl_string2426z00zz__gunza7ipza7,
		BgL_bgl_string2426za700za7za7_2484za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2427z00zz__gunza7ipza7,
		BgL_bgl_string2427za700za7za7_2485za7, "_open-input-inflate-file", 24);
	      DEFINE_STRING(BGl_string2428z00zz__gunza7ipza7,
		BgL_bgl_string2428za700za7za7_2486za7, "open-input-deflate-file", 23);
	      DEFINE_STRING(BGl_string2429z00zz__gunza7ipza7,
		BgL_bgl_string2429za700za7za7_2487za7, "_port->zlib-port", 16);
	      DEFINE_STRING(BGl_string2430z00zz__gunza7ipza7,
		BgL_bgl_string2430za700za7za7_2488za7, "port->zlib-port", 15);
	      DEFINE_STRING(BGl_string2431z00zz__gunza7ipza7,
		BgL_bgl_string2431za700za7za7_2489za7, "Illegal fcheck", 14);
	      DEFINE_STRING(BGl_string2432z00zz__gunza7ipza7,
		BgL_bgl_string2432za700za7za7_2490za7, "Unsupported format", 18);
	      DEFINE_STRING(BGl_string2433z00zz__gunza7ipza7,
		BgL_bgl_string2433za700za7za7_2491za7, "_open-input-zlib-file", 21);
	      DEFINE_STRING(BGl_string2434z00zz__gunza7ipza7,
		BgL_bgl_string2434za700za7za7_2492za7, "&check-adler32", 14);
	      DEFINE_STRING(BGl_string2438z00zz__gunza7ipza7,
		BgL_bgl_string2438za700za7za7_2493za7, "e", 1);
	      DEFINE_STRING(BGl_string2440z00zz__gunza7ipza7,
		BgL_bgl_string2440za700za7za7_2494za7, "long", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2435z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1978za7622495z00, BGl_z62lambda1978z62zz__gunza7ipza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2436z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1977za7622496z00, BGl_z62lambda1977z62zz__gunza7ipza7,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2444z00zz__gunza7ipza7,
		BgL_bgl_string2444za700za7za7_2497za7, "b", 1);
	      DEFINE_STRING(BGl_string2448z00zz__gunza7ipza7,
		BgL_bgl_string2448za700za7za7_2498za7, "v", 1);
	      DEFINE_STRING(BGl_string2369z00zz__gunza7ipza7,
		BgL_bgl_string2369za700za7za7_2499za7, "return", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_checkzd2adler32zd2envz00zz__gunza7ipza7,
		BgL_bgl_za762checkza7d2adler2500z00,
		BGl_z62checkzd2adler32zb0zz__gunza7ipza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2441z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1983za7622501z00, BGl_z62lambda1983z62zz__gunza7ipza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2442z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1982za7622502z00, BGl_z62lambda1982z62zz__gunza7ipza7,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2450z00zz__gunza7ipza7,
		BgL_bgl_string2450za700za7za7_2503za7, "obj", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2445z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1988za7622504z00, BGl_z62lambda1988z62zz__gunza7ipza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2446z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1987za7622505z00, BGl_z62lambda1987z62zz__gunza7ipza7,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2371z00zz__gunza7ipza7,
		BgL_bgl_string2371za700za7za7_2506za7, "complete", 8);
	      DEFINE_STRING(BGl_string2373z00zz__gunza7ipza7,
		BgL_bgl_string2373za700za7za7_2507za7, "flush", 5);
	      DEFINE_STRING(BGl_string2455z00zz__gunza7ipza7,
		BgL_bgl_string2455za700za7za7_2508za7, "huft", 4);
	      DEFINE_STRING(BGl_string2375z00zz__gunza7ipza7,
		BgL_bgl_string2375za700za7za7_2509za7, "step", 4);
	      DEFINE_STRING(BGl_string2457z00zz__gunza7ipza7,
		BgL_bgl_string2457za700za7za7_2510za7, "__gunzip", 8);
	      DEFINE_STRING(BGl_string2376z00zz__gunza7ipza7,
		BgL_bgl_string2376za700za7za7_2511za7, "inflate", 7);
	      DEFINE_STRING(BGl_string2377z00zz__gunza7ipza7,
		BgL_bgl_string2377za700za7za7_2512za7, "Illegal state", 13);
	      DEFINE_STRING(BGl_string2378z00zz__gunza7ipza7,
		BgL_bgl_string2378za700za7za7_2513za7, "unknown inflate type `~A'", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_openzd2inputzd2gza7ipzd2filezd2envza7zz__gunza7ipza7,
		BgL_bgl__openza7d2inputza7d22514z00, opt_generic_entry,
		BGl__openzd2inputzd2gza7ipzd2filez75zz__gunza7ipza7, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2451z00zz__gunza7ipza7,
		BgL_bgl_za762za7c3za704anonymo2515za7,
		BGl_z62zc3z04anonymousza31973ze3ze5zz__gunza7ipza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2452z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1971za7622516z00, BGl_z62lambda1971z62zz__gunza7ipza7,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inflatezd2sendcharszd2envz00zz__gunza7ipza7,
		BgL_bgl_za762inflateza7d2sen2517z00,
		BGl_z62inflatezd2sendcharszb0zz__gunza7ipza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2453z00zz__gunza7ipza7,
		BgL_bgl_za762lambda1969za7622518z00, BGl_z62lambda1969z62zz__gunza7ipza7,
		0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2387z00zz__gunza7ipza7,
		BgL_bgl_string2387za700za7za7_2519za7, "error in compressed data `~a'", 29);
	      DEFINE_STRING(BGl_string2388z00zz__gunza7ipza7,
		BgL_bgl_string2388za700za7za7_2520za7, "inflate-entry", 13);
	      DEFINE_STRING(BGl_string2389z00zz__gunza7ipza7,
		BgL_bgl_string2389za700za7za7_2521za7, "bad lengths `~a'", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_portzd2ze3za7libzd2portzd2envz96zz__gunza7ipza7,
		BgL_bgl__portza7d2za7e3za7a7li2522za7, opt_generic_entry,
		BGl__portzd2ze3za7libzd2portz44zz__gunza7ipza7, BFALSE, -1);
	      DEFINE_STRING(BGl_string2395z00zz__gunza7ipza7,
		BgL_bgl_string2395za700za7za7_2523za7, "incomplete code set", 19);
	      DEFINE_STRING(BGl_string2396z00zz__gunza7ipza7,
		BgL_bgl_string2396za700za7za7_2524za7, "bad hop `~a'", 12);
	      DEFINE_STRING(BGl_string2397z00zz__gunza7ipza7,
		BgL_bgl_string2397za700za7za7_2525za7, "bad input: more codes than bits",
		31);
	      DEFINE_STRING(BGl_string2398z00zz__gunza7ipza7,
		BgL_bgl_string2398za700za7za7_2526za7, "bad input: mode codes than bits",
		31);
	      DEFINE_STRING(BGl_string2399z00zz__gunza7ipza7,
		BgL_bgl_string2399za700za7za7_2527za7, "incomplete table", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_gunza7ipzd2sendcharszd2envza7zz__gunza7ipza7,
		BgL_bgl_za762gunza7a7ipza7d2se2528za7,
		BGl_z62gunza7ipzd2sendcharsz17zz__gunza7ipza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_portzd2ze3gza7ipzd2portzd2envz96zz__gunza7ipza7,
		BgL_bgl__portza7d2za7e3gza7a7i2529za7, opt_generic_entry,
		BGl__portzd2ze3gza7ipzd2portz44zz__gunza7ipza7, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list2380z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_list2382z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_list2384z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_list2386z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_huftz00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_list2391z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_list2394z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2379z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2381z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2383z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2385z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2390z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2392z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_vector2393z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2414z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2416z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2418z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2420z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2437z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2439z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2443z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2447z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2449z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2368z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2370z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2372z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2454z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2374z00zz__gunza7ipza7));
		     ADD_ROOT((void *) (&BGl_symbol2456z00zz__gunza7ipza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__gunza7ipza7(long
		BgL_checksumz00_4902, char *BgL_fromz00_4903)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__gunza7ipza7))
				{
					BGl_requirezd2initializa7ationz75zz__gunza7ipza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__gunza7ipza7();
					BGl_cnstzd2initzd2zz__gunza7ipza7();
					BGl_importedzd2moduleszd2initz00zz__gunza7ipza7();
					BGl_objectzd2initzd2zz__gunza7ipza7();
					return BGl_toplevelzd2initzd2zz__gunza7ipza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			BGl_symbol2368z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2369z00zz__gunza7ipza7);
			BGl_symbol2370z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2371z00zz__gunza7ipza7);
			BGl_symbol2372z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2373z00zz__gunza7ipza7);
			BGl_symbol2374z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2375z00zz__gunza7ipza7);
			BGl_list2380z00zz__gunza7ipza7 =
				MAKE_YOUNG_PAIR(BINT(3L),
				MAKE_YOUNG_PAIR(BINT(4L),
					MAKE_YOUNG_PAIR(BINT(5L),
						MAKE_YOUNG_PAIR(BINT(6L),
							MAKE_YOUNG_PAIR(BINT(7L),
								MAKE_YOUNG_PAIR(BINT(8L),
									MAKE_YOUNG_PAIR(BINT(9L),
										MAKE_YOUNG_PAIR(BINT(10L),
											MAKE_YOUNG_PAIR(BINT(11L),
												MAKE_YOUNG_PAIR(BINT(13L),
													MAKE_YOUNG_PAIR(BINT(15L),
														MAKE_YOUNG_PAIR(BINT(17L),
															MAKE_YOUNG_PAIR(BINT(19L),
																MAKE_YOUNG_PAIR(BINT(23L),
																	MAKE_YOUNG_PAIR(BINT(27L),
																		MAKE_YOUNG_PAIR(BINT(31L),
																			MAKE_YOUNG_PAIR(BINT(35L),
																				MAKE_YOUNG_PAIR(BINT(43L),
																					MAKE_YOUNG_PAIR(BINT(51L),
																						MAKE_YOUNG_PAIR(BINT(59L),
																							MAKE_YOUNG_PAIR(BINT(67L),
																								MAKE_YOUNG_PAIR(BINT(83L),
																									MAKE_YOUNG_PAIR(BINT(99L),
																										MAKE_YOUNG_PAIR(BINT(115L),
																											MAKE_YOUNG_PAIR(BINT
																												(131L),
																												MAKE_YOUNG_PAIR(BINT
																													(163L),
																													MAKE_YOUNG_PAIR(BINT
																														(195L),
																														MAKE_YOUNG_PAIR(BINT
																															(227L),
																															MAKE_YOUNG_PAIR
																															(BINT(258L),
																																MAKE_YOUNG_PAIR
																																(BINT(0L),
																																	MAKE_YOUNG_PAIR
																																	(BINT(0L),
																																		BNIL)))))))))))))))))))))))))))))));
			BGl_vector2379z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list2380z00zz__gunza7ipza7);
			BGl_list2382z00zz__gunza7ipza7 =
				MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(0L),
					MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(0L),
							MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(0L),
									MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(0L),
											MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
													MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
															MAKE_YOUNG_PAIR(BINT(2L),
																MAKE_YOUNG_PAIR(BINT(2L),
																	MAKE_YOUNG_PAIR(BINT(2L),
																		MAKE_YOUNG_PAIR(BINT(2L),
																			MAKE_YOUNG_PAIR(BINT(3L),
																				MAKE_YOUNG_PAIR(BINT(3L),
																					MAKE_YOUNG_PAIR(BINT(3L),
																						MAKE_YOUNG_PAIR(BINT(3L),
																							MAKE_YOUNG_PAIR(BINT(4L),
																								MAKE_YOUNG_PAIR(BINT(4L),
																									MAKE_YOUNG_PAIR(BINT(4L),
																										MAKE_YOUNG_PAIR(BINT(4L),
																											MAKE_YOUNG_PAIR(BINT(5L),
																												MAKE_YOUNG_PAIR(BINT
																													(5L),
																													MAKE_YOUNG_PAIR(BINT
																														(5L),
																														MAKE_YOUNG_PAIR(BINT
																															(5L),
																															MAKE_YOUNG_PAIR
																															(BINT(0L),
																																MAKE_YOUNG_PAIR
																																(BINT(99L),
																																	MAKE_YOUNG_PAIR
																																	(BINT(99L),
																																		BNIL)))))))))))))))))))))))))))))));
			BGl_vector2381z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list2382z00zz__gunza7ipza7);
			BGl_list2384z00zz__gunza7ipza7 =
				MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(2L),
					MAKE_YOUNG_PAIR(BINT(3L), MAKE_YOUNG_PAIR(BINT(4L),
							MAKE_YOUNG_PAIR(BINT(5L), MAKE_YOUNG_PAIR(BINT(7L),
									MAKE_YOUNG_PAIR(BINT(9L), MAKE_YOUNG_PAIR(BINT(13L),
											MAKE_YOUNG_PAIR(BINT(17L), MAKE_YOUNG_PAIR(BINT(25L),
													MAKE_YOUNG_PAIR(BINT(33L), MAKE_YOUNG_PAIR(BINT(49L),
															MAKE_YOUNG_PAIR(BINT(65L),
																MAKE_YOUNG_PAIR(BINT(97L),
																	MAKE_YOUNG_PAIR(BINT(129L),
																		MAKE_YOUNG_PAIR(BINT(193L),
																			MAKE_YOUNG_PAIR(BINT(257L),
																				MAKE_YOUNG_PAIR(BINT(385L),
																					MAKE_YOUNG_PAIR(BINT(513L),
																						MAKE_YOUNG_PAIR(BINT(769L),
																							MAKE_YOUNG_PAIR(BINT(1025L),
																								MAKE_YOUNG_PAIR(BINT(1537L),
																									MAKE_YOUNG_PAIR(BINT(2049L),
																										MAKE_YOUNG_PAIR(BINT(3073L),
																											MAKE_YOUNG_PAIR(BINT
																												(4097L),
																												MAKE_YOUNG_PAIR(BINT
																													(6145L),
																													MAKE_YOUNG_PAIR(BINT
																														(8193L),
																														MAKE_YOUNG_PAIR(BINT
																															(12289L),
																															MAKE_YOUNG_PAIR
																															(BINT(16385L),
																																MAKE_YOUNG_PAIR
																																(BINT(24577L),
																																	BNIL))))))))))))))))))))))))))))));
			BGl_vector2383z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list2384z00zz__gunza7ipza7);
			BGl_list2386z00zz__gunza7ipza7 =
				MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(0L),
					MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(0L),
							MAKE_YOUNG_PAIR(BINT(1L), MAKE_YOUNG_PAIR(BINT(1L),
									MAKE_YOUNG_PAIR(BINT(2L), MAKE_YOUNG_PAIR(BINT(2L),
											MAKE_YOUNG_PAIR(BINT(3L), MAKE_YOUNG_PAIR(BINT(3L),
													MAKE_YOUNG_PAIR(BINT(4L), MAKE_YOUNG_PAIR(BINT(4L),
															MAKE_YOUNG_PAIR(BINT(5L),
																MAKE_YOUNG_PAIR(BINT(5L),
																	MAKE_YOUNG_PAIR(BINT(6L),
																		MAKE_YOUNG_PAIR(BINT(6L),
																			MAKE_YOUNG_PAIR(BINT(7L),
																				MAKE_YOUNG_PAIR(BINT(7L),
																					MAKE_YOUNG_PAIR(BINT(8L),
																						MAKE_YOUNG_PAIR(BINT(8L),
																							MAKE_YOUNG_PAIR(BINT(9L),
																								MAKE_YOUNG_PAIR(BINT(9L),
																									MAKE_YOUNG_PAIR(BINT(10L),
																										MAKE_YOUNG_PAIR(BINT(10L),
																											MAKE_YOUNG_PAIR(BINT(11L),
																												MAKE_YOUNG_PAIR(BINT
																													(11L),
																													MAKE_YOUNG_PAIR(BINT
																														(12L),
																														MAKE_YOUNG_PAIR(BINT
																															(12L),
																															MAKE_YOUNG_PAIR
																															(BINT(13L),
																																MAKE_YOUNG_PAIR
																																(BINT(13L),
																																	BNIL))))))))))))))))))))))))))))));
			BGl_vector2385z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list2386z00zz__gunza7ipza7);
			BGl_list2391z00zz__gunza7ipza7 =
				MAKE_YOUNG_PAIR(BINT(16L), MAKE_YOUNG_PAIR(BINT(17L),
					MAKE_YOUNG_PAIR(BINT(18L), MAKE_YOUNG_PAIR(BINT(0L),
							MAKE_YOUNG_PAIR(BINT(8L), MAKE_YOUNG_PAIR(BINT(7L),
									MAKE_YOUNG_PAIR(BINT(9L), MAKE_YOUNG_PAIR(BINT(6L),
											MAKE_YOUNG_PAIR(BINT(10L), MAKE_YOUNG_PAIR(BINT(5L),
													MAKE_YOUNG_PAIR(BINT(11L), MAKE_YOUNG_PAIR(BINT(4L),
															MAKE_YOUNG_PAIR(BINT(12L),
																MAKE_YOUNG_PAIR(BINT(3L),
																	MAKE_YOUNG_PAIR(BINT(13L),
																		MAKE_YOUNG_PAIR(BINT(2L),
																			MAKE_YOUNG_PAIR(BINT(14L),
																				MAKE_YOUNG_PAIR(BINT(1L),
																					MAKE_YOUNG_PAIR(BINT(15L),
																						BNIL)))))))))))))))))));
			BGl_vector2390z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list2391z00zz__gunza7ipza7);
			BGl_vector2392z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BNIL);
			BGl_list2394z00zz__gunza7ipza7 =
				MAKE_YOUNG_PAIR(BINT(0L), MAKE_YOUNG_PAIR(BINT(1L),
					MAKE_YOUNG_PAIR(BINT(3L), MAKE_YOUNG_PAIR(BINT(7L),
							MAKE_YOUNG_PAIR(BINT(15L), MAKE_YOUNG_PAIR(BINT(31L),
									MAKE_YOUNG_PAIR(BINT(63L), MAKE_YOUNG_PAIR(BINT(127L),
											MAKE_YOUNG_PAIR(BINT(255L), MAKE_YOUNG_PAIR(BINT(511L),
													MAKE_YOUNG_PAIR(BINT(1023L),
														MAKE_YOUNG_PAIR(BINT(2047L),
															MAKE_YOUNG_PAIR(BINT(4095L),
																MAKE_YOUNG_PAIR(BINT(8191L),
																	MAKE_YOUNG_PAIR(BINT(16383L),
																		MAKE_YOUNG_PAIR(BINT(32767L),
																			MAKE_YOUNG_PAIR(BINT(65535L),
																				BNIL)))))))))))))))));
			BGl_vector2393z00zz__gunza7ipza7 =
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_list2394z00zz__gunza7ipza7);
			BGl_symbol2414z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2415z00zz__gunza7ipza7);
			BGl_symbol2416z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2417z00zz__gunza7ipza7);
			BGl_symbol2418z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2419z00zz__gunza7ipza7);
			BGl_symbol2420z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2421z00zz__gunza7ipza7);
			BGl_symbol2437z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2438z00zz__gunza7ipza7);
			BGl_symbol2439z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2440z00zz__gunza7ipza7);
			BGl_symbol2443z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2444z00zz__gunza7ipza7);
			BGl_symbol2447z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2448z00zz__gunza7ipza7);
			BGl_symbol2449z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2450z00zz__gunza7ipza7);
			BGl_symbol2454z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2455z00zz__gunza7ipza7);
			return (BGl_symbol2456z00zz__gunza7ipza7 =
				bstring_to_symbol(BGl_string2457z00zz__gunza7ipza7), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			return BUNSPEC;
		}

	}



/* build-vector */
	obj_t BGl_buildzd2vectorzd2zz__gunza7ipza7(long BgL_nz00_5, obj_t BgL_pz00_6)
	{
		{	/* Unsafe/gunzip.scm 266 */
			{	/* Unsafe/gunzip.scm 267 */
				obj_t BgL_vz00_1261;

				BgL_vz00_1261 = make_vector(BgL_nz00_5, BUNSPEC);
				{
					long BgL_i1071z00_2886;

					BgL_i1071z00_2886 = 0L;
				BgL_loop1072z00_2885:
					if ((BgL_i1071z00_2886 < BgL_nz00_5))
						{	/* Unsafe/gunzip.scm 268 */
							{	/* Unsafe/gunzip.scm 268 */
								BgL_huftz00_bglt BgL_arg1393z00_2891;

								BgL_arg1393z00_2891 =
									BGl_z62zc3z04anonymousza31640ze3ze5zz__gunza7ipza7(BgL_pz00_6,
									BgL_i1071z00_2886);
								VECTOR_SET(BgL_vz00_1261, BgL_i1071z00_2886,
									((obj_t) BgL_arg1393z00_2891));
							}
							{
								long BgL_i1071z00_5260;

								BgL_i1071z00_5260 = (BgL_i1071z00_2886 + 1L);
								BgL_i1071z00_2886 = BgL_i1071z00_5260;
								goto BgL_loop1072z00_2885;
							}
						}
					else
						{	/* Unsafe/gunzip.scm 268 */
							((bool_t) 0);
						}
				}
				return BgL_vz00_1261;
			}
		}

	}



/* subvector */
	obj_t BGl_subvectorz00zz__gunza7ipza7(obj_t BgL_vz00_7, long BgL_offsetz00_8)
	{
		{	/* Unsafe/gunzip.scm 274 */
			{	/* Unsafe/gunzip.scm 275 */
				long BgL_lenz00_1271;

				BgL_lenz00_1271 = (VECTOR_LENGTH(BgL_vz00_7) - BgL_offsetz00_8);
				{	/* Unsafe/gunzip.scm 275 */
					obj_t BgL_newz00_1272;

					BgL_newz00_1272 = make_vector(BgL_lenz00_1271, BUNSPEC);
					{	/* Unsafe/gunzip.scm 276 */

						{
							long BgL_i1073z00_2910;

							BgL_i1073z00_2910 = 0L;
						BgL_loop1074z00_2909:
							if ((BgL_i1073z00_2910 < BgL_lenz00_1271))
								{	/* Unsafe/gunzip.scm 277 */
									VECTOR_SET(BgL_newz00_1272, BgL_i1073z00_2910,
										VECTOR_REF(BgL_vz00_7,
											(BgL_i1073z00_2910 + BgL_offsetz00_8)));
									{
										long BgL_i1073z00_5270;

										BgL_i1073z00_5270 = (BgL_i1073z00_2910 + 1L);
										BgL_i1073z00_2910 = BgL_i1073z00_5270;
										goto BgL_loop1074z00_2909;
									}
								}
							else
								{	/* Unsafe/gunzip.scm 277 */
									((bool_t) 0);
								}
						}
						return BgL_newz00_1272;
					}
				}
			}
		}

	}



/* inflate-entry */
	obj_t BGl_inflatezd2entryzd2zz__gunza7ipza7(obj_t BgL_inputzd2portzd2_12,
		obj_t BgL_slidez00_13)
	{
		{	/* Unsafe/gunzip.scm 336 */
			{	/* Unsafe/gunzip.scm 336 */
				obj_t BgL_wpz00_4637;
				obj_t BgL_bbz00_4638;
				obj_t BgL_bkz00_4639;

				BgL_wpz00_4637 = MAKE_CELL(BINT(0L));
				BgL_bbz00_4638 = MAKE_CELL(BINT(0L));
				BgL_bkz00_4639 = MAKE_CELL(BINT(0L));
				{	/* Unsafe/gunzip.scm 340 */
					long BgL_wsiza7eza7_1296;

					BgL_wsiza7eza7_1296 = STRING_LENGTH(((obj_t) BgL_slidez00_13));
					{	/* Unsafe/gunzip.scm 350 */
						obj_t BgL_bufferz00_1297;

						{	/* Ieee/string.scm 172 */

							BgL_bufferz00_1297 = make_string(256L, ((unsigned char) ' '));
						}
						{	/* Unsafe/gunzip.scm 888 */
							obj_t BgL_auxz00_4640;

							BgL_auxz00_4640 = BINT(0L);
							CELL_SET(BgL_wpz00_4637, BgL_auxz00_4640);
						}
						{	/* Unsafe/gunzip.scm 889 */
							obj_t BgL_auxz00_4641;

							BgL_auxz00_4641 = BINT(0L);
							CELL_SET(BgL_bkz00_4639, BgL_auxz00_4641);
						}
						{	/* Unsafe/gunzip.scm 890 */
							obj_t BgL_auxz00_4642;

							BgL_auxz00_4642 = BINT(0L);
							CELL_SET(BgL_bbz00_4638, BgL_auxz00_4642);
						}
						return
							BGl_z62loopz62zz__gunza7ipza7(BgL_inputzd2portzd2_12,
							BgL_bkz00_4639, BgL_bbz00_4638, BgL_slidez00_13, BgL_wpz00_4637,
							BgL_wsiza7eza7_1296, BINT(0L));
					}
				}
			}
		}

	}



/* &laap */
	obj_t BGl_z62laapz62zz__gunza7ipza7(long BgL_huftsz00_4440,
		obj_t BgL_hz00_4439, obj_t BgL_wpz00_4438, obj_t BgL_inputzd2portzd2_4437,
		obj_t BgL_bkz00_4436, obj_t BgL_bbz00_4435, obj_t BgL_slidez00_4434,
		long BgL_wsiza7eza7_4433, obj_t BgL_statez00_1317, obj_t BgL_ez00_1318,
		obj_t BgL_rz00_1319)
	{
		{	/* Unsafe/gunzip.scm 897 */
			if ((BgL_statez00_1317 == BGl_symbol2368z00zz__gunza7ipza7))
				{	/* Unsafe/gunzip.scm 902 */
					bool_t BgL_test2534z00_5285;

					if (CBOOL(BgL_rz00_1319))
						{	/* Unsafe/gunzip.scm 902 */
							BgL_test2534z00_5285 = ((long) CINT(BgL_ez00_1318) == 0L);
						}
					else
						{	/* Unsafe/gunzip.scm 902 */
							BgL_test2534z00_5285 = ((bool_t) 0);
						}
					if (BgL_test2534z00_5285)
						{	/* Unsafe/gunzip.scm 903 */
							obj_t BgL_arg1412z00_1324;

							if ((BgL_huftsz00_4440 > (long) CINT(BgL_hz00_4439)))
								{	/* Unsafe/gunzip.scm 903 */
									BgL_arg1412z00_1324 = BINT(BgL_huftsz00_4440);
								}
							else
								{	/* Unsafe/gunzip.scm 903 */
									BgL_arg1412z00_1324 = BgL_hz00_4439;
								}
							return
								BGl_z62loopz62zz__gunza7ipza7(BgL_inputzd2portzd2_4437,
								BgL_bkz00_4436, BgL_bbz00_4435, BgL_slidez00_4434,
								BgL_wpz00_4438, BgL_wsiza7eza7_4433, BgL_arg1412z00_1324);
						}
					else
						{	/* Unsafe/gunzip.scm 907 */
							obj_t BgL_val0_1247z00_1326;
							obj_t BgL_val1_1248z00_1327;

							BgL_val0_1247z00_1326 = BGl_symbol2370z00zz__gunza7ipza7;
							BgL_val1_1248z00_1327 = CELL_REF(BgL_wpz00_4438);
							{	/* Unsafe/gunzip.scm 907 */
								int BgL_tmpz00_5295;

								BgL_tmpz00_5295 = (int) (3L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5295);
							}
							{	/* Unsafe/gunzip.scm 907 */
								int BgL_tmpz00_5298;

								BgL_tmpz00_5298 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5298, BgL_val1_1248z00_1327);
							}
							{	/* Unsafe/gunzip.scm 907 */
								int BgL_tmpz00_5301;

								BgL_tmpz00_5301 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5301, BUNSPEC);
							}
							return BgL_val0_1247z00_1326;
						}
				}
			else
				{	/* Unsafe/gunzip.scm 900 */
					if ((BgL_statez00_1317 == BGl_symbol2372z00zz__gunza7ipza7))
						{	/* Unsafe/gunzip.scm 909 */
							obj_t BgL_val0_1250z00_1330;

							BgL_val0_1250z00_1330 = BGl_symbol2374z00zz__gunza7ipza7;
							{	/* Unsafe/gunzip.scm 912 */
								obj_t BgL_zc3z04anonymousza31415ze3z87_4431;

								BgL_zc3z04anonymousza31415ze3z87_4431 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31415ze3ze5zz__gunza7ipza7,
									(int) (0L), (int) (9L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (0L),
									BINT(BgL_wsiza7eza7_4433));
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (1L),
									BgL_slidez00_4434);
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (2L),
									((obj_t) BgL_bbz00_4435));
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (3L),
									((obj_t) BgL_bkz00_4436));
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (4L),
									BgL_inputzd2portzd2_4437);
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (5L),
									((obj_t) BgL_wpz00_4438));
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (6L),
									BgL_hz00_4439);
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (7L),
									BINT(BgL_huftsz00_4440));
								PROCEDURE_SET(BgL_zc3z04anonymousza31415ze3z87_4431, (int) (8L),
									BgL_rz00_1319);
								{	/* Unsafe/gunzip.scm 909 */
									int BgL_tmpz00_5332;

									BgL_tmpz00_5332 = (int) (3L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5332);
								}
								{	/* Unsafe/gunzip.scm 909 */
									int BgL_tmpz00_5335;

									BgL_tmpz00_5335 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_5335, BgL_ez00_1318);
								}
								{	/* Unsafe/gunzip.scm 909 */
									int BgL_tmpz00_5338;

									BgL_tmpz00_5338 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_5338,
										BgL_zc3z04anonymousza31415ze3z87_4431);
								}
								return BgL_val0_1250z00_1330;
							}
						}
					else
						{	/* Unsafe/gunzip.scm 328 */
							BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3768;

							{	/* Unsafe/gunzip.scm 328 */
								BgL_z62iozd2parsezd2errorz62_bglt BgL_new1076z00_3769;

								{	/* Unsafe/gunzip.scm 328 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_new1075z00_3770;

									BgL_new1075z00_3770 =
										((BgL_z62iozd2parsezd2errorz62_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_z62iozd2parsezd2errorz62_bgl))));
									{	/* Unsafe/gunzip.scm 328 */
										long BgL_arg1407z00_3771;

										BgL_arg1407z00_3771 =
											BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1075z00_3770),
											BgL_arg1407z00_3771);
									}
									BgL_new1076z00_3769 = BgL_new1075z00_3770;
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1076z00_3769)))->
										BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_z62exceptionz62_bglt)
											COBJECT(((BgL_z62exceptionz62_bglt)
													BgL_new1076z00_3769)))->BgL_locationz00) =
									((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_5349;

									{	/* Unsafe/gunzip.scm 328 */
										obj_t BgL_arg1405z00_3775;

										{	/* Unsafe/gunzip.scm 328 */
											obj_t BgL_arg1406z00_3776;

											{	/* Unsafe/gunzip.scm 328 */
												obj_t BgL_classz00_3777;

												BgL_classz00_3777 =
													BGl_z62iozd2parsezd2errorz62zz__objectz00;
												BgL_arg1406z00_3776 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_3777);
											}
											BgL_arg1405z00_3775 = VECTOR_REF(BgL_arg1406z00_3776, 2L);
										}
										BgL_auxz00_5349 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1405z00_3775);
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1076z00_3769)))->
											BgL_stackz00) = ((obj_t) BgL_auxz00_5349), BUNSPEC);
								}
								((((BgL_z62errorz62_bglt) COBJECT(
												((BgL_z62errorz62_bglt) BgL_new1076z00_3769)))->
										BgL_procz00) =
									((obj_t) BGl_string2376z00zz__gunza7ipza7), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1076z00_3769)))->BgL_msgz00) =
									((obj_t) BGl_string2377z00zz__gunza7ipza7), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1076z00_3769)))->BgL_objz00) =
									((obj_t) BgL_statez00_1317), BUNSPEC);
								BgL_arg1404z00_3768 = BgL_new1076z00_3769;
							}
							return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1404z00_3768));
						}
				}
		}

	}



/* &loop */
	obj_t BGl_z62loopz62zz__gunza7ipza7(obj_t BgL_inputzd2portzd2_4446,
		obj_t BgL_bkz00_4445, obj_t BgL_bbz00_4444, obj_t BgL_slidez00_4443,
		obj_t BgL_wpz00_4442, long BgL_wsiza7eza7_4441, obj_t BgL_hz00_1310)
	{
		{	/* Unsafe/gunzip.scm 893 */
			{
				obj_t BgL_tlz00_1553;
				obj_t BgL_tdz00_1554;
				obj_t BgL_blz00_1555;
				obj_t BgL_bdz00_1556;

				{	/* Unsafe/gunzip.scm 895 */
					obj_t BgL_statez00_1313;

					{	/* Unsafe/gunzip.scm 860 */
						long BgL_ez00_1341;

						{	/* Unsafe/gunzip.scm 860 */
							obj_t BgL_arg1424z00_1371;

							BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4444, BgL_bkz00_4445,
								BgL_inputzd2portzd2_4446, BINT(1L));
							{	/* Unsafe/gunzip.scm 375 */
								obj_t BgL_rz00_3723;

								BgL_rz00_3723 = CELL_REF(BgL_bbz00_4444);
								{	/* Unsafe/gunzip.scm 370 */
									obj_t BgL_auxz00_4447;

									{	/* Unsafe/gunzip.scm 370 */
										long BgL_xz00_3724;

										BgL_xz00_3724 = (long) CINT(CELL_REF(BgL_bbz00_4444));
										BgL_auxz00_4447 = BINT((BgL_xz00_3724 >> (int) (1L)));
									}
									CELL_SET(BgL_bbz00_4444, BgL_auxz00_4447);
								}
								{	/* Unsafe/gunzip.scm 371 */
									obj_t BgL_auxz00_4448;

									BgL_auxz00_4448 = SUBFX(CELL_REF(BgL_bkz00_4445), BINT(1L));
									CELL_SET(BgL_bkz00_4445, BgL_auxz00_4448);
								}
								BgL_arg1424z00_1371 = BgL_rz00_3723;
							}
							BgL_ez00_1341 = ((long) CINT(BgL_arg1424z00_1371) & 1L);
						}
						{	/* Unsafe/gunzip.scm 862 */
							long BgL_tz00_1342;

							{	/* Unsafe/gunzip.scm 862 */
								obj_t BgL_arg1423z00_1370;

								BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4444,
									BgL_bkz00_4445, BgL_inputzd2portzd2_4446, BINT(2L));
								{	/* Unsafe/gunzip.scm 375 */
									obj_t BgL_rz00_3727;

									BgL_rz00_3727 = CELL_REF(BgL_bbz00_4444);
									{	/* Unsafe/gunzip.scm 370 */
										obj_t BgL_auxz00_4449;

										{	/* Unsafe/gunzip.scm 370 */
											long BgL_xz00_3728;

											BgL_xz00_3728 = (long) CINT(CELL_REF(BgL_bbz00_4444));
											BgL_auxz00_4449 = BINT((BgL_xz00_3728 >> (int) (2L)));
										}
										CELL_SET(BgL_bbz00_4444, BgL_auxz00_4449);
									}
									{	/* Unsafe/gunzip.scm 371 */
										obj_t BgL_auxz00_4450;

										BgL_auxz00_4450 = SUBFX(CELL_REF(BgL_bkz00_4445), BINT(2L));
										CELL_SET(BgL_bkz00_4445, BgL_auxz00_4450);
									}
									BgL_arg1423z00_1370 = BgL_rz00_3727;
								}
								BgL_tz00_1342 = ((long) CINT(BgL_arg1423z00_1370) & 3L);
							}
							{	/* Unsafe/gunzip.scm 863 */
								obj_t BgL_statez00_1343;

								{

									switch (BgL_tz00_1342)
										{
										case 2L:

											{	/* Unsafe/gunzip.scm 766 */
												struct bgl_cell BgL_box2538_4774z00;
												obj_t BgL_iz00_4774;
												obj_t BgL_lz00_1375;

												BgL_iz00_4774 =
													MAKE_CELL_STACK(BINT(0L), BgL_box2538_4774z00);
												BgL_lz00_1375 = BINT(0L);
												{	/* Unsafe/gunzip.scm 766 */
													long BgL_nlz00_1376;

													{	/* Unsafe/gunzip.scm 766 */
														long BgL_arg1480z00_1469;

														{	/* Unsafe/gunzip.scm 766 */
															obj_t BgL_arg1481z00_1470;

															BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4444,
																BgL_bkz00_4445, BgL_inputzd2portzd2_4446,
																BINT(5L));
															{	/* Unsafe/gunzip.scm 375 */
																obj_t BgL_rz00_3583;

																BgL_rz00_3583 = CELL_REF(BgL_bbz00_4444);
																{	/* Unsafe/gunzip.scm 370 */
																	obj_t BgL_auxz00_4457;

																	{	/* Unsafe/gunzip.scm 370 */
																		long BgL_xz00_3584;

																		BgL_xz00_3584 =
																			(long) CINT(CELL_REF(BgL_bbz00_4444));
																		BgL_auxz00_4457 =
																			BINT((BgL_xz00_3584 >> (int) (5L)));
																	}
																	CELL_SET(BgL_bbz00_4444, BgL_auxz00_4457);
																}
																{	/* Unsafe/gunzip.scm 371 */
																	obj_t BgL_auxz00_4458;

																	BgL_auxz00_4458 =
																		SUBFX(CELL_REF(BgL_bkz00_4445), BINT(5L));
																	CELL_SET(BgL_bkz00_4445, BgL_auxz00_4458);
																}
																BgL_arg1481z00_1470 = BgL_rz00_3583;
															}
															BgL_arg1480z00_1469 =
																((long) CINT(BgL_arg1481z00_1470) & 31L);
														}
														BgL_nlz00_1376 = (257L + BgL_arg1480z00_1469);
													}
													{	/* Unsafe/gunzip.scm 767 */
														long BgL_ndz00_1377;

														{	/* Unsafe/gunzip.scm 767 */
															long BgL_arg1478z00_1467;

															{	/* Unsafe/gunzip.scm 767 */
																obj_t BgL_arg1479z00_1468;

																BGl_z62NEEDBITSz62zz__gunza7ipza7
																	(BgL_bbz00_4444, BgL_bkz00_4445,
																	BgL_inputzd2portzd2_4446, BINT(5L));
																{	/* Unsafe/gunzip.scm 375 */
																	obj_t BgL_rz00_3588;

																	BgL_rz00_3588 = CELL_REF(BgL_bbz00_4444);
																	{	/* Unsafe/gunzip.scm 370 */
																		obj_t BgL_auxz00_4459;

																		{	/* Unsafe/gunzip.scm 370 */
																			long BgL_xz00_3589;

																			BgL_xz00_3589 =
																				(long) CINT(CELL_REF(BgL_bbz00_4444));
																			BgL_auxz00_4459 =
																				BINT((BgL_xz00_3589 >> (int) (5L)));
																		}
																		CELL_SET(BgL_bbz00_4444, BgL_auxz00_4459);
																	}
																	{	/* Unsafe/gunzip.scm 371 */
																		obj_t BgL_auxz00_4460;

																		BgL_auxz00_4460 =
																			SUBFX(CELL_REF(BgL_bkz00_4445), BINT(5L));
																		CELL_SET(BgL_bkz00_4445, BgL_auxz00_4460);
																	}
																	BgL_arg1479z00_1468 = BgL_rz00_3588;
																}
																BgL_arg1478z00_1467 =
																	((long) CINT(BgL_arg1479z00_1468) & 31L);
															}
															BgL_ndz00_1377 = (1L + BgL_arg1478z00_1467);
														}
														{	/* Unsafe/gunzip.scm 768 */
															long BgL_nbz00_1378;

															{	/* Unsafe/gunzip.scm 768 */
																long BgL_arg1476z00_1465;

																{	/* Unsafe/gunzip.scm 768 */
																	obj_t BgL_arg1477z00_1466;

																	BGl_z62NEEDBITSz62zz__gunza7ipza7
																		(BgL_bbz00_4444, BgL_bkz00_4445,
																		BgL_inputzd2portzd2_4446, BINT(4L));
																	{	/* Unsafe/gunzip.scm 375 */
																		obj_t BgL_rz00_3593;

																		BgL_rz00_3593 = CELL_REF(BgL_bbz00_4444);
																		{	/* Unsafe/gunzip.scm 370 */
																			obj_t BgL_auxz00_4461;

																			{	/* Unsafe/gunzip.scm 370 */
																				long BgL_xz00_3594;

																				BgL_xz00_3594 =
																					(long) CINT(CELL_REF(BgL_bbz00_4444));
																				BgL_auxz00_4461 =
																					BINT((BgL_xz00_3594 >> (int) (4L)));
																			}
																			CELL_SET(BgL_bbz00_4444, BgL_auxz00_4461);
																		}
																		{	/* Unsafe/gunzip.scm 371 */
																			obj_t BgL_auxz00_4462;

																			BgL_auxz00_4462 =
																				SUBFX(CELL_REF(BgL_bkz00_4445),
																				BINT(4L));
																			CELL_SET(BgL_bkz00_4445, BgL_auxz00_4462);
																		}
																		BgL_arg1477z00_1466 = BgL_rz00_3593;
																	}
																	BgL_arg1476z00_1465 =
																		((long) CINT(BgL_arg1477z00_1466) & 15L);
																}
																BgL_nbz00_1378 = (4L + BgL_arg1476z00_1465);
															}
															{	/* Unsafe/gunzip.scm 769 */
																obj_t BgL_llz00_1379;

																BgL_llz00_1379 = make_vector(316L, BUNSPEC);
																if ((BgL_nlz00_1376 > 286L))
																	{	/* Unsafe/gunzip.scm 775 */
																		obj_t BgL_arg1427z00_1381;

																		{	/* Unsafe/gunzip.scm 775 */
																			obj_t BgL_list1428z00_1382;

																			BgL_list1428z00_1382 =
																				MAKE_YOUNG_PAIR(BINT(BgL_nlz00_1376),
																				BNIL);
																			BgL_arg1427z00_1381 =
																				BGl_formatz00zz__r4_output_6_10_3z00
																				(BGl_string2389z00zz__gunza7ipza7,
																				BgL_list1428z00_1382);
																		}
																		{	/* Unsafe/gunzip.scm 328 */
																			BgL_z62iozd2parsezd2errorz62_bglt
																				BgL_arg1404z00_3600;
																			{	/* Unsafe/gunzip.scm 328 */
																				BgL_z62iozd2parsezd2errorz62_bglt
																					BgL_new1076z00_3601;
																				{	/* Unsafe/gunzip.scm 328 */
																					BgL_z62iozd2parsezd2errorz62_bglt
																						BgL_new1075z00_3602;
																					BgL_new1075z00_3602 =
																						((BgL_z62iozd2parsezd2errorz62_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_z62iozd2parsezd2errorz62_bgl))));
																					{	/* Unsafe/gunzip.scm 328 */
																						long BgL_arg1407z00_3603;

																						BgL_arg1407z00_3603 =
																							BGL_CLASS_NUM
																							(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1075z00_3602),
																							BgL_arg1407z00_3603);
																					}
																					BgL_new1076z00_3601 =
																						BgL_new1075z00_3602;
																				}
																				((((BgL_z62exceptionz62_bglt) COBJECT(
																								((BgL_z62exceptionz62_bglt)
																									BgL_new1076z00_3601)))->
																						BgL_fnamez00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_z62exceptionz62_bglt)
																							COBJECT((
																									(BgL_z62exceptionz62_bglt)
																									BgL_new1076z00_3601)))->
																						BgL_locationz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				{
																					obj_t BgL_auxz00_5432;

																					{	/* Unsafe/gunzip.scm 328 */
																						obj_t BgL_arg1405z00_3607;

																						{	/* Unsafe/gunzip.scm 328 */
																							obj_t BgL_arg1406z00_3608;

																							{	/* Unsafe/gunzip.scm 328 */
																								obj_t BgL_classz00_3609;

																								BgL_classz00_3609 =
																									BGl_z62iozd2parsezd2errorz62zz__objectz00;
																								BgL_arg1406z00_3608 =
																									BGL_CLASS_ALL_FIELDS
																									(BgL_classz00_3609);
																							}
																							BgL_arg1405z00_3607 =
																								VECTOR_REF(BgL_arg1406z00_3608,
																								2L);
																						}
																						BgL_auxz00_5432 =
																							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																							(BgL_arg1405z00_3607);
																					}
																					((((BgL_z62exceptionz62_bglt) COBJECT(
																									((BgL_z62exceptionz62_bglt)
																										BgL_new1076z00_3601)))->
																							BgL_stackz00) =
																						((obj_t) BgL_auxz00_5432), BUNSPEC);
																				}
																				((((BgL_z62errorz62_bglt) COBJECT(
																								((BgL_z62errorz62_bglt)
																									BgL_new1076z00_3601)))->
																						BgL_procz00) =
																					((obj_t)
																						BGl_string2376z00zz__gunza7ipza7),
																					BUNSPEC);
																				((((BgL_z62errorz62_bglt)
																							COBJECT(((BgL_z62errorz62_bglt)
																									BgL_new1076z00_3601)))->
																						BgL_msgz00) =
																					((obj_t) BgL_arg1427z00_1381),
																					BUNSPEC);
																				((((BgL_z62errorz62_bglt)
																							COBJECT(((BgL_z62errorz62_bglt)
																									BgL_new1076z00_3601)))->
																						BgL_objz00) =
																					((obj_t) BgL_inputzd2portzd2_4446),
																					BUNSPEC);
																				BgL_arg1404z00_3600 =
																					BgL_new1076z00_3601;
																			}
																			BgL_statez00_1343 =
																				BGl_raisez00zz__errorz00(
																				((obj_t) BgL_arg1404z00_3600));
																	}}
																else
																	{	/* Unsafe/gunzip.scm 774 */
																		if ((BgL_ndz00_1377 > 30L))
																			{	/* Unsafe/gunzip.scm 777 */
																				obj_t BgL_arg1430z00_1384;

																				{	/* Unsafe/gunzip.scm 777 */
																					obj_t BgL_list1431z00_1385;

																					BgL_list1431z00_1385 =
																						MAKE_YOUNG_PAIR(BINT
																						(BgL_ndz00_1377), BNIL);
																					BgL_arg1430z00_1384 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string2389z00zz__gunza7ipza7,
																						BgL_list1431z00_1385);
																				}
																				{	/* Unsafe/gunzip.scm 328 */
																					BgL_z62iozd2parsezd2errorz62_bglt
																						BgL_arg1404z00_3612;
																					{	/* Unsafe/gunzip.scm 328 */
																						BgL_z62iozd2parsezd2errorz62_bglt
																							BgL_new1076z00_3613;
																						{	/* Unsafe/gunzip.scm 328 */
																							BgL_z62iozd2parsezd2errorz62_bglt
																								BgL_new1075z00_3614;
																							BgL_new1075z00_3614 =
																								(
																								(BgL_z62iozd2parsezd2errorz62_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_z62iozd2parsezd2errorz62_bgl))));
																							{	/* Unsafe/gunzip.scm 328 */
																								long BgL_arg1407z00_3615;

																								BgL_arg1407z00_3615 =
																									BGL_CLASS_NUM
																									(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1075z00_3614),
																									BgL_arg1407z00_3615);
																							}
																							BgL_new1076z00_3613 =
																								BgL_new1075z00_3614;
																						}
																						((((BgL_z62exceptionz62_bglt)
																									COBJECT((
																											(BgL_z62exceptionz62_bglt)
																											BgL_new1076z00_3613)))->
																								BgL_fnamez00) =
																							((obj_t) BFALSE), BUNSPEC);
																						((((BgL_z62exceptionz62_bglt)
																									COBJECT((
																											(BgL_z62exceptionz62_bglt)
																											BgL_new1076z00_3613)))->
																								BgL_locationz00) =
																							((obj_t) BFALSE), BUNSPEC);
																						{
																							obj_t BgL_auxz00_5459;

																							{	/* Unsafe/gunzip.scm 328 */
																								obj_t BgL_arg1405z00_3619;

																								{	/* Unsafe/gunzip.scm 328 */
																									obj_t BgL_arg1406z00_3620;

																									{	/* Unsafe/gunzip.scm 328 */
																										obj_t BgL_classz00_3621;

																										BgL_classz00_3621 =
																											BGl_z62iozd2parsezd2errorz62zz__objectz00;
																										BgL_arg1406z00_3620 =
																											BGL_CLASS_ALL_FIELDS
																											(BgL_classz00_3621);
																									}
																									BgL_arg1405z00_3619 =
																										VECTOR_REF
																										(BgL_arg1406z00_3620, 2L);
																								}
																								BgL_auxz00_5459 =
																									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																									(BgL_arg1405z00_3619);
																							}
																							((((BgL_z62exceptionz62_bglt)
																										COBJECT((
																												(BgL_z62exceptionz62_bglt)
																												BgL_new1076z00_3613)))->
																									BgL_stackz00) =
																								((obj_t) BgL_auxz00_5459),
																								BUNSPEC);
																						}
																						((((BgL_z62errorz62_bglt) COBJECT(
																										((BgL_z62errorz62_bglt)
																											BgL_new1076z00_3613)))->
																								BgL_procz00) =
																							((obj_t)
																								BGl_string2376z00zz__gunza7ipza7),
																							BUNSPEC);
																						((((BgL_z62errorz62_bglt)
																									COBJECT((
																											(BgL_z62errorz62_bglt)
																											BgL_new1076z00_3613)))->
																								BgL_msgz00) =
																							((obj_t) BgL_arg1430z00_1384),
																							BUNSPEC);
																						((((BgL_z62errorz62_bglt)
																									COBJECT((
																											(BgL_z62errorz62_bglt)
																											BgL_new1076z00_3613)))->
																								BgL_objz00) =
																							((obj_t)
																								BgL_inputzd2portzd2_4446),
																							BUNSPEC);
																						BgL_arg1404z00_3612 =
																							BgL_new1076z00_3613;
																					}
																					BgL_statez00_1343 =
																						BGl_raisez00zz__errorz00(
																						((obj_t) BgL_arg1404z00_3612));
																			}}
																		else
																			{	/* Unsafe/gunzip.scm 776 */
																				{
																					long BgL_i1145z00_1387;

																					BgL_i1145z00_1387 = 0L;
																				BgL_zc3z04anonymousza31432ze3z87_1388:
																					if (
																						(BgL_i1145z00_1387 <
																							BgL_nbz00_1378))
																						{	/* Unsafe/gunzip.scm 780 */
																							{	/* Unsafe/gunzip.scm 783 */
																								long BgL_arg1434z00_1391;
																								long BgL_arg1435z00_1392;

																								{	/* Unsafe/gunzip.scm 783 */
																									obj_t BgL_arg1436z00_1393;

																									BgL_arg1436z00_1393 =
																										BGl_vector2390z00zz__gunza7ipza7;
																									BgL_arg1434z00_1391 =
																										(long)
																										CINT(VECTOR_REF
																										(BgL_arg1436z00_1393,
																											BgL_i1145z00_1387));
																								}
																								{	/* Unsafe/gunzip.scm 783 */
																									obj_t BgL_arg1437z00_1394;

																									BGl_z62NEEDBITSz62zz__gunza7ipza7
																										(BgL_bbz00_4444,
																										BgL_bkz00_4445,
																										BgL_inputzd2portzd2_4446,
																										BINT(3L));
																									{	/* Unsafe/gunzip.scm 375 */
																										obj_t BgL_rz00_3627;

																										BgL_rz00_3627 =
																											CELL_REF(BgL_bbz00_4444);
																										{	/* Unsafe/gunzip.scm 370 */
																											obj_t BgL_auxz00_4463;

																											{	/* Unsafe/gunzip.scm 370 */
																												long BgL_xz00_3628;

																												BgL_xz00_3628 =
																													(long)
																													CINT(CELL_REF
																													(BgL_bbz00_4444));
																												BgL_auxz00_4463 =
																													BINT((BgL_xz00_3628 >>
																														(int) (3L)));
																											}
																											CELL_SET(BgL_bbz00_4444,
																												BgL_auxz00_4463);
																										}
																										{	/* Unsafe/gunzip.scm 371 */
																											obj_t BgL_auxz00_4464;

																											BgL_auxz00_4464 =
																												SUBFX(CELL_REF
																												(BgL_bkz00_4445),
																												BINT(3L));
																											CELL_SET(BgL_bkz00_4445,
																												BgL_auxz00_4464);
																										}
																										BgL_arg1437z00_1394 =
																											BgL_rz00_3627;
																									}
																									BgL_arg1435z00_1392 =
																										(
																										(long)
																										CINT(BgL_arg1437z00_1394) &
																										7L);
																								}
																								VECTOR_SET(BgL_llz00_1379,
																									BgL_arg1434z00_1391,
																									BINT(BgL_arg1435z00_1392));
																							}
																							{
																								long BgL_i1145z00_5489;

																								BgL_i1145z00_5489 =
																									(BgL_i1145z00_1387 + 1L);
																								BgL_i1145z00_1387 =
																									BgL_i1145z00_5489;
																								goto
																									BgL_zc3z04anonymousza31432ze3z87_1388;
																							}
																						}
																					else
																						{	/* Unsafe/gunzip.scm 780 */
																							((bool_t) 0);
																						}
																				}
																				{
																					long BgL_i1147z00_1398;

																					BgL_i1147z00_1398 = BgL_nbz00_1378;
																				BgL_zc3z04anonymousza31439ze3z87_1399:
																					if ((BgL_i1147z00_1398 < 19L))
																						{	/* Unsafe/gunzip.scm 784 */
																							{	/* Unsafe/gunzip.scm 787 */
																								long BgL_arg1441z00_1402;

																								{	/* Unsafe/gunzip.scm 787 */
																									obj_t BgL_arg1442z00_1403;

																									BgL_arg1442z00_1403 =
																										BGl_vector2390z00zz__gunza7ipza7;
																									BgL_arg1441z00_1402 =
																										(long)
																										CINT(VECTOR_REF
																										(BgL_arg1442z00_1403,
																											BgL_i1147z00_1398));
																								}
																								VECTOR_SET(BgL_llz00_1379,
																									BgL_arg1441z00_1402,
																									BINT(0L));
																							}
																							{
																								long BgL_i1147z00_5497;

																								BgL_i1147z00_5497 =
																									(BgL_i1147z00_1398 + 1L);
																								BgL_i1147z00_1398 =
																									BgL_i1147z00_5497;
																								goto
																									BgL_zc3z04anonymousza31439ze3z87_1399;
																							}
																						}
																					else
																						{	/* Unsafe/gunzip.scm 784 */
																							((bool_t) 0);
																						}
																				}
																				{	/* Unsafe/gunzip.scm 790 */
																					obj_t BgL_tlz00_1406;

																					BgL_tlz00_1406 =
																						BGl_huftzd2buildze70z35zz__gunza7ipza7
																						(BgL_inputzd2portzd2_4446,
																						BgL_llz00_1379, 19L, 19L,
																						BGl_vector2392z00zz__gunza7ipza7,
																						BGl_vector2392z00zz__gunza7ipza7,
																						7L, ((bool_t) 0));
																					{	/* Unsafe/gunzip.scm 791 */
																						obj_t BgL_blz00_1407;
																						obj_t BgL_okzf3zf3_1408;

																						{	/* Unsafe/gunzip.scm 792 */
																							obj_t BgL_tmpz00_3640;

																							{	/* Unsafe/gunzip.scm 792 */
																								int BgL_tmpz00_5500;

																								BgL_tmpz00_5500 = (int) (1L);
																								BgL_tmpz00_3640 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_5500);
																							}
																							{	/* Unsafe/gunzip.scm 792 */
																								int BgL_tmpz00_5503;

																								BgL_tmpz00_5503 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_5503, BUNSPEC);
																							}
																							BgL_blz00_1407 = BgL_tmpz00_3640;
																						}
																						{	/* Unsafe/gunzip.scm 792 */
																							obj_t BgL_tmpz00_3641;

																							{	/* Unsafe/gunzip.scm 792 */
																								int BgL_tmpz00_5506;

																								BgL_tmpz00_5506 = (int) (2L);
																								BgL_tmpz00_3641 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_5506);
																							}
																							{	/* Unsafe/gunzip.scm 792 */
																								int BgL_tmpz00_5509;

																								BgL_tmpz00_5509 = (int) (2L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_5509, BUNSPEC);
																							}
																							BgL_okzf3zf3_1408 =
																								BgL_tmpz00_3641;
																						}
																						if (CBOOL(BgL_okzf3zf3_1408))
																							{	/* Unsafe/gunzip.scm 795 */
																								long BgL_nz00_1410;
																								long BgL_mz00_1411;

																								BgL_nz00_1410 =
																									(BgL_nlz00_1376 +
																									BgL_ndz00_1377);
																								{	/* Unsafe/gunzip.scm 796 */
																									obj_t BgL_arg1473z00_1463;

																									BgL_arg1473z00_1463 =
																										BGl_vector2393z00zz__gunza7ipza7;
																									{	/* Unsafe/gunzip.scm 796 */
																										long BgL_kz00_3645;

																										BgL_kz00_3645 =
																											(long)
																											CINT(BgL_blz00_1407);
																										BgL_mz00_1411 =
																											(long)
																											CINT(VECTOR_REF
																											(BgL_arg1473z00_1463,
																												BgL_kz00_3645));
																								}}
																								{	/* Unsafe/gunzip.scm 797 */
																									obj_t BgL_auxz00_4775;

																									BgL_auxz00_4775 = BINT(0L);
																									CELL_SET(BgL_iz00_4774,
																										BgL_auxz00_4775);
																								}
																								BgL_lz00_1375 = BINT(0L);
																								{

																								BgL_zc3z04anonymousza31444ze3z87_1413:
																									if (
																										((long) CINT(
																												((obj_t)
																													((obj_t)
																														CELL_REF
																														(BgL_iz00_4774)))) <
																											BgL_nz00_1410))
																										{	/* Unsafe/gunzip.scm 800 */
																											BGl_z62NEEDBITSz62zz__gunza7ipza7
																												(BgL_bbz00_4444,
																												BgL_bkz00_4445,
																												BgL_inputzd2portzd2_4446,
																												BgL_blz00_1407);
																											{	/* Unsafe/gunzip.scm 802 */
																												long BgL_posz00_1415;

																												{	/* Unsafe/gunzip.scm 802 */
																													long BgL_xz00_3648;

																													BgL_xz00_3648 =
																														(long)
																														CINT(CELL_REF
																														(BgL_bbz00_4444));
																													BgL_posz00_1415 =
																														(BgL_xz00_3648 &
																														BgL_mz00_1411);
																												}
																												{	/* Unsafe/gunzip.scm 802 */
																													obj_t BgL_tdz00_1416;

																													BgL_tdz00_1416 =
																														VECTOR_REF(
																														((obj_t)
																															BgL_tlz00_1406),
																														BgL_posz00_1415);
																													{	/* Unsafe/gunzip.scm 803 */
																														long
																															BgL_dmpz00_1417;
																														BgL_dmpz00_1417 =
																															(((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt) BgL_tdz00_1416)))->BgL_bz00);
																														{	/* Unsafe/gunzip.scm 804 */
																															obj_t
																																BgL_jz00_1418;
																															BgL_jz00_1418 =
																																(((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt) BgL_tdz00_1416)))->BgL_vz00);
																															{	/* Unsafe/gunzip.scm 806 */

																																{	/* Unsafe/gunzip.scm 370 */
																																	obj_t
																																		BgL_auxz00_4465;
																																	{	/* Unsafe/gunzip.scm 370 */
																																		long
																																			BgL_xz00_3672;
																																		BgL_xz00_3672
																																			=
																																			(long)
																																			CINT
																																			(CELL_REF
																																			(BgL_bbz00_4444));
																																		BgL_auxz00_4465
																																			=
																																			BINT(
																																			(BgL_xz00_3672
																																				>>
																																				(int)
																																				(BgL_dmpz00_1417)));
																																	}
																																	CELL_SET
																																		(BgL_bbz00_4444,
																																		BgL_auxz00_4465);
																																}
																																{	/* Unsafe/gunzip.scm 371 */
																																	obj_t
																																		BgL_auxz00_4466;
																																	BgL_auxz00_4466
																																		=
																																		SUBFX
																																		(CELL_REF
																																		(BgL_bkz00_4445),
																																		BINT
																																		(BgL_dmpz00_1417));
																																	CELL_SET
																																		(BgL_bkz00_4445,
																																		BgL_auxz00_4466);
																																}
																																if (
																																	((long)
																																		CINT
																																		(BgL_jz00_1418)
																																		< 16L))
																																	{	/* Unsafe/gunzip.scm 819 */
																																		{	/* Unsafe/gunzip.scm 821 */
																																			long
																																				BgL_kz00_3678;
																																			BgL_kz00_3678
																																				=
																																				(long)
																																				CINT((
																																					(obj_t)
																																					((obj_t) CELL_REF(BgL_iz00_4774))));
																																			VECTOR_SET
																																				(BgL_llz00_1379,
																																				BgL_kz00_3678,
																																				BgL_jz00_1418);
																																		}
																																		BgL_lz00_1375
																																			=
																																			BgL_jz00_1418;
																																		{	/* Unsafe/gunzip.scm 824 */
																																			obj_t
																																				BgL_auxz00_4776;
																																			BgL_auxz00_4776
																																				=
																																				ADDFX((
																																					(obj_t)
																																					((obj_t) CELL_REF(BgL_iz00_4774))), BINT(1L));
																																			CELL_SET
																																				(BgL_iz00_4774,
																																				BgL_auxz00_4776);
																																	}}
																																else
																																	{	/* Unsafe/gunzip.scm 819 */
																																		if (
																																			((long)
																																				CINT
																																				(BgL_jz00_1418)
																																				== 16L))
																																			{	/* Unsafe/gunzip.scm 827 */
																																				long
																																					BgL_jz00_1422;
																																				{	/* Unsafe/gunzip.scm 827 */
																																					long
																																						BgL_arg1448z00_1423;
																																					{	/* Unsafe/gunzip.scm 827 */
																																						obj_t
																																							BgL_arg1449z00_1424;
																																						BGl_z62NEEDBITSz62zz__gunza7ipza7
																																							(BgL_bbz00_4444,
																																							BgL_bkz00_4445,
																																							BgL_inputzd2portzd2_4446,
																																							BINT
																																							(2L));
																																						{	/* Unsafe/gunzip.scm 375 */
																																							obj_t
																																								BgL_rz00_3681;
																																							BgL_rz00_3681
																																								=
																																								CELL_REF
																																								(BgL_bbz00_4444);
																																							{	/* Unsafe/gunzip.scm 370 */
																																								obj_t
																																									BgL_auxz00_4467;
																																								{	/* Unsafe/gunzip.scm 370 */
																																									long
																																										BgL_xz00_3682;
																																									BgL_xz00_3682
																																										=
																																										(long)
																																										CINT
																																										(CELL_REF
																																										(BgL_bbz00_4444));
																																									BgL_auxz00_4467
																																										=
																																										BINT
																																										(
																																										(BgL_xz00_3682
																																											>>
																																											(int)
																																											(2L)));
																																								}
																																								CELL_SET
																																									(BgL_bbz00_4444,
																																									BgL_auxz00_4467);
																																							}
																																							{	/* Unsafe/gunzip.scm 371 */
																																								obj_t
																																									BgL_auxz00_4468;
																																								BgL_auxz00_4468
																																									=
																																									SUBFX
																																									(CELL_REF
																																									(BgL_bkz00_4445),
																																									BINT
																																									(2L));
																																								CELL_SET
																																									(BgL_bkz00_4445,
																																									BgL_auxz00_4468);
																																							}
																																							BgL_arg1449z00_1424
																																								=
																																								BgL_rz00_3681;
																																						}
																																						BgL_arg1448z00_1423
																																							=
																																							(
																																							(long)
																																							CINT
																																							(BgL_arg1449z00_1424)
																																							&
																																							3L);
																																					}
																																					BgL_jz00_1422
																																						=
																																						(3L
																																						+
																																						BgL_arg1448z00_1423);
																																				}
																																				BBOOL
																																					(BGl_setzd2litze70z35zz__gunza7ipza7
																																					(BgL_llz00_1379,
																																						BgL_iz00_4774,
																																						BgL_inputzd2portzd2_4446,
																																						BgL_nz00_1410,
																																						BgL_jz00_1422,
																																						BgL_lz00_1375));
																																			}
																																		else
																																			{	/* Unsafe/gunzip.scm 829 */
																																				bool_t
																																					BgL_test2547z00_5564;
																																				if (INTEGERP(BgL_jz00_1418))
																																					{	/* Unsafe/gunzip.scm 829 */
																																						BgL_test2547z00_5564
																																							=
																																							(
																																							(long)
																																							CINT
																																							(BgL_jz00_1418)
																																							==
																																							17L);
																																					}
																																				else
																																					{	/* Unsafe/gunzip.scm 829 */
																																						BgL_test2547z00_5564
																																							=
																																							BGl_2zd3zd3zz__r4_numbers_6_5z00
																																							(BgL_jz00_1418,
																																							BINT
																																							(17L));
																																					}
																																				if (BgL_test2547z00_5564)
																																					{	/* Unsafe/gunzip.scm 831 */
																																						long
																																							BgL_jz00_1426;
																																						{	/* Unsafe/gunzip.scm 831 */
																																							long
																																								BgL_arg1451z00_1427;
																																							{	/* Unsafe/gunzip.scm 831 */
																																								obj_t
																																									BgL_arg1452z00_1428;
																																								BGl_z62NEEDBITSz62zz__gunza7ipza7
																																									(BgL_bbz00_4444,
																																									BgL_bkz00_4445,
																																									BgL_inputzd2portzd2_4446,
																																									BINT
																																									(3L));
																																								{	/* Unsafe/gunzip.scm 375 */
																																									obj_t
																																										BgL_rz00_3687;
																																									BgL_rz00_3687
																																										=
																																										CELL_REF
																																										(BgL_bbz00_4444);
																																									{	/* Unsafe/gunzip.scm 370 */
																																										obj_t
																																											BgL_auxz00_4469;
																																										{	/* Unsafe/gunzip.scm 370 */
																																											long
																																												BgL_xz00_3688;
																																											BgL_xz00_3688
																																												=
																																												(long)
																																												CINT
																																												(CELL_REF
																																												(BgL_bbz00_4444));
																																											BgL_auxz00_4469
																																												=
																																												BINT
																																												(
																																												(BgL_xz00_3688
																																													>>
																																													(int)
																																													(3L)));
																																										}
																																										CELL_SET
																																											(BgL_bbz00_4444,
																																											BgL_auxz00_4469);
																																									}
																																									{	/* Unsafe/gunzip.scm 371 */
																																										obj_t
																																											BgL_auxz00_4470;
																																										BgL_auxz00_4470
																																											=
																																											SUBFX
																																											(CELL_REF
																																											(BgL_bkz00_4445),
																																											BINT
																																											(3L));
																																										CELL_SET
																																											(BgL_bkz00_4445,
																																											BgL_auxz00_4470);
																																									}
																																									BgL_arg1452z00_1428
																																										=
																																										BgL_rz00_3687;
																																								}
																																								BgL_arg1451z00_1427
																																									=
																																									(
																																									(long)
																																									CINT
																																									(BgL_arg1452z00_1428)
																																									&
																																									7L);
																																							}
																																							BgL_jz00_1426
																																								=
																																								(3L
																																								+
																																								BgL_arg1451z00_1427);
																																						}
																																						BGl_setzd2litze70z35zz__gunza7ipza7
																																							(BgL_llz00_1379,
																																							BgL_iz00_4774,
																																							BgL_inputzd2portzd2_4446,
																																							BgL_nz00_1410,
																																							BgL_jz00_1426,
																																							BINT
																																							(0L));
																																						BgL_lz00_1375
																																							=
																																							BINT
																																							(0L);
																																					}
																																				else
																																					{	/* Unsafe/gunzip.scm 836 */
																																						long
																																							BgL_jz00_1429;
																																						{	/* Unsafe/gunzip.scm 836 */
																																							long
																																								BgL_arg1453z00_1430;
																																							{	/* Unsafe/gunzip.scm 836 */
																																								obj_t
																																									BgL_arg1454z00_1431;
																																								BGl_z62NEEDBITSz62zz__gunza7ipza7
																																									(BgL_bbz00_4444,
																																									BgL_bkz00_4445,
																																									BgL_inputzd2portzd2_4446,
																																									BINT
																																									(7L));
																																								{	/* Unsafe/gunzip.scm 375 */
																																									obj_t
																																										BgL_rz00_3692;
																																									BgL_rz00_3692
																																										=
																																										CELL_REF
																																										(BgL_bbz00_4444);
																																									{	/* Unsafe/gunzip.scm 370 */
																																										obj_t
																																											BgL_auxz00_4471;
																																										{	/* Unsafe/gunzip.scm 370 */
																																											long
																																												BgL_xz00_3693;
																																											BgL_xz00_3693
																																												=
																																												(long)
																																												CINT
																																												(CELL_REF
																																												(BgL_bbz00_4444));
																																											BgL_auxz00_4471
																																												=
																																												BINT
																																												(
																																												(BgL_xz00_3693
																																													>>
																																													(int)
																																													(7L)));
																																										}
																																										CELL_SET
																																											(BgL_bbz00_4444,
																																											BgL_auxz00_4471);
																																									}
																																									{	/* Unsafe/gunzip.scm 371 */
																																										obj_t
																																											BgL_auxz00_4472;
																																										BgL_auxz00_4472
																																											=
																																											SUBFX
																																											(CELL_REF
																																											(BgL_bkz00_4445),
																																											BINT
																																											(7L));
																																										CELL_SET
																																											(BgL_bkz00_4445,
																																											BgL_auxz00_4472);
																																									}
																																									BgL_arg1454z00_1431
																																										=
																																										BgL_rz00_3692;
																																								}
																																								BgL_arg1453z00_1430
																																									=
																																									(
																																									(long)
																																									CINT
																																									(BgL_arg1454z00_1431)
																																									&
																																									127L);
																																							}
																																							BgL_jz00_1429
																																								=
																																								(11L
																																								+
																																								BgL_arg1453z00_1430);
																																						}
																																						BGl_setzd2litze70z35zz__gunza7ipza7
																																							(BgL_llz00_1379,
																																							BgL_iz00_4774,
																																							BgL_inputzd2portzd2_4446,
																																							BgL_nz00_1410,
																																							BgL_jz00_1429,
																																							BINT
																																							(0L));
																																						BgL_lz00_1375
																																							=
																																							BINT
																																							(0L);
																											}}}}}}}}
																											goto
																												BgL_zc3z04anonymousza31444ze3z87_1413;
																										}
																									else
																										{	/* Unsafe/gunzip.scm 800 */
																											((bool_t) 0);
																										}
																								}
																								{	/* Unsafe/gunzip.scm 843 */
																									obj_t BgL_tlz00_1450;

																									BgL_tlz00_1450 =
																										BGl_huftzd2buildze70z35zz__gunza7ipza7
																										(BgL_inputzd2portzd2_4446,
																										BgL_llz00_1379,
																										BgL_nlz00_1376, 257L,
																										BGl_vector2379z00zz__gunza7ipza7,
																										BGl_vector2381z00zz__gunza7ipza7,
																										9L, ((bool_t) 0));
																									{	/* Unsafe/gunzip.scm 844 */
																										obj_t BgL_blz00_1451;
																										obj_t BgL_okzf3zf3_1452;

																										{	/* Unsafe/gunzip.scm 845 */
																											obj_t BgL_tmpz00_3697;

																											{	/* Unsafe/gunzip.scm 845 */
																												int BgL_tmpz00_5600;

																												BgL_tmpz00_5600 =
																													(int) (1L);
																												BgL_tmpz00_3697 =
																													BGL_MVALUES_VAL
																													(BgL_tmpz00_5600);
																											}
																											{	/* Unsafe/gunzip.scm 845 */
																												int BgL_tmpz00_5603;

																												BgL_tmpz00_5603 =
																													(int) (1L);
																												BGL_MVALUES_VAL_SET
																													(BgL_tmpz00_5603,
																													BUNSPEC);
																											}
																											BgL_blz00_1451 =
																												BgL_tmpz00_3697;
																										}
																										{	/* Unsafe/gunzip.scm 845 */
																											obj_t BgL_tmpz00_3698;

																											{	/* Unsafe/gunzip.scm 845 */
																												int BgL_tmpz00_5606;

																												BgL_tmpz00_5606 =
																													(int) (2L);
																												BgL_tmpz00_3698 =
																													BGL_MVALUES_VAL
																													(BgL_tmpz00_5606);
																											}
																											{	/* Unsafe/gunzip.scm 845 */
																												int BgL_tmpz00_5609;

																												BgL_tmpz00_5609 =
																													(int) (2L);
																												BGL_MVALUES_VAL_SET
																													(BgL_tmpz00_5609,
																													BUNSPEC);
																											}
																											BgL_okzf3zf3_1452 =
																												BgL_tmpz00_3698;
																										}
																										if (CBOOL
																											(BgL_okzf3zf3_1452))
																											{	/* Unsafe/gunzip.scm 849 */
																												obj_t BgL_tdz00_1453;

																												BgL_tdz00_1453 =
																													BGl_huftzd2buildze70z35zz__gunza7ipza7
																													(BgL_inputzd2portzd2_4446,
																													BGl_subvectorz00zz__gunza7ipza7
																													(BgL_llz00_1379,
																														BgL_nlz00_1376),
																													BgL_ndz00_1377, 0L,
																													BGl_vector2383z00zz__gunza7ipza7,
																													BGl_vector2385z00zz__gunza7ipza7,
																													6L, ((bool_t) 0));
																												{	/* Unsafe/gunzip.scm 850 */
																													obj_t BgL_bdz00_1454;
																													obj_t
																														BgL_okzf3zf3_1455;
																													{	/* Unsafe/gunzip.scm 851 */
																														obj_t
																															BgL_tmpz00_3699;
																														{	/* Unsafe/gunzip.scm 851 */
																															int
																																BgL_tmpz00_5616;
																															BgL_tmpz00_5616 =
																																(int) (1L);
																															BgL_tmpz00_3699 =
																																BGL_MVALUES_VAL
																																(BgL_tmpz00_5616);
																														}
																														{	/* Unsafe/gunzip.scm 851 */
																															int
																																BgL_tmpz00_5619;
																															BgL_tmpz00_5619 =
																																(int) (1L);
																															BGL_MVALUES_VAL_SET
																																(BgL_tmpz00_5619,
																																BUNSPEC);
																														}
																														BgL_bdz00_1454 =
																															BgL_tmpz00_3699;
																													}
																													{	/* Unsafe/gunzip.scm 851 */
																														obj_t
																															BgL_tmpz00_3700;
																														{	/* Unsafe/gunzip.scm 851 */
																															int
																																BgL_tmpz00_5622;
																															BgL_tmpz00_5622 =
																																(int) (2L);
																															BgL_tmpz00_3700 =
																																BGL_MVALUES_VAL
																																(BgL_tmpz00_5622);
																														}
																														{	/* Unsafe/gunzip.scm 851 */
																															int
																																BgL_tmpz00_5625;
																															BgL_tmpz00_5625 =
																																(int) (2L);
																															BGL_MVALUES_VAL_SET
																																(BgL_tmpz00_5625,
																																BUNSPEC);
																														}
																														BgL_okzf3zf3_1455 =
																															BgL_tmpz00_3700;
																													}
																													if (CBOOL
																														(BgL_okzf3zf3_1455))
																														{	/* Unsafe/gunzip.scm 851 */
																															BgL_tlz00_1553 =
																																BgL_tlz00_1450;
																															BgL_tdz00_1554 =
																																BgL_tdz00_1453;
																															BgL_blz00_1555 =
																																BgL_blz00_1451;
																															BgL_bdz00_1556 =
																																BgL_bdz00_1454;
																														BgL_zc3z04anonymousza31523ze3z87_1557:
																															{	/* Unsafe/gunzip.scm 592 */
																																long
																																	BgL_mlz00_1558;
																																long
																																	BgL_mdz00_1559;
																																obj_t
																																	BgL_tz00_4473;
																																obj_t
																																	BgL_ez00_4474;
																																obj_t
																																	BgL_nz00_4475;
																																obj_t
																																	BgL_dz00_4476;
																																{	/* Unsafe/gunzip.scm 592 */
																																	obj_t
																																		BgL_arg1595z00_1671;
																																	BgL_arg1595z00_1671
																																		=
																																		BGl_vector2393z00zz__gunza7ipza7;
																																	{	/* Unsafe/gunzip.scm 592 */
																																		long
																																			BgL_kz00_3354;
																																		BgL_kz00_3354
																																			=
																																			(long)
																																			CINT
																																			(BgL_blz00_1555);
																																		BgL_mlz00_1558
																																			=
																																			(long)
																																			CINT
																																			(VECTOR_REF
																																			(BgL_arg1595z00_1671,
																																				BgL_kz00_3354));
																																}}
																																{	/* Unsafe/gunzip.scm 593 */
																																	obj_t
																																		BgL_arg1598z00_1672;
																																	BgL_arg1598z00_1672
																																		=
																																		BGl_vector2393z00zz__gunza7ipza7;
																																	{	/* Unsafe/gunzip.scm 593 */
																																		long
																																			BgL_kz00_3356;
																																		BgL_kz00_3356
																																			=
																																			(long)
																																			CINT
																																			(BgL_bdz00_1556);
																																		BgL_mdz00_1559
																																			=
																																			(long)
																																			CINT
																																			(VECTOR_REF
																																			(BgL_arg1598z00_1672,
																																				BgL_kz00_3356));
																																}}
																																BgL_tz00_4473 =
																																	MAKE_CELL
																																	(BUNSPEC);
																																BgL_ez00_4474 =
																																	MAKE_CELL(BINT
																																	(0L));
																																BgL_nz00_4475 =
																																	MAKE_CELL(BINT
																																	(0L));
																																BgL_dz00_4476 =
																																	MAKE_CELL(BINT
																																	(0L));
																																BgL_statez00_1343
																																	=
																																	BGl_z62loopzd2inflatezb0zz__gunza7ipza7
																																	(BgL_tdz00_1554,
																																	BgL_mdz00_1559,
																																	BgL_bdz00_1556,
																																	BgL_tlz00_1553,
																																	BgL_mlz00_1558,
																																	BgL_blz00_1555,
																																	BgL_bkz00_4445,
																																	BgL_bbz00_4444,
																																	BgL_tz00_4473,
																																	BgL_inputzd2portzd2_4446,
																																	BgL_slidez00_4443,
																																	BgL_ez00_4474,
																																	BgL_wpz00_4442,
																																	BgL_nz00_4475,
																																	BgL_dz00_4476,
																																	BgL_wsiza7eza7_4441,
																																	BINT(0L));
																														}}
																													else
																														{	/* Unsafe/gunzip.scm 328 */
																															BgL_z62iozd2parsezd2errorz62_bglt
																																BgL_arg1404z00_3701;
																															{	/* Unsafe/gunzip.scm 328 */
																																BgL_z62iozd2parsezd2errorz62_bglt
																																	BgL_new1076z00_3702;
																																{	/* Unsafe/gunzip.scm 328 */
																																	BgL_z62iozd2parsezd2errorz62_bglt
																																		BgL_new1075z00_3703;
																																	BgL_new1075z00_3703
																																		=
																																		(
																																		(BgL_z62iozd2parsezd2errorz62_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_z62iozd2parsezd2errorz62_bgl))));
																																	{	/* Unsafe/gunzip.scm 328 */
																																		long
																																			BgL_arg1407z00_3704;
																																		BgL_arg1407z00_3704
																																			=
																																			BGL_CLASS_NUM
																																			(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1075z00_3703), BgL_arg1407z00_3704);
																																	}
																																	BgL_new1076z00_3702
																																		=
																																		BgL_new1075z00_3703;
																																}
																																((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1076z00_3702)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																																((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1076z00_3702)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																																{
																																	obj_t
																																		BgL_auxz00_5649;
																																	{	/* Unsafe/gunzip.scm 328 */
																																		obj_t
																																			BgL_arg1405z00_3708;
																																		{	/* Unsafe/gunzip.scm 328 */
																																			obj_t
																																				BgL_arg1406z00_3709;
																																			{	/* Unsafe/gunzip.scm 328 */
																																				obj_t
																																					BgL_classz00_3710;
																																				BgL_classz00_3710
																																					=
																																					BGl_z62iozd2parsezd2errorz62zz__objectz00;
																																				BgL_arg1406z00_3709
																																					=
																																					BGL_CLASS_ALL_FIELDS
																																					(BgL_classz00_3710);
																																			}
																																			BgL_arg1405z00_3708
																																				=
																																				VECTOR_REF
																																				(BgL_arg1406z00_3709,
																																				2L);
																																		}
																																		BgL_auxz00_5649
																																			=
																																			BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																																			(BgL_arg1405z00_3708);
																																	}
																																	((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1076z00_3702)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5649), BUNSPEC);
																																}
																																((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1076z00_3702)))->BgL_procz00) = ((obj_t) BGl_string2376z00zz__gunza7ipza7), BUNSPEC);
																																((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1076z00_3702)))->BgL_msgz00) = ((obj_t) BGl_string2395z00zz__gunza7ipza7), BUNSPEC);
																																((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1076z00_3702)))->BgL_objz00) = ((obj_t) BgL_inputzd2portzd2_4446), BUNSPEC);
																																BgL_arg1404z00_3701
																																	=
																																	BgL_new1076z00_3702;
																															}
																															BgL_statez00_1343
																																=
																																BGl_raisez00zz__errorz00
																																(((obj_t)
																																	BgL_arg1404z00_3701));
																											}}}
																										else
																											{	/* Unsafe/gunzip.scm 328 */
																												BgL_z62iozd2parsezd2errorz62_bglt
																													BgL_arg1404z00_3712;
																												{	/* Unsafe/gunzip.scm 328 */
																													BgL_z62iozd2parsezd2errorz62_bglt
																														BgL_new1076z00_3713;
																													{	/* Unsafe/gunzip.scm 328 */
																														BgL_z62iozd2parsezd2errorz62_bglt
																															BgL_new1075z00_3714;
																														BgL_new1075z00_3714
																															=
																															(
																															(BgL_z62iozd2parsezd2errorz62_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_z62iozd2parsezd2errorz62_bgl))));
																														{	/* Unsafe/gunzip.scm 328 */
																															long
																																BgL_arg1407z00_3715;
																															BgL_arg1407z00_3715
																																=
																																BGL_CLASS_NUM
																																(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1075z00_3714), BgL_arg1407z00_3715);
																														}
																														BgL_new1076z00_3713
																															=
																															BgL_new1075z00_3714;
																													}
																													((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1076z00_3713)))->BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																													((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1076z00_3713)))->BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
																													{
																														obj_t
																															BgL_auxz00_5671;
																														{	/* Unsafe/gunzip.scm 328 */
																															obj_t
																																BgL_arg1405z00_3719;
																															{	/* Unsafe/gunzip.scm 328 */
																																obj_t
																																	BgL_arg1406z00_3720;
																																{	/* Unsafe/gunzip.scm 328 */
																																	obj_t
																																		BgL_classz00_3721;
																																	BgL_classz00_3721
																																		=
																																		BGl_z62iozd2parsezd2errorz62zz__objectz00;
																																	BgL_arg1406z00_3720
																																		=
																																		BGL_CLASS_ALL_FIELDS
																																		(BgL_classz00_3721);
																																}
																																BgL_arg1405z00_3719
																																	=
																																	VECTOR_REF
																																	(BgL_arg1406z00_3720,
																																	2L);
																															}
																															BgL_auxz00_5671 =
																																BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																																(BgL_arg1405z00_3719);
																														}
																														((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt) BgL_new1076z00_3713)))->BgL_stackz00) = ((obj_t) BgL_auxz00_5671), BUNSPEC);
																													}
																													((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1076z00_3713)))->BgL_procz00) = ((obj_t) BGl_string2376z00zz__gunza7ipza7), BUNSPEC);
																													((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1076z00_3713)))->BgL_msgz00) = ((obj_t) BGl_string2395z00zz__gunza7ipza7), BUNSPEC);
																													((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt) BgL_new1076z00_3713)))->BgL_objz00) = ((obj_t) BgL_inputzd2portzd2_4446), BUNSPEC);
																													BgL_arg1404z00_3712 =
																														BgL_new1076z00_3713;
																												}
																												BgL_statez00_1343 =
																													BGl_raisez00zz__errorz00
																													(((obj_t)
																														BgL_arg1404z00_3712));
																							}}}}
																						else
																							{	/* Unsafe/gunzip.scm 792 */
																								BgL_statez00_1343 = BFALSE;
																							}
																					}
																				}
																			}
																	}
															}
														}
													}
												}
											}
											break;
										case 0L:

											{	/* Unsafe/gunzip.scm 712 */
												long BgL_arg1505z00_1524;

												{	/* Unsafe/gunzip.scm 712 */
													long BgL_xz00_3475;

													BgL_xz00_3475 = (long) CINT(CELL_REF(BgL_bkz00_4445));
													BgL_arg1505z00_1524 = (BgL_xz00_3475 & 7L);
												}
												{	/* Unsafe/gunzip.scm 370 */
													obj_t BgL_auxz00_4451;

													{	/* Unsafe/gunzip.scm 370 */
														long BgL_xz00_3476;

														BgL_xz00_3476 =
															(long) CINT(CELL_REF(BgL_bbz00_4444));
														BgL_auxz00_4451 =
															BINT(
															(BgL_xz00_3476 >> (int) (BgL_arg1505z00_1524)));
													}
													CELL_SET(BgL_bbz00_4444, BgL_auxz00_4451);
												}
												{	/* Unsafe/gunzip.scm 371 */
													obj_t BgL_auxz00_4452;

													BgL_auxz00_4452 =
														SUBFX(CELL_REF(BgL_bkz00_4445),
														BINT(BgL_arg1505z00_1524));
													CELL_SET(BgL_bkz00_4445, BgL_auxz00_4452);
											}}
											BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4444,
												BgL_bkz00_4445, BgL_inputzd2portzd2_4446, BINT(16L));
											{	/* Unsafe/gunzip.scm 716 */
												long BgL_nz00_1525;

												{	/* Unsafe/gunzip.scm 716 */
													long BgL_xz00_3480;

													BgL_xz00_3480 = (long) CINT(CELL_REF(BgL_bbz00_4444));
													BgL_nz00_1525 = (BgL_xz00_3480 & 65535L);
												}
												{	/* Unsafe/gunzip.scm 370 */
													obj_t BgL_auxz00_4453;

													{	/* Unsafe/gunzip.scm 370 */
														long BgL_xz00_3481;

														BgL_xz00_3481 =
															(long) CINT(CELL_REF(BgL_bbz00_4444));
														BgL_auxz00_4453 =
															BINT((BgL_xz00_3481 >> (int) (16L)));
													}
													CELL_SET(BgL_bbz00_4444, BgL_auxz00_4453);
												}
												{	/* Unsafe/gunzip.scm 371 */
													obj_t BgL_auxz00_4454;

													BgL_auxz00_4454 =
														SUBFX(CELL_REF(BgL_bkz00_4445), BINT(16L));
													CELL_SET(BgL_bkz00_4445, BgL_auxz00_4454);
												}
												BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4444,
													BgL_bkz00_4445, BgL_inputzd2portzd2_4446, BINT(16L));
												{	/* Unsafe/gunzip.scm 719 */
													bool_t BgL_test2551z00_5705;

													{	/* Unsafe/gunzip.scm 719 */
														long BgL_arg1511z00_1531;

														{	/* Unsafe/gunzip.scm 719 */
															long BgL_tmpz00_5706;

															{	/* Unsafe/gunzip.scm 719 */
																long BgL_xz00_3483;

																BgL_xz00_3483 =
																	(long) CINT(CELL_REF(BgL_bbz00_4444));
																BgL_tmpz00_5706 = ~(BgL_xz00_3483);
															}
															BgL_arg1511z00_1531 = (BgL_tmpz00_5706 & 65535L);
														}
														BgL_test2551z00_5705 =
															(BgL_nz00_1525 == BgL_arg1511z00_1531);
													}
													if (BgL_test2551z00_5705)
														{	/* Unsafe/gunzip.scm 719 */
															BFALSE;
														}
													else
														{	/* Unsafe/gunzip.scm 721 */
															obj_t BgL_arg1509z00_1529;

															{	/* Unsafe/gunzip.scm 721 */
																obj_t BgL_list1510z00_1530;

																BgL_list1510z00_1530 =
																	MAKE_YOUNG_PAIR(BINT(BgL_nz00_1525), BNIL);
																BgL_arg1509z00_1529 =
																	BGl_formatz00zz__r4_output_6_10_3z00
																	(BGl_string2387z00zz__gunza7ipza7,
																	BgL_list1510z00_1530);
															}
															{	/* Unsafe/gunzip.scm 328 */
																BgL_z62iozd2parsezd2errorz62_bglt
																	BgL_arg1404z00_3487;
																{	/* Unsafe/gunzip.scm 328 */
																	BgL_z62iozd2parsezd2errorz62_bglt
																		BgL_new1076z00_3488;
																	{	/* Unsafe/gunzip.scm 328 */
																		BgL_z62iozd2parsezd2errorz62_bglt
																			BgL_new1075z00_3489;
																		BgL_new1075z00_3489 =
																			((BgL_z62iozd2parsezd2errorz62_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_z62iozd2parsezd2errorz62_bgl))));
																		{	/* Unsafe/gunzip.scm 328 */
																			long BgL_arg1407z00_3490;

																			BgL_arg1407z00_3490 =
																				BGL_CLASS_NUM
																				(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1075z00_3489),
																				BgL_arg1407z00_3490);
																		}
																		BgL_new1076z00_3488 = BgL_new1075z00_3489;
																	}
																	((((BgL_z62exceptionz62_bglt) COBJECT(
																					((BgL_z62exceptionz62_bglt)
																						BgL_new1076z00_3488)))->
																			BgL_fnamez00) =
																		((obj_t) BFALSE), BUNSPEC);
																	((((BgL_z62exceptionz62_bglt)
																				COBJECT(((BgL_z62exceptionz62_bglt)
																						BgL_new1076z00_3488)))->
																			BgL_locationz00) =
																		((obj_t) BFALSE), BUNSPEC);
																	{
																		obj_t BgL_auxz00_5722;

																		{	/* Unsafe/gunzip.scm 328 */
																			obj_t BgL_arg1405z00_3494;

																			{	/* Unsafe/gunzip.scm 328 */
																				obj_t BgL_arg1406z00_3495;

																				{	/* Unsafe/gunzip.scm 328 */
																					obj_t BgL_classz00_3496;

																					BgL_classz00_3496 =
																						BGl_z62iozd2parsezd2errorz62zz__objectz00;
																					BgL_arg1406z00_3495 =
																						BGL_CLASS_ALL_FIELDS
																						(BgL_classz00_3496);
																				}
																				BgL_arg1405z00_3494 =
																					VECTOR_REF(BgL_arg1406z00_3495, 2L);
																			}
																			BgL_auxz00_5722 =
																				BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																				(BgL_arg1405z00_3494);
																		}
																		((((BgL_z62exceptionz62_bglt) COBJECT(
																						((BgL_z62exceptionz62_bglt)
																							BgL_new1076z00_3488)))->
																				BgL_stackz00) =
																			((obj_t) BgL_auxz00_5722), BUNSPEC);
																	}
																	((((BgL_z62errorz62_bglt) COBJECT(
																					((BgL_z62errorz62_bglt)
																						BgL_new1076z00_3488)))->
																			BgL_procz00) =
																		((obj_t) BGl_string2388z00zz__gunza7ipza7),
																		BUNSPEC);
																	((((BgL_z62errorz62_bglt)
																				COBJECT(((BgL_z62errorz62_bglt)
																						BgL_new1076z00_3488)))->
																			BgL_msgz00) =
																		((obj_t) BgL_arg1509z00_1529), BUNSPEC);
																	((((BgL_z62errorz62_bglt)
																				COBJECT(((BgL_z62errorz62_bglt)
																						BgL_new1076z00_3488)))->
																			BgL_objz00) =
																		((obj_t) BgL_inputzd2portzd2_4446),
																		BUNSPEC);
																	BgL_arg1404z00_3487 = BgL_new1076z00_3488;
																}
																BGl_raisez00zz__errorz00(
																	((obj_t) BgL_arg1404z00_3487));
												}}}
												{	/* Unsafe/gunzip.scm 370 */
													obj_t BgL_auxz00_4455;

													{	/* Unsafe/gunzip.scm 370 */
														long BgL_xz00_3498;

														BgL_xz00_3498 =
															(long) CINT(CELL_REF(BgL_bbz00_4444));
														BgL_auxz00_4455 =
															BINT((BgL_xz00_3498 >> (int) (16L)));
													}
													CELL_SET(BgL_bbz00_4444, BgL_auxz00_4455);
												}
												{	/* Unsafe/gunzip.scm 371 */
													obj_t BgL_auxz00_4456;

													BgL_auxz00_4456 =
														SUBFX(CELL_REF(BgL_bkz00_4445), BINT(16L));
													CELL_SET(BgL_bkz00_4445, BgL_auxz00_4456);
												}
												BgL_statez00_1343 =
													BGl_z62loop2314z62zz__gunza7ipza7(BgL_bkz00_4445,
													BgL_wpz00_4442, BgL_slidez00_4443, BgL_bbz00_4444,
													BgL_inputzd2portzd2_4446, BgL_wsiza7eza7_4441,
													BgL_nz00_1525);
											} break;
										case 1L:

											{	/* Unsafe/gunzip.scm 743 */
												obj_t BgL_lz00_1473;

												BgL_lz00_1473 = make_vector(288L, BUNSPEC);
												{
													long BgL_i1133z00_3519;

													BgL_i1133z00_3519 = 0L;
												BgL_loop1134z00_3518:
													if ((BgL_i1133z00_3519 < 144L))
														{	/* Unsafe/gunzip.scm 745 */
															VECTOR_SET(BgL_lz00_1473, BgL_i1133z00_3519,
																BINT(8L));
															{
																long BgL_i1133z00_5748;

																BgL_i1133z00_5748 = (BgL_i1133z00_3519 + 1L);
																BgL_i1133z00_3519 = BgL_i1133z00_5748;
																goto BgL_loop1134z00_3518;
															}
														}
													else
														{	/* Unsafe/gunzip.scm 745 */
															((bool_t) 0);
														}
												}
												{
													long BgL_i1135z00_3532;

													BgL_i1135z00_3532 = 144L;
												BgL_loop1136z00_3531:
													if ((BgL_i1135z00_3532 < 256L))
														{	/* Unsafe/gunzip.scm 746 */
															VECTOR_SET(BgL_lz00_1473, BgL_i1135z00_3532,
																BINT(9L));
															{
																long BgL_i1135z00_5754;

																BgL_i1135z00_5754 = (BgL_i1135z00_3532 + 1L);
																BgL_i1135z00_3532 = BgL_i1135z00_5754;
																goto BgL_loop1136z00_3531;
															}
														}
													else
														{	/* Unsafe/gunzip.scm 746 */
															((bool_t) 0);
														}
												}
												{
													long BgL_i1137z00_3545;

													BgL_i1137z00_3545 = 256L;
												BgL_loop1138z00_3544:
													if ((BgL_i1137z00_3545 < 280L))
														{	/* Unsafe/gunzip.scm 747 */
															VECTOR_SET(BgL_lz00_1473, BgL_i1137z00_3545,
																BINT(7L));
															{
																long BgL_i1137z00_5760;

																BgL_i1137z00_5760 = (BgL_i1137z00_3545 + 1L);
																BgL_i1137z00_3545 = BgL_i1137z00_5760;
																goto BgL_loop1138z00_3544;
															}
														}
													else
														{	/* Unsafe/gunzip.scm 747 */
															((bool_t) 0);
														}
												}
												{
													long BgL_i1139z00_3558;

													BgL_i1139z00_3558 = 280L;
												BgL_loop1140z00_3557:
													if ((BgL_i1139z00_3558 < 288L))
														{	/* Unsafe/gunzip.scm 748 */
															VECTOR_SET(BgL_lz00_1473, BgL_i1139z00_3558,
																BINT(8L));
															{
																long BgL_i1139z00_5766;

																BgL_i1139z00_5766 = (BgL_i1139z00_3558 + 1L);
																BgL_i1139z00_3558 = BgL_i1139z00_5766;
																goto BgL_loop1140z00_3557;
															}
														}
													else
														{	/* Unsafe/gunzip.scm 748 */
															((bool_t) 0);
														}
												}
												{	/* Unsafe/gunzip.scm 750 */
													obj_t BgL_tlz00_1502;

													BgL_tlz00_1502 =
														BGl_huftzd2buildze70z35zz__gunza7ipza7
														(BgL_inputzd2portzd2_4446, BgL_lz00_1473, 288L,
														257L, BGl_vector2379z00zz__gunza7ipza7,
														BGl_vector2381z00zz__gunza7ipza7, 7L, ((bool_t) 0));
													{	/* Unsafe/gunzip.scm 751 */
														obj_t BgL_blz00_1503;
														obj_t BgL_okzf3zf3_1504;

														{	/* Unsafe/gunzip.scm 753 */
															obj_t BgL_tmpz00_3566;

															{	/* Unsafe/gunzip.scm 753 */
																int BgL_tmpz00_5769;

																BgL_tmpz00_5769 = (int) (1L);
																BgL_tmpz00_3566 =
																	BGL_MVALUES_VAL(BgL_tmpz00_5769);
															}
															{	/* Unsafe/gunzip.scm 753 */
																int BgL_tmpz00_5772;

																BgL_tmpz00_5772 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_5772, BUNSPEC);
															}
															BgL_blz00_1503 = BgL_tmpz00_3566;
														}
														{	/* Unsafe/gunzip.scm 753 */
															obj_t BgL_tmpz00_3567;

															{	/* Unsafe/gunzip.scm 753 */
																int BgL_tmpz00_5775;

																BgL_tmpz00_5775 = (int) (2L);
																BgL_tmpz00_3567 =
																	BGL_MVALUES_VAL(BgL_tmpz00_5775);
															}
															{	/* Unsafe/gunzip.scm 753 */
																int BgL_tmpz00_5778;

																BgL_tmpz00_5778 = (int) (2L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_5778, BUNSPEC);
															}
															BgL_okzf3zf3_1504 = BgL_tmpz00_3567;
														}
														if (CBOOL(BgL_okzf3zf3_1504))
															{	/* Unsafe/gunzip.scm 753 */
																{
																	long BgL_i1142z00_3573;

																	BgL_i1142z00_3573 = 0L;
																BgL_loop1143z00_3572:
																	if ((BgL_i1142z00_3573 < 30L))
																		{	/* Unsafe/gunzip.scm 755 */
																			VECTOR_SET(BgL_lz00_1473,
																				BgL_i1142z00_3573, BINT(5L));
																			{
																				long BgL_i1142z00_5787;

																				BgL_i1142z00_5787 =
																					(BgL_i1142z00_3573 + 1L);
																				BgL_i1142z00_3573 = BgL_i1142z00_5787;
																				goto BgL_loop1143z00_3572;
																			}
																		}
																	else
																		{	/* Unsafe/gunzip.scm 755 */
																			((bool_t) 0);
																		}
																}
																{	/* Unsafe/gunzip.scm 756 */
																	obj_t BgL_tdz00_1513;

																	BgL_tdz00_1513 =
																		BGl_huftzd2buildze70z35zz__gunza7ipza7
																		(BgL_inputzd2portzd2_4446, BgL_lz00_1473,
																		30L, 0L, BGl_vector2383z00zz__gunza7ipza7,
																		BGl_vector2385z00zz__gunza7ipza7, 5L,
																		((bool_t) 1));
																	{	/* Unsafe/gunzip.scm 757 */
																		obj_t BgL_bdz00_1514;
																		obj_t BgL_okzf3zf3_1515;

																		{	/* Unsafe/gunzip.scm 758 */
																			obj_t BgL_tmpz00_3581;

																			{	/* Unsafe/gunzip.scm 758 */
																				int BgL_tmpz00_5790;

																				BgL_tmpz00_5790 = (int) (1L);
																				BgL_tmpz00_3581 =
																					BGL_MVALUES_VAL(BgL_tmpz00_5790);
																			}
																			{	/* Unsafe/gunzip.scm 758 */
																				int BgL_tmpz00_5793;

																				BgL_tmpz00_5793 = (int) (1L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_5793,
																					BUNSPEC);
																			}
																			BgL_bdz00_1514 = BgL_tmpz00_3581;
																		}
																		{	/* Unsafe/gunzip.scm 758 */
																			obj_t BgL_tmpz00_3582;

																			{	/* Unsafe/gunzip.scm 758 */
																				int BgL_tmpz00_5796;

																				BgL_tmpz00_5796 = (int) (2L);
																				BgL_tmpz00_3582 =
																					BGL_MVALUES_VAL(BgL_tmpz00_5796);
																			}
																			{	/* Unsafe/gunzip.scm 758 */
																				int BgL_tmpz00_5799;

																				BgL_tmpz00_5799 = (int) (2L);
																				BGL_MVALUES_VAL_SET(BgL_tmpz00_5799,
																					BUNSPEC);
																			}
																			BgL_okzf3zf3_1515 = BgL_tmpz00_3582;
																		}
																		if (CBOOL(BgL_okzf3zf3_1515))
																			{
																				obj_t BgL_bdz00_5807;
																				obj_t BgL_blz00_5806;
																				obj_t BgL_tdz00_5805;
																				obj_t BgL_tlz00_5804;

																				BgL_tlz00_5804 = BgL_tlz00_1502;
																				BgL_tdz00_5805 = BgL_tdz00_1513;
																				BgL_blz00_5806 = BgL_blz00_1503;
																				BgL_bdz00_5807 = BgL_bdz00_1514;
																				BgL_bdz00_1556 = BgL_bdz00_5807;
																				BgL_blz00_1555 = BgL_blz00_5806;
																				BgL_tdz00_1554 = BgL_tdz00_5805;
																				BgL_tlz00_1553 = BgL_tlz00_5804;
																				goto
																					BgL_zc3z04anonymousza31523ze3z87_1557;
																			}
																		else
																			{	/* Unsafe/gunzip.scm 758 */
																				BgL_statez00_1343 = BFALSE;
																			}
																	}
																}
															}
														else
															{	/* Unsafe/gunzip.scm 753 */
																BgL_statez00_1343 = BFALSE;
															}
													}
												}
											}
											break;
										default:
											{	/* Unsafe/gunzip.scm 869 */
												obj_t BgL_arg1421z00_1368;

												{	/* Unsafe/gunzip.scm 869 */
													obj_t BgL_list1422z00_1369;

													BgL_list1422z00_1369 =
														MAKE_YOUNG_PAIR(BINT(BgL_tz00_1342), BNIL);
													BgL_arg1421z00_1368 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string2378z00zz__gunza7ipza7,
														BgL_list1422z00_1369);
												}
												{	/* Unsafe/gunzip.scm 328 */
													BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3731;

													{	/* Unsafe/gunzip.scm 328 */
														BgL_z62iozd2parsezd2errorz62_bglt
															BgL_new1076z00_3732;
														{	/* Unsafe/gunzip.scm 328 */
															BgL_z62iozd2parsezd2errorz62_bglt
																BgL_new1075z00_3733;
															BgL_new1075z00_3733 =
																((BgL_z62iozd2parsezd2errorz62_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_z62iozd2parsezd2errorz62_bgl))));
															{	/* Unsafe/gunzip.scm 328 */
																long BgL_arg1407z00_3734;

																BgL_arg1407z00_3734 =
																	BGL_CLASS_NUM
																	(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																		BgL_new1075z00_3733), BgL_arg1407z00_3734);
															}
															BgL_new1076z00_3732 = BgL_new1075z00_3733;
														}
														((((BgL_z62exceptionz62_bglt) COBJECT(
																		((BgL_z62exceptionz62_bglt)
																			BgL_new1076z00_3732)))->BgL_fnamez00) =
															((obj_t) BFALSE), BUNSPEC);
														((((BgL_z62exceptionz62_bglt)
																	COBJECT(((BgL_z62exceptionz62_bglt)
																			BgL_new1076z00_3732)))->BgL_locationz00) =
															((obj_t) BFALSE), BUNSPEC);
														{
															obj_t BgL_auxz00_5819;

															{	/* Unsafe/gunzip.scm 328 */
																obj_t BgL_arg1405z00_3738;

																{	/* Unsafe/gunzip.scm 328 */
																	obj_t BgL_arg1406z00_3739;

																	{	/* Unsafe/gunzip.scm 328 */
																		obj_t BgL_classz00_3740;

																		BgL_classz00_3740 =
																			BGl_z62iozd2parsezd2errorz62zz__objectz00;
																		BgL_arg1406z00_3739 =
																			BGL_CLASS_ALL_FIELDS(BgL_classz00_3740);
																	}
																	BgL_arg1405z00_3738 =
																		VECTOR_REF(BgL_arg1406z00_3739, 2L);
																}
																BgL_auxz00_5819 =
																	BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																	(BgL_arg1405z00_3738);
															}
															((((BgL_z62exceptionz62_bglt) COBJECT(
																			((BgL_z62exceptionz62_bglt)
																				BgL_new1076z00_3732)))->BgL_stackz00) =
																((obj_t) BgL_auxz00_5819), BUNSPEC);
														}
														((((BgL_z62errorz62_bglt) COBJECT(
																		((BgL_z62errorz62_bglt)
																			BgL_new1076z00_3732)))->BgL_procz00) =
															((obj_t) BGl_string2376z00zz__gunza7ipza7),
															BUNSPEC);
														((((BgL_z62errorz62_bglt)
																	COBJECT(((BgL_z62errorz62_bglt)
																			BgL_new1076z00_3732)))->BgL_msgz00) =
															((obj_t) BgL_arg1421z00_1368), BUNSPEC);
														((((BgL_z62errorz62_bglt)
																	COBJECT(((BgL_z62errorz62_bglt)
																			BgL_new1076z00_3732)))->BgL_objz00) =
															((obj_t) BgL_inputzd2portzd2_4446), BUNSPEC);
														BgL_arg1404z00_3731 = BgL_new1076z00_3732;
													}
													BgL_statez00_1343 =
														BGl_raisez00zz__errorz00(
														((obj_t) BgL_arg1404z00_3731));
								}}}}
								{	/* Unsafe/gunzip.scm 864 */
									obj_t BgL_valz00_1344;
									obj_t BgL_kontz00_1345;

									{	/* Unsafe/gunzip.scm 871 */
										obj_t BgL_tmpz00_3742;

										{	/* Unsafe/gunzip.scm 871 */
											int BgL_tmpz00_5834;

											BgL_tmpz00_5834 = (int) (1L);
											BgL_tmpz00_3742 = BGL_MVALUES_VAL(BgL_tmpz00_5834);
										}
										{	/* Unsafe/gunzip.scm 871 */
											int BgL_tmpz00_5837;

											BgL_tmpz00_5837 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_5837, BUNSPEC);
										}
										BgL_valz00_1344 = BgL_tmpz00_3742;
									}
									{	/* Unsafe/gunzip.scm 871 */
										obj_t BgL_tmpz00_3743;

										{	/* Unsafe/gunzip.scm 871 */
											int BgL_tmpz00_5840;

											BgL_tmpz00_5840 = (int) (2L);
											BgL_tmpz00_3743 = BGL_MVALUES_VAL(BgL_tmpz00_5840);
										}
										{	/* Unsafe/gunzip.scm 871 */
											int BgL_tmpz00_5843;

											BgL_tmpz00_5843 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_5843, BUNSPEC);
										}
										BgL_kontz00_1345 = BgL_tmpz00_3743;
									}
									BgL_statez00_1313 =
										BGl_z62loop2313z62zz__gunza7ipza7(BgL_ez00_1341,
										BgL_statez00_1343, BgL_valz00_1344, BgL_kontz00_1345);
					}}}}
					{	/* Unsafe/gunzip.scm 896 */
						obj_t BgL_ez00_1314;
						obj_t BgL_rz00_1315;

						{	/* Unsafe/gunzip.scm 897 */
							obj_t BgL_tmpz00_3759;

							{	/* Unsafe/gunzip.scm 897 */
								int BgL_tmpz00_5847;

								BgL_tmpz00_5847 = (int) (1L);
								BgL_tmpz00_3759 = BGL_MVALUES_VAL(BgL_tmpz00_5847);
							}
							{	/* Unsafe/gunzip.scm 897 */
								int BgL_tmpz00_5850;

								BgL_tmpz00_5850 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5850, BUNSPEC);
							}
							BgL_ez00_1314 = BgL_tmpz00_3759;
						}
						{	/* Unsafe/gunzip.scm 897 */
							obj_t BgL_tmpz00_3760;

							{	/* Unsafe/gunzip.scm 897 */
								int BgL_tmpz00_5853;

								BgL_tmpz00_5853 = (int) (2L);
								BgL_tmpz00_3760 = BGL_MVALUES_VAL(BgL_tmpz00_5853);
							}
							{	/* Unsafe/gunzip.scm 897 */
								int BgL_tmpz00_5856;

								BgL_tmpz00_5856 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_5856, BUNSPEC);
							}
							BgL_rz00_1315 = BgL_tmpz00_3760;
						}
						return
							BGl_z62laapz62zz__gunza7ipza7(0L, BgL_hz00_1310, BgL_wpz00_4442,
							BgL_inputzd2portzd2_4446, BgL_bkz00_4445, BgL_bbz00_4444,
							BgL_slidez00_4443, BgL_wsiza7eza7_4441, BgL_statez00_1313,
							BgL_ez00_1314, BgL_rz00_1315);
					}
				}
			}
		}

	}



/* set-lit~0 */
	bool_t BGl_setzd2litze70z35zz__gunza7ipza7(obj_t BgL_llz00_4771,
		obj_t BgL_iz00_4770, obj_t BgL_inputzd2portzd2_4769, long BgL_nz00_4768,
		long BgL_jz00_1432, obj_t BgL_lz00_1433)
	{
		{	/* Unsafe/gunzip.scm 806 */
			{	/* Unsafe/gunzip.scm 807 */
				bool_t BgL_test2559z00_5860;

				{	/* Unsafe/gunzip.scm 807 */
					long BgL_tmpz00_5861;

					{	/* Unsafe/gunzip.scm 807 */
						long BgL_za71za7_3652;

						BgL_za71za7_3652 =
							(long) CINT(((obj_t) ((obj_t) CELL_REF(BgL_iz00_4770))));
						BgL_tmpz00_5861 = (BgL_za71za7_3652 + BgL_jz00_1432);
					}
					BgL_test2559z00_5860 = (BgL_tmpz00_5861 > BgL_nz00_4768);
				}
				if (BgL_test2559z00_5860)
					{	/* Unsafe/gunzip.scm 810 */
						obj_t BgL_arg1458z00_1437;

						{	/* Unsafe/gunzip.scm 810 */
							obj_t BgL_list1459z00_1438;

							BgL_list1459z00_1438 = MAKE_YOUNG_PAIR(BINT(BgL_nz00_4768), BNIL);
							BgL_arg1458z00_1437 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2396z00zz__gunza7ipza7, BgL_list1459z00_1438);
						}
						{	/* Unsafe/gunzip.scm 328 */
							BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3656;

							{	/* Unsafe/gunzip.scm 328 */
								BgL_z62iozd2parsezd2errorz62_bglt BgL_new1076z00_3657;

								{	/* Unsafe/gunzip.scm 328 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_new1075z00_3658;

									BgL_new1075z00_3658 =
										((BgL_z62iozd2parsezd2errorz62_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_z62iozd2parsezd2errorz62_bgl))));
									{	/* Unsafe/gunzip.scm 328 */
										long BgL_arg1407z00_3659;

										BgL_arg1407z00_3659 =
											BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1075z00_3658),
											BgL_arg1407z00_3659);
									}
									BgL_new1076z00_3657 = BgL_new1075z00_3658;
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1076z00_3657)))->
										BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_z62exceptionz62_bglt)
											COBJECT(((BgL_z62exceptionz62_bglt)
													BgL_new1076z00_3657)))->BgL_locationz00) =
									((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_5877;

									{	/* Unsafe/gunzip.scm 328 */
										obj_t BgL_arg1405z00_3663;

										{	/* Unsafe/gunzip.scm 328 */
											obj_t BgL_arg1406z00_3664;

											{	/* Unsafe/gunzip.scm 328 */
												obj_t BgL_classz00_3665;

												BgL_classz00_3665 =
													BGl_z62iozd2parsezd2errorz62zz__objectz00;
												BgL_arg1406z00_3664 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_3665);
											}
											BgL_arg1405z00_3663 = VECTOR_REF(BgL_arg1406z00_3664, 2L);
										}
										BgL_auxz00_5877 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1405z00_3663);
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1076z00_3657)))->
											BgL_stackz00) = ((obj_t) BgL_auxz00_5877), BUNSPEC);
								}
								((((BgL_z62errorz62_bglt) COBJECT(
												((BgL_z62errorz62_bglt) BgL_new1076z00_3657)))->
										BgL_procz00) =
									((obj_t) BGl_string2376z00zz__gunza7ipza7), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1076z00_3657)))->BgL_msgz00) =
									((obj_t) BgL_arg1458z00_1437), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1076z00_3657)))->BgL_objz00) =
									((obj_t) BgL_inputzd2portzd2_4769), BUNSPEC);
								BgL_arg1404z00_3656 = BgL_new1076z00_3657;
							}
							BGl_raisez00zz__errorz00(((obj_t) BgL_arg1404z00_3656));
					}}
				else
					{	/* Unsafe/gunzip.scm 807 */
						BFALSE;
					}
			}
			{
				long BgL_jz00_1441;

				BgL_jz00_1441 = BgL_jz00_1432;
			BgL_zc3z04anonymousza31461ze3z87_1442:
				if ((BgL_jz00_1441 == 0L))
					{	/* Unsafe/gunzip.scm 813 */
						return ((bool_t) 0);
					}
				else
					{	/* Unsafe/gunzip.scm 813 */
						{	/* Unsafe/gunzip.scm 814 */
							long BgL_kz00_3669;

							BgL_kz00_3669 =
								(long) CINT(((obj_t) ((obj_t) CELL_REF(BgL_iz00_4770))));
							VECTOR_SET(BgL_llz00_4771, BgL_kz00_3669, BgL_lz00_1433);
						}
						{	/* Unsafe/gunzip.scm 815 */
							obj_t BgL_auxz00_4772;

							BgL_auxz00_4772 =
								ADDFX(((obj_t) ((obj_t) CELL_REF(BgL_iz00_4770))), BINT(1L));
							CELL_SET(BgL_iz00_4770, BgL_auxz00_4772);
						}
						{
							long BgL_jz00_5899;

							BgL_jz00_5899 = (BgL_jz00_1441 - 1L);
							BgL_jz00_1441 = BgL_jz00_5899;
							goto BgL_zc3z04anonymousza31461ze3z87_1442;
						}
					}
			}
		}

	}



/* huft-build~0 */
	obj_t BGl_huftzd2buildze70z35zz__gunza7ipza7(obj_t BgL_inputzd2portzd2_4773,
		obj_t BgL_bz00_1674, long BgL_nz00_1675, long BgL_sz00_1676,
		obj_t BgL_dz00_1677, obj_t BgL_ez00_1678, long BgL_mz00_1679,
		bool_t BgL_incompzd2okpzd2_1680)
	{
		{	/* Unsafe/gunzip.scm 387 */
			{	/* Unsafe/gunzip.scm 387 */
				long BgL_finalzd2yzd2_1682;
				obj_t BgL_tzd2resultzd2_1683;

				BgL_finalzd2yzd2_1682 = 0L;
				BgL_tzd2resultzd2_1683 = BFALSE;
				{	/* Unsafe/gunzip.scm 387 */
					obj_t BgL_cz00_1684;

					BgL_cz00_1684 = make_vector((16L + 1L), BINT(0L));
					{	/* Unsafe/gunzip.scm 388 */
						obj_t BgL_vz00_1685;

						BgL_vz00_1685 = make_vector(288L, BUNSPEC);
						{	/* Unsafe/gunzip.scm 389 */
							obj_t BgL_xz00_1686;

							BgL_xz00_1686 = make_vector((16L + 1L), BUNSPEC);
							{
								long BgL_i1082z00_1688;

								BgL_i1082z00_1688 = 0L;
							BgL_zc3z04anonymousza31600ze3z87_1689:
								if ((BgL_i1082z00_1688 < BgL_nz00_1675))
									{	/* Unsafe/gunzip.scm 394 */
										{	/* Unsafe/gunzip.scm 396 */
											obj_t BgL_za2pza2_1692;

											BgL_za2pza2_1692 =
												VECTOR_REF(BgL_bz00_1674, BgL_i1082z00_1688);
											{	/* Unsafe/gunzip.scm 397 */
												long BgL_arg1602z00_1693;

												{	/* Unsafe/gunzip.scm 397 */
													long BgL_arg1603z00_1694;

													{	/* Unsafe/gunzip.scm 397 */
														long BgL_kz00_3033;

														BgL_kz00_3033 = (long) CINT(BgL_za2pza2_1692);
														BgL_arg1603z00_1694 =
															(long) CINT(VECTOR_REF(BgL_cz00_1684,
																BgL_kz00_3033));
													}
													BgL_arg1602z00_1693 = (BgL_arg1603z00_1694 + 1L);
												}
												VECTOR_SET(BgL_cz00_1684,
													(long) CINT(BgL_za2pza2_1692),
													BINT(BgL_arg1602z00_1693));
										}}
										{
											long BgL_i1082z00_5917;

											BgL_i1082z00_5917 = (BgL_i1082z00_1688 + 1L);
											BgL_i1082z00_1688 = BgL_i1082z00_5917;
											goto BgL_zc3z04anonymousza31600ze3z87_1689;
										}
									}
								else
									{	/* Unsafe/gunzip.scm 394 */
										((bool_t) 0);
									}
							}
							if (((long) CINT(VECTOR_REF(BgL_cz00_1684, 0L)) == BgL_nz00_1675))
								{	/* Unsafe/gunzip.scm 399 */
									{	/* Unsafe/gunzip.scm 401 */
										int BgL_tmpz00_5923;

										BgL_tmpz00_5923 = (int) (3L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_5923);
									}
									{	/* Unsafe/gunzip.scm 401 */
										obj_t BgL_auxz00_5928;
										int BgL_tmpz00_5926;

										BgL_auxz00_5928 = BINT(0L);
										BgL_tmpz00_5926 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_5926, BgL_auxz00_5928);
									}
									{	/* Unsafe/gunzip.scm 401 */
										int BgL_tmpz00_5931;

										BgL_tmpz00_5931 = (int) (2L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_5931, BFALSE);
									}
									return BFALSE;
								}
							else
								{	/* Unsafe/gunzip.scm 403 */
									long BgL_jz00_1702;

									{
										long BgL_jz00_3048;

										BgL_jz00_3048 = 1L;
									BgL_loopz00_3047:
										if ((BgL_jz00_3048 > 16L))
											{	/* Unsafe/gunzip.scm 405 */
												BgL_jz00_1702 = BgL_jz00_3048;
											}
										else
											{	/* Unsafe/gunzip.scm 405 */
												if (
													((long) CINT(VECTOR_REF(BgL_cz00_1684,
																BgL_jz00_3048)) > 0L))
													{	/* Unsafe/gunzip.scm 406 */
														BgL_jz00_1702 = BgL_jz00_3048;
													}
												else
													{
														long BgL_jz00_5940;

														BgL_jz00_5940 = (BgL_jz00_3048 + 1L);
														BgL_jz00_3048 = BgL_jz00_5940;
														goto BgL_loopz00_3047;
													}
											}
									}
									{	/* Unsafe/gunzip.scm 408 */
										long BgL_iz00_1704;

										{
											long BgL_iz00_1880;

											BgL_iz00_1880 = 16L;
										BgL_zc3z04anonymousza31711ze3z87_1881:
											if ((BgL_iz00_1880 == 0L))
												{	/* Unsafe/gunzip.scm 411 */
													BgL_iz00_1704 = 0L;
												}
											else
												{	/* Unsafe/gunzip.scm 411 */
													if (
														((long) CINT(VECTOR_REF(BgL_cz00_1684,
																	BgL_iz00_1880)) > 0L))
														{	/* Unsafe/gunzip.scm 412 */
															BgL_iz00_1704 = BgL_iz00_1880;
														}
													else
														{
															long BgL_iz00_5948;

															BgL_iz00_5948 = (BgL_iz00_1880 - 1L);
															BgL_iz00_1880 = BgL_iz00_5948;
															goto BgL_zc3z04anonymousza31711ze3z87_1881;
														}
												}
										}
										{	/* Unsafe/gunzip.scm 414 */
											long BgL_lz00_1706;

											{	/* Unsafe/gunzip.scm 415 */
												long BgL_az00_1872;

												if ((BgL_mz00_1679 > BgL_jz00_1702))
													{	/* Unsafe/gunzip.scm 415 */
														BgL_az00_1872 = BgL_mz00_1679;
													}
												else
													{	/* Unsafe/gunzip.scm 415 */
														BgL_az00_1872 = BgL_jz00_1702;
													}
												if ((BgL_az00_1872 < BgL_iz00_1704))
													{	/* Unsafe/gunzip.scm 415 */
														BgL_lz00_1706 = BgL_az00_1872;
													}
												else
													{	/* Unsafe/gunzip.scm 415 */
														BgL_lz00_1706 = BgL_iz00_1704;
													}
											}
											{	/* Unsafe/gunzip.scm 416 */

												{	/* Unsafe/gunzip.scm 419 */
													obj_t BgL_y0z00_1708;

													{	/* Unsafe/gunzip.scm 419 */
														long BgL_g1085z00_1713;

														BgL_g1085z00_1713 = (1L << (int) (BgL_jz00_1702));
														{
															long BgL_yz00_1715;
															long BgL_jz00_1716;

															BgL_yz00_1715 = BgL_g1085z00_1713;
															BgL_jz00_1716 = BgL_jz00_1702;
														BgL_zc3z04anonymousza31612ze3z87_1717:
															if ((BgL_jz00_1716 >= BgL_iz00_1704))
																{	/* Unsafe/gunzip.scm 421 */
																	BgL_y0z00_1708 = BINT(BgL_yz00_1715);
																}
															else
																{	/* Unsafe/gunzip.scm 423 */
																	long BgL_y2z00_1719;

																	BgL_y2z00_1719 =
																		(BgL_yz00_1715 -
																		(long) CINT(VECTOR_REF(BgL_cz00_1684,
																				BgL_jz00_1716)));
																	if ((BgL_y2z00_1719 < 0L))
																		{	/* Unsafe/gunzip.scm 328 */
																			BgL_z62iozd2parsezd2errorz62_bglt
																				BgL_arg1404z00_3077;
																			{	/* Unsafe/gunzip.scm 328 */
																				BgL_z62iozd2parsezd2errorz62_bglt
																					BgL_new1076z00_3078;
																				{	/* Unsafe/gunzip.scm 328 */
																					BgL_z62iozd2parsezd2errorz62_bglt
																						BgL_new1075z00_3079;
																					BgL_new1075z00_3079 =
																						((BgL_z62iozd2parsezd2errorz62_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_z62iozd2parsezd2errorz62_bgl))));
																					{	/* Unsafe/gunzip.scm 328 */
																						long BgL_arg1407z00_3080;

																						BgL_arg1407z00_3080 =
																							BGL_CLASS_NUM
																							(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1075z00_3079),
																							BgL_arg1407z00_3080);
																					}
																					BgL_new1076z00_3078 =
																						BgL_new1075z00_3079;
																				}
																				((((BgL_z62exceptionz62_bglt) COBJECT(
																								((BgL_z62exceptionz62_bglt)
																									BgL_new1076z00_3078)))->
																						BgL_fnamez00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_z62exceptionz62_bglt)
																							COBJECT((
																									(BgL_z62exceptionz62_bglt)
																									BgL_new1076z00_3078)))->
																						BgL_locationz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				{
																					obj_t BgL_auxz00_5972;

																					{	/* Unsafe/gunzip.scm 328 */
																						obj_t BgL_arg1405z00_3084;

																						{	/* Unsafe/gunzip.scm 328 */
																							obj_t BgL_arg1406z00_3085;

																							{	/* Unsafe/gunzip.scm 328 */
																								obj_t BgL_classz00_3086;

																								BgL_classz00_3086 =
																									BGl_z62iozd2parsezd2errorz62zz__objectz00;
																								BgL_arg1406z00_3085 =
																									BGL_CLASS_ALL_FIELDS
																									(BgL_classz00_3086);
																							}
																							BgL_arg1405z00_3084 =
																								VECTOR_REF(BgL_arg1406z00_3085,
																								2L);
																						}
																						BgL_auxz00_5972 =
																							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																							(BgL_arg1405z00_3084);
																					}
																					((((BgL_z62exceptionz62_bglt) COBJECT(
																									((BgL_z62exceptionz62_bglt)
																										BgL_new1076z00_3078)))->
																							BgL_stackz00) =
																						((obj_t) BgL_auxz00_5972), BUNSPEC);
																				}
																				((((BgL_z62errorz62_bglt) COBJECT(
																								((BgL_z62errorz62_bglt)
																									BgL_new1076z00_3078)))->
																						BgL_procz00) =
																					((obj_t)
																						BGl_string2376z00zz__gunza7ipza7),
																					BUNSPEC);
																				((((BgL_z62errorz62_bglt)
																							COBJECT(((BgL_z62errorz62_bglt)
																									BgL_new1076z00_3078)))->
																						BgL_msgz00) =
																					((obj_t)
																						BGl_string2397z00zz__gunza7ipza7),
																					BUNSPEC);
																				((((BgL_z62errorz62_bglt)
																							COBJECT(((BgL_z62errorz62_bglt)
																									BgL_new1076z00_3078)))->
																						BgL_objz00) =
																					((obj_t) BgL_inputzd2portzd2_4773),
																					BUNSPEC);
																				BgL_arg1404z00_3077 =
																					BgL_new1076z00_3078;
																			}
																			BgL_y0z00_1708 =
																				BGl_raisez00zz__errorz00(
																				((obj_t) BgL_arg1404z00_3077));
																		}
																	else
																		{
																			long BgL_jz00_5988;
																			long BgL_yz00_5986;

																			BgL_yz00_5986 = (BgL_y2z00_1719 * 2L);
																			BgL_jz00_5988 = (BgL_jz00_1716 + 1L);
																			BgL_jz00_1716 = BgL_jz00_5988;
																			BgL_yz00_1715 = BgL_yz00_5986;
																			goto
																				BgL_zc3z04anonymousza31612ze3z87_1717;
																		}
																}
														}
													}
													BgL_finalzd2yzd2_1682 =
														(
														(long) CINT(BgL_y0z00_1708) -
														(long) CINT(VECTOR_REF(BgL_cz00_1684,
																BgL_iz00_1704)));
													if ((BgL_finalzd2yzd2_1682 < 0L))
														{	/* Unsafe/gunzip.scm 328 */
															BgL_z62iozd2parsezd2errorz62_bglt
																BgL_arg1404z00_3095;
															{	/* Unsafe/gunzip.scm 328 */
																BgL_z62iozd2parsezd2errorz62_bglt
																	BgL_new1076z00_3096;
																{	/* Unsafe/gunzip.scm 328 */
																	BgL_z62iozd2parsezd2errorz62_bglt
																		BgL_new1075z00_3097;
																	BgL_new1075z00_3097 =
																		((BgL_z62iozd2parsezd2errorz62_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_z62iozd2parsezd2errorz62_bgl))));
																	{	/* Unsafe/gunzip.scm 328 */
																		long BgL_arg1407z00_3098;

																		BgL_arg1407z00_3098 =
																			BGL_CLASS_NUM
																			(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1075z00_3097),
																			BgL_arg1407z00_3098);
																	}
																	BgL_new1076z00_3096 = BgL_new1075z00_3097;
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1076z00_3096)))->
																		BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
																((((BgL_z62exceptionz62_bglt)
																			COBJECT(((BgL_z62exceptionz62_bglt)
																					BgL_new1076z00_3096)))->
																		BgL_locationz00) =
																	((obj_t) BFALSE), BUNSPEC);
																{
																	obj_t BgL_auxz00_6004;

																	{	/* Unsafe/gunzip.scm 328 */
																		obj_t BgL_arg1405z00_3102;

																		{	/* Unsafe/gunzip.scm 328 */
																			obj_t BgL_arg1406z00_3103;

																			{	/* Unsafe/gunzip.scm 328 */
																				obj_t BgL_classz00_3104;

																				BgL_classz00_3104 =
																					BGl_z62iozd2parsezd2errorz62zz__objectz00;
																				BgL_arg1406z00_3103 =
																					BGL_CLASS_ALL_FIELDS
																					(BgL_classz00_3104);
																			}
																			BgL_arg1405z00_3102 =
																				VECTOR_REF(BgL_arg1406z00_3103, 2L);
																		}
																		BgL_auxz00_6004 =
																			BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																			(BgL_arg1405z00_3102);
																	}
																	((((BgL_z62exceptionz62_bglt) COBJECT(
																					((BgL_z62exceptionz62_bglt)
																						BgL_new1076z00_3096)))->
																			BgL_stackz00) =
																		((obj_t) BgL_auxz00_6004), BUNSPEC);
																}
																((((BgL_z62errorz62_bglt) COBJECT(
																				((BgL_z62errorz62_bglt)
																					BgL_new1076z00_3096)))->BgL_procz00) =
																	((obj_t) BGl_string2376z00zz__gunza7ipza7),
																	BUNSPEC);
																((((BgL_z62errorz62_bglt)
																			COBJECT(((BgL_z62errorz62_bglt)
																					BgL_new1076z00_3096)))->BgL_msgz00) =
																	((obj_t) BGl_string2398z00zz__gunza7ipza7),
																	BUNSPEC);
																((((BgL_z62errorz62_bglt)
																			COBJECT(((BgL_z62errorz62_bglt)
																					BgL_new1076z00_3096)))->BgL_objz00) =
																	((obj_t) BgL_inputzd2portzd2_4773), BUNSPEC);
																BgL_arg1404z00_3095 = BgL_new1076z00_3096;
															}
															BGl_raisez00zz__errorz00(
																((obj_t) BgL_arg1404z00_3095));
														}
													else
														{	/* Unsafe/gunzip.scm 431 */
															BFALSE;
														}
													{	/* Unsafe/gunzip.scm 435 */
														long BgL_arg1610z00_1711;

														{	/* Unsafe/gunzip.scm 435 */
															long BgL_arg1611z00_1712;

															BgL_arg1611z00_1712 =
																(long) CINT(VECTOR_REF(BgL_cz00_1684,
																	BgL_iz00_1704));
															BgL_arg1610z00_1711 =
																(BgL_arg1611z00_1712 + BgL_finalzd2yzd2_1682);
														}
														VECTOR_SET(BgL_cz00_1684, BgL_iz00_1704,
															BINT(BgL_arg1610z00_1711));
												}}
												VECTOR_SET(BgL_xz00_1686, 1L, BINT(0L));
												{	/* Unsafe/gunzip.scm 439 */
													long BgL_jz00_1725;

													{	/* Unsafe/gunzip.scm 439 */
														long BgL_g1086z00_1857;

														BgL_g1086z00_1857 = (BgL_iz00_1704 - 1L);
														{
															long BgL_iz00_3153;
															long BgL_xzd2poszd2_3154;
															long BgL_czd2poszd2_3155;
															long BgL_jz00_3156;

															BgL_iz00_3153 = BgL_g1086z00_1857;
															BgL_xzd2poszd2_3154 = 2L;
															BgL_czd2poszd2_3155 = 1L;
															BgL_jz00_3156 = 0L;
														BgL_loopz00_3152:
															if ((BgL_iz00_3153 == 0L))
																{	/* Unsafe/gunzip.scm 443 */
																	BgL_jz00_1725 = BgL_jz00_3156;
																}
															else
																{	/* Unsafe/gunzip.scm 445 */
																	long BgL_vz00_3165;

																	BgL_vz00_3165 =
																		(long) CINT(VECTOR_REF(BgL_cz00_1684,
																			BgL_czd2poszd2_3155));
																	{	/* Unsafe/gunzip.scm 446 */
																		long BgL_arg1704z00_3166;

																		BgL_arg1704z00_3166 =
																			(BgL_jz00_3156 + BgL_vz00_3165);
																		VECTOR_SET(BgL_xz00_1686,
																			BgL_xzd2poszd2_3154,
																			BINT(BgL_arg1704z00_3166));
																	}
																	{
																		long BgL_jz00_6039;
																		long BgL_czd2poszd2_6037;
																		long BgL_xzd2poszd2_6035;
																		long BgL_iz00_6033;

																		BgL_iz00_6033 = (BgL_iz00_3153 - 1L);
																		BgL_xzd2poszd2_6035 =
																			(BgL_xzd2poszd2_3154 + 1L);
																		BgL_czd2poszd2_6037 =
																			(BgL_czd2poszd2_3155 + 1L);
																		BgL_jz00_6039 =
																			(BgL_jz00_3156 + BgL_vz00_3165);
																		BgL_jz00_3156 = BgL_jz00_6039;
																		BgL_czd2poszd2_3155 = BgL_czd2poszd2_6037;
																		BgL_xzd2poszd2_3154 = BgL_xzd2poszd2_6035;
																		BgL_iz00_3153 = BgL_iz00_6033;
																		goto BgL_loopz00_3152;
																	}
																}
														}
													}
													{
														long BgL_iz00_3198;
														long BgL_bzd2poszd2_3199;

														BgL_iz00_3198 = 0L;
														BgL_bzd2poszd2_3199 = 0L;
													BgL_loopz00_3197:
														{	/* Unsafe/gunzip.scm 455 */
															obj_t BgL_jz00_3200;

															BgL_jz00_3200 =
																VECTOR_REF(BgL_bz00_1674, BgL_bzd2poszd2_3199);
															if (((long) CINT(BgL_jz00_3200) == 0L))
																{	/* Unsafe/gunzip.scm 456 */
																	BFALSE;
																}
															else
																{	/* Unsafe/gunzip.scm 457 */
																	obj_t BgL_xjz00_3205;

																	BgL_xjz00_3205 =
																		VECTOR_REF(BgL_xz00_1686,
																		(long) CINT(BgL_jz00_3200));
																	{	/* Unsafe/gunzip.scm 458 */
																		long BgL_arg1620z00_3208;

																		BgL_arg1620z00_3208 =
																			(1L + (long) CINT(BgL_xjz00_3205));
																		VECTOR_SET(BgL_xz00_1686,
																			(long) CINT(BgL_jz00_3200),
																			BINT(BgL_arg1620z00_3208));
																	}
																	VECTOR_SET(BgL_vz00_1685,
																		(long) CINT(BgL_xjz00_3205),
																		BINT(BgL_iz00_3198));
																}
															{	/* Unsafe/gunzip.scm 460 */
																long BgL_i2z00_3214;

																BgL_i2z00_3214 = (1L + BgL_iz00_3198);
																if ((BgL_i2z00_3214 < BgL_nz00_1675))
																	{
																		long BgL_bzd2poszd2_6059;
																		long BgL_iz00_6058;

																		BgL_iz00_6058 = BgL_i2z00_3214;
																		BgL_bzd2poszd2_6059 =
																			(1L + BgL_bzd2poszd2_3199);
																		BgL_bzd2poszd2_3199 = BgL_bzd2poszd2_6059;
																		BgL_iz00_3198 = BgL_iz00_6058;
																		goto BgL_loopz00_3197;
																	}
																else
																	{	/* Unsafe/gunzip.scm 461 */
																		((bool_t) 0);
																	}
															}
														}
													}
													VECTOR_SET(BgL_xz00_1686, 0L, BINT(0L));
													{	/* Unsafe/gunzip.scm 467 */
														long BgL_vzd2poszd2_1738;
														long BgL_iz00_1739;
														long BgL_hz00_1740;
														long BgL_wz00_1741;
														obj_t BgL_uz00_1742;
														obj_t BgL_qz00_1743;
														long BgL_za7za7_1744;
														BgL_huftz00_bglt BgL_rz00_1745;

														BgL_vzd2poszd2_1738 = 0L;
														BgL_iz00_1739 = 0L;
														BgL_hz00_1740 = -1L;
														BgL_wz00_1741 = NEG(BgL_lz00_1706);
														BgL_uz00_1742 = make_vector(16L, BUNSPEC);
														BgL_qz00_1743 = BUNSPEC;
														BgL_za7za7_1744 = 0L;
														{	/* Unsafe/gunzip.scm 474 */
															BgL_huftz00_bglt BgL_new1088z00_1854;

															{	/* Unsafe/gunzip.scm 474 */
																BgL_huftz00_bglt BgL_new1087z00_1855;

																BgL_new1087z00_1855 =
																	((BgL_huftz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_huftz00_bgl))));
																{	/* Unsafe/gunzip.scm 474 */
																	long BgL_arg1701z00_1856;

																	BgL_arg1701z00_1856 =
																		BGL_CLASS_NUM(BGl_huftz00zz__gunza7ipza7);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1087z00_1855),
																		BgL_arg1701z00_1856);
																}
																BgL_new1088z00_1854 = BgL_new1087z00_1855;
															}
															((((BgL_huftz00_bglt)
																		COBJECT(BgL_new1088z00_1854))->BgL_ez00) =
																((long) 0L), BUNSPEC);
															((((BgL_huftz00_bglt)
																		COBJECT(BgL_new1088z00_1854))->BgL_bz00) =
																((long) 0L), BUNSPEC);
															((((BgL_huftz00_bglt)
																		COBJECT(BgL_new1088z00_1854))->BgL_vz00) =
																((obj_t) BINT(0L)), BUNSPEC);
															BgL_rz00_1745 = BgL_new1088z00_1854;
														}
														{
															long BgL_kz00_1747;

															BgL_kz00_1747 = BgL_jz00_1702;
														BgL_zc3z04anonymousza31623ze3z87_1748:
															if ((BgL_kz00_1747 <= BgL_iz00_1704))
																{	/* Unsafe/gunzip.scm 480 */
																	long BgL_az00_1750;

																	BgL_az00_1750 =
																		(long) CINT(VECTOR_REF(BgL_cz00_1684,
																			BgL_kz00_1747));
																	{	/* Unsafe/gunzip.scm 481 */
																		long BgL_g1089z00_1751;

																		BgL_g1089z00_1751 = (BgL_az00_1750 - 1L);
																		{
																			long BgL_az00_1753;

																			BgL_az00_1753 = BgL_g1089z00_1751;
																		BgL_zc3z04anonymousza31625ze3z87_1754:
																			if ((BgL_az00_1753 < 0L))
																				{	/* Unsafe/gunzip.scm 482 */
																					((bool_t) 0);
																				}
																			else
																				{	/* Unsafe/gunzip.scm 482 */
																					{

																					BgL_zc3z04anonymousza31627ze3z87_1757:
																						if (
																							(BgL_kz00_1747 >
																								(BgL_wz00_1741 +
																									BgL_lz00_1706)))
																							{	/* Unsafe/gunzip.scm 487 */
																								BgL_hz00_1740 =
																									(BgL_hz00_1740 + 1L);
																								BgL_wz00_1741 =
																									(BgL_wz00_1741 +
																									BgL_lz00_1706);
																								{	/* Unsafe/gunzip.scm 493 */
																									long BgL_az00_1760;

																									BgL_az00_1760 =
																										(BgL_iz00_1704 -
																										BgL_wz00_1741);
																									if ((BgL_az00_1760 <
																											BgL_lz00_1706))
																										{	/* Unsafe/gunzip.scm 493 */
																											BgL_za7za7_1744 =
																												BgL_az00_1760;
																										}
																									else
																										{	/* Unsafe/gunzip.scm 493 */
																											BgL_za7za7_1744 =
																												BgL_lz00_1706;
																										}
																								}
																								{	/* Unsafe/gunzip.scm 494 */
																									long BgL_jz00_1763;

																									BgL_jz00_1763 =
																										(BgL_kz00_1747 -
																										BgL_wz00_1741);
																									{	/* Unsafe/gunzip.scm 494 */
																										long BgL_fz00_1764;

																										{	/* Unsafe/gunzip.scm 495 */
																											long BgL_yz00_3246;

																											BgL_yz00_3246 =
																												BgL_jz00_1763;
																											BgL_fz00_1764 =
																												(1L <<
																												(int) (BgL_yz00_3246));
																										}
																										{	/* Unsafe/gunzip.scm 495 */

																											if (
																												(BgL_fz00_1764 >
																													(BgL_az00_1753 + 1L)))
																												{	/* Unsafe/gunzip.scm 496 */
																													BgL_fz00_1764 =
																														(BgL_fz00_1764 -
																														(BgL_az00_1753 +
																															1L));
																													{
																														long
																															BgL_czd2poszd2_1769;
																														BgL_czd2poszd2_1769
																															= BgL_kz00_1747;
																													BgL_zc3z04anonymousza31635ze3z87_1770:
																														BgL_jz00_1763 =
																															(BgL_jz00_1763 +
																															1L);
																														if ((BgL_jz00_1763 <
																																BgL_za7za7_1744))
																															{	/* Unsafe/gunzip.scm 500 */
																																BgL_fz00_1764 =
																																	(BgL_fz00_1764
																																	* 2L);
																																{	/* Unsafe/gunzip.scm 502 */
																																	long
																																		BgL_czd2poszd2_1772;
																																	BgL_czd2poszd2_1772
																																		=
																																		(BgL_czd2poszd2_1769
																																		+ 1L);
																																	{	/* Unsafe/gunzip.scm 502 */
																																		long
																																			BgL_cvz00_1773;
																																		BgL_cvz00_1773
																																			=
																																			(long)
																																			CINT
																																			(VECTOR_REF
																																			(BgL_cz00_1684,
																																				BgL_czd2poszd2_1772));
																																		{	/* Unsafe/gunzip.scm 503 */

																																			if (
																																				(BgL_fz00_1764
																																					<=
																																					BgL_cvz00_1773))
																																				{	/* Unsafe/gunzip.scm 504 */
																																					((bool_t) 0);
																																				}
																																			else
																																				{	/* Unsafe/gunzip.scm 504 */
																																					BgL_fz00_1764
																																						=
																																						(BgL_fz00_1764
																																						-
																																						BgL_cvz00_1773);
																																					{
																																						long
																																							BgL_czd2poszd2_6106;
																																						BgL_czd2poszd2_6106
																																							=
																																							BgL_czd2poszd2_1772;
																																						BgL_czd2poszd2_1769
																																							=
																																							BgL_czd2poszd2_6106;
																																						goto
																																							BgL_zc3z04anonymousza31635ze3z87_1770;
																																					}
																																				}
																																		}
																																	}
																																}
																															}
																														else
																															{	/* Unsafe/gunzip.scm 500 */
																																((bool_t) 0);
																															}
																													}
																												}
																											else
																												{	/* Unsafe/gunzip.scm 496 */
																													((bool_t) 0);
																												}
																											{	/* Unsafe/gunzip.scm 507 */
																												long BgL_yz00_3264;

																												BgL_yz00_3264 =
																													BgL_jz00_1763;
																												BgL_za7za7_1744 =
																													(1L <<
																													(int)
																													(BgL_yz00_3264));
																											}
																											{	/* Unsafe/gunzip.scm 512 */
																												obj_t
																													BgL_zc3z04anonymousza31640ze3z87_4429;
																												{
																													int BgL_tmpz00_6109;

																													BgL_tmpz00_6109 =
																														(int) (0L);
																													BgL_zc3z04anonymousza31640ze3z87_4429
																														=
																														MAKE_EL_PROCEDURE
																														(BgL_tmpz00_6109);
																												}
																												BgL_qz00_1743 =
																													BGl_buildzd2vectorzd2zz__gunza7ipza7
																													(BgL_za7za7_1744,
																													BgL_zc3z04anonymousza31640ze3z87_4429);
																											}
																											if (CBOOL
																												(BgL_tzd2resultzd2_1683))
																												{	/* Unsafe/gunzip.scm 517 */
																													BFALSE;
																												}
																											else
																												{	/* Unsafe/gunzip.scm 517 */
																													BgL_tzd2resultzd2_1683
																														= BgL_qz00_1743;
																												}
																											VECTOR_SET(BgL_uz00_1742,
																												BgL_hz00_1740,
																												BgL_qz00_1743);
																											if ((BgL_hz00_1740 == 0L))
																												{	/* Unsafe/gunzip.scm 524 */
																													BFALSE;
																												}
																											else
																												{	/* Unsafe/gunzip.scm 524 */
																													VECTOR_SET
																														(BgL_xz00_1686,
																														BgL_hz00_1740,
																														BINT
																														(BgL_iz00_1739));
																													((((BgL_huftz00_bglt)
																																COBJECT
																																(BgL_rz00_1745))->
																															BgL_bz00) =
																														((long)
																															BgL_lz00_1706),
																														BUNSPEC);
																													{
																														long
																															BgL_auxz00_6121;
																														{	/* Unsafe/gunzip.scm 527 */
																															long
																																BgL_za71za7_3275;
																															BgL_za71za7_3275 =
																																BgL_jz00_1763;
																															BgL_auxz00_6121 =
																																(BgL_za71za7_3275
																																+ 16L);
																														}
																														((((BgL_huftz00_bglt) COBJECT(BgL_rz00_1745))->BgL_ez00) = ((long) BgL_auxz00_6121), BUNSPEC);
																													}
																													((((BgL_huftz00_bglt)
																																COBJECT
																																(BgL_rz00_1745))->
																															BgL_vz00) =
																														((obj_t)
																															BgL_qz00_1743),
																														BUNSPEC);
																													{	/* Unsafe/gunzip.scm 529 */
																														long BgL_xz00_3278;

																														BgL_xz00_3278 =
																															BgL_iz00_1739;
																														{	/* Unsafe/gunzip.scm 529 */
																															int
																																BgL_tmpz00_6125;
																															{	/* Unsafe/gunzip.scm 529 */
																																long
																																	BgL_za71za7_3276;
																																BgL_za71za7_3276
																																	=
																																	BgL_wz00_1741;
																																BgL_tmpz00_6125
																																	=
																																	(int) (
																																	(BgL_za71za7_3276
																																		-
																																		BgL_lz00_1706));
																															}
																															BgL_jz00_1763 =
																																(BgL_xz00_3278
																																>>
																																BgL_tmpz00_6125);
																													}}
																													{	/* Unsafe/gunzip.scm 533 */
																														obj_t
																															BgL_arg1644z00_1789;
																														{	/* Unsafe/gunzip.scm 533 */
																															obj_t
																																BgL_arg1645z00_1790;
																															BgL_arg1645z00_1790
																																=
																																VECTOR_REF
																																(BgL_uz00_1742,
																																(BgL_hz00_1740 -
																																	1L));
																															{	/* Unsafe/gunzip.scm 532 */
																																long
																																	BgL_kz00_3284;
																																BgL_kz00_3284 =
																																	BgL_jz00_1763;
																																BgL_arg1644z00_1789
																																	=
																																	VECTOR_REF((
																																		(obj_t)
																																		BgL_arg1645z00_1790),
																																	BgL_kz00_3284);
																														}}
																														((((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt) BgL_arg1644z00_1789)))->BgL_ez00) = ((long) (((BgL_huftz00_bglt) COBJECT(BgL_rz00_1745))->BgL_ez00)), BUNSPEC);
																														((((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt) BgL_arg1644z00_1789)))->BgL_bz00) = ((long) (((BgL_huftz00_bglt) COBJECT(BgL_rz00_1745))->BgL_bz00)), BUNSPEC);
																														((((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt) BgL_arg1644z00_1789)))->BgL_vz00) = ((obj_t) (((BgL_huftz00_bglt) COBJECT(BgL_rz00_1745))->BgL_vz00)), BUNSPEC);
																								}}}}}
																								goto
																									BgL_zc3z04anonymousza31627ze3z87_1757;
																							}
																						else
																							{	/* Unsafe/gunzip.scm 487 */
																								((bool_t) 0);
																							}
																					}
																					{
																						long BgL_auxz00_6142;

																						{	/* Unsafe/gunzip.scm 538 */
																							long BgL_za72za7_3288;

																							BgL_za72za7_3288 = BgL_wz00_1741;
																							BgL_auxz00_6142 =
																								(BgL_kz00_1747 -
																								BgL_za72za7_3288);
																						}
																						((((BgL_huftz00_bglt)
																									COBJECT(BgL_rz00_1745))->
																								BgL_bz00) =
																							((long) BgL_auxz00_6142),
																							BUNSPEC);
																					}
																					if (
																						(BgL_vzd2poszd2_1738 >=
																							BgL_nz00_1675))
																						{	/* Unsafe/gunzip.scm 540 */
																							((((BgL_huftz00_bglt)
																										COBJECT(BgL_rz00_1745))->
																									BgL_ez00) =
																								((long) 99L), BUNSPEC);
																						}
																					else
																						{	/* Unsafe/gunzip.scm 542 */
																							obj_t BgL_vvz00_1797;

																							BgL_vvz00_1797 =
																								VECTOR_REF(BgL_vz00_1685,
																								BgL_vzd2poszd2_1738);
																							if (((long) CINT(BgL_vvz00_1797) <
																									BgL_sz00_1676))
																								{	/* Unsafe/gunzip.scm 543 */
																									{
																										long BgL_auxz00_6152;

																										if (
																											((long)
																												CINT(BgL_vvz00_1797) <
																												256L))
																											{	/* Unsafe/gunzip.scm 546 */
																												BgL_auxz00_6152 = 16L;
																											}
																										else
																											{	/* Unsafe/gunzip.scm 546 */
																												BgL_auxz00_6152 = 15L;
																											}
																										((((BgL_huftz00_bglt)
																													COBJECT
																													(BgL_rz00_1745))->
																												BgL_ez00) =
																											((long) BgL_auxz00_6152),
																											BUNSPEC);
																									}
																									((((BgL_huftz00_bglt)
																												COBJECT
																												(BgL_rz00_1745))->
																											BgL_vz00) =
																										((obj_t) BgL_vvz00_1797),
																										BUNSPEC);
																								}
																							else
																								{	/* Unsafe/gunzip.scm 543 */
																									{
																										long BgL_auxz00_6158;

																										{	/* Unsafe/gunzip.scm 550 */
																											long BgL_arg1652z00_1803;

																											BgL_arg1652z00_1803 =
																												(
																												(long)
																												CINT(BgL_vvz00_1797) -
																												BgL_sz00_1676);
																											BgL_auxz00_6158 =
																												(long)
																												CINT(VECTOR_REF
																												(BgL_ez00_1678,
																													BgL_arg1652z00_1803));
																										}
																										((((BgL_huftz00_bglt)
																													COBJECT
																													(BgL_rz00_1745))->
																												BgL_ez00) =
																											((long) BgL_auxz00_6158),
																											BUNSPEC);
																									}
																									{
																										obj_t BgL_auxz00_6164;

																										{	/* Unsafe/gunzip.scm 552 */
																											long BgL_arg1653z00_1805;

																											BgL_arg1653z00_1805 =
																												(
																												(long)
																												CINT(BgL_vvz00_1797) -
																												BgL_sz00_1676);
																											BgL_auxz00_6164 =
																												VECTOR_REF
																												(BgL_dz00_1677,
																												BgL_arg1653z00_1805);
																										}
																										((((BgL_huftz00_bglt)
																													COBJECT
																													(BgL_rz00_1745))->
																												BgL_vz00) =
																											((obj_t) BgL_auxz00_6164),
																											BUNSPEC);
																								}}
																							BgL_vzd2poszd2_1738 =
																								(BgL_vzd2poszd2_1738 + 1L);
																						}
																					{	/* Unsafe/gunzip.scm 555 */
																						long BgL_fz00_1806;

																						{	/* Unsafe/gunzip.scm 555 */
																							int BgL_tmpz00_6170;

																							{	/* Unsafe/gunzip.scm 555 */
																								long BgL_za72za7_3306;

																								BgL_za72za7_3306 =
																									BgL_wz00_1741;
																								BgL_tmpz00_6170 =
																									(int) ((BgL_kz00_1747 -
																										BgL_za72za7_3306));
																							}
																							BgL_fz00_1806 =
																								(1L << BgL_tmpz00_6170);
																						}
																						{	/* Unsafe/gunzip.scm 556 */
																							long BgL_g1110z00_1807;

																							{	/* Unsafe/gunzip.scm 556 */
																								long BgL_xz00_3308;
																								long BgL_yz00_3309;

																								BgL_xz00_3308 = BgL_iz00_1739;
																								BgL_yz00_3309 = BgL_wz00_1741;
																								BgL_g1110z00_1807 =
																									(BgL_xz00_3308 >>
																									(int) (BgL_yz00_3309));
																							}
																							{
																								long BgL_jz00_1809;

																								BgL_jz00_1809 =
																									BgL_g1110z00_1807;
																							BgL_zc3z04anonymousza31654ze3z87_1810:
																								if (
																									(BgL_jz00_1809 <
																										BgL_za7za7_1744))
																									{	/* Unsafe/gunzip.scm 557 */
																										{	/* Unsafe/gunzip.scm 558 */
																											obj_t BgL_arg1656z00_1812;

																											BgL_arg1656z00_1812 =
																												VECTOR_REF
																												(BgL_qz00_1743,
																												BgL_jz00_1809);
																											((((BgL_huftz00_bglt)
																														COBJECT((
																																(BgL_huftz00_bglt)
																																BgL_arg1656z00_1812)))->
																													BgL_ez00) =
																												((long) ((
																															(BgL_huftz00_bglt)
																															COBJECT
																															(BgL_rz00_1745))->
																														BgL_ez00)),
																												BUNSPEC);
																											((((BgL_huftz00_bglt)
																														COBJECT((
																																(BgL_huftz00_bglt)
																																BgL_arg1656z00_1812)))->
																													BgL_bz00) =
																												((long) ((
																															(BgL_huftz00_bglt)
																															COBJECT
																															(BgL_rz00_1745))->
																														BgL_bz00)),
																												BUNSPEC);
																											((((BgL_huftz00_bglt)
																														COBJECT((
																																(BgL_huftz00_bglt)
																																BgL_arg1656z00_1812)))->
																													BgL_vz00) =
																												((obj_t) ((
																															(BgL_huftz00_bglt)
																															COBJECT
																															(BgL_rz00_1745))->
																														BgL_vz00)),
																												BUNSPEC);
																										}
																										{
																											long BgL_jz00_6188;

																											BgL_jz00_6188 =
																												(BgL_jz00_1809 +
																												BgL_fz00_1806);
																											BgL_jz00_1809 =
																												BgL_jz00_6188;
																											goto
																												BgL_zc3z04anonymousza31654ze3z87_1810;
																										}
																									}
																								else
																									{	/* Unsafe/gunzip.scm 557 */
																										((bool_t) 0);
																									}
																							}
																						}
																					}
																					{	/* Unsafe/gunzip.scm 561 */
																						long BgL_g1111z00_1816;

																						BgL_g1111z00_1816 =
																							(1L <<
																							(int) ((BgL_kz00_1747 - 1L)));
																						{
																							long BgL_jz00_1818;

																							BgL_jz00_1818 = BgL_g1111z00_1816;
																						BgL_zc3z04anonymousza31659ze3z87_1819:
																							{	/* Unsafe/gunzip.scm 562 */
																								bool_t BgL_test2588z00_6193;

																								{	/* Unsafe/gunzip.scm 562 */
																									obj_t BgL_auxz00_6194;

																									{	/* Unsafe/gunzip.scm 562 */
																										long BgL_xz00_3320;

																										BgL_xz00_3320 =
																											BgL_iz00_1739;
																										BgL_auxz00_6194 =
																											BINT((BgL_xz00_3320 &
																												BgL_jz00_1818));
																									}
																									BgL_test2588z00_6193 =
																										BGl_positivezf3zf3zz__r4_numbers_6_5z00
																										(BgL_auxz00_6194);
																								}
																								if (BgL_test2588z00_6193)
																									{	/* Unsafe/gunzip.scm 562 */
																										BgL_iz00_1739 =
																											(BgL_iz00_1739 ^
																											BgL_jz00_1818);
																										{
																											long BgL_jz00_6199;

																											BgL_jz00_6199 =
																												(BgL_jz00_1818 >>
																												(int) (1L));
																											BgL_jz00_1818 =
																												BgL_jz00_6199;
																											goto
																												BgL_zc3z04anonymousza31659ze3z87_1819;
																										}
																									}
																								else
																									{	/* Unsafe/gunzip.scm 562 */
																										BgL_iz00_1739 =
																											(BgL_iz00_1739 ^
																											BgL_jz00_1818);
																									}
																							}
																						}
																					}
																					{

																					BgL_zc3z04anonymousza31668ze3z87_1827:
																						{	/* Unsafe/gunzip.scm 569 */
																							bool_t BgL_test2589z00_6203;

																							{	/* Unsafe/gunzip.scm 569 */
																								obj_t BgL_arg1678z00_1833;
																								long BgL_arg1681z00_1834;

																								BgL_arg1678z00_1833 =
																									VECTOR_REF(BgL_xz00_1686,
																									BgL_hz00_1740);
																								{	/* Unsafe/gunzip.scm 571 */
																									long BgL_arg1684z00_1835;

																									{	/* Unsafe/gunzip.scm 571 */
																										long BgL_tmpz00_6205;

																										{	/* Unsafe/gunzip.scm 571 */
																											long BgL_yz00_3329;

																											BgL_yz00_3329 =
																												BgL_wz00_1741;
																											BgL_tmpz00_6205 =
																												(1L <<
																												(int) (BgL_yz00_3329));
																										}
																										BgL_arg1684z00_1835 =
																											(BgL_tmpz00_6205 - 1L);
																									}
																									BgL_arg1681z00_1834 =
																										(BgL_iz00_1739 &
																										BgL_arg1684z00_1835);
																								}
																								BgL_test2589z00_6203 =
																									(
																									(long)
																									CINT(BgL_arg1678z00_1833) ==
																									BgL_arg1681z00_1834);
																							}
																							if (BgL_test2589z00_6203)
																								{	/* Unsafe/gunzip.scm 569 */
																									((bool_t) 0);
																								}
																							else
																								{	/* Unsafe/gunzip.scm 569 */
																									BgL_hz00_1740 =
																										(BgL_hz00_1740 - 1L);
																									BgL_wz00_1741 =
																										(BgL_wz00_1741 -
																										BgL_lz00_1706);
																									goto
																										BgL_zc3z04anonymousza31668ze3z87_1827;
																								}
																						}
																					}
																					{
																						long BgL_az00_6214;

																						BgL_az00_6214 =
																							(BgL_az00_1753 - 1L);
																						BgL_az00_1753 = BgL_az00_6214;
																						goto
																							BgL_zc3z04anonymousza31625ze3z87_1754;
																					}
																				}
																		}
																	}
																	{
																		long BgL_kz00_6216;

																		BgL_kz00_6216 = (BgL_kz00_1747 + 1L);
																		BgL_kz00_1747 = BgL_kz00_6216;
																		goto BgL_zc3z04anonymousza31623ze3z87_1748;
																	}
																}
															else
																{	/* Unsafe/gunzip.scm 479 */
																	((bool_t) 0);
																}
														}
														{	/* Unsafe/gunzip.scm 581 */
															bool_t BgL_okpz00_1842;

															if (BgL_incompzd2okpzd2_1680)
																{	/* Unsafe/gunzip.scm 581 */
																	BgL_okpz00_1842 = BgL_incompzd2okpzd2_1680;
																}
															else
																{	/* Unsafe/gunzip.scm 582 */
																	bool_t BgL_test2591z00_6219;

																	if ((0L == BgL_finalzd2yzd2_1682))
																		{	/* Unsafe/gunzip.scm 582 */
																			BgL_test2591z00_6219 = ((bool_t) 0);
																		}
																	else
																		{	/* Unsafe/gunzip.scm 582 */
																			if ((BgL_iz00_1704 == 1L))
																				{	/* Unsafe/gunzip.scm 583 */
																					BgL_test2591z00_6219 = ((bool_t) 0);
																				}
																			else
																				{	/* Unsafe/gunzip.scm 583 */
																					BgL_test2591z00_6219 = ((bool_t) 1);
																				}
																		}
																	if (BgL_test2591z00_6219)
																		{	/* Unsafe/gunzip.scm 582 */
																			BgL_okpz00_1842 = ((bool_t) 0);
																		}
																	else
																		{	/* Unsafe/gunzip.scm 582 */
																			BgL_okpz00_1842 = ((bool_t) 1);
																		}
																}
															if (BgL_okpz00_1842)
																{	/* Unsafe/gunzip.scm 584 */
																	BFALSE;
																}
															else
																{	/* Unsafe/gunzip.scm 328 */
																	BgL_z62iozd2parsezd2errorz62_bglt
																		BgL_arg1404z00_3342;
																	{	/* Unsafe/gunzip.scm 328 */
																		BgL_z62iozd2parsezd2errorz62_bglt
																			BgL_new1076z00_3343;
																		{	/* Unsafe/gunzip.scm 328 */
																			BgL_z62iozd2parsezd2errorz62_bglt
																				BgL_new1075z00_3344;
																			BgL_new1075z00_3344 =
																				((BgL_z62iozd2parsezd2errorz62_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_z62iozd2parsezd2errorz62_bgl))));
																			{	/* Unsafe/gunzip.scm 328 */
																				long BgL_arg1407z00_3345;

																				BgL_arg1407z00_3345 =
																					BGL_CLASS_NUM
																					(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1075z00_3344),
																					BgL_arg1407z00_3345);
																			}
																			BgL_new1076z00_3343 = BgL_new1075z00_3344;
																		}
																		((((BgL_z62exceptionz62_bglt) COBJECT(
																						((BgL_z62exceptionz62_bglt)
																							BgL_new1076z00_3343)))->
																				BgL_fnamez00) =
																			((obj_t) BFALSE), BUNSPEC);
																		((((BgL_z62exceptionz62_bglt)
																					COBJECT(((BgL_z62exceptionz62_bglt)
																							BgL_new1076z00_3343)))->
																				BgL_locationz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		{
																			obj_t BgL_auxz00_6233;

																			{	/* Unsafe/gunzip.scm 328 */
																				obj_t BgL_arg1405z00_3349;

																				{	/* Unsafe/gunzip.scm 328 */
																					obj_t BgL_arg1406z00_3350;

																					{	/* Unsafe/gunzip.scm 328 */
																						obj_t BgL_classz00_3351;

																						BgL_classz00_3351 =
																							BGl_z62iozd2parsezd2errorz62zz__objectz00;
																						BgL_arg1406z00_3350 =
																							BGL_CLASS_ALL_FIELDS
																							(BgL_classz00_3351);
																					}
																					BgL_arg1405z00_3349 =
																						VECTOR_REF(BgL_arg1406z00_3350, 2L);
																				}
																				BgL_auxz00_6233 =
																					BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																					(BgL_arg1405z00_3349);
																			}
																			((((BgL_z62exceptionz62_bglt) COBJECT(
																							((BgL_z62exceptionz62_bglt)
																								BgL_new1076z00_3343)))->
																					BgL_stackz00) =
																				((obj_t) BgL_auxz00_6233), BUNSPEC);
																		}
																		((((BgL_z62errorz62_bglt) COBJECT(
																						((BgL_z62errorz62_bglt)
																							BgL_new1076z00_3343)))->
																				BgL_procz00) =
																			((obj_t)
																				BGl_string2388z00zz__gunza7ipza7),
																			BUNSPEC);
																		((((BgL_z62errorz62_bglt)
																					COBJECT(((BgL_z62errorz62_bglt)
																							BgL_new1076z00_3343)))->
																				BgL_msgz00) =
																			((obj_t)
																				BGl_string2399z00zz__gunza7ipza7),
																			BUNSPEC);
																		((((BgL_z62errorz62_bglt)
																					COBJECT(((BgL_z62errorz62_bglt)
																							BgL_new1076z00_3343)))->
																				BgL_objz00) =
																			((obj_t) BgL_inputzd2portzd2_4773),
																			BUNSPEC);
																		BgL_arg1404z00_3342 = BgL_new1076z00_3343;
																	}
																	BGl_raisez00zz__errorz00(
																		((obj_t) BgL_arg1404z00_3342));
																}
															{	/* Unsafe/gunzip.scm 588 */
																obj_t BgL_val0_1220z00_1843;

																BgL_val0_1220z00_1843 = BgL_tzd2resultzd2_1683;
																{	/* Unsafe/gunzip.scm 588 */
																	int BgL_tmpz00_6247;

																	BgL_tmpz00_6247 = (int) (3L);
																	BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6247);
																}
																{	/* Unsafe/gunzip.scm 588 */
																	obj_t BgL_auxz00_6252;
																	int BgL_tmpz00_6250;

																	BgL_auxz00_6252 = BINT(BgL_lz00_1706);
																	BgL_tmpz00_6250 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_6250,
																		BgL_auxz00_6252);
																}
																{	/* Unsafe/gunzip.scm 588 */
																	obj_t BgL_auxz00_6257;
																	int BgL_tmpz00_6255;

																	BgL_auxz00_6257 = BBOOL(BgL_okpz00_1842);
																	BgL_tmpz00_6255 = (int) (2L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_6255,
																		BgL_auxz00_6257);
																}
																return BgL_val0_1220z00_1843;
															}
														}
													}
												}
											}
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* &loop2313 */
	obj_t BGl_z62loop2313z62zz__gunza7ipza7(long BgL_ez00_4481,
		obj_t BgL_statez00_1347, obj_t BgL_valz00_1348, obj_t BgL_kontz00_1349)
	{
		{	/* Unsafe/gunzip.scm 871 */
			if ((BgL_statez00_1347 == BGl_symbol2368z00zz__gunza7ipza7))
				{	/* Unsafe/gunzip.scm 876 */
					obj_t BgL_val0_1241z00_1353;

					BgL_val0_1241z00_1353 = BGl_symbol2368z00zz__gunza7ipza7;
					{	/* Unsafe/gunzip.scm 876 */
						int BgL_tmpz00_6262;

						BgL_tmpz00_6262 = (int) (3L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6262);
					}
					{	/* Unsafe/gunzip.scm 876 */
						obj_t BgL_auxz00_6267;
						int BgL_tmpz00_6265;

						BgL_auxz00_6267 = BINT(BgL_ez00_4481);
						BgL_tmpz00_6265 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_6265, BgL_auxz00_6267);
					}
					{	/* Unsafe/gunzip.scm 876 */
						int BgL_tmpz00_6270;

						BgL_tmpz00_6270 = (int) (2L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_6270, BgL_valz00_1348);
					}
					return BgL_val0_1241z00_1353;
				}
			else
				{	/* Unsafe/gunzip.scm 874 */
					if ((BgL_statez00_1347 == BGl_symbol2372z00zz__gunza7ipza7))
						{	/* Unsafe/gunzip.scm 878 */
							obj_t BgL_val0_1244z00_1357;

							BgL_val0_1244z00_1357 = BGl_symbol2372z00zz__gunza7ipza7;
							{	/* Unsafe/gunzip.scm 881 */
								obj_t BgL_zc3z04anonymousza31420ze3z87_4427;

								BgL_zc3z04anonymousza31420ze3z87_4427 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31420ze3ze5zz__gunza7ipza7,
									(int) (0L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31420ze3z87_4427, (int) (0L),
									BINT(BgL_ez00_4481));
								PROCEDURE_SET(BgL_zc3z04anonymousza31420ze3z87_4427, (int) (1L),
									BgL_kontz00_1349);
								{	/* Unsafe/gunzip.scm 878 */
									int BgL_tmpz00_6283;

									BgL_tmpz00_6283 = (int) (3L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6283);
								}
								{	/* Unsafe/gunzip.scm 878 */
									int BgL_tmpz00_6286;

									BgL_tmpz00_6286 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_6286, BgL_valz00_1348);
								}
								{	/* Unsafe/gunzip.scm 878 */
									int BgL_tmpz00_6289;

									BgL_tmpz00_6289 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_6289,
										BgL_zc3z04anonymousza31420ze3z87_4427);
								}
								return BgL_val0_1244z00_1357;
							}
						}
					else
						{	/* Unsafe/gunzip.scm 328 */
							BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3748;

							{	/* Unsafe/gunzip.scm 328 */
								BgL_z62iozd2parsezd2errorz62_bglt BgL_new1076z00_3749;

								{	/* Unsafe/gunzip.scm 328 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_new1075z00_3750;

									BgL_new1075z00_3750 =
										((BgL_z62iozd2parsezd2errorz62_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_z62iozd2parsezd2errorz62_bgl))));
									{	/* Unsafe/gunzip.scm 328 */
										long BgL_arg1407z00_3751;

										BgL_arg1407z00_3751 =
											BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1075z00_3750),
											BgL_arg1407z00_3751);
									}
									BgL_new1076z00_3749 = BgL_new1075z00_3750;
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1076z00_3749)))->
										BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_z62exceptionz62_bglt)
											COBJECT(((BgL_z62exceptionz62_bglt)
													BgL_new1076z00_3749)))->BgL_locationz00) =
									((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_6300;

									{	/* Unsafe/gunzip.scm 328 */
										obj_t BgL_arg1405z00_3755;

										{	/* Unsafe/gunzip.scm 328 */
											obj_t BgL_arg1406z00_3756;

											{	/* Unsafe/gunzip.scm 328 */
												obj_t BgL_classz00_3757;

												BgL_classz00_3757 =
													BGl_z62iozd2parsezd2errorz62zz__objectz00;
												BgL_arg1406z00_3756 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_3757);
											}
											BgL_arg1405z00_3755 = VECTOR_REF(BgL_arg1406z00_3756, 2L);
										}
										BgL_auxz00_6300 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1405z00_3755);
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1076z00_3749)))->
											BgL_stackz00) = ((obj_t) BgL_auxz00_6300), BUNSPEC);
								}
								((((BgL_z62errorz62_bglt) COBJECT(
												((BgL_z62errorz62_bglt) BgL_new1076z00_3749)))->
										BgL_procz00) =
									((obj_t) BGl_string2376z00zz__gunza7ipza7), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1076z00_3749)))->BgL_msgz00) =
									((obj_t) BGl_string2377z00zz__gunza7ipza7), BUNSPEC);
								((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
													BgL_new1076z00_3749)))->BgL_objz00) =
									((obj_t) BgL_statez00_1347), BUNSPEC);
								BgL_arg1404z00_3748 = BgL_new1076z00_3749;
							}
							return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1404z00_3748));
						}
				}
		}

	}



/* &NEEDBITS */
	bool_t BGl_z62NEEDBITSz62zz__gunza7ipza7(obj_t BgL_bbz00_4484,
		obj_t BgL_bkz00_4483, obj_t BgL_inputzd2portzd2_4482, obj_t BgL_nz00_1918)
	{
		{	/* Unsafe/gunzip.scm 367 */
			{

				{

				BgL_zc3z04anonymousza31741ze3z87_1921:
					{	/* Unsafe/gunzip.scm 364 */
						bool_t BgL_test2597z00_6314;

						{	/* Unsafe/gunzip.scm 364 */
							long BgL_n1z00_3006;
							long BgL_n2z00_3007;

							BgL_n1z00_3006 = (long) CINT(CELL_REF(BgL_bkz00_4483));
							BgL_n2z00_3007 = (long) CINT(BgL_nz00_1918);
							BgL_test2597z00_6314 = (BgL_n1z00_3006 < BgL_n2z00_3007);
						}
						if (BgL_test2597z00_6314)
							{	/* Unsafe/gunzip.scm 364 */
								{	/* Unsafe/gunzip.scm 365 */
									obj_t BgL_auxz00_4485;

									{	/* Unsafe/gunzip.scm 365 */
										long BgL_arg1743z00_1923;

										{	/* Unsafe/gunzip.scm 365 */
											obj_t BgL_arg1744z00_1924;

											{
												obj_t BgL_iportz00_1930;

												BgL_iportz00_1930 = BgL_inputzd2portzd2_4482;
												{

													{	/* Unsafe/gunzip.scm 356 */
														obj_t BgL_tmpz00_6318;

														BgL_tmpz00_6318 = ((obj_t) BgL_iportz00_1930);
														RGC_START_MATCH(BgL_tmpz00_6318);
													}
													{	/* Unsafe/gunzip.scm 356 */
														long BgL_matchz00_2097;

														{	/* Unsafe/gunzip.scm 356 */
															long BgL_arg1846z00_2100;
															long BgL_arg1847z00_2101;

															{	/* Unsafe/gunzip.scm 356 */
																obj_t BgL_tmpz00_6321;

																BgL_tmpz00_6321 = ((obj_t) BgL_iportz00_1930);
																BgL_arg1846z00_2100 =
																	RGC_BUFFER_FORWARD(BgL_tmpz00_6321);
															}
															{	/* Unsafe/gunzip.scm 356 */
																obj_t BgL_tmpz00_6324;

																BgL_tmpz00_6324 = ((obj_t) BgL_iportz00_1930);
																BgL_arg1847z00_2101 =
																	RGC_BUFFER_BUFPOS(BgL_tmpz00_6324);
															}
															{
																long BgL_forwardz00_2974;
																long BgL_bufposz00_2975;

																BgL_forwardz00_2974 = BgL_arg1846z00_2100;
																BgL_bufposz00_2975 = BgL_arg1847z00_2101;
															BgL_statezd20zd21077z00_2973:
																if ((BgL_forwardz00_2974 == BgL_bufposz00_2975))
																	{	/* Unsafe/gunzip.scm 356 */
																		if (rgc_fill_buffer(
																				((obj_t) BgL_iportz00_1930)))
																			{	/* Unsafe/gunzip.scm 356 */
																				long BgL_arg1751z00_2978;
																				long BgL_arg1752z00_2979;

																				{	/* Unsafe/gunzip.scm 356 */
																					obj_t BgL_tmpz00_6332;

																					BgL_tmpz00_6332 =
																						((obj_t) BgL_iportz00_1930);
																					BgL_arg1751z00_2978 =
																						RGC_BUFFER_FORWARD(BgL_tmpz00_6332);
																				}
																				{	/* Unsafe/gunzip.scm 356 */
																					obj_t BgL_tmpz00_6335;

																					BgL_tmpz00_6335 =
																						((obj_t) BgL_iportz00_1930);
																					BgL_arg1752z00_2979 =
																						RGC_BUFFER_BUFPOS(BgL_tmpz00_6335);
																				}
																				{
																					long BgL_bufposz00_6339;
																					long BgL_forwardz00_6338;

																					BgL_forwardz00_6338 =
																						BgL_arg1751z00_2978;
																					BgL_bufposz00_6339 =
																						BgL_arg1752z00_2979;
																					BgL_bufposz00_2975 =
																						BgL_bufposz00_6339;
																					BgL_forwardz00_2974 =
																						BgL_forwardz00_6338;
																					goto BgL_statezd20zd21077z00_2973;
																				}
																			}
																		else
																			{	/* Unsafe/gunzip.scm 356 */
																				BgL_matchz00_2097 = 1L;
																			}
																	}
																else
																	{	/* Unsafe/gunzip.scm 356 */
																		int BgL_curz00_2980;

																		{	/* Unsafe/gunzip.scm 356 */
																			obj_t BgL_tmpz00_6340;

																			BgL_tmpz00_6340 =
																				((obj_t) BgL_iportz00_1930);
																			BgL_curz00_2980 =
																				RGC_BUFFER_GET_CHAR(BgL_tmpz00_6340,
																				BgL_forwardz00_2974);
																		}
																		{	/* Unsafe/gunzip.scm 356 */

																			{	/* Unsafe/gunzip.scm 356 */
																				long BgL_arg1753z00_2981;

																				BgL_arg1753z00_2981 =
																					(1L + BgL_forwardz00_2974);
																				{	/* Unsafe/gunzip.scm 356 */
																					long BgL_newzd2matchzd2_2990;

																					{	/* Unsafe/gunzip.scm 356 */
																						obj_t BgL_tmpz00_6344;

																						BgL_tmpz00_6344 =
																							((obj_t) BgL_iportz00_1930);
																						RGC_STOP_MATCH(BgL_tmpz00_6344,
																							BgL_arg1753z00_2981);
																					}
																					BgL_newzd2matchzd2_2990 = 0L;
																					BgL_matchz00_2097 =
																						BgL_newzd2matchzd2_2990;
														}}}}}}
														{	/* Unsafe/gunzip.scm 356 */
															obj_t BgL_tmpz00_6347;

															BgL_tmpz00_6347 = ((obj_t) BgL_iportz00_1930);
															RGC_SET_FILEPOS(BgL_tmpz00_6347);
														}
														switch (BgL_matchz00_2097)
															{
															case 1L:

																{	/* Unsafe/gunzip.scm 328 */
																	BgL_z62iozd2parsezd2errorz62_bglt
																		BgL_arg1404z00_2994;
																	{	/* Unsafe/gunzip.scm 328 */
																		BgL_z62iozd2parsezd2errorz62_bglt
																			BgL_new1076z00_2995;
																		{	/* Unsafe/gunzip.scm 328 */
																			BgL_z62iozd2parsezd2errorz62_bglt
																				BgL_new1075z00_2996;
																			BgL_new1075z00_2996 =
																				((BgL_z62iozd2parsezd2errorz62_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_z62iozd2parsezd2errorz62_bgl))));
																			{	/* Unsafe/gunzip.scm 328 */
																				long BgL_arg1407z00_2997;

																				BgL_arg1407z00_2997 =
																					BGL_CLASS_NUM
																					(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1075z00_2996),
																					BgL_arg1407z00_2997);
																			}
																			BgL_new1076z00_2995 = BgL_new1075z00_2996;
																		}
																		((((BgL_z62exceptionz62_bglt) COBJECT(
																						((BgL_z62exceptionz62_bglt)
																							BgL_new1076z00_2995)))->
																				BgL_fnamez00) =
																			((obj_t) BFALSE), BUNSPEC);
																		((((BgL_z62exceptionz62_bglt)
																					COBJECT(((BgL_z62exceptionz62_bglt)
																							BgL_new1076z00_2995)))->
																				BgL_locationz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		{
																			obj_t BgL_auxz00_6358;

																			{	/* Unsafe/gunzip.scm 328 */
																				obj_t BgL_arg1405z00_3001;

																				{	/* Unsafe/gunzip.scm 328 */
																					obj_t BgL_arg1406z00_3002;

																					{	/* Unsafe/gunzip.scm 328 */
																						obj_t BgL_classz00_3003;

																						BgL_classz00_3003 =
																							BGl_z62iozd2parsezd2errorz62zz__objectz00;
																						BgL_arg1406z00_3002 =
																							BGL_CLASS_ALL_FIELDS
																							(BgL_classz00_3003);
																					}
																					BgL_arg1405z00_3001 =
																						VECTOR_REF(BgL_arg1406z00_3002, 2L);
																				}
																				BgL_auxz00_6358 =
																					BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																					(BgL_arg1405z00_3001);
																			}
																			((((BgL_z62exceptionz62_bglt) COBJECT(
																							((BgL_z62exceptionz62_bglt)
																								BgL_new1076z00_2995)))->
																					BgL_stackz00) =
																				((obj_t) BgL_auxz00_6358), BUNSPEC);
																		}
																		((((BgL_z62errorz62_bglt) COBJECT(
																						((BgL_z62errorz62_bglt)
																							BgL_new1076z00_2995)))->
																				BgL_procz00) =
																			((obj_t)
																				BGl_string2376z00zz__gunza7ipza7),
																			BUNSPEC);
																		((((BgL_z62errorz62_bglt)
																					COBJECT(((BgL_z62errorz62_bglt)
																							BgL_new1076z00_2995)))->
																				BgL_msgz00) =
																			((obj_t)
																				BGl_string2400z00zz__gunza7ipza7),
																			BUNSPEC);
																		((((BgL_z62errorz62_bglt)
																					COBJECT(((BgL_z62errorz62_bglt)
																							BgL_new1076z00_2995)))->
																				BgL_objz00) =
																			((obj_t) BgL_inputzd2portzd2_4482),
																			BUNSPEC);
																		BgL_arg1404z00_2994 = BgL_new1076z00_2995;
																	}
																	BgL_arg1744z00_1924 =
																		BGl_raisez00zz__errorz00(
																		((obj_t) BgL_arg1404z00_2994));
																} break;
															case 0L:

																{	/* Unsafe/gunzip.scm 356 */
																	int BgL_tmpz00_6372;

																	{	/* Unsafe/gunzip.scm 356 */
																		obj_t BgL_tmpz00_6373;

																		BgL_tmpz00_6373 =
																			((obj_t) BgL_iportz00_1930);
																		BgL_tmpz00_6372 =
																			RGC_BUFFER_BYTE(BgL_tmpz00_6373);
																	}
																	BgL_arg1744z00_1924 = BINT(BgL_tmpz00_6372);
																} break;
															default:
																BgL_arg1744z00_1924 =
																	BGl_errorz00zz__errorz00
																	(BGl_string2401z00zz__gunza7ipza7,
																	BGl_string2402z00zz__gunza7ipza7,
																	BINT(BgL_matchz00_2097));
															}
													}
												}
											}
											{	/* Unsafe/gunzip.scm 365 */
												long BgL_xz00_3008;
												long BgL_yz00_3009;

												BgL_xz00_3008 = (long) CINT(BgL_arg1744z00_1924);
												BgL_yz00_3009 = (long) CINT(CELL_REF(BgL_bkz00_4483));
												BgL_arg1743z00_1923 =
													(BgL_xz00_3008 << (int) (BgL_yz00_3009));
										}}
										BgL_auxz00_4485 =
											ADDFX(CELL_REF(BgL_bbz00_4484),
											BINT(BgL_arg1743z00_1923));
									}
									CELL_SET(BgL_bbz00_4484, BgL_auxz00_4485);
								}
								{	/* Unsafe/gunzip.scm 366 */
									obj_t BgL_auxz00_4486;

									BgL_auxz00_4486 = ADDFX(CELL_REF(BgL_bkz00_4483), BINT(8L));
									CELL_SET(BgL_bkz00_4483, BgL_auxz00_4486);
								}
								goto BgL_zc3z04anonymousza31741ze3z87_1921;
							}
						else
							{	/* Unsafe/gunzip.scm 364 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* &loop-inflate */
	obj_t BGl_z62loopzd2inflatezb0zz__gunza7ipza7(obj_t BgL_tdz00_4502,
		long BgL_mdz00_4501, obj_t BgL_bdz00_4500, obj_t BgL_tlz00_4499,
		long BgL_mlz00_4498, obj_t BgL_blz00_4497, obj_t BgL_bkz00_4496,
		obj_t BgL_bbz00_4495, obj_t BgL_tz00_4494, obj_t BgL_inputzd2portzd2_4493,
		obj_t BgL_slidez00_4492, obj_t BgL_ez00_4491, obj_t BgL_wpz00_4490,
		obj_t BgL_nz00_4489, obj_t BgL_dz00_4488, long BgL_wsiza7eza7_4487,
		obj_t BgL_retz00_1628)
	{
		{	/* Unsafe/gunzip.scm 703 */
		BGl_z62loopzd2inflatezb0zz__gunza7ipza7:
			{

				if (((long) CINT(BgL_retz00_1628) > 0L))
					{	/* Unsafe/gunzip.scm 660 */
						obj_t BgL_val0_1229z00_1631;

						BgL_val0_1229z00_1631 = BGl_symbol2372z00zz__gunza7ipza7;
						{	/* Unsafe/gunzip.scm 660 */
							obj_t BgL_zc3z04anonymousza31564ze3z87_4422;

							BgL_zc3z04anonymousza31564ze3z87_4422 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31564ze3ze5zz__gunza7ipza7, (int) (0L),
								(int) (16L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (0L),
								BINT(BgL_wsiza7eza7_4487));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (1L),
								((obj_t) BgL_dz00_4488));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (2L),
								((obj_t) BgL_nz00_4489));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (3L),
								((obj_t) BgL_wpz00_4490));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (4L),
								((obj_t) BgL_ez00_4491));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (5L),
								BgL_slidez00_4492);
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (6L),
								BgL_inputzd2portzd2_4493);
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (7L),
								((obj_t) BgL_tz00_4494));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (8L),
								((obj_t) BgL_bbz00_4495));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (9L),
								((obj_t) BgL_bkz00_4496));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (10L),
								BgL_blz00_4497);
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (11L),
								BINT(BgL_mlz00_4498));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (12L),
								BgL_tlz00_4499);
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (13L),
								BgL_bdz00_4500);
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (14L),
								BINT(BgL_mdz00_4501));
							PROCEDURE_SET(BgL_zc3z04anonymousza31564ze3z87_4422, (int) (15L),
								BgL_tdz00_4502);
							{	/* Unsafe/gunzip.scm 660 */
								int BgL_tmpz00_6436;

								BgL_tmpz00_6436 = (int) (3L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6436);
							}
							{	/* Unsafe/gunzip.scm 660 */
								int BgL_tmpz00_6439;

								BgL_tmpz00_6439 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6439, BgL_retz00_1628);
							}
							{	/* Unsafe/gunzip.scm 660 */
								int BgL_tmpz00_6442;

								BgL_tmpz00_6442 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6442,
									BgL_zc3z04anonymousza31564ze3z87_4422);
							}
							return BgL_val0_1229z00_1631;
						}
					}
				else
					{	/* Unsafe/gunzip.scm 659 */
						BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4495, BgL_bkz00_4496,
							BgL_inputzd2portzd2_4493, BgL_blz00_4497);
						{	/* Unsafe/gunzip.scm 663 */
							obj_t BgL_auxz00_4503;

							{	/* Unsafe/gunzip.scm 663 */
								long BgL_arg1565z00_1636;

								{	/* Unsafe/gunzip.scm 663 */
									long BgL_xz00_3426;

									BgL_xz00_3426 = (long) CINT(CELL_REF(BgL_bbz00_4495));
									BgL_arg1565z00_1636 = (BgL_xz00_3426 & BgL_mlz00_4498);
								}
								BgL_auxz00_4503 =
									VECTOR_REF(((obj_t) BgL_tlz00_4499), BgL_arg1565z00_1636);
							}
							CELL_SET(BgL_tz00_4494, BgL_auxz00_4503);
						}
						{	/* Unsafe/gunzip.scm 664 */
							obj_t BgL_auxz00_4504;

							{	/* Unsafe/gunzip.scm 664 */
								BgL_huftz00_bglt BgL_i1119z00_1637;

								BgL_i1119z00_1637 =
									((BgL_huftz00_bglt)
									((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
								BgL_auxz00_4504 =
									BINT(
									(((BgL_huftz00_bglt) COBJECT(BgL_i1119z00_1637))->BgL_ez00));
							}
							CELL_SET(BgL_ez00_4491, BgL_auxz00_4504);
						}
						{	/* Unsafe/gunzip.scm 665 */
							bool_t BgL_test2601z00_6454;

							{	/* Unsafe/gunzip.scm 665 */
								long BgL_n1z00_3430;

								BgL_n1z00_3430 = (long) CINT(CELL_REF(BgL_ez00_4491));
								BgL_test2601z00_6454 = (BgL_n1z00_3430 > 16L);
							}
							if (BgL_test2601z00_6454)
								{	/* Unsafe/gunzip.scm 665 */
									BGl_jumpzd2tozd2nextze70ze7zz__gunza7ipza7(BgL_bkz00_4496,
										BgL_bbz00_4495, BgL_tz00_4494, BgL_inputzd2portzd2_4493,
										BgL_ez00_4491);
								}
							else
								{	/* Unsafe/gunzip.scm 665 */
									((bool_t) 0);
								}
						}
						{	/* Unsafe/gunzip.scm 666 */
							long BgL_arg1567z00_1639;

							{	/* Unsafe/gunzip.scm 666 */
								BgL_huftz00_bglt BgL_i1121z00_1640;

								BgL_i1121z00_1640 =
									((BgL_huftz00_bglt)
									((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
								BgL_arg1567z00_1639 =
									(((BgL_huftz00_bglt) COBJECT(BgL_i1121z00_1640))->BgL_bz00);
							}
							{	/* Unsafe/gunzip.scm 370 */
								obj_t BgL_auxz00_4505;

								{	/* Unsafe/gunzip.scm 370 */
									long BgL_xz00_3431;

									BgL_xz00_3431 = (long) CINT(CELL_REF(BgL_bbz00_4495));
									BgL_auxz00_4505 =
										BINT((BgL_xz00_3431 >> (int) (BgL_arg1567z00_1639)));
								}
								CELL_SET(BgL_bbz00_4495, BgL_auxz00_4505);
							}
							{	/* Unsafe/gunzip.scm 371 */
								obj_t BgL_auxz00_4506;

								BgL_auxz00_4506 =
									SUBFX(CELL_REF(BgL_bkz00_4496), BINT(BgL_arg1567z00_1639));
								CELL_SET(BgL_bkz00_4496, BgL_auxz00_4506);
						}}
						{	/* Unsafe/gunzip.scm 667 */
							bool_t BgL_test2602z00_6467;

							{	/* Unsafe/gunzip.scm 667 */
								long BgL_n1z00_3435;

								BgL_n1z00_3435 = (long) CINT(CELL_REF(BgL_ez00_4491));
								BgL_test2602z00_6467 = (BgL_n1z00_3435 == 16L);
							}
							if (BgL_test2602z00_6467)
								{	/* Unsafe/gunzip.scm 667 */
									{	/* Unsafe/gunzip.scm 670 */
										unsigned char BgL_arg1571z00_1642;

										{	/* Unsafe/gunzip.scm 670 */
											obj_t BgL_arg1573z00_1643;

											{	/* Unsafe/gunzip.scm 670 */
												BgL_huftz00_bglt BgL_i1123z00_1644;

												BgL_i1123z00_1644 =
													((BgL_huftz00_bglt)
													((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
												BgL_arg1573z00_1643 =
													(((BgL_huftz00_bglt) COBJECT(BgL_i1123z00_1644))->
													BgL_vz00);
											}
											BgL_arg1571z00_1642 = ((long) CINT(BgL_arg1573z00_1643));
										}
										{	/* Unsafe/gunzip.scm 670 */
											long BgL_kz00_3438;

											BgL_kz00_3438 = (long) CINT(CELL_REF(BgL_wpz00_4490));
											{	/* Unsafe/gunzip.scm 670 */
												obj_t BgL_tmpz00_6476;

												BgL_tmpz00_6476 = ((obj_t) BgL_slidez00_4492);
												STRING_SET(BgL_tmpz00_6476, BgL_kz00_3438,
													BgL_arg1571z00_1642);
									}}}
									{	/* Unsafe/gunzip.scm 671 */
										obj_t BgL_auxz00_4507;

										BgL_auxz00_4507 = ADDFX(CELL_REF(BgL_wpz00_4490), BINT(1L));
										CELL_SET(BgL_wpz00_4490, BgL_auxz00_4507);
									}
									{	/* Unsafe/gunzip.scm 672 */
										obj_t BgL_arg1575z00_1645;

										BgL_arg1575z00_1645 =
											BGl_z62checkzd2flushzb0zz__gunza7ipza7
											(BgL_wsiza7eza7_4487, BgL_wpz00_4490);
										{
											obj_t BgL_retz00_6482;

											BgL_retz00_6482 = BgL_arg1575z00_1645;
											BgL_retz00_1628 = BgL_retz00_6482;
											goto BGl_z62loopzd2inflatezb0zz__gunza7ipza7;
										}
									}
								}
							else
								{	/* Unsafe/gunzip.scm 675 */
									bool_t BgL_test2603z00_6483;

									{	/* Unsafe/gunzip.scm 675 */
										long BgL_n1z00_3441;

										BgL_n1z00_3441 = (long) CINT(CELL_REF(BgL_ez00_4491));
										BgL_test2603z00_6483 = (BgL_n1z00_3441 == 15L);
									}
									if (BgL_test2603z00_6483)
										{	/* Unsafe/gunzip.scm 676 */
											obj_t BgL_val0_1232z00_1647;

											BgL_val0_1232z00_1647 = BGl_symbol2368z00zz__gunza7ipza7;
											{	/* Unsafe/gunzip.scm 676 */
												int BgL_tmpz00_6486;

												BgL_tmpz00_6486 = (int) (3L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6486);
											}
											{	/* Unsafe/gunzip.scm 676 */
												int BgL_tmpz00_6489;

												BgL_tmpz00_6489 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_6489, BTRUE);
											}
											{	/* Unsafe/gunzip.scm 676 */
												int BgL_tmpz00_6492;

												BgL_tmpz00_6492 = (int) (2L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_6492, BFALSE);
											}
											return BgL_val0_1232z00_1647;
										}
									else
										{	/* Unsafe/gunzip.scm 675 */
											BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4495,
												BgL_bkz00_4496, BgL_inputzd2portzd2_4493,
												CELL_REF(BgL_ez00_4491));
											{	/* Unsafe/gunzip.scm 680 */
												obj_t BgL_auxz00_4508;

												{	/* Unsafe/gunzip.scm 680 */
													obj_t BgL_arg1578z00_1650;
													long BgL_arg1579z00_1651;

													{	/* Unsafe/gunzip.scm 680 */
														BgL_huftz00_bglt BgL_i1125z00_1652;

														BgL_i1125z00_1652 =
															((BgL_huftz00_bglt)
															((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
														BgL_arg1578z00_1650 =
															(((BgL_huftz00_bglt) COBJECT(BgL_i1125z00_1652))->
															BgL_vz00);
													}
													{	/* Unsafe/gunzip.scm 682 */
														long BgL_arg1580z00_1653;

														{	/* Unsafe/gunzip.scm 682 */
															obj_t BgL_arg1582z00_1654;

															BgL_arg1582z00_1654 =
																BGl_vector2393z00zz__gunza7ipza7;
															{	/* Unsafe/gunzip.scm 682 */
																long BgL_kz00_3443;

																BgL_kz00_3443 =
																	(long) CINT(CELL_REF(BgL_ez00_4491));
																BgL_arg1580z00_1653 =
																	(long) CINT(VECTOR_REF(BgL_arg1582z00_1654,
																		BgL_kz00_3443));
														}}
														{	/* Unsafe/gunzip.scm 681 */
															long BgL_xz00_3444;

															BgL_xz00_3444 =
																(long) CINT(CELL_REF(BgL_bbz00_4495));
															BgL_arg1579z00_1651 =
																(BgL_xz00_3444 & BgL_arg1580z00_1653);
													}}
													BgL_auxz00_4508 =
														ADDFX(BgL_arg1578z00_1650,
														BINT(BgL_arg1579z00_1651));
												}
												CELL_SET(BgL_nz00_4489, BgL_auxz00_4508);
											}
											{	/* Unsafe/gunzip.scm 683 */
												obj_t BgL_nz00_3448;

												BgL_nz00_3448 = CELL_REF(BgL_ez00_4491);
												{	/* Unsafe/gunzip.scm 370 */
													obj_t BgL_auxz00_4509;

													{	/* Unsafe/gunzip.scm 370 */
														long BgL_xz00_3449;
														long BgL_yz00_3450;

														BgL_xz00_3449 =
															(long) CINT(CELL_REF(BgL_bbz00_4495));
														BgL_yz00_3450 = (long) CINT(BgL_nz00_3448);
														BgL_auxz00_4509 =
															BINT((BgL_xz00_3449 >> (int) (BgL_yz00_3450)));
													}
													CELL_SET(BgL_bbz00_4495, BgL_auxz00_4509);
												}
												{	/* Unsafe/gunzip.scm 371 */
													obj_t BgL_auxz00_4510;

													BgL_auxz00_4510 =
														SUBFX(CELL_REF(BgL_bkz00_4496), BgL_nz00_3448);
													CELL_SET(BgL_bkz00_4496, BgL_auxz00_4510);
											}}
											BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4495,
												BgL_bkz00_4496, BgL_inputzd2portzd2_4493,
												BgL_bdz00_4500);
											{	/* Unsafe/gunzip.scm 687 */
												obj_t BgL_auxz00_4511;

												{	/* Unsafe/gunzip.scm 687 */
													long BgL_arg1583z00_1655;

													{	/* Unsafe/gunzip.scm 687 */
														long BgL_xz00_3453;

														BgL_xz00_3453 =
															(long) CINT(CELL_REF(BgL_bbz00_4495));
														BgL_arg1583z00_1655 =
															(BgL_xz00_3453 & BgL_mdz00_4501);
													}
													BgL_auxz00_4511 =
														VECTOR_REF(
														((obj_t) BgL_tdz00_4502), BgL_arg1583z00_1655);
												}
												CELL_SET(BgL_tz00_4494, BgL_auxz00_4511);
											}
											{	/* Unsafe/gunzip.scm 688 */
												obj_t BgL_auxz00_4512;

												{	/* Unsafe/gunzip.scm 688 */
													BgL_huftz00_bglt BgL_i1127z00_1656;

													BgL_i1127z00_1656 =
														((BgL_huftz00_bglt)
														((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
													BgL_auxz00_4512 =
														BINT(
														(((BgL_huftz00_bglt) COBJECT(BgL_i1127z00_1656))->
															BgL_ez00));
												}
												CELL_SET(BgL_ez00_4491, BgL_auxz00_4512);
											}
											{	/* Unsafe/gunzip.scm 689 */
												bool_t BgL_test2604z00_6521;

												{	/* Unsafe/gunzip.scm 689 */
													long BgL_n1z00_3457;

													BgL_n1z00_3457 = (long) CINT(CELL_REF(BgL_ez00_4491));
													BgL_test2604z00_6521 = (BgL_n1z00_3457 > 16L);
												}
												if (BgL_test2604z00_6521)
													{	/* Unsafe/gunzip.scm 689 */
														BGl_jumpzd2tozd2nextze70ze7zz__gunza7ipza7
															(BgL_bkz00_4496, BgL_bbz00_4495, BgL_tz00_4494,
															BgL_inputzd2portzd2_4493, BgL_ez00_4491);
													}
												else
													{	/* Unsafe/gunzip.scm 689 */
														((bool_t) 0);
													}
											}
											{	/* Unsafe/gunzip.scm 690 */
												long BgL_arg1585z00_1658;

												{	/* Unsafe/gunzip.scm 690 */
													BgL_huftz00_bglt BgL_i1129z00_1659;

													BgL_i1129z00_1659 =
														((BgL_huftz00_bglt)
														((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
													BgL_arg1585z00_1658 =
														(((BgL_huftz00_bglt) COBJECT(BgL_i1129z00_1659))->
														BgL_bz00);
												}
												{	/* Unsafe/gunzip.scm 370 */
													obj_t BgL_auxz00_4513;

													{	/* Unsafe/gunzip.scm 370 */
														long BgL_xz00_3458;

														BgL_xz00_3458 =
															(long) CINT(CELL_REF(BgL_bbz00_4495));
														BgL_auxz00_4513 =
															BINT(
															(BgL_xz00_3458 >> (int) (BgL_arg1585z00_1658)));
													}
													CELL_SET(BgL_bbz00_4495, BgL_auxz00_4513);
												}
												{	/* Unsafe/gunzip.scm 371 */
													obj_t BgL_auxz00_4514;

													BgL_auxz00_4514 =
														SUBFX(CELL_REF(BgL_bkz00_4496),
														BINT(BgL_arg1585z00_1658));
													CELL_SET(BgL_bkz00_4496, BgL_auxz00_4514);
											}}
											BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4495,
												BgL_bkz00_4496, BgL_inputzd2portzd2_4493,
												CELL_REF(BgL_ez00_4491));
											{	/* Unsafe/gunzip.scm 693 */
												obj_t BgL_auxz00_4515;

												{	/* Unsafe/gunzip.scm 695 */
													long BgL_arg1586z00_1660;

													{	/* Unsafe/gunzip.scm 695 */
														long BgL_arg1587z00_1661;

														{	/* Unsafe/gunzip.scm 695 */
															obj_t BgL_arg1589z00_1662;
															long BgL_arg1591z00_1663;

															{	/* Unsafe/gunzip.scm 695 */
																BgL_huftz00_bglt BgL_i1131z00_1664;

																BgL_i1131z00_1664 =
																	((BgL_huftz00_bglt)
																	((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4494)));
																BgL_arg1589z00_1662 =
																	(((BgL_huftz00_bglt)
																		COBJECT(BgL_i1131z00_1664))->BgL_vz00);
															}
															{	/* Unsafe/gunzip.scm 698 */
																long BgL_arg1593z00_1665;

																{	/* Unsafe/gunzip.scm 698 */
																	obj_t BgL_arg1594z00_1666;

																	BgL_arg1594z00_1666 =
																		BGl_vector2393z00zz__gunza7ipza7;
																	{	/* Unsafe/gunzip.scm 698 */
																		long BgL_kz00_3463;

																		BgL_kz00_3463 =
																			(long) CINT(CELL_REF(BgL_ez00_4491));
																		BgL_arg1593z00_1665 =
																			(long)
																			CINT(VECTOR_REF(BgL_arg1594z00_1666,
																				BgL_kz00_3463));
																}}
																{	/* Unsafe/gunzip.scm 696 */
																	long BgL_xz00_3464;

																	BgL_xz00_3464 =
																		(long) CINT(CELL_REF(BgL_bbz00_4495));
																	BgL_arg1591z00_1663 =
																		(BgL_xz00_3464 & BgL_arg1593z00_1665);
															}}
															BgL_arg1587z00_1661 =
																(
																(long) CINT(BgL_arg1589z00_1662) +
																BgL_arg1591z00_1663);
														}
														{	/* Unsafe/gunzip.scm 694 */
															long BgL_za71za7_3468;

															BgL_za71za7_3468 =
																(long) CINT(CELL_REF(BgL_wpz00_4490));
															BgL_arg1586z00_1660 =
																(BgL_za71za7_3468 - BgL_arg1587z00_1661);
													}}
													BgL_auxz00_4515 =
														BINT(BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00
														(BgL_arg1586z00_1660, BgL_wsiza7eza7_4487));
												}
												CELL_SET(BgL_dz00_4488, BgL_auxz00_4515);
											}
											{	/* Unsafe/gunzip.scm 700 */
												obj_t BgL_nz00_3470;

												BgL_nz00_3470 = CELL_REF(BgL_ez00_4491);
												{	/* Unsafe/gunzip.scm 370 */
													obj_t BgL_auxz00_4516;

													{	/* Unsafe/gunzip.scm 370 */
														long BgL_xz00_3471;
														long BgL_yz00_3472;

														BgL_xz00_3471 =
															(long) CINT(CELL_REF(BgL_bbz00_4495));
														BgL_yz00_3472 = (long) CINT(BgL_nz00_3470);
														BgL_auxz00_4516 =
															BINT((BgL_xz00_3471 >> (int) (BgL_yz00_3472)));
													}
													CELL_SET(BgL_bbz00_4495, BgL_auxz00_4516);
												}
												{	/* Unsafe/gunzip.scm 371 */
													obj_t BgL_auxz00_4517;

													BgL_auxz00_4517 =
														SUBFX(CELL_REF(BgL_bkz00_4496), BgL_nz00_3470);
													CELL_SET(BgL_bkz00_4496, BgL_auxz00_4517);
											}}
										BgL_zc3z04anonymousza31538ze3z87_1584:
											{	/* Unsafe/gunzip.scm 615 */
												obj_t BgL_auxz00_4518;

												{	/* Unsafe/gunzip.scm 615 */
													long BgL_xz00_3382;

													BgL_xz00_3382 = (long) CINT(CELL_REF(BgL_dz00_4488));
													BgL_auxz00_4518 =
														BINT((BgL_xz00_3382 & (BgL_wsiza7eza7_4487 - 1L)));
												}
												CELL_SET(BgL_dz00_4488, BgL_auxz00_4518);
											}
											{	/* Unsafe/gunzip.scm 616 */
												obj_t BgL_auxz00_4519;

												{	/* Unsafe/gunzip.scm 616 */
													obj_t BgL_az00_1586;
													long BgL_bz00_1587;

													BgL_az00_1586 = CELL_REF(BgL_nz00_4489);
													{	/* Unsafe/gunzip.scm 616 */
														obj_t BgL_arg1543z00_1589;

														{	/* Unsafe/gunzip.scm 616 */
															obj_t BgL_az00_1590;
															obj_t BgL_bz00_1591;

															BgL_az00_1590 = CELL_REF(BgL_dz00_4488);
															BgL_bz00_1591 = CELL_REF(BgL_wpz00_4490);
															if (
																((long) CINT(BgL_az00_1590) >
																	(long) CINT(BgL_bz00_1591)))
																{	/* Unsafe/gunzip.scm 616 */
																	BgL_arg1543z00_1589 = BgL_az00_1590;
																}
															else
																{	/* Unsafe/gunzip.scm 616 */
																	BgL_arg1543z00_1589 = BgL_bz00_1591;
																}
														}
														BgL_bz00_1587 =
															(BgL_wsiza7eza7_4487 -
															(long) CINT(BgL_arg1543z00_1589));
													}
													if (((long) CINT(BgL_az00_1586) < BgL_bz00_1587))
														{	/* Unsafe/gunzip.scm 616 */
															BgL_auxz00_4519 = BgL_az00_1586;
														}
													else
														{	/* Unsafe/gunzip.scm 616 */
															BgL_auxz00_4519 = BINT(BgL_bz00_1587);
														}
												}
												CELL_SET(BgL_ez00_4491, BgL_auxz00_4519);
											}
											{	/* Unsafe/gunzip.scm 617 */
												obj_t BgL_auxz00_4520;

												BgL_auxz00_4520 =
													SUBFX(CELL_REF(BgL_nz00_4489),
													CELL_REF(BgL_ez00_4491));
												CELL_SET(BgL_nz00_4489, BgL_auxz00_4520);
											}
											{

											BgL_zc3z04anonymousza31545ze3z87_1594:
												{	/* Unsafe/gunzip.scm 619 */
													unsigned char BgL_arg1546z00_1595;

													{	/* Unsafe/gunzip.scm 619 */
														long BgL_kz00_3393;

														BgL_kz00_3393 =
															(long) CINT(CELL_REF(BgL_dz00_4488));
														BgL_arg1546z00_1595 =
															STRING_REF(
															((obj_t) BgL_slidez00_4492), BgL_kz00_3393);
													}
													{	/* Unsafe/gunzip.scm 619 */
														long BgL_kz00_3395;

														BgL_kz00_3395 =
															(long) CINT(CELL_REF(BgL_wpz00_4490));
														{	/* Unsafe/gunzip.scm 619 */
															obj_t BgL_tmpz00_6574;

															BgL_tmpz00_6574 = ((obj_t) BgL_slidez00_4492);
															STRING_SET(BgL_tmpz00_6574, BgL_kz00_3395,
																BgL_arg1546z00_1595);
												}}}
												{	/* Unsafe/gunzip.scm 620 */
													obj_t BgL_auxz00_4521;

													BgL_auxz00_4521 =
														ADDFX(CELL_REF(BgL_wpz00_4490), BINT(1L));
													CELL_SET(BgL_wpz00_4490, BgL_auxz00_4521);
												}
												{	/* Unsafe/gunzip.scm 621 */
													obj_t BgL_auxz00_4522;

													BgL_auxz00_4522 =
														ADDFX(CELL_REF(BgL_dz00_4488), BINT(1L));
													CELL_SET(BgL_dz00_4488, BgL_auxz00_4522);
												}
												{	/* Unsafe/gunzip.scm 622 */
													obj_t BgL_auxz00_4523;

													BgL_auxz00_4523 =
														SUBFX(CELL_REF(BgL_ez00_4491), BINT(1L));
													CELL_SET(BgL_ez00_4491, BgL_auxz00_4523);
												}
												{	/* Unsafe/gunzip.scm 623 */
													bool_t BgL_test2607z00_6583;

													{	/* Unsafe/gunzip.scm 623 */
														long BgL_n1z00_3400;

														BgL_n1z00_3400 =
															(long) CINT(CELL_REF(BgL_ez00_4491));
														BgL_test2607z00_6583 = (BgL_n1z00_3400 == 0L);
													}
													if (BgL_test2607z00_6583)
														{	/* Unsafe/gunzip.scm 623 */
															((bool_t) 0);
														}
													else
														{	/* Unsafe/gunzip.scm 623 */
															goto BgL_zc3z04anonymousza31545ze3z87_1594;
														}
												}
											}
											{	/* Unsafe/gunzip.scm 624 */
												obj_t BgL_rz00_1598;

												BgL_rz00_1598 =
													BGl_z62checkzd2flushzb0zz__gunza7ipza7
													(BgL_wsiza7eza7_4487, BgL_wpz00_4490);
												{	/* Unsafe/gunzip.scm 626 */
													bool_t BgL_test2608z00_6587;

													{	/* Unsafe/gunzip.scm 626 */
														long BgL_n1z00_3401;

														BgL_n1z00_3401 =
															(long) CINT(CELL_REF(BgL_nz00_4489));
														BgL_test2608z00_6587 = (BgL_n1z00_3401 == 0L);
													}
													if (BgL_test2608z00_6587)
														{
															obj_t BgL_retz00_6590;

															BgL_retz00_6590 = BgL_rz00_1598;
															BgL_retz00_1628 = BgL_retz00_6590;
															goto BGl_z62loopzd2inflatezb0zz__gunza7ipza7;
														}
													else
														{	/* Unsafe/gunzip.scm 626 */
															if (((long) CINT(BgL_rz00_1598) == 0L))
																{	/* Unsafe/gunzip.scm 628 */
																	goto BgL_zc3z04anonymousza31538ze3z87_1584;
																}
															else
																{	/* Unsafe/gunzip.scm 631 */
																	obj_t BgL_val0_1223z00_1601;

																	BgL_val0_1223z00_1601 =
																		BGl_symbol2372z00zz__gunza7ipza7;
																	{	/* Unsafe/gunzip.scm 631 */
																		obj_t BgL_zc3z04anonymousza31550ze3z87_4425;

																		BgL_zc3z04anonymousza31550ze3z87_4425 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31550ze3ze5zz__gunza7ipza7,
																			(int) (0L), (int) (16L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (0L), BgL_inputzd2portzd2_4493);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (1L), ((obj_t) BgL_tz00_4494));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (2L), ((obj_t) BgL_bbz00_4495));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (3L), ((obj_t) BgL_bkz00_4496));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (4L), BgL_blz00_4497);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (5L), BINT(BgL_mlz00_4498));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (6L), BgL_tlz00_4499);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (7L), BgL_bdz00_4500);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (8L), BINT(BgL_mdz00_4501));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (9L), BgL_tdz00_4502);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (10L), BINT(BgL_wsiza7eza7_4487));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (11L), ((obj_t) BgL_dz00_4488));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (12L), ((obj_t) BgL_nz00_4489));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (13L), ((obj_t) BgL_wpz00_4490));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (14L), ((obj_t) BgL_ez00_4491));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31550ze3z87_4425,
																			(int) (15L), BgL_slidez00_4492);
																		{	/* Unsafe/gunzip.scm 631 */
																			int BgL_tmpz00_6639;

																			BgL_tmpz00_6639 = (int) (3L);
																			BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6639);
																		}
																		{	/* Unsafe/gunzip.scm 631 */
																			int BgL_tmpz00_6642;

																			BgL_tmpz00_6642 = (int) (1L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_6642,
																				BgL_rz00_1598);
																		}
																		{	/* Unsafe/gunzip.scm 631 */
																			int BgL_tmpz00_6645;

																			BgL_tmpz00_6645 = (int) (2L);
																			BGL_MVALUES_VAL_SET(BgL_tmpz00_6645,
																				BgL_zc3z04anonymousza31550ze3z87_4425);
																		}
																		return BgL_val0_1223z00_1601;
																	}
																}
														}
												}
											}
										}
								}
						}
					}
			}
		}

	}



/* jump-to-next~0 */
	bool_t BGl_jumpzd2tozd2nextze70ze7zz__gunza7ipza7(obj_t BgL_bkz00_4767,
		obj_t BgL_bbz00_4766, obj_t BgL_tz00_4765, obj_t BgL_inputzd2portzd2_4764,
		obj_t BgL_ez00_4763)
	{
		{	/* Unsafe/gunzip.scm 611 */
			{

			BgL_zc3z04anonymousza31525ze3z87_1570:
				{	/* Unsafe/gunzip.scm 601 */
					bool_t BgL_test2610z00_6648;

					{	/* Unsafe/gunzip.scm 601 */
						long BgL_n1z00_3357;

						BgL_n1z00_3357 = (long) CINT(CELL_REF(BgL_ez00_4763));
						BgL_test2610z00_6648 = (BgL_n1z00_3357 == 99L);
					}
					if (BgL_test2610z00_6648)
						{	/* Unsafe/gunzip.scm 603 */
							obj_t BgL_arg1527z00_1572;

							{	/* Unsafe/gunzip.scm 603 */
								obj_t BgL_list1528z00_1573;

								BgL_list1528z00_1573 =
									MAKE_YOUNG_PAIR(CELL_REF(BgL_ez00_4763), BNIL);
								BgL_arg1527z00_1572 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2403z00zz__gunza7ipza7, BgL_list1528z00_1573);
							}
							{	/* Unsafe/gunzip.scm 328 */
								BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3358;

								{	/* Unsafe/gunzip.scm 328 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_new1076z00_3359;

									{	/* Unsafe/gunzip.scm 328 */
										BgL_z62iozd2parsezd2errorz62_bglt BgL_new1075z00_3360;

										BgL_new1075z00_3360 =
											((BgL_z62iozd2parsezd2errorz62_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62iozd2parsezd2errorz62_bgl))));
										{	/* Unsafe/gunzip.scm 328 */
											long BgL_arg1407z00_3361;

											BgL_arg1407z00_3361 =
												BGL_CLASS_NUM
												(BGl_z62iozd2parsezd2errorz62zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
													BgL_new1075z00_3360), BgL_arg1407z00_3361);
										}
										BgL_new1076z00_3359 = BgL_new1075z00_3360;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1076z00_3359)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1076z00_3359)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_6661;

										{	/* Unsafe/gunzip.scm 328 */
											obj_t BgL_arg1405z00_3365;

											{	/* Unsafe/gunzip.scm 328 */
												obj_t BgL_arg1406z00_3366;

												{	/* Unsafe/gunzip.scm 328 */
													obj_t BgL_classz00_3367;

													BgL_classz00_3367 =
														BGl_z62iozd2parsezd2errorz62zz__objectz00;
													BgL_arg1406z00_3366 =
														BGL_CLASS_ALL_FIELDS(BgL_classz00_3367);
												}
												BgL_arg1405z00_3365 =
													VECTOR_REF(BgL_arg1406z00_3366, 2L);
											}
											BgL_auxz00_6661 =
												BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
												(BgL_arg1405z00_3365);
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1076z00_3359)))->
												BgL_stackz00) = ((obj_t) BgL_auxz00_6661), BUNSPEC);
									}
									((((BgL_z62errorz62_bglt) COBJECT(
													((BgL_z62errorz62_bglt) BgL_new1076z00_3359)))->
											BgL_procz00) =
										((obj_t) BGl_string2388z00zz__gunza7ipza7), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1076z00_3359)))->BgL_msgz00) =
										((obj_t) BgL_arg1527z00_1572), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1076z00_3359)))->BgL_objz00) =
										((obj_t) BgL_inputzd2portzd2_4764), BUNSPEC);
									BgL_arg1404z00_3358 = BgL_new1076z00_3359;
								}
								BGl_raisez00zz__errorz00(((obj_t) BgL_arg1404z00_3358));
						}}
					else
						{	/* Unsafe/gunzip.scm 601 */
							BFALSE;
						}
				}
				{	/* Unsafe/gunzip.scm 605 */
					long BgL_arg1529z00_1574;

					{	/* Unsafe/gunzip.scm 605 */
						BgL_huftz00_bglt BgL_i1113z00_1575;

						BgL_i1113z00_1575 =
							((BgL_huftz00_bglt) ((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4765)));
						BgL_arg1529z00_1574 =
							(((BgL_huftz00_bglt) COBJECT(BgL_i1113z00_1575))->BgL_bz00);
					}
					{	/* Unsafe/gunzip.scm 370 */
						obj_t BgL_auxz00_4524;

						{	/* Unsafe/gunzip.scm 370 */
							long BgL_xz00_3369;

							BgL_xz00_3369 = (long) CINT(CELL_REF(BgL_bbz00_4766));
							BgL_auxz00_4524 =
								BINT((BgL_xz00_3369 >> (int) (BgL_arg1529z00_1574)));
						}
						CELL_SET(BgL_bbz00_4766, BgL_auxz00_4524);
					}
					{	/* Unsafe/gunzip.scm 371 */
						obj_t BgL_auxz00_4525;

						BgL_auxz00_4525 =
							SUBFX(CELL_REF(BgL_bkz00_4767), BINT(BgL_arg1529z00_1574));
						CELL_SET(BgL_bkz00_4767, BgL_auxz00_4525);
				}}
				{	/* Unsafe/gunzip.scm 606 */
					obj_t BgL_auxz00_4526;

					BgL_auxz00_4526 = SUBFX(CELL_REF(BgL_ez00_4763), BINT(16L));
					CELL_SET(BgL_ez00_4763, BgL_auxz00_4526);
				}
				BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4766, BgL_bkz00_4767,
					BgL_inputzd2portzd2_4764, CELL_REF(BgL_ez00_4763));
				{	/* Unsafe/gunzip.scm 608 */
					obj_t BgL_auxz00_4527;

					{	/* Unsafe/gunzip.scm 609 */
						obj_t BgL_arg1530z00_1576;
						long BgL_arg1531z00_1577;

						{	/* Unsafe/gunzip.scm 609 */
							BgL_huftz00_bglt BgL_i1115z00_1578;

							BgL_i1115z00_1578 =
								((BgL_huftz00_bglt)
								((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4765)));
							BgL_arg1530z00_1576 =
								(((BgL_huftz00_bglt) COBJECT(BgL_i1115z00_1578))->BgL_vz00);
						}
						{	/* Unsafe/gunzip.scm 609 */
							long BgL_arg1535z00_1579;

							{	/* Unsafe/gunzip.scm 609 */
								obj_t BgL_arg1536z00_1580;

								BgL_arg1536z00_1580 = BGl_vector2393z00zz__gunza7ipza7;
								{	/* Unsafe/gunzip.scm 609 */
									long BgL_kz00_3375;

									BgL_kz00_3375 = (long) CINT(CELL_REF(BgL_ez00_4763));
									BgL_arg1535z00_1579 =
										(long) CINT(VECTOR_REF(BgL_arg1536z00_1580, BgL_kz00_3375));
							}}
							{	/* Unsafe/gunzip.scm 609 */
								long BgL_xz00_3376;

								BgL_xz00_3376 = (long) CINT(CELL_REF(BgL_bbz00_4766));
								BgL_arg1531z00_1577 = (BgL_xz00_3376 & BgL_arg1535z00_1579);
						}}
						BgL_auxz00_4527 =
							VECTOR_REF(((obj_t) BgL_arg1530z00_1576), BgL_arg1531z00_1577);
					}
					CELL_SET(BgL_tz00_4765, BgL_auxz00_4527);
				}
				{	/* Unsafe/gunzip.scm 610 */
					obj_t BgL_auxz00_4528;

					{	/* Unsafe/gunzip.scm 610 */
						BgL_huftz00_bglt BgL_i1117z00_1581;

						BgL_i1117z00_1581 =
							((BgL_huftz00_bglt) ((BgL_huftz00_bglt) CELL_REF(BgL_tz00_4765)));
						BgL_auxz00_4528 =
							BINT((((BgL_huftz00_bglt) COBJECT(BgL_i1117z00_1581))->BgL_ez00));
					}
					CELL_SET(BgL_ez00_4763, BgL_auxz00_4528);
				}
				{	/* Unsafe/gunzip.scm 611 */
					bool_t BgL_test2611z00_6701;

					{	/* Unsafe/gunzip.scm 611 */
						long BgL_n1z00_3380;

						BgL_n1z00_3380 = (long) CINT(CELL_REF(BgL_ez00_4763));
						BgL_test2611z00_6701 = (BgL_n1z00_3380 > 16L);
					}
					if (BgL_test2611z00_6701)
						{	/* Unsafe/gunzip.scm 611 */
							goto BgL_zc3z04anonymousza31525ze3z87_1570;
						}
					else
						{	/* Unsafe/gunzip.scm 611 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* &check-flush */
	obj_t BGl_z62checkzd2flushzb0zz__gunza7ipza7(long BgL_wsiza7eza7_4530,
		obj_t BgL_wpz00_4529)
	{
		{	/* Unsafe/gunzip.scm 384 */
			{	/* Unsafe/gunzip.scm 380 */
				bool_t BgL_test2612z00_6704;

				{	/* Unsafe/gunzip.scm 380 */
					long BgL_n1z00_3022;

					BgL_n1z00_3022 = (long) CINT(CELL_REF(BgL_wpz00_4529));
					BgL_test2612z00_6704 = (BgL_n1z00_3022 == BgL_wsiza7eza7_4530);
				}
				if (BgL_test2612z00_6704)
					{	/* Unsafe/gunzip.scm 380 */
						{	/* Unsafe/gunzip.scm 382 */
							obj_t BgL_auxz00_4531;

							BgL_auxz00_4531 = BINT(0L);
							CELL_SET(BgL_wpz00_4529, BgL_auxz00_4531);
						}
						return BINT(BgL_wsiza7eza7_4530);
					}
				else
					{	/* Unsafe/gunzip.scm 380 */
						return BINT(0L);
					}
			}
		}

	}



/* &loop2314 */
	obj_t BGl_z62loop2314z62zz__gunza7ipza7(obj_t BgL_bkz00_4537,
		obj_t BgL_wpz00_4536, obj_t BgL_slidez00_4535, obj_t BgL_bbz00_4534,
		obj_t BgL_inputzd2portzd2_4533, long BgL_wsiza7eza7_4532,
		long BgL_nz00_1534)
	{
		{	/* Unsafe/gunzip.scm 726 */
		BGl_z62loop2314z62zz__gunza7ipza7:
			if ((BgL_nz00_1534 > 0L))
				{	/* Unsafe/gunzip.scm 727 */
					BGl_z62NEEDBITSz62zz__gunza7ipza7(BgL_bbz00_4534, BgL_bkz00_4537,
						BgL_inputzd2portzd2_4533, BINT(8L));
					{	/* Unsafe/gunzip.scm 730 */
						unsigned char BgL_arg1516z00_1537;

						{	/* Unsafe/gunzip.scm 730 */
							long BgL_tmpz00_6714;

							{	/* Unsafe/gunzip.scm 730 */
								long BgL_xz00_3501;

								BgL_xz00_3501 = (long) CINT(CELL_REF(BgL_bbz00_4534));
								BgL_tmpz00_6714 = (BgL_xz00_3501 & 255L);
							}
							BgL_arg1516z00_1537 = (BgL_tmpz00_6714);
						}
						{	/* Unsafe/gunzip.scm 730 */
							long BgL_kz00_3504;

							BgL_kz00_3504 = (long) CINT(CELL_REF(BgL_wpz00_4536));
							{	/* Unsafe/gunzip.scm 730 */
								obj_t BgL_tmpz00_6719;

								BgL_tmpz00_6719 = ((obj_t) BgL_slidez00_4535);
								STRING_SET(BgL_tmpz00_6719, BgL_kz00_3504, BgL_arg1516z00_1537);
					}}}
					{	/* Unsafe/gunzip.scm 731 */
						obj_t BgL_auxz00_4538;

						BgL_auxz00_4538 = ADDFX(CELL_REF(BgL_wpz00_4536), BINT(1L));
						CELL_SET(BgL_wpz00_4536, BgL_auxz00_4538);
					}
					{	/* Unsafe/gunzip.scm 732 */
						obj_t BgL_rz00_1539;

						BgL_rz00_1539 =
							BGl_z62checkzd2flushzb0zz__gunza7ipza7(BgL_wsiza7eza7_4532,
							BgL_wpz00_4536);
						{	/* Unsafe/gunzip.scm 370 */
							obj_t BgL_auxz00_4539;

							{	/* Unsafe/gunzip.scm 370 */
								long BgL_xz00_3507;

								BgL_xz00_3507 = (long) CINT(CELL_REF(BgL_bbz00_4534));
								BgL_auxz00_4539 = BINT((BgL_xz00_3507 >> (int) (8L)));
							}
							CELL_SET(BgL_bbz00_4534, BgL_auxz00_4539);
						}
						{	/* Unsafe/gunzip.scm 371 */
							obj_t BgL_auxz00_4540;

							BgL_auxz00_4540 = SUBFX(CELL_REF(BgL_bkz00_4537), BINT(8L));
							CELL_SET(BgL_bkz00_4537, BgL_auxz00_4540);
						}
						if (((long) CINT(BgL_rz00_1539) > 0L))
							{	/* Unsafe/gunzip.scm 735 */
								obj_t BgL_val0_1235z00_1541;

								BgL_val0_1235z00_1541 = BGl_symbol2372z00zz__gunza7ipza7;
								{	/* Unsafe/gunzip.scm 735 */
									obj_t BgL_zc3z04anonymousza31519ze3z87_4420;

									BgL_zc3z04anonymousza31519ze3z87_4420 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31519ze3ze5zz__gunza7ipza7,
										(int) (0L), (int) (7L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (0L), BINT(BgL_wsiza7eza7_4532));
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (1L), BgL_inputzd2portzd2_4533);
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (2L), ((obj_t) BgL_bbz00_4534));
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (3L), BgL_slidez00_4535);
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (4L), ((obj_t) BgL_wpz00_4536));
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (5L), ((obj_t) BgL_bkz00_4537));
									PROCEDURE_SET(BgL_zc3z04anonymousza31519ze3z87_4420,
										(int) (6L), BINT(BgL_nz00_1534));
									{	/* Unsafe/gunzip.scm 735 */
										int BgL_tmpz00_6756;

										BgL_tmpz00_6756 = (int) (3L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6756);
									}
									{	/* Unsafe/gunzip.scm 735 */
										int BgL_tmpz00_6759;

										BgL_tmpz00_6759 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_6759, BgL_rz00_1539);
									}
									{	/* Unsafe/gunzip.scm 735 */
										int BgL_tmpz00_6762;

										BgL_tmpz00_6762 = (int) (2L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_6762,
											BgL_zc3z04anonymousza31519ze3z87_4420);
									}
									return BgL_val0_1235z00_1541;
								}
							}
						else
							{	/* Unsafe/gunzip.scm 736 */
								long BgL_arg1522z00_1547;

								BgL_arg1522z00_1547 = (BgL_nz00_1534 - 1L);
								{
									long BgL_nz00_6766;

									BgL_nz00_6766 = BgL_arg1522z00_1547;
									BgL_nz00_1534 = BgL_nz00_6766;
									goto BGl_z62loop2314z62zz__gunza7ipza7;
								}
							}
					}
				}
			else
				{	/* Unsafe/gunzip.scm 737 */
					obj_t BgL_val0_1238z00_1548;

					BgL_val0_1238z00_1548 = BGl_symbol2368z00zz__gunza7ipza7;
					{	/* Unsafe/gunzip.scm 737 */
						int BgL_tmpz00_6767;

						BgL_tmpz00_6767 = (int) (3L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6767);
					}
					{	/* Unsafe/gunzip.scm 737 */
						int BgL_tmpz00_6770;

						BgL_tmpz00_6770 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_6770, BTRUE);
					}
					{	/* Unsafe/gunzip.scm 737 */
						int BgL_tmpz00_6773;

						BgL_tmpz00_6773 = (int) (2L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_6773, BUNSPEC);
					}
					return BgL_val0_1238z00_1548;
				}
		}

	}



/* &%do-copy2 */
	obj_t BGl_z62z52dozd2copy2ze2zz__gunza7ipza7(obj_t BgL_slidez00_4556,
		obj_t BgL_ez00_4555, obj_t BgL_wpz00_4554, obj_t BgL_nz00_4553,
		obj_t BgL_dz00_4552, long BgL_wsiza7eza7_4551, obj_t BgL_tdz00_4550,
		long BgL_mdz00_4549, obj_t BgL_bdz00_4548, obj_t BgL_tlz00_4547,
		long BgL_mlz00_4546, obj_t BgL_blz00_4545, obj_t BgL_bkz00_4544,
		obj_t BgL_bbz00_4543, obj_t BgL_tz00_4542, obj_t BgL_inputzd2portzd2_4541)
	{
		{	/* Unsafe/gunzip.scm 640 */
		BGl_z62z52dozd2copy2ze2zz__gunza7ipza7:
			{	/* Unsafe/gunzip.scm 640 */
				obj_t BgL_auxz00_4557;

				{	/* Unsafe/gunzip.scm 640 */
					long BgL_xz00_3404;

					BgL_xz00_3404 = (long) CINT(CELL_REF(BgL_dz00_4552));
					BgL_auxz00_4557 = BINT((BgL_xz00_3404 & (BgL_wsiza7eza7_4551 - 1L)));
				}
				CELL_SET(BgL_dz00_4552, BgL_auxz00_4557);
			}
			{	/* Unsafe/gunzip.scm 641 */
				obj_t BgL_auxz00_4558;

				{	/* Unsafe/gunzip.scm 641 */
					obj_t BgL_az00_1608;
					long BgL_bz00_1609;

					BgL_az00_1608 = CELL_REF(BgL_nz00_4553);
					{	/* Unsafe/gunzip.scm 641 */
						obj_t BgL_arg1554z00_1611;

						{	/* Unsafe/gunzip.scm 641 */
							obj_t BgL_az00_1612;
							obj_t BgL_bz00_1613;

							BgL_az00_1612 = CELL_REF(BgL_dz00_4552);
							BgL_bz00_1613 = CELL_REF(BgL_wpz00_4554);
							if (((long) CINT(BgL_az00_1612) > (long) CINT(BgL_bz00_1613)))
								{	/* Unsafe/gunzip.scm 641 */
									BgL_arg1554z00_1611 = BgL_az00_1612;
								}
							else
								{	/* Unsafe/gunzip.scm 641 */
									BgL_arg1554z00_1611 = BgL_bz00_1613;
								}
						}
						BgL_bz00_1609 =
							(BgL_wsiza7eza7_4551 - (long) CINT(BgL_arg1554z00_1611));
					}
					if (((long) CINT(BgL_az00_1608) < BgL_bz00_1609))
						{	/* Unsafe/gunzip.scm 641 */
							BgL_auxz00_4558 = BgL_az00_1608;
						}
					else
						{	/* Unsafe/gunzip.scm 641 */
							BgL_auxz00_4558 = BINT(BgL_bz00_1609);
						}
				}
				CELL_SET(BgL_ez00_4555, BgL_auxz00_4558);
			}
			{	/* Unsafe/gunzip.scm 642 */
				obj_t BgL_auxz00_4559;

				BgL_auxz00_4559 =
					SUBFX(CELL_REF(BgL_nz00_4553), CELL_REF(BgL_ez00_4555));
				CELL_SET(BgL_nz00_4553, BgL_auxz00_4559);
			}
			{

			BgL_zc3z04anonymousza31556ze3z87_1616:
				{	/* Unsafe/gunzip.scm 644 */
					unsigned char BgL_arg1557z00_1617;

					{	/* Unsafe/gunzip.scm 644 */
						long BgL_kz00_3415;

						BgL_kz00_3415 = (long) CINT(CELL_REF(BgL_dz00_4552));
						BgL_arg1557z00_1617 =
							STRING_REF(((obj_t) BgL_slidez00_4556), BgL_kz00_3415);
					}
					{	/* Unsafe/gunzip.scm 644 */
						long BgL_kz00_3417;

						BgL_kz00_3417 = (long) CINT(CELL_REF(BgL_wpz00_4554));
						{	/* Unsafe/gunzip.scm 644 */
							obj_t BgL_tmpz00_6795;

							BgL_tmpz00_6795 = ((obj_t) BgL_slidez00_4556);
							STRING_SET(BgL_tmpz00_6795, BgL_kz00_3417, BgL_arg1557z00_1617);
				}}}
				{	/* Unsafe/gunzip.scm 645 */
					obj_t BgL_auxz00_4560;

					BgL_auxz00_4560 = ADDFX(CELL_REF(BgL_wpz00_4554), BINT(1L));
					CELL_SET(BgL_wpz00_4554, BgL_auxz00_4560);
				}
				{	/* Unsafe/gunzip.scm 646 */
					obj_t BgL_auxz00_4561;

					BgL_auxz00_4561 = ADDFX(CELL_REF(BgL_dz00_4552), BINT(1L));
					CELL_SET(BgL_dz00_4552, BgL_auxz00_4561);
				}
				{	/* Unsafe/gunzip.scm 647 */
					obj_t BgL_auxz00_4562;

					BgL_auxz00_4562 = SUBFX(CELL_REF(BgL_ez00_4555), BINT(1L));
					CELL_SET(BgL_ez00_4555, BgL_auxz00_4562);
				}
				{	/* Unsafe/gunzip.scm 648 */
					bool_t BgL_test2617z00_6804;

					{	/* Unsafe/gunzip.scm 648 */
						long BgL_n1z00_3422;

						BgL_n1z00_3422 = (long) CINT(CELL_REF(BgL_ez00_4555));
						BgL_test2617z00_6804 = (BgL_n1z00_3422 == 0L);
					}
					if (BgL_test2617z00_6804)
						{	/* Unsafe/gunzip.scm 648 */
							((bool_t) 0);
						}
					else
						{	/* Unsafe/gunzip.scm 648 */
							goto BgL_zc3z04anonymousza31556ze3z87_1616;
						}
				}
			}
			{	/* Unsafe/gunzip.scm 649 */
				obj_t BgL_rz00_1620;

				BgL_rz00_1620 =
					BGl_z62checkzd2flushzb0zz__gunza7ipza7(BgL_wsiza7eza7_4551,
					BgL_wpz00_4554);
				{	/* Unsafe/gunzip.scm 651 */
					bool_t BgL_test2618z00_6808;

					{	/* Unsafe/gunzip.scm 651 */
						long BgL_n1z00_3423;

						BgL_n1z00_3423 = (long) CINT(CELL_REF(BgL_nz00_4553));
						BgL_test2618z00_6808 = (BgL_n1z00_3423 == 0L);
					}
					if (BgL_test2618z00_6808)
						{	/* Unsafe/gunzip.scm 651 */
							return
								BGl_z62loopzd2inflatezb0zz__gunza7ipza7(BgL_tdz00_4550,
								BgL_mdz00_4549, BgL_bdz00_4548, BgL_tlz00_4547, BgL_mlz00_4546,
								BgL_blz00_4545, BgL_bkz00_4544, BgL_bbz00_4543, BgL_tz00_4542,
								BgL_inputzd2portzd2_4541, BgL_slidez00_4556, BgL_ez00_4555,
								BgL_wpz00_4554, BgL_nz00_4553, BgL_dz00_4552,
								BgL_wsiza7eza7_4551, BgL_rz00_1620);
						}
					else
						{	/* Unsafe/gunzip.scm 651 */
							if (((long) CINT(BgL_rz00_1620) == 0L))
								{

									goto BGl_z62z52dozd2copy2ze2zz__gunza7ipza7;
								}
							else
								{	/* Unsafe/gunzip.scm 656 */
									obj_t BgL_val0_1226z00_1623;

									BgL_val0_1226z00_1623 = BGl_symbol2372z00zz__gunza7ipza7;
									{	/* Unsafe/gunzip.scm 656 */
										obj_t BgL_zc3z04anonymousza31561ze3z87_4423;

										BgL_zc3z04anonymousza31561ze3z87_4423 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31561ze3ze5zz__gunza7ipza7,
											(int) (0L), (int) (16L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (0L), BgL_inputzd2portzd2_4541);
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (1L), ((obj_t) BgL_tz00_4542));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (2L), ((obj_t) BgL_bbz00_4543));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (3L), ((obj_t) BgL_bkz00_4544));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (4L), BgL_blz00_4545);
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (5L), BINT(BgL_mlz00_4546));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (6L), BgL_tlz00_4547);
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (7L), BgL_bdz00_4548);
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (8L), BINT(BgL_mdz00_4549));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (9L), BgL_tdz00_4550);
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (10L), BINT(BgL_wsiza7eza7_4551));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (11L), ((obj_t) BgL_dz00_4552));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (12L), ((obj_t) BgL_nz00_4553));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (13L), ((obj_t) BgL_wpz00_4554));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (14L), ((obj_t) BgL_ez00_4555));
										PROCEDURE_SET(BgL_zc3z04anonymousza31561ze3z87_4423,
											(int) (15L), BgL_slidez00_4556);
										{	/* Unsafe/gunzip.scm 656 */
											int BgL_tmpz00_6860;

											BgL_tmpz00_6860 = (int) (3L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_6860);
										}
										{	/* Unsafe/gunzip.scm 656 */
											int BgL_tmpz00_6863;

											BgL_tmpz00_6863 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_6863, BgL_rz00_1620);
										}
										{	/* Unsafe/gunzip.scm 656 */
											int BgL_tmpz00_6866;

											BgL_tmpz00_6866 = (int) (2L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_6866,
												BgL_zc3z04anonymousza31561ze3z87_4423);
										}
										return BgL_val0_1226z00_1623;
									}
								}
						}
				}
			}
		}

	}



/* &<@anonymous:1415> */
	obj_t BGl_z62zc3z04anonymousza31415ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4563)
	{
		{	/* Unsafe/gunzip.scm 911 */
			{	/* Unsafe/gunzip.scm 912 */
				long BgL_wsiza7eza7_4564;
				obj_t BgL_slidez00_4565;
				obj_t BgL_bbz00_4566;
				obj_t BgL_bkz00_4567;
				obj_t BgL_inputzd2portzd2_4568;
				obj_t BgL_wpz00_4569;
				obj_t BgL_hz00_4570;
				long BgL_huftsz00_4571;
				obj_t BgL_rz00_4572;

				BgL_wsiza7eza7_4564 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4563, (int) (0L)));
				BgL_slidez00_4565 = PROCEDURE_REF(BgL_envz00_4563, (int) (1L));
				BgL_bbz00_4566 = PROCEDURE_REF(BgL_envz00_4563, (int) (2L));
				BgL_bkz00_4567 = PROCEDURE_REF(BgL_envz00_4563, (int) (3L));
				BgL_inputzd2portzd2_4568 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4563, (int) (4L)));
				BgL_wpz00_4569 = PROCEDURE_REF(BgL_envz00_4563, (int) (5L));
				BgL_hz00_4570 = PROCEDURE_REF(BgL_envz00_4563, (int) (6L));
				BgL_huftsz00_4571 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4563, (int) (7L)));
				BgL_rz00_4572 = PROCEDURE_REF(BgL_envz00_4563, (int) (8L));
				{	/* Unsafe/gunzip.scm 912 */
					obj_t BgL_state2z00_4849;

					BgL_state2z00_4849 = BGL_PROCEDURE_CALL0(BgL_rz00_4572);
					{	/* Unsafe/gunzip.scm 913 */
						obj_t BgL_e2z00_4850;
						obj_t BgL_r2z00_4851;

						{	/* Unsafe/gunzip.scm 914 */
							obj_t BgL_tmpz00_4852;

							{	/* Unsafe/gunzip.scm 914 */
								int BgL_tmpz00_6893;

								BgL_tmpz00_6893 = (int) (1L);
								BgL_tmpz00_4852 = BGL_MVALUES_VAL(BgL_tmpz00_6893);
							}
							{	/* Unsafe/gunzip.scm 914 */
								int BgL_tmpz00_6896;

								BgL_tmpz00_6896 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6896, BUNSPEC);
							}
							BgL_e2z00_4850 = BgL_tmpz00_4852;
						}
						{	/* Unsafe/gunzip.scm 914 */
							obj_t BgL_tmpz00_4853;

							{	/* Unsafe/gunzip.scm 914 */
								int BgL_tmpz00_6899;

								BgL_tmpz00_6899 = (int) (2L);
								BgL_tmpz00_4853 = BGL_MVALUES_VAL(BgL_tmpz00_6899);
							}
							{	/* Unsafe/gunzip.scm 914 */
								int BgL_tmpz00_6902;

								BgL_tmpz00_6902 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6902, BUNSPEC);
							}
							BgL_r2z00_4851 = BgL_tmpz00_4853;
						}
						return
							BGl_z62laapz62zz__gunza7ipza7(BgL_huftsz00_4571, BgL_hz00_4570,
							BgL_wpz00_4569, BgL_inputzd2portzd2_4568, BgL_bkz00_4567,
							BgL_bbz00_4566, BgL_slidez00_4565, BgL_wsiza7eza7_4564,
							BgL_state2z00_4849, BgL_e2z00_4850, BgL_r2z00_4851);
					}
				}
			}
		}

	}



/* &<@anonymous:1420> */
	obj_t BGl_z62zc3z04anonymousza31420ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4573)
	{
		{	/* Unsafe/gunzip.scm 880 */
			{	/* Unsafe/gunzip.scm 881 */
				long BgL_ez00_4574;
				obj_t BgL_kontz00_4575;

				BgL_ez00_4574 = (long) CINT(PROCEDURE_REF(BgL_envz00_4573, (int) (0L)));
				BgL_kontz00_4575 = PROCEDURE_REF(BgL_envz00_4573, (int) (1L));
				{	/* Unsafe/gunzip.scm 881 */
					obj_t BgL_state2z00_4854;

					BgL_state2z00_4854 = BGL_PROCEDURE_CALL0(BgL_kontz00_4575);
					{	/* Unsafe/gunzip.scm 882 */
						obj_t BgL_val2z00_4855;
						obj_t BgL_kont2z00_4856;

						{	/* Unsafe/gunzip.scm 883 */
							obj_t BgL_tmpz00_4857;

							{	/* Unsafe/gunzip.scm 883 */
								int BgL_tmpz00_6914;

								BgL_tmpz00_6914 = (int) (1L);
								BgL_tmpz00_4857 = BGL_MVALUES_VAL(BgL_tmpz00_6914);
							}
							{	/* Unsafe/gunzip.scm 883 */
								int BgL_tmpz00_6917;

								BgL_tmpz00_6917 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6917, BUNSPEC);
							}
							BgL_val2z00_4855 = BgL_tmpz00_4857;
						}
						{	/* Unsafe/gunzip.scm 883 */
							obj_t BgL_tmpz00_4858;

							{	/* Unsafe/gunzip.scm 883 */
								int BgL_tmpz00_6920;

								BgL_tmpz00_6920 = (int) (2L);
								BgL_tmpz00_4858 = BGL_MVALUES_VAL(BgL_tmpz00_6920);
							}
							{	/* Unsafe/gunzip.scm 883 */
								int BgL_tmpz00_6923;

								BgL_tmpz00_6923 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_6923, BUNSPEC);
							}
							BgL_kont2z00_4856 = BgL_tmpz00_4858;
						}
						return
							BGl_z62loop2313z62zz__gunza7ipza7(BgL_ez00_4574,
							BgL_state2z00_4854, BgL_val2z00_4855, BgL_kont2z00_4856);
					}
				}
			}
		}

	}



/* &<@anonymous:1519> */
	obj_t BGl_z62zc3z04anonymousza31519ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4576)
	{
		{	/* Unsafe/gunzip.scm 735 */
			{	/* Unsafe/gunzip.scm 735 */
				long BgL_wsiza7eza7_4577;
				obj_t BgL_inputzd2portzd2_4578;
				obj_t BgL_bbz00_4579;
				obj_t BgL_slidez00_4580;
				obj_t BgL_wpz00_4581;
				obj_t BgL_bkz00_4582;
				long BgL_nz00_4583;

				BgL_wsiza7eza7_4577 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4576, (int) (0L)));
				BgL_inputzd2portzd2_4578 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4576, (int) (1L)));
				BgL_bbz00_4579 = PROCEDURE_REF(BgL_envz00_4576, (int) (2L));
				BgL_slidez00_4580 = PROCEDURE_REF(BgL_envz00_4576, (int) (3L));
				BgL_wpz00_4581 = PROCEDURE_REF(BgL_envz00_4576, (int) (4L));
				BgL_bkz00_4582 = PROCEDURE_REF(BgL_envz00_4576, (int) (5L));
				BgL_nz00_4583 = (long) CINT(PROCEDURE_REF(BgL_envz00_4576, (int) (6L)));
				return
					BGl_z62loop2314z62zz__gunza7ipza7(BgL_bkz00_4582, BgL_wpz00_4581,
					BgL_slidez00_4580, BgL_bbz00_4579, BgL_inputzd2portzd2_4578,
					BgL_wsiza7eza7_4577, (BgL_nz00_4583 - 1L));
			}
		}

	}



/* &<@anonymous:1564> */
	obj_t BGl_z62zc3z04anonymousza31564ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4584)
	{
		{	/* Unsafe/gunzip.scm 660 */
			{	/* Unsafe/gunzip.scm 660 */
				long BgL_wsiza7eza7_4585;
				obj_t BgL_dz00_4586;
				obj_t BgL_nz00_4587;
				obj_t BgL_wpz00_4588;
				obj_t BgL_ez00_4589;
				obj_t BgL_slidez00_4590;
				obj_t BgL_inputzd2portzd2_4591;
				obj_t BgL_tz00_4592;
				obj_t BgL_bbz00_4593;
				obj_t BgL_bkz00_4594;
				obj_t BgL_blz00_4595;
				long BgL_mlz00_4596;
				obj_t BgL_tlz00_4597;
				obj_t BgL_bdz00_4598;
				long BgL_mdz00_4599;
				obj_t BgL_tdz00_4600;

				BgL_wsiza7eza7_4585 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4584, (int) (0L)));
				BgL_dz00_4586 = PROCEDURE_REF(BgL_envz00_4584, (int) (1L));
				BgL_nz00_4587 = PROCEDURE_REF(BgL_envz00_4584, (int) (2L));
				BgL_wpz00_4588 = PROCEDURE_REF(BgL_envz00_4584, (int) (3L));
				BgL_ez00_4589 = PROCEDURE_REF(BgL_envz00_4584, (int) (4L));
				BgL_slidez00_4590 = PROCEDURE_REF(BgL_envz00_4584, (int) (5L));
				BgL_inputzd2portzd2_4591 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4584, (int) (6L)));
				BgL_tz00_4592 = PROCEDURE_REF(BgL_envz00_4584, (int) (7L));
				BgL_bbz00_4593 = PROCEDURE_REF(BgL_envz00_4584, (int) (8L));
				BgL_bkz00_4594 = PROCEDURE_REF(BgL_envz00_4584, (int) (9L));
				BgL_blz00_4595 = PROCEDURE_REF(BgL_envz00_4584, (int) (10L));
				BgL_mlz00_4596 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4584, (int) (11L)));
				BgL_tlz00_4597 = PROCEDURE_REF(BgL_envz00_4584, (int) (12L));
				BgL_bdz00_4598 = PROCEDURE_REF(BgL_envz00_4584, (int) (13L));
				BgL_mdz00_4599 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4584, (int) (14L)));
				BgL_tdz00_4600 = PROCEDURE_REF(BgL_envz00_4584, (int) (15L));
				return
					BGl_z62loopzd2inflatezb0zz__gunza7ipza7(BgL_tdz00_4600,
					BgL_mdz00_4599, BgL_bdz00_4598, BgL_tlz00_4597, BgL_mlz00_4596,
					BgL_blz00_4595, BgL_bkz00_4594, BgL_bbz00_4593, BgL_tz00_4592,
					BgL_inputzd2portzd2_4591, BgL_slidez00_4590, BgL_ez00_4589,
					BgL_wpz00_4588, BgL_nz00_4587, BgL_dz00_4586, BgL_wsiza7eza7_4585,
					BINT(0L));
			}
		}

	}



/* &<@anonymous:1561> */
	obj_t BGl_z62zc3z04anonymousza31561ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4601)
	{
		{	/* Unsafe/gunzip.scm 656 */
			{	/* Unsafe/gunzip.scm 656 */
				obj_t BgL_inputzd2portzd2_4602;
				obj_t BgL_tz00_4603;
				obj_t BgL_bbz00_4604;
				obj_t BgL_bkz00_4605;
				obj_t BgL_blz00_4606;
				long BgL_mlz00_4607;
				obj_t BgL_tlz00_4608;
				obj_t BgL_bdz00_4609;
				long BgL_mdz00_4610;
				obj_t BgL_tdz00_4611;
				long BgL_wsiza7eza7_4612;
				obj_t BgL_dz00_4613;
				obj_t BgL_nz00_4614;
				obj_t BgL_wpz00_4615;
				obj_t BgL_ez00_4616;
				obj_t BgL_slidez00_4617;

				BgL_inputzd2portzd2_4602 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4601, (int) (0L)));
				BgL_tz00_4603 = PROCEDURE_REF(BgL_envz00_4601, (int) (1L));
				BgL_bbz00_4604 = PROCEDURE_REF(BgL_envz00_4601, (int) (2L));
				BgL_bkz00_4605 = PROCEDURE_REF(BgL_envz00_4601, (int) (3L));
				BgL_blz00_4606 = PROCEDURE_REF(BgL_envz00_4601, (int) (4L));
				BgL_mlz00_4607 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4601, (int) (5L)));
				BgL_tlz00_4608 = PROCEDURE_REF(BgL_envz00_4601, (int) (6L));
				BgL_bdz00_4609 = PROCEDURE_REF(BgL_envz00_4601, (int) (7L));
				BgL_mdz00_4610 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4601, (int) (8L)));
				BgL_tdz00_4611 = PROCEDURE_REF(BgL_envz00_4601, (int) (9L));
				BgL_wsiza7eza7_4612 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4601, (int) (10L)));
				BgL_dz00_4613 = PROCEDURE_REF(BgL_envz00_4601, (int) (11L));
				BgL_nz00_4614 = PROCEDURE_REF(BgL_envz00_4601, (int) (12L));
				BgL_wpz00_4615 = PROCEDURE_REF(BgL_envz00_4601, (int) (13L));
				BgL_ez00_4616 = PROCEDURE_REF(BgL_envz00_4601, (int) (14L));
				BgL_slidez00_4617 = PROCEDURE_REF(BgL_envz00_4601, (int) (15L));
				return
					BGl_z62z52dozd2copy2ze2zz__gunza7ipza7(BgL_slidez00_4617,
					BgL_ez00_4616, BgL_wpz00_4615, BgL_nz00_4614, BgL_dz00_4613,
					BgL_wsiza7eza7_4612, BgL_tdz00_4611, BgL_mdz00_4610, BgL_bdz00_4609,
					BgL_tlz00_4608, BgL_mlz00_4607, BgL_blz00_4606, BgL_bkz00_4605,
					BgL_bbz00_4604, BgL_tz00_4603, BgL_inputzd2portzd2_4602);
			}
		}

	}



/* &<@anonymous:1550> */
	obj_t BGl_z62zc3z04anonymousza31550ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4618)
	{
		{	/* Unsafe/gunzip.scm 631 */
			{	/* Unsafe/gunzip.scm 631 */
				obj_t BgL_inputzd2portzd2_4619;
				obj_t BgL_tz00_4620;
				obj_t BgL_bbz00_4621;
				obj_t BgL_bkz00_4622;
				obj_t BgL_blz00_4623;
				long BgL_mlz00_4624;
				obj_t BgL_tlz00_4625;
				obj_t BgL_bdz00_4626;
				long BgL_mdz00_4627;
				obj_t BgL_tdz00_4628;
				long BgL_wsiza7eza7_4629;
				obj_t BgL_dz00_4630;
				obj_t BgL_nz00_4631;
				obj_t BgL_wpz00_4632;
				obj_t BgL_ez00_4633;
				obj_t BgL_slidez00_4634;

				BgL_inputzd2portzd2_4619 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4618, (int) (0L)));
				BgL_tz00_4620 = PROCEDURE_REF(BgL_envz00_4618, (int) (1L));
				BgL_bbz00_4621 = PROCEDURE_REF(BgL_envz00_4618, (int) (2L));
				BgL_bkz00_4622 = PROCEDURE_REF(BgL_envz00_4618, (int) (3L));
				BgL_blz00_4623 = PROCEDURE_REF(BgL_envz00_4618, (int) (4L));
				BgL_mlz00_4624 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4618, (int) (5L)));
				BgL_tlz00_4625 = PROCEDURE_REF(BgL_envz00_4618, (int) (6L));
				BgL_bdz00_4626 = PROCEDURE_REF(BgL_envz00_4618, (int) (7L));
				BgL_mdz00_4627 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4618, (int) (8L)));
				BgL_tdz00_4628 = PROCEDURE_REF(BgL_envz00_4618, (int) (9L));
				BgL_wsiza7eza7_4629 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4618, (int) (10L)));
				BgL_dz00_4630 = PROCEDURE_REF(BgL_envz00_4618, (int) (11L));
				BgL_nz00_4631 = PROCEDURE_REF(BgL_envz00_4618, (int) (12L));
				BgL_wpz00_4632 = PROCEDURE_REF(BgL_envz00_4618, (int) (13L));
				BgL_ez00_4633 = PROCEDURE_REF(BgL_envz00_4618, (int) (14L));
				BgL_slidez00_4634 = PROCEDURE_REF(BgL_envz00_4618, (int) (15L));
				return
					BGl_z62z52dozd2copy2ze2zz__gunza7ipza7(BgL_slidez00_4634,
					BgL_ez00_4633, BgL_wpz00_4632, BgL_nz00_4631, BgL_dz00_4630,
					BgL_wsiza7eza7_4629, BgL_tdz00_4628, BgL_mdz00_4627, BgL_bdz00_4626,
					BgL_tlz00_4625, BgL_mlz00_4624, BgL_blz00_4623, BgL_bkz00_4622,
					BgL_bbz00_4621, BgL_tz00_4620, BgL_inputzd2portzd2_4619);
			}
		}

	}



/* &<@anonymous:1640> */
	BgL_huftz00_bglt BGl_z62zc3z04anonymousza31640ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4635, long BgL_iz00_4636)
	{
		{	/* Unsafe/gunzip.scm 511 */
			{	/* Unsafe/gunzip.scm 512 */
				BgL_huftz00_bglt BgL_new1091z00_4860;

				{	/* Unsafe/gunzip.scm 513 */
					BgL_huftz00_bglt BgL_new1090z00_4861;

					BgL_new1090z00_4861 =
						((BgL_huftz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_huftz00_bgl))));
					{	/* Unsafe/gunzip.scm 513 */
						long BgL_arg1641z00_4862;

						BgL_arg1641z00_4862 = BGL_CLASS_NUM(BGl_huftz00zz__gunza7ipza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1090z00_4861), BgL_arg1641z00_4862);
					}
					BgL_new1091z00_4860 = BgL_new1090z00_4861;
				}
				((((BgL_huftz00_bglt) COBJECT(BgL_new1091z00_4860))->BgL_ez00) =
					((long) 0L), BUNSPEC);
				((((BgL_huftz00_bglt) COBJECT(BgL_new1091z00_4860))->BgL_bz00) =
					((long) 0L), BUNSPEC);
				((((BgL_huftz00_bglt) COBJECT(BgL_new1091z00_4860))->BgL_vz00) =
					((obj_t) BINT(0L)), BUNSPEC);
				return BgL_new1091z00_4860;
			}
		}

	}



/* gunzip-parse-header */
	BGL_EXPORTED_DEF obj_t BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(obj_t
		BgL_inz00_20)
	{
		{	/* Unsafe/gunzip.scm 939 */
			{	/* Unsafe/gunzip.scm 940 */
				obj_t BgL_bufz00_2151;

				{	/* Ieee/string.scm 172 */

					BgL_bufz00_2151 = make_string(4L, ((unsigned char) ' '));
				}
				{

					{	/* Unsafe/gunzip.scm 954 */
						obj_t BgL_headerz00_2155;

						BgL_headerz00_2155 =
							BGl_readzd2charszd2zz__r4_input_6_10_2z00(BINT(2L), BgL_inz00_20);
						{	/* Unsafe/gunzip.scm 955 */
							bool_t BgL_test2620z00_7069;

							if (STRINGP(BgL_headerz00_2155))
								{	/* Unsafe/gunzip.scm 955 */
									if ((STRING_LENGTH(BgL_headerz00_2155) == 2L))
										{	/* Unsafe/gunzip.scm 956 */
											if (((STRING_REF(BgL_headerz00_2155, 0L)) == 31L))
												{	/* Unsafe/gunzip.scm 957 */
													BgL_test2620z00_7069 =
														((STRING_REF(BgL_headerz00_2155, 1L)) == 139L);
												}
											else
												{	/* Unsafe/gunzip.scm 957 */
													BgL_test2620z00_7069 = ((bool_t) 0);
												}
										}
									else
										{	/* Unsafe/gunzip.scm 956 */
											BgL_test2620z00_7069 = ((bool_t) 0);
										}
								}
							else
								{	/* Unsafe/gunzip.scm 955 */
									BgL_test2620z00_7069 = ((bool_t) 0);
								}
							if (BgL_test2620z00_7069)
								{	/* Unsafe/gunzip.scm 955 */
									BFALSE;
								}
							else
								{	/* Unsafe/gunzip.scm 960 */
									obj_t BgL_arg1880z00_2168;

									{	/* Unsafe/gunzip.scm 960 */
										obj_t BgL_list1881z00_2169;

										BgL_list1881z00_2169 =
											MAKE_YOUNG_PAIR(BgL_headerz00_2155, BNIL);
										BgL_arg1880z00_2168 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string2404z00zz__gunza7ipza7, BgL_list1881z00_2169);
									}
									{	/* Unsafe/gunzip.scm 328 */
										BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3872;

										{	/* Unsafe/gunzip.scm 328 */
											BgL_z62iozd2parsezd2errorz62_bglt BgL_new1076z00_3873;

											{	/* Unsafe/gunzip.scm 328 */
												BgL_z62iozd2parsezd2errorz62_bglt BgL_new1075z00_3874;

												BgL_new1075z00_3874 =
													((BgL_z62iozd2parsezd2errorz62_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_z62iozd2parsezd2errorz62_bgl))));
												{	/* Unsafe/gunzip.scm 328 */
													long BgL_arg1407z00_3875;

													BgL_arg1407z00_3875 =
														BGL_CLASS_NUM
														(BGl_z62iozd2parsezd2errorz62zz__objectz00);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
															BgL_new1075z00_3874), BgL_arg1407z00_3875);
												}
												BgL_new1076z00_3873 = BgL_new1075z00_3874;
											}
											((((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																BgL_new1076z00_3873)))->BgL_fnamez00) =
												((obj_t) BFALSE), BUNSPEC);
											((((BgL_z62exceptionz62_bglt)
														COBJECT(((BgL_z62exceptionz62_bglt)
																BgL_new1076z00_3873)))->BgL_locationz00) =
												((obj_t) BFALSE), BUNSPEC);
											{
												obj_t BgL_auxz00_7092;

												{	/* Unsafe/gunzip.scm 328 */
													obj_t BgL_arg1405z00_3879;

													{	/* Unsafe/gunzip.scm 328 */
														obj_t BgL_arg1406z00_3880;

														{	/* Unsafe/gunzip.scm 328 */
															obj_t BgL_classz00_3881;

															BgL_classz00_3881 =
																BGl_z62iozd2parsezd2errorz62zz__objectz00;
															BgL_arg1406z00_3880 =
																BGL_CLASS_ALL_FIELDS(BgL_classz00_3881);
														}
														BgL_arg1405z00_3879 =
															VECTOR_REF(BgL_arg1406z00_3880, 2L);
													}
													BgL_auxz00_7092 =
														BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
														(BgL_arg1405z00_3879);
												}
												((((BgL_z62exceptionz62_bglt) COBJECT(
																((BgL_z62exceptionz62_bglt)
																	BgL_new1076z00_3873)))->BgL_stackz00) =
													((obj_t) BgL_auxz00_7092), BUNSPEC);
											}
											((((BgL_z62errorz62_bglt) COBJECT(
															((BgL_z62errorz62_bglt) BgL_new1076z00_3873)))->
													BgL_procz00) =
												((obj_t) BGl_string2405z00zz__gunza7ipza7), BUNSPEC);
											((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
																BgL_new1076z00_3873)))->BgL_msgz00) =
												((obj_t) BgL_arg1880z00_2168), BUNSPEC);
											((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
																BgL_new1076z00_3873)))->BgL_objz00) =
												((obj_t) BgL_inz00_20), BUNSPEC);
											BgL_arg1404z00_3872 = BgL_new1076z00_3873;
										}
										BGl_raisez00zz__errorz00(((obj_t) BgL_arg1404z00_3872));
					}}}}
					{	/* Unsafe/gunzip.scm 962 */
						obj_t BgL_compressionzd2typezd2_2178;

						BgL_compressionzd2typezd2_2178 =
							BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_inz00_20);
						if ((BgL_compressionzd2typezd2_2178 == BCHAR(((unsigned char) 8))))
							{	/* Unsafe/gunzip.scm 963 */
								BFALSE;
							}
						else
							{	/* Unsafe/gunzip.scm 965 */
								obj_t BgL_arg1888z00_2179;

								{	/* Unsafe/gunzip.scm 965 */
									obj_t BgL_list1889z00_2180;

									BgL_list1889z00_2180 =
										MAKE_YOUNG_PAIR(BgL_compressionzd2typezd2_2178, BNIL);
									BgL_arg1888z00_2179 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2406z00zz__gunza7ipza7, BgL_list1889z00_2180);
								}
								{	/* Unsafe/gunzip.scm 328 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1404z00_3883;

									{	/* Unsafe/gunzip.scm 328 */
										BgL_z62iozd2parsezd2errorz62_bglt BgL_new1076z00_3884;

										{	/* Unsafe/gunzip.scm 328 */
											BgL_z62iozd2parsezd2errorz62_bglt BgL_new1075z00_3885;

											BgL_new1075z00_3885 =
												((BgL_z62iozd2parsezd2errorz62_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_z62iozd2parsezd2errorz62_bgl))));
											{	/* Unsafe/gunzip.scm 328 */
												long BgL_arg1407z00_3886;

												BgL_arg1407z00_3886 =
													BGL_CLASS_NUM
													(BGl_z62iozd2parsezd2errorz62zz__objectz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1075z00_3885), BgL_arg1407z00_3886);
											}
											BgL_new1076z00_3884 = BgL_new1075z00_3885;
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1076z00_3884)))->
												BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_z62exceptionz62_bglt)
													COBJECT(((BgL_z62exceptionz62_bglt)
															BgL_new1076z00_3884)))->BgL_locationz00) =
											((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_7120;

											{	/* Unsafe/gunzip.scm 328 */
												obj_t BgL_arg1405z00_3890;

												{	/* Unsafe/gunzip.scm 328 */
													obj_t BgL_arg1406z00_3891;

													{	/* Unsafe/gunzip.scm 328 */
														obj_t BgL_classz00_3892;

														BgL_classz00_3892 =
															BGl_z62iozd2parsezd2errorz62zz__objectz00;
														BgL_arg1406z00_3891 =
															BGL_CLASS_ALL_FIELDS(BgL_classz00_3892);
													}
													BgL_arg1405z00_3890 =
														VECTOR_REF(BgL_arg1406z00_3891, 2L);
												}
												BgL_auxz00_7120 =
													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
													(BgL_arg1405z00_3890);
											}
											((((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																BgL_new1076z00_3884)))->BgL_stackz00) =
												((obj_t) BgL_auxz00_7120), BUNSPEC);
										}
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_new1076z00_3884)))->
												BgL_procz00) =
											((obj_t) BGl_string2405z00zz__gunza7ipza7), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1076z00_3884)))->BgL_msgz00) =
											((obj_t) BgL_arg1888z00_2179), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1076z00_3884)))->BgL_objz00) =
											((obj_t) BgL_inz00_20), BUNSPEC);
										BgL_arg1404z00_3883 = BgL_new1076z00_3884;
									}
									BGl_raisez00zz__errorz00(((obj_t) BgL_arg1404z00_3883));
					}}}
					{	/* Unsafe/gunzip.scm 968 */
						long BgL_flagsz00_2181;

						{	/* Unsafe/gunzip.scm 968 */
							obj_t BgL_arg1904z00_2214;

							BgL_arg1904z00_2214 =
								BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_inz00_20);
							BgL_flagsz00_2181 = (CCHAR(BgL_arg1904z00_2214));
						}
						{	/* Unsafe/gunzip.scm 969 */
							bool_t BgL_continuationzf3zf3_2183;

							BgL_continuationzf3zf3_2183 =
								BGl_positivezf3zf3zz__r4_numbers_6_5z00(BINT(
									(BgL_flagsz00_2181 & 2L)));
							{	/* Unsafe/gunzip.scm 970 */
								bool_t BgL_haszd2extrazd2fieldzf3zf3_2184;

								BgL_haszd2extrazd2fieldzf3zf3_2184 =
									BGl_positivezf3zf3zz__r4_numbers_6_5z00(BINT(
										(BgL_flagsz00_2181 & 4L)));
								{	/* Unsafe/gunzip.scm 971 */
									bool_t BgL_haszd2originalzd2filenamezf3zf3_2185;

									BgL_haszd2originalzd2filenamezf3zf3_2185 =
										BGl_positivezf3zf3zz__r4_numbers_6_5z00(BINT(
											(BgL_flagsz00_2181 & 8L)));
									{	/* Unsafe/gunzip.scm 972 */
										bool_t BgL_haszd2commentzf3z21_2186;

										BgL_haszd2commentzf3z21_2186 =
											BGl_positivezf3zf3zz__r4_numbers_6_5z00(BINT(
												(BgL_flagsz00_2181 & 16L)));
										{	/* Unsafe/gunzip.scm 973 */
											bool_t BgL_encryptedzf3zf3_2187;

											BgL_encryptedzf3zf3_2187 =
												BGl_positivezf3zf3zz__r4_numbers_6_5z00(BINT(
													(BgL_flagsz00_2181 & 32L)));
											{	/* Unsafe/gunzip.scm 974 */

												if (BgL_encryptedzf3zf3_2187)
													{	/* Unsafe/gunzip.scm 328 */
														BgL_z62iozd2parsezd2errorz62_bglt
															BgL_arg1404z00_3901;
														{	/* Unsafe/gunzip.scm 328 */
															BgL_z62iozd2parsezd2errorz62_bglt
																BgL_new1076z00_3902;
															{	/* Unsafe/gunzip.scm 328 */
																BgL_z62iozd2parsezd2errorz62_bglt
																	BgL_new1075z00_3903;
																BgL_new1075z00_3903 =
																	((BgL_z62iozd2parsezd2errorz62_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_z62iozd2parsezd2errorz62_bgl))));
																{	/* Unsafe/gunzip.scm 328 */
																	long BgL_arg1407z00_3904;

																	BgL_arg1407z00_3904 =
																		BGL_CLASS_NUM
																		(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_new1075z00_3903),
																		BgL_arg1407z00_3904);
																}
																BgL_new1076z00_3902 = BgL_new1075z00_3903;
															}
															((((BgL_z62exceptionz62_bglt) COBJECT(
																			((BgL_z62exceptionz62_bglt)
																				BgL_new1076z00_3902)))->BgL_fnamez00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_z62exceptionz62_bglt)
																		COBJECT(((BgL_z62exceptionz62_bglt)
																				BgL_new1076z00_3902)))->
																	BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
															{
																obj_t BgL_auxz00_7161;

																{	/* Unsafe/gunzip.scm 328 */
																	obj_t BgL_arg1405z00_3908;

																	{	/* Unsafe/gunzip.scm 328 */
																		obj_t BgL_arg1406z00_3909;

																		{	/* Unsafe/gunzip.scm 328 */
																			obj_t BgL_classz00_3910;

																			BgL_classz00_3910 =
																				BGl_z62iozd2parsezd2errorz62zz__objectz00;
																			BgL_arg1406z00_3909 =
																				BGL_CLASS_ALL_FIELDS(BgL_classz00_3910);
																		}
																		BgL_arg1405z00_3908 =
																			VECTOR_REF(BgL_arg1406z00_3909, 2L);
																	}
																	BgL_auxz00_7161 =
																		BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																		(BgL_arg1405z00_3908);
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1076z00_3902)))->
																		BgL_stackz00) =
																	((obj_t) BgL_auxz00_7161), BUNSPEC);
															}
															((((BgL_z62errorz62_bglt) COBJECT(
																			((BgL_z62errorz62_bglt)
																				BgL_new1076z00_3902)))->BgL_procz00) =
																((obj_t) BGl_string2405z00zz__gunza7ipza7),
																BUNSPEC);
															((((BgL_z62errorz62_bglt)
																		COBJECT(((BgL_z62errorz62_bglt)
																				BgL_new1076z00_3902)))->BgL_msgz00) =
																((obj_t) BGl_string2407z00zz__gunza7ipza7),
																BUNSPEC);
															((((BgL_z62errorz62_bglt)
																		COBJECT(((BgL_z62errorz62_bglt)
																				BgL_new1076z00_3902)))->BgL_objz00) =
																((obj_t) BgL_inz00_20), BUNSPEC);
															BgL_arg1404z00_3901 = BgL_new1076z00_3902;
														}
														BGl_raisez00zz__errorz00(
															((obj_t) BgL_arg1404z00_3901));
													}
												else
													{	/* Unsafe/gunzip.scm 975 */
														BFALSE;
													}
												if (BgL_continuationzf3zf3_2183)
													{	/* Unsafe/gunzip.scm 328 */
														BgL_z62iozd2parsezd2errorz62_bglt
															BgL_arg1404z00_3912;
														{	/* Unsafe/gunzip.scm 328 */
															BgL_z62iozd2parsezd2errorz62_bglt
																BgL_new1076z00_3913;
															{	/* Unsafe/gunzip.scm 328 */
																BgL_z62iozd2parsezd2errorz62_bglt
																	BgL_new1075z00_3914;
																BgL_new1075z00_3914 =
																	((BgL_z62iozd2parsezd2errorz62_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_z62iozd2parsezd2errorz62_bgl))));
																{	/* Unsafe/gunzip.scm 328 */
																	long BgL_arg1407z00_3915;

																	BgL_arg1407z00_3915 =
																		BGL_CLASS_NUM
																		(BGl_z62iozd2parsezd2errorz62zz__objectz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_new1075z00_3914),
																		BgL_arg1407z00_3915);
																}
																BgL_new1076z00_3913 = BgL_new1075z00_3914;
															}
															((((BgL_z62exceptionz62_bglt) COBJECT(
																			((BgL_z62exceptionz62_bglt)
																				BgL_new1076z00_3913)))->BgL_fnamez00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_z62exceptionz62_bglt)
																		COBJECT(((BgL_z62exceptionz62_bglt)
																				BgL_new1076z00_3913)))->
																	BgL_locationz00) = ((obj_t) BFALSE), BUNSPEC);
															{
																obj_t BgL_auxz00_7184;

																{	/* Unsafe/gunzip.scm 328 */
																	obj_t BgL_arg1405z00_3919;

																	{	/* Unsafe/gunzip.scm 328 */
																		obj_t BgL_arg1406z00_3920;

																		{	/* Unsafe/gunzip.scm 328 */
																			obj_t BgL_classz00_3921;

																			BgL_classz00_3921 =
																				BGl_z62iozd2parsezd2errorz62zz__objectz00;
																			BgL_arg1406z00_3920 =
																				BGL_CLASS_ALL_FIELDS(BgL_classz00_3921);
																		}
																		BgL_arg1405z00_3919 =
																			VECTOR_REF(BgL_arg1406z00_3920, 2L);
																	}
																	BgL_auxz00_7184 =
																		BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																		(BgL_arg1405z00_3919);
																}
																((((BgL_z62exceptionz62_bglt) COBJECT(
																				((BgL_z62exceptionz62_bglt)
																					BgL_new1076z00_3913)))->
																		BgL_stackz00) =
																	((obj_t) BgL_auxz00_7184), BUNSPEC);
															}
															((((BgL_z62errorz62_bglt) COBJECT(
																			((BgL_z62errorz62_bglt)
																				BgL_new1076z00_3913)))->BgL_procz00) =
																((obj_t) BGl_string2405z00zz__gunza7ipza7),
																BUNSPEC);
															((((BgL_z62errorz62_bglt)
																		COBJECT(((BgL_z62errorz62_bglt)
																				BgL_new1076z00_3913)))->BgL_msgz00) =
																((obj_t) BGl_string2408z00zz__gunza7ipza7),
																BUNSPEC);
															((((BgL_z62errorz62_bglt)
																		COBJECT(((BgL_z62errorz62_bglt)
																				BgL_new1076z00_3913)))->BgL_objz00) =
																((obj_t) BgL_inz00_20), BUNSPEC);
															BgL_arg1404z00_3912 = BgL_new1076z00_3913;
														}
														BGl_raisez00zz__errorz00(
															((obj_t) BgL_arg1404z00_3912));
													}
												else
													{	/* Unsafe/gunzip.scm 977 */
														BFALSE;
													}
												{	/* Unsafe/gunzip.scm 979 */
													long BgL_unixzd2modzd2timez00_2188;

													BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00
														(BgL_bufz00_2151, BINT(4L), BgL_inz00_20);
													{	/* Unsafe/gunzip.scm 946 */
														unsigned char BgL_arg1913z00_2227;
														unsigned char BgL_arg1914z00_2228;
														unsigned char BgL_arg1916z00_2229;
														unsigned char BgL_arg1917z00_2230;

														BgL_arg1913z00_2227 =
															STRING_REF(BgL_bufz00_2151, 0L);
														BgL_arg1914z00_2228 =
															STRING_REF(BgL_bufz00_2151, 1L);
														BgL_arg1916z00_2229 =
															STRING_REF(BgL_bufz00_2151, 2L);
														BgL_arg1917z00_2230 =
															STRING_REF(BgL_bufz00_2151, 3L);
														{	/* Unsafe/gunzip.scm 929 */
															long BgL_arg1851z00_3832;
															long BgL_arg1852z00_3833;

															{	/* Unsafe/gunzip.scm 929 */
																long BgL_tmpz00_7204;

																BgL_tmpz00_7204 = (BgL_arg1913z00_2227);
																BgL_arg1851z00_3832 = (long) (BgL_tmpz00_7204);
															}
															{	/* Unsafe/gunzip.scm 931 */
																long BgL_arg1854z00_3835;
																long BgL_arg1856z00_3836;

																{	/* Unsafe/gunzip.scm 931 */
																	long BgL_arg1857z00_3837;

																	{	/* Unsafe/gunzip.scm 931 */
																		long BgL_tmpz00_7207;

																		BgL_tmpz00_7207 = (BgL_arg1914z00_2228);
																		BgL_arg1857z00_3837 =
																			(long) (BgL_tmpz00_7207);
																	}
																	BgL_arg1854z00_3835 =
																		(BgL_arg1857z00_3837 << (int) (8L));
																}
																{	/* Unsafe/gunzip.scm 933 */
																	long BgL_arg1859z00_3839;
																	long BgL_arg1860z00_3840;

																	{	/* Unsafe/gunzip.scm 933 */
																		long BgL_arg1862z00_3841;

																		{	/* Unsafe/gunzip.scm 933 */
																			long BgL_tmpz00_7212;

																			BgL_tmpz00_7212 = (BgL_arg1916z00_2229);
																			BgL_arg1862z00_3841 =
																				(long) (BgL_tmpz00_7212);
																		}
																		BgL_arg1859z00_3839 =
																			(BgL_arg1862z00_3841 << (int) (16L));
																	}
																	{	/* Unsafe/gunzip.scm 934 */
																		long BgL_arg1864z00_3843;

																		{	/* Unsafe/gunzip.scm 934 */
																			long BgL_tmpz00_7217;

																			BgL_tmpz00_7217 = (BgL_arg1917z00_2230);
																			BgL_arg1864z00_3843 =
																				(long) (BgL_tmpz00_7217);
																		}
																		BgL_arg1860z00_3840 =
																			(BgL_arg1864z00_3843 << (int) (24L));
																	}
																	BgL_arg1856z00_3836 =
																		(BgL_arg1859z00_3839 | BgL_arg1860z00_3840);
																}
																BgL_arg1852z00_3833 =
																	(BgL_arg1854z00_3835 | BgL_arg1856z00_3836);
															}
															BgL_unixzd2modzd2timez00_2188 =
																(BgL_arg1851z00_3832 | BgL_arg1852z00_3833);
													}}
													{	/* Unsafe/gunzip.scm 979 */
														obj_t BgL_extrazd2flagszd2_2189;

														BgL_extrazd2flagszd2_2189 =
															BGl_readzd2charzd2zz__r4_input_6_10_2z00
															(BgL_inz00_20);
														{	/* Unsafe/gunzip.scm 980 */
															obj_t BgL_sourcezd2oszd2_2190;

															BgL_sourcezd2oszd2_2190 =
																BGl_readzd2charzd2zz__r4_input_6_10_2z00
																(BgL_inz00_20);
															{	/* Unsafe/gunzip.scm 981 */

																if (BgL_continuationzf3zf3_2183)
																	{	/* Unsafe/gunzip.scm 982 */
																		BINT(BGl_readzd2int2ze70z35zz__gunza7ipza7
																			(BgL_inz00_20, BgL_bufz00_2151));
																	}
																else
																	{	/* Unsafe/gunzip.scm 982 */
																		BFALSE;
																	}
																if (BgL_haszd2extrazd2fieldzf3zf3_2184)
																	{	/* Unsafe/gunzip.scm 985 */
																		long BgL_lenz00_2191;

																		BgL_lenz00_2191 =
																			BGl_readzd2int2ze70z35zz__gunza7ipza7
																			(BgL_inz00_20, BgL_bufz00_2151);
																		{
																			long BgL_lenz00_3930;

																			BgL_lenz00_3930 = BgL_lenz00_2191;
																		BgL_loopz00_3929:
																			if (BGl_za7erozf3z54zz__r4_numbers_6_5z00
																				(BINT(BgL_lenz00_3930)))
																				{	/* Unsafe/gunzip.scm 987 */
																					((bool_t) 0);
																				}
																			else
																				{	/* Unsafe/gunzip.scm 987 */
																					BGl_readzd2charzd2zz__r4_input_6_10_2z00
																						(BgL_inz00_20);
																					{
																						long BgL_lenz00_7236;

																						BgL_lenz00_7236 =
																							(BgL_lenz00_3930 - 1L);
																						BgL_lenz00_3930 = BgL_lenz00_7236;
																						goto BgL_loopz00_3929;
																					}
																				}
																		}
																	}
																else
																	{	/* Unsafe/gunzip.scm 984 */
																		((bool_t) 0);
																	}
																{	/* Unsafe/gunzip.scm 990 */
																	obj_t BgL_originalzd2filenamezd2_2198;

																	if (BgL_haszd2originalzd2filenamezf3zf3_2185)
																		{	/* Unsafe/gunzip.scm 990 */
																			BgL_originalzd2filenamezd2_2198 =
																				BGl_readzd2nullzd2termzd2stringze70z35zz__gunza7ipza7
																				(BgL_inz00_20);
																		}
																	else
																		{	/* Unsafe/gunzip.scm 990 */
																			BgL_originalzd2filenamezd2_2198 = BFALSE;
																		}
																	{	/* Unsafe/gunzip.scm 990 */
																		obj_t BgL_commentz00_2199;

																		if (BgL_haszd2commentzf3z21_2186)
																			{	/* Unsafe/gunzip.scm 992 */
																				BgL_commentz00_2199 =
																					BGl_readzd2nullzd2termzd2stringze70z35zz__gunza7ipza7
																					(BgL_inz00_20);
																			}
																		else
																			{	/* Unsafe/gunzip.scm 992 */
																				BgL_commentz00_2199 = BFALSE;
																			}
																		{	/* Unsafe/gunzip.scm 992 */

																			if (BgL_encryptedzf3zf3_2187)
																				{
																					long BgL_nz00_3943;

																					{	/* Unsafe/gunzip.scm 994 */
																						bool_t BgL_tmpz00_7243;

																						BgL_nz00_3943 = 12L;
																					BgL_loopz00_3942:
																						if (BGl_za7erozf3z54zz__r4_numbers_6_5z00(BINT(BgL_nz00_3943)))
																							{	/* Unsafe/gunzip.scm 995 */
																								BgL_tmpz00_7243 = ((bool_t) 0);
																							}
																						else
																							{	/* Unsafe/gunzip.scm 995 */
																								BGl_readzd2charzd2zz__r4_input_6_10_2z00
																									(BgL_inz00_20);
																								{
																									long BgL_nz00_7248;

																									BgL_nz00_7248 =
																										(BgL_nz00_3943 - 1L);
																									BgL_nz00_3943 = BgL_nz00_7248;
																									goto BgL_loopz00_3942;
																								}
																							}
																						return BBOOL(BgL_tmpz00_7243);
																					}
																				}
																			else
																				{	/* Unsafe/gunzip.scm 993 */
																					return BFALSE;
																				}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* read-int2~0 */
	long BGl_readzd2int2ze70z35zz__gunza7ipza7(obj_t BgL_inz00_4761,
		obj_t BgL_bufz00_4760)
	{
		{	/* Unsafe/gunzip.scm 942 */
			BGl_readzd2charsz12zc0zz__r4_input_6_10_2z00(BgL_bufz00_4760,
				BINT(2L), BgL_inz00_4761);
			return
				(
				(STRING_REF(BgL_bufz00_4760, 0L)) |
				((STRING_REF(BgL_bufz00_4760, 1L)) << (int) (8L)));
		}

	}



/* read-null-term-string~0 */
	obj_t BGl_readzd2nullzd2termzd2stringze70z35zz__gunza7ipza7(obj_t
		BgL_inz00_4762)
	{
		{	/* Unsafe/gunzip.scm 953 */
			{
				obj_t BgL_sz00_2218;

				BgL_sz00_2218 = BNIL;
			BgL_zc3z04anonymousza31906ze3z87_2219:
				{	/* Unsafe/gunzip.scm 950 */
					obj_t BgL_rz00_2220;

					BgL_rz00_2220 =
						BGl_readzd2charzd2zz__r4_input_6_10_2z00(BgL_inz00_4762);
					if ((((unsigned char) '\000') == CCHAR(BgL_rz00_2220)))
						{	/* Unsafe/gunzip.scm 951 */
							return
								BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(bgl_reverse_bang
								(BgL_sz00_2218));
						}
					else
						{	/* Unsafe/gunzip.scm 953 */
							obj_t BgL_arg1911z00_2223;

							BgL_arg1911z00_2223 =
								MAKE_YOUNG_PAIR(BgL_rz00_2220, BgL_sz00_2218);
							{
								obj_t BgL_sz00_7267;

								BgL_sz00_7267 = BgL_arg1911z00_2223;
								BgL_sz00_2218 = BgL_sz00_7267;
								goto BgL_zc3z04anonymousza31906ze3z87_2219;
							}
						}
				}
			}
		}

	}



/* &gunzip-parse-header */
	obj_t BGl_z62gunza7ipzd2parsezd2headerzc5zz__gunza7ipza7(obj_t
		BgL_envz00_4646, obj_t BgL_inz00_4647)
	{
		{	/* Unsafe/gunzip.scm 939 */
			return BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(BgL_inz00_4647);
		}

	}



/* inflate */
	obj_t BGl_inflatez00zz__gunza7ipza7(obj_t BgL_inz00_21, obj_t BgL_outz00_22)
	{
		{	/* Unsafe/gunzip.scm 1002 */
			{	/* Unsafe/gunzip.scm 1002 */
				obj_t BgL_bufferz00_2238;

				{	/* Ieee/string.scm 172 */

					BgL_bufferz00_2238 = make_string(32768L, ((unsigned char) ' '));
				}
				{	/* Unsafe/gunzip.scm 1004 */
					obj_t BgL_statez00_2239;

					BgL_statez00_2239 =
						BGl_inflatezd2entryzd2zz__gunza7ipza7(BgL_inz00_21,
						BgL_bufferz00_2238);
					{	/* Unsafe/gunzip.scm 1005 */
						obj_t BgL_valz00_2240;
						obj_t BgL_kontz00_2241;

						{	/* Unsafe/gunzip.scm 1006 */
							obj_t BgL_tmpz00_3950;

							{	/* Unsafe/gunzip.scm 1006 */
								int BgL_tmpz00_7271;

								BgL_tmpz00_7271 = (int) (1L);
								BgL_tmpz00_3950 = BGL_MVALUES_VAL(BgL_tmpz00_7271);
							}
							{	/* Unsafe/gunzip.scm 1006 */
								int BgL_tmpz00_7274;

								BgL_tmpz00_7274 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_7274, BUNSPEC);
							}
							BgL_valz00_2240 = BgL_tmpz00_3950;
						}
						{	/* Unsafe/gunzip.scm 1006 */
							obj_t BgL_tmpz00_3951;

							{	/* Unsafe/gunzip.scm 1006 */
								int BgL_tmpz00_7277;

								BgL_tmpz00_7277 = (int) (2L);
								BgL_tmpz00_3951 = BGL_MVALUES_VAL(BgL_tmpz00_7277);
							}
							{	/* Unsafe/gunzip.scm 1006 */
								int BgL_tmpz00_7280;

								BgL_tmpz00_7280 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_7280, BUNSPEC);
							}
							BgL_kontz00_2241 = BgL_tmpz00_3951;
						}
						{
							obj_t BgL_statez00_3961;
							obj_t BgL_valz00_3962;
							obj_t BgL_kontz00_3963;
							long BgL_siza7eza7_3964;

							BgL_statez00_3961 = BgL_statez00_2239;
							BgL_valz00_3962 = BgL_valz00_2240;
							BgL_kontz00_3963 = BgL_kontz00_2241;
							BgL_siza7eza7_3964 = 0L;
						BgL_loopz00_3960:
							if ((BgL_statez00_3961 == BGl_symbol2370z00zz__gunza7ipza7))
								{	/* Unsafe/gunzip.scm 1010 */
									{	/* Unsafe/gunzip.scm 1012 */
										long BgL_tmpz00_7285;

										BgL_tmpz00_7285 = (long) CINT(BgL_valz00_3962);
										bgl_display_substring(BgL_bufferz00_2238, 0L,
											BgL_tmpz00_7285, BgL_outz00_22);
									}
									BgL_bufferz00_2238 = BFALSE;
									return ADDFX(BINT(BgL_siza7eza7_3964), BgL_valz00_3962);
								}
							else
								{	/* Unsafe/gunzip.scm 1010 */
									if ((BgL_statez00_3961 == BGl_symbol2374z00zz__gunza7ipza7))
										{	/* Unsafe/gunzip.scm 1010 */
											{	/* Unsafe/gunzip.scm 1016 */
												long BgL_tmpz00_7292;

												BgL_tmpz00_7292 = (long) CINT(BgL_valz00_3962);
												bgl_display_substring(BgL_bufferz00_2238, 0L,
													BgL_tmpz00_7292, BgL_outz00_22);
											}
											{	/* Unsafe/gunzip.scm 1017 */
												obj_t BgL_state2z00_3972;

												BgL_state2z00_3972 =
													BGL_PROCEDURE_CALL0(BgL_kontz00_3963);
												{	/* Unsafe/gunzip.scm 1018 */
													obj_t BgL_val2z00_3973;
													obj_t BgL_kont2z00_3974;

													{	/* Unsafe/gunzip.scm 1019 */
														obj_t BgL_tmpz00_3975;

														{	/* Unsafe/gunzip.scm 1019 */
															int BgL_tmpz00_7298;

															BgL_tmpz00_7298 = (int) (1L);
															BgL_tmpz00_3975 =
																BGL_MVALUES_VAL(BgL_tmpz00_7298);
														}
														{	/* Unsafe/gunzip.scm 1019 */
															int BgL_tmpz00_7301;

															BgL_tmpz00_7301 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_7301, BUNSPEC);
														}
														BgL_val2z00_3973 = BgL_tmpz00_3975;
													}
													{	/* Unsafe/gunzip.scm 1019 */
														obj_t BgL_tmpz00_3976;

														{	/* Unsafe/gunzip.scm 1019 */
															int BgL_tmpz00_7304;

															BgL_tmpz00_7304 = (int) (2L);
															BgL_tmpz00_3976 =
																BGL_MVALUES_VAL(BgL_tmpz00_7304);
														}
														{	/* Unsafe/gunzip.scm 1019 */
															int BgL_tmpz00_7307;

															BgL_tmpz00_7307 = (int) (2L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_7307, BUNSPEC);
														}
														BgL_kont2z00_3974 = BgL_tmpz00_3976;
													}
													{
														long BgL_siza7eza7_7313;
														obj_t BgL_kontz00_7312;
														obj_t BgL_valz00_7311;
														obj_t BgL_statez00_7310;

														BgL_statez00_7310 = BgL_state2z00_3972;
														BgL_valz00_7311 = BgL_val2z00_3973;
														BgL_kontz00_7312 = BgL_kont2z00_3974;
														BgL_siza7eza7_7313 =
															(
															(long) CINT(BgL_valz00_3962) +
															BgL_siza7eza7_3964);
														BgL_siza7eza7_3964 = BgL_siza7eza7_7313;
														BgL_kontz00_3963 = BgL_kontz00_7312;
														BgL_valz00_3962 = BgL_valz00_7311;
														BgL_statez00_3961 = BgL_statez00_7310;
														goto BgL_loopz00_3960;
													}
												}
											}
										}
									else
										{	/* Unsafe/gunzip.scm 1010 */
											return BUNSPEC;
										}
								}
						}
					}
				}
			}
		}

	}



/* gunzip-sendchars */
	BGL_EXPORTED_DEF obj_t BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(obj_t
		BgL_inz00_25, obj_t BgL_outz00_26)
	{
		{	/* Unsafe/gunzip.scm 1031 */
			{	/* Unsafe/gunzip.scm 1032 */
				obj_t BgL_arg1926z00_3980;

				BgL_arg1926z00_3980 = BGL_INPUT_GZIP_PORT_INPUT_PORT(BgL_inz00_25);
				BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7(BgL_arg1926z00_3980);
				BGL_TAIL return
					BGl_inflatez00zz__gunza7ipza7(BgL_arg1926z00_3980, BgL_outz00_26);
			}
		}

	}



/* &gunzip-sendchars */
	obj_t BGl_z62gunza7ipzd2sendcharsz17zz__gunza7ipza7(obj_t BgL_envz00_4648,
		obj_t BgL_inz00_4649, obj_t BgL_outz00_4650)
	{
		{	/* Unsafe/gunzip.scm 1031 */
			{	/* Unsafe/gunzip.scm 1032 */
				obj_t BgL_auxz00_7326;
				obj_t BgL_auxz00_7319;

				if (OUTPUT_PORTP(BgL_outz00_4650))
					{	/* Unsafe/gunzip.scm 1032 */
						BgL_auxz00_7326 = BgL_outz00_4650;
					}
				else
					{
						obj_t BgL_auxz00_7329;

						BgL_auxz00_7329 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2409z00zz__gunza7ipza7,
							BINT(35558L), BGl_string2410z00zz__gunza7ipza7,
							BGl_string2412z00zz__gunza7ipza7, BgL_outz00_4650);
						FAILURE(BgL_auxz00_7329, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_inz00_4649))
					{	/* Unsafe/gunzip.scm 1032 */
						BgL_auxz00_7319 = BgL_inz00_4649;
					}
				else
					{
						obj_t BgL_auxz00_7322;

						BgL_auxz00_7322 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2409z00zz__gunza7ipza7,
							BINT(35558L), BGl_string2410z00zz__gunza7ipza7,
							BGl_string2411z00zz__gunza7ipza7, BgL_inz00_4649);
						FAILURE(BgL_auxz00_7322, BFALSE, BFALSE);
					}
				return
					BGl_gunza7ipzd2sendcharsz75zz__gunza7ipza7(BgL_auxz00_7319,
					BgL_auxz00_7326);
			}
		}

	}



/* inflate-sendchars */
	BGL_EXPORTED_DEF obj_t BGl_inflatezd2sendcharszd2zz__gunza7ipza7(obj_t
		BgL_inz00_27, obj_t BgL_outz00_28)
	{
		{	/* Unsafe/gunzip.scm 1037 */
			{	/* Unsafe/gunzip.scm 1038 */
				obj_t BgL_arg1927z00_3983;

				BgL_arg1927z00_3983 = BGL_INPUT_GZIP_PORT_INPUT_PORT(BgL_inz00_27);
				BGL_TAIL return
					BGl_inflatez00zz__gunza7ipza7(BgL_arg1927z00_3983, BgL_outz00_28);
			}
		}

	}



/* &inflate-sendchars */
	obj_t BGl_z62inflatezd2sendcharszb0zz__gunza7ipza7(obj_t BgL_envz00_4651,
		obj_t BgL_inz00_4652, obj_t BgL_outz00_4653)
	{
		{	/* Unsafe/gunzip.scm 1037 */
			{	/* Unsafe/gunzip.scm 1038 */
				obj_t BgL_auxz00_7343;
				obj_t BgL_auxz00_7336;

				if (OUTPUT_PORTP(BgL_outz00_4653))
					{	/* Unsafe/gunzip.scm 1038 */
						BgL_auxz00_7343 = BgL_outz00_4653;
					}
				else
					{
						obj_t BgL_auxz00_7346;

						BgL_auxz00_7346 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2409z00zz__gunza7ipza7,
							BINT(35893L), BGl_string2413z00zz__gunza7ipza7,
							BGl_string2412z00zz__gunza7ipza7, BgL_outz00_4653);
						FAILURE(BgL_auxz00_7346, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_inz00_4652))
					{	/* Unsafe/gunzip.scm 1038 */
						BgL_auxz00_7336 = BgL_inz00_4652;
					}
				else
					{
						obj_t BgL_auxz00_7339;

						BgL_auxz00_7339 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2409z00zz__gunza7ipza7,
							BINT(35893L), BGl_string2413z00zz__gunza7ipza7,
							BGl_string2411z00zz__gunza7ipza7, BgL_inz00_4652);
						FAILURE(BgL_auxz00_7339, BFALSE, BFALSE);
					}
				return
					BGl_inflatezd2sendcharszd2zz__gunza7ipza7(BgL_auxz00_7336,
					BgL_auxz00_7343);
			}
		}

	}



/* port->port */
	obj_t BGl_portzd2ze3portz31zz__gunza7ipza7(obj_t BgL_inz00_37,
		obj_t BgL_statez00_38, obj_t BgL_bufinfoz00_39, long BgL_bufsiza7eza7_40,
		obj_t BgL_oncompletez00_41)
	{
		{	/* Unsafe/gunzip.scm 1059 */
			{	/* Unsafe/gunzip.scm 1060 */
				obj_t BgL_bufferz00_4670;
				obj_t BgL_statez00_4671;
				obj_t BgL_kontz00_4672;

				{	/* Unsafe/gunzip.scm 1060 */
					obj_t BgL_cellvalz00_7351;

					{	/* Ieee/string.scm 172 */

						BgL_cellvalz00_7351 =
							make_string(BgL_bufsiza7eza7_40, ((unsigned char) ' '));
					}
					BgL_bufferz00_4670 = MAKE_CELL(BgL_cellvalz00_7351);
				}
				BgL_statez00_4671 = MAKE_CELL(BgL_statez00_38);
				BgL_kontz00_4672 = MAKE_CELL(BUNSPEC);
				{	/* Unsafe/gunzip.scm 1064 */
					obj_t BgL_arg1931z00_2267;

					BgL_arg1931z00_2267 =
						BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00(CELL_REF
						(BgL_statez00_4671), BgL_bufinfoz00_39, (int) (default_io_bufsiz));
					{	/* Unsafe/gunzip.scm 1066 */
						obj_t BgL_zc3z04anonymousza31932ze3z87_4654;

						BgL_zc3z04anonymousza31932ze3z87_4654 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31932ze3ze5zz__gunza7ipza7, (int) (0L),
							(int) (6L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31932ze3z87_4654, (int) (0L),
							((obj_t) BgL_statez00_4671));
						PROCEDURE_SET(BgL_zc3z04anonymousza31932ze3z87_4654, (int) (1L),
							BgL_inz00_37);
						PROCEDURE_SET(BgL_zc3z04anonymousza31932ze3z87_4654, (int) (2L),
							((obj_t) BgL_bufferz00_4670));
						PROCEDURE_SET(BgL_zc3z04anonymousza31932ze3z87_4654, (int) (3L),
							((obj_t) BgL_kontz00_4672));
						PROCEDURE_SET(BgL_zc3z04anonymousza31932ze3z87_4654, (int) (4L),
							BINT(BgL_bufsiza7eza7_40));
						PROCEDURE_SET(BgL_zc3z04anonymousza31932ze3z87_4654, (int) (5L),
							BgL_oncompletez00_41);
						return
							bgl_open_input_gzip_port(BgL_zc3z04anonymousza31932ze3z87_4654,
							BgL_inz00_37, BgL_arg1931z00_2267);
					}
				}
			}
		}

	}



/* &<@anonymous:1932> */
	obj_t BGl_z62zc3z04anonymousza31932ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4655)
	{
		{	/* Unsafe/gunzip.scm 1064 */
			{	/* Unsafe/gunzip.scm 1066 */
				obj_t BgL_statez00_4656;
				obj_t BgL_inz00_4657;
				obj_t BgL_bufferz00_4658;
				obj_t BgL_kontz00_4659;
				long BgL_bufsiza7eza7_4660;
				obj_t BgL_oncompletez00_4661;

				BgL_statez00_4656 = PROCEDURE_REF(BgL_envz00_4655, (int) (0L));
				BgL_inz00_4657 = ((obj_t) PROCEDURE_REF(BgL_envz00_4655, (int) (1L)));
				BgL_bufferz00_4658 = PROCEDURE_REF(BgL_envz00_4655, (int) (2L));
				BgL_kontz00_4659 = PROCEDURE_REF(BgL_envz00_4655, (int) (3L));
				BgL_bufsiza7eza7_4660 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4655, (int) (4L)));
				BgL_oncompletez00_4661 = PROCEDURE_REF(BgL_envz00_4655, (int) (5L));
				{
					obj_t BgL_valz00_4864;

					BgL_valz00_4864 = BINT(0L);
				BgL_loopz00_4863:
					{	/* Unsafe/gunzip.scm 1066 */
						obj_t BgL_casezd2valuezd2_4865;

						BgL_casezd2valuezd2_4865 = CELL_REF(BgL_statez00_4656);
						if ((BgL_casezd2valuezd2_4865 == BGl_symbol2414z00zz__gunza7ipza7))
							{	/* Unsafe/gunzip.scm 1066 */
								if (PROCEDUREP(BgL_oncompletez00_4661))
									{	/* Unsafe/gunzip.scm 1068 */
										BGL_PROCEDURE_CALL2(BgL_oncompletez00_4661, BgL_inz00_4657,
											CELL_REF(BgL_bufferz00_4658));
									}
								else
									{	/* Unsafe/gunzip.scm 1068 */
										BFALSE;
									}
								{	/* Unsafe/gunzip.scm 1070 */
									obj_t BgL_auxz00_4866;

									BgL_auxz00_4866 = BFALSE;
									CELL_SET(BgL_bufferz00_4658, BgL_auxz00_4866);
								}
								return BFALSE;
							}
						else
							{	/* Unsafe/gunzip.scm 1066 */
								if (
									(BgL_casezd2valuezd2_4865 ==
										BGl_symbol2370z00zz__gunza7ipza7))
									{	/* Unsafe/gunzip.scm 1066 */
										{	/* Unsafe/gunzip.scm 1073 */
											obj_t BgL_auxz00_4867;

											BgL_auxz00_4867 = BGl_symbol2414z00zz__gunza7ipza7;
											CELL_SET(BgL_statez00_4656, BgL_auxz00_4867);
										}
										{	/* Unsafe/gunzip.scm 1074 */
											obj_t BgL_bufferz00_4868;

											BgL_bufferz00_4868 = CELL_REF(BgL_bufferz00_4658);
											if (
												((long) CINT(BgL_valz00_4864) == BgL_bufsiza7eza7_4660))
												{	/* Unsafe/gunzip.scm 1052 */
													return BgL_bufferz00_4868;
												}
											else
												{	/* Unsafe/gunzip.scm 1054 */
													long BgL_lz00_4869;

													BgL_lz00_4869 = (long) CINT(BgL_valz00_4864);
													{	/* Unsafe/gunzip.scm 1054 */
														obj_t BgL_tmpz00_7404;

														BgL_tmpz00_7404 = ((obj_t) BgL_bufferz00_4868);
														return
															bgl_string_shrink(BgL_tmpz00_7404, BgL_lz00_4869);
													}
												}
										}
									}
								else
									{	/* Unsafe/gunzip.scm 1066 */
										if (
											(BgL_casezd2valuezd2_4865 ==
												BGl_symbol2374z00zz__gunza7ipza7))
											{	/* Unsafe/gunzip.scm 1066 */
												{	/* Unsafe/gunzip.scm 1076 */
													obj_t BgL_auxz00_4870;

													BgL_auxz00_4870 = BGl_symbol2416z00zz__gunza7ipza7;
													CELL_SET(BgL_statez00_4656, BgL_auxz00_4870);
												}
												{	/* Unsafe/gunzip.scm 1077 */
													obj_t BgL_bufferz00_4871;

													BgL_bufferz00_4871 = CELL_REF(BgL_bufferz00_4658);
													if (
														((long) CINT(BgL_valz00_4864) ==
															BgL_bufsiza7eza7_4660))
														{	/* Unsafe/gunzip.scm 1044 */
															return BgL_bufferz00_4871;
														}
													else
														{	/* Unsafe/gunzip.scm 1046 */
															long BgL_endz00_4872;

															BgL_endz00_4872 = (long) CINT(BgL_valz00_4864);
															return
																c_substring(
																((obj_t) BgL_bufferz00_4871), 0L,
																BgL_endz00_4872);
														}
												}
											}
										else
											{	/* Unsafe/gunzip.scm 1066 */
												if (
													(BgL_casezd2valuezd2_4865 ==
														BGl_symbol2416z00zz__gunza7ipza7))
													{	/* Unsafe/gunzip.scm 1079 */
														obj_t BgL_state2z00_4873;

														{	/* Unsafe/gunzip.scm 1080 */
															obj_t BgL_tmpfunz00_7419;

															BgL_tmpfunz00_7419 = CELL_REF(BgL_kontz00_4659);
															BgL_state2z00_4873 =
																(VA_PROCEDUREP(BgL_tmpfunz00_7419)
																? ((obj_t(*)(obj_t,
																			...))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_7419)) (CELL_REF
																	(BgL_kontz00_4659),
																	BEOA) : ((obj_t(*)(obj_t))
																	PROCEDURE_ENTRY(BgL_tmpfunz00_7419)) (CELL_REF
																	(BgL_kontz00_4659)));
														}
														{	/* Unsafe/gunzip.scm 1080 */
															obj_t BgL_val2z00_4874;
															obj_t BgL_kont2z00_4875;

															{	/* Unsafe/gunzip.scm 1081 */
																obj_t BgL_tmpz00_4876;

																{	/* Unsafe/gunzip.scm 1081 */
																	int BgL_tmpz00_7420;

																	BgL_tmpz00_7420 = (int) (1L);
																	BgL_tmpz00_4876 =
																		BGL_MVALUES_VAL(BgL_tmpz00_7420);
																}
																{	/* Unsafe/gunzip.scm 1081 */
																	int BgL_tmpz00_7423;

																	BgL_tmpz00_7423 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_7423, BUNSPEC);
																}
																BgL_val2z00_4874 = BgL_tmpz00_4876;
															}
															{	/* Unsafe/gunzip.scm 1081 */
																obj_t BgL_tmpz00_4877;

																{	/* Unsafe/gunzip.scm 1081 */
																	int BgL_tmpz00_7426;

																	BgL_tmpz00_7426 = (int) (2L);
																	BgL_tmpz00_4877 =
																		BGL_MVALUES_VAL(BgL_tmpz00_7426);
																}
																{	/* Unsafe/gunzip.scm 1081 */
																	int BgL_tmpz00_7429;

																	BgL_tmpz00_7429 = (int) (2L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_7429, BUNSPEC);
																}
																BgL_kont2z00_4875 = BgL_tmpz00_4877;
															}
															CELL_SET(BgL_statez00_4656, BgL_state2z00_4873);
															CELL_SET(BgL_kontz00_4659, BgL_kont2z00_4875);
															{
																obj_t BgL_valz00_7432;

																BgL_valz00_7432 = BgL_val2z00_4874;
																BgL_valz00_4864 = BgL_valz00_7432;
																goto BgL_loopz00_4863;
															}
														}
													}
												else
													{	/* Unsafe/gunzip.scm 1066 */
														if (
															(BgL_casezd2valuezd2_4865 ==
																BGl_symbol2418z00zz__gunza7ipza7))
															{	/* Unsafe/gunzip.scm 1066 */
																BGl_gunza7ipzd2parsezd2headerza7zz__gunza7ipza7
																	(BgL_inz00_4657);
																{	/* Unsafe/gunzip.scm 1086 */
																	obj_t BgL_auxz00_4878;

																	BgL_auxz00_4878 =
																		BGl_symbol2420z00zz__gunza7ipza7;
																	CELL_SET(BgL_statez00_4656, BgL_auxz00_4878);
																}
																{

																	goto BgL_loopz00_4863;
																}
															}
														else
															{	/* Unsafe/gunzip.scm 1066 */
																if (
																	(BgL_casezd2valuezd2_4865 ==
																		BGl_symbol2420z00zz__gunza7ipza7))
																	{	/* Unsafe/gunzip.scm 1089 */
																		obj_t BgL_state0z00_4879;

																		BgL_state0z00_4879 =
																			BGl_inflatezd2entryzd2zz__gunza7ipza7
																			(BgL_inz00_4657,
																			CELL_REF(BgL_bufferz00_4658));
																		{	/* Unsafe/gunzip.scm 1090 */
																			obj_t BgL_val0z00_4880;
																			obj_t BgL_kont0z00_4881;

																			{	/* Unsafe/gunzip.scm 1091 */
																				obj_t BgL_tmpz00_4882;

																				{	/* Unsafe/gunzip.scm 1091 */
																					int BgL_tmpz00_7439;

																					BgL_tmpz00_7439 = (int) (1L);
																					BgL_tmpz00_4882 =
																						BGL_MVALUES_VAL(BgL_tmpz00_7439);
																				}
																				{	/* Unsafe/gunzip.scm 1091 */
																					int BgL_tmpz00_7442;

																					BgL_tmpz00_7442 = (int) (1L);
																					BGL_MVALUES_VAL_SET(BgL_tmpz00_7442,
																						BUNSPEC);
																				}
																				BgL_val0z00_4880 = BgL_tmpz00_4882;
																			}
																			{	/* Unsafe/gunzip.scm 1091 */
																				obj_t BgL_tmpz00_4883;

																				{	/* Unsafe/gunzip.scm 1091 */
																					int BgL_tmpz00_7445;

																					BgL_tmpz00_7445 = (int) (2L);
																					BgL_tmpz00_4883 =
																						BGL_MVALUES_VAL(BgL_tmpz00_7445);
																				}
																				{	/* Unsafe/gunzip.scm 1091 */
																					int BgL_tmpz00_7448;

																					BgL_tmpz00_7448 = (int) (2L);
																					BGL_MVALUES_VAL_SET(BgL_tmpz00_7448,
																						BUNSPEC);
																				}
																				BgL_kont0z00_4881 = BgL_tmpz00_4883;
																			}
																			CELL_SET(BgL_statez00_4656,
																				BgL_state0z00_4879);
																			CELL_SET(BgL_kontz00_4659,
																				BgL_kont0z00_4881);
																			{
																				obj_t BgL_valz00_7451;

																				BgL_valz00_7451 = BgL_val0z00_4880;
																				BgL_valz00_4864 = BgL_valz00_7451;
																				goto BgL_loopz00_4863;
																			}
																		}
																	}
																else
																	{	/* Unsafe/gunzip.scm 1066 */
																		return
																			BGl_errorz00zz__errorz00
																			(BGl_string2422z00zz__gunza7ipza7,
																			BGl_string2377z00zz__gunza7ipza7,
																			CELL_REF(BgL_statez00_4656));
																	}
															}
													}
											}
									}
							}
					}
				}
			}
		}

	}



/* _port->inflate-port */
	obj_t BGl__portzd2ze3inflatezd2portze3zz__gunza7ipza7(obj_t BgL_env1278z00_45,
		obj_t BgL_opt1277z00_44)
	{
		{	/* Unsafe/gunzip.scm 1102 */
			{	/* Unsafe/gunzip.scm 1102 */
				obj_t BgL_g1279z00_2290;

				BgL_g1279z00_2290 = VECTOR_REF(BgL_opt1277z00_44, 0L);
				switch (VECTOR_LENGTH(BgL_opt1277z00_44))
					{
					case 1L:

						{	/* Unsafe/gunzip.scm 1102 */

							{	/* Unsafe/gunzip.scm 1102 */
								obj_t BgL_inz00_4017;

								if (INPUT_PORTP(BgL_g1279z00_2290))
									{	/* Unsafe/gunzip.scm 1102 */
										BgL_inz00_4017 = BgL_g1279z00_2290;
									}
								else
									{
										obj_t BgL_auxz00_7457;

										BgL_auxz00_7457 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2409z00zz__gunza7ipza7, BINT(38091L),
											BGl_string2423z00zz__gunza7ipza7,
											BGl_string2411z00zz__gunza7ipza7, BgL_g1279z00_2290);
										FAILURE(BgL_auxz00_7457, BFALSE, BFALSE);
									}
								return
									BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_inz00_4017,
									BGl_symbol2420z00zz__gunza7ipza7, BTRUE, 32768L, BFALSE);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/gunzip.scm 1102 */
							obj_t BgL_bufinfoz00_2294;

							BgL_bufinfoz00_2294 = VECTOR_REF(BgL_opt1277z00_44, 1L);
							{	/* Unsafe/gunzip.scm 1102 */

								{	/* Unsafe/gunzip.scm 1102 */
									obj_t BgL_inz00_4019;

									if (INPUT_PORTP(BgL_g1279z00_2290))
										{	/* Unsafe/gunzip.scm 1102 */
											BgL_inz00_4019 = BgL_g1279z00_2290;
										}
									else
										{
											obj_t BgL_auxz00_7465;

											BgL_auxz00_7465 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2409z00zz__gunza7ipza7, BINT(38091L),
												BGl_string2423z00zz__gunza7ipza7,
												BGl_string2411z00zz__gunza7ipza7, BgL_g1279z00_2290);
											FAILURE(BgL_auxz00_7465, BFALSE, BFALSE);
										}
									return
										BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_inz00_4019,
										BGl_symbol2420z00zz__gunza7ipza7, BgL_bufinfoz00_2294,
										32768L, BFALSE);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* port->inflate-port */
	BGL_EXPORTED_DEF obj_t BGl_portzd2ze3inflatezd2portze3zz__gunza7ipza7(obj_t
		BgL_inz00_42, obj_t BgL_bufinfoz00_43)
	{
		{	/* Unsafe/gunzip.scm 1102 */
			return
				BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_inz00_42,
				BGl_symbol2420z00zz__gunza7ipza7, BgL_bufinfoz00_43, 32768L, BFALSE);
		}

	}



/* _port->gzip-port */
	obj_t BGl__portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(obj_t BgL_env1283z00_49,
		obj_t BgL_opt1282z00_48)
	{
		{	/* Unsafe/gunzip.scm 1108 */
			{	/* Unsafe/gunzip.scm 1108 */
				obj_t BgL_g1284z00_2296;

				BgL_g1284z00_2296 = VECTOR_REF(BgL_opt1282z00_48, 0L);
				switch (VECTOR_LENGTH(BgL_opt1282z00_48))
					{
					case 1L:

						{	/* Unsafe/gunzip.scm 1108 */

							{	/* Unsafe/gunzip.scm 1108 */
								obj_t BgL_inz00_4022;

								if (INPUT_PORTP(BgL_g1284z00_2296))
									{	/* Unsafe/gunzip.scm 1108 */
										BgL_inz00_4022 = BgL_g1284z00_2296;
									}
								else
									{
										obj_t BgL_auxz00_7476;

										BgL_auxz00_7476 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2409z00zz__gunza7ipza7, BINT(38470L),
											BGl_string2424z00zz__gunza7ipza7,
											BGl_string2411z00zz__gunza7ipza7, BgL_g1284z00_2296);
										FAILURE(BgL_auxz00_7476, BFALSE, BFALSE);
									}
								return
									BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_inz00_4022,
									BGl_symbol2418z00zz__gunza7ipza7, BTRUE, 32768L, BFALSE);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/gunzip.scm 1108 */
							obj_t BgL_bufinfoz00_2300;

							BgL_bufinfoz00_2300 = VECTOR_REF(BgL_opt1282z00_48, 1L);
							{	/* Unsafe/gunzip.scm 1108 */

								{	/* Unsafe/gunzip.scm 1108 */
									obj_t BgL_inz00_4024;

									if (INPUT_PORTP(BgL_g1284z00_2296))
										{	/* Unsafe/gunzip.scm 1108 */
											BgL_inz00_4024 = BgL_g1284z00_2296;
										}
									else
										{
											obj_t BgL_auxz00_7484;

											BgL_auxz00_7484 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2409z00zz__gunza7ipza7, BINT(38470L),
												BGl_string2424z00zz__gunza7ipza7,
												BGl_string2411z00zz__gunza7ipza7, BgL_g1284z00_2296);
											FAILURE(BgL_auxz00_7484, BFALSE, BFALSE);
										}
									return
										BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_inz00_4024,
										BGl_symbol2418z00zz__gunza7ipza7, BgL_bufinfoz00_2300,
										32768L, BFALSE);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* port->gzip-port */
	BGL_EXPORTED_DEF obj_t BGl_portzd2ze3gza7ipzd2portz44zz__gunza7ipza7(obj_t
		BgL_inz00_46, obj_t BgL_bufinfoz00_47)
	{
		{	/* Unsafe/gunzip.scm 1108 */
			return
				BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_inz00_46,
				BGl_symbol2418z00zz__gunza7ipza7, BgL_bufinfoz00_47, 32768L, BFALSE);
		}

	}



/* _open-input-gzip-file */
	obj_t BGl__openzd2inputzd2gza7ipzd2filez75zz__gunza7ipza7(obj_t
		BgL_env1288z00_54, obj_t BgL_opt1287z00_53)
	{
		{	/* Unsafe/gunzip.scm 1114 */
			{	/* Unsafe/gunzip.scm 1114 */
				obj_t BgL_g1289z00_2302;

				BgL_g1289z00_2302 = VECTOR_REF(BgL_opt1287z00_53, 0L);
				switch (VECTOR_LENGTH(BgL_opt1287z00_53))
					{
					case 1L:

						{	/* Unsafe/gunzip.scm 1114 */

							{	/* Unsafe/gunzip.scm 1114 */
								obj_t BgL_namez00_4027;

								if (STRINGP(BgL_g1289z00_2302))
									{	/* Unsafe/gunzip.scm 1114 */
										BgL_namez00_4027 = BgL_g1289z00_2302;
									}
								else
									{
										obj_t BgL_auxz00_7495;

										BgL_auxz00_7495 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2409z00zz__gunza7ipza7, BINT(38840L),
											BGl_string2425z00zz__gunza7ipza7,
											BGl_string2426z00zz__gunza7ipza7, BgL_g1289z00_2302);
										FAILURE(BgL_auxz00_7495, BFALSE, BFALSE);
									}
								{	/* Unsafe/gunzip.scm 1115 */
									obj_t BgL_pz00_4028;

									{	/* Unsafe/gunzip.scm 1115 */

										BgL_pz00_4028 =
											BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
											(BgL_namez00_4027, BTRUE, BINT(5000000L));
									}
									if (INPUT_PORTP(BgL_pz00_4028))
										{	/* Unsafe/gunzip.scm 1117 */
											obj_t BgL_piz00_4033;

											BgL_piz00_4033 =
												BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4028,
												BGl_symbol2418z00zz__gunza7ipza7, BTRUE, 32768L,
												BFALSE);
											{	/* Unsafe/gunzip.scm 1118 */
												obj_t BgL_zc3z04anonymousza31944ze3z87_4678;

												BgL_zc3z04anonymousza31944ze3z87_4678 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31944ze32316ze5zz__gunza7ipza7,
													(int) (1L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4678,
													(int) (0L), BgL_pz00_4028);
												BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
													(BgL_piz00_4033,
													BgL_zc3z04anonymousza31944ze3z87_4678);
											}
											return BgL_piz00_4033;
										}
									else
										{	/* Unsafe/gunzip.scm 1116 */
											return BFALSE;
										}
								}
							}
						}
						break;
					case 2L:

						{	/* Unsafe/gunzip.scm 1114 */
							obj_t BgL_bufinfoz00_2307;

							BgL_bufinfoz00_2307 = VECTOR_REF(BgL_opt1287z00_53, 1L);
							{	/* Unsafe/gunzip.scm 1114 */

								{	/* Unsafe/gunzip.scm 1114 */
									obj_t BgL_namez00_4039;

									if (STRINGP(BgL_g1289z00_2302))
										{	/* Unsafe/gunzip.scm 1114 */
											BgL_namez00_4039 = BgL_g1289z00_2302;
										}
									else
										{
											obj_t BgL_auxz00_7513;

											BgL_auxz00_7513 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2409z00zz__gunza7ipza7, BINT(38840L),
												BGl_string2425z00zz__gunza7ipza7,
												BGl_string2426z00zz__gunza7ipza7, BgL_g1289z00_2302);
											FAILURE(BgL_auxz00_7513, BFALSE, BFALSE);
										}
									{	/* Unsafe/gunzip.scm 1115 */
										obj_t BgL_pz00_4040;

										{	/* Unsafe/gunzip.scm 1115 */

											BgL_pz00_4040 =
												BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
												(BgL_namez00_4039, BgL_bufinfoz00_2307, BINT(5000000L));
										}
										if (INPUT_PORTP(BgL_pz00_4040))
											{	/* Unsafe/gunzip.scm 1117 */
												obj_t BgL_piz00_4045;

												BgL_piz00_4045 =
													BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4040,
													BGl_symbol2418z00zz__gunza7ipza7, BTRUE, 32768L,
													BFALSE);
												{	/* Unsafe/gunzip.scm 1118 */
													obj_t BgL_zc3z04anonymousza31944ze3z87_4677;

													BgL_zc3z04anonymousza31944ze3z87_4677 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31944ze32315ze5zz__gunza7ipza7,
														(int) (1L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4677,
														(int) (0L), BgL_pz00_4040);
													BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
														(BgL_piz00_4045,
														BgL_zc3z04anonymousza31944ze3z87_4677);
												}
												return BgL_piz00_4045;
											}
										else
											{	/* Unsafe/gunzip.scm 1116 */
												return BFALSE;
											}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/gunzip.scm 1114 */
							obj_t BgL_bufinfoz00_2309;

							BgL_bufinfoz00_2309 = VECTOR_REF(BgL_opt1287z00_53, 1L);
							{	/* Unsafe/gunzip.scm 1114 */
								obj_t BgL_timeoutz00_2310;

								BgL_timeoutz00_2310 = VECTOR_REF(BgL_opt1287z00_53, 2L);
								{	/* Unsafe/gunzip.scm 1114 */

									{	/* Unsafe/gunzip.scm 1114 */
										obj_t BgL_namez00_4051;

										if (STRINGP(BgL_g1289z00_2302))
											{	/* Unsafe/gunzip.scm 1114 */
												BgL_namez00_4051 = BgL_g1289z00_2302;
											}
										else
											{
												obj_t BgL_auxz00_7532;

												BgL_auxz00_7532 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2409z00zz__gunza7ipza7, BINT(38840L),
													BGl_string2425z00zz__gunza7ipza7,
													BGl_string2426z00zz__gunza7ipza7, BgL_g1289z00_2302);
												FAILURE(BgL_auxz00_7532, BFALSE, BFALSE);
											}
										{	/* Unsafe/gunzip.scm 1115 */
											obj_t BgL_pz00_4052;

											{	/* Unsafe/gunzip.scm 1115 */

												BgL_pz00_4052 =
													BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
													(BgL_namez00_4051, BgL_bufinfoz00_2309,
													BINT(5000000L));
											}
											if (INPUT_PORTP(BgL_pz00_4052))
												{	/* Unsafe/gunzip.scm 1117 */
													obj_t BgL_piz00_4057;

													BgL_piz00_4057 =
														BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4052,
														BGl_symbol2418z00zz__gunza7ipza7, BTRUE, 32768L,
														BFALSE);
													{	/* Unsafe/gunzip.scm 1118 */
														obj_t BgL_zc3z04anonymousza31944ze3z87_4676;

														BgL_zc3z04anonymousza31944ze3z87_4676 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31944ze3ze5zz__gunza7ipza7,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4676,
															(int) (0L), BgL_pz00_4052);
														BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
															(BgL_piz00_4057,
															BgL_zc3z04anonymousza31944ze3z87_4676);
													}
													return BgL_piz00_4057;
												}
											else
												{	/* Unsafe/gunzip.scm 1116 */
													return BFALSE;
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:1944> */
	obj_t BGl_z62zc3z04anonymousza31944ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4679, obj_t BgL_vz00_4681)
	{
		{	/* Unsafe/gunzip.scm 1118 */
			{	/* Unsafe/gunzip.scm 1118 */
				obj_t BgL_pz00_4680;

				BgL_pz00_4680 = PROCEDURE_REF(BgL_envz00_4679, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4680));
			}
		}

	}



/* &<@anonymous:1944>2315 */
	obj_t BGl_z62zc3z04anonymousza31944ze32315ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4682, obj_t BgL_vz00_4684)
	{
		{	/* Unsafe/gunzip.scm 1118 */
			{	/* Unsafe/gunzip.scm 1118 */
				obj_t BgL_pz00_4683;

				BgL_pz00_4683 = PROCEDURE_REF(BgL_envz00_4682, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4683));
			}
		}

	}



/* &<@anonymous:1944>2316 */
	obj_t BGl_z62zc3z04anonymousza31944ze32316ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4685, obj_t BgL_vz00_4687)
	{
		{	/* Unsafe/gunzip.scm 1118 */
			{	/* Unsafe/gunzip.scm 1118 */
				obj_t BgL_pz00_4686;

				BgL_pz00_4686 = PROCEDURE_REF(BgL_envz00_4685, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4686));
			}
		}

	}



/* open-input-gzip-file */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2gza7ipzd2filez75zz__gunza7ipza7(obj_t BgL_namez00_50,
		obj_t BgL_bufinfoz00_51, obj_t BgL_timeoutz00_52)
	{
		{	/* Unsafe/gunzip.scm 1114 */
			{	/* Unsafe/gunzip.scm 1115 */
				obj_t BgL_pz00_4063;

				{	/* Unsafe/gunzip.scm 1115 */

					BgL_pz00_4063 =
						BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_namez00_50,
						BgL_bufinfoz00_51, BINT(5000000L));
				}
				if (INPUT_PORTP(BgL_pz00_4063))
					{	/* Unsafe/gunzip.scm 1117 */
						obj_t BgL_piz00_4068;

						BgL_piz00_4068 =
							BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4063,
							BGl_symbol2418z00zz__gunza7ipza7, BTRUE, 32768L, BFALSE);
						{	/* Unsafe/gunzip.scm 1118 */
							obj_t BgL_zc3z04anonymousza31944ze3z87_4688;

							BgL_zc3z04anonymousza31944ze3z87_4688 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31944ze32317ze5zz__gunza7ipza7,
								(int) (1L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31944ze3z87_4688, (int) (0L),
								BgL_pz00_4063);
							BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
								(BgL_piz00_4068, BgL_zc3z04anonymousza31944ze3z87_4688);
						}
						return BgL_piz00_4068;
					}
				else
					{	/* Unsafe/gunzip.scm 1116 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1944>2317 */
	obj_t BGl_z62zc3z04anonymousza31944ze32317ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4689, obj_t BgL_vz00_4691)
	{
		{	/* Unsafe/gunzip.scm 1118 */
			{	/* Unsafe/gunzip.scm 1118 */
				obj_t BgL_pz00_4690;

				BgL_pz00_4690 = PROCEDURE_REF(BgL_envz00_4689, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4690));
			}
		}

	}



/* _open-input-inflate-file */
	obj_t BGl__openzd2inputzd2inflatezd2filezd2zz__gunza7ipza7(obj_t
		BgL_env1293z00_59, obj_t BgL_opt1292z00_58)
	{
		{	/* Unsafe/gunzip.scm 1124 */
			{	/* Unsafe/gunzip.scm 1124 */
				obj_t BgL_g1294z00_2321;

				BgL_g1294z00_2321 = VECTOR_REF(BgL_opt1292z00_58, 0L);
				switch (VECTOR_LENGTH(BgL_opt1292z00_58))
					{
					case 1L:

						{	/* Unsafe/gunzip.scm 1124 */

							{	/* Unsafe/gunzip.scm 1124 */
								obj_t BgL_namez00_4074;

								if (STRINGP(BgL_g1294z00_2321))
									{	/* Unsafe/gunzip.scm 1124 */
										BgL_namez00_4074 = BgL_g1294z00_2321;
									}
								else
									{
										obj_t BgL_auxz00_7579;

										BgL_auxz00_7579 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2409z00zz__gunza7ipza7, BINT(39339L),
											BGl_string2427z00zz__gunza7ipza7,
											BGl_string2426z00zz__gunza7ipza7, BgL_g1294z00_2321);
										FAILURE(BgL_auxz00_7579, BFALSE, BFALSE);
									}
								{	/* Unsafe/gunzip.scm 1125 */
									obj_t BgL_pz00_4075;
									obj_t BgL_bz00_4076;

									{	/* Unsafe/gunzip.scm 1125 */

										BgL_pz00_4075 =
											BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
											(BgL_namez00_4074, BTRUE, BINT(5000000L));
									}
									BgL_bz00_4076 =
										BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
										(BGl_string2428z00zz__gunza7ipza7, BTRUE,
										(int) (default_io_bufsiz));
									if (INPUT_PORTP(BgL_pz00_4075))
										{	/* Unsafe/gunzip.scm 1128 */
											obj_t BgL_piz00_4081;

											BgL_piz00_4081 =
												BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4075,
												BGl_symbol2420z00zz__gunza7ipza7, BgL_bz00_4076, 32768L,
												BFALSE);
											{	/* Unsafe/gunzip.scm 1129 */
												obj_t BgL_zc3z04anonymousza31947ze3z87_4694;

												BgL_zc3z04anonymousza31947ze3z87_4694 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31947ze32319ze5zz__gunza7ipza7,
													(int) (1L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31947ze3z87_4694,
													(int) (0L), BgL_pz00_4075);
												BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
													(BgL_piz00_4081,
													BgL_zc3z04anonymousza31947ze3z87_4694);
											}
											return BgL_piz00_4081;
										}
									else
										{	/* Unsafe/gunzip.scm 1127 */
											return BFALSE;
										}
								}
							}
						}
						break;
					case 2L:

						{	/* Unsafe/gunzip.scm 1124 */
							obj_t BgL_bufinfoz00_2326;

							BgL_bufinfoz00_2326 = VECTOR_REF(BgL_opt1292z00_58, 1L);
							{	/* Unsafe/gunzip.scm 1124 */

								{	/* Unsafe/gunzip.scm 1124 */
									obj_t BgL_namez00_4087;

									if (STRINGP(BgL_g1294z00_2321))
										{	/* Unsafe/gunzip.scm 1124 */
											BgL_namez00_4087 = BgL_g1294z00_2321;
										}
									else
										{
											obj_t BgL_auxz00_7599;

											BgL_auxz00_7599 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2409z00zz__gunza7ipza7, BINT(39339L),
												BGl_string2427z00zz__gunza7ipza7,
												BGl_string2426z00zz__gunza7ipza7, BgL_g1294z00_2321);
											FAILURE(BgL_auxz00_7599, BFALSE, BFALSE);
										}
									{	/* Unsafe/gunzip.scm 1125 */
										obj_t BgL_pz00_4088;
										obj_t BgL_bz00_4089;

										{	/* Unsafe/gunzip.scm 1125 */

											BgL_pz00_4088 =
												BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
												(BgL_namez00_4087, BgL_bufinfoz00_2326, BINT(5000000L));
										}
										BgL_bz00_4089 =
											BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
											(BGl_string2428z00zz__gunza7ipza7, BTRUE,
											(int) (default_io_bufsiz));
										if (INPUT_PORTP(BgL_pz00_4088))
											{	/* Unsafe/gunzip.scm 1128 */
												obj_t BgL_piz00_4094;

												BgL_piz00_4094 =
													BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4088,
													BGl_symbol2420z00zz__gunza7ipza7, BgL_bz00_4089,
													32768L, BFALSE);
												{	/* Unsafe/gunzip.scm 1129 */
													obj_t BgL_zc3z04anonymousza31947ze3z87_4693;

													BgL_zc3z04anonymousza31947ze3z87_4693 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31947ze32318ze5zz__gunza7ipza7,
														(int) (1L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31947ze3z87_4693,
														(int) (0L), BgL_pz00_4088);
													BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
														(BgL_piz00_4094,
														BgL_zc3z04anonymousza31947ze3z87_4693);
												}
												return BgL_piz00_4094;
											}
										else
											{	/* Unsafe/gunzip.scm 1127 */
												return BFALSE;
											}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/gunzip.scm 1124 */
							obj_t BgL_bufinfoz00_2328;

							BgL_bufinfoz00_2328 = VECTOR_REF(BgL_opt1292z00_58, 1L);
							{	/* Unsafe/gunzip.scm 1124 */
								obj_t BgL_timeoutz00_2329;

								BgL_timeoutz00_2329 = VECTOR_REF(BgL_opt1292z00_58, 2L);
								{	/* Unsafe/gunzip.scm 1124 */

									{	/* Unsafe/gunzip.scm 1124 */
										obj_t BgL_namez00_4100;

										if (STRINGP(BgL_g1294z00_2321))
											{	/* Unsafe/gunzip.scm 1124 */
												BgL_namez00_4100 = BgL_g1294z00_2321;
											}
										else
											{
												obj_t BgL_auxz00_7620;

												BgL_auxz00_7620 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2409z00zz__gunza7ipza7, BINT(39339L),
													BGl_string2427z00zz__gunza7ipza7,
													BGl_string2426z00zz__gunza7ipza7, BgL_g1294z00_2321);
												FAILURE(BgL_auxz00_7620, BFALSE, BFALSE);
											}
										{	/* Unsafe/gunzip.scm 1125 */
											obj_t BgL_pz00_4101;
											obj_t BgL_bz00_4102;

											{	/* Unsafe/gunzip.scm 1125 */

												BgL_pz00_4101 =
													BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
													(BgL_namez00_4100, BgL_bufinfoz00_2328,
													BINT(5000000L));
											}
											BgL_bz00_4102 =
												BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
												(BGl_string2428z00zz__gunza7ipza7, BTRUE,
												(int) (default_io_bufsiz));
											if (INPUT_PORTP(BgL_pz00_4101))
												{	/* Unsafe/gunzip.scm 1128 */
													obj_t BgL_piz00_4107;

													BgL_piz00_4107 =
														BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4101,
														BGl_symbol2420z00zz__gunza7ipza7, BgL_bz00_4102,
														32768L, BFALSE);
													{	/* Unsafe/gunzip.scm 1129 */
														obj_t BgL_zc3z04anonymousza31947ze3z87_4692;

														BgL_zc3z04anonymousza31947ze3z87_4692 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31947ze3ze5zz__gunza7ipza7,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31947ze3z87_4692,
															(int) (0L), BgL_pz00_4101);
														BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
															(BgL_piz00_4107,
															BgL_zc3z04anonymousza31947ze3z87_4692);
													}
													return BgL_piz00_4107;
												}
											else
												{	/* Unsafe/gunzip.scm 1127 */
													return BFALSE;
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:1947> */
	obj_t BGl_z62zc3z04anonymousza31947ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4695, obj_t BgL_vz00_4697)
	{
		{	/* Unsafe/gunzip.scm 1129 */
			{	/* Unsafe/gunzip.scm 1129 */
				obj_t BgL_pz00_4696;

				BgL_pz00_4696 = PROCEDURE_REF(BgL_envz00_4695, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4696));
			}
		}

	}



/* &<@anonymous:1947>2318 */
	obj_t BGl_z62zc3z04anonymousza31947ze32318ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4698, obj_t BgL_vz00_4700)
	{
		{	/* Unsafe/gunzip.scm 1129 */
			{	/* Unsafe/gunzip.scm 1129 */
				obj_t BgL_pz00_4699;

				BgL_pz00_4699 = PROCEDURE_REF(BgL_envz00_4698, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4699));
			}
		}

	}



/* &<@anonymous:1947>2319 */
	obj_t BGl_z62zc3z04anonymousza31947ze32319ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4701, obj_t BgL_vz00_4703)
	{
		{	/* Unsafe/gunzip.scm 1129 */
			{	/* Unsafe/gunzip.scm 1129 */
				obj_t BgL_pz00_4702;

				BgL_pz00_4702 = PROCEDURE_REF(BgL_envz00_4701, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4702));
			}
		}

	}



/* open-input-inflate-file */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2inflatezd2filezd2zz__gunza7ipza7(obj_t BgL_namez00_55,
		obj_t BgL_bufinfoz00_56, obj_t BgL_timeoutz00_57)
	{
		{	/* Unsafe/gunzip.scm 1124 */
			{	/* Unsafe/gunzip.scm 1125 */
				obj_t BgL_pz00_4113;
				obj_t BgL_bz00_4114;

				{	/* Unsafe/gunzip.scm 1125 */

					BgL_pz00_4113 =
						BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_namez00_55,
						BgL_bufinfoz00_56, BINT(5000000L));
				}
				BgL_bz00_4114 =
					BGl_getzd2portzd2bufferz00zz__r4_ports_6_10_1z00
					(BGl_string2428z00zz__gunza7ipza7, BTRUE, (int) (default_io_bufsiz));
				if (INPUT_PORTP(BgL_pz00_4113))
					{	/* Unsafe/gunzip.scm 1128 */
						obj_t BgL_piz00_4119;

						BgL_piz00_4119 =
							BGl_portzd2ze3portz31zz__gunza7ipza7(BgL_pz00_4113,
							BGl_symbol2420z00zz__gunza7ipza7, BgL_bz00_4114, 32768L, BFALSE);
						{	/* Unsafe/gunzip.scm 1129 */
							obj_t BgL_zc3z04anonymousza31947ze3z87_4704;

							BgL_zc3z04anonymousza31947ze3z87_4704 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31947ze32320ze5zz__gunza7ipza7,
								(int) (1L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31947ze3z87_4704, (int) (0L),
								BgL_pz00_4113);
							BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
								(BgL_piz00_4119, BgL_zc3z04anonymousza31947ze3z87_4704);
						}
						return BgL_piz00_4119;
					}
				else
					{	/* Unsafe/gunzip.scm 1127 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1947>2320 */
	obj_t BGl_z62zc3z04anonymousza31947ze32320ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4705, obj_t BgL_vz00_4707)
	{
		{	/* Unsafe/gunzip.scm 1129 */
			{	/* Unsafe/gunzip.scm 1129 */
				obj_t BgL_pz00_4706;

				BgL_pz00_4706 = PROCEDURE_REF(BgL_envz00_4705, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4706));
			}
		}

	}



/* _port->zlib-port */
	obj_t BGl__portzd2ze3za7libzd2portz44zz__gunza7ipza7(obj_t BgL_env1298z00_63,
		obj_t BgL_opt1297z00_62)
	{
		{	/* Unsafe/gunzip.scm 1135 */
			{	/* Unsafe/gunzip.scm 1135 */
				obj_t BgL_g1299z00_2341;

				BgL_g1299z00_2341 = VECTOR_REF(BgL_opt1297z00_62, 0L);
				switch (VECTOR_LENGTH(BgL_opt1297z00_62))
					{
					case 1L:

						{	/* Unsafe/gunzip.scm 1135 */

							{	/* Unsafe/gunzip.scm 1135 */
								obj_t BgL_auxz00_7669;

								if (INPUT_PORTP(BgL_g1299z00_2341))
									{	/* Unsafe/gunzip.scm 1135 */
										BgL_auxz00_7669 = BgL_g1299z00_2341;
									}
								else
									{
										obj_t BgL_auxz00_7672;

										BgL_auxz00_7672 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2409z00zz__gunza7ipza7, BINT(39917L),
											BGl_string2429z00zz__gunza7ipza7,
											BGl_string2411z00zz__gunza7ipza7, BgL_g1299z00_2341);
										FAILURE(BgL_auxz00_7672, BFALSE, BFALSE);
									}
								return
									BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7(BgL_auxz00_7669,
									BTRUE);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/gunzip.scm 1135 */
							obj_t BgL_bufinfoz00_2345;

							BgL_bufinfoz00_2345 = VECTOR_REF(BgL_opt1297z00_62, 1L);
							{	/* Unsafe/gunzip.scm 1135 */

								{	/* Unsafe/gunzip.scm 1135 */
									obj_t BgL_auxz00_7678;

									if (INPUT_PORTP(BgL_g1299z00_2341))
										{	/* Unsafe/gunzip.scm 1135 */
											BgL_auxz00_7678 = BgL_g1299z00_2341;
										}
									else
										{
											obj_t BgL_auxz00_7681;

											BgL_auxz00_7681 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2409z00zz__gunza7ipza7, BINT(39917L),
												BGl_string2429z00zz__gunza7ipza7,
												BGl_string2411z00zz__gunza7ipza7, BgL_g1299z00_2341);
											FAILURE(BgL_auxz00_7681, BFALSE, BFALSE);
										}
									return
										BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7
										(BgL_auxz00_7678, BgL_bufinfoz00_2345);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* port->zlib-port */
	BGL_EXPORTED_DEF obj_t BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7(obj_t
		BgL_inz00_60, obj_t BgL_bufinfoz00_61)
	{
		{	/* Unsafe/gunzip.scm 1135 */
			{	/* Unsafe/gunzip.scm 1136 */
				obj_t BgL_cmfz00_2346;

				BgL_cmfz00_2346 =
					BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_inz00_60);
				{	/* Unsafe/gunzip.scm 1136 */
					obj_t BgL_flgz00_2347;

					BgL_flgz00_2347 =
						BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_inz00_60);
					{	/* Unsafe/gunzip.scm 1137 */
						long BgL_cmz00_2348;

						BgL_cmz00_2348 = ((long) CINT(BgL_cmfz00_2346) & 15L);
						{	/* Unsafe/gunzip.scm 1138 */
							long BgL_cinfoz00_2349;

							BgL_cinfoz00_2349 = ((long) CINT(BgL_cmfz00_2346) >> (int) (4L));
							{	/* Unsafe/gunzip.scm 1139 */
								long BgL_fcheckz00_2350;

								BgL_fcheckz00_2350 = ((long) CINT(BgL_flgz00_2347) & 15L);
								{	/* Unsafe/gunzip.scm 1140 */
									long BgL_fdictz00_2351;

									BgL_fdictz00_2351 =
										(((long) CINT(BgL_flgz00_2347) & 16L) >> (int) (5L));
									{	/* Unsafe/gunzip.scm 1142 */

										if ((BgL_cmz00_2348 == 8L))
											{	/* Unsafe/gunzip.scm 1146 */
												bool_t BgL_test2671z00_7703;

												{	/* Unsafe/gunzip.scm 1146 */
													obj_t BgL_arg1958z00_2364;

													{	/* Unsafe/gunzip.scm 1146 */
														long BgL_arg1959z00_2365;

														BgL_arg1959z00_2365 =
															(
															((long) CINT(BgL_cmfz00_2346) * 256L) +
															(long) CINT(BgL_flgz00_2347));
														BgL_arg1958z00_2364 =
															BGl_remainderz00zz__r4_numbers_6_5_fixnumz00(BINT
															(BgL_arg1959z00_2365), BINT(31L));
													}
													BgL_test2671z00_7703 =
														((long) CINT(BgL_arg1958z00_2364) == 0L);
												}
												if (BgL_test2671z00_7703)
													{	/* Unsafe/gunzip.scm 1146 */
														if ((BgL_fdictz00_2351 == 0L))
															{	/* Unsafe/gunzip.scm 1149 */
																long BgL_arg1954z00_2359;

																BgL_arg1954z00_2359 =
																	(1L << (int) ((8L + BgL_cinfoz00_2349)));
																return
																	BGl_portzd2ze3portz31zz__gunza7ipza7
																	(BgL_inz00_60,
																	BGl_symbol2420z00zz__gunza7ipza7, BTRUE,
																	BgL_arg1954z00_2359,
																	BGl_checkzd2adler32zd2envz00zz__gunza7ipza7);
															}
														else
															{	/* Unsafe/gunzip.scm 1152 */
																obj_t BgL_dictz00_2361;

																BgL_dictz00_2361 =
																	BGl_readzd2charszd2zz__r4_input_6_10_2z00(BINT
																	(4L), BgL_inz00_60);
																{	/* Unsafe/gunzip.scm 1153 */
																	long BgL_arg1956z00_2362;

																	BgL_arg1956z00_2362 =
																		(1L << (int) ((8L + BgL_cinfoz00_2349)));
																	return
																		BGl_portzd2ze3portz31zz__gunza7ipza7
																		(BgL_inz00_60,
																		BGl_symbol2420z00zz__gunza7ipza7, BTRUE,
																		BgL_arg1956z00_2362, BFALSE);
																}
															}
													}
												else
													{	/* Unsafe/gunzip.scm 1146 */
														return
															BGl_errorz00zz__errorz00
															(BGl_string2430z00zz__gunza7ipza7,
															BGl_string2431z00zz__gunza7ipza7,
															BINT(BgL_fcheckz00_2350));
													}
											}
										else
											{	/* Unsafe/gunzip.scm 1144 */
												return
													BGl_errorz00zz__errorz00
													(BGl_string2430z00zz__gunza7ipza7,
													BGl_string2432z00zz__gunza7ipza7,
													BINT(BgL_cmz00_2348));
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _open-input-zlib-file */
	obj_t BGl__openzd2inputzd2za7libzd2filez75zz__gunza7ipza7(obj_t
		BgL_env1303z00_68, obj_t BgL_opt1302z00_67)
	{
		{	/* Unsafe/gunzip.scm 1158 */
			{	/* Unsafe/gunzip.scm 1158 */
				obj_t BgL_g1304z00_2368;

				BgL_g1304z00_2368 = VECTOR_REF(BgL_opt1302z00_67, 0L);
				switch (VECTOR_LENGTH(BgL_opt1302z00_67))
					{
					case 1L:

						{	/* Unsafe/gunzip.scm 1158 */

							{	/* Unsafe/gunzip.scm 1158 */
								obj_t BgL_namez00_4141;

								if (STRINGP(BgL_g1304z00_2368))
									{	/* Unsafe/gunzip.scm 1158 */
										BgL_namez00_4141 = BgL_g1304z00_2368;
									}
								else
									{
										obj_t BgL_auxz00_7732;

										BgL_auxz00_7732 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2409z00zz__gunza7ipza7, BINT(40867L),
											BGl_string2433z00zz__gunza7ipza7,
											BGl_string2426z00zz__gunza7ipza7, BgL_g1304z00_2368);
										FAILURE(BgL_auxz00_7732, BFALSE, BFALSE);
									}
								{	/* Unsafe/gunzip.scm 1159 */
									obj_t BgL_pz00_4142;

									{	/* Unsafe/gunzip.scm 1159 */

										BgL_pz00_4142 =
											BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
											(BgL_namez00_4141, BTRUE, BINT(5000000L));
									}
									if (INPUT_PORTP(BgL_pz00_4142))
										{	/* Unsafe/gunzip.scm 1161 */
											obj_t BgL_piz00_4147;

											{	/* Unsafe/gunzip.scm 71 */

												BgL_piz00_4147 =
													BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7
													(BgL_pz00_4142, BTRUE);
											}
											{	/* Unsafe/gunzip.scm 1162 */
												obj_t BgL_zc3z04anonymousza31963ze3z87_4713;

												BgL_zc3z04anonymousza31963ze3z87_4713 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31963ze32322ze5zz__gunza7ipza7,
													(int) (1L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_4713,
													(int) (0L), BgL_pz00_4142);
												BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
													(BgL_piz00_4147,
													BgL_zc3z04anonymousza31963ze3z87_4713);
											}
											return BgL_piz00_4147;
										}
									else
										{	/* Unsafe/gunzip.scm 1160 */
											return BFALSE;
										}
								}
							}
						}
						break;
					case 2L:

						{	/* Unsafe/gunzip.scm 1158 */
							obj_t BgL_bufinfoz00_2373;

							BgL_bufinfoz00_2373 = VECTOR_REF(BgL_opt1302z00_67, 1L);
							{	/* Unsafe/gunzip.scm 1158 */

								{	/* Unsafe/gunzip.scm 1158 */
									obj_t BgL_namez00_4153;

									if (STRINGP(BgL_g1304z00_2368))
										{	/* Unsafe/gunzip.scm 1158 */
											BgL_namez00_4153 = BgL_g1304z00_2368;
										}
									else
										{
											obj_t BgL_auxz00_7750;

											BgL_auxz00_7750 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2409z00zz__gunza7ipza7, BINT(40867L),
												BGl_string2433z00zz__gunza7ipza7,
												BGl_string2426z00zz__gunza7ipza7, BgL_g1304z00_2368);
											FAILURE(BgL_auxz00_7750, BFALSE, BFALSE);
										}
									{	/* Unsafe/gunzip.scm 1159 */
										obj_t BgL_pz00_4154;

										{	/* Unsafe/gunzip.scm 1159 */

											BgL_pz00_4154 =
												BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
												(BgL_namez00_4153, BgL_bufinfoz00_2373, BINT(5000000L));
										}
										if (INPUT_PORTP(BgL_pz00_4154))
											{	/* Unsafe/gunzip.scm 1161 */
												obj_t BgL_piz00_4159;

												{	/* Unsafe/gunzip.scm 71 */

													BgL_piz00_4159 =
														BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7
														(BgL_pz00_4154, BTRUE);
												}
												{	/* Unsafe/gunzip.scm 1162 */
													obj_t BgL_zc3z04anonymousza31963ze3z87_4712;

													BgL_zc3z04anonymousza31963ze3z87_4712 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31963ze32321ze5zz__gunza7ipza7,
														(int) (1L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_4712,
														(int) (0L), BgL_pz00_4154);
													BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
														(BgL_piz00_4159,
														BgL_zc3z04anonymousza31963ze3z87_4712);
												}
												return BgL_piz00_4159;
											}
										else
											{	/* Unsafe/gunzip.scm 1160 */
												return BFALSE;
											}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/gunzip.scm 1158 */
							obj_t BgL_bufinfoz00_2375;

							BgL_bufinfoz00_2375 = VECTOR_REF(BgL_opt1302z00_67, 1L);
							{	/* Unsafe/gunzip.scm 1158 */
								obj_t BgL_timeoutz00_2376;

								BgL_timeoutz00_2376 = VECTOR_REF(BgL_opt1302z00_67, 2L);
								{	/* Unsafe/gunzip.scm 1158 */

									{	/* Unsafe/gunzip.scm 1158 */
										obj_t BgL_namez00_4165;

										if (STRINGP(BgL_g1304z00_2368))
											{	/* Unsafe/gunzip.scm 1158 */
												BgL_namez00_4165 = BgL_g1304z00_2368;
											}
										else
											{
												obj_t BgL_auxz00_7769;

												BgL_auxz00_7769 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2409z00zz__gunza7ipza7, BINT(40867L),
													BGl_string2433z00zz__gunza7ipza7,
													BGl_string2426z00zz__gunza7ipza7, BgL_g1304z00_2368);
												FAILURE(BgL_auxz00_7769, BFALSE, BFALSE);
											}
										{	/* Unsafe/gunzip.scm 1159 */
											obj_t BgL_pz00_4166;

											{	/* Unsafe/gunzip.scm 1159 */

												BgL_pz00_4166 =
													BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
													(BgL_namez00_4165, BgL_bufinfoz00_2375,
													BINT(5000000L));
											}
											if (INPUT_PORTP(BgL_pz00_4166))
												{	/* Unsafe/gunzip.scm 1161 */
													obj_t BgL_piz00_4171;

													{	/* Unsafe/gunzip.scm 71 */

														BgL_piz00_4171 =
															BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7
															(BgL_pz00_4166, BTRUE);
													}
													{	/* Unsafe/gunzip.scm 1162 */
														obj_t BgL_zc3z04anonymousza31963ze3z87_4711;

														BgL_zc3z04anonymousza31963ze3z87_4711 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31963ze3ze5zz__gunza7ipza7,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_4711,
															(int) (0L), BgL_pz00_4166);
														BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
															(BgL_piz00_4171,
															BgL_zc3z04anonymousza31963ze3z87_4711);
													}
													return BgL_piz00_4171;
												}
											else
												{	/* Unsafe/gunzip.scm 1160 */
													return BFALSE;
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:1963> */
	obj_t BGl_z62zc3z04anonymousza31963ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4714, obj_t BgL_vz00_4716)
	{
		{	/* Unsafe/gunzip.scm 1162 */
			{	/* Unsafe/gunzip.scm 1162 */
				obj_t BgL_pz00_4715;

				BgL_pz00_4715 = PROCEDURE_REF(BgL_envz00_4714, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4715));
			}
		}

	}



/* &<@anonymous:1963>2321 */
	obj_t BGl_z62zc3z04anonymousza31963ze32321ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4717, obj_t BgL_vz00_4719)
	{
		{	/* Unsafe/gunzip.scm 1162 */
			{	/* Unsafe/gunzip.scm 1162 */
				obj_t BgL_pz00_4718;

				BgL_pz00_4718 = PROCEDURE_REF(BgL_envz00_4717, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4718));
			}
		}

	}



/* &<@anonymous:1963>2322 */
	obj_t BGl_z62zc3z04anonymousza31963ze32322ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4720, obj_t BgL_vz00_4722)
	{
		{	/* Unsafe/gunzip.scm 1162 */
			{	/* Unsafe/gunzip.scm 1162 */
				obj_t BgL_pz00_4721;

				BgL_pz00_4721 = PROCEDURE_REF(BgL_envz00_4720, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4721));
			}
		}

	}



/* open-input-zlib-file */
	BGL_EXPORTED_DEF obj_t
		BGl_openzd2inputzd2za7libzd2filez75zz__gunza7ipza7(obj_t BgL_namez00_64,
		obj_t BgL_bufinfoz00_65, obj_t BgL_timeoutz00_66)
	{
		{	/* Unsafe/gunzip.scm 1158 */
			{	/* Unsafe/gunzip.scm 1159 */
				obj_t BgL_pz00_4177;

				{	/* Unsafe/gunzip.scm 1159 */

					BgL_pz00_4177 =
						BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(BgL_namez00_64,
						BgL_bufinfoz00_65, BINT(5000000L));
				}
				if (INPUT_PORTP(BgL_pz00_4177))
					{	/* Unsafe/gunzip.scm 1161 */
						obj_t BgL_piz00_4182;

						{	/* Unsafe/gunzip.scm 71 */

							BgL_piz00_4182 =
								BGl_portzd2ze3za7libzd2portz44zz__gunza7ipza7(BgL_pz00_4177,
								BTRUE);
						}
						{	/* Unsafe/gunzip.scm 1162 */
							obj_t BgL_zc3z04anonymousza31963ze3z87_4723;

							BgL_zc3z04anonymousza31963ze3z87_4723 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31963ze32323ze5zz__gunza7ipza7,
								(int) (1L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_4723, (int) (0L),
								BgL_pz00_4177);
							BGl_inputzd2portzd2closezd2hookzd2setz12z12zz__r4_ports_6_10_1z00
								(BgL_piz00_4182, BgL_zc3z04anonymousza31963ze3z87_4723);
						}
						return BgL_piz00_4182;
					}
				else
					{	/* Unsafe/gunzip.scm 1160 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1963>2323 */
	obj_t BGl_z62zc3z04anonymousza31963ze32323ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4724, obj_t BgL_vz00_4726)
	{
		{	/* Unsafe/gunzip.scm 1162 */
			{	/* Unsafe/gunzip.scm 1162 */
				obj_t BgL_pz00_4725;

				BgL_pz00_4725 = PROCEDURE_REF(BgL_envz00_4724, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4725));
			}
		}

	}



/* &check-adler32 */
	obj_t BGl_z62checkzd2adler32zb0zz__gunza7ipza7(obj_t BgL_envz00_4708,
		obj_t BgL_inz00_4709, obj_t BgL_bufz00_4710)
	{
		{	/* Unsafe/gunzip.scm 1172 */
			{	/* Unsafe/gunzip.scm 1173 */
				obj_t BgL_inz00_4884;
				obj_t BgL_bufz00_4885;

				if (INPUT_PORTP(BgL_inz00_4709))
					{	/* Unsafe/gunzip.scm 1173 */
						BgL_inz00_4884 = BgL_inz00_4709;
					}
				else
					{
						obj_t BgL_auxz00_7815;

						BgL_auxz00_7815 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2409z00zz__gunza7ipza7,
							BINT(41727L), BGl_string2434z00zz__gunza7ipza7,
							BGl_string2411z00zz__gunza7ipza7, BgL_inz00_4709);
						FAILURE(BgL_auxz00_7815, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_bufz00_4710))
					{	/* Unsafe/gunzip.scm 1173 */
						BgL_bufz00_4885 = BgL_bufz00_4710;
					}
				else
					{
						obj_t BgL_auxz00_7821;

						BgL_auxz00_7821 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2409z00zz__gunza7ipza7,
							BINT(41727L), BGl_string2434z00zz__gunza7ipza7,
							BGl_string2426z00zz__gunza7ipza7, BgL_bufz00_4710);
						FAILURE(BgL_auxz00_7821, BFALSE, BFALSE);
					}
				BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_inz00_4884);
				BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_inz00_4884);
				BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_inz00_4884);
				return BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_inz00_4884);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			{	/* Unsafe/gunzip.scm 68 */
				obj_t BgL_arg1967z00_2392;
				obj_t BgL_arg1968z00_2393;

				{	/* Unsafe/gunzip.scm 68 */
					obj_t BgL_v1259z00_2406;

					BgL_v1259z00_2406 = create_vector(3L);
					VECTOR_SET(BgL_v1259z00_2406, 0L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2437z00zz__gunza7ipza7, BGl_proc2436z00zz__gunza7ipza7,
							BGl_proc2435z00zz__gunza7ipza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2439z00zz__gunza7ipza7));
					VECTOR_SET(BgL_v1259z00_2406, 1L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2443z00zz__gunza7ipza7, BGl_proc2442z00zz__gunza7ipza7,
							BGl_proc2441z00zz__gunza7ipza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2439z00zz__gunza7ipza7));
					VECTOR_SET(BgL_v1259z00_2406, 2L,
						BGl_makezd2classzd2fieldz00zz__objectz00
						(BGl_symbol2447z00zz__gunza7ipza7, BGl_proc2446z00zz__gunza7ipza7,
							BGl_proc2445z00zz__gunza7ipza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_symbol2449z00zz__gunza7ipza7));
					BgL_arg1967z00_2392 = BgL_v1259z00_2406;
				}
				{	/* Unsafe/gunzip.scm 68 */
					obj_t BgL_v1260z00_2437;

					BgL_v1260z00_2437 = create_vector(0L);
					BgL_arg1968z00_2393 = BgL_v1260z00_2437;
				}
				return (BGl_huftz00zz__gunza7ipza7 =
					BGl_registerzd2classz12zc0zz__objectz00
					(BGl_symbol2454z00zz__gunza7ipza7, BGl_symbol2456z00zz__gunza7ipza7,
						BGl_objectz00zz__objectz00, 18873L, BGl_proc2453z00zz__gunza7ipza7,
						BGl_proc2452z00zz__gunza7ipza7, BFALSE,
						BGl_proc2451z00zz__gunza7ipza7, BFALSE, BgL_arg1967z00_2392,
						BgL_arg1968z00_2393), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1973> */
	obj_t BGl_z62zc3z04anonymousza31973ze3ze5zz__gunza7ipza7(obj_t
		BgL_envz00_4736, obj_t BgL_new1044z00_4737)
	{
		{	/* Unsafe/gunzip.scm 68 */
			{
				BgL_huftz00_bglt BgL_auxz00_7838;

				((((BgL_huftz00_bglt) COBJECT(
								((BgL_huftz00_bglt) BgL_new1044z00_4737)))->BgL_ez00) =
					((long) 0L), BUNSPEC);
				((((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt)
									BgL_new1044z00_4737)))->BgL_bz00) = ((long) 0L), BUNSPEC);
				((((BgL_huftz00_bglt) COBJECT(((BgL_huftz00_bglt)
									BgL_new1044z00_4737)))->BgL_vz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_7838 = ((BgL_huftz00_bglt) BgL_new1044z00_4737);
				return ((obj_t) BgL_auxz00_7838);
			}
		}

	}



/* &lambda1971 */
	BgL_huftz00_bglt BGl_z62lambda1971z62zz__gunza7ipza7(obj_t BgL_envz00_4738)
	{
		{	/* Unsafe/gunzip.scm 68 */
			{	/* Unsafe/gunzip.scm 68 */
				BgL_huftz00_bglt BgL_new1043z00_4887;

				BgL_new1043z00_4887 =
					((BgL_huftz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_huftz00_bgl))));
				{	/* Unsafe/gunzip.scm 68 */
					long BgL_arg1972z00_4888;

					BgL_arg1972z00_4888 = BGL_CLASS_NUM(BGl_huftz00zz__gunza7ipza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1043z00_4887), BgL_arg1972z00_4888);
				}
				return BgL_new1043z00_4887;
			}
		}

	}



/* &lambda1969 */
	BgL_huftz00_bglt BGl_z62lambda1969z62zz__gunza7ipza7(obj_t BgL_envz00_4739,
		obj_t BgL_e1040z00_4740, obj_t BgL_b1041z00_4741, obj_t BgL_v1042z00_4742)
	{
		{	/* Unsafe/gunzip.scm 68 */
			{	/* Unsafe/gunzip.scm 68 */
				long BgL_e1040z00_4889;
				long BgL_b1041z00_4890;

				BgL_e1040z00_4889 = (long) CINT(BgL_e1040z00_4740);
				BgL_b1041z00_4890 = (long) CINT(BgL_b1041z00_4741);
				{	/* Unsafe/gunzip.scm 68 */
					BgL_huftz00_bglt BgL_new1169z00_4891;

					{	/* Unsafe/gunzip.scm 68 */
						BgL_huftz00_bglt BgL_new1168z00_4892;

						BgL_new1168z00_4892 =
							((BgL_huftz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_huftz00_bgl))));
						{	/* Unsafe/gunzip.scm 68 */
							long BgL_arg1970z00_4893;

							BgL_arg1970z00_4893 = BGL_CLASS_NUM(BGl_huftz00zz__gunza7ipza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1168z00_4892),
								BgL_arg1970z00_4893);
						}
						BgL_new1169z00_4891 = BgL_new1168z00_4892;
					}
					((((BgL_huftz00_bglt) COBJECT(BgL_new1169z00_4891))->BgL_ez00) =
						((long) BgL_e1040z00_4889), BUNSPEC);
					((((BgL_huftz00_bglt) COBJECT(BgL_new1169z00_4891))->BgL_bz00) =
						((long) BgL_b1041z00_4890), BUNSPEC);
					((((BgL_huftz00_bglt) COBJECT(BgL_new1169z00_4891))->BgL_vz00) =
						((obj_t) BgL_v1042z00_4742), BUNSPEC);
					return BgL_new1169z00_4891;
				}
			}
		}

	}



/* &lambda1988 */
	obj_t BGl_z62lambda1988z62zz__gunza7ipza7(obj_t BgL_envz00_4743,
		obj_t BgL_oz00_4744, obj_t BgL_vz00_4745)
	{
		{	/* Unsafe/gunzip.scm 68 */
			return
				((((BgL_huftz00_bglt) COBJECT(
							((BgL_huftz00_bglt) BgL_oz00_4744)))->BgL_vz00) =
				((obj_t) BgL_vz00_4745), BUNSPEC);
		}

	}



/* &lambda1987 */
	obj_t BGl_z62lambda1987z62zz__gunza7ipza7(obj_t BgL_envz00_4746,
		obj_t BgL_oz00_4747)
	{
		{	/* Unsafe/gunzip.scm 68 */
			return
				(((BgL_huftz00_bglt) COBJECT(
						((BgL_huftz00_bglt) BgL_oz00_4747)))->BgL_vz00);
		}

	}



/* &lambda1983 */
	obj_t BGl_z62lambda1983z62zz__gunza7ipza7(obj_t BgL_envz00_4748,
		obj_t BgL_oz00_4749, obj_t BgL_vz00_4750)
	{
		{	/* Unsafe/gunzip.scm 68 */
			{	/* Unsafe/gunzip.scm 68 */
				long BgL_vz00_4897;

				BgL_vz00_4897 = (long) CINT(BgL_vz00_4750);
				return
					((((BgL_huftz00_bglt) COBJECT(
								((BgL_huftz00_bglt) BgL_oz00_4749)))->BgL_bz00) =
					((long) BgL_vz00_4897), BUNSPEC);
		}}

	}



/* &lambda1982 */
	obj_t BGl_z62lambda1982z62zz__gunza7ipza7(obj_t BgL_envz00_4751,
		obj_t BgL_oz00_4752)
	{
		{	/* Unsafe/gunzip.scm 68 */
			return
				BINT(
				(((BgL_huftz00_bglt) COBJECT(
							((BgL_huftz00_bglt) BgL_oz00_4752)))->BgL_bz00));
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zz__gunza7ipza7(obj_t BgL_envz00_4753,
		obj_t BgL_oz00_4754, obj_t BgL_vz00_4755)
	{
		{	/* Unsafe/gunzip.scm 68 */
			{	/* Unsafe/gunzip.scm 68 */
				long BgL_vz00_4900;

				BgL_vz00_4900 = (long) CINT(BgL_vz00_4755);
				return
					((((BgL_huftz00_bglt) COBJECT(
								((BgL_huftz00_bglt) BgL_oz00_4754)))->BgL_ez00) =
					((long) BgL_vz00_4900), BUNSPEC);
		}}

	}



/* &lambda1977 */
	obj_t BGl_z62lambda1977z62zz__gunza7ipza7(obj_t BgL_envz00_4756,
		obj_t BgL_oz00_4757)
	{
		{	/* Unsafe/gunzip.scm 68 */
			return
				BINT(
				(((BgL_huftz00_bglt) COBJECT(
							((BgL_huftz00_bglt) BgL_oz00_4757)))->BgL_ez00));
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__gunza7ipza7(void)
	{
		{	/* Unsafe/gunzip.scm 25 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2457z00zz__gunza7ipza7));
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(185672289L,
				BSTRING_TO_STRING(BGl_string2457z00zz__gunza7ipza7));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2457z00zz__gunza7ipza7));
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(228151370L,
				BSTRING_TO_STRING(BGl_string2457z00zz__gunza7ipza7));
			BGl_modulezd2initializa7ationz75zz__objectz00(475449627L,
				BSTRING_TO_STRING(BGl_string2457z00zz__gunza7ipza7));
			return
				BGl_modulezd2initializa7ationz75zz__rgcz00(352600006L,
				BSTRING_TO_STRING(BGl_string2457z00zz__gunza7ipza7));
		}

	}

#ifdef __cplusplus
}
#endif
