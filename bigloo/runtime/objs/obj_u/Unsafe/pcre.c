/*===========================================================================*/
/*   (Unsafe/pcre.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/pcre.scm -indent -o objs/obj_u/Unsafe/pcre.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___REGEXP_TYPE_DEFINITIONS
#define BGL___REGEXP_TYPE_DEFINITIONS
#endif													// BGL___REGEXP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2quotezd2zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_regexpzd2patternzd2zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regexpzf3zf3zz__regexpz00(obj_t);
	static obj_t BGl_z62regexpzf3z91zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__regexpz00 = BUNSPEC;
	static obj_t BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00(obj_t,
		obj_t, long);
	static obj_t BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_pregexpzd2listzd2refz00zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_list1839z00zz__regexpz00 = BUNSPEC;
	BGL_EXPORTED_DECL long
		BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t, obj_t, obj_t,
		long, long, obj_t);
	static obj_t BGl_z62pregexpzd2quotezb0zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__regexpz00(void);
	static obj_t BGl_genericzd2initzd2zz__regexpz00(void);
	static obj_t BGl__pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__regexpz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__regexpz00(void);
	static obj_t BGl_objectzd2initzd2zz__regexpz00(void);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_pregexpzd2replacezd2auxz00zz__regexpz00(obj_t, obj_t, long,
		obj_t);
	static obj_t BGl__pregexpzd2matchzd2zz__regexpz00(obj_t, obj_t);
	extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpz00zz__regexpz00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62pregexpz62zz__regexpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2splitzd2zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2replacezd2zz__regexpz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62pregexpzd2replacezb0zz__regexpz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bgl_regcomp(obj_t, obj_t, bool_t);
	static obj_t BGl_z62pregexpzd2replaceza2z12zz__regexpz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62regexpzd2patternzb0zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__regexpz00(void);
	static obj_t BGl_z62regexpzd2capturezd2countz62zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2matchzd2zz__regexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62pregexpzd2splitzb0zz__regexpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_regexpzd2capturezd2countz00zz__regexpz00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2envzd2zz__regexpz00,
		BgL_bgl_za762pregexpza762za7za7_1842z00, va_generic_entry,
		BGl_z62pregexpz62zz__regexpz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regexpzd2capturezd2countzd2envzd2zz__regexpz00,
		BgL_bgl_za762regexpza7d2capt1843z00,
		BGl_z62regexpzd2capturezd2countz62zz__regexpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2splitzd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2spl1844z00, BGl_z62pregexpzd2splitzb0zz__regexpz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2quotezd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2quo1845z00, BGl_z62pregexpzd2quotezb0zz__regexpz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regexpzd2patternzd2envz00zz__regexpz00,
		BgL_bgl_za762regexpza7d2patt1846z00,
		BGl_z62regexpzd2patternzb0zz__regexpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2replacezd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2rep1847z00,
		BGl_z62pregexpzd2replacezb0zz__regexpz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regexpzf3zd2envz21zz__regexpz00,
		BgL_bgl_za762regexpza7f3za791za71848z00, BGl_z62regexpzf3z91zz__regexpz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2replaceza2zd2envza2zz__regexpz00,
		BgL_bgl_za762pregexpza7d2rep1849z00,
		BGl_z62pregexpzd2replaceza2z12zz__regexpz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pregexpzd2matchzd2nzd2positionsz12zd2envz12zz__regexpz00,
		BgL_bgl__pregexpza7d2match1850za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1824z00zz__regexpz00,
		BgL_bgl_string1824za700za7za7_1851za7,
		"/tmp/bigloo/runtime/Unsafe/pcre.scm", 35);
	      DEFINE_STRING(BGl_string1825z00zz__regexpz00,
		BgL_bgl_string1825za700za7za7_1852za7, "&regexp-pattern", 15);
	      DEFINE_STRING(BGl_string1826z00zz__regexpz00,
		BgL_bgl_string1826za700za7za7_1853za7, "regexp", 6);
	      DEFINE_STRING(BGl_string1827z00zz__regexpz00,
		BgL_bgl_string1827za700za7za7_1854za7, "&regexp-capture-count", 21);
	      DEFINE_STRING(BGl_string1828z00zz__regexpz00,
		BgL_bgl_string1828za700za7za7_1855za7, "&pregexp", 8);
	      DEFINE_STRING(BGl_string1829z00zz__regexpz00,
		BgL_bgl_string1829za700za7za7_1856za7, "bstring", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pregexpzd2matchzd2positionszd2envzd2zz__regexpz00,
		BgL_bgl__pregexpza7d2match1857za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2positionsz00zz__regexpz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1830z00zz__regexpz00,
		BgL_bgl_string1830za700za7za7_1858za7, "_pregexp-match-positions", 24);
	      DEFINE_STRING(BGl_string1831z00zz__regexpz00,
		BgL_bgl_string1831za700za7za7_1859za7, "bint", 4);
	      DEFINE_STRING(BGl_string1832z00zz__regexpz00,
		BgL_bgl_string1832za700za7za7_1860za7, "_pregexp-match-n-positions!", 27);
	      DEFINE_STRING(BGl_string1833z00zz__regexpz00,
		BgL_bgl_string1833za700za7za7_1861za7, "vector", 6);
	      DEFINE_STRING(BGl_string1834z00zz__regexpz00,
		BgL_bgl_string1834za700za7za7_1862za7, "_pregexp-match", 14);
	      DEFINE_STRING(BGl_string1835z00zz__regexpz00,
		BgL_bgl_string1835za700za7za7_1863za7, "", 0);
	      DEFINE_STRING(BGl_string1836z00zz__regexpz00,
		BgL_bgl_string1836za700za7za7_1864za7, "&pregexp-split", 14);
	      DEFINE_STRING(BGl_string1837z00zz__regexpz00,
		BgL_bgl_string1837za700za7za7_1865za7, "&pregexp-replace", 16);
	      DEFINE_STRING(BGl_string1838z00zz__regexpz00,
		BgL_bgl_string1838za700za7za7_1866za7, "&pregexp-replace*", 17);
	      DEFINE_STRING(BGl_string1840z00zz__regexpz00,
		BgL_bgl_string1840za700za7za7_1867za7, "&pregexp-quote", 14);
	      DEFINE_STRING(BGl_string1841z00zz__regexpz00,
		BgL_bgl_string1841za700za7za7_1868za7, "__regexp", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2matchzd2envz00zz__regexpz00,
		BgL_bgl__pregexpza7d2match1869za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2zz__regexpz00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list1839z00zz__regexpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long
		BgL_checksumz00_2388, char *BgL_fromz00_2389)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__regexpz00))
				{
					BGl_requirezd2initializa7ationz75zz__regexpz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__regexpz00();
					BGl_cnstzd2initzd2zz__regexpz00();
					BGl_importedzd2moduleszd2initz00zz__regexpz00();
					return BGl_methodzd2initzd2zz__regexpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pcre.scm 15 */
			return (BGl_list1839z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')),
					MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '.')),
						MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '?')),
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '*')),
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '+')),
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '|')),
										MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '^')),
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '$')),
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '[')),
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '{')),
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '}')),
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '(')),
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
																		BNIL)))))))))))))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__regexpz00(void)
	{
		{	/* Unsafe/pcre.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* regexp? */
	BGL_EXPORTED_DEF bool_t BGl_regexpzf3zf3zz__regexpz00(obj_t BgL_objz00_3)
	{
		{	/* Unsafe/pcre.scm 92 */
			return BGL_REGEXPP(BgL_objz00_3);
		}

	}



/* &regexp? */
	obj_t BGl_z62regexpzf3z91zz__regexpz00(obj_t BgL_envz00_2247,
		obj_t BgL_objz00_2248)
	{
		{	/* Unsafe/pcre.scm 92 */
			return BBOOL(BGl_regexpzf3zf3zz__regexpz00(BgL_objz00_2248));
		}

	}



/* regexp-pattern */
	BGL_EXPORTED_DEF obj_t BGl_regexpzd2patternzd2zz__regexpz00(obj_t BgL_rez00_4)
	{
		{	/* Unsafe/pcre.scm 98 */
			return BGL_REGEXP_PAT(BgL_rez00_4);
		}

	}



/* &regexp-pattern */
	obj_t BGl_z62regexpzd2patternzb0zz__regexpz00(obj_t BgL_envz00_2249,
		obj_t BgL_rez00_2250)
	{
		{	/* Unsafe/pcre.scm 98 */
			{	/* Unsafe/pcre.scm 99 */
				obj_t BgL_auxz00_2430;

				if (BGl_regexpzf3zf3zz__regexpz00(BgL_rez00_2250))
					{	/* Unsafe/pcre.scm 99 */
						BgL_auxz00_2430 = BgL_rez00_2250;
					}
				else
					{
						obj_t BgL_auxz00_2433;

						BgL_auxz00_2433 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(4253L), BGl_string1825z00zz__regexpz00,
							BGl_string1826z00zz__regexpz00, BgL_rez00_2250);
						FAILURE(BgL_auxz00_2433, BFALSE, BFALSE);
					}
				return BGl_regexpzd2patternzd2zz__regexpz00(BgL_auxz00_2430);
			}
		}

	}



/* regexp-capture-count */
	BGL_EXPORTED_DEF long BGl_regexpzd2capturezd2countz00zz__regexpz00(obj_t
		BgL_rez00_5)
	{
		{	/* Unsafe/pcre.scm 104 */
			return BGL_REGEXP_CAPTURE_COUNT(BgL_rez00_5);
		}

	}



/* &regexp-capture-count */
	obj_t BGl_z62regexpzd2capturezd2countz62zz__regexpz00(obj_t BgL_envz00_2251,
		obj_t BgL_rez00_2252)
	{
		{	/* Unsafe/pcre.scm 104 */
			{	/* Unsafe/pcre.scm 105 */
				long BgL_tmpz00_2439;

				{	/* Unsafe/pcre.scm 105 */
					obj_t BgL_auxz00_2440;

					if (BGl_regexpzf3zf3zz__regexpz00(BgL_rez00_2252))
						{	/* Unsafe/pcre.scm 105 */
							BgL_auxz00_2440 = BgL_rez00_2252;
						}
					else
						{
							obj_t BgL_auxz00_2443;

							BgL_auxz00_2443 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
								BINT(4542L), BGl_string1827z00zz__regexpz00,
								BGl_string1826z00zz__regexpz00, BgL_rez00_2252);
							FAILURE(BgL_auxz00_2443, BFALSE, BFALSE);
						}
					BgL_tmpz00_2439 =
						BGl_regexpzd2capturezd2countz00zz__regexpz00(BgL_auxz00_2440);
				}
				return BINT(BgL_tmpz00_2439);
			}
		}

	}



/* pregexp */
	BGL_EXPORTED_DEF obj_t BGl_pregexpz00zz__regexpz00(obj_t BgL_rez00_6,
		obj_t BgL_optzd2argszd2_7)
	{
		{	/* Unsafe/pcre.scm 119 */
			return bgl_regcomp(BgL_rez00_6, BgL_optzd2argszd2_7, ((bool_t) 1));
		}

	}



/* &pregexp */
	obj_t BGl_z62pregexpz62zz__regexpz00(obj_t BgL_envz00_2253,
		obj_t BgL_rez00_2254, obj_t BgL_optzd2argszd2_2255)
	{
		{	/* Unsafe/pcre.scm 119 */
			{	/* Unsafe/pcre.scm 120 */
				obj_t BgL_auxz00_2450;

				if (STRINGP(BgL_rez00_2254))
					{	/* Unsafe/pcre.scm 120 */
						BgL_auxz00_2450 = BgL_rez00_2254;
					}
				else
					{
						obj_t BgL_auxz00_2453;

						BgL_auxz00_2453 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(5172L), BGl_string1828z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_rez00_2254);
						FAILURE(BgL_auxz00_2453, BFALSE, BFALSE);
					}
				return
					BGl_pregexpz00zz__regexpz00(BgL_auxz00_2450, BgL_optzd2argszd2_2255);
			}
		}

	}



/* _pregexp-match-positions */
	obj_t BGl__pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t
		BgL_env1119z00_22, obj_t BgL_opt1118z00_21)
	{
		{	/* Unsafe/pcre.scm 136 */
			{	/* Unsafe/pcre.scm 136 */
				obj_t BgL_patz00_1197;
				obj_t BgL_strz00_1198;

				BgL_patz00_1197 = VECTOR_REF(BgL_opt1118z00_21, 0L);
				BgL_strz00_1198 = VECTOR_REF(BgL_opt1118z00_21, 1L);
				switch (VECTOR_LENGTH(BgL_opt1118z00_21))
					{
					case 2L:

						{	/* Unsafe/pcre.scm 136 */
							long BgL_endz00_1202;

							{	/* Unsafe/pcre.scm 136 */
								obj_t BgL_stringz00_1827;

								if (STRINGP(BgL_strz00_1198))
									{	/* Unsafe/pcre.scm 136 */
										BgL_stringz00_1827 = BgL_strz00_1198;
									}
								else
									{
										obj_t BgL_auxz00_2462;

										BgL_auxz00_2462 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1824z00zz__regexpz00, BINT(5973L),
											BGl_string1830z00zz__regexpz00,
											BGl_string1829z00zz__regexpz00, BgL_strz00_1198);
										FAILURE(BgL_auxz00_2462, BFALSE, BFALSE);
									}
								BgL_endz00_1202 = STRING_LENGTH(BgL_stringz00_1827);
							}
							{	/* Unsafe/pcre.scm 136 */

								{	/* Unsafe/pcre.scm 136 */
									obj_t BgL_strz00_1828;

									if (STRINGP(BgL_strz00_1198))
										{	/* Unsafe/pcre.scm 136 */
											BgL_strz00_1828 = BgL_strz00_1198;
										}
									else
										{
											obj_t BgL_auxz00_2469;

											BgL_auxz00_2469 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(5908L),
												BGl_string1830z00zz__regexpz00,
												BGl_string1829z00zz__regexpz00, BgL_strz00_1198);
											FAILURE(BgL_auxz00_2469, BFALSE, BFALSE);
										}
									if (BGL_REGEXPP(BgL_patz00_1197))
										{	/* Unsafe/pcre.scm 127 */
											int BgL_auxz00_2488;
											int BgL_auxz00_2486;
											int BgL_auxz00_2484;
											char *BgL_auxz00_2482;
											obj_t BgL_tmpz00_2475;

											BgL_auxz00_2488 = (int) (0L);
											BgL_auxz00_2486 = (int) (BgL_endz00_1202);
											BgL_auxz00_2484 = (int) (0L);
											BgL_auxz00_2482 = BSTRING_TO_STRING(BgL_strz00_1828);
											if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1197))
												{	/* Unsafe/pcre.scm 127 */
													BgL_tmpz00_2475 = BgL_patz00_1197;
												}
											else
												{
													obj_t BgL_auxz00_2478;

													BgL_auxz00_2478 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1824z00zz__regexpz00, BINT(5525L),
														BGl_string1830z00zz__regexpz00,
														BGl_string1826z00zz__regexpz00, BgL_patz00_1197);
													FAILURE(BgL_auxz00_2478, BFALSE, BFALSE);
												}
											return
												BGL_REGEXP_MATCH(BgL_tmpz00_2475, BgL_auxz00_2482,
												((bool_t) 0), BgL_auxz00_2484, BgL_auxz00_2486,
												BgL_auxz00_2488);
										}
									else
										{	/* Unsafe/pcre.scm 128 */
											obj_t BgL_rxz00_1830;

											{	/* Unsafe/pcre.scm 128 */
												obj_t BgL_tmpz00_2491;

												if (STRINGP(BgL_patz00_1197))
													{	/* Unsafe/pcre.scm 128 */
														BgL_tmpz00_2491 = BgL_patz00_1197;
													}
												else
													{
														obj_t BgL_auxz00_2494;

														BgL_auxz00_2494 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1824z00zz__regexpz00, BINT(5587L),
															BGl_string1830z00zz__regexpz00,
															BGl_string1829z00zz__regexpz00, BgL_patz00_1197);
														FAILURE(BgL_auxz00_2494, BFALSE, BFALSE);
													}
												BgL_rxz00_1830 =
													bgl_regcomp(BgL_tmpz00_2491, BNIL, ((bool_t) 0));
											}
											{	/* Unsafe/pcre.scm 128 */
												obj_t BgL_valz00_1831;

												{	/* Unsafe/pcre.scm 129 */
													int BgL_auxz00_2505;
													int BgL_auxz00_2503;
													int BgL_auxz00_2501;
													char *BgL_tmpz00_2499;

													BgL_auxz00_2505 = (int) (0L);
													BgL_auxz00_2503 = (int) (BgL_endz00_1202);
													BgL_auxz00_2501 = (int) (0L);
													BgL_tmpz00_2499 = BSTRING_TO_STRING(BgL_strz00_1828);
													BgL_valz00_1831 =
														BGL_REGEXP_MATCH(BgL_rxz00_1830, BgL_tmpz00_2499,
														((bool_t) 0), BgL_auxz00_2501, BgL_auxz00_2503,
														BgL_auxz00_2505);
												}
												{	/* Unsafe/pcre.scm 129 */

													BGL_REGEXP_FREE(BgL_rxz00_1830);
													return BgL_valz00_1831;
												}
											}
										}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/pcre.scm 136 */
							obj_t BgL_begz00_1204;

							BgL_begz00_1204 = VECTOR_REF(BgL_opt1118z00_21, 2L);
							{	/* Unsafe/pcre.scm 136 */
								long BgL_endz00_1205;

								{	/* Unsafe/pcre.scm 136 */
									obj_t BgL_stringz00_1832;

									if (STRINGP(BgL_strz00_1198))
										{	/* Unsafe/pcre.scm 136 */
											BgL_stringz00_1832 = BgL_strz00_1198;
										}
									else
										{
											obj_t BgL_auxz00_2512;

											BgL_auxz00_2512 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(5973L),
												BGl_string1830z00zz__regexpz00,
												BGl_string1829z00zz__regexpz00, BgL_strz00_1198);
											FAILURE(BgL_auxz00_2512, BFALSE, BFALSE);
										}
									BgL_endz00_1205 = STRING_LENGTH(BgL_stringz00_1832);
								}
								{	/* Unsafe/pcre.scm 136 */

									{	/* Unsafe/pcre.scm 136 */
										obj_t BgL_strz00_1833;

										if (STRINGP(BgL_strz00_1198))
											{	/* Unsafe/pcre.scm 136 */
												BgL_strz00_1833 = BgL_strz00_1198;
											}
										else
											{
												obj_t BgL_auxz00_2519;

												BgL_auxz00_2519 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1824z00zz__regexpz00, BINT(5908L),
													BGl_string1830z00zz__regexpz00,
													BGl_string1829z00zz__regexpz00, BgL_strz00_1198);
												FAILURE(BgL_auxz00_2519, BFALSE, BFALSE);
											}
										if (BGL_REGEXPP(BgL_patz00_1197))
											{	/* Unsafe/pcre.scm 127 */
												int BgL_auxz00_2545;
												int BgL_auxz00_2543;
												int BgL_auxz00_2534;
												char *BgL_auxz00_2532;
												obj_t BgL_tmpz00_2525;

												BgL_auxz00_2545 = (int) (0L);
												BgL_auxz00_2543 = (int) (BgL_endz00_1205);
												{	/* Unsafe/pcre.scm 127 */
													obj_t BgL_tmpz00_2535;

													if (INTEGERP(BgL_begz00_1204))
														{	/* Unsafe/pcre.scm 127 */
															BgL_tmpz00_2535 = BgL_begz00_1204;
														}
													else
														{
															obj_t BgL_auxz00_2538;

															BgL_auxz00_2538 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(5541L),
																BGl_string1830z00zz__regexpz00,
																BGl_string1831z00zz__regexpz00,
																BgL_begz00_1204);
															FAILURE(BgL_auxz00_2538, BFALSE, BFALSE);
														}
													BgL_auxz00_2534 = CINT(BgL_tmpz00_2535);
												}
												BgL_auxz00_2532 = BSTRING_TO_STRING(BgL_strz00_1833);
												if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1197))
													{	/* Unsafe/pcre.scm 127 */
														BgL_tmpz00_2525 = BgL_patz00_1197;
													}
												else
													{
														obj_t BgL_auxz00_2528;

														BgL_auxz00_2528 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1824z00zz__regexpz00, BINT(5525L),
															BGl_string1830z00zz__regexpz00,
															BGl_string1826z00zz__regexpz00, BgL_patz00_1197);
														FAILURE(BgL_auxz00_2528, BFALSE, BFALSE);
													}
												return
													BGL_REGEXP_MATCH(BgL_tmpz00_2525, BgL_auxz00_2532,
													((bool_t) 0), BgL_auxz00_2534, BgL_auxz00_2543,
													BgL_auxz00_2545);
											}
										else
											{	/* Unsafe/pcre.scm 128 */
												obj_t BgL_rxz00_1835;

												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_tmpz00_2548;

													if (STRINGP(BgL_patz00_1197))
														{	/* Unsafe/pcre.scm 128 */
															BgL_tmpz00_2548 = BgL_patz00_1197;
														}
													else
														{
															obj_t BgL_auxz00_2551;

															BgL_auxz00_2551 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(5587L),
																BGl_string1830z00zz__regexpz00,
																BGl_string1829z00zz__regexpz00,
																BgL_patz00_1197);
															FAILURE(BgL_auxz00_2551, BFALSE, BFALSE);
														}
													BgL_rxz00_1835 =
														bgl_regcomp(BgL_tmpz00_2548, BNIL, ((bool_t) 0));
												}
												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_valz00_1836;

													{	/* Unsafe/pcre.scm 129 */
														int BgL_auxz00_2569;
														int BgL_auxz00_2567;
														int BgL_auxz00_2558;
														char *BgL_tmpz00_2556;

														BgL_auxz00_2569 = (int) (0L);
														BgL_auxz00_2567 = (int) (BgL_endz00_1205);
														{	/* Unsafe/pcre.scm 129 */
															obj_t BgL_tmpz00_2559;

															if (INTEGERP(BgL_begz00_1204))
																{	/* Unsafe/pcre.scm 129 */
																	BgL_tmpz00_2559 = BgL_begz00_1204;
																}
															else
																{
																	obj_t BgL_auxz00_2562;

																	BgL_auxz00_2562 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1824z00zz__regexpz00,
																		BINT(5638L), BGl_string1830z00zz__regexpz00,
																		BGl_string1831z00zz__regexpz00,
																		BgL_begz00_1204);
																	FAILURE(BgL_auxz00_2562, BFALSE, BFALSE);
																}
															BgL_auxz00_2558 = CINT(BgL_tmpz00_2559);
														}
														BgL_tmpz00_2556 =
															BSTRING_TO_STRING(BgL_strz00_1833);
														BgL_valz00_1836 =
															BGL_REGEXP_MATCH(BgL_rxz00_1835, BgL_tmpz00_2556,
															((bool_t) 0), BgL_auxz00_2558, BgL_auxz00_2567,
															BgL_auxz00_2569);
													}
													{	/* Unsafe/pcre.scm 129 */

														BGL_REGEXP_FREE(BgL_rxz00_1835);
														return BgL_valz00_1836;
													}
												}
											}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Unsafe/pcre.scm 136 */
							obj_t BgL_begz00_1207;

							BgL_begz00_1207 = VECTOR_REF(BgL_opt1118z00_21, 2L);
							{	/* Unsafe/pcre.scm 136 */
								obj_t BgL_endz00_1208;

								BgL_endz00_1208 = VECTOR_REF(BgL_opt1118z00_21, 3L);
								{	/* Unsafe/pcre.scm 136 */

									{	/* Unsafe/pcre.scm 136 */
										obj_t BgL_strz00_1837;

										if (STRINGP(BgL_strz00_1198))
											{	/* Unsafe/pcre.scm 136 */
												BgL_strz00_1837 = BgL_strz00_1198;
											}
										else
											{
												obj_t BgL_auxz00_2577;

												BgL_auxz00_2577 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1824z00zz__regexpz00, BINT(5908L),
													BGl_string1830z00zz__regexpz00,
													BGl_string1829z00zz__regexpz00, BgL_strz00_1198);
												FAILURE(BgL_auxz00_2577, BFALSE, BFALSE);
											}
										if (BGL_REGEXPP(BgL_patz00_1197))
											{	/* Unsafe/pcre.scm 127 */
												int BgL_auxz00_2610;
												int BgL_auxz00_2601;
												int BgL_auxz00_2592;
												char *BgL_auxz00_2590;
												obj_t BgL_tmpz00_2583;

												BgL_auxz00_2610 = (int) (0L);
												{	/* Unsafe/pcre.scm 127 */
													obj_t BgL_tmpz00_2602;

													if (INTEGERP(BgL_endz00_1208))
														{	/* Unsafe/pcre.scm 127 */
															BgL_tmpz00_2602 = BgL_endz00_1208;
														}
													else
														{
															obj_t BgL_auxz00_2605;

															BgL_auxz00_2605 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(5545L),
																BGl_string1830z00zz__regexpz00,
																BGl_string1831z00zz__regexpz00,
																BgL_endz00_1208);
															FAILURE(BgL_auxz00_2605, BFALSE, BFALSE);
														}
													BgL_auxz00_2601 = CINT(BgL_tmpz00_2602);
												}
												{	/* Unsafe/pcre.scm 127 */
													obj_t BgL_tmpz00_2593;

													if (INTEGERP(BgL_begz00_1207))
														{	/* Unsafe/pcre.scm 127 */
															BgL_tmpz00_2593 = BgL_begz00_1207;
														}
													else
														{
															obj_t BgL_auxz00_2596;

															BgL_auxz00_2596 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(5541L),
																BGl_string1830z00zz__regexpz00,
																BGl_string1831z00zz__regexpz00,
																BgL_begz00_1207);
															FAILURE(BgL_auxz00_2596, BFALSE, BFALSE);
														}
													BgL_auxz00_2592 = CINT(BgL_tmpz00_2593);
												}
												BgL_auxz00_2590 = BSTRING_TO_STRING(BgL_strz00_1837);
												if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1197))
													{	/* Unsafe/pcre.scm 127 */
														BgL_tmpz00_2583 = BgL_patz00_1197;
													}
												else
													{
														obj_t BgL_auxz00_2586;

														BgL_auxz00_2586 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1824z00zz__regexpz00, BINT(5525L),
															BGl_string1830z00zz__regexpz00,
															BGl_string1826z00zz__regexpz00, BgL_patz00_1197);
														FAILURE(BgL_auxz00_2586, BFALSE, BFALSE);
													}
												return
													BGL_REGEXP_MATCH(BgL_tmpz00_2583, BgL_auxz00_2590,
													((bool_t) 0), BgL_auxz00_2592, BgL_auxz00_2601,
													BgL_auxz00_2610);
											}
										else
											{	/* Unsafe/pcre.scm 128 */
												obj_t BgL_rxz00_1839;

												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_tmpz00_2613;

													if (STRINGP(BgL_patz00_1197))
														{	/* Unsafe/pcre.scm 128 */
															BgL_tmpz00_2613 = BgL_patz00_1197;
														}
													else
														{
															obj_t BgL_auxz00_2616;

															BgL_auxz00_2616 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(5587L),
																BGl_string1830z00zz__regexpz00,
																BGl_string1829z00zz__regexpz00,
																BgL_patz00_1197);
															FAILURE(BgL_auxz00_2616, BFALSE, BFALSE);
														}
													BgL_rxz00_1839 =
														bgl_regcomp(BgL_tmpz00_2613, BNIL, ((bool_t) 0));
												}
												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_valz00_1840;

													{	/* Unsafe/pcre.scm 129 */
														int BgL_auxz00_2641;
														int BgL_auxz00_2632;
														int BgL_auxz00_2623;
														char *BgL_tmpz00_2621;

														BgL_auxz00_2641 = (int) (0L);
														{	/* Unsafe/pcre.scm 129 */
															obj_t BgL_tmpz00_2633;

															if (INTEGERP(BgL_endz00_1208))
																{	/* Unsafe/pcre.scm 129 */
																	BgL_tmpz00_2633 = BgL_endz00_1208;
																}
															else
																{
																	obj_t BgL_auxz00_2636;

																	BgL_auxz00_2636 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1824z00zz__regexpz00,
																		BINT(5642L), BGl_string1830z00zz__regexpz00,
																		BGl_string1831z00zz__regexpz00,
																		BgL_endz00_1208);
																	FAILURE(BgL_auxz00_2636, BFALSE, BFALSE);
																}
															BgL_auxz00_2632 = CINT(BgL_tmpz00_2633);
														}
														{	/* Unsafe/pcre.scm 129 */
															obj_t BgL_tmpz00_2624;

															if (INTEGERP(BgL_begz00_1207))
																{	/* Unsafe/pcre.scm 129 */
																	BgL_tmpz00_2624 = BgL_begz00_1207;
																}
															else
																{
																	obj_t BgL_auxz00_2627;

																	BgL_auxz00_2627 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1824z00zz__regexpz00,
																		BINT(5638L), BGl_string1830z00zz__regexpz00,
																		BGl_string1831z00zz__regexpz00,
																		BgL_begz00_1207);
																	FAILURE(BgL_auxz00_2627, BFALSE, BFALSE);
																}
															BgL_auxz00_2623 = CINT(BgL_tmpz00_2624);
														}
														BgL_tmpz00_2621 =
															BSTRING_TO_STRING(BgL_strz00_1837);
														BgL_valz00_1840 =
															BGL_REGEXP_MATCH(BgL_rxz00_1839, BgL_tmpz00_2621,
															((bool_t) 0), BgL_auxz00_2623, BgL_auxz00_2632,
															BgL_auxz00_2641);
													}
													{	/* Unsafe/pcre.scm 129 */

														BGL_REGEXP_FREE(BgL_rxz00_1839);
														return BgL_valz00_1840;
													}
												}
											}
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Unsafe/pcre.scm 136 */
							obj_t BgL_begz00_1210;

							BgL_begz00_1210 = VECTOR_REF(BgL_opt1118z00_21, 2L);
							{	/* Unsafe/pcre.scm 136 */
								obj_t BgL_endz00_1211;

								BgL_endz00_1211 = VECTOR_REF(BgL_opt1118z00_21, 3L);
								{	/* Unsafe/pcre.scm 136 */
									obj_t BgL_offsetz00_1212;

									BgL_offsetz00_1212 = VECTOR_REF(BgL_opt1118z00_21, 4L);
									{	/* Unsafe/pcre.scm 136 */

										{	/* Unsafe/pcre.scm 136 */
											obj_t BgL_strz00_1841;

											if (STRINGP(BgL_strz00_1198))
												{	/* Unsafe/pcre.scm 136 */
													BgL_strz00_1841 = BgL_strz00_1198;
												}
											else
												{
													obj_t BgL_auxz00_2650;

													BgL_auxz00_2650 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1824z00zz__regexpz00, BINT(5908L),
														BGl_string1830z00zz__regexpz00,
														BGl_string1829z00zz__regexpz00, BgL_strz00_1198);
													FAILURE(BgL_auxz00_2650, BFALSE, BFALSE);
												}
											if (BGL_REGEXPP(BgL_patz00_1197))
												{	/* Unsafe/pcre.scm 127 */
													int BgL_auxz00_2683;
													int BgL_auxz00_2674;
													int BgL_auxz00_2665;
													char *BgL_auxz00_2663;
													obj_t BgL_tmpz00_2656;

													{	/* Unsafe/pcre.scm 127 */
														obj_t BgL_tmpz00_2684;

														if (INTEGERP(BgL_offsetz00_1212))
															{	/* Unsafe/pcre.scm 127 */
																BgL_tmpz00_2684 = BgL_offsetz00_1212;
															}
														else
															{
																obj_t BgL_auxz00_2687;

																BgL_auxz00_2687 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(5549L),
																	BGl_string1830z00zz__regexpz00,
																	BGl_string1831z00zz__regexpz00,
																	BgL_offsetz00_1212);
																FAILURE(BgL_auxz00_2687, BFALSE, BFALSE);
															}
														BgL_auxz00_2683 = CINT(BgL_tmpz00_2684);
													}
													{	/* Unsafe/pcre.scm 127 */
														obj_t BgL_tmpz00_2675;

														if (INTEGERP(BgL_endz00_1211))
															{	/* Unsafe/pcre.scm 127 */
																BgL_tmpz00_2675 = BgL_endz00_1211;
															}
														else
															{
																obj_t BgL_auxz00_2678;

																BgL_auxz00_2678 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(5545L),
																	BGl_string1830z00zz__regexpz00,
																	BGl_string1831z00zz__regexpz00,
																	BgL_endz00_1211);
																FAILURE(BgL_auxz00_2678, BFALSE, BFALSE);
															}
														BgL_auxz00_2674 = CINT(BgL_tmpz00_2675);
													}
													{	/* Unsafe/pcre.scm 127 */
														obj_t BgL_tmpz00_2666;

														if (INTEGERP(BgL_begz00_1210))
															{	/* Unsafe/pcre.scm 127 */
																BgL_tmpz00_2666 = BgL_begz00_1210;
															}
														else
															{
																obj_t BgL_auxz00_2669;

																BgL_auxz00_2669 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(5541L),
																	BGl_string1830z00zz__regexpz00,
																	BGl_string1831z00zz__regexpz00,
																	BgL_begz00_1210);
																FAILURE(BgL_auxz00_2669, BFALSE, BFALSE);
															}
														BgL_auxz00_2665 = CINT(BgL_tmpz00_2666);
													}
													BgL_auxz00_2663 = BSTRING_TO_STRING(BgL_strz00_1841);
													if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1197))
														{	/* Unsafe/pcre.scm 127 */
															BgL_tmpz00_2656 = BgL_patz00_1197;
														}
													else
														{
															obj_t BgL_auxz00_2659;

															BgL_auxz00_2659 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(5525L),
																BGl_string1830z00zz__regexpz00,
																BGl_string1826z00zz__regexpz00,
																BgL_patz00_1197);
															FAILURE(BgL_auxz00_2659, BFALSE, BFALSE);
														}
													return
														BGL_REGEXP_MATCH(BgL_tmpz00_2656, BgL_auxz00_2663,
														((bool_t) 0), BgL_auxz00_2665, BgL_auxz00_2674,
														BgL_auxz00_2683);
												}
											else
												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_rxz00_1843;

													{	/* Unsafe/pcre.scm 128 */
														obj_t BgL_tmpz00_2693;

														if (STRINGP(BgL_patz00_1197))
															{	/* Unsafe/pcre.scm 128 */
																BgL_tmpz00_2693 = BgL_patz00_1197;
															}
														else
															{
																obj_t BgL_auxz00_2696;

																BgL_auxz00_2696 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(5587L),
																	BGl_string1830z00zz__regexpz00,
																	BGl_string1829z00zz__regexpz00,
																	BgL_patz00_1197);
																FAILURE(BgL_auxz00_2696, BFALSE, BFALSE);
															}
														BgL_rxz00_1843 =
															bgl_regcomp(BgL_tmpz00_2693, BNIL, ((bool_t) 0));
													}
													{	/* Unsafe/pcre.scm 128 */
														obj_t BgL_valz00_1844;

														{	/* Unsafe/pcre.scm 129 */
															int BgL_auxz00_2721;
															int BgL_auxz00_2712;
															int BgL_auxz00_2703;
															char *BgL_tmpz00_2701;

															{	/* Unsafe/pcre.scm 129 */
																obj_t BgL_tmpz00_2722;

																if (INTEGERP(BgL_offsetz00_1212))
																	{	/* Unsafe/pcre.scm 129 */
																		BgL_tmpz00_2722 = BgL_offsetz00_1212;
																	}
																else
																	{
																		obj_t BgL_auxz00_2725;

																		BgL_auxz00_2725 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1824z00zz__regexpz00,
																			BINT(5646L),
																			BGl_string1830z00zz__regexpz00,
																			BGl_string1831z00zz__regexpz00,
																			BgL_offsetz00_1212);
																		FAILURE(BgL_auxz00_2725, BFALSE, BFALSE);
																	}
																BgL_auxz00_2721 = CINT(BgL_tmpz00_2722);
															}
															{	/* Unsafe/pcre.scm 129 */
																obj_t BgL_tmpz00_2713;

																if (INTEGERP(BgL_endz00_1211))
																	{	/* Unsafe/pcre.scm 129 */
																		BgL_tmpz00_2713 = BgL_endz00_1211;
																	}
																else
																	{
																		obj_t BgL_auxz00_2716;

																		BgL_auxz00_2716 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1824z00zz__regexpz00,
																			BINT(5642L),
																			BGl_string1830z00zz__regexpz00,
																			BGl_string1831z00zz__regexpz00,
																			BgL_endz00_1211);
																		FAILURE(BgL_auxz00_2716, BFALSE, BFALSE);
																	}
																BgL_auxz00_2712 = CINT(BgL_tmpz00_2713);
															}
															{	/* Unsafe/pcre.scm 129 */
																obj_t BgL_tmpz00_2704;

																if (INTEGERP(BgL_begz00_1210))
																	{	/* Unsafe/pcre.scm 129 */
																		BgL_tmpz00_2704 = BgL_begz00_1210;
																	}
																else
																	{
																		obj_t BgL_auxz00_2707;

																		BgL_auxz00_2707 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1824z00zz__regexpz00,
																			BINT(5638L),
																			BGl_string1830z00zz__regexpz00,
																			BGl_string1831z00zz__regexpz00,
																			BgL_begz00_1210);
																		FAILURE(BgL_auxz00_2707, BFALSE, BFALSE);
																	}
																BgL_auxz00_2703 = CINT(BgL_tmpz00_2704);
															}
															BgL_tmpz00_2701 =
																BSTRING_TO_STRING(BgL_strz00_1841);
															BgL_valz00_1844 =
																BGL_REGEXP_MATCH(BgL_rxz00_1843,
																BgL_tmpz00_2701, ((bool_t) 0), BgL_auxz00_2703,
																BgL_auxz00_2712, BgL_auxz00_2721);
														}
														{	/* Unsafe/pcre.scm 129 */

															BGL_REGEXP_FREE(BgL_rxz00_1843);
															return BgL_valz00_1844;
														}
													}
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match-positions */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t
		BgL_patz00_16, obj_t BgL_strz00_17, obj_t BgL_begz00_18,
		obj_t BgL_endz00_19, obj_t BgL_offsetz00_20)
	{
		{	/* Unsafe/pcre.scm 136 */
			if (BGL_REGEXPP(BgL_patz00_16))
				{	/* Unsafe/pcre.scm 127 */
					int BgL_auxz00_2742;
					int BgL_auxz00_2740;
					int BgL_auxz00_2738;
					char *BgL_tmpz00_2736;

					BgL_auxz00_2742 = CINT(BgL_offsetz00_20);
					BgL_auxz00_2740 = CINT(BgL_endz00_19);
					BgL_auxz00_2738 = CINT(BgL_begz00_18);
					BgL_tmpz00_2736 = BSTRING_TO_STRING(BgL_strz00_17);
					return
						BGL_REGEXP_MATCH(BgL_patz00_16, BgL_tmpz00_2736, ((bool_t) 0),
						BgL_auxz00_2738, BgL_auxz00_2740, BgL_auxz00_2742);
				}
			else
				{	/* Unsafe/pcre.scm 128 */
					obj_t BgL_rxz00_1846;

					BgL_rxz00_1846 = bgl_regcomp(BgL_patz00_16, BNIL, ((bool_t) 0));
					{	/* Unsafe/pcre.scm 128 */
						obj_t BgL_valz00_1847;

						{	/* Unsafe/pcre.scm 129 */
							int BgL_auxz00_2752;
							int BgL_auxz00_2750;
							int BgL_auxz00_2748;
							char *BgL_tmpz00_2746;

							BgL_auxz00_2752 = CINT(BgL_offsetz00_20);
							BgL_auxz00_2750 = CINT(BgL_endz00_19);
							BgL_auxz00_2748 = CINT(BgL_begz00_18);
							BgL_tmpz00_2746 = BSTRING_TO_STRING(BgL_strz00_17);
							BgL_valz00_1847 =
								BGL_REGEXP_MATCH(BgL_rxz00_1846, BgL_tmpz00_2746, ((bool_t) 0),
								BgL_auxz00_2748, BgL_auxz00_2750, BgL_auxz00_2752);
						}
						{	/* Unsafe/pcre.scm 129 */

							BGL_REGEXP_FREE(BgL_rxz00_1846);
							return BgL_valz00_1847;
						}
					}
				}
		}

	}



/* _pregexp-match-n-positions! */
	obj_t BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t
		BgL_env1123z00_30, obj_t BgL_opt1122z00_29)
	{
		{	/* Unsafe/pcre.scm 142 */
			{	/* Unsafe/pcre.scm 142 */
				obj_t BgL_g1124z00_2372;
				obj_t BgL_g1125z00_2373;
				obj_t BgL_g1126z00_2374;
				obj_t BgL_g1127z00_2375;
				obj_t BgL_g1128z00_2376;

				BgL_g1124z00_2372 = VECTOR_REF(BgL_opt1122z00_29, 0L);
				BgL_g1125z00_2373 = VECTOR_REF(BgL_opt1122z00_29, 1L);
				BgL_g1126z00_2374 = VECTOR_REF(BgL_opt1122z00_29, 2L);
				BgL_g1127z00_2375 = VECTOR_REF(BgL_opt1122z00_29, 3L);
				BgL_g1128z00_2376 = VECTOR_REF(BgL_opt1122z00_29, 4L);
				switch (VECTOR_LENGTH(BgL_opt1122z00_29))
					{
					case 5L:

						{	/* Unsafe/pcre.scm 142 */

							{	/* Unsafe/pcre.scm 142 */
								obj_t BgL_patz00_2377;
								obj_t BgL_strz00_2378;
								obj_t BgL_vresz00_2379;
								long BgL_begz00_2380;
								long BgL_endz00_2381;

								if (BGl_regexpzf3zf3zz__regexpz00(BgL_g1124z00_2372))
									{	/* Unsafe/pcre.scm 142 */
										BgL_patz00_2377 = BgL_g1124z00_2372;
									}
								else
									{
										obj_t BgL_auxz00_2763;

										BgL_auxz00_2763 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1824z00zz__regexpz00, BINT(6267L),
											BGl_string1832z00zz__regexpz00,
											BGl_string1826z00zz__regexpz00, BgL_g1124z00_2372);
										FAILURE(BgL_auxz00_2763, BFALSE, BFALSE);
									}
								if (STRINGP(BgL_g1125z00_2373))
									{	/* Unsafe/pcre.scm 142 */
										BgL_strz00_2378 = BgL_g1125z00_2373;
									}
								else
									{
										obj_t BgL_auxz00_2769;

										BgL_auxz00_2769 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1824z00zz__regexpz00, BINT(6267L),
											BGl_string1832z00zz__regexpz00,
											BGl_string1829z00zz__regexpz00, BgL_g1125z00_2373);
										FAILURE(BgL_auxz00_2769, BFALSE, BFALSE);
									}
								if (VECTORP(BgL_g1126z00_2374))
									{	/* Unsafe/pcre.scm 142 */
										BgL_vresz00_2379 = BgL_g1126z00_2374;
									}
								else
									{
										obj_t BgL_auxz00_2775;

										BgL_auxz00_2775 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1824z00zz__regexpz00, BINT(6267L),
											BGl_string1832z00zz__regexpz00,
											BGl_string1833z00zz__regexpz00, BgL_g1126z00_2374);
										FAILURE(BgL_auxz00_2775, BFALSE, BFALSE);
									}
								{	/* Unsafe/pcre.scm 142 */
									obj_t BgL_tmpz00_2779;

									if (INTEGERP(BgL_g1127z00_2375))
										{	/* Unsafe/pcre.scm 142 */
											BgL_tmpz00_2779 = BgL_g1127z00_2375;
										}
									else
										{
											obj_t BgL_auxz00_2782;

											BgL_auxz00_2782 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6267L),
												BGl_string1832z00zz__regexpz00,
												BGl_string1831z00zz__regexpz00, BgL_g1127z00_2375);
											FAILURE(BgL_auxz00_2782, BFALSE, BFALSE);
										}
									BgL_begz00_2380 = (long) CINT(BgL_tmpz00_2779);
								}
								{	/* Unsafe/pcre.scm 142 */
									obj_t BgL_tmpz00_2787;

									if (INTEGERP(BgL_g1128z00_2376))
										{	/* Unsafe/pcre.scm 142 */
											BgL_tmpz00_2787 = BgL_g1128z00_2376;
										}
									else
										{
											obj_t BgL_auxz00_2790;

											BgL_auxz00_2790 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6267L),
												BGl_string1832z00zz__regexpz00,
												BGl_string1831z00zz__regexpz00, BgL_g1128z00_2376);
											FAILURE(BgL_auxz00_2790, BFALSE, BFALSE);
										}
									BgL_endz00_2381 = (long) CINT(BgL_tmpz00_2787);
								}
								{	/* Unsafe/pcre.scm 143 */
									long BgL_tmpz00_2795;

									{	/* Unsafe/pcre.scm 143 */
										int BgL_auxz00_2802;
										int BgL_auxz00_2800;
										int BgL_auxz00_2798;
										char *BgL_tmpz00_2796;

										BgL_auxz00_2802 = (int) (0L);
										BgL_auxz00_2800 = (int) (BgL_endz00_2381);
										BgL_auxz00_2798 = (int) (BgL_begz00_2380);
										BgL_tmpz00_2796 = BSTRING_TO_STRING(BgL_strz00_2378);
										BgL_tmpz00_2795 =
											BGL_REGEXP_MATCH_N(BgL_patz00_2377, BgL_tmpz00_2796,
											BgL_vresz00_2379, BgL_auxz00_2798, BgL_auxz00_2800,
											BgL_auxz00_2802);
									}
									return BINT(BgL_tmpz00_2795);
								}
							}
						}
						break;
					case 6L:

						{	/* Unsafe/pcre.scm 142 */
							obj_t BgL_offsetz00_2382;

							BgL_offsetz00_2382 = VECTOR_REF(BgL_opt1122z00_29, 5L);
							{	/* Unsafe/pcre.scm 142 */

								{	/* Unsafe/pcre.scm 142 */
									obj_t BgL_patz00_2383;
									obj_t BgL_strz00_2384;
									obj_t BgL_vresz00_2385;
									long BgL_begz00_2386;
									long BgL_endz00_2387;

									if (BGl_regexpzf3zf3zz__regexpz00(BgL_g1124z00_2372))
										{	/* Unsafe/pcre.scm 142 */
											BgL_patz00_2383 = BgL_g1124z00_2372;
										}
									else
										{
											obj_t BgL_auxz00_2809;

											BgL_auxz00_2809 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6267L),
												BGl_string1832z00zz__regexpz00,
												BGl_string1826z00zz__regexpz00, BgL_g1124z00_2372);
											FAILURE(BgL_auxz00_2809, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1125z00_2373))
										{	/* Unsafe/pcre.scm 142 */
											BgL_strz00_2384 = BgL_g1125z00_2373;
										}
									else
										{
											obj_t BgL_auxz00_2815;

											BgL_auxz00_2815 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6267L),
												BGl_string1832z00zz__regexpz00,
												BGl_string1829z00zz__regexpz00, BgL_g1125z00_2373);
											FAILURE(BgL_auxz00_2815, BFALSE, BFALSE);
										}
									if (VECTORP(BgL_g1126z00_2374))
										{	/* Unsafe/pcre.scm 142 */
											BgL_vresz00_2385 = BgL_g1126z00_2374;
										}
									else
										{
											obj_t BgL_auxz00_2821;

											BgL_auxz00_2821 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6267L),
												BGl_string1832z00zz__regexpz00,
												BGl_string1833z00zz__regexpz00, BgL_g1126z00_2374);
											FAILURE(BgL_auxz00_2821, BFALSE, BFALSE);
										}
									{	/* Unsafe/pcre.scm 142 */
										obj_t BgL_tmpz00_2825;

										if (INTEGERP(BgL_g1127z00_2375))
											{	/* Unsafe/pcre.scm 142 */
												BgL_tmpz00_2825 = BgL_g1127z00_2375;
											}
										else
											{
												obj_t BgL_auxz00_2828;

												BgL_auxz00_2828 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1824z00zz__regexpz00, BINT(6267L),
													BGl_string1832z00zz__regexpz00,
													BGl_string1831z00zz__regexpz00, BgL_g1127z00_2375);
												FAILURE(BgL_auxz00_2828, BFALSE, BFALSE);
											}
										BgL_begz00_2386 = (long) CINT(BgL_tmpz00_2825);
									}
									{	/* Unsafe/pcre.scm 142 */
										obj_t BgL_tmpz00_2833;

										if (INTEGERP(BgL_g1128z00_2376))
											{	/* Unsafe/pcre.scm 142 */
												BgL_tmpz00_2833 = BgL_g1128z00_2376;
											}
										else
											{
												obj_t BgL_auxz00_2836;

												BgL_auxz00_2836 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1824z00zz__regexpz00, BINT(6267L),
													BGl_string1832z00zz__regexpz00,
													BGl_string1831z00zz__regexpz00, BgL_g1128z00_2376);
												FAILURE(BgL_auxz00_2836, BFALSE, BFALSE);
											}
										BgL_endz00_2387 = (long) CINT(BgL_tmpz00_2833);
									}
									{	/* Unsafe/pcre.scm 143 */
										long BgL_tmpz00_2841;

										{	/* Unsafe/pcre.scm 143 */
											int BgL_auxz00_2848;
											int BgL_auxz00_2846;
											int BgL_auxz00_2844;
											char *BgL_tmpz00_2842;

											{	/* Unsafe/pcre.scm 143 */
												obj_t BgL_tmpz00_2849;

												if (INTEGERP(BgL_offsetz00_2382))
													{	/* Unsafe/pcre.scm 143 */
														BgL_tmpz00_2849 = BgL_offsetz00_2382;
													}
												else
													{
														obj_t BgL_auxz00_2852;

														BgL_auxz00_2852 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1824z00zz__regexpz00, BINT(6399L),
															BGl_string1832z00zz__regexpz00,
															BGl_string1831z00zz__regexpz00,
															BgL_offsetz00_2382);
														FAILURE(BgL_auxz00_2852, BFALSE, BFALSE);
													}
												BgL_auxz00_2848 = CINT(BgL_tmpz00_2849);
											}
											BgL_auxz00_2846 = (int) (BgL_endz00_2387);
											BgL_auxz00_2844 = (int) (BgL_begz00_2386);
											BgL_tmpz00_2842 = BSTRING_TO_STRING(BgL_strz00_2384);
											BgL_tmpz00_2841 =
												BGL_REGEXP_MATCH_N(BgL_patz00_2383, BgL_tmpz00_2842,
												BgL_vresz00_2385, BgL_auxz00_2844, BgL_auxz00_2846,
												BgL_auxz00_2848);
										}
										return BINT(BgL_tmpz00_2841);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match-n-positions! */
	BGL_EXPORTED_DEF long
		BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t BgL_patz00_23,
		obj_t BgL_strz00_24, obj_t BgL_vresz00_25, long BgL_begz00_26,
		long BgL_endz00_27, obj_t BgL_offsetz00_28)
	{
		{	/* Unsafe/pcre.scm 142 */
			{	/* Unsafe/pcre.scm 143 */
				int BgL_auxz00_2867;
				int BgL_auxz00_2865;
				int BgL_auxz00_2863;
				char *BgL_tmpz00_2861;

				BgL_auxz00_2867 = CINT(BgL_offsetz00_28);
				BgL_auxz00_2865 = (int) (BgL_endz00_27);
				BgL_auxz00_2863 = (int) (BgL_begz00_26);
				BgL_tmpz00_2861 = BSTRING_TO_STRING(BgL_strz00_24);
				return
					BGL_REGEXP_MATCH_N(BgL_patz00_23, BgL_tmpz00_2861, BgL_vresz00_25,
					BgL_auxz00_2863, BgL_auxz00_2865, BgL_auxz00_2867);
			}
		}

	}



/* _pregexp-match */
	obj_t BGl__pregexpzd2matchzd2zz__regexpz00(obj_t BgL_env1132z00_36,
		obj_t BgL_opt1131z00_35)
	{
		{	/* Unsafe/pcre.scm 148 */
			{	/* Unsafe/pcre.scm 148 */
				obj_t BgL_patz00_1222;
				obj_t BgL_strz00_1223;

				BgL_patz00_1222 = VECTOR_REF(BgL_opt1131z00_35, 0L);
				BgL_strz00_1223 = VECTOR_REF(BgL_opt1131z00_35, 1L);
				switch (VECTOR_LENGTH(BgL_opt1131z00_35))
					{
					case 2L:

						{	/* Unsafe/pcre.scm 148 */
							long BgL_endz00_1227;

							{	/* Unsafe/pcre.scm 148 */
								obj_t BgL_stringz00_1867;

								if (STRINGP(BgL_strz00_1223))
									{	/* Unsafe/pcre.scm 148 */
										BgL_stringz00_1867 = BgL_strz00_1223;
									}
								else
									{
										obj_t BgL_auxz00_2874;

										BgL_auxz00_2874 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1824z00zz__regexpz00, BINT(6686L),
											BGl_string1834z00zz__regexpz00,
											BGl_string1829z00zz__regexpz00, BgL_strz00_1223);
										FAILURE(BgL_auxz00_2874, BFALSE, BFALSE);
									}
								BgL_endz00_1227 = STRING_LENGTH(BgL_stringz00_1867);
							}
							{	/* Unsafe/pcre.scm 148 */

								{	/* Unsafe/pcre.scm 148 */
									obj_t BgL_strz00_1868;

									if (STRINGP(BgL_strz00_1223))
										{	/* Unsafe/pcre.scm 148 */
											BgL_strz00_1868 = BgL_strz00_1223;
										}
									else
										{
											obj_t BgL_auxz00_2881;

											BgL_auxz00_2881 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6631L),
												BGl_string1834z00zz__regexpz00,
												BGl_string1829z00zz__regexpz00, BgL_strz00_1223);
											FAILURE(BgL_auxz00_2881, BFALSE, BFALSE);
										}
									{	/* Unsafe/pcre.scm 149 */

										if (BGL_REGEXPP(BgL_patz00_1222))
											{	/* Unsafe/pcre.scm 127 */
												int BgL_auxz00_2900;
												int BgL_auxz00_2898;
												int BgL_auxz00_2896;
												char *BgL_auxz00_2894;
												obj_t BgL_tmpz00_2887;

												BgL_auxz00_2900 = (int) (0L);
												BgL_auxz00_2898 = (int) (BgL_endz00_1227);
												BgL_auxz00_2896 = (int) (0L);
												BgL_auxz00_2894 = BSTRING_TO_STRING(BgL_strz00_1868);
												if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1222))
													{	/* Unsafe/pcre.scm 149 */
														BgL_tmpz00_2887 = BgL_patz00_1222;
													}
												else
													{
														obj_t BgL_auxz00_2890;

														BgL_auxz00_2890 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1824z00zz__regexpz00, BINT(6718L),
															BGl_string1834z00zz__regexpz00,
															BGl_string1826z00zz__regexpz00, BgL_patz00_1222);
														FAILURE(BgL_auxz00_2890, BFALSE, BFALSE);
													}
												return
													BGL_REGEXP_MATCH(BgL_tmpz00_2887, BgL_auxz00_2894,
													((bool_t) 1), BgL_auxz00_2896, BgL_auxz00_2898,
													BgL_auxz00_2900);
											}
										else
											{	/* Unsafe/pcre.scm 128 */
												obj_t BgL_rxz00_1876;

												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_tmpz00_2903;

													if (STRINGP(BgL_patz00_1222))
														{	/* Unsafe/pcre.scm 149 */
															BgL_tmpz00_2903 = BgL_patz00_1222;
														}
													else
														{
															obj_t BgL_auxz00_2906;

															BgL_auxz00_2906 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(6718L),
																BGl_string1834z00zz__regexpz00,
																BGl_string1829z00zz__regexpz00,
																BgL_patz00_1222);
															FAILURE(BgL_auxz00_2906, BFALSE, BFALSE);
														}
													BgL_rxz00_1876 =
														bgl_regcomp(BgL_tmpz00_2903, BNIL, ((bool_t) 0));
												}
												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_valz00_1877;

													{	/* Unsafe/pcre.scm 129 */
														int BgL_auxz00_2917;
														int BgL_auxz00_2915;
														int BgL_auxz00_2913;
														char *BgL_tmpz00_2911;

														BgL_auxz00_2917 = (int) (0L);
														BgL_auxz00_2915 = (int) (BgL_endz00_1227);
														BgL_auxz00_2913 = (int) (0L);
														BgL_tmpz00_2911 =
															BSTRING_TO_STRING(BgL_strz00_1868);
														BgL_valz00_1877 =
															BGL_REGEXP_MATCH(BgL_rxz00_1876, BgL_tmpz00_2911,
															((bool_t) 1), BgL_auxz00_2913, BgL_auxz00_2915,
															BgL_auxz00_2917);
													}
													{	/* Unsafe/pcre.scm 129 */

														BGL_REGEXP_FREE(BgL_rxz00_1876);
														return BgL_valz00_1877;
													}
												}
											}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/pcre.scm 148 */
							obj_t BgL_begz00_1228;

							BgL_begz00_1228 = VECTOR_REF(BgL_opt1131z00_35, 2L);
							{	/* Unsafe/pcre.scm 148 */
								long BgL_endz00_1229;

								{	/* Unsafe/pcre.scm 148 */
									obj_t BgL_stringz00_1878;

									if (STRINGP(BgL_strz00_1223))
										{	/* Unsafe/pcre.scm 148 */
											BgL_stringz00_1878 = BgL_strz00_1223;
										}
									else
										{
											obj_t BgL_auxz00_2924;

											BgL_auxz00_2924 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1824z00zz__regexpz00, BINT(6686L),
												BGl_string1834z00zz__regexpz00,
												BGl_string1829z00zz__regexpz00, BgL_strz00_1223);
											FAILURE(BgL_auxz00_2924, BFALSE, BFALSE);
										}
									BgL_endz00_1229 = STRING_LENGTH(BgL_stringz00_1878);
								}
								{	/* Unsafe/pcre.scm 148 */

									{	/* Unsafe/pcre.scm 148 */
										obj_t BgL_strz00_1879;

										if (STRINGP(BgL_strz00_1223))
											{	/* Unsafe/pcre.scm 148 */
												BgL_strz00_1879 = BgL_strz00_1223;
											}
										else
											{
												obj_t BgL_auxz00_2931;

												BgL_auxz00_2931 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1824z00zz__regexpz00, BINT(6631L),
													BGl_string1834z00zz__regexpz00,
													BGl_string1829z00zz__regexpz00, BgL_strz00_1223);
												FAILURE(BgL_auxz00_2931, BFALSE, BFALSE);
											}
										{	/* Unsafe/pcre.scm 149 */

											if (BGL_REGEXPP(BgL_patz00_1222))
												{	/* Unsafe/pcre.scm 127 */
													int BgL_auxz00_2957;
													int BgL_auxz00_2955;
													int BgL_auxz00_2946;
													char *BgL_auxz00_2944;
													obj_t BgL_tmpz00_2937;

													BgL_auxz00_2957 = (int) (0L);
													BgL_auxz00_2955 = (int) (BgL_endz00_1229);
													{	/* Unsafe/pcre.scm 149 */
														obj_t BgL_tmpz00_2947;

														if (INTEGERP(BgL_begz00_1228))
															{	/* Unsafe/pcre.scm 149 */
																BgL_tmpz00_2947 = BgL_begz00_1228;
															}
														else
															{
																obj_t BgL_auxz00_2950;

																BgL_auxz00_2950 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(6729L),
																	BGl_string1834z00zz__regexpz00,
																	BGl_string1831z00zz__regexpz00,
																	BgL_begz00_1228);
																FAILURE(BgL_auxz00_2950, BFALSE, BFALSE);
															}
														BgL_auxz00_2946 = CINT(BgL_tmpz00_2947);
													}
													BgL_auxz00_2944 = BSTRING_TO_STRING(BgL_strz00_1879);
													if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1222))
														{	/* Unsafe/pcre.scm 149 */
															BgL_tmpz00_2937 = BgL_patz00_1222;
														}
													else
														{
															obj_t BgL_auxz00_2940;

															BgL_auxz00_2940 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(6718L),
																BGl_string1834z00zz__regexpz00,
																BGl_string1826z00zz__regexpz00,
																BgL_patz00_1222);
															FAILURE(BgL_auxz00_2940, BFALSE, BFALSE);
														}
													return
														BGL_REGEXP_MATCH(BgL_tmpz00_2937, BgL_auxz00_2944,
														((bool_t) 1), BgL_auxz00_2946, BgL_auxz00_2955,
														BgL_auxz00_2957);
												}
											else
												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_rxz00_1887;

													{	/* Unsafe/pcre.scm 128 */
														obj_t BgL_tmpz00_2960;

														if (STRINGP(BgL_patz00_1222))
															{	/* Unsafe/pcre.scm 149 */
																BgL_tmpz00_2960 = BgL_patz00_1222;
															}
														else
															{
																obj_t BgL_auxz00_2963;

																BgL_auxz00_2963 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(6718L),
																	BGl_string1834z00zz__regexpz00,
																	BGl_string1829z00zz__regexpz00,
																	BgL_patz00_1222);
																FAILURE(BgL_auxz00_2963, BFALSE, BFALSE);
															}
														BgL_rxz00_1887 =
															bgl_regcomp(BgL_tmpz00_2960, BNIL, ((bool_t) 0));
													}
													{	/* Unsafe/pcre.scm 128 */
														obj_t BgL_valz00_1888;

														{	/* Unsafe/pcre.scm 129 */
															int BgL_auxz00_2981;
															int BgL_auxz00_2979;
															int BgL_auxz00_2970;
															char *BgL_tmpz00_2968;

															BgL_auxz00_2981 = (int) (0L);
															BgL_auxz00_2979 = (int) (BgL_endz00_1229);
															{	/* Unsafe/pcre.scm 149 */
																obj_t BgL_tmpz00_2971;

																if (INTEGERP(BgL_begz00_1228))
																	{	/* Unsafe/pcre.scm 149 */
																		BgL_tmpz00_2971 = BgL_begz00_1228;
																	}
																else
																	{
																		obj_t BgL_auxz00_2974;

																		BgL_auxz00_2974 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1824z00zz__regexpz00,
																			BINT(6729L),
																			BGl_string1834z00zz__regexpz00,
																			BGl_string1831z00zz__regexpz00,
																			BgL_begz00_1228);
																		FAILURE(BgL_auxz00_2974, BFALSE, BFALSE);
																	}
																BgL_auxz00_2970 = CINT(BgL_tmpz00_2971);
															}
															BgL_tmpz00_2968 =
																BSTRING_TO_STRING(BgL_strz00_1879);
															BgL_valz00_1888 =
																BGL_REGEXP_MATCH(BgL_rxz00_1887,
																BgL_tmpz00_2968, ((bool_t) 1), BgL_auxz00_2970,
																BgL_auxz00_2979, BgL_auxz00_2981);
														}
														{	/* Unsafe/pcre.scm 129 */

															BGL_REGEXP_FREE(BgL_rxz00_1887);
															return BgL_valz00_1888;
														}
													}
												}
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Unsafe/pcre.scm 148 */
							obj_t BgL_begz00_1230;

							BgL_begz00_1230 = VECTOR_REF(BgL_opt1131z00_35, 2L);
							{	/* Unsafe/pcre.scm 148 */
								obj_t BgL_endz00_1231;

								BgL_endz00_1231 = VECTOR_REF(BgL_opt1131z00_35, 3L);
								{	/* Unsafe/pcre.scm 148 */

									{	/* Unsafe/pcre.scm 148 */
										obj_t BgL_strz00_1889;

										if (STRINGP(BgL_strz00_1223))
											{	/* Unsafe/pcre.scm 148 */
												BgL_strz00_1889 = BgL_strz00_1223;
											}
										else
											{
												obj_t BgL_auxz00_2989;

												BgL_auxz00_2989 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1824z00zz__regexpz00, BINT(6631L),
													BGl_string1834z00zz__regexpz00,
													BGl_string1829z00zz__regexpz00, BgL_strz00_1223);
												FAILURE(BgL_auxz00_2989, BFALSE, BFALSE);
											}
										{	/* Unsafe/pcre.scm 149 */

											if (BGL_REGEXPP(BgL_patz00_1222))
												{	/* Unsafe/pcre.scm 127 */
													int BgL_auxz00_3022;
													int BgL_auxz00_3013;
													int BgL_auxz00_3004;
													char *BgL_auxz00_3002;
													obj_t BgL_tmpz00_2995;

													BgL_auxz00_3022 = (int) (0L);
													{	/* Unsafe/pcre.scm 149 */
														obj_t BgL_tmpz00_3014;

														if (INTEGERP(BgL_endz00_1231))
															{	/* Unsafe/pcre.scm 149 */
																BgL_tmpz00_3014 = BgL_endz00_1231;
															}
														else
															{
																obj_t BgL_auxz00_3017;

																BgL_auxz00_3017 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(6733L),
																	BGl_string1834z00zz__regexpz00,
																	BGl_string1831z00zz__regexpz00,
																	BgL_endz00_1231);
																FAILURE(BgL_auxz00_3017, BFALSE, BFALSE);
															}
														BgL_auxz00_3013 = CINT(BgL_tmpz00_3014);
													}
													{	/* Unsafe/pcre.scm 149 */
														obj_t BgL_tmpz00_3005;

														if (INTEGERP(BgL_begz00_1230))
															{	/* Unsafe/pcre.scm 149 */
																BgL_tmpz00_3005 = BgL_begz00_1230;
															}
														else
															{
																obj_t BgL_auxz00_3008;

																BgL_auxz00_3008 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(6729L),
																	BGl_string1834z00zz__regexpz00,
																	BGl_string1831z00zz__regexpz00,
																	BgL_begz00_1230);
																FAILURE(BgL_auxz00_3008, BFALSE, BFALSE);
															}
														BgL_auxz00_3004 = CINT(BgL_tmpz00_3005);
													}
													BgL_auxz00_3002 = BSTRING_TO_STRING(BgL_strz00_1889);
													if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1222))
														{	/* Unsafe/pcre.scm 149 */
															BgL_tmpz00_2995 = BgL_patz00_1222;
														}
													else
														{
															obj_t BgL_auxz00_2998;

															BgL_auxz00_2998 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1824z00zz__regexpz00, BINT(6718L),
																BGl_string1834z00zz__regexpz00,
																BGl_string1826z00zz__regexpz00,
																BgL_patz00_1222);
															FAILURE(BgL_auxz00_2998, BFALSE, BFALSE);
														}
													return
														BGL_REGEXP_MATCH(BgL_tmpz00_2995, BgL_auxz00_3002,
														((bool_t) 1), BgL_auxz00_3004, BgL_auxz00_3013,
														BgL_auxz00_3022);
												}
											else
												{	/* Unsafe/pcre.scm 128 */
													obj_t BgL_rxz00_1897;

													{	/* Unsafe/pcre.scm 128 */
														obj_t BgL_tmpz00_3025;

														if (STRINGP(BgL_patz00_1222))
															{	/* Unsafe/pcre.scm 149 */
																BgL_tmpz00_3025 = BgL_patz00_1222;
															}
														else
															{
																obj_t BgL_auxz00_3028;

																BgL_auxz00_3028 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1824z00zz__regexpz00, BINT(6718L),
																	BGl_string1834z00zz__regexpz00,
																	BGl_string1829z00zz__regexpz00,
																	BgL_patz00_1222);
																FAILURE(BgL_auxz00_3028, BFALSE, BFALSE);
															}
														BgL_rxz00_1897 =
															bgl_regcomp(BgL_tmpz00_3025, BNIL, ((bool_t) 0));
													}
													{	/* Unsafe/pcre.scm 128 */
														obj_t BgL_valz00_1898;

														{	/* Unsafe/pcre.scm 129 */
															int BgL_auxz00_3053;
															int BgL_auxz00_3044;
															int BgL_auxz00_3035;
															char *BgL_tmpz00_3033;

															BgL_auxz00_3053 = (int) (0L);
															{	/* Unsafe/pcre.scm 149 */
																obj_t BgL_tmpz00_3045;

																if (INTEGERP(BgL_endz00_1231))
																	{	/* Unsafe/pcre.scm 149 */
																		BgL_tmpz00_3045 = BgL_endz00_1231;
																	}
																else
																	{
																		obj_t BgL_auxz00_3048;

																		BgL_auxz00_3048 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1824z00zz__regexpz00,
																			BINT(6733L),
																			BGl_string1834z00zz__regexpz00,
																			BGl_string1831z00zz__regexpz00,
																			BgL_endz00_1231);
																		FAILURE(BgL_auxz00_3048, BFALSE, BFALSE);
																	}
																BgL_auxz00_3044 = CINT(BgL_tmpz00_3045);
															}
															{	/* Unsafe/pcre.scm 149 */
																obj_t BgL_tmpz00_3036;

																if (INTEGERP(BgL_begz00_1230))
																	{	/* Unsafe/pcre.scm 149 */
																		BgL_tmpz00_3036 = BgL_begz00_1230;
																	}
																else
																	{
																		obj_t BgL_auxz00_3039;

																		BgL_auxz00_3039 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1824z00zz__regexpz00,
																			BINT(6729L),
																			BGl_string1834z00zz__regexpz00,
																			BGl_string1831z00zz__regexpz00,
																			BgL_begz00_1230);
																		FAILURE(BgL_auxz00_3039, BFALSE, BFALSE);
																	}
																BgL_auxz00_3035 = CINT(BgL_tmpz00_3036);
															}
															BgL_tmpz00_3033 =
																BSTRING_TO_STRING(BgL_strz00_1889);
															BgL_valz00_1898 =
																BGL_REGEXP_MATCH(BgL_rxz00_1897,
																BgL_tmpz00_3033, ((bool_t) 1), BgL_auxz00_3035,
																BgL_auxz00_3044, BgL_auxz00_3053);
														}
														{	/* Unsafe/pcre.scm 129 */

															BGL_REGEXP_FREE(BgL_rxz00_1897);
															return BgL_valz00_1898;
														}
													}
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2matchzd2zz__regexpz00(obj_t
		BgL_patz00_31, obj_t BgL_strz00_32, obj_t BgL_begz00_33,
		obj_t BgL_endz00_34)
	{
		{	/* Unsafe/pcre.scm 148 */
			{	/* Unsafe/pcre.scm 149 */

				if (BGL_REGEXPP(BgL_patz00_31))
					{	/* Unsafe/pcre.scm 127 */
						int BgL_auxz00_3067;
						int BgL_auxz00_3065;
						int BgL_auxz00_3063;
						char *BgL_tmpz00_3061;

						BgL_auxz00_3067 = (int) (0L);
						BgL_auxz00_3065 = CINT(BgL_endz00_34);
						BgL_auxz00_3063 = CINT(BgL_begz00_33);
						BgL_tmpz00_3061 = BSTRING_TO_STRING(BgL_strz00_32);
						return
							BGL_REGEXP_MATCH(BgL_patz00_31, BgL_tmpz00_3061, ((bool_t) 1),
							BgL_auxz00_3063, BgL_auxz00_3065, BgL_auxz00_3067);
					}
				else
					{	/* Unsafe/pcre.scm 128 */
						obj_t BgL_rxz00_1906;

						BgL_rxz00_1906 = bgl_regcomp(BgL_patz00_31, BNIL, ((bool_t) 0));
						{	/* Unsafe/pcre.scm 128 */
							obj_t BgL_valz00_1907;

							{	/* Unsafe/pcre.scm 129 */
								int BgL_auxz00_3077;
								int BgL_auxz00_3075;
								int BgL_auxz00_3073;
								char *BgL_tmpz00_3071;

								BgL_auxz00_3077 = (int) (0L);
								BgL_auxz00_3075 = CINT(BgL_endz00_34);
								BgL_auxz00_3073 = CINT(BgL_begz00_33);
								BgL_tmpz00_3071 = BSTRING_TO_STRING(BgL_strz00_32);
								BgL_valz00_1907 =
									BGL_REGEXP_MATCH(BgL_rxz00_1906, BgL_tmpz00_3071,
									((bool_t) 1), BgL_auxz00_3073, BgL_auxz00_3075,
									BgL_auxz00_3077);
							}
							{	/* Unsafe/pcre.scm 129 */

								BGL_REGEXP_FREE(BgL_rxz00_1906);
								return BgL_valz00_1907;
							}
						}
					}
			}
		}

	}



/* pregexp-read-escaped-number */
	obj_t BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00(obj_t BgL_sz00_37,
		obj_t BgL_iz00_38, long BgL_nz00_39)
	{
		{	/* Unsafe/pcre.scm 154 */
			if ((((long) CINT(BgL_iz00_38) + 1L) < BgL_nz00_39))
				{	/* Unsafe/pcre.scm 157 */
					unsigned char BgL_cz00_1239;

					BgL_cz00_1239 =
						STRING_REF(BgL_sz00_37, ((long) CINT(BgL_iz00_38) + 1L));
					if (isdigit(BgL_cz00_1239))
						{	/* Unsafe/pcre.scm 159 */
							long BgL_g1049z00_1241;
							obj_t BgL_g1050z00_1242;

							BgL_g1049z00_1241 = ((long) CINT(BgL_iz00_38) + 2L);
							{	/* Unsafe/pcre.scm 159 */
								obj_t BgL_list1227z00_1267;

								BgL_list1227z00_1267 =
									MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1239), BNIL);
								BgL_g1050z00_1242 = BgL_list1227z00_1267;
							}
							{
								long BgL_iz00_1244;
								obj_t BgL_rz00_1245;

								BgL_iz00_1244 = BgL_g1049z00_1241;
								BgL_rz00_1245 = BgL_g1050z00_1242;
							BgL_zc3z04anonymousza31205ze3z87_1246:
								if ((BgL_iz00_1244 >= BgL_nz00_39))
									{	/* Unsafe/pcre.scm 161 */
										obj_t BgL_arg1208z00_1248;

										{	/* Unsafe/pcre.scm 161 */
											obj_t BgL_arg1212z00_1251;

											BgL_arg1212z00_1251 =
												BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
												(bgl_reverse_bang(BgL_rz00_1245));
											{	/* Ieee/number.scm 175 */

												BgL_arg1208z00_1248 =
													BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
													(BgL_arg1212z00_1251, BINT(10L));
											}
										}
										{	/* Unsafe/pcre.scm 161 */
											obj_t BgL_list1209z00_1249;

											{	/* Unsafe/pcre.scm 161 */
												obj_t BgL_arg1210z00_1250;

												BgL_arg1210z00_1250 =
													MAKE_YOUNG_PAIR(BINT(BgL_iz00_1244), BNIL);
												BgL_list1209z00_1249 =
													MAKE_YOUNG_PAIR(BgL_arg1208z00_1248,
													BgL_arg1210z00_1250);
											}
											return BgL_list1209z00_1249;
										}
									}
								else
									{	/* Unsafe/pcre.scm 162 */
										unsigned char BgL_cz00_1255;

										BgL_cz00_1255 = STRING_REF(BgL_sz00_37, BgL_iz00_1244);
										if (isdigit(BgL_cz00_1255))
											{	/* Unsafe/pcre.scm 164 */
												long BgL_arg1218z00_1257;
												obj_t BgL_arg1219z00_1258;

												BgL_arg1218z00_1257 = (BgL_iz00_1244 + 1L);
												BgL_arg1219z00_1258 =
													MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1255), BgL_rz00_1245);
												{
													obj_t BgL_rz00_3110;
													long BgL_iz00_3109;

													BgL_iz00_3109 = BgL_arg1218z00_1257;
													BgL_rz00_3110 = BgL_arg1219z00_1258;
													BgL_rz00_1245 = BgL_rz00_3110;
													BgL_iz00_1244 = BgL_iz00_3109;
													goto BgL_zc3z04anonymousza31205ze3z87_1246;
												}
											}
										else
											{	/* Unsafe/pcre.scm 165 */
												obj_t BgL_arg1220z00_1259;

												{	/* Unsafe/pcre.scm 165 */
													obj_t BgL_arg1225z00_1262;

													BgL_arg1225z00_1262 =
														BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
														(bgl_reverse_bang(BgL_rz00_1245));
													{	/* Ieee/number.scm 175 */

														BgL_arg1220z00_1259 =
															BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
															(BgL_arg1225z00_1262, BINT(10L));
													}
												}
												{	/* Unsafe/pcre.scm 165 */
													obj_t BgL_list1221z00_1260;

													{	/* Unsafe/pcre.scm 165 */
														obj_t BgL_arg1223z00_1261;

														BgL_arg1223z00_1261 =
															MAKE_YOUNG_PAIR(BINT(BgL_iz00_1244), BNIL);
														BgL_list1221z00_1260 =
															MAKE_YOUNG_PAIR(BgL_arg1220z00_1259,
															BgL_arg1223z00_1261);
													}
													return BgL_list1221z00_1260;
												}
											}
									}
							}
						}
					else
						{	/* Unsafe/pcre.scm 158 */
							return BFALSE;
						}
				}
			else
				{	/* Unsafe/pcre.scm 156 */
					return BFALSE;
				}
		}

	}



/* pregexp-list-ref */
	obj_t BGl_pregexpzd2listzd2refz00zz__regexpz00(obj_t BgL_sz00_40,
		obj_t BgL_iz00_41)
	{
		{	/* Unsafe/pcre.scm 171 */
			{
				obj_t BgL_sz00_1944;
				long BgL_kz00_1945;

				BgL_sz00_1944 = BgL_sz00_40;
				BgL_kz00_1945 = 0L;
			BgL_loopz00_1943:
				if (NULLP(BgL_sz00_1944))
					{	/* Unsafe/pcre.scm 175 */
						return BFALSE;
					}
				else
					{	/* Unsafe/pcre.scm 175 */
						if ((BgL_kz00_1945 == (long) CINT(BgL_iz00_41)))
							{	/* Unsafe/pcre.scm 176 */
								return CAR(((obj_t) BgL_sz00_1944));
							}
						else
							{
								long BgL_kz00_3128;
								obj_t BgL_sz00_3125;

								BgL_sz00_3125 = CDR(((obj_t) BgL_sz00_1944));
								BgL_kz00_3128 = (BgL_kz00_1945 + 1L);
								BgL_kz00_1945 = BgL_kz00_3128;
								BgL_sz00_1944 = BgL_sz00_3125;
								goto BgL_loopz00_1943;
							}
					}
			}
		}

	}



/* pregexp-replace-aux */
	obj_t BGl_pregexpzd2replacezd2auxz00zz__regexpz00(obj_t BgL_strz00_42,
		obj_t BgL_insz00_43, long BgL_nz00_44, obj_t BgL_backrefsz00_45)
	{
		{	/* Unsafe/pcre.scm 182 */
			{
				obj_t BgL_iz00_1280;
				obj_t BgL_rz00_1281;

				BgL_iz00_1280 = BINT(0L);
				BgL_rz00_1281 = BGl_string1835z00zz__regexpz00;
			BgL_zc3z04anonymousza31235ze3z87_1282:
				if (((long) CINT(BgL_iz00_1280) >= BgL_nz00_44))
					{	/* Unsafe/pcre.scm 184 */
						return BgL_rz00_1281;
					}
				else
					{	/* Unsafe/pcre.scm 185 */
						unsigned char BgL_cz00_1284;

						BgL_cz00_1284 =
							STRING_REF(BgL_insz00_43, (long) CINT(BgL_iz00_1280));
						if ((BgL_cz00_1284 == ((unsigned char) '\\')))
							{	/* Unsafe/pcre.scm 187 */
								obj_t BgL_brzd2izd2_1286;

								BgL_brzd2izd2_1286 =
									BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00
									(BgL_insz00_43, BgL_iz00_1280, BgL_nz00_44);
								{	/* Unsafe/pcre.scm 187 */
									obj_t BgL_brz00_1287;

									if (CBOOL(BgL_brzd2izd2_1286))
										{	/* Unsafe/pcre.scm 188 */
											BgL_brz00_1287 = CAR(((obj_t) BgL_brzd2izd2_1286));
										}
									else
										{	/* Unsafe/pcre.scm 188 */
											if (
												(STRING_REF(BgL_insz00_43,
														((long) CINT(BgL_iz00_1280) + 1L)) ==
													((unsigned char) '&')))
												{	/* Unsafe/pcre.scm 189 */
													BgL_brz00_1287 = BINT(0L);
												}
											else
												{	/* Unsafe/pcre.scm 189 */
													BgL_brz00_1287 = BFALSE;
												}
										}
									{	/* Unsafe/pcre.scm 188 */
										obj_t BgL_iz00_1288;

										if (CBOOL(BgL_brzd2izd2_1286))
											{	/* Unsafe/pcre.scm 191 */
												obj_t BgL_pairz00_1974;

												BgL_pairz00_1974 = CDR(((obj_t) BgL_brzd2izd2_1286));
												BgL_iz00_1288 = CAR(BgL_pairz00_1974);
											}
										else
											{	/* Unsafe/pcre.scm 191 */
												if (CBOOL(BgL_brz00_1287))
													{	/* Unsafe/pcre.scm 192 */
														BgL_iz00_1288 = ADDFX(BgL_iz00_1280, BINT(2L));
													}
												else
													{	/* Unsafe/pcre.scm 192 */
														BgL_iz00_1288 = ADDFX(BgL_iz00_1280, BINT(1L));
													}
											}
										{	/* Unsafe/pcre.scm 191 */

											if (CBOOL(BgL_brz00_1287))
												{	/* Unsafe/pcre.scm 200 */
													obj_t BgL_arg1238z00_1289;

													{	/* Unsafe/pcre.scm 200 */
														obj_t BgL_backrefz00_1290;

														BgL_backrefz00_1290 =
															BGl_pregexpzd2listzd2refz00zz__regexpz00
															(BgL_backrefsz00_45, BgL_brz00_1287);
														if (CBOOL(BgL_backrefz00_1290))
															{	/* Unsafe/pcre.scm 203 */
																obj_t BgL_arg1239z00_1291;

																{	/* Unsafe/pcre.scm 203 */
																	obj_t BgL_arg1242z00_1292;
																	obj_t BgL_arg1244z00_1293;

																	BgL_arg1242z00_1292 =
																		CAR(((obj_t) BgL_backrefz00_1290));
																	BgL_arg1244z00_1293 =
																		CDR(((obj_t) BgL_backrefz00_1290));
																	BgL_arg1239z00_1291 =
																		c_substring(BgL_strz00_42,
																		(long) CINT(BgL_arg1242z00_1292),
																		(long) CINT(BgL_arg1244z00_1293));
																}
																BgL_arg1238z00_1289 =
																	string_append(BgL_rz00_1281,
																	BgL_arg1239z00_1291);
															}
														else
															{	/* Unsafe/pcre.scm 201 */
																BgL_arg1238z00_1289 = BgL_rz00_1281;
															}
													}
													{
														obj_t BgL_rz00_3173;
														obj_t BgL_iz00_3172;

														BgL_iz00_3172 = BgL_iz00_1288;
														BgL_rz00_3173 = BgL_arg1238z00_1289;
														BgL_rz00_1281 = BgL_rz00_3173;
														BgL_iz00_1280 = BgL_iz00_3172;
														goto BgL_zc3z04anonymousza31235ze3z87_1282;
													}
												}
											else
												{	/* Unsafe/pcre.scm 195 */
													unsigned char BgL_c2z00_1294;

													BgL_c2z00_1294 =
														STRING_REF(BgL_insz00_43,
														(long) CINT(BgL_iz00_1288));
													{	/* Unsafe/pcre.scm 196 */
														long BgL_arg1248z00_1295;
														obj_t BgL_arg1249z00_1296;

														BgL_arg1248z00_1295 =
															((long) CINT(BgL_iz00_1288) + 1L);
														if ((BgL_c2z00_1294 == ((unsigned char) '$')))
															{	/* Unsafe/pcre.scm 197 */
																BgL_arg1249z00_1296 = BgL_rz00_1281;
															}
														else
															{	/* Unsafe/pcre.scm 198 */
																obj_t BgL_arg1252z00_1298;

																{	/* Unsafe/pcre.scm 198 */
																	obj_t BgL_list1253z00_1299;

																	BgL_list1253z00_1299 =
																		MAKE_YOUNG_PAIR(BCHAR(BgL_c2z00_1294),
																		BNIL);
																	BgL_arg1252z00_1298 =
																		BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
																		(BgL_list1253z00_1299);
																}
																BgL_arg1249z00_1296 =
																	string_append(BgL_rz00_1281,
																	BgL_arg1252z00_1298);
															}
														{
															obj_t BgL_rz00_3186;
															obj_t BgL_iz00_3184;

															BgL_iz00_3184 = BINT(BgL_arg1248z00_1295);
															BgL_rz00_3186 = BgL_arg1249z00_1296;
															BgL_rz00_1281 = BgL_rz00_3186;
															BgL_iz00_1280 = BgL_iz00_3184;
															goto BgL_zc3z04anonymousza31235ze3z87_1282;
														}
													}
												}
										}
									}
								}
							}
						else
							{	/* Unsafe/pcre.scm 205 */
								long BgL_arg1305z00_1305;
								obj_t BgL_arg1306z00_1306;

								BgL_arg1305z00_1305 = ((long) CINT(BgL_iz00_1280) + 1L);
								{	/* Unsafe/pcre.scm 205 */
									obj_t BgL_arg1307z00_1307;

									{	/* Unsafe/pcre.scm 205 */
										obj_t BgL_list1308z00_1308;

										BgL_list1308z00_1308 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1284), BNIL);
										BgL_arg1307z00_1307 =
											BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
											(BgL_list1308z00_1308);
									}
									BgL_arg1306z00_1306 =
										string_append(BgL_rz00_1281, BgL_arg1307z00_1307);
								}
								{
									obj_t BgL_rz00_3195;
									obj_t BgL_iz00_3193;

									BgL_iz00_3193 = BINT(BgL_arg1305z00_1305);
									BgL_rz00_3195 = BgL_arg1306z00_1306;
									BgL_rz00_1281 = BgL_rz00_3195;
									BgL_iz00_1280 = BgL_iz00_3193;
									goto BgL_zc3z04anonymousza31235ze3z87_1282;
								}
							}
					}
			}
		}

	}



/* pregexp-split */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2splitzd2zz__regexpz00(obj_t
		BgL_patz00_46, obj_t BgL_strz00_47)
	{
		{	/* Unsafe/pcre.scm 210 */
			{	/* Unsafe/pcre.scm 212 */
				long BgL_nz00_1310;

				BgL_nz00_1310 = STRING_LENGTH(BgL_strz00_47);
				{
					obj_t BgL_iz00_1313;
					obj_t BgL_rz00_1314;
					bool_t BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315;

					BgL_iz00_1313 = BINT(0L);
					BgL_rz00_1314 = BNIL;
					BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315 = ((bool_t) 0);
				BgL_zc3z04anonymousza31309ze3z87_1316:
					if (((long) CINT(BgL_iz00_1313) >= BgL_nz00_1310))
						{	/* Unsafe/pcre.scm 214 */
							return bgl_reverse_bang(BgL_rz00_1314);
						}
					else
						{	/* Unsafe/pcre.scm 215 */
							obj_t BgL_g1052z00_1318;

							{	/* Unsafe/pcre.scm 76 */

								if (BGL_REGEXPP(BgL_patz00_46))
									{	/* Unsafe/pcre.scm 127 */
										int BgL_auxz00_3210;
										int BgL_auxz00_3208;
										int BgL_auxz00_3206;
										char *BgL_tmpz00_3204;

										BgL_auxz00_3210 = (int) (0L);
										BgL_auxz00_3208 = (int) (BgL_nz00_1310);
										BgL_auxz00_3206 = CINT(BgL_iz00_1313);
										BgL_tmpz00_3204 = BSTRING_TO_STRING(BgL_strz00_47);
										BgL_g1052z00_1318 =
											BGL_REGEXP_MATCH(BgL_patz00_46, BgL_tmpz00_3204,
											((bool_t) 0), BgL_auxz00_3206, BgL_auxz00_3208,
											BgL_auxz00_3210);
									}
								else
									{	/* Unsafe/pcre.scm 128 */
										obj_t BgL_rxz00_1993;

										BgL_rxz00_1993 =
											bgl_regcomp(BgL_patz00_46, BNIL, ((bool_t) 0));
										{	/* Unsafe/pcre.scm 128 */
											obj_t BgL_valz00_1994;

											{	/* Unsafe/pcre.scm 129 */
												int BgL_auxz00_3220;
												int BgL_auxz00_3218;
												int BgL_auxz00_3216;
												char *BgL_tmpz00_3214;

												BgL_auxz00_3220 = (int) (0L);
												BgL_auxz00_3218 = (int) (BgL_nz00_1310);
												BgL_auxz00_3216 = CINT(BgL_iz00_1313);
												BgL_tmpz00_3214 = BSTRING_TO_STRING(BgL_strz00_47);
												BgL_valz00_1994 =
													BGL_REGEXP_MATCH(BgL_rxz00_1993, BgL_tmpz00_3214,
													((bool_t) 0), BgL_auxz00_3216, BgL_auxz00_3218,
													BgL_auxz00_3220);
											}
											{	/* Unsafe/pcre.scm 129 */

												BGL_REGEXP_FREE(BgL_rxz00_1993);
												BgL_g1052z00_1318 = BgL_valz00_1994;
							}}}}
							if (CBOOL(BgL_g1052z00_1318))
								{	/* Unsafe/pcre.scm 218 */
									obj_t BgL_jkz00_1321;

									BgL_jkz00_1321 = CAR(((obj_t) BgL_g1052z00_1318));
									{	/* Unsafe/pcre.scm 219 */
										obj_t BgL_jz00_1322;
										obj_t BgL_kz00_1323;

										BgL_jz00_1322 = CAR(((obj_t) BgL_jkz00_1321));
										BgL_kz00_1323 = CDR(((obj_t) BgL_jkz00_1321));
										if (
											((long) CINT(BgL_jz00_1322) ==
												(long) CINT(BgL_kz00_1323)))
											{	/* Unsafe/pcre.scm 221 */
												long BgL_arg1312z00_1325;
												obj_t BgL_arg1314z00_1326;

												BgL_arg1312z00_1325 = ((long) CINT(BgL_kz00_1323) + 1L);
												BgL_arg1314z00_1326 =
													MAKE_YOUNG_PAIR(c_substring(BgL_strz00_47,
														(long) CINT(BgL_iz00_1313),
														((long) CINT(BgL_jz00_1322) + 1L)), BgL_rz00_1314);
												{
													bool_t
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3246;
													obj_t BgL_rz00_3245;
													obj_t BgL_iz00_3243;

													BgL_iz00_3243 = BINT(BgL_arg1312z00_1325);
													BgL_rz00_3245 = BgL_arg1314z00_1326;
													BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3246
														= ((bool_t) 1);
													BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315
														=
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3246;
													BgL_rz00_1314 = BgL_rz00_3245;
													BgL_iz00_1313 = BgL_iz00_3243;
													goto BgL_zc3z04anonymousza31309ze3z87_1316;
												}
											}
										else
											{	/* Unsafe/pcre.scm 223 */
												bool_t BgL_test1956z00_3247;

												if (
													((long) CINT(BgL_jz00_1322) ==
														(long) CINT(BgL_iz00_1313)))
													{	/* Unsafe/pcre.scm 223 */
														BgL_test1956z00_3247 =
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315;
													}
												else
													{	/* Unsafe/pcre.scm 223 */
														BgL_test1956z00_3247 = ((bool_t) 0);
													}
												if (BgL_test1956z00_3247)
													{
														bool_t
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3253;
														obj_t BgL_iz00_3252;

														BgL_iz00_3252 = BgL_kz00_1323;
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3253
															= ((bool_t) 0);
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315
															=
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3253;
														BgL_iz00_1313 = BgL_iz00_3252;
														goto BgL_zc3z04anonymousza31309ze3z87_1316;
													}
												else
													{	/* Unsafe/pcre.scm 226 */
														obj_t BgL_arg1319z00_1331;

														BgL_arg1319z00_1331 =
															MAKE_YOUNG_PAIR(c_substring(BgL_strz00_47,
																(long) CINT(BgL_iz00_1313),
																(long) CINT(BgL_jz00_1322)), BgL_rz00_1314);
														{
															bool_t
																BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3260;
															obj_t BgL_rz00_3259;
															obj_t BgL_iz00_3258;

															BgL_iz00_3258 = BgL_kz00_1323;
															BgL_rz00_3259 = BgL_arg1319z00_1331;
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3260
																= ((bool_t) 0);
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315
																=
																BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3260;
															BgL_rz00_1314 = BgL_rz00_3259;
															BgL_iz00_1313 = BgL_iz00_3258;
															goto BgL_zc3z04anonymousza31309ze3z87_1316;
														}
													}
											}
									}
								}
							else
								{	/* Unsafe/pcre.scm 227 */
									obj_t BgL_arg1321z00_1334;

									BgL_arg1321z00_1334 =
										MAKE_YOUNG_PAIR(c_substring(BgL_strz00_47,
											(long) CINT(BgL_iz00_1313), BgL_nz00_1310),
										BgL_rz00_1314);
									{
										bool_t
											BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3267;
										obj_t BgL_rz00_3266;
										obj_t BgL_iz00_3264;

										BgL_iz00_3264 = BINT(BgL_nz00_1310);
										BgL_rz00_3266 = BgL_arg1321z00_1334;
										BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3267 =
											((bool_t) 0);
										BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1315 =
											BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3267;
										BgL_rz00_1314 = BgL_rz00_3266;
										BgL_iz00_1313 = BgL_iz00_3264;
										goto BgL_zc3z04anonymousza31309ze3z87_1316;
									}
								}
						}
				}
			}
		}

	}



/* &pregexp-split */
	obj_t BGl_z62pregexpzd2splitzb0zz__regexpz00(obj_t BgL_envz00_2256,
		obj_t BgL_patz00_2257, obj_t BgL_strz00_2258)
	{
		{	/* Unsafe/pcre.scm 210 */
			{	/* Unsafe/pcre.scm 212 */
				obj_t BgL_auxz00_3269;

				if (STRINGP(BgL_strz00_2258))
					{	/* Unsafe/pcre.scm 212 */
						BgL_auxz00_3269 = BgL_strz00_2258;
					}
				else
					{
						obj_t BgL_auxz00_3272;

						BgL_auxz00_3272 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(9157L), BGl_string1836z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_strz00_2258);
						FAILURE(BgL_auxz00_3272, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2splitzd2zz__regexpz00(BgL_patz00_2257, BgL_auxz00_3269);
			}
		}

	}



/* pregexp-replace */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2replacezd2zz__regexpz00(obj_t
		BgL_patz00_48, obj_t BgL_strz00_49, obj_t BgL_insz00_50)
	{
		{	/* Unsafe/pcre.scm 232 */
			{	/* Unsafe/pcre.scm 233 */
				long BgL_nz00_1342;

				BgL_nz00_1342 = STRING_LENGTH(BgL_strz00_49);
				{	/* Unsafe/pcre.scm 233 */
					obj_t BgL_ppz00_1343;

					{	/* Unsafe/pcre.scm 76 */

						if (BGL_REGEXPP(BgL_patz00_48))
							{	/* Unsafe/pcre.scm 127 */
								int BgL_auxz00_3286;
								int BgL_auxz00_3284;
								int BgL_auxz00_3282;
								char *BgL_tmpz00_3280;

								BgL_auxz00_3286 = (int) (0L);
								BgL_auxz00_3284 = (int) (BgL_nz00_1342);
								BgL_auxz00_3282 = (int) (0L);
								BgL_tmpz00_3280 = BSTRING_TO_STRING(BgL_strz00_49);
								BgL_ppz00_1343 =
									BGL_REGEXP_MATCH(BgL_patz00_48, BgL_tmpz00_3280, ((bool_t) 0),
									BgL_auxz00_3282, BgL_auxz00_3284, BgL_auxz00_3286);
							}
						else
							{	/* Unsafe/pcre.scm 128 */
								obj_t BgL_rxz00_2016;

								BgL_rxz00_2016 = bgl_regcomp(BgL_patz00_48, BNIL, ((bool_t) 0));
								{	/* Unsafe/pcre.scm 128 */
									obj_t BgL_valz00_2017;

									{	/* Unsafe/pcre.scm 129 */
										int BgL_auxz00_3296;
										int BgL_auxz00_3294;
										int BgL_auxz00_3292;
										char *BgL_tmpz00_3290;

										BgL_auxz00_3296 = (int) (0L);
										BgL_auxz00_3294 = (int) (BgL_nz00_1342);
										BgL_auxz00_3292 = (int) (0L);
										BgL_tmpz00_3290 = BSTRING_TO_STRING(BgL_strz00_49);
										BgL_valz00_2017 =
											BGL_REGEXP_MATCH(BgL_rxz00_2016, BgL_tmpz00_3290,
											((bool_t) 0), BgL_auxz00_3292, BgL_auxz00_3294,
											BgL_auxz00_3296);
									}
									{	/* Unsafe/pcre.scm 129 */

										BGL_REGEXP_FREE(BgL_rxz00_2016);
										BgL_ppz00_1343 = BgL_valz00_2017;
					}}}}
					{	/* Unsafe/pcre.scm 234 */

						if (CBOOL(BgL_ppz00_1343))
							{	/* Unsafe/pcre.scm 236 */
								long BgL_inszd2lenzd2_1344;
								obj_t BgL_mzd2izd2_1345;
								obj_t BgL_mzd2nzd2_1346;

								BgL_inszd2lenzd2_1344 = STRING_LENGTH(BgL_insz00_50);
								{	/* Unsafe/pcre.scm 237 */
									obj_t BgL_pairz00_2022;

									BgL_pairz00_2022 = CAR(((obj_t) BgL_ppz00_1343));
									BgL_mzd2izd2_1345 = CAR(BgL_pairz00_2022);
								}
								{	/* Unsafe/pcre.scm 238 */
									obj_t BgL_pairz00_2026;

									BgL_pairz00_2026 = CAR(((obj_t) BgL_ppz00_1343));
									BgL_mzd2nzd2_1346 = CDR(BgL_pairz00_2026);
								}
								return
									string_append_3(c_substring(BgL_strz00_49, 0L,
										(long) CINT(BgL_mzd2izd2_1345)),
									BGl_pregexpzd2replacezd2auxz00zz__regexpz00(BgL_strz00_49,
										BgL_insz00_50, BgL_inszd2lenzd2_1344, BgL_ppz00_1343),
									c_substring(BgL_strz00_49, (long) CINT(BgL_mzd2nzd2_1346),
										BgL_nz00_1342));
							}
						else
							{	/* Unsafe/pcre.scm 235 */
								return BgL_strz00_49;
							}
					}
				}
			}
		}

	}



/* &pregexp-replace */
	obj_t BGl_z62pregexpzd2replacezb0zz__regexpz00(obj_t BgL_envz00_2259,
		obj_t BgL_patz00_2260, obj_t BgL_strz00_2261, obj_t BgL_insz00_2262)
	{
		{	/* Unsafe/pcre.scm 232 */
			{	/* Unsafe/pcre.scm 233 */
				obj_t BgL_auxz00_3322;
				obj_t BgL_auxz00_3315;

				if (STRINGP(BgL_insz00_2262))
					{	/* Unsafe/pcre.scm 233 */
						BgL_auxz00_3322 = BgL_insz00_2262;
					}
				else
					{
						obj_t BgL_auxz00_3325;

						BgL_auxz00_3325 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(9973L), BGl_string1837z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_insz00_2262);
						FAILURE(BgL_auxz00_3325, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_strz00_2261))
					{	/* Unsafe/pcre.scm 233 */
						BgL_auxz00_3315 = BgL_strz00_2261;
					}
				else
					{
						obj_t BgL_auxz00_3318;

						BgL_auxz00_3318 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(9973L), BGl_string1837z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_strz00_2261);
						FAILURE(BgL_auxz00_3318, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2replacezd2zz__regexpz00(BgL_patz00_2260,
					BgL_auxz00_3315, BgL_auxz00_3322);
			}
		}

	}



/* pregexp-replace* */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t
		BgL_patz00_51, obj_t BgL_strz00_52, obj_t BgL_insz00_53)
	{
		{	/* Unsafe/pcre.scm 247 */
			{	/* Unsafe/pcre.scm 250 */
				obj_t BgL_patz00_1355;
				long BgL_nz00_1356;
				long BgL_inszd2lenzd2_1357;

				if (STRINGP(BgL_patz00_51))
					{	/* Unsafe/pcre.scm 250 */
						BgL_patz00_1355 = bgl_regcomp(BgL_patz00_51, BNIL, ((bool_t) 1));
					}
				else
					{	/* Unsafe/pcre.scm 250 */
						BgL_patz00_1355 = BgL_patz00_51;
					}
				BgL_nz00_1356 = STRING_LENGTH(BgL_strz00_52);
				BgL_inszd2lenzd2_1357 = STRING_LENGTH(BgL_insz00_53);
				{
					obj_t BgL_iz00_1359;
					obj_t BgL_rz00_1360;

					BgL_iz00_1359 = BINT(0L);
					BgL_rz00_1360 = BGl_string1835z00zz__regexpz00;
				BgL_zc3z04anonymousza31327ze3z87_1361:
					if (((long) CINT(BgL_iz00_1359) >= BgL_nz00_1356))
						{	/* Unsafe/pcre.scm 256 */
							return BgL_rz00_1360;
						}
					else
						{	/* Unsafe/pcre.scm 257 */
							obj_t BgL_ppz00_1363;

							{	/* Unsafe/pcre.scm 76 */

								if (BGL_REGEXPP(BgL_patz00_1355))
									{	/* Unsafe/pcre.scm 127 */
										int BgL_auxz00_3346;
										int BgL_auxz00_3344;
										int BgL_auxz00_3342;
										char *BgL_tmpz00_3340;

										BgL_auxz00_3346 = (int) (0L);
										BgL_auxz00_3344 = (int) (BgL_nz00_1356);
										BgL_auxz00_3342 = CINT(BgL_iz00_1359);
										BgL_tmpz00_3340 = BSTRING_TO_STRING(BgL_strz00_52);
										BgL_ppz00_1363 =
											BGL_REGEXP_MATCH(BgL_patz00_1355, BgL_tmpz00_3340,
											((bool_t) 0), BgL_auxz00_3342, BgL_auxz00_3344,
											BgL_auxz00_3346);
									}
								else
									{	/* Unsafe/pcre.scm 128 */
										obj_t BgL_rxz00_2039;

										BgL_rxz00_2039 =
											bgl_regcomp(BgL_patz00_1355, BNIL, ((bool_t) 0));
										{	/* Unsafe/pcre.scm 128 */
											obj_t BgL_valz00_2040;

											{	/* Unsafe/pcre.scm 129 */
												int BgL_auxz00_3356;
												int BgL_auxz00_3354;
												int BgL_auxz00_3352;
												char *BgL_tmpz00_3350;

												BgL_auxz00_3356 = (int) (0L);
												BgL_auxz00_3354 = (int) (BgL_nz00_1356);
												BgL_auxz00_3352 = CINT(BgL_iz00_1359);
												BgL_tmpz00_3350 = BSTRING_TO_STRING(BgL_strz00_52);
												BgL_valz00_2040 =
													BGL_REGEXP_MATCH(BgL_rxz00_2039, BgL_tmpz00_3350,
													((bool_t) 0), BgL_auxz00_3352, BgL_auxz00_3354,
													BgL_auxz00_3356);
											}
											{	/* Unsafe/pcre.scm 129 */

												BGL_REGEXP_FREE(BgL_rxz00_2039);
												BgL_ppz00_1363 = BgL_valz00_2040;
							}}}}
							if (CBOOL(BgL_ppz00_1363))
								{	/* Unsafe/pcre.scm 268 */
									obj_t BgL_arg1329z00_1364;
									obj_t BgL_arg1331z00_1365;

									{	/* Unsafe/pcre.scm 268 */
										obj_t BgL_pairz00_2044;

										BgL_pairz00_2044 = CAR(((obj_t) BgL_ppz00_1363));
										BgL_arg1329z00_1364 = CDR(BgL_pairz00_2044);
									}
									{	/* Unsafe/pcre.scm 271 */
										obj_t BgL_arg1332z00_1366;
										obj_t BgL_arg1333z00_1367;

										{	/* Unsafe/pcre.scm 271 */
											obj_t BgL_arg1334z00_1368;

											{	/* Unsafe/pcre.scm 271 */
												obj_t BgL_pairz00_2048;

												BgL_pairz00_2048 = CAR(((obj_t) BgL_ppz00_1363));
												BgL_arg1334z00_1368 = CAR(BgL_pairz00_2048);
											}
											BgL_arg1332z00_1366 =
												c_substring(BgL_strz00_52,
												(long) CINT(BgL_iz00_1359),
												(long) CINT(BgL_arg1334z00_1368));
										}
										BgL_arg1333z00_1367 =
											BGl_pregexpzd2replacezd2auxz00zz__regexpz00(BgL_strz00_52,
											BgL_insz00_53, BgL_inszd2lenzd2_1357, BgL_ppz00_1363);
										BgL_arg1331z00_1365 =
											string_append_3(BgL_rz00_1360, BgL_arg1332z00_1366,
											BgL_arg1333z00_1367);
									}
									{
										obj_t BgL_rz00_3374;
										obj_t BgL_iz00_3373;

										BgL_iz00_3373 = BgL_arg1329z00_1364;
										BgL_rz00_3374 = BgL_arg1331z00_1365;
										BgL_rz00_1360 = BgL_rz00_3374;
										BgL_iz00_1359 = BgL_iz00_3373;
										goto BgL_zc3z04anonymousza31327ze3z87_1361;
									}
								}
							else
								{	/* Unsafe/pcre.scm 258 */
									if (((long) CINT(BgL_iz00_1359) == 0L))
										{	/* Unsafe/pcre.scm 259 */
											return BgL_strz00_52;
										}
									else
										{	/* Unsafe/pcre.scm 259 */
											return
												string_append(BgL_rz00_1360,
												c_substring(BgL_strz00_52,
													(long) CINT(BgL_iz00_1359), BgL_nz00_1356));
		}}}}}}

	}



/* &pregexp-replace* */
	obj_t BGl_z62pregexpzd2replaceza2z12zz__regexpz00(obj_t BgL_envz00_2263,
		obj_t BgL_patz00_2264, obj_t BgL_strz00_2265, obj_t BgL_insz00_2266)
	{
		{	/* Unsafe/pcre.scm 247 */
			{	/* Unsafe/pcre.scm 250 */
				obj_t BgL_auxz00_3389;
				obj_t BgL_auxz00_3382;

				if (STRINGP(BgL_insz00_2266))
					{	/* Unsafe/pcre.scm 250 */
						BgL_auxz00_3389 = BgL_insz00_2266;
					}
				else
					{
						obj_t BgL_auxz00_3392;

						BgL_auxz00_3392 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(10597L), BGl_string1838z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_insz00_2266);
						FAILURE(BgL_auxz00_3392, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_strz00_2265))
					{	/* Unsafe/pcre.scm 250 */
						BgL_auxz00_3382 = BgL_strz00_2265;
					}
				else
					{
						obj_t BgL_auxz00_3385;

						BgL_auxz00_3385 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(10597L), BGl_string1838z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_strz00_2265);
						FAILURE(BgL_auxz00_3385, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2replaceza2z70zz__regexpz00(BgL_patz00_2264,
					BgL_auxz00_3382, BgL_auxz00_3389);
			}
		}

	}



/* pregexp-quote */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2quotezd2zz__regexpz00(obj_t BgL_sz00_54)
	{
		{	/* Unsafe/pcre.scm 277 */
			{	/* Unsafe/pcre.scm 278 */
				long BgL_g1054z00_1379;

				BgL_g1054z00_1379 = (STRING_LENGTH(BgL_sz00_54) - 1L);
				{
					long BgL_iz00_1382;
					obj_t BgL_rz00_1383;

					BgL_iz00_1382 = BgL_g1054z00_1379;
					BgL_rz00_1383 = BNIL;
				BgL_zc3z04anonymousza31339ze3z87_1384:
					if ((BgL_iz00_1382 < 0L))
						{	/* Unsafe/pcre.scm 279 */
							return
								BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_rz00_1383);
						}
					else
						{	/* Unsafe/pcre.scm 281 */
							long BgL_arg1341z00_1386;
							obj_t BgL_arg1342z00_1387;

							BgL_arg1341z00_1386 = (BgL_iz00_1382 - 1L);
							{	/* Unsafe/pcre.scm 282 */
								unsigned char BgL_cz00_1388;

								BgL_cz00_1388 = STRING_REF(BgL_sz00_54, BgL_iz00_1382);
								if (CBOOL(BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BCHAR
											(BgL_cz00_1388), BGl_list1839z00zz__regexpz00)))
									{	/* Unsafe/pcre.scm 285 */
										obj_t BgL_arg1344z00_1390;

										BgL_arg1344z00_1390 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1388), BgL_rz00_1383);
										BgL_arg1342z00_1387 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')),
											BgL_arg1344z00_1390);
									}
								else
									{	/* Unsafe/pcre.scm 283 */
										BgL_arg1342z00_1387 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1388), BgL_rz00_1383);
									}
							}
							{
								obj_t BgL_rz00_3415;
								long BgL_iz00_3414;

								BgL_iz00_3414 = BgL_arg1341z00_1386;
								BgL_rz00_3415 = BgL_arg1342z00_1387;
								BgL_rz00_1383 = BgL_rz00_3415;
								BgL_iz00_1382 = BgL_iz00_3414;
								goto BgL_zc3z04anonymousza31339ze3z87_1384;
							}
						}
				}
			}
		}

	}



/* &pregexp-quote */
	obj_t BGl_z62pregexpzd2quotezb0zz__regexpz00(obj_t BgL_envz00_2267,
		obj_t BgL_sz00_2268)
	{
		{	/* Unsafe/pcre.scm 277 */
			{	/* Unsafe/pcre.scm 278 */
				obj_t BgL_auxz00_3416;

				if (STRINGP(BgL_sz00_2268))
					{	/* Unsafe/pcre.scm 278 */
						BgL_auxz00_3416 = BgL_sz00_2268;
					}
				else
					{
						obj_t BgL_auxz00_3419;

						BgL_auxz00_3419 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1824z00zz__regexpz00,
							BINT(11558L), BGl_string1840z00zz__regexpz00,
							BGl_string1829z00zz__regexpz00, BgL_sz00_2268);
						FAILURE(BgL_auxz00_3419, BFALSE, BFALSE);
					}
				return BGl_pregexpzd2quotezd2zz__regexpz00(BgL_auxz00_3416);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pcre.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pcre.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pcre.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__regexpz00(void)
	{
		{	/* Unsafe/pcre.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1841z00zz__regexpz00));
		}

	}

#ifdef __cplusplus
}
#endif
