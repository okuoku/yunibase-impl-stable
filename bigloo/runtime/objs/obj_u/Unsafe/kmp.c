/*===========================================================================*/
/*   (Unsafe/kmp.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/kmp.scm -indent -o objs/obj_u/Unsafe/kmp.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___KMP_TYPE_DEFINITIONS
#define BGL___KMP_TYPE_DEFINITIONS
#endif													// BGL___KMP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62kmpzd2mmapzb0zz__kmpz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__kmpz00 = BUNSPEC;
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__kmpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62kmpzd2tablezb0zz__kmpz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zz__kmpz00(void);
	static obj_t BGl_genericzd2initzd2zz__kmpz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__kmpz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__kmpz00(void);
	static obj_t BGl_objectzd2initzd2zz__kmpz00(void);
	BGL_EXPORTED_DECL obj_t BGl_kmpzd2tablezd2zz__kmpz00(obj_t);
	BGL_EXPORTED_DECL long BGl_kmpzd2stringzd2zz__kmpz00(obj_t, obj_t, long);
	extern obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL long BGl_kmpzd2mmapzd2zz__kmpz00(obj_t, obj_t, long);
	extern obj_t make_vector(long, obj_t);
	static obj_t BGl_z62kmpzd2stringzb0zz__kmpz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__kmpz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1720z00zz__kmpz00 = BUNSPEC;
	static obj_t BGl_symbol1722z00zz__kmpz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kmpzd2stringzd2envz00zz__kmpz00,
		BgL_bgl_za762kmpza7d2stringza71732za7, BGl_z62kmpzd2stringzb0zz__kmpz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1715z00zz__kmpz00,
		BgL_bgl_string1715za700za7za7_1733za7, "/tmp/bigloo/runtime/Unsafe/kmp.scm",
		34);
	      DEFINE_STRING(BGl_string1716z00zz__kmpz00,
		BgL_bgl_string1716za700za7za7_1734za7, "&kmp-table", 10);
	      DEFINE_STRING(BGl_string1717z00zz__kmpz00,
		BgL_bgl_string1717za700za7za7_1735za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1718z00zz__kmpz00,
		BgL_bgl_string1718za700za7za7_1736za7, "kmp-mmap", 8);
	      DEFINE_STRING(BGl_string1719z00zz__kmpz00,
		BgL_bgl_string1719za700za7za7_1737za7, "Illegal kmp-table", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_kmpzd2mmapzd2envz00zz__kmpz00,
		BgL_bgl_za762kmpza7d2mmapza7b01738za7, BGl_z62kmpzd2mmapzb0zz__kmpz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1721z00zz__kmpz00,
		BgL_bgl_string1721za700za7za7_1739za7, "string", 6);
	      DEFINE_STRING(BGl_string1723z00zz__kmpz00,
		BgL_bgl_string1723za700za7za7_1740za7, "vector", 6);
	      DEFINE_STRING(BGl_string1724z00zz__kmpz00,
		BgL_bgl_string1724za700za7za7_1741za7, "&kmp-mmap", 9);
	      DEFINE_STRING(BGl_string1725z00zz__kmpz00,
		BgL_bgl_string1725za700za7za7_1742za7, "pair", 4);
	      DEFINE_STRING(BGl_string1726z00zz__kmpz00,
		BgL_bgl_string1726za700za7za7_1743za7, "mmap", 4);
	      DEFINE_STRING(BGl_string1727z00zz__kmpz00,
		BgL_bgl_string1727za700za7za7_1744za7, "belong", 6);
	      DEFINE_STRING(BGl_string1728z00zz__kmpz00,
		BgL_bgl_string1728za700za7za7_1745za7, "kmp-string", 10);
	      DEFINE_STRING(BGl_string1729z00zz__kmpz00,
		BgL_bgl_string1729za700za7za7_1746za7, "&kmp-string", 11);
	      DEFINE_STRING(BGl_string1730z00zz__kmpz00,
		BgL_bgl_string1730za700za7za7_1747za7, "bint", 4);
	      DEFINE_STRING(BGl_string1731z00zz__kmpz00,
		BgL_bgl_string1731za700za7za7_1748za7, "__kmp", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_kmpzd2tablezd2envz00zz__kmpz00,
		BgL_bgl_za762kmpza7d2tableza7b1749za7, BGl_z62kmpzd2tablezb0zz__kmpz00, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__kmpz00));
		     ADD_ROOT((void *) (&BGl_symbol1720z00zz__kmpz00));
		     ADD_ROOT((void *) (&BGl_symbol1722z00zz__kmpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__kmpz00(long
		BgL_checksumz00_1988, char *BgL_fromz00_1989)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__kmpz00))
				{
					BGl_requirezd2initializa7ationz75zz__kmpz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__kmpz00();
					BGl_cnstzd2initzd2zz__kmpz00();
					BGl_importedzd2moduleszd2initz00zz__kmpz00();
					return BGl_methodzd2initzd2zz__kmpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__kmpz00(void)
	{
		{	/* Unsafe/kmp.scm 15 */
			BGl_symbol1720z00zz__kmpz00 =
				bstring_to_symbol(BGl_string1721z00zz__kmpz00);
			return (BGl_symbol1722z00zz__kmpz00 =
				bstring_to_symbol(BGl_string1723z00zz__kmpz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__kmpz00(void)
	{
		{	/* Unsafe/kmp.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* kmp-table */
	BGL_EXPORTED_DEF obj_t BGl_kmpzd2tablezd2zz__kmpz00(obj_t BgL_pz00_3)
	{
		{	/* Unsafe/kmp.scm 52 */
			{	/* Unsafe/kmp.scm 53 */
				long BgL_lpz00_1130;

				BgL_lpz00_1130 = STRING_LENGTH(BgL_pz00_3);
				{	/* Unsafe/kmp.scm 53 */
					obj_t BgL_tz00_1131;

					BgL_tz00_1131 = make_vector((BgL_lpz00_1130 + 2L), BINT(0L));
					{	/* Unsafe/kmp.scm 54 */
						long BgL_iz00_1132;

						BgL_iz00_1132 = 0L;
						{	/* Unsafe/kmp.scm 55 */
							obj_t BgL_jz00_1133;

							BgL_jz00_1133 = BINT(-1L);
							{	/* Unsafe/kmp.scm 56 */
								unsigned char BgL_cz00_1134;

								BgL_cz00_1134 = ((unsigned char) '\000');
								{	/* Unsafe/kmp.scm 57 */

									VECTOR_SET(BgL_tz00_1131, 0L, BgL_jz00_1133);
									{

									BgL_zc3z04anonymousza31167ze3z87_1136:
										if ((BgL_iz00_1132 < BgL_lpz00_1130))
											{	/* Unsafe/kmp.scm 60 */
												{	/* Unsafe/kmp.scm 62 */
													bool_t BgL_test1752z00_2008;

													{	/* Unsafe/kmp.scm 62 */
														unsigned char BgL_char2z00_1678;

														BgL_char2z00_1678 = BgL_cz00_1134;
														BgL_test1752z00_2008 =
															(STRING_REF(BgL_pz00_3,
																BgL_iz00_1132) == BgL_char2z00_1678);
													}
													if (BgL_test1752z00_2008)
														{	/* Unsafe/kmp.scm 62 */
															{	/* Unsafe/kmp.scm 63 */
																long BgL_arg1182z00_1140;
																long BgL_arg1183z00_1141;

																BgL_arg1182z00_1140 = (BgL_iz00_1132 + 1L);
																BgL_arg1183z00_1141 =
																	((long) CINT(BgL_jz00_1133) + 1L);
																VECTOR_SET(BgL_tz00_1131, BgL_arg1182z00_1140,
																	BINT(BgL_arg1183z00_1141));
															}
															BgL_jz00_1133 = ADDFX(BgL_jz00_1133, BINT(1L));
															BgL_iz00_1132 = (BgL_iz00_1132 + 1L);
														}
													else
														{	/* Unsafe/kmp.scm 62 */
															if (((long) CINT(BgL_jz00_1133) > 0L))
																{	/* Unsafe/kmp.scm 66 */
																	BgL_jz00_1133 =
																		VECTOR_REF(BgL_tz00_1131,
																		(long) CINT(BgL_jz00_1133));
																}
															else
																{	/* Unsafe/kmp.scm 66 */
																	VECTOR_SET(BgL_tz00_1131,
																		(BgL_iz00_1132 + 1L), BINT(0L));
																	BgL_iz00_1132 = (BgL_iz00_1132 + 1L);
																	BgL_jz00_1133 = BINT(0L);
																}
														}
												}
												BgL_cz00_1134 =
													STRING_REF(BgL_pz00_3, (long) CINT(BgL_jz00_1133));
												goto BgL_zc3z04anonymousza31167ze3z87_1136;
											}
										else
											{	/* Unsafe/kmp.scm 60 */
												((bool_t) 0);
											}
									}
									return MAKE_YOUNG_PAIR(BgL_tz00_1131, BgL_pz00_3);
								}
							}
						}
					}
				}
			}
		}

	}



/* &kmp-table */
	obj_t BGl_z62kmpzd2tablezb0zz__kmpz00(obj_t BgL_envz00_1963,
		obj_t BgL_pz00_1964)
	{
		{	/* Unsafe/kmp.scm 52 */
			{	/* Unsafe/kmp.scm 53 */
				obj_t BgL_auxz00_2032;

				if (STRINGP(BgL_pz00_1964))
					{	/* Unsafe/kmp.scm 53 */
						BgL_auxz00_2032 = BgL_pz00_1964;
					}
				else
					{
						obj_t BgL_auxz00_2035;

						BgL_auxz00_2035 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
							BINT(1837L), BGl_string1716z00zz__kmpz00,
							BGl_string1717z00zz__kmpz00, BgL_pz00_1964);
						FAILURE(BgL_auxz00_2035, BFALSE, BFALSE);
					}
				return BGl_kmpzd2tablezd2zz__kmpz00(BgL_auxz00_2032);
			}
		}

	}



/* kmp-mmap */
	BGL_EXPORTED_DEF long BGl_kmpzd2mmapzd2zz__kmpz00(obj_t BgL_tpz00_4,
		obj_t BgL_mmz00_5, long BgL_mz00_6)
	{
		{	/* Unsafe/kmp.scm 79 */
			{	/* Unsafe/kmp.scm 81 */
				bool_t BgL_test1755z00_2040;

				{	/* Unsafe/kmp.scm 81 */
					obj_t BgL_tmpz00_2041;

					BgL_tmpz00_2041 = CAR(BgL_tpz00_4);
					BgL_test1755z00_2040 = VECTORP(BgL_tmpz00_2041);
				}
				if (BgL_test1755z00_2040)
					{	/* Unsafe/kmp.scm 83 */
						bool_t BgL_test1756z00_2044;

						{	/* Unsafe/kmp.scm 83 */
							obj_t BgL_tmpz00_2045;

							BgL_tmpz00_2045 = CDR(BgL_tpz00_4);
							BgL_test1756z00_2044 = STRINGP(BgL_tmpz00_2045);
						}
						if (BgL_test1756z00_2044)
							{	/* Unsafe/kmp.scm 85 */
								bool_t BgL_test1757z00_2048;

								{	/* Unsafe/kmp.scm 85 */
									long BgL_arg1226z00_1183;
									long BgL_arg1227z00_1184;

									{	/* Unsafe/kmp.scm 85 */
										obj_t BgL_arg1228z00_1185;

										BgL_arg1228z00_1185 = CAR(BgL_tpz00_4);
										BgL_arg1226z00_1183 =
											VECTOR_LENGTH(((obj_t) BgL_arg1228z00_1185));
									}
									BgL_arg1227z00_1184 = (2L + STRING_LENGTH(CDR(BgL_tpz00_4)));
									BgL_test1757z00_2048 =
										(BgL_arg1226z00_1183 == BgL_arg1227z00_1184);
								}
								if (BgL_test1757z00_2048)
									{	/* Unsafe/kmp.scm 88 */
										obj_t BgL_tz00_1157;

										BgL_tz00_1157 = CAR(BgL_tpz00_4);
										{	/* Unsafe/kmp.scm 88 */
											obj_t BgL_pz00_1158;

											BgL_pz00_1158 = CDR(BgL_tpz00_4);
											{	/* Unsafe/kmp.scm 89 */
												long BgL_lsz00_1159;

												BgL_lsz00_1159 = BGL_MMAP_LENGTH(BgL_mmz00_5);
												{	/* Unsafe/kmp.scm 90 */
													long BgL_lpz00_1160;

													{	/* Unsafe/kmp.scm 91 */
														long BgL_arg1225z00_1182;

														BgL_arg1225z00_1182 =
															STRING_LENGTH(((obj_t) BgL_pz00_1158));
														BgL_lpz00_1160 = (long) (BgL_arg1225z00_1182);
													}
													{	/* Unsafe/kmp.scm 91 */

														{
															long BgL_iz00_1162;

															BgL_iz00_1162 = ((long) 0);
														BgL_zc3z04anonymousza31202ze3z87_1163:
															if ((BgL_iz00_1162 == BgL_lpz00_1160))
																{	/* Unsafe/kmp.scm 94 */
																	return BgL_mz00_6;
																}
															else
																{	/* Unsafe/kmp.scm 94 */
																	if (
																		((BgL_iz00_1162 + BgL_mz00_6) >=
																			BgL_lsz00_1159))
																		{	/* Unsafe/kmp.scm 96 */
																			return ((long) -1);
																		}
																	else
																		{	/* Unsafe/kmp.scm 99 */
																			long BgL_fiz00_1167;

																			BgL_fiz00_1167 = (long) (BgL_iz00_1162);
																			{	/* Unsafe/kmp.scm 100 */
																				bool_t BgL_test1760z00_2068;

																				{	/* Unsafe/kmp.scm 100 */
																					unsigned char BgL_arg1219z00_1177;
																					unsigned char BgL_arg1220z00_1178;

																					{	/* Unsafe/kmp.scm 100 */
																						long BgL_arg1221z00_1179;

																						BgL_arg1221z00_1179 =
																							(BgL_iz00_1162 + BgL_mz00_6);
																						{	/* Unsafe/kmp.scm 100 */
																							unsigned char BgL_res1700z00_1725;

																							{	/* Unsafe/kmp.scm 100 */
																								unsigned char BgL_cz00_1719;

																								BgL_cz00_1719 =
																									BGL_MMAP_REF(BgL_mmz00_5,
																									BgL_arg1221z00_1179);
																								{	/* Unsafe/kmp.scm 100 */
																									long BgL_arg1691z00_1720;

																									BgL_arg1691z00_1720 =
																										(BgL_arg1221z00_1179 +
																										((long) 1));
																									BGL_MMAP_RP_SET(BgL_mmz00_5,
																										BgL_arg1691z00_1720);
																									BUNSPEC;
																									BgL_arg1691z00_1720;
																								}
																								BgL_res1700z00_1725 =
																									BgL_cz00_1719;
																							}
																							BgL_arg1219z00_1177 =
																								BgL_res1700z00_1725;
																					}}
																					BgL_arg1220z00_1178 =
																						STRING_REF(
																						((obj_t) BgL_pz00_1158),
																						BgL_fiz00_1167);
																					BgL_test1760z00_2068 =
																						(BgL_arg1219z00_1177 ==
																						BgL_arg1220z00_1178);
																				}
																				if (BgL_test1760z00_2068)
																					{
																						long BgL_iz00_2076;

																						BgL_iz00_2076 =
																							(BgL_iz00_1162 + ((long) 1));
																						BgL_iz00_1162 = BgL_iz00_2076;
																						goto
																							BgL_zc3z04anonymousza31202ze3z87_1163;
																					}
																				else
																					{	/* Unsafe/kmp.scm 102 */
																						long BgL_tiz00_1173;

																						{	/* Unsafe/kmp.scm 102 */
																							obj_t BgL_arg1218z00_1176;

																							BgL_arg1218z00_1176 =
																								VECTOR_REF(
																								((obj_t) BgL_tz00_1157),
																								BgL_fiz00_1167);
																							{	/* Unsafe/kmp.scm 102 */
																								long BgL_tmpz00_2080;

																								BgL_tmpz00_2080 =
																									(long)
																									CINT(BgL_arg1218z00_1176);
																								BgL_tiz00_1173 =
																									(long) (BgL_tmpz00_2080);
																						}}
																						{	/* Unsafe/kmp.scm 103 */
																							long BgL_za71za7_1738;

																							BgL_za71za7_1738 = BgL_mz00_6;
																							{	/* Unsafe/kmp.scm 103 */
																								long BgL_tmpz00_2083;

																								{	/* Unsafe/kmp.scm 103 */
																									long BgL_res1701z00_1737;

																									{	/* Unsafe/kmp.scm 103 */
																										long BgL_tmpz00_2084;

																										BgL_tmpz00_2084 =
																											(BgL_iz00_1162 -
																											BgL_tiz00_1173);
																										BgL_res1701z00_1737 =
																											(long) (BgL_tmpz00_2084);
																									}
																									BgL_tmpz00_2083 =
																										BgL_res1701z00_1737;
																								}
																								BgL_mz00_6 =
																									(BgL_za71za7_1738 +
																									BgL_tmpz00_2083);
																						}}
																						if ((BgL_iz00_1162 > ((long) 0)))
																							{
																								long BgL_iz00_2090;

																								BgL_iz00_2090 = BgL_tiz00_1173;
																								BgL_iz00_1162 = BgL_iz00_2090;
																								goto
																									BgL_zc3z04anonymousza31202ze3z87_1163;
																							}
																						else
																							{

																								goto
																									BgL_zc3z04anonymousza31202ze3z87_1163;
																							}
																					}
																			}
																		}
																}
														}
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/kmp.scm 86 */
										obj_t BgL_tmpz00_2091;

										BgL_tmpz00_2091 =
											BGl_errorz00zz__errorz00(BGl_string1718z00zz__kmpz00,
											BGl_string1719z00zz__kmpz00, BgL_tpz00_4);
										return BELONG_TO_LONG(BgL_tmpz00_2091);
									}
							}
						else
							{	/* Unsafe/kmp.scm 84 */
								obj_t BgL_arg1231z00_1188;

								BgL_arg1231z00_1188 = CDR(BgL_tpz00_4);
								{	/* Unsafe/kmp.scm 84 */
									obj_t BgL_tmpz00_2095;

									BgL_tmpz00_2095 =
										BGl_bigloozd2typezd2errorz00zz__errorz00
										(BGl_string1718z00zz__kmpz00, BGl_symbol1720z00zz__kmpz00,
										BgL_arg1231z00_1188);
									return BELONG_TO_LONG(BgL_tmpz00_2095);
								}
							}
					}
				else
					{	/* Unsafe/kmp.scm 82 */
						obj_t BgL_arg1233z00_1190;

						BgL_arg1233z00_1190 = CAR(BgL_tpz00_4);
						{	/* Unsafe/kmp.scm 82 */
							obj_t BgL_tmpz00_2099;

							BgL_tmpz00_2099 =
								BGl_bigloozd2typezd2errorz00zz__errorz00
								(BGl_string1718z00zz__kmpz00, BGl_symbol1722z00zz__kmpz00,
								BgL_arg1233z00_1190);
							return BELONG_TO_LONG(BgL_tmpz00_2099);
						}
					}
			}
		}

	}



/* &kmp-mmap */
	obj_t BGl_z62kmpzd2mmapzb0zz__kmpz00(obj_t BgL_envz00_1965,
		obj_t BgL_tpz00_1966, obj_t BgL_mmz00_1967, obj_t BgL_mz00_1968)
	{
		{	/* Unsafe/kmp.scm 79 */
			{	/* Unsafe/kmp.scm 81 */
				long BgL_tmpz00_2102;

				{	/* Unsafe/kmp.scm 81 */
					long BgL_auxz00_2117;
					obj_t BgL_auxz00_2110;
					obj_t BgL_auxz00_2103;

					{	/* Unsafe/kmp.scm 81 */
						obj_t BgL_tmpz00_2118;

						if (ELONGP(BgL_mz00_1968))
							{	/* Unsafe/kmp.scm 81 */
								BgL_tmpz00_2118 = BgL_mz00_1968;
							}
						else
							{
								obj_t BgL_auxz00_2121;

								BgL_auxz00_2121 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
									BINT(2586L), BGl_string1724z00zz__kmpz00,
									BGl_string1727z00zz__kmpz00, BgL_mz00_1968);
								FAILURE(BgL_auxz00_2121, BFALSE, BFALSE);
							}
						BgL_auxz00_2117 = BELONG_TO_LONG(BgL_tmpz00_2118);
					}
					if (BGL_MMAPP(BgL_mmz00_1967))
						{	/* Unsafe/kmp.scm 81 */
							BgL_auxz00_2110 = BgL_mmz00_1967;
						}
					else
						{
							obj_t BgL_auxz00_2113;

							BgL_auxz00_2113 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
								BINT(2586L), BGl_string1724z00zz__kmpz00,
								BGl_string1726z00zz__kmpz00, BgL_mmz00_1967);
							FAILURE(BgL_auxz00_2113, BFALSE, BFALSE);
						}
					if (PAIRP(BgL_tpz00_1966))
						{	/* Unsafe/kmp.scm 81 */
							BgL_auxz00_2103 = BgL_tpz00_1966;
						}
					else
						{
							obj_t BgL_auxz00_2106;

							BgL_auxz00_2106 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
								BINT(2586L), BGl_string1724z00zz__kmpz00,
								BGl_string1725z00zz__kmpz00, BgL_tpz00_1966);
							FAILURE(BgL_auxz00_2106, BFALSE, BFALSE);
						}
					BgL_tmpz00_2102 =
						BGl_kmpzd2mmapzd2zz__kmpz00(BgL_auxz00_2103, BgL_auxz00_2110,
						BgL_auxz00_2117);
				}
				return make_belong(BgL_tmpz00_2102);
			}
		}

	}



/* kmp-string */
	BGL_EXPORTED_DEF long BGl_kmpzd2stringzd2zz__kmpz00(obj_t BgL_tpz00_7,
		obj_t BgL_strz00_8, long BgL_mz00_9)
	{
		{	/* Unsafe/kmp.scm 111 */
			{	/* Unsafe/kmp.scm 113 */
				bool_t BgL_test1765z00_2128;

				{	/* Unsafe/kmp.scm 113 */
					obj_t BgL_tmpz00_2129;

					BgL_tmpz00_2129 = CAR(BgL_tpz00_7);
					BgL_test1765z00_2128 = VECTORP(BgL_tmpz00_2129);
				}
				if (BgL_test1765z00_2128)
					{	/* Unsafe/kmp.scm 115 */
						bool_t BgL_test1766z00_2132;

						{	/* Unsafe/kmp.scm 115 */
							obj_t BgL_tmpz00_2133;

							BgL_tmpz00_2133 = CDR(BgL_tpz00_7);
							BgL_test1766z00_2132 = STRINGP(BgL_tmpz00_2133);
						}
						if (BgL_test1766z00_2132)
							{	/* Unsafe/kmp.scm 117 */
								bool_t BgL_test1767z00_2136;

								{	/* Unsafe/kmp.scm 117 */
									long BgL_arg1314z00_1225;
									long BgL_arg1315z00_1226;

									{	/* Unsafe/kmp.scm 117 */
										obj_t BgL_arg1316z00_1227;

										BgL_arg1316z00_1227 = CAR(BgL_tpz00_7);
										BgL_arg1314z00_1225 =
											VECTOR_LENGTH(((obj_t) BgL_arg1316z00_1227));
									}
									BgL_arg1315z00_1226 = (2L + STRING_LENGTH(CDR(BgL_tpz00_7)));
									BgL_test1767z00_2136 =
										(BgL_arg1314z00_1225 == BgL_arg1315z00_1226);
								}
								if (BgL_test1767z00_2136)
									{	/* Unsafe/kmp.scm 120 */
										obj_t BgL_tz00_1202;

										BgL_tz00_1202 = CAR(BgL_tpz00_7);
										{	/* Unsafe/kmp.scm 120 */
											obj_t BgL_pz00_1203;

											BgL_pz00_1203 = CDR(BgL_tpz00_7);
											{	/* Unsafe/kmp.scm 121 */
												long BgL_lsz00_1204;

												BgL_lsz00_1204 = STRING_LENGTH(BgL_strz00_8);
												{	/* Unsafe/kmp.scm 122 */
													long BgL_lpz00_1205;

													BgL_lpz00_1205 =
														STRING_LENGTH(((obj_t) BgL_pz00_1203));
													{	/* Unsafe/kmp.scm 123 */

														{
															obj_t BgL_iz00_1207;

															BgL_iz00_1207 = BINT(0L);
														BgL_zc3z04anonymousza31254ze3z87_1208:
															if (
																((long) CINT(BgL_iz00_1207) == BgL_lpz00_1205))
																{	/* Unsafe/kmp.scm 126 */
																	return BgL_mz00_9;
																}
															else
																{	/* Unsafe/kmp.scm 126 */
																	if (
																		(((long) CINT(BgL_iz00_1207) +
																				BgL_mz00_9) >= BgL_lsz00_1204))
																		{	/* Unsafe/kmp.scm 128 */
																			return -1L;
																		}
																	else
																		{	/* Unsafe/kmp.scm 131 */
																			bool_t BgL_test1770z00_2156;

																			{	/* Unsafe/kmp.scm 131 */
																				unsigned char BgL_arg1309z00_1220;

																				BgL_arg1309z00_1220 =
																					STRING_REF(BgL_strz00_8,
																					((long) CINT(BgL_iz00_1207) +
																						BgL_mz00_9));
																				BgL_test1770z00_2156 =
																					(BgL_arg1309z00_1220 ==
																					STRING_REF(((obj_t) BgL_pz00_1203),
																						(long) CINT(BgL_iz00_1207)));
																			}
																			if (BgL_test1770z00_2156)
																				{	/* Unsafe/kmp.scm 132 */
																					long BgL_arg1306z00_1216;

																					BgL_arg1306z00_1216 =
																						((long) CINT(BgL_iz00_1207) + 1L);
																					{
																						obj_t BgL_iz00_2166;

																						BgL_iz00_2166 =
																							BINT(BgL_arg1306z00_1216);
																						BgL_iz00_1207 = BgL_iz00_2166;
																						goto
																							BgL_zc3z04anonymousza31254ze3z87_1208;
																					}
																				}
																			else
																				{	/* Unsafe/kmp.scm 133 */
																					obj_t BgL_tiz00_1217;

																					{	/* Unsafe/kmp.scm 133 */
																						long BgL_kz00_1773;

																						BgL_kz00_1773 =
																							(long) CINT(BgL_iz00_1207);
																						BgL_tiz00_1217 =
																							VECTOR_REF(
																							((obj_t) BgL_tz00_1202),
																							BgL_kz00_1773);
																					}
																					BgL_mz00_9 =
																						(BgL_mz00_9 +
																						((long) CINT(BgL_iz00_1207) -
																							(long) CINT(BgL_tiz00_1217)));
																					if (((long) CINT(BgL_iz00_1207) > 0L))
																						{
																							obj_t BgL_iz00_2178;

																							BgL_iz00_2178 = BgL_tiz00_1217;
																							BgL_iz00_1207 = BgL_iz00_2178;
																							goto
																								BgL_zc3z04anonymousza31254ze3z87_1208;
																						}
																					else
																						{

																							goto
																								BgL_zc3z04anonymousza31254ze3z87_1208;
																						}
																				}
																		}
																}
														}
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/kmp.scm 117 */
										return
											(long)
											CINT(BGl_errorz00zz__errorz00(BGl_string1728z00zz__kmpz00,
												BGl_string1719z00zz__kmpz00, BgL_tpz00_7));
							}}
						else
							{	/* Unsafe/kmp.scm 116 */
								obj_t BgL_arg1319z00_1230;

								BgL_arg1319z00_1230 = CDR(BgL_tpz00_7);
								return
									(long)
									CINT(BGl_bigloozd2typezd2errorz00zz__errorz00
									(BGl_string1728z00zz__kmpz00, BGl_symbol1720z00zz__kmpz00,
										BgL_arg1319z00_1230));
					}}
				else
					{	/* Unsafe/kmp.scm 114 */
						obj_t BgL_arg1321z00_1232;

						BgL_arg1321z00_1232 = CAR(BgL_tpz00_7);
						return
							(long)
							CINT(BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string1728z00zz__kmpz00, BGl_symbol1722z00zz__kmpz00,
								BgL_arg1321z00_1232));
		}}}

	}



/* &kmp-string */
	obj_t BGl_z62kmpzd2stringzb0zz__kmpz00(obj_t BgL_envz00_1969,
		obj_t BgL_tpz00_1970, obj_t BgL_strz00_1971, obj_t BgL_mz00_1972)
	{
		{	/* Unsafe/kmp.scm 111 */
			{	/* Unsafe/kmp.scm 113 */
				long BgL_tmpz00_2188;

				{	/* Unsafe/kmp.scm 113 */
					long BgL_auxz00_2203;
					obj_t BgL_auxz00_2196;
					obj_t BgL_auxz00_2189;

					{	/* Unsafe/kmp.scm 113 */
						obj_t BgL_tmpz00_2204;

						if (INTEGERP(BgL_mz00_1972))
							{	/* Unsafe/kmp.scm 113 */
								BgL_tmpz00_2204 = BgL_mz00_1972;
							}
						else
							{
								obj_t BgL_auxz00_2207;

								BgL_auxz00_2207 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
									BINT(3680L), BGl_string1729z00zz__kmpz00,
									BGl_string1730z00zz__kmpz00, BgL_mz00_1972);
								FAILURE(BgL_auxz00_2207, BFALSE, BFALSE);
							}
						BgL_auxz00_2203 = (long) CINT(BgL_tmpz00_2204);
					}
					if (STRINGP(BgL_strz00_1971))
						{	/* Unsafe/kmp.scm 113 */
							BgL_auxz00_2196 = BgL_strz00_1971;
						}
					else
						{
							obj_t BgL_auxz00_2199;

							BgL_auxz00_2199 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
								BINT(3680L), BGl_string1729z00zz__kmpz00,
								BGl_string1717z00zz__kmpz00, BgL_strz00_1971);
							FAILURE(BgL_auxz00_2199, BFALSE, BFALSE);
						}
					if (PAIRP(BgL_tpz00_1970))
						{	/* Unsafe/kmp.scm 113 */
							BgL_auxz00_2189 = BgL_tpz00_1970;
						}
					else
						{
							obj_t BgL_auxz00_2192;

							BgL_auxz00_2192 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1715z00zz__kmpz00,
								BINT(3680L), BGl_string1729z00zz__kmpz00,
								BGl_string1725z00zz__kmpz00, BgL_tpz00_1970);
							FAILURE(BgL_auxz00_2192, BFALSE, BFALSE);
						}
					BgL_tmpz00_2188 =
						BGl_kmpzd2stringzd2zz__kmpz00(BgL_auxz00_2189, BgL_auxz00_2196,
						BgL_auxz00_2203);
				}
				return BINT(BgL_tmpz00_2188);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__kmpz00(void)
	{
		{	/* Unsafe/kmp.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__kmpz00(void)
	{
		{	/* Unsafe/kmp.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__kmpz00(void)
	{
		{	/* Unsafe/kmp.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__kmpz00(void)
	{
		{	/* Unsafe/kmp.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1731z00zz__kmpz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1731z00zz__kmpz00));
		}

	}

#ifdef __cplusplus
}
#endif
