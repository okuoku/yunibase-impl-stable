/*===========================================================================*/
/*   (Unsafe/bm.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/bm.scm -indent -o objs/obj_u/Unsafe/bm.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BM_TYPE_DEFINITIONS
#define BGL___BM_TYPE_DEFINITIONS
#endif													// BGL___BM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62bmhzd2stringzb0zz__bmz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__bmz00 = BUNSPEC;
	BGL_EXPORTED_DECL long BGl_bmhzd2mmapzd2zz__bmz00(obj_t, obj_t, long);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__bmz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL long BGl_bmzd2mmapzd2zz__bmz00(obj_t, obj_t, long);
	static obj_t BGl_z62bmzd2mmapzb0zz__bmz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_symbol1872z00zz__bmz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__bmz00(void);
	static obj_t BGl_genericzd2initzd2zz__bmz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__bmz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__bmz00(void);
	static bool_t BGl_iszd2prefixzf3z21zz__bmz00(obj_t, long);
	static obj_t BGl_objectzd2initzd2zz__bmz00(void);
	extern obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long, uint32_t);
	BGL_EXPORTED_DECL obj_t BGl_bmzd2tablezd2zz__bmz00(obj_t);
	static bool_t BGl_makezd2delta1zd2zz__bmz00(obj_t, obj_t);
	static bool_t BGl_makezd2delta2zd2zz__bmz00(obj_t, obj_t);
	static obj_t BGl_z62bmhzd2mmapzb0zz__bmz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static long BGl_suffixzd2lengthzd2zz__bmz00(obj_t, long);
	static obj_t BGl_methodzd2initzd2zz__bmz00(void);
	extern obj_t BGl_bigloozd2typezd2errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bmhzd2tablezb0zz__bmz00(obj_t, obj_t);
	static obj_t BGl_z62bmzd2stringzb0zz__bmz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62bmzd2tablezb0zz__bmz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_bmhzd2stringzd2zz__bmz00(obj_t, obj_t, long);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bmhzd2tablezd2zz__bmz00(obj_t);
	BGL_EXPORTED_DECL long BGl_bmzd2stringzd2zz__bmz00(obj_t, obj_t, long);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1870z00zz__bmz00,
		BgL_bgl_string1870za700za7za7_1886za7, "bm", 2);
	      DEFINE_STRING(BGl_string1871z00zz__bmz00,
		BgL_bgl_string1871za700za7za7_1887za7, "Illegal bm-table", 16);
	      DEFINE_STRING(BGl_string1873z00zz__bmz00,
		BgL_bgl_string1873za700za7za7_1888za7, "u32vector", 9);
	      DEFINE_STRING(BGl_string1874z00zz__bmz00,
		BgL_bgl_string1874za700za7za7_1889za7, "&bm-mmap", 8);
	      DEFINE_STRING(BGl_string1875z00zz__bmz00,
		BgL_bgl_string1875za700za7za7_1890za7, "epair", 5);
	      DEFINE_STRING(BGl_string1876z00zz__bmz00,
		BgL_bgl_string1876za700za7za7_1891za7, "mmap", 4);
	      DEFINE_STRING(BGl_string1877z00zz__bmz00,
		BgL_bgl_string1877za700za7za7_1892za7, "belong", 6);
	      DEFINE_STRING(BGl_string1878z00zz__bmz00,
		BgL_bgl_string1878za700za7za7_1893za7, "&bm-string", 10);
	      DEFINE_STRING(BGl_string1879z00zz__bmz00,
		BgL_bgl_string1879za700za7za7_1894za7, "bint", 4);
	      DEFINE_STRING(BGl_string1880z00zz__bmz00,
		BgL_bgl_string1880za700za7za7_1895za7, "&bmh-table", 10);
	      DEFINE_STRING(BGl_string1881z00zz__bmz00,
		BgL_bgl_string1881za700za7za7_1896za7, "bmh", 3);
	      DEFINE_STRING(BGl_string1882z00zz__bmz00,
		BgL_bgl_string1882za700za7za7_1897za7, "Illegal bmh-table", 17);
	      DEFINE_STRING(BGl_string1883z00zz__bmz00,
		BgL_bgl_string1883za700za7za7_1898za7, "&bmh-mmap", 9);
	      DEFINE_STRING(BGl_string1884z00zz__bmz00,
		BgL_bgl_string1884za700za7za7_1899za7, "&bmh-string", 11);
	      DEFINE_STRING(BGl_string1885z00zz__bmz00,
		BgL_bgl_string1885za700za7za7_1900za7, "__bm", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmzd2tablezd2envz00zz__bmz00,
		BgL_bgl_za762bmza7d2tableza7b01901za7, BGl_z62bmzd2tablezb0zz__bmz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmhzd2stringzd2envz00zz__bmz00,
		BgL_bgl_za762bmhza7d2stringza71902za7, BGl_z62bmhzd2stringzb0zz__bmz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmzd2stringzd2envz00zz__bmz00,
		BgL_bgl_za762bmza7d2stringza7b1903za7, BGl_z62bmzd2stringzb0zz__bmz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmhzd2mmapzd2envz00zz__bmz00,
		BgL_bgl_za762bmhza7d2mmapza7b01904za7, BGl_z62bmhzd2mmapzb0zz__bmz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmzd2mmapzd2envz00zz__bmz00,
		BgL_bgl_za762bmza7d2mmapza7b0za71905z00, BGl_z62bmzd2mmapzb0zz__bmz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bmhzd2tablezd2envz00zz__bmz00,
		BgL_bgl_za762bmhza7d2tableza7b1906za7, BGl_z62bmhzd2tablezb0zz__bmz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1867z00zz__bmz00,
		BgL_bgl_string1867za700za7za7_1907za7, "/tmp/bigloo/runtime/Unsafe/bm.scm",
		33);
	      DEFINE_STRING(BGl_string1868z00zz__bmz00,
		BgL_bgl_string1868za700za7za7_1908za7, "&bm-table", 9);
	      DEFINE_STRING(BGl_string1869z00zz__bmz00,
		BgL_bgl_string1869za700za7za7_1909za7, "bstring", 7);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__bmz00));
		     ADD_ROOT((void *) (&BGl_symbol1872z00zz__bmz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__bmz00(long
		BgL_checksumz00_2419, char *BgL_fromz00_2420)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__bmz00))
				{
					BGl_requirezd2initializa7ationz75zz__bmz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__bmz00();
					BGl_cnstzd2initzd2zz__bmz00();
					BGl_importedzd2moduleszd2initz00zz__bmz00();
					return BGl_methodzd2initzd2zz__bmz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__bmz00(void)
	{
		{	/* Unsafe/bm.scm 15 */
			return (BGl_symbol1872z00zz__bmz00 =
				bstring_to_symbol(BGl_string1873z00zz__bmz00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__bmz00(void)
	{
		{	/* Unsafe/bm.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-delta1 */
	bool_t BGl_makezd2delta1zd2zz__bmz00(obj_t BgL_delta1z00_3,
		obj_t BgL_patz00_4)
	{
		{	/* Unsafe/bm.scm 101 */
			{	/* Unsafe/bm.scm 102 */
				uint32_t BgL_notzd2foundzd2_1216;
				long BgL_patlenzd21zd2_1217;

				{	/* Unsafe/bm.scm 102 */
					long BgL_tmpz00_2430;

					BgL_tmpz00_2430 = STRING_LENGTH(BgL_patz00_4);
					BgL_notzd2foundzd2_1216 = (uint32_t) (BgL_tmpz00_2430);
				}
				BgL_patlenzd21zd2_1217 = (STRING_LENGTH(BgL_patz00_4) - 1L);
				{
					long BgL_iz00_1942;

					BgL_iz00_1942 = 0L;
				BgL_for1046z00_1941:
					if ((BgL_iz00_1942 < 256L))
						{	/* Unsafe/bm.scm 104 */
							BGL_U32VSET(BgL_delta1z00_3, BgL_iz00_1942,
								BgL_notzd2foundzd2_1216);
							BUNSPEC;
							{
								long BgL_iz00_2438;

								BgL_iz00_2438 = (BgL_iz00_1942 + 1L);
								BgL_iz00_1942 = BgL_iz00_2438;
								goto BgL_for1046z00_1941;
							}
						}
					else
						{	/* Unsafe/bm.scm 104 */
							((bool_t) 0);
						}
				}
				{
					long BgL_iz00_1227;

					BgL_iz00_1227 = 0L;
				BgL_zc3z04anonymousza31197ze3z87_1228:
					if ((BgL_iz00_1227 < BgL_patlenzd21zd2_1217))
						{	/* Unsafe/bm.scm 106 */
							{	/* Unsafe/bm.scm 107 */
								long BgL_arg1199z00_1230;
								uint32_t BgL_arg1200z00_1231;

								BgL_arg1199z00_1230 = (STRING_REF(BgL_patz00_4, BgL_iz00_1227));
								{	/* Unsafe/bm.scm 108 */
									long BgL_tmpz00_2444;

									BgL_tmpz00_2444 = (BgL_patlenzd21zd2_1217 - BgL_iz00_1227);
									BgL_arg1200z00_1231 = (uint32_t) (BgL_tmpz00_2444);
								}
								BGL_U32VSET(BgL_delta1z00_3, BgL_arg1199z00_1230,
									BgL_arg1200z00_1231);
								BUNSPEC;
							}
							{
								long BgL_iz00_2448;

								BgL_iz00_2448 = (BgL_iz00_1227 + 1L);
								BgL_iz00_1227 = BgL_iz00_2448;
								goto BgL_zc3z04anonymousza31197ze3z87_1228;
							}
						}
					else
						{	/* Unsafe/bm.scm 106 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* is-prefix? */
	bool_t BGl_iszd2prefixzf3z21zz__bmz00(obj_t BgL_wordz00_5, long BgL_posz00_6)
	{
		{	/* Unsafe/bm.scm 116 */
			{	/* Unsafe/bm.scm 117 */
				long BgL_suffixlenz00_1238;

				BgL_suffixlenz00_1238 = (STRING_LENGTH(BgL_wordz00_5) - BgL_posz00_6);
				{
					long BgL_iz00_1240;

					BgL_iz00_1240 = 0L;
				BgL_zc3z04anonymousza31209ze3z87_1241:
					if ((BgL_iz00_1240 < BgL_suffixlenz00_1238))
						{	/* Unsafe/bm.scm 119 */
							if (
								(STRING_REF(BgL_wordz00_5, BgL_iz00_1240) ==
									STRING_REF(BgL_wordz00_5, (BgL_posz00_6 + BgL_iz00_1240))))
								{
									long BgL_iz00_2459;

									BgL_iz00_2459 = (BgL_iz00_1240 + 1L);
									BgL_iz00_1240 = BgL_iz00_2459;
									goto BgL_zc3z04anonymousza31209ze3z87_1241;
								}
							else
								{	/* Unsafe/bm.scm 120 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Unsafe/bm.scm 119 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* suffix-length */
	long BGl_suffixzd2lengthzd2zz__bmz00(obj_t BgL_wordz00_7, long BgL_posz00_8)
	{
		{	/* Unsafe/bm.scm 130 */
			{	/* Unsafe/bm.scm 131 */
				long BgL_wordlenzd21zd2_1253;

				BgL_wordlenzd21zd2_1253 = (STRING_LENGTH(BgL_wordz00_7) - 1L);
				{
					long BgL_iz00_1255;

					BgL_iz00_1255 = 0L;
				BgL_zc3z04anonymousza31224ze3z87_1256:
					if (
						(STRING_REF(BgL_wordz00_7,
								(BgL_posz00_8 - BgL_iz00_1255)) ==
							STRING_REF(BgL_wordz00_7,
								(BgL_wordlenzd21zd2_1253 - BgL_iz00_1255))))
						{	/* Unsafe/bm.scm 133 */
							if ((BgL_iz00_1255 < BgL_posz00_8))
								{
									long BgL_iz00_2471;

									BgL_iz00_2471 = (BgL_iz00_1255 + 1L);
									BgL_iz00_1255 = BgL_iz00_2471;
									goto BgL_zc3z04anonymousza31224ze3z87_1256;
								}
							else
								{	/* Unsafe/bm.scm 135 */
									return BgL_iz00_1255;
								}
						}
					else
						{	/* Unsafe/bm.scm 133 */
							return BgL_iz00_1255;
						}
				}
			}
		}

	}



/* make-delta2 */
	bool_t BGl_makezd2delta2zd2zz__bmz00(obj_t BgL_delta2z00_9,
		obj_t BgL_patz00_10)
	{
		{	/* Unsafe/bm.scm 179 */
			{	/* Unsafe/bm.scm 180 */
				long BgL_patlenzd21zd2_1270;

				BgL_patlenzd21zd2_1270 = (STRING_LENGTH(BgL_patz00_10) - 1L);
				{	/* Unsafe/bm.scm 180 */
					long BgL_lastzd2prefixzd2indexz00_1271;

					BgL_lastzd2prefixzd2indexz00_1271 = BgL_patlenzd21zd2_1270;
					{	/* Unsafe/bm.scm 181 */

						{
							long BgL_pz00_1274;

							BgL_pz00_1274 = BgL_patlenzd21zd2_1270;
						BgL_zc3z04anonymousza31239ze3z87_1275:
							if ((BgL_pz00_1274 >= 0L))
								{	/* Unsafe/bm.scm 183 */
									if (BGl_iszd2prefixzf3z21zz__bmz00(BgL_patz00_10,
											(BgL_pz00_1274 + 1L)))
										{	/* Unsafe/bm.scm 184 */
											BgL_lastzd2prefixzd2indexz00_1271 = (BgL_pz00_1274 + 1L);
										}
									else
										{	/* Unsafe/bm.scm 184 */
											BFALSE;
										}
									{	/* Unsafe/bm.scm 187 */
										long BgL_arg1248z00_1280;

										BgL_arg1248z00_1280 =
											(BgL_lastzd2prefixzd2indexz00_1271 +
											(BgL_patlenzd21zd2_1270 - BgL_pz00_1274));
										{	/* Unsafe/bm.scm 186 */
											uint32_t BgL_tmpz00_2483;

											BgL_tmpz00_2483 = (uint32_t) (BgL_arg1248z00_1280);
											BGL_U32VSET(BgL_delta2z00_9, BgL_pz00_1274,
												BgL_tmpz00_2483);
										} BUNSPEC;
									}
									{
										long BgL_pz00_2486;

										BgL_pz00_2486 = (BgL_pz00_1274 - 1L);
										BgL_pz00_1274 = BgL_pz00_2486;
										goto BgL_zc3z04anonymousza31239ze3z87_1275;
									}
								}
							else
								{	/* Unsafe/bm.scm 183 */
									((bool_t) 0);
								}
						}
						{
							long BgL_pz00_1286;

							BgL_pz00_1286 = 0L;
						BgL_zc3z04anonymousza31253ze3z87_1287:
							if ((BgL_pz00_1286 < BgL_patlenzd21zd2_1270))
								{	/* Unsafe/bm.scm 189 */
									{	/* Unsafe/bm.scm 190 */
										long BgL_slenz00_1289;

										BgL_slenz00_1289 =
											BGl_suffixzd2lengthzd2zz__bmz00(BgL_patz00_10,
											BgL_pz00_1286);
										if ((STRING_REF(BgL_patz00_10,
													(BgL_pz00_1286 - BgL_slenz00_1289)) ==
												STRING_REF(BgL_patz00_10,
													(BgL_patlenzd21zd2_1270 - BgL_slenz00_1289))))
											{	/* Unsafe/bm.scm 191 */
												BFALSE;
											}
										else
											{	/* Unsafe/bm.scm 193 */
												long BgL_arg1306z00_1295;
												uint32_t BgL_arg1307z00_1296;

												BgL_arg1306z00_1295 =
													(BgL_patlenzd21zd2_1270 - BgL_slenz00_1289);
												{	/* Unsafe/bm.scm 194 */
													long BgL_tmpz00_2498;

													BgL_tmpz00_2498 =
														(
														(BgL_patlenzd21zd2_1270 - BgL_pz00_1286) +
														BgL_slenz00_1289);
													BgL_arg1307z00_1296 = (uint32_t) (BgL_tmpz00_2498);
												}
												BGL_U32VSET(BgL_delta2z00_9, BgL_arg1306z00_1295,
													BgL_arg1307z00_1296);
												BUNSPEC;
									}}
									{
										long BgL_pz00_2503;

										BgL_pz00_2503 = (BgL_pz00_1286 + 1L);
										BgL_pz00_1286 = BgL_pz00_2503;
										goto BgL_zc3z04anonymousza31253ze3z87_1287;
									}
								}
							else
								{	/* Unsafe/bm.scm 189 */
									return ((bool_t) 0);
								}
						}
					}
				}
			}
		}

	}



/* bm-table */
	BGL_EXPORTED_DEF obj_t BGl_bmzd2tablezd2zz__bmz00(obj_t BgL_patz00_11)
	{
		{	/* Unsafe/bm.scm 200 */
			{	/* Unsafe/bm.scm 201 */
				obj_t BgL_delta1z00_1306;
				obj_t BgL_delta2z00_1307;

				{	/* Llib/srfi4.scm 451 */

					BgL_delta1z00_1306 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(256L, (uint32_t) (0));
				}
				{	/* Unsafe/bm.scm 202 */
					long BgL_arg1319z00_1311;

					BgL_arg1319z00_1311 = STRING_LENGTH(BgL_patz00_11);
					{	/* Llib/srfi4.scm 451 */

						BgL_delta2z00_1307 =
							BGl_makezd2u32vectorzd2zz__srfi4z00(BgL_arg1319z00_1311,
							(uint32_t) (0));
				}}
				BGl_makezd2delta1zd2zz__bmz00(BgL_delta1z00_1306, BgL_patz00_11);
				BGl_makezd2delta2zd2zz__bmz00(BgL_delta2z00_1307, BgL_patz00_11);
				{	/* Unsafe/bm.scm 205 */
					obj_t BgL_res1836z00_2018;

					BgL_res1836z00_2018 =
						MAKE_YOUNG_EPAIR(BgL_delta1z00_1306, BgL_delta2z00_1307,
						BgL_patz00_11);
					return BgL_res1836z00_2018;
				}
			}
		}

	}



/* &bm-table */
	obj_t BGl_z62bmzd2tablezb0zz__bmz00(obj_t BgL_envz00_2372,
		obj_t BgL_patz00_2373)
	{
		{	/* Unsafe/bm.scm 200 */
			{	/* Unsafe/bm.scm 201 */
				obj_t BgL_auxz00_2511;

				if (STRINGP(BgL_patz00_2373))
					{	/* Unsafe/bm.scm 201 */
						BgL_auxz00_2511 = BgL_patz00_2373;
					}
				else
					{
						obj_t BgL_auxz00_2514;

						BgL_auxz00_2514 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
							BINT(9589L), BGl_string1868z00zz__bmz00,
							BGl_string1869z00zz__bmz00, BgL_patz00_2373);
						FAILURE(BgL_auxz00_2514, BFALSE, BFALSE);
					}
				return BGl_bmzd2tablezd2zz__bmz00(BgL_auxz00_2511);
			}
		}

	}



/* bm-mmap */
	BGL_EXPORTED_DEF long BGl_bmzd2mmapzd2zz__bmz00(obj_t BgL_tpz00_12,
		obj_t BgL_objz00_13, long BgL_mz00_14)
	{
		{	/* Unsafe/bm.scm 250 */
			{	/* Unsafe/bm.scm 251 */
				bool_t BgL_test1922z00_2519;

				{	/* Unsafe/bm.scm 251 */
					obj_t BgL_tmpz00_2520;

					BgL_tmpz00_2520 = CAR(BgL_tpz00_12);
					BgL_test1922z00_2519 = BGL_U32VECTORP(BgL_tmpz00_2520);
				}
				if (BgL_test1922z00_2519)
					{	/* Unsafe/bm.scm 251 */
						bool_t BgL_test1923z00_2523;

						{	/* Unsafe/bm.scm 251 */
							obj_t BgL_tmpz00_2524;

							BgL_tmpz00_2524 = CDR(BgL_tpz00_12);
							BgL_test1923z00_2523 = BGL_U32VECTORP(BgL_tmpz00_2524);
						}
						if (BgL_test1923z00_2523)
							{	/* Unsafe/bm.scm 251 */
								bool_t BgL_test1924z00_2527;

								{	/* Unsafe/bm.scm 251 */
									obj_t BgL_tmpz00_2528;

									BgL_tmpz00_2528 = CER(BgL_tpz00_12);
									BgL_test1924z00_2527 = STRINGP(BgL_tmpz00_2528);
								}
								if (BgL_test1924z00_2527)
									{	/* Unsafe/bm.scm 251 */
										obj_t BgL_patz00_1320;

										BgL_patz00_1320 = CER(BgL_tpz00_12);
										{	/* Unsafe/bm.scm 251 */
											long BgL_patlenz00_1321;

											BgL_patlenz00_1321 =
												STRING_LENGTH(((obj_t) BgL_patz00_1320));
											{	/* Unsafe/bm.scm 251 */

												if ((BgL_patlenz00_1321 == 0L))
													{	/* Unsafe/bm.scm 251 */
														return (long) (-1L);
													}
												else
													{	/* Unsafe/bm.scm 251 */
														obj_t BgL_delta1z00_1323;
														obj_t BgL_delta2z00_1324;
														long BgL_strlenz00_1325;

														BgL_delta1z00_1323 = CAR(BgL_tpz00_12);
														BgL_delta2z00_1324 = CDR(BgL_tpz00_12);
														BgL_strlenz00_1325 = BGL_MMAP_LENGTH(BgL_objz00_13);
														{	/* Unsafe/bm.scm 251 */
															long BgL_g1057z00_1326;

															{	/* Unsafe/bm.scm 251 */
																long BgL_za71za7_2029;

																BgL_za71za7_2029 = (long) (BgL_mz00_14);
																BgL_g1057z00_1326 =
																	(BgL_za71za7_2029 +
																	(BgL_patlenz00_1321 - 1L));
															}
															{
																long BgL_iz00_1328;

																BgL_iz00_1328 = BgL_g1057z00_1326;
															BgL_zc3z04anonymousza31328ze3z87_1329:
																{	/* Unsafe/bm.scm 251 */
																	bool_t BgL_test1926z00_2543;

																	{	/* Unsafe/bm.scm 251 */
																		long BgL_n1z00_2031;
																		long BgL_n2z00_2032;

																		BgL_n1z00_2031 = BgL_iz00_1328;
																		BgL_n2z00_2032 =
																			(long) (BgL_strlenz00_1325);
																		BgL_test1926z00_2543 =
																			(BgL_n1z00_2031 < BgL_n2z00_2032);
																	}
																	if (BgL_test1926z00_2543)
																		{	/* Unsafe/bm.scm 251 */
																			long BgL_g1058z00_1331;

																			BgL_g1058z00_1331 =
																				(BgL_patlenz00_1321 - 1L);
																			{
																				long BgL_jz00_1333;

																				BgL_jz00_1333 = BgL_g1058z00_1331;
																			BgL_zc3z04anonymousza31330ze3z87_1334:
																				if ((BgL_jz00_1333 < 0L))
																					{	/* Unsafe/bm.scm 251 */
																						long BgL_tmpz00_2549;

																						BgL_tmpz00_2549 =
																							(BgL_iz00_1328 + 1L);
																						return (long) (BgL_tmpz00_2549);
																					}
																				else
																					{	/* Unsafe/bm.scm 251 */
																						bool_t BgL_test1928z00_2552;

																						{	/* Unsafe/bm.scm 251 */
																							unsigned char BgL_arg1344z00_1351;
																							unsigned char BgL_arg1346z00_1352;

																							{	/* Unsafe/bm.scm 251 */
																								long BgL_arg1347z00_1353;

																								BgL_arg1347z00_1353 =
																									(long) (BgL_iz00_1328);
																								{	/* Unsafe/bm.scm 251 */
																									unsigned char
																										BgL_res1837z00_2046;
																									{	/* Unsafe/bm.scm 251 */
																										unsigned char BgL_cz00_2040;

																										BgL_cz00_2040 =
																											BGL_MMAP_REF
																											(BgL_objz00_13,
																											BgL_arg1347z00_1353);
																										{	/* Unsafe/bm.scm 251 */
																											long BgL_arg1833z00_2041;

																											BgL_arg1833z00_2041 =
																												(BgL_arg1347z00_1353 +
																												((long) 1));
																											BGL_MMAP_RP_SET
																												(BgL_objz00_13,
																												BgL_arg1833z00_2041);
																											BUNSPEC;
																											BgL_arg1833z00_2041;
																										}
																										BgL_res1837z00_2046 =
																											BgL_cz00_2040;
																									}
																									BgL_arg1344z00_1351 =
																										BgL_res1837z00_2046;
																							}}
																							BgL_arg1346z00_1352 =
																								STRING_REF(
																								((obj_t) BgL_patz00_1320),
																								BgL_jz00_1333);
																							BgL_test1928z00_2552 =
																								(BgL_arg1344z00_1351 ==
																								BgL_arg1346z00_1352);
																						}
																						if (BgL_test1928z00_2552)
																							{	/* Unsafe/bm.scm 251 */
																								BgL_iz00_1328 =
																									(BgL_iz00_1328 - 1L);
																								{
																									long BgL_jz00_2561;

																									BgL_jz00_2561 =
																										(BgL_jz00_1333 - 1L);
																									BgL_jz00_1333 = BgL_jz00_2561;
																									goto
																										BgL_zc3z04anonymousza31330ze3z87_1334;
																								}
																							}
																						else
																							{	/* Unsafe/bm.scm 251 */
																								long BgL_incz00_1342;

																								{	/* Unsafe/bm.scm 251 */
																									long BgL_az00_1344;
																									long BgL_bz00_1345;

																									{	/* Unsafe/bm.scm 251 */
																										uint32_t
																											BgL_arg1340z00_1347;
																										{	/* Unsafe/bm.scm 251 */
																											long BgL_arg1341z00_1348;

																											{	/* Unsafe/bm.scm 251 */
																												unsigned char
																													BgL_arg1342z00_1349;
																												{	/* Unsafe/bm.scm 251 */
																													unsigned char
																														BgL_res1838z00_2061;
																													{	/* Unsafe/bm.scm 251 */
																														long BgL_iz00_2054;

																														BgL_iz00_2054 =
																															(long)
																															(BgL_iz00_1328);
																														{	/* Unsafe/bm.scm 251 */
																															unsigned char
																																BgL_cz00_2055;
																															BgL_cz00_2055 =
																																BGL_MMAP_REF
																																(BgL_objz00_13,
																																BgL_iz00_2054);
																															{	/* Unsafe/bm.scm 251 */
																																long
																																	BgL_arg1833z00_2056;
																																BgL_arg1833z00_2056
																																	=
																																	(BgL_iz00_2054
																																	+ ((long) 1));
																																BGL_MMAP_RP_SET
																																	(BgL_objz00_13,
																																	BgL_arg1833z00_2056);
																																BUNSPEC;
																																BgL_arg1833z00_2056;
																															}
																															BgL_res1838z00_2061
																																= BgL_cz00_2055;
																													}}
																													BgL_arg1342z00_1349 =
																														BgL_res1838z00_2061;
																												}
																												BgL_arg1341z00_1348 =
																													(BgL_arg1342z00_1349);
																											}
																											BgL_arg1340z00_1347 =
																												BGL_U32VREF
																												(BgL_delta1z00_1323,
																												BgL_arg1341z00_1348);
																										}
																										BgL_az00_1344 =
																											(long)
																											(BgL_arg1340z00_1347);
																									}
																									{	/* Unsafe/bm.scm 251 */
																										uint32_t
																											BgL_arg1343z00_1350;
																										BgL_arg1343z00_1350 =
																											BGL_U32VREF
																											(BgL_delta2z00_1324,
																											BgL_jz00_1333);
																										BgL_bz00_1345 =
																											(long)
																											(BgL_arg1343z00_1350);
																									}
																									if (
																										(BgL_az00_1344 >
																											BgL_bz00_1345))
																										{	/* Unsafe/bm.scm 251 */
																											BgL_incz00_1342 =
																												BgL_az00_1344;
																										}
																									else
																										{	/* Unsafe/bm.scm 251 */
																											BgL_incz00_1342 =
																												BgL_bz00_1345;
																										}
																								}
																								{
																									long BgL_iz00_2574;

																									BgL_iz00_2574 =
																										(BgL_iz00_1328 +
																										BgL_incz00_1342);
																									BgL_iz00_1328 = BgL_iz00_2574;
																									goto
																										BgL_zc3z04anonymousza31328ze3z87_1329;
																								}
																							}
																					}
																			}
																		}
																	else
																		{	/* Unsafe/bm.scm 251 */
																			return (long) (-1L);
									}}}}}}}}
								else
									{	/* Unsafe/bm.scm 251 */
										obj_t BgL_tmpz00_2577;

										BgL_tmpz00_2577 =
											BGl_errorz00zz__errorz00(BGl_string1870z00zz__bmz00,
											BGl_string1871z00zz__bmz00, BgL_tpz00_12);
										return BELONG_TO_LONG(BgL_tmpz00_2577);
									}
							}
						else
							{	/* Unsafe/bm.scm 251 */
								obj_t BgL_arg1350z00_1358;

								BgL_arg1350z00_1358 = CDR(BgL_tpz00_12);
								{	/* Unsafe/bm.scm 251 */
									obj_t BgL_tmpz00_2581;

									BgL_tmpz00_2581 =
										BGl_bigloozd2typezd2errorz00zz__errorz00
										(BGl_string1870z00zz__bmz00, BGl_symbol1872z00zz__bmz00,
										BgL_arg1350z00_1358);
									return BELONG_TO_LONG(BgL_tmpz00_2581);
								}
							}
					}
				else
					{	/* Unsafe/bm.scm 251 */
						obj_t BgL_arg1352z00_1360;

						BgL_arg1352z00_1360 = CAR(BgL_tpz00_12);
						{	/* Unsafe/bm.scm 251 */
							obj_t BgL_tmpz00_2585;

							BgL_tmpz00_2585 =
								BGl_bigloozd2typezd2errorz00zz__errorz00
								(BGl_string1870z00zz__bmz00, BGl_symbol1872z00zz__bmz00,
								BgL_arg1352z00_1360);
							return BELONG_TO_LONG(BgL_tmpz00_2585);
						}
					}
			}
		}

	}



/* &bm-mmap */
	obj_t BGl_z62bmzd2mmapzb0zz__bmz00(obj_t BgL_envz00_2374,
		obj_t BgL_tpz00_2375, obj_t BgL_objz00_2376, obj_t BgL_mz00_2377)
	{
		{	/* Unsafe/bm.scm 250 */
			{	/* Unsafe/bm.scm 251 */
				long BgL_tmpz00_2588;

				{	/* Unsafe/bm.scm 251 */
					long BgL_auxz00_2603;
					obj_t BgL_auxz00_2596;
					obj_t BgL_auxz00_2589;

					{	/* Unsafe/bm.scm 251 */
						obj_t BgL_tmpz00_2604;

						if (ELONGP(BgL_mz00_2377))
							{	/* Unsafe/bm.scm 251 */
								BgL_tmpz00_2604 = BgL_mz00_2377;
							}
						else
							{
								obj_t BgL_auxz00_2607;

								BgL_auxz00_2607 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
									BINT(11306L), BGl_string1874z00zz__bmz00,
									BGl_string1877z00zz__bmz00, BgL_mz00_2377);
								FAILURE(BgL_auxz00_2607, BFALSE, BFALSE);
							}
						BgL_auxz00_2603 = BELONG_TO_LONG(BgL_tmpz00_2604);
					}
					if (BGL_MMAPP(BgL_objz00_2376))
						{	/* Unsafe/bm.scm 251 */
							BgL_auxz00_2596 = BgL_objz00_2376;
						}
					else
						{
							obj_t BgL_auxz00_2599;

							BgL_auxz00_2599 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(11306L), BGl_string1874z00zz__bmz00,
								BGl_string1876z00zz__bmz00, BgL_objz00_2376);
							FAILURE(BgL_auxz00_2599, BFALSE, BFALSE);
						}
					if (EPAIRP(BgL_tpz00_2375))
						{	/* Unsafe/bm.scm 251 */
							BgL_auxz00_2589 = BgL_tpz00_2375;
						}
					else
						{
							obj_t BgL_auxz00_2592;

							BgL_auxz00_2592 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(11306L), BGl_string1874z00zz__bmz00,
								BGl_string1875z00zz__bmz00, BgL_tpz00_2375);
							FAILURE(BgL_auxz00_2592, BFALSE, BFALSE);
						}
					BgL_tmpz00_2588 =
						BGl_bmzd2mmapzd2zz__bmz00(BgL_auxz00_2589, BgL_auxz00_2596,
						BgL_auxz00_2603);
				}
				return make_belong(BgL_tmpz00_2588);
			}
		}

	}



/* bm-string */
	BGL_EXPORTED_DEF long BGl_bmzd2stringzd2zz__bmz00(obj_t BgL_tpz00_15,
		obj_t BgL_objz00_16, long BgL_mz00_17)
	{
		{	/* Unsafe/bm.scm 256 */
			{	/* Unsafe/bm.scm 257 */
				bool_t BgL_test1933z00_2614;

				{	/* Unsafe/bm.scm 257 */
					obj_t BgL_tmpz00_2615;

					BgL_tmpz00_2615 = CAR(BgL_tpz00_15);
					BgL_test1933z00_2614 = BGL_U32VECTORP(BgL_tmpz00_2615);
				}
				if (BgL_test1933z00_2614)
					{	/* Unsafe/bm.scm 257 */
						bool_t BgL_test1934z00_2618;

						{	/* Unsafe/bm.scm 257 */
							obj_t BgL_tmpz00_2619;

							BgL_tmpz00_2619 = CDR(BgL_tpz00_15);
							BgL_test1934z00_2618 = BGL_U32VECTORP(BgL_tmpz00_2619);
						}
						if (BgL_test1934z00_2618)
							{	/* Unsafe/bm.scm 257 */
								bool_t BgL_test1935z00_2622;

								{	/* Unsafe/bm.scm 257 */
									obj_t BgL_tmpz00_2623;

									BgL_tmpz00_2623 = CER(BgL_tpz00_15);
									BgL_test1935z00_2622 = STRINGP(BgL_tmpz00_2623);
								}
								if (BgL_test1935z00_2622)
									{	/* Unsafe/bm.scm 257 */
										obj_t BgL_patz00_1368;

										BgL_patz00_1368 = CER(BgL_tpz00_15);
										{	/* Unsafe/bm.scm 257 */
											long BgL_patlenz00_1369;

											BgL_patlenz00_1369 =
												STRING_LENGTH(((obj_t) BgL_patz00_1368));
											{	/* Unsafe/bm.scm 257 */

												if ((BgL_patlenz00_1369 == 0L))
													{	/* Unsafe/bm.scm 257 */
														return -1L;
													}
												else
													{	/* Unsafe/bm.scm 257 */
														obj_t BgL_delta1z00_1371;
														obj_t BgL_delta2z00_1372;
														long BgL_strlenz00_1373;

														BgL_delta1z00_1371 = CAR(BgL_tpz00_15);
														BgL_delta2z00_1372 = CDR(BgL_tpz00_15);
														BgL_strlenz00_1373 = STRING_LENGTH(BgL_objz00_16);
														{	/* Unsafe/bm.scm 257 */
															long BgL_g1059z00_1374;

															BgL_g1059z00_1374 =
																(BgL_mz00_17 + (BgL_patlenz00_1369 - 1L));
															{
																long BgL_iz00_1376;

																BgL_iz00_1376 = BgL_g1059z00_1374;
															BgL_zc3z04anonymousza31363ze3z87_1377:
																if ((BgL_iz00_1376 < BgL_strlenz00_1373))
																	{	/* Unsafe/bm.scm 257 */
																		long BgL_g1060z00_1379;

																		BgL_g1060z00_1379 =
																			(BgL_patlenz00_1369 - 1L);
																		{
																			long BgL_jz00_1381;

																			BgL_jz00_1381 = BgL_g1060z00_1379;
																		BgL_zc3z04anonymousza31365ze3z87_1382:
																			if ((BgL_jz00_1381 < 0L))
																				{	/* Unsafe/bm.scm 257 */
																					return (BgL_iz00_1376 + 1L);
																				}
																			else
																				{	/* Unsafe/bm.scm 257 */
																					bool_t BgL_test1939z00_2642;

																					{	/* Unsafe/bm.scm 257 */
																						unsigned char BgL_arg1379z00_1400;

																						BgL_arg1379z00_1400 =
																							STRING_REF(BgL_objz00_16,
																							BgL_iz00_1376);
																						BgL_test1939z00_2642 =
																							(BgL_arg1379z00_1400 ==
																							STRING_REF(((obj_t)
																									BgL_patz00_1368),
																								BgL_jz00_1381));
																					}
																					if (BgL_test1939z00_2642)
																						{	/* Unsafe/bm.scm 257 */
																							BgL_iz00_1376 =
																								(BgL_iz00_1376 - 1L);
																							{
																								long BgL_jz00_2648;

																								BgL_jz00_2648 =
																									(BgL_jz00_1381 - 1L);
																								BgL_jz00_1381 = BgL_jz00_2648;
																								goto
																									BgL_zc3z04anonymousza31365ze3z87_1382;
																							}
																						}
																					else
																						{	/* Unsafe/bm.scm 257 */
																							long BgL_incz00_1391;

																							{	/* Unsafe/bm.scm 257 */
																								long BgL_az00_1393;
																								long BgL_bz00_1394;

																								{	/* Unsafe/bm.scm 257 */
																									uint32_t BgL_arg1375z00_1396;

																									{	/* Unsafe/bm.scm 257 */
																										long BgL_tmpz00_2650;

																										BgL_tmpz00_2650 =
																											(STRING_REF(BgL_objz00_16,
																												BgL_iz00_1376));
																										BgL_arg1375z00_1396 =
																											BGL_U32VREF
																											(BgL_delta1z00_1371,
																											BgL_tmpz00_2650);
																									}
																									BgL_az00_1393 =
																										(long)
																										(BgL_arg1375z00_1396);
																								}
																								{	/* Unsafe/bm.scm 257 */
																									uint32_t BgL_arg1378z00_1399;

																									BgL_arg1378z00_1399 =
																										BGL_U32VREF
																										(BgL_delta2z00_1372,
																										BgL_jz00_1381);
																									BgL_bz00_1394 =
																										(long)
																										(BgL_arg1378z00_1399);
																								}
																								if (
																									(BgL_az00_1393 >
																										BgL_bz00_1394))
																									{	/* Unsafe/bm.scm 257 */
																										BgL_incz00_1391 =
																											BgL_az00_1393;
																									}
																								else
																									{	/* Unsafe/bm.scm 257 */
																										BgL_incz00_1391 =
																											BgL_bz00_1394;
																									}
																							}
																							{
																								long BgL_iz00_2659;

																								BgL_iz00_2659 =
																									(BgL_iz00_1376 +
																									BgL_incz00_1391);
																								BgL_iz00_1376 = BgL_iz00_2659;
																								goto
																									BgL_zc3z04anonymousza31363ze3z87_1377;
																							}
																						}
																				}
																		}
																	}
																else
																	{	/* Unsafe/bm.scm 257 */
																		return -1L;
																	}
															}
														}
													}
											}
										}
									}
								else
									{	/* Unsafe/bm.scm 257 */
										return
											(long)
											CINT(BGl_errorz00zz__errorz00(BGl_string1870z00zz__bmz00,
												BGl_string1871z00zz__bmz00, BgL_tpz00_15));
							}}
						else
							{	/* Unsafe/bm.scm 257 */
								obj_t BgL_arg1387z00_1409;

								BgL_arg1387z00_1409 = CDR(BgL_tpz00_15);
								return
									(long)
									CINT(BGl_bigloozd2typezd2errorz00zz__errorz00
									(BGl_string1870z00zz__bmz00, BGl_symbol1872z00zz__bmz00,
										BgL_arg1387z00_1409));
					}}
				else
					{	/* Unsafe/bm.scm 257 */
						obj_t BgL_arg1389z00_1411;

						BgL_arg1389z00_1411 = CAR(BgL_tpz00_15);
						return
							(long)
							CINT(BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string1870z00zz__bmz00, BGl_symbol1872z00zz__bmz00,
								BgL_arg1389z00_1411));
		}}}

	}



/* &bm-string */
	obj_t BGl_z62bmzd2stringzb0zz__bmz00(obj_t BgL_envz00_2378,
		obj_t BgL_tpz00_2379, obj_t BgL_objz00_2380, obj_t BgL_mz00_2381)
	{
		{	/* Unsafe/bm.scm 256 */
			{	/* Unsafe/bm.scm 257 */
				long BgL_tmpz00_2669;

				{	/* Unsafe/bm.scm 257 */
					long BgL_auxz00_2684;
					obj_t BgL_auxz00_2677;
					obj_t BgL_auxz00_2670;

					{	/* Unsafe/bm.scm 257 */
						obj_t BgL_tmpz00_2685;

						if (INTEGERP(BgL_mz00_2381))
							{	/* Unsafe/bm.scm 257 */
								BgL_tmpz00_2685 = BgL_mz00_2381;
							}
						else
							{
								obj_t BgL_auxz00_2688;

								BgL_auxz00_2688 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
									BINT(11609L), BGl_string1878z00zz__bmz00,
									BGl_string1879z00zz__bmz00, BgL_mz00_2381);
								FAILURE(BgL_auxz00_2688, BFALSE, BFALSE);
							}
						BgL_auxz00_2684 = (long) CINT(BgL_tmpz00_2685);
					}
					if (STRINGP(BgL_objz00_2380))
						{	/* Unsafe/bm.scm 257 */
							BgL_auxz00_2677 = BgL_objz00_2380;
						}
					else
						{
							obj_t BgL_auxz00_2680;

							BgL_auxz00_2680 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(11609L), BGl_string1878z00zz__bmz00,
								BGl_string1869z00zz__bmz00, BgL_objz00_2380);
							FAILURE(BgL_auxz00_2680, BFALSE, BFALSE);
						}
					if (EPAIRP(BgL_tpz00_2379))
						{	/* Unsafe/bm.scm 257 */
							BgL_auxz00_2670 = BgL_tpz00_2379;
						}
					else
						{
							obj_t BgL_auxz00_2673;

							BgL_auxz00_2673 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(11609L), BGl_string1878z00zz__bmz00,
								BGl_string1875z00zz__bmz00, BgL_tpz00_2379);
							FAILURE(BgL_auxz00_2673, BFALSE, BFALSE);
						}
					BgL_tmpz00_2669 =
						BGl_bmzd2stringzd2zz__bmz00(BgL_auxz00_2670, BgL_auxz00_2677,
						BgL_auxz00_2684);
				}
				return BINT(BgL_tmpz00_2669);
			}
		}

	}



/* bmh-table */
	BGL_EXPORTED_DEF obj_t BGl_bmhzd2tablezd2zz__bmz00(obj_t BgL_patz00_18)
	{
		{	/* Unsafe/bm.scm 262 */
			{	/* Unsafe/bm.scm 263 */
				obj_t BgL_delta1z00_2107;

				{	/* Unsafe/bm.scm 263 */

					BgL_delta1z00_2107 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(256L, (uint32_t) (0));
				}
				BGl_makezd2delta1zd2zz__bmz00(BgL_delta1z00_2107, BgL_patz00_18);
				return MAKE_YOUNG_PAIR(BgL_delta1z00_2107, BgL_patz00_18);
			}
		}

	}



/* &bmh-table */
	obj_t BGl_z62bmhzd2tablezb0zz__bmz00(obj_t BgL_envz00_2382,
		obj_t BgL_patz00_2383)
	{
		{	/* Unsafe/bm.scm 262 */
			{	/* Unsafe/bm.scm 263 */
				obj_t BgL_auxz00_2698;

				if (STRINGP(BgL_patz00_2383))
					{	/* Unsafe/bm.scm 263 */
						BgL_auxz00_2698 = BgL_patz00_2383;
					}
				else
					{
						obj_t BgL_auxz00_2701;

						BgL_auxz00_2701 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
							BINT(11921L), BGl_string1880z00zz__bmz00,
							BGl_string1869z00zz__bmz00, BgL_patz00_2383);
						FAILURE(BgL_auxz00_2701, BFALSE, BFALSE);
					}
				return BGl_bmhzd2tablezd2zz__bmz00(BgL_auxz00_2698);
			}
		}

	}



/* bmh-mmap */
	BGL_EXPORTED_DEF long BGl_bmhzd2mmapzd2zz__bmz00(obj_t BgL_tpz00_19,
		obj_t BgL_objz00_20, long BgL_mz00_21)
	{
		{	/* Unsafe/bm.scm 327 */
			{	/* Unsafe/bm.scm 328 */
				bool_t BgL_test1945z00_2706;

				{	/* Unsafe/bm.scm 328 */
					obj_t BgL_tmpz00_2707;

					BgL_tmpz00_2707 = CAR(BgL_tpz00_19);
					BgL_test1945z00_2706 = BGL_U32VECTORP(BgL_tmpz00_2707);
				}
				if (BgL_test1945z00_2706)
					{	/* Unsafe/bm.scm 328 */
						bool_t BgL_test1946z00_2710;

						{	/* Unsafe/bm.scm 328 */
							obj_t BgL_tmpz00_2711;

							BgL_tmpz00_2711 = CDR(BgL_tpz00_19);
							BgL_test1946z00_2710 = STRINGP(BgL_tmpz00_2711);
						}
						if (BgL_test1946z00_2710)
							{	/* Unsafe/bm.scm 328 */
								obj_t BgL_patz00_1421;

								BgL_patz00_1421 = CDR(BgL_tpz00_19);
								{	/* Unsafe/bm.scm 328 */
									long BgL_patlenz00_1422;

									BgL_patlenz00_1422 = STRING_LENGTH(((obj_t) BgL_patz00_1421));
									{	/* Unsafe/bm.scm 328 */

										if ((BgL_patlenz00_1422 == 0L))
											{	/* Unsafe/bm.scm 328 */
												return (long) (-1L);
											}
										else
											{	/* Unsafe/bm.scm 328 */
												obj_t BgL_delta1z00_1424;
												long BgL_strlenz00_1425;

												BgL_delta1z00_1424 = CAR(BgL_tpz00_19);
												BgL_strlenz00_1425 = BGL_MMAP_LENGTH(BgL_objz00_20);
												{
													long BgL_skipz00_1427;

													BgL_skipz00_1427 = 0L;
												BgL_zc3z04anonymousza31397ze3z87_1428:
													{	/* Unsafe/bm.scm 328 */
														bool_t BgL_test1948z00_2722;

														{	/* Unsafe/bm.scm 328 */
															long BgL_tmpz00_2723;

															{	/* Unsafe/bm.scm 328 */
																long BgL_za71za7_2118;

																BgL_za71za7_2118 = (long) (BgL_strlenz00_1425);
																BgL_tmpz00_2723 =
																	(BgL_za71za7_2118 - BgL_skipz00_1427);
															}
															BgL_test1948z00_2722 =
																(BgL_tmpz00_2723 >= BgL_patlenz00_1422);
														}
														if (BgL_test1948z00_2722)
															{	/* Unsafe/bm.scm 328 */
																long BgL_g1064z00_1431;

																BgL_g1064z00_1431 = (BgL_patlenz00_1422 - 1L);
																{
																	long BgL_iz00_1433;

																	BgL_iz00_1433 = BgL_g1064z00_1431;
																BgL_zc3z04anonymousza31401ze3z87_1434:
																	{	/* Unsafe/bm.scm 328 */
																		bool_t BgL_test1949z00_2728;

																		{	/* Unsafe/bm.scm 328 */
																			unsigned char BgL_arg1416z00_1448;
																			unsigned char BgL_arg1417z00_1449;

																			{	/* Unsafe/bm.scm 328 */
																				long BgL_arg1418z00_1450;

																				BgL_arg1418z00_1450 =
																					(BgL_skipz00_1427 + BgL_iz00_1433);
																				{	/* Unsafe/bm.scm 328 */
																					unsigned char BgL_res1839z00_2133;

																					{	/* Unsafe/bm.scm 328 */
																						long BgL_iz00_2126;

																						BgL_iz00_2126 =
																							(long) (BgL_arg1418z00_1450);
																						{	/* Unsafe/bm.scm 328 */
																							unsigned char BgL_cz00_2127;

																							BgL_cz00_2127 =
																								BGL_MMAP_REF(BgL_objz00_20,
																								BgL_iz00_2126);
																							{	/* Unsafe/bm.scm 328 */
																								long BgL_arg1833z00_2128;

																								BgL_arg1833z00_2128 =
																									(BgL_iz00_2126 + ((long) 1));
																								BGL_MMAP_RP_SET(BgL_objz00_20,
																									BgL_arg1833z00_2128);
																								BUNSPEC;
																								BgL_arg1833z00_2128;
																							}
																							BgL_res1839z00_2133 =
																								BgL_cz00_2127;
																					}}
																					BgL_arg1416z00_1448 =
																						BgL_res1839z00_2133;
																			}}
																			BgL_arg1417z00_1449 =
																				STRING_REF(
																				((obj_t) BgL_patz00_1421),
																				BgL_iz00_1433);
																			BgL_test1949z00_2728 =
																				(BgL_arg1416z00_1448 ==
																				BgL_arg1417z00_1449);
																		}
																		if (BgL_test1949z00_2728)
																			{	/* Unsafe/bm.scm 328 */
																				if ((BgL_iz00_1433 == 0L))
																					{	/* Unsafe/bm.scm 328 */
																						return (long) (BgL_skipz00_1427);
																					}
																				else
																					{
																						long BgL_iz00_2740;

																						BgL_iz00_2740 =
																							(BgL_iz00_1433 - 1L);
																						BgL_iz00_1433 = BgL_iz00_2740;
																						goto
																							BgL_zc3z04anonymousza31401ze3z87_1434;
																					}
																			}
																		else
																			{	/* Unsafe/bm.scm 328 */
																				long BgL_arg1408z00_1441;

																				{	/* Unsafe/bm.scm 328 */
																					long BgL_arg1410z00_1442;

																					{	/* Unsafe/bm.scm 328 */
																						uint32_t BgL_arg1411z00_1443;

																						{	/* Unsafe/bm.scm 328 */
																							long BgL_arg1412z00_1444;

																							{	/* Unsafe/bm.scm 328 */
																								unsigned char
																									BgL_arg1413z00_1445;
																								{	/* Unsafe/bm.scm 328 */
																									long BgL_arg1414z00_1446;

																									BgL_arg1414z00_1446 =
																										(BgL_skipz00_1427 +
																										(BgL_patlenz00_1422 - 1L));
																									{	/* Unsafe/bm.scm 328 */
																										unsigned char
																											BgL_res1840z00_2152;
																										{	/* Unsafe/bm.scm 328 */
																											long BgL_iz00_2145;

																											BgL_iz00_2145 =
																												(long)
																												(BgL_arg1414z00_1446);
																											{	/* Unsafe/bm.scm 328 */
																												unsigned char
																													BgL_cz00_2146;
																												BgL_cz00_2146 =
																													BGL_MMAP_REF
																													(BgL_objz00_20,
																													BgL_iz00_2145);
																												{	/* Unsafe/bm.scm 328 */
																													long
																														BgL_arg1833z00_2147;
																													BgL_arg1833z00_2147 =
																														(BgL_iz00_2145 +
																														((long) 1));
																													BGL_MMAP_RP_SET
																														(BgL_objz00_20,
																														BgL_arg1833z00_2147);
																													BUNSPEC;
																													BgL_arg1833z00_2147;
																												}
																												BgL_res1840z00_2152 =
																													BgL_cz00_2146;
																										}}
																										BgL_arg1413z00_1445 =
																											BgL_res1840z00_2152;
																								}}
																								BgL_arg1412z00_1444 =
																									(BgL_arg1413z00_1445);
																							}
																							BgL_arg1411z00_1443 =
																								BGL_U32VREF(BgL_delta1z00_1424,
																								BgL_arg1412z00_1444);
																						}
																						BgL_arg1410z00_1442 =
																							(long) (BgL_arg1411z00_1443);
																					}
																					BgL_arg1408z00_1441 =
																						(BgL_skipz00_1427 +
																						BgL_arg1410z00_1442);
																				}
																				{
																					long BgL_skipz00_2752;

																					BgL_skipz00_2752 =
																						BgL_arg1408z00_1441;
																					BgL_skipz00_1427 = BgL_skipz00_2752;
																					goto
																						BgL_zc3z04anonymousza31397ze3z87_1428;
																				}
																			}
																	}
																}
															}
														else
															{	/* Unsafe/bm.scm 328 */
																return (long) (-1L);
							}}}}}}}
						else
							{	/* Unsafe/bm.scm 328 */
								obj_t BgL_tmpz00_2754;

								BgL_tmpz00_2754 =
									BGl_errorz00zz__errorz00(BGl_string1881z00zz__bmz00,
									BGl_string1882z00zz__bmz00, BgL_tpz00_19);
								return BELONG_TO_LONG(BgL_tmpz00_2754);
							}
					}
				else
					{	/* Unsafe/bm.scm 328 */
						obj_t BgL_arg1421z00_1455;

						BgL_arg1421z00_1455 = CAR(BgL_tpz00_19);
						{	/* Unsafe/bm.scm 328 */
							obj_t BgL_tmpz00_2758;

							BgL_tmpz00_2758 =
								BGl_bigloozd2typezd2errorz00zz__errorz00
								(BGl_string1881z00zz__bmz00, BGl_symbol1872z00zz__bmz00,
								BgL_arg1421z00_1455);
							return BELONG_TO_LONG(BgL_tmpz00_2758);
						}
					}
			}
		}

	}



/* &bmh-mmap */
	obj_t BGl_z62bmhzd2mmapzb0zz__bmz00(obj_t BgL_envz00_2384,
		obj_t BgL_tpz00_2385, obj_t BgL_objz00_2386, obj_t BgL_mz00_2387)
	{
		{	/* Unsafe/bm.scm 327 */
			{	/* Unsafe/bm.scm 328 */
				long BgL_tmpz00_2761;

				{	/* Unsafe/bm.scm 328 */
					long BgL_auxz00_2776;
					obj_t BgL_auxz00_2769;
					obj_t BgL_auxz00_2762;

					{	/* Unsafe/bm.scm 328 */
						obj_t BgL_tmpz00_2777;

						if (ELONGP(BgL_mz00_2387))
							{	/* Unsafe/bm.scm 328 */
								BgL_tmpz00_2777 = BgL_mz00_2387;
							}
						else
							{
								obj_t BgL_auxz00_2780;

								BgL_auxz00_2780 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
									BINT(14503L), BGl_string1883z00zz__bmz00,
									BGl_string1877z00zz__bmz00, BgL_mz00_2387);
								FAILURE(BgL_auxz00_2780, BFALSE, BFALSE);
							}
						BgL_auxz00_2776 = BELONG_TO_LONG(BgL_tmpz00_2777);
					}
					if (BGL_MMAPP(BgL_objz00_2386))
						{	/* Unsafe/bm.scm 328 */
							BgL_auxz00_2769 = BgL_objz00_2386;
						}
					else
						{
							obj_t BgL_auxz00_2772;

							BgL_auxz00_2772 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(14503L), BGl_string1883z00zz__bmz00,
								BGl_string1876z00zz__bmz00, BgL_objz00_2386);
							FAILURE(BgL_auxz00_2772, BFALSE, BFALSE);
						}
					if (EPAIRP(BgL_tpz00_2385))
						{	/* Unsafe/bm.scm 328 */
							BgL_auxz00_2762 = BgL_tpz00_2385;
						}
					else
						{
							obj_t BgL_auxz00_2765;

							BgL_auxz00_2765 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(14503L), BGl_string1883z00zz__bmz00,
								BGl_string1875z00zz__bmz00, BgL_tpz00_2385);
							FAILURE(BgL_auxz00_2765, BFALSE, BFALSE);
						}
					BgL_tmpz00_2761 =
						BGl_bmhzd2mmapzd2zz__bmz00(BgL_auxz00_2762, BgL_auxz00_2769,
						BgL_auxz00_2776);
				}
				return make_belong(BgL_tmpz00_2761);
			}
		}

	}



/* bmh-string */
	BGL_EXPORTED_DEF long BGl_bmhzd2stringzd2zz__bmz00(obj_t BgL_tpz00_22,
		obj_t BgL_objz00_23, long BgL_mz00_24)
	{
		{	/* Unsafe/bm.scm 333 */
			{	/* Unsafe/bm.scm 334 */
				bool_t BgL_test1954z00_2787;

				{	/* Unsafe/bm.scm 334 */
					obj_t BgL_tmpz00_2788;

					BgL_tmpz00_2788 = CAR(BgL_tpz00_22);
					BgL_test1954z00_2787 = BGL_U32VECTORP(BgL_tmpz00_2788);
				}
				if (BgL_test1954z00_2787)
					{	/* Unsafe/bm.scm 334 */
						bool_t BgL_test1955z00_2791;

						{	/* Unsafe/bm.scm 334 */
							obj_t BgL_tmpz00_2792;

							BgL_tmpz00_2792 = CDR(BgL_tpz00_22);
							BgL_test1955z00_2791 = STRINGP(BgL_tmpz00_2792);
						}
						if (BgL_test1955z00_2791)
							{	/* Unsafe/bm.scm 334 */
								obj_t BgL_patz00_1461;

								BgL_patz00_1461 = CDR(BgL_tpz00_22);
								{	/* Unsafe/bm.scm 334 */
									long BgL_patlenz00_1462;

									BgL_patlenz00_1462 = STRING_LENGTH(((obj_t) BgL_patz00_1461));
									{	/* Unsafe/bm.scm 334 */

										if ((BgL_patlenz00_1462 == 0L))
											{	/* Unsafe/bm.scm 334 */
												return -1L;
											}
										else
											{	/* Unsafe/bm.scm 334 */
												obj_t BgL_delta1z00_1464;
												long BgL_strlenz00_1465;

												BgL_delta1z00_1464 = CAR(BgL_tpz00_22);
												BgL_strlenz00_1465 = STRING_LENGTH(BgL_objz00_23);
												{
													long BgL_skipz00_1467;

													BgL_skipz00_1467 = 0L;
												BgL_zc3z04anonymousza31428ze3z87_1468:
													if (
														((BgL_strlenz00_1465 - BgL_skipz00_1467) >=
															BgL_patlenz00_1462))
														{	/* Unsafe/bm.scm 334 */
															long BgL_g1065z00_1471;

															BgL_g1065z00_1471 = (BgL_patlenz00_1462 - 1L);
															{
																long BgL_iz00_1473;

																BgL_iz00_1473 = BgL_g1065z00_1471;
															BgL_zc3z04anonymousza31431ze3z87_1474:
																if (
																	(STRING_REF(BgL_objz00_23,
																			(BgL_skipz00_1467 + BgL_iz00_1473)) ==
																		STRING_REF(
																			((obj_t) BgL_patz00_1461),
																			BgL_iz00_1473)))
																	{	/* Unsafe/bm.scm 334 */
																		if ((BgL_iz00_1473 == 0L))
																			{	/* Unsafe/bm.scm 334 */
																				return BgL_skipz00_1467;
																			}
																		else
																			{
																				long BgL_iz00_2814;

																				BgL_iz00_2814 = (BgL_iz00_1473 - 1L);
																				BgL_iz00_1473 = BgL_iz00_2814;
																				goto
																					BgL_zc3z04anonymousza31431ze3z87_1474;
																			}
																	}
																else
																	{	/* Unsafe/bm.scm 334 */
																		long BgL_arg1440z00_1482;

																		{	/* Unsafe/bm.scm 334 */
																			long BgL_arg1441z00_1483;

																			{	/* Unsafe/bm.scm 334 */
																				uint32_t BgL_arg1442z00_1484;

																				{	/* Unsafe/bm.scm 334 */
																					long BgL_tmpz00_2816;

																					BgL_tmpz00_2816 =
																						(STRING_REF(BgL_objz00_23,
																							(BgL_skipz00_1467 +
																								(BgL_patlenz00_1462 - 1L))));
																					BgL_arg1442z00_1484 =
																						BGL_U32VREF(BgL_delta1z00_1464,
																						BgL_tmpz00_2816);
																				}
																				BgL_arg1441z00_1483 =
																					(long) (BgL_arg1442z00_1484);
																			}
																			BgL_arg1440z00_1482 =
																				(BgL_skipz00_1467 +
																				BgL_arg1441z00_1483);
																		}
																		{
																			long BgL_skipz00_2824;

																			BgL_skipz00_2824 = BgL_arg1440z00_1482;
																			BgL_skipz00_1467 = BgL_skipz00_2824;
																			goto
																				BgL_zc3z04anonymousza31428ze3z87_1468;
																		}
																	}
															}
														}
													else
														{	/* Unsafe/bm.scm 334 */
															return -1L;
														}
												}
											}
									}
								}
							}
						else
							{	/* Unsafe/bm.scm 334 */
								return
									(long)
									CINT(BGl_errorz00zz__errorz00(BGl_string1881z00zz__bmz00,
										BGl_string1882z00zz__bmz00, BgL_tpz00_22));
					}}
				else
					{	/* Unsafe/bm.scm 334 */
						obj_t BgL_arg1452z00_1497;

						BgL_arg1452z00_1497 = CAR(BgL_tpz00_22);
						return
							(long)
							CINT(BGl_bigloozd2typezd2errorz00zz__errorz00
							(BGl_string1881z00zz__bmz00, BGl_symbol1872z00zz__bmz00,
								BgL_arg1452z00_1497));
		}}}

	}



/* &bmh-string */
	obj_t BGl_z62bmhzd2stringzb0zz__bmz00(obj_t BgL_envz00_2388,
		obj_t BgL_tpz00_2389, obj_t BgL_objz00_2390, obj_t BgL_mz00_2391)
	{
		{	/* Unsafe/bm.scm 333 */
			{	/* Unsafe/bm.scm 334 */
				long BgL_tmpz00_2830;

				{	/* Unsafe/bm.scm 334 */
					long BgL_auxz00_2845;
					obj_t BgL_auxz00_2838;
					obj_t BgL_auxz00_2831;

					{	/* Unsafe/bm.scm 334 */
						obj_t BgL_tmpz00_2846;

						if (INTEGERP(BgL_mz00_2391))
							{	/* Unsafe/bm.scm 334 */
								BgL_tmpz00_2846 = BgL_mz00_2391;
							}
						else
							{
								obj_t BgL_auxz00_2849;

								BgL_auxz00_2849 =
									BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
									BINT(14808L), BGl_string1884z00zz__bmz00,
									BGl_string1879z00zz__bmz00, BgL_mz00_2391);
								FAILURE(BgL_auxz00_2849, BFALSE, BFALSE);
							}
						BgL_auxz00_2845 = (long) CINT(BgL_tmpz00_2846);
					}
					if (STRINGP(BgL_objz00_2390))
						{	/* Unsafe/bm.scm 334 */
							BgL_auxz00_2838 = BgL_objz00_2390;
						}
					else
						{
							obj_t BgL_auxz00_2841;

							BgL_auxz00_2841 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(14808L), BGl_string1884z00zz__bmz00,
								BGl_string1869z00zz__bmz00, BgL_objz00_2390);
							FAILURE(BgL_auxz00_2841, BFALSE, BFALSE);
						}
					if (EPAIRP(BgL_tpz00_2389))
						{	/* Unsafe/bm.scm 334 */
							BgL_auxz00_2831 = BgL_tpz00_2389;
						}
					else
						{
							obj_t BgL_auxz00_2834;

							BgL_auxz00_2834 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1867z00zz__bmz00,
								BINT(14808L), BGl_string1884z00zz__bmz00,
								BGl_string1875z00zz__bmz00, BgL_tpz00_2389);
							FAILURE(BgL_auxz00_2834, BFALSE, BFALSE);
						}
					BgL_tmpz00_2830 =
						BGl_bmhzd2stringzd2zz__bmz00(BgL_auxz00_2831, BgL_auxz00_2838,
						BgL_auxz00_2845);
				}
				return BINT(BgL_tmpz00_2830);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__bmz00(void)
	{
		{	/* Unsafe/bm.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__bmz00(void)
	{
		{	/* Unsafe/bm.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__bmz00(void)
	{
		{	/* Unsafe/bm.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__bmz00(void)
	{
		{	/* Unsafe/bm.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1885z00zz__bmz00));
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1885z00zz__bmz00));
		}

	}

#ifdef __cplusplus
}
#endif
