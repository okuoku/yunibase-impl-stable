/*===========================================================================*/
/*   (Unsafe/pregexp.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/pregexp.scm -indent -o objs/obj_u/Unsafe/pregexp.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___REGEXP_TYPE_DEFINITIONS
#define BGL___REGEXP_TYPE_DEFINITIONS
#endif													// BGL___REGEXP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_substringz00zz__r4_strings_6_7z00(obj_t, long, long);
	static obj_t BGl_list2531z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_list2534z00zz__regexpz00 = BUNSPEC;
	extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_list2618z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_list2537z00zz__regexpz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2quotezd2zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_regexpzd2patternzd2zz__regexpz00(obj_t);
	static obj_t BGl_z62fkz62zz__regexpz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31919ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_pregexpzd2readzd2branchz00zz__regexpz00(obj_t, obj_t, long);
	static obj_t BGl_z52pregexpz52zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regexpzf3zf3zz__regexpz00(obj_t);
	static obj_t BGl_symbol2496z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2498z00zz__regexpz00 = BUNSPEC;
	extern obj_t bstring_to_keyword(obj_t);
	static obj_t BGl_z62regexpzf3z91zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__regexpz00 = BUNSPEC;
	static obj_t
		BGl_pregexpzd2checkzd2ifzd2inzd2charzd2classzf3z21zz__regexpz00(unsigned
		char, obj_t);
	static obj_t BGl_z62loupzd2orzb0zz__regexpz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_pregexpzd2readzd2subpatternz00zz__regexpz00(obj_t, long,
		long);
	static obj_t BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00(obj_t,
		obj_t, long);
	static obj_t BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t,
		obj_t);
	static obj_t BGl_symbol2586z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31864ze3ze5zz__regexpz00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_symbol2590z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2592z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_pregexpzd2listzd2refz00zz__regexpz00(obj_t, obj_t);
	static bool_t BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00;
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t, obj_t,
		obj_t);
	static unsigned char BGl_za2pregexpzd2tabzd2charza2z00zz__regexpz00;
	BGL_EXPORTED_DECL long
		BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t, obj_t, obj_t,
		long, long, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__regexpz00(void);
	static obj_t BGl_pregexpzd2readzd2patternz00zz__regexpz00(obj_t, obj_t, long);
	static obj_t BGl_z62subz62zz__regexpz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31890ze3ze5zz__regexpz00(obj_t);
	static obj_t BGl_pregexpzd2readzd2charzd2listzd2zz__regexpz00(obj_t, long,
		long);
	static obj_t BGl_z62zc3z04anonymousza31858ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_z62pregexpzd2quotezb0zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_keyword2501z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31900ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_keyword2503z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2505z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2508z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00(obj_t,
		obj_t, long);
	static obj_t BGl_cnstzd2initzd2zz__regexpz00(void);
	extern obj_t BGl_assvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zz__regexpz00(void);
	static long BGl_za2pregexpzd2versionza2zd2zz__regexpz00 = 0L;
	static obj_t BGl__pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__regexpz00(void);
	static obj_t BGl_keyword2511z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31901ze3ze5zz__regexpz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zz__regexpz00(void);
	static obj_t BGl_list2596z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2517z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2519z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zz__regexpz00(void);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_pregexpzd2replacezd2auxz00zz__regexpz00(obj_t, obj_t, long,
		obj_t);
	static obj_t BGl_z62loupzd2pzb0zz__regexpz00(obj_t, obj_t, bool_t, obj_t,
		obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, long,
		obj_t);
	static obj_t BGl_z62loupzd2qzb0zz__regexpz00(obj_t, bool_t, bool_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, long, obj_t);
	static unsigned char BGl_za2pregexpzd2returnzd2charza2z00zz__regexpz00;
	static obj_t BGl_keyword2521z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2526z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl__pregexpzd2matchzd2zz__regexpz00(obj_t, obj_t);
	extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_keyword2529z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31869ze3ze5zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpz00zz__regexpz00(obj_t, obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62pregexpz62zz__regexpz00(obj_t, obj_t, obj_t);
	static obj_t BGl_keyword2532z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2535z00zz__regexpz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2splitzd2zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_keyword2538z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31894ze3ze5zz__regexpz00(obj_t);
	static obj_t BGl_z62loupzd2seqzb0zz__regexpz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_pregexpzd2readzd2clusterzd2typezd2zz__regexpz00(obj_t, long,
		long);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2replacezd2zz__regexpz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_pregexpzd2reversez12zc0zz__regexpz00(obj_t);
	static obj_t BGl_z62pregexpzd2replacezb0zz__regexpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62pregexpzd2replaceza2z12zz__regexpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_keyword2548z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31879ze3ze5zz__regexpz00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_pregexpzd2readzd2escapedzd2charzd2zz__regexpz00(obj_t, obj_t,
		long);
	static obj_t BGl_z62zc3z04anonymousza31824ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_pregexpzd2readzd2posixzd2charzd2classz00zz__regexpz00(obj_t,
		long, long);
	static obj_t BGl_z62zc3z04anonymousza31816ze3ze5zz__regexpz00(obj_t);
	static obj_t BGl_appendzd221011zd2zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_z62regexpzd2patternzb0zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_symbol2514z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zz__regexpz00(void);
	static obj_t BGl_pregexpzd2matchzd2positionszd2auxzd2zz__regexpz00(obj_t,
		obj_t, long, obj_t, obj_t, obj_t);
	static obj_t BGl_z62regexpzd2capturezd2countz62zz__regexpz00(obj_t, obj_t);
	static long BGl_za2pregexpzd2nulzd2charzd2intza2zd2zz__regexpz00 = 0L;
	static obj_t BGl_z62identityz62zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31841ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_keyword2560z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31914ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31833ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_keyword2480z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31825ze3ze5zz__regexpz00(obj_t);
	static obj_t BGl_keyword2562z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2482z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62loupzd2onezd2ofzd2charszb0zz__regexpz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_keyword2564z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_pregexpzd2atzd2wordzd2boundaryzf3z21zz__regexpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_keyword2484z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_pregexpzd2readzd2piecez00zz__regexpz00(obj_t, obj_t, long);
	static obj_t BGl_keyword2566z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2486z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2568z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2488z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2600z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2523z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2607z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2609z00zz__regexpz00 = BUNSPEC;
	extern bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_keyword2570z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2490z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_pregexpzd2readzd2numsz00zz__regexpz00(obj_t, long, long);
	static obj_t BGl_keyword2572z00zz__regexpz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2matchzd2zz__regexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_keyword2492z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2574z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2494z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2576z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62pregexpzd2splitzb0zz__regexpz00(obj_t, obj_t, obj_t);
	static unsigned char BGl_za2pregexpzd2commentzd2charza2z00zz__regexpz00;
	extern obj_t bgl_make_regexp(obj_t);
	static obj_t BGl_keyword2578z00zz__regexpz00 = BUNSPEC;
	BGL_EXPORTED_DECL long BGl_regexpzd2capturezd2countz00zz__regexpz00(obj_t);
	static obj_t BGl_list2507z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__regexpz00(obj_t);
	static obj_t BGl_keyword2580z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2582z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_keyword2584z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_subze70ze7zz__regexpz00(obj_t);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t BGl_symbol2540z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2542z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_list2510z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2544z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2546z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_list2513z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_list2516z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2550z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2553z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2555z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_symbol2558z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_list2525z00zz__regexpz00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_list2528z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_z62g1104z62zz__regexpz00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2594z00zz__regexpz00,
		BgL_bgl_za762za7c3za704anonymo2621za7,
		BGl_z62zc3z04anonymousza31864ze3ze5zz__regexpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2595z00zz__regexpz00,
		BgL_bgl_za762za7c3za704anonymo2622za7,
		BGl_z62zc3z04anonymousza31869ze3ze5zz__regexpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2597z00zz__regexpz00,
		BgL_bgl_za762za7c3za704anonymo2623za7,
		BGl_z62zc3z04anonymousza31879ze3ze5zz__regexpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2598z00zz__regexpz00,
		BgL_bgl_za762za7c3za704anonymo2624za7,
		BGl_z62zc3z04anonymousza31890ze3ze5zz__regexpz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2599z00zz__regexpz00,
		BgL_bgl_za762za7c3za704anonymo2625za7,
		BGl_z62zc3z04anonymousza31894ze3ze5zz__regexpz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2envzd2zz__regexpz00,
		BgL_bgl_za762pregexpza762za7za7_2626z00, va_generic_entry,
		BGl_z62pregexpz62zz__regexpz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regexpzd2capturezd2countzd2envzd2zz__regexpz00,
		BgL_bgl_za762regexpza7d2capt2627z00,
		BGl_z62regexpzd2capturezd2countz62zz__regexpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2500z00zz__regexpz00,
		BgL_bgl_string2500za700za7za7_2628za7, "pregexp", 7);
	      DEFINE_STRING(BGl_string2502z00zz__regexpz00,
		BgL_bgl_string2502za700za7za7_2629za7, "wbdry", 5);
	      DEFINE_STRING(BGl_string2504z00zz__regexpz00,
		BgL_bgl_string2504za700za7za7_2630za7, "not-wbdry", 9);
	      DEFINE_STRING(BGl_string2506z00zz__regexpz00,
		BgL_bgl_string2506za700za7za7_2631za7, "digit", 5);
	      DEFINE_STRING(BGl_string2509z00zz__regexpz00,
		BgL_bgl_string2509za700za7za7_2632za7, "space", 5);
	      DEFINE_STRING(BGl_string2512z00zz__regexpz00,
		BgL_bgl_string2512za700za7za7_2633za7, "word", 4);
	      DEFINE_STRING(BGl_string2515z00zz__regexpz00,
		BgL_bgl_string2515za700za7za7_2634za7, "pregexp-read-posix-char-class", 29);
	      DEFINE_STRING(BGl_string2518z00zz__regexpz00,
		BgL_bgl_string2518za700za7za7_2635za7, "sub", 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2splitzd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2spl2636z00, BGl_z62pregexpzd2splitzb0zz__regexpz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2quotezd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2quo2637z00, BGl_z62pregexpzd2quotezb0zz__regexpz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regexpzd2patternzd2envz00zz__regexpz00,
		BgL_bgl_za762regexpza7d2patt2638z00,
		BGl_z62regexpzd2patternzb0zz__regexpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2replacezd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2rep2639z00,
		BGl_z62pregexpzd2replacezb0zz__regexpz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2601z00zz__regexpz00,
		BgL_bgl_string2601za700za7za7_2640za7,
		"greedy-quantifier-operand-could-be-empty", 40);
	      DEFINE_STRING(BGl_string2520z00zz__regexpz00,
		BgL_bgl_string2520za700za7za7_2641za7, "case-sensitive", 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regexpzf3zd2envz21zz__regexpz00,
		BgL_bgl_za762regexpza7f3za791za72642z00, BGl_z62regexpzf3z91zz__regexpz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2602z00zz__regexpz00,
		BgL_bgl_string2602za700za7za7_2643za7, "", 0);
	      DEFINE_STRING(BGl_string2603z00zz__regexpz00,
		BgL_bgl_string2603za700za7za7_2644za7, "&pregexp", 8);
	      DEFINE_STRING(BGl_string2522z00zz__regexpz00,
		BgL_bgl_string2522za700za7za7_2645za7, "case-insensitive", 16);
	      DEFINE_STRING(BGl_string2604z00zz__regexpz00,
		BgL_bgl_string2604za700za7za7_2646za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2605z00zz__regexpz00,
		BgL_bgl_string2605za700za7za7_2647za7, "(*UTF8)", 7);
	      DEFINE_STRING(BGl_string2524z00zz__regexpz00,
		BgL_bgl_string2524za700za7za7_2648za7, "pregexp-read-cluster-type", 25);
	      DEFINE_STRING(BGl_string2606z00zz__regexpz00,
		BgL_bgl_string2606za700za7za7_2649za7, "_pregexp-match-positions", 24);
	      DEFINE_STRING(BGl_string2608z00zz__regexpz00,
		BgL_bgl_string2608za700za7za7_2650za7,
		"pattern-must-be-compiled-or-string-regexp", 41);
	      DEFINE_STRING(BGl_string2527z00zz__regexpz00,
		BgL_bgl_string2527za700za7za7_2651za7, "lookahead", 9);
	      DEFINE_STRING(BGl_string2610z00zz__regexpz00,
		BgL_bgl_string2610za700za7za7_2652za7, "pregexp-match-positions", 23);
	      DEFINE_STRING(BGl_string2611z00zz__regexpz00,
		BgL_bgl_string2611za700za7za7_2653za7, "_pregexp-match-n-positions!", 27);
	      DEFINE_STRING(BGl_string2530z00zz__regexpz00,
		BgL_bgl_string2530za700za7za7_2654za7, "neg-lookahead", 13);
	      DEFINE_STRING(BGl_string2612z00zz__regexpz00,
		BgL_bgl_string2612za700za7za7_2655za7, "vector", 6);
	      DEFINE_STRING(BGl_string2613z00zz__regexpz00,
		BgL_bgl_string2613za700za7za7_2656za7, "bint", 4);
	      DEFINE_STRING(BGl_string2614z00zz__regexpz00,
		BgL_bgl_string2614za700za7za7_2657za7, "_pregexp-match", 14);
	      DEFINE_STRING(BGl_string2533z00zz__regexpz00,
		BgL_bgl_string2533za700za7za7_2658za7, "no-backtrack", 12);
	      DEFINE_STRING(BGl_string2615z00zz__regexpz00,
		BgL_bgl_string2615za700za7za7_2659za7, "&pregexp-split", 14);
	      DEFINE_STRING(BGl_string2616z00zz__regexpz00,
		BgL_bgl_string2616za700za7za7_2660za7, "&pregexp-replace", 16);
	      DEFINE_STRING(BGl_string2617z00zz__regexpz00,
		BgL_bgl_string2617za700za7za7_2661za7, "&pregexp-replace*", 17);
	      DEFINE_STRING(BGl_string2536z00zz__regexpz00,
		BgL_bgl_string2536za700za7za7_2662za7, "lookbehind", 10);
	      DEFINE_STRING(BGl_string2619z00zz__regexpz00,
		BgL_bgl_string2619za700za7za7_2663za7, "&pregexp-quote", 14);
	      DEFINE_STRING(BGl_string2539z00zz__regexpz00,
		BgL_bgl_string2539za700za7za7_2664za7, "neg-lookbehind", 14);
	      DEFINE_STRING(BGl_string2620z00zz__regexpz00,
		BgL_bgl_string2620za700za7za7_2665za7, "__regexp", 8);
	      DEFINE_STRING(BGl_string2541z00zz__regexpz00,
		BgL_bgl_string2541za700za7za7_2666za7, "pregexp-read-subpattern", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2replaceza2zd2envza2zz__regexpz00,
		BgL_bgl_za762pregexpza7d2rep2667z00,
		BGl_z62pregexpzd2replaceza2z12zz__regexpz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2543z00zz__regexpz00,
		BgL_bgl_string2543za700za7za7_2668za7, "at-most", 7);
	extern obj_t BGl_charzd2cizc3zd3zf3zd2envze3zz__r4_characters_6_6z00;
	   
		 
		DEFINE_STRING(BGl_string2545z00zz__regexpz00,
		BgL_bgl_string2545za700za7za7_2669za7, "at-least", 8);
	      DEFINE_STRING(BGl_string2547z00zz__regexpz00,
		BgL_bgl_string2547za700za7za7_2670za7, "minimal?", 8);
	      DEFINE_STRING(BGl_string2549z00zz__regexpz00,
		BgL_bgl_string2549za700za7za7_2671za7, "between", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pregexpzd2matchzd2nzd2positionsz12zd2envz12zz__regexpz00,
		BgL_bgl__pregexpza7d2match2672za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2551z00zz__regexpz00,
		BgL_bgl_string2551za700za7za7_2673za7, "next-i", 6);
	      DEFINE_STRING(BGl_string2552z00zz__regexpz00,
		BgL_bgl_string2552za700za7za7_2674za7,
		"left bracket must be followed by number", 39);
	      DEFINE_STRING(BGl_string2554z00zz__regexpz00,
		BgL_bgl_string2554za700za7za7_2675za7, "pregexp-wrap-quantifier-if-any",
		30);
	      DEFINE_STRING(BGl_string2556z00zz__regexpz00,
		BgL_bgl_string2556za700za7za7_2676za7, "pregexp-read-nums", 17);
	      DEFINE_STRING(BGl_string2557z00zz__regexpz00,
		BgL_bgl_string2557za700za7za7_2677za7, "character class ended too soon",
		30);
	      DEFINE_STRING(BGl_string2476z00zz__regexpz00,
		BgL_bgl_string2476za700za7za7_2678za7,
		"/tmp/bigloo/runtime/Unsafe/pregexp.scm", 38);
	      DEFINE_STRING(BGl_string2477z00zz__regexpz00,
		BgL_bgl_string2477za700za7za7_2679za7, "&regexp-pattern", 15);
	      DEFINE_STRING(BGl_string2559z00zz__regexpz00,
		BgL_bgl_string2559za700za7za7_2680za7, "pregexp-read-char-list", 22);
	      DEFINE_STRING(BGl_string2478z00zz__regexpz00,
		BgL_bgl_string2478za700za7za7_2681za7, "regexp", 6);
	      DEFINE_STRING(BGl_string2479z00zz__regexpz00,
		BgL_bgl_string2479za700za7za7_2682za7, "&regexp-capture-count", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pregexpzd2matchzd2positionszd2envzd2zz__regexpz00,
		BgL_bgl__pregexpza7d2match2683za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2positionsz00zz__regexpz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2561z00zz__regexpz00,
		BgL_bgl_string2561za700za7za7_2684za7, "one-of-chars", 12);
	      DEFINE_STRING(BGl_string2481z00zz__regexpz00,
		BgL_bgl_string2481za700za7za7_2685za7, "seq", 3);
	      DEFINE_STRING(BGl_string2563z00zz__regexpz00,
		BgL_bgl_string2563za700za7za7_2686za7, "char-range", 10);
	      DEFINE_STRING(BGl_string2483z00zz__regexpz00,
		BgL_bgl_string2483za700za7za7_2687za7, "or", 2);
	      DEFINE_STRING(BGl_string2565z00zz__regexpz00,
		BgL_bgl_string2565za700za7za7_2688za7, "alnum", 5);
	      DEFINE_STRING(BGl_string2485z00zz__regexpz00,
		BgL_bgl_string2485za700za7za7_2689za7, "empty", 5);
	      DEFINE_STRING(BGl_string2567z00zz__regexpz00,
		BgL_bgl_string2567za700za7za7_2690za7, "alpha", 5);
	      DEFINE_STRING(BGl_string2487z00zz__regexpz00,
		BgL_bgl_string2487za700za7za7_2691za7, "bos", 3);
	      DEFINE_STRING(BGl_string2569z00zz__regexpz00,
		BgL_bgl_string2569za700za7za7_2692za7, "ascii", 5);
	      DEFINE_STRING(BGl_string2489z00zz__regexpz00,
		BgL_bgl_string2489za700za7za7_2693za7, "eos", 3);
	      DEFINE_STRING(BGl_string2571z00zz__regexpz00,
		BgL_bgl_string2571za700za7za7_2694za7, "blank", 5);
	      DEFINE_STRING(BGl_string2491z00zz__regexpz00,
		BgL_bgl_string2491za700za7za7_2695za7, "any", 3);
	      DEFINE_STRING(BGl_string2573z00zz__regexpz00,
		BgL_bgl_string2573za700za7za7_2696za7, "cntrl", 5);
	      DEFINE_STRING(BGl_string2493z00zz__regexpz00,
		BgL_bgl_string2493za700za7za7_2697za7, "neg-char", 8);
	      DEFINE_STRING(BGl_string2575z00zz__regexpz00,
		BgL_bgl_string2575za700za7za7_2698za7, "graph", 5);
	      DEFINE_STRING(BGl_string2495z00zz__regexpz00,
		BgL_bgl_string2495za700za7za7_2699za7, "backref", 7);
	      DEFINE_STRING(BGl_string2577z00zz__regexpz00,
		BgL_bgl_string2577za700za7za7_2700za7, "lower", 5);
	extern obj_t BGl_charzc3zd3zf3zd2envz31zz__r4_characters_6_6z00;
	   
		 
		DEFINE_STRING(BGl_string2497z00zz__regexpz00,
		BgL_bgl_string2497za700za7za7_2701za7, "backslash", 9);
	      DEFINE_STRING(BGl_string2579z00zz__regexpz00,
		BgL_bgl_string2579za700za7za7_2702za7, "print", 5);
	      DEFINE_STRING(BGl_string2499z00zz__regexpz00,
		BgL_bgl_string2499za700za7za7_2703za7, "pregexp-read-piece", 18);
	      DEFINE_STRING(BGl_string2581z00zz__regexpz00,
		BgL_bgl_string2581za700za7za7_2704za7, "punct", 5);
	      DEFINE_STRING(BGl_string2583z00zz__regexpz00,
		BgL_bgl_string2583za700za7za7_2705za7, "upper", 5);
	      DEFINE_STRING(BGl_string2585z00zz__regexpz00,
		BgL_bgl_string2585za700za7za7_2706za7, "xdigit", 6);
	      DEFINE_STRING(BGl_string2587z00zz__regexpz00,
		BgL_bgl_string2587za700za7za7_2707za7, "pregexp-check-if-in-char-class?",
		31);
	      DEFINE_STRING(BGl_string2591z00zz__regexpz00,
		BgL_bgl_string2591za700za7za7_2708za7, "pregexp-match-positions-aux", 27);
	      DEFINE_STRING(BGl_string2593z00zz__regexpz00,
		BgL_bgl_string2593za700za7za7_2709za7, "non-existent-backref", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2588z00zz__regexpz00,
		BgL_bgl_za762identityza762za7za72710z00, BGl_z62identityz62zz__regexpz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2matchzd2envz00zz__regexpz00,
		BgL_bgl__pregexpza7d2match2711za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2zz__regexpz00, BFALSE, -1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2589z00zz__regexpz00,
		BgL_bgl_za762g1104za762za7za7__r2712z00, BGl_z62g1104z62zz__regexpz00, 0L,
		BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_list2531z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2534z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2618z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2537z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2496z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2498z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2586z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2590z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2592z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2501z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2503z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2505z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2508z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2511z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2596z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2517z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2519z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2521z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2526z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2529z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2532z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2535z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2538z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2548z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2514z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2560z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2480z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2562z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2482z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2564z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2484z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2566z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2486z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2568z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2488z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2600z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2523z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2607z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2609z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2570z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2490z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2572z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2492z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2574z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2494z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2576z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2578z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2507z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2580z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2582z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_keyword2584z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2540z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2542z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2510z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2544z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2546z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2513z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2516z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2550z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2553z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2555z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_symbol2558z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2525z00zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list2528z00zz__regexpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long
		BgL_checksumz00_4498, char *BgL_fromz00_4499)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__regexpz00))
				{
					BGl_requirezd2initializa7ationz75zz__regexpz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__regexpz00();
					BGl_cnstzd2initzd2zz__regexpz00();
					BGl_importedzd2moduleszd2initz00zz__regexpz00();
					return BGl_toplevelzd2initzd2zz__regexpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			BGl_keyword2480z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2481z00zz__regexpz00);
			BGl_keyword2482z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2483z00zz__regexpz00);
			BGl_keyword2484z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2485z00zz__regexpz00);
			BGl_keyword2486z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2487z00zz__regexpz00);
			BGl_keyword2488z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2489z00zz__regexpz00);
			BGl_keyword2490z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2491z00zz__regexpz00);
			BGl_keyword2492z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2493z00zz__regexpz00);
			BGl_keyword2494z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2495z00zz__regexpz00);
			BGl_symbol2496z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2497z00zz__regexpz00);
			BGl_symbol2498z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2499z00zz__regexpz00);
			BGl_keyword2501z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2502z00zz__regexpz00);
			BGl_keyword2503z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2504z00zz__regexpz00);
			BGl_keyword2505z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2506z00zz__regexpz00);
			BGl_list2507z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2492z00zz__regexpz00,
				MAKE_YOUNG_PAIR(BGl_keyword2505z00zz__regexpz00, BNIL));
			BGl_keyword2508z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2509z00zz__regexpz00);
			BGl_list2510z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2492z00zz__regexpz00,
				MAKE_YOUNG_PAIR(BGl_keyword2508z00zz__regexpz00, BNIL));
			BGl_keyword2511z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2512z00zz__regexpz00);
			BGl_list2513z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2492z00zz__regexpz00,
				MAKE_YOUNG_PAIR(BGl_keyword2511z00zz__regexpz00, BNIL));
			BGl_symbol2514z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2515z00zz__regexpz00);
			BGl_keyword2517z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2518z00zz__regexpz00);
			BGl_list2516z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2517z00zz__regexpz00, BNIL);
			BGl_keyword2519z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2520z00zz__regexpz00);
			BGl_keyword2521z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2522z00zz__regexpz00);
			BGl_symbol2523z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2524z00zz__regexpz00);
			BGl_keyword2526z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2527z00zz__regexpz00);
			BGl_list2525z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2526z00zz__regexpz00, BNIL);
			BGl_keyword2529z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2530z00zz__regexpz00);
			BGl_list2528z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2529z00zz__regexpz00, BNIL);
			BGl_keyword2532z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2533z00zz__regexpz00);
			BGl_list2531z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2532z00zz__regexpz00, BNIL);
			BGl_keyword2535z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2536z00zz__regexpz00);
			BGl_list2534z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2535z00zz__regexpz00, BNIL);
			BGl_keyword2538z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2539z00zz__regexpz00);
			BGl_list2537z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2538z00zz__regexpz00, BNIL);
			BGl_symbol2540z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2541z00zz__regexpz00);
			BGl_symbol2542z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2543z00zz__regexpz00);
			BGl_symbol2544z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2545z00zz__regexpz00);
			BGl_symbol2546z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2547z00zz__regexpz00);
			BGl_keyword2548z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2549z00zz__regexpz00);
			BGl_symbol2550z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2551z00zz__regexpz00);
			BGl_symbol2553z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2554z00zz__regexpz00);
			BGl_symbol2555z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2556z00zz__regexpz00);
			BGl_symbol2558z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2559z00zz__regexpz00);
			BGl_keyword2560z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2561z00zz__regexpz00);
			BGl_keyword2562z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2563z00zz__regexpz00);
			BGl_keyword2564z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2565z00zz__regexpz00);
			BGl_keyword2566z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2567z00zz__regexpz00);
			BGl_keyword2568z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2569z00zz__regexpz00);
			BGl_keyword2570z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2571z00zz__regexpz00);
			BGl_keyword2572z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2573z00zz__regexpz00);
			BGl_keyword2574z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2575z00zz__regexpz00);
			BGl_keyword2576z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2577z00zz__regexpz00);
			BGl_keyword2578z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2579z00zz__regexpz00);
			BGl_keyword2580z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2581z00zz__regexpz00);
			BGl_keyword2582z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2583z00zz__regexpz00);
			BGl_keyword2584z00zz__regexpz00 =
				bstring_to_keyword(BGl_string2585z00zz__regexpz00);
			BGl_symbol2586z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2587z00zz__regexpz00);
			BGl_symbol2590z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2591z00zz__regexpz00);
			BGl_symbol2592z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2593z00zz__regexpz00);
			BGl_list2596z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BGl_keyword2548z00zz__regexpz00,
				MAKE_YOUNG_PAIR(BBOOL(((bool_t) 0)),
					MAKE_YOUNG_PAIR(BINT(0L),
						MAKE_YOUNG_PAIR(BBOOL(((bool_t) 0)),
							MAKE_YOUNG_PAIR(BGl_keyword2490z00zz__regexpz00, BNIL)))));
			BGl_symbol2600z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2601z00zz__regexpz00);
			BGl_symbol2607z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2608z00zz__regexpz00);
			BGl_symbol2609z00zz__regexpz00 =
				bstring_to_symbol(BGl_string2610z00zz__regexpz00);
			return (BGl_list2618z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')),
					MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '.')),
						MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '?')),
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '*')),
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '+')),
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '|')),
										MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '^')),
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '$')),
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '[')),
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '{')),
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '}')),
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '(')),
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
																		BNIL)))))))))))))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			BGl_za2pregexpzd2versionza2zd2zz__regexpz00 = 20050502L;
			BGl_za2pregexpzd2commentzd2charza2z00zz__regexpz00 =
				((unsigned char) '#');
			BGl_za2pregexpzd2nulzd2charzd2intza2zd2zz__regexpz00 = 0L;
			BGl_za2pregexpzd2returnzd2charza2z00zz__regexpz00 = ((13L + 0L));
			BGl_za2pregexpzd2tabzd2charza2z00zz__regexpz00 = ((9L + 0L));
			return (BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00 =
				((bool_t) 1), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zz__regexpz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1225;

				BgL_headz00_1225 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2774;
					obj_t BgL_tailz00_2775;

					BgL_prevz00_2774 = BgL_headz00_1225;
					BgL_tailz00_2775 = BgL_l1z00_1;
				BgL_loopz00_2773:
					if (PAIRP(BgL_tailz00_2775))
						{
							obj_t BgL_newzd2prevzd2_2781;

							BgL_newzd2prevzd2_2781 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2775), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2774, BgL_newzd2prevzd2_2781);
							{
								obj_t BgL_tailz00_4620;
								obj_t BgL_prevz00_4619;

								BgL_prevz00_4619 = BgL_newzd2prevzd2_2781;
								BgL_tailz00_4620 = CDR(BgL_tailz00_2775);
								BgL_tailz00_2775 = BgL_tailz00_4620;
								BgL_prevz00_2774 = BgL_prevz00_4619;
								goto BgL_loopz00_2773;
							}
						}
					else
						{
							BNIL;
						}
				}
				return CDR(BgL_headz00_1225);
			}
		}

	}



/* regexp? */
	BGL_EXPORTED_DEF bool_t BGl_regexpzf3zf3zz__regexpz00(obj_t BgL_objz00_3)
	{
		{	/* Unsafe/pregexp.scm 92 */
			return BGL_REGEXPP(BgL_objz00_3);
		}

	}



/* &regexp? */
	obj_t BGl_z62regexpzf3z91zz__regexpz00(obj_t BgL_envz00_4115,
		obj_t BgL_objz00_4116)
	{
		{	/* Unsafe/pregexp.scm 92 */
			return BBOOL(BGl_regexpzf3zf3zz__regexpz00(BgL_objz00_4116));
		}

	}



/* regexp-pattern */
	BGL_EXPORTED_DEF obj_t BGl_regexpzd2patternzd2zz__regexpz00(obj_t BgL_rez00_4)
	{
		{	/* Unsafe/pregexp.scm 98 */
			return BGL_REGEXP_PAT(BgL_rez00_4);
		}

	}



/* &regexp-pattern */
	obj_t BGl_z62regexpzd2patternzb0zz__regexpz00(obj_t BgL_envz00_4117,
		obj_t BgL_rez00_4118)
	{
		{	/* Unsafe/pregexp.scm 98 */
			{	/* Unsafe/pregexp.scm 99 */
				obj_t BgL_auxz00_4627;

				if (BGl_regexpzf3zf3zz__regexpz00(BgL_rez00_4118))
					{	/* Unsafe/pregexp.scm 99 */
						BgL_auxz00_4627 = BgL_rez00_4118;
					}
				else
					{
						obj_t BgL_auxz00_4630;

						BgL_auxz00_4630 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(3961L), BGl_string2477z00zz__regexpz00,
							BGl_string2478z00zz__regexpz00, BgL_rez00_4118);
						FAILURE(BgL_auxz00_4630, BFALSE, BFALSE);
					}
				return BGl_regexpzd2patternzd2zz__regexpz00(BgL_auxz00_4627);
			}
		}

	}



/* regexp-capture-count */
	BGL_EXPORTED_DEF long BGl_regexpzd2capturezd2countz00zz__regexpz00(obj_t
		BgL_rez00_5)
	{
		{	/* Unsafe/pregexp.scm 104 */
			return BGL_REGEXP_CAPTURE_COUNT(BgL_rez00_5);
		}

	}



/* &regexp-capture-count */
	obj_t BGl_z62regexpzd2capturezd2countz62zz__regexpz00(obj_t BgL_envz00_4119,
		obj_t BgL_rez00_4120)
	{
		{	/* Unsafe/pregexp.scm 104 */
			{	/* Unsafe/pregexp.scm 105 */
				long BgL_tmpz00_4636;

				{	/* Unsafe/pregexp.scm 105 */
					obj_t BgL_auxz00_4637;

					if (BGl_regexpzf3zf3zz__regexpz00(BgL_rez00_4120))
						{	/* Unsafe/pregexp.scm 105 */
							BgL_auxz00_4637 = BgL_rez00_4120;
						}
					else
						{
							obj_t BgL_auxz00_4640;

							BgL_auxz00_4640 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
								BINT(4250L), BGl_string2479z00zz__regexpz00,
								BGl_string2478z00zz__regexpz00, BgL_rez00_4120);
							FAILURE(BgL_auxz00_4640, BFALSE, BFALSE);
						}
					BgL_tmpz00_4636 =
						BGl_regexpzd2capturezd2countz00zz__regexpz00(BgL_auxz00_4637);
				}
				return BINT(BgL_tmpz00_4636);
			}
		}

	}



/* pregexp-reverse! */
	obj_t BGl_pregexpzd2reversez12zc0zz__regexpz00(obj_t BgL_sz00_8)
	{
		{	/* Unsafe/pregexp.scm 137 */
			{
				obj_t BgL_sz00_2800;
				obj_t BgL_rz00_2801;

				BgL_sz00_2800 = BgL_sz00_8;
				BgL_rz00_2801 = BNIL;
			BgL_loopz00_2799:
				if (NULLP(BgL_sz00_2800))
					{	/* Unsafe/pregexp.scm 141 */
						return BgL_rz00_2801;
					}
				else
					{	/* Unsafe/pregexp.scm 142 */
						obj_t BgL_dz00_2805;

						BgL_dz00_2805 = CDR(((obj_t) BgL_sz00_2800));
						{	/* Unsafe/pregexp.scm 143 */
							obj_t BgL_tmpz00_4650;

							BgL_tmpz00_4650 = ((obj_t) BgL_sz00_2800);
							SET_CDR(BgL_tmpz00_4650, BgL_rz00_2801);
						}
						{
							obj_t BgL_rz00_4654;
							obj_t BgL_sz00_4653;

							BgL_sz00_4653 = BgL_dz00_2805;
							BgL_rz00_4654 = BgL_sz00_2800;
							BgL_rz00_2801 = BgL_rz00_4654;
							BgL_sz00_2800 = BgL_sz00_4653;
							goto BgL_loopz00_2799;
						}
					}
			}
		}

	}



/* pregexp-read-pattern */
	obj_t BGl_pregexpzd2readzd2patternz00zz__regexpz00(obj_t BgL_sz00_9,
		obj_t BgL_iz00_10, long BgL_nz00_11)
	{
		{	/* Unsafe/pregexp.scm 150 */
			if (((long) CINT(BgL_iz00_10) >= BgL_nz00_11))
				{	/* Unsafe/pregexp.scm 154 */
					obj_t BgL_arg1314z00_1244;

					{	/* Unsafe/pregexp.scm 154 */
						obj_t BgL_arg1317z00_1247;

						{	/* Unsafe/pregexp.scm 154 */
							obj_t BgL_list1320z00_1250;

							BgL_list1320z00_1250 =
								MAKE_YOUNG_PAIR(BGl_keyword2480z00zz__regexpz00, BNIL);
							BgL_arg1317z00_1247 = BgL_list1320z00_1250;
						}
						{	/* Unsafe/pregexp.scm 154 */
							obj_t BgL_list1318z00_1248;

							{	/* Unsafe/pregexp.scm 154 */
								obj_t BgL_arg1319z00_1249;

								BgL_arg1319z00_1249 =
									MAKE_YOUNG_PAIR(BgL_arg1317z00_1247, BNIL);
								BgL_list1318z00_1248 =
									MAKE_YOUNG_PAIR(BGl_keyword2482z00zz__regexpz00,
									BgL_arg1319z00_1249);
							}
							BgL_arg1314z00_1244 = BgL_list1318z00_1248;
					}}
					{	/* Unsafe/pregexp.scm 153 */
						obj_t BgL_list1315z00_1245;

						{	/* Unsafe/pregexp.scm 153 */
							obj_t BgL_arg1316z00_1246;

							BgL_arg1316z00_1246 = MAKE_YOUNG_PAIR(BgL_iz00_10, BNIL);
							BgL_list1315z00_1245 =
								MAKE_YOUNG_PAIR(BgL_arg1314z00_1244, BgL_arg1316z00_1246);
						}
						return BgL_list1315z00_1245;
					}
				}
			else
				{
					obj_t BgL_branchesz00_1253;
					obj_t BgL_iz00_1254;

					BgL_branchesz00_1253 = BNIL;
					BgL_iz00_1254 = BgL_iz00_10;
				BgL_zc3z04anonymousza31321ze3z87_1255:
					{	/* Unsafe/pregexp.scm 156 */
						bool_t BgL_test2719z00_4663;

						if (((long) CINT(BgL_iz00_1254) >= BgL_nz00_11))
							{	/* Unsafe/pregexp.scm 156 */
								BgL_test2719z00_4663 = ((bool_t) 1);
							}
						else
							{	/* Unsafe/pregexp.scm 156 */
								BgL_test2719z00_4663 =
									(STRING_REF(BgL_sz00_9,
										(long) CINT(BgL_iz00_1254)) == ((unsigned char) ')'));
							}
						if (BgL_test2719z00_4663)
							{	/* Unsafe/pregexp.scm 158 */
								obj_t BgL_arg1325z00_1259;

								{	/* Unsafe/pregexp.scm 158 */
									obj_t BgL_arg1328z00_1262;

									BgL_arg1328z00_1262 =
										BGl_pregexpzd2reversez12zc0zz__regexpz00
										(BgL_branchesz00_1253);
									BgL_arg1325z00_1259 =
										MAKE_YOUNG_PAIR(BGl_keyword2482z00zz__regexpz00,
										BgL_arg1328z00_1262);
								}
								{	/* Unsafe/pregexp.scm 158 */
									obj_t BgL_list1326z00_1260;

									{	/* Unsafe/pregexp.scm 158 */
										obj_t BgL_arg1327z00_1261;

										BgL_arg1327z00_1261 = MAKE_YOUNG_PAIR(BgL_iz00_1254, BNIL);
										BgL_list1326z00_1260 =
											MAKE_YOUNG_PAIR(BgL_arg1325z00_1259, BgL_arg1327z00_1261);
									}
									return BgL_list1326z00_1260;
								}
							}
						else
							{	/* Unsafe/pregexp.scm 159 */
								obj_t BgL_vvz00_1263;

								{	/* Unsafe/pregexp.scm 161 */
									obj_t BgL_arg1333z00_1267;

									if (
										(STRING_REF(BgL_sz00_9,
												(long) CINT(BgL_iz00_1254)) == ((unsigned char) '|')))
										{	/* Unsafe/pregexp.scm 161 */
											BgL_arg1333z00_1267 = ADDFX(BgL_iz00_1254, BINT(1L));
										}
									else
										{	/* Unsafe/pregexp.scm 161 */
											BgL_arg1333z00_1267 = BgL_iz00_1254;
										}
									BgL_vvz00_1263 =
										BGl_pregexpzd2readzd2branchz00zz__regexpz00(BgL_sz00_9,
										BgL_arg1333z00_1267, BgL_nz00_11);
								}
								{	/* Unsafe/pregexp.scm 162 */
									obj_t BgL_arg1329z00_1264;
									obj_t BgL_arg1331z00_1265;

									{	/* Unsafe/pregexp.scm 162 */
										obj_t BgL_arg1332z00_1266;

										BgL_arg1332z00_1266 = CAR(((obj_t) BgL_vvz00_1263));
										BgL_arg1329z00_1264 =
											MAKE_YOUNG_PAIR(BgL_arg1332z00_1266,
											BgL_branchesz00_1253);
									}
									{	/* Unsafe/pregexp.scm 162 */
										obj_t BgL_pairz00_2829;

										BgL_pairz00_2829 = CDR(((obj_t) BgL_vvz00_1263));
										BgL_arg1331z00_1265 = CAR(BgL_pairz00_2829);
									}
									{
										obj_t BgL_iz00_4688;
										obj_t BgL_branchesz00_4687;

										BgL_branchesz00_4687 = BgL_arg1329z00_1264;
										BgL_iz00_4688 = BgL_arg1331z00_1265;
										BgL_iz00_1254 = BgL_iz00_4688;
										BgL_branchesz00_1253 = BgL_branchesz00_4687;
										goto BgL_zc3z04anonymousza31321ze3z87_1255;
									}
								}
							}
					}
				}
		}

	}



/* pregexp-read-branch */
	obj_t BGl_pregexpzd2readzd2branchz00zz__regexpz00(obj_t BgL_sz00_12,
		obj_t BgL_iz00_13, long BgL_nz00_14)
	{
		{	/* Unsafe/pregexp.scm 164 */
			{
				obj_t BgL_piecesz00_1276;
				obj_t BgL_iz00_1277;

				BgL_piecesz00_1276 = BNIL;
				BgL_iz00_1277 = BgL_iz00_13;
			BgL_zc3z04anonymousza31338ze3z87_1278:
				if (((long) CINT(BgL_iz00_1277) >= BgL_nz00_14))
					{	/* Unsafe/pregexp.scm 168 */
						obj_t BgL_arg1340z00_1280;

						{	/* Unsafe/pregexp.scm 168 */
							obj_t BgL_arg1343z00_1283;

							BgL_arg1343z00_1283 =
								BGl_pregexpzd2reversez12zc0zz__regexpz00(BgL_piecesz00_1276);
							BgL_arg1340z00_1280 =
								MAKE_YOUNG_PAIR(BGl_keyword2480z00zz__regexpz00,
								BgL_arg1343z00_1283);
						}
						{	/* Unsafe/pregexp.scm 168 */
							obj_t BgL_list1341z00_1281;

							{	/* Unsafe/pregexp.scm 168 */
								obj_t BgL_arg1342z00_1282;

								BgL_arg1342z00_1282 = MAKE_YOUNG_PAIR(BgL_iz00_1277, BNIL);
								BgL_list1341z00_1281 =
									MAKE_YOUNG_PAIR(BgL_arg1340z00_1280, BgL_arg1342z00_1282);
							}
							return BgL_list1341z00_1281;
						}
					}
				else
					{	/* Unsafe/pregexp.scm 169 */
						bool_t BgL_test2723z00_4696;

						{	/* Unsafe/pregexp.scm 169 */
							unsigned char BgL_cz00_1295;

							BgL_cz00_1295 =
								STRING_REF(BgL_sz00_12, (long) CINT(BgL_iz00_1277));
							{	/* Unsafe/pregexp.scm 170 */
								bool_t BgL__ortest_1042z00_1296;

								BgL__ortest_1042z00_1296 =
									(BgL_cz00_1295 == ((unsigned char) '|'));
								if (BgL__ortest_1042z00_1296)
									{	/* Unsafe/pregexp.scm 170 */
										BgL_test2723z00_4696 = BgL__ortest_1042z00_1296;
									}
								else
									{	/* Unsafe/pregexp.scm 170 */
										BgL_test2723z00_4696 =
											(BgL_cz00_1295 == ((unsigned char) ')'));
						}}}
						if (BgL_test2723z00_4696)
							{	/* Unsafe/pregexp.scm 172 */
								obj_t BgL_arg1346z00_1287;

								{	/* Unsafe/pregexp.scm 172 */
									obj_t BgL_arg1349z00_1290;

									BgL_arg1349z00_1290 =
										BGl_pregexpzd2reversez12zc0zz__regexpz00
										(BgL_piecesz00_1276);
									BgL_arg1346z00_1287 =
										MAKE_YOUNG_PAIR(BGl_keyword2480z00zz__regexpz00,
										BgL_arg1349z00_1290);
								}
								{	/* Unsafe/pregexp.scm 172 */
									obj_t BgL_list1347z00_1288;

									{	/* Unsafe/pregexp.scm 172 */
										obj_t BgL_arg1348z00_1289;

										BgL_arg1348z00_1289 = MAKE_YOUNG_PAIR(BgL_iz00_1277, BNIL);
										BgL_list1347z00_1288 =
											MAKE_YOUNG_PAIR(BgL_arg1346z00_1287, BgL_arg1348z00_1289);
									}
									return BgL_list1347z00_1288;
								}
							}
						else
							{	/* Unsafe/pregexp.scm 173 */
								obj_t BgL_vvz00_1291;

								BgL_vvz00_1291 =
									BGl_pregexpzd2readzd2piecez00zz__regexpz00(BgL_sz00_12,
									BgL_iz00_1277, BgL_nz00_14);
								{	/* Unsafe/pregexp.scm 174 */
									obj_t BgL_arg1350z00_1292;
									obj_t BgL_arg1351z00_1293;

									{	/* Unsafe/pregexp.scm 174 */
										obj_t BgL_arg1352z00_1294;

										BgL_arg1352z00_1294 = CAR(((obj_t) BgL_vvz00_1291));
										BgL_arg1350z00_1292 =
											MAKE_YOUNG_PAIR(BgL_arg1352z00_1294, BgL_piecesz00_1276);
									}
									{	/* Unsafe/pregexp.scm 174 */
										obj_t BgL_pairz00_2844;

										BgL_pairz00_2844 = CDR(((obj_t) BgL_vvz00_1291));
										BgL_arg1351z00_1293 = CAR(BgL_pairz00_2844);
									}
									{
										obj_t BgL_iz00_4714;
										obj_t BgL_piecesz00_4713;

										BgL_piecesz00_4713 = BgL_arg1350z00_1292;
										BgL_iz00_4714 = BgL_arg1351z00_1293;
										BgL_iz00_1277 = BgL_iz00_4714;
										BgL_piecesz00_1276 = BgL_piecesz00_4713;
										goto BgL_zc3z04anonymousza31338ze3z87_1278;
									}
								}
							}
					}
			}
		}

	}



/* pregexp-read-piece */
	obj_t BGl_pregexpzd2readzd2piecez00zz__regexpz00(obj_t BgL_sz00_15,
		obj_t BgL_iz00_16, long BgL_nz00_17)
	{
		{	/* Unsafe/pregexp.scm 176 */
			{	/* Unsafe/pregexp.scm 178 */
				unsigned char BgL_cz00_1298;

				BgL_cz00_1298 = STRING_REF(BgL_sz00_15, (long) CINT(BgL_iz00_16));
				{

					switch (BgL_cz00_1298)
						{
						case ((unsigned char) '^'):

							{	/* Unsafe/pregexp.scm 180 */
								long BgL_arg1354z00_1302;

								BgL_arg1354z00_1302 = ((long) CINT(BgL_iz00_16) + 1L);
								{	/* Unsafe/pregexp.scm 180 */
									obj_t BgL_list1355z00_1303;

									{	/* Unsafe/pregexp.scm 180 */
										obj_t BgL_arg1356z00_1304;

										BgL_arg1356z00_1304 =
											MAKE_YOUNG_PAIR(BINT(BgL_arg1354z00_1302), BNIL);
										BgL_list1355z00_1303 =
											MAKE_YOUNG_PAIR(BGl_keyword2486z00zz__regexpz00,
											BgL_arg1356z00_1304);
									}
									return BgL_list1355z00_1303;
								}
							}
							break;
						case ((unsigned char) '$'):

							{	/* Unsafe/pregexp.scm 181 */
								long BgL_arg1357z00_1305;

								BgL_arg1357z00_1305 = ((long) CINT(BgL_iz00_16) + 1L);
								{	/* Unsafe/pregexp.scm 181 */
									obj_t BgL_list1358z00_1306;

									{	/* Unsafe/pregexp.scm 181 */
										obj_t BgL_arg1359z00_1307;

										BgL_arg1359z00_1307 =
											MAKE_YOUNG_PAIR(BINT(BgL_arg1357z00_1305), BNIL);
										BgL_list1358z00_1306 =
											MAKE_YOUNG_PAIR(BGl_keyword2488z00zz__regexpz00,
											BgL_arg1359z00_1307);
									}
									return BgL_list1358z00_1306;
								}
							}
							break;
						case ((unsigned char) '.'):

							{	/* Unsafe/pregexp.scm 183 */
								obj_t BgL_arg1360z00_1308;

								{	/* Unsafe/pregexp.scm 183 */
									long BgL_arg1361z00_1309;

									BgL_arg1361z00_1309 = ((long) CINT(BgL_iz00_16) + 1L);
									{	/* Unsafe/pregexp.scm 183 */
										obj_t BgL_list1362z00_1310;

										{	/* Unsafe/pregexp.scm 183 */
											obj_t BgL_arg1363z00_1311;

											BgL_arg1363z00_1311 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1361z00_1309), BNIL);
											BgL_list1362z00_1310 =
												MAKE_YOUNG_PAIR(BGl_keyword2490z00zz__regexpz00,
												BgL_arg1363z00_1311);
										}
										BgL_arg1360z00_1308 = BgL_list1362z00_1310;
								}}
								return
									BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00
									(BgL_arg1360z00_1308, BgL_sz00_15, BgL_nz00_17);
							}
							break;
						case ((unsigned char) '['):

							{	/* Unsafe/pregexp.scm 184 */
								long BgL_izb21zb2_1312;

								BgL_izb21zb2_1312 = ((long) CINT(BgL_iz00_16) + 1L);
								{	/* Unsafe/pregexp.scm 186 */
									obj_t BgL_arg1364z00_1313;

									{	/* Unsafe/pregexp.scm 186 */
										obj_t BgL_aux1047z00_1315;

										if ((BgL_izb21zb2_1312 < BgL_nz00_17))
											{	/* Unsafe/pregexp.scm 186 */
												BgL_aux1047z00_1315 =
													BCHAR(STRING_REF(BgL_sz00_15, BgL_izb21zb2_1312));
											}
										else
											{	/* Unsafe/pregexp.scm 186 */
												BgL_aux1047z00_1315 = BFALSE;
											}
										if (CHARP(BgL_aux1047z00_1315))
											{	/* Unsafe/pregexp.scm 186 */
												switch (CCHAR(BgL_aux1047z00_1315))
													{
													case ((unsigned char) '^'):

														{	/* Unsafe/pregexp.scm 188 */
															obj_t BgL_vvz00_1317;

															BgL_vvz00_1317 =
																BGl_pregexpzd2readzd2charzd2listzd2zz__regexpz00
																(BgL_sz00_15, ((long) CINT(BgL_iz00_16) + 2L),
																BgL_nz00_17);
															{	/* Unsafe/pregexp.scm 189 */
																obj_t BgL_arg1366z00_1318;
																obj_t BgL_arg1367z00_1319;

																{	/* Unsafe/pregexp.scm 189 */
																	obj_t BgL_arg1370z00_1322;

																	BgL_arg1370z00_1322 =
																		CAR(((obj_t) BgL_vvz00_1317));
																	{	/* Unsafe/pregexp.scm 189 */
																		obj_t BgL_list1371z00_1323;

																		{	/* Unsafe/pregexp.scm 189 */
																			obj_t BgL_arg1372z00_1324;

																			BgL_arg1372z00_1324 =
																				MAKE_YOUNG_PAIR(BgL_arg1370z00_1322,
																				BNIL);
																			BgL_list1371z00_1323 =
																				MAKE_YOUNG_PAIR
																				(BGl_keyword2492z00zz__regexpz00,
																				BgL_arg1372z00_1324);
																		}
																		BgL_arg1366z00_1318 = BgL_list1371z00_1323;
																}}
																{	/* Unsafe/pregexp.scm 189 */
																	obj_t BgL_pairz00_2883;

																	BgL_pairz00_2883 =
																		CDR(((obj_t) BgL_vvz00_1317));
																	BgL_arg1367z00_1319 = CAR(BgL_pairz00_2883);
																}
																{	/* Unsafe/pregexp.scm 189 */
																	obj_t BgL_list1368z00_1320;

																	{	/* Unsafe/pregexp.scm 189 */
																		obj_t BgL_arg1369z00_1321;

																		BgL_arg1369z00_1321 =
																			MAKE_YOUNG_PAIR(BgL_arg1367z00_1319,
																			BNIL);
																		BgL_list1368z00_1320 =
																			MAKE_YOUNG_PAIR(BgL_arg1366z00_1318,
																			BgL_arg1369z00_1321);
																	}
																	BgL_arg1364z00_1313 = BgL_list1368z00_1320;
														}}} break;
													default:
														BgL_arg1364z00_1313 =
															BGl_pregexpzd2readzd2charzd2listzd2zz__regexpz00
															(BgL_sz00_15, BgL_izb21zb2_1312, BgL_nz00_17);
													}
											}
										else
											{	/* Unsafe/pregexp.scm 186 */
												BgL_arg1364z00_1313 =
													BGl_pregexpzd2readzd2charzd2listzd2zz__regexpz00
													(BgL_sz00_15, BgL_izb21zb2_1312, BgL_nz00_17);
											}
									}
									return
										BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00
										(BgL_arg1364z00_1313, BgL_sz00_15, BgL_nz00_17);
								}
							}
							break;
						case ((unsigned char) '('):

							return
								BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00
								(BGl_pregexpzd2readzd2subpatternz00zz__regexpz00(BgL_sz00_15,
									((long) CINT(BgL_iz00_16) + 1L), BgL_nz00_17), BgL_sz00_15,
								BgL_nz00_17);
							break;
						case ((unsigned char) '\\'):

							{	/* Unsafe/pregexp.scm 197 */
								obj_t BgL_arg1377z00_1329;

								{	/* Unsafe/pregexp.scm 197 */
									obj_t BgL_g1049z00_1330;

									BgL_g1049z00_1330 =
										BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00
										(BgL_sz00_15, BgL_iz00_16, BgL_nz00_17);
									if (CBOOL(BgL_g1049z00_1330))
										{	/* Unsafe/pregexp.scm 199 */
											obj_t BgL_arg1378z00_1333;
											obj_t BgL_arg1379z00_1334;

											{	/* Unsafe/pregexp.scm 199 */
												obj_t BgL_arg1383z00_1337;

												BgL_arg1383z00_1337 = CAR(((obj_t) BgL_g1049z00_1330));
												{	/* Unsafe/pregexp.scm 199 */
													obj_t BgL_list1384z00_1338;

													{	/* Unsafe/pregexp.scm 199 */
														obj_t BgL_arg1387z00_1339;

														BgL_arg1387z00_1339 =
															MAKE_YOUNG_PAIR(BgL_arg1383z00_1337, BNIL);
														BgL_list1384z00_1338 =
															MAKE_YOUNG_PAIR(BGl_keyword2494z00zz__regexpz00,
															BgL_arg1387z00_1339);
													}
													BgL_arg1378z00_1333 = BgL_list1384z00_1338;
												}
											}
											{	/* Unsafe/pregexp.scm 199 */
												obj_t BgL_pairz00_2891;

												BgL_pairz00_2891 = CDR(((obj_t) BgL_g1049z00_1330));
												BgL_arg1379z00_1334 = CAR(BgL_pairz00_2891);
											}
											{	/* Unsafe/pregexp.scm 199 */
												obj_t BgL_list1380z00_1335;

												{	/* Unsafe/pregexp.scm 199 */
													obj_t BgL_arg1382z00_1336;

													BgL_arg1382z00_1336 =
														MAKE_YOUNG_PAIR(BgL_arg1379z00_1334, BNIL);
													BgL_list1380z00_1335 =
														MAKE_YOUNG_PAIR(BgL_arg1378z00_1333,
														BgL_arg1382z00_1336);
												}
												BgL_arg1377z00_1329 = BgL_list1380z00_1335;
											}
										}
									else
										{	/* Unsafe/pregexp.scm 200 */
											obj_t BgL_g1051z00_1340;

											BgL_g1051z00_1340 =
												BGl_pregexpzd2readzd2escapedzd2charzd2zz__regexpz00
												(BgL_sz00_15, BgL_iz00_16, BgL_nz00_17);
											if (CBOOL(BgL_g1051z00_1340))
												{	/* Unsafe/pregexp.scm 202 */
													obj_t BgL_arg1388z00_1343;
													obj_t BgL_arg1389z00_1344;

													BgL_arg1388z00_1343 =
														CAR(((obj_t) BgL_g1051z00_1340));
													{	/* Unsafe/pregexp.scm 202 */
														obj_t BgL_pairz00_2897;

														BgL_pairz00_2897 = CDR(((obj_t) BgL_g1051z00_1340));
														BgL_arg1389z00_1344 = CAR(BgL_pairz00_2897);
													}
													{	/* Unsafe/pregexp.scm 202 */
														obj_t BgL_list1390z00_1345;

														{	/* Unsafe/pregexp.scm 202 */
															obj_t BgL_arg1391z00_1346;

															BgL_arg1391z00_1346 =
																MAKE_YOUNG_PAIR(BgL_arg1389z00_1344, BNIL);
															BgL_list1390z00_1345 =
																MAKE_YOUNG_PAIR(BgL_arg1388z00_1343,
																BgL_arg1391z00_1346);
														}
														BgL_arg1377z00_1329 = BgL_list1390z00_1345;
													}
												}
											else
												{	/* Unsafe/pregexp.scm 203 */
													obj_t BgL_list1392z00_1347;

													BgL_list1392z00_1347 =
														MAKE_YOUNG_PAIR(BGl_symbol2496z00zz__regexpz00,
														BNIL);
													BgL_arg1377z00_1329 =
														BGl_errorz00zz__errorz00
														(BGl_string2500z00zz__regexpz00,
														BGl_symbol2498z00zz__regexpz00,
														CAR(BgL_list1392z00_1347));
												}
										}
								}
								return
									BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00
									(BgL_arg1377z00_1329, BgL_sz00_15, BgL_nz00_17);
							}
							break;
						default:
							{	/* Unsafe/pregexp.scm 206 */
								bool_t BgL_test2729z00_4788;

								if (BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00)
									{	/* Unsafe/pregexp.scm 206 */
										BgL_test2729z00_4788 = ((bool_t) 1);
									}
								else
									{	/* Unsafe/pregexp.scm 206 */
										if (isspace(BgL_cz00_1298))
											{	/* Unsafe/pregexp.scm 207 */
												BgL_test2729z00_4788 = ((bool_t) 0);
											}
										else
											{	/* Unsafe/pregexp.scm 207 */
												if ((BgL_cz00_1298 == ((unsigned char) '#')))
													{	/* Unsafe/pregexp.scm 208 */
														BgL_test2729z00_4788 = ((bool_t) 0);
													}
												else
													{	/* Unsafe/pregexp.scm 208 */
														BgL_test2729z00_4788 = ((bool_t) 1);
													}
											}
									}
								if (BgL_test2729z00_4788)
									{	/* Unsafe/pregexp.scm 210 */
										obj_t BgL_arg1395z00_1351;

										{	/* Unsafe/pregexp.scm 210 */
											long BgL_arg1396z00_1352;

											BgL_arg1396z00_1352 = ((long) CINT(BgL_iz00_16) + 1L);
											{	/* Unsafe/pregexp.scm 210 */
												obj_t BgL_list1397z00_1353;

												{	/* Unsafe/pregexp.scm 210 */
													obj_t BgL_arg1399z00_1354;

													BgL_arg1399z00_1354 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1396z00_1352), BNIL);
													BgL_list1397z00_1353 =
														MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1298),
														BgL_arg1399z00_1354);
												}
												BgL_arg1395z00_1351 = BgL_list1397z00_1353;
										}}
										return
											BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00
											(BgL_arg1395z00_1351, BgL_sz00_15, BgL_nz00_17);
									}
								else
									{
										obj_t BgL_iz00_1356;
										bool_t BgL_inzd2commentzf3z21_1357;

										BgL_iz00_1356 = BgL_iz00_16;
										BgL_inzd2commentzf3z21_1357 = ((bool_t) 0);
									BgL_zc3z04anonymousza31400ze3z87_1358:
										if (((long) CINT(BgL_iz00_1356) >= BgL_nz00_17))
											{	/* Unsafe/pregexp.scm 212 */
												obj_t BgL_list1402z00_1360;

												{	/* Unsafe/pregexp.scm 212 */
													obj_t BgL_arg1403z00_1361;

													BgL_arg1403z00_1361 =
														MAKE_YOUNG_PAIR(BgL_iz00_1356, BNIL);
													BgL_list1402z00_1360 =
														MAKE_YOUNG_PAIR(BGl_keyword2484z00zz__regexpz00,
														BgL_arg1403z00_1361);
												}
												return BgL_list1402z00_1360;
											}
										else
											{	/* Unsafe/pregexp.scm 213 */
												unsigned char BgL_cz00_1362;

												BgL_cz00_1362 =
													STRING_REF(BgL_sz00_15, (long) CINT(BgL_iz00_1356));
												if (BgL_inzd2commentzf3z21_1357)
													{	/* Unsafe/pregexp.scm 215 */
														long BgL_arg1404z00_1363;
														bool_t BgL_arg1405z00_1364;

														BgL_arg1404z00_1363 =
															((long) CINT(BgL_iz00_1356) + 1L);
														if ((BgL_cz00_1362 == ((unsigned char) 10)))
															{	/* Unsafe/pregexp.scm 216 */
																BgL_arg1405z00_1364 = ((bool_t) 0);
															}
														else
															{	/* Unsafe/pregexp.scm 216 */
																BgL_arg1405z00_1364 = ((bool_t) 1);
															}
														{
															bool_t BgL_inzd2commentzf3z21_4815;
															obj_t BgL_iz00_4813;

															BgL_iz00_4813 = BINT(BgL_arg1404z00_1363);
															BgL_inzd2commentzf3z21_4815 = BgL_arg1405z00_1364;
															BgL_inzd2commentzf3z21_1357 =
																BgL_inzd2commentzf3z21_4815;
															BgL_iz00_1356 = BgL_iz00_4813;
															goto BgL_zc3z04anonymousza31400ze3z87_1358;
														}
													}
												else
													{	/* Unsafe/pregexp.scm 214 */
														if (isspace(BgL_cz00_1362))
															{	/* Unsafe/pregexp.scm 218 */
																long BgL_arg1408z00_1367;

																BgL_arg1408z00_1367 =
																	((long) CINT(BgL_iz00_1356) + 1L);
																{
																	bool_t BgL_inzd2commentzf3z21_4822;
																	obj_t BgL_iz00_4820;

																	BgL_iz00_4820 = BINT(BgL_arg1408z00_1367);
																	BgL_inzd2commentzf3z21_4822 = ((bool_t) 0);
																	BgL_inzd2commentzf3z21_1357 =
																		BgL_inzd2commentzf3z21_4822;
																	BgL_iz00_1356 = BgL_iz00_4820;
																	goto BgL_zc3z04anonymousza31400ze3z87_1358;
																}
															}
														else
															{	/* Unsafe/pregexp.scm 217 */
																if ((BgL_cz00_1362 == ((unsigned char) '#')))
																	{	/* Unsafe/pregexp.scm 220 */
																		long BgL_arg1410z00_1369;

																		BgL_arg1410z00_1369 =
																			((long) CINT(BgL_iz00_1356) + 1L);
																		{
																			bool_t BgL_inzd2commentzf3z21_4829;
																			obj_t BgL_iz00_4827;

																			BgL_iz00_4827 = BINT(BgL_arg1410z00_1369);
																			BgL_inzd2commentzf3z21_4829 =
																				((bool_t) 1);
																			BgL_inzd2commentzf3z21_1357 =
																				BgL_inzd2commentzf3z21_4829;
																			BgL_iz00_1356 = BgL_iz00_4827;
																			goto
																				BgL_zc3z04anonymousza31400ze3z87_1358;
																		}
																	}
																else
																	{	/* Unsafe/pregexp.scm 221 */
																		obj_t BgL_list1411z00_1370;

																		{	/* Unsafe/pregexp.scm 221 */
																			obj_t BgL_arg1412z00_1371;

																			BgL_arg1412z00_1371 =
																				MAKE_YOUNG_PAIR(BgL_iz00_1356, BNIL);
																			BgL_list1411z00_1370 =
																				MAKE_YOUNG_PAIR
																				(BGl_keyword2484z00zz__regexpz00,
																				BgL_arg1412z00_1371);
																		}
																		return BgL_list1411z00_1370;
																	}
															}
													}
											}
									}
							}
						}
				}
			}
		}

	}



/* pregexp-read-escaped-number */
	obj_t BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00(obj_t BgL_sz00_18,
		obj_t BgL_iz00_19, long BgL_nz00_20)
	{
		{	/* Unsafe/pregexp.scm 223 */
			if ((((long) CINT(BgL_iz00_19) + 1L) < BgL_nz00_20))
				{	/* Unsafe/pregexp.scm 227 */
					unsigned char BgL_cz00_1376;

					BgL_cz00_1376 =
						STRING_REF(BgL_sz00_18, ((long) CINT(BgL_iz00_19) + 1L));
					if (isdigit(BgL_cz00_1376))
						{	/* Unsafe/pregexp.scm 229 */
							long BgL_g1055z00_1378;
							obj_t BgL_g1056z00_1379;

							BgL_g1055z00_1378 = ((long) CINT(BgL_iz00_19) + 2L);
							{	/* Unsafe/pregexp.scm 229 */
								obj_t BgL_list1429z00_1404;

								BgL_list1429z00_1404 =
									MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1376), BNIL);
								BgL_g1056z00_1379 = BgL_list1429z00_1404;
							}
							{
								long BgL_iz00_1381;
								obj_t BgL_rz00_1382;

								BgL_iz00_1381 = BgL_g1055z00_1378;
								BgL_rz00_1382 = BgL_g1056z00_1379;
							BgL_zc3z04anonymousza31413ze3z87_1383:
								if ((BgL_iz00_1381 >= BgL_nz00_20))
									{	/* Unsafe/pregexp.scm 232 */
										obj_t BgL_arg1415z00_1385;

										{	/* Unsafe/pregexp.scm 232 */
											obj_t BgL_arg1418z00_1388;

											BgL_arg1418z00_1388 =
												BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
												(BGl_pregexpzd2reversez12zc0zz__regexpz00
												(BgL_rz00_1382));
											{	/* Ieee/number.scm 175 */

												BgL_arg1415z00_1385 =
													BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
													(BgL_arg1418z00_1388, BINT(10L));
											}
										}
										{	/* Unsafe/pregexp.scm 231 */
											obj_t BgL_list1416z00_1386;

											{	/* Unsafe/pregexp.scm 231 */
												obj_t BgL_arg1417z00_1387;

												BgL_arg1417z00_1387 =
													MAKE_YOUNG_PAIR(BINT(BgL_iz00_1381), BNIL);
												BgL_list1416z00_1386 =
													MAKE_YOUNG_PAIR(BgL_arg1415z00_1385,
													BgL_arg1417z00_1387);
											}
											return BgL_list1416z00_1386;
										}
									}
								else
									{	/* Unsafe/pregexp.scm 233 */
										unsigned char BgL_cz00_1392;

										BgL_cz00_1392 = STRING_REF(BgL_sz00_18, BgL_iz00_1381);
										if (isdigit(BgL_cz00_1392))
											{	/* Unsafe/pregexp.scm 235 */
												long BgL_arg1422z00_1394;
												obj_t BgL_arg1423z00_1395;

												BgL_arg1422z00_1394 = (BgL_iz00_1381 + 1L);
												BgL_arg1423z00_1395 =
													MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1392), BgL_rz00_1382);
												{
													obj_t BgL_rz00_4862;
													long BgL_iz00_4861;

													BgL_iz00_4861 = BgL_arg1422z00_1394;
													BgL_rz00_4862 = BgL_arg1423z00_1395;
													BgL_rz00_1382 = BgL_rz00_4862;
													BgL_iz00_1381 = BgL_iz00_4861;
													goto BgL_zc3z04anonymousza31413ze3z87_1383;
												}
											}
										else
											{	/* Unsafe/pregexp.scm 237 */
												obj_t BgL_arg1424z00_1396;

												{	/* Unsafe/pregexp.scm 237 */
													obj_t BgL_arg1427z00_1399;

													BgL_arg1427z00_1399 =
														BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
														(BGl_pregexpzd2reversez12zc0zz__regexpz00
														(BgL_rz00_1382));
													{	/* Ieee/number.scm 175 */

														BgL_arg1424z00_1396 =
															BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
															(BgL_arg1427z00_1399, BINT(10L));
													}
												}
												{	/* Unsafe/pregexp.scm 236 */
													obj_t BgL_list1425z00_1397;

													{	/* Unsafe/pregexp.scm 236 */
														obj_t BgL_arg1426z00_1398;

														BgL_arg1426z00_1398 =
															MAKE_YOUNG_PAIR(BINT(BgL_iz00_1381), BNIL);
														BgL_list1425z00_1397 =
															MAKE_YOUNG_PAIR(BgL_arg1424z00_1396,
															BgL_arg1426z00_1398);
													}
													return BgL_list1425z00_1397;
												}
											}
									}
							}
						}
					else
						{	/* Unsafe/pregexp.scm 228 */
							return BFALSE;
						}
				}
			else
				{	/* Unsafe/pregexp.scm 226 */
					return BFALSE;
				}
		}

	}



/* pregexp-read-escaped-char */
	obj_t BGl_pregexpzd2readzd2escapedzd2charzd2zz__regexpz00(obj_t BgL_sz00_21,
		obj_t BgL_iz00_22, long BgL_nz00_23)
	{
		{	/* Unsafe/pregexp.scm 240 */
			if ((((long) CINT(BgL_iz00_22) + 1L) < BgL_nz00_23))
				{	/* Unsafe/pregexp.scm 244 */
					unsigned char BgL_cz00_1408;

					BgL_cz00_1408 =
						STRING_REF(BgL_sz00_21, ((long) CINT(BgL_iz00_22) + 1L));
					{

						switch (BgL_cz00_1408)
							{
							case ((unsigned char) 'b'):

								{	/* Unsafe/pregexp.scm 246 */
									long BgL_arg1434z00_1412;

									BgL_arg1434z00_1412 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 246 */
										obj_t BgL_list1435z00_1413;

										{	/* Unsafe/pregexp.scm 246 */
											obj_t BgL_arg1436z00_1414;

											BgL_arg1436z00_1414 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1434z00_1412), BNIL);
											BgL_list1435z00_1413 =
												MAKE_YOUNG_PAIR(BGl_keyword2501z00zz__regexpz00,
												BgL_arg1436z00_1414);
										}
										return BgL_list1435z00_1413;
									}
								}
								break;
							case ((unsigned char) 'B'):

								{	/* Unsafe/pregexp.scm 247 */
									long BgL_arg1437z00_1415;

									BgL_arg1437z00_1415 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 247 */
										obj_t BgL_list1438z00_1416;

										{	/* Unsafe/pregexp.scm 247 */
											obj_t BgL_arg1439z00_1417;

											BgL_arg1439z00_1417 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1437z00_1415), BNIL);
											BgL_list1438z00_1416 =
												MAKE_YOUNG_PAIR(BGl_keyword2503z00zz__regexpz00,
												BgL_arg1439z00_1417);
										}
										return BgL_list1438z00_1416;
									}
								}
								break;
							case ((unsigned char) 'd'):

								{	/* Unsafe/pregexp.scm 248 */
									long BgL_arg1440z00_1418;

									BgL_arg1440z00_1418 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 248 */
										obj_t BgL_list1441z00_1419;

										{	/* Unsafe/pregexp.scm 248 */
											obj_t BgL_arg1442z00_1420;

											BgL_arg1442z00_1420 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1440z00_1418), BNIL);
											BgL_list1441z00_1419 =
												MAKE_YOUNG_PAIR(BGl_keyword2505z00zz__regexpz00,
												BgL_arg1442z00_1420);
										}
										return BgL_list1441z00_1419;
									}
								}
								break;
							case ((unsigned char) 'D'):

								{	/* Unsafe/pregexp.scm 249 */
									long BgL_arg1443z00_1421;

									BgL_arg1443z00_1421 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 249 */
										obj_t BgL_list1444z00_1422;

										{	/* Unsafe/pregexp.scm 249 */
											obj_t BgL_arg1445z00_1423;

											BgL_arg1445z00_1423 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1443z00_1421), BNIL);
											BgL_list1444z00_1422 =
												MAKE_YOUNG_PAIR(BGl_list2507z00zz__regexpz00,
												BgL_arg1445z00_1423);
										}
										return BgL_list1444z00_1422;
									}
								}
								break;
							case ((unsigned char) 'n'):

								{	/* Unsafe/pregexp.scm 250 */
									long BgL_arg1446z00_1424;

									BgL_arg1446z00_1424 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 250 */
										obj_t BgL_list1447z00_1425;

										{	/* Unsafe/pregexp.scm 250 */
											obj_t BgL_arg1448z00_1426;

											BgL_arg1448z00_1426 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1446z00_1424), BNIL);
											BgL_list1447z00_1425 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
												BgL_arg1448z00_1426);
										}
										return BgL_list1447z00_1425;
									}
								}
								break;
							case ((unsigned char) 'r'):

								{	/* Unsafe/pregexp.scm 251 */
									long BgL_arg1449z00_1427;

									BgL_arg1449z00_1427 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 251 */
										obj_t BgL_list1450z00_1428;

										{	/* Unsafe/pregexp.scm 251 */
											obj_t BgL_arg1451z00_1429;

											BgL_arg1451z00_1429 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1449z00_1427), BNIL);
											BgL_list1450z00_1428 =
												MAKE_YOUNG_PAIR(BCHAR
												(BGl_za2pregexpzd2returnzd2charza2z00zz__regexpz00),
												BgL_arg1451z00_1429);
										}
										return BgL_list1450z00_1428;
									}
								}
								break;
							case ((unsigned char) 's'):

								{	/* Unsafe/pregexp.scm 252 */
									long BgL_arg1452z00_1430;

									BgL_arg1452z00_1430 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 252 */
										obj_t BgL_list1453z00_1431;

										{	/* Unsafe/pregexp.scm 252 */
											obj_t BgL_arg1454z00_1432;

											BgL_arg1454z00_1432 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1452z00_1430), BNIL);
											BgL_list1453z00_1431 =
												MAKE_YOUNG_PAIR(BGl_keyword2508z00zz__regexpz00,
												BgL_arg1454z00_1432);
										}
										return BgL_list1453z00_1431;
									}
								}
								break;
							case ((unsigned char) 'S'):

								{	/* Unsafe/pregexp.scm 253 */
									long BgL_arg1455z00_1433;

									BgL_arg1455z00_1433 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 253 */
										obj_t BgL_list1456z00_1434;

										{	/* Unsafe/pregexp.scm 253 */
											obj_t BgL_arg1457z00_1435;

											BgL_arg1457z00_1435 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1455z00_1433), BNIL);
											BgL_list1456z00_1434 =
												MAKE_YOUNG_PAIR(BGl_list2510z00zz__regexpz00,
												BgL_arg1457z00_1435);
										}
										return BgL_list1456z00_1434;
									}
								}
								break;
							case ((unsigned char) 't'):

								{	/* Unsafe/pregexp.scm 254 */
									long BgL_arg1458z00_1436;

									BgL_arg1458z00_1436 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 254 */
										obj_t BgL_list1459z00_1437;

										{	/* Unsafe/pregexp.scm 254 */
											obj_t BgL_arg1460z00_1438;

											BgL_arg1460z00_1438 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1458z00_1436), BNIL);
											BgL_list1459z00_1437 =
												MAKE_YOUNG_PAIR(BCHAR
												(BGl_za2pregexpzd2tabzd2charza2z00zz__regexpz00),
												BgL_arg1460z00_1438);
										}
										return BgL_list1459z00_1437;
									}
								}
								break;
							case ((unsigned char) 'w'):

								{	/* Unsafe/pregexp.scm 255 */
									long BgL_arg1461z00_1439;

									BgL_arg1461z00_1439 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 255 */
										obj_t BgL_list1462z00_1440;

										{	/* Unsafe/pregexp.scm 255 */
											obj_t BgL_arg1463z00_1441;

											BgL_arg1463z00_1441 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1461z00_1439), BNIL);
											BgL_list1462z00_1440 =
												MAKE_YOUNG_PAIR(BGl_keyword2511z00zz__regexpz00,
												BgL_arg1463z00_1441);
										}
										return BgL_list1462z00_1440;
									}
								}
								break;
							case ((unsigned char) 'W'):

								{	/* Unsafe/pregexp.scm 256 */
									long BgL_arg1464z00_1442;

									BgL_arg1464z00_1442 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 256 */
										obj_t BgL_list1465z00_1443;

										{	/* Unsafe/pregexp.scm 256 */
											obj_t BgL_arg1466z00_1444;

											BgL_arg1466z00_1444 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1464z00_1442), BNIL);
											BgL_list1465z00_1443 =
												MAKE_YOUNG_PAIR(BGl_list2513z00zz__regexpz00,
												BgL_arg1466z00_1444);
										}
										return BgL_list1465z00_1443;
									}
								}
								break;
							default:
								{	/* Unsafe/pregexp.scm 257 */
									long BgL_arg1467z00_1445;

									BgL_arg1467z00_1445 = ((long) CINT(BgL_iz00_22) + 2L);
									{	/* Unsafe/pregexp.scm 257 */
										obj_t BgL_list1468z00_1446;

										{	/* Unsafe/pregexp.scm 257 */
											obj_t BgL_arg1469z00_1447;

											BgL_arg1469z00_1447 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg1467z00_1445), BNIL);
											BgL_list1468z00_1446 =
												MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1408),
												BgL_arg1469z00_1447);
										}
										return BgL_list1468z00_1446;
									}
								}
							}
					}
				}
			else
				{	/* Unsafe/pregexp.scm 243 */
					return BFALSE;
				}
		}

	}



/* pregexp-read-posix-char-class */
	obj_t BGl_pregexpzd2readzd2posixzd2charzd2classz00zz__regexpz00(obj_t
		BgL_sz00_24, long BgL_iz00_25, long BgL_nz00_26)
	{
		{	/* Unsafe/pregexp.scm 259 */
			{	/* Unsafe/pregexp.scm 262 */
				bool_t BgL_negzf3zf3_1450;

				BgL_negzf3zf3_1450 = ((bool_t) 0);
				{
					long BgL_iz00_1453;
					obj_t BgL_rz00_1454;

					BgL_iz00_1453 = BgL_iz00_25;
					BgL_rz00_1454 = BNIL;
				BgL_zc3z04anonymousza31474ze3z87_1455:
					if ((BgL_iz00_1453 >= BgL_nz00_26))
						{	/* Unsafe/pregexp.scm 267 */
							return
								BGl_errorz00zz__errorz00(BGl_string2500z00zz__regexpz00,
								BGl_symbol2514z00zz__regexpz00, BUNSPEC);
						}
					else
						{	/* Unsafe/pregexp.scm 269 */
							unsigned char BgL_cz00_1458;

							BgL_cz00_1458 = STRING_REF(BgL_sz00_24, BgL_iz00_1453);
							if ((BgL_cz00_1458 == ((unsigned char) '^')))
								{	/* Unsafe/pregexp.scm 270 */
									BgL_negzf3zf3_1450 = ((bool_t) 1);
									{
										long BgL_iz00_4948;

										BgL_iz00_4948 = (BgL_iz00_1453 + 1L);
										BgL_iz00_1453 = BgL_iz00_4948;
										goto BgL_zc3z04anonymousza31474ze3z87_1455;
									}
								}
							else
								{	/* Unsafe/pregexp.scm 270 */
									if (isalpha(BgL_cz00_1458))
										{	/* Unsafe/pregexp.scm 274 */
											long BgL_arg1480z00_1462;
											obj_t BgL_arg1481z00_1463;

											BgL_arg1480z00_1462 = (BgL_iz00_1453 + 1L);
											BgL_arg1481z00_1463 =
												MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1458), BgL_rz00_1454);
											{
												obj_t BgL_rz00_4956;
												long BgL_iz00_4955;

												BgL_iz00_4955 = BgL_arg1480z00_1462;
												BgL_rz00_4956 = BgL_arg1481z00_1463;
												BgL_rz00_1454 = BgL_rz00_4956;
												BgL_iz00_1453 = BgL_iz00_4955;
												goto BgL_zc3z04anonymousza31474ze3z87_1455;
											}
										}
									else
										{	/* Unsafe/pregexp.scm 273 */
											if ((BgL_cz00_1458 == ((unsigned char) ':')))
												{	/* Unsafe/pregexp.scm 276 */
													bool_t BgL_test2747z00_4959;

													if (((BgL_iz00_1453 + 1L) >= BgL_nz00_26))
														{	/* Unsafe/pregexp.scm 276 */
															BgL_test2747z00_4959 = ((bool_t) 1);
														}
													else
														{	/* Unsafe/pregexp.scm 276 */
															if (
																(STRING_REF(BgL_sz00_24,
																		(BgL_iz00_1453 + 1L)) ==
																	((unsigned char) ']')))
																{	/* Unsafe/pregexp.scm 277 */
																	BgL_test2747z00_4959 = ((bool_t) 0);
																}
															else
																{	/* Unsafe/pregexp.scm 277 */
																	BgL_test2747z00_4959 = ((bool_t) 1);
																}
														}
													if (BgL_test2747z00_4959)
														{	/* Unsafe/pregexp.scm 278 */
															obj_t BgL_list1492z00_1474;

															BgL_list1492z00_1474 =
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ':')),
																BNIL);
															return
																BGl_errorz00zz__errorz00
																(BGl_string2500z00zz__regexpz00,
																BGl_symbol2514z00zz__regexpz00,
																CAR(BgL_list1492z00_1474));
														}
													else
														{	/* Unsafe/pregexp.scm 279 */
															obj_t BgL_posixzd2classzd2_1475;

															BgL_posixzd2classzd2_1475 =
																bstring_to_keyword
																(BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
																(BGl_pregexpzd2reversez12zc0zz__regexpz00
																	(BgL_rz00_1454)));
															{	/* Unsafe/pregexp.scm 282 */
																obj_t BgL_arg1494z00_1476;
																long BgL_arg1495z00_1477;

																if (BgL_negzf3zf3_1450)
																	{	/* Unsafe/pregexp.scm 282 */
																		obj_t BgL_list1498z00_1480;

																		{	/* Unsafe/pregexp.scm 282 */
																			obj_t BgL_arg1499z00_1481;

																			BgL_arg1499z00_1481 =
																				MAKE_YOUNG_PAIR
																				(BgL_posixzd2classzd2_1475, BNIL);
																			BgL_list1498z00_1480 =
																				MAKE_YOUNG_PAIR
																				(BGl_keyword2492z00zz__regexpz00,
																				BgL_arg1499z00_1481);
																		}
																		BgL_arg1494z00_1476 = BgL_list1498z00_1480;
																	}
																else
																	{	/* Unsafe/pregexp.scm 282 */
																		BgL_arg1494z00_1476 =
																			BgL_posixzd2classzd2_1475;
																	}
																BgL_arg1495z00_1477 = (BgL_iz00_1453 + 2L);
																{	/* Unsafe/pregexp.scm 282 */
																	obj_t BgL_list1496z00_1478;

																	{	/* Unsafe/pregexp.scm 282 */
																		obj_t BgL_arg1497z00_1479;

																		BgL_arg1497z00_1479 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1495z00_1477),
																			BNIL);
																		BgL_list1496z00_1478 =
																			MAKE_YOUNG_PAIR(BgL_arg1494z00_1476,
																			BgL_arg1497z00_1479);
																	}
																	return BgL_list1496z00_1478;
																}
															}
														}
												}
											else
												{	/* Unsafe/pregexp.scm 275 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string2500z00zz__regexpz00,
														BGl_symbol2514z00zz__regexpz00, BUNSPEC);
												}
										}
								}
						}
				}
			}
		}

	}



/* pregexp-read-cluster-type */
	obj_t BGl_pregexpzd2readzd2clusterzd2typezd2zz__regexpz00(obj_t BgL_sz00_27,
		long BgL_iz00_28, long BgL_nz00_29)
	{
		{	/* Unsafe/pregexp.scm 288 */
			{	/* Unsafe/pregexp.scm 291 */
				unsigned char BgL_cz00_1491;

				BgL_cz00_1491 = STRING_REF(BgL_sz00_27, BgL_iz00_28);
				{

					switch (BgL_cz00_1491)
						{
						case ((unsigned char) '?'):

							{	/* Unsafe/pregexp.scm 294 */
								long BgL_iz00_1495;

								BgL_iz00_1495 = (BgL_iz00_28 + 1L);
								{

									{	/* Unsafe/pregexp.scm 295 */
										unsigned char BgL_aux1067z00_1497;

										BgL_aux1067z00_1497 =
											STRING_REF(BgL_sz00_27, BgL_iz00_1495);
										switch (BgL_aux1067z00_1497)
											{
											case ((unsigned char) ':'):

												{	/* Unsafe/pregexp.scm 296 */
													long BgL_arg1508z00_1499;

													BgL_arg1508z00_1499 = (BgL_iz00_1495 + 1L);
													{	/* Unsafe/pregexp.scm 296 */
														obj_t BgL_list1509z00_1500;

														{	/* Unsafe/pregexp.scm 296 */
															obj_t BgL_arg1510z00_1501;

															BgL_arg1510z00_1501 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1508z00_1499),
																BNIL);
															BgL_list1509z00_1500 =
																MAKE_YOUNG_PAIR(BNIL, BgL_arg1510z00_1501);
														}
														return BgL_list1509z00_1500;
													}
												}
												break;
											case ((unsigned char) '='):

												{	/* Unsafe/pregexp.scm 297 */
													long BgL_arg1511z00_1502;

													BgL_arg1511z00_1502 = (BgL_iz00_1495 + 1L);
													{	/* Unsafe/pregexp.scm 297 */
														obj_t BgL_list1512z00_1503;

														{	/* Unsafe/pregexp.scm 297 */
															obj_t BgL_arg1513z00_1504;

															BgL_arg1513z00_1504 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1511z00_1502),
																BNIL);
															BgL_list1512z00_1503 =
																MAKE_YOUNG_PAIR(BGl_list2525z00zz__regexpz00,
																BgL_arg1513z00_1504);
														}
														return BgL_list1512z00_1503;
													}
												}
												break;
											case ((unsigned char) '!'):

												{	/* Unsafe/pregexp.scm 298 */
													long BgL_arg1514z00_1505;

													BgL_arg1514z00_1505 = (BgL_iz00_1495 + 1L);
													{	/* Unsafe/pregexp.scm 298 */
														obj_t BgL_list1515z00_1506;

														{	/* Unsafe/pregexp.scm 298 */
															obj_t BgL_arg1516z00_1507;

															BgL_arg1516z00_1507 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1514z00_1505),
																BNIL);
															BgL_list1515z00_1506 =
																MAKE_YOUNG_PAIR(BGl_list2528z00zz__regexpz00,
																BgL_arg1516z00_1507);
														}
														return BgL_list1515z00_1506;
													}
												}
												break;
											case ((unsigned char) '>'):

												{	/* Unsafe/pregexp.scm 299 */
													long BgL_arg1517z00_1508;

													BgL_arg1517z00_1508 = (BgL_iz00_1495 + 1L);
													{	/* Unsafe/pregexp.scm 299 */
														obj_t BgL_list1518z00_1509;

														{	/* Unsafe/pregexp.scm 299 */
															obj_t BgL_arg1521z00_1510;

															BgL_arg1521z00_1510 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1517z00_1508),
																BNIL);
															BgL_list1518z00_1509 =
																MAKE_YOUNG_PAIR(BGl_list2531z00zz__regexpz00,
																BgL_arg1521z00_1510);
														}
														return BgL_list1518z00_1509;
													}
												}
												break;
											case ((unsigned char) '<'):

												{	/* Unsafe/pregexp.scm 301 */
													obj_t BgL_arg1522z00_1511;
													long BgL_arg1523z00_1512;

													{	/* Unsafe/pregexp.scm 301 */
														unsigned char BgL_aux1069z00_1516;

														BgL_aux1069z00_1516 =
															STRING_REF(BgL_sz00_27, (BgL_iz00_1495 + 1L));
														switch (BgL_aux1069z00_1516)
															{
															case ((unsigned char) '='):

																BgL_arg1522z00_1511 =
																	BGl_list2534z00zz__regexpz00;
																break;
															case ((unsigned char) '!'):

																BgL_arg1522z00_1511 =
																	BGl_list2537z00zz__regexpz00;
																break;
															default:
																{	/* Unsafe/pregexp.scm 304 */
																	obj_t BgL_list1528z00_3029;

																	BgL_list1528z00_3029 =
																		MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																				'<')), BNIL);
																	BgL_arg1522z00_1511 =
																		BGl_errorz00zz__errorz00
																		(BGl_string2500z00zz__regexpz00,
																		BGl_symbol2523z00zz__regexpz00,
																		CAR(BgL_list1528z00_3029));
													}}}
													BgL_arg1523z00_1512 = (BgL_iz00_1495 + 2L);
													{	/* Unsafe/pregexp.scm 301 */
														obj_t BgL_list1524z00_1513;

														{	/* Unsafe/pregexp.scm 301 */
															obj_t BgL_arg1525z00_1514;

															BgL_arg1525z00_1514 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1523z00_1512),
																BNIL);
															BgL_list1524z00_1513 =
																MAKE_YOUNG_PAIR(BgL_arg1522z00_1511,
																BgL_arg1525z00_1514);
														}
														return BgL_list1524z00_1513;
													}
												}
												break;
											default:
												{
													long BgL_iz00_1522;
													obj_t BgL_rz00_1523;
													bool_t BgL_invzf3zf3_1524;

													BgL_iz00_1522 = BgL_iz00_1495;
													BgL_rz00_1523 = BNIL;
													BgL_invzf3zf3_1524 = ((bool_t) 0);
												BgL_zc3z04anonymousza31529ze3z87_1525:
													{	/* Unsafe/pregexp.scm 307 */
														unsigned char BgL_cz00_1526;

														BgL_cz00_1526 =
															STRING_REF(BgL_sz00_27, BgL_iz00_1522);
														switch (BgL_cz00_1526)
															{
															case ((unsigned char) '-'):

																{
																	bool_t BgL_invzf3zf3_5015;
																	long BgL_iz00_5013;

																	BgL_iz00_5013 = (BgL_iz00_1522 + 1L);
																	BgL_invzf3zf3_5015 = ((bool_t) 1);
																	BgL_invzf3zf3_1524 = BgL_invzf3zf3_5015;
																	BgL_iz00_1522 = BgL_iz00_5013;
																	goto BgL_zc3z04anonymousza31529ze3z87_1525;
																}
																break;
															case ((unsigned char) 'i'):

																{	/* Unsafe/pregexp.scm 310 */
																	long BgL_arg1535z00_1531;
																	obj_t BgL_arg1536z00_1532;

																	BgL_arg1535z00_1531 = (BgL_iz00_1522 + 1L);
																	{	/* Unsafe/pregexp.scm 311 */
																		obj_t BgL_arg1537z00_1533;

																		if (BgL_invzf3zf3_1524)
																			{	/* Unsafe/pregexp.scm 311 */
																				BgL_arg1537z00_1533 =
																					BGl_keyword2519z00zz__regexpz00;
																			}
																		else
																			{	/* Unsafe/pregexp.scm 311 */
																				BgL_arg1537z00_1533 =
																					BGl_keyword2521z00zz__regexpz00;
																			}
																		BgL_arg1536z00_1532 =
																			MAKE_YOUNG_PAIR(BgL_arg1537z00_1533,
																			BgL_rz00_1523);
																	}
																	{
																		bool_t BgL_invzf3zf3_5021;
																		obj_t BgL_rz00_5020;
																		long BgL_iz00_5019;

																		BgL_iz00_5019 = BgL_arg1535z00_1531;
																		BgL_rz00_5020 = BgL_arg1536z00_1532;
																		BgL_invzf3zf3_5021 = ((bool_t) 0);
																		BgL_invzf3zf3_1524 = BgL_invzf3zf3_5021;
																		BgL_rz00_1523 = BgL_rz00_5020;
																		BgL_iz00_1522 = BgL_iz00_5019;
																		goto BgL_zc3z04anonymousza31529ze3z87_1525;
																	}
																}
																break;
															case ((unsigned char) 'x'):

																BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00 = BgL_invzf3zf3_1524;
																{
																	bool_t BgL_invzf3zf3_5024;
																	long BgL_iz00_5022;

																	BgL_iz00_5022 = (BgL_iz00_1522 + 1L);
																	BgL_invzf3zf3_5024 = ((bool_t) 0);
																	BgL_invzf3zf3_1524 = BgL_invzf3zf3_5024;
																	BgL_iz00_1522 = BgL_iz00_5022;
																	goto BgL_zc3z04anonymousza31529ze3z87_1525;
																}
																break;
															case ((unsigned char) ':'):

																{	/* Unsafe/pregexp.scm 316 */
																	long BgL_arg1540z00_1535;

																	BgL_arg1540z00_1535 = (BgL_iz00_1522 + 1L);
																	{	/* Unsafe/pregexp.scm 316 */
																		obj_t BgL_list1541z00_1536;

																		{	/* Unsafe/pregexp.scm 316 */
																			obj_t BgL_arg1543z00_1537;

																			BgL_arg1543z00_1537 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg1540z00_1535), BNIL);
																			BgL_list1541z00_1536 =
																				MAKE_YOUNG_PAIR(BgL_rz00_1523,
																				BgL_arg1543z00_1537);
																		}
																		return BgL_list1541z00_1536;
																	}
																}
																break;
															default:
																{	/* Unsafe/pregexp.scm 317 */
																	obj_t BgL_list1544z00_3001;

																	BgL_list1544z00_3001 =
																		MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1526), BNIL);
																	return
																		BGl_errorz00zz__errorz00
																		(BGl_string2500z00zz__regexpz00,
																		BGl_symbol2523z00zz__regexpz00,
																		CAR(BgL_list1544z00_3001));
																}
															}
													}
												}
											}
									}
								}
							}
							break;
						default:
							{	/* Unsafe/pregexp.scm 318 */
								obj_t BgL_list1545z00_1540;

								{	/* Unsafe/pregexp.scm 318 */
									obj_t BgL_arg1546z00_1541;

									BgL_arg1546z00_1541 =
										MAKE_YOUNG_PAIR(BINT(BgL_iz00_28), BNIL);
									BgL_list1545z00_1540 =
										MAKE_YOUNG_PAIR(BGl_list2516z00zz__regexpz00,
										BgL_arg1546z00_1541);
								}
								return BgL_list1545z00_1540;
							}
						}
				}
			}
		}

	}



/* pregexp-read-subpattern */
	obj_t BGl_pregexpzd2readzd2subpatternz00zz__regexpz00(obj_t BgL_sz00_30,
		long BgL_iz00_31, long BgL_nz00_32)
	{
		{	/* Unsafe/pregexp.scm 320 */
			{	/* Unsafe/pregexp.scm 322 */
				bool_t BgL_rememberzd2spacezd2sensitivezf3zf3_1542;

				BgL_rememberzd2spacezd2sensitivezf3zf3_1542 =
					BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00;
				{	/* Unsafe/pregexp.scm 322 */
					obj_t BgL_ctypzd2izd2_1543;

					BgL_ctypzd2izd2_1543 =
						BGl_pregexpzd2readzd2clusterzd2typezd2zz__regexpz00(BgL_sz00_30,
						BgL_iz00_31, BgL_nz00_32);
					{	/* Unsafe/pregexp.scm 323 */
						obj_t BgL_ctypz00_1544;

						BgL_ctypz00_1544 = CAR(((obj_t) BgL_ctypzd2izd2_1543));
						{	/* Unsafe/pregexp.scm 324 */
							obj_t BgL_iz00_1545;

							{	/* Unsafe/pregexp.scm 325 */
								obj_t BgL_pairz00_3045;

								BgL_pairz00_3045 = CDR(((obj_t) BgL_ctypzd2izd2_1543));
								BgL_iz00_1545 = CAR(BgL_pairz00_3045);
							}
							{	/* Unsafe/pregexp.scm 325 */
								obj_t BgL_vvz00_1546;

								BgL_vvz00_1546 =
									BGl_pregexpzd2readzd2patternz00zz__regexpz00(BgL_sz00_30,
									BgL_iz00_1545, BgL_nz00_32);
								{	/* Unsafe/pregexp.scm 326 */

									BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00 =
										BgL_rememberzd2spacezd2sensitivezf3zf3_1542;
									{	/* Unsafe/pregexp.scm 328 */
										obj_t BgL_vvzd2rezd2_1547;
										obj_t BgL_vvzd2izd2_1548;

										BgL_vvzd2rezd2_1547 = CAR(((obj_t) BgL_vvz00_1546));
										{	/* Unsafe/pregexp.scm 329 */
											obj_t BgL_pairz00_3050;

											BgL_pairz00_3050 = CDR(((obj_t) BgL_vvz00_1546));
											BgL_vvzd2izd2_1548 = CAR(BgL_pairz00_3050);
										}
										{	/* Unsafe/pregexp.scm 330 */
											bool_t BgL_test2752z00_5051;

											if (((long) CINT(BgL_vvzd2izd2_1548) < BgL_nz00_32))
												{	/* Unsafe/pregexp.scm 330 */
													BgL_test2752z00_5051 =
														(STRING_REF(BgL_sz00_30,
															(long) CINT(BgL_vvzd2izd2_1548)) ==
														((unsigned char) ')'));
												}
											else
												{	/* Unsafe/pregexp.scm 330 */
													BgL_test2752z00_5051 = ((bool_t) 0);
												}
											if (BgL_test2752z00_5051)
												{	/* Unsafe/pregexp.scm 335 */
													obj_t BgL_arg1552z00_1552;
													long BgL_arg1553z00_1553;

													{
														obj_t BgL_ctypz00_3076;
														obj_t BgL_rez00_3077;

														BgL_ctypz00_3076 = BgL_ctypz00_1544;
														BgL_rez00_3077 = BgL_vvzd2rezd2_1547;
													BgL_loopz00_3075:
														if (NULLP(BgL_ctypz00_3076))
															{	/* Unsafe/pregexp.scm 335 */
																BgL_arg1552z00_1552 = BgL_rez00_3077;
															}
														else
															{	/* Unsafe/pregexp.scm 336 */
																obj_t BgL_arg1558z00_3085;
																obj_t BgL_arg1559z00_3086;

																BgL_arg1558z00_3085 =
																	CDR(((obj_t) BgL_ctypz00_3076));
																{	/* Unsafe/pregexp.scm 337 */
																	obj_t BgL_arg1561z00_3087;

																	BgL_arg1561z00_3087 =
																		CAR(((obj_t) BgL_ctypz00_3076));
																	{	/* Unsafe/pregexp.scm 337 */
																		obj_t BgL_list1562z00_3088;

																		{	/* Unsafe/pregexp.scm 337 */
																			obj_t BgL_arg1564z00_3089;

																			BgL_arg1564z00_3089 =
																				MAKE_YOUNG_PAIR(BgL_rez00_3077, BNIL);
																			BgL_list1562z00_3088 =
																				MAKE_YOUNG_PAIR(BgL_arg1561z00_3087,
																				BgL_arg1564z00_3089);
																		}
																		BgL_arg1559z00_3086 = BgL_list1562z00_3088;
																	}
																}
																{
																	obj_t BgL_rez00_5067;
																	obj_t BgL_ctypz00_5066;

																	BgL_ctypz00_5066 = BgL_arg1558z00_3085;
																	BgL_rez00_5067 = BgL_arg1559z00_3086;
																	BgL_rez00_3077 = BgL_rez00_5067;
																	BgL_ctypz00_3076 = BgL_ctypz00_5066;
																	goto BgL_loopz00_3075;
																}
															}
													}
													BgL_arg1553z00_1553 =
														((long) CINT(BgL_vvzd2izd2_1548) + 1L);
													{	/* Unsafe/pregexp.scm 333 */
														obj_t BgL_list1554z00_1554;

														{	/* Unsafe/pregexp.scm 333 */
															obj_t BgL_arg1555z00_1555;

															BgL_arg1555z00_1555 =
																MAKE_YOUNG_PAIR(BINT(BgL_arg1553z00_1553),
																BNIL);
															BgL_list1554z00_1554 =
																MAKE_YOUNG_PAIR(BgL_arg1552z00_1552,
																BgL_arg1555z00_1555);
														}
														return BgL_list1554z00_1554;
													}
												}
											else
												{	/* Unsafe/pregexp.scm 330 */
													return
														BGl_errorz00zz__errorz00
														(BGl_string2500z00zz__regexpz00,
														BGl_symbol2540z00zz__regexpz00, BUNSPEC);
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* pregexp-wrap-quantifier-if-any */
	obj_t BGl_pregexpzd2wrapzd2quantifierzd2ifzd2anyz00zz__regexpz00(obj_t
		BgL_vvz00_33, obj_t BgL_sz00_34, long BgL_nz00_35)
	{
		{	/* Unsafe/pregexp.scm 341 */
			{	/* Unsafe/pregexp.scm 343 */
				obj_t BgL_rez00_1570;

				BgL_rez00_1570 = CAR(((obj_t) BgL_vvz00_33));
				{	/* Unsafe/pregexp.scm 344 */
					obj_t BgL_g1070z00_1571;

					{	/* Unsafe/pregexp.scm 344 */
						obj_t BgL_pairz00_3103;

						BgL_pairz00_3103 = CDR(((obj_t) BgL_vvz00_33));
						BgL_g1070z00_1571 = CAR(BgL_pairz00_3103);
					}
					{
						obj_t BgL_iz00_1573;

						BgL_iz00_1573 = BgL_g1070z00_1571;
					BgL_zc3z04anonymousza31568ze3z87_1574:
						if (((long) CINT(BgL_iz00_1573) >= BgL_nz00_35))
							{	/* Unsafe/pregexp.scm 345 */
								return BgL_vvz00_33;
							}
						else
							{	/* Unsafe/pregexp.scm 346 */
								unsigned char BgL_cz00_1576;

								BgL_cz00_1576 =
									STRING_REF(BgL_sz00_34, (long) CINT(BgL_iz00_1573));
								{	/* Unsafe/pregexp.scm 347 */
									bool_t BgL_test2756z00_5084;

									if (isspace(BgL_cz00_1576))
										{	/* Unsafe/pregexp.scm 347 */
											if (BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00)
												{	/* Unsafe/pregexp.scm 347 */
													BgL_test2756z00_5084 = ((bool_t) 0);
												}
											else
												{	/* Unsafe/pregexp.scm 347 */
													BgL_test2756z00_5084 = ((bool_t) 1);
												}
										}
									else
										{	/* Unsafe/pregexp.scm 347 */
											BgL_test2756z00_5084 = ((bool_t) 0);
										}
									if (BgL_test2756z00_5084)
										{	/* Unsafe/pregexp.scm 348 */
											long BgL_arg1573z00_1579;

											BgL_arg1573z00_1579 = ((long) CINT(BgL_iz00_1573) + 1L);
											{
												obj_t BgL_iz00_5090;

												BgL_iz00_5090 = BINT(BgL_arg1573z00_1579);
												BgL_iz00_1573 = BgL_iz00_5090;
												goto BgL_zc3z04anonymousza31568ze3z87_1574;
											}
										}
									else
										{	/* Unsafe/pregexp.scm 347 */
											switch (BgL_cz00_1576)
												{
												case ((unsigned char) '*'):
												case ((unsigned char) '+'):
												case ((unsigned char) '?'):
												case ((unsigned char) '{'):

													{	/* Unsafe/pregexp.scm 351 */
														obj_t BgL_newzd2rezd2_1583;

														{	/* Unsafe/pregexp.scm 351 */
															obj_t BgL_list1609z00_1622;

															{	/* Unsafe/pregexp.scm 351 */
																obj_t BgL_arg1610z00_1623;

																{	/* Unsafe/pregexp.scm 351 */
																	obj_t BgL_arg1611z00_1624;

																	{	/* Unsafe/pregexp.scm 351 */
																		obj_t BgL_arg1612z00_1625;

																		{	/* Unsafe/pregexp.scm 351 */
																			obj_t BgL_arg1613z00_1626;

																			BgL_arg1613z00_1626 =
																				MAKE_YOUNG_PAIR(BgL_rez00_1570, BNIL);
																			BgL_arg1612z00_1625 =
																				MAKE_YOUNG_PAIR
																				(BGl_symbol2542z00zz__regexpz00,
																				BgL_arg1613z00_1626);
																		}
																		BgL_arg1611z00_1624 =
																			MAKE_YOUNG_PAIR
																			(BGl_symbol2544z00zz__regexpz00,
																			BgL_arg1612z00_1625);
																	}
																	BgL_arg1610z00_1623 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2546z00zz__regexpz00,
																		BgL_arg1611z00_1624);
																}
																BgL_list1609z00_1622 =
																	MAKE_YOUNG_PAIR
																	(BGl_keyword2548z00zz__regexpz00,
																	BgL_arg1610z00_1623);
															}
															BgL_newzd2rezd2_1583 = BgL_list1609z00_1622;
														}
														{	/* Unsafe/pregexp.scm 351 */
															obj_t BgL_newzd2vvzd2_1584;

															{	/* Unsafe/pregexp.scm 353 */
																obj_t BgL_list1607z00_1620;

																{	/* Unsafe/pregexp.scm 353 */
																	obj_t BgL_arg1608z00_1621;

																	BgL_arg1608z00_1621 =
																		MAKE_YOUNG_PAIR
																		(BGl_symbol2550z00zz__regexpz00, BNIL);
																	BgL_list1607z00_1620 =
																		MAKE_YOUNG_PAIR(BgL_newzd2rezd2_1583,
																		BgL_arg1608z00_1621);
																}
																BgL_newzd2vvzd2_1584 = BgL_list1607z00_1620;
															}
															{	/* Unsafe/pregexp.scm 353 */

																{	/* Unsafe/pregexp.scm 354 */
																	obj_t BgL_aux1074z00_1586;

																	BgL_aux1074z00_1586 = BCHAR(BgL_cz00_1576);
																	switch (CCHAR(BgL_aux1074z00_1586))
																		{
																		case ((unsigned char) '*'):

																			{	/* Unsafe/pregexp.scm 355 */
																				obj_t BgL_arg1576z00_1588;

																				{	/* Unsafe/pregexp.scm 355 */
																					obj_t BgL_pairz00_3115;

																					BgL_pairz00_3115 =
																						CDR(((obj_t) BgL_newzd2rezd2_1583));
																					BgL_arg1576z00_1588 =
																						CDR(BgL_pairz00_3115);
																				}
																				{	/* Unsafe/pregexp.scm 355 */
																					obj_t BgL_auxz00_5105;
																					obj_t BgL_tmpz00_5103;

																					BgL_auxz00_5105 = BINT(0L);
																					BgL_tmpz00_5103 =
																						((obj_t) BgL_arg1576z00_1588);
																					SET_CAR(BgL_tmpz00_5103,
																						BgL_auxz00_5105);
																			}}
																			{	/* Unsafe/pregexp.scm 356 */
																				obj_t BgL_arg1578z00_1589;

																				{	/* Unsafe/pregexp.scm 356 */
																					obj_t BgL_pairz00_3122;

																					{	/* Unsafe/pregexp.scm 356 */
																						obj_t BgL_pairz00_3121;

																						BgL_pairz00_3121 =
																							CDR(
																							((obj_t) BgL_newzd2rezd2_1583));
																						BgL_pairz00_3122 =
																							CDR(BgL_pairz00_3121);
																					}
																					BgL_arg1578z00_1589 =
																						CDR(BgL_pairz00_3122);
																				}
																				{	/* Unsafe/pregexp.scm 356 */
																					obj_t BgL_tmpz00_5112;

																					BgL_tmpz00_5112 =
																						((obj_t) BgL_arg1578z00_1589);
																					SET_CAR(BgL_tmpz00_5112, BFALSE);
																			}} break;
																		case ((unsigned char) '+'):

																			{	/* Unsafe/pregexp.scm 357 */
																				obj_t BgL_arg1579z00_1590;

																				{	/* Unsafe/pregexp.scm 357 */
																					obj_t BgL_pairz00_3127;

																					BgL_pairz00_3127 =
																						CDR(((obj_t) BgL_newzd2rezd2_1583));
																					BgL_arg1579z00_1590 =
																						CDR(BgL_pairz00_3127);
																				}
																				{	/* Unsafe/pregexp.scm 357 */
																					obj_t BgL_auxz00_5120;
																					obj_t BgL_tmpz00_5118;

																					BgL_auxz00_5120 = BINT(1L);
																					BgL_tmpz00_5118 =
																						((obj_t) BgL_arg1579z00_1590);
																					SET_CAR(BgL_tmpz00_5118,
																						BgL_auxz00_5120);
																			}}
																			{	/* Unsafe/pregexp.scm 358 */
																				obj_t BgL_arg1580z00_1591;

																				{	/* Unsafe/pregexp.scm 358 */
																					obj_t BgL_pairz00_3134;

																					{	/* Unsafe/pregexp.scm 358 */
																						obj_t BgL_pairz00_3133;

																						BgL_pairz00_3133 =
																							CDR(
																							((obj_t) BgL_newzd2rezd2_1583));
																						BgL_pairz00_3134 =
																							CDR(BgL_pairz00_3133);
																					}
																					BgL_arg1580z00_1591 =
																						CDR(BgL_pairz00_3134);
																				}
																				{	/* Unsafe/pregexp.scm 358 */
																					obj_t BgL_tmpz00_5127;

																					BgL_tmpz00_5127 =
																						((obj_t) BgL_arg1580z00_1591);
																					SET_CAR(BgL_tmpz00_5127, BFALSE);
																			}} break;
																		case ((unsigned char) '?'):

																			{	/* Unsafe/pregexp.scm 359 */
																				obj_t BgL_arg1582z00_1592;

																				{	/* Unsafe/pregexp.scm 359 */
																					obj_t BgL_pairz00_3139;

																					BgL_pairz00_3139 =
																						CDR(((obj_t) BgL_newzd2rezd2_1583));
																					BgL_arg1582z00_1592 =
																						CDR(BgL_pairz00_3139);
																				}
																				{	/* Unsafe/pregexp.scm 359 */
																					obj_t BgL_auxz00_5135;
																					obj_t BgL_tmpz00_5133;

																					BgL_auxz00_5135 = BINT(0L);
																					BgL_tmpz00_5133 =
																						((obj_t) BgL_arg1582z00_1592);
																					SET_CAR(BgL_tmpz00_5133,
																						BgL_auxz00_5135);
																			}}
																			{	/* Unsafe/pregexp.scm 360 */
																				obj_t BgL_arg1583z00_1593;

																				{	/* Unsafe/pregexp.scm 360 */
																					obj_t BgL_pairz00_3146;

																					{	/* Unsafe/pregexp.scm 360 */
																						obj_t BgL_pairz00_3145;

																						BgL_pairz00_3145 =
																							CDR(
																							((obj_t) BgL_newzd2rezd2_1583));
																						BgL_pairz00_3146 =
																							CDR(BgL_pairz00_3145);
																					}
																					BgL_arg1583z00_1593 =
																						CDR(BgL_pairz00_3146);
																				}
																				{	/* Unsafe/pregexp.scm 360 */
																					obj_t BgL_auxz00_5144;
																					obj_t BgL_tmpz00_5142;

																					BgL_auxz00_5144 = BINT(1L);
																					BgL_tmpz00_5142 =
																						((obj_t) BgL_arg1583z00_1593);
																					SET_CAR(BgL_tmpz00_5142,
																						BgL_auxz00_5144);
																			}} break;
																		case ((unsigned char) '{'):

																			{	/* Unsafe/pregexp.scm 361 */
																				obj_t BgL_pqz00_1594;

																				BgL_pqz00_1594 =
																					BGl_pregexpzd2readzd2numsz00zz__regexpz00
																					(BgL_sz00_34,
																					((long) CINT(BgL_iz00_1573) + 1L),
																					BgL_nz00_35);
																				if (CBOOL(BgL_pqz00_1594))
																					{	/* Unsafe/pregexp.scm 362 */
																						BFALSE;
																					}
																				else
																					{	/* Unsafe/pregexp.scm 363 */
																						obj_t BgL_list1584z00_1595;

																						BgL_list1584z00_1595 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2552z00zz__regexpz00,
																							BNIL);
																						BGl_errorz00zz__errorz00
																							(BGl_string2500z00zz__regexpz00,
																							BGl_symbol2553z00zz__regexpz00,
																							CAR(BgL_list1584z00_1595));
																					}
																				{	/* Unsafe/pregexp.scm 366 */
																					obj_t BgL_arg1585z00_1596;
																					obj_t BgL_arg1586z00_1597;

																					{	/* Unsafe/pregexp.scm 366 */
																						obj_t BgL_pairz00_3156;

																						BgL_pairz00_3156 =
																							CDR(
																							((obj_t) BgL_newzd2rezd2_1583));
																						BgL_arg1585z00_1596 =
																							CDR(BgL_pairz00_3156);
																					}
																					BgL_arg1586z00_1597 =
																						CAR(((obj_t) BgL_pqz00_1594));
																					{	/* Unsafe/pregexp.scm 366 */
																						obj_t BgL_tmpz00_5160;

																						BgL_tmpz00_5160 =
																							((obj_t) BgL_arg1585z00_1596);
																						SET_CAR(BgL_tmpz00_5160,
																							BgL_arg1586z00_1597);
																					}
																				}
																				{	/* Unsafe/pregexp.scm 367 */
																					obj_t BgL_arg1587z00_1598;
																					obj_t BgL_arg1589z00_1599;

																					{	/* Unsafe/pregexp.scm 367 */
																						obj_t BgL_pairz00_3164;

																						{	/* Unsafe/pregexp.scm 367 */
																							obj_t BgL_pairz00_3163;

																							BgL_pairz00_3163 =
																								CDR(
																								((obj_t) BgL_newzd2rezd2_1583));
																							BgL_pairz00_3164 =
																								CDR(BgL_pairz00_3163);
																						}
																						BgL_arg1587z00_1598 =
																							CDR(BgL_pairz00_3164);
																					}
																					{	/* Unsafe/pregexp.scm 367 */
																						obj_t BgL_pairz00_3168;

																						BgL_pairz00_3168 =
																							CDR(((obj_t) BgL_pqz00_1594));
																						BgL_arg1589z00_1599 =
																							CAR(BgL_pairz00_3168);
																					}
																					{	/* Unsafe/pregexp.scm 367 */
																						obj_t BgL_tmpz00_5170;

																						BgL_tmpz00_5170 =
																							((obj_t) BgL_arg1587z00_1598);
																						SET_CAR(BgL_tmpz00_5170,
																							BgL_arg1589z00_1599);
																					}
																				}
																				{	/* Unsafe/pregexp.scm 368 */
																					obj_t BgL_pairz00_3175;

																					{	/* Unsafe/pregexp.scm 368 */
																						obj_t BgL_pairz00_3174;

																						BgL_pairz00_3174 =
																							CDR(((obj_t) BgL_pqz00_1594));
																						BgL_pairz00_3175 =
																							CDR(BgL_pairz00_3174);
																					}
																					BgL_iz00_1573 = CAR(BgL_pairz00_3175);
																				}
																			}
																			break;
																		default:
																			BUNSPEC;
																		}
																}
																{	/* Unsafe/pregexp.scm 369 */
																	long BgL_g1075z00_1601;

																	BgL_g1075z00_1601 =
																		((long) CINT(BgL_iz00_1573) + 1L);
																	{
																		long BgL_iz00_1603;

																		BgL_iz00_1603 = BgL_g1075z00_1601;
																	BgL_zc3z04anonymousza31592ze3z87_1604:
																		if ((BgL_iz00_1603 >= BgL_nz00_35))
																			{	/* Unsafe/pregexp.scm 370 */
																				{	/* Unsafe/pregexp.scm 371 */
																					obj_t BgL_arg1594z00_1606;

																					BgL_arg1594z00_1606 =
																						CDR(((obj_t) BgL_newzd2rezd2_1583));
																					{	/* Unsafe/pregexp.scm 371 */
																						obj_t BgL_tmpz00_5185;

																						BgL_tmpz00_5185 =
																							((obj_t) BgL_arg1594z00_1606);
																						SET_CAR(BgL_tmpz00_5185, BFALSE);
																					}
																				}
																				{	/* Unsafe/pregexp.scm 372 */
																					obj_t BgL_arg1595z00_1607;

																					BgL_arg1595z00_1607 =
																						CDR(((obj_t) BgL_newzd2vvzd2_1584));
																					{	/* Unsafe/pregexp.scm 372 */
																						obj_t BgL_auxz00_5192;
																						obj_t BgL_tmpz00_5190;

																						BgL_auxz00_5192 =
																							BINT(BgL_iz00_1603);
																						BgL_tmpz00_5190 =
																							((obj_t) BgL_arg1595z00_1607);
																						SET_CAR(BgL_tmpz00_5190,
																							BgL_auxz00_5192);
																					}
																				}
																			}
																		else
																			{	/* Unsafe/pregexp.scm 373 */
																				unsigned char BgL_cz00_1608;

																				BgL_cz00_1608 =
																					STRING_REF(BgL_sz00_34,
																					BgL_iz00_1603);
																				{	/* Unsafe/pregexp.scm 374 */
																					bool_t BgL_test2761z00_5196;

																					if (isspace(BgL_cz00_1608))
																						{	/* Unsafe/pregexp.scm 374 */
																							if (BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00)
																								{	/* Unsafe/pregexp.scm 375 */
																									BgL_test2761z00_5196 =
																										((bool_t) 0);
																								}
																							else
																								{	/* Unsafe/pregexp.scm 375 */
																									BgL_test2761z00_5196 =
																										((bool_t) 1);
																								}
																						}
																					else
																						{	/* Unsafe/pregexp.scm 374 */
																							BgL_test2761z00_5196 =
																								((bool_t) 0);
																						}
																					if (BgL_test2761z00_5196)
																						{
																							long BgL_iz00_5200;

																							BgL_iz00_5200 =
																								(BgL_iz00_1603 + 1L);
																							BgL_iz00_1603 = BgL_iz00_5200;
																							goto
																								BgL_zc3z04anonymousza31592ze3z87_1604;
																						}
																					else
																						{	/* Unsafe/pregexp.scm 374 */
																							if (
																								(BgL_cz00_1608 ==
																									((unsigned char) '?')))
																								{	/* Unsafe/pregexp.scm 377 */
																									{	/* Unsafe/pregexp.scm 378 */
																										obj_t BgL_arg1601z00_1613;

																										BgL_arg1601z00_1613 =
																											CDR(
																											((obj_t)
																												BgL_newzd2rezd2_1583));
																										{	/* Unsafe/pregexp.scm 378 */
																											obj_t BgL_tmpz00_5206;

																											BgL_tmpz00_5206 =
																												((obj_t)
																												BgL_arg1601z00_1613);
																											SET_CAR(BgL_tmpz00_5206,
																												BTRUE);
																									}}
																									{	/* Unsafe/pregexp.scm 379 */
																										obj_t BgL_arg1602z00_1614;
																										long BgL_arg1603z00_1615;

																										BgL_arg1602z00_1614 =
																											CDR(
																											((obj_t)
																												BgL_newzd2vvzd2_1584));
																										BgL_arg1603z00_1615 =
																											(BgL_iz00_1603 + 1L);
																										{	/* Unsafe/pregexp.scm 379 */
																											obj_t BgL_auxz00_5214;
																											obj_t BgL_tmpz00_5212;

																											BgL_auxz00_5214 =
																												BINT
																												(BgL_arg1603z00_1615);
																											BgL_tmpz00_5212 =
																												((obj_t)
																												BgL_arg1602z00_1614);
																											SET_CAR(BgL_tmpz00_5212,
																												BgL_auxz00_5214);
																								}}}
																							else
																								{	/* Unsafe/pregexp.scm 377 */
																									{	/* Unsafe/pregexp.scm 380 */
																										obj_t BgL_arg1605z00_1616;

																										BgL_arg1605z00_1616 =
																											CDR(
																											((obj_t)
																												BgL_newzd2rezd2_1583));
																										{	/* Unsafe/pregexp.scm 380 */
																											obj_t BgL_tmpz00_5219;

																											BgL_tmpz00_5219 =
																												((obj_t)
																												BgL_arg1605z00_1616);
																											SET_CAR(BgL_tmpz00_5219,
																												BFALSE);
																										}
																									}
																									{	/* Unsafe/pregexp.scm 381 */
																										obj_t BgL_arg1606z00_1617;

																										BgL_arg1606z00_1617 =
																											CDR(
																											((obj_t)
																												BgL_newzd2vvzd2_1584));
																										{	/* Unsafe/pregexp.scm 381 */
																											obj_t BgL_auxz00_5226;
																											obj_t BgL_tmpz00_5224;

																											BgL_auxz00_5226 =
																												BINT(BgL_iz00_1603);
																											BgL_tmpz00_5224 =
																												((obj_t)
																												BgL_arg1606z00_1617);
																											SET_CAR(BgL_tmpz00_5224,
																												BgL_auxz00_5226);
																										}
																									}
																								}
																						}
																				}
																			}
																	}
																}
																return BgL_newzd2vvzd2_1584;
															}
														}
													}
													break;
												default:
													return BgL_vvz00_33;
												}
										}
								}
							}
					}
				}
			}
		}

	}



/* pregexp-read-nums */
	obj_t BGl_pregexpzd2readzd2numsz00zz__regexpz00(obj_t BgL_sz00_36,
		long BgL_iz00_37, long BgL_nz00_38)
	{
		{	/* Unsafe/pregexp.scm 387 */
			{
				obj_t BgL_pz00_1632;
				obj_t BgL_qz00_1633;
				long BgL_kz00_1634;
				long BgL_readingz00_1635;

				BgL_pz00_1632 = BNIL;
				BgL_qz00_1633 = BNIL;
				BgL_kz00_1634 = BgL_iz00_37;
				BgL_readingz00_1635 = 1L;
			BgL_zc3z04anonymousza31614ze3z87_1636:
				if ((BgL_kz00_1634 >= BgL_nz00_38))
					{	/* Unsafe/pregexp.scm 392 */
						BGl_errorz00zz__errorz00(BGl_string2500z00zz__regexpz00,
							BGl_symbol2555z00zz__regexpz00, BUNSPEC);
					}
				else
					{	/* Unsafe/pregexp.scm 392 */
						BFALSE;
					}
				{	/* Unsafe/pregexp.scm 393 */
					unsigned char BgL_cz00_1639;

					BgL_cz00_1639 = STRING_REF(BgL_sz00_36, BgL_kz00_1634);
					if (isdigit(BgL_cz00_1639))
						{	/* Unsafe/pregexp.scm 394 */
							if ((BgL_readingz00_1635 == 1L))
								{	/* Unsafe/pregexp.scm 396 */
									obj_t BgL_arg1619z00_1642;
									long BgL_arg1620z00_1643;

									BgL_arg1619z00_1642 =
										MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1639), BgL_pz00_1632);
									BgL_arg1620z00_1643 = (BgL_kz00_1634 + 1L);
									{
										long BgL_readingz00_5243;
										long BgL_kz00_5242;
										obj_t BgL_pz00_5241;

										BgL_pz00_5241 = BgL_arg1619z00_1642;
										BgL_kz00_5242 = BgL_arg1620z00_1643;
										BgL_readingz00_5243 = 1L;
										BgL_readingz00_1635 = BgL_readingz00_5243;
										BgL_kz00_1634 = BgL_kz00_5242;
										BgL_pz00_1632 = BgL_pz00_5241;
										goto BgL_zc3z04anonymousza31614ze3z87_1636;
									}
								}
							else
								{	/* Unsafe/pregexp.scm 397 */
									obj_t BgL_arg1621z00_1644;
									long BgL_arg1622z00_1645;

									BgL_arg1621z00_1644 =
										MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1639), BgL_qz00_1633);
									BgL_arg1622z00_1645 = (BgL_kz00_1634 + 1L);
									{
										long BgL_readingz00_5249;
										long BgL_kz00_5248;
										obj_t BgL_qz00_5247;

										BgL_qz00_5247 = BgL_arg1621z00_1644;
										BgL_kz00_5248 = BgL_arg1622z00_1645;
										BgL_readingz00_5249 = 2L;
										BgL_readingz00_1635 = BgL_readingz00_5249;
										BgL_kz00_1634 = BgL_kz00_5248;
										BgL_qz00_1633 = BgL_qz00_5247;
										goto BgL_zc3z04anonymousza31614ze3z87_1636;
									}
								}
						}
					else
						{	/* Unsafe/pregexp.scm 398 */
							bool_t BgL_test2768z00_5250;

							if (isspace(BgL_cz00_1639))
								{	/* Unsafe/pregexp.scm 398 */
									if (BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00)
										{	/* Unsafe/pregexp.scm 398 */
											BgL_test2768z00_5250 = ((bool_t) 0);
										}
									else
										{	/* Unsafe/pregexp.scm 398 */
											BgL_test2768z00_5250 = ((bool_t) 1);
										}
								}
							else
								{	/* Unsafe/pregexp.scm 398 */
									BgL_test2768z00_5250 = ((bool_t) 0);
								}
							if (BgL_test2768z00_5250)
								{
									long BgL_kz00_5254;

									BgL_kz00_5254 = (BgL_kz00_1634 + 1L);
									BgL_kz00_1634 = BgL_kz00_5254;
									goto BgL_zc3z04anonymousza31614ze3z87_1636;
								}
							else
								{	/* Unsafe/pregexp.scm 400 */
									bool_t BgL_test2771z00_5256;

									if ((BgL_cz00_1639 == ((unsigned char) ',')))
										{	/* Unsafe/pregexp.scm 400 */
											BgL_test2771z00_5256 = (BgL_readingz00_1635 == 1L);
										}
									else
										{	/* Unsafe/pregexp.scm 400 */
											BgL_test2771z00_5256 = ((bool_t) 0);
										}
									if (BgL_test2771z00_5256)
										{
											long BgL_readingz00_5262;
											long BgL_kz00_5260;

											BgL_kz00_5260 = (BgL_kz00_1634 + 1L);
											BgL_readingz00_5262 = 2L;
											BgL_readingz00_1635 = BgL_readingz00_5262;
											BgL_kz00_1634 = BgL_kz00_5260;
											goto BgL_zc3z04anonymousza31614ze3z87_1636;
										}
									else
										{	/* Unsafe/pregexp.scm 400 */
											if ((BgL_cz00_1639 == ((unsigned char) '}')))
												{	/* Unsafe/pregexp.scm 403 */
													obj_t BgL_pz00_1653;
													obj_t BgL_qz00_1654;

													{	/* Unsafe/pregexp.scm 403 */
														obj_t BgL_arg1644z00_1666;

														BgL_arg1644z00_1666 =
															BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
															(BGl_pregexpzd2reversez12zc0zz__regexpz00
															(BgL_pz00_1632));
														{	/* Ieee/number.scm 175 */

															BgL_pz00_1653 =
																BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																(BgL_arg1644z00_1666, BINT(10L));
													}}
													{	/* Unsafe/pregexp.scm 404 */
														obj_t BgL_arg1646z00_1670;

														BgL_arg1646z00_1670 =
															BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
															(BGl_pregexpzd2reversez12zc0zz__regexpz00
															(BgL_qz00_1633));
														{	/* Ieee/number.scm 175 */

															BgL_qz00_1654 =
																BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																(BgL_arg1646z00_1670, BINT(10L));
													}}
													{	/* Unsafe/pregexp.scm 405 */
														bool_t BgL_test2774z00_5273;

														if (CBOOL(BgL_pz00_1653))
															{	/* Unsafe/pregexp.scm 405 */
																BgL_test2774z00_5273 = ((bool_t) 0);
															}
														else
															{	/* Unsafe/pregexp.scm 405 */
																BgL_test2774z00_5273 =
																	(BgL_readingz00_1635 == 1L);
															}
														if (BgL_test2774z00_5273)
															{	/* Unsafe/pregexp.scm 405 */
																obj_t BgL_list1631z00_1656;

																{	/* Unsafe/pregexp.scm 405 */
																	obj_t BgL_arg1634z00_1657;

																	{	/* Unsafe/pregexp.scm 405 */
																		obj_t BgL_arg1636z00_1658;

																		BgL_arg1636z00_1658 =
																			MAKE_YOUNG_PAIR(BINT(BgL_kz00_1634),
																			BNIL);
																		BgL_arg1634z00_1657 =
																			MAKE_YOUNG_PAIR(BFALSE,
																			BgL_arg1636z00_1658);
																	}
																	BgL_list1631z00_1656 =
																		MAKE_YOUNG_PAIR(BINT(0L),
																		BgL_arg1634z00_1657);
																}
																return BgL_list1631z00_1656;
															}
														else
															{	/* Unsafe/pregexp.scm 405 */
																if ((BgL_readingz00_1635 == 1L))
																	{	/* Unsafe/pregexp.scm 406 */
																		obj_t BgL_list1638z00_1660;

																		{	/* Unsafe/pregexp.scm 406 */
																			obj_t BgL_arg1639z00_1661;

																			{	/* Unsafe/pregexp.scm 406 */
																				obj_t BgL_arg1640z00_1662;

																				BgL_arg1640z00_1662 =
																					MAKE_YOUNG_PAIR(BINT(BgL_kz00_1634),
																					BNIL);
																				BgL_arg1639z00_1661 =
																					MAKE_YOUNG_PAIR(BgL_pz00_1653,
																					BgL_arg1640z00_1662);
																			}
																			BgL_list1638z00_1660 =
																				MAKE_YOUNG_PAIR(BgL_pz00_1653,
																				BgL_arg1639z00_1661);
																		}
																		return BgL_list1638z00_1660;
																	}
																else
																	{	/* Unsafe/pregexp.scm 407 */
																		obj_t BgL_list1641z00_1663;

																		{	/* Unsafe/pregexp.scm 407 */
																			obj_t BgL_arg1642z00_1664;

																			{	/* Unsafe/pregexp.scm 407 */
																				obj_t BgL_arg1643z00_1665;

																				BgL_arg1643z00_1665 =
																					MAKE_YOUNG_PAIR(BINT(BgL_kz00_1634),
																					BNIL);
																				BgL_arg1642z00_1664 =
																					MAKE_YOUNG_PAIR(BgL_qz00_1654,
																					BgL_arg1643z00_1665);
																			}
																			BgL_list1641z00_1663 =
																				MAKE_YOUNG_PAIR(BgL_pz00_1653,
																				BgL_arg1642z00_1664);
																		}
																		return BgL_list1641z00_1663;
																	}
															}
													}
												}
											else
												{	/* Unsafe/pregexp.scm 402 */
													return BFALSE;
												}
										}
								}
						}
				}
			}
		}

	}



/* pregexp-read-char-list */
	obj_t BGl_pregexpzd2readzd2charzd2listzd2zz__regexpz00(obj_t BgL_sz00_40,
		long BgL_iz00_41, long BgL_nz00_42)
	{
		{	/* Unsafe/pregexp.scm 417 */
			{
				obj_t BgL_rz00_1680;
				obj_t BgL_iz00_1681;

				BgL_rz00_1680 = BNIL;
				BgL_iz00_1681 = BINT(BgL_iz00_41);
			BgL_zc3z04anonymousza31650ze3z87_1682:
				if (((long) CINT(BgL_iz00_1681) >= BgL_nz00_42))
					{	/* Unsafe/pregexp.scm 421 */
						obj_t BgL_list1652z00_1684;

						BgL_list1652z00_1684 =
							MAKE_YOUNG_PAIR(BGl_string2557z00zz__regexpz00, BNIL);
						return
							BGl_errorz00zz__errorz00(BGl_string2500z00zz__regexpz00,
							BGl_symbol2558z00zz__regexpz00, CAR(BgL_list1652z00_1684));
					}
				else
					{	/* Unsafe/pregexp.scm 423 */
						unsigned char BgL_cz00_1685;

						BgL_cz00_1685 = STRING_REF(BgL_sz00_40, (long) CINT(BgL_iz00_1681));
						{

							switch (BgL_cz00_1685)
								{
								case ((unsigned char) ']'):

									if (NULLP(BgL_rz00_1680))
										{	/* Unsafe/pregexp.scm 426 */
											obj_t BgL_arg1656z00_1690;
											long BgL_arg1657z00_1691;

											BgL_arg1656z00_1690 =
												MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1685), BgL_rz00_1680);
											BgL_arg1657z00_1691 = ((long) CINT(BgL_iz00_1681) + 1L);
											{
												obj_t BgL_iz00_5307;
												obj_t BgL_rz00_5306;

												BgL_rz00_5306 = BgL_arg1656z00_1690;
												BgL_iz00_5307 = BINT(BgL_arg1657z00_1691);
												BgL_iz00_1681 = BgL_iz00_5307;
												BgL_rz00_1680 = BgL_rz00_5306;
												goto BgL_zc3z04anonymousza31650ze3z87_1682;
											}
										}
									else
										{	/* Unsafe/pregexp.scm 427 */
											obj_t BgL_arg1658z00_1692;
											long BgL_arg1661z00_1693;

											{	/* Unsafe/pregexp.scm 427 */
												obj_t BgL_arg1664z00_1696;

												BgL_arg1664z00_1696 =
													BGl_pregexpzd2reversez12zc0zz__regexpz00
													(BgL_rz00_1680);
												BgL_arg1658z00_1692 =
													MAKE_YOUNG_PAIR(BGl_keyword2560z00zz__regexpz00,
													BgL_arg1664z00_1696);
											}
											BgL_arg1661z00_1693 = ((long) CINT(BgL_iz00_1681) + 1L);
											{	/* Unsafe/pregexp.scm 427 */
												obj_t BgL_list1662z00_1694;

												{	/* Unsafe/pregexp.scm 427 */
													obj_t BgL_arg1663z00_1695;

													BgL_arg1663z00_1695 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1661z00_1693), BNIL);
													BgL_list1662z00_1694 =
														MAKE_YOUNG_PAIR(BgL_arg1658z00_1692,
														BgL_arg1663z00_1695);
												}
												return BgL_list1662z00_1694;
											}
										}
									break;
								case ((unsigned char) '\\'):

									{	/* Unsafe/pregexp.scm 430 */
										obj_t BgL_charzd2izd2_1697;

										BgL_charzd2izd2_1697 =
											BGl_pregexpzd2readzd2escapedzd2charzd2zz__regexpz00
											(BgL_sz00_40, BgL_iz00_1681, BgL_nz00_42);
										if (CBOOL(BgL_charzd2izd2_1697))
											{	/* Unsafe/pregexp.scm 431 */
												obj_t BgL_arg1667z00_1698;
												obj_t BgL_arg1668z00_1699;

												{	/* Unsafe/pregexp.scm 431 */
													obj_t BgL_arg1669z00_1700;

													BgL_arg1669z00_1700 =
														CAR(((obj_t) BgL_charzd2izd2_1697));
													BgL_arg1667z00_1698 =
														MAKE_YOUNG_PAIR(BgL_arg1669z00_1700, BgL_rz00_1680);
												}
												{	/* Unsafe/pregexp.scm 431 */
													obj_t BgL_pairz00_3239;

													BgL_pairz00_3239 =
														CDR(((obj_t) BgL_charzd2izd2_1697));
													BgL_arg1668z00_1699 = CAR(BgL_pairz00_3239);
												}
												{
													obj_t BgL_iz00_5326;
													obj_t BgL_rz00_5325;

													BgL_rz00_5325 = BgL_arg1667z00_1698;
													BgL_iz00_5326 = BgL_arg1668z00_1699;
													BgL_iz00_1681 = BgL_iz00_5326;
													BgL_rz00_1680 = BgL_rz00_5325;
													goto BgL_zc3z04anonymousza31650ze3z87_1682;
												}
											}
										else
											{	/* Unsafe/pregexp.scm 432 */
												obj_t BgL_list1670z00_1701;

												BgL_list1670z00_1701 =
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')), BNIL);
												return
													BGl_errorz00zz__errorz00
													(BGl_string2500z00zz__regexpz00,
													BGl_symbol2558z00zz__regexpz00,
													CAR(BgL_list1670z00_1701));
											}
									}
									break;
								case ((unsigned char) '-'):

									{	/* Unsafe/pregexp.scm 433 */
										bool_t BgL_test2780z00_5331;

										if (NULLP(BgL_rz00_1680))
											{	/* Unsafe/pregexp.scm 433 */
												BgL_test2780z00_5331 = ((bool_t) 1);
											}
										else
											{	/* Unsafe/pregexp.scm 434 */
												long BgL_izb21zb2_1723;

												BgL_izb21zb2_1723 = ((long) CINT(BgL_iz00_1681) + 1L);
												if ((BgL_izb21zb2_1723 < BgL_nz00_42))
													{	/* Unsafe/pregexp.scm 435 */
														BgL_test2780z00_5331 =
															(STRING_REF(BgL_sz00_40,
																BgL_izb21zb2_1723) == ((unsigned char) ']'));
													}
												else
													{	/* Unsafe/pregexp.scm 435 */
														BgL_test2780z00_5331 = ((bool_t) 0);
													}
											}
										if (BgL_test2780z00_5331)
											{	/* Unsafe/pregexp.scm 437 */
												obj_t BgL_arg1678z00_1707;
												long BgL_arg1681z00_1708;

												BgL_arg1678z00_1707 =
													MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1685), BgL_rz00_1680);
												BgL_arg1681z00_1708 = ((long) CINT(BgL_iz00_1681) + 1L);
												{
													obj_t BgL_iz00_5345;
													obj_t BgL_rz00_5344;

													BgL_rz00_5344 = BgL_arg1678z00_1707;
													BgL_iz00_5345 = BINT(BgL_arg1681z00_1708);
													BgL_iz00_1681 = BgL_iz00_5345;
													BgL_rz00_1680 = BgL_rz00_5344;
													goto BgL_zc3z04anonymousza31650ze3z87_1682;
												}
											}
										else
											{	/* Unsafe/pregexp.scm 438 */
												obj_t BgL_czd2prevzd2_1709;

												BgL_czd2prevzd2_1709 = CAR(((obj_t) BgL_rz00_1680));
												if (CHARP(BgL_czd2prevzd2_1709))
													{	/* Unsafe/pregexp.scm 441 */
														obj_t BgL_arg1684z00_1711;
														long BgL_arg1685z00_1712;

														{	/* Unsafe/pregexp.scm 441 */
															obj_t BgL_arg1688z00_1713;
															obj_t BgL_arg1689z00_1714;

															{	/* Unsafe/pregexp.scm 441 */
																unsigned char BgL_arg1691z00_1715;

																BgL_arg1691z00_1715 =
																	STRING_REF(BgL_sz00_40,
																	((long) CINT(BgL_iz00_1681) + 1L));
																{	/* Unsafe/pregexp.scm 440 */
																	obj_t BgL_list1692z00_1716;

																	{	/* Unsafe/pregexp.scm 440 */
																		obj_t BgL_arg1699z00_1717;

																		{	/* Unsafe/pregexp.scm 440 */
																			obj_t BgL_arg1700z00_1718;

																			BgL_arg1700z00_1718 =
																				MAKE_YOUNG_PAIR(BCHAR
																				(BgL_arg1691z00_1715), BNIL);
																			BgL_arg1699z00_1717 =
																				MAKE_YOUNG_PAIR(BgL_czd2prevzd2_1709,
																				BgL_arg1700z00_1718);
																		}
																		BgL_list1692z00_1716 =
																			MAKE_YOUNG_PAIR
																			(BGl_keyword2562z00zz__regexpz00,
																			BgL_arg1699z00_1717);
																	}
																	BgL_arg1688z00_1713 = BgL_list1692z00_1716;
															}}
															BgL_arg1689z00_1714 =
																CDR(((obj_t) BgL_rz00_1680));
															BgL_arg1684z00_1711 =
																MAKE_YOUNG_PAIR(BgL_arg1688z00_1713,
																BgL_arg1689z00_1714);
														}
														BgL_arg1685z00_1712 =
															((long) CINT(BgL_iz00_1681) + 2L);
														{
															obj_t BgL_iz00_5364;
															obj_t BgL_rz00_5363;

															BgL_rz00_5363 = BgL_arg1684z00_1711;
															BgL_iz00_5364 = BINT(BgL_arg1685z00_1712);
															BgL_iz00_1681 = BgL_iz00_5364;
															BgL_rz00_1680 = BgL_rz00_5363;
															goto BgL_zc3z04anonymousza31650ze3z87_1682;
														}
													}
												else
													{	/* Unsafe/pregexp.scm 443 */
														obj_t BgL_arg1702z00_1720;
														long BgL_arg1703z00_1721;

														BgL_arg1702z00_1720 =
															MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1685),
															BgL_rz00_1680);
														BgL_arg1703z00_1721 =
															((long) CINT(BgL_iz00_1681) + 1L);
														{
															obj_t BgL_iz00_5371;
															obj_t BgL_rz00_5370;

															BgL_rz00_5370 = BgL_arg1702z00_1720;
															BgL_iz00_5371 = BINT(BgL_arg1703z00_1721);
															BgL_iz00_1681 = BgL_iz00_5371;
															BgL_rz00_1680 = BgL_rz00_5370;
															goto BgL_zc3z04anonymousza31650ze3z87_1682;
														}
													}
											}
									}
									break;
								case ((unsigned char) '['):

									if (
										(STRING_REF(BgL_sz00_40,
												((long) CINT(BgL_iz00_1681) + 1L)) ==
											((unsigned char) ':')))
										{	/* Unsafe/pregexp.scm 445 */
											obj_t BgL_posixzd2charzd2classzd2izd2_1729;

											BgL_posixzd2charzd2classzd2izd2_1729 =
												BGl_pregexpzd2readzd2posixzd2charzd2classz00zz__regexpz00
												(BgL_sz00_40, ((long) CINT(BgL_iz00_1681) + 2L),
												BgL_nz00_42);
											{	/* Unsafe/pregexp.scm 447 */
												obj_t BgL_arg1708z00_1730;
												obj_t BgL_arg1709z00_1731;

												{	/* Unsafe/pregexp.scm 447 */
													obj_t BgL_arg1710z00_1732;

													BgL_arg1710z00_1732 =
														CAR(((obj_t) BgL_posixzd2charzd2classzd2izd2_1729));
													BgL_arg1708z00_1730 =
														MAKE_YOUNG_PAIR(BgL_arg1710z00_1732, BgL_rz00_1680);
												}
												{	/* Unsafe/pregexp.scm 448 */
													obj_t BgL_pairz00_3270;

													BgL_pairz00_3270 =
														CDR(((obj_t) BgL_posixzd2charzd2classzd2izd2_1729));
													BgL_arg1709z00_1731 = CAR(BgL_pairz00_3270);
												}
												{
													obj_t BgL_iz00_5388;
													obj_t BgL_rz00_5387;

													BgL_rz00_5387 = BgL_arg1708z00_1730;
													BgL_iz00_5388 = BgL_arg1709z00_1731;
													BgL_iz00_1681 = BgL_iz00_5388;
													BgL_rz00_1680 = BgL_rz00_5387;
													goto BgL_zc3z04anonymousza31650ze3z87_1682;
												}
											}
										}
									else
										{	/* Unsafe/pregexp.scm 449 */
											obj_t BgL_arg1714z00_1734;
											long BgL_arg1715z00_1735;

											BgL_arg1714z00_1734 =
												MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1685), BgL_rz00_1680);
											BgL_arg1715z00_1735 = ((long) CINT(BgL_iz00_1681) + 1L);
											{
												obj_t BgL_iz00_5394;
												obj_t BgL_rz00_5393;

												BgL_rz00_5393 = BgL_arg1714z00_1734;
												BgL_iz00_5394 = BINT(BgL_arg1715z00_1735);
												BgL_iz00_1681 = BgL_iz00_5394;
												BgL_rz00_1680 = BgL_rz00_5393;
												goto BgL_zc3z04anonymousza31650ze3z87_1682;
											}
										}
									break;
								default:
									{	/* Unsafe/pregexp.scm 450 */
										obj_t BgL_arg1720z00_1738;
										long BgL_arg1722z00_1739;

										BgL_arg1720z00_1738 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1685), BgL_rz00_1680);
										BgL_arg1722z00_1739 = ((long) CINT(BgL_iz00_1681) + 1L);
										{
											obj_t BgL_iz00_5401;
											obj_t BgL_rz00_5400;

											BgL_rz00_5400 = BgL_arg1720z00_1738;
											BgL_iz00_5401 = BINT(BgL_arg1722z00_1739);
											BgL_iz00_1681 = BgL_iz00_5401;
											BgL_rz00_1680 = BgL_rz00_5400;
											goto BgL_zc3z04anonymousza31650ze3z87_1682;
										}
									}
								}
						}
					}
			}
		}

	}



/* pregexp-at-word-boundary? */
	obj_t BGl_pregexpzd2atzd2wordzd2boundaryzf3z21zz__regexpz00(obj_t BgL_sz00_50,
		obj_t BgL_iz00_51, obj_t BgL_nz00_52)
	{
		{	/* Unsafe/pregexp.scm 473 */
			{	/* Unsafe/pregexp.scm 475 */
				bool_t BgL__ortest_1085z00_1759;

				BgL__ortest_1085z00_1759 = ((long) CINT(BgL_iz00_51) == 0L);
				if (BgL__ortest_1085z00_1759)
					{	/* Unsafe/pregexp.scm 475 */
						return BBOOL(BgL__ortest_1085z00_1759);
					}
				else
					{	/* Unsafe/pregexp.scm 475 */
						bool_t BgL__ortest_1086z00_1760;

						BgL__ortest_1086z00_1760 =
							((long) CINT(BgL_iz00_51) >= (long) CINT(BgL_nz00_52));
						if (BgL__ortest_1086z00_1760)
							{	/* Unsafe/pregexp.scm 475 */
								return BBOOL(BgL__ortest_1086z00_1760);
							}
						else
							{	/* Unsafe/pregexp.scm 476 */
								unsigned char BgL_czf2izf2_1761;
								unsigned char BgL_czf2izd21z20_1762;

								BgL_czf2izf2_1761 =
									STRING_REF(((obj_t) BgL_sz00_50), (long) CINT(BgL_iz00_51));
								BgL_czf2izd21z20_1762 =
									STRING_REF(
									((obj_t) BgL_sz00_50), ((long) CINT(BgL_iz00_51) - 1L));
								{	/* Unsafe/pregexp.scm 478 */
									obj_t BgL_czf2izf2wzf3zf3_1763;
									obj_t BgL_czf2izd21zf2wzf3z21_1764;

									BgL_czf2izf2wzf3zf3_1763 =
										BGl_pregexpzd2checkzd2ifzd2inzd2charzd2classzf3z21zz__regexpz00
										(BgL_czf2izf2_1761, BGl_keyword2511z00zz__regexpz00);
									BgL_czf2izd21zf2wzf3z21_1764 =
										BGl_pregexpzd2checkzd2ifzd2inzd2charzd2classzf3z21zz__regexpz00
										(BgL_czf2izd21z20_1762, BGl_keyword2511z00zz__regexpz00);
									{	/* Unsafe/pregexp.scm 482 */
										bool_t BgL__ortest_1087z00_1765;

										if (CBOOL(BgL_czf2izf2wzf3zf3_1763))
											{	/* Unsafe/pregexp.scm 482 */
												if (CBOOL(BgL_czf2izd21zf2wzf3z21_1764))
													{	/* Unsafe/pregexp.scm 482 */
														BgL__ortest_1087z00_1765 = ((bool_t) 0);
													}
												else
													{	/* Unsafe/pregexp.scm 482 */
														BgL__ortest_1087z00_1765 = ((bool_t) 1);
													}
											}
										else
											{	/* Unsafe/pregexp.scm 482 */
												BgL__ortest_1087z00_1765 = ((bool_t) 0);
											}
										if (BgL__ortest_1087z00_1765)
											{	/* Unsafe/pregexp.scm 482 */
												return BBOOL(BgL__ortest_1087z00_1765);
											}
										else
											{	/* Unsafe/pregexp.scm 482 */
												if (CBOOL(BgL_czf2izf2wzf3zf3_1763))
													{	/* Unsafe/pregexp.scm 483 */
														return BFALSE;
													}
												else
													{	/* Unsafe/pregexp.scm 483 */
														return BgL_czf2izd21zf2wzf3z21_1764;
													}
											}
									}
								}
							}
					}
			}
		}

	}



/* pregexp-check-if-in-char-class? */
	obj_t BGl_pregexpzd2checkzd2ifzd2inzd2charzd2classzf3z21zz__regexpz00(unsigned
		char BgL_cz00_53, obj_t BgL_charzd2classzd2_54)
	{
		{	/* Unsafe/pregexp.scm 485 */
			if ((BgL_charzd2classzd2_54 == BGl_keyword2490z00zz__regexpz00))
				{	/* Unsafe/pregexp.scm 487 */
					if ((BgL_cz00_53 == ((unsigned char) 10)))
						{	/* Unsafe/pregexp.scm 488 */
							return BFALSE;
						}
					else
						{	/* Unsafe/pregexp.scm 488 */
							return BTRUE;
						}
				}
			else
				{	/* Unsafe/pregexp.scm 487 */
					if ((BgL_charzd2classzd2_54 == BGl_keyword2564z00zz__regexpz00))
						{	/* Unsafe/pregexp.scm 490 */
							bool_t BgL__ortest_1090z00_1773;

							BgL__ortest_1090z00_1773 = isalpha(BgL_cz00_53);
							if (BgL__ortest_1090z00_1773)
								{	/* Unsafe/pregexp.scm 490 */
									return BBOOL(BgL__ortest_1090z00_1773);
								}
							else
								{	/* Unsafe/pregexp.scm 490 */
									return BBOOL(isdigit(BgL_cz00_53));
								}
						}
					else
						{	/* Unsafe/pregexp.scm 487 */
							if ((BgL_charzd2classzd2_54 == BGl_keyword2566z00zz__regexpz00))
								{	/* Unsafe/pregexp.scm 487 */
									return BBOOL(isalpha(BgL_cz00_53));
								}
							else
								{	/* Unsafe/pregexp.scm 487 */
									if (
										(BgL_charzd2classzd2_54 == BGl_keyword2568z00zz__regexpz00))
										{	/* Unsafe/pregexp.scm 487 */
											return BBOOL(((BgL_cz00_53) < 128L));
										}
									else
										{	/* Unsafe/pregexp.scm 487 */
											if (
												(BgL_charzd2classzd2_54 ==
													BGl_keyword2570z00zz__regexpz00))
												{	/* Unsafe/pregexp.scm 493 */
													bool_t BgL__ortest_1091z00_1778;

													BgL__ortest_1091z00_1778 =
														(BgL_cz00_53 == ((unsigned char) ' '));
													if (BgL__ortest_1091z00_1778)
														{	/* Unsafe/pregexp.scm 493 */
															return BBOOL(BgL__ortest_1091z00_1778);
														}
													else
														{	/* Unsafe/pregexp.scm 493 */
															unsigned char BgL_char2z00_3322;

															BgL_char2z00_3322 =
																BGl_za2pregexpzd2tabzd2charza2z00zz__regexpz00;
															return BBOOL((BgL_cz00_53 == BgL_char2z00_3322));
														}
												}
											else
												{	/* Unsafe/pregexp.scm 487 */
													if (
														(BgL_charzd2classzd2_54 ==
															BGl_keyword2572z00zz__regexpz00))
														{	/* Unsafe/pregexp.scm 487 */
															return BBOOL(((BgL_cz00_53) < 32L));
														}
													else
														{	/* Unsafe/pregexp.scm 487 */
															if (
																(BgL_charzd2classzd2_54 ==
																	BGl_keyword2505z00zz__regexpz00))
																{	/* Unsafe/pregexp.scm 487 */
																	return BBOOL(isdigit(BgL_cz00_53));
																}
															else
																{	/* Unsafe/pregexp.scm 487 */
																	if (
																		(BgL_charzd2classzd2_54 ==
																			BGl_keyword2574z00zz__regexpz00))
																		{	/* Unsafe/pregexp.scm 487 */
																			if (((BgL_cz00_53) >= 32L))
																				{	/* Unsafe/pregexp.scm 496 */
																					if (isspace(BgL_cz00_53))
																						{	/* Unsafe/pregexp.scm 497 */
																							return BFALSE;
																						}
																					else
																						{	/* Unsafe/pregexp.scm 497 */
																							return BTRUE;
																						}
																				}
																			else
																				{	/* Unsafe/pregexp.scm 496 */
																					return BFALSE;
																				}
																		}
																	else
																		{	/* Unsafe/pregexp.scm 487 */
																			if (
																				(BgL_charzd2classzd2_54 ==
																					BGl_keyword2576z00zz__regexpz00))
																				{	/* Unsafe/pregexp.scm 487 */
																					return BBOOL(islower(BgL_cz00_53));
																				}
																			else
																				{	/* Unsafe/pregexp.scm 487 */
																					if (
																						(BgL_charzd2classzd2_54 ==
																							BGl_keyword2578z00zz__regexpz00))
																						{	/* Unsafe/pregexp.scm 487 */
																							return
																								BBOOL(((BgL_cz00_53) >= 32L));
																						}
																					else
																						{	/* Unsafe/pregexp.scm 487 */
																							if (
																								(BgL_charzd2classzd2_54 ==
																									BGl_keyword2580z00zz__regexpz00))
																								{	/* Unsafe/pregexp.scm 487 */
																									if (((BgL_cz00_53) >= 32L))
																										{	/* Unsafe/pregexp.scm 500 */
																											if (isspace(BgL_cz00_53))
																												{	/* Unsafe/pregexp.scm 501 */
																													return BFALSE;
																												}
																											else
																												{	/* Unsafe/pregexp.scm 501 */
																													if (isalpha
																														(BgL_cz00_53))
																														{	/* Unsafe/pregexp.scm 502 */
																															return BFALSE;
																														}
																													else
																														{	/* Unsafe/pregexp.scm 502 */
																															if (isdigit
																																(BgL_cz00_53))
																																{	/* Unsafe/pregexp.scm 503 */
																																	return BFALSE;
																																}
																															else
																																{	/* Unsafe/pregexp.scm 503 */
																																	return BTRUE;
																																}
																														}
																												}
																										}
																									else
																										{	/* Unsafe/pregexp.scm 500 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Unsafe/pregexp.scm 487 */
																									if (
																										(BgL_charzd2classzd2_54 ==
																											BGl_keyword2508z00zz__regexpz00))
																										{	/* Unsafe/pregexp.scm 487 */
																											return
																												BBOOL(isspace
																												(BgL_cz00_53));
																										}
																									else
																										{	/* Unsafe/pregexp.scm 487 */
																											if (
																												(BgL_charzd2classzd2_54
																													==
																													BGl_keyword2582z00zz__regexpz00))
																												{	/* Unsafe/pregexp.scm 487 */
																													return
																														BBOOL(isupper
																														(BgL_cz00_53));
																												}
																											else
																												{	/* Unsafe/pregexp.scm 487 */
																													if (
																														(BgL_charzd2classzd2_54
																															==
																															BGl_keyword2511z00zz__regexpz00))
																														{	/* Unsafe/pregexp.scm 506 */
																															bool_t
																																BgL__ortest_1096z00_1798;
																															BgL__ortest_1096z00_1798
																																=
																																isalpha
																																(BgL_cz00_53);
																															if (BgL__ortest_1096z00_1798)
																																{	/* Unsafe/pregexp.scm 506 */
																																	return
																																		BBOOL
																																		(BgL__ortest_1096z00_1798);
																																}
																															else
																																{	/* Unsafe/pregexp.scm 507 */
																																	bool_t
																																		BgL__ortest_1097z00_1799;
																																	BgL__ortest_1097z00_1799
																																		=
																																		isdigit
																																		(BgL_cz00_53);
																																	if (BgL__ortest_1097z00_1799)
																																		{	/* Unsafe/pregexp.scm 507 */
																																			return
																																				BBOOL
																																				(BgL__ortest_1097z00_1799);
																																		}
																																	else
																																		{	/* Unsafe/pregexp.scm 507 */
																																			return
																																				BBOOL(
																																				(BgL_cz00_53
																																					==
																																					((unsigned char) '_')));
																														}}}
																													else
																														{	/* Unsafe/pregexp.scm 487 */
																															if (
																																(BgL_charzd2classzd2_54
																																	==
																																	BGl_keyword2584z00zz__regexpz00))
																																{	/* Unsafe/pregexp.scm 509 */
																																	bool_t
																																		BgL__ortest_1098z00_1801;
																																	BgL__ortest_1098z00_1801
																																		=
																																		isdigit
																																		(BgL_cz00_53);
																																	if (BgL__ortest_1098z00_1801)
																																		{	/* Unsafe/pregexp.scm 509 */
																																			return
																																				BBOOL
																																				(BgL__ortest_1098z00_1801);
																																		}
																																	else
																																		{	/* Unsafe/pregexp.scm 510 */
																																			bool_t
																																				BgL__ortest_1099z00_1802;
																																			BgL__ortest_1099z00_1802
																																				=
																																				(toupper
																																				(BgL_cz00_53)
																																				==
																																				toupper(
																																					((unsigned char) 'a')));
																																			if (BgL__ortest_1099z00_1802)
																																				{	/* Unsafe/pregexp.scm 510 */
																																					return
																																						BBOOL
																																						(BgL__ortest_1099z00_1802);
																																				}
																																			else
																																				{	/* Unsafe/pregexp.scm 510 */
																																					bool_t
																																						BgL__ortest_1100z00_1803;
																																					BgL__ortest_1100z00_1803
																																						=
																																						(toupper
																																						(BgL_cz00_53)
																																						==
																																						toupper
																																						(((unsigned char) 'b')));
																																					if (BgL__ortest_1100z00_1803)
																																						{	/* Unsafe/pregexp.scm 510 */
																																							return
																																								BBOOL
																																								(BgL__ortest_1100z00_1803);
																																						}
																																					else
																																						{	/* Unsafe/pregexp.scm 511 */
																																							bool_t
																																								BgL__ortest_1101z00_1804;
																																							BgL__ortest_1101z00_1804
																																								=
																																								(toupper
																																								(BgL_cz00_53)
																																								==
																																								toupper
																																								(((unsigned char) 'c')));
																																							if (BgL__ortest_1101z00_1804)
																																								{	/* Unsafe/pregexp.scm 511 */
																																									return
																																										BBOOL
																																										(BgL__ortest_1101z00_1804);
																																								}
																																							else
																																								{	/* Unsafe/pregexp.scm 511 */
																																									bool_t
																																										BgL__ortest_1102z00_1805;
																																									BgL__ortest_1102z00_1805
																																										=
																																										(toupper
																																										(BgL_cz00_53)
																																										==
																																										toupper
																																										(((unsigned char) 'd')));
																																									if (BgL__ortest_1102z00_1805)
																																										{	/* Unsafe/pregexp.scm 511 */
																																											return
																																												BBOOL
																																												(BgL__ortest_1102z00_1805);
																																										}
																																									else
																																										{	/* Unsafe/pregexp.scm 512 */
																																											bool_t
																																												BgL__ortest_1103z00_1806;
																																											BgL__ortest_1103z00_1806
																																												=
																																												(toupper
																																												(BgL_cz00_53)
																																												==
																																												toupper
																																												(((unsigned char) 'e')));
																																											if (BgL__ortest_1103z00_1806)
																																												{	/* Unsafe/pregexp.scm 512 */
																																													return
																																														BBOOL
																																														(BgL__ortest_1103z00_1806);
																																												}
																																											else
																																												{	/* Unsafe/pregexp.scm 512 */
																																													return
																																														BBOOL
																																														(
																																														(toupper
																																															(BgL_cz00_53)
																																															==
																																															toupper
																																															(((unsigned char) 'f'))));
																																}}}}}}}
																															else
																																{	/* Unsafe/pregexp.scm 513 */
																																	obj_t
																																		BgL_list1759z00_1807;
																																	BgL_list1759z00_1807
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_charzd2classzd2_54,
																																		BNIL);
																																	return
																																		BGl_errorz00zz__errorz00
																																		(BGl_string2500z00zz__regexpz00,
																																		BGl_symbol2586z00zz__regexpz00,
																																		CAR
																																		(BgL_list1759z00_1807));
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* pregexp-list-ref */
	obj_t BGl_pregexpzd2listzd2refz00zz__regexpz00(obj_t BgL_sz00_55,
		obj_t BgL_iz00_56)
	{
		{	/* Unsafe/pregexp.scm 515 */
			{
				obj_t BgL_sz00_3425;
				long BgL_kz00_3426;

				BgL_sz00_3425 = BgL_sz00_55;
				BgL_kz00_3426 = 0L;
			BgL_loopz00_3424:
				if (NULLP(BgL_sz00_3425))
					{	/* Unsafe/pregexp.scm 520 */
						return BFALSE;
					}
				else
					{	/* Unsafe/pregexp.scm 520 */
						if ((BgL_kz00_3426 == (long) CINT(BgL_iz00_56)))
							{	/* Unsafe/pregexp.scm 521 */
								return CAR(((obj_t) BgL_sz00_3425));
							}
						else
							{
								long BgL_kz00_5559;
								obj_t BgL_sz00_5556;

								BgL_sz00_5556 = CDR(((obj_t) BgL_sz00_3425));
								BgL_kz00_5559 = (BgL_kz00_3426 + 1L);
								BgL_kz00_3426 = BgL_kz00_5559;
								BgL_sz00_3425 = BgL_sz00_5556;
								goto BgL_loopz00_3424;
							}
					}
			}
		}

	}



/* sub~0 */
	obj_t BGl_subze70ze7zz__regexpz00(obj_t BgL_rez00_1818)
	{
		{	/* Unsafe/pregexp.scm 536 */
			if (PAIRP(BgL_rez00_1818))
				{	/* Unsafe/pregexp.scm 538 */
					obj_t BgL_carzd2rezd2_1821;
					obj_t BgL_subzd2cdrzd2rez00_1822;

					BgL_carzd2rezd2_1821 = CAR(BgL_rez00_1818);
					BgL_subzd2cdrzd2rez00_1822 =
						BGl_subze70ze7zz__regexpz00(CDR(BgL_rez00_1818));
					if ((BgL_carzd2rezd2_1821 == BGl_keyword2517z00zz__regexpz00))
						{	/* Unsafe/pregexp.scm 541 */
							obj_t BgL_arg1768z00_1824;

							BgL_arg1768z00_1824 = MAKE_YOUNG_PAIR(BgL_rez00_1818, BFALSE);
							return
								MAKE_YOUNG_PAIR(BgL_arg1768z00_1824,
								BgL_subzd2cdrzd2rez00_1822);
						}
					else
						{	/* Unsafe/pregexp.scm 540 */
							return
								BGl_appendzd221011zd2zz__regexpz00(BGl_subze70ze7zz__regexpz00
								(BgL_carzd2rezd2_1821), BgL_subzd2cdrzd2rez00_1822);
						}
				}
			else
				{	/* Unsafe/pregexp.scm 537 */
					return BNIL;
				}
		}

	}



/* pregexp-match-positions-aux */
	obj_t BGl_pregexpzd2matchzd2positionszd2auxzd2zz__regexpz00(obj_t
		BgL_rez00_58, obj_t BgL_sz00_59, long BgL_snz00_60, obj_t BgL_startz00_61,
		obj_t BgL_nz00_62, obj_t BgL_iz00_63)
	{
		{	/* Unsafe/pregexp.scm 545 */
			{	/* Unsafe/pregexp.scm 547 */
				obj_t BgL_nz00_4357;
				obj_t BgL_snz00_4356;

				BgL_nz00_4357 = MAKE_CELL(BgL_nz00_62);
				BgL_snz00_4356 = MAKE_CELL(BINT(BgL_snz00_60));
				{	/* Unsafe/pregexp.scm 547 */
					obj_t BgL_backrefsz00_1829;
					obj_t BgL_casezd2sensitivezf3z21_4358;

					BgL_backrefsz00_1829 = BGl_subze70ze7zz__regexpz00(BgL_rez00_58);
					BgL_casezd2sensitivezf3z21_4358 = MAKE_CELL(BTRUE);
					BGl_z62subz62zz__regexpz00(BgL_startz00_61, BgL_sz00_59,
						BgL_backrefsz00_1829, BgL_snz00_4356, BGl_proc2588z00zz__regexpz00,
						BgL_casezd2sensitivezf3z21_4358, BgL_nz00_4357, BgL_rez00_58,
						BgL_iz00_63, BGl_proc2588z00zz__regexpz00,
						BGl_proc2589z00zz__regexpz00);
					{	/* Unsafe/pregexp.scm 722 */
						obj_t BgL_backrefsz00_2076;

						if (NULLP(BgL_backrefsz00_1829))
							{	/* Unsafe/pregexp.scm 722 */
								BgL_backrefsz00_2076 = BNIL;
							}
						else
							{	/* Unsafe/pregexp.scm 722 */
								obj_t BgL_head1173z00_2080;

								{	/* Unsafe/pregexp.scm 722 */
									obj_t BgL_arg1936z00_2092;

									{	/* Unsafe/pregexp.scm 722 */
										obj_t BgL_pairz00_3700;

										BgL_pairz00_3700 = CAR(((obj_t) BgL_backrefsz00_1829));
										BgL_arg1936z00_2092 = CDR(BgL_pairz00_3700);
									}
									BgL_head1173z00_2080 =
										MAKE_YOUNG_PAIR(BgL_arg1936z00_2092, BNIL);
								}
								{	/* Unsafe/pregexp.scm 722 */
									obj_t BgL_g1176z00_2081;

									BgL_g1176z00_2081 = CDR(((obj_t) BgL_backrefsz00_1829));
									{
										obj_t BgL_l1171z00_3721;
										obj_t BgL_tail1174z00_3722;

										BgL_l1171z00_3721 = BgL_g1176z00_2081;
										BgL_tail1174z00_3722 = BgL_head1173z00_2080;
									BgL_zc3z04anonymousza31931ze3z87_3720:
										if (NULLP(BgL_l1171z00_3721))
											{	/* Unsafe/pregexp.scm 722 */
												BgL_backrefsz00_2076 = BgL_head1173z00_2080;
											}
										else
											{	/* Unsafe/pregexp.scm 722 */
												obj_t BgL_newtail1175z00_3729;

												{	/* Unsafe/pregexp.scm 722 */
													obj_t BgL_arg1934z00_3730;

													{	/* Unsafe/pregexp.scm 722 */
														obj_t BgL_pairz00_3734;

														BgL_pairz00_3734 = CAR(((obj_t) BgL_l1171z00_3721));
														BgL_arg1934z00_3730 = CDR(BgL_pairz00_3734);
													}
													BgL_newtail1175z00_3729 =
														MAKE_YOUNG_PAIR(BgL_arg1934z00_3730, BNIL);
												}
												SET_CDR(BgL_tail1174z00_3722, BgL_newtail1175z00_3729);
												{	/* Unsafe/pregexp.scm 722 */
													obj_t BgL_arg1933z00_3732;

													BgL_arg1933z00_3732 =
														CDR(((obj_t) BgL_l1171z00_3721));
													{
														obj_t BgL_tail1174z00_5593;
														obj_t BgL_l1171z00_5592;

														BgL_l1171z00_5592 = BgL_arg1933z00_3732;
														BgL_tail1174z00_5593 = BgL_newtail1175z00_3729;
														BgL_tail1174z00_3722 = BgL_tail1174z00_5593;
														BgL_l1171z00_3721 = BgL_l1171z00_5592;
														goto BgL_zc3z04anonymousza31931ze3z87_3720;
													}
												}
											}
									}
								}
							}
						if (CBOOL(CAR(((obj_t) BgL_backrefsz00_2076))))
							{	/* Unsafe/pregexp.scm 723 */
								return BgL_backrefsz00_2076;
							}
						else
							{	/* Unsafe/pregexp.scm 723 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* &loup-one-of-chars */
	obj_t BGl_z62loupzd2onezd2ofzd2charszb0zz__regexpz00(obj_t BgL_fkz00_4162,
		obj_t BgL_skz00_4161, obj_t BgL_iz00_4160, obj_t BgL_startz00_4159,
		obj_t BgL_sz00_4158, obj_t BgL_backrefsz00_4157, obj_t BgL_snz00_4156,
		obj_t BgL_identityz00_4155, obj_t BgL_casezd2sensitivezf3z21_4154,
		obj_t BgL_nz00_4153, obj_t BgL_charsz00_1886)
	{
		{	/* Unsafe/pregexp.scm 592 */
			if (NULLP(BgL_charsz00_1886))
				{	/* Unsafe/pregexp.scm 593 */
					return BGL_PROCEDURE_CALL0(BgL_fkz00_4162);
				}
			else
				{	/* Unsafe/pregexp.scm 594 */
					obj_t BgL_arg1814z00_1889;

					BgL_arg1814z00_1889 = CAR(((obj_t) BgL_charsz00_1886));
					{	/* Unsafe/pregexp.scm 596 */
						obj_t BgL_zc3z04anonymousza31816ze3z87_4150;

						BgL_zc3z04anonymousza31816ze3z87_4150 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31816ze3ze5zz__regexpz00, (int) (0L),
							(int) (11L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (0L),
							((obj_t) BgL_nz00_4153));
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (1L),
							((obj_t) BgL_casezd2sensitivezf3z21_4154));
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (2L),
							BgL_identityz00_4155);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (3L),
							((obj_t) BgL_snz00_4156));
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (4L),
							BgL_backrefsz00_4157);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (5L),
							BgL_sz00_4158);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (6L),
							BgL_startz00_4159);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (7L),
							BgL_iz00_4160);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (8L),
							BgL_skz00_4161);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (9L),
							BgL_fkz00_4162);
						PROCEDURE_SET(BgL_zc3z04anonymousza31816ze3z87_4150, (int) (10L),
							BgL_charsz00_1886);
						return BGl_z62subz62zz__regexpz00(BgL_startz00_4159, BgL_sz00_4158,
							BgL_backrefsz00_4157, BgL_snz00_4156, BgL_identityz00_4155,
							BgL_casezd2sensitivezf3z21_4154, BgL_nz00_4153,
							BgL_arg1814z00_1889, BgL_iz00_4160, BgL_skz00_4161,
							BgL_zc3z04anonymousza31816ze3z87_4150);
					}
				}
		}

	}



/* &sub */
	obj_t BGl_z62subz62zz__regexpz00(obj_t BgL_startz00_4169, obj_t BgL_sz00_4168,
		obj_t BgL_backrefsz00_4167, obj_t BgL_snz00_4166,
		obj_t BgL_identityz00_4165, obj_t BgL_casezd2sensitivezf3z21_4164,
		obj_t BgL_nz00_4163, obj_t BgL_rez00_1833, obj_t BgL_iz00_1834,
		obj_t BgL_skz00_1835, obj_t BgL_fkz00_1836)
	{
		{	/* Unsafe/pregexp.scm 550 */
		BGl_z62subz62zz__regexpz00:
			if ((BgL_rez00_1833 == BGl_keyword2486z00zz__regexpz00))
				{	/* Unsafe/pregexp.scm 552 */
					if (((long) CINT(BgL_iz00_1834) == (long) CINT(BgL_startz00_4169)))
						{	/* Unsafe/pregexp.scm 554 */
							return BGL_PROCEDURE_CALL1(BgL_skz00_1835, BgL_iz00_1834);
						}
					else
						{	/* Unsafe/pregexp.scm 554 */
							return BGL_PROCEDURE_CALL0(BgL_fkz00_1836);
						}
				}
			else
				{	/* Unsafe/pregexp.scm 552 */
					if ((BgL_rez00_1833 == BGl_keyword2488z00zz__regexpz00))
						{	/* Unsafe/pregexp.scm 558 */
							bool_t BgL_test2834z00_5649;

							{	/* Unsafe/pregexp.scm 558 */
								long BgL_n1z00_3447;
								long BgL_n2z00_3448;

								BgL_n1z00_3447 = (long) CINT(BgL_iz00_1834);
								BgL_n2z00_3448 = (long) CINT(CELL_REF(BgL_nz00_4163));
								BgL_test2834z00_5649 = (BgL_n1z00_3447 >= BgL_n2z00_3448);
							}
							if (BgL_test2834z00_5649)
								{	/* Unsafe/pregexp.scm 558 */
									return BGL_PROCEDURE_CALL1(BgL_skz00_1835, BgL_iz00_1834);
								}
							else
								{	/* Unsafe/pregexp.scm 558 */
									return BGL_PROCEDURE_CALL0(BgL_fkz00_1836);
								}
						}
					else
						{	/* Unsafe/pregexp.scm 556 */
							if ((BgL_rez00_1833 == BGl_keyword2484z00zz__regexpz00))
								{	/* Unsafe/pregexp.scm 560 */
									return BGL_PROCEDURE_CALL1(BgL_skz00_1835, BgL_iz00_1834);
								}
							else
								{	/* Unsafe/pregexp.scm 560 */
									if ((BgL_rez00_1833 == BGl_keyword2501z00zz__regexpz00))
										{	/* Unsafe/pregexp.scm 562 */
											if (CBOOL
												(BGl_pregexpzd2atzd2wordzd2boundaryzf3z21zz__regexpz00
													(BgL_sz00_4168, BgL_iz00_1834,
														CELL_REF(BgL_nz00_4163))))
												{	/* Unsafe/pregexp.scm 563 */
													return
														BGL_PROCEDURE_CALL1(BgL_skz00_1835, BgL_iz00_1834);
												}
											else
												{	/* Unsafe/pregexp.scm 563 */
													return BGL_PROCEDURE_CALL0(BgL_fkz00_1836);
												}
										}
									else
										{	/* Unsafe/pregexp.scm 562 */
											if ((BgL_rez00_1833 == BGl_keyword2503z00zz__regexpz00))
												{	/* Unsafe/pregexp.scm 566 */
													if (CBOOL
														(BGl_pregexpzd2atzd2wordzd2boundaryzf3z21zz__regexpz00
															(BgL_sz00_4168, BgL_iz00_1834,
																CELL_REF(BgL_nz00_4163))))
														{	/* Unsafe/pregexp.scm 567 */
															return BGL_PROCEDURE_CALL0(BgL_fkz00_1836);
														}
													else
														{	/* Unsafe/pregexp.scm 567 */
															return
																BGL_PROCEDURE_CALL1(BgL_skz00_1835,
																BgL_iz00_1834);
														}
												}
											else
												{	/* Unsafe/pregexp.scm 570 */
													bool_t BgL_test2840z00_5690;

													if (CHARP(BgL_rez00_1833))
														{	/* Unsafe/pregexp.scm 570 */
															long BgL_n1z00_3452;
															long BgL_n2z00_3453;

															BgL_n1z00_3452 = (long) CINT(BgL_iz00_1834);
															BgL_n2z00_3453 =
																(long) CINT(CELL_REF(BgL_nz00_4163));
															BgL_test2840z00_5690 =
																(BgL_n1z00_3452 < BgL_n2z00_3453);
														}
													else
														{	/* Unsafe/pregexp.scm 570 */
															BgL_test2840z00_5690 = ((bool_t) 0);
														}
													if (BgL_test2840z00_5690)
														{	/* Unsafe/pregexp.scm 572 */
															bool_t BgL_test2842z00_5696;

															{	/* Unsafe/pregexp.scm 573 */
																unsigned char BgL_g1787z00_1853;

																BgL_g1787z00_1853 =
																	STRING_REF(BgL_sz00_4168,
																	(long) CINT(BgL_iz00_1834));
																if (CBOOL(CELL_REF
																		(BgL_casezd2sensitivezf3z21_4164)))
																	{	/* Unsafe/pregexp.scm 572 */
																		BgL_test2842z00_5696 =
																			(BgL_g1787z00_1853 ==
																			CCHAR(BgL_rez00_1833));
																	}
																else
																	{	/* Unsafe/pregexp.scm 572 */
																		BgL_test2842z00_5696 =
																			(toupper(BgL_g1787z00_1853) ==
																			toupper(CCHAR(BgL_rez00_1833)));
																	}
															}
															if (BgL_test2842z00_5696)
																{	/* Unsafe/pregexp.scm 574 */
																	long BgL_arg1786z00_1852;

																	BgL_arg1786z00_1852 =
																		((long) CINT(BgL_iz00_1834) + 1L);
																	return
																		BGL_PROCEDURE_CALL1(BgL_skz00_1835,
																		BINT(BgL_arg1786z00_1852));
																}
															else
																{	/* Unsafe/pregexp.scm 572 */
																	return BGL_PROCEDURE_CALL0(BgL_fkz00_1836);
																}
														}
													else
														{	/* Unsafe/pregexp.scm 575 */
															bool_t BgL_test2844z00_5717;

															if (PAIRP(BgL_rez00_1833))
																{	/* Unsafe/pregexp.scm 575 */
																	BgL_test2844z00_5717 = ((bool_t) 0);
																}
															else
																{	/* Unsafe/pregexp.scm 575 */
																	long BgL_n1z00_3467;
																	long BgL_n2z00_3468;

																	BgL_n1z00_3467 = (long) CINT(BgL_iz00_1834);
																	BgL_n2z00_3468 =
																		(long) CINT(CELL_REF(BgL_nz00_4163));
																	BgL_test2844z00_5717 =
																		(BgL_n1z00_3467 < BgL_n2z00_3468);
																}
															if (BgL_test2844z00_5717)
																{	/* Unsafe/pregexp.scm 576 */
																	bool_t BgL_test2846z00_5723;

																	{	/* Unsafe/pregexp.scm 577 */
																		unsigned char BgL_arg1794z00_1860;

																		BgL_arg1794z00_1860 =
																			STRING_REF(BgL_sz00_4168,
																			(long) CINT(BgL_iz00_1834));
																		BgL_test2846z00_5723 =
																			CBOOL
																			(BGl_pregexpzd2checkzd2ifzd2inzd2charzd2classzf3z21zz__regexpz00
																			(BgL_arg1794z00_1860, BgL_rez00_1833));
																	}
																	if (BgL_test2846z00_5723)
																		{	/* Unsafe/pregexp.scm 578 */
																			long BgL_arg1793z00_1859;

																			BgL_arg1793z00_1859 =
																				((long) CINT(BgL_iz00_1834) + 1L);
																			return
																				BGL_PROCEDURE_CALL1(BgL_skz00_1835,
																				BINT(BgL_arg1793z00_1859));
																		}
																	else
																		{	/* Unsafe/pregexp.scm 576 */
																			return
																				BGL_PROCEDURE_CALL0(BgL_fkz00_1836);
																		}
																}
															else
																{	/* Unsafe/pregexp.scm 579 */
																	bool_t BgL_test2847z00_5738;

																	if (PAIRP(BgL_rez00_1833))
																		{	/* Unsafe/pregexp.scm 579 */
																			if (
																				(CAR(BgL_rez00_1833) ==
																					BGl_keyword2562z00zz__regexpz00))
																				{	/* Unsafe/pregexp.scm 579 */
																					long BgL_n1z00_3474;
																					long BgL_n2z00_3475;

																					BgL_n1z00_3474 =
																						(long) CINT(BgL_iz00_1834);
																					BgL_n2z00_3475 =
																						(long)
																						CINT(CELL_REF(BgL_nz00_4163));
																					BgL_test2847z00_5738 =
																						(BgL_n1z00_3474 < BgL_n2z00_3475);
																				}
																			else
																				{	/* Unsafe/pregexp.scm 579 */
																					BgL_test2847z00_5738 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Unsafe/pregexp.scm 579 */
																			BgL_test2847z00_5738 = ((bool_t) 0);
																		}
																	if (BgL_test2847z00_5738)
																		{	/* Unsafe/pregexp.scm 580 */
																			unsigned char BgL_cz00_1866;

																			BgL_cz00_1866 =
																				STRING_REF(BgL_sz00_4168,
																				(long) CINT(BgL_iz00_1834));
																			{	/* Unsafe/pregexp.scm 581 */
																				bool_t BgL_test2850z00_5749;

																				{	/* Unsafe/pregexp.scm 581 */
																					obj_t BgL_czc3zc3_1873;

																					if (CBOOL(CELL_REF
																							(BgL_casezd2sensitivezf3z21_4164)))
																						{	/* Unsafe/pregexp.scm 581 */
																							BgL_czc3zc3_1873 =
																								BGl_charzc3zd3zf3zd2envz31zz__r4_characters_6_6z00;
																						}
																					else
																						{	/* Unsafe/pregexp.scm 581 */
																							BgL_czc3zc3_1873 =
																								BGl_charzd2cizc3zd3zf3zd2envze3zz__r4_characters_6_6z00;
																						}
																					{	/* Unsafe/pregexp.scm 582 */
																						obj_t BgL__andtest_1105z00_1874;

																						{	/* Unsafe/pregexp.scm 582 */
																							obj_t BgL_arg1805z00_1876;

																							BgL_arg1805z00_1876 =
																								CAR(CDR(BgL_rez00_1833));
																							BgL__andtest_1105z00_1874 =
																								BGL_PROCEDURE_CALL2
																								(BgL_czc3zc3_1873,
																								BgL_arg1805z00_1876,
																								BCHAR(BgL_cz00_1866));
																						}
																						if (CBOOL
																							(BgL__andtest_1105z00_1874))
																							{	/* Unsafe/pregexp.scm 583 */
																								obj_t BgL_arg1804z00_1875;

																								BgL_arg1804z00_1875 =
																									CAR(CDR(CDR(BgL_rez00_1833)));
																								BgL_test2850z00_5749 =
																									CBOOL(BGL_PROCEDURE_CALL2
																									(BgL_czc3zc3_1873,
																										BCHAR(BgL_cz00_1866),
																										BgL_arg1804z00_1875));
																							}
																						else
																							{	/* Unsafe/pregexp.scm 582 */
																								BgL_test2850z00_5749 =
																									((bool_t) 0);
																							}
																					}
																				}
																				if (BgL_test2850z00_5749)
																					{	/* Unsafe/pregexp.scm 584 */
																						long BgL_arg1803z00_1872;

																						BgL_arg1803z00_1872 =
																							((long) CINT(BgL_iz00_1834) + 1L);
																						return
																							BGL_PROCEDURE_CALL1
																							(BgL_skz00_1835,
																							BINT(BgL_arg1803z00_1872));
																					}
																				else
																					{	/* Unsafe/pregexp.scm 581 */
																						return
																							BGL_PROCEDURE_CALL0
																							(BgL_fkz00_1836);
																					}
																			}
																		}
																	else
																		{	/* Unsafe/pregexp.scm 579 */
																			if (PAIRP(BgL_rez00_1833))
																				{	/* Unsafe/pregexp.scm 586 */
																					obj_t BgL_casezd2valuezd2_1878;

																					BgL_casezd2valuezd2_1878 =
																						CAR(BgL_rez00_1833);
																					if (
																						(BgL_casezd2valuezd2_1878 ==
																							BGl_keyword2562z00zz__regexpz00))
																						{	/* Unsafe/pregexp.scm 588 */
																							bool_t BgL_test2855z00_5787;

																							{	/* Unsafe/pregexp.scm 588 */
																								long BgL_n1z00_3491;
																								long BgL_n2z00_3492;

																								BgL_n1z00_3491 =
																									(long) CINT(BgL_iz00_1834);
																								BgL_n2z00_3492 =
																									(long)
																									CINT(CELL_REF(BgL_nz00_4163));
																								BgL_test2855z00_5787 =
																									(BgL_n1z00_3491 >=
																									BgL_n2z00_3492);
																							}
																							if (BgL_test2855z00_5787)
																								{	/* Unsafe/pregexp.scm 588 */
																									return
																										BGL_PROCEDURE_CALL0
																										(BgL_fkz00_1836);
																								}
																							else
																								{	/* Unsafe/pregexp.scm 588 */
																									return
																										BGl_errorz00zz__errorz00
																										(BGl_string2500z00zz__regexpz00,
																										BGl_symbol2590z00zz__regexpz00,
																										BUNSPEC);
																								}
																						}
																					else
																						{	/* Unsafe/pregexp.scm 586 */
																							if (
																								(BgL_casezd2valuezd2_1878 ==
																									BGl_keyword2560z00zz__regexpz00))
																								{	/* Unsafe/pregexp.scm 591 */
																									bool_t BgL_test2857z00_5797;

																									{	/* Unsafe/pregexp.scm 591 */
																										long BgL_n1z00_3498;
																										long BgL_n2z00_3499;

																										BgL_n1z00_3498 =
																											(long)
																											CINT(BgL_iz00_1834);
																										BgL_n2z00_3499 =
																											(long)
																											CINT(CELL_REF
																											(BgL_nz00_4163));
																										BgL_test2857z00_5797 =
																											(BgL_n1z00_3498 >=
																											BgL_n2z00_3499);
																									}
																									if (BgL_test2857z00_5797)
																										{	/* Unsafe/pregexp.scm 591 */
																											return
																												BGL_PROCEDURE_CALL0
																												(BgL_fkz00_1836);
																										}
																									else
																										{	/* Unsafe/pregexp.scm 592 */
																											obj_t BgL_g1106z00_1884;

																											BgL_g1106z00_1884 =
																												CDR(BgL_rez00_1833);
																											return
																												BGl_z62loupzd2onezd2ofzd2charszb0zz__regexpz00
																												(BgL_fkz00_1836,
																												BgL_skz00_1835,
																												BgL_iz00_1834,
																												BgL_startz00_4169,
																												BgL_sz00_4168,
																												BgL_backrefsz00_4167,
																												BgL_snz00_4166,
																												BgL_identityz00_4165,
																												BgL_casezd2sensitivezf3z21_4164,
																												BgL_nz00_4163,
																												BgL_g1106z00_1884);
																										}
																								}
																							else
																								{	/* Unsafe/pregexp.scm 586 */
																									if (
																										(BgL_casezd2valuezd2_1878 ==
																											BGl_keyword2492z00zz__regexpz00))
																										{	/* Unsafe/pregexp.scm 598 */
																											bool_t
																												BgL_test2859z00_5808;
																											{	/* Unsafe/pregexp.scm 598 */
																												long BgL_n1z00_3505;
																												long BgL_n2z00_3506;

																												BgL_n1z00_3505 =
																													(long)
																													CINT(BgL_iz00_1834);
																												BgL_n2z00_3506 =
																													(long)
																													CINT(CELL_REF
																													(BgL_nz00_4163));
																												BgL_test2859z00_5808 =
																													(BgL_n1z00_3505 >=
																													BgL_n2z00_3506);
																											}
																											if (BgL_test2859z00_5808)
																												{	/* Unsafe/pregexp.scm 598 */
																													return
																														BGL_PROCEDURE_CALL0
																														(BgL_fkz00_1836);
																												}
																											else
																												{	/* Unsafe/pregexp.scm 599 */
																													obj_t
																														BgL_arg1820z00_1897;
																													BgL_arg1820z00_1897 =
																														CAR(CDR
																														(BgL_rez00_1833));
																													{	/* Unsafe/pregexp.scm 600 */
																														obj_t
																															BgL_zc3z04anonymousza31824ze3z87_4130;
																														obj_t
																															BgL_zc3z04anonymousza31825ze3z87_4131;
																														BgL_zc3z04anonymousza31824ze3z87_4130
																															=
																															MAKE_FX_PROCEDURE
																															(BGl_z62zc3z04anonymousza31824ze3ze5zz__regexpz00,
																															(int) (1L),
																															(int) (1L));
																														BgL_zc3z04anonymousza31825ze3z87_4131
																															=
																															MAKE_FX_PROCEDURE
																															(BGl_z62zc3z04anonymousza31825ze3ze5zz__regexpz00,
																															(int) (0L),
																															(int) (2L));
																														PROCEDURE_SET
																															(BgL_zc3z04anonymousza31824ze3z87_4130,
																															(int) (0L),
																															BgL_fkz00_1836);
																														PROCEDURE_SET
																															(BgL_zc3z04anonymousza31825ze3z87_4131,
																															(int) (0L),
																															BgL_iz00_1834);
																														PROCEDURE_SET
																															(BgL_zc3z04anonymousza31825ze3z87_4131,
																															(int) (1L),
																															BgL_skz00_1835);
																														{
																															obj_t
																																BgL_fkz00_5831;
																															obj_t
																																BgL_skz00_5830;
																															obj_t
																																BgL_rez00_5829;
																															BgL_rez00_5829 =
																																BgL_arg1820z00_1897;
																															BgL_skz00_5830 =
																																BgL_zc3z04anonymousza31824ze3z87_4130;
																															BgL_fkz00_5831 =
																																BgL_zc3z04anonymousza31825ze3z87_4131;
																															BgL_fkz00_1836 =
																																BgL_fkz00_5831;
																															BgL_skz00_1835 =
																																BgL_skz00_5830;
																															BgL_rez00_1833 =
																																BgL_rez00_5829;
																															goto
																																BGl_z62subz62zz__regexpz00;
																														}
																													}
																												}
																										}
																									else
																										{	/* Unsafe/pregexp.scm 586 */
																											if (
																												(BgL_casezd2valuezd2_1878
																													==
																													BGl_keyword2480z00zz__regexpz00))
																												{	/* Unsafe/pregexp.scm 603 */
																													obj_t
																														BgL_g1107z00_1907;
																													BgL_g1107z00_1907 =
																														CDR(BgL_rez00_1833);
																													return
																														BGl_z62loupzd2seqzb0zz__regexpz00
																														(BgL_skz00_1835,
																														BgL_fkz00_1836,
																														BgL_startz00_4169,
																														BgL_sz00_4168,
																														BgL_backrefsz00_4167,
																														BgL_snz00_4166,
																														BgL_identityz00_4165,
																														BgL_casezd2sensitivezf3z21_4164,
																														BgL_nz00_4163,
																														BgL_g1107z00_1907,
																														BgL_iz00_1834);
																												}
																											else
																												{	/* Unsafe/pregexp.scm 586 */
																													if (
																														(BgL_casezd2valuezd2_1878
																															==
																															BGl_keyword2482z00zz__regexpz00))
																														{	/* Unsafe/pregexp.scm 610 */
																															obj_t
																																BgL_g1108z00_1921;
																															BgL_g1108z00_1921
																																=
																																CDR
																																(BgL_rez00_1833);
																															return
																																BGl_z62loupzd2orzb0zz__regexpz00
																																(BgL_fkz00_1836,
																																BgL_iz00_1834,
																																BgL_skz00_1835,
																																BgL_startz00_4169,
																																BgL_sz00_4168,
																																BgL_backrefsz00_4167,
																																BgL_snz00_4166,
																																BgL_identityz00_4165,
																																BgL_casezd2sensitivezf3z21_4164,
																																BgL_nz00_4163,
																																BgL_g1108z00_1921);
																														}
																													else
																														{	/* Unsafe/pregexp.scm 586 */
																															if (
																																(BgL_casezd2valuezd2_1878
																																	==
																																	BGl_keyword2494z00zz__regexpz00))
																																{	/* Unsafe/pregexp.scm 618 */
																																	obj_t
																																		BgL_cz00_1939;
																																	BgL_cz00_1939
																																		=
																																		BGl_pregexpzd2listzd2refz00zz__regexpz00
																																		(BgL_backrefsz00_4167,
																																		CAR(CDR
																																			(BgL_rez00_1833)));
																																	{	/* Unsafe/pregexp.scm 618 */
																																		obj_t
																																			BgL_backrefz00_1940;
																																		if (CBOOL
																																			(BgL_cz00_1939))
																																			{	/* Unsafe/pregexp.scm 621 */
																																				BgL_backrefz00_1940
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cz00_1939));
																																			}
																																		else
																																			{	/* Unsafe/pregexp.scm 621 */
																																				{	/* Unsafe/pregexp.scm 622 */
																																					obj_t
																																						BgL_list1851z00_1950;
																																					{	/* Unsafe/pregexp.scm 622 */
																																						obj_t
																																							BgL_arg1852z00_1951;
																																						BgL_arg1852z00_1951
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_rez00_1833,
																																							BNIL);
																																						BgL_list1851z00_1950
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_symbol2592z00zz__regexpz00,
																																							BgL_arg1852z00_1951);
																																					}
																																					BGl_errorz00zz__errorz00
																																						(BGl_string2500z00zz__regexpz00,
																																						BGl_symbol2590z00zz__regexpz00,
																																						CAR
																																						(BgL_list1851z00_1950));
																																				}
																																				BgL_backrefz00_1940
																																					=
																																					BFALSE;
																																			}
																																		{	/* Unsafe/pregexp.scm 619 */

																																			if (CBOOL
																																				(BgL_backrefz00_1940))
																																				{	/* Unsafe/pregexp.scm 627 */
																																					obj_t
																																						BgL_arg1846z00_1941;
																																					{	/* Unsafe/pregexp.scm 627 */
																																						obj_t
																																							BgL_arg1848z00_1943;
																																						obj_t
																																							BgL_arg1849z00_1944;
																																						BgL_arg1848z00_1943
																																							=
																																							CAR
																																							(((obj_t) BgL_backrefz00_1940));
																																						BgL_arg1849z00_1944
																																							=
																																							CDR
																																							(((obj_t) BgL_backrefz00_1940));
																																						BgL_arg1846z00_1941
																																							=
																																							c_substring
																																							(BgL_sz00_4168,
																																							(long)
																																							CINT
																																							(BgL_arg1848z00_1943),
																																							(long)
																																							CINT
																																							(BgL_arg1849z00_1944));
																																					}
																																					{	/* Unsafe/pregexp.scm 626 */
																																						obj_t
																																							BgL_nz00_3570;
																																						BgL_nz00_3570
																																							=
																																							CELL_REF
																																							(BgL_nz00_4163);
																																						{	/* Unsafe/pregexp.scm 456 */
																																							long
																																								BgL_n1z00_3571;
																																							BgL_n1z00_3571
																																								=
																																								STRING_LENGTH
																																								(BgL_arg1846z00_1941);
																																							if ((BgL_n1z00_3571 > (long) CINT(BgL_nz00_3570)))
																																								{	/* Unsafe/pregexp.scm 457 */
																																									return
																																										BGL_PROCEDURE_CALL0
																																										(BgL_fkz00_1836);
																																								}
																																							else
																																								{
																																									long
																																										BgL_jz00_3574;
																																									obj_t
																																										BgL_kz00_3575;
																																									BgL_jz00_3574
																																										=
																																										0L;
																																									BgL_kz00_3575
																																										=
																																										BgL_iz00_1834;
																																								BgL_loopz00_3573:
																																									if ((BgL_jz00_3574 >= BgL_n1z00_3571))
																																										{	/* Unsafe/pregexp.scm 459 */
																																											return
																																												BGL_PROCEDURE_CALL1
																																												(BgL_skz00_1835,
																																												BgL_kz00_3575);
																																										}
																																									else
																																										{	/* Unsafe/pregexp.scm 459 */
																																											if (((long) CINT(BgL_kz00_3575) >= (long) CINT(BgL_nz00_3570)))
																																												{	/* Unsafe/pregexp.scm 460 */
																																													return
																																														BGL_PROCEDURE_CALL0
																																														(BgL_fkz00_1836);
																																												}
																																											else
																																												{	/* Unsafe/pregexp.scm 460 */
																																													if ((STRING_REF(BgL_arg1846z00_1941, BgL_jz00_3574) == STRING_REF(BgL_sz00_4168, (long) CINT(BgL_kz00_3575))))
																																														{	/* Unsafe/pregexp.scm 462 */
																																															long
																																																BgL_arg1730z00_3581;
																																															long
																																																BgL_arg1731z00_3582;
																																															BgL_arg1730z00_3581
																																																=
																																																(BgL_jz00_3574
																																																+
																																																1L);
																																															BgL_arg1731z00_3582
																																																=
																																																(
																																																(long)
																																																CINT
																																																(BgL_kz00_3575)
																																																+
																																																1L);
																																															{
																																																obj_t
																																																	BgL_kz00_5891;
																																																long
																																																	BgL_jz00_5890;
																																																BgL_jz00_5890
																																																	=
																																																	BgL_arg1730z00_3581;
																																																BgL_kz00_5891
																																																	=
																																																	BINT
																																																	(BgL_arg1731z00_3582);
																																																BgL_kz00_3575
																																																	=
																																																	BgL_kz00_5891;
																																																BgL_jz00_3574
																																																	=
																																																	BgL_jz00_5890;
																																																goto
																																																	BgL_loopz00_3573;
																																															}
																																														}
																																													else
																																														{	/* Unsafe/pregexp.scm 461 */
																																															return
																																																BGL_PROCEDURE_CALL0
																																																(BgL_fkz00_1836);
																																														}
																																												}
																																										}
																																								}
																																						}
																																					}
																																				}
																																			else
																																				{	/* Unsafe/pregexp.scm 625 */
																																					return
																																						BGL_PROCEDURE_CALL1
																																						(BgL_skz00_1835,
																																						BgL_iz00_1834);
																																				}
																																		}
																																	}
																																}
																															else
																																{	/* Unsafe/pregexp.scm 586 */
																																	if (
																																		(BgL_casezd2valuezd2_1878
																																			==
																																			BGl_keyword2517z00zz__regexpz00))
																																		{	/* Unsafe/pregexp.scm 631 */
																																			obj_t
																																				BgL_arg1856z00_1954;
																																			BgL_arg1856z00_1954
																																				=
																																				CAR(CDR
																																				(BgL_rez00_1833));
																																			{	/* Unsafe/pregexp.scm 633 */
																																				obj_t
																																					BgL_zc3z04anonymousza31858ze3z87_4129;
																																				BgL_zc3z04anonymousza31858ze3z87_4129
																																					=
																																					MAKE_FX_PROCEDURE
																																					(BGl_z62zc3z04anonymousza31858ze3ze5zz__regexpz00,
																																					(int)
																																					(1L),
																																					(int)
																																					(4L));
																																				PROCEDURE_SET
																																					(BgL_zc3z04anonymousza31858ze3z87_4129,
																																					(int)
																																					(0L),
																																					BgL_rez00_1833);
																																				PROCEDURE_SET
																																					(BgL_zc3z04anonymousza31858ze3z87_4129,
																																					(int)
																																					(1L),
																																					BgL_backrefsz00_4167);
																																				PROCEDURE_SET
																																					(BgL_zc3z04anonymousza31858ze3z87_4129,
																																					(int)
																																					(2L),
																																					BgL_iz00_1834);
																																				PROCEDURE_SET
																																					(BgL_zc3z04anonymousza31858ze3z87_4129,
																																					(int)
																																					(3L),
																																					BgL_skz00_1835);
																																				{
																																					obj_t
																																						BgL_skz00_5916;
																																					obj_t
																																						BgL_rez00_5915;
																																					BgL_rez00_5915
																																						=
																																						BgL_arg1856z00_1954;
																																					BgL_skz00_5916
																																						=
																																						BgL_zc3z04anonymousza31858ze3z87_4129;
																																					BgL_skz00_1835
																																						=
																																						BgL_skz00_5916;
																																					BgL_rez00_1833
																																						=
																																						BgL_rez00_5915;
																																					goto
																																						BGl_z62subz62zz__regexpz00;
																																				}
																																			}
																																		}
																																	else
																																		{	/* Unsafe/pregexp.scm 586 */
																																			if (
																																				(BgL_casezd2valuezd2_1878
																																					==
																																					BGl_keyword2526z00zz__regexpz00))
																																				{	/* Unsafe/pregexp.scm 636 */
																																					obj_t
																																						BgL_foundzd2itzf3z21_1962;
																																					{	/* Unsafe/pregexp.scm 637 */
																																						obj_t
																																							BgL_arg1862z00_1963;
																																						BgL_arg1862z00_1963
																																							=
																																							CAR
																																							(CDR
																																							(BgL_rez00_1833));
																																						BgL_foundzd2itzf3z21_1962
																																							=
																																							BGl_z62subz62zz__regexpz00
																																							(BgL_startz00_4169,
																																							BgL_sz00_4168,
																																							BgL_backrefsz00_4167,
																																							BgL_snz00_4166,
																																							BgL_identityz00_4165,
																																							BgL_casezd2sensitivezf3z21_4164,
																																							BgL_nz00_4163,
																																							BgL_arg1862z00_1963,
																																							BgL_iz00_1834,
																																							BgL_identityz00_4165,
																																							BGl_proc2594z00zz__regexpz00);
																																					}
																																					if (CBOOL(BgL_foundzd2itzf3z21_1962))
																																						{	/* Unsafe/pregexp.scm 639 */
																																							return
																																								BGL_PROCEDURE_CALL1
																																								(BgL_skz00_1835,
																																								BgL_iz00_1834);
																																						}
																																					else
																																						{	/* Unsafe/pregexp.scm 639 */
																																							return
																																								BGL_PROCEDURE_CALL0
																																								(BgL_fkz00_1836);
																																						}
																																				}
																																			else
																																				{	/* Unsafe/pregexp.scm 586 */
																																					if (
																																						(BgL_casezd2valuezd2_1878
																																							==
																																							BGl_keyword2529z00zz__regexpz00))
																																						{	/* Unsafe/pregexp.scm 641 */
																																							obj_t
																																								BgL_foundzd2itzf3z21_1968;
																																							{	/* Unsafe/pregexp.scm 642 */
																																								obj_t
																																									BgL_arg1866z00_1969;
																																								BgL_arg1866z00_1969
																																									=
																																									CAR
																																									(CDR
																																									(BgL_rez00_1833));
																																								BgL_foundzd2itzf3z21_1968
																																									=
																																									BGl_z62subz62zz__regexpz00
																																									(BgL_startz00_4169,
																																									BgL_sz00_4168,
																																									BgL_backrefsz00_4167,
																																									BgL_snz00_4166,
																																									BgL_identityz00_4165,
																																									BgL_casezd2sensitivezf3z21_4164,
																																									BgL_nz00_4163,
																																									BgL_arg1866z00_1969,
																																									BgL_iz00_1834,
																																									BgL_identityz00_4165,
																																									BGl_proc2595z00zz__regexpz00);
																																							}
																																							if (CBOOL(BgL_foundzd2itzf3z21_1968))
																																								{	/* Unsafe/pregexp.scm 644 */
																																									return
																																										BGL_PROCEDURE_CALL0
																																										(BgL_fkz00_1836);
																																								}
																																							else
																																								{	/* Unsafe/pregexp.scm 644 */
																																									return
																																										BGL_PROCEDURE_CALL1
																																										(BgL_skz00_1835,
																																										BgL_iz00_1834);
																																								}
																																						}
																																					else
																																						{	/* Unsafe/pregexp.scm 586 */
																																							if ((BgL_casezd2valuezd2_1878 == BGl_keyword2535z00zz__regexpz00))
																																								{	/* Unsafe/pregexp.scm 646 */
																																									obj_t
																																										BgL_nzd2actualzd2_1974;
																																									obj_t
																																										BgL_snzd2actualzd2_1975;
																																									BgL_nzd2actualzd2_1974
																																										=
																																										CELL_REF
																																										(BgL_nz00_4163);
																																									BgL_snzd2actualzd2_1975
																																										=
																																										CELL_REF
																																										(BgL_snz00_4166);
																																									CELL_SET
																																										(BgL_nz00_4163,
																																										BgL_iz00_1834);
																																									CELL_SET
																																										(BgL_snz00_4166,
																																										BgL_iz00_1834);
																																									{	/* Unsafe/pregexp.scm 648 */
																																										obj_t
																																											BgL_foundzd2itzf3z21_1976;
																																										{	/* Unsafe/pregexp.scm 650 */
																																											obj_t
																																												BgL_arg1872z00_1977;
																																											{	/* Unsafe/pregexp.scm 650 */
																																												obj_t
																																													BgL_arg1874z00_1979;
																																												BgL_arg1874z00_1979
																																													=
																																													CAR
																																													(CDR
																																													(BgL_rez00_1833));
																																												{	/* Unsafe/pregexp.scm 649 */
																																													obj_t
																																														BgL_list1875z00_1980;
																																													{	/* Unsafe/pregexp.scm 649 */
																																														obj_t
																																															BgL_arg1876z00_1981;
																																														{	/* Unsafe/pregexp.scm 649 */
																																															obj_t
																																																BgL_arg1877z00_1982;
																																															{	/* Unsafe/pregexp.scm 649 */
																																																obj_t
																																																	BgL_arg1878z00_1983;
																																																BgL_arg1878z00_1983
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_keyword2488z00zz__regexpz00,
																																																	BNIL);
																																																BgL_arg1877z00_1982
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1874z00_1979,
																																																	BgL_arg1878z00_1983);
																																															}
																																															BgL_arg1876z00_1981
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BGl_list2596z00zz__regexpz00,
																																																BgL_arg1877z00_1982);
																																														}
																																														BgL_list1875z00_1980
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_keyword2480z00zz__regexpz00,
																																															BgL_arg1876z00_1981);
																																													}
																																													BgL_arg1872z00_1977
																																														=
																																														BgL_list1875z00_1980;
																																												}
																																											}
																																											BgL_foundzd2itzf3z21_1976
																																												=
																																												BGl_z62subz62zz__regexpz00
																																												(BgL_startz00_4169,
																																												BgL_sz00_4168,
																																												BgL_backrefsz00_4167,
																																												BgL_snz00_4166,
																																												BgL_identityz00_4165,
																																												BgL_casezd2sensitivezf3z21_4164,
																																												BgL_nz00_4163,
																																												BgL_arg1872z00_1977,
																																												BINT
																																												(0L),
																																												BgL_identityz00_4165,
																																												BGl_proc2597z00zz__regexpz00);
																																										}
																																										CELL_SET
																																											(BgL_nz00_4163,
																																											BgL_nzd2actualzd2_1974);
																																										CELL_SET
																																											(BgL_snz00_4166,
																																											BgL_snzd2actualzd2_1975);
																																										if (CBOOL(BgL_foundzd2itzf3z21_1976))
																																											{	/* Unsafe/pregexp.scm 653 */
																																												return
																																													BGL_PROCEDURE_CALL1
																																													(BgL_skz00_1835,
																																													BgL_iz00_1834);
																																											}
																																										else
																																											{	/* Unsafe/pregexp.scm 653 */
																																												return
																																													BGL_PROCEDURE_CALL0
																																													(BgL_fkz00_1836);
																																											}
																																									}
																																								}
																																							else
																																								{	/* Unsafe/pregexp.scm 586 */
																																									if ((BgL_casezd2valuezd2_1878 == BGl_keyword2538z00zz__regexpz00))
																																										{	/* Unsafe/pregexp.scm 655 */
																																											obj_t
																																												BgL_nzd2actualzd2_1987;
																																											obj_t
																																												BgL_snzd2actualzd2_1988;
																																											BgL_nzd2actualzd2_1987
																																												=
																																												CELL_REF
																																												(BgL_nz00_4163);
																																											BgL_snzd2actualzd2_1988
																																												=
																																												CELL_REF
																																												(BgL_snz00_4166);
																																											CELL_SET
																																												(BgL_nz00_4163,
																																												BgL_iz00_1834);
																																											CELL_SET
																																												(BgL_snz00_4166,
																																												BgL_iz00_1834);
																																											{	/* Unsafe/pregexp.scm 657 */
																																												obj_t
																																													BgL_foundzd2itzf3z21_1989;
																																												{	/* Unsafe/pregexp.scm 659 */
																																													obj_t
																																														BgL_arg1882z00_1990;
																																													{	/* Unsafe/pregexp.scm 659 */
																																														obj_t
																																															BgL_arg1884z00_1992;
																																														BgL_arg1884z00_1992
																																															=
																																															CAR
																																															(CDR
																																															(BgL_rez00_1833));
																																														{	/* Unsafe/pregexp.scm 658 */
																																															obj_t
																																																BgL_list1885z00_1993;
																																															{	/* Unsafe/pregexp.scm 658 */
																																																obj_t
																																																	BgL_arg1887z00_1994;
																																																{	/* Unsafe/pregexp.scm 658 */
																																																	obj_t
																																																		BgL_arg1888z00_1995;
																																																	{	/* Unsafe/pregexp.scm 658 */
																																																		obj_t
																																																			BgL_arg1889z00_1996;
																																																		BgL_arg1889z00_1996
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BGl_keyword2488z00zz__regexpz00,
																																																			BNIL);
																																																		BgL_arg1888z00_1995
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg1884z00_1992,
																																																			BgL_arg1889z00_1996);
																																																	}
																																																	BgL_arg1887z00_1994
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BGl_list2596z00zz__regexpz00,
																																																		BgL_arg1888z00_1995);
																																																}
																																																BgL_list1885z00_1993
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_keyword2480z00zz__regexpz00,
																																																	BgL_arg1887z00_1994);
																																															}
																																															BgL_arg1882z00_1990
																																																=
																																																BgL_list1885z00_1993;
																																														}
																																													}
																																													BgL_foundzd2itzf3z21_1989
																																														=
																																														BGl_z62subz62zz__regexpz00
																																														(BgL_startz00_4169,
																																														BgL_sz00_4168,
																																														BgL_backrefsz00_4167,
																																														BgL_snz00_4166,
																																														BgL_identityz00_4165,
																																														BgL_casezd2sensitivezf3z21_4164,
																																														BgL_nz00_4163,
																																														BgL_arg1882z00_1990,
																																														BINT
																																														(0L),
																																														BgL_identityz00_4165,
																																														BGl_proc2598z00zz__regexpz00);
																																												}
																																												CELL_SET
																																													(BgL_nz00_4163,
																																													BgL_nzd2actualzd2_1987);
																																												CELL_SET
																																													(BgL_snz00_4166,
																																													BgL_snzd2actualzd2_1988);
																																												if (CBOOL(BgL_foundzd2itzf3z21_1989))
																																													{	/* Unsafe/pregexp.scm 662 */
																																														return
																																															BGL_PROCEDURE_CALL0
																																															(BgL_fkz00_1836);
																																													}
																																												else
																																													{	/* Unsafe/pregexp.scm 662 */
																																														return
																																															BGL_PROCEDURE_CALL1
																																															(BgL_skz00_1835,
																																															BgL_iz00_1834);
																																													}
																																											}
																																										}
																																									else
																																										{	/* Unsafe/pregexp.scm 586 */
																																											if ((BgL_casezd2valuezd2_1878 == BGl_keyword2532z00zz__regexpz00))
																																												{	/* Unsafe/pregexp.scm 664 */
																																													obj_t
																																														BgL_foundzd2itzf3z21_2000;
																																													{	/* Unsafe/pregexp.scm 664 */
																																														obj_t
																																															BgL_arg1892z00_2001;
																																														BgL_arg1892z00_2001
																																															=
																																															CAR
																																															(CDR
																																															(BgL_rez00_1833));
																																														BgL_foundzd2itzf3z21_2000
																																															=
																																															BGl_z62subz62zz__regexpz00
																																															(BgL_startz00_4169,
																																															BgL_sz00_4168,
																																															BgL_backrefsz00_4167,
																																															BgL_snz00_4166,
																																															BgL_identityz00_4165,
																																															BgL_casezd2sensitivezf3z21_4164,
																																															BgL_nz00_4163,
																																															BgL_arg1892z00_2001,
																																															BgL_iz00_1834,
																																															BgL_identityz00_4165,
																																															BGl_proc2599z00zz__regexpz00);
																																													}
																																													if (CBOOL(BgL_foundzd2itzf3z21_2000))
																																														{	/* Unsafe/pregexp.scm 666 */
																																															return
																																																BGL_PROCEDURE_CALL1
																																																(BgL_skz00_1835,
																																																BgL_foundzd2itzf3z21_2000);
																																														}
																																													else
																																														{	/* Unsafe/pregexp.scm 666 */
																																															return
																																																BGL_PROCEDURE_CALL0
																																																(BgL_fkz00_1836);
																																														}
																																												}
																																											else
																																												{	/* Unsafe/pregexp.scm 586 */
																																													bool_t
																																														BgL_test2880z00_5997;
																																													{	/* Unsafe/pregexp.scm 586 */
																																														bool_t
																																															BgL__ortest_1112z00_2065;
																																														BgL__ortest_1112z00_2065
																																															=
																																															(BgL_casezd2valuezd2_1878
																																															==
																																															BGl_keyword2519z00zz__regexpz00);
																																														if (BgL__ortest_1112z00_2065)
																																															{	/* Unsafe/pregexp.scm 586 */
																																																BgL_test2880z00_5997
																																																	=
																																																	BgL__ortest_1112z00_2065;
																																															}
																																														else
																																															{	/* Unsafe/pregexp.scm 586 */
																																																BgL_test2880z00_5997
																																																	=
																																																	(BgL_casezd2valuezd2_1878
																																																	==
																																																	BGl_keyword2521z00zz__regexpz00);
																																															}
																																													}
																																													if (BgL_test2880z00_5997)
																																														{	/* Unsafe/pregexp.scm 670 */
																																															obj_t
																																																BgL_oldz00_2007;
																																															BgL_oldz00_2007
																																																=
																																																CELL_REF
																																																(BgL_casezd2sensitivezf3z21_4164);
																																															{	/* Unsafe/pregexp.scm 671 */
																																																obj_t
																																																	BgL_auxz00_4178;
																																																BgL_auxz00_4178
																																																	=
																																																	BBOOL
																																																	(
																																																	(BgL_casezd2valuezd2_1878
																																																		==
																																																		BGl_keyword2519z00zz__regexpz00));
																																																CELL_SET
																																																	(BgL_casezd2sensitivezf3z21_4164,
																																																	BgL_auxz00_4178);
																																															}
																																															{	/* Unsafe/pregexp.scm 673 */
																																																obj_t
																																																	BgL_arg1897z00_2009;
																																																BgL_arg1897z00_2009
																																																	=
																																																	CAR
																																																	(CDR
																																																	(BgL_rez00_1833));
																																																{	/* Unsafe/pregexp.scm 675 */
																																																	obj_t
																																																		BgL_zc3z04anonymousza31900ze3z87_4121;
																																																	obj_t
																																																		BgL_zc3z04anonymousza31901ze3z87_4122;
																																																	BgL_zc3z04anonymousza31900ze3z87_4121
																																																		=
																																																		MAKE_FX_PROCEDURE
																																																		(BGl_z62zc3z04anonymousza31900ze3ze5zz__regexpz00,
																																																		(int)
																																																		(1L),
																																																		(int)
																																																		(3L));
																																																	BgL_zc3z04anonymousza31901ze3z87_4122
																																																		=
																																																		MAKE_FX_PROCEDURE
																																																		(BGl_z62zc3z04anonymousza31901ze3ze5zz__regexpz00,
																																																		(int)
																																																		(0L),
																																																		(int)
																																																		(3L));
																																																	PROCEDURE_SET
																																																		(BgL_zc3z04anonymousza31900ze3z87_4121,
																																																		(int)
																																																		(0L),
																																																		BgL_oldz00_2007);
																																																	PROCEDURE_SET
																																																		(BgL_zc3z04anonymousza31900ze3z87_4121,
																																																		(int)
																																																		(1L),
																																																		((obj_t) BgL_casezd2sensitivezf3z21_4164));
																																																	PROCEDURE_SET
																																																		(BgL_zc3z04anonymousza31900ze3z87_4121,
																																																		(int)
																																																		(2L),
																																																		BgL_skz00_1835);
																																																	PROCEDURE_SET
																																																		(BgL_zc3z04anonymousza31901ze3z87_4122,
																																																		(int)
																																																		(0L),
																																																		BgL_oldz00_2007);
																																																	PROCEDURE_SET
																																																		(BgL_zc3z04anonymousza31901ze3z87_4122,
																																																		(int)
																																																		(1L),
																																																		((obj_t) BgL_casezd2sensitivezf3z21_4164));
																																																	PROCEDURE_SET
																																																		(BgL_zc3z04anonymousza31901ze3z87_4122,
																																																		(int)
																																																		(2L),
																																																		BgL_fkz00_1836);
																																																	{
																																																		obj_t
																																																			BgL_fkz00_6027;
																																																		obj_t
																																																			BgL_skz00_6026;
																																																		obj_t
																																																			BgL_rez00_6025;
																																																		BgL_rez00_6025
																																																			=
																																																			BgL_arg1897z00_2009;
																																																		BgL_skz00_6026
																																																			=
																																																			BgL_zc3z04anonymousza31900ze3z87_4121;
																																																		BgL_fkz00_6027
																																																			=
																																																			BgL_zc3z04anonymousza31901ze3z87_4122;
																																																		BgL_fkz00_1836
																																																			=
																																																			BgL_fkz00_6027;
																																																		BgL_skz00_1835
																																																			=
																																																			BgL_skz00_6026;
																																																		BgL_rez00_1833
																																																			=
																																																			BgL_rez00_6025;
																																																		goto
																																																			BGl_z62subz62zz__regexpz00;
																																																	}
																																																}
																																															}
																																														}
																																													else
																																														{	/* Unsafe/pregexp.scm 586 */
																																															if ((BgL_casezd2valuezd2_1878 == BGl_keyword2548z00zz__regexpz00))
																																																{	/* Unsafe/pregexp.scm 681 */
																																																	bool_t
																																																		BgL_maximalzf3zf3_2018;
																																																	{	/* Unsafe/pregexp.scm 681 */
																																																		bool_t
																																																			BgL_test2883z00_6030;
																																																		{	/* Unsafe/pregexp.scm 681 */
																																																			obj_t
																																																				BgL_pairz00_3643;
																																																			BgL_pairz00_3643
																																																				=
																																																				CDR
																																																				(BgL_rez00_1833);
																																																			BgL_test2883z00_6030
																																																				=
																																																				CBOOL
																																																				(CAR
																																																				(BgL_pairz00_3643));
																																																		}
																																																		if (BgL_test2883z00_6030)
																																																			{	/* Unsafe/pregexp.scm 681 */
																																																				BgL_maximalzf3zf3_2018
																																																					=
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																		else
																																																			{	/* Unsafe/pregexp.scm 681 */
																																																				BgL_maximalzf3zf3_2018
																																																					=
																																																					(
																																																					(bool_t)
																																																					1);
																																																			}
																																																	}
																																																	{	/* Unsafe/pregexp.scm 681 */
																																																		obj_t
																																																			BgL_pz00_2019;
																																																		BgL_pz00_2019
																																																			=
																																																			CAR
																																																			(CDR
																																																			(CDR
																																																				(BgL_rez00_1833)));
																																																		{	/* Unsafe/pregexp.scm 682 */
																																																			obj_t
																																																				BgL_qz00_2020;
																																																			BgL_qz00_2020
																																																				=
																																																				CAR
																																																				(CDR
																																																				(CDR
																																																					(CDR
																																																						(BgL_rez00_1833))));
																																																			{	/* Unsafe/pregexp.scm 683 */
																																																				bool_t
																																																					BgL_couldzd2loopzd2infinitelyzf3zf3_2021;
																																																				if (BgL_maximalzf3zf3_2018)
																																																					{	/* Unsafe/pregexp.scm 684 */
																																																						if (CBOOL(BgL_qz00_2020))
																																																							{	/* Unsafe/pregexp.scm 684 */
																																																								BgL_couldzd2loopzd2infinitelyzf3zf3_2021
																																																									=
																																																									(
																																																									(bool_t)
																																																									0);
																																																							}
																																																						else
																																																							{	/* Unsafe/pregexp.scm 684 */
																																																								BgL_couldzd2loopzd2infinitelyzf3zf3_2021
																																																									=
																																																									(
																																																									(bool_t)
																																																									1);
																																																							}
																																																					}
																																																				else
																																																					{	/* Unsafe/pregexp.scm 684 */
																																																						BgL_couldzd2loopzd2infinitelyzf3zf3_2021
																																																							=
																																																							(
																																																							(bool_t)
																																																							0);
																																																					}
																																																				{	/* Unsafe/pregexp.scm 684 */
																																																					obj_t
																																																						BgL_rez00_2022;
																																																					BgL_rez00_2022
																																																						=
																																																						CAR
																																																						(CDR
																																																						(CDR
																																																							(CDR
																																																								(CDR
																																																									(BgL_rez00_1833)))));
																																																					{	/* Unsafe/pregexp.scm 685 */

																																																						return
																																																							BGl_z62loupzd2pzb0zz__regexpz00
																																																							(BgL_fkz00_1836,
																																																							BgL_rez00_2022,
																																																							BgL_couldzd2loopzd2infinitelyzf3zf3_2021,
																																																							BgL_pz00_2019,
																																																							BgL_qz00_2020,
																																																							BgL_maximalzf3zf3_2018,
																																																							BgL_skz00_1835,
																																																							BgL_startz00_4169,
																																																							BgL_sz00_4168,
																																																							BgL_backrefsz00_4167,
																																																							BgL_snz00_4166,
																																																							BgL_identityz00_4165,
																																																							BgL_casezd2sensitivezf3z21_4164,
																																																							BgL_nz00_4163,
																																																							0L,
																																																							BgL_iz00_1834);
																																																					}
																																																				}
																																																			}
																																																		}
																																																	}
																																																}
																																															else
																																																{	/* Unsafe/pregexp.scm 586 */
																																																	return
																																																		BGl_errorz00zz__errorz00
																																																		(BGl_string2500z00zz__regexpz00,
																																																		BGl_symbol2590z00zz__regexpz00,
																																																		BUNSPEC);
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																			else
																				{	/* Unsafe/pregexp.scm 719 */
																					bool_t BgL_test2886z00_6051;

																					{	/* Unsafe/pregexp.scm 719 */
																						long BgL_n1z00_3693;
																						long BgL_n2z00_3694;

																						BgL_n1z00_3693 =
																							(long) CINT(BgL_iz00_1834);
																						BgL_n2z00_3694 =
																							(long)
																							CINT(CELL_REF(BgL_nz00_4163));
																						BgL_test2886z00_6051 =
																							(BgL_n1z00_3693 >=
																							BgL_n2z00_3694);
																					}
																					if (BgL_test2886z00_6051)
																						{	/* Unsafe/pregexp.scm 719 */
																							return
																								BGL_PROCEDURE_CALL0
																								(BgL_fkz00_1836);
																						}
																					else
																						{	/* Unsafe/pregexp.scm 719 */
																							return
																								BGl_errorz00zz__errorz00
																								(BGl_string2500z00zz__regexpz00,
																								BGl_symbol2590z00zz__regexpz00,
																								BUNSPEC);
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &loup-seq */
	obj_t BGl_z62loupzd2seqzb0zz__regexpz00(obj_t BgL_skz00_4187,
		obj_t BgL_fkz00_4186, obj_t BgL_startz00_4185, obj_t BgL_sz00_4184,
		obj_t BgL_backrefsz00_4183, obj_t BgL_snz00_4182,
		obj_t BgL_identityz00_4181, obj_t BgL_casezd2sensitivezf3z21_4180,
		obj_t BgL_nz00_4179, obj_t BgL_resz00_3532, obj_t BgL_iz00_3533)
	{
		{	/* Unsafe/pregexp.scm 603 */
			if (NULLP(BgL_resz00_3532))
				{	/* Unsafe/pregexp.scm 604 */
					return BGL_PROCEDURE_CALL1(BgL_skz00_4187, BgL_iz00_3533);
				}
			else
				{	/* Unsafe/pregexp.scm 605 */
					obj_t BgL_arg1831z00_3540;

					BgL_arg1831z00_3540 = CAR(((obj_t) BgL_resz00_3532));
					{	/* Unsafe/pregexp.scm 607 */
						obj_t BgL_zc3z04anonymousza31833ze3z87_4141;

						BgL_zc3z04anonymousza31833ze3z87_4141 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31833ze3ze5zz__regexpz00, (int) (1L),
							(int) (10L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (0L),
							((obj_t) BgL_nz00_4179));
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (1L),
							((obj_t) BgL_casezd2sensitivezf3z21_4180));
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (2L),
							BgL_identityz00_4181);
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (3L),
							((obj_t) BgL_snz00_4182));
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (4L),
							BgL_backrefsz00_4183);
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (5L),
							BgL_sz00_4184);
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (6L),
							BgL_startz00_4185);
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (7L),
							BgL_fkz00_4186);
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (8L),
							BgL_skz00_4187);
						PROCEDURE_SET(BgL_zc3z04anonymousza31833ze3z87_4141, (int) (9L),
							BgL_resz00_3532);
						return BGl_z62subz62zz__regexpz00(BgL_startz00_4185, BgL_sz00_4184,
							BgL_backrefsz00_4183, BgL_snz00_4182, BgL_identityz00_4181,
							BgL_casezd2sensitivezf3z21_4180, BgL_nz00_4179,
							BgL_arg1831z00_3540, BgL_iz00_3533,
							BgL_zc3z04anonymousza31833ze3z87_4141, BgL_fkz00_4186);
					}
				}
		}

	}



/* &loup-or */
	obj_t BGl_z62loupzd2orzb0zz__regexpz00(obj_t BgL_fkz00_4197,
		obj_t BgL_iz00_4196, obj_t BgL_skz00_4195, obj_t BgL_startz00_4194,
		obj_t BgL_sz00_4193, obj_t BgL_backrefsz00_4192, obj_t BgL_snz00_4191,
		obj_t BgL_identityz00_4190, obj_t BgL_casezd2sensitivezf3z21_4189,
		obj_t BgL_nz00_4188, obj_t BgL_resz00_1923)
	{
		{	/* Unsafe/pregexp.scm 610 */
			if (NULLP(BgL_resz00_1923))
				{	/* Unsafe/pregexp.scm 611 */
					return BGL_PROCEDURE_CALL0(BgL_fkz00_4197);
				}
			else
				{	/* Unsafe/pregexp.scm 612 */
					obj_t BgL_arg1838z00_1926;

					BgL_arg1838z00_1926 = CAR(((obj_t) BgL_resz00_1923));
					{	/* Unsafe/pregexp.scm 614 */
						obj_t BgL_zc3z04anonymousza31841ze3z87_4139;
						obj_t BgL_zc3z04anonymousza31843ze3z87_4138;

						BgL_zc3z04anonymousza31841ze3z87_4139 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31841ze3ze5zz__regexpz00, (int) (1L),
							(int) (11L));
						BgL_zc3z04anonymousza31843ze3z87_4138 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31843ze3ze5zz__regexpz00, (int) (0L),
							(int) (11L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (0L),
							((obj_t) BgL_nz00_4188));
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (1L),
							((obj_t) BgL_casezd2sensitivezf3z21_4189));
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (2L),
							BgL_identityz00_4190);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (3L),
							((obj_t) BgL_snz00_4191));
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (4L),
							BgL_backrefsz00_4192);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (5L),
							BgL_sz00_4193);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (6L),
							BgL_startz00_4194);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (7L),
							BgL_iz00_4196);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (8L),
							BgL_fkz00_4197);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (9L),
							BgL_skz00_4195);
						PROCEDURE_SET(BgL_zc3z04anonymousza31841ze3z87_4139, (int) (10L),
							BgL_resz00_1923);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (0L),
							((obj_t) BgL_nz00_4188));
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (1L),
							((obj_t) BgL_casezd2sensitivezf3z21_4189));
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (2L),
							BgL_identityz00_4190);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (3L),
							((obj_t) BgL_snz00_4191));
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (4L),
							BgL_backrefsz00_4192);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (5L),
							BgL_sz00_4193);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (6L),
							BgL_startz00_4194);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (7L),
							BgL_skz00_4195);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (8L),
							BgL_iz00_4196);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (9L),
							BgL_fkz00_4197);
						PROCEDURE_SET(BgL_zc3z04anonymousza31843ze3z87_4138, (int) (10L),
							BgL_resz00_1923);
						return BGl_z62subz62zz__regexpz00(BgL_startz00_4194, BgL_sz00_4193,
							BgL_backrefsz00_4192, BgL_snz00_4191, BgL_identityz00_4190,
							BgL_casezd2sensitivezf3z21_4189, BgL_nz00_4188,
							BgL_arg1838z00_1926, BgL_iz00_4196,
							BgL_zc3z04anonymousza31841ze3z87_4139,
							BgL_zc3z04anonymousza31843ze3z87_4138);
					}
				}
		}

	}



/* &loup-p */
	obj_t BGl_z62loupzd2pzb0zz__regexpz00(obj_t BgL_fkz00_4211,
		obj_t BgL_rez00_4210, bool_t BgL_couldzd2loopzd2infinitelyzf3zf3_4209,
		obj_t BgL_pz00_4208, obj_t BgL_qz00_4207, bool_t BgL_maximalzf3zf3_4206,
		obj_t BgL_skz00_4205, obj_t BgL_startz00_4204, obj_t BgL_sz00_4203,
		obj_t BgL_backrefsz00_4202, obj_t BgL_snz00_4201,
		obj_t BgL_identityz00_4200, obj_t BgL_casezd2sensitivezf3z21_4199,
		obj_t BgL_nz00_4198, long BgL_kz00_2024, obj_t BgL_iz00_2025)
	{
		{	/* Unsafe/pregexp.scm 686 */
			if ((BgL_kz00_2024 < (long) CINT(BgL_pz00_4208)))
				{	/* Unsafe/pregexp.scm 690 */
					obj_t BgL_zc3z04anonymousza31907ze3z87_4136;

					BgL_zc3z04anonymousza31907ze3z87_4136 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31907ze3ze5zz__regexpz00,
						(int) (1L), (int) (16L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (0L), ((obj_t) BgL_nz00_4198));
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (1L), ((obj_t) BgL_casezd2sensitivezf3z21_4199));
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (2L), BgL_identityz00_4200);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (3L), ((obj_t) BgL_snz00_4201));
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (4L), BgL_backrefsz00_4202);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (5L), BgL_sz00_4203);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (6L), BgL_startz00_4204);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (7L), BgL_skz00_4205);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (8L), BBOOL(BgL_maximalzf3zf3_4206));
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (9L), BgL_qz00_4207);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (10L), BgL_pz00_4208);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (11L), BgL_rez00_4210);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (12L), BgL_fkz00_4211);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (13L), BgL_iz00_2025);
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (14L), BBOOL(BgL_couldzd2loopzd2infinitelyzf3zf3_4209));
					PROCEDURE_SET(BgL_zc3z04anonymousza31907ze3z87_4136,
						(int) (15L), BINT(BgL_kz00_2024));
					return
						BGl_z62subz62zz__regexpz00(BgL_startz00_4204, BgL_sz00_4203,
						BgL_backrefsz00_4202, BgL_snz00_4201, BgL_identityz00_4200,
						BgL_casezd2sensitivezf3z21_4199, BgL_nz00_4198, BgL_rez00_4210,
						BgL_iz00_2025, BgL_zc3z04anonymousza31907ze3z87_4136,
						BgL_fkz00_4211);
				}
			else
				{	/* Unsafe/pregexp.scm 697 */
					obj_t BgL_qz00_2035;

					if (CBOOL(BgL_qz00_4207))
						{	/* Unsafe/pregexp.scm 697 */
							BgL_qz00_2035 = SUBFX(BgL_qz00_4207, BgL_pz00_4208);
						}
					else
						{	/* Unsafe/pregexp.scm 697 */
							BgL_qz00_2035 = BFALSE;
						}
					return
						BGl_z62loupzd2qzb0zz__regexpz00(BgL_qz00_2035,
						BgL_maximalzf3zf3_4206, BgL_couldzd2loopzd2infinitelyzf3zf3_4209,
						BgL_rez00_4210, BgL_skz00_4205, BgL_startz00_4204, BgL_sz00_4203,
						BgL_backrefsz00_4202, BgL_snz00_4201, BgL_identityz00_4200,
						BgL_casezd2sensitivezf3z21_4199, BgL_nz00_4198, 0L, BgL_iz00_2025);
				}
		}

	}



/* &loup-q */
	obj_t BGl_z62loupzd2qzb0zz__regexpz00(obj_t BgL_qz00_4223,
		bool_t BgL_maximalzf3zf3_4222,
		bool_t BgL_couldzd2loopzd2infinitelyzf3zf3_4221, obj_t BgL_rez00_4220,
		obj_t BgL_skz00_4219, obj_t BgL_startz00_4218, obj_t BgL_sz00_4217,
		obj_t BgL_backrefsz00_4216, obj_t BgL_snz00_4215,
		obj_t BgL_identityz00_4214, obj_t BgL_casezd2sensitivezf3z21_4213,
		obj_t BgL_nz00_4212, long BgL_kz00_2037, obj_t BgL_iz00_2038)
	{
		{	/* Unsafe/pregexp.scm 698 */
			{	/* Unsafe/pregexp.scm 700 */
				obj_t BgL_fkz00_4133;

				BgL_fkz00_4133 =
					MAKE_FX_PROCEDURE(BGl_z62fkz62zz__regexpz00, (int) (0L), (int) (2L));
				PROCEDURE_SET(BgL_fkz00_4133, (int) (0L), BgL_skz00_4219);
				PROCEDURE_SET(BgL_fkz00_4133, (int) (1L), BgL_iz00_2038);
				{	/* Unsafe/pregexp.scm 701 */
					bool_t BgL_test2891z00_6214;

					if (CBOOL(BgL_qz00_4223))
						{	/* Unsafe/pregexp.scm 701 */
							BgL_test2891z00_6214 =
								(BgL_kz00_2037 >= (long) CINT(BgL_qz00_4223));
						}
					else
						{	/* Unsafe/pregexp.scm 701 */
							BgL_test2891z00_6214 = ((bool_t) 0);
						}
					if (BgL_test2891z00_6214)
						{	/* Unsafe/pregexp.scm 701 */
							return BGL_PROCEDURE_CALL1(BgL_skz00_4219, BgL_iz00_2038);
						}
					else
						{	/* Unsafe/pregexp.scm 701 */
							if (BgL_maximalzf3zf3_4222)
								{	/* Unsafe/pregexp.scm 705 */
									obj_t BgL_zc3z04anonymousza31914ze3z87_4132;

									BgL_zc3z04anonymousza31914ze3z87_4132 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31914ze3ze5zz__regexpz00,
										(int) (1L), (int) (14L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (0L), ((obj_t) BgL_nz00_4212));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (1L), ((obj_t) BgL_casezd2sensitivezf3z21_4213));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (2L), BgL_identityz00_4214);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (3L), ((obj_t) BgL_snz00_4215));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (4L), BgL_backrefsz00_4216);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (5L), BgL_sz00_4217);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (6L), BgL_startz00_4218);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (7L), BgL_rez00_4220);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (8L), BBOOL(BgL_maximalzf3zf3_4222));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (9L), BgL_qz00_4223);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (10L), BgL_iz00_2038);
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (11L),
										BBOOL(BgL_couldzd2loopzd2infinitelyzf3zf3_4221));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (12L), BINT(BgL_kz00_2037));
									PROCEDURE_SET(BgL_zc3z04anonymousza31914ze3z87_4132,
										(int) (13L), BgL_skz00_4219);
									return BGl_z62subz62zz__regexpz00(BgL_startz00_4218,
										BgL_sz00_4217, BgL_backrefsz00_4216, BgL_snz00_4215,
										BgL_identityz00_4214, BgL_casezd2sensitivezf3z21_4213,
										BgL_nz00_4212, BgL_rez00_4220, BgL_iz00_2038,
										BgL_zc3z04anonymousza31914ze3z87_4132, BgL_fkz00_4133);
								}
							else
								{	/* Unsafe/pregexp.scm 713 */
									obj_t BgL__ortest_1116z00_2050;

									BgL__ortest_1116z00_2050 =
										BGL_PROCEDURE_CALL1(BgL_skz00_4219, BgL_iz00_2038);
									if (CBOOL(BgL__ortest_1116z00_2050))
										{	/* Unsafe/pregexp.scm 713 */
											return BgL__ortest_1116z00_2050;
										}
									else
										{	/* Unsafe/pregexp.scm 716 */
											obj_t BgL_zc3z04anonymousza31919ze3z87_4134;

											BgL_zc3z04anonymousza31919ze3z87_4134 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31919ze3ze5zz__regexpz00,
												(int) (1L), (int) (13L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (0L), ((obj_t) BgL_nz00_4212));
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (1L), ((obj_t) BgL_casezd2sensitivezf3z21_4213));
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (2L), BgL_identityz00_4214);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (3L), ((obj_t) BgL_snz00_4215));
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (4L), BgL_backrefsz00_4216);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (5L), BgL_sz00_4217);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (6L), BgL_startz00_4218);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (7L), BgL_skz00_4219);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (8L), BgL_rez00_4220);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (9L),
												BBOOL(BgL_couldzd2loopzd2infinitelyzf3zf3_4221));
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (10L), BBOOL(BgL_maximalzf3zf3_4222));
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (11L), BgL_qz00_4223);
											PROCEDURE_SET(BgL_zc3z04anonymousza31919ze3z87_4134,
												(int) (12L), BINT(BgL_kz00_2037));
											return BGl_z62subz62zz__regexpz00(BgL_startz00_4218,
												BgL_sz00_4217, BgL_backrefsz00_4216, BgL_snz00_4215,
												BgL_identityz00_4214, BgL_casezd2sensitivezf3z21_4213,
												BgL_nz00_4212, BgL_rez00_4220, BgL_iz00_2038,
												BgL_zc3z04anonymousza31919ze3z87_4134, BgL_fkz00_4133);
										}
								}
						}
				}
			}
		}

	}



/* &g1104 */
	obj_t BGl_z62g1104z62zz__regexpz00(obj_t BgL_envz00_4224)
	{
		{	/* Unsafe/pregexp.scm 550 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1816> */
	obj_t BGl_z62zc3z04anonymousza31816ze3ze5zz__regexpz00(obj_t BgL_envz00_4225)
	{
		{	/* Unsafe/pregexp.scm 595 */
			{	/* Unsafe/pregexp.scm 596 */
				obj_t BgL_nz00_4226;
				obj_t BgL_casezd2sensitivezf3z21_4227;
				obj_t BgL_identityz00_4228;
				obj_t BgL_snz00_4229;
				obj_t BgL_backrefsz00_4230;
				obj_t BgL_sz00_4231;
				obj_t BgL_startz00_4232;
				obj_t BgL_iz00_4233;
				obj_t BgL_skz00_4234;
				obj_t BgL_fkz00_4235;
				obj_t BgL_charsz00_4236;

				BgL_nz00_4226 = PROCEDURE_REF(BgL_envz00_4225, (int) (0L));
				BgL_casezd2sensitivezf3z21_4227 =
					PROCEDURE_REF(BgL_envz00_4225, (int) (1L));
				BgL_identityz00_4228 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4225, (int) (2L)));
				BgL_snz00_4229 = PROCEDURE_REF(BgL_envz00_4225, (int) (3L));
				BgL_backrefsz00_4230 = PROCEDURE_REF(BgL_envz00_4225, (int) (4L));
				BgL_sz00_4231 = ((obj_t) PROCEDURE_REF(BgL_envz00_4225, (int) (5L)));
				BgL_startz00_4232 = PROCEDURE_REF(BgL_envz00_4225, (int) (6L));
				BgL_iz00_4233 = PROCEDURE_REF(BgL_envz00_4225, (int) (7L));
				BgL_skz00_4234 = PROCEDURE_REF(BgL_envz00_4225, (int) (8L));
				BgL_fkz00_4235 = PROCEDURE_REF(BgL_envz00_4225, (int) (9L));
				BgL_charsz00_4236 = PROCEDURE_REF(BgL_envz00_4225, (int) (10L));
				{	/* Unsafe/pregexp.scm 596 */
					obj_t BgL_arg1817z00_4485;

					BgL_arg1817z00_4485 = CDR(((obj_t) BgL_charsz00_4236));
					return
						BGl_z62loupzd2onezd2ofzd2charszb0zz__regexpz00(BgL_fkz00_4235,
						BgL_skz00_4234, BgL_iz00_4233, BgL_startz00_4232, BgL_sz00_4231,
						BgL_backrefsz00_4230, BgL_snz00_4229, BgL_identityz00_4228,
						BgL_casezd2sensitivezf3z21_4227, BgL_nz00_4226,
						BgL_arg1817z00_4485);
				}
			}
		}

	}



/* &<@anonymous:1825> */
	obj_t BGl_z62zc3z04anonymousza31825ze3ze5zz__regexpz00(obj_t BgL_envz00_4237)
	{
		{	/* Unsafe/pregexp.scm 601 */
			{	/* Unsafe/pregexp.scm 601 */
				obj_t BgL_iz00_4238;
				obj_t BgL_skz00_4239;

				BgL_iz00_4238 = PROCEDURE_REF(BgL_envz00_4237, (int) (0L));
				BgL_skz00_4239 = PROCEDURE_REF(BgL_envz00_4237, (int) (1L));
				{	/* Unsafe/pregexp.scm 601 */
					long BgL_arg1826z00_4486;

					BgL_arg1826z00_4486 = ((long) CINT(BgL_iz00_4238) + 1L);
					return BGL_PROCEDURE_CALL1(BgL_skz00_4239, BINT(BgL_arg1826z00_4486));
				}
			}
		}

	}



/* &<@anonymous:1824> */
	obj_t BGl_z62zc3z04anonymousza31824ze3ze5zz__regexpz00(obj_t BgL_envz00_4240,
		obj_t BgL_i1z00_4242)
	{
		{	/* Unsafe/pregexp.scm 600 */
			{	/* Unsafe/pregexp.scm 600 */
				obj_t BgL_fkz00_4241;

				BgL_fkz00_4241 = PROCEDURE_REF(BgL_envz00_4240, (int) (0L));
				return BGL_PROCEDURE_CALL0(BgL_fkz00_4241);
			}
		}

	}



/* &<@anonymous:1833> */
	obj_t BGl_z62zc3z04anonymousza31833ze3ze5zz__regexpz00(obj_t BgL_envz00_4243,
		obj_t BgL_i1z00_4254)
	{
		{	/* Unsafe/pregexp.scm 606 */
			{	/* Unsafe/pregexp.scm 607 */
				obj_t BgL_nz00_4244;
				obj_t BgL_casezd2sensitivezf3z21_4245;
				obj_t BgL_identityz00_4246;
				obj_t BgL_snz00_4247;
				obj_t BgL_backrefsz00_4248;
				obj_t BgL_sz00_4249;
				obj_t BgL_startz00_4250;
				obj_t BgL_fkz00_4251;
				obj_t BgL_skz00_4252;
				obj_t BgL_resz00_4253;

				BgL_nz00_4244 = PROCEDURE_REF(BgL_envz00_4243, (int) (0L));
				BgL_casezd2sensitivezf3z21_4245 =
					PROCEDURE_REF(BgL_envz00_4243, (int) (1L));
				BgL_identityz00_4246 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4243, (int) (2L)));
				BgL_snz00_4247 = PROCEDURE_REF(BgL_envz00_4243, (int) (3L));
				BgL_backrefsz00_4248 = PROCEDURE_REF(BgL_envz00_4243, (int) (4L));
				BgL_sz00_4249 = ((obj_t) PROCEDURE_REF(BgL_envz00_4243, (int) (5L)));
				BgL_startz00_4250 = PROCEDURE_REF(BgL_envz00_4243, (int) (6L));
				BgL_fkz00_4251 = PROCEDURE_REF(BgL_envz00_4243, (int) (7L));
				BgL_skz00_4252 = PROCEDURE_REF(BgL_envz00_4243, (int) (8L));
				BgL_resz00_4253 = PROCEDURE_REF(BgL_envz00_4243, (int) (9L));
				{	/* Unsafe/pregexp.scm 607 */
					obj_t BgL_arg1834z00_4487;

					BgL_arg1834z00_4487 = CDR(((obj_t) BgL_resz00_4253));
					return
						BGl_z62loupzd2seqzb0zz__regexpz00(BgL_skz00_4252, BgL_fkz00_4251,
						BgL_startz00_4250, BgL_sz00_4249, BgL_backrefsz00_4248,
						BgL_snz00_4247, BgL_identityz00_4246,
						BgL_casezd2sensitivezf3z21_4245, BgL_nz00_4244, BgL_arg1834z00_4487,
						BgL_i1z00_4254);
				}
			}
		}

	}



/* &<@anonymous:1843> */
	obj_t BGl_z62zc3z04anonymousza31843ze3ze5zz__regexpz00(obj_t BgL_envz00_4255)
	{
		{	/* Unsafe/pregexp.scm 616 */
			{	/* Unsafe/pregexp.scm 616 */
				obj_t BgL_nz00_4256;
				obj_t BgL_casezd2sensitivezf3z21_4257;
				obj_t BgL_identityz00_4258;
				obj_t BgL_snz00_4259;
				obj_t BgL_backrefsz00_4260;
				obj_t BgL_sz00_4261;
				obj_t BgL_startz00_4262;
				obj_t BgL_skz00_4263;
				obj_t BgL_iz00_4264;
				obj_t BgL_fkz00_4265;
				obj_t BgL_resz00_4266;

				BgL_nz00_4256 = PROCEDURE_REF(BgL_envz00_4255, (int) (0L));
				BgL_casezd2sensitivezf3z21_4257 =
					PROCEDURE_REF(BgL_envz00_4255, (int) (1L));
				BgL_identityz00_4258 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4255, (int) (2L)));
				BgL_snz00_4259 = PROCEDURE_REF(BgL_envz00_4255, (int) (3L));
				BgL_backrefsz00_4260 = PROCEDURE_REF(BgL_envz00_4255, (int) (4L));
				BgL_sz00_4261 = ((obj_t) PROCEDURE_REF(BgL_envz00_4255, (int) (5L)));
				BgL_startz00_4262 = PROCEDURE_REF(BgL_envz00_4255, (int) (6L));
				BgL_skz00_4263 = PROCEDURE_REF(BgL_envz00_4255, (int) (7L));
				BgL_iz00_4264 = PROCEDURE_REF(BgL_envz00_4255, (int) (8L));
				BgL_fkz00_4265 = PROCEDURE_REF(BgL_envz00_4255, (int) (9L));
				BgL_resz00_4266 = PROCEDURE_REF(BgL_envz00_4255, (int) (10L));
				{	/* Unsafe/pregexp.scm 616 */
					obj_t BgL_arg1844z00_4488;

					BgL_arg1844z00_4488 = CDR(((obj_t) BgL_resz00_4266));
					return
						BGl_z62loupzd2orzb0zz__regexpz00(BgL_fkz00_4265, BgL_iz00_4264,
						BgL_skz00_4263, BgL_startz00_4262, BgL_sz00_4261,
						BgL_backrefsz00_4260, BgL_snz00_4259, BgL_identityz00_4258,
						BgL_casezd2sensitivezf3z21_4257, BgL_nz00_4256,
						BgL_arg1844z00_4488);
				}
			}
		}

	}



/* &<@anonymous:1841> */
	obj_t BGl_z62zc3z04anonymousza31841ze3ze5zz__regexpz00(obj_t BgL_envz00_4267,
		obj_t BgL_i1z00_4279)
	{
		{	/* Unsafe/pregexp.scm 613 */
			{	/* Unsafe/pregexp.scm 614 */
				obj_t BgL_nz00_4268;
				obj_t BgL_casezd2sensitivezf3z21_4269;
				obj_t BgL_identityz00_4270;
				obj_t BgL_snz00_4271;
				obj_t BgL_backrefsz00_4272;
				obj_t BgL_sz00_4273;
				obj_t BgL_startz00_4274;
				obj_t BgL_iz00_4275;
				obj_t BgL_fkz00_4276;
				obj_t BgL_skz00_4277;
				obj_t BgL_resz00_4278;

				BgL_nz00_4268 = PROCEDURE_REF(BgL_envz00_4267, (int) (0L));
				BgL_casezd2sensitivezf3z21_4269 =
					PROCEDURE_REF(BgL_envz00_4267, (int) (1L));
				BgL_identityz00_4270 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4267, (int) (2L)));
				BgL_snz00_4271 = PROCEDURE_REF(BgL_envz00_4267, (int) (3L));
				BgL_backrefsz00_4272 = PROCEDURE_REF(BgL_envz00_4267, (int) (4L));
				BgL_sz00_4273 = ((obj_t) PROCEDURE_REF(BgL_envz00_4267, (int) (5L)));
				BgL_startz00_4274 = PROCEDURE_REF(BgL_envz00_4267, (int) (6L));
				BgL_iz00_4275 = PROCEDURE_REF(BgL_envz00_4267, (int) (7L));
				BgL_fkz00_4276 = PROCEDURE_REF(BgL_envz00_4267, (int) (8L));
				BgL_skz00_4277 = PROCEDURE_REF(BgL_envz00_4267, (int) (9L));
				BgL_resz00_4278 = PROCEDURE_REF(BgL_envz00_4267, (int) (10L));
				{	/* Unsafe/pregexp.scm 614 */
					obj_t BgL__ortest_1109z00_4489;

					BgL__ortest_1109z00_4489 =
						BGL_PROCEDURE_CALL1(BgL_skz00_4277, BgL_i1z00_4279);
					if (CBOOL(BgL__ortest_1109z00_4489))
						{	/* Unsafe/pregexp.scm 614 */
							return BgL__ortest_1109z00_4489;
						}
					else
						{	/* Unsafe/pregexp.scm 615 */
							obj_t BgL_arg1842z00_4490;

							BgL_arg1842z00_4490 = CDR(((obj_t) BgL_resz00_4278));
							return
								BGl_z62loupzd2orzb0zz__regexpz00(BgL_fkz00_4276, BgL_iz00_4275,
								BgL_skz00_4277, BgL_startz00_4274, BgL_sz00_4273,
								BgL_backrefsz00_4272, BgL_snz00_4271, BgL_identityz00_4270,
								BgL_casezd2sensitivezf3z21_4269, BgL_nz00_4268,
								BgL_arg1842z00_4490);
						}
				}
			}
		}

	}



/* &<@anonymous:1858> */
	obj_t BGl_z62zc3z04anonymousza31858ze3ze5zz__regexpz00(obj_t BgL_envz00_4280,
		obj_t BgL_i1z00_4285)
	{
		{	/* Unsafe/pregexp.scm 632 */
			{	/* Unsafe/pregexp.scm 633 */
				obj_t BgL_rez00_4281;
				obj_t BgL_backrefsz00_4282;
				obj_t BgL_iz00_4283;
				obj_t BgL_skz00_4284;

				BgL_rez00_4281 = PROCEDURE_REF(BgL_envz00_4280, (int) (0L));
				BgL_backrefsz00_4282 = PROCEDURE_REF(BgL_envz00_4280, (int) (1L));
				BgL_iz00_4283 = PROCEDURE_REF(BgL_envz00_4280, (int) (2L));
				BgL_skz00_4284 = PROCEDURE_REF(BgL_envz00_4280, (int) (3L));
				{	/* Unsafe/pregexp.scm 633 */
					obj_t BgL_arg1859z00_4491;
					obj_t BgL_arg1860z00_4492;

					BgL_arg1859z00_4491 =
						BGl_assvz00zz__r4_pairs_and_lists_6_3z00(BgL_rez00_4281,
						BgL_backrefsz00_4282);
					BgL_arg1860z00_4492 = MAKE_YOUNG_PAIR(BgL_iz00_4283, BgL_i1z00_4285);
					{	/* Unsafe/pregexp.scm 633 */
						obj_t BgL_tmpz00_6443;

						BgL_tmpz00_6443 = ((obj_t) BgL_arg1859z00_4491);
						SET_CDR(BgL_tmpz00_6443, BgL_arg1860z00_4492);
				}}
				return BGL_PROCEDURE_CALL1(BgL_skz00_4284, BgL_i1z00_4285);
			}
		}

	}



/* &<@anonymous:1864> */
	obj_t BGl_z62zc3z04anonymousza31864ze3ze5zz__regexpz00(obj_t BgL_envz00_4286)
	{
		{	/* Unsafe/pregexp.scm 638 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1869> */
	obj_t BGl_z62zc3z04anonymousza31869ze3ze5zz__regexpz00(obj_t BgL_envz00_4287)
	{
		{	/* Unsafe/pregexp.scm 643 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1879> */
	obj_t BGl_z62zc3z04anonymousza31879ze3ze5zz__regexpz00(obj_t BgL_envz00_4288)
	{
		{	/* Unsafe/pregexp.scm 651 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1890> */
	obj_t BGl_z62zc3z04anonymousza31890ze3ze5zz__regexpz00(obj_t BgL_envz00_4289)
	{
		{	/* Unsafe/pregexp.scm 660 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1894> */
	obj_t BGl_z62zc3z04anonymousza31894ze3ze5zz__regexpz00(obj_t BgL_envz00_4290)
	{
		{	/* Unsafe/pregexp.scm 665 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &identity */
	obj_t BGl_z62identityz62zz__regexpz00(obj_t BgL_envz00_4291,
		obj_t BgL_xz00_4292)
	{
		{	/* Unsafe/pregexp.scm 547 */
			return BgL_xz00_4292;
		}

	}



/* &<@anonymous:1901> */
	obj_t BGl_z62zc3z04anonymousza31901ze3ze5zz__regexpz00(obj_t BgL_envz00_4293)
	{
		{	/* Unsafe/pregexp.scm 677 */
			{	/* Unsafe/pregexp.scm 678 */
				obj_t BgL_oldz00_4294;
				obj_t BgL_casezd2sensitivezf3z21_4295;
				obj_t BgL_fkz00_4296;

				BgL_oldz00_4294 = PROCEDURE_REF(BgL_envz00_4293, (int) (0L));
				BgL_casezd2sensitivezf3z21_4295 =
					PROCEDURE_REF(BgL_envz00_4293, (int) (1L));
				BgL_fkz00_4296 = PROCEDURE_REF(BgL_envz00_4293, (int) (2L));
				CELL_SET(BgL_casezd2sensitivezf3z21_4295, BgL_oldz00_4294);
				return BGL_PROCEDURE_CALL0(BgL_fkz00_4296);
			}
		}

	}



/* &<@anonymous:1900> */
	obj_t BGl_z62zc3z04anonymousza31900ze3ze5zz__regexpz00(obj_t BgL_envz00_4298,
		obj_t BgL_i1z00_4302)
	{
		{	/* Unsafe/pregexp.scm 674 */
			{	/* Unsafe/pregexp.scm 675 */
				obj_t BgL_oldz00_4299;
				obj_t BgL_casezd2sensitivezf3z21_4300;
				obj_t BgL_skz00_4301;

				BgL_oldz00_4299 = PROCEDURE_REF(BgL_envz00_4298, (int) (0L));
				BgL_casezd2sensitivezf3z21_4300 =
					PROCEDURE_REF(BgL_envz00_4298, (int) (1L));
				BgL_skz00_4301 = PROCEDURE_REF(BgL_envz00_4298, (int) (2L));
				CELL_SET(BgL_casezd2sensitivezf3z21_4300, BgL_oldz00_4299);
				return BGL_PROCEDURE_CALL1(BgL_skz00_4301, BgL_i1z00_4302);
			}
		}

	}



/* &<@anonymous:1907> */
	obj_t BGl_z62zc3z04anonymousza31907ze3ze5zz__regexpz00(obj_t BgL_envz00_4304,
		obj_t BgL_i1z00_4321)
	{
		{	/* Unsafe/pregexp.scm 689 */
			{	/* Unsafe/pregexp.scm 690 */
				obj_t BgL_nz00_4305;
				obj_t BgL_casezd2sensitivezf3z21_4306;
				obj_t BgL_identityz00_4307;
				obj_t BgL_snz00_4308;
				obj_t BgL_backrefsz00_4309;
				obj_t BgL_sz00_4310;
				obj_t BgL_startz00_4311;
				obj_t BgL_skz00_4312;
				bool_t BgL_maximalzf3zf3_4313;
				obj_t BgL_qz00_4314;
				obj_t BgL_pz00_4315;
				obj_t BgL_rez00_4316;
				obj_t BgL_fkz00_4317;
				obj_t BgL_iz00_4318;
				bool_t BgL_couldzd2loopzd2infinitelyzf3zf3_4319;
				long BgL_kz00_4320;

				BgL_nz00_4305 = PROCEDURE_REF(BgL_envz00_4304, (int) (0L));
				BgL_casezd2sensitivezf3z21_4306 =
					PROCEDURE_REF(BgL_envz00_4304, (int) (1L));
				BgL_identityz00_4307 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4304, (int) (2L)));
				BgL_snz00_4308 = PROCEDURE_REF(BgL_envz00_4304, (int) (3L));
				BgL_backrefsz00_4309 = PROCEDURE_REF(BgL_envz00_4304, (int) (4L));
				BgL_sz00_4310 = ((obj_t) PROCEDURE_REF(BgL_envz00_4304, (int) (5L)));
				BgL_startz00_4311 = PROCEDURE_REF(BgL_envz00_4304, (int) (6L));
				BgL_skz00_4312 = PROCEDURE_REF(BgL_envz00_4304, (int) (7L));
				BgL_maximalzf3zf3_4313 =
					CBOOL(PROCEDURE_REF(BgL_envz00_4304, (int) (8L)));
				BgL_qz00_4314 = PROCEDURE_REF(BgL_envz00_4304, (int) (9L));
				BgL_pz00_4315 = PROCEDURE_REF(BgL_envz00_4304, (int) (10L));
				BgL_rez00_4316 = PROCEDURE_REF(BgL_envz00_4304, (int) (11L));
				BgL_fkz00_4317 = PROCEDURE_REF(BgL_envz00_4304, (int) (12L));
				BgL_iz00_4318 = PROCEDURE_REF(BgL_envz00_4304, (int) (13L));
				BgL_couldzd2loopzd2infinitelyzf3zf3_4319 =
					CBOOL(PROCEDURE_REF(BgL_envz00_4304, (int) (14L)));
				BgL_kz00_4320 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4304, (int) (15L)));
				{	/* Unsafe/pregexp.scm 690 */
					bool_t BgL_test2896z00_6511;

					if (BgL_couldzd2loopzd2infinitelyzf3zf3_4319)
						{	/* Unsafe/pregexp.scm 690 */
							BgL_test2896z00_6511 =
								((long) CINT(BgL_i1z00_4321) == (long) CINT(BgL_iz00_4318));
						}
					else
						{	/* Unsafe/pregexp.scm 690 */
							BgL_test2896z00_6511 = ((bool_t) 0);
						}
					if (BgL_test2896z00_6511)
						{	/* Unsafe/pregexp.scm 692 */
							obj_t BgL_list1909z00_4493;

							BgL_list1909z00_4493 =
								MAKE_YOUNG_PAIR(BGl_symbol2600z00zz__regexpz00, BNIL);
							BGl_errorz00zz__errorz00(BGl_string2500z00zz__regexpz00,
								BGl_symbol2590z00zz__regexpz00, CAR(BgL_list1909z00_4493));
						}
					else
						{	/* Unsafe/pregexp.scm 690 */
							BFALSE;
						}
				}
				return
					BGl_z62loupzd2pzb0zz__regexpz00(BgL_fkz00_4317, BgL_rez00_4316,
					BgL_couldzd2loopzd2infinitelyzf3zf3_4319, BgL_pz00_4315,
					BgL_qz00_4314, BgL_maximalzf3zf3_4313, BgL_skz00_4312,
					BgL_startz00_4311, BgL_sz00_4310, BgL_backrefsz00_4309,
					BgL_snz00_4308, BgL_identityz00_4307, BgL_casezd2sensitivezf3z21_4306,
					BgL_nz00_4305, (BgL_kz00_4320 + 1L), BgL_i1z00_4321);
			}
		}

	}



/* &<@anonymous:1914> */
	obj_t BGl_z62zc3z04anonymousza31914ze3ze5zz__regexpz00(obj_t BgL_envz00_4322,
		obj_t BgL_i1z00_4337)
	{
		{	/* Unsafe/pregexp.scm 704 */
			{	/* Unsafe/pregexp.scm 705 */
				obj_t BgL_nz00_4323;
				obj_t BgL_casezd2sensitivezf3z21_4324;
				obj_t BgL_identityz00_4325;
				obj_t BgL_snz00_4326;
				obj_t BgL_backrefsz00_4327;
				obj_t BgL_sz00_4328;
				obj_t BgL_startz00_4329;
				obj_t BgL_rez00_4330;
				bool_t BgL_maximalzf3zf3_4331;
				obj_t BgL_qz00_4332;
				obj_t BgL_iz00_4333;
				bool_t BgL_couldzd2loopzd2infinitelyzf3zf3_4334;
				long BgL_kz00_4335;
				obj_t BgL_skz00_4336;

				BgL_nz00_4323 = PROCEDURE_REF(BgL_envz00_4322, (int) (0L));
				BgL_casezd2sensitivezf3z21_4324 =
					PROCEDURE_REF(BgL_envz00_4322, (int) (1L));
				BgL_identityz00_4325 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4322, (int) (2L)));
				BgL_snz00_4326 = PROCEDURE_REF(BgL_envz00_4322, (int) (3L));
				BgL_backrefsz00_4327 = PROCEDURE_REF(BgL_envz00_4322, (int) (4L));
				BgL_sz00_4328 = ((obj_t) PROCEDURE_REF(BgL_envz00_4322, (int) (5L)));
				BgL_startz00_4329 = PROCEDURE_REF(BgL_envz00_4322, (int) (6L));
				BgL_rez00_4330 = PROCEDURE_REF(BgL_envz00_4322, (int) (7L));
				BgL_maximalzf3zf3_4331 =
					CBOOL(PROCEDURE_REF(BgL_envz00_4322, (int) (8L)));
				BgL_qz00_4332 = PROCEDURE_REF(BgL_envz00_4322, (int) (9L));
				BgL_iz00_4333 = PROCEDURE_REF(BgL_envz00_4322, (int) (10L));
				BgL_couldzd2loopzd2infinitelyzf3zf3_4334 =
					CBOOL(PROCEDURE_REF(BgL_envz00_4322, (int) (11L)));
				BgL_kz00_4335 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4322, (int) (12L)));
				BgL_skz00_4336 = PROCEDURE_REF(BgL_envz00_4322, (int) (13L));
				{	/* Unsafe/pregexp.scm 705 */
					bool_t BgL_test2898z00_6554;

					if (BgL_couldzd2loopzd2infinitelyzf3zf3_4334)
						{	/* Unsafe/pregexp.scm 705 */
							BgL_test2898z00_6554 =
								((long) CINT(BgL_i1z00_4337) == (long) CINT(BgL_iz00_4333));
						}
					else
						{	/* Unsafe/pregexp.scm 705 */
							BgL_test2898z00_6554 = ((bool_t) 0);
						}
					if (BgL_test2898z00_6554)
						{	/* Unsafe/pregexp.scm 707 */
							obj_t BgL_list1916z00_4495;

							BgL_list1916z00_4495 =
								MAKE_YOUNG_PAIR(BGl_symbol2600z00zz__regexpz00, BNIL);
							BGl_errorz00zz__errorz00(BGl_string2500z00zz__regexpz00,
								BGl_symbol2590z00zz__regexpz00, CAR(BgL_list1916z00_4495));
						}
					else
						{	/* Unsafe/pregexp.scm 705 */
							BFALSE;
						}
				}
				{	/* Unsafe/pregexp.scm 710 */
					obj_t BgL__ortest_1115z00_4497;

					BgL__ortest_1115z00_4497 =
						BGl_z62loupzd2qzb0zz__regexpz00(BgL_qz00_4332,
						BgL_maximalzf3zf3_4331, BgL_couldzd2loopzd2infinitelyzf3zf3_4334,
						BgL_rez00_4330, BgL_skz00_4336, BgL_startz00_4329, BgL_sz00_4328,
						BgL_backrefsz00_4327, BgL_snz00_4326, BgL_identityz00_4325,
						BgL_casezd2sensitivezf3z21_4324, BgL_nz00_4323,
						(BgL_kz00_4335 + 1L), BgL_i1z00_4337);
					if (CBOOL(BgL__ortest_1115z00_4497))
						{	/* Unsafe/pregexp.scm 710 */
							return BgL__ortest_1115z00_4497;
						}
					else
						{	/* Unsafe/pregexp.scm 710 */
							return BGL_PROCEDURE_CALL1(BgL_skz00_4336, BgL_iz00_4333);
						}
				}
			}
		}

	}



/* &fk */
	obj_t BGl_z62fkz62zz__regexpz00(obj_t BgL_envz00_4338)
	{
		{	/* Unsafe/pregexp.scm 699 */
			{	/* Unsafe/pregexp.scm 700 */
				obj_t BgL_skz00_4339;
				obj_t BgL_iz00_4340;

				BgL_skz00_4339 = PROCEDURE_REF(BgL_envz00_4338, (int) (0L));
				BgL_iz00_4340 = PROCEDURE_REF(BgL_envz00_4338, (int) (1L));
				return BGL_PROCEDURE_CALL1(BgL_skz00_4339, BgL_iz00_4340);
			}
		}

	}



/* &<@anonymous:1919> */
	obj_t BGl_z62zc3z04anonymousza31919ze3ze5zz__regexpz00(obj_t BgL_envz00_4341,
		obj_t BgL_i1z00_4355)
	{
		{	/* Unsafe/pregexp.scm 715 */
			{	/* Unsafe/pregexp.scm 716 */
				obj_t BgL_nz00_4342;
				obj_t BgL_casezd2sensitivezf3z21_4343;
				obj_t BgL_identityz00_4344;
				obj_t BgL_snz00_4345;
				obj_t BgL_backrefsz00_4346;
				obj_t BgL_sz00_4347;
				obj_t BgL_startz00_4348;
				obj_t BgL_skz00_4349;
				obj_t BgL_rez00_4350;
				bool_t BgL_couldzd2loopzd2infinitelyzf3zf3_4351;
				bool_t BgL_maximalzf3zf3_4352;
				obj_t BgL_qz00_4353;
				long BgL_kz00_4354;

				BgL_nz00_4342 = PROCEDURE_REF(BgL_envz00_4341, (int) (0L));
				BgL_casezd2sensitivezf3z21_4343 =
					PROCEDURE_REF(BgL_envz00_4341, (int) (1L));
				BgL_identityz00_4344 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4341, (int) (2L)));
				BgL_snz00_4345 = PROCEDURE_REF(BgL_envz00_4341, (int) (3L));
				BgL_backrefsz00_4346 = PROCEDURE_REF(BgL_envz00_4341, (int) (4L));
				BgL_sz00_4347 = ((obj_t) PROCEDURE_REF(BgL_envz00_4341, (int) (5L)));
				BgL_startz00_4348 = PROCEDURE_REF(BgL_envz00_4341, (int) (6L));
				BgL_skz00_4349 = PROCEDURE_REF(BgL_envz00_4341, (int) (7L));
				BgL_rez00_4350 = PROCEDURE_REF(BgL_envz00_4341, (int) (8L));
				BgL_couldzd2loopzd2infinitelyzf3zf3_4351 =
					CBOOL(PROCEDURE_REF(BgL_envz00_4341, (int) (9L)));
				BgL_maximalzf3zf3_4352 =
					CBOOL(PROCEDURE_REF(BgL_envz00_4341, (int) (10L)));
				BgL_qz00_4353 = PROCEDURE_REF(BgL_envz00_4341, (int) (11L));
				BgL_kz00_4354 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4341, (int) (12L)));
				return
					BGl_z62loupzd2qzb0zz__regexpz00(BgL_qz00_4353, BgL_maximalzf3zf3_4352,
					BgL_couldzd2loopzd2infinitelyzf3zf3_4351, BgL_rez00_4350,
					BgL_skz00_4349, BgL_startz00_4348, BgL_sz00_4347,
					BgL_backrefsz00_4346, BgL_snz00_4345, BgL_identityz00_4344,
					BgL_casezd2sensitivezf3z21_4343, BgL_nz00_4342, (BgL_kz00_4354 + 1L),
					BgL_i1z00_4355);
			}
		}

	}



/* pregexp-replace-aux */
	obj_t BGl_pregexpzd2replacezd2auxz00zz__regexpz00(obj_t BgL_strz00_64,
		obj_t BgL_insz00_65, long BgL_nz00_66, obj_t BgL_backrefsz00_67)
	{
		{	/* Unsafe/pregexp.scm 725 */
			{
				obj_t BgL_iz00_2098;
				obj_t BgL_rz00_2099;

				BgL_iz00_2098 = BINT(0L);
				BgL_rz00_2099 = BGl_string2602z00zz__regexpz00;
			BgL_zc3z04anonymousza31939ze3z87_2100:
				if (((long) CINT(BgL_iz00_2098) >= BgL_nz00_66))
					{	/* Unsafe/pregexp.scm 728 */
						return BgL_rz00_2099;
					}
				else
					{	/* Unsafe/pregexp.scm 729 */
						unsigned char BgL_cz00_2102;

						BgL_cz00_2102 =
							STRING_REF(BgL_insz00_65, (long) CINT(BgL_iz00_2098));
						if ((BgL_cz00_2102 == ((unsigned char) '\\')))
							{	/* Unsafe/pregexp.scm 731 */
								obj_t BgL_brzd2izd2_2104;

								BgL_brzd2izd2_2104 =
									BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00
									(BgL_insz00_65, BgL_iz00_2098, BgL_nz00_66);
								{	/* Unsafe/pregexp.scm 731 */
									obj_t BgL_brz00_2105;

									if (CBOOL(BgL_brzd2izd2_2104))
										{	/* Unsafe/pregexp.scm 732 */
											BgL_brz00_2105 = CAR(((obj_t) BgL_brzd2izd2_2104));
										}
									else
										{	/* Unsafe/pregexp.scm 732 */
											if (
												(STRING_REF(BgL_insz00_65,
														((long) CINT(BgL_iz00_2098) + 1L)) ==
													((unsigned char) '&')))
												{	/* Unsafe/pregexp.scm 733 */
													BgL_brz00_2105 = BINT(0L);
												}
											else
												{	/* Unsafe/pregexp.scm 733 */
													BgL_brz00_2105 = BFALSE;
												}
										}
									{	/* Unsafe/pregexp.scm 732 */
										obj_t BgL_iz00_2106;

										if (CBOOL(BgL_brzd2izd2_2104))
											{	/* Unsafe/pregexp.scm 735 */
												obj_t BgL_pairz00_3753;

												BgL_pairz00_3753 = CDR(((obj_t) BgL_brzd2izd2_2104));
												BgL_iz00_2106 = CAR(BgL_pairz00_3753);
											}
										else
											{	/* Unsafe/pregexp.scm 735 */
												if (CBOOL(BgL_brz00_2105))
													{	/* Unsafe/pregexp.scm 736 */
														BgL_iz00_2106 = ADDFX(BgL_iz00_2098, BINT(2L));
													}
												else
													{	/* Unsafe/pregexp.scm 736 */
														BgL_iz00_2106 = ADDFX(BgL_iz00_2098, BINT(1L));
													}
											}
										{	/* Unsafe/pregexp.scm 735 */

											if (CBOOL(BgL_brz00_2105))
												{	/* Unsafe/pregexp.scm 744 */
													obj_t BgL_arg1942z00_2107;

													{	/* Unsafe/pregexp.scm 744 */
														obj_t BgL_backrefz00_2108;

														BgL_backrefz00_2108 =
															BGl_pregexpzd2listzd2refz00zz__regexpz00
															(BgL_backrefsz00_67, BgL_brz00_2105);
														if (CBOOL(BgL_backrefz00_2108))
															{	/* Unsafe/pregexp.scm 747 */
																obj_t BgL_arg1943z00_2109;

																{	/* Unsafe/pregexp.scm 747 */
																	obj_t BgL_arg1944z00_2110;
																	obj_t BgL_arg1945z00_2111;

																	BgL_arg1944z00_2110 =
																		CAR(((obj_t) BgL_backrefz00_2108));
																	BgL_arg1945z00_2111 =
																		CDR(((obj_t) BgL_backrefz00_2108));
																	BgL_arg1943z00_2109 =
																		c_substring(BgL_strz00_64,
																		(long) CINT(BgL_arg1944z00_2110),
																		(long) CINT(BgL_arg1945z00_2111));
																}
																BgL_arg1942z00_2107 =
																	string_append(BgL_rz00_2099,
																	BgL_arg1943z00_2109);
															}
														else
															{	/* Unsafe/pregexp.scm 745 */
																BgL_arg1942z00_2107 = BgL_rz00_2099;
															}
													}
													{
														obj_t BgL_rz00_6654;
														obj_t BgL_iz00_6653;

														BgL_iz00_6653 = BgL_iz00_2106;
														BgL_rz00_6654 = BgL_arg1942z00_2107;
														BgL_rz00_2099 = BgL_rz00_6654;
														BgL_iz00_2098 = BgL_iz00_6653;
														goto BgL_zc3z04anonymousza31939ze3z87_2100;
													}
												}
											else
												{	/* Unsafe/pregexp.scm 739 */
													unsigned char BgL_c2z00_2112;

													BgL_c2z00_2112 =
														STRING_REF(BgL_insz00_65,
														(long) CINT(BgL_iz00_2106));
													{	/* Unsafe/pregexp.scm 740 */
														long BgL_arg1946z00_2113;
														obj_t BgL_arg1947z00_2114;

														BgL_arg1946z00_2113 =
															((long) CINT(BgL_iz00_2106) + 1L);
														if ((BgL_c2z00_2112 == ((unsigned char) '$')))
															{	/* Unsafe/pregexp.scm 741 */
																BgL_arg1947z00_2114 = BgL_rz00_2099;
															}
														else
															{	/* Unsafe/pregexp.scm 742 */
																obj_t BgL_arg1949z00_2116;

																{	/* Unsafe/pregexp.scm 742 */
																	obj_t BgL_list1950z00_2117;

																	BgL_list1950z00_2117 =
																		MAKE_YOUNG_PAIR(BCHAR(BgL_c2z00_2112),
																		BNIL);
																	BgL_arg1949z00_2116 =
																		BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
																		(BgL_list1950z00_2117);
																}
																BgL_arg1947z00_2114 =
																	string_append(BgL_rz00_2099,
																	BgL_arg1949z00_2116);
															}
														{
															obj_t BgL_rz00_6667;
															obj_t BgL_iz00_6665;

															BgL_iz00_6665 = BINT(BgL_arg1946z00_2113);
															BgL_rz00_6667 = BgL_arg1947z00_2114;
															BgL_rz00_2099 = BgL_rz00_6667;
															BgL_iz00_2098 = BgL_iz00_6665;
															goto BgL_zc3z04anonymousza31939ze3z87_2100;
														}
													}
												}
										}
									}
								}
							}
						else
							{	/* Unsafe/pregexp.scm 749 */
								long BgL_arg1956z00_2123;
								obj_t BgL_arg1957z00_2124;

								BgL_arg1956z00_2123 = ((long) CINT(BgL_iz00_2098) + 1L);
								{	/* Unsafe/pregexp.scm 749 */
									obj_t BgL_arg1958z00_2125;

									{	/* Unsafe/pregexp.scm 749 */
										obj_t BgL_list1959z00_2126;

										BgL_list1959z00_2126 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_2102), BNIL);
										BgL_arg1958z00_2125 =
											BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
											(BgL_list1959z00_2126);
									}
									BgL_arg1957z00_2124 =
										string_append(BgL_rz00_2099, BgL_arg1958z00_2125);
								}
								{
									obj_t BgL_rz00_6676;
									obj_t BgL_iz00_6674;

									BgL_iz00_6674 = BINT(BgL_arg1956z00_2123);
									BgL_rz00_6676 = BgL_arg1957z00_2124;
									BgL_rz00_2099 = BgL_rz00_6676;
									BgL_iz00_2098 = BgL_iz00_6674;
									goto BgL_zc3z04anonymousza31939ze3z87_2100;
								}
							}
					}
			}
		}

	}



/* pregexp */
	BGL_EXPORTED_DEF obj_t BGl_pregexpz00zz__regexpz00(obj_t BgL_sz00_68,
		obj_t BgL_optzd2argszd2_69)
	{
		{	/* Unsafe/pregexp.scm 751 */
			{	/* Unsafe/pregexp.scm 753 */
				obj_t BgL_rez00_3767;

				BgL_rez00_3767 = bgl_make_regexp(BgL_sz00_68);
				{	/* Unsafe/pregexp.scm 754 */
					obj_t BgL_tmpz00_6679;

					BgL_tmpz00_6679 =
						BGl_treezd2copyzd2zz__r4_pairs_and_lists_6_3z00
						(BGl_z52pregexpz52zz__regexpz00(BgL_sz00_68));
					BGL_REGEXP_PREG_SET(BgL_rez00_3767, BgL_tmpz00_6679);
				}
				return BgL_rez00_3767;
			}
		}

	}



/* &pregexp */
	obj_t BGl_z62pregexpz62zz__regexpz00(obj_t BgL_envz00_4360,
		obj_t BgL_sz00_4361, obj_t BgL_optzd2argszd2_4362)
	{
		{	/* Unsafe/pregexp.scm 751 */
			{	/* Unsafe/pregexp.scm 753 */
				obj_t BgL_auxz00_6683;

				if (STRINGP(BgL_sz00_4361))
					{	/* Unsafe/pregexp.scm 753 */
						BgL_auxz00_6683 = BgL_sz00_4361;
					}
				else
					{
						obj_t BgL_auxz00_6686;

						BgL_auxz00_6686 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(31147L), BGl_string2603z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_sz00_4361);
						FAILURE(BgL_auxz00_6686, BFALSE, BFALSE);
					}
				return
					BGl_pregexpz00zz__regexpz00(BgL_auxz00_6683, BgL_optzd2argszd2_4362);
			}
		}

	}



/* %pregexp */
	obj_t BGl_z52pregexpz52zz__regexpz00(obj_t BgL_sz00_70)
	{
		{	/* Unsafe/pregexp.scm 757 */
			{	/* Unsafe/pregexp.scm 759 */
				bool_t BgL_test2911z00_6691;

				{	/* Unsafe/pregexp.scm 759 */
					obj_t BgL_s2z00_2142;

					BgL_s2z00_2142 = BgL_sz00_70;
					{	/* Unsafe/pregexp.scm 759 */

						BgL_test2911z00_6691 =
							BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
							(BGl_string2605z00zz__regexpz00, BgL_s2z00_2142, BFALSE, BFALSE,
							BFALSE, BFALSE);
					}
				}
				if (BgL_test2911z00_6691)
					{	/* Unsafe/pregexp.scm 759 */
						obj_t BgL_stringz00_2138;

						BgL_stringz00_2138 = BgL_sz00_70;
						{	/* Ieee/string.scm 194 */
							long BgL_endz00_2140;

							BgL_endz00_2140 = STRING_LENGTH(BgL_stringz00_2138);
							{	/* Ieee/string.scm 194 */

								BgL_sz00_70 =
									BGl_substringz00zz__r4_strings_6_7z00(BgL_stringz00_2138, 7L,
									BgL_endz00_2140);
					}}}
				else
					{	/* Unsafe/pregexp.scm 759 */
						BFALSE;
					}
			}
			BGl_za2pregexpzd2spacezd2sensitivezf3za2zf3zz__regexpz00 = ((bool_t) 1);
			{	/* Unsafe/pregexp.scm 761 */
				obj_t BgL_arg1964z00_2147;

				{	/* Unsafe/pregexp.scm 761 */
					obj_t BgL_arg1967z00_2150;

					{	/* Unsafe/pregexp.scm 761 */
						long BgL_arg1968z00_2151;

						BgL_arg1968z00_2151 = STRING_LENGTH(BgL_sz00_70);
						BgL_arg1967z00_2150 =
							BGl_pregexpzd2readzd2patternz00zz__regexpz00(BgL_sz00_70,
							BINT(0L), BgL_arg1968z00_2151);
					}
					BgL_arg1964z00_2147 = CAR(((obj_t) BgL_arg1967z00_2150));
				}
				{	/* Unsafe/pregexp.scm 761 */
					obj_t BgL_list1965z00_2148;

					{	/* Unsafe/pregexp.scm 761 */
						obj_t BgL_arg1966z00_2149;

						BgL_arg1966z00_2149 = MAKE_YOUNG_PAIR(BgL_arg1964z00_2147, BNIL);
						BgL_list1965z00_2148 =
							MAKE_YOUNG_PAIR(BGl_keyword2517z00zz__regexpz00,
							BgL_arg1966z00_2149);
					}
					return BgL_list1965z00_2148;
				}
			}
		}

	}



/* _pregexp-match-positions */
	obj_t BGl__pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t
		BgL_env1199z00_77, obj_t BgL_opt1198z00_76)
	{
		{	/* Unsafe/pregexp.scm 763 */
			{	/* Unsafe/pregexp.scm 763 */
				obj_t BgL_patz00_2152;
				obj_t BgL_strz00_2153;

				BgL_patz00_2152 = VECTOR_REF(BgL_opt1198z00_76, 0L);
				BgL_strz00_2153 = VECTOR_REF(BgL_opt1198z00_76, 1L);
				switch (VECTOR_LENGTH(BgL_opt1198z00_76))
					{
					case 2L:

						{	/* Unsafe/pregexp.scm 765 */
							long BgL_endz00_2157;

							{	/* Unsafe/pregexp.scm 765 */
								obj_t BgL_stringz00_3774;

								if (STRINGP(BgL_strz00_2153))
									{	/* Unsafe/pregexp.scm 765 */
										BgL_stringz00_3774 = BgL_strz00_2153;
									}
								else
									{
										obj_t BgL_auxz00_6706;

										BgL_auxz00_6706 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2476z00zz__regexpz00, BINT(31575L),
											BGl_string2606z00zz__regexpz00,
											BGl_string2604z00zz__regexpz00, BgL_strz00_2153);
										FAILURE(BgL_auxz00_6706, BFALSE, BFALSE);
									}
								BgL_endz00_2157 = STRING_LENGTH(BgL_stringz00_3774);
							}
							{	/* Unsafe/pregexp.scm 763 */

								{	/* Unsafe/pregexp.scm 763 */
									obj_t BgL_auxz00_6711;

									if (STRINGP(BgL_strz00_2153))
										{	/* Unsafe/pregexp.scm 763 */
											BgL_auxz00_6711 = BgL_strz00_2153;
										}
									else
										{
											obj_t BgL_auxz00_6714;

											BgL_auxz00_6714 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(31470L),
												BGl_string2606z00zz__regexpz00,
												BGl_string2604z00zz__regexpz00, BgL_strz00_2153);
											FAILURE(BgL_auxz00_6714, BFALSE, BFALSE);
										}
									return
										BGl_pregexpzd2matchzd2positionsz00zz__regexpz00
										(BgL_patz00_2152, BgL_auxz00_6711, BINT(0L),
										BINT(BgL_endz00_2157), BINT(0L));
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/pregexp.scm 763 */
							obj_t BgL_begz00_2159;

							BgL_begz00_2159 = VECTOR_REF(BgL_opt1198z00_76, 2L);
							{	/* Unsafe/pregexp.scm 765 */
								long BgL_endz00_2160;

								{	/* Unsafe/pregexp.scm 765 */
									obj_t BgL_stringz00_3775;

									if (STRINGP(BgL_strz00_2153))
										{	/* Unsafe/pregexp.scm 765 */
											BgL_stringz00_3775 = BgL_strz00_2153;
										}
									else
										{
											obj_t BgL_auxz00_6725;

											BgL_auxz00_6725 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(31575L),
												BGl_string2606z00zz__regexpz00,
												BGl_string2604z00zz__regexpz00, BgL_strz00_2153);
											FAILURE(BgL_auxz00_6725, BFALSE, BFALSE);
										}
									BgL_endz00_2160 = STRING_LENGTH(BgL_stringz00_3775);
								}
								{	/* Unsafe/pregexp.scm 763 */

									{	/* Unsafe/pregexp.scm 763 */
										obj_t BgL_auxz00_6730;

										if (STRINGP(BgL_strz00_2153))
											{	/* Unsafe/pregexp.scm 763 */
												BgL_auxz00_6730 = BgL_strz00_2153;
											}
										else
											{
												obj_t BgL_auxz00_6733;

												BgL_auxz00_6733 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(31470L),
													BGl_string2606z00zz__regexpz00,
													BGl_string2604z00zz__regexpz00, BgL_strz00_2153);
												FAILURE(BgL_auxz00_6733, BFALSE, BFALSE);
											}
										return
											BGl_pregexpzd2matchzd2positionsz00zz__regexpz00
											(BgL_patz00_2152, BgL_auxz00_6730, BgL_begz00_2159,
											BINT(BgL_endz00_2160), BINT(0L));
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Unsafe/pregexp.scm 763 */
							obj_t BgL_begz00_2162;

							BgL_begz00_2162 = VECTOR_REF(BgL_opt1198z00_76, 2L);
							{	/* Unsafe/pregexp.scm 763 */
								obj_t BgL_endz00_2163;

								BgL_endz00_2163 = VECTOR_REF(BgL_opt1198z00_76, 3L);
								{	/* Unsafe/pregexp.scm 763 */

									{	/* Unsafe/pregexp.scm 763 */
										obj_t BgL_auxz00_6742;

										if (STRINGP(BgL_strz00_2153))
											{	/* Unsafe/pregexp.scm 763 */
												BgL_auxz00_6742 = BgL_strz00_2153;
											}
										else
											{
												obj_t BgL_auxz00_6745;

												BgL_auxz00_6745 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(31470L),
													BGl_string2606z00zz__regexpz00,
													BGl_string2604z00zz__regexpz00, BgL_strz00_2153);
												FAILURE(BgL_auxz00_6745, BFALSE, BFALSE);
											}
										return
											BGl_pregexpzd2matchzd2positionsz00zz__regexpz00
											(BgL_patz00_2152, BgL_auxz00_6742, BgL_begz00_2162,
											BgL_endz00_2163, BINT(0L));
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Unsafe/pregexp.scm 763 */
							obj_t BgL_begz00_2165;

							BgL_begz00_2165 = VECTOR_REF(BgL_opt1198z00_76, 2L);
							{	/* Unsafe/pregexp.scm 763 */
								obj_t BgL_endz00_2166;

								BgL_endz00_2166 = VECTOR_REF(BgL_opt1198z00_76, 3L);
								{	/* Unsafe/pregexp.scm 763 */
									obj_t BgL_offsetz00_2167;

									BgL_offsetz00_2167 = VECTOR_REF(BgL_opt1198z00_76, 4L);
									{	/* Unsafe/pregexp.scm 763 */

										{	/* Unsafe/pregexp.scm 763 */
											obj_t BgL_auxz00_6754;

											if (STRINGP(BgL_strz00_2153))
												{	/* Unsafe/pregexp.scm 763 */
													BgL_auxz00_6754 = BgL_strz00_2153;
												}
											else
												{
													obj_t BgL_auxz00_6757;

													BgL_auxz00_6757 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2476z00zz__regexpz00, BINT(31470L),
														BGl_string2606z00zz__regexpz00,
														BGl_string2604z00zz__regexpz00, BgL_strz00_2153);
													FAILURE(BgL_auxz00_6757, BFALSE, BFALSE);
												}
											return
												BGl_pregexpzd2matchzd2positionsz00zz__regexpz00
												(BgL_patz00_2152, BgL_auxz00_6754, BgL_begz00_2165,
												BgL_endz00_2166, BgL_offsetz00_2167);
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match-positions */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t
		BgL_patz00_71, obj_t BgL_strz00_72, obj_t BgL_begz00_73,
		obj_t BgL_endz00_74, obj_t BgL_offsetz00_75)
	{
		{	/* Unsafe/pregexp.scm 763 */
			if (STRINGP(BgL_patz00_71))
				{	/* Unsafe/pregexp.scm 766 */
					BgL_patz00_71 = BGl_z52pregexpz52zz__regexpz00(BgL_patz00_71);
				}
			else
				{	/* Unsafe/pregexp.scm 766 */
					if (BGL_REGEXPP(BgL_patz00_71))
						{	/* Unsafe/pregexp.scm 767 */
							BgL_patz00_71 = BGL_REGEXP_PREG(BgL_patz00_71);
						}
					else
						{	/* Unsafe/pregexp.scm 767 */
							if (PAIRP(BgL_patz00_71))
								{	/* Unsafe/pregexp.scm 768 */
									BTRUE;
								}
							else
								{	/* Unsafe/pregexp.scm 769 */
									obj_t BgL_list1972z00_2171;

									{	/* Unsafe/pregexp.scm 769 */
										obj_t BgL_arg1973z00_2172;

										BgL_arg1973z00_2172 = MAKE_YOUNG_PAIR(BgL_patz00_71, BNIL);
										BgL_list1972z00_2171 =
											MAKE_YOUNG_PAIR(BGl_symbol2607z00zz__regexpz00,
											BgL_arg1973z00_2172);
									}
									BGl_errorz00zz__errorz00(BGl_string2500z00zz__regexpz00,
										BGl_symbol2609z00zz__regexpz00, CAR(BgL_list1972z00_2171));
								}
						}
				}
			{	/* Unsafe/pregexp.scm 772 */
				obj_t BgL_strz00_2173;

				if (((long) CINT(BgL_offsetz00_75) > 0L))
					{	/* Unsafe/pregexp.scm 772 */
						BgL_strz00_2173 =
							c_substring(BgL_strz00_72,
							(long) CINT(BgL_offsetz00_75),
							((long) CINT(BgL_offsetz00_75) + (long) CINT(BgL_endz00_74)));
					}
				else
					{	/* Unsafe/pregexp.scm 772 */
						BgL_strz00_2173 = BgL_strz00_72;
					}
				{	/* Unsafe/pregexp.scm 772 */
					long BgL_strzd2lenzd2_2174;

					BgL_strzd2lenzd2_2174 = STRING_LENGTH(BgL_strz00_2173);
					{	/* Unsafe/pregexp.scm 776 */

						{
							obj_t BgL_iz00_2177;

							BgL_iz00_2177 = BgL_begz00_73;
						BgL_zc3z04anonymousza31974ze3z87_2178:
							if (((long) CINT(BgL_iz00_2177) <= (long) CINT(BgL_endz00_74)))
								{	/* Unsafe/pregexp.scm 779 */
									obj_t BgL__ortest_1120z00_2180;

									BgL__ortest_1120z00_2180 =
										BGl_pregexpzd2matchzd2positionszd2auxzd2zz__regexpz00
										(BgL_patz00_71, BgL_strz00_2173, BgL_strzd2lenzd2_2174,
										BgL_begz00_73, BgL_endz00_74, BgL_iz00_2177);
									if (CBOOL(BgL__ortest_1120z00_2180))
										{	/* Unsafe/pregexp.scm 779 */
											return BgL__ortest_1120z00_2180;
										}
									else
										{	/* Unsafe/pregexp.scm 780 */
											long BgL_arg1975z00_2181;

											BgL_arg1975z00_2181 = ((long) CINT(BgL_iz00_2177) + 1L);
											{
												obj_t BgL_iz00_6794;

												BgL_iz00_6794 = BINT(BgL_arg1975z00_2181);
												BgL_iz00_2177 = BgL_iz00_6794;
												goto BgL_zc3z04anonymousza31974ze3z87_2178;
											}
										}
								}
							else
								{	/* Unsafe/pregexp.scm 778 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* _pregexp-match-n-positions! */
	obj_t BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t
		BgL_env1203z00_85, obj_t BgL_opt1202z00_84)
	{
		{	/* Unsafe/pregexp.scm 782 */
			{	/* Unsafe/pregexp.scm 782 */
				obj_t BgL_g1204z00_2185;
				obj_t BgL_g1205z00_2186;
				obj_t BgL_g1206z00_2187;
				obj_t BgL_g1207z00_2188;
				obj_t BgL_g1208z00_2189;

				BgL_g1204z00_2185 = VECTOR_REF(BgL_opt1202z00_84, 0L);
				BgL_g1205z00_2186 = VECTOR_REF(BgL_opt1202z00_84, 1L);
				BgL_g1206z00_2187 = VECTOR_REF(BgL_opt1202z00_84, 2L);
				BgL_g1207z00_2188 = VECTOR_REF(BgL_opt1202z00_84, 3L);
				BgL_g1208z00_2189 = VECTOR_REF(BgL_opt1202z00_84, 4L);
				switch (VECTOR_LENGTH(BgL_opt1202z00_84))
					{
					case 5L:

						{	/* Unsafe/pregexp.scm 782 */

							{	/* Unsafe/pregexp.scm 782 */
								long BgL_tmpz00_6801;

								{	/* Unsafe/pregexp.scm 782 */
									long BgL_auxz00_6832;
									long BgL_auxz00_6823;
									obj_t BgL_auxz00_6816;
									obj_t BgL_auxz00_6809;
									obj_t BgL_auxz00_6802;

									{	/* Unsafe/pregexp.scm 782 */
										obj_t BgL_tmpz00_6833;

										if (INTEGERP(BgL_g1208z00_2189))
											{	/* Unsafe/pregexp.scm 782 */
												BgL_tmpz00_6833 = BgL_g1208z00_2189;
											}
										else
											{
												obj_t BgL_auxz00_6836;

												BgL_auxz00_6836 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32196L),
													BGl_string2611z00zz__regexpz00,
													BGl_string2613z00zz__regexpz00, BgL_g1208z00_2189);
												FAILURE(BgL_auxz00_6836, BFALSE, BFALSE);
											}
										BgL_auxz00_6832 = (long) CINT(BgL_tmpz00_6833);
									}
									{	/* Unsafe/pregexp.scm 782 */
										obj_t BgL_tmpz00_6824;

										if (INTEGERP(BgL_g1207z00_2188))
											{	/* Unsafe/pregexp.scm 782 */
												BgL_tmpz00_6824 = BgL_g1207z00_2188;
											}
										else
											{
												obj_t BgL_auxz00_6827;

												BgL_auxz00_6827 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32196L),
													BGl_string2611z00zz__regexpz00,
													BGl_string2613z00zz__regexpz00, BgL_g1207z00_2188);
												FAILURE(BgL_auxz00_6827, BFALSE, BFALSE);
											}
										BgL_auxz00_6823 = (long) CINT(BgL_tmpz00_6824);
									}
									if (VECTORP(BgL_g1206z00_2187))
										{	/* Unsafe/pregexp.scm 782 */
											BgL_auxz00_6816 = BgL_g1206z00_2187;
										}
									else
										{
											obj_t BgL_auxz00_6819;

											BgL_auxz00_6819 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(32196L),
												BGl_string2611z00zz__regexpz00,
												BGl_string2612z00zz__regexpz00, BgL_g1206z00_2187);
											FAILURE(BgL_auxz00_6819, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1205z00_2186))
										{	/* Unsafe/pregexp.scm 782 */
											BgL_auxz00_6809 = BgL_g1205z00_2186;
										}
									else
										{
											obj_t BgL_auxz00_6812;

											BgL_auxz00_6812 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(32196L),
												BGl_string2611z00zz__regexpz00,
												BGl_string2604z00zz__regexpz00, BgL_g1205z00_2186);
											FAILURE(BgL_auxz00_6812, BFALSE, BFALSE);
										}
									if (BGl_regexpzf3zf3zz__regexpz00(BgL_g1204z00_2185))
										{	/* Unsafe/pregexp.scm 782 */
											BgL_auxz00_6802 = BgL_g1204z00_2185;
										}
									else
										{
											obj_t BgL_auxz00_6805;

											BgL_auxz00_6805 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(32196L),
												BGl_string2611z00zz__regexpz00,
												BGl_string2478z00zz__regexpz00, BgL_g1204z00_2185);
											FAILURE(BgL_auxz00_6805, BFALSE, BFALSE);
										}
									BgL_tmpz00_6801 =
										BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00
										(BgL_auxz00_6802, BgL_auxz00_6809, BgL_auxz00_6816,
										BgL_auxz00_6823, BgL_auxz00_6832, BINT(0L));
								}
								return BINT(BgL_tmpz00_6801);
							}
						}
						break;
					case 6L:

						{	/* Unsafe/pregexp.scm 782 */
							obj_t BgL_offsetz00_2193;

							BgL_offsetz00_2193 = VECTOR_REF(BgL_opt1202z00_84, 5L);
							{	/* Unsafe/pregexp.scm 782 */

								{	/* Unsafe/pregexp.scm 782 */
									long BgL_tmpz00_6845;

									{	/* Unsafe/pregexp.scm 782 */
										long BgL_auxz00_6876;
										long BgL_auxz00_6867;
										obj_t BgL_auxz00_6860;
										obj_t BgL_auxz00_6853;
										obj_t BgL_auxz00_6846;

										{	/* Unsafe/pregexp.scm 782 */
											obj_t BgL_tmpz00_6877;

											if (INTEGERP(BgL_g1208z00_2189))
												{	/* Unsafe/pregexp.scm 782 */
													BgL_tmpz00_6877 = BgL_g1208z00_2189;
												}
											else
												{
													obj_t BgL_auxz00_6880;

													BgL_auxz00_6880 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2476z00zz__regexpz00, BINT(32196L),
														BGl_string2611z00zz__regexpz00,
														BGl_string2613z00zz__regexpz00, BgL_g1208z00_2189);
													FAILURE(BgL_auxz00_6880, BFALSE, BFALSE);
												}
											BgL_auxz00_6876 = (long) CINT(BgL_tmpz00_6877);
										}
										{	/* Unsafe/pregexp.scm 782 */
											obj_t BgL_tmpz00_6868;

											if (INTEGERP(BgL_g1207z00_2188))
												{	/* Unsafe/pregexp.scm 782 */
													BgL_tmpz00_6868 = BgL_g1207z00_2188;
												}
											else
												{
													obj_t BgL_auxz00_6871;

													BgL_auxz00_6871 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string2476z00zz__regexpz00, BINT(32196L),
														BGl_string2611z00zz__regexpz00,
														BGl_string2613z00zz__regexpz00, BgL_g1207z00_2188);
													FAILURE(BgL_auxz00_6871, BFALSE, BFALSE);
												}
											BgL_auxz00_6867 = (long) CINT(BgL_tmpz00_6868);
										}
										if (VECTORP(BgL_g1206z00_2187))
											{	/* Unsafe/pregexp.scm 782 */
												BgL_auxz00_6860 = BgL_g1206z00_2187;
											}
										else
											{
												obj_t BgL_auxz00_6863;

												BgL_auxz00_6863 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32196L),
													BGl_string2611z00zz__regexpz00,
													BGl_string2612z00zz__regexpz00, BgL_g1206z00_2187);
												FAILURE(BgL_auxz00_6863, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1205z00_2186))
											{	/* Unsafe/pregexp.scm 782 */
												BgL_auxz00_6853 = BgL_g1205z00_2186;
											}
										else
											{
												obj_t BgL_auxz00_6856;

												BgL_auxz00_6856 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32196L),
													BGl_string2611z00zz__regexpz00,
													BGl_string2604z00zz__regexpz00, BgL_g1205z00_2186);
												FAILURE(BgL_auxz00_6856, BFALSE, BFALSE);
											}
										if (BGl_regexpzf3zf3zz__regexpz00(BgL_g1204z00_2185))
											{	/* Unsafe/pregexp.scm 782 */
												BgL_auxz00_6846 = BgL_g1204z00_2185;
											}
										else
											{
												obj_t BgL_auxz00_6849;

												BgL_auxz00_6849 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32196L),
													BGl_string2611z00zz__regexpz00,
													BGl_string2478z00zz__regexpz00, BgL_g1204z00_2185);
												FAILURE(BgL_auxz00_6849, BFALSE, BFALSE);
											}
										BgL_tmpz00_6845 =
											BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00
											(BgL_auxz00_6846, BgL_auxz00_6853, BgL_auxz00_6860,
											BgL_auxz00_6867, BgL_auxz00_6876, BgL_offsetz00_2193);
									}
									return BINT(BgL_tmpz00_6845);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match-n-positions! */
	BGL_EXPORTED_DEF long
		BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t BgL_patz00_78,
		obj_t BgL_strz00_79, obj_t BgL_resz00_80, long BgL_begz00_81,
		long BgL_endz00_82, obj_t BgL_offsetz00_83)
	{
		{	/* Unsafe/pregexp.scm 782 */
			{	/* Unsafe/pregexp.scm 785 */
				obj_t BgL_posz00_2194;
				long BgL_lenz00_2195;

				BgL_posz00_2194 =
					BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(BgL_patz00_78,
					BgL_strz00_79, BINT(BgL_begz00_81), BINT(BgL_endz00_82),
					BgL_offsetz00_83);
				BgL_lenz00_2195 = (VECTOR_LENGTH(BgL_resz00_80) & ~(1L));
				if (CBOOL(BgL_posz00_2194))
					{
						long BgL_iz00_2197;
						obj_t BgL_posz00_2198;

						BgL_iz00_2197 = 0L;
						BgL_posz00_2198 = BgL_posz00_2194;
					BgL_zc3z04anonymousza31978ze3z87_2199:
						{	/* Unsafe/pregexp.scm 792 */
							bool_t BgL_test2935z00_6897;

							if ((BgL_iz00_2197 == BgL_lenz00_2195))
								{	/* Unsafe/pregexp.scm 792 */
									BgL_test2935z00_6897 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/pregexp.scm 792 */
									BgL_test2935z00_6897 = NULLP(BgL_posz00_2198);
								}
							if (BgL_test2935z00_6897)
								{	/* Unsafe/pregexp.scm 792 */
									return BgL_iz00_2197;
								}
							else
								{	/* Unsafe/pregexp.scm 794 */
									bool_t BgL_test2937z00_6901;

									{	/* Unsafe/pregexp.scm 794 */
										obj_t BgL_tmpz00_6902;

										BgL_tmpz00_6902 = CAR(((obj_t) BgL_posz00_2198));
										BgL_test2937z00_6901 = PAIRP(BgL_tmpz00_6902);
									}
									if (BgL_test2937z00_6901)
										{	/* Unsafe/pregexp.scm 794 */
											{	/* Unsafe/pregexp.scm 795 */
												obj_t BgL_arg1983z00_2204;

												{	/* Unsafe/pregexp.scm 795 */
													obj_t BgL_pairz00_3802;

													BgL_pairz00_3802 = CAR(((obj_t) BgL_posz00_2198));
													BgL_arg1983z00_2204 = CAR(BgL_pairz00_3802);
												}
												VECTOR_SET(BgL_resz00_80, BgL_iz00_2197,
													BgL_arg1983z00_2204);
											}
											{	/* Unsafe/pregexp.scm 796 */
												long BgL_arg1984z00_2205;
												obj_t BgL_arg1985z00_2206;

												BgL_arg1984z00_2205 = (BgL_iz00_2197 + 1L);
												{	/* Unsafe/pregexp.scm 796 */
													obj_t BgL_pairz00_3809;

													BgL_pairz00_3809 = CAR(((obj_t) BgL_posz00_2198));
													BgL_arg1985z00_2206 = CDR(BgL_pairz00_3809);
												}
												VECTOR_SET(BgL_resz00_80, BgL_arg1984z00_2205,
													BgL_arg1985z00_2206);
											}
											{	/* Unsafe/pregexp.scm 797 */
												long BgL_arg1986z00_2207;
												obj_t BgL_arg1987z00_2208;

												BgL_arg1986z00_2207 = (BgL_iz00_2197 + 2L);
												BgL_arg1987z00_2208 = CDR(((obj_t) BgL_posz00_2198));
												{
													obj_t BgL_posz00_6919;
													long BgL_iz00_6918;

													BgL_iz00_6918 = BgL_arg1986z00_2207;
													BgL_posz00_6919 = BgL_arg1987z00_2208;
													BgL_posz00_2198 = BgL_posz00_6919;
													BgL_iz00_2197 = BgL_iz00_6918;
													goto BgL_zc3z04anonymousza31978ze3z87_2199;
												}
											}
										}
									else
										{	/* Unsafe/pregexp.scm 794 */
											VECTOR_SET(BgL_resz00_80, BgL_iz00_2197, BINT(-1L));
											VECTOR_SET(BgL_resz00_80,
												(BgL_iz00_2197 + 1L), BINT(-1L));
											{	/* Unsafe/pregexp.scm 801 */
												long BgL_arg1989z00_2210;
												obj_t BgL_arg1990z00_2211;

												BgL_arg1989z00_2210 = (BgL_iz00_2197 + 2L);
												BgL_arg1990z00_2211 = CDR(((obj_t) BgL_posz00_2198));
												{
													obj_t BgL_posz00_6929;
													long BgL_iz00_6928;

													BgL_iz00_6928 = BgL_arg1989z00_2210;
													BgL_posz00_6929 = BgL_arg1990z00_2211;
													BgL_posz00_2198 = BgL_posz00_6929;
													BgL_iz00_2197 = BgL_iz00_6928;
													goto BgL_zc3z04anonymousza31978ze3z87_2199;
												}
											}
										}
								}
						}
					}
				else
					{	/* Unsafe/pregexp.scm 787 */
						return -1L;
					}
			}
		}

	}



/* _pregexp-match */
	obj_t BGl__pregexpzd2matchzd2zz__regexpz00(obj_t BgL_env1212z00_91,
		obj_t BgL_opt1211z00_90)
	{
		{	/* Unsafe/pregexp.scm 803 */
			{	/* Unsafe/pregexp.scm 803 */
				obj_t BgL_patz00_2217;
				obj_t BgL_strz00_2218;

				BgL_patz00_2217 = VECTOR_REF(BgL_opt1211z00_90, 0L);
				BgL_strz00_2218 = VECTOR_REF(BgL_opt1211z00_90, 1L);
				switch (VECTOR_LENGTH(BgL_opt1211z00_90))
					{
					case 2L:

						{	/* Unsafe/pregexp.scm 804 */
							long BgL_endz00_2222;

							{	/* Unsafe/pregexp.scm 804 */
								obj_t BgL_stringz00_3821;

								if (STRINGP(BgL_strz00_2218))
									{	/* Unsafe/pregexp.scm 804 */
										BgL_stringz00_3821 = BgL_strz00_2218;
									}
								else
									{
										obj_t BgL_auxz00_6934;

										BgL_auxz00_6934 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2476z00zz__regexpz00, BINT(32868L),
											BGl_string2614z00zz__regexpz00,
											BGl_string2604z00zz__regexpz00, BgL_strz00_2218);
										FAILURE(BgL_auxz00_6934, BFALSE, BFALSE);
									}
								BgL_endz00_2222 = STRING_LENGTH(BgL_stringz00_3821);
							}
							{	/* Unsafe/pregexp.scm 803 */

								{	/* Unsafe/pregexp.scm 803 */
									obj_t BgL_auxz00_6939;

									if (STRINGP(BgL_strz00_2218))
										{	/* Unsafe/pregexp.scm 803 */
											BgL_auxz00_6939 = BgL_strz00_2218;
										}
									else
										{
											obj_t BgL_auxz00_6942;

											BgL_auxz00_6942 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(32803L),
												BGl_string2614z00zz__regexpz00,
												BGl_string2604z00zz__regexpz00, BgL_strz00_2218);
											FAILURE(BgL_auxz00_6942, BFALSE, BFALSE);
										}
									return
										BGl_pregexpzd2matchzd2zz__regexpz00(BgL_patz00_2217,
										BgL_auxz00_6939, BINT(0L), BINT(BgL_endz00_2222));
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/pregexp.scm 803 */
							obj_t BgL_begz00_2223;

							BgL_begz00_2223 = VECTOR_REF(BgL_opt1211z00_90, 2L);
							{	/* Unsafe/pregexp.scm 804 */
								long BgL_endz00_2224;

								{	/* Unsafe/pregexp.scm 804 */
									obj_t BgL_stringz00_3822;

									if (STRINGP(BgL_strz00_2218))
										{	/* Unsafe/pregexp.scm 804 */
											BgL_stringz00_3822 = BgL_strz00_2218;
										}
									else
										{
											obj_t BgL_auxz00_6952;

											BgL_auxz00_6952 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2476z00zz__regexpz00, BINT(32868L),
												BGl_string2614z00zz__regexpz00,
												BGl_string2604z00zz__regexpz00, BgL_strz00_2218);
											FAILURE(BgL_auxz00_6952, BFALSE, BFALSE);
										}
									BgL_endz00_2224 = STRING_LENGTH(BgL_stringz00_3822);
								}
								{	/* Unsafe/pregexp.scm 803 */

									{	/* Unsafe/pregexp.scm 803 */
										obj_t BgL_auxz00_6957;

										if (STRINGP(BgL_strz00_2218))
											{	/* Unsafe/pregexp.scm 803 */
												BgL_auxz00_6957 = BgL_strz00_2218;
											}
										else
											{
												obj_t BgL_auxz00_6960;

												BgL_auxz00_6960 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32803L),
													BGl_string2614z00zz__regexpz00,
													BGl_string2604z00zz__regexpz00, BgL_strz00_2218);
												FAILURE(BgL_auxz00_6960, BFALSE, BFALSE);
											}
										return
											BGl_pregexpzd2matchzd2zz__regexpz00(BgL_patz00_2217,
											BgL_auxz00_6957, BgL_begz00_2223, BINT(BgL_endz00_2224));
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Unsafe/pregexp.scm 803 */
							obj_t BgL_begz00_2225;

							BgL_begz00_2225 = VECTOR_REF(BgL_opt1211z00_90, 2L);
							{	/* Unsafe/pregexp.scm 803 */
								obj_t BgL_endz00_2226;

								BgL_endz00_2226 = VECTOR_REF(BgL_opt1211z00_90, 3L);
								{	/* Unsafe/pregexp.scm 803 */

									{	/* Unsafe/pregexp.scm 803 */
										obj_t BgL_auxz00_6968;

										if (STRINGP(BgL_strz00_2218))
											{	/* Unsafe/pregexp.scm 803 */
												BgL_auxz00_6968 = BgL_strz00_2218;
											}
										else
											{
												obj_t BgL_auxz00_6971;

												BgL_auxz00_6971 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string2476z00zz__regexpz00, BINT(32803L),
													BGl_string2614z00zz__regexpz00,
													BGl_string2604z00zz__regexpz00, BgL_strz00_2218);
												FAILURE(BgL_auxz00_6971, BFALSE, BFALSE);
											}
										return
											BGl_pregexpzd2matchzd2zz__regexpz00(BgL_patz00_2217,
											BgL_auxz00_6968, BgL_begz00_2225, BgL_endz00_2226);
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2matchzd2zz__regexpz00(obj_t
		BgL_patz00_86, obj_t BgL_strz00_87, obj_t BgL_begz00_88,
		obj_t BgL_endz00_89)
	{
		{	/* Unsafe/pregexp.scm 803 */
			{	/* Unsafe/pregexp.scm 805 */
				obj_t BgL_ixzd2prszd2_2227;

				{	/* Unsafe/pregexp.scm 76 */

					BgL_ixzd2prszd2_2227 =
						BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(BgL_patz00_86,
						BgL_strz00_87, BgL_begz00_88, BgL_endz00_89, BINT(0L));
				}
				if (CBOOL(BgL_ixzd2prszd2_2227))
					{	/* Unsafe/pregexp.scm 806 */
						if (NULLP(BgL_ixzd2prszd2_2227))
							{	/* Unsafe/pregexp.scm 807 */
								return BNIL;
							}
						else
							{	/* Unsafe/pregexp.scm 807 */
								obj_t BgL_head1182z00_2231;

								BgL_head1182z00_2231 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1180z00_2233;
									obj_t BgL_tail1183z00_2234;

									BgL_l1180z00_2233 = BgL_ixzd2prszd2_2227;
									BgL_tail1183z00_2234 = BgL_head1182z00_2231;
								BgL_zc3z04anonymousza31995ze3z87_2235:
									if (NULLP(BgL_l1180z00_2233))
										{	/* Unsafe/pregexp.scm 807 */
											return CDR(BgL_head1182z00_2231);
										}
									else
										{	/* Unsafe/pregexp.scm 807 */
											obj_t BgL_newtail1184z00_2237;

											{	/* Unsafe/pregexp.scm 807 */
												obj_t BgL_arg1998z00_2239;

												{	/* Unsafe/pregexp.scm 807 */
													obj_t BgL_ixzd2przd2_2240;

													BgL_ixzd2przd2_2240 =
														CAR(((obj_t) BgL_l1180z00_2233));
													if (CBOOL(BgL_ixzd2przd2_2240))
														{	/* Unsafe/pregexp.scm 810 */
															obj_t BgL_arg1999z00_2242;
															obj_t BgL_arg2000z00_2243;

															BgL_arg1999z00_2242 =
																CAR(((obj_t) BgL_ixzd2przd2_2240));
															BgL_arg2000z00_2243 =
																CDR(((obj_t) BgL_ixzd2przd2_2240));
															BgL_arg1998z00_2239 =
																c_substring(BgL_strz00_87,
																(long) CINT(BgL_arg1999z00_2242),
																(long) CINT(BgL_arg2000z00_2243));
														}
													else
														{	/* Unsafe/pregexp.scm 809 */
															BgL_arg1998z00_2239 = BFALSE;
														}
												}
												BgL_newtail1184z00_2237 =
													MAKE_YOUNG_PAIR(BgL_arg1998z00_2239, BNIL);
											}
											SET_CDR(BgL_tail1183z00_2234, BgL_newtail1184z00_2237);
											{	/* Unsafe/pregexp.scm 807 */
												obj_t BgL_arg1997z00_2238;

												BgL_arg1997z00_2238 = CDR(((obj_t) BgL_l1180z00_2233));
												{
													obj_t BgL_tail1183z00_7004;
													obj_t BgL_l1180z00_7003;

													BgL_l1180z00_7003 = BgL_arg1997z00_2238;
													BgL_tail1183z00_7004 = BgL_newtail1184z00_2237;
													BgL_tail1183z00_2234 = BgL_tail1183z00_7004;
													BgL_l1180z00_2233 = BgL_l1180z00_7003;
													goto BgL_zc3z04anonymousza31995ze3z87_2235;
												}
											}
										}
								}
							}
					}
				else
					{	/* Unsafe/pregexp.scm 806 */
						return BFALSE;
					}
			}
		}

	}



/* pregexp-split */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2splitzd2zz__regexpz00(obj_t
		BgL_patz00_92, obj_t BgL_strz00_93)
	{
		{	/* Unsafe/pregexp.scm 813 */
			{	/* Unsafe/pregexp.scm 816 */
				long BgL_nz00_2250;

				BgL_nz00_2250 = STRING_LENGTH(BgL_strz00_93);
				{
					obj_t BgL_iz00_2253;
					obj_t BgL_rz00_2254;
					bool_t BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255;

					BgL_iz00_2253 = BINT(0L);
					BgL_rz00_2254 = BNIL;
					BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255 = ((bool_t) 0);
				BgL_zc3z04anonymousza32001ze3z87_2256:
					if (((long) CINT(BgL_iz00_2253) >= BgL_nz00_2250))
						{	/* Unsafe/pregexp.scm 818 */
							return BGl_pregexpzd2reversez12zc0zz__regexpz00(BgL_rz00_2254);
						}
					else
						{	/* Unsafe/pregexp.scm 819 */
							obj_t BgL_g1126z00_2258;

							{	/* Unsafe/pregexp.scm 76 */

								BgL_g1126z00_2258 =
									BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(BgL_patz00_92,
									BgL_strz00_93, BgL_iz00_2253, BINT(BgL_nz00_2250), BINT(0L));
							}
							if (CBOOL(BgL_g1126z00_2258))
								{	/* Unsafe/pregexp.scm 822 */
									obj_t BgL_jkz00_2261;

									BgL_jkz00_2261 = CAR(((obj_t) BgL_g1126z00_2258));
									{	/* Unsafe/pregexp.scm 823 */
										obj_t BgL_jz00_2262;
										obj_t BgL_kz00_2263;

										BgL_jz00_2262 = CAR(((obj_t) BgL_jkz00_2261));
										BgL_kz00_2263 = CDR(((obj_t) BgL_jkz00_2261));
										if (
											((long) CINT(BgL_jz00_2262) ==
												(long) CINT(BgL_kz00_2263)))
											{	/* Unsafe/pregexp.scm 827 */
												long BgL_arg2004z00_2265;
												obj_t BgL_arg2006z00_2266;

												BgL_arg2004z00_2265 = ((long) CINT(BgL_kz00_2263) + 1L);
												BgL_arg2006z00_2266 =
													MAKE_YOUNG_PAIR(c_substring(BgL_strz00_93,
														(long) CINT(BgL_iz00_2253),
														((long) CINT(BgL_jz00_2262) + 1L)), BgL_rz00_2254);
												{
													bool_t
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7035;
													obj_t BgL_rz00_7034;
													obj_t BgL_iz00_7032;

													BgL_iz00_7032 = BINT(BgL_arg2004z00_2265);
													BgL_rz00_7034 = BgL_arg2006z00_2266;
													BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7035
														= ((bool_t) 1);
													BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255
														=
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7035;
													BgL_rz00_2254 = BgL_rz00_7034;
													BgL_iz00_2253 = BgL_iz00_7032;
													goto BgL_zc3z04anonymousza32001ze3z87_2256;
												}
											}
										else
											{	/* Unsafe/pregexp.scm 829 */
												bool_t BgL_test2950z00_7036;

												if (
													((long) CINT(BgL_jz00_2262) ==
														(long) CINT(BgL_iz00_2253)))
													{	/* Unsafe/pregexp.scm 829 */
														BgL_test2950z00_7036 =
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255;
													}
												else
													{	/* Unsafe/pregexp.scm 829 */
														BgL_test2950z00_7036 = ((bool_t) 0);
													}
												if (BgL_test2950z00_7036)
													{
														bool_t
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7042;
														obj_t BgL_iz00_7041;

														BgL_iz00_7041 = BgL_kz00_2263;
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7042
															= ((bool_t) 0);
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255
															=
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7042;
														BgL_iz00_2253 = BgL_iz00_7041;
														goto BgL_zc3z04anonymousza32001ze3z87_2256;
													}
												else
													{	/* Unsafe/pregexp.scm 833 */
														obj_t BgL_arg2011z00_2271;

														BgL_arg2011z00_2271 =
															MAKE_YOUNG_PAIR(c_substring(BgL_strz00_93,
																(long) CINT(BgL_iz00_2253),
																(long) CINT(BgL_jz00_2262)), BgL_rz00_2254);
														{
															bool_t
																BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7049;
															obj_t BgL_rz00_7048;
															obj_t BgL_iz00_7047;

															BgL_iz00_7047 = BgL_kz00_2263;
															BgL_rz00_7048 = BgL_arg2011z00_2271;
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7049
																= ((bool_t) 0);
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255
																=
																BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7049;
															BgL_rz00_2254 = BgL_rz00_7048;
															BgL_iz00_2253 = BgL_iz00_7047;
															goto BgL_zc3z04anonymousza32001ze3z87_2256;
														}
													}
											}
									}
								}
							else
								{	/* Unsafe/pregexp.scm 834 */
									obj_t BgL_arg2013z00_2274;

									BgL_arg2013z00_2274 =
										MAKE_YOUNG_PAIR(c_substring(BgL_strz00_93,
											(long) CINT(BgL_iz00_2253), BgL_nz00_2250),
										BgL_rz00_2254);
									{
										bool_t
											BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7056;
										obj_t BgL_rz00_7055;
										obj_t BgL_iz00_7053;

										BgL_iz00_7053 = BINT(BgL_nz00_2250);
										BgL_rz00_7055 = BgL_arg2013z00_2274;
										BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7056 =
											((bool_t) 0);
										BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_2255 =
											BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_7056;
										BgL_rz00_2254 = BgL_rz00_7055;
										BgL_iz00_2253 = BgL_iz00_7053;
										goto BgL_zc3z04anonymousza32001ze3z87_2256;
									}
								}
						}
				}
			}
		}

	}



/* &pregexp-split */
	obj_t BGl_z62pregexpzd2splitzb0zz__regexpz00(obj_t BgL_envz00_4363,
		obj_t BgL_patz00_4364, obj_t BgL_strz00_4365)
	{
		{	/* Unsafe/pregexp.scm 813 */
			{	/* Unsafe/pregexp.scm 816 */
				obj_t BgL_auxz00_7058;

				if (STRINGP(BgL_strz00_4365))
					{	/* Unsafe/pregexp.scm 816 */
						BgL_auxz00_7058 = BgL_strz00_4365;
					}
				else
					{
						obj_t BgL_auxz00_7061;

						BgL_auxz00_7061 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(33230L), BGl_string2615z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_strz00_4365);
						FAILURE(BgL_auxz00_7061, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2splitzd2zz__regexpz00(BgL_patz00_4364, BgL_auxz00_7058);
			}
		}

	}



/* pregexp-replace */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2replacezd2zz__regexpz00(obj_t
		BgL_patz00_94, obj_t BgL_strz00_95, obj_t BgL_insz00_96)
	{
		{	/* Unsafe/pregexp.scm 836 */
			{	/* Unsafe/pregexp.scm 838 */
				long BgL_nz00_2282;

				BgL_nz00_2282 = STRING_LENGTH(BgL_strz00_95);
				{	/* Unsafe/pregexp.scm 838 */
					obj_t BgL_ppz00_2283;

					{	/* Unsafe/pregexp.scm 76 */

						BgL_ppz00_2283 =
							BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(BgL_patz00_94,
							BgL_strz00_95, BINT(0L), BINT(BgL_nz00_2282), BINT(0L));
					}
					{	/* Unsafe/pregexp.scm 839 */

						if (CBOOL(BgL_ppz00_2283))
							{	/* Unsafe/pregexp.scm 841 */
								long BgL_inszd2lenzd2_2284;
								obj_t BgL_mzd2izd2_2285;
								obj_t BgL_mzd2nzd2_2286;

								BgL_inszd2lenzd2_2284 = STRING_LENGTH(BgL_insz00_96);
								{	/* Unsafe/pregexp.scm 842 */
									obj_t BgL_pairz00_3858;

									BgL_pairz00_3858 = CAR(((obj_t) BgL_ppz00_2283));
									BgL_mzd2izd2_2285 = CAR(BgL_pairz00_3858);
								}
								{	/* Unsafe/pregexp.scm 843 */
									obj_t BgL_pairz00_3862;

									BgL_pairz00_3862 = CAR(((obj_t) BgL_ppz00_2283));
									BgL_mzd2nzd2_2286 = CDR(BgL_pairz00_3862);
								}
								return
									string_append_3(c_substring(BgL_strz00_95, 0L,
										(long) CINT(BgL_mzd2izd2_2285)),
									BGl_pregexpzd2replacezd2auxz00zz__regexpz00(BgL_strz00_95,
										BgL_insz00_96, BgL_inszd2lenzd2_2284, BgL_ppz00_2283),
									c_substring(BgL_strz00_95, (long) CINT(BgL_mzd2nzd2_2286),
										BgL_nz00_2282));
							}
						else
							{	/* Unsafe/pregexp.scm 840 */
								return BgL_strz00_95;
							}
					}
				}
			}
		}

	}



/* &pregexp-replace */
	obj_t BGl_z62pregexpzd2replacezb0zz__regexpz00(obj_t BgL_envz00_4366,
		obj_t BgL_patz00_4367, obj_t BgL_strz00_4368, obj_t BgL_insz00_4369)
	{
		{	/* Unsafe/pregexp.scm 836 */
			{	/* Unsafe/pregexp.scm 838 */
				obj_t BgL_auxz00_7093;
				obj_t BgL_auxz00_7086;

				if (STRINGP(BgL_insz00_4369))
					{	/* Unsafe/pregexp.scm 838 */
						BgL_auxz00_7093 = BgL_insz00_4369;
					}
				else
					{
						obj_t BgL_auxz00_7096;

						BgL_auxz00_7096 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(34283L), BGl_string2616z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_insz00_4369);
						FAILURE(BgL_auxz00_7096, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_strz00_4368))
					{	/* Unsafe/pregexp.scm 838 */
						BgL_auxz00_7086 = BgL_strz00_4368;
					}
				else
					{
						obj_t BgL_auxz00_7089;

						BgL_auxz00_7089 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(34283L), BGl_string2616z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_strz00_4368);
						FAILURE(BgL_auxz00_7089, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2replacezd2zz__regexpz00(BgL_patz00_4367,
					BgL_auxz00_7086, BgL_auxz00_7093);
			}
		}

	}



/* pregexp-replace* */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t
		BgL_patz00_97, obj_t BgL_strz00_98, obj_t BgL_insz00_99)
	{
		{	/* Unsafe/pregexp.scm 849 */
			{	/* Unsafe/pregexp.scm 853 */
				obj_t BgL_patz00_2295;
				long BgL_nz00_2296;
				long BgL_inszd2lenzd2_2297;

				if (STRINGP(BgL_patz00_97))
					{	/* Unsafe/pregexp.scm 853 */
						BgL_patz00_2295 = BGl_z52pregexpz52zz__regexpz00(BgL_patz00_97);
					}
				else
					{	/* Unsafe/pregexp.scm 853 */
						BgL_patz00_2295 = BgL_patz00_97;
					}
				BgL_nz00_2296 = STRING_LENGTH(BgL_strz00_98);
				BgL_inszd2lenzd2_2297 = STRING_LENGTH(BgL_insz00_99);
				{
					obj_t BgL_iz00_2299;
					obj_t BgL_rz00_2300;

					BgL_iz00_2299 = BINT(0L);
					BgL_rz00_2300 = BGl_string2602z00zz__regexpz00;
				BgL_zc3z04anonymousza32018ze3z87_2301:
					if (((long) CINT(BgL_iz00_2299) >= BgL_nz00_2296))
						{	/* Unsafe/pregexp.scm 859 */
							return BgL_rz00_2300;
						}
					else
						{	/* Unsafe/pregexp.scm 860 */
							obj_t BgL_ppz00_2303;

							{	/* Unsafe/pregexp.scm 76 */

								BgL_ppz00_2303 =
									BGl_pregexpzd2matchzd2positionsz00zz__regexpz00
									(BgL_patz00_2295, BgL_strz00_98, BgL_iz00_2299,
									BINT(BgL_nz00_2296), BINT(0L));
							}
							if (CBOOL(BgL_ppz00_2303))
								{	/* Unsafe/pregexp.scm 871 */
									obj_t BgL_arg2020z00_2304;
									obj_t BgL_arg2021z00_2305;

									{	/* Unsafe/pregexp.scm 871 */
										obj_t BgL_pairz00_3875;

										BgL_pairz00_3875 = CAR(((obj_t) BgL_ppz00_2303));
										BgL_arg2020z00_2304 = CDR(BgL_pairz00_3875);
									}
									{	/* Unsafe/pregexp.scm 874 */
										obj_t BgL_arg2022z00_2306;
										obj_t BgL_arg2024z00_2307;

										{	/* Unsafe/pregexp.scm 874 */
											obj_t BgL_arg2025z00_2308;

											{	/* Unsafe/pregexp.scm 874 */
												obj_t BgL_pairz00_3879;

												BgL_pairz00_3879 = CAR(((obj_t) BgL_ppz00_2303));
												BgL_arg2025z00_2308 = CAR(BgL_pairz00_3879);
											}
											BgL_arg2022z00_2306 =
												c_substring(BgL_strz00_98,
												(long) CINT(BgL_iz00_2299),
												(long) CINT(BgL_arg2025z00_2308));
										}
										BgL_arg2024z00_2307 =
											BGl_pregexpzd2replacezd2auxz00zz__regexpz00(BgL_strz00_98,
											BgL_insz00_99, BgL_inszd2lenzd2_2297, BgL_ppz00_2303);
										BgL_arg2021z00_2305 =
											string_append_3(BgL_rz00_2300, BgL_arg2022z00_2306,
											BgL_arg2024z00_2307);
									}
									{
										obj_t BgL_rz00_7126;
										obj_t BgL_iz00_7125;

										BgL_iz00_7125 = BgL_arg2020z00_2304;
										BgL_rz00_7126 = BgL_arg2021z00_2305;
										BgL_rz00_2300 = BgL_rz00_7126;
										BgL_iz00_2299 = BgL_iz00_7125;
										goto BgL_zc3z04anonymousza32018ze3z87_2301;
									}
								}
							else
								{	/* Unsafe/pregexp.scm 861 */
									if (((long) CINT(BgL_iz00_2299) == 0L))
										{	/* Unsafe/pregexp.scm 862 */
											return BgL_strz00_98;
										}
									else
										{	/* Unsafe/pregexp.scm 862 */
											return
												string_append(BgL_rz00_2300,
												c_substring(BgL_strz00_98,
													(long) CINT(BgL_iz00_2299), BgL_nz00_2296));
		}}}}}}

	}



/* &pregexp-replace* */
	obj_t BGl_z62pregexpzd2replaceza2z12zz__regexpz00(obj_t BgL_envz00_4370,
		obj_t BgL_patz00_4371, obj_t BgL_strz00_4372, obj_t BgL_insz00_4373)
	{
		{	/* Unsafe/pregexp.scm 849 */
			{	/* Unsafe/pregexp.scm 853 */
				obj_t BgL_auxz00_7141;
				obj_t BgL_auxz00_7134;

				if (STRINGP(BgL_insz00_4373))
					{	/* Unsafe/pregexp.scm 853 */
						BgL_auxz00_7141 = BgL_insz00_4373;
					}
				else
					{
						obj_t BgL_auxz00_7144;

						BgL_auxz00_7144 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(34784L), BGl_string2617z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_insz00_4373);
						FAILURE(BgL_auxz00_7144, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_strz00_4372))
					{	/* Unsafe/pregexp.scm 853 */
						BgL_auxz00_7134 = BgL_strz00_4372;
					}
				else
					{
						obj_t BgL_auxz00_7137;

						BgL_auxz00_7137 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(34784L), BGl_string2617z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_strz00_4372);
						FAILURE(BgL_auxz00_7137, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2replaceza2z70zz__regexpz00(BgL_patz00_4371,
					BgL_auxz00_7134, BgL_auxz00_7141);
			}
		}

	}



/* pregexp-quote */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2quotezd2zz__regexpz00(obj_t BgL_sz00_100)
	{
		{	/* Unsafe/pregexp.scm 877 */
			{	/* Unsafe/pregexp.scm 879 */
				long BgL_g1128z00_2318;

				BgL_g1128z00_2318 = (STRING_LENGTH(BgL_sz00_100) - 1L);
				{
					long BgL_iz00_2321;
					obj_t BgL_rz00_2322;

					BgL_iz00_2321 = BgL_g1128z00_2318;
					BgL_rz00_2322 = BNIL;
				BgL_zc3z04anonymousza32029ze3z87_2323:
					if ((BgL_iz00_2321 < 0L))
						{	/* Unsafe/pregexp.scm 880 */
							return
								BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_rz00_2322);
						}
					else
						{	/* Unsafe/pregexp.scm 881 */
							long BgL_arg2031z00_2325;
							obj_t BgL_arg2033z00_2326;

							BgL_arg2031z00_2325 = (BgL_iz00_2321 - 1L);
							{	/* Unsafe/pregexp.scm 882 */
								unsigned char BgL_cz00_2327;

								BgL_cz00_2327 = STRING_REF(BgL_sz00_100, BgL_iz00_2321);
								if (CBOOL(BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BCHAR
											(BgL_cz00_2327), BGl_list2618z00zz__regexpz00)))
									{	/* Unsafe/pregexp.scm 885 */
										obj_t BgL_arg2036z00_2329;

										BgL_arg2036z00_2329 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_2327), BgL_rz00_2322);
										BgL_arg2033z00_2326 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')),
											BgL_arg2036z00_2329);
									}
								else
									{	/* Unsafe/pregexp.scm 883 */
										BgL_arg2033z00_2326 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_2327), BgL_rz00_2322);
									}
							}
							{
								obj_t BgL_rz00_7167;
								long BgL_iz00_7166;

								BgL_iz00_7166 = BgL_arg2031z00_2325;
								BgL_rz00_7167 = BgL_arg2033z00_2326;
								BgL_rz00_2322 = BgL_rz00_7167;
								BgL_iz00_2321 = BgL_iz00_7166;
								goto BgL_zc3z04anonymousza32029ze3z87_2323;
							}
						}
				}
			}
		}

	}



/* &pregexp-quote */
	obj_t BGl_z62pregexpzd2quotezb0zz__regexpz00(obj_t BgL_envz00_4374,
		obj_t BgL_sz00_4375)
	{
		{	/* Unsafe/pregexp.scm 877 */
			{	/* Unsafe/pregexp.scm 879 */
				obj_t BgL_auxz00_7168;

				if (STRINGP(BgL_sz00_4375))
					{	/* Unsafe/pregexp.scm 879 */
						BgL_auxz00_7168 = BgL_sz00_4375;
					}
				else
					{
						obj_t BgL_auxz00_7171;

						BgL_auxz00_7171 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2476z00zz__regexpz00,
							BINT(35837L), BGl_string2619z00zz__regexpz00,
							BGl_string2604z00zz__regexpz00, BgL_sz00_4375);
						FAILURE(BgL_auxz00_7171, BFALSE, BFALSE);
					}
				return BGl_pregexpzd2quotezd2zz__regexpz00(BgL_auxz00_7168);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__regexpz00(void)
	{
		{	/* Unsafe/pregexp.scm 18 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string2620z00zz__regexpz00));
		}

	}

#ifdef __cplusplus
}
#endif
