/*===========================================================================*/
/*   (Unsafe/regex.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/regex.scm -indent -o objs/obj_u/Unsafe/regex.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___REGEXP_TYPE_DEFINITIONS
#define BGL___REGEXP_TYPE_DEFINITIONS
#endif													// BGL___REGEXP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t bgl_regfree(obj_t);
	extern obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2quotezd2zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_regexpzd2patternzd2zz__regexpz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regexpzf3zf3zz__regexpz00(obj_t);
	extern obj_t bgl_regmatch(obj_t, char *, bool_t, int, int, int);
	static obj_t BGl_z62regexpzf3z91zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__regexpz00 = BUNSPEC;
	static obj_t BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00(obj_t,
		obj_t, long);
	static obj_t BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t,
		obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_pregexpzd2listzd2refz00zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t, obj_t, obj_t,
		long, long, obj_t);
	static obj_t BGl_z62pregexpzd2quotezb0zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_list1938z00zz__regexpz00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zz__regexpz00(void);
	static obj_t BGl_genericzd2initzd2zz__regexpz00(void);
	static obj_t BGl__pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__regexpz00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__regexpz00(void);
	static obj_t BGl_objectzd2initzd2zz__regexpz00(void);
	static obj_t BGl_pregexpzd2normaliza7ez75zz__regexpz00(obj_t);
	extern obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_pregexpzd2replacezd2auxz00zz__regexpz00(obj_t, obj_t, long,
		obj_t);
	static obj_t BGl__pregexpzd2matchzd2zz__regexpz00(obj_t, obj_t);
	extern obj_t BGl_memvz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpz00zz__regexpz00(obj_t, obj_t);
	extern obj_t bgl_reverse_bang(obj_t);
	extern obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62pregexpz62zz__regexpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2splitzd2zz__regexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2replacezd2zz__regexpz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62pregexpzd2replacezb0zz__regexpz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t bgl_regcomp(obj_t, obj_t, bool_t);
	static obj_t BGl_z62pregexpzd2replaceza2z12zz__regexpz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62regexpzd2patternzb0zz__regexpz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__regexpz00(void);
	static obj_t BGl_z62regexpzd2capturezd2countz62zz__regexpz00(obj_t, obj_t);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	BGL_EXPORTED_DECL obj_t BGl_pregexpzd2matchzd2zz__regexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62pregexpzd2splitzb0zz__regexpz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_regexpzd2capturezd2countz00zz__regexpz00(obj_t);
	extern obj_t make_string(long, unsigned char);
	extern obj_t string_append(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2envzd2zz__regexpz00,
		BgL_bgl_za762pregexpza762za7za7_1941z00, va_generic_entry,
		BGl_z62pregexpz62zz__regexpz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regexpzd2capturezd2countzd2envzd2zz__regexpz00,
		BgL_bgl_za762regexpza7d2capt1942z00,
		BGl_z62regexpzd2capturezd2countz62zz__regexpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2splitzd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2spl1943z00, BGl_z62pregexpzd2splitzb0zz__regexpz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2quotezd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2quo1944z00, BGl_z62pregexpzd2quotezb0zz__regexpz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regexpzd2patternzd2envz00zz__regexpz00,
		BgL_bgl_za762regexpza7d2patt1945z00,
		BGl_z62regexpzd2patternzb0zz__regexpz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2replacezd2envz00zz__regexpz00,
		BgL_bgl_za762pregexpza7d2rep1946z00,
		BGl_z62pregexpzd2replacezb0zz__regexpz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regexpzf3zd2envz21zz__regexpz00,
		BgL_bgl_za762regexpza7f3za791za71947z00, BGl_z62regexpzf3z91zz__regexpz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2replaceza2zd2envza2zz__regexpz00,
		BgL_bgl_za762pregexpza7d2rep1948z00,
		BGl_z62pregexpzd2replaceza2z12zz__regexpz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pregexpzd2matchzd2nzd2positionsz12zd2envz12zz__regexpz00,
		BgL_bgl__pregexpza7d2match1949za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pregexpzd2matchzd2positionszd2envzd2zz__regexpz00,
		BgL_bgl__pregexpza7d2match1950za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2positionsz00zz__regexpz00, BFALSE, -1);
	      DEFINE_STRING(BGl_string1917z00zz__regexpz00,
		BgL_bgl_string1917za700za7za7_1951za7,
		"/tmp/bigloo/runtime/Unsafe/regex.scm", 36);
	      DEFINE_STRING(BGl_string1918z00zz__regexpz00,
		BgL_bgl_string1918za700za7za7_1952za7, "&regexp-pattern", 15);
	      DEFINE_STRING(BGl_string1919z00zz__regexpz00,
		BgL_bgl_string1919za700za7za7_1953za7, "regexp", 6);
	      DEFINE_STRING(BGl_string1920z00zz__regexpz00,
		BgL_bgl_string1920za700za7za7_1954za7, "&regexp-capture-count", 21);
	      DEFINE_STRING(BGl_string1921z00zz__regexpz00,
		BgL_bgl_string1921za700za7za7_1955za7, "[A-Za-z0-9_]", 12);
	      DEFINE_STRING(BGl_string1922z00zz__regexpz00,
		BgL_bgl_string1922za700za7za7_1956za7, "[^A-Za-z0-9_]", 13);
	      DEFINE_STRING(BGl_string1923z00zz__regexpz00,
		BgL_bgl_string1923za700za7za7_1957za7, "[0-9]", 5);
	      DEFINE_STRING(BGl_string1924z00zz__regexpz00,
		BgL_bgl_string1924za700za7za7_1958za7, "[^0-9]", 6);
	      DEFINE_STRING(BGl_string1925z00zz__regexpz00,
		BgL_bgl_string1925za700za7za7_1959za7, "[ \t\r\n\v\f]", 8);
	      DEFINE_STRING(BGl_string1926z00zz__regexpz00,
		BgL_bgl_string1926za700za7za7_1960za7, "[^ \t\r\n\v\f]", 9);
	      DEFINE_STRING(BGl_string1927z00zz__regexpz00,
		BgL_bgl_string1927za700za7za7_1961za7, "&pregexp", 8);
	      DEFINE_STRING(BGl_string1928z00zz__regexpz00,
		BgL_bgl_string1928za700za7za7_1962za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1929z00zz__regexpz00,
		BgL_bgl_string1929za700za7za7_1963za7, "_pregexp-match-positions", 24);
	      DEFINE_STRING(BGl_string1930z00zz__regexpz00,
		BgL_bgl_string1930za700za7za7_1964za7, "bint", 4);
	      DEFINE_STRING(BGl_string1931z00zz__regexpz00,
		BgL_bgl_string1931za700za7za7_1965za7, "_pregexp-match", 14);
	      DEFINE_STRING(BGl_string1932z00zz__regexpz00,
		BgL_bgl_string1932za700za7za7_1966za7, "_pregexp-match-n-positions!", 27);
	      DEFINE_STRING(BGl_string1933z00zz__regexpz00,
		BgL_bgl_string1933za700za7za7_1967za7, "vector", 6);
	      DEFINE_STRING(BGl_string1934z00zz__regexpz00,
		BgL_bgl_string1934za700za7za7_1968za7, "", 0);
	      DEFINE_STRING(BGl_string1935z00zz__regexpz00,
		BgL_bgl_string1935za700za7za7_1969za7, "&pregexp-split", 14);
	      DEFINE_STRING(BGl_string1936z00zz__regexpz00,
		BgL_bgl_string1936za700za7za7_1970za7, "&pregexp-replace", 16);
	      DEFINE_STRING(BGl_string1937z00zz__regexpz00,
		BgL_bgl_string1937za700za7za7_1971za7, "&pregexp-replace*", 17);
	      DEFINE_STRING(BGl_string1939z00zz__regexpz00,
		BgL_bgl_string1939za700za7za7_1972za7, "&pregexp-quote", 14);
	      DEFINE_STRING(BGl_string1940z00zz__regexpz00,
		BgL_bgl_string1940za700za7za7_1973za7, "__regexp", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pregexpzd2matchzd2envz00zz__regexpz00,
		BgL_bgl__pregexpza7d2match1974za7, opt_generic_entry,
		BGl__pregexpzd2matchzd2zz__regexpz00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__regexpz00));
		     ADD_ROOT((void *) (&BGl_list1938z00zz__regexpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long
		BgL_checksumz00_2602, char *BgL_fromz00_2603)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__regexpz00))
				{
					BGl_requirezd2initializa7ationz75zz__regexpz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__regexpz00();
					BGl_cnstzd2initzd2zz__regexpz00();
					BGl_importedzd2moduleszd2initz00zz__regexpz00();
					return BGl_methodzd2initzd2zz__regexpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/regex.scm 15 */
			return (BGl_list1938z00zz__regexpz00 =
				MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')),
					MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '.')),
						MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '?')),
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '*')),
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '+')),
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '|')),
										MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '^')),
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '$')),
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '[')),
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ']')),
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '{')),
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '}')),
																MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '(')),
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ')')),
																		BNIL)))))))))))))), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__regexpz00(void)
	{
		{	/* Unsafe/regex.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* regexp? */
	BGL_EXPORTED_DEF bool_t BGl_regexpzf3zf3zz__regexpz00(obj_t BgL_objz00_3)
	{
		{	/* Unsafe/regex.scm 83 */
			return BGL_REGEXPP(BgL_objz00_3);
		}

	}



/* &regexp? */
	obj_t BGl_z62regexpzf3z91zz__regexpz00(obj_t BgL_envz00_2478,
		obj_t BgL_objz00_2479)
	{
		{	/* Unsafe/regex.scm 83 */
			return BBOOL(BGl_regexpzf3zf3zz__regexpz00(BgL_objz00_2479));
		}

	}



/* regexp-pattern */
	BGL_EXPORTED_DEF obj_t BGl_regexpzd2patternzd2zz__regexpz00(obj_t BgL_rez00_4)
	{
		{	/* Unsafe/regex.scm 89 */
			return BGL_REGEXP_PAT(BgL_rez00_4);
		}

	}



/* &regexp-pattern */
	obj_t BGl_z62regexpzd2patternzb0zz__regexpz00(obj_t BgL_envz00_2480,
		obj_t BgL_rez00_2481)
	{
		{	/* Unsafe/regex.scm 89 */
			{	/* Unsafe/regex.scm 90 */
				obj_t BgL_auxz00_2644;

				if (BGl_regexpzf3zf3zz__regexpz00(BgL_rez00_2481))
					{	/* Unsafe/regex.scm 90 */
						BgL_auxz00_2644 = BgL_rez00_2481;
					}
				else
					{
						obj_t BgL_auxz00_2647;

						BgL_auxz00_2647 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(3765L), BGl_string1918z00zz__regexpz00,
							BGl_string1919z00zz__regexpz00, BgL_rez00_2481);
						FAILURE(BgL_auxz00_2647, BFALSE, BFALSE);
					}
				return BGl_regexpzd2patternzd2zz__regexpz00(BgL_auxz00_2644);
			}
		}

	}



/* regexp-capture-count */
	BGL_EXPORTED_DEF long BGl_regexpzd2capturezd2countz00zz__regexpz00(obj_t
		BgL_rez00_5)
	{
		{	/* Unsafe/regex.scm 95 */
			return BGL_REGEXP_CAPTURE_COUNT(BgL_rez00_5);
		}

	}



/* &regexp-capture-count */
	obj_t BGl_z62regexpzd2capturezd2countz62zz__regexpz00(obj_t BgL_envz00_2482,
		obj_t BgL_rez00_2483)
	{
		{	/* Unsafe/regex.scm 95 */
			{	/* Unsafe/regex.scm 96 */
				long BgL_tmpz00_2653;

				{	/* Unsafe/regex.scm 96 */
					obj_t BgL_auxz00_2654;

					if (BGl_regexpzf3zf3zz__regexpz00(BgL_rez00_2483))
						{	/* Unsafe/regex.scm 96 */
							BgL_auxz00_2654 = BgL_rez00_2483;
						}
					else
						{
							obj_t BgL_auxz00_2657;

							BgL_auxz00_2657 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
								BINT(4054L), BGl_string1920z00zz__regexpz00,
								BGl_string1919z00zz__regexpz00, BgL_rez00_2483);
							FAILURE(BgL_auxz00_2657, BFALSE, BFALSE);
						}
					BgL_tmpz00_2653 =
						BGl_regexpzd2capturezd2countz00zz__regexpz00(BgL_auxz00_2654);
				}
				return BINT(BgL_tmpz00_2653);
			}
		}

	}



/* pregexp-normalize */
	obj_t BGl_pregexpzd2normaliza7ez75zz__regexpz00(obj_t BgL_rez00_6)
	{
		{	/* Unsafe/regex.scm 110 */
			{
				obj_t BgL_rez00_1215;
				long BgL_cz00_1216;
				obj_t BgL_rez00_1190;

				{	/* Unsafe/regex.scm 164 */
					long BgL_cz00_1188;

					BgL_rez00_1190 = BgL_rez00_6;
					{	/* Unsafe/regex.scm 113 */
						long BgL_lenz00_1192;

						BgL_lenz00_1192 = STRING_LENGTH(((obj_t) BgL_rez00_1190));
						{
							long BgL_iz00_1194;
							long BgL_cz00_1195;

							BgL_iz00_1194 = 0L;
							BgL_cz00_1195 = 0L;
						BgL_zc3z04anonymousza31212ze3z87_1196:
							if ((BgL_iz00_1194 >= (BgL_lenz00_1192 - 1L)))
								{	/* Unsafe/regex.scm 117 */
									BgL_cz00_1188 = BgL_cz00_1195;
								}
							else
								{	/* Unsafe/regex.scm 117 */
									if (
										(STRING_REF(
												((obj_t) BgL_rez00_1190),
												BgL_iz00_1194) == ((unsigned char) '\\')))
										{
											long BgL_cz00_2674;
											long BgL_iz00_2672;

											BgL_iz00_2672 = (BgL_iz00_1194 + 2L);
											{	/* Unsafe/regex.scm 121 */
												long BgL_tmpz00_2675;

												switch (STRING_REF(
														((obj_t) BgL_rez00_1190), (BgL_iz00_1194 + 1L)))
													{
													case ((unsigned char) 'w'):

														BgL_tmpz00_2675 = 10L;
														break;
													case ((unsigned char) 'W'):

														BgL_tmpz00_2675 = 11L;
														break;
													case ((unsigned char) 'd'):

														BgL_tmpz00_2675 = 3L;
														break;
													case ((unsigned char) 'D'):

														BgL_tmpz00_2675 = 4L;
														break;
													case ((unsigned char) 's'):

														BgL_tmpz00_2675 = 6L;
														break;
													case ((unsigned char) 'S'):

														BgL_tmpz00_2675 = 7L;
														break;
													default:
														BgL_tmpz00_2675 = 0L;
													}
												BgL_cz00_2674 = (BgL_cz00_1195 + BgL_tmpz00_2675);
											}
											BgL_cz00_1195 = BgL_cz00_2674;
											BgL_iz00_1194 = BgL_iz00_2672;
											goto BgL_zc3z04anonymousza31212ze3z87_1196;
										}
									else
										{
											long BgL_iz00_2681;

											BgL_iz00_2681 = (BgL_iz00_1194 + 1L);
											BgL_iz00_1194 = BgL_iz00_2681;
											goto BgL_zc3z04anonymousza31212ze3z87_1196;
										}
								}
						}
					}
					if ((BgL_cz00_1188 == 0L))
						{	/* Unsafe/regex.scm 165 */
							return BgL_rez00_6;
						}
					else
						{	/* Unsafe/regex.scm 165 */
							BgL_rez00_1215 = BgL_rez00_6;
							BgL_cz00_1216 = BgL_cz00_1188;
							{	/* Unsafe/regex.scm 133 */
								long BgL_lenz00_1218;

								BgL_lenz00_1218 = STRING_LENGTH(((obj_t) BgL_rez00_1215));
								{	/* Unsafe/regex.scm 133 */
									obj_t BgL_newz00_1219;

									{	/* Unsafe/regex.scm 134 */
										long BgL_arg1327z00_1263;

										BgL_arg1327z00_1263 = (BgL_lenz00_1218 + BgL_cz00_1216);
										{	/* Ieee/string.scm 172 */

											BgL_newz00_1219 =
												make_string(BgL_arg1327z00_1263, ((unsigned char) ' '));
									}}
									{	/* Unsafe/regex.scm 134 */

										{
											long BgL_iz00_1221;
											long BgL_jz00_1222;

											BgL_iz00_1221 = 0L;
											BgL_jz00_1222 = 0L;
										BgL_zc3z04anonymousza31231ze3z87_1223:
											if ((BgL_iz00_1221 >= (BgL_lenz00_1218 - 1L)))
												{	/* Unsafe/regex.scm 138 */
													if ((BgL_iz00_1221 < BgL_lenz00_1218))
														{	/* Unsafe/regex.scm 140 */
															unsigned char BgL_arg1236z00_1227;

															BgL_arg1236z00_1227 =
																STRING_REF(
																((obj_t) BgL_rez00_1215), BgL_iz00_1221);
															STRING_SET(BgL_newz00_1219, BgL_jz00_1222,
																BgL_arg1236z00_1227);
														}
													else
														{	/* Unsafe/regex.scm 139 */
															BFALSE;
														}
													return BgL_newz00_1219;
												}
											else
												{	/* Unsafe/regex.scm 138 */
													if (
														(STRING_REF(
																((obj_t) BgL_rez00_1215),
																BgL_iz00_1221) == ((unsigned char) '\\')))
														{

															{	/* Unsafe/regex.scm 143 */
																unsigned char BgL_aux1045z00_1231;

																BgL_aux1045z00_1231 =
																	STRING_REF(
																	((obj_t) BgL_rez00_1215),
																	(BgL_iz00_1221 + 1L));
																switch (BgL_aux1045z00_1231)
																	{
																	case ((unsigned char) 'w'):

																		{	/* Unsafe/regex.scm 145 */
																			long BgL_arg1242z00_1233;
																			long BgL_arg1244z00_1234;

																			BgL_arg1242z00_1233 =
																				(BgL_iz00_1221 + 2L);
																			{	/* Unsafe/regex.scm 145 */
																				long BgL_arg1248z00_1235;

																				blit_string
																					(BGl_string1921z00zz__regexpz00, 0L,
																					BgL_newz00_1219, BgL_jz00_1222, 12L);
																				BgL_arg1248z00_1235 = 12L;
																				BgL_arg1244z00_1234 =
																					(BgL_jz00_1222 + BgL_arg1248z00_1235);
																			}
																			{
																				long BgL_jz00_2708;
																				long BgL_iz00_2707;

																				BgL_iz00_2707 = BgL_arg1242z00_1233;
																				BgL_jz00_2708 = BgL_arg1244z00_1234;
																				BgL_jz00_1222 = BgL_jz00_2708;
																				BgL_iz00_1221 = BgL_iz00_2707;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_1223;
																			}
																		}
																		break;
																	case ((unsigned char) 'W'):

																		{	/* Unsafe/regex.scm 147 */
																			long BgL_arg1249z00_1236;
																			long BgL_arg1252z00_1237;

																			BgL_arg1249z00_1236 =
																				(BgL_iz00_1221 + 2L);
																			{	/* Unsafe/regex.scm 147 */
																				long BgL_arg1268z00_1238;

																				blit_string
																					(BGl_string1922z00zz__regexpz00, 0L,
																					BgL_newz00_1219, BgL_jz00_1222, 13L);
																				BgL_arg1268z00_1238 = 13L;
																				BgL_arg1252z00_1237 =
																					(BgL_jz00_1222 + BgL_arg1268z00_1238);
																			}
																			{
																				long BgL_jz00_2713;
																				long BgL_iz00_2712;

																				BgL_iz00_2712 = BgL_arg1249z00_1236;
																				BgL_jz00_2713 = BgL_arg1252z00_1237;
																				BgL_jz00_1222 = BgL_jz00_2713;
																				BgL_iz00_1221 = BgL_iz00_2712;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_1223;
																			}
																		}
																		break;
																	case ((unsigned char) 'd'):

																		{	/* Unsafe/regex.scm 149 */
																			long BgL_arg1272z00_1239;
																			long BgL_arg1284z00_1240;

																			BgL_arg1272z00_1239 =
																				(BgL_iz00_1221 + 2L);
																			{	/* Unsafe/regex.scm 149 */
																				long BgL_arg1304z00_1241;

																				blit_string
																					(BGl_string1923z00zz__regexpz00, 0L,
																					BgL_newz00_1219, BgL_jz00_1222, 5L);
																				BgL_arg1304z00_1241 = 5L;
																				BgL_arg1284z00_1240 =
																					(BgL_jz00_1222 + BgL_arg1304z00_1241);
																			}
																			{
																				long BgL_jz00_2718;
																				long BgL_iz00_2717;

																				BgL_iz00_2717 = BgL_arg1272z00_1239;
																				BgL_jz00_2718 = BgL_arg1284z00_1240;
																				BgL_jz00_1222 = BgL_jz00_2718;
																				BgL_iz00_1221 = BgL_iz00_2717;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_1223;
																			}
																		}
																		break;
																	case ((unsigned char) 'D'):

																		{	/* Unsafe/regex.scm 151 */
																			long BgL_arg1305z00_1242;
																			long BgL_arg1306z00_1243;

																			BgL_arg1305z00_1242 =
																				(BgL_iz00_1221 + 2L);
																			{	/* Unsafe/regex.scm 151 */
																				long BgL_arg1307z00_1244;

																				blit_string
																					(BGl_string1924z00zz__regexpz00, 0L,
																					BgL_newz00_1219, BgL_jz00_1222, 6L);
																				BgL_arg1307z00_1244 = 6L;
																				BgL_arg1306z00_1243 =
																					(BgL_jz00_1222 + BgL_arg1307z00_1244);
																			}
																			{
																				long BgL_jz00_2723;
																				long BgL_iz00_2722;

																				BgL_iz00_2722 = BgL_arg1305z00_1242;
																				BgL_jz00_2723 = BgL_arg1306z00_1243;
																				BgL_jz00_1222 = BgL_jz00_2723;
																				BgL_iz00_1221 = BgL_iz00_2722;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_1223;
																			}
																		}
																		break;
																	case ((unsigned char) 's'):

																		{	/* Unsafe/regex.scm 153 */
																			long BgL_arg1308z00_1245;
																			long BgL_arg1309z00_1246;

																			BgL_arg1308z00_1245 =
																				(BgL_iz00_1221 + 2L);
																			{	/* Unsafe/regex.scm 153 */
																				long BgL_arg1310z00_1247;

																				blit_string
																					(BGl_string1925z00zz__regexpz00, 0L,
																					BgL_newz00_1219, BgL_jz00_1222, 8L);
																				BgL_arg1310z00_1247 = 8L;
																				BgL_arg1309z00_1246 =
																					(BgL_jz00_1222 + BgL_arg1310z00_1247);
																			}
																			{
																				long BgL_jz00_2728;
																				long BgL_iz00_2727;

																				BgL_iz00_2727 = BgL_arg1308z00_1245;
																				BgL_jz00_2728 = BgL_arg1309z00_1246;
																				BgL_jz00_1222 = BgL_jz00_2728;
																				BgL_iz00_1221 = BgL_iz00_2727;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_1223;
																			}
																		}
																		break;
																	case ((unsigned char) 'S'):

																		{	/* Unsafe/regex.scm 155 */
																			long BgL_arg1311z00_1248;
																			long BgL_arg1312z00_1249;

																			BgL_arg1311z00_1248 =
																				(BgL_iz00_1221 + 2L);
																			{	/* Unsafe/regex.scm 155 */
																				long BgL_arg1314z00_1250;

																				blit_string
																					(BGl_string1926z00zz__regexpz00, 0L,
																					BgL_newz00_1219, BgL_jz00_1222, 9L);
																				BgL_arg1314z00_1250 = 9L;
																				BgL_arg1312z00_1249 =
																					(BgL_jz00_1222 + BgL_arg1314z00_1250);
																			}
																			{
																				long BgL_jz00_2733;
																				long BgL_iz00_2732;

																				BgL_iz00_2732 = BgL_arg1311z00_1248;
																				BgL_jz00_2733 = BgL_arg1312z00_1249;
																				BgL_jz00_1222 = BgL_jz00_2733;
																				BgL_iz00_1221 = BgL_iz00_2732;
																				goto
																					BgL_zc3z04anonymousza31231ze3z87_1223;
																			}
																		}
																		break;
																	default:
																		STRING_SET(BgL_newz00_1219, BgL_jz00_1222,
																			((unsigned char) '\\'));
																		{	/* Unsafe/regex.scm 158 */
																			long BgL_arg1316z00_1252;
																			unsigned char BgL_arg1317z00_1253;

																			BgL_arg1316z00_1252 =
																				(BgL_jz00_1222 + 1L);
																			BgL_arg1317z00_1253 =
																				STRING_REF(
																				((obj_t) BgL_rez00_1215),
																				(BgL_iz00_1221 + 1L));
																			STRING_SET(BgL_newz00_1219,
																				BgL_arg1316z00_1252,
																				BgL_arg1317z00_1253);
																		}
																		{
																			long BgL_jz00_2742;
																			long BgL_iz00_2740;

																			BgL_iz00_2740 = (BgL_iz00_1221 + 2L);
																			BgL_jz00_2742 = (BgL_jz00_1222 + 2L);
																			BgL_jz00_1222 = BgL_jz00_2742;
																			BgL_iz00_1221 = BgL_iz00_2740;
																			goto
																				BgL_zc3z04anonymousza31231ze3z87_1223;
																		}
																	}
															}
														}
													else
														{	/* Unsafe/regex.scm 142 */
															{	/* Unsafe/regex.scm 161 */
																unsigned char BgL_arg1321z00_1257;

																BgL_arg1321z00_1257 =
																	STRING_REF(
																	((obj_t) BgL_rez00_1215), BgL_iz00_1221);
																STRING_SET(BgL_newz00_1219, BgL_jz00_1222,
																	BgL_arg1321z00_1257);
															}
															{
																long BgL_jz00_2750;
																long BgL_iz00_2748;

																BgL_iz00_2748 = (BgL_iz00_1221 + 1L);
																BgL_jz00_2750 = (BgL_jz00_1222 + 1L);
																BgL_jz00_1222 = BgL_jz00_2750;
																BgL_iz00_1221 = BgL_iz00_2748;
																goto BgL_zc3z04anonymousza31231ze3z87_1223;
															}
														}
												}
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* pregexp */
	BGL_EXPORTED_DEF obj_t BGl_pregexpz00zz__regexpz00(obj_t BgL_rez00_7,
		obj_t BgL_optzd2argszd2_8)
	{
		{	/* Unsafe/regex.scm 172 */
			return
				bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00(BgL_rez00_7),
				BgL_optzd2argszd2_8, ((bool_t) 1));
		}

	}



/* &pregexp */
	obj_t BGl_z62pregexpz62zz__regexpz00(obj_t BgL_envz00_2484,
		obj_t BgL_rez00_2485, obj_t BgL_optzd2argszd2_2486)
	{
		{	/* Unsafe/regex.scm 172 */
			{	/* Unsafe/regex.scm 173 */
				obj_t BgL_auxz00_2754;

				if (STRINGP(BgL_rez00_2485))
					{	/* Unsafe/regex.scm 173 */
						BgL_auxz00_2754 = BgL_rez00_2485;
					}
				else
					{
						obj_t BgL_auxz00_2757;

						BgL_auxz00_2757 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(6633L), BGl_string1927z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_rez00_2485);
						FAILURE(BgL_auxz00_2757, BFALSE, BFALSE);
					}
				return
					BGl_pregexpz00zz__regexpz00(BgL_auxz00_2754, BgL_optzd2argszd2_2486);
			}
		}

	}



/* _pregexp-match-positions */
	obj_t BGl__pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t
		BgL_env1124z00_23, obj_t BgL_opt1123z00_22)
	{
		{	/* Unsafe/regex.scm 189 */
			{	/* Unsafe/regex.scm 189 */
				obj_t BgL_patz00_1282;
				obj_t BgL_strz00_1283;

				BgL_patz00_1282 = VECTOR_REF(BgL_opt1123z00_22, 0L);
				BgL_strz00_1283 = VECTOR_REF(BgL_opt1123z00_22, 1L);
				switch (VECTOR_LENGTH(BgL_opt1123z00_22))
					{
					case 2L:

						{	/* Unsafe/regex.scm 189 */
							long BgL_endz00_1287;

							{	/* Unsafe/regex.scm 189 */
								obj_t BgL_stringz00_2031;

								if (STRINGP(BgL_strz00_1283))
									{	/* Unsafe/regex.scm 189 */
										BgL_stringz00_2031 = BgL_strz00_1283;
									}
								else
									{
										obj_t BgL_auxz00_2766;

										BgL_auxz00_2766 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1917z00zz__regexpz00, BINT(7464L),
											BGl_string1929z00zz__regexpz00,
											BGl_string1928z00zz__regexpz00, BgL_strz00_1283);
										FAILURE(BgL_auxz00_2766, BFALSE, BFALSE);
									}
								BgL_endz00_1287 = STRING_LENGTH(BgL_stringz00_2031);
							}
							{	/* Unsafe/regex.scm 189 */

								{	/* Unsafe/regex.scm 189 */
									obj_t BgL_strz00_2032;

									if (STRINGP(BgL_strz00_1283))
										{	/* Unsafe/regex.scm 189 */
											BgL_strz00_2032 = BgL_strz00_1283;
										}
									else
										{
											obj_t BgL_auxz00_2773;

											BgL_auxz00_2773 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(7399L),
												BGl_string1929z00zz__regexpz00,
												BGl_string1928z00zz__regexpz00, BgL_strz00_1283);
											FAILURE(BgL_auxz00_2773, BFALSE, BFALSE);
										}
									if (BGL_REGEXPP(BgL_patz00_1282))
										{	/* Unsafe/regex.scm 180 */
											obj_t BgL_tmpz00_2779;

											if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1282))
												{	/* Unsafe/regex.scm 180 */
													BgL_tmpz00_2779 = BgL_patz00_1282;
												}
											else
												{
													obj_t BgL_auxz00_2782;

													BgL_auxz00_2782 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1917z00zz__regexpz00, BINT(6996L),
														BGl_string1929z00zz__regexpz00,
														BGl_string1919z00zz__regexpz00, BgL_patz00_1282);
													FAILURE(BgL_auxz00_2782, BFALSE, BFALSE);
												}
											return
												bgl_regmatch(BgL_tmpz00_2779,
												BSTRING_TO_STRING(BgL_strz00_2032), ((bool_t) 0),
												(int) (0L), (int) (BgL_endz00_1287), (int) (0L));
										}
									else
										{	/* Unsafe/regex.scm 181 */
											obj_t BgL_rxz00_2034;

											{	/* Unsafe/regex.scm 181 */
												obj_t BgL_arg1331z00_2035;

												BgL_arg1331z00_2035 =
													BGl_pregexpzd2normaliza7ez75zz__regexpz00
													(BgL_patz00_1282);
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_tmpz00_2792;

													if (STRINGP(BgL_arg1331z00_2035))
														{	/* Unsafe/regex.scm 181 */
															BgL_tmpz00_2792 = BgL_arg1331z00_2035;
														}
													else
														{
															obj_t BgL_auxz00_2795;

															BgL_auxz00_2795 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(7080L),
																BGl_string1929z00zz__regexpz00,
																BGl_string1928z00zz__regexpz00,
																BgL_arg1331z00_2035);
															FAILURE(BgL_auxz00_2795, BFALSE, BFALSE);
														}
													BgL_rxz00_2034 =
														bgl_regcomp(BgL_tmpz00_2792, BNIL, ((bool_t) 0));
												}
											}
											{	/* Unsafe/regex.scm 181 */
												obj_t BgL_valz00_2036;

												BgL_valz00_2036 =
													bgl_regmatch(BgL_rxz00_2034,
													BSTRING_TO_STRING(BgL_strz00_2032), ((bool_t) 0),
													(int) (0L), (int) (BgL_endz00_1287), (int) (0L));
												{	/* Unsafe/regex.scm 182 */

													bgl_regfree(BgL_rxz00_2034);
													return BgL_valz00_2036;
												}
											}
										}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/regex.scm 189 */
							obj_t BgL_begz00_1289;

							BgL_begz00_1289 = VECTOR_REF(BgL_opt1123z00_22, 2L);
							{	/* Unsafe/regex.scm 189 */
								long BgL_endz00_1290;

								{	/* Unsafe/regex.scm 189 */
									obj_t BgL_stringz00_2037;

									if (STRINGP(BgL_strz00_1283))
										{	/* Unsafe/regex.scm 189 */
											BgL_stringz00_2037 = BgL_strz00_1283;
										}
									else
										{
											obj_t BgL_auxz00_2809;

											BgL_auxz00_2809 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(7464L),
												BGl_string1929z00zz__regexpz00,
												BGl_string1928z00zz__regexpz00, BgL_strz00_1283);
											FAILURE(BgL_auxz00_2809, BFALSE, BFALSE);
										}
									BgL_endz00_1290 = STRING_LENGTH(BgL_stringz00_2037);
								}
								{	/* Unsafe/regex.scm 189 */

									{	/* Unsafe/regex.scm 189 */
										obj_t BgL_strz00_2038;

										if (STRINGP(BgL_strz00_1283))
											{	/* Unsafe/regex.scm 189 */
												BgL_strz00_2038 = BgL_strz00_1283;
											}
										else
											{
												obj_t BgL_auxz00_2816;

												BgL_auxz00_2816 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(7399L),
													BGl_string1929z00zz__regexpz00,
													BGl_string1928z00zz__regexpz00, BgL_strz00_1283);
												FAILURE(BgL_auxz00_2816, BFALSE, BFALSE);
											}
										if (BGL_REGEXPP(BgL_patz00_1282))
											{	/* Unsafe/regex.scm 180 */
												int BgL_auxz00_2829;
												obj_t BgL_tmpz00_2822;

												{	/* Unsafe/regex.scm 180 */
													obj_t BgL_tmpz00_2831;

													if (INTEGERP(BgL_begz00_1289))
														{	/* Unsafe/regex.scm 180 */
															BgL_tmpz00_2831 = BgL_begz00_1289;
														}
													else
														{
															obj_t BgL_auxz00_2834;

															BgL_auxz00_2834 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(7012L),
																BGl_string1929z00zz__regexpz00,
																BGl_string1930z00zz__regexpz00,
																BgL_begz00_1289);
															FAILURE(BgL_auxz00_2834, BFALSE, BFALSE);
														}
													BgL_auxz00_2829 = CINT(BgL_tmpz00_2831);
												}
												if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1282))
													{	/* Unsafe/regex.scm 180 */
														BgL_tmpz00_2822 = BgL_patz00_1282;
													}
												else
													{
														obj_t BgL_auxz00_2825;

														BgL_auxz00_2825 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1917z00zz__regexpz00, BINT(6996L),
															BGl_string1929z00zz__regexpz00,
															BGl_string1919z00zz__regexpz00, BgL_patz00_1282);
														FAILURE(BgL_auxz00_2825, BFALSE, BFALSE);
													}
												return
													bgl_regmatch(BgL_tmpz00_2822,
													BSTRING_TO_STRING(BgL_strz00_2038), ((bool_t) 0),
													BgL_auxz00_2829, (int) (BgL_endz00_1290), (int) (0L));
											}
										else
											{	/* Unsafe/regex.scm 181 */
												obj_t BgL_rxz00_2040;

												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_arg1331z00_2041;

													BgL_arg1331z00_2041 =
														BGl_pregexpzd2normaliza7ez75zz__regexpz00
														(BgL_patz00_1282);
													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_tmpz00_2843;

														if (STRINGP(BgL_arg1331z00_2041))
															{	/* Unsafe/regex.scm 181 */
																BgL_tmpz00_2843 = BgL_arg1331z00_2041;
															}
														else
															{
																obj_t BgL_auxz00_2846;

																BgL_auxz00_2846 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7080L),
																	BGl_string1929z00zz__regexpz00,
																	BGl_string1928z00zz__regexpz00,
																	BgL_arg1331z00_2041);
																FAILURE(BgL_auxz00_2846, BFALSE, BFALSE);
															}
														BgL_rxz00_2040 =
															bgl_regcomp(BgL_tmpz00_2843, BNIL, ((bool_t) 0));
													}
												}
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_valz00_2042;

													{	/* Unsafe/regex.scm 182 */
														int BgL_tmpz00_2851;

														{	/* Unsafe/regex.scm 182 */
															obj_t BgL_tmpz00_2853;

															if (INTEGERP(BgL_begz00_1289))
																{	/* Unsafe/regex.scm 182 */
																	BgL_tmpz00_2853 = BgL_begz00_1289;
																}
															else
																{
																	obj_t BgL_auxz00_2856;

																	BgL_auxz00_2856 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1917z00zz__regexpz00,
																		BINT(7129L), BGl_string1929z00zz__regexpz00,
																		BGl_string1930z00zz__regexpz00,
																		BgL_begz00_1289);
																	FAILURE(BgL_auxz00_2856, BFALSE, BFALSE);
																}
															BgL_tmpz00_2851 = CINT(BgL_tmpz00_2853);
														}
														BgL_valz00_2042 =
															bgl_regmatch(BgL_rxz00_2040,
															BSTRING_TO_STRING(BgL_strz00_2038), ((bool_t) 0),
															BgL_tmpz00_2851, (int) (BgL_endz00_1290),
															(int) (0L));
													}
													{	/* Unsafe/regex.scm 182 */

														bgl_regfree(BgL_rxz00_2040);
														return BgL_valz00_2042;
													}
												}
											}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Unsafe/regex.scm 189 */
							obj_t BgL_begz00_1292;

							BgL_begz00_1292 = VECTOR_REF(BgL_opt1123z00_22, 2L);
							{	/* Unsafe/regex.scm 189 */
								obj_t BgL_endz00_1293;

								BgL_endz00_1293 = VECTOR_REF(BgL_opt1123z00_22, 3L);
								{	/* Unsafe/regex.scm 189 */

									{	/* Unsafe/regex.scm 189 */
										obj_t BgL_strz00_2043;

										if (STRINGP(BgL_strz00_1283))
											{	/* Unsafe/regex.scm 189 */
												BgL_strz00_2043 = BgL_strz00_1283;
											}
										else
											{
												obj_t BgL_auxz00_2869;

												BgL_auxz00_2869 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(7399L),
													BGl_string1929z00zz__regexpz00,
													BGl_string1928z00zz__regexpz00, BgL_strz00_1283);
												FAILURE(BgL_auxz00_2869, BFALSE, BFALSE);
											}
										if (BGL_REGEXPP(BgL_patz00_1282))
											{	/* Unsafe/regex.scm 180 */
												int BgL_auxz00_2892;
												int BgL_auxz00_2882;
												obj_t BgL_tmpz00_2875;

												{	/* Unsafe/regex.scm 180 */
													obj_t BgL_tmpz00_2893;

													if (INTEGERP(BgL_endz00_1293))
														{	/* Unsafe/regex.scm 180 */
															BgL_tmpz00_2893 = BgL_endz00_1293;
														}
													else
														{
															obj_t BgL_auxz00_2896;

															BgL_auxz00_2896 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(7016L),
																BGl_string1929z00zz__regexpz00,
																BGl_string1930z00zz__regexpz00,
																BgL_endz00_1293);
															FAILURE(BgL_auxz00_2896, BFALSE, BFALSE);
														}
													BgL_auxz00_2892 = CINT(BgL_tmpz00_2893);
												}
												{	/* Unsafe/regex.scm 180 */
													obj_t BgL_tmpz00_2884;

													if (INTEGERP(BgL_begz00_1292))
														{	/* Unsafe/regex.scm 180 */
															BgL_tmpz00_2884 = BgL_begz00_1292;
														}
													else
														{
															obj_t BgL_auxz00_2887;

															BgL_auxz00_2887 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(7012L),
																BGl_string1929z00zz__regexpz00,
																BGl_string1930z00zz__regexpz00,
																BgL_begz00_1292);
															FAILURE(BgL_auxz00_2887, BFALSE, BFALSE);
														}
													BgL_auxz00_2882 = CINT(BgL_tmpz00_2884);
												}
												if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1282))
													{	/* Unsafe/regex.scm 180 */
														BgL_tmpz00_2875 = BgL_patz00_1282;
													}
												else
													{
														obj_t BgL_auxz00_2878;

														BgL_auxz00_2878 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1917z00zz__regexpz00, BINT(6996L),
															BGl_string1929z00zz__regexpz00,
															BGl_string1919z00zz__regexpz00, BgL_patz00_1282);
														FAILURE(BgL_auxz00_2878, BFALSE, BFALSE);
													}
												return
													bgl_regmatch(BgL_tmpz00_2875,
													BSTRING_TO_STRING(BgL_strz00_2043), ((bool_t) 0),
													BgL_auxz00_2882, BgL_auxz00_2892, (int) (0L));
											}
										else
											{	/* Unsafe/regex.scm 181 */
												obj_t BgL_rxz00_2045;

												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_arg1331z00_2046;

													BgL_arg1331z00_2046 =
														BGl_pregexpzd2normaliza7ez75zz__regexpz00
														(BgL_patz00_1282);
													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_tmpz00_2904;

														if (STRINGP(BgL_arg1331z00_2046))
															{	/* Unsafe/regex.scm 181 */
																BgL_tmpz00_2904 = BgL_arg1331z00_2046;
															}
														else
															{
																obj_t BgL_auxz00_2907;

																BgL_auxz00_2907 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7080L),
																	BGl_string1929z00zz__regexpz00,
																	BGl_string1928z00zz__regexpz00,
																	BgL_arg1331z00_2046);
																FAILURE(BgL_auxz00_2907, BFALSE, BFALSE);
															}
														BgL_rxz00_2045 =
															bgl_regcomp(BgL_tmpz00_2904, BNIL, ((bool_t) 0));
													}
												}
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_valz00_2047;

													{	/* Unsafe/regex.scm 182 */
														int BgL_auxz00_2922;
														int BgL_tmpz00_2912;

														{	/* Unsafe/regex.scm 182 */
															obj_t BgL_tmpz00_2923;

															if (INTEGERP(BgL_endz00_1293))
																{	/* Unsafe/regex.scm 182 */
																	BgL_tmpz00_2923 = BgL_endz00_1293;
																}
															else
																{
																	obj_t BgL_auxz00_2926;

																	BgL_auxz00_2926 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1917z00zz__regexpz00,
																		BINT(7133L), BGl_string1929z00zz__regexpz00,
																		BGl_string1930z00zz__regexpz00,
																		BgL_endz00_1293);
																	FAILURE(BgL_auxz00_2926, BFALSE, BFALSE);
																}
															BgL_auxz00_2922 = CINT(BgL_tmpz00_2923);
														}
														{	/* Unsafe/regex.scm 182 */
															obj_t BgL_tmpz00_2914;

															if (INTEGERP(BgL_begz00_1292))
																{	/* Unsafe/regex.scm 182 */
																	BgL_tmpz00_2914 = BgL_begz00_1292;
																}
															else
																{
																	obj_t BgL_auxz00_2917;

																	BgL_auxz00_2917 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1917z00zz__regexpz00,
																		BINT(7129L), BGl_string1929z00zz__regexpz00,
																		BGl_string1930z00zz__regexpz00,
																		BgL_begz00_1292);
																	FAILURE(BgL_auxz00_2917, BFALSE, BFALSE);
																}
															BgL_tmpz00_2912 = CINT(BgL_tmpz00_2914);
														}
														BgL_valz00_2047 =
															bgl_regmatch(BgL_rxz00_2045,
															BSTRING_TO_STRING(BgL_strz00_2043), ((bool_t) 0),
															BgL_tmpz00_2912, BgL_auxz00_2922, (int) (0L));
													}
													{	/* Unsafe/regex.scm 182 */

														bgl_regfree(BgL_rxz00_2045);
														return BgL_valz00_2047;
													}
												}
											}
									}
								}
							}
						}
						break;
					case 5L:

						{	/* Unsafe/regex.scm 189 */
							obj_t BgL_begz00_1295;

							BgL_begz00_1295 = VECTOR_REF(BgL_opt1123z00_22, 2L);
							{	/* Unsafe/regex.scm 189 */
								obj_t BgL_endz00_1296;

								BgL_endz00_1296 = VECTOR_REF(BgL_opt1123z00_22, 3L);
								{	/* Unsafe/regex.scm 189 */
									obj_t BgL_offsetz00_1297;

									BgL_offsetz00_1297 = VECTOR_REF(BgL_opt1123z00_22, 4L);
									{	/* Unsafe/regex.scm 189 */

										{	/* Unsafe/regex.scm 189 */
											obj_t BgL_strz00_2048;

											if (STRINGP(BgL_strz00_1283))
												{	/* Unsafe/regex.scm 189 */
													BgL_strz00_2048 = BgL_strz00_1283;
												}
											else
												{
													obj_t BgL_auxz00_2939;

													BgL_auxz00_2939 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1917z00zz__regexpz00, BINT(7399L),
														BGl_string1929z00zz__regexpz00,
														BGl_string1928z00zz__regexpz00, BgL_strz00_1283);
													FAILURE(BgL_auxz00_2939, BFALSE, BFALSE);
												}
											if (BGL_REGEXPP(BgL_patz00_1282))
												{	/* Unsafe/regex.scm 180 */
													int BgL_auxz00_2971;
													int BgL_auxz00_2962;
													int BgL_auxz00_2952;
													obj_t BgL_tmpz00_2945;

													{	/* Unsafe/regex.scm 180 */
														obj_t BgL_tmpz00_2972;

														if (INTEGERP(BgL_offsetz00_1297))
															{	/* Unsafe/regex.scm 180 */
																BgL_tmpz00_2972 = BgL_offsetz00_1297;
															}
														else
															{
																obj_t BgL_auxz00_2975;

																BgL_auxz00_2975 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7020L),
																	BGl_string1929z00zz__regexpz00,
																	BGl_string1930z00zz__regexpz00,
																	BgL_offsetz00_1297);
																FAILURE(BgL_auxz00_2975, BFALSE, BFALSE);
															}
														BgL_auxz00_2971 = CINT(BgL_tmpz00_2972);
													}
													{	/* Unsafe/regex.scm 180 */
														obj_t BgL_tmpz00_2963;

														if (INTEGERP(BgL_endz00_1296))
															{	/* Unsafe/regex.scm 180 */
																BgL_tmpz00_2963 = BgL_endz00_1296;
															}
														else
															{
																obj_t BgL_auxz00_2966;

																BgL_auxz00_2966 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7016L),
																	BGl_string1929z00zz__regexpz00,
																	BGl_string1930z00zz__regexpz00,
																	BgL_endz00_1296);
																FAILURE(BgL_auxz00_2966, BFALSE, BFALSE);
															}
														BgL_auxz00_2962 = CINT(BgL_tmpz00_2963);
													}
													{	/* Unsafe/regex.scm 180 */
														obj_t BgL_tmpz00_2954;

														if (INTEGERP(BgL_begz00_1295))
															{	/* Unsafe/regex.scm 180 */
																BgL_tmpz00_2954 = BgL_begz00_1295;
															}
														else
															{
																obj_t BgL_auxz00_2957;

																BgL_auxz00_2957 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7012L),
																	BGl_string1929z00zz__regexpz00,
																	BGl_string1930z00zz__regexpz00,
																	BgL_begz00_1295);
																FAILURE(BgL_auxz00_2957, BFALSE, BFALSE);
															}
														BgL_auxz00_2952 = CINT(BgL_tmpz00_2954);
													}
													if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1282))
														{	/* Unsafe/regex.scm 180 */
															BgL_tmpz00_2945 = BgL_patz00_1282;
														}
													else
														{
															obj_t BgL_auxz00_2948;

															BgL_auxz00_2948 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(6996L),
																BGl_string1929z00zz__regexpz00,
																BGl_string1919z00zz__regexpz00,
																BgL_patz00_1282);
															FAILURE(BgL_auxz00_2948, BFALSE, BFALSE);
														}
													return
														bgl_regmatch(BgL_tmpz00_2945,
														BSTRING_TO_STRING(BgL_strz00_2048), ((bool_t) 0),
														BgL_auxz00_2952, BgL_auxz00_2962, BgL_auxz00_2971);
												}
											else
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_rxz00_2050;

													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_arg1331z00_2051;

														BgL_arg1331z00_2051 =
															BGl_pregexpzd2normaliza7ez75zz__regexpz00
															(BgL_patz00_1282);
														{	/* Unsafe/regex.scm 181 */
															obj_t BgL_tmpz00_2982;

															if (STRINGP(BgL_arg1331z00_2051))
																{	/* Unsafe/regex.scm 181 */
																	BgL_tmpz00_2982 = BgL_arg1331z00_2051;
																}
															else
																{
																	obj_t BgL_auxz00_2985;

																	BgL_auxz00_2985 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1917z00zz__regexpz00,
																		BINT(7080L), BGl_string1929z00zz__regexpz00,
																		BGl_string1928z00zz__regexpz00,
																		BgL_arg1331z00_2051);
																	FAILURE(BgL_auxz00_2985, BFALSE, BFALSE);
																}
															BgL_rxz00_2050 =
																bgl_regcomp(BgL_tmpz00_2982, BNIL,
																((bool_t) 0));
														}
													}
													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_valz00_2052;

														{	/* Unsafe/regex.scm 182 */
															int BgL_auxz00_3009;
															int BgL_auxz00_3000;
															int BgL_tmpz00_2990;

															{	/* Unsafe/regex.scm 182 */
																obj_t BgL_tmpz00_3010;

																if (INTEGERP(BgL_offsetz00_1297))
																	{	/* Unsafe/regex.scm 182 */
																		BgL_tmpz00_3010 = BgL_offsetz00_1297;
																	}
																else
																	{
																		obj_t BgL_auxz00_3013;

																		BgL_auxz00_3013 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1917z00zz__regexpz00,
																			BINT(7137L),
																			BGl_string1929z00zz__regexpz00,
																			BGl_string1930z00zz__regexpz00,
																			BgL_offsetz00_1297);
																		FAILURE(BgL_auxz00_3013, BFALSE, BFALSE);
																	}
																BgL_auxz00_3009 = CINT(BgL_tmpz00_3010);
															}
															{	/* Unsafe/regex.scm 182 */
																obj_t BgL_tmpz00_3001;

																if (INTEGERP(BgL_endz00_1296))
																	{	/* Unsafe/regex.scm 182 */
																		BgL_tmpz00_3001 = BgL_endz00_1296;
																	}
																else
																	{
																		obj_t BgL_auxz00_3004;

																		BgL_auxz00_3004 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1917z00zz__regexpz00,
																			BINT(7133L),
																			BGl_string1929z00zz__regexpz00,
																			BGl_string1930z00zz__regexpz00,
																			BgL_endz00_1296);
																		FAILURE(BgL_auxz00_3004, BFALSE, BFALSE);
																	}
																BgL_auxz00_3000 = CINT(BgL_tmpz00_3001);
															}
															{	/* Unsafe/regex.scm 182 */
																obj_t BgL_tmpz00_2992;

																if (INTEGERP(BgL_begz00_1295))
																	{	/* Unsafe/regex.scm 182 */
																		BgL_tmpz00_2992 = BgL_begz00_1295;
																	}
																else
																	{
																		obj_t BgL_auxz00_2995;

																		BgL_auxz00_2995 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1917z00zz__regexpz00,
																			BINT(7129L),
																			BGl_string1929z00zz__regexpz00,
																			BGl_string1930z00zz__regexpz00,
																			BgL_begz00_1295);
																		FAILURE(BgL_auxz00_2995, BFALSE, BFALSE);
																	}
																BgL_tmpz00_2990 = CINT(BgL_tmpz00_2992);
															}
															BgL_valz00_2052 =
																bgl_regmatch(BgL_rxz00_2050,
																BSTRING_TO_STRING(BgL_strz00_2048),
																((bool_t) 0), BgL_tmpz00_2990, BgL_auxz00_3000,
																BgL_auxz00_3009);
														}
														{	/* Unsafe/regex.scm 182 */

															bgl_regfree(BgL_rxz00_2050);
															return BgL_valz00_2052;
														}
													}
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match-positions */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2matchzd2positionsz00zz__regexpz00(obj_t
		BgL_patz00_17, obj_t BgL_strz00_18, obj_t BgL_begz00_19,
		obj_t BgL_endz00_20, obj_t BgL_offsetz00_21)
	{
		{	/* Unsafe/regex.scm 189 */
			if (BGL_REGEXPP(BgL_patz00_17))
				{	/* Unsafe/regex.scm 179 */
					return
						bgl_regmatch(BgL_patz00_17,
						BSTRING_TO_STRING(BgL_strz00_18), ((bool_t) 0),
						CINT(BgL_begz00_19), CINT(BgL_endz00_20), CINT(BgL_offsetz00_21));
				}
			else
				{	/* Unsafe/regex.scm 181 */
					obj_t BgL_rxz00_2054;

					BgL_rxz00_2054 =
						bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
						(BgL_patz00_17), BNIL, ((bool_t) 0));
					{	/* Unsafe/regex.scm 181 */
						obj_t BgL_valz00_2056;

						BgL_valz00_2056 =
							bgl_regmatch(BgL_rxz00_2054,
							BSTRING_TO_STRING(BgL_strz00_18), ((bool_t) 0),
							CINT(BgL_begz00_19), CINT(BgL_endz00_20), CINT(BgL_offsetz00_21));
						{	/* Unsafe/regex.scm 182 */

							bgl_regfree(BgL_rxz00_2054);
							return BgL_valz00_2056;
						}
					}
				}
		}

	}



/* _pregexp-match */
	obj_t BGl__pregexpzd2matchzd2zz__regexpz00(obj_t BgL_env1128z00_29,
		obj_t BgL_opt1127z00_28)
	{
		{	/* Unsafe/regex.scm 195 */
			{	/* Unsafe/regex.scm 195 */
				obj_t BgL_patz00_1298;
				obj_t BgL_strz00_1299;

				BgL_patz00_1298 = VECTOR_REF(BgL_opt1127z00_28, 0L);
				BgL_strz00_1299 = VECTOR_REF(BgL_opt1127z00_28, 1L);
				switch (VECTOR_LENGTH(BgL_opt1127z00_28))
					{
					case 2L:

						{	/* Unsafe/regex.scm 195 */
							long BgL_endz00_1303;

							{	/* Unsafe/regex.scm 195 */
								obj_t BgL_stringz00_2057;

								if (STRINGP(BgL_strz00_1299))
									{	/* Unsafe/regex.scm 195 */
										BgL_stringz00_2057 = BgL_strz00_1299;
									}
								else
									{
										obj_t BgL_auxz00_3041;

										BgL_auxz00_3041 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string1917z00zz__regexpz00, BINT(7813L),
											BGl_string1931z00zz__regexpz00,
											BGl_string1928z00zz__regexpz00, BgL_strz00_1299);
										FAILURE(BgL_auxz00_3041, BFALSE, BFALSE);
									}
								BgL_endz00_1303 = STRING_LENGTH(BgL_stringz00_2057);
							}
							{	/* Unsafe/regex.scm 195 */

								{	/* Unsafe/regex.scm 195 */
									obj_t BgL_strz00_2058;

									if (STRINGP(BgL_strz00_1299))
										{	/* Unsafe/regex.scm 195 */
											BgL_strz00_2058 = BgL_strz00_1299;
										}
									else
										{
											obj_t BgL_auxz00_3048;

											BgL_auxz00_3048 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(7758L),
												BGl_string1931z00zz__regexpz00,
												BGl_string1928z00zz__regexpz00, BgL_strz00_1299);
											FAILURE(BgL_auxz00_3048, BFALSE, BFALSE);
										}
									{	/* Unsafe/regex.scm 196 */

										if (BGL_REGEXPP(BgL_patz00_1298))
											{	/* Unsafe/regex.scm 180 */
												obj_t BgL_tmpz00_3054;

												if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1298))
													{	/* Unsafe/regex.scm 196 */
														BgL_tmpz00_3054 = BgL_patz00_1298;
													}
												else
													{
														obj_t BgL_auxz00_3057;

														BgL_auxz00_3057 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string1917z00zz__regexpz00, BINT(7845L),
															BGl_string1931z00zz__regexpz00,
															BGl_string1919z00zz__regexpz00, BgL_patz00_1298);
														FAILURE(BgL_auxz00_3057, BFALSE, BFALSE);
													}
												return
													bgl_regmatch(BgL_tmpz00_3054,
													BSTRING_TO_STRING(BgL_strz00_2058), ((bool_t) 1),
													(int) (0L), (int) (BgL_endz00_1303), (int) (0L));
											}
										else
											{	/* Unsafe/regex.scm 181 */
												obj_t BgL_rxz00_2066;

												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_arg1331z00_2067;

													BgL_arg1331z00_2067 =
														BGl_pregexpzd2normaliza7ez75zz__regexpz00
														(BgL_patz00_1298);
													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_tmpz00_3067;

														if (STRINGP(BgL_arg1331z00_2067))
															{	/* Unsafe/regex.scm 181 */
																BgL_tmpz00_3067 = BgL_arg1331z00_2067;
															}
														else
															{
																obj_t BgL_auxz00_3070;

																BgL_auxz00_3070 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7080L),
																	BGl_string1931z00zz__regexpz00,
																	BGl_string1928z00zz__regexpz00,
																	BgL_arg1331z00_2067);
																FAILURE(BgL_auxz00_3070, BFALSE, BFALSE);
															}
														BgL_rxz00_2066 =
															bgl_regcomp(BgL_tmpz00_3067, BNIL, ((bool_t) 0));
													}
												}
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_valz00_2068;

													BgL_valz00_2068 =
														bgl_regmatch(BgL_rxz00_2066,
														BSTRING_TO_STRING(BgL_strz00_2058), ((bool_t) 1),
														(int) (0L), (int) (BgL_endz00_1303), (int) (0L));
													{	/* Unsafe/regex.scm 182 */

														bgl_regfree(BgL_rxz00_2066);
														return BgL_valz00_2068;
													}
												}
											}
									}
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/regex.scm 195 */
							obj_t BgL_begz00_1304;

							BgL_begz00_1304 = VECTOR_REF(BgL_opt1127z00_28, 2L);
							{	/* Unsafe/regex.scm 195 */
								long BgL_endz00_1305;

								{	/* Unsafe/regex.scm 195 */
									obj_t BgL_stringz00_2069;

									if (STRINGP(BgL_strz00_1299))
										{	/* Unsafe/regex.scm 195 */
											BgL_stringz00_2069 = BgL_strz00_1299;
										}
									else
										{
											obj_t BgL_auxz00_3084;

											BgL_auxz00_3084 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(7813L),
												BGl_string1931z00zz__regexpz00,
												BGl_string1928z00zz__regexpz00, BgL_strz00_1299);
											FAILURE(BgL_auxz00_3084, BFALSE, BFALSE);
										}
									BgL_endz00_1305 = STRING_LENGTH(BgL_stringz00_2069);
								}
								{	/* Unsafe/regex.scm 195 */

									{	/* Unsafe/regex.scm 195 */
										obj_t BgL_strz00_2070;

										if (STRINGP(BgL_strz00_1299))
											{	/* Unsafe/regex.scm 195 */
												BgL_strz00_2070 = BgL_strz00_1299;
											}
										else
											{
												obj_t BgL_auxz00_3091;

												BgL_auxz00_3091 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(7758L),
													BGl_string1931z00zz__regexpz00,
													BGl_string1928z00zz__regexpz00, BgL_strz00_1299);
												FAILURE(BgL_auxz00_3091, BFALSE, BFALSE);
											}
										{	/* Unsafe/regex.scm 196 */

											if (BGL_REGEXPP(BgL_patz00_1298))
												{	/* Unsafe/regex.scm 180 */
													int BgL_auxz00_3104;
													obj_t BgL_tmpz00_3097;

													{	/* Unsafe/regex.scm 196 */
														obj_t BgL_tmpz00_3106;

														if (INTEGERP(BgL_begz00_1304))
															{	/* Unsafe/regex.scm 196 */
																BgL_tmpz00_3106 = BgL_begz00_1304;
															}
														else
															{
																obj_t BgL_auxz00_3109;

																BgL_auxz00_3109 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7856L),
																	BGl_string1931z00zz__regexpz00,
																	BGl_string1930z00zz__regexpz00,
																	BgL_begz00_1304);
																FAILURE(BgL_auxz00_3109, BFALSE, BFALSE);
															}
														BgL_auxz00_3104 = CINT(BgL_tmpz00_3106);
													}
													if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1298))
														{	/* Unsafe/regex.scm 196 */
															BgL_tmpz00_3097 = BgL_patz00_1298;
														}
													else
														{
															obj_t BgL_auxz00_3100;

															BgL_auxz00_3100 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(7845L),
																BGl_string1931z00zz__regexpz00,
																BGl_string1919z00zz__regexpz00,
																BgL_patz00_1298);
															FAILURE(BgL_auxz00_3100, BFALSE, BFALSE);
														}
													return
														bgl_regmatch(BgL_tmpz00_3097,
														BSTRING_TO_STRING(BgL_strz00_2070), ((bool_t) 1),
														BgL_auxz00_3104, (int) (BgL_endz00_1305),
														(int) (0L));
												}
											else
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_rxz00_2078;

													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_arg1331z00_2079;

														BgL_arg1331z00_2079 =
															BGl_pregexpzd2normaliza7ez75zz__regexpz00
															(BgL_patz00_1298);
														{	/* Unsafe/regex.scm 181 */
															obj_t BgL_tmpz00_3118;

															if (STRINGP(BgL_arg1331z00_2079))
																{	/* Unsafe/regex.scm 181 */
																	BgL_tmpz00_3118 = BgL_arg1331z00_2079;
																}
															else
																{
																	obj_t BgL_auxz00_3121;

																	BgL_auxz00_3121 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1917z00zz__regexpz00,
																		BINT(7080L), BGl_string1931z00zz__regexpz00,
																		BGl_string1928z00zz__regexpz00,
																		BgL_arg1331z00_2079);
																	FAILURE(BgL_auxz00_3121, BFALSE, BFALSE);
																}
															BgL_rxz00_2078 =
																bgl_regcomp(BgL_tmpz00_3118, BNIL,
																((bool_t) 0));
														}
													}
													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_valz00_2080;

														{	/* Unsafe/regex.scm 182 */
															int BgL_tmpz00_3126;

															{	/* Unsafe/regex.scm 196 */
																obj_t BgL_tmpz00_3128;

																if (INTEGERP(BgL_begz00_1304))
																	{	/* Unsafe/regex.scm 196 */
																		BgL_tmpz00_3128 = BgL_begz00_1304;
																	}
																else
																	{
																		obj_t BgL_auxz00_3131;

																		BgL_auxz00_3131 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1917z00zz__regexpz00,
																			BINT(7856L),
																			BGl_string1931z00zz__regexpz00,
																			BGl_string1930z00zz__regexpz00,
																			BgL_begz00_1304);
																		FAILURE(BgL_auxz00_3131, BFALSE, BFALSE);
																	}
																BgL_tmpz00_3126 = CINT(BgL_tmpz00_3128);
															}
															BgL_valz00_2080 =
																bgl_regmatch(BgL_rxz00_2078,
																BSTRING_TO_STRING(BgL_strz00_2070),
																((bool_t) 1), BgL_tmpz00_3126,
																(int) (BgL_endz00_1305), (int) (0L));
														}
														{	/* Unsafe/regex.scm 182 */

															bgl_regfree(BgL_rxz00_2078);
															return BgL_valz00_2080;
														}
													}
												}
										}
									}
								}
							}
						}
						break;
					case 4L:

						{	/* Unsafe/regex.scm 195 */
							obj_t BgL_begz00_1306;

							BgL_begz00_1306 = VECTOR_REF(BgL_opt1127z00_28, 2L);
							{	/* Unsafe/regex.scm 195 */
								obj_t BgL_endz00_1307;

								BgL_endz00_1307 = VECTOR_REF(BgL_opt1127z00_28, 3L);
								{	/* Unsafe/regex.scm 195 */

									{	/* Unsafe/regex.scm 195 */
										obj_t BgL_strz00_2081;

										if (STRINGP(BgL_strz00_1299))
											{	/* Unsafe/regex.scm 195 */
												BgL_strz00_2081 = BgL_strz00_1299;
											}
										else
											{
												obj_t BgL_auxz00_3144;

												BgL_auxz00_3144 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(7758L),
													BGl_string1931z00zz__regexpz00,
													BGl_string1928z00zz__regexpz00, BgL_strz00_1299);
												FAILURE(BgL_auxz00_3144, BFALSE, BFALSE);
											}
										{	/* Unsafe/regex.scm 196 */

											if (BGL_REGEXPP(BgL_patz00_1298))
												{	/* Unsafe/regex.scm 180 */
													int BgL_auxz00_3167;
													int BgL_auxz00_3157;
													obj_t BgL_tmpz00_3150;

													{	/* Unsafe/regex.scm 196 */
														obj_t BgL_tmpz00_3168;

														if (INTEGERP(BgL_endz00_1307))
															{	/* Unsafe/regex.scm 196 */
																BgL_tmpz00_3168 = BgL_endz00_1307;
															}
														else
															{
																obj_t BgL_auxz00_3171;

																BgL_auxz00_3171 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7860L),
																	BGl_string1931z00zz__regexpz00,
																	BGl_string1930z00zz__regexpz00,
																	BgL_endz00_1307);
																FAILURE(BgL_auxz00_3171, BFALSE, BFALSE);
															}
														BgL_auxz00_3167 = CINT(BgL_tmpz00_3168);
													}
													{	/* Unsafe/regex.scm 196 */
														obj_t BgL_tmpz00_3159;

														if (INTEGERP(BgL_begz00_1306))
															{	/* Unsafe/regex.scm 196 */
																BgL_tmpz00_3159 = BgL_begz00_1306;
															}
														else
															{
																obj_t BgL_auxz00_3162;

																BgL_auxz00_3162 =
																	BGl_typezd2errorzd2zz__errorz00
																	(BGl_string1917z00zz__regexpz00, BINT(7856L),
																	BGl_string1931z00zz__regexpz00,
																	BGl_string1930z00zz__regexpz00,
																	BgL_begz00_1306);
																FAILURE(BgL_auxz00_3162, BFALSE, BFALSE);
															}
														BgL_auxz00_3157 = CINT(BgL_tmpz00_3159);
													}
													if (BGl_regexpzf3zf3zz__regexpz00(BgL_patz00_1298))
														{	/* Unsafe/regex.scm 196 */
															BgL_tmpz00_3150 = BgL_patz00_1298;
														}
													else
														{
															obj_t BgL_auxz00_3153;

															BgL_auxz00_3153 =
																BGl_typezd2errorzd2zz__errorz00
																(BGl_string1917z00zz__regexpz00, BINT(7845L),
																BGl_string1931z00zz__regexpz00,
																BGl_string1919z00zz__regexpz00,
																BgL_patz00_1298);
															FAILURE(BgL_auxz00_3153, BFALSE, BFALSE);
														}
													return
														bgl_regmatch(BgL_tmpz00_3150,
														BSTRING_TO_STRING(BgL_strz00_2081), ((bool_t) 1),
														BgL_auxz00_3157, BgL_auxz00_3167, (int) (0L));
												}
											else
												{	/* Unsafe/regex.scm 181 */
													obj_t BgL_rxz00_2089;

													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_arg1331z00_2090;

														BgL_arg1331z00_2090 =
															BGl_pregexpzd2normaliza7ez75zz__regexpz00
															(BgL_patz00_1298);
														{	/* Unsafe/regex.scm 181 */
															obj_t BgL_tmpz00_3179;

															if (STRINGP(BgL_arg1331z00_2090))
																{	/* Unsafe/regex.scm 181 */
																	BgL_tmpz00_3179 = BgL_arg1331z00_2090;
																}
															else
																{
																	obj_t BgL_auxz00_3182;

																	BgL_auxz00_3182 =
																		BGl_typezd2errorzd2zz__errorz00
																		(BGl_string1917z00zz__regexpz00,
																		BINT(7080L), BGl_string1931z00zz__regexpz00,
																		BGl_string1928z00zz__regexpz00,
																		BgL_arg1331z00_2090);
																	FAILURE(BgL_auxz00_3182, BFALSE, BFALSE);
																}
															BgL_rxz00_2089 =
																bgl_regcomp(BgL_tmpz00_3179, BNIL,
																((bool_t) 0));
														}
													}
													{	/* Unsafe/regex.scm 181 */
														obj_t BgL_valz00_2091;

														{	/* Unsafe/regex.scm 182 */
															int BgL_auxz00_3197;
															int BgL_tmpz00_3187;

															{	/* Unsafe/regex.scm 196 */
																obj_t BgL_tmpz00_3198;

																if (INTEGERP(BgL_endz00_1307))
																	{	/* Unsafe/regex.scm 196 */
																		BgL_tmpz00_3198 = BgL_endz00_1307;
																	}
																else
																	{
																		obj_t BgL_auxz00_3201;

																		BgL_auxz00_3201 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1917z00zz__regexpz00,
																			BINT(7860L),
																			BGl_string1931z00zz__regexpz00,
																			BGl_string1930z00zz__regexpz00,
																			BgL_endz00_1307);
																		FAILURE(BgL_auxz00_3201, BFALSE, BFALSE);
																	}
																BgL_auxz00_3197 = CINT(BgL_tmpz00_3198);
															}
															{	/* Unsafe/regex.scm 196 */
																obj_t BgL_tmpz00_3189;

																if (INTEGERP(BgL_begz00_1306))
																	{	/* Unsafe/regex.scm 196 */
																		BgL_tmpz00_3189 = BgL_begz00_1306;
																	}
																else
																	{
																		obj_t BgL_auxz00_3192;

																		BgL_auxz00_3192 =
																			BGl_typezd2errorzd2zz__errorz00
																			(BGl_string1917z00zz__regexpz00,
																			BINT(7856L),
																			BGl_string1931z00zz__regexpz00,
																			BGl_string1930z00zz__regexpz00,
																			BgL_begz00_1306);
																		FAILURE(BgL_auxz00_3192, BFALSE, BFALSE);
																	}
																BgL_tmpz00_3187 = CINT(BgL_tmpz00_3189);
															}
															BgL_valz00_2091 =
																bgl_regmatch(BgL_rxz00_2089,
																BSTRING_TO_STRING(BgL_strz00_2081),
																((bool_t) 1), BgL_tmpz00_3187, BgL_auxz00_3197,
																(int) (0L));
														}
														{	/* Unsafe/regex.scm 182 */

															bgl_regfree(BgL_rxz00_2089);
															return BgL_valz00_2091;
														}
													}
												}
										}
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2matchzd2zz__regexpz00(obj_t
		BgL_patz00_24, obj_t BgL_strz00_25, obj_t BgL_begz00_26,
		obj_t BgL_endz00_27)
	{
		{	/* Unsafe/regex.scm 195 */
			{	/* Unsafe/regex.scm 196 */

				if (BGL_REGEXPP(BgL_patz00_24))
					{	/* Unsafe/regex.scm 179 */
						return
							bgl_regmatch(BgL_patz00_24,
							BSTRING_TO_STRING(BgL_strz00_25), ((bool_t) 1),
							CINT(BgL_begz00_26), CINT(BgL_endz00_27), (int) (0L));
					}
				else
					{	/* Unsafe/regex.scm 181 */
						obj_t BgL_rxz00_2099;

						BgL_rxz00_2099 =
							bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
							(BgL_patz00_24), BNIL, ((bool_t) 0));
						{	/* Unsafe/regex.scm 181 */
							obj_t BgL_valz00_2101;

							BgL_valz00_2101 =
								bgl_regmatch(BgL_rxz00_2099,
								BSTRING_TO_STRING(BgL_strz00_25), ((bool_t) 1),
								CINT(BgL_begz00_26), CINT(BgL_endz00_27), (int) (0L));
							{	/* Unsafe/regex.scm 182 */

								bgl_regfree(BgL_rxz00_2099);
								return BgL_valz00_2101;
							}
						}
					}
			}
		}

	}



/* _pregexp-match-n-positions! */
	obj_t BGl__pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t
		BgL_env1132z00_37, obj_t BgL_opt1131z00_36)
	{
		{	/* Unsafe/regex.scm 201 */
			{	/* Unsafe/regex.scm 201 */
				obj_t BgL_g1133z00_1314;
				obj_t BgL_g1134z00_1315;
				obj_t BgL_g1135z00_1316;
				obj_t BgL_g1136z00_1317;
				obj_t BgL_g1137z00_1318;

				BgL_g1133z00_1314 = VECTOR_REF(BgL_opt1131z00_36, 0L);
				BgL_g1134z00_1315 = VECTOR_REF(BgL_opt1131z00_36, 1L);
				BgL_g1135z00_1316 = VECTOR_REF(BgL_opt1131z00_36, 2L);
				BgL_g1136z00_1317 = VECTOR_REF(BgL_opt1131z00_36, 3L);
				BgL_g1137z00_1318 = VECTOR_REF(BgL_opt1131z00_36, 4L);
				switch (VECTOR_LENGTH(BgL_opt1131z00_36))
					{
					case 5L:

						{	/* Unsafe/regex.scm 201 */

							{	/* Unsafe/regex.scm 201 */
								long BgL_tmpz00_3231;

								{	/* Unsafe/regex.scm 201 */
									long BgL_auxz00_3262;
									long BgL_auxz00_3253;
									obj_t BgL_auxz00_3246;
									obj_t BgL_auxz00_3239;
									obj_t BgL_auxz00_3232;

									{	/* Unsafe/regex.scm 201 */
										obj_t BgL_tmpz00_3263;

										if (INTEGERP(BgL_g1137z00_1318))
											{	/* Unsafe/regex.scm 201 */
												BgL_tmpz00_3263 = BgL_g1137z00_1318;
											}
										else
											{
												obj_t BgL_auxz00_3266;

												BgL_auxz00_3266 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(8089L),
													BGl_string1932z00zz__regexpz00,
													BGl_string1930z00zz__regexpz00, BgL_g1137z00_1318);
												FAILURE(BgL_auxz00_3266, BFALSE, BFALSE);
											}
										BgL_auxz00_3262 = (long) CINT(BgL_tmpz00_3263);
									}
									{	/* Unsafe/regex.scm 201 */
										obj_t BgL_tmpz00_3254;

										if (INTEGERP(BgL_g1136z00_1317))
											{	/* Unsafe/regex.scm 201 */
												BgL_tmpz00_3254 = BgL_g1136z00_1317;
											}
										else
											{
												obj_t BgL_auxz00_3257;

												BgL_auxz00_3257 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(8089L),
													BGl_string1932z00zz__regexpz00,
													BGl_string1930z00zz__regexpz00, BgL_g1136z00_1317);
												FAILURE(BgL_auxz00_3257, BFALSE, BFALSE);
											}
										BgL_auxz00_3253 = (long) CINT(BgL_tmpz00_3254);
									}
									if (VECTORP(BgL_g1135z00_1316))
										{	/* Unsafe/regex.scm 201 */
											BgL_auxz00_3246 = BgL_g1135z00_1316;
										}
									else
										{
											obj_t BgL_auxz00_3249;

											BgL_auxz00_3249 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(8089L),
												BGl_string1932z00zz__regexpz00,
												BGl_string1933z00zz__regexpz00, BgL_g1135z00_1316);
											FAILURE(BgL_auxz00_3249, BFALSE, BFALSE);
										}
									if (STRINGP(BgL_g1134z00_1315))
										{	/* Unsafe/regex.scm 201 */
											BgL_auxz00_3239 = BgL_g1134z00_1315;
										}
									else
										{
											obj_t BgL_auxz00_3242;

											BgL_auxz00_3242 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(8089L),
												BGl_string1932z00zz__regexpz00,
												BGl_string1928z00zz__regexpz00, BgL_g1134z00_1315);
											FAILURE(BgL_auxz00_3242, BFALSE, BFALSE);
										}
									if (BGl_regexpzf3zf3zz__regexpz00(BgL_g1133z00_1314))
										{	/* Unsafe/regex.scm 201 */
											BgL_auxz00_3232 = BgL_g1133z00_1314;
										}
									else
										{
											obj_t BgL_auxz00_3235;

											BgL_auxz00_3235 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string1917z00zz__regexpz00, BINT(8089L),
												BGl_string1932z00zz__regexpz00,
												BGl_string1919z00zz__regexpz00, BgL_g1133z00_1314);
											FAILURE(BgL_auxz00_3235, BFALSE, BFALSE);
										}
									BgL_tmpz00_3231 =
										BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00
										(BgL_auxz00_3232, BgL_auxz00_3239, BgL_auxz00_3246,
										BgL_auxz00_3253, BgL_auxz00_3262, BINT(0L));
								}
								return BINT(BgL_tmpz00_3231);
							}
						}
						break;
					case 6L:

						{	/* Unsafe/regex.scm 201 */
							obj_t BgL_offsetz00_1322;

							BgL_offsetz00_1322 = VECTOR_REF(BgL_opt1131z00_36, 5L);
							{	/* Unsafe/regex.scm 201 */

								{	/* Unsafe/regex.scm 201 */
									long BgL_tmpz00_3275;

									{	/* Unsafe/regex.scm 201 */
										long BgL_auxz00_3306;
										long BgL_auxz00_3297;
										obj_t BgL_auxz00_3290;
										obj_t BgL_auxz00_3283;
										obj_t BgL_auxz00_3276;

										{	/* Unsafe/regex.scm 201 */
											obj_t BgL_tmpz00_3307;

											if (INTEGERP(BgL_g1137z00_1318))
												{	/* Unsafe/regex.scm 201 */
													BgL_tmpz00_3307 = BgL_g1137z00_1318;
												}
											else
												{
													obj_t BgL_auxz00_3310;

													BgL_auxz00_3310 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1917z00zz__regexpz00, BINT(8089L),
														BGl_string1932z00zz__regexpz00,
														BGl_string1930z00zz__regexpz00, BgL_g1137z00_1318);
													FAILURE(BgL_auxz00_3310, BFALSE, BFALSE);
												}
											BgL_auxz00_3306 = (long) CINT(BgL_tmpz00_3307);
										}
										{	/* Unsafe/regex.scm 201 */
											obj_t BgL_tmpz00_3298;

											if (INTEGERP(BgL_g1136z00_1317))
												{	/* Unsafe/regex.scm 201 */
													BgL_tmpz00_3298 = BgL_g1136z00_1317;
												}
											else
												{
													obj_t BgL_auxz00_3301;

													BgL_auxz00_3301 =
														BGl_typezd2errorzd2zz__errorz00
														(BGl_string1917z00zz__regexpz00, BINT(8089L),
														BGl_string1932z00zz__regexpz00,
														BGl_string1930z00zz__regexpz00, BgL_g1136z00_1317);
													FAILURE(BgL_auxz00_3301, BFALSE, BFALSE);
												}
											BgL_auxz00_3297 = (long) CINT(BgL_tmpz00_3298);
										}
										if (VECTORP(BgL_g1135z00_1316))
											{	/* Unsafe/regex.scm 201 */
												BgL_auxz00_3290 = BgL_g1135z00_1316;
											}
										else
											{
												obj_t BgL_auxz00_3293;

												BgL_auxz00_3293 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(8089L),
													BGl_string1932z00zz__regexpz00,
													BGl_string1933z00zz__regexpz00, BgL_g1135z00_1316);
												FAILURE(BgL_auxz00_3293, BFALSE, BFALSE);
											}
										if (STRINGP(BgL_g1134z00_1315))
											{	/* Unsafe/regex.scm 201 */
												BgL_auxz00_3283 = BgL_g1134z00_1315;
											}
										else
											{
												obj_t BgL_auxz00_3286;

												BgL_auxz00_3286 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(8089L),
													BGl_string1932z00zz__regexpz00,
													BGl_string1928z00zz__regexpz00, BgL_g1134z00_1315);
												FAILURE(BgL_auxz00_3286, BFALSE, BFALSE);
											}
										if (BGl_regexpzf3zf3zz__regexpz00(BgL_g1133z00_1314))
											{	/* Unsafe/regex.scm 201 */
												BgL_auxz00_3276 = BgL_g1133z00_1314;
											}
										else
											{
												obj_t BgL_auxz00_3279;

												BgL_auxz00_3279 =
													BGl_typezd2errorzd2zz__errorz00
													(BGl_string1917z00zz__regexpz00, BINT(8089L),
													BGl_string1932z00zz__regexpz00,
													BGl_string1919z00zz__regexpz00, BgL_g1133z00_1314);
												FAILURE(BgL_auxz00_3279, BFALSE, BFALSE);
											}
										BgL_tmpz00_3275 =
											BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00
											(BgL_auxz00_3276, BgL_auxz00_3283, BgL_auxz00_3290,
											BgL_auxz00_3297, BgL_auxz00_3306, BgL_offsetz00_1322);
									}
									return BINT(BgL_tmpz00_3275);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* pregexp-match-n-positions! */
	BGL_EXPORTED_DEF long
		BGl_pregexpzd2matchzd2nzd2positionsz12zc0zz__regexpz00(obj_t BgL_patz00_30,
		obj_t BgL_strz00_31, obj_t BgL_vresz00_32, long BgL_begz00_33,
		long BgL_endz00_34, obj_t BgL_offsetz00_35)
	{
		{	/* Unsafe/regex.scm 201 */
			{	/* Unsafe/regex.scm 202 */
				obj_t BgL_posz00_1323;
				long BgL_lenz00_1324;

				if (BGL_REGEXPP(BgL_patz00_30))
					{	/* Unsafe/regex.scm 179 */
						BgL_posz00_1323 =
							bgl_regmatch(BgL_patz00_30,
							BSTRING_TO_STRING(BgL_strz00_31), ((bool_t) 0),
							(int) (BgL_begz00_33),
							(int) (BgL_endz00_34), CINT(BgL_offsetz00_35));
					}
				else
					{	/* Unsafe/regex.scm 181 */
						obj_t BgL_rxz00_2104;

						BgL_rxz00_2104 =
							bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
							(BgL_patz00_30), BNIL, ((bool_t) 0));
						{	/* Unsafe/regex.scm 181 */
							obj_t BgL_valz00_2106;

							BgL_valz00_2106 =
								bgl_regmatch(BgL_rxz00_2104,
								BSTRING_TO_STRING(BgL_strz00_31), ((bool_t) 0),
								(int) (BgL_begz00_33),
								(int) (BgL_endz00_34), CINT(BgL_offsetz00_35));
							{	/* Unsafe/regex.scm 182 */

								bgl_regfree(BgL_rxz00_2104);
								BgL_posz00_1323 = BgL_valz00_2106;
					}}}
				BgL_lenz00_1324 = (VECTOR_LENGTH(BgL_vresz00_32) & ~(1L));
				{
					long BgL_iz00_1326;
					obj_t BgL_posz00_1327;

					BgL_iz00_1326 = 0L;
					BgL_posz00_1327 = BgL_posz00_1323;
				BgL_zc3z04anonymousza31332ze3z87_1328:
					{	/* Unsafe/regex.scm 207 */
						bool_t BgL_test2048z00_3337;

						if ((BgL_iz00_1326 == BgL_lenz00_1324))
							{	/* Unsafe/regex.scm 207 */
								BgL_test2048z00_3337 = ((bool_t) 1);
							}
						else
							{	/* Unsafe/regex.scm 207 */
								BgL_test2048z00_3337 = NULLP(BgL_posz00_1327);
							}
						if (BgL_test2048z00_3337)
							{	/* Unsafe/regex.scm 207 */
								return BgL_iz00_1326;
							}
						else
							{	/* Unsafe/regex.scm 209 */
								bool_t BgL_test2050z00_3341;

								{	/* Unsafe/regex.scm 209 */
									obj_t BgL_tmpz00_3342;

									BgL_tmpz00_3342 = CAR(((obj_t) BgL_posz00_1327));
									BgL_test2050z00_3341 = PAIRP(BgL_tmpz00_3342);
								}
								if (BgL_test2050z00_3341)
									{	/* Unsafe/regex.scm 209 */
										{	/* Unsafe/regex.scm 210 */
											obj_t BgL_arg1337z00_1333;

											{	/* Unsafe/regex.scm 210 */
												obj_t BgL_pairz00_2116;

												BgL_pairz00_2116 = CAR(((obj_t) BgL_posz00_1327));
												BgL_arg1337z00_1333 = CAR(BgL_pairz00_2116);
											}
											VECTOR_SET(BgL_vresz00_32, BgL_iz00_1326,
												BgL_arg1337z00_1333);
										}
										{	/* Unsafe/regex.scm 211 */
											long BgL_arg1338z00_1334;
											obj_t BgL_arg1339z00_1335;

											BgL_arg1338z00_1334 = (BgL_iz00_1326 + 2L);
											{	/* Unsafe/regex.scm 211 */
												obj_t BgL_pairz00_2123;

												BgL_pairz00_2123 = CDR(((obj_t) BgL_posz00_1327));
												BgL_arg1339z00_1335 = CAR(BgL_pairz00_2123);
											}
											VECTOR_SET(BgL_vresz00_32, BgL_arg1338z00_1334,
												BgL_arg1339z00_1335);
										}
										{	/* Unsafe/regex.scm 212 */
											long BgL_arg1340z00_1336;
											obj_t BgL_arg1341z00_1337;

											BgL_arg1340z00_1336 = (BgL_iz00_1326 + 2L);
											BgL_arg1341z00_1337 = CDR(((obj_t) BgL_posz00_1327));
											{
												obj_t BgL_posz00_3359;
												long BgL_iz00_3358;

												BgL_iz00_3358 = BgL_arg1340z00_1336;
												BgL_posz00_3359 = BgL_arg1341z00_1337;
												BgL_posz00_1327 = BgL_posz00_3359;
												BgL_iz00_1326 = BgL_iz00_3358;
												goto BgL_zc3z04anonymousza31332ze3z87_1328;
											}
										}
									}
								else
									{	/* Unsafe/regex.scm 209 */
										VECTOR_SET(BgL_vresz00_32, BgL_iz00_1326, BINT(-1L));
										VECTOR_SET(BgL_vresz00_32, (BgL_iz00_1326 + 2L), BINT(-1L));
										{	/* Unsafe/regex.scm 216 */
											long BgL_arg1343z00_1339;
											obj_t BgL_arg1344z00_1340;

											BgL_arg1343z00_1339 = (BgL_iz00_1326 + 2L);
											BgL_arg1344z00_1340 = CDR(((obj_t) BgL_posz00_1327));
											{
												obj_t BgL_posz00_3369;
												long BgL_iz00_3368;

												BgL_iz00_3368 = BgL_arg1343z00_1339;
												BgL_posz00_3369 = BgL_arg1344z00_1340;
												BgL_posz00_1327 = BgL_posz00_3369;
												BgL_iz00_1326 = BgL_iz00_3368;
												goto BgL_zc3z04anonymousza31332ze3z87_1328;
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* pregexp-read-escaped-number */
	obj_t BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00(obj_t BgL_sz00_38,
		obj_t BgL_iz00_39, long BgL_nz00_40)
	{
		{	/* Unsafe/regex.scm 221 */
			if ((((long) CINT(BgL_iz00_39) + 1L) < BgL_nz00_40))
				{	/* Unsafe/regex.scm 224 */
					unsigned char BgL_cz00_1347;

					BgL_cz00_1347 =
						STRING_REF(BgL_sz00_38, ((long) CINT(BgL_iz00_39) + 1L));
					if (isdigit(BgL_cz00_1347))
						{	/* Unsafe/regex.scm 226 */
							long BgL_g1052z00_1349;
							obj_t BgL_g1053z00_1350;

							BgL_g1052z00_1349 = ((long) CINT(BgL_iz00_39) + 2L);
							{	/* Unsafe/regex.scm 226 */
								obj_t BgL_list1367z00_1375;

								BgL_list1367z00_1375 =
									MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1347), BNIL);
								BgL_g1053z00_1350 = BgL_list1367z00_1375;
							}
							{
								long BgL_iz00_1352;
								obj_t BgL_rz00_1353;

								BgL_iz00_1352 = BgL_g1052z00_1349;
								BgL_rz00_1353 = BgL_g1053z00_1350;
							BgL_zc3z04anonymousza31349ze3z87_1354:
								if ((BgL_iz00_1352 >= BgL_nz00_40))
									{	/* Unsafe/regex.scm 228 */
										obj_t BgL_arg1351z00_1356;

										{	/* Unsafe/regex.scm 228 */
											obj_t BgL_arg1356z00_1359;

											BgL_arg1356z00_1359 =
												BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
												(bgl_reverse_bang(BgL_rz00_1353));
											{	/* Ieee/number.scm 175 */

												BgL_arg1351z00_1356 =
													BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
													(BgL_arg1356z00_1359, BINT(10L));
											}
										}
										{	/* Unsafe/regex.scm 228 */
											obj_t BgL_list1352z00_1357;

											{	/* Unsafe/regex.scm 228 */
												obj_t BgL_arg1354z00_1358;

												BgL_arg1354z00_1358 =
													MAKE_YOUNG_PAIR(BINT(BgL_iz00_1352), BNIL);
												BgL_list1352z00_1357 =
													MAKE_YOUNG_PAIR(BgL_arg1351z00_1356,
													BgL_arg1354z00_1358);
											}
											return BgL_list1352z00_1357;
										}
									}
								else
									{	/* Unsafe/regex.scm 229 */
										unsigned char BgL_cz00_1363;

										BgL_cz00_1363 = STRING_REF(BgL_sz00_38, BgL_iz00_1352);
										if (isdigit(BgL_cz00_1363))
											{	/* Unsafe/regex.scm 231 */
												long BgL_arg1360z00_1365;
												obj_t BgL_arg1361z00_1366;

												BgL_arg1360z00_1365 = (BgL_iz00_1352 + 1L);
												BgL_arg1361z00_1366 =
													MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1363), BgL_rz00_1353);
												{
													obj_t BgL_rz00_3399;
													long BgL_iz00_3398;

													BgL_iz00_3398 = BgL_arg1360z00_1365;
													BgL_rz00_3399 = BgL_arg1361z00_1366;
													BgL_rz00_1353 = BgL_rz00_3399;
													BgL_iz00_1352 = BgL_iz00_3398;
													goto BgL_zc3z04anonymousza31349ze3z87_1354;
												}
											}
										else
											{	/* Unsafe/regex.scm 232 */
												obj_t BgL_arg1362z00_1367;

												{	/* Unsafe/regex.scm 232 */
													obj_t BgL_arg1365z00_1370;

													BgL_arg1365z00_1370 =
														BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
														(bgl_reverse_bang(BgL_rz00_1353));
													{	/* Ieee/number.scm 175 */

														BgL_arg1362z00_1367 =
															BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
															(BgL_arg1365z00_1370, BINT(10L));
													}
												}
												{	/* Unsafe/regex.scm 232 */
													obj_t BgL_list1363z00_1368;

													{	/* Unsafe/regex.scm 232 */
														obj_t BgL_arg1364z00_1369;

														BgL_arg1364z00_1369 =
															MAKE_YOUNG_PAIR(BINT(BgL_iz00_1352), BNIL);
														BgL_list1363z00_1368 =
															MAKE_YOUNG_PAIR(BgL_arg1362z00_1367,
															BgL_arg1364z00_1369);
													}
													return BgL_list1363z00_1368;
												}
											}
									}
							}
						}
					else
						{	/* Unsafe/regex.scm 225 */
							return BFALSE;
						}
				}
			else
				{	/* Unsafe/regex.scm 223 */
					return BFALSE;
				}
		}

	}



/* pregexp-list-ref */
	obj_t BGl_pregexpzd2listzd2refz00zz__regexpz00(obj_t BgL_sz00_41,
		obj_t BgL_iz00_42)
	{
		{	/* Unsafe/regex.scm 238 */
			{
				obj_t BgL_sz00_2171;
				long BgL_kz00_2172;

				BgL_sz00_2171 = BgL_sz00_41;
				BgL_kz00_2172 = 0L;
			BgL_loopz00_2170:
				if (NULLP(BgL_sz00_2171))
					{	/* Unsafe/regex.scm 242 */
						return BFALSE;
					}
				else
					{	/* Unsafe/regex.scm 242 */
						if ((BgL_kz00_2172 == (long) CINT(BgL_iz00_42)))
							{	/* Unsafe/regex.scm 243 */
								return CAR(((obj_t) BgL_sz00_2171));
							}
						else
							{
								long BgL_kz00_3417;
								obj_t BgL_sz00_3414;

								BgL_sz00_3414 = CDR(((obj_t) BgL_sz00_2171));
								BgL_kz00_3417 = (BgL_kz00_2172 + 1L);
								BgL_kz00_2172 = BgL_kz00_3417;
								BgL_sz00_2171 = BgL_sz00_3414;
								goto BgL_loopz00_2170;
							}
					}
			}
		}

	}



/* pregexp-replace-aux */
	obj_t BGl_pregexpzd2replacezd2auxz00zz__regexpz00(obj_t BgL_strz00_43,
		obj_t BgL_insz00_44, long BgL_nz00_45, obj_t BgL_backrefsz00_46)
	{
		{	/* Unsafe/regex.scm 249 */
			{
				obj_t BgL_iz00_1388;
				obj_t BgL_rz00_1389;

				BgL_iz00_1388 = BINT(0L);
				BgL_rz00_1389 = BGl_string1934z00zz__regexpz00;
			BgL_zc3z04anonymousza31376ze3z87_1390:
				if (((long) CINT(BgL_iz00_1388) >= BgL_nz00_45))
					{	/* Unsafe/regex.scm 251 */
						return BgL_rz00_1389;
					}
				else
					{	/* Unsafe/regex.scm 252 */
						unsigned char BgL_cz00_1392;

						BgL_cz00_1392 =
							STRING_REF(BgL_insz00_44, (long) CINT(BgL_iz00_1388));
						if ((BgL_cz00_1392 == ((unsigned char) '\\')))
							{	/* Unsafe/regex.scm 254 */
								obj_t BgL_brzd2izd2_1394;

								BgL_brzd2izd2_1394 =
									BGl_pregexpzd2readzd2escapedzd2numberzd2zz__regexpz00
									(BgL_insz00_44, BgL_iz00_1388, BgL_nz00_45);
								{	/* Unsafe/regex.scm 254 */
									obj_t BgL_brz00_1395;

									if (CBOOL(BgL_brzd2izd2_1394))
										{	/* Unsafe/regex.scm 255 */
											BgL_brz00_1395 = CAR(((obj_t) BgL_brzd2izd2_1394));
										}
									else
										{	/* Unsafe/regex.scm 255 */
											if (
												(STRING_REF(BgL_insz00_44,
														((long) CINT(BgL_iz00_1388) + 1L)) ==
													((unsigned char) '&')))
												{	/* Unsafe/regex.scm 256 */
													BgL_brz00_1395 = BINT(0L);
												}
											else
												{	/* Unsafe/regex.scm 256 */
													BgL_brz00_1395 = BFALSE;
												}
										}
									{	/* Unsafe/regex.scm 255 */
										obj_t BgL_iz00_1396;

										if (CBOOL(BgL_brzd2izd2_1394))
											{	/* Unsafe/regex.scm 258 */
												obj_t BgL_pairz00_2201;

												BgL_pairz00_2201 = CDR(((obj_t) BgL_brzd2izd2_1394));
												BgL_iz00_1396 = CAR(BgL_pairz00_2201);
											}
										else
											{	/* Unsafe/regex.scm 258 */
												if (CBOOL(BgL_brz00_1395))
													{	/* Unsafe/regex.scm 259 */
														BgL_iz00_1396 = ADDFX(BgL_iz00_1388, BINT(2L));
													}
												else
													{	/* Unsafe/regex.scm 259 */
														BgL_iz00_1396 = ADDFX(BgL_iz00_1388, BINT(1L));
													}
											}
										{	/* Unsafe/regex.scm 258 */

											if (CBOOL(BgL_brz00_1395))
												{	/* Unsafe/regex.scm 267 */
													obj_t BgL_arg1379z00_1397;

													{	/* Unsafe/regex.scm 267 */
														obj_t BgL_backrefz00_1398;

														BgL_backrefz00_1398 =
															BGl_pregexpzd2listzd2refz00zz__regexpz00
															(BgL_backrefsz00_46, BgL_brz00_1395);
														if (CBOOL(BgL_backrefz00_1398))
															{	/* Unsafe/regex.scm 270 */
																obj_t BgL_arg1380z00_1399;

																{	/* Unsafe/regex.scm 270 */
																	obj_t BgL_arg1382z00_1400;
																	obj_t BgL_arg1383z00_1401;

																	BgL_arg1382z00_1400 =
																		CAR(((obj_t) BgL_backrefz00_1398));
																	BgL_arg1383z00_1401 =
																		CDR(((obj_t) BgL_backrefz00_1398));
																	BgL_arg1380z00_1399 =
																		c_substring(BgL_strz00_43,
																		(long) CINT(BgL_arg1382z00_1400),
																		(long) CINT(BgL_arg1383z00_1401));
																}
																BgL_arg1379z00_1397 =
																	string_append(BgL_rz00_1389,
																	BgL_arg1380z00_1399);
															}
														else
															{	/* Unsafe/regex.scm 268 */
																BgL_arg1379z00_1397 = BgL_rz00_1389;
															}
													}
													{
														obj_t BgL_rz00_3462;
														obj_t BgL_iz00_3461;

														BgL_iz00_3461 = BgL_iz00_1396;
														BgL_rz00_3462 = BgL_arg1379z00_1397;
														BgL_rz00_1389 = BgL_rz00_3462;
														BgL_iz00_1388 = BgL_iz00_3461;
														goto BgL_zc3z04anonymousza31376ze3z87_1390;
													}
												}
											else
												{	/* Unsafe/regex.scm 262 */
													unsigned char BgL_c2z00_1402;

													BgL_c2z00_1402 =
														STRING_REF(BgL_insz00_44,
														(long) CINT(BgL_iz00_1396));
													{	/* Unsafe/regex.scm 263 */
														long BgL_arg1384z00_1403;
														obj_t BgL_arg1387z00_1404;

														BgL_arg1384z00_1403 =
															((long) CINT(BgL_iz00_1396) + 1L);
														if ((BgL_c2z00_1402 == ((unsigned char) '$')))
															{	/* Unsafe/regex.scm 264 */
																BgL_arg1387z00_1404 = BgL_rz00_1389;
															}
														else
															{	/* Unsafe/regex.scm 265 */
																obj_t BgL_arg1389z00_1406;

																{	/* Unsafe/regex.scm 265 */
																	obj_t BgL_list1390z00_1407;

																	BgL_list1390z00_1407 =
																		MAKE_YOUNG_PAIR(BCHAR(BgL_c2z00_1402),
																		BNIL);
																	BgL_arg1389z00_1406 =
																		BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
																		(BgL_list1390z00_1407);
																}
																BgL_arg1387z00_1404 =
																	string_append(BgL_rz00_1389,
																	BgL_arg1389z00_1406);
															}
														{
															obj_t BgL_rz00_3475;
															obj_t BgL_iz00_3473;

															BgL_iz00_3473 = BINT(BgL_arg1384z00_1403);
															BgL_rz00_3475 = BgL_arg1387z00_1404;
															BgL_rz00_1389 = BgL_rz00_3475;
															BgL_iz00_1388 = BgL_iz00_3473;
															goto BgL_zc3z04anonymousza31376ze3z87_1390;
														}
													}
												}
										}
									}
								}
							}
						else
							{	/* Unsafe/regex.scm 272 */
								long BgL_arg1396z00_1413;
								obj_t BgL_arg1397z00_1414;

								BgL_arg1396z00_1413 = ((long) CINT(BgL_iz00_1388) + 1L);
								{	/* Unsafe/regex.scm 272 */
									obj_t BgL_arg1399z00_1415;

									{	/* Unsafe/regex.scm 272 */
										obj_t BgL_list1400z00_1416;

										BgL_list1400z00_1416 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1392), BNIL);
										BgL_arg1399z00_1415 =
											BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
											(BgL_list1400z00_1416);
									}
									BgL_arg1397z00_1414 =
										string_append(BgL_rz00_1389, BgL_arg1399z00_1415);
								}
								{
									obj_t BgL_rz00_3484;
									obj_t BgL_iz00_3482;

									BgL_iz00_3482 = BINT(BgL_arg1396z00_1413);
									BgL_rz00_3484 = BgL_arg1397z00_1414;
									BgL_rz00_1389 = BgL_rz00_3484;
									BgL_iz00_1388 = BgL_iz00_3482;
									goto BgL_zc3z04anonymousza31376ze3z87_1390;
								}
							}
					}
			}
		}

	}



/* pregexp-split */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2splitzd2zz__regexpz00(obj_t
		BgL_patz00_47, obj_t BgL_strz00_48)
	{
		{	/* Unsafe/regex.scm 277 */
			{	/* Unsafe/regex.scm 279 */
				long BgL_nz00_1418;

				BgL_nz00_1418 = STRING_LENGTH(BgL_strz00_48);
				{
					obj_t BgL_iz00_1421;
					obj_t BgL_rz00_1422;
					bool_t BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423;

					BgL_iz00_1421 = BINT(0L);
					BgL_rz00_1422 = BNIL;
					BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423 = ((bool_t) 0);
				BgL_zc3z04anonymousza31401ze3z87_1424:
					if (((long) CINT(BgL_iz00_1421) >= BgL_nz00_1418))
						{	/* Unsafe/regex.scm 281 */
							return bgl_reverse_bang(BgL_rz00_1422);
						}
					else
						{	/* Unsafe/regex.scm 282 */
							obj_t BgL_g1055z00_1426;

							{	/* Unsafe/regex.scm 67 */

								if (BGL_REGEXPP(BgL_patz00_47))
									{	/* Unsafe/regex.scm 179 */
										BgL_g1055z00_1426 =
											bgl_regmatch(BgL_patz00_47,
											BSTRING_TO_STRING(BgL_strz00_48), ((bool_t) 0),
											CINT(BgL_iz00_1421), (int) (BgL_nz00_1418), (int) (0L));
									}
								else
									{	/* Unsafe/regex.scm 181 */
										obj_t BgL_rxz00_2220;

										BgL_rxz00_2220 =
											bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
											(BgL_patz00_47), BNIL, ((bool_t) 0));
										{	/* Unsafe/regex.scm 181 */
											obj_t BgL_valz00_2222;

											BgL_valz00_2222 =
												bgl_regmatch(BgL_rxz00_2220,
												BSTRING_TO_STRING(BgL_strz00_48), ((bool_t) 0),
												CINT(BgL_iz00_1421), (int) (BgL_nz00_1418), (int) (0L));
											{	/* Unsafe/regex.scm 182 */

												bgl_regfree(BgL_rxz00_2220);
												BgL_g1055z00_1426 = BgL_valz00_2222;
							}}}}
							if (CBOOL(BgL_g1055z00_1426))
								{	/* Unsafe/regex.scm 285 */
									obj_t BgL_jkz00_1429;

									BgL_jkz00_1429 = CAR(((obj_t) BgL_g1055z00_1426));
									{	/* Unsafe/regex.scm 286 */
										obj_t BgL_jz00_1430;
										obj_t BgL_kz00_1431;

										BgL_jz00_1430 = CAR(((obj_t) BgL_jkz00_1429));
										BgL_kz00_1431 = CDR(((obj_t) BgL_jkz00_1429));
										if (
											((long) CINT(BgL_jz00_1430) ==
												(long) CINT(BgL_kz00_1431)))
											{	/* Unsafe/regex.scm 290 */
												long BgL_arg1404z00_1433;
												obj_t BgL_arg1405z00_1434;

												BgL_arg1404z00_1433 = ((long) CINT(BgL_kz00_1431) + 1L);
												BgL_arg1405z00_1434 =
													MAKE_YOUNG_PAIR(c_substring(BgL_strz00_48,
														(long) CINT(BgL_iz00_1421),
														((long) CINT(BgL_jz00_1430) + 1L)), BgL_rz00_1422);
												{
													bool_t
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3528;
													obj_t BgL_rz00_3527;
													obj_t BgL_iz00_3525;

													BgL_iz00_3525 = BINT(BgL_arg1404z00_1433);
													BgL_rz00_3527 = BgL_arg1405z00_1434;
													BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3528
														= ((bool_t) 1);
													BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423
														=
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3528;
													BgL_rz00_1422 = BgL_rz00_3527;
													BgL_iz00_1421 = BgL_iz00_3525;
													goto BgL_zc3z04anonymousza31401ze3z87_1424;
												}
											}
										else
											{	/* Unsafe/regex.scm 292 */
												bool_t BgL_test2070z00_3529;

												if (
													((long) CINT(BgL_jz00_1430) ==
														(long) CINT(BgL_iz00_1421)))
													{	/* Unsafe/regex.scm 292 */
														BgL_test2070z00_3529 =
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423;
													}
												else
													{	/* Unsafe/regex.scm 292 */
														BgL_test2070z00_3529 = ((bool_t) 0);
													}
												if (BgL_test2070z00_3529)
													{
														bool_t
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3535;
														obj_t BgL_iz00_3534;

														BgL_iz00_3534 = BgL_kz00_1431;
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3535
															= ((bool_t) 0);
														BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423
															=
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3535;
														BgL_iz00_1421 = BgL_iz00_3534;
														goto BgL_zc3z04anonymousza31401ze3z87_1424;
													}
												else
													{	/* Unsafe/regex.scm 296 */
														obj_t BgL_arg1410z00_1439;

														BgL_arg1410z00_1439 =
															MAKE_YOUNG_PAIR(c_substring(BgL_strz00_48,
																(long) CINT(BgL_iz00_1421),
																(long) CINT(BgL_jz00_1430)), BgL_rz00_1422);
														{
															bool_t
																BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3542;
															obj_t BgL_rz00_3541;
															obj_t BgL_iz00_3540;

															BgL_iz00_3540 = BgL_kz00_1431;
															BgL_rz00_3541 = BgL_arg1410z00_1439;
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3542
																= ((bool_t) 0);
															BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423
																=
																BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3542;
															BgL_rz00_1422 = BgL_rz00_3541;
															BgL_iz00_1421 = BgL_iz00_3540;
															goto BgL_zc3z04anonymousza31401ze3z87_1424;
														}
													}
											}
									}
								}
							else
								{	/* Unsafe/regex.scm 297 */
									obj_t BgL_arg1412z00_1442;

									BgL_arg1412z00_1442 =
										MAKE_YOUNG_PAIR(c_substring(BgL_strz00_48,
											(long) CINT(BgL_iz00_1421), BgL_nz00_1418),
										BgL_rz00_1422);
									{
										bool_t
											BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3549;
										obj_t BgL_rz00_3548;
										obj_t BgL_iz00_3546;

										BgL_iz00_3546 = BINT(BgL_nz00_1418);
										BgL_rz00_3548 = BgL_arg1412z00_1442;
										BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3549 =
											((bool_t) 0);
										BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_1423 =
											BgL_pickedzd2upzd2onezd2undelimitedzd2charzf3zf3_3549;
										BgL_rz00_1422 = BgL_rz00_3548;
										BgL_iz00_1421 = BgL_iz00_3546;
										goto BgL_zc3z04anonymousza31401ze3z87_1424;
									}
								}
						}
				}
			}
		}

	}



/* &pregexp-split */
	obj_t BGl_z62pregexpzd2splitzb0zz__regexpz00(obj_t BgL_envz00_2487,
		obj_t BgL_patz00_2488, obj_t BgL_strz00_2489)
	{
		{	/* Unsafe/regex.scm 277 */
			{	/* Unsafe/regex.scm 279 */
				obj_t BgL_auxz00_3551;

				if (STRINGP(BgL_strz00_2489))
					{	/* Unsafe/regex.scm 279 */
						BgL_auxz00_3551 = BgL_strz00_2489;
					}
				else
					{
						obj_t BgL_auxz00_3554;

						BgL_auxz00_3554 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(11051L), BGl_string1935z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_strz00_2489);
						FAILURE(BgL_auxz00_3554, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2splitzd2zz__regexpz00(BgL_patz00_2488, BgL_auxz00_3551);
			}
		}

	}



/* pregexp-replace */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2replacezd2zz__regexpz00(obj_t
		BgL_patz00_49, obj_t BgL_strz00_50, obj_t BgL_insz00_51)
	{
		{	/* Unsafe/regex.scm 302 */
			{	/* Unsafe/regex.scm 303 */
				long BgL_nz00_1450;

				BgL_nz00_1450 = STRING_LENGTH(BgL_strz00_50);
				{	/* Unsafe/regex.scm 303 */
					obj_t BgL_ppz00_1451;

					{	/* Unsafe/regex.scm 67 */

						if (BGL_REGEXPP(BgL_patz00_49))
							{	/* Unsafe/regex.scm 179 */
								BgL_ppz00_1451 =
									bgl_regmatch(BgL_patz00_49,
									BSTRING_TO_STRING(BgL_strz00_50), ((bool_t) 0),
									(int) (0L), (int) (BgL_nz00_1450), (int) (0L));
							}
						else
							{	/* Unsafe/regex.scm 181 */
								obj_t BgL_rxz00_2244;

								BgL_rxz00_2244 =
									bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
									(BgL_patz00_49), BNIL, ((bool_t) 0));
								{	/* Unsafe/regex.scm 181 */
									obj_t BgL_valz00_2246;

									BgL_valz00_2246 =
										bgl_regmatch(BgL_rxz00_2244,
										BSTRING_TO_STRING(BgL_strz00_50), ((bool_t) 0),
										(int) (0L), (int) (BgL_nz00_1450), (int) (0L));
									{	/* Unsafe/regex.scm 182 */

										bgl_regfree(BgL_rxz00_2244);
										BgL_ppz00_1451 = BgL_valz00_2246;
					}}}}
					{	/* Unsafe/regex.scm 304 */

						if (CBOOL(BgL_ppz00_1451))
							{	/* Unsafe/regex.scm 306 */
								long BgL_inszd2lenzd2_1452;
								obj_t BgL_mzd2izd2_1453;
								obj_t BgL_mzd2nzd2_1454;

								BgL_inszd2lenzd2_1452 = STRING_LENGTH(BgL_insz00_51);
								{	/* Unsafe/regex.scm 307 */
									obj_t BgL_pairz00_2251;

									BgL_pairz00_2251 = CAR(((obj_t) BgL_ppz00_1451));
									BgL_mzd2izd2_1453 = CAR(BgL_pairz00_2251);
								}
								{	/* Unsafe/regex.scm 308 */
									obj_t BgL_pairz00_2255;

									BgL_pairz00_2255 = CAR(((obj_t) BgL_ppz00_1451));
									BgL_mzd2nzd2_1454 = CDR(BgL_pairz00_2255);
								}
								return
									string_append_3(c_substring(BgL_strz00_50, 0L,
										(long) CINT(BgL_mzd2izd2_1453)),
									BGl_pregexpzd2replacezd2auxz00zz__regexpz00(BgL_strz00_50,
										BgL_insz00_51, BgL_inszd2lenzd2_1452, BgL_ppz00_1451),
									c_substring(BgL_strz00_50, (long) CINT(BgL_mzd2nzd2_1454),
										BgL_nz00_1450));
							}
						else
							{	/* Unsafe/regex.scm 305 */
								return BgL_strz00_50;
							}
					}
				}
			}
		}

	}



/* &pregexp-replace */
	obj_t BGl_z62pregexpzd2replacezb0zz__regexpz00(obj_t BgL_envz00_2490,
		obj_t BgL_patz00_2491, obj_t BgL_strz00_2492, obj_t BgL_insz00_2493)
	{
		{	/* Unsafe/regex.scm 302 */
			{	/* Unsafe/regex.scm 303 */
				obj_t BgL_auxz00_3597;
				obj_t BgL_auxz00_3590;

				if (STRINGP(BgL_insz00_2493))
					{	/* Unsafe/regex.scm 303 */
						BgL_auxz00_3597 = BgL_insz00_2493;
					}
				else
					{
						obj_t BgL_auxz00_3600;

						BgL_auxz00_3600 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(12022L), BGl_string1936z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_insz00_2493);
						FAILURE(BgL_auxz00_3600, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_strz00_2492))
					{	/* Unsafe/regex.scm 303 */
						BgL_auxz00_3590 = BgL_strz00_2492;
					}
				else
					{
						obj_t BgL_auxz00_3593;

						BgL_auxz00_3593 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(12022L), BGl_string1936z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_strz00_2492);
						FAILURE(BgL_auxz00_3593, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2replacezd2zz__regexpz00(BgL_patz00_2491,
					BgL_auxz00_3590, BgL_auxz00_3597);
			}
		}

	}



/* pregexp-replace* */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t
		BgL_patz00_52, obj_t BgL_strz00_53, obj_t BgL_insz00_54)
	{
		{	/* Unsafe/regex.scm 317 */
			{	/* Unsafe/regex.scm 320 */
				obj_t BgL_patz00_1463;
				long BgL_nz00_1464;
				long BgL_inszd2lenzd2_1465;

				if (STRINGP(BgL_patz00_52))
					{	/* Unsafe/regex.scm 320 */
						BgL_patz00_1463 =
							bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
							(BgL_patz00_52), BNIL, ((bool_t) 1));
					}
				else
					{	/* Unsafe/regex.scm 320 */
						BgL_patz00_1463 = BgL_patz00_52;
					}
				BgL_nz00_1464 = STRING_LENGTH(BgL_strz00_53);
				BgL_inszd2lenzd2_1465 = STRING_LENGTH(BgL_insz00_54);
				{
					obj_t BgL_iz00_1467;
					obj_t BgL_rz00_1468;

					BgL_iz00_1467 = BINT(0L);
					BgL_rz00_1468 = BGl_string1934z00zz__regexpz00;
				BgL_zc3z04anonymousza31417ze3z87_1469:
					if (((long) CINT(BgL_iz00_1467) >= BgL_nz00_1464))
						{	/* Unsafe/regex.scm 326 */
							return BgL_rz00_1468;
						}
					else
						{	/* Unsafe/regex.scm 327 */
							obj_t BgL_ppz00_1471;

							{	/* Unsafe/regex.scm 67 */

								if (BGL_REGEXPP(BgL_patz00_1463))
									{	/* Unsafe/regex.scm 179 */
										BgL_ppz00_1471 =
											bgl_regmatch(BgL_patz00_1463,
											BSTRING_TO_STRING(BgL_strz00_53), ((bool_t) 0),
											CINT(BgL_iz00_1467), (int) (BgL_nz00_1464), (int) (0L));
									}
								else
									{	/* Unsafe/regex.scm 181 */
										obj_t BgL_rxz00_2269;

										BgL_rxz00_2269 =
											bgl_regcomp(BGl_pregexpzd2normaliza7ez75zz__regexpz00
											(BgL_patz00_1463), BNIL, ((bool_t) 0));
										{	/* Unsafe/regex.scm 181 */
											obj_t BgL_valz00_2271;

											BgL_valz00_2271 =
												bgl_regmatch(BgL_rxz00_2269,
												BSTRING_TO_STRING(BgL_strz00_53), ((bool_t) 0),
												CINT(BgL_iz00_1467), (int) (BgL_nz00_1464), (int) (0L));
											{	/* Unsafe/regex.scm 182 */

												bgl_regfree(BgL_rxz00_2269);
												BgL_ppz00_1471 = BgL_valz00_2271;
							}}}}
							if (CBOOL(BgL_ppz00_1471))
								{	/* Unsafe/regex.scm 338 */
									obj_t BgL_arg1419z00_1472;
									obj_t BgL_arg1420z00_1473;

									{	/* Unsafe/regex.scm 338 */
										obj_t BgL_pairz00_2275;

										BgL_pairz00_2275 = CAR(((obj_t) BgL_ppz00_1471));
										BgL_arg1419z00_1472 = CDR(BgL_pairz00_2275);
									}
									{	/* Unsafe/regex.scm 341 */
										obj_t BgL_arg1421z00_1474;
										obj_t BgL_arg1422z00_1475;

										{	/* Unsafe/regex.scm 341 */
											obj_t BgL_arg1423z00_1476;

											{	/* Unsafe/regex.scm 341 */
												obj_t BgL_pairz00_2279;

												BgL_pairz00_2279 = CAR(((obj_t) BgL_ppz00_1471));
												BgL_arg1423z00_1476 = CAR(BgL_pairz00_2279);
											}
											BgL_arg1421z00_1474 =
												c_substring(BgL_strz00_53,
												(long) CINT(BgL_iz00_1467),
												(long) CINT(BgL_arg1423z00_1476));
										}
										BgL_arg1422z00_1475 =
											BGl_pregexpzd2replacezd2auxz00zz__regexpz00(BgL_strz00_53,
											BgL_insz00_54, BgL_inszd2lenzd2_1465, BgL_ppz00_1471);
										BgL_arg1420z00_1473 =
											string_append_3(BgL_rz00_1468, BgL_arg1421z00_1474,
											BgL_arg1422z00_1475);
									}
									{
										obj_t BgL_rz00_3643;
										obj_t BgL_iz00_3642;

										BgL_iz00_3642 = BgL_arg1419z00_1472;
										BgL_rz00_3643 = BgL_arg1420z00_1473;
										BgL_rz00_1468 = BgL_rz00_3643;
										BgL_iz00_1467 = BgL_iz00_3642;
										goto BgL_zc3z04anonymousza31417ze3z87_1469;
									}
								}
							else
								{	/* Unsafe/regex.scm 328 */
									if (((long) CINT(BgL_iz00_1467) == 0L))
										{	/* Unsafe/regex.scm 329 */
											return BgL_strz00_53;
										}
									else
										{	/* Unsafe/regex.scm 329 */
											return
												string_append(BgL_rz00_1468,
												c_substring(BgL_strz00_53,
													(long) CINT(BgL_iz00_1467), BgL_nz00_1464));
		}}}}}}

	}



/* &pregexp-replace* */
	obj_t BGl_z62pregexpzd2replaceza2z12zz__regexpz00(obj_t BgL_envz00_2494,
		obj_t BgL_patz00_2495, obj_t BgL_strz00_2496, obj_t BgL_insz00_2497)
	{
		{	/* Unsafe/regex.scm 317 */
			{	/* Unsafe/regex.scm 320 */
				obj_t BgL_auxz00_3658;
				obj_t BgL_auxz00_3651;

				if (STRINGP(BgL_insz00_2497))
					{	/* Unsafe/regex.scm 320 */
						BgL_auxz00_3658 = BgL_insz00_2497;
					}
				else
					{
						obj_t BgL_auxz00_3661;

						BgL_auxz00_3661 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(12646L), BGl_string1937z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_insz00_2497);
						FAILURE(BgL_auxz00_3661, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_strz00_2496))
					{	/* Unsafe/regex.scm 320 */
						BgL_auxz00_3651 = BgL_strz00_2496;
					}
				else
					{
						obj_t BgL_auxz00_3654;

						BgL_auxz00_3654 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(12646L), BGl_string1937z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_strz00_2496);
						FAILURE(BgL_auxz00_3654, BFALSE, BFALSE);
					}
				return
					BGl_pregexpzd2replaceza2z70zz__regexpz00(BgL_patz00_2495,
					BgL_auxz00_3651, BgL_auxz00_3658);
			}
		}

	}



/* pregexp-quote */
	BGL_EXPORTED_DEF obj_t BGl_pregexpzd2quotezd2zz__regexpz00(obj_t BgL_sz00_55)
	{
		{	/* Unsafe/regex.scm 347 */
			{	/* Unsafe/regex.scm 348 */
				long BgL_g1057z00_1487;

				BgL_g1057z00_1487 = (STRING_LENGTH(BgL_sz00_55) - 1L);
				{
					long BgL_iz00_1490;
					obj_t BgL_rz00_1491;

					BgL_iz00_1490 = BgL_g1057z00_1487;
					BgL_rz00_1491 = BNIL;
				BgL_zc3z04anonymousza31428ze3z87_1492:
					if ((BgL_iz00_1490 < 0L))
						{	/* Unsafe/regex.scm 349 */
							return
								BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(BgL_rz00_1491);
						}
					else
						{	/* Unsafe/regex.scm 351 */
							long BgL_arg1430z00_1494;
							obj_t BgL_arg1431z00_1495;

							BgL_arg1430z00_1494 = (BgL_iz00_1490 - 1L);
							{	/* Unsafe/regex.scm 352 */
								unsigned char BgL_cz00_1496;

								BgL_cz00_1496 = STRING_REF(BgL_sz00_55, BgL_iz00_1490);
								if (CBOOL(BGl_memvz00zz__r4_pairs_and_lists_6_3z00(BCHAR
											(BgL_cz00_1496), BGl_list1938z00zz__regexpz00)))
									{	/* Unsafe/regex.scm 355 */
										obj_t BgL_arg1434z00_1498;

										BgL_arg1434z00_1498 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1496), BgL_rz00_1491);
										BgL_arg1431z00_1495 =
											MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '\\')),
											BgL_arg1434z00_1498);
									}
								else
									{	/* Unsafe/regex.scm 353 */
										BgL_arg1431z00_1495 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_cz00_1496), BgL_rz00_1491);
									}
							}
							{
								obj_t BgL_rz00_3684;
								long BgL_iz00_3683;

								BgL_iz00_3683 = BgL_arg1430z00_1494;
								BgL_rz00_3684 = BgL_arg1431z00_1495;
								BgL_rz00_1491 = BgL_rz00_3684;
								BgL_iz00_1490 = BgL_iz00_3683;
								goto BgL_zc3z04anonymousza31428ze3z87_1492;
							}
						}
				}
			}
		}

	}



/* &pregexp-quote */
	obj_t BGl_z62pregexpzd2quotezb0zz__regexpz00(obj_t BgL_envz00_2498,
		obj_t BgL_sz00_2499)
	{
		{	/* Unsafe/regex.scm 347 */
			{	/* Unsafe/regex.scm 348 */
				obj_t BgL_auxz00_3685;

				if (STRINGP(BgL_sz00_2499))
					{	/* Unsafe/regex.scm 348 */
						BgL_auxz00_3685 = BgL_sz00_2499;
					}
				else
					{
						obj_t BgL_auxz00_3688;

						BgL_auxz00_3688 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string1917z00zz__regexpz00,
							BINT(13607L), BGl_string1939z00zz__regexpz00,
							BGl_string1928z00zz__regexpz00, BgL_sz00_2499);
						FAILURE(BgL_auxz00_3688, BFALSE, BFALSE);
					}
				return BGl_pregexpzd2quotezd2zz__regexpz00(BgL_auxz00_3685);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/regex.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/regex.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__regexpz00(void)
	{
		{	/* Unsafe/regex.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__regexpz00(void)
	{
		{	/* Unsafe/regex.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zz__errorz00(88804785L,
				BSTRING_TO_STRING(BGl_string1940z00zz__regexpz00));
		}

	}

#ifdef __cplusplus
}
#endif
