/*===========================================================================*/
/*   (Unsafe/sha2.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/sha2.scm -indent -o objs/obj_u/Unsafe/sha2.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___SHA2_TYPE_DEFINITIONS
#define BGL___SHA2_TYPE_DEFINITIONS
#endif													// BGL___SHA2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_sha512zd2internalzd2transformz00zz__sha2z00(obj_t, obj_t);
	static obj_t BGl_sha256zd2updatezd2zz__sha2z00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hmaczd2sha512sumzd2stringz00zz__sha2z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_hmaczd2sha256sumzd2stringz00zz__sha2z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha256sumzd2filezd2zz__sha2z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32003ze3ze5zz__sha2z00(obj_t);
	static obj_t BGl_state64zd2ze3stringz31zz__sha2z00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__sha2z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_sha256sumzd2portzd2zz__sha2z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32004ze3ze5zz__sha2z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha512sumz00zz__sha2z00(obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__sha2z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__srfi4z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__hmacz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha256sumzd2mmapzd2zz__sha2z00(obj_t);
	extern obj_t BGl_openzd2mmapzd2zz__mmapz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sha512sumzd2stringzb0zz__sha2z00(obj_t, obj_t);
	static long BGl_z62fillzd2word32zd2mmapz12z70zz__sha2z00(obj_t, obj_t, long,
		obj_t, long);
	static obj_t BGl_z62sha256sumzd2stringzb0zz__sha2z00(obj_t, obj_t);
	static long BGl_z62fillzd2word32zd2stringz12z70zz__sha2z00(obj_t, obj_t, long,
		obj_t, long);
	static uint32_t BGl_addu32z00zz__sha2z00(uint32_t, uint32_t);
	static obj_t BGl_toplevelzd2initzd2zz__sha2z00(void);
	extern obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62hmaczd2sha512sumzd2stringz62zz__sha2z00(obj_t, obj_t,
		obj_t);
	static long BGl_z62fillzd2word64zd2mmapz12z70zz__sha2z00(obj_t, obj_t, long,
		obj_t, long);
	static obj_t BGl_z62sha512sumzd2filezb0zz__sha2z00(obj_t, obj_t);
	static obj_t BGl_z62hmaczd2sha256sumzd2stringz62zz__sha2z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zz__sha2z00(void);
	static obj_t BGl_z62sha512sumzd2portzb0zz__sha2z00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zz__sha2z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__sha2z00(void);
	static obj_t BGl_objectzd2initzd2zz__sha2z00(void);
	extern obj_t BGl_hmaczd2stringzd2zz__hmacz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sha512sumzd2mmapzb0zz__sha2z00(obj_t, obj_t);
	extern obj_t BGl_makezd2u32vectorzd2zz__srfi4z00(long, uint32_t);
	extern obj_t BGl_makezd2u64vectorzd2zz__srfi4z00(long, uint64_t);
	static obj_t BGl_sha256zd2initialzd2hashzd2valuezd2zz__sha2z00(void);
	BGL_EXPORTED_DECL obj_t BGl_sha512sumzd2filezd2zz__sha2z00(obj_t);
	static obj_t BGl_z62sha256sumzd2filezb0zz__sha2z00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zz__sha2z00(void);
	extern obj_t blit_string(obj_t, long, obj_t, long, long);
	static obj_t BGl_z62sha256sumz62zz__sha2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha512sumzd2portzd2zz__sha2z00(obj_t);
	static obj_t BGl_statezd2ze3stringz31zz__sha2z00(obj_t);
	static obj_t BGl_z62sha256sumzd2portzb0zz__sha2z00(obj_t, obj_t);
	static uint32_t BGl_Sigma1zd2256zd2zz__sha2z00(uint32_t);
	static long BGl_z62fillzd2word32zd2portz12z70zz__sha2z00(obj_t, obj_t, long,
		obj_t, long);
	static obj_t BGl_u32zd2fillz12zc0zz__sha2z00(obj_t, long, uint32_t);
	static obj_t BGl_z62zc3z04anonymousza31656ze3ze5zz__sha2z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha512sumzd2mmapzd2zz__sha2z00(obj_t);
	static obj_t BGl_sha512zd2initialzd2hashzd2valuezd2zz__sha2z00(void);
	extern obj_t make_string(long, unsigned char);
	static obj_t BGl_K256z00zz__sha2z00 = BUNSPEC;
	extern obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_z62sha256sumzd2mmapzb0zz__sha2z00(obj_t, obj_t);
	static obj_t BGl_sha256zd2internalzd2transformz00zz__sha2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha512sumzd2stringzd2zz__sha2z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha256sumzd2stringzd2zz__sha2z00(obj_t);
	static long BGl_z62fillzd2word64zd2portz12z70zz__sha2z00(obj_t, obj_t, long,
		obj_t, long);
	static obj_t BGl_u64zd2fillz12zc0zz__sha2z00(obj_t, long, uint64_t);
	static obj_t BGl_z62zc3z04anonymousza31657ze3ze5zz__sha2z00(obj_t);
	extern obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
	static uint64_t BGl_Sigma1zd2512zd2zz__sha2z00(uint64_t);
	static obj_t BGl_z62sha512sumz62zz__sha2z00(obj_t, obj_t);
	static obj_t BGl_K512z00zz__sha2z00 = BUNSPEC;
	static obj_t BGl_sha512zd2updatezd2zz__sha2z00(obj_t, obj_t, obj_t, obj_t);
	static long BGl_z62fillzd2word64zd2stringz12z70zz__sha2z00(obj_t, obj_t, long,
		obj_t, long);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sha256sumz00zz__sha2z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha512sumzd2stringzd2envz00zz__sha2z00,
		BgL_bgl_za762sha512sumza7d2s2398z00,
		BGl_z62sha512sumzd2stringzb0zz__sha2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha256sumzd2portzd2envz00zz__sha2z00,
		BgL_bgl_za762sha256sumza7d2p2399z00, BGl_z62sha256sumzd2portzb0zz__sha2z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha512sumzd2filezd2envz00zz__sha2z00,
		BgL_bgl_za762sha512sumza7d2f2400z00, BGl_z62sha512sumzd2filezb0zz__sha2z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha256sumzd2stringzd2envz00zz__sha2z00,
		BgL_bgl_za762sha256sumza7d2s2401z00,
		BGl_z62sha256sumzd2stringzb0zz__sha2z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha512sumzd2envzd2zz__sha2z00,
		BgL_bgl_za762sha512sumza762za72402za7, BGl_z62sha512sumz62zz__sha2z00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha256sumzd2envzd2zz__sha2z00,
		BgL_bgl_za762sha256sumza762za72403za7, BGl_z62sha256sumz62zz__sha2z00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha512sumzd2portzd2envz00zz__sha2z00,
		BgL_bgl_za762sha512sumza7d2p2404z00, BGl_z62sha512sumzd2portzb0zz__sha2z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2378z00zz__sha2z00,
		BgL_bgl_string2378za700za7za7_2405za7,
		"/tmp/bigloo/runtime/Unsafe/sha2.scm", 35);
	      DEFINE_STRING(BGl_string2379z00zz__sha2z00,
		BgL_bgl_string2379za700za7za7_2406za7, "&sha256sum-mmap", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha256sumzd2mmapzd2envz00zz__sha2z00,
		BgL_bgl_za762sha256sumza7d2m2407z00, BGl_z62sha256sumzd2mmapzb0zz__sha2z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2380z00zz__sha2z00,
		BgL_bgl_string2380za700za7za7_2408za7, "mmap", 4);
	      DEFINE_STRING(BGl_string2382z00zz__sha2z00,
		BgL_bgl_string2382za700za7za7_2409za7, "&sha256sum-string", 17);
	      DEFINE_STRING(BGl_string2383z00zz__sha2z00,
		BgL_bgl_string2383za700za7za7_2410za7, "bstring", 7);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2377z00zz__sha2z00,
		BgL_bgl_za762fillza7d2word322411z00,
		BGl_z62fillzd2word32zd2mmapz12z70zz__sha2z00);
	      DEFINE_STRING(BGl_string2384z00zz__sha2z00,
		BgL_bgl_string2384za700za7za7_2412za7, "&sha256sum-port", 15);
	      DEFINE_STRING(BGl_string2385z00zz__sha2z00,
		BgL_bgl_string2385za700za7za7_2413za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2386z00zz__sha2z00,
		BgL_bgl_string2386za700za7za7_2414za7, "&sha256sum-file", 15);
	      DEFINE_STRING(BGl_string2387z00zz__sha2z00,
		BgL_bgl_string2387za700za7za7_2415za7, "sha256sum", 9);
	      DEFINE_STRING(BGl_string2388z00zz__sha2z00,
		BgL_bgl_string2388za700za7za7_2416za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string2389z00zz__sha2z00,
		BgL_bgl_string2389za700za7za7_2417za7, "&sha512sum-mmap", 15);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2381z00zz__sha2z00,
		BgL_bgl_za762fillza7d2word322418z00,
		BGl_z62fillzd2word32zd2stringz12z70zz__sha2z00);
	      DEFINE_STRING(BGl_string2391z00zz__sha2z00,
		BgL_bgl_string2391za700za7za7_2419za7, "&sha512sum-string", 17);
	      DEFINE_STRING(BGl_string2392z00zz__sha2z00,
		BgL_bgl_string2392za700za7za7_2420za7, "&sha512sum-port", 15);
	      DEFINE_STRING(BGl_string2393z00zz__sha2z00,
		BgL_bgl_string2393za700za7za7_2421za7, "&sha512sum-file", 15);
	      DEFINE_STRING(BGl_string2394z00zz__sha2z00,
		BgL_bgl_string2394za700za7za7_2422za7, "sha512sum", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_hmaczd2sha512sumzd2stringzd2envzd2zz__sha2z00,
		BgL_bgl_za762hmacza7d2sha5122423z00,
		BGl_z62hmaczd2sha512sumzd2stringz62zz__sha2z00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2395z00zz__sha2z00,
		BgL_bgl_string2395za700za7za7_2424za7, "&hmac-sha256sum-string", 22);
	      DEFINE_STRING(BGl_string2396z00zz__sha2z00,
		BgL_bgl_string2396za700za7za7_2425za7, "&hmac-sha512sum-string", 22);
	      DEFINE_STRING(BGl_string2397z00zz__sha2z00,
		BgL_bgl_string2397za700za7za7_2426za7, "__sha2", 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha256sumzd2filezd2envz00zz__sha2z00,
		BgL_bgl_za762sha256sumza7d2f2427z00, BGl_z62sha256sumzd2filezb0zz__sha2z00,
		0L, BUNSPEC, 1);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2390z00zz__sha2z00,
		BgL_bgl_za762fillza7d2word642428z00,
		BGl_z62fillzd2word64zd2stringz12z70zz__sha2z00);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_hmaczd2sha256sumzd2stringzd2envzd2zz__sha2z00,
		BgL_bgl_za762hmacza7d2sha2562429z00,
		BGl_z62hmaczd2sha256sumzd2stringz62zz__sha2z00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sha512sumzd2mmapzd2envz00zz__sha2z00,
		BgL_bgl_za762sha512sumza7d2m2430z00, BGl_z62sha512sumzd2mmapzb0zz__sha2z00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__sha2z00));
		     ADD_ROOT((void *) (&BGl_K256z00zz__sha2z00));
		     ADD_ROOT((void *) (&BGl_K512z00zz__sha2z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__sha2z00(long
		BgL_checksumz00_5345, char *BgL_fromz00_5346)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__sha2z00))
				{
					BGl_requirezd2initializa7ationz75zz__sha2z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__sha2z00();
					BGl_importedzd2moduleszd2initz00zz__sha2z00();
					return BGl_toplevelzd2initzd2zz__sha2z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 50 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 50 */
			{	/* Unsafe/sha2.scm 339 */
				obj_t BgL_vz00_1363;

				{	/* Llib/srfi4.scm 451 */

					BgL_vz00_1363 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(64L, (uint32_t) (0));
				}
				BGL_U32VSET(BgL_vz00_1363, 0L, (uint32_t) (1116352408));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 1L, (uint32_t) (1899447441));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 2L, (uint32_t) (3049323471));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 3L, (uint32_t) (3921009573));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 4L, (uint32_t) (961987163));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 5L, (uint32_t) (1508970993));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 6L, (uint32_t) (2453635748));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 7L, (uint32_t) (2870763221));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 8L, (uint32_t) (3624381080));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 9L, (uint32_t) (310598401));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 10L, (uint32_t) (607225278));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 11L, (uint32_t) (1426881987));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 12L, (uint32_t) (1925078388));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 13L, (uint32_t) (2162078206));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 14L, (uint32_t) (2614888103));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 15L, (uint32_t) (3248222580));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 16L, (uint32_t) (3835390401));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 17L, (uint32_t) (4022224774));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 18L, (uint32_t) (264347078));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 19L, (uint32_t) (604807628));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 20L, (uint32_t) (770255983));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 21L, (uint32_t) (1249150122));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 22L, (uint32_t) (1555081692));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 23L, (uint32_t) (1996064986));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 24L, (uint32_t) (2554220882));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 25L, (uint32_t) (2821834349));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 26L, (uint32_t) (2952996808));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 27L, (uint32_t) (3210313671));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 28L, (uint32_t) (3336571891));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 29L, (uint32_t) (3584528711));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 30L, (uint32_t) (113926993));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 31L, (uint32_t) (338241895));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 32L, (uint32_t) (666307205));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 33L, (uint32_t) (773529912));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 34L, (uint32_t) (1294757372));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 35L, (uint32_t) (1396182291));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 36L, (uint32_t) (1695183700));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 37L, (uint32_t) (1986661051));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 38L, (uint32_t) (2177026350));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 39L, (uint32_t) (2456956037));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 40L, (uint32_t) (2730485921));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 41L, (uint32_t) (2820302411));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 42L, (uint32_t) (3259730800));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 43L, (uint32_t) (3345764771));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 44L, (uint32_t) (3516065817));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 45L, (uint32_t) (3600352804));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 46L, (uint32_t) (4094571909));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 47L, (uint32_t) (275423344));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 48L, (uint32_t) (430227734));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 49L, (uint32_t) (506948616));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 50L, (uint32_t) (659060556));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 51L, (uint32_t) (883997877));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 52L, (uint32_t) (958139571));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 53L, (uint32_t) (1322822218));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 54L, (uint32_t) (1537002063));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 55L, (uint32_t) (1747873779));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 56L, (uint32_t) (1955562222));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 57L, (uint32_t) (2024104815));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 58L, (uint32_t) (2227730452));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 59L, (uint32_t) (2361852424));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 60L, (uint32_t) (2428436474));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 61L, (uint32_t) (2756734187));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 62L, (uint32_t) (3204031479));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1363, 63L, (uint32_t) (3329325298));
				BUNSPEC;
				BGl_K256z00zz__sha2z00 = BgL_vz00_1363;
			}
			{	/* Unsafe/sha2.scm 440 */
				obj_t BgL_vz00_1366;

				{	/* Llib/srfi4.scm 453 */

					BgL_vz00_1366 =
						BGl_makezd2u64vectorzd2zz__srfi4z00(80L, (uint64_t) (0));
				}
				BGL_U64VSET(BgL_vz00_1366, 0L, (uint64_t) (4794697086780616226));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 1L, (uint64_t) (8158064640168781261));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 2L, (uint64_t) (-5349999486874862801));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 3L, (uint64_t) (-1606136188198331460));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 4L, (uint64_t) (4131703408338449720));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 5L, (uint64_t) (6480981068601479193));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 6L, (uint64_t) (-7908458776815382629));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 7L, (uint64_t) (-6116909921290321640));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 8L, (uint64_t) (-2880145864133508542));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 9L, (uint64_t) (1334009975649890238));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 10L, (uint64_t) (2608012711638119052));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 11L, (uint64_t) (6128411473006802146));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 12L, (uint64_t) (8268148722764581231));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 13L, (uint64_t) (-9160688886553864527));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 14L, (uint64_t) (-7215885187991268811));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 15L, (uint64_t) (-4495734319001033068));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 16L, (uint64_t) (-1973867731355612462));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 17L, (uint64_t) (-1171420211273849373));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 18L, (uint64_t) (1135362057144423861));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 19L, (uint64_t) (2597628984639134821));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 20L, (uint64_t) (3308224258029322869));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 21L, (uint64_t) (5365058923640841347));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 22L, (uint64_t) (6679025012923562964));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 23L, (uint64_t) (8573033837759648693));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 24L, (uint64_t) (-7476448914759557205));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 25L, (uint64_t) (-6327057829258317296));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 26L, (uint64_t) (-5763719355590565569));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 27L, (uint64_t) (-4658551843659510044));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 28L, (uint64_t) (-4116276920077217854));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 29L, (uint64_t) (-3051310485924567259));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 30L, (uint64_t) (489312712824947311));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 31L, (uint64_t) (1452737877330783856));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 32L, (uint64_t) (2861767655752347644));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 33L, (uint64_t) (3322285676063803686));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 34L, (uint64_t) (5560940570517711597));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 35L, (uint64_t) (5996557281743188959));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 36L, (uint64_t) (7280758554555802590));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 37L, (uint64_t) (8532644243296465576));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 38L, (uint64_t) (-9096487096722542874));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 39L, (uint64_t) (-7894198246740708037));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 40L, (uint64_t) (-6719396339535248540));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 41L, (uint64_t) (-6333637450476146687));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 42L, (uint64_t) (-4446306890439682159));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 43L, (uint64_t) (-4076793802049405392));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 44L, (uint64_t) (-3345356375505022440));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 45L, (uint64_t) (-2983346525034927856));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 46L, (uint64_t) (-860691631967231958));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 47L, (uint64_t) (1182934255886127544));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 48L, (uint64_t) (1847814050463011016));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 49L, (uint64_t) (2177327727835720531));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 50L, (uint64_t) (2830643537854262169));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 51L, (uint64_t) (3796741975233480872));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 52L, (uint64_t) (4115178125766777443));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 53L, (uint64_t) (5681478168544905931));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 54L, (uint64_t) (6601373596472566643));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 55L, (uint64_t) (7507060721942968483));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 56L, (uint64_t) (8399075790359081724));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 57L, (uint64_t) (8693463985226723168));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 58L, (uint64_t) (-8878714635349349518));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 59L, (uint64_t) (-8302665154208450068));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 60L, (uint64_t) (-8016688836872298968));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 61L, (uint64_t) (-6606660893046293015));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 62L, (uint64_t) (-4685533653050689259));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 63L, (uint64_t) (-4147400797238176981));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 64L, (uint64_t) (-3880063495543823972));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 65L, (uint64_t) (-3348786107499101689));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 66L, (uint64_t) (-1523767162380948706));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 67L, (uint64_t) (-757361751448694408));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 68L, (uint64_t) (500013540394364858));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 69L, (uint64_t) (748580250866718886));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 70L, (uint64_t) (1242879168328830382));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 71L, (uint64_t) (1977374033974150939));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 72L, (uint64_t) (2944078676154940804));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 73L, (uint64_t) (3659926193048069267));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 74L, (uint64_t) (4368137639120453308));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 75L, (uint64_t) (4836135668995329356));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 76L, (uint64_t) (5532061633213252278));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 77L, (uint64_t) (6448918945643986474));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 78L, (uint64_t) (6902733635092675308));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1366, 79L, (uint64_t) (7801388544844847127));
				BUNSPEC;
				return (BGl_K512z00zz__sha2z00 = BgL_vz00_1366, BUNSPEC);
			}
		}

	}



/* addu32 */
	uint32_t BGl_addu32z00zz__sha2z00(uint32_t BgL_n1z00_3, uint32_t BgL_n2z00_4)
	{
		{	/* Unsafe/sha2.scm 185 */
			{	/* Unsafe/sha2.scm 186 */
				uint32_t BgL_h1z00_1377;

				BgL_h1z00_1377 = (BgL_n1z00_3 >> (int) (16L));
				{	/* Unsafe/sha2.scm 186 */
					uint32_t BgL_h2z00_1378;

					BgL_h2z00_1378 = (BgL_n2z00_4 >> (int) (16L));
					{	/* Unsafe/sha2.scm 187 */
						uint32_t BgL_hz00_1379;

						{	/* Unsafe/sha2.scm 188 */
							uint32_t BgL_arg1304z00_1386;
							uint32_t BgL_arg1305z00_1387;

							BgL_arg1304z00_1386 = (uint32_t) (65535L);
							BgL_arg1305z00_1387 = (BgL_h1z00_1377 + BgL_h2z00_1378);
							BgL_hz00_1379 = (BgL_arg1304z00_1386 & BgL_arg1305z00_1387);
						}
						{	/* Unsafe/sha2.scm 188 */
							uint32_t BgL_l1z00_1380;

							{	/* Unsafe/sha2.scm 189 */
								uint32_t BgL_arg1284z00_1385;

								BgL_arg1284z00_1385 = (uint32_t) (65535L);
								BgL_l1z00_1380 = (BgL_n1z00_3 & BgL_arg1284z00_1385);
							}
							{	/* Unsafe/sha2.scm 189 */
								uint32_t BgL_l2z00_1381;

								{	/* Unsafe/sha2.scm 190 */
									uint32_t BgL_arg1272z00_1384;

									BgL_arg1272z00_1384 = (uint32_t) (65535L);
									BgL_l2z00_1381 = (BgL_n2z00_4 & BgL_arg1272z00_1384);
								}
								{	/* Unsafe/sha2.scm 192 */

									return
										(
										(BgL_hz00_1379 <<
											(int) (16L)) + (BgL_l1z00_1380 + BgL_l2z00_1381));
		}}}}}}}

	}



/* u32-fill! */
	obj_t BGl_u32zd2fillz12zc0zz__sha2z00(obj_t BgL_strz00_9,
		long BgL_offsetz00_10, uint32_t BgL_wz00_11)
	{
		{	/* Unsafe/sha2.scm 236 */
			{	/* Unsafe/sha2.scm 237 */
				obj_t BgL_s1z00_1394;

				{	/* Unsafe/sha2.scm 237 */
					long BgL_arg1321z00_1406;

					{	/* Unsafe/sha2.scm 237 */
						uint32_t BgL_tmpz00_5515;

						BgL_tmpz00_5515 = (BgL_wz00_11 >> (int) (16L));
						BgL_arg1321z00_1406 = (long) (BgL_tmpz00_5515);
					}
					BgL_s1z00_1394 =
						BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
						(BgL_arg1321z00_1406, 16L);
				}
				{	/* Unsafe/sha2.scm 237 */
					long BgL_l1z00_1395;

					BgL_l1z00_1395 = STRING_LENGTH(BgL_s1z00_1394);
					{	/* Unsafe/sha2.scm 238 */
						obj_t BgL_s2z00_1396;

						{	/* Unsafe/sha2.scm 239 */
							long BgL_arg1318z00_1403;

							{	/* Unsafe/sha2.scm 239 */
								uint32_t BgL_arg1319z00_1404;

								BgL_arg1319z00_1404 =
									(BgL_wz00_11 & ((uint32_t) (65536) - (uint32_t) (1L)));
								BgL_arg1318z00_1403 = (long) (BgL_arg1319z00_1404);
							}
							BgL_s2z00_1396 =
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
								(BgL_arg1318z00_1403, 16L);
						}
						{	/* Unsafe/sha2.scm 239 */
							long BgL_l2z00_1397;

							BgL_l2z00_1397 = STRING_LENGTH(BgL_s2z00_1396);
							{	/* Unsafe/sha2.scm 240 */

								blit_string(BgL_s1z00_1394, 0L, BgL_strz00_9,
									(BgL_offsetz00_10 + (4L - BgL_l1z00_1395)), BgL_l1z00_1395);
								return
									blit_string(BgL_s2z00_1396, 0L, BgL_strz00_9,
									(BgL_offsetz00_10 +
										(4L + (4L - BgL_l2z00_1397))), BgL_l2z00_1397);
							}
						}
					}
				}
			}
		}

	}



/* u64-fill! */
	obj_t BGl_u64zd2fillz12zc0zz__sha2z00(obj_t BgL_strz00_12,
		long BgL_offsetz00_13, uint64_t BgL_wz00_14)
	{
		{	/* Unsafe/sha2.scm 247 */
			{	/* Unsafe/sha2.scm 248 */
				obj_t BgL_s1z00_1408;

				{	/* Unsafe/sha2.scm 248 */
					long BgL_arg1360z00_1448;

					{	/* Unsafe/sha2.scm 248 */
						long BgL_arg1361z00_1449;

						{	/* Unsafe/sha2.scm 248 */
							uint32_t BgL_arg1362z00_1450;

							{	/* Unsafe/sha2.scm 248 */
								uint32_t BgL_arg1363z00_1451;

								{	/* Unsafe/sha2.scm 248 */
									uint64_t BgL_tmpz00_5534;

									BgL_tmpz00_5534 = (BgL_wz00_14 >> (int) (32L));
									BgL_arg1363z00_1451 = (uint32_t) (BgL_tmpz00_5534);
								}
								BgL_arg1362z00_1450 = (BgL_arg1363z00_1451 >> (int) (16L));
							}
							BgL_arg1361z00_1449 = (long) (BgL_arg1362z00_1450);
						}
						{	/* Unsafe/sha2.scm 248 */
							uint32_t BgL_xz00_2990;

							BgL_xz00_2990 = (uint32_t) (BgL_arg1361z00_1449);
							BgL_arg1360z00_1448 = (long) (BgL_xz00_2990);
					}}
					BgL_s1z00_1408 =
						BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
						(BgL_arg1360z00_1448, 16L);
				}
				{	/* Unsafe/sha2.scm 248 */
					long BgL_l1z00_1409;

					BgL_l1z00_1409 = STRING_LENGTH(BgL_s1z00_1408);
					{	/* Unsafe/sha2.scm 249 */
						obj_t BgL_s2z00_1410;

						{	/* Unsafe/sha2.scm 250 */
							long BgL_arg1352z00_1442;

							{	/* Unsafe/sha2.scm 250 */
								long BgL_arg1354z00_1443;

								{	/* Unsafe/sha2.scm 250 */
									uint32_t BgL_arg1356z00_1444;

									{	/* Unsafe/sha2.scm 250 */
										uint32_t BgL_arg1357z00_1445;
										uint32_t BgL_arg1358z00_1446;

										{	/* Unsafe/sha2.scm 250 */
											uint64_t BgL_tmpz00_5545;

											BgL_tmpz00_5545 = (BgL_wz00_14 >> (int) (32L));
											BgL_arg1357z00_1445 = (uint32_t) (BgL_tmpz00_5545);
										}
										BgL_arg1358z00_1446 =
											((uint32_t) (65536) - (uint32_t) (1L));
										BgL_arg1356z00_1444 =
											(BgL_arg1357z00_1445 & BgL_arg1358z00_1446);
									}
									BgL_arg1354z00_1443 = (long) (BgL_arg1356z00_1444);
								}
								{	/* Unsafe/sha2.scm 250 */
									uint32_t BgL_xz00_2999;

									BgL_xz00_2999 = (uint32_t) (BgL_arg1354z00_1443);
									BgL_arg1352z00_1442 = (long) (BgL_xz00_2999);
							}}
							BgL_s2z00_1410 =
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
								(BgL_arg1352z00_1442, 16L);
						}
						{	/* Unsafe/sha2.scm 250 */
							long BgL_l2z00_1411;

							BgL_l2z00_1411 = STRING_LENGTH(BgL_s2z00_1410);
							{	/* Unsafe/sha2.scm 251 */
								obj_t BgL_s3z00_1412;

								{	/* Unsafe/sha2.scm 252 */
									long BgL_arg1344z00_1435;

									{	/* Unsafe/sha2.scm 252 */
										long BgL_arg1346z00_1436;

										{	/* Unsafe/sha2.scm 252 */
											uint32_t BgL_arg1347z00_1437;

											{	/* Unsafe/sha2.scm 252 */
												uint32_t BgL_arg1348z00_1438;

												{	/* Unsafe/sha2.scm 252 */
													uint64_t BgL_arg1349z00_1439;

													BgL_arg1349z00_1439 =
														(BgL_wz00_14 &
														(((uint64_t) (1) <<
																(int) (32L)) - (uint64_t) (1L)));
													BgL_arg1348z00_1438 =
														(uint32_t) (BgL_arg1349z00_1439);
												}
												BgL_arg1347z00_1437 =
													(BgL_arg1348z00_1438 >> (int) (16L));
											}
											BgL_arg1346z00_1436 = (long) (BgL_arg1347z00_1437);
										}
										{	/* Unsafe/sha2.scm 252 */
											uint32_t BgL_xz00_3009;

											BgL_xz00_3009 = (uint32_t) (BgL_arg1346z00_1436);
											BgL_arg1344z00_1435 = (long) (BgL_xz00_3009);
									}}
									BgL_s3z00_1412 =
										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
										(BgL_arg1344z00_1435, 16L);
								}
								{	/* Unsafe/sha2.scm 252 */
									long BgL_l3z00_1413;

									BgL_l3z00_1413 = STRING_LENGTH(BgL_s3z00_1412);
									{	/* Unsafe/sha2.scm 253 */
										obj_t BgL_s4z00_1414;

										{	/* Unsafe/sha2.scm 254 */
											long BgL_arg1336z00_1427;

											{	/* Unsafe/sha2.scm 254 */
												long BgL_arg1337z00_1428;

												{	/* Unsafe/sha2.scm 254 */
													uint32_t BgL_arg1338z00_1429;

													{	/* Unsafe/sha2.scm 254 */
														uint32_t BgL_arg1339z00_1430;
														uint32_t BgL_arg1340z00_1431;

														{	/* Unsafe/sha2.scm 254 */
															uint64_t BgL_arg1341z00_1432;

															BgL_arg1341z00_1432 =
																(BgL_wz00_14 &
																(((uint64_t) (1) <<
																		(int) (32L)) - (uint64_t) (1L)));
															BgL_arg1339z00_1430 =
																(uint32_t) (BgL_arg1341z00_1432);
														}
														BgL_arg1340z00_1431 =
															((uint32_t) (65536) - (uint32_t) (1L));
														BgL_arg1338z00_1429 =
															(BgL_arg1339z00_1430 & BgL_arg1340z00_1431);
													}
													BgL_arg1337z00_1428 = (long) (BgL_arg1338z00_1429);
												}
												{	/* Unsafe/sha2.scm 254 */
													uint32_t BgL_xz00_3022;

													BgL_xz00_3022 = (uint32_t) (BgL_arg1337z00_1428);
													BgL_arg1336z00_1427 = (long) (BgL_xz00_3022);
											}}
											BgL_s4z00_1414 =
												BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BgL_arg1336z00_1427, 16L);
										}
										{	/* Unsafe/sha2.scm 254 */
											long BgL_l4z00_1415;

											BgL_l4z00_1415 = STRING_LENGTH(BgL_s4z00_1414);
											{	/* Unsafe/sha2.scm 255 */

												blit_string(BgL_s1z00_1408, 0L, BgL_strz00_12,
													(BgL_offsetz00_13 +
														(4L - BgL_l1z00_1409)), BgL_l1z00_1409);
												blit_string(BgL_s2z00_1410, 0L, BgL_strz00_12,
													(BgL_offsetz00_13 +
														(4L + (4L - BgL_l2z00_1411))), BgL_l2z00_1411);
												blit_string(BgL_s3z00_1412, 0L, BgL_strz00_12,
													(BgL_offsetz00_13 +
														(8L + (4L - BgL_l3z00_1413))), BgL_l3z00_1413);
												return
													blit_string(BgL_s4z00_1414, 0L, BgL_strz00_12,
													(BgL_offsetz00_13 +
														(12L + (4L - BgL_l4z00_1415))), BgL_l4z00_1415);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* state->string */
	obj_t BGl_statezd2ze3stringz31zz__sha2z00(obj_t BgL_statez00_15)
	{
		{	/* Unsafe/sha2.scm 264 */
			{	/* Unsafe/sha2.scm 265 */
				obj_t BgL_rz00_1453;

				BgL_rz00_1453 = make_string(64L, ((unsigned char) '0'));
				{	/* Unsafe/sha2.scm 266 */
					uint32_t BgL_arg1365z00_1454;

					BgL_arg1365z00_1454 = BGL_U32VREF(BgL_statez00_15, 0L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 0L,
						BgL_arg1365z00_1454);
				}
				{	/* Unsafe/sha2.scm 267 */
					uint32_t BgL_arg1366z00_1455;

					BgL_arg1366z00_1455 = BGL_U32VREF(BgL_statez00_15, 1L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 8L,
						BgL_arg1366z00_1455);
				}
				{	/* Unsafe/sha2.scm 268 */
					uint32_t BgL_arg1367z00_1456;

					BgL_arg1367z00_1456 = BGL_U32VREF(BgL_statez00_15, 2L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 16L,
						BgL_arg1367z00_1456);
				}
				{	/* Unsafe/sha2.scm 269 */
					uint32_t BgL_arg1368z00_1457;

					BgL_arg1368z00_1457 = BGL_U32VREF(BgL_statez00_15, 3L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 24L,
						BgL_arg1368z00_1457);
				}
				{	/* Unsafe/sha2.scm 270 */
					uint32_t BgL_arg1369z00_1458;

					BgL_arg1369z00_1458 = BGL_U32VREF(BgL_statez00_15, 4L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 32L,
						BgL_arg1369z00_1458);
				}
				{	/* Unsafe/sha2.scm 271 */
					uint32_t BgL_arg1370z00_1459;

					BgL_arg1370z00_1459 = BGL_U32VREF(BgL_statez00_15, 5L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 40L,
						BgL_arg1370z00_1459);
				}
				{	/* Unsafe/sha2.scm 272 */
					uint32_t BgL_arg1371z00_1460;

					BgL_arg1371z00_1460 = BGL_U32VREF(BgL_statez00_15, 6L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 48L,
						BgL_arg1371z00_1460);
				}
				{	/* Unsafe/sha2.scm 273 */
					uint32_t BgL_arg1372z00_1461;

					BgL_arg1372z00_1461 = BGL_U32VREF(BgL_statez00_15, 7L);
					BGl_u32zd2fillz12zc0zz__sha2z00(BgL_rz00_1453, 56L,
						BgL_arg1372z00_1461);
				}
				return BgL_rz00_1453;
			}
		}

	}



/* state64->string */
	obj_t BGl_state64zd2ze3stringz31zz__sha2z00(obj_t BgL_statez00_16)
	{
		{	/* Unsafe/sha2.scm 279 */
			{	/* Unsafe/sha2.scm 280 */
				obj_t BgL_rz00_1462;

				BgL_rz00_1462 = make_string(128L, ((unsigned char) '0'));
				{	/* Unsafe/sha2.scm 281 */
					uint64_t BgL_arg1373z00_1463;

					BgL_arg1373z00_1463 = BGL_U64VREF(BgL_statez00_16, 0L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 0L,
						BgL_arg1373z00_1463);
				}
				{	/* Unsafe/sha2.scm 282 */
					uint64_t BgL_arg1375z00_1464;

					BgL_arg1375z00_1464 = BGL_U64VREF(BgL_statez00_16, 1L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 16L,
						BgL_arg1375z00_1464);
				}
				{	/* Unsafe/sha2.scm 283 */
					uint64_t BgL_arg1376z00_1465;

					BgL_arg1376z00_1465 = BGL_U64VREF(BgL_statez00_16, 2L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 32L,
						BgL_arg1376z00_1465);
				}
				{	/* Unsafe/sha2.scm 284 */
					uint64_t BgL_arg1377z00_1466;

					BgL_arg1377z00_1466 = BGL_U64VREF(BgL_statez00_16, 3L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 48L,
						BgL_arg1377z00_1466);
				}
				{	/* Unsafe/sha2.scm 285 */
					uint64_t BgL_arg1378z00_1467;

					BgL_arg1378z00_1467 = BGL_U64VREF(BgL_statez00_16, 4L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 64L,
						BgL_arg1378z00_1467);
				}
				{	/* Unsafe/sha2.scm 286 */
					uint64_t BgL_arg1379z00_1468;

					BgL_arg1379z00_1468 = BGL_U64VREF(BgL_statez00_16, 5L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 80L,
						BgL_arg1379z00_1468);
				}
				{	/* Unsafe/sha2.scm 287 */
					uint64_t BgL_arg1380z00_1469;

					BgL_arg1380z00_1469 = BGL_U64VREF(BgL_statez00_16, 6L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 96L,
						BgL_arg1380z00_1469);
				}
				{	/* Unsafe/sha2.scm 288 */
					uint64_t BgL_arg1382z00_1470;

					BgL_arg1382z00_1470 = BGL_U64VREF(BgL_statez00_16, 7L);
					BGl_u64zd2fillz12zc0zz__sha2z00(BgL_rz00_1462, 112L,
						BgL_arg1382z00_1470);
				}
				return BgL_rz00_1462;
			}
		}

	}



/* sha256-initial-hash-value */
	obj_t BGl_sha256zd2initialzd2hashzd2valuezd2zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 409 */
			{	/* Unsafe/sha2.scm 410 */
				obj_t BgL_vz00_1471;

				{	/* Llib/srfi4.scm 451 */

					BgL_vz00_1471 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(8L, (uint32_t) (0));
				}
				BGL_U32VSET(BgL_vz00_1471, 0L, (uint32_t) (1779033703));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 1L, (uint32_t) (3144134277));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 2L, (uint32_t) (1013904242));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 3L, (uint32_t) (2773480762));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 4L, (uint32_t) (1359893119));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 5L, (uint32_t) (2600822924));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 6L, (uint32_t) (528734635));
				BUNSPEC;
				BGL_U32VSET(BgL_vz00_1471, 7L, (uint32_t) (1541459225));
				BUNSPEC;
				return BgL_vz00_1471;
			}
		}

	}



/* sha512-initial-hash-value */
	obj_t BGl_sha512zd2initialzd2hashzd2valuezd2zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 424 */
			{	/* Unsafe/sha2.scm 425 */
				obj_t BgL_vz00_1474;

				{	/* Llib/srfi4.scm 453 */

					BgL_vz00_1474 =
						BGl_makezd2u64vectorzd2zz__srfi4z00(8L, (uint64_t) (0));
				}
				BGL_U64VSET(BgL_vz00_1474, 0L, (uint64_t) (7640891576956012808));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 1L, (uint64_t) (-4942790177534073029));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 2L, (uint64_t) (4354685564936845355));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 3L, (uint64_t) (-6534734903238641935));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 4L, (uint64_t) (5840696475078001361));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 5L, (uint64_t) (-7276294671716946913));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 6L, (uint64_t) (2270897969802886507));
				BUNSPEC;
				BGL_U64VSET(BgL_vz00_1474, 7L, (uint64_t) (6620516959819538809));
				BUNSPEC;
				return BgL_vz00_1474;
			}
		}

	}



/* Sigma1-256 */
	uint32_t BGl_Sigma1zd2256zd2zz__sha2z00(uint32_t BgL_xz00_19)
	{
		{	/* Unsafe/sha2.scm 539 */
			return
				(
				((BgL_xz00_19 >>
						(int) (6L)) |
					(BgL_xz00_19 <<
						(int) (
							(32L - 6L)))) ^
				(((BgL_xz00_19 >>
							(int) (11L)) |
						(BgL_xz00_19 <<
							(int) (
								(32L - 11L)))) ^
					((BgL_xz00_19 >>
							(int) (25L)) | (BgL_xz00_19 << (int) ((32L - 25L))))));
		}

	}



/* Sigma1-512 */
	uint64_t BGl_Sigma1zd2512zd2zz__sha2z00(uint64_t BgL_xz00_22)
	{
		{	/* Unsafe/sha2.scm 557 */
			return
				(
				((BgL_xz00_22 >>
						(int) (14L)) |
					(BgL_xz00_22 <<
						(int) (
							(64L - 14L)))) ^
				(((BgL_xz00_22 >>
							(int) (18L)) |
						(BgL_xz00_22 <<
							(int) (
								(64L - 18L)))) ^
					((BgL_xz00_22 >>
							(int) (41L)) | (BgL_xz00_22 << (int) ((64L - 41L))))));
		}

	}



/* sha256-internal-transform */
	obj_t BGl_sha256zd2internalzd2transformz00zz__sha2z00(obj_t BgL_statez00_33,
		obj_t BgL_bufferz00_34)
	{
		{	/* Unsafe/sha2.scm 587 */
			{
				obj_t BgL_statez00_1596;
				uint32_t BgL_az00_1597;
				uint32_t BgL_bz00_1598;
				uint32_t BgL_cz00_1599;
				uint32_t BgL_dz00_1600;
				uint32_t BgL_ez00_1601;
				uint32_t BgL_fz00_1602;
				uint32_t BgL_gz00_1603;
				uint32_t BgL_hz00_1604;

				{	/* Unsafe/sha2.scm 622 */
					uint32_t BgL_g1066z00_1514;
					uint32_t BgL_g1067z00_1515;
					uint32_t BgL_g1068z00_1516;
					uint32_t BgL_g1069z00_1517;
					uint32_t BgL_g1070z00_1518;
					uint32_t BgL_g1071z00_1519;
					uint32_t BgL_g1072z00_1520;
					uint32_t BgL_g1073z00_1521;

					BgL_g1066z00_1514 = BGL_U32VREF(BgL_statez00_33, 0L);
					BgL_g1067z00_1515 = BGL_U32VREF(BgL_statez00_33, 1L);
					BgL_g1068z00_1516 = BGL_U32VREF(BgL_statez00_33, 2L);
					BgL_g1069z00_1517 = BGL_U32VREF(BgL_statez00_33, 3L);
					BgL_g1070z00_1518 = BGL_U32VREF(BgL_statez00_33, 4L);
					BgL_g1071z00_1519 = BGL_U32VREF(BgL_statez00_33, 5L);
					BgL_g1072z00_1520 = BGL_U32VREF(BgL_statez00_33, 6L);
					BgL_g1073z00_1521 = BGL_U32VREF(BgL_statez00_33, 7L);
					{
						uint32_t BgL_az00_1523;
						uint32_t BgL_bz00_1524;
						uint32_t BgL_cz00_1525;
						uint32_t BgL_dz00_1526;
						uint32_t BgL_ez00_1527;
						uint32_t BgL_fz00_1528;
						uint32_t BgL_gz00_1529;
						uint32_t BgL_hz00_1530;
						long BgL_jz00_1531;

						BgL_az00_1523 = BgL_g1066z00_1514;
						BgL_bz00_1524 = BgL_g1067z00_1515;
						BgL_cz00_1525 = BgL_g1068z00_1516;
						BgL_dz00_1526 = BgL_g1069z00_1517;
						BgL_ez00_1527 = BgL_g1070z00_1518;
						BgL_fz00_1528 = BgL_g1071z00_1519;
						BgL_gz00_1529 = BgL_g1072z00_1520;
						BgL_hz00_1530 = BgL_g1073z00_1521;
						BgL_jz00_1531 = 0L;
					BgL_zc3z04anonymousza31419ze3z87_1532:
						if ((BgL_jz00_1531 < 16L))
							{	/* Unsafe/sha2.scm 633 */
								uint32_t BgL_wz00_1534;

								BgL_wz00_1534 = BGL_U32VREF(BgL_bufferz00_34, BgL_jz00_1531);
								{	/* Unsafe/sha2.scm 633 */
									uint32_t BgL_t1z00_1535;

									{	/* Unsafe/sha2.scm 596 */
										uint32_t BgL_arg1453z00_3386;

										{	/* Unsafe/sha2.scm 596 */
											uint32_t BgL_arg1454z00_3387;
											uint32_t BgL_arg1455z00_3388;

											BgL_arg1454z00_3387 =
												BGl_Sigma1zd2256zd2zz__sha2z00(BgL_ez00_1527);
											{	/* Unsafe/sha2.scm 596 */
												uint32_t BgL_arg1456z00_3389;
												uint32_t BgL_arg1457z00_3390;

												BgL_arg1456z00_3389 =
													(
													(BgL_ez00_1527 & BgL_fz00_1528) ^
													(~(BgL_ez00_1527) & BgL_gz00_1529));
												{	/* Unsafe/sha2.scm 596 */
													uint32_t BgL_arg1458z00_3391;

													BgL_arg1458z00_3391 =
														BGL_U32VREF(BGl_K256z00zz__sha2z00, BgL_jz00_1531);
													BgL_arg1457z00_3390 =
														BGl_addu32z00zz__sha2z00(BgL_arg1458z00_3391,
														BgL_wz00_1534);
												}
												BgL_arg1455z00_3388 =
													BGl_addu32z00zz__sha2z00(BgL_arg1456z00_3389,
													BgL_arg1457z00_3390);
											}
											BgL_arg1453z00_3386 =
												BGl_addu32z00zz__sha2z00(BgL_arg1454z00_3387,
												BgL_arg1455z00_3388);
										}
										BgL_t1z00_1535 =
											BGl_addu32z00zz__sha2z00(BgL_hz00_1530,
											BgL_arg1453z00_3386);
									}
									{	/* Unsafe/sha2.scm 634 */
										uint32_t BgL_t2z00_1536;

										BgL_t2z00_1536 =
											BGl_addu32z00zz__sha2z00(
											(((BgL_az00_1523 >>
														(int) (2L)) |
													(BgL_az00_1523 <<
														(int) (
															(32L - 2L)))) ^
												(((BgL_az00_1523 >>
															(int) (13L)) |
														(BgL_az00_1523 <<
															(int) (
																(32L - 13L)))) ^
													((BgL_az00_1523 >>
															(int) (22L)) |
														(BgL_az00_1523 <<
															(int) (
																(32L - 22L)))))),
											((BgL_az00_1523 & BgL_bz00_1524) ^
												((BgL_az00_1523 & BgL_cz00_1525) ^
													(BgL_bz00_1524 & BgL_cz00_1525))));
										{	/* Unsafe/sha2.scm 635 */

											{
												long BgL_jz00_5748;
												uint32_t BgL_hz00_5747;
												uint32_t BgL_gz00_5746;
												uint32_t BgL_fz00_5745;
												uint32_t BgL_ez00_5743;
												uint32_t BgL_dz00_5742;
												uint32_t BgL_cz00_5741;
												uint32_t BgL_bz00_5740;
												uint32_t BgL_az00_5738;

												BgL_az00_5738 =
													BGl_addu32z00zz__sha2z00(BgL_t1z00_1535,
													BgL_t2z00_1536);
												BgL_bz00_5740 = BgL_az00_1523;
												BgL_cz00_5741 = BgL_bz00_1524;
												BgL_dz00_5742 = BgL_cz00_1525;
												BgL_ez00_5743 =
													BGl_addu32z00zz__sha2z00(BgL_dz00_1526,
													BgL_t1z00_1535);
												BgL_fz00_5745 = BgL_ez00_1527;
												BgL_gz00_5746 = BgL_fz00_1528;
												BgL_hz00_5747 = BgL_gz00_1529;
												BgL_jz00_5748 = (BgL_jz00_1531 + 1L);
												BgL_jz00_1531 = BgL_jz00_5748;
												BgL_hz00_1530 = BgL_hz00_5747;
												BgL_gz00_1529 = BgL_gz00_5746;
												BgL_fz00_1528 = BgL_fz00_5745;
												BgL_ez00_1527 = BgL_ez00_5743;
												BgL_dz00_1526 = BgL_dz00_5742;
												BgL_cz00_1525 = BgL_cz00_5741;
												BgL_bz00_1524 = BgL_bz00_5740;
												BgL_az00_1523 = BgL_az00_5738;
												goto BgL_zc3z04anonymousza31419ze3z87_1532;
											}
										}
									}
								}
							}
						else
							{	/* Unsafe/sha2.scm 632 */
								if ((BgL_jz00_1531 < 64L))
									{	/* Unsafe/sha2.scm 638 */
										uint32_t BgL_s0z00_1543;

										{	/* Unsafe/sha2.scm 638 */
											uint32_t BgL_arg1441z00_1561;

											{	/* Unsafe/sha2.scm 638 */
												long BgL_arg1442z00_1562;

												BgL_arg1442z00_1562 = (BgL_jz00_1531 + 1L);
												{	/* Unsafe/sha2.scm 599 */
													long BgL_tmpz00_5753;

													BgL_tmpz00_5753 = (BgL_arg1442z00_1562 & 15L);
													BgL_arg1441z00_1561 =
														BGL_U32VREF(BgL_bufferz00_34, BgL_tmpz00_5753);
											}}
											BgL_s0z00_1543 =
												(
												((BgL_arg1441z00_1561 >>
														(int) (7L)) |
													(BgL_arg1441z00_1561 <<
														(int) (
															(32L - 7L)))) ^
												(((BgL_arg1441z00_1561 >>
															(int) (18L)) |
														(BgL_arg1441z00_1561 <<
															(int) (
																(32L - 18L)))) ^
													(BgL_arg1441z00_1561 >> (int) (3L))));
										}
										{	/* Unsafe/sha2.scm 638 */
											uint32_t BgL_s1z00_1544;

											{	/* Unsafe/sha2.scm 639 */
												uint32_t BgL_arg1439z00_1559;

												{	/* Unsafe/sha2.scm 639 */
													long BgL_arg1440z00_1560;

													BgL_arg1440z00_1560 = (BgL_jz00_1531 + 14L);
													{	/* Unsafe/sha2.scm 599 */
														long BgL_tmpz00_5773;

														BgL_tmpz00_5773 = (BgL_arg1440z00_1560 & 15L);
														BgL_arg1439z00_1559 =
															BGL_U32VREF(BgL_bufferz00_34, BgL_tmpz00_5773);
												}}
												BgL_s1z00_1544 =
													(
													((BgL_arg1439z00_1559 >>
															(int) (17L)) |
														(BgL_arg1439z00_1559 <<
															(int) (
																(32L - 17L)))) ^
													(((BgL_arg1439z00_1559 >>
																(int) (19L)) |
															(BgL_arg1439z00_1559 <<
																(int) (
																	(32L - 19L)))) ^
														(BgL_arg1439z00_1559 >> (int) (10L))));
											}
											{	/* Unsafe/sha2.scm 639 */
												long BgL_ndxz00_1545;

												BgL_ndxz00_1545 =
													(BgL_jz00_1531 &
													LLONG_TO_LONG(((BGL_LONGLONG_T) 15)));
												{	/* Unsafe/sha2.scm 640 */
													uint32_t BgL_wz00_1546;

													{	/* Unsafe/sha2.scm 641 */
														uint32_t BgL_arg1434z00_1554;
														uint32_t BgL_arg1435z00_1555;

														{	/* Unsafe/sha2.scm 599 */
															long BgL_tmpz00_5794;

															BgL_tmpz00_5794 = (BgL_jz00_1531 & 15L);
															BgL_arg1434z00_1554 =
																BGL_U32VREF(BgL_bufferz00_34, BgL_tmpz00_5794);
														}
														{	/* Unsafe/sha2.scm 641 */
															uint32_t BgL_arg1436z00_1556;

															{	/* Unsafe/sha2.scm 641 */
																uint32_t BgL_arg1437z00_1557;

																{	/* Unsafe/sha2.scm 641 */
																	long BgL_arg1438z00_1558;

																	BgL_arg1438z00_1558 = (BgL_jz00_1531 + 9L);
																	{	/* Unsafe/sha2.scm 599 */
																		long BgL_tmpz00_5798;

																		BgL_tmpz00_5798 =
																			(BgL_arg1438z00_1558 & 15L);
																		BgL_arg1437z00_1557 =
																			BGL_U32VREF(BgL_bufferz00_34,
																			BgL_tmpz00_5798);
																}}
																BgL_arg1436z00_1556 =
																	BGl_addu32z00zz__sha2z00(BgL_arg1437z00_1557,
																	BgL_s0z00_1543);
															}
															BgL_arg1435z00_1555 =
																BGl_addu32z00zz__sha2z00(BgL_s1z00_1544,
																BgL_arg1436z00_1556);
														}
														BgL_wz00_1546 =
															BGl_addu32z00zz__sha2z00(BgL_arg1434z00_1554,
															BgL_arg1435z00_1555);
													}
													{	/* Unsafe/sha2.scm 641 */
														uint32_t BgL_t1z00_1547;

														{	/* Unsafe/sha2.scm 596 */
															uint32_t BgL_arg1453z00_3538;

															{	/* Unsafe/sha2.scm 596 */
																uint32_t BgL_arg1454z00_3539;
																uint32_t BgL_arg1455z00_3540;

																BgL_arg1454z00_3539 =
																	BGl_Sigma1zd2256zd2zz__sha2z00(BgL_ez00_1527);
																{	/* Unsafe/sha2.scm 596 */
																	uint32_t BgL_arg1456z00_3541;
																	uint32_t BgL_arg1457z00_3542;

																	BgL_arg1456z00_3541 =
																		(
																		(BgL_ez00_1527 & BgL_fz00_1528) ^
																		(~(BgL_ez00_1527) & BgL_gz00_1529));
																	{	/* Unsafe/sha2.scm 596 */
																		uint32_t BgL_arg1458z00_3543;

																		BgL_arg1458z00_3543 =
																			BGL_U32VREF(BGl_K256z00zz__sha2z00,
																			BgL_jz00_1531);
																		BgL_arg1457z00_3542 =
																			BGl_addu32z00zz__sha2z00
																			(BgL_arg1458z00_3543, BgL_wz00_1546);
																	}
																	BgL_arg1455z00_3540 =
																		BGl_addu32z00zz__sha2z00
																		(BgL_arg1456z00_3541, BgL_arg1457z00_3542);
																}
																BgL_arg1453z00_3538 =
																	BGl_addu32z00zz__sha2z00(BgL_arg1454z00_3539,
																	BgL_arg1455z00_3540);
															}
															BgL_t1z00_1547 =
																BGl_addu32z00zz__sha2z00(BgL_hz00_1530,
																BgL_arg1453z00_3538);
														}
														{	/* Unsafe/sha2.scm 642 */
															uint32_t BgL_t2z00_1548;

															BgL_t2z00_1548 =
																BGl_addu32z00zz__sha2z00(
																(((BgL_az00_1523 >>
																			(int) (2L)) |
																		(BgL_az00_1523 <<
																			(int) (
																				(32L - 2L)))) ^
																	(((BgL_az00_1523 >>
																				(int) (13L)) |
																			(BgL_az00_1523 <<
																				(int) (
																					(32L - 13L)))) ^
																		((BgL_az00_1523 >>
																				(int) (22L)) |
																			(BgL_az00_1523 <<
																				(int) (
																					(32L - 22L)))))),
																((BgL_az00_1523 & BgL_bz00_1524) ^
																	((BgL_az00_1523 & BgL_cz00_1525) ^
																		(BgL_bz00_1524 & BgL_cz00_1525))));
															{	/* Unsafe/sha2.scm 643 */

																BGL_U32VSET(BgL_bufferz00_34, BgL_ndxz00_1545,
																	BgL_wz00_1546);
																BUNSPEC;
																{
																	long BgL_jz00_5851;
																	uint32_t BgL_hz00_5850;
																	uint32_t BgL_gz00_5849;
																	uint32_t BgL_fz00_5848;
																	uint32_t BgL_ez00_5846;
																	uint32_t BgL_dz00_5845;
																	uint32_t BgL_cz00_5844;
																	uint32_t BgL_bz00_5843;
																	uint32_t BgL_az00_5841;

																	BgL_az00_5841 =
																		BGl_addu32z00zz__sha2z00(BgL_t1z00_1547,
																		BgL_t2z00_1548);
																	BgL_bz00_5843 = BgL_az00_1523;
																	BgL_cz00_5844 = BgL_bz00_1524;
																	BgL_dz00_5845 = BgL_cz00_1525;
																	BgL_ez00_5846 =
																		BGl_addu32z00zz__sha2z00(BgL_dz00_1526,
																		BgL_t1z00_1547);
																	BgL_fz00_5848 = BgL_ez00_1527;
																	BgL_gz00_5849 = BgL_fz00_1528;
																	BgL_hz00_5850 = BgL_gz00_1529;
																	BgL_jz00_5851 = (BgL_jz00_1531 + 1L);
																	BgL_jz00_1531 = BgL_jz00_5851;
																	BgL_hz00_1530 = BgL_hz00_5850;
																	BgL_gz00_1529 = BgL_gz00_5849;
																	BgL_fz00_1528 = BgL_fz00_5848;
																	BgL_ez00_1527 = BgL_ez00_5846;
																	BgL_dz00_1526 = BgL_dz00_5845;
																	BgL_cz00_1525 = BgL_cz00_5844;
																	BgL_bz00_1524 = BgL_bz00_5843;
																	BgL_az00_1523 = BgL_az00_5841;
																	goto BgL_zc3z04anonymousza31419ze3z87_1532;
																}
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/sha2.scm 637 */
										BgL_statez00_1596 = BgL_statez00_33;
										BgL_az00_1597 = BgL_az00_1523;
										BgL_bz00_1598 = BgL_bz00_1524;
										BgL_cz00_1599 = BgL_cz00_1525;
										BgL_dz00_1600 = BgL_dz00_1526;
										BgL_ez00_1601 = BgL_ez00_1527;
										BgL_fz00_1602 = BgL_fz00_1528;
										BgL_gz00_1603 = BgL_gz00_1529;
										BgL_hz00_1604 = BgL_hz00_1530;
										{	/* Unsafe/sha2.scm 604 */
											uint32_t BgL_oaz00_1606;
											uint32_t BgL_obz00_1607;
											uint32_t BgL_ocz00_1608;
											uint32_t BgL_odz00_1609;
											uint32_t BgL_oez00_1610;
											uint32_t BgL_ofz00_1611;
											uint32_t BgL_ogz00_1612;
											uint32_t BgL_ohz00_1613;

											BgL_oaz00_1606 = BGL_U32VREF(BgL_statez00_1596, 0L);
											BgL_obz00_1607 = BGL_U32VREF(BgL_statez00_1596, 1L);
											BgL_ocz00_1608 = BGL_U32VREF(BgL_statez00_1596, 2L);
											BgL_odz00_1609 = BGL_U32VREF(BgL_statez00_1596, 3L);
											BgL_oez00_1610 = BGL_U32VREF(BgL_statez00_1596, 4L);
											BgL_ofz00_1611 = BGL_U32VREF(BgL_statez00_1596, 5L);
											BgL_ogz00_1612 = BGL_U32VREF(BgL_statez00_1596, 6L);
											BgL_ohz00_1613 = BGL_U32VREF(BgL_statez00_1596, 7L);
											{	/* Unsafe/sha2.scm 613 */
												uint32_t BgL_arg1462z00_1615;

												BgL_arg1462z00_1615 =
													BGl_addu32z00zz__sha2z00(BgL_oaz00_1606,
													BgL_az00_1597);
												BGL_U32VSET(BgL_statez00_1596, 0L, BgL_arg1462z00_1615);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 614 */
												uint32_t BgL_arg1463z00_1616;

												BgL_arg1463z00_1616 =
													BGl_addu32z00zz__sha2z00(BgL_obz00_1607,
													BgL_bz00_1598);
												BGL_U32VSET(BgL_statez00_1596, 1L, BgL_arg1463z00_1616);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 615 */
												uint32_t BgL_arg1464z00_1617;

												BgL_arg1464z00_1617 =
													BGl_addu32z00zz__sha2z00(BgL_ocz00_1608,
													BgL_cz00_1599);
												BGL_U32VSET(BgL_statez00_1596, 2L, BgL_arg1464z00_1617);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 616 */
												uint32_t BgL_arg1465z00_1618;

												BgL_arg1465z00_1618 =
													BGl_addu32z00zz__sha2z00(BgL_odz00_1609,
													BgL_dz00_1600);
												BGL_U32VSET(BgL_statez00_1596, 3L, BgL_arg1465z00_1618);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 617 */
												uint32_t BgL_arg1466z00_1619;

												BgL_arg1466z00_1619 =
													BGl_addu32z00zz__sha2z00(BgL_oez00_1610,
													BgL_ez00_1601);
												BGL_U32VSET(BgL_statez00_1596, 4L, BgL_arg1466z00_1619);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 618 */
												uint32_t BgL_arg1467z00_1620;

												BgL_arg1467z00_1620 =
													BGl_addu32z00zz__sha2z00(BgL_ofz00_1611,
													BgL_fz00_1602);
												BGL_U32VSET(BgL_statez00_1596, 5L, BgL_arg1467z00_1620);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 619 */
												uint32_t BgL_arg1468z00_1621;

												BgL_arg1468z00_1621 =
													BGl_addu32z00zz__sha2z00(BgL_ogz00_1612,
													BgL_gz00_1603);
												BGL_U32VSET(BgL_statez00_1596, 6L, BgL_arg1468z00_1621);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 620 */
												uint32_t BgL_arg1469z00_1622;

												BgL_arg1469z00_1622 =
													BGl_addu32z00zz__sha2z00(BgL_ohz00_1613,
													BgL_hz00_1604);
												BGL_U32VSET(BgL_statez00_1596, 7L, BgL_arg1469z00_1622);
												BUNSPEC;
											}
										}
										return BgL_statez00_33;
									}
							}
					}
				}
			}
		}

	}



/* sha256-update */
	obj_t BGl_sha256zd2updatezd2zz__sha2z00(obj_t BgL_statez00_35,
		obj_t BgL_bufferz00_36, obj_t BgL_oz00_37, obj_t BgL_fillzd2wordz12zc0_38)
	{
		{	/* Unsafe/sha2.scm 653 */
			{
				obj_t BgL_bufferz00_1648;
				long BgL_iz00_1649;

				{
					long BgL_iz00_1631;
					long BgL_lz00_1632;

					BgL_iz00_1631 = 0L;
					BgL_lz00_1632 = 0L;
				BgL_zc3z04anonymousza31470ze3z87_1633:
					{	/* Unsafe/sha2.scm 673 */
						long BgL_bytesz00_1634;

						BgL_bufferz00_1648 = BgL_bufferz00_36;
						BgL_iz00_1649 = BgL_iz00_1631;
						{
							long BgL_jz00_3617;
							long BgL_iz00_3618;
							long BgL_nz00_3619;

							BgL_jz00_3617 = 0L;
							BgL_iz00_3618 = BgL_iz00_1649;
							BgL_nz00_3619 = 0L;
						BgL_loopz00_3616:
							if ((BgL_jz00_3617 < 16L))
								{	/* Unsafe/sha2.scm 662 */
									long BgL_arg1484z00_3622;
									long BgL_arg1485z00_3623;
									long BgL_arg1486z00_3624;

									BgL_arg1484z00_3622 = (BgL_jz00_3617 + 1L);
									BgL_arg1485z00_3623 = (BgL_iz00_3618 + 4L);
									{	/* Unsafe/sha2.scm 662 */
										long BgL_b1146z00_3627;

										BgL_b1146z00_3627 =
											((long (*)(obj_t, obj_t, long, obj_t,
													long))
											PROCEDURE_L_ENTRY(BgL_fillzd2wordz12zc0_38))
											(BgL_fillzd2wordz12zc0_38, BgL_bufferz00_1648,
											BgL_jz00_3617, BgL_oz00_37, BgL_iz00_3618);
										BgL_arg1486z00_3624 = (BgL_nz00_3619 + BgL_b1146z00_3627);
									}
									{
										long BgL_nz00_5891;
										long BgL_iz00_5890;
										long BgL_jz00_5889;

										BgL_jz00_5889 = BgL_arg1484z00_3622;
										BgL_iz00_5890 = BgL_arg1485z00_3623;
										BgL_nz00_5891 = BgL_arg1486z00_3624;
										BgL_nz00_3619 = BgL_nz00_5891;
										BgL_iz00_3618 = BgL_iz00_5890;
										BgL_jz00_3617 = BgL_jz00_5889;
										goto BgL_loopz00_3616;
									}
								}
							else
								{	/* Unsafe/sha2.scm 661 */
									BgL_bytesz00_1634 = BgL_nz00_3619;
								}
						}
						if ((BgL_bytesz00_1634 == 64L))
							{	/* Unsafe/sha2.scm 675 */
								BGl_sha256zd2internalzd2transformz00zz__sha2z00(BgL_statez00_35,
									BgL_bufferz00_36);
								{
									long BgL_lz00_5897;
									long BgL_iz00_5895;

									BgL_iz00_5895 = (BgL_iz00_1631 + 64L);
									BgL_lz00_5897 = (BgL_lz00_1632 + 64L);
									BgL_lz00_1632 = BgL_lz00_5897;
									BgL_iz00_1631 = BgL_iz00_5895;
									goto BgL_zc3z04anonymousza31470ze3z87_1633;
								}
							}
						else
							{	/* Unsafe/sha2.scm 675 */
								if (((64L - BgL_bytesz00_1634) >= 8L))
									{	/* Unsafe/sha2.scm 679 */
										{	/* Unsafe/sha2.scm 682 */
											uint32_t BgL_ulenz00_1640;

											{	/* Unsafe/sha2.scm 682 */
												long BgL_tmpz00_5902;

												BgL_tmpz00_5902 =
													(8L * ((BgL_lz00_1632 - 1L) + BgL_bytesz00_1634));
												BgL_ulenz00_1640 = (uint32_t) (BgL_tmpz00_5902);
											}
											BGL_U32VSET(BgL_bufferz00_36, 15L, BgL_ulenz00_1640);
											BUNSPEC;
										}
										return
											BGl_sha256zd2internalzd2transformz00zz__sha2z00
											(BgL_statez00_35, BgL_bufferz00_36);
									}
								else
									{	/* Unsafe/sha2.scm 679 */
										BGl_sha256zd2internalzd2transformz00zz__sha2z00
											(BgL_statez00_35, BgL_bufferz00_36);
										{
											long BgL_iz00_3655;

											BgL_iz00_3655 = 0L;
										BgL_loopz00_3654:
											if ((BgL_iz00_3655 < 15L))
												{	/* Unsafe/sha2.scm 667 */
													BGL_U32VSET(BgL_bufferz00_36, BgL_iz00_3655,
														(uint32_t) (0));
													BUNSPEC;
													{
														long BgL_iz00_5913;

														BgL_iz00_5913 = (BgL_iz00_3655 + 1L);
														BgL_iz00_3655 = BgL_iz00_5913;
														goto BgL_loopz00_3654;
													}
												}
											else
												{	/* Unsafe/sha2.scm 667 */
													((bool_t) 0);
												}
										}
										{	/* Unsafe/sha2.scm 689 */
											uint32_t BgL_ulenz00_1643;

											{	/* Unsafe/sha2.scm 689 */
												long BgL_tmpz00_5915;

												BgL_tmpz00_5915 =
													(8L * ((BgL_lz00_1632 - 1L) + BgL_bytesz00_1634));
												BgL_ulenz00_1643 = (uint32_t) (BgL_tmpz00_5915);
											}
											BGL_U32VSET(BgL_bufferz00_36, 15L, BgL_ulenz00_1643);
											BUNSPEC;
										}
										return
											BGl_sha256zd2internalzd2transformz00zz__sha2z00
											(BgL_statez00_35, BgL_bufferz00_36);
									}
							}
					}
				}
			}
		}

	}



/* sha256sum-mmap */
	BGL_EXPORTED_DEF obj_t BGl_sha256sumzd2mmapzd2zz__sha2z00(obj_t BgL_mmz00_39)
	{
		{	/* Unsafe/sha2.scm 696 */
			{	/* Unsafe/sha2.scm 733 */
				obj_t BgL_statez00_1677;
				obj_t BgL_bufferz00_1678;

				BgL_statez00_1677 = BGl_sha256zd2initialzd2hashzd2valuezd2zz__sha2z00();
				{	/* Llib/srfi4.scm 451 */

					BgL_bufferz00_1678 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(16L, (uint32_t) (0));
				}
				BGl_sha256zd2updatezd2zz__sha2z00(BgL_statez00_1677, BgL_bufferz00_1678,
					BgL_mmz00_39, BGl_proc2377z00zz__sha2z00);
				return BGl_statezd2ze3stringz31zz__sha2z00(BgL_statez00_1677);
			}
		}

	}



/* &sha256sum-mmap */
	obj_t BGl_z62sha256sumzd2mmapzb0zz__sha2z00(obj_t BgL_envz00_4863,
		obj_t BgL_mmz00_4864)
	{
		{	/* Unsafe/sha2.scm 696 */
			{	/* Unsafe/sha2.scm 699 */
				obj_t BgL_auxz00_5926;

				if (BGL_MMAPP(BgL_mmz00_4864))
					{	/* Unsafe/sha2.scm 699 */
						BgL_auxz00_5926 = BgL_mmz00_4864;
					}
				else
					{
						obj_t BgL_auxz00_5929;

						BgL_auxz00_5929 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(31200L), BGl_string2379z00zz__sha2z00,
							BGl_string2380z00zz__sha2z00, BgL_mmz00_4864);
						FAILURE(BgL_auxz00_5929, BFALSE, BFALSE);
					}
				return BGl_sha256sumzd2mmapzd2zz__sha2z00(BgL_auxz00_5926);
			}
		}

	}



/* &fill-word32-mmap! */
	long BGl_z62fillzd2word32zd2mmapz12z70zz__sha2z00(obj_t BgL_envz00_4865,
		obj_t BgL_v32z00_4866, long BgL_iz00_4867, obj_t BgL_mmz00_4868,
		long BgL_nz00_4869)
	{
		{	/* Unsafe/sha2.scm 731 */
			{	/* Unsafe/sha2.scm 702 */
				long BgL_lz00_4975;

				{	/* Unsafe/sha2.scm 702 */
					obj_t BgL_tmpz00_5934;

					BgL_tmpz00_5934 = ((obj_t) BgL_mmz00_4868);
					BgL_lz00_4975 = BGL_MMAP_LENGTH(BgL_tmpz00_5934);
				}
				{	/* Unsafe/sha2.scm 704 */
					bool_t BgL_test2439z00_5937;

					{	/* Unsafe/sha2.scm 704 */
						long BgL_n2z00_4976;

						BgL_n2z00_4976 = (long) (BgL_lz00_4975);
						BgL_test2439z00_5937 = ((BgL_nz00_4869 + 4L) <= BgL_n2z00_4976);
					}
					if (BgL_test2439z00_5937)
						{	/* Unsafe/sha2.scm 705 */
							uint32_t BgL_v0z00_4977;

							{	/* Unsafe/sha2.scm 699 */
								unsigned char BgL_arg1494z00_4978;

								{	/* Unsafe/sha2.scm 699 */
									long BgL_auxz00_5943;
									obj_t BgL_tmpz00_5941;

									BgL_auxz00_5943 = (long) (BgL_nz00_4869);
									BgL_tmpz00_5941 = ((obj_t) BgL_mmz00_4868);
									BgL_arg1494z00_4978 =
										BGL_MMAP_REF(BgL_tmpz00_5941, BgL_auxz00_5943);
								}
								{	/* Unsafe/sha2.scm 699 */
									long BgL_tmpz00_5946;

									BgL_tmpz00_5946 = (BgL_arg1494z00_4978);
									BgL_v0z00_4977 = (uint32_t) (BgL_tmpz00_5946);
							}}
							{	/* Unsafe/sha2.scm 705 */
								uint32_t BgL_v1z00_4979;

								{	/* Unsafe/sha2.scm 706 */
									long BgL_arg1513z00_4980;

									BgL_arg1513z00_4980 = (BgL_nz00_4869 + 1L);
									{	/* Unsafe/sha2.scm 699 */
										unsigned char BgL_arg1494z00_4981;

										{	/* Unsafe/sha2.scm 699 */
											long BgL_auxz00_5952;
											obj_t BgL_tmpz00_5950;

											BgL_auxz00_5952 = (long) (BgL_arg1513z00_4980);
											BgL_tmpz00_5950 = ((obj_t) BgL_mmz00_4868);
											BgL_arg1494z00_4981 =
												BGL_MMAP_REF(BgL_tmpz00_5950, BgL_auxz00_5952);
										}
										{	/* Unsafe/sha2.scm 699 */
											long BgL_tmpz00_5955;

											BgL_tmpz00_5955 = (BgL_arg1494z00_4981);
											BgL_v1z00_4979 = (uint32_t) (BgL_tmpz00_5955);
								}}}
								{	/* Unsafe/sha2.scm 706 */
									uint32_t BgL_v2z00_4982;

									{	/* Unsafe/sha2.scm 707 */
										long BgL_arg1511z00_4983;

										BgL_arg1511z00_4983 = (BgL_nz00_4869 + 2L);
										{	/* Unsafe/sha2.scm 699 */
											unsigned char BgL_arg1494z00_4984;

											{	/* Unsafe/sha2.scm 699 */
												long BgL_auxz00_5961;
												obj_t BgL_tmpz00_5959;

												BgL_auxz00_5961 = (long) (BgL_arg1511z00_4983);
												BgL_tmpz00_5959 = ((obj_t) BgL_mmz00_4868);
												BgL_arg1494z00_4984 =
													BGL_MMAP_REF(BgL_tmpz00_5959, BgL_auxz00_5961);
											}
											{	/* Unsafe/sha2.scm 699 */
												long BgL_tmpz00_5964;

												BgL_tmpz00_5964 = (BgL_arg1494z00_4984);
												BgL_v2z00_4982 = (uint32_t) (BgL_tmpz00_5964);
									}}}
									{	/* Unsafe/sha2.scm 707 */
										uint32_t BgL_v3z00_4985;

										{	/* Unsafe/sha2.scm 708 */
											long BgL_arg1510z00_4986;

											BgL_arg1510z00_4986 = (BgL_nz00_4869 + 3L);
											{	/* Unsafe/sha2.scm 699 */
												unsigned char BgL_arg1494z00_4987;

												{	/* Unsafe/sha2.scm 699 */
													long BgL_auxz00_5970;
													obj_t BgL_tmpz00_5968;

													BgL_auxz00_5970 = (long) (BgL_arg1510z00_4986);
													BgL_tmpz00_5968 = ((obj_t) BgL_mmz00_4868);
													BgL_arg1494z00_4987 =
														BGL_MMAP_REF(BgL_tmpz00_5968, BgL_auxz00_5970);
												}
												{	/* Unsafe/sha2.scm 699 */
													long BgL_tmpz00_5973;

													BgL_tmpz00_5973 = (BgL_arg1494z00_4987);
													BgL_v3z00_4985 = (uint32_t) (BgL_tmpz00_5973);
										}}}
										{	/* Unsafe/sha2.scm 708 */
											uint32_t BgL_vz00_4988;

											{	/* Unsafe/sha2.scm 709 */
												uint32_t BgL_arg1499z00_4989;
												uint32_t BgL_arg1500z00_4990;

												{	/* Unsafe/sha2.scm 709 */
													uint32_t BgL_arg1501z00_4991;

													{	/* Unsafe/sha2.scm 709 */
														long BgL_arg1502z00_4992;

														{	/* Unsafe/sha2.scm 709 */
															long BgL_arg1503z00_4993;
															long BgL_arg1504z00_4994;

															{	/* Unsafe/sha2.scm 709 */
																uint32_t BgL_tmpz00_5976;

																BgL_tmpz00_5976 =
																	(BgL_v0z00_4977 << (int) (8L));
																BgL_arg1503z00_4993 = (long) (BgL_tmpz00_5976);
															}
															BgL_arg1504z00_4994 = (long) (BgL_v1z00_4979);
															BgL_arg1502z00_4992 =
																(BgL_arg1503z00_4993 | BgL_arg1504z00_4994);
														}
														BgL_arg1501z00_4991 =
															(uint32_t) (BgL_arg1502z00_4992);
													}
													BgL_arg1499z00_4989 =
														(BgL_arg1501z00_4991 << (int) (16L));
												}
												{	/* Unsafe/sha2.scm 709 */
													long BgL_arg1506z00_4995;

													{	/* Unsafe/sha2.scm 709 */
														long BgL_arg1507z00_4996;
														long BgL_arg1508z00_4997;

														{	/* Unsafe/sha2.scm 709 */
															uint32_t BgL_tmpz00_5985;

															BgL_tmpz00_5985 = (BgL_v2z00_4982 << (int) (8L));
															BgL_arg1507z00_4996 = (long) (BgL_tmpz00_5985);
														}
														BgL_arg1508z00_4997 = (long) (BgL_v3z00_4985);
														BgL_arg1506z00_4995 =
															(BgL_arg1507z00_4996 | BgL_arg1508z00_4997);
													}
													BgL_arg1500z00_4990 =
														(uint32_t) (BgL_arg1506z00_4995);
												}
												BgL_vz00_4988 =
													(BgL_arg1499z00_4989 | BgL_arg1500z00_4990);
											}
											{	/* Unsafe/sha2.scm 709 */

												BGL_U32VSET(BgL_v32z00_4866, BgL_iz00_4867,
													BgL_vz00_4988);
												BUNSPEC;
												return 4L;
											}
										}
									}
								}
							}
						}
					else
						{	/* Unsafe/sha2.scm 712 */
							bool_t BgL_test2440z00_5994;

							{	/* Unsafe/sha2.scm 712 */
								long BgL_tmpz00_5995;

								{	/* Unsafe/sha2.scm 712 */
									long BgL_za72za7_4998;

									BgL_za72za7_4998 = (long) (BgL_lz00_4975);
									BgL_tmpz00_5995 = (1L + BgL_za72za7_4998);
								}
								BgL_test2440z00_5994 = (BgL_nz00_4869 >= BgL_tmpz00_5995);
							}
							if (BgL_test2440z00_5994)
								{	/* Unsafe/sha2.scm 712 */
									{	/* Unsafe/sha2.scm 713 */
										uint32_t BgL_tmpz00_5999;

										BgL_tmpz00_5999 = (uint32_t) (0L);
										BGL_U32VSET(BgL_v32z00_4866, BgL_iz00_4867,
											BgL_tmpz00_5999);
									}
									BUNSPEC;
									return 0L;
								}
							else
								{	/* Unsafe/sha2.scm 716 */
									obj_t BgL_vz00_4999;
									long BgL_kz00_5000;

									BgL_vz00_4999 =
										BGl_makezd2u32vectorzd2zz__srfi4z00(4L, (uint32_t) (0L));
									{	/* Unsafe/sha2.scm 717 */
										long BgL_tmpz00_6004;

										{	/* Unsafe/sha2.scm 717 */
											long BgL_za72za7_5001;

											BgL_za72za7_5001 = (long) (BgL_lz00_4975);
											BgL_tmpz00_6004 =
												((BgL_nz00_4869 + 4L) - BgL_za72za7_5001);
										}
										BgL_kz00_5000 = (4L - BgL_tmpz00_6004);
									}
									{
										long BgL_jz00_5003;

										BgL_jz00_5003 = 0L;
									BgL_loopz00_5002:
										if ((BgL_jz00_5003 == BgL_kz00_5000))
											{	/* Unsafe/sha2.scm 719 */
												{	/* Unsafe/sha2.scm 721 */
													uint32_t BgL_tmpz00_6011;

													BgL_tmpz00_6011 = (uint32_t) (128L);
													BGL_U32VSET(BgL_vz00_4999, BgL_jz00_5003,
														BgL_tmpz00_6011);
												}
												BUNSPEC;
												{	/* Unsafe/sha2.scm 722 */
													uint32_t BgL_v0z00_5004;

													BgL_v0z00_5004 = BGL_U32VREF(BgL_vz00_4999, 0L);
													{	/* Unsafe/sha2.scm 722 */
														uint32_t BgL_v1z00_5005;

														BgL_v1z00_5005 = BGL_U32VREF(BgL_vz00_4999, 1L);
														{	/* Unsafe/sha2.scm 723 */
															uint32_t BgL_v2z00_5006;

															BgL_v2z00_5006 = BGL_U32VREF(BgL_vz00_4999, 2L);
															{	/* Unsafe/sha2.scm 724 */
																uint32_t BgL_v3z00_5007;

																BgL_v3z00_5007 = BGL_U32VREF(BgL_vz00_4999, 3L);
																{	/* Unsafe/sha2.scm 725 */
																	uint32_t BgL_vz00_5008;

																	{	/* Unsafe/sha2.scm 726 */
																		uint32_t BgL_arg1521z00_5009;
																		uint32_t BgL_arg1522z00_5010;

																		{	/* Unsafe/sha2.scm 726 */
																			uint32_t BgL_arg1523z00_5011;

																			{	/* Unsafe/sha2.scm 726 */
																				long BgL_arg1524z00_5012;

																				{	/* Unsafe/sha2.scm 726 */
																					long BgL_arg1525z00_5013;
																					long BgL_arg1526z00_5014;

																					{	/* Unsafe/sha2.scm 726 */
																						uint32_t BgL_tmpz00_6018;

																						BgL_tmpz00_6018 =
																							(BgL_v0z00_5004 << (int) (8L));
																						BgL_arg1525z00_5013 =
																							(long) (BgL_tmpz00_6018);
																					}
																					BgL_arg1526z00_5014 =
																						(long) (BgL_v1z00_5005);
																					BgL_arg1524z00_5012 =
																						(BgL_arg1525z00_5013 |
																						BgL_arg1526z00_5014);
																				}
																				BgL_arg1523z00_5011 =
																					(uint32_t) (BgL_arg1524z00_5012);
																			}
																			BgL_arg1521z00_5009 =
																				(BgL_arg1523z00_5011 << (int) (16L));
																		}
																		{	/* Unsafe/sha2.scm 726 */
																			long BgL_arg1528z00_5015;

																			{	/* Unsafe/sha2.scm 726 */
																				long BgL_arg1529z00_5016;
																				long BgL_arg1530z00_5017;

																				{	/* Unsafe/sha2.scm 726 */
																					uint32_t BgL_tmpz00_6027;

																					BgL_tmpz00_6027 =
																						(BgL_v2z00_5006 << (int) (8L));
																					BgL_arg1529z00_5016 =
																						(long) (BgL_tmpz00_6027);
																				}
																				BgL_arg1530z00_5017 =
																					(long) (BgL_v3z00_5007);
																				BgL_arg1528z00_5015 =
																					(BgL_arg1529z00_5016 |
																					BgL_arg1530z00_5017);
																			}
																			BgL_arg1522z00_5010 =
																				(uint32_t) (BgL_arg1528z00_5015);
																		}
																		BgL_vz00_5008 =
																			(BgL_arg1521z00_5009 |
																			BgL_arg1522z00_5010);
																	}
																	{	/* Unsafe/sha2.scm 726 */

																		BGL_U32VSET(BgL_v32z00_4866, BgL_iz00_4867,
																			BgL_vz00_5008);
																		BUNSPEC;
																		return (BgL_jz00_5003 + 1L);
																	}
																}
															}
														}
													}
												}
											}
										else
											{	/* Unsafe/sha2.scm 719 */
												{	/* Unsafe/sha2.scm 730 */
													long BgL_arg1535z00_5018;

													{	/* Unsafe/sha2.scm 730 */
														long BgL_arg1536z00_5019;

														BgL_arg1536z00_5019 =
															(BgL_nz00_4869 + BgL_jz00_5003);
														{	/* Unsafe/sha2.scm 699 */
															unsigned char BgL_arg1494z00_5020;

															{	/* Unsafe/sha2.scm 699 */
																long BgL_auxz00_6040;
																obj_t BgL_tmpz00_6038;

																BgL_auxz00_6040 = (long) (BgL_arg1536z00_5019);
																BgL_tmpz00_6038 = ((obj_t) BgL_mmz00_4868);
																BgL_arg1494z00_5020 =
																	BGL_MMAP_REF(BgL_tmpz00_6038,
																	BgL_auxz00_6040);
															}
															BgL_arg1535z00_5018 = (BgL_arg1494z00_5020);
													}}
													{	/* Unsafe/sha2.scm 730 */
														uint32_t BgL_tmpz00_6044;

														BgL_tmpz00_6044 = (uint32_t) (BgL_arg1535z00_5018);
														BGL_U32VSET(BgL_vz00_4999, BgL_jz00_5003,
															BgL_tmpz00_6044);
													} BUNSPEC;
												}
												{
													long BgL_jz00_6047;

													BgL_jz00_6047 = (BgL_jz00_5003 + 1L);
													BgL_jz00_5003 = BgL_jz00_6047;
													goto BgL_loopz00_5002;
												}
											}
									}
								}
						}
				}
			}
		}

	}



/* sha256sum-string */
	BGL_EXPORTED_DEF obj_t BGl_sha256sumzd2stringzd2zz__sha2z00(obj_t
		BgL_strz00_40)
	{
		{	/* Unsafe/sha2.scm 741 */
			{	/* Unsafe/sha2.scm 778 */
				obj_t BgL_statez00_1749;
				obj_t BgL_bufferz00_1750;

				BgL_statez00_1749 = BGl_sha256zd2initialzd2hashzd2valuezd2zz__sha2z00();
				{	/* Llib/srfi4.scm 451 */

					BgL_bufferz00_1750 =
						BGl_makezd2u32vectorzd2zz__srfi4z00(16L, (uint32_t) (0));
				}
				BGl_sha256zd2updatezd2zz__sha2z00(BgL_statez00_1749, BgL_bufferz00_1750,
					BgL_strz00_40, BGl_proc2381z00zz__sha2z00);
				return BGl_statezd2ze3stringz31zz__sha2z00(BgL_statez00_1749);
			}
		}

	}



/* &sha256sum-string */
	obj_t BGl_z62sha256sumzd2stringzb0zz__sha2z00(obj_t BgL_envz00_4871,
		obj_t BgL_strz00_4872)
	{
		{	/* Unsafe/sha2.scm 741 */
			{	/* Unsafe/sha2.scm 744 */
				obj_t BgL_auxz00_6053;

				if (STRINGP(BgL_strz00_4872))
					{	/* Unsafe/sha2.scm 744 */
						BgL_auxz00_6053 = BgL_strz00_4872;
					}
				else
					{
						obj_t BgL_auxz00_6056;

						BgL_auxz00_6056 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(32705L), BGl_string2382z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_strz00_4872);
						FAILURE(BgL_auxz00_6056, BFALSE, BFALSE);
					}
				return BGl_sha256sumzd2stringzd2zz__sha2z00(BgL_auxz00_6053);
			}
		}

	}



/* &fill-word32-string! */
	long BGl_z62fillzd2word32zd2stringz12z70zz__sha2z00(obj_t BgL_envz00_4873,
		obj_t BgL_v32z00_4874, long BgL_iz00_4875, obj_t BgL_strz00_4876,
		long BgL_nz00_4877)
	{
		{	/* Unsafe/sha2.scm 776 */
			{	/* Unsafe/sha2.scm 747 */
				long BgL_lz00_5025;

				BgL_lz00_5025 = STRING_LENGTH(((obj_t) BgL_strz00_4876));
				if (((BgL_nz00_4877 + 4L) <= BgL_lz00_5025))
					{	/* Unsafe/sha2.scm 750 */
						uint32_t BgL_v0z00_5026;

						{	/* Unsafe/sha2.scm 744 */
							long BgL_tmpz00_6066;

							BgL_tmpz00_6066 =
								(STRING_REF(((obj_t) BgL_strz00_4876), BgL_nz00_4877));
							BgL_v0z00_5026 = (uint32_t) (BgL_tmpz00_6066);
						}
						{	/* Unsafe/sha2.scm 750 */
							uint32_t BgL_v1z00_5027;

							{	/* Unsafe/sha2.scm 744 */
								long BgL_tmpz00_6071;

								BgL_tmpz00_6071 =
									(STRING_REF(((obj_t) BgL_strz00_4876), (BgL_nz00_4877 + 1L)));
								BgL_v1z00_5027 = (uint32_t) (BgL_tmpz00_6071);
							}
							{	/* Unsafe/sha2.scm 751 */
								uint32_t BgL_v2z00_5028;

								{	/* Unsafe/sha2.scm 744 */
									long BgL_tmpz00_6077;

									BgL_tmpz00_6077 =
										(STRING_REF(
											((obj_t) BgL_strz00_4876), (BgL_nz00_4877 + 2L)));
									BgL_v2z00_5028 = (uint32_t) (BgL_tmpz00_6077);
								}
								{	/* Unsafe/sha2.scm 752 */
									uint32_t BgL_v3z00_5029;

									{	/* Unsafe/sha2.scm 744 */
										long BgL_tmpz00_6083;

										BgL_tmpz00_6083 =
											(STRING_REF(
												((obj_t) BgL_strz00_4876), (BgL_nz00_4877 + 3L)));
										BgL_v3z00_5029 = (uint32_t) (BgL_tmpz00_6083);
									}
									{	/* Unsafe/sha2.scm 753 */
										uint32_t BgL_vz00_5030;

										{	/* Unsafe/sha2.scm 754 */
											uint32_t BgL_arg1554z00_5031;
											uint32_t BgL_arg1555z00_5032;

											{	/* Unsafe/sha2.scm 754 */
												uint32_t BgL_arg1556z00_5033;

												{	/* Unsafe/sha2.scm 754 */
													long BgL_arg1557z00_5034;

													{	/* Unsafe/sha2.scm 754 */
														long BgL_arg1558z00_5035;
														long BgL_arg1559z00_5036;

														{	/* Unsafe/sha2.scm 754 */
															uint32_t BgL_tmpz00_6089;

															BgL_tmpz00_6089 = (BgL_v0z00_5026 << (int) (8L));
															BgL_arg1558z00_5035 = (long) (BgL_tmpz00_6089);
														}
														BgL_arg1559z00_5036 = (long) (BgL_v1z00_5027);
														BgL_arg1557z00_5034 =
															(BgL_arg1558z00_5035 | BgL_arg1559z00_5036);
													}
													BgL_arg1556z00_5033 =
														(uint32_t) (BgL_arg1557z00_5034);
												}
												BgL_arg1554z00_5031 =
													(BgL_arg1556z00_5033 << (int) (16L));
											}
											{	/* Unsafe/sha2.scm 754 */
												long BgL_arg1562z00_5037;

												{	/* Unsafe/sha2.scm 754 */
													long BgL_arg1564z00_5038;
													long BgL_arg1565z00_5039;

													{	/* Unsafe/sha2.scm 754 */
														uint32_t BgL_tmpz00_6098;

														BgL_tmpz00_6098 = (BgL_v2z00_5028 << (int) (8L));
														BgL_arg1564z00_5038 = (long) (BgL_tmpz00_6098);
													}
													BgL_arg1565z00_5039 = (long) (BgL_v3z00_5029);
													BgL_arg1562z00_5037 =
														(BgL_arg1564z00_5038 | BgL_arg1565z00_5039);
												}
												BgL_arg1555z00_5032 = (uint32_t) (BgL_arg1562z00_5037);
											}
											BgL_vz00_5030 =
												(BgL_arg1554z00_5031 | BgL_arg1555z00_5032);
										}
										{	/* Unsafe/sha2.scm 754 */

											BGL_U32VSET(BgL_v32z00_4874, BgL_iz00_4875,
												BgL_vz00_5030);
											BUNSPEC;
											return 4L;
										}
									}
								}
							}
						}
					}
				else
					{	/* Unsafe/sha2.scm 749 */
						if ((BgL_nz00_4877 >= (1L + BgL_lz00_5025)))
							{	/* Unsafe/sha2.scm 757 */
								{	/* Unsafe/sha2.scm 758 */
									uint32_t BgL_tmpz00_6110;

									BgL_tmpz00_6110 = (uint32_t) (0L);
									BGL_U32VSET(BgL_v32z00_4874, BgL_iz00_4875, BgL_tmpz00_6110);
								}
								BUNSPEC;
								return 0L;
							}
						else
							{	/* Unsafe/sha2.scm 761 */
								obj_t BgL_vz00_5040;
								long BgL_kz00_5041;

								BgL_vz00_5040 =
									BGl_makezd2u32vectorzd2zz__srfi4z00(4L, (uint32_t) (0L));
								BgL_kz00_5041 = (4L - ((BgL_nz00_4877 + 4L) - BgL_lz00_5025));
								{
									long BgL_jz00_5043;

									BgL_jz00_5043 = 0L;
								BgL_loopz00_5042:
									if ((BgL_jz00_5043 == BgL_kz00_5041))
										{	/* Unsafe/sha2.scm 764 */
											{	/* Unsafe/sha2.scm 766 */
												uint32_t BgL_tmpz00_6120;

												BgL_tmpz00_6120 = (uint32_t) (128L);
												BGL_U32VSET(BgL_vz00_5040, BgL_jz00_5043,
													BgL_tmpz00_6120);
											}
											BUNSPEC;
											{	/* Unsafe/sha2.scm 767 */
												uint32_t BgL_v0z00_5044;

												BgL_v0z00_5044 = BGL_U32VREF(BgL_vz00_5040, 0L);
												{	/* Unsafe/sha2.scm 767 */
													uint32_t BgL_v1z00_5045;

													BgL_v1z00_5045 = BGL_U32VREF(BgL_vz00_5040, 1L);
													{	/* Unsafe/sha2.scm 768 */
														uint32_t BgL_v2z00_5046;

														BgL_v2z00_5046 = BGL_U32VREF(BgL_vz00_5040, 2L);
														{	/* Unsafe/sha2.scm 769 */
															uint32_t BgL_v3z00_5047;

															BgL_v3z00_5047 = BGL_U32VREF(BgL_vz00_5040, 3L);
															{	/* Unsafe/sha2.scm 770 */
																uint32_t BgL_vz00_5048;

																{	/* Unsafe/sha2.scm 771 */
																	uint32_t BgL_arg1580z00_5049;
																	uint32_t BgL_arg1582z00_5050;

																	{	/* Unsafe/sha2.scm 771 */
																		uint32_t BgL_arg1583z00_5051;

																		{	/* Unsafe/sha2.scm 771 */
																			long BgL_arg1584z00_5052;

																			{	/* Unsafe/sha2.scm 771 */
																				long BgL_arg1585z00_5053;
																				long BgL_arg1586z00_5054;

																				{	/* Unsafe/sha2.scm 771 */
																					uint32_t BgL_tmpz00_6127;

																					BgL_tmpz00_6127 =
																						(BgL_v0z00_5044 << (int) (8L));
																					BgL_arg1585z00_5053 =
																						(long) (BgL_tmpz00_6127);
																				}
																				BgL_arg1586z00_5054 =
																					(long) (BgL_v1z00_5045);
																				BgL_arg1584z00_5052 =
																					(BgL_arg1585z00_5053 |
																					BgL_arg1586z00_5054);
																			}
																			BgL_arg1583z00_5051 =
																				(uint32_t) (BgL_arg1584z00_5052);
																		}
																		BgL_arg1580z00_5049 =
																			(BgL_arg1583z00_5051 << (int) (16L));
																	}
																	{	/* Unsafe/sha2.scm 771 */
																		long BgL_arg1589z00_5055;

																		{	/* Unsafe/sha2.scm 771 */
																			long BgL_arg1591z00_5056;
																			long BgL_arg1593z00_5057;

																			{	/* Unsafe/sha2.scm 771 */
																				uint32_t BgL_tmpz00_6136;

																				BgL_tmpz00_6136 =
																					(BgL_v2z00_5046 << (int) (8L));
																				BgL_arg1591z00_5056 =
																					(long) (BgL_tmpz00_6136);
																			}
																			BgL_arg1593z00_5057 =
																				(long) (BgL_v3z00_5047);
																			BgL_arg1589z00_5055 =
																				(BgL_arg1591z00_5056 |
																				BgL_arg1593z00_5057);
																		}
																		BgL_arg1582z00_5050 =
																			(uint32_t) (BgL_arg1589z00_5055);
																	}
																	BgL_vz00_5048 =
																		(BgL_arg1580z00_5049 | BgL_arg1582z00_5050);
																}
																{	/* Unsafe/sha2.scm 771 */

																	BGL_U32VSET(BgL_v32z00_4874, BgL_iz00_4875,
																		BgL_vz00_5048);
																	BUNSPEC;
																	return (BgL_jz00_5043 + 1L);
																}
															}
														}
													}
												}
											}
										}
									else
										{	/* Unsafe/sha2.scm 764 */
											{	/* Unsafe/sha2.scm 775 */
												long BgL_arg1595z00_5058;

												BgL_arg1595z00_5058 =
													(STRING_REF(
														((obj_t) BgL_strz00_4876),
														(BgL_nz00_4877 + BgL_jz00_5043)));
												{	/* Unsafe/sha2.scm 775 */
													uint32_t BgL_tmpz00_6150;

													BgL_tmpz00_6150 = (uint32_t) (BgL_arg1595z00_5058);
													BGL_U32VSET(BgL_vz00_5040, BgL_jz00_5043,
														BgL_tmpz00_6150);
												} BUNSPEC;
											}
											{
												long BgL_jz00_6153;

												BgL_jz00_6153 = (BgL_jz00_5043 + 1L);
												BgL_jz00_5043 = BgL_jz00_6153;
												goto BgL_loopz00_5042;
											}
										}
								}
							}
					}
			}
		}

	}



/* sha256sum-port */
	BGL_EXPORTED_DEF obj_t BGl_sha256sumzd2portzd2zz__sha2z00(obj_t BgL_pz00_41)
	{
		{	/* Unsafe/sha2.scm 786 */
			{	/* Unsafe/sha2.scm 786 */
				obj_t BgL_lenz00_4889;

				BgL_lenz00_4889 = MAKE_CELL(BINT(0L));
				{	/* Unsafe/sha2.scm 788 */
					obj_t BgL_bufz00_1820;

					{	/* Llib/srfi4.scm 451 */

						BgL_bufz00_1820 =
							BGl_makezd2u32vectorzd2zz__srfi4z00(4L, (uint32_t) (0));
					}
					{	/* Unsafe/sha2.scm 809 */
						obj_t BgL_fillzd2word32zd2portz12z12_4878;

						{
							int BgL_tmpz00_6157;

							BgL_tmpz00_6157 = (int) (2L);
							BgL_fillzd2word32zd2portz12z12_4878 =
								MAKE_L_PROCEDURE(BGl_z62fillzd2word32zd2portz12z70zz__sha2z00,
								BgL_tmpz00_6157);
						}
						PROCEDURE_L_SET(BgL_fillzd2word32zd2portz12z12_4878,
							(int) (0L), BgL_bufz00_1820);
						PROCEDURE_L_SET(BgL_fillzd2word32zd2portz12z12_4878,
							(int) (1L), ((obj_t) BgL_lenz00_4889));
						{	/* Unsafe/sha2.scm 841 */
							obj_t BgL_statez00_1823;
							obj_t BgL_bufferz00_1824;

							BgL_statez00_1823 =
								BGl_sha256zd2initialzd2hashzd2valuezd2zz__sha2z00();
							{	/* Llib/srfi4.scm 451 */

								BgL_bufferz00_1824 =
									BGl_makezd2u32vectorzd2zz__srfi4z00(16L, (uint32_t) (0));
							}
							BGl_sha256zd2updatezd2zz__sha2z00(BgL_statez00_1823,
								BgL_bufferz00_1824, BgL_pz00_41,
								BgL_fillzd2word32zd2portz12z12_4878);
							return BGl_statezd2ze3stringz31zz__sha2z00(BgL_statez00_1823);
						}
					}
				}
			}
		}

	}



/* &sha256sum-port */
	obj_t BGl_z62sha256sumzd2portzb0zz__sha2z00(obj_t BgL_envz00_4879,
		obj_t BgL_pz00_4880)
	{
		{	/* Unsafe/sha2.scm 786 */
			{	/* Unsafe/sha2.scm 786 */
				obj_t BgL_auxz00_6169;

				if (INPUT_PORTP(BgL_pz00_4880))
					{	/* Unsafe/sha2.scm 786 */
						BgL_auxz00_6169 = BgL_pz00_4880;
					}
				else
					{
						obj_t BgL_auxz00_6172;

						BgL_auxz00_6172 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(34139L), BGl_string2384z00zz__sha2z00,
							BGl_string2385z00zz__sha2z00, BgL_pz00_4880);
						FAILURE(BgL_auxz00_6172, BFALSE, BFALSE);
					}
				return BGl_sha256sumzd2portzd2zz__sha2z00(BgL_auxz00_6169);
			}
		}

	}



/* &fill-word32-port! */
	long BGl_z62fillzd2word32zd2portz12z70zz__sha2z00(obj_t BgL_envz00_4881,
		obj_t BgL_v32z00_4884, long BgL_iz00_4885, obj_t BgL_pz00_4886,
		long BgL_nz00_4887)
	{
		{	/* Unsafe/sha2.scm 839 */
			{	/* Unsafe/sha2.scm 809 */
				obj_t BgL_bufz00_4882;
				obj_t BgL_lenz00_4883;

				BgL_bufz00_4882 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4881, (int) (0L)));
				BgL_lenz00_4883 = PROCEDURE_L_REF(BgL_envz00_4881, (int) (1L));
				{
					obj_t BgL_pz00_5064;

					{	/* Unsafe/sha2.scm 809 */
						long BgL_lz00_5071;

						BgL_pz00_5064 = ((obj_t) BgL_pz00_4886);
						{
							long BgL_iz00_5066;

							BgL_iz00_5066 = 0L;
						BgL_loopz00_5065:
							if ((BgL_iz00_5066 == 4L))
								{	/* Unsafe/sha2.scm 794 */
									BgL_lz00_5071 = BgL_iz00_5066;
								}
							else
								{	/* Unsafe/sha2.scm 796 */
									obj_t BgL_cz00_5067;

									BgL_cz00_5067 =
										BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_pz00_5064);
									if (EOF_OBJECTP(BgL_cz00_5067))
										{
											long BgL_jz00_5069;

											BgL_jz00_5069 = BgL_iz00_5066;
										BgL_liipz00_5068:
											if ((BgL_jz00_5069 == 4L))
												{	/* Unsafe/sha2.scm 799 */
													BgL_lz00_5071 = BgL_iz00_5066;
												}
											else
												{	/* Unsafe/sha2.scm 799 */
													{	/* Unsafe/sha2.scm 802 */
														uint32_t BgL_tmpz00_6189;

														BgL_tmpz00_6189 = (uint32_t) (0L);
														BGL_U32VSET(BgL_bufz00_4882, BgL_jz00_5069,
															BgL_tmpz00_6189);
													}
													BUNSPEC;
													{
														long BgL_jz00_6192;

														BgL_jz00_6192 = (BgL_jz00_5069 + 1L);
														BgL_jz00_5069 = BgL_jz00_6192;
														goto BgL_liipz00_5068;
													}
												}
										}
									else
										{	/* Unsafe/sha2.scm 797 */
											{	/* Unsafe/sha2.scm 805 */
												uint32_t BgL_arg1653z00_5070;

												{	/* Unsafe/sha2.scm 805 */
													char BgL_tmpz00_6194;

													BgL_tmpz00_6194 = (signed char) CINT(BgL_cz00_5067);
													BgL_arg1653z00_5070 = (uint32_t) (BgL_tmpz00_6194);
												}
												BGL_U32VSET(BgL_bufz00_4882, BgL_iz00_5066,
													BgL_arg1653z00_5070);
												BUNSPEC;
											}
											{
												long BgL_iz00_6198;

												BgL_iz00_6198 = (BgL_iz00_5066 + 1L);
												BgL_iz00_5066 = BgL_iz00_6198;
												goto BgL_loopz00_5065;
											}
										}
								}
						}
						{	/* Unsafe/sha2.scm 810 */
							obj_t BgL_auxz00_5072;

							BgL_auxz00_5072 =
								ADDFX(CELL_REF(BgL_lenz00_4883), BINT(BgL_lz00_5071));
							CELL_SET(BgL_lenz00_4883, BgL_auxz00_5072);
						}
						if (
							((BgL_nz00_4887 + 4L) <= (long) CINT(CELL_REF(BgL_lenz00_4883))))
							{	/* Unsafe/sha2.scm 813 */
								uint32_t BgL_v0z00_5073;

								BgL_v0z00_5073 = BGL_U32VREF(BgL_bufz00_4882, 0L);
								{	/* Unsafe/sha2.scm 813 */
									uint32_t BgL_v1z00_5074;

									BgL_v1z00_5074 = BGL_U32VREF(BgL_bufz00_4882, 1L);
									{	/* Unsafe/sha2.scm 814 */
										uint32_t BgL_v2z00_5075;

										BgL_v2z00_5075 = BGL_U32VREF(BgL_bufz00_4882, 2L);
										{	/* Unsafe/sha2.scm 815 */
											uint32_t BgL_v3z00_5076;

											BgL_v3z00_5076 = BGL_U32VREF(BgL_bufz00_4882, 3L);
											{	/* Unsafe/sha2.scm 816 */
												uint32_t BgL_vz00_5077;

												{	/* Unsafe/sha2.scm 817 */
													uint32_t BgL_arg1610z00_5078;
													uint32_t BgL_arg1611z00_5079;

													{	/* Unsafe/sha2.scm 817 */
														uint32_t BgL_arg1612z00_5080;

														{	/* Unsafe/sha2.scm 817 */
															long BgL_arg1613z00_5081;

															{	/* Unsafe/sha2.scm 817 */
																long BgL_arg1615z00_5082;
																long BgL_arg1616z00_5083;

																{	/* Unsafe/sha2.scm 817 */
																	uint32_t BgL_tmpz00_6211;

																	BgL_tmpz00_6211 =
																		(BgL_v0z00_5073 << (int) (8L));
																	BgL_arg1615z00_5082 =
																		(long) (BgL_tmpz00_6211);
																}
																BgL_arg1616z00_5083 = (long) (BgL_v1z00_5074);
																BgL_arg1613z00_5081 =
																	(BgL_arg1615z00_5082 | BgL_arg1616z00_5083);
															}
															BgL_arg1612z00_5080 =
																(uint32_t) (BgL_arg1613z00_5081);
														}
														BgL_arg1610z00_5078 =
															(BgL_arg1612z00_5080 << (int) (16L));
													}
													{	/* Unsafe/sha2.scm 817 */
														long BgL_arg1618z00_5084;

														{	/* Unsafe/sha2.scm 817 */
															long BgL_arg1619z00_5085;
															long BgL_arg1620z00_5086;

															{	/* Unsafe/sha2.scm 817 */
																uint32_t BgL_tmpz00_6220;

																BgL_tmpz00_6220 =
																	(BgL_v2z00_5075 << (int) (8L));
																BgL_arg1619z00_5085 = (long) (BgL_tmpz00_6220);
															}
															BgL_arg1620z00_5086 = (long) (BgL_v3z00_5076);
															BgL_arg1618z00_5084 =
																(BgL_arg1619z00_5085 | BgL_arg1620z00_5086);
														}
														BgL_arg1611z00_5079 =
															(uint32_t) (BgL_arg1618z00_5084);
													}
													BgL_vz00_5077 =
														(BgL_arg1610z00_5078 | BgL_arg1611z00_5079);
												}
												{	/* Unsafe/sha2.scm 817 */

													BGL_U32VSET(BgL_v32z00_4884, BgL_iz00_4885,
														BgL_vz00_5077);
													BUNSPEC;
													return 4L;
												}
											}
										}
									}
								}
							}
						else
							{	/* Unsafe/sha2.scm 812 */
								if (
									(BgL_nz00_4887 >=
										(1L + (long) CINT(CELL_REF(BgL_lenz00_4883)))))
									{	/* Unsafe/sha2.scm 820 */
										{	/* Unsafe/sha2.scm 821 */
											uint32_t BgL_tmpz00_6233;

											BgL_tmpz00_6233 = (uint32_t) (0L);
											BGL_U32VSET(BgL_v32z00_4884, BgL_iz00_4885,
												BgL_tmpz00_6233);
										} BUNSPEC;
										return 0L;
									}
								else
									{	/* Unsafe/sha2.scm 824 */
										obj_t BgL_vz00_5087;
										long BgL_kz00_5088;

										BgL_vz00_5087 =
											BGl_makezd2u32vectorzd2zz__srfi4z00(4L, (uint32_t) (0L));
										BgL_kz00_5088 =
											(4L -
											((BgL_nz00_4887 + 4L) -
												(long) CINT(CELL_REF(BgL_lenz00_4883))));
										{
											long BgL_jz00_5090;

											BgL_jz00_5090 = 0L;
										BgL_loopz00_5089:
											if ((BgL_jz00_5090 == BgL_kz00_5088))
												{	/* Unsafe/sha2.scm 827 */
													{	/* Unsafe/sha2.scm 829 */
														uint32_t BgL_tmpz00_6244;

														BgL_tmpz00_6244 = (uint32_t) (128L);
														BGL_U32VSET(BgL_vz00_5087, BgL_jz00_5090,
															BgL_tmpz00_6244);
													}
													BUNSPEC;
													{	/* Unsafe/sha2.scm 830 */
														uint32_t BgL_v0z00_5091;

														BgL_v0z00_5091 = BGL_U32VREF(BgL_vz00_5087, 0L);
														{	/* Unsafe/sha2.scm 830 */
															uint32_t BgL_v1z00_5092;

															BgL_v1z00_5092 = BGL_U32VREF(BgL_vz00_5087, 1L);
															{	/* Unsafe/sha2.scm 831 */
																uint32_t BgL_v2z00_5093;

																BgL_v2z00_5093 = BGL_U32VREF(BgL_vz00_5087, 2L);
																{	/* Unsafe/sha2.scm 832 */
																	uint32_t BgL_v3z00_5094;

																	BgL_v3z00_5094 =
																		BGL_U32VREF(BgL_vz00_5087, 3L);
																	{	/* Unsafe/sha2.scm 833 */
																		uint32_t BgL_vz00_5095;

																		{	/* Unsafe/sha2.scm 834 */
																			uint32_t BgL_arg1626z00_5096;
																			uint32_t BgL_arg1627z00_5097;

																			{	/* Unsafe/sha2.scm 834 */
																				uint32_t BgL_arg1628z00_5098;

																				{	/* Unsafe/sha2.scm 834 */
																					long BgL_arg1629z00_5099;

																					{	/* Unsafe/sha2.scm 834 */
																						long BgL_arg1630z00_5100;
																						long BgL_arg1631z00_5101;

																						{	/* Unsafe/sha2.scm 834 */
																							uint32_t BgL_tmpz00_6251;

																							BgL_tmpz00_6251 =
																								(BgL_v0z00_5091 << (int) (8L));
																							BgL_arg1630z00_5100 =
																								(long) (BgL_tmpz00_6251);
																						}
																						BgL_arg1631z00_5101 =
																							(long) (BgL_v1z00_5092);
																						BgL_arg1629z00_5099 =
																							(BgL_arg1630z00_5100 |
																							BgL_arg1631z00_5101);
																					}
																					BgL_arg1628z00_5098 =
																						(uint32_t) (BgL_arg1629z00_5099);
																				}
																				BgL_arg1626z00_5096 =
																					(BgL_arg1628z00_5098 << (int) (16L));
																			}
																			{	/* Unsafe/sha2.scm 834 */
																				long BgL_arg1636z00_5102;

																				{	/* Unsafe/sha2.scm 834 */
																					long BgL_arg1637z00_5103;
																					long BgL_arg1638z00_5104;

																					{	/* Unsafe/sha2.scm 834 */
																						uint32_t BgL_tmpz00_6260;

																						BgL_tmpz00_6260 =
																							(BgL_v2z00_5093 << (int) (8L));
																						BgL_arg1637z00_5103 =
																							(long) (BgL_tmpz00_6260);
																					}
																					BgL_arg1638z00_5104 =
																						(long) (BgL_v3z00_5094);
																					BgL_arg1636z00_5102 =
																						(BgL_arg1637z00_5103 |
																						BgL_arg1638z00_5104);
																				}
																				BgL_arg1627z00_5097 =
																					(uint32_t) (BgL_arg1636z00_5102);
																			}
																			BgL_vz00_5095 =
																				(BgL_arg1626z00_5096 |
																				BgL_arg1627z00_5097);
																		}
																		{	/* Unsafe/sha2.scm 834 */

																			BGL_U32VSET(BgL_v32z00_4884,
																				BgL_iz00_4885, BgL_vz00_5095);
																			BUNSPEC;
																			return (BgL_jz00_5090 + 1L);
																		}
																	}
																}
															}
														}
													}
												}
											else
												{	/* Unsafe/sha2.scm 827 */
													{	/* Unsafe/sha2.scm 838 */
														uint32_t BgL_arg1640z00_5105;

														BgL_arg1640z00_5105 =
															BGL_U32VREF(BgL_bufz00_4882, BgL_jz00_5090);
														BGL_U32VSET(BgL_vz00_5087, BgL_jz00_5090,
															BgL_arg1640z00_5105);
														BUNSPEC;
													}
													{
														long BgL_jz00_6272;

														BgL_jz00_6272 = (BgL_jz00_5090 + 1L);
														BgL_jz00_5090 = BgL_jz00_6272;
														goto BgL_loopz00_5089;
													}
												}
										}
									}
							}
					}
				}
			}
		}

	}



/* sha256sum-file */
	BGL_EXPORTED_DEF obj_t BGl_sha256sumzd2filezd2zz__sha2z00(obj_t
		BgL_fnamez00_42)
	{
		{	/* Unsafe/sha2.scm 849 */
			{	/* Unsafe/sha2.scm 850 */
				obj_t BgL_mmz00_1903;

				BgL_mmz00_1903 =
					BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_42, BTRUE, BFALSE);
				if (BGL_MMAPP(BgL_mmz00_1903))
					{	/* Unsafe/sha2.scm 852 */
						obj_t BgL_exitd1074z00_1905;

						BgL_exitd1074z00_1905 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Unsafe/sha2.scm 854 */
							obj_t BgL_zc3z04anonymousza31656ze3z87_4891;

							BgL_zc3z04anonymousza31656ze3z87_4891 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31656ze3ze5zz__sha2z00, (int) (0L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31656ze3z87_4891, (int) (0L),
								BgL_mmz00_1903);
							{	/* Unsafe/sha2.scm 852 */
								obj_t BgL_arg2351z00_3861;

								{	/* Unsafe/sha2.scm 852 */
									obj_t BgL_arg2352z00_3862;

									BgL_arg2352z00_3862 =
										BGL_EXITD_PROTECT(BgL_exitd1074z00_1905);
									BgL_arg2351z00_3861 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31656ze3z87_4891,
										BgL_arg2352z00_3862);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1074z00_1905,
									BgL_arg2351z00_3861);
								BUNSPEC;
							}
							{	/* Unsafe/sha2.scm 853 */
								obj_t BgL_tmp1076z00_1907;

								BgL_tmp1076z00_1907 =
									BGl_sha256sumzd2mmapzd2zz__sha2z00(BgL_mmz00_1903);
								{	/* Unsafe/sha2.scm 852 */
									bool_t BgL_test2454z00_6287;

									{	/* Unsafe/sha2.scm 852 */
										obj_t BgL_arg2350z00_3864;

										BgL_arg2350z00_3864 =
											BGL_EXITD_PROTECT(BgL_exitd1074z00_1905);
										BgL_test2454z00_6287 = PAIRP(BgL_arg2350z00_3864);
									}
									if (BgL_test2454z00_6287)
										{	/* Unsafe/sha2.scm 852 */
											obj_t BgL_arg2348z00_3865;

											{	/* Unsafe/sha2.scm 852 */
												obj_t BgL_arg2349z00_3866;

												BgL_arg2349z00_3866 =
													BGL_EXITD_PROTECT(BgL_exitd1074z00_1905);
												BgL_arg2348z00_3865 =
													CDR(((obj_t) BgL_arg2349z00_3866));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1074z00_1905,
												BgL_arg2348z00_3865);
											BUNSPEC;
										}
									else
										{	/* Unsafe/sha2.scm 852 */
											BFALSE;
										}
								}
								bgl_close_mmap(BgL_mmz00_1903);
								return BgL_tmp1076z00_1907;
							}
						}
					}
				else
					{	/* Unsafe/sha2.scm 855 */
						obj_t BgL_pz00_1910;

						{	/* Ieee/port.scm 466 */

							BgL_pz00_1910 =
								BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_fnamez00_42, BTRUE, BINT(5000000L));
						}
						{	/* Unsafe/sha2.scm 856 */
							obj_t BgL_exitd1078z00_1911;

							BgL_exitd1078z00_1911 = BGL_EXITD_TOP_AS_OBJ();
							{	/* Unsafe/sha2.scm 858 */
								obj_t BgL_zc3z04anonymousza31657ze3z87_4892;

								BgL_zc3z04anonymousza31657ze3z87_4892 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31657ze3ze5zz__sha2z00, (int) (0L),
									(int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31657ze3z87_4892, (int) (0L),
									BgL_pz00_1910);
								{	/* Unsafe/sha2.scm 856 */
									obj_t BgL_arg2351z00_3870;

									{	/* Unsafe/sha2.scm 856 */
										obj_t BgL_arg2352z00_3871;

										BgL_arg2352z00_3871 =
											BGL_EXITD_PROTECT(BgL_exitd1078z00_1911);
										BgL_arg2351z00_3870 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31657ze3z87_4892,
											BgL_arg2352z00_3871);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1078z00_1911,
										BgL_arg2351z00_3870);
									BUNSPEC;
								}
								{	/* Unsafe/sha2.scm 857 */
									obj_t BgL_tmp1080z00_1913;

									BgL_tmp1080z00_1913 =
										BGl_sha256sumzd2portzd2zz__sha2z00(BgL_pz00_1910);
									{	/* Unsafe/sha2.scm 856 */
										bool_t BgL_test2455z00_6307;

										{	/* Unsafe/sha2.scm 856 */
											obj_t BgL_arg2350z00_3873;

											BgL_arg2350z00_3873 =
												BGL_EXITD_PROTECT(BgL_exitd1078z00_1911);
											BgL_test2455z00_6307 = PAIRP(BgL_arg2350z00_3873);
										}
										if (BgL_test2455z00_6307)
											{	/* Unsafe/sha2.scm 856 */
												obj_t BgL_arg2348z00_3874;

												{	/* Unsafe/sha2.scm 856 */
													obj_t BgL_arg2349z00_3875;

													BgL_arg2349z00_3875 =
														BGL_EXITD_PROTECT(BgL_exitd1078z00_1911);
													BgL_arg2348z00_3874 =
														CDR(((obj_t) BgL_arg2349z00_3875));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1078z00_1911,
													BgL_arg2348z00_3874);
												BUNSPEC;
											}
										else
											{	/* Unsafe/sha2.scm 856 */
												BFALSE;
											}
									}
									bgl_close_input_port(((obj_t) BgL_pz00_1910));
									return BgL_tmp1080z00_1913;
								}
							}
						}
					}
			}
		}

	}



/* &sha256sum-file */
	obj_t BGl_z62sha256sumzd2filezb0zz__sha2z00(obj_t BgL_envz00_4893,
		obj_t BgL_fnamez00_4894)
	{
		{	/* Unsafe/sha2.scm 849 */
			{	/* Unsafe/sha2.scm 850 */
				obj_t BgL_auxz00_6316;

				if (STRINGP(BgL_fnamez00_4894))
					{	/* Unsafe/sha2.scm 850 */
						BgL_auxz00_6316 = BgL_fnamez00_4894;
					}
				else
					{
						obj_t BgL_auxz00_6319;

						BgL_auxz00_6319 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(36025L), BGl_string2386z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_fnamez00_4894);
						FAILURE(BgL_auxz00_6319, BFALSE, BFALSE);
					}
				return BGl_sha256sumzd2filezd2zz__sha2z00(BgL_auxz00_6316);
			}
		}

	}



/* &<@anonymous:1656> */
	obj_t BGl_z62zc3z04anonymousza31656ze3ze5zz__sha2z00(obj_t BgL_envz00_4895)
	{
		{	/* Unsafe/sha2.scm 852 */
			{	/* Unsafe/sha2.scm 854 */
				obj_t BgL_mmz00_4896;

				BgL_mmz00_4896 = ((obj_t) PROCEDURE_REF(BgL_envz00_4895, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_4896);
			}
		}

	}



/* &<@anonymous:1657> */
	obj_t BGl_z62zc3z04anonymousza31657ze3ze5zz__sha2z00(obj_t BgL_envz00_4897)
	{
		{	/* Unsafe/sha2.scm 856 */
			{	/* Unsafe/sha2.scm 858 */
				obj_t BgL_pz00_4898;

				BgL_pz00_4898 = PROCEDURE_REF(BgL_envz00_4897, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4898));
			}
		}

	}



/* sha256sum */
	BGL_EXPORTED_DEF obj_t BGl_sha256sumz00zz__sha2z00(obj_t BgL_objz00_43)
	{
		{	/* Unsafe/sha2.scm 863 */
			if (BGL_MMAPP(BgL_objz00_43))
				{	/* Unsafe/sha2.scm 865 */
					return BGl_sha256sumzd2mmapzd2zz__sha2z00(BgL_objz00_43);
				}
			else
				{	/* Unsafe/sha2.scm 865 */
					if (STRINGP(BgL_objz00_43))
						{	/* Unsafe/sha2.scm 867 */
							return BGl_sha256sumzd2stringzd2zz__sha2z00(BgL_objz00_43);
						}
					else
						{	/* Unsafe/sha2.scm 867 */
							if (INPUT_PORTP(BgL_objz00_43))
								{	/* Unsafe/sha2.scm 869 */
									return BGl_sha256sumzd2portzd2zz__sha2z00(BgL_objz00_43);
								}
							else
								{	/* Unsafe/sha2.scm 869 */
									return
										BGl_errorz00zz__errorz00(BGl_string2387z00zz__sha2z00,
										BGl_string2388z00zz__sha2z00, BgL_objz00_43);
								}
						}
				}
		}

	}



/* &sha256sum */
	obj_t BGl_z62sha256sumz62zz__sha2z00(obj_t BgL_envz00_4899,
		obj_t BgL_objz00_4900)
	{
		{	/* Unsafe/sha2.scm 863 */
			return BGl_sha256sumz00zz__sha2z00(BgL_objz00_4900);
		}

	}



/* sha512-internal-transform */
	obj_t BGl_sha512zd2internalzd2transformz00zz__sha2z00(obj_t BgL_statez00_52,
		obj_t BgL_bufferz00_53)
	{
		{	/* Unsafe/sha2.scm 889 */
			{
				obj_t BgL_statez00_2011;
				uint64_t BgL_az00_2012;
				uint64_t BgL_bz00_2013;
				uint64_t BgL_cz00_2014;
				uint64_t BgL_dz00_2015;
				uint64_t BgL_ez00_2016;
				uint64_t BgL_fz00_2017;
				uint64_t BgL_gz00_2018;
				uint64_t BgL_hz00_2019;

				{	/* Unsafe/sha2.scm 924 */
					uint64_t BgL_g1082z00_1929;
					uint64_t BgL_g1083z00_1930;
					uint64_t BgL_g1084z00_1931;
					uint64_t BgL_g1085z00_1932;
					uint64_t BgL_g1086z00_1933;
					uint64_t BgL_g1087z00_1934;
					uint64_t BgL_g1089z00_1935;
					uint64_t BgL_g1090z00_1936;

					BgL_g1082z00_1929 = BGL_U64VREF(BgL_statez00_52, 0L);
					BgL_g1083z00_1930 = BGL_U64VREF(BgL_statez00_52, 1L);
					BgL_g1084z00_1931 = BGL_U64VREF(BgL_statez00_52, 2L);
					BgL_g1085z00_1932 = BGL_U64VREF(BgL_statez00_52, 3L);
					BgL_g1086z00_1933 = BGL_U64VREF(BgL_statez00_52, 4L);
					BgL_g1087z00_1934 = BGL_U64VREF(BgL_statez00_52, 5L);
					BgL_g1089z00_1935 = BGL_U64VREF(BgL_statez00_52, 6L);
					BgL_g1090z00_1936 = BGL_U64VREF(BgL_statez00_52, 7L);
					{
						uint64_t BgL_az00_1938;
						uint64_t BgL_bz00_1939;
						uint64_t BgL_cz00_1940;
						uint64_t BgL_dz00_1941;
						uint64_t BgL_ez00_1942;
						uint64_t BgL_fz00_1943;
						uint64_t BgL_gz00_1944;
						uint64_t BgL_hz00_1945;
						long BgL_jz00_1946;

						BgL_az00_1938 = BgL_g1082z00_1929;
						BgL_bz00_1939 = BgL_g1083z00_1930;
						BgL_cz00_1940 = BgL_g1084z00_1931;
						BgL_dz00_1941 = BgL_g1085z00_1932;
						BgL_ez00_1942 = BgL_g1086z00_1933;
						BgL_fz00_1943 = BgL_g1087z00_1934;
						BgL_gz00_1944 = BgL_g1089z00_1935;
						BgL_hz00_1945 = BgL_g1090z00_1936;
						BgL_jz00_1946 = 0L;
					BgL_zc3z04anonymousza31662ze3z87_1947:
						if ((BgL_jz00_1946 < 16L))
							{	/* Unsafe/sha2.scm 935 */
								uint64_t BgL_wz00_1949;

								BgL_wz00_1949 = BGL_U64VREF(BgL_bufferz00_53, BgL_jz00_1946);
								{	/* Unsafe/sha2.scm 935 */
									uint64_t BgL_t1z00_1950;

									{	/* Unsafe/sha2.scm 898 */
										uint64_t BgL_arg1714z00_3985;

										{	/* Unsafe/sha2.scm 898 */
											uint64_t BgL_arg1715z00_3986;
											uint64_t BgL_arg1717z00_3987;

											BgL_arg1715z00_3986 =
												BGl_Sigma1zd2512zd2zz__sha2z00(BgL_ez00_1942);
											{	/* Unsafe/sha2.scm 898 */
												uint64_t BgL_arg1718z00_3988;
												uint64_t BgL_arg1720z00_3989;

												BgL_arg1718z00_3988 =
													(
													(BgL_ez00_1942 & BgL_fz00_1943) ^
													(~(BgL_ez00_1942) & BgL_gz00_1944));
												{	/* Unsafe/sha2.scm 898 */
													uint64_t BgL_arg1722z00_3990;

													BgL_arg1722z00_3990 =
														BGL_U64VREF(BGl_K512z00zz__sha2z00, BgL_jz00_1946);
													BgL_arg1720z00_3989 =
														(BgL_arg1722z00_3990 + BgL_wz00_1949);
												}
												BgL_arg1717z00_3987 =
													(BgL_arg1718z00_3988 + BgL_arg1720z00_3989);
											}
											BgL_arg1714z00_3985 =
												(BgL_arg1715z00_3986 + BgL_arg1717z00_3987);
										}
										BgL_t1z00_1950 = (BgL_hz00_1945 + BgL_arg1714z00_3985);
									}
									{	/* Unsafe/sha2.scm 936 */
										uint64_t BgL_t2z00_1951;

										BgL_t2z00_1951 =
											(
											(((BgL_az00_1938 >>
														(int) (28L)) |
													(BgL_az00_1938 <<
														(int) (
															(64L - 28L)))) ^
												(((BgL_az00_1938 >>
															(int) (34L)) |
														(BgL_az00_1938 <<
															(int) (
																(64L - 34L)))) ^
													((BgL_az00_1938 >>
															(int) (39L)) |
														(BgL_az00_1938 <<
															(int) (
																(64L - 39L)))))) +
											((BgL_az00_1938 & BgL_bz00_1939) ^
												((BgL_az00_1938 & BgL_cz00_1940) ^
													(BgL_bz00_1939 & BgL_cz00_1940))));
										{	/* Unsafe/sha2.scm 937 */

											{
												long BgL_jz00_6400;
												uint64_t BgL_hz00_6399;
												uint64_t BgL_gz00_6398;
												uint64_t BgL_fz00_6397;
												uint64_t BgL_ez00_6395;
												uint64_t BgL_dz00_6394;
												uint64_t BgL_cz00_6393;
												uint64_t BgL_bz00_6392;
												uint64_t BgL_az00_6390;

												BgL_az00_6390 = (BgL_t1z00_1950 + BgL_t2z00_1951);
												BgL_bz00_6392 = BgL_az00_1938;
												BgL_cz00_6393 = BgL_bz00_1939;
												BgL_dz00_6394 = BgL_cz00_1940;
												BgL_ez00_6395 = (BgL_dz00_1941 + BgL_t1z00_1950);
												BgL_fz00_6397 = BgL_ez00_1942;
												BgL_gz00_6398 = BgL_fz00_1943;
												BgL_hz00_6399 = BgL_gz00_1944;
												BgL_jz00_6400 = (BgL_jz00_1946 + 1L);
												BgL_jz00_1946 = BgL_jz00_6400;
												BgL_hz00_1945 = BgL_hz00_6399;
												BgL_gz00_1944 = BgL_gz00_6398;
												BgL_fz00_1943 = BgL_fz00_6397;
												BgL_ez00_1942 = BgL_ez00_6395;
												BgL_dz00_1941 = BgL_dz00_6394;
												BgL_cz00_1940 = BgL_cz00_6393;
												BgL_bz00_1939 = BgL_bz00_6392;
												BgL_az00_1938 = BgL_az00_6390;
												goto BgL_zc3z04anonymousza31662ze3z87_1947;
											}
										}
									}
								}
							}
						else
							{	/* Unsafe/sha2.scm 934 */
								if ((BgL_jz00_1946 < 80L))
									{	/* Unsafe/sha2.scm 940 */
										uint64_t BgL_s0z00_1958;

										{	/* Unsafe/sha2.scm 940 */
											uint64_t BgL_arg1701z00_1976;

											{	/* Unsafe/sha2.scm 940 */
												long BgL_arg1702z00_1977;

												BgL_arg1702z00_1977 = (BgL_jz00_1946 + 1L);
												{	/* Unsafe/sha2.scm 901 */
													long BgL_tmpz00_6405;

													BgL_tmpz00_6405 = (BgL_arg1702z00_1977 & 15L);
													BgL_arg1701z00_1976 =
														BGL_U64VREF(BgL_bufferz00_53, BgL_tmpz00_6405);
											}}
											BgL_s0z00_1958 =
												(
												((BgL_arg1701z00_1976 >>
														(int) (1L)) |
													(BgL_arg1701z00_1976 <<
														(int) (
															(64L - 1L)))) ^
												(((BgL_arg1701z00_1976 >>
															(int) (8L)) |
														(BgL_arg1701z00_1976 <<
															(int) (
																(64L - 8L)))) ^
													(BgL_arg1701z00_1976 >> (int) (7L))));
										}
										{	/* Unsafe/sha2.scm 940 */
											uint64_t BgL_s1z00_1959;

											{	/* Unsafe/sha2.scm 941 */
												uint64_t BgL_arg1699z00_1974;

												{	/* Unsafe/sha2.scm 941 */
													long BgL_arg1700z00_1975;

													BgL_arg1700z00_1975 = (BgL_jz00_1946 + 14L);
													{	/* Unsafe/sha2.scm 901 */
														long BgL_tmpz00_6425;

														BgL_tmpz00_6425 = (BgL_arg1700z00_1975 & 15L);
														BgL_arg1699z00_1974 =
															BGL_U64VREF(BgL_bufferz00_53, BgL_tmpz00_6425);
												}}
												BgL_s1z00_1959 =
													(
													((BgL_arg1699z00_1974 >>
															(int) (19L)) |
														(BgL_arg1699z00_1974 <<
															(int) (
																(64L - 19L)))) ^
													(((BgL_arg1699z00_1974 >>
																(int) (61L)) |
															(BgL_arg1699z00_1974 <<
																(int) (
																	(64L - 61L)))) ^
														(BgL_arg1699z00_1974 >> (int) (6L))));
											}
											{	/* Unsafe/sha2.scm 941 */
												long BgL_ndxz00_1960;

												BgL_ndxz00_1960 =
													(BgL_jz00_1946 &
													LLONG_TO_LONG(((BGL_LONGLONG_T) 15)));
												{	/* Unsafe/sha2.scm 942 */
													uint64_t BgL_wz00_1961;

													{	/* Unsafe/sha2.scm 943 */
														uint64_t BgL_arg1685z00_1969;
														uint64_t BgL_arg1688z00_1970;

														{	/* Unsafe/sha2.scm 901 */
															long BgL_tmpz00_6446;

															BgL_tmpz00_6446 = (BgL_jz00_1946 & 15L);
															BgL_arg1685z00_1969 =
																BGL_U64VREF(BgL_bufferz00_53, BgL_tmpz00_6446);
														}
														{	/* Unsafe/sha2.scm 943 */
															uint64_t BgL_arg1689z00_1971;

															{	/* Unsafe/sha2.scm 943 */
																uint64_t BgL_arg1691z00_1972;

																{	/* Unsafe/sha2.scm 943 */
																	long BgL_arg1692z00_1973;

																	BgL_arg1692z00_1973 = (BgL_jz00_1946 + 9L);
																	{	/* Unsafe/sha2.scm 901 */
																		long BgL_tmpz00_6450;

																		BgL_tmpz00_6450 =
																			(BgL_arg1692z00_1973 & 15L);
																		BgL_arg1691z00_1972 =
																			BGL_U64VREF(BgL_bufferz00_53,
																			BgL_tmpz00_6450);
																}}
																BgL_arg1689z00_1971 =
																	(BgL_arg1691z00_1972 + BgL_s0z00_1958);
															}
															BgL_arg1688z00_1970 =
																(BgL_s1z00_1959 + BgL_arg1689z00_1971);
														}
														BgL_wz00_1961 =
															(BgL_arg1685z00_1969 + BgL_arg1688z00_1970);
													}
													{	/* Unsafe/sha2.scm 943 */
														uint64_t BgL_t1z00_1962;

														{	/* Unsafe/sha2.scm 898 */
															uint64_t BgL_arg1714z00_4157;

															{	/* Unsafe/sha2.scm 898 */
																uint64_t BgL_arg1715z00_4158;
																uint64_t BgL_arg1717z00_4159;

																BgL_arg1715z00_4158 =
																	BGl_Sigma1zd2512zd2zz__sha2z00(BgL_ez00_1942);
																{	/* Unsafe/sha2.scm 898 */
																	uint64_t BgL_arg1718z00_4160;
																	uint64_t BgL_arg1720z00_4161;

																	BgL_arg1718z00_4160 =
																		(
																		(BgL_ez00_1942 & BgL_fz00_1943) ^
																		(~(BgL_ez00_1942) & BgL_gz00_1944));
																	{	/* Unsafe/sha2.scm 898 */
																		uint64_t BgL_arg1722z00_4162;

																		BgL_arg1722z00_4162 =
																			BGL_U64VREF(BGl_K512z00zz__sha2z00,
																			BgL_jz00_1946);
																		BgL_arg1720z00_4161 =
																			(BgL_arg1722z00_4162 + BgL_wz00_1961);
																	}
																	BgL_arg1717z00_4159 =
																		(BgL_arg1718z00_4160 + BgL_arg1720z00_4161);
																}
																BgL_arg1714z00_4157 =
																	(BgL_arg1715z00_4158 + BgL_arg1717z00_4159);
															}
															BgL_t1z00_1962 =
																(BgL_hz00_1945 + BgL_arg1714z00_4157);
														}
														{	/* Unsafe/sha2.scm 944 */
															uint64_t BgL_t2z00_1963;

															BgL_t2z00_1963 =
																(
																(((BgL_az00_1938 >>
																			(int) (28L)) |
																		(BgL_az00_1938 <<
																			(int) (
																				(64L - 28L)))) ^
																	(((BgL_az00_1938 >>
																				(int) (34L)) |
																			(BgL_az00_1938 <<
																				(int) (
																					(64L - 34L)))) ^
																		((BgL_az00_1938 >>
																				(int) (39L)) |
																			(BgL_az00_1938 <<
																				(int) (
																					(64L - 39L)))))) +
																((BgL_az00_1938 & BgL_bz00_1939) ^
																	((BgL_az00_1938 & BgL_cz00_1940) ^
																		(BgL_bz00_1939 & BgL_cz00_1940))));
															{	/* Unsafe/sha2.scm 945 */

																BGL_U64VSET(BgL_bufferz00_53, BgL_ndxz00_1960,
																	BgL_wz00_1961);
																BUNSPEC;
																{
																	long BgL_jz00_6503;
																	uint64_t BgL_hz00_6502;
																	uint64_t BgL_gz00_6501;
																	uint64_t BgL_fz00_6500;
																	uint64_t BgL_ez00_6498;
																	uint64_t BgL_dz00_6497;
																	uint64_t BgL_cz00_6496;
																	uint64_t BgL_bz00_6495;
																	uint64_t BgL_az00_6493;

																	BgL_az00_6493 =
																		(BgL_t1z00_1962 + BgL_t2z00_1963);
																	BgL_bz00_6495 = BgL_az00_1938;
																	BgL_cz00_6496 = BgL_bz00_1939;
																	BgL_dz00_6497 = BgL_cz00_1940;
																	BgL_ez00_6498 =
																		(BgL_dz00_1941 + BgL_t1z00_1962);
																	BgL_fz00_6500 = BgL_ez00_1942;
																	BgL_gz00_6501 = BgL_fz00_1943;
																	BgL_hz00_6502 = BgL_gz00_1944;
																	BgL_jz00_6503 = (BgL_jz00_1946 + 1L);
																	BgL_jz00_1946 = BgL_jz00_6503;
																	BgL_hz00_1945 = BgL_hz00_6502;
																	BgL_gz00_1944 = BgL_gz00_6501;
																	BgL_fz00_1943 = BgL_fz00_6500;
																	BgL_ez00_1942 = BgL_ez00_6498;
																	BgL_dz00_1941 = BgL_dz00_6497;
																	BgL_cz00_1940 = BgL_cz00_6496;
																	BgL_bz00_1939 = BgL_bz00_6495;
																	BgL_az00_1938 = BgL_az00_6493;
																	goto BgL_zc3z04anonymousza31662ze3z87_1947;
																}
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/sha2.scm 939 */
										BgL_statez00_2011 = BgL_statez00_52;
										BgL_az00_2012 = BgL_az00_1938;
										BgL_bz00_2013 = BgL_bz00_1939;
										BgL_cz00_2014 = BgL_cz00_1940;
										BgL_dz00_2015 = BgL_dz00_1941;
										BgL_ez00_2016 = BgL_ez00_1942;
										BgL_fz00_2017 = BgL_fz00_1943;
										BgL_gz00_2018 = BgL_gz00_1944;
										BgL_hz00_2019 = BgL_hz00_1945;
										{	/* Unsafe/sha2.scm 906 */
											uint64_t BgL_oaz00_2021;
											uint64_t BgL_obz00_2022;
											uint64_t BgL_ocz00_2023;
											uint64_t BgL_odz00_2024;
											uint64_t BgL_oez00_2025;
											uint64_t BgL_ofz00_2026;
											uint64_t BgL_ogz00_2027;
											uint64_t BgL_ohz00_2028;

											BgL_oaz00_2021 = BGL_U64VREF(BgL_statez00_2011, 0L);
											BgL_obz00_2022 = BGL_U64VREF(BgL_statez00_2011, 1L);
											BgL_ocz00_2023 = BGL_U64VREF(BgL_statez00_2011, 2L);
											BgL_odz00_2024 = BGL_U64VREF(BgL_statez00_2011, 3L);
											BgL_oez00_2025 = BGL_U64VREF(BgL_statez00_2011, 4L);
											BgL_ofz00_2026 = BGL_U64VREF(BgL_statez00_2011, 5L);
											BgL_ogz00_2027 = BGL_U64VREF(BgL_statez00_2011, 6L);
											BgL_ohz00_2028 = BGL_U64VREF(BgL_statez00_2011, 7L);
											{	/* Unsafe/sha2.scm 915 */
												uint64_t BgL_arg1726z00_2030;

												BgL_arg1726z00_2030 = (BgL_oaz00_2021 + BgL_az00_2012);
												BGL_U64VSET(BgL_statez00_2011, 0L, BgL_arg1726z00_2030);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 916 */
												uint64_t BgL_arg1727z00_2031;

												BgL_arg1727z00_2031 = (BgL_obz00_2022 + BgL_bz00_2013);
												BGL_U64VSET(BgL_statez00_2011, 1L, BgL_arg1727z00_2031);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 917 */
												uint64_t BgL_arg1728z00_2032;

												BgL_arg1728z00_2032 = (BgL_ocz00_2023 + BgL_cz00_2014);
												BGL_U64VSET(BgL_statez00_2011, 2L, BgL_arg1728z00_2032);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 918 */
												uint64_t BgL_arg1729z00_2033;

												BgL_arg1729z00_2033 = (BgL_odz00_2024 + BgL_dz00_2015);
												BGL_U64VSET(BgL_statez00_2011, 3L, BgL_arg1729z00_2033);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 919 */
												uint64_t BgL_arg1730z00_2034;

												BgL_arg1730z00_2034 = (BgL_oez00_2025 + BgL_ez00_2016);
												BGL_U64VSET(BgL_statez00_2011, 4L, BgL_arg1730z00_2034);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 920 */
												uint64_t BgL_arg1731z00_2035;

												BgL_arg1731z00_2035 = (BgL_ofz00_2026 + BgL_fz00_2017);
												BGL_U64VSET(BgL_statez00_2011, 5L, BgL_arg1731z00_2035);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 921 */
												uint64_t BgL_arg1733z00_2036;

												BgL_arg1733z00_2036 = (BgL_ogz00_2027 + BgL_gz00_2018);
												BGL_U64VSET(BgL_statez00_2011, 6L, BgL_arg1733z00_2036);
												BUNSPEC;
											}
											{	/* Unsafe/sha2.scm 922 */
												uint64_t BgL_arg1734z00_2037;

												BgL_arg1734z00_2037 = (BgL_ohz00_2028 + BgL_hz00_2019);
												BGL_U64VSET(BgL_statez00_2011, 7L, BgL_arg1734z00_2037);
												BUNSPEC;
											}
										}
										return BgL_statez00_52;
									}
							}
					}
				}
			}
		}

	}



/* sha512-update */
	obj_t BGl_sha512zd2updatezd2zz__sha2z00(obj_t BgL_statez00_54,
		obj_t BgL_bufferz00_55, obj_t BgL_oz00_56, obj_t BgL_fillzd2wordz12zc0_57)
	{
		{	/* Unsafe/sha2.scm 955 */
			{
				obj_t BgL_bufferz00_2063;
				long BgL_iz00_2064;

				{
					long BgL_iz00_2046;
					long BgL_lz00_2047;

					BgL_iz00_2046 = 0L;
					BgL_lz00_2047 = 0L;
				BgL_zc3z04anonymousza31735ze3z87_2048:
					{	/* Unsafe/sha2.scm 975 */
						long BgL_bytesz00_2049;

						BgL_bufferz00_2063 = BgL_bufferz00_55;
						BgL_iz00_2064 = BgL_iz00_2046;
						{
							long BgL_jz00_4250;
							long BgL_iz00_4251;
							long BgL_nz00_4252;

							BgL_jz00_4250 = 0L;
							BgL_iz00_4251 = BgL_iz00_2064;
							BgL_nz00_4252 = 0L;
						BgL_loopz00_4249:
							if ((BgL_jz00_4250 < 16L))
								{	/* Unsafe/sha2.scm 964 */
									long BgL_arg1750z00_4255;
									long BgL_arg1751z00_4256;
									long BgL_arg1752z00_4257;

									BgL_arg1750z00_4255 = (BgL_jz00_4250 + 1L);
									BgL_arg1751z00_4256 = (BgL_iz00_4251 + 8L);
									{	/* Unsafe/sha2.scm 964 */
										long BgL_b1147z00_4260;

										BgL_b1147z00_4260 =
											((long (*)(obj_t, obj_t, long, obj_t,
													long))
											PROCEDURE_L_ENTRY(BgL_fillzd2wordz12zc0_57))
											(BgL_fillzd2wordz12zc0_57, BgL_bufferz00_2063,
											BgL_jz00_4250, BgL_oz00_56, BgL_iz00_4251);
										BgL_arg1752z00_4257 = (BgL_nz00_4252 + BgL_b1147z00_4260);
									}
									{
										long BgL_nz00_6543;
										long BgL_iz00_6542;
										long BgL_jz00_6541;

										BgL_jz00_6541 = BgL_arg1750z00_4255;
										BgL_iz00_6542 = BgL_arg1751z00_4256;
										BgL_nz00_6543 = BgL_arg1752z00_4257;
										BgL_nz00_4252 = BgL_nz00_6543;
										BgL_iz00_4251 = BgL_iz00_6542;
										BgL_jz00_4250 = BgL_jz00_6541;
										goto BgL_loopz00_4249;
									}
								}
							else
								{	/* Unsafe/sha2.scm 963 */
									BgL_bytesz00_2049 = BgL_nz00_4252;
								}
						}
						if ((BgL_bytesz00_2049 == 128L))
							{	/* Unsafe/sha2.scm 977 */
								BGl_sha512zd2internalzd2transformz00zz__sha2z00(BgL_statez00_54,
									BgL_bufferz00_55);
								{
									long BgL_lz00_6549;
									long BgL_iz00_6547;

									BgL_iz00_6547 = (BgL_iz00_2046 + 128L);
									BgL_lz00_6549 = (BgL_lz00_2047 + 128L);
									BgL_lz00_2047 = BgL_lz00_6549;
									BgL_iz00_2046 = BgL_iz00_6547;
									goto BgL_zc3z04anonymousza31735ze3z87_2048;
								}
							}
						else
							{	/* Unsafe/sha2.scm 977 */
								if (((128L - BgL_bytesz00_2049) >= 8L))
									{	/* Unsafe/sha2.scm 981 */
										{	/* Unsafe/sha2.scm 984 */
											uint64_t BgL_ulenz00_2055;

											{	/* Unsafe/sha2.scm 984 */
												long BgL_tmpz00_6554;

												BgL_tmpz00_6554 =
													(8L * ((BgL_lz00_2047 - 1L) + BgL_bytesz00_2049));
												BgL_ulenz00_2055 = (uint64_t) (BgL_tmpz00_6554);
											}
											BGL_U64VSET(BgL_bufferz00_55, 15L, BgL_ulenz00_2055);
											BUNSPEC;
										}
										return
											BGl_sha512zd2internalzd2transformz00zz__sha2z00
											(BgL_statez00_54, BgL_bufferz00_55);
									}
								else
									{	/* Unsafe/sha2.scm 981 */
										BGl_sha512zd2internalzd2transformz00zz__sha2z00
											(BgL_statez00_54, BgL_bufferz00_55);
										{
											long BgL_iz00_4288;

											BgL_iz00_4288 = 0L;
										BgL_loopz00_4287:
											if ((BgL_iz00_4288 < 15L))
												{	/* Unsafe/sha2.scm 969 */
													BGL_U64VSET(BgL_bufferz00_55, BgL_iz00_4288,
														(uint64_t) (0));
													BUNSPEC;
													{
														long BgL_iz00_6565;

														BgL_iz00_6565 = (BgL_iz00_4288 + 1L);
														BgL_iz00_4288 = BgL_iz00_6565;
														goto BgL_loopz00_4287;
													}
												}
											else
												{	/* Unsafe/sha2.scm 969 */
													((bool_t) 0);
												}
										}
										{	/* Unsafe/sha2.scm 991 */
											uint64_t BgL_ulenz00_2058;

											{	/* Unsafe/sha2.scm 991 */
												long BgL_tmpz00_6567;

												BgL_tmpz00_6567 =
													(8L * ((BgL_lz00_2047 - 1L) + BgL_bytesz00_2049));
												BgL_ulenz00_2058 = (uint64_t) (BgL_tmpz00_6567);
											}
											BGL_U64VSET(BgL_bufferz00_55, 15L, BgL_ulenz00_2058);
											BUNSPEC;
										}
										return
											BGl_sha512zd2internalzd2transformz00zz__sha2z00
											(BgL_statez00_54, BgL_bufferz00_55);
									}
							}
					}
				}
			}
		}

	}



/* sha512sum-mmap */
	BGL_EXPORTED_DEF obj_t BGl_sha512sumzd2mmapzd2zz__sha2z00(obj_t BgL_mmz00_58)
	{
		{	/* Unsafe/sha2.scm 998 */
			{	/* Unsafe/sha2.scm 1001 */
				obj_t BgL_fillzd2word64zd2mmapz12z12_4901;

				{
					int BgL_tmpz00_6574;

					BgL_tmpz00_6574 = (int) (1L);
					BgL_fillzd2word64zd2mmapz12z12_4901 =
						MAKE_L_PROCEDURE(BGl_z62fillzd2word64zd2mmapz12z70zz__sha2z00,
						BgL_tmpz00_6574);
				}
				PROCEDURE_L_SET(BgL_fillzd2word64zd2mmapz12z12_4901,
					(int) (0L), BgL_mmz00_58);
				{	/* Unsafe/sha2.scm 1045 */
					obj_t BgL_statez00_2092;
					obj_t BgL_bufferz00_2093;

					BgL_statez00_2092 =
						BGl_sha512zd2initialzd2hashzd2valuezd2zz__sha2z00();
					{	/* Llib/srfi4.scm 453 */

						BgL_bufferz00_2093 =
							BGl_makezd2u64vectorzd2zz__srfi4z00(16L, (uint64_t) (0));
					}
					BGl_sha512zd2updatezd2zz__sha2z00(BgL_statez00_2092,
						BgL_bufferz00_2093, BgL_mmz00_58,
						BgL_fillzd2word64zd2mmapz12z12_4901);
					return BGl_state64zd2ze3stringz31zz__sha2z00(BgL_statez00_2092);
				}
			}
		}

	}



/* &sha512sum-mmap */
	obj_t BGl_z62sha512sumzd2mmapzb0zz__sha2z00(obj_t BgL_envz00_4902,
		obj_t BgL_mmz00_4903)
	{
		{	/* Unsafe/sha2.scm 998 */
			{	/* Unsafe/sha2.scm 1001 */
				obj_t BgL_auxz00_6583;

				if (BGL_MMAPP(BgL_mmz00_4903))
					{	/* Unsafe/sha2.scm 1001 */
						BgL_auxz00_6583 = BgL_mmz00_4903;
					}
				else
					{
						obj_t BgL_auxz00_6586;

						BgL_auxz00_6586 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(41684L), BGl_string2389z00zz__sha2z00,
							BGl_string2380z00zz__sha2z00, BgL_mmz00_4903);
						FAILURE(BgL_auxz00_6586, BFALSE, BFALSE);
					}
				return BGl_sha512sumzd2mmapzd2zz__sha2z00(BgL_auxz00_6583);
			}
		}

	}



/* &fill-word64-mmap! */
	long BGl_z62fillzd2word64zd2mmapz12z70zz__sha2z00(obj_t BgL_envz00_4904,
		obj_t BgL_v64z00_4906, long BgL_iz00_4907, obj_t BgL_mmz00_4908,
		long BgL_nz00_4909)
	{
		{	/* Unsafe/sha2.scm 1043 */
			{	/* Unsafe/sha2.scm 1004 */
				obj_t BgL_mmz00_4905;

				BgL_mmz00_4905 = ((obj_t) PROCEDURE_L_REF(BgL_envz00_4904, (int) (0L)));
				{	/* Unsafe/sha2.scm 1004 */
					long BgL_lz00_5110;

					{	/* Unsafe/sha2.scm 1004 */
						obj_t BgL_tmpz00_6594;

						BgL_tmpz00_6594 = ((obj_t) BgL_mmz00_4908);
						BgL_lz00_5110 = BGL_MMAP_LENGTH(BgL_tmpz00_6594);
					}
					{	/* Unsafe/sha2.scm 1006 */
						bool_t BgL_test2467z00_6597;

						{	/* Unsafe/sha2.scm 1006 */
							long BgL_n2z00_5111;

							BgL_n2z00_5111 = (long) (BgL_lz00_5110);
							BgL_test2467z00_6597 = ((BgL_nz00_4909 + 8L) <= BgL_n2z00_5111);
						}
						if (BgL_test2467z00_6597)
							{	/* Unsafe/sha2.scm 1007 */
								uint32_t BgL_v0z00_5112;

								{	/* Unsafe/sha2.scm 1001 */
									unsigned char BgL_arg1759z00_5113;

									{	/* Unsafe/sha2.scm 1001 */
										long BgL_tmpz00_6601;

										BgL_tmpz00_6601 = (long) (BgL_nz00_4909);
										BgL_arg1759z00_5113 =
											BGL_MMAP_REF(BgL_mmz00_4905, BgL_tmpz00_6601);
									}
									{	/* Unsafe/sha2.scm 1001 */
										long BgL_tmpz00_6604;

										BgL_tmpz00_6604 = (BgL_arg1759z00_5113);
										BgL_v0z00_5112 = (uint32_t) (BgL_tmpz00_6604);
								}}
								{	/* Unsafe/sha2.scm 1007 */
									uint32_t BgL_v1z00_5114;

									{	/* Unsafe/sha2.scm 1008 */
										long BgL_arg1798z00_5115;

										BgL_arg1798z00_5115 = (BgL_nz00_4909 + 1L);
										{	/* Unsafe/sha2.scm 1001 */
											unsigned char BgL_arg1759z00_5116;

											{	/* Unsafe/sha2.scm 1001 */
												long BgL_tmpz00_6608;

												BgL_tmpz00_6608 = (long) (BgL_arg1798z00_5115);
												BgL_arg1759z00_5116 =
													BGL_MMAP_REF(BgL_mmz00_4905, BgL_tmpz00_6608);
											}
											{	/* Unsafe/sha2.scm 1001 */
												long BgL_tmpz00_6611;

												BgL_tmpz00_6611 = (BgL_arg1759z00_5116);
												BgL_v1z00_5114 = (uint32_t) (BgL_tmpz00_6611);
									}}}
									{	/* Unsafe/sha2.scm 1008 */
										uint32_t BgL_v2z00_5117;

										{	/* Unsafe/sha2.scm 1009 */
											long BgL_arg1797z00_5118;

											BgL_arg1797z00_5118 = (BgL_nz00_4909 + 2L);
											{	/* Unsafe/sha2.scm 1001 */
												unsigned char BgL_arg1759z00_5119;

												{	/* Unsafe/sha2.scm 1001 */
													long BgL_tmpz00_6615;

													BgL_tmpz00_6615 = (long) (BgL_arg1797z00_5118);
													BgL_arg1759z00_5119 =
														BGL_MMAP_REF(BgL_mmz00_4905, BgL_tmpz00_6615);
												}
												{	/* Unsafe/sha2.scm 1001 */
													long BgL_tmpz00_6618;

													BgL_tmpz00_6618 = (BgL_arg1759z00_5119);
													BgL_v2z00_5117 = (uint32_t) (BgL_tmpz00_6618);
										}}}
										{	/* Unsafe/sha2.scm 1009 */
											uint32_t BgL_v3z00_5120;

											{	/* Unsafe/sha2.scm 1010 */
												long BgL_arg1796z00_5121;

												BgL_arg1796z00_5121 = (BgL_nz00_4909 + 3L);
												{	/* Unsafe/sha2.scm 1001 */
													unsigned char BgL_arg1759z00_5122;

													{	/* Unsafe/sha2.scm 1001 */
														long BgL_tmpz00_6622;

														BgL_tmpz00_6622 = (long) (BgL_arg1796z00_5121);
														BgL_arg1759z00_5122 =
															BGL_MMAP_REF(BgL_mmz00_4905, BgL_tmpz00_6622);
													}
													{	/* Unsafe/sha2.scm 1001 */
														long BgL_tmpz00_6625;

														BgL_tmpz00_6625 = (BgL_arg1759z00_5122);
														BgL_v3z00_5120 = (uint32_t) (BgL_tmpz00_6625);
											}}}
											{	/* Unsafe/sha2.scm 1010 */
												uint32_t BgL_v4z00_5123;

												{	/* Unsafe/sha2.scm 1011 */
													long BgL_arg1795z00_5124;

													BgL_arg1795z00_5124 = (BgL_nz00_4909 + 4L);
													{	/* Unsafe/sha2.scm 1001 */
														unsigned char BgL_arg1759z00_5125;

														{	/* Unsafe/sha2.scm 1001 */
															long BgL_tmpz00_6629;

															BgL_tmpz00_6629 = (long) (BgL_arg1795z00_5124);
															BgL_arg1759z00_5125 =
																BGL_MMAP_REF(BgL_mmz00_4905, BgL_tmpz00_6629);
														}
														{	/* Unsafe/sha2.scm 1001 */
															long BgL_tmpz00_6632;

															BgL_tmpz00_6632 = (BgL_arg1759z00_5125);
															BgL_v4z00_5123 = (uint32_t) (BgL_tmpz00_6632);
												}}}
												{	/* Unsafe/sha2.scm 1011 */
													uint32_t BgL_v5z00_5126;

													{	/* Unsafe/sha2.scm 1012 */
														long BgL_arg1794z00_5127;

														BgL_arg1794z00_5127 = (BgL_nz00_4909 + 5L);
														{	/* Unsafe/sha2.scm 1001 */
															unsigned char BgL_arg1759z00_5128;

															{	/* Unsafe/sha2.scm 1001 */
																long BgL_tmpz00_6636;

																BgL_tmpz00_6636 = (long) (BgL_arg1794z00_5127);
																BgL_arg1759z00_5128 =
																	BGL_MMAP_REF(BgL_mmz00_4905, BgL_tmpz00_6636);
															}
															{	/* Unsafe/sha2.scm 1001 */
																long BgL_tmpz00_6639;

																BgL_tmpz00_6639 = (BgL_arg1759z00_5128);
																BgL_v5z00_5126 = (uint32_t) (BgL_tmpz00_6639);
													}}}
													{	/* Unsafe/sha2.scm 1012 */
														uint32_t BgL_v6z00_5129;

														{	/* Unsafe/sha2.scm 1013 */
															long BgL_arg1793z00_5130;

															BgL_arg1793z00_5130 = (BgL_nz00_4909 + 6L);
															{	/* Unsafe/sha2.scm 1001 */
																unsigned char BgL_arg1759z00_5131;

																{	/* Unsafe/sha2.scm 1001 */
																	long BgL_tmpz00_6643;

																	BgL_tmpz00_6643 =
																		(long) (BgL_arg1793z00_5130);
																	BgL_arg1759z00_5131 =
																		BGL_MMAP_REF(BgL_mmz00_4905,
																		BgL_tmpz00_6643);
																}
																{	/* Unsafe/sha2.scm 1001 */
																	long BgL_tmpz00_6646;

																	BgL_tmpz00_6646 = (BgL_arg1759z00_5131);
																	BgL_v6z00_5129 = (uint32_t) (BgL_tmpz00_6646);
														}}}
														{	/* Unsafe/sha2.scm 1013 */
															uint32_t BgL_v7z00_5132;

															{	/* Unsafe/sha2.scm 1014 */
																long BgL_arg1792z00_5133;

																BgL_arg1792z00_5133 = (BgL_nz00_4909 + 7L);
																{	/* Unsafe/sha2.scm 1001 */
																	unsigned char BgL_arg1759z00_5134;

																	{	/* Unsafe/sha2.scm 1001 */
																		long BgL_tmpz00_6650;

																		BgL_tmpz00_6650 =
																			(long) (BgL_arg1792z00_5133);
																		BgL_arg1759z00_5134 =
																			BGL_MMAP_REF(BgL_mmz00_4905,
																			BgL_tmpz00_6650);
																	}
																	{	/* Unsafe/sha2.scm 1001 */
																		long BgL_tmpz00_6653;

																		BgL_tmpz00_6653 = (BgL_arg1759z00_5134);
																		BgL_v7z00_5132 =
																			(uint32_t) (BgL_tmpz00_6653);
															}}}
															{	/* Unsafe/sha2.scm 1014 */
																uint64_t BgL_vz00_5135;

																{	/* Unsafe/sha2.scm 1015 */
																	uint64_t BgL_arg1763z00_5136;
																	uint64_t BgL_arg1764z00_5137;

																	{	/* Unsafe/sha2.scm 1015 */
																		uint64_t BgL_arg1765z00_5138;

																		{	/* Unsafe/sha2.scm 1015 */
																			long BgL_arg1766z00_5139;

																			{	/* Unsafe/sha2.scm 1015 */
																				long BgL_arg1767z00_5140;
																				long BgL_arg1768z00_5141;

																				{	/* Unsafe/sha2.scm 1015 */
																					uint32_t BgL_tmpz00_6656;

																					BgL_tmpz00_6656 =
																						(BgL_v0z00_5112 << (int) (8L));
																					BgL_arg1767z00_5140 =
																						(long) (BgL_tmpz00_6656);
																				}
																				BgL_arg1768z00_5141 =
																					(long) (BgL_v1z00_5114);
																				BgL_arg1766z00_5139 =
																					(BgL_arg1767z00_5140 |
																					BgL_arg1768z00_5141);
																			}
																			BgL_arg1765z00_5138 =
																				(uint64_t) (BgL_arg1766z00_5139);
																		}
																		BgL_arg1763z00_5136 =
																			(BgL_arg1765z00_5138 << (int) (48L));
																	}
																	{	/* Unsafe/sha2.scm 1015 */
																		uint64_t BgL_arg1770z00_5142;
																		uint64_t BgL_arg1771z00_5143;

																		{	/* Unsafe/sha2.scm 1015 */
																			uint64_t BgL_arg1772z00_5144;

																			{	/* Unsafe/sha2.scm 1015 */
																				long BgL_arg1773z00_5145;

																				{	/* Unsafe/sha2.scm 1015 */
																					long BgL_arg1774z00_5146;
																					long BgL_arg1775z00_5147;

																					{	/* Unsafe/sha2.scm 1015 */
																						uint32_t BgL_tmpz00_6665;

																						BgL_tmpz00_6665 =
																							(BgL_v2z00_5117 << (int) (8L));
																						BgL_arg1774z00_5146 =
																							(long) (BgL_tmpz00_6665);
																					}
																					BgL_arg1775z00_5147 =
																						(long) (BgL_v3z00_5120);
																					BgL_arg1773z00_5145 =
																						(BgL_arg1774z00_5146 |
																						BgL_arg1775z00_5147);
																				}
																				BgL_arg1772z00_5144 =
																					(uint64_t) (BgL_arg1773z00_5145);
																			}
																			BgL_arg1770z00_5142 =
																				(BgL_arg1772z00_5144 << (int) (32L));
																		}
																		{	/* Unsafe/sha2.scm 1016 */
																			uint64_t BgL_arg1779z00_5148;
																			uint64_t BgL_arg1781z00_5149;

																			{	/* Unsafe/sha2.scm 1016 */
																				uint64_t BgL_arg1782z00_5150;

																				{	/* Unsafe/sha2.scm 1016 */
																					long BgL_arg1783z00_5151;

																					{	/* Unsafe/sha2.scm 1016 */
																						long BgL_arg1785z00_5152;
																						long BgL_arg1786z00_5153;

																						{	/* Unsafe/sha2.scm 1016 */
																							uint32_t BgL_tmpz00_6674;

																							BgL_tmpz00_6674 =
																								(BgL_v4z00_5123 << (int) (8L));
																							BgL_arg1785z00_5152 =
																								(long) (BgL_tmpz00_6674);
																						}
																						BgL_arg1786z00_5153 =
																							(long) (BgL_v5z00_5126);
																						BgL_arg1783z00_5151 =
																							(BgL_arg1785z00_5152 |
																							BgL_arg1786z00_5153);
																					}
																					BgL_arg1782z00_5150 =
																						(uint64_t) (BgL_arg1783z00_5151);
																				}
																				BgL_arg1779z00_5148 =
																					(BgL_arg1782z00_5150 << (int) (16L));
																			}
																			{	/* Unsafe/sha2.scm 1016 */
																				long BgL_arg1788z00_5154;

																				{	/* Unsafe/sha2.scm 1016 */
																					long BgL_arg1789z00_5155;
																					long BgL_arg1790z00_5156;

																					{	/* Unsafe/sha2.scm 1016 */
																						uint32_t BgL_tmpz00_6683;

																						BgL_tmpz00_6683 =
																							(BgL_v6z00_5129 << (int) (8L));
																						BgL_arg1789z00_5155 =
																							(long) (BgL_tmpz00_6683);
																					}
																					BgL_arg1790z00_5156 =
																						(long) (BgL_v7z00_5132);
																					BgL_arg1788z00_5154 =
																						(BgL_arg1789z00_5155 |
																						BgL_arg1790z00_5156);
																				}
																				BgL_arg1781z00_5149 =
																					(uint64_t) (BgL_arg1788z00_5154);
																			}
																			BgL_arg1771z00_5143 =
																				(BgL_arg1779z00_5148 |
																				BgL_arg1781z00_5149);
																		}
																		BgL_arg1764z00_5137 =
																			(BgL_arg1770z00_5142 |
																			BgL_arg1771z00_5143);
																	}
																	BgL_vz00_5135 =
																		(BgL_arg1763z00_5136 | BgL_arg1764z00_5137);
																}
																{	/* Unsafe/sha2.scm 1015 */

																	BGL_U64VSET(BgL_v64z00_4906, BgL_iz00_4907,
																		BgL_vz00_5135);
																	BUNSPEC;
																	return 8L;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Unsafe/sha2.scm 1019 */
								bool_t BgL_test2468z00_6694;

								{	/* Unsafe/sha2.scm 1019 */
									long BgL_tmpz00_6695;

									{	/* Unsafe/sha2.scm 1019 */
										long BgL_za72za7_5157;

										BgL_za72za7_5157 = (long) (BgL_lz00_5110);
										BgL_tmpz00_6695 = (1L + BgL_za72za7_5157);
									}
									BgL_test2468z00_6694 = (BgL_nz00_4909 >= BgL_tmpz00_6695);
								}
								if (BgL_test2468z00_6694)
									{	/* Unsafe/sha2.scm 1019 */
										BGL_U64VSET(BgL_v64z00_4906, BgL_iz00_4907, (uint64_t) (0));
										BUNSPEC;
										return 0L;
									}
								else
									{	/* Unsafe/sha2.scm 1023 */
										obj_t BgL_vz00_5158;
										long BgL_kz00_5159;

										BgL_vz00_5158 =
											BGl_makezd2u32vectorzd2zz__srfi4z00(8L, (uint32_t) (0L));
										{	/* Unsafe/sha2.scm 1024 */
											long BgL_tmpz00_6702;

											{	/* Unsafe/sha2.scm 1024 */
												long BgL_za72za7_5160;

												BgL_za72za7_5160 = (long) (BgL_lz00_5110);
												BgL_tmpz00_6702 =
													((BgL_nz00_4909 + 8L) - BgL_za72za7_5160);
											}
											BgL_kz00_5159 = (8L - BgL_tmpz00_6702);
										}
										{
											long BgL_jz00_5162;

											BgL_jz00_5162 = 0L;
										BgL_loopz00_5161:
											if ((BgL_jz00_5162 == BgL_kz00_5159))
												{	/* Unsafe/sha2.scm 1026 */
													{	/* Unsafe/sha2.scm 1028 */
														uint32_t BgL_tmpz00_6709;

														BgL_tmpz00_6709 = (uint32_t) (128L);
														BGL_U32VSET(BgL_vz00_5158, BgL_jz00_5162,
															BgL_tmpz00_6709);
													}
													BUNSPEC;
													{	/* Unsafe/sha2.scm 1029 */
														uint32_t BgL_v0z00_5163;

														BgL_v0z00_5163 = BGL_U32VREF(BgL_vz00_5158, 0L);
														{	/* Unsafe/sha2.scm 1029 */
															uint32_t BgL_v1z00_5164;

															BgL_v1z00_5164 = BGL_U32VREF(BgL_vz00_5158, 1L);
															{	/* Unsafe/sha2.scm 1030 */
																uint32_t BgL_v2z00_5165;

																BgL_v2z00_5165 = BGL_U32VREF(BgL_vz00_5158, 2L);
																{	/* Unsafe/sha2.scm 1031 */
																	uint32_t BgL_v3z00_5166;

																	BgL_v3z00_5166 =
																		BGL_U32VREF(BgL_vz00_5158, 3L);
																	{	/* Unsafe/sha2.scm 1032 */
																		uint32_t BgL_v4z00_5167;

																		BgL_v4z00_5167 =
																			BGL_U32VREF(BgL_vz00_5158, 4L);
																		{	/* Unsafe/sha2.scm 1033 */
																			uint32_t BgL_v5z00_5168;

																			BgL_v5z00_5168 =
																				BGL_U32VREF(BgL_vz00_5158, 5L);
																			{	/* Unsafe/sha2.scm 1034 */
																				uint32_t BgL_v6z00_5169;

																				BgL_v6z00_5169 =
																					BGL_U32VREF(BgL_vz00_5158, 6L);
																				{	/* Unsafe/sha2.scm 1035 */
																					uint32_t BgL_v7z00_5170;

																					BgL_v7z00_5170 =
																						BGL_U32VREF(BgL_vz00_5158, 7L);
																					{	/* Unsafe/sha2.scm 1036 */
																						uint64_t BgL_vz00_5171;

																						{	/* Unsafe/sha2.scm 1037 */
																							uint64_t BgL_arg1803z00_5172;
																							uint64_t BgL_arg1804z00_5173;

																							{	/* Unsafe/sha2.scm 1037 */
																								uint64_t BgL_arg1805z00_5174;

																								{	/* Unsafe/sha2.scm 1037 */
																									long BgL_arg1806z00_5175;

																									{	/* Unsafe/sha2.scm 1037 */
																										long BgL_arg1807z00_5176;
																										long BgL_arg1808z00_5177;

																										{	/* Unsafe/sha2.scm 1037 */
																											uint32_t BgL_tmpz00_6720;

																											BgL_tmpz00_6720 =
																												(BgL_v0z00_5163 <<
																												(int) (8L));
																											BgL_arg1807z00_5176 =
																												(long)
																												(BgL_tmpz00_6720);
																										}
																										BgL_arg1808z00_5177 =
																											(long) (BgL_v1z00_5164);
																										BgL_arg1806z00_5175 =
																											(BgL_arg1807z00_5176 |
																											BgL_arg1808z00_5177);
																									}
																									BgL_arg1805z00_5174 =
																										(uint64_t)
																										(BgL_arg1806z00_5175);
																								}
																								BgL_arg1803z00_5172 =
																									(BgL_arg1805z00_5174 <<
																									(int) (48L));
																							}
																							{	/* Unsafe/sha2.scm 1037 */
																								uint64_t BgL_arg1810z00_5178;
																								uint64_t BgL_arg1811z00_5179;

																								{	/* Unsafe/sha2.scm 1037 */
																									uint64_t BgL_arg1812z00_5180;

																									{	/* Unsafe/sha2.scm 1037 */
																										long BgL_arg1813z00_5181;

																										{	/* Unsafe/sha2.scm 1037 */
																											long BgL_arg1814z00_5182;
																											long BgL_arg1815z00_5183;

																											{	/* Unsafe/sha2.scm 1037 */
																												uint32_t
																													BgL_tmpz00_6729;
																												BgL_tmpz00_6729 =
																													(BgL_v2z00_5165 <<
																													(int) (8L));
																												BgL_arg1814z00_5182 =
																													(long)
																													(BgL_tmpz00_6729);
																											}
																											BgL_arg1815z00_5183 =
																												(long) (BgL_v3z00_5166);
																											BgL_arg1813z00_5181 =
																												(BgL_arg1814z00_5182 |
																												BgL_arg1815z00_5183);
																										}
																										BgL_arg1812z00_5180 =
																											(uint64_t)
																											(BgL_arg1813z00_5181);
																									}
																									BgL_arg1810z00_5178 =
																										(BgL_arg1812z00_5180 <<
																										(int) (32L));
																								}
																								{	/* Unsafe/sha2.scm 1038 */
																									uint64_t BgL_arg1817z00_5184;
																									uint64_t BgL_arg1818z00_5185;

																									{	/* Unsafe/sha2.scm 1038 */
																										uint64_t
																											BgL_arg1819z00_5186;
																										{	/* Unsafe/sha2.scm 1038 */
																											long BgL_arg1820z00_5187;

																											{	/* Unsafe/sha2.scm 1038 */
																												long
																													BgL_arg1822z00_5188;
																												long
																													BgL_arg1823z00_5189;
																												{	/* Unsafe/sha2.scm 1038 */
																													uint32_t
																														BgL_tmpz00_6738;
																													BgL_tmpz00_6738 =
																														(BgL_v4z00_5167 <<
																														(int) (8L));
																													BgL_arg1822z00_5188 =
																														(long)
																														(BgL_tmpz00_6738);
																												}
																												BgL_arg1823z00_5189 =
																													(long)
																													(BgL_v5z00_5168);
																												BgL_arg1820z00_5187 =
																													(BgL_arg1822z00_5188 |
																													BgL_arg1823z00_5189);
																											}
																											BgL_arg1819z00_5186 =
																												(uint64_t)
																												(BgL_arg1820z00_5187);
																										}
																										BgL_arg1817z00_5184 =
																											(BgL_arg1819z00_5186 <<
																											(int) (16L));
																									}
																									{	/* Unsafe/sha2.scm 1038 */
																										long BgL_arg1827z00_5190;

																										{	/* Unsafe/sha2.scm 1038 */
																											long BgL_arg1828z00_5191;
																											long BgL_arg1829z00_5192;

																											{	/* Unsafe/sha2.scm 1038 */
																												uint32_t
																													BgL_tmpz00_6747;
																												BgL_tmpz00_6747 =
																													(BgL_v6z00_5169 <<
																													(int) (8L));
																												BgL_arg1828z00_5191 =
																													(long)
																													(BgL_tmpz00_6747);
																											}
																											BgL_arg1829z00_5192 =
																												(long) (BgL_v7z00_5170);
																											BgL_arg1827z00_5190 =
																												(BgL_arg1828z00_5191 |
																												BgL_arg1829z00_5192);
																										}
																										BgL_arg1818z00_5185 =
																											(uint64_t)
																											(BgL_arg1827z00_5190);
																									}
																									BgL_arg1811z00_5179 =
																										(BgL_arg1817z00_5184 |
																										BgL_arg1818z00_5185);
																								}
																								BgL_arg1804z00_5173 =
																									(BgL_arg1810z00_5178 |
																									BgL_arg1811z00_5179);
																							}
																							BgL_vz00_5171 =
																								(BgL_arg1803z00_5172 |
																								BgL_arg1804z00_5173);
																						}
																						{	/* Unsafe/sha2.scm 1037 */

																							BGL_U64VSET(BgL_v64z00_4906,
																								BgL_iz00_4907, BgL_vz00_5171);
																							BUNSPEC;
																							return (BgL_jz00_5162 + 1L);
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											else
												{	/* Unsafe/sha2.scm 1026 */
													{	/* Unsafe/sha2.scm 1042 */
														long BgL_arg1832z00_5193;

														{	/* Unsafe/sha2.scm 1042 */
															long BgL_arg1833z00_5194;

															BgL_arg1833z00_5194 =
																(BgL_nz00_4909 + BgL_jz00_5162);
															{	/* Unsafe/sha2.scm 1001 */
																unsigned char BgL_arg1759z00_5195;

																{	/* Unsafe/sha2.scm 1001 */
																	long BgL_tmpz00_6760;

																	BgL_tmpz00_6760 =
																		(long) (BgL_arg1833z00_5194);
																	BgL_arg1759z00_5195 =
																		BGL_MMAP_REF(BgL_mmz00_4905,
																		BgL_tmpz00_6760);
																}
																BgL_arg1832z00_5193 = (BgL_arg1759z00_5195);
														}}
														{	/* Unsafe/sha2.scm 1042 */
															uint32_t BgL_tmpz00_6764;

															BgL_tmpz00_6764 =
																(uint32_t) (BgL_arg1832z00_5193);
															BGL_U32VSET(BgL_vz00_5158, BgL_jz00_5162,
																BgL_tmpz00_6764);
														} BUNSPEC;
													}
													{
														long BgL_jz00_6767;

														BgL_jz00_6767 = (BgL_jz00_5162 + 1L);
														BgL_jz00_5162 = BgL_jz00_6767;
														goto BgL_loopz00_5161;
													}
												}
										}
									}
							}
					}
				}
			}
		}

	}



/* sha512sum-string */
	BGL_EXPORTED_DEF obj_t BGl_sha512sumzd2stringzd2zz__sha2z00(obj_t
		BgL_strz00_59)
	{
		{	/* Unsafe/sha2.scm 1053 */
			{	/* Unsafe/sha2.scm 1100 */
				obj_t BgL_statez00_2204;
				obj_t BgL_bufferz00_2205;

				BgL_statez00_2204 = BGl_sha512zd2initialzd2hashzd2valuezd2zz__sha2z00();
				{	/* Llib/srfi4.scm 453 */

					BgL_bufferz00_2205 =
						BGl_makezd2u64vectorzd2zz__srfi4z00(16L, (uint64_t) (0));
				}
				BGl_sha512zd2updatezd2zz__sha2z00(BgL_statez00_2204, BgL_bufferz00_2205,
					BgL_strz00_59, BGl_proc2390z00zz__sha2z00);
				return BGl_state64zd2ze3stringz31zz__sha2z00(BgL_statez00_2204);
			}
		}

	}



/* &sha512sum-string */
	obj_t BGl_z62sha512sumzd2stringzb0zz__sha2z00(obj_t BgL_envz00_4911,
		obj_t BgL_strz00_4912)
	{
		{	/* Unsafe/sha2.scm 1053 */
			{	/* Unsafe/sha2.scm 1056 */
				obj_t BgL_auxz00_6773;

				if (STRINGP(BgL_strz00_4912))
					{	/* Unsafe/sha2.scm 1056 */
						BgL_auxz00_6773 = BgL_strz00_4912;
					}
				else
					{
						obj_t BgL_auxz00_6776;

						BgL_auxz00_6776 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(43595L), BGl_string2391z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_strz00_4912);
						FAILURE(BgL_auxz00_6776, BFALSE, BFALSE);
					}
				return BGl_sha512sumzd2stringzd2zz__sha2z00(BgL_auxz00_6773);
			}
		}

	}



/* &fill-word64-string! */
	long BGl_z62fillzd2word64zd2stringz12z70zz__sha2z00(obj_t BgL_envz00_4913,
		obj_t BgL_v64z00_4914, long BgL_iz00_4915, obj_t BgL_strz00_4916,
		long BgL_nz00_4917)
	{
		{	/* Unsafe/sha2.scm 1098 */
			{	/* Unsafe/sha2.scm 1059 */
				long BgL_lz00_5200;

				BgL_lz00_5200 = STRING_LENGTH(((obj_t) BgL_strz00_4916));
				if (((BgL_nz00_4917 + 8L) <= BgL_lz00_5200))
					{	/* Unsafe/sha2.scm 1062 */
						uint32_t BgL_v0z00_5201;

						{	/* Unsafe/sha2.scm 1056 */
							long BgL_tmpz00_6786;

							BgL_tmpz00_6786 =
								(STRING_REF(((obj_t) BgL_strz00_4916), BgL_nz00_4917));
							BgL_v0z00_5201 = (uint32_t) (BgL_tmpz00_6786);
						}
						{	/* Unsafe/sha2.scm 1062 */
							uint32_t BgL_v1z00_5202;

							{	/* Unsafe/sha2.scm 1056 */
								long BgL_tmpz00_6791;

								BgL_tmpz00_6791 =
									(STRING_REF(((obj_t) BgL_strz00_4916), (BgL_nz00_4917 + 1L)));
								BgL_v1z00_5202 = (uint32_t) (BgL_tmpz00_6791);
							}
							{	/* Unsafe/sha2.scm 1063 */
								uint32_t BgL_v2z00_5203;

								{	/* Unsafe/sha2.scm 1056 */
									long BgL_tmpz00_6797;

									BgL_tmpz00_6797 =
										(STRING_REF(
											((obj_t) BgL_strz00_4916), (BgL_nz00_4917 + 2L)));
									BgL_v2z00_5203 = (uint32_t) (BgL_tmpz00_6797);
								}
								{	/* Unsafe/sha2.scm 1064 */
									uint32_t BgL_v3z00_5204;

									{	/* Unsafe/sha2.scm 1056 */
										long BgL_tmpz00_6803;

										BgL_tmpz00_6803 =
											(STRING_REF(
												((obj_t) BgL_strz00_4916), (BgL_nz00_4917 + 3L)));
										BgL_v3z00_5204 = (uint32_t) (BgL_tmpz00_6803);
									}
									{	/* Unsafe/sha2.scm 1065 */
										uint32_t BgL_v4z00_5205;

										{	/* Unsafe/sha2.scm 1056 */
											long BgL_tmpz00_6809;

											BgL_tmpz00_6809 =
												(STRING_REF(
													((obj_t) BgL_strz00_4916), (BgL_nz00_4917 + 4L)));
											BgL_v4z00_5205 = (uint32_t) (BgL_tmpz00_6809);
										}
										{	/* Unsafe/sha2.scm 1066 */
											uint32_t BgL_v5z00_5206;

											{	/* Unsafe/sha2.scm 1056 */
												long BgL_tmpz00_6815;

												BgL_tmpz00_6815 =
													(STRING_REF(
														((obj_t) BgL_strz00_4916), (BgL_nz00_4917 + 5L)));
												BgL_v5z00_5206 = (uint32_t) (BgL_tmpz00_6815);
											}
											{	/* Unsafe/sha2.scm 1067 */
												uint32_t BgL_v6z00_5207;

												{	/* Unsafe/sha2.scm 1056 */
													long BgL_tmpz00_6821;

													BgL_tmpz00_6821 =
														(STRING_REF(
															((obj_t) BgL_strz00_4916), (BgL_nz00_4917 + 6L)));
													BgL_v6z00_5207 = (uint32_t) (BgL_tmpz00_6821);
												}
												{	/* Unsafe/sha2.scm 1068 */
													uint32_t BgL_v7z00_5208;

													{	/* Unsafe/sha2.scm 1056 */
														long BgL_tmpz00_6827;

														BgL_tmpz00_6827 =
															(STRING_REF(
																((obj_t) BgL_strz00_4916),
																(BgL_nz00_4917 + 7L)));
														BgL_v7z00_5208 = (uint32_t) (BgL_tmpz00_6827);
													}
													{	/* Unsafe/sha2.scm 1069 */
														uint64_t BgL_vz00_5209;

														{	/* Unsafe/sha2.scm 1070 */
															uint64_t BgL_arg1846z00_5210;
															uint64_t BgL_arg1847z00_5211;

															{	/* Unsafe/sha2.scm 1070 */
																uint64_t BgL_arg1848z00_5212;

																{	/* Unsafe/sha2.scm 1070 */
																	long BgL_arg1849z00_5213;

																	{	/* Unsafe/sha2.scm 1070 */
																		long BgL_arg1850z00_5214;
																		long BgL_arg1851z00_5215;

																		{	/* Unsafe/sha2.scm 1070 */
																			uint32_t BgL_tmpz00_6833;

																			BgL_tmpz00_6833 =
																				(BgL_v0z00_5201 << (int) (8L));
																			BgL_arg1850z00_5214 =
																				(long) (BgL_tmpz00_6833);
																		}
																		BgL_arg1851z00_5215 =
																			(long) (BgL_v1z00_5202);
																		BgL_arg1849z00_5213 =
																			(BgL_arg1850z00_5214 |
																			BgL_arg1851z00_5215);
																	}
																	BgL_arg1848z00_5212 =
																		(uint64_t) (BgL_arg1849z00_5213);
																}
																BgL_arg1846z00_5210 =
																	(BgL_arg1848z00_5212 << (int) (48L));
															}
															{	/* Unsafe/sha2.scm 1070 */
																uint64_t BgL_arg1853z00_5216;
																uint64_t BgL_arg1854z00_5217;

																{	/* Unsafe/sha2.scm 1070 */
																	uint64_t BgL_arg1856z00_5218;

																	{	/* Unsafe/sha2.scm 1070 */
																		long BgL_arg1857z00_5219;

																		{	/* Unsafe/sha2.scm 1070 */
																			long BgL_arg1858z00_5220;
																			long BgL_arg1859z00_5221;

																			{	/* Unsafe/sha2.scm 1070 */
																				uint32_t BgL_tmpz00_6842;

																				BgL_tmpz00_6842 =
																					(BgL_v2z00_5203 << (int) (8L));
																				BgL_arg1858z00_5220 =
																					(long) (BgL_tmpz00_6842);
																			}
																			BgL_arg1859z00_5221 =
																				(long) (BgL_v3z00_5204);
																			BgL_arg1857z00_5219 =
																				(BgL_arg1858z00_5220 |
																				BgL_arg1859z00_5221);
																		}
																		BgL_arg1856z00_5218 =
																			(uint64_t) (BgL_arg1857z00_5219);
																	}
																	BgL_arg1853z00_5216 =
																		(BgL_arg1856z00_5218 << (int) (32L));
																}
																{	/* Unsafe/sha2.scm 1071 */
																	uint64_t BgL_arg1862z00_5222;
																	uint64_t BgL_arg1863z00_5223;

																	{	/* Unsafe/sha2.scm 1071 */
																		uint64_t BgL_arg1864z00_5224;

																		{	/* Unsafe/sha2.scm 1071 */
																			long BgL_arg1866z00_5225;

																			{	/* Unsafe/sha2.scm 1071 */
																				long BgL_arg1868z00_5226;
																				long BgL_arg1869z00_5227;

																				{	/* Unsafe/sha2.scm 1071 */
																					uint32_t BgL_tmpz00_6851;

																					BgL_tmpz00_6851 =
																						(BgL_v4z00_5205 << (int) (8L));
																					BgL_arg1868z00_5226 =
																						(long) (BgL_tmpz00_6851);
																				}
																				BgL_arg1869z00_5227 =
																					(long) (BgL_v5z00_5206);
																				BgL_arg1866z00_5225 =
																					(BgL_arg1868z00_5226 |
																					BgL_arg1869z00_5227);
																			}
																			BgL_arg1864z00_5224 =
																				(uint64_t) (BgL_arg1866z00_5225);
																		}
																		BgL_arg1862z00_5222 =
																			(BgL_arg1864z00_5224 << (int) (16L));
																	}
																	{	/* Unsafe/sha2.scm 1071 */
																		long BgL_arg1872z00_5228;

																		{	/* Unsafe/sha2.scm 1071 */
																			long BgL_arg1873z00_5229;
																			long BgL_arg1874z00_5230;

																			{	/* Unsafe/sha2.scm 1071 */
																				uint32_t BgL_tmpz00_6860;

																				BgL_tmpz00_6860 =
																					(BgL_v6z00_5207 << (int) (8L));
																				BgL_arg1873z00_5229 =
																					(long) (BgL_tmpz00_6860);
																			}
																			BgL_arg1874z00_5230 =
																				(long) (BgL_v7z00_5208);
																			BgL_arg1872z00_5228 =
																				(BgL_arg1873z00_5229 |
																				BgL_arg1874z00_5230);
																		}
																		BgL_arg1863z00_5223 =
																			(uint64_t) (BgL_arg1872z00_5228);
																	}
																	BgL_arg1854z00_5217 =
																		(BgL_arg1862z00_5222 | BgL_arg1863z00_5223);
																}
																BgL_arg1847z00_5211 =
																	(BgL_arg1853z00_5216 | BgL_arg1854z00_5217);
															}
															BgL_vz00_5209 =
																(BgL_arg1846z00_5210 | BgL_arg1847z00_5211);
														}
														{	/* Unsafe/sha2.scm 1070 */

															BGL_U64VSET(BgL_v64z00_4914, BgL_iz00_4915,
																BgL_vz00_5209);
															BUNSPEC;
															return 8L;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				else
					{	/* Unsafe/sha2.scm 1061 */
						if ((BgL_nz00_4917 >= (1L + BgL_lz00_5200)))
							{	/* Unsafe/sha2.scm 1074 */
								{	/* Unsafe/sha2.scm 1075 */
									uint64_t BgL_tmpz00_6874;

									BgL_tmpz00_6874 = (uint64_t) (0L);
									BGL_U64VSET(BgL_v64z00_4914, BgL_iz00_4915, BgL_tmpz00_6874);
								}
								BUNSPEC;
								return 0L;
							}
						else
							{	/* Unsafe/sha2.scm 1078 */
								obj_t BgL_vz00_5231;
								long BgL_kz00_5232;

								BgL_vz00_5231 =
									BGl_makezd2u32vectorzd2zz__srfi4z00(8L, (uint32_t) (0L));
								BgL_kz00_5232 = (8L - ((BgL_nz00_4917 + 8L) - BgL_lz00_5200));
								{
									long BgL_jz00_5234;

									BgL_jz00_5234 = 0L;
								BgL_loopz00_5233:
									if ((BgL_jz00_5234 == BgL_kz00_5232))
										{	/* Unsafe/sha2.scm 1081 */
											{	/* Unsafe/sha2.scm 1083 */
												uint32_t BgL_tmpz00_6884;

												BgL_tmpz00_6884 = (uint32_t) (128L);
												BGL_U32VSET(BgL_vz00_5231, BgL_jz00_5234,
													BgL_tmpz00_6884);
											}
											BUNSPEC;
											{	/* Unsafe/sha2.scm 1084 */
												uint32_t BgL_v0z00_5235;

												BgL_v0z00_5235 = BGL_U32VREF(BgL_vz00_5231, 0L);
												{	/* Unsafe/sha2.scm 1084 */
													uint32_t BgL_v1z00_5236;

													BgL_v1z00_5236 = BGL_U32VREF(BgL_vz00_5231, 1L);
													{	/* Unsafe/sha2.scm 1085 */
														uint32_t BgL_v2z00_5237;

														BgL_v2z00_5237 = BGL_U32VREF(BgL_vz00_5231, 2L);
														{	/* Unsafe/sha2.scm 1086 */
															uint32_t BgL_v3z00_5238;

															BgL_v3z00_5238 = BGL_U32VREF(BgL_vz00_5231, 3L);
															{	/* Unsafe/sha2.scm 1087 */
																uint32_t BgL_v4z00_5239;

																BgL_v4z00_5239 = BGL_U32VREF(BgL_vz00_5231, 4L);
																{	/* Unsafe/sha2.scm 1088 */
																	uint32_t BgL_v5z00_5240;

																	BgL_v5z00_5240 =
																		BGL_U32VREF(BgL_vz00_5231, 5L);
																	{	/* Unsafe/sha2.scm 1089 */
																		uint32_t BgL_v6z00_5241;

																		BgL_v6z00_5241 =
																			BGL_U32VREF(BgL_vz00_5231, 6L);
																		{	/* Unsafe/sha2.scm 1090 */
																			uint32_t BgL_v7z00_5242;

																			BgL_v7z00_5242 =
																				BGL_U32VREF(BgL_vz00_5231, 7L);
																			{	/* Unsafe/sha2.scm 1091 */
																				uint64_t BgL_vz00_5243;

																				{	/* Unsafe/sha2.scm 1092 */
																					uint64_t BgL_arg1888z00_5244;
																					uint64_t BgL_arg1889z00_5245;

																					{	/* Unsafe/sha2.scm 1092 */
																						uint64_t BgL_arg1890z00_5246;

																						{	/* Unsafe/sha2.scm 1092 */
																							long BgL_arg1891z00_5247;

																							{	/* Unsafe/sha2.scm 1092 */
																								long BgL_arg1892z00_5248;
																								long BgL_arg1893z00_5249;

																								{	/* Unsafe/sha2.scm 1092 */
																									uint32_t BgL_tmpz00_6895;

																									BgL_tmpz00_6895 =
																										(BgL_v0z00_5235 <<
																										(int) (8L));
																									BgL_arg1892z00_5248 =
																										(long) (BgL_tmpz00_6895);
																								}
																								BgL_arg1893z00_5249 =
																									(long) (BgL_v1z00_5236);
																								BgL_arg1891z00_5247 =
																									(BgL_arg1892z00_5248 |
																									BgL_arg1893z00_5249);
																							}
																							BgL_arg1890z00_5246 =
																								(uint64_t)
																								(BgL_arg1891z00_5247);
																						}
																						BgL_arg1888z00_5244 =
																							(BgL_arg1890z00_5246 <<
																							(int) (48L));
																					}
																					{	/* Unsafe/sha2.scm 1092 */
																						uint64_t BgL_arg1896z00_5250;
																						uint64_t BgL_arg1897z00_5251;

																						{	/* Unsafe/sha2.scm 1092 */
																							uint64_t BgL_arg1898z00_5252;

																							{	/* Unsafe/sha2.scm 1092 */
																								long BgL_arg1899z00_5253;

																								{	/* Unsafe/sha2.scm 1092 */
																									long BgL_arg1901z00_5254;
																									long BgL_arg1902z00_5255;

																									{	/* Unsafe/sha2.scm 1092 */
																										uint32_t BgL_tmpz00_6904;

																										BgL_tmpz00_6904 =
																											(BgL_v2z00_5237 <<
																											(int) (8L));
																										BgL_arg1901z00_5254 =
																											(long) (BgL_tmpz00_6904);
																									}
																									BgL_arg1902z00_5255 =
																										(long) (BgL_v3z00_5238);
																									BgL_arg1899z00_5253 =
																										(BgL_arg1901z00_5254 |
																										BgL_arg1902z00_5255);
																								}
																								BgL_arg1898z00_5252 =
																									(uint64_t)
																									(BgL_arg1899z00_5253);
																							}
																							BgL_arg1896z00_5250 =
																								(BgL_arg1898z00_5252 <<
																								(int) (32L));
																						}
																						{	/* Unsafe/sha2.scm 1093 */
																							uint64_t BgL_arg1904z00_5256;
																							uint64_t BgL_arg1906z00_5257;

																							{	/* Unsafe/sha2.scm 1093 */
																								uint64_t BgL_arg1910z00_5258;

																								{	/* Unsafe/sha2.scm 1093 */
																									long BgL_arg1911z00_5259;

																									{	/* Unsafe/sha2.scm 1093 */
																										long BgL_arg1912z00_5260;
																										long BgL_arg1913z00_5261;

																										{	/* Unsafe/sha2.scm 1093 */
																											uint32_t BgL_tmpz00_6913;

																											BgL_tmpz00_6913 =
																												(BgL_v4z00_5239 <<
																												(int) (8L));
																											BgL_arg1912z00_5260 =
																												(long)
																												(BgL_tmpz00_6913);
																										}
																										BgL_arg1913z00_5261 =
																											(long) (BgL_v5z00_5240);
																										BgL_arg1911z00_5259 =
																											(BgL_arg1912z00_5260 |
																											BgL_arg1913z00_5261);
																									}
																									BgL_arg1910z00_5258 =
																										(uint64_t)
																										(BgL_arg1911z00_5259);
																								}
																								BgL_arg1904z00_5256 =
																									(BgL_arg1910z00_5258 <<
																									(int) (16L));
																							}
																							{	/* Unsafe/sha2.scm 1093 */
																								long BgL_arg1916z00_5262;

																								{	/* Unsafe/sha2.scm 1093 */
																									long BgL_arg1917z00_5263;
																									long BgL_arg1918z00_5264;

																									{	/* Unsafe/sha2.scm 1093 */
																										uint32_t BgL_tmpz00_6922;

																										BgL_tmpz00_6922 =
																											(BgL_v6z00_5241 <<
																											(int) (8L));
																										BgL_arg1917z00_5263 =
																											(long) (BgL_tmpz00_6922);
																									}
																									BgL_arg1918z00_5264 =
																										(long) (BgL_v7z00_5242);
																									BgL_arg1916z00_5262 =
																										(BgL_arg1917z00_5263 |
																										BgL_arg1918z00_5264);
																								}
																								BgL_arg1906z00_5257 =
																									(uint64_t)
																									(BgL_arg1916z00_5262);
																							}
																							BgL_arg1897z00_5251 =
																								(BgL_arg1904z00_5256 |
																								BgL_arg1906z00_5257);
																						}
																						BgL_arg1889z00_5245 =
																							(BgL_arg1896z00_5250 |
																							BgL_arg1897z00_5251);
																					}
																					BgL_vz00_5243 =
																						(BgL_arg1888z00_5244 |
																						BgL_arg1889z00_5245);
																				}
																				{	/* Unsafe/sha2.scm 1092 */

																					BGL_U64VSET(BgL_v64z00_4914,
																						BgL_iz00_4915, BgL_vz00_5243);
																					BUNSPEC;
																					return (BgL_jz00_5234 + 1L);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									else
										{	/* Unsafe/sha2.scm 1081 */
											{	/* Unsafe/sha2.scm 1097 */
												long BgL_arg1920z00_5265;

												BgL_arg1920z00_5265 =
													(STRING_REF(
														((obj_t) BgL_strz00_4916),
														(BgL_nz00_4917 + BgL_jz00_5234)));
												{	/* Unsafe/sha2.scm 1097 */
													uint32_t BgL_tmpz00_6938;

													BgL_tmpz00_6938 = (uint32_t) (BgL_arg1920z00_5265);
													BGL_U32VSET(BgL_vz00_5231, BgL_jz00_5234,
														BgL_tmpz00_6938);
												} BUNSPEC;
											}
											{
												long BgL_jz00_6941;

												BgL_jz00_6941 = (BgL_jz00_5234 + 1L);
												BgL_jz00_5234 = BgL_jz00_6941;
												goto BgL_loopz00_5233;
											}
										}
								}
							}
					}
			}
		}

	}



/* sha512sum-port */
	BGL_EXPORTED_DEF obj_t BGl_sha512sumzd2portzd2zz__sha2z00(obj_t BgL_pz00_60)
	{
		{	/* Unsafe/sha2.scm 1108 */
			{	/* Unsafe/sha2.scm 1108 */
				obj_t BgL_lenz00_4929;

				BgL_lenz00_4929 = MAKE_CELL(BINT(0L));
				{	/* Unsafe/sha2.scm 1110 */
					obj_t BgL_bufz00_2315;

					{	/* Llib/srfi4.scm 451 */

						BgL_bufz00_2315 =
							BGl_makezd2u32vectorzd2zz__srfi4z00(8L, (uint32_t) (0));
					}
					{	/* Unsafe/sha2.scm 1131 */
						obj_t BgL_fillzd2word64zd2portz12z12_4918;

						{
							int BgL_tmpz00_6945;

							BgL_tmpz00_6945 = (int) (2L);
							BgL_fillzd2word64zd2portz12z12_4918 =
								MAKE_L_PROCEDURE(BGl_z62fillzd2word64zd2portz12z70zz__sha2z00,
								BgL_tmpz00_6945);
						}
						PROCEDURE_L_SET(BgL_fillzd2word64zd2portz12z12_4918,
							(int) (0L), BgL_bufz00_2315);
						PROCEDURE_L_SET(BgL_fillzd2word64zd2portz12z12_4918,
							(int) (1L), ((obj_t) BgL_lenz00_4929));
						{	/* Unsafe/sha2.scm 1173 */
							obj_t BgL_statez00_2318;
							obj_t BgL_bufferz00_2319;

							BgL_statez00_2318 =
								BGl_sha512zd2initialzd2hashzd2valuezd2zz__sha2z00();
							{	/* Llib/srfi4.scm 453 */

								BgL_bufferz00_2319 =
									BGl_makezd2u64vectorzd2zz__srfi4z00(16L, (uint64_t) (0));
							}
							BGl_sha512zd2updatezd2zz__sha2z00(BgL_statez00_2318,
								BgL_bufferz00_2319, BgL_pz00_60,
								BgL_fillzd2word64zd2portz12z12_4918);
							return BGl_state64zd2ze3stringz31zz__sha2z00(BgL_statez00_2318);
						}
					}
				}
			}
		}

	}



/* &sha512sum-port */
	obj_t BGl_z62sha512sumzd2portzb0zz__sha2z00(obj_t BgL_envz00_4919,
		obj_t BgL_pz00_4920)
	{
		{	/* Unsafe/sha2.scm 1108 */
			{	/* Unsafe/sha2.scm 1108 */
				obj_t BgL_auxz00_6957;

				if (INPUT_PORTP(BgL_pz00_4920))
					{	/* Unsafe/sha2.scm 1108 */
						BgL_auxz00_6957 = BgL_pz00_4920;
					}
				else
					{
						obj_t BgL_auxz00_6960;

						BgL_auxz00_6960 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(45445L), BGl_string2392z00zz__sha2z00,
							BGl_string2385z00zz__sha2z00, BgL_pz00_4920);
						FAILURE(BgL_auxz00_6960, BFALSE, BFALSE);
					}
				return BGl_sha512sumzd2portzd2zz__sha2z00(BgL_auxz00_6957);
			}
		}

	}



/* &fill-word64-port! */
	long BGl_z62fillzd2word64zd2portz12z70zz__sha2z00(obj_t BgL_envz00_4921,
		obj_t BgL_v64z00_4924, long BgL_iz00_4925, obj_t BgL_pz00_4926,
		long BgL_nz00_4927)
	{
		{	/* Unsafe/sha2.scm 1171 */
			{	/* Unsafe/sha2.scm 1131 */
				obj_t BgL_bufz00_4922;
				obj_t BgL_lenz00_4923;

				BgL_bufz00_4922 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_4921, (int) (0L)));
				BgL_lenz00_4923 = PROCEDURE_L_REF(BgL_envz00_4921, (int) (1L));
				{
					obj_t BgL_pz00_5271;

					{	/* Unsafe/sha2.scm 1131 */
						long BgL_lz00_5278;

						BgL_pz00_5271 = ((obj_t) BgL_pz00_4926);
						{
							long BgL_iz00_5273;

							BgL_iz00_5273 = 0L;
						BgL_loopz00_5272:
							if ((BgL_iz00_5273 == 4L))
								{	/* Unsafe/sha2.scm 1116 */
									BgL_lz00_5278 = BgL_iz00_5273;
								}
							else
								{	/* Unsafe/sha2.scm 1118 */
									obj_t BgL_cz00_5274;

									BgL_cz00_5274 =
										BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_pz00_5271);
									if (EOF_OBJECTP(BgL_cz00_5274))
										{
											long BgL_jz00_5276;

											BgL_jz00_5276 = BgL_iz00_5273;
										BgL_liipz00_5275:
											if ((BgL_jz00_5276 == 4L))
												{	/* Unsafe/sha2.scm 1121 */
													BgL_lz00_5278 = BgL_iz00_5273;
												}
											else
												{	/* Unsafe/sha2.scm 1121 */
													{	/* Unsafe/sha2.scm 1124 */
														uint32_t BgL_tmpz00_6977;

														BgL_tmpz00_6977 = (uint32_t) (0L);
														BGL_U32VSET(BgL_bufz00_4922, BgL_jz00_5276,
															BgL_tmpz00_6977);
													}
													BUNSPEC;
													{
														long BgL_jz00_6980;

														BgL_jz00_6980 = (BgL_jz00_5276 + 1L);
														BgL_jz00_5276 = BgL_jz00_6980;
														goto BgL_liipz00_5275;
													}
												}
										}
									else
										{	/* Unsafe/sha2.scm 1119 */
											{	/* Unsafe/sha2.scm 1127 */
												uint32_t BgL_arg2000z00_5277;

												{	/* Unsafe/sha2.scm 1127 */
													char BgL_tmpz00_6982;

													BgL_tmpz00_6982 = (signed char) CINT(BgL_cz00_5274);
													BgL_arg2000z00_5277 = (uint32_t) (BgL_tmpz00_6982);
												}
												BGL_U32VSET(BgL_bufz00_4922, BgL_iz00_5273,
													BgL_arg2000z00_5277);
												BUNSPEC;
											}
											{
												long BgL_iz00_6986;

												BgL_iz00_6986 = (BgL_iz00_5273 + 1L);
												BgL_iz00_5273 = BgL_iz00_6986;
												goto BgL_loopz00_5272;
											}
										}
								}
						}
						{	/* Unsafe/sha2.scm 1132 */
							obj_t BgL_auxz00_5279;

							BgL_auxz00_5279 =
								ADDFX(CELL_REF(BgL_lenz00_4923), BINT(BgL_lz00_5278));
							CELL_SET(BgL_lenz00_4923, BgL_auxz00_5279);
						}
						if (((BgL_nz00_4927 + 8L) <= BgL_lz00_5278))
							{	/* Unsafe/sha2.scm 1135 */
								uint32_t BgL_v0z00_5280;

								BgL_v0z00_5280 = BGL_U32VREF(BgL_bufz00_4922, 0L);
								{	/* Unsafe/sha2.scm 1135 */
									uint32_t BgL_v1z00_5281;

									BgL_v1z00_5281 = BGL_U32VREF(BgL_bufz00_4922, 1L);
									{	/* Unsafe/sha2.scm 1136 */
										uint32_t BgL_v2z00_5282;

										BgL_v2z00_5282 = BGL_U32VREF(BgL_bufz00_4922, 2L);
										{	/* Unsafe/sha2.scm 1137 */
											uint32_t BgL_v3z00_5283;

											BgL_v3z00_5283 = BGL_U32VREF(BgL_bufz00_4922, 3L);
											{	/* Unsafe/sha2.scm 1138 */
												uint32_t BgL_v4z00_5284;

												BgL_v4z00_5284 = BGL_U32VREF(BgL_bufz00_4922, 4L);
												{	/* Unsafe/sha2.scm 1139 */
													uint32_t BgL_v5z00_5285;

													BgL_v5z00_5285 = BGL_U32VREF(BgL_bufz00_4922, 5L);
													{	/* Unsafe/sha2.scm 1140 */
														uint32_t BgL_v6z00_5286;

														BgL_v6z00_5286 = BGL_U32VREF(BgL_bufz00_4922, 6L);
														{	/* Unsafe/sha2.scm 1141 */
															uint32_t BgL_v7z00_5287;

															BgL_v7z00_5287 = BGL_U32VREF(BgL_bufz00_4922, 7L);
															{	/* Unsafe/sha2.scm 1142 */
																uint64_t BgL_vz00_5288;

																{	/* Unsafe/sha2.scm 1143 */
																	uint64_t BgL_arg1933z00_5289;
																	uint64_t BgL_arg1934z00_5290;

																	{	/* Unsafe/sha2.scm 1143 */
																		uint64_t BgL_arg1935z00_5291;

																		{	/* Unsafe/sha2.scm 1143 */
																			long BgL_arg1936z00_5292;

																			{	/* Unsafe/sha2.scm 1143 */
																				long BgL_arg1937z00_5293;
																				long BgL_arg1938z00_5294;

																				{	/* Unsafe/sha2.scm 1143 */
																					uint32_t BgL_tmpz00_7002;

																					BgL_tmpz00_7002 =
																						(BgL_v0z00_5280 << (int) (8L));
																					BgL_arg1937z00_5293 =
																						(long) (BgL_tmpz00_7002);
																				}
																				BgL_arg1938z00_5294 =
																					(long) (BgL_v1z00_5281);
																				BgL_arg1936z00_5292 =
																					(BgL_arg1937z00_5293 |
																					BgL_arg1938z00_5294);
																			}
																			BgL_arg1935z00_5291 =
																				(uint64_t) (BgL_arg1936z00_5292);
																		}
																		BgL_arg1933z00_5289 =
																			(BgL_arg1935z00_5291 << (int) (48L));
																	}
																	{	/* Unsafe/sha2.scm 1143 */
																		uint64_t BgL_arg1940z00_5295;
																		uint64_t BgL_arg1941z00_5296;

																		{	/* Unsafe/sha2.scm 1143 */
																			uint64_t BgL_arg1942z00_5297;

																			{	/* Unsafe/sha2.scm 1143 */
																				long BgL_arg1943z00_5298;

																				{	/* Unsafe/sha2.scm 1143 */
																					long BgL_arg1944z00_5299;
																					long BgL_arg1945z00_5300;

																					{	/* Unsafe/sha2.scm 1143 */
																						uint32_t BgL_tmpz00_7011;

																						BgL_tmpz00_7011 =
																							(BgL_v2z00_5282 << (int) (8L));
																						BgL_arg1944z00_5299 =
																							(long) (BgL_tmpz00_7011);
																					}
																					BgL_arg1945z00_5300 =
																						(long) (BgL_v3z00_5283);
																					BgL_arg1943z00_5298 =
																						(BgL_arg1944z00_5299 |
																						BgL_arg1945z00_5300);
																				}
																				BgL_arg1942z00_5297 =
																					(uint64_t) (BgL_arg1943z00_5298);
																			}
																			BgL_arg1940z00_5295 =
																				(BgL_arg1942z00_5297 << (int) (32L));
																		}
																		{	/* Unsafe/sha2.scm 1144 */
																			uint64_t BgL_arg1947z00_5301;
																			uint64_t BgL_arg1948z00_5302;

																			{	/* Unsafe/sha2.scm 1144 */
																				uint64_t BgL_arg1949z00_5303;

																				{	/* Unsafe/sha2.scm 1144 */
																					long BgL_arg1950z00_5304;

																					{	/* Unsafe/sha2.scm 1144 */
																						long BgL_arg1951z00_5305;
																						long BgL_arg1952z00_5306;

																						{	/* Unsafe/sha2.scm 1144 */
																							uint32_t BgL_tmpz00_7020;

																							BgL_tmpz00_7020 =
																								(BgL_v4z00_5284 << (int) (8L));
																							BgL_arg1951z00_5305 =
																								(long) (BgL_tmpz00_7020);
																						}
																						BgL_arg1952z00_5306 =
																							(long) (BgL_v5z00_5285);
																						BgL_arg1950z00_5304 =
																							(BgL_arg1951z00_5305 |
																							BgL_arg1952z00_5306);
																					}
																					BgL_arg1949z00_5303 =
																						(uint64_t) (BgL_arg1950z00_5304);
																				}
																				BgL_arg1947z00_5301 =
																					(BgL_arg1949z00_5303 << (int) (16L));
																			}
																			{	/* Unsafe/sha2.scm 1144 */
																				long BgL_arg1954z00_5307;

																				{	/* Unsafe/sha2.scm 1144 */
																					long BgL_arg1955z00_5308;
																					long BgL_arg1956z00_5309;

																					{	/* Unsafe/sha2.scm 1144 */
																						uint32_t BgL_tmpz00_7029;

																						BgL_tmpz00_7029 =
																							(BgL_v6z00_5286 << (int) (8L));
																						BgL_arg1955z00_5308 =
																							(long) (BgL_tmpz00_7029);
																					}
																					BgL_arg1956z00_5309 =
																						(long) (BgL_v7z00_5287);
																					BgL_arg1954z00_5307 =
																						(BgL_arg1955z00_5308 |
																						BgL_arg1956z00_5309);
																				}
																				BgL_arg1948z00_5302 =
																					(uint64_t) (BgL_arg1954z00_5307);
																			}
																			BgL_arg1941z00_5296 =
																				(BgL_arg1947z00_5301 |
																				BgL_arg1948z00_5302);
																		}
																		BgL_arg1934z00_5290 =
																			(BgL_arg1940z00_5295 |
																			BgL_arg1941z00_5296);
																	}
																	BgL_vz00_5288 =
																		(BgL_arg1933z00_5289 | BgL_arg1934z00_5290);
																}
																{	/* Unsafe/sha2.scm 1143 */

																	BGL_U64VSET(BgL_v64z00_4924, BgL_iz00_4925,
																		BgL_vz00_5288);
																	BUNSPEC;
																	return 8L;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						else
							{	/* Unsafe/sha2.scm 1134 */
								if ((BgL_nz00_4927 >= (1L + BgL_lz00_5278)))
									{	/* Unsafe/sha2.scm 1147 */
										{	/* Unsafe/sha2.scm 1148 */
											uint64_t BgL_tmpz00_7043;

											BgL_tmpz00_7043 = (uint64_t) (0L);
											BGL_U64VSET(BgL_v64z00_4924, BgL_iz00_4925,
												BgL_tmpz00_7043);
										}
										BUNSPEC;
										return 0L;
									}
								else
									{	/* Unsafe/sha2.scm 1151 */
										obj_t BgL_vz00_5310;
										long BgL_kz00_5311;

										BgL_vz00_5310 =
											BGl_makezd2u32vectorzd2zz__srfi4z00(8L, (uint32_t) (0L));
										BgL_kz00_5311 =
											(8L - ((BgL_nz00_4927 + 8L) - BgL_lz00_5278));
										{
											long BgL_jz00_5313;

											BgL_jz00_5313 = 0L;
										BgL_loopz00_5312:
											if ((BgL_jz00_5313 == BgL_kz00_5311))
												{	/* Unsafe/sha2.scm 1154 */
													{	/* Unsafe/sha2.scm 1156 */
														uint32_t BgL_tmpz00_7053;

														BgL_tmpz00_7053 = (uint32_t) (128L);
														BGL_U32VSET(BgL_vz00_5310, BgL_jz00_5313,
															BgL_tmpz00_7053);
													}
													BUNSPEC;
													{	/* Unsafe/sha2.scm 1157 */
														uint32_t BgL_v0z00_5314;

														BgL_v0z00_5314 = BGL_U32VREF(BgL_vz00_5310, 0L);
														{	/* Unsafe/sha2.scm 1157 */
															uint32_t BgL_v1z00_5315;

															BgL_v1z00_5315 = BGL_U32VREF(BgL_vz00_5310, 1L);
															{	/* Unsafe/sha2.scm 1158 */
																uint32_t BgL_v2z00_5316;

																BgL_v2z00_5316 = BGL_U32VREF(BgL_vz00_5310, 2L);
																{	/* Unsafe/sha2.scm 1159 */
																	uint32_t BgL_v3z00_5317;

																	BgL_v3z00_5317 =
																		BGL_U32VREF(BgL_vz00_5310, 3L);
																	{	/* Unsafe/sha2.scm 1160 */
																		uint32_t BgL_v4z00_5318;

																		BgL_v4z00_5318 =
																			BGL_U32VREF(BgL_vz00_5310, 4L);
																		{	/* Unsafe/sha2.scm 1161 */
																			uint32_t BgL_v5z00_5319;

																			BgL_v5z00_5319 =
																				BGL_U32VREF(BgL_vz00_5310, 5L);
																			{	/* Unsafe/sha2.scm 1162 */
																				uint32_t BgL_v6z00_5320;

																				BgL_v6z00_5320 =
																					BGL_U32VREF(BgL_vz00_5310, 6L);
																				{	/* Unsafe/sha2.scm 1163 */
																					uint32_t BgL_v7z00_5321;

																					BgL_v7z00_5321 =
																						BGL_U32VREF(BgL_vz00_5310, 7L);
																					{	/* Unsafe/sha2.scm 1164 */
																						uint64_t BgL_vz00_5322;

																						{	/* Unsafe/sha2.scm 1165 */
																							uint64_t BgL_arg1962z00_5323;
																							uint64_t BgL_arg1963z00_5324;

																							{	/* Unsafe/sha2.scm 1165 */
																								uint64_t BgL_arg1964z00_5325;

																								{	/* Unsafe/sha2.scm 1165 */
																									long BgL_arg1965z00_5326;

																									{	/* Unsafe/sha2.scm 1165 */
																										long BgL_arg1966z00_5327;
																										long BgL_arg1967z00_5328;

																										{	/* Unsafe/sha2.scm 1165 */
																											uint32_t BgL_tmpz00_7064;

																											BgL_tmpz00_7064 =
																												(BgL_v0z00_5314 <<
																												(int) (8L));
																											BgL_arg1966z00_5327 =
																												(long)
																												(BgL_tmpz00_7064);
																										}
																										BgL_arg1967z00_5328 =
																											(long) (BgL_v1z00_5315);
																										BgL_arg1965z00_5326 =
																											(BgL_arg1966z00_5327 |
																											BgL_arg1967z00_5328);
																									}
																									BgL_arg1964z00_5325 =
																										(uint64_t)
																										(BgL_arg1965z00_5326);
																								}
																								BgL_arg1962z00_5323 =
																									(BgL_arg1964z00_5325 <<
																									(int) (48L));
																							}
																							{	/* Unsafe/sha2.scm 1165 */
																								uint64_t BgL_arg1969z00_5329;
																								uint64_t BgL_arg1970z00_5330;

																								{	/* Unsafe/sha2.scm 1165 */
																									uint64_t BgL_arg1971z00_5331;

																									{	/* Unsafe/sha2.scm 1165 */
																										long BgL_arg1972z00_5332;

																										{	/* Unsafe/sha2.scm 1165 */
																											long BgL_arg1973z00_5333;
																											long BgL_arg1974z00_5334;

																											{	/* Unsafe/sha2.scm 1165 */
																												uint32_t
																													BgL_tmpz00_7073;
																												BgL_tmpz00_7073 =
																													(BgL_v2z00_5316 <<
																													(int) (8L));
																												BgL_arg1973z00_5333 =
																													(long)
																													(BgL_tmpz00_7073);
																											}
																											BgL_arg1974z00_5334 =
																												(long) (BgL_v3z00_5317);
																											BgL_arg1972z00_5332 =
																												(BgL_arg1973z00_5333 |
																												BgL_arg1974z00_5334);
																										}
																										BgL_arg1971z00_5331 =
																											(uint64_t)
																											(BgL_arg1972z00_5332);
																									}
																									BgL_arg1969z00_5329 =
																										(BgL_arg1971z00_5331 <<
																										(int) (32L));
																								}
																								{	/* Unsafe/sha2.scm 1166 */
																									uint64_t BgL_arg1976z00_5335;
																									uint64_t BgL_arg1977z00_5336;

																									{	/* Unsafe/sha2.scm 1166 */
																										uint64_t
																											BgL_arg1978z00_5337;
																										{	/* Unsafe/sha2.scm 1166 */
																											long BgL_arg1979z00_5338;

																											{	/* Unsafe/sha2.scm 1166 */
																												long
																													BgL_arg1980z00_5339;
																												long
																													BgL_arg1981z00_5340;
																												{	/* Unsafe/sha2.scm 1166 */
																													uint32_t
																														BgL_tmpz00_7082;
																													BgL_tmpz00_7082 =
																														(BgL_v4z00_5318 <<
																														(int) (8L));
																													BgL_arg1980z00_5339 =
																														(long)
																														(BgL_tmpz00_7082);
																												}
																												BgL_arg1981z00_5340 =
																													(long)
																													(BgL_v5z00_5319);
																												BgL_arg1979z00_5338 =
																													(BgL_arg1980z00_5339 |
																													BgL_arg1981z00_5340);
																											}
																											BgL_arg1978z00_5337 =
																												(uint64_t)
																												(BgL_arg1979z00_5338);
																										}
																										BgL_arg1976z00_5335 =
																											(BgL_arg1978z00_5337 <<
																											(int) (16L));
																									}
																									{	/* Unsafe/sha2.scm 1166 */
																										long BgL_arg1983z00_5341;

																										{	/* Unsafe/sha2.scm 1166 */
																											long BgL_arg1984z00_5342;
																											long BgL_arg1985z00_5343;

																											{	/* Unsafe/sha2.scm 1166 */
																												uint32_t
																													BgL_tmpz00_7091;
																												BgL_tmpz00_7091 =
																													(BgL_v6z00_5320 <<
																													(int) (8L));
																												BgL_arg1984z00_5342 =
																													(long)
																													(BgL_tmpz00_7091);
																											}
																											BgL_arg1985z00_5343 =
																												(long) (BgL_v7z00_5321);
																											BgL_arg1983z00_5341 =
																												(BgL_arg1984z00_5342 |
																												BgL_arg1985z00_5343);
																										}
																										BgL_arg1977z00_5336 =
																											(uint64_t)
																											(BgL_arg1983z00_5341);
																									}
																									BgL_arg1970z00_5330 =
																										(BgL_arg1976z00_5335 |
																										BgL_arg1977z00_5336);
																								}
																								BgL_arg1963z00_5324 =
																									(BgL_arg1969z00_5329 |
																									BgL_arg1970z00_5330);
																							}
																							BgL_vz00_5322 =
																								(BgL_arg1962z00_5323 |
																								BgL_arg1963z00_5324);
																						}
																						{	/* Unsafe/sha2.scm 1165 */

																							BGL_U64VSET(BgL_v64z00_4924,
																								BgL_iz00_4925, BgL_vz00_5322);
																							BUNSPEC;
																							return (BgL_jz00_5313 + 1L);
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											else
												{	/* Unsafe/sha2.scm 1154 */
													{	/* Unsafe/sha2.scm 1170 */
														uint32_t BgL_arg1987z00_5344;

														BgL_arg1987z00_5344 =
															BGL_U32VREF(BgL_bufz00_4922, BgL_jz00_5313);
														BGL_U32VSET(BgL_vz00_5310, BgL_jz00_5313,
															BgL_arg1987z00_5344);
														BUNSPEC;
													}
													{
														long BgL_jz00_7105;

														BgL_jz00_7105 = (BgL_jz00_5313 + 1L);
														BgL_jz00_5313 = BgL_jz00_7105;
														goto BgL_loopz00_5312;
													}
												}
										}
									}
							}
					}
				}
			}
		}

	}



/* sha512sum-file */
	BGL_EXPORTED_DEF obj_t BGl_sha512sumzd2filezd2zz__sha2z00(obj_t
		BgL_fnamez00_61)
	{
		{	/* Unsafe/sha2.scm 1181 */
			{	/* Unsafe/sha2.scm 1182 */
				obj_t BgL_mmz00_2434;

				BgL_mmz00_2434 =
					BGl_openzd2mmapzd2zz__mmapz00(BgL_fnamez00_61, BTRUE, BFALSE);
				if (BGL_MMAPP(BgL_mmz00_2434))
					{	/* Unsafe/sha2.scm 1184 */
						obj_t BgL_exitd1091z00_2436;

						BgL_exitd1091z00_2436 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Unsafe/sha2.scm 1186 */
							obj_t BgL_zc3z04anonymousza32003ze3z87_4931;

							BgL_zc3z04anonymousza32003ze3z87_4931 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza32003ze3ze5zz__sha2z00, (int) (0L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza32003ze3z87_4931, (int) (0L),
								BgL_mmz00_2434);
							{	/* Unsafe/sha2.scm 1184 */
								obj_t BgL_arg2351z00_4624;

								{	/* Unsafe/sha2.scm 1184 */
									obj_t BgL_arg2352z00_4625;

									BgL_arg2352z00_4625 =
										BGL_EXITD_PROTECT(BgL_exitd1091z00_2436);
									BgL_arg2351z00_4624 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32003ze3z87_4931,
										BgL_arg2352z00_4625);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1091z00_2436,
									BgL_arg2351z00_4624);
								BUNSPEC;
							}
							{	/* Unsafe/sha2.scm 1185 */
								obj_t BgL_tmp1093z00_2438;

								BgL_tmp1093z00_2438 =
									BGl_sha512sumzd2mmapzd2zz__sha2z00(BgL_mmz00_2434);
								{	/* Unsafe/sha2.scm 1184 */
									bool_t BgL_test2482z00_7120;

									{	/* Unsafe/sha2.scm 1184 */
										obj_t BgL_arg2350z00_4627;

										BgL_arg2350z00_4627 =
											BGL_EXITD_PROTECT(BgL_exitd1091z00_2436);
										BgL_test2482z00_7120 = PAIRP(BgL_arg2350z00_4627);
									}
									if (BgL_test2482z00_7120)
										{	/* Unsafe/sha2.scm 1184 */
											obj_t BgL_arg2348z00_4628;

											{	/* Unsafe/sha2.scm 1184 */
												obj_t BgL_arg2349z00_4629;

												BgL_arg2349z00_4629 =
													BGL_EXITD_PROTECT(BgL_exitd1091z00_2436);
												BgL_arg2348z00_4628 =
													CDR(((obj_t) BgL_arg2349z00_4629));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1091z00_2436,
												BgL_arg2348z00_4628);
											BUNSPEC;
										}
									else
										{	/* Unsafe/sha2.scm 1184 */
											BFALSE;
										}
								}
								bgl_close_mmap(BgL_mmz00_2434);
								return BgL_tmp1093z00_2438;
							}
						}
					}
				else
					{	/* Unsafe/sha2.scm 1187 */
						obj_t BgL_pz00_2441;

						{	/* Ieee/port.scm 466 */

							BgL_pz00_2441 =
								BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_fnamez00_61, BTRUE, BINT(5000000L));
						}
						{	/* Unsafe/sha2.scm 1188 */
							obj_t BgL_exitd1095z00_2442;

							BgL_exitd1095z00_2442 = BGL_EXITD_TOP_AS_OBJ();
							{	/* Unsafe/sha2.scm 1190 */
								obj_t BgL_zc3z04anonymousza32004ze3z87_4932;

								BgL_zc3z04anonymousza32004ze3z87_4932 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza32004ze3ze5zz__sha2z00, (int) (0L),
									(int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza32004ze3z87_4932, (int) (0L),
									BgL_pz00_2441);
								{	/* Unsafe/sha2.scm 1188 */
									obj_t BgL_arg2351z00_4633;

									{	/* Unsafe/sha2.scm 1188 */
										obj_t BgL_arg2352z00_4634;

										BgL_arg2352z00_4634 =
											BGL_EXITD_PROTECT(BgL_exitd1095z00_2442);
										BgL_arg2351z00_4633 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza32004ze3z87_4932,
											BgL_arg2352z00_4634);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1095z00_2442,
										BgL_arg2351z00_4633);
									BUNSPEC;
								}
								{	/* Unsafe/sha2.scm 1189 */
									obj_t BgL_tmp1097z00_2444;

									BgL_tmp1097z00_2444 =
										BGl_sha512sumzd2portzd2zz__sha2z00(BgL_pz00_2441);
									{	/* Unsafe/sha2.scm 1188 */
										bool_t BgL_test2483z00_7140;

										{	/* Unsafe/sha2.scm 1188 */
											obj_t BgL_arg2350z00_4636;

											BgL_arg2350z00_4636 =
												BGL_EXITD_PROTECT(BgL_exitd1095z00_2442);
											BgL_test2483z00_7140 = PAIRP(BgL_arg2350z00_4636);
										}
										if (BgL_test2483z00_7140)
											{	/* Unsafe/sha2.scm 1188 */
												obj_t BgL_arg2348z00_4637;

												{	/* Unsafe/sha2.scm 1188 */
													obj_t BgL_arg2349z00_4638;

													BgL_arg2349z00_4638 =
														BGL_EXITD_PROTECT(BgL_exitd1095z00_2442);
													BgL_arg2348z00_4637 =
														CDR(((obj_t) BgL_arg2349z00_4638));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1095z00_2442,
													BgL_arg2348z00_4637);
												BUNSPEC;
											}
										else
											{	/* Unsafe/sha2.scm 1188 */
												BFALSE;
											}
									}
									bgl_close_input_port(((obj_t) BgL_pz00_2441));
									return BgL_tmp1097z00_2444;
								}
							}
						}
					}
			}
		}

	}



/* &sha512sum-file */
	obj_t BGl_z62sha512sumzd2filezb0zz__sha2z00(obj_t BgL_envz00_4933,
		obj_t BgL_fnamez00_4934)
	{
		{	/* Unsafe/sha2.scm 1181 */
			{	/* Unsafe/sha2.scm 1182 */
				obj_t BgL_auxz00_7149;

				if (STRINGP(BgL_fnamez00_4934))
					{	/* Unsafe/sha2.scm 1182 */
						BgL_auxz00_7149 = BgL_fnamez00_4934;
					}
				else
					{
						obj_t BgL_auxz00_7152;

						BgL_auxz00_7152 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(47709L), BGl_string2393z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_fnamez00_4934);
						FAILURE(BgL_auxz00_7152, BFALSE, BFALSE);
					}
				return BGl_sha512sumzd2filezd2zz__sha2z00(BgL_auxz00_7149);
			}
		}

	}



/* &<@anonymous:2003> */
	obj_t BGl_z62zc3z04anonymousza32003ze3ze5zz__sha2z00(obj_t BgL_envz00_4935)
	{
		{	/* Unsafe/sha2.scm 1184 */
			{	/* Unsafe/sha2.scm 1186 */
				obj_t BgL_mmz00_4936;

				BgL_mmz00_4936 = ((obj_t) PROCEDURE_REF(BgL_envz00_4935, (int) (0L)));
				return bgl_close_mmap(BgL_mmz00_4936);
			}
		}

	}



/* &<@anonymous:2004> */
	obj_t BGl_z62zc3z04anonymousza32004ze3ze5zz__sha2z00(obj_t BgL_envz00_4937)
	{
		{	/* Unsafe/sha2.scm 1188 */
			{	/* Unsafe/sha2.scm 1190 */
				obj_t BgL_pz00_4938;

				BgL_pz00_4938 = PROCEDURE_REF(BgL_envz00_4937, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_pz00_4938));
			}
		}

	}



/* sha512sum */
	BGL_EXPORTED_DEF obj_t BGl_sha512sumz00zz__sha2z00(obj_t BgL_objz00_62)
	{
		{	/* Unsafe/sha2.scm 1195 */
			if (BGL_MMAPP(BgL_objz00_62))
				{	/* Unsafe/sha2.scm 1197 */
					return BGl_sha512sumzd2mmapzd2zz__sha2z00(BgL_objz00_62);
				}
			else
				{	/* Unsafe/sha2.scm 1197 */
					if (STRINGP(BgL_objz00_62))
						{	/* Unsafe/sha2.scm 1199 */
							return BGl_sha512sumzd2stringzd2zz__sha2z00(BgL_objz00_62);
						}
					else
						{	/* Unsafe/sha2.scm 1199 */
							if (INPUT_PORTP(BgL_objz00_62))
								{	/* Unsafe/sha2.scm 1201 */
									return BGl_sha512sumzd2portzd2zz__sha2z00(BgL_objz00_62);
								}
							else
								{	/* Unsafe/sha2.scm 1201 */
									return
										BGl_errorz00zz__errorz00(BGl_string2394z00zz__sha2z00,
										BGl_string2388z00zz__sha2z00, BgL_objz00_62);
								}
						}
				}
		}

	}



/* &sha512sum */
	obj_t BGl_z62sha512sumz62zz__sha2z00(obj_t BgL_envz00_4939,
		obj_t BgL_objz00_4940)
	{
		{	/* Unsafe/sha2.scm 1195 */
			return BGl_sha512sumz00zz__sha2z00(BgL_objz00_4940);
		}

	}



/* hmac-sha256sum-string */
	BGL_EXPORTED_DEF obj_t BGl_hmaczd2sha256sumzd2stringz00zz__sha2z00(obj_t
		BgL_keyz00_63, obj_t BgL_msgz00_64)
	{
		{	/* Unsafe/sha2.scm 1209 */
			return
				BGl_hmaczd2stringzd2zz__hmacz00(BgL_keyz00_63, BgL_msgz00_64,
				BGl_sha256sumzd2stringzd2envz00zz__sha2z00);
		}

	}



/* &hmac-sha256sum-string */
	obj_t BGl_z62hmaczd2sha256sumzd2stringz62zz__sha2z00(obj_t BgL_envz00_4941,
		obj_t BgL_keyz00_4942, obj_t BgL_msgz00_4943)
	{
		{	/* Unsafe/sha2.scm 1209 */
			{	/* Unsafe/sha2.scm 1210 */
				obj_t BgL_auxz00_7184;
				obj_t BgL_auxz00_7177;

				if (STRINGP(BgL_msgz00_4943))
					{	/* Unsafe/sha2.scm 1210 */
						BgL_auxz00_7184 = BgL_msgz00_4943;
					}
				else
					{
						obj_t BgL_auxz00_7187;

						BgL_auxz00_7187 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(48688L), BGl_string2395z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_msgz00_4943);
						FAILURE(BgL_auxz00_7187, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_keyz00_4942))
					{	/* Unsafe/sha2.scm 1210 */
						BgL_auxz00_7177 = BgL_keyz00_4942;
					}
				else
					{
						obj_t BgL_auxz00_7180;

						BgL_auxz00_7180 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(48688L), BGl_string2395z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_keyz00_4942);
						FAILURE(BgL_auxz00_7180, BFALSE, BFALSE);
					}
				return
					BGl_hmaczd2sha256sumzd2stringz00zz__sha2z00(BgL_auxz00_7177,
					BgL_auxz00_7184);
			}
		}

	}



/* hmac-sha512sum-string */
	BGL_EXPORTED_DEF obj_t BGl_hmaczd2sha512sumzd2stringz00zz__sha2z00(obj_t
		BgL_keyz00_65, obj_t BgL_msgz00_66)
	{
		{	/* Unsafe/sha2.scm 1215 */
			return
				BGl_hmaczd2stringzd2zz__hmacz00(BgL_keyz00_65, BgL_msgz00_66,
				BGl_sha512sumzd2stringzd2envz00zz__sha2z00);
		}

	}



/* &hmac-sha512sum-string */
	obj_t BGl_z62hmaczd2sha512sumzd2stringz62zz__sha2z00(obj_t BgL_envz00_4944,
		obj_t BgL_keyz00_4945, obj_t BgL_msgz00_4946)
	{
		{	/* Unsafe/sha2.scm 1215 */
			{	/* Unsafe/sha2.scm 1216 */
				obj_t BgL_auxz00_7200;
				obj_t BgL_auxz00_7193;

				if (STRINGP(BgL_msgz00_4946))
					{	/* Unsafe/sha2.scm 1216 */
						BgL_auxz00_7200 = BgL_msgz00_4946;
					}
				else
					{
						obj_t BgL_auxz00_7203;

						BgL_auxz00_7203 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(48994L), BGl_string2396z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_msgz00_4946);
						FAILURE(BgL_auxz00_7203, BFALSE, BFALSE);
					}
				if (STRINGP(BgL_keyz00_4945))
					{	/* Unsafe/sha2.scm 1216 */
						BgL_auxz00_7193 = BgL_keyz00_4945;
					}
				else
					{
						obj_t BgL_auxz00_7196;

						BgL_auxz00_7196 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2378z00zz__sha2z00,
							BINT(48994L), BGl_string2396z00zz__sha2z00,
							BGl_string2383z00zz__sha2z00, BgL_keyz00_4945);
						FAILURE(BgL_auxz00_7196, BFALSE, BFALSE);
					}
				return
					BGl_hmaczd2sha512sumzd2stringz00zz__sha2z00(BgL_auxz00_7193,
					BgL_auxz00_7200);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 50 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 50 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 50 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__sha2z00(void)
	{
		{	/* Unsafe/sha2.scm 50 */
			BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2397z00zz__sha2z00));
			BGl_modulezd2initializa7ationz75zz__hmacz00(285132844L,
				BSTRING_TO_STRING(BGl_string2397z00zz__sha2z00));
			BGl_modulezd2initializa7ationz75zz__tvectorz00(135276775L,
				BSTRING_TO_STRING(BGl_string2397z00zz__sha2z00));
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(86988580L,
				BSTRING_TO_STRING(BGl_string2397z00zz__sha2z00));
			return
				BGl_modulezd2initializa7ationz75zz__srfi4z00(467924964L,
				BSTRING_TO_STRING(BGl_string2397z00zz__sha2z00));
		}

	}

#ifdef __cplusplus
}
#endif
