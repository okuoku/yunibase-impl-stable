/*===========================================================================*/
/*   (Unsafe/crc16.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/crc16.scm -indent -o objs/obj_u/Unsafe/crc16.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___CRC16_TYPE_DEFINITIONS
#define BGL___CRC16_TYPE_DEFINITIONS
#endif													// BGL___CRC16_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62crc16zd2portzb0zz__crc16z00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zz__crc16z00 = BUNSPEC;
	static obj_t BGl_z62crc16zd2mmapzb0zz__crc16z00(obj_t, obj_t);
	static long BGl_crczd2valuezd2zz__crc16z00(long, long);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__crc16z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zz__crc16z00(void);
	static obj_t BGl_genericzd2initzd2zz__crc16z00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__crc16z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__crc16z00(void);
	static obj_t BGl_objectzd2initzd2zz__crc16z00(void);
	BGL_EXPORTED_DECL int BGl_crc16z00zz__crc16z00(obj_t);
	extern obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62crc16zd2stringzb0zz__crc16z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31226ze3ze5zz__crc16z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__crc16z00(void);
	static obj_t BGl_z62crc16z62zz__crc16z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_crc16zd2stringzd2zz__crc16z00(obj_t);
	extern obj_t BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int BGl_crc16zd2filezd2zz__crc16z00(obj_t);
	extern obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
	BGL_EXPORTED_DECL int BGl_crc16zd2portzd2zz__crc16z00(obj_t);
	static obj_t BGl_symbol1660z00zz__crc16z00 = BUNSPEC;
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62crc16zd2filezb0zz__crc16z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_crc16zd2mmapzd2zz__crc16z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_crc16zd2portzd2envz00zz__crc16z00,
		BgL_bgl_za762crc16za7d2portza71673za7, BGl_z62crc16zd2portzb0zz__crc16z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crc16zd2envzd2zz__crc16z00,
		BgL_bgl_za762crc16za762za7za7__c1674z00, BGl_z62crc16z62zz__crc16z00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crc16zd2stringzd2envz00zz__crc16z00,
		BgL_bgl_za762crc16za7d2strin1675z00, BGl_z62crc16zd2stringzb0zz__crc16z00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crc16zd2mmapzd2envz00zz__crc16z00,
		BgL_bgl_za762crc16za7d2mmapza71676za7, BGl_z62crc16zd2mmapzb0zz__crc16z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1661z00zz__crc16z00,
		BgL_bgl_string1661za700za7za7_1677za7, "crc16", 5);
	      DEFINE_STRING(BGl_string1662z00zz__crc16z00,
		BgL_bgl_string1662za700za7za7_1678za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string1663z00zz__crc16z00,
		BgL_bgl_string1663za700za7za7_1679za7,
		"/tmp/bigloo/runtime/Unsafe/crc16.scm", 36);
	      DEFINE_STRING(BGl_string1664z00zz__crc16z00,
		BgL_bgl_string1664za700za7za7_1680za7, "&crc16-mmap", 11);
	      DEFINE_STRING(BGl_string1665z00zz__crc16z00,
		BgL_bgl_string1665za700za7za7_1681za7, "mmap", 4);
	      DEFINE_STRING(BGl_string1666z00zz__crc16z00,
		BgL_bgl_string1666za700za7za7_1682za7, "&crc16-string", 13);
	      DEFINE_STRING(BGl_string1667z00zz__crc16z00,
		BgL_bgl_string1667za700za7za7_1683za7, "bstring", 7);
	      DEFINE_STRING(BGl_string1668z00zz__crc16z00,
		BgL_bgl_string1668za700za7za7_1684za7, "&crc16-port", 11);
	      DEFINE_STRING(BGl_string1669z00zz__crc16z00,
		BgL_bgl_string1669za700za7za7_1685za7, "input-port", 10);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_crc16zd2filezd2envz00zz__crc16z00,
		BgL_bgl_za762crc16za7d2fileza71686za7, BGl_z62crc16zd2filezb0zz__crc16z00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1671z00zz__crc16z00,
		BgL_bgl_string1671za700za7za7_1687za7, "&crc16-file", 11);
	      DEFINE_STRING(BGl_string1672z00zz__crc16z00,
		BgL_bgl_string1672za700za7za7_1688za7, "__crc16", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1670z00zz__crc16z00,
		BgL_bgl_za762za7c3za704anonymo1689za7,
		BGl_z62zc3z04anonymousza31226ze3ze5zz__crc16z00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__crc16z00));
		     ADD_ROOT((void *) (&BGl_symbol1660z00zz__crc16z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__crc16z00(long
		BgL_checksumz00_2175, char *BgL_fromz00_2176)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__crc16z00))
				{
					BGl_requirezd2initializa7ationz75zz__crc16z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__crc16z00();
					BGl_cnstzd2initzd2zz__crc16z00();
					BGl_importedzd2moduleszd2initz00zz__crc16z00();
					return BGl_methodzd2initzd2zz__crc16z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zz__crc16z00(void)
	{
		{	/* Unsafe/crc16.scm 17 */
			return (BGl_symbol1660z00zz__crc16z00 =
				bstring_to_symbol(BGl_string1661z00zz__crc16z00), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__crc16z00(void)
	{
		{	/* Unsafe/crc16.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* crc16 */
	BGL_EXPORTED_DEF int BGl_crc16z00zz__crc16z00(obj_t BgL_objz00_3)
	{
		{	/* Unsafe/crc16.scm 66 */
			if (BGL_MMAPP(BgL_objz00_3))
				{	/* Unsafe/crc16.scm 68 */
					return BGl_crc16zd2mmapzd2zz__crc16z00(BgL_objz00_3);
				}
			else
				{	/* Unsafe/crc16.scm 68 */
					if (STRINGP(BgL_objz00_3))
						{	/* Unsafe/crc16.scm 70 */
							return BGl_crc16zd2stringzd2zz__crc16z00(BgL_objz00_3);
						}
					else
						{	/* Unsafe/crc16.scm 70 */
							if (INPUT_PORTP(BgL_objz00_3))
								{	/* Unsafe/crc16.scm 72 */
									return BGl_crc16zd2portzd2zz__crc16z00(BgL_objz00_3);
								}
							else
								{	/* Unsafe/crc16.scm 72 */
									return
										CINT(BGl_errorz00zz__errorz00(BGl_symbol1660z00zz__crc16z00,
											BGl_string1662z00zz__crc16z00, BgL_objz00_3));
								}
						}
				}
		}

	}



/* &crc16 */
	obj_t BGl_z62crc16z62zz__crc16z00(obj_t BgL_envz00_2153,
		obj_t BgL_objz00_2154)
	{
		{	/* Unsafe/crc16.scm 66 */
			return BINT(BGl_crc16z00zz__crc16z00(BgL_objz00_2154));
		}

	}



/* crc-value */
	long BGl_crczd2valuezd2zz__crc16z00(long BgL_valz00_4, long BgL_crcz00_5)
	{
		{	/* Unsafe/crc16.scm 80 */
			{
				long BgL_iz00_1809;
				long BgL_valuez00_1810;
				long BgL_crcz00_1811;

				BgL_iz00_1809 = 0L;
				BgL_valuez00_1810 = (BgL_valz00_4 << (int) (8L));
				BgL_crcz00_1811 = BgL_crcz00_5;
			BgL_loopz00_1808:
				if ((BgL_iz00_1809 == 8L))
					{	/* Unsafe/crc16.scm 84 */
						return BgL_crcz00_1811;
					}
				else
					{	/* Unsafe/crc16.scm 86 */
						long BgL_valuez00_1814;
						long BgL_crcz00_1815;

						BgL_valuez00_1814 = (BgL_valuez00_1810 << (int) (1L));
						BgL_crcz00_1815 = (BgL_crcz00_1811 << (int) (1L));
						{
							long BgL_crcz00_2208;
							long BgL_valuez00_2207;
							long BgL_iz00_2205;

							BgL_iz00_2205 = (BgL_iz00_1809 + 1L);
							BgL_valuez00_2207 = BgL_valuez00_1814;
							if ((0L == (65536L & (BgL_crcz00_1815 ^ BgL_valuez00_1814))))
								{	/* Unsafe/crc16.scm 90 */
									BgL_crcz00_2208 = BgL_crcz00_1815;
								}
							else
								{	/* Unsafe/crc16.scm 90 */
									BgL_crcz00_2208 = (BgL_crcz00_1815 ^ 32773L);
								}
							BgL_crcz00_1811 = BgL_crcz00_2208;
							BgL_valuez00_1810 = BgL_valuez00_2207;
							BgL_iz00_1809 = BgL_iz00_2205;
							goto BgL_loopz00_1808;
						}
					}
			}
		}

	}



/* crc16-mmap */
	BGL_EXPORTED_DEF int BGl_crc16zd2mmapzd2zz__crc16z00(obj_t BgL_mmapz00_6)
	{
		{	/* Unsafe/crc16.scm 97 */
			{	/* Unsafe/crc16.scm 98 */
				long BgL_lenz00_1306;

				BgL_lenz00_1306 = BGL_MMAP_LENGTH(BgL_mmapz00_6);
				{
					long BgL_iz00_1854;
					long BgL_crcz00_1855;

					{	/* Unsafe/crc16.scm 99 */
						long BgL_tmpz00_2217;

						BgL_iz00_1854 = 0L;
						BgL_crcz00_1855 = 65535L;
					BgL_loopz00_1853:
						{	/* Unsafe/crc16.scm 101 */
							bool_t BgL_test1696z00_2218;

							{	/* Unsafe/crc16.scm 101 */
								long BgL_n2z00_1867;

								BgL_n2z00_1867 = (long) (BgL_lenz00_1306);
								BgL_test1696z00_2218 = (BgL_iz00_1854 == BgL_n2z00_1867);
							}
							if (BgL_test1696z00_2218)
								{	/* Unsafe/crc16.scm 101 */
									BgL_tmpz00_2217 = (BgL_crcz00_1855 & 65535L);
								}
							else
								{	/* Unsafe/crc16.scm 103 */
									long BgL_arg1206z00_1862;
									long BgL_arg1208z00_1863;

									BgL_arg1206z00_1862 = (BgL_iz00_1854 + 1L);
									{	/* Unsafe/crc16.scm 104 */
										long BgL_arg1209z00_1864;

										{	/* Unsafe/crc16.scm 104 */
											unsigned char BgL_arg1210z00_1865;

											{	/* Unsafe/crc16.scm 104 */
												long BgL_tmpz00_2223;

												BgL_tmpz00_2223 = (long) (BgL_iz00_1854);
												BgL_arg1210z00_1865 =
													BGL_MMAP_REF(BgL_mmapz00_6, BgL_tmpz00_2223);
											}
											BgL_arg1209z00_1864 = (BgL_arg1210z00_1865);
										}
										BgL_arg1208z00_1863 =
											BGl_crczd2valuezd2zz__crc16z00(BgL_arg1209z00_1864,
											BgL_crcz00_1855);
									}
									{
										long BgL_crcz00_2229;
										long BgL_iz00_2228;

										BgL_iz00_2228 = BgL_arg1206z00_1862;
										BgL_crcz00_2229 = BgL_arg1208z00_1863;
										BgL_crcz00_1855 = BgL_crcz00_2229;
										BgL_iz00_1854 = BgL_iz00_2228;
										goto BgL_loopz00_1853;
									}
								}
						}
						return (int) (BgL_tmpz00_2217);
		}}}}

	}



/* &crc16-mmap */
	obj_t BGl_z62crc16zd2mmapzb0zz__crc16z00(obj_t BgL_envz00_2155,
		obj_t BgL_mmapz00_2156)
	{
		{	/* Unsafe/crc16.scm 97 */
			{	/* Unsafe/crc16.scm 98 */
				int BgL_tmpz00_2231;

				{	/* Unsafe/crc16.scm 98 */
					obj_t BgL_auxz00_2232;

					if (BGL_MMAPP(BgL_mmapz00_2156))
						{	/* Unsafe/crc16.scm 98 */
							BgL_auxz00_2232 = BgL_mmapz00_2156;
						}
					else
						{
							obj_t BgL_auxz00_2235;

							BgL_auxz00_2235 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1663z00zz__crc16z00,
								BINT(3427L), BGl_string1664z00zz__crc16z00,
								BGl_string1665z00zz__crc16z00, BgL_mmapz00_2156);
							FAILURE(BgL_auxz00_2235, BFALSE, BFALSE);
						}
					BgL_tmpz00_2231 = BGl_crc16zd2mmapzd2zz__crc16z00(BgL_auxz00_2232);
				}
				return BINT(BgL_tmpz00_2231);
			}
		}

	}



/* crc16-string */
	BGL_EXPORTED_DEF int BGl_crc16zd2stringzd2zz__crc16z00(obj_t BgL_stringz00_7)
	{
		{	/* Unsafe/crc16.scm 109 */
			{	/* Unsafe/crc16.scm 110 */
				long BgL_lenz00_1317;

				BgL_lenz00_1317 = STRING_LENGTH(BgL_stringz00_7);
				{
					long BgL_iz00_1898;
					long BgL_crcz00_1899;

					{	/* Unsafe/crc16.scm 111 */
						long BgL_tmpz00_2242;

						BgL_iz00_1898 = 0L;
						BgL_crcz00_1899 = 65535L;
					BgL_loopz00_1897:
						if ((BgL_iz00_1898 == BgL_lenz00_1317))
							{	/* Unsafe/crc16.scm 113 */
								BgL_tmpz00_2242 = (BgL_crcz00_1899 & 65535L);
							}
						else
							{
								long BgL_crcz00_2248;
								long BgL_iz00_2246;

								BgL_iz00_2246 = (BgL_iz00_1898 + 1L);
								BgL_crcz00_2248 =
									BGl_crczd2valuezd2zz__crc16z00(
									(STRING_REF(BgL_stringz00_7, BgL_iz00_1898)),
									BgL_crcz00_1899);
								BgL_crcz00_1899 = BgL_crcz00_2248;
								BgL_iz00_1898 = BgL_iz00_2246;
								goto BgL_loopz00_1897;
							}
						return (int) (BgL_tmpz00_2242);
		}}}}

	}



/* &crc16-string */
	obj_t BGl_z62crc16zd2stringzb0zz__crc16z00(obj_t BgL_envz00_2157,
		obj_t BgL_stringz00_2158)
	{
		{	/* Unsafe/crc16.scm 109 */
			{	/* Unsafe/crc16.scm 110 */
				int BgL_tmpz00_2253;

				{	/* Unsafe/crc16.scm 110 */
					obj_t BgL_auxz00_2254;

					if (STRINGP(BgL_stringz00_2158))
						{	/* Unsafe/crc16.scm 110 */
							BgL_auxz00_2254 = BgL_stringz00_2158;
						}
					else
						{
							obj_t BgL_auxz00_2257;

							BgL_auxz00_2257 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1663z00zz__crc16z00,
								BINT(3889L), BGl_string1666z00zz__crc16z00,
								BGl_string1667z00zz__crc16z00, BgL_stringz00_2158);
							FAILURE(BgL_auxz00_2257, BFALSE, BFALSE);
						}
					BgL_tmpz00_2253 = BGl_crc16zd2stringzd2zz__crc16z00(BgL_auxz00_2254);
				}
				return BINT(BgL_tmpz00_2253);
			}
		}

	}



/* crc16-port */
	BGL_EXPORTED_DEF int BGl_crc16zd2portzd2zz__crc16z00(obj_t BgL_portz00_8)
	{
		{	/* Unsafe/crc16.scm 121 */
			{
				long BgL_crcz00_1927;

				{	/* Unsafe/crc16.scm 122 */
					long BgL_tmpz00_2263;

					BgL_crcz00_1927 = 65535L;
				BgL_loopz00_1926:
					{	/* Unsafe/crc16.scm 123 */
						long BgL_bytez00_1931;

						BgL_bytez00_1931 =
							(long)
							CINT(BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_portz00_8));
						{	/* Unsafe/crc16.scm 124 */
							bool_t BgL_test1700z00_2266;

							{	/* Unsafe/crc16.scm 124 */
								obj_t BgL_tmpz00_2267;

								BgL_tmpz00_2267 = BINT(BgL_bytez00_1931);
								BgL_test1700z00_2266 = EOF_OBJECTP(BgL_tmpz00_2267);
							}
							if (BgL_test1700z00_2266)
								{	/* Unsafe/crc16.scm 124 */
									BgL_tmpz00_2263 = (BgL_crcz00_1927 & 65535L);
								}
							else
								{
									long BgL_crcz00_2271;

									BgL_crcz00_2271 =
										BGl_crczd2valuezd2zz__crc16z00(BgL_bytez00_1931,
										BgL_crcz00_1927);
									BgL_crcz00_1927 = BgL_crcz00_2271;
									goto BgL_loopz00_1926;
								}
						}
					}
					return (int) (BgL_tmpz00_2263);
		}}}

	}



/* &crc16-port */
	obj_t BGl_z62crc16zd2portzb0zz__crc16z00(obj_t BgL_envz00_2159,
		obj_t BgL_portz00_2160)
	{
		{	/* Unsafe/crc16.scm 121 */
			{	/* Unsafe/crc16.scm 122 */
				int BgL_tmpz00_2274;

				{	/* Unsafe/crc16.scm 122 */
					obj_t BgL_auxz00_2275;

					if (INPUT_PORTP(BgL_portz00_2160))
						{	/* Unsafe/crc16.scm 122 */
							BgL_auxz00_2275 = BgL_portz00_2160;
						}
					else
						{
							obj_t BgL_auxz00_2278;

							BgL_auxz00_2278 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1663z00zz__crc16z00,
								BINT(4354L), BGl_string1668z00zz__crc16z00,
								BGl_string1669z00zz__crc16z00, BgL_portz00_2160);
							FAILURE(BgL_auxz00_2278, BFALSE, BFALSE);
						}
					BgL_tmpz00_2274 = BGl_crc16zd2portzd2zz__crc16z00(BgL_auxz00_2275);
				}
				return BINT(BgL_tmpz00_2274);
			}
		}

	}



/* crc16-file */
	BGL_EXPORTED_DEF int BGl_crc16zd2filezd2zz__crc16z00(obj_t BgL_filez00_9)
	{
		{	/* Unsafe/crc16.scm 131 */
			return
				CINT(BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
				(BgL_filez00_9, BGl_proc1670z00zz__crc16z00));
		}

	}



/* &crc16-file */
	obj_t BGl_z62crc16zd2filezb0zz__crc16z00(obj_t BgL_envz00_2162,
		obj_t BgL_filez00_2163)
	{
		{	/* Unsafe/crc16.scm 131 */
			{	/* Unsafe/crc16.scm 133 */
				int BgL_tmpz00_2286;

				{	/* Unsafe/crc16.scm 133 */
					obj_t BgL_auxz00_2287;

					if (STRINGP(BgL_filez00_2163))
						{	/* Unsafe/crc16.scm 133 */
							BgL_auxz00_2287 = BgL_filez00_2163;
						}
					else
						{
							obj_t BgL_auxz00_2290;

							BgL_auxz00_2290 =
								BGl_typezd2errorzd2zz__errorz00(BGl_string1663z00zz__crc16z00,
								BINT(4826L), BGl_string1671z00zz__crc16z00,
								BGl_string1667z00zz__crc16z00, BgL_filez00_2163);
							FAILURE(BgL_auxz00_2290, BFALSE, BFALSE);
						}
					BgL_tmpz00_2286 = BGl_crc16zd2filezd2zz__crc16z00(BgL_auxz00_2287);
				}
				return BINT(BgL_tmpz00_2286);
			}
		}

	}



/* &<@anonymous:1226> */
	obj_t BGl_z62zc3z04anonymousza31226ze3ze5zz__crc16z00(obj_t BgL_envz00_2164)
	{
		{	/* Unsafe/crc16.scm 133 */
			{	/* Unsafe/crc16.scm 133 */
				int BgL_tmpz00_2296;

				{	/* Unsafe/crc16.scm 133 */
					obj_t BgL_arg1227z00_2174;

					{	/* Unsafe/crc16.scm 133 */
						obj_t BgL_tmpz00_2297;

						BgL_tmpz00_2297 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1227z00_2174 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_2297);
					}
					BgL_tmpz00_2296 =
						BGl_crc16zd2portzd2zz__crc16z00(BgL_arg1227z00_2174);
				}
				return BINT(BgL_tmpz00_2296);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__crc16z00(void)
	{
		{	/* Unsafe/crc16.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__crc16z00(void)
	{
		{	/* Unsafe/crc16.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__crc16z00(void)
	{
		{	/* Unsafe/crc16.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__crc16z00(void)
	{
		{	/* Unsafe/crc16.scm 17 */
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string1672z00zz__crc16z00));
		}

	}

#ifdef __cplusplus
}
#endif
