/*===========================================================================*/
/*   (Unsafe/base64.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -srfi enable-gmp -srfi bigloo-unsafe -O3 -fcfa-arithmetic -q -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -rm -copt -w -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -mklib -cc gcc -fsharing -q -no-hello -unsafe -safee -O4 -stdc -c Unsafe/base64.scm -indent -o objs/obj_u/Unsafe/base64.c) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL___BASE64_TYPE_DEFINITIONS
#define BGL___BASE64_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;

	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62iozd2parsezd2errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                               *BgL_z62iozd2parsezd2errorz62_bglt;


#endif													// BGL___BASE64_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_makezd2u8vectorzd2zz__srfi4z00(long, uint8_t);
	static obj_t BGl_z62zc3z04anonymousza31962ze32336ze5zz__base64z00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31962ze32337ze5zz__base64z00(obj_t,
		obj_t);
	static obj_t BGl_bytezd2tablezd2zz__base64z00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zz__base64z00 = BUNSPEC;
	extern obj_t BGl_raisez00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_base64zd2encodezd2zz__base64z00(obj_t, obj_t);
	static obj_t BGl_ignoreze70ze7zz__base64z00(obj_t, obj_t);
	extern obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zz__base64z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	static obj_t BGl_z62pemzd2readzd2filez62zz__base64z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_base64zd2decodezd2portz00zz__base64z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31962ze3ze5zz__base64z00(obj_t, obj_t);
	static obj_t BGl__base64zd2decodezd2zz__base64z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zz__base64z00(void);
	static obj_t BGl__base64zd2encodezd2portz00zz__base64z00(obj_t, obj_t);
	extern obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_readzd2linezd2zz__r4_input_6_10_2z00(obj_t);
	extern obj_t BGl_z62iozd2parsezd2errorz62zz__objectz00;
	static obj_t BGl_genericzd2initzd2zz__base64z00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zz__base64z00(void);
	static obj_t BGl_gczd2rootszd2initz00zz__base64z00(void);
	static obj_t BGl_objectzd2initzd2zz__base64z00(void);
	extern bool_t bigloo_strcmp_at(obj_t, obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza31966ze3ze5zz__base64z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pemzd2decodezd2portz00zz__base64z00(obj_t, obj_t);
	static obj_t BGl_z62pemzd2decodezd2portz62zz__base64z00(obj_t, obj_t, obj_t);
	static long BGl_actualzd2stringzd2lengthz00zz__base64z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_base64zd2decodezd2zz__base64z00(obj_t, obj_t);
	extern obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static obj_t BGl__base64zd2decodezd2portz00zz__base64z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31306ze3ze5zz__base64z00(obj_t, obj_t,
		obj_t, obj_t, long, long, obj_t, bool_t);
	extern obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62zc3z04anonymousza31565ze3ze5zz__base64z00(obj_t, obj_t,
		long);
	static obj_t BGl_z62zc3z04anonymousza31993ze3ze5zz__base64z00(obj_t);
	static obj_t BGl_methodzd2initzd2zz__base64z00(void);
	extern bool_t rgc_buffer_eof2_p(obj_t, long, long);
	extern bool_t rgc_fill_buffer(obj_t);
	static obj_t BGl__base64zd2encodezd2zz__base64z00(obj_t, obj_t);
	extern obj_t make_string(long, unsigned char);
	extern obj_t BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	extern obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	extern obj_t BGl_readzd2bytezd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_base64zd2decodezd2grammarz00zz__base64z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_base64zd2encodezd2portz00zz__base64z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_pemzd2markupzd2grammarz00zz__base64z00 = BUNSPEC;
	extern obj_t rgc_buffer_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t BGl_pemzd2readzd2filez00zz__base64z00(obj_t);
	extern obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t bgl_close_output_port(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_base64zd2encodezd2envz00zz__base64z00,
		BgL_bgl__base64za7d2encode2394za7, opt_generic_entry,
		BGl__base64zd2encodezd2zz__base64z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_base64zd2decodezd2portzd2envzd2zz__base64z00,
		BgL_bgl__base64za7d2decode2395za7, opt_generic_entry,
		BGl__base64zd2decodezd2portz00zz__base64z00, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_base64zd2encodezd2portzd2envzd2zz__base64z00,
		BgL_bgl__base64za7d2encode2396za7, opt_generic_entry,
		BGl__base64zd2encodezd2portz00zz__base64z00, BFALSE, -1);
	      DEFINE_STRING(BGl_string2368z00zz__base64z00,
		BgL_bgl_string2368za700za7za7_2397za7, "pem-decode-port", 15);
	      DEFINE_STRING(BGl_string2369z00zz__base64z00,
		BgL_bgl_string2369za700za7za7_2398za7, "Illegal character in PEM markup",
		31);
	      DEFINE_STRING(BGl_string2370z00zz__base64z00,
		BgL_bgl_string2370za700za7za7_2399za7, "{~a}~a", 6);
	      DEFINE_STRING(BGl_string2371z00zz__base64z00,
		BgL_bgl_string2371za700za7za7_2400za7, "Illegal PEM markup", 18);
	      DEFINE_STRING(BGl_string2372z00zz__base64z00,
		BgL_bgl_string2372za700za7za7_2401za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string2373z00zz__base64z00,
		BgL_bgl_string2373za700za7za7_2402za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string2374z00zz__base64z00,
		BgL_bgl_string2374za700za7za7_2403za7,
		"/tmp/bigloo/runtime/Unsafe/base64.scm", 37);
	      DEFINE_STRING(BGl_string2375z00zz__base64z00,
		BgL_bgl_string2375za700za7za7_2404za7, "_base64-encode", 14);
	      DEFINE_STRING(BGl_string2376z00zz__base64z00,
		BgL_bgl_string2376za700za7za7_2405za7, "bstring", 7);
	      DEFINE_STRING(BGl_string2377z00zz__base64z00,
		BgL_bgl_string2377za700za7za7_2406za7,
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", 64);
	      DEFINE_STRING(BGl_string2378z00zz__base64z00,
		BgL_bgl_string2378za700za7za7_2407za7, "_base64-encode-port", 19);
	      DEFINE_STRING(BGl_string2379z00zz__base64z00,
		BgL_bgl_string2379za700za7za7_2408za7, "input-port", 10);
	      DEFINE_STRING(BGl_string2380z00zz__base64z00,
		BgL_bgl_string2380za700za7za7_2409za7, "output-port", 11);
	      DEFINE_STRING(BGl_string2381z00zz__base64z00,
		BgL_bgl_string2381za700za7za7_2410za7, "_base64-decode", 14);
	      DEFINE_STRING(BGl_string2382z00zz__base64z00,
		BgL_bgl_string2382za700za7za7_2411za7, "_base64-decode-port", 19);
	      DEFINE_STRING(BGl_string2386z00zz__base64z00,
		BgL_bgl_string2386za700za7za7_2412za7, "BEGIN ", 6);
	      DEFINE_STRING(BGl_string2387z00zz__base64z00,
		BgL_bgl_string2387za700za7za7_2413za7, "Illegal PEM begin markup", 24);
	      DEFINE_STRING(BGl_string2388z00zz__base64z00,
		BgL_bgl_string2388za700za7za7_2414za7, "&pem-decode-port", 16);
	      DEFINE_STRING(BGl_string2389z00zz__base64z00,
		BgL_bgl_string2389za700za7za7_2415za7, "END ", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pemzd2decodezd2portzd2envzd2zz__base64z00,
		BgL_bgl_za762pemza7d2decodeza72416za7,
		BGl_z62pemzd2decodezd2portz62zz__base64z00, 0L, BUNSPEC, 2);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2383z00zz__base64z00,
		BgL_bgl_za762za7c3za704anonymo2417za7,
		BGl_z62zc3z04anonymousza31962ze32336ze5zz__base64z00);
	      DEFINE_STRING(BGl_string2390z00zz__base64z00,
		BgL_bgl_string2390za700za7za7_2418za7, "PEM begin/end markup mismatch", 29);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2384z00zz__base64z00,
		BgL_bgl_za762za7c3za704anonymo2419za7,
		BGl_z62zc3z04anonymousza31962ze3ze5zz__base64z00);
	      DEFINE_STRING(BGl_string2391z00zz__base64z00,
		BgL_bgl_string2391za700za7za7_2420za7, "Illegal character", 17);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2385z00zz__base64z00,
		BgL_bgl_za762za7c3za704anonymo2421za7,
		BGl_z62zc3z04anonymousza31962ze32337ze5zz__base64z00);
	      DEFINE_STRING(BGl_string2392z00zz__base64z00,
		BgL_bgl_string2392za700za7za7_2422za7, "&pem-read-file", 14);
	      DEFINE_STRING(BGl_string2393z00zz__base64z00,
		BgL_bgl_string2393za700za7za7_2423za7, "__base64", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pemzd2readzd2filezd2envzd2zz__base64z00,
		BgL_bgl_za762pemza7d2readza7d22424za7,
		BGl_z62pemzd2readzd2filez62zz__base64z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_base64zd2decodezd2envz00zz__base64z00,
		BgL_bgl__base64za7d2decode2425za7, opt_generic_entry,
		BGl__base64zd2decodezd2zz__base64z00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_bytezd2tablezd2zz__base64z00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zz__base64z00));
		     ADD_ROOT((void *) (&BGl_base64zd2decodezd2grammarz00zz__base64z00));
		     ADD_ROOT((void *) (&BGl_pemzd2markupzd2grammarz00zz__base64z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zz__base64z00(long
		BgL_checksumz00_4565, char *BgL_fromz00_4566)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zz__base64z00))
				{
					BGl_requirezd2initializa7ationz75zz__base64z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zz__base64z00();
					BGl_importedzd2moduleszd2initz00zz__base64z00();
					return BGl_toplevelzd2initzd2zz__base64z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zz__base64z00(void)
	{
		{	/* Unsafe/base64.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zz__base64z00(void)
	{
		{	/* Unsafe/base64.scm 15 */
			{	/* Unsafe/base64.scm 173 */
				obj_t BgL_tablez00_1255;

				BgL_tablez00_1255 =
					BGl_makezd2u8vectorzd2zz__srfi4z00(128L, (uint8_t) (0L));
				{
					long BgL_iz00_1258;

					BgL_iz00_1258 = 0L;
				BgL_zc3z04anonymousza31246ze3z87_1259:
					if ((BgL_iz00_1258 < 26L))
						{	/* Unsafe/base64.scm 177 */
							{	/* Unsafe/base64.scm 178 */
								long BgL_arg1248z00_1261;

								BgL_arg1248z00_1261 = (BgL_iz00_1258 + 65L);
								{	/* Unsafe/base64.scm 175 */
									uint8_t BgL_tmpz00_4579;

									BgL_tmpz00_4579 = (uint8_t) (BgL_iz00_1258);
									BGL_U8VSET(BgL_tablez00_1255, BgL_arg1248z00_1261,
										BgL_tmpz00_4579);
								} BUNSPEC;
							}
							{	/* Unsafe/base64.scm 179 */
								long BgL_arg1249z00_1262;
								long BgL_arg1252z00_1263;

								BgL_arg1249z00_1262 = (BgL_iz00_1258 + 97L);
								BgL_arg1252z00_1263 = (BgL_iz00_1258 + 26L);
								{	/* Unsafe/base64.scm 175 */
									uint8_t BgL_tmpz00_4584;

									BgL_tmpz00_4584 = (uint8_t) (BgL_arg1252z00_1263);
									BGL_U8VSET(BgL_tablez00_1255, BgL_arg1249z00_1262,
										BgL_tmpz00_4584);
								} BUNSPEC;
							}
							{
								long BgL_iz00_4587;

								BgL_iz00_4587 = (BgL_iz00_1258 + 1L);
								BgL_iz00_1258 = BgL_iz00_4587;
								goto BgL_zc3z04anonymousza31246ze3z87_1259;
							}
						}
					else
						{	/* Unsafe/base64.scm 177 */
							((bool_t) 0);
						}
				}
				{
					long BgL_iz00_2910;

					BgL_iz00_2910 = 0L;
				BgL_loopz00_2909:
					if ((BgL_iz00_2910 < 10L))
						{	/* Unsafe/base64.scm 182 */
							{	/* Unsafe/base64.scm 183 */
								long BgL_arg1272z00_2913;
								long BgL_arg1284z00_2914;

								BgL_arg1272z00_2913 = (BgL_iz00_2910 + 48L);
								BgL_arg1284z00_2914 = (BgL_iz00_2910 + 52L);
								{	/* Unsafe/base64.scm 175 */
									uint8_t BgL_tmpz00_4593;

									BgL_tmpz00_4593 = (uint8_t) (BgL_arg1284z00_2914);
									BGL_U8VSET(BgL_tablez00_1255, BgL_arg1272z00_2913,
										BgL_tmpz00_4593);
								} BUNSPEC;
							}
							{
								long BgL_iz00_4596;

								BgL_iz00_4596 = (BgL_iz00_2910 + 1L);
								BgL_iz00_2910 = BgL_iz00_4596;
								goto BgL_loopz00_2909;
							}
						}
					else
						{	/* Unsafe/base64.scm 182 */
							((bool_t) 0);
						}
				}
				{	/* Unsafe/base64.scm 175 */
					uint8_t BgL_tmpz00_4598;

					BgL_tmpz00_4598 = (uint8_t) (62L);
					BGL_U8VSET(BgL_tablez00_1255, 43L, BgL_tmpz00_4598);
				}
				BUNSPEC;
				{	/* Unsafe/base64.scm 175 */
					uint8_t BgL_tmpz00_4601;

					BgL_tmpz00_4601 = (uint8_t) (62L);
					BGL_U8VSET(BgL_tablez00_1255, 45L, BgL_tmpz00_4601);
				}
				BUNSPEC;
				{	/* Unsafe/base64.scm 175 */
					uint8_t BgL_tmpz00_4604;

					BgL_tmpz00_4604 = (uint8_t) (63L);
					BGL_U8VSET(BgL_tablez00_1255, 47L, BgL_tmpz00_4604);
				}
				BUNSPEC;
				{	/* Unsafe/base64.scm 175 */
					uint8_t BgL_tmpz00_4607;

					BgL_tmpz00_4607 = (uint8_t) (63L);
					BGL_U8VSET(BgL_tablez00_1255, 95L, BgL_tmpz00_4607);
				}
				BUNSPEC;
				BGl_bytezd2tablezd2zz__base64z00 = BgL_tablez00_1255;
			}
			{	/* Unsafe/base64.scm 300 */
				obj_t BgL_zc3z04anonymousza31306ze3z87_4270;

				{
					int BgL_tmpz00_4610;

					BgL_tmpz00_4610 = (int) (0L);
					BgL_zc3z04anonymousza31306ze3z87_4270 =
						MAKE_EL_PROCEDURE(BgL_tmpz00_4610);
				}
				BGl_base64zd2decodezd2grammarz00zz__base64z00 =
					BgL_zc3z04anonymousza31306ze3z87_4270;
			}
			{	/* Unsafe/base64.scm 388 */
				obj_t BgL_zc3z04anonymousza31565ze3z87_4269;

				{
					int BgL_tmpz00_4613;

					BgL_tmpz00_4613 = (int) (0L);
					BgL_zc3z04anonymousza31565ze3z87_4269 =
						MAKE_EL_PROCEDURE(BgL_tmpz00_4613);
				}
				return (BGl_pemzd2markupzd2grammarz00zz__base64z00 =
					BgL_zc3z04anonymousza31565ze3z87_4269, BUNSPEC);
			}
		}

	}



/* &<@anonymous:1565> */
	obj_t BGl_z62zc3z04anonymousza31565ze3ze5zz__base64z00(obj_t BgL_envz00_4271,
		obj_t BgL_iportz00_4272, long BgL_countz00_4273)
	{
		{	/* Unsafe/base64.scm 388 */
			{	/* Unsafe/base64.scm 388 */
				long BgL_countz00_4339;

				BgL_countz00_4339 = BgL_countz00_4273;
				{	/* Unsafe/base64.scm 388 */
					struct bgl_cell BgL_box2429_4340z00;
					obj_t BgL_countz00_4340;

					BgL_countz00_4340 =
						MAKE_CELL_STACK(BINT(BgL_countz00_4339), BgL_box2429_4340z00);
					return
						BGl_ignoreze70ze7zz__base64z00(BgL_countz00_4340,
						BgL_iportz00_4272);
				}
			}
		}

	}



/* ignore~0 */
	obj_t BGl_ignoreze70ze7zz__base64z00(obj_t BgL_countz00_4305,
		obj_t BgL_iportz00_4304)
	{
		{	/* Unsafe/base64.scm 388 */
		BGl_ignoreze70ze7zz__base64z00:
			{
				obj_t BgL_iportz00_1908;
				long BgL_lastzd2matchzd2_1909;
				long BgL_forwardz00_1910;
				long BgL_bufposz00_1911;
				obj_t BgL_iportz00_1893;
				long BgL_lastzd2matchzd2_1894;
				long BgL_forwardz00_1895;
				long BgL_bufposz00_1896;
				obj_t BgL_iportz00_1870;
				long BgL_lastzd2matchzd2_1871;
				long BgL_forwardz00_1872;
				long BgL_bufposz00_1873;
				obj_t BgL_iportz00_1849;
				long BgL_lastzd2matchzd2_1850;
				long BgL_forwardz00_1851;
				long BgL_bufposz00_1852;
				obj_t BgL_iportz00_1832;
				long BgL_lastzd2matchzd2_1833;
				long BgL_forwardz00_1834;
				long BgL_bufposz00_1835;

				RGC_START_MATCH(BgL_iportz00_4304);
				{	/* Unsafe/base64.scm 388 */
					long BgL_matchz00_2044;

					{	/* Unsafe/base64.scm 388 */
						long BgL_arg1748z00_2071;
						long BgL_arg1749z00_2072;

						BgL_arg1748z00_2071 = RGC_BUFFER_FORWARD(BgL_iportz00_4304);
						BgL_arg1749z00_2072 = RGC_BUFFER_BUFPOS(BgL_iportz00_4304);
						BgL_iportz00_1908 = BgL_iportz00_4304;
						BgL_lastzd2matchzd2_1909 = 3L;
						BgL_forwardz00_1910 = BgL_arg1748z00_2071;
						BgL_bufposz00_1911 = BgL_arg1749z00_2072;
					BgL_zc3z04anonymousza31610ze3z87_1912:
						if ((BgL_forwardz00_1910 == BgL_bufposz00_1911))
							{	/* Unsafe/base64.scm 388 */
								if (rgc_fill_buffer(BgL_iportz00_1908))
									{	/* Unsafe/base64.scm 388 */
										long BgL_arg1613z00_1915;
										long BgL_arg1615z00_1916;

										BgL_arg1613z00_1915 = RGC_BUFFER_FORWARD(BgL_iportz00_1908);
										BgL_arg1615z00_1916 = RGC_BUFFER_BUFPOS(BgL_iportz00_1908);
										{
											long BgL_bufposz00_4628;
											long BgL_forwardz00_4627;

											BgL_forwardz00_4627 = BgL_arg1613z00_1915;
											BgL_bufposz00_4628 = BgL_arg1615z00_1916;
											BgL_bufposz00_1911 = BgL_bufposz00_4628;
											BgL_forwardz00_1910 = BgL_forwardz00_4627;
											goto BgL_zc3z04anonymousza31610ze3z87_1912;
										}
									}
								else
									{	/* Unsafe/base64.scm 388 */
										BgL_matchz00_2044 = BgL_lastzd2matchzd2_1909;
									}
							}
						else
							{	/* Unsafe/base64.scm 388 */
								int BgL_curz00_1917;

								BgL_curz00_1917 =
									RGC_BUFFER_GET_CHAR(BgL_iportz00_1908, BgL_forwardz00_1910);
								{	/* Unsafe/base64.scm 388 */

									if (((long) (BgL_curz00_1917) == 45L))
										{	/* Unsafe/base64.scm 388 */
											BgL_iportz00_1849 = BgL_iportz00_1908;
											BgL_lastzd2matchzd2_1850 = BgL_lastzd2matchzd2_1909;
											BgL_forwardz00_1851 = (1L + BgL_forwardz00_1910);
											BgL_bufposz00_1852 = BgL_bufposz00_1911;
										BgL_zc3z04anonymousza31579ze3z87_1853:
											{	/* Unsafe/base64.scm 388 */
												long BgL_newzd2matchzd2_1854;

												RGC_STOP_MATCH(BgL_iportz00_1849, BgL_forwardz00_1851);
												BgL_newzd2matchzd2_1854 = 0L;
												if ((BgL_forwardz00_1851 == BgL_bufposz00_1852))
													{	/* Unsafe/base64.scm 388 */
														if (rgc_fill_buffer(BgL_iportz00_1849))
															{	/* Unsafe/base64.scm 388 */
																long BgL_arg1582z00_1857;
																long BgL_arg1583z00_1858;

																BgL_arg1582z00_1857 =
																	RGC_BUFFER_FORWARD(BgL_iportz00_1849);
																BgL_arg1583z00_1858 =
																	RGC_BUFFER_BUFPOS(BgL_iportz00_1849);
																{
																	long BgL_bufposz00_4641;
																	long BgL_forwardz00_4640;

																	BgL_forwardz00_4640 = BgL_arg1582z00_1857;
																	BgL_bufposz00_4641 = BgL_arg1583z00_1858;
																	BgL_bufposz00_1852 = BgL_bufposz00_4641;
																	BgL_forwardz00_1851 = BgL_forwardz00_4640;
																	goto BgL_zc3z04anonymousza31579ze3z87_1853;
																}
															}
														else
															{	/* Unsafe/base64.scm 388 */
																BgL_matchz00_2044 = BgL_newzd2matchzd2_1854;
															}
													}
												else
													{	/* Unsafe/base64.scm 388 */
														int BgL_curz00_1859;

														BgL_curz00_1859 =
															RGC_BUFFER_GET_CHAR(BgL_iportz00_1849,
															BgL_forwardz00_1851);
														{	/* Unsafe/base64.scm 388 */

															if (((long) (BgL_curz00_1859) == 45L))
																{	/* Unsafe/base64.scm 388 */
																	BgL_iportz00_1893 = BgL_iportz00_1849;
																	BgL_lastzd2matchzd2_1894 =
																		BgL_newzd2matchzd2_1854;
																	BgL_forwardz00_1895 =
																		(1L + BgL_forwardz00_1851);
																	BgL_bufposz00_1896 = BgL_bufposz00_1852;
																BgL_zc3z04anonymousza31600ze3z87_1897:
																	{	/* Unsafe/base64.scm 388 */
																		long BgL_newzd2matchzd2_1898;

																		RGC_STOP_MATCH(BgL_iportz00_1893,
																			BgL_forwardz00_1895);
																		BgL_newzd2matchzd2_1898 = 0L;
																		if (
																			(BgL_forwardz00_1895 ==
																				BgL_bufposz00_1896))
																			{	/* Unsafe/base64.scm 388 */
																				if (rgc_fill_buffer(BgL_iportz00_1893))
																					{	/* Unsafe/base64.scm 388 */
																						long BgL_arg1603z00_1901;
																						long BgL_arg1605z00_1902;

																						BgL_arg1603z00_1901 =
																							RGC_BUFFER_FORWARD
																							(BgL_iportz00_1893);
																						BgL_arg1605z00_1902 =
																							RGC_BUFFER_BUFPOS
																							(BgL_iportz00_1893);
																						{
																							long BgL_bufposz00_4654;
																							long BgL_forwardz00_4653;

																							BgL_forwardz00_4653 =
																								BgL_arg1603z00_1901;
																							BgL_bufposz00_4654 =
																								BgL_arg1605z00_1902;
																							BgL_bufposz00_1896 =
																								BgL_bufposz00_4654;
																							BgL_forwardz00_1895 =
																								BgL_forwardz00_4653;
																							goto
																								BgL_zc3z04anonymousza31600ze3z87_1897;
																						}
																					}
																				else
																					{	/* Unsafe/base64.scm 388 */
																						BgL_matchz00_2044 =
																							BgL_newzd2matchzd2_1898;
																					}
																			}
																		else
																			{	/* Unsafe/base64.scm 388 */
																				int BgL_curz00_1903;

																				BgL_curz00_1903 =
																					RGC_BUFFER_GET_CHAR(BgL_iportz00_1893,
																					BgL_forwardz00_1895);
																				{	/* Unsafe/base64.scm 388 */

																					if (((long) (BgL_curz00_1903) == 45L))
																						{
																							long BgL_forwardz00_4660;
																							long BgL_lastzd2matchzd2_4659;

																							BgL_lastzd2matchzd2_4659 =
																								BgL_newzd2matchzd2_1898;
																							BgL_forwardz00_4660 =
																								(1L + BgL_forwardz00_1895);
																							BgL_forwardz00_1895 =
																								BgL_forwardz00_4660;
																							BgL_lastzd2matchzd2_1894 =
																								BgL_lastzd2matchzd2_4659;
																							goto
																								BgL_zc3z04anonymousza31600ze3z87_1897;
																						}
																					else
																						{	/* Unsafe/base64.scm 388 */
																							if (
																								((long) (BgL_curz00_1903) ==
																									10L))
																								{	/* Unsafe/base64.scm 388 */
																									long BgL_arg1609z00_1907;

																									BgL_arg1609z00_1907 =
																										(1L + BgL_forwardz00_1895);
																									{	/* Unsafe/base64.scm 388 */
																										long
																											BgL_newzd2matchzd2_3415;
																										RGC_STOP_MATCH
																											(BgL_iportz00_1893,
																											BgL_arg1609z00_1907);
																										BgL_newzd2matchzd2_3415 =
																											1L;
																										BgL_matchz00_2044 =
																											BgL_newzd2matchzd2_3415;
																								}}
																							else
																								{	/* Unsafe/base64.scm 388 */
																									BgL_matchz00_2044 =
																										BgL_newzd2matchzd2_1898;
																								}
																						}
																				}
																			}
																	}
																}
															else
																{	/* Unsafe/base64.scm 388 */
																	if (((long) (BgL_curz00_1859) == 10L))
																		{	/* Unsafe/base64.scm 388 */
																			long BgL_arg1587z00_1863;

																			BgL_arg1587z00_1863 =
																				(1L + BgL_forwardz00_1851);
																			{	/* Unsafe/base64.scm 388 */
																				long BgL_newzd2matchzd2_3380;

																				RGC_STOP_MATCH(BgL_iportz00_1849,
																					BgL_arg1587z00_1863);
																				BgL_newzd2matchzd2_3380 = 1L;
																				BgL_matchz00_2044 =
																					BgL_newzd2matchzd2_3380;
																		}}
																	else
																		{	/* Unsafe/base64.scm 388 */
																			BgL_matchz00_2044 =
																				BgL_newzd2matchzd2_1854;
																		}
																}
														}
													}
											}
										}
									else
										{	/* Unsafe/base64.scm 388 */
											bool_t BgL_test2441z00_4674;

											if (((long) (BgL_curz00_1917) == 10L))
												{	/* Unsafe/base64.scm 388 */
													BgL_test2441z00_4674 = ((bool_t) 1);
												}
											else
												{	/* Unsafe/base64.scm 388 */
													BgL_test2441z00_4674 =
														((long) (BgL_curz00_1917) == 13L);
												}
											if (BgL_test2441z00_4674)
												{	/* Unsafe/base64.scm 388 */
													long BgL_arg1620z00_1922;

													BgL_arg1620z00_1922 = (1L + BgL_forwardz00_1910);
													{	/* Unsafe/base64.scm 388 */
														long BgL_newzd2matchzd2_3430;

														RGC_STOP_MATCH(BgL_iportz00_1908,
															BgL_arg1620z00_1922);
														BgL_newzd2matchzd2_3430 = 3L;
														BgL_matchz00_2044 = BgL_newzd2matchzd2_3430;
												}}
											else
												{	/* Unsafe/base64.scm 388 */
													BgL_iportz00_1870 = BgL_iportz00_1908;
													BgL_lastzd2matchzd2_1871 = BgL_lastzd2matchzd2_1909;
													BgL_forwardz00_1872 = (1L + BgL_forwardz00_1910);
													BgL_bufposz00_1873 = BgL_bufposz00_1911;
												BgL_zc3z04anonymousza31589ze3z87_1874:
													{	/* Unsafe/base64.scm 388 */
														long BgL_newzd2matchzd2_1875;

														RGC_STOP_MATCH(BgL_iportz00_1870,
															BgL_forwardz00_1872);
														BgL_newzd2matchzd2_1875 = 2L;
														if ((BgL_forwardz00_1872 == BgL_bufposz00_1873))
															{	/* Unsafe/base64.scm 388 */
																if (rgc_fill_buffer(BgL_iportz00_1870))
																	{	/* Unsafe/base64.scm 388 */
																		long BgL_arg1593z00_1878;
																		long BgL_arg1594z00_1879;

																		BgL_arg1593z00_1878 =
																			RGC_BUFFER_FORWARD(BgL_iportz00_1870);
																		BgL_arg1594z00_1879 =
																			RGC_BUFFER_BUFPOS(BgL_iportz00_1870);
																		{
																			long BgL_bufposz00_4690;
																			long BgL_forwardz00_4689;

																			BgL_forwardz00_4689 = BgL_arg1593z00_1878;
																			BgL_bufposz00_4690 = BgL_arg1594z00_1879;
																			BgL_bufposz00_1873 = BgL_bufposz00_4690;
																			BgL_forwardz00_1872 = BgL_forwardz00_4689;
																			goto
																				BgL_zc3z04anonymousza31589ze3z87_1874;
																		}
																	}
																else
																	{	/* Unsafe/base64.scm 388 */
																		BgL_matchz00_2044 = BgL_newzd2matchzd2_1875;
																	}
															}
														else
															{	/* Unsafe/base64.scm 388 */
																int BgL_curz00_1880;

																BgL_curz00_1880 =
																	RGC_BUFFER_GET_CHAR(BgL_iportz00_1870,
																	BgL_forwardz00_1872);
																{	/* Unsafe/base64.scm 388 */

																	{	/* Unsafe/base64.scm 388 */
																		bool_t BgL_test2445z00_4692;

																		if (((long) (BgL_curz00_1880) == 10L))
																			{	/* Unsafe/base64.scm 388 */
																				BgL_test2445z00_4692 = ((bool_t) 1);
																			}
																		else
																			{	/* Unsafe/base64.scm 388 */
																				if (((long) (BgL_curz00_1880) == 13L))
																					{	/* Unsafe/base64.scm 388 */
																						BgL_test2445z00_4692 = ((bool_t) 1);
																					}
																				else
																					{	/* Unsafe/base64.scm 388 */
																						BgL_test2445z00_4692 =
																							((long) (BgL_curz00_1880) == 45L);
																			}}
																		if (BgL_test2445z00_4692)
																			{	/* Unsafe/base64.scm 388 */
																				BgL_matchz00_2044 =
																					BgL_newzd2matchzd2_1875;
																			}
																		else
																			{	/* Unsafe/base64.scm 388 */
																				BgL_iportz00_1832 = BgL_iportz00_1870;
																				BgL_lastzd2matchzd2_1833 =
																					BgL_newzd2matchzd2_1875;
																				BgL_forwardz00_1834 =
																					(1L + BgL_forwardz00_1872);
																				BgL_bufposz00_1835 = BgL_bufposz00_1873;
																			BgL_zc3z04anonymousza31566ze3z87_1836:
																				{	/* Unsafe/base64.scm 388 */
																					long BgL_newzd2matchzd2_1837;

																					RGC_STOP_MATCH(BgL_iportz00_1832,
																						BgL_forwardz00_1834);
																					BgL_newzd2matchzd2_1837 = 2L;
																					if (
																						(BgL_forwardz00_1834 ==
																							BgL_bufposz00_1835))
																						{	/* Unsafe/base64.scm 388 */
																							if (rgc_fill_buffer
																								(BgL_iportz00_1832))
																								{	/* Unsafe/base64.scm 388 */
																									long BgL_arg1571z00_1840;
																									long BgL_arg1573z00_1841;

																									BgL_arg1571z00_1840 =
																										RGC_BUFFER_FORWARD
																										(BgL_iportz00_1832);
																									BgL_arg1573z00_1841 =
																										RGC_BUFFER_BUFPOS
																										(BgL_iportz00_1832);
																									{
																										long BgL_bufposz00_4709;
																										long BgL_forwardz00_4708;

																										BgL_forwardz00_4708 =
																											BgL_arg1571z00_1840;
																										BgL_bufposz00_4709 =
																											BgL_arg1573z00_1841;
																										BgL_bufposz00_1835 =
																											BgL_bufposz00_4709;
																										BgL_forwardz00_1834 =
																											BgL_forwardz00_4708;
																										goto
																											BgL_zc3z04anonymousza31566ze3z87_1836;
																									}
																								}
																							else
																								{	/* Unsafe/base64.scm 388 */
																									BgL_matchz00_2044 =
																										BgL_newzd2matchzd2_1837;
																								}
																						}
																					else
																						{	/* Unsafe/base64.scm 388 */
																							int BgL_curz00_1842;

																							BgL_curz00_1842 =
																								RGC_BUFFER_GET_CHAR
																								(BgL_iportz00_1832,
																								BgL_forwardz00_1834);
																							{	/* Unsafe/base64.scm 388 */

																								{	/* Unsafe/base64.scm 388 */
																									bool_t BgL_test2450z00_4711;

																									if (
																										((long) (BgL_curz00_1842) ==
																											10L))
																										{	/* Unsafe/base64.scm 388 */
																											BgL_test2450z00_4711 =
																												((bool_t) 1);
																										}
																									else
																										{	/* Unsafe/base64.scm 388 */
																											if (
																												((long)
																													(BgL_curz00_1842) ==
																													13L))
																												{	/* Unsafe/base64.scm 388 */
																													BgL_test2450z00_4711 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Unsafe/base64.scm 388 */
																													BgL_test2450z00_4711 =
																														(
																														(long)
																														(BgL_curz00_1842) ==
																														45L);
																										}}
																									if (BgL_test2450z00_4711)
																										{	/* Unsafe/base64.scm 388 */
																											BgL_matchz00_2044 =
																												BgL_newzd2matchzd2_1837;
																										}
																									else
																										{
																											long BgL_forwardz00_4721;
																											long
																												BgL_lastzd2matchzd2_4720;
																											BgL_lastzd2matchzd2_4720 =
																												BgL_newzd2matchzd2_1837;
																											BgL_forwardz00_4721 =
																												(1L +
																												BgL_forwardz00_1834);
																											BgL_forwardz00_1834 =
																												BgL_forwardz00_4721;
																											BgL_lastzd2matchzd2_1833 =
																												BgL_lastzd2matchzd2_4720;
																											goto
																												BgL_zc3z04anonymousza31566ze3z87_1836;
																										}
																								}
																							}
																						}
																				}
																			}
																	}
																}
															}
													}
												}
										}
								}
							}
					}
					RGC_SET_FILEPOS(BgL_iportz00_4304);
					switch (BgL_matchz00_2044)
						{
						case 3L:

							{	/* Unsafe/base64.scm 404 */
								obj_t BgL_cz00_2047;

								{	/* Unsafe/base64.scm 388 */
									bool_t BgL_test2453z00_4726;

									{	/* Unsafe/base64.scm 388 */
										long BgL_arg1722z00_2034;

										BgL_arg1722z00_2034 =
											RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4304);
										BgL_test2453z00_4726 = (BgL_arg1722z00_2034 == 0L);
									}
									if (BgL_test2453z00_4726)
										{	/* Unsafe/base64.scm 388 */
											BgL_cz00_2047 = BEOF;
										}
									else
										{	/* Unsafe/base64.scm 388 */
											BgL_cz00_2047 =
												BCHAR(RGC_BUFFER_CHARACTER(BgL_iportz00_4304));
										}
								}
								{	/* Unsafe/base64.scm 405 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1729z00_2048;

									{	/* Unsafe/base64.scm 405 */
										BgL_z62iozd2parsezd2errorz62_bglt BgL_new1078z00_2049;

										{	/* Unsafe/base64.scm 405 */
											BgL_z62iozd2parsezd2errorz62_bglt BgL_new1077z00_2056;

											BgL_new1077z00_2056 =
												((BgL_z62iozd2parsezd2errorz62_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_z62iozd2parsezd2errorz62_bgl))));
											{	/* Unsafe/base64.scm 405 */
												long BgL_arg1737z00_2057;

												BgL_arg1737z00_2057 =
													BGL_CLASS_NUM
													(BGl_z62iozd2parsezd2errorz62zz__objectz00);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1077z00_2056), BgL_arg1737z00_2057);
											}
											BgL_new1078z00_2049 = BgL_new1077z00_2056;
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1078z00_2049)))->
												BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_z62exceptionz62_bglt)
													COBJECT(((BgL_z62exceptionz62_bglt)
															BgL_new1078z00_2049)))->BgL_locationz00) =
											((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_4739;

											{	/* Unsafe/base64.scm 405 */
												obj_t BgL_arg1730z00_2050;

												{	/* Unsafe/base64.scm 405 */
													obj_t BgL_arg1731z00_2051;

													{	/* Unsafe/base64.scm 405 */
														obj_t BgL_classz00_3449;

														BgL_classz00_3449 =
															BGl_z62iozd2parsezd2errorz62zz__objectz00;
														BgL_arg1731z00_2051 =
															BGL_CLASS_ALL_FIELDS(BgL_classz00_3449);
													}
													BgL_arg1730z00_2050 =
														VECTOR_REF(BgL_arg1731z00_2051, 2L);
												}
												BgL_auxz00_4739 =
													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
													(BgL_arg1730z00_2050);
											}
											((((BgL_z62exceptionz62_bglt) COBJECT(
															((BgL_z62exceptionz62_bglt)
																BgL_new1078z00_2049)))->BgL_stackz00) =
												((obj_t) BgL_auxz00_4739), BUNSPEC);
										}
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_new1078z00_2049)))->
												BgL_procz00) =
											((obj_t) BGl_string2368z00zz__base64z00), BUNSPEC);
										((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
															BgL_new1078z00_2049)))->BgL_msgz00) =
											((obj_t) BGl_string2369z00zz__base64z00), BUNSPEC);
										{
											obj_t BgL_auxz00_4749;

											{	/* Unsafe/base64.scm 408 */
												obj_t BgL_arg1733z00_2052;

												BgL_arg1733z00_2052 =
													BGl_readzd2linezd2zz__r4_input_6_10_2z00
													(BgL_iportz00_4304);
												{	/* Unsafe/base64.scm 408 */
													obj_t BgL_list1734z00_2053;

													{	/* Unsafe/base64.scm 408 */
														obj_t BgL_arg1735z00_2054;

														BgL_arg1735z00_2054 =
															MAKE_YOUNG_PAIR(BgL_arg1733z00_2052, BNIL);
														BgL_list1734z00_2053 =
															MAKE_YOUNG_PAIR(BgL_cz00_2047,
															BgL_arg1735z00_2054);
													}
													BgL_auxz00_4749 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string2370z00zz__base64z00,
														BgL_list1734z00_2053);
											}}
											((((BgL_z62errorz62_bglt) COBJECT(
															((BgL_z62errorz62_bglt) BgL_new1078z00_2049)))->
													BgL_objz00) = ((obj_t) BgL_auxz00_4749), BUNSPEC);
										}
										BgL_arg1729z00_2048 = BgL_new1078z00_2049;
									}
									return
										BGl_raisez00zz__errorz00(((obj_t) BgL_arg1729z00_2048));
								}
							}
							break;
						case 2L:

							{	/* Unsafe/base64.scm 395 */
								obj_t BgL_sz00_2058;

								{	/* Unsafe/base64.scm 388 */
									long BgL_arg1628z00_3452;

									BgL_arg1628z00_3452 =
										RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4304);
									BgL_sz00_2058 =
										rgc_buffer_substring(BgL_iportz00_4304, 0L,
										BgL_arg1628z00_3452);
								}
								{	/* Unsafe/base64.scm 395 */
									obj_t BgL_countez00_2059;

									BgL_countez00_2059 =
										BGl_ignoreze70ze7zz__base64z00(BgL_countz00_4305,
										BgL_iportz00_4304);
									{	/* Unsafe/base64.scm 396 */

										if (
											(((obj_t)
													((obj_t) CELL_REF(BgL_countz00_4305))) ==
												BgL_countez00_2059))
											{	/* Unsafe/base64.scm 397 */
												return BgL_sz00_2058;
											}
										else
											{	/* Unsafe/base64.scm 399 */
												BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1738z00_2060;

												{	/* Unsafe/base64.scm 399 */
													BgL_z62iozd2parsezd2errorz62_bglt BgL_new1080z00_2061;

													{	/* Unsafe/base64.scm 399 */
														BgL_z62iozd2parsezd2errorz62_bglt
															BgL_new1079z00_2067;
														BgL_new1079z00_2067 =
															((BgL_z62iozd2parsezd2errorz62_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_z62iozd2parsezd2errorz62_bgl))));
														{	/* Unsafe/base64.scm 399 */
															long BgL_arg1745z00_2068;

															BgL_arg1745z00_2068 =
																BGL_CLASS_NUM
																(BGl_z62iozd2parsezd2errorz62zz__objectz00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1079z00_2067), BgL_arg1745z00_2068);
														}
														BgL_new1080z00_2061 = BgL_new1079z00_2067;
													}
													((((BgL_z62exceptionz62_bglt) COBJECT(
																	((BgL_z62exceptionz62_bglt)
																		BgL_new1080z00_2061)))->BgL_fnamez00) =
														((obj_t) BFALSE), BUNSPEC);
													((((BgL_z62exceptionz62_bglt)
																COBJECT(((BgL_z62exceptionz62_bglt)
																		BgL_new1080z00_2061)))->BgL_locationz00) =
														((obj_t) BFALSE), BUNSPEC);
													{
														obj_t BgL_auxz00_4772;

														{	/* Unsafe/base64.scm 399 */
															obj_t BgL_arg1739z00_2062;

															{	/* Unsafe/base64.scm 399 */
																obj_t BgL_arg1740z00_2063;

																{	/* Unsafe/base64.scm 399 */
																	obj_t BgL_classz00_3459;

																	BgL_classz00_3459 =
																		BGl_z62iozd2parsezd2errorz62zz__objectz00;
																	BgL_arg1740z00_2063 =
																		BGL_CLASS_ALL_FIELDS(BgL_classz00_3459);
																}
																BgL_arg1739z00_2062 =
																	VECTOR_REF(BgL_arg1740z00_2063, 2L);
															}
															BgL_auxz00_4772 =
																BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																(BgL_arg1739z00_2062);
														}
														((((BgL_z62exceptionz62_bglt) COBJECT(
																		((BgL_z62exceptionz62_bglt)
																			BgL_new1080z00_2061)))->BgL_stackz00) =
															((obj_t) BgL_auxz00_4772), BUNSPEC);
													}
													((((BgL_z62errorz62_bglt) COBJECT(
																	((BgL_z62errorz62_bglt)
																		BgL_new1080z00_2061)))->BgL_procz00) =
														((obj_t) BGl_string2368z00zz__base64z00), BUNSPEC);
													((((BgL_z62errorz62_bglt)
																COBJECT(((BgL_z62errorz62_bglt)
																		BgL_new1080z00_2061)))->BgL_msgz00) =
														((obj_t) BGl_string2371z00zz__base64z00), BUNSPEC);
													{
														obj_t BgL_auxz00_4782;

														{	/* Unsafe/base64.scm 402 */
															obj_t BgL_list1741z00_2064;

															{	/* Unsafe/base64.scm 402 */
																obj_t BgL_arg1743z00_2065;

																{	/* Unsafe/base64.scm 402 */
																	obj_t BgL_arg1744z00_2066;

																	BgL_arg1744z00_2066 =
																		MAKE_YOUNG_PAIR(BgL_countez00_2059, BNIL);
																	BgL_arg1743z00_2065 =
																		MAKE_YOUNG_PAIR(
																		((obj_t)
																			((obj_t) CELL_REF(BgL_countz00_4305))),
																		BgL_arg1744z00_2066);
																}
																BgL_list1741z00_2064 =
																	MAKE_YOUNG_PAIR(BgL_sz00_2058,
																	BgL_arg1743z00_2065);
															}
															BgL_auxz00_4782 = BgL_list1741z00_2064;
														}
														((((BgL_z62errorz62_bglt) COBJECT(
																		((BgL_z62errorz62_bglt)
																			BgL_new1080z00_2061)))->BgL_objz00) =
															((obj_t) BgL_auxz00_4782), BUNSPEC);
													}
													BgL_arg1738z00_2060 = BgL_new1080z00_2061;
												}
												return
													BGl_raisez00zz__errorz00(
													((obj_t) BgL_arg1738z00_2060));
											}
									}
								}
							}
							break;
						case 1L:

							{	/* Unsafe/base64.scm 393 */
								long BgL_arg1746z00_2069;

								BgL_arg1746z00_2069 =
									RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4304);
								return BINT((BgL_arg1746z00_2069 - 1L));
							}
							break;
						case 0L:

							{	/* Unsafe/base64.scm 390 */
								obj_t BgL_auxz00_4306;

								{	/* Unsafe/base64.scm 390 */
									long BgL_arg1747z00_2070;

									BgL_arg1747z00_2070 =
										RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4304);
									BgL_auxz00_4306 =
										ADDFX(BINT(BgL_arg1747z00_2070),
										((obj_t) ((obj_t) CELL_REF(BgL_countz00_4305))));
								}
								CELL_SET(BgL_countz00_4305, BgL_auxz00_4306);
							}
							{

								goto BGl_ignoreze70ze7zz__base64z00;
							}
							break;
						default:
							return
								BGl_errorz00zz__errorz00(BGl_string2372z00zz__base64z00,
								BGl_string2373z00zz__base64z00, BINT(BgL_matchz00_2044));
						}
				}
			}
		}

	}



/* &<@anonymous:1306> */
	obj_t BGl_z62zc3z04anonymousza31306ze3ze5zz__base64z00(obj_t BgL_envz00_4274,
		obj_t BgL_iportz00_4275, obj_t BgL_opz00_4276, obj_t BgL_bufz00_4277,
		long BgL_wz00_4278, long BgL_lenz00_4279, obj_t BgL_hookz00_4280,
		bool_t BgL_eofzd2nozd2paddingz00_4281)
	{
		{	/* Unsafe/base64.scm 300 */
			{	/* Unsafe/base64.scm 300 */
				long BgL_wz00_4344;

				BgL_wz00_4344 = BgL_wz00_4278;
				{
					obj_t BgL_iportz00_4525;
					long BgL_lastzd2matchzd2_4526;
					long BgL_forwardz00_4527;
					long BgL_bufposz00_4528;
					obj_t BgL_iportz00_4510;
					long BgL_lastzd2matchzd2_4511;
					long BgL_forwardz00_4512;
					long BgL_bufposz00_4513;
					obj_t BgL_iportz00_4499;
					long BgL_lastzd2matchzd2_4500;
					long BgL_forwardz00_4501;
					long BgL_bufposz00_4502;
					obj_t BgL_iportz00_4482;
					long BgL_lastzd2matchzd2_4483;
					long BgL_forwardz00_4484;
					long BgL_bufposz00_4485;
					obj_t BgL_iportz00_4471;
					long BgL_lastzd2matchzd2_4472;
					long BgL_forwardz00_4473;
					long BgL_bufposz00_4474;
					obj_t BgL_iportz00_4457;
					long BgL_lastzd2matchzd2_4458;
					long BgL_forwardz00_4459;
					long BgL_bufposz00_4460;

				BgL_ignorez00_4348:
					RGC_START_MATCH(BgL_iportz00_4275);
					{	/* Unsafe/base64.scm 300 */
						long BgL_matchz00_4360;

						{	/* Unsafe/base64.scm 300 */
							long BgL_arg1562z00_4361;
							long BgL_arg1564z00_4362;

							BgL_arg1562z00_4361 = RGC_BUFFER_FORWARD(BgL_iportz00_4275);
							BgL_arg1564z00_4362 = RGC_BUFFER_BUFPOS(BgL_iportz00_4275);
							BgL_iportz00_4499 = BgL_iportz00_4275;
							BgL_lastzd2matchzd2_4500 = 8L;
							BgL_forwardz00_4501 = BgL_arg1562z00_4361;
							BgL_bufposz00_4502 = BgL_arg1564z00_4362;
						BgL_statezd20zd21046z00_4357:
							if ((BgL_forwardz00_4501 == BgL_bufposz00_4502))
								{	/* Unsafe/base64.scm 300 */
									if (rgc_fill_buffer(BgL_iportz00_4499))
										{	/* Unsafe/base64.scm 300 */
											long BgL_arg1346z00_4503;
											long BgL_arg1347z00_4504;

											BgL_arg1346z00_4503 =
												RGC_BUFFER_FORWARD(BgL_iportz00_4499);
											BgL_arg1347z00_4504 =
												RGC_BUFFER_BUFPOS(BgL_iportz00_4499);
											{
												long BgL_bufposz00_4811;
												long BgL_forwardz00_4810;

												BgL_forwardz00_4810 = BgL_arg1346z00_4503;
												BgL_bufposz00_4811 = BgL_arg1347z00_4504;
												BgL_bufposz00_4502 = BgL_bufposz00_4811;
												BgL_forwardz00_4501 = BgL_forwardz00_4810;
												goto BgL_statezd20zd21046z00_4357;
											}
										}
									else
										{	/* Unsafe/base64.scm 300 */
											BgL_matchz00_4360 = BgL_lastzd2matchzd2_4500;
										}
								}
							else
								{	/* Unsafe/base64.scm 300 */
									int BgL_curz00_4505;

									BgL_curz00_4505 =
										RGC_BUFFER_GET_CHAR(BgL_iportz00_4499, BgL_forwardz00_4501);
									{	/* Unsafe/base64.scm 300 */

										{	/* Unsafe/base64.scm 300 */
											bool_t BgL_test2457z00_4813;

											if (((long) (BgL_curz00_4505) == 43L))
												{	/* Unsafe/base64.scm 300 */
													BgL_test2457z00_4813 = ((bool_t) 1);
												}
											else
												{	/* Unsafe/base64.scm 300 */
													if (((long) (BgL_curz00_4505) == 45L))
														{	/* Unsafe/base64.scm 300 */
															BgL_test2457z00_4813 = ((bool_t) 1);
														}
													else
														{	/* Unsafe/base64.scm 300 */
															bool_t BgL_test2460z00_4820;

															if (((long) (BgL_curz00_4505) >= 47L))
																{	/* Unsafe/base64.scm 300 */
																	BgL_test2460z00_4820 =
																		((long) (BgL_curz00_4505) < 58L);
																}
															else
																{	/* Unsafe/base64.scm 300 */
																	BgL_test2460z00_4820 = ((bool_t) 0);
																}
															if (BgL_test2460z00_4820)
																{	/* Unsafe/base64.scm 300 */
																	BgL_test2457z00_4813 = ((bool_t) 1);
																}
															else
																{	/* Unsafe/base64.scm 300 */
																	bool_t BgL_test2462z00_4826;

																	if (((long) (BgL_curz00_4505) >= 65L))
																		{	/* Unsafe/base64.scm 300 */
																			BgL_test2462z00_4826 =
																				((long) (BgL_curz00_4505) < 91L);
																		}
																	else
																		{	/* Unsafe/base64.scm 300 */
																			BgL_test2462z00_4826 = ((bool_t) 0);
																		}
																	if (BgL_test2462z00_4826)
																		{	/* Unsafe/base64.scm 300 */
																			BgL_test2457z00_4813 = ((bool_t) 1);
																		}
																	else
																		{	/* Unsafe/base64.scm 300 */
																			if (((long) (BgL_curz00_4505) == 95L))
																				{	/* Unsafe/base64.scm 300 */
																					BgL_test2457z00_4813 = ((bool_t) 1);
																				}
																			else
																				{	/* Unsafe/base64.scm 300 */
																					if (((long) (BgL_curz00_4505) >= 97L))
																						{	/* Unsafe/base64.scm 300 */
																							BgL_test2457z00_4813 =
																								(
																								(long) (BgL_curz00_4505) <
																								123L);
																						}
																					else
																						{	/* Unsafe/base64.scm 300 */
																							BgL_test2457z00_4813 =
																								((bool_t) 0);
																						}
																				}
																		}
																}
														}
												}
											if (BgL_test2457z00_4813)
												{	/* Unsafe/base64.scm 300 */
													BgL_iportz00_4482 = BgL_iportz00_4499;
													BgL_lastzd2matchzd2_4483 = BgL_lastzd2matchzd2_4500;
													BgL_forwardz00_4484 = (1L + BgL_forwardz00_4501);
													BgL_bufposz00_4485 = BgL_bufposz00_4502;
												BgL_statezd23zd21049z00_4356:
													{	/* Unsafe/base64.scm 300 */
														long BgL_newzd2matchzd2_4486;

														{	/* Unsafe/base64.scm 300 */
															bool_t BgL_test2466z00_4840;

															{	/* Unsafe/base64.scm 300 */
																bool_t BgL_rz00_4487;

																BgL_rz00_4487 =
																	rgc_buffer_eof2_p(BgL_iportz00_4482,
																	BgL_forwardz00_4484, BgL_bufposz00_4485);
																BgL_forwardz00_4484 =
																	RGC_BUFFER_FORWARD(BgL_iportz00_4482);
																BgL_bufposz00_4485 =
																	RGC_BUFFER_BUFPOS(BgL_iportz00_4482);
																BgL_test2466z00_4840 = BgL_rz00_4487;
															}
															if (BgL_test2466z00_4840)
																{	/* Unsafe/base64.scm 300 */
																	RGC_STOP_MATCH(BgL_iportz00_4482,
																		BgL_forwardz00_4484);
																	BgL_newzd2matchzd2_4486 = 6L;
																}
															else
																{	/* Unsafe/base64.scm 300 */
																	RGC_STOP_MATCH(BgL_iportz00_4482,
																		BgL_forwardz00_4484);
																	BgL_newzd2matchzd2_4486 = 8L;
																}
														}
														if ((BgL_forwardz00_4484 == BgL_bufposz00_4485))
															{	/* Unsafe/base64.scm 300 */
																if (rgc_fill_buffer(BgL_iportz00_4482))
																	{	/* Unsafe/base64.scm 300 */
																		long BgL_arg1365z00_4488;
																		long BgL_arg1366z00_4489;

																		BgL_arg1365z00_4488 =
																			RGC_BUFFER_FORWARD(BgL_iportz00_4482);
																		BgL_arg1366z00_4489 =
																			RGC_BUFFER_BUFPOS(BgL_iportz00_4482);
																		{
																			long BgL_bufposz00_4853;
																			long BgL_forwardz00_4852;

																			BgL_forwardz00_4852 = BgL_arg1365z00_4488;
																			BgL_bufposz00_4853 = BgL_arg1366z00_4489;
																			BgL_bufposz00_4485 = BgL_bufposz00_4853;
																			BgL_forwardz00_4484 = BgL_forwardz00_4852;
																			goto BgL_statezd23zd21049z00_4356;
																		}
																	}
																else
																	{	/* Unsafe/base64.scm 300 */
																		BgL_matchz00_4360 = BgL_newzd2matchzd2_4486;
																	}
															}
														else
															{	/* Unsafe/base64.scm 300 */
																int BgL_curz00_4490;

																BgL_curz00_4490 =
																	RGC_BUFFER_GET_CHAR(BgL_iportz00_4482,
																	BgL_forwardz00_4484);
																{	/* Unsafe/base64.scm 300 */

																	if (((long) (BgL_curz00_4490) == 61L))
																		{	/* Unsafe/base64.scm 300 */
																			long BgL_arg1368z00_4491;

																			BgL_arg1368z00_4491 =
																				(1L + BgL_forwardz00_4484);
																			{
																				long BgL_forwardz00_4493;
																				long BgL_bufposz00_4494;

																				BgL_forwardz00_4493 =
																					BgL_arg1368z00_4491;
																				BgL_bufposz00_4494 = BgL_bufposz00_4485;
																			BgL_statezd25zd21051z00_4492:
																				if (
																					(BgL_forwardz00_4493 ==
																						BgL_bufposz00_4494))
																					{	/* Unsafe/base64.scm 300 */
																						if (rgc_fill_buffer
																							(BgL_iportz00_4482))
																							{	/* Unsafe/base64.scm 300 */
																								long BgL_arg1320z00_4495;
																								long BgL_arg1321z00_4496;

																								BgL_arg1320z00_4495 =
																									RGC_BUFFER_FORWARD
																									(BgL_iportz00_4482);
																								BgL_arg1321z00_4496 =
																									RGC_BUFFER_BUFPOS
																									(BgL_iportz00_4482);
																								{
																									long BgL_bufposz00_4866;
																									long BgL_forwardz00_4865;

																									BgL_forwardz00_4865 =
																										BgL_arg1320z00_4495;
																									BgL_bufposz00_4866 =
																										BgL_arg1321z00_4496;
																									BgL_bufposz00_4494 =
																										BgL_bufposz00_4866;
																									BgL_forwardz00_4493 =
																										BgL_forwardz00_4865;
																									goto
																										BgL_statezd25zd21051z00_4492;
																								}
																							}
																						else
																							{	/* Unsafe/base64.scm 300 */
																								BgL_matchz00_4360 =
																									BgL_newzd2matchzd2_4486;
																							}
																					}
																				else
																					{	/* Unsafe/base64.scm 300 */
																						int BgL_curz00_4497;

																						BgL_curz00_4497 =
																							RGC_BUFFER_GET_CHAR
																							(BgL_iportz00_4482,
																							BgL_forwardz00_4493);
																						{	/* Unsafe/base64.scm 300 */

																							if (
																								((long) (BgL_curz00_4497) ==
																									61L))
																								{	/* Unsafe/base64.scm 300 */
																									BgL_iportz00_4457 =
																										BgL_iportz00_4482;
																									BgL_lastzd2matchzd2_4458 =
																										BgL_newzd2matchzd2_4486;
																									BgL_forwardz00_4459 =
																										(1L + BgL_forwardz00_4493);
																									BgL_bufposz00_4460 =
																										BgL_bufposz00_4494;
																								BgL_statezd27zd21053z00_4354:
																									if (
																										(BgL_forwardz00_4459 ==
																											BgL_bufposz00_4460))
																										{	/* Unsafe/base64.scm 300 */
																											if (rgc_fill_buffer
																												(BgL_iportz00_4457))
																												{	/* Unsafe/base64.scm 300 */
																													long
																														BgL_arg1401z00_4461;
																													long
																														BgL_arg1402z00_4462;
																													BgL_arg1401z00_4461 =
																														RGC_BUFFER_FORWARD
																														(BgL_iportz00_4457);
																													BgL_arg1402z00_4462 =
																														RGC_BUFFER_BUFPOS
																														(BgL_iportz00_4457);
																													if (
																														(BgL_arg1401z00_4461
																															==
																															BgL_arg1402z00_4462))
																														{	/* Unsafe/base64.scm 300 */
																															if (rgc_fill_buffer(BgL_iportz00_4457))
																																{	/* Unsafe/base64.scm 300 */
																																	long
																																		BgL_arg1401z00_4463;
																																	long
																																		BgL_arg1402z00_4464;
																																	BgL_arg1401z00_4463
																																		=
																																		RGC_BUFFER_FORWARD
																																		(BgL_iportz00_4457);
																																	BgL_arg1402z00_4464
																																		=
																																		RGC_BUFFER_BUFPOS
																																		(BgL_iportz00_4457);
																																	{
																																		long
																																			BgL_bufposz00_4884;
																																		long
																																			BgL_forwardz00_4883;
																																		BgL_forwardz00_4883
																																			=
																																			BgL_arg1401z00_4463;
																																		BgL_bufposz00_4884
																																			=
																																			BgL_arg1402z00_4464;
																																		BgL_bufposz00_4460
																																			=
																																			BgL_bufposz00_4884;
																																		BgL_forwardz00_4459
																																			=
																																			BgL_forwardz00_4883;
																																		goto
																																			BgL_statezd27zd21053z00_4354;
																																	}
																																}
																															else
																																{	/* Unsafe/base64.scm 300 */
																																	BgL_matchz00_4360
																																		=
																																		BgL_lastzd2matchzd2_4458;
																																}
																														}
																													else
																														{	/* Unsafe/base64.scm 300 */
																															int
																																BgL_curz00_4465;
																															BgL_curz00_4465 =
																																RGC_BUFFER_GET_CHAR
																																(BgL_iportz00_4457,
																																BgL_arg1401z00_4461);
																															{	/* Unsafe/base64.scm 300 */

																																if (
																																	((long)
																																		(BgL_curz00_4465)
																																		== 61L))
																																	{	/* Unsafe/base64.scm 300 */
																																		long
																																			BgL_arg1404z00_4466;
																																		BgL_arg1404z00_4466
																																			=
																																			(1L +
																																			BgL_arg1401z00_4461);
																																		{	/* Unsafe/base64.scm 300 */
																																			long
																																				BgL_newzd2matchzd2_4467;
																																			RGC_STOP_MATCH
																																				(BgL_iportz00_4457,
																																				BgL_arg1404z00_4466);
																																			BgL_newzd2matchzd2_4467
																																				= 5L;
																																			BgL_matchz00_4360
																																				=
																																				BgL_newzd2matchzd2_4467;
																																	}}
																																else
																																	{	/* Unsafe/base64.scm 300 */
																																		BgL_matchz00_4360
																																			=
																																			BgL_lastzd2matchzd2_4458;
																																	}
																															}
																														}
																												}
																											else
																												{	/* Unsafe/base64.scm 300 */
																													BgL_matchz00_4360 =
																														BgL_lastzd2matchzd2_4458;
																												}
																										}
																									else
																										{	/* Unsafe/base64.scm 300 */
																											int BgL_curz00_4468;

																											BgL_curz00_4468 =
																												RGC_BUFFER_GET_CHAR
																												(BgL_iportz00_4457,
																												BgL_forwardz00_4459);
																											{	/* Unsafe/base64.scm 300 */

																												if (
																													((long)
																														(BgL_curz00_4468) ==
																														61L))
																													{	/* Unsafe/base64.scm 300 */
																														long
																															BgL_arg1404z00_4469;
																														BgL_arg1404z00_4469
																															=
																															(1L +
																															BgL_forwardz00_4459);
																														{	/* Unsafe/base64.scm 300 */
																															long
																																BgL_newzd2matchzd2_4470;
																															RGC_STOP_MATCH
																																(BgL_iportz00_4457,
																																BgL_arg1404z00_4469);
																															BgL_newzd2matchzd2_4470
																																= 5L;
																															BgL_matchz00_4360
																																=
																																BgL_newzd2matchzd2_4470;
																													}}
																												else
																													{	/* Unsafe/base64.scm 300 */
																														BgL_matchz00_4360 =
																															BgL_lastzd2matchzd2_4458;
																													}
																											}
																										}
																								}
																							else
																								{	/* Unsafe/base64.scm 300 */
																									BgL_matchz00_4360 =
																										BgL_newzd2matchzd2_4486;
																								}
																						}
																					}
																			}
																		}
																	else
																		{	/* Unsafe/base64.scm 300 */
																			bool_t BgL_test2479z00_4898;

																			if (((long) (BgL_curz00_4490) == 43L))
																				{	/* Unsafe/base64.scm 300 */
																					BgL_test2479z00_4898 = ((bool_t) 1);
																				}
																			else
																				{	/* Unsafe/base64.scm 300 */
																					if (((long) (BgL_curz00_4490) == 45L))
																						{	/* Unsafe/base64.scm 300 */
																							BgL_test2479z00_4898 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Unsafe/base64.scm 300 */
																							bool_t BgL_test2482z00_4905;

																							if (
																								((long) (BgL_curz00_4490) >=
																									47L))
																								{	/* Unsafe/base64.scm 300 */
																									BgL_test2482z00_4905 =
																										(
																										(long) (BgL_curz00_4490) <
																										58L);
																								}
																							else
																								{	/* Unsafe/base64.scm 300 */
																									BgL_test2482z00_4905 =
																										((bool_t) 0);
																								}
																							if (BgL_test2482z00_4905)
																								{	/* Unsafe/base64.scm 300 */
																									BgL_test2479z00_4898 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Unsafe/base64.scm 300 */
																									bool_t BgL_test2484z00_4911;

																									if (
																										((long) (BgL_curz00_4490) >=
																											65L))
																										{	/* Unsafe/base64.scm 300 */
																											BgL_test2484z00_4911 =
																												(
																												(long) (BgL_curz00_4490)
																												< 91L);
																										}
																									else
																										{	/* Unsafe/base64.scm 300 */
																											BgL_test2484z00_4911 =
																												((bool_t) 0);
																										}
																									if (BgL_test2484z00_4911)
																										{	/* Unsafe/base64.scm 300 */
																											BgL_test2479z00_4898 =
																												((bool_t) 1);
																										}
																									else
																										{	/* Unsafe/base64.scm 300 */
																											if (
																												((long)
																													(BgL_curz00_4490) ==
																													95L))
																												{	/* Unsafe/base64.scm 300 */
																													BgL_test2479z00_4898 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Unsafe/base64.scm 300 */
																													if (
																														((long)
																															(BgL_curz00_4490)
																															>= 97L))
																														{	/* Unsafe/base64.scm 300 */
																															BgL_test2479z00_4898
																																=
																																((long)
																																(BgL_curz00_4490)
																																< 123L);
																														}
																													else
																														{	/* Unsafe/base64.scm 300 */
																															BgL_test2479z00_4898
																																= ((bool_t) 0);
																														}
																												}
																										}
																								}
																						}
																				}
																			if (BgL_test2479z00_4898)
																				{	/* Unsafe/base64.scm 300 */
																					long BgL_arg1377z00_4498;

																					BgL_arg1377z00_4498 =
																						(1L + BgL_forwardz00_4484);
																					BgL_iportz00_4471 = BgL_iportz00_4482;
																					BgL_lastzd2matchzd2_4472 =
																						BgL_newzd2matchzd2_4486;
																					BgL_forwardz00_4473 =
																						BgL_arg1377z00_4498;
																					BgL_bufposz00_4474 =
																						BgL_bufposz00_4485;
																				BgL_statezd24zd21050z00_4355:
																					{	/* Unsafe/base64.scm 300 */
																						long BgL_newzd2matchzd2_4475;

																						{	/* Unsafe/base64.scm 300 */
																							bool_t BgL_test2488z00_4926;

																							{	/* Unsafe/base64.scm 300 */
																								bool_t BgL_rz00_4476;

																								BgL_rz00_4476 =
																									rgc_buffer_eof2_p
																									(BgL_iportz00_4471,
																									BgL_forwardz00_4473,
																									BgL_bufposz00_4474);
																								BgL_forwardz00_4473 =
																									RGC_BUFFER_FORWARD
																									(BgL_iportz00_4471);
																								BgL_bufposz00_4474 =
																									RGC_BUFFER_BUFPOS
																									(BgL_iportz00_4471);
																								BgL_test2488z00_4926 =
																									BgL_rz00_4476;
																							}
																							if (BgL_test2488z00_4926)
																								{	/* Unsafe/base64.scm 300 */
																									RGC_STOP_MATCH
																										(BgL_iportz00_4471,
																										BgL_forwardz00_4473);
																									BgL_newzd2matchzd2_4475 = 4L;
																								}
																							else
																								{	/* Unsafe/base64.scm 300 */
																									BgL_newzd2matchzd2_4475 =
																										BgL_lastzd2matchzd2_4472;
																								}
																						}
																						if (
																							(BgL_forwardz00_4473 ==
																								BgL_bufposz00_4474))
																							{	/* Unsafe/base64.scm 300 */
																								if (rgc_fill_buffer
																									(BgL_iportz00_4471))
																									{	/* Unsafe/base64.scm 300 */
																										long BgL_arg1383z00_4477;
																										long BgL_arg1384z00_4478;

																										BgL_arg1383z00_4477 =
																											RGC_BUFFER_FORWARD
																											(BgL_iportz00_4471);
																										BgL_arg1384z00_4478 =
																											RGC_BUFFER_BUFPOS
																											(BgL_iportz00_4471);
																										{
																											long BgL_bufposz00_4938;
																											long BgL_forwardz00_4937;

																											BgL_forwardz00_4937 =
																												BgL_arg1383z00_4477;
																											BgL_bufposz00_4938 =
																												BgL_arg1384z00_4478;
																											BgL_bufposz00_4474 =
																												BgL_bufposz00_4938;
																											BgL_forwardz00_4473 =
																												BgL_forwardz00_4937;
																											goto
																												BgL_statezd24zd21050z00_4355;
																										}
																									}
																								else
																									{	/* Unsafe/base64.scm 300 */
																										BgL_matchz00_4360 =
																											BgL_newzd2matchzd2_4475;
																									}
																							}
																						else
																							{	/* Unsafe/base64.scm 300 */
																								int BgL_curz00_4479;

																								BgL_curz00_4479 =
																									RGC_BUFFER_GET_CHAR
																									(BgL_iportz00_4471,
																									BgL_forwardz00_4473);
																								{	/* Unsafe/base64.scm 300 */

																									if (
																										((long) (BgL_curz00_4479) ==
																											61L))
																										{	/* Unsafe/base64.scm 300 */
																											long BgL_arg1387z00_4480;

																											BgL_arg1387z00_4480 =
																												(1L +
																												BgL_forwardz00_4473);
																											BgL_iportz00_4525 =
																												BgL_iportz00_4471;
																											BgL_lastzd2matchzd2_4526 =
																												BgL_newzd2matchzd2_4475;
																											BgL_forwardz00_4527 =
																												BgL_arg1387z00_4480;
																											BgL_bufposz00_4528 =
																												BgL_bufposz00_4474;
																										BgL_statezd210zd21056z00_4359:
																											{	/* Unsafe/base64.scm 300 */
																												long
																													BgL_newzd2matchzd2_4529;
																												{	/* Unsafe/base64.scm 300 */
																													bool_t
																														BgL_test2492z00_4944;
																													{	/* Unsafe/base64.scm 300 */
																														bool_t
																															BgL_rz00_4530;
																														BgL_rz00_4530 =
																															rgc_buffer_eof2_p
																															(BgL_iportz00_4525,
																															BgL_forwardz00_4527,
																															BgL_bufposz00_4528);
																														BgL_forwardz00_4527
																															=
																															RGC_BUFFER_FORWARD
																															(BgL_iportz00_4525);
																														BgL_bufposz00_4528 =
																															RGC_BUFFER_BUFPOS
																															(BgL_iportz00_4525);
																														BgL_test2492z00_4944
																															= BgL_rz00_4530;
																													}
																													if (BgL_test2492z00_4944)
																														{	/* Unsafe/base64.scm 300 */
																															RGC_STOP_MATCH
																																(BgL_iportz00_4525,
																																BgL_forwardz00_4527);
																															BgL_newzd2matchzd2_4529
																																= 4L;
																														}
																													else
																														{	/* Unsafe/base64.scm 300 */
																															BgL_newzd2matchzd2_4529
																																=
																																BgL_lastzd2matchzd2_4526;
																														}
																												}
																												if (
																													(BgL_forwardz00_4527
																														==
																														BgL_bufposz00_4528))
																													{	/* Unsafe/base64.scm 300 */
																														if (rgc_fill_buffer
																															(BgL_iportz00_4525))
																															{	/* Unsafe/base64.scm 300 */
																																long
																																	BgL_arg1310z00_4531;
																																long
																																	BgL_arg1311z00_4532;
																																BgL_arg1310z00_4531
																																	=
																																	RGC_BUFFER_FORWARD
																																	(BgL_iportz00_4525);
																																BgL_arg1311z00_4532
																																	=
																																	RGC_BUFFER_BUFPOS
																																	(BgL_iportz00_4525);
																																{
																																	long
																																		BgL_bufposz00_4956;
																																	long
																																		BgL_forwardz00_4955;
																																	BgL_forwardz00_4955
																																		=
																																		BgL_arg1310z00_4531;
																																	BgL_bufposz00_4956
																																		=
																																		BgL_arg1311z00_4532;
																																	BgL_bufposz00_4528
																																		=
																																		BgL_bufposz00_4956;
																																	BgL_forwardz00_4527
																																		=
																																		BgL_forwardz00_4955;
																																	goto
																																		BgL_statezd210zd21056z00_4359;
																																}
																															}
																														else
																															{	/* Unsafe/base64.scm 300 */
																																BgL_matchz00_4360
																																	=
																																	BgL_newzd2matchzd2_4529;
																															}
																													}
																												else
																													{	/* Unsafe/base64.scm 300 */
																														int BgL_curz00_4533;

																														BgL_curz00_4533 =
																															RGC_BUFFER_GET_CHAR
																															(BgL_iportz00_4525,
																															BgL_forwardz00_4527);
																														{	/* Unsafe/base64.scm 300 */

																															if (
																																((long)
																																	(BgL_curz00_4533)
																																	== 61L))
																																{	/* Unsafe/base64.scm 300 */
																																	long
																																		BgL_arg1314z00_4534;
																																	BgL_arg1314z00_4534
																																		=
																																		(1L +
																																		BgL_forwardz00_4527);
																																	{	/* Unsafe/base64.scm 300 */
																																		long
																																			BgL_bufposz00_4535;
																																		BgL_bufposz00_4535
																																			=
																																			BgL_bufposz00_4528;
																																		{	/* Unsafe/base64.scm 300 */
																																			long
																																				BgL_newzd2matchzd2_4536;
																																			RGC_STOP_MATCH
																																				(BgL_iportz00_4525,
																																				BgL_arg1314z00_4534);
																																			BgL_newzd2matchzd2_4536
																																				= 3L;
																																			BgL_matchz00_4360
																																				=
																																				BgL_newzd2matchzd2_4536;
																																}}}
																															else
																																{	/* Unsafe/base64.scm 300 */
																																	BgL_matchz00_4360
																																		=
																																		BgL_newzd2matchzd2_4529;
																																}
																														}
																													}
																											}
																										}
																									else
																										{	/* Unsafe/base64.scm 300 */
																											bool_t
																												BgL_test2496z00_4963;
																											if (((long)
																													(BgL_curz00_4479) ==
																													43L))
																												{	/* Unsafe/base64.scm 300 */
																													BgL_test2496z00_4963 =
																														((bool_t) 1);
																												}
																											else
																												{	/* Unsafe/base64.scm 300 */
																													if (
																														((long)
																															(BgL_curz00_4479)
																															== 45L))
																														{	/* Unsafe/base64.scm 300 */
																															BgL_test2496z00_4963
																																= ((bool_t) 1);
																														}
																													else
																														{	/* Unsafe/base64.scm 300 */
																															bool_t
																																BgL_test2499z00_4970;
																															if (((long)
																																	(BgL_curz00_4479)
																																	>= 47L))
																																{	/* Unsafe/base64.scm 300 */
																																	BgL_test2499z00_4970
																																		=
																																		((long)
																																		(BgL_curz00_4479)
																																		< 58L);
																																}
																															else
																																{	/* Unsafe/base64.scm 300 */
																																	BgL_test2499z00_4970
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test2499z00_4970)
																																{	/* Unsafe/base64.scm 300 */
																																	BgL_test2496z00_4963
																																		=
																																		((bool_t)
																																		1);
																																}
																															else
																																{	/* Unsafe/base64.scm 300 */
																																	bool_t
																																		BgL_test2501z00_4976;
																																	if (((long)
																																			(BgL_curz00_4479)
																																			>= 65L))
																																		{	/* Unsafe/base64.scm 300 */
																																			BgL_test2501z00_4976
																																				=
																																				((long)
																																				(BgL_curz00_4479)
																																				< 91L);
																																		}
																																	else
																																		{	/* Unsafe/base64.scm 300 */
																																			BgL_test2501z00_4976
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																	if (BgL_test2501z00_4976)
																																		{	/* Unsafe/base64.scm 300 */
																																			BgL_test2496z00_4963
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																	else
																																		{	/* Unsafe/base64.scm 300 */
																																			if (
																																				((long)
																																					(BgL_curz00_4479)
																																					==
																																					95L))
																																				{	/* Unsafe/base64.scm 300 */
																																					BgL_test2496z00_4963
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Unsafe/base64.scm 300 */
																																					if (
																																						((long) (BgL_curz00_4479) >= 97L))
																																						{	/* Unsafe/base64.scm 300 */
																																							BgL_test2496z00_4963
																																								=
																																								(
																																								(long)
																																								(BgL_curz00_4479)
																																								<
																																								123L);
																																						}
																																					else
																																						{	/* Unsafe/base64.scm 300 */
																																							BgL_test2496z00_4963
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																}
																														}
																												}
																											if (BgL_test2496z00_4963)
																												{	/* Unsafe/base64.scm 300 */
																													long
																														BgL_arg1396z00_4481;
																													BgL_arg1396z00_4481 =
																														(1L +
																														BgL_forwardz00_4473);
																													BgL_iportz00_4510 =
																														BgL_iportz00_4471;
																													BgL_lastzd2matchzd2_4511
																														=
																														BgL_newzd2matchzd2_4475;
																													BgL_forwardz00_4512 =
																														BgL_arg1396z00_4481;
																													BgL_bufposz00_4513 =
																														BgL_bufposz00_4474;
																												BgL_statezd29zd21055z00_4358:
																													{	/* Unsafe/base64.scm 300 */
																														long
																															BgL_newzd2matchzd2_4514;
																														{	/* Unsafe/base64.scm 300 */
																															bool_t
																																BgL_test2505z00_4991;
																															{	/* Unsafe/base64.scm 300 */
																																bool_t
																																	BgL_rz00_4515;
																																BgL_rz00_4515 =
																																	rgc_buffer_eof2_p
																																	(BgL_iportz00_4510,
																																	BgL_forwardz00_4512,
																																	BgL_bufposz00_4513);
																																BgL_forwardz00_4512
																																	=
																																	RGC_BUFFER_FORWARD
																																	(BgL_iportz00_4510);
																																BgL_bufposz00_4513
																																	=
																																	RGC_BUFFER_BUFPOS
																																	(BgL_iportz00_4510);
																																BgL_test2505z00_4991
																																	=
																																	BgL_rz00_4515;
																															}
																															if (BgL_test2505z00_4991)
																																{	/* Unsafe/base64.scm 300 */
																																	RGC_STOP_MATCH
																																		(BgL_iportz00_4510,
																																		BgL_forwardz00_4512);
																																	BgL_newzd2matchzd2_4514
																																		= 2L;
																																}
																															else
																																{	/* Unsafe/base64.scm 300 */
																																	BgL_newzd2matchzd2_4514
																																		=
																																		BgL_lastzd2matchzd2_4511;
																																}
																														}
																														if (
																															(BgL_forwardz00_4512
																																==
																																BgL_bufposz00_4513))
																															{	/* Unsafe/base64.scm 300 */
																																if (rgc_fill_buffer(BgL_iportz00_4510))
																																	{	/* Unsafe/base64.scm 300 */
																																		long
																																			BgL_arg1328z00_4516;
																																		long
																																			BgL_arg1329z00_4517;
																																		BgL_arg1328z00_4516
																																			=
																																			RGC_BUFFER_FORWARD
																																			(BgL_iportz00_4510);
																																		BgL_arg1329z00_4517
																																			=
																																			RGC_BUFFER_BUFPOS
																																			(BgL_iportz00_4510);
																																		{
																																			long
																																				BgL_bufposz00_5003;
																																			long
																																				BgL_forwardz00_5002;
																																			BgL_forwardz00_5002
																																				=
																																				BgL_arg1328z00_4516;
																																			BgL_bufposz00_5003
																																				=
																																				BgL_arg1329z00_4517;
																																			BgL_bufposz00_4513
																																				=
																																				BgL_bufposz00_5003;
																																			BgL_forwardz00_4512
																																				=
																																				BgL_forwardz00_5002;
																																			goto
																																				BgL_statezd29zd21055z00_4358;
																																		}
																																	}
																																else
																																	{	/* Unsafe/base64.scm 300 */
																																		BgL_matchz00_4360
																																			=
																																			BgL_newzd2matchzd2_4514;
																																	}
																															}
																														else
																															{	/* Unsafe/base64.scm 300 */
																																int
																																	BgL_curz00_4518;
																																BgL_curz00_4518
																																	=
																																	RGC_BUFFER_GET_CHAR
																																	(BgL_iportz00_4510,
																																	BgL_forwardz00_4512);
																																{	/* Unsafe/base64.scm 300 */

																																	if (
																																		((long)
																																			(BgL_curz00_4518)
																																			== 61L))
																																		{	/* Unsafe/base64.scm 300 */
																																			long
																																				BgL_arg1331z00_4519;
																																			BgL_arg1331z00_4519
																																				=
																																				(1L +
																																				BgL_forwardz00_4512);
																																			{	/* Unsafe/base64.scm 300 */
																																				long
																																					BgL_bufposz00_4520;
																																				BgL_bufposz00_4520
																																					=
																																					BgL_bufposz00_4513;
																																				{	/* Unsafe/base64.scm 300 */
																																					long
																																						BgL_newzd2matchzd2_4521;
																																					RGC_STOP_MATCH
																																						(BgL_iportz00_4510,
																																						BgL_arg1331z00_4519);
																																					BgL_newzd2matchzd2_4521
																																						=
																																						1L;
																																					BgL_matchz00_4360
																																						=
																																						BgL_newzd2matchzd2_4521;
																																		}}}
																																	else
																																		{	/* Unsafe/base64.scm 300 */
																																			bool_t
																																				BgL_test2509z00_5010;
																																			if ((
																																					(long)
																																					(BgL_curz00_4518)
																																					==
																																					43L))
																																				{	/* Unsafe/base64.scm 300 */
																																					BgL_test2509z00_5010
																																						=
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Unsafe/base64.scm 300 */
																																					if (
																																						((long) (BgL_curz00_4518) == 45L))
																																						{	/* Unsafe/base64.scm 300 */
																																							BgL_test2509z00_5010
																																								=
																																								(
																																								(bool_t)
																																								1);
																																						}
																																					else
																																						{	/* Unsafe/base64.scm 300 */
																																							bool_t
																																								BgL_test2512z00_5017;
																																							if (((long) (BgL_curz00_4518) >= 47L))
																																								{	/* Unsafe/base64.scm 300 */
																																									BgL_test2512z00_5017
																																										=
																																										(
																																										(long)
																																										(BgL_curz00_4518)
																																										<
																																										58L);
																																								}
																																							else
																																								{	/* Unsafe/base64.scm 300 */
																																									BgL_test2512z00_5017
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																							if (BgL_test2512z00_5017)
																																								{	/* Unsafe/base64.scm 300 */
																																									BgL_test2509z00_5010
																																										=
																																										(
																																										(bool_t)
																																										1);
																																								}
																																							else
																																								{	/* Unsafe/base64.scm 300 */
																																									bool_t
																																										BgL_test2514z00_5023;
																																									if (((long) (BgL_curz00_4518) >= 65L))
																																										{	/* Unsafe/base64.scm 300 */
																																											BgL_test2514z00_5023
																																												=
																																												(
																																												(long)
																																												(BgL_curz00_4518)
																																												<
																																												91L);
																																										}
																																									else
																																										{	/* Unsafe/base64.scm 300 */
																																											BgL_test2514z00_5023
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																									if (BgL_test2514z00_5023)
																																										{	/* Unsafe/base64.scm 300 */
																																											BgL_test2509z00_5010
																																												=
																																												(
																																												(bool_t)
																																												1);
																																										}
																																									else
																																										{	/* Unsafe/base64.scm 300 */
																																											if (((long) (BgL_curz00_4518) == 95L))
																																												{	/* Unsafe/base64.scm 300 */
																																													BgL_test2509z00_5010
																																														=
																																														(
																																														(bool_t)
																																														1);
																																												}
																																											else
																																												{	/* Unsafe/base64.scm 300 */
																																													if (((long) (BgL_curz00_4518) >= 97L))
																																														{	/* Unsafe/base64.scm 300 */
																																															BgL_test2509z00_5010
																																																=
																																																(
																																																(long)
																																																(BgL_curz00_4518)
																																																<
																																																123L);
																																														}
																																													else
																																														{	/* Unsafe/base64.scm 300 */
																																															BgL_test2509z00_5010
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																			if (BgL_test2509z00_5010)
																																				{	/* Unsafe/base64.scm 300 */
																																					long
																																						BgL_arg1340z00_4522;
																																					BgL_arg1340z00_4522
																																						=
																																						(1L
																																						+
																																						BgL_forwardz00_4512);
																																					{	/* Unsafe/base64.scm 300 */
																																						long
																																							BgL_bufposz00_4523;
																																						BgL_bufposz00_4523
																																							=
																																							BgL_bufposz00_4513;
																																						{	/* Unsafe/base64.scm 300 */
																																							long
																																								BgL_newzd2matchzd2_4524;
																																							RGC_STOP_MATCH
																																								(BgL_iportz00_4510,
																																								BgL_arg1340z00_4522);
																																							BgL_newzd2matchzd2_4524
																																								=
																																								0L;
																																							BgL_matchz00_4360
																																								=
																																								BgL_newzd2matchzd2_4524;
																																				}}}
																																			else
																																				{	/* Unsafe/base64.scm 300 */
																																					BgL_matchz00_4360
																																						=
																																						BgL_newzd2matchzd2_4514;
																																				}
																																		}
																																}
																															}
																													}
																												}
																											else
																												{	/* Unsafe/base64.scm 300 */
																													BgL_matchz00_4360 =
																														BgL_newzd2matchzd2_4475;
																												}
																										}
																								}
																							}
																					}
																				}
																			else
																				{	/* Unsafe/base64.scm 300 */
																					BgL_matchz00_4360 =
																						BgL_newzd2matchzd2_4486;
																				}
																		}
																}
															}
													}
												}
											else
												{	/* Unsafe/base64.scm 300 */
													bool_t BgL_test2518z00_5040;

													if (((long) (BgL_curz00_4505) == 10L))
														{	/* Unsafe/base64.scm 300 */
															BgL_test2518z00_5040 = ((bool_t) 1);
														}
													else
														{	/* Unsafe/base64.scm 300 */
															BgL_test2518z00_5040 =
																((long) (BgL_curz00_4505) == 13L);
														}
													if (BgL_test2518z00_5040)
														{	/* Unsafe/base64.scm 300 */
															long BgL_arg1359z00_4506;

															BgL_arg1359z00_4506 = (1L + BgL_forwardz00_4501);
															{	/* Unsafe/base64.scm 300 */
																long BgL_newzd2matchzd2_4507;

																RGC_STOP_MATCH(BgL_iportz00_4499,
																	BgL_arg1359z00_4506);
																BgL_newzd2matchzd2_4507 = 7L;
																BgL_matchz00_4360 = BgL_newzd2matchzd2_4507;
														}}
													else
														{	/* Unsafe/base64.scm 300 */
															long BgL_arg1360z00_4508;

															BgL_arg1360z00_4508 = (1L + BgL_forwardz00_4501);
															{	/* Unsafe/base64.scm 300 */
																long BgL_newzd2matchzd2_4509;

																RGC_STOP_MATCH(BgL_iportz00_4499,
																	BgL_arg1360z00_4508);
																BgL_newzd2matchzd2_4509 = 8L;
																BgL_matchz00_4360 = BgL_newzd2matchzd2_4509;
						}}}}}}}
						RGC_SET_FILEPOS(BgL_iportz00_4275);
						switch (BgL_matchz00_4360)
							{
							case 8L:

								{	/* Unsafe/base64.scm 369 */
									obj_t BgL_cz00_4363;

									{	/* Unsafe/base64.scm 300 */
										bool_t BgL_test2520z00_5051;

										{	/* Unsafe/base64.scm 300 */
											long BgL_arg1488z00_4456;

											BgL_arg1488z00_4456 =
												RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4275);
											BgL_test2520z00_5051 = (BgL_arg1488z00_4456 == 0L);
										}
										if (BgL_test2520z00_5051)
											{	/* Unsafe/base64.scm 300 */
												BgL_cz00_4363 = BEOF;
											}
										else
											{	/* Unsafe/base64.scm 300 */
												BgL_cz00_4363 =
													BCHAR(RGC_BUFFER_CHARACTER(BgL_iportz00_4275));
											}
									}
									{	/* Unsafe/base64.scm 370 */
										bool_t BgL_test2521z00_5056;

										if (EOF_OBJECTP(BgL_cz00_4363))
											{	/* Unsafe/base64.scm 370 */
												BgL_test2521z00_5056 = ((bool_t) 1);
											}
										else
											{	/* Unsafe/base64.scm 370 */
												BgL_test2521z00_5056 =
													CBOOL(
													((obj_t(*)(obj_t,
																obj_t))
														PROCEDURE_L_ENTRY(BgL_hookz00_4280))
													(BgL_hookz00_4280, BgL_cz00_4363));
											}
										if (BgL_test2521z00_5056)
											{	/* Unsafe/base64.scm 370 */
												if ((BgL_wz00_4344 > 0L))
													{	/* Unsafe/base64.scm 372 */
														bgl_display_substring(BgL_bufz00_4277, 0L,
															BgL_wz00_4344, BgL_opz00_4276);
													}
												else
													{	/* Unsafe/base64.scm 372 */
														BFALSE;
													}
												return BTRUE;
											}
										else
											{	/* Unsafe/base64.scm 370 */
												goto BgL_ignorez00_4348;
											}
									}
								}
								break;
							case 7L:

								goto BgL_ignorez00_4348;
								break;
							case 6L:

								if (BgL_eofzd2nozd2paddingz00_4281)
									{	/* Unsafe/base64.scm 365 */
									BgL_chunk1z00_4352:
										{	/* Unsafe/base64.scm 345 */
											int BgL_q0z00_4439;

											{	/* Unsafe/base64.scm 345 */
												int BgL_arg1499z00_4440;

												{	/* Unsafe/base64.scm 300 */
													int BgL_tmpz00_5068;

													BgL_tmpz00_5068 = (int) (0L);
													BgL_arg1499z00_4440 =
														RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
														BgL_tmpz00_5068);
												}
												{	/* Unsafe/base64.scm 345 */
													int BgL_res2314z00_4441;

													{	/* Unsafe/base64.scm 345 */
														char BgL_cz00_4442;

														BgL_cz00_4442 = (signed char) (BgL_arg1499z00_4440);
														{	/* Unsafe/base64.scm 195 */
															uint8_t BgL_arg1856z00_4443;

															{	/* Unsafe/base64.scm 195 */
																long BgL_tmpz00_5072;

																BgL_tmpz00_5072 = (long) (BgL_cz00_4442);
																BgL_arg1856z00_4443 =
																	BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																	BgL_tmpz00_5072);
															}
															{	/* Unsafe/base64.scm 195 */
																int8_t BgL_xz00_4444;

																BgL_xz00_4444 = (int8_t) (BgL_arg1856z00_4443);
																{	/* Unsafe/base64.scm 195 */
																	long BgL_arg2274z00_4445;

																	BgL_arg2274z00_4445 = (long) (BgL_xz00_4444);
																	BgL_res2314z00_4441 =
																		(int) ((long) (BgL_arg2274z00_4445));
													}}}}
													BgL_q0z00_4439 = BgL_res2314z00_4441;
											}}
											{	/* Unsafe/base64.scm 345 */
												int BgL_q1z00_4446;

												{	/* Unsafe/base64.scm 346 */
													unsigned char BgL_cz00_4447;

													BgL_cz00_4447 = (char) (((unsigned char) '='));
													{	/* Unsafe/base64.scm 201 */
														long BgL_arg1857z00_4448;

														BgL_arg1857z00_4448 =
															((unsigned char) (BgL_cz00_4447));
														{	/* Unsafe/base64.scm 201 */
															int BgL_res2315z00_4449;

															{	/* Unsafe/base64.scm 201 */
																char BgL_cz00_4450;

																BgL_cz00_4450 =
																	(signed char) (BgL_arg1857z00_4448);
																{	/* Unsafe/base64.scm 195 */
																	uint8_t BgL_arg1856z00_4451;

																	{	/* Unsafe/base64.scm 195 */
																		long BgL_tmpz00_5083;

																		BgL_tmpz00_5083 = (long) (BgL_cz00_4450);
																		BgL_arg1856z00_4451 =
																			BGL_U8VREF
																			(BGl_bytezd2tablezd2zz__base64z00,
																			BgL_tmpz00_5083);
																	}
																	{	/* Unsafe/base64.scm 195 */
																		int8_t BgL_xz00_4452;

																		BgL_xz00_4452 =
																			(int8_t) (BgL_arg1856z00_4451);
																		{	/* Unsafe/base64.scm 195 */
																			long BgL_arg2274z00_4453;

																			BgL_arg2274z00_4453 =
																				(long) (BgL_xz00_4452);
																			BgL_res2315z00_4449 =
																				(int) ((long) (BgL_arg2274z00_4453));
															}}}}
															BgL_q1z00_4446 = BgL_res2315z00_4449;
												}}}
												{	/* Unsafe/base64.scm 346 */
													long BgL_v0z00_4454;

													BgL_v0z00_4454 =
														(
														((long) (BgL_q0z00_4439) <<
															(int) (2L)) |
														((long) (BgL_q1z00_4446) >> (int) (4L)));
													{	/* Unsafe/base64.scm 347 */

														{	/* Unsafe/base64.scm 348 */
															unsigned char BgL_arg1495z00_4455;

															BgL_arg1495z00_4455 = (BgL_v0z00_4454);
															STRING_SET(BgL_bufz00_4277, BgL_wz00_4344,
																BgL_arg1495z00_4455);
														}
														return
															bgl_display_substring(BgL_bufz00_4277, 0L,
															BgL_wz00_4344, BgL_opz00_4276);
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/base64.scm 365 */
										return BFALSE;
									}
								break;
							case 5L:

								goto BgL_chunk1z00_4352;
								break;
							case 4L:

								if (BgL_eofzd2nozd2paddingz00_4281)
									{	/* Unsafe/base64.scm 361 */
									BgL_chunk2z00_4351:
										{	/* Unsafe/base64.scm 336 */
											int BgL_q0z00_4422;

											{	/* Unsafe/base64.scm 336 */
												int BgL_arg1509z00_4423;

												{	/* Unsafe/base64.scm 300 */
													int BgL_tmpz00_5101;

													BgL_tmpz00_5101 = (int) (0L);
													BgL_arg1509z00_4423 =
														RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
														BgL_tmpz00_5101);
												}
												{	/* Unsafe/base64.scm 336 */
													int BgL_res2316z00_4424;

													{	/* Unsafe/base64.scm 336 */
														char BgL_cz00_4425;

														BgL_cz00_4425 = (signed char) (BgL_arg1509z00_4423);
														{	/* Unsafe/base64.scm 195 */
															uint8_t BgL_arg1856z00_4426;

															{	/* Unsafe/base64.scm 195 */
																long BgL_tmpz00_5105;

																BgL_tmpz00_5105 = (long) (BgL_cz00_4425);
																BgL_arg1856z00_4426 =
																	BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																	BgL_tmpz00_5105);
															}
															{	/* Unsafe/base64.scm 195 */
																int8_t BgL_xz00_4427;

																BgL_xz00_4427 = (int8_t) (BgL_arg1856z00_4426);
																{	/* Unsafe/base64.scm 195 */
																	long BgL_arg2274z00_4428;

																	BgL_arg2274z00_4428 = (long) (BgL_xz00_4427);
																	BgL_res2316z00_4424 =
																		(int) ((long) (BgL_arg2274z00_4428));
													}}}}
													BgL_q0z00_4422 = BgL_res2316z00_4424;
											}}
											{	/* Unsafe/base64.scm 336 */
												int BgL_q1z00_4429;

												{	/* Unsafe/base64.scm 337 */
													int BgL_arg1508z00_4430;

													{	/* Unsafe/base64.scm 300 */
														int BgL_tmpz00_5112;

														BgL_tmpz00_5112 = (int) (1L);
														BgL_arg1508z00_4430 =
															RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
															BgL_tmpz00_5112);
													}
													{	/* Unsafe/base64.scm 337 */
														int BgL_res2317z00_4431;

														{	/* Unsafe/base64.scm 337 */
															char BgL_cz00_4432;

															BgL_cz00_4432 =
																(signed char) (BgL_arg1508z00_4430);
															{	/* Unsafe/base64.scm 195 */
																uint8_t BgL_arg1856z00_4433;

																{	/* Unsafe/base64.scm 195 */
																	long BgL_tmpz00_5116;

																	BgL_tmpz00_5116 = (long) (BgL_cz00_4432);
																	BgL_arg1856z00_4433 =
																		BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																		BgL_tmpz00_5116);
																}
																{	/* Unsafe/base64.scm 195 */
																	int8_t BgL_xz00_4434;

																	BgL_xz00_4434 =
																		(int8_t) (BgL_arg1856z00_4433);
																	{	/* Unsafe/base64.scm 195 */
																		long BgL_arg2274z00_4435;

																		BgL_arg2274z00_4435 =
																			(long) (BgL_xz00_4434);
																		BgL_res2317z00_4431 =
																			(int) ((long) (BgL_arg2274z00_4435));
														}}}}
														BgL_q1z00_4429 = BgL_res2317z00_4431;
												}}
												{	/* Unsafe/base64.scm 337 */
													long BgL_v0z00_4436;

													BgL_v0z00_4436 =
														(
														((long) (BgL_q0z00_4422) <<
															(int) (2L)) |
														((long) (BgL_q1z00_4429) >> (int) (4L)));
													{	/* Unsafe/base64.scm 338 */
														long BgL_v1z00_4437;

														BgL_v1z00_4437 =
															(((long) (BgL_q1z00_4429) << (int) (4L)) & 240L);
														{	/* Unsafe/base64.scm 339 */

															{	/* Unsafe/base64.scm 340 */
																unsigned char BgL_arg1501z00_4438;

																BgL_arg1501z00_4438 = (BgL_v0z00_4436);
																STRING_SET(BgL_bufz00_4277, BgL_wz00_4344,
																	BgL_arg1501z00_4438);
															}
															{	/* Unsafe/base64.scm 341 */
																unsigned char BgL_auxz00_5138;
																long BgL_tmpz00_5136;

																BgL_auxz00_5138 = (BgL_v1z00_4437);
																BgL_tmpz00_5136 = (BgL_wz00_4344 + 1L);
																STRING_SET(BgL_bufz00_4277, BgL_tmpz00_5136,
																	BgL_auxz00_5138);
															}
															{	/* Unsafe/base64.scm 342 */
																long BgL_tmpz00_5141;

																BgL_tmpz00_5141 = (BgL_wz00_4344 + 1L);
																return
																	bgl_display_substring(BgL_bufz00_4277, 0L,
																	BgL_tmpz00_5141, BgL_opz00_4276);
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/base64.scm 361 */
										return BFALSE;
									}
								break;
							case 3L:

								goto BgL_chunk2z00_4351;
								break;
							case 2L:

								if (BgL_eofzd2nozd2paddingz00_4281)
									{	/* Unsafe/base64.scm 357 */
									BgL_chunk3z00_4350:
										{	/* Unsafe/base64.scm 324 */
											int BgL_q0z00_4397;

											{	/* Unsafe/base64.scm 324 */
												int BgL_arg1530z00_4398;

												{	/* Unsafe/base64.scm 300 */
													int BgL_tmpz00_5145;

													BgL_tmpz00_5145 = (int) (0L);
													BgL_arg1530z00_4398 =
														RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
														BgL_tmpz00_5145);
												}
												{	/* Unsafe/base64.scm 324 */
													int BgL_res2318z00_4399;

													{	/* Unsafe/base64.scm 324 */
														char BgL_cz00_4400;

														BgL_cz00_4400 = (signed char) (BgL_arg1530z00_4398);
														{	/* Unsafe/base64.scm 195 */
															uint8_t BgL_arg1856z00_4401;

															{	/* Unsafe/base64.scm 195 */
																long BgL_tmpz00_5149;

																BgL_tmpz00_5149 = (long) (BgL_cz00_4400);
																BgL_arg1856z00_4401 =
																	BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																	BgL_tmpz00_5149);
															}
															{	/* Unsafe/base64.scm 195 */
																int8_t BgL_xz00_4402;

																BgL_xz00_4402 = (int8_t) (BgL_arg1856z00_4401);
																{	/* Unsafe/base64.scm 195 */
																	long BgL_arg2274z00_4403;

																	BgL_arg2274z00_4403 = (long) (BgL_xz00_4402);
																	BgL_res2318z00_4399 =
																		(int) ((long) (BgL_arg2274z00_4403));
													}}}}
													BgL_q0z00_4397 = BgL_res2318z00_4399;
											}}
											{	/* Unsafe/base64.scm 324 */
												int BgL_q1z00_4404;

												{	/* Unsafe/base64.scm 325 */
													int BgL_arg1529z00_4405;

													{	/* Unsafe/base64.scm 300 */
														int BgL_tmpz00_5156;

														BgL_tmpz00_5156 = (int) (1L);
														BgL_arg1529z00_4405 =
															RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
															BgL_tmpz00_5156);
													}
													{	/* Unsafe/base64.scm 325 */
														int BgL_res2319z00_4406;

														{	/* Unsafe/base64.scm 325 */
															char BgL_cz00_4407;

															BgL_cz00_4407 =
																(signed char) (BgL_arg1529z00_4405);
															{	/* Unsafe/base64.scm 195 */
																uint8_t BgL_arg1856z00_4408;

																{	/* Unsafe/base64.scm 195 */
																	long BgL_tmpz00_5160;

																	BgL_tmpz00_5160 = (long) (BgL_cz00_4407);
																	BgL_arg1856z00_4408 =
																		BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																		BgL_tmpz00_5160);
																}
																{	/* Unsafe/base64.scm 195 */
																	int8_t BgL_xz00_4409;

																	BgL_xz00_4409 =
																		(int8_t) (BgL_arg1856z00_4408);
																	{	/* Unsafe/base64.scm 195 */
																		long BgL_arg2274z00_4410;

																		BgL_arg2274z00_4410 =
																			(long) (BgL_xz00_4409);
																		BgL_res2319z00_4406 =
																			(int) ((long) (BgL_arg2274z00_4410));
														}}}}
														BgL_q1z00_4404 = BgL_res2319z00_4406;
												}}
												{	/* Unsafe/base64.scm 325 */
													int BgL_q2z00_4411;

													{	/* Unsafe/base64.scm 326 */
														int BgL_arg1528z00_4412;

														{	/* Unsafe/base64.scm 300 */
															int BgL_tmpz00_5167;

															BgL_tmpz00_5167 = (int) (2L);
															BgL_arg1528z00_4412 =
																RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
																BgL_tmpz00_5167);
														}
														{	/* Unsafe/base64.scm 326 */
															int BgL_res2320z00_4413;

															{	/* Unsafe/base64.scm 326 */
																char BgL_cz00_4414;

																BgL_cz00_4414 =
																	(signed char) (BgL_arg1528z00_4412);
																{	/* Unsafe/base64.scm 195 */
																	uint8_t BgL_arg1856z00_4415;

																	{	/* Unsafe/base64.scm 195 */
																		long BgL_tmpz00_5171;

																		BgL_tmpz00_5171 = (long) (BgL_cz00_4414);
																		BgL_arg1856z00_4415 =
																			BGL_U8VREF
																			(BGl_bytezd2tablezd2zz__base64z00,
																			BgL_tmpz00_5171);
																	}
																	{	/* Unsafe/base64.scm 195 */
																		int8_t BgL_xz00_4416;

																		BgL_xz00_4416 =
																			(int8_t) (BgL_arg1856z00_4415);
																		{	/* Unsafe/base64.scm 195 */
																			long BgL_arg2274z00_4417;

																			BgL_arg2274z00_4417 =
																				(long) (BgL_xz00_4416);
																			BgL_res2320z00_4413 =
																				(int) ((long) (BgL_arg2274z00_4417));
															}}}}
															BgL_q2z00_4411 = BgL_res2320z00_4413;
													}}
													{	/* Unsafe/base64.scm 326 */
														long BgL_v0z00_4418;

														BgL_v0z00_4418 =
															(
															((long) (BgL_q0z00_4397) <<
																(int) (2L)) |
															((long) (BgL_q1z00_4404) >> (int) (4L)));
														{	/* Unsafe/base64.scm 327 */
															long BgL_v1z00_4419;

															BgL_v1z00_4419 =
																(
																(((long) (BgL_q1z00_4404) <<
																		(int) (4L)) & 240L) |
																((long) (BgL_q2z00_4411) >> (int) (2L)));
															{	/* Unsafe/base64.scm 328 */
																long BgL_v2z00_4420;

																BgL_v2z00_4420 =
																	(
																	((long) (BgL_q2z00_4411) <<
																		(int) (6L)) & 192L);
																{	/* Unsafe/base64.scm 329 */

																	{	/* Unsafe/base64.scm 330 */
																		unsigned char BgL_arg1511z00_4421;

																		BgL_arg1511z00_4421 = (BgL_v0z00_4418);
																		STRING_SET(BgL_bufz00_4277, BgL_wz00_4344,
																			BgL_arg1511z00_4421);
																	}
																	{	/* Unsafe/base64.scm 331 */
																		unsigned char BgL_auxz00_5201;
																		long BgL_tmpz00_5199;

																		BgL_auxz00_5201 = (BgL_v1z00_4419);
																		BgL_tmpz00_5199 = (BgL_wz00_4344 + 1L);
																		STRING_SET(BgL_bufz00_4277, BgL_tmpz00_5199,
																			BgL_auxz00_5201);
																	}
																	{	/* Unsafe/base64.scm 332 */
																		unsigned char BgL_auxz00_5206;
																		long BgL_tmpz00_5204;

																		BgL_auxz00_5206 = (BgL_v2z00_4420);
																		BgL_tmpz00_5204 = (BgL_wz00_4344 + 2L);
																		STRING_SET(BgL_bufz00_4277, BgL_tmpz00_5204,
																			BgL_auxz00_5206);
																	}
																	{	/* Unsafe/base64.scm 333 */
																		long BgL_tmpz00_5209;

																		BgL_tmpz00_5209 = (BgL_wz00_4344 + 2L);
																		return
																			bgl_display_substring(BgL_bufz00_4277, 0L,
																			BgL_tmpz00_5209, BgL_opz00_4276);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Unsafe/base64.scm 357 */
										return BFALSE;
									}
								break;
							case 1L:

								goto BgL_chunk3z00_4350;
								break;
							case 0L:

								{	/* Unsafe/base64.scm 306 */
									int BgL_q0z00_4364;

									{	/* Unsafe/base64.scm 306 */
										int BgL_arg1557z00_4365;

										{	/* Unsafe/base64.scm 300 */
											int BgL_tmpz00_5212;

											BgL_tmpz00_5212 = (int) (0L);
											BgL_arg1557z00_4365 =
												RGC_BUFFER_BYTE_REF(BgL_iportz00_4275, BgL_tmpz00_5212);
										}
										{	/* Unsafe/base64.scm 306 */
											int BgL_res2321z00_4366;

											{	/* Unsafe/base64.scm 306 */
												char BgL_cz00_4367;

												BgL_cz00_4367 = (signed char) (BgL_arg1557z00_4365);
												{	/* Unsafe/base64.scm 195 */
													uint8_t BgL_arg1856z00_4368;

													{	/* Unsafe/base64.scm 195 */
														long BgL_tmpz00_5216;

														BgL_tmpz00_5216 = (long) (BgL_cz00_4367);
														BgL_arg1856z00_4368 =
															BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
															BgL_tmpz00_5216);
													}
													{	/* Unsafe/base64.scm 195 */
														int8_t BgL_xz00_4369;

														BgL_xz00_4369 = (int8_t) (BgL_arg1856z00_4368);
														{	/* Unsafe/base64.scm 195 */
															long BgL_arg2274z00_4370;

															BgL_arg2274z00_4370 = (long) (BgL_xz00_4369);
															BgL_res2321z00_4366 =
																(int) ((long) (BgL_arg2274z00_4370));
											}}}}
											BgL_q0z00_4364 = BgL_res2321z00_4366;
									}}
									{	/* Unsafe/base64.scm 306 */
										int BgL_q1z00_4371;

										{	/* Unsafe/base64.scm 307 */
											int BgL_arg1556z00_4372;

											{	/* Unsafe/base64.scm 300 */
												int BgL_tmpz00_5223;

												BgL_tmpz00_5223 = (int) (1L);
												BgL_arg1556z00_4372 =
													RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
													BgL_tmpz00_5223);
											}
											{	/* Unsafe/base64.scm 307 */
												int BgL_res2322z00_4373;

												{	/* Unsafe/base64.scm 307 */
													char BgL_cz00_4374;

													BgL_cz00_4374 = (signed char) (BgL_arg1556z00_4372);
													{	/* Unsafe/base64.scm 195 */
														uint8_t BgL_arg1856z00_4375;

														{	/* Unsafe/base64.scm 195 */
															long BgL_tmpz00_5227;

															BgL_tmpz00_5227 = (long) (BgL_cz00_4374);
															BgL_arg1856z00_4375 =
																BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																BgL_tmpz00_5227);
														}
														{	/* Unsafe/base64.scm 195 */
															int8_t BgL_xz00_4376;

															BgL_xz00_4376 = (int8_t) (BgL_arg1856z00_4375);
															{	/* Unsafe/base64.scm 195 */
																long BgL_arg2274z00_4377;

																BgL_arg2274z00_4377 = (long) (BgL_xz00_4376);
																BgL_res2322z00_4373 =
																	(int) ((long) (BgL_arg2274z00_4377));
												}}}}
												BgL_q1z00_4371 = BgL_res2322z00_4373;
										}}
										{	/* Unsafe/base64.scm 307 */
											int BgL_q2z00_4378;

											{	/* Unsafe/base64.scm 308 */
												int BgL_arg1555z00_4379;

												{	/* Unsafe/base64.scm 300 */
													int BgL_tmpz00_5234;

													BgL_tmpz00_5234 = (int) (2L);
													BgL_arg1555z00_4379 =
														RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
														BgL_tmpz00_5234);
												}
												{	/* Unsafe/base64.scm 308 */
													int BgL_res2323z00_4380;

													{	/* Unsafe/base64.scm 308 */
														char BgL_cz00_4381;

														BgL_cz00_4381 = (signed char) (BgL_arg1555z00_4379);
														{	/* Unsafe/base64.scm 195 */
															uint8_t BgL_arg1856z00_4382;

															{	/* Unsafe/base64.scm 195 */
																long BgL_tmpz00_5238;

																BgL_tmpz00_5238 = (long) (BgL_cz00_4381);
																BgL_arg1856z00_4382 =
																	BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																	BgL_tmpz00_5238);
															}
															{	/* Unsafe/base64.scm 195 */
																int8_t BgL_xz00_4383;

																BgL_xz00_4383 = (int8_t) (BgL_arg1856z00_4382);
																{	/* Unsafe/base64.scm 195 */
																	long BgL_arg2274z00_4384;

																	BgL_arg2274z00_4384 = (long) (BgL_xz00_4383);
																	BgL_res2323z00_4380 =
																		(int) ((long) (BgL_arg2274z00_4384));
													}}}}
													BgL_q2z00_4378 = BgL_res2323z00_4380;
											}}
											{	/* Unsafe/base64.scm 308 */
												int BgL_q3z00_4385;

												{	/* Unsafe/base64.scm 309 */
													int BgL_arg1554z00_4386;

													{	/* Unsafe/base64.scm 300 */
														int BgL_tmpz00_5245;

														BgL_tmpz00_5245 = (int) (3L);
														BgL_arg1554z00_4386 =
															RGC_BUFFER_BYTE_REF(BgL_iportz00_4275,
															BgL_tmpz00_5245);
													}
													{	/* Unsafe/base64.scm 309 */
														int BgL_res2324z00_4387;

														{	/* Unsafe/base64.scm 309 */
															char BgL_cz00_4388;

															BgL_cz00_4388 =
																(signed char) (BgL_arg1554z00_4386);
															{	/* Unsafe/base64.scm 195 */
																uint8_t BgL_arg1856z00_4389;

																{	/* Unsafe/base64.scm 195 */
																	long BgL_tmpz00_5249;

																	BgL_tmpz00_5249 = (long) (BgL_cz00_4388);
																	BgL_arg1856z00_4389 =
																		BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																		BgL_tmpz00_5249);
																}
																{	/* Unsafe/base64.scm 195 */
																	int8_t BgL_xz00_4390;

																	BgL_xz00_4390 =
																		(int8_t) (BgL_arg1856z00_4389);
																	{	/* Unsafe/base64.scm 195 */
																		long BgL_arg2274z00_4391;

																		BgL_arg2274z00_4391 =
																			(long) (BgL_xz00_4390);
																		BgL_res2324z00_4387 =
																			(int) ((long) (BgL_arg2274z00_4391));
														}}}}
														BgL_q3z00_4385 = BgL_res2324z00_4387;
												}}
												{	/* Unsafe/base64.scm 309 */
													long BgL_v0z00_4392;

													BgL_v0z00_4392 =
														(
														((long) (BgL_q0z00_4364) <<
															(int) (2L)) |
														((long) (BgL_q1z00_4371) >> (int) (4L)));
													{	/* Unsafe/base64.scm 310 */
														long BgL_v1z00_4393;

														BgL_v1z00_4393 =
															(
															(((long) (BgL_q1z00_4371) <<
																	(int) (4L)) & 240L) |
															((long) (BgL_q2z00_4378) >> (int) (2L)));
														{	/* Unsafe/base64.scm 311 */
															long BgL_v2z00_4394;

															BgL_v2z00_4394 =
																(
																(((long) (BgL_q2z00_4378) <<
																		(int) (6L)) & 192L) |
																(long) (BgL_q3z00_4385));
															{	/* Unsafe/base64.scm 312 */

																{	/* Unsafe/base64.scm 313 */
																	unsigned char BgL_arg1535z00_4395;

																	BgL_arg1535z00_4395 = (BgL_v0z00_4392);
																	STRING_SET(BgL_bufz00_4277, BgL_wz00_4344,
																		BgL_arg1535z00_4395);
																}
																{	/* Unsafe/base64.scm 314 */
																	unsigned char BgL_auxz00_5281;
																	long BgL_tmpz00_5279;

																	BgL_auxz00_5281 = (BgL_v1z00_4393);
																	BgL_tmpz00_5279 = (BgL_wz00_4344 + 1L);
																	STRING_SET(BgL_bufz00_4277, BgL_tmpz00_5279,
																		BgL_auxz00_5281);
																}
																{	/* Unsafe/base64.scm 315 */
																	unsigned char BgL_auxz00_5286;
																	long BgL_tmpz00_5284;

																	BgL_auxz00_5286 = (BgL_v2z00_4394);
																	BgL_tmpz00_5284 = (BgL_wz00_4344 + 2L);
																	STRING_SET(BgL_bufz00_4277, BgL_tmpz00_5284,
																		BgL_auxz00_5286);
																}
																{	/* Unsafe/base64.scm 316 */
																	long BgL_nwz00_4396;

																	BgL_nwz00_4396 = (BgL_wz00_4344 + 3L);
																	if ((BgL_nwz00_4396 == BgL_lenz00_4279))
																		{	/* Unsafe/base64.scm 317 */
																			bgl_display_string(BgL_bufz00_4277,
																				BgL_opz00_4276);
																			BgL_wz00_4344 = 0L;
																		}
																	else
																		{	/* Unsafe/base64.scm 317 */
																			BgL_wz00_4344 = BgL_nwz00_4396;
																		}
																}
															}
														}
													}
												}
											}
										}
									}
								}
								goto BgL_ignorez00_4348;
								break;
							default:
								return
									BGl_errorz00zz__errorz00(BGl_string2372z00zz__base64z00,
									BGl_string2373z00zz__base64z00, BINT(BgL_matchz00_4360));
							}
					}
				}
			}
		}

	}



/* _base64-encode */
	obj_t BGl__base64zd2encodezd2zz__base64z00(obj_t BgL_env1144z00_7,
		obj_t BgL_opt1143z00_6)
	{
		{	/* Unsafe/base64.scm 71 */
			{	/* Unsafe/base64.scm 71 */
				obj_t BgL_g1145z00_2116;

				BgL_g1145z00_2116 = VECTOR_REF(BgL_opt1143z00_6, 0L);
				switch (VECTOR_LENGTH(BgL_opt1143z00_6))
					{
					case 1L:

						{	/* Unsafe/base64.scm 71 */

							{	/* Unsafe/base64.scm 71 */
								obj_t BgL_auxz00_5297;

								if (STRINGP(BgL_g1145z00_2116))
									{	/* Unsafe/base64.scm 71 */
										BgL_auxz00_5297 = BgL_g1145z00_2116;
									}
								else
									{
										obj_t BgL_auxz00_5300;

										BgL_auxz00_5300 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2374z00zz__base64z00, BINT(2752L),
											BGl_string2375z00zz__base64z00,
											BGl_string2376z00zz__base64z00, BgL_g1145z00_2116);
										FAILURE(BgL_auxz00_5300, BFALSE, BFALSE);
									}
								return
									BGl_base64zd2encodezd2zz__base64z00(BgL_auxz00_5297,
									BINT(76L));
							}
						}
						break;
					case 2L:

						{	/* Unsafe/base64.scm 71 */
							obj_t BgL_paddingz00_2120;

							BgL_paddingz00_2120 = VECTOR_REF(BgL_opt1143z00_6, 1L);
							{	/* Unsafe/base64.scm 71 */

								{	/* Unsafe/base64.scm 71 */
									obj_t BgL_auxz00_5307;

									if (STRINGP(BgL_g1145z00_2116))
										{	/* Unsafe/base64.scm 71 */
											BgL_auxz00_5307 = BgL_g1145z00_2116;
										}
									else
										{
											obj_t BgL_auxz00_5310;

											BgL_auxz00_5310 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2374z00zz__base64z00, BINT(2752L),
												BGl_string2375z00zz__base64z00,
												BGl_string2376z00zz__base64z00, BgL_g1145z00_2116);
											FAILURE(BgL_auxz00_5310, BFALSE, BFALSE);
										}
									return
										BGl_base64zd2encodezd2zz__base64z00(BgL_auxz00_5307,
										BgL_paddingz00_2120);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* base64-encode */
	BGL_EXPORTED_DEF obj_t BGl_base64zd2encodezd2zz__base64z00(obj_t BgL_sz00_4,
		obj_t BgL_paddingz00_5)
	{
		{	/* Unsafe/base64.scm 71 */
			{	/* Unsafe/base64.scm 74 */
				long BgL_lenz00_2124;

				BgL_lenz00_2124 = STRING_LENGTH(BgL_sz00_4);
				{	/* Unsafe/base64.scm 75 */
					long BgL_lenzd23zd2_2125;

					BgL_lenzd23zd2_2125 = (BgL_lenz00_2124 - 3L);
					{	/* Unsafe/base64.scm 76 */
						long BgL_rlenz00_2126;

						BgL_rlenz00_2126 = (4L * ((2L + BgL_lenz00_2124) / 3L));
						{	/* Unsafe/base64.scm 77 */
							long BgL_padz00_2127;

							{	/* Unsafe/base64.scm 78 */
								bool_t BgL_test2530z00_5322;

								if (INTEGERP(BgL_paddingz00_5))
									{	/* Unsafe/base64.scm 78 */
										BgL_test2530z00_5322 = ((long) CINT(BgL_paddingz00_5) > 0L);
									}
								else
									{	/* Unsafe/base64.scm 78 */
										BgL_test2530z00_5322 = ((bool_t) 0);
									}
								if (BgL_test2530z00_5322)
									{	/* Unsafe/base64.scm 78 */
										BgL_padz00_2127 =
											((3L * (long) CINT(BgL_paddingz00_5)) / 4L);
									}
								else
									{	/* Unsafe/base64.scm 78 */
										BgL_padz00_2127 = -1L;
									}
							}
							{	/* Unsafe/base64.scm 78 */
								long BgL_flenz00_2128;

								if ((BgL_padz00_2127 > 0L))
									{	/* Unsafe/base64.scm 81 */
										BgL_flenz00_2128 =
											(BgL_rlenz00_2126 +
											(BgL_rlenz00_2126 / (long) CINT(BgL_paddingz00_5)));
									}
								else
									{	/* Unsafe/base64.scm 81 */
										BgL_flenz00_2128 = BgL_rlenz00_2126;
									}
								{	/* Unsafe/base64.scm 81 */
									obj_t BgL_resz00_2129;

									BgL_resz00_2129 =
										make_string(BgL_flenz00_2128, ((unsigned char) 10));
									{	/* Unsafe/base64.scm 84 */

										{
											long BgL_xz00_2131;
											long BgL_yz00_2132;

											BgL_xz00_2131 = 0L;
											BgL_yz00_2132 = 0L;
										BgL_zc3z04anonymousza31754ze3z87_2133:
											if ((BgL_xz00_2131 <= BgL_lenzd23zd2_2125))
												{	/* Unsafe/base64.scm 88 */
													long BgL_i0z00_2135;

													BgL_i0z00_2135 =
														(STRING_REF(BgL_sz00_4, BgL_xz00_2131));
													{	/* Unsafe/base64.scm 88 */
														long BgL_i1z00_2136;

														BgL_i1z00_2136 =
															(STRING_REF(BgL_sz00_4, (1L + BgL_xz00_2131)));
														{	/* Unsafe/base64.scm 89 */
															long BgL_i2z00_2137;

															BgL_i2z00_2137 =
																(STRING_REF(BgL_sz00_4, (2L + BgL_xz00_2131)));
															{	/* Unsafe/base64.scm 90 */
																long BgL_o0z00_2138;

																BgL_o0z00_2138 =
																	((BgL_i0z00_2135 & 252L) >> (int) (2L));
																{	/* Unsafe/base64.scm 91 */
																	long BgL_o1z00_2139;

																	BgL_o1z00_2139 =
																		(
																		((BgL_i0z00_2135 & 3L) <<
																			(int) (4L)) |
																		((BgL_i1z00_2136 & 240L) >> (int) (4L)));
																	{	/* Unsafe/base64.scm 92 */
																		long BgL_o2z00_2140;

																		BgL_o2z00_2140 =
																			(
																			((BgL_i1z00_2136 & 15L) <<
																				(int) (2L)) |
																			((BgL_i2z00_2137 & 192L) >> (int) (6L)));
																		{	/* Unsafe/base64.scm 94 */
																			long BgL_o3z00_2141;

																			BgL_o3z00_2141 = (BgL_i2z00_2137 & 63L);
																			{	/* Unsafe/base64.scm 96 */

																				{	/* Unsafe/base64.scm 97 */
																					unsigned char BgL_tmpz00_5364;

																					BgL_tmpz00_5364 =
																						STRING_REF
																						(BGl_string2377z00zz__base64z00,
																						BgL_o0z00_2138);
																					STRING_SET(BgL_resz00_2129,
																						BgL_yz00_2132, BgL_tmpz00_5364);
																				}
																				{	/* Unsafe/base64.scm 98 */
																					unsigned char BgL_auxz00_5369;
																					long BgL_tmpz00_5367;

																					BgL_auxz00_5369 =
																						STRING_REF
																						(BGl_string2377z00zz__base64z00,
																						BgL_o1z00_2139);
																					BgL_tmpz00_5367 =
																						(BgL_yz00_2132 + 1L);
																					STRING_SET(BgL_resz00_2129,
																						BgL_tmpz00_5367, BgL_auxz00_5369);
																				}
																				{	/* Unsafe/base64.scm 99 */
																					unsigned char BgL_auxz00_5374;
																					long BgL_tmpz00_5372;

																					BgL_auxz00_5374 =
																						STRING_REF
																						(BGl_string2377z00zz__base64z00,
																						BgL_o2z00_2140);
																					BgL_tmpz00_5372 =
																						(BgL_yz00_2132 + 2L);
																					STRING_SET(BgL_resz00_2129,
																						BgL_tmpz00_5372, BgL_auxz00_5374);
																				}
																				{	/* Unsafe/base64.scm 100 */
																					unsigned char BgL_auxz00_5379;
																					long BgL_tmpz00_5377;

																					BgL_auxz00_5379 =
																						STRING_REF
																						(BGl_string2377z00zz__base64z00,
																						BgL_o3z00_2141);
																					BgL_tmpz00_5377 =
																						(BgL_yz00_2132 + 3L);
																					STRING_SET(BgL_resz00_2129,
																						BgL_tmpz00_5377, BgL_auxz00_5379);
																				}
																				{	/* Unsafe/base64.scm 101 */
																					long BgL_nxz00_2149;

																					BgL_nxz00_2149 = (BgL_xz00_2131 + 3L);
																					{	/* Unsafe/base64.scm 102 */
																						bool_t BgL_test2534z00_5383;

																						if ((BgL_padz00_2127 > 0L))
																							{	/* Unsafe/base64.scm 102 */
																								long BgL_arg1768z00_2156;

																								{	/* Unsafe/base64.scm 102 */
																									long BgL_n1z00_3541;
																									long BgL_n2z00_3542;

																									BgL_n1z00_3541 =
																										BgL_nxz00_2149;
																									BgL_n2z00_3542 =
																										BgL_padz00_2127;
																									{	/* Unsafe/base64.scm 102 */
																										bool_t BgL_test2536z00_5386;

																										{	/* Unsafe/base64.scm 102 */
																											long BgL_arg2169z00_3544;

																											BgL_arg2169z00_3544 =
																												(((BgL_n1z00_3541) |
																													(BgL_n2z00_3542)) &
																												-2147483648);
																											BgL_test2536z00_5386 =
																												(BgL_arg2169z00_3544 ==
																												0L);
																										}
																										if (BgL_test2536z00_5386)
																											{	/* Unsafe/base64.scm 102 */
																												int32_t
																													BgL_arg2166z00_3545;
																												{	/* Unsafe/base64.scm 102 */
																													int32_t
																														BgL_arg2167z00_3546;
																													int32_t
																														BgL_arg2168z00_3547;
																													BgL_arg2167z00_3546 =
																														(int32_t)
																														(BgL_n1z00_3541);
																													BgL_arg2168z00_3547 =
																														(int32_t)
																														(BgL_n2z00_3542);
																													BgL_arg2166z00_3545 =
																														(BgL_arg2167z00_3546
																														%
																														BgL_arg2168z00_3547);
																												}
																												{	/* Unsafe/base64.scm 102 */
																													long
																														BgL_arg2270z00_3552;
																													BgL_arg2270z00_3552 =
																														(long)
																														(BgL_arg2166z00_3545);
																													BgL_arg1768z00_2156 =
																														(long)
																														(BgL_arg2270z00_3552);
																											}}
																										else
																											{	/* Unsafe/base64.scm 102 */
																												BgL_arg1768z00_2156 =
																													(BgL_n1z00_3541 %
																													BgL_n2z00_3542);
																											}
																									}
																								}
																								BgL_test2534z00_5383 =
																									(BgL_arg1768z00_2156 == 0L);
																							}
																						else
																							{	/* Unsafe/base64.scm 102 */
																								BgL_test2534z00_5383 =
																									((bool_t) 0);
																							}
																						if (BgL_test2534z00_5383)
																							{
																								long BgL_yz00_5397;
																								long BgL_xz00_5396;

																								BgL_xz00_5396 = BgL_nxz00_2149;
																								BgL_yz00_5397 =
																									(BgL_yz00_2132 + 5L);
																								BgL_yz00_2132 = BgL_yz00_5397;
																								BgL_xz00_2131 = BgL_xz00_5396;
																								goto
																									BgL_zc3z04anonymousza31754ze3z87_2133;
																							}
																						else
																							{
																								long BgL_yz00_5400;
																								long BgL_xz00_5399;

																								BgL_xz00_5399 = BgL_nxz00_2149;
																								BgL_yz00_5400 =
																									(BgL_yz00_2132 + 4L);
																								BgL_yz00_2132 = BgL_yz00_5400;
																								BgL_xz00_2131 = BgL_xz00_5399;
																								goto
																									BgL_zc3z04anonymousza31754ze3z87_2133;
																							}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											else
												{	/* Unsafe/base64.scm 105 */
													long BgL_aux1041z00_2172;

													BgL_aux1041z00_2172 =
														(BgL_lenz00_2124 - BgL_xz00_2131);
													switch (BgL_aux1041z00_2172)
														{
														case 2L:

															{	/* Unsafe/base64.scm 107 */
																long BgL_i0z00_2173;

																BgL_i0z00_2173 =
																	(STRING_REF(BgL_sz00_4, BgL_xz00_2131));
																{	/* Unsafe/base64.scm 107 */
																	long BgL_i1z00_2174;

																	BgL_i1z00_2174 =
																		(STRING_REF(BgL_sz00_4,
																			(BgL_xz00_2131 + 1L)));
																	{	/* Unsafe/base64.scm 108 */
																		long BgL_o0z00_2175;

																		BgL_o0z00_2175 =
																			((BgL_i0z00_2173 & 252L) >> (int) (2L));
																		{	/* Unsafe/base64.scm 109 */
																			long BgL_o1z00_2176;

																			BgL_o1z00_2176 =
																				(
																				((BgL_i0z00_2173 & 3L) <<
																					(int) (4L)) |
																				((BgL_i1z00_2174 & 240L) >>
																					(int) (4L)));
																			{	/* Unsafe/base64.scm 110 */
																				long BgL_o2z00_2177;

																				BgL_o2z00_2177 =
																					(
																					(BgL_i1z00_2174 & 15L) << (int) (2L));
																				{	/* Unsafe/base64.scm 112 */

																					{	/* Unsafe/base64.scm 113 */
																						unsigned char BgL_tmpz00_5421;

																						BgL_tmpz00_5421 =
																							STRING_REF
																							(BGl_string2377z00zz__base64z00,
																							BgL_o0z00_2175);
																						STRING_SET(BgL_resz00_2129,
																							BgL_yz00_2132, BgL_tmpz00_5421);
																					}
																					{	/* Unsafe/base64.scm 114 */
																						unsigned char BgL_auxz00_5426;
																						long BgL_tmpz00_5424;

																						BgL_auxz00_5426 =
																							STRING_REF
																							(BGl_string2377z00zz__base64z00,
																							BgL_o1z00_2176);
																						BgL_tmpz00_5424 =
																							(BgL_yz00_2132 + 1L);
																						STRING_SET(BgL_resz00_2129,
																							BgL_tmpz00_5424, BgL_auxz00_5426);
																					}
																					{	/* Unsafe/base64.scm 115 */
																						unsigned char BgL_auxz00_5431;
																						long BgL_tmpz00_5429;

																						BgL_auxz00_5431 =
																							STRING_REF
																							(BGl_string2377z00zz__base64z00,
																							BgL_o2z00_2177);
																						BgL_tmpz00_5429 =
																							(BgL_yz00_2132 + 2L);
																						STRING_SET(BgL_resz00_2129,
																							BgL_tmpz00_5429, BgL_auxz00_5431);
																					}
																					{	/* Unsafe/base64.scm 116 */
																						long BgL_tmpz00_5434;

																						BgL_tmpz00_5434 =
																							(BgL_yz00_2132 + 3L);
																						STRING_SET(BgL_resz00_2129,
																							BgL_tmpz00_5434,
																							((unsigned char) '='));
															}}}}}}} break;
														case 1L:

															{	/* Unsafe/base64.scm 118 */
																long BgL_iz00_2193;

																BgL_iz00_2193 =
																	(STRING_REF(BgL_sz00_4, BgL_xz00_2131));
																{	/* Unsafe/base64.scm 118 */
																	long BgL_o0z00_2194;

																	BgL_o0z00_2194 =
																		((BgL_iz00_2193 & 252L) >> (int) (2L));
																	{	/* Unsafe/base64.scm 119 */
																		long BgL_o1z00_2195;

																		BgL_o1z00_2195 =
																			((BgL_iz00_2193 & 3L) << (int) (4L));
																		{	/* Unsafe/base64.scm 120 */

																			{	/* Unsafe/base64.scm 121 */
																				unsigned char BgL_tmpz00_5445;

																				BgL_tmpz00_5445 =
																					STRING_REF
																					(BGl_string2377z00zz__base64z00,
																					BgL_o0z00_2194);
																				STRING_SET(BgL_resz00_2129,
																					BgL_yz00_2132, BgL_tmpz00_5445);
																			}
																			{	/* Unsafe/base64.scm 122 */
																				unsigned char BgL_auxz00_5450;
																				long BgL_tmpz00_5448;

																				BgL_auxz00_5450 =
																					STRING_REF
																					(BGl_string2377z00zz__base64z00,
																					BgL_o1z00_2195);
																				BgL_tmpz00_5448 = (BgL_yz00_2132 + 1L);
																				STRING_SET(BgL_resz00_2129,
																					BgL_tmpz00_5448, BgL_auxz00_5450);
																			}
																			{	/* Unsafe/base64.scm 123 */
																				long BgL_tmpz00_5453;

																				BgL_tmpz00_5453 = (BgL_yz00_2132 + 2L);
																				STRING_SET(BgL_resz00_2129,
																					BgL_tmpz00_5453,
																					((unsigned char) '='));
																			}
																			{	/* Unsafe/base64.scm 124 */
																				long BgL_tmpz00_5456;

																				BgL_tmpz00_5456 = (BgL_yz00_2132 + 3L);
																				STRING_SET(BgL_resz00_2129,
																					BgL_tmpz00_5456,
																					((unsigned char) '='));
															}}}}} break;
														default:
															BUNSPEC;
														}
												}
										}
										return BgL_resz00_2129;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* _base64-encode-port */
	obj_t BGl__base64zd2encodezd2portz00zz__base64z00(obj_t BgL_env1149z00_12,
		obj_t BgL_opt1148z00_11)
	{
		{	/* Unsafe/base64.scm 130 */
			{	/* Unsafe/base64.scm 130 */
				obj_t BgL_g1150z00_2213;
				obj_t BgL_g1151z00_2214;

				BgL_g1150z00_2213 = VECTOR_REF(BgL_opt1148z00_11, 0L);
				BgL_g1151z00_2214 = VECTOR_REF(BgL_opt1148z00_11, 1L);
				switch (VECTOR_LENGTH(BgL_opt1148z00_11))
					{
					case 2L:

						{	/* Unsafe/base64.scm 130 */

							{	/* Unsafe/base64.scm 130 */
								obj_t BgL_auxz00_5469;
								obj_t BgL_auxz00_5462;

								if (OUTPUT_PORTP(BgL_g1151z00_2214))
									{	/* Unsafe/base64.scm 130 */
										BgL_auxz00_5469 = BgL_g1151z00_2214;
									}
								else
									{
										obj_t BgL_auxz00_5472;

										BgL_auxz00_5472 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2374z00zz__base64z00, BINT(4911L),
											BGl_string2378z00zz__base64z00,
											BGl_string2380z00zz__base64z00, BgL_g1151z00_2214);
										FAILURE(BgL_auxz00_5472, BFALSE, BFALSE);
									}
								if (INPUT_PORTP(BgL_g1150z00_2213))
									{	/* Unsafe/base64.scm 130 */
										BgL_auxz00_5462 = BgL_g1150z00_2213;
									}
								else
									{
										obj_t BgL_auxz00_5465;

										BgL_auxz00_5465 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2374z00zz__base64z00, BINT(4911L),
											BGl_string2378z00zz__base64z00,
											BGl_string2379z00zz__base64z00, BgL_g1150z00_2213);
										FAILURE(BgL_auxz00_5465, BFALSE, BFALSE);
									}
								return
									BGl_base64zd2encodezd2portz00zz__base64z00(BgL_auxz00_5462,
									BgL_auxz00_5469, BINT(76L));
							}
						}
						break;
					case 3L:

						{	/* Unsafe/base64.scm 130 */
							obj_t BgL_paddingz00_2218;

							BgL_paddingz00_2218 = VECTOR_REF(BgL_opt1148z00_11, 2L);
							{	/* Unsafe/base64.scm 130 */

								{	/* Unsafe/base64.scm 130 */
									obj_t BgL_auxz00_5486;
									obj_t BgL_auxz00_5479;

									if (OUTPUT_PORTP(BgL_g1151z00_2214))
										{	/* Unsafe/base64.scm 130 */
											BgL_auxz00_5486 = BgL_g1151z00_2214;
										}
									else
										{
											obj_t BgL_auxz00_5489;

											BgL_auxz00_5489 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2374z00zz__base64z00, BINT(4911L),
												BGl_string2378z00zz__base64z00,
												BGl_string2380z00zz__base64z00, BgL_g1151z00_2214);
											FAILURE(BgL_auxz00_5489, BFALSE, BFALSE);
										}
									if (INPUT_PORTP(BgL_g1150z00_2213))
										{	/* Unsafe/base64.scm 130 */
											BgL_auxz00_5479 = BgL_g1150z00_2213;
										}
									else
										{
											obj_t BgL_auxz00_5482;

											BgL_auxz00_5482 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2374z00zz__base64z00, BINT(4911L),
												BGl_string2378z00zz__base64z00,
												BGl_string2379z00zz__base64z00, BgL_g1150z00_2213);
											FAILURE(BgL_auxz00_5482, BFALSE, BFALSE);
										}
									return
										BGl_base64zd2encodezd2portz00zz__base64z00(BgL_auxz00_5479,
										BgL_auxz00_5486, BgL_paddingz00_2218);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* base64-encode-port */
	BGL_EXPORTED_DEF obj_t BGl_base64zd2encodezd2portz00zz__base64z00(obj_t
		BgL_ipz00_8, obj_t BgL_opz00_9, obj_t BgL_paddingz00_10)
	{
		{	/* Unsafe/base64.scm 130 */
			{	/* Unsafe/base64.scm 131 */
				long BgL_padz00_2219;

				BgL_padz00_2219 = ((long) CINT(BgL_paddingz00_10) - 4L);
				{
					long BgL_iz00_2221;

					BgL_iz00_2221 = 0L;
				BgL_zc3z04anonymousza31817ze3z87_2222:
					{	/* Unsafe/base64.scm 133 */
						obj_t BgL_i0z00_2223;

						BgL_i0z00_2223 =
							BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_8);
						if (EOF_OBJECTP(BgL_i0z00_2223))
							{	/* Unsafe/base64.scm 134 */
								return BFALSE;
							}
						else
							{	/* Unsafe/base64.scm 135 */
								obj_t BgL_i1z00_2225;

								BgL_i1z00_2225 =
									BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_8);
								if (EOF_OBJECTP(BgL_i1z00_2225))
									{	/* Unsafe/base64.scm 137 */
										long BgL_o0z00_2227;

										BgL_o0z00_2227 =
											(((long) CINT(BgL_i0z00_2223) & 252L) >> (int) (2L));
										{	/* Unsafe/base64.scm 137 */
											long BgL_o1z00_2228;

											BgL_o1z00_2228 =
												(((long) CINT(BgL_i0z00_2223) & 3L) << (int) (4L));
											{	/* Unsafe/base64.scm 138 */

												{	/* Unsafe/base64.scm 139 */
													unsigned char BgL_tmpz00_5512;

													BgL_tmpz00_5512 =
														STRING_REF(BGl_string2377z00zz__base64z00,
														BgL_o0z00_2227);
													bgl_display_char(BgL_tmpz00_5512, BgL_opz00_9);
												}
												{	/* Unsafe/base64.scm 140 */
													unsigned char BgL_tmpz00_5515;

													BgL_tmpz00_5515 =
														STRING_REF(BGl_string2377z00zz__base64z00,
														BgL_o1z00_2228);
													bgl_display_char(BgL_tmpz00_5515, BgL_opz00_9);
												}
												bgl_display_char(((unsigned char) '='), BgL_opz00_9);
												return
													bgl_display_char(((unsigned char) '='), BgL_opz00_9);
									}}}
								else
									{	/* Unsafe/base64.scm 143 */
										obj_t BgL_i2z00_2233;

										BgL_i2z00_2233 =
											BGl_readzd2bytezd2zz__r4_input_6_10_2z00(BgL_ipz00_8);
										if (EOF_OBJECTP(BgL_i2z00_2233))
											{	/* Unsafe/base64.scm 145 */
												long BgL_o0z00_2235;

												BgL_o0z00_2235 =
													(((long) CINT(BgL_i0z00_2223) & 252L) >> (int) (2L));
												{	/* Unsafe/base64.scm 145 */
													long BgL_o1z00_2236;

													BgL_o1z00_2236 =
														(
														(((long) CINT(BgL_i0z00_2223) & 3L) <<
															(int) (4L)) |
														(((long) CINT(BgL_i1z00_2225) & 240L) >>
															(int) (4L)));
													{	/* Unsafe/base64.scm 146 */
														long BgL_o2z00_2237;

														BgL_o2z00_2237 =
															(
															((long) CINT(BgL_i1z00_2225) & 15L) <<
															(int) (2L));
														{	/* Unsafe/base64.scm 148 */

															{	/* Unsafe/base64.scm 149 */
																unsigned char BgL_tmpz00_5540;

																BgL_tmpz00_5540 =
																	STRING_REF(BGl_string2377z00zz__base64z00,
																	BgL_o0z00_2235);
																bgl_display_char(BgL_tmpz00_5540, BgL_opz00_9);
															}
															{	/* Unsafe/base64.scm 150 */
																unsigned char BgL_tmpz00_5543;

																BgL_tmpz00_5543 =
																	STRING_REF(BGl_string2377z00zz__base64z00,
																	BgL_o1z00_2236);
																bgl_display_char(BgL_tmpz00_5543, BgL_opz00_9);
															}
															{	/* Unsafe/base64.scm 151 */
																unsigned char BgL_tmpz00_5546;

																BgL_tmpz00_5546 =
																	STRING_REF(BGl_string2377z00zz__base64z00,
																	BgL_o2z00_2237);
																bgl_display_char(BgL_tmpz00_5546, BgL_opz00_9);
															}
															return
																bgl_display_char(((unsigned char) '='),
																BgL_opz00_9);
											}}}}
										else
											{	/* Unsafe/base64.scm 153 */
												long BgL_o0z00_2247;

												BgL_o0z00_2247 =
													(((long) CINT(BgL_i0z00_2223) & 252L) >> (int) (2L));
												{	/* Unsafe/base64.scm 153 */
													long BgL_o1z00_2248;

													BgL_o1z00_2248 =
														(
														(((long) CINT(BgL_i0z00_2223) & 3L) <<
															(int) (4L)) |
														(((long) CINT(BgL_i1z00_2225) & 240L) >>
															(int) (4L)));
													{	/* Unsafe/base64.scm 154 */
														long BgL_o2z00_2249;

														BgL_o2z00_2249 =
															(
															(((long) CINT(BgL_i1z00_2225) & 15L) <<
																(int) (2L)) |
															(((long) CINT(BgL_i2z00_2233) & 192L) >>
																(int) (6L)));
														{	/* Unsafe/base64.scm 156 */
															long BgL_o3z00_2250;

															BgL_o3z00_2250 =
																((long) CINT(BgL_i2z00_2233) & 63L);
															{	/* Unsafe/base64.scm 158 */

																{	/* Unsafe/base64.scm 159 */
																	unsigned char BgL_tmpz00_5574;

																	BgL_tmpz00_5574 =
																		STRING_REF(BGl_string2377z00zz__base64z00,
																		BgL_o0z00_2247);
																	bgl_display_char(BgL_tmpz00_5574,
																		BgL_opz00_9);
																}
																{	/* Unsafe/base64.scm 160 */
																	unsigned char BgL_tmpz00_5577;

																	BgL_tmpz00_5577 =
																		STRING_REF(BGl_string2377z00zz__base64z00,
																		BgL_o1z00_2248);
																	bgl_display_char(BgL_tmpz00_5577,
																		BgL_opz00_9);
																}
																{	/* Unsafe/base64.scm 161 */
																	unsigned char BgL_tmpz00_5580;

																	BgL_tmpz00_5580 =
																		STRING_REF(BGl_string2377z00zz__base64z00,
																		BgL_o2z00_2249);
																	bgl_display_char(BgL_tmpz00_5580,
																		BgL_opz00_9);
																}
																{	/* Unsafe/base64.scm 162 */
																	unsigned char BgL_tmpz00_5583;

																	BgL_tmpz00_5583 =
																		STRING_REF(BGl_string2377z00zz__base64z00,
																		BgL_o3z00_2250);
																	bgl_display_char(BgL_tmpz00_5583,
																		BgL_opz00_9);
																}
																{	/* Unsafe/base64.scm 163 */
																	bool_t BgL_test2544z00_5586;

																	if ((BgL_iz00_2221 >= BgL_padz00_2219))
																		{	/* Unsafe/base64.scm 163 */
																			BgL_test2544z00_5586 =
																				(BgL_padz00_2219 > 0L);
																		}
																	else
																		{	/* Unsafe/base64.scm 163 */
																			BgL_test2544z00_5586 = ((bool_t) 0);
																		}
																	if (BgL_test2544z00_5586)
																		{	/* Unsafe/base64.scm 163 */
																			bgl_display_char(((unsigned char) 10),
																				BgL_opz00_9);
																			{
																				long BgL_iz00_5591;

																				BgL_iz00_5591 = 0L;
																				BgL_iz00_2221 = BgL_iz00_5591;
																				goto
																					BgL_zc3z04anonymousza31817ze3z87_2222;
																			}
																		}
																	else
																		{
																			long BgL_iz00_5592;

																			BgL_iz00_5592 = (BgL_iz00_2221 + 4L);
																			BgL_iz00_2221 = BgL_iz00_5592;
																			goto
																				BgL_zc3z04anonymousza31817ze3z87_2222;
																		}
																}
															}
														}
													}
												}
											}
									}
							}
					}
				}
			}
		}

	}



/* actual-string-length */
	long BGl_actualzd2stringzd2lengthz00zz__base64z00(obj_t BgL_sz00_15)
	{
		{	/* Unsafe/base64.scm 206 */
			{
				long BgL_iz00_2273;

				BgL_iz00_2273 = (STRING_LENGTH(BgL_sz00_15) - 1L);
			BgL_zc3z04anonymousza31858ze3z87_2274:
				if ((BgL_iz00_2273 == 0L))
					{	/* Unsafe/base64.scm 208 */
						return BgL_iz00_2273;
					}
				else
					{	/* Unsafe/base64.scm 210 */
						unsigned char BgL_cz00_2276;

						BgL_cz00_2276 = STRING_REF(BgL_sz00_15, BgL_iz00_2273);
						{	/* Unsafe/base64.scm 211 */
							bool_t BgL_test2547z00_5597;

							if ((BgL_cz00_2276 == ((unsigned char) 10)))
								{	/* Unsafe/base64.scm 211 */
									BgL_test2547z00_5597 = ((bool_t) 1);
								}
							else
								{	/* Unsafe/base64.scm 211 */
									BgL_test2547z00_5597 =
										(BgL_cz00_2276 == ((unsigned char) 13));
								}
							if (BgL_test2547z00_5597)
								{
									long BgL_iz00_5601;

									BgL_iz00_5601 = (BgL_iz00_2273 - 1L);
									BgL_iz00_2273 = BgL_iz00_5601;
									goto BgL_zc3z04anonymousza31858ze3z87_2274;
								}
							else
								{	/* Unsafe/base64.scm 211 */
									return (BgL_iz00_2273 + 1L);
								}
						}
					}
			}
		}

	}



/* _base64-decode */
	obj_t BGl__base64zd2decodezd2zz__base64z00(obj_t BgL_env1155z00_19,
		obj_t BgL_opt1154z00_18)
	{
		{	/* Unsafe/base64.scm 218 */
			{	/* Unsafe/base64.scm 218 */
				obj_t BgL_g1156z00_2283;

				BgL_g1156z00_2283 = VECTOR_REF(BgL_opt1154z00_18, 0L);
				switch (VECTOR_LENGTH(BgL_opt1154z00_18))
					{
					case 1L:

						{	/* Unsafe/base64.scm 218 */

							{	/* Unsafe/base64.scm 218 */
								obj_t BgL_auxz00_5607;

								if (STRINGP(BgL_g1156z00_2283))
									{	/* Unsafe/base64.scm 218 */
										BgL_auxz00_5607 = BgL_g1156z00_2283;
									}
								else
									{
										obj_t BgL_auxz00_5610;

										BgL_auxz00_5610 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2374z00zz__base64z00, BINT(8420L),
											BGl_string2381z00zz__base64z00,
											BGl_string2376z00zz__base64z00, BgL_g1156z00_2283);
										FAILURE(BgL_auxz00_5610, BFALSE, BFALSE);
									}
								return
									BGl_base64zd2decodezd2zz__base64z00(BgL_auxz00_5607, BFALSE);
							}
						}
						break;
					case 2L:

						{	/* Unsafe/base64.scm 218 */
							obj_t BgL_eofzd2nozd2paddingz00_2287;

							BgL_eofzd2nozd2paddingz00_2287 =
								VECTOR_REF(BgL_opt1154z00_18, 1L);
							{	/* Unsafe/base64.scm 218 */

								{	/* Unsafe/base64.scm 218 */
									obj_t BgL_auxz00_5616;

									if (STRINGP(BgL_g1156z00_2283))
										{	/* Unsafe/base64.scm 218 */
											BgL_auxz00_5616 = BgL_g1156z00_2283;
										}
									else
										{
											obj_t BgL_auxz00_5619;

											BgL_auxz00_5619 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2374z00zz__base64z00, BINT(8420L),
												BGl_string2381z00zz__base64z00,
												BGl_string2376z00zz__base64z00, BgL_g1156z00_2283);
											FAILURE(BgL_auxz00_5619, BFALSE, BFALSE);
										}
									return
										BGl_base64zd2decodezd2zz__base64z00(BgL_auxz00_5616,
										BgL_eofzd2nozd2paddingz00_2287);
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* base64-decode */
	BGL_EXPORTED_DEF obj_t BGl_base64zd2decodezd2zz__base64z00(obj_t BgL_sz00_16,
		obj_t BgL_eofzd2nozd2paddingz00_17)
	{
		{	/* Unsafe/base64.scm 218 */
			{	/* Unsafe/base64.scm 219 */
				long BgL_lenz00_2288;

				BgL_lenz00_2288 =
					BGl_actualzd2stringzd2lengthz00zz__base64z00(BgL_sz00_16);
				{	/* Unsafe/base64.scm 219 */
					long BgL_nlenz00_2289;

					if (CBOOL(BgL_eofzd2nozd2paddingz00_17))
						{	/* Unsafe/base64.scm 220 */
							BgL_nlenz00_2289 = ((1L + (BgL_lenz00_2288 / 4L)) * 3L);
						}
					else
						{	/* Unsafe/base64.scm 220 */
							BgL_nlenz00_2289 = ((BgL_lenz00_2288 / 4L) * 3L);
						}
					{	/* Unsafe/base64.scm 220 */
						obj_t BgL_resz00_2290;

						{	/* Ieee/string.scm 172 */

							BgL_resz00_2290 =
								make_string(BgL_nlenz00_2289, ((unsigned char) ' '));
						}
						{	/* Unsafe/base64.scm 223 */

							{
								long BgL_xz00_2292;
								long BgL_yz00_2293;

								BgL_xz00_2292 = 0L;
								BgL_yz00_2293 = 0L;
							BgL_zc3z04anonymousza31864ze3z87_2294:
								if ((BgL_xz00_2292 < BgL_lenz00_2288))
									{	/* Unsafe/base64.scm 227 */
										unsigned char BgL_cz00_2296;

										BgL_cz00_2296 =
											STRING_REF(BgL_sz00_16, (BgL_xz00_2292 + 0L));
										{	/* Unsafe/base64.scm 227 */
											int BgL_q0z00_2297;

											{	/* Unsafe/base64.scm 228 */
												unsigned char BgL_cz00_3748;

												BgL_cz00_3748 = (char) (BgL_cz00_2296);
												{	/* Unsafe/base64.scm 201 */
													long BgL_arg1857z00_3749;

													BgL_arg1857z00_3749 =
														((unsigned char) (BgL_cz00_3748));
													{	/* Unsafe/base64.scm 201 */
														int BgL_res2328z00_3756;

														{	/* Unsafe/base64.scm 201 */
															char BgL_cz00_3751;

															BgL_cz00_3751 =
																(signed char) (BgL_arg1857z00_3749);
															{	/* Unsafe/base64.scm 195 */
																uint8_t BgL_arg1856z00_3752;

																{	/* Unsafe/base64.scm 195 */
																	long BgL_tmpz00_5643;

																	BgL_tmpz00_5643 = (long) (BgL_cz00_3751);
																	BgL_arg1856z00_3752 =
																		BGL_U8VREF(BGl_bytezd2tablezd2zz__base64z00,
																		BgL_tmpz00_5643);
																}
																{	/* Unsafe/base64.scm 195 */
																	int8_t BgL_xz00_3753;

																	BgL_xz00_3753 =
																		(int8_t) (BgL_arg1856z00_3752);
																	{	/* Unsafe/base64.scm 195 */
																		long BgL_arg2274z00_3754;

																		BgL_arg2274z00_3754 =
																			(long) (BgL_xz00_3753);
																		BgL_res2328z00_3756 =
																			(int) ((long) (BgL_arg2274z00_3754));
														}}}}
														BgL_q0z00_2297 = BgL_res2328z00_3756;
											}}}
											{	/* Unsafe/base64.scm 228 */

												{	/* Unsafe/base64.scm 229 */
													bool_t BgL_test2553z00_5650;

													if (((long) (BgL_q0z00_2297) == 0L))
														{	/* Unsafe/base64.scm 230 */
															bool_t BgL__ortest_1045z00_2385;

															BgL__ortest_1045z00_2385 =
																(BgL_cz00_2296 == ((unsigned char) 10));
															if (BgL__ortest_1045z00_2385)
																{	/* Unsafe/base64.scm 230 */
																	BgL_test2553z00_5650 =
																		BgL__ortest_1045z00_2385;
																}
															else
																{	/* Unsafe/base64.scm 230 */
																	BgL_test2553z00_5650 =
																		(BgL_cz00_2296 == ((unsigned char) 13));
														}}
													else
														{	/* Unsafe/base64.scm 229 */
															BgL_test2553z00_5650 = ((bool_t) 0);
														}
													if (BgL_test2553z00_5650)
														{
															long BgL_xz00_5657;

															BgL_xz00_5657 = (BgL_xz00_2292 + 1L);
															BgL_xz00_2292 = BgL_xz00_5657;
															goto BgL_zc3z04anonymousza31864ze3z87_2294;
														}
													else
														{	/* Unsafe/base64.scm 229 */
															if ((BgL_xz00_2292 <= (BgL_lenz00_2288 - 4L)))
																{	/* Unsafe/base64.scm 234 */
																	int BgL_q1z00_2304;

																	{	/* Unsafe/base64.scm 234 */
																		unsigned char BgL_arg1892z00_2328;

																		BgL_arg1892z00_2328 =
																			STRING_REF(BgL_sz00_16,
																			(BgL_xz00_2292 + 1L));
																		{	/* Unsafe/base64.scm 234 */
																			unsigned char BgL_cz00_3769;

																			BgL_cz00_3769 =
																				(char) (BgL_arg1892z00_2328);
																			{	/* Unsafe/base64.scm 201 */
																				long BgL_arg1857z00_3770;

																				BgL_arg1857z00_3770 =
																					((unsigned char) (BgL_cz00_3769));
																				{	/* Unsafe/base64.scm 201 */
																					int BgL_res2329z00_3777;

																					{	/* Unsafe/base64.scm 201 */
																						char BgL_cz00_3772;

																						BgL_cz00_3772 =
																							(signed
																							char) (BgL_arg1857z00_3770);
																						{	/* Unsafe/base64.scm 195 */
																							uint8_t BgL_arg1856z00_3773;

																							{	/* Unsafe/base64.scm 195 */
																								long BgL_tmpz00_5668;

																								BgL_tmpz00_5668 =
																									(long) (BgL_cz00_3772);
																								BgL_arg1856z00_3773 =
																									BGL_U8VREF
																									(BGl_bytezd2tablezd2zz__base64z00,
																									BgL_tmpz00_5668);
																							}
																							{	/* Unsafe/base64.scm 195 */
																								int8_t BgL_xz00_3774;

																								BgL_xz00_3774 =
																									(int8_t)
																									(BgL_arg1856z00_3773);
																								{	/* Unsafe/base64.scm 195 */
																									long BgL_arg2274z00_3775;

																									BgL_arg2274z00_3775 =
																										(long) (BgL_xz00_3774);
																									BgL_res2329z00_3777 =
																										(int) (
																										(long)
																										(BgL_arg2274z00_3775));
																					}}}}
																					BgL_q1z00_2304 = BgL_res2329z00_3777;
																	}}}}
																	{	/* Unsafe/base64.scm 234 */
																		int BgL_q2z00_2305;

																		{	/* Unsafe/base64.scm 235 */
																			unsigned char BgL_arg1890z00_2326;

																			BgL_arg1890z00_2326 =
																				STRING_REF(BgL_sz00_16,
																				(BgL_xz00_2292 + 2L));
																			{	/* Unsafe/base64.scm 235 */
																				unsigned char BgL_cz00_3781;

																				BgL_cz00_3781 =
																					(char) (BgL_arg1890z00_2326);
																				{	/* Unsafe/base64.scm 201 */
																					long BgL_arg1857z00_3782;

																					BgL_arg1857z00_3782 =
																						((unsigned char) (BgL_cz00_3781));
																					{	/* Unsafe/base64.scm 201 */
																						int BgL_res2330z00_3789;

																						{	/* Unsafe/base64.scm 201 */
																							char BgL_cz00_3784;

																							BgL_cz00_3784 =
																								(signed
																								char) (BgL_arg1857z00_3782);
																							{	/* Unsafe/base64.scm 195 */
																								uint8_t BgL_arg1856z00_3785;

																								{	/* Unsafe/base64.scm 195 */
																									long BgL_tmpz00_5681;

																									BgL_tmpz00_5681 =
																										(long) (BgL_cz00_3784);
																									BgL_arg1856z00_3785 =
																										BGL_U8VREF
																										(BGl_bytezd2tablezd2zz__base64z00,
																										BgL_tmpz00_5681);
																								}
																								{	/* Unsafe/base64.scm 195 */
																									int8_t BgL_xz00_3786;

																									BgL_xz00_3786 =
																										(int8_t)
																										(BgL_arg1856z00_3785);
																									{	/* Unsafe/base64.scm 195 */
																										long BgL_arg2274z00_3787;

																										BgL_arg2274z00_3787 =
																											(long) (BgL_xz00_3786);
																										BgL_res2330z00_3789 =
																											(int) (
																											(long)
																											(BgL_arg2274z00_3787));
																						}}}}
																						BgL_q2z00_2305 =
																							BgL_res2330z00_3789;
																		}}}}
																		{	/* Unsafe/base64.scm 235 */
																			int BgL_q3z00_2306;

																			{	/* Unsafe/base64.scm 236 */
																				unsigned char BgL_arg1888z00_2324;

																				BgL_arg1888z00_2324 =
																					STRING_REF(BgL_sz00_16,
																					(BgL_xz00_2292 + 3L));
																				{	/* Unsafe/base64.scm 236 */
																					unsigned char BgL_cz00_3793;

																					BgL_cz00_3793 =
																						(char) (BgL_arg1888z00_2324);
																					{	/* Unsafe/base64.scm 201 */
																						long BgL_arg1857z00_3794;

																						BgL_arg1857z00_3794 =
																							((unsigned char) (BgL_cz00_3793));
																						{	/* Unsafe/base64.scm 201 */
																							int BgL_res2331z00_3801;

																							{	/* Unsafe/base64.scm 201 */
																								char BgL_cz00_3796;

																								BgL_cz00_3796 =
																									(signed
																									char) (BgL_arg1857z00_3794);
																								{	/* Unsafe/base64.scm 195 */
																									uint8_t BgL_arg1856z00_3797;

																									{	/* Unsafe/base64.scm 195 */
																										long BgL_tmpz00_5694;

																										BgL_tmpz00_5694 =
																											(long) (BgL_cz00_3796);
																										BgL_arg1856z00_3797 =
																											BGL_U8VREF
																											(BGl_bytezd2tablezd2zz__base64z00,
																											BgL_tmpz00_5694);
																									}
																									{	/* Unsafe/base64.scm 195 */
																										int8_t BgL_xz00_3798;

																										BgL_xz00_3798 =
																											(int8_t)
																											(BgL_arg1856z00_3797);
																										{	/* Unsafe/base64.scm 195 */
																											long BgL_arg2274z00_3799;

																											BgL_arg2274z00_3799 =
																												(long) (BgL_xz00_3798);
																											BgL_res2331z00_3801 =
																												(int) (
																												(long)
																												(BgL_arg2274z00_3799));
																							}}}}
																							BgL_q3z00_2306 =
																								BgL_res2331z00_3801;
																			}}}}
																			{	/* Unsafe/base64.scm 236 */
																				long BgL_v0z00_2307;

																				BgL_v0z00_2307 =
																					(
																					((long) (BgL_q0z00_2297) <<
																						(int) (2L)) |
																					((long) (BgL_q1z00_2304) >>
																						(int) (4L)));
																				{	/* Unsafe/base64.scm 237 */
																					long BgL_v1z00_2308;

																					BgL_v1z00_2308 =
																						(
																						(((long) (BgL_q1z00_2304) <<
																								(int) (4L)) & 240L) |
																						((long) (BgL_q2z00_2305) >>
																							(int) (2L)));
																					{	/* Unsafe/base64.scm 239 */
																						long BgL_v2z00_2309;

																						BgL_v2z00_2309 =
																							(
																							(((long) (BgL_q2z00_2305) <<
																									(int) (6L)) & 192L) |
																							(long) (BgL_q3z00_2306));
																						{	/* Unsafe/base64.scm 241 */

																							{	/* Unsafe/base64.scm 243 */
																								unsigned char BgL_tmpz00_5722;

																								BgL_tmpz00_5722 =
																									(BgL_v0z00_2307);
																								STRING_SET(BgL_resz00_2290,
																									BgL_yz00_2293,
																									BgL_tmpz00_5722);
																							}
																							{	/* Unsafe/base64.scm 244 */
																								unsigned char BgL_auxz00_5727;
																								long BgL_tmpz00_5725;

																								BgL_auxz00_5727 =
																									(BgL_v1z00_2308);
																								BgL_tmpz00_5725 =
																									(BgL_yz00_2293 + 1L);
																								STRING_SET(BgL_resz00_2290,
																									BgL_tmpz00_5725,
																									BgL_auxz00_5727);
																							}
																							{	/* Unsafe/base64.scm 245 */
																								unsigned char BgL_auxz00_5732;
																								long BgL_tmpz00_5730;

																								BgL_auxz00_5732 =
																									(BgL_v2z00_2309);
																								BgL_tmpz00_5730 =
																									(BgL_yz00_2293 + 2L);
																								STRING_SET(BgL_resz00_2290,
																									BgL_tmpz00_5730,
																									BgL_auxz00_5732);
																							}
																							{
																								long BgL_yz00_5737;
																								long BgL_xz00_5735;

																								BgL_xz00_5735 =
																									(BgL_xz00_2292 + 4L);
																								BgL_yz00_5737 =
																									(BgL_yz00_2293 + 3L);
																								BgL_yz00_2293 = BgL_yz00_5737;
																								BgL_xz00_2292 = BgL_xz00_5735;
																								goto
																									BgL_zc3z04anonymousza31864ze3z87_2294;
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															else
																{	/* Unsafe/base64.scm 247 */
																	bool_t BgL_test2557z00_5739;

																	if ((BgL_xz00_2292 <= (BgL_lenz00_2288 - 3L)))
																		{	/* Unsafe/base64.scm 247 */
																			BgL_test2557z00_5739 =
																				CBOOL(BgL_eofzd2nozd2paddingz00_17);
																		}
																	else
																		{	/* Unsafe/base64.scm 247 */
																			BgL_test2557z00_5739 = ((bool_t) 0);
																		}
																	if (BgL_test2557z00_5739)
																		{	/* Unsafe/base64.scm 248 */
																			unsigned char BgL_c1z00_2334;

																			BgL_c1z00_2334 =
																				STRING_REF(BgL_sz00_16,
																				(BgL_xz00_2292 + 1L));
																			{	/* Unsafe/base64.scm 248 */
																				unsigned char BgL_c2z00_2335;

																				BgL_c2z00_2335 =
																					STRING_REF(BgL_sz00_16,
																					(BgL_xz00_2292 + 2L));
																				{	/* Unsafe/base64.scm 249 */
																					int BgL_q1z00_2336;

																					{	/* Unsafe/base64.scm 250 */
																						unsigned char BgL_cz00_3840;

																						BgL_cz00_3840 =
																							(char) (BgL_c1z00_2334);
																						{	/* Unsafe/base64.scm 201 */
																							long BgL_arg1857z00_3841;

																							BgL_arg1857z00_3841 =
																								(
																								(unsigned
																									char) (BgL_cz00_3840));
																							{	/* Unsafe/base64.scm 201 */
																								int BgL_res2332z00_3848;

																								{	/* Unsafe/base64.scm 201 */
																									char BgL_cz00_3843;

																									BgL_cz00_3843 =
																										(signed
																										char) (BgL_arg1857z00_3841);
																									{	/* Unsafe/base64.scm 195 */
																										uint8_t BgL_arg1856z00_3844;

																										{	/* Unsafe/base64.scm 195 */
																											long BgL_tmpz00_5752;

																											BgL_tmpz00_5752 =
																												(long) (BgL_cz00_3843);
																											BgL_arg1856z00_3844 =
																												BGL_U8VREF
																												(BGl_bytezd2tablezd2zz__base64z00,
																												BgL_tmpz00_5752);
																										}
																										{	/* Unsafe/base64.scm 195 */
																											int8_t BgL_xz00_3845;

																											BgL_xz00_3845 =
																												(int8_t)
																												(BgL_arg1856z00_3844);
																											{	/* Unsafe/base64.scm 195 */
																												long
																													BgL_arg2274z00_3846;
																												BgL_arg2274z00_3846 =
																													(long)
																													(BgL_xz00_3845);
																												BgL_res2332z00_3848 =
																													(int) ((long)
																													(BgL_arg2274z00_3846));
																								}}}}
																								BgL_q1z00_2336 =
																									BgL_res2332z00_3848;
																					}}}
																					{	/* Unsafe/base64.scm 250 */
																						int BgL_q2z00_2337;

																						{	/* Unsafe/base64.scm 251 */
																							unsigned char BgL_cz00_3849;

																							BgL_cz00_3849 =
																								(char) (BgL_c2z00_2335);
																							{	/* Unsafe/base64.scm 201 */
																								long BgL_arg1857z00_3850;

																								BgL_arg1857z00_3850 =
																									(
																									(unsigned
																										char) (BgL_cz00_3849));
																								{	/* Unsafe/base64.scm 201 */
																									int BgL_res2333z00_3857;

																									{	/* Unsafe/base64.scm 201 */
																										char BgL_cz00_3852;

																										BgL_cz00_3852 =
																											(signed
																											char)
																											(BgL_arg1857z00_3850);
																										{	/* Unsafe/base64.scm 195 */
																											uint8_t
																												BgL_arg1856z00_3853;
																											{	/* Unsafe/base64.scm 195 */
																												long BgL_tmpz00_5763;

																												BgL_tmpz00_5763 =
																													(long)
																													(BgL_cz00_3852);
																												BgL_arg1856z00_3853 =
																													BGL_U8VREF
																													(BGl_bytezd2tablezd2zz__base64z00,
																													BgL_tmpz00_5763);
																											}
																											{	/* Unsafe/base64.scm 195 */
																												int8_t BgL_xz00_3854;

																												BgL_xz00_3854 =
																													(int8_t)
																													(BgL_arg1856z00_3853);
																												{	/* Unsafe/base64.scm 195 */
																													long
																														BgL_arg2274z00_3855;
																													BgL_arg2274z00_3855 =
																														(long)
																														(BgL_xz00_3854);
																													BgL_res2333z00_3857 =
																														(int) ((long)
																														(BgL_arg2274z00_3855));
																									}}}}
																									BgL_q2z00_2337 =
																										BgL_res2333z00_3857;
																						}}}
																						{	/* Unsafe/base64.scm 251 */
																							long BgL_v0z00_2338;

																							BgL_v0z00_2338 =
																								(
																								((long) (BgL_q0z00_2297) <<
																									(int) (2L)) |
																								((long) (BgL_q1z00_2336) >>
																									(int) (4L)));
																							{	/* Unsafe/base64.scm 252 */
																								long BgL_v1z00_2339;

																								BgL_v1z00_2339 =
																									(
																									(((long) (BgL_q1z00_2336) <<
																											(int) (4L)) & 240L) |
																									((long) (BgL_q2z00_2337) >>
																										(int) (2L)));
																								{	/* Unsafe/base64.scm 254 */

																									{	/* Unsafe/base64.scm 256 */
																										unsigned char
																											BgL_tmpz00_5785;
																										BgL_tmpz00_5785 =
																											(BgL_v0z00_2338);
																										STRING_SET(BgL_resz00_2290,
																											BgL_yz00_2293,
																											BgL_tmpz00_5785);
																									}
																									{	/* Unsafe/base64.scm 257 */
																										unsigned char
																											BgL_auxz00_5790;
																										long BgL_tmpz00_5788;

																										BgL_auxz00_5790 =
																											(BgL_v1z00_2339);
																										BgL_tmpz00_5788 =
																											(BgL_yz00_2293 + 1L);
																										STRING_SET(BgL_resz00_2290,
																											BgL_tmpz00_5788,
																											BgL_auxz00_5790);
																									}
																									if (
																										(BgL_c1z00_2334 ==
																											((unsigned char) '=')))
																										{	/* Unsafe/base64.scm 259 */
																											return
																												bgl_string_shrink
																												(BgL_resz00_2290,
																												BgL_yz00_2293);
																										}
																									else
																										{	/* Unsafe/base64.scm 259 */
																											if (
																												(BgL_c2z00_2335 ==
																													((unsigned char)
																														'=')))
																												{	/* Unsafe/base64.scm 262 */
																													long BgL_tmpz00_5798;

																													BgL_tmpz00_5798 =
																														(BgL_yz00_2293 +
																														1L);
																													return
																														bgl_string_shrink
																														(BgL_resz00_2290,
																														BgL_tmpz00_5798);
																												}
																											else
																												{	/* Unsafe/base64.scm 264 */
																													long BgL_tmpz00_5801;

																													BgL_tmpz00_5801 =
																														(BgL_yz00_2293 +
																														2L);
																													return
																														bgl_string_shrink
																														(BgL_resz00_2290,
																														BgL_tmpz00_5801);
																												}
																										}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	else
																		{	/* Unsafe/base64.scm 265 */
																			bool_t BgL_test2561z00_5804;

																			if (
																				(BgL_xz00_2292 <=
																					(BgL_lenz00_2288 - 2L)))
																				{	/* Unsafe/base64.scm 265 */
																					BgL_test2561z00_5804 =
																						CBOOL(BgL_eofzd2nozd2paddingz00_17);
																				}
																			else
																				{	/* Unsafe/base64.scm 265 */
																					BgL_test2561z00_5804 = ((bool_t) 0);
																				}
																			if (BgL_test2561z00_5804)
																				{	/* Unsafe/base64.scm 266 */
																					unsigned char BgL_c1z00_2358;

																					BgL_c1z00_2358 =
																						STRING_REF(BgL_sz00_16,
																						(BgL_xz00_2292 + 1L));
																					{	/* Unsafe/base64.scm 266 */
																						int BgL_q1z00_2359;

																						{	/* Unsafe/base64.scm 267 */
																							unsigned char BgL_cz00_3894;

																							BgL_cz00_3894 =
																								(char) (BgL_c1z00_2358);
																							{	/* Unsafe/base64.scm 201 */
																								long BgL_arg1857z00_3895;

																								BgL_arg1857z00_3895 =
																									(
																									(unsigned
																										char) (BgL_cz00_3894));
																								{	/* Unsafe/base64.scm 201 */
																									int BgL_res2334z00_3902;

																									{	/* Unsafe/base64.scm 201 */
																										char BgL_cz00_3897;

																										BgL_cz00_3897 =
																											(signed
																											char)
																											(BgL_arg1857z00_3895);
																										{	/* Unsafe/base64.scm 195 */
																											uint8_t
																												BgL_arg1856z00_3898;
																											{	/* Unsafe/base64.scm 195 */
																												long BgL_tmpz00_5815;

																												BgL_tmpz00_5815 =
																													(long)
																													(BgL_cz00_3897);
																												BgL_arg1856z00_3898 =
																													BGL_U8VREF
																													(BGl_bytezd2tablezd2zz__base64z00,
																													BgL_tmpz00_5815);
																											}
																											{	/* Unsafe/base64.scm 195 */
																												int8_t BgL_xz00_3899;

																												BgL_xz00_3899 =
																													(int8_t)
																													(BgL_arg1856z00_3898);
																												{	/* Unsafe/base64.scm 195 */
																													long
																														BgL_arg2274z00_3900;
																													BgL_arg2274z00_3900 =
																														(long)
																														(BgL_xz00_3899);
																													BgL_res2334z00_3902 =
																														(int) ((long)
																														(BgL_arg2274z00_3900));
																									}}}}
																									BgL_q1z00_2359 =
																										BgL_res2334z00_3902;
																						}}}
																						{	/* Unsafe/base64.scm 267 */
																							long BgL_v0z00_2360;

																							BgL_v0z00_2360 =
																								(
																								((long) (BgL_q0z00_2297) <<
																									(int) (2L)) |
																								((long) (BgL_q1z00_2359) >>
																									(int) (4L)));
																							{	/* Unsafe/base64.scm 268 */

																								{	/* Unsafe/base64.scm 270 */
																									unsigned char BgL_tmpz00_5829;

																									BgL_tmpz00_5829 =
																										(BgL_v0z00_2360);
																									STRING_SET(BgL_resz00_2290,
																										BgL_yz00_2293,
																										BgL_tmpz00_5829);
																								}
																								if (
																									(BgL_c1z00_2358 ==
																										((unsigned char) '=')))
																									{	/* Unsafe/base64.scm 271 */
																										return
																											bgl_string_shrink
																											(BgL_resz00_2290,
																											BgL_yz00_2293);
																									}
																								else
																									{	/* Unsafe/base64.scm 273 */
																										long BgL_tmpz00_5835;

																										BgL_tmpz00_5835 =
																											(BgL_yz00_2293 + 1L);
																										return
																											bgl_string_shrink
																											(BgL_resz00_2290,
																											BgL_tmpz00_5835);
																									}
																							}
																						}
																					}
																				}
																			else
																				{	/* Unsafe/base64.scm 274 */
																					bool_t BgL_test2564z00_5838;

																					if (
																						(BgL_xz00_2292 <=
																							(BgL_lenz00_2288 - 1L)))
																						{	/* Unsafe/base64.scm 274 */
																							BgL_test2564z00_5838 =
																								CBOOL
																								(BgL_eofzd2nozd2paddingz00_17);
																						}
																					else
																						{	/* Unsafe/base64.scm 274 */
																							BgL_test2564z00_5838 =
																								((bool_t) 0);
																						}
																					if (BgL_test2564z00_5838)
																						{	/* Unsafe/base64.scm 275 */
																							int BgL_q1z00_2371;

																							{	/* Unsafe/base64.scm 275 */
																								unsigned char BgL_cz00_3921;

																								BgL_cz00_3921 =
																									(char) (((unsigned char)
																										'='));
																								{	/* Unsafe/base64.scm 201 */
																									long BgL_arg1857z00_3922;

																									BgL_arg1857z00_3922 =
																										(
																										(unsigned
																											char) (BgL_cz00_3921));
																									{	/* Unsafe/base64.scm 201 */
																										int BgL_res2335z00_3929;

																										{	/* Unsafe/base64.scm 201 */
																											char BgL_cz00_3924;

																											BgL_cz00_3924 =
																												(signed
																												char)
																												(BgL_arg1857z00_3922);
																											{	/* Unsafe/base64.scm 195 */
																												uint8_t
																													BgL_arg1856z00_3925;
																												{	/* Unsafe/base64.scm 195 */
																													long BgL_tmpz00_5847;

																													BgL_tmpz00_5847 =
																														(long)
																														(BgL_cz00_3924);
																													BgL_arg1856z00_3925 =
																														BGL_U8VREF
																														(BGl_bytezd2tablezd2zz__base64z00,
																														BgL_tmpz00_5847);
																												}
																												{	/* Unsafe/base64.scm 195 */
																													int8_t BgL_xz00_3926;

																													BgL_xz00_3926 =
																														(int8_t)
																														(BgL_arg1856z00_3925);
																													{	/* Unsafe/base64.scm 195 */
																														long
																															BgL_arg2274z00_3927;
																														BgL_arg2274z00_3927
																															=
																															(long)
																															(BgL_xz00_3926);
																														BgL_res2335z00_3929
																															=
																															(int) ((long)
																															(BgL_arg2274z00_3927));
																										}}}}
																										BgL_q1z00_2371 =
																											BgL_res2335z00_3929;
																							}}}
																							{	/* Unsafe/base64.scm 275 */
																								long BgL_v0z00_2372;

																								BgL_v0z00_2372 =
																									(
																									((long) (BgL_q0z00_2297) <<
																										(int) (2L)) |
																									((long) (BgL_q1z00_2371) >>
																										(int) (4L)));
																								{	/* Unsafe/base64.scm 276 */

																									{	/* Unsafe/base64.scm 277 */
																										unsigned char
																											BgL_tmpz00_5861;
																										BgL_tmpz00_5861 =
																											(BgL_v0z00_2372);
																										STRING_SET(BgL_resz00_2290,
																											BgL_yz00_2293,
																											BgL_tmpz00_5861);
																									}
																									return
																										bgl_string_shrink
																										(BgL_resz00_2290,
																										BgL_yz00_2293);
																								}
																							}
																						}
																					else
																						{	/* Unsafe/base64.scm 280 */
																							long BgL_tmpz00_5865;

																							BgL_tmpz00_5865 =
																								(BgL_yz00_2293 + 1L);
																							return
																								bgl_string_shrink
																								(BgL_resz00_2290,
																								BgL_tmpz00_5865);
																						}
																				}
																		}
																}
														}
												}
											}
										}
									}
								else
									{	/* Unsafe/base64.scm 282 */
										bool_t BgL_test2566z00_5868;

										if ((BgL_lenz00_2288 > 2L))
											{	/* Unsafe/base64.scm 282 */
												BgL_test2566z00_5868 =
													(STRING_REF(BgL_sz00_16,
														(BgL_lenz00_2288 - 2L)) == ((unsigned char) '='));
											}
										else
											{	/* Unsafe/base64.scm 282 */
												BgL_test2566z00_5868 = ((bool_t) 0);
											}
										if (BgL_test2566z00_5868)
											{	/* Unsafe/base64.scm 283 */
												long BgL_tmpz00_5874;

												BgL_tmpz00_5874 = (BgL_yz00_2293 - 2L);
												return
													bgl_string_shrink(BgL_resz00_2290, BgL_tmpz00_5874);
											}
										else
											{	/* Unsafe/base64.scm 284 */
												bool_t BgL_test2568z00_5877;

												if ((BgL_lenz00_2288 > 1L))
													{	/* Unsafe/base64.scm 284 */
														BgL_test2568z00_5877 =
															(STRING_REF(BgL_sz00_16,
																(BgL_lenz00_2288 - 1L)) ==
															((unsigned char) '='));
													}
												else
													{	/* Unsafe/base64.scm 284 */
														BgL_test2568z00_5877 = ((bool_t) 0);
													}
												if (BgL_test2568z00_5877)
													{	/* Unsafe/base64.scm 285 */
														long BgL_tmpz00_5883;

														BgL_tmpz00_5883 = (BgL_yz00_2293 - 1L);
														return
															bgl_string_shrink(BgL_resz00_2290,
															BgL_tmpz00_5883);
													}
												else
													{	/* Unsafe/base64.scm 284 */
														if ((BgL_yz00_2293 < BgL_nlenz00_2289))
															{	/* Unsafe/base64.scm 287 */
																return
																	bgl_string_shrink(BgL_resz00_2290,
																	BgL_yz00_2293);
															}
														else
															{	/* Unsafe/base64.scm 287 */
																return BgL_resz00_2290;
															}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* _base64-decode-port */
	obj_t BGl__base64zd2decodezd2portz00zz__base64z00(obj_t BgL_env1160z00_24,
		obj_t BgL_opt1159z00_23)
	{
		{	/* Unsafe/base64.scm 380 */
			{	/* Unsafe/base64.scm 380 */
				obj_t BgL_g1161z00_2410;
				obj_t BgL_g1162z00_2411;

				BgL_g1161z00_2410 = VECTOR_REF(BgL_opt1159z00_23, 0L);
				BgL_g1162z00_2411 = VECTOR_REF(BgL_opt1159z00_23, 1L);
				switch (VECTOR_LENGTH(BgL_opt1159z00_23))
					{
					case 2L:

						{	/* Unsafe/base64.scm 380 */

							{	/* Unsafe/base64.scm 380 */
								obj_t BgL_ipz00_3965;
								obj_t BgL_opz00_3966;

								if (INPUT_PORTP(BgL_g1161z00_2410))
									{	/* Unsafe/base64.scm 380 */
										BgL_ipz00_3965 = BgL_g1161z00_2410;
									}
								else
									{
										obj_t BgL_auxz00_5893;

										BgL_auxz00_5893 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2374z00zz__base64z00, BINT(14330L),
											BGl_string2382z00zz__base64z00,
											BGl_string2379z00zz__base64z00, BgL_g1161z00_2410);
										FAILURE(BgL_auxz00_5893, BFALSE, BFALSE);
									}
								if (OUTPUT_PORTP(BgL_g1162z00_2411))
									{	/* Unsafe/base64.scm 380 */
										BgL_opz00_3966 = BgL_g1162z00_2411;
									}
								else
									{
										obj_t BgL_auxz00_5899;

										BgL_auxz00_5899 =
											BGl_typezd2errorzd2zz__errorz00
											(BGl_string2374z00zz__base64z00, BINT(14330L),
											BGl_string2382z00zz__base64z00,
											BGl_string2380z00zz__base64z00, BgL_g1162z00_2411);
										FAILURE(BgL_auxz00_5899, BFALSE, BFALSE);
									}
								{	/* Unsafe/base64.scm 381 */
									obj_t BgL_arg1960z00_3967;

									{	/* Unsafe/base64.scm 381 */

										BgL_arg1960z00_3967 =
											make_string(84L, ((unsigned char) ' '));
									}
									return
										BGl_z62zc3z04anonymousza31306ze3ze5zz__base64z00
										(BGl_base64zd2decodezd2grammarz00zz__base64z00,
										BgL_ipz00_3965, BgL_opz00_3966, BgL_arg1960z00_3967, 0L,
										84L, BGl_proc2383z00zz__base64z00, ((bool_t) 1));
								}
							}
						}
						break;
					case 3L:

						{	/* Unsafe/base64.scm 380 */
							obj_t BgL_eofzd2nozd2paddingz00_2415;

							BgL_eofzd2nozd2paddingz00_2415 =
								VECTOR_REF(BgL_opt1159z00_23, 2L);
							{	/* Unsafe/base64.scm 380 */

								{	/* Unsafe/base64.scm 380 */
									obj_t BgL_ipz00_3973;
									obj_t BgL_opz00_3974;

									if (INPUT_PORTP(BgL_g1161z00_2410))
										{	/* Unsafe/base64.scm 380 */
											BgL_ipz00_3973 = BgL_g1161z00_2410;
										}
									else
										{
											obj_t BgL_auxz00_5917;

											BgL_auxz00_5917 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2374z00zz__base64z00, BINT(14330L),
												BGl_string2382z00zz__base64z00,
												BGl_string2379z00zz__base64z00, BgL_g1161z00_2410);
											FAILURE(BgL_auxz00_5917, BFALSE, BFALSE);
										}
									if (OUTPUT_PORTP(BgL_g1162z00_2411))
										{	/* Unsafe/base64.scm 380 */
											BgL_opz00_3974 = BgL_g1162z00_2411;
										}
									else
										{
											obj_t BgL_auxz00_5923;

											BgL_auxz00_5923 =
												BGl_typezd2errorzd2zz__errorz00
												(BGl_string2374z00zz__base64z00, BINT(14330L),
												BGl_string2382z00zz__base64z00,
												BGl_string2380z00zz__base64z00, BgL_g1162z00_2411);
											FAILURE(BgL_auxz00_5923, BFALSE, BFALSE);
										}
									{	/* Unsafe/base64.scm 381 */
										obj_t BgL_arg1960z00_3975;

										{	/* Unsafe/base64.scm 381 */

											BgL_arg1960z00_3975 =
												make_string(84L, ((unsigned char) ' '));
										}
										return
											BGl_z62zc3z04anonymousza31306ze3ze5zz__base64z00
											(BGl_base64zd2decodezd2grammarz00zz__base64z00,
											BgL_ipz00_3973, BgL_opz00_3974, BgL_arg1960z00_3975, 0L,
											84L, BGl_proc2384z00zz__base64z00, ((bool_t) 1));
									}
								}
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* &<@anonymous:1962> */
	obj_t BGl_z62zc3z04anonymousza31962ze3ze5zz__base64z00(obj_t BgL_envz00_4284,
		obj_t BgL_cz00_4285)
	{
		{	/* Unsafe/base64.scm 382 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &<@anonymous:1962>2336 */
	obj_t BGl_z62zc3z04anonymousza31962ze32336ze5zz__base64z00(obj_t
		BgL_envz00_4286, obj_t BgL_cz00_4287)
	{
		{	/* Unsafe/base64.scm 382 */
			return BBOOL(((bool_t) 0));
		}

	}



/* base64-decode-port */
	BGL_EXPORTED_DEF obj_t BGl_base64zd2decodezd2portz00zz__base64z00(obj_t
		BgL_ipz00_20, obj_t BgL_opz00_21, obj_t BgL_eofzd2nozd2paddingz00_22)
	{
		{	/* Unsafe/base64.scm 380 */
			{	/* Unsafe/base64.scm 381 */
				obj_t BgL_arg1960z00_3981;

				{	/* Unsafe/base64.scm 381 */

					BgL_arg1960z00_3981 = make_string(84L, ((unsigned char) ' '));
				}
				return
					BGl_z62zc3z04anonymousza31306ze3ze5zz__base64z00
					(BGl_base64zd2decodezd2grammarz00zz__base64z00, BgL_ipz00_20,
					BgL_opz00_21, BgL_arg1960z00_3981, 0L, 84L,
					BGl_proc2385z00zz__base64z00, ((bool_t) 1));
			}
		}

	}



/* &<@anonymous:1962>2337 */
	obj_t BGl_z62zc3z04anonymousza31962ze32337ze5zz__base64z00(obj_t
		BgL_envz00_4289, obj_t BgL_cz00_4290)
	{
		{	/* Unsafe/base64.scm 382 */
			return BBOOL(((bool_t) 0));
		}

	}



/* pem-decode-port */
	BGL_EXPORTED_DEF obj_t BGl_pemzd2decodezd2portz00zz__base64z00(obj_t
		BgL_ipz00_25, obj_t BgL_opz00_26)
	{
		{	/* Unsafe/base64.scm 413 */
			{	/* Unsafe/base64.scm 431 */
				obj_t BgL_startz00_2424;

				BgL_startz00_2424 =
					BGl_z62zc3z04anonymousza31565ze3ze5zz__base64z00
					(BGl_pemzd2markupzd2grammarz00zz__base64z00, BgL_ipz00_25, 0L);
				if (bigloo_strcmp_at(BgL_startz00_2424, BGl_string2386z00zz__base64z00,
						0L))
					{	/* Unsafe/base64.scm 433 */
						obj_t BgL_arg1964z00_2426;

						{	/* Ieee/string.scm 172 */

							BgL_arg1964z00_2426 = make_string(84L, ((unsigned char) ' '));
						}
						{	/* Unsafe/base64.scm 435 */
							obj_t BgL_zc3z04anonymousza31966ze3z87_4291;

							{
								int BgL_tmpz00_5962;

								BgL_tmpz00_5962 = (int) (2L);
								BgL_zc3z04anonymousza31966ze3z87_4291 =
									MAKE_L_PROCEDURE
									(BGl_z62zc3z04anonymousza31966ze3ze5zz__base64z00,
									BgL_tmpz00_5962);
							}
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31966ze3z87_4291,
								(int) (0L), BgL_ipz00_25);
							PROCEDURE_L_SET(BgL_zc3z04anonymousza31966ze3z87_4291,
								(int) (1L), BgL_startz00_2424);
							return
								BGl_z62zc3z04anonymousza31306ze3ze5zz__base64z00
								(BGl_base64zd2decodezd2grammarz00zz__base64z00, BgL_ipz00_25,
								BgL_opz00_26, BgL_arg1964z00_2426, 0L, 84L,
								BgL_zc3z04anonymousza31966ze3z87_4291, ((bool_t) 0));
						}
					}
				else
					{	/* Unsafe/base64.scm 438 */
						BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1969z00_2435;

						{	/* Unsafe/base64.scm 438 */
							BgL_z62iozd2parsezd2errorz62_bglt BgL_new1086z00_2436;

							{	/* Unsafe/base64.scm 438 */
								BgL_z62iozd2parsezd2errorz62_bglt BgL_new1085z00_2439;

								BgL_new1085z00_2439 =
									((BgL_z62iozd2parsezd2errorz62_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_z62iozd2parsezd2errorz62_bgl))));
								{	/* Unsafe/base64.scm 438 */
									long BgL_arg1972z00_2440;

									BgL_arg1972z00_2440 =
										BGL_CLASS_NUM(BGl_z62iozd2parsezd2errorz62zz__objectz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1085z00_2439),
										BgL_arg1972z00_2440);
								}
								BgL_new1086z00_2436 = BgL_new1085z00_2439;
							}
							((((BgL_z62exceptionz62_bglt) COBJECT(
											((BgL_z62exceptionz62_bglt) BgL_new1086z00_2436)))->
									BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_z62exceptionz62_bglt) COBJECT(((BgL_z62exceptionz62_bglt)
												BgL_new1086z00_2436)))->BgL_locationz00) =
								((obj_t) BFALSE), BUNSPEC);
							{
								obj_t BgL_auxz00_5987;

								{	/* Unsafe/base64.scm 438 */
									obj_t BgL_arg1970z00_2437;

									{	/* Unsafe/base64.scm 438 */
										obj_t BgL_arg1971z00_2438;

										{	/* Unsafe/base64.scm 438 */
											obj_t BgL_classz00_4022;

											BgL_classz00_4022 =
												BGl_z62iozd2parsezd2errorz62zz__objectz00;
											BgL_arg1971z00_2438 =
												BGL_CLASS_ALL_FIELDS(BgL_classz00_4022);
										}
										BgL_arg1970z00_2437 = VECTOR_REF(BgL_arg1971z00_2438, 2L);
									}
									BgL_auxz00_5987 =
										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
										(BgL_arg1970z00_2437);
								}
								((((BgL_z62exceptionz62_bglt) COBJECT(
												((BgL_z62exceptionz62_bglt) BgL_new1086z00_2436)))->
										BgL_stackz00) = ((obj_t) BgL_auxz00_5987), BUNSPEC);
							}
							((((BgL_z62errorz62_bglt) COBJECT(
											((BgL_z62errorz62_bglt) BgL_new1086z00_2436)))->
									BgL_procz00) =
								((obj_t) BGl_string2368z00zz__base64z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1086z00_2436)))->BgL_msgz00) =
								((obj_t) BGl_string2387z00zz__base64z00), BUNSPEC);
							((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
												BgL_new1086z00_2436)))->BgL_objz00) =
								((obj_t) BgL_startz00_2424), BUNSPEC);
							BgL_arg1969z00_2435 = BgL_new1086z00_2436;
						}
						return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1969z00_2435));
					}
			}
		}

	}



/* &pem-decode-port */
	obj_t BGl_z62pemzd2decodezd2portz62zz__base64z00(obj_t BgL_envz00_4292,
		obj_t BgL_ipz00_4293, obj_t BgL_opz00_4294)
	{
		{	/* Unsafe/base64.scm 413 */
			{	/* Unsafe/base64.scm 416 */
				obj_t BgL_auxz00_6008;
				obj_t BgL_auxz00_6001;

				if (OUTPUT_PORTP(BgL_opz00_4294))
					{	/* Unsafe/base64.scm 416 */
						BgL_auxz00_6008 = BgL_opz00_4294;
					}
				else
					{
						obj_t BgL_auxz00_6011;

						BgL_auxz00_6011 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2374z00zz__base64z00,
							BINT(15725L), BGl_string2388z00zz__base64z00,
							BGl_string2380z00zz__base64z00, BgL_opz00_4294);
						FAILURE(BgL_auxz00_6011, BFALSE, BFALSE);
					}
				if (INPUT_PORTP(BgL_ipz00_4293))
					{	/* Unsafe/base64.scm 416 */
						BgL_auxz00_6001 = BgL_ipz00_4293;
					}
				else
					{
						obj_t BgL_auxz00_6004;

						BgL_auxz00_6004 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2374z00zz__base64z00,
							BINT(15725L), BGl_string2388z00zz__base64z00,
							BGl_string2379z00zz__base64z00, BgL_ipz00_4293);
						FAILURE(BgL_auxz00_6004, BFALSE, BFALSE);
					}
				return
					BGl_pemzd2decodezd2portz00zz__base64z00(BgL_auxz00_6001,
					BgL_auxz00_6008);
			}
		}

	}



/* &<@anonymous:1966> */
	obj_t BGl_z62zc3z04anonymousza31966ze3ze5zz__base64z00(obj_t BgL_envz00_4295,
		obj_t BgL_cz00_4298)
	{
		{	/* Unsafe/base64.scm 434 */
			{	/* Unsafe/base64.scm 435 */
				obj_t BgL_ipz00_4296;
				obj_t BgL_startz00_4297;

				BgL_ipz00_4296 = ((obj_t) PROCEDURE_L_REF(BgL_envz00_4295, (int) (0L)));
				BgL_startz00_4297 = PROCEDURE_L_REF(BgL_envz00_4295, (int) (1L));
				{
					obj_t BgL_startz00_4538;
					obj_t BgL_cz00_4539;

					{	/* Unsafe/base64.scm 435 */
						obj_t BgL_arg1967z00_4562;

						{	/* Unsafe/base64.scm 435 */
							long BgL_arg1968z00_4563;

							BgL_arg1968z00_4563 = STRING_LENGTH(((obj_t) BgL_startz00_4297));
							BgL_arg1967z00_4562 =
								c_substring(
								((obj_t) BgL_startz00_4297), 7L, BgL_arg1968z00_4563);
						}
						BgL_startz00_4538 = BgL_arg1967z00_4562;
						BgL_cz00_4539 = BgL_cz00_4298;
						if ((CCHAR(BgL_cz00_4539) == ((unsigned char) '-')))
							{	/* Unsafe/base64.scm 421 */
								obj_t BgL_endz00_4540;

								BgL_endz00_4540 =
									BGl_z62zc3z04anonymousza31565ze3ze5zz__base64z00
									(BGl_pemzd2markupzd2grammarz00zz__base64z00, BgL_ipz00_4296,
									1L);
								if (bigloo_strcmp_at(BgL_endz00_4540,
										BGl_string2389z00zz__base64z00, 0L))
									{	/* Unsafe/base64.scm 423 */
										bool_t BgL_test2580z00_6035;

										{	/* Unsafe/base64.scm 423 */
											obj_t BgL_arg1983z00_4541;

											{	/* Unsafe/base64.scm 423 */
												long BgL_arg1984z00_4542;

												BgL_arg1984z00_4542 =
													STRING_LENGTH(((obj_t) BgL_endz00_4540));
												BgL_arg1983z00_4541 =
													c_substring(
													((obj_t) BgL_endz00_4540), 5L, BgL_arg1984z00_4542);
											}
											{	/* Unsafe/base64.scm 423 */
												long BgL_l1z00_4543;

												BgL_l1z00_4543 = STRING_LENGTH(BgL_startz00_4538);
												if (
													(BgL_l1z00_4543 ==
														STRING_LENGTH(BgL_arg1983z00_4541)))
													{	/* Unsafe/base64.scm 423 */
														int BgL_arg2057z00_4544;

														{	/* Unsafe/base64.scm 423 */
															char *BgL_auxz00_6046;
															char *BgL_tmpz00_6044;

															BgL_auxz00_6046 =
																BSTRING_TO_STRING(BgL_arg1983z00_4541);
															BgL_tmpz00_6044 =
																BSTRING_TO_STRING(BgL_startz00_4538);
															BgL_arg2057z00_4544 =
																memcmp(BgL_tmpz00_6044, BgL_auxz00_6046,
																BgL_l1z00_4543);
														}
														BgL_test2580z00_6035 =
															((long) (BgL_arg2057z00_4544) == 0L);
													}
												else
													{	/* Unsafe/base64.scm 423 */
														BgL_test2580z00_6035 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2580z00_6035)
											{	/* Unsafe/base64.scm 423 */
												return BTRUE;
											}
										else
											{	/* Unsafe/base64.scm 426 */
												BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1979z00_4545;

												{	/* Unsafe/base64.scm 426 */
													BgL_z62iozd2parsezd2errorz62_bglt BgL_new1084z00_4546;

													{	/* Unsafe/base64.scm 426 */
														BgL_z62iozd2parsezd2errorz62_bglt
															BgL_new1083z00_4547;
														BgL_new1083z00_4547 =
															((BgL_z62iozd2parsezd2errorz62_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_z62iozd2parsezd2errorz62_bgl))));
														{	/* Unsafe/base64.scm 426 */
															long BgL_arg1982z00_4548;

															BgL_arg1982z00_4548 =
																BGL_CLASS_NUM
																(BGl_z62iozd2parsezd2errorz62zz__objectz00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1083z00_4547), BgL_arg1982z00_4548);
														}
														BgL_new1084z00_4546 = BgL_new1083z00_4547;
													}
													((((BgL_z62exceptionz62_bglt) COBJECT(
																	((BgL_z62exceptionz62_bglt)
																		BgL_new1084z00_4546)))->BgL_fnamez00) =
														((obj_t) BFALSE), BUNSPEC);
													((((BgL_z62exceptionz62_bglt)
																COBJECT(((BgL_z62exceptionz62_bglt)
																		BgL_new1084z00_4546)))->BgL_locationz00) =
														((obj_t) BFALSE), BUNSPEC);
													{
														obj_t BgL_auxz00_6059;

														{	/* Unsafe/base64.scm 426 */
															obj_t BgL_arg1980z00_4549;

															{	/* Unsafe/base64.scm 426 */
																obj_t BgL_arg1981z00_4550;

																{	/* Unsafe/base64.scm 426 */
																	obj_t BgL_classz00_4551;

																	BgL_classz00_4551 =
																		BGl_z62iozd2parsezd2errorz62zz__objectz00;
																	BgL_arg1981z00_4550 =
																		BGL_CLASS_ALL_FIELDS(BgL_classz00_4551);
																}
																BgL_arg1980z00_4549 =
																	VECTOR_REF(BgL_arg1981z00_4550, 2L);
															}
															BgL_auxz00_6059 =
																BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																(BgL_arg1980z00_4549);
														}
														((((BgL_z62exceptionz62_bglt) COBJECT(
																		((BgL_z62exceptionz62_bglt)
																			BgL_new1084z00_4546)))->BgL_stackz00) =
															((obj_t) BgL_auxz00_6059), BUNSPEC);
													}
													((((BgL_z62errorz62_bglt) COBJECT(
																	((BgL_z62errorz62_bglt)
																		BgL_new1084z00_4546)))->BgL_procz00) =
														((obj_t) BGl_string2368z00zz__base64z00), BUNSPEC);
													((((BgL_z62errorz62_bglt)
																COBJECT(((BgL_z62errorz62_bglt)
																		BgL_new1084z00_4546)))->BgL_msgz00) =
														((obj_t) BGl_string2390z00zz__base64z00), BUNSPEC);
													((((BgL_z62errorz62_bglt)
																COBJECT(((BgL_z62errorz62_bglt)
																		BgL_new1084z00_4546)))->BgL_objz00) =
														((obj_t) BgL_endz00_4540), BUNSPEC);
													BgL_arg1979z00_4545 = BgL_new1084z00_4546;
												}
												return
													BGl_raisez00zz__errorz00(
													((obj_t) BgL_arg1979z00_4545));
											}
									}
								else
									{	/* Unsafe/base64.scm 422 */
										return BFALSE;
									}
							}
						else
							{	/* Unsafe/base64.scm 417 */
								BgL_z62iozd2parsezd2errorz62_bglt BgL_arg1985z00_4552;

								{	/* Unsafe/base64.scm 417 */
									BgL_z62iozd2parsezd2errorz62_bglt BgL_new1082z00_4553;

									{	/* Unsafe/base64.scm 417 */
										BgL_z62iozd2parsezd2errorz62_bglt BgL_new1081z00_4554;

										BgL_new1081z00_4554 =
											((BgL_z62iozd2parsezd2errorz62_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62iozd2parsezd2errorz62_bgl))));
										{	/* Unsafe/base64.scm 417 */
											long BgL_arg1991z00_4555;

											BgL_arg1991z00_4555 =
												BGL_CLASS_NUM
												(BGl_z62iozd2parsezd2errorz62zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
													BgL_new1081z00_4554), BgL_arg1991z00_4555);
										}
										BgL_new1082z00_4553 = BgL_new1081z00_4554;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1082z00_4553)))->
											BgL_fnamez00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1082z00_4553)))->BgL_locationz00) =
										((obj_t) BFALSE), BUNSPEC);
									{
										obj_t BgL_auxz00_6081;

										{	/* Unsafe/base64.scm 417 */
											obj_t BgL_arg1986z00_4556;

											{	/* Unsafe/base64.scm 417 */
												obj_t BgL_arg1987z00_4557;

												{	/* Unsafe/base64.scm 417 */
													obj_t BgL_classz00_4558;

													BgL_classz00_4558 =
														BGl_z62iozd2parsezd2errorz62zz__objectz00;
													BgL_arg1987z00_4557 =
														BGL_CLASS_ALL_FIELDS(BgL_classz00_4558);
												}
												BgL_arg1986z00_4556 =
													VECTOR_REF(BgL_arg1987z00_4557, 2L);
											}
											BgL_auxz00_6081 =
												BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
												(BgL_arg1986z00_4556);
										}
										((((BgL_z62exceptionz62_bglt) COBJECT(
														((BgL_z62exceptionz62_bglt) BgL_new1082z00_4553)))->
												BgL_stackz00) = ((obj_t) BgL_auxz00_6081), BUNSPEC);
									}
									((((BgL_z62errorz62_bglt) COBJECT(
													((BgL_z62errorz62_bglt) BgL_new1082z00_4553)))->
											BgL_procz00) =
										((obj_t) BGl_string2368z00zz__base64z00), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(((BgL_z62errorz62_bglt)
														BgL_new1082z00_4553)))->BgL_msgz00) =
										((obj_t) BGl_string2391z00zz__base64z00), BUNSPEC);
									{
										obj_t BgL_auxz00_6091;

										{	/* Unsafe/base64.scm 420 */
											obj_t BgL_arg1988z00_4559;

											BgL_arg1988z00_4559 =
												BGl_readzd2linezd2zz__r4_input_6_10_2z00
												(BgL_ipz00_4296);
											{	/* Unsafe/base64.scm 420 */
												obj_t BgL_list1989z00_4560;

												{	/* Unsafe/base64.scm 420 */
													obj_t BgL_arg1990z00_4561;

													BgL_arg1990z00_4561 =
														MAKE_YOUNG_PAIR(BgL_arg1988z00_4559, BNIL);
													BgL_list1989z00_4560 =
														MAKE_YOUNG_PAIR(BgL_cz00_4539, BgL_arg1990z00_4561);
												}
												BgL_auxz00_6091 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string2370z00zz__base64z00,
													BgL_list1989z00_4560);
										}}
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_new1082z00_4553)))->
												BgL_objz00) = ((obj_t) BgL_auxz00_6091), BUNSPEC);
									}
									BgL_arg1985z00_4552 = BgL_new1082z00_4553;
								}
								return BGl_raisez00zz__errorz00(((obj_t) BgL_arg1985z00_4552));
							}
					}
				}
			}
		}

	}



/* pem-read-file */
	BGL_EXPORTED_DEF obj_t BGl_pemzd2readzd2filez00zz__base64z00(obj_t
		BgL_filez00_27)
	{
		{	/* Unsafe/base64.scm 446 */
			{	/* Unsafe/base64.scm 447 */
				obj_t BgL_pz00_4024;

				{	/* Unsafe/base64.scm 447 */

					{	/* Unsafe/base64.scm 447 */

						BgL_pz00_4024 =
							BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
					}
				}
				{	/* Unsafe/base64.scm 450 */
					obj_t BgL_zc3z04anonymousza31993ze3z87_4299;

					BgL_zc3z04anonymousza31993ze3z87_4299 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31993ze3ze5zz__base64z00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31993ze3z87_4299,
						(int) (0L), BgL_pz00_4024);
					BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
						(BgL_filez00_27, BgL_zc3z04anonymousza31993ze3z87_4299);
				}
				return bgl_close_output_port(BgL_pz00_4024);
			}
		}

	}



/* &pem-read-file */
	obj_t BGl_z62pemzd2readzd2filez62zz__base64z00(obj_t BgL_envz00_4300,
		obj_t BgL_filez00_4301)
	{
		{	/* Unsafe/base64.scm 446 */
			{	/* Unsafe/base64.scm 447 */
				obj_t BgL_auxz00_6108;

				if (STRINGP(BgL_filez00_4301))
					{	/* Unsafe/base64.scm 447 */
						BgL_auxz00_6108 = BgL_filez00_4301;
					}
				else
					{
						obj_t BgL_auxz00_6111;

						BgL_auxz00_6111 =
							BGl_typezd2errorzd2zz__errorz00(BGl_string2374z00zz__base64z00,
							BINT(16871L), BGl_string2392z00zz__base64z00,
							BGl_string2376z00zz__base64z00, BgL_filez00_4301);
						FAILURE(BgL_auxz00_6111, BFALSE, BFALSE);
					}
				return BGl_pemzd2readzd2filez00zz__base64z00(BgL_auxz00_6108);
			}
		}

	}



/* &<@anonymous:1993> */
	obj_t BGl_z62zc3z04anonymousza31993ze3ze5zz__base64z00(obj_t BgL_envz00_4302)
	{
		{	/* Unsafe/base64.scm 449 */
			{	/* Unsafe/base64.scm 450 */
				obj_t BgL_pz00_4303;

				BgL_pz00_4303 = ((obj_t) PROCEDURE_REF(BgL_envz00_4302, (int) (0L)));
				{	/* Unsafe/base64.scm 450 */
					obj_t BgL_arg1994z00_4564;

					{	/* Unsafe/base64.scm 450 */
						obj_t BgL_tmpz00_6119;

						BgL_tmpz00_6119 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1994z00_4564 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_6119);
					}
					return
						BGl_pemzd2decodezd2portz00zz__base64z00(BgL_arg1994z00_4564,
						BgL_pz00_4303);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zz__base64z00(void)
	{
		{	/* Unsafe/base64.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zz__base64z00(void)
	{
		{	/* Unsafe/base64.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zz__base64z00(void)
	{
		{	/* Unsafe/base64.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zz__base64z00(void)
	{
		{	/* Unsafe/base64.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zz__paramz00(453939141L,
				BSTRING_TO_STRING(BGl_string2393z00zz__base64z00));
		}

	}

#ifdef __cplusplus
}
#endif
